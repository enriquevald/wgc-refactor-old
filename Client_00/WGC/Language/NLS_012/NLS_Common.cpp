// ------------------------------------------------------------------------------------
// Copyright � 2002 Win Systems
// ------------------------------------------------------------------------------------
// 
//   MODULE NAME : NLS_Common.cpp
//
//   DESCRIPTION : Support to NLS resources
//
//        AUTHOR : Carlos A. Costa
//
// CREATION DATE : 05-MAR-2002
//
// REVISION HISTORY:
// 
// Date         Author Description
// -----------  ------ ------------------------------------------------------------------
// 05-MAR-2002  CC     Initial release
// 25-APR-2002  TJG    New function GetModuleNLSBase
// 19-JUN-2002  APB    Added definition for COMMON_MODULE_ID_GUI_STOCKS and 
//                     COMMON_MODULE_ID_SERVICES.
// 20-JUN-2002  APB    Renamed COMMON_MODULE_ID_GUI_STOCKS to 
//                     COMMON_MODULE_ID_GUI_TICKETS_MANAGER.
// 01-AUG-2002  APB    Added definition for COMMON_MODULE_ID_RESOURCE_MANAGER and 
//                     COMMON_MODULE_RESOURCE_MANAGER.
// 27-AUG-2002  APB    Added definition for COMMON_MODULE_ID_BATCH_TRX_MGR and 
//                     COMMON_MODULE_BATCH_TRX_MGR.
// 02-OCT-2002  APB    Added definition for COMMON_MODULE_ID_ONLINE_TRX_MGR and 
//                     COMMON_MODULE_ONLINE_TRX_MGR.
// 18-OCT-2002  APB    Added definition for COMMON_MODULE_ID_TRX_AUDIT and 
//                     COMMON_MODULE_TRX_AUDIT.
// 19-NOV-2002  APB    Added definition for COMMON_MODULE_ID_LKC_DATABASE and 
//                     COMMON_MODULE_LKC_DATABASE.
// 27-NOV-2002  APB    Added definition for COMMON_MODULE_ID_GUI_SALES_CONTROL and 
//                     COMMON_MODULE_GUI_SALES_CONTROL. Corrected definitions for
//                     COMMON_MODULE_ID_GUI_CONTROL and COMMON_MODULE_ID_GUI_CONF.
// 09-JAN-2003  APB    Added definition for COMMON_MODULE_ID_GUI_JACKPOT_MANAGER and 
//                     COMMON_MODULE_GUI_JACKPOT_MANAGER.
// 23-JAN-2003  APB    Added definition for COMMON_MODULE_ID_GUI_SYSTEM_MONITOR and 
//                     COMMON_MODULE_GUI_SYSTEM_MONITOR.
// 28-APR-2003  APB    Renamed COMMON_MODULE_ID_GUI_SALES_CONTROL and 
//                     COMMON_MODULE_GUI_SALES_CONTROL to COMMON_MODULE_ID_GUI_STATISTICS 
//                     and COMMON_MODULE_GUI_STATISTICS.
// 21-MAY-2003  APB    Added definition for COMMON_MODULE_ID_SW_DOWNLOAD_MGR and
//                     COMMON_MODULE_SW_DOWNLOAD_MGR.
// 22-MAY-2003  APB    Added definition for COMMON_MODULE_ID_GUI_INVOICE_SYSTEM and
//                     COMMON_MODULE_GUI_INVOICE_SYSTEM.
// 04-JUN-2003  APB    Added definition for COMMON_MODULE_ID_GUI_SW_DOWNLOAD and  
//                     COMMON_MODULE_GUI_SW_DOWNLOAD.
// 04-DEC-2003  OC    Added definition for COMMON_MODULE_ID_GUI_LAUNCH and  
//                     COMMON_MODULE_GUI_LAUNCH.
// 17-MAR-2004  JOR   Added definition for COMMON_MODULE_ID_GUI_DATE_CODE and  
//                     COMMON_MODULE_GUI_DATE_CODE.
// 29-OCT-2004  GZ    Added definition for COMMON_MODULE_ID_LKAS_FUNCTIONS and  
//                     COMMON_MODULE_LKAS_FUNCTIONS.
// --------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------
//  INCLUDES
//------------------------------------------------------------------------------------
#include "CommonDef.h"

#include "NLS_Common.h"
#include "NLS_Only.h"

//------------------------------------------------------------------------------------
//  PRIVATE CONSTANTS
//------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------
//  PRIVATE DATATYPES
//------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------
//  PRIVATE DATA STRUCTURES
//------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------
//  PRIVATE FUNCTION PROTOTYPES
//------------------------------------------------------------------------------------
BOOL        SearchModuleInList (TYPE_MODULE_NLS     * pList,
                                DWORD               NumItems,
                                DWORD               ModuleId,
                                DWORD               * pNLSBaseId)
{
  DWORD     _idx;

  for ( _idx = 0; _idx < NumItems; _idx++ )
  {
    if ( pList[_idx].module_id == ModuleId ) 
    {
      * pNLSBaseId = pList[_idx].nls_base_id;

      return TRUE;
    } // if
  } // if

  return FALSE;
} // SearchModuleInList

//------------------------------------------------------------------------------------
//  PUBLIC FUNCTIONS IMPLEMENTATION
//------------------------------------------------------------------------------------

NLS_CLIENT_API BOOL WINAPI      NLS_GetModuleBase (DWORD    ModuleId,
                                                   DWORD    * pNLSBaseId)
{
  static    TYPE_MODULE_NLS _module_id_nls [] = 
  {  (COMMON_MODULE_ID_ALARMS              , COMMON_MODULE_ALARMS)
   , (COMMON_MODULE_ID_VERSION_CONTROL     , COMMON_MODULE_VERSION_CONTROL)
   , (COMMON_MODULE_ID_GUI_ALARMS          , COMMON_MODULE_GUI_ALARMS)
   , (COMMON_MODULE_ID_GUI_COMM            , COMMON_MODULE_GUI_COMM)
   , (COMMON_MODULE_ID_GUI_CONTROL         , COMMON_MODULE_GUI_CONTROL)
   , (COMMON_MODULE_ID_GUI_CONF            , COMMON_MODULE_GUI_CONF)
   , (COMMON_MODULE_ID_GUI_VERSION_CONTROL , COMMON_MODULE_GUI_VERSION_CONTROL)
   , (COMMON_MODULE_ID_LOGGER              , COMMON_MODULE_LOGGER)
   , (COMMON_MODULE_ID_GUI_CONTROLS        , COMMON_MODULE_GUI_CONTROLS)
   , (COMMON_MODULE_ID_GUI_SETUP           , COMMON_MODULE_GUI_SETUP)
   , (COMMON_MODULE_ID_GUI_AUDIT           , COMMON_MODULE_GUI_AUDIT)
   , (COMMON_MODULE_ID_GUI_COMMON_MISC     , COMMON_MODULE_GUI_COMMON_MISC)
   , (COMMON_MODULE_ID_APPLICATION_LOG     , COMMON_MODULE_APPLICATION_LOG)
   , (COMMON_MODULE_ID_SERVICES            , COMMON_MODULE_SERVICES)
   , (COMMON_MODULE_ID_GUI_TICKETS_MANAGER , COMMON_MODULE_GUI_TICKETS_MANAGER)
   , (COMMON_MODULE_ID_RESOURCE_MANAGER    , COMMON_MODULE_RESOURCE_MANAGER)
   , (COMMON_MODULE_ID_BATCH_TRX_MGR       , COMMON_MODULE_BATCH_TRX_MGR)
   , (COMMON_MODULE_ID_TRX_AUDIT           , COMMON_MODULE_TRX_AUDIT)
   , (COMMON_MODULE_ID_ONLINE_TRX_MGR      , COMMON_MODULE_ONLINE_TRX_MGR)
   , (COMMON_MODULE_ID_LKC_DATABASE        , COMMON_MODULE_LKC_DATABASE)
   , (COMMON_MODULE_ID_GUI_STATISTICS      , COMMON_MODULE_GUI_STATISTICS)
   , (COMMON_MODULE_ID_GUI_JACKPOT_MANAGER , COMMON_MODULE_GUI_JACKPOT_MANAGER)
   , (COMMON_MODULE_ID_GUI_SYSTEM_MONITOR  , COMMON_MODULE_GUI_SYSTEM_MONITOR)
   , (COMMON_MODULE_ID_SW_DOWNLOAD_MGR     , COMMON_MODULE_SW_DOWNLOAD_MGR)
   , (COMMON_MODULE_ID_GUI_INVOICE_SYSTEM  , COMMON_MODULE_GUI_INVOICE_SYSTEM)
   , (COMMON_MODULE_ID_GUI_SW_DOWNLOAD     , COMMON_MODULE_GUI_SW_DOWNLOAD)
   , (COMMON_MODULE_ID_GUI_LAUNCH          , COMMON_MODULE_GUI_LAUNCH)
   , (COMMON_MODULE_ID_LKAS_FUNCTIONS      , COMMON_MODULE_LKAS_FUNCTIONS)
   , (COMMON_MODULE_ID_GUI_CLASS_II        , COMMON_MODULE_GUI_CLASS_II)
   , (COMMON_MODULE_ID_GUI_PLAYER_TRACKING , COMMON_MODULE_GUI_PLAYER_TRACKING)

  } ;

  if ( SearchModuleInList (_module_id_nls,
                           sizeof (_module_id_nls),
                           ModuleId,
                           pNLSBaseId) )
  {
    // Found
    return TRUE;
  } // IF

  // Look for it in the client NLS id's range
  return GetClientModuleNLSBase (ModuleId, pNLSBaseId);
} // NLS_GetModuleBase
