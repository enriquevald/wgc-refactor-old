// ------------------------------------------------------------------------------------
// Copyright � 2002 Win Systems
// ------------------------------------------------------------------------------------
//
//   MODULE NAME: LKC_NLS_Client.h
//
//   DESCRIPTION: NLS Id ranges for Client LKC modules
//
//        AUTHOR: Carlos A. Costa
//
// CREATION DATE: 13-MAR-2002
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ -------------------------------------------------------------------
// 13-MAR-2002 CC     Initial release
// --------------------------------------------------------------------------------------

#ifndef __NLS_CLIENT_MODULES
#define __NLS_CLIENT_MODULES

// NLS Id ranges for Client LKC modules
#include    "NLS_Common.h"

// #define CLIENT_MODULE_xxx        COMMON_CLIENT_START + 500    // xxx module
// #define CLIENT_MODULE_yyy        COMMON_CLIENT_START + 1000   // yyy module
//
// #define NLS_ID_xxx(n)                (COMMON_MODULE_xxx + n)
// #define NLS_ID_yyy(n)                (COMMON_MODULE_yyy + n)

#endif // __NLS_CLIENT_MODULES
