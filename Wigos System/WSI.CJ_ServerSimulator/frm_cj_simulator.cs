using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Messaging;
using System.Net;
using System.Xml;
using WSI.Common;
using System.IO;

namespace WSI.CJ_ServerSimulator
{
  public partial class frm_cj_simulator:Form
  {
    CJ_ServerSimulator server = new CJ_ServerSimulator();
    Int32 num_rcv;
    Int32 num_snt;

    public frm_cj_simulator ()
    {
      InitializeComponent ();
      num_rcv = 0;
      num_snt = 0;

      // RCI 16-DEC-2010: Load last used data.
      LoadLastUsedData();
    }

    private void button1_Click (object sender, EventArgs e)
    {
      // RCI 16-DEC-2010: Save last used data.
      SaveLastUsedData();

      server.Init(txt_server.Text, txt_client.Text);
      server.LoadDatabase ();
      server.Start (true);

      dgv_cards.DataSource = server.Cards;

      txt_server.Enabled = false;
      txt_client.Enabled = false;
      button1.Enabled = false;
    }

    private void tmr_refresh_Tick (object sender, EventArgs e)
    {
      this.lbl_rcv_counter.Text = num_rcv.ToString ();
      this.lbl_snt_counter.Text = num_snt.ToString ();

      server.SaveDatabase ();
    }

    private void btn_add_Click (object sender, EventArgs e)
    {
      DataTable cards;
      Decimal amount;
      Decimal balance;

      amount = 0;
      Decimal.TryParse (txt_amount.Text, out amount);

      cards = server.Cards;

      lock ( cards )
      {
        foreach ( DataRow card in cards.Rows  )
        {
          balance = (Decimal) card["CashBalance"];
          balance += amount;
          card["CashBalance"] = balance;
        }
      }
    }

    private void btn_log_off_Click (object sender, EventArgs e)
    {
      DataTable cards;

      cards = server.Cards;

      lock ( cards )
      {
        foreach ( DataRow card in cards.Rows )
        {
          card["Machine"] = null;
        }
      }

    }

    private void txt_client_TextChanged(object sender, EventArgs e)
    {

    }

    private void txt_server_TextChanged(object sender, EventArgs e)
    {

    }

    //------------------------------------------------------------------------------
    // PURPOSE : Load last used data: Server & Client Queues.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void LoadLastUsedData()
    {
      String _file;
      String _line;
      StreamReader _sr;

      try
      {
        _file = "USER_DATA";

        if (!File.Exists(_file))
        {
          return;
        }

        using (_sr = new StreamReader(_file))
        {
          if ((_line = _sr.ReadLine()) != null)
          {
            txt_server.Text = _line.Trim();
          }
          if ((_line = _sr.ReadLine()) != null)
          {
            txt_client.Text = _line.Trim();
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } // LoadLastUsedData

    //------------------------------------------------------------------------------
    // PURPOSE : Save last used data: Server & Client Queues.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void SaveLastUsedData()
    {
      String _file;
      StringBuilder _line;
      StreamWriter _sw;

      try
      {
        _file = "USER_DATA";
        _sw = new StreamWriter(_file);

        _line = new StringBuilder();
        _line.Append(txt_server.Text);
        _sw.WriteLine(_line.ToString());

        _line = new StringBuilder();
        _line.Append(txt_client.Text);
        _sw.WriteLine(_line.ToString());

        _sw.Close();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } // SaveLastUsedData
  }
}