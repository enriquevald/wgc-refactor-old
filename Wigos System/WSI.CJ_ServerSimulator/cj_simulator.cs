using System;
using System.Collections.Generic;
using System.Windows.Forms;
using WSI.Common;

namespace WSI.CJ_ServerSimulator
{
  static class cj_simulator
  {
    /// <summary>
    /// The main entry point for the application.
    /// </summary>
    [MTAThread]
    static void Main ()
    {
      Log.AddListener(new SimpleLogger("CJ"));

      Application.EnableVisualStyles ();
      Application.SetCompatibleTextRenderingDefault (false);
      Application.Run(new frm_cj_simulator());
      //Application.Run(new frm_cj_redirector());
    }
  }
}