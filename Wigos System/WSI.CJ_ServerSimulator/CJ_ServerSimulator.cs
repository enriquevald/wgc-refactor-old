using System;
using System.Collections.Generic;
using System.Text;
using System.Messaging;
using System.Data;
using WSI.Common;

using WSI.WCP.CadillacJack;

namespace WSI.CJ_ServerSimulator
{
  class CJ_ServerSimulator:MQ_Listener 
  {
    DataSet cjdb;
    private static UInt32 current_seq_number;

    public DataTable Cards
    {
      get
      {
        return cjdb.Tables["Cards"];
      }
    }

    public void LoadDatabase ()
    {
      LoadCJDB ();

      cjdb.AcceptChanges ();
    }

    public void SaveDatabase ()
    {
      if ( cjdb != null )
      {
        cjdb.WriteXml ("CJDB.xml", XmlWriteMode.WriteSchema);
      }
    }

    private void LoadCJDB ()
    {
      DataSet ds;
      ds = new DataSet ();

      try
      {
        ds.ReadXml ("CJDB.xml", XmlReadMode.ReadSchema);
      }
      catch
      {
        try
        {
          CreateTable ();
          ds.ReadXml ("CJDB.xml", XmlReadMode.ReadSchema);
        }
        catch (Exception ex)
        {
          Log.Exception(ex);
        }
      }
      cjdb = ds;
    }

    private void CreateTable ()
    {
      DataSet ds;
      DataTable cards;
      DataColumn col;
      DataColumn[] pk;

      ds = new DataSet ("CJDB");
      cards = ds.Tables.Add ("Cards");

      col = cards.Columns.Add ("Track");
      col.DataType = Type.GetType ("System.Int64");
      col.AutoIncrementSeed = 100000000000000000;
      col.AutoIncrementStep = 1;
      col.AutoIncrement = true;

      col = cards.Columns.Add ("Active");
      col.DataType = Type.GetType ("System.Boolean");
      col.AllowDBNull = false;
      col.DefaultValue = true;
      
      col = cards.Columns.Add ("PlayerFirstName");
      col.DataType = Type.GetType ("System.String");
      col.AllowDBNull = false;
      col.DefaultValue = "PlayerFirstName";

      col = cards.Columns.Add ("PlayerLastName");
      col.DataType = Type.GetType ("System.String");
      col.AllowDBNull = false;
      col.DefaultValue = "PlayerLastName";

      col = cards.Columns.Add ("CashBalance");
      col.DataType = Type.GetType ("System.Decimal");
      col.AllowDBNull = false;
      col.DefaultValue = 0;

      col = cards.Columns.Add ("Machine");
      col.DataType = Type.GetType ("System.String");
      col.AllowDBNull = true;

      pk = new DataColumn[1];
      pk[0] = cards.Columns["Track"];

      cards.PrimaryKey = pk;

      ds.WriteXml ("CJDB.xml", XmlWriteMode.WriteSchema);
    }


    protected override void MessageReceived (String Xml, ref Boolean DequeueMessage)
    {
      CJ_Message cj_request;
      CJ_Message cj_response;
      Boolean out_of_sequence = false;

      cj_request = CJ_Message.CreateMessage(Xml);
      cj_response = null;

      // Check for sequence number
      //if (current_seq_number != 0
      //    && current_seq_number + 1 != cj_request.MsgHeader.SequenceNumber)
      //{
      //  Log.Error("Out of Sequence detected. Expected: " + (current_seq_number + 1).ToString() + " Received: " + cj_request.MsgHeader.SequenceNumber.ToString());

      //  out_of_sequence = true;
      //}
      //else
      //{
        current_seq_number = cj_request.MsgHeader.SequenceNumber;
      //}

      //Log.Message("Arrived sequence number: " + cj_request.MsgHeader.SequenceNumber.ToString()); 

      switch (cj_request.MsgHeader.MsgType )
      {
        case CJ_MsgTypes.PlayerCardIn:
          {
            CJ_MsgPlayerCardInReply response;
            cj_response = CJ_Message.CreateMessage(CJ_MsgTypes.PlayerCardInReply);
            response = (CJ_MsgPlayerCardInReply) cj_response.MsgContent;
            response.PlayerFirstName = "";
            response.PlayerLastName = "";
            response.CashBalance = 0;
            response.AbandonedCardTime = 3 * 60; // 3 Minutes
            response.ErrorCode = CJ_ErrCode.NoError;

            // Response
            //  - Header

            lock ( Cards )
            {
              DataRow[] cards;
              DataRow the_card;

              cards = Cards.Select ("Track=" + cj_request.MsgHeader.TrackingNumber1);
              if (cards.Length == 1)
              {
                the_card = cards[0];

                //  - Content
                response.PlayerFirstName = (String)the_card["PlayerFirstName"];
                response.PlayerLastName = (String)the_card["PlayerLastName"];
                response.CashBalance = (uint)(((Decimal)the_card["CashBalance"]) * 100);
                response.AbandonedCardTime = 3 * 60; // 3 Minutes

                if (!(Boolean)the_card["Active"])
                {
                  // Account Inactive
                  response.ErrorCode = CJ_ErrCode.PlayerAccountInactive;

                  Log.Error("Player Account is inactive. CardId: " + the_card["Track"].ToString());
                }
                else
                {
                  if (the_card.IsNull("Machine"))
                  {
                    // Previously not logged
                    response.ErrorCode = CJ_ErrCode.NoError;
                    response.ErrMsg = response.ErrorCode.ToString();

                    the_card["Machine"] = cj_request.MsgHeader.EPSName;
                  }
                  else
                  {
                    if (cj_request.MsgHeader.EPSName.Equals(the_card["Machine"]))
                    {

                      Log.Error("Player Account already logged in. CardId: " + the_card["Track"].ToString());

                      // Logged on the same machine
                      // Previously not logged
                      response.ErrorCode = CJ_ErrCode.PlayerAccountAlreadyLoggedIn;

                    }
                    else
                    {
                      Log.Error("Player Account already logged in. CardId: " + the_card["Track"].ToString());

                      // Logged on a different same machine
                      response.ErrorCode = CJ_ErrCode.PlayerAccountAlreadyLoggedIn;
                    }
                  }
                }
              }
              else
              {
                // No registered in CJ
                response.ErrorCode = CJ_ErrCode.PlayerAccountDoesNotExist;
                Log.Error("Account does not exist. Card Track number: " + cj_request.MsgHeader.TrackingNumber1.ToString());

              }
            }

            if (out_of_sequence)
            {
              response.ErrorCode = CJ_ErrCode.MessageOutOfSequence;
              response.ErrMsg = "Message Out of Sequence. Expecting Sequence " + (current_seq_number + 1).ToString();
            }
            else
            {
              response.ErrMsg = response.ErrorCode.ToString();
            }

          }
          break;

        case CJ_MsgTypes.PlayMsg:
          {
            CJ_MsgPlayMsg request;
            CJ_MsgPlayACKMsg response;
            cj_response = CJ_Message.CreateMessage(CJ_MsgTypes.PlayACKMsg);

            request = (CJ_MsgPlayMsg)cj_request.MsgContent;
            response = (CJ_MsgPlayACKMsg)cj_response.MsgContent;

            // Response
            //  - Header

            lock ( Cards )
            {
              DataRow[] cards;
              DataRow the_card;

              cards = Cards.Select ("Track=" + cj_request.MsgHeader.TrackingNumber1);
              if ( cards.Length == 1 )
              {
                the_card = cards[0];

                if ( cj_request.MsgHeader.EPSName.Equals (the_card["Machine"]) )
                {
                  // Check Initial Balance
                  Decimal ini_bal;
                  Decimal fin_bal;
                  Decimal fin_balx;

                  ini_bal = request.StartingBalance;
                  ini_bal /= 100;
                  fin_bal = request.EndingBalance;
                  fin_bal /= 100;

                  if ( !ini_bal.Equals (the_card["CashBalance"])  )
                  {

                    Log.Error("Starting Balance does not match. Local: " + the_card["CashBalance"].ToString() + " Received: " + ini_bal.ToString()); 

                    //throw (new Exception ("Initial Balance doesn't match"));
                    response.ErrorCode = CJ_ErrCode.PAPTAccountingDiscrepancy;
                  }
                  
                  fin_balx = ini_bal;
                  fin_balx = fin_balx - ((Decimal)(request.TotalBetCents))/100;
                  if ( fin_balx < 0 )
                  {
                    Log.Error("Player Account Balance is negative. StartingBalance: " + fin_balx.ToString() + " Bet: " + request.TotalBetCents.ToString());

                    response.ErrorCode = CJ_ErrCode.PlayerAccountBalanceIsNegative;

                  }
                  fin_balx = fin_balx + ((Decimal)(request.TotalWinCents))/100;
                  if ( fin_balx != fin_bal )
                  {
                    Log.Error("Final Balance does not match. Local: " + fin_balx.ToString() + " Final: " + fin_bal.ToString()); 

                    //throw (new Exception ("Final Balance doesn't mach"));
                    response.ErrorCode = CJ_ErrCode.PAPTAccountingDiscrepancy;
                  }

                  //  - Content                  
                  response.ErrMsg = response.ErrorCode.ToString ();
                  response.StartingBalance = request.StartingBalance;
                  response.EndingBalance = request.EndingBalance;
                  response.Denomination = request.Denomination;
                  response.TotalBetCents = request.TotalBetCents;
                  response.TotalWinCents = request.TotalWinCents;

                  Decimal bal;
                  bal = request.EndingBalance;
                  bal /= 100;
                  the_card["CashBalance"] = bal;
                
                }
              }
            }

            if (out_of_sequence)
            {
              response.ErrorCode = CJ_ErrCode.MessageOutOfSequence;
              response.ErrMsg = "Message Out of Sequence. Expecting Sequence " + (current_seq_number + 1).ToString();
            }
            else
            {
              response.ErrMsg = response.ErrorCode.ToString();
            }

          }
          break;

        case CJ_MsgTypes.PlayerCardOut:
          {
            CJ_MsgPlayerCardOut request;
            CJ_MsgPlayerCardOutReply response;

            cj_response = CJ_Message.CreateMessage(CJ_MsgTypes.PlayerCardOutReply);

            request = (CJ_MsgPlayerCardOut)cj_request.MsgContent;
            response = (CJ_MsgPlayerCardOutReply)cj_response.MsgContent;

            // Response
            //  - Header

            //  - Content
            response.ErrorCode = CJ_ErrCode.NoError;
            response.ErrMsg = response.ErrorCode.ToString();
            response.CashBalance = request.CashBalance;

            // Response
            //  - Header

            lock ( Cards )
            {
              DataRow[] cards;
              DataRow the_card; 

              cards = Cards.Select ("Track=" + cj_request.MsgHeader.TrackingNumber1);
              if ( cards.Length == 1 )
              {
                the_card = cards[0];

                if ( cj_request.MsgHeader.EPSName.Equals (the_card["Machine"]) )
                {
                  the_card["Machine"] = null;
                }
              }
            }
            if (out_of_sequence)
            {
              response.ErrorCode = CJ_ErrCode.MessageOutOfSequence;
              response.ErrMsg = "Message Out of Sequence. Expecting Sequence " + (current_seq_number + 1).ToString();
            }
            else
            {
              response.ErrMsg = response.ErrorCode.ToString();
            }
          }
          break;
      } // switch ( msg_type )

      

      if ( cj_response != null )
      {
        // Copy Sequence Number
        cj_response.MsgHeader.SequenceNumber = cj_request.MsgHeader.SequenceNumber;
        cj_response.MsgHeader.EPSName = cj_request.MsgHeader.EPSName;
        cj_response.MsgHeader.TrackingNumber1 = cj_request.MsgHeader.TrackingNumber1;

        base.Send(cj_response.ToXml());
      }
    }
  }
}
