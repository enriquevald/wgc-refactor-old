namespace WSI.CJ_ServerSimulator
{
  partial class frm_cj_redirector
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.label1 = new System.Windows.Forms.Label();
      this.txt_q0 = new System.Windows.Forms.TextBox();
      this.label2 = new System.Windows.Forms.Label();
      this.txt_q1 = new System.Windows.Forms.TextBox();
      this.label3 = new System.Windows.Forms.Label();
      this.txt_q2 = new System.Windows.Forms.TextBox();
      this.label4 = new System.Windows.Forms.Label();
      this.txt_q3 = new System.Windows.Forms.TextBox();
      this.btn_start = new System.Windows.Forms.Button();
      this.timer1 = new System.Windows.Forms.Timer(this.components);
      this.lbl_state = new System.Windows.Forms.Label();
      this.txt_vendor_ip = new System.Windows.Forms.TextBox();
      this.label5 = new System.Windows.Forms.Label();
      this.SuspendLayout();
      // 
      // label1
      // 
      this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(12, 19);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(82, 13);
      this.label1.TabIndex = 5;
      this.label1.Text = "Request Queue";
      // 
      // txt_q0
      // 
      this.txt_q0.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.txt_q0.Location = new System.Drawing.Point(12, 38);
      this.txt_q0.Name = "txt_q0";
      this.txt_q0.Size = new System.Drawing.Size(597, 20);
      this.txt_q0.TabIndex = 4;
      this.txt_q0.Text = ".\\Private$\\WIN15RecQ";
      // 
      // label2
      // 
      this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(50, 72);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(97, 13);
      this.label2.TabIndex = 7;
      this.label2.Text = "CJ Request Queue";
      // 
      // txt_q1
      // 
      this.txt_q1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.txt_q1.Location = new System.Drawing.Point(50, 91);
      this.txt_q1.Name = "txt_q1";
      this.txt_q1.Size = new System.Drawing.Size(559, 20);
      this.txt_q1.TabIndex = 6;
      this.txt_q1.Text = "FormatName:DIRECT=HTTP://10.0.4.15/msmq/Private$/WIN15RecQ";
      // 
      // label3
      // 
      this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(50, 129);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(84, 13);
      this.label3.TabIndex = 9;
      this.label3.Text = "CJ Reply Queue";
      // 
      // txt_q2
      // 
      this.txt_q2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.txt_q2.Location = new System.Drawing.Point(50, 145);
      this.txt_q2.Name = "txt_q2";
      this.txt_q2.Size = new System.Drawing.Size(559, 20);
      this.txt_q2.TabIndex = 8;
      this.txt_q2.Text = ".\\Private$\\WIN15ReplyQ";
      // 
      // label4
      // 
      this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(15, 180);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(69, 13);
      this.label4.TabIndex = 11;
      this.label4.Text = "Reply Queue";
      // 
      // txt_q3
      // 
      this.txt_q3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.txt_q3.Location = new System.Drawing.Point(15, 199);
      this.txt_q3.Name = "txt_q3";
      this.txt_q3.Size = new System.Drawing.Size(597, 20);
      this.txt_q3.TabIndex = 10;
      this.txt_q3.Text = "FormatName:DIRECT=HTTP://192.165.150.251/msmq/Private$/WIN15ReplyQ";
      // 
      // btn_start
      // 
      this.btn_start.Location = new System.Drawing.Point(537, 244);
      this.btn_start.Name = "btn_start";
      this.btn_start.Size = new System.Drawing.Size(75, 23);
      this.btn_start.TabIndex = 12;
      this.btn_start.Text = "Start";
      this.btn_start.UseVisualStyleBackColor = true;
      this.btn_start.Click += new System.EventHandler(this.btn_start_Click);
      // 
      // timer1
      // 
      this.timer1.Enabled = true;
      this.timer1.Interval = 350;
      this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
      // 
      // lbl_state
      // 
      this.lbl_state.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_state.AutoSize = true;
      this.lbl_state.Location = new System.Drawing.Point(15, 249);
      this.lbl_state.Name = "lbl_state";
      this.lbl_state.Size = new System.Drawing.Size(0, 13);
      this.lbl_state.TabIndex = 13;
      // 
      // txt_vendor_ip
      // 
      this.txt_vendor_ip.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.txt_vendor_ip.Location = new System.Drawing.Point(514, 64);
      this.txt_vendor_ip.Name = "txt_vendor_ip";
      this.txt_vendor_ip.Size = new System.Drawing.Size(95, 20);
      this.txt_vendor_ip.TabIndex = 14;
      this.txt_vendor_ip.Text = "10.1.1.115";
      // 
      // label5
      // 
      this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.label5.AutoSize = true;
      this.label5.Location = new System.Drawing.Point(454, 67);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(54, 13);
      this.label5.TabIndex = 15;
      this.label5.Text = "Vendor IP";
      // 
      // frm_cj_redirector
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(643, 284);
      this.Controls.Add(this.label5);
      this.Controls.Add(this.txt_vendor_ip);
      this.Controls.Add(this.lbl_state);
      this.Controls.Add(this.btn_start);
      this.Controls.Add(this.label4);
      this.Controls.Add(this.txt_q3);
      this.Controls.Add(this.label3);
      this.Controls.Add(this.txt_q2);
      this.Controls.Add(this.label2);
      this.Controls.Add(this.txt_q1);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.txt_q0);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
      this.Name = "frm_cj_redirector";
      this.Text = "WSI.CadillacJackRedirector";
      this.Load += new System.EventHandler(this.frm_cj_redirector_Load);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.TextBox txt_q0;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.TextBox txt_q1;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.TextBox txt_q2;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.TextBox txt_q3;
    private System.Windows.Forms.Button btn_start;
    private System.Windows.Forms.Timer timer1;
    private System.Windows.Forms.Label lbl_state;
    private System.Windows.Forms.TextBox txt_vendor_ip;
    private System.Windows.Forms.Label label5;
  }
}