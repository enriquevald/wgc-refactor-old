using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Messaging;
using WSI.WCP.CadillacJack; 


namespace WSI.CJ_ServerSimulator
{
  public partial class frm_cj_redirector : Form
  {
    MessageQueue q0;
    MessageQueue q1;
    MessageQueue q2;
    MessageQueue q3;
    Int32 num_requests = 0;
    Int32 num_responses = 0;
    String vendor_ip;

    public frm_cj_redirector()
    {
      InitializeComponent();
    }

    private void frm_cj_redirector_Load(object sender, EventArgs e)
    {

    }

    private void RequestThreadOld()
    {
      System.Messaging.Message m;
      String xml;

      while (true)
      {
        m = q0.Receive();
        xml = (String)m.Body;
        q1.Send(xml);
        num_requests++;
      }
    }

    private void RequestThread()
    {
      System.Messaging.Message m;
      String xml;
      String x1;
      String x2;
      int idx;

      while (true)
      {
        ////CJ_Message cj;

        m = q0.Receive();
        xml = (String)m.Body;

        idx = xml.IndexOf("<VendorIP>");
        x1 = xml.Substring(0, idx) + "<VendorIP>";
        idx = xml.IndexOf("</VendorIP>");
        x2 = xml.Substring(idx, xml.Length - idx );

        xml = x1 + vendor_ip + x2;

        q1.Send(xml);
        num_requests++;

        m = q2.Receive();
        xml = (String)m.Body;
        q3.Send(xml);
        num_responses++;
      }
    }

    private void ResponseThread()
    {
      System.Messaging.Message m;
      String xml;

      while (true)
      {
        m = q2.Receive();
        xml = (String)m.Body;
        q3.Send(xml);
        num_responses++;
      }
    }

    private void btn_start_Click(object sender, EventArgs e)
    {
      btn_start.Enabled = false;

      if (!MessageQueue.Exists(txt_q0.Text) )
      {
        q0 = MessageQueue.Create(txt_q0.Text);
      }
      else
      {
        q0 = new MessageQueue(txt_q0.Text);
      }


      q1 = new MessageQueue(txt_q1.Text);

      if (!MessageQueue.Exists(txt_q2.Text))
      {
        q2 = MessageQueue.Create(txt_q2.Text);
      }
      else
      {
        q2 = new MessageQueue(txt_q2.Text);
      }

      q3 = new MessageQueue(txt_q3.Text);

      q0.Formatter = new CJMessageFormatter();
      q1.Formatter = new CJMessageFormatter();
      q2.Formatter = new CJMessageFormatter();
      q3.Formatter = new CJMessageFormatter();

      vendor_ip = txt_vendor_ip.Text;

      System.Threading.Thread thread;
      thread = new System.Threading.Thread(RequestThread);
      thread.Start();

      ////thread = new System.Threading.Thread(ResponseThread);
      ////thread.Start();
    }

    private void timer1_Tick(object sender, EventArgs e)
    {
      lbl_state.Text = "Requests: " + num_requests.ToString() + "  Responses: " + num_responses.ToString();
    }

  


  }
}