namespace WSI.CJ_ServerSimulator
{
  partial class frm_cj_simulator
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose (bool disposing)
    {
      if ( disposing && ( components != null ) )
      {
        components.Dispose ();
      }
      base.Dispose (disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent ()
    {
      this.components = new System.ComponentModel.Container();
      this.button1 = new System.Windows.Forms.Button();
      this.txt_server = new System.Windows.Forms.TextBox();
      this.label1 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.lbl_rcv_counter = new System.Windows.Forms.Label();
      this.label4 = new System.Windows.Forms.Label();
      this.lbl_snt_counter = new System.Windows.Forms.Label();
      this.label6 = new System.Windows.Forms.Label();
      this.tmr_refresh = new System.Windows.Forms.Timer(this.components);
      this.label3 = new System.Windows.Forms.Label();
      this.txt_client = new System.Windows.Forms.TextBox();
      this.dgv_cards = new System.Windows.Forms.DataGridView();
      this.btn_add = new System.Windows.Forms.Button();
      this.txt_amount = new System.Windows.Forms.TextBox();
      this.btn_log_off = new System.Windows.Forms.Button();
      ((System.ComponentModel.ISupportInitialize)(this.dgv_cards)).BeginInit();
      this.SuspendLayout();
      // 
      // button1
      // 
      this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.button1.Location = new System.Drawing.Point(664, 207);
      this.button1.Name = "button1";
      this.button1.Size = new System.Drawing.Size(93, 23);
      this.button1.TabIndex = 0;
      this.button1.Text = "Start";
      this.button1.UseVisualStyleBackColor = true;
      this.button1.Click += new System.EventHandler(this.button1_Click);
      // 
      // txt_server
      // 
      this.txt_server.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.txt_server.Location = new System.Drawing.Point(12, 37);
      this.txt_server.Name = "txt_server";
      this.txt_server.Size = new System.Drawing.Size(745, 20);
      this.txt_server.TabIndex = 2;
      this.txt_server.Text = ".\\Private$\\WIN15RecQ";
      this.txt_server.TextChanged += new System.EventHandler(this.txt_server_TextChanged);
      // 
      // label1
      // 
      this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(12, 18);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(73, 13);
      this.label1.TabIndex = 3;
      this.label1.Text = "Server Queue";
      // 
      // label2
      // 
      this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(12, 70);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(107, 13);
      this.label2.TabIndex = 4;
      this.label2.Text = "Received Messages:";
      // 
      // lbl_rcv_counter
      // 
      this.lbl_rcv_counter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_rcv_counter.AutoSize = true;
      this.lbl_rcv_counter.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_rcv_counter.Location = new System.Drawing.Point(122, 70);
      this.lbl_rcv_counter.Name = "lbl_rcv_counter";
      this.lbl_rcv_counter.Size = new System.Drawing.Size(14, 13);
      this.lbl_rcv_counter.TabIndex = 5;
      this.lbl_rcv_counter.Text = "0";
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(126, 70);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(0, 13);
      this.label4.TabIndex = 6;
      // 
      // lbl_snt_counter
      // 
      this.lbl_snt_counter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_snt_counter.AutoSize = true;
      this.lbl_snt_counter.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_snt_counter.Location = new System.Drawing.Point(122, 161);
      this.lbl_snt_counter.Name = "lbl_snt_counter";
      this.lbl_snt_counter.Size = new System.Drawing.Size(14, 13);
      this.lbl_snt_counter.TabIndex = 8;
      this.lbl_snt_counter.Text = "0";
      // 
      // label6
      // 
      this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.label6.AutoSize = true;
      this.label6.Location = new System.Drawing.Point(12, 161);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(83, 13);
      this.label6.TabIndex = 7;
      this.label6.Text = "Sent Messages:";
      // 
      // tmr_refresh
      // 
      this.tmr_refresh.Enabled = true;
      this.tmr_refresh.Interval = 500;
      this.tmr_refresh.Tick += new System.EventHandler(this.tmr_refresh_Tick);
      // 
      // label3
      // 
      this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(12, 109);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(68, 13);
      this.label3.TabIndex = 10;
      this.label3.Text = "Client Queue";
      // 
      // txt_client
      // 
      this.txt_client.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.txt_client.Location = new System.Drawing.Point(12, 128);
      this.txt_client.Name = "txt_client";
      this.txt_client.Size = new System.Drawing.Size(745, 20);
      this.txt_client.TabIndex = 9;
      this.txt_client.Text = ".\\Private$\\WIN15ReplyQ";
      this.txt_client.TextChanged += new System.EventHandler(this.txt_client_TextChanged);
      // 
      // dgv_cards
      // 
      this.dgv_cards.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                  | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.dgv_cards.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.dgv_cards.Location = new System.Drawing.Point(12, 245);
      this.dgv_cards.Name = "dgv_cards";
      this.dgv_cards.Size = new System.Drawing.Size(745, 316);
      this.dgv_cards.TabIndex = 12;
      // 
      // btn_add
      // 
      this.btn_add.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_add.Location = new System.Drawing.Point(682, 581);
      this.btn_add.Name = "btn_add";
      this.btn_add.Size = new System.Drawing.Size(75, 23);
      this.btn_add.TabIndex = 14;
      this.btn_add.Text = "Add";
      this.btn_add.UseVisualStyleBackColor = true;
      this.btn_add.Click += new System.EventHandler(this.btn_add_Click);
      // 
      // txt_amount
      // 
      this.txt_amount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.txt_amount.Location = new System.Drawing.Point(575, 583);
      this.txt_amount.Name = "txt_amount";
      this.txt_amount.Size = new System.Drawing.Size(100, 20);
      this.txt_amount.TabIndex = 15;
      this.txt_amount.Text = "1000";
      this.txt_amount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      // 
      // btn_log_off
      // 
      this.btn_log_off.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.btn_log_off.Location = new System.Drawing.Point(12, 580);
      this.btn_log_off.Name = "btn_log_off";
      this.btn_log_off.Size = new System.Drawing.Size(75, 23);
      this.btn_log_off.TabIndex = 16;
      this.btn_log_off.Text = "Log Off";
      this.btn_log_off.UseVisualStyleBackColor = true;
      this.btn_log_off.Click += new System.EventHandler(this.btn_log_off_Click);
      // 
      // frm_cj_simulator
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(771, 610);
      this.Controls.Add(this.btn_log_off);
      this.Controls.Add(this.txt_amount);
      this.Controls.Add(this.btn_add);
      this.Controls.Add(this.dgv_cards);
      this.Controls.Add(this.label3);
      this.Controls.Add(this.txt_client);
      this.Controls.Add(this.lbl_snt_counter);
      this.Controls.Add(this.label6);
      this.Controls.Add(this.label4);
      this.Controls.Add(this.lbl_rcv_counter);
      this.Controls.Add(this.label2);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.txt_server);
      this.Controls.Add(this.button1);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "frm_cj_simulator";
      this.Text = "CJ Simulator";
      ((System.ComponentModel.ISupportInitialize)(this.dgv_cards)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Button button1;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.TextBox txt_server;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.Label lbl_rcv_counter;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label lbl_snt_counter;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.Timer tmr_refresh;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.TextBox txt_client;
    private System.Windows.Forms.DataGridView dgv_cards;
    private System.Windows.Forms.Button btn_add;
    private System.Windows.Forms.TextBox txt_amount;
    private System.Windows.Forms.Button btn_log_off;
  }
}

