﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using WSI.Common;

namespace WS_3GS
{
  //public delegate void MessageEventHandler(object sender, EventArgs e);
  // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "WigosService" in code, svc and config file together.
  // NOTE: In order to launch WCF Test Client for testing this service, please select WigosService.svc or WigosService.svc.cs at the Solution Explorer and start debugging.
  [ServiceBehavior(InstanceContextMode=InstanceContextMode.Single)]
  public class WebService: IWigosService
  {
    //public event MessageEventHandler MessageReceived;
    public WebService()
    {
      
    }

    public bool StartSession(string value)
    {
      return true;
    }

    public bool EndSession(string value)
    {
      return true;
    }

    public bool UpdateSession(string value)
    {
      return true;
    }

    public bool Events(string value)
    {
      return true;
    }
  }
}
