﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WS_3GS
{
  // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IWigosService" in both code and config file together.
  [ServiceContract]
  public interface IWigosService
  {

    [OperationContract]
    Boolean StartSession(String value);

    [OperationContract]
    Boolean EndSession(String value);

    [OperationContract]
    Boolean UpdateSession(String value);

    [OperationContract]
    Boolean Events(String value);
  }
}
