﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Text;
using System.Threading;
using WSI.Common;

namespace WS_3GS
{
  class WebServices
  {

    public static void Main()
    {
      WebServices.Init();

      while (true) ;
    }

    /// <summary>
    /// Initialize WigosService
    /// </summary>
    public static void Init()
    {
      try
      {
        Uri _base_address;
        _base_address = GetBaseAdress();

        // Create the ServiceHost.
        WebService _ws = new WebService();
        ServiceHost host = new ServiceHost(_ws, _base_address);

        // Enable metadata publishing.
        ServiceMetadataBehavior smb = new ServiceMetadataBehavior();
        smb.HttpGetEnabled = true;
        smb.MetadataExporter.PolicyVersion = PolicyVersion.Policy15;
        host.Description.Behaviors.Add(smb);

        // Open the ServiceHost to start listening for messages. Since
        // no endpoints are explicitly configured, the runtime will create
        // one endpoint per base address for each service contract implemented
        // by the service.
        host.Open();

        Console.WriteLine("The service [3GSService] is ready at " + _base_address);
        Log.Message("The service [3GSService] is ready at " + _base_address);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Log.Error("The service [3GSService] cant run, check configuration!!!!");
      }
    }

    private static Uri GetBaseAdress()
    {
      Uri _base_address;
      _base_address = null;

      try
      {
        if (System.IO.File.Exists("WigosService.cfg"))
        {
          _base_address = new Uri(System.IO.File.ReadAllText("WigosService.cfg"));
        }
      }
      catch
      {
        Log.Error("Error in WigosService.cfg, take URL from GeneralParam");
      }

      try
      {
        if (_base_address == null)
        {
          //_base_address = new Uri(GeneralParam.GetString("PlayerTracking.ExternalLoyaltyProgram", "WebService.Uri"));
          _base_address = new Uri("http://localhost:7779/WigosService");
        }
      }
      catch
      {
        Log.Error("Incorrect GP: PlayerTracking.ExternalLoyaltyProgram - WebService.Uri");
      }

      return _base_address;
    }
  }
}
