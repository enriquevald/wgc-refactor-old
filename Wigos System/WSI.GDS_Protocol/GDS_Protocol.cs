//------------------------------------------------------------------------------
// Copyright © 2008 Win Systems International
//------------------------------------------------------------------------------
// 
//   MODULE NAME: GDS_Protocol.cs
// 
//   DESCRIPTION: GDS Protocol Dll - 
//                
// 
//        AUTHOR: Ronald Rodríguez
// 
// CREATION DATE: 12-AUG-2008
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 12-AUG-2008 RRT    First release.
//------------------------------------------------------------------------------

using System;
using System.Timers;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using System.Net;
using System.Net.Sockets;
using WSI.GDS;
using WSI.Common;

namespace WSI.GDS.GDS_Protocol
{

  [StructLayout(LayoutKind.Sequential)]
  public struct GDS_Parameters
  {
    public int keep_alive_interval;
    public int connection_timeout;
    public long utc_datetime;
    public int time_zone;
  }

  public enum GDS_Msg
  {
      // Request                 // Reply
      RequestService,            RequestServiceReply
    , EnrollServer,              EnrollServerReply
    , EnrollTerminal,            EnrollTerminalReply
    , UnenrollTerminal,          UnenrollTerminalReply
    , UnenrollServer,            UnenrollServerReply
    , GetParameters,             GetParametersReply
    , NewParamsNotification ,    NewParamsNotificationReply
    , NotifyCurrentParameters,   NotifyCurrentParametersReply
    , ExecuteCommand,            ExecuteCommandReply
    , KeepAlive,                 KeepAliveReply
    , Error
    , Unknown
  };

  public enum GDS_RC
  {
    OK
  ,
    ERROR
  ,
    DATABASE_OFFLINE
  ,
    SERVICE_NOT_AVAILABLE
  ,
    SERVICE_NOT_FOUND
  ,
    SERVER_NOT_AUTHORIZED
  ,
    SERVER_SESSION_NOT_VALID
  ,
    TERMINAL_NOT_AUTHORIZED
  ,
    TERMINAL_SESSION_NOT_VALID
  ,
    CARD_NOT_VALID
  ,
    CARD_BLOCKED
  ,
    CARD_EXPIRED
  ,
    CARD_ALREADY_IN_USE
  ,
    CARD_NOT_VALIDATED
  ,
    CARD_BALANCE_MISMATCH
  ,
    CARD_REMOTE_BALANCE_MISMATCH
  ,
    CARD_SESSION_NOT_CLOSED
  ,
    CARD_SESSION_NOT_VALID
  ,
    INVALID_SESSION_ID
  ,
    UNKNOWN_GAME_ID
  ,
    INVALID_AMOUNT
  ,
    EVENT_NOT_VALID
  ,
    CRC_MISMATCH
  ,
    NOT_ENOUGH_PLAYERS

  ,
    MSG_FORMAT_ERROR
  ,
    INVALID_MSG_TYPE
  ,
    INVALID_PROTOCOL_VERSION
  ,
    INVALID_SEQUENCE_ID
  ,
    INVALID_SERVER_ID
  ,
    INVALID_TERMINAL_ID
  ,
    INVALID_SERVER_SESSION_ID
  ,
    INVALID_TERMINAL_SESSION_ID
  ,
    INVALID_COMPRESSION_METHOD
  ,
    INVALID_RESPONSE_CODE

  ,
    UNKNOWN
  , LAST
  }
  
  public interface GDS_IMessage
  {
    void GetSequenceId([Out, MarshalAs(UnmanagedType.I8)] out Int64 SequenceId);
    void GetMessageType(out GDS_Msg MessageType);
    void GetResponseCode(out GDS_RC ResponseCode);
  }

  public interface GDS_IReportEventList
  {
    void AddDeviceStatus(GDS_IMessage Request, long Code, long Status, long Priority, long Date);
    void AddOperation(GDS_IMessage Request, long Code, long Data, long Date);
  }

  /// <summary>
  /// Interface for GDS Protocol. 
  /// </summary>
  public interface GDS_IProtocol
  {

    // Public Requests from Kiosk protocol

    // Basic operations
    void Start([In, MarshalAs(UnmanagedType.BStr, SizeConst = 50)] String TerminalName);
    void Stop();

    void GetIsConnected([Out, MarshalAs(UnmanagedType.Bool)] out Boolean IsConnected);
    void GetSequenceId([Out, MarshalAs(UnmanagedType.I8)] out Int64 SequenceId);
    void GetTransactionId([Out, MarshalAs(UnmanagedType.I8)] out Int64 TransactionId);

    void WaitForMessage(out GDS_IMessage ReceivedMessage);
    void Send(GDS_IMessage MessageToSend);
    void NewRequest_GetParameters(out GDS_IMessage Request, long SequenceId);
    void GetReply_GetParameters    (GDS_IMessage ReceivedMessage, out GDS_Parameters Parameters);
  } // GDS_IProtocol

  /// <summary>
  /// Protocol Class
  /// </summary>
  public class GDS_Protocol_Class : GDS_IProtocol
  {

    #region Constants

    private const Int32 GDS_TIMEOUT = 5000;

    #endregion

    #region Enums


    

    private enum GDS_PROTOCOL_STATUS
    {
      UNKNOWN,
      DISCONNECTED,
      ENROLLING,
      GETTING_PARAMETERS,
      CONNECTED,
    }
    #endregion

    #region Members

    private WaitableQueue GDS_queue_send;         // Queue to send messages.
    private WaitableQueue GDS_queue_receive;      // Queue for all responses.
    private WaitableQueue GDS_to_kiosk_response;  // Queue for responses to Terminal. (Kiosk)

    private TCPClientSSL tcp_client;


    // Status
    private GDS_PROTOCOL_STATUS m_status = GDS_PROTOCOL_STATUS.UNKNOWN;
    private long m_status_tick = 0;
    private AutoResetEvent m_status_changed = new AutoResetEvent(true);

    private String m_terminal_name = "";
    private Int64 m_terminal_session_id = 0;
    private Int64 m_sequence_id = 0;
    private Int64 m_transaction_id = 0;

    private int m_keep_alive_interval = 30000;
    private int m_connection_timeout = 60000;

    //////private GDS_Parameters m_parameters;


    private DateTime m_last_send = DateTime.Now;
    private DateTime m_last_receive = DateTime.Now;

    private Thread m_GDS_main_thread;
    private Thread m_GDS_keep_alive_thread;

    //////////WSI.GDS.GDS_Message x;


    #endregion // Members

    #region Private Methods


    private void KeepAliveThread()
    {
      int wait_hint;

      wait_hint = 0;

      while (true)
      {
        System.Threading.Thread.Sleep(wait_hint);

        if (m_status != GDS_PROTOCOL_STATUS.CONNECTED)
        {
          wait_hint = 1000;

          continue;
        }

        wait_hint = Math.Min(m_keep_alive_interval, m_connection_timeout);
        wait_hint = wait_hint / 10;
        wait_hint = Math.Max(wait_hint, 1000);

        if (m_status == GDS_PROTOCOL_STATUS.CONNECTED)
        {
          DateTime now;
          TimeSpan elapsed;

          now = DateTime.Now;

          // Check Last Received Message
          elapsed = now.Subtract(m_last_receive);
          if (elapsed.TotalMilliseconds > m_connection_timeout)
          {
            SetStatus(GDS_PROTOCOL_STATUS.DISCONNECTED);

            continue;
          }

          // Check Last Send Message
          elapsed = now.Subtract(m_last_send);

          if (elapsed.TotalMilliseconds > m_keep_alive_interval)
          {
            GDS_Message GDS_request;
            String xml_request;
            Byte[] request;

            GDS_request = GDS_Message.CreateMessage(GDS_MsgTypes.GDS_MsgKeepAlive);
            GDS_request.MsgHeader.TerminalId = m_terminal_name;
            xml_request = GDS_request.ToXml();
            request = Encoding.UTF8.GetBytes(xml_request);

            tcp_client.BeginSend(request, new AsyncCallback(SendCallback), tcp_client);

            m_last_send = DateTime.Now;
          }
        }
      }
    }


    /// <summary>
    /// GDS_Protocol Constructor
    /// </summary>
    public GDS_Protocol_Class()
    {
      // Queues
      GDS_queue_send = new WaitableQueue();         // Send to WC2 Server
      GDS_queue_receive = new WaitableQueue();      // Received from WC2 Server
      GDS_to_kiosk_response = new WaitableQueue();  // Queue where kiosk remain blocked waiting for a response

      tcp_client = new TCPClientSSL(GDS_queue_receive);


      m_GDS_main_thread = new Thread(new ThreadStart(MainThread));
      m_GDS_main_thread.Name = "GDS Main Thread";
      m_GDS_main_thread.Start();

      m_GDS_keep_alive_thread = new Thread(new ThreadStart(KeepAliveThread));
      m_GDS_keep_alive_thread.Name = "GDS KeepAlive Thread";
      m_GDS_keep_alive_thread.Start();

    } // GDS_Protocol


    /// <summary>
    /// Manage kiosk requests and periodically messages requests.
    /// </summary>
    private void MainThread()
    {
      GDS_PROTOCOL_STATUS status;

      Log.Message("Main Thread Started");
      
      while (true)
      {
        status = WaitStatusChanged();

        switch (status)
        {
          case GDS_PROTOCOL_STATUS.UNKNOWN:
            SetStatus(GDS_PROTOCOL_STATUS.DISCONNECTED);
            break;

          case GDS_PROTOCOL_STATUS.DISCONNECTED:
            {
              Boolean disconnect;
              Boolean enrolled;
              Boolean msg_ok;

              disconnect = true;
              enrolled = false;
              msg_ok = false;

              while (!enrolled)
              {
                GDS_QueryService GDS_qs;
                IPEndPoint ipe_service;

                Thread.Sleep(1000);

                ipe_service = null;
                GDS_qs = null;

                try
                {
                  Log.Message("Query Service ...");
                  
                  // Ask for GDS_Service's IP endpoint to the GDS_DirectoryService 
                  GDS_qs = new GDS_QueryService();
                  ipe_service = GDS_qs.QueryService(1000);
                  if (ipe_service == null)
                  {
                    continue;
                  }

                  Log.Message("GDS Service found at: " + ipe_service.ToString());
                  
                  // Connect to the GDS_Service
                  tcp_client = new TCPClientSSL(GDS_queue_receive);
                  tcp_client.Connect(ipe_service);
                  if (!tcp_client.IsConnected)
                  {
                    tcp_client.Disconnect();
                    tcp_client = null;

                    continue;
                  }


                  Log.Message("Connected to:" + ipe_service.ToString());

                  msg_ok = false;


                  // Enroll
                  GDS_Message GDS_request;
                  GDS_Message GDS_response;
                  String xml_request;
                  Byte[] request;
                  IAsyncResult result;
                  GDS_MsgEnrollTerminal GDS_msg_request;

                  GDS_request = GDS_Message.CreateMessage(GDS_MsgTypes.GDS_MsgEnrollTerminal);

                  GDS_request.MsgHeader.TerminalId = m_terminal_name;
                  GDS_msg_request = (GDS_MsgEnrollTerminal)GDS_request.MsgContent;
                  GDS_msg_request.ProviderId = "WSI";

                  xml_request = GDS_request.ToXml();
                  request = Encoding.UTF8.GetBytes(xml_request);
                  result = tcp_client.BeginSend(request, new AsyncCallback(SendCallback), tcp_client);

                  result = tcp_client.BeginReceive(new AsyncCallback(ReceiveCallback), tcp_client);
                  if (result.AsyncWaitHandle.WaitOne(GDS_TIMEOUT, false))
                  {
                    // Response received
                    byte[] dgram;
                    string xml;

                    // Enrolled
                    dgram = tcp_client.EndReceive(result);
                    xml = Encoding.UTF8.GetString(dgram);
                    GDS_response = GDS_Message.CreateMessage(xml);
                    Log.Message("Enroll ResponseCode: " + GDS_response.MsgHeader.ResponseCode.ToString());
                    if (GDS_response.MsgHeader.ResponseCode == GDS_ResponseCodes.GDS_RC_OK)
                    {
                      GDS_MsgEnrollTerminalReply GDS_msg_reply;

                      GDS_msg_reply = (GDS_MsgEnrollTerminalReply)GDS_response.MsgContent;
                      msg_ok = true;

                      m_terminal_session_id = GDS_response.MsgHeader.TerminalSessionId;
                      m_sequence_id = 1 + GDS_msg_reply.LastSequenceId;
                      m_transaction_id = 1 + GDS_msg_reply.LastTransactionId;
                      
                      Log.Message("ENROLL, SESSION ID: " + m_terminal_session_id );
                    }
                  }
                  if (!msg_ok)
                  {
                    continue;
                  }

                  msg_ok = false; 
                  
                  // Get Parameters
                  GDS_request = GDS_Message.CreateMessage(GDS_MsgTypes.GDS_MsgGetParameters);
                  GDS_request.MsgHeader.TerminalId = m_terminal_name;
                  GDS_request.MsgHeader.TerminalSessionId = m_terminal_session_id;
                  this.GetSequenceId(out GDS_request.MsgHeader.SequenceId);

                  xml_request = GDS_request.ToXml();
                  request = Encoding.UTF8.GetBytes(xml_request);
                  result = tcp_client.BeginSend(request, new AsyncCallback(SendCallback), tcp_client);

                  result = tcp_client.BeginReceive(new AsyncCallback(ReceiveCallback), tcp_client);
                  if (result.AsyncWaitHandle.WaitOne(GDS_TIMEOUT, false))
                  {
                    // Response received
                    byte[] dgram;
                    string xml;

                    // Enrolled
                    dgram = tcp_client.EndReceive(result);
                    xml = Encoding.UTF8.GetString(dgram);
                    GDS_response = GDS_Message.CreateMessage(xml);
                    Log.Message("GetParameters ResponseCode: " + GDS_response.MsgHeader.ResponseCode.ToString());
                    if (GDS_response.MsgHeader.ResponseCode == GDS_ResponseCodes.GDS_RC_OK)
                    {
                      GDS_MsgGetParametersReply GDS_msg_reply;

                      GDS_msg_reply = (GDS_MsgGetParametersReply)GDS_response.MsgContent;
                      msg_ok = true;

                      //m_keep_alive_interval = 1000 * GDS_msg_reply.KeepAliveInterval;
                      //m_connection_timeout  = 1000 * GDS_msg_reply.ConnectionTimeout;
                    }
                    else
                    {
                      Log.Message("Session: " + m_terminal_session_id + " " + GDS_request.MsgHeader.TerminalSessionId + " " +  GDS_response.MsgHeader.TerminalSessionId);
                    }
                  }
                  if (!msg_ok)
                  {
                    continue;
                  }

                  // Start receiving data
                  tcp_client.BeginReceive(new AsyncCallback(EnqueueReceivedMessage), tcp_client);


                  enrolled = true;
                  disconnect = false;

                  SetStatus(GDS_PROTOCOL_STATUS.CONNECTED);
                }
                catch (Exception ex)
                {
                  Log.Exception(ex);
                }
                finally
                {
                  GDS_qs = null;

                  if (disconnect)
                  {
                    if (tcp_client != null)
                    {
                      tcp_client.Disconnect();
                    }
                  }
                }
              }
            }
            break;

          case GDS_PROTOCOL_STATUS.CONNECTED:
            // Do nothing
            break;
        };
      }
    } // MainThread


    #endregion

    #region Public Methods

    #endregion

    #region Public Messages

    /// <summary>
    /// Enroll Terminal: LKT Request.
    /// </summary>
    void GDS_IProtocol.Start([In, MarshalAs(UnmanagedType.BStr, SizeConst = 50)] String TerminalName)
    {
      m_terminal_name = TerminalName;

    } // StartProtocol


    /// <summary>
    /// 
    /// </summary>
    void GDS_IProtocol.Stop()
    {
      // Do nothing ...
      // Do nothing ...
      Log.Message("GDS STOP!!!");
      Log.Message("");
    } // Stop

    public void GetIsConnected(out bool IsConnected)
    {
      IsConnected = (m_status == GDS_PROTOCOL_STATUS.CONNECTED);
    }

    public void GetSequenceId([Out, MarshalAs(UnmanagedType.I8)] out Int64 SequenceId)
    {
      SequenceId = m_sequence_id++;

    } // GetSequenceId

    public void GetTransactionId([Out, MarshalAs(UnmanagedType.I8)] out Int64 TransactionId)
    {
      TransactionId = m_transaction_id++;

    } // GetTransactionId

    void GDS_IProtocol.GetReply_GetParameters(GDS_IMessage ReceivedMessage, out GDS_Parameters Parameters)
    {
      GDS_MessageEnvelope       msg;
      GDS_MsgGetParametersReply reply;
      GDS_Parameters            parameters;
      DateTime                  utc_time;
      
      parameters = new GDS_Parameters();

      msg = (GDS_MessageEnvelope)ReceivedMessage;
      reply = (GDS_MsgGetParametersReply)msg.Message.MsgContent;

      //parameters.connection_timeout  = 1000 * reply.ConnectionTimeout;
      //parameters.keep_alive_interval = 1000 * reply.KeepAliveInterval;

      // Get Local Time
      utc_time = msg.Message.MsgHeader.GetSystemTime;
      parameters.utc_datetime = utc_time.ToFileTime();
      parameters.time_zone = msg.Message.MsgHeader.GetTimeZone;
      
      Parameters = parameters;

    } // GetReply_EndSession
    



    private void SendCallback(IAsyncResult Result)
    {
      try
      {
        TCPClientSSL sender;

        m_last_send = DateTime.Now;

        sender = (TCPClientSSL)Result.AsyncState;
        sender.EndSend(Result);
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
    }

    private void ReceiveCallback(IAsyncResult Result)
    {
      m_last_receive = DateTime.Now;
    }

    private void EnqueueReceivedMessage(IAsyncResult Result)
    {
      try
      {
        TCPClientSSL sender;
        Byte[] raw;

        m_last_receive = DateTime.Now;

        sender = (TCPClientSSL)Result.AsyncState;
        raw = sender.EndReceive(Result);

        if (raw == null)
        {
          SetStatus(GDS_PROTOCOL_STATUS.DISCONNECTED);

          return;
        }

        if (raw.Length > 0)
        {
          String xml;
          GDS_Message GDS_response;

          xml = Encoding.UTF8.GetString(raw);
          GDS_response = GDS_Message.CreateMessage(xml);

          if (GDS_response.MsgHeader.ResponseCode == GDS_ResponseCodes.GDS_RC_TERMINAL_SESSION_NOT_VALID)
          {
            SetStatus(GDS_PROTOCOL_STATUS.DISCONNECTED);

            return;
          }

          if (GDS_response.MsgHeader.MsgType != GDS_MsgTypes.GDS_MsgKeepAliveReply)
          {
            GDS_to_kiosk_response.Enqueue(GDS_response);
          }

        }


        sender.BeginReceive(new AsyncCallback(EnqueueReceivedMessage), sender);

      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
    }


    public void Send(GDS_IMessage MessageToSend)
    {
      GDS_Message request;
      GDS_MessageEnvelope envelope;
      String xml;
      Byte[] raw;

      envelope = (GDS_MessageEnvelope)MessageToSend;
      request = envelope.Message;

      request.MsgHeader.TerminalId = m_terminal_name;
      request.MsgHeader.TerminalSessionId = m_terminal_session_id;

      xml = request.ToXml();
      raw = Encoding.UTF8.GetBytes(xml);

      Log.Message("" + request.MsgHeader.SequenceId.ToString("000000") + " " + request.MsgHeader.MsgType.ToString() + ": " + request.MsgHeader.ResponseCode.ToString());

      tcp_client.BeginSend(raw, new AsyncCallback(SendCallback), tcp_client);
    }

    /// <summary>
    /// Function called by Kiosk to obtain a response to a pending request.
    /// </summary>
    public void WaitForMessage(out GDS_IMessage ReceivedMessage)
    {
      GDS_Message GDS_message;

      ReceivedMessage = null;

      try
      {
        GDS_message = (GDS_Message)GDS_to_kiosk_response.Dequeue();

        Log.Message("    " + GDS_message.MsgHeader.SequenceId.ToString("000000") + " " + GDS_message.MsgHeader.MsgType.ToString() + ": " + GDS_message.MsgHeader.ResponseCode.ToString());

        ReceivedMessage = GDS_MessageEnvelope.CreateEnvelope(GDS_message);
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

    } // Response_GetMsgType

    private GDS_PROTOCOL_STATUS WaitStatusChanged()
    {
      m_status_changed.WaitOne();

      return m_status;
    }

    private GDS_PROTOCOL_STATUS SetStatus(GDS_PROTOCOL_STATUS NewStatus)
    {
      GDS_PROTOCOL_STATUS old_status;

      lock (this)
      {
        old_status = m_status;

        if (old_status != NewStatus)
        {
          m_status = NewStatus;
          m_status_tick = Environment.TickCount;
          m_status_changed.Set();

          if (NewStatus == GDS_PROTOCOL_STATUS.DISCONNECTED)
          {
            tcp_client.Disconnect();
          }
          Log.Message("GDS_PROTOCOL_STATUS changed: " + old_status.ToString() + " --> " + m_status.ToString());
        }
      }

      return old_status;
    }

    private GDS_PROTOCOL_STATUS Status
    {
      get
      {
        lock (this)
        {
          return m_status;
        }
      }
    }





    void GDS_IProtocol.NewRequest_GetParameters(out GDS_IMessage Request, long SequenceId)
    {
      GDS_Message          GDS_request;
      GDS_MsgGetParameters GDS_content;

      GDS_request = GDS_Message.CreateMessage(GDS_MsgTypes.GDS_MsgGetParameters);
      GDS_request.MsgHeader.SequenceId = SequenceId;
      GDS_content = (GDS_MsgGetParameters)GDS_request.MsgContent;

      Request = GDS_MessageEnvelope.CreateEnvelope(GDS_request);
    }
                                   
    #endregion // Public Messages

    #region GDS_IProtocol Members




    #endregion
  }

  public class GDS_MessageEnvelope : GDS_IMessage
  {
    GDS_Message m_message;

    #region GDS_IMessage Members

    void GDS_IMessage.GetSequenceId(out long SequenceId)
    {
      SequenceId = Message.MsgHeader.SequenceId;
    }

    void GDS_IMessage.GetMessageType(out GDS_Msg MessageType)
    {
      MessageType = (GDS_Msg) ((int) Message.MsgHeader.MsgType);
    }

    void GDS_IMessage.GetResponseCode(out GDS_RC ResponseCode)
    {
      ResponseCode = (GDS_RC)Message.MsgHeader.ResponseCode;
    }

    #endregion

    public static GDS_MessageEnvelope CreateEnvelope(GDS_Message Message)
    {
      GDS_MessageEnvelope msg;

      msg = new GDS_MessageEnvelope();

      msg.m_message = Message;

      return msg;
    }

    public GDS_Message Message
    {
      get { return m_message; }
    }
  }

}


