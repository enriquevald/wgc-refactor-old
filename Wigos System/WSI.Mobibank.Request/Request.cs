﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Request.cs
// 
//   DESCRIPTION: 
// 
//        AUTHOR: Diego Tenutto
// 
// CREATION DATE: 22-NOV-2017 
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 22-NOV-2017 DMT    First release
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading;
using System.Timers;
using WSI.Common;


namespace WSI.Mobibank.Request
{
  public class   RequestBuilder
  {
    static Thread syncThread = null;
    static System.Timers.Timer m_timer;
    static SimpleLogger _logger = new SimpleLogger("MOVIBANK");
    static int DEFAUL_TIME = 0;
    public static event EventHandler CompleteTaskEvent;

    public static void Init()
    {

      _logger.WriteLine("Init Movibank Requests -  Expiration verification process.");
      DEFAUL_TIME = GeneralParam.GetInt32("MobileBank", "ExpirationRequestTimeDefault");
      m_timer = new System.Timers.Timer();
      m_timer.Interval = 5000; // 5 segundos
      m_timer.Enabled = true;
      m_timer.Elapsed += new ElapsedEventHandler(timer_Elapsed);
      m_timer.Start();
      WSI.Mobibank.Request.RequestBuilder.CompleteTaskEvent += RequestBuilder_CompleteTaskEvent;

    }

    static void RequestBuilder_CompleteTaskEvent(object sender, EventArgs e)
    {
      m_timer.Start();
    }

    protected static void OnCompleteTaskEvent()
    {
      EventHandler handler = CompleteTaskEvent;
      if (handler != null)
      {
        handler(null, EventArgs.Empty);
      }
    }

    protected static void timer_Elapsed(object sender, ElapsedEventArgs e)
    {
      syncThread = new Thread(new ThreadStart(RunProcess));
      syncThread.Start();
    }

    private static void RunProcess()
    {

      //If you aren't active, do nothing.
      var _active = GeneralParam.GetInt32("Movibank", "Device", 0);
      if (_active == 0)
      {
        OnCompleteTaskEvent();
        return;
      }
        
      bool _result;
      DEFAUL_TIME = GeneralParam.GetInt32("MobileBank", "ExpirationRequestTimeDefault");

      _logger.WriteLine("Movibank Requests -  Expiration verification process running...");

      m_timer.Stop();

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (DEFAUL_TIME != 0) // If is 0 no expiration requests time
          {
            var _requests = GetRequests(_db_trx.SqlTransaction);

            if (_requests != null && _requests.Count > 0)
            {
              foreach (var item in _requests)
              {
                _result = UpdateRequestInStatusNew(item, _db_trx.SqlTransaction);

                if (!_result)
                {
                  _db_trx.SqlTransaction.Rollback();
                  _logger.WriteLine("Movibank Requests - Expiration verification process finished with error.");
                  return;
                }

              }

              _db_trx.SqlTransaction.Commit();

            }
          }
        }

        _logger.WriteLine("Movibank Requests - Expiration verification process finished.");

        OnCompleteTaskEvent();
      }
      catch (Exception _ex)
      {
        _logger.WriteLine("Movibank Requests - Expiration verification process finished with error. - " + _ex.Message);
        Log.Error("Movibank Requests - Expiration verification process finished with error.");
        Log.Exception(_ex);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get the requests
    // 
    //  PARAMS:
    //      - INPUT:
    //        - Trx
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //    - RequestsNews: List of request
    private static RequestsNews GetRequests(SqlTransaction Trx)
    {
      StringBuilder _sql_select;
      SqlCommand sql_command;
      SqlDataReader _sql_data_reader;
      RequestNew _request;
      RequestsNews _requests;


      _sql_select = new StringBuilder();
      _requests = new RequestsNews();

      try
      {
        _logger.WriteLine("Getting Mobibank Request - Start");

        _sql_select.Append("SELECT mbr_id");
        _sql_select.Append(",mbr_time_request");
        _sql_select.Append(",mbr_status_id");
        _sql_select.Append(",mbr_terminal_id");
        _sql_select.Append(",mbr_account_id");
        _sql_select.Append(" FROM mobibank_request ");
        _sql_select.Append(" WHERE ");
        _sql_select.Append(" mbr_status_id = @status ");

        sql_command = new SqlCommand(_sql_select.ToString());
        sql_command.Connection = Trx.Connection;
        sql_command.Transaction = Trx;

        sql_command.Parameters.Add("@status", SqlDbType.Int).Value = MobileBankStatusRequestRechare.New;

        _sql_data_reader = sql_command.ExecuteReader();

        if (_sql_data_reader.HasRows)
        {
          while (_sql_data_reader.Read())
          {
            _request = new RequestNew();
            _request.Id = _sql_data_reader.GetInt64(0);
            _request.TimeRequest = _sql_data_reader.GetDateTime(1);
            _request.Status = _sql_data_reader.GetInt32(2);
            _request.TerminalId = _sql_data_reader.GetInt32(3);
            _request.AccountId = _sql_data_reader.GetInt64(4);

            _requests.Add(_request);
          }
        }

        if (!_sql_data_reader.IsClosed)
          _sql_data_reader.Close();

        _logger.WriteLine("Getting Movibank Requests - Finished. Status: Success");

        return _requests;
      }
      catch (Exception _ex)
      {
        _logger.WriteLine("Movibank Requests. Exception in function: GetRequests");
        Log.Error("Movibank Requests. Exception in function: GetRequests");
        Log.Exception(_ex);
        return null;
      }
    }//GetRequests


    //------------------------------------------------------------------------------
    // PURPOSE: Update the request in if status is New
    // 
    //  PARAMS:
    //      - INPUT:
    //        - Request
    //        - Trx
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //    - true: if the oparations was success
    //    - false: if the oparations was error
    private static bool UpdateRequestInStatusNew(RequestNew request, SqlTransaction Trx)
    {
      StringBuilder _sql_insert;
      SqlCommand sql_command;

      _sql_insert = new StringBuilder();

      try
      {
        TimeSpan span = WSI.Common.WGDB.Now.Subtract(request.TimeRequest);

        if (span.TotalMinutes > DEFAUL_TIME)
        {
          _sql_insert.Append(" UPDATE mobibank_request  ");
          _sql_insert.Append(" SET  mbr_status_id = @status,"); //Status 3 = Void
          _sql_insert.Append("      mbr_elapsed_time_request = GETDATE(), ");
          _sql_insert.Append("      mbr_reason = @expired ");
          _sql_insert.Append(" WHERE mbr_id = @id  ");

          sql_command = new SqlCommand(_sql_insert.ToString());
          sql_command.Connection = Trx.Connection;
          sql_command.Transaction = Trx;

          sql_command.Parameters.Add("@id", SqlDbType.BigInt).Value = request.Id;
          sql_command.Parameters.Add("@status", SqlDbType.Int).Value = MobileBankStatusRequestRechare.Void;
          sql_command.Parameters.Add("@expired", SqlDbType.Int).Value = MobileBankStatusExpiredReason.Expired;

          sql_command.ExecuteNonQuery();

          WSI.Common.MobiBank.MobiBankRecharge.InsertWCPCommand_CancelRequest(request.TerminalId, request.AccountId, Trx);
        }

        return true;
      }
      catch (Exception _ex)
      {
        _logger.WriteLine("Movibank Requests. Exception in function: UpdateRequestInStatusNew - " + _ex.Message);
        Log.Error("Movibank Requests. Exception in function: UpdateRequestInStatusNew");
        Log.Exception(_ex);
        return false;
      }
    }//UpdateRequestInStatusNew


    public static void Stop()
    {
      _logger.WriteLine("Stopping Movibank Requests - Expiration verification process...");
      _logger.WriteLine("Movibank Requests - Expiration verification process stopped.");
    }

  }
}
