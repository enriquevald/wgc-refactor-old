﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: RequestNew.cs
// 
//   DESCRIPTION: 
// 
//        AUTHOR: Diego Tenutto
// 
// CREATION DATE: 22-NOV-2017 
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 22-NOV-2017 DMT    First release
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using WSI.Common;

namespace WSI.Mobibank.Request
{
  public class RequestNew
  {
    public RequestNew()
    {

    }

    public Int64 Id { get; set; }
    public Int32 EGMId { get; set; }
    public DateTime TimeRequest { get; set; }
    public Int32 Status { get; set; }
    public Int32 TerminalId { get; set; }
    public Int64 AccountId { get; set; }
  }

  public class RequestsNews : List<RequestNew>
  {

  }
}
