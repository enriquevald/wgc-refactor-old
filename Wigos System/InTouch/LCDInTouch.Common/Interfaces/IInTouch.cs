//------------------------------------------------------------------------------
// Copyright � 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: IInTouch.cs
// 
//   DESCRIPTION: InTouch Interface. 
// 
//        AUTHOR: Javi Barea
//  
// CREATION DATE: 20-JUN-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 20-JUN-2017 JBP    First release.
//------------------------------------------------------------------------------
using System;
using System.Runtime.InteropServices;

namespace LCDInTouch.Common
{
  #region Delegates

  public delegate void KeyPressed(int Key);
  public delegate void ProtocolRequest(int FunctionCode, [Out, MarshalAs(UnmanagedType.LPStr)] String ProtocolData);
  public delegate IntPtr FormatNumberToString(UInt64 Amount, Boolean ShowCurrencySymbol, Boolean ShowDecimals, Boolean AmountIsInCents);
  public delegate void ParameterizedThreadStart(Object obj);

  #endregion

  #region Interface IInTouch

  //------------------------------------------------------------------------------
  // PURPOSE: InTouch interface
  // 
  public interface IInTouch
  {
    void Init();
    bool MsgInterface(int Type, string Parameters);

    void SetKeyPressedHandler(IntPtr Handler);
    void ProtocolRequestHandler(IntPtr Handler);
    void ProtocolResponse(int FunctionCode, int Result, IntPtr Data);
    void FormatNumberToStringHandler(IntPtr Handler);

  } // IInTouch

  #endregion

}