//------------------------------------------------------------------------------
// Copyright � 2014 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Common.cs
// 
//   DESCRIPTION: Common clase. 
// 
//        AUTHOR: Javi Barea
// 
// CREATION DATE: 16-MAY-2014
// 
// REVISION HISTORY:
// 
// Date        Author     Description
// ----------- ------     ----------------------------------------------------------
// 16-MAY-2014 JBP        First release.
// 16-JUL-2014 JAB        Type converter implemented for promotions.
// 05-AGU-2015 YNM		    Added Account_min_allowed_cash_in_amount Message for TFS-3109
// 01-OCT-2015 FJC        Product BackLog Item 4700.
// 19-NOV-2015 FJC        Product Backlog 6117 (GamGateway)
// 12-JAN-2016 ETP        Fixed BUG - 7591 / 7592 Promotions and gift not showed. 
// 09-FEB-2016 FJC        Product Backlog Item 9105:BonoPlay: LCD: Cambios varios
// 22-FEB-2016 FJC        Product Backlog Item 9434:PimPamGo: Otorgar Premios
// 02-MAR-2016 FJC        PBI 9105:BonoPlay: LCD: Cambios varios
// 13-MAY-2016 JBP        PBI 12541:LCDTouch - Redimensi�n
// 15-FEB-2017 SCJA & FJC PBI 23849:Mensajer�a. Fase II
// 30-MAY-2017 FJC        PBI 27761:Sorteo de m�quina. Modificiones en la visualizaci�n de las pantallas (LCD y InTouch)). 
// 08-JUN-2017 FJC        WIGOS-2747 Terminal draw. Reduce waiting time before participate in terminal draw
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Resources;
using System.Globalization;
using System.Drawing.Imaging;

using System.IO;
using System.Net;
using System.Diagnostics;

namespace LCDInTouch.Common
{
  #region Clases

  #region Constants

  public class Constants
  {
    // FileName configs
    public const string FILE_NAME_CONFIG = "LCDTouch.Configuration.cfg";
    public const string FILE_NAME_LOCATION = "lcdlocation.txt";
    public const string FILE_NAME_RESOLUTION = "resolution.txt";
    public const string FILE_NAME_DEVELOPMENT = "development.txt";
    public const string FILE_NAME_MEMORY = "memory.txt";
    public const string FILE_NAME_MOUSE = "mouse.txt";
    public const string FILE_NAME_BORDERS = "border.txt";

    // Config properties 
    public const string CONFIG_NAME_LOCATION = "[Location]=";
    public const string CONFIG_NAME_RESOLUTION = "[Resolution]=";
    public const string CONFIG_NAME_DEVELOPMENT = "[Development]=";
    public const string CONFIG_NAME_MEMORY = "[Memory]=";
    public const string CONFIG_NAME_MOUSE = "[Mouse]=";
    public const string CONFIG_NAME_BORDERS = "[Border]=";

    // Development Log
    public const int DEVELOPMENT_LOG_PREFIX = 200;

    // Language
    public const string TYPE_LANGUAGE_ENGLISH = "en-US";
    public const string TYPE_LANGUAGE_SPANISH = "es-MX";

    // GameGateWay
    // language
    public const string TYPE_LANGUAGE_ENGLISH_GAME_GATEWAY = "en_US";
    public const string TYPE_LANGUAGE_SPANISH_GAME_GATEWAY = "es_ES";
    // browser
    public const string DEFAULT_BROWSER_URL = "http://WKS-55/LCDInTouch";
    public const string BLANK_PAGE = "about:blank";
    public const string TEST_PAGE = "http://google.es";

    // Message
    public const int WM_APP = 0x8000;
    public const int WM_MOUSECLICK = 0x210;
    public const int WM_XBUTTONDBLCLK = 0x020D;
    public const int WM_XBUTTONDOWN = 0x020B;
    public const int WM_XBUTTONUP = 0x020C;
    public const int WM_GRAPHNOTIFY = WM_APP + 1;

    // Message Params
    public const int WM_MOUSE_LEFT_CLICK = 0x201;
    public const int WM_MOUSE_RIGHT_CLICK = 0x204;
    public const int WM_MOUSE_MID_CLICK = 0x207;

    // Message Showing Times
    public const int DEFAULT_MINTIME_SHOWING_MESSAGE = 1000;
    //public const int LARGE_MINTIME_SHOWING_MESSAGE = 2 * 1000;
    public const int DEFAULT_MAXTIME_SHOWING_MESSAGE = 2750;
    public const int DEFAULT_MAXTIME_ERROR_SHOWING_MESSAGE = 3250;
    public const int INFINITE_SHOWING_MESSAGE = 0;
    public const int TIMEOUT_TO_CLOSE_MESSAGE = 2 * 60 * 1000;
    //public const int CLOSING_MESSAGE_MAXTIME = 200;
    public const int DEFAULT_CLOSING_MESSAGE = 1000;
    public const int FAST_CLOSING_MESSAGE = 250;

    // BaseThread Sleep Times
    public const int DEFAULT_BASETHREAD_WAIT_TIME = 1000;

    // Gift Lengths
    public const int GIFT_NAME_LENGTH = 50;
    public const int GIFT_DESC_LENGTH = 256;
    public const int MAX_GIFT = 90;
    public const int MAX_PATH = 260;

    // Params Lengths
    public const int MAX_PARAM_FUNC = 20;
    public const int MAX_PARAM_RESOURCES = 10;

    // Promos Lengths
    public const int MAX_PROMOS = 40;
    public const int PROMO_NAME_LENGTH = 50;
    public const int PROMO_DESC_LENGTH = 256;

    public const string RESOURCE_STRING_NOT_FOUND = "STRING_NOT_FOUND";

    // uc_message
    public struct UCMessageValues
    {
      public const Int32 WIDTH_PANEL = 595;
      public const Int32 WIDTH_FULLSCREEN_PANEL = 800;
      public const Int32 FULL_HEIGHT_PANEL = 272;
    }

    // uc_button    
    //public const Int32 BUTTON_HEIGHT_X_SMALL = 45;
    public const Int32 BUTTON_HEIGHT_SMALL = 46;
    public const Int32 BUTTON_HEIGHT_MEDIUM = 60;
    public const Int32 BUTTON_HEIGHT_BIG = 73;
    public const Int32 BUTTON_HEIGHT_PIN = 50;
    public const Int32 BUTTON_HEIGHT_TRANSFER_AMOUNT = 46;
    public const Int32 BUTTON_HEIGHT_KEYBOARD = 63;
    public const Int32 BUTTON_HEIGHT_OPERATOR_UP_DOWN = 46;
    public const Int32 BUTTON_HEIGHT_OPERATOR_CALIBRATION = 94;

    //public const Int32 BUTTON_WIDTH_X_SMALL = 140;
    public const Int32 BUTTON_WIDTH_SMALL = 140;
    public const Int32 BUTTON_WIDTH_MEDIUM = 280;
    public const Int32 BUTTON_WIDTH_BIG = 342;
    public const Int32 BUTTON_WIDTH_PIN = 61;
    public const Int32 BUTTON_WIDTH_TRANSFER_AMOUNT = 60;
    public const Int32 BUTTON_WIDTH_KEYBOARD = 63;
    public const Int32 BUTTON_WIDTH_OPERATOR_UP_DOWN = 78;
    public const Int32 BUTTON_WIDTH_OPERATOR_CALIBRATION = 60;

    //public const Int32 BUTTON_IMAGE_WIDTH_X_SMALL = 45;
    public const Int32 BUTTON_IMAGE_WIDTH_SMALL = 50;
    public const Int32 BUTTON_IMAGE_WIDTH_MEDIUM = 90;
    public const Int32 BUTTON_IMAGE_WIDTH_BIG = 100;
    public const Int32 BUTTON_IMAGE_WIDTH_NO_IMG = 0;

    // Interval Timers
    public const Int32 JACKPOT_TIME_SHOW = 1 * 45 * 1000;  // Jackpot simulator show
    public const Int32 JACKPOT_TIME_CLOSE = 2 * 60 * 1000;  // Jackpot close (Changed, set 2 min by AJQ)
    public const Int32 DEFAULT_LANGUAGE_TIMER = 1 * 60 * 1000;  // Set default language 
    public const Int32 DEFAULT_LANGUAGE_FORCED = 10 * 1000;  // Force default language 
    public const Int32 LOADING_TIME = 5 * 1000;  // loading panel

    // FJC 08-JUN-2017 
    // WIGOS-2747 Terminal draw. Reduce waiting time before participate in terminal draw
    // We increment Timer Tick because when we are running a terminal draw it goes slowly loading screen balls, so:
    // 1. Every timer tick (DEFAULT_CHECK_ACTIVITY ==> 500ms) we execute BrowserActivity for executing quickly
    // 2. Every timer tick and when we have 4 ticks ( DEFAULT_CHECK_ACTIVITY (500ms) * 4) ==> 2 seconds, 
    //    we execute rest of code from function MainTimer_Tick() as usual
    //public const Int32 DEFAULT_CHECK_ACTIVITY = 2 * 1000;             // checking activity
    public const Int32 DEFAULT_CHECK_ACTIVITY = 1 * 500;                // checking activity (ms)
    public const Int32 DEFAULT_CHECK_ACTIVITY_NUM_TICKS = 4;            // checking activity (count)
    public const Int32 DEFAULT_CHECK_ACTIVITY_AUTO_LOG_OUT = 2 * 1000;  // checking activity (ms)

    public const Int32 DEFAULT_LOGOUT_TIME = 5 * 60 * 1000;  // auto logout
    public const Int32 DEFAULT_SHOW_VIDEO_TIME = 5 * 60 * 1000;  // auto Show video
    public const Int32 DEFAULT_HIDE_VIDEO_TIME = 10 * 60 * 1000;  // auto Hide video
    public const Int32 LIST_LOGOUT_TIME = 4 * 60 * 1000;  // uc_list
    public const Int32 DETAIL_LOGOUT_TIME = 1 * 60 * 1000;  // item detail
    public const Int32 DEFAULT_LCD_BRINGTOFRONT = 1 * 30 * 1000;  // time to BringToFront 
    public const Int32 DEFAULT_ACCOUNT_INFO_TIME = 2 * 60 * 1000;  // time to Get Account Values  //OJO TODO FJC 10/02/2016
    public const Int32 DEFAULT_BROWSER_DELAY_TIME = 200;  // time to show browser
    public const Int32 DEFAULT_WAITING_MESSAGE_TIME = 1000;  // Waiting Message
    public const Int32 DEFAULT_ACCOUNT_INFO_SLEEP_TIME = 3 * 60 * 1000;  // time to Get Account Values 
    //public const Int32 DEFAULT_CHECK_ACTIVITY_GAMEGATEWAY = 1 * 30 * 1000;  // checking activity GameGateway
    //public const Int32 DEFAULT_CHECK_ACTIVITY_BROWSER = 1 * 10 * 1000;  // checking activity FrmBrowser Timer
    public const Int32 DEFAULT_CHECK_ACTIVITY_GAMEGATEWAY = 3 * 60 * 1000;  // checking activity GameGateway  (for showing interactive message) 
    public const Int32 DEFAULT_CHECK_ACTIVITY_BROWSER = 1 * 60 * 1000;  // checking activity FrmBrowser Timer (elapsed Timer) 
    public const Int32 DEFAULT_CHECK_ACTIVITY_INTERACTIVE_MESSAGE = 1 * 10 * 1000;
    public const Int32 DEFAULT_CHECK_ACTIVITY_INTERACTIVE_MESSAGE_AWARDED_PRIZE = 5 * 60 * 1000;  // checking activity FrmBrowser Timer


    //Internal Images && LCD_PARAMS
    public const string MESSAGE_INSERT_CARD = "MESSAGE_INSERT_CARD";
    public const string FILENAME_LOGO = "LogoCasino.png";
    public const string FILENAME_LOGO_INIT = "LogoCasinoInit.png";
    public const string FILENAME_VIDEO_INIT = "VideoInit.flv";
    public const string FILENAME_VIDEO = "Video";
    public const string FILENAME_VIDEO_NEXT = "_next";

    public const Int32 TIME_TO_ASK_PARAMS = 2 * 60 * 1000;  //Time between request params to protocol
    public const Int32 TIME_TO_ASK_CARD_STATUS = 1 * 60 * 1000;  //Time between request params to protocol
    public const Int32 TIME_TO_ASK_PARAMS_SLEEP = 30 * 1000;      //Time between request params to protocol
    public const Int32 TIME_TO_ASK_GAME_GATEWAY_AWARDED_PRIZE = 2 * 60 * 1000;  //Time between show granted prizes

    public const Int32 BUTTON_UP_LOCATION_X = 93;
    public const Int32 BUTTON_UP_LOCATION_Y = 20;
    public const Int32 BUTTON_CALIBRATION_LOCATION_X = 31;

    public const String ZERO_POINTS_CHAR = "";

    public const Double OPACITY_SHOW = 1;
    public const Double OPACITY_HIDE = 0;

    public const Int32 DEFAULT_INITIAL_POSITION_X_AND_Y = -100;
    public const Int32 DEFAULT_INITIAL_SIZE_W_AND_H = -100;

    internal struct ResourcesValues
    {
      public const Int32 LOGO_IMAGE = 0;      // Logo id in LCD_PARAMS
      public const Int32 VIDEO = 1;      // VIDEO in LCD_PARAMS
      public const Int32 DISABLED = -1;      // Resource disabled   
      public const Int32 NULL = 52428;   // Null   

      // Posiblemente se utilice provisionalmente
      internal struct PreloadResources
      {
        public const Int32 REDEEM_POINTS_ICON = 0;
        public const Int32 CREDIT_TRANSFER_ICON = 1;
        public const Int32 DRAW_LAYOUT_1 = 2;
        public const Int32 DRAW_LAYOUT_2 = 3;
        public const Int32 DRAW_LAYOUT_3 = 4;
        public const Int32 PIMPAMGO_POPUP = 5;
        public const Int32 LOYALTY_CLUB = 6;
        public const Int32 WEB_GLOBE = 7;
        public const Int32 ACCOUNT_INFO = 8;

        // IMPORTANT: Update total resources!!
        public const Int32 TOTAL_PRELOADED_RESOURCES = 9;
      }
    }

    internal enum FontSize
    {
      SIZE_08 = 8,
      SIZE_09 = 9,
      SIZE_10 = 10,
      SIZE_11 = 11,
      SIZE_12 = 12,
      SIZE_14 = 14,
      SIZE_16 = 16,
      SIZE_18 = 18,
      SIZE_20 = 20,
      SIZE_22 = 22,
      SIZE_24 = 24,
      SIZE_26 = 26,
      SIZE_28 = 28,
      SIZE_36 = 36,
      SIZE_48 = 48,
      SIZE_72 = 72,
      SIZE_87 = 87,
      SIZE_100 = 100,
      SIZE_115 = 115,
      SIZE_130 = 130
    }

    // uc_round_button 
    internal struct RoundButton
    {
      internal struct ButtonEvent
      {
        public const Int32 NORMAL = 0;
        public const Int32 MOUSEDOWN = 1;
        public const Int32 DISABLED = 2;
      }

      // Height
      internal struct HeightValues
      {
        public const Int32 SMALL = 41;
        public const Int32 MEDIUM = 63;
        public const Int32 BIG = 72;
      }

      // Width
      internal struct WidthValues
      {
        public const Int32 SMALL = 120;
        public const Int32 MEDIUM = 176;
        public const Int32 BIG = 219;
      }

      // IconWidth
      internal struct IconWidthValues
      {
        public const Int32 SMALL = 0; // No Image
        public const Int32 MEDIUM = 50;
        public const Int32 BIG = 50;
      }

      // IconMargin
      internal struct IconMarginValues
      {
        public const Int32 SMALL = 0; // No Image
        public const Int32 MEDIUM = 15;
        public const Int32 BIG = 20;
      }
    }

    public struct ScaleValues
    {
      public const Int32 DEFAULT_WIDTH = 800;
      public const Int32 DEFAULT_HEIGHT = 296; // 256 + (20 * 2) (256 = pnl_content) and  (20 * 2 = margin up & down)
    }
  }

  #endregion

  #region Functions

  #endregion

  #endregion

  #region Structs

  public struct LCD_RESOURCE
  {
    public Boolean PendingDownload;
    public Boolean Downloading;
    public LCD_PARAM_RESOURCES Resource;
    public String CurrentVideo;
    public String NextVideo;
    public String Extension;
    public Boolean Exists;
    public Boolean IsDefault;
  }

  public struct LCD_ITEM
  {
    // Commons
    public UInt64 Id;
    public Image Image;
    public String Name;
    public String Description;
    public TYPE_ITEM Type;

    // Gifts
    public Int64 Amount;
    public UInt64 Credits;
    public TYPE_GIFT GiftCategory;

    // Promotions [LCD_PROMO_ITEM]
    public TYPE_PROMOTION_TYPE PromotionCategory;

    // Promotions [LCD_PROMO_ITEM_APPLICABLE]
    public TYPE_CREDIT promo_credit_type;
    public Double min_spent;
    public Double min_spent_reward;
    public Double spent;
    public Double spent_reward;
    public Double current_reward;
    public Double recharges_reward;
    public Double available_limit;
  }

  //Marshaling with C#
  //More info:
  //http://www.c-sharpcorner.com/uploadfile/GemingLeader/marshaling-with-C-Sharp-chapter-2-marshaling-simple-types/
  //http://manski.net/2012/06/pinvoke-tutorial-passing-parameters-part-3/

  // GIFTS --------------------------------------------------------
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public struct LCD_GIFT_ITEM
  {
    // NOT ALTER ORDER OF FIELDS //
    [MarshalAs(UnmanagedType.U8)]
    public UInt64 Id;
    [MarshalAs(UnmanagedType.I4)]
    public TYPE_GIFT type;
    [MarshalAs(UnmanagedType.ByValTStr, SizeConst = Constants.GIFT_NAME_LENGTH)]
    public String name;
    [MarshalAs(UnmanagedType.ByValTStr, SizeConst = Constants.GIFT_DESC_LENGTH)]
    public String desc;
    [MarshalAs(UnmanagedType.U8)]
    public UInt64 points;
    [MarshalAs(UnmanagedType.U8)]
    public UInt64 num_credits;
    [MarshalAs(UnmanagedType.U2)]
    public ushort stock;
    [MarshalAs(UnmanagedType.U2)]
    public ushort maxium_units;
    [MarshalAs(UnmanagedType.ByValTStr, SizeConst = Constants.MAX_PATH + 4)]
    public String small_resource;
    [MarshalAs(UnmanagedType.ByValTStr, SizeConst = Constants.MAX_PATH + 4)]
    public String large_resource;
  }//TYPE_COMMON_GIFT_DATA

  [StructLayout(LayoutKind.Sequential)]
  public struct LCD_PLAYER_GIFTS
  {
    // NOT ALTER ORDER OF FIELDS //
    [MarshalAs(UnmanagedType.U2)]
    public ushort num_current;
    [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.Struct, SizeConst = Constants.MAX_GIFT)]
    public LCD_GIFT_ITEM[] current;

    [MarshalAs(UnmanagedType.U2)]
    public ushort num_next;
    [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.Struct, SizeConst = Constants.MAX_GIFT)]
    public LCD_GIFT_ITEM[] next;
  }

  // PARAMETERS ----------------------------------------------------
  [StructLayout(LayoutKind.Sequential)]
  public struct LCD_PARAMS_GAME_GATEWAY_PARAMS
  {
    [MarshalAs(UnmanagedType.U4)]
    public UInt32 GameGateway;
    [MarshalAs(UnmanagedType.U4)]
    public UInt32 TerminalId;
    [MarshalAs(UnmanagedType.U4)]
    public UInt32 ReservedCredit;
    [MarshalAs(UnmanagedType.U2)]
    public UInt16 AwardPrizes;
    [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 240)]
    public String Url;
    [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 240)]
    public String UrlTest;
    [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 240)]
    public String ProviderName;
    [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 120)]
    public String PartnerId;
  } // TYPE_COMMON_LCD_GAME_GATEWAY_PARAMS

  [StructLayout(LayoutKind.Sequential)]
  public struct LCD_GAME_GATEWAY_SESSION
  {
    [MarshalAs(UnmanagedType.U8)]
    public UInt64 PlaySession_Id;
    [MarshalAs(UnmanagedType.U8)]
    public UInt64 Account_Id;
    [MarshalAs(UnmanagedType.U2)]
    public UInt16 PopupMsgType;
    [MarshalAs(UnmanagedType.U2)]
    public UInt16 ReservedMode;
    [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 240)]
    public String PopupMsgText;

  } // TYPE_COMMON_LCD_GAME_GATEWAY_SESSION

  [StructLayout(LayoutKind.Sequential)]
  public struct LCD_PARAMS_FB_PARAMS
  {
    [MarshalAs(UnmanagedType.U4)]
    public UInt32 Enabled;
    [MarshalAs(UnmanagedType.U4)]
    public UInt32 PinRequest;
    [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 240)]
    public String HostAddress;
    [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 240)]
    public String Url;
    [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 120)]
    public String Login;
    [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 120)]
    public String Password;
  } // LCD_PARAMS_FB_PARAMS

  [StructLayout(LayoutKind.Sequential, Pack = 2)]
  public struct LCD_PARAM_RESOURCES
  {
    [MarshalAs(UnmanagedType.U4)]
    public uint id;

    [MarshalAs(UnmanagedType.ByValTStr, SizeConst = Constants.MAX_PATH + 4)]
    public string path;
  }

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public struct LCD_PARAMS_CONFIGURATION
  {
    // NOT ALTER ORDER OF FIELDS //
    [MarshalAs(UnmanagedType.U2)]
    public ushort num_functionalities;
    [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.U2, SizeConst = Constants.MAX_PARAM_FUNC)]
    public ushort[] functionalities;
    [MarshalAs(UnmanagedType.U2)]
    public ushort num_resource;
    [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.Struct, SizeConst = Constants.MAX_PARAM_RESOURCES)]
    public LCD_PARAM_RESOURCES[] resource;
  }

  [StructLayout(LayoutKind.Sequential)]
  public struct LCD_PARAMS
  {
    // DO NOT ALTER THE ORDER OF THE FIELDS //
    [MarshalAs(UnmanagedType.U2)]
    public ushort SubsystemMode;
    [MarshalAs(UnmanagedType.U2)]
    public ushort LanguageId;
    [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 23 + 1)]
    public String TerminalName;

    [MarshalAs(UnmanagedType.Struct, ArraySubType = UnmanagedType.Struct)]
    public LCD_PARAMS_CONFIGURATION Parameters;

    [MarshalAs(UnmanagedType.Struct, ArraySubType = UnmanagedType.Struct)]
    public LCD_PARAMS_GAME_GATEWAY_PARAMS GameGateWayParameters;

    [MarshalAs(UnmanagedType.Struct, ArraySubType = UnmanagedType.Struct)]
    public LCD_PARAMS_FB_PARAMS FBParameters;

    [MarshalAs(UnmanagedType.Struct, ArraySubType = UnmanagedType.Struct)]
    public LCD_PARAMS_TERMINAL_DRAW_PARAMS TerminalDrawParameters;
  } // LCD_PARAMS


  // PROMOS --------------------------------------------------------
  [StructLayout(LayoutKind.Sequential)]
  public struct LCD_PLAYER_PROMOTIONS
  {
    // NOT ALTER ORDER OF FIELDS //
    [MarshalAs(UnmanagedType.U2)]
    public ushort num_current;
    [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.Struct, SizeConst = Constants.MAX_PROMOS)]
    public LCD_PROMO_ITEM[] current;

    [MarshalAs(UnmanagedType.U2)]
    public ushort num_next;
    [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.Struct, SizeConst = Constants.MAX_PROMOS)]
    public LCD_PROMO_ITEM[] next;

    [MarshalAs(UnmanagedType.U2)]
    public ushort num_future;
    [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.Struct, SizeConst = Constants.MAX_PROMOS)]
    public LCD_PROMO_ITEM[] future;

    [MarshalAs(UnmanagedType.U2)]
    public ushort num_applicable;
    [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.Struct, SizeConst = Constants.MAX_PROMOS)]
    public LCD_PROMO_ITEM_APPLICABLE[] applicable;
  }

  [StructLayout(LayoutKind.Sequential, Pack = 2)]
  public struct LCD_PROMO_ITEM_APPLICABLE
  {
    [MarshalAs(UnmanagedType.Struct)]
    public LCD_PROMO_ITEM promo_data;
    [MarshalAs(UnmanagedType.I4)]
    public TYPE_CREDIT promo_credit_type;
    [MarshalAs(UnmanagedType.R8)]
    public double min_spent;
    [MarshalAs(UnmanagedType.R8)]
    public double min_spent_reward;
    [MarshalAs(UnmanagedType.R8)]
    public double spent;
    [MarshalAs(UnmanagedType.R8)]
    public double spent_reward;
    [MarshalAs(UnmanagedType.R8)]
    public double current_reward;
    [MarshalAs(UnmanagedType.R8)]
    public double recharges_reward;
    [MarshalAs(UnmanagedType.R8)]
    public double available_limit;
  }

  [StructLayout(LayoutKind.Sequential, Pack = 2)]
  public struct LCD_PROMO_ITEM
  {
    // NOT ALTER ORDER OF FIELDS //
    [MarshalAs(UnmanagedType.U4)]
    public uint Id;
    [MarshalAs(UnmanagedType.ByValTStr, SizeConst = Constants.PROMO_NAME_LENGTH)]
    public String name;
    [MarshalAs(UnmanagedType.ByValTStr, SizeConst = Constants.PROMO_DESC_LENGTH)]
    public String description;
    [MarshalAs(UnmanagedType.ByValTStr, SizeConst = Constants.MAX_PATH + 4)]
    public String large_resource;
    [MarshalAs(UnmanagedType.ByValTStr, SizeConst = Constants.MAX_PATH + 4)]
    public String small_resource;
  }

  [StructLayout(LayoutKind.Sequential)]
  public struct LCD_PLAYER_REQUEST_BALANCE
  {
    // NOT ALTER ORDER OF FIELDS //
    [MarshalAs(UnmanagedType.Struct, ArraySubType = UnmanagedType.Struct)]
    public LCD_BALANCE_PARTS balance;
    [MarshalAs(UnmanagedType.U8)]
    public UInt64 total_points;
    [MarshalAs(UnmanagedType.U8)]
    public UInt64 error_printer;
    [MarshalAs(UnmanagedType.U8)]
    public UInt64 game_gateway_awarded_prize_amount;
    [MarshalAs(UnmanagedType.Struct, ArraySubType = UnmanagedType.Struct)]
    public LCD_GAME_GATEWAY_SESSION game_gateway_session;

  } // TYPE_COMMON_PROMOTION_BALANCE

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public struct LCD_BALANCE_PARTS
  {
    [MarshalAs(UnmanagedType.U8)]
    public UInt64 total_cents;

    [MarshalAs(UnmanagedType.U8)]
    public UInt64 redeemable_cents;
    [MarshalAs(UnmanagedType.U8)]
    public UInt64 promo_redeemable_cents;
    [MarshalAs(UnmanagedType.U8)]
    public UInt64 promo_not_redeemable_cents;
    [MarshalAs(UnmanagedType.U8)]
    public UInt64 redeemable_reserved_cents;
    [MarshalAs(UnmanagedType.U2)]
    public UInt16 reserved_mode;

  } // TYPE_COMMON_BALANCE_PARTS;


  // TERMINAL DRAW --------------------------------------------------------
  [StructLayout(LayoutKind.Sequential)]
  public struct LCD_TERMINAL_DRAW_DRAW_PARAMS
  {
    [MarshalAs(UnmanagedType.U8)]
    public UInt64 game_id;
    [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 240)]
    public String terminal_game_url;
    [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 240)]
    public String terminal_game_name;
    [MarshalAs(UnmanagedType.U8)]
    public UInt64 is_winner;
    [MarshalAs(UnmanagedType.U8)]
    public UInt64 amount_re_played_draw;
    [MarshalAs(UnmanagedType.U8)]
    public UInt64 amount_re_played_won;
    [MarshalAs(UnmanagedType.U8)]
    public UInt64 amount_nr_played_draw;
    [MarshalAs(UnmanagedType.U8)]
    public UInt64 amount_nr_played_won;
    [MarshalAs(UnmanagedType.U8)]
    public UInt64 amount_points_played_draw;
    [MarshalAs(UnmanagedType.U8)]
    public UInt64 amount_points_played_won;
    [MarshalAs(UnmanagedType.U8)]
    public UInt64 amount_total_balance;
    [MarshalAs(UnmanagedType.U8)]
    public UInt64 play_session_id;
    [MarshalAs(UnmanagedType.U8)]
    public UInt64 amount_total_bet;

    public override string ToString()
    {
      return string.Format("{0}|{1}|{2}|{3}|{4}|{5}|{6}|{7}|{8}|{9}",
        terminal_game_url,                              //m_url = _params[0];
        (amount_re_played_draw + amount_nr_played_draw),//long.TryParse(_params[1], NumberStyles.Integer, CultureInfo.InvariantCulture, out m_bet_amount);
        is_winner,                                      //ToBool(_params[2], out m_is_winner);
        (amount_re_played_won + amount_nr_played_won),  //long.TryParse(_params[3], NumberStyles.Integer, CultureInfo.InvariantCulture, out m_win_amount);
        amount_total_balance,                           //long.TryParse(_params[4], NumberStyles.Integer, CultureInfo.InvariantCulture, out m_balance);
        0,                                              //if (long.TryParse(_params[5], NumberStyles.Integer, CultureInfo.InvariantCulture, out m_timeout))
        0,                                              //ToBool(_params[6], out m_is_forced);
        0,                                              //ToBool(_params[7], out m_run_draw);
        0,                                              //ToBool(_params[8], out m_show_result);
        0);                                             //ToBool(_params[9], out m_auto_draw);
    }
  } // TYPE_COMMON_TERMINAL_DRAW_DETAILS

  [StructLayout(LayoutKind.Sequential)]
  public struct LCD_PARAMS_TERMINAL_DRAW_PARAMS
  {
    [MarshalAs(UnmanagedType.U8)]
    public UInt64 Enabled;
  } // TYPE_COMMON_TERMINAL_DRAW_PARAMS

  [StructLayout(LayoutKind.Sequential)]
  public struct LCD_TERMINAL_DRAW_PARAMS_PARTICIPATE
  {
    [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 240)]
    public String terminal_game_url;

    [MarshalAs(UnmanagedType.U8)]
    public UInt64 terminal_game_time_out;

    [MarshalAs(UnmanagedType.U8)]
    public UInt64 forced;

    [MarshalAs(UnmanagedType.U8)]
    public UInt64 auto_draw;

    [MarshalAs(UnmanagedType.U8)]
    public UInt64 amount_total_balance;

    [MarshalAs(UnmanagedType.U8)]
    public UInt64 amount_total_bet;

    public override string ToString()
    {
      return string.Format("{0}|{1}|{2}|{3}|{4}|{5}|{6}|{7}|{8}|{9}",
        terminal_game_url,     //m_url = _params[0];
        amount_total_bet,      //long.TryParse(_params[1], NumberStyles.Integer, CultureInfo.InvariantCulture, out m_bet_amount);
        0,                     //ToBool(_params[2], out m_is_winner);
        0,                     //long.TryParse(_params[3], NumberStyles.Integer, CultureInfo.InvariantCulture, out m_win_amount);
        amount_total_balance,  //long.TryParse(_params[4], NumberStyles.Integer, CultureInfo.InvariantCulture, out m_balance);
        terminal_game_time_out,//if (long.TryParse(_params[5], NumberStyles.Integer, CultureInfo.InvariantCulture, out m_timeout))
        forced,                //ToBool(_params[6], out m_is_forced);
        0,                     //ToBool(_params[7], out m_run_draw);
        0,                     //ToBool(_params[8], out m_show_result);
        auto_draw);            //ToBool(_params[9], out m_auto_draw);
    }
  } // TYPE_COMMON_TERMINAL_DRAW_GET_PENDING_LCD                                                 



  //JCA For Start Process
  public struct PROCESS_INFORMATION
  {
    public IntPtr hProcess;
    public IntPtr hThread;
    public uint dwProcessId;
    public uint dwThreadId;
  }

  public struct STARTUPINFO
  {
    public uint cb;
    public string lpReserved;
    public string lpDesktop;
    public string lpTitle;
    public uint dwX;
    public uint dwY;
    public uint dwXSize;
    public uint dwYSize;
    public uint dwXCountChars;
    public uint dwYCountChars;
    public uint dwFillAttribute;
    public uint dwFlags;
    public short wShowWindow;
    public short cbReserved2;
    public IntPtr lpReserved2;
    public IntPtr hStdInput;
    public IntPtr hStdOutput;
    public IntPtr hStdError;
  }

  public struct SECURITY_ATTRIBUTES
  {
    public int length;
    public IntPtr lpSecurityDescriptor;
    public bool bInheritHandle;
  }
  //JCA For Start Process

  #endregion

  #region Enums

  public enum TYPE_BODY
  {
    CONFIGURATION = -5,
    KEYBOARD = -4,
    LOG_VIEWER = -3,
    STATUS = -2,
    OPERATOR = -1,
    VOID = 0,
    MAIN = 1,
    LOGIN = 2,
    ACCOUNT_MENU = 3,
    ACCOUNT_INFO = 4,
    RAFFLES = 5,
    JACKPOTS = 6,
    PROMOTION_MENU = 7,
    PROMOTIONS_LIST = 8,
    PROMOTION_DETAIL = 9,
    GIFT_MENU = 10,
    GIFTS_LIST = 11,
    GIFT_DETAIL = 12,
    //REDEEM = 13,
    TRANSFERRING = 14,
    VIDEO = 15,
    SHUTDOWN = 16,
    //LOADING = 17,
    CREDIT_TRANSFER = 18,
    LOTTO_RACE = 19,
    INTERACTIVE_MESSAGE = 20,
    RESERVED_MENU = 21,
    RESERVED_TRANSFER = 22,
    BROWSER = 23,
  }

  public enum TYPE_LOADING
  {
    INIT = 0,
    SHUTDOWN = 1,
    VIDEO = 2    
  }

  public enum TYPE_EVENT
  {
    EXTRACT_CARD = 0,
    INSERT_CARD = 1,
    OPERATOR_MESSAGE = 2,
    //JACKPOT_INTERVAL = 3,
    CASH_OUT_EVENT = 4,
    SHOW_MESSAGE = 5,
    OPERATOR_PANEL = 6,
    SEND_RESPONSE = 7,
    FORM_READY = 8,
    FORM_LOGIN = 9,
    BLOCK_REASON = 10,
    SHOW_JACKPOT = 11,

    SHOW_UC_ACCOUNT_INFO = 5001,
    SHOW_UC_ACCOUNT_GIFTS = 5002,
    SHOW_UC_ACCOUNT_SESSION_POINTS = 5003,
    SHOW_UC_ACCOUNT_PROMOS = 5004,

    GIFT_REQUEST = 7001,
    TRANSFER_CARD_CREDIT_TO_MACHINE = 7002,
    PROMOTION_REQUEST = 7003,
    GAME_GATEWAY_RESERVE_CREDIT = 7004,
    TERMINAL_DRAW_ASK_FOR_PARTICIPATE = 7005,
    TERMINAL_DRAW_PARTICIPATE = 7006,
    TERMINAL_DRAW_GAME_EXECUTED = 7007
  }

  public enum TYPE_MESSAGE_CODE
  {
    CLEAR = 0,
    INSERT_CARD = 1,
    MESSAGE_INFO = 2,
    MESSAGE_ERROR = 3
  }

  public enum TYPE_ACCOUNT_RESPONSE
  {
    INFO = 0,
    GIFTS = 1,
    PROMOTIONS = 2,
    PROMOTIONS_REQUEST = 3,
    DRAWS = 4,
    SESSION_POINTS = 5
  }

  public enum TYPE_GIFT_TYPE
  {
    AVAILABLE = 0,
    NEXTS = 1,
    CREDITS = 2
  }

  public enum TYPE_PROMOTION_TYPE
  {
    APPLICABLES = 0,
    CURRENTS = 1,
    NEXTS = 2,
    FUTURES = 3
  }

  public enum TYPE_GIFT : int        // Must match enum ENUM_GIFT_TYPE from LKT CommonConstants.h
  {
    OBJECT = 0,
    NOT_REDEEMABLE_CREDITS = 1,
    DRAW_NUMBERS = 2,
    SERVICES = 3,
    REDEEMABLE_CREDITS = 4,
    UNKNOWN = 999
  }

  public enum TYPE_CREDIT : int      // Must match enum ACCOUNT_PROMO_CREDIT_TYPE from WSI.Common.SharedDefinitions.cs  
  {
    PROMO_CREDIT_TYPE_UNKNOWN = 0,
    PROMO_CREDIT_TYPE_NR1 = 1,        // Non-Redeemable - Spent at the end (with withhold)
    PROMO_CREDIT_TYPE_NR2 = 2,        // Non-Redeemable - Spent at beggining
    PROMO_CREDIT_TYPE_REDEEMABLE = 3,
    PROMO_CREDIT_TYPE_POINT = 4,
    PROMO_CREDIT_TYPE_UNR1 = 5,       // Spent at the end
    PROMO_CREDIT_TYPE_UNR2 = 6,       // Spent at beggining
    PROMO_CREDIT_TYPE_NR3 = 7         // Non-Redeemable - Spent at the end (without withhold)
  }

  //public enum TYPE_REDEEM
  //{
  //  PROMOTION = 0,
  //  GIFT = 1
  //}

  public enum TYPE_ITEM
  {
    GIFT = 0,
    DRAW = 1,
    PROMOTION = 2
  }

  public enum TYPE_STATUS
  {
    OK = 0,
    ERR = 1,
    UNK = 2,
    NOTINSTALLED = 3,
    JAM = 4,
    SN = 5,
    OPEN = 6,
    FULL = 7
  }

  //public enum TYPE_MODE
  //{
  //  TITO = 0,
  //  CASHLESS = 1
  //}

  public enum TYPE_MESSAGE_COLOR
  {
    WHITE = 0,
    YELLOW = 1,
    GREEN = 2,
    BLUE = 3
  }

  public enum TYPE_JACKPOT
  {
    BRONZE = 0,
    SILVER = 1,
    GOLD = 2,
  }

  public enum TYPE_MESSAGE_STYLE
  {
    NORMAL = 0,
    WAITING = 1
  }

  public enum TYPE_MESSAGE
  {
    ERROR = 0,
    WARNING = 1,
    INFO = 2,
    SUCCESS = 3,
    WELCOME = 4,
    FAREWELL = 5,
    INSERT_CARD = 6,
    NOTIFY = 7,
    VOID = 8,

    MEMORY = 99,
    CLOSE = 100,

    DEVELOPMENT_LVL1 = Constants.DEVELOPMENT_LOG_PREFIX + 1,
    DEVELOPMENT_LVL2 = Constants.DEVELOPMENT_LOG_PREFIX + 2,
    DEVELOPMENT_LVL3 = Constants.DEVELOPMENT_LOG_PREFIX + 3,

    DEFAULT = 999,

    NONE = -1
  }

  public enum MESSAGE_SOURCE
  {
    NONE = 0,
    LCD = 1,
    SAS_HOST = 2
  }

  public enum TYPE_MESSAGE_PRIORITY
  {
    DEFINITIVE = 0,
    NONE = 1
  }

  public enum TYPE_SIMULATOR_MEMBER
  {
    GET_REQUEST = 0,
    REUQEST_TIMEOUT = 1
  }

  public enum TYPE_RESPONSE
  {
    OK = 0,
    KO = 1,
    TIMEOUT = 2
  }

  // Defined in UpperDisplay_model_005Internals
  // UPPER_DISPLAY_XXXX_XXX
  public enum TYPE_REQUEST
  {
    CARD_STATUS = 0,
    HOLDER_NAME = 1,
    CARD_TYPE = 2,
    PIN = 3,
    GIFT = 4,
    DRAW = 5,  //TODO REVISAR DONDE SE USA
    PROMOTION = 6,
    ACCOUNT_INFO = 7,
    ESTIMATED_POINTS = 8,
    PROMOTION_REQUEST = 9,
    BLOCK_REASON = 10,
    FT_ENTER = 11,
    CARD_MESSAGE = 12,
    PARAMS = 13,
    LCD_ACTIVITY = 14,
    MOBILE_BANK = 15,
    SHUTDOWN = 16,
    TECH_MENU = 17,
    CHANGE_STACKER = 18,
    DISPLAY_BONUS_AWARDED = 19,
    GIFT_REQUEST = 20,
    PLAYER_LANGUAGE_ID = 21,
    TRANSFER_CARD_CREDIT_TO_MACHINE = 22,
    TERMINAL_ACTIVE = 23,
    MESSAGE_CODE = 24,
    GET_PROMO_BALANCE = 25,
    TRANSFER_PARTIAL_CARD_CREDIT_TO_MACHINE = 26,
    GAME_GATEWAY = 27,
    GAME_GATEWAY_RESERVE_CREDIT = 28,
    GAME_GATEWAY_PROCESS_MSG = 29,
    TERMINAL_DRAW_ASK_FOR_PARTICIPATE = 30,
    TERMINAL_DRAW_PARTICIPATE = 31,
    TERMINAL_DRAW_GAME_RESULT = 32,
    INSERT_CARD = 33,

    UNKNOWN = 999
  }

  public enum TYPE_SCREEN_STATUS
  {
    UNKNOWN = 0,
    ENTER = 1,
    EXIT = 2
  }

  public enum TYPE_CARD_STATUS
  {
    CARD_REMOVED = 0,
    CARD_INSERTED = 1,
    CARD_INSERTED_VALIDATED = 2,
    CARD_INSERTED_NOT_VALIDATED = 3,
    CARD_REMOVED_VALIDATED = 4,
    CARD_INFORMATION = 998,
    CARD_ERROR = 999
  }

  public enum TYPE_CARD
  {
    UNKNOWN = 0,
    PLAYER = 1,
    MOBILE_BANK = 2,
    PLAYER_PIN = 3,
    TECH = 10,
    TECH_PIN = 11,
    CHANGE_STACKER = 12,
    CHANGE_STACKER_PIN = 13
  }//TYPE_CARD
  //If add TYPE_CARD, revise Functions.TypeCardMode(.....)

  public enum KB_TYPE
  {
    EXIT,
    MOBILE_BANK,
    TECH,
    CONFIGURATION,
    COMPLETE
  }

  //Defined in Protocol_WCP_API.h
  //SAME as #define PROTOCOL_xxxx_xxxx
  public enum TYPE_PROTOCOL_RESPONSE
  {
    //This enum is equal in Protocol_WCP_API.h
    WCP_STATUS_OK = 1,
    WCP_STATUS_ERROR = 2,
    WCP_STATUS_NOT_AVAILABLE = 3,
    WCP_STATUS_COMM_ERROR = 4,
    WCP_STATUS_CARD_NOT_VALID = 5,
    WCP_STATUS_SERVER_NOT_AUTHORIZED = 6,
    WCP_STATUS_SERVER_SESSION_NOT_VALID = 7,
    WCP_STATUS_TERMINAL_NOT_AUTHORIZED = 8,
    WCP_STATUS_TERMINAL_SESSION_NOT_VALID = 9,
    WCP_STATUS_CARD_NOT_VALIDATED = 10,
    WCP_STATUS_CARD_BLOCKED = 11,
    WCP_STATUS_CARD_EXPIRED = 12,
    WCP_STATUS_CARD_ALREADY_IN_USE = 13,
    WCP_STATUS_DATABASE_OFFLINE = 14,
    WCP_STATUS_CARD_BALANCE_MISMATCH = 15,
    WCP_STATUS_NOT_ENOUGH_PLAYERS = 16,
    WCP_STATUS_CARD_WRONG_PIN = 17,
    WCP_STATUS_INVALID_AMOUNT = 18,
    WCP_STATUS_RECHARGE_NOT_AUTHORIZED = 19,

    WCP_STATUS_MB_EXCEEDED_TOTAL_RECHARGES_LIMIT = 21,
    WCP_STATUS_MB_EXCEEDED_RECHARGE_LIMIT = 22,
    WCP_STATUS_MB_EXCEEDED_RECHARGES_NUMBER_LIMIT = 23,
    WCP_STATUS_EXCEEDED_MAX_ALLOWED_CASH_IN = 24,
    WCP_STATUS_EXCEEDED_MAX_ALLOWED_DAILY_CASH_IN = 25,
    WCP_STATUS_ML_RECHARGE_NOT_AUTHORIZED = 26,
    WCP_STATUS_ACCOUNT_GLOBAL_MAX_ALLOWED_CASH_IN_COUNT_EXCEEDED = 27,
    WCP_STATUS_ACCOUNT_GLOBAL_MAX_ALLOWED_CASH_IN_AMOUNT_EXCEEDED = 28,
    WCP_STATUS_ACCOUNT_DAILY_MAX_ALLOWED_CASH_IN_COUNT_EXCEEDED = 29,
    WCP_STATUS_ACCOUNT_DAILY_MAX_ALLOWED_CASH_IN_AMOUNT_EXCEEDED = 30,

    WCP_STATUS_MB_SESSION_CLOSED = 31,
    WCP_STATUS_STACKER_ID_NOT_FOUND = 32,
    WCP_STATUS_STACKER_ID_ALREADY_IN_USE = 33,

    WCP_STATUS_GAMING_DAY_EXPIRED = 34,
    WCP_STATUS_ACCOUNT_MIN_ALLOWED_CASH_IN_AMOUNT_FALL_SHORT = 35,

    WCP_STATUS_WKT_CARD_OK = 10000,  // OK  
    WCP_STATUS_WKT_DISABLED = 10001,
    WCP_STATUS_WKT_OK = WCP_STATUS_WKT_CARD_OK,
    WCP_STATUS_WKT_ERROR = 10002,
    WCP_STATUS_WKT_MESSAGE_NOT_SUPPORTED = 10003,
    WCP_STATUS_WKT_RESOURCE_NOT_FOUND = 10004,

    WCP_STATUS_WKT_CARD_NOT_VALID = 10010,  // Card Not Valid     / Tarjeta no v�lida
    WCP_STATUS_WKT_CARD_BLOCKED = 10011,  // Acccount Blocked   / Cuenta bloqueada
    WCP_STATUS_WKT_CARD_NO_PIN = 10012,  // PIN not configured / PIN sin configurar
    WCP_STATUS_WKT_CARD_WRONG_PIN = 10013,   // Wrong PIN          / PIN err�neo    
    WCP_STATUS_WKT_UNKNOWN = 10014,
    WCP_STATUS_WKT_CARD_ERROR = 10015,
    WCP_STATUS_WKT_NOT_ENOUGH_POINTS = 10016,
    WCP_STATUS_WKT_NOT_AVAILABLE = 10017,
    WCP_STATUS_WKT_NOT_ENOUGH_STOCK = 10018,
    WCP_STATUS_WKT_CARD_HAS_NON_REDEEMABLE = 10019,
    WCP_STATUS_WKT_FUNCTIONALITY_OFF_SCHEDULE = 10020,
    WCP_STATUS_WKT_PLAYER_NOT_ALLOWED_TO_REDEEM_POINTS = 10021,
    WCP_STATUS_WKT_FUNCTIONALITY_NOT_ENABLED = 10022,
    WCP_STATUS_WKT_PROMO_NOT_CURRENTLY_APPLICABLE = 10027,
    LCD_FORCE_BALANCE_REQUEST = 10032,
    WCP_STATUS_WKT_NOT_ENOUGH_MACHINE_CREDIT = 10033,
    WCP_STATUS_WKT_MACHINE_PLAYING = 10034

  }

  public enum TYPE_PROTOCOL_MESSAGE
  {
    DEFAULT = 0,            // Aquest no l�enviar� mai. Opci� per defecte.
    NO_NOTIFY = 1,          // Aquest no l�enviar� mai. 
    INFO = 2,               // Missatge de tipus informatiu. (Validating..., Transferring...)
    ERROR = 3,              // Missatge d�error.
    ENTER_AMOUNT = 4,
    ENTER_PIN = 5,

    NONE = 98,              // Esborrar missatge quan s�acabi el timer. (Cal revisar a veure com funciona).
    NONE_IMMEDIATE = 99     // Esborrar el missatge immediatament.  
  }

  public enum LCD_MENU
  {
    ACCOUNT = 1,
    DRAWS = 2,
    PROMOTIONS = 3,
    JACKPOT = 4,
    GIFTS = 5,
    EXCHANGE_CREDITS = 6,
    SHOW_JACKPOT_AWARDED = 7,
    LOTTO_RACE = 8,
    FB = 9
  }

  public enum TYPE_BLOCK_REASON
  {
    NONE = 0,
    //ANY = 0xFFFFFFFF,

    E2PROM_ERROR = 1 << (0),
    ERROR = 1 << (1),
    NOT_ASSIGNED = 1 << (2),
    RNG_MODE = 1 << (3),
    REQUESTED_BY_LKAS = 1 << (4),
    REQUESTED_BY_LKT = 1 << (5),
    DISCONNECTED = 1 << (6),
    DATABASE_ERROR = 1 << (7),
    SHUTTING_DOWN = 1 << (8),
    MIO_OFFLINE = 1 << (9),
    SALE_NOT_ALLOWED = 1 << (10),
    VERSION_STATUS_ERROR = 1 << (11),
    PRINTER_ERROR = 1 << (12),
    INTRUSION_DETECTION = 1 << (13),
    SAS_HOST_DISCONNECTED = 1 << (14),
    CARD_READER_ERROR = 1 << (15),
    SAS_CLIENT_DISCONNECTED = 1 << (16),
    SAS_CLIENT_SHUTDOWN = 1 << (17),
    SAS_CLIENT_AFT_LOCK = 1 << (18),
    FACTORY_TEST_ENTERED = 1 << (19),
    DOOR_OPENED = 1 << (20),
    HANDPAY = 1 << (21),
    LOADING = 1 << (22),

  }

  public enum LCD_MODE
  {
    CASHLESS = 0,
    TITO = 1,
    WASS = 2,
  }

  // uc_button Styles

  /**********************************************/
  /*  First Number  = Image Position in Button  */
  /*  Second Number = Image Aligment            */
  /**********************************************/
  public enum TYPE_IMAGE_STYLE
  {
    LEFT_LEFT = 11,
    LEFT_MID = 12,
    LEFT_RIGHT = 13,

    CENTER_LEFT = 21,
    CENTER_MID = 22,
    CENTER_RIGHT = 23,

    RIGHT_LEFT = 31,
    RIGHT_MID = 32,
    RIGHT_RIGHT = 33,

    NONE = 99
  }

  public enum TYPE_BUTTON_SIZE_STYLE
  {
    //X_Small = 9,
    Small = 10,
    Medium = 11,
    Big = 12,
    Pin = 13,
    Keyboard = 14,
    Opr_UpDown = 15,
    Opr_Calibrate = 16,
    TransferAmount = 17,
    Custom = 18
  }

  public enum TYPE_BALANCE_BAR
  {
    BALANCE = 0,
    POINTS = 1,
    ALL = 2
  }

  public enum TYPE_BORDER_BALANCE_BAR
  {
    NONE = 0,
    RIGHT = 1,
    LEFT = 2,
    ALL = 3
  }

  public enum STATUS_REQUEST_PIN
  {
    NONE = 0,  //False
    REQUEST = 1,  //True
    END_REQUEST = 2,
  }

  /******/

  public enum LanguageIdentifier
  {
    Neutral = 0,
    Chinese = 4,
    Czech = 5,
    English = 9,
    Spanish = 10,
    Italian = 16,
    Korean = 18,
    Russian = 25,
  }

  public enum TYPE_ACTIVITY
  {
    AutoShowHideVideo = 0,
    LoadingPanel = 1,
    Jackpot = 2,
    DefaultLanguage = 3,
    BringToFront = 4,
    AccountInfo = 5,
    GameGateway = 6,
    BrowserActive = 7,
    //WaitingMessage = 8
  }

  public enum TYPE_ITEM_STYLE
  {
    Basic = 0,
    Simple = 1,
    Double = 2
  }

  public enum STATUS_BROWSER
  {
    IDLE = 0,
    END = 1,
    HIDE = 2,
    CLOSING = 3,
    INIT = 4,
    LOADING = 5,
    COMPLETED = 6,
    RUNNING = 7
  }
  public enum TARGET_BROWSER
  {
    MAIN = 0,
    GAMEGATEWAY = 1,
    FB = 2,
    TERMINAL_DRAW = 3
  }

  //
  //GameGateWay
  //
  public enum GAME_GATEWAY_MESSAGE_TYPE
  {
    GAME_GATEWAY_MESSAGE_TYPE_NONE = 0,
    GAME_GATEWAY_MESSAGE_TYPE_DRAW = 1,
    GAME_GATEWAY_MESSAGE_TYPE_PRIZE = 2,
    GAME_GATEWAY_MESSAGE_TYPE_GET_CREDIT = 3,
    GAME_GATEWAY_MESSAGE_TYPE_RESERVED_CREDIT_FROM_LCD = 4,
    GAME_GATEWAY_MESSAGE_TYPE_BROWSER_TIMEOUT = 5,
    GAME_GATEWAY_MESSAGE_TYPE_AWARDED_PRIZE = 6
  }

  public enum GAME_GATEWAY_RESERVED_MODE
  {
    GAME_GATEWAY_RESERVED_MODE_WITHOUT_BUCKETS = 0,
    GAME_GATEWAY_RESERVED_MODE_WITH_BUCKETS = 1,
    GAME_GATEWAY_RESERVED_MODE_MIXED = 2,
  }

  public enum GAME_GATEWAY_ENABLED
  {
    GAME_GATEWAY_ENABLED_NO = 0,
    GAME_GATEWAY_ENABLED_YES = 1
  }

  public enum ENUM_GAME_GATEWAY_PROMO_BALANCE_ACTION
  {
    GAME_GATEWAY_PROMO_BALANCE_ACTION_NONE = 0,
    GAME_GATEWAY_PROMO_BALANCE_ACTION_COLLECT_PRIZE = 1,
    GAME_GATEWAY_PROMO_BALANCE_ACTION_WITHOUT_PIN = 2

  };

  public enum GAME_GATEWAY_ENTER_MODE_WITHOUT_PIN
  {
    GAME_GATEWAY_ENTER_MODE_WITHOUT_PIN_WEB = 0,
    GAME_GATEWAY_ENTER_MODE_WITHOUT_PIN_RESERVE = 1
  }
  //
  //GameGateWay
  //

  // IWebBrowser
  public enum OLECMDEXECOPT
  {
    OLECMDEXECOPT_DODEFAULT = 0,
    OLECMDEXECOPT_PROMPTUSER = 1,
    OLECMDEXECOPT_DONTPROMPTUSER = 2,
    OLECMDEXECOPT_SHOWHELP = 3
  }

  public enum OLECMDID
  {
    OLECMDID_OPEN = 1,
    OLECMDID_NEW = 2,
    OLECMDID_SAVE = 3,
    OLECMDID_SAVEAS = 4,
    OLECMDID_SAVECOPYAS = 5,
    OLECMDID_PRINT = 6,
    OLECMDID_PRINTPREVIEW = 7,
    OLECMDID_PAGESETUP = 8,
    OLECMDID_SPELL = 9,
    OLECMDID_PROPERTIES = 10,
    OLECMDID_CUT = 11,
    OLECMDID_COPY = 12,
    OLECMDID_PASTE = 13,
    OLECMDID_PASTESPECIAL = 14,
    OLECMDID_UNDO = 15,
    OLECMDID_REDO = 16,
    OLECMDID_SELECTALL = 17,
    OLECMDID_CLEARSELECTION = 18,
    OLECMDID_ZOOM = 19,
    OLECMDID_GETZOOMRANGE = 20,
    OLECMDID_UPDATECOMMANDS = 21,
    OLECMDID_REFRESH = 22,
    OLECMDID_STOP = 23,
    OLECMDID_HIDETOOLBARS = 24,
    OLECMDID_SETPROGRESSMAX = 25,
    OLECMDID_SETPROGRESSPOS = 26,
    OLECMDID_SETPROGRESSTEXT = 27,
    OLECMDID_SETTITLE = 28,
    OLECMDID_SETDOWNLOADSTATE = 29,
    OLECMDID_STOPDOWNLOAD = 30,
    OLECMDID_ONTOOLBARACTIVATED = 31,
    OLECMDID_FIND = 32,
    OLECMDID_DELETE = 33,
    OLECMDID_HTTPEQUIV = 34,
    OLECMDID_HTTPEQUIV_DONE = 35,
    OLECMDID_ENABLE_INTERACTION = 36,
    OLECMDID_ONUNLOAD = 37,
    OLECMDID_PROPERTYBAG2 = 38,
    OLECMDID_PREREFRESH = 39,
    OLECMDID_SHOWSCRIPTERROR = 40,
    OLECMDID_SHOWMESSAGE = 41,
    OLECMDID_SHOWFIND = 42,
    OLECMDID_SHOWPAGESETUP = 43,
    OLECMDID_SHOWPRINT = 44,
    OLECMDID_CLOSE = 45,
    OLECMDID_ALLOWUILESSSAVEAS = 46,
    OLECMDID_DONTDOWNLOADCSS = 47,
    OLECMDID_UPDATEPAGESTATUS = 48,
    OLECMDID_PRINT2 = 49,
    OLECMDID_PRINTPREVIEW2 = 50,
    OLECMDID_SETPRINTTEMPLATE = 51,
    OLECMDID_GETPRINTTEMPLATE = 52,
    OLECMDID_PAGEACTIONBLOCKED = 55,
    OLECMDID_PAGEACTIONUIQUERY = 56,
    OLECMDID_FOCUSVIEWCONTROLS = 57,
    OLECMDID_FOCUSVIEWCONTROLSQUERY = 58,
    OLECMDID_SHOWPAGEACTIONMENU = 59,
    //!! From: http://stackoverflow.com/questions/738232/zoom-in-on-a-web-page-using-webbrowser-net-control
    OLECMDID_OPTICAL_ZOOM = 63,
    OLECMDID_OPTICAL_GETZOOMRANGE = 64,
  }

  public enum OLECMDF
  {
    OLECMDF_SUPPORTED = 1,
    OLECMDF_ENABLED = 2,
    OLECMDF_LATCHED = 4,
    OLECMDF_NINCHED = 8,
    OLECMDF_INVISIBLE = 16,
    OLECMDF_DEFHIDEONCTXTMENU = 32
  }

  public enum tagREADYSTATE
  {
    READYSTATE_UNINITIALIZED = 0,
    READYSTATE_LOADING = 1,
    READYSTATE_LOADED = 2,
    READYSTATE_INTERACTIVE = 3,
    READYSTATE_COMPLETE = 4
  }

  #endregion
}