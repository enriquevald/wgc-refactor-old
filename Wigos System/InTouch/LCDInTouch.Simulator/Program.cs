//------------------------------------------------------------------------------
// Copyright � 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//      MODULE NAME: Program.cs
// 
//      DESCRIPTION: Class for config intouch simulator
// 
//           AUTHOR: Javier Barea
// 
//    CREATION DATE: 30-JUN-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 30-JUN-2017 JBP    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace LCDInTouch.Simulator
{
  static class Program
  {
    /// <summary>
    /// The main entry point for the application.
    /// </summary>
    [STAThread]
    static void Main()
    {
      Application.EnableVisualStyles();
      Application.SetCompatibleTextRenderingDefault(false);
      Application.Run(new frm_intouch_simulator());
    }
  }
}