namespace LCDInTouch.Simulator
{
  partial class frm_intouch_simulator
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.btn_pin_ok = new System.Windows.Forms.Button();
      this.button1 = new System.Windows.Forms.Button();
      this.button2 = new System.Windows.Forms.Button();
      this.button3 = new System.Windows.Forms.Button();
      this.SuspendLayout();
      // 
      // btn_pin_ok
      // 
      this.btn_pin_ok.Location = new System.Drawing.Point(197, 12);
      this.btn_pin_ok.Name = "btn_pin_ok";
      this.btn_pin_ok.Size = new System.Drawing.Size(75, 23);
      this.btn_pin_ok.TabIndex = 0;
      this.btn_pin_ok.Text = "Pin OK";
      this.btn_pin_ok.UseVisualStyleBackColor = true;
      this.btn_pin_ok.Click += new System.EventHandler(this.btn_pin_ok_Click);
      // 
      // button1
      // 
      this.button1.Location = new System.Drawing.Point(116, 12);
      this.button1.Name = "button1";
      this.button1.Size = new System.Drawing.Size(75, 23);
      this.button1.TabIndex = 1;
      this.button1.Text = "Insert Card";
      this.button1.UseVisualStyleBackColor = true;
      this.button1.Click += new System.EventHandler(this.button1_Click);
      // 
      // button2
      // 
      this.button2.Location = new System.Drawing.Point(197, 55);
      this.button2.Name = "button2";
      this.button2.Size = new System.Drawing.Size(75, 23);
      this.button2.TabIndex = 2;
      this.button2.Text = "Extract Card";
      this.button2.UseVisualStyleBackColor = true;
      this.button2.Click += new System.EventHandler(this.button2_Click);
      // 
      // button3
      // 
      this.button3.Location = new System.Drawing.Point(12, 12);
      this.button3.Name = "button3";
      this.button3.Size = new System.Drawing.Size(98, 23);
      this.button3.TabIndex = 3;
      this.button3.Text = "Show Insert Card";
      this.button3.UseVisualStyleBackColor = true;
      this.button3.Click += new System.EventHandler(this.button3_Click);
      // 
      // frm_intouch_simulator
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(284, 90);
      this.Controls.Add(this.button3);
      this.Controls.Add(this.button2);
      this.Controls.Add(this.button1);
      this.Controls.Add(this.btn_pin_ok);
      this.Name = "frm_intouch_simulator";
      this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
      this.Text = "InTouch Simulator";
      this.Load += new System.EventHandler(this.frm_lcd_automode_Load);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Button btn_pin_ok;
    private System.Windows.Forms.Button button1;
    private System.Windows.Forms.Button button2;
    private System.Windows.Forms.Button button3;
  }
}