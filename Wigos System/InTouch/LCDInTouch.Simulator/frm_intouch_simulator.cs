//------------------------------------------------------------------------------
// Copyright � 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//      MODULE NAME: frm_intouch_simulator.cs
// 
//      DESCRIPTION: simulator form for LCD inTouch
// 
//           AUTHOR: Javier Barea
// 
//    CREATION DATE: 30-JUN-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 30-JUN-2017 JBP    First release.
//------------------------------------------------------------------------------

using System;
using System.Drawing;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Threading;
using LCD_Display;

namespace LCDInTouch.Simulator
{
  public partial class frm_intouch_simulator : Form
  {
    private ILCD IInTouch;

    public frm_intouch_simulator()
    {
      InitializeComponent();
    }

    private void frm_lcd_automode_Load(object sender, EventArgs e)
    {
      try
      {

        InTouch _in_touch;
        _in_touch = new InTouch();

        _in_touch.KeyHandler = new KeyPressed(KeyPressedHandler);
        _in_touch.ProtocolHandler = new ProtocolRequest(ProtocolRequestHandler);
        this.IInTouch = _in_touch;
        
        //this.WindowState = FormWindowState.Minimized;

        this.Location = new Point(250, 500);
        _in_touch.Init(0);

        Thread _thread;
        //_thread = new Thread(MainFormThread);
        _thread = new Thread(RunLCD);
        _thread.Start();


        MessageLCD();
      }
      catch (Exception _ex)
      {
        MessageBox.Show("An error ocurred loading simulator: " + _ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
    }
    private void KeyPressedHandler(int Key)
    {

    }

    private void ProtocolRequestHandler(int Request, string Data)
    {
      String _info;
      IntPtr strPtr;

      strPtr = new IntPtr();
      try
      {
        switch ((TYPE_REQUEST)Request)
        {
          case TYPE_REQUEST.CARD_STATUS:
            break;
          case TYPE_REQUEST.HOLDER_NAME:
            break;
          case TYPE_REQUEST.CARD_TYPE:
            break;
          case TYPE_REQUEST.PIN:
            if (Data == "1234")
            {
              this.IInTouch.ProtocolResponse(Request, (int)TYPE_PROTOCOL_RESPONSE.WCP_STATUS_WKT_CARD_OK, new IntPtr());
            }
            else
            {
              this.IInTouch.ProtocolResponse(Request, (int)TYPE_PROTOCOL_RESPONSE.WCP_STATUS_WKT_CARD_WRONG_PIN, new IntPtr());
            }
            break;
          case TYPE_REQUEST.GIFT:
            break;
          case TYPE_REQUEST.DRAW:
            break;
          case TYPE_REQUEST.PROMOTION:
            break;
          case TYPE_REQUEST.ACCOUNT_INFO:

            _info = String.Format("<Level>{0}</><Points>{1}</><Balance>{2}</><NextLevel>{3}</><NextLevelPoints>{4}</><EstimatedPoints>{5}</>",
                                    "Oro", "100", "25", "Platinium", "200", "15");

            strPtr = Marshal.StringToHGlobalAnsi(_info);
            this.IInTouch.ProtocolResponse(Request, (int)TYPE_PROTOCOL_RESPONSE.WCP_STATUS_WKT_CARD_OK, strPtr);

            break;
          case TYPE_REQUEST.ESTIMATED_POINTS:
            _info = String.Format("18");

            strPtr = Marshal.StringToHGlobalAnsi(_info);
            this.IInTouch.ProtocolResponse(Request, (int)TYPE_PROTOCOL_RESPONSE.WCP_STATUS_WKT_CARD_OK, strPtr);
            break;
          case TYPE_REQUEST.BLOCK_REASON:
            break;
          case TYPE_REQUEST.FT_ENTER:
            break;
          case TYPE_REQUEST.CARD_MESSAGE:
            break;
          default:
            break;
        }
      }
      catch (Exception _ex)
      {
        MessageBox.Show(_ex.Message);
      }
      finally
      {
        if (strPtr != null)
        {
          Marshal.FreeHGlobal(strPtr);
        }
      }

    }


    private void RunLCD()
    {

    }
    private void MessageLCD()
    {

      int _multiplicador = 1;
      String _msg;

      //OBSOLETE
      //while (true)
      //{
      //  try
      //  {
      //    if (this.IInTouch.MsgInterface((int)TYPE_EVENT.FORM_READY, String.Empty))
      //    {
      //      break;
      //    }

      //    System.Threading.Thread.Sleep(500 * _multiplicador);
      //  }
      //  catch { ;}
      //}

      //Sleep before form is created
      System.Threading.Thread.Sleep(5000);

      return;

      //this.IInTouch.ProtocolResponse((int)TYPE_REQUEST.CARD_STATUS, (int)TYPE_CARD_STATUS.CARD_ERROR, new IntPtr());
      //return;
      //this.IInTouch.ProtocolResponse((int)TYPE_REQUEST.BLOCK_REASON, (int)TYPE_RESPONSE_PIN.WCP_STATUS_WKT_CARD_OK, new IntPtr());
      //System.Threading.Thread.Sleep(2000 * _multiplicador);
      //this.IInTouch.MsgInterface((int)TYPE_EVENT.BLOCK_REASON, "Sigue bloqueado");
      //System.Threading.Thread.Sleep(5000 * _multiplicador);
      //this.IInTouch.MsgInterface((int)TYPE_EVENT.BLOCK_REASON, "");

      //this.IInTouch.MsgInterface((int)TYPE_EVENT.INSERT_CARD, "4|Joaquin Calero");
      //return;

      //this.IInTouch.MsgInterface((int)TYPE_EVENT.INSERT_CARD, "1|Joaquin Calero");
      //return;
      //System.Threading.Thread.Sleep(3000 * _multiplicador);


      //this.IInTouch.MsgInterface(2, "Leer Base de Datos".PadRight(20, ' '));
      //System.Threading.Thread.Sleep(3000 * _multiplicador);

      //this.IInTouch.ProtocolResponse((int)TYPE_REQUEST.PIN, (int)TYPE_RESPONSE_PIN.WCP_STATUS_WKT_CARD_OK, new IntPtr());
      //return;

      //this.IInTouch.MsgInterface((int)TYPE_EVENT.EXTRACT_CARD, "");
      //return;





      _msg = "Leer Base de Datos".PadRight(20, ' ') + "\nDispositivos: :Carga".PadRight(20, ' ') + "\n- UpperDisplay: :C:".PadRight(20, ' ');
      this.IInTouch.MsgInterface(2, _msg);
      System.Threading.Thread.Sleep(3000 * _multiplicador);

      _msg = "BLOQUEADO".PadRight(20, ' ') + "\nSin conexi�n Centro".PadRight(20, ' ') + "\nLector Tarj.: Error".PadRight(20, ' ');
      this.IInTouch.MsgInterface(2, _msg);
      System.Threading.Thread.Sleep(3000 * _multiplicador);

      for (int i = 0; i < 10; i++)
      {
        _msg = "Welcome".PadRight(20, ' ') + "\nInsert card".PadRight(20, ' ') + "\n[SAS-HOST CALERO 005]".PadRight(20, ' ');
        this.IInTouch.MsgInterface(2, _msg);
        System.Threading.Thread.Sleep(3000 * _multiplicador);

        this.IInTouch.MsgInterface(2, "Validating...".PadRight(20, ' '));
        System.Threading.Thread.Sleep(3000 * _multiplicador);

        _msg = "SR CALERO".PadRight(20, ' ') + "\nCredit $173.60".PadRight(20, ' ') + "\nPoints: 3".PadRight(20, ' ') + "\nTransferring...".PadRight(20, ' ');
        this.IInTouch.MsgInterface(2, _msg);
        System.Threading.Thread.Sleep(1500 * _multiplicador);

        _msg = "\nCredit $173.60".PadRight(20, ' ') + "\n".PadRight(20, ' ') + "\n[SAS-HOST CALERO 005]".PadRight(20, ' ');
        this.IInTouch.MsgInterface(2, _msg);
        System.Threading.Thread.Sleep(1500 * _multiplicador);

        _msg = "\nCredit $172.60".PadRight(20, ' ') + "\n".PadRight(20, ' ') + "\n[SAS-HOST CALERO 005]".PadRight(20, ' ');
        this.IInTouch.MsgInterface(2, _msg);
        System.Threading.Thread.Sleep(1500 * _multiplicador);

        _msg = "\nCredit $171.00".PadRight(20, ' ') + "\n".PadRight(20, ' ') + "\n[SAS-HOST CALERO 005]".PadRight(20, ' ');
        this.IInTouch.MsgInterface(2, _msg);
        System.Threading.Thread.Sleep(1500 * _multiplicador);

        _msg = "\nCredit $178.00".PadRight(20, ' ') + "\n".PadRight(20, ' ') + "\n[SAS-HOST CALERO 005]".PadRight(20, ' ');
        this.IInTouch.MsgInterface(2, _msg);
        System.Threading.Thread.Sleep(1500 * _multiplicador);

        _msg = "\nCredit $183.20".PadRight(20, ' ') + "\n".PadRight(20, ' ') + "\n[SAS-HOST CALERO 005]".PadRight(20, ' ');
        this.IInTouch.MsgInterface(2, _msg);
        System.Threading.Thread.Sleep(1500 * _multiplicador);

        _msg = "\nCredit $180.00".PadRight(20, ' ') + "\n".PadRight(20, ' ') + "\n[SAS-HOST CALERO 005]".PadRight(20, ' ');
        this.IInTouch.MsgInterface(2, _msg);
        System.Threading.Thread.Sleep(1500 * _multiplicador);

        _msg = "\nCredit $175.20".PadRight(20, ' ') + "\n".PadRight(20, ' ') + "\n[SAS-HOST CALERO 005]".PadRight(20, ' ');
        this.IInTouch.MsgInterface(2, _msg);
        System.Threading.Thread.Sleep(1500 * _multiplicador);

        _msg = "\nCredit $169.60".PadRight(20, ' ') + "\n".PadRight(20, ' ') + "\n[SAS-HOST CALERO 005]".PadRight(20, ' ');
        this.IInTouch.MsgInterface(2, _msg);
        System.Threading.Thread.Sleep(1500 * _multiplicador);


        _msg = "Thank you".PadRight(20, ' ') + "\nCredit $169.60".PadRight(20, ' ') + "\n".PadRight(20, ' ') + "\nTransferring...".PadRight(20, ' ');
        this.IInTouch.MsgInterface(2, _msg);
        System.Threading.Thread.Sleep(1500 * _multiplicador);

        _msg = "Thank you".PadRight(20, ' ') + "\nCredit $169.60".PadRight(20, ' ') + "\nPoints: 7".PadRight(20, ' ');
        this.IInTouch.MsgInterface(2, _msg);
        System.Threading.Thread.Sleep(2000 * _multiplicador);
      }

      Application.Exit();
      //this.IInTouch.MsgInterface(2, "");
      //System.Threading.Thread.Sleep(3000*_multiplicador);

    }

    private void btn_pin_ok_Click(object sender, EventArgs e)
    {

      this.IInTouch.ProtocolResponse((int)TYPE_REQUEST.PIN, (int)TYPE_PROTOCOL_RESPONSE.WCP_STATUS_WKT_CARD_OK, new IntPtr());
      //return;
    }

    private void button1_Click(object sender, EventArgs e)
    {
      this.IInTouch.MsgInterface((int)TYPE_EVENT.INSERT_CARD, "1|Joaquin Calero");
    }

    private void button2_Click(object sender, EventArgs e)
    {
      this.IInTouch.MsgInterface((int)TYPE_EVENT.EXTRACT_CARD, "");
    }

    private void button3_Click(object sender, EventArgs e)
    {
      this.IInTouch.ProtocolResponse((int)TYPE_REQUEST.MESSAGE_CODE, (int)TYPE_MESSAGE_CODE.INSERT_CARD, new IntPtr());
    }
   
  }
}