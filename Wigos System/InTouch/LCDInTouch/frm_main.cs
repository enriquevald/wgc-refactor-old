//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_main.cs
// 
//   DESCRIPTION: InTouch Main form 
// 
//        AUTHOR: Javier Barea
// 
// CREATION DATE: 20-JUN-2017
// 
// REVISION HISTORY:
// 
// Date        Author  Description
// ----------- ------  ---------------------------------------------------
// 20-JUN-2017 JBP     First release.
//------------------------------------------------------------------------
using System;
using System.Drawing;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;

using AxAXVLC;
using LCDInTouch.Common;

namespace LCDInTouch
{
  public partial class frm_main : frm_base
  {
    #region Constants

    //private const int DEFAULT_TIMER_INTERVAL_JACKPOT = 5000;
    private const string DEFAULT_MAIN_URL = "http://WKS-55/LCDInTouch";
    //private const string DEFAULT_PIMPAMGO_URL = "http://uat2.lottorace.com/client/web/secure/winsys/login?sessionid=existing&lang=en_US&html=true#";
    //private const string DEFAULT_FB_URL = "http://10.254.4.154/lcd_test/default.aspx?customerid={0}&posid={1}";
    //private const string DEFAULT_FB_URL_LOCALHOST = "http://localhost:61150/default.aspx?customerid={0}&posid={1}";

    #endregion

    #region Variables

    private Int32 m_current_list_item;

    private LCD_ITEM m_item;

    private Int32 m_jackpot_interval = 0;
    private Boolean m_is_jackpot_showed = false;        // To control if the jackpot control is showed by timer event, not by user.    
    private Boolean m_is_chashout = false;              // To know is Cash Out is done.
    private String m_marquee_text = string.Empty;

    private Timer m_timer_marquee = new Timer();        // Timer to move the message label.
    private Timer m_timer_hide_CA = new Timer();        // Timer to hide call attendant button.
    private Timer m_main_timer;
    private Int16 m_main_timer_num_ticks = 1;

    private Int32 m_time_to_logout;
    private STATUS_REQUEST_PIN m_show_request_pin;

    private Boolean m_pending_logout_operator_panel;
    private Boolean m_next_message_is_mobile_bank_response;

    private Boolean m_message_bring_to_front = false;
    private SASParams m_sas_params;
    private TerminalDrawNavigationParams m_TerminalDrawNavigationParams = null;

    #endregion

    #region Properties

    public Timer MainTimer
    {
      get
      {
        if (this.m_main_timer == null)
        {
          this.m_main_timer = new Timer();

          this.m_main_timer.Interval = Constants.DEFAULT_CHECK_ACTIVITY;
          this.m_main_timer.Tick += new EventHandler(this.MainTimer_Tick);

          this.UpdateLastActivity();
        }

        return this.m_main_timer;
      }
      set { this.m_main_timer = value; }
    }

    public Int16 MainTimerNumTicks
    {
      get { return m_main_timer_num_ticks; }
      set { m_main_timer_num_ticks = value; }
    } 

    public SASParams SASParams
    {
      get { return this.m_sas_params; }
      set { this.m_sas_params = value; }
    }

    public AxVLCPlugin2 VLC_Plugin
    {
      get { return this.axVLCPlugin21; }
      set { this.axVLCPlugin21 = value; }
    } // VLC_Plugin

    public Boolean PendingLogoutOperatorPanel
    {
      get { return this.m_pending_logout_operator_panel; }
      set { this.m_pending_logout_operator_panel = value; }
    } // PendingLogoutOperatorPanel

    public Boolean NextMessageIsMobileBankResponse
    {
      get { return this.m_next_message_is_mobile_bank_response; }
      set { this.m_next_message_is_mobile_bank_response = value; }
    } // PendingLogoutOperatorPanel

    public Int32 AutoLogoutTime
    {
      get
      {
        if (this.m_time_to_logout <= 0)
        {
          this.m_time_to_logout = Constants.DEFAULT_LOGOUT_TIME;
        }

        return this.m_time_to_logout;
      }
      set { this.m_time_to_logout = value; }
    } // LastActivity

    public Panel Marquee
    {
      get { return this.pnl_marquee; }
      set { this.pnl_marquee = value; }
    } // Marquee

    public Boolean MessageBringToFront
    {
      get { return this.m_message_bring_to_front; }
      set { this.m_message_bring_to_front = value; }
    }

    //public Int32 DefaultTimerIntervalJackport
    //{
    //  get { return DEFAULT_TIMER_INTERVAL_JACKPOT; }
    //} // DefaultTimerIntervalJackport

    public Boolean IsJackpotShowed
    {
      get { return this.m_is_jackpot_showed; }
      set { this.m_is_jackpot_showed = value; }
    } // IsJackpotShowed

    public Int32 CurrentListItem
    {
      get { return this.m_current_list_item; }
      set { this.m_current_list_item = value; }
    }

    public LCD_ITEM SelectedItem
    {
      get { return this.m_item; }
      set { this.m_item = value; }
    } // SelectedItem

    public Int32 JackpotInterval
    {
      get { return this.m_jackpot_interval; }
      set { this.m_jackpot_interval = value; }
    } // JackpotInterval

    public String MarqueeText
    {
      get { return this.m_marquee_text; }
      set { this.m_marquee_text = value; }
    } // MarqueeText

    public Timer TimerMarquee
    {
      get { return this.m_timer_marquee; }
      set { this.m_timer_marquee = value; }
    } // TimerMarquee

    public Timer TimerCallAttendant
    {
      get { return this.m_timer_hide_CA; }
      set { this.m_timer_hide_CA = value; }
    } // TimerCallAttendant

    public Boolean IsCashOut
    {
      get { return this.m_is_chashout; }
      set { this.m_is_chashout = value; }
    } // IsCashOut

    public Boolean OnlyAccountInfo
    {
      // AJQ Oly for first version
      get { return true; }
    }

    public Boolean TITOReedimCredits
    {
      get { return true; }
    }

    public STATUS_REQUEST_PIN ShowRequestPin
    {
      get { return this.m_show_request_pin; }
      set { this.m_show_request_pin = value; }
    }

    public TerminalDrawNavigationParams TerminalDrawNavigationParams
    {
      get { return m_TerminalDrawNavigationParams; }
      set { m_TerminalDrawNavigationParams = value; }
    }

    #endregion

    #region Message Interface

    delegate void SetTextCallback(Control Control, String Text);
    delegate void SetHideBrowser(double Opacity);

    //------------------------------------------------------------------------------
    // PURPOSE: To Insert a card.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    internal void InsertCard(String HolderName)
    {
      try
      {
        //this.InitializeAccountStructures(HolderName);

        //switch (this.TypeCardMode)
        //{
        //  case TYPE_CARD.UNKNOWN:
        //  case TYPE_CARD.PLAYER:
        //  case TYPE_CARD.PLAYER_PIN:
        //    this.PlayerLogin(HolderName);
        //    this.CurrentBody = TYPE_BODY.MAIN;
        //    break;
        //  case TYPE_CARD.TECH:
        //  case TYPE_CARD.TECH_PIN:
        //  case TYPE_CARD.CHANGE_STACKER:
        //  case TYPE_CARD.CHANGE_STACKER_PIN:
        //  case TYPE_CARD.MOBILE_BANK:
        //    this.CurrentBody = TYPE_BODY.KEYBOARD;
        //    break;
        //  default:
        //    Log.Add(TYPE_MESSAGE.WARNING, "Type not defined in 'InsertCard()': '" + this.TypeCardMode.ToString() + "'");
        //    break;
        //}
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // InsertCard

    //------------------------------------------------------------------------------
    // PURPOSE: Operator login
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    internal void OperatorLogin(int Status, TYPE_CARD TypeCard)
    {
      //Boolean _logout;
      //TYPE_CARD _previous_card_mode;
      //_previous_card_mode = this.TypeCardMode;

      //try
      //{
      //  this.TypeCardMode = TypeCard;

      //  if ((TYPE_SCREEN_STATUS)Status == TYPE_SCREEN_STATUS.ENTER)
      //  {
      //    if (TypeCard == TYPE_CARD.TECH)
      //    {
      //      _logout = (_previous_card_mode != TYPE_CARD.TECH && _previous_card_mode != TYPE_CARD.TECH_PIN);
      //    }
      //    else
      //    {
      //      _logout = (_previous_card_mode != TYPE_CARD.CHANGE_STACKER && _previous_card_mode != TYPE_CARD.CHANGE_STACKER_PIN);
      //    }

      //    if (_logout)
      //    {
      //      this.Body.Keyboard.HideOperatorButtons(false);
      //    }

      //    this.CloseMessage();
      //    this.BindContainer(TYPE_BODY.KEYBOARD);
      //    if (TypeCard == TYPE_CARD.TECH)
      //    {
      //      this.ChangeOnKeyboard = KB_TYPE.TECH;
      //    }
      //    else
      //    {
      //      this.ChangeOnKeyboard = KB_TYPE.COMPLETE;
      //    }
      //  }
      //  else
      //  {
      //    this.Body.Keyboard.HideOperatorButtons(false);

      //    if (!this.KeyboardManualEntry)
      //    {
      //      this.ChangeOnKeyboard = KB_TYPE.EXIT;
      //      this.BindContainer(TYPE_BODY.VOID);
      //    }
      //    else
      //    {
      //      this.ChangeOnKeyboard = KB_TYPE.COMPLETE;
      //    }

      //    this.LCD.InitializeTimeToAskCardStatus();
      //  }
      //}
      //catch (Exception _ex)
      //{
      //  Log.Add(_ex);
      //}

    } // OperatorLogin


    //------------------------------------------------------------------------------
    // PURPOSE:  Check if show Jackpot
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    internal void CheckShowJackpotAwared_Funcionality(ushort Count, ushort[] Funcionalities)
    {
      ushort _idx;
      LCD_MENU _menu_item;
      this.ShowJackpotAwared = false;

      for (_idx = 0; _idx < Count; _idx++)
      {
        _menu_item = (LCD_MENU)Funcionalities[_idx];
        switch (_menu_item)
        {
          case LCD_MENU.SHOW_JACKPOT_AWARDED:
            this.ShowJackpotAwared = true;
            break;
          default:
            break;
        }
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE:  MobileBank  mode
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    internal void MobileBank(int Status, TYPE_CARD TypeCard)
    {
      //try
      //{
      //  if ((TYPE_SCREEN_STATUS)Status == TYPE_SCREEN_STATUS.ENTER)
      //  {
      //    this.TypeCardMode = TypeCard;

      //    this.MessageQueue.Enqueue(this.LCD.CloseAllMessage);
      //    this.BindContainer(TYPE_BODY.KEYBOARD);
      //    this.ChangeOnKeyboard = KB_TYPE.MOBILE_BANK;

      //    this.PendingLogoutOperatorPanel = true;
      //  }
      //  else
      //  {
      //    this.ChangeOnKeyboard = KB_TYPE.EXIT;
      //    this.LogoutMobileBank();
      //  }
      //}
      //catch (Exception _ex)
      //{
      //  Log.Add(_ex);
      //}
    } // MB Login

    //------------------------------------------------------------------------------
    // PURPOSE: To Extract the card.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    internal void ExtractCard()
    {
      this.ExtractCard(false);
    }
    internal void ExtractCard(Boolean ForzeExtractCard)
    {
      //try
      //{
      //  this.PendingEndPlayerSession = false;
      //  //JCA Delay check Account info
      //  if (!ForzeExtractCard)
      //  {
      //    this.Activities.UpdateActivity(TYPE_ACTIVITY.AccountInfo, DateTime.Now.AddMilliseconds(Constants.DEFAULT_ACCOUNT_INFO_TIME));
      //  }

      //  // 15-FEB-2016 JCA        Product Backlog Item 9384:LCD: No quitar la pantalla de Jackpot automáticamente.
      //  this.CloseJackpot();

      //  //if (this.LCD.IsGameGateway && this.BrowserForm != null)
      //  if (this.BrowserForm != null)
      //  {
      //    //this.ShowHideMainBody(false);
      //    this.EnableLoadingPictures(false);
      //    this.StopBrowser();

      //    this.PendingEndPlayerSession = true;

      //    return;
      //  }

      //  if (this.IsInsertedCardValidated || this.ShowRequestPin != STATUS_REQUEST_PIN.NONE)
      //  {
      //    this.ClosePlayerSession();
      //  }
      //}
      //catch (Exception _ex)
      //{
      //  Log.Add(_ex);
      //  this.ExtractCard(true);
      //}
    } // ExtractCard

    //------------------------------------------------------------------------------
    // PURPOSE: Close player session
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    internal Boolean ClosePlayerSession()
    {
      //try
      //{
      //  this.Account = null; //JCA if extract card not have account!!!!
      //  this.ShowRequestPin = STATUS_REQUEST_PIN.NONE;
      //  this.IsInsertedCardValidated = false;
      //  this.PendingEndPlayerSession = false;
      //  this.InitializeAccountStructures(String.Empty);

      //  this.IsCashOut = false;

      //  if (this.IsNotTechnicalScreen()
      //    && (this.BrowserIsClosing
      //      || this.TypeCardMode == TYPE_CARD.PLAYER
      //      || this.TypeCardMode == TYPE_CARD.PLAYER_PIN))
      //  {
      //    this.BindContainer(TYPE_BODY.VOID);
      //  }

      //  this.TypeCardMode = TYPE_CARD.UNKNOWN;
      //  this.Activities.UpdateActivities();

      //  return true;
      //}
      //catch (Exception _ex)
      //{
      //  Log.Add(_ex);
      //}

      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Close browser manager
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    internal Boolean CloseBrowserManager()
    {
      try
      {
        this.CloseBrowser(false);

        this.CloseFrmBrowser();
        this.UnInstanceBrowser();
        TerminalDrawNavigationParams = null;
        //Delete All files with extension *.swf (if you delete one file, doesn't work ¿?)
        Functions.DeleteFileTempInternetFilesSWF();
        SafeInvokeExt.SetPropertyValue(this.pb_loading_browser, "Visible", false);

        return true;
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }

      return false;
    } // CloseBrowserManager

    //------------------------------------------------------------------------------
    // PURPOSE: To clear WebBrowser.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    internal void CloseBrowser(Boolean RefreshBalance)
    {
      //if (this.BrowserForm != null && !this.BrowserForm.IsDisposed)
      //{
      //  if (this.BrowserForm.InvokeRequired)
      //  {
      //    this.BrowserForm.BeginInvoke(new MethodInvoker(delegate
      //     {
      //       this.CloseBrowser(RefreshBalance);
      //     }));
      //  }
      //  else
      //  {
      //    /**FJC: If clear Sas Params when CloseBrowser, when we try to open browser in the same session card, we don't have Params Sessions,
      //    /So, We Clear SASParams when we extract Card (see: internal void ExtractCard())**/
      //    //this.SASParams = null; 

      //    //Browser
      //    if (this.BrowserForm != null && !this.BrowserForm.IsDisposed)
      //    {
      //      this.BrowserForm.UnLoad();
      //    }

      //    // Only for gamegateway
      //    if(this.BrowserTarget== TARGET_BROWSER.GAMEGATEWAY)
      //    {
      //      this.EnableButtonsGameGateWay(true);
      //      if (this.CurrentBody == TYPE_BODY.INTERACTIVE_MESSAGE)
      //      {
      //        //////this.Body.InteractiveMessage.Close();
      //      }
      //    }
      //    //Stop Thread Image transitions web Loading
      //    this.LCD.StopWebLoadThread();

      //    //Update Balance
      //    if ((BrowserTarget == TARGET_BROWSER.GAMEGATEWAY || BrowserTarget == TARGET_BROWSER.FB) && RefreshBalance)
      //    {
      //      this.Account.BackgroundGetInfo = true;
      //      this.Activities.UpdateActivity(TYPE_ACTIVITY.AccountInfo, DateTime.Now.AddMilliseconds(Constants.DEFAULT_ACCOUNT_INFO_TIME));
      //      this.LCD.SendProtocolHandler(TYPE_REQUEST.GET_PROMO_BALANCE, ((Int16)ENUM_GAME_GATEWAY_PROMO_BALANCE_ACTION.GAME_GATEWAY_PROMO_BALANCE_ACTION_WITHOUT_PIN).ToString());
      //    }
      //    else if (BrowserTarget == TARGET_BROWSER.TERMINAL_DRAW)
      //      LCD.SendProtocolHandler(TYPE_REQUEST.TERMINAL_DRAW_GAME_RESULT, "999");

      //  }
      //}
    }//CloseBrowser

    //------------------------------------------------------------------------------
    // PURPOSE: To set the Jackpot Timer Interval .
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Int32 Interval.
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    internal override void UpdateMarqueeMessage()
    {
      base.UpdateMarqueeMessage();

      if (!String.IsNullOrEmpty(this.MarqueeText))
      {
        this.SetMarqueeMessage(this.MarqueeText);
      }
    } // UpdateMarqueeMessage

    //------------------------------------------------------------------------------
    // PURPOSE: To set the Jackpot Timer Interval .
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Int32 Interval.
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    internal void SetMarqueeMessage(String Msg)
    {
      //try
      //{
      //  this.MarqueeText = Msg.Replace("\n", "");

      //  if (this.KeyBoardEnabled || String.IsNullOrEmpty(this.MarqueeText.Trim()))
      //  {
      //    if (this.Marquee.Visible)
      //    {
      //      this.HideMarquee();

      //      if (MessageBringToFront)
      //      {
      //        SafeInvokeExt.InvokeMethodCtrl(this.Message, "BringToFront", null);
      //        MessageBringToFront = false;
      //      }
      //    }

      //    return;
      //  }
      //  this.ShowMarquee();
      //}
      //catch (Exception _ex)
      //{
      //  Log.Add(_ex);
      //}
    } // SetMarqueeMessage

    //------------------------------------------------------------------------------
    // PURPOSE: To set the Jackpot Timer Interval .
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Int32 Interval.
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    internal void ShowJackpotInterval(Int32 Interval)
    {
      try
      {
        this.JackpotInterval = Interval * 1000;
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // ShowJackpotInterval

    //------------------------------------------------------------------------------
    // PURPOSE: To show the marquee with the message.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Int32 Interval.
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    internal void CashOutEvent(Int32 Interval)
    {
      try
      {
        this.IsCashOut = true;

        if (this.MainVideo.Panel.Visible == false)
        {
          this.InitHideCallAttendant(Interval);
        }
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // CashOutEvent

    //------------------------------------------------------------------------------
    // PURPOSE: To show the operator panel.
    // 
    //  PARAMS:
    //      - INPUT:
    //          · Nothing.
    //
    //      - OUTPUT:
    //          · Nothing.
    //
    // RETURNS:
    //      - Nothing.
    //
    internal void ShowOperatorPanel()
    {
      //try
      //{
      //  this.MainVideo.Close();
      //  this.BindContainer(TYPE_BODY.OPERATOR);
      //}
      //catch (Exception _ex)
      //{
      //  Log.Add(_ex);
      //}
    } // ShowOperatorPanel

    //------------------------------------------------------------------------------
    // PURPOSE: To show the Keyboard panel.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Nothing
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - Void
    internal void ShowKeyboard()
    {
      //try
      //{
      //  if (this.MainVideo != null)
      //  {
      //    this.MainVideo.Close();
      //  }
      //  this.BindContainer(TYPE_BODY.KEYBOARD);
      //  this.ChangeOnKeyboard = KB_TYPE.TECH;
      //  this.CloseMessage();
      //}
      //catch (Exception _ex)
      //{
      //  Log.Add(_ex);
      //}
    } //ShowKeyboard

    ////------------------------------------------------------------------------------
    //// PURPOSE: To set the marquee location.
    //// 
    ////  PARAMS:
    ////      - INPUT:
    ////
    ////      - OUTPUT:
    ////
    //// RETURNS:
    ////
    //internal void SetMarqueeLocation()
    //{
    //  Int32 _width;
    //  Int32 _height;
    //  Int32 _left;
    //  Int32 _top;

    //  _left = 0;
    //  _height = this.ApplyScaleY(30);
    //  _top = this.pnl_bot.Top - _height;

    //  _width = (this.VideoIsPlaying || !this.ControlsLoaded) ? this.Width
    //                                                          : this.pnl_base.Width;

    //  // This scaled in frm_main constructor
    //  this.Marquee.Location = new Point(_left, _top);
    //  this.Marquee.Size = new Size(_width, _height);
    //} // SetMarqueeLocation

    //------------------------------------------------------------------------------
    // PURPOSE: To set the marquee font size.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    internal Font SetMarqueeFontSize()
    {
      //Font _font;

      //if (this.MarqueeText.Length >= 100)
      //{
      //  _font = new Font(this.lbl_msg.Font.FontFamily, 8);
      //}
      //else if (this.MarqueeText.Length >= 90)
      //{
      //  _font = new Font(this.lbl_msg.Font.FontFamily, 9);
      //}
      //else if (this.MarqueeText.Length >= 75)
      //{
      //  _font = new Font(this.lbl_msg.Font.FontFamily, 10);
      //}
      //else
      //{
      //  _font = new Font(this.lbl_msg.Font.FontFamily, 12);
      //}
      return this.Font;
    } // SetMarqueeFontSize

    //------------------------------------------------------------------------------
    // PURPOSE: To set text.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    internal void SetText(Control Control, String Text)
    {
      if (Control.InvokeRequired)
      {
        SetTextCallback d = new SetTextCallback(SetText);
        this.Invoke(d, new object[] { Text });
      }
      else
      {
        Control.Text = Text;
      }
    } // SetText

    //------------------------------------------------------------------------------
    // PURPOSE: To set text.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    internal void SetHide(double Opacity)
    {
      Opacity = 0;
    } // SetHide

    #endregion

    #region Public Functions

    //------------------------------------------------------------------------------
    // PURPOSE: Main.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    public frm_main(InTouch InTouch)
    {
      this.InTouch = InTouch;

      //this.InTouch.Init();

      this.InitializeComponent();
      this.InitializeControls();
    }

    internal void PlayVideo()
    {
      if (this.VLC_Plugin != null
        && this.VLC_Plugin.playlist.items.count > 0)
      {
        if (this.MainVideo.Initializing)
        {
          this.VLC_Plugin.playlist.playItem(0);
        }
        else
        {
          this.VLC_Plugin.playlist.togglePause();
        }

        this.PlayVideoPending = false;
      }
      else
      {
        ;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: To initialize several controls
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    internal override void InitializeControls()
    {
      try
      {
        base.InitializeControls();

        this.SetFormLocation();
        this.SetApplicationLogo();
        this.MainTimer.Start();

        this.pnl_loading.BringToFront();
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // InitializeControls

    //------------------------------------------------------------------------------
    // PURPOSE: Add body panel.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - TYPE_CONTAINER Container.
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    internal override void AddBody(TYPE_BODY TypeBody)
    {
      //try
      //{
      //  if (this.pnl_base.InvokeRequired)
      //  {
      //    this.pnl_base.BeginInvoke(new MethodInvoker(delegate
      //    {
      //      this.BindBody(TypeBody);
      //    }));
      //  }
      //  else
      //  {
      //    base.AddBody(TypeBody);
      //    this.BindBody(TypeBody);
      //  }
      //}
      //catch (Exception _ex)
      //{
      //  Log.Add(_ex);
      //}
    } // AddBody

    public void ShowLoadingPanel(TYPE_LOADING Type)
    {
      try
      {
        if (this.pnl_loading.InvokeRequired)
        {
          this.pnl_base.BeginInvoke(new MethodInvoker(delegate
          {
            this.ShowLoadingPanel(Type);
          }));
        }
        else
        {
          switch (Type)
          {
            case TYPE_LOADING.SHUTDOWN:
              //this.ForceShutDown = true;
              break;

            case TYPE_LOADING.VIDEO:
              this.ControlsLoaded = false;
              this.UpdateLastActivity();
              break;
          }

          this.pnl_loading.Visible = true;
          this.pnl_loading.BringToFront();

          if (this.MainVideo != null)
          {
            this.MainVideo.Close(Type == TYPE_LOADING.SHUTDOWN);
          }

          this.UpdateMarqueeMessage();
        }
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    }

    public void ShowJackpotPanel()
    {
      try
      {
        if (this.pnl_jackpot.InvokeRequired)
        {
          this.pnl_base.BeginInvoke(new MethodInvoker(delegate
          {
            this.ShowJackpotPanel();
          }));
        }
        else
        {
          this.pnl_jackpot.Visible = true;
          this.pnl_jackpot.BringToFront();

          if (this.MainVideo != null)
          {
            this.MainVideo.Close();
          }

          this.UpdateMarqueeMessage();
        }
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: To set the application logo.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    public void SetApplicationLogo()
    {
      String _logo_name;
      _logo_name = "";
      try
      {
        if (File.Exists(Constants.FILENAME_LOGO_INIT))
        {
          _logo_name = Constants.FILENAME_LOGO_INIT;
        }
        if (File.Exists(Constants.FILENAME_LOGO))
        {
          //File.Copy(Constants.LOGO_NAME, Constants.LOGO_INIT_NAME, true);
          _logo_name = Constants.FILENAME_LOGO;
        }

        if (!string.IsNullOrEmpty(_logo_name))
        {
          Image _image;
          _image = null;

          using (FileStream stream = new FileStream(_logo_name, FileMode.Open, FileAccess.Read))
          {
            _image = Image.FromStream(stream);

            // Do something with the image.
          }
          this.pnl_logo_top.BackgroundImage = _image;//Image.FromFile(Constants.LOGO_INIT_NAME);

          return;
        }
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    }

    ////------------------------------------------------------------------------------
    //// PURPOSE: To initialize the jackpot timer.
    //// 
    ////  PARAMS:
    ////      - INPUT:
    ////          - Int32 Interval.
    ////
    ////      - OUTPUT:
    ////
    //// RETURNS:
    ////
    //public void StartJackpotTimer()
    //{
    //  try
    //  {
    //    if (this.JackpotInterval == 0)
    //    {
    //      this.JackpotInterval = this.DefaultTimerIntervalJackport;
    //    }
    //  }
    //  catch (Exception _ex)
    //  {
    //    Log.Add(_ex);
    //  }
    //} // StartJackpotTimer

    //------------------------------------------------------------------------------
    // PURPOSE: Show / Hide Main controls.
    // 
    //  PARAMS:
    //      - INPUT:
    //          · Nothing.
    //
    //      - OUTPUT:
    //          · Nothing.
    //
    // RETURNS: Nothing.
    //
    internal void ShowHideMainControls()
    {
      try
      {
        this.EnableDisableBaseButtons();
        this.ShowHideLanguagePanel();
        this.ShowHideMachineName();
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Show / Hide Main controls.
    // 
    //  PARAMS:
    //      - INPUT:
    //          · Nothing.
    //
    //      - OUTPUT:
    //          · Nothing.
    //
    // RETURNS: Nothing.
    //
    internal void ShowHideMachineName()
    {
      //Boolean _visible;

      //try
      //{
      //  _visible = (this.CurrentBody == TYPE_BODY.VOID
      //              || this.CurrentBody == TYPE_BODY.MAIN
      //              || this.CurrentBody == TYPE_BODY.ACCOUNT_MENU
      //              || this.CurrentBody == TYPE_BODY.GIFT_MENU
      //              || this.CurrentBody == TYPE_BODY.PROMOTION_MENU
      //              || this.CurrentBody == TYPE_BODY.BROWSER);

      //  SafeInvokeExt.SetPropertyValue(this.lbl_egm_name, "Visible", _visible);
      //}
      //catch (Exception _ex)
      //{
      //  Log.Add(_ex);
      //}
    }

    //------------------------------------------------------------------------------
    // PURPOSE: To set enabled/disabled the back button.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Boolean Enable.
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    internal void EnableDisableBaseButtons()
    {
      //Boolean _visible;

      //try
      //{
      //  _visible = (this.IsNotTechnicalScreen()
      //            && this.IsInsertedCardValidated
      //            && this.CurrentBody != TYPE_BODY.MAIN);

      //  SafeInvokeExt.SetPropertyValue(this.pnl_menu_mid, "Visible", _visible);
      //  SafeInvokeExt.SetPropertyValue(this.btn_back, "IsEnabled", !this.Message.IsShowingMessage);
      //  SafeInvokeExt.SetPropertyValue(this.btn_menu, "IsEnabled", !this.Message.IsShowingMessage);
      //}
      //catch (Exception _ex)
      //{
      //  Log.Add(_ex);
      //}
    } // EnableDisableBaseButtons

    //------------------------------------------------------------------------------
    // PURPOSE: To initialize the block reasons.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Boolean Enable.
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    public void ShowBlockReasons()
    {
      try
      {
        if (this.InTouch != null)
        {
          this.SetMarqueeMessage(this.InTouch.BlockReason);
        }
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // ShowBlockReasons

    //------------------------------------------------------------------------------
    // PURPOSE: Set form position
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    public void SetFormLocation()
    {
      this.StartPosition = FormStartPosition.Manual;

      if (this.InTouch != null
        && !Functions.SetLocation(this, this.InTouch.Config))
      {
        if (!System.Diagnostics.Debugger.IsAttached)
        {
          this.Location = new System.Drawing.Point(0, -this.pnl_top.Height);
        }
        else
        {
          this.Location = new System.Drawing.Point(0, 0);
        }
      }
    } // Set position

    //------------------------------------------------------------------------------
    // PURPOSE: To check if it's necesary to show the marquee.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    public void ShowMarquee()
    {
      ////Log.Add(TYPE_MESSAGE.DEVELOPMENT, "ShowMarquee " + DateTime.Now);
      //if (this.Marquee.InvokeRequired)
      //{
      //  this.Marquee.BeginInvoke(new MethodInvoker(delegate
      //  {
      //    // Executes the following code on the GUI thread.
      //    this.ShowMarquee();
      //  }));
      //}
      //else
      //{
      //  this.SetMarqueeLocation();

      //  if (this.lbl_msg.Text != this.MarqueeText)
      //  {
      //    this.lbl_msg.Font = this.SetMarqueeFontSize();
      //    this.SetText(this.lbl_msg, this.MarqueeText);
      //  }
      //  this.Marquee.Visible = true;
      //  this.Marquee.BringToFront();

      //  this.Message.SendToBack();
      //  this.MessageBringToFront = true;
      //}
    } // ShowMarquee

    ////------------------------------------------------------------------------------
    //// PURPOSE: Hide the marquee.
    //// 
    ////  PARAMS:
    ////      - INPUT:
    ////
    ////      - OUTPUT:
    ////
    //// RETURNS:
    ////
    //public void HideMarquee()
    //{
    //  try
    //  {
    //    SafeInvokeExt.SetPropertyValue(this.Marquee, "Visible", false);
    //    SafeInvokeExt.InvokeMethodCtrl(this.Marquee, "SendToBack", null);
    //  }
    //  catch (Exception _ex)
    //  {
    //    Log.Add(_ex);
    //  }
    //}

    //------------------------------------------------------------------------------
    // PURPOSE: Player login
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    public void PlayerLogin(String HolderName)
    {
      //String _welcome_message;
      //Boolean _show_request_pin;

      //_show_request_pin = false;
      //try
      //{
      //  this.AnonymousPlayer = (HolderName.Trim() == String.Empty);

      //  if (this.CurrentBody == TYPE_BODY.MAIN || CheckShowRequestPin(out _show_request_pin))
      //  {
      //    _welcome_message = Resource.StringFormat("MESSAGE_WELCOME", new String[] { HolderName });

      //    this.Body.Main.SetButtonAccountVisibility(true);

      //    if (this.MainVideo != null)
      //    {
      //      this.MainVideo.Close();
      //    }
      //    if (_show_request_pin)
      //    {
      //      this.BindContainer(TYPE_BODY.MAIN);
      //    }

      //    // AJQ Card just inserted, go directly to request pin
      //    if (this.KeyBoardEnabled)
      //    {
      //      return;
      //    }

      //    this.Body.Main.Bind();
      //  }
      //}
      //catch (Exception _ex)
      //{
      //  Log.Add(_ex);
      //}
    }

    private Boolean CheckShowRequestPin(out Boolean ShowRequestPin)
    {
      ShowRequestPin = false;
      if (this.ShowRequestPin == STATUS_REQUEST_PIN.REQUEST)
      {
        ShowRequestPin = true;
        this.ShowRequestPin = STATUS_REQUEST_PIN.END_REQUEST; //false;
        return true;
      }
      return false;
    } // PlayerLogin

    //internal void EnableLoadingPictures(Boolean Enable)
    //{
    //  try
    //  {
    //    if (this.InvokeRequired)
    //    {
    //      this.BeginInvoke(new MethodInvoker(delegate
    //      {
    //        this.EnableLoadingPictures(Enable);
    //      }));
    //    }
    //    else
    //    {
    //      switch (this.BrowserTarget)
    //      {
    //        case TARGET_BROWSER.MAIN:
    //          this.LoadingMain(Enable); // TODO CHANGE FOR MAIN BROWSER
    //          break;

    //        case TARGET_BROWSER.FB:
    //          this.LoadingFB(Enable);
    //          break;

    //        case TARGET_BROWSER.TERMINAL_DRAW:
    //          this.LoadingTerminalDraw(Enable);
    //          break;

    //        case TARGET_BROWSER.GAMEGATEWAY:
    //          this.LoadingPimPamGO(Enable);
    //          break;
    //      }
    //    }
    //  }
    //  catch (Exception _ex)
    //  {
    //    Log.Add(_ex);
    //  }
    //}

    /// <summary>
    /// Loading screen of pimpamgo
    /// </summary>
    private void LoadingPimPamGO(Boolean Enable)
    {
      //this.pb_loading_browser.Visible = Enable && (this.BrowserForm == null || this.BrowserForm.Opacity != Constants.OPACITY_SHOW);

      //if (this.pb_loading_browser.Visible)
      //{
      //  this.pb_loading_browser.BackgroundImage = LCDInTouch.Properties.Resources.Pantalla2espera;
      //  this.pb_loading_browser.BringToFront();
      //}
      //else
      //{
      //  this.pb_loading_browser.BackgroundImage = null;
      //  this.pb_loading_browser.SendToBack();
      //}
    } // LoadingPimPamGO

    /// <summary>
    /// Loading screen of Main Web
    /// </summary>
    private void LoadingMain(Boolean Enable)
    {
      //// TODO REVISE ALL
      //this.pb_loading_browser.Visible = Enable && (this.BrowserForm == null || this.BrowserForm.Opacity != Constants.OPACITY_SHOW);

      //if (this.pb_loading_browser.Visible)
      //{
      //  this.pb_loading_browser.BackgroundImage = LCDInTouch.Properties.Resources.Pantalla2espera_FB;
      //  this.pb_loading_browser.BringToFront();
      //}
      //else
      //{
      //  this.pb_loading_browser.BackgroundImage = null;
      //  this.pb_loading_browser.SendToBack();
      //}
    } // LoadingPimPamGO

    /// <summary>
    /// Loading screen of pimpamgo
    /// </summary>
    private void LoadingFB(Boolean Enable)
    {
      //this.pb_loading_browser.Visible = Enable && (this.BrowserForm == null || this.BrowserForm.Opacity != Constants.OPACITY_SHOW);

      //if (this.pb_loading_browser.Visible)
      //{
      //  this.pb_loading_browser.BackgroundImage = LCDInTouch.Properties.Resources.Pantalla2espera_FB;
      //  this.pb_loading_browser.BringToFront();
      //}
      //else
      //{
      //  this.pb_loading_browser.BackgroundImage = null;
      //  this.pb_loading_browser.SendToBack();
      //}
    } // LoadingPimPamGO

    private void LoadingTerminalDraw(Boolean Enable)
    {
      //this.pb_loading_browser.Visible = Enable && (this.BrowserForm == null || this.BrowserForm.Opacity != Constants.OPACITY_SHOW);

      //if (this.pb_loading_browser.Visible)
      //{
      //  this.pb_loading_browser.BackgroundImage = LCDInTouch.Properties.Resources.Pantalla3espera_Terminal_Draw;
      //  this.pb_loading_browser.BringToFront();
      //}
      //else
      //{
      //  this.pb_loading_browser.BackgroundImage = null;
      //  this.pb_loading_browser.SendToBack();
      //}
    } // LoadingTerminalDraw

    #endregion

    #region Private Functions

    //------------------------------------------------------------------------------
    // PURPOSE: Initialize timer to hide the call attendant button.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Int32 Interval.
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    private void InitHideCallAttendant(Int32 Interval)
    {
      //try
      //{
      //  this.TimerCallAttendant.Tick += new EventHandler(this.HideCallAttendant_Tick);
      //  this.TimerCallAttendant.Interval = Interval;
      //  this.TimerCallAttendant.Start();
      //}
      //catch (Exception _ex)
      //{
      //  Log.Add(_ex);
      //}
    } // InitHideCallAttendant

    //------------------------------------------------------------------------------
    // PURPOSE: Timer Tick event to show keyboard.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - object sender.
    //          - EventArgs e.
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    private void RestartTimeBetweenClicks(object sender, EventArgs e)
    {
      this.TimeBetweenClicks.Stop();
      this.PanelLogoBotClick = false;
      this.PanelLogoTopClick = false;
    } // RestartTimeBetweenClicks

    //------------------------------------------------------------------------------
    // PURPOSE: Timer tick event to show Jackpot.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - object sender.
    //          - EventArgs e.
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    private void TimerTickShowJackpot(object sender, EventArgs e)
    {
      //try
      //{
      //  // TODO: Usar un timer expecifico para controlar el timeout de inactividad.
      //  this.BindContainer(TYPE_BODY.MAIN);
      //}
      //catch (Exception _ex)
      //{
      //  Log.Add(_ex);
      //}
    } // TimerTickShowJackpot

    //------------------------------------------------------------------------------
    // PURPOSE: Function to check if in correct Body to ask for account info.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT: False if not possible ask parameters
    //
    // RETURNS:
    //
    private Boolean CheckBody_AccountInfo()
    {
      Boolean _check_account_info;
      _check_account_info = false;
      //try
      //{
      //  switch (this.CurrentBody)
      //  {
      //    case TYPE_BODY.OPERATOR:
      //    case TYPE_BODY.KEYBOARD:
      //    case TYPE_BODY.LOGIN:
      //    case TYPE_BODY.VIDEO:
      //    case TYPE_BODY.SHUTDOWN:
      //      _check_account_info = false;
      //      break;
      //    case TYPE_BODY.MAIN:
      //      if (this.IsInsertedCardValidated)
      //      {
      //        _check_account_info = true;
      //      }
      //      else
      //      {
      //        _check_account_info = false;
      //      }
      //      break;
      //    default:
      //      _check_account_info = true;
      //      break;
      //  }
      //}
      //catch (Exception _ex)
      //{
      //  Log.Add(_ex);
      //}
      return _check_account_info;
    }

    internal override void StartVideo()
    {
      base.StartVideo();

      try
      {
        this.ChangeOnKeyboard = KB_TYPE.EXIT;

        if (this.MainVideo == null)
        {
          this.MainVideo = new Video(this.pnl_video);
        }

        this.ShowRequestPin = STATUS_REQUEST_PIN.NONE; //false
        this.MainVideo.SetVideoBackground();
        this.MainVideo.ShowHideVideoPanel();
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    }

    internal void SetVLC_Plugin()
    {
      String[] _vlc_option;
      Uri _uri;

      try
      {
        if (this.VLC_Plugin != null
          && this.VLC_Plugin.playlist.items.count == 0)
        {
          _vlc_option = new String[] { ":aspect-ratio=" + this.VLC_Plugin.Width + ":" + this.VLC_Plugin.Height };

          if (this.InTouch != null
            && File.Exists(this.InTouch.Video.CurrentVideo))
          {
            _uri = new Uri(Application.StartupPath + "/" + this.InTouch.Video.CurrentVideo);
          }
          else
          {
            this.MainVideo.Close();
            return;
          }

          // Load the video.
          this.VLC_Plugin.playlist.add(_uri.AbsoluteUri, " ", _vlc_option);

          // Mark initialized
          this.MainVideo.Initialized = true;
        }
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    }

    public void ClearPlayList()
    {
      try
      {
        if (this.VLC_Plugin != null
          && this.VLC_Plugin.playlist.items.count > 0)
        {
          this.VLC_Plugin.playlist.stop();
          this.VLC_Plugin.playlist.clear();
        }
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    }

    internal void SetNextVideoFile()
    {
      LCD_RESOURCE _video;

      try
      {
        if (this.InTouch != null)
        {
          _video = this.InTouch.Video;

          // If default video exist, always exist video.
          _video.Exists = this.DefaultVideoInitialized;

          if (File.Exists(_video.NextVideo))
          {
            _video.CurrentVideo = _video.NextVideo.Replace(Constants.FILENAME_VIDEO_NEXT, String.Empty);
            _video.Exists = true;
            _video.IsDefault = false;
          }
          else
          {
            ;
          }

          if (_video.Exists)
          {
            this.ClearPlayList();

            Functions.RenameFile(_video.NextVideo, _video.CurrentVideo);
          }

          this.InTouch.Video = _video;
        }
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    }

    internal void InitVideo()
    {
      try
      {
        this.SetNextVideoFile();
        this.SetVLC_Plugin();
        this.PlayVideo();
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
      finally
      {
        this.MainVideo.Initializing = false;
      }
    }

    internal void ExitKeyboard()
    {
      this.ExitKeyboard(this.IsSpecialCard);
    }
    internal void ExitKeyboard(Boolean ForceESCLogout)
    {
      this.ExitKeyboard(ForceESCLogout, true);
    }

    internal void ExitKeyboard(Boolean ForceESCLogout, Boolean SendActivity)
    {
      //if (!this.KeyBoardEnabled)
      //{
      //  return;
      //}

      //this.NextMessageIsMobileBankResponse = false;
      //this.PendingLogoutOperatorPanel = false;
      //this.PanelLogoBotClick = false;
      //this.KeyboardManualEntry = false;
      //this.ChangeOnKeyboard = KB_TYPE.EXIT;
      //this.Body.Keyboard.HideOperatorButtons(ForceESCLogout);

      //if (SendActivity)
      //{
      //  this.UpdateLastActivity();

      //  this.LCD.SendProtocolHandler(TYPE_REQUEST.LCD_ACTIVITY, String.Empty);
      //}

      //this.BindContainer(TYPE_BODY.VOID);
    }

    internal void LCD_BringToFront()
    {
      if (!this.ControlsLoaded)
      {
        return;
      }
      if (this.InvokeRequired)
      {
        this.BeginInvoke(new MethodInvoker(delegate
        {
          this.LCD_BringToFront();
        }));
      }
      else
      {
        //if (!Debugger.IsAttached)
        //{
        this.BringToFront();
        this.Focus();
        this.Activate();
        //}
      }
    }

    #region Activity Methods

    internal void LoadingPanel_Activity()
    {
      try
      {
        if (!this.ControlsLoaded
          && this.Activities.IsExpired(TYPE_ACTIVITY.LoadingPanel, Constants.LOADING_TIME))
        {
          this.LCD_BringToFront();
          this.ControlsLoaded = true;

          if (this.InTouch.Video.Exists)
          {
            if (this.MainVideo == null)
            {
              return;
            }

            this.MainVideo.ShowHideVideoPanel();
          }

          this.pnl_loading.Visible = false;
          this.pnl_loading.SendToBack();
        }
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    }

    internal void Jackpot_Activity()
    {
      //try
      //{
      //  if (this.JackpotIsVisible
      //    && this.Activities.IsExpired(TYPE_ACTIVITY.Jackpot, Constants.JACKPOT_TIME_CLOSE))
      //  {
      //    this.CloseJackpot();
      //  }
      //}
      //catch (Exception _ex)
      //{
      //  Log.Add(_ex);
      //}
    }

    internal void AccountInfo_Activity()
    {
      try
      {
        if (this.IsInsertedCardValidated && (this.TypeCardMode == TYPE_CARD.PLAYER || this.TypeCardMode == TYPE_CARD.PLAYER_PIN) &&
            this.Activities.IsExpired(TYPE_ACTIVITY.AccountInfo, Constants.DEFAULT_ACCOUNT_INFO_TIME))
        {
          if (!this.CheckBody_AccountInfo())
          {
            this.Account.PinValidated = false;

            return;
          }
          //if ( (this.Account.PinValidated && !this.Account.BackgroundGetInfo)
          // FJC & JCA 29-FEB-2016 Validation with Center without PIN.
          if ((this.Account.PinValidated && !this.Account.BackgroundGetInfo)
              || (this.InTouch.IsGameGateway))
          {
            this.Account.BackgroundGetInfo = true;
            //JCA change to new call
            //this.GetAccountInfo(false, false);
            this.InTouch.SendProtocolHandler(TYPE_REQUEST.GET_PROMO_BALANCE, ((Int16)ENUM_GAME_GATEWAY_PROMO_BALANCE_ACTION.GAME_GATEWAY_PROMO_BALANCE_ACTION_WITHOUT_PIN).ToString());
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    }

    /// <summary>
    /// 
    /// </summary>
    internal void Browser_Activity()
    {
      try
      {
        this.BrowserStatusManager();
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    }

    /// <summary>
    /// 
    /// </summary>
    internal void AutoShowHideVideo_Activity()
    {
      //try
      //{
      //  if (this.Activities.IsExpired(TYPE_ACTIVITY.AutoShowHideVideo, Constants.DEFAULT_SHOW_VIDEO_TIME, false))
      //  {
      //    // 12-JUL-2015 FJC    Product Backlog Item 282.
      //    if ((!(this.IsInsertedCardValidated && (TypeCardMode == TYPE_CARD.TECH || TypeCardMode == TYPE_CARD.TECH_PIN))) && (this.IsNotTechnicalScreen()))
      //    {
      //      // Set keyboard to default. WIG-2202
      //      this.ExitKeyboard(false, false);
      //    }

      //    if (this.VideoIsPlaying)
      //    {
      //      if (this.Activities.IsExpired(TYPE_ACTIVITY.AutoShowHideVideo, Constants.DEFAULT_HIDE_VIDEO_TIME))
      //      {
      //        this.UpdateLastActivity();
      //        this.MainVideo.Close();
      //      }
      //    }
      //    else
      //    {
      //      this.UpdateLastActivity();
      //      if (!this.IsInsertedCardValidated)
      //      {
      //        this.BindContainer(TYPE_BODY.VIDEO);
      //      }
      //    }
      //  }
      //}
      //catch (Exception _ex)
      //{
      //  Log.Add(_ex);
      //}
    }

    internal void BringToFront_Activity()
    {
      try
      {
        if (!Debugger.IsAttached
          && this.Activities.IsExpired(TYPE_ACTIVITY.BringToFront, Constants.DEFAULT_LCD_BRINGTOFRONT))
        {
          this.LCD_BringToFront();
        }
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    }

    internal void LogoutMobileBank()
    {
      //try
      //{
      //  this.TypeCardMode = TYPE_CARD.UNKNOWN;

      //  this.NextMessageIsMobileBankResponse = false;
      //  this.PendingLogoutOperatorPanel = false;

      //  this.ExitKeyboard();
      //  this.BindContainer(TYPE_BODY.VOID);

      //  this.LCD.InitializeTimeToAskCardStatus();

      //  System.Threading.Thread.Sleep(500);
      //}
      //catch (Exception _ex)
      //{
      //  Log.Add(_ex);
      //}
    }

    #endregion

    #endregion

    #region GameGateWay Functions

    //------------------------------------------------------------------------------
    // PURPOSE: Hide show main panel
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS: void 
    // 
    internal void ShowHideLogoPanel(Boolean Visibility)
    {
      SafeInvokeExt.SetPropertyValue(this.pnl_menu, "Visible", Visibility);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Browser Status Manager
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS: void 
    // 
    internal void BrowserStatusManager()
    {
      switch (this.BrowserState)
      {
        case STATUS_BROWSER.INIT:
          
          if (this.BrowserForm == null)
          {
            this.InstanceBrowser();
          }
          else if (this.NavigateBrowser())
          {
            this.BrowserState = STATUS_BROWSER.LOADING;
          }

          break;

        case STATUS_BROWSER.LOADING:
          if (!this.StartMainBrowser())
          {
            return;
          }

          this.ShowBrowser();
          this.BrowserState = STATUS_BROWSER.COMPLETED;

          break;

        case STATUS_BROWSER.COMPLETED:
          if (this.BrowserForm != null && this.BrowserForm.Ready)
          {
            this.BrowserState = STATUS_BROWSER.RUNNING;
          }
          break;

        case STATUS_BROWSER.RUNNING:
          // Browser is running
          break;

        case STATUS_BROWSER.CLOSING:
          
          if (!this.CloseBrowserManager())
          {
            return;
          }

          // End player session if ExtractCard
          if ( this.PendingEndPlayerSession && !this.ClosePlayerSession())
          {
            return;
          }

          if (this.BrowserForm == null)
          {
            this.BrowserState = STATUS_BROWSER.HIDE;
          }
          break;

        case STATUS_BROWSER.HIDE:
          // TODO: ¿Anything?
          this.BrowserState = STATUS_BROWSER.END;
          break;

        case STATUS_BROWSER.END:
          if (!this.IsInsertedCardValidated)
          {
            this.SASParams = null;
            this.InTouch.PlayerRequestBalance = new LCD_PLAYER_REQUEST_BALANCE();
          }

          this.BrowserState = STATUS_BROWSER.IDLE;
          break;

        default:
        case STATUS_BROWSER.IDLE:
          if (!this.IsInsertedCardValidated && this.BrowserForm != null)
          {
            this.StopBrowser();
          }

          // RELAUNCH BROWSER
          this.BrowserState = STATUS_BROWSER.INIT;
          break;
      }

      //Application.DoEvents();
    } // BrowserStatusManager

    /// <summary>
    /// Start navigation
    /// </summary>
    /// <returns></returns>
    private Boolean StartNavigation()
    {
      switch (this.BrowserTarget)
      {
        case TARGET_BROWSER.MAIN:

          if (!this.StartMainBrowser())
          {
            return false;
          }
          break;

        case TARGET_BROWSER.GAMEGATEWAY:

          if (!this.StartGameGateWay())
          {
            return false;
          }
          break;

        case TARGET_BROWSER.FB:
          if (!this.StartFB())
          {
            return false;
          }
          break;
        case TARGET_BROWSER.TERMINAL_DRAW:
          if (!this.StartTerminal_Draw())
          {
            return false;
          }
          break;
      }

      return true;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Builts Url with SAS Params concatenades
    // 
    //  PARAMS:
    //      - INPUT:
    //          · Object SAS_PARAMS
    //
    //      - OUTPUT:
    //          · Nothing.
    //
    // RETURNS: String 
    // 
    public String BuildUrl_GameGateway()
    {
      String _sessionID = null;
      String _language = null;
      String _underscore = "_";

      try
      {
        _sessionID = String.Format(this.SASParams.PlaySessionID + "{0}"
                                 + this.SASParams.AccountID + "{0}"
                                 + this.SASParams.TerminalId + "{0}"
                                 + this.SASParams.PartnerId, _underscore);

        _language = this.InTouch.GetLanguageName(true);
      }

      catch (Exception _ex)
      {
        Log.Add(_ex);
      }

      return String.Format(this.SASParams.Url, _sessionID, _language);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Checks if any param not set value (Gamegateway)
    // 
    //  PARAMS:
    //      - INPUT:
    //          · Object SAS_PARAMS
    //
    //      - OUTPUT:
    //          · Nothing.
    //
    // RETURNS: Boolean 
    // 
    public Boolean HasNavigateParams_Gamegateway()
    {
      try
      {
        if (this.SASParams == null)
        {
          return false;
        }

        if (Convert.ToUInt64(this.SASParams.AccountID) == 0)
        {
          return false;
        }
        //if (Convert.ToUInt16(this.SASParams.Language) == 0)
        //{
        //   return false;
        //}
        if (Convert.ToUInt64(this.SASParams.PlaySessionID) == 0)
        {
          return false;
        }
        if (Convert.ToUInt32(this.SASParams.PartnerId) == 0)
        {
          return false;
        }
        if (String.IsNullOrEmpty(this.SASParams.ProviderName))
        {
          return false;
        }
        if (Convert.ToUInt32(this.SASParams.TerminalId) == 0)
        {
          return false;
        }
        if (String.IsNullOrEmpty(this.SASParams.TerminalName))
        {
          return false;
        }
        if (String.IsNullOrEmpty(this.SASParams.Url))
        {
          return false;
        }

        return true;
      }
      catch (Exception _exception)
      {
        Log.Add(_exception);
      }

      return false;
    } // HasNavigateParams_Gamegateway

    /// <summary>
    /// Show Browser
    /// </summary>
    public void ShowBrowser()
    {
      try
      {
        if (this.BrowserForm.InvokeRequired)
        {
          this.BrowserForm.BeginInvoke(new MethodInvoker(delegate
         {
           this.ShowBrowser();
         }));
        }
        else
        {
          this.BrowserForm.Opacity = (this.BrowserForm.DocumentCompleted) ? Constants.OPACITY_SHOW : Constants.OPACITY_HIDE;
          this.BrowserForm.Location = this.Location;
          this.BrowserForm.WindowState = FormWindowState.Normal;
          this.BrowserForm.Show();
          this.BrowserForm.Location = this.Location;
          this.BrowserForm.BringToFront();
          this.BrowserForm.TopMost = true;
        }
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: if all params are informed shows form browser
    // 
    //  PARAMS:
    //      - INPUT:
    //          · Object SAS_PARAMS
    //
    //      - OUTPUT:
    //          · Nothing.
    //
    // RETURNS: Nothing 
    // 
    public Boolean NavigateBrowser(String URL)
    {
      try
      {
        return this.BrowserForm.NavigateParams(URL);
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }

      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: if all params are informed shows form browser
    // 
    //  PARAMS:
    //      - INPUT:
    //          · Object SAS_PARAMS
    //
    //      - OUTPUT:
    //          · Nothing.
    //
    // RETURNS: Nothing 
    // 
    public Boolean NavigateBrowser()
    {
      String _final_url;
      _final_url = String.Empty;

      try
      {
        if (this.BrowserForm.MainForm == null)
        {
          //Delete All files with extension *.swf (if you delete one file, doesn't work ¿?)
          Functions.DeleteFileTempInternetFilesSWF();
          this.BrowserForm.MainForm = this;
        }

        _final_url = this.LoadNavigateParams();

        if (!String.IsNullOrEmpty(_final_url))
        {
          this.BrowserForm.Location = this.Location;
          this.BrowserForm.WindowState = FormWindowState.Normal;

          if (this.NavigateBrowser(_final_url))
          {
            return true;
          }
        }
        else
        {
          this.MessageQueue.Enqueue(MESSAGE_SOURCE.LCD, TYPE_MESSAGE.ERROR, "MESSAGE_NOT_COMPLETED");
          this.EnableButtonsGameGateWay(true);
          this.StopBrowser();
        }
      } // end try
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }

      return false;
    }

    private String LoadNavigateParams()
    {
      switch (this.BrowserTarget)
      {
        case TARGET_BROWSER.MAIN:
        {
          return LoadNavigateParams_MainForm();
        }
        case TARGET_BROWSER.GAMEGATEWAY:
        {
          return LoadNavigateParams_PimPamGO();
        }
        case TARGET_BROWSER.FB:
        {
          return LoadNavigateParams_FB();
        }
        case TARGET_BROWSER.TERMINAL_DRAW:
        {
          return LoadNavigateParams_Terminal_Draw();
        }
      }

      return String.Empty;
    }

    private String LoadNavigateParams_MainForm()
    {
      return DEFAULT_MAIN_URL;
    }

    private String LoadNavigateParams_PimPamGO()
    {
      //if (this.HasNavigateParams_Gamegateway())
      //{
      //  return this.BuildUrl_GameGateway();
      //}
      //if (!String.IsNullOrEmpty(Environment.GetEnvironmentVariable("DEVELOPMENTWEB")))
      //{
      //  return DEFAULT_PIMPAMGO_URL;
      //}

      return String.Empty;
    }

    public void EnableButtonsGameGateWay(Boolean Enabled)
    {
      //try
      //{
      //  if (this.BrowserTarget != TARGET_BROWSER.GAMEGATEWAY)
      //  {
      //    return;
      //  }

      //  if (this.Body.GetBody(this.CurrentBody) != null)
      //  {
      //    this.Body.GetBody(this.CurrentBody).EnableButtonsGameGateway(Enabled);
      //  }
      //}
      //catch (Exception _ex)
      //{
      //  Log.Add(_ex);
      //}
    }

    /// <summary>
    /// Start GameGateway
    /// </summary>
    /// <returns></returns>
    public Boolean StartMainBrowser()
    {
      try
      {
        if (this.BrowserTarget != TARGET_BROWSER.MAIN)
        {
          return true;
        }

        if (!this.NavigateMainBrowser())
        {
          //Error loading Game
          //Enque Error
          this.MessageQueue.Enqueue(MESSAGE_SOURCE.LCD, TYPE_MESSAGE.ERROR, "BONOPLAY_MESSAGE_WEBPAGE_ERROR");

          //Enable Buttons
          this.StopBrowser();

          return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }

      return false;
    }

    /// <summary>
    /// Start GameGateway
    /// </summary>
    /// <returns></returns>
    public Boolean StartGameGateWay()
    {
      //try
      //{
      //  if (this.BrowserTarget != TARGET_BROWSER.GAMEGATEWAY)
      //  {
      //    return true;
      //  }

      //  //Disable Buttons Menu when we are trying to navigate to WebPage
      //  this.EnableButtonsGameGateWay(false);

      //  if (this.CurrentBody == TYPE_BODY.INTERACTIVE_MESSAGE)
      //  {
      //    this.Body.InteractiveMessage.InteractiveMessageTimer.Enabled = true;
      //  }

      //  if (!this.NavigateGameGateway())
      //  {
      //    //Error loading Game
      //    //Enque Error
      //    this.MessageQueue.Enqueue(MESSAGE_SOURCE.LCD, TYPE_MESSAGE.ERROR, "BONOPLAY_MESSAGE_WEBPAGE_ERROR");

      //    //Enable Buttons
      //    this.EnableButtonsGameGateWay(true);
      //    this.StopBrowser();

      //    return false;
      //  }

      //  return true;
      //}
      //catch (Exception _ex)
      //{
      //  //Enable Buttons
      //  this.EnableButtonsGameGateWay(true);
      //  Log.Add(_ex);
      //}

      return false;
    }

    /// <summary>
    /// Navigate Url and check URL is correct and can navigate.
    /// </summary>
    /// <returns></returns>
    public Boolean NavigateMainBrowser()
    {
      String _url_test;

      _url_test = String.Empty;

      try
      {
        _url_test = LoadNavigateParams_MainForm();

        //Start thread transition images (MAIN WEB Loading )
        this.InTouch.StartWebLoadThread(TARGET_BROWSER.MAIN);

        // TODO UNCOMMENT: CUANDO ESTE LISTA LA WEB
        if (Functions.CanNavigate(_url_test))
        {
          return true;
        }

        //SafeInvokeExt.SetPropertyValue(this.pb_loading_browser, "BackgroundImage", null);
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }

      //Stop Tread Web Loading
      this.InTouch.StopWebLoadThread();

      return false;
    } // NavigateMainBrowser

    /// <summary>
    /// Navigate Url and check URL is correct and can navigate.
    /// </summary>
    /// <returns></returns>
    public Boolean NavigateGameGateway()
    {
      String _url_test;

      _url_test = String.Empty;

      try
      {
        if (this.HasNavigateParams_Gamegateway())
        {
          _url_test = this.SASParams.Url;
          if (!String.IsNullOrEmpty(this.SASParams.UrlTest))
          {
            _url_test = this.SASParams.UrlTest;
          }
          else
          {
            _url_test = this.BuildUrl_GameGateway();
          }

          //Start thread transition images (Web PIM PAM GO!!! Loading )
          this.InTouch.StartWebLoadThread(TARGET_BROWSER.GAMEGATEWAY);

          if (Functions.CanNavigate(_url_test))
          {
            return true;
          }

          SafeInvokeExt.SetPropertyValue(this.pb_loading_browser, "BackgroundImage", null);
        }
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }

      //Stop Tread Web Loading
      this.InTouch.StopWebLoadThread();

      return false;
    }

    /// <summary>
    /// Instance Browser for gamegateway
    /// </summary>
    public void InstanceBrowser()
    {
      try
      {
        if (this.InvokeRequired)
        {
          this.BeginInvoke(new MethodInvoker(delegate
         {
           this.InstanceBrowser();
         }));
        }
        else
        {
          if (this.BrowserForm == null || this.BrowserForm.IsDisposed)
          {
            this.InTouch.Config = new LCDConfiguration(this.InTouch);// TODO REVISE: Se debe setear en otro punto.
            this.BrowserForm = new frm_browser(this);
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    }

    /// <summary>
    /// UnInstance Browser for gamegateway
    /// </summary>
    public void UnInstanceBrowser()
    {
      try
      {
        if (this.InvokeRequired)
        {
          this.BeginInvoke(new MethodInvoker(delegate
         {
           this.UnInstanceBrowser();
         }));
        }
        else
        {
          if (this.BrowserForm != null)
          {
            if (!this.BrowserForm.IsDisposed)
            {
              this.BrowserForm.Dispose();
            }
            this.BrowserForm = null;
          }
        }

      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    }

    /// <summary>
    /// Instance Browser for gamegateway
    /// </summary> 
    public void CloseFrmBrowser()
    {
      try
      {
        if (this.InvokeRequired)
        {
          this.BeginInvoke(new MethodInvoker(delegate
         {
           this.CloseFrmBrowser();
         }));
        }
        else
        {
          if (this.BrowserForm != null && !this.BrowserForm.IsDisposed)
          {
            this.BrowserForm.Close();
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    }

    #endregion

    #region "FB Functions"

    private String LoadNavigateParams_FB()
    {
      String _final_url = String.Empty;

      //if (this.SASParams == null)
      //{
      //  Log.Add(TYPE_MESSAGE.INFO, "LoadNavigateParams_FB: SASParams is NULL");

      //  return String.Empty;
      //}

      //if (this.HasNavigateParams_FB())
      //{
      //  _final_url = String.Format(this.SASParams.FBParams.Url, this.SASParams.AccountID, this.SASParams.TerminalId);
      //}
      //else
      //{
      //  _final_url = String.Format(DEFAULT_FB_URL_LOCALHOST, this.SASParams.AccountID, this.SASParams.TerminalId);
      //}

      //Log.Add(TYPE_MESSAGE.DEVELOPMENT_LVL1, _final_url);

      return _final_url;
    } // LoadNavigateParams_FB

    /// <summary>
    /// Navigate Url and check URL is correct and can navigate (FB).
    /// </summary>
    /// <returns></returns>
    public Boolean NavigateFB()
    {
      String _url_test;

      _url_test = String.Empty;

      try
      {
        _url_test = this.LoadNavigateParams_FB();

        //Start thread transition images (Web FB Loading)
        this.InTouch.StartWebLoadThread(TARGET_BROWSER.FB);

        if (Functions.CanNavigate(_url_test))
        {
          return true;
        }

        SafeInvokeExt.SetPropertyValue(this.pb_loading_browser, "BackgroundImage", null);
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }

      //Stop Tread Web Loading
      this.InTouch.StopWebLoadThread();

      return false;
    } // NavigateFB

    public Boolean StartFB()
    {
      //try
      //{
      //  if (this.BrowserTarget != TARGET_BROWSER.FB)
      //  {
      //    return true;
      //  }

      //  if (this.CurrentBody == TYPE_BODY.INTERACTIVE_MESSAGE)
      //  {
      //    this.Body.InteractiveMessage.InteractiveMessageTimer.Enabled = true;
      //  }

      //  if (!this.NavigateFB())
      //  {
      //    //Error loading service
      //    //Enque Error
      //    this.MessageQueue.Enqueue(MESSAGE_SOURCE.LCD, TYPE_MESSAGE.ERROR, "FB_MESSAGE_WEBPAGE_ERROR");
      //    this.StopBrowser();

      //    return false;
      //  }
      //  return true;
      //}
      //catch (Exception _ex)
      //{
      //  Log.Add(_ex);
      //}

      return false;
    } // StartFB

    /// <summary>
    /// Checks if any param not set value (FB)
    /// </summary>
    /// <returns></returns>
    public Boolean HasNavigateParams_FB()
    {
      if (this.SASParams == null)
      {
        return false;
      }

      if (Convert.ToUInt32(this.SASParams.FBParams.Enabled) == 0)
      {
        return false;
      }
      if (String.IsNullOrEmpty(this.SASParams.FBParams.HostAddress))
      {
        return false;
      }
      if (String.IsNullOrEmpty(this.SASParams.FBParams.Url))
      {
        return false;
      }
      if (String.IsNullOrEmpty(this.SASParams.FBParams.Login))
      {
        return false;
      }
      if (String.IsNullOrEmpty(this.SASParams.FBParams.Password))
      {
        return false;
      }

      return true;

    } // HasNavigateParams_FB

    #endregion

    #region "Terminal Draw Functions"
    private String LoadNavigateParams_Terminal_Draw()
    {
      String _final_url;

      if (this.TerminalDrawNavigationParams == null)
      {
        Log.Add(TYPE_MESSAGE.INFO, "LoadNavigateParams_Terminal_Draw: TerminalDrawNavigationParams is NULL");

        return String.Empty;
      }

      if (this.HasNavigateParams_Terminal_Draw())
      {
        _final_url = TerminalDrawNavigationParams.Url.Replace("@LANG", this.InTouch.GetLanguageName(true).Substring(0, 2)) + (!string.IsNullOrEmpty(TerminalDrawNavigationParams.Params) ? "?" : "") + TerminalDrawNavigationParams.Params;
      }
      else
      {
        _final_url = string.Empty;
      }

      Log.Add(TYPE_MESSAGE.DEVELOPMENT_LVL1, _final_url);

      return _final_url;
    }// LoadNavigateParams_Terminal_Draw

    public Boolean NavigateTerminal_Draw()
    {
      String _url_test;

      _url_test = String.Empty;

      try
      {
        _url_test = this.LoadNavigateParams_Terminal_Draw();

        //Start thread transition images (Web FB Loading)
        this.InTouch.StartWebLoadThread(TARGET_BROWSER.TERMINAL_DRAW);

        if (Functions.CanNavigate(_url_test))
        {
          return true;
        }

        SafeInvokeExt.SetPropertyValue(this.pb_loading_browser, "BackgroundImage", null);
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }

      //Stop Tread Web Loading
      this.InTouch.StopWebLoadThread();

      return false;
    } // NavigateTerminal_Draw

    public Boolean StartTerminal_Draw()
    {
      //try
      //{
      //  if (this.BrowserTarget != TARGET_BROWSER.TERMINAL_DRAW)
      //  {
      //    return true;
      //  }

      //  if (this.CurrentBody == TYPE_BODY.INTERACTIVE_MESSAGE)
      //  {
      //    this.Body.InteractiveMessage.InteractiveMessageTimer.Enabled = true;
      //  }

      //  if (!this.NavigateTerminal_Draw())
      //  {
      //    //Error loading service
      //    //Enque Error
      //    this.MessageQueue.Enqueue(MESSAGE_SOURCE.LCD, TYPE_MESSAGE.ERROR, "TERMINAL_DRAW_MESSAGE_WEBPAGE_ERROR");
      //    this.StopBrowser();

      //    return false;
      //  }
      //  return true;
      //}
      //catch (Exception _ex)
      //{
      //  Log.Add(_ex);
      //}

      return false;
    }// StartTerminal_Draw

    public Boolean HasNavigateParams_Terminal_Draw()
    {
      if (this.TerminalDrawNavigationParams == null)
      {
        return false;
      }

      if (string.IsNullOrEmpty(this.TerminalDrawNavigationParams.Url))
      {
        return false;
      }
      if (this.TerminalDrawNavigationParams.Timeout == default(long))
      {
        return false;
      }
      return true;
    } // HasNavigateParams_Terminal_Draw

    #endregion

    #region Events

    private void frm_main_Load(object sender, EventArgs e)
    {
      //this.LoadBodys();

      //this.PreviousBody = TYPE_BODY.VOID;
      //this.CurrentBody = TYPE_BODY.VOID;

      this.ShowBlockReasons();
    } // frm_main_Load

    private void pnl_logo_bot_Click(object sender, EventArgs e)
    {
      //Int16 _interval;
      this.UpdateLastActivity();
      this.InTouch.SendProtocolHandler(TYPE_REQUEST.LCD_ACTIVITY, String.Empty);

      //_interval = 2000;

      try
      {
        //if (this.CurrentBody == TYPE_BODY.KEYBOARD)
        //{
        //  this.PanelLogoBotClick = false;
        //  this.ExitKeyboard();
        //}
        //else
        //{
        //  this.TimeBetweenClicks.Tick += new EventHandler(this.RestartTimeBetweenClicks);
        //  this.TimeBetweenClicks.Interval = _interval;
        //  this.TimeBetweenClicks.Start();
        //  this.PanelLogoBotClick = true;
        //  this.KeyboardManualEntry = false;
        //}
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }

    } // pnl_logo_bot_Click

    //------------------------------------------------------------------------------
    // PURPOSE: Actions to do when the time is over.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    private void MainTimer_Tick(object sender, EventArgs e)
    {
      try
      {
        if (this.MainTimerNumTicks >= Constants.DEFAULT_CHECK_ACTIVITY_NUM_TICKS)
        {
          this.LoadingPanel_Activity();
          this.Jackpot_Activity();
          this.BringToFront_Activity();
          this.AccountInfo_Activity();

          // JCA: Se muestra y oculta el video solo en función de la llamada
          // que se realiza desde el SAS-HOS (TYPE_REQUEST.TERMINAL_ACTIVE)
          // JCA: Se vuelve a activar porque si se reproduce el video constantemente se queda sin memoria.
          this.AutoShowHideVideo_Activity();

          this.UpdateMarqueeMessage();

          // MainTimerNumTicks Set
          this.MainTimerNumTicks = 1;
        }
        else
        {
          // MainTimerNumTicks Set
          this.MainTimerNumTicks++;
        }

        this.Browser_Activity();
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    }

    #endregion

    #region Overrides

    protected override void WndProc(ref Message m)
    {
      if (this.VideoIsPlaying)
      {
        if (m.Msg == Constants.WM_MOUSECLICK)
        {
          if (m.WParam.ToInt32() == Constants.WM_MOUSE_LEFT_CLICK)
          {
            this.MainVideo.Close();
            this.InTouch.SendProtocolHandler(TYPE_REQUEST.LCD_ACTIVITY, String.Empty);
          }
        }
      }

      base.WndProc(ref m);
    } // WndProc

    #endregion Overrides END

    internal void BrowserInteraction(string Action)
    {
      string[] _actions = Action.Split('|');
      int _function_id = 0;
      if (_actions.Length == 2 && int.TryParse(_actions[0], out _function_id))
        InTouch.SendProtocolHandler((TYPE_REQUEST)_function_id, _actions[1]);

    }

    internal bool UpdateTerminalDrawBrowser()
    {
      if (BrowserForm == null)
      { return false; }
      BrowserForm.HostInteractionCallback(TerminalDrawNavigationParams.Params);
      return true;
    }
  }
}