﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_loading.cs
// 
//   DESCRIPTION: Base form. 
// 
//        AUTHOR: Javi Barea
// 
// CREATION DATE: 31-AUG-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 31-AUG-2017 JBP    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace LCDInTouch
{
  public partial class frm_loading : Form
  {
    #region " Properties "

    public InTouch InTouch { get; set; }
    public Thread LoadingThread { get; set; }
    public LOADING_MODE LoadingMode { get; set; }
    
    public Boolean IsLoadingMode
    {
      get { return (this.LoadingMode != LOADING_MODE.NONE); }
    }

    #endregion

    #region " Public Methods "

    public frm_loading(InTouch InTouch)
    {
      this.InTouch = InTouch;
      this.InitializeComponent();
    }

    /// <summary>
    /// Show loading panel
    /// </summary>
    public void ShowLoadingPanel()
    {
      try
      {
        if (this.InvokeRequired)
        {
          this.BeginInvoke(new MethodInvoker(delegate
          {
            this.ShowLoadingPanel();
          }));
        }
        else
        {
          if (this.Visible)
          {
            return;
          }

          // Show loading panel
          this.Visible = true;
          this.BringToFront();
        }
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // ShowLoadingPanel

    /// <summary>
    /// Hide loading panel
    /// </summary>
    public void HideLoadingPanel()
    {
      try
      {
        if (this.InvokeRequired)
        {
          this.BeginInvoke(new MethodInvoker(delegate
          {
            this.HideLoadingPanel();
          }));
        }
        else
        {
          if (!this.Visible)
          {
            return;
          }

          // Hide loading panel
          this.Visible = false;
          this.SendToBack();
        }
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // HideLoadingPanel
    
    /// <summary>
    /// Loading panel manager
    /// </summary>
    public void LoadingPanelManagement()
    {
      if (this.IsLoadingMode)
      {
        this.ShowLoadingPanel();

        return;
      }

      this.HideLoadingPanel();

    } // LoadingPanelManagement

    #endregion 

    #region " Private Methods "
    
    /// <summary>
    /// Start form thread
    /// </summary>
    private void StartLoadingThread()
    {
      this.LoadingThread = new Thread(LoadingThreadInternal);
      this.LoadingThread.SetApartmentState(ApartmentState.STA);
      this.LoadingThread.Start();
    } // StartFormThread
    
    /// <summary>
    /// Loading thread
    /// </summary>
    private void LoadingThreadInternal()
    {

      while (true)
      {
        try
        {
          if (this.IsDisposed || this.Disposing)
          {
            return;
          }
          // Loading panel manager
          this.LoadingPanelManagement();

          Thread.Sleep(100);
        }
        catch (Exception _ex)
        {
          Log.Add(_ex);
        }
      }

    } // LoadingThreadInternal

    #endregion


  }
}
