using System.ComponentModel;
using LCDInTouch.Properties;
using LCDInTouch.Controls;
using System.Windows.Forms;
namespace LCDInTouch
{
  partial class frm_base
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    //private IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_base));
      this.pnl_loading = new System.Windows.Forms.Panel();
      this.pnl_block_reasons = new System.Windows.Forms.Panel();
      this.lbl_block_reason_msg = new System.Windows.Forms.Label();
      this.pb_loading = new System.Windows.Forms.PictureBox();
      this.pnl_top = new System.Windows.Forms.Panel();
      this.pnl_bot = new System.Windows.Forms.Panel();
      this.pnl_keyboard = new System.Windows.Forms.Panel();
      this.uc_keyboard = new LCDInTouch.Controls.uc_keyboard();
      this.pnl_video = new System.Windows.Forms.Panel();
      this.axVLCPlugin21 = new AxAXVLC.AxVLCPlugin2();
      this.pnl_black = new System.Windows.Forms.Panel();
      this.pnl_configuration = new System.Windows.Forms.Panel();
      this.uc_configuration = new LCDInTouch.Controls.uc_configuration();
      this.pnl_loading.SuspendLayout();
      this.pnl_block_reasons.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pb_loading)).BeginInit();
      this.pnl_keyboard.SuspendLayout();
      this.pnl_video.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.axVLCPlugin21)).BeginInit();
      this.pnl_configuration.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_loading
      // 
      this.pnl_loading.BackColor = System.Drawing.Color.Black;
      this.pnl_loading.Controls.Add(this.pnl_block_reasons);
      this.pnl_loading.Controls.Add(this.pb_loading);
      this.pnl_loading.Location = new System.Drawing.Point(0, 20);
      this.pnl_loading.Name = "pnl_loading";
      this.pnl_loading.Size = new System.Drawing.Size(800, 256);
      this.pnl_loading.TabIndex = 3;
      // 
      // pnl_block_reasons
      // 
      this.pnl_block_reasons.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.pnl_block_reasons.BackColor = System.Drawing.Color.Gold;
      this.pnl_block_reasons.Controls.Add(this.lbl_block_reason_msg);
      this.pnl_block_reasons.Location = new System.Drawing.Point(0, 226);
      this.pnl_block_reasons.Name = "pnl_block_reasons";
      this.pnl_block_reasons.Size = new System.Drawing.Size(800, 30);
      this.pnl_block_reasons.TabIndex = 0;
      // 
      // lbl_block_reason_msg
      // 
      this.lbl_block_reason_msg.BackColor = System.Drawing.Color.Transparent;
      this.lbl_block_reason_msg.Font = new System.Drawing.Font("MyriadProRegular", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_block_reason_msg.ForeColor = System.Drawing.Color.Red;
      this.lbl_block_reason_msg.Location = new System.Drawing.Point(0, 0);
      this.lbl_block_reason_msg.Name = "lbl_block_reason_msg";
      this.lbl_block_reason_msg.Size = new System.Drawing.Size(800, 30);
      this.lbl_block_reason_msg.TabIndex = 0;
      this.lbl_block_reason_msg.Text = "xBlockReasons";
      this.lbl_block_reason_msg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // pb_loading
      // 
      this.pb_loading.ErrorImage = null;
      this.pb_loading.Image = ((System.Drawing.Image)(resources.GetObject("pb_loading.Image")));
      this.pb_loading.InitialImage = null;
      this.pb_loading.Location = new System.Drawing.Point(160, -38);
      this.pb_loading.Name = "pb_loading";
      this.pb_loading.Size = new System.Drawing.Size(480, 350);
      this.pb_loading.TabIndex = 0;
      this.pb_loading.TabStop = false;
      // 
      // pnl_top
      // 
      this.pnl_top.BackColor = System.Drawing.Color.Gray;
      this.pnl_top.Dock = System.Windows.Forms.DockStyle.Top;
      this.pnl_top.Location = new System.Drawing.Point(0, 0);
      this.pnl_top.Name = "pnl_top";
      this.pnl_top.Size = new System.Drawing.Size(800, 20);
      this.pnl_top.TabIndex = 0;
      // 
      // pnl_bot
      // 
      this.pnl_bot.BackColor = System.Drawing.Color.Gray;
      this.pnl_bot.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.pnl_bot.Location = new System.Drawing.Point(0, 276);
      this.pnl_bot.Name = "pnl_bot";
      this.pnl_bot.Size = new System.Drawing.Size(800, 20);
      this.pnl_bot.TabIndex = 0;
      // 
      // pnl_keyboard
      // 
      this.pnl_keyboard.BackColor = System.Drawing.Color.Transparent;
      this.pnl_keyboard.Controls.Add(this.uc_keyboard);
      this.pnl_keyboard.Location = new System.Drawing.Point(0, 20);
      this.pnl_keyboard.Name = "pnl_keyboard";
      this.pnl_keyboard.Size = new System.Drawing.Size(800, 256);
      this.pnl_keyboard.TabIndex = 5;
      this.pnl_keyboard.Visible = false;
      // 
      // uc_keyboard
      // 
      this.uc_keyboard.AutoSize = true;
      this.uc_keyboard.BackColor = System.Drawing.Color.Transparent;
      this.uc_keyboard.Location = new System.Drawing.Point(0, 0);
      this.uc_keyboard.Name = "uc_keyboard";
      this.uc_keyboard.Size = new System.Drawing.Size(800, 256);
      this.uc_keyboard.TabIndex = 1;
      // 
      // pnl_video
      // 
      this.pnl_video.BackColor = System.Drawing.Color.Transparent;
      this.pnl_video.Controls.Add(this.pnl_black);
      this.pnl_video.Controls.Add(this.axVLCPlugin21);
      this.pnl_video.Location = new System.Drawing.Point(0, 20);
      this.pnl_video.Name = "pnl_video";
      this.pnl_video.Size = new System.Drawing.Size(800, 256);
      this.pnl_video.TabIndex = 2;
      // 
      // axVLCPlugin21
      // 
      this.axVLCPlugin21.Dock = System.Windows.Forms.DockStyle.Fill;
      this.axVLCPlugin21.Enabled = true;
      this.axVLCPlugin21.Location = new System.Drawing.Point(0, 0);
      this.axVLCPlugin21.Name = "axVLCPlugin21";
      this.axVLCPlugin21.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axVLCPlugin21.OcxState")));
      this.axVLCPlugin21.Size = new System.Drawing.Size(800, 256);
      this.axVLCPlugin21.TabIndex = 0;
      // 
      // pnl_black
      // 
      this.pnl_black.BackColor = System.Drawing.Color.Black;
      this.pnl_black.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pnl_black.Location = new System.Drawing.Point(0, 0);
      this.pnl_black.Name = "pnl_black";
      this.pnl_black.Size = new System.Drawing.Size(800, 256);
      this.pnl_black.TabIndex = 2;
      // 
      // pnl_configuration
      // 
      this.pnl_configuration.BackColor = System.Drawing.Color.Transparent;
      this.pnl_configuration.Controls.Add(this.uc_configuration);
      this.pnl_configuration.Location = new System.Drawing.Point(0, 20);
      this.pnl_configuration.Name = "pnl_configuration";
      this.pnl_configuration.Size = new System.Drawing.Size(799, 256);
      this.pnl_configuration.TabIndex = 5;
      this.pnl_configuration.Visible = false;
      // 
      // uc_configuration
      // 
      this.uc_configuration.BackColor = System.Drawing.Color.Transparent;
      this.uc_configuration.Location = new System.Drawing.Point(0, 0);
      this.uc_configuration.MessageTimer = null;
      this.uc_configuration.Name = "uc_configuration";
      this.uc_configuration.Size = new System.Drawing.Size(800, 256);
      this.uc_configuration.TabIndex = 1;
      // 
      // frm_base
      // 
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
      this.BackColor = System.Drawing.Color.Black;
      this.ClientSize = new System.Drawing.Size(800, 296);
      this.Controls.Add(this.pnl_loading);
      this.Controls.Add(this.pnl_video);
      this.Controls.Add(this.pnl_keyboard);
      this.Controls.Add(this.pnl_top);
      this.Controls.Add(this.pnl_bot);
      this.Controls.Add(this.pnl_configuration);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
      this.KeyPreview = true;
      this.Name = "frm_base";
      this.ShowInTaskbar = false;
      this.Text = "frm_base";
      this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_base_KeyPress);
      this.pnl_loading.ResumeLayout(false);
      this.pnl_block_reasons.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.pb_loading)).EndInit();
      this.pnl_keyboard.ResumeLayout(false);
      this.pnl_keyboard.PerformLayout();
      this.pnl_video.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.axVLCPlugin21)).EndInit();
      this.pnl_configuration.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    public Panel pnl_top;
    public Panel pnl_bot;
    public Panel pnl_loading;
    public PictureBox pb_loading;
    public Panel pnl_keyboard;
    public Panel pnl_configuration;
    public Panel pnl_video;
    public AxAXVLC.AxVLCPlugin2 axVLCPlugin21;
    private System.Windows.Forms.Panel pnl_block_reasons;
    private System.Windows.Forms.Label lbl_block_reason_msg;
    public uc_keyboard uc_keyboard;
    public uc_configuration uc_configuration;
    public Panel pnl_black;

  }
}