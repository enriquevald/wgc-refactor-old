﻿using System.ComponentModel;
using LCDInTouch.Properties;
using LCDInTouch.Controls;
using System.Windows.Forms;

namespace LCDInTouch
{
  partial class frm_loading
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.pnl_loading = new System.Windows.Forms.Panel();
      this.pb_loading = new System.Windows.Forms.PictureBox();
      this.pnl_top = new System.Windows.Forms.Panel();
      this.pnl_bot = new System.Windows.Forms.Panel();
      this.pnl_loading.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pb_loading)).BeginInit();
      this.SuspendLayout();
      // 
      // pnl_loading
      // 
      this.pnl_loading.BackColor = System.Drawing.Color.Black;
      this.pnl_loading.Controls.Add(this.pb_loading);
      this.pnl_loading.Location = new System.Drawing.Point(0, 20);
      this.pnl_loading.Name = "pnl_loading";
      this.pnl_loading.Size = new System.Drawing.Size(800, 256);
      this.pnl_loading.TabIndex = 3;
      // 
      // pb_loading
      // 
      this.pb_loading.ErrorImage = null;
      this.pb_loading.Image = global::LCDInTouch.Properties.Resources.loading;
      this.pb_loading.InitialImage = global::LCDInTouch.Properties.Resources.loading;
      this.pb_loading.Location = new System.Drawing.Point(160, -38);
      this.pb_loading.Name = "pb_loading";
      this.pb_loading.Size = new System.Drawing.Size(480, 350);
      this.pb_loading.TabIndex = 0;
      this.pb_loading.TabStop = false;
      // 
      // pnl_top
      // 
      this.pnl_top.Dock = System.Windows.Forms.DockStyle.Top;
      this.pnl_top.Location = new System.Drawing.Point(0, 0);
      this.pnl_top.Name = "pnl_top";
      this.pnl_top.Size = new System.Drawing.Size(800, 20);
      this.pnl_top.TabIndex = 0;
      // 
      // pnl_bot
      // 
      this.pnl_bot.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.pnl_bot.Location = new System.Drawing.Point(0, 276);
      this.pnl_bot.Name = "pnl_bot";
      this.pnl_bot.Size = new System.Drawing.Size(800, 20);
      this.pnl_bot.TabIndex = 0;
      // 
      // frm_loading
      // 
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
      this.ClientSize = new System.Drawing.Size(800, 296);
      this.Controls.Add(this.pnl_top);
      this.Controls.Add(this.pnl_bot);
      this.Controls.Add(this.pnl_loading);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
      this.KeyPreview = true;
      this.Name = "frm_loading";
      this.ShowInTaskbar = false;
      this.Text = "frm_base";
      this.pnl_loading.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.pb_loading)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    public Panel pnl_top;
    public Panel pnl_bot;
    public Panel pnl_loading;
    public PictureBox pb_loading;

  }
}