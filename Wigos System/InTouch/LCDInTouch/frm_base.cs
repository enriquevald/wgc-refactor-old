//------------------------------------------------------------------------------
// Copyright � 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_base.cs
// 
//   DESCRIPTION: Base form. 
// 
//        AUTHOR: Javi Barea
// 
// CREATION DATE: 20-JUN-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 20-JUN-2017 JBP    First release.
//------------------------------------------------------------------------------
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Threading;
using AxAXVLC;

namespace LCDInTouch
{
  public partial class frm_base : Form
  {

    #region " Members "

    private KB_TYPE m_change_on_keyboard;
    private SHOW_MODE m_show_mode;

    #endregion

    #region " Properties "

    public Thread FormThread { get; set; }
    public Thread BrowserThread { get; set; }
    public Thread VideoThread { get; set; }
    public InTouch InTouch { get; set; }
    public Browser Browser { get; set; }
    public LCD_Queue MessageQueue { get; set; }
    public Boolean ControlsLoaded { get; set; }
    public InTouchActivity Activity { get; set; }

    public InTouchActivity Activities { get; set; }
    public TerminalDrawParams TerminalDrawNavigationParams { get; set; }
    public SASParams SASParams { get; set; }

    //public Boolean ShowModeChanged { get; set; }
    public TYPE_CARD CardTypeMode { get; set; }
    public STATUS_REQUEST_PIN ShowRequestPin { get; set; }

    public String MessageInsertCard { get; set; }
    public TYPE_MESSAGE TypeMessage { get; set; }
    public Boolean ShowingInsertCardMessage { get; set; }

    public String DrawItemSelected { get; set; }
    public Boolean PendingLogoutOperatorPanel { get; set; }
    public Boolean NextMessageIsMobileBankResponse { get; set; }
    public Video Video { get; set; }
    public AxVLCPlugin2 VLC_Plugin
    {
      get { return this.axVLCPlugin21; }
      set { this.axVLCPlugin21 = value; }
    }

    public LOADING_MODE LoadingMode { get; set; }
    public SHOW_MODE ShowMode
    {
      get { return this.m_show_mode; }
      set
      {
        this.PanelBlackManager(value);
        this.m_show_mode = value;

        if (this.m_show_mode != SHOW_MODE.LOADING)
        {
          this.LoadingMode = LOADING_MODE.NONE;
        }
      }
    }

    private void PanelBlackManager(SHOW_MODE ShowMode)
    {
      if (this.pnl_black.InvokeRequired)
      {
        this.pnl_black.BeginInvoke(new MethodInvoker(delegate
        {
          this.PanelBlackManager(ShowMode);
        }));

        return;
      }

      if (this.ControlsLoaded)
      {
        this.pnl_black.Visible = (ShowMode != SHOW_MODE.VIDEO);

        if(ShowMode != SHOW_MODE.VIDEO)
        {
          this.pnl_black.BringToFront();
        }        
      }
    }

    public KB_TYPE ChangeOnKeyboard
    {
      get { return this.m_change_on_keyboard; }
      set
      {
        this.m_change_on_keyboard = value;
        this.uc_keyboard.SetKeyboardStyle();
      }
    } // ChangeOnKeyboard

    public Boolean IsSpecialCard
    {
      get
      {
        return ( this.CardTypeMode == TYPE_CARD.TECH
              || this.CardTypeMode == TYPE_CARD.TECH_PIN
              || this.CardTypeMode == TYPE_CARD.CHANGE_STACKER
              || this.CardTypeMode == TYPE_CARD.CHANGE_STACKER_PIN
              || this.CardTypeMode == TYPE_CARD.MOBILE_BANK);
      }
    } // IsSpecialCard

    #region " ShowMode Properties "

    public Boolean IsLoadingMode
    {
      get { return (this.ShowMode == SHOW_MODE.LOADING); }
    }

    public Boolean KeyboardEnabled
    {
      get { return (this.IsTechMode || this.IsConfigMode); }
    }

    public Boolean IsBrowserMode
    {
      get { return (this.ShowMode == SHOW_MODE.BROWSER); }
    }

    public Boolean IsTechMode
    {
      get { return (this.ShowMode == SHOW_MODE.TECH); }
    }

    public Boolean IsConfigMode
    {
      get { return (this.ShowMode == SHOW_MODE.CONFIG); }
    }

    public Boolean IsVideoMode
    {
      get { return (this.ShowMode == SHOW_MODE.VIDEO); }
    }

    #endregion

    #endregion

    #region " Public Functions "

    /// <summary>
    /// Constructor
    /// </summary>
    public frm_base(InTouch InTouch)
    {
      this.InTouch = InTouch;
      this.InitializeComponent();
      this.Initialization();
    } // frm_base

    /// <summary>
    /// Update last activity time
    /// </summary>
    public void ResetActivities()
    {
      if (this.Activities == null)
      {
        this.Activities = new InTouchActivity();
      }
      else
      {
        this.Activities.ResetActivities();
      }

      this.InTouch.SendProtocolHandler(TYPE_REQUEST.LCD_ACTIVITY, String.Empty);
    } // UpdateLastActivity

    /// <summary>
    /// Show loading panel
    /// </summary>
    public void ShowLoadingPanel()
    {
      try
      {
        if (this.pnl_loading.InvokeRequired)
        {
          this.pnl_loading.BeginInvoke(new MethodInvoker(delegate
          {
            this.ShowLoadingPanel();
          }));

          return;
        }

        if (this.pnl_loading.Visible)
        {
          return;
        }

        // Show loading panel
        this.pnl_loading.Visible = true;
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // ShowLoadingPanel

    /// <summary>
    /// Hide loading panel
    /// </summary>
    public void HideLoadingPanel()
    {
      try
      {
        if (this.pnl_loading.InvokeRequired)
        {
          this.pnl_loading.BeginInvoke(new MethodInvoker(delegate
          {
            this.HideLoadingPanel();
          }));
        }
        else
        {
          if (!this.pnl_loading.Visible)
          {
            return;
          }

          // Hide loading panel
          this.pnl_loading.Visible = false;
        }
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // HideLoadingPanel

    /// <summary>
    /// Keypress manager
    /// </summary>
    public void KeyPressManager(Char KeyChar)
    {
      this.ResetActivities();

      if (KeyChar == 27)
      {
        if (this.IsConfigMode)
        {
          this.ShowMode = SHOW_MODE.TECH;
          this.ChangeOnKeyboard = KB_TYPE.TECH;

          return;
        }

        this.InTouch.LogoutManagement();

        return;
      }

      if (this.IsConfigMode)
      {
        return;
      }

      this.InTouch.KeyHandler(KeyChar);
    } // KeyPressManager

    /// <summary>
    /// Loading panel manager
    /// </summary>
    public void LoadingPanelManagement()
    {
      if (this.IsLoadingMode)
      {
        this.ShowLoadingPanel();

        return;
      }

      this.HideLoadingPanel();

    } // LoadingPanelManagement

    /// <summary>
    /// Show/Hide keyboard manager
    /// </summary>
    public void ShowHideKeyboard()
    {
      if (this.pnl_keyboard.InvokeRequired)
      {
        this.pnl_keyboard.BeginInvoke(new MethodInvoker(delegate
        {
          this.ShowHideKeyboard();
        }));
      }
      else
      {
        // Show
        if (this.IsTechMode)
        {
          this.ResetActivities();
          this.uc_keyboard.Bind();

          return;
        }

        // Hide 
        this.uc_keyboard.Unbind();
      }
    } // ShowHideKeyboard

    /// <summary>
    /// Show/Hide configuration manager
    /// </summary>
    public void ShowHideConfiguration()
    {
      if (this.pnl_configuration.InvokeRequired)
      {
        this.pnl_configuration.BeginInvoke(new MethodInvoker(delegate
        {
          this.ShowHideConfiguration();
        }));
      }
      else
      {
        // Show
        if (this.IsConfigMode)
        {
          this.ResetActivities();
          this.uc_configuration.Bind();

          return;
        }

        // Hide 
        this.uc_configuration.Unbind();
      }
    } // ShowHideConfiguration

    #endregion

    #region " Private Functions "

    /// <summary>
    /// Form Initialization
    /// </summary>
    private void Initialization()
    {
      this.InitializeMainObjects();
      this.InitializeInTouch();
      this.InitializeActivities();
      this.SetFormLocation();
      this.StartFormThread();
      this.StartBrowserThread();
      this.StartVideoThread();
      this.SetBorderStyle();
      this.NeedShowInTaskbar();
      this.BlockReasonPanelManager();

      this.ControlsLoaded = true;
    } // Initialization

    /// <summary>
    /// Initialize main objects
    /// </summary>
    private void InitializeMainObjects()
    {
      this.Browser = new Browser(this);
      this.Video   = new Video(this.pnl_video);
    } // InitializeMainObjects

    /// <summary>
    /// Set form position
    /// </summary>
    private void SetFormLocation()
    {
      this.StartPosition = FormStartPosition.Manual;

      if (this.InTouch != null && !Functions.SetLocation(this, this.InTouch.Config))
      {
        if (!System.Diagnostics.Debugger.IsAttached)
        {
          this.Location = new System.Drawing.Point(0, -this.pnl_top.Height);
        }
        else
        {
          this.Location = new System.Drawing.Point(0, 0);
        }
      }
    } // Set position

    /// <summary>
    /// Initialize InTouch properties
    /// </summary>
    private void InitializeInTouch()
    {
      this.Activity = new InTouchActivity();
      this.SetLoading(LOADING_MODE.INIT);
      this.ChangeOnKeyboard = KB_TYPE.TECH;
    } // InitializeInTouch

    /// <summary>
    /// Initialize Activities
    /// </summary>
    private void InitializeActivities()
    {
      this.ResetActivities();
    } // InitializeActivities

    /// <summary>
    /// Start form thread
    /// </summary>
    private void StartFormThread()
    {
      this.FormThread = new Thread(FormThreadInternal);
      this.FormThread.SetApartmentState(ApartmentState.STA);
      this.FormThread.Start();
    } // StartFormThread

    /// <summary>
    /// Start Browser form thread
    /// </summary>
    private void StartBrowserThread()
    {
      this.BrowserThread = new Thread(BrowserThreadInternal);
      this.BrowserThread.SetApartmentState(ApartmentState.STA);
      this.BrowserThread.Start();
    } // StartBrowserThread

    /// <summary>
    /// Start Browser form thread
    /// </summary>
    private void StartVideoThread()
    {
      this.VideoThread = new Thread(VideoThreadInternal);
      this.VideoThread.SetApartmentState(ApartmentState.STA);
      this.VideoThread.Start();
    } // StartBrowserThread

    /// <summary>
    /// Set border style
    /// </summary>
    private void SetBorderStyle()
    {
      if (System.Diagnostics.Debugger.IsAttached || !String.IsNullOrEmpty(Environment.GetEnvironmentVariable("DEVELOPMENT")))
      {
        this.FormBorderStyle = FormBorderStyle.FixedToolWindow;
      }
    } // SetBorderStyle

    /// <summary>
    /// Set border style
    /// </summary>
    private void NeedShowInTaskbar()
    {
      if (System.Diagnostics.Debugger.IsAttached || !String.IsNullOrEmpty(Environment.GetEnvironmentVariable("DEVELOPMENT")))
      {
        this.ShowInTaskbar = true;
      }
    } // NeedShowInTaskbar

    #region " Base Thread "

    /// <summary>
    /// Base thread
    /// </summary>
    private void FormThreadInternal()
    {
      while (this.InTouch.MainFormRunning)
      {
        try
        {
          if (this.IsDisposed || this.Disposing || !this.InTouch.MainFormRunning)
          {
            return;
          }

          Thread.Sleep(Constants.DEFAULT_BASETHREAD_WAIT_TIME);
          
          // Params management
          this.InTouch.ParamsManagement();

          // Manage views visibility
          this.PanelsManagement();
        }
        catch (ThreadAbortException _th_ex)
        {
          Log.Add(TYPE_MESSAGE.INFO, "'BaseThread()' aborted: '" + _th_ex.Message + "'");
        }
        catch (Exception _ex)
        {
          Log.Add(_ex);
        }

        //this.ShowModeChanged = false;
      }

      Log.Add(TYPE_MESSAGE.DEVELOPMENT_LVL1, "End 'BaseThread'");
    } // FormThreadInternal

    /// <summary>
    /// Browser thread
    /// </summary>
    private void BrowserThreadInternal()
    {
      this.Browser.StartBrowser();

      while (this.InTouch.MainFormRunning)
      {
        this.BrowserThreadDoWork();
      }

      Log.Add(TYPE_MESSAGE.DEVELOPMENT_LVL1, "End 'BrowserThread'");
    } // BrowserThreadInternal

    /// <summary>
    /// Function that do the browser thread work
    /// </summary>
    private void BrowserThreadDoWork()
    {

      try
      {
        if (this.IsDisposed || this.Disposing || !this.InTouch.MainFormRunning)
        {
          return;
        }

        Thread.Sleep(Math.Max(Constants.BROWSER_OK_SLEEP, this.Browser.SleepTime));

        // Begin invoke
        if (this.Browser.BrowserForm != null && this.Browser.BrowserForm.InvokeRequired)
        {
          this.Browser.BrowserForm.BeginInvoke(new MethodInvoker(delegate
          {
            this.Browser.BrowserStatusManager();
          }));

          return;
        }

        // Browser manager
        this.Browser.BrowserStatusManager();
      }
      catch (ThreadAbortException _th_ex)
      {
        Log.Add(TYPE_MESSAGE.INFO, "'BrowserThreadDoWork()' aborted: '" + _th_ex.Message + "'");
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // BrowserThreadDoWork

    /// <summary>
    /// Video thread
    /// </summary>
    private void VideoThreadInternal()
    {
      this.Video.Start();

      while (this.InTouch.MainFormRunning)
      {
        this.VideoThreadDoWork();
      }

      Log.Add(TYPE_MESSAGE.DEVELOPMENT_LVL1, "End 'VideoThread'");
    } // VideoThreadInternal

    /// <summary>
    /// Function that do the video thread work
    /// </summary>
    private void VideoThreadDoWork()
    {
      try
      {
        if (this.IsDisposed || this.Disposing || !this.InTouch.MainFormRunning)
        {
          return;
        }

        Thread.Sleep(Math.Max(Constants.VIDEO_FAST_SLEEP, this.Video.SleepTime));

        // Video manager
        this.Video.VideoStatusManager();

        // Garbage collector
        GC.Collect();
        GC.WaitForPendingFinalizers();
      }
      catch (ThreadAbortException _th_ex)
      {
        Log.Add(TYPE_MESSAGE.INFO, "'VideoThreadDoWork()' aborted: '" + _th_ex.Message + "'");
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // VideoThreadDoWork

    /// <summary>
    /// Panels manager
    /// </summary>
    private void PanelsManagement()
    {
      if (this.InvokeRequired)
      {
        this.BeginInvoke(new MethodInvoker(delegate
        {
          this.PanelsManagement();
        }));

        return;
      }

      // Loading panel manager
      this.LoadingPanelManagement();

      // Keyboard visibility manager
      this.ShowHideKeyboard();

      // Configuration visibility manager
      this.ShowHideConfiguration();

    } // PanelsManagement

    /// <summary>
    /// Show operator login 
    /// </summary>
    private void ShowOperatorLogin_Enter(TYPE_CARD CurrentCardMode, TYPE_CARD PreviousCardMode)
    {
      Boolean _logout;

      if (CurrentCardMode == TYPE_CARD.TECH)
      {
        _logout = (PreviousCardMode != TYPE_CARD.TECH && PreviousCardMode != TYPE_CARD.TECH_PIN);
      }
      else
      {
        _logout = (PreviousCardMode != TYPE_CARD.CHANGE_STACKER && PreviousCardMode != TYPE_CARD.CHANGE_STACKER_PIN);
      }

      if (_logout)
      {
        this.uc_keyboard.HideOperatorButtons(false);
      }

      this.ShowMode = SHOW_MODE.TECH;
      this.ChangeOnKeyboard = (CurrentCardMode == TYPE_CARD.TECH) ? KB_TYPE.TECH : KB_TYPE.COMPLETE;
    } // ShowOperatorLogin_Enter

    /// <summary>
    /// Show operator login (Exit)
    /// </summary>
    private void ShowOperatorLogin_Exit(TYPE_CARD CurrentCardMode, TYPE_CARD PreviousCardMode)
    {
      this.uc_keyboard.HideOperatorButtons(false);
      this.ChangeOnKeyboard = KB_TYPE.COMPLETE;

      this.InTouch.InitializeTimeToAskCardStatus();
    } // ShowOperatorLogin_Exit
    
    #endregion

    #endregion

    #region " Internal Functions "

    /// <summary>
    /// Set loading mode
    /// </summary>
    /// <param name="Mode"></param>
    internal void SetLoading(LOADING_MODE Mode)
    {
      if (Mode == LOADING_MODE.SHUTDOWN || this.LoadingMode == LOADING_MODE.NONE)
      {
        this.LoadingMode = Mode;
      }

      this.LoadingMode = Mode;
      this.ShowMode = SHOW_MODE.LOADING;      
    } // SetLoading
            
    /// <summary>
    /// Is loading panel visible?
    /// </summary>
    /// <returns></returns>
    internal Boolean IsLoading()
    {
      return this.IsLoading(LOADING_MODE.NONE);
    }
    internal Boolean IsLoading(LOADING_MODE Mode)
    {
      Boolean _rc;

      _rc = false;

      // Check required invoke
      if (this.InvokeRequired)
      {
        this.BeginInvoke(new MethodInvoker(delegate
        {
          _rc = this.IsLoading(Mode);
        }));

        return _rc;
      }

      return ( this.IsLoadingMode
            && this.pnl_loading.Visible
            && ( this.LoadingMode == LOADING_MODE.NONE 
              || this.LoadingMode == LOADING_MODE.INIT 
              || this.LoadingMode == Mode) );
    } // IsLoading

    /// <summary>
    /// Block reasons manager
    /// </summary>
    /// <param name="BlockReasonsId"></param>
    /// <param name="BlockReason"></param>
    internal void BrowserBlockReasonsManager()
    {
      this.BrowserBlockReasonsManager(INTOUCH_BLOCK_REASON.WEB_HOST_CONNECTION);
    } // BrowserBlockReasonsManager
    internal void BrowserBlockReasonsManager(INTOUCH_BLOCK_REASON ReasonId)
    {
      try
      {
        if (this.InvokeRequired)
        {
          this.BeginInvoke(new MethodInvoker(delegate
          {
            this.BrowserBlockReasonsManager(ReasonId);
          }));

          return;
        }

        if ( ReasonId != INTOUCH_BLOCK_REASON.NONE 
          && this.pnl_block_reasons.Visible)
        {
          return;
        }

        this.Browser.SleepTime = Constants.BROWSER_ERROR_SLEEP;
        this.Browser.ConnectionWithServer = false;
        this.Browser.NavigateInProgress = false;

        if(!this.IsVideoMode)
        {
          this.SetLoading(LOADING_MODE.BROWSER);
          this.Browser.BrowserStatus = STATUS_BROWSER.HIDE;    
        }

        // Block reason panel manager
        this.BlockReasonPanelManager(ReasonId);
      }
      catch(Exception _ex)
      {
        Log.Add(_ex);
      }
    } // BrowserBlockReasonsManager
            
    /// <summary>
    /// Show block reason panel
    /// </summary>
    internal void BlockReasonPanelManager()
    {
      this.BlockReasonPanelManager(INTOUCH_BLOCK_REASON.NONE);
    } // BlockReasonPanelManager
    internal void BlockReasonPanelManager(INTOUCH_BLOCK_REASON ReasonId)
    {
      if (this.InvokeRequired)
      {
        this.BeginInvoke(new MethodInvoker(delegate
        {
          this.BlockReasonPanelManager(ReasonId);
        }));

        return;
      }

      this.lbl_block_reason_msg.Text = this.GetBlockReasonDescription(ReasonId);
      this.pnl_block_reasons.Visible = (ReasonId != INTOUCH_BLOCK_REASON.NONE);
    } // BlockReasonPanelManager

    /// <summary>
    /// Returns specific blockreason description for intouch
    /// </summary>
    /// <param name="ReasonId"></param>
    internal String GetBlockReasonDescription(INTOUCH_BLOCK_REASON ReasonId)
    {
      switch(ReasonId)
      {
        case INTOUCH_BLOCK_REASON.WEB_HOST_CONNECTION:
          return "Could not connect to server";

        case INTOUCH_BLOCK_REASON.SHUTING_DOWN:
          return "Shutting down...";

        default:
        case INTOUCH_BLOCK_REASON.NONE:
          return String.Empty;
      }
      
    } // GetBlockReasonDescription

    /// <summary>
    /// MobileBank  mode
    /// </summary>
    /// <param name="Status"></param>
    /// <param name="TypeCard"></param>
    internal void MobileBank(int Status, TYPE_CARD TypeCard)
    {
      try
      {
        if ((TYPE_SCREEN_STATUS)Status == TYPE_SCREEN_STATUS.ENTER)
        {
          this.CardTypeMode = TypeCard;

          this.ShowMode = SHOW_MODE.TECH;
          this.ChangeOnKeyboard = KB_TYPE.MOBILE_BANK;

          this.PendingLogoutOperatorPanel = true;
        }
        else
        {
          this.ChangeOnKeyboard = KB_TYPE.EXIT;
          this.LogoutMobileBank();
        }
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // MB Login

    /// <summary>
    /// Logout MB
    /// </summary>
    internal void LogoutMobileBank()
    {
      try
      {
        this.NextMessageIsMobileBankResponse = false;
        this.PendingLogoutOperatorPanel = false;

        this.ExitKeyboard();
        this.InTouch.InitializeTimeToAskCardStatus();

        Thread.Sleep(500);
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // LogoutMobileBank

    /// <summary>
    /// Exit keyboard
    /// </summary>
    internal void ExitKeyboard()
    {
      this.ExitKeyboard(this.IsSpecialCard);
    } // ExitKeyboard
    internal void ExitKeyboard(Boolean ForceESCLogout)
    {
      if (!this.KeyboardEnabled)
      {
        return;
      }

      this.NextMessageIsMobileBankResponse = false;
      this.PendingLogoutOperatorPanel = false;
      this.ChangeOnKeyboard = KB_TYPE.EXIT;
      this.uc_keyboard.HideOperatorButtons(ForceESCLogout);
      
      this.CardTypeMode = TYPE_CARD.UNKNOWN;
      this.ShowMode = SHOW_MODE.BROWSER;
      
      this.ResetActivities();
    } // ExitKeyboard
    
    ///// <summary>
    ///// Set browser show mode
    ///// </summary>
    //internal void SetBrowserShowMode(STATUS_BROWSER NextStatus)
    //{
    //  if(!this.Browser.ConnectionWithServer)
    //  {
    //    this.HideBrowser();        

    //    return;
    //  }

    //  this.ShowMode = SHOW_MODE.BROWSER;
    //  this.Browser.BrowserStatus = NextStatus;
    //} // SetBrowserShowMode

    /// <summary>
    /// Operator login
    /// </summary>
    /// <param name="Status"></param>
    /// <param name="TypeCard"></param>
    internal void OperatorLogin(int Status, TYPE_CARD TypeCard)
    {
      try
      {
        this.CardTypeMode = TypeCard;

        if ((TYPE_SCREEN_STATUS)Status == TYPE_SCREEN_STATUS.ENTER)
        {
          this.ShowOperatorLogin_Enter(TypeCard, this.InTouch.CardType);
        }
        else
        {
          this.ShowOperatorLogin_Exit(TypeCard, this.InTouch.CardType);
        }
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // OperatorLogin

    /// <summary>
    /// Update terminal draw browser
    /// </summary>
    /// <returns></returns>
    internal bool UpdateTerminalDrawBrowser()
    {
      if (this.Browser == null)
      {
        return false;
      }

      this.Browser.BrowserForm.HostInteractionCallback(this.TerminalDrawNavigationParams.Params);

      return true;
    } // UpdateTerminalDrawBrowser

    #endregion

    #region Events

    /// <summary>
    /// frm_base_KeyPress
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// <summary>
    private void frm_base_KeyPress(object sender, KeyPressEventArgs e)
    {
      try
      {
        this.KeyPressManager(e.KeyChar);
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // // frm_base_KeyPress

    /// <summary>
    /// To catch mouse click event
    /// </summary>
    /// <param name="m"></param>
    protected override void WndProc(ref Message m)
    {
      if (m.Msg == Constants.WM_MOUSECLICK)
      {
        this.ResetActivities();

        if (m.WParam.ToInt32() == Constants.WM_MOUSE_LEFT_CLICK)
        {
          // Only for hide video
          if (!this.Browser.MainForm.IsVideoMode)
          {
            return;
          }

          this.Video.Stop();
        }
      }

      base.WndProc(ref m);
    } // WndProc
    
    #endregion
    
  } // frm_base
}
