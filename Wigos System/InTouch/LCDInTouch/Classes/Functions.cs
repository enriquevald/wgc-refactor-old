﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//      MODULE NAME: Functions.cs
// 
//      DESCRIPTION: Class for Functions on LCDInTouch
// 
//           AUTHOR: Javier Barea
// 
//    CREATION DATE: 26-JUN-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 26-JUN-2017 JBP    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Net;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;

namespace LCDInTouch
{
  public sealed class Functions
  {

    #region " Public Functions "

    /// <summary>
    /// Set form location.
    /// </summary>
    /// <param name="Form"></param>
    /// <param name="Config"></param>
    /// <returns></returns>
    public static Boolean SetLocation(Form Form, LCDConfiguration Config)
    {
      try
      {
        if (Config != null)
        {
          Form.Location = new Point(Config.Location.X, Config.Location.Y);
          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
      return false;
    } // SetLocation

    /// <summary>
    /// Delete all by name
    /// </summary>
    /// <param name="Name"></param>
    public static void DeleteAllByName(String Name)
    {
      DirectoryInfo _path;
      FileInfo[] _list_files;
      String _name;

      try
      {
        // Get only name. 
        _name = Name.Split('.')[0];
        _path = new DirectoryInfo(Directory.GetCurrentDirectory());
        _list_files = _path.GetFiles(_name + ".*");

        Thread.Sleep(500);

        foreach (FileInfo _file_info in _list_files)
        {
          if (File.Exists(_file_info.Name))
          {
            File.Delete(_file_info.Name);
          }
        }
      }
      catch (Exception)
      {
        ;
      }
    } // DeleteAllByName

    /// <summary>
    /// Rename file
    /// </summary>
    /// <param name="NewName"></param>
    /// <param name="OldName"></param>
    /// <param name="DeleteNew"></param>
    public static void RenameFile(String NewName, String OldName)
    {
      RenameFile(NewName, OldName, true);
    } // RenameFile
    public static void RenameFile(String NewName, String OldName, Boolean DeleteNew)
    {
      try
      {
        if (File.Exists(NewName))
        {
          DeleteAllByName(OldName);

          File.Copy(NewName, OldName, true);

          if (DeleteNew)
          {
            File.Delete(NewName);
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // RenameFile

    /// <summary>
    /// Gets if we can navigate to a URL
    /// </summary>
    /// <param name="Url"></param>
    /// <returns></returns>
    public static Boolean CanNavigate(String Url)
    {
      try
      {
        HttpWebRequest _request = (HttpWebRequest)WebRequest.Create(Url);
        _request.Method = WebRequestMethods.Http.Head;
        _request.Timeout = 10000;
        _request.KeepAlive = false;

        HttpWebResponse _response = (HttpWebResponse)_request.GetResponse();
        _response.Close();

        switch (_response.StatusCode)
        {
          case (HttpStatusCode.OK):
            return true;

          default:
            return false;
        }
      }
      catch (WebException _wex)
      {
        switch (_wex.Status)
        {
          case (WebExceptionStatus.Success):
            return true;

          case (WebExceptionStatus.Timeout):
            return false;

          default:
            //Log.Add(_wex);
            return false;
        }
      }
      catch(InvalidOperationException)
      {
        return false;
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);        
      }

      return false;
    } // CanNavigate

    #endregion

    #region " Internal Methods "

    /// <summary>
    /// Formated amount text.
    /// </summary>
    /// <param name="Amount"></param>
    /// <param name="InTouch"></param>
    /// <returns></returns>
    internal static String FormatCurrency(String Amount, InTouch InTouch)
    {
      return FormatCurrency(Amount, true, true, true, InTouch);
    }//FormatCurrency
    internal static String FormatCurrency(Int64 Amount, InTouch InTouch)
    {
      return FormatCurrency(Amount.ToString(), true, true, true, InTouch);
    }//FormatCurrency
    internal static String FormatCurrency(Int64 Amount, Boolean ShowCurrencySymbol, Boolean ShowDecimals, Boolean AmountIsInCents, InTouch InTouch)
    {
      return FormatCurrency(Amount.ToString(), ShowCurrencySymbol, ShowDecimals, AmountIsInCents, InTouch);
    }//FormatCurrency
    internal static String FormatCurrency(String Amount, Boolean ShowCurrencySymbol, Boolean ShowDecimals, Boolean AmountIsInCents, InTouch InTouch)
    {
      return InTouch.NumberToFormattedString(Convert.ToUInt64(Amount), ShowCurrencySymbol, ShowDecimals, AmountIsInCents);
    }//FormatCurrencyl

    #endregion 

  } // Functions
}
