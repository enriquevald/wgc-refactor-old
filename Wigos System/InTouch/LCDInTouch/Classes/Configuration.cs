//------------------------------------------------------------------------------
// Copyright � 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//      MODULE NAME: Configuration.cs
// 
//      DESCRIPTION: Class for configuration on LCDInTouch
// 
//           AUTHOR: Javier Barea
// 
//    CREATION DATE: 26-JUN-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 26-JUN-2017 JBP    First release.
//------------------------------------------------------------------------------
using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace LCDInTouch
{
  // Resolution
  internal struct LCDResolution
  {
    internal Int32 Height;
    internal Int32 Width;
  }

  internal struct LCDLocation
  {
    internal Int32 X;
    internal Int32 Y;
  }

  internal struct LCDDevelopmentLog
  {
    internal Boolean Enabled;
    internal Int32 Level;
  }
  
  public class LCDConfiguration
  {
    #region " Constants "

    private const string BACKUP_PATH = "OldConfiguration";

    #endregion
    
    #region Variables

    private Message m_message;
    private InTouch m_lcd;

    #endregion 

    #region " Class "

    public class Message 
    {
      private String m_text;
      private Boolean m_is_error;

      #region Properties

      public String Text
      {
        get { return this.m_text; }
        set { this.m_text = value; }
      } // Text

      public Boolean IsError
      {
        get { return this.m_is_error; }
        set { this.m_is_error = value; }
      } // IsError

      #endregion

      public Message(String Text, Boolean IsError)
      {
        this.m_text = Text;
        this.m_is_error = IsError;
      }      
    }

    #endregion

    #region Properties

    public InTouch InTouch
    {
      get { return this.m_lcd; }
      set { this.m_lcd = value; }
    } // InTouch

    public Message UserMessage
    {
      get { return this.m_message; }
      set { this.m_message = value; }
    } // UserMessage

    // Resolution
    internal LCDResolution Resolution;

    // Location
    internal LCDLocation Location;

    // Other
    internal Boolean MemoryLog;
    internal Boolean ShowBorders;
    internal String UrlWeb;
    internal Boolean MouseVisibility;

    internal LCDDevelopmentLog Development;

    #endregion

    #region " Public Functions "

    /// <summary>
    /// SASHost Constructor
    /// </summary>
    /// <param name="InTouch"></param>
    public LCDConfiguration(InTouch InTouch)
    {
      this.InTouch = InTouch;
      this.SetDefaultValues();

      Log.InitLCD(this);

      this.Initialize();      
    } // LCDConfiguration

    /// <summary>
    /// Web Constructor
    /// </summary>
    public LCDConfiguration()
    {
      this.SetDefaultValues();
      Log.InitWeb(this, Environment.MachineName);
    } // LCDConfiguration

    /// <summary>
    /// Set default values
    /// </summary>
    private void SetDefaultValues()
    {
      // Default Resolution
      this.Resolution = new LCDResolution();
      this.SetDefaultResolution();

      // Location
      this.Location = new LCDLocation();
      this.Location.X = 0;
      this.Location.Y = 0;

      // Development
      this.Development.Enabled = false;
      this.Development.Level = 0;

      // Others       
      this.MemoryLog = false;
      this.ShowBorders = false;
      this.MouseVisibility = false;
      this.UrlWeb = Constants.DEFAULT_BROWSER_URL;

      this.UserMessage = null;
    } // SetDefaultValues
        
    /// <summary>
    /// Save configuration
    /// </summary>
    /// <returns></returns>
    public Boolean Save()
    {
      return this.SaveConfigFile();
    } // Save
    
    /// <summary>
    /// Set Location
    /// </summary>
    /// <param name="Form"></param>
    /// <param name="Config"></param>
    /// <returns></returns>
    public static Boolean SetLocation(Form Form, LCDConfiguration Config)
    {
      try
      {
        if (Config != null)
        {
          Form.Location = new Point(Config.Location.X, Config.Location.Y);
          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
      return false;
    } // SetLocation

    #endregion

    #region " Internal Functions "

    /// <summary>
    /// Set default resolution
    /// </summary>
    internal void SetDefaultResolution()
    {
      this.Resolution.Width   = Constants.ScaleValues.DEFAULT_WIDTH;
      this.Resolution.Height  = Constants.ScaleValues.DEFAULT_HEIGHT;
    } // SetDefaultResolution

    /// <summary>
    /// Set error message
    /// </summary>
    /// <param name="Message"></param>
    internal void SetErrorMessage(String Message)
    {
      try
      {
        this.UserMessage = new Message(Message, true);
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // SetErrorMessage

    /// <summary>
    /// Set info message
    /// </summary>
    /// <param name="Message"></param>
    internal void SetInfoMessage(String Message)
    {
      try
      {
        this.UserMessage = new Message(Message, false);
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // SetInfoMessage

    /// <summary>
    /// Initialize
    /// </summary>
    internal void Initialize()
    {
      this.UserMessage = null;
      this.InitializeInternal();
      this.AddLogMessage();
    } // Initialize

    #endregion

    #region " Private Functions "

    /// <summary>
    /// Initialize internal
    /// </summary>
    private void InitializeInternal()
    {
      try
      {
        if (this.ExistOldConfiguration())
        {
          if (!this.SetOldConfiguration())
          {
            Log.Add(TYPE_MESSAGE.INFO, "Error on 'SetOldConfiguration'.");
          }
          return;
        }

        if (!this.SetConfiguration())
        {
          Log.Add(TYPE_MESSAGE.INFO, "Error on 'SetConfiguration'.");
        }
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // InitializeInternal

    /// <summary>
    /// Add log message
    /// </summary>
    private void AddLogMessage()
    {
      try
      {
        if (this.UserMessage != null && this.UserMessage.IsError)
        {
          Log.Add(TYPE_MESSAGE.ERROR, this.UserMessage.Text);
        }
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // AddLogMessage

    /// <summary>
    /// Set old configuration
    /// </summary>
    /// <returns></returns>
    private Boolean SetOldConfiguration()
    {
      try
      {
        Log.Add(TYPE_MESSAGE.INFO, "Exist old configuration...");
        if (!this.GetOldConfiguration())
        {
          return false;
        }

        if (!this.SaveNewConfiguration())
        {
          return false;
        }

        if (!this.BackUpOldConfigFiles())
        {
          return false;
        }

        Log.Add(TYPE_MESSAGE.INFO, "Backup completed.");

        return true;
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }

      return false;
    } // SetOldConfiguration

    /// <summary>
    /// Set configuration
    /// </summary>
    /// <returns></returns>
    private Boolean SetConfiguration()
    {
      String _line;

      try
      {
        Log.Add(TYPE_MESSAGE.INFO, "Loading configuration...");

        if (!File.Exists(Constants.FILE_NAME_CONFIG))
        {
          Log.Add(TYPE_MESSAGE.INFO, "Default configuration loaded.");
          return true;
        }

        using (StreamReader _sr = new StreamReader(Constants.FILE_NAME_CONFIG))
        {
          while ((_line = _sr.ReadLine()) != null)
          {
            if (_line.Contains(Constants.CONFIG_NAME_RESOLUTION))
            {
              if (!this.SetResolution(_line))
              {
                return false;
              }
            }

            if (_line.Contains(Constants.CONFIG_NAME_LOCATION))
            {
              if (!this.SetLocation(_line))
              {
                return false;
              }
            }

            if (_line.Contains(Constants.CONFIG_NAME_MOUSE))
            {
              if (!this.SetMouse(_line))
              {
                return false;
              }
            }

            if (_line.Contains(Constants.CONFIG_NAME_BORDERS))
            {
              if (!this.SetBorders(_line))
              {
                return false;
              }
            }

            if (_line.Contains(Constants.CONFIG_URL_WEB))
            {
              if (!this.SetUrlWeb(_line))
              {
                return false;
              }
            }

            if (_line.Contains(Constants.CONFIG_NAME_DEVELOPMENT))
            {
              if (!this.SetDevelopment(_line))
              {
                return false;
              }
            }
          }
        }

        Log.Add(TYPE_MESSAGE.INFO, "Configuration loaded.");
        return true;
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }

      return false;
    } // SetConfiguration

    /// <summary>
    /// Set resolution
    /// </summary>
    /// <param name="Line"></param>
    /// <returns></returns>
    private Boolean SetResolution(String Line)
    {
      String[] _resolution;

      this.UserMessage = null;

      try
      {
        Line = Line.Replace(Constants.CONFIG_NAME_RESOLUTION, String.Empty).Trim();
        _resolution = Line.Split(';');

        if (_resolution.Length == 2)
        {
          if ( Int32.TryParse(_resolution[0], out this.Resolution.Width)
            && Int32.TryParse(_resolution[1], out this.Resolution.Height))
          {
            return true;
          }
        }        
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }

      this.SetDefaultResolution();
      this.SetErrorMessage(Resource.GetString("KEYBOARD_ERROR_LOAD_RESOLUTION"));
      return false;
    } // SetResolution

    /// <summary>
    /// Set location
    /// </summary>
    /// <param name="Line"></param>
    /// <returns></returns>
    private Boolean SetLocation(String Line)
    {
      String[] _location;
      this.UserMessage = null;

      try
      {
        Line = Line.Replace(Constants.CONFIG_NAME_LOCATION, String.Empty).Trim();
        _location = Line.Split(';');

        if (_location.Length == 2)
        {
          if ( Int32.TryParse(_location[0], out this.Location.X)
            && Int32.TryParse(_location[1], out this.Location.Y))
          {
            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }

      this.SetErrorMessage(Resource.GetString("KEYBOARD_ERROR_LOAD_LOCATION"));
      return false;
    } // SetLocation

    /// <summary>
    /// Set Development
    /// </summary>
    /// <param name="Line"></param>
    /// <returns></returns>
    private Boolean SetDevelopment(String Line)
    {
      String[] _dev_log;

      this.UserMessage = null;

      try
      {
        Line = Line.Replace(Constants.CONFIG_NAME_DEVELOPMENT, String.Empty).Trim();
        _dev_log = Line.Split(';');

        if ( _dev_log.Length > 0)
        {
          this.Development.Enabled  = (_dev_log[0] == "1");
          this.Development.Level    = (Int32)TYPE_MESSAGE.DEVELOPMENT_LVL3; // Default

          if ( _dev_log.Length == 2
            && Int32.TryParse(_dev_log[1], out this.Development.Level) )
          {
            // Set configured lvl
            this.Development.Level += Constants.DEVELOPMENT_LOG_PREFIX;
            return true;
          }        
        }
        
        return true;
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }

      this.SetErrorMessage(Resource.GetString("KEYBOARD_ERROR_LOAD_DEVELOPMENT"));
      return false;
    } // SetDevelopment

    /// <summary>
    /// Set memory
    /// </summary>
    /// <param name="Line"></param>
    /// <returns></returns>
    private Boolean SetMemory(String Line)
    {
      this.UserMessage = null;

      try
      {
        Line = Line.Replace(Constants.CONFIG_NAME_MEMORY, String.Empty).Trim();
        this.MemoryLog = (Line == "1");
        return true;
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }

      this.SetErrorMessage(Resource.GetString("KEYBOARD_ERROR_LOAD_MEMORY"));
      return false;
    }

    private Boolean SetMouse(String Line)
    {
      this.UserMessage = null;

      try
      {
        Line = Line.Replace(Constants.CONFIG_NAME_MOUSE, String.Empty).Trim();
        this.MouseVisibility = (Line == "1");
        return true;
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }

      this.SetErrorMessage(Resource.GetString("KEYBOARD_ERROR_LOAD_MOUSE"));
      return false;
    } // SetMemory

    /// <summary>
    /// Set borders
    /// </summary>
    /// <param name="Line"></param>
    /// <returns></returns>
    private Boolean SetBorders(String Line)
    {
      this.UserMessage = null;

      try
      {
        Line = Line.Replace(Constants.CONFIG_NAME_BORDERS, String.Empty).Trim();
        this.ShowBorders = (Line == "1");
        return true;
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }

      this.SetErrorMessage(Resource.GetString("KEYBOARD_ERROR_LOAD_BORDER"));
      return false;
    } // SetBorders

    /// <summary>
    /// Set url web
    /// </summary>
    /// <param name="Line"></param>
    /// <returns></returns>
    private Boolean SetUrlWeb(String Line)
    {
      this.UserMessage = null;

      try
      {
        this.UrlWeb = Line.Replace(Constants.CONFIG_URL_WEB, String.Empty).Trim();
        return true;
      }
      catch (Exception _ex)
      {
        this.UrlWeb = Constants.CONFIG_URL_WEB;
        Log.Add(_ex);
      }

      this.SetErrorMessage(Resource.GetString("KEYBOARD_ERROR_URL_WEB"));
      return false;
    } // SetUrlWeb

    /// <summary>
    /// Check if exist old configuration
    /// </summary>
    /// <returns></returns>
    private Boolean ExistOldConfiguration()
    {
      // Si existe "BACKUP_FILE" es que ya se ha cargado la primera vez la configuraci�n antigua y no debe volver a pasar por aqu�. 
      // Posteriores veces siempre tiene que hacerse del modo actual. 

      return !Directory.Exists(BACKUP_PATH) 
            && ( File.Exists(Constants.FILE_NAME_MOUSE)
              || File.Exists(Constants.FILE_NAME_DEVELOPMENT)
              || File.Exists(Constants.FILE_NAME_MEMORY)
              || File.Exists(Constants.FILE_NAME_BORDERS)
              || File.Exists(Constants.FILE_NAME_LOCATION)
              || File.Exists(Constants.FILE_NAME_RESOLUTION));
    } // ExistOldConfiguration

    /// <summary>
    /// Get old configuration
    /// </summary>
    /// <returns></returns>
    private Boolean GetOldConfiguration()
    {
      Log.Add(TYPE_MESSAGE.INFO, "Getting old configuration...");
      this.UserMessage = null;

      try
      {
        if (!this.GetLocation_OldMode())
        {
          this.SetErrorMessage(Resource.GetString("KEYBOARD_ERROR_LOAD_OLD_LOCATION"));
          return false;
        }

        if (!this.GetResolution_OldMode())
        {
          this.SetErrorMessage(Resource.GetString("KEYBOARD_ERROR_LOAD_OLD_RESOLUTION"));
          return false;
        }

        this.Development.Enabled  = File.Exists(Constants.FILE_NAME_DEVELOPMENT);
        this.Development.Level    = Constants.DEVELOPMENT_LOG_PREFIX + 3;

        this.MouseVisibility      = File.Exists(Constants.FILE_NAME_MOUSE);
        this.MemoryLog            = File.Exists(Constants.FILE_NAME_MEMORY);
        this.ShowBorders          = File.Exists(Constants.FILE_NAME_BORDERS);
        
        return true;
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }

      return false;
    } // GetOldConfiguration

    /// <summary>
    /// Get location (Old Mode)
    /// </summary>
    /// <returns></returns>
    private Boolean GetLocation_OldMode()
    {
      Boolean _exist_config;
      String _str_location;

      try
      {
        _exist_config = File.Exists(Constants.FILE_NAME_LOCATION);

        if (!_exist_config)
        {
          return true;
        }

        using (StreamReader sr = new StreamReader(Constants.FILE_NAME_LOCATION))
        {
          _str_location = sr.ReadToEnd();
          if (!string.IsNullOrEmpty(_str_location))
          {
            this.Location.X = Convert.ToInt32(_str_location.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries)[0]);
            this.Location.Y = Convert.ToInt32(_str_location.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries)[1]);

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }

      return false;
    } // GetLocation_OldMode

    /// <summary>
    /// Get resolution (Old Mode)
    /// </summary>
    /// <returns></returns>
    private Boolean GetResolution_OldMode()
    {
      Int32 _custom_width;
      Int32 _custom_height;
      Boolean _exist_config;
      String _str_resolution;
      String[] _size;

      try
      {
        _exist_config = File.Exists(Constants.FILE_NAME_RESOLUTION);

        if (!_exist_config)
        {
          return true;
        }

        using (StreamReader sr = new StreamReader(Constants.FILE_NAME_RESOLUTION))
        {
          _str_resolution = sr.ReadLine();
          if (!string.IsNullOrEmpty(_str_resolution))
          {
            _size = _str_resolution.Split(';');
            if (_size != null && _size.Length == 2)
            {
              if (Int32.TryParse(_size[0], out _custom_width)
                && Int32.TryParse(_size[1], out _custom_height))
              {
                this.Resolution.Width = _custom_width;
                this.Resolution.Height = _custom_height;

                return true;
              }
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }

      return false;
    } // GetResolution_OldMode

    /// <summary>
    /// Save new configuration
    /// </summary>
    /// <returns></returns>
    private Boolean SaveNewConfiguration()
    {
      this.UserMessage = null;

      try
      {
        if (!this.SaveConfigFile())
        {
          this.SetErrorMessage(Resource.GetString("KEYBOARD_ERROR_SAVE_CONFIG_FILE"));
          return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }

      return false;
    } // SaveNewConfiguration

    /// <summary>
    /// Save configuration file
    /// </summary>
    /// <returns></returns>
    private Boolean SaveConfigFile()
    {
      try
      {
        if (File.Exists(Constants.FILE_NAME_CONFIG))
        {
          File.Delete(Constants.FILE_NAME_CONFIG);
        }

        using (StreamWriter _sw = File.CreateText(Constants.FILE_NAME_CONFIG))
        {
          // Location 
          _sw.WriteLine("{0}{1};{2}"
                        , Constants.CONFIG_NAME_LOCATION
                        , this.Location.X
                        , this.Location.Y);

          // Resolution 
          _sw.WriteLine("{0}{1};{2}"
                        , Constants.CONFIG_NAME_RESOLUTION
                        , this.Resolution.Width
                        , this.Resolution.Height);

          // Development 
          _sw.WriteLine("{0}{1};{2}"
                        , Constants.CONFIG_NAME_DEVELOPMENT
                        , this.Development.Enabled ? "1" : "0"
                        , Math.Min(this.Development.Level - Constants.DEVELOPMENT_LOG_PREFIX, 0));

          // Others
          _sw.WriteLine("{0}{1}", Constants.CONFIG_NAME_BORDERS , (this.ShowBorders)      ? "1" : "0");
          _sw.WriteLine("{0}{1}", Constants.CONFIG_NAME_MOUSE   , (this.MouseVisibility)  ? "1" : "0");
          _sw.WriteLine("{0}{1}", Constants.CONFIG_NAME_MEMORY  , (this.MemoryLog)        ? "1" : "0");
          _sw.WriteLine("{0}{1}", Constants.CONFIG_URL_WEB      , this.UrlWeb);

          _sw.Flush();
          _sw.Close();
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }

      return false;
    } // SaveConfigFile

    /// <summary>
    /// Backup old configuration files
    /// </summary>
    /// <returns></returns>
    private Boolean BackUpOldConfigFiles()
    {
      Log.Add(TYPE_MESSAGE.INFO, "Backup old configuration...");
      this.UserMessage = null;

      try
      {
        if (!Directory.Exists(BACKUP_PATH))
        {
          Directory.CreateDirectory(BACKUP_PATH);
        }

        // mouse txt
        if (File.Exists(Constants.FILE_NAME_MOUSE))
        {
          if (!this.BackUpFile(Constants.FILE_NAME_MOUSE))
          {
            this.SetErrorMessage(String.Format(Resource.GetString("KEYBOARD_ERROR_SAVE_FILE"), Constants.FILE_NAME_MOUSE));
            return false;
          }
        }

        // location txt
        if (File.Exists(Constants.FILE_NAME_LOCATION))
        {
          if (!this.BackUpFile(Constants.FILE_NAME_LOCATION))
          {
            this.SetErrorMessage(String.Format(Resource.GetString("KEYBOARD_ERROR_SAVE_FILE"), Constants.FILE_NAME_LOCATION));
            return false;
          }
        }

        // development txt
        if (File.Exists(Constants.FILE_NAME_DEVELOPMENT))
        {
          if (!this.BackUpFile(Constants.FILE_NAME_DEVELOPMENT))
          {
            this.SetErrorMessage(String.Format(Resource.GetString("KEYBOARD_ERROR_SAVE_FILE"), Constants.FILE_NAME_DEVELOPMENT));
            return false;
          }
        }
        
        // memory txt
        if (File.Exists(Constants.FILE_NAME_MEMORY))
        {
          if (!this.BackUpFile(Constants.FILE_NAME_MEMORY))
          {
            this.SetErrorMessage(String.Format(Resource.GetString("KEYBOARD_ERROR_SAVE_FILE"), Constants.FILE_NAME_MEMORY));
            return false;
          }
        }

        // borders txt
        if (File.Exists(Constants.FILE_NAME_BORDERS))
        {
          if (!this.BackUpFile(Constants.FILE_NAME_BORDERS))
          {
            this.SetErrorMessage(String.Format(Resource.GetString("KEYBOARD_ERROR_SAVE_FILE"), Constants.FILE_NAME_BORDERS));
            return false;
          }
        }

        // resolution txt
        if (File.Exists(Constants.FILE_NAME_RESOLUTION))
        {
          if (!this.BackUpFile(Constants.FILE_NAME_RESOLUTION))
          {
            this.SetErrorMessage(String.Format(Resource.GetString("KEYBOARD_ERROR_SAVE_FILE"), Constants.FILE_NAME_RESOLUTION));
            return false;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }

      return false;
    } // BackUpOldConfigFiles

    /// <summary>
    /// Backup file
    /// </summary>
    /// <param name="FileName"></param>
    /// <returns></returns>
    private Boolean BackUpFile(String FileName)
    {
      try
      {
        File.Copy(FileName, BACKUP_PATH + "/" + FileName, true);
        File.Delete(FileName);

        return true;
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }

      return false;
    } // BackUpFile

    #endregion

  }
}
