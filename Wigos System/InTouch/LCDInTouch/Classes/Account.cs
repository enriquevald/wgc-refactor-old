using System;
using System.Collections.Generic;
using System.Text;

namespace LCDInTouch
{
  // Tipo delegado para enlazar notificaciones de cambios.
  public delegate void ChangedBalanceEventHandler(object sender, EventArgs e);
  public delegate void ChangedPointsEventHandler(object sender, EventArgs e);

  #region Class Balance

  public class Balance : ICloneable
  {
    #region Members
    public String BalanceStr;
    public String BalanceRStr;
    public String BalanceNRStr;
    public String BalanceNotReservedStr;
    public String BalanceRReservedStr;
    public String PointsStr;

    public Int64 BalanceTotal;
    public Int64 BalanceR;
    public Int64 BalanceRPromo;
    public Int64 BalanceNR;
    public Int64 BalanceRReserved;
    public Int64 Points;

    #endregion

    #region Public Functions
    public Balance()
    {
      this.BalanceStr = "";
      this.BalanceNotReservedStr = "";
      this.BalanceRStr = "";
      this.BalanceNRStr = "";
      this.PointsStr = "";
      this.BalanceTotal = 0;
      this.BalanceR = 0;
      this.BalanceRPromo = 0;
      this.BalanceNR = 0;
      this.BalanceRReserved = 0;
      this.Points = -1;
    }
    #endregion

    #region ICloneable Members

    public object Clone()
    {
      return this.MemberwiseClone();
    }

    #endregion
  }

  #endregion

  #region Gifts Class

  public class Gifts
  {
    #region Members

    private Boolean m_request_pending;
    private Boolean m_request_in_progress;

    private LCD_PLAYER_GIFTS m_all_lists;
    private List<LCD_ITEM> m_current_gifts_list;
    private List<LCD_ITEM> m_next_gifts_list;
    private List<LCD_ITEM> m_credits_gifts_list;

    #endregion

    #region Properties

    public Boolean ResponsePending
    {
      get { return this.m_request_pending; }
      set { this.m_request_pending = value; }
    } // RequestPending

    public Boolean RequestInProgress
    {
      get { return this.m_request_in_progress; }
      set { this.m_request_in_progress = value; }
    } // RequestInProgress

    public LCD_PLAYER_GIFTS AllLists
    {
      get { return this.m_all_lists; }
      set
      {
        this.m_current_gifts_list = null;
        this.m_next_gifts_list = null;
        this.m_credits_gifts_list = null;
        this.m_all_lists = value;
      }
    } // Gifts

    public List<LCD_ITEM> CurrentGiftsList
    {
      get { return this.m_current_gifts_list; }
      set { this.m_current_gifts_list = value; }
    } // CurrentGiftsList

    public List<LCD_ITEM> NextGiftsList
    {
      get { return this.m_next_gifts_list; }
      set { this.m_next_gifts_list = value; }
    } // NextGiftsList

    public List<LCD_ITEM> CreditsGiftsList
    {
      get { return this.m_credits_gifts_list; }
      set { this.m_credits_gifts_list = value; }
    } // CurrentGiftsList

    #endregion


    #region Public Functions

    public Gifts()
    {
      this.m_request_pending = true;
      this.m_request_in_progress = false;

      this.m_current_gifts_list = null;
      this.m_next_gifts_list = null;
      this.m_credits_gifts_list = null;
      this.m_all_lists = new LCD_PLAYER_GIFTS();
    }

    #endregion
  }

  #endregion

  #region Promotion Class

  public class Promotion
  {
    #region Members

    private Boolean m_request_pending;
    private Boolean m_request_in_progress;

    private LCD_PLAYER_PROMOTIONS m_all_lists;
    private List<LCD_ITEM> m_current_promotions_list;     // current
    private List<LCD_ITEM> m_next_promotions_list;        // next
    private List<LCD_ITEM> m_future_promotions_list;      // future
    private List<LCD_ITEM> m_applicable_promotions_list;  // applicable

    #endregion

    #region Properties

    public Boolean ResponsePending
    {
      get { return this.m_request_pending; }
      set { this.m_request_pending = value; }
    } // RequestPending

    public Boolean RequestInProgress
    {
      get { return this.m_request_in_progress; }
      set { this.m_request_in_progress = value; }
    } // RequestInProgress

    public LCD_PLAYER_PROMOTIONS AllLists
    {
      get { return this.m_all_lists; }
      set
      {
        this.m_current_promotions_list = null;
        this.m_next_promotions_list = null;
        this.m_future_promotions_list = null;
        this.m_applicable_promotions_list = null;
        this.m_all_lists = value;
      }
    } // Promotions

    public List<LCD_ITEM> CurrentPromotionsList
    {
      get { return this.m_current_promotions_list; }
      set { this.m_current_promotions_list = value; }
    } // CurrentPromotionsList

    public List<LCD_ITEM> NextPromotionsList
    {
      get { return this.m_next_promotions_list; }
      set { this.m_next_promotions_list = value; }
    } // NextPromotionsList

    public List<LCD_ITEM> FuturePromotionsList
    {
      get { return this.m_future_promotions_list; }
      set { this.m_future_promotions_list = value; }
    } // FuturePromotionsList

    public List<LCD_ITEM> ApplicablePromotionsList
    {
      get { return this.m_applicable_promotions_list; }
      set { this.m_applicable_promotions_list = value; }
    } // ApplicablePromotionsList

    #endregion

    #region Public Functions

    public Promotion()
    {
      this.m_request_pending = true;
      //this.m_request_in_progress = false;

      this.m_current_promotions_list = null;
      this.m_next_promotions_list = null;
      this.m_future_promotions_list = null;
      this.m_applicable_promotions_list = null;
      this.m_all_lists = new LCD_PLAYER_PROMOTIONS();
    }

    #endregion
  }

  #endregion

  #region Account Class

  public class Account : ICloneable
  {
    #region Delegates
    public event ChangedBalanceEventHandler ChangedBalance;
    public event ChangedPointsEventHandler ChangedPoints;
    #endregion

    #region Members
    public TYPE_CARD Type;
    public Int32 AccountId;
    public Int32 Pin;
    public String Name;
    public String Address;
    public String Telephone;
    public String Email;
    public String PointsToNextLevel;
    public String EstimatedPoints;
    public Int64 RedeemPoints;
    public Int64 RedeemPromoPoints;
    public Int64 NonRedeemPoints;
    public String ActualLevel;
    public String NextLevelName;
    //public Boolean RequestInfo;
    //public Boolean RequestInfoEstimatedPoints;
    public String NextLevelPointsGenerated;
    public String NextLevelPointsDiscretionaries;
    public String LevelPoints;
    public Boolean PinValidated;

    public Balance Balance;
    public Gifts Gifts;
    public Promotion Promotions;

    public Boolean ResponseAccountInfoPending;
    public Boolean RequestAccountInfoInProgress;

    // Background account info update. 
    public Boolean BackgroundGetInfo;

    #endregion

    #region Public Functions
    public Account()
    {
      this.RequestAccountInfoInProgress = false;
      this.ResponseAccountInfoPending = true;

      this.Balance = new Balance();
      this.Gifts = new Gifts();
      this.Promotions = new Promotion();
    }
    //------------------------------------------------------------------------------
    // PURPOSE: Return if the Account has Balance.  
    //  PARAMS:
    //      - INPUT:
    //          � Nothing.
    //
    //      - OUTPUT:
    //          � Nothing.
    //
    // RETURNS: Nothing.
    // 
    internal bool HaveBalance()
    {
      return (this.Balance.BalanceTotal != 0 && this.Balance.BalanceTotal > this.Balance.BalanceRReserved);
    }
    //------------------------------------------------------------------------------
    // PURPOSE: Update Account Balance.  
    //  PARAMS:
    //      - INPUT:
    //          � Nothing.
    //
    //      - OUTPUT:
    //          � Nothing.
    //
    // RETURNS: Nothing.
    // 
    internal void SetBalance(LCD_PLAYER_REQUEST_BALANCE Balance, InTouch LCD)
    {
      Balance _balance;
      _balance = new Balance();

      _balance.BalanceTotal = (Int64)Balance.balance.total_cents;
      _balance.BalanceR = (Int64)Balance.balance.redeemable_cents;
      _balance.BalanceRPromo = (Int64)Balance.balance.promo_redeemable_cents;
      _balance.BalanceNR = (Int64)Balance.balance.promo_not_redeemable_cents;
      _balance.BalanceRReserved = (Int64)Balance.balance.redeemable_reserved_cents;
      _balance.Points = (Int64)Balance.total_points;

      SetBalance(_balance, LCD);
    }
    //------------------------------------------------------------------------------
    // PURPOSE: Update Account Balance, set empty all fields.
    //  PARAMS:
    //      - INPUT:
    //          � Nothing.
    //
    //      - OUTPUT:
    //          � Nothing.
    //
    // RETURNS: Nothing.
    // 
    internal void EmptyMoneyBalance()
    {
      this.Balance.BalanceTotal = 0;
      this.Balance.BalanceR = 0;
      this.Balance.BalanceRPromo = 0;
      this.Balance.BalanceNR = 0;
      this.Balance.BalanceRReserved = 0;
      this.Balance.BalanceStr = "";
      this.Balance.BalanceNotReservedStr = "";
      this.Balance.BalanceRStr = "";
      this.Balance.BalanceNRStr = "";

      this.OnChangedBalance(EventArgs.Empty);
    }//EmptyMoneyBalance

    //------------------------------------------------------------------------------
    // PURPOSE: Update Account Balance.  
    //  PARAMS:
    //      - INPUT:
    //          � Nothing.
    //
    //      - OUTPUT:
    //          � Nothing.
    //
    // RETURNS: Nothing.
    // 
    internal void SetBalancePoints(Balance Balance, InTouch LCD)
    {
      try
      {
        if (this.Balance.Points != (Int64)Balance.Points)
        {
          this.Balance.Points = (Int64)Balance.Points;
          this.Balance.PointsStr = (this.Balance.Points > 0) ? Functions.FormatCurrency(this.Balance.Points, false, false, false, LCD) : "0";
          this.OnChangedPoints(EventArgs.Empty);
        }
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    }
    //------------------------------------------------------------------------------
    // PURPOSE: Update Account Balance.  
    //  PARAMS:
    //      - INPUT:
    //          � Nothing.
    //
    //      - OUTPUT:
    //          � Nothing.
    //
    // RETURNS: Nothing.
    // 
    internal void SetBalance(Balance Balance, InTouch LCD)
    {
      try
      {
        Balance _previous_balance;
        Boolean _balance_change;
        long _redeemable_balance;

        _previous_balance = (Balance)this.Balance.Clone();
        _balance_change = false;

        this.Balance.BalanceTotal = (Int64)Balance.BalanceTotal;
        this.Balance.BalanceR = (Int64)Balance.BalanceR;
        this.Balance.BalanceRPromo = (Int64)Balance.BalanceRPromo;
        this.Balance.BalanceNR = (Int64)Balance.BalanceNR;
        _redeemable_balance = this.Balance.BalanceR + this.Balance.BalanceRPromo;
        this.Balance.BalanceRReserved = (Int64)Balance.BalanceRReserved;

        if (this.BalanceChanged(this.Balance, _previous_balance))
        {
          _balance_change = true;
        }
        this.Balance.BalanceStr = (this.Balance.BalanceTotal != 0) ? Functions.FormatCurrency(this.Balance.BalanceTotal, LCD) : "";
        this.Balance.BalanceNotReservedStr = (this.Balance.BalanceTotal != 0) ? Functions.FormatCurrency(this.Balance.BalanceTotal - this.Balance.BalanceRReserved, LCD) : "";
        this.Balance.BalanceRStr = (_redeemable_balance != 0) ? Functions.FormatCurrency(_redeemable_balance, LCD) : "";
        this.Balance.BalanceNRStr = (this.Balance.BalanceNR != 0) ? Functions.FormatCurrency(this.Balance.BalanceNR, false, true, true, LCD) : "";
        this.Balance.BalanceRReservedStr = (this.Balance.BalanceRReserved != 0) ? Functions.FormatCurrency(this.Balance.BalanceRReserved, false, true, true, LCD) : "";

        //JCA  Balance.total_points.ToString()  �Se tiene que llamar a la funci�n de Cots para formatear bien?
        if (this.Balance.Points != (Int64)Balance.Points)
        {
          this.Balance.Points = (Int64)Balance.Points;
          this.Balance.PointsStr = (this.Balance.Points > 0) ? Functions.FormatCurrency(this.Balance.Points, false, false, false, LCD) : "0";
          this.OnChangedPoints(EventArgs.Empty);
        }

        if (_balance_change)
        {
          this.OnChangedBalance(EventArgs.Empty);
        }
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    }

    private bool BalanceChanged(Balance ActualBalance, Balance PreviousBalance)
    {
      if (ActualBalance.BalanceNR != PreviousBalance.BalanceNR)
      {
        return true;
      }
      else if (ActualBalance.BalanceR != PreviousBalance.BalanceR)
      {
        return true;
      }
      else if (ActualBalance.BalanceRPromo != PreviousBalance.BalanceRPromo)
      {
        return true;
      }
      else if (ActualBalance.BalanceTotal != PreviousBalance.BalanceTotal)
      {
        return true;
      }
      else if (ActualBalance.BalanceRReserved != PreviousBalance.BalanceRReserved)
      {
        return true;
      }

      return false;
    }

    protected virtual void OnChangedBalance(EventArgs e)
    {
      if (this.ChangedBalance != null)
      {
        this.ChangedBalance(this, e);
      }
    }
    protected virtual void OnChangedPoints(EventArgs e)
    {
      if (this.ChangedPoints != null)
      {
        this.ChangedPoints(this, e);
      }
    }
    #endregion

    #region Private Functions

    #endregion

    #region ICloneable Members

    public object Clone()
    {
      return this.MemberwiseClone();
    }

    #endregion

  } // Account

  #endregion

}
