﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//      MODULE NAME: Resource.cs
// 
//      DESCRIPTION: Class for Resource on LCDInTouch
// 
//           AUTHOR: Javier Barea
// 
//    CREATION DATE: 30-JUN-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 30-JUN-2017 JBP    First release.
//------------------------------------------------------------------------------
using System;
using System.Globalization;
using System.IO;
using System.Resources;

namespace LCDInTouch
{

  public class Resource
  {

    #region " Properties 

    public static Boolean FolderCreate { get; set; }
    public static ResourceManager ResourceMng { get; set; }
    public static CultureInfo CultureInf { get; set; }
    public static String LastErrorMessage { get; set; }
    public static Boolean RepeatedError { get; set; }

    #endregion

    /// <summary>
    /// Initialization
    /// </summary>
    /// <param name="Language"></param>
    public static void Init(String Language)
    {
      String[] _lang_files;
      String[] _tmp_files;
      String _resource_file_name;
      String _tmp_file_name;
      String _directory_file;

      if (!FolderCreate)
      {
        // Create Per Language Folders
        _lang_files = System.IO.Directory.GetFiles(System.IO.Directory.GetCurrentDirectory(), "*_LCD_Display.resources.dll");

        foreach (String _lang_file in _lang_files)
        {
          _directory_file = _lang_file.Substring(0, _lang_file.IndexOf("_LCD_Display.resources.dll"));
          _resource_file_name = _directory_file + "\\LCD_Display.resources.dll";

          // Create Language folder
          System.IO.Directory.CreateDirectory(_directory_file);

          // Copy Language DLL
          for (int _idx_try = 0; _idx_try < 3; _idx_try++)
          {
            if (_idx_try > 0)
            {
              System.Threading.Thread.Sleep(500);
            }
            try
            {
              System.IO.File.Copy(_lang_file, _resource_file_name, true);

              break;
            }
            catch { ; }

            // Rename files with TMP files
            if (_idx_try < 2)
            {
              try
              {
                _tmp_file_name = _directory_file + "\\LCD_RESOURCE_" + Path.GetRandomFileName() + ".TMP";
                System.IO.File.Move(_resource_file_name, _tmp_file_name);
              }
              catch { }
            }
          } // for  _idx_try

          //
          // Delete TMP files
          //
          _tmp_files = System.IO.Directory.GetFiles(_directory_file, "LCD_RESOURCE_*.TMP");
          foreach (String _tmp_file in _tmp_files)
          {
            try
            {
              System.IO.File.Delete(_tmp_file);
            }
            catch { ; }
          }

        } // for each lang

        FolderCreate = true;
      }

      if (ResourceMng != null)
      {
        ResourceMng.ReleaseAllResources();
        ResourceMng = null;
      }

      ResourceMng = new ResourceManager("LCD_501.Properties.LCD_Resources", System.Reflection.Assembly.GetExecutingAssembly());
      CultureInf = new CultureInfo(Language);
      Log.Add(TYPE_MESSAGE.DEVELOPMENT_LVL1, String.Format("Setting Culture Info: {0}", Language));

    } // Init
    
    /// <summary>
    /// String format of resources.
    /// </summary>
    /// <param name="Name"></param>
    /// <param name="Params"></param>
    /// <returns></returns>
    public static String StringFormat(String Name, String[] Params)
    {
      return String.Format(GetString(Name), Params).Replace("\\r\\n", "\r\n");
    } // StringFormat

    /// <summary>
    /// Get string of resources, if not exists return default message.
    /// </summary>
    /// <param name="Name"></param>
    /// <returns></returns>
    public static String GetString(String Name)
    {
      return GetString(Name, Constants.RESOURCE_STRING_NOT_FOUND);
    } // GetString
    public static String GetString(String Name, String DefaultMSG)
    {
      String _str;

      _str = null;

      try
      {
        if (ResourceMng == null)
        {
          Resource.Init(Constants.TYPE_LANGUAGE_ENGLISH);
          return GetString(Name);
        }

        _str = ResourceMng.GetString(Name, CultureInf);
        if (_str != null)
        {
          return _str;
        }
      }
      catch
      {
        ;
      }

      return DefaultMSG;
    } // GetString

  }

}
