//------------------------------------------------------------------------------
// Copyright � 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: TerminalDrawNavigationParams.cs
// 
//   DESCRIPTION: Manage properties (TerminalDraw) from SAS HOST to LCD. 
// 
//        AUTHOR: Javi Barea
// 
// CREATION DATE: 25-JUL-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 25-JUL-2017 JBP    First release.
//------------------------------------------------------------------------------
using System;
using System.Globalization;

namespace LCDInTouch
{
  public class TerminalDrawParams
  {

    #region " Constants "

    private const int MIN_NUM_PARAMS = 11;
    private const int NUM_PARAMS = 13;

    private const int POSITION_URL          = 0;
    private const int POSITION_BET_AMOUNT   = 1;
    private const int POSITION_IS_WINNER    = 2;
    private const int POSITION_WIN_AMOUNT   = 3;
    private const int POSITION_BALANCE      = 4;
    private const int POSITION_TIMEOUT      = 5;
    private const int POSITION_IS_FORCED    = 6;
    private const int POSITION_RUN_DRAW     = 7;
    private const int POSITION_SHOW_RESULT  = 8;
    private const int POSITION_AUTO_DRAW    = 9;
    private const int POSITION_ACCOUNT_ID   = 10;
    private const int POSITION_GAME_ID      = 11;
    private const int POSITION_GAME_TYPE    = 12;

    #endregion

    #region " Properties "

    public InTouch Lcd { get; set; }
    public String Url { get; set; }
    public Int64 TimeOut { get; set; }    
    public Int64 AccountId { get; set; }
    public Int64 BetAmount { get; set; }
    public Int64 Balance { get; set; }
    public Int64 WinAmount { get; set; }
    public Boolean IsWinner { get; set; }
    public Boolean IsForced { get; set; }
    public Boolean ParamsOk { get; set; }
    public Boolean RunDraw { get; set; }
    public Boolean ShowResult { get; set; }
    public Boolean AutoDraw { get; set; }
    public Int64 GameId { get; set; }
    public Int64 GameType { get; set; }
    public Decimal NewBalance { get { return (Decimal)(Balance + WinAmount); } }

    public String Params
    {
      get 
      {
        return  "BetAmount="    + Functions.FormatCurrency(this.BetAmount, this.Lcd) +
                "&Won="         + this.IsWinner.ToString(CultureInfo.InvariantCulture) +
                "&WinAmount="   + Functions.FormatCurrency(this.WinAmount, this.Lcd) +
                "&Balance="     + Functions.FormatCurrency(this.Balance, this.Lcd) +
                "&Timeout="     + ((this.TimeOut) * 1000).ToString(CultureInfo.InvariantCulture) +
                "&ForceDraw="   + this.IsForced.ToString(CultureInfo.InvariantCulture) +
                "&ShowDraw="    + this.RunDraw.ToString(CultureInfo.InvariantCulture) +
                "&ShowResult="  + this.ShowResult.ToString(CultureInfo.InvariantCulture) +
                "&AutoDraw="    + this.AutoDraw.ToString(CultureInfo.InvariantCulture) +
                "&AccountId="   + this.AccountId.ToString(CultureInfo.InvariantCulture) +
                "&GameId="      + this.GameId.ToString() +
                "&GameType ="   + this.GameType.ToString();
      }
    }

    #endregion

    #region "Public Functions"

    /// <summary>
    /// Mapp params to class properties
    /// </summary>
    /// <param name="Params"></param>
    /// <param name="Lcd"></param>
    public TerminalDrawParams(String Params)
    {
      Int64 _bet_amount;      
      Int64 _win_amount;
      Int64 _balance;
      Int64 _timeout;
      Int64 _account_id;
      Boolean _is_winner;
      Boolean _is_forced;
      Boolean _run_draw;
      Boolean _show_result;
      Boolean _auto_draw;
      Int64 _game_id;
      Int64 _game_type; // TODO - FOS : UNRANKED

      this.ParamsOk = true;

      if (!String.IsNullOrEmpty(Params))
      {
        String[] _params = Params.Split('|');

        if (_params.Length >= MIN_NUM_PARAMS)
        {
          this.Url = _params[POSITION_URL];

          if (Int64.TryParse(_params[POSITION_BET_AMOUNT], NumberStyles.Integer, CultureInfo.InvariantCulture, out _bet_amount))
          {
            this.BetAmount = _bet_amount;
          }

          if (Int64.TryParse(_params[POSITION_WIN_AMOUNT], NumberStyles.Integer, CultureInfo.InvariantCulture, out _win_amount))
          {
            this.WinAmount = _win_amount;
          }

          if (Int64.TryParse(_params[POSITION_BALANCE], NumberStyles.Integer, CultureInfo.InvariantCulture, out _balance))
          {
            this.Balance = _balance;
          }

          if (Int64.TryParse(_params[POSITION_TIMEOUT], NumberStyles.Integer, CultureInfo.InvariantCulture, out _timeout))
          {
            this.TimeOut = _timeout + 2;
          }

          if (Int64.TryParse(_params[POSITION_ACCOUNT_ID], NumberStyles.Integer, CultureInfo.InvariantCulture, out _account_id))
          {
            this.AccountId = _account_id;
          }
          
          if (Int64.TryParse(_params[POSITION_GAME_ID].Split(',')[0], NumberStyles.Integer, CultureInfo.InvariantCulture, out _game_id))
          {
            this.GameId = _game_id;
          }

          if (Int64.TryParse(_params[POSITION_GAME_TYPE], NumberStyles.Integer, CultureInfo.InvariantCulture, out _game_type))
          {
            this.GameType = _game_type;
          }

          this.ToBool(_params[POSITION_IS_WINNER]   , out _is_winner);
          this.ToBool(_params[POSITION_IS_FORCED]   , out _is_forced);
          this.ToBool(_params[POSITION_RUN_DRAW]    , out _run_draw);
          this.ToBool(_params[POSITION_SHOW_RESULT] , out _show_result);
          this.ToBool(_params[POSITION_AUTO_DRAW]   , out _auto_draw);

          this.IsWinner = _is_winner;
          this.IsForced = _is_forced;
          this.RunDraw = _run_draw;
          this.ShowResult = _show_result;
          this.AutoDraw = _auto_draw;

        }
      }
    } // TerminalDrawNavigationParams

    /// <summary>
    /// Convert string to boolean
    /// </summary>
    /// <param name="Val"></param>
    /// <param name="Result"></param>
    /// <returns></returns>
    private Boolean ToBool(String Val, out Boolean Result)
    {
      try
      {
        switch (Val.ToUpper(CultureInfo.InvariantCulture))
        {
          case "1":
          case "TRUE":
          case "T":
            Result = true;
            break;

          default:
            Result = false;
            break;
        }
        return true;
      }
      catch 
      {
        Result = false;
        return false;
      }
    } // ToBool
    
    #endregion

  } // TerminalDrawNavigationParams

}
