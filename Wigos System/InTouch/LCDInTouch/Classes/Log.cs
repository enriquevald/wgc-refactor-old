//------------------------------------------------------------------------------
// Copyright � 2014 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Log.cs
// 
//   DESCRIPTION: Logger clase. 
// 
//        AUTHOR: Javi Barea
// 
// CREATION DATE: 02-JUN-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 02-JUN-2014 JBP    First release.
// 09-FEB-2016 FJC    Product Backlog Item 9105:BonoPlay: LCD: Cambios varios
//------------------------------------------------------------------------------
using System;
using System.Diagnostics;
using System.IO;

namespace LCDInTouch
{
  public class Log
  {
    #region Constants

    private const String  PATH_LOG = "InTouchLog";
    private const Int16   EXPIRATION_DAYS = 30;

    #endregion

    #region Variables
    private static Boolean m_initialized_log;
    private static String m_terminal_name;
    //private static long m_init_timer;
    //private static long m_elapsed_timer;
    private static LCDConfiguration m_config;
    #endregion

    #region Properties

    public static String AbsolutePath { get; set; }
    public static String FilePath { get; set; }

    public static String FilePathLCD
    {
      get { return PATH_LOG + "/" + "LCDInTouch_" + DateTime.Now.ToString("yyyy_MM_dd") + ".LOG"; }
    }

    public static String FilePathWeb
    {
      get { return PATH_LOG + "/" + "WEBInTouch_" + DateTime.Now.ToString("yyyy_MM_dd") + ".LOG"; }
    }

    public static LCDConfiguration Config
    {
      get { return m_config; }
      set { m_config = value; }
    }

    public static Boolean InitializedLog
    {
      get { return m_initialized_log; }
      set { m_initialized_log = value; }
    }

    #endregion

    #region Public Functions

    /// <summary>
    /// Add initialize INFO
    /// </summary>
    public static void Separator()
    {
      try
      {
        Add(TYPE_MESSAGE.VOID, "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
                                
      }
      catch (Exception _ex)
      {
        Console.WriteLine(_ex.Message);
      }
    } // Separator

    /// <summary>
    ///  Add initialize INFO
    /// </summary>
    /// <param name="LCDConfig"></param>
    public static void InitLCD(LCDConfiguration LCDConfig)
    {
      try
      {
        FilePath = Log.FilePathLCD;
        AbsolutePath = String.Empty;

        SetLogger(LCDConfig);

        Add(TYPE_MESSAGE.INFO, "----------------------------------------------------------------------------");
        Add(TYPE_MESSAGE.INFO, "*                                 Init LCD                                 *");
        Add(TYPE_MESSAGE.INFO, "----------------------------------------------------------------------------");
        Add(TYPE_MESSAGE.INFO, "Initializing...");
      }
      catch (Exception _ex)
      {
        Console.WriteLine(_ex.Message);
      }
    } // InitLCD

    /// <summary>
    /// Add initialize INFO
    /// </summary>
    /// <param name="Configuration"></param>
    public static void InitWeb(LCDConfiguration Configuration, String TerminalName)
    {
      try
      {
        m_terminal_name = TerminalName;
        FilePath = Log.FilePathWeb;
        AbsolutePath = Environment.GetFolderPath(Environment.SpecialFolder.InternetCache) + "\\";

        SetLogger(Configuration);

        Add(TYPE_MESSAGE.INFO, "----------------------------------------------------------------------------");
        Add(TYPE_MESSAGE.INFO, "*                                Init WEB                                  *");
        Add(TYPE_MESSAGE.INFO, "----------------------------------------------------------------------------");
        Add(TYPE_MESSAGE.INFO, "Starting...");
      }
      catch (Exception _ex)
      {
        Console.WriteLine(_ex.Message);
      }
    } // InitWeb

    /// <summary>
    /// Check if message must write. 
    /// </summary>
    /// <param name="Type"></param>
    /// <param name="Message"></param>
    /// <returns></returns>
    public static Boolean CheckMessage(TYPE_MESSAGE Type, String Message)
    {
      if (Message.Length == 0)
      {
        return false;
      }

      switch (Type)
      {
        case TYPE_MESSAGE.DEVELOPMENT_LVL1:
        case TYPE_MESSAGE.DEVELOPMENT_LVL2:
        case TYPE_MESSAGE.DEVELOPMENT_LVL3:
          {
            return DevelopmentLogFilter(Type);
          }

        case TYPE_MESSAGE.MEMORY:
          {
            return MemoryLogFilter();
          }

        default:
          // Nothing todo
          break;
      }

      return true;
    } // CheckMessage

    /// <summary>
    /// Add line into log file
    /// </summary>
    /// <param name="Ex"></param>
    public static void Add(Exception Ex)
    {
      StackTrace _st;

      String _message_ex;
      String _message_st;
      
      _st = new StackTrace();

      _message_ex = "Error ocurred in '" +
                    _st.GetFrame(1).GetMethod().Module + "." + 
                    _st.GetFrame(1).GetMethod().Name + "()'. \n " + //(Line: " + _st.GetFrame(1).GetFileLineNumber() + ")'. \n " +
                    "              " + 
                    "   - Exception : " + Ex.Message;

      _message_st = "   " + 
                    "   - StackTrace: " + Ex.StackTrace;

      Separator();
      Add(TYPE_MESSAGE.ERROR, _message_ex);
      Add(TYPE_MESSAGE.VOID, _message_st);
      Separator();
    } // Add
    public static void Add(TYPE_MESSAGE Type, String Message)
    {
      try
      {
        if ( CheckMessage(Type, Message) )
        {
          AddToLog(Type, Message);
        }
      }
      catch (Exception _ex)
      {
        Console.WriteLine(_ex.Message);
      }
    } //  Add

    #endregion

    #region Private Functions

    /// <summary>
    /// Add line into log file
    /// </summary>
    /// <param name="Type"></param>
    /// <param name="Message"></param>
    private static void AddToLog(TYPE_MESSAGE Type, String Message)
    {
      try
      {
        if (InitializedLog)
        {
          using (StreamWriter _sw = File.AppendText(AbsolutePath + FilePath))
          {
            _sw.WriteLine(" {0} - {1}[{2}]: {3}"
                          , (Type != TYPE_MESSAGE.VOID) ? DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss") : String.Empty
                          , GetTypeErrorString(Type)
                          , m_terminal_name
                          , Message);
            _sw.Flush();
          }
        }
      }
      catch (Exception _ex)
      {
        Console.WriteLine(_ex.Message);
      }
    } // AddToLog

    /// <summary>
    /// Create log files. 
    /// </summary>
    private static void CreateLogFile()
    {
      try
      {
        String _path;

        _path = String.Format("{0}{1}", AbsolutePath, Log.PATH_LOG);

        if (!File.Exists(FilePath))
        {
          if (!Directory.Exists(_path))
          {
            Directory.CreateDirectory(_path);
          }
        }
      }
      catch (Exception _ex)
      {
        Add(_ex);
      }
    } // CreateLogFile

    /// <summary>
    /// Delete old logs. 
    /// </summary>
    private static void DeleteOldLogs()
    {
      try
      {
        String[] _files;

        _files = Directory.GetFiles(AbsolutePath + Log.PATH_LOG);
        
        foreach (String _file in _files)
        {
          if (File.GetCreationTime(_file) < DateTime.Now.AddDays(-Log.EXPIRATION_DAYS))
          {
            File.Delete(_file);
          }
        }
      }
      catch (Exception _ex)
      {
        Add(_ex);
      }
    } // DeleteOldLogs
    
    /// <summary>
    /// Set logger folder and file. 
    /// </summary>
    /// <param name="Configuration"></param>
    private static void SetLogger(LCDConfiguration Configuration)
    {
      InitializedLog = false;

      try
      {
        CreateLogFile();
        DeleteOldLogs();
        InitializedLog = true;

        if (Configuration != null)
        {
          Config = Configuration;
        }
      }
      catch (Exception _ex)
      {
        Console.WriteLine(_ex.Message);
      }
    } // SetLogger

    /// <summary>
    /// Get type error description
    /// </summary>
    /// <param name="Type"></param>
    /// <returns></returns>
    private static String GetTypeErrorString(TYPE_MESSAGE Type)
    {
      String _type;

      switch (Type)
      {
        case TYPE_MESSAGE.ERROR:
          _type = "  Error";
          break;
        case TYPE_MESSAGE.VOID:
          _type = "       ";
          break;
        case TYPE_MESSAGE.WARNING:
          _type = "Warning";
          break;
        case TYPE_MESSAGE.INFO:
          _type = "   Info";
          break;
        case TYPE_MESSAGE.DEVELOPMENT_LVL1:
        case TYPE_MESSAGE.DEVELOPMENT_LVL2:
        case TYPE_MESSAGE.DEVELOPMENT_LVL3:
          _type = "Develop";
          break;
        default:
          _type = "Unknown";
          break;
      }

      return _type;
    } // GetTypeErrorString

    /// <summary>
    /// Must be filter message
    /// </summary>
    /// <param name="Type"></param>
    /// <returns></returns>
    private static Boolean DevelopmentLogFilter(TYPE_MESSAGE Type)
    {
      try
      {
        return ( Config != null 
              && Config.Development.Enabled
              && Config.Development.Level >= (Int32)Type);
      }
      catch (Exception _ex)
      {
        Add(_ex);
      }

      return false;
    } // DevelopmentLogFilter

    /// <summary>
    /// Must be show message
    /// </summary>
    /// <param name="Type"></param>
    /// <returns></returns>
    private static Boolean MemoryLogFilter()
    {
      try
      {
        return (Config != null && Config.MemoryLog);
      }
      catch (Exception _ex)
      {
        Add(_ex);
      }

      return false;
    } // MemoryLogFilter

    #endregion

  }
}
