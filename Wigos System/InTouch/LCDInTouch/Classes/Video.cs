//------------------------------------------------------------------------------
// Copyright � 2014 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Video.cs
// 
//   DESCRIPTION: Video Class. 
// 
//        AUTHOR: Javi Barea
// 
// CREATION DATE: 20-MAY-2014
// 
// REVISION HISTORY:
// 
// Date        Author   Description
// ----------- ------   --------------------------------------------------------
// 20-MAY-2014 JBP      First release.
//------------------------------------------------------------------------------
using System;
using System.IO;
using System.Windows.Forms;

namespace LCDInTouch
{
  public class Video
  {

    #region Enums

    enum MediaStatus
    {
      None = 0,
      Stopped = 1,
      Paused = 2,
      Running = 3
    }

    #endregion

    #region Constants

    private const int EC_COMPLETE = 0x01;
    private const int WS_CHILD = 0x40000000;
    private const int WS_CLIPCHILDREN = 0x2000000;

    #endregion

    #region Properties

    public Panel Panel { get; set; }
    public LCD_RESOURCE VideoResource { get; set; }
    public Boolean VideoManagerInProcess { get; set; }
    public Int32 SleepTime { get; set; }
    public STATUS_VIDEO VideoStatus { get; set; }

    public frm_base MainForm
    {
      get { return (frm_base)this.Panel.Parent; }
    }

    #endregion

    #region " Public Methods "

    /// <summary>
    /// Video constructor
    /// </summary>
    /// <param name="Panel"></param>
    public Video(Panel Panel)
    {
      this.Panel = Panel;
    }

    ///// <summary>
    ///// �Video is playing?
    ///// </summary>
    //public Boolean VideoIsPlaying()
    //{ 
    //  try
    //  {
    //    if (this.MainForm.VLC_Plugin != null && this.MainForm.VLC_Plugin.playlist == null)
    //    {
    //      return false;
    //    }

    //    return (this.MainForm.VLC_Plugin.playlist.isPlaying && this.Panel.Visible);
    //  }
    //  catch
    //  {
    //    return false;
    //  }
    //} // VideoIsPlaying

    /// <summary>
    /// Set Video status to Init resource
    /// </summary>
    public void Start()
    {
      this.VideoStatus = STATUS_VIDEO.INIT_RESOURCE;
    } // Start

    ///// <summary>
    ///// Set Video status to Play video
    ///// </summary>
    //public void Play()
    //{
    //  this.VideoStatus = STATUS_VIDEO.INIT_RESOURCE;
    //} // Play

    /// <summary>
    /// Set Video status to unloading video
    /// </summary>
    public void Stop()
    {
      this.VideoStatus = STATUS_VIDEO.HIDE_VIDEO;
    } // Stop

    #endregion

    #region " Internal Methods "

    /// <summary>
    /// Video Status Manager
    /// </summary>
    internal void VideoStatusManager()
    {
      try
      {
        if (this.VideoManagerInProcess)
        {
          return;
        }

        // Protect thread iteration
        this.VideoManagerInProcess = true;

        // Check required invoke
        if (this.Panel.InvokeRequired)
        {
          this.Panel.BeginInvoke(new MethodInvoker(delegate
          {
            this.VideoStatusManager();
          }));

          return;
        }

        // Start Video manager...
        //  

        this.SleepTime  = Constants.VIDEO_FAST_SLEEP;
 
        // Video Manager
        switch (this.VideoStatus)
        {
          case STATUS_VIDEO.INIT_RESOURCE:
            this.InitVideoResource();
            break;

          case STATUS_VIDEO.LOAD_RESOURCE:
            this.LoadVideoResource();
            break;

          case STATUS_VIDEO.INIT_VIDEO:
            this.InitVideo();
            break;

          case STATUS_VIDEO.LOAD_VIDEO:
            this.LoadVideo();
            break;

          case STATUS_VIDEO.PLAY_VIDEO:
            this.PlayVideo();
            break;

          case STATUS_VIDEO.SHOW_VIDEO:
            this.ShowVideo();
            break;

          case STATUS_VIDEO.RUNNING:
            this.Running();
            break;

          case STATUS_VIDEO.HIDE_VIDEO:
            this.HideVideo();
            break;

          case STATUS_VIDEO.CLOSE_VIDEO:
            this.CloseVideo();
            break;

          default:
          case STATUS_VIDEO.IDLE:
            this.VideoIDLE();
            break;
        }

        // Develpment log
        Log.Add(TYPE_MESSAGE.DEVELOPMENT_LVL1, String.Format("VideoStatusManager : {0}", this.VideoStatus));

        Application.DoEvents();
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
      finally
      {
        this.VideoManagerInProcess = false;
      }
    } // VideoStatusManager

    #endregion

    #region " Private Methods "

    #region " Video resource manager

    /// <summary>
    /// Init video from WCP.
    /// </summary>
    private void InitVideoResource()
    {
      LCD_PARAM_RESOURCES[] _resources;
      LCD_RESOURCE _video_resource;
      int _real_resource_id;

      try
      {
        // Check params resources
        if(this.MainForm.InTouch.Params.Parameters.resource == null)
        {
          this.SleepTime = Constants.VIDEO_QUICK_WAIT_SLEEP;

          return;
        }

        _resources = this.MainForm.InTouch.Params.Parameters.resource;
        _real_resource_id = this.GetRightResourceId(Constants.ResourcesValues.VIDEO);

        // Check resource
        if (_real_resource_id == Constants.ResourcesValues.DISABLED)
        {
          this.SleepTime = Constants.VIDEO_QUICK_WAIT_SLEEP;

          return;
        }

        // Check resource
        if (String.IsNullOrEmpty(_resources[_real_resource_id].path))
        {
          Log.Add(TYPE_MESSAGE.ERROR, "Unable to initialize video resource.");
          this.SleepTime = Constants.VIDEO_WAIT_SLEEP;

          return;
        }
        
        // Check if is the same resource
        if ( this.VideoResource.Resource.path != null
          && this.VideoResource.Resource.path == _resources[_real_resource_id].path)
        {
          this.VideoStatus = File.Exists(this.VideoResource.CurrentVideo) ? STATUS_VIDEO.PLAY_VIDEO 
                                                                          : STATUS_VIDEO.LOAD_RESOURCE;

          return;
        }

        // Set video resource
        _video_resource = new LCD_RESOURCE();
        _video_resource.Resource = _resources[_real_resource_id];

        this.VideoResource = _video_resource;

        // Set next state
        this.VideoStatus = STATUS_VIDEO.LOAD_RESOURCE;
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // InitVideoResource

    /// <summary>
    /// Load video from WCP.
    /// </summary>
    private void LoadVideoResource()
    {
      LCD_PARAM_RESOURCES[] _resources;
      LCD_RESOURCE _video_resource;
      Int32 _real_resource_id;

      try
      {
        _resources = this.MainForm.InTouch.Params.Parameters.resource;
        _real_resource_id = this.GetRightResourceId(Constants.ResourcesValues.VIDEO);

        if (!File.Exists(this.VideoResource.Resource.path))
        {
          Log.Add(TYPE_MESSAGE.ERROR, "Unable to load video resource.");
          this.SleepTime = Constants.VIDEO_WAIT_SLEEP;
          this.VideoStatus = STATUS_VIDEO.IDLE;

          return;
        }

        _video_resource = this.VideoResource;
        _video_resource.Extension = Path.GetExtension(this.VideoResource.Resource.path);
        _video_resource.CurrentVideo = Constants.FILENAME_VIDEO + _video_resource.Extension;

        Functions.RenameFile(_resources[_real_resource_id].path, _video_resource.CurrentVideo, false);

        this.VideoResource = _video_resource;

        this.VideoStatus = STATUS_VIDEO.INIT_VIDEO;
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // LoadVideoResource

    /// <summary>
    /// Init VLCPlugin.
    /// </summary>
    private void InitVideo()
    {
      try
      {
        // Check video exist
        if (!File.Exists(this.VideoResource.CurrentVideo))
        {
          Log.Add(TYPE_MESSAGE.ERROR, "Unable to initialize video.");
          this.SleepTime = Constants.VIDEO_WAIT_SLEEP;
          this.VideoStatus = STATUS_VIDEO.IDLE;

          return;
        }

        this.VideoStatus = STATUS_VIDEO.LOAD_VIDEO;
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // InitVideo

    /// <summary>
    /// Load video in VLCPlugin.
    /// </summary>
    private void LoadVideo()
    {
      String[] _vlc_option;
      Uri _uri;

      try
      {
        // Remove video 
        this.ClearPlayList();

        _vlc_option = new String[] { ":aspect-ratio=" + this.MainForm.VLC_Plugin.Width + ":" + this.MainForm.VLC_Plugin.Height };
        _uri = new Uri(Application.StartupPath + "/" + this.VideoResource.CurrentVideo);

        // Load the video.
        this.MainForm.VLC_Plugin.playlist.add(_uri.AbsoluteUri, " ", _vlc_option);
        this.MainForm.VLC_Plugin.playlist.playItem(0);
        
        // Play video
        this.VideoStatus = STATUS_VIDEO.HIDE_VIDEO;
      }
      catch (Exception _ex)
      {
        this.VideoStatus = STATUS_VIDEO.CLOSE_VIDEO;
        Log.Add(_ex);
      }
    } // LoadVideo

    /// <summary>
    /// Play video
    /// </summary>
    private void PlayVideo()
    {
      try
      {
        // Check playlist
        if (this.MainForm.VLC_Plugin.playlist.items.count == 0)
        {
          Log.Add(TYPE_MESSAGE.ERROR, "Unable to play video.");
          this.SleepTime  = Constants.VIDEO_WAIT_SLEEP;
          this.VideoStatus = STATUS_VIDEO.IDLE;          

          return;
        }

        // Play video
        this.MainForm.VLC_Plugin.playlist.playItem(0);
        this.VideoStatus = STATUS_VIDEO.SHOW_VIDEO;
        
      }
      catch (Exception _ex)
      {
        this.VideoStatus = STATUS_VIDEO.CLOSE_VIDEO;

        Log.Add(_ex);
      }
    } // PlayVideo

    /// <summary>
    /// Show video
    /// </summary>
    public void ShowVideo()
    {
      this.MainForm.ShowMode = SHOW_MODE.VIDEO; 
      this.VideoStatus = STATUS_VIDEO.RUNNING;

      // Update activity
      this.MainForm.Activities.UpdateActivity(TYPE_ACTIVITY.ShowHideVideo, DateTime.Now);      
    } // ShowVideo

    /// <summary>
    /// Video is running...
    /// </summary>
    private void Running()
    {
      try
      {
        // In running increment wait slepp
        this.SleepTime = Constants.VIDEO_RUNNING_WAIT_SLEEP;

        // Only manage when is video mode. 
        if (this.MainForm.KeyboardEnabled)
        {
          return;
        }

        // Hide management
        if (this.MainForm.Activities.IsExpired(TYPE_ACTIVITY.ShowHideVideo, Constants.DEFAULT_HIDE_VIDEO_TIME))
        {
          this.Stop();

          return;
        }
      }
      catch (Exception _ex)
      {
        this.VideoStatus = STATUS_VIDEO.CLOSE_VIDEO;
        Log.Add(_ex);
      }
    } // Running
    
    /// <summary>
    /// Hide video
    /// </summary>
    private void HideVideo()
    {
      try
      {
        // Hide management
        if (this.MainForm.IsVideoMode)
        {
          this.HideVideoInternal();
        }

        // Check if user is in session
        if(this.MainForm.InTouch.InSession)
        {
          return;
        }
        
        // Show management
        if (this.MainForm.Activities.IsExpired(TYPE_ACTIVITY.ShowHideVideo, Constants.DEFAULT_SHOW_VIDEO_TIME))
        {
          this.Start();
        }
      }
      catch (Exception _ex)
      {
        this.VideoStatus = STATUS_VIDEO.CLOSE_VIDEO;
        Log.Add(_ex);
      }      
    } // HideVideo

    /// <summary>
    /// HIde video internal
    /// </summary>
    private void HideVideoInternal()
    {
      //// Wait loading visibility
      //if (!this.MainForm.IsLoading(LOADING_MODE.VIDEO))
      //{
      //  return;
      //}

      // Stop video
      if ( this.MainForm.VLC_Plugin.playlist.items.count > 0)
      {
        this.MainForm.VLC_Plugin.playlist.togglePause();
      }

      // Update activity
      this.MainForm.Activities.UpdateActivity(TYPE_ACTIVITY.ShowHideVideo, DateTime.Now);

      if (this.MainForm.KeyboardEnabled)
      {
        return;
      }

      this.MainForm.ShowMode = (this.MainForm.Browser.ConnectionWithServer) ? SHOW_MODE.BROWSER : SHOW_MODE.LOADING;

    } // HideVideoInternal

    /// <summary>
    /// Clear LCD_RESOURCE video and video in VLC playlist.
    /// </summary>
    private void CloseVideo()
    {
      LCD_RESOURCE _video_resource;

      try
      {
        // Initialize video resource
        _video_resource = new LCD_RESOURCE();
        _video_resource.Resource.id = 0;
        _video_resource.Resource.path = string.Empty;

        this.VideoResource = _video_resource;

        if (this.MainForm.VLC_Plugin.playlist.items.count > 0)
        {
          this.ClearPlayList();
        }

        this.VideoStatus = STATUS_VIDEO.IDLE;
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // CloseVideo

    /// <summary>
    /// IDLE Video status
    /// </summary>
    private void VideoIDLE()
    {
      // Relaunch Video
      if (this.MainForm.Activities.IsExpired(TYPE_ACTIVITY.ShowHideVideo, Constants.DEFAULT_SHOW_VIDEO_TIME))
      {
        this.Start();
      }
    } // VideoIDLE

    /// <summary>
    /// Clear play list
    /// </summary>
    private void ClearPlayList()
    {
      try
      {
        if ( this.MainForm.VLC_Plugin != null
          && this.MainForm.VLC_Plugin.playlist.items.count > 0)
        {
          this.MainForm.VLC_Plugin.playlist.stop();
          this.MainForm.VLC_Plugin.playlist.clear();
        }
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // ClearPlayList

    /// <summary>
    /// Check resource
    /// </summary>
    /// <param name="TheoreticalId"></param>
    /// <returns></returns>
    private Int32 GetRightResourceId(Int32 TheoreticalId)
    {
      if (this.MainForm.InTouch.Params.Parameters.resource[Constants.ResourcesValues.LOGO_IMAGE].id == TheoreticalId)
      {
        return Constants.ResourcesValues.LOGO_IMAGE;
      }

      if (this.MainForm.InTouch.Params.Parameters.resource[Constants.ResourcesValues.VIDEO].id == TheoreticalId)
      {
        return Constants.ResourcesValues.VIDEO;
      }

      return Constants.ResourcesValues.DISABLED;
    } // GetRightResourceId

    #endregion

    #endregion

  }
}
