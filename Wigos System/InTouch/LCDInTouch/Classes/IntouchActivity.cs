//------------------------------------------------------------------------------
// Copyright � 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//      MODULE NAME: InTouchActivity.cs
// 
//      DESCRIPTION: Class for browser on LCDInTouch
// 
//           AUTHOR: Javier Barea
// 
//    CREATION DATE: 26-JUN-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 26-JUN-2017 JBP    First release.
//------------------------------------------------------------------------------
using System;

namespace LCDInTouch
{
  public class InTouchActivity
  {

    #region Members

    public DateTime TimeRequestParams { get; set; }
    public DateTime TimeSendParams { get; set; }
    public DateTime TimeRefreshBrowserBlockReasons { get; set; }
    public DateTime TimeShowVideo { get; set; }

    #endregion

    #region Public Functions

    public InTouchActivity()
    {
      this.InitializeActivities();
    } // InTouchActivity

    /// <summary>
    /// Reset activities
    /// </summary>
    public void ResetActivities()
    {
      try
      {
        // Wait for next time to request
        this.TimeSendParams = DateTime.Now;
        this.TimeShowVideo = DateTime.Now;
        this.TimeRefreshBrowserBlockReasons = DateTime.Now;
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // UpdateActivities

    /// <summary>
    /// Is expired 
    /// </summary>
    /// <param name="Type"></param>
    /// <param name="Time"></param>
    /// <param name="UpdateActivity"></param>
    /// <returns></returns>
    public Boolean IsExpired(TYPE_ACTIVITY Type, Int32 Time)
    {
      return this.IsExpired(Type, Time, true);
    } // IsExpired
    public Boolean IsExpired(TYPE_ACTIVITY Type, Int32 Time, Boolean UpdateActivity)
    {
      try
      {
        if (this.GetActivity(Type).AddMilliseconds(Time) < DateTime.Now)
        {
          if (UpdateActivity)
          {
            this.UpdateActivity(Type);
          }

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }

      return false;
    } // IsExpired

    #endregion

    #region Private Functions
    
    /// <summary>
    /// Reset activities
    /// </summary>
    private void InitializeActivities()
    {
      // Only RequestParams are requested inmediately 
      this.TimeRequestParams = DateTime.MinValue;

      // Wait for next time to request
      this.TimeSendParams = DateTime.Now;
      this.TimeShowVideo = DateTime.Now.AddMilliseconds(Constants.DEFAULT_SHOW_VIDEO_TIME);
      this.TimeRefreshBrowserBlockReasons = DateTime.Now;
    } // InitializeActivities

    /// <summary>
    /// GetActivity
    /// </summary>
    /// <param name="Activity"></param>
    /// <returns></returns>
    private DateTime GetActivity(TYPE_ACTIVITY Activity)
    {
      DateTime _time;

      _time = DateTime.Now;

      try
      {
        switch (Activity)
        {
          case TYPE_ACTIVITY.RequestParams:
            _time = this.TimeRequestParams;
            break;

          case TYPE_ACTIVITY.SendParams:
            _time = this.TimeSendParams;
            break;

          case TYPE_ACTIVITY.RefreshBrowserBlockReasons:
            _time = this.TimeRefreshBrowserBlockReasons;
            break;

          case TYPE_ACTIVITY.ShowHideVideo:
            _time = this.TimeShowVideo;
            break;
            
          default:
            Log.Add(TYPE_MESSAGE.WARNING, "Type activity not defined in 'GetActivity()': '" + Activity.ToString() + "'");
            break;
        }
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }

      return _time;
    } // GetActivity

    /// <summary>
    /// Update activity
    /// </summary>
    /// <param name="Activity"></param>
    private void UpdateActivity(TYPE_ACTIVITY Activity)
    {
      try
      {
        this.UpdateActivity(Activity, DateTime.Now);
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // UpdateActivity

    #endregion

    /// <summary>
    /// Update activity
    /// </summary>
    /// <param name="Activity"></param>
    /// <param name="Datetime"></param>
    internal void UpdateActivity(TYPE_ACTIVITY Activity, DateTime Datetime)
    {
      try
      {
        switch (Activity)
        {
          case TYPE_ACTIVITY.RequestParams:
            this.TimeRequestParams = Datetime;
            break;

          case TYPE_ACTIVITY.SendParams:
            this.TimeSendParams = Datetime;
            break;

          case TYPE_ACTIVITY.RefreshBrowserBlockReasons:
            this.TimeRefreshBrowserBlockReasons = Datetime;
            break;

          case TYPE_ACTIVITY.ShowHideVideo:
            this.TimeShowVideo = Datetime;
            break;

          default:
            Log.Add(TYPE_MESSAGE.WARNING, "Type activity not defined in 'UpdateActivity()': '" + Activity.ToString() + "'");
            break;
        }
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // UpdateActivity
  }
}
