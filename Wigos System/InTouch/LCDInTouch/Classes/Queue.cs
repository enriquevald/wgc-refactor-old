//------------------------------------------------------------------------------
// Copyright � 2014 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Queue.cs
// 
//   DESCRIPTION: Queue clase. 
// 
//        AUTHOR: Javi Barea
// 
// CREATION DATE: 03-JUL-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 03-JUL-2014 JBP    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;

namespace LCDInTouch
{

  #region LCD_Queue

  public class LCD_Queue : Queue<LCD_Message>
  {

    #region Variables
    
    private System.Threading.AutoResetEvent m_event = new System.Threading.AutoResetEvent(true);

    #endregion

    #region Properties

    #endregion

    #region Public Functions

    public LCD_Queue()
    {

    }    

    public new void Enqueue(LCD_Message M)
    {      
      base.Enqueue(M);
      this.m_event.Set();
    }

    public Boolean WaitDataQueued(Int32 Timeout)
    {
      return this.m_event.WaitOne(Timeout);
    }

    public void Enqueue(MESSAGE_SOURCE Source, TYPE_MESSAGE Type, String IdNLS)
    {
      Log.Add(TYPE_MESSAGE.DEVELOPMENT_LVL3, String.Format("Source[{0}], Type[{1}], IdNLS[{2}]", Source.ToString(), Type.ToString(), IdNLS));
      this.Enqueue(new LCD_Message(Source, Type, IdNLS, IdNLS));
    }

    public void Enqueue(MESSAGE_SOURCE Source, TYPE_MESSAGE Type, String IdNLS, String DefaultIdNLS)
    {
      Log.Add(TYPE_MESSAGE.DEVELOPMENT_LVL3,String.Format("Source[{0}], Type[{1}], IdNLS[{2}]", Source.ToString(), Type.ToString(), IdNLS));
      this.Enqueue(new LCD_Message(Source, Type, IdNLS, DefaultIdNLS));
    }

    // SAS Host Messages
    public void Enqueue(TYPE_MESSAGE Type, String Message, Int32 MaxTime)
    {
      Log.Add(TYPE_MESSAGE.DEVELOPMENT_LVL3,String.Format("Type[{0}], Message[{1}], MaxTime[{2}]", Type.ToString(), Message, MaxTime.ToString()));      
      this.Enqueue(new LCD_Message(Type, Message, MaxTime));
    }

    #endregion
  }
  
  #endregion

  #region LCD_Message

  public class LCD_Message
  {
    #region Variables

    static private Int32 m_transaction_counter = Int32.MinValue;

    private Int32 m_trans;
    private MESSAGE_SOURCE m_source;
    private TYPE_MESSAGE m_type;
    private TYPE_MESSAGE_PRIORITY m_priority;
    private String m_text;
    private String m_default_text;
    private Int32 m_min_time;
    private Int32 m_max_time;
    private Boolean m_is_nls;
    private String m_nls;

    #endregion

    #region Properties

    public Int32 Transaction
    {
      get { return this.m_trans; }
    }

    public TYPE_MESSAGE Type
    {
      get { return this.m_type; }
      set { this.m_type = value; }
    }

    public MESSAGE_SOURCE Source
    {
      get { return this.m_source; }
      set { this.m_source = value; }
    }

    public TYPE_MESSAGE_PRIORITY Priority
    {
      get { return this.m_priority; }
      set { this.m_priority = value; }
    }

    public String Text
    {
      get { return this.m_text; }
      set { this.m_text = value; }
    }

    public Int32 MaxTime
    {
      get { return this.m_max_time; }
      set { this.m_max_time = value; }
    }

    public Int32 MinTime
    {
      get { return this.m_min_time; }
      set { this.m_min_time = value; }
    }

    public Boolean IsNLS
    {
      get { return this.m_is_nls; }
      set { this.m_is_nls = value; }
    }

    public String NLS
    {
      get { return this.m_nls; }
      set { this.m_nls = value; }
    }

    #endregion

    #region Public Functions

    // SASHost Messages from 'CardMessage_ProtocolResponse'
    public LCD_Message(TYPE_MESSAGE TypeMessage, String Message)
    {      
      this.Init ( MESSAGE_SOURCE.SAS_HOST
                , TypeMessage
                , Message
                , String.Empty
                , TYPE_MESSAGE_PRIORITY.NONE
                , Constants.DEFAULT_MINTIME_SHOWING_MESSAGE
                , GetDefaultMaxTimeValue(TypeMessage)
                , false );
    }

    // SAS Host Messages from 'ShowMessage_MsgInterface' and "CloseAllMessages'
    public LCD_Message(TYPE_MESSAGE TypeMessage, String Message, Int32 MaxTime)
    {
      this.Init(  MESSAGE_SOURCE.SAS_HOST
                , TypeMessage
                , Message
                , String.Empty
                , TYPE_MESSAGE_PRIORITY.NONE
                , MaxTime
                , MaxTime
                , false);
    }

    // From responses of SAS Host
    public LCD_Message(MESSAGE_SOURCE Source, TYPE_MESSAGE TypeMessage, String IdNLS)
    {
      this.Init(Source
                , TypeMessage
                , IdNLS
                , String.Empty
                , TYPE_MESSAGE_PRIORITY.NONE
                , Constants.DEFAULT_MINTIME_SHOWING_MESSAGE
                , GetDefaultMaxTimeValue(TypeMessage)
                , true);
    }
    public LCD_Message(MESSAGE_SOURCE Source, TYPE_MESSAGE TypeMessage, String IdNLS, String DefaultIdNLS)
    {
      this.Init ( Source
                , TypeMessage
                , IdNLS
                , DefaultIdNLS
                , TYPE_MESSAGE_PRIORITY.NONE
                , Constants.DEFAULT_MINTIME_SHOWING_MESSAGE
                , GetDefaultMaxTimeValue(TypeMessage)
                , true );
    }

    // Timeout messages
    public LCD_Message(TYPE_MESSAGE TypeMessage, String IdNLS, TYPE_MESSAGE_PRIORITY Priority)
    {
      this.Init ( Source
                , TypeMessage
                , IdNLS
                , String.Empty
                , Priority
                , Constants.DEFAULT_MINTIME_SHOWING_MESSAGE
                , GetDefaultMaxTimeValue(TypeMessage)
                , true );
    }
    public LCD_Message(TYPE_MESSAGE TypeMessage, String IdNLS, String DefaultIdNLS, TYPE_MESSAGE_PRIORITY Priority)
    {
      this.Init ( Source
                , TypeMessage
                , IdNLS
                , DefaultIdNLS
                , Priority
                , Constants.DEFAULT_MINTIME_SHOWING_MESSAGE
                , GetDefaultMaxTimeValue(TypeMessage)
                , true );
    }
    
    // Get Methods from LCD to SAS Host Messages
    public LCD_Message(MESSAGE_SOURCE Source, TYPE_MESSAGE TypeMessage, String IdNLS, Int32 MaxTime)
    {
      this.Init(Source
                , TypeMessage
                , IdNLS
                , String.Empty
                , TYPE_MESSAGE_PRIORITY.NONE
                , Constants.DEFAULT_MINTIME_SHOWING_MESSAGE
                , MaxTime
                , true);
    }
    public LCD_Message(MESSAGE_SOURCE Source, TYPE_MESSAGE TypeMessage, String IdNLS, String DefaultIdNLS, Int32 MaxTime)
    {
      this.Init ( Source
                , TypeMessage
                , IdNLS
                , DefaultIdNLS
                , TYPE_MESSAGE_PRIORITY.NONE
                , Constants.DEFAULT_MINTIME_SHOWING_MESSAGE
                , MaxTime
                , true );
    }

    #endregion 

    #region Private Functions

    public Int32 GetDefaultMaxTimeValue(TYPE_MESSAGE TypeMessage)
    {
      if (TypeMessage == TYPE_MESSAGE.CLOSE)
      {
        return Constants.DEFAULT_CLOSING_MESSAGE;
      }

      return (TypeMessage == TYPE_MESSAGE.ERROR) ? Constants.DEFAULT_MAXTIME_ERROR_SHOWING_MESSAGE
                                                 : Constants.DEFAULT_MAXTIME_SHOWING_MESSAGE;
    }

    private Int32 NewTransaction()
    {
      m_transaction_counter = Math.Max(m_transaction_counter, 0);
      m_transaction_counter++;

      return m_transaction_counter;
    }

    private void Init(MESSAGE_SOURCE Source, TYPE_MESSAGE TypeMessage, String Message, String DefaultIdNLS, TYPE_MESSAGE_PRIORITY Priority, Int32 MinTime, Int32 MaxTime, Boolean IsNLS)
    {
      string _default_message;

      _default_message = String.Empty;
      this.m_trans    = this.NewTransaction();

      if (MinTime > MaxTime)
      {
        MinTime = MaxTime;
      }

      if (TypeMessage == TYPE_MESSAGE.INSERT_CARD && Message != Constants.MESSAGE_INSERT_CARD)
      {
        IsNLS = false;
      }

      this.m_source       = Source;
      this.m_type         = TypeMessage;
      this.m_priority     = Priority;
      this.m_text         = Message;
      this.m_default_text = DefaultIdNLS;
      this.m_min_time     = MinTime;
      this.m_max_time     = MaxTime;
      this.m_is_nls       = IsNLS;
      
      if (IsNLS)
      {
        if (DefaultIdNLS != String.Empty)
        {
          _default_message = Resource.GetString(DefaultIdNLS);
        }

        this.m_text = Resource.GetString(Message, _default_message);
        this.m_nls  = Message;
      }
    }

    #endregion

  }

  #endregion

}