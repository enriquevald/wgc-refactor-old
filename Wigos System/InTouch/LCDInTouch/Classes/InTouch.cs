﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//      MODULE NAME: InTouch.cs
// 
//      DESCRIPTION: Class for LCDInTouch
// 
//           AUTHOR: Javier Barea
// 
//    CREATION DATE: 26-JUN-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 26-JUN-2017 JBP    First release.
//------------------------------------------------------------------------------
using LCD_Display;
using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;

namespace LCDInTouch
{

  public class InTouch : ILCD
  {

    #region " Members "

    private Boolean m_in_session;

    #endregion

    #region " Properties "

    public KeyPressed KeyHandler { get; set; }
    public ProtocolRequest ProtocolHandler { get; set; }
    public FormatNumberToString FormatNumberHandler { get; set; }
    internal LCDConfiguration Config { get; set; }
    public Boolean StopInTouchThread { get { return false; } }
    public DateTime TimeToAskCardStatus { get; set; }
    public Boolean MainFormRunning { get; set; }
    public frm_base MainForm { get; set; }
    public TYPE_CARD CardType { get; set; }
    public Boolean TransferInProgress { get; set; }
    public Thread InTouchThread { get; set; }
    public LCD_PARAMS Params { get; set; }
    public UInt16 DefaultLanguage { get; set; }
    public LanguageIdentifier LanguageIdentifier { get; set; }
    public LCD_TERMINAL_DRAW_PARAMS_PARTICIPATE TerminalDrawParamsParticipate { get; set; }
    public LCD_TERMINAL_DRAW_DRAW_PARAMS TerminalDrawParams { get; set; }
    public DateTime TimeToAskParam { get; set; }
    internal Boolean ConnectionWCP { get; set; }
    internal Boolean IsCardRemovedInProgress { get; set; }
    internal Boolean IsGameInProgress { get; set; }
    internal Boolean IsCardRemoved { get; set; }
    public int BlockReasonId { get; set; }
    public string PlayerInfo { get; set; }
    public String AccountId { get; set; }
    public String CardErrorMessage { get; set; }
    public String WelcomeMessageLine1 { get; set; }
    public String WelcomeMessageLine2 { get; set; }
    public String CardMessage { get; set; }
    public Boolean PendingLogout { get; set; }
    public Boolean InSession
    {
      get { return (this.m_in_session || this.PendingLogout); }
      set { this.m_in_session = value; }
    }

    #endregion

    #region " Public Methods "

    /// <summary>
    /// Constructor 
    /// </summary>
    public InTouch() { ; }

    /// <summary>
    /// InTouch Form Thread
    /// </summary>
    public void InTouchFormThread()
    {

      if (!String.IsNullOrEmpty(Environment.GetEnvironmentVariable("DEVELOPMENT")))
      {
        Log.Add(TYPE_MESSAGE.WARNING, "DEVELOPMENT");
      }

      this.TimeToAskCardStatus = DateTime.Now.AddDays(1);

      // Infinite thread
      while (!this.StopInTouchThread)
      {
        try
        {
          this.StartMainForm();
        }
        catch (Exception _ex)
        {
          this.MainFormRunning = false;

          Log.Add(_ex);
        }
        finally
        {
          Log.Add(TYPE_MESSAGE.INFO, "Application end, enter in finally section");

          this.EndMainForm();
        }
      }
    } // LCDFormThread

    #region " Initialization Methods "

    /// <summary>
    /// LCD Init thread
    /// </summary>
    public void Init(int PortNumber)
    {
      this.StartMainThread();
    } // Init

    /// <summary>
    /// Initialization
    /// </summary>
    public void Initialization()
    {
      try
      {
        // Connection WCP = true by default. If exist BlockReasons, ConnectionWCP is set to false.
        this.ConnectionWCP = true;
        this.DefaultLanguage = 0;
        this.Config = new LCDConfiguration(this);
        this.MainForm = new frm_base(this);
        this.HideCursor();
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // SetLanguage

    /// <summary>
    /// End initialization
    /// </summary>
    public void EndInitialization()
    {
      try
      {
        // TODO: Others...

        Log.Add(TYPE_MESSAGE.INFO, "Initialized.");
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // EndInitialization

    #endregion

    #region " Protocol Response "

    /// <summary>
    /// Protocol Response 
    /// </summary>
    /// <param name="FunctionCode">Id of function</param>
    /// <param name="Result">Result OK, FAIL, etc...</param>
    /// <param name="Data">Data Pointer. Info must parse.</param>
    public void ProtocolResponse(int FunctionCode, int Result, IntPtr Data)
    {
      try
      {
        if (this.MainForm == null || (this.MainForm.IsLoading() && !this.IsKeyboardRequest((TYPE_REQUEST)FunctionCode)))
        {
          return;
        }

        Log.Add(TYPE_MESSAGE.DEVELOPMENT_LVL2, String.Format("ProtocolResponse [{0}][Result={1}]", ((TYPE_REQUEST)FunctionCode).ToString(), Result));

        switch ((TYPE_REQUEST)FunctionCode)
        {
          case TYPE_REQUEST.MESSAGE_CODE:
            this.MessageCode_ProtocolResponse(Data, Result);
            break;

          case TYPE_REQUEST.BLOCK_REASON:
            this.BlockReason_ProtocolResponse(Result);
            break;

          case TYPE_REQUEST.CARD_MESSAGE:
            this.CardMessage_ProtocolResponse(Data, Result);
            break;

          case TYPE_REQUEST.CARD_STATUS:
            this.CardStatus_ProtocolResponse(this.ProtocolResponseDataToString(Data), Result);
            break;

          case TYPE_REQUEST.TERMINAL_DRAW_ASK_FOR_PARTICIPATE:
            this.TerminalDrawAskForParticipate_ProtocolResponse(Data);
            break;

          case TYPE_REQUEST.TERMINAL_DRAW_PARTICIPATE:
            this.TerminalDrawParticipate_ProtocolResponse(Data);
            break;

          case TYPE_REQUEST.TERMINAL_DRAW_GAME_RESULT:
            this.TerminalDrawShowResult_ProtocolResponse(Data);
            break;

          case TYPE_REQUEST.PARAMS:
            this.ParamsManagement_ProtocolResponse(Data, Result);
            break;

          case TYPE_REQUEST.MOBILE_BANK:
            this.MobileBank_ProtocolResponse(Result);
            break;

          case TYPE_REQUEST.FT_ENTER:
            this.FTEnter_ProtocolResponse();
            break;

          case TYPE_REQUEST.TECH_MENU:
          case TYPE_REQUEST.CHANGE_STACKER:
            this.ChangeStacker_ProtocolResponse(Result, FunctionCode);
            break;

          case TYPE_REQUEST.TRANSFER_CARD_CREDIT_TO_MACHINE:
            this.TransferCardCreditToMachine_ProtocolResponse(Data, Result);
            break;

          case TYPE_REQUEST.SHUTDOWN:
            this.ShutDown_ProtocolResponse();
            break;

          case TYPE_REQUEST.TERMINAL_ACTIVE:
            this.TerminalActive_ProtocolResponse(Result);
            break;

          default:
            Log.Add(TYPE_MESSAGE.DEVELOPMENT_LVL2, String.Format("Not implemented: {0}", ((TYPE_REQUEST)FunctionCode).ToString()));
            break;
        }
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
      finally
      {
      }

    } // ProtocolResponse

    #endregion

    #region " MsgInterface Overloads "

    /// <summary>
    /// LCD Message Inteface
    /// </summary>
    /// <param name="Type"></param>
    /// <returns></returns>
    public Boolean MsgInterface(int Type)
    {
      return this.MsgInterface(Type, null);
    } // MsgInterface
    public Boolean MsgInterface(int Type, String Params)
    {
      try
      {
        switch ((TYPE_EVENT)Type)
        {
          case TYPE_EVENT.OPERATOR_MESSAGE:
            // this.Parameters only contains operator message.
            this.OperatorMessage_MsgInterface(this.Parameters(Params));
            break;

          default:
            Log.Add(TYPE_MESSAGE.DEVELOPMENT_LVL1, "MsgInterface [" + ((TYPE_EVENT)Type).ToString() + "][" + Params + "]");
            break;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }

      return false;
    } // MsgInterface

    #endregion

    #region " IInTouch Members "

    /// <summary>
    /// Set key pressed handler.
    /// </summary>
    /// <param name="Handler"></param>
    public void SetKeyPressedHandler(IntPtr Handler)
    {
      this.KeyHandler = (LCD_Display.KeyPressed)Marshal.GetDelegateForFunctionPointer(Handler, typeof(KeyPressed));
    } // SetKeyPressedHandler

    /// <summary>
    /// Protocol request handler.
    /// </summary>
    /// <param name="Handler"></param>
    public void ProtocolRequestHandler(IntPtr Handler)
    {
      this.ProtocolHandler = (LCD_Display.ProtocolRequest)Marshal.GetDelegateForFunctionPointer(Handler, typeof(ProtocolRequest));
    } // ProtocolRequestHandler

    /// <summary>
    /// Format Number To String handler.
    /// </summary>
    /// <param name="Handler"></param>
    public void FormatNumberToStringHandler(IntPtr Handler)
    {
      this.FormatNumberHandler = (LCD_Display.FormatNumberToString)Marshal.GetDelegateForFunctionPointer(Handler, typeof(FormatNumberToString));
    } // FormatNumberToStringHandler

    /// <summary>
    /// Protocol send handler.
    /// </summary>
    /// <param name="Request"></param>
    /// <param name="ProtocolData"></param>
    public void SendProtocolHandler(TYPE_REQUEST Request, String ProtocolData)
    {
      try
      {
        // Development log
        Log.Add(TYPE_MESSAGE.DEVELOPMENT_LVL2, String.Format("SendProtocolHandler({0}[{1}], [{2}])", Request.ToString(), ((int)Request).ToString(), ProtocolData));

        switch (Request)
        {
          // Transitional actions
          case TYPE_REQUEST.TRANSFER_CARD_CREDIT_TO_MACHINE:
            this.TransferInProgress = true;
            break;

          // Final actions
          case TYPE_REQUEST.TECH_MENU:
            this.ShowTechMenu();

            return;

          case TYPE_REQUEST.PLAYER_LANGUAGE_ID:
            this.SetLanguage(ProtocolData);

            return;

          case TYPE_REQUEST.PENDING_LOGOUT:
            this.PendingLogoutManager();
            break;
        }

        // Send request to SASHost
        this.ProtocolHandler((int)Request, ProtocolData);

      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // SendProtocolHandler

    /// <summary>
    /// Convert number to a string formatted number
    /// </summary>
    /// <param name="Amount"></param>
    /// <param name="ShowCurrencySymbol"></param>
    /// <param name="ShowDecimals"></param>
    /// <param name="AmountIsInCents"></param>
    /// <returns></returns>
    public String NumberToFormattedString(UInt64 Amount, Boolean ShowCurrencySymbol, Boolean ShowDecimals, Boolean AmountIsInCents)
    {
      IntPtr _return_pointer;
      String _formatted_number;
      if (FormatNumberHandler == null)
      {
        return "";
      }

      _return_pointer = this.FormatNumberHandler(Amount, ShowCurrencySymbol, ShowDecimals, AmountIsInCents);
      _formatted_number = Marshal.PtrToStringBSTR(_return_pointer);
      Marshal.FreeBSTR(_return_pointer);

      return _formatted_number;
    } // NumberToFormattedString

    //------------------------------------------------------------------------------
    //  PURPOSE: Parameters from SAS-HOST
    //  PARAMS:
    //      - INPUT:
    //          · Parameters
    //  RETURNS: Formatted Parameters.
    public String Parameters(String Params)
    {
      return (String.IsNullOrEmpty(Params)) ? " " : Params;
    } // Parameters

    /// <summary>
    /// Set LCD Language.
    /// </summary>
    public void SetCurrentLanguage()
    {
      LCD_PARAMS _params;

      try
      {
        _params = this.Params;

        // "Initialize LanguageId if its null".
        if ((LanguageIdentifier)_params.LanguageId != LanguageIdentifier.Spanish)
        {
          _params.LanguageId = (int)LanguageIdentifier.English;
        }

        this.Params = _params;
        this.LanguageIdentifier = (LanguageIdentifier)this.Params.LanguageId;
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // SetCurrentLanguage

    #endregion

    #region " Block Reasons "

    /// <summary>
    /// Manage block reason and show in LCD 
    /// </summary>
    public void ShowBlockReason()
    {
      this.ShowBlockReason(false);
    } // ShowBlockReason
    public void ShowBlockReason(Boolean ForzeForward)
    {
      TYPE_BLOCK_REASON _reason;
      String _block_reason;
      Boolean _conection_wcp;

      _conection_wcp = true;

      _reason = (TYPE_BLOCK_REASON)Enum.Parse(typeof(TYPE_BLOCK_REASON), this.BlockReasonId.ToString());
      _block_reason = String.Empty;

      if ((TYPE_BLOCK_REASON.E2PROM_ERROR & _reason).Equals(TYPE_BLOCK_REASON.E2PROM_ERROR))
      {
        _block_reason = this.SetBlockReasonMsg(_block_reason, "BLOCK_REASON_E2PROM_ERROR");
      }
      if ((TYPE_BLOCK_REASON.ERROR & _reason).Equals(TYPE_BLOCK_REASON.ERROR))
      {
        _block_reason = this.SetBlockReasonMsg(_block_reason, "BLOCK_REASON_ERROR");
      }
      if ((TYPE_BLOCK_REASON.NOT_ASSIGNED & _reason).Equals(TYPE_BLOCK_REASON.NOT_ASSIGNED))
      {
        _block_reason = this.SetBlockReasonMsg(_block_reason, "BLOCK_REASON_NOT_ASSIGNED");
      }
      if ((TYPE_BLOCK_REASON.RNG_MODE & _reason).Equals(TYPE_BLOCK_REASON.RNG_MODE))
      {
        _block_reason = this.SetBlockReasonMsg(_block_reason, "BLOCK_REASON_RNG_MODE");
        _conection_wcp = false;
      }
      if ((TYPE_BLOCK_REASON.REQUESTED_BY_LKAS & _reason).Equals(TYPE_BLOCK_REASON.REQUESTED_BY_LKAS))
      {
        _block_reason = this.SetBlockReasonMsg(_block_reason, "BLOCK_REASON_REQUESTED_BY_LKAS");
      }
      if ((TYPE_BLOCK_REASON.REQUESTED_BY_LKT & _reason).Equals(TYPE_BLOCK_REASON.REQUESTED_BY_LKT))
      {
        _block_reason = this.SetBlockReasonMsg(_block_reason, "BLOCK_REASON_REQUESTED_BY_LKT");
      }
      if ((TYPE_BLOCK_REASON.DISCONNECTED & _reason).Equals(TYPE_BLOCK_REASON.DISCONNECTED))
      {
        _block_reason = this.SetBlockReasonMsg(_block_reason, "BLOCK_REASON_DISCONNECTED");
        _conection_wcp = false;
      }
      if ((TYPE_BLOCK_REASON.DATABASE_ERROR & _reason).Equals(TYPE_BLOCK_REASON.DATABASE_ERROR))
      {
        _block_reason = this.SetBlockReasonMsg(_block_reason, "BLOCK_REASON_DATABASE_ERROR");
      }
      if ((TYPE_BLOCK_REASON.SHUTTING_DOWN & _reason).Equals(TYPE_BLOCK_REASON.SHUTTING_DOWN))
      {
        _block_reason = this.SetBlockReasonMsg(_block_reason, "BLOCK_REASON_SHUTTING_DOWN");
        _conection_wcp = false;
      }
      if ((TYPE_BLOCK_REASON.MIO_OFFLINE & _reason).Equals(TYPE_BLOCK_REASON.MIO_OFFLINE))
      {
        _block_reason = this.SetBlockReasonMsg(_block_reason, "BLOCK_REASON_MIO_OFFLINE");
      }
      if ((TYPE_BLOCK_REASON.SALE_NOT_ALLOWED & _reason).Equals(TYPE_BLOCK_REASON.SALE_NOT_ALLOWED))
      {
        _block_reason = this.SetBlockReasonMsg(_block_reason, "BLOCK_REASON_SALE_NOT_ALLOWED");
      }
      if ((TYPE_BLOCK_REASON.VERSION_STATUS_ERROR & _reason).Equals(TYPE_BLOCK_REASON.VERSION_STATUS_ERROR))
      {
        _block_reason = this.SetBlockReasonMsg(_block_reason, "BLOCK_REASON_VERSION_STATUS_ERROR");
      }
      if ((TYPE_BLOCK_REASON.PRINTER_ERROR & _reason).Equals(TYPE_BLOCK_REASON.PRINTER_ERROR))
      {
        _block_reason = this.SetBlockReasonMsg(_block_reason, "BLOCK_REASON_PRINTER_ERROR");
      }
      if ((TYPE_BLOCK_REASON.INTRUSION_DETECTION & _reason).Equals(TYPE_BLOCK_REASON.INTRUSION_DETECTION))
      {
        _block_reason = this.SetBlockReasonMsg(_block_reason, "BLOCK_REASON_INTRUSION_DETECTION");
      }
      if ((TYPE_BLOCK_REASON.SAS_HOST_DISCONNECTED & _reason).Equals(TYPE_BLOCK_REASON.SAS_HOST_DISCONNECTED))
      {
        _block_reason = this.SetBlockReasonMsg(_block_reason, "BLOCK_REASON_SAS_HOST_DISCONNECTED");
      }
      if ((TYPE_BLOCK_REASON.CARD_READER_ERROR & _reason).Equals(TYPE_BLOCK_REASON.CARD_READER_ERROR))
      {
        _block_reason = this.SetBlockReasonMsg(_block_reason, "BLOCK_REASON_CARD_READER_ERROR");
      }
      if ((TYPE_BLOCK_REASON.SAS_CLIENT_DISCONNECTED & _reason).Equals(TYPE_BLOCK_REASON.SAS_CLIENT_DISCONNECTED))
      {
        _block_reason = this.SetBlockReasonMsg(_block_reason, "BLOCK_REASON_SAS_CLIENT_DISCONNECTED");
        _conection_wcp = false;
      }
      if ((TYPE_BLOCK_REASON.SAS_CLIENT_SHUTDOWN & _reason).Equals(TYPE_BLOCK_REASON.SAS_CLIENT_SHUTDOWN))
      {
        _block_reason = this.SetBlockReasonMsg(_block_reason, "BLOCK_REASON_SAS_CLIENT_SHUTDOWN");
      }
      if ((TYPE_BLOCK_REASON.SAS_CLIENT_AFT_LOCK & _reason).Equals(TYPE_BLOCK_REASON.SAS_CLIENT_AFT_LOCK))
      {
        _block_reason = this.SetBlockReasonMsg(_block_reason, "BLOCK_REASON_SAS_CLIENT_AFT_LOCK");
      }
      if ((TYPE_BLOCK_REASON.FACTORY_TEST_ENTERED & _reason).Equals(TYPE_BLOCK_REASON.FACTORY_TEST_ENTERED))
      {
        _block_reason = this.SetBlockReasonMsg(_block_reason, "BLOCK_REASON_FACTORY_TEST_ENTERED");
      }
      if ((TYPE_BLOCK_REASON.DOOR_OPENED & _reason).Equals(TYPE_BLOCK_REASON.DOOR_OPENED))
      {
        _block_reason = this.SetBlockReasonMsg(_block_reason, "BLOCK_REASON_DOOR_OPENED");
      }
      if ((TYPE_BLOCK_REASON.HANDPAY & _reason).Equals(TYPE_BLOCK_REASON.HANDPAY))
      {
        _block_reason = this.SetBlockReasonMsg(_block_reason, "BLOCK_REASON_HANDPAY");
      }
      if ((TYPE_BLOCK_REASON.LOADING & _reason).Equals(TYPE_BLOCK_REASON.LOADING))
      {
        _block_reason = this.SetBlockReasonMsg(_block_reason, "BLOCK_REASON_LOADING");
      }

      // Send block reasons to web
      this.MainForm.Browser.SendProtocolResponse(TYPE_REQUEST.BLOCK_REASON, this.BlockReasonId, _block_reason, ForzeForward);

      if (!this.ConnectionWCP && _conection_wcp)
      {
        this.TimeToAskParam = DateTime.Now.AddMilliseconds(-Constants.TIME_TO_ASK_PARAMS);
      }

      this.ConnectionWCP = _conection_wcp;

    } // ShowBlockReason

    /// <summary>
    /// Initialization of time to ask card status. 
    /// </summary>
    internal void InitializeTimeToAskCardStatus()
    {
      if (this.ConnectionWCP)
      {
        this.TimeToAskCardStatus = DateTime.Now.AddDays(1);//Para que no vuelva ha entrar mientras se hace la petición
        this.SendProtocolHandler(TYPE_REQUEST.CARD_STATUS, String.Empty);
        this.TimeToAskCardStatus = DateTime.Now;
      }
    } // InitializeTimeToAskCardStatus

    /// <summary>
    /// Initialization of time to ask card status. 
    /// </summary>
    internal void LogoutManagement()
    {
      try
      {
        this.KeyHandler(0x08);

        if (this.MainForm.KeyboardEnabled && this.MainForm.uc_keyboard.OperatorText.Contains("Insert"))
        {
          this.MainForm.ExitKeyboard(false);
        }
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // LogoutManagement

    #endregion

    #endregion

    #region " Internal Methods "

    /// <summary>
    /// Set terminal draw participate params
    /// </summary>
    /// <param name="Data"></param>
    /// <param name="Params"></param>
    /// <returns></returns>
    private Boolean SetTerminalDrawParticipateParams(IntPtr Data, out String Params)
    {
      Params = String.Empty;
      this.IsGameInProgress = true;

      try
      {
        if (Data != null && Data != IntPtr.Zero)
        {
          this.TerminalDrawParamsParticipate = (LCD_TERMINAL_DRAW_PARAMS_PARTICIPATE)Marshal.PtrToStructure(Data, typeof(LCD_TERMINAL_DRAW_PARAMS_PARTICIPATE));
        }

        // Used for filter request to InTouch Web
        Params = String.Format(Constants.TERMINAL_DRAW_PARTICIPATE_PARAMS_FORMAT
                      , this.TerminalDrawParamsParticipate.ToString()
                      , this.TerminalDrawParamsParticipate.GameId()
                      , this.TerminalDrawParamsParticipate.account_id);

        return true;
      }
      catch (Exception _ex)
      {
        this.IsGameInProgress = false;

        Log.Add(TYPE_MESSAGE.ERROR, "Error on SetTerminalDrawParticipateParams(). Error on convert Data.");
        Log.Add(_ex);
      }

      return false;
    } // SetTerminalDrawParticipateParams

    /// <summary>
    /// Show "definitive" Messages (Insert Card & Close all).  
    /// </summary>
    /// <param name="Data"></param>
    /// <param name="Result"></param> 
    internal void MessageCode_ProtocolResponse(IntPtr Data, int Result)
    {
      try
      {
        if (this.MainForm == null)
        {
          return;
        }

        switch ((TYPE_MESSAGE_CODE)Result)
        {
          case TYPE_MESSAGE_CODE.INSERT_CARD:
            this.SetWelcomeMessage(Data);

            break;

          case TYPE_MESSAGE_CODE.CLEAR:
          case TYPE_MESSAGE_CODE.MESSAGE_INFO:
          case TYPE_MESSAGE_CODE.MESSAGE_ERROR:
            break;

          default:
            Log.Add(TYPE_MESSAGE.WARNING, "Message not defined in 'MessageCode_ProtocolResponse()': '" + Result.ToString() + "'");
            break;
        }
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // MessageCode_ProtocolResponse

    /// <summary>
    /// Block reason management in Protocol Responses.
    /// </summary>
    /// <param name="Result"></param>
    internal void BlockReason_ProtocolResponse(int Result)
    {
      try
      {
        if (Result != this.BlockReasonId)
        {
          this.BlockReasonId = Result;
          this.ShowBlockReason();
        }

        // Test: TODO REMOVE IF NOT WORK
        this.ConnectionWCP = (Result == 0);
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // BlockReason_ProtocolResponse

    /// <summary>
    /// Card Status management in Protocol Responses.
    /// </summary>
    /// <param name="Data"></param>
    /// <param name="Result"></param>
    internal void CardStatus_ProtocolResponse(String Data, int Result)
    {
      try
      {
        switch ((TYPE_CARD_STATUS)Result)
        {
          case TYPE_CARD_STATUS.CARD_INSERTED_VALIDATED:
            this.InsertCard_ProtocolResponse(Data);
            break;

          case TYPE_CARD_STATUS.CARD_REMOVED_VALIDATED:
            this.RemoveCard_ProtocolResponse((TYPE_CARD_STATUS)Result);
            break;

          default:
            this.IsCardRemovedInProgress = false;
            this.IsGameInProgress = false;
            this.TransferInProgress = false;
            break;
        }
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // CardStatus_ProtocolResponse

    /// <summary>
    /// Insert card protocol response
    /// </summary>
    /// <param name="InsertCardData"></param>
    internal void InsertCard_ProtocolResponse(String InsertCardData)
    {
      String[] _insert_card_data;

      this.IsGameInProgress = false;

      // If insert card is InSession (TransferCredit), not need check account
      if (!this.InSession)
      {
        if (String.IsNullOrEmpty(InsertCardData))
        {
          Log.Add(TYPE_MESSAGE.ERROR, "Error on InsertCard_ProtocolResponse(). Invalid AccountId");
          this.MainForm.Browser.SendCardStatus(TYPE_CARD_STATUS.CARD_INSERTED_NOT_VALIDATED);

          return;
        }

        _insert_card_data = InsertCardData.Split(';');

        if (String.IsNullOrEmpty(_insert_card_data[0]))
        {
          Log.Add(TYPE_MESSAGE.ERROR, "Error on InsertCard_ProtocolResponse(). Invalid AccountId");
          this.MainForm.Browser.SendCardStatus(TYPE_CARD_STATUS.CARD_INSERTED_NOT_VALIDATED);

          return;
        }

        this.AccountId = _insert_card_data[0];
      }

      this.InSession = true;
      this.MainForm.Browser.SendCardStatus(TYPE_CARD_STATUS.CARD_INSERTED_VALIDATED, this.AccountId);
    } // InsertCard_ProtocolResponse

    /// <summary>
    /// Remove card protocol response
    /// </summary>
    internal void RemoveCard_ProtocolResponse(TYPE_CARD_STATUS Status)
    {
      this.IsGameInProgress = false;
      this.IsCardRemovedInProgress = true;
      this.InSession = false;
      this.AccountId = String.Empty;
      this.CardErrorMessage = String.Empty;
      this.IsCardRemoved = true;

      this.MainForm.Browser.SendCardStatus(Status);
    } // RemoveCard_ProtocolResponse   

    /// <summary>
    /// Card message management in Protocol Responses.
    /// </summary>
    /// <param name="Data"></param>
    /// <param name="Result"></param> 
    internal void CardMessage_ProtocolResponse(IntPtr Data, int Result)
    {
      TYPE_CARD_STATUS _type_card_status;

      try
      {
        if (this.MainForm == null || this.MainForm.IsSpecialCard)
        {
          return;
        }

        switch ((TYPE_PROTOCOL_MESSAGE)Result)
        {
          case TYPE_PROTOCOL_MESSAGE.INFO:
            if (this.SetCardStatusFromCardMessages(this.ProtocolResponseDataToString(Data), out _type_card_status))
            {
              this.MainForm.Browser.SendCardStatus(_type_card_status);

              return;
            }

            break;

          case TYPE_PROTOCOL_MESSAGE.ERROR:
            if (this.CheckCardErrorMessage(this.ProtocolResponseDataToString(Data)))
            {
              this.MainForm.Browser.SendCardStatus(TYPE_CARD_STATUS.CARD_INSERTED_NOT_VALIDATED, this.CardErrorMessage);

              return;
            }

            break;

          case TYPE_PROTOCOL_MESSAGE.ENTER_AMOUNT:
          case TYPE_PROTOCOL_MESSAGE.ENTER_PIN:
          case TYPE_PROTOCOL_MESSAGE.NONE_IMMEDIATE:
          case TYPE_PROTOCOL_MESSAGE.DEFAULT:
          case TYPE_PROTOCOL_MESSAGE.NONE:
          case TYPE_PROTOCOL_MESSAGE.NO_NOTIFY:
          default:
            // NOTHING TODO
            break;
        }
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // CardMessage_ProtocolResponse

    /// <summary>
    /// Mobile bank management in Protocol Responses.
    /// </summary>
    /// <param name="Result"></param>
    internal void MobileBank_ProtocolResponse(int Result)
    {
      try
      {
        this.MainForm.MobileBank(Result, TYPE_CARD.MOBILE_BANK);
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // MobileBank_ProtocolResponse

    /// <summary>
    /// Tech enter management in Protocol Responses.
    /// </summary>
    internal void FTEnter_ProtocolResponse()
    {
      this.FTEnter_ProtocolResponse(true);
    }
    internal void FTEnter_ProtocolResponse(Boolean FromSASHost)
    {
      try
      {
        if (this.MainForm == null)
        {
          return;
        }

        this.CardType = TYPE_CARD.TECH;
        this.MainForm.ShowMode = SHOW_MODE.TECH;
        this.MainForm.ChangeOnKeyboard = (FromSASHost) ? KB_TYPE.TECH : KB_TYPE.COMPLETE;
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // FTEnter_ProtocolResponse

    /// <summary>
    /// Change stacker management in Protocol Responses.
    /// </summary>
    /// <param name="Result"></param>
    /// <param name="FunctionCode"></param>
    internal void ChangeStacker_ProtocolResponse(int Result, int FunctionCode)
    {
      try
      {
        if (this.MainForm == null)
        {
          return;
        }

        if (this.CardType == TYPE_CARD.UNKNOWN)
        {
          if (this.MainForm.CardTypeMode == TYPE_CARD.UNKNOWN)
          {
            this.MainForm.CardTypeMode = ((TYPE_REQUEST)FunctionCode == TYPE_REQUEST.TECH_MENU) ? TYPE_CARD.TECH : TYPE_CARD.CHANGE_STACKER;
          }
          this.CardType = this.MainForm.CardTypeMode;
        }

        this.MainForm.OperatorLogin(Result, this.CardType);
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // ChangeStacker_ProtocolResponse

    /// <summary>
    /// Shut down management in Protocol Responses.
    /// </summary>
    internal void ShutDown_ProtocolResponse()
    {
      try
      {
        this.MainForm.SetLoading(LOADING_MODE.SHUTDOWN);
        this.MainForm.BrowserBlockReasonsManager(INTOUCH_BLOCK_REASON.SHUTING_DOWN);
        Log.Add(TYPE_MESSAGE.DEVELOPMENT_LVL1, "Shuting down...");
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // ShutDown_ProtocolResponse

    /// <summary>
    /// ProtocolResponse_DataString LCD property.
    ///    - Cast to String of data pointer send by LKT to LCD. 
    /// </summary>
    /// <param name="Data">Data Pointer. Info must parse.</param>
    /// <returns></returns>
    internal String ProtocolResponseDataToString(IntPtr Data)
    {
      if (Data != null && Data != IntPtr.Zero)
      {
        return Marshal.PtrToStringAnsi(Data);
      }
      else
      {
        return String.Empty;
      }
    } // ProtocolResponseDataToString

    /// <summary>
    /// Terminal Draw Ask for participate data management
    /// </summary>
    /// <param name="Data"></param>
    internal void TerminalDrawAskForParticipate_ProtocolResponse(IntPtr Data)
    {
      String _params;

      try
      {
        // Set TerminalDraw Ask Participate params
        if (!this.SetTerminalDrawParticipateParams(Data, out _params))
        {
          Log.Add(TYPE_MESSAGE.ERROR, "Error on TerminalDrawAskForParticipate_ProtocolResponse(). Error on convert Data.");

          return;
        }

        this.MainForm.Browser.SendCardStatus(TYPE_CARD_STATUS.ASK_PENDING_DRAWS, _params);
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // TerminalDrawAskForParticipate_ProtocolResponse

    /// <summary>
    /// Terminal Draw Ask participate data management
    /// </summary>
    /// <param name="Data"></param>
    internal void TerminalDrawParticipate_ProtocolResponse(IntPtr Data)
    {
      String _params;

      try
      {
        // Set TerminalDraw Participate params
        if (!this.SetTerminalDrawParticipateParams(Data, out _params))
        {
          return;
        }

        this.MainForm.Browser.SendCardStatus(TYPE_CARD_STATUS.PENDING_DRAWS, _params);
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // TerminalDrawParticipate_ProtocolResponse

    /// <summary>
    /// Terminal Draw show result data management
    /// </summary>
    /// <param name="Data"></param>
    internal void TerminalDrawShowResult_ProtocolResponse(IntPtr Data)
    {
      Int32 _item_selected;

      try
      {
        if (Data != null && Data != IntPtr.Zero)
        {
          this.TerminalDrawParams = (LCD_TERMINAL_DRAW_DRAW_PARAMS)Marshal.PtrToStructure(Data, typeof(LCD_TERMINAL_DRAW_DRAW_PARAMS));
        }

        // Set selected PromoGame item
        if (!Int32.TryParse(this.MainForm.DrawItemSelected, out _item_selected))
        {
          _item_selected = Constants.DEFAULT_ITEM_SELECTED_PROMOGAME;
        }

        this.MainForm.Browser.SendProtocolResponse(TYPE_REQUEST.TERMINAL_DRAW_GAME_RESULT, _item_selected, this.TerminalDrawParams.ToString());
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // TerminalDrawShowResult_ProtocolResponse

    /// <summary>
    /// Transfer credit to EGM data management
    /// </summary>
    /// <param name="Data"></param>
    /// <param name="Result"></param>
    internal void TransferCardCreditToMachine_ProtocolResponse(IntPtr Data, int Result)
    {
      try
      {
        this.MainForm.Browser.SendProtocolResponse(TYPE_REQUEST.TRANSFER_CARD_CREDIT_TO_MACHINE, Result, this.Params.ToString());
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // TransferCardCreditToMachine_ProtocolResponse

    //------------------------------------------------------------------------------
    // PURPOSE: Show Video when was not Activity.
    // 
    //  PARAMS:
    //      - INPUT:
    //          · Nothing.
    //
    //      - OUTPUT:
    //          · Nothing.
    //
    // RETURNS: Nothing.
    // TRUE --> Terminal actiu  (1)
    // FALSE--> Terminal inactiu (0)
    internal void TerminalActive_ProtocolResponse(int Result)
    {
      try
      {
        //////////////this.SASHOST_Active = false;

        ////////////if (this.MainForm == null || this.MainForm.MainVideo == null)
        ////////////{
        ////////////  return;
        ////////////}

        ////////////if (Result == 1)
        ////////////{
        ////////////  this.MainForm.MainVideo.Close();

        ////////////  //JCA Relacionado con:
        ////////////  //JCA: Buscar: "// JCA: Se vuelve a activar porque si se reproduce el video constantemente se queda sin memoria."
        ////////////  this.MainForm.Activities.UpdateActivity(TYPE_ACTIVITY.AutoShowHideVideo, DateTime.Now);

        ////////////  return;
        ////////////}

        ////////////if (Result == 0
        ////////////  && this.MainForm.ControlsLoaded
        ////////////  && !this.MainForm.VideoIsPlaying)
        ////////////{
        ////////////  //this.MainForm.MainVideo.ShowHideVideoPanel();
        ////////////  this.MainForm.BindContainer(TYPE_BODY.VIDEO);

        ////////////  //JCA Relacionado con:
        ////////////  //JCA: Buscar: "// JCA: Se vuelve a activar porque si se reproduce el video constantemente se queda sin memoria."
        ////////////  this.MainForm.Activities.UpdateActivity(TYPE_ACTIVITY.AutoShowHideVideo, DateTime.Now);
        ////////////}
        ////////////// Exit keyboard. Kentucky on timeout, don't show
        //////////////DLL: Se comenta esta linea porque no deja mostrar el video.
        //////////////this.MainForm.ExitKeyboard(false, false);        
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // TerminalActive_ProtocolResponse

    /// <summary>
    /// Pending Logout Manager
    /// </summary>
    internal void PendingLogoutManager()
    {
      TYPE_CARD_STATUS _status;

      _status = (this.PendingLogout) ? TYPE_CARD_STATUS.CARD_REMOVED_VALIDATED
                                     : TYPE_CARD_STATUS.CARD_NOT_INSERTED;

      this.MainForm.Browser.SendCardStatus(_status, this.CardMessage, true);
    } // PendingLogoutManager

    #region " Params "

    #region " Params Public Methods "
    
    /// <summary>
    /// Request params to WCP.
    /// </summary>
    public void RequestParams(Int32 Interval)
    {
      if ( this.ConnectionWCP
        && this.MainForm.Activity.IsExpired(TYPE_ACTIVITY.RequestParams, Interval))
      {
        this.SendProtocolHandler(TYPE_REQUEST.PARAMS, String.Empty);
        Log.Add(TYPE_MESSAGE.DEVELOPMENT_LVL1, "RequestParams");

        return;
      }
    } // RequestParams

    #endregion

    #region " Params Internal Methods

    /// <summary>
    /// Params management in Protocol Responses.
    /// </summary>
    /// <param name="Data"></param>
    /// <param name="Result"></param>
    internal void ParamsManagement_ProtocolResponse(IntPtr Data, int Result)
    {
      try
      {
        this.SetParams(Data);        
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // ParamsManagement_ProtocolResponse

    /// <summary>
    /// Set params
    /// </summary>
    /// <param name="Data"></param>
    internal Boolean SetParams(IntPtr Data)
    {
      LCD_PARAMS _params;

      try
      {
        if (Data != null && Data != IntPtr.Zero)
        {
          _params = (LCD_PARAMS)Marshal.PtrToStructure(Data, typeof(LCD_PARAMS));

          if (this.DefaultLanguage == 0)
          {
            this.DefaultLanguage = _params.LanguageId;
          }

          _params.LanguageId = this.DefaultLanguage;
          this.Params = _params;
        }
        else
        {
          this.Params = new LCD_PARAMS();
          Log.Add(TYPE_MESSAGE.WARNING, "SetParams(): Data is null or empty");
        }
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }

      return false;
    } // SetParams

    /// <summary>
    /// Params management.
    /// </summary>
    internal void ParamsManagement()
    {
      Int32 _request_interval;
      
      _request_interval = (this.MainForm.IsLoadingMode) ? Constants.TIME_TO_ASK_PARAMS_FAST : Constants.TIME_TO_ASK_PARAMS_SLEEP;

      this.ParamsManagement(_request_interval, Constants.TIME_TO_SEND_PARAMS_SLEEP);
    } // ParamsManagement
    internal void ParamsManagement(Int32 RequestInterval, Int32 SendInterval)
    {
      // Request params to SASHost
      this.RequestParams(RequestInterval);

      // Send current params to web
      this.SendParams(SendInterval);

    } // ParamsManagement

    #endregion

    #region " Params private methods "

    /// <summary>
    /// Send params
    /// </summary>
    /// <param name="SendInterval"></param>
    private void SendParams(Int32 SendInterval)
    {      
      // Only forze request params when players are not in session
      if (!this.InSession
        && this.MainForm.Activity.IsExpired(TYPE_ACTIVITY.SendParams, SendInterval))
      {
        this.MainForm.Browser.SendProtocolResponse(TYPE_REQUEST.PARAMS, 999, this.Params.ToInTouchParamsProtocol(WelcomeMessageLine1, WelcomeMessageLine2), true);
        
        // Set Language in SASHost
        this.ProtocolHandler((int)TYPE_REQUEST.PLAYER_LANGUAGE_ID, this.DefaultLanguage.ToString());
      }
    } // SendParams

    #endregion

    #endregion

    #endregion

    #region " Private Methods "

    #region Threads Methods

    /// <summary>
    /// Start Main Thread
    /// </summary>
    private void StartMainThread()
    {
      this.InTouchThread = new Thread(this.InTouchFormThread);
      this.InTouchThread.SetApartmentState(ApartmentState.STA);
      this.InTouchThread.Start();
    } // StartMainThread

    /// <summary>
    /// Start Form Base
    /// </summary>
    private void StartMainForm()
    {
      try
      {
        this.MainFormRunning = true;

        // Start initialization
        this.Initialization();
        this.EndInitialization();

        // Show LCDTouch
        this.MainForm.ShowDialog();

        // MainForm closed... 
        this.MainFormRunning = false;

        if (this.MainForm.FormThread.IsAlive)
        {
          this.MainForm.FormThread.Abort();
        }

        if (this.MainForm.BrowserThread.IsAlive)
        {
          this.MainForm.BrowserThread.Abort();
        }

        if (this.MainForm.VideoThread.IsAlive)
        {
          this.MainForm.VideoThread.Abort();
        }

        GC.Collect();
        GC.WaitForPendingFinalizers();
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
        throw new Exception("Error ocurred in 'MainFormThread()': " + _ex.Message);
      }
    } // StartMainForm

    /// <summary>
    /// End main form 
    /// </summary>
    private void EndMainForm()
    {
      try
      {
        if (this.MainForm != null)
        {
          this.MainFormRunning = false;
          this.MainForm.Dispose();
        }

        System.GC.Collect();
        System.GC.WaitForPendingFinalizers();
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // EndMainForm

    #endregion

    /// <summary>
    /// Checks if is Keyboard request
    /// </summary>
    /// <param name="Request"></param>
    /// <returns></returns>
    private Boolean IsKeyboardRequest(TYPE_REQUEST Request)
    {
      return ( Request == TYPE_REQUEST.PARAMS 
            || Request == TYPE_REQUEST.FT_ENTER 
            || Request == TYPE_REQUEST.MOBILE_BANK
            || Request == TYPE_REQUEST.TECH_MENU
            || Request == TYPE_REQUEST.CHANGE_STACKER
            || Request == TYPE_REQUEST.SHUTDOWN
            || Request == TYPE_REQUEST.TERMINAL_ACTIVE);
    } // IsKeyboardRequest

    /// <summary>
    /// Check Error Messages
    /// </summary>
    /// <param name="ErrorMessage"></param>
    /// <returns></returns>
    private Boolean CheckCardErrorMessage(String ErrorMessage)
    {
      // Check error message
      if (String.IsNullOrEmpty(ErrorMessage))
      {
        return false;
      }

      this.CardErrorMessage = ErrorMessage;

      return true;
    } // CheckErrorMessage

    /// <summary>
    /// Hide cursor.
    /// </summary>
    private void HideCursor()
    {
      if (!this.Config.MouseVisibility && !Debugger.IsAttached)
      {
        Cursor.Hide();
      }
    } // HideCursor

    /// <summary>
    /// Check requested card status
    /// </summary>
    /// <param name="Msg"></param>
    /// <returns></returns>
    private void CheckRequestCardStatus(String Msg)
    {
      try
      {
        if (this.CheckAskCardStatus() && this.CheckContains(Msg))
        {
          this.InitializeTimeToAskCardStatus();
        }
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // CheckRequestCardStatus

    /// <summary>
    /// Check response of mobile bank
    /// </summary>
    /// <param name="Message"></param>
    /// <param name="ShowInKeyboard">Returns if message must be showed in uc_keyboard.</param>
    /// <returns> </returns>
    private Boolean CheckResponseMobileBank(String Message, out Boolean ShowInKeyboard)
    {
      ShowInKeyboard = true;

      try
      {
        if (this.MainForm.PendingLogoutOperatorPanel)
        {
          // Next message its response.
          if (Message.Contains("Transf"))
          {
            this.MainForm.NextMessageIsMobileBankResponse = true;
          }

          if (!Message.Contains("Transf")
            && this.MainForm.NextMessageIsMobileBankResponse)
          {
            // If message contains Welcome, end response, and proced to logout.
            if (Message.Contains("Welcome") || Message.Contains("Bienvenido") || Message.Contains("Valida"))
            {
              ShowInKeyboard = false;
              this.MainForm.LogoutMobileBank();
            }
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }

      return false;
    } // CheckRresponseMobileBank

    ////------------------------------------------------------------------------------
    //// PURPOSE: Check if main form is null and not card insertde validated.
    //// 
    ////  PARAMS:
    ////      - INPUT:
    ////          · Nothing.
    ////
    ////      - OUTPUT:
    ////          · Nothing.
    ////
    //// RETURNS: Result of check. 
    ////
    //private Boolean CheckNotNullsCardStatus()
    //{
    //  if (this.MainForm != null && !this.MainForm.IsInsertedCardValidated)
    //  {
    //    return true;
    //  }

    //  return false;
    //} // CheckNotNullsCardStatus

    /// <summary>
    /// Check if must check card status.
    /// </summary>
    /// <returns></returns>
    private Boolean CheckAskCardStatus()
    {
      return ((this.TimeToAskCardStatus.AddMilliseconds(Constants.TIME_TO_ASK_CARD_STATUS) < DateTime.Now) && this.ConnectionWCP);
    } // CheckAskCardStatus

    /// <summary>
    /// Check if message contains some strings.
    /// </summary>
    /// <param name="Msg"></param>
    /// <returns></returns>
    private Boolean CheckContains(String Msg)
    {
      String _msg;

      if (String.IsNullOrEmpty(Msg))
      {
        return false;
      }
      _msg = Msg.ToLower();

      if (_msg.Contains("credit"))
      {
        return true;
      }
      if (_msg.Contains("transf"))
      {
        return true;
      }
      if (_msg.Contains("valid"))
      {
        return true;
      }
      if (_msg.Contains("pin:"))
      {
        if (this.MainForm != null && this.MainForm.IsTechMode)
        {
          this.MainForm.ShowHideKeyboard();
        }
      }

      return false;
    } // CheckContains

    /// <summary>
    /// Set card statusfrom card messages
    /// </summary>
    /// <param name="Message"></param>
    /// <returns></returns>
    private Boolean SetCardStatusFromCardMessages(String Message, out TYPE_CARD_STATUS TypeCardStatus)
    {
      this.PendingLogout = false;
      this.CardMessage = Message;
      TypeCardStatus = TYPE_CARD_STATUS.CARD_INFORMATION;

      if (Message.Contains("Valida"))
      {
        IsCardRemoved = false;
        TypeCardStatus = TYPE_CARD_STATUS.CARD_INSERTED;

        return (!this.IsGameInProgress && !this.InSession); // Only when not card inserted validated
      }

      if (this.IsTransferring(Message))
      {
        this.PendingLogout = this.IsCardRemoved;
        TypeCardStatus = TYPE_CARD_STATUS.TRANSFER_CREDIT;

        return (!this.IsCardRemovedInProgress && !this.IsGameInProgress); // Only when not card inserted validated
      }

      return false;

    }  // SetCardStatusFromCardMessages

    /// <summary>
    /// Returns if SASHost is transferring credit to account
    /// </summary>
    /// <param name="Message"></param>
    /// <returns></returns>
    private bool IsTransferring(String Message)
    {
      // TODO: Check with another way
      return (Message.Contains("Transf")
            || Message == "La sesión terminará\nal finalizar la\njugada"
            || Message == "Session will end\nafter current\nplay");
    } // IsTransferring

    /// <summary>
    /// Show TechMenu
    /// </summary>
    private void ShowTechMenu()
    {
      this.MainForm.InTouch.FTEnter_ProtocolResponse(false);
    } // ShowTechMenu    

    /// <summary>
    /// Set Language
    /// </summary>
    /// <param name="Language"></param>
    private void SetLanguage(String Language)
    {
      Int32 _language;

      // Get Language Id
      _language = this.LanguageStringToIdintifier(Language);

      this.SetLanguageInternal(_language, true);
    } // SetLanguage
    private void SetLanguage(Int32 Language)
    {
      this.SetLanguageInternal(Language, false);
    } // SetLanguage

    /// <summary>
    /// Set Language Internal
    /// </summary>
    /// <param name="Language"></param>
    /// <param name="InTouchOnly"></param>
    private void SetLanguageInternal(Int32 Language, Boolean InTouchOnly)
    {
      // Check incomming language
      if (this.LanguageIdentifier == (LanguageIdentifier)Language)
      {
        this.MainForm.Browser.ForceNavigation();
      }

      // Set Language in InTouch class
      this.LanguageIdentifier = (LanguageIdentifier)Language;

      // Set Language in LCDInTouchWeb
      this.MainForm.Browser.SendProtocolResponse(TYPE_REQUEST.PLAYER_LANGUAGE_ID, Language);

      // Set Language in SASHost
      this.ProtocolHandler((int)TYPE_REQUEST.PLAYER_LANGUAGE_ID, Language.ToString());
    } // SetLanguageInternal

    /// <summary>
    /// Convert language string to language Id
    /// </summary>
    /// <param name="Language"></param>
    /// <returns></returns>
    private Int32 LanguageStringToIdintifier(String Language)
    {
      Int32 _language;

      // Check Language reuqest
      if (!Int32.TryParse(Language, out _language))
      {
        _language = (Int32)LanguageIdentifier.English;

        Log.Add(TYPE_MESSAGE.ERROR
                , String.Format("Error on set Language. Set {0} by de fault."
                , LanguageIdentifier.English));
      }

      return _language;
    } // LanguageStringToIdintifier

    /// <summary>
    /// Set the block reason message.
    /// </summary>
    /// <param name="BlockReasonMsg"></param>
    /// <param name="Msg"></param>
    /// <returns></returns>
    private String SetBlockReasonMsg(String BlockReasonMsg, String Msg)
    {
      if (BlockReasonMsg.Contains(Msg))
      {
        return BlockReasonMsg;
      }

      return (String.IsNullOrEmpty(BlockReasonMsg) ? Msg : BlockReasonMsg + " | " + Msg);
    } // SetBlockReasonMsg

    /// <summary>
    /// Set Welcome Message
    /// </summary>
    /// <param name="Data"></param>
    private void SetWelcomeMessage(IntPtr Data)
    {
      String[] _message_code;
      _message_code = this.ProtocolResponseDataToString(Data).Split('\n');

      WelcomeMessageLine1 = (_message_code.Length > 0) ? _message_code[0].Trim() : String.Empty;
      WelcomeMessageLine2 = (_message_code.Length > 1) ? _message_code[1].Trim() : String.Empty;

    } // SetWelcomeMessage

    #region " Message Interface Private Methods "

    /// <summary>
    /// Operator message in message interface management.
    /// </summary>
    /// <param name="Message"></param>  
    private void OperatorMessage_MsgInterface(String Message)
    {
      try
      {
        if (this.MainForm == null)
        {
          return;
        }

        if (this.MainForm.KeyboardEnabled)
        {
          this.OperatorMessageManagement_OnKeyboard(Message);
        }
        else
        {
          this.OperatorMessageManagement_OnMain(Message);
        }

      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // OperatorMessage_MsgInterface

    /// <summary>
    /// Operator message in message interface management when keyboard is showed.
    /// </summary>
    /// <param name="Message"></param> 
    internal void OperatorMessageManagement_OnKeyboard(String Message)
    {
      Boolean _show_in_keyboard;

      try
      {
        _show_in_keyboard = false;

        this.CheckResponseMobileBank(Message, out _show_in_keyboard);

        if (_show_in_keyboard)
        {
          this.MainForm.uc_keyboard.SetText(Message);
        }

        this.CheckRequestCardStatus(Message);
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // OperatorMessageManagement_OnKeyboard

    /// <summary>
    /// Operator message in message interface management when keyboard is not showed.
    /// </summary>
    /// <param name="Message"></param>   
    internal void OperatorMessageManagement_OnMain(String Message)
    {
      try
      {
        if (Message.Contains("PIN:"))
        {
          if (!this.MainForm.KeyboardEnabled
            && this.MainForm.ShowRequestPin == STATUS_REQUEST_PIN.REQUEST)
          {
            Log.Add(TYPE_MESSAGE.WARNING, "!!!REVISE: MUST BE SHOW PIN SCREEN¡¡¡");
          }
          else
          {

            this.MainForm.ShowHideKeyboard();

            this.MainForm.ShowRequestPin = STATUS_REQUEST_PIN.REQUEST;
            this.MainForm.uc_keyboard.SetText(Message);
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // OperatorMessageManagement_OnMain

    #endregion

    #endregion

  } // InTouch
}
