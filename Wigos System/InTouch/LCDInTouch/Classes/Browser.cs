﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//      MODULE NAME: Browser.cs
// 
//      DESCRIPTION: Class for browser on LCDInTouch
// 
//           AUTHOR: Javier Barea
// 
//    CREATION DATE: 30-JUN-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 30-JUN-2017 JBP    First release.
//------------------------------------------------------------------------------
using System;
using System.Diagnostics;
using System.Text;
using System.Threading;
using System.Web;
using System.Windows.Forms;

namespace LCDInTouch
{
  public class Browser
  {

    #region " Properties "

    public frm_base MainForm { get; set; }
    public frm_browser BrowserForm { get; set; }
    public String RequestedUrl { get; set; }
    internal STATUS_BROWSER BrowserStatus { get; set; }
    public Boolean IsHidden { get; set; }
    public Boolean NavigateInProgress { get; set; }
    public Boolean BrowserManagerInProcess { get; set; }
    public Boolean ConnectionWithServer { get; set; }
    public Int32 SleepTime { get; set; }

    #endregion

    #region " Public Methods "

    public Browser(frm_base FormBase)
    {
      this.MainForm = FormBase;
      this.IsHidden = true;
    }

    /// <summary>
    /// Force navigation
    /// </summary>
    public void ForceNavigation()
    {
      this.NavigateInProgress = false;
      this.RequestedUrl = String.Empty;
    } // ForceNavigation

    #endregion

    #region " Internal Methods "

    /// <summary>
    /// Check action in session
    /// </summary>
    /// <param name="Action"></param>
    /// <returns></returns>
    internal Boolean CheckActionInSession(TYPE_REQUEST Action)
    {
      // If action is TERMINAL_DRAW_GAME_RESULT, the PlayDraw execute in StartCardSession (Simulated when user is in session)
      return (this.MainForm.InTouch.InSession
     && !(this.MainForm.InTouch.TransferInProgress && this.IsTerminalDrawAction(Action)));
    } // CheckActionInSession

    /// <summary>
    /// return true if is terminal draw action
    /// </summary>
    /// <param name="Action"></param>
    /// <returns></returns>
    internal Boolean IsTerminalDrawAction(TYPE_REQUEST Action)
    {
      return ( Action == TYPE_REQUEST.TERMINAL_DRAW_ASK_FOR_PARTICIPATE
            || Action == TYPE_REQUEST.TERMINAL_DRAW_PARTICIPATE
            || Action == TYPE_REQUEST.TERMINAL_DRAW_GAME_RESULT);
    } // IsTerminalDraAction

    #endregion

    #region " Browser Management "

    /// <summary>
    /// Browser Status Manager
    /// </summary>
    internal void BrowserStatusManager()
    {
      try
      {
        if (this.BrowserManagerInProcess)
        {
          return;
        }

        this.BrowserManagerInProcess = true;

        // Check required invoke
        if ( this.BrowserForm != null
          && this.BrowserForm.InvokeRequired)
        {
          this.BrowserForm.BeginInvoke(new MethodInvoker(delegate
          {
            this.BrowserStatusManager();
          }));

          return;
        }

        // Start Browser manager...
        //  

        this.SleepTime = Constants.BROWSER_OK_SLEEP;

        // Browser Manager
        switch (this.BrowserStatus)
        {
          case STATUS_BROWSER.PRELOAD:
            this.PreloadBrowser();
            break;

          case STATUS_BROWSER.INIT:
            this.InitBrowser();
            break;

          case STATUS_BROWSER.LOADING:
            this.LoadingBrowser();
            break;

          case STATUS_BROWSER.HIDE:
            this.HideBrowser();
            break;

          case STATUS_BROWSER.SHOW:
            this.ShowBrowser();
            break;

          case STATUS_BROWSER.RUNNING:
            this.Running();
            break;

          case STATUS_BROWSER.CLOSING:
            this.ClosingBrowser();
            break;

          case STATUS_BROWSER.END:
            this.EndBrowser();
            break;

          default:
          case STATUS_BROWSER.IDLE:
            this.BrowserIDLE();
            break;
        }

        // Develpment log
        Log.Add(TYPE_MESSAGE.DEVELOPMENT_LVL1, String.Format("BrowserStatusManager : {0}", this.BrowserStatus));

        Application.DoEvents();
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }

      this.BrowserManagerInProcess = false;
    } // BrowserStatusManager

    #region " Browser Management "

    /// <summary>
    /// Preload browser status
    /// </summary>
    private void PreloadBrowser()
    {
      if(!this.MainForm.KeyboardEnabled)
      {
        this.MainForm.SetLoading(LOADING_MODE.BROWSER);
      }

      if (this.MainForm.IsLoading(LOADING_MODE.BROWSER))
      {
        this.BrowserStatus = STATUS_BROWSER.INIT;
      }
    } // PreloadBrowser

    /// <summary>
    /// Initialize browser status
    /// </summary>
    private void InitBrowser()
    {
      if (this.BrowserForm == null)
      {
        this.InstanceBrowser();
      }
      else if(!this.NavigateInProgress && this.NavigateMainBrowser())
      {
        this.BrowserStatus = STATUS_BROWSER.LOADING;
      }
      else if( !this.MainForm.KeyboardEnabled && !this.MainForm.IsVideoMode)
      {
        this.MainForm.BrowserBlockReasonsManager(INTOUCH_BLOCK_REASON.WEB_HOST_CONNECTION);
      }
    } // InitBrowser

    /// <summary>
    /// Loading browser status
    /// </summary>
    private void LoadingBrowser()
    {
      if (this.NavigateBrowser())
      {
        this.NavigateInProgress = false;
        this.BrowserStatus = STATUS_BROWSER.SHOW;
      }
    } // LoadingBrowser

    /// <summary>
    /// Hide browser status
    /// </summary>
    private void HideBrowser()
    {
      try
      {
        if (this.BrowserForm == null)
        {
          this.BrowserStatus = STATUS_BROWSER.PRELOAD;

          return;
        }

        // Hide browser
        if (!this.IsHidden)
        {
          this.IsHidden = true;
          this.BrowserForm.Opacity = Constants.OPACITY_HIDE;
        }

        // Check visibility mode
        if (!this.MainForm.KeyboardEnabled && !this.MainForm.IsVideoMode)
        {
          this.BrowserStatus = (this.ConnectionWithServer) ? STATUS_BROWSER.RUNNING : STATUS_BROWSER.LOADING;
        }
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // HideBrowser

    /// <summary>
    /// Show browser status
    /// </summary>
    private void ShowBrowser()
    {
      try
      {
        if (!this.ConnectionWithServer)
        {
          this.HideBrowser();

          return;
        }

        if (!this.BrowserForm.Ready)
        {
          this.SleepTime = Constants.BROWSER_WAIT_SLEEP;

          return;
        }

        if(this.IsHidden)
        {
          this.IsHidden = false;
          this.BrowserForm.Opacity = Constants.OPACITY_SHOW;
          this.BrowserForm.Location = this.MainForm.Location;
          this.BrowserForm.Show();
          this.BrowserForm.Activate();
          this.MainForm.ShowMode = SHOW_MODE.BROWSER;    
        }
                
        this.BrowserStatus = STATUS_BROWSER.RUNNING;
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // ShowBrowser

    /// <summary>
    /// Running browser status
    /// </summary>
    private void Running()
    {
      // Browser visibility
      if (!this.MainForm.IsBrowserMode)
      {
        this.BrowserStatus = STATUS_BROWSER.HIDE;

        return;
      }

      // Show browser if is hidden
      if(this.IsHidden)
      {
        this.BrowserStatus = STATUS_BROWSER.SHOW;
      }

      // Check if is time to forward block reasons to web
      if (!this.MainForm.Activity.IsExpired(TYPE_ACTIVITY.RefreshBrowserBlockReasons, Constants.TIME_TO_REFRESH_BLOCK_REASONS))
      {
        return;
      }

      // Forward block reasons
      if (this.MainForm.InTouch.BlockReasonId > 0)
      {
        this.MainForm.InTouch.ShowBlockReason(true);
      }
    } // Running

    /// <summary>
    /// Closing browser status
    /// </summary>
    private void ClosingBrowser()
    {
      try
      {
        this.CloseBrowser(false);

        this.CloseFormBrowser();
        this.UnInstanceBrowser();
        this.MainForm.TerminalDrawNavigationParams = null;

        if (this.BrowserForm == null)
        {
          this.BrowserStatus = STATUS_BROWSER.END;
        }
      }
      catch(Exception _ex)
      {
        Log.Add(_ex);
      }
    } // ClosingBrowser

    /// <summary>
    /// End browser status
    /// </summary>
    private void EndBrowser()
    {
      this.BrowserStatus = STATUS_BROWSER.IDLE;
    } // EndBrowser

    /// <summary>
    /// IDLE browser status
    /// </summary>
    private void BrowserIDLE()
    {
      if (this.BrowserForm != null)
      {
        this.StopBrowser();
      }

      // relaunch browser if is in Browser mode
      if (this.MainForm.IsBrowserMode)
      {
        this.BrowserStatus = STATUS_BROWSER.INIT;
      }
    } // BrowserIDLE

    /// <summary>
    /// Set browser status to Init
    /// </summary>
    public void StartBrowser()
    {
      this.NavigateInProgress = false;
      this.BrowserStatus = STATUS_BROWSER.INIT;
    } // StartBrowser

    /// <summary>
    /// Set browser status to Closing
    /// </summary>
    public void StopBrowser()
    {
      this.NavigateInProgress = false;
      this.BrowserStatus = STATUS_BROWSER.CLOSING;
    } // StopBrowser

    #endregion

    /// <summary>
    /// if all params are informed shows form browser
    /// </summary>
    /// <param name="URL"></param>
    /// <returns></returns>
    public Boolean NavigateBrowser()
    {
      return this.NavigateBrowser(this.MainForm.InTouch.Config.UrlWeb);
    } // NavigateBrowser
    public Boolean NavigateBrowser(String Url)
    {
      Boolean _rc;

      _rc = false;

      if (this.BrowserForm == null)      
      {
        return false;
      }
      
      // Check rekired invoke
      if (this.BrowserForm.InvokeRequired)
      {
        this.BrowserForm.BeginInvoke(new MethodInvoker(delegate
        {
          _rc = this.NavigateBrowser(Url);
        }));

        return _rc;
      }

      // Navigate
      if ( !this.MainForm.KeyboardEnabled
        && !this.MainForm.IsVideoMode
        && !this.NavigateInProgress
        && !this.NavigateBrowserInternal(Url) )
      {
        this.MainForm.BrowserBlockReasonsManager(INTOUCH_BLOCK_REASON.WEB_HOST_CONNECTION);

        return false;
      }

      this.MainForm.BlockReasonPanelManager();

      return true;
    } // NavigateBrowser

    /// <summary>
    /// Navigate Url and check URL is correct and can navigate.
    /// </summary>
    /// <returns></returns>
    public Boolean NavigateMainBrowser()
    {
      String _url_test;

      this.NavigateInProgress = true;

      try
      {
        _url_test = String.Empty;

        if (Debugger.IsAttached && !String.IsNullOrEmpty(this.MainForm.InTouch.Config.UrlWeb))
        {
          _url_test = this.MainForm.InTouch.Config.UrlWeb;
        }
        else if (this.MainForm.InTouch.Config != null)
        {
          this.MainForm.InTouch.Config.UrlWeb = this.MainForm.InTouch.Params.InTouchWebUrl;
        }

        if (Functions.CanNavigate(this.MainForm.InTouch.Config.UrlWeb))
        {
          this.ConnectionWithServer = true;

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
      finally
      {
        this.NavigateInProgress = false;
      }

      return false;
    } // NavigateMainBrowser

    /// <summary>
    /// Instance Browser for gamegateway
    /// </summary>
    public void InstanceBrowser()
    {
      try
      {
        if (this.BrowserForm == null || this.BrowserForm.IsDisposed)
        {
          this.BrowserForm = new frm_browser(this);
        }
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // InstanceBrowser

    /// <summary>
    /// UnInstance Browser
    /// </summary>
    public void UnInstanceBrowser()
    {
      try
      {
        if (this.BrowserForm != null)
        {
          if (!this.BrowserForm.IsDisposed)
          {
            this.BrowserForm.Dispose();
          }
          this.BrowserForm = null;
        }
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    }

    /// <summary>
    /// Instance Browser
    /// </summary> 
    public void CloseFormBrowser()
    {
      try
      {
        if (this.BrowserForm != null && !this.BrowserForm.IsDisposed)
        {
          this.BrowserForm.Close();
        }
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // CloseFrmBrowser

    /// <summary>
    /// Close browser manager
    /// </summary>
    /// <param name="RefreshBalance"></param>
    internal void CloseBrowser(Boolean RefreshBalance)
    {
      //Browser
      if (this.BrowserForm != null && !this.BrowserForm.IsDisposed)
      {
        this.BrowserForm.UnloadBrowser();
      }
    } //CloseBrowser

    #endregion

    #region " Browser Actions "

    /// <summary>
    /// Check request url
    /// </summary>
    /// <param name="RequestUrl"></param>
    internal Boolean CheckUrlRequest(String RequestUrl)
    {
      if (this.RequestedUrl != RequestUrl)
      {
        this.RequestedUrl = RequestUrl;

        return true;
      }

      return false;
    } // CheckUrlRequest

    /// <summary>
    /// Send card status to web
    /// </summary>
    /// <param name="Status">CARD STATUS</param>
    /// <param name="Data">AccountId</param>
    internal void SendCardStatus(TYPE_CARD_STATUS Status)
    {
      this.SendCardStatus(Status, String.Empty, false);
    } // SendCardStatus
    internal void SendCardStatus(TYPE_CARD_STATUS Status, String Data)
    {
      this.SendCardStatus(Status, Data, false);
    } // SendCardStatus
    internal void SendCardStatus(TYPE_CARD_STATUS Status, String Data, Boolean ForceRequest)
    {
      String _request_url;
      String _data;

      if (!this.ConnectionWithServer)
      {
        return;
      }

      // Prepare data to send in Url
      if (!this.PrepareProtocolData(Data, out _data))
      {
        Log.Add(TYPE_MESSAGE.ERROR, "Error on SendProtocolResponse(). Invalid Data.");

        return;
      }

      _request_url = String.Format(this.MainForm.InTouch.Config.UrlWeb + Constants.INTOUCH_PARAMS_ALL, TYPE_REQUEST.CARD_STATUS, (int)Status, _data);

      if (this.CheckUrlRequest(_request_url) || ForceRequest)
      {
        this.NavigateBrowser(RequestedUrl);
      }
    } // SendCardStatus

    /// <summary>
    /// Send protocol response to web
    /// </summary>
    /// <param name="FunctionCode"></param>
    /// <param name="Result"></param>
    /// <param name="Data"></param>
    internal void SendProtocolResponse(TYPE_REQUEST FunctionCode)
    {
      this.SendProtocolResponse(FunctionCode, 999, String.Empty, false);
    } // SendProtocolResponse
    internal void SendProtocolResponse(TYPE_REQUEST FunctionCode, String Data)
    {
      this.SendProtocolResponse(FunctionCode, 999, Data, false);
    } // SendProtocolResponse
    internal void SendProtocolResponse(TYPE_REQUEST FunctionCode, int Result)
    {
      this.SendProtocolResponse(FunctionCode, Result, String.Empty, false);
    } // SendProtocolResponse
    internal void SendProtocolResponse(TYPE_REQUEST FunctionCode, int Result, String Data)
    {
      this.SendProtocolResponse(FunctionCode, Result, Data, false);
    } // SendProtocolResponse
    internal void SendProtocolResponse(TYPE_REQUEST FunctionCode, int Result, String Data, Boolean ForceRequest)
    {
      String _request_url;
      String _data;

      if (!this.ConnectionWithServer)
      {
        return;
      }

      // Prepare data to send in Url
      if (!this.PrepareProtocolData(Data, out _data))
      {
        return;
      }

      _request_url = String.Format(this.MainForm.InTouch.Config.UrlWeb + Constants.INTOUCH_PARAMS_ALL, FunctionCode, Result, _data);

      if (this.CheckUrlRequest(_request_url) || ForceRequest)
      {
        this.NavigateBrowser(this.RequestedUrl);
      }
    } // SendProtocolResponse

    /// <summary>
    /// Interact when players is in session (By default, normal functionality)
    /// </summary>
    /// <param name="Action"></param>
    internal void InteractionInSession(TYPE_REQUEST Action, String Data)
    {
      switch (Action)
      {
        case TYPE_REQUEST.TERMINAL_DRAW_ASK_FOR_PARTICIPATE:
          this.InteractionInSession_DrawAskParticipate(Data);
          break;

        case TYPE_REQUEST.TERMINAL_DRAW_PARTICIPATE:
          this.InteractionInSession_DrawParticipate(Data);
          break;

        case TYPE_REQUEST.TERMINAL_DRAW_GAME_RESULT:
          this.InteractionInSession_DrawGameResult();
          break;

        case TYPE_REQUEST.PENDING_LOGOUT:
          this.MainForm.InTouch.PendingLogoutManager();
          break;

        default:
          // Default: Send to SASHost
          this.MainForm.InTouch.SendProtocolHandler(Action, Data);
          break;
      }
    } // InteractionInSession

    #endregion

    #region " Private Methods "

    #region " Interaction Methods "

    /// <summary>
    /// SendProtocolHandler browser interaction
    /// </summary>
    /// <param name="Action"></param>
    internal void SendProtocolHandlerInteraction(String Params)
    {
      TYPE_REQUEST _request;
      String _data;
      String _extra_data;

      try
      {
        // Check Action
        if (!this.CheckInteractionParams(Params, out _request, out _data, out _extra_data))
        {
          Log.Add(TYPE_MESSAGE.ERROR, "SendProtocolHandlerInteraction(). Invalid Params.");

          return;
        }

        // Manage ExtraData
        this.ManageExtraData(_request, _extra_data);

        // Browser interact in session
        if (this.CheckActionInSession(_request))
        {
          this.InteractionInSession(_request, _data);

          return;
        }

        // Default functionality
        this.MainForm.InTouch.SendProtocolHandler(_request, _data);
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // SendProtocolHandlerInteraction

    /// <summary>
    /// Check interaction params
    /// </summary>
    /// <param name="Params"></param>
    /// <param name="Request"></param>
    /// <param name="Data"></param>
    /// <param name="ExtraData"></param>
    /// <returns></returns>
    private Boolean CheckInteractionParams(String Params, out TYPE_REQUEST Request, out String Data, out String ExtraData)
    {
      Int32 _request;
      String[] _action;

      Request = TYPE_REQUEST.UNKNOWN;
      Data = String.Empty;
      ExtraData = String.Empty;

      // Split protocol
      _action = Params.Split('|');

      // Check Params split
      if (_action.Length < Constants.MIN_INTERACTION_PARAMS)
      {
        return false;
      }

      // Check action
      if (!Int32.TryParse(_action[Constants.INTERACTION_PARAMS_ACTION], out _request))
      {
        return false;
      }

      Request = (TYPE_REQUEST)_request;

      // Set interaction data
      if (_action.Length > Constants.INTERACTION_PARAMS_DATA
  && !String.IsNullOrEmpty(_action[Constants.INTERACTION_PARAMS_DATA]))
      {
        Data = _action[Constants.INTERACTION_PARAMS_DATA];
      }

      // Set interaction extra data
      if (_action.Length > Constants.INTERACTION_PARAMS_EXTRA_DATA
  && !String.IsNullOrEmpty(_action[Constants.INTERACTION_PARAMS_EXTRA_DATA]))
      {
        ExtraData = _action[Constants.INTERACTION_PARAMS_EXTRA_DATA];
      }

      return true;

    } // CheckInteractionParams

    /// <summary>
    /// ExtraData Manager
    /// </summary>
    /// <param name="Request"></param>
    /// <param name="ExtraData"></param>
    private void ManageExtraData(TYPE_REQUEST Request, String ExtraData)
    {
      try
      {
        switch (Request)
        {
          case TYPE_REQUEST.TERMINAL_DRAW_PARTICIPATE:
            this.MainForm.DrawItemSelected = ExtraData;
            break;

          default:
            // NOTHING TODO
            break;
        }
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // ManageExtraData

    #endregion

    #region " InteractionInSession Methods "

    /// <summary>
    /// InteractWithHost DrawParticipate action
    /// </summary>
    /// <param name="Data"></param>
    private void InteractionInSession_DrawAskParticipate(String Data)
    {
      this.MainForm.Browser.SendCardStatus(TYPE_CARD_STATUS.PENDING_DRAWS, Data);
    } // InteractionInSession_DrawAskParticipate

    /// <summary>
    /// InteractWithHost DrawParticipate action
    /// </summary>
    /// <param name="Data"></param>
    private void InteractionInSession_DrawParticipate(String Data)
    {
      // Only for item selection in termimnal draw
      this.MainForm.InTouch.IsGameInProgress = true;
      this.SendProtocolResponse(TYPE_REQUEST.TERMINAL_DRAW_GAME_RESULT, Data);
    } // InteractionInSession_DrawParticipate

    /// <summary>
    /// InteractWithHost DrawGameResult action
    /// </summary>
    private void InteractionInSession_DrawGameResult()
    {
      this.SendProtocolResponse(TYPE_REQUEST.TERMINAL_DRAW_GAME_END);
    } // InteractionInSession_DrawGameResult

    #endregion

    /// <summary>
    /// Navigate browser internal
    /// </summary>
    /// <param name="URL"></param>
    /// <returns></returns>
    private Boolean NavigateBrowserInternal(String URL)
    {
      this.NavigateInProgress = true;

      try
      {
        if (this.BrowserForm == null)
        {
          return false;
        }

        if (String.IsNullOrEmpty(URL))
        {
          return false;
        }

        // Check navigation
        if (!Functions.CanNavigate(this.MainForm.InTouch.Config.UrlWeb))
        {
          return false;
        }

        this.BrowserForm.Location = this.MainForm.Location;
        this.BrowserForm.WindowState = FormWindowState.Normal;

        if (this.BrowserForm.NavigateUrl(URL))
        {
          this.ConnectionWithServer = true;

          return true;
        }
      } // end try
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
      finally
      {
        this.NavigateInProgress = false;
      }

      return false;
    } // NavigateBrowserInternal

    /// <summary>
    /// Prepare protocol data to send in URL Request
    /// </summary>
    /// <param name="Data"></param>
    /// <param name="ReturnData"></param>
    /// <returns></returns>
    private Boolean PrepareProtocolData(String Data, out String ReturnData)
    {
      ReturnData = String.Empty;

      try
      {
        this.FormatData(Data, out ReturnData);

        // Format data for Url
        ReturnData = HttpUtility.UrlPathEncode(ReturnData);

        return true;
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }

      return false;
    } // PrepareProtocolData

    /// <summary>
    /// Format SASHost Data
    /// </summary>
    /// <param name="Data"></param>
    /// <param name="ReturnData"></param>
    /// <returns></returns>
    private void FormatData(String Data, out String ReturnData)
    {
      // Mark "Intro" in SASHost messages to format in InTouchWeb
      ReturnData = Data.Replace(".\n", "|");

      // Set espace in SASHost message
      ReturnData = ReturnData.Replace("\n", " ");
    } // FormatData

    #endregion

  } // Browser
}
