//------------------------------------------------------------------------------
// Copyright � 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: SASParams.cs
// 
//   DESCRIPTION: Manage properties from SAS HOST to LCD. 
// 
//        AUTHOR: Javier Barea
// 
// CREATION DATE: 21-JUN-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 21-JUN-2017 JBP    First release.
//------------------------------------------------------------------------------
using System;

namespace LCDInTouch
{
  public class SASParams
  {

    #region Structs 

    public struct FBSASParams
    {
      public UInt32 Enabled;
      public UInt32 PinRequest;
      public String HostAddress;
      public String Url;
      public String Login;
      public String Password;
    }

    #endregion 

    #region " Properties "

    public UInt64 PlaySessionID { get; set; }
    public UInt64 AccountID { get; set; }
    public UInt32 TerminalId { get; set; }
    public String TerminalName { get; set; }
    public String ProviderName { get; set; }
    public String PartnerId { get; set; }
    public String Url { get; set; }
    public String UrlTest { get; set; }
    public UInt16 Language { get; set; }
    public String PopUpMessageText { get; set; }
    public String Logo { get; set; }
    public ulong Jackpot { get; set; }
    public ulong Prize { get; set; }
    public String JackPotText { get; set; }
    public String PrizeText { get; set; }
    public UInt32 Type { get; set; }
    public UInt16 ReservedMode { get; set; }
    public UInt64 CmdId { get; set; }
    public FBSASParams FBParams { get; set; }

    #endregion

    #region " Public Methods "

    public SASParams()
    {
      PlaySessionID = 0;
      AccountID = 0;
      TerminalId = 0;
      TerminalName = String.Empty;
      PartnerId = String.Empty;
      ProviderName = String.Empty;
      Url = String.Empty;
      Language = 0;
      PopUpMessageText = String.Empty;
      Logo = String.Empty;
      Jackpot = 0;
      Prize = 0;
      JackPotText = String.Empty;
      PrizeText = String.Empty;
      Type = 0;
      ReservedMode = 0;
      CmdId = 0;

      FBParams = new FBSASParams()
      {
        Enabled = 0,
        PinRequest = 0,
        HostAddress = String.Empty,
        Url = String.Empty,
        Login = String.Empty,
        Password = String.Empty
      };
    }

    #endregion

  }
}
