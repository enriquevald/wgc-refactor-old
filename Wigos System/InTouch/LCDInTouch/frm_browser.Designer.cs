using LCDInTouch.Controls;
using System.Windows.Forms;
namespace LCDInTouch
{
  partial class frm_browser
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.pnl_top = new System.Windows.Forms.Panel();
      this.pnl_bot = new System.Windows.Forms.Panel();
      this.link_close_browser = new System.Windows.Forms.LinkLabel();
      this.web_browser = new LCDInTouch.Controls.uc_web_browser();
      this.pnl_bot.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_top
      // 
      this.pnl_top.BackColor = System.Drawing.Color.Gray;
      this.pnl_top.Dock = System.Windows.Forms.DockStyle.Top;
      this.pnl_top.Location = new System.Drawing.Point(0, 0);
      this.pnl_top.Name = "pnl_top";
      this.pnl_top.Size = new System.Drawing.Size(800, 20);
      this.pnl_top.TabIndex = 0;
      // 
      // pnl_bot
      // 
      this.pnl_bot.BackColor = System.Drawing.Color.Gray;
      this.pnl_bot.Controls.Add(this.link_close_browser);
      this.pnl_bot.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.pnl_bot.Location = new System.Drawing.Point(0, 276);
      this.pnl_bot.Name = "pnl_bot";
      this.pnl_bot.Size = new System.Drawing.Size(800, 20);
      this.pnl_bot.TabIndex = 1;
      // 
      // link_close_browser
      // 
      this.link_close_browser.BackColor = System.Drawing.Color.Gray;
      this.link_close_browser.Font = new System.Drawing.Font("Candara", 13F, System.Drawing.FontStyle.Bold);
      this.link_close_browser.LinkColor = System.Drawing.Color.DarkSlateGray;
      this.link_close_browser.Location = new System.Drawing.Point(332, -1);
      this.link_close_browser.Name = "link_close_browser";
      this.link_close_browser.Size = new System.Drawing.Size(138, 20);
      this.link_close_browser.TabIndex = 8;
      this.link_close_browser.TabStop = true;
      this.link_close_browser.Text = "Close Browser";
      this.link_close_browser.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      this.link_close_browser.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.link_close_browser_LinkClicked);
      // 
      // web_browser
      // 
      this.web_browser.Dock = System.Windows.Forms.DockStyle.Fill;
      this.web_browser.Location = new System.Drawing.Point(0, 20);
      this.web_browser.Margin = new System.Windows.Forms.Padding(10);
      this.web_browser.MinimumSize = new System.Drawing.Size(20, 20);
      this.web_browser.Name = "web_browser";
      this.web_browser.Size = new System.Drawing.Size(800, 256);
      this.web_browser.TabIndex = 6;
      this.web_browser.Url = new System.Uri("", System.UriKind.Relative);
      // 
      // frm_browser
      // 
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
      this.BackColor = System.Drawing.Color.Black;
      this.ClientSize = new System.Drawing.Size(800, 296);
      this.Controls.Add(this.web_browser);
      this.Controls.Add(this.pnl_bot);
      this.Controls.Add(this.pnl_top);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
      this.Name = "frm_browser";
      this.Text = "InTouchLCD";
      this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_browser_FormClosing);
      this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_browser_KeyPress);
      this.pnl_bot.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private Panel pnl_top;
    private Panel pnl_bot;
    private uc_web_browser web_browser;
    private LinkLabel link_close_browser;

  }
}