using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace LCDInTouch.Controls
{
  public partial class uc_button_label : Label
  {

    #region Members
    
    private Int32 m_border_width;
    private Color m_border_color;
    private String m_tag;

    #endregion 

    #region Properties

    [Browsable(false)]
    public Color BorderColor
    {
      get { return this.m_border_color; }
      set
      {
        this.m_border_color = value;
        this.Invalidate();
      }
    }
    
    [Browsable(false)]
    public Int32 BorderWidth
    {
      get
      {
        if (this.m_border_width < 0)
        {
          this.m_border_width = 0;
        }

        if (this.m_border_width > 5)
        {
          this.m_border_width = 5;
        }

        return this.m_border_width;
      }
      set
      {
        this.m_border_width = (value > 5) ? 5 : value;
        this.Invalidate();
      }
    }

    [Browsable(false)]
    public new String Tag
    {
      get { return this.m_tag; }
      set
      {
        this.m_tag = value;
        this.Invalidate();
      }
    }
    #endregion 

    public uc_button_label()
    {
      InitializeComponent();
    }

    #region Events

    protected override void OnPaint(PaintEventArgs e)
    {
      Pen _pen;
      Int32 _xy;
      Int32 _width;
      Int32 _height;
      Int32 _idx;

      try
      {
        base.OnPaint(e);

        _xy = 0;
        _width = this.ClientSize.Width;
        _height = this.ClientSize.Height;

        _pen = new Pen(this.BorderColor);

        for (_idx = 0; _idx < this.BorderWidth; _idx++)
        {
          e.Graphics.DrawRectangle(_pen, _xy + _idx, _xy + _idx, _width - (_idx << 1) - 1, _height - (_idx << 1) - 1);
        }
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    }

    #endregion 

  }
}
