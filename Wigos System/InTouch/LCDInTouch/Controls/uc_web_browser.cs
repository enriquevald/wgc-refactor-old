//------------------------------------------------------------------------------
// Copyright � 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//      MODULE NAME: Uc_Web_Browser.cs
// 
//      DESCRIPTION: Uc Web Browser
// 
//           AUTHOR: Javier Barea
// 
//    CREATION DATE: 26-JUN-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 26-JUN-2017 JBP    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace LCDInTouch.Controls
{
  class uc_web_browser : WebBrowser
  {
    private IWebBrowser2 m_ax_iwebbrowser2;

    #region Constants
    private const int FEATURE_DISABLE_NAVIGATION_SOUNDS = 21;
    private const int SET_FEATURE_ON_THREAD = 0x00000001;
    private const int SET_FEATURE_ON_PROCESS = 0x00000002;
    private const int SET_FEATURE_IN_REGISTRY = 0x00000004;
    private const int SET_FEATURE_ON_THREAD_LOCALMACHINE = 0x00000008;
    private const int SET_FEATURE_ON_THREAD_INTRANET = 0x00000010;
    private const int SET_FEATURE_ON_THREAD_TRUSTED = 0x00000020;
    private const int SET_FEATURE_ON_THREAD_INTERNET = 0x00000040;
    private const int SET_FEATURE_ON_THREAD_RESTRICTED = 0x00000080;
    #endregion

    private IWebBrowser2 axIWebBrowser2
    {
      get { return this.m_ax_iwebbrowser2; }
      set { this.m_ax_iwebbrowser2 = value; }
    }

    protected override void AttachInterfaces(
        object nativeActiveXObject)
    {
      base.AttachInterfaces(nativeActiveXObject);
      this.axIWebBrowser2 = (IWebBrowser2)nativeActiveXObject;
    }

    public uc_web_browser()
    {
      CoInternetSetFeatureEnabled(FEATURE_DISABLE_NAVIGATION_SOUNDS, SET_FEATURE_ON_PROCESS, true);
    }

    public void Close()
    {
      this.axIWebBrowser2.Stop();
      this.axIWebBrowser2.Quit();
    }

    protected override void DetachInterfaces()
    {
      base.DetachInterfaces();
      this.axIWebBrowser2 = null;
    }

    public void Zoom(int factor)
    {
      object pvaIn = factor;
      object _zero = IntPtr.Zero;

      try
      {
        this.axIWebBrowser2.ExecWB(OLECMDID.OLECMDID_OPTICAL_ZOOM,
           OLECMDEXECOPT.OLECMDEXECOPT_DONTPROMPTUSER,
           ref pvaIn, ref _zero);
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    }
    
    [DllImport("urlmon.dll")]
    [PreserveSig]
    [return: MarshalAs(UnmanagedType.Error)]
    static extern int CoInternetSetFeatureEnabled(int FeatureEntry, [MarshalAs(UnmanagedType.U4)] int dwFlags, bool fEnable);
  }
}
