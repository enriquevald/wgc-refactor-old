//------------------------------------------------------------------------------
// Copyright � 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_check.cs
// 
//   DESCRIPTION: Check Control. 
// 
//        AUTHOR: Javi Barea
// 
// CREATION DATE: 30-JUN-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 30-JUN-2016 JBP    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

using LCDInTouch.Properties;

namespace LCDInTouch.Controls
{
  public partial class uc_check : UserControl
  {

    #region Members

    private Boolean m_value;
    
    #endregion

    #region Properties
    
    #region Style Properties

    public frm_base MainForm { get { return ((frm_base)this.ParentForm); } }

    [Description("Value"), Category(".Style Properties")]
    public Boolean Value
    {
      get { return this.m_value; }
      set 
      {
        this.SetValue(value);
        this.m_value = value; 
      }
    }
    
    [Description("Total height button"), Category(".Style Properties")]
    public Int32 Total_Height
    {
      get { return this.Height; }
      set { this.Height = value; }
    }
    
    [Description("Total width button"), Category(".Style Properties")]
    public Int32 Total_Width
    {
      get { return this.Width; }
      set
      {
        this.Width = value;
        this.lbl_description.Width = (this.Width - this.pnl_color.Width);
      }
    }

    [Description("Text of button"), Category(".Style Properties")]
    public String Text_Literal
    {
      get { return this.lbl_description.Text; }
      set { this.lbl_description.Text = value; }
    }

    [Description("Total image width button"), Category(".Style Properties")]
    public Int32 Text_Width
    {
      get { return this.lbl_description.Width; }
      set { this.Total_Width += (value - this.lbl_description.Width); }
    }

    [Description("Font button"), Category(".Style Properties")]
    public Font Text_Font
    {
      get { return this.lbl_description.Font; }
      set { this.lbl_description.Font = value; }
    }

    [Description("Text align inside label"), Category(".Style Properties")]
    public ContentAlignment Text_Align
    {
      get { return this.lbl_description.TextAlign; }
      set { this.lbl_description.TextAlign = value; }
    }

    [Description("Label Text padding"), Category(".Style Properties")]
    public Padding Text_Padding
    {
      get { return this.lbl_description.Padding; }
      set { this.lbl_description.Padding = value; }
    }

    [Description("Text color"), Category(".Style Properties")]
    public Color Text_Color
    {
      get { return this.lbl_description.ForeColor; }
      set { this.lbl_description.ForeColor = value; }
    }

    
    #endregion

    #endregion

    #region Public Functions

    public uc_check()
    {
      this.InitializeComponent();

    }

    #endregion

    #region Private

    private void UpdateLastActivity()
    {
      try
      {
        this.MainForm.ResetActivities();        
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    }

    private void SetValue(Boolean Value)
    {
      this.pnl_color.BackColor = (Value) ? Color.LawnGreen : Color.Red;
      this.pnl_switch.Dock = (Value) ? DockStyle.Left : DockStyle.Right;
    }

    #endregion

    #region Events

    #region Public Events

    public void Click_UcButtonEvent(object sender, EventArgs e)
    {
    }

    public void MouseUp_UcButtonEvent(object sender, EventArgs e)
    {
    }

    public void MouseDown_UcButtonEvent(object sender, EventArgs e)
    {
    }

    public void Click_Button_Base(object sender, EventArgs e)
    {
      this.UpdateLastActivity();
    }

    #endregion

    #region Internal Events

    internal void uc_button_Click(object sender, EventArgs e)
    {
        
    }

    protected override void OnPaint(PaintEventArgs e)
    {
      Pen _pen;
      Int32 _x;
      Int32 _y;
      Int32 _width;
      Int32 _height;
      Int32 _idx;

      try
      {
        base.OnPaint(e);

        _x = pnl_color.Location.X;
        _y = pnl_color.Location.Y;
        _width = this.pnl_color.Width;
        _height = this.pnl_color.Height;

        _pen = new Pen(Color.DarkGray);

        for (_idx = 0; _idx < 2; _idx++)
        {
          e.Graphics.DrawRectangle(_pen, _x + _idx, _y + _idx, _width - (_idx << 1) - 1, _height - (_idx << 1) - 1);
        }
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    }
    
    private void pnl_color_Click(object sender, EventArgs e)
    {
      this.Value = !this.Value;
    }

    #endregion

    #endregion

  }
}
