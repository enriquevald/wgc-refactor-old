using System.Windows.Forms;
namespace LCDInTouch.Controls
{
  partial class uc_keyboard
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.pnl_buttons = new System.Windows.Forms.Panel();
      this.btn_custom_4 = new System.Windows.Forms.Button();
      this.btn_custom_3 = new System.Windows.Forms.Button();
      this.btn_custom_2 = new System.Windows.Forms.Button();
      this.btn_custom_1 = new System.Windows.Forms.Button();
      this.btn_enter_config = new System.Windows.Forms.Button();
      this.pnl_left = new System.Windows.Forms.Panel();
      this.lbl_line_1 = new System.Windows.Forms.Label();
      this.pnl_mb_buttons = new System.Windows.Forms.Panel();
      this.btn_f2 = new System.Windows.Forms.Button();
      this.btn_f1 = new System.Windows.Forms.Button();
      this.btn_0 = new System.Windows.Forms.Button();
      this.btn_esc = new System.Windows.Forms.Button();
      this.btn_ok = new System.Windows.Forms.Button();
      this.btn_dot = new System.Windows.Forms.Button();
      this.btn_3 = new System.Windows.Forms.Button();
      this.btn_2 = new System.Windows.Forms.Button();
      this.btn_5 = new System.Windows.Forms.Button();
      this.btn_1 = new System.Windows.Forms.Button();
      this.btn_4 = new System.Windows.Forms.Button();
      this.btn_6 = new System.Windows.Forms.Button();
      this.btn_9 = new System.Windows.Forms.Button();
      this.btn_8 = new System.Windows.Forms.Button();
      this.btn_7 = new System.Windows.Forms.Button();
      this.pnl_container.SuspendLayout();
      this.pnl_buttons.SuspendLayout();
      this.pnl_left.SuspendLayout();
      this.pnl_mb_buttons.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_container
      // 
      this.pnl_container.Controls.Add(this.pnl_mb_buttons);
      this.pnl_container.Controls.Add(this.pnl_left);
      this.pnl_container.Size = new System.Drawing.Size(800, 256);
      // 
      // pnl_buttons
      // 
      this.pnl_buttons.Controls.Add(this.btn_custom_4);
      this.pnl_buttons.Controls.Add(this.btn_custom_3);
      this.pnl_buttons.Controls.Add(this.btn_custom_2);
      this.pnl_buttons.Controls.Add(this.btn_custom_1);
      this.pnl_buttons.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.pnl_buttons.Location = new System.Drawing.Point(0, 182);
      this.pnl_buttons.Name = "pnl_buttons";
      this.pnl_buttons.Size = new System.Drawing.Size(536, 74);
      this.pnl_buttons.TabIndex = 2;
      // 
      // btn_custom_4
      // 
      this.btn_custom_4.BackColor = System.Drawing.Color.LightGray;
      this.btn_custom_4.CausesValidation = false;
      this.btn_custom_4.FlatAppearance.BorderColor = System.Drawing.Color.Black;
      this.btn_custom_4.FlatAppearance.BorderSize = 0;
      this.btn_custom_4.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
      this.btn_custom_4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
      this.btn_custom_4.FlatStyle = System.Windows.Forms.FlatStyle.System;
      this.btn_custom_4.Font = new System.Drawing.Font("MyriadProRegular", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btn_custom_4.Location = new System.Drawing.Point(394, 7);
      this.btn_custom_4.Name = "btn_custom_4";
      this.btn_custom_4.Size = new System.Drawing.Size(126, 62);
      this.btn_custom_4.TabIndex = 52;
      this.btn_custom_4.TabStop = false;
      this.btn_custom_4.Text = "500";
      this.btn_custom_4.UseVisualStyleBackColor = false;
      this.btn_custom_4.Click += new System.EventHandler(this.KeyboardButtonClicked);
      // 
      // btn_custom_3
      // 
      this.btn_custom_3.BackColor = System.Drawing.Color.LightGray;
      this.btn_custom_3.CausesValidation = false;
      this.btn_custom_3.FlatAppearance.BorderColor = System.Drawing.Color.Black;
      this.btn_custom_3.FlatAppearance.BorderSize = 0;
      this.btn_custom_3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
      this.btn_custom_3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
      this.btn_custom_3.FlatStyle = System.Windows.Forms.FlatStyle.System;
      this.btn_custom_3.Font = new System.Drawing.Font("MyriadProRegular", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btn_custom_3.Location = new System.Drawing.Point(268, 7);
      this.btn_custom_3.Name = "btn_custom_3";
      this.btn_custom_3.Size = new System.Drawing.Size(126, 62);
      this.btn_custom_3.TabIndex = 51;
      this.btn_custom_3.TabStop = false;
      this.btn_custom_3.Text = "200";
      this.btn_custom_3.UseVisualStyleBackColor = false;
      this.btn_custom_3.Click += new System.EventHandler(this.KeyboardButtonClicked);
      // 
      // btn_custom_2
      // 
      this.btn_custom_2.BackColor = System.Drawing.Color.LightGray;
      this.btn_custom_2.CausesValidation = false;
      this.btn_custom_2.FlatAppearance.BorderColor = System.Drawing.Color.Black;
      this.btn_custom_2.FlatAppearance.BorderSize = 0;
      this.btn_custom_2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
      this.btn_custom_2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
      this.btn_custom_2.FlatStyle = System.Windows.Forms.FlatStyle.System;
      this.btn_custom_2.Font = new System.Drawing.Font("MyriadProRegular", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btn_custom_2.Location = new System.Drawing.Point(142, 7);
      this.btn_custom_2.Name = "btn_custom_2";
      this.btn_custom_2.Size = new System.Drawing.Size(126, 62);
      this.btn_custom_2.TabIndex = 50;
      this.btn_custom_2.TabStop = false;
      this.btn_custom_2.Text = "100";
      this.btn_custom_2.UseVisualStyleBackColor = false;
      this.btn_custom_2.Click += new System.EventHandler(this.KeyboardButtonClicked);
      // 
      // btn_custom_1
      // 
      this.btn_custom_1.BackColor = System.Drawing.Color.LightGray;
      this.btn_custom_1.CausesValidation = false;
      this.btn_custom_1.FlatAppearance.BorderColor = System.Drawing.Color.Black;
      this.btn_custom_1.FlatAppearance.BorderSize = 0;
      this.btn_custom_1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
      this.btn_custom_1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
      this.btn_custom_1.FlatStyle = System.Windows.Forms.FlatStyle.System;
      this.btn_custom_1.Font = new System.Drawing.Font("MyriadProRegular", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btn_custom_1.Location = new System.Drawing.Point(16, 7);
      this.btn_custom_1.Name = "btn_custom_1";
      this.btn_custom_1.Size = new System.Drawing.Size(126, 62);
      this.btn_custom_1.TabIndex = 49;
      this.btn_custom_1.TabStop = false;
      this.btn_custom_1.Text = "50";
      this.btn_custom_1.UseVisualStyleBackColor = false;
      this.btn_custom_1.Click += new System.EventHandler(this.KeyboardButtonClicked);
      // 
      // btn_enter_config
      // 
      this.btn_enter_config.BackColor = System.Drawing.Color.Gray;
      this.btn_enter_config.Font = new System.Drawing.Font("MyriadProRegular", 20F);
      this.btn_enter_config.ForeColor = System.Drawing.Color.White;
      this.btn_enter_config.Location = new System.Drawing.Point(114, 188);
      this.btn_enter_config.Margin = new System.Windows.Forms.Padding(8);
      this.btn_enter_config.Name = "btn_enter_config";
      this.btn_enter_config.Padding = new System.Windows.Forms.Padding(5, 0, 5, 0);
      this.btn_enter_config.Size = new System.Drawing.Size(280, 60);
      this.btn_enter_config.TabIndex = 53;
      this.btn_enter_config.Text = "xConfiguration";
      this.btn_enter_config.UseVisualStyleBackColor = false;
      this.btn_enter_config.Click += new System.EventHandler(this.ConfigurationButtonClicked);
      // 
      // pnl_left
      // 
      this.pnl_left.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
      this.pnl_left.Controls.Add(this.btn_enter_config);
      this.pnl_left.Controls.Add(this.pnl_buttons);
      this.pnl_left.Controls.Add(this.lbl_line_1);
      this.pnl_left.Location = new System.Drawing.Point(0, 0);
      this.pnl_left.Name = "pnl_left";
      this.pnl_left.Size = new System.Drawing.Size(536, 256);
      this.pnl_left.TabIndex = 3;
      // 
      // lbl_line_1
      // 
      this.lbl_line_1.BackColor = System.Drawing.Color.Transparent;
      this.lbl_line_1.Font = new System.Drawing.Font("Lucida Console", 20F);
      this.lbl_line_1.ForeColor = System.Drawing.Color.White;
      this.lbl_line_1.Location = new System.Drawing.Point(16, 16);
      this.lbl_line_1.Name = "lbl_line_1";
      this.lbl_line_1.Size = new System.Drawing.Size(504, 163);
      this.lbl_line_1.TabIndex = 0;
      this.lbl_line_1.Text = "01234567890123456789";
      // 
      // pnl_mb_buttons
      // 
      this.pnl_mb_buttons.Controls.Add(this.btn_f2);
      this.pnl_mb_buttons.Controls.Add(this.btn_f1);
      this.pnl_mb_buttons.Controls.Add(this.btn_0);
      this.pnl_mb_buttons.Controls.Add(this.btn_esc);
      this.pnl_mb_buttons.Controls.Add(this.btn_ok);
      this.pnl_mb_buttons.Controls.Add(this.btn_dot);
      this.pnl_mb_buttons.Controls.Add(this.btn_3);
      this.pnl_mb_buttons.Controls.Add(this.btn_2);
      this.pnl_mb_buttons.Controls.Add(this.btn_5);
      this.pnl_mb_buttons.Controls.Add(this.btn_1);
      this.pnl_mb_buttons.Controls.Add(this.btn_4);
      this.pnl_mb_buttons.Controls.Add(this.btn_6);
      this.pnl_mb_buttons.Controls.Add(this.btn_9);
      this.pnl_mb_buttons.Controls.Add(this.btn_8);
      this.pnl_mb_buttons.Controls.Add(this.btn_7);
      this.pnl_mb_buttons.Dock = System.Windows.Forms.DockStyle.Right;
      this.pnl_mb_buttons.Location = new System.Drawing.Point(537, 0);
      this.pnl_mb_buttons.Name = "pnl_mb_buttons";
      this.pnl_mb_buttons.Size = new System.Drawing.Size(263, 256);
      this.pnl_mb_buttons.TabIndex = 5;
      // 
      // btn_f2
      // 
      this.btn_f2.BackColor = System.Drawing.Color.LightGray;
      this.btn_f2.CausesValidation = false;
      this.btn_f2.FlatAppearance.BorderColor = System.Drawing.Color.Black;
      this.btn_f2.FlatAppearance.BorderSize = 0;
      this.btn_f2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
      this.btn_f2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
      this.btn_f2.FlatStyle = System.Windows.Forms.FlatStyle.System;
      this.btn_f2.Font = new System.Drawing.Font("MyriadProRegular", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btn_f2.Location = new System.Drawing.Point(194, 128);
      this.btn_f2.Name = "btn_f2";
      this.btn_f2.Size = new System.Drawing.Size(63, 62);
      this.btn_f2.TabIndex = 45;
      this.btn_f2.TabStop = false;
      this.btn_f2.Text = "F2";
      this.btn_f2.UseVisualStyleBackColor = false;
      this.btn_f2.Click += new System.EventHandler(this.KeyboardButtonClicked);
      // 
      // btn_f1
      // 
      this.btn_f1.BackColor = System.Drawing.Color.LightGray;
      this.btn_f1.CausesValidation = false;
      this.btn_f1.FlatAppearance.BorderColor = System.Drawing.Color.Black;
      this.btn_f1.FlatAppearance.BorderSize = 0;
      this.btn_f1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
      this.btn_f1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
      this.btn_f1.FlatStyle = System.Windows.Forms.FlatStyle.System;
      this.btn_f1.Font = new System.Drawing.Font("MyriadProRegular", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btn_f1.Location = new System.Drawing.Point(194, 66);
      this.btn_f1.Name = "btn_f1";
      this.btn_f1.Size = new System.Drawing.Size(63, 62);
      this.btn_f1.TabIndex = 45;
      this.btn_f1.TabStop = false;
      this.btn_f1.Text = "F1";
      this.btn_f1.UseVisualStyleBackColor = false;
      this.btn_f1.Click += new System.EventHandler(this.KeyboardButtonClicked);
      // 
      // btn_0
      // 
      this.btn_0.BackColor = System.Drawing.Color.LightGray;
      this.btn_0.CausesValidation = false;
      this.btn_0.FlatAppearance.BorderColor = System.Drawing.Color.Black;
      this.btn_0.FlatAppearance.BorderSize = 0;
      this.btn_0.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
      this.btn_0.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
      this.btn_0.FlatStyle = System.Windows.Forms.FlatStyle.System;
      this.btn_0.Font = new System.Drawing.Font("MyriadProRegular", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btn_0.Location = new System.Drawing.Point(5, 190);
      this.btn_0.Name = "btn_0";
      this.btn_0.Size = new System.Drawing.Size(63, 62);
      this.btn_0.TabIndex = 48;
      this.btn_0.TabStop = false;
      this.btn_0.Text = "0";
      this.btn_0.UseVisualStyleBackColor = false;
      this.btn_0.Click += new System.EventHandler(this.KeyboardButtonClicked);
      // 
      // btn_esc
      // 
      this.btn_esc.BackColor = System.Drawing.Color.LightGray;
      this.btn_esc.CausesValidation = false;
      this.btn_esc.FlatAppearance.BorderColor = System.Drawing.Color.Black;
      this.btn_esc.FlatAppearance.BorderSize = 0;
      this.btn_esc.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
      this.btn_esc.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
      this.btn_esc.FlatStyle = System.Windows.Forms.FlatStyle.System;
      this.btn_esc.Font = new System.Drawing.Font("MyriadProRegular", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btn_esc.ForeColor = System.Drawing.Color.Red;
      this.btn_esc.Location = new System.Drawing.Point(194, 4);
      this.btn_esc.Name = "btn_esc";
      this.btn_esc.Size = new System.Drawing.Size(63, 62);
      this.btn_esc.TabIndex = 45;
      this.btn_esc.TabStop = false;
      this.btn_esc.Text = "ESC";
      this.btn_esc.UseVisualStyleBackColor = false;
      this.btn_esc.Click += new System.EventHandler(this.KeyboardButtonClicked);
      // 
      // btn_ok
      // 
      this.btn_ok.BackColor = System.Drawing.Color.LightGray;
      this.btn_ok.CausesValidation = false;
      this.btn_ok.FlatAppearance.BorderColor = System.Drawing.Color.Black;
      this.btn_ok.FlatAppearance.BorderSize = 0;
      this.btn_ok.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
      this.btn_ok.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
      this.btn_ok.FlatStyle = System.Windows.Forms.FlatStyle.System;
      this.btn_ok.Font = new System.Drawing.Font("MyriadProRegular", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btn_ok.Location = new System.Drawing.Point(131, 190);
      this.btn_ok.Name = "btn_ok";
      this.btn_ok.Size = new System.Drawing.Size(126, 62);
      this.btn_ok.TabIndex = 45;
      this.btn_ok.TabStop = false;
      this.btn_ok.Text = "OK";
      this.btn_ok.UseVisualStyleBackColor = false;
      this.btn_ok.Click += new System.EventHandler(this.KeyboardButtonClicked);
      // 
      // btn_dot
      // 
      this.btn_dot.BackColor = System.Drawing.Color.LightGray;
      this.btn_dot.CausesValidation = false;
      this.btn_dot.FlatAppearance.BorderColor = System.Drawing.Color.Black;
      this.btn_dot.FlatAppearance.BorderSize = 0;
      this.btn_dot.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
      this.btn_dot.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
      this.btn_dot.FlatStyle = System.Windows.Forms.FlatStyle.System;
      this.btn_dot.Font = new System.Drawing.Font("MyriadProRegular", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btn_dot.Location = new System.Drawing.Point(68, 190);
      this.btn_dot.Name = "btn_dot";
      this.btn_dot.Size = new System.Drawing.Size(63, 62);
      this.btn_dot.TabIndex = 45;
      this.btn_dot.TabStop = false;
      this.btn_dot.Text = ".";
      this.btn_dot.UseVisualStyleBackColor = false;
      this.btn_dot.Click += new System.EventHandler(this.KeyboardButtonClicked);
      // 
      // btn_3
      // 
      this.btn_3.BackColor = System.Drawing.Color.LightGray;
      this.btn_3.CausesValidation = false;
      this.btn_3.FlatAppearance.BorderColor = System.Drawing.Color.Black;
      this.btn_3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
      this.btn_3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
      this.btn_3.FlatStyle = System.Windows.Forms.FlatStyle.System;
      this.btn_3.Font = new System.Drawing.Font("MyriadProRegular", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btn_3.Location = new System.Drawing.Point(131, 128);
      this.btn_3.Name = "btn_3";
      this.btn_3.Size = new System.Drawing.Size(63, 62);
      this.btn_3.TabIndex = 47;
      this.btn_3.TabStop = false;
      this.btn_3.Text = "3";
      this.btn_3.UseVisualStyleBackColor = false;
      this.btn_3.Click += new System.EventHandler(this.KeyboardButtonClicked);
      // 
      // btn_2
      // 
      this.btn_2.BackColor = System.Drawing.Color.LightGray;
      this.btn_2.CausesValidation = false;
      this.btn_2.FlatAppearance.BorderColor = System.Drawing.Color.Black;
      this.btn_2.FlatAppearance.BorderSize = 0;
      this.btn_2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
      this.btn_2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
      this.btn_2.FlatStyle = System.Windows.Forms.FlatStyle.System;
      this.btn_2.Font = new System.Drawing.Font("MyriadProRegular", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btn_2.Location = new System.Drawing.Point(68, 128);
      this.btn_2.Name = "btn_2";
      this.btn_2.Size = new System.Drawing.Size(63, 62);
      this.btn_2.TabIndex = 46;
      this.btn_2.TabStop = false;
      this.btn_2.Text = "2";
      this.btn_2.UseVisualStyleBackColor = false;
      this.btn_2.Click += new System.EventHandler(this.KeyboardButtonClicked);
      // 
      // btn_5
      // 
      this.btn_5.BackColor = System.Drawing.Color.LightGray;
      this.btn_5.CausesValidation = false;
      this.btn_5.FlatAppearance.BorderColor = System.Drawing.Color.Black;
      this.btn_5.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
      this.btn_5.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
      this.btn_5.FlatStyle = System.Windows.Forms.FlatStyle.System;
      this.btn_5.Font = new System.Drawing.Font("MyriadProRegular", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btn_5.Location = new System.Drawing.Point(68, 66);
      this.btn_5.Name = "btn_5";
      this.btn_5.Size = new System.Drawing.Size(63, 62);
      this.btn_5.TabIndex = 46;
      this.btn_5.TabStop = false;
      this.btn_5.Text = "5";
      this.btn_5.UseVisualStyleBackColor = false;
      this.btn_5.Click += new System.EventHandler(this.KeyboardButtonClicked);
      // 
      // btn_1
      // 
      this.btn_1.BackColor = System.Drawing.Color.LightGray;
      this.btn_1.CausesValidation = false;
      this.btn_1.FlatAppearance.BorderColor = System.Drawing.Color.Black;
      this.btn_1.FlatAppearance.BorderSize = 0;
      this.btn_1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
      this.btn_1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
      this.btn_1.FlatStyle = System.Windows.Forms.FlatStyle.System;
      this.btn_1.Font = new System.Drawing.Font("MyriadProRegular", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btn_1.Location = new System.Drawing.Point(5, 128);
      this.btn_1.Name = "btn_1";
      this.btn_1.Size = new System.Drawing.Size(63, 62);
      this.btn_1.TabIndex = 45;
      this.btn_1.TabStop = false;
      this.btn_1.Text = "1";
      this.btn_1.UseVisualStyleBackColor = false;
      this.btn_1.Click += new System.EventHandler(this.KeyboardButtonClicked);
      // 
      // btn_4
      // 
      this.btn_4.BackColor = System.Drawing.Color.LightGray;
      this.btn_4.CausesValidation = false;
      this.btn_4.FlatAppearance.BorderColor = System.Drawing.Color.Black;
      this.btn_4.FlatAppearance.BorderSize = 0;
      this.btn_4.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
      this.btn_4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
      this.btn_4.FlatStyle = System.Windows.Forms.FlatStyle.System;
      this.btn_4.Font = new System.Drawing.Font("MyriadProRegular", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btn_4.Location = new System.Drawing.Point(5, 66);
      this.btn_4.Name = "btn_4";
      this.btn_4.Size = new System.Drawing.Size(63, 62);
      this.btn_4.TabIndex = 45;
      this.btn_4.TabStop = false;
      this.btn_4.Text = "4";
      this.btn_4.UseVisualStyleBackColor = false;
      this.btn_4.Click += new System.EventHandler(this.KeyboardButtonClicked);
      // 
      // btn_6
      // 
      this.btn_6.BackColor = System.Drawing.Color.LightGray;
      this.btn_6.CausesValidation = false;
      this.btn_6.FlatAppearance.BorderColor = System.Drawing.Color.Black;
      this.btn_6.FlatAppearance.BorderSize = 0;
      this.btn_6.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
      this.btn_6.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
      this.btn_6.FlatStyle = System.Windows.Forms.FlatStyle.System;
      this.btn_6.Font = new System.Drawing.Font("MyriadProRegular", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btn_6.Location = new System.Drawing.Point(131, 66);
      this.btn_6.Name = "btn_6";
      this.btn_6.Size = new System.Drawing.Size(63, 62);
      this.btn_6.TabIndex = 45;
      this.btn_6.TabStop = false;
      this.btn_6.Text = "6";
      this.btn_6.UseVisualStyleBackColor = false;
      this.btn_6.Click += new System.EventHandler(this.KeyboardButtonClicked);
      // 
      // btn_9
      // 
      this.btn_9.BackColor = System.Drawing.Color.LightGray;
      this.btn_9.CausesValidation = false;
      this.btn_9.FlatAppearance.BorderColor = System.Drawing.Color.Black;
      this.btn_9.FlatAppearance.BorderSize = 0;
      this.btn_9.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
      this.btn_9.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
      this.btn_9.FlatStyle = System.Windows.Forms.FlatStyle.System;
      this.btn_9.Font = new System.Drawing.Font("MyriadProRegular", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btn_9.Location = new System.Drawing.Point(131, 4);
      this.btn_9.Name = "btn_9";
      this.btn_9.Size = new System.Drawing.Size(63, 62);
      this.btn_9.TabIndex = 47;
      this.btn_9.TabStop = false;
      this.btn_9.Text = "9";
      this.btn_9.UseVisualStyleBackColor = false;
      this.btn_9.Click += new System.EventHandler(this.KeyboardButtonClicked);
      // 
      // btn_8
      // 
      this.btn_8.BackColor = System.Drawing.Color.LightGray;
      this.btn_8.CausesValidation = false;
      this.btn_8.FlatAppearance.BorderColor = System.Drawing.Color.Black;
      this.btn_8.FlatAppearance.BorderSize = 0;
      this.btn_8.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
      this.btn_8.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
      this.btn_8.FlatStyle = System.Windows.Forms.FlatStyle.System;
      this.btn_8.Font = new System.Drawing.Font("MyriadProRegular", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btn_8.Location = new System.Drawing.Point(68, 4);
      this.btn_8.Name = "btn_8";
      this.btn_8.Size = new System.Drawing.Size(63, 62);
      this.btn_8.TabIndex = 46;
      this.btn_8.TabStop = false;
      this.btn_8.Text = "8";
      this.btn_8.UseVisualStyleBackColor = false;
      this.btn_8.Click += new System.EventHandler(this.KeyboardButtonClicked);
      // 
      // btn_7
      // 
      this.btn_7.BackColor = System.Drawing.Color.LightGray;
      this.btn_7.CausesValidation = false;
      this.btn_7.FlatAppearance.BorderColor = System.Drawing.Color.Black;
      this.btn_7.FlatAppearance.BorderSize = 0;
      this.btn_7.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
      this.btn_7.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
      this.btn_7.FlatStyle = System.Windows.Forms.FlatStyle.System;
      this.btn_7.Font = new System.Drawing.Font("MyriadProRegular", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btn_7.Location = new System.Drawing.Point(5, 4);
      this.btn_7.Name = "btn_7";
      this.btn_7.Size = new System.Drawing.Size(63, 62);
      this.btn_7.TabIndex = 45;
      this.btn_7.TabStop = false;
      this.btn_7.Text = "7";
      this.btn_7.UseVisualStyleBackColor = false;
      this.btn_7.Click += new System.EventHandler(this.KeyboardButtonClicked);
      // 
      // uc_keyboard
      // 
      this.AutoSize = true;
      this.BackColor = System.Drawing.Color.Transparent;
      this.Name = "uc_keyboard";
      this.Size = new System.Drawing.Size(800, 256);
      this.pnl_container.ResumeLayout(false);
      this.pnl_buttons.ResumeLayout(false);
      this.pnl_left.ResumeLayout(false);
      this.pnl_mb_buttons.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private Panel pnl_buttons;
    private Panel pnl_left;
    private Label lbl_line_1;
    private Panel pnl_mb_buttons;
    private Button btn_0;
    private Button btn_esc;
    private Button btn_ok;
    private Button btn_dot;
    private Button btn_3;
    private Button btn_2;
    private Button btn_5;
    private Button btn_1;
    private Button btn_4;
    private Button btn_6;
    private Button btn_9;
    private Button btn_8;
    private Button btn_7;
    private Button btn_f2;
    private Button btn_f1;
    private Button btn_custom_4;
    private Button btn_custom_3;
    private Button btn_custom_2;
    private Button btn_custom_1;
    private Button btn_enter_config;

  }
}
