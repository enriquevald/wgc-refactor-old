//------------------------------------------------------------------------------
// Copyright � 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_configuration.cs
// 
//   DESCRIPTION: User Control Configuration. 
// 
//        AUTHOR: Javi Barea
// 
// CREATION DATE: 30-JUN-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 30-JUN-2017 JBP    First release.
//------------------------------------------------------------------------------
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Diagnostics;

namespace LCDInTouch.Controls
{
  public partial class uc_configuration : uc_base
  {

    #region " Constants "

    private const Int32 DEFAULT_MESSAGE_TIME = 4000;

    #endregion

    #region Enums

    public enum TYPE_SET_FORM
    {
      ALL,
      RESOLUTION,
      LOCATION
    }

    #endregion

    #region Variables
    private Timer m_message_timer;
    #endregion

    #region Properties

    private LCDConfiguration Config
    {
      get { return this.MainForm.InTouch.Config; }
      set { this.MainForm.InTouch.Config = value; }
    }

    public Timer MessageTimer
    {
      get { return this.m_message_timer; }
      set { this.m_message_timer = value; }
    }

    #endregion

    #region Private Functions

    /// <summary>
    /// Get form values
    /// </summary>
    /// <returns></returns>
    private Boolean GetFormValues()
    {
      this.Config.UserMessage = null;

      try
      {
        if (!Int32.TryParse(this.txt_height.Text, out this.Config.Resolution.Height))
        {
          this.Config.SetErrorMessage("'Height' have invalid value");
          return false;
        }

        if (!Int32.TryParse(this.txt_width.Text, out this.Config.Resolution.Width))
        {
          this.Config.SetErrorMessage("'Width' have invalid value");
          return false;
        }

        if (!Int32.TryParse(this.txt_x.Text, out this.Config.Location.X))
        {
          this.Config.SetErrorMessage("'X' have invalid value");
          return false;
        }

        if (!Int32.TryParse(this.txt_y.Text, out this.Config.Location.Y))
        {
          this.Config.SetErrorMessage("'Y' have invalid value");
          return false;
        }

        this.Config.MouseVisibility     = this.chk_mouse.Value;
        this.Config.Development.Enabled = Debugger.IsAttached && this.chk_development.Value; // Set lvl manualmente en fichero de config
        this.Config.MemoryLog           = Debugger.IsAttached && this.chk_memory.Value;
        this.Config.ShowBorders         = Debugger.IsAttached && this.chk_border.Value;

        return true;
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }

      return false;
    } // GetFormValues
    
    /// <summary>
    /// Initialize timer
    /// </summary>
    private void InitializeTimer()
    {
      this.UnbindMessageTimer();

      this.MessageTimer = new Timer();
    } // InitializeTimers

    /// <summary>
    /// Unbind Message timer
    /// </summary>
    private void UnbindMessageTimer()
    {
      try
      {
        if (this.MessageTimer != null)
        {
          this.MessageTimer.Stop();
          this.MessageTimer.Dispose();
        }
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // UnbindMessageTimer

    /// <summary>
    /// Set message timer values
    /// </summary>
    private void SetMessageTimer()
    {
      this.MessageTimer.Stop();
      this.MessageTimer.Tick += new EventHandler(this.MessageTimer_Tick);
      this.MessageTimer.Interval = DEFAULT_MESSAGE_TIME;
      this.MessageTimer.Start();
    } // SetMessageTimer

    /// <summary>
    /// Start message timer
    /// </summary>
    private void StartMessageTimer()
    {
      try
      {
        if (this.InvokeRequired)
        {
          this.BeginInvoke(new MethodInvoker(delegate
          {
            this.StartMessageTimer();
          }));

          return;
        }

        this.InitializeTimer();
        this.SetMessageTimer();
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // StartMessageTimer

    #endregion

    #region Public Functions

    /// <summary>
    /// Constructor
    /// </summary>
    public uc_configuration()
    {
      this.InitializeComponent();
      this.SetControls();
    } // uc_configuration

    /// <summary>
    /// Set form values
    /// </summary>
    /// <param name="Type"></param>
    /// <returns></returns>
    public Boolean SetFormValues()
    {
      return this.SetFormValues(TYPE_SET_FORM.ALL);
    } // SetFormValues
    public Boolean SetFormValues(TYPE_SET_FORM Type)
    {
      this.Config.UserMessage = null;

      try
      {
        if (Type == TYPE_SET_FORM.ALL || Type == TYPE_SET_FORM.RESOLUTION)
        {
          this.txt_height.Text = this.Config.Resolution.Height.ToString();
          this.txt_width.Text = this.Config.Resolution.Width.ToString();
        }

        if (Type == TYPE_SET_FORM.ALL || Type == TYPE_SET_FORM.LOCATION)
        {
          this.txt_x.Text = this.Config.Location.X.ToString();
          this.txt_y.Text = this.Config.Location.Y.ToString();
        }

        if (Type == TYPE_SET_FORM.ALL)
        {
          this.chk_mouse.Value        = this.Config.MouseVisibility;
          this.chk_development.Value  = this.Config.Development.Enabled;
          this.chk_memory.Value       = this.Config.MemoryLog;
          this.chk_border.Value       = this.Config.ShowBorders;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }

      this.Config.SetErrorMessage("Error on load form");

      return false;
    } // SetFormValues

    /// <summary>
    /// Bind method
    /// </summary>
    public void Bind()
    {
      try
      {
        if (this.Parent.Visible)
        {
          return;
        }

        this.Config.Initialize();
        this.SetFormValues();
        this.ShowUserMessage();

        this.Parent.BringToFront();
        this.Parent.Visible = true;
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // Bind

    /// <summary>
    /// Unbind method
    /// </summary>
    public void Unbind()
    {
      try
      {
        if (!this.Parent.Visible)
        {
          return;
        }

        this.Parent.Visible = false;
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // Unbind

    #endregion
    
    #region Internal Functions

    /// <summary>
    /// Set configuration controls
    /// </summary>
    internal void SetControls()
    {
      try
      {
        SafeInvokeExt.SetPropertyValue(this.lbl_message     , "Text"        , String.Empty);
        SafeInvokeExt.SetPropertyValue(this.chk_memory      , "Visible"     , Debugger.IsAttached);
        SafeInvokeExt.SetPropertyValue(this.chk_development , "Visible"     , Debugger.IsAttached);
        SafeInvokeExt.SetPropertyValue(this.chk_border      , "Visible"     , Debugger.IsAttached);
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // SetControls

    /// <summary>
    /// Show user message
    /// </summary>
    internal void ShowUserMessage()
    {
      try
      {
        if (this.Config.UserMessage != null)
        {
          if (this.Config.UserMessage.IsError)
          {
            Log.Add(TYPE_MESSAGE.ERROR, this.Config.UserMessage.Text);
          }

          this.lbl_message.ForeColor = (this.Config.UserMessage.IsError) ? Color.Red : Color.LawnGreen;
          this.lbl_message.Text = this.Config.UserMessage.Text;

          this.StartMessageTimer();
        }
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // ShowUserMessage

    /// <summary>
    /// Update form location
    /// </summary>
    internal void UpdateLocation()
    {
      try
      {
        this.MainForm.Location = new Point(this.Config.Location.X, this.Config.Location.Y);
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // UpdateLocation

    /// <summary>
    /// Save form data
    /// </summary>
    internal void SaveData()
    {
      try
      {
        if (this.GetFormValues())
        {
          if (!this.Config.Save())
          {
            this.Config.SetErrorMessage("Error on save form");
          }
          else
          {
            this.Config.SetInfoMessage("Configuration saved");
          }
        }

        this.UpdateLocation();
        this.ShowUserMessage();
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // SaveData

    /// <summary>
    /// Exit configuration
    /// </summary>
    private void ExitConfiguration()
    {
      try
      {
        this.MainForm.ChangeOnKeyboard = KB_TYPE.TECH;
        this.MainForm.ShowMode = SHOW_MODE.TECH;
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // ExitConfiguration

    /// <summary>
    /// Increase X or Y form position 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="Property"></param>
    internal void Increase(object sender, ref Int32 Property)
    {
      try
      {
        Property += 1;
        this.UpdateForm(sender, Property);
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // Increase

    /// <summary>
    /// Decrease X or Y form position 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="Property"></param>
    internal void Decrease(object sender, ref Int32 Property)
    {
      try
      {
        Property -= 1;
        this.UpdateForm(sender, Property);
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // Decrease

    /// <summary>
    /// Update form position
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="Property"></param>
    internal void UpdateForm(object sender, Int32 Property)
    {
      try
      {
        switch (((Button)sender).Name)
        {
          case "btn_incr_x":
          case "btn_decr_x":
            this.txt_x.Text = Property.ToString();
            break;

          case "btn_incr_y":
          case "btn_decr_y":
            this.txt_y.Text = Property.ToString();
            break;

          case "btn_incr_width":
          case "btn_decr_width":
            this.txt_width.Text = Property.ToString();
            break;

          case "btn_incr_height":
          case "btn_decr_height":
            this.txt_height.Text = Property.ToString();
            break;
        }
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }

      this.Config.SetErrorMessage("Configuration saved");
    } // UpdateForm

    #endregion

    #region Events

    /// <summary>
    /// Launch Calibrate bat for Touch Screen
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btn_calibration_Click_Button(object sender, EventArgs e)
    {
      //this.ButtonCalibrationClick();
      MessageBox.Show("TODO...");
    } // btn_calibration_Click_Button

    /// <summary>
    /// Increase X position event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btn_incr_x_Click(object sender, EventArgs e)
    {
      this.Increase(sender, ref this.Config.Location.X);
      this.UpdateLocation();
    } // btn_incr_x_Click

    /// <summary>
    /// Decrease X position event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btn_decr_x_Click(object sender, EventArgs e)
    {
      this.Decrease(sender, ref this.Config.Location.X);
      this.UpdateLocation();
    } // btn_decr_x_Click

    /// <summary>
    /// Increase Y position event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btn_incr_y_Click(object sender, EventArgs e)
    {
      this.Increase(sender, ref this.Config.Location.Y);
      this.UpdateLocation();
    } // btn_incr_y_Click

    /// <summary>
    /// Decrease Y position event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btn_decr_y_Click(object sender, EventArgs e)
    {
      this.Decrease(sender, ref this.Config.Location.Y);
      this.UpdateLocation();
    } // btn_decr_y_Click

    /// <summary>
    /// Increase heigh size event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btn_incr_height_Click(object sender, EventArgs e)
    {
      this.Increase(sender, ref this.Config.Resolution.Height);
    } // btn_incr_height_Click
    
    /// <summary>
    /// Decrease heigh size event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btn_decr_height_Click(object sender, EventArgs e)
    {
      this.Decrease(sender, ref this.Config.Resolution.Height);
    } // btn_decr_height_Click

    /// <summary>
    /// Increase width size event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btn_incr_width_Click(object sender, EventArgs e)
    {
      this.Increase(sender, ref this.Config.Resolution.Width);
    } // btn_incr_width_Click

    /// <summary>
    /// Decrease width size event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btn_decr_width_Click(object sender, EventArgs e)
    {
      this.Decrease(sender, ref this.Config.Resolution.Width);
    } // btn_decr_width_Click
    
    /// <summary>
    /// Save configuration event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btn_save_config_Click_Button(object sender, EventArgs e)
    {
      this.SaveData();
    } // btn_save_config_Click_Button
    
    /// <summary>
    /// Exist configuration event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btn_exit_config_Click_Button(object sender, EventArgs e)
    {
      this.ExitConfiguration();
    } // btn_exit_config_Click_Button

    /// <summary>
    /// Reset configuration event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btn_reset_location_Click_Button(object sender, EventArgs e)
    {
      try
      {
        this.Config.Location.X = 0;
        this.Config.Location.Y = 0;

        this.UpdateLocation();
        this.SetFormValues(TYPE_SET_FORM.LOCATION);        
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // btn_reset_location_Click_Button

    /// <summary>
    /// Reset resolution configuration event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btn_reset_resolution_Click_Button(object sender, EventArgs e)
    {
      try
      {
        this.Config.SetDefaultResolution();
        this.SetFormValues(TYPE_SET_FORM.RESOLUTION);
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // btn_reset_resolution_Click_Button

    /// <summary>
    /// Message timer tick event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void MessageTimer_Tick(object sender, EventArgs e)
    {
      this.lbl_message.Text = String.Empty;
      this.UnbindMessageTimer();
    } // MessageTimer_Tick
    
    #endregion

  } // uc_configuration
}
