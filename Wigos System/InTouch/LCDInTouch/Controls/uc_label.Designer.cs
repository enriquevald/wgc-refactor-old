namespace LCD_Display.Controls
{
  partial class uc_label
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.pnl_img.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.img)).BeginInit();
      this.pnl_container.SuspendLayout();
      this.SuspendLayout();
      // 
      // btn
      // 
      //this.btn.Size = new System.Drawing.Size(280, 60);
      // 
      // lbl
      // 
      this.lbl.Font = new System.Drawing.Font("MyriadProRegular", 20F);
      this.lbl.ForeColor = System.Drawing.Color.White;
      this.lbl.Location = new System.Drawing.Point(90, 0);
      this.lbl.Size = new System.Drawing.Size(190, 60);
      this.lbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // pnl_img
      // 
      this.pnl_img.Size = new System.Drawing.Size(90, 60);
      // 
      // img
      // 
      this.img.Dock = System.Windows.Forms.DockStyle.Left;
      this.img.Location = new System.Drawing.Point(0, 0);
      this.img.Size = new System.Drawing.Size(55, 60);
      // 
      // pnl_container
      // 
      this.pnl_container.BackColor = System.Drawing.Color.Transparent;
      this.pnl_container.Size = new System.Drawing.Size(280, 60);
      // 
      // uc_label
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
      this.Background_Color = System.Drawing.Color.Transparent;
      this.Control_Style = LCD_Display.TYPE_BUTTON_SIZE_STYLE.Medium;
      this.Control_StyleImage = LCD_Display.TYPE_IMAGE_STYLE.LEFT_LEFT;
      this.Image_Align = System.Windows.Forms.DockStyle.Left;
      this.Image_Width = 90;
      this.IsLabel = true;
      this.Name = "uc_label";
      this.Size = new System.Drawing.Size(280, 60);
      this.Text_Align = System.Drawing.ContentAlignment.MiddleLeft;
      this.Text_Color = System.Drawing.Color.White;
      this.Text_Font = new System.Drawing.Font("MyriadProRegular", 20F);
      this.Text_Width = 190;
      this.Total_Height = 60;
      this.Total_Width = 280;
      this.pnl_img.ResumeLayout(false);
      this.pnl_img.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.img)).EndInit();
      this.pnl_container.ResumeLayout(false);
      this.pnl_container.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion
  }
}
