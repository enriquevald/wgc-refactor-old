﻿//------------------------------------------------------------------------------
// Copyright � 2014 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_base.cs
// 
//   DESCRIPTION: Base Control. 
// 
//        AUTHOR: Javi Barea
// 
// CREATION DATE: 16-MAY-2014
// 
// REVISION HISTORY:
// 
// Date        Author     Description
// ----------- ---------  ----------------------------------------------------------
// 16-MAY-2014 JBP        First release.
// 16-JUL-2014 JAB        Changes to make a default logout timer.
// 13-MAY-2016 JBP        PBI 12541:LCDTouch - Redimensi�n
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace LCDInTouch.Controls
{
  public partial class uc_base : UserControl
  {

    #region Properties

    [Browsable(false)]
    public frm_base MainForm { get { return ((frm_base)this.ParentForm); } }

    #endregion

    #region Public Functions

    public uc_base()
    {
      this.InitializeComponent();
    } // uc_base
    
    #endregion

  }
}
