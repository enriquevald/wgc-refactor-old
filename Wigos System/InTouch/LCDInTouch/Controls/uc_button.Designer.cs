namespace LCD_Display.Controls
{
  partial class uc_button
  {
    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.lbl = new LCD_Display.Controls.uc_button_label();
      this.pnl_img = new System.Windows.Forms.Panel();
      this.img = new System.Windows.Forms.PictureBox();
      this.pnl_container = new System.Windows.Forms.Panel();
      this.pnl_img.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.img)).BeginInit();
      this.pnl_container.SuspendLayout();
      this.SuspendLayout();
      // 
      // lbl
      // 
      this.lbl.BackColor = System.Drawing.Color.Transparent;
      this.lbl.BorderColor = System.Drawing.Color.Empty;
      this.lbl.BorderWidth = 0;
      this.lbl.Dock = System.Windows.Forms.DockStyle.Right;
      this.lbl.Font = new System.Drawing.Font("MyriadProRegular", 22F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl.ForeColor = System.Drawing.Color.Black;
      this.lbl.Location = new System.Drawing.Point(118, 0);
      this.lbl.Name = "lbl";
      this.lbl.Padding = new System.Windows.Forms.Padding(5, 0, 5, 0);
      this.lbl.Size = new System.Drawing.Size(241, 77);
      this.lbl.TabIndex = 1;
      this.lbl.Tag = null;
      this.lbl.Text = "xText";
      this.lbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.lbl.Click += new System.EventHandler(this.uc_button_Click);
      this.lbl.MouseDown += new System.Windows.Forms.MouseEventHandler(this.uc_button_MouseDown);
      this.lbl.MouseUp += new System.Windows.Forms.MouseEventHandler(this.uc_button_MouseUp);
      // 
      // pnl_img
      // 
      this.pnl_img.BackColor = System.Drawing.Color.Transparent;
      this.pnl_img.Controls.Add(this.img);
      this.pnl_img.Dock = System.Windows.Forms.DockStyle.Left;
      this.pnl_img.Location = new System.Drawing.Point(0, 0);
      this.pnl_img.Name = "pnl_img";
      this.pnl_img.Size = new System.Drawing.Size(100, 77);
      this.pnl_img.TabIndex = 2;
      this.pnl_img.Click += new System.EventHandler(this.uc_button_Click);
      this.pnl_img.MouseDown += new System.Windows.Forms.MouseEventHandler(this.uc_button_MouseDown);
      this.pnl_img.MouseUp += new System.Windows.Forms.MouseEventHandler(this.uc_button_MouseUp);
      // 
      // img
      // 
      this.img.BackColor = System.Drawing.Color.Transparent;
      this.img.Dock = System.Windows.Forms.DockStyle.Fill;
      this.img.Location = new System.Drawing.Point(0, 0);
      this.img.Name = "img";
      this.img.Size = new System.Drawing.Size(100, 77);
      this.img.TabIndex = 0;
      this.img.TabStop = false;
      this.img.Click += new System.EventHandler(this.uc_button_Click);
      this.img.MouseDown += new System.Windows.Forms.MouseEventHandler(this.uc_button_MouseDown);
      this.img.MouseUp += new System.Windows.Forms.MouseEventHandler(this.uc_button_MouseUp);
      // 
      // pnl_container
      // 
      this.pnl_container.Controls.Add(this.pnl_img);
      this.pnl_container.Controls.Add(this.lbl);
      this.pnl_container.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pnl_container.Location = new System.Drawing.Point(0, 0);
      this.pnl_container.Name = "pnl_container";
      this.pnl_container.Size = new System.Drawing.Size(359, 77);
      this.pnl_container.TabIndex = 0;
      this.pnl_container.Click += new System.EventHandler(this.uc_button_Click);
      this.pnl_container.MouseDown += new System.Windows.Forms.MouseEventHandler(this.uc_button_MouseDown);
      this.pnl_container.MouseUp += new System.Windows.Forms.MouseEventHandler(this.uc_button_MouseUp);
      // 
      // uc_button
      // 
      this.Controls.Add(this.pnl_container);
      this.Margin = new System.Windows.Forms.Padding(8);
      this.Name = "uc_button";
      this.Size = new System.Drawing.Size(359, 77);
      this.Click += new System.EventHandler(this.uc_button_Click);
      this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.uc_button_MouseDown);
      this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.uc_button_MouseUp);
      this.pnl_img.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.img)).EndInit();
      this.pnl_container.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    public uc_button_label lbl;
    public System.Windows.Forms.Panel pnl_img;
    public System.Windows.Forms.PictureBox img;
    public System.Windows.Forms.Panel pnl_container;
  }
}
