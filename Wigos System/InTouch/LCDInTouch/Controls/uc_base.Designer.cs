﻿namespace LCDInTouch.Controls
{
  partial class uc_base
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.pnl_container = new System.Windows.Forms.Panel();
      this.SuspendLayout();
      // 
      // pnl_container
      // 
      this.pnl_container.BackColor = System.Drawing.Color.Transparent;
      this.pnl_container.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pnl_container.Font = new System.Drawing.Font("MyriadProRegular", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.pnl_container.Location = new System.Drawing.Point(0, 0);
      this.pnl_container.Name = "pnl_container";
      this.pnl_container.Size = new System.Drawing.Size(150, 150);
      this.pnl_container.TabIndex = 1;
      // 
      // uc_base
      // 
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
      this.BackColor = System.Drawing.Color.Transparent;
      this.Controls.Add(this.pnl_container);
      this.Name = "uc_base";
      this.ResumeLayout(false);

    }

    #endregion

    public System.Windows.Forms.Panel pnl_container;

  }
}
