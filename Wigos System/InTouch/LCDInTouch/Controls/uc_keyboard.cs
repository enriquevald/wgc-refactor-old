//------------------------------------------------------------------------------
// Copyright � 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_keyboard.cs
// 
//   DESCRIPTION: User Control Keyboard. 
// 
//        AUTHOR: Javi Barea
// 
// CREATION DATE: 30-JUN-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 30-JUN-2017 JBP    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

using System.IO;
using System.Threading;
using LCDInTouch;
using LCDInTouch.Controls;

namespace LCDInTouch.Controls
{
  public partial class uc_keyboard : uc_base
  {
    #region Fields

    public Boolean m_buttons_up_down_showed;
    public Image m_pnl_logo_image;
    private Font FONT_LUCIDA_CONS_24 = new Font("Lucida Console", 24.0f);
    private Font FONT_LUCIDA_CONS_22 = new Font("Lucida Console", 22.0f);

    #endregion

    // This delegate enables asynchronous calls for setting
    // the text property on a TextBox control.
    delegate void SetTextCallback(string text);

    #region Private Properties

    internal String OperatorText
    {
      get { return this.lbl_line_1.Text; }
    }

    #endregion

    #region Public Functions

    public uc_keyboard()
    {
      this.InitializeComponent();
      this.Init();
    }

    public String Message
    {
      get { return this.lbl_line_1.Text; }
    }


    /// <summary>
    /// Bind method
    /// </summary>
    public void Bind()
    {
      try
      {
        if (this.Parent.Visible)
        {
          return;
        }

        this.Parent.BringToFront();
        this.Parent.Visible = true;
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // Bind

    /// <summary>
    /// Unbind method
    /// </summary>
    public void Unbind()
    {
      try
      {
        if (!this.Parent.Visible)
        {
          return;
        }

        this.Parent.Visible = false;
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // Unbind

    #endregion

    #region Internal Functions

    /// <summary>
    /// Initialize control
    /// </summary>
    internal void Init()
    {
      this.SetControls();
      this.OnKeyboard_Exit();
    } // Init

    /// <summary>
    /// To initialize controls texts
    /// </summary>
    internal void SetControls()
    {
      if (this.InvokeRequired)
      {
        this.BeginInvoke(new MethodInvoker(delegate
        {
          this.SetControls();
        }));

        return;
      }

      this.btn_enter_config.Text = "Display Config";
    } // SetControls

    /// <summary>
    /// Set Text in line 1
    /// </summary>
    /// <param name="Text"></param>
    internal void SetText(string Text)
    {
      try
      {
        if (!this.lbl_line_1.InvokeRequired)
        {
          if (this.lbl_line_1.Text != Text)
          {
            this.lbl_line_1.Text = Text;
          }
          return;
        }
        
        // InvokeRequired required compares the thread ID of the
        // calling thread to the thread ID of the creating thread.
        // If these threads are different, it returns true.
        SetTextCallback d = new SetTextCallback(SetText);
        this.Invoke(d, new object[] { Text });
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // SetText

    #endregion

    #region Private Functions

    /// <summary>
    /// Prepare Keyboard for exit
    /// </summary>
    private void OnKeyboard_Exit()
    {
      try
      {
        this.lbl_line_1.Text = String.Empty;     
        this.pnl_buttons.Visible    = false;
        this.pnl_mb_buttons.Visible = false;                 
      }
      catch (Exception ex)
      {
        Log.Add(ex);
      }
    } // OnKeyboard_Exit

    /// <summary>
    /// Action on Keyboard load complete
    /// </summary>
    private void OnKeyboard_Complete()
    {
      try
      {
        this.pnl_buttons.Visible = false;
        this.pnl_mb_buttons.Visible = true;
      }
      catch (Exception ex)
      {
        Log.Add(ex);
      }
    } // OnKeyboard_Complete

    /// <summary>
    /// Set Keyboard 
    /// </summary>
    private void SetKeyboard_Tech()
    {
      try
      {
        this.pnl_buttons.Visible = false;
        this.pnl_mb_buttons.Visible = true;
        this.btn_enter_config.Visible = true;
      }
      catch (Exception ex)
      {
        Log.Add(ex);
      }
    } // SetKeyboard_Tech

    /// <summary>
    /// Show configuration panel
    /// </summary>
    private void ShowConfigurationPanel()
    {
      this.MainForm.ShowMode = SHOW_MODE.CONFIG;
    } // ShowConfigurationPanel

    /// <summary>
    /// SetKeyboard_MobileBank
    /// </summary>
    private void SetKeyboard_MobileBank()
    {
      try
      {
        this.pnl_buttons.Visible = true;
        this.pnl_mb_buttons.Visible = true;
      }
      catch (Exception ex)
      {
        Log.Add(ex);
      }
    } // SetKeyboard_MobileBank

    /// <summary>
    /// Configuration button click event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void ConfigurationButtonClicked(object sender, EventArgs e)
    {
      this.MainForm.ChangeOnKeyboard = KB_TYPE.CONFIGURATION;
    } // ConfigurationButtonClicked

    /// <summary>
    /// Keyboard button click event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void KeyboardButtonClicked(object sender, EventArgs e)
    {
      String _text;
      Int32 _value;

      try
      {
        if (this.MainForm.InTouch.KeyHandler != null)
        {
          if (this.MainForm != null)
          {
            this.MainForm.ResetActivities();
            this.MainForm.InTouch.SendProtocolHandler(TYPE_REQUEST.LCD_ACTIVITY, String.Empty);
          }

          _text = ((Button)sender).Text;

          if (_text.Length == 1)
          {
            this.MainForm.InTouch.KeyHandler((int)_text[0]);
            return;
          }

          switch (_text)
          {
            case "F1":
            {
              this.MainForm.InTouch.KeyHandler((int)'a');
              return;
            }

            case "F2":
            {
              this.MainForm.InTouch.KeyHandler((int)'b');
              return;
            }

            case "ESC":
            {
              this.MainForm.InTouch.LogoutManagement();
              return;
            }

            case "OK":
            {
              this.MainForm.InTouch.KeyHandler((int)0x0D);
              return;
            }

            default:
            {
              if (Int32.TryParse(_text, out _value))
              {
                for (int _idx_char = 0; _idx_char < _text.Length; _idx_char++)
                {
                  this.MainForm.InTouch.KeyHandler((int)_text[_idx_char]);
                }
                if (_value > 0)
                {
                  this.MainForm.InTouch.KeyHandler((int)0x0D);
                }
              }

              return;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
      finally
      {
        this.pnl_mb_buttons.Focus();
      }
    } // KeyboardButtonClicked

    /// <summary>
    /// Send logout to SASHost
    /// </summary>
    public void Logout()
    {
      Int32 _idx;

      if (this.MainForm != null
        && this.MainForm.InTouch.KeyHandler != null)
      {
        for (_idx = 0; _idx < 5; _idx++)
        {
          this.MainForm.InTouch.KeyHandler((int)0x08);
          Thread.Sleep(100);
        }
      }
    } // Logout

    /// <summary>
    /// Hide operation buttons
    /// </summary>
    /// <param name="SendESC"></param>
    public void HideOperatorButtons(Boolean SendESC)
    {
      try
      {
        if (this.InvokeRequired)
        {
          this.BeginInvoke(new MethodInvoker(delegate
          {
            this.HideOperatorButtons(SendESC);
          }));

          return;
        }

        this.SuspendLayout();

        if (SendESC)
        {
          this.Logout();            
        }

        this.btn_enter_config.Visible = false;
        this.ResumeLayout();
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // HideOperatorButtons

    /// <summary>
    /// Set keyboard style
    /// </summary>
    public void SetKeyboardStyle()
    {
      KB_TYPE _action;

      try
      {
        if (this.InvokeRequired)
        {
          this.BeginInvoke(new MethodInvoker(delegate
          {
            this.SetKeyboardStyle();
          }));

          return;
        }

        _action = this.MainForm.ChangeOnKeyboard;
        this.btn_enter_config.Visible = false;
        this.BackColor = (_action == KB_TYPE.EXIT) ? Color.Transparent : Color.Black;
          
        switch (_action)
        {
          case KB_TYPE.MOBILE_BANK:
            this.SetKeyboard_MobileBank();            
          break;

          case KB_TYPE.TECH:
            this.SetKeyboard_Tech();
          break;    
        
          case KB_TYPE.CONFIGURATION:
            this.ShowConfigurationPanel();
          break;  
          
          case KB_TYPE.COMPLETE:
            this.OnKeyboard_Complete();
            break;

          default:
          case KB_TYPE.EXIT:            
            this.OnKeyboard_Exit();             
            break;
        }
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // SetKeyboardStyle

    #endregion

  } // uc_keyboard
}
