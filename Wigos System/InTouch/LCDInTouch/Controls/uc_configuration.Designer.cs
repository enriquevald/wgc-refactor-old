using System.Windows.Forms;
namespace LCDInTouch.Controls
{
  partial class uc_configuration : uc_base
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.lbl_location = new System.Windows.Forms.Label();
      this.lbl_y = new System.Windows.Forms.Label();
      this.txt_y = new System.Windows.Forms.TextBox();
      this.txt_x = new System.Windows.Forms.TextBox();
      this.lbl_x = new System.Windows.Forms.Label();
      this.btn_incr_x = new System.Windows.Forms.Button();
      this.btn_decr_x = new System.Windows.Forms.Button();
      this.btn_decr_y = new System.Windows.Forms.Button();
      this.btn_incr_y = new System.Windows.Forms.Button();
      this.lbl_message = new System.Windows.Forms.Label();
      this.btn_save_config = new System.Windows.Forms.Button();
      this.btn_calibration = new System.Windows.Forms.Button();
      this.btn_exit = new System.Windows.Forms.Button();
      this.btn_reset_location = new System.Windows.Forms.Button();
      this.chk_mouse = new LCDInTouch.Controls.uc_check();
      this.chk_development = new LCDInTouch.Controls.uc_check();
      this.chk_border = new LCDInTouch.Controls.uc_check();
      this.chk_memory = new LCDInTouch.Controls.uc_check();
      this.lbl_resolution = new System.Windows.Forms.Label();
      this.lbl_width = new System.Windows.Forms.Label();
      this.txt_width = new System.Windows.Forms.TextBox();
      this.lbl_height = new System.Windows.Forms.Label();
      this.txt_height = new System.Windows.Forms.TextBox();
      this.btn_incr_height = new System.Windows.Forms.Button();
      this.btn_decr_height = new System.Windows.Forms.Button();
      this.btn_incr_width = new System.Windows.Forms.Button();
      this.btn_decr_width = new System.Windows.Forms.Button();
      this.btn_reset_resolution = new System.Windows.Forms.Button();
      this.pnl_container.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_container
      // 
      this.pnl_container.BackColor = System.Drawing.Color.Black;
      this.pnl_container.Controls.Add(this.btn_reset_resolution);
      this.pnl_container.Controls.Add(this.btn_reset_location);
      this.pnl_container.Controls.Add(this.chk_memory);
      this.pnl_container.Controls.Add(this.btn_calibration);
      this.pnl_container.Controls.Add(this.chk_border);
      this.pnl_container.Controls.Add(this.chk_development);
      this.pnl_container.Controls.Add(this.chk_mouse);
      this.pnl_container.Controls.Add(this.btn_exit);
      this.pnl_container.Controls.Add(this.btn_save_config);
      this.pnl_container.Controls.Add(this.lbl_message);
      this.pnl_container.Controls.Add(this.btn_decr_width);
      this.pnl_container.Controls.Add(this.btn_incr_width);
      this.pnl_container.Controls.Add(this.btn_decr_height);
      this.pnl_container.Controls.Add(this.btn_incr_height);
      this.pnl_container.Controls.Add(this.txt_height);
      this.pnl_container.Controls.Add(this.lbl_height);
      this.pnl_container.Controls.Add(this.txt_width);
      this.pnl_container.Controls.Add(this.lbl_width);
      this.pnl_container.Controls.Add(this.lbl_resolution);
      this.pnl_container.Controls.Add(this.btn_decr_y);
      this.pnl_container.Controls.Add(this.btn_incr_y);
      this.pnl_container.Controls.Add(this.btn_decr_x);
      this.pnl_container.Controls.Add(this.btn_incr_x);
      this.pnl_container.Controls.Add(this.txt_x);
      this.pnl_container.Controls.Add(this.lbl_x);
      this.pnl_container.Controls.Add(this.txt_y);
      this.pnl_container.Controls.Add(this.lbl_y);
      this.pnl_container.Controls.Add(this.lbl_location);
      this.pnl_container.Size = new System.Drawing.Size(800, 258);
      // 
      // lbl_location
      // 
      this.lbl_location.Font = new System.Drawing.Font("MyriadProRegular", 22F);
      this.lbl_location.ForeColor = System.Drawing.Color.White;
      this.lbl_location.Location = new System.Drawing.Point(43, 16);
      this.lbl_location.Margin = new System.Windows.Forms.Padding(8);
      this.lbl_location.Name = "lbl_location";
      this.lbl_location.Size = new System.Drawing.Size(217, 33);
      this.lbl_location.TabIndex = 0;
      this.lbl_location.Text = "Location";
      this.lbl_location.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lbl_y
      // 
      this.lbl_y.Font = new System.Drawing.Font("MyriadProRegular", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_y.ForeColor = System.Drawing.Color.White;
      this.lbl_y.Location = new System.Drawing.Point(43, 102);
      this.lbl_y.Margin = new System.Windows.Forms.Padding(8);
      this.lbl_y.Name = "lbl_y";
      this.lbl_y.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
      this.lbl_y.Size = new System.Drawing.Size(36, 30);
      this.lbl_y.TabIndex = 4;
      this.lbl_y.Text = "Y";
      this.lbl_y.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // txt_y
      // 
      this.txt_y.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.txt_y.Font = new System.Drawing.Font("MyriadProRegular", 21F, System.Drawing.FontStyle.Bold);
      this.txt_y.ForeColor = System.Drawing.Color.Black;
      this.txt_y.Location = new System.Drawing.Point(90, 102);
      this.txt_y.MaxLength = 5;
      this.txt_y.Name = "txt_y";
      this.txt_y.Size = new System.Drawing.Size(80, 34);
      this.txt_y.TabIndex = 26;
      this.txt_y.Text = "999";
      this.txt_y.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // txt_x
      // 
      this.txt_x.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.txt_x.Font = new System.Drawing.Font("MyriadProRegular", 21F, System.Drawing.FontStyle.Bold);
      this.txt_x.ForeColor = System.Drawing.Color.Black;
      this.txt_x.Location = new System.Drawing.Point(90, 60);
      this.txt_x.MaxLength = 5;
      this.txt_x.Name = "txt_x";
      this.txt_x.Size = new System.Drawing.Size(80, 34);
      this.txt_x.TabIndex = 28;
      this.txt_x.Text = "999";
      this.txt_x.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // lbl_x
      // 
      this.lbl_x.Font = new System.Drawing.Font("MyriadProRegular", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_x.ForeColor = System.Drawing.Color.White;
      this.lbl_x.Location = new System.Drawing.Point(43, 60);
      this.lbl_x.Margin = new System.Windows.Forms.Padding(8);
      this.lbl_x.Name = "lbl_x";
      this.lbl_x.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
      this.lbl_x.Size = new System.Drawing.Size(36, 30);
      this.lbl_x.TabIndex = 27;
      this.lbl_x.Text = "X";
      this.lbl_x.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // btn_incr_x
      // 
      this.btn_incr_x.BackColor = System.Drawing.Color.Gray;
      this.btn_incr_x.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_incr_x.Font = new System.Drawing.Font("MyriadProRegular", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btn_incr_x.ForeColor = System.Drawing.Color.White;
      this.btn_incr_x.Location = new System.Drawing.Point(224, 61);
      this.btn_incr_x.Margin = new System.Windows.Forms.Padding(0);
      this.btn_incr_x.Name = "btn_incr_x";
      this.btn_incr_x.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
      this.btn_incr_x.Size = new System.Drawing.Size(36, 33);
      this.btn_incr_x.TabIndex = 29;
      this.btn_incr_x.Text = "+";
      this.btn_incr_x.UseVisualStyleBackColor = false;
      this.btn_incr_x.Click += new System.EventHandler(this.btn_incr_x_Click);
      // 
      // btn_decr_x
      // 
      this.btn_decr_x.BackColor = System.Drawing.Color.Gray;
      this.btn_decr_x.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_decr_x.Font = new System.Drawing.Font("MyriadProRegular", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btn_decr_x.ForeColor = System.Drawing.Color.White;
      this.btn_decr_x.Location = new System.Drawing.Point(181, 61);
      this.btn_decr_x.Margin = new System.Windows.Forms.Padding(0);
      this.btn_decr_x.Name = "btn_decr_x";
      this.btn_decr_x.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
      this.btn_decr_x.Size = new System.Drawing.Size(36, 33);
      this.btn_decr_x.TabIndex = 30;
      this.btn_decr_x.Text = "-";
      this.btn_decr_x.UseVisualStyleBackColor = false;
      this.btn_decr_x.Click += new System.EventHandler(this.btn_decr_x_Click);
      // 
      // btn_decr_y
      // 
      this.btn_decr_y.BackColor = System.Drawing.Color.Gray;
      this.btn_decr_y.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_decr_y.Font = new System.Drawing.Font("MyriadProRegular", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btn_decr_y.ForeColor = System.Drawing.Color.White;
      this.btn_decr_y.Location = new System.Drawing.Point(181, 103);
      this.btn_decr_y.Margin = new System.Windows.Forms.Padding(0);
      this.btn_decr_y.Name = "btn_decr_y";
      this.btn_decr_y.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
      this.btn_decr_y.Size = new System.Drawing.Size(36, 33);
      this.btn_decr_y.TabIndex = 32;
      this.btn_decr_y.Text = "-";
      this.btn_decr_y.UseVisualStyleBackColor = false;
      this.btn_decr_y.Click += new System.EventHandler(this.btn_decr_y_Click);
      // 
      // btn_incr_y
      // 
      this.btn_incr_y.BackColor = System.Drawing.Color.Gray;
      this.btn_incr_y.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_incr_y.Font = new System.Drawing.Font("MyriadProRegular", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btn_incr_y.ForeColor = System.Drawing.Color.White;
      this.btn_incr_y.Location = new System.Drawing.Point(224, 103);
      this.btn_incr_y.Margin = new System.Windows.Forms.Padding(0);
      this.btn_incr_y.Name = "btn_incr_y";
      this.btn_incr_y.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
      this.btn_incr_y.Size = new System.Drawing.Size(36, 33);
      this.btn_incr_y.TabIndex = 31;
      this.btn_incr_y.Text = "+";
      this.btn_incr_y.UseVisualStyleBackColor = false;
      this.btn_incr_y.Click += new System.EventHandler(this.btn_incr_y_Click);
      // 
      // lbl_message
      // 
      this.lbl_message.Font = new System.Drawing.Font("MyriadProRegular", 14.25F);
      this.lbl_message.ForeColor = System.Drawing.Color.White;
      this.lbl_message.Location = new System.Drawing.Point(7, 215);
      this.lbl_message.Name = "lbl_message";
      this.lbl_message.Size = new System.Drawing.Size(421, 35);
      this.lbl_message.TabIndex = 42;
      this.lbl_message.Text = "lbl_message";
      this.lbl_message.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // btn_save_config
      // 
      this.btn_save_config.BackColor = System.Drawing.Color.Gray;
      this.btn_save_config.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_save_config.Font = new System.Drawing.Font("MyriadProRegular", 18F);
      this.btn_save_config.ForeColor = System.Drawing.Color.White;
      this.btn_save_config.Location = new System.Drawing.Point(523, 204);
      this.btn_save_config.Margin = new System.Windows.Forms.Padding(8);
      this.btn_save_config.Name = "btn_save_config";
      this.btn_save_config.Size = new System.Drawing.Size(120, 46);
      this.btn_save_config.TabIndex = 43;
      this.btn_save_config.Text = "Save";
      this.btn_save_config.UseVisualStyleBackColor = false;
      this.btn_save_config.Click += new System.EventHandler(this.btn_save_config_Click_Button);
      // 
      // btn_calibration
      // 
      this.btn_calibration.BackColor = System.Drawing.Color.Gray;
      this.btn_calibration.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_calibration.Font = new System.Drawing.Font("MyriadProRegular", 22F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btn_calibration.ForeColor = System.Drawing.Color.White;
      this.btn_calibration.Location = new System.Drawing.Point(449, 204);
      this.btn_calibration.Margin = new System.Windows.Forms.Padding(8);
      this.btn_calibration.Name = "btn_calibration";
      this.btn_calibration.Size = new System.Drawing.Size(58, 46);
      this.btn_calibration.TabIndex = 45;
      this.btn_calibration.UseVisualStyleBackColor = false;
      this.btn_calibration.Click += new System.EventHandler(this.btn_calibration_Click_Button);
      // 
      // btn_exit
      // 
      this.btn_exit.BackColor = System.Drawing.Color.Gray;
      this.btn_exit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_exit.Font = new System.Drawing.Font("MyriadProRegular", 18F);
      this.btn_exit.ForeColor = System.Drawing.Color.White;
      this.btn_exit.Location = new System.Drawing.Point(659, 204);
      this.btn_exit.Margin = new System.Windows.Forms.Padding(8);
      this.btn_exit.Name = "btn_exit";
      this.btn_exit.Size = new System.Drawing.Size(120, 46);
      this.btn_exit.TabIndex = 43;
      this.btn_exit.Text = "Exit";
      this.btn_exit.UseVisualStyleBackColor = false;
      this.btn_exit.Click += new System.EventHandler(this.btn_exit_config_Click_Button);
      // 
      // btn_reset_location
      // 
      this.btn_reset_location.BackColor = System.Drawing.Color.Gray;
      this.btn_reset_location.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_reset_location.Font = new System.Drawing.Font("MyriadProRegular", 16F);
      this.btn_reset_location.ForeColor = System.Drawing.Color.White;
      this.btn_reset_location.Location = new System.Drawing.Point(181, 145);
      this.btn_reset_location.Margin = new System.Windows.Forms.Padding(8);
      this.btn_reset_location.Name = "btn_reset_location";
      this.btn_reset_location.Size = new System.Drawing.Size(79, 32);
      this.btn_reset_location.TabIndex = 47;
      this.btn_reset_location.Text = "RESET";
      this.btn_reset_location.UseVisualStyleBackColor = false;
      this.btn_reset_location.Click += new System.EventHandler(this.btn_reset_location_Click_Button);
      // 
      // chk_mouse
      // 
      this.chk_mouse.BackColor = System.Drawing.Color.Black;
      this.chk_mouse.Location = new System.Drawing.Point(562, 20);
      this.chk_mouse.Margin = new System.Windows.Forms.Padding(8);
      this.chk_mouse.Name = "chk_mouse";
      this.chk_mouse.Size = new System.Drawing.Size(217, 37);
      this.chk_mouse.TabIndex = 44;
      this.chk_mouse.Text_Align = System.Drawing.ContentAlignment.MiddleRight;
      this.chk_mouse.Text_Color = System.Drawing.Color.White;
      this.chk_mouse.Text_Font = new System.Drawing.Font("MyriadProRegular", 15.75F);
      this.chk_mouse.Text_Literal = "Mouse";
      this.chk_mouse.Text_Padding = new System.Windows.Forms.Padding(0);
      this.chk_mouse.Text_Width = 153;
      this.chk_mouse.Total_Height = 37;
      this.chk_mouse.Total_Width = 217;
      this.chk_mouse.Value = false;
      // 
      // chk_development
      // 
      this.chk_development.BackColor = System.Drawing.Color.Transparent;
      this.chk_development.Location = new System.Drawing.Point(562, 112);
      this.chk_development.Margin = new System.Windows.Forms.Padding(8);
      this.chk_development.Name = "chk_development";
      this.chk_development.Size = new System.Drawing.Size(217, 37);
      this.chk_development.TabIndex = 44;
      this.chk_development.Text_Align = System.Drawing.ContentAlignment.MiddleRight;
      this.chk_development.Text_Color = System.Drawing.Color.White;
      this.chk_development.Text_Font = new System.Drawing.Font("MyriadProRegular", 15.75F);
      this.chk_development.Text_Literal = "Develpment";
      this.chk_development.Text_Padding = new System.Windows.Forms.Padding(0);
      this.chk_development.Text_Width = 153;
      this.chk_development.Total_Height = 37;
      this.chk_development.Total_Width = 217;
      this.chk_development.Value = false;
      // 
      // chk_border
      // 
      this.chk_border.BackColor = System.Drawing.Color.Transparent;
      this.chk_border.Location = new System.Drawing.Point(562, 66);
      this.chk_border.Margin = new System.Windows.Forms.Padding(8);
      this.chk_border.Name = "chk_border";
      this.chk_border.Size = new System.Drawing.Size(217, 37);
      this.chk_border.TabIndex = 44;
      this.chk_border.Text_Align = System.Drawing.ContentAlignment.MiddleRight;
      this.chk_border.Text_Color = System.Drawing.Color.White;
      this.chk_border.Text_Font = new System.Drawing.Font("MyriadProRegular", 15.75F);
      this.chk_border.Text_Literal = "Border";
      this.chk_border.Text_Padding = new System.Windows.Forms.Padding(0);
      this.chk_border.Text_Width = 153;
      this.chk_border.Total_Height = 37;
      this.chk_border.Total_Width = 217;
      this.chk_border.Value = false;
      // 
      // chk_memory
      // 
      this.chk_memory.BackColor = System.Drawing.Color.Transparent;
      this.chk_memory.Location = new System.Drawing.Point(562, 158);
      this.chk_memory.Margin = new System.Windows.Forms.Padding(8);
      this.chk_memory.Name = "chk_memory";
      this.chk_memory.Size = new System.Drawing.Size(217, 37);
      this.chk_memory.TabIndex = 46;
      this.chk_memory.Text_Align = System.Drawing.ContentAlignment.MiddleRight;
      this.chk_memory.Text_Color = System.Drawing.Color.White;
      this.chk_memory.Text_Font = new System.Drawing.Font("MyriadProRegular", 15.75F);
      this.chk_memory.Text_Literal = "Memory";
      this.chk_memory.Text_Padding = new System.Windows.Forms.Padding(0);
      this.chk_memory.Text_Width = 153;
      this.chk_memory.Total_Height = 37;
      this.chk_memory.Total_Width = 217;
      this.chk_memory.Value = false;
      // 
      // lbl_resolution
      // 
      this.lbl_resolution.BackColor = System.Drawing.Color.Black;
      this.lbl_resolution.Font = new System.Drawing.Font("MyriadProRegular", 22F);
      this.lbl_resolution.ForeColor = System.Drawing.Color.White;
      this.lbl_resolution.Location = new System.Drawing.Point(299, 16);
      this.lbl_resolution.Margin = new System.Windows.Forms.Padding(8);
      this.lbl_resolution.Name = "lbl_resolution";
      this.lbl_resolution.Size = new System.Drawing.Size(208, 33);
      this.lbl_resolution.TabIndex = 33;
      this.lbl_resolution.Text = "Resolution";
      this.lbl_resolution.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.lbl_resolution.Visible = false;
      // 
      // lbl_width
      // 
      this.lbl_width.BackColor = System.Drawing.Color.Black;
      this.lbl_width.Font = new System.Drawing.Font("MyriadProRegular", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_width.ForeColor = System.Drawing.Color.White;
      this.lbl_width.Location = new System.Drawing.Point(299, 102);
      this.lbl_width.Margin = new System.Windows.Forms.Padding(8);
      this.lbl_width.Name = "lbl_width";
      this.lbl_width.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
      this.lbl_width.Size = new System.Drawing.Size(36, 30);
      this.lbl_width.TabIndex = 34;
      this.lbl_width.Text = "W";
      this.lbl_width.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.lbl_width.Visible = false;
      // 
      // txt_width
      // 
      this.txt_width.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.txt_width.Font = new System.Drawing.Font("MyriadProRegular", 21F, System.Drawing.FontStyle.Bold);
      this.txt_width.ForeColor = System.Drawing.Color.Black;
      this.txt_width.Location = new System.Drawing.Point(337, 102);
      this.txt_width.MaxLength = 4;
      this.txt_width.Name = "txt_width";
      this.txt_width.Size = new System.Drawing.Size(80, 34);
      this.txt_width.TabIndex = 35;
      this.txt_width.Text = "9999";
      this.txt_width.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.txt_width.Visible = false;
      // 
      // lbl_height
      // 
      this.lbl_height.BackColor = System.Drawing.Color.Black;
      this.lbl_height.Font = new System.Drawing.Font("MyriadProRegular", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_height.ForeColor = System.Drawing.Color.White;
      this.lbl_height.Location = new System.Drawing.Point(299, 60);
      this.lbl_height.Margin = new System.Windows.Forms.Padding(8);
      this.lbl_height.Name = "lbl_height";
      this.lbl_height.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
      this.lbl_height.Size = new System.Drawing.Size(36, 30);
      this.lbl_height.TabIndex = 36;
      this.lbl_height.Text = "H";
      this.lbl_height.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.lbl_height.Visible = false;
      // 
      // txt_height
      // 
      this.txt_height.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.txt_height.Font = new System.Drawing.Font("MyriadProRegular", 21F, System.Drawing.FontStyle.Bold);
      this.txt_height.ForeColor = System.Drawing.Color.Black;
      this.txt_height.Location = new System.Drawing.Point(337, 60);
      this.txt_height.MaxLength = 4;
      this.txt_height.Name = "txt_height";
      this.txt_height.Size = new System.Drawing.Size(80, 34);
      this.txt_height.TabIndex = 37;
      this.txt_height.Text = "9999";
      this.txt_height.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.txt_height.Visible = false;
      // 
      // btn_incr_height
      // 
      this.btn_incr_height.BackColor = System.Drawing.Color.Gray;
      this.btn_incr_height.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_incr_height.Font = new System.Drawing.Font("MyriadProRegular", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btn_incr_height.ForeColor = System.Drawing.Color.White;
      this.btn_incr_height.Location = new System.Drawing.Point(471, 61);
      this.btn_incr_height.Margin = new System.Windows.Forms.Padding(0);
      this.btn_incr_height.Name = "btn_incr_height";
      this.btn_incr_height.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
      this.btn_incr_height.Size = new System.Drawing.Size(36, 33);
      this.btn_incr_height.TabIndex = 38;
      this.btn_incr_height.Text = "+";
      this.btn_incr_height.UseVisualStyleBackColor = false;
      this.btn_incr_height.Visible = false;
      this.btn_incr_height.Click += new System.EventHandler(this.btn_incr_height_Click);
      // 
      // btn_decr_height
      // 
      this.btn_decr_height.BackColor = System.Drawing.Color.Gray;
      this.btn_decr_height.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_decr_height.Font = new System.Drawing.Font("MyriadProRegular", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btn_decr_height.ForeColor = System.Drawing.Color.White;
      this.btn_decr_height.Location = new System.Drawing.Point(428, 61);
      this.btn_decr_height.Margin = new System.Windows.Forms.Padding(0);
      this.btn_decr_height.Name = "btn_decr_height";
      this.btn_decr_height.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
      this.btn_decr_height.Size = new System.Drawing.Size(36, 33);
      this.btn_decr_height.TabIndex = 39;
      this.btn_decr_height.Text = "-";
      this.btn_decr_height.UseVisualStyleBackColor = false;
      this.btn_decr_height.Visible = false;
      this.btn_decr_height.Click += new System.EventHandler(this.btn_decr_height_Click);
      // 
      // btn_incr_width
      // 
      this.btn_incr_width.BackColor = System.Drawing.Color.Gray;
      this.btn_incr_width.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_incr_width.Font = new System.Drawing.Font("MyriadProRegular", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btn_incr_width.ForeColor = System.Drawing.Color.White;
      this.btn_incr_width.Location = new System.Drawing.Point(471, 103);
      this.btn_incr_width.Margin = new System.Windows.Forms.Padding(0);
      this.btn_incr_width.Name = "btn_incr_width";
      this.btn_incr_width.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
      this.btn_incr_width.Size = new System.Drawing.Size(36, 33);
      this.btn_incr_width.TabIndex = 40;
      this.btn_incr_width.Text = "+";
      this.btn_incr_width.UseVisualStyleBackColor = false;
      this.btn_incr_width.Visible = false;
      this.btn_incr_width.Click += new System.EventHandler(this.btn_incr_width_Click);
      // 
      // btn_decr_width
      // 
      this.btn_decr_width.BackColor = System.Drawing.Color.Gray;
      this.btn_decr_width.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_decr_width.Font = new System.Drawing.Font("MyriadProRegular", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btn_decr_width.ForeColor = System.Drawing.Color.White;
      this.btn_decr_width.Location = new System.Drawing.Point(428, 103);
      this.btn_decr_width.Margin = new System.Windows.Forms.Padding(0);
      this.btn_decr_width.Name = "btn_decr_width";
      this.btn_decr_width.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
      this.btn_decr_width.Size = new System.Drawing.Size(36, 33);
      this.btn_decr_width.TabIndex = 41;
      this.btn_decr_width.Text = "-";
      this.btn_decr_width.UseVisualStyleBackColor = false;
      this.btn_decr_width.Visible = false;
      this.btn_decr_width.Click += new System.EventHandler(this.btn_decr_width_Click);
      // 
      // btn_reset_resolution
      // 
      this.btn_reset_resolution.BackColor = System.Drawing.Color.Gray;
      this.btn_reset_resolution.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_reset_resolution.Font = new System.Drawing.Font("MyriadProRegular", 16F);
      this.btn_reset_resolution.ForeColor = System.Drawing.Color.White;
      this.btn_reset_resolution.Location = new System.Drawing.Point(428, 145);
      this.btn_reset_resolution.Margin = new System.Windows.Forms.Padding(8);
      this.btn_reset_resolution.Name = "btn_reset_resolution";
      this.btn_reset_resolution.Size = new System.Drawing.Size(79, 32);
      this.btn_reset_resolution.TabIndex = 47;
      this.btn_reset_resolution.Text = "RESET";
      this.btn_reset_resolution.UseVisualStyleBackColor = false;
      this.btn_reset_resolution.Visible = false;
      this.btn_reset_resolution.Click += new System.EventHandler(this.btn_reset_resolution_Click_Button);
      // 
      // uc_configuration
      // 
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
      this.Name = "uc_configuration";
      this.Size = new System.Drawing.Size(800, 258);
      this.pnl_container.ResumeLayout(false);
      this.pnl_container.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private Label lbl_location;
    private Label lbl_y;
    private System.Windows.Forms.TextBox txt_x;
    private Label lbl_x;
    private System.Windows.Forms.TextBox txt_y;
    private Button btn_incr_x;
    private Button btn_decr_x;
    private Button btn_decr_y;
    private Button btn_incr_y;
    private System.Windows.Forms.Label lbl_message;
    private Button btn_save_config;
    private Button btn_calibration;
    private Button btn_exit;
    private Button btn_reset_location;
    private uc_check chk_memory;
    private uc_check chk_border;
    private uc_check chk_development;
    private uc_check chk_mouse;
    private Button btn_reset_resolution;
    private Button btn_decr_width;
    private Button btn_incr_width;
    private Button btn_decr_height;
    private Button btn_incr_height;
    private TextBox txt_height;
    private Label lbl_height;
    private TextBox txt_width;
    private Label lbl_width;
    private Label lbl_resolution;


  }
}
