namespace LCDInTouch.Controls
{
  partial class uc_check 
  {
    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.pnl_container = new System.Windows.Forms.Panel();
      this.pnl_color = new System.Windows.Forms.Panel();
      this.pnl_switch = new System.Windows.Forms.Panel();
      this.lbl_description = new System.Windows.Forms.Label();
      this.pnl_container.SuspendLayout();
      this.pnl_color.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_container
      // 
      this.pnl_container.Controls.Add(this.pnl_color);
      this.pnl_container.Controls.Add(this.lbl_description);
      this.pnl_container.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pnl_container.Location = new System.Drawing.Point(0, 0);
      this.pnl_container.Name = "pnl_container";
      this.pnl_container.Size = new System.Drawing.Size(217, 37);
      this.pnl_container.TabIndex = 0;
      this.pnl_container.Click += new System.EventHandler(this.uc_button_Click);
      // 
      // pnl_color
      // 
      this.pnl_color.BackColor = System.Drawing.Color.Red;
      this.pnl_color.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.pnl_color.Controls.Add(this.pnl_switch);
      this.pnl_color.Dock = System.Windows.Forms.DockStyle.Right;
      this.pnl_color.Location = new System.Drawing.Point(153, 0);
      this.pnl_color.Name = "pnl_color";
      this.pnl_color.Size = new System.Drawing.Size(64, 37);
      this.pnl_color.TabIndex = 1;
      this.pnl_color.Click += new System.EventHandler(this.pnl_color_Click);
      // 
      // pnl_switch
      // 
      this.pnl_switch.BackColor = System.Drawing.Color.LightGray;
      this.pnl_switch.Dock = System.Windows.Forms.DockStyle.Right;
      this.pnl_switch.Location = new System.Drawing.Point(30, 0);
      this.pnl_switch.Name = "pnl_switch";
      this.pnl_switch.Size = new System.Drawing.Size(30, 33);
      this.pnl_switch.TabIndex = 0;
      this.pnl_switch.Click += new System.EventHandler(this.pnl_color_Click);
      // 
      // lbl_description
      // 
      this.lbl_description.BackColor = System.Drawing.Color.Transparent;
      this.lbl_description.Dock = System.Windows.Forms.DockStyle.Left;
      this.lbl_description.Font = new System.Drawing.Font("MyriadProRegular", 15.75F);
      this.lbl_description.Location = new System.Drawing.Point(0, 0);
      this.lbl_description.Name = "lbl_description";
      this.lbl_description.Size = new System.Drawing.Size(147, 37);
      this.lbl_description.TabIndex = 0;
      this.lbl_description.Text = "xDescription";
      this.lbl_description.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // uc_check
      // 
      this.BackColor = System.Drawing.Color.Transparent;
      this.Controls.Add(this.pnl_container);
      this.Margin = new System.Windows.Forms.Padding(8);
      this.Name = "uc_check";
      this.Size = new System.Drawing.Size(217, 37);
      this.Click += new System.EventHandler(this.uc_button_Click);
      this.pnl_container.ResumeLayout(false);
      this.pnl_color.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    public System.Windows.Forms.Panel pnl_container;
    private System.Windows.Forms.Label lbl_description;
    private System.Windows.Forms.Panel pnl_color;
    private System.Windows.Forms.Panel pnl_switch;
  }
}
