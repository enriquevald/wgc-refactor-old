//------------------------------------------------------------------------------
// Copyright © 2014 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_button.cs
// 
//   DESCRIPTION: Button Control. 
// 
//        AUTHOR: Javi Barea
// 
// CREATION DATE: 16-MAY-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 16-MAY-2014 JBP    First release.
// 13-MAY-2016 JBP    PBI 12541:LCDTouch - Redimensión
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

using LCDInTouch.Properties;

namespace LCDInTouch.Controls
{
  public partial class uc_button : UserControl
  {
    #region Replaced Events

    [Description("Click button event. * This replace Click event *"), Category("Action")]
    public virtual event EventHandler Click_Button;

    [Description("MouseDown button event. * This replace MouseDown event *"), Category("Mouse")]
    public virtual event MouseEventHandler MouseDown_Button;

    [Description("MouseUp button event. * This replace MouseUp event *"), Category("Mouse")]
    public virtual event MouseEventHandler MouseUp_Button;

    #endregion

    #region Members

    private uc_body m_current_body;
    private TYPE_BUTTON_SIZE_STYLE m_style;
    private TYPE_IMAGE_STYLE m_style_image;
    private Image m_background_image;
    private Image m_background_image_mousedown;
    private Image m_background_image_disabled;

    private Color m_background_color;
    private Color m_background_color_disabled;
    private Color m_background_color_mousedown;

    private Boolean m_is_selectable;
    private Boolean m_is_label;

    private Point m_initial_position = new Point(Constants.DEFAULT_INITIAL_POSITION_X_AND_Y, Constants.DEFAULT_INITIAL_POSITION_X_AND_Y);
    private Size m_initial_size = new Size(Constants.DEFAULT_INITIAL_SIZE_W_AND_H, Constants.DEFAULT_INITIAL_SIZE_W_AND_H);
    private Size[] m_initial_image_size;
    private Size m_initial_icon_size;

    #endregion

    #region Properties
    
    public Single WidthRatio
    {
      get
      {
        if (this.CurrentBody != null)
        {
          return this.CurrentBody.WidthRatio;
        }

        return 1;
      }
    }

    public Single HeightRatio
    {
      get
      {
        if (this.CurrentBody != null)
        {
          return this.CurrentBody.HeightRatio;
        }

        return 1;
      }
    }

    #region Style Properties

    [Description("Predefined style of button"), Category(".Style Properties")]
    public TYPE_BUTTON_SIZE_STYLE Control_Style
    {
      get { return this.m_style; }
      set
      {
        if (this.m_style != value)
        {
          this.SetSize(value, true);
          this.m_style = value;
        }
      }
    }

    [Description("Control position"), Category(".Style Properties")]
    public Point Control_Position
    {
      get { return this.Location; }
      set 
      {
        this.Location = value;

        if (this.m_initial_position.X == Constants.DEFAULT_INITIAL_POSITION_X_AND_Y
          && this.m_initial_position.Y == Constants.DEFAULT_INITIAL_POSITION_X_AND_Y)
        {
          this.m_initial_position = new Point(this.Location.X, this.Location.Y);
        }
      }
    }

    [Description("Image style"), Category(".Style Properties")]
    public TYPE_IMAGE_STYLE Control_StyleImage
    {
      get { return this.m_style_image; }
      set
      {
        if (this.m_style_image != value)
        {
          this.SetImageAlign(value);
          this.m_style_image = value;
        }
      }
    }

    [Description("Define control select type"), Category(".Style Properties")]
    public Boolean IsSelectable
    {
      get { return this.m_is_selectable; }
      set { this.m_is_selectable = value; }
    }

    [Description("Define control type"), Category(".Style Properties")]
    public Boolean IsLabel
    {
      get { return this.m_is_label; }
      set { this.m_is_label = value; }
    }

    [Description("Enable/Disable control"), Category(".Style Properties")]
    public Boolean IsEnabled
    {
      get { return this.pnl_container.Enabled; }
      set 
      {
        if ( value == this.pnl_container.Enabled)
        {
          return;
        }

        if (value)
        {
          if ( this.IsSelectable
            && this.CurrentBody.MainForm.SelectedButton == this.ButtonCode)
          {
            return;
          }

          if (this.Background_Image != null)
          {
            this.Icon = this.Background_Image;
          }

          this.pnl_container.BackColor = this.Background_Color;
        }
        else
        {
          if (this.Background_Image_Disabled != null)
          {
            this.Icon = this.Background_Image_Disabled;
          }

          this.pnl_container.BackColor = this.Background_Color_Disabled;
        }

        this.Enabled = value;
        this.pnl_container.Enabled = value;
      }
    }

    [Description("Icon of button"), Category(".Style Properties")]
    public Image Icon
    {
      get { return this.img.Image; }
      set
      {
        this.img.Image = value;
        if (value != null
          && this.InitialIconSize.Width == 0
          && this.InitialIconSize.Height == 0)
        {
          this.InitialIconSize = new Size(value.Width, value.Height);
        }
      }
    }

    [Description("Image of button"), Category(".Style Properties")]
    public Image Background_Image
    {
      get { return this.m_background_image; }
      set
      {
        this.img.Image = value;
        this.m_background_image = value;

        if ( value != null
          && this.InitialImageSizeNotSet(Constants.RoundButton.ButtonEvent.NORMAL))
        {
          this.InitialImageSize[Constants.RoundButton.ButtonEvent.NORMAL] = new Size(value.Size.Width, value.Size.Height);
        }
      }
    }

    [Description("Image of button mouse down"), Category(".Style Properties")]
    public Image Background_Image_MouseDown
    {
      get { return this.m_background_image_mousedown; }
      set 
      { 
        this.m_background_image_mousedown = value;

        if ( value != null
          && this.InitialImageSizeNotSet(Constants.RoundButton.ButtonEvent.MOUSEDOWN))
        {
          this.InitialImageSize[Constants.RoundButton.ButtonEvent.MOUSEDOWN] = new Size(value.Size.Width, value.Size.Height);
        }
      }
    }

    [Description("Image of button disabled"), Category(".Style Properties")]
    public Image Background_Image_Disabled
    {
      get { return this.m_background_image_disabled; }
      set
      {
        this.m_background_image_disabled = value;

        if ( value != null
          && this.InitialImageSizeNotSet(Constants.RoundButton.ButtonEvent.DISABLED))
        {
          this.InitialImageSize[Constants.RoundButton.ButtonEvent.DISABLED] = new Size(value.Size.Width, value.Size.Height);
        }
      }
    }

    [Description("Background color"), Category(".Style Properties")]
    public Color Background_Color
    {
      get { return this.m_background_color; }
      set
      {
        this.pnl_container.BackColor = value;
        this.m_background_color = value;
      }
    }

    [Description("Background color disabled"), Category(".Style Properties")]
    public Color Background_Color_Disabled
    {
      get { return this.m_background_color_disabled; }
    }

    [Description("Background color MouseDown"), Category(".Style Properties")]
    public Color Background_Color_MouseDown
    {
      get { return this.m_background_color_mousedown; }
      set { this.m_background_color_mousedown = value; }
    }

    [Description("Total height button"), Category(".Style Properties")]
    public Int32 Total_Height
    {
      get { return this.Height; }
      set { this.Height = value; }
    }

    [Description("Total width button"), Category(".Style Properties")]
    public Int32 Total_Width
    {
      get { return this.Width; }
      set
      {
        this.Width = value;
        this.lbl.Width = (this.Total_Width - this.Image_Width);
      }
    }

    [Description("Image location in button"), Category(".Style Properties")]
    public DockStyle Image_Location
    {
      get { return this.pnl_img.Dock; }
      set { this.pnl_img.Dock = value; }
    }

    [Description("Image align panel image button"), Category(".Style Properties")]
    public DockStyle Image_Align
    {
      get { return this.img.Dock; }
      set { this.img.Dock = value; }
    }

    [Description("Image layout panel image button"), Category(".Style Properties")]
    public PictureBoxSizeMode Image_SizeMode
    {
      get { return this.img.SizeMode; }
      set { this.img.SizeMode = value; }
    }

    [Description("Text location"), Category(".Style Properties")]
    public DockStyle Text_Location
    {
      get { return this.lbl.Dock; }
      set { this.lbl.Dock = value; }
    }

    [Description("Total image width button"), Category(".Style Properties")]
    public Int32 Image_Width
    {
      get { return this.pnl_img.Width; }
      set
      {
        this.pnl_img.Width = value;
        this.lbl.Width = (this.Total_Width - this.Image_Width);
      }
    }
    
    [Description("Text of button"), Category(".Style Properties")]
    public String Text_Literal
    {
      get { return this.lbl.Text; }
      set { this.lbl.Text = value; }
    }

    [Description("Total image width button"), Category(".Style Properties")]
    public Int32 Text_Width
    {
      get { return this.lbl.Width; }
      set
      {
        this.lbl.Width = value;
        this.lbl.Visible = (value > 0);
      }
    }

    [Description("Font button"), Category(".Style Properties")]
    public Font Text_Font
    {
      get { return this.lbl.Font; }
      set { this.lbl.Font = value; }
    }

    [Description("Text align inside label"), Category(".Style Properties")]
    public ContentAlignment Text_Align
    {
      get { return this.lbl.TextAlign ; }
      set { this.lbl.TextAlign = value; }
    }

    [Description("Label Text padding"), Category(".Style Properties")]
    public Padding Text_Padding
    {
      get { return this.lbl.Padding; }
      set { this.lbl.Padding = value; }
    }

    [Description("Text color"), Category(".Style Properties")]
    public Color Text_Color
    {
      get { return this.lbl.ForeColor; }
      set { this.lbl.ForeColor = value; }
    }

    [Description("Border color"), Category(".Style Properties")]
    public Color Border_Color
    {
      get { return this.lbl.BorderColor; }
      set
      {
        this.lbl.BorderColor = value;
        this.Invalidate();
      }
    }

    [Description("Border height"), Category(".Style Properties")]
    public Int32 Border_Width
    {
      get { return this.lbl.BorderWidth; }
      set { this.lbl.BorderWidth = value; }
    }

    #endregion
    
    [Browsable(false)]
    public uc_body CurrentBody
    {
      get
      {
        if (this.m_current_body == null)
        {
          this.m_current_body = Functions.GetParentBody(this);
        }

        return this.m_current_body;
      }
    }

    [Browsable(false)]
    public String ButtonCode
    {
      get { return this.Name + "|" + this.CurrentBody.Name; }
    }

    [Browsable(false)]
    public String Text_Tag
    {
      get { return this.lbl.Tag; }
      set { this.lbl.Tag = value; }
    }


    [Browsable(false)]
    public Size InitialSize
    {
      get 
      {
        if ( this.m_initial_size.Width == Constants.DEFAULT_INITIAL_SIZE_W_AND_H
          && this.m_initial_size.Height == Constants.DEFAULT_INITIAL_SIZE_W_AND_H )
        {
          this.m_initial_size = this.Size;
        }

        return this.m_initial_size; 
      }      
    }

    [Browsable(false)]
    public Size[] InitialImageSize
    {
      get { return this.m_initial_image_size; }
      set { this.m_initial_image_size = value; }
    }

    [Browsable(false)]
    public Size InitialIconSize
    {
      get { return this.m_initial_icon_size; }
      set { this.m_initial_icon_size = value; }
    }

    [Browsable(false)]
    public Point InitialPosition
    {
      get
      {
        //if (this.m_initial_position.X == Constants.DEFAULT_INITIAL_POSITION_X_AND_Y
        //  && this.m_initial_position.Y == Constants.DEFAULT_INITIAL_POSITION_X_AND_Y)
        //{
        //  this.m_initial_position = new Point(this.Location.X, this.Location.Y);
        //}
        return this.m_initial_position;
      }
    }

    #endregion

    #region Public Functions

    public uc_button()
    {
      this.InitializeComponent();
      this.InitialImageSize = new Size[3];

      this.m_background_color_disabled = Color.FromArgb(145, 145, 145);

      this.Click_Button     += new EventHandler(this.Click_UcButtonEvent);
      this.MouseUp_Button   += new MouseEventHandler(this.MouseUp_UcButtonEvent);
      this.MouseDown_Button += new MouseEventHandler(this.MouseDown_UcButtonEvent);
    }

    public void SetInitialPosition(Point Location)
    {
      if (this.InvokeRequired)
      {
        this.BeginInvoke(new MethodInvoker(delegate
        {
          this.SetInitialPosition(Location);
        }));
      }
      else
      {
        this.m_initial_position = Location;
      }
    }

    #endregion

    #region Private

    private Boolean InitialImageSizeNotSet(Int32 Type)
    {
      return ( this.InitialImageSize[Type] != null
            && this.InitialImageSize[Type].Height == 0
            && this.InitialImageSize[Type].Width == 0);
    }

    private void SetSize(TYPE_BUTTON_SIZE_STYLE SizeStyle, Boolean AutoSetImageAlign)
    {
      try
      {
        Font _font = this.Text_Font;
        switch (SizeStyle)
        { 
          //case TYPE_BUTTON_SIZE_STYLE.X_Small:
          //  this.Total_Height = Constants.BUTTON_HEIGHT_X_SMALL;
          //  this.Total_Width = Constants.BUTTON_WIDTH_X_SMALL;
          //  this.Image_Width = Constants.BUTTON_IMAGE_WIDTH_X_SMALL;
          //  this.Text_Color = Color.White;
          //  this.Text_Font = new Font("MyriadProRegular", 22F);
          //  break;

          case TYPE_BUTTON_SIZE_STYLE.Small:
            this.Total_Height     = Constants.BUTTON_HEIGHT_SMALL;
            this.Total_Width      = Constants.BUTTON_WIDTH_SMALL;
            this.Image_Width      = Constants.BUTTON_IMAGE_WIDTH_SMALL;
            this.Image_Align      = DockStyle.Fill;
            this.Text_Color       = Color.White;

            _font = new Font("MyriadProRegular", 22);
            break;

          case TYPE_BUTTON_SIZE_STYLE.Medium:
            this.Total_Height     = Constants.BUTTON_HEIGHT_MEDIUM;
            this.Total_Width      = Constants.BUTTON_WIDTH_MEDIUM;
            this.Image_Width      = Constants.BUTTON_IMAGE_WIDTH_MEDIUM;
            this.Text_Color       = Color.White;

            _font = new Font("MyriadProRegular", 20);
            break;

          case TYPE_BUTTON_SIZE_STYLE.Big:
            this.Total_Height     = Constants.BUTTON_HEIGHT_BIG;
            this.Total_Width      = Constants.BUTTON_WIDTH_BIG;
            this.Image_Width      = Constants.BUTTON_IMAGE_WIDTH_BIG;
            this.Text_Color       = Color.White;

            _font = new Font("MyriadProRegular", 22);
            break;

          case TYPE_BUTTON_SIZE_STYLE.Pin:
            this.Total_Height     = Constants.BUTTON_HEIGHT_PIN;
            this.Total_Width      = Constants.BUTTON_WIDTH_PIN;
            this.Image_Width      = Constants.BUTTON_IMAGE_WIDTH_NO_IMG;
            this.Text_Color       = Color.White;
            this.Background_Color = Color.FromArgb(255, 128, 0);

            _font = new Font("MyriadProRegular", 22);
            break;

          case TYPE_BUTTON_SIZE_STYLE.Keyboard:
            this.Total_Height     = Constants.BUTTON_HEIGHT_KEYBOARD;
            this.Total_Width      = Constants.BUTTON_WIDTH_KEYBOARD;            
            this.Image_Width      = Constants.BUTTON_IMAGE_WIDTH_NO_IMG;
            this.Border_Width     = 1;
            this.Border_Color     = Color.Black;

            _font = new Font("MyriadProRegular", 18, FontStyle.Bold);
            break;

          case TYPE_BUTTON_SIZE_STYLE.Opr_UpDown:
            this.Total_Height     = Constants.BUTTON_HEIGHT_OPERATOR_UP_DOWN;
            this.Total_Width      = Constants.BUTTON_WIDTH_OPERATOR_UP_DOWN;
            this.Image_Width      = Constants.BUTTON_WIDTH_OPERATOR_UP_DOWN;
            break;

          case TYPE_BUTTON_SIZE_STYLE.Opr_Calibrate:
            this.Total_Height     = Constants.BUTTON_HEIGHT_OPERATOR_CALIBRATION;
            this.Total_Width      = Constants.BUTTON_WIDTH_OPERATOR_CALIBRATION;
            this.Image_Width      = Constants.BUTTON_WIDTH_OPERATOR_CALIBRATION;
            break;

          case TYPE_BUTTON_SIZE_STYLE.TransferAmount:
            this.Total_Height     = Constants.BUTTON_HEIGHT_TRANSFER_AMOUNT;
            this.Total_Width      = Constants.BUTTON_WIDTH_TRANSFER_AMOUNT;
            this.Image_Width      = Constants.BUTTON_IMAGE_WIDTH_NO_IMG;            
            this.Text_Color       = Color.White;

            _font = new Font("MyriadProRegular", 22);
            break;

          case TYPE_BUTTON_SIZE_STYLE.Custom:
            AutoSetImageAlign = false;
            break;
        }

        // Resize button font 
        this.Text_Font = (this.CurrentBody != null) ? this.CurrentBody.ScaleFont(_font) : _font;

        if (AutoSetImageAlign)
        {
          this.SetImageAlign(SizeStyle);
        }

        //this.InitialSize = new Size(this.Width, this.Height);
        //this.InitialPosition = new Point(this.Location.X, this.Location.Y);
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    }
    //////////// Examples ///////////////////////////////
    /// <summary>
    ///       FIRST = Image location y button.
    ///       SECON = Image aligment in image panel. 
    ///       
    ///       * The image have auto size
    /// 
    ///     
    ///       Example --> LEFT_MID
    ///      __________________________________________
    ///     |  ___LEFT__   __________RIGHT___________  |
    ///     | |  __M__  | |                          | |
    ///     | | |     | | |                          | |
    ///     | |L|IMAGE|R| |          LABEL           | |
    ///     | | |_____| | |                          | |
    ///     | |_________| |__________________________| |
    ///     |__________________________________________|
    ///    
    /// 
    ///       Example --> LEFT_LEFT
    ///      __________________________________________
    ///     |  ___LEFT__   __________RIGHT___________  |
    ///     | | __M__   | |                          | |
    ///     | ||     |  | |                          | |
    ///     | ||IMAGE| R| |          LABEL           | |
    ///     | ||_____|  | |                          | |
    ///     | |_________| |__________________________| |
    ///     |__________________________________________|
    ///     
    /// 
    ///       Example --> CENTER_LEFT (LABEL NOT VISIBLE)
    ///      __________________________________________
    ///     |  _______________CENTER_________________  |
    ///     | | _____          MID                   | |
    ///     | ||     |                               | |
    ///     | ||IMAGE|                          RIGHT| |
    ///     | ||_____|                               | |
    ///     | |______________________________________| |
    ///     |__________________________________________|
    /// 
    /// </summary>

    private void SetImageAlign(TYPE_BUTTON_SIZE_STYLE SizeStyle)
    {
      switch (SizeStyle)
      {
        case TYPE_BUTTON_SIZE_STYLE.Medium:
          this.Control_StyleImage = TYPE_IMAGE_STYLE.RIGHT_RIGHT;
          break;

        case TYPE_BUTTON_SIZE_STYLE.Small:
        case TYPE_BUTTON_SIZE_STYLE.Big:
          this.Control_StyleImage = TYPE_IMAGE_STYLE.LEFT_LEFT;
          break;

        case TYPE_BUTTON_SIZE_STYLE.Pin:
        case TYPE_BUTTON_SIZE_STYLE.Keyboard:
          this.Control_StyleImage = TYPE_IMAGE_STYLE.NONE;
          break;

        case TYPE_BUTTON_SIZE_STYLE.Opr_UpDown:
        case TYPE_BUTTON_SIZE_STYLE.Opr_Calibrate:
          this.Control_StyleImage = TYPE_IMAGE_STYLE.CENTER_LEFT;
          break;
        case TYPE_BUTTON_SIZE_STYLE.Custom:
        default:

          break;
      }
    }

    private void SetImageAlign(TYPE_IMAGE_STYLE ImageStyle)
    {
      try
      {
        this.pnl_img.Visible = true;
        this.lbl.Visible = true;

        this.pnl_img.BringToFront();

        switch (ImageStyle)
        {
          case TYPE_IMAGE_STYLE.LEFT_LEFT:
            this.Image_Location = DockStyle.Left;
            //this.Image_Align = DockStyle.Left;
            this.Image_Align = DockStyle.Fill;
            this.Text_Location = DockStyle.Right;
            break;
          case TYPE_IMAGE_STYLE.LEFT_MID:
            this.Image_Location = DockStyle.Left;
            this.Image_Align = DockStyle.None;
            this.Text_Location = DockStyle.Right;
            break;
          case TYPE_IMAGE_STYLE.LEFT_RIGHT:
            this.Image_Location = DockStyle.Left;
            //this.Image_Align = DockStyle.Right;
            this.Image_Align = DockStyle.Fill;
            this.Text_Location = DockStyle.Right;
            break;
          case TYPE_IMAGE_STYLE.CENTER_LEFT:
            this.Image_Location = DockStyle.Fill;
            this.Image_Align = DockStyle.Top;
            this.Text_Location = DockStyle.None;
            this.lbl.Visible = false;

            this.lbl.BringToFront();
            break;
          case TYPE_IMAGE_STYLE.CENTER_MID:
            this.Image_Location = DockStyle.Fill;
            this.Image_Align = DockStyle.Fill;
            this.Text_Location = DockStyle.None;
            this.lbl.Visible = false;

            this.lbl.BringToFront();
            break;
          case TYPE_IMAGE_STYLE.CENTER_RIGHT:
            this.Image_Location = DockStyle.Fill;
            //this.Image_Align = DockStyle.Right;
            this.Image_Align = DockStyle.Fill;
            this.Text_Location = DockStyle.None;

            this.lbl.Visible = false;

            this.lbl.BringToFront();
            break;

          case TYPE_IMAGE_STYLE.RIGHT_LEFT:
            this.Image_Location = DockStyle.Right;
//            this.Image_Align = DockStyle.Left;
            this.Image_Align = DockStyle.Fill;
            this.Text_Location = DockStyle.Left;
            break;
          case TYPE_IMAGE_STYLE.RIGHT_MID:
            this.Image_Location = DockStyle.Right;
            this.Image_Align = DockStyle.None;
            this.Text_Location = DockStyle.Left;
            break;
          case TYPE_IMAGE_STYLE.RIGHT_RIGHT:
            this.Image_Location = DockStyle.Fill;
            //this.Image_Align = DockStyle.Right;
            this.Image_Align = DockStyle.Fill;
            this.Text_Location = DockStyle.Left;
            break;

          case TYPE_IMAGE_STYLE.NONE:
          default:
            this.pnl_img.Visible = false;
            
            this.Image_Location = DockStyle.None;
            this.Text_Location = DockStyle.Fill;

            this.lbl.BringToFront();
            break;
        }
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    }

    internal void SetInitialBackgroundColor()
    {
      try
      {
        if ( this.IsSelectable 
          && this.CurrentBody.MainForm.SelectedButton == this.ButtonCode)
        {
          return;
        }

        if (this.IsEnabled)
        {
          if (this.Background_Image != null)
          {
            this.Icon = this.Background_Image;
          }
          this.pnl_container.BackColor = this.Background_Color;
        }
        else
        {
          if (this.Background_Image_Disabled != null)
          {
            this.Icon = this.Background_Image_Disabled;
          }

          this.pnl_container.BackColor = this.Background_Color_Disabled;
        }
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    }

    internal void SetPressedBackgroundColor()
    {
      try
      {
        if (this.IsEnabled)
        {
          if (this.IsSelectable)
          {
            this.CurrentBody.MainForm.SelectedButton = this.ButtonCode;
          }

          if (this.Background_Image_MouseDown != null)
          {
            this.Icon = this.Background_Image_MouseDown;
          }

          this.pnl_container.BackColor = this.Background_Color_MouseDown;
        }
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    }

    private void UpdateLastActivity()
    {
      try
      {
        if (this.CurrentBody == null)
        {
          return;
        }

        if (this.CurrentBody.MainForm != null)
        {
          this.CurrentBody.MainForm.UpdateLastActivity();
          this.CurrentBody.MainForm.LCD.SendProtocolHandler(TYPE_REQUEST.LCD_ACTIVITY, String.Empty);
        }
        else if (this.CurrentBody.KeyboardForm != null)
        {
          this.CurrentBody.KeyboardForm.UpdateLastActivity();
          this.CurrentBody.KeyboardForm.LCD.SendProtocolHandler(TYPE_REQUEST.LCD_ACTIVITY, String.Empty);
        }
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    }

    #endregion

    #region Events

    #region Public Events

    public void Click_UcButtonEvent(object sender, EventArgs e)
    {
    }

    public void MouseUp_UcButtonEvent(object sender, EventArgs e)
    {
    }

    public void MouseDown_UcButtonEvent(object sender, EventArgs e)
    {
    }

    public void Click_Button_Base(object sender, EventArgs e)
    {
      this.UpdateLastActivity();
    }

    public void MouseDown_Button_Base(object sender, MouseEventArgs e)
    {
      try
      {
        this.SetPressedBackgroundColor();
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    }

    public void MouseUp_Button_Base(object sender, MouseEventArgs e)
    {
      try
      {
        this.SetInitialBackgroundColor();             
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    }

    #endregion

    #region Internal Events

    internal void uc_button_Click(object sender, EventArgs e)
    {
      if (this.IsLabel)
      {
        return;
      }

      this.Click_Button(sender, e);
      this.Click_Button_Base(sender, e);      
    }

    internal void uc_button_MouseDown(object sender, MouseEventArgs e)
    {
      if (this.IsLabel)
      {
        return;
      }

      this.MouseDown_Button(sender, e);
      this.MouseDown_Button_Base(sender, e);
    }

    internal void uc_button_MouseUp(object sender, MouseEventArgs e)
    {
      if (this.IsLabel)
      {
        return;
      }

      this.MouseUp_Button(sender, e);
      this.MouseUp_Button_Base(sender, e);
    }    

    #endregion

    #endregion
  }
}
