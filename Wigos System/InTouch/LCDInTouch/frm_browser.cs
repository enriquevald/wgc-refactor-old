//------------------------------------------------------------------------------
// Copyright � 2015  Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_browser.cs
// 
//   DESCRIPTION: Form with web_browser (gamegateway). 
// 
//        AUTHOR: Alberto Di�s
// 
// CREATION DATE: 01-NOV-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 01-NOV-2016 ADI    First release.
// 02-MAR-2016 FJC    PBI 9105:BonoPlay: LCD: Cambios varios
// 13-MAY-2016 JBP    PBI 12541:LCDTouch - Redimensi�n
// 06-JUN-2016 JBP    Bug 13952:PimPamGo: Al realizar una puesta sin cr�dito suficiente y quitar la tarjeta, permanece la sesi�n abierta
// 07-JUN-2016 FJC    Bug 14238:PimPamGo: PimPamGo: no se puede acceder a la web habiendo jugado previamente.
// 15-FEB-2017 SCJA & FJC Product Backlog Item 23849:Mensajer�a. Fase II
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.IO;
using System.Net;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Timers;

namespace LCDInTouch
{
  [ComVisibleAttribute(true)]
  public partial class frm_browser : Form
  {

    #region " Properties "

    public Browser Browser { get; set; }
    public String URLNavigate { get; set; }
    public Boolean Ready { get; set; }

    #endregion

    #region " Public Methods "

    public frm_browser()
    {
      // Browser initialization
      this.InitBrowser();
    }

    public frm_browser(Browser Browser)
    {
      this.Browser = Browser;
      this.InitializeComponent();
      this.LoadBrowser();

      if (Debugger.IsAttached || !String.IsNullOrEmpty(Environment.GetEnvironmentVariable("DEVELOPMENT")))
      {
        this.Text = this.URLNavigate;
        this.FormBorderStyle = FormBorderStyle.FixedToolWindow;
      }

      this.InitBrowser();
    }

    /// <summary>
    /// Browser  unload
    /// </summary>
    public void UnloadBrowser()
    {
      if (this.InvokeRequired)
      {
        this.BeginInvoke(new MethodInvoker(delegate
        {
          this.UnloadBrowser();
        }));

        return;
      }

      this.Opacity = Constants.OPACITY_HIDE;
      this.NavigateUrl(Constants.BLANK_PAGE);      
    } // UnloadBrowser 

    /// <summary>
    /// Navigate to Url
    /// </summary>
    /// <param name="Url"></param>
    /// <returns></returns>
    public Boolean NavigateUrl(String Url)
    {
      try
      {
        if (this.web_browser.InvokeRequired)
        {
          this.web_browser.BeginInvoke(new MethodInvoker(delegate
          {
            this.NavigateUrl(Url);
          }));
        }
        else
        {
          this.URLNavigate = Url;

          // Check Url
          if (String.IsNullOrEmpty(this.URLNavigate))
          {
            return false;
          }

          //Navigate
          this.web_browser.Navigate(this.URLNavigate);

          // Show url in browser form title
          this.Text = (Debugger.IsAttached) ? this.URLNavigate : String.Empty;
        }
        
        return true;
      }
      catch (COMException)
      {
        ;
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }

      return false;
    } // NavigateUrl

    #endregion

    #region Privates methods

    /// <summary>
    /// Browser load
    /// </summary>
    private void LoadBrowser()
    {
      this.web_browser.ScrollBarsEnabled = false;
      this.WindowState = this.Browser.MainForm.WindowState;
      this.Location = this.Browser.MainForm.Location;
      this.Opacity = Constants.OPACITY_HIDE;
    } // LoadBrowser

    /// <summary>
    /// Browser initialization
    /// </summary>
    private void InitBrowser()
    {
      //Initialize Params
      this.Ready = false;

      this.web_browser.ScriptErrorsSuppressed = true;
      this.web_browser.AllowWebBrowserDrop = false;
      this.web_browser.IsWebBrowserContextMenuEnabled = false;
      this.web_browser.WebBrowserShortcutsEnabled = false;
      this.web_browser.ObjectForScripting = this;
      this.web_browser.DocumentCompleted -= new WebBrowserDocumentCompletedEventHandler(this.WBDocumentCompleted);
      this.web_browser.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(this.WBDocumentCompleted);
    } // InitBrowser

    #endregion

    #region Events

    private void link_close_browser_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
    {
      this.Browser.StopBrowser();
    } // link_close_browser_LinkClicked

    private void frm_browser_FormClosing(object sender, FormClosingEventArgs e)
    {
      this.Browser.BrowserStatus = STATUS_BROWSER.CLOSING;
      this.Dispose();
    } // frm_browser_FormClosing

    /// <summary>
    /// Document complete event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void WBDocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
    {
      try
      {
        // Check main form
        if (this.Browser.MainForm == null || this.Browser.MainForm.IsDisposed)
        {
          return;
        }

        // Check url
        if (String.IsNullOrEmpty(this.URLNavigate))
        {
          return;
        }

        // Document is ready
        this.Ready = true;

        // Initialize DocumentCompleted event
        this.web_browser.Document.Body.KeyPress -= new HtmlElementEventHandler(Body_KeyPress);
        this.web_browser.Document.Body.KeyPress += new HtmlElementEventHandler(Body_KeyPress);
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // WBDocumentCompleted

    #endregion

    #region " Browser interaction "

    /// <summary>
    /// Host interaction callback
    /// </summary>
    /// <param name="Data"></param>
    internal void HostInteractionCallback(String Data)
    {
      if (this.InvokeRequired)
      {
        this.BeginInvoke(new MethodInvoker(delegate
        {
          this.HostInteractionCallback(Data);
        }));

        return;
      }

      try
      {
        object[] _objarray = Data.Split('|');
        web_browser.Document.InvokeScript("hostInteractionCallBack", _objarray);
        return;
      }
      catch (Exception ex)
      {
        Log.Add(ex);
        this.Browser.CloseBrowser(false);
      }
    } // HostInteractionCallback

    /// <summary>
    /// Interaction with host
    /// </summary>
    /// <param name="Action"></param>
    /// <returns></returns>
    public bool InteractWithHost(string Action)
    {
      try
      {
        if (this.Browser.MainForm == null)
        {
          return false;
        }

        this.Browser.SendProtocolHandlerInteraction(Action);

        return true;
      }
      catch(Exception _ex)
      {
        Log.Add(_ex);

        return false;
      }
    } // InteractWithHost
    
    private void Body_KeyPress(object sender, HtmlElementEventArgs e)
    {
      this.Browser.MainForm.ResetActivities();
      this.Browser.MainForm.KeyPressManager((char)e.KeyPressedCode);
    } // Body_KeyPress

    private void frm_browser_KeyPress(object sender, KeyPressEventArgs e)
    {
      this.Browser.MainForm.ResetActivities();
      this.Browser.MainForm.KeyPressManager(e.KeyChar);
    } // frm_browser_KeyPress

    /// <summary>
    /// To catch mouse click event
    /// </summary>
    /// <param name="m"></param>
    protected override void WndProc(ref Message m)
    {
      if (m.Msg == Constants.WM_MOUSECLICK)
      {
        this.Browser.MainForm.ResetActivities();

        if (m.WParam.ToInt32() == Constants.WM_MOUSE_LEFT_CLICK)
        {
          // Only for hide video
          if (!this.Browser.MainForm.IsVideoMode)
          {
            return;
          }

          this.Browser.MainForm.Video.Stop();
        }
      }

      base.WndProc(ref m);
    } // WndProc

    #endregion 

  } // frm_browser
}