namespace LCDInTouch
{
  partial class frm_main
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_main));
      this.pnl_video = new System.Windows.Forms.Panel();
      this.axVLCPlugin21 = new AxAXVLC.AxVLCPlugin2();
      this.pnl_marquee = new System.Windows.Forms.Panel();
      this.lbl_msg = new System.Windows.Forms.Label();
      this.pnl_content.SuspendLayout();
      this.pnl_menu_mid.SuspendLayout();
      this.pnl_logo_bot.SuspendLayout();
      this.pnl_loading.SuspendLayout();
      this.pnl_jackpot.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pb_loading)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.pb_jackpot)).BeginInit();
      this.pnl_menu.SuspendLayout();
      this.pnl_video.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.axVLCPlugin21)).BeginInit();
      this.pnl_marquee.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_logo_bot
      // 
      this.pnl_logo_bot.Click += new System.EventHandler(this.pnl_logo_bot_Click);
      // 
      // pnl_jackpot
      // 
      this.pnl_jackpot.Dock = System.Windows.Forms.DockStyle.Fill;
      // 
      // pb_loading
      // 
      this.pb_loading.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
      this.pb_loading.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pb_loading.Location = new System.Drawing.Point(0, 0);
      this.pb_loading.Size = new System.Drawing.Size(800, 256);
      this.pb_loading.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
      // 
      // pb_jackpot
      // 
      this.pb_jackpot.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
      // 
      // pnl_logo_wigos
      // 
      this.pnl_logo_wigos.Click += new System.EventHandler(this.pnl_logo_bot_Click);
      // 
      // lbl_egm_name
      // 
      this.lbl_egm_name.Click += new System.EventHandler(this.pnl_logo_bot_Click);
      // 
      // pnl_video
      // 
      this.pnl_video.BackColor = System.Drawing.Color.Transparent;
      this.pnl_video.Controls.Add(this.axVLCPlugin21);
      this.pnl_video.Location = new System.Drawing.Point(0, 20);
      this.pnl_video.Name = "pnl_video";
      this.pnl_video.Size = new System.Drawing.Size(800, 256);
      this.pnl_video.TabIndex = 2;
      // 
      // axVLCPlugin21
      // 
      this.axVLCPlugin21.Dock = System.Windows.Forms.DockStyle.Fill;
      this.axVLCPlugin21.Enabled = true;
      this.axVLCPlugin21.Location = new System.Drawing.Point(0, 0);
      this.axVLCPlugin21.Name = "axVLCPlugin21";
      this.axVLCPlugin21.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axVLCPlugin21.OcxState")));
      this.axVLCPlugin21.Size = new System.Drawing.Size(800, 256);
      this.axVLCPlugin21.TabIndex = 0;
      // 
      // pnl_marquee
      // 
      this.pnl_marquee.BackColor = System.Drawing.Color.Gold;
      this.pnl_marquee.Controls.Add(this.lbl_msg);
      this.pnl_marquee.Location = new System.Drawing.Point(0, 0);
      this.pnl_marquee.Name = "pnl_marquee";
      this.pnl_marquee.Size = new System.Drawing.Size(800, 30);
      this.pnl_marquee.TabIndex = 0;
      // 
      // lbl_msg
      // 
      this.lbl_msg.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_msg.Font = new System.Drawing.Font("MyriadProRegular", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_msg.Location = new System.Drawing.Point(0, 0);
      this.lbl_msg.Name = "lbl_msg";
      this.lbl_msg.Size = new System.Drawing.Size(800, 30);
      this.lbl_msg.TabIndex = 0;
      this.lbl_msg.Text = "0123456789012345678901234567890123456789";
      this.lbl_msg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // frm_main
      // 
      this.BindRequired = true;
      this.ClientSize = new System.Drawing.Size(800, 296);
      this.Controls.Add(this.pnl_marquee);
      this.Controls.Add(this.pnl_video);
      this.Location = new System.Drawing.Point(0, 0);
      this.Name = "frm_main";
      this.Load += new System.EventHandler(this.frm_main_Load);
      this.Controls.SetChildIndex(this.pnl_video, 0);
      this.Controls.SetChildIndex(this.pnl_marquee, 0);
      this.Controls.SetChildIndex(this.pnl_top, 0);
      this.Controls.SetChildIndex(this.pnl_content, 0);
      this.Controls.SetChildIndex(this.pnl_bot, 0);
      this.Controls.SetChildIndex(this.pnl_jackpot, 0);
      this.Controls.SetChildIndex(this.pnl_loading, 0);
      this.pnl_content.ResumeLayout(false);
      this.pnl_content.PerformLayout();
      this.pnl_menu_mid.ResumeLayout(false);
      this.pnl_logo_bot.ResumeLayout(false);
      this.pnl_loading.ResumeLayout(false);
      this.pnl_jackpot.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.pb_loading)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.pb_jackpot)).EndInit();
      this.pnl_menu.ResumeLayout(false);
      this.pnl_video.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.axVLCPlugin21)).EndInit();
      this.pnl_marquee.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    public System.Windows.Forms.Panel pnl_video;
    private System.Windows.Forms.Panel pnl_marquee;
    private System.Windows.Forms.Label lbl_msg;
    public AxAXVLC.AxVLCPlugin2 axVLCPlugin21;

  }
}