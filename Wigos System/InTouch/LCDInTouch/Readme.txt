CONFIGURACI�N -- UC_BUTTONS -- 

  - Las propiedades para configurar los uc_button deben ser siempre las del grupo ".Style Properties"
    c�mo por ejemplo Enabled, BackColor, ForeColor, etc... en su lugar deben configurarse las propiedades
    IsEnabled, Background_Color, Text_Color ya que estas ultimas puede que tengan alguna funcionalidad 
    al cambiar su valor. 
    