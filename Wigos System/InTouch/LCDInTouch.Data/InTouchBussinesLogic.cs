﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using WSI.Common;

namespace LCDInTouch.Data
{
  public class InTouchBussinesLogic
  {

    #region " Public Methods "

    /// <summary>
    /// Get InTouch Parameters from DB
    /// </summary>
    /// <param name="TerminalId"></param>
    //public Boolean GetInTouchParameters(String TerminalName, out InTouchParams Params)
    //{
    //  //WCP_LCD_MsgGetParametersReply _response;
    //  //WCP_LCD_MsgGetParameters _request;
    //  //InTouchParams _params;

    //  StringBuilder _sb;
    //  List<Int32> _list_message_id;
    //  String _provider_id;
    //  TerminalList _terminal_list;


    //  Params = new InTouchParams();

    //  //_request = (WCP_LCD_MsgGetParameters)WktRequest.MsgContent;
    //  //_response = (WCP_LCD_MsgGetParametersReply)WktResponse.MsgContent;

    //  _list_message_id = new List<Int32>();
    //  _list_message_id.Clear();
    //  _provider_id = String.Empty;
    //  _terminal_list = new TerminalList();

    //  Params.TerminalName = TerminalName;

    //  try
    //  {
    //    //_response.Functions = new List<Int32>();
    //    //_response.Images = new WKT_Images();
    //    //_response.Resources = new WKT_ResourceInfoList();
    //    //_response.m_game_gateway_params = new LCDMessages.LcdGameGateWayParams();
    //    //_response.m_game_gateway_params.ProviderName = string.Empty;
    //    //_response.m_game_gateway_params.PartnerId = string.Empty;
    //    //_response.m_game_gateway_params.Url = string.Empty;
    //    //_response.m_game_gateway_params.UrlTest = string.Empty;

    //    //_response.m_fb_params = new LCDMessages.LcdFBParams();
    //    //_response.m_fb_params.Enabled = 0;
    //    //_response.m_fb_params.PinRequest = 0;
    //    //_response.m_fb_params.HostAddress = String.Empty;
    //    //_response.m_fb_params.Url = String.Empty;
    //    //_response.m_fb_params.Login = String.Empty;
    //    //_response.m_fb_params.Password = String.Empty;

    //    using (DB_TRX _db_trx = new DB_TRX())
    //    {
    //      // Get 
    //      //    - Kiosk Functionalities (only enabled)
    //      //    - Kiosk Customized Images
    //      //    - Customized Messages
    //      //      Access to GENERAL_PARAMS table to obtain the Id of every customized message. 
    //      //      Message contents will be accessed in the ordinary way with GeneralParam.Value ()
    //      //    - Operation schedule interval

    //      _sb = new StringBuilder();
    //      _sb.AppendLine("    SELECT   FUN_FUNCTION_ID                    ");
    //      _sb.AppendLine("      FROM   LCD_FUNCTIONALITIES                ");
    //      _sb.AppendLine("     WHERE   FUN_ENABLED = 1                    ");
    //      _sb.AppendLine(GetSqlWhereFunctionalities());

    //      // TODO GET RESOURCES (SEE LCD_ProcessGetParameters function)

    //      //_sb.AppendLine(" ;                                              ");
    //      //_sb.AppendLine("    SELECT   RES_RESOURCE_ID                    ");
    //      //_sb.AppendLine("           , RES_EXTENSION                      ");
    //      //_sb.AppendLine("           , RES_HASH                           ");
    //      //_sb.AppendLine("           , RES_LENGTH                         ");
    //      //_sb.AppendLine("           , CIM_IMAGE_ID                       ");
    //      //_sb.AppendLine("      FROM   WKT_RESOURCES                      ");
    //      //_sb.AppendLine("INNER JOIN   LCD_IMAGES                         ");
    //      //_sb.AppendLine("        ON   RES_RESOURCE_ID = CIM_RESOURCE_ID  ");

    //      using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString()))
    //      {
    //        using (SqlDataReader _sql_dr = _db_trx.ExecuteReader(_sql_cmd))
    //        {
    //          // Fill Functionalities
    //          while (_sql_dr.Read())
    //          {
    //            //_response.Functions.Add(_sql_dr.GetInt32(0));
    //          }

    //          // Fill Custom Images, and Resources of this Images
    //          if (_sql_dr.NextResult())
    //          {
    //            while (_sql_dr.Read())
    //            {
    //              //_response.Images.Add(new WKT_Image(_sql_dr.GetInt32(4), _sql_dr.GetInt64(0)));
    //              //_response.Resources.Add(new WKT_ResourceInfo(_sql_dr.GetInt64(0), _sql_dr.GetString(1), _sql_dr.GetSqlBytes(2).Value, _sql_dr.GetInt32(3)));
    //            }
    //          }
    //        }
    //      }
    //    } // Using DB_TRX


    //    //TODO GET GAMEGATEWAY (SEE LCD_ProcessGetParameters function)

    //    ////*** FILL GAMEGATEWAY
    //    //// 1. Get GP's about GameGateWay 
    //    //// 2. Transfer Parameters to structure
    //    //if ( Misc.IsGameGatewayEnabled() 
    //    //  && ( TerminalId.HasValue && _terminal_list.IsGameGatewayTerminalEnabled(TerminalId.Value)))
    //    //{
    //    //  _provider_id = GeneralParam.GetString("GameGateway", "Provider", string.Empty);
    //    //  if (GeneralParam.GetBoolean("GameGateway", String.Format("Provider.{0}.Enabled", _provider_id), false))
    //    //  {
    //    //    //_response.m_game_gateway_params.GameGateway = 1;
    //    //    //_response.m_game_gateway_params.ReservedCredit = GeneralParam.GetInt32("GameGateway", "Reserved.Enabled", (Int32)MultiPromos.GAME_GATEWAY_RESERVED_MODE.GAME_GATEWAY_RESERVED_MODE_WITHOUT_BUCKETS);
    //    //    //_response.m_game_gateway_params.PartnerId = GeneralParam.GetString("GameGateway", String.Format("Provider.{0}.PartnerId", _provider_id), String.Empty);
    //    //    //_response.m_game_gateway_params.ProviderName = GeneralParam.GetString("GameGateway", String.Format("Provider.{0}.Name", _provider_id), String.Empty);
    //    //    //_response.m_game_gateway_params.Url = GeneralParam.GetString("GameGateway", String.Format("Provider.{0}.Uri", _provider_id), String.Empty);
    //    //    //_response.m_game_gateway_params.UrlTest = GeneralParam.GetString("GameGateway", String.Format("Provider.{0}.UriTest", _provider_id), String.Empty);
    //    //    //_response.m_game_gateway_params.AwardPrizes = GeneralParam.GetInt32("GameGateway", "AwardPrizes", 1);
    //    //  }
    //    //}

    //    ////*** FILL FB
    //    //// 1. Get GP's about GameGateWay 
    //    //// 2. Transfer Parameters to structure
    //    //if (GeneralParam.GetBoolean("FB", "Enabled", false))
    //    //{
    //    //  //_response.m_fb_params.Enabled = 1;
    //    //  //_response.m_fb_params.PinRequest = GeneralParam.GetInt32("FB", "PinRequest", 0);
    //    //  //_response.m_fb_params.HostAddress = GeneralParam.GetString("FB", "HostAddress", String.Empty);
    //    //  //_response.m_fb_params.Url = GeneralParam.GetString("FB", "Web.Url", String.Empty);
    //    //  //_response.m_fb_params.Login = GeneralParam.GetString("FB", "Login", String.Empty);
    //    //  //_response.m_fb_params.Password = GeneralParam.GetString("FB", "Password", String.Empty);
    //    //}

    //    //*** FILL TerminalDraw
    //    // 1. Get GP's about TerminalDraw
    //    // 2. Transfer Parameters to structure
    //    if (Misc.IsTerminalDrawEnabled())
    //    {
    //      InTouchTerminalDraw _terminal_enabled =  new InTouchTerminalDraw();
    //      _terminal_enabled.Enabled = true;
    //    }

    //    //WktResponse.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_WKT_OK;

    //    return true;

    //  }
    //  catch (Exception _ex)
    //  {
    //    Log.Exception(_ex);

    //    //_response.Functions = new List<Int32>();
    //    //_response.Images = new WKT_Images();
    //    //_response.Resources = new WKT_ResourceInfoList();
    //    //_response.m_game_gateway_params = new LCDMessages.LcdGameGateWayParams();
    //    //_response.m_fb_params = new LCDMessages.LcdFBParams();

    //    //WktResponse.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_WKT_ERROR;
    //  }

    //  return false;

    //} // GetInTouchParameters

    /// <summary>
    /// Get InTouch Data Account
    /// </summary>
    /// <param name="AccountId"></param>
    /// <returns></returns>
    //public Account GetInTouchAccount(String AccountData)
    //{
    //  Account _account = new Account();

    //  //_account.ActualLevel = "GOLD";
    //  //_account.Balance.BalanceTotal = 1000;
    //  //_account.Name = "PEPE";
    //  //_account.PointsToNextLevel = "400";
    //  //_account.RedeemPoints = 500;
    //  //_account.LevelPoints = "900";

    //  //TODO: Obtener datos según account ID
    //  //TODO: Mandar datos obtenidos 
    //  return _account;
    //}

    //public LCDInTouch.Data.WCP_WKT.WKT_Player GetInTouchPlayer(Int32 AccountId, string TerminalName)
    //{
    //  Account _account = new Account();
    //  SqlTransaction SqlTrx = new SqlTransaction();
    //  this.ReadPlayerInfo(AccountId, TerminalName, SqlTrx, _account);

    //  return _player;
    //}

    #endregion

    #region " Private Methods "

    /// <summary>
    /// Filter functionalities form GP value
    /// </summary>
    /// <returns></returns>
    private static String GetSqlWhereFunctionalities()
    {
      String _where;

      _where = String.Empty;

      // GameGateway
      if (!Misc.IsGameGatewayEnabled())
      {
        _where += String.Format(" AND FUN_FUNCTION_ID != {0}", (Int32)TYPE_LCD_FUNCTIONALITIES.GAMEGATEWAY);
      }

      // FB
      if (!GeneralParam.GetBoolean("FB", "Enabled", false))
      {
        _where += String.Format(" AND FUN_FUNCTION_ID != {0}", (Int32)TYPE_LCD_FUNCTIONALITIES.FB);
      }

      return _where;
    } //WKT_ProcessGetParameters

    /// <summary>
    /// Read the account info for WKT_ProcessGetPlayerInfoFields
    /// </summary>
    /// <param name="AccountId"></param>
    /// <param name="TerminalName"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    //public bool ReadPlayerInfo(Int64 AccountId, String TerminalName, SqlTransaction SqlTrx, Account _account)
    //{
      //return true;
      //StringBuilder _sql_txt;
      //Int64 _play_session_id;
      //String _aux_text;
      ////DateTime _day_start_counting_points;
      ////Int32 _days_counting_points;
      //ENUM_ANTI_MONEY_LAUNDERING_LEVEL _anti_ml_level;
      //Boolean _account_already_registered;
      //Boolean _threshold_crossed;
      //Currency _redeemable_amount_to_add;
      //DataTable _dt_remaining_spent_per_provider;
      //Currency _current_spent;
      //Currency _current_played;
      //Points _points_generated;
      //Points _points_discretionaries;

      //LCDInTouch.Data.WCP_WKT.WKT_PlayerInfo PlayerInfo = null;

      //PlayerInfo = null;

      //try
      //{
      //  _sql_txt = new StringBuilder();

      //  _sql_txt.Append("EXECUTE   GetAccountPointsCache ");
      //  _sql_txt.Append("          @pAccountId           ");

      //  _sql_txt.Append("  SELECT   ACP_PROMO_DATE                                                            ");
      //  _sql_txt.Append("         , ACP_PROMO_NAME                                                            ");
      //  _sql_txt.Append("         , ACP_INI_BALANCE                                                           ");
      //  _sql_txt.Append("         , ACP_BALANCE                                                               ");
      //  _sql_txt.Append("    FROM   ACCOUNT_PROMOTIONS WITH (INDEX(IX_acp_account_status))                    ");
      //  _sql_txt.Append("   WHERE   ACP_ACCOUNT_ID = @pAccountId                                              ");
      //  _sql_txt.Append("     AND   ACP_STATUS     = @pStatusActive                                           ");
      //  _sql_txt.Append("     AND   ACP_PROMO_TYPE <> @pExcludePromoNR2;                                      ");

      //  using (SqlCommand _sql_cmd = new SqlCommand(_sql_txt.ToString(), SqlTrx.Connection, SqlTrx))
      //  {
      //    _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
      //    _sql_cmd.Parameters.Add("@pStatusActive", SqlDbType.Int).Value = ACCOUNT_PROMO_STATUS.ACTIVE;
      //    _sql_cmd.Parameters.Add("@pExcludePromoNR2", SqlDbType.Int).Value = Promotion.PROMOTION_TYPE.AUTOMATIC_PER_RECHARGE_COVER_COUPON;

      //    using (SqlDataReader _sql_reader = _sql_cmd.ExecuteReader())
      //    {
      //      if (!_sql_reader.Read())
      //      {
      //        return false;
      //      }

      //      //_account.
      //        // FullName must match WCP_BasicPlayerInfoParameters->FullName size (=80)
      //      _aux_text = _sql_reader.GetString(_sql_reader.GetOrdinal("AC_HOLDER_NAME"));
      //      PlayerInfo.FullName = (_aux_text.Length > 80) ? _aux_text.Remove(80)
      //                                                    : _aux_text;
      //      PlayerInfo.Gender = (DrawGenderFilter)_sql_reader.GetInt32(_sql_reader.GetOrdinal("AC_HOLDER_GENDER"));
      //      PlayerInfo.Birthday = _sql_reader.GetDateTime(_sql_reader.GetOrdinal("AC_HOLDER_BIRTH_DATE"));
      //      PlayerInfo.MoneyBalance = (Currency)_sql_reader.GetSqlMoney(_sql_reader.GetOrdinal("AC_BALANCE"));

      //      PlayerInfo.PointsBalance = (Points)_sql_reader.GetDecimal(_sql_reader.GetOrdinal("AC_POINTS"));
      //      PlayerInfo.Level = WKT_PlayerInfo_GetLevel(_sql_reader);

      //      // Next Level
      //      _points_generated = (Points)_sql_reader.GetSqlMoney(_sql_reader.GetOrdinal("AM_POINTS_GENERATED"));
      //      _points_discretionaries = (Points)_sql_reader.GetSqlMoney(_sql_reader.GetOrdinal("AM_POINTS_DISCRETIONARIES"));

      //      PlayerInfo.NextLevel.SetLevel(_points_generated, _points_discretionaries, PlayerInfo.Level.LevelId);

      //      PlayerInfo.Money2Balance = 0;
      //      PlayerInfo.Points2Balance = 0;

      //      _play_session_id = _sql_reader.GetInt64(_sql_reader.GetOrdinal("AC_CURRENT_PLAY_SESSION_ID"));
      //      PlayerInfo.InSession = (_play_session_id != 0);
      //      PlayerInfo.InSessionTerminalName = "";

      //      if (PlayerInfo.InSession)
      //      {
      //        PlayerInfo.InSessionTerminalName = _sql_reader.GetString(_sql_reader.GetOrdinal("AC_CURRENT_TERMINAL_NAME"));
      //      }
      //      PlayerInfo.BalanceParts = WKT_PlayerInfo_GetBalanceParts(_sql_reader);

      //      PlayerInfo.CurrentSpent = 0;
      //      PlayerInfo.CurrentPlayed = 0;
      //    }

      //    if (Misc.IsGamingDayEnabled())
      //    {
      //      DateTime _next_gaming_day;
      //      CashierSessionInfo _session_info;
      //      WSI.Common.Terminal.TerminalInfo _terminalInfo;

      //      if (!WSI.Common.Terminal.Trx_GetTerminalInfo(TerminalName, out _terminalInfo, SqlTrx))
      //      {
      //        Log.Error(String.Format("Wkt_player.Read: Could not get terminalInfo. Terminal Name = {0}, Account_id = {1}", TerminalName, AccountId));
      //        return false;
      //      }

      //      if (_terminalInfo.TerminalType == TerminalTypes.PROMOBOX)
      //      {
      //        _session_info = Cashier.GetSystemCashierSessionInfo(Misc.GetUserTypePromobox(), SqlTrx, TerminalName);

      //        _next_gaming_day = _session_info.GamingDay.AddDays(1);

      //        if (WGDB.Now >= _next_gaming_day)
      //        {
      //          // The WKT should disable its note acceptor.
      //          PlayerInfo.NoteAcceptorEnabled = false;

      //          return true;
      //        }
      //      }
      //    }

      //    // AntiMoneyLaundery only available for Mexico.  Highest value bank value in Mexico is 1000$
      //    _redeemable_amount_to_add = GeneralParam.GetCurrency("NoteAcceptor", "HighestBankNoteValue", 1000);

      //    if (!Accounts.CheckAntiMoneyLaunderingFilters(AccountId, ENUM_CREDITS_DIRECTION.Recharge, _redeemable_amount_to_add, SqlTrx,
      //                                      out _anti_ml_level, out _threshold_crossed, out _account_already_registered))
      //    {
      //      PlayerInfo.NoteAcceptorEnabled = false;

      //      return false;
      //    }

      //    PlayerInfo.NoteAcceptorEnabled = WSI.Common.Misc.IsNoteAcceptorEnabled();

      //    switch (_anti_ml_level)
      //    {
      //      case ENUM_ANTI_MONEY_LAUNDERING_LEVEL.None:
      //        break;

      //      case ENUM_ANTI_MONEY_LAUNDERING_LEVEL.IdentificationWarning:
      //        if (!_account_already_registered && _threshold_crossed)
      //        {
      //          if (GeneralParam.GetBoolean("AntiMoneyLaundering", "Recharge.Identification.DontAllowMB"))
      //          {
      //            PlayerInfo.NoteAcceptorEnabled = false;
      //          }
      //        }
      //        break;

      //      case ENUM_ANTI_MONEY_LAUNDERING_LEVEL.ReportWarning:
      //        if (_threshold_crossed)
      //        {
      //          if (GeneralParam.GetBoolean("AntiMoneyLaundering", "Recharge.Report.DontAllowMB"))
      //          {
      //            PlayerInfo.NoteAcceptorEnabled = false;
      //          }
      //        }
      //        break;

      //      case ENUM_ANTI_MONEY_LAUNDERING_LEVEL.Identification:
      //      case ENUM_ANTI_MONEY_LAUNDERING_LEVEL.Report:
      //        if (!_account_already_registered)
      //        {
      //          PlayerInfo.NoteAcceptorEnabled = false;
      //        }
      //        break;

      //      default:
      //        PlayerInfo.NoteAcceptorEnabled = false;
      //        break;
      //    }

      //    _dt_remaining_spent_per_provider = Promotion.AccountRemainingDataPerProvider(AccountId, SqlTrx);
      //    Promotion.CalculateDataByProvider(_dt_remaining_spent_per_provider, null, out _current_spent, out _current_played);

      //    PlayerInfo.CurrentSpent = _current_spent;
      //    //SDS 22-10-2015
      //    PlayerInfo.CurrentPlayed = _current_played;

      //    return true;
      //  }
      //}
      //catch (Exception _ex)
      //{
      //  Log.Exception(_ex);
      //  return false;
      //}
    //}//Read

    #endregion 

  }
}
