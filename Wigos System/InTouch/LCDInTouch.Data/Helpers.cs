﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Common.cs
// 
//   DESCRIPTION: Common clase. 
// 
//        AUTHOR: Javi Barea
// 
// CREATION DATE: 05-JUL-2017
// 
// REVISION HISTORY:
// 
// Date        Author     Description
// ----------- ------     ------------------------------------------------------
// 05-JUL-2017 JBP        First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LCDInTouch.Data
{
  #region " Structs "

  public struct InTouchParams
  {
    public Int16 SubsystemMode;
    public Int16 LanguageId;
    public String TerminalName;
    
    public InTouchParamsConfig Parameters;
    public InTouchGameGateWay GameGateWayParameters;
    public InTouchParamsFBParams FBParameters;
    public InTouchTerminalDraw TerminalDrawParameters;
  } // InTouchParams

  public struct InTouchParamsConfig
  {
    public Int64 NumFunctionalities;
    public Int64[] Functionalities;
    public Int64 NumResources;
    public InTouchParamsResources[] Resource;

  } // InTouchParamsConfig

  public struct InTouchParamsResources
  {
    public Int64 Id;
    public String Path;
  } //InTouchParamsResources

  public struct InTouchGameGateWay
  {
    public Int32 GameGateway;
    public Int32 TerminalId;
    public Int32 ReservedCredit;
    public Int16 AwardPrizes;
    public String Url;
    public String UrlTest;
    public String ProviderName;
    public String PartnerId;
  } // InTouchGameGateWay

  public struct InTouchParamsFBParams
  {
    public Boolean Enabled;
    public Int32   PinRequest;
    public String  HostAddress;
    public String  Url;
    public String  Login;
    public String  Password;
     
  } //InTouchParamsFBParams

  public struct InTouchTerminalDraw
  {
    public Boolean Enabled;
  } //InTouchTerminalDraw

  #endregion

}