﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//      MODULE NAME: Util.cs
// 
//      DESCRIPTION: Util class for LCDInTouchWeb
// 
//           AUTHOR: Ferran Ortner
// 
//    CREATION DATE: 30-JUN-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 30-JUN-2017 FOS    First release.
//------------------------------------------------------------------------------
using System;
using System.Configuration;
using System.Reflection;

namespace LCDInTouch.Web.Classes
{
  public class Util
  {

    #region "Properties"

    /// <summary>
    /// Get the Current Version
    /// </summary>
    public static string CurrentVersion
    {
      get
      {
        Assembly _assembly;
        Version _version;

        _assembly = Assembly.GetExecutingAssembly();
        _version = _assembly.GetName().Version;

        return _version.Major.ToString("00") + "." + _version.Minor.ToString("000");
      }
    } // CurrentVersion

    /// <summary>
    /// Get the app name
    /// </summary>
    public static string AppName
    {
      get
      {
        Assembly _assembly;
        String _name;

        _assembly = Assembly.GetExecutingAssembly();
        _name = _assembly.GetName().Name;

        return _name;
      }
    } // CurrentVersion

    #endregion

    #region "Public Methods"

    /// <summary>
    /// Get the ConfigurationFile Settings
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="Key"></param>
    /// <returns></returns>
    public static T GetSetting<T>(String Key)
    {
      return GetSetting<T>(Key, default(T));
    } //GetSetting

    /// <summary>
    /// Get the ConfigurationFile Settings
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="Key"></param>
    /// <param name="DefaultValue"></param>
    /// <returns></returns>
    public static T GetSetting<T>(String Key, T DefaultValue)
    {
      try
      {
        var value = ConfigurationManager.AppSettings[Key];

        return (T)Convert.ChangeType(value, typeof(T));
      }
      catch (Exception)
      {   
        return DefaultValue;
      }
    } //GetSetting

    #endregion

  }
}