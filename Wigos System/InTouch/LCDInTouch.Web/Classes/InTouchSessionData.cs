﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//      MODULE NAME: InTouchSessionData.cs
// 
//      DESCRIPTION: InTouch Session data to save properties
// 
//           AUTHOR: Javier Barea
// 
//    CREATION DATE: 07-JUL-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 07-JUL-2017 JBP    First release.
//------------------------------------------------------------------------------
using LCDInTouch.Web.Models;
using System;

namespace LCDInTouch.Web.Classes
{
  public class InTouchSessionData
  {

    #region " Properties "

    public TYPE_CARD_STATUS CardStatus { get; set; }
    public TYPE_CARD_STATUS PreviousCardStatus { get; set; }

    public String WebHost { get; set; }
    public Object ParamArg { get; set; }
    public String Language { get; set; }
    public String ErrorMessage { get; set; }
    public Boolean PendingReset { get; set; }
    public Boolean TransferInProgress { get; set; }
    public Boolean SendTransferAfterPlayGame { get; set; }
    public Boolean PendingRequestParams { get; set; }
        
    // Protocol Actions
    public String ProtocolAction { get; set; }
    public String ProtocolController { get; set; }

    // Navigate Actions
    public String CurrentAction { get; set; }
    public String CurrentController { get; set; }
    public String PreviousAction { get; set; }
    public String PreviousController { get; set; }

    // Models
    public InTouchParamsModel SASParams { get; set; }
    public InTouchPinModel Pin { get; set; }
    public InTouchAccount Account { get; set; }
    public InTouchGameGatewayModel GameGateWay { get; set; }
    public InTouchTerminalDrawModel TerminalDraw { get; set; }
    public InTouchBlockReasons BlockReasons { get; set; }
    public InTouchPointsModel PointsConfiguration { get; set; }
    public InTouchRedirects Redirects { get; set; }
    public Message Message { get; set; }
   
    #endregion

    #region " Public Methods "

    public InTouchSessionData ()
    {
      this.PendingReset         = false;
      this.TransferInProgress   = false;
      this.PendingRequestParams = false;
      this.ErrorMessage         = String.Empty;         
      this.CardStatus           = TYPE_CARD_STATUS.CARD_REMOVED;
      this.Language             = Constants.TYPE_LANGUAGE_SPANISH;
      this.SendTransferAfterPlayGame = false;

      // Actions
      this.ProtocolAction       = TypeView.Manager;
      this.ProtocolController   = TypeController.Manager;  
      this.CurrentAction        = TypeView.Manager;
      this.CurrentController    = TypeController.Manager;      
      this.PreviousAction       = TypeView.Manager;
      this.PreviousController   = TypeController.Manager;

      this.SASParams            = new InTouchParamsModel(); 
      this.Account              = new InTouchAccount();
      this.Pin                  = new InTouchPinModel();
      this.TerminalDraw         = new InTouchTerminalDrawModel();
      this.GameGateWay          = new InTouchGameGatewayModel();
      this.BlockReasons         = new InTouchBlockReasons();
      this.PointsConfiguration  = new InTouchPointsModel();
      this.Redirects            = new InTouchRedirects();
      this.Message              = new Message();
    } // InTouchSessionData

    /// <summary>
    /// Update Current action
    /// </summary>
    /// <param name="View"></param>
    /// <param name="Controller"></param>
    internal void UpdateCurrentAction(String Controller, String View)
    {
      // Set current action and controller
      this.CurrentController = Controller;
      this.CurrentAction = View;

    } // UpdateCurrentAction

    #endregion    

  } // InTouchSessionData
}