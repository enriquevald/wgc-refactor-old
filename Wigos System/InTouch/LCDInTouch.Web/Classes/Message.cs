﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//      MODULE NAME: Message.cs
// 
//      DESCRIPTION: Message info class for LCDInTouch
// 
//           AUTHOR: David Perelló
// 
//    CREATION DATE: 11-JUL-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 20-OCT-2017 DP    First release.
//------------------------------------------------------------------------------
using System;

namespace LCDInTouch.Web.Classes
{

  public class Message
  {
    public TYPE_MESSAGE_TO_SHOW Type { get; set; }
    public String Text { get; set; }

    #region " Public Methods "

    public Message()
    {
      this.Type = TYPE_MESSAGE_TO_SHOW.None;
    } // Message

    #endregion

    #region " Internal Methods "

    /// <summary>
    /// Set message properties
    /// </summary>
    /// <param name="Title"></param>
    /// <param name="ErrorMessage"></param>
    /// <param name="ErrorDescription"></param>
    /// <param name="DefaultError"></param>
    /// <param name="Icon"></param>
    internal void SetMessage(String Title, String ErrorMessage, String ErrorDescription, String DefaultError, TypeIcon Icon)
    {
      //if (String.IsNullOrEmpty(this.SessionData.ErrorMessage))
      //{
      //  SessionData.ErrorMessage = resources.nls_resource.ce_problema;
      //}


      //ViewBag.MessageTitle = resources.nls_resource.ce_Atencion;
      //ViewBag.MainDescription = SessionData.ErrorMessage;
      //ViewBag.Description = resources.nls_resource.ce_responsable;
      //ViewBag.Icon = TypeIcon.Warning;
    } // SetMessage

    #endregion

  } // Message

}