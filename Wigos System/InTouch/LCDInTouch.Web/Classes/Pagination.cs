﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//      MODULE NAME: InTouchSessionData.cs
// 
//      DESCRIPTION: InTouch Session data to save properties
// 
//           AUTHOR: Ferran Ortner
// 
//    CREATION DATE: 07-JUL-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 07-JUL-2017 FOS    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using WSI.Common;
using WSI.Common.InTouch;

namespace LCDInTouch.Web.Classes
{
  public class Pagination<T> : List<T>
  {

    #region "Constants "

    private const String K_DATA_ACCOUNT_LEVEL_FORMAT            = "GIFTS.GI_POINTS_LEVEL{0}";
    private const String K_PAGE_CENTRED_POSITION_FORMAT         = "width: {0}px";
    private const String K_RANGE_POINTS_INFO_BUTTON             = "{0} TO {1} {2}";
    private const String K_POINTS_INFO_BUTTON                   = "{0}{1}";
    private const String K_DISABLED                             = "disabled";
    private const String K_CREDIT_REDEEM                        = " CR";
    private const String K_CREDIT_NO_REDEEM                     = " CNR";
                                                                
    private const Int32 K_MAX_ZERO_ITEMS_PER_PAGE               = 0;
    private const Int32 K_MAX_ITEMS_PER_PAGE_PLAY_POINTS        = 4;
    private const Int32 K_MARGIN_NAVIGATION_ELEMENTS            = 67;
    private const Int32 K_WIDTH_PAGE_NAVIGATION_ELEMENTS_POINTS = 550;
    private const Int32 K_ITEMS_FOR_PAGE_PLAY_REWARD            = 2;
    private const Int32 K_WIDTH_PAGE_NAVIGATION_ELEMENTS_REWARD = 580;

    #endregion

    #region " Properties "

    public int PageIndex { get; private set; }
    public int PageSize { get; private set; }
    public int TotalCount { get; private set; }
    public int TotalPages { get; private set; }
    public Boolean OneItem { get; set; }
    public String CenterOnPage { get; set; }
    public int CurrentPage { get; private set; }

    public bool HasPreviousPage
    {
      get { return (PageIndex > 0); }
    }

    public bool HasNextPage
    {
      get { return (PageIndex + 1 < TotalPages); }
    }

    #endregion 

    #region " Public Methods "

    public Pagination(IQueryable<T> source, int pageIndex, int pageSize)
    {
      PageIndex = pageIndex;
      PageSize = pageSize;
      TotalCount = source.Count();
      TotalPages = (int)Math.Ceiling(TotalCount / (double)PageSize);
   
      this.AddRange(source.Skip(PageIndex * PageSize).Take(PageSize));
    } // Pagination


    public Pagination()
    {
      //Default constructor
    }

    #endregion 

    #region
    /// <summary>
    /// Function that returns all the rewards/Playpoints promo into navigation control
    /// </summary>
    /// <param name="page"></param>
    /// <returns></returns>
    public Pagination<InTouchListItem> GenerateNavigationControl(int? page, Int32 LevelId, Int32 PointsBalance, Int32 AccountId, Boolean IsPlayReward = false)
    {
      InTouchListItem _list_item;
      List<InTouchListItem> _points_list;
      Pagination<InTouchListItem> _paginated_items;
      Int32 _current_page;
      List<InTouchListItem> _play_points_button_list;
      String _data_account_level;
      Int32 _width_value_for_button;

      _list_item = new InTouchListItem();
     
      if (AccountId >  0 && IsPlayReward)
      {
        List<InTouchListItem> _reward_list;
     
        _reward_list = _list_item.GetAllRewardItemList(AccountId);
        _paginated_items = new Pagination<InTouchListItem>(_reward_list.AsQueryable(), page ?? K_MAX_ZERO_ITEMS_PER_PAGE, K_ITEMS_FOR_PAGE_PLAY_REWARD);

        if (_paginated_items.Count == 1)
        {
          _paginated_items.OneItem = true;
        }
        else
        {
          _paginated_items.OneItem = false;
        }

        _width_value_for_button = K_WIDTH_PAGE_NAVIGATION_ELEMENTS_REWARD;
     }
      else
      {
        _data_account_level = String.Format(K_DATA_ACCOUNT_LEVEL_FORMAT, LevelId);
        _points_list = _list_item.GetAllPointsItemList(_data_account_level,AccountId);
        _play_points_button_list = CreatePlayPointButtons(_points_list, PointsBalance);
        _paginated_items = new Pagination<InTouchListItem>(_play_points_button_list.AsQueryable().Distinct(), page ?? K_MAX_ZERO_ITEMS_PER_PAGE, K_MAX_ITEMS_PER_PAGE_PLAY_POINTS);
        _width_value_for_button = K_WIDTH_PAGE_NAVIGATION_ELEMENTS_POINTS;
        _paginated_items.OneItem = (_paginated_items.Count < K_MAX_ITEMS_PER_PAGE_PLAY_POINTS);

      }
      _current_page = _paginated_items.PageIndex + 1;

      _paginated_items.PageIndex = _current_page;
      _paginated_items.TotalPages = _paginated_items.TotalPages;

      _current_page = _width_value_for_button - (_paginated_items.TotalPages * K_MARGIN_NAVIGATION_ELEMENTS);
      _paginated_items.CurrentPage = _current_page / 2;
      _paginated_items.CenterOnPage = String.Format(K_PAGE_CENTRED_POSITION_FORMAT, _paginated_items.CurrentPage);

      return _paginated_items;
    } // GenerateNavigationControl

    /// <summary>
    /// Create all the Buttons to show on screen
    /// </summary>
    /// <param name="PointList"></param>
    /// <param name="SessionData"></param>
    /// <returns></returns>
    private List<InTouchListItem> CreatePlayPointButtons(List<InTouchListItem> PointList, Int32 PointsBalance)
    {
      String _promotion_name;
      String _prize_reward_aux;
      List<InTouchListItem> _play_points_buttons_list;
      List<InTouchListItem> _points_list_aux;
      String _return_units_str;

      _promotion_name = String.Empty;
      _play_points_buttons_list = new List<InTouchListItem>();

      if (PointList == null)
      {
        return _play_points_buttons_list;
      }

      foreach (var item in PointList)
      {
        if (_promotion_name != item.PromotionName)
        {
          _points_list_aux = (List<InTouchListItem>)PointList.Where(x => x.PromotionName.Equals(item.PromotionName)).ToList();


          for (int i = 0; i < _points_list_aux.Count; i++)
          {
            _return_units_str = GetUnitReturnTypeStr(_points_list_aux[i].PromoGameReturnUnitsId);

            if (i + 1 < _points_list_aux.Count)
            {
              _prize_reward_aux = String.Format(K_RANGE_POINTS_INFO_BUTTON, _points_list_aux[i].LevelValue.ToString(), _points_list_aux[i + 1].LevelValue.ToString(), _return_units_str);
            }
            else
            {
              _prize_reward_aux = String.Format(K_POINTS_INFO_BUTTON, _points_list_aux[i].LevelValue.ToString(), _return_units_str);
            }
            _points_list_aux[i].PrizeValue = _prize_reward_aux;
            if (PointsBalance < Convert.ToInt64(_points_list_aux[i].LevelValue))
            {
              _points_list_aux[i].EnableButton = K_DISABLED;
            }

            if (_points_list_aux[i].CanShow)
            {
              _play_points_buttons_list.Add(_points_list_aux[i]);
            }
          }
        }
        _promotion_name = item.PromotionName;
      }
      return _play_points_buttons_list;
    } // CreatePlayPointButtons

    /// <summary>
    /// Get the unitReturn string to show into the buttons
    /// </summary>
    /// <param name="ReturnTypeUnits"></param>
    /// <returns></returns>
    private string GetUnitReturnTypeStr(Int64 ReturnTypeUnits)
    {
      String _return_units_str;
      _return_units_str = string.Empty;

      switch ((PrizeType)ReturnTypeUnits)
      {
        case PrizeType.Redeemable:
          _return_units_str = K_CREDIT_REDEEM;
          break;

        case PrizeType.NoRedeemable:
          _return_units_str = K_CREDIT_NO_REDEEM;
          break;

        default:
          _return_units_str = String.Empty;
          break;
      }
      return _return_units_str;
    } // GetUnitReturnTypeStr
    #endregion
  } // Pagination
}