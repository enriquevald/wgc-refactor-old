﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//      MODULE NAME: InTouchAction.cs
// 
//      DESCRIPTION: Action class
// 
//           AUTHOR: David Perelló
// 
//    CREATION DATE: 19-OCT-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 19-OCT-2017 DPC    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LCDInTouch.Web.Classes
{
  public class InTouchAction
  {
    public String Controller { get; set; }
    public String Action { get; set; }

    /// <summary>
    /// Constructor
    /// </summary>
    public InTouchAction()
    {
    } // InTouchAction

    /// <summary>
    /// Contructor
    /// </summary>
    /// <param name="CurrentController"></param>
    /// <param name="CurrentAction"></param>
    public InTouchAction(String CurrentController, String CurrentAction)
    {
      this.Controller = CurrentController;
      this.Action = CurrentAction;
    } // InTouchAction

  } // InTouchAction
}