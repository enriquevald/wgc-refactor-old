﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//      MODULE NAME: InTouchProtocol.cs
// 
//      DESCRIPTION: Protocol class por inTouchWeb
// 
//           AUTHOR: Javier Barea
// 
//    CREATION DATE: 06-JUL-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 06-JUL-2017 JBP    First release.
//------------------------------------------------------------------------------
using LCDInTouch.Web.Models;
using LCDInTouch.Web.resources;
using System;
using System.Collections.Generic;
using System.Globalization;
using WSI.Common;
using WSI.Common.ExtentionMethods;
using WSI.Common.TerminalDraw;
using System.Linq;

namespace LCDInTouch.Web.Classes
{ 
  
  public class InTouchProtocol
  {

    #region " Properties "

    public InTouchSessionData SessionData { get; set; }

    #endregion

    #region " Public Methods "

    public InTouchProtocol(InTouchSessionData SessionData)
    {
      this.SessionData = SessionData;
    } // InTouchProtocol

    /// <summary>
    /// Protocol requests manager
    /// </summary>
    /// <param name="FunctionCode"></param>
    /// <param name="Result"></param>
    /// <param name="Data"></param>
    /// <returns></returns>
    public void Manager(TYPE_REQUEST FunctionCode, int Result, String Data)
    {
      switch (FunctionCode)
      {
        case TYPE_REQUEST.BLOCK_REASON:
          this.ShowBlockReasons(Result, Data);          
          break;

        case TYPE_REQUEST.PARAMS:
          this.GetInTouchParams(Data);
          break;

        case TYPE_REQUEST.CARD_STATUS:
          this.CardStatusManager(Result, Data);    
          break;

        case TYPE_REQUEST.TERMINAL_DRAW_GAME_RESULT:
          this.ShowDrawResult(Result, Data);    
          break;

        case TYPE_REQUEST.TERMINAL_DRAW_GAME_END:
          this.EndPlayDraw();
          break;

        case TYPE_REQUEST.TRANSFER_CARD_CREDIT_TO_MACHINE:
          this.ShowTransferCreditToMachineResult(Result, Data);
          break;

        case TYPE_REQUEST.PLAYER_LANGUAGE_ID:
          this.SetLanguage(Result);
          break;

        case TYPE_REQUEST.REDIRECT_ACTION:
        case TYPE_REQUEST.REDIRECT_PREVIOUS_ACTION:
          this.RedirectAction(FunctionCode, Data);
          break;

        default:
          Log.Add(TYPE_MESSAGE.ERROR, String.Format("Error on InTouchProtocol.Manager(). Unknown type request {0}.", FunctionCode));
          break;
      }

    } // Manager
    
    /// <summary>
    /// Function that set the draw info
    /// </summary>
    /// <param name="FirstLine"></param>
    /// <param name="SecondLine"></param>
    /// <param name="UrlRedirect"></param>
    public void GetDrawInfo(out String FirstLine, out String SecondLine, out String ThirdLine) //, out String UrlRedirect)
    {
      String _day;
      String _year;
      DateTime _date;
      String _type_units;
      String _month_name;
      PromoGame _game;

      // Check Price units
      if (this.SessionData.TerminalDraw.GameParams.GetPriceUnits() == PrizeType.Unknown)
      {
        Log.Add(TYPE_MESSAGE.ERROR, "Error on GetDrawParams(). Unknown PrizeType.");
        this.SessionData.Message.Type = TYPE_MESSAGE_TO_SHOW.GeneralError;
      }
      
      // Check Game Type
      if (this.SessionData.TerminalDraw.GameParams.GetGameType() == PromoGame.GameType.Unknown)
      {
        Log.Add(TYPE_MESSAGE.ERROR, "Error on GetDrawParams(). Unknown GameType.");
        this.SessionData.Message.Type = TYPE_MESSAGE_TO_SHOW.GeneralError;
      }

      // Check Promotion Id
      _game = this.SessionData.TerminalDraw.GameParams.FirstOrDefault();

      

      if (_game == null
        || _game.Type != PromoGame.GameType.Welcome
        && _game.PromotionId <= 0)
      {
        Log.Add(TYPE_MESSAGE.ERROR, "Error on GetDrawParams(). Unknown PromotionId.");
        this.SessionData.Message.Type = TYPE_MESSAGE_TO_SHOW.GeneralError;
        this.SetActionFromManager(TypeController.Message, TypeView.Show);
      }

      //Promotion.GetExpirationDate(this.SessionData.TerminalDraw.GameParams.PromotionId(), out _date);
      AccountPromotion _ap = new AccountPromotion();
      _date = _ap.GetExpirationDateFromAccountPromotion(_game.PromotionId, this.SessionData.Account.Id);


      _day = _date.ToString("dd", CultureInfo.InvariantCulture);
      _month_name = _date.ToString("MMM", CultureInfo.InvariantCulture);
      _year = _date.ToString("yyyy", CultureInfo.InvariantCulture);

      // Set type units literal
      _type_units = (this.SessionData.TerminalDraw.GameParams.GetPriceUnits() == PrizeType.Redeemable) ? nls_resource.generic_redeem : nls_resource.generic_no_redeem;

      // Set PopUp data
      FirstLine = String.Format(nls_resource.pd_cash_description_firstline, this.SessionData.TerminalDraw.GameParams.TotalPrice().ToString(), _type_units);
      SecondLine = String.Format(nls_resource.pd_cash_description_secondline, this.SessionData.TerminalDraw.GameParams.TotalMinimumPrize().ToString(), _type_units);

      // If GameID is welcomeDraw doesn't have expiration date
      if (this.SessionData.TerminalDraw.GameParams.Id() > TerminalDraw.WELCOME_DRAW_ID)
      {
        ThirdLine = String.Format(nls_resource.pd_cash_description_thirdline, _day, _month_name, _year);
      }
      else
      {
        ThirdLine = String.Empty;
      }

      // If GameID is welcomeDraw doesn't have expiration date
      if (this.SessionData.TerminalDraw.GameParams.Id() > TerminalDraw.WELCOME_DRAW_ID)
      {
        ThirdLine = String.Format(nls_resource.pd_cash_description_thirdline, _day, _month_name, _year);
      }
      else
      {
        ThirdLine = String.Empty;
      }

      // If GameID is welcomeDraw doesn't have expiration date
      if (this.SessionData.TerminalDraw.GameParams.Id() > TerminalDraw.WELCOME_DRAW_ID)
      {
        ThirdLine = String.Format(nls_resource.pd_cash_description_thirdline, _day, _month_name, _year);
      }
      else
      {
        ThirdLine = String.Empty;
      }

      // TODO UNRANKED: Max prize
      if (this.SessionData.TerminalDraw.GameParams.GetGameType() == PromoGame.GameType.PlayPoint)
      {
        SecondLine += String.Format(nls_resource.pd_cash_description_secondline_play_points, this.SessionData.TerminalDraw.GameParams.TotalPrice().ToString(), _type_units);
      }

      //UrlRedirect = this.GetTerminalDrawGameUrl();
    } // GetDrawInfo

    /// <summary>
    /// Set action from Manager request.
    /// </summary>
    public void SetActionFromManager()
    {
      this.SetActionFromManager(this.SessionData.CurrentController, this.SessionData.CurrentAction, null);
    } // SetActionFromManager
    public void SetActionFromManager(String Controller, String View)
    {
      this.SetActionFromManager(Controller, View, null);
    } // SetActionFromManager
    public void SetActionFromManager(String Controller, String View, Object Param)
    {      
      this.SessionData.ProtocolController = Controller;
      this.SessionData.ProtocolAction = View;
      this.SessionData.ParamArg = Param;
    } // SetActionFromManager

    /// <summary>
    /// Get Account data
    /// </summary>
    /// <param name="AccountId"></param>
    /// <returns></returns>
    public Boolean GetAccountData(Int64 AccountId)
    {
      this.SessionData.Account = new InTouchAccount(AccountId);
    
      this.SessionData.Pin.NumRetries = this.SessionData.Account.PinNumRetries;

      return (!SessionData.Account.HasError);
    } // GetAccountData

    /// <summary>
    /// Ask participate draw manager (Only on StartCardSession)
    /// </summary>
    /// <param name="TerminalDrawParams"></param>
    public void AskParticipateDrawManager()
    {
      // Mark from ask participate. 
      this.SessionData.TerminalDraw.FromAskParticipate = true;

      // Redirect to Cant Participate manager
      this.SetActionFromManager(TypeController.PlayDraw, TypeView.PlayDrawCanParticipate);

    } // AskParticipateDrawManager

    /// <summary>
    /// Start draw manager
    /// </summary>
    public void StartDrawManager()
    {
      if (this.SessionData.TerminalDraw.GameParams == null)
      {
        //this.SetActionFromManager(TypeController.Message, TypeView.GeneralError);
        this.SessionData.Message.Type = TYPE_MESSAGE_TO_SHOW.GeneralError;
        return;
      }

      // Manage next view
      switch (this.SessionData.TerminalDraw.GameParams.GetGameType())
      {
        case PromoGame.GameType.Welcome:
          this.SetActionFromManager(TypeController.WelcomeDraw, TypeView.WelcomeDrawStart);
          break;

        case PromoGame.GameType.PlayCash:
          this.SetActionFromManager(TypeController.PlayCash, TypeView.PlayCashStart);
          break;

        case PromoGame.GameType.PlayReward:
          this.SetActionFromManager(TypeController.PlayReward, TypeView.PlayRewardStart);
          break;

        case PromoGame.GameType.PlayPoint:
          this.SetActionFromManager(TypeController.PlayPoint, TypeView.PlayPointStart);
          break;

        default:
          Log.Add(TYPE_MESSAGE.ERROR, "Error on StartDrawManager(). Unknown GameType.");
          this.SessionData.Message.Type = TYPE_MESSAGE_TO_SHOW.GeneralError;
          //this.SetActionFromManager(TypeController.Message, TypeView.GeneralError);
          break;
      }

    } // StartDrawManager
    
    /// <summary>
    /// Set Game Parameters
    /// </summary>
    /// <param name="GameId"></param>
    public void SetGameParams(Int64 GameId)
    {
      // Only for StartCardSession 
      this.SetGameParams(GameId, (PromoGame.GameType)this.SessionData.TerminalDraw.DrawParams.GameType);
    } // SetGameParams
    public void SetGameParams(Int64 GameId, PromoGame.GameType GameType)
    {
      // Set WelcomeDraw GameParams
      if (GameType == PromoGame.GameType.Welcome)
    {      
        this.SetGameParams_WelcomeDraw();

        return;
      }

      // Set PromoGame GameParams
      this.SetGameParams_PromoGame(GameId, GameType);
    } // SetGameParams

    #endregion 

    #region " Private Methods "

    /// <summary>
    /// Set Game Params (WelcomeDraw)
    /// </summary>
    /// <param name="GameId"></param>
    /// <param name="GameType"></param>
    /// <returns></returns>
    private void SetGameParams_WelcomeDraw()
    {
      Currency _total_price;
      Currency _minimum_prize;

      // Get TerminalDraw minimum prize
      if (!TerminalDraw.GetMinimumPrizeTerminalDraw(this.SessionData.Account.Id, this.SessionData.TerminalDraw.DrawParams.GameId, out _total_price, out _minimum_prize))
      {
        Log.Add(TYPE_MESSAGE.ERROR, "Error on SetGameParams_WelcomeDraw(). GetMinimumPrizeTerminalDraw not find pending WelcomeDraws.");

        return;
      }

      // Set GameParams: Welcome Draw
      //  - Set especific Game type from TerminalDrawParams result. Is the same of terminal_draws_recharges
      //  - In WelcomeDraw, needs calculate total price and minimum prize (Get SetGameParams)
      this.SessionData.TerminalDraw.GameParams = new List<PromoGame>()
      {
        new PromoGame(this.SessionData.TerminalDraw.DrawParams.GameId)
        {
          Price = _total_price,
          MinimumPrize = _minimum_prize
        }
      };
    } // SetGameParams_WelcomeDraw

    /// <summary>
    /// Set Game Params (PromoGame)
    /// </summary>
    /// <param name="GameId"></param>
    /// <param name="GameType"></param>
    /// <returns></returns>
    private void SetGameParams_PromoGame(Int64 GameId, PromoGame.GameType GameType)
    {
      // Set GameParams: PromoGames
      //  - Set especific Game type from TerminalDrawParams result. Is the same of terminal_draws_recharges
      //  - In PlayPoint cases, this its necessary because the asociated PromoGames, convert 
      //    the type to PlayPoint only in the moment on purchase the promotion.
      //  - All the PromoGames have PlayCash or PlayReward GameType. 
      this.SessionData.TerminalDraw.GameParams = new List<PromoGame>()
      {
        new PromoGame(GameId)
        {
          Type = GameType
        }
      };
    } // SetGameParams_PromoGame

    /// <summary>
    /// Card status manager
    /// </summary>
    /// <param name="Data"></param>
    /// <param name="Result"></param>
    private void CardStatusManager(Int32 Result, String Data)
    {
      try
      {
        Log.Add(TYPE_MESSAGE.INFO, String.Format("Send: {0}", ((TYPE_CARD_STATUS)Result).ToString()));

        this.SessionData.PreviousCardStatus = this.SessionData.CardStatus;
        this.SessionData.CardStatus = (TYPE_CARD_STATUS)Result;

        // Filter repeats card status        
        if (!this.CheckCardStatus())
        {
          return;
        }

        // Manage card status
        switch (this.SessionData.CardStatus)
        {
          case TYPE_CARD_STATUS.CARD_INSERTED:
            this.CardInserted();
            break;

          case TYPE_CARD_STATUS.CARD_INSERTED_VALIDATED:
            this.CardInsertedValidated(Data);
            break;

          case TYPE_CARD_STATUS.CARD_INSERTED_NOT_VALIDATED:
            this.CardInsertedNotValidated(Data);
            break;

          case TYPE_CARD_STATUS.TRANSFER_CREDIT:
            this.TransferCredit();
            break;

          case TYPE_CARD_STATUS.CARD_REMOVED:
          case TYPE_CARD_STATUS.CARD_REMOVED_VALIDATED:
            this.CardRemovedValidated(Data);
            break;

          case TYPE_CARD_STATUS.CARD_NOT_INSERTED:
            this.CardLogout();
            break;

          case TYPE_CARD_STATUS.ASK_PENDING_DRAWS:
            this.AskPendingDraw(Data);
            break;

          case TYPE_CARD_STATUS.PENDING_DRAWS:
            this.ShowPendingDraw(Data);
            break;

          default:
            Log.Add(TYPE_MESSAGE.ERROR, String.Format("CardStatus_ProtocolResponse() type not defined {0}", Data));
            break;

        }
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }
    } // CardStatusManager

    /// <summary>
    /// Check card status
    /// </summary>
    /// <returns></returns>
    private Boolean CheckCardStatus()
    {
      // Not need repeat same action
      if (this.SessionData.CardStatus == this.SessionData.PreviousCardStatus)
      {
        Log.Add(TYPE_MESSAGE.INFO, String.Format("Repeated card status {0}", this.SessionData.CardStatus.ToString()));
      }

      return true;
    } // CheckCardStatus

    /// <summary>
    /// Get terminal draw game url
    /// </summary>
    /// <returns></returns>
    private string GetTerminalDrawGameUrl()
    {
      return String.Format("{0}?BetAmount={1}&Balance={2}&AccountId={3}&ShowDraw={4}&Won={5}&WinAmount={6}"
                          , this.SessionData.TerminalDraw.DrawParams.Url
                          , this.SessionData.TerminalDraw.DrawParams.BetAmount
                          , this.SessionData.Account.BalanceInSession
                          , this.SessionData.Account.Id
                          , this.SessionData.TerminalDraw.DrawParams.RunDraw
                          , this.SessionData.TerminalDraw.DrawParams.IsWinner
                          , this.SessionData.TerminalDraw.DrawParams.WinAmount);
    } // GetTerminalDrawGameUrl

    /// <summary>
    /// Show block reasons
    /// </summary>
    /// <param name="Result"></param>
    private void ShowBlockReasons(int Result, String Data)
    {
      this.SetBlockReason(Result, Data);
      this.SetActionFromManager(TypeController.Manager, TypeView.Manager);
    } // ShowBlockReasons

    /// <summary>
    /// Set block reason
    /// </summary>
    /// <param name="Result"></param>
    /// <param name="Data"></param>
    private void SetBlockReason(int Result, string Data)
    {
      this.SessionData.BlockReasons.Id = Result;
      this.SessionData.BlockReasons.Code = Data.Split('|');
      this.SessionData.BlockReasons.Description = String.Empty;
    } // SetBlockReason

    /// <summary>
    /// Card inserted validated manager
    /// </summary>
    private void CardInsertedValidated(String AccountId)
    {
      InTouchTerminalDrawModel _terminal_draw_model = new InTouchTerminalDrawModel();

      // Set account id
      if (!this.SetAccountId(AccountId))
      {
        return;
      }

      // if transfer is in progress and there are no more pending terminal draws, than the transfer has finished
      if (this.SessionData.TransferInProgress && !_terminal_draw_model.GetPendingPlayDraws(this.SessionData.Account.Id, PromoGame.GameType.All))
      {
        this.SessionData.TransferInProgress = false;
        this.RedirectAction(TYPE_REQUEST.REDIRECT_ACTION, String.Format("{0}|{1}", TypeController.DataAccount, TypeView.DataAccountMenu));

        return;
      }

      this.SetActionFromManager(TypeController.DataAccount, TypeView.DataAccount);          
    } // CardInsertedValidated

    /// <summary>
    /// Function to check if exist pending terminals draw: Only in INSERT_CARD event
    /// </summary>
    /// <param name="DrawParams"></param>
    private void AskPendingDraw(String DrawParams)
    {
      TerminalDrawParams _result = new TerminalDrawParams(DrawParams);

      if (!_result.ParamsOk || _result.AccountId <= 0)
      {
        //this.SetActionFromManager(TypeController.Message, TypeView.CardError);
        this.SessionData.Message.Type = TYPE_MESSAGE_TO_SHOW.CardError;
        return;
      }

      // Set TerminalDraw & Account data
      this.SessionData.Account = new InTouchAccount(_result.AccountId);
      this.SessionData.TerminalDraw.DrawParams = _result.ToTerminalDrawModel();

      // Set GameParams
      this.SetGameParams(_result.GameId);     

      // Ask Participate on TerminalDraw manager
      this.AskParticipateDrawManager();
    } // AskPendingDraws

    /// <summary>
    /// Function to check if exist pending terminals draw: Only in INSERT_CARD event
    /// </summary>
    /// <param name="DrawParams"></param>
    private void ShowPendingDraw(String DrawParams)
    {
      TerminalDrawParams _result = new TerminalDrawParams(DrawParams);

      if (!_result.ParamsOk || _result.AccountId <= 0)
      {
        this.SessionData.Message.Type = TYPE_MESSAGE_TO_SHOW.CardError;
        //this.SetActionFromManager(TypeController.Message, TypeView.CardError);

        return;
      }

      // Set TerminalDraw & Account data
      this.SessionData.Account = new InTouchAccount(_result.AccountId);
      this.SessionData.TerminalDraw.DrawParams = _result.ToTerminalDrawModel();

      // Set GameParams
      this.SetGameParams(_result.GameId);     

      // Start Draw
      this.StartDrawManager();
    } // ShowPendingDraws

    /// <summary>
    /// Function to show the draw result
    /// </summary>
    /// <param name="DrawResult"></param>
    private void ShowDrawResult(Int32 SelectedItem, String DrawResult)
    {
      TerminalDrawParams _result;

      _result = new TerminalDrawParams(DrawResult);

      if (!_result.ParamsOk)
      {
        this.SessionData.Message.Type = TYPE_MESSAGE_TO_SHOW.GeneralError;
        //this.SetActionFromManager(TypeController.Message, TypeView.GeneralError);

        return;
      }

      this.SessionData.TerminalDraw.DrawParams = _result.ToTerminalDrawModel();
      this.SetActionFromManager(this.GetTypeControllerGameResult(), this.GetTypeViewGameResult(), SelectedItem);
    } // ShowDrawResult

    /// <summary>
    /// Function to show the draw result
    /// </summary>
    /// <param name="DrawResult"></param>
    private void EndPlayDraw()
    {
      this.SetActionFromManager(TypeController.PlayDraw, TypeView.PlayDrawFinished);
    } // EndPlayDraw

    /// <summary>
    /// To get the game result view
    /// </summary>
    /// <returns></returns>
    private String GetTypeViewGameResult()
    {
      // Manage next view
      switch (this.SessionData.TerminalDraw.GameParams.GetGameType())
      {
        case PromoGame.GameType.Welcome:
          return TypeView.WelcomeDrawResult;

        case PromoGame.GameType.PlayCash:
          return TypeView.PlayCashResult;

        case PromoGame.GameType.PlayReward:
          return TypeView.PlayRewardResult;

        case PromoGame.GameType.PlayPoint:
          return TypeView.PlayPointResult;
      }

      Log.Add(TYPE_MESSAGE.ERROR, "Error on GetTypeViewGameResult(). Unknown GameType.");

      return TypeView.Show;
    } // GetTypeViewGameResult

    /// <summary>
    /// To get the game result controller
    /// </summary>
    /// <returns></returns>
    private String GetTypeControllerGameResult()
    {
      // Manage next controller
      switch (this.SessionData.TerminalDraw.GameParams.GetGameType())
      {
        case PromoGame.GameType.Welcome:
          return TypeController.WelcomeDraw;

        case PromoGame.GameType.PlayCash:
          return TypeController.PlayCash;

        case PromoGame.GameType.PlayReward:
          return TypeController.PlayReward;

        case PromoGame.GameType.PlayPoint:
          return TypeController.PlayPoint;
      }

      Log.Add(TYPE_MESSAGE.ERROR, "Error on GetTypeControllerGameResult(). Unknown GameType.");

      return TypeController.Message;
    } // GetTypeControllerGameResult

    /// <summary>
    /// Card inserted not validated
    /// </summary>
    private void CardInsertedNotValidated(String ErrorStr)
    {
      this.SessionData.Account = new InTouchAccount();
      this.SessionData.ErrorMessage = ErrorStr;
      this.SessionData.TransferInProgress = false;

      this.SessionData.Message.Type = TYPE_MESSAGE_TO_SHOW.CardError;
    } // CardInsertedNotValidated

    /// <summary>
    /// Card removed validated
    /// </summary>
    private void CardRemovedValidated(String CardMessage)
    {
      String _view;
      String _controller;
      
      switch (this.SessionData.PreviousCardStatus)
      {
        case TYPE_CARD_STATUS.CARD_INSERTED_VALIDATED:
          _view = TypeView.DataAccount03;
          _controller = TypeController.DataAccount;
          break;

        case TYPE_CARD_STATUS.CARD_REMOVED_VALIDATED:
          this.SessionData.Message.Type = TYPE_MESSAGE_TO_SHOW.PendingLogout;
          this.SessionData.Message.Text = CardMessage;
          _view = TypeView.Show;
          _controller = TypeController.Message;
          break;

        default:
          _view = TypeView.Index;
          _controller = TypeController.Manager;
          break;
      }

      this.SessionData.PendingReset = true;

      this.SetActionFromManager(_controller, _view);
    } // CardRemovedValidated

    /// <summary>
    /// Card logout
    /// </summary>
    private void CardLogout()
    {
      // Show logout
      this.SetActionFromManager(TypeController.Logout, TypeView.LogOut);
    } // CardLogout

    /// <summary>
    /// Show Message (Insert Card).  
    /// </summary> 
    internal void CardInserted()
    {
      if (this.SessionData.CardStatus == TYPE_CARD_STATUS.CARD_INSERTED_VALIDATED)
      {
        Log.Add(TYPE_MESSAGE.INFO, "Return in CardInserted");

        return;
      }

      this.SetActionFromManager(TypeController.Validate, TypeView.Validate);
    } // CardInserted

    /// <summary>
    /// Show Message (Transferring).  
    /// </summary> 
    internal void TransferCredit()
    {
      this.SetActionFromManager(TypeController.Transfer, TypeView.Transfer);
    } // TransferCredit

    /// <summary>
    /// Method that sets InTouch Params into SessionData
    /// </summary>
    /// <param name="SASParams"></param>
    
    private void GetInTouchParams(String SASParams)
    {
      InTouchParamsModel _params;

      _params = new InTouchParamsModel().GetInTouchParameters(SASParams);

      if (!_params.CheckParams())
      {
        Log.Add(TYPE_MESSAGE.WARNING, "An error on get SASParams");

        return;
      }

      this.SessionData.SASParams = _params;
      this.SetLanguage(this.SessionData.SASParams.LanguageId);

      this.SetActionFromManager(TypeController.Manager, TypeView.Manager);
    } //GetInTouchParams
            
    /// <summary>
    /// Set account id in SessionData
    /// </summary>
    /// <param name="AccountId"></param>
    /// <returns></returns>
    private Boolean SetAccountId(String AccountId)
    {
      Int64 _account_id;

      if (!Int64.TryParse(AccountId, out _account_id))
      {
        this.SessionData.Message.Type = TYPE_MESSAGE_TO_SHOW.CardError;
        //this.SetActionFromManager(TypeController.Message, TypeView.CardError);

        return false;
      }

      if (!this.GetAccountData(_account_id))
      {
        this.SessionData.ErrorMessage = resources.nls_resource.ge_account_error + String.Format("(AccountId: {0})", _account_id);
        this.SessionData.Message.Type = TYPE_MESSAGE_TO_SHOW.GeneralError;
        this.SessionData.Message.Text = this.SessionData.ErrorMessage;
        //this.SetActionFromManager(TypeController.Message, TypeView.GeneralError);

        return false;
      }

      return true;
    } // SetAccountId

    /// <summary>
    /// To show the result of the transfer card credit to the machine
    /// </summary>
    private void ShowTransferCreditToMachineResult(int Result, String Data)
    {
      if ((TYPE_PROTOCOL_RESPONSE)Result == TYPE_PROTOCOL_RESPONSE.WCP_STATUS_WKT_CARD_OK)
      {
        this.SessionData.TransferInProgress = true;
        this.RedirectAction(TYPE_REQUEST.REDIRECT_ACTION, String.Format("{0}|{1}", TypeController.DataAccount, TypeView.DataAccount02));
      }
      else
      {
        this.SessionData.TransferInProgress = false;
        this.SessionData.Message.Type = TYPE_MESSAGE_TO_SHOW.TransferCreditError;
      }
    } // ShowTransferCreditToMachineResult

    /// <summary>
    /// Set Language
    /// </summary>
    private void SetLanguage(Int32 Language)
    {
      // Set Language
      this.SessionData.Language = this.GetLanguageById(Language);
          
      // Stay in the same view
      this.SetActionFromManager(this.SessionData.CurrentController, this.SessionData.CurrentAction);
    } // SetLanguage

    /// <summary>
    /// Get language
    /// </summary>
    /// <param name="Language"></param>
    /// <returns></returns>
    private String GetLanguageById(Int32 Language)
    {
      switch ((LanguageIdentifier)Language)
      {
        case LanguageIdentifier.Spanish:
          {
            return Constants.TYPE_LANGUAGE_SPANISH;
          }
          case LanguageIdentifier.French:
          {
              return Constants.TYPE_LANGUAGE_FRENCH;
          }
      }

      return Constants.TYPE_LANGUAGE_ENGLISH;
    } // GetLanguageById

    /// <summary>
    /// Redirect to action
    /// </summary>
    private void RedirectAction(TYPE_REQUEST FunctionCode, String Parameters)
    {
      String[] _parameters;
      String _view;
      String _controller;

      _parameters = Parameters.Split('|');

      if (_parameters.Length < 2)
      {
        Log.Add(TYPE_MESSAGE.ERROR, "Error on RedirectAction(). Invalid params.");

        return;
      }

      // Set View & Controller
      _controller = _parameters[0];
      _view = _parameters[1];

      // SET REDIRECTION
      if (!this.SessionData.Redirects.Manager(FunctionCode, ref _controller, ref _view))
      {
        this.SessionData.Message.Type = TYPE_MESSAGE_TO_SHOW.GeneralError;
      }

      // Set Current Action & Controller
      this.SessionData.UpdateCurrentAction(_controller, _view);

      // Redirect to action
      this.SetActionFromManager(_controller, _view);
      
    } // RedirectAction

    #endregion
        
  } // InTouchProtocol
}