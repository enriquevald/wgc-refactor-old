﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//      MODULE NAME: InTouchRedirects.cs
// 
//      DESCRIPTION: Class to manage the redirections
// 
//           AUTHOR: David Perelló
// 
//    CREATION DATE: 19-OCT-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 19-OCT-2017 DPC    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LCDInTouch.Web.Classes
{
  public class InTouchRedirects
  {

    #region Elements

    private List<InTouchAction> m_actions;
    private List<InTouchAction> m_list_excluded_actions;

    #endregion

    #region " Contructors "

    /// <summary>
    /// Constructor
    /// </summary>
    public InTouchRedirects()
    {
      this.m_actions = new List<InTouchAction>();
      this.m_list_excluded_actions = new List<InTouchAction>();

      //Exculded views to redirect
      m_list_excluded_actions.Add(new InTouchAction { Controller = TypeController.Pin, Action = TypeView.PinAuthentication });
      m_list_excluded_actions.Add(new InTouchAction { Controller = TypeController.Pin, Action = TypeView.PinNewPin });

    } // InTouchRedirects

    #endregion // Contructors

    #region "Public methods"

    /// <summary>
    /// Manager of redirections
    /// </summary>
    /// <param name="TypeRequest"></param>
    /// <param name="CurrentController"></param>
    /// <param name="CurrentAction"></param>
    public Boolean Manager(TYPE_REQUEST TypeRequest, ref String CurrentController, ref String CurrentAction)
    {
      try
      {
        if (CurrentController == InTouchConstants.DEFAULT_CONTROLLER_AFTER_INSERT_CARD && CurrentAction == InTouchConstants.DEFAULT_VIEW_AFTER_INSERT_CARD)
        {
          this.m_actions = new List<InTouchAction>();
        }

        switch (TypeRequest)
        {
          case TYPE_REQUEST.REDIRECT_ACTION:

            //If the view is in the list of excludes, not add
            if (!CheckIfViewPreviousExcluded(CurrentController, CurrentAction))
            {
              this.Add(CurrentController, CurrentAction);
            }

            break;

          case TYPE_REQUEST.REDIRECT_PREVIOUS_ACTION:

            InTouchAction _action = SelectCurrentAction();

            this.RemoveCurrentAction(_action);

            _action = SelectCurrentAction();

            CurrentController = _action.Controller;
            CurrentAction = _action.Action;

            break;

        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, String.Format("InTouchRedirects.Manager --> {0}", _ex.Message));
        Log.Add(_ex);
      }

      return false;
    } // Manager

    #endregion // Public methods

    #region " Private methods "

    /// <summary>
    /// Add redirection to object
    /// </summary>
    /// <param name="CurrentController"></param>
    /// <param name="CurrentAction"></param>
    private void Add(String CurrentController, String CurrentAction)
    {
      InTouchAction _current_action;

      try
      {
        _current_action = SelectCurrentAction();

        if (_current_action != null && _current_action.Controller == CurrentController && _current_action.Action == CurrentAction)
        {
          return;
        }

        this.m_actions.Add(new InTouchAction(CurrentController, CurrentAction));
      }
      catch (Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, String.Format("InTouchRedirects.Add --> {0}", _ex.Message));
        Log.Add(_ex);
      }

    } // Add

    /// <summary>
    /// Remove redirection to object
    /// </summary>
    /// <param name="CurrentAction"></param>
    private void RemoveCurrentAction(InTouchAction CurrentAction)
    {
      try
      {
        if (CurrentAction != null)
        {
          this.m_actions.Remove(CurrentAction);
        }
      }
      catch (Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, String.Format("InTouchRedirects.RemoveCurrentAction --> {0}", _ex.Message));
        Log.Add(_ex);
      }
    } // RemoveCurrentAction

    /// <summary>
    /// Select the current redirection
    /// </summary>
    /// <returns></returns>
    private InTouchAction SelectCurrentAction()
    {
      try
      {

        return this.m_actions.LastOrDefault();
      }
      catch (Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, String.Format("InTouchRedirects.SelectCurrentAction --> {0}", _ex.Message));
        Log.Add(_ex);
      }

      return null;
    } // SelectCurrentAction

    /// <summary>
    /// Check if the redirect is excluded to save in session data
    /// </summary>
    /// <param name="CurrentController"></param>
    /// <param name="CurrentAction"></param>
    /// <returns></returns>
    private Boolean CheckIfViewPreviousExcluded(String CurrentController, String CurrentAction)
    {
      try
      {

        return (m_list_excluded_actions.Where(_item => _item.Controller == CurrentController && _item.Action == CurrentAction).Count() > 0);
      }
      catch (Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, String.Format("InTouchRedirects.CheckIfViewPreviousExcluded --> {0}", _ex.Message));
        Log.Add(_ex);
      }

      return false;
    } // CheckIfViewPreviousExcluded

    #endregion // Private methods
    
  } // InTouchRedirects
}