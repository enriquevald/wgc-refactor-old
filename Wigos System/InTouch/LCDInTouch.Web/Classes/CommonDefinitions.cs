﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//      MODULE NAME: CommonDefinitions.cs
// 
//      DESCRIPTION: Common definitions class for in touch web
// 
//           AUTHOR: Javier Barea
// 
//    CREATION DATE: 06-JUL-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 06-JUL-2017 JBP    First release.
//------------------------------------------------------------------------------
using System;

namespace LCDInTouch.Web.Classes
{

  #region " Constants "

  public class InTouchConstants 
  {
    public const String K_SessionDataSession        = "SessionData";
    public const Int32 K_TimeInSeconds              = 5;
    public const Int32 K_TimeInSecondsDraw          = 10;
    public const Int32 K_TimeInSecondsPendingLogout = 1;

    public const String DEFAULT_LOCALHOST_URL = "http://localhost:1092";
    public const String DEFAULT_SITE_NAME     = "LCDInTouch";

    public const Int32 NAME_MAX_LENGTH        = 80;

    public const String DEFAULT_CONTROLLER_AFTER_INSERT_CARD = TypeController.DataAccount;
    public const String DEFAULT_VIEW_AFTER_INSERT_CARD = TypeView.DataAccount02;

    public const Int32 MAX_RETRIES_PIN = 3;

  } // InTouchConstants

  #endregion

  #region " Enums "

  public class TypeView
  {
    public const string Manager                  = "Manager";
    public const string Index                    = "Index";
    public const string Validate                 = "Validate";
    public const string DataAccount              = "DataAccount";
    public const string DataAccount01            = "DataAccount01";
    public const string DataAccount02            = "DataAccount02";
    public const string DataAccount03            = "DataAccount03";
    public const string DataAccountMenu          = "DataAccountMenu";
    public const string DataAccountStatus        = "DataAccountStatus";
    public const string PlayDrawCanParticipate   = "PlayDrawCanParticipate"; 
    public const string ShowBuyDialog            = "ShowBuyDialog";
    public const string PlayDrawMenu             = "PlayDrawMenu";
    public const string PlayDrawGame             = "PlayDrawGame";
    public const string PlayDrawBuy              = "PlayDrawBuy";
    public const string PlayDrawInsert           = "PlayDrawInsert";
    public const string PlayDrawNoCredit         = "PlayDrawNoCredit";
    public const string PlayDrawFinished         = "PlayDrawFinished";
    public const string WelcomeDrawStart         = "WelcomeDrawStart";
    public const string WelcomeDrawResult        = "WelcomeDrawResult";    
    public const string PlayCashStart            = "PlayCashStart";   
    public const string PlayCashResult           = "PlayCashResult";
    public const string PlayRewardMenu           = "PlayRewardMenu";
    public const string PlayRewardStart          = "PlayRewardStart";
    public const string PlayRewardResult         = "PlayRewardResult";
    public const string PlayPointMenu            = "PlayPointMenu";
    public const string PlayPointStart           = "PlayPointStart";
    public const string PlayPointResult          = "PlayPointResult";
    public const string PinAuthentication        = "PinAuthentication";    
    public const string PinNewPin                = "PinNewPin";
    public const string GiftConfirmGiftBuy       = "ConfirmGiftBuy";
    public const string Transfer                 = "Transfer";
    public const string Transfer03               = "Transfer03";
    public const string TransferCredit           = "TransferCredit";
    public const string LogOut                   = "LogOut";
    public const string Show                     = "Show";

  } // TypeView

  public class TypeController
  {
    public const string Base            = "Base";
    public const string Manager         = "Manager";
    public const string Validate        = "Validate";
    public const string Message         = "Message";
    public const string DataAccount     = "DataAccount";
    public const string PlayDraw        = "PlayDraw";
    public const string WelcomeDraw     = "WelcomeDraw";
    public const string PlayCash        = "PlayCash";
    public const string PlayReward      = "PlayReward";
    public const string PlayPoint       = "PlayPoint";
    public const string Transfer        = "Transfer";
    public const string TransferCredit  = "TransferCredit";
    public const string Logout          = "LogOut";
    public const string Pin             = "Pin";
    public const string Gift            = "Gift";

  } // TypeController

  public class TypeIcon
  {
    public const string Check      = "Check.png";
    public const string Warning   = "Warning.png";
  }

  public enum TYPE_MESSAGE_TO_SHOW
  {
    None                  = 0,
    GeneralError          = 1,
    CardError             = 2,
    PinError              = 3, 
    PinLocked             = 4,
    PinNotMatch           = 5,
    PinInfoOk             = 6, 
    TransferCreditError   = 7,
    PendingLogout         = 8

  } // TYPE_ERROR

  #endregion

}