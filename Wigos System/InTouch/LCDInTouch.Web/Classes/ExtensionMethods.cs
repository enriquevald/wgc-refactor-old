﻿using LCDInTouch.Web.Models;
//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//      MODULE NAME: ExtensionMethods.cs
// 
//      DESCRIPTION: InTouch Extension Methods
// 
//           AUTHOR: Javi Barea
// 
//    CREATION DATE: 27-JUL-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 27-JUL-2017 JBP    First release.
//------------------------------------------------------------------------------
using System;

namespace LCDInTouch.Web.Classes
{
  public static class ExtensionMethods
  {

    #region " InTouchTerminalDrawParamsModel "

    /// <summary>
    /// Mapp TerminalDrawParams to end InTouchTerminalDrawResultModel
    /// </summary>
    /// <param name="Request"></param>
    /// <returns></returns>
    public static InTouchTerminalDrawParamsModel ToTerminalDrawModel(this TerminalDrawParams TerminalDrawParams)
    {
      // This method only calls when Game is processed from SASHost on Insert Card. 
      return new InTouchTerminalDrawParamsModel()
      {
        PlaySessionId = 0,  // Managed in SASHost
        AccountId     = TerminalDrawParams.AccountId,
        AutoDraw      = TerminalDrawParams.AutoDraw,
        Url           = TerminalDrawParams.Url,
        IsForced      = TerminalDrawParams.IsForced,
        IsWinner      = TerminalDrawParams.IsWinner,
        ParamsOk      = TerminalDrawParams.ParamsOk,
        RunDraw       = TerminalDrawParams.RunDraw,
        ShowResult    = TerminalDrawParams.ShowResult,
        TimeOut       = (Int32)(TerminalDrawParams.TimeOut / 1000),
        Balance       = (Decimal)TerminalDrawParams.Balance / 100,
        BetAmount     = (Decimal)TerminalDrawParams.BetAmount / 100,
        WinAmount     = (Decimal)TerminalDrawParams.WinAmount / 100,
        NewBalance    = TerminalDrawParams.NewBalance / 100,
        GameId        = TerminalDrawParams.GameId,
        GameType      = TerminalDrawParams.GameType
      };

    } // ToTerminalDrawModel

    #endregion

  } // Extension Methods
  
}