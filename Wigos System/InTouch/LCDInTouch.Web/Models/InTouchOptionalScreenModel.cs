﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//      MODULE NAME: InTouchOptionalScreenModel.cs
// 
//      DESCRIPTION: Model for set dvalues un optional screen
// 
//           AUTHOR: Ferran Ortner
// 
//    CREATION DATE: 20-JUL-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 20-JUL-2017 FOS    First release.
//------------------------------------------------------------------------------
using System;

namespace LCDInTouch.Web.Models
{
  public class InTouchOptionalScreenModel
  {

    #region " Constants "

    public const Int32 K_BUTTON_VISIBLE = 1;
    public const Int32 K_BUTTON_NO_VISIBLE = 2;

    #endregion

    #region " Properties "

    public InTouchOptionalQuestionScreenModel Question { get; set; }
    public InTouchOptionalDescriptionScreenModel Description { get; set; }      
    public InTouchOptionalButtonScreenModel AcceptButton { get; set; }
    public InTouchOptionalButtonScreenModel CancelButton { get; set; }    
    public Int32 ButtonCancelVisible{ get; set; }
    
    #endregion

    #region " Constructor "

    public InTouchOptionalScreenModel()
    {
      this.Question     = new InTouchOptionalQuestionScreenModel();
      this.Description  = new InTouchOptionalDescriptionScreenModel();
      this.AcceptButton = new InTouchOptionalButtonScreenModel();
      this.ButtonCancelVisible = K_BUTTON_NO_VISIBLE;
    } // InTouchOptionalScreenModel
    public InTouchOptionalScreenModel(InTouchOptionalQuestionScreenModel Question,
                                      InTouchOptionalButtonScreenModel AcceptButton,
                                      InTouchOptionalDescriptionScreenModel Description)
    {
      this.Question = Question;
      this.Description = Description;
      this.AcceptButton = AcceptButton;
      this.ButtonCancelVisible = K_BUTTON_NO_VISIBLE;
    } // InTouchOptionalScreenModel
    public InTouchOptionalScreenModel(InTouchOptionalQuestionScreenModel Question,
                                      InTouchOptionalButtonScreenModel AcceptButton,
                                      InTouchOptionalButtonScreenModel CancelButton,
                                      InTouchOptionalDescriptionScreenModel Description)
    {
      this.Question = Question;
      this.Description = Description;
      this.AcceptButton = AcceptButton;
      this.CancelButton = CancelButton;
      this.ButtonCancelVisible = K_BUTTON_VISIBLE;
    } // InTouchOptionalScreenModel
    
    #endregion

  } // InTouchOptionalScreenModel

  /// <summary>
  /// Class to Inform the propeties of the button in the partial View
  /// </summary>
  public class InTouchOptionalButtonScreenModel
  {

    #region " Constants "

    private const String K_REDIRECT_ACTION_OPTIONAL_BUTTON = "RedirectAction('{0}', '{1}');";

    #endregion

    #region " Properties "

    public String Text { get; set; }
    public String RedirectAction { get; set; }
    public String InteractWithHostParams { get; set; }

    #endregion

    #region " Constructor "

    public InTouchOptionalButtonScreenModel()
    {
      this.Text = String.Empty;
      this.RedirectAction = String.Empty;
      this.InteractWithHostParams = String.Empty;
    } // InTouchOptionalButtonScreenModel

    public InTouchOptionalButtonScreenModel(String Text, String RedirectController, String RedirectView)
    {
      this.Text = Text;
      this.RedirectAction = String.Format(K_REDIRECT_ACTION_OPTIONAL_BUTTON, RedirectController, RedirectView);
      this.InteractWithHostParams = String.Empty;
    } // InTouchOptionalButtonScreenModel

    public InTouchOptionalButtonScreenModel(String Text, String InteractWithHostParams)
    {
      this.Text = Text;
      this.RedirectAction = String.Empty;
      this.InteractWithHostParams = InteractWithHostParams;
    } // InTouchOptionalButtonScreenModel
    
    #endregion

  } // InTouchOptionalButtonScreenModel
  
  /// <summary>
  /// Class to Set the question that show in the partial View
  /// </summary>
  public class InTouchOptionalQuestionScreenModel
  {

    #region " Properties "

    public String Description { get; set; }
    public String DescriptionStyle { get; set; }
    
    #endregion 

    #region " Constructor "

    public InTouchOptionalQuestionScreenModel()
    {
      this.Description = String.Empty;
      this.DescriptionStyle = string.Empty;
    } // InTouchOptionalQuestionScreenModel
    public InTouchOptionalQuestionScreenModel(String Description , String DescriptionBold = "", String DescriptionStyle ="")
    {
      this.Description = Description;
      this.DescriptionStyle = DescriptionStyle;

      if(!String.IsNullOrEmpty(DescriptionBold))
      {
        this.Description = String.Format(Description, DescriptionBold);
      }
    } // InTouchOptionalQuestionScreenModel

    #endregion 

  } // InTouchOptionalQuestionScreenModel
  
  /// <summary>
  /// Class to Set the Description or extra explanations in the partial View
  /// </summary>
  public class InTouchOptionalDescriptionScreenModel
  {

    # region " Properties "

    public String FirstLine { get; set; }
    public String SecondLine { get; set; }
    public String ThirdLine { get; set; }

    #endregion

    #region " Constructor "

    public InTouchOptionalDescriptionScreenModel()
    {
      this.FirstLine = String.Empty;
      this.SecondLine = String.Empty;
      this.ThirdLine = String.Empty;
    } // InTouchOptionalDescriptionScreenModel
    public InTouchOptionalDescriptionScreenModel(String FirstLine , String SecondLine , String ThirdLine )
    {
      this.FirstLine  = FirstLine;
      this.SecondLine = SecondLine;
      this.ThirdLine  = ThirdLine; 
    } // InTouchOptionalDescriptionScreenModel

    #endregion

  } // InTouchOptionalDescriptionScreenModel
}