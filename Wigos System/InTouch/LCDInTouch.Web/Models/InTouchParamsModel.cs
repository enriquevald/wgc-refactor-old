﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//      MODULE NAME: InTouchParamsModel.cs
// 
//      DESCRIPTION: Model for params inTouchWeb
// 
//           AUTHOR: Ferran Ortner
// 
//    CREATION DATE: 10-JUL-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 10-JUL-2017 FOS    First release.
//------------------------------------------------------------------------------
using System;

namespace LCDInTouch.Web.Models
{
  public class InTouchParamsModel
  {

    #region " Constants "

    private const int NUM_SAS_PARAMS = 6;
    
    private const int SAS_PARAM_SUBSYSTEM_MODE = 0;
    private const int SAS_PARAM_LANGUAGE_ID = 1;
    private const int SAS_PARAM_TERMINAL_NAME = 2;
    private const int SAS_PARAM_TERMINAL_ID = 3;
    private const int SAS_PARAM_WELCOMEMESSAGELINE1 = 4;
    private const int SAS_PARAM_WELCOMEMESSAGELINE2 = 5;

    #endregion 

    #region " Property "

    public Int32 SubsystemMode { get; set; }
    public Int32 LanguageId { get; set; }
    public String TerminalName { get; set; }
    public Int32 TerminalId { get; set; }
    public InTouchFunctionalitiesModel Functionalities { get; set; }
    public String WelcomeMessageLine1 { get; set; }
    public String WelcomeMessageLine2 { get; set; }
    
    #endregion

    #region " Constructor "

    /// <summary>
    /// Default Constructor
    /// </summary>
    public InTouchParamsModel()
    {
      SubsystemMode = 0;
      LanguageId = 0;
      TerminalName = String.Empty;
      TerminalId = 0;
      Functionalities = new InTouchFunctionalitiesModel();
      WelcomeMessageLine1 = String.Empty;
      WelcomeMessageLine2 = String.Empty;
    } // InTouchParamsModel
    
    #endregion

    #region " Publics Methods "
    
    /// <summary>
    /// Get InTouch Parameters from DB
    /// </summary>
    /// <param name="SASParams"></param>
    /// <returns></returns>
    public InTouchParamsModel GetInTouchParameters(String SASParams)
    {
      Int32 _subsystem_mode;
      Int32 _language_id;
      String _terminal_name;
      Int32 _terminal_id;
      String _welcome_message_line_1;
      String _welcome_message_line_2;

      try
      {
        String[] _sas_params = SASParams.Split('|');

        if (_sas_params.Length != NUM_SAS_PARAMS)
        {
          Log.Add(TYPE_MESSAGE.ERROR, "Error on GetInTouchParameters(): Invalid SASParams.");

          return new InTouchParamsModel();
        }

        // System Mode (Tito/Cashless)
        if (!int.TryParse(_sas_params[SAS_PARAM_SUBSYSTEM_MODE], out _subsystem_mode))
        {
          _subsystem_mode = 0;
        }

        // Language
        if (!int.TryParse(_sas_params[SAS_PARAM_LANGUAGE_ID], out _language_id))
        {
          _language_id = 0;
        }

        // Terminal Name
        _terminal_name = String.Empty;
        if (!String.IsNullOrEmpty(_sas_params[SAS_PARAM_TERMINAL_NAME]))
        {
          _terminal_name = _sas_params[SAS_PARAM_TERMINAL_NAME];
        }

        // Terminal Id
        if (!int.TryParse(_sas_params[SAS_PARAM_TERMINAL_ID], out _terminal_id))
        {
          _terminal_id = 0;
        }

        Log.Add(TYPE_MESSAGE.INFO, "In SAS_PARAM_WELCOMEMESSAGELINE1");

        // Welcome Message Line 1
        _welcome_message_line_1 = String.Empty;
        if (!String.IsNullOrEmpty(_sas_params[SAS_PARAM_WELCOMEMESSAGELINE1]))
        {
          _welcome_message_line_1 = _sas_params[SAS_PARAM_WELCOMEMESSAGELINE1];

          Log.Add(TYPE_MESSAGE.INFO, "Dentro SAS_PARAM_WELCOMEMESSAGELINE1 " + _welcome_message_line_1);
        }

        Log.Add(TYPE_MESSAGE.INFO, "Out SAS_PARAM_WELCOMEMESSAGELINE1 " + _welcome_message_line_1);

        Log.Add(TYPE_MESSAGE.INFO, "In SAS_PARAM_WELCOMEMESSAGELINE2");

        // Welcome Message Line 2
        _welcome_message_line_2 = String.Empty;
        if (!String.IsNullOrEmpty(_sas_params[SAS_PARAM_WELCOMEMESSAGELINE2]))
        {
          _welcome_message_line_2 = _sas_params[SAS_PARAM_WELCOMEMESSAGELINE2];

          Log.Add(TYPE_MESSAGE.INFO, "Dentro SAS_PARAM_WELCOMEMESSAGELINE2 " + _welcome_message_line_2);
        }

        Log.Add(TYPE_MESSAGE.INFO, "Out SAS_PARAM_WELCOMEMESSAGELINE2");

        return new InTouchParamsModel()
        {
          SubsystemMode = _subsystem_mode,
          LanguageId = _language_id,
          TerminalName = _terminal_name,
          TerminalId = _terminal_id,
          WelcomeMessageLine1 = _welcome_message_line_1,
          WelcomeMessageLine2 = _welcome_message_line_2
        };
      }
      catch (Exception Ex)
      {
        Log.Add(Ex);
      }

      return new InTouchParamsModel();
    }// GetInTouchParameters

    /// <summary>
    /// Check properties values
    /// </summary>
    public Boolean CheckParams()
    {
      // Terminal Id
      if (this.TerminalId <= 0)
      {
        return false;
      }

      // Terminal Name
      if (String.IsNullOrEmpty(this.TerminalName))
      {
        return false;
      }

      // Language Id
      if (this.LanguageId <= 0)
      {
        return false;
      }

      // SubSystemMode
      if (this.SubsystemMode < 0)
      {
        return false;
      }

      // Functionalities
      if (this.Functionalities.NumFunctionalities == 0)
      {
        //return false;
      }

      return true;
    } // CheckParams

    #endregion
    
  }
}