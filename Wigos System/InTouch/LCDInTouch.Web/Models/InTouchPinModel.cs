﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//      MODULE NAME: InTouchPinModel.cs
// 
//      DESCRIPTION: InTouch Pin Model 
// 
//           AUTHOR: Rubén Lama Ordóñez
// 
//    CREATION DATE: 07-AGO-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 07-AGO-2017 RLO    First release.
//------------------------------------------------------------------------------
using System;
using WSI.Common;

namespace LCDInTouch.Web.Models
{
  public class InTouchPinModel
  {

    #region " Constants "

    public const Int32 DEFAULT_NUM_PIN_TRIES = 0;

    #endregion

    #region  " Property "

    //Params
    public Int32 ValidPinTimeOut { get; set; }
    public Boolean NeverRequestForPIN { get; set; }

    public Boolean PinOk { get; set; }
    public DateTime LastPinOk { get; set; }
    public Int32 NumRetries { get; set; }
    public Boolean ChangePin { get; set; }

    #endregion 

    #region " Constructor "

    public InTouchPinModel()
    {
      this.NeverRequestForPIN = GeneralParam.GetBoolean("DisplayTouch", "NeverRequestForPIN");
      this.NumRetries = DEFAULT_NUM_PIN_TRIES;
      this.LastPinOk = WGDB.MinDate;
      this.ValidPinTimeOut = GeneralParam.GetInt32("DisplayTouch", "ValidPinTimeout");
      this.PinOk = false;
    } // InTouchPinModel

    public InTouchPinModel(Boolean test)
    {
      this.NeverRequestForPIN = true;
      this.NumRetries = DEFAULT_NUM_PIN_TRIES;
      this.LastPinOk = WGDB.MinDate;
      this.ValidPinTimeOut = 0;
      this.PinOk = false;
    } // InTouchPinModel

    #endregion

    #region "Public Functions"

    /// <summary>
    /// Return if pin time out for ask again pin
    /// </summary>
    /// <returns></returns>
    public Boolean CheckIfPinTimeOutPass()
    {
      return this.LastPinOk.AddSeconds(this.ValidPinTimeOut) < WGDB.Now;
    } //CheckIfPinTimeOutPass

    /// <summary>
    /// Method to check pin config
    /// </summary>
    /// <returns></returns>
    public Boolean CheckPinConfig()
    {
      this.NeverRequestForPIN = true ; //TODO RLO when param is loaded correctly erase this line
      this.ValidPinTimeOut = 40;

      // If NeverRequestForPin = false Always demmand pin
      if (this.NeverRequestForPIN)
      {
        return false;
      }
      else if (this.ValidPinTimeOut > 0) // If CheckIfPinTimeOutPass = false
      {
        return this.CheckIfPinTimeOutPass();
      }

      return true;
    } // CheckPinConfig

    #endregion

  }
}