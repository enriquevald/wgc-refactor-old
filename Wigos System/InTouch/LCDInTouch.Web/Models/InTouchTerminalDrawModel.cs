﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//      MODULE NAME: InTouchTerminalDrawModel.cs
// 
//      DESCRIPTION: InTouch terminal draw model Model 
// 
//           AUTHOR: Ferran Ortner
// 
//    CREATION DATE: 10-JUL-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 10-JUL-2017 FOS    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using WSI.Common;
using WSI.Common.TerminalDraw.Persistence;

namespace LCDInTouch.Web.Models
{
  public class InTouchTerminalDrawModel
  {

    #region " Constants "

    private const Int32 DB_TDR_GAME_ID = 0;

    #endregion

    #region  " Property "

    public Boolean Enabled { get; set; }
    public Boolean ShowButtonToCancel { get; set; }
    public Boolean ShowBuyDialog { get; set; }

    public Boolean HasTerminalDrawPending { get; set; }
    public Int64 NumberPlayCashDrawPending { get; set; }
    public Boolean HasPlayDrawPending { get; set; }
    public Boolean FromAskParticipate { get; set; }
    public InTouchTerminalDrawParamsModel DrawParams { get; set; }
    public List<PromoGame> GameParams { get; set; }
    public TerminalDrawAccountEntity AccountEntity { get; set; }
    
    #endregion

    #region " Constructor "

    public InTouchTerminalDrawModel()
    {
      this.Enabled            = Misc.IsTerminalDrawEnabled();
      this.ShowBuyDialog      = GeneralParam.GetBoolean("CashDesk.DrawConfiguration.02", "ShowTheBuyDialog", false);
      this.ShowButtonToCancel = GeneralParam.GetBoolean("CashDesk.DrawConfiguration.02", "PlayerCanCancelDraw", false);
      this.DrawParams         = new InTouchTerminalDrawParamsModel();
      this.AccountEntity      = new TerminalDrawAccountEntity();
    } // InTouchTerminalDrawModel

    #endregion

    #region "Public Methods"

    /// <summary>
    /// Check if there are pending play draws
    /// </summary>
    /// <param name="AccountId"></param>
    /// <param name="Type"></param>
    /// <returns></returns>
    public Boolean GetPendingPlayDraws(Int64 AccountId, PromoGame.GameType Type)
    {
      List<PromoGame> _promo_games;

      return this.GetPendingPlayDraws(AccountId, 0, Type, out _promo_games);
    } // GetPendingPlayDraws
    public Boolean GetPendingPlayDraws(Int64 AccountId, PromoGame.GameType Type, out List<PromoGame> PromoGames)
    {
      return this.GetPendingPlayDraws(AccountId, 0, Type, out PromoGames);
    } // GetPendingPlayDraws
    public Boolean GetPendingPlayDraws(Int64 AccountId, Int64 GameId, PromoGame.GameType Type, out List<PromoGame> PromoGames)
    {
      return this.DB_GetPendingPlayDraws(AccountId, GameId, Type, out PromoGames);
    } // GetPendingPlayDraws

    #endregion

    #region "Private Methods"  

    private String PendingDraws_Query(PromoGame.GameType Type, Int64 GameId)
    {
      if(Type == PromoGame.GameType.All)
      {
        return this.PendingDraws_All();
      }

      return this.PendingDraws_ByGameId(GameId);
    } // PendingDraws_Query

    /// <summary>
    /// Generate a query to get pending draws by game id
    /// </summary>
    /// <param name="GameId"></param>
    /// <returns></returns>
    private String PendingDraws_ByGameId(Int64 GameId)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine("   DECLARE  @pGameId AS BIGINT  ");

      if (GameId > 0)
      {
        _sb.AppendLine(String.Format("   SET  @pGameId = {0}   ", GameId));
      }
      else
      {
        _sb.AppendLine("    SELECT  TOP(1) @pGameId = TDR_GAME_ID ");
        _sb.AppendLine("      FROM  TERMINAL_DRAWS_RECHARGES      ");
        _sb.AppendLine("     WHERE  TDR_ACCOUNT_ID  = @pAccountId ");
        _sb.AppendLine("       AND  TDR_GAME_TYPE   = @pGameType  ");
        _sb.AppendLine("  ORDER BY  TDR_ID ASC                    ");
      }

      _sb.AppendLine("      SELECT  TOP 5 TDR_GAME_ID             ");
      _sb.AppendLine("        FROM  TERMINAL_DRAWS_RECHARGES      ");
      _sb.AppendLine("       WHERE  TDR_ACCOUNT_ID = @pAccountId  ");
      _sb.AppendLine("         AND  TDR_GAME_TYPE  = @pGameType   ");
      _sb.AppendLine("         AND  TDR_GAME_ID    = @pGameId     ");

      return _sb.ToString();
    } // PendingDraws_ByGameId

    /// <summary>
    /// Generate a query to get all pending draws
    /// </summary>
    /// <returns></returns>
    private String PendingDraws_All()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine("      SELECT  TDR_GAME_ID                   ");
      _sb.AppendLine("        FROM  TERMINAL_DRAWS_RECHARGES      ");
      _sb.AppendLine("       WHERE  TDR_ACCOUNT_ID = @pAccountId  ");

      return _sb.ToString();
    } // PendingDraws_ByGameId

    /// <summary>
    /// Get all play draws of Same GameId
    /// </summary>
    /// <param name="AccountId"></param>
    /// <param name="GameId"></param>
    /// <param name="Type"></param>
    /// <param name="PromoGames"></param>
    /// <returns></returns>
    private Boolean DB_GetPendingPlayDraws(Int64 AccountId, Int64 GameId, PromoGame.GameType Type, out List<PromoGame> PromoGames)
    {
      PromoGame _game;

      _game = null;
      PromoGames = new List<PromoGame>();

      try
      {
        // IMPORTANT: ONLY FOR SAME GAMEID

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(this.PendingDraws_Query(Type, GameId), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pAccountId" , SqlDbType.BigInt).Value = AccountId;
            _cmd.Parameters.Add("@pGameType"  , SqlDbType.BigInt).Value = (Int64)Type;

            using (SqlDataReader _sql_reader = _cmd.ExecuteReader())
            {
              while (_sql_reader.Read())
              {
                // Set game: All games must are the same.
                if (_game == null)
                {
                  // Get PromoGame
                  _game = new PromoGame(_sql_reader.GetInt64(DB_TDR_GAME_ID))
                  {
                    Type = Type 
                  };
                }

                // Add PromoGame
                PromoGames.Add(_game);
              }

              this.NumberPlayCashDrawPending = PromoGames.Count;
              this.HasPlayDrawPending = (this.NumberPlayCashDrawPending > 0);

              return this.HasPlayDrawPending;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, String.Format("InTouchTerminalDrawModel.DB_GetPendingPlayDraws -> AccountId: {0}", AccountId));
        Log.Add(_ex);
      }

      return false;
    } // DB_GetPendingPlayDraws

    #endregion

  }
}