﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//      MODULE NAME: InTouchPointsModel.cs
// 
//      DESCRIPTION: Model to Set the Session Data Points configuration
// 
//           AUTHOR: Ferran Ortner
// 
//    CREATION DATE: 29-SEP-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 29-SEP-2017 FOS    First release.
//------------------------------------------------------------------------------
using System;
using WSI.Common;

namespace LCDInTouch.Web.Models
{
  public class InTouchPointsModel
  {

    #region " Properties "

    public Boolean VisibilityPointsAtInsertAndRemoveCard { get; set; }
    public String LabelPointsText { get; set; }
    public String LabelPointsForLevelText { get; set; }
    public String LabelPointsText_EN { get; set; }
    public String LabelPointsForLevelText_EN { get; set; }

    #endregion

    #region " Constructor "

    public InTouchPointsModel()
    {
      this.VisibilityPointsAtInsertAndRemoveCard = GeneralParam.GetBoolean("DisplayTouch", "DisplayPointsAtInsertAndRemoveCard", true);
      this.LabelPointsText = GeneralParam.GetString("DisplayTouch", "LabelForPoints_ES", "Puntos");
      this.LabelPointsText_EN = GeneralParam.GetString("DisplayTouch", "LabelForPoints_EN", "Points");
      this.LabelPointsForLevelText = GeneralParam.GetString("DisplayTouch", "LabelForLevelPoints_ES", "Puntos nivel");
      this.LabelPointsForLevelText_EN = GeneralParam.GetString("DisplayTouch", "LabelForLevelPoints_EN", "Level points");
    } // InTouchPointsModel
    
    #endregion

  }
}