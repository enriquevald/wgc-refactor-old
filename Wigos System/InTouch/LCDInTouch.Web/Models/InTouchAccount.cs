
//------------------------------------------------------------------------------
// Copyright � 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//      MODULE NAME: InTouchAccount.cs
// 
//      DESCRIPTION: Model InTouch Account
// 
//           AUTHOR: Rub�n Lama
// 
//    CREATION DATE: 11-JUL-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 11-JUL-2017 RLO    First release.
//------------------------------------------------------------------------------
using LCDInTouch.Web.Classes;
using System;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using WSI.Common;

namespace LCDInTouch.Web.Models
{

  #region " Class Balance "

  public class Balance : ICloneable
  {

    #region Members

    public String BalanceStr;
    public String BalanceRStr;
    public String BalanceNRStr;
    public String BalanceNotReservedStr;
    public String BalanceRReservedStr;
    public String PointsStr;

    public Int64 BalanceTotal;
    public Int64 BalanceR;
    public Int64 BalanceRPromo;
    public Int64 BalanceNR;
    public Int64 BalanceRReserved;
    public Int64 Points;
 
    #endregion

    #region Public Functions

    public Balance()
    {
      this.BalanceStr = "";
      this.BalanceNotReservedStr = "";
      this.BalanceRStr = "";
      this.BalanceNRStr = "";
      this.PointsStr = "";
      this.BalanceTotal = 0;
      this.BalanceR = 0;
      this.BalanceRPromo = 0;
      this.BalanceNR = 0;
      this.BalanceRReserved = 0;
      this.Points = -1;
    } // Balance

    #endregion

    #region " ICloneable Members "

    public object Clone()
    {
      return this.MemberwiseClone();
    } // Clone

    #endregion

  } // Balance

  #endregion

  #region Gifts Class

  //public class Gifts
  //{
  //  #region Members

  //  private Boolean m_request_pending;
  //  private Boolean m_request_in_progress;

  //  private LCD_PLAYER_GIFTS m_all_lists;
  //  private List<LCD_ITEM> m_current_gifts_list;
  //  private List<LCD_ITEM> m_next_gifts_list;
  //  private List<LCD_ITEM> m_credits_gifts_list;

  //  #endregion

  //  #region Properties

  //  public Boolean ResponsePending
  //  {
  //    get { return this.m_request_pending; }
  //    set { this.m_request_pending = value; }
  //  } // RequestPending

  //  public Boolean RequestInProgress
  //  {
  //    get { return this.m_request_in_progress; }
  //    set { this.m_request_in_progress = value; }
  //  } // RequestInProgress

  //  public LCD_PLAYER_GIFTS AllLists
  //  {
  //    get { return this.m_all_lists; }
  //    set
  //    {
  //      this.m_current_gifts_list = null;
  //      this.m_next_gifts_list = null;
  //      this.m_credits_gifts_list = null;
  //      this.m_all_lists = value;
  //    }
  //  } // Gifts

  //  public List<LCD_ITEM> CurrentGiftsList
  //  {
  //    get { return this.m_current_gifts_list; }
  //    set { this.m_current_gifts_list = value; }
  //  } // CurrentGiftsList

  //  public List<LCD_ITEM> NextGiftsList
  //  {
  //    get { return this.m_next_gifts_list; }
  //    set { this.m_next_gifts_list = value; }
  //  } // NextGiftsList

  //  public List<LCD_ITEM> CreditsGiftsList
  //  {
  //    get { return this.m_credits_gifts_list; }
  //    set { this.m_credits_gifts_list = value; }
  //  } // CurrentGiftsList

  //  #endregion


  //  #region Public Functions

  //  public Gifts()
  //  {
  //    this.m_request_pending = true;
  //    this.m_request_in_progress = false;

  //    this.m_current_gifts_list = null;
  //    this.m_next_gifts_list = null;
  //    this.m_credits_gifts_list = null;
  //    this.m_all_lists = new LCD_PLAYER_GIFTS();
  //  }

  //  #endregion
  //}

  #endregion

  #region " Account Class "

  public class InTouchAccount : ICloneable
  {
    #region " Constants "

    // Database constants
    private const Int32 DB_AC_HOLDER_NAME                     = 0;
    private const string DB_AC_BALANCE                        = "AC_BALANCE";
    private const string DB_ACCOUNT_POINTS                    = "AC_POINTS";
    private const string DB_AC_CURRENT_HOLDER_LEVEL           = "AC_CURRENT_HOLDER_LEVEL";
    private const string DB_AC_HOLDER_GENDER                  = "AC_HOLDER_GENDER";
    private const string DB_AC_HOLDER_BIRTH_DATE              = "AC_HOLDER_BIRTH_DATE";
    private const string DB_AC_CURRENT_PLAY_SESSION_ID        = "AC_CURRENT_PLAY_SESSION_ID";
    private const string DB_AC_HOLDER_LEVEL_ENTERED           = "AC_HOLDER_LEVEL_ENTERED";
    private const string DB_AC_HOLDER_LEVEL_EXPIRATION        = "AC_HOLDER_LEVEL_EXPIRATION";
    private const string DB_AM_POINTS_GENERATED               = "AM_POINTS_GENERATED";
    private const string DB_AM_POINTS_DISCRETIONARIES         = "AM_POINTS_DISCRETIONARIES";
    private const string DB_AC_IN_SESSION_RE_BALANCE          = "AC_IN_SESSION_RE_BALANCE";
    private const string DB_AC_PIN                            = "AC_PIN";
    private const string DB_AC_PIN_FAILURES                   = "AC_PIN_FAILURES";
    private const string DB_AC_RE_BALANCE                     = "AC_RE_BALANCE";
    private const string DB_AC_IN_SESSION_PROMO_RE_BALANCE    = "AC_IN_SESSION_PROMO_RE_BALANCE";
    private const string DB_AC_IN_SESSION_PROMO_NR_BALANCE    = "AC_IN_SESSION_PROMO_NR_BALANCE";
    private const string DB_AC_HOLDER_NAME3                   = "AC_HOLDER_NAME3";
    private const string DB_AC_TRACK_DATA                     = "AC_TRACK_DATA";

    private const string DB_PS_TOTAL_CASH_OUT                 = "PS_TOTAL_CASH_OUT"; 
    private const string DB_PS_COMPUTED_POINTS                = "PS_COMPUTED_POINTS";
    private const string DB_PS_PLAYED_AMOUNT                  = "PS_PLAYED_AMOUNT";
    private const string DB_PS_WON_AMOUNT                     = "PS_WON_AMOUNT";
    private const string DB_PS_TOTAL_CASH_IN                  = "PS_TOTAL_CASH_IN";

    // Misc constants
    private const Int32 K_BLOCK_VALUE     = 1;
    private const Int32 K_ACCOUNT_LEVEL_0 = 0;
    private const Int32 K_ACCOUNT_LEVEL_1 = 1;
    private const Int32 K_ACCOUNT_LEVEL_2 = 2;
    private const Int32 K_ACCOUNT_LEVEL_3 = 3;
    private const Int32 K_ACCOUNT_LEVEL_4 = 4;

    #endregion

    #region " Members "

    public class AccountInfo_Level
    {
      public Int32 LevelId;
      public String LevelName;
      public DateTime EnterDate;
      public DateTime? Expiration;
      public Currency PointsToEnter;
    }

    #endregion

    #region " Properties "

    public Int64 Id { get; set; }
    public String FullName { get; set; }
    public GENDER Gender { get; set; }
    public DateTime Birthday { get; set; }
    public Currency MoneyBalance { get; set; }
    public Currency BalanceInSession { get; set; }
    public Points PointsBalance { get; set; }
    public Currency InSessionNR { get; set; }
    public Currency InSessionR { get; set; }
    public Points Points2Balance { get; set; }
    public AccountInfo_Level Level { get; set; }
    public AccountNextLevel NextLevel { get; set; }
    public Boolean InSession { get; set; }
    public String InSessionTerminalName { get; set; }
    public bool NoteAcceptorEnabled { get; set; }
    public Currency CurrentSpent { get; set; }
    public Currency CurrentPlayed { get; set; }
    public Boolean HasError { get; set; }
    public Int32 Multiplier { get; set; }
    [DataType(DataType.Password)]
    public String Pin { get; set; }
    public Int32 PinNumRetries { get; set; }
    public Currency TotalBalance { get; set; }
    public decimal PercentageIncrease
    {
      get
      {
        if (this.NextLevel.PointsToReach > 0 && this.NextLevel.PointsGenerated > 0)
        {
          return (this.NextLevel.PointsGenerated / this.NextLevel.PointsToReach) * 100;
        }

        return 0;
      }
    }
    public Points LevelPointsInView { get; set; }
    public Points ExchangePointsInView { get; set; }
    public String LevelIconPath { get; set; }
    public String TrackData { get; set; }

    public String ShortName { get; set; }

    #endregion " Properties "

    #region " Public Methods "

    #region " Constructors "

    public InTouchAccount()
    {
      this.Initialize();
    } // InTouchAccount
    public InTouchAccount(Int64 AccountId)
    {
      this.Initialize();

      if (!this.DB_GetAccount(AccountId))
      {
        this.HasError = true;
        Log.Add(TYPE_MESSAGE.ERROR, "Error ocurred on get account data");
      }
    } // InTouchAccount

    #endregion

    /// <summary>
    /// Clone
    /// </summary>
    /// <returns></returns>
    public object Clone()
    {
      return this.MemberwiseClone();
    } // Clone

    /// <summary>
    /// To check if there is credit into the account
    /// </summary>
    /// <returns></returns>
    public Boolean HasCreditInAccount()
    {
      if (this.MoneyBalance > 0)
      {
        return true;
      }

      return false;
    } // HasCreditInAccount

    /// <summary>
    /// Get Last Value for Account Id from playsessions
    /// </summary>
    /// <param name="AccountId"></param>
    /// <returns></returns>
    public Boolean GetLastPlaySessionByAccountId()
    {
      return DB_GetLastPlaySessionByAccountId();
    } // GetLastPlaySessionByAccountId

    /// <summary>
    /// Update Account Pin
    /// </summary>
    /// <param name="AccountId"></param>
    /// <param name="NewPin"></param>
    /// <returns></returns>
    public Boolean UpdatePin(String NewPin)
    {
      return DB_UpdatePin(NewPin);
    } // UpdatePin

    /// <summary>
    /// Update Block Account
    /// </summary>
    /// <param name="AccountId"></param>
    /// <returns></returns>
    public Boolean BlockAccount()
    {
      return DB_BlockAccount();
    } // BlockAccount

    #endregion

    #region " Private Methods "

    /// <summary>
    /// Initialize InTouchAccount
    /// </summary>
    private void Initialize()
    {
      FullName = String.Empty;
      Gender = GENDER.UNKNOWN;
      Birthday = DateTime.MinValue;
      MoneyBalance = 0;
      PointsBalance = 0;
      InSessionNR = 0;
      Points2Balance = 0;
      Level = new AccountInfo_Level();
      NextLevel = new AccountNextLevel();
      InSession = false;
      InSessionTerminalName = String.Empty;
      NoteAcceptorEnabled = false;
      CurrentSpent = 0;
      CurrentPlayed = 0;
      HasError = false;
      Multiplier = 0;
    } // Initialize

    /// <summary>
    /// Update to block Account
    /// </summary>
    /// <returns></returns>
    private Boolean DB_BlockAccount()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      try
      {

        _sb = new StringBuilder();
        _sb.AppendLine(" UPDATE   ACCOUNTS                                                                                            ");
        _sb.AppendLine("    SET   AC_BLOCKED           = @pBlocked                                                                    ");
        _sb.AppendLine("        , AC_BLOCK_REASON      = CASE WHEN @pBlocked = 0                                                      ");
        _sb.AppendLine("                                 THEN 0                                                                       ");
        _sb.AppendLine("                                 ELSE (dbo.BlockReason_ToNewEnumerate(AC_BLOCK_REASON) | @pBlockReason) END   ");
        _sb.AppendLine("        , AC_BLOCK_DESCRIPTION = @pBlockDescription                                                           ");
        _sb.AppendLine("  WHERE   AC_ACCOUNT_ID        = @pAccountId                                                                  ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pBlocked", SqlDbType.Bit).Value = K_BLOCK_VALUE;
            _cmd.Parameters.Add("@pBlockReason", SqlDbType.Int).Value = AccountBlockReason.MAX_PIN_FAILURES;
            _cmd.Parameters.Add("@pBlockDescription", SqlDbType.NVarChar).Value = @resources.nls_resource.BLOCK_REASON_DESCRIPTION;
            _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = this.Id;

            if (_cmd.ExecuteNonQuery() == 1)
            {
              _db_trx.Commit();
              return true;
            }

            return false;
          }
        }

      }
      catch (Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, String.Format("InTouch.DB_BlockAccount -> AccounId: {0}", this.Id));
        Log.Add(_ex);
      }

      return false;

    } // DB_BlockAccount

   /// <summary>
    /// Method to update pin account
   /// </summary>
   /// <param name="NewPin"></param>
   /// <returns></returns>
    private Boolean DB_UpdatePin(String NewPin)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" UPDATE   ACCOUNTS                       ");
        _sb.AppendLine("    SET   AC_PIN        = @pNewPin       ");
        _sb.AppendLine("  WHERE   AC_ACCOUNT_ID = @pAccountId    ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pNewPin"    , SqlDbType.NVarChar).Value = NewPin;
            _cmd.Parameters.Add("@pAccountId" , SqlDbType.BigInt  ).Value = this.Id;

            if (_cmd.ExecuteNonQuery() == 1)
            {
              return _db_trx.Commit();
            }

            return false;
          }
        }

      }
      catch (Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, String.Format("InTouch.DB_UpdatePin -> AccountId: {0}", this.Id));
        Log.Add(_ex);
      }

      return false;

    } // DB_UpdatePin

    /// <summary>
    /// Get Account data
    /// </summary>
    /// <param name="AccountId"></param>
    /// <returns></returns>
    private Boolean DB_GetAccount(Int64 AccountId)
    {
      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(this.DB_AccountQuery_Select(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
            _cmd.Parameters.Add("@pStatusActive", SqlDbType.Int).Value = ACCOUNT_PROMO_STATUS.ACTIVE;
            _cmd.Parameters.Add("@pExcludePromoNR2", SqlDbType.Int).Value = Promotion.PROMOTION_TYPE.AUTOMATIC_PER_RECHARGE_COVER_COUPON;

            using (SqlDataReader _sql_reader = _cmd.ExecuteReader())
            {
              if (_sql_reader.Read())
              {
                this.Id = AccountId;
                return this.DB_AccountDataMapper(_sql_reader);
              }
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, "Jackpot.DB_GetAaccount --> Error on read account list");
        Log.Add(_ex);
      }

      return false;

    } // DB_GetAccount

    /// <summary>
    /// Mapper from Account Data
    /// </summary>
    /// <param name="SqlReader"></param>
    /// <returns></returns>
    private Boolean DB_AccountDataMapper(SqlDataReader SqlReader)
    {
      try
      {
        string _aux_text;
        String _internal_track_data;
        String _external_track_data;

        this.Points2Balance = 0;

        _aux_text = SqlReader.GetString(DB_AC_HOLDER_NAME);

// Next Level
        this.Level                = this.AccountInfoGetLevel(SqlReader);
        this.NextLevel.SetLevel ( (Points)SqlReader.GetSqlMoney(SqlReader.GetOrdinal(DB_AM_POINTS_GENERATED))
                                , (Points)SqlReader.GetSqlMoney(SqlReader.GetOrdinal(DB_AM_POINTS_DISCRETIONARIES))
                                , this.Level.LevelId);

        this.FullName             = (_aux_text.Length > InTouchConstants.NAME_MAX_LENGTH) ? _aux_text.Remove(InTouchConstants.NAME_MAX_LENGTH) : _aux_text;
        this.MoneyBalance         = (Currency)SqlReader.GetSqlMoney(SqlReader.GetOrdinal(DB_AC_BALANCE));
        this.PointsBalance        = (Points)SqlReader.GetDecimal(SqlReader.GetOrdinal(DB_ACCOUNT_POINTS));        
        this.BalanceInSession     = (Currency)SqlReader.GetSqlMoney(SqlReader.GetOrdinal(DB_AC_IN_SESSION_RE_BALANCE));
        
        this.Gender               = (SqlReader[DB_AC_HOLDER_BIRTH_DATE] == DBNull.Value) ? GENDER.UNKNOWN       : (GENDER)SqlReader[DB_AC_HOLDER_GENDER];
        this.Birthday             = (SqlReader[DB_AC_HOLDER_BIRTH_DATE] == DBNull.Value) ? DateTime.MinValue    : (DateTime)SqlReader[DB_AC_HOLDER_BIRTH_DATE];        
        this.Pin                  = (SqlReader[DB_AC_PIN]               == DBNull.Value) ? String.Empty         : (String)SqlReader[DB_AC_PIN];
        this.PinNumRetries        = (SqlReader[DB_AC_PIN_FAILURES]      == DBNull.Value) ? 0                    : (Int32)SqlReader[DB_AC_PIN_FAILURES]; 

        this.InSession            = (SqlReader[DB_AC_CURRENT_PLAY_SESSION_ID] != DBNull.Value && (Int64)SqlReader[DB_AC_CURRENT_PLAY_SESSION_ID] > 0);

        this.InSessionNR          = SqlReader.GetSqlMoney(SqlReader.GetOrdinal(DB_AC_IN_SESSION_PROMO_NR_BALANCE));

        this.InSessionR           = SqlReader.GetSqlMoney(SqlReader.GetOrdinal(DB_AC_IN_SESSION_RE_BALANCE))
                                    + SqlReader.GetSqlMoney(SqlReader.GetOrdinal(DB_AC_IN_SESSION_PROMO_RE_BALANCE));

        this.TotalBalance         = SqlReader.GetSqlMoney(SqlReader.GetOrdinal(DB_AC_IN_SESSION_RE_BALANCE))
                                    + SqlReader.GetSqlMoney(SqlReader.GetOrdinal(DB_AC_IN_SESSION_PROMO_RE_BALANCE))
                                    + SqlReader.GetSqlMoney(SqlReader.GetOrdinal(DB_AC_IN_SESSION_PROMO_NR_BALANCE));        
         
        this.LevelPointsInView    = this.ExtractPointValueTruncated(this.PointsBalance);
        this.ExchangePointsInView = this.ExtractPointValueTruncated(this.NextLevel.PointsGenerated);

        _internal_track_data = SqlReader.GetString(SqlReader.GetOrdinal(DB_AC_TRACK_DATA));
        if (CardNumber.TrackDataToExternal(out _external_track_data,  _internal_track_data, 0))
        {
          this.TrackData          = _external_track_data;
        }

        this.ShortName            = SqlReader.GetSqlString(SqlReader.GetOrdinal(DB_AC_HOLDER_NAME3)).ToString();
        return true;
      }
      catch (Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, "Error on DB_AccountDataMapper");
        Log.Add(_ex);
      }

      return false;
    } // DB_AccountDataMapper    

    private Int32 ExtractPointValueTruncated(Points PointValue)
    {
      return Convert.ToInt32(Math.Truncate(PointValue.SqlMoney.ToDouble()));
    }

    /// <summary>
    /// Gets Account select query
    /// </summary>
    /// <returns></returns>
    private String DB_AccountQuery_Select()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.Append("EXECUTE   GetAccountPointsCache                                                                ");
      _sb.Append("          @pAccountId                                                                          ");

      _sb.Append("  SELECT   ACP_PROMO_DATE                                                                      ");
      _sb.Append("         , ACP_PROMO_NAME                                                                      ");
      _sb.Append("         , ACP_INI_BALANCE                                                                     ");
      _sb.Append("         , ACP_BALANCE                                                                         ");
      _sb.Append("    FROM   ACCOUNT_PROMOTIONS WITH (INDEX(IX_acp_account_status))                              ");
      _sb.Append("   WHERE   ACP_ACCOUNT_ID  = @pAccountId                                                       ");
      _sb.Append("     AND   ACP_STATUS      = @pStatusActive                                                    ");
      _sb.Append("     AND   ACP_PROMO_TYPE <> @pExcludePromoNR2;                                                ");

      return _sb.ToString();

    } // DB_AccountQuery_Select

    /// <summary>
    /// Get Level Info
    /// </summary>
    /// <param name="SqlReader"></param>
    /// <returns></returns>
    private AccountInfo_Level AccountInfoGetLevel(SqlDataReader SqlReader)
    {
      AccountInfo_Level _level;
      Int32 _aux_level_id;

      // Set Level
      _level = new AccountInfo_Level();

      _aux_level_id = SqlReader.GetInt32(SqlReader.GetOrdinal(DB_AC_CURRENT_HOLDER_LEVEL));
      _level.LevelId = Math.Max(K_ACCOUNT_LEVEL_0, Math.Min(_aux_level_id, K_ACCOUNT_LEVEL_4));  // Level id range: From 0 to 4.

      _level.LevelName = GeneralParam.GetString("PlayerTracking", "Level0" + _level.LevelId + ".Name");

      // Set EnterDate
      if (SqlReader.IsDBNull(SqlReader.GetOrdinal(DB_AC_HOLDER_LEVEL_ENTERED)))
      {
        _level.EnterDate = DateTime.MinValue;
      }
      else
      {
        _level.EnterDate = SqlReader.GetDateTime(SqlReader.GetOrdinal(DB_AC_HOLDER_LEVEL_ENTERED));
      }

      // Set Expiration
      if (SqlReader.IsDBNull(SqlReader.GetOrdinal(DB_AC_HOLDER_LEVEL_EXPIRATION)))
      {
        _level.Expiration = null;
      }
      else
      {
        _level.Expiration = SqlReader.GetDateTime(SqlReader.GetOrdinal(DB_AC_HOLDER_LEVEL_EXPIRATION));
      }

      return _level;
    } // AccountInfo_GetLevel

    /// <summary>
    /// Get Current PlaySession Info
    /// </summary>
    /// <param name="AccountId"></param>
    /// <returns></returns>
    private Boolean DB_GetLastPlaySessionByAccountId()
    {
      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(this.DB_GetLastPlaySessionValueByAccountId(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = this.Id;
            _cmd.Parameters.Add("@pPsType", SqlDbType.Int).Value = PlaySessionType.WIN;

            using (SqlDataReader _sql_reader = _cmd.ExecuteReader())
            {
              if (_sql_reader.Read())
              {
                return this.DB_UpdateAccountDataMapper(_sql_reader);
              }
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, "InTouchAccount.DB_GetLastPlaySessionByAccountId --> Error on read account list");
        Log.Add(_ex);
      }

      return false;

    } // DB_GetLastPlaySessionByAccountId

    /// <summary>
    /// Function that return last values from EGM play sessions
    /// </summary>
    /// <returns></returns>
    private String DB_GetLastPlaySessionValueByAccountId()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();


      _sb.AppendLine("       SELECT   TOP 1                                                   ");
      _sb.AppendLine("                PS_TOTAL_CASH_OUT                                       ");
      _sb.AppendLine("              , PS_TOTAL_CASH_IN                                        ");
      _sb.AppendLine("              , ISNULL (PS_COMPUTED_POINTS,0) AS PS_COMPUTED_POINTS     ");
      _sb.AppendLine("              , PS_PLAYED_AMOUNT                                        ");
      _sb.AppendLine("              , PS_WON_AMOUNT                                           ");
      _sb.AppendLine("        FROM    PLAY_SESSIONS  WITH( INDEX(IX_PS_ACCOUNT_ID_FINISHED))  ");
      _sb.AppendLine("  INNER JOIN    ACCOUNTS                                                ");
      _sb.AppendLine("          ON    PS_ACCOUNT_ID  = AC_ACCOUNT_ID                          ");
      _sb.AppendLine("       WHERE    PS_ACCOUNT_ID  = @pAccountId                            ");
      _sb.AppendLine("         AND    PS_TYPE        = @pPsType                               ");
      _sb.AppendLine("    ORDER BY    PS_PLAY_SESSION_ID  DESC                                ");

      return _sb.ToString();
    } // DB_GetLastPlaySessionValueByAccountId

    /// <summary>
    /// Mapper Account data After play in EGM
    /// </summary>
    /// <param name="SqlReader"></param>
    /// <returns></returns>
    private Boolean DB_UpdateAccountDataMapper(SqlDataReader SqlReader)
    {
      try
      {

        this.TotalBalance = (Currency)(SqlReader.GetSqlMoney(SqlReader.GetOrdinal(DB_PS_TOTAL_CASH_IN))
                             - SqlReader.GetSqlMoney(SqlReader.GetOrdinal(DB_PS_PLAYED_AMOUNT))
                             + SqlReader.GetSqlMoney(SqlReader.GetOrdinal(DB_PS_WON_AMOUNT)));

        this.PointsBalance = this.PointsBalance 
                             + (Points)SqlReader.GetDecimal(SqlReader.GetOrdinal(DB_PS_COMPUTED_POINTS));


        return true;
      }
      catch (Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, "Error on DB_UpdateAccountDataMapper");
        Log.Add(_ex);
      }

      return false;

    } // DB_UpdateAccountDataMapper

    /// <summary>
    /// Set Level icon 
    /// </summary>
    /// <param name="LevelId"></param>
    private String GetLevelIcon()
    {
      return String.Format("{0}LEVEL_{1}.png", this.LevelIconPath, this.Level.LevelId);
    } // SetLevelIcon

    #endregion

  } // InTouchAccount
  
  #endregion

}
