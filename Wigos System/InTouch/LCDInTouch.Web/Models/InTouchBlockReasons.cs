﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//      MODULE NAME: InTouchParamsConfigModel.cs
// 
//      DESCRIPTION: Model for params config inTouchWeb
// 
//           AUTHOR: Ferran Ortner
// 
//    CREATION DATE: 10-JUL-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 10-JUL-2017 FOS    First release.
//------------------------------------------------------------------------------
using System;

namespace LCDInTouch.Web.Models
{
  public class InTouchBlockReasons
  {

    #region " Properties "

    public int Id { get; set; }
    public String[] Code { get; set; }
    public String Description { get; set; }

    #endregion 

    #region " Public Methods "

    public InTouchBlockReasons()
    {
      Id = 0;
      Code = null;
      Description = String.Empty;
    }

    #endregion

  } // InTouchBlockReasons
}