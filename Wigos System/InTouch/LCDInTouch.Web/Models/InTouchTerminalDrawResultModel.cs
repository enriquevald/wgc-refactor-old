﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//      MODULE NAME: InTouchTerminalDrawResultModel.cs
// 
//      DESCRIPTION: InTouch terminal draw Game Result Model 
// 
//           AUTHOR: Javi Barea
// 
//    CREATION DATE: 27-JUL-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 17-JUL-2017 FOS    First release.
//------------------------------------------------------------------------------
using System;
using WSI.Common;

namespace LCDInTouch.Web.Models
{
  public class InTouchTerminalDrawParamsModel
  {

    #region " Properties "

    public String Url { get; set; }
    public Int32 TimeOut { get; set; }
    public Int64 AccountId { get; set; }    
    public Int64 PlaySessionId { get; set; }
    public Currency BetAmount { get; set; }
    public Currency Balance { get; set; }
    public Currency WinAmount { get; set; }
    public Currency NewBalance { get; set; }
    public Boolean IsWinner { get; set; }
    public Boolean IsForced { get; set; }
    public Boolean ParamsOk { get; set; }
    public Boolean RunDraw { get; set; }
    public Boolean ShowResult { get; set; }
    public Boolean AutoDraw { get; set; }
    public Int64 GameId{ get; set; }
    public Int64 GameType { get; set; }    

    #endregion

    #region " Constructor "

    public InTouchTerminalDrawParamsModel()
    {

    } // InTouchTerminalDrawParamsModel

    #endregion

  } // InTouchTerminalDrawResultModel
}