﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//      MODULE NAME: InTouchFunctionalitiesModel.cs
// 
//      DESCRIPTION: Model for params config inTouchWeb
// 
//           AUTHOR: Ferran Ortner
// 
//    CREATION DATE: 10-JUL-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 10-JUL-2017 FOS    First release.
//------------------------------------------------------------------------------
using System;

namespace LCDInTouch.Web.Models
{
  public class InTouchFunctionalitiesModel
  {

    #region " Properties "

    public Int64 NumFunctionalities;
    public Int64[] Functionalities;
    public Int64 NumResources;

    #endregion

  } // InTouchFunctionalitiesModel
}