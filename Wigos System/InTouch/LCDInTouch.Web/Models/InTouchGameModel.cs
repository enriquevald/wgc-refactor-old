﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//      MODULE NAME: InTouchModel.cs
// 
//      DESCRIPTION: General Model por InTouchLCD
// 
//           AUTHOR: Rubén Lama
// 
//    CREATION DATE: 29-JUN-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 29-JUN-2017 RLO    First release.
//------------------------------------------------------------------------------
using System;
using WSI.Common;

namespace LCDInTouch.Web.Models
{

  public class GameModel
  {
    public Currency Balance { get; set; }
    public Currency BetAmount { get; set; }
    public Currency WinAmount { get; set; }
    public Currency NewBalance { get; set; }   
  }
}