﻿using LCDInTouch.Web.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using WSI.Common;

namespace LCDInTouch.Web
{
  public class DataBaseConfig
  {
    public static LCDConfiguration Configuration { get; set; }

    public static void Init()
    {
      // Web API configuration and services

      // Connect to DB
      WGDB.Init(Util.GetSetting<Int32>("DBId"), Util.GetSetting<String>("DBPrincipal"), Util.GetSetting<String>("DBMirror"));
      WGDB.SetApplication(Util.AppName, Util.CurrentVersion);
      WGDB.ConnectAs("WGROOT");

      Configuration = new LCDConfiguration();
    }
  }
}