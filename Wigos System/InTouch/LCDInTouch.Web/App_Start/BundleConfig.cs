﻿using System.Web;
using System.Web.Optimization;

namespace LCDInTouch.Web
{
  public class BundleConfig
  {
    // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
    public static void RegisterBundles(BundleCollection bundles)
    {
      bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                  "~/Scripts/jquery-{version}.min.js"));

      bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                  "~/Scripts/jquery.validate*"));

      // Use the development version of Modernizr to develop with and learn from. Then, when you're
      // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
      bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                  "~/Scripts/modernizr-*"));

      bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                "~/Scripts/Common.js"
              ));

      bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/bootstrap.css",
                "~/Content/style2.css",
                "~/Content/site.css",
                "~/Content/Draws.css",
                "~/Content/Buttons.css",
                "~/Content/WidthDimensions.css",
                "~/Content/MarginDimensions.css",
                "~/Content/Level.css",
                "~/Content/DataAccount.css"));
    }
  }
}
