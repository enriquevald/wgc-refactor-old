﻿
var _tech_menu_status = 0;

$('#topLeftCorner').click(function () {
  _tech_menu_status = 1;
});

$('#bottomLeftCorner').click(function () {
  _tech_menu_status = (_tech_menu_status == 1) ? _tech_menu_status = 2 : _tech_menu_status = 0;
});

$('#bottomLeftCornerLabel').click(function () {
  _tech_menu_status = (_tech_menu_status == 1) ? _tech_menu_status = 2 : _tech_menu_status = 0;
});

$('#bottomRightCorner').click(function () {
  if (_tech_menu_status == 2) {
    InteractWithHost('17'); // 17 = TYPE_REQUEST.TECH_MENU
  }

  _tech_menu_status = 0;
});