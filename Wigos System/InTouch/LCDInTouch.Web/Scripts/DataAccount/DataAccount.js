﻿var _interval_miliseconds = 10000; // TODO - UNRANKED CCG: Set to 120000 for release

$(document).ready(function () {
  CheckIfPendingPlayDraws();
  setInterval(CheckIfPendingPlayDraws, _interval_miliseconds);
});

function CheckIfPendingPlayDraws() {
  $.ajax({
    url: '/LCDInTouch/DataAccount/GetPendingPlayDraws',
    type: 'GET',
    cache: false,
    success: function (response) {
      if (response === "True") {
        $('#img_juegos').show();
        $('#img_no_juegos').hide();
      }
      else {
        $('#img_juegos').hide();
        $('#img_no_juegos').show();
      }

      return true;
    },
    error: function () {
      //alert('error');
    }
  });
}