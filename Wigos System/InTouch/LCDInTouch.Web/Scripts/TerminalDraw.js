var sPageURL = decodeURIComponent(window.location.search.substring(1));
var selected = false;
var bounceProtection = true;
var timeOut = null;
var itemClicked = null;
var sSourceURL = '';

function view(view)
{
	showNone();

	
	switch(view)
	{
	  case 'prompt':
			showParticipate();
			break;	
	  case 'draw':
			showDraw();
			break;	
		case 'result':
			showResult();
			break;	
		default:
			showWait();
			break;	
	}
}

function showNone()
{
	$('.welcome').css('display', 'none');
	$('.wait').css('display', 'none');
	$('.draw').css('display', 'none');
	$('.drawPrompt').css('display', 'none');
	$('.drawPromptBalance').css('display', 'none');
	$('.drawPromptBet').css('display', 'none');
	$('.drawResult').css('display', 'none');
	$('.drawResultWinner').css('display', 'none');
	$('.drawResultLooser').css('display', 'none');
}

function showWait()
{
	$('.wait').css('display', '');
}

function showResult()
{
	$('.div_lang').css('display', 'none');
	$('.div_lang_es').css('display', 'none');
	$('.div_lang_en').css('display', 'none');
	
	
	if (getUrlParameter('Won', 'FALSE').toUpperCase() == 'TRUE')
	{
		$(itemClicked).children().first()
			.attr('src', 'img/ball.png');
		$(itemClicked)
			.css('background-color','transparent')
			.css('border','none');
		$('.drawResultWinner').html($('.drawResultWinner').html().replace('@WinAmount', (getUrlParameter('WinAmount', 0))));
		$('.drawResultWinner').css('display', '');
	}
	else
	{
		$('.drawResultLooser').css('display', '');
		$(itemClicked).children().first()
			.attr('src', 'img/blank.gif');
		$(itemClicked)
			.css('background-color','transparent')
			.css('border','none');
	}
	$('.draw').css('display', '');
	$('.drawResult').css('display', '');
	$('.drawResult').html($('.drawResult').html().replace('@balance', (getUrlParameter('Balance', 0))));
	setTimeout(function()
	{
		bounceProtection = false;
	}, 50);
	
	if (timeOut != null)
	{
		clearTimeout(timeOut);
	}
	timeOut = setTimeout(function()
	{
		$(document).trigger('click');
	}, 3000 );
}

function showParticipate()
{
	$('.welcome').css('display', '');
	$('#participateYes').click(function()
	{
		participateInDraw();
	});
	$('#participateNo').click(function()
	{
		dontParticipateInDraw();
	});
	if (timeOut != null)
	{
		clearTimeout(timeOut);
	}
	timeOut = setTimeout(function()
	{
		if(getUrlParameter('AutoDraw', 'FALSE').toUpperCase()=='FALSE')
		{
			try
			{
				window.external.InteractWithHost('30|3');
			}
			catch (exception)
			{
				alert('would call InteractWithHost 30|3');
			}
			setTimeout(function()
			{
				try
				{
					window.external.Exit();
				}
				catch (exception)
				{
					alert('would call Exit in lcd');
				}
			}, 200);
		} else {
			$('#participateYes').trigger('click');
		}
	}, getUrlParameter('Timeout', 10000));
	
}

function showDraw()
{
	$('.draw').css('display', '');
	$('.drawPrompt').css('display', '');
	
	//Balance
	$('.drawPromptBalance').css('display', '');
	$('.drawPromptBalance').html($('.drawPromptBalance').html().replace('@balance', (getUrlParameter('Balance', 0))));
	
	//Bet Amount
	$('.drawPromptBet').css('display', '');
	$('.drawPromptBet').html($('.drawPromptBet').html().replace('@betamount', (getUrlParameter('BetAmount', 0))));
	
	$('.div_item_draw').click(function()
	{
		if (!selected)
		{
			selected = true;
			if(itemClicked==null)
				itemClicked=this;
				$(itemClicked)
					.css('background-color','#D3D3D3')
					.css('border-bottom','3px gray solid')
					.css('border-right','1px gray solid');
			try
			{
			  console.log("clicked button");
				window.external.InteractWithHost('31|4');
			}
			catch (exception)
			{
				alert('would call InteractWithHost 31|4');
			}
		}
	});
	if (timeOut != null)
	{
		clearTimeout(timeOut);
	}
	timeOut = setTimeout(function()
	{
		try
		{
			window.external.InteractWithHost('31|5');
		}
		catch (exception)
		{
			alert('would call InteractWithHost 31|5');
		}
	}, getUrlParameter('Timeout', 10000) );
};

function  ZoomOut (event) {
	$("#div_item_draw img").height($("#div_item_draw img").height()* 0.5);
    $("#div_item_draw img").width($("#div_item_draw img").width()* 0.5);
}

function clickRandomitem()
{
	$("#item" + (Math.floor(Math.random() * 3) + 1)).trigger('click');
};

function getUrlParameter(sParam, sDefault)
{
	var sURLVariables = sPageURL.split('&');
	var sParameterName;
	var i;
	for (i = 0; i < sURLVariables.length; i++)
	{
		sParameterName = sURLVariables[i].split('=');
		if (sParameterName[0].toUpperCase() === sParam.toUpperCase())
		{
			return sParameterName[1] === undefined ? true : sParameterName[1];
		}
	}
	return sDefault;
};

function participateInDraw()
{
	view('wait');
	try
	{

	 // AjaxCalling("30","2");
	 window.external.InteractWithHost('30|2'); //send participate yes
	}
	catch (exception)
	{
	  alert(exception);
		alert('would call InteractWithHost 30|2');
	}
};

function dontParticipateInDraw()
{
	view('wait');
	try
	{
    AjaxCalling("30","1");

		//window.external.InteractWithHost('30|1'); // send participate no
	}
	catch (exception)
	{
		alert('would call InteractWithHost 30|1');
	}
	if (timeOut != null)
	{
		clearTimeout(timeOut);
	}
	setTimeout(function()
	{
		try
		{
			window.external.Exit();
		}
		catch (exception)
		{
			alert('would call Exit in lcd');
		}
	}, 200);
};

function hostInteractionCallBack(sData)
{
	if (sData == null || sData.length > 0) sPageURL = sData;
	if(getUrlParameter('ShowResult','FALSE').toUpperCase()=='TRUE')
	{
		view('result');
		return true;
	}
	if(getUrlParameter('ShowDraw','FALSE').toUpperCase()=='TRUE')
	{
		view('draw');
		return true;
	}
	return true;
};

function AjaxCalling(functionCode, protocolData)
{
  $.ajax({
    url: '/Home/StartParticipate',
    data:{
      functionCode : functionCode,
      protocolData : protocolData
    },
    type: 'GET',                
    success: function (response) {
      if (response) {
        $('.wait').css('display', 'none');
        view('draw');
        return true;
      } else{
        alert("something error");
      }
    },
    error : function(){
      alert('error');
    }
  });
}

$(document).ready(function()
{
	$(document).click(function()
	{
		if (selected && !bounceProtection)
		{
			try
			{
				window.external.Exit();
			}
			catch (exception)
			{
				alert('would call Exit in lcd');
			}
		}
	});
	view('wait');
	if (getUrlParameter('ForceDraw', 'FALSE').toUpperCase() == 'TRUE' 
			||
		getUrlParameter('ShowDraw','FALSE').toUpperCase() == 'TRUE') view('draw');
	else view('prompt');
	
	$('.div_lang_es').click(function()
	{
		sSourceURL = 'index_es.html';
		location.href = sSourceURL + '?' + sPageURL;
	});
	
	$('.div_lang_en').click(function()
	{
		sSourceURL = 'index_en.html';
		location.href = sSourceURL + '?' + sPageURL;
		
	});
	
});
