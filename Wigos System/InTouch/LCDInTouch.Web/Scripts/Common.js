
/// <summary>
/// On document Ready
/// </summary>
$(document).ready(function () {
  SetDate();
  $('html').css('cursor', 'url("../images/trasnsparent.cur") !important;');
  $(document).mouseXPos(-1000).mouseYPos(-1000);
});

/// <summary>
/// Interaction with LCDInTouch
/// </summary>
function InteractWithHost(Params)
{
  window.external.InteractWithHost(Params);
} // InteractWithHost

/// <summary>
/// Set date formatted
/// </summary>
function SetDate()
{
  if ( document.getElementById("LanguageHidden") == "undefined"
    || document.getElementById("LanguageHidden").value == "")
  {
    return;
  }

  var _months = new Array("01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12");
  var _date_time = new Date();
  var _language = document.getElementById("LanguageHidden").value;
  var _date_formatted = "";

  //Spanish
  _date_formatted = (_date_time.getDate() + "/" + _months[_date_time.getMonth()] + "/" + _date_time.getFullYear());

  //English
  if (_language == "en-US")
  {
    _date_formatted = (_months[_date_time.getMonth()] + "/" + _date_time.getDate() + "/" + _date_time.getFullYear());
}

  if (document.getElementById("DateFormatted"))
  {
    document.getElementById("DateFormatted").innerHTML = _date_formatted;
}
} // SetDate


