﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//      MODULE NAME: InTouchSessionData.cs
// 
//      DESCRIPTION: InTouch Session data to save properties
// 
//           AUTHOR: Rubén Lama
// 
//    CREATION DATE: 07-JUL-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 07-JUL-2017 RLO    First release.
//------------------------------------------------------------------------------
using LCDInTouch.Web.Classes;
using LCDInTouch.Web.Models;
using System;
using System.Web.Mvc;
using WSI.Common;

namespace LCDInTouch.Web.Controllers
{
  public class PinController : BaseController
  {

    #region "Public Methods"

    /// <summary>
    /// Action to show pinAuthentication View
    /// </summary>
    /// <returns></returns>
    public ActionResult PinAuthentication()
    {
      this.ViewBag.PinNumRetries = InTouchConstants.MAX_RETRIES_PIN - this.SessionData.Pin.NumRetries;

      return View();
    } // PinAuthentication

    /// <summary>
    /// Action to Control validate pin
    /// </summary>
    /// <param name="Pin"></param>
    /// <returns></returns>
    public ActionResult Validate(String Pin)
    {

      PinCheckStatus _pin_status;
      ActionResult _action_result;

      ///Check pin | if is pin ok reset the retries | if not add 1 retry | if arrive to 3 atempts block the account
      _pin_status = Accounts.DB_CheckAccountPIN(this.SessionData.Account.Id, Pin);
      _action_result = null;

      switch (_pin_status)
      {
        case PinCheckStatus.OK:
          _action_result = this.PinOk();
          break;
        case PinCheckStatus.WRONG_PIN:
          _action_result = this.PinKO();
          break;
        case PinCheckStatus.ACCOUNT_BLOCKED:
          _action_result = this.BlockAccount();
          break;
      }

      return _action_result;

    } // Validate
    
    /// <summary>
    /// Change pin >> always need authentication
    /// </summary>
    /// <returns></returns>
    public ActionResult ChangePin()
    {
      SessionData.Pin.ChangePin = true;

      return base.RedirectToManager(TYPE_REQUEST.REDIRECT_ACTION, TypeController.Pin, TypeView.PinAuthentication);
    } // ChangePin
    
    /// <summary>
    /// Action to show new pin View
    /// </summary>
    /// <returns></returns>
    public ActionResult PinNewPin()
    {
      SessionData.PreviousAction = TypeView.PinNewPin;
      SessionData.PreviousController = TypeController.Pin;

      SessionData.Pin.ChangePin = false;

      return View();

    } //PinNewPin

    /// <summary>
    /// Validate new pin
    /// </summary>
    /// <param name="Pin"></param>
    /// <param name="NewPin"></param>
    /// <returns></returns>
    public ActionResult ValidateNewPin(String Pin, String NewPin)
    {
      // If two pins match
      if (Pin == NewPin)
      {
        // Update pin
        if (SessionData.Account.UpdatePin(NewPin))
        {
          this.SessionData.Account = new InTouchAccount(this.SessionData.Account.Id);
          this.SessionData.Pin.PinOk = false;
          this.SessionData.Message.Type = TYPE_MESSAGE_TO_SHOW.PinInfoOk;

          return this.ShowMessage();
        }

        // General Error
        this.SessionData.Message.Type = TYPE_MESSAGE_TO_SHOW.GeneralError;

        return this.ShowMessage();
      }
      // General Error
      this.SessionData.Message.Type = TYPE_MESSAGE_TO_SHOW.PinNotMatch;

      return this.ShowMessage();
    }

    /// <summary>
    /// PinOk
    /// </summary>
    /// <returns></returns>
    public ActionResult PinOk()
    {
      //Set time for default time out
      this.SessionData.Pin.LastPinOk = WGDB.Now;

      if (this.SessionData.PreviousAction == TypeView.PinNewPin)
      {
        this.SessionData.Pin.PinOk = true;
      }

      if (SessionData.Pin.ChangePin)
      {
        return base.RedirectToManager(TYPE_REQUEST.REDIRECT_ACTION, TypeController.Pin, TypeView.PinNewPin);
      }

      //Goes to Previous Action And controller
      return base.RedirectToManager(TYPE_REQUEST.REDIRECT_ACTION, this.SessionData.PreviousController, this.SessionData.PreviousAction);

    } //PinKO

    /// <summary>
    /// PinKO
    /// </summary>
    /// <returns></returns>
    public ActionResult PinKO()
    {
      //Set values
      this.SessionData.Pin.LastPinOk = WGDB.MinDate;
      this.SessionData.Pin.NumRetries++;
      this.SessionData.Message.Type = TYPE_MESSAGE_TO_SHOW.PinError;

      return this.ShowMessage();
    } //PinKO

    /// <summary>
    /// BlockAccount
    /// </summary>
    /// <returns></returns>
    public ActionResult BlockAccount()
    {
      //Block Account
      this.SessionData.Account.BlockAccount();
      //Set values
      this.SessionData.Pin.LastPinOk = WGDB.MinDate;
      this.SessionData.Pin.NumRetries = 0;
      this.SessionData.Message.Type = TYPE_MESSAGE_TO_SHOW.PinLocked;

      return this.ShowMessage();

    } //BlockAccount
    #endregion

  }
}