﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//      MODULE NAME: RewardsDrawController.cs
// 
//      DESCRIPTION: Rewards Draw Controller for LCDInTouchWeb
// 
//           AUTHOR: Ferran Ortner
// 
//    CREATION DATE: 08-SEP-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 08-SEP-2017 FOS    First release.
//------------------------------------------------------------------------------
using LCDInTouch.Web.Classes;
using System;
using System.Web.Mvc;
using WSI.Common;
using WSI.Common.InTouch;

namespace LCDInTouch.Web.Controllers
{
  public class PlayRewardController : PlayDrawController
  {

    #region " Public Methods "

    /// <summary>
    /// Action to show RewardsDraw Menu
    /// </summary>
    /// <param name="page"></param>
    /// <returns></returns>
    public ActionResult PlayRewardMenu(int? page = 0)
    {
      Pagination<InTouchListItem> _paginated_items;

      //Pin Config
      if (this.SessionData.Pin.CheckPinConfig())
      {
        return this.GoToAction( TypeController.Pin       , TypeView.PinAuthentication    // Action 
                              , TypeController.PlayReward, TypeView.PlayRewardMenu); // Previous
      }

     // _paginated_items = this.GenerateNavigationControl(page);

      _paginated_items = new Pagination<InTouchListItem>();
      _paginated_items = _paginated_items.GenerateNavigationControl(page, 0, 0, (Int32)SessionData.Account.Id, true);

      ViewBag.redirect = String.Format  ( Constants.K_REDIRECT_PATH_WITH_GAME_INFO
                                        , base.GetWebHost()
                                        , TypeController.PlayDraw
                                        , TypeView.PlayDrawCanParticipate
                                        , TypeController.PlayReward
                                        , TypeView.PlayRewardMenu
                                        , "{0}"
                                        , (Int64)PromoGame.GameType.PlayReward); // After String.Format, execute another for GameId with only 1 parameter (in view PlayRewardController)

      ViewBag.PageIndex = _paginated_items.PageIndex;
      ViewBag.OneItem = _paginated_items.OneItem;
      ViewBag.TotalPages = _paginated_items.TotalPages;
      ViewBag.width = _paginated_items.CenterOnPage;

      return View(_paginated_items);
    } // PlayRewardMenu

    /// <summary>
    /// Action Result that returns the view PlayRewardStart
    /// </summary>
    /// <returns></returns>
    public ActionResult PlayRewardStart()
    {
      // Set Game data
      if (!this.SetGameStartData(TypeView.PlayRewardResult))
      {
        this.SessionData.Message.Type = TYPE_MESSAGE_TO_SHOW.GeneralError;

        return this.ShowMessage();
      }

      ViewBag.SelectOption = String.Format(resources.nls_resource.pd_select_option, resources.nls_resource.pd_select_playReward);

      // Set redirect action
      ViewBag.DelayRefresh = base.SetRedirectAfterDelayInteractWithHost(TYPE_EVENT.TERMINAL_DRAW_PARTICIPATE);

      return View(this.GetGameModel());
    } // PlayRewardStart

    /// <summary>
    /// Action Result that returns the PlayRewardResult
    /// </summary>
    /// <returns></returns>
    public ActionResult PlayRewardResult()
    {
      // Set game result
      if (!base.SetGameResultData((Int32)this.SessionData.ParamArg))
      {
        this.SessionData.Message.Type = TYPE_MESSAGE_TO_SHOW.GeneralError;

        return this.ShowMessage();
      }

      // Set account data
      this.SessionData.Account = new Models.InTouchAccount(this.SessionData.Account.Id);

      // Set redirect action
      ViewBag.DelayRefresh = base.SetRedirectAfterDelayInteractWithHost(TYPE_EVENT.TERMINAL_DRAW_GAME_EXECUTED);

      return View(this.SessionData.TerminalDraw.DrawParams);
    } // PlayRewardResult


    #endregion 

  }
}