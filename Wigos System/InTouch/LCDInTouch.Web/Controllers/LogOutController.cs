﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//      MODULE NAME: LogOutController.cs
// 
//      DESCRIPTION: Log Out Controller for LCDInTouchWeb
// 
//           AUTHOR: Rubén Lama 
// 
//    CREATION DATE: 13-JUL-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 13-JUL-2017 RLO    First release.
//------------------------------------------------------------------------------
using LCDInTouch.Web.Classes;
using System.Web.Mvc;

namespace LCDInTouch.Web.Controllers
{
  public class LogOutController : BaseController
  {

    #region " Public Methods "

    /// <summary>
    /// Logout action result
    /// </summary>
    /// <returns></returns>
    public ActionResult LogOut()
    {

      ViewBag.DelayRefresh = base.SetRedirectAfterDelay(TypeController.Manager, TypeView.Manager);

      return View();
    } // LogOut

    #endregion 

  } // LogOutController
}