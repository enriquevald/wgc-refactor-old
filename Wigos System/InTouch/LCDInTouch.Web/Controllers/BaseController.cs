﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//      MODULE NAME: BaseController.cs
// 
//      DESCRIPTION: Base Controller for LCDInTouchWeb
// 
//           AUTHOR: Ferran Ortner
// 
//    CREATION DATE: 10-JUL-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 10-JUL-2017 FOS    First release.
//------------------------------------------------------------------------------
using LCDInTouch.Web.Classes;
using System;
using System.Globalization;
using System.Threading;
using System.Web.Mvc;
using System.Web.Routing;
using System.Linq;
using System.Text;
using WSI.Common;
using System.Data.SqlClient;
using System.Data;
using System.Drawing;
using System.IO;
using LCDInTouch.Web.resources;

namespace LCDInTouch.Web.Controllers
{
  public class BaseController : Controller
  {

    #region "Members"

    private InTouchProtocol m_protocol;    

    #endregion

    #region "Constants"
    private const string DB_ITP_PENDING_DOWNLOAD = "ITP_PENDING_DOWNLOAD";
    private const string DB_ITP_ICON = "ITP_ICON";
    private const string DB_ITP_INDEX = "ITP_INDEX";

    #endregion

    #region " Properties "

    public InTouchSessionData SessionData
    {
      get
      {
        if (this.Session[InTouchConstants.K_SessionDataSession] == null)
        {
          this.Session[InTouchConstants.K_SessionDataSession] = new InTouchSessionData();
        }

        return (InTouchSessionData)this.Session[InTouchConstants.K_SessionDataSession];
      }
      set { this.Session[InTouchConstants.K_SessionDataSession] = value; }
    } // SessionData

    public InTouchProtocol Protocol 
    { 
      get
      {
        if (this.m_protocol == null)
        {
          this.m_protocol = new InTouchProtocol(this.SessionData);
        }

        return this.m_protocol;
      }
      set { this.m_protocol = value; }
    }

    public string DefaultPathLCDInTouch { get; set; }

    #endregion

    #region " Public Methods "

    /// <summary>
    /// Initialize BaseController
    /// </summary>
    /// <param name="requestContext"></param>
    protected override void Initialize(RequestContext requestContext)
    {
      // Initialize Controller
      base.Initialize(requestContext);

      // Set culture info
      this.SetCulture();
      
      // Refresh BlockReasons
      this.RefreshBlockReasons();

      // Set display flag
      this.SetDisplayFlag();

      // Save web host
      this.ViewBag.WebHost = this.GetWebHost();

    } // Initialize
    
    /// <summary>
    /// Refresh block reasosons
    /// </summary>
    public void RefreshBlockReasons()
    {
      if (this.SessionData.BlockReasons.Code == null)
      {
        return;
      }

      this.SessionData.BlockReasons.Description = String.Empty;

      // Set block reasons messages
      foreach (String _block_reason in this.SessionData.BlockReasons.Code)
      {
        if (!String.IsNullOrEmpty(this.SessionData.BlockReasons.Description))
        {
          this.SessionData.BlockReasons.Description += " | ";
        }

        this.SessionData.BlockReasons.Description += nls_resource.ResourceManager.GetString(_block_reason.Trim());
      }
    } // RefreshBlockReasons

    /// <summary>
    /// Function to get the Host where is located the web
    /// </summary>
    /// <returns></returns>
    public String GetWebHost()
    {
      String _host_name;

      _host_name = this.GetHostName();

      // Check HostName      
      if (_host_name.Contains("localhost"))
      {
        return InTouchConstants.DEFAULT_LOCALHOST_URL;
      }

      return String.Format("{0}/{1}/", _host_name, InTouchConstants.DEFAULT_SITE_NAME);
    } // GetWebHost

    public String GetServerLevelIconPath()
    {
      return String.Format(@"{0}{1}", Server.MapPath("~"), Constants.DEFAULT_PATH_LEVEL_ICONS);
    }

    public String GetLevelIconUrl()
    {
      return String.Format("{0}{1}LEVEL_{2}.png", this.GetWebHost(), Constants.DEFAULT_PATH_LEVEL_ICONS_WEB, this.SessionData.Account.Level.LevelId);
    }

    /// <summary>
    /// Request Manager.
    /// </summary>
    /// <param name="FunctionCode"></param>
    /// <param name="Result"></param>
    /// <param name="Data"></param>
    /// <returns></returns>
    public virtual ActionResult RequestManager(TYPE_REQUEST FunctionCode = TYPE_REQUEST.UNKNOWN, int Result = 999, String Data = "")
    {
      // Protocol Manager
      this.Protocol.Manager(FunctionCode, Result, Data);

      if (this.SessionData.Message.Type != TYPE_MESSAGE_TO_SHOW.None)
      {
        return this.ShowMessage();
      }

      // Redirect to action
      return this.GoToAction(this.SessionData.ProtocolController, this.SessionData.ProtocolAction);
    } // RequestManager

    /// <summary>
    /// Redirecto to manager
    /// </summary>
    /// <param name="FunctionCodeParam"></param>
    /// <param name="ResultParam"></param>
    /// <param name="DataParam"></param>
    /// <returns></returns>
    public ActionResult RedirectToManager(TYPE_REQUEST FunctionCode, String Controller, String View)
    {
      return RedirectToManager(FunctionCode, String.Format("{0}|{1}", Controller, View));
    } // RedirectToManager

    /// <summary>
    /// Redirecto to manager
    /// </summary>
    /// <param name="FunctionCodeParam"></param>
    /// <param name="ResultParam"></param>
    /// <param name="DataParam"></param>
    /// <returns></returns>
    public ActionResult RedirectToManager(TYPE_REQUEST FunctionCodeParam = TYPE_REQUEST.UNKNOWN, String DataParam = "")
    {
      return RedirectToAction(TypeView.Manager, TypeController.Manager, new { FunctionCode = FunctionCodeParam, Result = 999, Data = DataParam });
    } // RedirectToManager

    #endregion

    #region " Action Results that validate and redirect to the correct view "

    /// <summary>
    /// Action Method to redirect into view
    /// </summary>
    /// <returns>redirect to the new view</returns>
    public ActionResult GoToAction(String ControllerName, String ViewName)
    {
      return this.GoToAction(ControllerName, ViewName, String.Empty, String.Empty);
    } // GoToAction
    public ActionResult GoToAction(String ControllerName, String ActionName, String PreviousController, String PreviousAction)
    {
      // Update Action & Controller
      this.UpdateActionAndController(ControllerName, ActionName, PreviousController, PreviousAction);

      // TODO AUDIT NAVIGATION

      return this.RedirectToAction(ActionName, ControllerName);
    } // GoToAction

    /// <summary>
    /// Show error
    /// </summary>
    /// <param name="ErrorType"></param>
    /// <returns></returns>
    public ActionResult ShowMessage()
    {
      return this.RedirectToAction(TypeView.Show, TypeController.Message);
    } // ShowError

    /// <summary>
    /// Function that sets meta tag
    /// </summary>
    /// <param name="ControllerName">Name of Controller to redirect</param>
    /// <param name="ViewName">Name of view to redirect</param>
    /// <returns>String that set the content of metatag, after 5 seconds</returns>
    public String SetRedirectAfterDelay(String ControllerName, String ViewName)
    {
      return SetRedirectAfterDelay(ControllerName, ViewName, InTouchConstants.K_TimeInSeconds);
    } // SetRedirectAfterDelay
    public String SetRedirectAfterDelay(String ControllerName, String ViewName, Int32 TimeOut)
    {
      String _params;
      String _controller_view;

      // Set redirect action
      _controller_view = String.Format("{0}|{1}", ControllerName, ViewName);

      // Set Request Url 
      _params = String.Format(Constants.INTOUCH_PARAMS_FUNCTION_CODE_AND_DATA, TYPE_REQUEST.REDIRECT_ACTION, _controller_view);

      return String.Format(Constants.REDIRECT_ACTION_FORMAT, TimeOut, GetWebHost(), _params);
    } // SetRedirectAfterDelay

    /// <summary>
    /// Set redirect to interact with host
    /// </summary>
    /// <param name="Event"></param>
    /// <returns></returns>
    public String SetRedirectAfterDelayInteractWithHost(TYPE_EVENT Event, Int32 TimeOut = 0)
    {
      String _params;
      Random _rnd;
      Int32 _winner;
      Int32 _time_out;
      
      _params = String.Empty;

      if (TimeOut == 0)
      {
        _time_out = (Event != TYPE_EVENT.PENDING_LOGOUT) ? this.SessionData.TerminalDraw.GameParams.FirstOrDefault().TransferTimeOut
                                                         : InTouchConstants.K_TimeInSeconds;
      }else{
        _time_out = TimeOut;
      }

      switch (Event)
      {
        case TYPE_EVENT.TERMINAL_DRAW_ASK_FOR_PARTICIPATE:

          _params = Constants.INTOUCH_PARAMS_ACTION_INTERACT_WITH_HOST_TIMEOUT_ASK;

          break;

        case TYPE_EVENT.TERMINAL_DRAW_PARTICIPATE:

          _rnd = new Random();
          _winner = _rnd.Next(1, 3);

          _params = String.Format(Constants.INTOUCH_PARAMS_ACTION_INTERACT_WITH_HOST_TIMEOUT_DRAW, _winner);

          break;

        case TYPE_EVENT.TERMINAL_DRAW_GAME_EXECUTED:

          _params = Constants.INTOUCH_PARAMS_ACTION_INTERACT_WITH_HOST_TIMEOUT_RESULT;

          break;

        case TYPE_EVENT.PENDING_LOGOUT:

          _params = Constants.INTOUCH_PARAMS_ACTION_INTERACT_WITH_HOST_TIMEOUT_PENDING_LOGOUT;

          break;
      }

      return SetRedirectAfterDelayJS(String.Format(Constants.INTOUCH_PARAMS_FUNCTION_INTERACT_WITH_HOST, _params), _time_out);
    
    } // SetRedirectAfterDelayInteractWithHost

    /// <summary>
    /// Set redirecti with JS
    /// </summary>
    /// <param name="Function"></param>
    /// <param name="TimeOutInSeconds"></param>
    /// <returns></returns>
    public String SetRedirectAfterDelayJS(String Function, Int32 TimeOutInSeconds)
    {
      return String.Format(Constants.REDIRECT_ACTION_FORMAT_JS, Function, TimeOutInSeconds * 1000);
    } // SetRedirectAfterDelayJS

    /// <summary>
    /// Update Actions
    /// </summary>
    internal void UpdateActionAndController(String ControllerName, String ActionName)
    {
      this.UpdateActionAndController(ControllerName, ActionName, String.Empty, String.Empty);
    } // UpdateActionAndController
    internal void UpdateActionAndController(String ControllerName, String ActionName, String PreviousController, String PreviousAction)
    {
      // Set Previous Action & Controller
      //   -  If CurrentAction or CurrentController is set in SetActionFromManager, not need update PreviousAction & PreviousController
      if (this.SessionData.CurrentAction != ActionName)
      {
        this.SessionData.PreviousAction = this.SessionData.CurrentAction;
      }
      if (this.SessionData.CurrentController != ControllerName)
      {
        this.SessionData.PreviousController = this.SessionData.CurrentController;
      }

      // Set specific previous action
      if (!String.IsNullOrEmpty(PreviousAction) && !String.IsNullOrEmpty(ControllerName))
      {
        this.SessionData.PreviousAction = PreviousAction;
        this.SessionData.PreviousController = PreviousController;
      }

      // Set Current Action & Controller
      this.SessionData.UpdateCurrentAction(ControllerName, ActionName);

    } // UpdateActionAndController

    #endregion

    #region " Private Methods "

    /// <summary>
    /// Get host name
    /// </summary>
    /// <returns></returns>
    private String GetHostName()
    {
      Uri _uri;
      String _path;

      _uri = new Uri(Request.Url.AbsoluteUri);
      _path = _uri.PathAndQuery;

      return _uri.ToString().Replace(_path, String.Empty);
    } // GetHostName

    /// <summary>
    /// Methods that sets the current culture 
    /// </summary>
    private void SetCulture()
    {
      // Set Language in View
      ViewBag.Language = this.SessionData.Language;

      // Set Culture
      Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(this.SessionData.Language);
      Thread.CurrentThread.CurrentUICulture = new CultureInfo(this.SessionData.Language);
    } // SetCulture

    /// <summary>
    /// Method that sets the correct flag in the web
    /// </summary>
    private void SetDisplayFlag()
    {
      ViewBag.Flag = (this.SessionData.Language == LCDInTouch.Constants.TYPE_LANGUAGE_SPANISH) ? Constants.SpanishFlag : Constants.EnglishFlag;
    } // SetDisplayFlag

    #endregion

    #region " Internal Methods "

    /// <summary>
    /// To check if account data is correct.
    /// </summary>
    /// <returns></returns>
    internal Boolean CheckAccount()
    {
      return this.Protocol.GetAccountData(SessionData.Account.Id);
    } // CheckAccount

    /// <summary>
    /// To check if the account has enough credit to play the draaw
    /// </summary>
    /// <returns></returns>
    internal Boolean CheckEnoughMoneyToBet(Decimal BetAmount)
    {
      // Si estamos en transfiriendo crédito a la máquina, debemos comprobar SIEMPRE si tenemos dinero en el centro
      // ya que hemos simulado un extract card para forzar que el crédito
      if (this.SessionData.TransferInProgress)
      {
        return SessionData.Account.MoneyBalance > BetAmount;
      }
      // TODO DELETE JBP: FORZAMOS QUE SIEMPRE SEA CON PREGUNTA Y EN STARTCARDSESSION
      else if (!this.SessionData.Account.InSession)
      {
        return SessionData.Account.MoneyBalance > BetAmount;
      }

      return false;
    } // CheckEnoughMoneyToBet

    #endregion

  } // BaseController
}