﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//      MODULE NAME: InTouchSessionData.cs
// 
//      DESCRIPTION: InTouch Session data to save properties
// 
//           AUTHOR: Javier Barea
// 
//    CREATION DATE: 07-JUL-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 07-JUL-2017 JBP    First release.
//------------------------------------------------------------------------------
using LCDInTouch.Web.Classes;
using System;
using System.Web.Mvc;

namespace LCDInTouch.Web.Controllers
{
  public class PlayCashController : PlayDrawController
  {

    #region " Public Methods "

    /// <summary>
    /// Action Result that returns the view PlayCashStart
    /// </summary>
    /// <returns></returns>
    public ActionResult PlayCashStart()
    {      
      if (!this.SetGameStartData(TypeView.PlayCashResult))
      {
        this.SessionData.Message.Type = TYPE_MESSAGE_TO_SHOW.GeneralError;

        return this.ShowMessage();
      }

      ViewBag.SelectOption = String.Format(resources.nls_resource.pd_select_option, resources.nls_resource.pd_select_playCash);

      // Set redirect action
      ViewBag.DelayRefresh = base.SetRedirectAfterDelayInteractWithHost(TYPE_EVENT.TERMINAL_DRAW_PARTICIPATE);

      return View(this.GetGameModel());
    } // PlayCashStart

    /// <summary>
    /// Action Result that returns the PlayCashResult
    /// </summary>
    /// <returns></returns>
    public ActionResult PlayCashResult()
    {
      if (!base.SetGameResultData((Int32)this.SessionData.ParamArg))
      {
        this.SessionData.Message.Type = TYPE_MESSAGE_TO_SHOW.GeneralError;

        return this.ShowMessage();
      }

      this.SessionData.Account = new Models.InTouchAccount(this.SessionData.Account.Id);

      // Set redirect action
      ViewBag.DelayRefresh = base.SetRedirectAfterDelayInteractWithHost(TYPE_EVENT.TERMINAL_DRAW_GAME_EXECUTED);

      return View(this.SessionData.TerminalDraw.DrawParams);
    } // PlayCashResult

    #endregion

  } // PlayCashController
}