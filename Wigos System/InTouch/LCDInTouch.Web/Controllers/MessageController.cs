﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//      MODULE NAME: ErrorController.cs
// 
//      DESCRIPTION: Error Controller for LCDInTouchWeb
// 
//           AUTHOR: Javier Barea
// 
//    CREATION DATE: 11-JUL-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 11-JUL-2017 JBP    First release.
//------------------------------------------------------------------------------
using LCDInTouch.Web.Classes;
using System;
using System.Web.Mvc;

namespace LCDInTouch.Web.Controllers
{
  public class MessageController : BaseController
  {

    #region " Constants "

    private const int K_ERROR_MESSAGE_LINE_1 = 0;
    private const int K_ERROR_MESSAGE_LINE_2 = 1;

    #endregion 

    #region " Properties "

    private string K_SessionDataDefaultError = resources.nls_resource.ge_defaultError;

    #endregion

    #region " Public Methods "

    public MessageController()
      : base()
    {
    } // ErrorController
       
    /// <summary>
    /// Show merssage
    /// </summary>
    /// <returns></returns>
    public ActionResult Show()
    {
      String[] _sashost_message;

      switch (this.SessionData.Message.Type)
      {
        case TYPE_MESSAGE_TO_SHOW.CardError:

          ViewBag.DelayRefresh = base.SetRedirectAfterDelay(TypeController.Manager, TypeView.Manager);

          // TODO : CALL with needed params --> this.SessionData.Message.SetMessage();

          // TODO: Move all to Message.SetMessage (DELETE THIS COMMENT)          
          _sashost_message = this.SessionData.ErrorMessage.Split('|');

          ViewBag.Icon            = TypeIcon.Warning;
          ViewBag.MessageTitle    = resources.nls_resource.ce_Atencion;
          ViewBag.MainDescription = resources.nls_resource.ce_problema;
          ViewBag.Description     = resources.nls_resource.ce_responsable;

          // Set SASHost Message title
          if (!String.IsNullOrEmpty(_sashost_message[K_ERROR_MESSAGE_LINE_1]))
          {
            ViewBag.MainDescription = _sashost_message[K_ERROR_MESSAGE_LINE_1];
          }

          // Set SASHost message description 
          if ( _sashost_message.Length > K_ERROR_MESSAGE_LINE_2 
            && !String.IsNullOrEmpty(_sashost_message[K_ERROR_MESSAGE_LINE_2]))
          {
            ViewBag.Description = _sashost_message[K_ERROR_MESSAGE_LINE_2];
          }
          
          break;

        case TYPE_MESSAGE_TO_SHOW.PinError:

          ViewBag.DelayRefresh = base.SetRedirectAfterDelay(TypeController.Pin, TypeView.PinAuthentication);
          SessionData.ErrorMessage = resources.nls_resource.ce_pin_error;

          ViewBag.MessageTitle = resources.nls_resource.ce_Atencion;
          ViewBag.MainDescription = SessionData.ErrorMessage;
          ViewBag.Description = "";
          ViewBag.Icon = TypeIcon.Warning;

          break;

        case TYPE_MESSAGE_TO_SHOW.PinLocked:

          ViewBag.DelayRefresh = base.SetRedirectAfterDelay(TypeController.Manager, TypeView.Manager);
          SessionData.ErrorMessage = resources.nls_resource.ce_pin_locked;

          ViewBag.MessageTitle = resources.nls_resource.ce_Atencion;
          ViewBag.MainDescription = SessionData.ErrorMessage;
          ViewBag.Description = "";
          ViewBag.Icon = TypeIcon.Warning;

          break;

        case TYPE_MESSAGE_TO_SHOW.PinNotMatch:

          ViewBag.DelayRefresh = base.SetRedirectAfterDelay(TypeController.Pin, TypeView.PinNewPin);
          SessionData.ErrorMessage = resources.nls_resource.ce_pinNotMatch;

          ViewBag.MessageTitle = resources.nls_resource.ce_Atencion;
          ViewBag.MainDescription = SessionData.ErrorMessage;
          ViewBag.Description = "";
          ViewBag.Icon = TypeIcon.Warning;

          break;

        case TYPE_MESSAGE_TO_SHOW.PinInfoOk:

          ViewBag.DelayRefresh = base.SetRedirectAfterDelay(TypeController.DataAccount, TypeView.DataAccount01);
          SessionData.ErrorMessage = resources.nls_resource.ce_pinChangeOk;

          ViewBag.MessageTitle = resources.nls_resource.ce_Atencion;
          ViewBag.MainDescription = SessionData.ErrorMessage;
          ViewBag.Description = "";
          ViewBag.Icon = TypeIcon.Check;

          break;

        case TYPE_MESSAGE_TO_SHOW.TransferCreditError:

          ViewBag.DelayRefresh = base.SetRedirectAfterDelay(TypeController.DataAccount, TypeView.DataAccount);
          SessionData.ErrorMessage = resources.nls_resource.tc_error;

          ViewBag.Icon = TypeIcon.Warning;
          ViewBag.MessageTitle = resources.nls_resource.ce_Atencion;
          ViewBag.MainDescription = SessionData.ErrorMessage;
          ViewBag.Description = "";          
          
          break;

        case TYPE_MESSAGE_TO_SHOW.PendingLogout:

          ViewBag.DelayRefresh = base.SetRedirectAfterDelayInteractWithHost(TYPE_EVENT.PENDING_LOGOUT, InTouchConstants.K_TimeInSecondsPendingLogout);

          // TODO : CALL with needed params --> this.SessionData.Message.SetMessage();

          // TODO: Move all to Message.SetMessage (DELETE THIS COMMENT)          
          _sashost_message = this.SessionData.ErrorMessage.Split('|');

          ViewBag.Icon            = TypeIcon.Warning;
          ViewBag.MessageTitle    = resources.nls_resource.ce_Atencion;
          ViewBag.MainDescription = resources.nls_resource.tr_waitendsession;
          ViewBag.Description     = String.Empty;

          // Set SASHost Message title
          if (!String.IsNullOrEmpty(_sashost_message[K_ERROR_MESSAGE_LINE_1]))
          {
            ViewBag.MainDescription = _sashost_message[K_ERROR_MESSAGE_LINE_1];
          }

          // Set SASHost message description 
          if (_sashost_message.Length > K_ERROR_MESSAGE_LINE_2
            && !String.IsNullOrEmpty(_sashost_message[K_ERROR_MESSAGE_LINE_2]))
          {
            ViewBag.Description = _sashost_message[K_ERROR_MESSAGE_LINE_2];
          }

          break;

        default:
          
          ViewBag.DelayRefresh = base.SetRedirectAfterDelay(TypeController.Manager, TypeView.Manager);

          if (this.SessionData.ErrorMessage == String.Empty)
          {
            this.SessionData.ErrorMessage = K_SessionDataDefaultError;
          }

          ViewBag.MessageTitle = resources.nls_resource.ce_Atencion;
          ViewBag.MainDescription = SessionData.ErrorMessage;
          ViewBag.Description = "";
          ViewBag.Icon = TypeIcon.Warning;

          break;

      }

      if (!string.IsNullOrEmpty(this.SessionData.Message.Text))
      {
        ViewBag.MainDescription = this.SessionData.Message.Text;        
      }

      this.SessionData.ErrorMessage = String.Empty;
      this.SessionData.Message = new Message();

      return View();
    }
    
    #endregion

  } // ErrorController
}