﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//      MODULE NAME: ValidateController.cs
// 
//      DESCRIPTION: Validate Controller for LCDInTouchWeb
// 
//           AUTHOR: Ferran Ortner
// 
//    CREATION DATE: 10-JUL-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 10-JUL-2017 FOS    First release.
//------------------------------------------------------------------------------
using System.Web.Mvc;

namespace LCDInTouch.Web.Controllers
{
  public class ValidateController : BaseController
  {

    #region " Public Methods " 

    public ValidateController() : base()
    {
    } // ValidateController

    /// <summary>
    /// Validate view
    /// </summary>
    /// <returns></returns>
    public ActionResult Validate()
    {
      return View();
    } // Validate

    #endregion

  } // ValidateController
}