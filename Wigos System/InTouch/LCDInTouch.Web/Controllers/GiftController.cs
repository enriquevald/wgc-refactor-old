﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//      MODULE NAME: InTouchSessionData.cs
// 
//      DESCRIPTION: InTouch Session data to save properties
// 
//           AUTHOR: Rubén Lama
// 
//    CREATION DATE: 07-JUL-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 07-JUL-2017 RLO    First release.
//------------------------------------------------------------------------------
using LCDInTouch.Web.Classes;
using System.Web.Mvc;

namespace LCDInTouch.Web.Controllers
{
  public class GiftController : BaseController
  {

    #region " Public Methods "

    /// <summary>
    /// Action to show confirm gift buy screen
    /// </summary>
    /// <returns></returns>
    public ActionResult ConfirmGiftBuy()
    {
      //Pin Config
      if (this.SessionData.Pin.CheckPinConfig())
      {
        return this.GoToAction(TypeController.Pin, TypeView.PinAuthentication, TypeController.Gift, TypeView.GiftConfirmGiftBuy);
      }

      return View();
    } // ConfirmGiftBuy

    #endregion 

  } // GiftController
}