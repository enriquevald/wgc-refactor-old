﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//      MODULE NAME: InTouchSessionData.cs
// 
//      DESCRIPTION: InTouch Session data to save properties
// 
//           AUTHOR: Javier Barea
// 
//    CREATION DATE: 07-JUL-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 07-JUL-2017 JBP    First release.
//------------------------------------------------------------------------------
using LCDInTouch.Web.Classes;
using System;
using System.Web.Mvc;

namespace LCDInTouch.Web.Controllers
{
  public class WelcomeDrawController : PlayDrawController
  {

    /// <summary>
    /// Action Result that returns the view WelcomeDrawStart
    /// </summary>
    /// <returns></returns>
    public ActionResult WelcomeDrawStart()
    {      
      if (!this.SetGameStartData(TypeView.WelcomeDrawResult))
      {
        this.SessionData.Message.Type = TYPE_MESSAGE_TO_SHOW.GeneralError;

        return this.ShowMessage();
      }

      ViewBag.SelectOption = String.Format(resources.nls_resource.pd_select_option, resources.nls_resource.pd_select_welcome);

      // Set redirect action
      ViewBag.DelayRefresh = base.SetRedirectAfterDelayInteractWithHost(TYPE_EVENT.TERMINAL_DRAW_PARTICIPATE);

      return View(this.GetGameModel());
    } // WelcomeDrawStart

    /// <summary>
    /// Action Result that returns the WelcomeDrawResult
    /// </summary>
    /// <returns></returns>
    public ActionResult WelcomeDrawResult()
    {      
      if (!base.SetGameResultData((Int32)this.SessionData.ParamArg))
      {
        this.SessionData.Message.Type = TYPE_MESSAGE_TO_SHOW.GeneralError;

        return this.ShowMessage();
      }

      // Set redirect action
      ViewBag.DelayRefresh = base.SetRedirectAfterDelayInteractWithHost(TYPE_EVENT.TERMINAL_DRAW_GAME_EXECUTED);

      return View(this.SessionData.TerminalDraw.DrawParams);
    } // WelcomeDrawResult

  }
}