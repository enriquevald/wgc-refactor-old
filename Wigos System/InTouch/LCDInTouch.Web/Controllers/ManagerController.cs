﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//      MODULE NAME: ManagerController.cs
// 
//      DESCRIPTION: Manager Controller for LCDInTouchWeb
// 
//           AUTHOR: Javier Barea
// 
//    CREATION DATE: 30-JUN-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 30-JUN-2017 JBP    First release.
//------------------------------------------------------------------------------
using LCDInTouch.Web.Classes;
using LCDInTouch.Web.Models;
using System;
using System.Diagnostics;
using System.Web.Mvc;
using WSI.Common;

namespace LCDInTouch.Web.Controllers
{
  public class ManagerController : BaseController
  {

    #region " Public Methods "

    public ManagerController() : base()
    {

    }

    /// <summary>
    /// Action Result that returns the view Manager/Manager
    /// </summary>
    /// <param name="FunctionCode"></param>
    /// <param name="Result"></param>
    /// <param name="Data"></param>
    /// <returns></returns>
    public ActionResult Manager(TYPE_REQUEST FunctionCode = TYPE_REQUEST.UNKNOWN, int Result = 999, String Data = "")
    {
      this.Initialize();

      if (FunctionCode != TYPE_REQUEST.UNKNOWN)
      {
        // Request manager.
        return base.RequestManager(FunctionCode, Result, Data);
      }

      // Show Index View
      return this.GoToAction(TypeController.Manager, TypeView.Index);
    } // Manager

    /// <summary>
    /// Action Result that returns the view Manager/Index
    /// </summary>
    public ActionResult Index()
    {
      //// Donwload Level icons
      this.DownloadLevelIcons();

      ViewBag.IsBlocked = (this.SessionData.BlockReasons.Id != 0);
      ViewBag.BlockDescription = this.SessionData.BlockReasons.Description;

      ViewBag.VersionMode = (Debugger.IsAttached) ? new DB_TRX().SqlTransaction.Connection.Database : "v";
      ViewBag.CursorClass  = (Debugger.IsAttached) ? "" : "HideCursor";

      return View(base.SessionData.SASParams);
    } // Manager

    #endregion

    #region " Private Methods "

    /// <summary>
    /// Reset session when extract card
    /// </summary>
    private void ResetSession()
    {
      this.SessionData.TransferInProgress = false;
      this.SessionData.PendingReset       = false;
      this.SessionData.PreviousAction     = TypeView.Manager;
      this.SessionData.PreviousController = TypeController.Manager;
      this.SessionData.Account            = new InTouchAccount();
      this.SessionData.Pin                = new InTouchPinModel();
      this.SessionData.TerminalDraw       = new InTouchTerminalDrawModel();
      this.SessionData.GameGateWay        = new InTouchGameGatewayModel();
      this.SessionData.Redirects          = new InTouchRedirects();
    } // ResetSession

    private void Initialize()
    {
      // Initialize session
      if (this.SessionData.PendingReset)
      {
        this.ResetSession();
      }
    }

    private void SetLevelIconPath()
    {
      if (this.SessionData != null && this.SessionData.Account != null)
      {
        this.SessionData.Account.LevelIconPath = base.GetServerLevelIconPath();
      }
    }

    private void DownloadLevelIcons()
    {
      PlayerLevelIcon _player_level_icon;

      _player_level_icon = new PlayerLevelIcon();

      this.SetLevelIconPath();

      _player_level_icon.DownloadLevelIcons(base.GetServerLevelIconPath());
    }

    #endregion

  } // ManagerController
}