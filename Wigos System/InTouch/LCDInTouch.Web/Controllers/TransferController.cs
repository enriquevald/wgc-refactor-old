﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//      MODULE NAME: TransferController.cs
// 
//      DESCRIPTION: Transfer Controller for LCDInTouchWeb
// 
//           AUTHOR: Javier Barea
// 
//    CREATION DATE: 11-JUL-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 11-JUL-2017 JBP    First release.
//------------------------------------------------------------------------------
using LCDInTouch.Web.Classes;
using System.Web.Mvc;

namespace LCDInTouch.Web.Controllers
{
  public class TransferController : BaseController
  {

    #region " Public Methods " 

    public TransferController() : base()
    {
    } // TransferController

    /// <summary>
    /// Transfer view
    /// </summary>
    /// <returns></returns>
    public ActionResult Transfer()
    {
      return View();
    } // Transfer

    /// <summary>
    /// Action to show Trasnfer03 view
    /// </summary>
    /// <returns></returns>
    public ActionResult Transfer03()
    {
      //Pin Config
      if (this.SessionData.Pin.CheckPinConfig())
      {
        return this.GoToAction(TypeController.Pin, TypeView.PinAuthentication, TypeController.Transfer, TypeView.Transfer03);
      }
      else
      {
        return View();
      }
    } // Transfer03

    #endregion

  } // TransferController
}