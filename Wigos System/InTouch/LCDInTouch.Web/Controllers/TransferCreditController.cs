﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//      MODULE NAME: TransferCreditController.cs
// 
//      DESCRIPTION: Transfer Credit Controller for LCDInTouchWeb
// 
//           AUTHOR: Carlos Cáceres
// 
//    CREATION DATE: 14-SEP-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 14-SEP-2017 CCG    First release.
//------------------------------------------------------------------------------
using LCDInTouch.Web.Classes;
using System;
using System.Web.Mvc;

namespace LCDInTouch.Web.Controllers
{
  public class TransferCreditController : BaseController
  {

    #region " Public Methods "

    /// <summary>
    /// Transfer Credit Controller
    /// </summary>
    public TransferCreditController() : base()
    {
    } // TransferCreditController

    /// <summary>
    /// Action Result that returns the view TransferCredit/TransferCredit
    /// </summary>
    /// <returns></returns>
    public ActionResult TransferCredit()
    {
      this.Protocol.GetAccountData(SessionData.Account.Id);

      ViewBag.RedirectNoButton = String.Format( Constants.K_DEFAULT_SIMPLE_REDIRECT_PATH
                                              , base.GetWebHost()
                                              , TypeController.DataAccount
                                              , TypeView.DataAccount02);

      ViewBag.AvailableCredits = SessionData.Account.MoneyBalance;

      return View();
    } // TransferCredit

    #endregion

  } // DataAccountController

}