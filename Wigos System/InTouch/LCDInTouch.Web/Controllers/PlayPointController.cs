﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//      MODULE NAME: RewardsDrawController.cs
// 
//      DESCRIPTION: Rewards Draw Controller for LCDInTouchWeb
// 
//           AUTHOR: Ruben Lama
// 
//    CREATION DATE: 08-SEP-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 08-SEP-2017 RLO    First release.
//------------------------------------------------------------------------------
using LCDInTouch.Web.Classes;
using System;
using System.Web.Mvc;
using WSI.Common;
using WSI.Common.InTouch;

namespace LCDInTouch.Web.Controllers
{
  public class PlayPointController : PlayDrawController
  {
    #region " Public Functions "
    
    /// <summary>
    /// Action to show points menu
    /// </summary>
    /// <param name="Page"></param>
    /// <returns></returns>
    public ActionResult PlayPointMenu(int? Page = 0)
    {
      Pagination<InTouchListItem> _paginated_items;

      //Pin Config
      if (this.SessionData.Pin.CheckPinConfig())
      {
        return this.GoToAction( TypeController.Pin      , TypeView.PinAuthentication  // Action
                              , TypeController.PlayPoint, TypeView.PlayPointMenu);   // Previous
      }
      else
      {
        _paginated_items = new Pagination<InTouchListItem>();
        _paginated_items = _paginated_items.GenerateNavigationControl(Page, SessionData.Account.Level.LevelId, (Int32)SessionData.Account.PointsBalance,(Int32)SessionData.Account.Id);

        ViewBag.redirect = String.Format(Constants.K_REDIRECT_PATH_WITH_GAME_INFO
                                          , base.GetWebHost()
                                          , TypeController.PlayDraw
                                          , TypeView.PlayDrawCanParticipate
                                          , TypeController.PlayPoint
                                          , TypeView.PlayPointMenu
                                          , "{0}"
                                          , (Int64)PromoGame.GameType.PlayPoint); // After String.Format, execute another for GameId with only 1 parameter (in view PlayPointMenu.cshtml));

        ViewBag.AccountPoints = this.SessionData.Account.LevelPointsInView;
        ViewBag.Currency = CurrencyExchange.GetNationalCurrency();

        ViewBag.PageIndex = _paginated_items.PageIndex;
        ViewBag.OneItem = _paginated_items.OneItem;
        ViewBag.TotalPages = _paginated_items.TotalPages;
        ViewBag.width = _paginated_items.CenterOnPage;

        return View(_paginated_items); 
      }
    } // PlayPointMenu

    
    /// <summary>
    /// Action Result that starts a PlayPoint draw
    /// </summary>
    /// <returns></returns>
    public ActionResult PlayPointStart()
    {
      if (!this.SetGameStartData(TypeView.PlayPointResult))
      {
        this.SessionData.Message.Type = TYPE_MESSAGE_TO_SHOW.GeneralError;

        return this.ShowMessage();
      }

      ViewBag.SelectOption = String.Format(resources.nls_resource.pd_select_option, resources.nls_resource.pd_select_playPoint);

      // Set redirect action
      ViewBag.DelayRefresh = base.SetRedirectAfterDelayInteractWithHost(TYPE_EVENT.TERMINAL_DRAW_PARTICIPATE);

      return View(this.GetGameModel());
    } // PlayPointStart

    /// <summary>
    /// Action Result that shows the result of a PlayPoint draw
    /// </summary>
    /// <returns></returns>
    public ActionResult PlayPointResult()
    {
      if(!base.SetGameResultData((Int32)this.SessionData.ParamArg))
      {
        this.SessionData.Message.Type = TYPE_MESSAGE_TO_SHOW.GeneralError;

        return this.ShowMessage();
      }

      this.SessionData.Account = new Models.InTouchAccount(this.SessionData.Account.Id);

      // Set redirect action
      ViewBag.DelayRefresh = base.SetRedirectAfterDelayInteractWithHost(TYPE_EVENT.TERMINAL_DRAW_GAME_EXECUTED);

      return View(this.SessionData.TerminalDraw.DrawParams);
    }  // PlayPointResult

    #endregion
  }

}