﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//      MODULE NAME: DataAccountController.cs
// 
//      DESCRIPTION: Data Account Controller for LCDInTouchWeb
// 
//           AUTHOR: Javier Barea
// 
//    CREATION DATE: 11-JUL-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 11-JUL-2017 JBP    First release.
//------------------------------------------------------------------------------
using LCDInTouch.Web.Classes;
using LCDInTouch.Web.Models;
using System;
using System.Web.Mvc;
using WSI.Common;

namespace LCDInTouch.Web.Controllers
{
  public class DataAccountController : BaseController
  {

    #region " Public Methods "

    /// <summary>
    /// Data Account Controller
    /// </summary>
    public DataAccountController()
      : base()
    {
    } // DataAccountController

    /// <summary>
    /// Action Result that returns the view DataAccount/DataAccount
    /// </summary>
    /// <returns></returns>
    public ActionResult DataAccount()
    { 
      ViewBag.LevelIcon = base.GetLevelIconUrl();
      ViewBag.DelayRefresh = base.SetRedirectAfterDelay(TypeController.DataAccount, TypeView.DataAccount02);
      ViewBag.PointsVisibility = this.SessionData.PointsConfiguration.VisibilityPointsAtInsertAndRemoveCard;

      this.GetLabelPointsText();
      
      return View(SessionData.Account);

    } // DataAccount

    /// <summary>
    /// Action to show screen dataAccount01
    /// </summary>
    /// <param name="IsTest"></param>
    /// <returns></returns>
    public ActionResult DataAccount01(Boolean IsTest = false)
    {
      if (IsTest)
      {
        this.SessionData.Account.Pin = "1111";
        this.SessionData.Pin.NeverRequestForPIN = true;
      }

      ViewBag.LevelIcon = base.GetLevelIconUrl();
      ViewBag.PercentageIncrease = Math.Round(SessionData.Account.PercentageIncrease);

      if (SessionData.Account.NextLevel.LevelName == SessionData.Account.Level.LevelName)
      {
        ViewBag.ShowProgressBar = false;
      }
      else
      {
        ViewBag.ShowProgressBar = true;
      }

      //Pin Config
      SessionData.PreviousAction = TypeView.DataAccount01;
      SessionData.PreviousController = TypeController.DataAccount;

      GetLabelPointsText();

      //Pin Config
      if (this.SessionData.Pin.CheckPinConfig())
      {
        return base.RedirectToManager(TYPE_REQUEST.REDIRECT_ACTION, TypeController.Pin, TypeView.PinAuthentication);
      }
      else
      {
        return View(SessionData.Account);
      }
    } // DataAccount01

    /// <summary>
    /// DataAccount02 Controller
    /// </summary>
    /// <returns></returns>
    public ActionResult DataAccount02()
    {
      try
      {
        this.SessionData.Account = new InTouchAccount(this.SessionData.Account.Id);
        ViewBag.LevelIcon = base.GetLevelIconUrl();

      GetLabelPointsText();

      if (this.GetPendingPlayDraws())
      {
          ViewBag.redirect = String.Format(Constants.K_DEFAULT_REDIRECT_PATH
                                        , base.GetWebHost()
                                        , TypeController.PlayDraw
                                        , TypeView.PlayDrawMenu
                                        , TypeController.DataAccount
                                        , TypeView.DataAccount02);
      }
      else
      {
        ViewBag.redirect = "";
      }

      this.SessionData.Account.NextLevel.PointsGenerated = Math.Max(SessionData.Account.NextLevel.PointsGenerated, 0);
      this.SessionData.Account.Multiplier = 0; // TODO - UNRANKED: CCG - Set value > that 0 to show multipliers (For testing)

      this.ViewBag.ActionJS = String.Empty;

      if (this.SessionData.SendTransferAfterPlayGame)
      {
        this.SessionData.SendTransferAfterPlayGame = false;

        //force another transfer credit
        this.ViewBag.ActionJS = "<script>InteractWithHost('22');</script>";        
      }      

      ViewBag.ShowMultiplier = (this.SessionData.Account.Multiplier > 1) ? "data-account2-info-with-multiplier" : "data-account2-info";
      ViewBag.MultiplierVisibility = (this.SessionData.Account.Multiplier > 1) ? "display:inline" : "display:none";
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }

      return View(SessionData.Account);
    } // DataAccount02

    /// <summary>
    /// Action Result DataAccount03
    /// </summary>
    /// <returns></returns>
    public ActionResult DataAccount03()
    {
      this.SessionData.Account.GetLastPlaySessionByAccountId();
      this.SessionData.Message.Type = TYPE_MESSAGE_TO_SHOW.PendingLogout;

      ViewBag.LevelIcon = base.GetLevelIconUrl();
      ViewBag.DelayRefresh = base.SetRedirectAfterDelayInteractWithHost(TYPE_EVENT.PENDING_LOGOUT); 
      
      ViewBag.PointsVisibility = SessionData.PointsConfiguration.VisibilityPointsAtInsertAndRemoveCard;

      GetLabelPointsText();

      return View(SessionData.Account);
    } // DataAccount03

    /// <summary>
    /// Action Result that returns the view DataAccount/DataAccountMenu
    /// </summary>
    /// <returns>DataAccount/DataAccountMenu</returns>
    public ActionResult DataAccountMenu()
    {
      // Update account info
      this.SessionData.Account = new InTouchAccount(this.SessionData.Account.Id);

      ViewBag.hasPlayDraw = "";
      ViewBag.hasCreditInAccount = "";

      ViewBag.redirectPlayDraw = String.Format(Constants.K_DEFAULT_REDIRECT_PATH
                                              , base.GetWebHost()
                                              , TypeController.PlayDraw
                                              , TypeView.PlayDrawMenu
                                              , TypeController.DataAccount
                                              , TypeView.DataAccountMenu);
      

      ViewBag.redirectAccount = String.Format(Constants.K_DEFAULT_SIMPLE_REDIRECT_PATH
                                              , base.GetWebHost()
                                              , TypeController.DataAccount
                                              , TypeView.DataAccount01);

      ViewBag.redirectTransferCredit = String.Format(Constants.K_DEFAULT_SIMPLE_REDIRECT_PATH
                                                    , base.GetWebHost()
                                                    , TypeController.TransferCredit
                                                    , TypeView.TransferCredit);

      if (!this.SessionData.Account.HasCreditInAccount())
      {
        ViewBag.hasCreditInAccount = "disabled";
      }

      return View();
    } // DataAccountMenu

    /// <summary>
    /// Action result that returns the view DataAccount/DataAccountStatus
    /// </summary>
    /// <returns></returns>
    public ActionResult DataAccountStatus()
    {
      
      ViewBag.LevelIcon = base.GetLevelIconUrl();

      return View(SessionData.Account);
    } // DataAccountStatus

    /// <summary>
    /// To check if the current account has pending play draws
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public Boolean GetPendingPlayDraws()
    {
      return this.SessionData.TerminalDraw.GetPendingPlayDraws(this.SessionData.Account.Id, PromoGame.GameType.PlayCash);
    } //GetPendingPlayDraws

    #endregion

    #region "Private Methods"

    /// <summary>
    /// Function that Returns the Text of Points Label
    /// </summary>
    private void GetLabelPointsText()
    {
      if (SessionData.Language == LCDInTouch.Constants.TYPE_LANGUAGE_SPANISH)
      {
        ViewBag.PointsText = SessionData.PointsConfiguration.LabelPointsText;
        ViewBag.PointsForLevelText = SessionData.PointsConfiguration.LabelPointsForLevelText;
      }
      else if (SessionData.Language == LCDInTouch.Constants.TYPE_LANGUAGE_FRENCH)
      {
        ViewBag.PointsText = SessionData.PointsConfiguration.LabelPointsText;
        ViewBag.PointsForLevelText = SessionData.PointsConfiguration.LabelPointsForLevelText;
      }
      else
      {
        ViewBag.PointsText = SessionData.PointsConfiguration.LabelPointsText_EN;
        ViewBag.PointsForLevelText = SessionData.PointsConfiguration.LabelPointsForLevelText_EN;
      }
    } // GetLabelPointsText



    #endregion

  } // DataAccountController

}