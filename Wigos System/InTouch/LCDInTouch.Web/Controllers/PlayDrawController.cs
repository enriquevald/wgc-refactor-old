﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//      MODULE NAME: PlayDrawController.cs
// 
//      DESCRIPTION: Draw Controller for LCDInTouchWeb
// 
//           AUTHOR: Javier Barea
// 
//    CREATION DATE: 11-JUL-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 11-JUL-2017 JBP    First release.
//------------------------------------------------------------------------------
using LCDInTouch.Web.Classes;
using LCDInTouch.Web.Models;
using LCDInTouch.Web.resources;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using WSI.Common;
using WSI.Common.ExtentionMethods;
using WSI.Common.TerminalDraw;
using WSI.Common.TerminalDraw.Persistence;
using System.Linq;

namespace LCDInTouch.Web.Controllers
{
  public class PlayDrawController : BaseController
  {

    #region " Constants "
    public const String K_REDIRECT_FORMAT       = "location.href='{0}/{1}/{2}'";

    public const Int32 K_BUTTON_VISIBLE         = 1;
    public const Int32 K_BUTTON_NO_VISIBLE      = 2;

    #endregion

    #region " Public Methods "

    public PlayDrawController() : base()
    {
    } // PlayDrawController

    /// <summary>
    /// Check balance action manager
    /// </summary>
    /// <param name="PreviousController"></param>
    /// <param name="PreviousView"></param>
    /// <returns></returns>
    public ActionResult CheckBalanceActionManager(String PreviousController, String PreviousView)
    {
      // Check Account balance
      if (base.CheckEnoughMoneyToBet(this.SessionData.TerminalDraw.GameParams.TotalPrice()))
      {
        // Show Play Draw Buy Message
        return this.GoToAction(TypeController.PlayDraw, TypeView.PlayDrawBuy, PreviousController, PreviousView);
      }

      // Show No Credit Message
      return this.GoToAction(TypeController.PlayDraw, TypeView.PlayDrawNoCredit, PreviousController, PreviousView);
    } // CheckBalanceActionManager

    /// <summary>
    /// Action Result that returns the view PlayDrawFinished
    /// </summary>
    /// <returns></returns>
    public ActionResult PlayDrawFinished()
    {
      Boolean IsTimeOut = false; // TODO DELETE
      String _view = TypeView.DataAccount;

      if (this.SessionData.Account.InSession && 
          this.SessionData.TerminalDraw.GameParams.GetGameType() != PromoGame.GameType.Welcome &&
          this.SessionData.TerminalDraw.GameParams.FirstOrDefault().TransferScreen)
      {
        _view = TypeView.DataAccountStatus;
      }

      this.SessionData.TerminalDraw.DrawParams = new InTouchTerminalDrawParamsModel();
      this.SessionData.TerminalDraw.GameParams = new List<PromoGame>();

      if (this.SessionData.TransferInProgress)
      {
        this.SessionData.SendTransferAfterPlayGame = true;
      }

      this.SessionData.TransferInProgress = false;

      if (IsTimeOut)
      {
        if (!TerminalDraw.ProcessTerminalDrawEnd(this.SessionData.TerminalDraw.DrawParams.PlaySessionId, 0, true))
        {
          this.SessionData.Message.Type = TYPE_MESSAGE_TO_SHOW.GeneralError;

          return this.ShowMessage();
        }
      }

      return this.GoToAction(TypeController.DataAccount, _view);
    } // PlayDrawFinished

    /// <summary>
    /// Action Result that return the view PlayDrawBuy
    /// </summary>
    /// <returns></returns>
    public ActionResult PlayDrawBuy()
    {
      // Check GameParams
      if (this.SessionData.TerminalDraw.GameParams == null)
      {
        this.SessionData.Message.Type = TYPE_MESSAGE_TO_SHOW.GeneralError;

        this.ShowMessage();
      }

      // Check if is necessary show buy dialog.
      // TODO DELETE JBP: FORZAMOS QUE SIEMPRE SEA CON PREGUNTA Y EN STARTCARDSESSION
      if(false)
      //if (!this.SessionData.TerminalDraw.GameParams.ShowBuyDialog())
      {
        // Check if is necessary insert TerminalDraw recharge
        if (this.MustInsertTerminalDrawRecharge())
        {
          // This action is only for PlayReward & PlayPoint draws, on buy action.

          // Insert Terminal Draw recharge and return to PlayDrawManager          
          return this.GoToAction(TypeController.PlayDraw, TypeView.PlayDrawInsert, this.SessionData.PreviousController, this.SessionData.PreviousAction);
        }

        // Start Draw
        return this.StartDrawManager();
      }

      // Show buy dialog
      return this.ShowBuyDialogAction();
    } // PlayDrawBuy

    /// <summary>
    /// Action Result that return the view PlayDrawCashNoCredit
    /// </summary>
    /// <returns></returns>
    public ActionResult PlayDrawNoCredit()
    {
      InTouchOptionalScreenModel _view_model;
      
      // Set view model
      _view_model = new InTouchOptionalScreenModel( this.GetPlayDrawNoCreditQuestion()
                                                  , this.GetPlayDrawNoCreditAcceptButton()
                                                  , this.GetPlayDrawDescription());
      
      // Set redirect action
      ViewBag.redirect = String.Format(Constants.K_DEFAULT_SIMPLE_REDIRECT_PATH, base.GetWebHost(), this.SessionData.PreviousController, this.SessionData.PreviousAction);

      return View(_view_model);
    } // PlayDrawNoCredit

    // <summary>
    /// Action Result that insert a register into database
    /// </summary>
    /// <returns></returns>
    public ActionResult PlayDrawInsert(String PreviousController, String PreviousAction)
    {
      // IMPORTANT: GamaParams is set in:
      //              - On StartCardSession: AskParticipate or CanParticipate 
      //              - In Session: SetGameParams;
      //                                

      // Add terminal draw recharge when player is in session
      if (!TerminalDraw.AddTerminalDrawRecharge_InSession ( this.GetPlayDrawOperationCode()
                                                          , this.SessionData.Account.Id
                                                          , this.SessionData.Account.Level.LevelId
                                                          , this.SessionData.Account.PointsBalance
                                                          , this.SessionData.Account.TrackData
                                                          , this.SessionData.Account.Pin
                                                          , this.SessionData.SASParams.TerminalName
                                                          , this.SessionData.TerminalDraw.GameParams.PromotionId()
                                                          , this.SessionData.TerminalDraw.GameParams.Id()
                                                          , this.SessionData.TerminalDraw.GameParams.TotalPrice()
                                                          , this.SessionData.TerminalDraw.GameParams.TotalMinimumPrize()
                                                          , this.SessionData.TerminalDraw.GameParams.GetGameType()))
      {
        this.SessionData.Message.Type = TYPE_MESSAGE_TO_SHOW.GeneralError;

        return this.ShowMessage();
      }

      // Start Draw
      return this.GoToAction(TypeController.PlayDraw, TypeView.PlayDrawMenu);

    } // PlayDrawInsert

    /// <summary>
    /// Action Result that return the view PlayDrawMenu
    /// </summary>
    /// <returns>Draw/PlayDrawMenu</returns>
    public ActionResult PlayDrawMenu()
    {
      // Play Cash Button
      this.EnablePlayCashButton();

      // Play Reward Button
      this.EnablePlayRewardButton();

      // Play Points Button
      this.EnablePlayPointButton();

      return View();
    } // PlayDrawMenu

    /// <summary>
    /// Function that redirect to the screen to play at play draws
    /// </summary>
    /// <returns>Redirecto to the correct view</returns>
    public ActionResult PlayDrawCanParticipate(String PreviousController, String PreviousView, Int64 GameId = 0, Int64 GameType = 99)
    {
      // Check Account
      if (!base.CheckAccount())
      {
        // Show CardError Message
        this.SessionData.Message.Type = TYPE_MESSAGE_TO_SHOW.GeneralError;

        return this.ShowMessage();
      }

      // Prepare Game Params
      if(!this.PrepareGameParamsToParticipate(GameId, GameType))
      {
        Log.Add(TYPE_MESSAGE.ERROR, "Error on PlayDrawCanParticipate(). Incorrect GameId or GameType.");
        this.SessionData.Message.Type = TYPE_MESSAGE_TO_SHOW.GeneralError;

        return this.ShowMessage();
      }

      // Set PlayCash GameParams Manager  
      if (!this.PlayDrawSetGameParamsManager())
      {
        // Show Error Message
        this.SessionData.Message.Type = TYPE_MESSAGE_TO_SHOW.GeneralError;

        return this.ShowMessage();
      }

      // Check Balance
      return this.CheckBalanceActionManager(PreviousController, PreviousView);

    } // PlayDrawCanParticipate

    /// <summary>
    /// Set SetGameStartData data
    /// </summary>
    /// <returns></returns>
    public Boolean SetGameStartData(String TypeViewResult)
    {
      try
      {
        // Only on play games when user is in session and not Transfer in progress.
        //   - When player transfer credit (Simulate ExtractCard and InsertCard), 
        //     if player have a pending terminal draw, SASHost start terminal draw process. 
        //     In this case the Game is on StartCardSession.
        if (!this.SessionData.TransferInProgress && this.SessionData.Account.InSession)
        {
          if (!this.GameStartData_InSession())
          {
            Log.Add(TYPE_MESSAGE.ERROR, "Error on GameStartData_InSession. Can't play game");

            return false;
          }

          return true;
        }

        // Only on play games when user is in StartCardSession
        return this.GameStartData_StartCardSession(TypeViewResult);
      }
      catch (Exception _ex)
      {
        Log.Add(_ex);
      }

      return false;
    } // SetGameStartData

    /// <summary>
    /// Get GameModel
    /// </summary>
    /// <returns></returns>
    public GameModel GetGameModel()
    {
      // DrawParams is set in CardPendingDraws() (On CARD INSERTED and Terminal Draws pending)
      // DrawParams is set in GameStartData_InSession().SetTerminalDrawParams() (When players is in session)

      return new GameModel()
      {
        Balance = this.SessionData.Account.MoneyBalance,
        BetAmount = this.SessionData.TerminalDraw.DrawParams.BetAmount,
        WinAmount = this.SessionData.TerminalDraw.DrawParams.WinAmount,
        NewBalance = this.SessionData.TerminalDraw.DrawParams.NewBalance,
      };
    } // GetGameModel

    /// <summary>
    /// Set SetGameResultData data
    /// </summary>
    /// <returns></returns>
    public Boolean SetGameResultData(Int32 ItemId)
    {
      // Only on play games when user is in session and not Transfer in progress.
      //   - When player transfer credit (Simulate ExtractCard and InsertCard), 
      //     if player have a pending terminal draw, SASHost start terminal draw process. 
      //     In this case the Game is on StartCardSession.
      if (!this.SessionData.TransferInProgress && this.SessionData.Account.InSession)
      {
        if (!this.GameResultData_InSession())
        {
          Log.Add(TYPE_MESSAGE.ERROR, "Error on GameResultData_InSession. Can't execute game");

          return false;
        }
      }

      this.GameResultData_SetViewBagData(ItemId);

      return true;
    } // SetGameResultData

    #endregion

    #region " Internal Methods "

    /// <summary>
    /// Show buy dialog action
    /// </summary>
    /// <returns></returns>
    internal ActionResult ShowBuyDialogAction()
    {
      InTouchOptionalScreenModel _view_model;
      
        // Set view model
      _view_model = new InTouchOptionalScreenModel( this.GetPlayDrawQuestion()
                                                  , this.GetPlayDrawAcceptButton()
                                                  , this.GetPlayDrawCancelButton()
                                                  , this.GetPlayDrawDescription());

      // Set redirect action
      ViewBag.DelayRefresh = base.SetRedirectAfterDelayInteractWithHost(TYPE_EVENT.TERMINAL_DRAW_ASK_FOR_PARTICIPATE);

      return View(_view_model);
    } // ShowBuyDialogAction
    
    /// <summary>
    /// Start Draw
    /// </summary>
    /// <returns></returns>
    internal ActionResult StartDrawManager()
    {
      // Manage StartDraw action
      this.Protocol.StartDrawManager();

      // Redirect to Start Draw Action
      return this.GoToAction(this.SessionData.ProtocolController, this.SessionData.ProtocolAction);
    } // StartDrawManager

    #endregion

    #region " Private Methods "

    #region " Optional Screen Model "

    /// <summary>
    /// Get PlayDraw description
    /// </summary>
    /// <returns></returns>
    private InTouchOptionalDescriptionScreenModel GetPlayDrawDescription()
    {
      String _first_line;
      String _second_line;
      String _third_line;
      //String _url_redirect;

      // Get Draw info
      this.Protocol.GetDrawInfo(out _first_line, out _second_line, out _third_line); //, out _url_redirect);

      return new InTouchOptionalDescriptionScreenModel(_first_line, _second_line, _third_line); 
    } // GetPlayDrawDescription

    #region " Play Draw Buy "

    /// <summary>
    /// Get PlayDraw Question
    /// </summary>
    /// <returns></returns>
    private InTouchOptionalQuestionScreenModel GetPlayDrawQuestion()
    {
      return new InTouchOptionalQuestionScreenModel(nls_resource.pd_draw_buy_question, this.GetPlayDrawBuyQuestionByGameType());
    } // GetPlayDrawQuestion

    /// <summary>
    /// Get PlayDraw buy button
    /// </summary>
    /// <returns></returns>
    private InTouchOptionalButtonScreenModel GetPlayDrawAcceptButton()
    {
      // if is necessary insert terminal draw recharge
      if(this.MustInsertTerminalDrawRecharge())
      {
        return new InTouchOptionalButtonScreenModel(nls_resource.generic_yes_button, TypeController.PlayDraw, TypeView.PlayDrawInsert);
      }

      // By default, interact with host.
      return new InTouchOptionalButtonScreenModel(nls_resource.generic_yes_button, "InteractWithHost('30|2');");
    } // GetPlayDrawBuyButton

    /// <summary>
    /// Get PlayDraw cancel button
    /// </summary>
    /// <returns></returns>
    private InTouchOptionalButtonScreenModel GetPlayDrawCancelButton()
    {
      return new InTouchOptionalButtonScreenModel(nls_resource.generic_no_button, "InteractWithHost('30|1');");
    } // GetPlayDrawCancelButton

    #endregion 

    #region " PlayDraw No Credit "

    /// <summary>
    /// Get PlayDraw no credit Question
    /// </summary>
    /// <returns></returns>
    private InTouchOptionalQuestionScreenModel GetPlayDrawNoCreditQuestion()
    {
      return new InTouchOptionalQuestionScreenModel(nls_resource.pd_cash_nocredit_question);
    } // GetPlayDrawNoCreditQuestion

    /// <summary>
    /// Get PlayDraw no credit button
    /// </summary>
    /// <returns></returns>
    private InTouchOptionalButtonScreenModel GetPlayDrawNoCreditAcceptButton()
    {
      // if is necessary insert terminal draw recharge
      if (this.MustInsertTerminalDrawRecharge())
      {
        return new InTouchOptionalButtonScreenModel(nls_resource.generic_accept_button, TypeController.PlayDraw, TypeView.PlayDrawInsert);
      }

      // Show previous action
      return new InTouchOptionalButtonScreenModel(nls_resource.generic_accept_button, this.SessionData.PreviousController, this.SessionData.PreviousAction);
    } // GetPlayDrawNoCreditAcceptButton

    #endregion 

    #endregion
        
    /// <summary>
    /// Prepare GameParams to participate
    /// </summary>
    /// <param name="GameId"></param>
    /// <param name="GameType"></param>
    /// <returns></returns>
    private Boolean PrepareGameParamsToParticipate(Int64 GameId, Int64 GameType)
    {
      Int64 _game_id;
      PromoGame.GameType _game_type;

      // Set GameId
      if(!this.SetGameId(GameId, out _game_id))
      {
        return false;
      }

      // Set GameType
      if (!this.SetGameType((PromoGame.GameType)GameType, out _game_type))
      {
        return false;
      }

      // Set Game Params
      this.Protocol.SetGameParams(_game_id, _game_type);

      return true;
    } // PrepareGameParamsToParticipate

    /// <summary>
    /// Set game id
    /// </summary>
    /// <param name="GameId"></param>
    /// <param name="ReturnGameId"></param>
    /// <returns></returns>
    private Boolean SetGameId(Int64 GameId, out Int64 ReturnGameId)
    {
      ReturnGameId = 0;

      // InSession: PlayReward or PlayPoint request
      if (GameId > 0)
      {
        ReturnGameId = GameId;

        return true;
      }

      // Start card session: All
      if (this.SessionData.TerminalDraw.GameParams.Id() > 0)
      {
        ReturnGameId = this.SessionData.TerminalDraw.GameParams.Id();

        return true;
      }

      return false;
    } // SetGameId

    /// <summary>
    /// Set Game type
    /// </summary>
    /// <param name="GameType"></param>
    /// <param name="ReturnGameType"></param>
    /// <returns></returns>
    private Boolean SetGameType(PromoGame.GameType GameType, out PromoGame.GameType ReturnGameType)
    {
      ReturnGameType = PromoGame.GameType.Unknown;

      // InSession: PlayReward or PlayPoint request
      if ( GameType != PromoGame.GameType.Unknown )
      {
        ReturnGameType = GameType;

        return true;
      }

      // Start card session: All
      if(this.SessionData.TerminalDraw.GameParams.GetGameType() != PromoGame.GameType.Unknown)
      {
        ReturnGameType = this.SessionData.TerminalDraw.GameParams.GetGameType();

        return true;
      }

      return false;
    } // SetGameType

    /// <summary>
    /// Set PlayDraw Game Params manager
    /// </summary>
    private Boolean PlayDrawSetGameParamsManager()
    {
      List<PromoGame> _playdraw_games;

      // If is needed an insert, is not necessary get pending terminal draws
      if(this.MustInsertTerminalDrawRecharge())
    {
        return true;
      }

      // WelcomeDraw GameParams must set in SetGameParams_WelcomeDraw()
      if (this.SessionData.TerminalDraw.GameParams.GetGameType() == PromoGame.GameType.Welcome)
      {
        return true;
      }

      //// IMPORTANT: 
      //  - Some GameParams are set in AskParticipate or EnablePlayCashButton:
      //      · GameId
      //      · GameType

      // Get all pending PromoGame draws.
      //   - IMPORTANT: PlayCash always gets from TERMINAL_DRAWS_RECHARGES 
      if (!this.SessionData.TerminalDraw.GetPendingPlayDraws(this.SessionData.Account.Id
                                                            , this.SessionData.TerminalDraw.GameParams.Id()
                                                            , this.SessionData.TerminalDraw.GameParams.GetGameType()
                                                            , out _playdraw_games))
      {
        return false;
      }

      // Set pending PromoGame draws
      this.SessionData.TerminalDraw.GameParams = _playdraw_games;
      

      return true;

    } // PlayDrawSetGameParamsManager

    /// <summary>
    /// Checks if is necessary insert in terminal draw recharges
    /// </summary>
    /// <returns></returns>
    private Boolean MustInsertTerminalDrawRecharge()
    {
      // If Players is in session and GameType is PlayReward or PlayPoint:
      //   - Is necessary add pending terminal draw recharge.
      return ( this.SessionData.Account.InSession
            && ( this.SessionData.TerminalDraw.GameParams.GetGameType() == PromoGame.GameType.PlayReward
              || this.SessionData.TerminalDraw.GameParams.GetGameType() == PromoGame.GameType.PlayPoint));

    } // MustInsertTerminalDrawRecharge

    /// <summary>
    /// Get PlayDraw Insert action
    /// </summary>
    /// <returns></returns>
    private String GetPlayDrawInsertAction()
    {
      switch (this.SessionData.TerminalDraw.GameParams.GetGameType())
      {
        case PromoGame.GameType.PlayReward:
          return TypeView.PlayRewardStart;

        case PromoGame.GameType.PlayPoint:
          return TypeView.PlayPointStart;
      }

      Log.Add(TYPE_MESSAGE.ERROR, "Error on GetPlayDrawInsertAction(): Unknown GameType.");

      return TypeView.Show;
    } // GetPlayDrawInsertAction

    /// <summary>
    /// GetPlayDrawBuy controller
    /// </summary>
    /// <returns></returns>
    private String GetPlayDrawInsertController()
    {
      switch (this.SessionData.TerminalDraw.GameParams.GetGameType())
      {
        case PromoGame.GameType.PlayReward:
          return TypeController.PlayReward;

        case PromoGame.GameType.PlayPoint:
          return TypeController.PlayPoint;
      }

      Log.Add(TYPE_MESSAGE.ERROR, "Error on GetPlayDrawInsertController(): Invalid GameType.");

      return TypeController.Message;
    } // GetPlayDrawBuyController
    
    /// <summary>
    /// Get play draw operation code
    /// </summary>
    /// <returns></returns>
    private OperationCode GetPlayDrawOperationCode()
    {
      switch (this.SessionData.TerminalDraw.GameParams.GetGameType())
      {
        case PromoGame.GameType.PlayReward:
          return OperationCode.PLAYREWARD_DRAW;

        case PromoGame.GameType.PlayPoint:
          return OperationCode.PLAYPOINT_DRAW;
      }

      Log.Add(TYPE_MESSAGE.ERROR, "Error on GetPlayDrawBuyController(): Unknown GameType.");

      return OperationCode.NOT_SET;
    } // GetPlayDrawOperationCode

    /// <summary>
    /// Returns Play draw by question by GameType
    /// </summary>
    /// <returns></returns>
    private String GetPlayDrawBuyQuestionByGameType()
    {
      switch (this.SessionData.TerminalDraw.GameParams.GetGameType())
      {
        case PromoGame.GameType.Welcome:
          return nls_resource.welRaf_question_bold;

        case PromoGame.GameType.PlayCash:
          return nls_resource.pd_playcash;

        case PromoGame.GameType.PlayReward:
          return nls_resource.pd_playreward;

        case PromoGame.GameType.PlayPoint:
          return nls_resource.pd_playpoint;
      }

      Log.Add(TYPE_MESSAGE.ERROR, "Error on GetPlayDrawBuyQuestionByGameType(): Unknown GameType.");

      return String.Empty;
    } // GetPlayDrawBuyQuestionByGameType

    /// <summary>
    /// Get game start data when is StartCardSession
    /// </summary>
    /// <param name="TypeViewResult"></param>
    /// <returns></returns>
    private Boolean GameStartData_StartCardSession(String TypeViewResult)
    {
      Int32 _timeout;

      if (this.SessionData.TerminalDraw.DrawParams == null || !this.SessionData.TerminalDraw.DrawParams.ParamsOk)
      {
        return false;
      }

      // Set Account Promotion status to bought
      if (!TerminalDraw.TerminalDrawSetPromotionStatusToBought(this.SessionData.Account.Id
                                                              , this.SessionData.SASParams.TerminalId
                                                              , this.SessionData.TerminalDraw.DrawParams.GameId))
      {
        return false;
      }

      _timeout = Math.Max(this.SessionData.TerminalDraw.DrawParams.TimeOut, InTouchConstants.K_TimeInSecondsDraw);
      this.ViewBag.DelayRefresh = base.SetRedirectAfterDelay(TypeController.PlayDraw, TypeViewResult, _timeout);

      return true;
    } // GameStartData_StartCardSession

    /// <summary>
    /// Get game start data when is in session
    /// </summary>
    /// <returns></returns>
    private Boolean GameStartData_InSession()
    {
      TerminalDrawAccountEntity _account_entity;

      // Check if session params are Ok
      if (!this.CheckSessionParams())
      {
        Log.Add(TYPE_MESSAGE.ERROR, "Error on CheckSessionParams().");

        return false;
      }

      // Get Pending draws
      if (!TerminalDraw.TerminalDrawGetPendingDraw(this.SessionData.Account.Id
                                                  , this.SessionData.SASParams.TerminalId
                                                  , this.SessionData.TerminalDraw.GameParams.Id()
                                                  , out _account_entity))
      {
        return false;
      }

      // Set SessionData.TerminalDraw.DrawParams
      this.SetTerminalDrawParams(_account_entity);

      return true;
    } // GameStartData_InSession

    /// <summary>
    /// Check session data params
    /// </summary>
    /// <returns></returns>
    private Boolean CheckSessionParams()
    {
      return (this.SessionData.Account.Id > 0
            && this.SessionData.SASParams.TerminalId > 0
            && this.SessionData.TerminalDraw.GameParams != null
            && this.SessionData.TerminalDraw.GameParams.Id() > 0);

    } // CheckSessionParams

    /// <summary>
    /// Set game result ViewBag data
    /// </summary>
    /// <param name="TypeViewResult"></param>
    /// <returns></returns>
    private void GameResultData_SetViewBagData(Int32 ItemId)
    {
      // SessionData.TerminalDraw.DrawParams is set on ShowDrawResult() (When SASHost send TERMINAL_DRAW_GAME_RESULT TO WEB).
      Int32 _timeout;

      this.ViewBag.DrawResultText = nls_resource.gam_loose;
      if (this.SessionData.TerminalDraw.DrawParams.IsWinner)
      {
        this.ViewBag.DrawResultText = String.Format("{0} {1}", nls_resource.gam_win, this.SessionData.TerminalDraw.DrawParams.WinAmount.ToString());
      }

      this.ViewBag.DrawBalance = String.Format("{0} {1}", nls_resource.gam_user_balance, this.SessionData.TerminalDraw.DrawParams.Balance.ToString());

      // Redirection
      _timeout = Math.Max(this.SessionData.TerminalDraw.DrawParams.TimeOut, InTouchConstants.K_TimeInSecondsDraw);
      this.ViewBag.DelayRefresh = base.SetRedirectAfterDelay(TypeController.PlayDraw, TypeView.PlayDrawFinished, _timeout);

      // Select item
      this.SetSelectedItem(ItemId);
    } // GameResultData_SetViewBagData

    /// <summary>
    /// Get game result data when is in session
    /// </summary>
    /// <returns></returns>
    private Boolean GameResultData_InSession()
    {
      TerminalDrawPrizePlanGames _prize_plan_games;

      if (!TerminalDraw.TerminalDrawProcessDraw(this.SessionData.SASParams.TerminalId
                                                , this.SessionData.TerminalDraw.AccountEntity
                                                , this.SessionData.TerminalDraw.GameParams.Id()
                                                , out _prize_plan_games))
      {
        return false;
      }

      // Set SessionData.TerminalDraw.DrawParams
      this.SetTerminalDrawParams(_prize_plan_games.Games.PlaySessionId()
                                , _prize_plan_games.Games.TotalBetAmount()
                                , _prize_plan_games.Games.TotalWinAmount()
                                , _prize_plan_games.Games.IsWinner()
                                , _prize_plan_games.Games.TimeOut());

      return true;
    } // GameResultData_InSession

    /// <summary>
    /// Select item in html game
    /// </summary>
    /// <param name="ItemId"></param>
    private void SetSelectedItem(int ItemId)
    {
      this.ViewBag.item1 = 0;
      this.ViewBag.item2 = 0;
      this.ViewBag.item3 = 0;

      switch (ItemId)
      {
        case 1: this.ViewBag.item1 = 1; break;
        case 2: this.ViewBag.item2 = 1; break;
        case 3: this.ViewBag.item3 = 1; break;

        default: this.ViewBag.item2 = 1; break;
      }

    } // SetSelecteditem

    /// <summary>
    /// To enable/disable the play cash button
    /// </summary>
    private void EnablePlayCashButton()
    {
      List<PromoGame> _playcash_games;

      // Recuperamos los juegos pendientes por AccountID
      if (this.SessionData.TerminalDraw.GetPendingPlayDraws(this.SessionData.Account.Id, PromoGame.GameType.PlayCash, out _playcash_games))
      {
        // Get Pending PlayChas draws
        this.SessionData.TerminalDraw.GameParams = _playcash_games;

        ViewBag.redirectPlayCash = String.Format(Constants.K_DEFAULT_REDIRECT_PATH
                                                , base.GetWebHost()
                                                , TypeController.PlayDraw
                                                , TypeView.PlayDrawCanParticipate
                                                , TypeController.PlayDraw
                                                , TypeView.PlayDrawMenu);
        ViewBag.Disabled = String.Empty;
      }
      else // Disabled
      {
        ViewBag.Disabled = "disabled";
      }
    } // EnablePlayCashButton

    /// <summary>
    /// /// To enable/disable the play reward button
    /// </summary>
    private void EnablePlayRewardButton()
    {
      ViewBag.RedirectPlayRewards = String.Format(Constants.K_DEFAULT_REDIRECT_PATH
                                                  , base.GetWebHost()
                                                  , TypeController.PlayReward
                                                  , TypeView.PlayRewardMenu
                                                  , TypeController.PlayDraw
                                                  , TypeView.PlayDrawMenu);
    } // EnablePlayRewardButton

    /// <summary>
    /// To enable/disable the play points button
    /// </summary>
    private void EnablePlayPointButton()
    {
      ViewBag.redirectPlayPoints = String.Format(Constants.K_DEFAULT_REDIRECT_PATH
                                                , base.GetWebHost()
                                                , TypeController.PlayPoint
                                                , TypeView.PlayPointMenu
                                                , TypeController.PlayDraw
                                                , TypeView.PlayDrawMenu);
    } // EnablePlayPointButton


    /// <summary>
    /// Set Session.TerminalDraw.DrawParams
    /// </summary>
    /// <param name="TotalBetAmount"></param>
    /// <param name="WinAmount"></param>
    private void SetTerminalDrawParams(TerminalDrawAccountEntity AccountEntity)
    {
      Int64 _time_out;

      this.SessionData.TerminalDraw.AccountEntity = AccountEntity;

      _time_out = Math.Max(AccountEntity.Games.TimeOut(), InTouchConstants.K_TimeInSecondsDraw);

      // WinAmount = 0 because the draw not executed.
      this.SetTerminalDrawParams(AccountEntity.Games.PlaySessionId(), AccountEntity.Games.TotalBetAmount(), 0, false, _time_out);

    } // SetTerminalDrawParams
    private void SetTerminalDrawParams(Int64 PlaySessionId, Decimal TotalBetAmount, Decimal WinAmount, Boolean IsWinner, Int64 TimeOut)
    {
      Int32 _time_out;

      if (!Int32.TryParse(TimeOut.ToString(), out _time_out)) // TODO REVISE
      {
        _time_out = 0;
      }

      if (PlaySessionId <= 0)
      {
        Log.Add(TYPE_MESSAGE.WARNING, "Unknown PlaySessionId!");
      }

      this.SessionData.TerminalDraw.DrawParams = new InTouchTerminalDrawParamsModel()
      {
        // Params Data
        PlaySessionId = PlaySessionId,
        BetAmount = TotalBetAmount,
        WinAmount = WinAmount,
        IsWinner = IsWinner,
        TimeOut = Math.Max(InTouchConstants.K_TimeInSecondsDraw, _time_out),

        // Session data
        AccountId = this.SessionData.Account.Id,
        Balance = this.SessionData.Account.MoneyBalance,
        NewBalance = this.SessionData.Account.MoneyBalance + WinAmount,
        GameId = this.SessionData.TerminalDraw.GameParams.Id(),
        Url = this.SessionData.TerminalDraw.GameParams.GameUrl(),

        // Default values
        AutoDraw = false,
        IsForced = false,
        ParamsOk = true,
        RunDraw = true,
        ShowResult = true
      };
    } // SetTerminalDrawParams

    #endregion

  } // PlayDrawController
}