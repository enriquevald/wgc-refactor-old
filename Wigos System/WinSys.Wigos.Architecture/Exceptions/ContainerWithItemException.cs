﻿using System;
using System.Runtime.Serialization;

namespace WinSys.Wigos.Architecture.Exceptions
{
    /// <summary>
    /// Exception that contains an analyzed source object and its source item where the exceptions was thrown
    /// </summary>
    /// <typeparam name="TSourceContainer">Source container type</typeparam>
    /// <typeparam name="TSourceItem">Source item type</typeparam>
    public class ContainerWithItemException<TSourceContainer, TSourceItem> : Exception
    {
        private readonly TSourceContainer sourceContainer;
        private readonly TSourceItem sourceItem;

        /// <summary>
        /// Obtains the source where is located the SourceItem
        /// </summary>
        public TSourceContainer SourceContainer
        {
            get
            {
                return this.sourceContainer;
            }
        }

        /// <summary>
        /// Obtains the item which throws the exception
        /// </summary>
        public TSourceItem SourceItem
        {
            get
            {
                return this.sourceItem;
            }
        }

        public ContainerWithItemException() 
            : base()
        {
            this.sourceContainer = default(TSourceContainer);
            this.sourceItem = default(TSourceItem);
        }

        public ContainerWithItemException(
            TSourceContainer sourceContainer,
            TSourceItem sourceItem)
            : base()
        {
            this.sourceContainer = sourceContainer;
            this.sourceItem = sourceItem;
        }

        public ContainerWithItemException(string message) 
            : base(message)
        {
            this.sourceContainer = default(TSourceContainer);
            this.sourceItem = default(TSourceItem);
        }

        public ContainerWithItemException(
            TSourceContainer sourceContainer,
            TSourceItem sourceItem, 
            string message) 
            : base(message)
        {
            this.sourceContainer = sourceContainer;
            this.sourceItem = sourceItem;
        }

        public ContainerWithItemException(
            string message, 
            Exception innerException) 
            : base(message, innerException)
        {
            this.sourceContainer = default(TSourceContainer);
            this.sourceItem = default(TSourceItem);
        }

        public ContainerWithItemException(
            TSourceContainer sourceContainer,
            TSourceItem sourceItem, 
            string message, 
            Exception innerException) 
            : base(message, innerException)
        {
            this.sourceContainer = sourceContainer;
            this.sourceItem = sourceItem;
        }

        public ContainerWithItemException(
            SerializationInfo info, 
            StreamingContext context) 
            : base(info, context)
        {
            this.sourceContainer = default(TSourceContainer);
            this.sourceItem = default(TSourceItem);
        }
    }
}