﻿namespace WinSys.Wigos.Architecture.Patterns
{
	/// <summary>
	/// Implements a factory
	/// </summary>
	/// <typeparam name="TProductSelector">Type of the parameter that the factory will use to determine the <code>TProductOut</code> to create</typeparam>
	/// <typeparam name="TProductOut">Output product type</typeparam>
	public interface IFactory<in TProductSelector, out TProductOut>
	{
		/// <summary>
		/// Create a product of specified type
		/// </summary>
		/// <param name="productSelector">Type of the product to create</param>
		/// <returns>Created product</returns>
		TProductOut Create(TProductSelector productSelector);
	}
}