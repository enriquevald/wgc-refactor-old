﻿namespace WinSys.Wigos.Architecture.Patterns.Command
{
	/// <summary>
	/// Implements a command factory
	/// </summary>
	/// <typeparam name="TCommandSelector">Type of the parameter that the factory will use to determine the command to create</typeparam>
	public interface ICommandFactory<in TCommandSelector> : IFactory<TCommandSelector, ICommand>
	{
	}
}