﻿namespace WinSys.Wigos.Architecture.Patterns.Command
{
	/// <summary>
	/// Implements a command
	/// </summary>
	public interface ICommand
	{
		/// <summary>
		/// Execute the command
		/// </summary>
		void Execute();
	}
}