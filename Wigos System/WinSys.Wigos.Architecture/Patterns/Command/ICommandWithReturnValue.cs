﻿namespace WinSys.Wigos.Architecture.Patterns.Command
{
	/// <summary>
	/// Implements a command with a value to return
	/// </summary>
	/// <typeparam name="TOut">Type of the returned value</typeparam>
	public interface ICommandWithReturnValue<out TOut> : ICommand
	{
		/// <summary>
		/// Get the returned value of the command after its execution
		/// </summary>
		/// <returns>Value</returns>
		TOut GetValue();
	}
}