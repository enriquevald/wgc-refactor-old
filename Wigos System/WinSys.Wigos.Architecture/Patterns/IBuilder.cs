﻿namespace WinSys.Wigos.Architecture.Patterns
{
	/// <summary>
	/// Implements a builder
	/// </summary>
	/// <typeparam name="TOutput">Output type</typeparam>
	public interface IBuilder<out TOutput>
	{	
		/// <summary>
		/// Performs de construction of the object 
		/// </summary>
		/// <returns>Builded object generated</returns>
		TOutput Build();

		/// <summary>
		/// Reset current build status to perform a build from an initial status
		/// </summary>
		void Reset();
	}
}