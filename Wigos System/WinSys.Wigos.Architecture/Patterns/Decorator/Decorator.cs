﻿namespace WinSys.Wigos.Architecture.Patterns.Decorator
{
	/// <summary>
	/// Implements base class for Decorator
	/// </summary>
	public abstract class Decorator<TComponent> : IDecorator<TComponent>
	{
		/// <summary>
		/// Decorated component
		/// </summary>
		protected readonly TComponent Component;

		protected Decorator(TComponent component)
		{
			this.Component = component;
		}
	}
}