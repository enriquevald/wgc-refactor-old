﻿namespace WinSys.Wigos.Architecture.Patterns.Decorator
{
	/// <summary>
	/// Implements a component's decorator
	/// </summary>
	/// <typeparam name="TComponent">Type of the component to decorate</typeparam>
	public interface IDecorator<TComponent>
	{
	}
}