﻿namespace WinSys.Wigos.Architecture.Patterns
{
	/// <summary>
	/// Implements an adapter
	/// </summary>
	/// <typeparam name="TSource">Source type</typeparam>
	/// <typeparam name="TDestination">Destination type</typeparam>
	public interface IAdapter<in TSource, out TDestination>
	{
		/// <summary>
		/// Perform the adaptation from a especified source
		/// </summary>
		/// <param name="source">Source to make the adaptation</param>
		/// <returns>Result of adaptation</returns>
		TDestination Adapt(TSource source);
	}
}