﻿using System;

namespace WinSys.Wigos.Architecture.Services
{
	/// <summary>
	/// Implements a serializer for XML
	/// </summary>
	public interface IXmlSerializerService
	{
		/// <summary>
		/// Serialize a specified data
		/// </summary>
		/// <param name="type">Data type</param>
		/// <param name="data">Data to serialize</param>
		/// <returns>Serialized object</returns>
		string Serialize(Type type, object data);

		/// <summary>
		/// Serialize a specified data
		/// </summary>
		/// <typeparam name="T">Data type</typeparam>
		/// <param name="data">Data to serialize</param>
		/// <returns>Serialized object</returns>
		string Serialize<T>(T data);

		/// <summary>
		/// Deserialize a specified data
		/// </summary>
		/// <param name="type">Data type</param>
		/// <param name="data">Data to serialize</param>
		/// <returns>Deserialized object</returns>
		object Deserialize(Type type, string data);

		/// <summary>
		/// Deserialize a specified data
		/// </summary>
		/// <typeparam name="T">Data type</typeparam>
		/// <param name="data">Data to serialize</param>
		/// <returns>Deserialized typed object</returns>
		T Deserialize<T>(string data);
	}
}