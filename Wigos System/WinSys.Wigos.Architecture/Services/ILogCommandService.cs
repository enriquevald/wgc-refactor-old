﻿using System;

namespace WinSys.Wigos.Architecture.Services
{
	/// <summary>
	/// Implements log for commands service
	/// </summary>
	public interface ILogCommandService
	{
		void Information(object command, string message);
		void Warning(object command, string message);
		void Error(object command, string message);
		void Exception(object command, Exception exception);
		void Exception(object command, string message, Exception exception);
	}
}