﻿using System;
using WinSys.Wigos.Architecture.Enums;

namespace WinSys.Wigos.Architecture.Services
{
	/// <summary>
	/// Implements a log service
	/// </summary>
	public interface ILogService
	{
		LogLevel LogLevel { get; }

		void Information(string message);
		void Warning(string message);
		void Error(string message);
		void Exception(Exception exception);
		void Exception(string message, Exception exception);
	}
}