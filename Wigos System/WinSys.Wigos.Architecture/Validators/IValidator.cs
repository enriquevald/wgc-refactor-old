﻿namespace WinSys.Wigos.Architecture.Validators
{
	/// <summary>
	/// Implements a validator of type T
	/// </summary>
	public interface IValidator<in T>
	{
		/// <summary>
		/// Validate the specified item
		/// </summary>
		/// <param name="item">Item to validate</param>
		void Validate(T item);
	}
}