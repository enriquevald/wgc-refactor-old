﻿using System.Xml.Linq;

namespace WinSys.Wigos.Architecture.Validators
{
	/// <summary>
	/// Implements a syntax validator
	/// </summary>
	public interface ISyntaxValidator : IValidator<XDocument>
	{
	}
}