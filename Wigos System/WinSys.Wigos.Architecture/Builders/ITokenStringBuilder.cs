﻿using System.Collections.Generic;
using WinSys.Wigos.Architecture.Patterns;

namespace WinSys.Wigos.Architecture.Builders
{
	/// <summary>
	/// Implements a string builder with specified token format
	/// </summary>
	public interface ITokenStringBuilder : IBuilder<string>
	{
		/// <summary>
		/// Adds a new token with a simple text
		/// </summary>
		/// <param name="text">Text to include into token</param>
		void AddSingleToken(string text);

		/// <summary>
		/// Adds a new token with a list of text separated by commas
		/// </summary>
		/// <param name="texts">Texts list to include into token</param>
		void AddCommaSeparatedToken(params string[] texts);

		/// <summary>
		/// Adds a new token with a list of KeyValuePair values separated by commas
		/// </summary>
		/// <param name="keyValuePairs">KeyPairValues to include into token</param>
		void AddCommaSeparatedKeyValuePairToken(params KeyValuePair<string, string>[] keyValuePairs);
	}
}