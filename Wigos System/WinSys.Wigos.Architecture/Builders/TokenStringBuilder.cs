﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WinSys.Wigos.Architecture.Builders
{
	/// <summary>
	/// Implements a string builder with specified token format
	/// </summary>
	public class TokenStringBuilder : ITokenStringBuilder
	{
		private const string OpenToken = "[";
		private const string CommaToken = ", ";
		private const string CloseToken = "]";
		private const string KeyValuePairTemplate = "{0}='{1}'";

		private readonly StringBuilder stringBuilder;

		public TokenStringBuilder()
		{
			this.stringBuilder = new StringBuilder();
		}

		/// <summary>
		/// Performs de construction of the object 
		/// </summary>
		/// <returns>Builded object generated</returns>
		public string Build()
		{
			return this.stringBuilder.ToString();
		}

		/// <summary>
		/// Reset current build status to perform a build from an initial status
		/// </summary>
		public void Reset()
		{
			this.stringBuilder.Clear();
		}

		/// <summary>
		/// Adds a new token with a simple text
		/// </summary>
		/// <param name="text">Text to include into token</param>
		public void AddSingleToken(string text)
		{
			this.stringBuilder.Append(TokenStringBuilder.OpenToken);
			this.stringBuilder.Append(text);
			this.stringBuilder.Append(TokenStringBuilder.CloseToken);
		}

		/// <summary>
		/// Adds a new token with a list of text separated by commas
		/// </summary>
		/// <param name="texts">Texts list to include into token</param>
		public void AddCommaSeparatedToken(params string[] texts)
		{
			this.stringBuilder.Append(TokenStringBuilder.OpenToken);
			bool isFirstCycle = true;
			foreach (string text in texts)
			{
				if (!isFirstCycle)
				{
					this.stringBuilder.Append(TokenStringBuilder.CommaToken);
				}
				this.stringBuilder.Append(text);
				isFirstCycle = false;
			}

			this.stringBuilder.Append(TokenStringBuilder.CloseToken);
		}

		/// <summary>
		/// Adds a new token with a list of KeyValuePair values separated by commas
		/// </summary>
		/// <param name="keyValuePairs">KeyPairValues to include into token</param>
		public void AddCommaSeparatedKeyValuePairToken(params KeyValuePair<string, string>[] texts)
		{
			string[] formattedItems = texts.Select(item => string.Format(TokenStringBuilder.KeyValuePairTemplate, item.Key, item.Value)).ToArray();
			this.AddCommaSeparatedToken(formattedItems);
		}
	}
}