//------------------------------------------------------------------------------
// Copyright � 2010 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : frm_handpay_manual.cs
// 
//   DESCRIPTION : Implements the following classes:
//                 1. frm_handpay_manual: For manual handpays input.
//
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 01-JUL-2010 RCI    First release.
// 22-JUL-2010 TJG    Handpays implementation
// 23-NOV-2011 RCI    Have to adapt Voucher call due to the optional sections in Handpay vouchers.
// 02-DEC-2011 JMM    Adding denomination to Handpay vouchers
// 13-MAY-2013 NMR    Controlling entered amount with configured global limit.
// 03-JUL-2013 RRR    Moddified view parameters for Window Mode
// 25-OCT-2013 FBA    Fixed bug WIG-307
// 17-JUN-2014 JBC    Fixed bug WIG-342: Added Cashdesk draws terminals into query to hide.
// 11-NOV-2015 FOS    Product Backlog Item: 3709 Payment tickets & handpays
// 18-SEP-2017 DPC    WIGOS-5268: Cashier & GUI - Icons - Implement
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WSI.Common;
using System.Data.SqlClient;
using System.Globalization;

namespace WSI.Cashier
{
  public partial class frm_handpays_manual : Controls.frm_base_icon
  {
    #region Constants

    private const Int32 GRID_MAX_COLUMNS = 3;

    private const Int32 GRID_COLUMN_TERM_ID = 0;
    private const Int32 GRID_COLUMN_TERM_PROVIDER = 1;
    private const Int32 GRID_COLUMN_TERM_NAME = 2;
    #endregion

    #region Structures

    //private struct TYPE_TERMINAL_DATA
    //{
    //  public Int32 id;
    //  public String name;
    //}

    #endregion

    #region Attributes

    private frm_yesno form_yes_no;

    private static String m_decimal_str = System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencyDecimalSeparator;
    private static Boolean m_cancel = true;

    private static HandPay m_handpay;

    #endregion

    #region Constructor

    /// <summary>
    /// Constructor method.
    /// 1. Initialize controls.
    /// 2. Initialize control�s resources. (NLS, Images, etc)
    /// </summary>
    public frm_handpays_manual(CardData RelatedCard)
    {
      InitializeComponent();

      InitializeControlResources();

      m_handpay = new HandPay(RelatedCard);

      form_yes_no = new frm_yesno();
      form_yes_no.Opacity = 0.6;

      InitHandpaysManual();
    }

    #endregion

    #region Buttons

    private void btn_num_1_Click(object sender, EventArgs e)
    {
      AddNumber("1");
    }

    private void btn_num_2_Click(object sender, EventArgs e)
    {
      AddNumber("2");
    }

    private void btn_num_3_Click(object sender, EventArgs e)
    {
      AddNumber("3");
    }

    private void btn_num_4_Click(object sender, EventArgs e)
    {
      AddNumber("4");
    }

    private void btn_num_5_Click(object sender, EventArgs e)
    {
      AddNumber("5");
    }

    private void btn_num_6_Click(object sender, EventArgs e)
    {
      AddNumber("6");
    }

    private void btn_num_7_Click(object sender, EventArgs e)
    {
      AddNumber("7");
    }

    private void btn_num_8_Click(object sender, EventArgs e)
    {
      AddNumber("8");
    }

    private void btn_num_9_Click(object sender, EventArgs e)
    {
      AddNumber("9");
    }

    private void btn_num_10_Click(object sender, EventArgs e)
    {
      AddNumber("0");
    }

    private void btn_num_dot_Click(object sender, EventArgs e)
    {
      // Only one decimal separator is allowed
      if (txt_amount.Text.IndexOf(m_decimal_str) == -1)
      {
        txt_amount.Text = txt_amount.Text + m_decimal_str;
      }

      btn_intro.Focus();
    }

    private void btn_cancel_Click(object sender, EventArgs e)
    {
      m_cancel = true;

      Misc.WriteLog("[FORM CLOSE] frm_handpays_manual (cancel)", Log.Type.Message);
      this.Close();
    }

    private void btn_back_Click(object sender, EventArgs e)
    {
      if (txt_amount.Text.Length > 0)
      {
        txt_amount.Text = txt_amount.Text.Substring(0, txt_amount.Text.Length - 1);
      }
      btn_intro.Focus();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Handle the click on the ok button, which confirms all shown data
    //           and closes the screen.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    private void btn_ok_Click(object sender, EventArgs e)
    {
      Boolean _done;

      _done = false;
      try
      {
        this.btn_ok.Enabled = false;

        if (!m_handpay.HandpayMsg(Handpays.HANDPAY_MSG.REGISTER_CONF, this.ParentForm, false))
        {
          return;
        }

        // Process the selected handpay
        if (!m_handpay.Register(WGDB.Now))
        {

          switch (m_handpay.HandpayErrorMsg)
          {
            case Handpays.HANDPAY_MSG.REGISTER_ERROR_ALREADY_PAID:
              m_handpay.HandpayMsg(Handpays.HANDPAY_MSG.REGISTER_ERROR_ALREADY_PAID, this.ParentForm, false);
              break;

            case Handpays.HANDPAY_MSG.REGISTER_ERROR_VOIDED:
              m_handpay.HandpayMsg(Handpays.HANDPAY_MSG.REGISTER_ERROR_VOIDED, this.ParentForm, false);
              break;

            default:
              m_handpay.HandpayMsg(Handpays.HANDPAY_MSG.REGISTER_ERROR, this.ParentForm, false);
              break;
          }

          return;
        }

        m_cancel = false;

        _done = true;

      }
      finally
      {
        if (!_done)
        {
          this.btn_ok.Enabled = true;

          this.Focus();
        }
        else
        {
          Misc.WriteLog("[FORM CLOSE] frm_handpays_manual (ok)", Log.Type.Message);
          this.Close();
        }
      }

    } // btn_ok_Click

    //------------------------------------------------------------------------------
    // PURPOSE : Handle the click on the intro button, which confirms the amount on the
    //           numeric pad.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //

    private void btn_intro_Click(object sender, EventArgs e)
    {
      DataGridViewRow _view_row;
      DataRow _row;
      decimal _input_amount;
      decimal _max_allowed_manual_entry;

      btn_ok.Enabled = false;

      // Check before proceeding
      //    - Terminal selection
      //    - Valid amount entered

      //    - Terminal selection
      if (dgv_list.RowCount < 1)
      {
        Log.Error("btn_intro_Click. ManualHandpay." + Resource.String("STR_FRM_HANDPAYS_MANUAL_007"));

        frm_message.Show(Resource.String("STR_FRM_HANDPAYS_MANUAL_007"),
                          Resource.String("STR_APP_GEN_MSG_ERROR"),
                          MessageBoxButtons.OK,
                          MessageBoxIcon.Warning,
                          this.ParentForm);
        dgv_list.Focus();

        return;
      }

      if (dgv_list.SelectedRows.Count != 1)
      {
        Log.Error("btn_intro_Click. ManualHandpay." + Resource.String("STR_FRM_HANDPAYS_MANUAL_008"));

        frm_message.Show(Resource.String("STR_FRM_HANDPAYS_MANUAL_008"),
                          Resource.String("STR_APP_GEN_MSG_ERROR"),
                          MessageBoxButtons.OK,
                          MessageBoxIcon.Warning,
                          this.ParentForm);
        dgv_list.Focus();

        return;
      }

      //    - Valid amount entered
      _input_amount = 0;

      if (txt_amount.Text != "")
      {
        try
        {
          _input_amount = Decimal.Parse(txt_amount.Text);
        }
        catch
        {
          _input_amount = 0;
        }
      }

      if (_input_amount <= 0)
      {
        Log.Error("btn_intro_Click. Wrong handpay amount (" + _input_amount + ").");

        frm_message.Show(Resource.String("STR_FRM_HANDPAYS_MANUAL_006"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Warning, this.ParentForm);
        txt_amount.Focus();

        return;
      }

      // Get the Handpay Manual settings from the General Parameters table
      _max_allowed_manual_entry = GeneralParam.GetInt32("Cashier.HandPays", "MaxAllowedManualEntry");

      // If limit is configured, entered amount must be less or equal to limit
      if (_max_allowed_manual_entry > 0 && _input_amount > _max_allowed_manual_entry)
      {
        Currency _max_allowed_manual_entry_curr;

        _max_allowed_manual_entry_curr = _max_allowed_manual_entry;
        frm_message.Show(Resource.String("STR_FRM_AMOUNT_INPUT_MSG_MAX_ALLOWED", _max_allowed_manual_entry_curr),
                         Resource.String("STR_APP_GEN_MSG_WARNING"),
                         MessageBoxButtons.OK, MessageBoxIcon.Warning, this.ParentForm);
        txt_amount.Focus();

        return;
      }

      _view_row = dgv_list.SelectedRows[0];
      _row = ((DataRowView)_view_row.DataBoundItem).Row;

      m_handpay.TerminalId = (Int32)_row[GRID_COLUMN_TERM_ID];
      m_handpay.TerminalName = (String)_row[GRID_COLUMN_TERM_NAME];
      m_handpay.TerminalProvider = (String)_row[GRID_COLUMN_TERM_PROVIDER];
      m_handpay.Amount = _input_amount;
      m_handpay.Type = HANDPAY_TYPE.MANUAL;
      m_handpay.DateTime = WGDB.Now;

      // Generate voucher in preview mode
      PreviewVoucher();

      txt_amount.Text = "";
      btn_ok.Enabled = true;
      btn_ok.Focus();
    } // btn_intro_Click

    //------------------------------------------------------------------------------
    // PURPOSE : Generate a preview version of the voucher to be generated.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    private void PreviewVoucher()
    {
      SqlTransaction _sql_trx;
      SqlConnection _sql_conn;
      VoucherCardHandpay _voucher;
      String _voucher_file_name;

      _sql_conn = null;
      _sql_trx = null;

      _voucher_file_name = "";
      web_browser.Navigate(_voucher_file_name);

      try
      {
        // Get Sql Connection 
        _sql_conn = WSI.Common.WGDB.Connection();
        _sql_trx = _sql_conn.BeginTransaction();

        // Handpay voucher 

        VoucherCardHandpay.VoucherCardHandpayInputParams _input_params;

        _input_params = new VoucherCardHandpay.VoucherCardHandpayInputParams();

        _input_params.VoucherAccountInfo = m_handpay.Card.VoucherAccountInfo();
        _input_params.TerminalName = m_handpay.TerminalName;
        _input_params.TerminalProvider = m_handpay.TerminalProvider;
        _input_params.Type = m_handpay.Type;
        _input_params.TypeName = m_handpay.TypeName;
        _input_params.CardBalance = m_handpay.Card.CurrentBalance;
        _input_params.Amount = m_handpay.Amount;
        _input_params.SessionPlayedAmount = -1;
        _input_params.DenominacionStr = m_handpay.DenominationStr;
        _input_params.DenominacionValue = m_handpay.DenominationValue;
        _input_params.Mode = PrintMode.Preview;
        _input_params.ValidationNumber = null;
        _input_params.InitialAmount = 0;

        _input_params.TaxAmount = m_handpay.TaxAmount;
        _input_params.TaxBaseAmount = m_handpay.TaxBaseAmount;
        _input_params.TaxPct = m_handpay.TaxPct;

        _voucher = new VoucherCardHandpay(_input_params, OperationCode.NOT_SET, _sql_trx);

        _voucher_file_name = _voucher.GetFileName();
      }
      catch (Exception _ex)
      {
        _sql_trx.Rollback();
        Log.Exception(_ex);
      }

      finally
      {
        if (_sql_trx != null)
        {
          if (_sql_trx.Connection != null)
          {
            _sql_trx.Rollback();
          }

          _sql_trx.Dispose();
          _sql_trx = null;
        }

        // Close connection
        if (_sql_conn != null)
        {
          _sql_conn.Close();
          _sql_conn = null;
        }
      }

      web_browser.Navigate(_voucher_file_name);
    } // PreviewVoucher

    #endregion Buttons

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Init and Show the form.
    //
    //  PARAMS :
    //      - INPUT :
    //          - ParentForm
    //
    //      - OUTPUT :
    //
    // RETURNS : The result of the opperation
    //
    //   NOTES :
    //
    public Boolean Show(Form ParentForm)
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_handpays_manual", Log.Type.Message);

      txt_amount.Text = "";

      btn_ok.Enabled = false;

      gp_digits_box.Enabled = true;
      gp_voucher_box.Enabled = true;

      form_yes_no.Show();
      this.ShowDialog(form_yes_no);
      form_yes_no.Hide();

      if (m_cancel)
      {
        return false;
      }

      return true;
    } // Show

    //------------------------------------------------------------------------------
    // PURPOSE : Dispose yes_no form.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    new public void Dispose()
    {
      form_yes_no.Dispose();
      form_yes_no = null;

      base.Dispose();
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Initialize control resources (NLS strings, images, etc)
    /// </summary>
    public void InitializeControlResources()
    {
      // NLS Strings
      //    - Labels
      lbl_amount_input_title.Text = Resource.String("STR_FRM_HANDPAYS_MANUAL_001");
      gp_term_game.Text = Resource.String("STR_FRM_HANDPAYS_MANUAL_002");
      gp_digits_box.Text = Resource.String("STR_FRM_HANDPAYS_MANUAL_003");

      //    - Buttons
      btn_ok.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_OK");
      btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");
      btn_intro.Text = Resource.String("STR_FRM_AMOUNT_INPUT_BTN_ENTER");

    } // InitializeControlResources

    /// <summary>
    /// Initialize screen logic
    /// </summary>
    private void InitHandpaysManual()
    {
      // Center screen
      this.Location = new Point(1024 / 2 - this.Width / 2, 0);

      // Load the list of available terminals
      LoadTerminalsList();
    }

    /// <summary>
    /// Check and Add digit to amount
    /// </summary>
    /// <param name="NumberStr"></param>
    private void AddNumber(String NumberStr)
    {
      String aux_str;
      String tentative_number;
      Currency input_amount;
      int dec_symbol_pos;

      // Prevent exceeding maximum number of decimals (2)
      dec_symbol_pos = txt_amount.Text.IndexOf(m_decimal_str);
      if (dec_symbol_pos > -1)
      {
        aux_str = txt_amount.Text.Substring(dec_symbol_pos);
        if (aux_str.Length > 2)
        {
          return;
        }
      }

      // Prevent exceeding maximum amount length
      if (txt_amount.Text.Length >= Cashier.MAX_ALLOWED_AMOUNT_LENGTH)
      {
        return;
      }

      tentative_number = txt_amount.Text + NumberStr;

      try
      {
        input_amount = Decimal.Parse(tentative_number);
        if (input_amount > Cashier.MAX_ALLOWED_AMOUNT)
        {
          return;
        }
      }
      catch
      {
        input_amount = 0;

        return;
      }

      // Remove unneeded leading zeros
      // Unless we are entering a decimal value, there must not be any leading 0
      if (dec_symbol_pos == -1)
      {
        txt_amount.Text = txt_amount.Text.TrimStart('0') + NumberStr;
      }
      else
      {
        // Accept new symbol/digit 
        txt_amount.Text = txt_amount.Text + NumberStr;
      }

      btn_intro.Focus();
    }

    /// <summary>
    /// Load the terminals list
    /// </summary>
    /// <param name="NumberStr"></param>
    private void LoadTerminalsList()
    {
      DataSet _ds;
      String _date_format;
      String _time_separator;
      int _handpay_time_period = 0;
      int _handpay_cancel_time_period = 0;
      StringBuilder _sb;

      _ds = null;

      try
      {
        // Get the Handpays settings from the General Parameters table
        Handpays.GetParameters(out _handpay_time_period, out _handpay_cancel_time_period);

        _sb = new StringBuilder();
        _sb.AppendLine("DECLARE @Now As DateTime ");
        _sb.AppendLine("SET @Now = GETDATE() ");

        _sb.AppendLine("SELECT   TE_TERMINAL_ID ");
        _sb.AppendLine("       , TE_PROVIDER_ID ");
        _sb.AppendLine("       , TE_NAME  ");
        _sb.AppendLine("  FROM   TERMINALS ");
        _sb.AppendLine("       , ( ");
        _sb.AppendLine("           SELECT   PS_TERMINAL_ID ");
        _sb.AppendLine("                  , MAX(PS_STARTED) PS_STARTED  ");
        _sb.AppendLine("             FROM   PLAY_SESSIONS WITH (INDEX (IX_PS_STARTED)) ");
        _sb.AppendLine("            WHERE   PS_STARTED    >= DATEADD(MINUTE, -@pTimePeriod, @Now) ");
        _sb.AppendLine("              AND   PS_ACCOUNT_ID  = @pAccountId ");
        _sb.AppendLine("              AND   (PS_INITIAL_BALANCE <> PS_FINAL_BALANCE OR PS_PLAYED_COUNT > 0)  ");
        _sb.AppendLine("           GROUP BY PS_TERMINAL_ID ");
        _sb.AppendLine("         ) RECENT_TERMINALS ");
        _sb.AppendLine(" WHERE   TE_TERMINAL_TYPE <> @pTerminalTypeSite ");
        _sb.AppendLine("   AND   TE_TERMINAL_TYPE <> @pTerminalTypeCashdeskDraw ");
        _sb.AppendLine("   AND   TE_TERMINAL_TYPE <> @pTerminalTypeBonoPlus ");
        _sb.AppendLine("   AND   TE_TERMINAL_TYPE <> @pTerminalTypePariPlay ");
        _sb.AppendLine("   AND   TE_TERMINAL_ID    = RECENT_TERMINALS.PS_TERMINAL_ID ");
        _sb.AppendLine("ORDER BY PS_STARTED DESC ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = m_handpay.Card.AccountId;
            _cmd.Parameters.Add("@pTimePeriod", SqlDbType.Int).Value = _handpay_time_period;
            _cmd.Parameters.Add("@pTerminalTypeSite", SqlDbType.SmallInt).Value = TerminalTypes.SITE;
            _cmd.Parameters.Add("@pTerminalTypeCashdeskDraw", SqlDbType.SmallInt).Value = TerminalTypes.CASHDESK_DRAW;
            _cmd.Parameters.Add("@pTerminalTypeBonoPlus", SqlDbType.SmallInt).Value = TerminalTypes.GAMEGATEWAY;
            _cmd.Parameters.Add("@pTerminalTypePariPlay", SqlDbType.SmallInt).Value = TerminalTypes.PARIPLAY;

            using (SqlDataAdapter _da = new SqlDataAdapter(_cmd))
            {
              _ds = new DataSet();
              _da.Fill(_ds);
            }
          }
        }

        dgv_list.DataSource = _ds.Tables[0];

        _date_format = System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern;
        _time_separator = System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.TimeSeparator;

        dgv_list.ColumnHeadersHeight = 30;
        dgv_list.RowTemplate.Height = 30;
        dgv_list.RowTemplate.DividerHeight = 0;

        // Columns
        //    - Terminal Id
        //    - Terminal Name
        //    - Provider Name 

        //    - Terminal Id
        dgv_list.Columns[GRID_COLUMN_TERM_ID].Width = 0;
        dgv_list.Columns[GRID_COLUMN_TERM_ID].Visible = false;

        //    - Provider Name 
        dgv_list.Columns[GRID_COLUMN_TERM_PROVIDER].HeaderCell.Value = Resource.String("STR_FRM_HANDPAYS_MANUAL_005");
        dgv_list.Columns[GRID_COLUMN_TERM_PROVIDER].Width = 182;
        dgv_list.Columns[GRID_COLUMN_TERM_PROVIDER].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

        //    - Terminal Name
        dgv_list.Columns[GRID_COLUMN_TERM_NAME].HeaderCell.Value = Resource.String("STR_FRM_HANDPAYS_MANUAL_004");
        dgv_list.Columns[GRID_COLUMN_TERM_NAME].Width = 250;
        dgv_list.Columns[GRID_COLUMN_TERM_NAME].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {
        if (_ds != null)
        {
          _ds.Dispose();
          _ds = null;
        }
      }
    }  // LoadTerminalsList

    private void frm_handpays_manual_Shown(object sender, EventArgs e)
    {
      this.txt_amount.Focus();
    }

    private void frm_handpays_manual_KeyPress(object sender, KeyPressEventArgs e)
    {
      String aux_str;
      char c;

      c = e.KeyChar;

      if (c >= '0' && c <= '9')
      {
        aux_str = "" + c;
        AddNumber(aux_str);

        e.Handled = true;
      }

      if (c == '\r')
      {
        btn_intro_Click(null, null);

        e.Handled = true;
      }

      if (c == '\b')
      {
        btn_back_Click(null, null);

        e.Handled = true;
      }

      if (c == '.')
      {
        btn_num_dot_Click(null, null);

        e.Handled = true;
      }

      e.Handled = false;
    }

    private void splitContainer1_KeyPress(object sender, KeyPressEventArgs e)
    {
      frm_handpays_manual_KeyPress(sender, e);
    }

    private void frm_handpays_manual_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
    {
      char c;

      c = Convert.ToChar(e.KeyCode);
      if (c == '\r')
      {
        e.IsInputKey = true;
      }
    }

  }

    #endregion
}