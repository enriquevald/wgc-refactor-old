//------------------------------------------------------------------------------
// Copyright � 2014 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : uc_datetime.cs
// 
//   DESCRIPTION : User Control to Date Management
// 
// REVISION HISTORY :
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 09-JUL-2014 LEM    First Release.
// 05-NOV-2015 JMV    Product Backlog Item 5788:Dise�o cajero: Aplicar en uc_datetime.cs
// 07-JAN-2016 SJA    only focus next on change of value if sender has focus to
//                    avoid stealing focus on change of backing property
// 02-JUN-2016 DHA    Fixed Bug 14009: Tab between textboxes
//------------------------------------------------------------------------------


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Globalization;
using WSI.Common;

namespace WSI.Cashier
{
  public partial class uc_datetime : UserControl
  {
    #region Members

    public event EventHandler OnDateChanged;

    private Int32 m_datetext_width;
    private Int32 m_format_width;

    #endregion

    #region Constants

    private const String ESP_LNG = "es";

    #endregion

    #region Constructor



    protected override void OnValidating(CancelEventArgs e)
    {
      if ((txt_year.Text.Length > 0 || txt_month.Text.Length > 0 ||
          txt_day.Text.Length > 0) && GetDate() == DateTime.MinValue)
      {
        this.Focus();
        e.Cancel = true;
        this.Invalid = true;
        return;
      }
      this.Invalid = false;
      base.OnValidating(e);
    }

    public bool Invalid { get; set; }

    public uc_datetime()
    {
      InitializeComponent();
      Init();
      this.CausesValidation = true;

    }

    #endregion

    #region Properties

    public Font DateTextFont
    {
      get
      {
        return lbl_date_text.Font;
      }
      set
      {
        if (value != null)
        {
          lbl_date_text.Font = value;
        }
      }
    }

    public Font FormatFont
    {
      get
      {
        return lbl_format.Font;
      }
      set
      {
        if (value != null)
        {
          lbl_format.Font = value;
        }
      }
    }

    public Font ValuesFont
    {
      get
      {
        return txt_day.Font;
      }
      set
      {
        if (value != null)
        {
          txt_day.Font = value;
          txt_month.Font = value;
          txt_year.Font = value;
          lbl_sep1.Font = value;
          lbl_sep2.Font = value;
        }
      }
    }

    public String DateText
    {
      get
      {
        return lbl_date_text.Text;
      }
      set
      {
        lbl_date_text.Text = (value != null ? value : "");
      }
    }

    public Int32 DateTextWidth
    {
      get
      {
        return lbl_date_text.Width;
      }
      set
      {
        if (value >= 0)
        {
          lbl_date_text.Width = value;
        }
      }
    }

    public Int32 FormatWidth
    {
      get
      {
        return lbl_format.Width;
      }
      set
      {
        if (value >= 0)
        {
          lbl_format.Width = value;
        }
      }
    }

    public DateTime DateValue
    {
      get
      {
        return GetDate();
      }
      set
      {
        SetDate(value);
      }
    }

    public Boolean IsEmpty
    {
      get
      {
        return txt_day.Text == "" && txt_month.Text == "" && txt_year.Text == "";
      }
    }

    #endregion

    #region Publics

    public void Init()
    {
      String _format;

      _format = Format.DateTimeNormalizedString();

      if (CashierBusinessLogic.CashierLanguage() == ESP_LNG)
      {
        lbl_format.Text = "(" + _format.Replace('y', 'a').ToUpperInvariant() + ")";
      }
      else
      {
        lbl_format.Text = "(" + _format.ToUpperInvariant() + ")";
      }

      m_datetext_width = lbl_date_text.Width;
      m_format_width = lbl_format.Width;

      ResetDateValue();
      SetFormatControls(_format);
      SaveCurrentWidthFactor();


      txt_day.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      txt_month.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      txt_year.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
    }

    public void ResetDateValue()
    {
      this.DateValue = DateTime.MinValue;
    }

    public void DateError()
    {
      lbl_date_text.ForeColor = Color.Red;
    }

    public void NoError()
    {
      lbl_date_text.ResetForeColor();
    }

    public new void Focus()
    {
      base.Focus();
      this.ActiveControl = pnl_main.Controls[1];
    }

    public Boolean ValidateFormat()
    {
      DateTime _date;

      _date = GetDate();

      return (_date != DateTime.MinValue && WSI.Common.ValidateFormat.IsValidDate(_date));
    }

    #endregion

    #region Privates

    private void ChangeDateTextWidth()
    {
      this.Resize -= new EventHandler(uc_datetime_Resize);

      this.Width += lbl_date_text.Width - m_datetext_width;

      m_datetext_width = lbl_date_text.Width;

      this.Resize += new EventHandler(uc_datetime_Resize);
    }

    private void ChangeFormatWidth()
    {
      this.Resize -= new EventHandler(uc_datetime_Resize);

      this.Width += lbl_format.Width - m_format_width;

      m_format_width = lbl_format.Width;

      this.Resize += new EventHandler(uc_datetime_Resize);
    }

    private void SaveCurrentWidthFactor()
    {
      Double _base;

      _base = pnl_main.Width;

      foreach (Control _ctrl in pnl_main.Controls)
      {
        _ctrl.Tag = _ctrl.Width / _base;
      }
    }

    private void SetDate(DateTime Date)
    {
      txt_day.Text = "";
      txt_month.Text = "";
      txt_year.Text = "";

      if (Date != DateTime.MinValue)
      {
        txt_day.Text = Date.Day.ToString("00");
        txt_month.Text = Date.Month.ToString("00");
        txt_year.Text = Date.Year.ToString("0000");
      }
    }

    private void SetFormatControls(String Format)
    {
      Int32 _iday;
      Int32 _imonth;
      Int32 _iyear;

      switch (Format)
      {
        case "dd/MM/yyyy":
          _iday = 1;
          _imonth = 3;
          _iyear = 5;
          break;
        case "MM/dd/yyyy":
          _iday = 3;
          _imonth = 1;
          _iyear = 5;
          break;
        case "yyyy/MM/dd":
          _iday = 5;
          _imonth = 3;
          _iyear = 1;
          break;
        case "yyyy/dd/MM":
          _iday = 3;
          _imonth = 5;
          _iyear = 1;
          break;
        default:
          _iday = 1;
          _imonth = 3;
          _iyear = 5;
          break;
      }

      pnl_main.Controls.SetChildIndex(lbl_date_text, 0);
      pnl_main.Controls.SetChildIndex(txt_day, _iday);
      pnl_main.Controls.SetChildIndex(lbl_sep1, 2);
      pnl_main.Controls.SetChildIndex(txt_month, _imonth);
      pnl_main.Controls.SetChildIndex(lbl_sep2, 4);
      pnl_main.Controls.SetChildIndex(txt_year, _iyear);
      pnl_main.Controls.SetChildIndex(lbl_format, 6);

      txt_day.TabIndex = _iday;
      txt_month.TabIndex = _imonth;
      txt_year.TabIndex = _iyear;
    }

    private DateTime GetDate()
    {
      DateTime _date;
      //SJA 17=02=2016 Stop throwing exception when in paranoia exception mode
      try
        {
        if (txt_year.IntValue == default(int) || txt_month.IntValue == default(int) || txt_day.IntValue == default(int))
        {
            _date = DateTime.MinValue;
        }
        else
        {
          _date = new DateTime(txt_year.IntValue, txt_month.IntValue, txt_day.IntValue);
        }
      }
      catch (Exception)
      {
        _date = DateTime.MinValue;
      }

      return _date;
    }

    #endregion

    #region Events
    
    private void txt_date_TextChanged(object sender, EventArgs e)
    {
      WSI.Cashier.Controls.uc_round_textbox _tag;
      Int32 _index;
      Control _next;

      _tag = (WSI.Cashier.Controls.uc_round_textbox)sender;

      //SJA - stop focusing next on change of value, onl
      if (((NumericTextBox)sender).TextBox_Focused)
      {
        if (_tag.Text.Length >= _tag.MaxLength)
        {
          _index = pnl_main.Controls.GetChildIndex((NumericTextBox)sender, false);

          if (_index < 5)
          {

            _next = pnl_main.Controls[_index + 2];

            _next.Focus();

          }
          else
          {
            this.FindForm().SelectNextControl(this, true, true, true, true);
          }
        }
      }
      if (OnDateChanged != null)
      {
        OnDateChanged(this, e);
      }
    }

    private void lbl_date_text_SizeChanged(object sender, EventArgs e)
    {
      ChangeDateTextWidth();
      SaveCurrentWidthFactor();
    }

    private void lbl_format_SizeChanged(object sender, EventArgs e)
    {
      ChangeFormatWidth();
      SaveCurrentWidthFactor();
    }

    private void uc_datetime_Resize(object sender, EventArgs e)
    {
      Int32 _width;
      Int32 _height;

      _width = pnl_main.Width;
      _height = pnl_main.Height;

      lbl_date_text.SizeChanged -= new EventHandler(lbl_date_text_SizeChanged);
      lbl_format.SizeChanged -= new EventHandler(lbl_format_SizeChanged);

      foreach (Control _ctrl in pnl_main.Controls)
      {
        _ctrl.Height = _height;
        _ctrl.Width = (Int32)(_width * (Double)_ctrl.Tag);
      }

      lbl_date_text.SizeChanged += new EventHandler(lbl_date_text_SizeChanged);
      lbl_format.SizeChanged += new EventHandler(lbl_format_SizeChanged);
    }

    #endregion
  }
}
