//------------------------------------------------------------------------------
// Copyright � 20015 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_concept_comment.cs
// 
//   DESCRIPTION: 
//
//        AUTHOR: FAV
// 
// CREATION DATE: 04-AUG-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 04-AUG-2015 FAV    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Windows.Forms;
using WSI.Common;
using System.Threading;
using System.Drawing;
using WSI.Cashier.Controls;

namespace WSI.Cashier
{
  public partial class frm_comment : frm_base
  {
    #region Attributes

    private Form m_parent;
    
    #endregion

    #region Constructor

    public frm_comment()
    {
      InitializeComponent();

      InitializeControlResources();
    }

    #endregion

    #region Private Methods

    private void InitializeControlResources()
    {
      //   - Labels
        //lbl_comment_title.Text = Resource.String("STR_FRM_COMMENT_DISPUTE");
      this.FormTitle = Resource.String("STR_FRM_COMMENT_DISPUTE");

      //   - Buttons
      btn_ok.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_OK");
    }

    #endregion

    #region Public Methods

    public void Show(Form Parent, out string Comment)
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_comment", Log.Type.Message);
      Comment = string.Empty;

      m_parent = Parent;
      txt_block_comment.Select();
      this.ShowDialog(Parent);

      Comment = txt_block_comment.Text.Trim();
    } 

    new public void Dispose()
    {

      base.Dispose();
    } 

    #endregion

    #region Events

    private void btn_ok_Click(object sender, EventArgs e)
    {
      Misc.WriteLog("[FORM CLOSE] frm_comment (ok)", Log.Type.Message);
      this.Close();
    }

    #endregion
    
  }
}
