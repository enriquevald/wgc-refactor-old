//-------------------------------------------------------------------
// Copyright � 2014 Win Systems Ltd.
//-------------------------------------------------------------------
//
// MODULE NAME:   uc_input_amount_resizable.cs
// DESCRIPTION:   
// AUTHOR:        Jos� Mart�nez L�pez
// CREATION DATE: 18-JUN-2014
//
// REVISION HISTORY:
//
// Date         Author Description
// -----------  ------ -----------------------------------------------
// 18-JUN-2014  JML    Initial version.
// 25-JUL-2014  DHA    Fixed Bug #WIG-1119: set SetLastAction for player tracking gaming tables
// 12-AUG-2014  DCS    Fixed Bug with Focus and keyboard
// 10-DEC-2014  DRV    Decimal currency formatting: DUT 201411 - Tito Chile - Currency formatting without decimals
// 20-JUL-2016  FOS    Fixed Bug 15758: Close form Image 
// -------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using WSI.Common;

namespace WSI.Cashier
{
  public partial class uc_input_amount_resizable : UserControl
  {
    #region Constants

    private const Int32 HIDE_HEIGTH = 0; // 30;

    #endregion

    #region Events definitions

    public delegate void AmountSelectedEventHandler(Decimal Amount);
    public event AmountSelectedEventHandler OnAmountSelected;

    public delegate void ModifyValueEventHandler(String Amount);
    public event ModifyValueEventHandler OnModifyValue;

    #endregion

    #region Class Atributes

    private Int32 m_heigth;

    private static String m_decimal_str = System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencyDecimalSeparator;

    private Currency btn_money_1_amount;
    private Currency btn_money_2_amount;
    private Currency btn_money_3_amount;
    private Currency btn_money_4_amount;
    private string btn_money_1_value;
    private string btn_money_2_value;
    private string btn_money_3_value;
    private string btn_money_4_value;

    Int32 m_height_head;
    Int32 m_height_amount;
    Int32 m_height_buttons;
    Int32 m_height_keys;

    private DisplayWindowMode m_entry_mode;

    private Int32 m_display_height;
    private Int32 m_display_width;

    private Boolean m_recalculate_height;

    private InputMode m_input_mode;

    #endregion

    #region Enums

    public enum DisplayWindowMode
    {
      NONE = 0,
      COMPLETE = 1,
      COMPLETE_WITHOUT_DOT = 2,
      ONLY_NUM_KEYS = 3,
      ONLY_NUM_KEYS_WITHOUT_DOT = 4,
      WITHOUT_HEAD = 5,
      WITHOUT_HEAD_WITHOUT_DOT = 6,
      WITHOUT_AMOUNT_BUTTONS = 7,
      WITHOUT_AMOUNT_BUTTONS_WITHOUT_DOT = 8
    };

    public enum InputMode
    {
      AMOUNT = 0,
      TICKET = 1
    }
    #endregion

    #region Constructor

    public uc_input_amount_resizable()
    {
      InitControl();
    }

    public uc_input_amount_resizable(DisplayWindowMode EntryMode = DisplayWindowMode.COMPLETE,
                                    InputMode NumberMode = InputMode.AMOUNT,
                                    Int32 WindowWidth = 10,
                                    Int32 WindowHeight = 10)
    {
      InitControl();

      m_input_mode = NumberMode;

      if (WindowWidth > 0)
      {
        m_display_width = WindowWidth;
        this.Width = WindowWidth;
      }

      if (WindowHeight > 0)
      {
        m_display_height = WindowHeight;
        this.Height = WindowHeight;
      }

      m_heigth = this.Height;
      m_entry_mode = EntryMode;

      m_recalculate_height = false;

      SetDisplayMode();
    }

    private void InitControl()
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[UC LOAD] uc_input_amount_resizable", Log.Type.Message);

      InitializeComponent();

      InitControls();

      InitializeControlResources();

      m_heigth = this.Height;

    }

    //------------------------------------------------------------------------------
    // PURPOSE : Add event handler to scan and configure buttons
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    private void InitControls()
    {
      // These events are inserted on parent control (uc_gt_player_tracking).
      EventLastAction.AddEventLastAction(this.Controls);

      this.btn_num_0.Click += new System.EventHandler(this.btn_num_Click);
      this.btn_num_1.Click += new System.EventHandler(this.btn_num_Click);
      this.btn_num_2.Click += new System.EventHandler(this.btn_num_Click);
      this.btn_num_3.Click += new System.EventHandler(this.btn_num_Click);
      this.btn_num_4.Click += new System.EventHandler(this.btn_num_Click);
      this.btn_num_5.Click += new System.EventHandler(this.btn_num_Click);
      this.btn_num_6.Click += new System.EventHandler(this.btn_num_Click);
      this.btn_num_7.Click += new System.EventHandler(this.btn_num_Click);
      this.btn_num_8.Click += new System.EventHandler(this.btn_num_Click);
      this.btn_num_9.Click += new System.EventHandler(this.btn_num_Click);
      this.btn_money_1.Click += new System.EventHandler(this.btn_custom_Click);
      this.btn_money_2.Click += new System.EventHandler(this.btn_custom_Click);
      this.btn_money_3.Click += new System.EventHandler(this.btn_custom_Click);
      this.btn_money_4.Click += new System.EventHandler(this.btn_custom_Click);
      this.lbl_head.Click += new System.EventHandler(this.lbl_head_Click);
      this.btn_num_dot.Click += new System.EventHandler(this.btn_num_dot_Click);
      this.btn_intro.Click += new System.EventHandler(this.btn_intro_Click);
      this.btn_back.Click += new System.EventHandler(this.btn_back_Click);

      m_height_head = (Int32)this.tableLayoutPanel3.RowStyles[0].Height;
      m_height_amount = (Int32)this.tableLayoutPanel3.RowStyles[1].Height;
      m_height_buttons = (Int32)this.tableLayoutPanel3.RowStyles[2].Height;
      m_height_keys = (Int32)this.tableLayoutPanel3.RowStyles[3].Height;

    }

    #endregion

    #region Buttons


    public Boolean AcceptDecimals
    {
      get { return btn_num_dot.Enabled; }
      set { btn_num_dot.Enabled = value; }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Initialize Amount Buttons money.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void InitMoneyButtons()
    {
      String _currency_format = ""; //"C0";

      btn_money_1_amount = 0;
      btn_money_1_value = "";
      btn_money_2_amount = 0;
      btn_money_2_value = "";
      btn_money_3_amount = 0;
      btn_money_3_value = "";
      btn_money_4_amount = 0;
      btn_money_4_value = "";

      LoadDefaultAmountButtonsSQL();

      btn_money_1.Text = "+" + btn_money_1_amount.ToString(_currency_format);
      if ((Decimal)btn_money_1_amount == 0)
      {
        this.btn_money_1.Enabled = false;
        this.btn_money_1.Visible = false;
      }
      if ((Decimal)btn_money_1_amount >= 1000)
      {
        this.btn_money_1.Font = new System.Drawing.Font("Montserrat", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel); // new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      }

      btn_money_2.Text = "+" + btn_money_2_amount.ToString(_currency_format);
      if ((Decimal)btn_money_2_amount == 0)
      {
        this.btn_money_2.Enabled = false;
        this.btn_money_2.Visible = false;
      }
      if ((Decimal)btn_money_2_amount >= 1000)
      {
        this.btn_money_2.Font = new System.Drawing.Font("Montserrat", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);//new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      }

      btn_money_3.Text = "+" + btn_money_3_amount.ToString(_currency_format);
      if ((Decimal)btn_money_3_amount == 0)
      {
        this.btn_money_3.Enabled = false;
        this.btn_money_3.Visible = false;
      }
      if ((Decimal)btn_money_3_amount >= 1000)
      {
        this.btn_money_3.Font = new System.Drawing.Font("Montserrat", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);// new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      }

      btn_money_4.Text = "+" + btn_money_4_amount.ToString(_currency_format);
      if ((Decimal)btn_money_4_amount == 0)
      {
        this.btn_money_4.Enabled = false;
        this.btn_money_4.Visible = false;
      }
      if ((Decimal)btn_money_4_amount >= 1000)
      {
        this.btn_money_4.Font = new System.Drawing.Font("Montserrat", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);// new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      }
      if (Format.GetFormattedDecimals(CurrencyExchange.GetNationalCurrency()) == 0)
      {
        this.btn_num_dot.Enabled = false;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Load Default Amount Buttons money.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void LoadDefaultAmountButtonsSQL()
    {
      Decimal convert_money;

      try
      {
        btn_money_1_value = GeneralParam.GetString("Cashier.AddAmount", "GamblingTable.CustomButton1", GeneralParam.GetString("Cashier.AddAmount", "CustomButton1", "50"));
        convert_money = Decimal.Parse(btn_money_1_value);
        btn_money_1_amount = (Currency)convert_money;
      }
      catch (Exception ex)
      {
        Log.Error("Wrong value in parameter: Cashier.AddAmount - GamblingTable.CustomButton1");
        Log.Exception(ex);
      }

      try
      {
        btn_money_2_value = GeneralParam.GetString("Cashier.AddAmount", "GamblingTable.CustomButton2", GeneralParam.GetString("Cashier.AddAmount", "CustomButton2", "100"));
        convert_money = Decimal.Parse(btn_money_2_value);
        btn_money_2_amount = (Currency)convert_money;
      }
      catch (Exception ex)
      {
        Log.Error("Wrong value in parameter: Cashier.AddAmount - GamblingTable.CustomButton2");
        Log.Exception(ex);
      }

      try
      {
        btn_money_3_value = GeneralParam.GetString("Cashier.AddAmount", "GamblingTable.CustomButton3", GeneralParam.GetString("Cashier.AddAmount", "CustomButton3", "200"));
        convert_money = Decimal.Parse(btn_money_3_value);
        btn_money_3_amount = (Currency)convert_money;
      }
      catch (Exception ex)
      {
        Log.Error("Wrong value in parameter: Cashier.AddAmount - GamblingTable.CustomButton3");
        Log.Exception(ex);
      }

      try
      {
        btn_money_4_value = GeneralParam.GetString("Cashier.AddAmount", "GamblingTable.CustomButton4", GeneralParam.GetString("Cashier.AddAmount", "CustomButton4", "300"));
        convert_money = Decimal.Parse(btn_money_4_value);
        btn_money_4_amount = (Currency)convert_money;
      }
      catch (Exception ex)
      {
        Log.Error("Wrong value in parameter: Cashier.AddAmount - GamblingTable.CustomButton4");
        Log.Exception(ex);
      }

    } // LoadDefaultAmountButtonsSQL


    #endregion

    #region Private Methods

    private void InitializeControlResources()
    {
      //gp_digits_box.Text = Resource.String("STR_FRM_AMOUNT_INPUT_DIGITS_GROUP_BOX");

      // - Buttons
      //btn_intro.Text = Resource.String("STR_FRM_AMOUNT_INPUT_BTN_ENTER");

      txt_amount.Text = "";

      lbl_head.Text = Resource.String("STR_INPUT_AMOUNT_HEAD");

      InitMoneyButtons();


    }  // InitializeControlResources

    private void SumNumber(String NumberStr)
    {
      Decimal _number;
      Decimal _amount;
      Decimal _input_amount;

      if (!Decimal.TryParse(NumberStr, out _number))
      {
        _number = 0;
      }

      if (String.IsNullOrEmpty(txt_amount.Text) || txt_amount.Text == "0")
      {
        txt_amount.Text = NumberStr;
      }
      else
      {
        if (!Decimal.TryParse(txt_amount.Text, out _amount)) _amount = 0;
        Decimal _temp = _number + _amount;

        // JBP 18-FEB-2014: Checks max amount
        _input_amount = Decimal.Parse(_temp.ToString());
        if (_input_amount > Cashier.MAX_ALLOWED_AMOUNT)
        {
          return;
        }

        txt_amount.Text = _temp.ToString();
      }
      txt_amount.SelectionStart = txt_amount.Text.Length;

    } // SumNumber


    /// <summary>
    /// Check and Add digit to amount
    /// </summary>
    /// <param name="NumberStr"></param>
    private void AddNumber(String NumberStr)
    {
      String aux_str;
      String tentative_number;
      Currency input_amount;
      int dec_symbol_pos;

      dec_symbol_pos = txt_amount.Text.IndexOf(m_decimal_str);

      if (txt_amount.SelectionLength == txt_amount.TextLength)
      {
        txt_amount.Text = "";
      }
      else
      {

        // Prevent exceeding maximum number of decimals (2)
        if (dec_symbol_pos > -1)
        {
          aux_str = txt_amount.Text.Substring(dec_symbol_pos);
          if (aux_str.Length > 2)
          {
            return;
          }
        }

      }

      // Prevent exceeding maximum amount length
      if (m_input_mode == InputMode.AMOUNT && txt_amount.Text.Length >= Cashier.MAX_ALLOWED_AMOUNT_LENGTH
       || m_input_mode == InputMode.TICKET && txt_amount.Text.Length >= Cashier.MAX_ALLOWED_TICKET_LENGTH)
      {
        return;
      }

      tentative_number = txt_amount.Text + NumberStr;

      try
      {
        if (m_input_mode == InputMode.AMOUNT)
        {
          input_amount = Decimal.Parse(tentative_number);
          if (input_amount > Cashier.MAX_ALLOWED_AMOUNT)
          {
            return;
          }
        }
      }
      catch
      {
        input_amount = 0;

        return;
      }

      // Remove unneeded leading zeros
      // Unless we are entering a decimal value, there must not be any leading 0
      if (dec_symbol_pos == -1)
      {
        txt_amount.Text = txt_amount.Text.TrimStart('0') + NumberStr;
      }
      else
      {
        // Accept new symbol/digit 
        txt_amount.Text = txt_amount.Text + NumberStr;
      }

      txt_amount.Focus();
      txt_amount.SelectionStart = txt_amount.TextLength;
    }


    private void SetDisplayMode()
    {
      switch (m_entry_mode)
      {
        case DisplayWindowMode.NONE:
          break;
        case DisplayWindowMode.COMPLETE:
          break;
        case DisplayWindowMode.COMPLETE_WITHOUT_DOT:
          this.btn_num_dot.Enabled = false;

          break;
        case DisplayWindowMode.ONLY_NUM_KEYS:
          this.txt_amount.TextChanged += new System.EventHandler(this.txt_amount_TextChanged);
          Hide_Head();
          Hide_Amount();
          Hide_AmountButtons();
          if (m_display_height == 0)
          {
            m_recalculate_height = true;
            RecalculateHeight();
          }

          break;
        case DisplayWindowMode.ONLY_NUM_KEYS_WITHOUT_DOT:
          this.txt_amount.TextChanged += new System.EventHandler(this.txt_amount_TextChanged);
          Hide_Head();
          Hide_Amount();
          Hide_AmountButtons();
          this.btn_num_dot.Enabled = false;
          if (m_display_height == 0)
          {
            m_recalculate_height = true;
            RecalculateHeight();
          }

          break;
        case DisplayWindowMode.WITHOUT_HEAD:
          this.txt_amount.TextChanged += new System.EventHandler(this.txt_amount_TextChanged);
          Hide_Head();
          if (m_display_height == 0)
          {
            m_recalculate_height = true;
            RecalculateHeight();
          }

          break;
        case DisplayWindowMode.WITHOUT_HEAD_WITHOUT_DOT:
          this.txt_amount.TextChanged += new System.EventHandler(this.txt_amount_TextChanged);
          Hide_Head();
           this.btn_num_dot.Enabled = false;
         if (m_display_height == 0)
          {
            m_recalculate_height = true;
            RecalculateHeight();
          }

          break;
        case DisplayWindowMode.WITHOUT_AMOUNT_BUTTONS:
          this.txt_amount.TextChanged += new System.EventHandler(this.txt_amount_TextChanged);
          Hide_Head();
          Hide_Amount();
          Hide_AmountButtons();
          if (m_display_height == 0)
          {
            m_recalculate_height = true;
            RecalculateHeight();
          }

          break;
        case DisplayWindowMode.WITHOUT_AMOUNT_BUTTONS_WITHOUT_DOT:
          this.txt_amount.TextChanged += new System.EventHandler(this.txt_amount_TextChanged);
          Hide_Head();
          Hide_Amount();
          Hide_AmountButtons();
          this.btn_num_dot.Enabled = false;
          if (m_display_height == 0)
          {
            m_recalculate_height = true;
            RecalculateHeight();
          }

          break;
      }

      m_recalculate_height = false;

    }
    public void RecalculateHeight()
    {
      TableLayoutRowStyleCollection styles = this.tableLayoutPanel3.RowStyles;
      Int32 _new_height;
      _new_height = 0;

      if (!m_recalculate_height)
      {
        return;
      }

      foreach (RowStyle style in styles)
      {
        _new_height += (Int32)style.Height;
      }
      this.Height = _new_height;

    }

    #endregion

    #region Public Methods

    public void Hide_KeyBoard()
    {
      this.Height = HIDE_HEIGTH;
      this.Visible = false;
      pb_close.Image = WSI.Cashier.Images.Get(Images.CashierImage.Input_show);
    }

    public void Show_KeyBoard()
    {
      this.Height = m_heigth;
      this.Visible = true;
      pb_close.Image = WSI.Cashier.Images.Get(Images.CashierImage.Calc_Header_Close);
      txt_amount.Focus();
      txt_amount.SelectAll();
    }

    public void Hide_Head()
    {
      this.tableLayoutPanel3.RowStyles[0].Height = 0;
      this.lbl_head.Visible = false;
      this.pb_close.Visible = false;
      this.tableLayoutPanel4.Visible = false;
      RecalculateHeight();
    }

    public void Show_Head()
    {
      this.tableLayoutPanel3.RowStyles[0].Height = m_height_head;
      this.lbl_head.Visible = true;
      this.pb_close.Visible = true;
      this.tableLayoutPanel4.Visible = true;
      RecalculateHeight();
    }

    public void Hide_Amount()
    {
      this.tableLayoutPanel3.RowStyles[1].Height = 0;
      this.txt_amount.Visible = false;
      RecalculateHeight();
    }

    public void Show_Amount()
    {
      this.tableLayoutPanel3.RowStyles[1].Height = m_height_amount;
      this.txt_amount.Visible = true;
      RecalculateHeight();
    }

    public void Hide_AmountButtons()
    {
      this.tableLayoutPanel3.RowStyles[2].Height = 0;
      this.btn_money_1.Visible = false;
      this.btn_money_2.Visible = false;
      this.btn_money_3.Visible = false;
      this.btn_money_4.Visible = false;
      RecalculateHeight();
    }

    public void Show_AmountButtons()
    {
      this.tableLayoutPanel3.RowStyles[2].Height = m_height_buttons;
      this.btn_money_1.Visible = true;
      this.btn_money_2.Visible = true;
      this.btn_money_3.Visible = true;
      this.btn_money_4.Visible = true;
      RecalculateHeight();
    }

    public void Hide_NumericKeys()
    {
      this.tableLayoutPanel3.RowStyles[0].Height = 0;
      RecalculateHeight();
    }

    public void Show_NumericKeys()
    {
      this.tableLayoutPanel3.RowStyles[0].Height = m_height_keys;
      RecalculateHeight();
    }


    #endregion

    #region Events

    private void btn_num_Click(object sender, EventArgs e)
    {
      AddNumber(((System.Windows.Forms.ButtonBase)(sender)).Text);
    } // btn_num_Click

    private void txt_amount_TextChanged(object sender, EventArgs e)
    {
      if (OnModifyValue != null && txt_amount.Text.Length >= 0)
      {
        OnModifyValue(txt_amount.Text);
      }
    }

    private void btn_custom_Click(object sender, EventArgs e)
    {
      Decimal _money_value;
      String _button_text;

      try
      {
        if (m_input_mode == InputMode.TICKET)
        {
          return;
        }

        _button_text = ((ButtonBase)sender).Text;
        _button_text = _button_text.Remove(0, 1); // Remove "+"
        _money_value = Decimal.Parse(_button_text);
      }
      catch
      {
        _money_value = 0;
        _button_text = String.Empty;

      }

      if (_money_value <= Cashier.MAX_ALLOWED_AMOUNT)
      {
        SumNumber(_button_text);
      }
    } // btn_custom_Click

    private void btn_intro_Click(object sender, EventArgs e)
    {
      Decimal _input_amount;

      Decimal.TryParse(txt_amount.Text, out _input_amount);

      if (OnAmountSelected != null)
      {
        OnAmountSelected(_input_amount);
      }
    } // btn_intro_Click

    //------------------------------------------------------------------------------
    // PURPOSE : Deletes the last character.
    //
    //  PARAMS :
    //      - INPUT :
    //          - object:     sender
    //          - EventArgs:  e
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //  
    private void btn_back_Click(object sender, EventArgs e)
    {
      if (txt_amount.Text.Length > 0)
      {
        txt_amount.Text = txt_amount.Text.Substring(0, txt_amount.Text.Length - 1);
        txt_amount.SelectionStart = txt_amount.Text.Length + 1;
      }
      txt_amount.Focus();
    } // btn_back_Click

    private void btn_num_dot_Click(object sender, EventArgs e)
    {
      if (txt_amount.Text.IndexOf(m_decimal_str) == -1)
      {
        txt_amount.Text = txt_amount.Text + m_decimal_str;
        txt_amount.SelectionStart = txt_amount.Text.Length + 1;
      }
      txt_amount.Focus();
    }  // btn_num_dot_Click

    //------------------------------------------------------------------------------
    // PURPOSE : Handle the KeyPress.
    //
    //  PARAMS :
    //      - INPUT :
    //          - object:             sender
    //          - KeyPressEventArgs:  e
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void uc_input_amount_resizable_KeyPress(object sender, KeyPressEventArgs e)
    {
      String aux_str;
      char c;

      c = e.KeyChar;
      e.Handled = true;

      if (c >= '0' && c <= '9')
      {
        aux_str = "" + c;
        AddNumber(aux_str);
      }

      if (c == '\r')
      {
        btn_intro_Click(null, null);
      }

      if (c == '\b')
      {
        btn_back_Click(null, null);
      }

      if (c == '.')
      {
        btn_num_dot_Click(null, null);
      }

    }  // uc_input_amount_resizable_KeyPress

    private void uc_input_amount_resizable_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
    {
      char c;

      c = Convert.ToChar(e.KeyCode);
      if (c == '\r')
      {
        e.IsInputKey = true;
      }
    }  // uc_input_amount_resizable_PreviewKeyDown

    /// <summary>
    /// Show/hide the numeric keyboard
    /// </summary>
    /// <param name="NumberStr"></param>
    private void lbl_head_Click(object sender, EventArgs e)
    {
      if (this.Height == HIDE_HEIGTH)
      {
        Show_KeyBoard();
      }
      else
      {
        Hide_KeyBoard();
      }
    }  // lbl_head_Click

    /// <summary>
    /// Show/hide the numeric keyboard
    /// </summary>
    /// <param name="NumberStr"></param>
    private void pb_close_Click(object sender, EventArgs e)
    {
      Hide_KeyBoard();
    }  // pb_close_Click

    /// <summary>
    /// For move control with teh mouse
    /// </summary>
    private void uc_input_amount_resizable_MouseMove(object sender, MouseEventArgs e)
    {

      if (e.Button == MouseButtons.Left)
      {
        uc_input_amount_resizable us = sender as uc_input_amount_resizable;
        if (us != null)
        {
          us.Left += e.X;
          us.Top += e.Y;
        }
      }

    } // uc_input_amount_resizable_MouseMove

    #endregion

  }
}
