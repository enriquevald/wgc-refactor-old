﻿//------------------------------------------------------------------------------
// Copyright © 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_currency_exchange.cs
// 
//   DESCRIPTION: Implements the following classes:
//                1. frm_currency_exchange
//
//        AUTHOR: Jordi Masachs
// 
// CREATION DATE: 04-MAY-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 04-MAY-2016 JMV     First release.
// 05-JUL-2016 ETP     Product Backlog Item 15195:Cajero: permitir Multidivisa sin cuenta asociada.
// 05-JUL-2016 ETP     PBI 15060: Cambio de multidivisa: Identificar al cliente en cualquier operación con divisas
// 06-JUL-2016 DHA     Product Backlog Item 15064: multicurrency chips sale/purchase
// 07-SEP-2016 DHA     Product Backlog Item 15912: resize for differents windows size
// 03-NOV-2017 RGR     Bug 30558:WIGOS-6158 Exception when the user proceed with a "Devolucion de divisa"
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WSI.Cashier.Controls;
using WSI.Common;

namespace WSI.Cashier
{
  public partial class frm_currency_refund : frm_base
  {

    #region Attributes

    private CardData m_card_data;
    private CurrencyExchange m_selected_currency;
    private CurrencyExchangeResult m_exchange_result;
    private frm_generic_amount_input m_amount_input;
    private frm_generic_amount_input m_amount_input_foreign;
    private Boolean Modify = false;
    private Boolean m_first_time = true;
    private Decimal m_max_foreign_amount;
    private bool m_blocked = false;
    private Decimal m_max_national_amount;
    private Boolean m_max_national_reached;
    private Boolean m_foreign_forced;
    private List<CASHIER_MOVEMENT> m_movements_list;
    private List<CurrencyExchange> m_currencies = null;
    private CurrencyExchange m_national_currency = null;

    #endregion

    #region Public

    public CurrencyExchangeResult GetExchangeResult
    {
      get
      {
        return m_exchange_result;
      }
    }

    public frm_currency_refund(CardData card_data)
    {
      InitializeComponent();
      m_card_data = card_data;
      m_max_foreign_amount = 0;
      m_max_national_reached = false;
      m_foreign_forced = false;
      InitializeControlResources();
      if (m_card_data.IsVirtualCard)
      {
        lbl_max_refund.Visible = false;
        lbl_max_refund_amount.Visible = false;
        lbl_exchange.Visible = false;
        lbl_exchange_amount.Visible = false;
      }
      else
      {
        lbl_exchange_rate.Visible = false;
        lbl_exchange_rate_amount.Visible = false;
      }

    }

    #endregion

    #region Private

    /// <summary>
    /// Initialize control resources (NLS strings, images, etc)
    /// </summary>
    private void InitializeControlResources()
    {
      // - NLS Strings:
      //   - Title:
      this.FormTitle = Resource.String("STR_CURRENCY_REFUND_TITLE");
      this.FormSubTitle = "";

      //   - Labels
      lbl_max_refund.Text = Resource.String("STR_MAX_VALUE_TO_REFUND");
      lbl_exchange.Text = Resource.String("STR_CHANGE_VALUE");
      lbl_given_amount.Text = Resource.String("STR_NATIONAL_EXCHANGE_VALUE");
      lbl_amount_to_give_foreign.Text = Resource.String("STR_CURRENCY_EXCHANGE");
      lbl_exchange_rate.Text = Resource.String("STR_VOUCHER_EXCHANGE_RATE");
      lbl_amount_to_give_national.Text = "";

      //   - Buttons
      btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");
      btn_ok.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_OK");

      //   - Other

      btn_ok.Enabled = false;
      m_movements_list = new List<CASHIER_MOVEMENT>();
      m_movements_list.Add(CASHIER_MOVEMENT.CURRENCY_EXCHANGE_TO_NATIONAL_CURRENCY);
      m_movements_list.Add(CASHIER_MOVEMENT.CURRENCY_EXCHANGE_TO_FOREIGN_CURRENCY);
      m_movements_list.Add(CASHIER_MOVEMENT.CURRENCY_EXCHANGE_SPLIT1);

      SetBlankExchange(false);
      m_amount_input = new frm_generic_amount_input();
      m_amount_input.OnKeyPressed += new frm_generic_amount_input.KeyPressed(m_amount_input_OnKeyPress);
      m_amount_input_foreign = new frm_generic_amount_input();
      m_amount_input_foreign.OnKeyPressed += new frm_generic_amount_input.KeyPressed(m_amount_input_foreign_OnKeyPress);

      uc_payment_method1.HeaderText = Resource.String("STR_VOUCHER_CAGESESSIONREPORT_CURRENCY");

      List<CurrencyExchangeType> _types = new List<CurrencyExchangeType>();

      _types.Add(CurrencyExchangeType.CURRENCY);

      CurrencyExchange.GetAllowedCurrencies(true, _types, false, out m_national_currency, out m_currencies);

      // Remove national currency
      m_currencies.Remove(m_national_currency);

      uc_payment_method1.VoucherType = VoucherTypes.CardAdd; // m_voucher_type
      uc_payment_method1.InitControl(m_currencies, null, FeatureChips.ChipsOperation.Operation.NONE);

      uc_payment_method1.CurrencyExchange = m_national_currency;


    } // InitializeControlResources

    private void CalculateMaxRefund()
    {
      Decimal _max_amount;
      String _decimals_aux;
      String _decimals_national_aux;
      CurrencyExchangeResult _result;

      if (m_selected_currency != null)
      {
        _decimals_aux = m_selected_currency.Decimals.ToString();
        if (!m_card_data.IsVirtualCard)
        {
          _max_amount = WSI.Common.Cashier.GetLastOperationsAmount(m_movements_list, m_card_data.AccountId, m_selected_currency.CurrencyCode);
          m_max_foreign_amount = _max_amount;
          lbl_max_refund_amount.Text = m_max_foreign_amount.ToString("N" + _decimals_aux) + " " + m_selected_currency.CurrencyCode;
          m_selected_currency.ApplyExchange(m_max_foreign_amount, out _result);
          _decimals_national_aux = m_national_currency.Decimals.ToString();
          lbl_exchange_amount.Text = _result.GrossAmount.ToString("N" + _decimals_national_aux) + " " + m_national_currency.CurrencyCode;
        }
        else
        {
          m_max_foreign_amount = Decimal.MaxValue;
        }
      }
    }

    /// <summary>
    /// Calculate currency exchange (from national to foreign)
    /// </summary>
    private void CalculateExchangeFromNational()
    {
      Boolean _btnok_enabled;

      if (!m_blocked && !m_foreign_forced)
      {
        m_blocked = true;
        if (m_selected_currency != null)
        {
          CurrencyExchangeResult _exchange_result_rest;
          CurrencyExchangeResult _exchange_result_aux;
          Decimal _national_amount;
          Decimal _national_final_amount;
          Decimal _foreign_final;
          Decimal _foreign_rest;
          String _decimals_foreign_aux;
          String _decimals_nacional_aux;

          _national_amount = Format.ParseCurrency(uc_given_amount.Text);
          _decimals_foreign_aux = m_selected_currency.Decimals.ToString();
          _decimals_nacional_aux = m_national_currency.Decimals.ToString();
          m_max_national_reached = false;

          CalculateMaxRefund();

          if (_national_amount > 0)
          {
            _btnok_enabled = true;
            m_selected_currency.ApplyInverseExchange(_national_amount, out m_exchange_result, false, 0, false);
            if (m_exchange_result.GrossAmount > m_max_foreign_amount)
            {
              if (m_max_foreign_amount == 0)
              {
                _national_final_amount = _national_amount;
                _foreign_final = 0;
                _btnok_enabled = false;
              }
              else
              {
                _foreign_final = m_max_foreign_amount;
                m_selected_currency.ApplyExchange(_foreign_final, out _exchange_result_aux);
                _foreign_rest = m_exchange_result.GrossAmount - _foreign_final;
                m_selected_currency.ApplyExchange(_foreign_rest, out _exchange_result_rest);
                _national_final_amount = _exchange_result_rest.GrossAmount;
              }
              m_exchange_result.GrossAmount = _foreign_final;
              m_exchange_result.NetAmountSplit1 = _foreign_final;
            }
            else
            {
              _foreign_final = m_exchange_result.GrossAmount;
              _national_final_amount = 0;
            }

            m_exchange_result.OutCurrencyCode = m_selected_currency.CurrencyCode;
            uc_amount_to_give_foreign.TextChanged -= new System.EventHandler(this.uc_amount_to_give_foreign_TextChanged);
            m_exchange_result.NetAmountDevolution = _national_final_amount;

            uc_amount_to_give_foreign.Text = _foreign_final.ToString("N" + _decimals_foreign_aux);
            uc_amount_to_give_foreign.TextChanged += new System.EventHandler(this.uc_amount_to_give_foreign_TextChanged);

            lbl_refund_national.Text = _national_final_amount.ToString("N" + _decimals_nacional_aux);
            lbl_refund_iso_foreign.Text = m_selected_currency.CurrencyCode;
            m_exchange_result.ChangeRate = 1 / m_exchange_result.ChangeRate;
            m_exchange_result.Comission = 0;

            btn_ok.Enabled = _btnok_enabled;
          }
          else
          {
            SetBlankExchange(true);
          }
        }
        else
        {
          SetBlankExchange(false);
        }
        m_blocked = false;
      }
    }

    /// <summary>
    /// Calculate currency exchange (from foreign to national)
    /// </summary>
    private void CalculateExchangeFromForeign()
    {
      try
      {
        if (!m_blocked)
        {
          m_blocked = true;
          if (m_selected_currency != null)
          {
            Decimal _national_amount;
            Decimal _national_final_amount;
            Decimal _foreign_forced;
            CurrencyExchangeResult _national_forced;
            String _decimals_foreign_aux;
            String _decimals_nacional_aux;

            m_foreign_forced = true;
            _national_amount = Format.ParseCurrency(uc_given_amount.Text);
            _decimals_foreign_aux = m_selected_currency.Decimals.ToString();
            _decimals_nacional_aux = m_national_currency.Decimals.ToString();
            m_max_national_reached = false;

            _foreign_forced = Format.ParseCurrency(uc_amount_to_give_foreign.Text);

            CalculateMaxRefund();


            // Recalculate national amount
            m_selected_currency.ApplyExchange(_foreign_forced, out _national_forced);
            if (_national_forced.GrossAmount > m_max_national_amount)
            {
              m_max_national_reached = true;
            }
            _national_final_amount = _national_amount - _national_forced.GrossAmount;

            m_exchange_result = _national_forced;
            m_exchange_result.InAmount = _national_amount;
            m_exchange_result.InCurrencyCode = m_national_currency.CurrencyCode;
            m_exchange_result.NetAmountSplit1 = m_exchange_result.GrossAmount = _foreign_forced;
            m_exchange_result.OutCurrencyCode = m_selected_currency.CurrencyCode;
            m_exchange_result.NetAmountDevolution = _national_final_amount;
            lbl_refund_national.Text = _national_final_amount.ToString("N" + _decimals_nacional_aux);

            lbl_given_iso.Text = m_national_currency.CurrencyCode;
            lbl_refund_iso_foreign.Text = m_selected_currency.CurrencyCode;
            lbl_refund_iso_national.Text = m_national_currency.CurrencyCode;
            lbl_refund_iso_foreign.Text = m_selected_currency.CurrencyCode;
            m_exchange_result.ChangeRate = 1 / m_exchange_result.ChangeRate;
            m_exchange_result.Comission = 0;
            btn_ok.Enabled = true;
            if (_foreign_forced <= 0 || _national_final_amount < 0)
            {
              btn_ok.Enabled = false;
            }
          }
          else
          {
            SetBlankExchange(false);
          }
          m_blocked = false;
        }
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }

    /// <summary>
    /// Set controls to blank
    /// </summary>
    private void SetBlankExchange(bool keep_iso_codes)
    {
      m_blocked = true;
      m_exchange_result = null;

      uc_given_amount.Text = "";
      lbl_refund_national.Text = "";
      uc_amount_to_give_foreign.Text = "";
      if (!keep_iso_codes)
      {
        lbl_refund_iso_foreign.Text = "";
        lbl_refund_iso_national.Text = "";
        lbl_given_iso.Text = "";
      }
      btn_ok.Enabled = false;
      m_max_national_amount = 0;
      m_max_national_reached = false;
      m_blocked = false;
      lbl_refund_national.Text = "";
    }

    /// <summary>
    /// controls amount_input logic
    /// </summary>
    private void ShowOrCreate()
    {
      Decimal _amount;
      Boolean result;
      Currency _amount_old;
      
      if (this.m_amount_input == null || this.m_amount_input.IsDisposed)
      {
        this.m_amount_input = new frm_generic_amount_input();
        this.m_amount_input.OnKeyPressed += new frm_generic_amount_input.KeyPressed(m_amount_input_OnKeyPress);
      }

      this.m_amount_input.StartPosition = FormStartPosition.Manual;
      this.m_amount_input.CustomLocation = true;

      m_amount_input.Location = new Point(this.Location.X + this.Width, this.Location.Y);
      this.m_amount_input.BringToFront();

      _amount = Format.ParseCurrency(uc_given_amount.Text);

      _amount_old = _amount;

      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_currency_refund", Log.Type.Message);
      result = this.m_amount_input.Show(GENERIC_AMOUNT_INPUT_TYPE.AMOUNT, this.FormTitle, ref _amount);//ferran

      // JBC 08-OCT-2014 If it's same amount, don't set value on textbox (setting value on textbox throws keypress event and recalculate Exchange).
      if (_amount_old != _amount)
      {
        if (result)
        {
          uc_given_amount.Text = ((Currency)_amount).ToStringWithoutSymbols();
        }
        this.m_amount_input.BringToFront();
        this.Modify = false;
      }
    }

    /// <summary>
    /// controls amount_input_foreign logic
    /// </summary>
    private void ShowOrCreateForeign()
    {
      Decimal _amount;
      Boolean result;
      Currency _amount_old;
      
      if (this.m_amount_input_foreign == null || this.m_amount_input_foreign.IsDisposed)
      {
        this.m_amount_input_foreign = new frm_generic_amount_input();
        this.m_amount_input_foreign.OnKeyPressed += new frm_generic_amount_input.KeyPressed(m_amount_input_foreign_OnKeyPress);
      }

      this.m_amount_input_foreign.StartPosition = FormStartPosition.Manual;
      this.m_amount_input_foreign.CustomLocation = true;

      m_amount_input_foreign.Location = new Point(this.Location.X + this.Width, this.Location.Y);
      this.m_amount_input_foreign.BringToFront();

      _amount = Format.ParseCurrency(uc_amount_to_give_foreign.Text);

      _amount_old = _amount;

      result = this.m_amount_input_foreign.Show(GENERIC_AMOUNT_INPUT_TYPE.AMOUNT, this.FormTitle, ref _amount);//ferran

      // JBC 08-OCT-2014 If it's same amount, don't set value on textbox (setting value on textbox throws keypress event and recalculate Exchange).
      if (_amount_old != _amount)
      {
        if (result)
        {
          this.uc_amount_to_give_foreign.Text = ((Currency)_amount).ToStringWithoutSymbols();
        }
        this.m_amount_input_foreign.BringToFront();
        this.Modify = false;
      }
    }

    #endregion

    #region Events

    void m_amount_input_OnKeyPress(String Amount)
    {
      uc_given_amount.Text = Amount;
    }

    void m_amount_input_foreign_OnKeyPress(string Amount)
    {
      uc_amount_to_give_foreign.Text = Amount;
    }

    private void btn_cancel_Click(object sender, EventArgs e)
    {
      this.DialogResult = System.Windows.Forms.DialogResult.Cancel;

      Misc.WriteLog("[FORM CLOSE] frm_currency_refund (cancel)", Log.Type.Message);
      this.Close();
    }

    private void btn_ok_Click(object sender, EventArgs e)
    {
      Decimal _foreign_amount;
      String _error_str;
      if (m_max_national_reached)
      {
        frm_message.Show(Resource.String("STR_MAX_NATIONAL_AMOUNT_REACHED"), "WARNING", MessageBoxButtons.OK, this);
      }
      else
      {
        if (Decimal.TryParse(uc_amount_to_give_foreign.Text, out _foreign_amount))
        {
          if (_foreign_amount > m_max_foreign_amount)
          {
            if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.MultiCurrency_ExceedMaxReturn,
                                           ProfilePermissions.TypeOperation.RequestPasswd,
                                           this,
                                           out _error_str))
            {
              return;
            }

          }
          this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }
      }

    }


    /// <summary>
    /// Get Currency excheange Format
    /// </summary>
    /// <returns>String Exchange</returns>
    private string GetCurrencyExchangeFormat()
    {
      String _exchange;
      if (m_selected_currency == null || m_exchange_result == null || m_national_currency == null)
      {
        m_selected_currency.ApplyExchange(1, out m_exchange_result);
      }
      _exchange = String.Format("1  {0} = {1} {2}", m_selected_currency.CurrencyCode,
                                                    m_exchange_result.ChangeRate.ToString("n8"),
                                                    m_national_currency.CurrencyCode);
      return _exchange;
    }

    private void uc_foreign_currency_TextChanged(object sender, EventArgs e)
    {
      CalculateExchangeFromNational();

    }

    private void uc_given_amount_Click(object sender, EventArgs e)
    {
      this.uc_given_amount.BackColor = Color.LightGoldenrodYellow;
      this.uc_given_amount.Refresh();

      ShowOrCreate();

      uc_given_amount.TextChanged -= new System.EventHandler(this.uc_foreign_currency_TextChanged);
      uc_given_amount.Text = ((Currency)Format.ParseCurrency(uc_given_amount.Text)).ToStringWithoutSymbols();
      uc_given_amount.TextChanged += new System.EventHandler(this.uc_foreign_currency_TextChanged);
    }

    private void uc_given_amount_Enter(object sender, EventArgs e)
    {
      Currency _value;

      uc_given_amount.TextChanged -= new System.EventHandler(this.uc_given_amount_TextChanged);

      _value = Format.ParseCurrency(this.uc_given_amount.Text);
      uc_given_amount.Text = _value.ToStringWithoutSymbols();

      uc_given_amount.TextChanged += new System.EventHandler(this.uc_given_amount_TextChanged);
    }

    private void uc_given_amount_KeyPress(object sender, KeyPressEventArgs e)
    {
      uc_round_textbox _txt_aux;

      _txt_aux = uc_given_amount;
    }

    private void uc_given_amount_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
    {
      char c;

      c = Convert.ToChar(e.KeyCode);
      if (c == '\r')
      {
        e.IsInputKey = true;
      }
    }

    private void uc_given_amount_TextChanged(object sender, EventArgs e)
    {
      m_foreign_forced = false;
      CalculateExchangeFromNational();
    }

    private void frm_currency_refund_Activated(object sender, EventArgs e)
    {
      if (m_first_time)
      {
        this.Location = new Point(this.Location.X - 123, this.Location.Y);
        CalculateMaxRefund();
        m_first_time = false;
      }

      if (!Modify)
      {
        Modify = false;

        if (m_amount_input != null)
        {
          m_amount_input.Hide();
          m_amount_input.Dispose();
          m_amount_input = null;
        }
      }

      if (m_amount_input_foreign != null)
      {
        m_amount_input_foreign.Hide();
        m_amount_input_foreign.Dispose();
        m_amount_input_foreign = null;
      }
    }

    private void uc_amount_to_give_foreign_TextChanged(object sender, EventArgs e)
    {
      CalculateExchangeFromForeign();
    }

    private void uc_amount_to_give_foreign_Click(object sender, EventArgs e)
    {
      this.uc_amount_to_give_foreign.BackColor = Color.LightGoldenrodYellow;
      this.uc_amount_to_give_foreign.Refresh();

      ShowOrCreateForeign();

      uc_amount_to_give_foreign.TextChanged -= new System.EventHandler(this.uc_amount_to_give_foreign_TextChanged);
      uc_amount_to_give_foreign.Text = ((Currency)Format.ParseCurrency(uc_amount_to_give_foreign.Text)).ToStringWithoutSymbols();
      uc_amount_to_give_foreign.TextChanged += new System.EventHandler(this.uc_amount_to_give_foreign_TextChanged);
    }

    private void uc_amount_to_give_foreign_Enter(object sender, EventArgs e)
    {
      Currency _value;

      uc_amount_to_give_foreign.TextChanged -= new System.EventHandler(this.uc_amount_to_give_foreign_TextChanged);

      _value = Format.ParseCurrency(this.uc_amount_to_give_foreign.Text);
      uc_amount_to_give_foreign.Text = _value.ToStringWithoutSymbols();

      uc_amount_to_give_foreign.TextChanged += new System.EventHandler(this.uc_amount_to_give_foreign_TextChanged);
    }

    private void uc_amount_to_give_foreign_KeyPress(object sender, KeyPressEventArgs e)
    {
      uc_round_textbox _txt_aux;
      _txt_aux = uc_amount_to_give_foreign;

    }

    private void uc_amount_to_give_foreign_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
    {
      char c;

      c = Convert.ToChar(e.KeyCode);
      if (c == '\r')
      {
        e.IsInputKey = true;
      }

    }

    private void uc_given_amount_Leave(object sender, EventArgs e)
    {
      String _decimals_national_aux;
      Decimal _amount;

      _decimals_national_aux = m_national_currency.Decimals.ToString();

      _amount = Format.ParseCurrency(uc_given_amount.Text);
      m_max_national_amount = _amount;

      uc_given_amount.TextChanged -= new System.EventHandler(this.uc_given_amount_TextChanged);
      uc_given_amount.Text = _amount.ToString("N" + _decimals_national_aux);
      uc_given_amount.TextChanged += new System.EventHandler(this.uc_given_amount_TextChanged);
      this.uc_given_amount.Style = uc_round_textbox.RoundTextBoxStyle.BASIC_CUSTOM;
      this.uc_given_amount.Refresh();
    }

    private void uc_amount_to_give_foreign_Leave(object sender, EventArgs e)
    {
      String _decimals_foreign_aux;
      Decimal _amount;

      _decimals_foreign_aux = m_selected_currency.Decimals.ToString();

      _amount = Format.ParseCurrency(uc_amount_to_give_foreign.Text);

      m_blocked = true;
      uc_amount_to_give_foreign.TextChanged -= new System.EventHandler(this.uc_amount_to_give_foreign_TextChanged);

      uc_amount_to_give_foreign.Text = _amount.ToString("N" + _decimals_foreign_aux);
      uc_amount_to_give_foreign.TextChanged += new System.EventHandler(this.uc_amount_to_give_foreign_TextChanged);

      m_blocked = false;
      this.uc_amount_to_give_foreign.Style = uc_round_textbox.RoundTextBoxStyle.BASIC_CUSTOM;
      this.uc_amount_to_give_foreign.Refresh();

    }

    private void frm_currency_refund_Load(object sender, EventArgs e)
    {
      String _last_operation_isocode;
      Currency _last_cash_out_amount;
      Int32? _decimals_national_aux;

      _decimals_national_aux = m_national_currency.Decimals;

      _last_operation_isocode = WSI.Common.Cashier.GetLastOperationIsoCode(m_movements_list, m_card_data.AccountId);

      if (_last_operation_isocode != "")
      {
        uc_payment_method1.CurrencyExchange = m_currencies.Find(
          delegate(CurrencyExchange _cur_ex)
          {
            return _cur_ex.CurrencyCode == _last_operation_isocode;
          });
      }
      m_selected_currency = uc_payment_method1.CurrencyExchange;

      _last_cash_out_amount = WSI.Common.Cashier.GetLastCashOutFromCashier(Cashier.TerminalId, 300);
      uc_given_amount.Text = _last_cash_out_amount.ToString("n" + _decimals_national_aux);
    }

    private void lbl_refund_national_TextChanged(object sender, EventArgs e)
    {
      if (lbl_refund_national.Text.StartsWith("-"))
      {
        lbl_refund_national.ForeColor = lbl_refund_iso_national.ForeColor = Color.Red;
        lbl_amount_to_give_national.Text = Resource.String("STR_UC_CARD_PENDING_DEPOSITS");
        lbl_refund_national.TextChanged -= new System.EventHandler(this.lbl_refund_national_TextChanged);
        lbl_refund_national.Text = lbl_refund_national.Text.Remove(0, 1);
        lbl_refund_national.TextChanged += new System.EventHandler(this.lbl_refund_national_TextChanged);
      }
      else
      {
        lbl_refund_national.ForeColor = lbl_refund_iso_national.ForeColor = Color.Black;
        lbl_amount_to_give_national.Text = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_DIFF_EXTRA");
      }
    }

    private void uc_payment_method1_CurrencyChanged(CurrencyExchange CurrencyExchange)
    {
      if (CurrencyExchange != null)
      {
        m_foreign_forced = false;
        m_selected_currency = CurrencyExchange;
        lbl_given_iso.Text = lbl_refund_iso_national.Text = m_national_currency.CurrencyCode;
        lbl_refund_iso_foreign.Text = m_selected_currency.CurrencyCode;
        CalculateExchangeFromNational();
        CalculateMaxRefund();
        lbl_exchange_rate_amount.Text = GetCurrencyExchangeFormat();
      }
    }

    #endregion

    //_last_cash_out_amount = WSI.Common.Cashier.GetLastCashOutFromCashier(0, 300);
    //uc_given_amount.Text = _last_cash_out_amount.ToString("n" + _decimals_national_aux);
  }
}
