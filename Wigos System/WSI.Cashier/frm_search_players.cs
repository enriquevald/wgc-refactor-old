//------------------------------------------------------------------------------
// Copyright � 2010 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : frm_search_players.cs
// 
//   DESCRIPTION : Player tracking edition
// 
//        AUTHOR: APB
// 
// CREATION DATE: 27-OCT-2010
// 
// REVISION HISTORY :
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 27-OCT-2010 APB    First draft
// 20-APR-2012 JCM    Modified the form returned Encrypted TrackData by Account Id
// 08-MAY-2012 JAB    Fixed Defect #286: filter by holder
// 16-JUL-2012 RXM    Added field to find by external reference (imported user accounts)
// 05-FEB-2013 LEM    Replace/remove class NumericTextBox2 by WSI.Cashier.NumericTextBox 
// 07-FEB-2013 LEM    Added new optional filters acording general params Account.ExtendedSearch
//                    Moved Init() from frm_search_players() to Show() 
//                    Modificated Init() to recalculate form dimensions and location
// 12-FEB-2013 ICS    Fixed Bug with non numeric values in birthday fields 
//                    Moved birthday validation from CheckData() to ValidateFormat()
// 26-MAR-2013 RRB    Now use SQLCommand to do the search for accounts (GetSqlCommand())
// 03-JUL-2013 RRR    Moddified view parameters for Window Mode
// 09-AGO-2013 LEM    Auto TAB for date fields
// 12-AGO-2013 ANG    Expand search by AC_HOLDER_DOCUMENT_ID1 and AC_HOLDER_DOCUMENT_ID2
// 13-AUG-2013 DRV    Added automatic OSK support
// 20-AUG-2013 FBA    WIG-104: Fixed bug with auto-tab on date fields
// 14-APR-2014 DLL    Added new functionality: VIP Account
// 24-NOV-2015 FOS    Fixed Bug 6939: Search accounts when it pressed enter key
// 01-FEB-2016 RAB    Bug 8836:Excepci�n no controlada al buscar cuentas
//------------------------------------------------------------------------------

using System;
using System.Drawing;
using System.Windows.Forms;
using WSI.Common;
using System.Data.SqlClient;
using System.Text;
using System.Data;

namespace WSI.Cashier
{
  public partial class frm_search_players : Controls.frm_base
  {
    #region Attributes

    DateTime m_birthday_filter;
    DateTime m_wedding_filter;
    Int32 m_optional_filters;
    Int32 m_vertical_span;
    const Int32 MIN_HEIGHT_VALUE = 315;
    const Int32 FREE_VERTICAL_HEIGHT = 446;

    Int64 m_account_id;
    private frm_yesno form_yes_no = new frm_yesno();
    private frm_browse_players form_browse_players = new frm_browse_players();

    #endregion

    #region Constructor


    //------------------------------------------------------------------------------
    // PURPOSE: Form constructor
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:

    public frm_search_players()
    {
      InitializeComponent();

      InitializeControlResources();

    } // frm_search_players

    #endregion

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE: Initialize NLS resources
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:

    public void InitializeControlResources()
    {
      // - NLS Strings:
      //   - Title
      this.FormTitle = Resource.String("STR_FRM_PLAYER_SEARCH_001");
      //lbl_player_selection.Text = 

      //   - Labels
      lbl_external_id.Text = Resource.String("STR_FRM_PLAYER_SEARCH_008");
      lbl_name.Text = Resource.String("STR_FRM_PLAYER_SEARCH_002");
      lbl_id.Text = Resource.String("STR_FRM_PLAYER_SEARCH_003");
      lbl_phone.Text = Resource.String("STR_UC_PLAYER_TRACK_PHONE_01");
      uc_birth_date.DateText = Resource.String("STR_FRM_PLAYER_SEARCH_004");
      uc_wedding_date.DateText = Resource.String("STR_UC_PLAYER_TRACK_WEDDING");

      chk_vip_account.Text = Resource.String("STR_ONLY_VIP_ACCOUNT");

      //   - Buttons
      btn_ok.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_OK");
      btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");

      // Dependent forms
      form_browse_players.InitializeControlResources();

    } // InitializeControlResources

    //------------------------------------------------------------------------------
    // PURPOSE: Display form to search for players
    // 
    //  PARAMS:
    //      - INPUT: ParentForm
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //    Trackdata of the selected player (if one is selected).
    // 
    //   NOTES:

    public Int64 Show(Form ParentForm)
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_search_players", Log.Type.Message);

      m_account_id = -1;
      this.txt_name.Text = "";
      this.txt_id.Text = "";
      this.lbl_msg_blink.Visible = false;
      this.txt_external_id.Text = "";
      this.txt_phone.Text = "";
      this.chk_vip_account.Checked = false;
      uc_birth_date.Init();
      uc_wedding_date.Init();

      m_birthday_filter = DateTime.MinValue;
      m_wedding_filter = DateTime.MinValue;

      LoadOptionalFilters();

      Init();
      //Shows OSK if Automatic OSK it's enabled
      WSIKeyboard.Show();

      form_yes_no.Show();

      if (WindowManager.StartUpMode == WindowManager.StartUpModes.Explorer)
      {
        this.Location = new Point(WindowManager.GetFormCenterLocation(form_yes_no).X - (this.Width / 2), form_yes_no.Location.Y + 50);
      }
      this.ShowDialog(form_yes_no);

      form_yes_no.Hide();

      return m_account_id;
    } // Show

    //------------------------------------------------------------------------------
    // PURPOSE : Dispose yes_no form.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //

    new public void Dispose()
    {
      form_yes_no.Dispose();
      form_yes_no = null;
      form_browse_players.Dispose();
      form_browse_players = null;

      base.Dispose();
    } // Dispose

    #endregion

    #region Private Methods

    private void LoadOptionalFilters()
    {
      Boolean _wedding_date;
      Boolean _phone_number;
      Int32 _is_vip_pos_x;
      Int32 _is_vip_pos_y;

      _is_vip_pos_x = this.txt_name.Location.X;
      _is_vip_pos_y = this.uc_wedding_date.Location.Y;

      m_optional_filters = 0;
      m_vertical_span = this.txt_id.Location.Y - this.txt_name.Location.Y;

      try
      {
        _wedding_date = GeneralParam.GetBoolean("Account.ExtendedSearch", "WeddingDate") && GeneralParam.GetBoolean("Account.VisibleField", "WeddingDate");
        _phone_number = GeneralParam.GetBoolean("Account.ExtendedSearch", "PhoneNumber") && GeneralParam.GetBoolean("Account.VisibleField", "Phone1");

        //The calling order of these functions determines the position of the controls
        FilterVisibility("WeddingDate", _wedding_date);
        FilterVisibility("PhoneNumber", _phone_number);

        if (_wedding_date)
        {
          _is_vip_pos_y = this.txt_phone.Location.Y;
        }

        if (_phone_number)
        {
          _is_vip_pos_y = this.txt_phone.Location.Y + m_vertical_span;
        }
        this.chk_vip_account.Location = new Point(_is_vip_pos_x, _is_vip_pos_y);
      }
      catch (Exception)
      { }


    }

    private void FilterVisibility(String filter, Boolean visible)
    {
      Int32 _y;

      if (filter == "WeddingDate")
      {
        if (visible)
        {
          m_optional_filters++;
          _y = this.txt_external_id.Location.Y + m_vertical_span * m_optional_filters;

          uc_wedding_date.Top = _y;
          //lbl_wedding_date.Location = new Point(lbl_wedding_date.Location.X, _y);
          //txt_wedding_day.Location = new Point(txt_wedding_day.Location.X, _y);
          //lbl_day_month_sep2.Location = new Point(lbl_day_month_sep2.Location.X, _y);
          //txt_wedding_month.Location = new Point(txt_wedding_month.Location.X, _y);
          //lbl_month_year_sep2.Location = new Point(lbl_month_year_sep2.Location.X, _y);
          //txt_wedding_year.Location = new Point(txt_wedding_year.Location.X, _y);
          //lbl_legend_wedding_date.Location = new Point(lbl_legend_wedding_date.Location.X, _y);
        }

        uc_wedding_date.Visible = visible;
      }

      if (filter == "PhoneNumber")
      {
        if (visible)
        {
          m_optional_filters++;
          _y = this.txt_external_id.Location.Y + m_vertical_span * m_optional_filters;
          lbl_phone.Location = new Point(lbl_phone.Location.X, _y);
          txt_phone.Location = new Point(txt_phone.Location.X, _y);
        }
        lbl_phone.Visible = visible;
        txt_phone.Visible = visible;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Control initialization
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    private void Init()
    {
      //Size
      //this.Height = MIN_HEIGHT_VALUE + m_vertical_span * m_optional_filters;

      // Location
      this.Location = new Point(1024 / 2 - this.Width / 2, FREE_VERTICAL_HEIGHT / 2 - this.Height / 2);

      form_yes_no.Opacity = 0.6;

    } // Init

    //------------------------------------------------------------------------------
    // PURPOSE: Check data in search form
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //    True if all the information in text controls is correct.
    //    False otherwise.
    // 
    //   NOTES:
    private Boolean CheckData()
    {
      lbl_msg_blink.Visible = false;
      lbl_msg_blink.Text = "";

      try
      {
        // Check if there is at least one value
        if (uc_birth_date.IsEmpty &&
            txt_name.Text.Trim() == "" &&
            txt_id.Text.Trim() == "" &&
            txt_external_id.Text.Trim() == "" &&
            (uc_wedding_date.Visible &&
            uc_wedding_date.IsEmpty &&
            !chk_vip_account.Checked || 
            !uc_wedding_date.Visible) &&
            (lbl_phone.Visible && txt_phone.Text.Trim() == "" || !lbl_phone.Visible))
        {
          lbl_msg_blink.Text = Resource.String("STR_FRM_PLAYER_SEARCH_007");

          return false;
        }

        // Validate format of the fields
        if (!ValidateFormat())
        {
          return false;
        }

        return true;
      }
      finally
      {
        if (lbl_msg_blink.Text != "")
        {
          lbl_msg_blink.Visible = true;
        }
      }
    } // CheckData

    //------------------------------------------------------------------------------
    // PURPOSE: Construct SQL query to search for accounts
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //    SQL Command.
    // 
    //   NOTES:

    private SqlCommand GetSqlCommand()
    {
      SqlCommand _sql_cmd;
      StringBuilder _sb;
      String _str_where = "";
      String _with_index = "";
      String _search_text = "";
      int _max_rows;

      _sb = new StringBuilder();
      _sql_cmd = new SqlCommand();

      if (txt_name.Text != "")
      {
        _search_text = txt_name.Text.Replace("'", "''");
        _str_where += " AND ( AC_HOLDER_NAME LIKE '" + _search_text + "%' OR";
        _str_where += "       AC_HOLDER_NAME LIKE '% " + _search_text + "%') ";
      }

      if (txt_id.Text != "")
      {
        _search_text = txt_id.Text.Replace("'", "''");
        _str_where += " AND ( AC_HOLDER_ID LIKE @pSearchDocument OR AC_HOLDER_ID1 LIKE @pSearchDocument OR AC_HOLDER_ID2 LIKE @pSearchDocument ) ";

        _sql_cmd.Parameters.Add("@pSearchDocument", SqlDbType.NVarChar, 50).Value = String.Format("{0}%", _search_text);

      }

      if (m_birthday_filter != DateTime.MinValue)
      {
        _str_where += " AND AC_HOLDER_BIRTH_DATE = @pBirthDate ";
        _with_index += " WITH(INDEX(IX_ac_holder_birth_date)) ";

        _sql_cmd.Parameters.Add("@pBirthDate", SqlDbType.DateTime).Value = m_birthday_filter;
      }

      if (uc_wedding_date.Visible && m_wedding_filter != DateTime.MinValue)
      {
        _str_where += " AND AC_HOLDER_WEDDING_DATE = @pWeddingDate ";

        _sql_cmd.Parameters.Add("@pWeddingDate", SqlDbType.DateTime).Value = m_wedding_filter;
      }

      if (chk_vip_account.Checked)
      {
        _str_where += " AND AC_HOLDER_IS_VIP = 1 ";
      }

      if (txt_external_id.Text != "")
      {
        _search_text = txt_external_id.Text.Replace("'", "''");
        _str_where += " AND ( AC_EXTERNAL_REFERENCE LIKE '%" + _search_text + "%') ";
      }

      if (lbl_phone.Visible && txt_phone.Text != "")
      {
        _search_text = txt_phone.Text.Replace("'", "''");
        _str_where += " AND ( REPLACE(AC_HOLDER_PHONE_NUMBER_01, ' ', '') LIKE REPLACE('%" + _search_text + "%', ' ', '')";
        _str_where += "  OR   REPLACE(AC_HOLDER_PHONE_NUMBER_02, ' ', '') LIKE REPLACE('%" + _search_text + "%', ' ', '') )";
      }

      _max_rows = frm_browse_players.GRID_MAX_ROWS + 1;

      if (txt_external_id.Text.Trim() == "")
      {
        _sb.AppendLine(" SELECT   TOP (" + _max_rows.ToString() + ") AC_ACCOUNT_ID ");
        // RAB 1-FEB-2016
        _sb.AppendLine("        , ISNULL(AC_TRACK_DATA, '') ");
        _sb.AppendLine("        , AC_HOLDER_NAME          ");
        _sb.AppendLine("        , AC_HOLDER_ID            ");
        _sb.AppendLine("        , AC_HOLDER_BIRTH_DATE    ");
        _sb.AppendLine("   FROM   ACCOUNTS                ");
        _sb.AppendLine(_with_index);
        _sb.AppendLine("  WHERE   AC_USER_TYPE = 1        ");
        _sb.AppendLine(_str_where);
        _sb.AppendLine("  ORDER   BY AC_HOLDER_NAME       ");
      }
      else
      {
        _sb.AppendLine(" SELECT   TOP (" + _max_rows.ToString() + ") AC_ACCOUNT_ID ");
        _sb.AppendLine("        , AC_TRACK_DATA           ");
        _sb.AppendLine("        , ISNULL(AC_HOLDER_NAME, '') +' ref.: ' + AC_EXTERNAL_REFERENCE ");
        _sb.AppendLine("        , AC_HOLDER_ID            ");
        _sb.AppendLine("        , AC_HOLDER_BIRTH_DATE    ");
        _sb.AppendLine("   FROM   ACCOUNTS                ");
        _sb.AppendLine(_with_index);
        _sb.AppendLine("  WHERE   0 = 0                   ");
        _sb.AppendLine(_str_where);
        _sb.AppendLine("  ORDER   BY AC_EXTERNAL_REFERENCE");

      } // end   if (!(chk_anonymous.Checked))

      _sql_cmd.CommandText = _sb.ToString();

      return _sql_cmd;
    } // GetSqlCommand

    // PURPOSE : Validates the format of Name and Phone fields
    //
    //  PARAMS:
    //     - INPUT:
    //
    //     - OUTPUT:
    //
    // RETURNS:
    //         - TRUE:   If the format of all fields is correct
    //         - FALSE:  If there is a field that has incorrect format
    //
    private Boolean ValidateFormat()
    {
      DateTime _birthday;
      DateTime _wedding_date;

      _birthday = DateTime.MinValue;
      _wedding_date = DateTime.MinValue;

      // Check if it must validate the birthday fields
      if (!uc_birth_date.IsEmpty)
      {
        if (!uc_birth_date.ValidateFormat())
        {
          lbl_msg_blink.Text = Resource.String("STR_FRM_PLAYER_SEARCH_005");

          return false;
        }

        m_birthday_filter = uc_birth_date.DateValue;
      }

      // Check if it must validate the wedding fields
      if (uc_wedding_date.Visible
       && !uc_wedding_date.IsEmpty)
      {
        if(!uc_wedding_date.ValidateFormat())
        {
          lbl_msg_blink.Text = Resource.String("STR_FRM_PLAYER_SEARCH_010");

          return false;
        }
        
        // Set wedding filter
        m_wedding_filter = uc_wedding_date.DateValue;
      }

      return true;
    } // ValidateFormat()

    #endregion

    #region "Handlers"

    //------------------------------------------------------------------------------
    // PURPOSE: Set focus when entering screen
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:

    private void frm_search_players_Shown(object sender, EventArgs e)
    {
      this.txt_name.Focus();
    } // frm_search_players_Shown

    //------------------------------------------------------------------------------
    // PURPOSE: Launch query to select players fromthe given data
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:

    private void btn_ok_Click(object sender, EventArgs e)
    {
      SqlCommand _sql_cmd;

      // if data is correct
      if (CheckData())
      {
        _sql_cmd = GetSqlCommand();

        WSIKeyboard.Hide();
        this.Cursor = Cursors.WaitCursor;
        this.Hide();

        m_account_id = form_browse_players.Show(_sql_cmd);

        form_browse_players.Hide();
        this.Cursor = Cursors.Default;
      }
    } // btn_ok_Click

    //------------------------------------------------------------------------------
    // PURPOSE: Exit screen with no selection
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:

    private void btn_cancel_Click(object sender, EventArgs e)
    {
      // hide keyboard and close
      WSIKeyboard.Hide();

      Misc.WriteLog("[FORM CLOSE] frm_search_player (cancel)", Log.Type.Message);
      this.Close();
    } // btn_cancel_Click

    //------------------------------------------------------------------------------
    // PURPOSE: Toggle on-screen keyboard upon pressing button
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:

    private void btn_keyboard_Click(object sender, EventArgs e)
    {
      // show keyboard (if hidden)
      WSIKeyboard.ForceToggle();
    }


    private void txt_name_KeyPress(object sender, KeyPressEventArgs e)
    {
      if (e.KeyChar == (char)Keys.Return)
      {
        SqlCommand _sql_cmd;

        // if data is correct
        if (CheckData())
        {
          _sql_cmd = GetSqlCommand();

          WSIKeyboard.Hide();
          this.Cursor = Cursors.WaitCursor;
          this.Hide();

          m_account_id = form_browse_players.Show(_sql_cmd);

          form_browse_players.Hide();
          this.Cursor = Cursors.Default;
        }
      }
    }

    #endregion



  }
}