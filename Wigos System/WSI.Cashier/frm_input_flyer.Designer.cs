﻿namespace WSI.Cashier
{
  partial class frm_input_flyer
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.btn_ok = new WSI.Cashier.Controls.uc_round_button();
      this.btn_cancel = new WSI.Cashier.Controls.uc_round_button();
      this.txt_num_flyer = new WSI.Cashier.Controls.uc_round_textbox();
      this.lbl_enter_num_flyer = new WSI.Cashier.Controls.uc_label();
      this.btn_keyboard = new WSI.Cashier.Controls.uc_round_button();
      this.pnl_data.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.Controls.Add(this.btn_keyboard);
      this.pnl_data.Controls.Add(this.btn_ok);
      this.pnl_data.Controls.Add(this.btn_cancel);
      this.pnl_data.Controls.Add(this.txt_num_flyer);
      this.pnl_data.Controls.Add(this.lbl_enter_num_flyer);
      this.pnl_data.Size = new System.Drawing.Size(512, 181);
      // 
      // btn_ok
      // 
      this.btn_ok.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_ok.FlatAppearance.BorderSize = 0;
      this.btn_ok.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_ok.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_ok.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_ok.Image = null;
      this.btn_ok.IsSelected = false;
      this.btn_ok.Location = new System.Drawing.Point(266, 107);
      this.btn_ok.Name = "btn_ok";
      this.btn_ok.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_ok.Size = new System.Drawing.Size(205, 50);
      this.btn_ok.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_ok.TabIndex = 7;
      this.btn_ok.Text = "XOK";
      this.btn_ok.UseVisualStyleBackColor = false;
      this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
      // 
      // btn_cancel
      // 
      this.btn_cancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btn_cancel.FlatAppearance.BorderSize = 0;
      this.btn_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_cancel.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_cancel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_cancel.Image = null;
      this.btn_cancel.IsSelected = false;
      this.btn_cancel.Location = new System.Drawing.Point(40, 107);
      this.btn_cancel.Name = "btn_cancel";
      this.btn_cancel.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_cancel.Size = new System.Drawing.Size(205, 50);
      this.btn_cancel.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_cancel.TabIndex = 6;
      this.btn_cancel.Text = "XCANCEL";
      this.btn_cancel.UseVisualStyleBackColor = false;
      this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
      // 
      // txt_num_flyer
      // 
      this.txt_num_flyer.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_num_flyer.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_num_flyer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_num_flyer.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_num_flyer.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.txt_num_flyer.CornerRadius = 5;
      this.txt_num_flyer.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_num_flyer.Location = new System.Drawing.Point(180, 52);
      this.txt_num_flyer.MaxLength = 6;
      this.txt_num_flyer.Multiline = false;
      this.txt_num_flyer.Name = "txt_num_flyer";
      this.txt_num_flyer.PasswordChar = '\0';
      this.txt_num_flyer.ReadOnly = false;
      this.txt_num_flyer.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_num_flyer.SelectedText = "";
      this.txt_num_flyer.SelectionLength = 0;
      this.txt_num_flyer.SelectionStart = 0;
      this.txt_num_flyer.Size = new System.Drawing.Size(150, 40);
      this.txt_num_flyer.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_num_flyer.TabIndex = 5;
      this.txt_num_flyer.TabStop = false;
      this.txt_num_flyer.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.txt_num_flyer.UseSystemPasswordChar = false;
      this.txt_num_flyer.WaterMark = null;
      this.txt_num_flyer.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // lbl_enter_num_flyer
      // 
      this.lbl_enter_num_flyer.AutoSize = true;
      this.lbl_enter_num_flyer.BackColor = System.Drawing.Color.Transparent;
      this.lbl_enter_num_flyer.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_enter_num_flyer.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_enter_num_flyer.Location = new System.Drawing.Point(188, 18);
      this.lbl_enter_num_flyer.Name = "lbl_enter_num_flyer";
      this.lbl_enter_num_flyer.Size = new System.Drawing.Size(132, 19);
      this.lbl_enter_num_flyer.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_enter_num_flyer.TabIndex = 4;
      this.lbl_enter_num_flyer.Text = "xNone enter code";
      this.lbl_enter_num_flyer.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // btn_keyboard
      // 
      this.btn_keyboard.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
      this.btn_keyboard.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(47)))), ((int)(((byte)(50)))));
      this.btn_keyboard.FlatAppearance.BorderSize = 0;
      this.btn_keyboard.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_keyboard.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_keyboard.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_keyboard.Image = global::WSI.Cashier.Properties.Resources.keyboard;
      this.btn_keyboard.IsSelected = false;
      this.btn_keyboard.Location = new System.Drawing.Point(112, 52);
      this.btn_keyboard.Name = "btn_keyboard";
      this.btn_keyboard.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_keyboard.Size = new System.Drawing.Size(62, 40);
      this.btn_keyboard.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.TERTIARY;
      this.btn_keyboard.TabIndex = 9;
      this.btn_keyboard.TabStop = false;
      this.btn_keyboard.UseVisualStyleBackColor = false;
      this.btn_keyboard.Click += new System.EventHandler(this.btn_keyboard_Click);
      // 
      // frm_input_flyer
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(512, 236);
      this.Name = "frm_input_flyer";
      this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
      this.Style = WSI.Cashier.Controls.frm_base.BaseFormStyle.PRINCIPAL;
      this.Text = "frm_input_flyers";
      this.pnl_data.ResumeLayout(false);
      this.pnl_data.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private Controls.uc_round_button btn_ok;
    private Controls.uc_round_button btn_cancel;
    private Controls.uc_round_textbox txt_num_flyer;
    private Controls.uc_label lbl_enter_num_flyer;
    private Controls.uc_round_button btn_keyboard;
  }
}