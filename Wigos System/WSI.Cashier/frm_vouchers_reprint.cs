//------------------------------------------------------------------------------
// Copyright � 2010 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_vouchers_reprint.cs
// 
//   DESCRIPTION: Implements the following classes:
//                1. frm_vouchers_reprint: List last 20 and current cashier sessions and,
//                   allows to print one or all operation vouchers associated.
//
//        AUTHOR: RCI
// 
// CREATION DATE: 17-SEP-2010
//
// REVISION HISTORY: 
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 17-SEP-2010 RCI    First release.
// 09-MAR-2012 SSC    Added new Operation Codes for Note Acceptor
// 04-APR-2012 JAB    Changes to move operations to obtain and view tickets to the project WSI.Common
// 13-APR-2012 JAB    Changes to move print voucher functions to the project WSI.Common (VoucherManager.cs)
// 31-MAY-2012 RCI    Change Radio Button text if NoteAcceptor is enabled.
// 27-SEP-2012 DDM    Added cashier movement when is reprinted (REQUEST_TO_VOUCHER_REPRINT)
// 07-NOV-2012 JMM    Added OperationCode.PROMOBOX_TOTAL_RECHARGES_PRINT to the listing vouchers
// 05-APR-2013 ICS    Audits voucher reprints
// 09-APR-2013 ICS    Fixed Bug #694: Reprint buttons don't work when the cash session is closed
// 02-JUL-2013 RRR    Moddified view parameters for Window Mode
// 19-AUG-2013 FBA    Added control for new operation (gift redeemable as cash in)
// 04-SEP-2013 NMR    Added features for TITO-Ticket reprint
// 06-SEP-2013 NMR    Added features for TITO-Ticket reprint
// 13-SEP-2013 NMR    Modified features for TITO-ticket reprint
// 18-SEP-2013 JRM    Removed all references to the Ticket re-printing functionallity. A new form has been made for that 
// 03-OCT-2013 NMR    Cleaned TITO warnings.
// 22-OCT-2013 LEM    Unify saving account and cashier movements using SharedMovementsTables clases
// 08-NOV-2013 ACM    Fixed Bug: WIG-372 String for Account Customize Voucher
// 10-FEB-2014 LJM    Fixed Bug: WIGOSTITO-1052 String for tickets payment.
// 10-FEB-2014 LJM    Fixed Bug: WIG-606 Added credit transfer to the filter
// 25-APR-2014 HBB    Fixed Bug: WIGOSTITO-1212 No se muestran los vouchers de tickets offline.
// 18-SEP-2014 DLL    Added new movement Cash Advance
// 10-DEC-2014 DRV    Decimal currency formatting: DUT 201411 - Tito Chile - Currency formatting without decimals
// 26-AUG-2015 DHA    Added validation on reprint by voucher configuration
// 29-OCT-2015 JMV    Product Backlog Item 5864:Dise�o cajero: Aplicar en frm_vouchers_reprint.cs
// 19-JAN-2016 FOS    Backlog Item 7969: Apply Taxes in handpay authorization
// 09-FEB-2016 FAV    PBI 9148, Added operation name for void machine ticket
// 24-MAR-2016 RAB    Bug 10745: Reception: with the GP requiereNewCard to "0" when assigning a card client is counted as "replacement"
// 30-MAR-2016 RGR    Product Backlog Item 10981:Sprint Review 21 Winions - Mex
// 31-MAY-2016 ETP    Fixed bug 13997: Cambio de divisa extranjera: No se visualizan los tickets en el Cajero y en el GUI
// 06-MAY-2016 ETP    Fixed Bug 14116: Don't show Currency in points movements.
// 31-MAR-2017 ETP    PBI 25791: Payback vouchers.
// 08-MAY-2017 DHA    Bug 26735: WTA-350: Wait before printing next voucher
// 20-OCT-2017 RAB    Bug 30338:WIGOS-5198 [Ticket #8867] Reporte de Tickets de caja
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using WSI.Common;
using WSI.Cashier.TITO;
using System.IO;
using System.Drawing.Text;
using System.Reflection;
using System.Runtime.InteropServices;
using WSI.Cashier.Controls;
using System.Collections;

namespace WSI.Cashier
{
  public partial class frm_vouchers_reprint : frm_base
  {
    [DllImport("gdi32.dll")]
    private static extern IntPtr AddFontMemResourceEx(IntPtr pbFont, uint cbFont, IntPtr pdv, [In] ref uint pcFonts);

    #region Constants

    private const Int32 GRID_CS_MAX_COLUMNS = 5;
    private const Int32 GRID_CS_COLUMN_CURRENT_SESSION = 0;
    private const Int32 GRID_CS_COLUMN_SESSION_ID = 1;
    private const Int32 GRID_CS_COLUMN_NAME = 2;
    private const Int32 GRID_CS_COLUMN_STATUS = 3;
    private const Int32 GRID_CS_COLUMN_STATUS_NAME = 4;

    private const Int32 GRID_OP_MAX_COLUMNS = 8;
    private const Int32 GRID_OP_COLUMN_OPERATION_ID = 0;
    private const Int32 GRID_OP_COLUMN_DATETIME = 1;
    private const Int32 GRID_OP_COLUMN_CODE = 2;
    private const Int32 GRID_OP_COLUMN_CODE_NAME = 3;
    private const Int32 GRID_OP_COLUMN_AMOUNT = 4;
    private const Int32 GRID_OP_COLUMN_ACCOUNT_ID = 5;
    private const Int32 GRID_OP_COLUMN_ACCOUNT_NAME = 6;
    // Voucher Source: If voucher belongs to an operation or not.
    private const Int32 GRID_OP_COLUMN_VOUCHER_SOURCE = 7;
    private const Int32 GRID_OP_COLUMN_CASHIER_MOVEMENT = 8;

    #endregion // Constants

    #region Attributes

    private frm_yesno form_yes_no;
    private PrintParameters m_print_parameters;
    private Boolean m_reprint_voucher;
    private Int32 m_last_row_dgv_sessions = -1;
    private Int32 m_last_row_dgv_operations = -1;
    private CASHIER_STATUS m_cashier_status;

    #endregion // Attributes

    #region Constructor

    public frm_vouchers_reprint()
    {
      InitializeComponent();

      EventLastAction.AddEventLastAction(this.Controls);

      InitializeControlResources();

      m_reprint_voucher = GeneralParam.GetBoolean("Cashier.Voucher", "Reprint", true);
      m_print_parameters = new PrintParameters();
      m_print_parameters.m_printing = false;
      m_print_parameters.m_pause = false;
      m_print_parameters.m_cancel = false;

      tmr_status_printing.Enabled = true;

      if (BusinessLogic.IsModeTITO)
      {
        opt_mobile_bank.Visible = false;
        btn_print_all.Visible = false;
        opt_last_voucher.Location = new Point(opt_last_voucher.Location.X, opt_last_voucher.Location.Y + (opt_last_voucher.Height / 2));
        opt_cashier.Location = new Point(opt_cashier.Location.X, opt_cashier.Location.Y + (opt_cashier.Height / 2));
      }

      form_yes_no = new frm_yesno();
      form_yes_no.Opacity = 0.6;
    }

    #endregion // Constructor

    #region Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Initialize control resources (NLS strings, images, etc).
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public void InitializeControlResources()
    {
      this.FormTitle = Resource.String("STR_FRM_VOUCHERS_REPRINT_001");  // Vouchers Reprint
      opt_last_voucher.Text = Resource.String("STR_FRM_VOUCHERS_REPRINT_033");            // Opt Last Voucher

      //   - Frames
      gb_cashier_sessions.HeaderText = Resource.String("STR_FRM_VOUCHERS_REPRINT_002");         // Cashier Sessions
      gb_source.HeaderText = Resource.String("STR_FRM_VOUCHERS_REPRINT_003");                   // Source
      gb_last_operations.HeaderText = Resource.String("STR_FRM_VOUCHERS_REPRINT_004");          // Last Operations

      opt_cashier.Text = Resource.String("STR_FRM_VOUCHERS_REPRINT_005");                 // Opt Cashier

      // RCI 31-MAY-2012: Change Radio Button text if NoteAcceptor is enabled.
      if (Misc.IsNoteAcceptorEnabled())
      {
        opt_mobile_bank.Text = Resource.String("STR_FRM_VOUCHERS_REPRINT_043");           // Mobile Bank / Note Acceptor
      }
      else
      {
        opt_mobile_bank.Text = Resource.String("STR_FRM_VOUCHERS_REPRINT_006");           // Opt Mobile Bank
      }

      //   - Buttons
      btn_close.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CLOSE");               // Close
      btn_print_all.Text = Resource.String("STR_FRM_VOUCHERS_REPRINT_007");               // Print All
      btn_print_selected.Text = Resource.String("STR_FRM_VOUCHERS_REPRINT_BUTTON");      // Manual Entry

      btn_pause_continue.Text = Resource.String("STR_FRM_VOUCHERS_REPRINT_021");          // Pause

      opt_last_voucher.Checked = true;
      btn_pause_continue.Enabled = false;
      btn_print_all.Enabled = false;
      btn_print_selected.Enabled = false;
    } // InitializeControlResources

    //------------------------------------------------------------------------------
    // PURPOSE : Initialize CashierSessions Data Grid.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private void InitCashierSessionDataGrid()
    {
      // Cashier Sessions DataGrid
      dgv_cashier_sessions.RowTemplate.DividerHeight = 0;

      //    - CurrentSession
      dgv_cashier_sessions.Columns[GRID_CS_COLUMN_CURRENT_SESSION].Width = 0;
      dgv_cashier_sessions.Columns[GRID_CS_COLUMN_CURRENT_SESSION].Visible = false;

      //    - SessionId
      dgv_cashier_sessions.Columns[GRID_CS_COLUMN_SESSION_ID].Width = 0;
      dgv_cashier_sessions.Columns[GRID_CS_COLUMN_SESSION_ID].Visible = false;

      //    - SessionName
      dgv_cashier_sessions.Columns[GRID_CS_COLUMN_NAME].Width = 300;
      dgv_cashier_sessions.Columns[GRID_CS_COLUMN_NAME].HeaderCell.Value = Resource.String("STR_FRM_VOUCHERS_REPRINT_008");
      dgv_cashier_sessions.Columns[GRID_CS_COLUMN_NAME].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

      //    - Status
      dgv_cashier_sessions.Columns[GRID_CS_COLUMN_STATUS].Width = 0;
      dgv_cashier_sessions.Columns[GRID_CS_COLUMN_STATUS].Visible = false;

      //    - Status Name
      dgv_cashier_sessions.Columns[GRID_CS_COLUMN_STATUS_NAME].Width = 150;
      dgv_cashier_sessions.Columns[GRID_CS_COLUMN_STATUS_NAME].HeaderCell.Value = Resource.String("STR_FRM_VOUCHERS_REPRINT_009");
      dgv_cashier_sessions.Columns[GRID_CS_COLUMN_STATUS_NAME].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

      // Last Operations DataGrid
      dgv_last_operations.RowTemplate.DividerHeight = 0;

    } // InitCashierSessionDataGrid

    //------------------------------------------------------------------------------
    // PURPOSE : Initialize Operations Data Grid.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private void InitOperationsDataGrid()
    {
      String _date_format;
      String _time_separator;

      _date_format = System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern;
      _time_separator = System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.TimeSeparator;

      // Last Operations DataGrid
      dgv_last_operations.RowTemplate.DividerHeight = 0;

      //    - OperationId
      dgv_last_operations.Columns[GRID_OP_COLUMN_OPERATION_ID].Width = 0;
      dgv_last_operations.Columns[GRID_OP_COLUMN_OPERATION_ID].Visible = false;

      //    - DateTime
      dgv_last_operations.Columns[GRID_OP_COLUMN_DATETIME].Width = 156;
      dgv_last_operations.Columns[GRID_OP_COLUMN_DATETIME].HeaderCell.Value = Resource.String("STR_FRM_VOUCHERS_REPRINT_013");
      dgv_last_operations.Columns[GRID_OP_COLUMN_DATETIME].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
      dgv_last_operations.Columns[GRID_OP_COLUMN_DATETIME].DefaultCellStyle.Format = _date_format + " HH" + _time_separator + "mm" + _time_separator + "ss";

      //    - Code
      dgv_last_operations.Columns[GRID_OP_COLUMN_CODE].Width = 0;
      dgv_last_operations.Columns[GRID_OP_COLUMN_CODE].Visible = false;

      //    - Code Name
      dgv_last_operations.Columns[GRID_OP_COLUMN_CODE_NAME].Width = 120;
      dgv_last_operations.Columns[GRID_OP_COLUMN_CODE_NAME].HeaderCell.Value = Resource.String("STR_FRM_VOUCHERS_REPRINT_014");
      dgv_last_operations.Columns[GRID_OP_COLUMN_CODE_NAME].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

      //    - Amount
      dgv_last_operations.Columns[GRID_OP_COLUMN_AMOUNT].HeaderCell.Value = Resource.String("STR_FRM_VOUCHERS_REPRINT_015");
      dgv_last_operations.Columns[GRID_OP_COLUMN_AMOUNT].Width = 100;
      dgv_last_operations.Columns[GRID_OP_COLUMN_AMOUNT].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
      dgv_last_operations.Columns[GRID_OP_COLUMN_AMOUNT].DefaultCellStyle.Format = "C" + Math.Abs(Format.GetFormattedDecimals(CurrencyExchange.GetNationalCurrency())).ToString();

      //    - AccountId
      dgv_last_operations.Columns[GRID_OP_COLUMN_ACCOUNT_ID].Width = 0;
      dgv_last_operations.Columns[GRID_OP_COLUMN_ACCOUNT_ID].Visible = false;

      //    - Account Id+Name
      dgv_last_operations.Columns[GRID_OP_COLUMN_ACCOUNT_NAME].Width = 260;
      dgv_last_operations.Columns[GRID_OP_COLUMN_ACCOUNT_NAME].HeaderCell.Value = Resource.String("STR_FRM_VOUCHERS_REPRINT_016");
      dgv_last_operations.Columns[GRID_OP_COLUMN_ACCOUNT_NAME].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

      //    - Voucher Source: If voucher belongs to an operation or not.
      dgv_last_operations.Columns[GRID_OP_COLUMN_VOUCHER_SOURCE].Width = 0;
      dgv_last_operations.Columns[GRID_OP_COLUMN_VOUCHER_SOURCE].Visible = false;

      dgv_last_operations.Columns[GRID_OP_COLUMN_CASHIER_MOVEMENT].Width = 0;
      dgv_last_operations.Columns[GRID_OP_COLUMN_CASHIER_MOVEMENT].Visible = false;

    } // InitOperationsDataGrid

    //------------------------------------------------------------------------------
    // PURPOSE : Create a SQL Transaction for calling the overloaded version of LoadOperations.
    //
    //  PARAMS :
    //      - INPUT :
    //          - CashierSessionId
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private void LoadOperations(Int64 CashierSessionId)
    {
      SqlConnection _sql_conn;
      SqlTransaction _sql_trx;

      _sql_conn = null;
      _sql_trx = null;

      try
      {
        _sql_conn = WGDB.Connection();
        _sql_trx = _sql_conn.BeginTransaction();

        LoadOperations(CashierSessionId, _sql_trx);

        _sql_trx.Commit();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {
        if (_sql_trx != null)
        {
          if (_sql_trx.Connection != null)
          {
            try { _sql_trx.Rollback(); }
            catch { }
          }
          _sql_trx.Dispose();
          _sql_trx = null;
        }

        // Close connection
        if (_sql_conn != null)
        {
          try { _sql_conn.Close(); }
          catch { }
          _sql_conn = null;
        }
      }
    } // LoadOperations

    //------------------------------------------------------------------------------
    // PURPOSE : Load last operations from a CashierSessionId to the LastOperations DataGridView.
    //
    //  PARAMS :
    //      - INPUT :
    //          - CashierSessionId
    //          - SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private void LoadOperations(Int64 CashierSessionId, SqlTransaction SqlTrx)
    {
      string[] _message_params = { "" };
      DataTable _dt_operations;
      Int32 _idx_row;
      OperationCode _op_code;
      OperationSource _op_source;
      String _msg_error;

      web_browser.DocumentText = "";

      // RCI 04-JAN-2011: Operations are changed, refresh vouchers.
      m_last_row_dgv_operations = -1;

      if (opt_cashier.Checked)
      {
        _op_source = OperationSource.CASHIER;
      }
      else if (opt_mobile_bank.Checked)
      {
        _op_source = OperationSource.MOBILE_BANK;
      }
      else if (opt_last_voucher.Checked)
      {
        _op_source = OperationSource.LAST_VOUCHER;
      }
      else
      {
        _message_params[0] = CashierSessionId.ToString();
        frm_message.Show(Resource.String("STR_FRM_VOUCHERS_REPRINT_037", _message_params),
                         Resource.String("STR_APP_GEN_MSG_ERROR"),
                         MessageBoxButtons.OK,
                         MessageBoxIcon.Error,
                         this.ParentForm);
        return;
      }

      try
      {
        _dt_operations = PrintMobileBankTickets.LastOperationsFromCashierSessionId(CashierSessionId, _op_source, SqlTrx);

        if (_dt_operations == null)
        {
          _message_params[0] = CashierSessionId.ToString();
          if (_op_source == OperationSource.LAST_VOUCHER)
          {
            _msg_error = Resource.String("STR_FRM_VOUCHERS_REPRINT_036", _message_params);
          }
          else
          {
            _msg_error = Resource.String("STR_FRM_VOUCHERS_REPRINT_012", _message_params);
          }
          frm_message.Show(_msg_error,
                           Resource.String("STR_APP_GEN_MSG_ERROR"),
                           MessageBoxButtons.OK,
                           MessageBoxIcon.Error,
                           this.form_yes_no);
          return;
        }

        dgv_last_operations.DataSource = _dt_operations;

        InitOperationsDataGrid();

        btn_print_all.Enabled = false;
        btn_print_selected.Enabled = false;

        if (_dt_operations.Rows.Count == 0)
        {
          return;
        }

        // ICS 09-APR-2013: Must check if session is Open
        if (m_reprint_voucher && m_cashier_status == CASHIER_STATUS.OPEN)
        {
          btn_print_all.Enabled = opt_mobile_bank.Checked;
          btn_print_selected.Enabled = true;
        }

        for (_idx_row = 0; _idx_row < dgv_last_operations.Rows.Count; _idx_row++)
        {
          if ((VoucherSource)dgv_last_operations.Rows[_idx_row].Cells[GRID_OP_COLUMN_VOUCHER_SOURCE].Value == VoucherSource.FROM_VOUCHER)
          {
            dgv_last_operations.Rows[_idx_row].Cells[GRID_OP_COLUMN_CODE_NAME].Value = Resource.String("STR_FRM_VOUCHERS_REPRINT_035");
            dgv_last_operations.Rows[_idx_row].Cells[GRID_OP_COLUMN_AMOUNT].Value = DBNull.Value;
          }
          else
          {
            if ((String)dgv_last_operations.Rows[_idx_row].Cells[GRID_OP_COLUMN_ACCOUNT_NAME].Value == "")
            {
              if ((Int64)dgv_last_operations.Rows[_idx_row].Cells[GRID_OP_COLUMN_ACCOUNT_ID].Value != 0)
              {
                dgv_last_operations.Rows[_idx_row].Cells[GRID_OP_COLUMN_ACCOUNT_NAME].Value =
                    dgv_last_operations.Rows[_idx_row].Cells[GRID_OP_COLUMN_ACCOUNT_ID].Value;
              }
            }
            else
            {
              dgv_last_operations.Rows[_idx_row].Cells[GRID_OP_COLUMN_ACCOUNT_NAME].Value =
                  dgv_last_operations.Rows[_idx_row].Cells[GRID_OP_COLUMN_ACCOUNT_ID].Value +
                  "-" +
                  dgv_last_operations.Rows[_idx_row].Cells[GRID_OP_COLUMN_ACCOUNT_NAME].Value;
            }

            _op_code = (OperationCode)dgv_last_operations.Rows[_idx_row].Cells[GRID_OP_COLUMN_CODE].Value;

            // ICS 05-APR-2013 Added a new function to get the operation name
            dgv_last_operations.Rows[_idx_row].Cells[GRID_OP_COLUMN_CODE_NAME].Value = GetOperationName(_op_code);

            if (_op_code == OperationCode.DRAW_TICKET || _op_code == OperationCode.GIFT_DRAW_TICKET ||
               _op_code == OperationCode.GIFT_REDEEMABLE_AS_CASHIN || _op_code == OperationCode.GIFT_REQUEST ||
               _op_code == OperationCode.GIFT_DELIVERY || _op_code == OperationCode.CANCEL_GIFT_INSTANCE)
            {
              dgv_last_operations.Rows[_idx_row].Cells[GRID_OP_COLUMN_AMOUNT].Value = DBNull.Value;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } // LoadOperations

    //------------------------------------------------------------------------------
    // PURPOSE : Called before entering in printing mode, to disable buttons and grids.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private void PreProcessPrint()
    {
      btn_pause_continue.Text = Resource.String("STR_FRM_VOUCHERS_REPRINT_021");          // Pause
      btn_close.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");              // Cancelar

      btn_pause_continue.Enabled = true;
      btn_print_all.Enabled = false;
      btn_print_selected.Enabled = false;
      dgv_cashier_sessions.Enabled = false;
      dgv_last_operations.Enabled = false;
      opt_cashier.Enabled = false;
      opt_mobile_bank.Enabled = false;
      opt_last_voucher.Enabled = false;
    } // PreProcessPrint

    //------------------------------------------------------------------------------
    // PURPOSE : Called after printing mode, to restore buttons and grids state.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private void PostProcessPrint()
    {
      btn_pause_continue.Enabled = false;
      btn_pause_continue.Text = Resource.String("STR_FRM_VOUCHERS_REPRINT_021");          // Pause
      btn_print_all.Enabled = opt_mobile_bank.Checked;
      btn_print_selected.Enabled = true;
      btn_close.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CLOSE");               // Close

      dgv_cashier_sessions.Enabled = true;
      dgv_last_operations.Enabled = true;
      opt_cashier.Enabled = true;
      opt_mobile_bank.Enabled = true;
      opt_last_voucher.Enabled = true;
    } // PostProcessPrint

    //------------------------------------------------------------------------------
    // PURPOSE : Given an operation code returns it's name.
    //
    //  PARAMS :
    //      - INPUT :
    //              - OperationCode Code
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //              - String containing the operation name
    //
    private String GetOperationName(OperationCode Code)
    {
      switch (Code)
      {
        case OperationCode.CASH_IN:
          return Resource.String("STR_FRM_VOUCHERS_REPRINT_026");

        case OperationCode.CASH_OUT:
          if (WSI.Common.TITO.Utils.IsTitoMode())
          {
            return Resource.String("STR_FRM_VOUCHERS_REPRINT_082");
          }
          else
          {
            return Resource.String("STR_FRM_VOUCHERS_REPRINT_027");
          }

        case OperationCode.TITO_TICKET_VALIDATION:
          return Resource.String("STR_FRM_VOUCHERS_REPRINT_083");

        case OperationCode.PROMOTION:
          return Resource.String("STR_FRM_VOUCHERS_REPRINT_028");

        case OperationCode.MB_CASH_IN:
          return Resource.String("STR_FRM_VOUCHERS_REPRINT_029");

        case OperationCode.MB_PROMOTION:
          return Resource.String("STR_FRM_VOUCHERS_REPRINT_034");

        // SSC 09-MAR-2012: Added new Operation Codes for Note Acceptor
        case OperationCode.NA_CASH_IN:
          return Resource.String("STR_FRM_VOUCHERS_REPRINT_041");

        case OperationCode.NA_PROMOTION:
          return Resource.String("STR_FRM_VOUCHERS_REPRINT_042");

        case OperationCode.GIFT_REQUEST:
          return Resource.String("STR_FRM_VOUCHERS_REPRINT_038");

        case OperationCode.GIFT_DELIVERY:
          return Resource.String("STR_FRM_VOUCHERS_REPRINT_039");

        case OperationCode.DRAW_TICKET:
        case OperationCode.GIFT_DRAW_TICKET:
          return Resource.String("STR_FRM_VOUCHERS_REPRINT_040");

        case OperationCode.PROMOBOX_TOTAL_RECHARGES_PRINT:
          return Resource.String("STR_FRM_VOUCHERS_REPRINT_044");

        // ICS 05-APR-2013: Added operations that also have Vouchers
        case OperationCode.ACCOUNT_PIN_RANDOM:
          return Resource.String("STR_FRM_VOUCHERS_REPRINT_045");

        case OperationCode.HANDPAY:
          return Resource.String("STR_FRM_VOUCHERS_REPRINT_046");

        case OperationCode.HANDPAY_CANCELLATION:
          return Resource.String("STR_FRM_VOUCHERS_REPRINT_047");

        case OperationCode.CANCEL_GIFT_INSTANCE:
          return Resource.String("STR_FRM_VOUCHERS_REPRINT_048");

        // FBA 19-AUG-2013: Added control for new operation voucher
        case OperationCode.GIFT_REDEEMABLE_AS_CASHIN:
          return Resource.String("STR_FRM_VOUCHERS_REPRINT_050");

        //ACM 08-NOV-2013: Account Customize
        case OperationCode.ACCOUNT_PERSONALIZATION:
          return Resource.String("STR_VOUCHER_ACCOUNT_CUSTOMIZE_TITLE");

        // LJM 20-DEC-2013: Chips operations
        case OperationCode.CHIPS_PURCHASE:
          return Resource.String("STR_FRM_VOUCHERS_REPRINT_080");

        case OperationCode.CHIPS_SALE:
          return Resource.String("STR_FRM_VOUCHERS_REPRINT_078");

        case OperationCode.CHIPS_SALE_WITH_RECHARGE:
          return Resource.String("STR_FRM_VOUCHERS_REPRINT_079");
        // LJM

        // RMS 14-JAN-2014
        case OperationCode.TRANSFER_CREDIT_OUT:
        case OperationCode.TRANSFER_CREDIT_IN:
          return Resource.String("STR_FRM_VOUCHERS_REPRINT_081");

        // HBB 22-APR-2014
        case OperationCode.TITO_OFFLINE:
          return Resource.String("STR_FRM_VOUCHERS_REPRINT_084");

        case OperationCode.CHIPS_PURCHASE_WITH_CASH_OUT:
          return Resource.String("STR_FRM_VOUCHERS_REPRINT_085");
        // DLL 18-SEP-2014
        case OperationCode.CASH_ADVANCE:
          return Resource.String("STR_FRM_VOUCHERS_REPRINT_086");
        // HBB 19-JAN-2015
        case OperationCode.CASH_WITHDRAWAL:
          return Resource.String("STR_FRM_VOUCHERS_REPRINT_087");
        case OperationCode.CASH_DEPOSIT:
          return Resource.String("STR_FRM_VOUCHERS_REPRINT_088");
        // JPJ 28-JAN-2015
        case OperationCode.MB_DEPOSIT:
          return Resource.String("STR_FRM_VOUCHERS_REPRINT_089");
        // JPJ 30-JAN-2015
        case OperationCode.CARD_REPLACEMENT:
          return Resource.String("STR_FRM_VOUCHERS_REPRINT_091");
        // FAV 09-FEB-2016
        case OperationCode.TITO_VOID_MACHINE_TICKET:
          return Resource.String("STR_FRM_VOUCHERS_REPRINT_096");

        // FAV 10-DEC-2015
        case OperationCode.TERMINAL_REFILL_HOPPER:
          return Resource.String("STR_FRM_VOUCHERS_REPRINT_093");
        case OperationCode.TERMINAL_COLLECT_DROPBOX:
          return Resource.String("STR_FRM_VOUCHERS_REPRINT_094");
        case OperationCode.HANDPAY_VALIDATION:
          return Resource.String("STR_VOUCHER_AUTHORIZATION_001");

        //ETP 21-JAN-2015
        case OperationCode.MULTIPLE_BUCKETS_MANUAL:
          return Resource.String("STR_BUCKET_VOUCHER");

        //YNM 27-JAN-2015
        case OperationCode.CUSTOMER_ENTRANCE:
          return Resource.String("STR_MOVEMENT_TYPE_CUSTOMER_ENTRANCE");

        // RAB 24-MAR-2016: Bug 10745
        case OperationCode.CARD_ASSOCIATE:
          return Resource.String("BTN_CARD_FUNC_ASSIGN");

        // RGR 30-MAR-2016
        case OperationCode.CHIPS_SWAP:
          return Resource.String("STR_TICKET_TITO_SWAPS_CHIPS");

        case OperationCode.PROMOTION_WITH_TAXES:
          return Resource.String("STR_FRM_VOUCHERS_REPRINT_097");

        case OperationCode.PRIZE_PAYOUT:
          return Resource.String("STR_FRM_VOUCHERS_REPRINT_098");

        case OperationCode.PRIZE_PAYOUT_AND_RECHARGE:
          return Resource.String("STR_FRM_VOUCHERS_REPRINT_099");

        case OperationCode.CURRENCY_EXCHANGE_CHANGE:
          return Resource.String("STR_FRM_VOUCHERS_REPRINT_101");

        case OperationCode.CURRENCY_EXCHANGE_DEVOLUTION:
          return Resource.String("STR_FRM_VOUCHERS_REPRINT_102");

        case OperationCode.CLOSE_OPERATION:
          return Resource.String("STR_FRM_VOUCHERS_REPRINT_103");
        
        case OperationCode.REOPEN_OPERATION:
          return Resource.String("STR_FRM_VOUCHERS_REPRINT_104");

        case OperationCode.CREDIT_LINE:
          return Resource.String("STR_FRM_MOV_CREDITLINE_PAYBACK");

        default:
          // AMF 13-FEB-2015
          if (Code >= OperationCode.CAGE_CONCEPTS && Code <= OperationCode.CAGE_CONCEPTS_LAST)
          {
            Int32 _code;

            _code = (Int32)Code - (Int32)OperationCode.CAGE_CONCEPTS;

            return Cage.GetConceptDescription(_code);
          }

          return Resource.String("STR_FRM_VOUCHERS_REPRINT_030");
      }
    } // GetOperationName

    #endregion // Private Methods

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Init and Show the form.
    //
    //  PARAMS :
    //      - INPUT :
    //          - ParentForm
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //

    public void Show(Form ParentForm)
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_vouchers_reprint", Log.Type.Message);

      SqlConnection _sql_conn;
      SqlTransaction _sql_trx;
      DataTable _dt_sessions;
      Int32 _idx_row;
      CASHIER_SESSION_STATUS _status;

      _dt_sessions = null;

      _sql_conn = null;
      _sql_trx = null;

      try
      {
        _sql_conn = WGDB.Connection();
        _sql_trx = _sql_conn.BeginTransaction();
        _dt_sessions = PrintMobileBankTickets.LastSessions(_sql_trx);
        _sql_trx.Commit();

        m_cashier_status = CashierBusinessLogic.ReadCashierStatus(Cashier.SessionId);

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {
        if (_sql_trx != null)
        {
          if (_sql_trx.Connection != null)
          {
            try { _sql_trx.Rollback(); }
            catch { }
          }
          _sql_trx.Dispose();
          _sql_trx = null;
        }

        // Close connection
        if (_sql_conn != null)
        {
          try { _sql_conn.Close(); }
          catch { }
          _sql_conn = null;
        }
      }

      if (_dt_sessions == null)
      {
        frm_message.Show(Resource.String("STR_FRM_VOUCHERS_REPRINT_010"),
                         Resource.String("STR_APP_GEN_MSG_ERROR"),
                         MessageBoxButtons.OK,
                         MessageBoxIcon.Error,
                         this.ParentForm);

        return;
      }

      dgv_cashier_sessions.DataSource = _dt_sessions;

      InitCashierSessionDataGrid();

      for (_idx_row = 0; _idx_row < dgv_cashier_sessions.Rows.Count; _idx_row++)
      {
        _status = (CASHIER_SESSION_STATUS)dgv_cashier_sessions.Rows[_idx_row].Cells[GRID_CS_COLUMN_STATUS].Value;

        // QMP 02-MAY-2013: Use enum CASHIER_SESSION_STATUS
        if (_status == CASHIER_SESSION_STATUS.OPEN || _status == CASHIER_SESSION_STATUS.OPEN_PENDING)
        {
          dgv_cashier_sessions.Rows[_idx_row].Cells[GRID_CS_COLUMN_STATUS_NAME].Value = Resource.String("STR_FRM_VOUCHERS_REPRINT_024");
        }
        else if (_status == CASHIER_SESSION_STATUS.PENDING_CLOSING)
        {
          dgv_cashier_sessions.Rows[_idx_row].Cells[GRID_CS_COLUMN_STATUS_NAME].Value = Resource.String("STR_FRM_VOUCHERS_REPRINT_049");
        }
        else if (_status == CASHIER_SESSION_STATUS.CLOSED)
        {
          dgv_cashier_sessions.Rows[_idx_row].Cells[GRID_CS_COLUMN_STATUS_NAME].Value = Resource.String("STR_FRM_VOUCHERS_REPRINT_025");
        }
      }

      form_yes_no.Show();
      this.ShowDialog(form_yes_no);
      form_yes_no.Hide();

    } // Show

    //------------------------------------------------------------------------------
    // PURPOSE : Dispose yes_no form.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    new public void Dispose()
    {
      form_yes_no.Dispose();
      form_yes_no = null;

      base.Dispose();
    } // Dispose

    #endregion // Public Methods

    #region Events

    #region Buttons

    //------------------------------------------------------------------------------
    // PURPOSE : Handle the click on the Close/Cancel button.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private void btn_close_Click(object sender, EventArgs e)
    {
      // Close Button
      if (!m_print_parameters.m_printing)
      {
        Misc.WriteLog("[FORM CLOSE] frm_vouchers_reprint (not m_print_parameters.m_printing)", Log.Type.Message);
        this.Close();

        return;
      }

      // Cancel Button
      if (frm_message.Show(Resource.String("STR_FRM_VOUCHERS_REPRINT_031"),
                           Resource.String("STR_APP_GEN_MSG_WARNING"),
                           MessageBoxButtons.YesNo,
                           MessageBoxIcon.Warning,
                           this.ParentForm) == DialogResult.OK)
      {
        m_print_parameters.m_cancel = true;
      }
    } // btn_close_Click

    //------------------------------------------------------------------------------
    // PURPOSE : Handle the click on the Print All button.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private void btn_print_all_Click(object sender, EventArgs e)
    {
      DataGridViewRow _view_row;
      DataRow _row;
      Int64 _cashier_session_id;
      DataTable _vouchers;
      SqlConnection _sql_conn;
      SqlTransaction _sql_trx;
      String error_str;
      List<VoucherManager.AuditInfo> _audit_vouchers;
      VoucherManager.AuditInfo _audit_voucher;
      Int64 _current_operation;

      CashierMovementsTable _cashier_mov_table;
      Boolean _tickets_to_reprint_global;
      Boolean _tickets_to_reprint_by_operation;
      
      Voucher _temp_voucher;
      ArrayList _voucher_list;
      String _voucher_html;

      _voucher_html = String.Empty;
      _voucher_list = new ArrayList();

      _tickets_to_reprint_global = GeneralParam.GetBoolean("Cashier.Voucher", "Reprint", true);
      _tickets_to_reprint_by_operation = true;

      // Check if current user is an authorized user
      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.VoucherHistoryPrintAll,
                                               ProfilePermissions.TypeOperation.RequestPasswd,
                                               this,
                                               out error_str))
      {
        return;
      }

      // TODO: Why do we have a switch for one option here?
      switch (dgv_cashier_sessions.SelectedRows.Count)
      {
        case 1:
          break;

        case 0:
        default:
          frm_message.Show(Resource.String("STR_FRM_VOUCHERS_REPRINT_011"),
                           Resource.String("STR_APP_GEN_MSG_ERROR"),
                           MessageBoxButtons.OK,
                           MessageBoxIcon.Error,
                           this.ParentForm);
          this.Focus();

          return;
      }

      _view_row = dgv_cashier_sessions.SelectedRows[0];
      _row = ((DataRowView)_view_row.DataBoundItem).Row;
      _cashier_session_id = (Int64)_row[GRID_CS_COLUMN_SESSION_ID];

      _sql_conn = null;
      _sql_trx = null;

      try
      {
        _sql_conn = WGDB.Connection();
        _sql_trx = _sql_conn.BeginTransaction();

        // ICS 05-APR-2013: In this routine, we added 3 new columns in the _vouchers DataTable: AO_OPERATION_ID, AO_CODE and AO_ACCOUNT_ID,
        //                  needed for audit.
        _vouchers = PrintMobileBankTickets.GetVouchersFromCashierSessionId(_cashier_session_id, OperationSource.MOBILE_BANK, _sql_trx);

        // DDM 27-SEP-2012: Save movement before Reprint!
        _cashier_mov_table = new CashierMovementsTable(Cashier.CashierSessionInfo());
        _cashier_mov_table.Add(0, CASHIER_MOVEMENT.REQUEST_TO_VOUCHER_REPRINT, _vouchers.Rows.Count, 0, "");
        if (!_cashier_mov_table.Save(_sql_trx))
        {
          return;
        }

        //CashierBusinessLogic.CreateCashierMovement(_sql_trx, CASHIER_MOVEMENT.REQUEST_TO_VOUCHER_REPRINT, _vouchers.Rows.Count, Cashier.SessionId);
        _sql_trx.Commit();

        _sql_trx = null;
        _sql_trx = _sql_conn.BeginTransaction();

        if (_vouchers != null && _vouchers.Rows.Count > 0)
        {
          // ICS 05-APR-2013: Prepare audit vouchers reprint
          _audit_vouchers = new List<VoucherManager.AuditInfo>();
          _current_operation = -1;

          foreach (DataRow _voucher in _vouchers.Rows)
          {
            _tickets_to_reprint_by_operation = OperationVoucherParams.OperationVoucherDictionary.GetReprint((OperationCode)_voucher["AO_CODE"]);

            if (!_tickets_to_reprint_global || !_tickets_to_reprint_by_operation)
            {
              _voucher.Delete();
              continue;
            }

            if (_current_operation != (Int64)_voucher["AO_OPERATION_ID"])
            {
              _current_operation = (Int64)_voucher["AO_OPERATION_ID"];

              _audit_voucher = new VoucherManager.AuditInfo();
              _audit_voucher.Id = (Int64)_voucher["CV_VOUCHER_ID"];
              _audit_voucher.OperationName = GetOperationName((OperationCode)_voucher["AO_CODE"]);
              _audit_voucher.AccountId = (Int64)_voucher["AO_ACCOUNT_ID"];
              _audit_vouchers.Add(_audit_voucher);
            }
          }

          _vouchers.AcceptChanges();

          // ICS 05-APR-2013: Audit reprint vouchers
          VoucherManager.Audit(_audit_vouchers, ENUM_GUI.CASHIER, Cashier.UserId, Cashier.UserName, Cashier.TerminalName);

          foreach (DataRow _voucher in _vouchers.Rows)
          {
            _voucher_html = (String)_voucher["CV_HTML"];
            _temp_voucher = new VoucherReprint(_voucher_html, 0, PrintMode.Preview, _sql_trx);

            _voucher_list.Add(_temp_voucher);
          }

          if (!_tickets_to_reprint_global || _vouchers == null || _vouchers.Rows.Count == 0)
          {
            String _message;

            _message = Resource.String("STR_FRM_VOUCHER_REPRINT_MOBILE_BANK_OPERATION_WITHOUT_TICKETS");

            if (!_tickets_to_reprint_global)
            {
              _message = Resource.String("STR_FRM_VOUCHER_REPRINT_DISABLED_GP");
            }

            frm_message.Show(_message, Resource.String("STR_FRM_VOUCHERS_REPRINT_BUTTON"),
                           MessageBoxButtons.OK, MessageBoxIcon.Warning, this);
          }
          else
          {
            PreProcessPrint();
            VoucherPrint.Print(_voucher_list, ref m_print_parameters, this);
            PostProcessPrint();
          }
        }
        _sql_trx.Commit();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {
        if (_sql_trx != null)
        {
          if (_sql_trx.Connection != null)
          {
            try { _sql_trx.Rollback(); }
            catch { }
          }
          _sql_trx.Dispose();
          _sql_trx = null;
        }

        // Close connection
        if (_sql_conn != null)
        {
          try { _sql_conn.Close(); }
          catch { }
          _sql_conn = null;
        }
      }
    } // btn_print_all_Click

    //------------------------------------------------------------------------------
    // PURPOSE : Handle the click on the Print Selected button.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private void btn_print_selected_Click(object sender, EventArgs e)
    {
      DataGridViewRow _view_row;
      DataRow _row;
      Int64 _operation_id;
      DataTable _vouchers;
      SqlConnection _sql_conn;
      SqlTransaction _sql_trx;
      VoucherSource _voucher_source;
      String error_str;
      List<VoucherManager.AuditInfo> _audit_vouchers;
      VoucherManager.AuditInfo _audit_voucher;
      CASHIER_MOVEMENT _cashier_movement;

      CashierMovementsTable _cashier_mov_table;

      OperationCode _operation_code;
      Boolean _tickets_to_reprint_global;
      Boolean _tickets_to_reprint_by_operation;
      Boolean _no_reprint_tickets;

      Voucher _temp_voucher;
      ArrayList _voucher_list;
      String _voucher_html;

      _tickets_to_reprint_global = true;
      _tickets_to_reprint_by_operation = true;
      _no_reprint_tickets = false;
      _vouchers = null;

      _voucher_list = new ArrayList();

      // Check if current user is an authorized user
      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.VoucherHistoryPrint,
                                               ProfilePermissions.TypeOperation.RequestPasswd,
                                               this,
                                               out error_str))
      {
        return;
      }

      switch (dgv_last_operations.SelectedRows.Count)
      {
        case 1:
          break;

        case 0:
        default:
          frm_message.Show(Resource.String("STR_FRM_VOUCHERS_REPRINT_018"),
                           Resource.String("STR_APP_GEN_MSG_ERROR"),
                           MessageBoxButtons.OK,
                           MessageBoxIcon.Error,
                           this.ParentForm);
          this.Focus();

          return;
      }

      _view_row = dgv_last_operations.SelectedRows[0];
      _row = ((DataRowView)_view_row.DataBoundItem).Row;
      _operation_id = (Int64)_row[GRID_OP_COLUMN_OPERATION_ID];
      _voucher_source = (VoucherSource)_row[GRID_OP_COLUMN_VOUCHER_SOURCE];
      _cashier_movement = CASHIER_MOVEMENT.REQUEST_TO_VOUCHER_REPRINT;

      _sql_conn = null;
      _sql_trx = null;

      try
      {
        _sql_conn = WGDB.Connection();
        _sql_trx = _sql_conn.BeginTransaction();

        // Read Operation Code
        if (!Operations.DB_GetOperationCode(_operation_id, out _operation_code, _sql_trx))
        {
          _operation_code = OperationCode.NOT_SET;
        }

        // Get voucher configuration reprint
        _tickets_to_reprint_global = GeneralParam.GetBoolean("Cashier.Voucher", "Reprint", true);
        _tickets_to_reprint_by_operation = OperationVoucherParams.OperationVoucherDictionary.GetReprint(_operation_code);

        _vouchers = VoucherManager.GetVouchersFromId(_voucher_source, _operation_id, _sql_trx);

        if (!_tickets_to_reprint_by_operation || !_tickets_to_reprint_global || _vouchers == null || _vouchers.Rows.Count == 0)
        {
          _no_reprint_tickets = true;
        }
        else
        {
          // Save movement before Reprint
          _cashier_mov_table = new CashierMovementsTable(Cashier.CashierSessionInfo());
          _cashier_mov_table.Add(0, _cashier_movement, _vouchers.Rows.Count, 0, "");
          if (!_cashier_mov_table.Save(_sql_trx))
          {
            return;
          }
          _sql_trx.Commit();

          _sql_trx = null;
          _sql_trx = _sql_conn.BeginTransaction();

          if (_vouchers != null && _vouchers.Rows.Count > 0)
          {
            // ICS 05-APR-2013: Audit vouchers reprint
            _audit_vouchers = new List<VoucherManager.AuditInfo>();

            // Get only the first voucher id
            _audit_voucher = new VoucherManager.AuditInfo();
            _audit_voucher.Id = (Int64)_vouchers.Rows[0]["CV_VOUCHER_ID"];
            _audit_voucher.OperationName = (Int32)_row[GRID_OP_COLUMN_CODE] == 0 ? "" : (String)_row[GRID_OP_COLUMN_CODE_NAME];
            _audit_voucher.AccountId = (Int64)_row[GRID_OP_COLUMN_ACCOUNT_ID];
            _audit_vouchers.Add(_audit_voucher);

            VoucherManager.Audit(_audit_vouchers, ENUM_GUI.CASHIER, Cashier.UserId, Cashier.UserName, Cashier.TerminalName);

            foreach (DataRow _voucher in _vouchers.Rows)
            {
              _voucher_html = (String)_voucher["CV_HTML"];
              _temp_voucher = new VoucherReprint(_voucher_html, 0, PrintMode.Preview, _sql_trx);

              _voucher_list.Add(_temp_voucher);
            }

            PreProcessPrint();
            VoucherPrint.Print(_voucher_list, this);
            PostProcessPrint();
          }
        }
        _sql_trx.Commit();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {
        if (_sql_trx != null)
        {
          if (_sql_trx.Connection != null)
          {
            try { _sql_trx.Rollback(); }
            catch { }
          }
          _sql_trx.Dispose();
          _sql_trx = null;
        }

        // Close connection
        if (_sql_conn != null)
        {
          try { _sql_conn.Close(); }
          catch { }
          _sql_conn = null;
        }
      }

      // Show message, reprint is disabled
      if (_no_reprint_tickets)
      {
        String _message;

        _message = "";

        if (_vouchers == null || _vouchers.Rows.Count == 0)
        {
          _message = Resource.String("STR_FRM_VOUCHER_REPRINT_NO_VOUCHER_TO_PRINT");
        }
        else if (!_tickets_to_reprint_global)
        {
          _message = Resource.String("STR_FRM_VOUCHER_REPRINT_DISABLED_GP");
        }
        else
        {
          _message = Resource.String("STR_FRM_VOUCHER_REPRINT_DISABLED_BY_OPERATION");
        }

        frm_message.Show(_message, Resource.String("STR_FRM_VOUCHERS_REPRINT_BUTTON"),
                           MessageBoxButtons.OK, MessageBoxIcon.Warning, this);
      }
    } // btn_print_selected_Click

    //------------------------------------------------------------------------------
    // PURPOSE : Handle the click on the Pause/Continue button.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private void btn_pause_continue_Click(object sender, EventArgs e)
    {
      m_print_parameters.m_pause = !m_print_parameters.m_pause;

      if (m_print_parameters.m_pause)
      {
        btn_pause_continue.Text = Resource.String("STR_FRM_VOUCHERS_REPRINT_022");               // Continue
      }
      else
      {
        btn_pause_continue.Text = Resource.String("STR_FRM_VOUCHERS_REPRINT_021");               // Pause
      }
    } // btn_pause_continue_Click

    #endregion // Buttons

    #region Radio Buttons

    //------------------------------------------------------------------------------
    // PURPOSE : Handle the click on the Cashier radio button.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private void opt_cashier_CheckedChanged(object sender, EventArgs e)
    {
      if (!opt_cashier.Checked)
      {
        return;
      }

      if (dgv_cashier_sessions != null && dgv_cashier_sessions.CurrentRow != null)
      {
        LoadOperations((Int64)dgv_cashier_sessions.CurrentRow.Cells[GRID_CS_COLUMN_SESSION_ID].Value);
      }
    } // opt_cashier_CheckedChanged

    //------------------------------------------------------------------------------
    // PURPOSE : Handle the click on the Mobile Bank radio button.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private void opt_mobile_bank_CheckedChanged(object sender, EventArgs e)
    {
      if (!opt_mobile_bank.Checked)
      {
        return;
      }

      if (dgv_cashier_sessions != null && dgv_cashier_sessions.CurrentRow != null)
      {
        LoadOperations((Int64)dgv_cashier_sessions.CurrentRow.Cells[GRID_CS_COLUMN_SESSION_ID].Value);
      }
    } // opt_mobile_bank_CheckedChanged

    //------------------------------------------------------------------------------
    // PURPOSE : Handle the click on the Last Voucher radio button.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private void opt_last_voucher_CheckedChanged(object sender, EventArgs e)
    {
      Int64 _session_id;

      if (!opt_last_voucher.Checked)
      {
        return;
      }

      if (dgv_cashier_sessions != null && dgv_cashier_sessions.CurrentRow != null)
      {
        _session_id = (Int64)dgv_cashier_sessions.CurrentRow.Cells[GRID_CS_COLUMN_SESSION_ID].Value;
        LoadOperations(_session_id);
      }
    } // opt_last_voucher_CheckedChanged

    #endregion // Radio Buttons

    #region DataGridView

    //------------------------------------------------------------------------------
    // PURPOSE : Handle the RowEnter event on the Cashier DataGridView.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private void dgv_cashier_sessions_RowEnter(object sender, DataGridViewCellEventArgs e)
    {
      if (e.RowIndex != m_last_row_dgv_sessions)
      {
        LoadOperations((Int64)dgv_cashier_sessions.Rows[e.RowIndex].Cells[GRID_CS_COLUMN_SESSION_ID].Value);
        m_last_row_dgv_sessions = e.RowIndex;
      }
    } // dgv_cashier_sessions_RowEnter

    //------------------------------------------------------------------------------
    // PURPOSE : Handle the RowEnter event on the LastOperations DataGridView.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private void dgv_last_operations_RowEnter(object sender, DataGridViewCellEventArgs e)
    {
      if (e.RowIndex != m_last_row_dgv_operations)
      {
        if (!VoucherManager.LoadVouchers((VoucherSource)dgv_last_operations.Rows[e.RowIndex].Cells[GRID_OP_COLUMN_VOUCHER_SOURCE].Value,
                                         (Int64)dgv_last_operations.Rows[e.RowIndex].Cells[GRID_OP_COLUMN_OPERATION_ID].Value,
                                         true,
                                         web_browser))
        {
          frm_message.Show(Resource.String("STR_FRM_VOUCHERS_REPRINT_017"),
                           Resource.String("STR_APP_GEN_MSG_ERROR"),
                           MessageBoxButtons.OK,
                           MessageBoxIcon.Error,
                           this.ParentForm);
        }

        m_last_row_dgv_operations = e.RowIndex;
      }
    } // dgv_last_operations_RowEnter

    /// <summary>
    /// Row prepaint for points movements.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void dgv_last_operations_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
    {
      //Set points units for buckets movement types. 
      if ((OperationCode)dgv_last_operations.Rows[e.RowIndex].Cells[GRID_OP_COLUMN_CODE].Value == OperationCode.MULTIPLE_BUCKETS_MANUAL)
      {
        BucketsForm _bucketForm;
        _bucketForm = new BucketsForm();
        Buckets.BucketType _bucketType;

        _bucketForm.GetBucketType((CASHIER_MOVEMENT)dgv_last_operations.Rows[e.RowIndex].Cells[GRID_OP_COLUMN_CASHIER_MOVEMENT].Value, out _bucketType);

        if (Buckets.GetBucketUnits(_bucketType) == Buckets.BucketUnits.Points)
        {
          dgv_last_operations.Rows[e.RowIndex].Cells[GRID_OP_COLUMN_AMOUNT].Style.Format = "N0";
        }
      }


    } // dataGridView1_RowPrePaint

    #endregion // DataGridView

    #region Timer

    //------------------------------------------------------------------------------
    // PURPOSE : Handle the Tick event on the timer, to show printing message status.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private void tmr_status_printing_Tick(object sender, EventArgs e)
    {
      string[] _message_params = { "", "" };

      if (!m_print_parameters.m_printing)
      {
        lbl_print_status.Text = "";
        return;
      }

      if (m_print_parameters.m_current > 0)
      {
        _message_params[0] = m_print_parameters.m_current.ToString();
        _message_params[1] = m_print_parameters.m_total.ToString();

        if (m_print_parameters.m_pause)
        {
          // Printing paused.
          lbl_print_status.Text = Resource.String("STR_FRM_VOUCHERS_REPRINT_023", _message_params);
          return;
        }

        lbl_print_status.Text = Resource.String("STR_FRM_VOUCHERS_REPRINT_019", _message_params);
      }
      else
      {
        if (m_print_parameters.m_pause)
        {
          // Printing paused.
          lbl_print_status.Text = Resource.String("STR_FRM_VOUCHERS_REPRINT_032");
          return;
        }

        lbl_print_status.Text = Resource.String("STR_FRM_VOUCHERS_REPRINT_020");
      }
    } // tmr_status_printing_Tick

    #endregion // Timer

    #endregion // Events

  } // frm_vouchers_reprint

} // WSI.Cashier
