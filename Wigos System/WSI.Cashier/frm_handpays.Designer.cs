namespace WSI.Cashier
{
  partial class frm_handpays
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }
    
    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.gb_pending_handpays = new WSI.Cashier.Controls.uc_round_panel();
      this.dgv_pending_handpays = new WSI.Cashier.Controls.uc_DataGridView();
      this.btn_manual = new WSI.Cashier.Controls.uc_round_button();
      this.btn_pay = new WSI.Cashier.Controls.uc_round_button();
      this.gb_last_handpay = new WSI.Cashier.Controls.uc_round_panel();
      this.dgv_last_handpay = new WSI.Cashier.Controls.uc_DataGridView();
      this.btn_cancel_handpay = new WSI.Cashier.Controls.uc_round_button();
      this.btn_close = new WSI.Cashier.Controls.uc_round_button();
      this.pnl_data.SuspendLayout();
      this.gb_pending_handpays.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dgv_pending_handpays)).BeginInit();
      this.gb_last_handpay.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dgv_last_handpay)).BeginInit();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.Controls.Add(this.btn_manual);
      this.pnl_data.Controls.Add(this.btn_cancel_handpay);
      this.pnl_data.Controls.Add(this.btn_pay);
      this.pnl_data.Controls.Add(this.gb_pending_handpays);
      this.pnl_data.Controls.Add(this.gb_last_handpay);
      this.pnl_data.Controls.Add(this.btn_close);
      this.pnl_data.Size = new System.Drawing.Size(1024, 658);
      this.pnl_data.TabIndex = 0;
      // 
      // gb_pending_handpays
      // 
      this.gb_pending_handpays.BackColor = System.Drawing.Color.Transparent;
      this.gb_pending_handpays.BorderColor = System.Drawing.Color.Empty;
      this.gb_pending_handpays.Controls.Add(this.dgv_pending_handpays);
      this.gb_pending_handpays.CornerRadius = 10;
      this.gb_pending_handpays.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_pending_handpays.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gb_pending_handpays.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_pending_handpays.HeaderHeight = 35;
      this.gb_pending_handpays.HeaderSubText = null;
      this.gb_pending_handpays.HeaderText = null;
      this.gb_pending_handpays.Location = new System.Drawing.Point(42, 42);
      this.gb_pending_handpays.Name = "gb_pending_handpays";
      this.gb_pending_handpays.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_pending_handpays.Size = new System.Drawing.Size(940, 285);
      this.gb_pending_handpays.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.gb_pending_handpays.TabIndex = 0;
      this.gb_pending_handpays.Text = "xPending Handpays";
      // 
      // dgv_pending_handpays
      // 
      this.dgv_pending_handpays.AllowUserToAddRows = false;
      this.dgv_pending_handpays.AllowUserToDeleteRows = false;
      this.dgv_pending_handpays.AllowUserToResizeColumns = false;
      this.dgv_pending_handpays.AllowUserToResizeRows = false;
      this.dgv_pending_handpays.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.dgv_pending_handpays.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.dgv_pending_handpays.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.dgv_pending_handpays.ColumnHeaderHeight = 35;
      this.dgv_pending_handpays.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
      this.dgv_pending_handpays.ColumnHeadersHeight = 35;
      this.dgv_pending_handpays.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
      this.dgv_pending_handpays.CornerRadius = 10;
      this.dgv_pending_handpays.Cursor = System.Windows.Forms.Cursors.Default;
      this.dgv_pending_handpays.DefaultCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.dgv_pending_handpays.DefaultCellForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.dgv_pending_handpays.EnableHeadersVisualStyles = false;
      this.dgv_pending_handpays.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.dgv_pending_handpays.GridColor = System.Drawing.Color.LightGray;
      this.dgv_pending_handpays.HeaderBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
      this.dgv_pending_handpays.HeaderDefaultCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.dgv_pending_handpays.HeaderDefaultCellForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.dgv_pending_handpays.Location = new System.Drawing.Point(0, 35);
      this.dgv_pending_handpays.Name = "dgv_pending_handpays";
      this.dgv_pending_handpays.ReadOnly = true;
      this.dgv_pending_handpays.RowHeadersVisible = false;
      this.dgv_pending_handpays.RowHeadersWidth = 20;
      this.dgv_pending_handpays.RowTemplate.Height = 35;
      this.dgv_pending_handpays.RowTemplateHeight = 35;
      this.dgv_pending_handpays.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
      this.dgv_pending_handpays.Size = new System.Drawing.Size(941, 250);
      this.dgv_pending_handpays.Style = WSI.Cashier.Controls.uc_DataGridView.DataGridViewStyle.WITH_BOTTOM_ROUND_BORDERS;
      this.dgv_pending_handpays.TabIndex = 2;
      // 
      // btn_manual
      // 
      this.btn_manual.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_manual.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_manual.FlatAppearance.BorderSize = 0;
      this.btn_manual.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_manual.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_manual.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_manual.Image = null;
      this.btn_manual.IsSelected = false;
      this.btn_manual.Location = new System.Drawing.Point(556, 341);
      this.btn_manual.Name = "btn_manual";
      this.btn_manual.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_manual.Size = new System.Drawing.Size(205, 50);
      this.btn_manual.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_manual.TabIndex = 1;
      this.btn_manual.Text = "XMANUAL";
      this.btn_manual.UseVisualStyleBackColor = false;
      this.btn_manual.Click += new System.EventHandler(this.btn_manual_Click);
      // 
      // btn_pay
      // 
      this.btn_pay.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_pay.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_pay.FlatAppearance.BorderSize = 0;
      this.btn_pay.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_pay.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_pay.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_pay.Image = null;
      this.btn_pay.IsSelected = false;
      this.btn_pay.Location = new System.Drawing.Point(778, 341);
      this.btn_pay.Name = "btn_pay";
      this.btn_pay.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_pay.Size = new System.Drawing.Size(205, 50);
      this.btn_pay.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_pay.TabIndex = 2;
      this.btn_pay.Text = "XPAY";
      this.btn_pay.UseVisualStyleBackColor = false;
      this.btn_pay.Click += new System.EventHandler(this.btn_select_Click);
      // 
      // gb_last_handpay
      // 
      this.gb_last_handpay.BackColor = System.Drawing.Color.Transparent;
      this.gb_last_handpay.BorderColor = System.Drawing.Color.Empty;
      this.gb_last_handpay.Controls.Add(this.dgv_last_handpay);
      this.gb_last_handpay.CornerRadius = 10;
      this.gb_last_handpay.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_last_handpay.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gb_last_handpay.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_last_handpay.HeaderHeight = 35;
      this.gb_last_handpay.HeaderSubText = null;
      this.gb_last_handpay.HeaderText = null;
      this.gb_last_handpay.Location = new System.Drawing.Point(42, 420);
      this.gb_last_handpay.Name = "gb_last_handpay";
      this.gb_last_handpay.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_last_handpay.Size = new System.Drawing.Size(940, 107);
      this.gb_last_handpay.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.gb_last_handpay.TabIndex = 3;
      this.gb_last_handpay.Text = "xLast Handpay";
      // 
      // dgv_last_handpay
      // 
      this.dgv_last_handpay.AllowUserToAddRows = false;
      this.dgv_last_handpay.AllowUserToDeleteRows = false;
      this.dgv_last_handpay.AllowUserToResizeColumns = false;
      this.dgv_last_handpay.AllowUserToResizeRows = false;
      this.dgv_last_handpay.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.dgv_last_handpay.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.dgv_last_handpay.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.dgv_last_handpay.ColumnHeaderHeight = 35;
      this.dgv_last_handpay.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
      this.dgv_last_handpay.ColumnHeadersHeight = 35;
      this.dgv_last_handpay.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
      this.dgv_last_handpay.CornerRadius = 10;
      this.dgv_last_handpay.DefaultCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.dgv_last_handpay.DefaultCellForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.dgv_last_handpay.EnableHeadersVisualStyles = false;
      this.dgv_last_handpay.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.dgv_last_handpay.GridColor = System.Drawing.Color.LightGray;
      this.dgv_last_handpay.HeaderBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
      this.dgv_last_handpay.HeaderDefaultCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.dgv_last_handpay.HeaderDefaultCellForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.dgv_last_handpay.Location = new System.Drawing.Point(0, 35);
      this.dgv_last_handpay.Name = "dgv_last_handpay";
      this.dgv_last_handpay.ReadOnly = true;
      this.dgv_last_handpay.RowHeadersVisible = false;
      this.dgv_last_handpay.RowHeadersWidth = 20;
      this.dgv_last_handpay.RowTemplate.Height = 35;
      this.dgv_last_handpay.RowTemplateHeight = 35;
      this.dgv_last_handpay.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
      this.dgv_last_handpay.Size = new System.Drawing.Size(941, 72);
      this.dgv_last_handpay.Style = WSI.Cashier.Controls.uc_DataGridView.DataGridViewStyle.WITH_BOTTOM_ROUND_BORDERS;
      this.dgv_last_handpay.TabIndex = 2;
      // 
      // btn_cancel_handpay
      // 
      this.btn_cancel_handpay.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_cancel_handpay.FlatAppearance.BorderSize = 0;
      this.btn_cancel_handpay.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_cancel_handpay.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_cancel_handpay.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_cancel_handpay.Image = null;
      this.btn_cancel_handpay.IsSelected = false;
      this.btn_cancel_handpay.Location = new System.Drawing.Point(42, 545);
      this.btn_cancel_handpay.Name = "btn_cancel_handpay";
      this.btn_cancel_handpay.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_cancel_handpay.Size = new System.Drawing.Size(205, 50);
      this.btn_cancel_handpay.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_cancel_handpay.TabIndex = 4;
      this.btn_cancel_handpay.Text = "XCANCEL PAYMENT";
      this.btn_cancel_handpay.UseVisualStyleBackColor = false;
      this.btn_cancel_handpay.Click += new System.EventHandler(this.btn_cancel_handpay_Click);
      // 
      // btn_close
      // 
      this.btn_close.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_close.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_close.FlatAppearance.BorderSize = 0;
      this.btn_close.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_close.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_close.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_close.Image = null;
      this.btn_close.IsSelected = false;
      this.btn_close.Location = new System.Drawing.Point(827, 560);
      this.btn_close.Name = "btn_close";
      this.btn_close.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_close.Size = new System.Drawing.Size(155, 60);
      this.btn_close.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_close.TabIndex = 5;
      this.btn_close.Text = "XCLOSE";
      this.btn_close.UseVisualStyleBackColor = false;
      this.btn_close.Click += new System.EventHandler(this.btn_exit_Click);
      // 
      // frm_handpays
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(1024, 713);
      this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
      this.Name = "frm_handpays";
      this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
      this.Style = WSI.Cashier.Controls.frm_base.BaseFormStyle.PRINCIPAL;
      this.Text = "Hand Pays";
      this.pnl_data.ResumeLayout(false);
      this.gb_pending_handpays.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.dgv_pending_handpays)).EndInit();
      this.gb_last_handpay.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.dgv_last_handpay)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion
    
    private WSI.Cashier.Controls.uc_round_button btn_close;
    private WSI.Cashier.Controls.uc_round_panel gb_last_handpay;
    private WSI.Cashier.Controls.uc_round_panel gb_pending_handpays;
    private WSI.Cashier.Controls.uc_DataGridView dgv_pending_handpays;
    private WSI.Cashier.Controls.uc_round_button btn_manual;
    private WSI.Cashier.Controls.uc_round_button btn_pay;
    private WSI.Cashier.Controls.uc_round_button btn_cancel_handpay;
    private Controls.uc_DataGridView dgv_last_handpay;
  }
}