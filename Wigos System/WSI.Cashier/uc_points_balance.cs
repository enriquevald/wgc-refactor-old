//////using System;
//////using System.Collections.Generic;
//////using System.ComponentModel;
//////using System.Drawing;
//////using System.Data;
//////using System.Text;
//////using System.Windows.Forms;
//////using WSI.Common;
//////using System.Data.SqlClient;
//////using System.Data.SqlTypes;

//////namespace WSI.Cashier
//////{
//////  public partial class uc_points_balance : UserControl
//////  {

//////    #region Attributes

//////    CardData card;
//////    String str_card_level_01;
//////    String str_card_level_02;
//////    String str_card_level_03;
//////    String string_empty = " ";

//////    #endregion

//////    #region Public Methods

//////    public uc_points_balance()
//////    {
//////      InitializeComponent();
//////      InitializeControlResources();
//////    }

//////    /// <summary>
//////    /// Set the card and refresh data
//////    /// </summary>
//////    /// <param name="Card"></param>
//////    public void SetCard(CardData Card)
//////    {
//////      this.card = Card;
//////      RefreshData();
//////    }

//////    #endregion

//////    #region Private Methods

//////    /// <summary>
//////    /// Initialize control resources (NLS strings, images, etc)
//////    /// </summary>
//////    private void InitializeControlResources()
//////    {
//////      //   - Labels
//////      lbl_card_title.Text = Resource.String("STR_FORMAT_GENERAL_NAME_CARD") + ":";
//////      lbl_credit_title.Text = Resource.String("STR_UC_PLAYER_TRACK_NR_CREDIT") + ":";
//////      lbl_expiration_title.Text = Resource.String("STR_UC_PLAYER_TRACK_EXPIRATION") + ":";
//////      gb_points.Text = Resource.String("STR_UC_PLAYER_TRACK_POINTS");
//////      lbl_points_title.Text = Resource.String("STR_UC_PLAYER_TRACK_DIRECT_POINTS") + ":";
//////      lbl_bar_title.Text = Resource.String("STR_UC_PLAYER_TRACK_BAR") + ":";

//////      str_card_level_01 = GetGeneralParam("Level01.Name");
//////      str_card_level_02 = GetGeneralParam("Level02.Name");
//////      str_card_level_03 = GetGeneralParam("Level03.Name");

//////      // - Images:

//////    } // InitializeControlResources

//////    /// <summary>
//////    /// Get item from the General Params DB table
//////    /// </summary>
//////    /// <param name="Key"></param>
//////    private String GetGeneralParam(String Key)
//////    {
//////      SqlConnection sql_conn;
//////      SqlTransaction sql_trx;
//////      SqlCommand sql_command;
//////      String sql_str;
//////      String str_aux;
//////      String str_aux2;

//////      sql_conn = null;
//////      sql_trx = null;

//////      str_aux = "x" + Key;

//////      try
//////      {
//////        sql_conn = WGDB.Connection();
//////        sql_trx = sql_conn.BeginTransaction();
//////        sql_str = " SELECT  gp_key_value                    " +
//////                      "   FROM  general_params                  " +
//////                      "  WHERE  gp_group_key    = 'PlayerTracking'     " +
//////                      "    AND  gp_subject_key  = @p1 ";
//////        sql_command = new SqlCommand(sql_str);
//////        sql_command.Connection = sql_trx.Connection;
//////        sql_command.Transaction = sql_trx;
//////        sql_command.Parameters.Add("@p1", SqlDbType.NVarChar, 260, "gp_subject_key").Value = Key;

//////        str_aux2 = (String)sql_command.ExecuteScalar();
//////        if (str_aux2 != null)
//////        {
//////          str_aux = str_aux2;
//////        }
//////      }
//////      catch (Exception ex)
//////      {
//////        Log.Exception(ex);
//////      }
//////      finally
//////      {
//////        if (sql_trx != null)
//////        {
//////          if (sql_trx.Connection != null)
//////          {
//////            sql_trx.Rollback();
//////          }
//////          sql_trx.Dispose();
//////          sql_trx = null;
//////        }

//////        // Close connection
//////        if (sql_conn != null)
//////        {
//////          sql_conn.Close();
//////          sql_conn = null;
//////        }
//////      }

//////      return str_aux;
//////    } // LoadComboSQL

//////    /// <summary>
//////    /// Refresh data in 
//////    /// </summary>
//////    private void RefreshData()
//////    {
//////      DateTime expirtation_date = new DateTime(DateTime.Now.Year, DateTime.MaxValue.Month, DateTime.MaxValue.Day);
      
//////      lbl_bar.Text = card.PlayerTracking.CurrentPointsBar.ToString();
//////      lbl_credit.Text = card.PlayerTracking.CurrentPointsCreditNR.ToString();
//////      lbl_points.Text = card.PlayerTracking.CurrentPoints.ToString();

//////      lbl_card_type.Text = CardLevelNLS(card.PlayerTracking.CardLevel);

//////      switch (card.PlayerTracking.SubType)
//////      {
//////        // Player (CJ)
//////        case 1:
//////          {
//////            if (card.PlayerTracking.CurrentPoints == 0)
//////            {
//////              lbl_expiration.Visible = false;
//////              lbl_expiration_title.Visible = false;
//////            }
//////            else
//////            {
//////              lbl_expiration.Visible = true;
//////              lbl_expiration_title.Visible = true;
//////              lbl_expiration.Text = expirtation_date.ToShortDateString() + "\n" + Resource.String("STR_UC_PLAYER_TRACK_DAYS_REMAINING", ((Int32)365 - DateTime.Now.DayOfYear).ToString());
//////            }

//////            lbl_card_type.Visible = true;
//////            lbl_card_title.Visible = true;

//////            break;
//////          }

//////        // Service (restaurant)
//////        case 2:
//////          {
//////            lbl_expiration.Visible = false;
//////            lbl_expiration_title.Visible = false;

//////            lbl_card_type.Visible = false;
//////            lbl_card_title.Visible = false;

//////            break;
//////          }
//////        // Service (playable credits)
//////        case 3:
//////          {
//////            lbl_expiration.Visible = false;
//////            lbl_expiration_title.Visible = false;

//////            lbl_card_type.Visible = false;
//////            lbl_card_title.Visible = false;

//////            break;
//////          }

//////        // Service (expired credits)
//////        case 4:
//////          {
//////            lbl_expiration.Visible = false;
//////            lbl_expiration_title.Visible = false;

//////            lbl_card_type.Visible = false;
//////            lbl_card_title.Visible = false;

//////            break;
//////          }

//////        default:
//////          {
//////            lbl_expiration.Visible = false;
//////            lbl_expiration_title.Visible = false;

//////            lbl_card_type.Visible = false;
//////            lbl_card_title.Visible = false;

//////            break;
//////          }
//////      }

//////    }

//////    /// <summary>
//////    /// Get the card level string from the number
//////    /// </summary>
//////    /// <param name="CardLevel"></param>
//////    private string CardLevelNLS(int CardLevel)
//////    {
//////      switch (CardLevel)
//////      {
//////        case 1:
//////          return str_card_level_01;
//////        case 2:
//////          return str_card_level_02;
//////        case 3:
//////          return str_card_level_03;
//////        default:
//////          return string_empty;
//////      }
//////    }

//////    #endregion


//////  }

//////}
