using WSI.Cashier.Controls;
namespace WSI.Cashier
{
  partial class frm_card_assign
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.timer1 = new System.Windows.Forms.Timer(this.components);
      this.lbl_replacement_total_price_value = new WSI.Cashier.Controls.uc_label();
      this.lbl_replacement_total_price = new WSI.Cashier.Controls.uc_label();
      this.gp_card_box = new WSI.Cashier.Controls.uc_round_panel();
      this.lbl_replacement_commission_value = new WSI.Cashier.Controls.uc_label();
      this.lbl_replacement_commission = new WSI.Cashier.Controls.uc_label();
      this.lbl_replacement_price_value = new WSI.Cashier.Controls.uc_label();
      this.lbl_replacement_price = new WSI.Cashier.Controls.uc_label();
      this.lbl_old_track_value = new WSI.Cashier.Controls.uc_label();
      this.lbl_account_value = new WSI.Cashier.Controls.uc_label();
      this.lbl_old_track = new WSI.Cashier.Controls.uc_label();
      this.lbl_account = new WSI.Cashier.Controls.uc_label();
      this.btn_cancel = new WSI.Cashier.Controls.uc_round_button();
      this.flp_flowLayout = new System.Windows.Forms.FlowLayoutPanel();
      this.panel1 = new System.Windows.Forms.Panel();
      this.lbl_existent_card = new WSI.Cashier.Controls.uc_label();
      this.uc_card_reader = new WSI.Cashier.uc_card_reader();
      this.gp_replacement_type = new WSI.Cashier.Controls.uc_round_panel();
      this.pnl_container = new System.Windows.Forms.Panel();
      this.pnl_buttons = new System.Windows.Forms.Panel();
      this.btn_accept = new WSI.Cashier.Controls.uc_round_button();
      this.lbl_new_card_value = new WSI.Cashier.Controls.uc_label();
      this.lbl_new_card = new WSI.Cashier.Controls.uc_label();
      this.pnl_data.SuspendLayout();
      this.gp_card_box.SuspendLayout();
      this.flp_flowLayout.SuspendLayout();
      this.panel1.SuspendLayout();
      this.gp_replacement_type.SuspendLayout();
      this.pnl_buttons.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.AutoSize = true;
      this.pnl_data.Controls.Add(this.flp_flowLayout);
      this.pnl_data.Dock = System.Windows.Forms.DockStyle.None;
      this.pnl_data.Size = new System.Drawing.Size(517, 563);
      this.pnl_data.TabIndex = 0;
      // 
      // timer1
      // 
      this.timer1.Interval = 4000;
      this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
      // 
      // lbl_replacement_total_price_value
      // 
      this.lbl_replacement_total_price_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_replacement_total_price_value.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_replacement_total_price_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_replacement_total_price_value.Location = new System.Drawing.Point(137, 183);
      this.lbl_replacement_total_price_value.Name = "lbl_replacement_total_price_value";
      this.lbl_replacement_total_price_value.Size = new System.Drawing.Size(82, 16);
      this.lbl_replacement_total_price_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_replacement_total_price_value.TabIndex = 9;
      this.lbl_replacement_total_price_value.Text = "XXXXXXXXXXXXXXXXXX";
      this.lbl_replacement_total_price_value.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_replacement_total_price
      // 
      this.lbl_replacement_total_price.BackColor = System.Drawing.Color.Transparent;
      this.lbl_replacement_total_price.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_replacement_total_price.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_replacement_total_price.Location = new System.Drawing.Point(16, 183);
      this.lbl_replacement_total_price.Name = "lbl_replacement_total_price";
      this.lbl_replacement_total_price.Size = new System.Drawing.Size(115, 16);
      this.lbl_replacement_total_price.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_replacement_total_price.TabIndex = 8;
      this.lbl_replacement_total_price.Text = "xTotal";
      this.lbl_replacement_total_price.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // gp_card_box
      // 
      this.gp_card_box.BackColor = System.Drawing.Color.Transparent;
      this.gp_card_box.BorderColor = System.Drawing.Color.Empty;
      this.gp_card_box.Controls.Add(this.lbl_new_card_value);
      this.gp_card_box.Controls.Add(this.lbl_new_card);
      this.gp_card_box.Controls.Add(this.lbl_replacement_total_price_value);
      this.gp_card_box.Controls.Add(this.lbl_replacement_total_price);
      this.gp_card_box.Controls.Add(this.lbl_replacement_commission_value);
      this.gp_card_box.Controls.Add(this.lbl_replacement_commission);
      this.gp_card_box.Controls.Add(this.lbl_replacement_price_value);
      this.gp_card_box.Controls.Add(this.lbl_replacement_price);
      this.gp_card_box.Controls.Add(this.lbl_old_track_value);
      this.gp_card_box.Controls.Add(this.lbl_account_value);
      this.gp_card_box.Controls.Add(this.lbl_old_track);
      this.gp_card_box.Controls.Add(this.lbl_account);
      this.gp_card_box.CornerRadius = 10;
      this.gp_card_box.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gp_card_box.ForeColor = System.Drawing.Color.Black;
      this.gp_card_box.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gp_card_box.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gp_card_box.HeaderHeight = 35;
      this.gp_card_box.HeaderSubText = null;
      this.gp_card_box.HeaderText = null;
      this.gp_card_box.Location = new System.Drawing.Point(4, 3);
      this.gp_card_box.Name = "gp_card_box";
      this.gp_card_box.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gp_card_box.Size = new System.Drawing.Size(486, 218);
      this.gp_card_box.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.gp_card_box.TabIndex = 2;
      this.gp_card_box.Text = "xCard Data";
      // 
      // lbl_replacement_commission_value
      // 
      this.lbl_replacement_commission_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_replacement_commission_value.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_replacement_commission_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_replacement_commission_value.Location = new System.Drawing.Point(137, 156);
      this.lbl_replacement_commission_value.Name = "lbl_replacement_commission_value";
      this.lbl_replacement_commission_value.Size = new System.Drawing.Size(82, 16);
      this.lbl_replacement_commission_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_replacement_commission_value.TabIndex = 7;
      this.lbl_replacement_commission_value.Text = "XXXXXXXXXXXXXXXXXX";
      this.lbl_replacement_commission_value.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_replacement_commission
      // 
      this.lbl_replacement_commission.BackColor = System.Drawing.Color.Transparent;
      this.lbl_replacement_commission.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_replacement_commission.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_replacement_commission.Location = new System.Drawing.Point(16, 156);
      this.lbl_replacement_commission.Name = "lbl_replacement_commission";
      this.lbl_replacement_commission.Size = new System.Drawing.Size(115, 16);
      this.lbl_replacement_commission.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_replacement_commission.TabIndex = 6;
      this.lbl_replacement_commission.Text = "xCommission";
      this.lbl_replacement_commission.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_replacement_price_value
      // 
      this.lbl_replacement_price_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_replacement_price_value.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_replacement_price_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_replacement_price_value.Location = new System.Drawing.Point(137, 129);
      this.lbl_replacement_price_value.Name = "lbl_replacement_price_value";
      this.lbl_replacement_price_value.Size = new System.Drawing.Size(82, 16);
      this.lbl_replacement_price_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_replacement_price_value.TabIndex = 5;
      this.lbl_replacement_price_value.Text = "XXXXXXXXXXX";
      this.lbl_replacement_price_value.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_replacement_price
      // 
      this.lbl_replacement_price.BackColor = System.Drawing.Color.Transparent;
      this.lbl_replacement_price.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_replacement_price.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_replacement_price.Location = new System.Drawing.Point(16, 129);
      this.lbl_replacement_price.Name = "lbl_replacement_price";
      this.lbl_replacement_price.Size = new System.Drawing.Size(115, 16);
      this.lbl_replacement_price.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_replacement_price.TabIndex = 4;
      this.lbl_replacement_price.Text = "xPrice";
      this.lbl_replacement_price.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_old_track_value
      // 
      this.lbl_old_track_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_old_track_value.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_old_track_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_old_track_value.Location = new System.Drawing.Point(137, 75);
      this.lbl_old_track_value.Name = "lbl_old_track_value";
      this.lbl_old_track_value.Size = new System.Drawing.Size(192, 16);
      this.lbl_old_track_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_old_track_value.TabIndex = 3;
      this.lbl_old_track_value.Text = "XXXXXXXXXXXXXXXXXX";
      this.lbl_old_track_value.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_account_value
      // 
      this.lbl_account_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_account_value.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_account_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_account_value.Location = new System.Drawing.Point(137, 48);
      this.lbl_account_value.Name = "lbl_account_value";
      this.lbl_account_value.Size = new System.Drawing.Size(193, 16);
      this.lbl_account_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_account_value.TabIndex = 1;
      this.lbl_account_value.Text = "XXXXXXXXXXXXXXXXXX";
      this.lbl_account_value.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_old_track
      // 
      this.lbl_old_track.BackColor = System.Drawing.Color.Transparent;
      this.lbl_old_track.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_old_track.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_old_track.Location = new System.Drawing.Point(16, 75);
      this.lbl_old_track.Name = "lbl_old_track";
      this.lbl_old_track.Size = new System.Drawing.Size(115, 16);
      this.lbl_old_track.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_old_track.TabIndex = 2;
      this.lbl_old_track.Text = "xPrevious Card";
      this.lbl_old_track.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_account
      // 
      this.lbl_account.BackColor = System.Drawing.Color.Transparent;
      this.lbl_account.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_account.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_account.Location = new System.Drawing.Point(16, 48);
      this.lbl_account.Name = "lbl_account";
      this.lbl_account.Size = new System.Drawing.Size(115, 16);
      this.lbl_account.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_account.TabIndex = 0;
      this.lbl_account.Text = "xAccount";
      this.lbl_account.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // btn_cancel
      // 
      this.btn_cancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btn_cancel.FlatAppearance.BorderSize = 0;
      this.btn_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_cancel.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_cancel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_cancel.Image = null;
      this.btn_cancel.IsSelected = false;
      this.btn_cancel.Location = new System.Drawing.Point(280, 234);
      this.btn_cancel.Name = "btn_cancel";
      this.btn_cancel.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_cancel.Size = new System.Drawing.Size(191, 50);
      this.btn_cancel.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_cancel.TabIndex = 3;
      this.btn_cancel.Text = "XCANCEL";
      this.btn_cancel.UseVisualStyleBackColor = false;
      this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
      // 
      // flp_flowLayout
      // 
      this.flp_flowLayout.AutoSize = true;
      this.flp_flowLayout.Controls.Add(this.panel1);
      this.flp_flowLayout.Controls.Add(this.gp_replacement_type);
      this.flp_flowLayout.Controls.Add(this.pnl_buttons);
      this.flp_flowLayout.Location = new System.Drawing.Point(14, 6);
      this.flp_flowLayout.MaximumSize = new System.Drawing.Size(500, 750);
      this.flp_flowLayout.Name = "flp_flowLayout";
      this.flp_flowLayout.Size = new System.Drawing.Size(500, 554);
      this.flp_flowLayout.TabIndex = 4;
      // 
      // panel1
      // 
      this.panel1.Controls.Add(this.lbl_existent_card);
      this.panel1.Controls.Add(this.uc_card_reader);
      this.panel1.Location = new System.Drawing.Point(3, 3);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(505, 100);
      this.panel1.TabIndex = 4;
      // 
      // lbl_existent_card
      // 
      this.lbl_existent_card.BackColor = System.Drawing.Color.Transparent;
      this.lbl_existent_card.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_existent_card.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.lbl_existent_card.Location = new System.Drawing.Point(232, 64);
      this.lbl_existent_card.Name = "lbl_existent_card";
      this.lbl_existent_card.Size = new System.Drawing.Size(267, 18);
      this.lbl_existent_card.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_RED;
      this.lbl_existent_card.TabIndex = 3;
      this.lbl_existent_card.Text = "xCARD ID ALREADY EXISTS!";
      this.lbl_existent_card.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.lbl_existent_card.Visible = false;
      // 
      // uc_card_reader
      // 
      this.uc_card_reader.ErrorMessageBelow = false;
      this.uc_card_reader.InvalidCardTextVisible = false;
      this.uc_card_reader.Location = new System.Drawing.Point(5, 5);
      this.uc_card_reader.MaximumSize = new System.Drawing.Size(800, 90);
      this.uc_card_reader.MinimumSize = new System.Drawing.Size(400, 60);
      this.uc_card_reader.Name = "uc_card_reader";
      this.uc_card_reader.Size = new System.Drawing.Size(466, 90);
      this.uc_card_reader.TabIndex = 2;
      this.uc_card_reader.OnTrackNumberReadEvent += new WSI.Cashier.uc_card_reader.TrackNumberReadEventHandler(this.uc_card_reader_OnTrackNumberReadEvent);
      this.uc_card_reader.OnTrackNumberReadingEvent += new WSI.Cashier.uc_card_reader.TrackNumberReadingHandler(this.uc_card_reader_OnTrackNumberReadingEvent);
      // 
      // gp_replacement_type
      // 
      this.gp_replacement_type.AutoSize = true;
      this.gp_replacement_type.BackColor = System.Drawing.Color.Transparent;
      this.gp_replacement_type.BorderColor = System.Drawing.Color.Empty;
      this.gp_replacement_type.Controls.Add(this.pnl_container);
      this.gp_replacement_type.CornerRadius = 10;
      this.gp_replacement_type.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gp_replacement_type.ForeColor = System.Drawing.Color.Black;
      this.gp_replacement_type.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gp_replacement_type.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gp_replacement_type.HeaderHeight = 35;
      this.gp_replacement_type.HeaderSubText = null;
      this.gp_replacement_type.HeaderText = null;
      this.gp_replacement_type.Location = new System.Drawing.Point(3, 109);
      this.gp_replacement_type.Name = "gp_replacement_type";
      this.gp_replacement_type.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gp_replacement_type.Size = new System.Drawing.Size(493, 149);
      this.gp_replacement_type.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.gp_replacement_type.TabIndex = 0;
      this.gp_replacement_type.Text = "xPayIn";
      // 
      // pnl_container
      // 
      this.pnl_container.AutoSize = true;
      this.pnl_container.BackColor = System.Drawing.Color.Transparent;
      this.pnl_container.Location = new System.Drawing.Point(8, 45);
      this.pnl_container.Name = "pnl_container";
      this.pnl_container.Size = new System.Drawing.Size(482, 101);
      this.pnl_container.TabIndex = 0;
      // 
      // pnl_buttons
      // 
      this.pnl_buttons.AutoSize = true;
      this.pnl_buttons.Controls.Add(this.btn_accept);
      this.pnl_buttons.Controls.Add(this.btn_cancel);
      this.pnl_buttons.Controls.Add(this.gp_card_box);
      this.pnl_buttons.Location = new System.Drawing.Point(3, 264);
      this.pnl_buttons.Name = "pnl_buttons";
      this.pnl_buttons.Size = new System.Drawing.Size(493, 287);
      this.pnl_buttons.TabIndex = 5;
      // 
      // btn_accept
      // 
      this.btn_accept.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_accept.FlatAppearance.BorderSize = 0;
      this.btn_accept.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_accept.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_accept.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_accept.Image = null;
      this.btn_accept.IsSelected = false;
      this.btn_accept.Location = new System.Drawing.Point(24, 234);
      this.btn_accept.Name = "btn_accept";
      this.btn_accept.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_accept.Size = new System.Drawing.Size(191, 50);
      this.btn_accept.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_accept.TabIndex = 4;
      this.btn_accept.Text = "XACCEPT";
      this.btn_accept.UseVisualStyleBackColor = false;
      this.btn_accept.Click += new System.EventHandler(this.btn_accept_Click);
      // 
      // lbl_new_card_value
      // 
      this.lbl_new_card_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_new_card_value.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_new_card_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_new_card_value.Location = new System.Drawing.Point(137, 102);
      this.lbl_new_card_value.Name = "lbl_new_card_value";
      this.lbl_new_card_value.Size = new System.Drawing.Size(192, 15);
      this.lbl_new_card_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_new_card_value.TabIndex = 11;
      this.lbl_new_card_value.Text = "XXXXXXXXXXXXXXXXXX";
      this.lbl_new_card_value.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_new_card
      // 
      this.lbl_new_card.BackColor = System.Drawing.Color.Transparent;
      this.lbl_new_card.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_new_card.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_new_card.Location = new System.Drawing.Point(16, 102);
      this.lbl_new_card.Name = "lbl_new_card";
      this.lbl_new_card.Size = new System.Drawing.Size(115, 16);
      this.lbl_new_card.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_new_card.TabIndex = 10;
      this.lbl_new_card.Text = "xNew Card";
      this.lbl_new_card.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // frm_card_assign
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(528, 621);
      this.ControlBox = false;
      this.Name = "frm_card_assign";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Style = WSI.Cashier.Controls.frm_base.BaseFormStyle.PRINCIPAL;
      this.Text = "Card Assign";
      this.Shown += new System.EventHandler(this.frm_card_assign_Shown);
      this.pnl_data.ResumeLayout(false);
      this.pnl_data.PerformLayout();
      this.gp_card_box.ResumeLayout(false);
      this.flp_flowLayout.ResumeLayout(false);
      this.flp_flowLayout.PerformLayout();
      this.panel1.ResumeLayout(false);
      this.gp_replacement_type.ResumeLayout(false);
      this.gp_replacement_type.PerformLayout();
      this.pnl_buttons.ResumeLayout(false);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Timer timer1;
    private uc_label lbl_replacement_total_price_value;
    private uc_label lbl_replacement_total_price;
    private uc_round_panel gp_card_box;
    private uc_label lbl_replacement_commission_value;
    private uc_label lbl_replacement_commission;
    private uc_label lbl_replacement_price_value;
    private uc_label lbl_replacement_price;
    private uc_label lbl_old_track_value;
    private uc_label lbl_account_value;
    private uc_label lbl_old_track;
    private uc_label lbl_account;
    private uc_round_button btn_cancel;
    private System.Windows.Forms.FlowLayoutPanel flp_flowLayout;
    private System.Windows.Forms.Panel pnl_buttons;
    private System.Windows.Forms.Panel panel1;
    private uc_label lbl_existent_card;
    private uc_card_reader uc_card_reader;
    private uc_round_panel gp_replacement_type;
    private System.Windows.Forms.Panel pnl_container;
    private uc_round_button btn_accept;
    private uc_label lbl_new_card_value;
    private uc_label lbl_new_card;

  }
}