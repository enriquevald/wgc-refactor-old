namespace WSI.Cashier
{
  partial class frm_mb_account_limits_edit
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_mb_account_limits_edit));
      this.chk_number_of_recharges = new WSI.Cashier.Controls.uc_checkBox();
      this.chk_recharge_limit = new WSI.Cashier.Controls.uc_checkBox();
      this.chk_total_limit = new WSI.Cashier.Controls.uc_checkBox();
      this.txt_number_of_recharges = new WSI.Cashier.Controls.uc_round_textbox();
      this.txt_recharge_limit = new WSI.Cashier.Controls.uc_round_textbox();
      this.txt_total_limit = new WSI.Cashier.Controls.uc_round_textbox();
      this.lbl_msg_blink = new WSI.Cashier.Controls.uc_label();
      this.btn_keyboard = new WSI.Cashier.Controls.uc_round_button();
      this.btn_cancel = new WSI.Cashier.Controls.uc_round_button();
      this.btn_ok = new WSI.Cashier.Controls.uc_round_button();
      this.timer1 = new System.Windows.Forms.Timer(this.components);
      this.pnl_data.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.Controls.Add(this.lbl_msg_blink);
      this.pnl_data.Controls.Add(this.btn_keyboard);
      this.pnl_data.Controls.Add(this.btn_cancel);
      this.pnl_data.Controls.Add(this.btn_ok);
      this.pnl_data.Controls.Add(this.chk_number_of_recharges);
      this.pnl_data.Controls.Add(this.chk_recharge_limit);
      this.pnl_data.Controls.Add(this.chk_total_limit);
      this.pnl_data.Controls.Add(this.txt_number_of_recharges);
      this.pnl_data.Controls.Add(this.txt_recharge_limit);
      this.pnl_data.Controls.Add(this.txt_total_limit);
      this.pnl_data.Size = new System.Drawing.Size(449, 280);
      // 
      // chk_number_of_recharges
      // 
      this.chk_number_of_recharges.BackColor = System.Drawing.Color.Transparent;
      this.chk_number_of_recharges.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.chk_number_of_recharges.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.chk_number_of_recharges.Location = new System.Drawing.Point(16, 118);
      this.chk_number_of_recharges.MinimumSize = new System.Drawing.Size(212, 30);
      this.chk_number_of_recharges.Name = "chk_number_of_recharges";
      this.chk_number_of_recharges.Padding = new System.Windows.Forms.Padding(5);
      this.chk_number_of_recharges.Size = new System.Drawing.Size(212, 30);
      this.chk_number_of_recharges.TabIndex = 4;
      this.chk_number_of_recharges.Text = "xNumber of recharges";
      this.chk_number_of_recharges.UseVisualStyleBackColor = true;
      this.chk_number_of_recharges.CheckedChanged += new System.EventHandler(this.chk_number_of_recharges_CheckedChanged);
      // 
      // chk_recharge_limit
      // 
      this.chk_recharge_limit.BackColor = System.Drawing.Color.Transparent;
      this.chk_recharge_limit.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.chk_recharge_limit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.chk_recharge_limit.Location = new System.Drawing.Point(16, 70);
      this.chk_recharge_limit.MinimumSize = new System.Drawing.Size(212, 30);
      this.chk_recharge_limit.Name = "chk_recharge_limit";
      this.chk_recharge_limit.Padding = new System.Windows.Forms.Padding(5);
      this.chk_recharge_limit.Size = new System.Drawing.Size(212, 30);
      this.chk_recharge_limit.TabIndex = 2;
      this.chk_recharge_limit.Text = "xRecharge Limit";
      this.chk_recharge_limit.UseVisualStyleBackColor = true;
      this.chk_recharge_limit.CheckedChanged += new System.EventHandler(this.chk_recharge_limit_CheckedChanged);
      // 
      // chk_total_limit
      // 
      this.chk_total_limit.BackColor = System.Drawing.Color.Transparent;
      this.chk_total_limit.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.chk_total_limit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.chk_total_limit.Location = new System.Drawing.Point(16, 23);
      this.chk_total_limit.MinimumSize = new System.Drawing.Size(212, 30);
      this.chk_total_limit.Name = "chk_total_limit";
      this.chk_total_limit.Padding = new System.Windows.Forms.Padding(5);
      this.chk_total_limit.Size = new System.Drawing.Size(212, 30);
      this.chk_total_limit.TabIndex = 0;
      this.chk_total_limit.Text = "xTotal limit";
      this.chk_total_limit.UseVisualStyleBackColor = true;
      this.chk_total_limit.CheckedChanged += new System.EventHandler(this.chk_total_limit_CheckedChanged);
      // 
      // txt_number_of_recharges
      // 
      this.txt_number_of_recharges.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_number_of_recharges.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_number_of_recharges.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_number_of_recharges.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_number_of_recharges.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_number_of_recharges.CornerRadius = 5;
      this.txt_number_of_recharges.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_number_of_recharges.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.txt_number_of_recharges.Location = new System.Drawing.Point(235, 113);
      this.txt_number_of_recharges.MaxLength = 9;
      this.txt_number_of_recharges.Multiline = false;
      this.txt_number_of_recharges.Name = "txt_number_of_recharges";
      this.txt_number_of_recharges.PasswordChar = '\0';
      this.txt_number_of_recharges.ReadOnly = false;
      this.txt_number_of_recharges.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_number_of_recharges.SelectedText = "";
      this.txt_number_of_recharges.SelectionLength = 0;
      this.txt_number_of_recharges.SelectionStart = 0;
      this.txt_number_of_recharges.Size = new System.Drawing.Size(197, 40);
      this.txt_number_of_recharges.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_number_of_recharges.TabIndex = 5;
      this.txt_number_of_recharges.Text = "123";
      this.txt_number_of_recharges.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      this.txt_number_of_recharges.UseSystemPasswordChar = false;
      this.txt_number_of_recharges.WaterMark = null;
      this.txt_number_of_recharges.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_number_KeyPress);
      this.txt_number_of_recharges.Enter += new System.EventHandler(this.txt_number_Click);
      this.txt_number_of_recharges.Validating += new System.ComponentModel.CancelEventHandler(this.txt_number_of_recharges_Validating);
      // 
      // txt_recharge_limit
      // 
      this.txt_recharge_limit.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_recharge_limit.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_recharge_limit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_recharge_limit.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_recharge_limit.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_recharge_limit.CornerRadius = 5;
      this.txt_recharge_limit.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_recharge_limit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.txt_recharge_limit.Location = new System.Drawing.Point(235, 65);
      this.txt_recharge_limit.MaxLength = 28;
      this.txt_recharge_limit.Multiline = false;
      this.txt_recharge_limit.Name = "txt_recharge_limit";
      this.txt_recharge_limit.PasswordChar = '\0';
      this.txt_recharge_limit.ReadOnly = false;
      this.txt_recharge_limit.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_recharge_limit.SelectedText = "";
      this.txt_recharge_limit.SelectionLength = 0;
      this.txt_recharge_limit.SelectionStart = 0;
      this.txt_recharge_limit.Size = new System.Drawing.Size(197, 40);
      this.txt_recharge_limit.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_recharge_limit.TabIndex = 3;
      this.txt_recharge_limit.Text = "$123.45";
      this.txt_recharge_limit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      this.txt_recharge_limit.UseSystemPasswordChar = false;
      this.txt_recharge_limit.WaterMark = null;
      this.txt_recharge_limit.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_number_KeyPress);
      this.txt_recharge_limit.Enter += new System.EventHandler(this.txt_number_Click);
      this.txt_recharge_limit.Click += new System.EventHandler(this.txt_number_Click);
      this.txt_recharge_limit.Validating += new System.ComponentModel.CancelEventHandler(this.txt_recharge_limit_Validating);
      // 
      // txt_total_limit
      // 
      this.txt_total_limit.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_total_limit.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_total_limit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_total_limit.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_total_limit.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_total_limit.CornerRadius = 5;
      this.txt_total_limit.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_total_limit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.txt_total_limit.Location = new System.Drawing.Point(235, 18);
      this.txt_total_limit.MaxLength = 28;
      this.txt_total_limit.Multiline = false;
      this.txt_total_limit.Name = "txt_total_limit";
      this.txt_total_limit.PasswordChar = '\0';
      this.txt_total_limit.ReadOnly = false;
      this.txt_total_limit.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_total_limit.SelectedText = "";
      this.txt_total_limit.SelectionLength = 0;
      this.txt_total_limit.SelectionStart = 0;
      this.txt_total_limit.Size = new System.Drawing.Size(197, 40);
      this.txt_total_limit.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_total_limit.TabIndex = 1;
      this.txt_total_limit.Text = "$12,345.67";
      this.txt_total_limit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      this.txt_total_limit.UseSystemPasswordChar = false;
      this.txt_total_limit.WaterMark = null;
      this.txt_total_limit.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_number_KeyPress);
      this.txt_total_limit.Enter += new System.EventHandler(this.txt_number_Click);
      this.txt_total_limit.Click += new System.EventHandler(this.txt_number_Click);
      this.txt_total_limit.Validating += new System.ComponentModel.CancelEventHandler(this.txt_total_limit_Validating);
      // 
      // lbl_msg_blink
      // 
      this.lbl_msg_blink.BackColor = System.Drawing.Color.Transparent;
      this.lbl_msg_blink.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_msg_blink.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.lbl_msg_blink.Location = new System.Drawing.Point(12, 159);
      this.lbl_msg_blink.Name = "lbl_msg_blink";
      this.lbl_msg_blink.Size = new System.Drawing.Size(420, 38);
      this.lbl_msg_blink.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_RED;
      this.lbl_msg_blink.TabIndex = 6;
      this.lbl_msg_blink.Text = "XPERSONAL LIMITS CANNOT BE BLANK                                                 " +
    "      OR ONE GREATER THAN OTHER";
      this.lbl_msg_blink.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.lbl_msg_blink.Visible = false;
      // 
      // btn_keyboard
      // 
      this.btn_keyboard.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(47)))), ((int)(((byte)(50)))));
      this.btn_keyboard.FlatAppearance.BorderSize = 0;
      this.btn_keyboard.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_keyboard.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_keyboard.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_keyboard.Image = ((System.Drawing.Image)(resources.GetObject("btn_keyboard.Image")));
      this.btn_keyboard.Location = new System.Drawing.Point(15, 205);
      this.btn_keyboard.Name = "btn_keyboard";
      this.btn_keyboard.Size = new System.Drawing.Size(70, 60);
      this.btn_keyboard.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.TERTIARY;
      this.btn_keyboard.TabIndex = 7;
      this.btn_keyboard.UseVisualStyleBackColor = false;
      this.btn_keyboard.Click += new System.EventHandler(this.btn_keyboard_Click);
      // 
      // btn_cancel
      // 
      this.btn_cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_cancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_cancel.FlatAppearance.BorderSize = 0;
      this.btn_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_cancel.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_cancel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_cancel.Image = null;
      this.btn_cancel.Location = new System.Drawing.Point(108, 205);
      this.btn_cancel.Name = "btn_cancel";
      this.btn_cancel.Size = new System.Drawing.Size(155, 60);
      this.btn_cancel.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_cancel.TabIndex = 8;
      this.btn_cancel.Text = "XCANCEL";
      this.btn_cancel.UseVisualStyleBackColor = false;
      this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
      // 
      // btn_ok
      // 
      this.btn_ok.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_ok.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_ok.FlatAppearance.BorderSize = 0;
      this.btn_ok.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_ok.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_ok.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_ok.Image = null;
      this.btn_ok.Location = new System.Drawing.Point(277, 205);
      this.btn_ok.Name = "btn_ok";
      this.btn_ok.Size = new System.Drawing.Size(155, 60);
      this.btn_ok.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_ok.TabIndex = 9;
      this.btn_ok.Text = "XOK";
      this.btn_ok.UseVisualStyleBackColor = false;
      this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
      // 
      // timer1
      // 
      this.timer1.Interval = 5000;
      this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
      // 
      // frm_mb_account_limits_edit
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.ClientSize = new System.Drawing.Size(449, 335);
      this.ControlBox = false;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "frm_mb_account_limits_edit";
      this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
      this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
      this.Style = WSI.Cashier.Controls.frm_base.BaseFormStyle.PRINCIPAL;
      this.Text = "frm_mb_account_limits_edit";
      this.pnl_data.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

   
    private WSI.Cashier.Controls.uc_round_textbox txt_number_of_recharges;
    private WSI.Cashier.Controls.uc_round_textbox txt_recharge_limit;
    private WSI.Cashier.Controls.uc_round_textbox txt_total_limit;
    private WSI.Cashier.Controls.uc_label lbl_msg_blink;
    private WSI.Cashier.Controls.uc_round_button btn_keyboard;
    private WSI.Cashier.Controls.uc_round_button btn_cancel;
    private WSI.Cashier.Controls.uc_round_button btn_ok;
    internal System.Windows.Forms.Timer timer1;
    private WSI.Cashier.Controls.uc_checkBox chk_total_limit;
    private WSI.Cashier.Controls.uc_checkBox chk_number_of_recharges;
    private WSI.Cashier.Controls.uc_checkBox chk_recharge_limit;

  }
}