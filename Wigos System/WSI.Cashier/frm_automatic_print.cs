//------------------------------------------------------------------------------
// Copyright � 2011 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_automatic_print.cs
// 
//   DESCRIPTION: Implements the automatic print dialog. 
//
//        AUTHOR: Alberto Cuesta
// 
// CREATION DATE: 04-OCT-2011
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 04-OCT-2011 ACC    First release.
// 09-MAR-2012 SSC    Added new Operation Codes for Note Acceptor
// 28-MAR-2012 MPO    User session: Set the last action for the user
// 04-APR-2012 JAB    Changes to move operations to obtain and view tickets to the project WSI.Common
// 26-AUG-2015 DHA    Added validation on print by voucher configuration
// 08-NOV-2015 JMV    Product Backlog Item 5893:Dise�o cajero: Aplicar en frm_player_edit.cs
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Windows.Forms;
using WSI.Common;
using System.Threading;
using System.Drawing;
using WSI.Cashier.Controls;

namespace WSI.Cashier
{
  public partial class frm_automatic_print : frm_base
  {
    #region Constants

    private const Int32 PRINTER_MAX_QUEUE_ITEMS = 4;

    #endregion

    #region Members

    private frm_yesno form_yes_no;
    private Boolean m_pending_close;
    private Boolean m_close_allowed;
    private String m_text_progress;
    private Thread m_thread;

    private DateTime m_started_time;

    #endregion

    #region Constructor

    /// <summary>
    /// Constructor method.
    /// 1. Initialize controls.
    /// 2. Initialize control�s resources. (NLS, Images, etc)
    /// </summary>
    public frm_automatic_print()
    {
      InitializeComponent();
      InitializeControlResources();
      form_yes_no = new frm_yesno();
      form_yes_no.Opacity = 0.6;
    }

    #endregion

    #region Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Init resources
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void InitializeControlResources()
    {
      // NLS Strings
      this.FormTitle = "";
      btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");

    } // InitializeControlResources

    //------------------------------------------------------------------------------
    // PURPOSE : Init 
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void Init()
    {
      this.FormTitle = Resource.String("STR_FRM_AUTOMATIC_PRINT_TITLE");
      lbl_text.Text = Resource.String("STR_FRM_AUTOMATIC_PRINT_CHECKING");
      m_text_progress = Resource.String("STR_FRM_AUTOMATIC_PRINT_CHECKING");
      btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");
      m_pending_close = false;
      m_close_allowed = false;
      lbl_text_closing.Text = " ";
      tm_close.Enabled = true;

      if (m_thread == null)
      {
        m_thread = new Thread(this.AutomaticPrintThread);
        m_thread.Name = "AutomaticPrintThread";
        m_thread.SetApartmentState(ApartmentState.STA); 
        m_thread.Start();
      }

      this.Location = new Point(1024 / 2 - this.Width / 2, 768 / 2 - this.Height / 2);

    } // Init

    //------------------------------------------------------------------------------
    // PURPOSE : Init resources
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void AutomaticPrintThread()
    {
      DataTable _dt_operation;
      Random _random;
      Int32 _wait_hint;

      _dt_operation = new DataTable("OPERATIONS");
      _random = new Random(Misc.GetTickCount());
      _wait_hint = 0;

      m_started_time = WGDB.Now;

      // Mark as printed the previous pending vouchers.
      MarkAsPrintedBeforeDate(m_started_time.AddHours(-1));

      while (true)
      {
        try
        {
          if (_wait_hint > 0)
          {
            _wait_hint += _random.Next(1000);
          }
          Thread.Sleep(_wait_hint);
          _wait_hint = 5000;

          WSI.Common.Users.SetLastAction("Open", Resource.String("STR_FRM_AUTOMATIC_PRINT_TITLE"));

          // Check if have to exit...
          if (m_pending_close)
          {
            WaitForWebBrowserObject();
            m_close_allowed = true;

            return;
          }

          //
          // Check Printer Queue
          //
          if (!PrinterAvailable())
          {
            continue;
          }

          m_text_progress = Resource.String("STR_FRM_AUTOMATIC_PRINT_CHECKING");

          //
          // Check new operations
          //
          if (!GetNewOperations(out _dt_operation))
          {
            // No log, the exception is logged inside.

            continue;
          }

          if (_dt_operation.Rows.Count == 0)
          {
            m_text_progress = Resource.String("STR_FRM_AUTOMATIC_PRINT_CHECKING");
            _wait_hint = 2000;

            continue;
          }

          _wait_hint = 0;

          //
          // Print new operations
          //
          //    1.- Update operation
          //    2.- Print Vouchers
          //    3.- Commit or Rollback
          //
          while (_dt_operation.Rows.Count > 0)
          {
            Int64 _operation_id;
            Boolean _row_updated;

            // Check if have to exit...
            if (m_pending_close)
            {
              WaitForWebBrowserObject();
              m_close_allowed = true;

              return;
            }

            // Check Printer Queue
            if (!PrinterAvailable())
            {
              break;
            }

            // Get the first operation id...
            _operation_id = (Int64)_dt_operation.Rows[0][0];
            // ...  and delete it from the collection.
            _dt_operation.Rows[0].Delete();
            _dt_operation.AcceptChanges();

            m_text_progress = Resource.String("STR_FRM_AUTOMATIC_PRINT_PRINTING");

            using (DB_TRX _db_trx = new DB_TRX())
            {
              try
              {
                //    1.- Update operation
                if (UpdateOperation(_operation_id, _db_trx.SqlTransaction, out _row_updated))
                {
                  // If the row has been really updated, it means the vouchers have to be printed.
                  // It's possible another Cashier has printed this operation id, so it's necessary to check
                  // if the row (for this operation id) is really updated.
                  if (_row_updated)
                  {
                    //    2.- Print Vouchers
                    if (PrintVouchersOperation(_operation_id, _db_trx.SqlTransaction))
                    {
                      //    3.- Commit 
                      _db_trx.Commit();
                    }
                  }
                }
              }
              catch (Exception _ex)
              {
                Log.Exception(_ex);

                break;
              }
            } // using
          } // while

        } // try
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        } 

      } // while

    } // AutomaticPrintThread

    //------------------------------------------------------------------------------
    // PURPOSE : Get New Operation
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //          - OperationId 
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private Boolean GetNewOperations(out DataTable DtOperations)
    {
      StringBuilder _sql_txt;
      SqlCommand _sql_cmd;
      SqlDataAdapter _sql_adap;

      DtOperations = new DataTable("OPERATIONS");

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _sql_cmd = new SqlCommand();
          _sql_cmd.Connection = _db_trx.SqlTransaction.Connection;
          _sql_cmd.Transaction = _db_trx.SqlTransaction;

          _sql_txt = new StringBuilder();
          _sql_txt.AppendLine(" SELECT   TOP 5 ");
          _sql_txt.AppendLine("          AO_OPERATION_ID ");
          _sql_txt.AppendLine("   FROM   ACCOUNT_OPERATIONS ");
          _sql_txt.AppendLine("  WHERE   AO_CODE IN ( @pCodeCs01, @pCodeCs02, @pCodeCs03, @pCodeCs04) ");
          _sql_txt.AppendLine("    AND   AO_AUTOMATICALLY_PRINTED  = 0 ");
          _sql_txt.AppendLine("    AND   AO_DATETIME              >= @pStartedTime ");
          _sql_txt.AppendLine("  ORDER   BY AO_DATETIME ");

          _sql_cmd.CommandText = _sql_txt.ToString();

          _sql_cmd.Parameters.Add("@pCodeCs01", SqlDbType.Int).Value = (Int32)OperationCode.MB_CASH_IN;
          _sql_cmd.Parameters.Add("@pCodeCs02", SqlDbType.Int).Value = (Int32)OperationCode.MB_PROMOTION;
          //SSC 09-MAR-2012: Added new Operation Codes for Note Acceptor
          _sql_cmd.Parameters.Add("@pCodeCs03", SqlDbType.Int).Value = (Int32)OperationCode.NA_CASH_IN;
          _sql_cmd.Parameters.Add("@pCodeCs04", SqlDbType.Int).Value = (Int32)OperationCode.NA_PROMOTION;
          // RCI 13-OCT-2011: Print operation vouchers starting from now.
          _sql_cmd.Parameters.Add("@pStartedTime", SqlDbType.DateTime).Value = m_started_time;

          _sql_adap = new SqlDataAdapter();
          _sql_adap.SelectCommand = _sql_cmd;
          _sql_adap.InsertCommand = null;
          _sql_adap.UpdateCommand = null;
          _sql_adap.DeleteCommand = null;

          _sql_adap.Fill(DtOperations);

          return true;
        } // using
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
    } // GetNewOperations

    //------------------------------------------------------------------------------
    // PURPOSE : Update Operation
    //
    //  PARAMS :
    //      - INPUT :
    //          - OperationId 
    //          - SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private Boolean UpdateOperation(Int64 OperationId, SqlTransaction SqlTrx, out Boolean RowUpdated)
    {
      StringBuilder _sql_txt;
      SqlCommand _sql_cmd;
      Int32 _num_rows_updated;

      RowUpdated = false;

      try
      {
        _sql_txt = new StringBuilder();
        _sql_txt.AppendLine(" UPDATE   ACCOUNT_OPERATIONS ");
        _sql_txt.AppendLine("    SET   AO_AUTOMATICALLY_PRINTED = 1 ");
        _sql_txt.AppendLine("   FROM   ACCOUNT_OPERATIONS ");
        _sql_txt.AppendLine("  WHERE   AO_OPERATION_ID = @pOperationId ");
        _sql_txt.AppendLine("    AND   AO_AUTOMATICALLY_PRINTED = 0 ");

        _sql_cmd = new SqlCommand(_sql_txt.ToString());
        // Timeout = 1 sec, so if there is another Cashier printing this operation id, don't block more than 1 second.
        // This way, while one Cashier is printing, this one continue with the next operation id.
        _sql_cmd.CommandTimeout = 1;
        _sql_cmd.Connection = SqlTrx.Connection;
        _sql_cmd.Transaction = SqlTrx;

        _sql_cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt).Value = OperationId;
        _num_rows_updated = _sql_cmd.ExecuteNonQuery();

        RowUpdated = (_num_rows_updated == 1);

        return true;
      }
      catch (SqlException _sql_ex)
      {
        if (_sql_ex.Number == -2) // command timeout
        {
          return true;
        }
        else
        {
          Log.Warning(" *** Sql Exception: Number: " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // UpdateOperation

    //------------------------------------------------------------------------------
    // PURPOSE : Print Vouchers Operation
    //
    //  PARAMS :
    //      - INPUT :
    //          - OperationId 
    //          - SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private Boolean PrintVouchersOperation(Int64 OperationId, SqlTransaction SqlTrx)
    {
      DataTable _dt_vouchers;
      PrintParameters _print_parameters;
      Int32 _num_copies;
      OperationCode _operation_code;

      _print_parameters = new PrintParameters();
      _print_parameters.m_printing = false;
      _print_parameters.m_pause = false;
      _print_parameters.m_cancel = false;

      try
      {
        _dt_vouchers = VoucherManager.GetVouchersFromId(VoucherSource.FROM_OPERATION, OperationId, SqlTrx);
        
        // Read Operation Code
        if (!Operations.DB_GetOperationCode(OperationId, out _operation_code, SqlTrx))
        {
          _operation_code = OperationCode.NOT_SET;
        }

        if (_dt_vouchers != null && _dt_vouchers.Rows.Count > 0)
        {
          _num_copies = OperationVoucherParams.OperationVoucherDictionary.GetPrintCopy(_operation_code);
          
          for (Int32 _idx_copy = 0; _idx_copy < _num_copies; _idx_copy++)
          {
            VoucherManager.Print(_dt_vouchers, 0, null, ref _print_parameters, SqlTrx, _idx_copy > 0);
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }

      return true;

    } // PrintVouchersOperation

    //------------------------------------------------------------------------------
    // PURPOSE : Mark as printed the vouchers of the operations that are older than Date.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Date 
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void MarkAsPrintedBeforeDate(DateTime Date)
    {
      StringBuilder _sql_txt;
      SqlCommand _sql_cmd;
      Int32 _num_rows_updated;

      _num_rows_updated = 0;

      try
      {
        while (true)
        {
          using (DB_TRX _db_trx = new DB_TRX())
          {
            _sql_txt = new StringBuilder();
            _sql_txt.AppendLine(" UPDATE   TOP (500) ");
            _sql_txt.AppendLine("          ACCOUNT_OPERATIONS ");
            _sql_txt.AppendLine("    SET   AO_AUTOMATICALLY_PRINTED = 1 ");
            _sql_txt.AppendLine("   FROM   ACCOUNT_OPERATIONS ");
            _sql_txt.AppendLine("  WHERE   AO_CODE IN ( @pCodeCs01, @pCodeCs02, @pCodeCs03, @pCodeCs04) ");
            _sql_txt.AppendLine("    AND   AO_DATETIME              < @pDateTime ");
            _sql_txt.AppendLine("    AND   AO_AUTOMATICALLY_PRINTED = 0 ");

            _sql_cmd = new SqlCommand(_sql_txt.ToString());
            _sql_cmd.Connection = _db_trx.SqlTransaction.Connection;
            _sql_cmd.Transaction = _db_trx.SqlTransaction;

            _sql_cmd.Parameters.Add("@pCodeCs01", SqlDbType.Int).Value = (Int32)OperationCode.MB_CASH_IN;
            _sql_cmd.Parameters.Add("@pCodeCs02", SqlDbType.Int).Value = (Int32)OperationCode.MB_PROMOTION;
            _sql_cmd.Parameters.Add("@pCodeCs03", SqlDbType.Int).Value = (Int32)OperationCode.NA_CASH_IN;
            _sql_cmd.Parameters.Add("@pCodeCs04", SqlDbType.Int).Value = (Int32)OperationCode.NA_PROMOTION;
            _sql_cmd.Parameters.Add("@pDateTime", SqlDbType.DateTime).Value = Date;

            _num_rows_updated = _sql_cmd.ExecuteNonQuery();

            if (_num_rows_updated <= 0)
            {
              break;
            }

            _db_trx.Commit();

          } // while
        } // using
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return;
      }
    } // MarkAsPrintedBeforeDate

    //------------------------------------------------------------------------------
    // PURPOSE : Check if Printer is Available
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //     - true: Printer is available
    //     - false: Otherwise
    //
    //   NOTES :
    //
    private Boolean PrinterAvailable()
    {
      Boolean _printer_available;
      Boolean _out_of_paper;
      Boolean _queue_full;
      Int32 _printer_status;
      Int32 _printer_num_jobs_enqueued;

      _printer_available = false;
      _out_of_paper = false;
      _queue_full = false;

      try
      {
        if (!Printer.GetStatus(out _printer_status, out _printer_num_jobs_enqueued))
        {
          Log.Error("Printer.GetStatus: Failed !!");

          return false;
        }

        if (_printer_status == (Int32)Printer.Status.OutOfPaper)
        {
          _out_of_paper = true;

          return false;
        }

        if (_printer_status != (Int32)Printer.Status.Ready)
        {
          return false;
        }

        if (_printer_num_jobs_enqueued > PRINTER_MAX_QUEUE_ITEMS)
        {
          _queue_full = true;

          return false;
        }

        _printer_available = true;

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
      finally
      {
        if (_out_of_paper)
        {
          m_text_progress = Resource.String("STR_FRM_AUTOMATIC_PRINT_OUT_OF_PAPER");
        }
        else if (_queue_full)
        {
          m_text_progress = Resource.String("STR_FRM_AUTOMATIC_PRINT_WAITING_QUEUE");
        }
        else if (!_printer_available)
        {
          m_text_progress = Resource.String("STR_FRM_AUTOMATIC_PRINT_ERROR_PRINTER");
        }
      }
    } // PrinterAvailable

    //------------------------------------------------------------------------------
    // PURPOSE : Wait for web browser printed documents
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void WaitForWebBrowserObject()
    {
      Int32 _init_tick_count;

      _init_tick_count = Misc.GetTickCount();

      // Wait 6.5 secconds for printing last voucher (for no dispose the WebBrowser object)
      while (Misc.GetElapsedTicks(_init_tick_count) < 6500)
      {
        Application.DoEvents();

        Thread.Sleep(10);
      }

    } // WaitForWebBrowserObject

    #region Events

    //------------------------------------------------------------------------------
    // PURPOSE : Cancel button
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void btn_cancel_Click(object sender, EventArgs e)
    {
      m_pending_close = true;
      lbl_text_closing.Text = Resource.String("STR_FRM_AUTOMATIC_PRINT_CLOSING_DIALOG"); ;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Close timer
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void tm_close_Tick(object sender, EventArgs e)
    {
      if (m_close_allowed)
      {
        if (!m_thread.Join(5000))
        {
          try { m_thread.Abort(); }
          catch { }
        }

        tm_close.Enabled = false;
        Hide();
        form_yes_no.Hide();

        return;
      }

      lbl_text.Text = m_text_progress;
    }

    #endregion

    #endregion

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Show dialog function.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    static public void Show(Form Parent)
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_automatic_print", Log.Type.Message);
      frm_automatic_print wgc_automatic_print = new frm_automatic_print();

      wgc_automatic_print.Init();

      wgc_automatic_print.form_yes_no.Show(Parent);
      
      wgc_automatic_print.ShowDialog(wgc_automatic_print.form_yes_no);
      wgc_automatic_print.form_yes_no.Hide();

      if (Parent != null)
      {
        Parent.Focus();
      }

      wgc_automatic_print.Dispose();
      wgc_automatic_print = null;

    } // Show

    //------------------------------------------------------------------------------
    // PURPOSE : Dispose yes_no form.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    new public void Dispose()
    {
      form_yes_no.Dispose();
      form_yes_no = null;

      base.Dispose();
    } // Dispose

    #endregion

  }
}
