namespace WSI.Cashier
{
    partial class frm_sessions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
      this.components = new System.ComponentModel.Container();
      this.dg_sessions = new System.Windows.Forms.DataGridView();
      this.timer1 = new System.Windows.Forms.Timer(this.components);
      this.splitContainer1 = new System.Windows.Forms.SplitContainer();
      this.lbl_sessions_title = new System.Windows.Forms.Label();
      this.lbl_no_sessions = new System.Windows.Forms.Label();
      this.gb_session_filter = new System.Windows.Forms.GroupBox();
      this.rb_all = new System.Windows.Forms.RadioButton();
      this.rb_session_st = new System.Windows.Forms.RadioButton();
      this.btn_close = new System.Windows.Forms.Button();
      ((System.ComponentModel.ISupportInitialize)(this.dg_sessions)).BeginInit();
      this.splitContainer1.Panel1.SuspendLayout();
      this.splitContainer1.Panel2.SuspendLayout();
      this.splitContainer1.SuspendLayout();
      this.gb_session_filter.SuspendLayout();
      this.SuspendLayout();
      // 
      // dg_sessions
      // 
      this.dg_sessions.AllowUserToAddRows = false;
      this.dg_sessions.AllowUserToDeleteRows = false;
      this.dg_sessions.AllowUserToResizeColumns = false;
      this.dg_sessions.AllowUserToResizeRows = false;
      this.dg_sessions.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.dg_sessions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.dg_sessions.GridColor = System.Drawing.Color.LightGray;
      this.dg_sessions.Location = new System.Drawing.Point(12, 57);
      this.dg_sessions.Name = "dg_sessions";
      this.dg_sessions.ReadOnly = true;
      this.dg_sessions.RowHeadersVisible = false;
      this.dg_sessions.RowHeadersWidth = 20;
      this.dg_sessions.Size = new System.Drawing.Size(772, 514);
      this.dg_sessions.TabIndex = 0;
      this.dg_sessions.Sorted += new System.EventHandler(this.dataGridView1_Sorted);
      // 
      // timer1
      // 
      this.timer1.Interval = 1000;
      this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
      // 
      // splitContainer1
      // 
      this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
      this.splitContainer1.IsSplitterFixed = true;
      this.splitContainer1.Location = new System.Drawing.Point(0, 0);
      this.splitContainer1.Name = "splitContainer1";
      this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
      // 
      // splitContainer1.Panel1
      // 
      this.splitContainer1.Panel1.BackColor = System.Drawing.Color.CornflowerBlue;
      this.splitContainer1.Panel1.Controls.Add(this.lbl_sessions_title);
      // 
      // splitContainer1.Panel2
      // 
      this.splitContainer1.Panel2.Controls.Add(this.lbl_no_sessions);
      this.splitContainer1.Panel2.Controls.Add(this.gb_session_filter);
      this.splitContainer1.Panel2.Controls.Add(this.btn_close);
      this.splitContainer1.Size = new System.Drawing.Size(821, 637);
      this.splitContainer1.SplitterDistance = 25;
      this.splitContainer1.TabIndex = 9;
      // 
      // lbl_sessions_title
      // 
      this.lbl_sessions_title.AutoSize = true;
      this.lbl_sessions_title.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.lbl_sessions_title.ForeColor = System.Drawing.Color.White;
      this.lbl_sessions_title.Location = new System.Drawing.Point(11, 4);
      this.lbl_sessions_title.Name = "lbl_sessions_title";
      this.lbl_sessions_title.Size = new System.Drawing.Size(120, 16);
      this.lbl_sessions_title.TabIndex = 0;
      this.lbl_sessions_title.Text = "xOpen Sessions";
      // 
      // lbl_no_sessions
      // 
      this.lbl_no_sessions.AutoSize = true;
      this.lbl_no_sessions.BackColor = System.Drawing.Color.Transparent;
      this.lbl_no_sessions.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_no_sessions.Location = new System.Drawing.Point(392, 233);
      this.lbl_no_sessions.Name = "lbl_no_sessions";
      this.lbl_no_sessions.Size = new System.Drawing.Size(253, 18);
      this.lbl_no_sessions.TabIndex = 10;
      this.lbl_no_sessions.Text = "No opened sessions actually";
      this.lbl_no_sessions.Visible = false;
      // 
      // gb_session_filter
      // 
      this.gb_session_filter.Controls.Add(this.rb_all);
      this.gb_session_filter.Controls.Add(this.rb_session_st);
      this.gb_session_filter.Location = new System.Drawing.Point(30, 547);
      this.gb_session_filter.Name = "gb_session_filter";
      this.gb_session_filter.Size = new System.Drawing.Size(237, 48);
      this.gb_session_filter.TabIndex = 0;
      this.gb_session_filter.TabStop = false;
      this.gb_session_filter.Text = "Filters";
      this.gb_session_filter.Visible = false;
      // 
      // rb_all
      // 
      this.rb_all.AutoSize = true;
      this.rb_all.Location = new System.Drawing.Point(130, 17);
      this.rb_all.Name = "rb_all";
      this.rb_all.Size = new System.Drawing.Size(36, 17);
      this.rb_all.TabIndex = 1;
      this.rb_all.Text = "All";
      this.rb_all.UseVisualStyleBackColor = true;
      // 
      // rb_session_st
      // 
      this.rb_session_st.AutoSize = true;
      this.rb_session_st.Checked = true;
      this.rb_session_st.Location = new System.Drawing.Point(16, 17);
      this.rb_session_st.Name = "rb_session_st";
      this.rb_session_st.Size = new System.Drawing.Size(97, 17);
      this.rb_session_st.TabIndex = 0;
      this.rb_session_st.TabStop = true;
      this.rb_session_st.Text = "Session started";
      this.rb_session_st.UseVisualStyleBackColor = true;
      this.rb_session_st.CheckedChanged += new System.EventHandler(this.rb_session_st_CheckedChanged);
      // 
      // btn_close
      // 
      this.btn_close.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.btn_close.Location = new System.Drawing.Point(346, 547);
      this.btn_close.Name = "btn_close";
      this.btn_close.Size = new System.Drawing.Size(128, 48);
      this.btn_close.TabIndex = 1;
      this.btn_close.Text = "xClose";
      this.btn_close.UseVisualStyleBackColor = false;
      this.btn_close.Click += new System.EventHandler(this.btn_exit_Click);
      // 
      // frm_sessions
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(821, 637);
      this.Controls.Add(this.dg_sessions);
      this.Controls.Add(this.splitContainer1);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
      this.Name = "frm_sessions";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Open Gaming Sessions";
      this.Load += new System.EventHandler(this.frm_sessions_Load);
      ((System.ComponentModel.ISupportInitialize)(this.dg_sessions)).EndInit();
      this.splitContainer1.Panel1.ResumeLayout(false);
      this.splitContainer1.Panel1.PerformLayout();
      this.splitContainer1.Panel2.ResumeLayout(false);
      this.splitContainer1.Panel2.PerformLayout();
      this.splitContainer1.ResumeLayout(false);
      this.gb_session_filter.ResumeLayout(false);
      this.gb_session_filter.PerformLayout();
      this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dg_sessions;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label lbl_sessions_title;
        private System.Windows.Forms.Button btn_close;
        private System.Windows.Forms.GroupBox gb_session_filter;
        private System.Windows.Forms.RadioButton rb_session_st;
        private System.Windows.Forms.RadioButton rb_all;
        private System.Windows.Forms.Label lbl_no_sessions;
    }
}