﻿//------------------------------------------------------------------------------
// Copyright © 2010 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Cashier.cs
// 
//   DESCRIPTION: 
// 
//        AUTHOR: 
// 
// CREATION DATE: 
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 09-DEC-2015 YNM    First Release
//-------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using WSI.Common;

namespace WSI.Cashier
{
  public class CustomerVisitsTT
  {
 
    #region Properties

    public Int64 CustomerId { get; set; }

    public Int64 CustomerLevel { get; set; }

    public Boolean CustomerIsVIP { get; set; } 
    
    public long VisitId { get; set; }

    #endregion

    #region Constructor
    //------------------------------------------------------------------------------
    // PURPOSE: Get a CustomerVisitsTT object by Id
    //
    //  PARAMS:
    //      - INPUT:
    //        
    //      - OUTPUT:
    //        - 
    //
    // RETURNS :
    //
    //   NOTES:
    //
    public CustomerVisitsTT(Int64 _customerId,Int64 _customerLevel,Boolean _customerIsVIP)
    {
      this.CustomerId = _customerId;
      this.CustomerLevel = _customerLevel;
      this.CustomerIsVIP = _customerIsVIP;
    }
    #endregion

    #region Public Functions

    //------------------------------------------------------------------------------
    // PURPOSE: Register the entrance of customer 
    //
    //  PARAMS:
    //      - INPUT:
    //        
    //      - OUTPUT:
    //        - 
    //
    // RETURNS :
    //      - Boolean if entrance is registered
    //
    //   NOTES:
    //
    public Boolean RegisterEntrance()
    {

      if (CustomerId == 0){
        throw new Exception("CustomerId must be declared"); 
      } 

      try
      {
        VisitId = 0;
        VisitId = GetActualVisit();        

        using (DB_TRX _db_trx = new DB_TRX())
        {

          if (VisitId == 0)
          {
            //Save new visit 
            VisitId = SaveVisit();
          }

          if (VisitId > 0)
          {
            //Save new entrance
            return SaveEntrance();

          }

        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    }//RegisterEntrance

    #endregion

    #region Private Functions

    //------------------------------------------------------------------------------
    // PURPOSE: Save customer visit
    //
    //  PARAMS:
    //      - INPUT:
    //        
    //      - OUTPUT:
    //        - 
    //
    // RETURNS :
    //      - Int64 Visit_id
    //   NOTES:
    //
    private Int64 GetActualVisit()
    {
      StringBuilder _sb;

      VisitId = 0;

      using (DB_TRX _db_trx = new DB_TRX())
      {
        //Find if exists a previuos visit the same day 
        _sb = new StringBuilder();
        _sb.AppendLine("SELECT  CUT_VISIT_ID                    ");
        _sb.AppendLine("FROM    CUSTOMER_VISITS                 ");
        _sb.AppendLine("WHERE   CUT_CUSTOMER_ID = @pCustomerId  ");
        _sb.AppendLine("AND   CUT_GAMING_DAY  = dbo.GamingDayFromDateTime(@pGamingDay)     ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
        {
          _cmd.Parameters.Add("@pCustomerId", SqlDbType.BigInt).Value = CustomerId;
          _cmd.Parameters.Add("@pGamingDay", SqlDbType.DateTime).Value = Misc.TodayOpening();

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (_reader.Read())
            {
              VisitId = _reader.GetInt64(0);
            }
          }
        }
      }

      return VisitId;

    }//GetActualVisit

    //------------------------------------------------------------------------------
    // PURPOSE: Find if exists a previuos visit the same day 
    //
    //  PARAMS:
    //      - INPUT:
    //        
    //      - OUTPUT:
    //        - 
    //
    // RETURNS :
    //      - Int64 Visit_id
    //   NOTES:
    //
    private Int64 SaveVisit()
    {
      StringBuilder _sb;
      SqlParameter _param;

      VisitId = 0;

      try
      {

        using (DB_TRX _db_trx = new DB_TRX())
        {
          //Save new visit 
          _sb = new StringBuilder();
          _sb.AppendLine("INSERT INTO CUSTOMER_VISITS                                                 ");
          _sb.AppendLine("(CUT_GAMING_DAY,CUT_CUSTOMER_ID,CUT_LEVEL,CUT_IS_VIP) VALUES   ");
          _sb.AppendLine("(dbo.GamingDayFromDateTime(@pGamingDay),@pCustomerId,@pLevel,@pIsVIP)");
          _sb.AppendLine("SELECT @pVisitId = SCOPE_IDENTITY()");

          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pCustomerId", SqlDbType.BigInt).Value = CustomerId;
            _cmd.Parameters.Add("@pGamingDay", SqlDbType.DateTime).Value = Misc.TodayOpening();
            _cmd.Parameters.Add("@pLevel", SqlDbType.Int).Value = CustomerLevel;
            _cmd.Parameters.Add("@pIsVIP", SqlDbType.Bit).Value = CustomerIsVIP;


            _param = _cmd.Parameters.Add("@pVisitId", SqlDbType.BigInt);
            _param.Direction = ParameterDirection.Output;

            using (SqlDataReader _reader = _cmd.ExecuteReader())
            {
              VisitId = (Int64)_param.Value;             
             
            }
            
            _db_trx.SqlTransaction.Commit();
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return VisitId;

    }//SaveVisit

    //------------------------------------------------------------------------------
    // PURPOSE: Save customer entrance
    //
    //  PARAMS:
    //      - INPUT:
    //        
    //      - OUTPUT:
    //        - 
    //
    // RETURNS :
    //      - Int64 Visit_id
    //   NOTES:
    //
    private Boolean SaveEntrance()
    {
      StringBuilder _sb;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _sb = new StringBuilder();
          _sb.AppendLine("INSERT INTO CUSTOMER_ENTRANCES                                                            ");
          _sb.AppendLine("  (CUE_ENTRANCE_DATETIME, CUE_VISIT_ID, CUE_CASHIER_SESSION_ID, CUE_CASHIER_USER_ID       ");
          _sb.AppendLine("  ,CUE_ENTRANCE_BLOCK_REASON,CUE_ENTRANCE_BLOCK_REASON2, CUE_ENTRANCE_BLOCK_DESCRIPTION   ");
          _sb.AppendLine("  ,CUE_REMARKS, CUE_TICKET_ENTRY_ID, CUE_TICKET_ENTRY_PRICE, CUE_DOCUMENT_TYPE ");
          _sb.AppendLine("  ,CUE_DOCUMENT_NUMBER, CUE_VOUCHER_ID)");
          _sb.AppendLine("VALUES                                                                                    ");
          _sb.AppendLine("  (@pEntraceDate, @pVisitId, @pCashierSessionId, @pCashierUserId                          ");
          _sb.AppendLine("  ,@pEntBlockReason, @pEntBlockReason2, @pEntBlockDes                                     ");
          _sb.AppendLine("  ,@pRemarks, @pTicketId, @pTicketEntryPrice                                              ");
          _sb.AppendLine("  ,@pDocumentType, @pDocumentNumber, @pVoucherId)                                         ");

          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pEntraceDate", SqlDbType.DateTime).Value = WGDB.Now;
            _cmd.Parameters.Add("@pVisitId", SqlDbType.BigInt).Value = VisitId;
            _cmd.Parameters.Add("@pCashierSessionId", SqlDbType.BigInt).Value = Cashier.SessionId;
            _cmd.Parameters.Add("@pCashierUserId", SqlDbType.Int).Value = Cashier.UserId;
            _cmd.Parameters.Add("@pEntBlockReason", SqlDbType.Int).Value = DBNull.Value;
            _cmd.Parameters.Add("@pEntBlockReason2", SqlDbType.BigInt).Value = DBNull.Value;
            _cmd.Parameters.Add("@pEntBlockDes", SqlDbType.NVarChar).Value = DBNull.Value;
            _cmd.Parameters.Add("@pRemarks", SqlDbType.NVarChar).Value = DBNull.Value;
            _cmd.Parameters.Add("@pTicketId", SqlDbType.BigInt).Value = DBNull.Value;
            _cmd.Parameters.Add("@pTicketEntryPrice", SqlDbType.Money).Value = DBNull.Value;
            _cmd.Parameters.Add("@pDocumentType", SqlDbType.Int).Value = DBNull.Value;
            _cmd.Parameters.Add("@pDocumentNumber", SqlDbType.NVarChar).Value = DBNull.Value; ;
            _cmd.Parameters.Add("@pVoucherId", SqlDbType.BigInt).Value = DBNull.Value; ;

            if (_cmd.ExecuteNonQuery() == 1)
            {            
              _db_trx.SqlTransaction.Commit();  
              return true;
            }

          }
        }

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    }//SaveEntrance

    #endregion
    
  }
}
