//------------------------------------------------------------------------------
// Copyright � 2015 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_catalog.cs
// 
//   DESCRIPTION: frm_catalog
// 
//        AUTHOR: Ferran Ortner
// 
// CREATION DATE: 10-NOV-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 10-NOV-2015 FOS    First version.
// 23-NOV-2015 FOS    Bug 6771:Payment tickets & handpays
// 25-NOV-2015 FOS    Product Backlog Item: 3709 Payment tickets & handpays, applied new design
// 01-MAR-2016 EOR    Product Backlog Item: 7440 Disputes and Handpays division - New Button functionality
// 21-MAR-2016 AMF    Bug 10794: Exception BindingSource
// 30-MAY-2016 FOS    Bug 13795: Exception not controlled
// ----------- ------ ----------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using WSI.Common;
using WSI.Cashier.Controls;
using System.Data;

namespace WSI.Cashier
{
  public partial class frm_catalog : frm_base
  {
    #region Constants
    private const int CATALOG_Y_POSITION_SIZE_BALANCE = 160;
    #endregion

    #region Attributes
    private frm_yesno m_form_yes_no;
    private Boolean m_is_apply_tax_result;
    private Boolean m_is_get_apply_taxes_dictionary;
    private Int64 m_account_operation_reason_id;
    private String m_account_operation_comment;
    private Form m_parent;
    //EOR 01-MAR-2016 Attributes for Only Comments
    private Boolean m_without_reason;
    private Boolean m_dispute;
    #endregion

    #region Constructor

    /// <summary>
    /// Constructor method.
    /// 1. Initialize controls.
    /// 2. Initialize control�s resources. (NLS, Images, etc)
    /// </summary>
    public frm_catalog(Boolean ApplyTaxDictionary)
    {
      m_is_get_apply_taxes_dictionary = ApplyTaxDictionary;

      InitializeComponent();

      InitializeControlResources();

      Init();
    }

    //EOR 01-MAR-2016 New Constructors 
    public frm_catalog(Boolean Dispute, Boolean WithOutReason)
    {
      m_without_reason = WithOutReason;
      m_dispute = Dispute;

      InitializeComponent();

      InitializeControlResources();

      Init();
    }

    #endregion

    #region Private Methods


    /// <summary>
    /// Initialize screen logic
    /// </summary>
    private void Init()
    {
      // Center screen
      this.Location = new Point(1024 / 2 - this.Width / 2, CATALOG_Y_POSITION_SIZE_BALANCE);

      // Show keyboard if Automatic OSK it's enabled
      //WSIKeyboard.Toggle();

      m_form_yes_no = new frm_yesno();


      // Set focus
      this.txt_reason.Select();

    }



    //------------------------------------------------------------------------------
    // PURPOSE : Initialize control resources (NLS strings, images, etc).
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    public void InitializeControlResources()
    {
      String _coma = String.Empty;
      List<WSI.Common.Catalog.Catalog_Item> _item_catalog_list = new List<WSI.Common.Catalog.Catalog_Item>();
      DataTable _dt_reason;
      DataRow _dr_reason;

      _dt_reason = new DataTable();
      _dt_reason.Columns.Add("Key", Type.GetType("System.String"));
      _dt_reason.Columns.Add("Value", Type.GetType("System.Int64"));

      _dr_reason = _dt_reason.NewRow();
      _dr_reason["Key"] = "  ";
      _dr_reason["Value"] = 0;
      _dt_reason.Rows.Add(_dr_reason);

      // - Show unblock labels
      lbl_unblock_description.Visible = true;

      // - NLS Strings:
      lbl_unblock_description.Text = Resource.String("STR_FRM_CATALOG_REASON");
      lbl_block_unblock_description.Text = Resource.String("STR_FRM_CATALOG_COMMENTS");

      btn_accept.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_OK");
      btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");

      // EOR 01-MAR-2016
      if (m_without_reason == false)
      {
        Catalog catalog = new Catalog();

        if (m_is_get_apply_taxes_dictionary)
        {
          this.FormTitle = Resource.String("STR_FRM_CATALOG_TITLE_APPLY");
          _item_catalog_list = catalog.GetCatalogItems(Catalog.CatalogSystemType.ApplyTax);
        }
        else
        {
          this.FormTitle = Resource.String("STR_FRM_CATALOG_TITLE_NO_APPLY");
          _item_catalog_list = catalog.GetCatalogItems(Catalog.CatalogSystemType.RemoveTax);
        }

        for (int i = 0; i < _item_catalog_list.Count; i++)
        {
          _dr_reason = _dt_reason.NewRow();
          _dr_reason["Key"] = _item_catalog_list[i].Name;
          _dr_reason["Value"] = _item_catalog_list[i].CatalogItemId;
          _dt_reason.Rows.Add(_dr_reason);
        }

        cb_reason.DataSource = _dt_reason;
        cb_reason.ValueMember = "Value";
        cb_reason.DisplayMember = "Key";
      }
      else
      {
        // EOR 01-MAR-2016
        lbl_block_unblock_description.Location = lbl_unblock_description.Location;
        lbl_unblock_description.Visible = false;

        txt_reason.Location = cb_reason.Location;
        txt_reason.Size = new System.Drawing.Size(568, 203);

        cb_reason.Visible = false;
        lbl_unblock_description.Visible = false;

        if (m_dispute)
        {
          this.FormTitle = Resource.String("STR_FRM_COMMENT_DISPUTE");
        }
        else
        {
          this.FormTitle = Resource.String("STR_FRM_COMMENT_HANDPAY");
        }
      }

    } // InitializeControlResources

    #endregion

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Init and Show the form.
    //
    //  PARAMS :
    //      - INPUT :
    //          - ParentForm
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public void Show(Form ParentForm, out Boolean IsApplyTaxResult, out Int64 AccountOperationReasonId, out String AccountOperationComment)
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_catalog (4 params)", Log.Type.Message);
      m_parent = ParentForm;

      try
      {
        m_form_yes_no.Show(ParentForm);
        this.ShowDialog(m_form_yes_no);
        m_form_yes_no.Hide();
        IsApplyTaxResult = m_is_apply_tax_result;
        AccountOperationReasonId = m_account_operation_reason_id;
        AccountOperationComment = m_account_operation_comment;

        Misc.WriteLog("[FORM CLOSE] frm_catalog (4 params)", Log.Type.Message);
        this.Close();
      }
      catch (Exception _ex)
      {
        IsApplyTaxResult = false;
        AccountOperationComment = "";
        AccountOperationReasonId = 0;
        Log.Exception(_ex);
        m_form_yes_no.Hide();
      }

    } // Show

    //EOR 01-MAR-2016 Add Overrides for Only Comments
    public void Show(Form ParentForm, out String AccountOperationComment)
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_catalog (2 params)", Log.Type.Message);
      m_parent = ParentForm;

      try
      {
        m_form_yes_no.Show(ParentForm);
        this.ShowDialog(m_form_yes_no);
        m_form_yes_no.Hide();
        AccountOperationComment = m_account_operation_comment;

        Misc.WriteLog("[FORM CLOSE] frm_catalog (2 params)", Log.Type.Message);
        this.Close();
      }
      catch (Exception _ex)
      {
        AccountOperationComment = "";
        Log.Exception(_ex);
        m_form_yes_no.Hide();
      }

    } // Show

    //------------------------------------------------------------------------------
    // PURPOSE : Dispose yes_no form.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    new public void Dispose()
    {

      base.Dispose();
    }

    #endregion

    #region ButtonHandling

    private void btn_accept_Click(object sender, EventArgs e)
    {
      // EOR 01-MAR-2016
      if (m_without_reason == false)
      {

        Boolean _is_cancel_update_reason;

        _is_cancel_update_reason = (cb_reason.Text == Resource.String("STR_GENERAL_CATALOG_REASON_OTHERS")) && String.IsNullOrEmpty(txt_reason.Text);


        m_account_operation_reason_id = Convert.ToInt64(this.cb_reason.SelectedValue);
        m_account_operation_comment = this.txt_reason.Text;

        if (!_is_cancel_update_reason && m_account_operation_reason_id > 0)
        {
          m_is_apply_tax_result = true;

          // hide keyboard and close
          WSIKeyboard.Hide();

          Misc.WriteLog("[FORM CLOSE] frm_catalog (accept - not is_cancel_update_reason and m_account_operation_reason_id > 0)", Log.Type.Message);
          this.Close();
        }
        else
        {
          DialogResult _dlg_rc;
          _dlg_rc = new DialogResult();

          if (!(m_account_operation_reason_id > 0))
          {
            _dlg_rc = frm_message.Show(Resource.String("STR_CATALOG_ERROR_REASON"), "", MessageBoxButtons.OK, Images.CashierImage.Warning, Cashier.MainForm);
          }
          else
          {
            _dlg_rc = frm_message.Show(Resource.String("STR_CATALOG_ERROR_COMMENT"), "", MessageBoxButtons.OK, Images.CashierImage.Warning, Cashier.MainForm);
          }

          m_is_apply_tax_result = false;
        }
      }
      else
      {
        // EOR 01-MAR-2016
        m_account_operation_comment = txt_reason.Text.Trim();
        WSIKeyboard.Hide();

        Misc.WriteLog("[FORM CLOSE] frm_catalog (accept)", Log.Type.Message);
        this.Close();
      }
    }

    private void btn_cancel_Click(object sender, EventArgs e)
    {

      m_is_apply_tax_result = false;
      m_account_operation_comment = string.Empty;  // 01-MAR-2016 EOR
      // hide keyboard and close
      WSIKeyboard.Hide();

      Misc.WriteLog("[FORM CLOSE] frm_catalog (cancel)", Log.Type.Message);
      this.Close();
    }

    private void btn_keyboard_Click(object sender, EventArgs e)
    {
      // show keyboard (if hidden)
      WSIKeyboard.ForceToggle();
    }

    #endregion

  }
}
