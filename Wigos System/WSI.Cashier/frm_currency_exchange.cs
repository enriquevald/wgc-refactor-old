﻿//------------------------------------------------------------------------------
// Copyright © 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_currency_exchange.cs
// 
//   DESCRIPTION: Implements the following classes:
//                1. frm_currency_exchange
//
//        AUTHOR: Jordi Masachs
// 
// CREATION DATE: 04-MAY-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 04-MAY-2016 JMV     First release.
// 24-MAY-2016 ETP     PBI 12652: Forced to show currency selection on shown event.
// 01-JUN-2016 ETP     Fixed Bug 14039 - Cambio de divisa: no se lleva a término la operación al no aplicar las comisiones con el permiso deshabilitado
// 01-JUN-2016 FAV     Fixed Bug 14031: The operation is inabled when the Amount is equal to 0.
// 02-JUN-2016 ETP     Fixed Bug 13995: The waive button not works as especified.
// 06-JUL-2016 DHA     Product Backlog Item 15064: multicurrency chips sale/purchase
// 18-OCT-2016 ESE     Bug 14149: If there is no configurated comission then disable the "comission button"
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WSI.Cashier.Controls;
using WSI.Common;

namespace WSI.Cashier
{
  public partial class frm_currency_exchange : frm_base
  {

    #region Attributes

    private CardData m_card_data;
    private CurrencyExchange m_selected_currency;
    private CurrencyExchangeResult m_exchange_result;
    private frm_generic_amount_input m_amount_input;
    private Boolean Modify = false;
    private Boolean m_first_time = true;
    private Form m_ParentForm;
    private Boolean m_waive_commission;
    private List<CurrencyExchange> m_currencies = null;
    private CurrencyExchange m_national_currency = null;

    #endregion

    #region Public

    public CurrencyExchangeResult GetExchangeResult
    {
      get
      {
        return m_exchange_result;
      }
    }

    public frm_currency_exchange(CardData card_data, Form ParentForm)
    {
      InitializeComponent();
      InitializeControlResources();
      m_card_data = card_data;
      m_ParentForm = ParentForm;
      m_waive_commission = false;
    }

    #endregion

    #region Private

    /// <summary>
    /// Initialize control resources (NLS strings, images, etc)
    /// </summary>
    private void InitializeControlResources()
    {
      List<CurrencyExchangeType> _types = new List<CurrencyExchangeType>();

      // - NLS Strings:
      //   - Title:
      this.FormTitle = this.FormSubTitle = Resource.String("STR_CURRENCY_EXCHANGE_TITLE");
      this.FormSubTitle = "";

      //   - Labels
      lbl_exchange.Text = Resource.String("STR_VOUCHER_APPLIED_EXCHANGE");
      lbl_comission.Text = Resource.String("STR_VOUCHER_COMISSION");
      lbl_exchange_rate.Text = Resource.String("STR_VOUCHER_EXCHANGE_RATE");

      lbl_foreign_currency.Text = Resource.String("STR_CURRENCY_EXCHANGE_VALUE");
      lbl_amount_to_give.Text = Resource.String("STR_NATIONAL_EXCHANGE_VALUE");

      //   - Buttons
      btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");
      btn_ok.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_OK");
      chk_without_comissions.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_NOT_APPLY_COMISSION");

      //   - Other

      btn_ok.Enabled = false;

      // - Images:

      m_selected_currency = uc_payment_method1.CurrencyExchange;

      SetBlankExchange(false);

      m_amount_input = new frm_generic_amount_input();

      m_amount_input.OnKeyPressed += new frm_generic_amount_input.KeyPressed(m_amount_input_OnKeyPress);

      uc_payment_method1.HeaderText = Resource.String("STR_VOUCHER_CAGESESSIONREPORT_CURRENCY");

      _types.Add(CurrencyExchangeType.CURRENCY);

      CurrencyExchange.GetAllowedCurrencies(true, _types, false, out m_national_currency, out m_currencies);

      // Remove national currency
      m_currencies.Remove(m_national_currency);

      uc_payment_method1.VoucherType = VoucherTypes.CardAdd; // m_voucher_type
      uc_payment_method1.InitControl(m_currencies, null, FeatureChips.ChipsOperation.Operation.NONE);

      uc_payment_method1.CurrencyExchange = m_national_currency;

    } // InitializeControlResources

    /// <summary>
    /// Calculate currency exchange
    /// </summary>
    private void CalculateExchange()
    {
      Currency _value;
      Decimal _foreign_amount;
      String _format;

      if (m_selected_currency != null)
      {
        if (Decimal.TryParse(uc_foreign_currency.Text, out _foreign_amount))
        {
          if (_foreign_amount > 0)
          {
            m_selected_currency.ApplyExchange(_foreign_amount, out m_exchange_result);

            _value = Format.ParseCurrency(m_exchange_result.GrossAmount.ToString());
            _format = "N" + m_exchange_result.Decimals;
            lbl_exchange_amount.Text = _value.ToString(_format) + " " + m_national_currency.CurrencyCode;

            if (m_waive_commission)
            {
              m_exchange_result.Comission = 0;
              m_exchange_result.NetAmountSplit1 = m_exchange_result.GrossAmount;
            }
            _value = Format.ParseCurrency(m_exchange_result.Comission.ToString());
            lbl_comission_amount.Text = _value.ToString(_format) + " " + m_national_currency.CurrencyCode;

            _value = Format.ParseCurrency(m_exchange_result.NetAmount.ToString());
            lbl_amount_to_give_amount.Text = _value.ToString(_format) + " " + m_national_currency.CurrencyCode;

            m_exchange_result.OutCurrencyCode = m_national_currency.CurrencyCode;
            lbl_exchange_rate_amount.Text = GetCurrencyExchangeFormat();

            btn_ok.Enabled = (_value > 0);
          }
          else
          {
            SetBlankExchange(true);
          }
        }
        else
        {
          SetBlankExchange(true);
        }
      }
      else
      {
        SetBlankExchange(false);
      }
    }

    /// <summary>
    /// Get Currency excheange Format
    /// </summary>
    /// <returns>String Exchange</returns>
    private string GetCurrencyExchangeFormat()
    {
      String _exchange;


      _exchange = String.Format("1  {0} = {1} {2}", m_selected_currency.CurrencyCode,
                                                    m_exchange_result.ChangeRate.ToString("n8"),
                                                    m_national_currency.CurrencyCode);
      return _exchange;
    }

    /// <summary>
    /// Set controls to blank
    /// </summary>
    private void SetBlankExchange(bool keep_iso_codes)
    {
      if (m_selected_currency != null)
      {
        m_selected_currency.ApplyExchange(0, out m_exchange_result);
        String format = "n" + m_exchange_result.Decimals;
        lbl_exchange_amount.Text = m_exchange_result.GrossAmount.ToString(format) + " " + m_national_currency.CurrencyCode;
        lbl_comission_amount.Text = m_exchange_result.Comission.ToString(format) + " " + m_national_currency.CurrencyCode;
        lbl_exchange_rate_amount.Text = GetCurrencyExchangeFormat();
      }
      m_exchange_result = null;
      lbl_exchange_amount.Text = "";
      lbl_comission_amount.Text = "";
      lbl_amount_to_give_amount.Text = "";
      if (!keep_iso_codes)
      {
        lbl_foreign_iso.Text = "";
      }
      btn_ok.Enabled = false;
    }

    /// <summary>
    /// controls amount_input logic
    /// </summary>
    private void ShowOrCreate()
    {
      Decimal _amount;
      Boolean result;
      Currency _amount_old;

      if (this.m_amount_input == null || this.m_amount_input.IsDisposed)
      {
        this.m_amount_input = new frm_generic_amount_input();
        this.m_amount_input.OnKeyPressed += new frm_generic_amount_input.KeyPressed(m_amount_input_OnKeyPress);
      }

      this.m_amount_input.StartPosition = FormStartPosition.Manual;
      this.m_amount_input.CustomLocation = true;

      m_amount_input.Location = new Point(this.Location.X + this.Width, this.Location.Y);
      this.m_amount_input.BringToFront();

      _amount = Format.ParseCurrency(uc_foreign_currency.Text);

      _amount_old = _amount;

      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_currency_exchange", Log.Type.Message);
      result = this.m_amount_input.Show(GENERIC_AMOUNT_INPUT_TYPE.AMOUNT, this.FormTitle, ref _amount);//ferran

      // JBC 08-OCT-2014 If it's same amount, don't set value on textbox (setting value on textbox throws keypress event and recalculate Exchange).
      if (_amount_old != _amount)
      {
        if (result)
        {
          uc_foreign_currency.Text = ((Currency)_amount).ToStringWithoutSymbols();
        }
        this.m_amount_input.BringToFront();
        this.Modify = false;
      }
    }

    #endregion

    #region Events

    void m_amount_input_OnKeyPress(String Amount)
    {
      uc_foreign_currency.Text = Amount;
    }

    private void btn_cancel_Click(object sender, EventArgs e)
    {
      this.DialogResult = System.Windows.Forms.DialogResult.Cancel;

      Misc.WriteLog("[FORM CLOSE] frm_currency_exchange (cancel)", Log.Type.Message);
      this.Close();
    }

    private void btn_ok_Click(object sender, EventArgs e)
    {

      String _error_str;

      if (m_waive_commission)
      {
        if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.MultiCurrency_AllowNoComission, ProfilePermissions.TypeOperation.RequestPasswd, m_ParentForm, false, out _error_str))
        {
          return;
        }
      }

      this.DialogResult = System.Windows.Forms.DialogResult.OK;
    }

    private void uc_foreign_currency_TextChanged(object sender, EventArgs e)
    {
      CalculateExchange();
    }

    private void frm_currency_exchange_Activated_1(object sender, EventArgs e)
    {
      if (m_first_time)
      {
        this.Location = new Point(this.Location.X - 123, this.Location.Y);
        m_first_time = false;
      }

      if (!Modify)
      {
        Modify = false;

        if (m_amount_input != null)
        {
          m_amount_input.Hide();
          m_amount_input.Dispose();
          m_amount_input = null;
        }
      }
    }

    private void uc_foreign_currency_Click(object sender, EventArgs e)
    {
      this.uc_foreign_currency.BackColor = Color.LightGoldenrodYellow;
      this.uc_foreign_currency.Refresh();

      ShowOrCreate();

      uc_foreign_currency.TextChanged -= new System.EventHandler(this.uc_foreign_currency_TextChanged);
      uc_foreign_currency.Text = ((Currency)Format.ParseCurrency(uc_foreign_currency.Text)).ToStringWithoutSymbols();
      uc_foreign_currency.TextChanged += new System.EventHandler(this.uc_foreign_currency_TextChanged);
    }

    private void uc_foreign_currency_Enter(object sender, EventArgs e)
    {
      Currency _value;

      uc_foreign_currency.TextChanged -= new System.EventHandler(this.uc_foreign_currency_TextChanged);
      uc_foreign_currency.TextChanged -= new System.EventHandler(this.uc_foreign_currency_TextChanged);

      _value = Format.ParseCurrency(this.uc_foreign_currency.Text);
      uc_foreign_currency.Text = _value.ToStringWithoutSymbols();

      uc_foreign_currency.TextChanged += new System.EventHandler(this.uc_foreign_currency_TextChanged);
      uc_foreign_currency.TextChanged += new System.EventHandler(this.uc_foreign_currency_TextChanged);
    }

    private void uc_foreign_currency_KeyPress(object sender, KeyPressEventArgs e)
    {
      uc_round_textbox _txt_aux;

      _txt_aux = uc_foreign_currency;
    }

    private void uc_foreign_currency_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
    {
      char c;

      c = Convert.ToChar(e.KeyCode);
      if (c == '\r')
      {
        e.IsInputKey = true;
      }
    }

    private void uc_foreign_currency_Leave(object sender, EventArgs e)
    {
      Currency _value;
      if (m_exchange_result != null)
      {
        String _format = "n" + m_exchange_result.Decimals;

        uc_foreign_currency.TextChanged -= new System.EventHandler(this.uc_foreign_currency_TextChanged);
        uc_foreign_currency.TextChanged -= new System.EventHandler(this.uc_foreign_currency_TextChanged);

        _value = Format.ParseCurrency(this.uc_foreign_currency.Text);
        uc_foreign_currency.Text = _value.ToString(_format);

        uc_foreign_currency.TextChanged += new System.EventHandler(this.uc_foreign_currency_TextChanged);
        uc_foreign_currency.TextChanged += new System.EventHandler(this.uc_foreign_currency_TextChanged);


        this.uc_foreign_currency.Style = uc_round_textbox.RoundTextBoxStyle.BASIC_CUSTOM;
        this.uc_foreign_currency.Refresh();
      }
    }


    private void chk_without_comissions_Click(object sender, EventArgs e)
    {

      m_waive_commission = !m_waive_commission;

      if (m_waive_commission)
      {
        chk_without_comissions.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_APPLY_COMISSION");
      }
      else
      {
        chk_without_comissions.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_NOT_APPLY_COMISSION");
      }
      CalculateExchange();
    }

    private void frm_currency_exchange_Shown(object sender, EventArgs e)
    {
      if (GeneralParam.GetBoolean("Cashier.Recharge", "ShowCurrencyExchange", false))
      {
        uc_payment_method1.ShowCurrenciesForm(true);
      }
    }

    private void uc_payment_method1_CurrencyChanged(CurrencyExchange CurrencyExchange)
    {
      //ESE 18-OCT-2016 When change the currency check if there is no configurated comission, then disable the "comission button"
      chk_without_comissions.Enabled = (Decimal.Parse(CurrencyExchange.FixedComission.ToString()) > 0 || Decimal.Parse(CurrencyExchange.VariableComission.ToString()) > 0) ? true : false;

      if (CurrencyExchange != null)
      {
        m_selected_currency = CurrencyExchange;
        lbl_foreign_iso.Text = m_selected_currency.CurrencyCode;
        CalculateExchange();
      }
    }

    #endregion

  }
}
