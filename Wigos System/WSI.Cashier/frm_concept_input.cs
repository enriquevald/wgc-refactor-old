//------------------------------------------------------------------------------
// Copyright � 20014 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_conceptt_input.cs
// 
//   DESCRIPTION: Implements the following classes:
//                1. frm_concept_input
//
//        AUTHOR: RRR
// 
// CREATION DATE: 04-SEP-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 05-SEP-2014 RRR    First release.
// 18-sep-2014 LMRS   NLS adapt and output format
// 22-OCT-2014 DLL    Fixed Bug WIG-1549: Concepts operations not generate activity
// 10-DEC-2014 DRV    Decimal currency formatting: DUT 201411 - Tito Chile - Currency formatting without decimals
// 25-FEB-2014 YNM    Fixed Bug WIG-2102: Concepts amounts was in IsoCode instead of Currency Symbol.
// 05-AUG-2015 FAV    TFS 2797: Shows a Comment Windows using a general param
// 04-NOV-2015 SGB    Backlog Item WIG-5859: Change designer
// 27-APR-2016 DHA    Product Backlog Item 10825: added chips types to cage concepts
// 08-FEB-2017 CCG    Bug 24235:Cajero: En Conceptos se habilita el punto decimal al cambiar moneda
// 07-SEP-2017 EOR    Bug 29654: WIGOS-4980 Accounting - Not redeemed chips denomination are shown on unclaimed form
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WSI.Common;
using System.Data.SqlClient;
using System.Globalization;
using System.Collections;
using System.Reflection;
using System.Threading;
using WSI.Cashier.TITO;
using WSI.Common.TITO;


namespace WSI.Cashier
{

  public partial class frm_concept_input : WSI.Cashier.Controls.frm_base
  {
    #region Attributes

    private Currency m_total_amount;
    private Boolean m_is_priced_concept;
    private Currency m_price_per_unit;
    private String m_concept_name;
    private Boolean m_only_national_currency;
    private Int64 m_concept_id;

    private CashierConceptOperationResult m_result;

    private List<CurrencyExchange> m_currencies = null;
    private CurrencyExchange m_national_currency = null;

    private static String m_decimal_str = System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencyDecimalSeparator;
    private static frm_yesno form_yes_no;

    #endregion

    #region Constructor

    /// <summary>
    /// Constructor method.
    /// 1. Initialize controls.
    /// 2. Initialize control�s resources. (NLS, Images, etc)
    /// </summary>
    public frm_concept_input()
    {
      InitializeComponent();
      InitializeControlResources();

      EventLastAction.AddEventLastAction(this.Controls);

      m_result = new CashierConceptOperationResult();
      m_total_amount = 0;
      m_concept_name = "Unknown";

      form_yes_no = new frm_yesno();
      form_yes_no.Opacity = 0.6;
    }

    #endregion

    #region Buttons

    //private Boolean flag_money = false;
    private Currency btn_money_1_amount;
    private Currency btn_money_2_amount;
    private Currency btn_money_3_amount;
    private Currency btn_money_4_amount;
    private String btn_money_1_value;
    private String btn_money_2_value;
    private String btn_money_3_value;
    private String btn_money_4_value;

    //------------------------------------------------------------------------------
    // PURPOSE : Initialize Amount Buttons money.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void InitMoneyButtons()
    {
      String _currency_format = ""; //"C0";

      btn_money_1_amount = 0;
      btn_money_1_value = "";
      btn_money_2_amount = 0;
      btn_money_2_value = "";
      btn_money_3_amount = 0;
      btn_money_3_value = "";
      btn_money_4_amount = 0;
      btn_money_4_value = "";

      LoadDefaultAmountButtonsSQL();

      btn_money_1.Text = "+" + btn_money_1_amount.ToString(_currency_format);
      if ((Decimal)btn_money_1_amount == 0)
      {
        this.btn_money_1.Enabled = false;
        this.btn_money_1.Visible = false;
      }
      if ((Decimal)btn_money_1_amount >= 1000)
      {
        this.btn_money_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      }

      btn_money_2.Text = "+" + btn_money_2_amount.ToString(_currency_format);
      if ((Decimal)btn_money_2_amount == 0)
      {
        this.btn_money_2.Enabled = false;
        this.btn_money_2.Visible = false;
      }
      if ((Decimal)btn_money_2_amount >= 1000)
      {
        this.btn_money_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      }

      btn_money_3.Text = "+" + btn_money_3_amount.ToString(_currency_format);
      if ((Decimal)btn_money_3_amount == 0)
      {
        this.btn_money_3.Enabled = false;
        this.btn_money_3.Visible = false;
      }
      if ((Decimal)btn_money_3_amount >= 1000)
      {
        this.btn_money_3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      }

      btn_money_4.Text = "+" + btn_money_4_amount.ToString(_currency_format);
      if ((Decimal)btn_money_4_amount == 0)
      {
        this.btn_money_4.Enabled = false;
        this.btn_money_4.Visible = false;
      }
      if ((Decimal)btn_money_4_amount >= 1000)
      {
        this.btn_money_4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      }

    }

    //------------------------------------------------------------------------------
    // PURPOSE : Load Default Amount Buttons money.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void LoadDefaultAmountButtonsSQL()
    {
      SqlConnection sql_conn;
      SqlDataAdapter da;
      DataSet ds;
      String sql_str;
      Decimal convert_money;

      sql_conn = null;
      ds = null;
      da = null;

      try
      {
        sql_str = " SELECT  gp_subject_key, gp_key_value        " +
                      "   FROM  general_params                  " +
                      "  WHERE  gp_group_key    = 'Cashier.AddAmount' ";

        sql_conn = WGDB.Connection();
        ds = new DataSet();
        da = new SqlDataAdapter(sql_str, sql_conn);
        da.Fill(ds);

        if (ds.Tables[0].Rows.Count > 0)
        {
          foreach (DataRow row in ds.Tables[0].Rows)
          {
            switch ((String)row[0])
            {
              case "CustomButton1":
                try
                {
                  btn_money_1_value = (String)row[1];
                  convert_money = Decimal.Parse(btn_money_1_value);
                  btn_money_1_amount = (Currency)convert_money;
                }
                catch (Exception ex)
                {
                  Log.Exception(ex);
                }
                break;

              case "CustomButton2":
                try
                {
                  btn_money_2_value = (String)row[1];
                  convert_money = Decimal.Parse(btn_money_2_value);
                  btn_money_2_amount = (Currency)convert_money;
                }
                catch (Exception ex)
                {
                  Log.Exception(ex);
                }
                break;

              case "CustomButton3":
                try
                {
                  btn_money_3_value = (String)row[1];
                  convert_money = Decimal.Parse(btn_money_3_value);
                  btn_money_3_amount = (Currency)convert_money;
                }
                catch (Exception ex)
                {
                  Log.Exception(ex);
                }
                break;

              case "CustomButton4":
                try
                {
                  btn_money_4_value = (String)row[1];
                  convert_money = Decimal.Parse(btn_money_4_value);
                  btn_money_4_amount = (Currency)convert_money;
                }
                catch (Exception ex)
                {
                  Log.Exception(ex);
                }
                break;

              default:
                break;
            }
          }
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
      finally
      {
        // Close connection
        if (da != null)
        {
          da.Dispose();
          da = null;
        }

        if (ds != null)
        {
          ds.Dispose();
          ds = null;
        }

        if (sql_conn != null)
        {
          sql_conn.Close();
          sql_conn = null;
        }
      }
    } // LoadDefaultAmountButtonsSQL

    private void btn_num_1_Click(object sender, EventArgs e)
    {
      AddNumber(((System.Windows.Forms.ButtonBase)(sender)).Text);
    }

    private void btn_num_2_Click(object sender, EventArgs e)
    {
      AddNumber(((System.Windows.Forms.ButtonBase)(sender)).Text);
    }

    private void btn_num_3_Click(object sender, EventArgs e)
    {
      AddNumber(((System.Windows.Forms.ButtonBase)(sender)).Text);
    }

    private void btn_num_4_Click(object sender, EventArgs e)
    {
      AddNumber(((System.Windows.Forms.ButtonBase)(sender)).Text);
    }

    private void btn_num_5_Click(object sender, EventArgs e)
    {
      AddNumber(((System.Windows.Forms.ButtonBase)(sender)).Text);
    }

    private void btn_num_6_Click(object sender, EventArgs e)
    {
      AddNumber(((System.Windows.Forms.ButtonBase)(sender)).Text);
    }

    private void btn_num_7_Click(object sender, EventArgs e)
    {
      AddNumber(((System.Windows.Forms.ButtonBase)(sender)).Text);
    }

    private void btn_num_8_Click(object sender, EventArgs e)
    {
      AddNumber(((System.Windows.Forms.ButtonBase)(sender)).Text);
    }

    private void btn_num_9_Click(object sender, EventArgs e)
    {
      AddNumber(((System.Windows.Forms.ButtonBase)(sender)).Text);
    }

    private void btn_num_10_Click(object sender, EventArgs e)
    {
      AddNumber(((System.Windows.Forms.ButtonBase)(sender)).Text);
    }

    private void btn_money_1_Click(object sender, EventArgs e)
    {
      Decimal _money_value;
      String _money_text;

      try
      {
        _money_value = Decimal.Parse(btn_money_1_value);
        if (txt_amount.Text.Length > 0)
        {
          _money_value = _money_value + Decimal.Parse(txt_amount.Text);
        }
      }
      catch
      {
        _money_value = 0;
      }

      if (_money_value <= Cashier.MAX_ALLOWED_AMOUNT)
      {
        _money_text = _money_value.ToString();

        txt_amount.Text = "";
        AddNumber(_money_text);
      }
    }

    private void btn_money_2_Click(object sender, EventArgs e)
    {
      Decimal _money_value;
      String _money_text;

      try
      {
        _money_value = Decimal.Parse(btn_money_2_value);
        if (txt_amount.Text.Length > 0)
        {
          _money_value = _money_value + Decimal.Parse(txt_amount.Text);
        }
      }
      catch
      {
        _money_value = 0;
      }

      if (_money_value <= Cashier.MAX_ALLOWED_AMOUNT)
      {
        _money_text = _money_value.ToString();

        txt_amount.Text = "";
        AddNumber(_money_text);
      }
    }

    private void btn_money_3_Click(object sender, EventArgs e)
    {
      Decimal _money_value;
      String _money_text;

      try
      {
        _money_value = Decimal.Parse(btn_money_3_value);
        if (txt_amount.Text.Length > 0)
        {
          _money_value = _money_value + Decimal.Parse(txt_amount.Text);
        }
      }
      catch
      {
        _money_value = 0;
      }

      if (_money_value <= Cashier.MAX_ALLOWED_AMOUNT)
      {
        _money_text = _money_value.ToString();

        txt_amount.Text = "";
        AddNumber(_money_text);
      }
    }

    private void btn_money_4_Click(object sender, EventArgs e)
    {
      Decimal _money_value;
      String _money_text;

      try
      {
        _money_value = Decimal.Parse(btn_money_4_value);
        if (txt_amount.Text.Length > 0)
        {
          _money_value = _money_value + Decimal.Parse(txt_amount.Text);
        }
      }
      catch
      {
        _money_value = 0;
      }

      if (_money_value <= Cashier.MAX_ALLOWED_AMOUNT)
      {
        _money_text = _money_value.ToString();

        txt_amount.Text = "";
        AddNumber(_money_text);
      }
    }

    private void btn_num_dot_Click(object sender, EventArgs e)
    {
      if (txt_amount.Text.IndexOf(m_decimal_str) == -1)
      {
        txt_amount.Text = txt_amount.Text + m_decimal_str;
      }
      btn_intro.Focus();
    }

    private void btn_cancel_Click(object sender, EventArgs e)
    {
      // Clean To Pay label
      lbl_total_amount.Text = "";

      this.DialogResult = DialogResult.Cancel;

      Misc.WriteLog("[FORM CLOSE] frm_concept_input (cancel)", Log.Type.Message);
      this.Close();
    }

    private void btn_back_Click(object sender, EventArgs e)
    {
      if (txt_amount.Text.Length > 0)
      {
        txt_amount.Text = txt_amount.Text.Substring(0, txt_amount.Text.Length - 1);
      }
      btn_intro.Focus();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Handle the click on the ok button, which confirms all shown data
    //           and closes the screen.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void btn_ok_Click(object sender, EventArgs e)
    {
      if (ConfirmPaiment())
      {
        ShowCommentInput();
        m_result.Amount = m_total_amount;
        m_result.IsoCode = uc_payment_method1.CurrencyExchange.CurrencyCode;
        m_result.Quantity = m_price_per_unit == 0 ? 0 : (Int32)(m_total_amount / m_price_per_unit);
        m_result.CurrencyType = uc_payment_method1.CurrencyExchange.Type;
        m_result.ConceptId = m_concept_id;

        this.DialogResult = DialogResult.OK;

        Misc.WriteLog("[FORM CLOSE] frm_concept_input (ok)", Log.Type.Message);
        this.Close();

      }//if

    } // btn_ok_Click

    //------------------------------------------------------------------------------
    // PURPOSE : Handle the click on the intro button, which confirms the amount on the
    //           numeric pad.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void btn_intro_Click(object sender, EventArgs e)
    {
      Decimal _amount;

      if (!Decimal.TryParse(txt_amount.Text, out _amount))
      {
        _amount = 0;
      }

      if (m_is_priced_concept)
      {
        m_total_amount = _amount * (Decimal)m_price_per_unit;
      }
      else
      {
        m_total_amount = _amount;
      }

      m_total_amount.CurrencyIsoCode = uc_payment_method1.CurrencyExchange.CurrencyCode;
      txt_amount.Text = String.Empty;



      this.lbl_total_amount.Text = ShowTotal(m_total_amount);

    } // btn_intro_Click

    #endregion Buttons

    #region Public Methods

    public DialogResult Show(Int64 ConceptId
                      , String ConceptName
                      , Currency PricePerUnit
                      , Boolean OnlyNationalCurrency
                      , out CashierConceptOperationResult OperationResult)
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_concept_input", Log.Type.Message);
      OperationResult = new CashierConceptOperationResult();

      // Check if input data is OK
      if (!InputDataOk(ConceptName, PricePerUnit))
      {
        return DialogResult.Abort;
      }

      // Set currencies
      m_only_national_currency = OnlyNationalCurrency;

      List<CurrencyExchangeType> _coins_and_chips = new List<CurrencyExchangeType>();
      _coins_and_chips.Add(CurrencyExchangeType.CURRENCY);
      _coins_and_chips.Add(CurrencyExchangeType.CASINOCHIP);

      CurrencyExchange.GetAllowedCurrencies(true, _coins_and_chips, false, out m_national_currency, out m_currencies);
      uc_payment_method1.CurrencyExchange = m_national_currency;

      if (m_only_national_currency)
      {
        m_currencies.Clear();
        m_currencies.Add(m_national_currency);
        uc_payment_method1.BtnSelectedCurrencyEnabled = false;
      }

      //EOR 07-SEP-2017 If the concept equals an unclaimed not show Chips not redeemed
      List<CurrencyExchange> m_list_currencies = new List<CurrencyExchange>(m_currencies);

      if (ConceptId == CageMeters.CageConceptId.Unclaimed.GetHashCode())
      {
        foreach (CurrencyExchange _currency_exchange in m_list_currencies)
        {
          if (_currency_exchange.Type == CurrencyExchangeType.CASINO_CHIP_NRE)
          {
            m_currencies.Remove(_currency_exchange);
          }
        }
      }

      uc_payment_method1.InitControl(m_currencies, null, FeatureChips.ChipsOperation.Operation.CAGE_CONCEPTS);
      uc_payment_method1.CurrencyExchange = m_national_currency;
      uc_payment_method1.VoucherType = VoucherTypes.CageConcepts;

      // Set price per unit
      m_price_per_unit = PricePerUnit;

      if (m_price_per_unit > 0)
      {
        m_is_priced_concept = true;
      }
      else
      {
        m_is_priced_concept = false;
      }

      // Set concept name
      m_concept_name = ConceptName;

      // Set ConceptId
      m_concept_id = ConceptId;

      this.SetControlsView();
      this.ShowCurrencyExchange();

      // Show form
      DialogResult _dlg_rc = this.ShowDialog(Parent);
      OperationResult = m_result;

      return _dlg_rc;

    } // Show

    #endregion

    #region Private Methods

    /// <summary>
    /// Initialize control resources (NLS strings, images, etc)
    /// </summary>
    public void InitializeControlResources()
    {
      //   - Labels
      gp_digits_box.HeaderText = Resource.String("STR_FRM_AMOUNT_INPUT_DIGITS_GROUP_BOX");
      gb_total.HeaderText = gb_total.HeaderText = Resource.String("STR_FRM_INPUT_TOTAL_TO_PAY");
      uc_payment_method1.HeaderText = Resource.String("STR_FRM_AMOUNT_CURRENCY_GROUP_BOX");

      //   - Buttons
      btn_ok.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_OK");
      btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");
      
      //  - Money buttons 
      InitMoneyButtons();

    } // InitializeControlResources

    /// <summary>
    /// Check and Add digit to amount
    /// </summary>
    /// <param name="NumberStr"></param>
    private void AddNumber(String NumberStr)
    {
      String aux_str;
      String tentative_number;
      Currency input_amount;
      int dec_symbol_pos;

      // Prevent exceeding maximum number of decimals (2)
      dec_symbol_pos = txt_amount.Text.IndexOf(m_decimal_str);
      if (dec_symbol_pos > -1)
      {
        aux_str = txt_amount.Text.Substring(dec_symbol_pos);
        if (aux_str.Length > 2)
        {
          return;
        }
      }

      // Prevent exceeding maximum amount length
      if (txt_amount.Text.Length >= Cashier.MAX_ALLOWED_AMOUNT_LENGTH)
      {
        return;
      }

      tentative_number = txt_amount.Text + NumberStr;

      try
      {
        input_amount = Decimal.Parse(tentative_number);
        if (input_amount > Cashier.MAX_ALLOWED_AMOUNT)
        {
          return;
        }
      }
      catch
      {
        input_amount = 0;

        return;
      }

      // Remove unneeded leading zeros
      // Unless we are entering a decimal value, there must not be any leading 0
      if (dec_symbol_pos == -1)
      {
        txt_amount.Text = txt_amount.Text.TrimStart('0') + NumberStr;
      }
      else
      {
        // Accept new symbol/digit 
        txt_amount.Text = txt_amount.Text + NumberStr;
      }
      btn_intro.Focus();
    }

    private void frm_amount_input_KeyPress(object sender, KeyPressEventArgs e)
    {
      String aux_str;

      char c;

      c = e.KeyChar;

      if (c >= '0' && c <= '9')
      {
        aux_str = "" + c;
        AddNumber(aux_str);

        e.Handled = true;
      }

      if (c == '\r')
      {
        btn_intro_Click(null, null);
        e.Handled = true;
      }

      if (c == '\b')
      {
        btn_back_Click(null, null);

        e.Handled = true;
      }

      if (c == '.')
      {
        if (btn_num_dot.Enabled)
        {
          btn_num_dot_Click(null, null);

          e.Handled = true;
        }
      }

      e.Handled = false;
    }

    private void splitContainer1_KeyPress(object sender, KeyPressEventArgs e)
    {
      frm_amount_input_KeyPress(sender, e);
    }

    private void frm_amount_input_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
    {
      char c;

      c = Convert.ToChar(e.KeyCode);
      if (c == '\r')
      {
        e.IsInputKey = true;
      }
    }


    // PURPOSE: Show controls of Currency Exchange
    //  PARAMS:
    //     - INPUT:
    //           - none
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - none
    private void ShowCurrencyExchange()
    {
      Boolean _is_cage_enabled;
      _is_cage_enabled = Cage.IsCageEnabled();

      lbl_currency.Text = "";
      lbl_currency.Visible = !_is_cage_enabled;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Set controls view.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //    
    private void SetControlsView()
    {
      if (m_is_priced_concept)
      {
        this.lbl_price_per_unit.Text = Resource.String("STR_FRM_AMOUNT_UNIT_PRICE") + " " + m_price_per_unit.ToString();
        gp_digits_box.Text = Resource.String("STR_FRM_AMOUNT_ENTER_AMOUNT");
        this.btn_num_dot.Enabled = false;
      }

      else
      {
        this.lbl_price_per_unit.Text = String.Empty;
        gp_digits_box.Text = Resource.String("STR_FRM_AMOUNT_ENTER_MONTO");
        this.btn_num_dot.Enabled = true;
      }
      this.FormTitle = m_concept_name;

      this.txt_amount.Text = String.Empty;

      this.lbl_total_amount.Text = ShowTotal(0);

    } // SetControlsView

    private Boolean InputDataOk(String Description, Currency PricePerUnit)
    {
      if ((PricePerUnit < 0) || (String.IsNullOrEmpty(Description)))
      {
        return false;
      }

      return true;

    } // InputDataOk


    /// <summary>
    /// Returns the currency code  string. If currency is X01 returns the NLS
    /// </summary>
    /// <returns></returns>
    private String CurrencyString()
    {
      switch (uc_payment_method1.CurrencyExchange.Type)
      {
        case CurrencyExchangeType.CASINOCHIP:
        case CurrencyExchangeType.CASINO_CHIP_RE:
        case CurrencyExchangeType.CASINO_CHIP_NRE:
        case CurrencyExchangeType.CASINO_CHIP_COLOR:
          return Resource.String("STR_VOUCHER_OTHER_OPERATIONS_IN_CHIPS");

        case CurrencyExchangeType.CURRENCY:
          return uc_payment_method1.CurrencyExchange.CurrencyCode;

        default:

          return "";
      }

    }

    /// <summary>
    /// Returns a string for -> Total : Amount$ formated whit the nls and the apropiated currency string
    /// </summary>
    /// <returns></returns>
    private String ShowTotal(Currency Amount)
    {
      String _national_currency;

      if (String.IsNullOrEmpty(Amount.CurrencyIsoCode))
      {
        Amount.CurrencyIsoCode = uc_payment_method1.CurrencyExchange.CurrencyCode;
      }
      if (FeatureChips.IsChipsType(uc_payment_method1.CurrencyExchange.Type))
      {
        return Amount.ToString(WSI.Common.Gift.FORMAT_POINTS_DEC.ToString()) + " " + CurrencyString();
      }

      _national_currency = CurrencyExchange.GetNationalCurrency();
      if (Amount.CurrencyIsoCode == _national_currency)
      {
        return ((Currency)Amount).ToString();
      }
      else
      {
        return Currency.Format(Amount, Amount.CurrencyIsoCode);
      }

    }

    /// <summary>
    /// Validate if the amount contains decimals then show a message and retun false. If not contain decimals return true.
    /// </summary>
    /// <returns></returns>
    private bool ConfirmPaiment()
    {
      //if (m_total_amount % 1 != 0 && m_currency_exchange.CurrencyCode == Cage.CHIPS_ISO_CODE)
      //{

      //  frm_message.Show(Resource.String("STR_VOUCHER_OTHER_OPERATIONS_CANT_COLLECT", Resource.String("STR_VOUCHER_OTHER_OPERATIONS_IN_CHIPS")), 
      //                      Resource.String("STR_APP_GEN_MSG_WARNING"),
      //                      MessageBoxButtons.OK,
      //                      MessageBoxIcon.Warning,
      //                      ParentForm);

      //  return false;

      //}

      return true;

    }

    /// <summary>
    /// Shows a windows to add a comment to the concept
    /// </summary>
    private void ShowCommentInput()
    {
      if (GeneralParam.GetBoolean("Cashier.Voucher", "Concept.Comments.Enabled", false)
          && m_total_amount > 0)
      {
        form_yes_no.Show(this.ParentForm);

        string _comment = string.Empty;
        frm_concept_comment _frm = new frm_concept_comment();
        _frm.Show(this, out _comment);
        _frm.Dispose();
        m_result.Comment = _comment;

        form_yes_no.Hide();
      }
    }

    #endregion

    private void uc_payment_method1_CurrencyChanged(CurrencyExchange CurrencyExchange)
    {
      if (Format.GetFormattedDecimals(CurrencyExchange.CurrencyCode) == 0 || m_is_priced_concept)
      {
        btn_num_dot.Enabled = false;
      }
      else
      {
        btn_num_dot.Enabled = true;
      }

      m_total_amount.CurrencyIsoCode = uc_payment_method1.CurrencyExchange.CurrencyCode;

      InitMoneyButtons();

      this.txt_amount.Text = "";
      this.lbl_total_amount.Text = ShowTotal(0);
    }

  }
}