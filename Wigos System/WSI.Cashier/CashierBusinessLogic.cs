//------------------------------------------------------------------------------
// Copyright � 2007-2009 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: CashierBussinesLogic.cs
// 
//   DESCRIPTION: Class to manage an Administrative session of a Card.
//                Manage a Card complete administrative session:
//                Operations:
//                1. New Card credit add.
//                2. Existent Card credit add.
//                3. Card Partial redeem. (Reintegro Cr�dito)
//                4. Card complete redeem. (Reintegro Total)
// 
//        AUTHOR: Andreu Juli�.
// 
// CREATION DATE: 02-AUG-2007
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 09-AUG-2007 AJQ    First release.
// 02-DEC-2009 TJG    Split the cash subtotal to correctly show the subtotal in the new Cash Close voucher layout.
//                    Cash Close voucher has a different distribution of figures than the Cash Close screen.
//                    Card deposits shgould be excluded from the voucher Cash Close subtotal.
// 16-JUN-2010 RCI    Added 'Draw Ticket Print' movement type in routine CreateCashierMovement().
// 22-NOV-2011 JMM    Added 'UpdateCardPin' routine to allow account's PIN change
// 24-NOV-2011 RCI    Read column PinFailures from table Accounts
// 24-NOV-2011 JMM    Random PIN generation added to 'UpdateCardPin'
// 18-JAN-2012 ACC    Update spent used per provider in the play sessions
// 20-FEB-2012 RCI    Routine BalancePartsPreCashout() now takes in account the new NotRedeemable2 amount.
//                    In routine DB_CardCreditAdd(), can add the new NotRedeemable2(CoverCoupon) amount (depending on a General Param).
// 22-FEB-2012 RCI    Split NR1 and NR2 in two fields when creating account operations: routine DB_InsertOperation().
// 09-MAR-2012 MPO    Set the initial balance of the struct TYPE_CASH_REDEEM in CalculateCashAmountWork
// 21-MAR-2012 SSC    Method ReadTerminalId() moved to WSI.Common.Cashier
// 02-APR-2012 JCM    Witholding: Control column length NAME, ID1, ID2 from fields business/ representative
// 13-APR-2012 JCM    Modified methods CreateCard, UpdateCardHolder: Now SqlTransaction is a parameter. 
//                    Modified ACCOUNT_USER_TYPE enum as Public, and addedf the None value.
// 20-APR-2012 JCM    Added Flag: Is_New_Account, Actual account has Recycled the Card
//                    Added Recycle a Card 'DB_CardRecycle'
// 11-MAY-2012 DDM    Modifed method DB_GetReportedBalance, now the reported balance is obtained of ps_reported_balance_mismatch 
//                    in play_sessions. Modifed method CanAddCredit, If is balance mismatch, can not add credit.                                    
// 22-MAY-2012 RCI    Reset authorized user at the uc_card/uc_mobile_bank level, not in internal routines (don't do it in DB_CardCreditAdd()).
// 25-MAY-2012 SSC    Moved CardData, PlaySession and PlayerTrackingData classes to WSI.Common
// 04-JUN-2012 SSC    Moved common part of DB_PromotionRedeem in WSI.Cashier to UpdateAccountsPromotionToReedeemMoved method in WSI.Common
// 19-JUN-2012 JAB    Added GetCashierMovementAddSubAmount and GetCashierMovementBalanceIncrement functions.
// 24-JUL-2012 JAB    Fixed bug: account movements are inserted without "account_id" when you create a new account.
// 06-AUG-2012 DDM    Modified method DB_SaveMajorPrize.
// 07-AUG-2012 JCM    Add Cashier / MB Sales Limit
// 08-AUG-2012 MPO    VoucherOnPromotionCancelled: The Redeemable Promotions to be lost excludes the Prize Coupon.
// 30-AUG-2012 DDM    Modified DB_UpdateCardholder, Checks holder_id1 and holder_id2
// 08-FEB-2013 RRB    Modified DB_UpdateCardholder, set AC_POINTS_STATUS to 0 when the account is personalized. (Anonymous to Personalized)
// 21-FEB-2013 ICS    The following has been moved to WSI.Common:
//                    - Class MBCardData
//                    - Enum MB_SALES_LIMIT_OPERATION
//                    - DB_IsClosedSesionMB
//                    - DB_MBBalanceLost
//                    - The body of DB_InsertMBCardMovement
//                    - The body of DB_MBCloseCashSession
//                    - The body of UpdateSessionBalance
//                    - The body of DB_MBCardGetAllData
//                    - The body of DB_MB_SetNewSalesLimit
// 06-MAR-2013 LEM    Moved ACCOUNT_USER_TYPE to CommonDefinitions.cs
// 06-MAR-2013 LEM    Modified DB_UpdateCardPin and DB_UpdateMBCardHolder to unify in WSI.Common.CardData whith DB_UpdatePinCard and DB_UpdateDataCard
// 07-MAY-2013 JBP    Update DB_CardUpdateInfo, now includes the new field "AC_BLOCK_DESCRIPTION"
// 09-MAY-2013 JBP    Moved DB_CardUpdateInfo () to WSI.Common.CardData and renamed to DB_BlockUnblockCard ()
// 13-JUN-2013 RBG    Change Request #851: Insert Card movement although CardReplacement.card_replacement_price is 0
// 14-JUN-2013 RRR    #854: Set card as paid when account is created and card price is 0
// 25-JUL-2013 DLL    Add currency exchange
// 29-JUL-2013 RCI    Added Anti-Money Laundering support in DB_CardCreditRedeem.
// 13-SEP-2013 QMP    Added Work Shifts
// 11-OCT-2013 RMS    Fixed Bug WIG-272: Update Cashier Session Status when readed from DB (remotely closed session) 
// 17-OCT-2013 JAB    Added functionality. Ask before print voucher when AccountCustomize.Enabled = 2.
// 22-OCT-2013 LEM    Unify saving account and cashier movements using SharedMovementsTables clases. Deleted CreateCashierMovement and DB_InsertCardMovement
// 28-OCT-2013 JAB    Added functionality. Ask before print voucher, when RFC, CURP, Nationality and birthCountry fields are modified.
// 31-OCT-2013 LEM    Fixed Bug WIG-359: Don`t show trackdata for reclycled card in account movement.
// 12-NOV-2013 JML    Fixed Bug WIG-403: Recharge failed to reach the sales limit.
// 14-NOV-2013 LEM    Overloaded DB_DB_PromotionReset, DB_PromotionRedeem to use external transaction and optional don't show popup messages 
// 16-DIC-2013 LEM    CardCreditRedeem: Pass OperationId as parameter, InsertOperation must be done outside
// 17-MAR-2014 JRM    Added new functionality. Description contained in RequestPinCodeDUT.docx DUT
// 16-APR-2014 ICS, MPO & RCI    Fixed Bug WIG-830: Not enough available storage exception.
// 02-JUN-2014 RRR    Fixed Bug WIG-959: PIN required in anonymous accounts when general param disables it
// 02-JUL-2014 AMF    Personal Card Replacement
// 15-JUL-2014 AMF    Fixed Bug WIG-1082: Several errors in the new functionality
// 27-AUG-2014 DRV    Fixed Bug WIG-1211: Chips Sale/Purchase doesn't work with decimals
// 28-AUG-2014 RCI    Fixed Bug WIG-1156: Deposit value when user sale chips only takes in account the last one.
// 18-SEP-2014 DLL    Added new movement Cash Advance
// 03-OCT-2014 SGB    Modified call to VoucherMBCardDepositCash for new footer voucher
// 08-OCT-2014 DHA    Added property to accumulate if a recharge is going to refund without plays
// 03-NOV-2014 DHA    Fixed Bug WIG-1616: if RFC is generic don't update account RFC & CURP.
// 24-NOV-2014 MPO & OPC  LOGRAND CON02: Added IVA calculated (when needed).
// 27-NOV-2014 JBC    Fixed Bug WIG-1762: Close MB Session
// 09-DEC-2014 JMV    DUT - WIGOS 201412: Logrand - Bancos m�viles, Dep�sitos
// 21-JAN-2015 JPJ    Added new operation on mobile bank deposit
// 02-FEB-2015 MPO    WIG-1969: System crash when printing "constancias"
// 18-FEB-2015 DRV    WIG-2062: Error calculating tax base
// 26-FEB-2015 DRV    Fixed Bug WIG-2116: Voucher error on chips purchase with cash out
// 06-MAR32015 YNM    Fixed Bug WIG-2127: TITO: error procesing Payment Order in chips purchase
// 02-APR-2015 DHA    Avoid to send Chips sale register on table operations to MultiSite
// 10-APR-2015 YNM    Fixed Bug WIG-2126: Error OnClose Cashier Session when MB has Shortfall or OverCash and ReopenMode=1. 
// 15-APR-2015 DCS    Fixed Bug WIG-2214: Don't show PopUp printer selection when not is necessary
// 23-APR-2015 YNM    Fixed Bug WIG-2240: Payment Order + Chips Buy take cashier taxes 
// 24-APR-2015 YNM    Fixed Bug WIG-2247: TITO: Payment Order Voucher calculates taxes even when not apply. 
// 07-MAY-2015 YNM    Fixed Bug TFS-286: Cashier ChipsPurchase, taxes should be deducted when payment is with Ticket TITO
// 20-AUG-2015 DCS    Fixed Bug TFS-3840: Don't allow closing cashier session with card replacement in pay mode check
// 29-SEP-2015 DHA    Product Backlog Item 3902: external withholing
// 01-OCT-2015 ETP    Backlog Item 4893 Add new PLD control
// 07-OCT-2015 DLL    Fixed Bug TFS-4692: Holder Name is not showed in bank transaction report
// 27-OCT-2015 RCI    Fixed Bug TFS-5618: Chips sale register broken
// 04-NOV-2015 AVZ    Backlog Item 6145:BonoPLUS: Reserved credit - BonoPLUS Reserved: Cashier:  Mostrar el Reservado en Retiro
// 11-NOV-2015 FOS    Product Backlog Item: 3709 Payment tickets & handpays
// 23-NOV-2015 FOS    Bug 6771:Payment tickets & handpays
// 01-DEC-2015 FOS    Fixed Bug 7205: Protected taxWaiverMode and renamed function ApplyTaxCollection to EnableTitoTaxWaiver
// 02-DEC-2015 FOS    Fixed Bug 7208: Incorrect Tax Value on Handpays
// 15-DIC-2015 YNM,ADI,SA PBI 7503: Visits/Reception Trinidad y Tobago: search screen     
// 24-FEB-2016 EOR    Product Backlog Item 7402:SAS20: Add Comments HandPays
// 24-FEB-2016 EOR    Product Backlog Item 7402:SAS20: Add Comments HandPays
// 18-MAR-2016 YNM    Fixed Bug 10744: With GP requiereNewCard a "0" new account, trackdata is showed like "000-Recycled-Virtual.."
// 22-MAR-2016 YNM    Bug 10930:Reception can�t create new account critical error
// 24-MAR-2016 RAB    Bug 10745:Reception: with the GP requiereNewCard to "0" when assigning a card client is counted as "replacement"
// 29-MAR-2016 RGR    Product Backlog Item 10981:Sprint Review 21 Winions - Mex
// 04-APR-2016 AMF    Bug 11213:Modo TITO - Cambios de tarjeta: n�mero de tarjeta mal informado en las creaciones de nuevas cuentas
// 05-APR-2016 FOS    Fixed Bug 10396:It doesn't show print dialog when witholding documents doesn't generate
// 06-APR-2016 FOS    Fixed Bug 11470:We can print witholding documents without payment orders enabled
// 08-APR-2016 JPJ    Fixed Bug 11470:We can print witholding documents without payment orders enabled
// 20-APR-2016 EOR    Product Backlog Item 11296: Codere - Custody Tax: Implement Devolution Tax Custody
// 11-APR-2016 RAB, JPJ     Fixed Bug 11470:We can print witholding documents without payment orders enabled
// 11-MAY-2016 FAV     Product Backlog Item 12893: Card/Check refund 
// 25-MAY-2016 ETP    Fixed bug 13631: added some logs.
// 01-MAY-2016 ETP    Fixed Bugs 14045 & 14060: associate card not defiened.
// 10-JUN-2016 EOR    Product Backlog Item 13612,13613,13614:Generaci�n de constancias: Refactorizaci�n m�dulo de constancias
// 25-JUL-2016 FJC    PBI 15446:Visitas / Recepci�n: MEJORAS - Cajero - Apertura sesi�n de caja
// 19-SEP-2016 FGB    PBI 15985: Exped System: Cashier: Check if the payment/sale can be done
// 30-SEP-2016 ETP    Bug 18003: Cashier: los vouchers de recarga no reflejan los cambios solicitados por Televisa
// 30-SEP-2016 LTC    PBItem 17964:Televisa - Cashdesk draw (Participation prize) - Configuration
// 07-OCT-2016 LTC    Bug 18718:Gui - Accounts: User with redeemeable amount and having play sessoins, is not posible total redeem
// 14-OCT-2016 RAB    PBI 18098: General params: Automatically add the GP to the system
// 14-OCT-2016 FGB    Bug 19084: Exped: When making a cash advance exceeding the exped limit, a pop-up is displayed with no message and is not allowed to perform the operation.
// 24-OCT-2016 ETP    Fixed Bug 19314 Pin Pad recharge refactoring
// 26-OCT-2016 ETP    Fixed Bug 19314 Pin Pad recharge Wigos Rollback + Pinpad + Wigos Commit
// 08-NOV-2016 ETP    Fixed Bug Bug 20035: Exception out of memory when pinpad is used.
// 30-NOV-2016 JML    PBI 21149: Generation of Withholdind - Direct printing
// 19-DEC-2016 FAV    Bug 21719:CountR: Invalid tax when Cashier.TITOTaxWaiver is enabled
// 19-JAN-2017 FOS    Fixed Bug 22330: Print ticket 2 times, when modify the user data.
// 16-FEB-2017 DPC    Bug 24589:Pago de tarjeta: se tiene en cuenta el impuesto de la empresa B
// 24-MAR-2017 ATB    PBI 25262: Exped - Payment authorisation
// 27-APR-2017 DHA    Bug 26735:WTA-350: wait till cut manual paper on printer
// 21-MAY-2017 ETP    Fixed Bug WIGOS-2997: Added voucher data.
// 10-OCT-2017 JML    PBI 30023:WIGOS-4068 Payment threshold registration - Transaction information
// 19-OCT-2017 JML    Fixed Bug 30321:WIGOS-5924 [Ticket #9450] F�rmula datos tarjeta bancaria (entregado-faltante) - Resumen de sesi�n de caja
// 13-NOV-2017 DHA    Bug 30775:WIGOS-6110 [Ticket #9672] MTY I - Plaza Real - PinPad con error y autorizadas
// 14-NOV-2017 DHA    Bug 30785:WIGOS-6559 [Ticket #10454] Mty I: Error de Cobro Tarjeta Bancaria/PinPad en Sustitucion de tarjeta
// 16-NOV-2017 RAB    Bug 30828:WIGOS-6668 Threshold: Log error when cash advance operation is done with threshold disabled
// 23-JUL-2018 AGS    Bug 33698:WIGOS-13823 [Ticket #15907] Fallo - Lectura de Tarjeta Bancos M�viles Version V03.08.0027
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using WSI.Common;
using WSI.Common.TITO;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Collections;
using System.Drawing;
using WSI.Common.PinPad;
using WSI.Common.ExternalPaymentAndSaleValidation;
using WSI.Common.CreditLines;

namespace WSI.Cashier
{
  #region Public Clases

  public class CardAmountsAndTaxes
  {
    public Currency cash_from_balance;
    public Currency cash_from_initial_cash_in;
    public Currency cash_from_cash_in;
    public Currency cash_from_won;
    public Currency subs_from_won;
    public Currency cash_from_not_redeemable;
    public Currency won_taxes;
  }

  #endregion PublicClases

  #region Enums

  public enum CASHIER_STATUS
  {
    OPEN = 0,
    CLOSE = 1,
    DISABLE = 2,
    ERROR = -1
  };

  //public enum CASH_MODE
  //{
  //  PARTIAL_REDEEM = 0,
  //  TOTAL_REDEEM = 1
  //};

  //public enum PrintMode
  //{
  //  Preview = 0,
  //  Print = 1
  //}

  #endregion Enums

  static class CashierBusinessLogic
  {
    //public struct TYPE_CARD_REPLACEMENT
    //{
    //  // Structure used to print the card replacement voucher
    //  public Int64 account_id;
    //  public string old_track_data;
    //  public string new_track_data;
    //  public Currency card_replacement_price;
    //};

    //public struct TYPE_CARD_HANDPAY
    //{
    //  // Structure used to print the card handpay voucher
    //  public string terminal_name;
    //  public string provider_name;
    //  public Int64 account_id;
    //  public string track_data;
    //  public Currency amount;
    //};

    public struct TYPE_SESSION_STATS
    {
      ////////////  1. Inicial
      //////////public Currency initial_balance;
      //////////public Number total_initial_movs;
      ////////////  2. Entradas
      //////////public Currency total_cash_in;
      //////////public Number total_cash_in_movs;
      ////////////  3. Salidas
      //////////public Currency total_cash_out;
      //////////public Number total_cash_out_movs;
      ////////////  4. Banco Movil
      //////////public Currency total_mb_deposit;
      //////////public Number total_mb_deposit_movs;
      ////////////  5. Depositos
      //////////public Currency total_deposit;
      //////////public Number total_deposit_movs;
      ////////////  6. Retiros
      //////////public Currency total_withdraw;
      //////////public Number total_withdraw_movs;
      ////////////     ---------------------------
      ////////////  7. Total en caja
      //////////public Currency total_in_desk;
      //////////public Number total_in_desk_movs;
      ////////////  
      ////////////  8. Monto actual
      ////////////  -- Is the same as 7. --
      ////////////  9. Pendiente BM
      //////////public Currency total_mb_pending;
      //////////// 10. Monto inicial
      ////////////  -- It's the same as 1. --
      //////////// 11. Depositos
      ////////////  -- It's the same as 5. -- 
      //////////// 12. Retiros
      ////////////  -- It's the same as 6. --
      ////////////     ---------------------------
      //////////// 13. Sub total
      //////////public Currency sub_total;
      //////////// 14. Impuestos
      ////////////  -- It's the same as 18. --
      ////////////     ---------------------------
      //////////// 15. Resultado caja
      //////////public Currency final_balance;
      ////////////
      //////////// 16. No Redimible
      //////////public Currency total_non_redeem;
      //////////public Number total_non_redeem_movs;
      //////////// 17. Premios
      //////////public Currency total_prizes;
      //////////public Number total_prize_movs;
      //////////// 18. Impuestos
      //////////public Currency total_taxes;
      //////////public Number total_taxes_movs;
    };

    // RCI 18-FEB-2011
    public const Int32 DURATION_BUTTONS_ENABLED_AFTER_OPERATION = 30;

    // MPO 23-JAN-2015
    public static Int64 GLB_OperationIdForPrintDocs = 0;

    #region Card Functions

    //------------------------------------------------------------------------------
    // PURPOSE: Find out if a track data is already assigned to an account
    // 
    //  PARAMS:
    //      - INPUT:
    //          - CardTrackData
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - true: an account exists for the provided track data
    //      - false: an account does not exist for the provided track data
    // 
    //   NOTES:
    //
    public static Boolean DB_IsCardDefined(String CardTrackData)
    {
      StringBuilder _sb;
      String internal_card_id;
      Boolean rc;
      Int64 account_id;
      ExternalTrackData _ctd;
      rc = true;
      account_id = 0;


      // Check the card belongs to the site.
      _ctd = new ExternalTrackData(CardTrackData);
      if (!CardData.CheckTrackdataSiteId(_ctd))
      {
        Log.Error("DB_IsCardDefined. Card: " + CardTrackData + " doesn't belong to this site.");

        return false;
      }

      try
      {
        // Obtain internal card id - Get Sql Connection 
        using (DB_TRX _db_trx = new DB_TRX())
        {
        // Get card data
          _sb = new StringBuilder();
          _sb.AppendLine("SELECT   AC_ACCOUNT_ID        ");
          _sb.AppendLine("  FROM   ACCOUNTS             ");
          _sb.AppendLine(" WHERE   AC_TRACK_DATA = @p1  ");

          using (SqlCommand _sql_command = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_command.Parameters.Add("@p1", SqlDbType.NVarChar, 50, "AC_TRACK_DATA").Value = _ctd.InternalTrackData;

            using (SqlDataReader _reader = _sql_command.ExecuteReader())
        {
              if (_reader.Read())
          {
                account_id = _reader.IsDBNull(0) ? 0 : _reader.GetInt64(0);
          }
        }

        // Account does not exist but card is correct.
        // Check for an existent account.
            rc = (account_id != 0);
        }
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      return rc;
    } // DB_IsCardDefined

    /// <summary>
    /// Create a new card with the track data provided.
    /// </summary>
    /// <param name="TrackData">Track data</param>
    /// <returns>True: Successfull
    ///          False: Error</returns>
    public static Boolean DB_CreateCard(String TrackData, out Int64 AccountId, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      SqlCommand sql_command;
      Int32 num_rows_inserted;
      Boolean rc;
      string internal_card_id;
      int card_type;
      SqlParameter _param;
      long _operation_id;
      CashierMovementsTable _cashier_mov_table;
      AccountMovementsTable _account_mov_table;
      CashierSessionInfo _session_info;

      AccountId = 0;

      rc = true;

      if (string.IsNullOrEmpty(TrackData))
      {
        TrackData = "00000000000000000000" + CardData.RECYCLED_FLAG_TRACK_DATA;
      }
      else if (DB_IsCardDefined(TrackData))
      {
        // Card Already exist.
        return false;
      }

      // Obtain internal card identifier from track data
      rc = CardNumber.TrackDataToInternal(TrackData, out internal_card_id, out card_type);
      if (rc)
      {
        try
        {
          _sb = new StringBuilder();

          _sb.AppendLine("EXECUTE CreateAccount @pAccountId OUTPUT  ");

          _sb.AppendLine("UPDATE   ACCOUNTS                         ");

          if (AccountId == 0 && TrackData.Contains(CardData.RECYCLED_FLAG_TRACK_DATA))
          {
            _sb.AppendLine("   SET   AC_TRACK_DATA = @p1 + CONVERT(VARCHAR(19), @pAccountId)");
          }
          else
          {
            _sb.AppendLine("   SET   AC_TRACK_DATA = @p1              ");
          }

          _sb.AppendLine("       , AC_BLOCKED = 0                   ");
          _sb.AppendLine("       , AC_BALANCE = 0                   ");
          _sb.AppendLine(" WHERE   AC_ACCOUNT_ID = @pAccountId      ");

          sql_command = new SqlCommand(_sb.ToString());
          sql_command.Connection = SqlTrx.Connection;
          sql_command.Transaction = SqlTrx;

          sql_command.Parameters.Add("@p1", SqlDbType.NVarChar, 50, "AC_TRACK_DATA").Value = internal_card_id;
          _param = sql_command.Parameters.Add("@pAccountId", SqlDbType.BigInt);
          _param.Direction = ParameterDirection.Output;

          num_rows_inserted = sql_command.ExecuteNonQuery();

          if (num_rows_inserted < 1)
          {
            Log.Error("DB_CreateCard. Card could not be inserted.");
          }
          else
          {
            AccountId = (long)_param.Value;
          }

          if (AccountId == 0)
          {
            TrackData = TrackData + (long)_param.Value;
          }

          // MBF 11-OCT-2011 - Movement for card creation
          Operations.DB_InsertOperation(OperationCode.ACCOUNT_CREATION, AccountId, 0, 0, 0, 0, 0, 0, 0, string.Empty, out _operation_id, SqlTrx);

          _session_info = Cashier.CashierSessionInfo();

          _account_mov_table = new AccountMovementsTable(_session_info);
          _account_mov_table.Add(_operation_id, AccountId, MovementType.CardCreated, 0, 0, 0, 0);
          if (!_account_mov_table.Save(SqlTrx))
          {
            return false;
          }

          _cashier_mov_table = new CashierMovementsTable(_session_info);
          _cashier_mov_table.Add(_operation_id, CASHIER_MOVEMENT.CARD_CREATION, 0, AccountId, TrackData);
          if (!_cashier_mov_table.Save(SqlTrx))
          {
            return false;
          }

          rc = false;
        }
        catch (Exception ex)
        {
          Log.Exception(ex);
        }
      } // if (!error)

      return !rc;

    } // DB_CardCreate

    //------------------------------------------------------------------------------
    // PURPOSE:       Recycle a Card detaching the Card Id from actual Account.
    ///               Release a Card
    // 
    //  PARAMS:
    //
    //      - INPUT:
    //          - TrackData
    //          - AccountId
    //      - OUTPUT:
    //
    // RETURNS: 
    //      - Result of Operation (True -> Ok, False -> Unable Recycle the Card
    // 
    public static Boolean DB_CardRecycle(String TrackData, Int64 AccountId)
    {
      StringBuilder _sb;
      Int32 _num_rows_updated;
      int _card_type;
      String _new_internal_card_id;
      String _old_internal_card_id;
      CashierMovementsTable _cashier_mov_table;
      AccountMovementsTable _account_mov_table;
      CashierSessionInfo _session_info;

      _new_internal_card_id = "";
      _old_internal_card_id = "";

      try
      {
        if (!CardNumber.TrackDataToInternal(TrackData, out _old_internal_card_id, out _card_type))
        {
          return false;
        }

        _new_internal_card_id = _old_internal_card_id + CardData.RECYCLED_FLAG_TRACK_DATA + AccountId;

        using (DB_TRX _db_trx = new DB_TRX())
        {
          //Creates Mov Card & Casher with 0$ of amount
          _session_info = Cashier.CashierSessionInfo();

          _account_mov_table = new AccountMovementsTable(_session_info);
          _account_mov_table.Add(0, AccountId, MovementType.CardRecycled, 0, 0, 0, 0);
          if (!_account_mov_table.Save(_db_trx.SqlTransaction))
          {
            return false;
          }

          _cashier_mov_table = new CashierMovementsTable(_session_info);
          _cashier_mov_table.Add(0, CASHIER_MOVEMENT.CARD_RECYCLED, 0, AccountId, TrackData);
          if (!_cashier_mov_table.Save(_db_trx.SqlTransaction))
          {
            return false;
          }

          _sb = new StringBuilder();
          _sb.AppendLine("UPDATE   ACCOUNTS                         ");
          _sb.AppendLine("   SET   AC_TRACK_DATA  = @pNewTrackData  ");
          _sb.AppendLine(" WHERE   AC_ACCOUNT_ID  = @pAccountId     ");
          _sb.AppendLine("   AND   AC_TRACK_DATA  = @pOldTrackData  ");
          _sb.AppendLine("   AND   AC_BALANCE     = 0               ");

          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString()))
          {
            _sql_cmd.Parameters.Add("@pNewTrackData", SqlDbType.NVarChar, 50).Value = _new_internal_card_id;
            _sql_cmd.Parameters.Add("@pOldTrackData", SqlDbType.NVarChar, 50).Value = _old_internal_card_id;
            _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;

            _num_rows_updated = _db_trx.ExecuteNonQuery(_sql_cmd);
          }

          if (_num_rows_updated != 1)
          {
            Log.Error("DB_CardRecycle. Card not recycled.");

            return false;
          }

          _db_trx.Commit();

          return true;
        }
      } // try
      catch (Exception ex)
      {
        Log.Exception(ex);

        return false;
      }
    } //DB_CardRecycle

    public static ENUM_CARD_RECYCLE_STATUS DB_CardRecycleWithFeeback(String TrackData, Int64 AccountId)
    {
      StringBuilder _sb;
      Int32 _num_rows_updated;
      int _card_type;
      String _new_internal_card_id;
      String _old_internal_card_id;
      CashierMovementsTable _cashier_mov_table;
      AccountMovementsTable _account_mov_table;
      CashierSessionInfo _session_info;
      _new_internal_card_id = "";
      _old_internal_card_id = "";

      try
      {
        if (!CardNumber.TrackDataToInternal(TrackData, out _old_internal_card_id, out _card_type))
        {
          return ENUM_CARD_RECYCLE_STATUS.TrackDataConvertToInternalFailed;
        }

        _new_internal_card_id = _old_internal_card_id + CardData.RECYCLED_FLAG_TRACK_DATA + AccountId;

        using (DB_TRX _db_trx = new DB_TRX())
        {
          //Creates Mov Card & Casher with 0$ of amount
          _session_info = Cashier.CashierSessionInfo();

          _account_mov_table = new AccountMovementsTable(_session_info);
          _account_mov_table.Add(0, AccountId, MovementType.CardRecycled, 0, 0, 0, 0);
          if (!_account_mov_table.Save(_db_trx.SqlTransaction))
          {
            return ENUM_CARD_RECYCLE_STATUS.FailedToCreateAccountsMovement;
          }

          _cashier_mov_table = new CashierMovementsTable(_session_info);
          _cashier_mov_table.Add(0, CASHIER_MOVEMENT.CARD_RECYCLED, 0, AccountId, TrackData);
          if (!_cashier_mov_table.Save(_db_trx.SqlTransaction))
          {
            return ENUM_CARD_RECYCLE_STATUS.FailedToCreateCashierMovement;
          }

          _sb = new StringBuilder();
          _sb.AppendLine("UPDATE ACCOUNTS                         ");
          _sb.AppendLine("   SET AC_TRACK_DATA  = @pNewTrackData  ");
          _sb.AppendLine(" WHERE AC_ACCOUNT_ID  = @pAccountId     ");
          _sb.AppendLine("   AND AC_TRACK_DATA  = @pOldTrackData  ");
          _sb.AppendLine("   AND AC_BALANCE     = 0               ");

          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString()))
          {
            _sql_cmd.Parameters.Add("@pNewTrackData", SqlDbType.NVarChar, 50).Value = _new_internal_card_id;
            _sql_cmd.Parameters.Add("@pOldTrackData", SqlDbType.NVarChar, 50).Value = _old_internal_card_id;
            _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;

            _num_rows_updated = _db_trx.ExecuteNonQuery(_sql_cmd);
          }

          if (_num_rows_updated != 1)
          {
            Log.Error("DB_CardRecycle. Card not recycled.");

            _sb = new StringBuilder();
            _sb.AppendLine("SELECT AC_ACCOUNT_ID, AC_TRACK_DATA, AC_BALANCE ");
            _sb.AppendLine("  FROM ACCOUNTS                                 ");
            _sb.AppendLine(" WHERE AC_ACCOUNT_ID  = @pAccountId             ");

            using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString()))
            {
              _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;

              using (SqlDataReader _reader = _db_trx.ExecuteReader(_sql_cmd))
              {
                if (!_reader.Read()) return ENUM_CARD_RECYCLE_STATUS.InvalidAccount;
                if (_reader.IsDBNull(1)) return ENUM_CARD_RECYCLE_STATUS.TrackDataInvalid;
                if (_reader.IsDBNull(2)) return ENUM_CARD_RECYCLE_STATUS.AccountBalanceIsNull;
                if (!_reader.GetDecimal(2).Equals(0)) return ENUM_CARD_RECYCLE_STATUS.AccountBalanceNotZero;
              }
            }

            return ENUM_CARD_RECYCLE_STATUS.ErrorOccured;
          }

          _db_trx.Commit();

          return ENUM_CARD_RECYCLE_STATUS.Recycled;
        }
      } // try
      catch (Exception ex)
      {
        Log.Exception(ex);

        return ENUM_CARD_RECYCLE_STATUS.ExceptionOccured;
      }
    } //DB_CardRecycle

    public static Boolean DB_CardReplacementUpdateInfo(TYPE_CARD_REPLACEMENT CardReplacement, Boolean CardPaid, frm_card_assign ParentForm)
    {
      CardData _card_data;
      ArrayList _voucher_list;
      Boolean _is_pinpad_recharge;
      frm_tpv _form_tpv;
      PinPadInput _pinpad_input;
      PinPadOutput _pinpad_output;
      Int64 _operation_id;
      ArrayList _voucher_list_tpv;

      // _card_data must be always instanced to avoid compiler errors
      _card_data = new CardData();
      _voucher_list = new ArrayList();
      _pinpad_output = new PinPadOutput();
      _voucher_list_tpv = new ArrayList();

      // Retrieve the account's data in order to get the account's balance.
      // This action must be made before starting any update on the account.
      if (!CardData.DB_CardGetAllData(CardReplacement.account_id, _card_data))
      {
        Log.Error("DB_CardUpdateInfo. Error retrieving account's data." + CardReplacement.account_id.ToString());

        return false;
      }

      _is_pinpad_recharge = Misc.IsPinPadEnabled() && Common.Cashier.ReadPinPadEnabled() && CardReplacement.card_replacement_price > 0 &&
      (CardReplacement.type == PAY_MODE_CARD_REPLACEMENT.CARD_GENERIC || CardReplacement.type == PAY_MODE_CARD_REPLACEMENT.CARD_DEBIT || CardReplacement.type == PAY_MODE_CARD_REPLACEMENT.CARD_CREDIT);

      if (_is_pinpad_recharge)
      {
        if (!InternetConnection.GetServiceInternetConnectionStatus("WCP"))
        {
          frm_message.Show(Resource.String("STR_PINPAD_CANCELLED_OPERATION_ERROR_CONNEXION"), Resource.String("STR_APP_GEN_MSG_WARNING"), MessageBoxButtons.OK, MessageBoxIcon.Warning, ParentForm);
          return false;
        }

        _pinpad_input = new PinPadInput();
        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (!DB_CardReplacementUpdateInfo(CardReplacement, CardPaid, ParentForm, _card_data, out _voucher_list, out _operation_id, _db_trx.SqlTransaction))
          {            
            _db_trx.Rollback();
          }

          _db_trx.Rollback();
        }

        _pinpad_input.Amount = CardReplacement.ExchangeResult;
        _pinpad_input.Card = _card_data;
        _pinpad_input.Amount.CardPrice = CardReplacement.card_replacement_price;
        _pinpad_input.TerminalName = Cashier.CashierSessionInfo().TerminalName;

        _form_tpv = new frm_tpv(_pinpad_input);
        if (!_form_tpv.Show(out _pinpad_output)) // 20/10/2016 ETP: Pin Pad payment
        {
          String _message = String.Empty;

          if (_form_tpv.OperationCanceled)
          {
            _message = Resource.String("STR_PINPAD_CANCELED_BY_USER");
          }
          else
          {
            _message = _pinpad_output.PinPadTransaction.ErrorMessage;
          }

          frm_message.Show(_message,
               Resource.String("STR_APP_GEN_MSG_WARNING"),
               MessageBoxButtons.OK,
               MessageBoxIcon.Warning,
               ParentForm);

          _form_tpv.Dispose();

          return false;
        }

        _form_tpv.Dispose();
      }

      using (DB_TRX _db_trx = new DB_TRX())
      {
        if (!DB_CardReplacementUpdateInfo(CardReplacement, CardPaid, ParentForm, _card_data, out _voucher_list, out _operation_id, _db_trx.SqlTransaction))
        {
          Log.Error("CashierBusinessLogic.DB_CardReplacementUpdateInfo");

          return false;
        }

        if (_is_pinpad_recharge)
        {
          PinPadTransactions _transaction;

          _transaction = _pinpad_output.PinPadTransaction;

          _transaction.OperationId = _operation_id;
          _transaction.StatusCode = PinPadTransactions.STATUS.APPROVED;  //  20/10/2016 ETP: PinPad is payed OK.

          if (!_transaction.DB_Insert(_db_trx.SqlTransaction))
          {
            return false;
          }

          _transaction.UpdateCashierSessionHasPinPadOperation(Cashier.CashierSessionInfo().CashierSessionId, _db_trx.SqlTransaction);

          if (!PinPadTransactions.DeleteFromUndoPinPad(_transaction.ControlNumber, _db_trx.SqlTransaction))
          {
            return false;
          }

          if (!_pinpad_output.PinPadTransaction.CreateVoucher(CardReplacement.ExchangeResult, _pinpad_output, _db_trx.SqlTransaction, out _voucher_list_tpv))
          {
            return false;
          }

          foreach (var _voucher in _voucher_list_tpv)
          {
            _voucher_list.Add(_voucher);
          }
        }

        _db_trx.Commit();
      }

      // Print the voucher
      VoucherPrint.Print(_voucher_list);

      return true;
    } // DB_CardUpdateInfo

    private static Boolean DB_CardReplacementUpdateInfo(TYPE_CARD_REPLACEMENT CardReplacement, Boolean CardPaid, frm_card_assign ParentForm, CardData CardData, out ArrayList VoucherList, out Int64 Operation, SqlTransaction Trx)
    {
      ResultGetMarker _result_get_marker;
      StringBuilder _sb;
      Boolean _card_to_company_b;
      CashierSessionInfo _session_info;
      CashierMovementsTable _cashier_mov_table;
      AccountMovementsTable _account_mov_table;
      Int32 _num_rows_updated;
      OperationCode _operation_code;
      CASHIER_MOVEMENT _cashier_movement;
      MovementType _account_movement;
      Int64 _operation_id;
      Boolean _voucher_generation;
      String _internal_card_id;
      Boolean _rc;
      Int32 _card_type;

      _operation_code = OperationCode.CARD_REPLACEMENT;
      _cashier_movement = CASHIER_MOVEMENT.CARD_REPLACEMENT;
      _account_movement = MovementType.CardReplacement;
      _operation_id = 0;
      _voucher_generation = false;
      _rc = true;
      Operation = 0;
      VoucherList = new ArrayList();

      // Steps:
      //    1. Update account with new card 
      //    2. Insert movement
      //    3. Insert voucher
      //    4. Commit the whole operation


      // Obtain internal card identifier from track data
      _rc = CardNumber.TrackDataToInternal(CardReplacement.new_track_data,
                                             out _internal_card_id,
                                             out _card_type);

      if (!_rc)
      {
        // Error
        return false;
      }

      // Card replacement voucher is produced exclusively for Personal Cards and 
      // only if the Personal Card Replacement price is greater than 0.
      // Generate voucher if price is set to 0.
      if (GeneralParam.GetCurrency("Cashier", "PersonalCardReplacementPrice", 0) >= 0)
      {
        _voucher_generation = true;
      }

      try
      {
        // 1. Update account with new card 
        _sb = new StringBuilder();

        _sb.AppendLine(" UPDATE   ACCOUNTS ");
        _sb.AppendLine("    SET   AC_TRACK_DATA   = @pTrackData ");
        if ((CardReplacement.type == PAY_MODE_CARD_REPLACEMENT.CASH) && (GeneralParam.GetCurrency("Cashier", "PersonalCardReplacementPrice", 0) > 0) && CardPaid)
        {
          _sb.AppendLine("        , AC_CARD_REPLACEMENT_COUNT = ISNULL(AC_CARD_REPLACEMENT_COUNT,0) + 1 ");
        }
        // RAB 24-MAR-2016: Bug 10745: Add set AC_CARD_PAID to 1 when paid the card.
        _sb.AppendLine("        , AC_CARD_PAID    = 1 ");
        _sb.AppendLine("  WHERE   AC_ACCOUNT_ID   = @pAccountId ");

        using (SqlCommand _sql_command = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_command.Parameters.Add("@pTrackData", SqlDbType.NVarChar, 50, "AC_TRACK_DATA").Value = _internal_card_id;
          _sql_command.Parameters.Add("@pAccountId", SqlDbType.BigInt, 8, "AC_ACCOUNT_ID").Value = CardReplacement.account_id;

        _num_rows_updated = _sql_command.ExecuteNonQuery();

        if (_num_rows_updated != 1)
        {
          Log.Error("DB_CardUpdateInfo. Card not updated.");

          return false;
        }
        }

        // 2. Insert movements
        //      - Card movement
        //      - Cashier movement

        _session_info = Cashier.CashierSessionInfo();

        _account_mov_table = new AccountMovementsTable(_session_info);
        _cashier_mov_table = new CashierMovementsTable(_session_info);

        _card_to_company_b = Misc.IsCardPlayerToCompanyB();

        // RAB 24-MAR-2016: Bug 10745: Define three variables according to CardPaid variable.
        _operation_code = CardPaid ? OperationCode.CARD_REPLACEMENT : OperationCode.CARD_ASSOCIATE;

        if (CardReplacement.type != PAY_MODE_CARD_REPLACEMENT.CREDITLINE)
        {
        if (!Operations.DB_InsertOperation(_operation_code, CardReplacement.account_id, Cashier.SessionId, 0,
                                       0, CardReplacement.ExchangeResult.GrossAmount, 0, 0, 0, string.Empty, out _operation_id, Trx))
        {
          return false;
        }
        }

        Operation = _operation_id;

        switch (CardReplacement.type)
        {
          case PAY_MODE_CARD_REPLACEMENT.CASH:
            // RAB 24-MAR-2016: Bug 10745
            _cashier_movement = _card_to_company_b ? CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT : (CardPaid ? CASHIER_MOVEMENT.CARD_REPLACEMENT : CASHIER_MOVEMENT.CARD_ASSOCIATE);
            _account_movement = CardPaid ? MovementType.CardReplacement : MovementType.CardAssociate;
            if (CardReplacement.card_replacement_price == 0)
            {
              _account_mov_table.Add(_operation_id, CardReplacement.account_id, _account_movement, CardData.CurrentBalance, 0, 0,
                                 CardData.CurrentBalance, CardReplacement.old_track_data);
              _cashier_mov_table.Add(_operation_id, _cashier_movement, 0, CardReplacement.account_id,
                                    CardReplacement.new_track_data, CardReplacement.old_track_data);
            }

            break;

          case PAY_MODE_CARD_REPLACEMENT.FREE:
            // RAB 24-MAR-2016: Bug 10745
            _cashier_movement = CardPaid ? CASHIER_MOVEMENT.CARD_REPLACEMENT_FOR_FREE : CASHIER_MOVEMENT.CARD_ASSOCIATE;
            _account_movement = CardPaid ? MovementType.CardReplacementForFree : MovementType.CardAssociate;
            if (CardReplacement.card_replacement_price == 0)
            {
              _account_mov_table.Add(_operation_id, CardReplacement.account_id, _account_movement, CardData.CurrentBalance, 0, 0, CardData.CurrentBalance, CardReplacement.old_track_data);
              _cashier_mov_table.Add(_operation_id, _cashier_movement, 0, CardReplacement.account_id,
                                     CardReplacement.new_track_data, CardReplacement.old_track_data);
            }
            break;

          case PAY_MODE_CARD_REPLACEMENT.POINT:
            // RAB 24-MAR-2016: Bug 10745
            _cashier_movement = CardPaid ? CASHIER_MOVEMENT.CARD_REPLACEMENT_IN_POINTS : CASHIER_MOVEMENT.CARD_ASSOCIATE;
            _account_movement = CardPaid ? MovementType.CardReplacementInPoints : MovementType.CardAssociate;
            _account_mov_table.Add(_operation_id, CardReplacement.account_id, _account_movement, CardData.PlayerTracking.TruncatedPoints, CardReplacement.card_replacement_price, 0,
                                   CardData.PlayerTracking.TruncatedPoints - CardReplacement.card_replacement_price, CardReplacement.old_track_data);
            _cashier_mov_table.Add(_operation_id, _cashier_movement, CardReplacement.card_replacement_price, CardReplacement.account_id,
                                   CardReplacement.new_track_data, CardReplacement.old_track_data);
            break;

          case PAY_MODE_CARD_REPLACEMENT.CHECK:
            // RAB 24-MAR-2016: Bug 10745
            _cashier_movement = _card_to_company_b ? CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CHECK : (CardPaid ? CASHIER_MOVEMENT.CARD_REPLACEMENT_CHECK : CASHIER_MOVEMENT.CARD_ASSOCIATE);
            _account_movement = CardPaid ? MovementType.CardReplacementCheck : MovementType.CardAssociate;
            break;

          case PAY_MODE_CARD_REPLACEMENT.CARD_CREDIT:
            // RAB 24-MAR-2016: Bug 10745
            _cashier_movement = _card_to_company_b ? CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CARD_CREDIT : (CardPaid ? CASHIER_MOVEMENT.CARD_REPLACEMENT_CARD_CREDIT : CASHIER_MOVEMENT.CARD_ASSOCIATE);
            _account_movement = CardPaid ? MovementType.CardReplacementCardCredit : MovementType.CardAssociate;
            break;

          case PAY_MODE_CARD_REPLACEMENT.CARD_DEBIT:
            // RAB 24-MAR-2016: Bug 10745
            _cashier_movement = _card_to_company_b ? CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CARD_DEBIT : (CardPaid ? CASHIER_MOVEMENT.CARD_REPLACEMENT_CARD_DEBIT : CASHIER_MOVEMENT.CARD_ASSOCIATE);
            _account_movement = CardPaid ? MovementType.CardReplacementCardDebit : MovementType.CardAssociate;
            break;

          case PAY_MODE_CARD_REPLACEMENT.CARD_GENERIC:
            // RAB 24-MAR-2016: Bug 10745
            _cashier_movement = _card_to_company_b ? CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CARD_GENERIC : (CardPaid ? CASHIER_MOVEMENT.CARD_REPLACEMENT_CARD_GENERIC : CASHIER_MOVEMENT.CARD_ASSOCIATE);
            _account_movement = CardPaid ? MovementType.CardReplacementCardGeneric : MovementType.CardAssociate;
            break;

          case PAY_MODE_CARD_REPLACEMENT.CREDITLINE:

            #region " CREDIT LINE "

            _result_get_marker = WSI.Cashier.CreditLines.CreditLine.getMarker(CardData, CardReplacement.card_replacement_price, ParentForm,
                                                                            _session_info, CreditLineMovements.CreditLineMovementType.GetMarkerCardReplacement,
                                                                            Trx, out _operation_id);
            if (_result_get_marker != ResultGetMarker.OK)
            {
              return false;
            }

            break;

            #endregion " CREDIT LINE "

          default:
            break;
        }

        if (CardReplacement.type != PAY_MODE_CARD_REPLACEMENT.CREDITLINE)
        {
        if (_cashier_movement != CASHIER_MOVEMENT.CARD_REPLACEMENT_FOR_FREE
          && _cashier_movement != CASHIER_MOVEMENT.CARD_REPLACEMENT_IN_POINTS
          && CardReplacement.card_replacement_price > 0)
        {
          if (_cashier_movement != CASHIER_MOVEMENT.CARD_REPLACEMENT)
          {
            Accounts.CalculateSplit2Tax(CardReplacement.card_replacement_price, out CardReplacement.TotalBaseTaxSplit2, out  CardReplacement.TotalTaxSplit2);
          }
          else
          {
            CardReplacement.TotalTaxSplit2 = -1;
          }

            _account_mov_table.Add(_operation_id, CardReplacement.account_id, _account_movement, CardData.CurrentBalance, 0, CardReplacement.card_replacement_price,
                                   CardData.CurrentBalance, CardReplacement.old_track_data);
          _cashier_mov_table.Add(_operation_id, _cashier_movement, CardReplacement.ExchangeResult.InAmount, CardReplacement.account_id,
                                 CardReplacement.new_track_data, CardReplacement.old_track_data, String.Empty, 0, 0, 0, CardReplacement.TotalTaxSplit2);

          if (CardReplacement.ExchangeResult.Comission > 0)
          {
            _cashier_mov_table.AddSubAmountInLastMov(CardReplacement.ExchangeResult.Comission);
          }

            UpdateSessionBalance(Trx, Cashier.SessionId, CardReplacement.ExchangeResult.InAmount, CardReplacement.ExchangeResult.InCurrencyCode, CardReplacement.ExchangeResult.InType);
        }

          if (!_account_mov_table.Save(Trx))
        {
          return false;
        }

          if (!_cashier_mov_table.Save(Trx))
        {
          return false;
        }

        }
        // 3. Create voucher
        if (_voucher_generation)
        {
          CardReplacement.CashierMovement = _cashier_movement;
          Split.ReadSplitParameters(out CardReplacement.Splits);

          // RAB 24-MAR-2016: Bug 10745: Add CardPaid input parameter to function.
          VoucherList = VoucherBuilder.CardPlayerReplacement(_operation_id, CardReplacement, OperationCode.CARD_REPLACEMENT, PrintMode.Print, Trx, CardPaid);
        }

        CardReplacement.ExchangeResult.BankTransactionData.AccountHolderName = CardData.PlayerTracking.HolderName;
        // 4. Save the bank transaction
        if (CardReplacement.ExchangeResult.BankTransactionData.DocumentNumber != null)
        {
          if (!Accounts.DB_InsertBankTransaction(_operation_id, CardReplacement.ExchangeResult, CardReplacement.new_track_data, Trx))
          {
            Log.Error(Resource.String("STR_GENERIC_ERROR_CASH_ADVANCE"));

            return false;
          }
        }

        if (_voucher_generation)
        {
          if (VoucherList.Count == 0)
          {
            Log.Error("DB_CardUpdateInfo. Error saving voucher." + CardReplacement.account_id.ToString());

            return false;
          }
        }
      } // try
      catch (Exception ex)
      {
        Log.Exception(ex);

        return false;
      }

      return true;
        }

    //------------------------------------------------------------------------------
    // PURPOSE:       Set card paid flag to true when created when card price is 0.
    // 
    //  PARAMS:
    //
    //      - INPUT:
    //          - AccountId
    //          - AccountUserType
    //          - SqlTrx
    //      - OUTPUT:
    //          - CardPaid
    //
    // RETURNS: 
    //      - Result of Operation (True -> Ok, False -> Unable update card paid flag
    // 
    public static Boolean DB_UpdateCardPayAfterCreated(Int64 AccountId, ACCOUNT_USER_TYPE AccountUserType, out Boolean CardPaid, SqlTransaction SqlTrx)
    {
      String _parm_to_check;
      StringBuilder _sb;

      _sb = new StringBuilder();
      CardPaid = false;

      try
      {
        switch (AccountUserType)
        {
          case ACCOUNT_USER_TYPE.ANONYMOUS:
            _parm_to_check = "CardPrice";
            break;

          case ACCOUNT_USER_TYPE.PERSONAL:
            _parm_to_check = "PersonalCardPrice";
            break;

          default:
            return false;
        }

        if (GeneralParam.GetDecimal("Cashier", _parm_to_check) == 0)
        {
          // update card payment info
          _sb.Length = 0;
          _sb.AppendLine("UPDATE   ACCOUNTS ");
          _sb.AppendLine("   SET   AC_CARD_PAID  = 1 ");
          _sb.AppendLine(" WHERE   AC_ACCOUNT_ID = @pAccountId ");
          _sb.AppendLine("   AND   AC_CARD_PAID  = 0 ");

          using (SqlCommand _sql_command = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
          {
            _sql_command.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;

            if (_sql_command.ExecuteNonQuery() != 1)
            {
              Log.Error("DB_UpdateCardPayAfterCreated. Card Payment not updated. AccountId: " + AccountId.ToString());

              return false;
            }
          }

          CardPaid = true;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }//DB_UpdateCardPayAfterCreated

    // DB_CardCreditAdd moved to AccountaddCredit.cs
    /// <summary>
    /// Subtract cash from card: The amount to redeem is substracted from initial cash in
    /// and the rest is taken from won amount. (+ taxes to make achieve the amount to redeem)
    /// Redeem amount priority order:
    ///   3.1 Credit.
    ///   3.2 Not redemable credit.
    ///   3.3 Prize Amount.
    /// </summary>
    /// <param name="AccountId"></param>
    /// <param name="AmountToSubstract">Initial Cash In amount to substract.</param>
    /// <param name="CashMode">PARTIAL or TOTAL REDEEM.</param>
    /// <returns>true:  Amount substracted.
    ///          false: Error substracting amount. </returns>
    ///          
    public static Boolean DB_CardCreditRedeem(Int64 OperationId,
                                              Int64 AccountId,
                                              WithholdData Withhold,
                                              PaymentOrder PayOrder,
                                              Currency AmountToSubstract,
                                              CASH_MODE CashMode,
                                              OperationCode OpCode,
                                              SqlTransaction SqlTrx,
                                              out String ErrorMessage,
                                              Boolean IsApplyTaxes,
                                              out ArrayList VoucherList,
                                              ExternalValidationInputParameters ExpedInputParams = null,
                                              ExternalValidationOutputParameters ExpedOutputParams = null)
    {
      return DB_CardCreditRedeem(OperationId
                               , AccountId
                               , Withhold
                               , PayOrder
                               , AmountToSubstract
                               , CashMode
                               , null
                               , CreditRedeemSourceOperationType.DEFAULT
                               , null
                               , OpCode
                               , SqlTrx
                               , out ErrorMessage
                               , IsApplyTaxes
                               , out VoucherList
                               , ExpedInputParams
                               , ExpedOutputParams);
    }

    public static Boolean DB_CardCreditRedeem(Int64 OperationId,
                                              Int64 AccountId,
                                              WithholdData Withhold,
                                              PaymentOrder PayOrder,
                                              Currency AmountToSubstract,
                                              CASH_MODE CashMode,
                                              CreditRedeemSourceOperationType OperationType,
                                              OperationCode OpCode,
                                              CardData CardForChipsPurchaseWithCashOut,
                                              SqlTransaction SqlTrx,
                                              out String ErrorMessage,
                                              Boolean IsApplyTaxes,
                                              out ArrayList VoucherList)
    {
      return DB_CardCreditRedeem(OperationId
                               , AccountId
                               , Withhold
                               , PayOrder
                               , AmountToSubstract
                               , CashMode
                               , null
                               , OperationType
                               , CardForChipsPurchaseWithCashOut
                               , OpCode
                               , SqlTrx
                               , out ErrorMessage
                               , IsApplyTaxes
                               , out VoucherList);
    }

    public static Boolean DB_CardCreditRedeem(Int64 OperationId,
                                              Int64 AccountId,
                                              WithholdData Withhold,
                                              PaymentOrder PayOrder,
                                              Currency AmountToSubstract,
                                              CASH_MODE CashMode,
                                              CreditRedeemSourceOperationType OperationType,
                                              CardData CardForChipsPurchaseWithCashOut,
                                              SqlTransaction SqlTrx,
                                              out String ErrorMessage,
                                              Boolean IsApplyTaxes,
                                              out ArrayList VoucherList)
    {
      return DB_CardCreditRedeem(OperationId
                               , AccountId
                               , Withhold
                               , PayOrder
                               , AmountToSubstract
                               , CashMode
                               , null
                               , OperationType
                               , CardForChipsPurchaseWithCashOut
                               , OperationCode.NOT_SET
                               , SqlTrx
                               , out ErrorMessage
                               , IsApplyTaxes
                               , out VoucherList);
    }

    public static Boolean DB_CardCreditRedeem(Int64 OperationId,
                                              Int64 AccountId,
                                              WithholdData Withhold,
                                              PaymentOrder PayOrder,
                                              Currency AmountToSubstract,
                                              CASH_MODE CashMode,
                                              List<VoucherValidationNumber> ValidationNumbers,
                                              OperationCode OpCode,
                                              SqlTransaction SqlTrx,
                                              out String ErrorMessage,
                                              Boolean IsApplyTaxes,
                                              out ArrayList VoucherList)
    {
      return DB_CardCreditRedeem(OperationId, AccountId, Withhold, PayOrder, AmountToSubstract, CashMode, ValidationNumbers,
                                 CreditRedeemSourceOperationType.DEFAULT, null, OpCode, SqlTrx,
                                 out ErrorMessage, IsApplyTaxes, out VoucherList);
    }

    public static Boolean DB_CardCreditRedeem(Int64 OperationId,
                                              Int64 AccountId,
                                              WithholdData Withhold,
                                              PaymentOrder PayOrder,
                                              Currency AmountToSubstract,
                                              CASH_MODE CashMode,
                                              List<VoucherValidationNumber> ValidationNumbers,
                                              CreditRedeemSourceOperationType OperationType,
                                              CardData SourceCardData,
                                              OperationCode OpCode,
                                              SqlTransaction SqlTrx,
                                              out String ErrorMessage,
                                              Boolean IsApplyTaxes,
                                              out ArrayList VoucherList,
                                              ExternalValidationInputParameters ExpedInputParams = null,
                                              ExternalValidationOutputParameters ExpedOutputParams = null)
    {
      CashierSessionInfo _session;
      Boolean _witholding_needed;
      Currency _total_taxes;

      _session = Cashier.CashierSessionInfo();

      // MPO 23-JAN-2015
      // Global for print docs if needed

      GLB_OperationIdForPrintDocs = 0;

      if (WSI.Common.AccountsRedeem.DB_CardCreditRedeem(AccountId, Withhold, PayOrder, AmountToSubstract, CashMode, ValidationNumbers,
                                        OperationType, SourceCardData, OpCode, SqlTrx, IsApplyTaxes,
                                      _session, out _total_taxes, out VoucherList, out _witholding_needed, out ErrorMessage, ref OperationId,
                                      ExpedInputParams, ExpedOutputParams))
      {
        if ((_witholding_needed && Witholding.WitholdingDocumentModeGenerate() == Witholding.WITHHOLDING_PRINT_MODE.IMMEDIATE) || PayOrder.CheckPayment > 0) // EOR 10-JUN-2016
        {
          GLB_OperationIdForPrintDocs = OperationId;
        }

        return true;
      }

      Log.Error("DB_CardCreditRedeem: Error Card Redeem");

      return false;
    } // DB_CardCreditRedeem

    public static Boolean DB_CardCreditRedeem(Int64 AccountId,
                                          WithholdData Withhold,
                                          PaymentOrder PayOrder,
                                          Currency AmountToSubstract,
                                          OperationCode OpCode,
                                          CASH_MODE CashMode,
                                              out String ErrorMessage,
                                          Boolean IsApplyTaxes,
                                          String HolderName = "")
    {
      ArrayList _voucher_list;

      ErrorMessage = string.Empty;
      _voucher_list = new ArrayList();

      // If exped is active, we need to request authorization to refund
      if (ExternalPaymentAndSaleValidationCommon.ExpedMode == EnumExternalPaymentAndSaleValidationMode.MODE_EXPED)
      {
        if (ExternalWS.ExpedConnect(AccountId, AmountToSubstract, OpCode))
        {
          using (DB_TRX _db_trx = new DB_TRX())
          {
            if (!DB_CardCreditRedeem(0,
                                     AccountId,
                                     Withhold,
                                     PayOrder,
                                     AmountToSubstract,
                                     CashMode,
                                     OpCode,
                                     _db_trx.SqlTransaction,
                                     out ErrorMessage,
                                     IsApplyTaxes,
                                     out _voucher_list,
                                     Misc.ExternalWS.InputParams,
                                     Misc.ExternalWS.OutputParams))
            {
              frm_message.Show(ExternalPaymentAndSaleValidationCommon.GetErrorMessageFromExternalProvider(AccountId, HolderName, AmountToSubstract),
                           Resource.String("STR_APP_GEN_MSG_ERROR"),
                           MessageBoxButtons.OK,
                           MessageBoxIcon.Error,
                           Cashier.MainForm);

              return false;
            }

            _db_trx.Commit();

            return true;
          }
        }

        if (ExternalPaymentAndSaleValidationCommon.ExpedMode != EnumExternalPaymentAndSaleValidationMode.MODE_EXPED
            || Misc.ExternalWS.OutputParams.ValidationResult != EnumExternalPaymentAndSaleValidationResult.RESULT_AUTHORIZED)
        {
          frm_message.Show(ExternalPaymentAndSaleValidationCommon.GetErrorMessageFromExternalProvider(AccountId, HolderName, AmountToSubstract),
               Resource.String("STR_APP_GEN_MSG_ERROR"),
               MessageBoxButtons.OK,
               MessageBoxIcon.Error,
               Cashier.MainForm);
        }

        return false;
      }
      else
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (!DB_CardCreditRedeem(0,
                                   AccountId,
                                   Withhold,
                                   PayOrder,
                                   AmountToSubstract,
                                   CashMode,
                                   OpCode,
                                   _db_trx.SqlTransaction,
                                   out ErrorMessage,
                                   IsApplyTaxes,
                                   out _voucher_list))
          {
            return false;
          }

          _db_trx.Commit();
        }
      }

      VoucherPrint.Print(_voucher_list);

      if (CashierBusinessLogic.GLB_OperationIdForPrintDocs <= 0)
      {
        return true;
      }

      if (PayOrder.AccountId > 0) //PaymentOrder
      {
        if (PaymentOrder.IsGenerateDocumentEnabled())
        {
          PrintingDocuments.PrintDialogDocuments(CashierBusinessLogic.GLB_OperationIdForPrintDocs);
        }
      }
      else  //Witholding
      {
        PrintingDocuments.PrintDialogDocuments(CashierBusinessLogic.GLB_OperationIdForPrintDocs);

        return true;
      }

      return true;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Checks whether there already exists another account (different than 
    //           AccountID) with same name as Name
    // 
    //  PARAMS :
    //      - INPUT :
    //          - AccountID
    //          - Name
    //
    //      - OUTPUT :
    //
    // RETURNS :
    // 
    //   NOTES :

    internal static bool DB_CheckExistHolderName(UInt64 AccountID, String Name)
    {
      StringBuilder _sb;
      int _count;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _sb = new StringBuilder();
          _sb.AppendLine("SELECT   COUNT(*)                                     ");
          _sb.AppendLine("  FROM   ACCOUNTS                                     ");
          _sb.AppendLine(" WHERE   AC_ACCOUNT_ID <> @pAccountID                 ");
          _sb.AppendLine("   AND   { fn UCASE(AC_HOLDER_NAME) } = @pHolderName  ");

          using (SqlCommand sql_command = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
        sql_command.Parameters.Add("@pHolderName", SqlDbType.NVarChar, 200, "AC_HOLDER_NAME").Value = Name;
        sql_command.Parameters.Add("@pAccountID", SqlDbType.BigInt, 8, "AC_ACCOUNT_ID").Value = AccountID;

            _count = (int)sql_command.ExecuteScalar();

            return (_count > 0);
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // DB_CheckExistHolderName

    //------------------------------------------------------------------------------
    // PURPOSE : Checks whether there already exists another account (different than 
    //           AccountID) with same name HolderId1 and/or HolderId2
    // 
    //  PARAMS :
    //      - INPUT :
    //          - AccountID : Account Id 
    //          - HolderId1 : Holder Id1 (= RFC). Not checked when empty.
    //          - HolderId2 : Holder Id2 (= CURP). Not checked when empty.
    //
    //      - OUTPUT :
    //
    // RETURNS :
    // 
    //   NOTES :

    internal static bool DB_CheckExistHolderId(UInt64 AccountID,
                                                String HolderId1,
                                                String HolderId2)
    {
      StringBuilder _sb;
      int _count;
      String _holder_id1;
      String _holder_id2;

      try
      {
        // Clean the received holder Ids
        _holder_id1 = HolderId1.Trim().ToUpperInvariant();
        _holder_id2 = HolderId1.Trim().ToUpperInvariant();

        if (_holder_id1.Length == 0 && _holder_id2.Length == 0)
        {
          // No Ids: it should not happen => log the error! 
          return false;
        }

        //Get sql
        _sb = new StringBuilder();
        _sb.AppendLine("SELECT COUNT(*)");
        _sb.AppendLine("  FROM ACCOUNTS");
        _sb.AppendLine(" WHERE AC_ACCOUNT_ID <> @pAccountID");

        if (_holder_id1.Length > 0)
        {
          _sb.AppendLine("  AND { fn UCASE(AC_HOLDER_ID1) } = @pHolderID1");
        }

        if (_holder_id2.Length > 0)
        {
          _sb.AppendLine("  AND { fn UCASE(AC_HOLDER_ID2) } = @pHolderID2");
        }

        // Get Sql Connection 
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
        _sql_cmd.Parameters.Add("@pAccountID", SqlDbType.BigInt).Value = AccountID;

        if (_holder_id1.Length > 0)
        {
          _sql_cmd.Parameters.Add("@pHolderID1", SqlDbType.NVarChar).Value = _holder_id1;
        }

        if (_holder_id2.Length > 0)
        {
          _sql_cmd.Parameters.Add("@pHolderID2", SqlDbType.NVarChar).Value = _holder_id2;
        }

        _count = (int)_sql_cmd.ExecuteScalar();
          }
        }

        if (_count > 0)
        {
          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // DB_CheckExistHolderId

    //------------------------------------------------------------------------------
    // PURPOSE : Checks if the account data that is printed to the voucher has changed
    // 
    //  PARAMS :
    //      - INPUT :
    //          - Account : Edited account
    //          - AccountRead : Original account
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - TRUE
    //      - FALSE
    // 
    //   NOTES :
    internal static bool HasTicketChanges(CardData Account, CardData AccountRead)
    {
      String _account_customize_mode;
      Boolean _allow;

      _allow = false;
      _account_customize_mode = GeneralParam.GetString("Cashier.Voucher", "AccountCustomize.Enabled");

      if (_account_customize_mode == "0")
      {
        return _allow;
      }

      // "Los documentos de Constancia de Retenci�n de Impuestos fueron generados correctamente pero no fueron impresos."

      // Check holder name
      if (Account.PlayerTracking.HolderName1 != AccountRead.PlayerTracking.HolderName1
        || Account.PlayerTracking.HolderName2 != AccountRead.PlayerTracking.HolderName2
        || Account.PlayerTracking.HolderName3 != AccountRead.PlayerTracking.HolderName3)
      {
        _allow = true;
      }

      // Check RFC and CURP
      if (Account.PlayerTracking.HolderId1 != AccountRead.PlayerTracking.HolderId1
        || Account.PlayerTracking.HolderId2 != AccountRead.PlayerTracking.HolderId2)
      {
        _allow = true;
      }

      // Check Nationality and birthCountry
      if (Account.PlayerTracking.HolderNationality != AccountRead.PlayerTracking.HolderNationality
        || Account.PlayerTracking.HolderBirthCountry != AccountRead.PlayerTracking.HolderBirthCountry)
      {
        _allow = true;
      }

      // Check address
      if (Account.PlayerTracking.HolderAddress01 != AccountRead.PlayerTracking.HolderAddress01
        || Account.PlayerTracking.HolderAddress02 != AccountRead.PlayerTracking.HolderAddress02
        || Account.PlayerTracking.HolderAddress03 != AccountRead.PlayerTracking.HolderAddress03
        || Account.PlayerTracking.HolderExtNumber != AccountRead.PlayerTracking.HolderExtNumber
        || Account.PlayerTracking.HolderCity != AccountRead.PlayerTracking.HolderCity
        || Account.PlayerTracking.HolderZip != AccountRead.PlayerTracking.HolderZip
        || Account.PlayerTracking.HolderFedEntity != AccountRead.PlayerTracking.HolderFedEntity)
      {
        _allow = true;
      }

      // Check Phone number
      if (Account.PlayerTracking.HolderPhone01 != AccountRead.PlayerTracking.HolderPhone01
        || Account.PlayerTracking.HolderPhone02 != AccountRead.PlayerTracking.HolderPhone02)
      {
        _allow = true;
      }

      // Check Occupation
      if (Account.PlayerTracking.HolderOccupationId != AccountRead.PlayerTracking.HolderOccupationId)
      {
        _allow = true;
      }

      // Check if the holderHasBeneficiary check has changed
      if (Account.PlayerTracking.HolderHasBeneficiary != AccountRead.PlayerTracking.HolderHasBeneficiary)
      {
        _allow = true;
      }

      // Check beneficiary name if the beneficiary isn't the same as the holder
      if (Account.PlayerTracking.HolderHasBeneficiary
        && (Account.PlayerTracking.BeneficiaryName1 != AccountRead.PlayerTracking.BeneficiaryName1
         || Account.PlayerTracking.BeneficiaryName2 != AccountRead.PlayerTracking.BeneficiaryName2
         || Account.PlayerTracking.BeneficiaryName3 != AccountRead.PlayerTracking.BeneficiaryName3))
      {
        _allow = true;
      }

      // Check document type and document id
      if (Account.PlayerTracking.HolderIdType != AccountRead.PlayerTracking.HolderIdType
       || Account.PlayerTracking.HolderId != AccountRead.PlayerTracking.HolderId)
      {
        _allow = true;
      }

      if (_allow && (_account_customize_mode == "2"))
      {
        if (frm_message.Show(Resource.String("STR_FRM_ASK_BEFORE_PRINT"),
                             Resource.String("STR_APP_GEN_MSG_WARNING"),
                             MessageBoxButtons.YesNo,
                             MessageBoxIcon.Question,
                             Cashier.MainForm, true) == DialogResult.Cancel)
        {
          _allow = false;
        }
      }

      // Don't allow print voucher if there has been no modification in the voucher data
      return _allow;
    } // HasTicketChanges

    internal static Boolean DB_UpdateCardholder(CardData Account, CardData AccountRead, Boolean SaveDocuments, Boolean PrintInformation, SqlTransaction SqlTrx)
    {
      Int64 _operation_id;
      CashierMovementsTable _cashier_movement;
      Boolean _voucher_saved;
      VoucherCardHolderUpdate _voucher;

      try
      {
        if (!WSI.Common.CardData.DB_UpdateDataCard(Account, Cashier.CashierSessionInfo(), SaveDocuments, out _operation_id, SqlTrx))
        {
          return false;
        }

        _cashier_movement = new CashierMovementsTable(Cashier.CashierSessionInfo());

        if (!_cashier_movement.Add(_operation_id, CASHIER_MOVEMENT.CARD_PERSONALIZATION, 0, Account.AccountId, Account.TrackData))
        {
          return false;
        }

        if (!_cashier_movement.Save(SqlTrx))
        {
          return false;
        }

        // btn_print_information
        if (PrintInformation)
        {
          return true;
        }

        // vocher by changing some fields
        if (HasTicketChanges(Account, AccountRead))
        {
          _voucher = new VoucherCardHolderUpdate(Account, OperationCode.ACCOUNT_PERSONALIZATION, PrintMode.Print, SqlTrx);
          _voucher.AddString("VOUCHER_CARD_HOLDERNAME", Account.PlayerTracking.HolderName);

          _voucher_saved = _voucher.Save(_operation_id, SqlTrx);

          if (!_voucher_saved)
          {
            Log.Error("DB_UpdateDataCard. Error saving voucher. CardHolderUpdate: " + Account.AccountId);

            return false;
          }

          VoucherPrint.Print(_voucher);
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // DB_UpdateCardholder

    //------------------------------------------------------------------------------
    // PURPOSE : Reset Holder Level Notify to the Card in DB and Memory.
    //
    //  PARAMS :
    //      - INPUT :
    //          - CardData CardData
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - true: data was updated successfully
    //      - false: data was not updated

    internal static Boolean DB_ResetHolderLevelNotify(CardData CardData)
    {
      // RCI 11-NOV-2010

      SqlConnection _sql_conn;
      SqlTransaction _sql_trx;
      SqlCommand _sql_cmd;
      String _sql_str;
      Int32 _num_rows_updated;

      _sql_conn = null;
      _sql_trx = null;

      try
      {
        // Get Sql Connection 
        _sql_conn = WGDB.Connection();
        _sql_trx = _sql_conn.BeginTransaction();

        _sql_str = "";
        _sql_str += "UPDATE   ACCOUNTS ";
        _sql_str += "   SET   AC_HOLDER_LEVEL_NOTIFY = 0 ";
        _sql_str += " WHERE   AC_ACCOUNT_ID          = @pAccountId ";
        _sql_str += "   AND   AC_HOLDER_LEVEL_NOTIFY = @pOldLevelNotify ";

        _sql_cmd = new SqlCommand(_sql_str);
        _sql_cmd.Connection = _sql_trx.Connection;
        _sql_cmd.Transaction = _sql_trx;

        _sql_cmd.Parameters.Add("@pOldLevelNotify", SqlDbType.Int).Value = CardData.PlayerTracking.HolderLevelNotify;
        _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = CardData.AccountId;

        // Update DB Holder Level Notify value.
        _num_rows_updated = _sql_cmd.ExecuteNonQuery();

        if (_num_rows_updated != 1)
        {
          Log.Error("DB_SetHolderLevelNotify. Card Level Notify not updated.");
          return false;
        }

        // Update Memory Holder Level Notify value.
        CardData.PlayerTracking.HolderLevelNotify = 0;

        _sql_trx.Commit();

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return false;
      }
      finally
      {
        if (_sql_trx != null)
        {
          if (_sql_trx.Connection != null)
          {
            try { _sql_trx.Rollback(); }
            catch { }
          }

          _sql_trx.Dispose();
          _sql_trx = null;
        }

        // Close connection
        if (_sql_conn != null)
        {
          try { _sql_conn.Close(); }
          catch { }
          _sql_conn = null;
        }
      }
    } // DB_SetHolderLevelNotify

    //------------------------------------------------------------------------------
    // PURPOSE: Update card info: Pin.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Account
    //          - Pin (ignored if RandomPIN is true)
    //          - OldPin
    //          - RandomPIN
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - true: data was updated successfully
    //      - false: data was not updated
    // 
    //   NOTES:

    internal static Boolean DB_UpdateCardPin(CardData Card, String Pin, String OldPin, Boolean RandomPIN)
    {
      ArrayList _voucher_list;
      Int64 _operation_id;
      CashierMovementsTable _cashier_movement;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (!WSI.Common.CardData.DB_UpdatePinCard(Card, Cashier.CashierSessionInfo(), Pin, OldPin, RandomPIN,
                                                    out _voucher_list, out _operation_id, _db_trx.SqlTransaction))
          {
            return false;
          }

          _cashier_movement = new CashierMovementsTable(Cashier.CashierSessionInfo());

          if (!_cashier_movement.Add(_operation_id, RandomPIN ? CASHIER_MOVEMENT.ACCOUNT_PIN_RANDOM : CASHIER_MOVEMENT.ACCOUNT_PIN_CHANGE, 0, Card.AccountId, Card.TrackData))
          {
            return false;
          }

          if (!_cashier_movement.Save(_db_trx.SqlTransaction))
          {
            return false;
          }

          _db_trx.Commit();

          if (RandomPIN)
          {
            VoucherPrint.Print(_voucher_list);
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
    } // DB_UpdateCardPin

    #endregion Card Functions

    #region Mobile Bank Functions

    //------------------------------------------------------------------------------
    // PURPOSE: Find out if a track data is already assigned to an MB account
    // 
    //  PARAMS:
    //      - INPUT:
    //          - CardTrackData
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - true: an MB account exists for the provided track data
    //      - false: an MB account does not exist for the provided track data
    // 
    //   NOTES:
    //
    public static Boolean DB_IsMBCardDefined(String CardTrackData)
    {
      SqlConnection sql_conn;
      SqlTransaction sql_trx;
      SqlCommand sql_command;
      SqlDataReader reader;
      String internal_card_id;
      String sql_str;
      Boolean rc;
      Int64 account_id;
      ExternalTrackData _ctd;

      sql_conn = null;
      sql_trx = null;
      rc = true;
      account_id = 0;


      // Check the card belongs to the site.
      _ctd = new ExternalTrackData(CardTrackData);
      if (!CardData.CheckTrackdataSiteId(_ctd))
      {
        Log.Error("DB_IsMBCardDefined. Card: " + CardTrackData + " doesn't belong to this site.");

        return false;
      }

      try
      {
        // Obtain internal card id - Get Sql Connection 
        sql_conn = WGDB.Connection();
        sql_trx = sql_conn.BeginTransaction();

        // Get card data
        sql_str = "SELECT MB_ACCOUNT_ID " +
                    "FROM MOBILE_BANKS " +
                   "WHERE MB_TRACK_DATA = @p1 ";

        sql_command = new SqlCommand(sql_str);
        sql_command.Connection = sql_trx.Connection;
        sql_command.Transaction = sql_trx;
        sql_command.Parameters.Add("@p1", SqlDbType.NVarChar, 50, "MB_TRACK_DATA").Value = _ctd.InternalTrackData;

        reader = null;

        try
        {
          reader = sql_command.ExecuteReader();
          if (reader.Read() && !reader.IsDBNull(0))
          {
            account_id = (Int64)reader[0];
          }
        }
        catch (Exception ex)
        {
          Log.Exception(ex);
        }
        finally
        {
          if (reader != null)
          {
            reader.Close();
            reader.Dispose();
            reader = null;
          }
        }

        // Account does not exist but card is correct.
        sql_trx.Commit();

        // Check for an existent account.
        if (account_id != 0)
        {
          rc = true;
        }
        else
        {
          rc = false;
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
      finally
      {
        if (sql_trx != null)
        {
          if (rc && sql_trx.Connection != null)
          {
            sql_trx.Rollback();
          }

          sql_trx.Dispose();
          sql_trx = null;
        }

        // Close connection
        if (sql_conn != null)
        {
          sql_conn.Close();
          sql_conn = null;
        }
      }

      return rc;
    } // DB_IsMBCardDefined

    //------------------------------------------------------------------------------
    // PURPOSE: Retrieve all relevant data from a mobile bank account
    // 
    //  PARAMS:
    //      - INPUT:
    //          - CardTrackData
    //
    //      - OUTPUT:
    //          - CardInfo
    //
    // RETURNS:
    //      - true: provided track data format is correct (an account may exist or not)
    //      - false: provided track data is not formatted accoring to the standards
    // 
    //   NOTES:
    //      - If an account does not exist but track data is correct then the account id
    //      returned in the CardInfo structure is zero.

    public static Boolean DB_MBCardGetAllData(String CardTrackData,
                                              MBCardData CardInfo)
    {
      SqlConnection sql_conn;
      SqlTransaction sql_trx;
      SqlCommand sql_command;
      SqlDataReader reader;
      String sql_str;
      Boolean rc;
      ExternalTrackData _ctd;

      sql_conn = null;
      sql_trx = null;
      rc = true;

      // Initialize card data
      CardInfo.CardId = 0;



      // Check the card belong to the site.
      _ctd = new ExternalTrackData(CardTrackData);
      if (!CardData.CheckTrackdataSiteId(new ExternalTrackData(CardTrackData)))
      {
        Log.Error("DB_MBCardGetAllData. Card: " + CardTrackData + " doesn't belong to this site.");

        return true;
      }

      try
      {
        // Obtain internal card id - Get Sql Connection 
        sql_conn = WGDB.Connection();
        sql_trx = sql_conn.BeginTransaction();

        // Get card data
        sql_str = "SELECT MB_ACCOUNT_ID " +
                   " FROM MOBILE_BANKS " +
                   " WHERE MB_TRACK_DATA = @p1 ";
        if (MobileBankConfig.IsMobileBankLinkedToADevice)
        { 
          sql_str += " AND MB_USER_ID IS NOT NULL"; 
        }

        sql_command = new SqlCommand(sql_str);
        sql_command.Connection = sql_trx.Connection;
        sql_command.Transaction = sql_trx;
        sql_command.Parameters.Add("@p1", SqlDbType.NVarChar, 50, "MB_TRACK_DATA").Value = _ctd.InternalTrackData;

        reader = null;

        try
        {
          reader = sql_command.ExecuteReader();
          if (reader.Read() && !reader.IsDBNull(0))
          {
            CardInfo.CardId = (Int64)reader[0];
          }
        }
        catch (Exception ex)
        {
          Log.Exception(ex);
        }
        finally
        {
          if (reader != null)
          {
            reader.Close();
            reader.Dispose();
            reader = null;
          }
        }

        // MB Account may not exist but card is correct.
        sql_trx.Commit();
        rc = false;

        // Check for an existent account.
        if (CardInfo.CardId != 0)
        {
          if (!DB_MBCardGetAllData(CardInfo.CardId, CardInfo))
          {
            Log.Error("DB_MBCardGetAllData. DB_MBCardGetAllData: Error obtaining MB card full data.");

            rc = true;
          }
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
      finally
      {
        if (sql_trx != null)
        {
          if (rc && sql_trx.Connection != null)
          {
            sql_trx.Rollback();
          }

          sql_trx.Dispose();
          sql_trx = null;
        }

        // Close connection
        if (sql_conn != null)
        {
          sql_conn.Close();
          sql_conn = null;
        }
      }

      return !rc;
    } // DB_MBCardGetAllData

    //------------------------------------------------------------------------------
    // PURPOSE: Retrieve all relevant data related to an account from the database
    //          using the account identifier.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - CardId
    //
    //      - OUTPUT:
    //          - CardInfo
    //
    // RETURNS:
    //      - true: data was retrieved successfully
    //      - false: data was not retrieved
    // 
    //   NOTES:
    //

    /// <summary>
    /// Get Card All Data from Card Id.
    /// </summary>
    /// <param name="CardId"></param>
    /// <param name="CardInfo"></param>
    /// <returns></returns>
    public static Boolean DB_MBCardGetAllData(Int64 CardId,
                                              MBCardData CardInfo)
    {
      return MobileBank.DB_MBCardGetAllData(CardId, CardInfo, Cashier.CashierSessionInfo());
    } // DB_MBCardGetAllData

    //------------------------------------------------------------------------------
    // PURPOSE: Create a new card with the track data provided.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - TrackData
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - true: data was retrieved successfully
    //      - false: data was not retrieved
    // 
    //   NOTES:
    //    - All MB accounts are created with a default pin value (1234)

    public static Boolean DB_MBCreateCard(String TrackData)
    {
      MBCardData card_data = new MBCardData();
      Random rnd_mb_pin;
      String mb_pin;
      StringBuilder _sb;
      Int32 num_rows_inserted;
      Boolean rc;
      String internal_card_id;
      int card_type;

      card_data.CardId = 0;
      rc = true;
      rnd_mb_pin = new Random();

      if (!DB_MBCardGetAllData(TrackData, card_data))
      {
        Log.Error("DB_MBCreateCard. DB_MBCardGetAllData: Error obtaining card data.");

        return false;
      }

      if (card_data.CardId > 0)
      {
        // Card Already exists
        return false;
      }

      // Obtain internal card identifier from track data
      rc = CardNumber.TrackDataToInternal(TrackData, out internal_card_id, out card_type);
      if (rc)
      {
        try
        {
          using (DB_TRX _db_trx = new DB_TRX())
          {
            // Obtain a 4 digits random number for mobile bank pin
            mb_pin = rnd_mb_pin.Next(0, 9999).ToString("0000");

            _sb = new StringBuilder();
            _sb.AppendLine("DECLARE @pAccountId AS BIGINT ");

            _sb.AppendLine("INSERT   INTO MOBILE_BANKS ");
            _sb.AppendLine("       ( MB_TRACK_DATA     ");
            _sb.AppendLine("       , MB_BLOCKED        ");
            _sb.AppendLine("       , MB_BALANCE        ");
            _sb.AppendLine("       , MB_PIN )          ");
            _sb.AppendLine("VALUES ( @pInternal        ");
            _sb.AppendLine("       , 0                 ");
            _sb.AppendLine("       , 0                 ");
            _sb.AppendLine("       , @pPin)            ");
            _sb.AppendLine(" SET @pAccountId = SCOPE_IDENTITY() ");

            _sb.AppendLine("EXEC dbo.LinkCard @pExternal, @pLinkedType, @pAccountId ");

            using (SqlCommand _cmd = new SqlCommand(_sb.ToString()))
            {
              _cmd.Parameters.Add("@pInternal", SqlDbType.NVarChar).Value = internal_card_id;
              _cmd.Parameters.Add("@pExternal", SqlDbType.NVarChar).Value = TrackData;
              _cmd.Parameters.Add("@pPin", SqlDbType.NVarChar).Value = mb_pin;
              _cmd.Parameters.Add("@pLinkedType", SqlDbType.Int).Value = WCP_CardTypes.CARD_TYPE_MOBILE_BANK;

              num_rows_inserted = _db_trx.ExecuteNonQuery(_cmd);
            }
            if (num_rows_inserted != 1)
            {
              Log.Error("DB_MBCreateCard. Card could not be inserted.");
            }

            _db_trx.Commit();
          }

          rc = false;
        }
        catch (Exception ex)
        {
          Log.Exception(ex);
        }
      } // if (!error)

      return !rc;

    } // DB_MBCreateCard

    //------------------------------------------------------------------------------
    // PURPOSE: Update card info: PlayerTracking.HolderName, Blocked status and track data.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - CardInfo
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - true: data was retrieved successfully
    //      - false: data was not retrieved
    // 
    //   NOTES:
    //    

    public static Boolean DB_MBCardUpdateInfo(MBCardData CardInfo)
    {
      StringBuilder _sb;
      Int32 num_rows_updated;
      Boolean rc;
      CardData card_data = new CardData();
      string internal_card_id;
      int card_type;

      rc = true;

      // Obtain internal card identifier from track data
      rc = CardNumber.TrackDataToInternal(CardInfo.TrackData, out internal_card_id, out card_type);
      if (rc)
      {
        try
        {
          using (DB_TRX _db_trx = new DB_TRX())
          {
            _sb = new StringBuilder();
            _sb.AppendLine("UPDATE   MOBILE_BANKS ");
            _sb.AppendLine("   SET   MB_HOLDER_NAME  = @pHolderName ");
            _sb.AppendLine("       , MB_BLOCKED      = @pBlocked ");
            _sb.AppendLine("       , MB_TRACK_DATA   = @pInternal ");
            _sb.AppendLine(" WHERE   MB_ACCOUNT_ID   = @pAccountId ");

            _sb.AppendLine("EXEC dbo.LinkCard @pExternal, @pLinkedType, @pAccountId ");

            using (SqlCommand _cmd = new SqlCommand(_sb.ToString()))
            {
              if (CardInfo.HolderName.Trim() == "")
              {
                _cmd.Parameters.Add("@pHolderName", SqlDbType.NVarChar).Value = DBNull.Value;
              }
              else
              {
                _cmd.Parameters.Add("@pHolderName", SqlDbType.NVarChar).Value = CardInfo.HolderName;
              }

              _cmd.Parameters.Add("@pBlocked", SqlDbType.Bit).Value = CardInfo.Blocked;
              _cmd.Parameters.Add("@pInternal", SqlDbType.NVarChar).Value = internal_card_id;
              _cmd.Parameters.Add("@pExternal", SqlDbType.NVarChar).Value = CardInfo.TrackData;
              _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = CardInfo.CardId;
              _cmd.Parameters.Add("@pLinkedType", SqlDbType.Int).Value = WCP_CardTypes.CARD_TYPE_MOBILE_BANK;

              num_rows_updated = _db_trx.ExecuteNonQuery(_cmd);
            }

            if (num_rows_updated != 1)
            {
              Log.Error("DB_MBUpdateCardInfo. Card not updated.");
            }

            _db_trx.Commit();
          }

          rc = false;
        }
        catch (Exception ex)
        {
          Log.Exception(ex);
        }
      } // if (!error)

      return !rc;
    } // DB_MBCardUpdateInfo

    //------------------------------------------------------------------------------
    // PURPOSE: Update card info: track data.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - CardInfo
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - true: data was retrieved successfully
    //      - false: data was not retrieved
    // 
    //   NOTES:
    //    

    public static Boolean DB_MBCardUpdateInfo(string TrackData, Int64 AccountId)
    {
      StringBuilder _sb;
      Int32 num_rows_updated;
      Boolean rc;
      CardData card_data = new CardData();
      string internal_card_id;
      int card_type;

      rc = true;

      // Obtain internal card identifier from track data
      rc = CardNumber.TrackDataToInternal(TrackData, out internal_card_id, out card_type);
      if (rc)
      {
        try
        {
          using (DB_TRX _db_trx = new DB_TRX())
          {
            _sb = new StringBuilder();
            _sb.AppendLine("UPDATE   MOBILE_BANKS ");
            _sb.AppendLine("   SET   MB_TRACK_DATA   = @pInternal ");
            _sb.AppendLine(" WHERE   MB_ACCOUNT_ID   = @pAccountId ");

            _sb.AppendLine("EXEC dbo.LinkCard @pExternal, @pLinkedType, @pAccountId ");

            using (SqlCommand _cmd = new SqlCommand(_sb.ToString()))
            {
              _cmd.Parameters.Add("@pInternal", SqlDbType.NVarChar).Value = internal_card_id;
              _cmd.Parameters.Add("@pExternal", SqlDbType.NVarChar).Value = TrackData;
              _cmd.Parameters.Add("@pLinkedType", SqlDbType.Int).Value = WCP_CardTypes.CARD_TYPE_MOBILE_BANK;
              _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;

              num_rows_updated = _db_trx.ExecuteNonQuery(_cmd);
            }

            if (num_rows_updated != 1)
            {
              Log.Error("DB_MBUpdateCardInfo. Card not updated.");
            }

            _db_trx.Commit();
          }

          rc = false;
        }
        catch (Exception ex)
        {
          Log.Exception(ex);
        }
      } // if (!error)

      return !rc;
    } // DB_MBCardUpdateInfo

    ////////////------------------------------------------------------------------------------
    //////////// PURPOSE: Insert MB card movement into MB_movements table
    //////////// 
    ////////////  PARAMS:
    ////////////      - INPUT:
    ////////////          - MovementInfo
    ////////////          - SqlTrx
    ////////////
    ////////////      - OUTPUT:
    ////////////          - MovementId
    ////////////
    //////////// RETURNS: 
    ////////////      - True
    ////////////      - False
    //////////// 
    ////////////   NOTES:
    ////////////    

    //////public static Boolean DB_InsertMBCardMovement(MBMovementData MovementInfo,
    //////                                              out Int64 MovementId,
    //////                                              SqlTransaction SqlTrx)
    //////{
    //////  MovementInfo.CashierName = Cashier.TerminalName;
    //////  return Common.MobileBank.DB_InsertMBCardMovement(MovementInfo, out MovementId, SqlTrx);

    //////} // DB_InsertMBCardMovement

    //------------------------------------------------------------------------------
    // PURPOSE: Change a mobile bank current sales credit limit (Balance).
    // 
    //  PARAMS:
    //      - INPUT:
    //          - CardId
    //          - AmountToAdd       : Amount of Sales Credit to add on balance 
    //          - Operation         : If current update is from Extension, Balance Edit, or after deposits operation
    //
    //      - OUTPUT:
    //          - SalesLimitReached  : Flag if current amount for update balance is over the Cashier Session MB limit
    //          - RemainCreditToSale : Current amount Cashier Session MB limit available
    // RETURNS:
    //      - true: operation completed successfully
    //      - false: operation failed
    // 
    //   NOTES: Used from Limit Extensions and Edit Limit buttons
    //    
    public static Boolean DB_ChangeMBCardLimit(Int64 CardId,
                                               MB_SALES_LIMIT_OPERATION Operation,
                                               Currency AmountToAdd,
                                               out Boolean SalesLimitReached,
                                               out Currency RemainCreditToSale)
    {
      // Update Mobile Bank Sales Limit (MB_BALANCE)
      if (!MB_ChangeSalesLimit(CardId, Operation, AmountToAdd, out SalesLimitReached, out RemainCreditToSale))
      {
        Log.Error("DB_ChangeMBCardLimit. Error updating Mobile Bank Limit Sales. Mobile Bank: " + CardId.ToString());

        return false;
      }

      // Update Cashier Session Mobile Bank Sales Limit  (CS_MB_LIMIT)
      if (!RefreshSalesLimitMB(Cashier.UserId, Cashier.SessionId))
      {
        Log.Error("DB_ChangeMBCardLimit. Error updating Sales limit RefreshSalesLimitMB. Mobile Bank: " + CardId.ToString());

        return false;
      }

      return true;
    } // DB_ChangeMBCardLimit

    //------------------------------------------------------------------------------
    // PURPOSE: Change a mobile bank current sales credit limit (Balance).
    // 
    //  PARAMS:
    //      - INPUT:
    //          - CardId
    //          - OperationUpdateLimit : Source Function Request (Limit Extension, Limit Edit or Deposit)
    //          - AmountToAdd       : Amount of Sales Credit to add on balance 
    //
    //      - OUTPUT:
    //          - SalesLimitReached  : Flag if current amount for update balance is over the Cashier Session MB limit
    //          - RemainCreditToSale : Current amount Cashier Session MB limit available
    // RETURNS:
    //      - true: operation completed successfully
    //      - false: operation failed
    // 
    //   NOTES: Used from DB_ChangeMBCardLimit and Deposit 
    //
    public static Boolean MB_ChangeSalesLimit(Int64 CardId,
                                              MB_SALES_LIMIT_OPERATION Operation,
                                              Currency AmountToAdd,
                                              out Boolean SalesLimitReached,
                                              out Currency RemainingSalesLimit)
    {

      MBCardData _mb;
      VoucherMBCardChange _voucher;
      Boolean _voucher_saved;
      Currency _new_mb_sales_limit;        // MB Balance at the end of Operation Current + AmountAdd

      SalesLimitReached = false;
      RemainingSalesLimit = 0;

      try
      {
        _mb = new MBCardData();
        if (!CashierBusinessLogic.DB_MBCardGetAllData(CardId, _mb))
        {
          Log.Error("MB_ChangeSalesLimit: DB_MBCardGetAllData failed. Mobile Bank: " + CardId.ToString());

          return false;
        }

        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (!DB_MB_SetNewSalesLimit(_mb, Operation, AmountToAdd, out SalesLimitReached, out RemainingSalesLimit,
                                      out _new_mb_sales_limit, _db_trx.SqlTransaction))
          {
            Log.Error("MB_ChangeSalesLimit: DB_MB_SetNewSalesLimit failed. Mobile Bank: " + CardId.ToString());

            return false;
          }

          if (SalesLimitReached)
          {
            // Limit reached: No voucher, no commit.
            // JML & RCI 12-NOV-2013: Return true is ok.
            return true;
          }

          MBCardData _mb_card_data;
          _mb_card_data = new MBCardData();
          if (!MobileBank.DB_MBCardGetAllData(CardId, _mb_card_data, null))
          {
            Log.Error("MB_ChangeSalesLimit: Could not load MB data. Mobile Bank: " + CardId.ToString());

            return false;
          }

          _voucher = new VoucherMBCardChange(PrintMode.Print, _mb_card_data, _db_trx.SqlTransaction);

          // 3.2 Build voucher data 
          _voucher.AddCurrency("VOUCHER_MBCARD_CHANGE_INITIAL", _mb.SalesLimit);
          _voucher.AddCurrency("VOUCHER_MBCARD_CHANGE_FINAL", _new_mb_sales_limit);

          // 3.3 Save voucher
          _voucher_saved = _voucher.Save(_db_trx.SqlTransaction);

          if (!_voucher_saved)
          {
            Log.Error("DB_ChangeMBCardLimit. Error saving voucher. Mobile Bank: " + CardId.ToString());

            return false;
          }

          _db_trx.Commit();
        }

        VoucherPrint.Print(_voucher);

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
    }  // MB_ChangeSalesLimit

    public static Boolean DB_MB_SetNewSalesLimit(MBCardData MBCard,
                                                 MB_SALES_LIMIT_OPERATION Operation,
                                                 Currency AmountToAdd,
                                                 out Boolean SalesLimitReached,
                                                 out Currency RemainingSalesLimit,
                                                 out Currency MBSalesLimit,
                                                 SqlTransaction Trx)
    {
      return Common.MobileBank.DB_MB_SetNewSalesLimit(MBCard, Operation, AmountToAdd, Cashier.CashierSessionInfo(), Cashier.UserId,
                                                      out SalesLimitReached, out RemainingSalesLimit, out MBSalesLimit, Trx);
    }  // DB_MB_SetNewSalesLimit

    //------------------------------------------------------------------------------
    // PURPOSE: Refresh the MB Sales Limit with current limit efective
    // 
    //  PARAMS:
    //      - INPUT:
    //          - CardId
    //          - SessionId
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - true:  operation completed successfully
    //      - false: operation failed
    // 
    //   NOTES:
    //    
    private static Boolean RefreshSalesLimitMB(Int32 UserId, Int64 SessionId)
    {
      Currency _dummy_remain_credit;
      Currency _sales_limit;
      Boolean _limited_sales_mb;
      Int32 _gp_value;

      _limited_sales_mb = false;

      if (Int32.TryParse(GeneralParam.Value("MobileBank", "SalesLimit.Enabled"), out _gp_value))
      {
        _limited_sales_mb = (_gp_value == 1);
      }

      if (!_limited_sales_mb)
      {
        return true;
      }

      // Read Sales Limit and Remain Sale Credit. 
      if (!WSI.Common.Cashier.GetRemainingSalesLimit(UserId, SessionId, WSI.Common.Cashier.SalesLimitType.MobileBank, out _dummy_remain_credit, out _sales_limit))
      {
        Log.Error("DB_ChangeMBCardLimit. Error reading Cashier Sesion Limit Sales. Mobile Bank: " + UserId.ToString());

        return false;
      }

      // Update the Cashier Session Limit
      using (DB_TRX _db_trx = new DB_TRX())
      {
        if (!WSI.Common.Cashier.SetNewSalesLimit(UserId, SessionId, _sales_limit, WSI.Common.Cashier.SalesLimitType.MobileBank, _db_trx.SqlTransaction))
        {
          Log.Error("DB_ChangeMBCardLimit. Error updating Cashier Sesion Limit Sales. Mobile Bank: " + UserId.ToString());

          return false;
        }

        _db_trx.Commit();
      }

      return true;
    } // RefreshSalesLimitMB

    //------------------------------------------------------------------------------
    // PURPOSE: Amount cash for extend limit for a mobile bank into the cashier. Usefull for Voucher Preview
    // 
    //  PARAMS:
    //      - INPUT:
    //          - CardId
    //          - AmountToDeposit
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - true: operation completed successfully
    //      - false: operation failed
    // 
    //   NOTES:
    //    
    public static Boolean GetSalesLimitAutoIncrement(Int32 UserId, Int64 SessionId, Currency AmountToDeposit, out Currency SalesLimitIncrement)
    {
      Boolean _auto_increase;
      Int32 _gp_value;
      Currency _remaining_limit;
      Boolean _sales_limit_enabled;

      SalesLimitIncrement = 0;

      try
      {
        if (!Int32.TryParse(GeneralParam.Value("MobileBank", "AutomaticRenewLimitAfterDeposit"), out _gp_value))
        {
          _gp_value = 1;
        }

        _auto_increase = (_gp_value == 1);

        if (!_auto_increase)
        {
          // After deposit the limit is not increased automatically
          SalesLimitIncrement = 0;

          return true;
        }

        if (!Int32.TryParse(GeneralParam.Value("MobileBank", "SalesLimit.Enabled"), out _gp_value))
        {
          _gp_value = 0;
        }
        _sales_limit_enabled = (_gp_value == 1);

        if (!_sales_limit_enabled)
        {
          // Increase the limit with the deposit
          SalesLimitIncrement = AmountToDeposit;

          return true;
        }

        if (!WSI.Common.Cashier.GetRemainingSalesLimit(Cashier.UserId, Cashier.SessionId, WSI.Common.Cashier.SalesLimitType.MobileBank,
                                                       out _remaining_limit))
        {
          return false;
        }

        // Compute the available limit
        SalesLimitIncrement = Math.Min(AmountToDeposit, _remaining_limit);

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // GetSalesLimitAutoIncrement

    //------------------------------------------------------------------------------
    // PURPOSE: Deposit of MB on Cashier
    // 
    //  PARAMS:
    //      - INPUT:
    //          - CardId
    //          - AmountToDeposit
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - true: operation completed successfully
    //      - false: operation failed
    // 
    //   NOTES:
    //    
    public static Boolean DB_MBCardCashDeposit(MBCardData MBCard, Currency AmountToDeposit)
    {
      StringBuilder _sql_str;
      Int32 _num_rows;
      VoucherMBCardDepositCash _voucher;
      Boolean _sales_limit_enabled;
      Int32 _gp_value;
      Currency _new_mb_sales_limit;
      Boolean _dummy_sales_limit_reached;
      Currency _dummy_remaining_sales_limit;
      CashierMovementsTable _cashier_mov_table;
      Currency _over_cash;
      Currency _amount_to_add;
      Currency _pending_cash;
      Boolean _gp_pending_cash_partial;
      Int64 _operation_id;
      MBMovementData _mb_movement_info;

      // Initialize variables.
      _pending_cash = 0;
      _amount_to_add = 0;
      _over_cash = 0;
      _gp_pending_cash_partial = MobileBank.GP_GetRegisterShortfallOnDeposit();
      _sql_str = new StringBuilder();

      // JCM 01-JUL-2012 Mobile Bank Sales Limit is Enabled      
      if (!Int32.TryParse(GeneralParam.Value("MobileBank", "SalesLimit.Enabled"), out _gp_value))
      {
        _gp_value = 0;
      }
      _sales_limit_enabled = (_gp_value == 1);

      try
      {

        if (MBCard.PendingCash >= AmountToDeposit)
        {
          _over_cash = 0;
          _amount_to_add = AmountToDeposit;
          _pending_cash = MBCard.PendingCash - AmountToDeposit;
        }
        else
        {
          _over_cash = AmountToDeposit - MBCard.PendingCash;
          _amount_to_add = AmountToDeposit - _over_cash;
        }

        // Get Sql Connection 
        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (MBCard.CurrentCashierSessionId == 0)
          {
            Log.Error("MBCard.CurrentCashierSessionId == 0. Mobile Bank: " + MBCard.CardId.ToString());
            return false;
          }

          _sql_str.AppendLine("UPDATE   MOBILE_BANKS ");
          _sql_str.AppendLine("   SET   MB_DEPOSIT            = MB_DEPOSIT           + @pDeposit,        ");
          _sql_str.AppendLine("         MB_OVER_CASH          = MB_OVER_CASH         + @pOverCash ,      ");
          _sql_str.AppendLine("         MB_OVER_CASH_SESSION  = MB_OVER_CASH_SESSION + @pOverCash ,      ");

          if (_gp_pending_cash_partial)
          {
            _sql_str.AppendLine("         MB_PENDING_CASH       = 0,                                     ");
            _sql_str.AppendLine("         MB_SHORTFALL_CASH     = MB_SHORTFALL_CASH    + @pShortFallCash, ");
            _sql_str.AppendLine("         MB_SHORTFALL_CASH_SESSION = MB_SHORTFALL_CASH_SESSION    + @pShortFallCash ");
          }
          else
          {
            _sql_str.AppendLine("      MB_PENDING_CASH       = MB_PENDING_CASH - @pPending ");
          }

          _sql_str.AppendLine(" WHERE   MB_ACCOUNT_ID         = @pCardId                                 ");
          _sql_str.AppendLine("   AND   MB_CASHIER_SESSION_ID = @pCSId                                   ");

          using (SqlCommand _cmd = new SqlCommand(_sql_str.ToString()))
          {

            _cmd.Parameters.Add("@pDeposit", SqlDbType.Money).Value = AmountToDeposit.SqlMoney;

            if (_gp_pending_cash_partial)
            {
              _cmd.Parameters.Add("@pShortFallCash", SqlDbType.Money).Value = _pending_cash.SqlMoney;
            }
            else
            {
              _cmd.Parameters.Add("@pPending", SqlDbType.Money).Value = _amount_to_add.ToString();
            }
            _cmd.Parameters.Add("@pCardId", SqlDbType.BigInt).Value = MBCard.CardId;
            _cmd.Parameters.Add("@pCSId", SqlDbType.BigInt).Value = Cashier.SessionId;
            _cmd.Parameters.Add("@pOverCash", SqlDbType.Money).Value = _over_cash.SqlMoney;

            _num_rows = _db_trx.ExecuteNonQuery(_cmd);
          }

          if (_num_rows != 1)
          {
            Log.Error("DB_MBCardCashDeposit. MBCard not updated. Mobile Bank: " + MBCard.CardId);

            return false;
          }

          if (!WSI.Common.Cashier.UpdateSessionTotalSold(Cashier.SessionId, Cashier.UserId, WSI.Common.Cashier.SalesLimitType.MobileBank,
                                                         AmountToDeposit, _db_trx.SqlTransaction))
          {
            Log.Error("DB_MBCardCashDeposit. MBCard Sales not updated. Mobile Bank: " + MBCard.CardId.ToString());

            return false;
          }

          _operation_id = 0;

          // MB Card operation Deposit.
          if (!Common.Operations.DB_InsertOperation(OperationCode.MB_DEPOSIT, 0,
                                                    Cashier.SessionId, MBCard.CardId, 0, AmountToDeposit, 0,
                                                    0, 0, string.Empty, out _operation_id, _db_trx.SqlTransaction))
          {
            Log.Error("DB_MBCardCashDeposit. MBOperation not inserted. Mobile Bank: " + MBCard.CardId.ToString());

            return false;
          }

          // Mobile Bank Balance Limit Extension
          if (!DB_MB_SetNewSalesLimit(MBCard, MB_SALES_LIMIT_OPERATION.DEPOSIT, _amount_to_add, out _dummy_sales_limit_reached,
                                      out _dummy_remaining_sales_limit, out _new_mb_sales_limit, _db_trx.SqlTransaction))
          {
            Log.Error("DB_MBCardCashDeposit: Error extension cash. Mobile Bank: " + MBCard.CardId.ToString() + ". Amount:" + AmountToDeposit.ToString());

            return false;
          }

          long movement_id = 0;

          // MB Card Cash Deposit.
          _mb_movement_info = new MBMovementData();
          _mb_movement_info.CashierId = Cashier.TerminalId;
          _mb_movement_info.CardId = MBCard.CardId;
          _mb_movement_info.Type = MBMovementType.DepositCash;
          _mb_movement_info.CashierSessionId = Cashier.SessionId;
          _mb_movement_info.InitialBalance = MBCard.SalesLimit;
          _mb_movement_info.SubAmount = 0;
          _mb_movement_info.AddAmount = AmountToDeposit;
          _mb_movement_info.FinalBalance = _new_mb_sales_limit;
          _mb_movement_info.OperationId = _operation_id;
          _mb_movement_info.CashierName = Cashier.TerminalName;

          if (!Common.MobileBank.DB_InsertMBCardMovement(_mb_movement_info, out movement_id, _db_trx.SqlTransaction))
          {
            Log.Error("DB_MBCardCashDeposit. MBMovement not inserted. Mobile Bank: " + MBCard.CardId.ToString());

            return false;
          }

          if (_gp_pending_cash_partial)
          {
            if (_pending_cash > 0)
            {
              _mb_movement_info = new MBMovementData();
              _mb_movement_info.CashierId = Cashier.TerminalId;
              _mb_movement_info.CardId = MBCard.CardId;
              _mb_movement_info.Type = MBMovementType.CashShortFall;
              _mb_movement_info.CashierSessionId = Cashier.SessionId;
              _mb_movement_info.InitialBalance = MBCard.SalesLimit;
              _mb_movement_info.SubAmount = _pending_cash;
              _mb_movement_info.AddAmount = 0;
              _mb_movement_info.FinalBalance = _new_mb_sales_limit;
              _mb_movement_info.OperationId = _operation_id;
              _mb_movement_info.CashierName = Cashier.TerminalName;

              if (!Common.MobileBank.DB_InsertMBCardMovement(_mb_movement_info, out movement_id, _db_trx.SqlTransaction))
              {
                Log.Error("DB_MBCardCashDeposit. MBMovement not inserted. Mobile Bank: " + MBCard.CardId.ToString());

                return false;
              }
            }
          }

          if (_over_cash > 0)
          {
            _mb_movement_info = new MBMovementData();
            _mb_movement_info.CashierId = Cashier.TerminalId;
            _mb_movement_info.CardId = MBCard.CardId;
            _mb_movement_info.Type = MBMovementType.CashExcess;
            _mb_movement_info.CashierSessionId = Cashier.SessionId;
            _mb_movement_info.InitialBalance = MBCard.SalesLimit;
            _mb_movement_info.SubAmount = 0;
            _mb_movement_info.AddAmount = _over_cash;
            _mb_movement_info.FinalBalance = _new_mb_sales_limit;
            _mb_movement_info.OperationId = _operation_id;
            _mb_movement_info.CashierName = Cashier.TerminalName;

            if (!Common.MobileBank.DB_InsertMBCardMovement(_mb_movement_info, out movement_id, _db_trx.SqlTransaction))
            {
              Log.Error("DB_MBCardCashDeposit. MBMovement not inserted. Mobile Bank: " + MBCard.CardId.ToString());

              return false;
            }
          }

          _mb_movement_info = new MBMovementData();
          _mb_movement_info.CashierId = Cashier.TerminalId;
          _mb_movement_info.CardId = MBCard.CardId;
          _mb_movement_info.Type = MBMovementType.AutomaticChangeLimit;
          _mb_movement_info.CashierSessionId = Cashier.SessionId;
          _mb_movement_info.InitialBalance = MBCard.SalesLimit;
          _mb_movement_info.SubAmount = 0;
          _mb_movement_info.AddAmount = _new_mb_sales_limit - MBCard.SalesLimit;
          _mb_movement_info.FinalBalance = _new_mb_sales_limit;
          _mb_movement_info.OperationId = _operation_id;
          _mb_movement_info.CashierName = Cashier.TerminalName;

          if (!Common.MobileBank.DB_InsertMBCardMovement(_mb_movement_info, out movement_id, _db_trx.SqlTransaction))
          {
            Log.Error("DB_MBCardCashDeposit. MBMovement not inserted. Mobile Bank: " + MBCard.CardId.ToString());

            return false;
          }

          _cashier_mov_table = new CashierMovementsTable(Cashier.CashierSessionInfo());
          _cashier_mov_table.Add(_operation_id, CASHIER_MOVEMENT.MB_CASH_IN, AmountToDeposit, MBCard.CardId, MBCard.TrackData);

          if (!_cashier_mov_table.Save(_db_trx.SqlTransaction))
          {
            return false;
          }

          Currency _mb_deposit_excess;

          _mb_deposit_excess = MBCard.PendingCash - AmountToDeposit;

          // Insert voucher.

          // - Create voucher 
          _voucher = new VoucherMBCardDepositCash(PrintMode.Print, CashierVoucherType.MBDeposit, MBCard, _db_trx.SqlTransaction);

          _voucher.AddString("STR_VOUCHER_MBCARD_CASH_DEPOSIT_TITLE", Resource.String("STR_VOUCHER_MBCARD_CASH_DEPOSIT_TITLE"));
          _voucher.AddString("STR_VOUCHER_MBCARD_CASH_DEPOSIT_AMOUNT", ((String)Resource.String("STR_VOUCHER_MBCARD_CASH_DEPOSIT_AMOUNT") + ":"));
          _voucher.AddString("STR_VOUCHER_MBCARD_CASH_DEPOSIT_CASH", ((String)Resource.String("STR_VOUCHER_MBCARD_CASH_DEPOSIT_CASH") + ":"));

          if (_gp_pending_cash_partial)
          {
            _voucher.AddString("STR_VOUCHER_MBCARD_CASH_DEPOSIT_PENDING", ((String)Resource.String("STR_UC_CARD_PENDING_DEPOSITS") + ":"));
          }
          else
          {
            _voucher.AddString("STR_VOUCHER_MBCARD_CASH_DEPOSIT_PENDING", ((String)Resource.String("STR_VOUCHER_MBCARD_CASH_DEPOSIT_PENDING") + ":"));
          }

          _voucher.AddString("STR_VOUCHER_MBCARD_CASH_DEPOSIT_EXCESS", Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_DIFF_EXTRA") + ":");

          // Build voucher specific data
          _voucher.AddString("VOUCHER_CARD_ACCOUNT_ID", MBCard.CardId.ToString());
          _voucher.AddString("VOUCHER_CARD_CARD_ID", MBCard.TrackData);
          _voucher.AddString("VOUCHER_MBCARD_HOLDER_NAME", MBCard.HolderName);

          // Cash deposit detail 
          _voucher.AddCurrency("VOUCHER_MBCARD_CASH_DEPOSIT_INITIAL", MBCard.SalesLimit);
          _voucher.AddCurrency("VOUCHER_MBCARD_CASH_DEPOSIT_AMOUNT", AmountToDeposit);
          _voucher.AddCurrency("VOUCHER_MBCARD_CASH_DEPOSIT_FINAL", _new_mb_sales_limit);
          _voucher.AddCurrency("VOUCHER_MBCARD_CASH_DEPOSIT_CASH", AmountToDeposit);

          if (_mb_deposit_excess < 0)
          {
            _voucher.AddCurrency("VOUCHER_MBCARD_CASH_DEPOSIT_EXCESS", Math.Abs(_mb_deposit_excess));
            _voucher.AddCurrency("VOUCHER_MBCARD_CASH_DEPOSIT_PENDING", 0);
          }
          else
          {
            _voucher.AddCurrency("VOUCHER_MBCARD_CASH_DEPOSIT_EXCESS", 0);
            _voucher.AddCurrency("VOUCHER_MBCARD_CASH_DEPOSIT_PENDING", MBCard.PendingCash - AmountToDeposit);
          }

          // - Save voucher
          if (!_voucher.Save(_operation_id, _db_trx.SqlTransaction))
          {
            Log.Error("DB_MBCardCashDeposit. Error saving voucher. Card Id: " + MBCard.CardId.ToString());

            return false;
          }

          _db_trx.Commit();
        }

        VoucherPrint.Print(_voucher);

        //HULK TEST
        MBCard.PendingCash = MBCard.PendingCash - AmountToDeposit;

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // DB_MBCardCashDeposit


    //------------------------------------------------------------------------------
    // PURPOSE: Close Cash Session of MB on Cashier
    // 
    //  PARAMS:
    //      - INPUT:
    //          - CardId
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - true: operation completed successfully
    //      - false: operation failed
    // 
    //   NOTES:
    //    
    public static Boolean DB_MBCloseCashSession(MBCardData MBCard)
    {
      VoucherMBCardDepositCash _voucher;

      try
      {
        // Set 0 Sales Limit
        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (!Common.MobileBank.DB_MBCloseCashSession(MBCard, Cashier.CashierSessionInfo(), Cashier.UserId,
                                                       out _voucher, false, _db_trx.SqlTransaction))
          {
            return false;
          }

          _db_trx.Commit();
        }

        VoucherPrint.Print(_voucher);

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // DB_MBCloseCashSession

    //------------------------------------------------------------------------------
    // PURPOSE: Update card info: PlayerTracking.HolderName.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Id
    //          - Name
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - true: data was retrieved successfully
    //      - false: data was not retrieved
    // 
    //   NOTES:
    //    

    internal static void DB_UpdateMBCardHolder(string Id, string Name)
    {
      SqlConnection sql_conn;
      SqlTransaction sql_trx;
      SqlCommand sql_command;
      String sql_str;
      Int32 num_rows_updated;

      sql_conn = null;
      sql_trx = null;

      try
      {
        // Get Sql Connection 
        sql_conn = WGDB.Connection();
        sql_trx = sql_conn.BeginTransaction();

        sql_str = "";
        sql_str += "UPDATE   MOBILE_BANKS ";
        sql_str += "   SET   MB_HOLDER_NAME  = @p1 ";
        sql_str += "       , MB_ACCOUNT_TYPE = @p3 ";
        sql_str += " WHERE   MB_ACCOUNT_ID   = @p2 ";

        sql_command = new SqlCommand(sql_str);
        sql_command.Connection = sql_trx.Connection;
        sql_command.Transaction = sql_trx;

        if (Name.Trim() == "")
        {
          sql_command.Parameters.Add("@p1", SqlDbType.NVarChar, 50, "MB_HOLDER_NAME").Value = DBNull.Value;
          sql_command.Parameters.Add("@p3", SqlDbType.Int, sizeof(int), "MB_ACCOUNT_TYPE").Value = ACCOUNT_USER_TYPE.ANONYMOUS;
        }
        else
        {
          sql_command.Parameters.Add("@p1", SqlDbType.NVarChar, 50, "MB_HOLDER_NAME").Value = Name;
          sql_command.Parameters.Add("@p3", SqlDbType.Int, sizeof(int), "MB_ACCOUNT_TYPE").Value = ACCOUNT_USER_TYPE.PERSONAL;
        }

        sql_command.Parameters.Add("@p2", SqlDbType.BigInt, 8, "MB_ACCOUNT_ID").Value = Convert.ToUInt64(Id);

        num_rows_updated = sql_command.ExecuteNonQuery();

        if (num_rows_updated != 1)
        {
          Log.Error("DB_UpdateMBCardHolder. Card not updated. Card Id: " + Id);
        }

        sql_trx.Commit();
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
      finally
      {
        if (sql_trx != null)
        {
          if (sql_trx.Connection != null)
          {
            sql_trx.Rollback();
          }

          sql_trx.Dispose();
          sql_trx = null;
        }

        // Close connection
        if (sql_conn != null)
        {
          sql_conn.Close();
          sql_conn = null;
        }
      }
    } // DB_UpdateMBCardHolder

    //------------------------------------------------------------------------------
    // PURPOSE: Update card info: PlayerTracking.Limits.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Id
    //          - Limits
    //            - Total
    //            - By Recharge
    //            - By Number of Recharges
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - true: data was retrieved successfully
    //      - false: data was not retrieved
    // 
    //   NOTES:
    //    

    internal static void DB_UpdateMBCardLimits(string Id, Decimal TotalLimit, Decimal RechargeLimit, Decimal NumberOfRechargesLimit)
    {
      StringBuilder _sql_str;
      Int32 _num_rows_updated;

      _sql_str = new StringBuilder();

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _sql_str.Length = 0;
          _sql_str.AppendLine("UPDATE   MOBILE_BANKS    ");
          _sql_str.AppendLine("   SET   MB_TOTAL_LIMIT  = @pTotalLimit                          ");
          _sql_str.AppendLine("       , MB_RECHARGE_LIMIT = @pRechargeLimit                     ");
          _sql_str.AppendLine("       , MB_NUMBER_OF_RECHARGES_LIMIT = @pNumberOfRechargesLimit ");
          _sql_str.AppendLine(" WHERE   MB_ACCOUNT_ID = @pAccountId ");


          using (SqlCommand _sql_command = new SqlCommand(_sql_str.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_command.Parameters.Add("@pAccountId", SqlDbType.BigInt, 8, "MB_ACCOUNT_ID").Value = Convert.ToUInt64(Id);
            if (TotalLimit == 0)
            {
              _sql_command.Parameters.Add("@pTotalLimit", SqlDbType.Money).Value = DBNull.Value;
            }
            else
            {
              _sql_command.Parameters.Add("@pTotalLimit", SqlDbType.Money).Value = TotalLimit;
            }
            if (RechargeLimit == 0)
            {
              _sql_command.Parameters.Add("@pRechargeLimit", SqlDbType.Money).Value = DBNull.Value;
            }
            else
            {
              _sql_command.Parameters.Add("@pRechargeLimit", SqlDbType.Money).Value = RechargeLimit;
            }
            if (NumberOfRechargesLimit == 0)
            {
              _sql_command.Parameters.Add("@pNumberOfRechargesLimit", SqlDbType.Int).Value = DBNull.Value;
            }
            else
            {
              _sql_command.Parameters.Add("@pNumberOfRechargesLimit", SqlDbType.Int).Value = NumberOfRechargesLimit;
            }

            _num_rows_updated = _sql_command.ExecuteNonQuery();

            if (_num_rows_updated != 1)
            {
              Log.Error("DB_UpdateMBCardHolder. Card not updated. Card Id: " + Id);
            }

            _db_trx.Commit();
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return;
      }

    } // DB_UpdateMBCardLimits

    //------------------------------------------------------------------------------
    // PURPOSE: Update card info: Pin.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Id
    //          - Pin
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - true: data was retrieved successfully
    //      - false: data was not retrieved
    // 
    //   NOTES:

    internal static void DB_UpdateMBCardPin(Int64 Id, string Pin)
    {
      SqlConnection sql_conn;
      SqlTransaction sql_trx;
      SqlCommand sql_command;
      String sql_str;
      Int32 num_rows_updated;

      sql_conn = null;
      sql_trx = null;

      try
      {
        // Get Sql Connection 
        sql_conn = WGDB.Connection();
        sql_trx = sql_conn.BeginTransaction();

        sql_str = "";
        sql_str += "UPDATE   MOBILE_BANKS ";
        sql_str += "   SET   MB_PIN         = @p1 ";
        sql_str += " WHERE   MB_ACCOUNT_ID  = @p2 ";

        sql_command = new SqlCommand(sql_str);
        sql_command.Connection = sql_trx.Connection;
        sql_command.Transaction = sql_trx;

        sql_command.Parameters.Add("@p1", SqlDbType.NVarChar, 12, "MB_PIN").Value = Pin;
        sql_command.Parameters.Add("@p2", SqlDbType.BigInt, 8, "MB_ACCOUNT_ID").Value = Id;

        num_rows_updated = sql_command.ExecuteNonQuery();

        if (num_rows_updated != 1)
        {
          Log.Error("DB_UpdateMBCardPin. Card not updated. Card: " + Id.ToString());
        }

        sql_trx.Commit();
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
      finally
      {
        if (sql_trx != null)
        {
          if (sql_trx.Connection != null)
          {
            sql_trx.Rollback();
          }

          sql_trx.Dispose();
          sql_trx = null;
        }

        // Close connection
        if (sql_conn != null)
        {
          sql_conn.Close();
          sql_conn = null;
        }
      }
    } // DB_UpdateMBCardPin

    #endregion Mobile Bank Functions

    #region PROMOTION

    public static Boolean DB_PromotionReset(Int64 AccountId, String ExternalTrackData)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        if (DB_PromotionReset(AccountId, ExternalTrackData, true, _db_trx.SqlTransaction))
        {
          _db_trx.Commit();

          return true;
        }
      }

      return false;
    }
    public static Boolean DB_PromotionReset(Int64 AccountId, String ExternalTrackData, Boolean ShowConfirmDialog, SqlTransaction Trx)
    {
      DialogResult _dlg_rc;
      String _message;
      SqlTransaction _trx;
      String _msg;
      Int64 _play_session_id;
      MultiPromos.AccountBalance _ini_balance;
      MultiPromos.AccountBalance _promo_balance;
      MultiPromos.AccountBalance _cancelled;
      MultiPromos.AccountBalance _fin_balance;
      Currency _total_balance;
      Int32 _num_promos;
      Int32 _num_promos_cancelled;
      CashierMovementsTable _cashier_mov_table;
      AccountMovementsTable _account_mov_table;
      CashierSessionInfo _session_info;

      // "No se pudo cancelar la promoci�n."
      _msg = Resource.String("STR_PROMOTION_ERROR_MSG_CANT_CANCEL");

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _trx = _db_trx.SqlTransaction;

          _trx.Save("Trx_PromotionToRedeemable");
          if (!MultiPromos.Trx_AccountPromotionsAction(MultiPromos.AccountPromotionsAction.NotRedeemableToRedeemable, AccountId,
                                                       out _promo_balance, out _num_promos, _trx))
          {
            return false;
          }
          _trx.Rollback("Trx_PromotionToRedeemable");


          if (_promo_balance.TotalBalance > 0)
          {
            Currency _aux;
            _aux = _promo_balance.TotalBalance;
            // "Hay @p0 cr�ditos no redimibles que pueden ser convertidos a redimible."
            _msg = Resource.String("STR_PROMOTION_ERROR_MSG_CONVERT_NRE_TO_RE", _aux.ToString());

            return true;
          }

          if (!MultiPromos.Trx_AccountPromotionsAction(MultiPromos.AccountPromotionsAction.CancelByPlayer, AccountId,
                                                       out _promo_balance, out _num_promos, _trx))
          {
            return false;
          }

          if (_promo_balance.TotalBalance == 0 && _num_promos == 0)
          {
            // "No hay cr�ditos no redimibles que puedan ser cancelados."
            _msg = Resource.String("STR_PROMOTION_ERROR_MSG_CANT_CANCEL_NRE");

            return true;
          }
        }

        if (ShowConfirmDialog)
        {
          _total_balance = _promo_balance.TotalBalance;
          _message = Resource.String("STR_PROMOTION_CLEAR", _total_balance, _num_promos);
          _message = _message.Replace("\\r\\n", "\r\n");

          _dlg_rc = frm_message.Show(_message,
                                     Resource.String("STR_PROMOTION"),
                                     MessageBoxButtons.YesNo, Images.CashierImage.Question, Cashier.MainForm);
          if (_dlg_rc != DialogResult.OK)
          {
            _msg = "";

            return true;
          }
        }

        using (DB_TRX _db_trx = new DB_TRX())
        {
          _trx = _db_trx.SqlTransaction;

          if (!MultiPromos.Trx_UpdateAccountBalance(AccountId, MultiPromos.AccountBalance.Zero, out _ini_balance, out _play_session_id, _trx))
          {
            return false;
          }
          if (_play_session_id != 0)
          {
            _msg = Resource.String("STR_UC_CARD_IN_USE_ERROR");

            return true;
          }

          // The account is not in session, ensure the promotions are not in session.
          if (!MultiPromos.Trx_UnlinkPromotionsInSession(AccountId, 0, 0, _trx))
          {
            return false;
          }

          if (!MultiPromos.Trx_AccountPromotionsAction(MultiPromos.AccountPromotionsAction.CancelByPlayer, AccountId,
                                                       out _cancelled, out _num_promos_cancelled, _trx))
          {
            return false;
          }

          if (_promo_balance.TotalBalance != _cancelled.TotalBalance
              || _num_promos != _num_promos_cancelled)
          {
            return false;
          }

          if (_cancelled.TotalBalance > 0)
          {
            if (!MultiPromos.Trx_UpdateAccountBalance(AccountId, MultiPromos.AccountBalance.Negate(_cancelled), out _fin_balance, _trx))
            {
              return false;
            }

            if (_cancelled.TotalRedeemable > 0)
            {
              if (!MultiPromos.Trx_AccountPromotionCost(AccountId, ACCOUNTS_PROMO_COST.UPDATE_ON_REDEEM, _cancelled.TotalRedeemable, _trx))
              {
                return false;
              }
            }

            if (!MultiPromos.Trx_SetActivity(AccountId, _trx))
            {
              return false;
            }

            _session_info = Cashier.CashierSessionInfo();

            _account_mov_table = new AccountMovementsTable(_session_info);
            _account_mov_table.Add(0, AccountId, MovementType.PromoCancel, _ini_balance.TotalBalance, _cancelled.TotalBalance, 0, _fin_balance.TotalBalance);
            if (!_account_mov_table.Save(_trx))
            {
              return false;
            }

            _cashier_mov_table = new CashierMovementsTable(_session_info);
            _cashier_mov_table.Add(0, CASHIER_MOVEMENT.PROMO_CANCEL, _cancelled.TotalBalance, AccountId, ExternalTrackData);
            if (!_cashier_mov_table.Save(_trx))
            {
              return false;
            }
          }

          _db_trx.Commit();

          _msg = "";

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {
        if (!String.IsNullOrEmpty(_msg) && ShowConfirmDialog)
        {
          frm_message.Show(_msg,
                           Resource.String("STR_APP_GEN_MSG_ERROR"),
                           MessageBoxButtons.OK,
                           MessageBoxIcon.Error,
                           Cashier.MainForm);
        }
      }

      return false;
    }

    public static Boolean DB_PromotionRedeem(Int64 AccountId, String ExternalTrackData)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        if (DB_PromotionRedeem(AccountId, ExternalTrackData, true, _db_trx.SqlTransaction))
        {
          _db_trx.Commit();

          return true;
        }
      }

      return false;
    }
    public static Boolean DB_PromotionRedeem(Int64 AccountId, String ExternalTrackData, Boolean ShowConfirmDialog, SqlTransaction Trx)
    {
      DialogResult _dlg_rc;
      String _message;
      MultiPromos.AccountBalance _ini_balance;
      MultiPromos.AccountBalance _to_be_converted;
      MultiPromos.AccountBalance _converted;
      MultiPromos.AccountBalance _fin_balance;
      SqlTransaction _trx;
      String _msg;
      Int64 _play_session_id;
      Currency _total_balance;
      Int32 _num_promos;
      CashierMovementsTable _cashier_mov_table;
      AccountMovementsTable _account_mov_table;
      CashierSessionInfo _session_info;

      // "No se pudo convertir la promoci�n."
      _msg = Resource.String("STR_PROMOTION_ERROR_MSG_CANT_CONVERT");

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _trx = _db_trx.SqlTransaction;

          _trx.Save("Trx_PromotionToRedeemable");
          if (!MultiPromos.Trx_AccountPromotionsAction(MultiPromos.AccountPromotionsAction.NotRedeemableToRedeemable, AccountId,
                                                       out _to_be_converted, out _num_promos, _trx))
          {
            return false;
          }
          _trx.Rollback("Trx_PromotionToRedeemable");


          if (_to_be_converted.TotalBalance <= 0)
          {
            // "No hay cr�ditos no redimibles que puedan ser convertidos a redimible."
            _msg = Resource.String("STR_PROMOTION_ERROR_MSG_CANT_CONVERT_NRE_TO_RE");


            return true;
          }
        }

        if (ShowConfirmDialog)
        {
          _total_balance = _to_be_converted.TotalBalance;
          _message = Resource.String("STR_PROMOTION_TO_REDEEMABLE", _total_balance);
          _message = _message.Replace("\\r\\n", "\r\n");
          _dlg_rc = frm_message.Show(_message,
                                     Resource.String("STR_PROMOTION"),
                                     MessageBoxButtons.YesNo, Images.CashierImage.Question, Cashier.MainForm);
          if (_dlg_rc != DialogResult.OK)
          {
            _msg = "";

            return true;
          }
        }

        using (DB_TRX _db_trx = new DB_TRX())
        {
          _trx = _db_trx.SqlTransaction;

          if (!MultiPromos.Trx_UpdateAccountBalance(AccountId, MultiPromos.AccountBalance.Zero, out _ini_balance, out _play_session_id, _trx))
          {
            return false;
          }
          if (_play_session_id != 0)
          {
            _msg = Resource.String("STR_UC_CARD_IN_USE_ERROR");

            return true;
          }

          if (!MultiPromos.Trx_AccountPromotionsAction(MultiPromos.AccountPromotionsAction.NotRedeemableToRedeemable, AccountId,
                                                       out _converted, out _num_promos, _trx))
          {
            return false;
          }
          if (_converted.TotalBalance != _to_be_converted.TotalBalance)
          {
            return false;
          }

          _converted.PromoRedeemable = _converted.PromoNotRedeemable;
          _converted.PromoNotRedeemable = -_converted.PromoNotRedeemable;
          if (!MultiPromos.Trx_UpdateAccountBalance(AccountId, _converted, out _fin_balance, _trx))
          {
            return false;
          }
          _converted.PromoNotRedeemable = -_converted.PromoNotRedeemable;


          if (!MultiPromos.Trx_SetActivity(AccountId, _trx))
          {
            return false;
          }

          _session_info = Cashier.CashierSessionInfo();

          _account_mov_table = new AccountMovementsTable(_session_info);
          _account_mov_table.Add(0, AccountId, MovementType.PromoToRedeemable, _ini_balance.TotalBalance, _converted.PromoNotRedeemable, _converted.PromoRedeemable, _fin_balance.TotalBalance);
          if (!_account_mov_table.Save(_trx))
          {
            return false;
          }

          _cashier_mov_table = new CashierMovementsTable(_session_info);
          _cashier_mov_table.Add(0, CASHIER_MOVEMENT.PROMO_TO_REDEEMABLE, _converted.PromoNotRedeemable, AccountId, ExternalTrackData);
          if (!_cashier_mov_table.Save(_trx))
          {
            return false;
          }

          _db_trx.Commit();

          _msg = "";

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {
        if (!String.IsNullOrEmpty(_msg) && ShowConfirmDialog)
        {
          frm_message.Show(_msg,
                           Resource.String("STR_APP_GEN_MSG_ERROR"),
                           MessageBoxButtons.OK,
                           MessageBoxIcon.Error,
                           Cashier.MainForm);
        }
      }

      return false;
    }

    #endregion

    #region Session Functions

    // TODO: out ini & final_balance. Return boolean indicating ok or error.
    public static Currency UpdateSessionBalance(SqlTransaction SQLTransaction, Int64 CurrentSessionId, Currency Increment)
    {
      return Common.Cashier.UpdateSessionBalance(SQLTransaction, CurrentSessionId, Increment);
    }

    public static Currency UpdateSessionBalance(SqlTransaction SQLTransaction, Int64 CurrentSessionId, Currency Increment, String CurrencyCode, CurrencyExchangeType CurrencyType)
    {
      return Common.Cashier.UpdateSessionBalance(SQLTransaction, CurrentSessionId, Increment, CurrencyCode, CurrencyType);
    }

    public static Int64 GetSessionId(SqlTransaction SqlTrx, Cashier.BankMode Mode, Int64 TerminalId, Int32 UserId)
    {
      String sql_str;
      String sql_name;
      Int64 sql_param;
      Int64 session_id;
      SqlCommand sql_command;

      session_id = 0;

      try
      {
        switch (Mode)
        {
          case Cashier.BankMode.PerUser:
            sql_str = "SELECT ISNULL(MAX(cs_session_id), 0) FROM cashier_sessions WHERE(cs_status = @pStatusOpen) AND (cs_user_id = @p1)";
            sql_name = "cs_user_id";
            sql_param = UserId;
            break;

          case Cashier.BankMode.PerTerminal:
            sql_str = "SELECT ISNULL(MAX(cs_session_id), 0) FROM cashier_sessions WHERE(cs_status = @pStatusOpen) AND (cs_cashier_id = @p1)";
            sql_name = "cs_cashier_id";
            sql_param = TerminalId;
            break;

          default:
            return 0;
        }

        try
        {
          sql_command = new SqlCommand(sql_str);
          sql_command.Connection = SqlTrx.Connection;
          sql_command.Transaction = SqlTrx;

          sql_command.Parameters.Add("@p1", SqlDbType.BigInt, 8, sql_name).Value = sql_param;
          sql_command.Parameters.Add("@pStatusOpen", SqlDbType.Int).Value = CASHIER_SESSION_STATUS.OPEN;

          session_id = (Int64)sql_command.ExecuteScalar();
        }
        catch
        {

        }
        finally
        {
          // TODO Close connection / trx etc

        }
      }
      catch
      {
      }

      return session_id;

    } // GetSessionId


    public static CASHIER_STATUS ReadCashierStatusWithoutUpdateCashierSession(Int64 SessionId)
    {
      StringBuilder _sb;

      if (SessionId == 0)
      {
        return CASHIER_STATUS.CLOSE;
      }

      try
      {
        // Get Sql Connection
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _sb = new StringBuilder();
          _sb.AppendLine(" SELECT   COUNT(*) ");
          _sb.AppendLine("   FROM   CASHIER_SESSIONS ");
          _sb.AppendLine("  WHERE   CS_SESSION_ID = @pSessionId ");
          _sb.AppendLine("    AND   CS_STATUS     = @pSessionStatus ");

          using (SqlCommand _cmd = new SqlCommand(_sb.ToString()))
          {
            _cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = SessionId;
            _cmd.Parameters.Add("@pSessionStatus", SqlDbType.Int).Value = CASHIER_STATUS.OPEN;

            if ((Int32)_db_trx.ExecuteScalar(_cmd) > 0)
            {
              return CASHIER_STATUS.OPEN;
            }

            return CASHIER_STATUS.CLOSE;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return CASHIER_STATUS.ERROR;
      }

    } // ReadCashierStatus

    public static CASHIER_STATUS ReadCashierStatus(Int64 SessionId)
    {
      StringBuilder _sb;

      if (SessionId == 0)
      {
        return CASHIER_STATUS.CLOSE;
      }

      try
      {
        // Get Sql Connection
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _sb = new StringBuilder();
          _sb.AppendLine(" SELECT   COUNT(*) ");
          _sb.AppendLine("   FROM   CASHIER_SESSIONS ");
          _sb.AppendLine("  WHERE   CS_SESSION_ID = @pSessionId ");
          _sb.AppendLine("    AND   CS_STATUS     = @pSessionStatus ");

          using (SqlCommand _cmd = new SqlCommand(_sb.ToString()))
          {
            _cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = SessionId;
            _cmd.Parameters.Add("@pSessionStatus", SqlDbType.Int).Value = CASHIER_STATUS.OPEN;

            if ((Int32)_db_trx.ExecuteScalar(_cmd) > 0)
            {
              return CASHIER_STATUS.OPEN;
            }

            // RMS & RCI 11-OCT-2013: Reset Cashier SessionId when Cashier Status is CLOSED.
            Cashier.SetUserSession(0);

            return CASHIER_STATUS.CLOSE;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return CASHIER_STATUS.ERROR;
      }

    } // ReadCashierStatus

    public static Currency CalculatePendingAmount(Int64 CurrentSessionId)
    {
      SqlConnection sql_conn;
      SqlTransaction sql_trx;
      SqlDataReader sql_data_reader;
      SqlCommand sql_command;
      String sql_str;
      Boolean _gp_pending_cash_partial;

      Currency mb_deposit_amount = 0;
      Currency mb_cash_in_amount = 0;
      Currency mb_pending_amount = 0;
      Currency mb_balance_lost = 0;
      Currency mb_partial_pending = 0;
      Currency mb_excess_amount = 0;

      _gp_pending_cash_partial = MobileBank.GP_GetRegisterShortfallOnDeposit();

      try
      {
        // Get Sql Connection 
        sql_conn = WSI.Common.WGDB.Connection();
        sql_trx = sql_conn.BeginTransaction();

        sql_str = "   SELECT    mbm_type                              AS TYPE     " +
                  "           , SUM(mbm_sub_amount + mbm_add_amount)  AS AMOUNT   " +
                  "     FROM    mb_movements                                      " +
                  "    WHERE    mbm_cashier_session_id = @p1                      " +
                  "      AND    mbm_type in (@p2, @p3, @p4, @p5, @p6)             " +
                  " GROUP BY    mbm_type                                          ";

        sql_command = new SqlCommand(sql_str);
        sql_command.Connection = sql_trx.Connection;
        sql_command.Transaction = sql_trx;

        sql_command.Parameters.Add("@p1", SqlDbType.BigInt).Value = CurrentSessionId;
        sql_command.Parameters.Add("@p2", SqlDbType.SmallInt).Value = (Int16)MBMovementType.TransferCredit;
        sql_command.Parameters.Add("@p3", SqlDbType.SmallInt).Value = (Int16)MBMovementType.DepositCash;
        sql_command.Parameters.Add("@p4", SqlDbType.SmallInt).Value = (Int16)MBMovementType.CloseSessionWithCashLost;
        sql_command.Parameters.Add("@p5", SqlDbType.SmallInt).Value = (Int16)MBMovementType.CashExcess;
        sql_command.Parameters.Add("@p6", SqlDbType.SmallInt).Value = (Int16)MBMovementType.CashShortFall;

        sql_data_reader = sql_command.ExecuteReader();

        while (sql_data_reader.Read())
        {
          switch ((MBMovementType)(sql_data_reader["TYPE"]))
          {
            case MBMovementType.TransferCredit:
              mb_cash_in_amount += Convert.ToDecimal(sql_data_reader["AMOUNT"]);
              break;
            case MBMovementType.CloseSessionWithCashLost:
              mb_balance_lost += Convert.ToDecimal(sql_data_reader["AMOUNT"]);
              break;
            case MBMovementType.DepositCash:
              mb_deposit_amount += Convert.ToDecimal(sql_data_reader["AMOUNT"]);
              break;
            case MBMovementType.CashExcess:
              mb_excess_amount += Convert.ToDecimal(sql_data_reader["AMOUNT"]);
              break;
            case MBMovementType.CashShortFall:
              mb_partial_pending += Convert.ToDecimal(sql_data_reader["AMOUNT"]);
              break;
            default:
              Log.Error("CalculatePendingAmount. Error in movement type: " + Convert.ToInt64(sql_data_reader["TYPE"]));
              break;
          } // switch

        } // while

        sql_data_reader.Close();
      }
      catch (Exception ex)
      {
        Log.Exception(ex);

        return 0;
      }
      finally
      {
        mb_pending_amount = (mb_cash_in_amount + mb_excess_amount) - mb_deposit_amount - mb_balance_lost;

        if (_gp_pending_cash_partial)
        {
          mb_pending_amount -= mb_partial_pending;
          mb_pending_amount += mb_balance_lost;
        }

      } // finally

      return mb_pending_amount;

    } // CalculatePendingAmount

    //------------------------------------------------------------------------------
    // PURPOSE: Get the opening date for a given cashier session
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Int64 SessionId
    //          - SqlTransaction Trx
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - DateTime
    // 
    public static DateTime GetSessionOpeningDate(Int64 SessionId, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Object _obj;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT   CS_OPENING_DATE             ");
        _sb.AppendLine("   FROM   CASHIER_SESSIONS            ");
        _sb.AppendLine("  WHERE   CS_SESSION_ID = @pSessionId ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = SessionId;
          _obj = _cmd.ExecuteScalar();

          if (_obj != null && _obj != DBNull.Value)
          {
            return (DateTime)_obj;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return DateTime.MinValue;

    } // GetSessionOpeningDate

    //------------------------------------------------------------------------------
    // PURPOSE: Get the Gaming Day for a given cashier session
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Int64 SessionId
    //          - SqlTransaction Trx
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - DateTime
    // 
    public static DateTime GetSessionGamingDay(Int64 SessionId, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Object _obj;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT   CS_GAMING_DAY               ");
        _sb.AppendLine("   FROM   CASHIER_SESSIONS            ");
        _sb.AppendLine("  WHERE   CS_SESSION_ID = @pSessionId ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = SessionId;
          _obj = _cmd.ExecuteScalar();

          if (_obj != null && _obj != DBNull.Value)
          {
            return (DateTime)_obj;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return DateTime.MinValue;

    } // GetSessionGamingDay

    #endregion Session Functions

    #region General Parameters

    /// <summary>
    /// Get Terminal cashier mode:
    ///  - PerUser: 
    ///  - PerTerminal:
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public static Cashier.BankMode ReadBankMode(SqlTransaction SqlTrx)
    {
      string _bank_mode;

      _bank_mode = Misc.ReadGeneralParams("Cashier", "CashierMode", SqlTrx);

      switch (_bank_mode)
      {
        case "PerUser":
          return Cashier.BankMode.PerUser;

        case "PerTerminal":
          return Cashier.BankMode.PerTerminal;

        default:
          Log.Message("Error reading DATABASE");

          return Cashier.BankMode.Unknown;

      }
    } // ReadBankMode

    /// <summary>
    /// Get cashier current language string.
    /// </summary>
    /// <returns></returns>
    public static String CashierLanguage()
    {
      return Misc.ReadGeneralParams("Cashier", "Language");
    } // CashierLanguage

    /// <summary>
    /// Get site identifier from general parameters..
    /// </summary>
    /// <returns>Site id.</returns>
    public static Boolean ReadSiteId(ref Int32 SiteId)
    {
      return Int32.TryParse(Misc.ReadGeneralParams("Site", "Identifier"), out SiteId);
    } // ReadSiteId

    #endregion


    static public System.Drawing.Color GetInactivityColor(Int32 ElapsedSeconds)
    {
      Currency threshold;
      Currency x;

      threshold = 150;  // Seconds

      if (ElapsedSeconds <= threshold)
      {
        return System.Drawing.Color.White;
      }

      if (ElapsedSeconds <= 2 * threshold)
      {
        x = ElapsedSeconds - threshold;
        x = (255 * x) / threshold;

        if (x > 255)
        {
          x = 255;
        }
        if (x < 0)
        {
          x = 0;
        }

        return System.Drawing.Color.FromArgb(255, 255, (int)(255 - x));
      }

      if (ElapsedSeconds <= 3 * threshold)
      {
        x = ElapsedSeconds - 2 * threshold;
        x = (255 * x) / threshold;

        if (x > 255)
        {
          x = 255;
        }
        if (x < 0)
        {
          x = 0;
        }

        return System.Drawing.Color.FromArgb(255, (int)(255 - x), 0);
      }

      return System.Drawing.Color.Red;

    } // GetInactivityColor

    //------------------------------------------------------------------------------
    // PURPOSE: Get Reported Balance
    // 
    //  PARAMS:
    //      - INPUT:
    //          - PlaySessionId
    //
    //      - OUTPUT:
    //          - ReportedBalance
    //
    // RETURNS:
    //      - true: Ok
    //      - false: select failed
    // 
    //   NOTES:
    //
    public static void DB_GetReportedBalance(Int64 PlaySessionId, out Decimal ReportedBalance)
    {
      String _str_sql;
      SqlCommand _sql_cmd;
      Object _obj;

      ReportedBalance = 0;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          // DDM 07-MAY-2012: First, we checked if reported balance, is in play sessions.
          _str_sql = "  SELECT   PS_REPORTED_BALANCE_MISMATCH ";
          _str_sql += "   FROM   PLAY_SESSIONS ";
          _str_sql += "  WHERE   PS_PLAY_SESSION_ID = @pPlaySessionId ";

          using (_sql_cmd = new SqlCommand(_str_sql, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).Value = PlaySessionId;

            _obj = _sql_cmd.ExecuteScalar();
          }
          if (_obj != null && _obj != DBNull.Value)
          {
            ReportedBalance = (Decimal)_obj;

            return;
          }

          // DDM 07-MAY-2012: Otherwise, we search value in AUDIT_3GS  
          _str_sql = " SELECT TOP 1  A3GS_BALANCE";
          _str_sql += "        FROM  AUDIT_3GS ";
          _str_sql += "       WHERE  A3GS_SESSION_ID = @pPlaySessionId ";
          _str_sql += "    ORDER BY  A3GS_ID DESC ";

          using (_sql_cmd = new SqlCommand(_str_sql, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).Value = PlaySessionId;

            _obj = _sql_cmd.ExecuteScalar();
          }
          if (_obj != null && _obj != DBNull.Value)
          {
            ReportedBalance = (Decimal)_obj;

            return;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        ReportedBalance = 0;
      }
    } // DB_GetReportedBalance

    //------------------------------------------------------------------------------
    // PURPOSE : Gathers the data to generate the required Witholding documents 
    // 
    //  PARAMS :
    //      - INPUT :
    //          - CardData : Card to be checked
    //
    //      - OUTPUT :
    //
    // RETURNS :
    // 
    //   NOTES :
    static public Boolean EnterWitholdingData(Form Parent, CardData CardData, out WithholdData Withhold)
    {
      frm_withholding _frm_witholding;
      frm_yesno _frm_shader;
      DialogResult _rc;
      //String          _error_str;
      DialogResult _answer;
      string _message;

      Withhold = new WithholdData(CardData);

      try
      {
        if (Utils.IsTitoMode())
        {
          _message = Resource.String("STR_FRM_WITHOLDING_011");
        }
        else
        {
          _message = Resource.String("STR_FRM_WITHOLDING_007");
        }

        _message = _message.Replace("\\r\\n", "\r\n");

        _answer = frm_message.Show(_message,
                                    Resource.String("STR_APP_GEN_MSG_WARNING"),
                                    MessageBoxButtons.YesNo,
                                    MessageBoxIcon.Warning,
                                    Parent);

        if (_answer != DialogResult.OK)
        {
          // Requested data will not be entered => no further process
          return false;
        }

        //    - Enter required data to generate the Witholding documents
        _frm_shader = new frm_yesno();
        _frm_shader.Opacity = 0.6;

        _frm_shader.Show(Parent);

        _frm_witholding = new frm_withholding(Withhold);

        try
        {
          if (WindowManager.StartUpMode == WindowManager.StartUpModes.Explorer)
          {
            _frm_witholding.Location = new Point(WindowManager.GetFormCenterLocation(_frm_shader).X - (_frm_witholding.Width / 2),
                                                 _frm_shader.Location.Y + 20);
          }

          _rc = _frm_witholding.ShowDialog();
        }
        finally
        {
          // ICS 14-APR-2014 Free the object
          _frm_witholding.Dispose();

          _frm_shader.Hide();
          _frm_shader.Dispose();
        }

        if (_rc != DialogResult.OK)
        {
          return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
    } // EnterWitholdingData

    //------------------------------------------------------------------------------
    // PURPOSE : Asks the user for a pin number in case the system is setup that way. 
    //           This Pin request is called from selected functionallities
    //  PARAMS :
    //      - INPUT :
    //          - ThisForm: Callinf form
    //          - OperatonType
    //          - Card: Customer card
    //          - TotalToPay? TODO
    //      - OUTPUT :
    //
    // RETURNS :
    // 
    //   NOTES :
    public static Boolean RequestPin(Form ThisForm, Accounts.PinRequestOperationType OperationType, CardData Card, Currency TotalToPay)
    {
      //String _value;
      frm_yesno _shader;
      frm_player_enter_pin _player_enter_pin;
      DialogResult _rc;

      if (!Accounts.DoesActionRequirePlayerPinRequest(Accounts.PinRequestSource.CASHIER
                                                    , OperationType
                                                    , String.IsNullOrEmpty(Card.PlayerTracking.HolderName)))
      {
        return true;
      }

      _shader = new frm_yesno();
      _shader.Opacity = 0.6f;
      _shader.Show(ThisForm);

      _player_enter_pin = new frm_player_enter_pin(Card.AccountId, TotalToPay, OperationType);
      _rc = _player_enter_pin.ShowDialog(_shader);

      _shader.Hide();
      _shader.Dispose();

      // ICS 14-APR-2014 Free the object
      _player_enter_pin.Dispose();

      switch (_rc)
      {
        case DialogResult.Cancel:
          return false;

        case DialogResult.Abort:
          ThisForm.Close();

          return false;

        case DialogResult.OK:
          break;
      }

      return true;
    } // RequestPin

    static public Boolean EnterOrderPaymentData(Form Parent, CardData CardData, Currency Cashable, Currency TotalAmount, WithholdData Withhold, out PaymentOrder PayOrder)
    {
      frm_payment_order _frm_payment;
      frm_yesno _frm_shader;
      DialogResult _rc;
      String _error_str;
      WithholdData _withhold;
      List<ProfilePermissions.CashierFormFuncionality> _permissions_list;
      Boolean _has_witholding;

      PayOrder = new PaymentOrder();
      _has_witholding = false;

      try
      {
        _permissions_list = new List<ProfilePermissions.CashierFormFuncionality>();
        _permissions_list.Add(ProfilePermissions.CashierFormFuncionality.PaymentOrder);

        if (Withhold != null)
        {
          _permissions_list.Add(ProfilePermissions.CashierFormFuncionality.WitholdDocument);
        }

        // Check if current user is an authorized user
        if (!ProfilePermissions.CheckPermissionList(_permissions_list,
                                                    ProfilePermissions.TypeOperation.RequestPasswd,
                                                    Parent,
                                                    Cashier.AuthorizedByUserId,
                                                    out _error_str))
        {
          return false;
        }

        if (Withhold == null)
        {
          _withhold = new WithholdData(CardData);
          _has_witholding = false;
        }
        else
        {
          _withhold = Withhold;
          _has_witholding = true;
        }

        _frm_shader = new frm_yesno();
        _frm_shader.Opacity = 0.6;
        _frm_shader.Show(Parent);

        _rc = DialogResult.Abort;

        _frm_payment = new frm_payment_order(_withhold, CardData, Cashable, _has_witholding);

        try
        {
          if (!_frm_payment.IsDisposed)
          {
            if (WindowManager.StartUpMode == WindowManager.StartUpModes.Explorer)
            {
              _frm_payment.Location = new Point(WindowManager.GetFormCenterLocation(_frm_shader).X - (_frm_payment.Width / 2), _frm_shader.Location.Y + 20);
            }
            _rc = _frm_payment.Show(TotalAmount, out PayOrder);
          }
        }
        finally
        {
          // ICS 14-APR-2014 Free the object
          _frm_payment.Dispose();

          _frm_shader.Hide();
          _frm_shader.Dispose();
        }

        if (_rc != DialogResult.OK)
        {
          return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE:       Operation Cash Advance
    // 
    //  PARAMS:
    //
    //      - INPUT:
    //          - AccountId
    //          - ExchangeResult
    //          - InputParams
    //      - OUTPUT:
    //          - ErrorMessage
    //
    // RETURNS: 
    //      - Result of Operation (True -> Ok, False -> Unable to do it)
    // 
    public static Boolean DB_CashAdvance(RechargeInputParameters InputParams, CurrencyExchangeResult ExchangeResult, PaymentThresholdAuthorization PaymentThresholdAuthorization, out String ErrorMessage)
    {
      long _operation_id;
      CashierSessionInfo _cashier_session_info;
      Currency _session_balance;
      ArrayList _voucher_list;
      ArrayList _voucher_list_tpv;

      // ETP: PinPad vars:
      Boolean _return_error;
      Boolean _is_pinpad_recharge;
      frm_tpv _form_tpv;
      PinPadInput _pinpad_input;
      PinPadOutput _pinpad_output;

      ErrorMessage = String.Empty;

      _session_balance = 0;
      _voucher_list = null;
      _voucher_list_tpv = new ArrayList();
      _voucher_list = new ArrayList();

      CardData _card_data;
      _card_data = new CardData();

      CardData.DB_CardGetAllData(InputParams.AccountId, _card_data, false);

      _is_pinpad_recharge = Misc.IsPinPadEnabled() && Common.Cashier.ReadPinPadEnabled() && ExchangeResult != null && ExchangeResult.InType == CurrencyExchangeType.CARD;
      _cashier_session_info = Cashier.CashierSessionInfo();

      _pinpad_output = new PinPadOutput();

      if (_is_pinpad_recharge)
      {
        if (!InternetConnection.GetServiceInternetConnectionStatus("WCP"))
        {
          ErrorMessage = Resource.String("STR_PINPAD_CANCELLED_OPERATION_ERROR_CONNEXION");
          return false;
        }

        _pinpad_input = new PinPadInput();

        using (DB_TRX _db_trx = new DB_TRX())
        {
          Int64 _dummy_operation_id;
          ArrayList _dummy_voucher_list;

          if (!Common_CashAdvance(InputParams, ExchangeResult, _db_trx.SqlTransaction, out _dummy_operation_id, out _dummy_voucher_list, out ErrorMessage)) // Check if operation can be done.
          {
            _db_trx.Rollback();
            return false;
          }

          _db_trx.Rollback();
        }

        _pinpad_input.Amount = ExchangeResult;
        _pinpad_input.Card = _card_data;
        _pinpad_input.TerminalName = _cashier_session_info.TerminalName;

        _form_tpv = new frm_tpv(_pinpad_input);
        if (!_form_tpv.Show(out _pinpad_output)) // 20/10/2016 ETP: Pin Pad payement
        {
          String _message = String.Empty;

          if (_form_tpv.OperationCanceled)
          {
            _message = Resource.String("STR_PINPAD_CANCELED_BY_USER");
          }
          else
          {
            _message = _pinpad_output.PinPadTransaction.ErrorMessage;
          }

          ErrorMessage = _message;

          _form_tpv.Dispose();
          return false;
        }
        _form_tpv.Dispose();
        // 20/10/2016 ETP: Transaction is Pending of udpating wigos data.
      }

      _return_error = false;

      using (DB_TRX _db_trx = new DB_TRX())
      {
        if (Common_CashAdvance(InputParams, ExchangeResult, _db_trx.SqlTransaction, out _operation_id, out _voucher_list, out ErrorMessage))
        {
          if (_is_pinpad_recharge)
          {
            PinPadTransactions _transaction;

            _transaction = _pinpad_output.PinPadTransaction;

            _transaction.OperationId = _operation_id;
            _transaction.StatusCode = PinPadTransactions.STATUS.APPROVED;  //  20/10/2016 ETP: PinPad is payed OK.

            if (!_transaction.DB_Insert(_db_trx.SqlTransaction) && !_return_error)
            {
              _return_error = true;
            }

            _transaction.UpdateCashierSessionHasPinPadOperation(_cashier_session_info.CashierSessionId, _db_trx.SqlTransaction);

            if (!PinPadTransactions.DeleteFromUndoPinPad(_transaction.ControlNumber, _db_trx.SqlTransaction) && !_return_error)
            {
              _return_error = true;
            }

            if (!_pinpad_output.PinPadTransaction.CreateVoucher(ExchangeResult, _pinpad_output, _db_trx.SqlTransaction, out _voucher_list_tpv) && !_return_error)
            {
              _return_error = true;
            }
          }

          if (PaymentThresholdAuthorization != null && PaymentThresholdAuthorization.OutputThreshold > 0 && ExchangeResult.InAmount >= PaymentThresholdAuthorization.OutputThreshold)
          {
            PaymentThresholdAuthorization.SaveThresholdOperation(_operation_id, OperationCode.CASH_ADVANCE, _db_trx.SqlTransaction);
          }

          if (!_return_error)
          {
            _db_trx.Commit();
          }
        }
        else
        {
          _return_error = true;
        }
      }

      if (_return_error)
      {
        // 20/10/2016 ETP: Undo PinPad Operations if and error has ocurred:
        if (_is_pinpad_recharge)
        {
          using (DB_TRX _db_trx = new DB_TRX())
          {
            _pinpad_output.PinPadTransaction.OperationId = _operation_id;
            _pinpad_output.PinPadTransaction.StatusCode = PinPadTransactions.STATUS.ERROR;
            _pinpad_output.PinPadTransaction.DB_Insert(_db_trx.SqlTransaction);
            _pinpad_output.PinPadTransaction.UpdateCashierSessionHasPinPadOperation(_cashier_session_info.CashierSessionId, _db_trx.SqlTransaction);
            PinPadTransactions.UpdateDateUndoPinPad(_pinpad_output.PinPadTransaction.ControlNumber, _db_trx.SqlTransaction);
            _db_trx.Commit();
          }
        }

        return false;
      }

      _voucher_list.AddRange(_voucher_list_tpv);
      VoucherPrint.Print(_voucher_list);


      return true;
    }

    public static Boolean GetCashierMovementsByAccountAndType(Int64 AccountId, List<CASHIER_MOVEMENT> TypeList, Boolean FromTodayOpening, Boolean NotShowUndoStatus,
                                                              out List<CashierMovementsInfo> CashierMovementsInfoList, SqlTransaction Trx)
    {
      StringBuilder _sb;
      DateTime _today_opening;

      CashierMovementsInfoList = new List<CashierMovementsInfo>();

      try
      {
        _today_opening = Misc.TodayOpening();
        List<string> TypeStringList = TypeList.ConvertAll<string>(delegate(CASHIER_MOVEMENT i) { return ((int)i).ToString(); });

        _sb = new StringBuilder();
        _sb.AppendLine("SELECT    CM_MOVEMENT_ID               "); //0
        _sb.AppendLine("         ,CM_SESSION_ID                "); //1
        _sb.AppendLine("         ,CM_CASHIER_ID                "); //2
        _sb.AppendLine("         ,CM_USER_ID                   "); //3
        _sb.AppendLine("         ,CM_DATE                      "); //4
        _sb.AppendLine("         ,CM_TYPE                      "); //5
        _sb.AppendLine("         ,CM_MONEY_TYPE                "); //6
        _sb.AppendLine("         ,CM_ACCOUNT_MOVEMENT_ID       "); //7
        _sb.AppendLine("         ,CM_INITIAL_BALANCE           "); //8
        _sb.AppendLine("         ,CM_SUB_AMOUNT                "); //9
        _sb.AppendLine("         ,CM_ADD_AMOUNT                "); //10
        _sb.AppendLine("         ,CM_FINAL_BALANCE             "); //11
        _sb.AppendLine("         ,CM_USER_NAME                 "); //12
        _sb.AppendLine("         ,CM_CASHIER_NAME              "); //13
        _sb.AppendLine("         ,CM_CARD_TRACK_DATA           "); //14
        _sb.AppendLine("         ,CM_ACCOUNT_ID                "); //15
        _sb.AppendLine("         ,CM_OPERATION_ID              "); //16
        _sb.AppendLine("         ,CM_DETAILS                   "); //17
        _sb.AppendLine("         ,CM_CURRENCY_ISO_CODE         "); //18
        _sb.AppendLine("         ,CM_AUX_AMOUNT                "); //19
        _sb.AppendLine("         ,CM_UNDO_STATUS               "); //20
        _sb.AppendLine("         ,CM_CURRENCY_DENOMINATION     "); //21
        _sb.AppendLine("         ,CM_GAMING_TABLE_SESSION_ID   "); //22
        _sb.AppendLine("         ,CM_CHIP_ID                   "); //23
        _sb.AppendLine("         ,CM_CAGE_CURRENCY_TYPE        "); //24
        _sb.AppendLine("         ,CM_CHIPS_SALE_DENOMINATION   "); //25
        _sb.AppendLine("         ,CM_RELATED_ID                "); //26
        _sb.AppendLine("  FROM    CASHIER_MOVEMENTS            ");
        _sb.AppendLine(" WHERE    CM_ACCOUNT_ID = @pAccountId  ");
        _sb.AppendLine("   AND    CM_TYPE IN (" + String.Join(",", TypeStringList.ToArray()) + ")");

        if (FromTodayOpening)
        {
          _sb.AppendLine("    AND   CM_DATE >= @pTodayOpening ");
        }
        if (NotShowUndoStatus)
        {
          _sb.AppendLine("    AND   (CM_UNDO_STATUS IS NULL OR CM_UNDO_STATUS = @pNone) ");
        }

        _sb.AppendLine("ORDER BY    CM_DATE DESC  ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _cmd.Parameters.Add("@pTodayOpening", SqlDbType.DateTime).Value = Misc.TodayOpening();
          _cmd.Parameters.Add("@pNone", SqlDbType.Int).Value = UndoStatus.None;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            while (_reader.Read())
            {
              CashierMovementsInfo _cashier_movement = new CashierMovementsInfo()
              {
                MovementId = (Int64)_reader[0],
                SessionId = (Int64)_reader[1],
                CashierId = (Int32)_reader[2],
                UserlId = (Int32)_reader[3],
                Date = (DateTime)_reader[4],
                Type = (CASHIER_MOVEMENT)_reader[5],
                MoneyType = (Int32)_reader[6],
                AccountMovementId = _reader.IsDBNull(7) ? (Int64?)null : (Int64)_reader[7],
                InitialBalance = (Decimal)_reader[8],
                SubAmount = (Decimal)_reader[9],
                AddAmount = (Decimal)_reader[10],
                FinalBalance = (Decimal)_reader[11],
                UserName = _reader.IsDBNull(12) ? null : (String)_reader[12],
                CashierName = _reader.IsDBNull(13) ? null : (String)_reader[13],
                TrackData = _reader.IsDBNull(14) ? null : (String)_reader[14],
                AccountId = _reader.IsDBNull(15) ? (Int64?)null : (Int64)_reader[15],
                OperationId = _reader.IsDBNull(16) ? (Int64?)null : (Int64)_reader[16],
                Details = _reader.IsDBNull(17) ? null : (String)_reader[17],
                CurrencyIsoCode = _reader.IsDBNull(18) ? null : (String)_reader[18],
                AuxAmount = _reader.IsDBNull(19) ? (Decimal?)null : (Decimal)_reader[19],
                UndoStatus = _reader.IsDBNull(20) ? (Int32?)null : (Int32)_reader[20],
                CurrencyDenomination = _reader.IsDBNull(21) ? (Decimal?)null : (Decimal)_reader[21],
                GamingTableSessionId = _reader.IsDBNull(22) ? (Int64?)null : (Int64)_reader[22],
                ChipId = _reader.IsDBNull(23) ? (Int64?)null : (Int64)_reader[23],
                CageCurrencyType = _reader.IsDBNull(24) ? (CageCurrencyType?)null : (CageCurrencyType)_reader[24],
                ChipsSaleDenomination = _reader.IsDBNull(25) ? (Int32?)null : (Int32)_reader[25],
                RelatedId = _reader.IsDBNull(26) ? (Int64?)null : (Int64)_reader[26],
              };

              CashierMovementsInfoList.Add(_cashier_movement);
            }

            return true;
          }
        }
      }
      catch (SqlException _sql_ex)
      {
        if (_sql_ex.Number != -2)
        {
          Log.Warning(" *** Sql Exception: Number: " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message.ToString() + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    public delegate Boolean EnterWitholdingDataDelegate(Form Parent, CardData CardData, out WithholdData Withhold);
    public delegate Boolean EnterOrderPaymentDataDelegate(Form Parent, CardData CardData, Currency Cashable, Currency TotalAmount, WithholdData Withhold, out PaymentOrder PayOrder);

    public static ENUM_TITO_OPERATIONS_RESULT WithholdAndPaymentOrderDialog(OperationCode OpCode,
                                                                            ref ParamsTicketOperation OperationParams,
                                                                            EnterWitholdingDataDelegate dWitholdingData,
                                                                            EnterOrderPaymentDataDelegate dOrderPaymentData,
                                                                            frm_amount_input.CheckPermissionsToRedeemDelegate dCheckPermissionsToRedeem)
    {
      return WithholdAndPaymentOrderDialog(OpCode, ref OperationParams, dWitholdingData, dOrderPaymentData, dCheckPermissionsToRedeem, false);
    }

    public static ENUM_TITO_OPERATIONS_RESULT WithholdAndPaymentOrderDialog(OperationCode OpCode,
                                                                            ref ParamsTicketOperation OperationParams,
                                                                            EnterWitholdingDataDelegate dWitholdingData,
                                                                            EnterOrderPaymentDataDelegate dOrderPaymentData,
                                                                            frm_amount_input.CheckPermissionsToRedeemDelegate dCheckPermissionsToRedeem,
                                                                            Boolean IsChipsPurchaseWithCashOut)
    {
      return WithholdAndPaymentOrderDialog(OpCode, ref OperationParams, dWitholdingData, dOrderPaymentData, dCheckPermissionsToRedeem, IsChipsPurchaseWithCashOut, null);
    }

    public static ENUM_TITO_OPERATIONS_RESULT WithholdAndPaymentOrderDialog(OperationCode OpCode,
                                                                            ref ParamsTicketOperation OperationParams,
                                                                            EnterWitholdingDataDelegate dWitholdingData,
                                                                            EnterOrderPaymentDataDelegate dOrderPaymentData,
                                                                            frm_amount_input.CheckPermissionsToRedeemDelegate dCheckPermissionsToRedeem,
                                                                            Boolean IsChipsPurchaseWithCashOut,
                                                                            Nullable<Decimal> Prize)
    {
      String _error_str;
      Boolean _witholding_required;
      Boolean[] _witholding_required_docs;
      List<ProfilePermissions.CashierFormFuncionality> _permissions_list;
      TYPE_CASH_REDEEM _cash_redeem;
      TYPE_CASH_REDEEM _a_redeem;
      TYPE_CASH_REDEEM _b_redeem;
      MultiPromos.PromoBalance _ini_balance;
      MultiPromos.PromoBalance _fin_balance;

      TYPE_SPLITS _splits;
      Boolean _is_tito_mode;
      Currency _cashier_current_balance;
      Boolean _amount_exceed_bank;
      Boolean _payment_order_allowed;
      Decimal _cashable_payment_order;
      CashAmount _cash;

      _cash = new CashAmount();
      _cash_redeem = new TYPE_CASH_REDEEM();

      OperationParams.withhold = null;
      OperationParams.payment_order = new PaymentOrder();

      if (!Split.ReadSplitParameters(out _splits))
      {
        return ENUM_TITO_OPERATIONS_RESULT.AUX_OPERATION_ERROR;
      }

      _ini_balance = new MultiPromos.PromoBalance(OperationParams.in_account.AccountBalance, OperationParams.in_account.PlayerTracking.TruncatedPoints);


      if (!_cash.CalculateCashAmount(CASH_MODE.TOTAL_REDEEM, _splits,
                                                          _ini_balance, 0,
                                                          IsChipsPurchaseWithCashOut,
                                                          OperationParams.is_apply_tax,
                                                          false,
                                                          OperationParams.in_account,
                                                          out _fin_balance,
                                                          out _cash_redeem,
                                                          out _a_redeem,
                                                          out _b_redeem))
      {
        return ENUM_TITO_OPERATIONS_RESULT.AUX_OPERATION_ERROR;
      }

      OperationParams.cash_redeem_total_paid = _cash_redeem.TotalPaid;

      decimal _prize = _cash_redeem.prize;
      if (Prize.HasValue)
        _prize = Prize.Value;

      if (!Witholding.CheckWitholding(_prize, out _witholding_required, out _witholding_required_docs))
      {
        return ENUM_TITO_OPERATIONS_RESULT.AUX_OPERATION_ERROR;
      }

      _is_tito_mode = WSI.Common.TITO.Utils.IsTitoMode();

      // Check out all the required permissions just once
      _permissions_list = new List<ProfilePermissions.CashierFormFuncionality>();

      switch (OpCode)
      {
        case OperationCode.CHIPS_PURCHASE:
          _permissions_list.Add(ProfilePermissions.CashierFormFuncionality.ChipsPurchase);
          break;

        case OperationCode.CHIPS_PURCHASE_WITH_CASH_OUT:
          _permissions_list.Add(ProfilePermissions.CashierFormFuncionality.ChipsPurchase);
          _permissions_list.Add(ProfilePermissions.CashierFormFuncionality.CardTotalRedeemCredit);
          break;

        case OperationCode.CASH_OUT:
          if (_is_tito_mode)
          {
            _permissions_list.Add(ProfilePermissions.CashierFormFuncionality.TITO_RedeemTicket);
          }
          else
          {
            _permissions_list.Add(ProfilePermissions.CashierFormFuncionality.CardTotalRedeemCredit);
          }
          break;

        case OperationCode.TITO_OFFLINE:
          _permissions_list.Add(ProfilePermissions.CashierFormFuncionality.TITO_RedeemTicketOffline);
          break;

        default:
          break;
      }

      if (dCheckPermissionsToRedeem != null)
      {
        if (!dCheckPermissionsToRedeem(OperationParams.in_account.AccountId, _cash_redeem.TotalPaid, Accounts.AmountByExternalControl(_cash_redeem), _permissions_list))
        {
          return ENUM_TITO_OPERATIONS_RESULT.AUX_OPERATION_ERROR;
        }
      }

      // Enter specific data if generation of Witholding documents is required
      if (_witholding_required)
      {
        _permissions_list.Add(ProfilePermissions.CashierFormFuncionality.WitholdDocument);
      }

      // Check if current user is an authorized user
      if (!ProfilePermissions.CheckPermissionList(_permissions_list,
                                                  ProfilePermissions.TypeOperation.RequestPasswd,
                                                  OperationParams.in_window_parent,
                                                  Cashier.AuthorizedByUserId,
                                                  out _error_str))
      {
        return ENUM_TITO_OPERATIONS_RESULT.AUX_OPERATION_CANCELED;
      }

      // The cashier balance available must be checked out against the amount to cash.

      // Get cashier current balance
      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _cashier_current_balance = CashierBusinessLogic.UpdateSessionBalance(_db_trx.SqlTransaction, Cashier.SessionId, 0);
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return ENUM_TITO_OPERATIONS_RESULT.EXCEPTION;
      }

      //Modified by LEM 17-01-2014.
      // Is the cashier current balance enough to cash card?
      //  JCM 17-JAN-2012
      _amount_exceed_bank = _cash_redeem.TotalPaid > _cashier_current_balance;
      _payment_order_allowed = PaymentOrder.IsEnabled() && _cash_redeem.TotalPaid >= PaymentOrder.MinAmount();

      _error_str = String.Empty;

      if (_amount_exceed_bank)
      {
        if (!PaymentOrder.IsEnabled())
        {
          _error_str = Resource.String("STR_FRM_AMOUNT_INPUT_MSG_AMOUNT_EXCEED_BANK");
        }
      }

      if (!String.IsNullOrEmpty(_error_str))
      {
        frm_message.Show(_error_str,
                 Resource.String("STR_APP_GEN_MSG_ERROR"),
                 MessageBoxButtons.OK,
                 MessageBoxIcon.Error,
                 OperationParams.in_window_parent);

        return ENUM_TITO_OPERATIONS_RESULT.AUX_OPERATION_CANCELED;

      }

      if (_witholding_required)
      {
        if (dWitholdingData != null)
        {
          // Enter required data to generate the Witholding documents
          if (!dWitholdingData(OperationParams.in_window_parent, OperationParams.in_account, out OperationParams.withhold))
          {
            // Data entering process was cancelled => no further process
            return ENUM_TITO_OPERATIONS_RESULT.AUX_OPERATION_CANCELED;
          }

          // Witholding required data (RFC, CURP, Full Name, Biths date, gender) has 
          // been saved into the account and into _card_data.
        }
      } // if CheckWitholding

      if (OperationParams.check_redeem && _payment_order_allowed)
      {
        if (dOrderPaymentData != null)
        {
          _cashable_payment_order = 0;
          if (!OperationParams.is_apply_tax)
          {
            _cashable_payment_order = _cash_redeem.prize;
          }
          else
          {
            _cashable_payment_order = _cash_redeem.TotalPaid;
          }

          if (!dOrderPaymentData(OperationParams.in_window_parent, OperationParams.in_account, _cashable_payment_order, _cash_redeem.prize, OperationParams.withhold, out OperationParams.payment_order))
          {
            // Data entering process was cancelled => no further process
            return ENUM_TITO_OPERATIONS_RESULT.AUX_OPERATION_CANCELED;
          }
        }
      }

      return ENUM_TITO_OPERATIONS_RESULT.OK;
    }

    /// <summary>
    /// Function to set enabled state buttons in Reception Module (if GP OpenCashierAutomatically is different than zero or CASHIER_STATUS is OPEN)
    /// </summary>
    /// <param name="m_cashier_status"></param>
    /// <returns></returns>
    public static Boolean ReceptionButtonsEnabledCashierSessionOpenAutomatically(CASHIER_STATUS m_cashier_status)
    {
      Int32 _val_ret;

      _val_ret = Misc.GetReceptionCashierOpenAutomaticallyMode();

      return (_val_ret > 0
            || _val_ret == 0 && m_cashier_status == CASHIER_STATUS.OPEN);
    } // ReceptionButtonsEnabledCashierSessionOpenAutomatically

    /// <summary>
    /// Functions for managing an automatic Cashier Session Open when some operations in reception module requieres a Cashier Session Opened
    /// </summary>
    /// <param name="UcBank"></param>
    /// <param name="Parent"></param>
    /// <param name="CashierStatus"></param>
    /// <returns></returns>
    public static Cashier.CashierOpenAutomaticallyResult ManagementCashierSessionOpen(uc_bank UcBank, Form Parent, CASHIER_STATUS CashierStatus)
    {
      Boolean _open_cashier_session;
      Cashier.CashierOpenAutomaticallyResult _val_ret;
      DialogResult _dlg_rc;

      _open_cashier_session = false;
      _val_ret = Cashier.CashierOpenAutomaticallyResult.ERROR; // Error

      try
      {
        if (CashierStatus == CASHIER_STATUS.OPEN)
        {
          _val_ret = Cashier.CashierOpenAutomaticallyResult.OK;

          return _val_ret;
        }

        switch (Misc.GetReceptionCashierOpenAutomaticallyMode())
        {
          case 1:
            _dlg_rc = frm_message.Show(Resource.String("STR_FRM_RECEPTION_ASK_CASHIER_SESION_OPEN_AUTOMATICALLY").Replace("\\r\\n", "\r\n"),
                                        Resource.String("STR_APP_GEN_MSG_WARNING"),
                                        MessageBoxButtons.YesNo, Images.CashierImage.Question, Parent);
            if (_dlg_rc == DialogResult.OK)
            {
              _open_cashier_session = true;
            }
            else
            {
              _val_ret = Cashier.CashierOpenAutomaticallyResult.NOT_OPEN_CASHIER_SESSION_BY_USER; // Not continue by user action
            }

            break;

          case 2:
            _open_cashier_session = true;

            break;

          default:
            _open_cashier_session = false; // NOT APPLY

            break;
        }

        if (_open_cashier_session)
        {
          UcBank.ManagementOpenCashierSession(null, null, true);

          CashierStatus = CashierBusinessLogic.ReadCashierStatus(Cashier.SessionId);

          if (CashierStatus == CASHIER_STATUS.OPEN)
          {
            _val_ret = Cashier.CashierOpenAutomaticallyResult.OK;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return _val_ret;
    } // ManagementCashierSessionOpen

    // LTC 30-SEP-2016
    //------------------------------------------------------------------------------
    // PURPOSE: Check if last movement is a cash in
    //
    //  PARAMS:
    //      - INPUT:
    //          - AccountId
    //          - CashierSessionId
    //          - CodeOperation
    //
    //      - OUTPUT:
    //          - OperationId
    //
    // RETURNS:
    //      - Boolean: If last operation is cash in movement.
    //
    public static Boolean IsLastMovementCashIn(Int64 AccountId, out Int64 OperationId, Int64 CashierSessionId, OperationCode CodeOperation)
    {
      StringBuilder _sb;
      Int64 _operationCode;

      _sb = new StringBuilder();

      OperationId = 0;
      _operationCode = 0;

      // LTC 07-OCT-2016
      _sb.AppendLine(" DECLARE @_Operation BIGINT                                               ");
      _sb.AppendLine(" DECLARE @_OpeCode BIGINT                                                 ");
      _sb.AppendLine(" DECLARE @_Movement_Id BIGINT                                             ");
      _sb.AppendLine(" DECLARE @_Movement_Count INT                                             ");

      _sb.AppendLine(" SET @_Operation = 0                                                      ");
      _sb.AppendLine(" SET @_OpeCode = 0                                                        ");


      _sb.AppendLine(" SELECT TOP 1 @_Operation = AO_OPERATION_ID, @_OpeCode = AO_CODE          ");
      _sb.AppendLine(" FROM   ACCOUNT_OPERATIONS                                                ");
      _sb.AppendLine(" WHERE   AO_DATETIME  > @pTodayOpening                                    ");

      if (AccountId > 0)
      {
        _sb.AppendLine("      AND   AO_ACCOUNT_ID = @pAccountId                                 ");
      }

      _sb.AppendLine("   ORDER BY  AO_OPERATION_ID DESC                                         ");

      _sb.AppendLine("   IF @_OpeCode = @OpeCode                                                ");
      _sb.AppendLine("   BEGIN                                                                  ");
      _sb.AppendLine("      SELECT @_Movement_Id = MAX(am_movement_id)                          ");
      _sb.AppendLine("      FROM account_movements                                              ");
      _sb.AppendLine("      WHERE am_account_id = @pAccountId and am_operation_id = @_Operation ");

      _sb.AppendLine("      SELECT @_Movement_Count = COUNT(*)                                  ");
      _sb.AppendLine("      FROM account_movements                                              ");
      _sb.AppendLine("      WHERE  am_account_id = @pAccountId                                  ");
      _sb.AppendLine("      AND am_movement_id > @_Movement_Id AND am_terminal_id IS NOT NULL   ");
      _sb.AppendLine("   END                                                                    ");
      _sb.AppendLine("   ELSE                                                                   ");
      _sb.AppendLine("   BEGIN                                                                  ");
      _sb.AppendLine("      RETURN                                                              ");
      _sb.AppendLine("   END                                                                    ");

      _sb.AppendLine("   SELECT @_Operation, @_OpeCode, @_Movement_Count                        ");

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlTransaction Trx = _db_trx.SqlTransaction)
          {
            using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
            {
              _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
              _cmd.Parameters.Add("@pTodayOpening", SqlDbType.DateTime).Value = Misc.TodayOpening();
              _cmd.Parameters.Add("@OpeCode", SqlDbType.Int).Value = Convert.ToInt64(CodeOperation);

              using (SqlDataReader _reader = _cmd.ExecuteReader())
              {
                if (!_reader.Read())
                {
                  return false;
                }

                if (_reader.HasRows)
                {
                  if (Convert.ToInt64(_reader[2]) > 0)
                  {
                    return false;
                  }

                  OperationId = _reader.GetInt64(0);
                  _operationCode = Convert.ToInt64(_reader[1]);

                  if (_operationCode == Convert.ToInt64(CodeOperation))
                  {
                    return true;
                  }
                }
              }
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // GetOperation

    private static Boolean Common_CashAdvance(RechargeInputParameters InputParams, CurrencyExchangeResult ExchangeResult, SqlTransaction SqlTrx, out Int64 OperationId, out ArrayList VoucherList, out String ErrorMessage)
    {
      CashierSessionInfo _cashier_session_info;
      AccountMovementsTable _account_movements;
      CashierMovementsTable _cashier_movements;
      Currency _session_balance;
      MultiPromos.AccountBalance _fin_balance;
      Decimal _fin_points;
      TYPE_SPLITS _splits;

      ErrorMessage = String.Empty;

      _session_balance = 0;


      CardData _card_data;
      _card_data = new CardData();

      CardData.DB_CardGetAllData(InputParams.AccountId, _card_data, false);

      _cashier_session_info = Cashier.CashierSessionInfo();
      OperationId = 0;
      VoucherList = new ArrayList();


      if (!MultiPromos.Trx_UpdateAccountBalance(InputParams.AccountId, MultiPromos.AccountBalance.Zero, 0, out _fin_balance, out _fin_points, SqlTrx))
      {
        ErrorMessage = Resource.String("STR_GENERIC_ERROR_CASH_ADVANCE");

        return false;
      }

      _session_balance = UpdateSessionBalance(SqlTrx, Cashier.SessionId, 0);

      if (_session_balance < ExchangeResult.NetAmount)
      {
        ErrorMessage = Resource.String("STR_FRM_AMOUNT_INPUT_MSG_AMOUNT_EXCEED_BANK");

        return false;
      }

      if (!Operations.DB_InsertOperation(OperationCode.CASH_ADVANCE, InputParams.AccountId, Cashier.SessionId, 0,
                                         0, ExchangeResult.GrossAmount, 0, 0, 0, string.Empty,
                                         out OperationId, out ErrorMessage, SqlTrx))
      {
        return false;
      }

      _account_movements = new AccountMovementsTable(_cashier_session_info);
      _cashier_movements = new CashierMovementsTable(_cashier_session_info);

      if (!_account_movements.Add(OperationId, InputParams.AccountId, MovementType.CashAdvance, _fin_balance.TotalBalance, ExchangeResult.GrossAmount, ExchangeResult.GrossAmount, _fin_balance.TotalBalance))
      {
        ErrorMessage = Resource.String("STR_GENERIC_ERROR_CASH_ADVANCE");

        return false;
      }

      if (!_cashier_movements.AddCashAdvanceExchange(OperationId, InputParams.AccountId, InputParams.ExternalTrackData, ExchangeResult))
      {
        ErrorMessage = Resource.String("STR_GENERIC_ERROR_CASH_ADVANCE");

        return false;
      }

      if (!_cashier_movements.Add(OperationId, CASHIER_MOVEMENT.CASH_ADVANCE_CASH, ExchangeResult.NetAmount, InputParams.AccountId,
                            InputParams.ExternalTrackData, "", "", 0, 0, 0, 0, ExchangeResult.InType, null, null))
      {
        ErrorMessage = Resource.String("STR_GENERIC_ERROR_CASH_ADVANCE");

        return false;
      }

      if (!_account_movements.Save(SqlTrx))
      {
        ErrorMessage = Resource.String("STR_GENERIC_ERROR_CASH_ADVANCE");

        return false;
      }

      if (!_cashier_movements.Save(SqlTrx))
      {
        ErrorMessage = Resource.String("STR_GENERIC_ERROR_CASH_ADVANCE");

        return false;
      }

      if (ExchangeResult.BankTransactionData.DocumentNumber != null)
      {
        if (!Accounts.DB_InsertBankTransaction(OperationId, ExchangeResult, InputParams.ExternalTrackData, SqlTrx))
        {
          ErrorMessage = Resource.String("STR_GENERIC_ERROR_CASH_ADVANCE");

          return false;
        }
      }
      if (!Split.ReadSplitParameters(out _splits))
      {
        return false;
      }

      VoucherList = VoucherBuilder.CurrencyExchange(InputParams.VoucherAccountInfo, InputParams.AccountId, _splits, ExchangeResult,
                                                                       ExchangeResult.InType,
                                                                       CurrencyExchange.GetNationalCurrency(), OperationId, OperationCode.CASH_ADVANCE, PrintMode.Print, SqlTrx);
      return true;
    }

  }
}
