//------------------------------------------------------------------------------
// Copyright © 2009 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: TextPrintDocument.cs
// 
//   DESCRIPTION: Class to manage a document to print.
// 
//        AUTHOR: Ronald Rodríguez T.
// 
// CREATION DATE: 23-NOV-2009
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 23-NOV-2009 RRT    First release.
//------------------------------------------------------------------------------
using System;
using System.IO;
using System.Drawing;
using System.Drawing.Printing;
using System.Windows.Forms;
using WSI.Common;

namespace WSI.Cashier
{
  public class TextPrintDocument : PrintDocument 
  {
    #region Constants

    #endregion

    #region Enums

    #endregion

    #region Class Attributes

    private Font PrintFont;
    private TextReader PrintStream;
    private String DocName;

    #endregion

    #region Constructor

    /// <summary>
    /// Class contructor
    /// </summary>
    public TextPrintDocument()
    {

    }

    /// <summary>
    /// Class constructor overrided.
    /// </summary>
    /// <param name="PrintFileName"></param>
    public TextPrintDocument(String PrintFileName) : this()
    {
      DocName = PrintFileName;
    }

    #endregion


    #region Interface

    ///// <summary>
    ///// Interface
    ///// </summary>
    //public String FileToPrint
    //{
    //  get
    //  {
    //    return DocName;
    //  }
    //  set
    //  {
    //    if (File.Exists(value))
    //    {
    //      DocName = value;
    //      this.DocumentName = value;
    //    }
    //    else
    //      throw (new Exception("File not found."));
    //  }
    //}

    public Font Font
    {
      get
      {
        return PrintFont;
      }

      set
      {
        PrintFont = value;
      }
    }


    #endregion

    #region Private Methods

    #endregion

    #region Protected Methods

    /// <summary>
    /// Called on the beginning of the print.
    /// </summary>
    /// <param name="e"></param>
    protected override void OnBeginPrint(PrintEventArgs e)
    {
      base.OnBeginPrint(e);

      PrintStream = new StreamReader(DocName);
    }

    /// <summary>
    /// Event called when print is finished.
    /// </summary>
    /// <param name="e"></param>
    protected override void OnEndPrint(PrintEventArgs e)
    {
      base.OnEndPrint(e);

      PrintFont.Dispose();
      PrintStream.Close();
    }

    /// <summary>
    /// Event called during printing process.
    /// </summary>
    /// <param name="e"></param>
    protected override void OnPrintPage(PrintPageEventArgs e)
    {
      base.OnPrintPage(e);

      Graphics gdiPage = e.Graphics;
      float leftMargin = e.MarginBounds.Left;
      float topMargin = e.MarginBounds.Top;
      float lineHeight = PrintFont.GetHeight(gdiPage);
      float linesPerPage = e.MarginBounds.Height / lineHeight;
      int lineCount = 0;
      string lineText = null;
      Rectangle page_bounds = new Rectangle(e.MarginBounds.Left, e.MarginBounds.Top, e.MarginBounds.Width, e.MarginBounds.Height);
 
      // Print each line of the file.
      while (lineCount < linesPerPage && ((lineText = PrintStream.ReadLine()) != null))
      {
        //gdiPage.DrawString(lineText, PrintFont, Brushes.Black, leftMargin, (topMargin + (lineCount++ * lineHeight)));

        // Rectangle layout allows the text do to word wrapping
        gdiPage.DrawString(lineText, PrintFont, Brushes.Black, page_bounds);

      }

      e.HasMorePages = false;
    }

    #endregion

    #region Public Methods


    /// <summary>
    /// Set the name of the printer.
    /// </summary>
    /// <param name="PrinterName"></param>
    public void SetPrinterForDoc(String PrinterName)
    {
      this.PrinterSettings.PrinterName = PrinterName; 
    }

    /// <summary>
    /// Save a string into a document.
    /// </summary>
    public void SaveDocumentFile(String DocData)
    {
      FileStream file_stream;
      StreamWriter file_writer;
      String doc_filename;

      doc_filename = "";

      file_writer = null;

      try
      {
        // Create a temporary file with voucher to print
        doc_filename = Directory.GetCurrentDirectory() + "\\" + DocName;

        file_stream = new FileStream(doc_filename, FileMode.Create, FileAccess.Write);
        file_writer = new StreamWriter(file_stream);

        // Write voucher
        file_writer.BaseStream.Seek(0, SeekOrigin.Begin);
        file_writer.Write(DocData);

      }
      catch (Exception ex)
      {
        Log.Error("SaveDocumentFile. Error writing data into document. File: " + DocName);
        Log.Exception(ex);

        doc_filename = "";
      }
      finally
      {
        file_writer.Close();

        DocName = doc_filename;
      }
    } // SaveDocumentFile

    #endregion
  }
}    
