﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Security.Authentication.ExtendedProtection;
using System.Threading;
using System.Windows.Forms;
using WSI.Cashier.MVC.Controller.PlayerEditDetails;
using WSI.Cashier.MVC.View.PlayerEditDetails;
using WSI.Common;
using WSI.Common.Entrances;
using WSI.Common.Entrances.Enums;
using WSI.IDCamera;
using WSI.IDScanner.Model;
using Timer = System.Windows.Forms.Timer;

//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : frm_search_customers_reception.cs
// 
//   DESCRIPTION : Player tracking edition
// 
//        AUTHOR: ADI
// 
// CREATION DATE: 20-NOV-2015
// 
// REVISION HISTORY :
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 20-NOV-2015 ADI    First draft
// 11-DEC-2015 YNM    PBI 7503: Visits/Reception Trinidad y Tobago: search screen     
// 29-JAN-2016 YNM    Fixed Bug 8746 - Visits/Reception: incorrect NLS XEscanear ID  
// 03-FEB-2016 AVZ    Product Backlog Item 8427 - Task 8942:Visitas / Recepción: Modificaciones / Bugs equipo
// 04-FEB-2016 AVZ    Product Backlog Item 8427:Visitas / Recepción: Modificaciones / Bugs equipo
// 30-MAY-2016 JCA    Fixed Bug 13692:Recepción: error al crear un Nuevo Cliente con la caja cerrada
// 08-JUN-2016 JBP    Fixed Bug 14154:Errores de diseño de Recepción
// 20-JUN-2016 FAV    Fixed Bug 14685: Change position LastName and Name by general param
// 26-JUL-2016 PDM    PBI 15444:Visitas / Recepción: MEJORAS - Cajero - Expandir formulario de resultados de búsqueda
// 25-JUL-2016 FJC    PBI 15446:Visitas / Recepción: MEJORAS - Cajero - Apertura sesión de caja
// 20-SEP-2016 JRC    PBI 17677:Visitas / Recepción: MEJORAS II - Ref. 48 - Reportes recepción - separar columna Nombre (GUI)
//------------------------------------------------------------------------------

namespace WSI.Cashier
{
  public partial class FrmSearchCustomersReception : Controls.frm_base
  {
    private const int TrackNumberLength = 20;
    private CASHIER_STATUS m_cashier_status;
    private uc_bank m_uc_bank;
    #region Constructors


    public FrmSearchCustomersReception(IDScanner.IDScanner IDScannerInstance, uc_bank UcBank)
    {
      m_cashier_status = CashierBusinessLogic.ReadCashierStatus(Cashier.SessionId);
      IDScanner = IDScannerInstance;
      m_uc_bank = UcBank;
      InitializeComponent();
      EnableIdScanner();

      Init();
      InitControls();
      EventLastAction.AddEventLastAction(Controls);
      this.PerformOnExitClicked += () => {
        Misc.WriteLog("[FORM CLOSE] frm_search_customers_reception (searchcustomersreception - exit)", Log.Type.Message);
        this.Close();
      };
    } //FrmSearchCustomersReception

    #endregion
    private void uc_grid_entries_CellToolTipTextNeeded(object Sender, DataGridViewCellToolTipTextNeededEventArgs E)
    {
      //E.ToolTipText = Resource.String("STR_FRM_SEARCH_TOOL_TIP_TEXT");
    } //uc_grid_entries_CellToolTipTextNeeded

    private void uc_grid_entries_CellFormatting(object Sender, DataGridViewCellFormattingEventArgs E)
    {
      uc_grid_entries.Rows[E.RowIndex].DefaultCellStyle.SelectionBackColor = Color.FromArgb(45, 47, 50);
      uc_grid_entries.Rows[E.RowIndex].DefaultCellStyle.SelectionForeColor = Color.FromArgb(244, 246, 249);
    } //uc_grid_entries_CellFormatting
    #region Attributes

    private CustomerReception m_customer_reception;
    private Timer m_timer;
    private VisibleData m_visible_data;

    private bool m_found_record;
    private int m_user_type;
    #endregion
    #region Private Methods

    private bool m_scanner_enabled;

    private void EnableIdScanner()
    {
      btn_escanear_id.Enabled = IDScanner != null && IDScanner.AttatchToAllAvailable(HandleIDScannerEvent) > 0;
    } //EnableIdScanner

    //------------------------------------------------------------------------------
    // PURPOSE:  Set initials params in form
    //  PARAMS:
    //      - INPUT:
    //          -Nothing
    //
    //      - OUTPUT:
    //          · Nothing.
    //
    // RETURNS: Nothing.
    // 
    private void Init()
    {
      try
      {
        //Scanner
        this.ShowCloseButton = false;
        m_scanner_enabled = GeneralParam.GetBoolean("IDCardScanner", "Enabled");
        btn_escanear_id.Visible = m_scanner_enabled;
        //m_type_track_data = -1;
        //Form Position
        Location = new Point(1024 / 2 - Width / 2, 550 / 2 - Height / 2);
        m_customer_reception = new CustomerReception();
        txt_track_data.MaxLength = TrackNumberLength;

        LoadLastEntries();
      }

      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } //Init


    //------------------------------------------------------------------------------
    // PURPOSE: Set initial params to form controls
    // 
    //  PARAMS:
    //      - INPUT:
    //          -Nothing
    //
    //      - OUTPUT:
    //          · Nothing.
    //
    // RETURNS: Nothing.
    // 
    private void InitControls()
    {
      try
      {
        //Texts
        //txt_track_data.Location = new Point((uc_pnl_top.Width - txt_track_data.Width) / 2, txt_track_data.Location.Y);
        // Labels
        lbl_name.Text = AccountFields.GetName(); // Resource.String("STR_FRM_PLAYER_SEARCH_002");
        lbl_name2.Text = AccountFields.GetMiddleName(); // Resource.String("STR_FRM_PLAYER_EDIT_NAME4");
        lbl_lastname1.Text = AccountFields.GetSurname1(); // Resource.String("STR_FRM_PLAYER_EDIT_LAST_NAME1");
        lbl_lastname2.Text = AccountFields.GetSurname2(); // Resource.String("STR_FRM_PLAYER_EDIT_LAST_NAME2");
        lbl_fullname.Text = Resource.String("STR_FRM_PLAYER_EDIT_FULL_NAME");
        lbl_fullname2.Text = string.Empty;
        lbl_id.Text = Resource.String("STR_FRM_PLAYER_SEARCH_003");
        uc_lbl_birth_date.Text = Resource.String("STR_FRM_SEARCH_CUSTOMER_BIRTH_DATE");
        uc_lbl_sex.Text = Resource.String("STR_UC_PLAYER_TRACK_GENDER");
        LoadComboNls("STR_FRM_PLAYER_TRACKING_CB_GENDER_MALE", uc_cb_gender);
        LoadComboNls("STR_FRM_PLAYER_TRACKING_CB_GENDER_FEMALE", uc_cb_gender);
        FormTitle = Resource.String("STR_FRM_SEARCH_RECEPTION_TITLE");
        lbl_track_data.Text = Resource.String("STR_FRM_SEARCH_CUSTOMER_TRACKDATA");
        //lbl_track_data.Location = new Point((uc_pnl_top.Width - lbl_track_data.Width) / 2, lbl_track_data.Location.Y);
        uc_voucher_search.Text = Resource.String("STR_FRM_SEARCH_BY_VOUCHER");
        // 03-FEB-2016 AVZ
        uc_voucher_search.Enabled = Entrance.GetReceptionMode() != ENUM_RECEPTION_MODE.Lite;
        // this.lbl_fullname.Location = new Point((this.pnl_message.Width - this.lbl_fullname.Width) / 2, this.lbl_fullname.Location.Y);
        lbl_msg_blink.Width = lbl_msg_blink.Parent.Width;
        lbl_msg_blink.Location = new Point(0, lbl_msg_blink.Location.Y);
        //this.lbl_fullname2.Width = this.lbl_fullname2.Parent.Width;
        lbl_fullname2.Location = new Point(lbl_fullname.Location.X, lbl_fullname2.Location.Y);
        Width = 1024;

        //Buttons
        btn_cancel.Text = Resource.String("STR_ACTION_CLOSE");
        btn_add_customer.Text = Resource.String("STR_FRM_SEARCH_CUSTOMER_ADD");
        btn_add_customer.Enabled = CashierBusinessLogic.ReceptionButtonsEnabledCashierSessionOpenAutomatically(m_cashier_status);

        btn_escanear_id.Text = Resource.String("STR_FRM_SEARCH_SCAN_ID");
        btn_clear.Text = Resource.String("STR_CAGE_BTN_CLEAR");
        btn_visits_report.Text = Resource.String("STR_FRM_RECEPTION_VISITS_REPORT_15");
        uc_dt_birth_date.Init();
        uc_grid_entries.ShowCellToolTips = false;

        uc_pnl_grid.HeaderText = Resource.String("STR_FRM_SEARCH_LAST_ENTRIES");

        SetVisibleFields();
        SetNameAndLastNamePostion();
        SetFormatGridEntries();
        uc_voucher_search_CheckedChanged(null, null);


      }

      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } //LoadAccountByVoucher

    /// <summary>
    ///   Load Datasource in grid entries
    /// </summary>
    private void LoadLastEntries()
    {
      DataTable _dt_entries;
      try
      {
        if (!m_customer_reception.GetLastEntries(out _dt_entries))
        {
          return;
        }

        uc_grid_entries.AutoGenerateColumns = false;
        uc_grid_entries.DataSource = _dt_entries;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } //LoadLastEntries

    /// <summary>
    ///   Load account in PlayerEdit
    /// </summary>
    private void LoadAccountByVoucher()
    {
      try
      {
        DataSet _out_ds;
        m_found_record = false;

        Barcode _code;
        if (!Barcode.TryParse(txt_track_data.Text, out _code) || _code.Type != BarcodeType.Reception)
        {
          SetTimerVisibleMessage(Resource.String("STR_FRM_SEARCH_CUSTOMER_INVALID_VOUCHER"));
          return;
        }

        if (!m_customer_reception.GetAccountByVoucher(_code.UniqueId, out _out_ds))
        {
          SetTimerVisibleMessage(Resource.String("STR_FRM_SEARCH_CUSTOMER_RESULTS"));
          btn_add_customer.Enabled = CashierBusinessLogic.ReceptionButtonsEnabledCashierSessionOpenAutomatically(m_cashier_status);
          return;
        }

        if (!_out_ds.Tables.Contains("Accounts"))
        {
          return;
        }

        if (_out_ds.Tables["Accounts"].Rows.Count <= 0)
        {
          return;
        }

        if (!string.IsNullOrEmpty(m_customer_reception.CardCodBar))
        {
          m_customer_reception.AccountId = (long)_out_ds.Tables["Accounts"].Rows[0][0]; //account_ID
        }
        if (m_customer_reception == null)
        {
          return;
        }

        OpenPlayerEdit(_out_ds, false, LastScannedInfo);
        m_found_record = true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } //LoadAccountByVoucher

    /// <summary>
    ///   Load account in PlayerEdit
    /// </summary>
    private void LoadEntry()
    {


      try
      {
        DataSet _out_ds;
        m_found_record = false;
        if (!m_customer_reception.GetCustomerAccounts(out _out_ds))
        {
          return;
        }

        if (!_out_ds.Tables.Contains("Accounts"))
        {
          return;
        }

        if (_out_ds.Tables["Accounts"].Rows.Count <= 0)
        {
          return;
        }

        if (!string.IsNullOrEmpty(m_customer_reception.CardCodBar))
        {
          m_customer_reception.AccountId = (long)_out_ds.Tables["Accounts"].Rows[0][0]; //account_ID

        }

        if (m_customer_reception == null)
        {
          return;
        }

        OpenPlayerEdit(_out_ds, false, LastScannedInfo);
        m_found_record = true;
      }
      catch (Exception)
      {
        frm_message.Show(WSI.Common.Resource.String("STR_RECEPTION_ERROR_GENERAL_TITLE"),
  WSI.Common.Resource.String("STR_RECEPTION_ERROR_GENERAL_TEXT"), MessageBoxButtons.OK, MessageBoxIcon.Error, this);

      }
    } //LoadEntry

    /// <summary>
    ///   Changes Visibility of required fields.
    /// </summary>
    private void SetVisibleFields()
    {
      m_visible_data = new VisibleData();
      m_visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.NAME3, new Control[] { tlp_name });
      m_visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.NAME4, new Control[] { tlp_name2 });
      m_visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.NAME1, new Control[] { tlp_surname });
      m_visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.NAME2, new Control[] { tlp_surname2 });
      m_visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.GENDER, new Control[] { tlp_sex });
      m_visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.DOCUMENT, new Control[] { tlp_document });
      m_visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.BIRTHDATE, new Control[] { tlp_dob });


      m_visible_data.HideControls();

      // Recommend flow layout panel instead of moving controls

      //if (txt_lastname2.Enabled)
      //{
      //  return;
      //}

      //uc_lbl_birth_date.Location = lbl_id.Location;
      //uc_dt_birth_date.Location = new Point(uc_dt_birth_date.Location.X - 185, txt_id.Location.Y);
      //lbl_id.Location = lbl_lastname2.Location;
      //txt_id.Width = txt_lastname2.Width;
      //txt_id.Location = txt_lastname2.Location;
    } //SetVisibleFields

    /// <summary>
    /// Change position of name and last name by general param
    /// </summary>
    private void SetNameAndLastNamePostion()
    {
      if (GeneralParam.GetBoolean("Account", "InverseNameOrder", false))
      {
        flowLayoutPanel1.Controls.SetChildIndex(tlp_surname, 0);
        flowLayoutPanel1.Controls.SetChildIndex(tlp_surname2, 1);
        flowLayoutPanel1.Controls.SetChildIndex(tlp_name, 2);
        flowLayoutPanel1.Controls.SetChildIndex(tlp_name2, 3);

        int _tabIndex = 0;

        _tabIndex = tlp_name.TabIndex;
        tlp_name.TabIndex = tlp_surname.TabIndex;
        tlp_surname.TabIndex = _tabIndex;

        _tabIndex = tlp_name2.TabIndex;
        tlp_name2.TabIndex = tlp_surname2.TabIndex;
        tlp_surname2.TabIndex = _tabIndex;

      }


    }

    /// <summary>
    ///   Checks if birth_date format is valid
    /// </summary>
    /// <returns>TRUE if date is valid </returns>
    private bool DateValidate()
    {
      int _legal_age;

      try
      {
        if (uc_dt_birth_date.IsEmpty)
        {
          return true;
        }

        if (!uc_dt_birth_date.ValidateFormat())
        {
          SetTimerVisibleMessage(Resource.String("STR_FRM_PLAYER_SEARCH_005"));
          uc_dt_birth_date.Focus();
          return false;
        }


        if (!CardData.IsValidBirthdate(uc_dt_birth_date.DateValue, out _legal_age))
        {
          SetTimerVisibleMessage(Resource.String("STR_FRM_ACCOUNT_USER_EDIT_NOT_ADULT", _legal_age));
          uc_dt_birth_date.Focus();
          return false;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      return true;
    } //DateValidate

    /// <summary>
    ///   Set Init params in Combo
    /// </summary>
    /// <param name="Key"></param>
    /// <param name="Combo"></param>
    private void LoadComboNls(string Key, Controls.uc_round_combobox Combo)
    {
      string _str_aux;
      _str_aux = Resource.String(Key);
      Combo.Items.Add(_str_aux);
    } //LoadComboNls


    //------------------------------------------------------------------------------
    // PURPOSE: Set initial params to Timer
    // 
    //  PARAMS:
    //      - INPUT:
    //          -Nothing
    //
    //      - OUTPUT:
    //          · Nothing.
    //
    // RETURNS: Nothing.
    // 


    private void SetTimerVisibleMessage(string Message)
    {
      try
      {
        if (m_timer == null)
        {
          m_timer = new Timer { Interval = 6000, Enabled = true };
          m_timer.Tick += m_timer_Tick;
        }
        else
        {
          m_timer.Stop();
        }

        // if ( m_customer_reception != null)
        //{
        lbl_msg_blink.Visible = true;
        lbl_msg_blink.Text = Message;
        //}

        m_timer.Start();
      } // end try
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } //end SetTimer


    //------------------------------------------------------------------------------
    // PURPOSE: Checks and sets parameters for values where
    // 
    //  PARAMS:
    //      - INPUT: 
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //    
    // 
    //   NOTES:
    private void SetParametersWhere()
    {
      try
      {
        if (!string.IsNullOrEmpty(txt_track_data.Text.Trim()))
        {
          m_customer_reception.CardCodBar = txt_track_data.Text.Trim();
          return;
        }

        m_customer_reception.CustomerName = txt_name.Text.Trim();
        m_customer_reception.CustomerName2 = txt_name2.Text.Trim();
        m_customer_reception.CustomerLastname1 = txt_lastname1.Text.Trim();
        m_customer_reception.CustomerLastname2 = txt_lastname2.Text.Trim();
        m_customer_reception.Document = txt_id.Text.Trim();
        m_customer_reception.BirthDate = uc_dt_birth_date.DateValue;
        m_customer_reception.Gender = uc_cb_gender.SelectedIndex + 1;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } //SetParametersWhere

    //------------------------------------------------------------------------------
    // PURPOSE: Checks and sets parameters for values where
    // 
    //  PARAMS:
    //      - INPUT: 
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //    
    // 
    //   NOTES:
    private void GetCustomersToDataTable()
    {
      try
      {
        DataSet _out_ds;
        InitCustomerReception();


        if (m_customer_reception.IsCardMobileBank())
        {
          SetTimerVisibleMessage(Resource.String("STR_FRM_SEARCH_CUSTOMER_MOBILE_BANK"));
          return;
        }
        SetParametersWhere();
        if (!m_customer_reception.GetCustomerAccounts(out _out_ds))
        {
          NotFound();
          return;
        }

        if (_out_ds.Tables.Contains("Accounts") && _out_ds.Tables["Accounts"].Rows.Count <= 0)
        {
          NotFound();
          return;
        }


        if (!string.IsNullOrEmpty(m_customer_reception.CardCodBar))
        {
          m_customer_reception.AccountId = (long)_out_ds.Tables["Accounts"].Rows[0][0]; //account_ID
          m_user_type = (int)_out_ds.Tables["Accounts"].Rows[0][6];
          if (m_user_type == 0)
          {
            SetTimerVisibleMessage(Resource.String("STR_FRM_SEARCH_CUSTOMER_ANONYMOUS"));
            return;
          }
          if (_out_ds.Tables["Accounts"].Rows[0].IsNull(2))
          {
            m_customer_reception = null;
            NotFound();
            return;
          }
        }

        if (m_customer_reception == null)
        {
          NotFound();
          return;
        }
        OpenPlayerEdit(_out_ds, false, LastScannedInfo);
        Found();
      } //GetCustomersToDataTable

      catch (Exception _ex)
      {
        Log.Exception(_ex);
        frm_message.Show(WSI.Common.Resource.String("STR_RECEPTION_ERROR_GENERAL_TITLE"),
  WSI.Common.Resource.String("STR_RECEPTION_ERROR_GENERAL_TEXT"), MessageBoxButtons.OK, MessageBoxIcon.Error, this);
      }
    } //GetCustomersToDataTable

    /// <summary>
    /// Execute actions when the client is not registered
    /// </summary>
    public void NotFound()
    {
      m_found_record = false;
      DialogResult _dlg_rc;

      if (m_cashier_status != CASHIER_STATUS.OPEN)
      {
        frm_message.Show(Resource.String("STR_FRM_SEARCH_CUSTOMER_RESULTS"), Resource.String("STR_FRM_SEARCH_CUSTOMER_RESULTS"), MessageBoxButtons.OK, this);
      }
      else
      {
        _dlg_rc =
          frm_message.Show(
            string.Format(Resource.String("STR_FRM_SEARCH_CUSTOMER_RESULTS") + "\n" + Resource.String("STR_FRM_SEARCH_CUSTOMER_RESULTS_ASK"), "Warning", Environment.NewLine),
            Resource.String("STR_APP_GEN_MSG_WARNING"), MessageBoxButtons.YesNo, Images.CashierImage.Warning,
            this);

        if (_dlg_rc == DialogResult.OK)
        {
          btn_add_customer.PerformClick();
        }
        else
        {
          SetTimerVisibleMessage(Resource.String("STR_FRM_SEARCH_CUSTOMER_RESULTS"));
          btn_add_customer.Enabled = CashierBusinessLogic.ReceptionButtonsEnabledCashierSessionOpenAutomatically(m_cashier_status);
        }
      }
    } // NotFound

    /// <summary>
    /// Execute actions when the client is registered
    /// </summary>
    public void Found()
    {
      m_found_record = true;
      btn_add_customer.Enabled = CashierBusinessLogic.ReceptionButtonsEnabledCashierSessionOpenAutomatically(m_cashier_status);
    } // Found

    /// <summary>
    ///   Initialize class CustomerReception properties
    /// </summary>
    private void InitCustomerReception()
    {
      try
      {
        if (m_customer_reception == null)
        {
          m_customer_reception = new CustomerReception();
        }

        m_customer_reception.CardCodBar = uc_voucher_search.Checked ?
          txt_track_data.Text.Trim() :
          uc_card_reader1.Text;

        m_customer_reception.Document = txt_id.Text.Trim();
        m_customer_reception.CustomerName = txt_name.Text.Trim();
        m_customer_reception.CustomerName2 = txt_name2.Text.Trim();
        m_customer_reception.CustomerLastname1 = txt_lastname1.Text.Trim();
        m_customer_reception.CustomerLastname2 = txt_lastname2.Text.Trim();
        // m_customer_reception.BirthDate = String.IsNullOrEmpty(uc_dt_birth_date.DateValue.ToString()) ? String.Empty : uc_dt_birth_date.DateValue.ToString();
        m_customer_reception.Gender = string.IsNullOrEmpty(uc_cb_gender.Text) ? -1 : uc_cb_gender.SelectedIndex + 1;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } //InitCustomerReception

    //------------------------------------------------------------------------------
    // PURPOSE: Open frm_player_edit to edit data account or create new account
    // 
    //  PARAMS:
    //      - INPUT: 
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //    
    // 
    //   NOTES:
    private void OpenPlayerEdit(DataSet OutDs, bool NewCustomer = false, IDScannerInfo ScannedInfo = null, string TrackNumeber = null)
    {
      try
      {
        if (IDScanner != null)
        {
          IDScanner.DetatchFromAllAvailable(null);
        }
        btn_escanear_id.Enabled = false;
        CardData _card_data;
        DialogResult _dgr;
        _card_data = new CardData();


        if (m_customer_reception == null)
        {
          m_customer_reception = new CustomerReception();
        }

        if (m_customer_reception.AccountId > 0)
        {
          _card_data.AccountId = m_customer_reception.AccountId;
          _card_data.TrackData = m_customer_reception.CardCodBar;
          CardData.DB_CardGetPersonalData(_card_data.AccountId, _card_data);
        }

        if (!m_found_record)
        {
          _card_data.TrackData = m_customer_reception.CardCodBar;
        }

        if (m_camera_module == null)
        {
          m_camera_module = new CameraModule(this, typeof(frmCapturePhoto), typeof(SelectDevice)) { EventMode = false };
        }

        using (frm_yesno _shader = new frm_yesno())
        {
          using (PlayerEditDetailsReceptionView _reception_view = new PlayerEditDetailsReceptionView())
          {
            using (var _controller =
              new PlayerEditDetailsReceptionController(OutDs, m_customer_reception.StringSearchSql, NewCustomer,
                ScannedInfo, _reception_view, IDScanner, this.m_uc_bank, TrackNumeber, _reception_view.ViewTopResults) { SearchedDocumentId = m_customer_reception.Document })
            {
              // ATB 16-FEB-2017
              Misc.WriteLog("[FORM LOAD] frm_search_customers_reception", Log.Type.Message);

              _shader.Opacity = 0.6;
              _shader.Show();
              if (WindowManager.StartUpMode == WindowManager.StartUpModes.Explorer)
              {
                _reception_view.Location = new Point(_shader.Location.X, _shader.Location.Y);
              }
              var _player_edit_details_reception_controller =
                _reception_view.Controller as PlayerEditDetailsReceptionController;
              if (_player_edit_details_reception_controller != null)
              {
                if (!_card_data.IsNew && _card_data.PlayerTracking != null)
                  _player_edit_details_reception_controller.MustShowComments =
                    _card_data.PlayerTracking.ShowCommentsOnCashier;
                _player_edit_details_reception_controller.CameraModule = m_camera_module;
              }
              _dgr = DialogResult.Cancel;
              if (!_controller.AbortOpen)
                _dgr = _reception_view.ShowDialog(_shader);
            }
          }
        }

        if (_dgr == DialogResult.OK)
        {
          if (m_customer_reception.AccountId == 0)
          {
            m_customer_reception.AccountId = _card_data.AccountId;
          }
          m_customer_reception.Document = _card_data.PlayerTracking.HolderId;
          m_customer_reception.CustomerName = _card_data.PlayerTracking.HolderName3;
          m_customer_reception.CustomerLastname1 = _card_data.PlayerTracking.HolderName1;
          m_customer_reception.CustomerLastname2 = _card_data.PlayerTracking.HolderName2;

          SetTexts();
          LoadLastEntries();
          return;
        }

        m_customer_reception = null;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {
        EnableIdScanner();
        btn_clear.PerformClick();
        GC.Collect();
        ThreadPool.QueueUserWorkItem(delegate
        {
          GC.WaitForPendingFinalizers();
          GC.Collect();
        });
      }
    } //OpenPlayerEdit

    private CameraModule m_camera_module;

    //------------------------------------------------------------------------------
    // PURPOSE: Sets search results in text box
    // 
    //  PARAMS:
    //      - INPUT: 
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //    
    // 
    //   NOTES:
    private void SetTexts()
    {
      try
      {
        if (m_customer_reception != null)
        {
          txt_track_data.Text = m_customer_reception.CardCodBar;
          txt_name.Text = m_customer_reception.CustomerName;
          txt_name2.Text = m_customer_reception.CustomerName2;
          txt_lastname1.Text = m_customer_reception.CustomerLastname1;
          txt_lastname2.Text = m_customer_reception.CustomerLastname2;
          txt_id.Text = m_customer_reception.Document;
          uc_dt_birth_date.DateValue = m_customer_reception.BirthDate;
          uc_cb_gender.SelectedIndex = m_customer_reception.Gender - 1;
          return;
        }

        txt_track_data.Text = string.Empty;
        txt_name.Text = string.Empty;
        txt_name2.Text = string.Empty;
        txt_lastname1.Text = string.Empty;
        txt_lastname2.Text = string.Empty;
        txt_id.Text = string.Empty;
        uc_cb_gender.Text = string.Empty;
        uc_cb_gender.SelectedIndex = -1;
        uc_cb_gender.Refresh();
        uc_dt_birth_date.ResetDateValue();
        uc_voucher_search.Checked = false;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } //SetTexts

    //------------------------------------------------------------------------------
    // PURPOSE: Set BARCODE in txt_track_data
    // 
    //  PARAMS:
    //      - INPUT: 
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //    
    // 
    //   NOTES:
    private void AccountReceivedEvent(KbdMsgEvent E, object Data)
    {
      Barcode _barcode;
      //KeyPressEventArgs _event_args;

      try
      {
        if (E != KbdMsgEvent.EventBarcode)
        {
          return;
        }

        _barcode = Data as Barcode;
        if (_barcode == null)
        {
          return;
        }

        txt_track_data.Text = _barcode.ExternalCode;
        // assignation never used, remove?
        //_event_args = new KeyPressEventArgs('\r');
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } //AccountReceivedEvent

    private int GetIntField(Dictionary<string, object> Fields, string FieldName)
    {
      if (!Fields.ContainsKey(FieldName))
        return default(int);
      return (int)Fields[FieldName];
    }

    private DateTime? GetDateField(Dictionary<string, object> Fields, string FieldName)
    {
      if (!Fields.ContainsKey(FieldName))
        return default(DateTime?);
      return Fields[FieldName] as DateTime?;
    }

    private string GetStringField(Dictionary<string, object> Fields, string FieldName)
    {
      if (!Fields.ContainsKey(FieldName))
        return "";
      return Fields[FieldName] as string;
    }

    private GENDER GetGenderField(Dictionary<string, object> Fields, string FieldName)
    {
      if (!Fields.ContainsKey(FieldName))
        return GENDER.UNKNOWN;
      if (Fields[FieldName] is int)
      {
        return (GENDER)Fields[FieldName];
      }
      string _str = Fields[FieldName] as string;
      if (string.IsNullOrEmpty(_str))
        return GENDER.UNKNOWN;
      switch (_str.ToUpper())
      {
        case "M":
          return GENDER.MALE;
        case "F":
          return GENDER.FEMALE;
        default:
          return GENDER.UNKNOWN;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get info card from scanner
    // 
    //  PARAMS:
    //      - INPUT: 
    //         ScannedPersonalInfoEvent 
    //      - OUTPUT:
    //
    // RETURNS:
    //    
    // 
    //   NOTES:

    public void HandleIDScannerEvent(IDScannerInfoEvent Ev)
    {
      try
      {
        if (InvokeRequired)
        {
          Invoke(new IDScannerInfoHandler(HandleIDScannerEvent), Ev);
          return;
        }



        txt_name.Text = GetStringField(Ev.info.Fields, Ev.info.IDScannerFixedFieldMapping[ENUM_IDScannerFixedField.NAME]);
        txt_name2.Text = GetStringField(Ev.info.Fields,
          Ev.info.IDScannerFixedFieldMapping[ENUM_IDScannerFixedField.NAME2]);
        txt_lastname1.Text = GetStringField(Ev.info.Fields,
          Ev.info.IDScannerFixedFieldMapping[ENUM_IDScannerFixedField.SURNAME]);
        txt_lastname2.Text = GetStringField(Ev.info.Fields,
          Ev.info.IDScannerFixedFieldMapping[ENUM_IDScannerFixedField.SURNAME2]);
        txt_id.Text = GetStringField(Ev.info.Fields,
          Ev.info.IDScannerFixedFieldMapping[ENUM_IDScannerFixedField.ID_NUMBER]);
        uc_dt_birth_date.DateValue =
          GetDateField(Ev.info.Fields, Ev.info.IDScannerFixedFieldMapping[ENUM_IDScannerFixedField.DATE_OF_BIRTH]) ??
          default(DateTime);

        switch (GetGenderField(Ev.info.Fields, Ev.info.IDScannerFixedFieldMapping[ENUM_IDScannerFixedField.SEX]))
        {
          case GENDER.MALE:
            {
              uc_cb_gender.SelectedIndex = 0;
            }
            break;
          case GENDER.FEMALE:
            {
              uc_cb_gender.SelectedIndex = 1;
            }
            break;
          default:
            {
              uc_cb_gender.SelectedIndex = -1;
            }
            break;
        }

        LastScannedInfo = Ev.info;
        btn_ok_Click(null, null);
      }

      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } //HandleEvent

    public IDScannerInfo LastScannedInfo { get; set; } //LastScannedInfo

    private bool TrackNumberIsValid(string TrackNumber)
    {
      if (TrackNumber.Length != TrackNumberLength)
      {
        return false;

      }

      string _internal_track;
      int _card_type;

      if (!CardNumber.TrackDataToInternal(TrackNumber, out _internal_track, out _card_type))
      {
        return false;
      }

      return _card_type == 0 || _card_type == 1;
    } // TrackNumberIsValid


    private void SetFormatGridEntries()
    {
      string _date_format;
      try
      {
        uc_grid_entries.AutoGenerateColumns = false;
        uc_grid_entries.MultiSelect = false;
        uc_grid_entries.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

        _date_format = Resource.String("STR_FRM_SEARCH_RECEPTION_DATE_FORMAT");

        // Set row height for bigger font sizes

        uc_grid_entries.ColumnHeadersHeight = 32;
        uc_grid_entries.RowTemplate.Height = 50;
        uc_grid_entries.RowTemplate.MinimumHeight = 50;
        uc_grid_entries.RowTemplate.DividerHeight = 0;

        // Columns

        DataGridViewColumn _col1 = new DataGridViewTextBoxColumn
          {
            DataPropertyName = "AC_ACCOUNT_ID",
            Name = "AccountNumber",
            HeaderText = Resource.String("STR_FRM_PLAYER_SEARCH_RESULTS_002"),
            Width = 80
          };
        _col1.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        _col1.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

        DataGridViewColumn _col2 = new DataGridViewTextBoxColumn
          {
            DataPropertyName = "AC_HOLDER_NAME3",
            Name = "Name",
            HeaderText = Resource.String("STR_FRM_PLAYER_SEARCH_RESULTS_003"),
            AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
          };

        _col2.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
        _col2.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

        DataGridViewColumn _col6 = new DataGridViewTextBoxColumn
        {
          DataPropertyName = "AC_HOLDER_NAME12",
          Name = "Lastname",
          HeaderText = Resource.String("STR_FRM_PLAYER_SEARCH_RESULTS_007"),
          AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        };

        _col6.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
        _col6.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;



        DataGridViewColumn _col3 = new DataGridViewTextBoxColumn
          {
            DataPropertyName = "CUE_DOCUMENT_NUMBER",
            Name = "Document",
            HeaderText = Resource.String("STR_FRM_PLAYER_SEARCH_RESULTS_004"),
            Width = 156
          };
        _col3.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
        _col3.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

        DataGridViewColumn _col4 = new DataGridViewTextBoxColumn
          {
            DataPropertyName = "CUE_ENTRANCE_DATETIME",
            Name = "BirthDate",
            HeaderText = Resource.String("STR_FRM_PLAYER_SEARCH_ENTRIES"),
            Width = 145
          };

        _col4.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
        _col4.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
        _col4.DefaultCellStyle.Format = _date_format;

        DataGridViewColumn _col5 = new DataGridViewTextBoxColumn
          {
            DataPropertyName = "CT_NAME",
            Name = "TerminalID",
            HeaderText = Resource.String("STR_FRM_PLAYER_SERCH_TERMINAL_ID"),
            Width = 146
          };

        _col5.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
        _col5.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

        uc_grid_entries.Columns.Add(_col1);
        uc_grid_entries.Columns.Add(_col2);
        uc_grid_entries.Columns.Add(_col6);
        uc_grid_entries.Columns.Add(_col3);
        uc_grid_entries.Columns.Add(_col4);
        uc_grid_entries.Columns.Add(_col5);

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } //SetFormatGridEntries

    #endregion
    #region Events

    private void frm_search_customers_reception_FormClosing(object Sender, FormClosingEventArgs E)
    {
      E.Cancel = (E.CloseReason == CloseReason.None);
    } //frm_search_customers_reception_FormClosing

    private void m_timer_Tick(object Sender, EventArgs E)
    {
      try
      {
        m_timer.Enabled = false;
        lbl_msg_blink.Visible = false;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } //m_timer_Tick


    private void txt_track_data_VisibleChanged(object Sender, EventArgs E)
    {
      try
      {
        if (Visible)
        {
          //KeyboardMessageFilter.RegisterHandler(this, BarcodeType.FilterAnyBarcode, AccountReceivedEvent);
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } //txt_track_data_VisibleChanged

    private void txt_track_data_TextChanged(object Sender, EventArgs E)
    {


      //Search only when the track number/ barcode length is valid
      if (!uc_voucher_search.Checked)
      {
        if (txt_track_data.TextLength != TrackNumberLength)
        {
          return;
        }
      }

      try
      {
        if (m_customer_reception == null)
        {
          m_customer_reception = new CustomerReception();
        }

        // 04-FEB-2016 AVZ
        btn_add_customer.Enabled = CashierBusinessLogic.ReceptionButtonsEnabledCashierSessionOpenAutomatically(m_cashier_status);
        if (uc_voucher_search.Checked)
        {
          LoadAccountByVoucher();
          return;
        }

        LoadAccountByTrackData();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {
        txt_track_data.Text = string.Empty;
      }
    } //txt_track_data_TextChanged

    /// <summary>
    ///   Load the account by track data in PlayerEdit
    /// </summary>
    private void LoadAccountByTrackData()
    {
      if (TrackNumberIsValid(txt_track_data.Text.Trim()))
      {
        GetCustomersToDataTable();
        return;
      }

      SetTimerVisibleMessage(Resource.String("STR_UC_CARD_READER_INVALID_CARD"));
      btn_add_customer.Enabled = CashierBusinessLogic.ReceptionButtonsEnabledCashierSessionOpenAutomatically(m_cashier_status);
    } //LoadAccountByTrackData

    private void btn_ok_Click(object Sender, EventArgs E)
    {
      try
      {
        m_found_record = false;
        if (!DateValidate())
        {
          return;
        }

        if (string.IsNullOrEmpty(uc_card_reader1.Text.Trim()) && string.IsNullOrEmpty(txt_id.Text.Trim()) && string.IsNullOrEmpty(txt_name.Text.Trim()) && string.IsNullOrEmpty(txt_name2.Text.Trim()) &&
            string.IsNullOrEmpty(txt_lastname1.Text.Trim()) && string.IsNullOrEmpty(txt_lastname2.Text.Trim()) && string.IsNullOrEmpty(txt_track_data.Text.Trim()) &&
            string.IsNullOrEmpty(uc_cb_gender.Text.Trim()) && uc_dt_birth_date.DateValue.Year <= 1900)
        {
          SetTimerVisibleMessage(Resource.String("STR_FRM_SEARCH_CUSTOMER_EMPTY"));
          btn_add_customer.Enabled = CashierBusinessLogic.ReceptionButtonsEnabledCashierSessionOpenAutomatically(m_cashier_status);
          return;
        }
        GetCustomersToDataTable();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } //btn_ok_Click

    private void btn_cancel_Click(object Sender, EventArgs E)
    {
      try
      {
        WSIKeyboard.Hide();
        Close();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } //btn_cancel_Click

    private void btn_keyboard_Click(object Sender, EventArgs E)
    {
      try
      {
        WSIKeyboard.ForceToggle();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } //btn_keyboard_Click

    private void btn_escanear_id_Click(object Sender, EventArgs E)
    {
      if (IDScanner == null)
        return;
      IDScanner.ForceScan();
    } //btn_escanear_id_Click

    private void btn_add_customer_Click(object Sender, EventArgs E)
    {
      Cashier.CashierOpenAutomaticallyResult _val_ret;

      _val_ret = Cashier.CashierOpenAutomaticallyResult.ERROR;

      try
      {
        _val_ret = CashierBusinessLogic.ManagementCashierSessionOpen(this.m_uc_bank, this, this.m_cashier_status);

        if (_val_ret == Cashier.CashierOpenAutomaticallyResult.OK)
        {
          m_cashier_status = CashierBusinessLogic.ReadCashierStatus(Cashier.SessionId);
        }
        else
        {
          if (_val_ret == Cashier.CashierOpenAutomaticallyResult.ERROR)
          {
            frm_message.Show(Resource.String("STR_FRM_RECEPTION_CASHIER_SESION_OPEN_AUTOMATICALLY_ERROR"),
              Resource.String("STR_APP_GEN_MSG_ERROR"),
              MessageBoxButtons.OK,
              MessageBoxIcon.Error, this.ParentForm);
          }

          return;
        }

        if (LastScannedInfo == null)
        {
          LastScannedInfo = new IDScannerInfo()
          {
            Fields =
              new SerializableDictionary<string, object>()
			        {
				        {"NAME", txt_name.Text},
				        {"NAME2", txt_name2.Text},
				        {"SURNAME", txt_lastname1.Text},
				        {"SURNAME2", txt_lastname2.Text},
				        {"ID_NUMBER", txt_id.Text},
				        {
					        "SEX",
					        new[] {0, 1, 2}[(string.IsNullOrEmpty(uc_cb_gender.Text) ? 0 : uc_cb_gender.SelectedIndex + 1)]
				        },
				        {"DATEOFBIRTH", uc_dt_birth_date.DateValue},
								{"ISFRONT",true }
			        },
            IDScannerFixedFieldMapping =
              new Dictionary<ENUM_IDScannerFixedField, string>()
            {
				        {ENUM_IDScannerFixedField.DATE_OF_BIRTH, "DATEOFBIRTH"},
				        {ENUM_IDScannerFixedField.ID_NUMBER, "ID_NUMBER"},
				        {ENUM_IDScannerFixedField.ID_TYPE, "ID_TYPE"},
				        {ENUM_IDScannerFixedField.NAME, "NAME"},
				        {ENUM_IDScannerFixedField.NAME2, "NAME2"},
				        {ENUM_IDScannerFixedField.SURNAME, "SURNAME"},
				        {ENUM_IDScannerFixedField.SURNAME2, "SURNAME2"},
				        {ENUM_IDScannerFixedField.PHOTO, "PHOTO"},
				        {ENUM_IDScannerFixedField.SEX, "SEX"},
				        {ENUM_IDScannerFixedField.BIRTHPLACE, "BIRTHPLACE"},
				        {ENUM_IDScannerFixedField.ADDRESS, "Address"},
				        {ENUM_IDScannerFixedField.CITY, "City"},
				        {ENUM_IDScannerFixedField.NATIONALITY, "Nationality"},
				        {ENUM_IDScannerFixedField.ZIP, "Zip"},
				        {ENUM_IDScannerFixedField.FRONT, "DOC"},
				        {ENUM_IDScannerFixedField.DOCUMENT_EXPIRY, "EXPIRY"},
				        {ENUM_IDScannerFixedField.IS_FRONT, "ISFRONT"}
			        }
          };
        }
        OpenPlayerEdit(null, true, LastScannedInfo);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } //btn_add_customer_Click

    private void btn_clear_Click(object Sender, EventArgs E)
    {
      try
      {

        m_customer_reception = null;
        m_found_record = false;
        btn_add_customer.Enabled = CashierBusinessLogic.ReceptionButtonsEnabledCashierSessionOpenAutomatically(m_cashier_status);
        LastScannedInfo = null;
        uc_card_reader1.ClearTrackNumber();

        SetTexts();
        uc_card_reader1.Enabled = false;
        uc_card_reader1.Enabled = true;
        uc_voucher_search_CheckedChanged(null, null);
        Focus();
        if (GeneralParam.GetBoolean("Account", "InverseNameOrder", false))
        {
          txt_lastname1.Focus();
        }
        else
        {
          txt_name.Focus();
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } //btn_clear_Click

    private void txt_fullname_TextChanged(object Sender, EventArgs E)
    {
      lbl_fullname2.Text = CardData.FormatFullName(FULL_NAME_TYPE.ACCOUNT, txt_name.Text.Trim(), txt_name2.Text.Trim(),
        txt_lastname1.Text.Trim(), txt_lastname2.Text.Trim());
    } //txt_fullname_TextChanged

    private void uc_grid_entries_Click(object Sender, EventArgs E)
    {
      try
      {
        m_customer_reception = new CustomerReception();
        foreach (DataGridViewRow _row in uc_grid_entries.SelectedRows)
        {
          m_customer_reception.AccountId = Convert.ToInt64(_row.Cells[0].Value);
        }

        LoadEntry();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } //uc_grid_entries_DoubleClick

    private void uc_grid_entries_ColumnAdded(object Sender, DataGridViewColumnEventArgs E)
    {
      E.Column.SortMode = DataGridViewColumnSortMode.NotSortable;
    } //uc_grid_entries_ColumnAdded

    #endregion

    public IDScanner.IDScanner IDScanner { get; set; }

    private void btn_escanear_id_LongClick(object sender, EventArgs e)
    {
      if (IDScanner == null)
      {
        return;
      }
      string[] _scanner = IDScanner.AllAvailable();
      if (_scanner.Length == 1)
        IDScanner.AdvancedScan(_scanner[0]);
    }

    private void uc_card_reader1_OnTrackNumberReadEvent(string TrackNumber, ref bool IsValid)
    {
      CardData _card_data;
      _card_data = new CardData();


      //Get Data by TrackNumber
      CardData.DB_CardGetAllData(TrackNumber, _card_data, false);

      if (!CardData.CheckTrackdataSiteId(new ExternalTrackData(TrackNumber)))
      {

        Log.Error("LoadCardByAccountId. Card: " + _card_data.TrackData + " doesn´t belong to this site.");

        frm_message.Show(Resource.String("STR_CARD_SITE_ID_ERROR"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK,
                         MessageBoxIcon.Warning, this.ParentForm);

        uc_card_reader1.Focus();

        IsValid = false;
        return;

      }

      if (_card_data.IsNew)
      {
        DialogResult _dlg_rc;

        _dlg_rc =
          frm_message.Show(
            string.Format(
              Resource.String("STR_FRM_SEARCH_CUSTOMER_RESULTS") + "\n" +
              Resource.String("STR_FRM_SEARCH_CUSTOMER_RESULTS_ASK"), "Warning", Environment.NewLine),
            Resource.String("STR_APP_GEN_MSG_WARNING"), MessageBoxButtons.YesNo, Images.CashierImage.Warning,
            this);

        if (_dlg_rc == DialogResult.OK)
        {
          _card_data.TrackData = TrackNumber;
          OpenPlayerEdit(null, true, LastScannedInfo, TrackNumber);
        }
      }
      else
      {
        if (m_customer_reception == null) m_customer_reception = new CustomerReception();
        m_customer_reception.AccountId = _card_data.AccountId;
        LoadEntry();
      }
    }

    private void uc_card_reader1_Load(object sender, EventArgs e)
    {

    }

    private void uc_voucher_search_CheckedChanged(object sender, EventArgs e)
    {
      uc_card_reader1.Enabled = !uc_voucher_search.Checked;
      uc_card_reader1.Visible = !uc_voucher_search.Checked;
      lbl_track_data.Visible = uc_voucher_search.Checked;
      txt_track_data.Visible = uc_voucher_search.Checked;
      txt_track_data.Enabled = uc_voucher_search.Checked;
      img_ticket_scan.Visible = uc_voucher_search.Checked;
      if (uc_voucher_search.Checked)
      {
        KeyboardMessageFilter.RegisterHandler(this, BarcodeType.FilterAnyBarcode, AccountReceivedEvent);
      }
      else
      {
        KeyboardMessageFilter.UnregisterHandler(this, AccountReceivedEvent);
      }

    }

    private void FrmSearchCustomersReception_Shown(object sender, EventArgs e)
    {
      uc_voucher_search_CheckedChanged(null, null);
      if (GeneralParam.GetBoolean("Account", "InverseNameOrder", false))
      {
        txt_lastname1.Focus();
      }
      else
      {
        txt_name.Focus();
      }

    }

    private void uc_pnl_top_Paint(object sender, PaintEventArgs e)
    {

    }

    private void txt_id_KeyUp(object sender, KeyEventArgs e)
    {
    }

    private void txt_track_data_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.KeyCode == Keys.Return || e.KeyCode == Keys.Enter)
      {
        btn_ok.PerformClick();
      }
    }


    private void VisitsReport()
    {
      using (frm_yesno shader = new frm_yesno())
      {
        shader.Opacity = 0;
        shader.Show();

        using (frm_reception_visits_report _form_reception_visit_report = new frm_reception_visits_report(m_uc_bank))
        {
          if (WindowManager.StartUpMode == WindowManager.StartUpModes.Explorer)
          {
            _form_reception_visit_report.Location = new Point(
              WindowManager.GetFormCenterLocation(shader).X - (_form_reception_visit_report.Width / 2), shader.Location.Y + 2);
            _form_reception_visit_report.Width = this.Width;
            _form_reception_visit_report.Height = this.Height;
          }

          try
          {
            _form_reception_visit_report.Show(shader);
          }
          finally
          {
          }
        }
      }
    }

    private void btn_visits_report_Click(object sender, EventArgs e)
    {
      String _error_str;

      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.CustomersReceptionSearch,
                                 ProfilePermissions.TypeOperation.ShowMsg,
                                 this,
                                 out _error_str))
      {
        return;
      }

      VisitsReport();
    }

    private void uc_grid_entries_CellClick(object sender, DataGridViewCellEventArgs e)
    {
      try
      {
        m_customer_reception = new CustomerReception();
        foreach (DataGridViewRow _row in uc_grid_entries.SelectedRows)
        {
          m_customer_reception.AccountId = Convert.ToInt64(_row.Cells[0].Value);
        }

        LoadEntry();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    }

 

  }
}
