namespace WSI.Cashier
{
  partial class frm_handpay_action
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.btn_apply_tax = new WSI.Cashier.Controls.uc_round_button();
      this.lbl_handpays_title = new WSI.Cashier.Controls.uc_label();
      this.btn_tax = new WSI.Cashier.Controls.uc_round_button();
      this.lbl_tax_percent = new WSI.Cashier.Controls.uc_label();
      this.lbl_tax_base = new WSI.Cashier.Controls.uc_label();
      this.txt_base_amount = new WSI.Cashier.Controls.uc_round_textbox();
      this.txt_amount = new WSI.Cashier.Controls.uc_round_textbox();
      this.pnl_level = new System.Windows.Forms.Panel();
      this.cb_Level = new WSI.Cashier.Controls.uc_round_combobox();
      this.lbl_progresive = new WSI.Cashier.Controls.uc_label();
      this.lbl_level = new WSI.Cashier.Controls.uc_label();
      this.lbl_amount_to_pay = new WSI.Cashier.Controls.uc_label();
      this.gp_type = new WSI.Cashier.Controls.uc_round_panel();
      this.cb_hp_type = new WSI.Cashier.Controls.uc_round_combobox();
      this.lbl_to_pay = new WSI.Cashier.Controls.uc_label();
      this.gb_terminal = new WSI.Cashier.Controls.uc_round_panel();
      this.uc_terminal_filter = new WSI.Cashier.uc_terminal_filter();
      this.lbl_separator_1 = new WSI.Cashier.Controls.uc_label();
      this.lbl_tax = new WSI.Cashier.Controls.uc_label();
      this.lbl_amount = new WSI.Cashier.Controls.uc_label();
      this.pnl_body_handpay = new System.Windows.Forms.Panel();
      this.pnl_amount = new System.Windows.Forms.Panel();
      this.pnl_created_amount = new System.Windows.Forms.Panel();
      this.lbl_created_amount = new WSI.Cashier.Controls.uc_label();
      this.txt_created_amount = new WSI.Cashier.Controls.uc_round_textbox();
      this.btn_voided = new WSI.Cashier.Controls.uc_round_button();
      this.btn_payment = new WSI.Cashier.Controls.uc_round_button();
      this.btn_athorization = new WSI.Cashier.Controls.uc_round_button();
      this.btn_close = new WSI.Cashier.Controls.uc_round_button();
      this.pnl_data.SuspendLayout();
      this.pnl_level.SuspendLayout();
      this.gp_type.SuspendLayout();
      this.gb_terminal.SuspendLayout();
      this.pnl_body_handpay.SuspendLayout();
      this.pnl_amount.SuspendLayout();
      this.pnl_created_amount.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.Controls.Add(this.btn_voided);
      this.pnl_data.Controls.Add(this.btn_payment);
      this.pnl_data.Controls.Add(this.btn_athorization);
      this.pnl_data.Controls.Add(this.btn_close);
      this.pnl_data.Controls.Add(this.pnl_created_amount);
      this.pnl_data.Controls.Add(this.pnl_body_handpay);
      this.pnl_data.Controls.Add(this.pnl_level);
      this.pnl_data.Controls.Add(this.gp_type);
      this.pnl_data.Controls.Add(this.gb_terminal);
      this.pnl_data.Size = new System.Drawing.Size(471, 658);
      // 
      // btn_apply_tax
      // 
      this.btn_apply_tax.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_apply_tax.FlatAppearance.BorderSize = 0;
      this.btn_apply_tax.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_apply_tax.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_apply_tax.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_apply_tax.Image = null;
      this.btn_apply_tax.IsSelected = false;
      this.btn_apply_tax.Location = new System.Drawing.Point(135, 119);
      this.btn_apply_tax.Name = "btn_apply_tax";
      this.btn_apply_tax.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_apply_tax.Size = new System.Drawing.Size(99, 43);
      this.btn_apply_tax.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_apply_tax.TabIndex = 125;
      this.btn_apply_tax.Text = "XAPPLYTAX_HANDPAYS";
      this.btn_apply_tax.UseVisualStyleBackColor = false;
      this.btn_apply_tax.Visible = false;
      this.btn_apply_tax.Click += new System.EventHandler(this.btn_apply_tax_Click);
      // 
      // lbl_handpays_title
      // 
      this.lbl_handpays_title.BackColor = System.Drawing.Color.Transparent;
      this.lbl_handpays_title.Font = new System.Drawing.Font("Open Sans Semibold", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_handpays_title.Location = new System.Drawing.Point(0, 0);
      this.lbl_handpays_title.Name = "lbl_handpays_title";
      this.lbl_handpays_title.Size = new System.Drawing.Size(100, 23);
      this.lbl_handpays_title.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.lbl_handpays_title.TabIndex = 0;
      // 
      // btn_tax
      // 
      this.btn_tax.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(47)))), ((int)(((byte)(50)))));
      this.btn_tax.FlatAppearance.BorderSize = 0;
      this.btn_tax.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_tax.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_tax.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_tax.Image = null;
      this.btn_tax.IsSelected = false;
      this.btn_tax.Location = new System.Drawing.Point(131, 118);
      this.btn_tax.Name = "btn_tax";
      this.btn_tax.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_tax.Size = new System.Drawing.Size(99, 43);
      this.btn_tax.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.TERTIARY;
      this.btn_tax.TabIndex = 7;
      this.btn_tax.Text = "XTAX";
      this.btn_tax.UseVisualStyleBackColor = false;
      this.btn_tax.Visible = false;
      this.btn_tax.MouseClick += new System.Windows.Forms.MouseEventHandler(this.btn_tax_MouseClick);
      // 
      // lbl_tax_percent
      // 
      this.lbl_tax_percent.AutoSize = true;
      this.lbl_tax_percent.BackColor = System.Drawing.Color.Transparent;
      this.lbl_tax_percent.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_tax_percent.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_tax_percent.Location = new System.Drawing.Point(7, 117);
      this.lbl_tax_percent.Name = "lbl_tax_percent";
      this.lbl_tax_percent.Size = new System.Drawing.Size(78, 44);
      this.lbl_tax_percent.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_tax_percent.TabIndex = 124;
      this.lbl_tax_percent.Text = "xTaxes\r\n(21.00 %)";
      // 
      // lbl_tax_base
      // 
      this.lbl_tax_base.AutoSize = true;
      this.lbl_tax_base.BackColor = System.Drawing.Color.Transparent;
      this.lbl_tax_base.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_tax_base.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_tax_base.Location = new System.Drawing.Point(7, 75);
      this.lbl_tax_base.Name = "lbl_tax_base";
      this.lbl_tax_base.Size = new System.Drawing.Size(83, 22);
      this.lbl_tax_base.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_tax_base.TabIndex = 123;
      this.lbl_tax_base.Text = "xTaxBase";
      // 
      // txt_base_amount
      // 
      this.txt_base_amount.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_base_amount.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_base_amount.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_base_amount.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_base_amount.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_base_amount.CornerRadius = 5;
      this.txt_base_amount.Font = new System.Drawing.Font("Open Sans Semibold", 38F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_base_amount.Location = new System.Drawing.Point(131, 61);
      this.txt_base_amount.MaxLength = 18;
      this.txt_base_amount.Multiline = false;
      this.txt_base_amount.Name = "txt_base_amount";
      this.txt_base_amount.PasswordChar = '\0';
      this.txt_base_amount.ReadOnly = false;
      this.txt_base_amount.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_base_amount.SelectedText = "";
      this.txt_base_amount.SelectionLength = 0;
      this.txt_base_amount.SelectionStart = 0;
      this.txt_base_amount.Size = new System.Drawing.Size(315, 55);
      this.txt_base_amount.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC_CUSTOM;
      this.txt_base_amount.TabIndex = 6;
      this.txt_base_amount.TabStop = false;
      this.txt_base_amount.Text = "$99,000,000.00";
      this.txt_base_amount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      this.txt_base_amount.UseSystemPasswordChar = false;
      this.txt_base_amount.WaterMark = null;
      this.txt_base_amount.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // txt_amount
      // 
      this.txt_amount.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_amount.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_amount.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_amount.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_amount.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_amount.CornerRadius = 5;
      this.txt_amount.Font = new System.Drawing.Font("Open Sans Semibold", 38F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_amount.Location = new System.Drawing.Point(130, 2);
      this.txt_amount.MaxLength = 18;
      this.txt_amount.Multiline = false;
      this.txt_amount.Name = "txt_amount";
      this.txt_amount.PasswordChar = '\0';
      this.txt_amount.ReadOnly = true;
      this.txt_amount.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_amount.SelectedText = "";
      this.txt_amount.SelectionLength = 0;
      this.txt_amount.SelectionStart = 0;
      this.txt_amount.Size = new System.Drawing.Size(314, 55);
      this.txt_amount.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC_CUSTOM;
      this.txt_amount.TabIndex = 5;
      this.txt_amount.TabStop = false;
      this.txt_amount.Text = "$99,000,000.00";
      this.txt_amount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      this.txt_amount.UseSystemPasswordChar = false;
      this.txt_amount.WaterMark = null;
      this.txt_amount.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      this.txt_amount.TextChanged += new System.EventHandler(this.txt_amount_TextChanged);
      // 
      // pnl_level
      // 
      this.pnl_level.Controls.Add(this.cb_Level);
      this.pnl_level.Controls.Add(this.lbl_progresive);
      this.pnl_level.Controls.Add(this.lbl_level);
      this.pnl_level.Location = new System.Drawing.Point(9, 338);
      this.pnl_level.Margin = new System.Windows.Forms.Padding(0);
      this.pnl_level.Name = "pnl_level";
      this.pnl_level.Size = new System.Drawing.Size(448, 46);
      this.pnl_level.TabIndex = 4;
      // 
      // cb_Level
      // 
      this.cb_Level.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_Level.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.cb_Level.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.cb_Level.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.cb_Level.CornerRadius = 5;
      this.cb_Level.DataSource = null;
      this.cb_Level.DisplayMember = "";
      this.cb_Level.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
      this.cb_Level.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cb_Level.DropDownWidth = 65;
      this.cb_Level.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.cb_Level.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_Level.FormattingEnabled = true;
      this.cb_Level.IsDroppedDown = false;
      this.cb_Level.ItemHeight = 40;
      this.cb_Level.Location = new System.Drawing.Point(133, 3);
      this.cb_Level.MaxDropDownItems = 8;
      this.cb_Level.Name = "cb_Level";
      this.cb_Level.OnFocusOpenListBox = true;
      this.cb_Level.SelectedIndex = -1;
      this.cb_Level.SelectedItem = null;
      this.cb_Level.SelectedValue = null;
      this.cb_Level.SelectionLength = 0;
      this.cb_Level.SelectionStart = 0;
      this.cb_Level.Size = new System.Drawing.Size(65, 40);
      this.cb_Level.Sorted = false;
      this.cb_Level.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.BASIC;
      this.cb_Level.TabIndex = 0;
      this.cb_Level.TabStop = false;
      this.cb_Level.ValueMember = "";
      this.cb_Level.SelectedIndexChanged += new System.EventHandler(this.cb_Level_SelectedIndexChanged);
      // 
      // lbl_progresive
      // 
      this.lbl_progresive.BackColor = System.Drawing.Color.Transparent;
      this.lbl_progresive.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_progresive.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_progresive.Location = new System.Drawing.Point(198, 12);
      this.lbl_progresive.Name = "lbl_progresive";
      this.lbl_progresive.Size = new System.Drawing.Size(234, 23);
      this.lbl_progresive.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_progresive.TabIndex = 116;
      this.lbl_progresive.Text = "xProgressive: ---";
      this.lbl_progresive.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_level
      // 
      this.lbl_level.AutoSize = true;
      this.lbl_level.BackColor = System.Drawing.Color.Transparent;
      this.lbl_level.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_level.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_level.Location = new System.Drawing.Point(7, 12);
      this.lbl_level.Name = "lbl_level";
      this.lbl_level.Size = new System.Drawing.Size(59, 22);
      this.lbl_level.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_level.TabIndex = 106;
      this.lbl_level.Text = "xLevel";
      // 
      // lbl_amount_to_pay
      // 
      this.lbl_amount_to_pay.BackColor = System.Drawing.Color.Transparent;
      this.lbl_amount_to_pay.Font = new System.Drawing.Font("Open Sans Semibold", 32F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_amount_to_pay.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_amount_to_pay.Location = new System.Drawing.Point(99, 164);
      this.lbl_amount_to_pay.Name = "lbl_amount_to_pay";
      this.lbl_amount_to_pay.Size = new System.Drawing.Size(349, 37);
      this.lbl_amount_to_pay.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_32PX_BLACK;
      this.lbl_amount_to_pay.TabIndex = 111;
      this.lbl_amount_to_pay.Text = "$1,074,567.00";
      this.lbl_amount_to_pay.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // gp_type
      // 
      this.gp_type.BackColor = System.Drawing.Color.Transparent;
      this.gp_type.BorderColor = System.Drawing.Color.Empty;
      this.gp_type.Controls.Add(this.cb_hp_type);
      this.gp_type.CornerRadius = 10;
      this.gp_type.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gp_type.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gp_type.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gp_type.HeaderHeight = 35;
      this.gp_type.HeaderSubText = null;
      this.gp_type.HeaderText = null;
      this.gp_type.Location = new System.Drawing.Point(13, 184);
      this.gp_type.Margin = new System.Windows.Forms.Padding(0);
      this.gp_type.Name = "gp_type";
      this.gp_type.Padding = new System.Windows.Forms.Padding(2);
      this.gp_type.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gp_type.Size = new System.Drawing.Size(444, 93);
      this.gp_type.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.gp_type.TabIndex = 3;
      this.gp_type.Text = "Handpay type";
      // 
      // cb_hp_type
      // 
      this.cb_hp_type.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_hp_type.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.cb_hp_type.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.cb_hp_type.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.cb_hp_type.CornerRadius = 5;
      this.cb_hp_type.DataSource = null;
      this.cb_hp_type.DisplayMember = "";
      this.cb_hp_type.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
      this.cb_hp_type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cb_hp_type.DropDownWidth = 430;
      this.cb_hp_type.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.cb_hp_type.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_hp_type.FormattingEnabled = true;
      this.cb_hp_type.IsDroppedDown = false;
      this.cb_hp_type.ItemHeight = 40;
      this.cb_hp_type.Location = new System.Drawing.Point(7, 45);
      this.cb_hp_type.MaxDropDownItems = 8;
      this.cb_hp_type.Name = "cb_hp_type";
      this.cb_hp_type.OnFocusOpenListBox = true;
      this.cb_hp_type.SelectedIndex = -1;
      this.cb_hp_type.SelectedItem = null;
      this.cb_hp_type.SelectedValue = null;
      this.cb_hp_type.SelectionLength = 0;
      this.cb_hp_type.SelectionStart = 0;
      this.cb_hp_type.Size = new System.Drawing.Size(430, 40);
      this.cb_hp_type.Sorted = false;
      this.cb_hp_type.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.BASIC;
      this.cb_hp_type.TabIndex = 0;
      this.cb_hp_type.TabStop = false;
      this.cb_hp_type.ValueMember = "";
      this.cb_hp_type.SelectedIndexChanged += new System.EventHandler(this.cb_hp_type_SelectedIndexChanged);
      // 
      // lbl_to_pay
      // 
      this.lbl_to_pay.AutoSize = true;
      this.lbl_to_pay.BackColor = System.Drawing.Color.Transparent;
      this.lbl_to_pay.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_to_pay.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_to_pay.Location = new System.Drawing.Point(7, 171);
      this.lbl_to_pay.Name = "lbl_to_pay";
      this.lbl_to_pay.Size = new System.Drawing.Size(57, 22);
      this.lbl_to_pay.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_to_pay.TabIndex = 110;
      this.lbl_to_pay.Text = "xTotal";
      // 
      // gb_terminal
      // 
      this.gb_terminal.BackColor = System.Drawing.Color.Transparent;
      this.gb_terminal.BorderColor = System.Drawing.Color.Empty;
      this.gb_terminal.Controls.Add(this.uc_terminal_filter);
      this.gb_terminal.CornerRadius = 10;
      this.gb_terminal.Cursor = System.Windows.Forms.Cursors.Default;
      this.gb_terminal.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_terminal.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gb_terminal.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_terminal.HeaderHeight = 35;
      this.gb_terminal.HeaderSubText = null;
      this.gb_terminal.HeaderText = null;
      this.gb_terminal.Location = new System.Drawing.Point(13, 5);
      this.gb_terminal.Margin = new System.Windows.Forms.Padding(0);
      this.gb_terminal.Name = "gb_terminal";
      this.gb_terminal.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_terminal.Size = new System.Drawing.Size(444, 177);
      this.gb_terminal.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.gb_terminal.TabIndex = 2;
      this.gb_terminal.Text = "Terminal";
      // 
      // uc_terminal_filter
      // 
      this.uc_terminal_filter.BackColor = System.Drawing.Color.Transparent;
      this.uc_terminal_filter.Cursor = System.Windows.Forms.Cursors.Default;
      this.uc_terminal_filter.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(2)));
      this.uc_terminal_filter.Location = new System.Drawing.Point(-1, 35);
      this.uc_terminal_filter.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
      this.uc_terminal_filter.Name = "uc_terminal_filter";
      this.uc_terminal_filter.Size = new System.Drawing.Size(446, 142);
      this.uc_terminal_filter.TabIndex = 0;
      this.uc_terminal_filter.TerminalOrProviderChanged += new WSI.Cashier.uc_terminal_filter.TerminalOrProviderChangedEventHandler(this.uc_terminal_filter_TerminalOrProviderChanged);
      // 
      // lbl_separator_1
      // 
      this.lbl_separator_1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_separator_1.BackColor = System.Drawing.Color.Transparent;
      this.lbl_separator_1.Font = new System.Drawing.Font("Open Sans Semibold", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_separator_1.Location = new System.Drawing.Point(2, 166);
      this.lbl_separator_1.Name = "lbl_separator_1";
      this.lbl_separator_1.Size = new System.Drawing.Size(462, 1);
      this.lbl_separator_1.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.lbl_separator_1.TabIndex = 109;
      // 
      // lbl_tax
      // 
      this.lbl_tax.BackColor = System.Drawing.Color.Transparent;
      this.lbl_tax.Font = new System.Drawing.Font("Open Sans Semibold", 32F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_tax.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_tax.Location = new System.Drawing.Point(238, 118);
      this.lbl_tax.Name = "lbl_tax";
      this.lbl_tax.Size = new System.Drawing.Size(210, 37);
      this.lbl_tax.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_32PX_BLACK;
      this.lbl_tax.TabIndex = 108;
      this.lbl_tax.Text = "$160,000.00";
      this.lbl_tax.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_amount
      // 
      this.lbl_amount.AutoSize = true;
      this.lbl_amount.BackColor = System.Drawing.Color.Transparent;
      this.lbl_amount.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_amount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_amount.Location = new System.Drawing.Point(7, 17);
      this.lbl_amount.Name = "lbl_amount";
      this.lbl_amount.Size = new System.Drawing.Size(81, 22);
      this.lbl_amount.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_amount.TabIndex = 106;
      this.lbl_amount.Text = "xAmount";
      // 
      // pnl_body_handpay
      // 
      this.pnl_body_handpay.Controls.Add(this.pnl_amount);
      this.pnl_body_handpay.Controls.Add(this.btn_apply_tax);
      this.pnl_body_handpay.Controls.Add(this.btn_tax);
      this.pnl_body_handpay.Controls.Add(this.lbl_tax);
      this.pnl_body_handpay.Controls.Add(this.lbl_tax_percent);
      this.pnl_body_handpay.Controls.Add(this.lbl_separator_1);
      this.pnl_body_handpay.Controls.Add(this.lbl_tax_base);
      this.pnl_body_handpay.Controls.Add(this.lbl_to_pay);
      this.pnl_body_handpay.Controls.Add(this.txt_base_amount);
      this.pnl_body_handpay.Controls.Add(this.lbl_amount_to_pay);
      this.pnl_body_handpay.Location = new System.Drawing.Point(9, 386);
      this.pnl_body_handpay.Name = "pnl_body_handpay";
      this.pnl_body_handpay.Size = new System.Drawing.Size(449, 204);
      this.pnl_body_handpay.TabIndex = 127;
      // 
      // pnl_amount
      // 
      this.pnl_amount.Controls.Add(this.lbl_amount);
      this.pnl_amount.Controls.Add(this.txt_amount);
      this.pnl_amount.Location = new System.Drawing.Point(1, 0);
      this.pnl_amount.Name = "pnl_amount";
      this.pnl_amount.Size = new System.Drawing.Size(444, 59);
      this.pnl_amount.TabIndex = 132;
      // 
      // pnl_created_amount
      // 
      this.pnl_created_amount.Controls.Add(this.lbl_created_amount);
      this.pnl_created_amount.Controls.Add(this.txt_created_amount);
      this.pnl_created_amount.Location = new System.Drawing.Point(9, 280);
      this.pnl_created_amount.Name = "pnl_created_amount";
      this.pnl_created_amount.Size = new System.Drawing.Size(448, 57);
      this.pnl_created_amount.TabIndex = 128;
      // 
      // lbl_created_amount
      // 
      this.lbl_created_amount.BackColor = System.Drawing.Color.Transparent;
      this.lbl_created_amount.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_created_amount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_created_amount.Location = new System.Drawing.Point(6, 4);
      this.lbl_created_amount.Name = "lbl_created_amount";
      this.lbl_created_amount.Size = new System.Drawing.Size(81, 49);
      this.lbl_created_amount.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_created_amount.TabIndex = 131;
      this.lbl_created_amount.Text = "xCreated\r\nAmount";
      // 
      // txt_created_amount
      // 
      this.txt_created_amount.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_created_amount.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_created_amount.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_created_amount.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_created_amount.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_created_amount.CornerRadius = 5;
      this.txt_created_amount.Font = new System.Drawing.Font("Open Sans Semibold", 38F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_created_amount.Location = new System.Drawing.Point(132, 1);
      this.txt_created_amount.MaxLength = 18;
      this.txt_created_amount.Multiline = false;
      this.txt_created_amount.Name = "txt_created_amount";
      this.txt_created_amount.PasswordChar = '\0';
      this.txt_created_amount.ReadOnly = false;
      this.txt_created_amount.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_created_amount.SelectedText = "";
      this.txt_created_amount.SelectionLength = 0;
      this.txt_created_amount.SelectionStart = 0;
      this.txt_created_amount.Size = new System.Drawing.Size(314, 55);
      this.txt_created_amount.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC_CUSTOM;
      this.txt_created_amount.TabIndex = 130;
      this.txt_created_amount.TabStop = false;
      this.txt_created_amount.Text = "0";
      this.txt_created_amount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      this.txt_created_amount.UseSystemPasswordChar = false;
      this.txt_created_amount.WaterMark = null;
      this.txt_created_amount.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // btn_voided
      // 
      this.btn_voided.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_voided.FlatAppearance.BorderSize = 0;
      this.btn_voided.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_voided.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_voided.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_voided.Image = null;
      this.btn_voided.IsSelected = false;
      this.btn_voided.Location = new System.Drawing.Point(11, 592);
      this.btn_voided.Name = "btn_voided";
      this.btn_voided.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_voided.Size = new System.Drawing.Size(100, 60);
      this.btn_voided.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_voided.TabIndex = 137;
      this.btn_voided.Text = "XVOID";
      this.btn_voided.UseVisualStyleBackColor = false;
      this.btn_voided.Click += new System.EventHandler(this.btn_voided_Click);
      // 
      // btn_payment
      // 
      this.btn_payment.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_payment.FlatAppearance.BorderSize = 0;
      this.btn_payment.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_payment.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_payment.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_payment.Image = null;
      this.btn_payment.IsSelected = false;
      this.btn_payment.Location = new System.Drawing.Point(355, 592);
      this.btn_payment.Name = "btn_payment";
      this.btn_payment.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_payment.Size = new System.Drawing.Size(100, 60);
      this.btn_payment.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_payment.TabIndex = 139;
      this.btn_payment.Text = "XPAY";
      this.btn_payment.UseVisualStyleBackColor = false;
      this.btn_payment.Click += new System.EventHandler(this.btn_payment_Click);
      // 
      // btn_athorization
      // 
      this.btn_athorization.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(47)))), ((int)(((byte)(50)))));
      this.btn_athorization.FlatAppearance.BorderSize = 0;
      this.btn_athorization.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_athorization.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_athorization.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_athorization.Image = null;
      this.btn_athorization.IsSelected = false;
      this.btn_athorization.Location = new System.Drawing.Point(119, 592);
      this.btn_athorization.Name = "btn_athorization";
      this.btn_athorization.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_athorization.Size = new System.Drawing.Size(120, 60);
      this.btn_athorization.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.TERTIARY;
      this.btn_athorization.TabIndex = 138;
      this.btn_athorization.Text = "AUTHORIZE";
      this.btn_athorization.UseVisualStyleBackColor = false;
      this.btn_athorization.Click += new System.EventHandler(this.btn_athorization_Click);
      // 
      // btn_close
      // 
      this.btn_close.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_close.FlatAppearance.BorderSize = 0;
      this.btn_close.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_close.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_close.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_close.Image = null;
      this.btn_close.IsSelected = false;
      this.btn_close.Location = new System.Drawing.Point(247, 592);
      this.btn_close.Name = "btn_close";
      this.btn_close.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_close.Size = new System.Drawing.Size(100, 60);
      this.btn_close.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_close.TabIndex = 140;
      this.btn_close.Text = "XCANCEL";
      this.btn_close.UseVisualStyleBackColor = false;
      this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
      // 
      // frm_handpay_action
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.AutoSize = true;
      this.ClientSize = new System.Drawing.Size(471, 713);
      this.Name = "frm_handpay_action";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Style = WSI.Cashier.Controls.frm_base.BaseFormStyle.PRINCIPAL;
      this.Text = "Hand Pays";
      this.Activated += new System.EventHandler(this.frm_handpay_action_Activated);
      this.Load += new System.EventHandler(this.frm_handpay_action_Load);
      this.pnl_data.ResumeLayout(false);
      this.pnl_level.ResumeLayout(false);
      this.pnl_level.PerformLayout();
      this.gp_type.ResumeLayout(false);
      this.gb_terminal.ResumeLayout(false);
      this.pnl_body_handpay.ResumeLayout(false);
      this.pnl_body_handpay.PerformLayout();
      this.pnl_amount.ResumeLayout(false);
      this.pnl_amount.PerformLayout();
      this.pnl_created_amount.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private WSI.Cashier.Controls.uc_label lbl_handpays_title;
    private WSI.Cashier.Controls.uc_round_panel gb_terminal;

    private WSI.Cashier.Controls.uc_round_panel gp_type;
    private WSI.Cashier.Controls.uc_round_combobox cb_hp_type;
    private uc_terminal_filter uc_terminal_filter;
    private System.Windows.Forms.Panel pnl_level;
    private WSI.Cashier.Controls.uc_label lbl_level;
    private WSI.Cashier.Controls.uc_label lbl_amount;
    private WSI.Cashier.Controls.uc_label lbl_amount_to_pay;
    private WSI.Cashier.Controls.uc_label lbl_to_pay;
    private WSI.Cashier.Controls.uc_label lbl_separator_1;
    private WSI.Cashier.Controls.uc_label lbl_tax;
    private WSI.Cashier.Controls.uc_round_button btn_close;
    private WSI.Cashier.Controls.uc_round_button btn_athorization;
    private WSI.Cashier.Controls.uc_round_button btn_payment;
    private WSI.Cashier.Controls.uc_label lbl_progresive;
    private WSI.Cashier.Controls.uc_round_textbox txt_amount;
    private WSI.Cashier.Controls.uc_round_button btn_voided;
    private WSI.Cashier.Controls.uc_round_combobox cb_Level;
    private WSI.Cashier.Controls.uc_round_button btn_tax;
    private WSI.Cashier.Controls.uc_label lbl_tax_base;
    private WSI.Cashier.Controls.uc_round_textbox txt_base_amount;
    private WSI.Cashier.Controls.uc_label lbl_tax_percent;
    private WSI.Cashier.Controls.uc_round_button btn_apply_tax;
    private System.Windows.Forms.Panel pnl_body_handpay;
    private System.Windows.Forms.Panel pnl_created_amount;
    private Controls.uc_label lbl_created_amount;
    private Controls.uc_round_textbox txt_created_amount;
    private System.Windows.Forms.Panel pnl_amount;
  }
}
