namespace WSI.Cashier
{
  partial class uc_card_balance
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(uc_card_balance));
      this.pb_money = new System.Windows.Forms.PictureBox();
      this.lbl_reserved_value = new WSI.Cashier.Controls.uc_label();
      this.lbl_reserved = new WSI.Cashier.Controls.uc_label();
      this.lbl_custody_value = new WSI.Cashier.Controls.uc_label();
      this.lbl_tax_custody = new WSI.Cashier.Controls.uc_label();
      this.lbl_redeemable_value = new WSI.Cashier.Controls.uc_label();
      this.lbl_redeemable = new WSI.Cashier.Controls.uc_label();
      this.lbl_promo_redeemable_name = new WSI.Cashier.Controls.uc_label();
      this.lbl_service_value = new WSI.Cashier.Controls.uc_label();
      this.lbl_service_name = new WSI.Cashier.Controls.uc_label();
      this.lbl_promo_redeemable_value = new WSI.Cashier.Controls.uc_label();
      this.lbl_promo_not_redeemable_value = new WSI.Cashier.Controls.uc_label();
      this.lbl_promo_not_redeemable_name = new WSI.Cashier.Controls.uc_label();
      this.lbl_aux2_value = new WSI.Cashier.Controls.uc_label();
      this.lbl_aux2_name = new WSI.Cashier.Controls.uc_label();
      this.lbl_nr_won_lock_value = new WSI.Cashier.Controls.uc_label();
      this.lbl_nr_won_lock_name = new WSI.Cashier.Controls.uc_label();
      this.lbl_nr_withhold_value = new WSI.Cashier.Controls.uc_label();
      this.lbl_nr_withhold_name = new WSI.Cashier.Controls.uc_label();
      this.lbl_separator_2 = new WSI.Cashier.Controls.uc_label();
      this.lbl_cash_in_value_2 = new WSI.Cashier.Controls.uc_label();
      this.lbl_cash_in_name_2 = new WSI.Cashier.Controls.uc_label();
      this.lbl_balance_name_1 = new WSI.Cashier.Controls.uc_label();
      this.lbl_balance_value_1 = new WSI.Cashier.Controls.uc_label();
      this.lbl_casheable_value = new WSI.Cashier.Controls.uc_label();
      this.lbl_aux1_value = new WSI.Cashier.Controls.uc_label();
      this.lbl_won_value_2 = new WSI.Cashier.Controls.uc_label();
      this.lbl_initial_cash_in_value = new WSI.Cashier.Controls.uc_label();
      this.lbl_summary_not_redeemable_value = new WSI.Cashier.Controls.uc_label();
      this.lbl_casheable_name = new WSI.Cashier.Controls.uc_label();
      this.lbl_aux1_name = new WSI.Cashier.Controls.uc_label();
      this.lbl_summary_not_redeemable_name = new WSI.Cashier.Controls.uc_label();
      this.lbl_won_name_2 = new WSI.Cashier.Controls.uc_label();
      this.lbl_initial_cash_in_name = new WSI.Cashier.Controls.uc_label();
      this.lbl_bucket_re_value = new WSI.Cashier.Controls.uc_label();
      this.lbl_bucket_re = new WSI.Cashier.Controls.uc_label();
      this.lbl_bucket_nr_value = new WSI.Cashier.Controls.uc_label();
      this.lbl_bucket_nr = new WSI.Cashier.Controls.uc_label();
      this.lbl_credit_line_balance_value = new WSI.Cashier.Controls.uc_label();
      this.lbl_credit_line_balance = new WSI.Cashier.Controls.uc_label();
      ((System.ComponentModel.ISupportInitialize)(this.pb_money)).BeginInit();
      this.SuspendLayout();
      // 
      // pb_money
      // 
      this.pb_money.Image = global::WSI.Cashier.Properties.Resources.coins;
      this.pb_money.InitialImage = ((System.Drawing.Image)(resources.GetObject("pb_money.InitialImage")));
      this.pb_money.Location = new System.Drawing.Point(3, 3);
      this.pb_money.Name = "pb_money";
      this.pb_money.Size = new System.Drawing.Size(64, 59);
      this.pb_money.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
      this.pb_money.TabIndex = 94;
      this.pb_money.TabStop = false;
      // 
      // lbl_reserved_value
      // 
      this.lbl_reserved_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_reserved_value.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_reserved_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_reserved_value.Location = new System.Drawing.Point(156, 140);
      this.lbl_reserved_value.Name = "lbl_reserved_value";
      this.lbl_reserved_value.Size = new System.Drawing.Size(148, 15);
      this.lbl_reserved_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_reserved_value.TabIndex = 95;
      this.lbl_reserved_value.Text = "0,00";
      this.lbl_reserved_value.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_reserved
      // 
      this.lbl_reserved.BackColor = System.Drawing.Color.Transparent;
      this.lbl_reserved.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_reserved.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_reserved.Location = new System.Drawing.Point(21, 140);
      this.lbl_reserved.Name = "lbl_reserved";
      this.lbl_reserved.Size = new System.Drawing.Size(135, 15);
      this.lbl_reserved.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_reserved.TabIndex = 94;
      this.lbl_reserved.Text = "xReserved";
      this.lbl_reserved.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_custody_value
      // 
      this.lbl_custody_value.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_custody_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_custody_value.Location = new System.Drawing.Point(155, 298);
      this.lbl_custody_value.Name = "lbl_custody_value";
      this.lbl_custody_value.Size = new System.Drawing.Size(149, 20);
      this.lbl_custody_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_custody_value.TabIndex = 95;
      this.lbl_custody_value.Text = "0,00";
      this.lbl_custody_value.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_tax_custody
      // 
      this.lbl_tax_custody.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_tax_custody.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_tax_custody.Location = new System.Drawing.Point(1, 298);
      this.lbl_tax_custody.Name = "lbl_tax_custody";
      this.lbl_tax_custody.Size = new System.Drawing.Size(156, 20);
      this.lbl_tax_custody.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_tax_custody.TabIndex = 94;
      this.lbl_tax_custody.Text = "xCustody";
      this.lbl_tax_custody.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_redeemable_value
      // 
      this.lbl_redeemable_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_redeemable_value.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_redeemable_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_redeemable_value.Location = new System.Drawing.Point(156, 65);
      this.lbl_redeemable_value.Name = "lbl_redeemable_value";
      this.lbl_redeemable_value.Size = new System.Drawing.Size(148, 16);
      this.lbl_redeemable_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_redeemable_value.TabIndex = 92;
      this.lbl_redeemable_value.Text = "0,00";
      this.lbl_redeemable_value.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_redeemable
      // 
      this.lbl_redeemable.BackColor = System.Drawing.Color.Transparent;
      this.lbl_redeemable.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_redeemable.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_redeemable.Location = new System.Drawing.Point(0, 65);
      this.lbl_redeemable.Name = "lbl_redeemable";
      this.lbl_redeemable.Size = new System.Drawing.Size(156, 15);
      this.lbl_redeemable.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_redeemable.TabIndex = 93;
      this.lbl_redeemable.Text = "xRedeemable";
      this.lbl_redeemable.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_promo_redeemable_name
      // 
      this.lbl_promo_redeemable_name.BackColor = System.Drawing.Color.Transparent;
      this.lbl_promo_redeemable_name.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_promo_redeemable_name.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_promo_redeemable_name.Location = new System.Drawing.Point(0, 80);
      this.lbl_promo_redeemable_name.Name = "lbl_promo_redeemable_name";
      this.lbl_promo_redeemable_name.Size = new System.Drawing.Size(156, 15);
      this.lbl_promo_redeemable_name.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_promo_redeemable_name.TabIndex = 84;
      this.lbl_promo_redeemable_name.Text = "xProm Redeemable";
      this.lbl_promo_redeemable_name.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_service_value
      // 
      this.lbl_service_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_service_value.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_service_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_service_value.Location = new System.Drawing.Point(156, 321);
      this.lbl_service_value.Name = "lbl_service_value";
      this.lbl_service_value.Size = new System.Drawing.Size(148, 16);
      this.lbl_service_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_service_value.TabIndex = 90;
      this.lbl_service_value.Text = "0,00";
      this.lbl_service_value.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_service_name
      // 
      this.lbl_service_name.BackColor = System.Drawing.Color.Transparent;
      this.lbl_service_name.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_service_name.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_service_name.Location = new System.Drawing.Point(0, 319);
      this.lbl_service_name.Name = "lbl_service_name";
      this.lbl_service_name.Size = new System.Drawing.Size(156, 20);
      this.lbl_service_name.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_service_name.TabIndex = 91;
      this.lbl_service_name.Text = "xServiceCharge";
      this.lbl_service_name.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_promo_redeemable_value
      // 
      this.lbl_promo_redeemable_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_promo_redeemable_value.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_promo_redeemable_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_promo_redeemable_value.Location = new System.Drawing.Point(156, 80);
      this.lbl_promo_redeemable_value.Name = "lbl_promo_redeemable_value";
      this.lbl_promo_redeemable_value.Size = new System.Drawing.Size(148, 16);
      this.lbl_promo_redeemable_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_promo_redeemable_value.TabIndex = 89;
      this.lbl_promo_redeemable_value.Text = "0,00";
      this.lbl_promo_redeemable_value.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_promo_not_redeemable_value
      // 
      this.lbl_promo_not_redeemable_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_promo_not_redeemable_value.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_promo_not_redeemable_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_promo_not_redeemable_value.Location = new System.Drawing.Point(156, 95);
      this.lbl_promo_not_redeemable_value.Name = "lbl_promo_not_redeemable_value";
      this.lbl_promo_not_redeemable_value.Size = new System.Drawing.Size(148, 16);
      this.lbl_promo_not_redeemable_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_promo_not_redeemable_value.TabIndex = 88;
      this.lbl_promo_not_redeemable_value.Text = "0,00";
      this.lbl_promo_not_redeemable_value.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_promo_not_redeemable_name
      // 
      this.lbl_promo_not_redeemable_name.BackColor = System.Drawing.Color.Transparent;
      this.lbl_promo_not_redeemable_name.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_promo_not_redeemable_name.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_promo_not_redeemable_name.Location = new System.Drawing.Point(0, 95);
      this.lbl_promo_not_redeemable_name.Name = "lbl_promo_not_redeemable_name";
      this.lbl_promo_not_redeemable_name.Size = new System.Drawing.Size(156, 15);
      this.lbl_promo_not_redeemable_name.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_promo_not_redeemable_name.TabIndex = 87;
      this.lbl_promo_not_redeemable_name.Text = "xPromo Not Redeemable";
      this.lbl_promo_not_redeemable_name.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_aux2_value
      // 
      this.lbl_aux2_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_aux2_value.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_aux2_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_aux2_value.Location = new System.Drawing.Point(156, 341);
      this.lbl_aux2_value.Name = "lbl_aux2_value";
      this.lbl_aux2_value.Size = new System.Drawing.Size(148, 16);
      this.lbl_aux2_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_aux2_value.TabIndex = 85;
      this.lbl_aux2_value.Text = "0,00";
      this.lbl_aux2_value.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_aux2_name
      // 
      this.lbl_aux2_name.BackColor = System.Drawing.Color.Transparent;
      this.lbl_aux2_name.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_aux2_name.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_aux2_name.Location = new System.Drawing.Point(0, 339);
      this.lbl_aux2_name.Name = "lbl_aux2_name";
      this.lbl_aux2_name.Size = new System.Drawing.Size(156, 20);
      this.lbl_aux2_name.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_aux2_name.TabIndex = 86;
      this.lbl_aux2_name.Text = "xRounding";
      this.lbl_aux2_name.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_nr_won_lock_value
      // 
      this.lbl_nr_won_lock_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_nr_won_lock_value.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_nr_won_lock_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_nr_won_lock_value.Location = new System.Drawing.Point(156, 125);
      this.lbl_nr_won_lock_value.Name = "lbl_nr_won_lock_value";
      this.lbl_nr_won_lock_value.Size = new System.Drawing.Size(148, 16);
      this.lbl_nr_won_lock_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_nr_won_lock_value.TabIndex = 83;
      this.lbl_nr_won_lock_value.Text = "0,00";
      this.lbl_nr_won_lock_value.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_nr_won_lock_name
      // 
      this.lbl_nr_won_lock_name.BackColor = System.Drawing.Color.Transparent;
      this.lbl_nr_won_lock_name.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_nr_won_lock_name.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_nr_won_lock_name.Location = new System.Drawing.Point(0, 125);
      this.lbl_nr_won_lock_name.Name = "lbl_nr_won_lock_name";
      this.lbl_nr_won_lock_name.Size = new System.Drawing.Size(156, 15);
      this.lbl_nr_won_lock_name.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_nr_won_lock_name.TabIndex = 82;
      this.lbl_nr_won_lock_name.Text = "xNR Won Lock";
      this.lbl_nr_won_lock_name.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_nr_withhold_value
      // 
      this.lbl_nr_withhold_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_nr_withhold_value.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_nr_withhold_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_nr_withhold_value.Location = new System.Drawing.Point(156, 110);
      this.lbl_nr_withhold_value.Name = "lbl_nr_withhold_value";
      this.lbl_nr_withhold_value.Size = new System.Drawing.Size(148, 16);
      this.lbl_nr_withhold_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_nr_withhold_value.TabIndex = 79;
      this.lbl_nr_withhold_value.Text = "0,00";
      this.lbl_nr_withhold_value.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_nr_withhold_name
      // 
      this.lbl_nr_withhold_name.BackColor = System.Drawing.Color.Transparent;
      this.lbl_nr_withhold_name.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_nr_withhold_name.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_nr_withhold_name.Location = new System.Drawing.Point(0, 110);
      this.lbl_nr_withhold_name.Name = "lbl_nr_withhold_name";
      this.lbl_nr_withhold_name.Size = new System.Drawing.Size(156, 15);
      this.lbl_nr_withhold_name.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_nr_withhold_name.TabIndex = 78;
      this.lbl_nr_withhold_name.Text = "xWithHold";
      this.lbl_nr_withhold_name.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_separator_2
      // 
      this.lbl_separator_2.BackColor = System.Drawing.Color.Transparent;
      this.lbl_separator_2.Font = new System.Drawing.Font("Open Sans Semibold", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_separator_2.Location = new System.Drawing.Point(3, 302);
      this.lbl_separator_2.Name = "lbl_separator_2";
      this.lbl_separator_2.Size = new System.Drawing.Size(307, 1);
      this.lbl_separator_2.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.lbl_separator_2.TabIndex = 76;
      this.lbl_separator_2.Text = "0,00";
      this.lbl_separator_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_cash_in_value_2
      // 
      this.lbl_cash_in_value_2.BackColor = System.Drawing.Color.Transparent;
      this.lbl_cash_in_value_2.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_cash_in_value_2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_cash_in_value_2.Location = new System.Drawing.Point(156, 235);
      this.lbl_cash_in_value_2.Name = "lbl_cash_in_value_2";
      this.lbl_cash_in_value_2.Size = new System.Drawing.Size(148, 16);
      this.lbl_cash_in_value_2.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_cash_in_value_2.TabIndex = 75;
      this.lbl_cash_in_value_2.Text = "0,00";
      this.lbl_cash_in_value_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_cash_in_name_2
      // 
      this.lbl_cash_in_name_2.BackColor = System.Drawing.Color.Transparent;
      this.lbl_cash_in_name_2.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_cash_in_name_2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_cash_in_name_2.Location = new System.Drawing.Point(0, 233);
      this.lbl_cash_in_name_2.Name = "lbl_cash_in_name_2";
      this.lbl_cash_in_name_2.Size = new System.Drawing.Size(156, 20);
      this.lbl_cash_in_name_2.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_cash_in_name_2.TabIndex = 74;
      this.lbl_cash_in_name_2.Text = "xCash In";
      this.lbl_cash_in_name_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_balance_name_1
      // 
      this.lbl_balance_name_1.BackColor = System.Drawing.Color.Transparent;
      this.lbl_balance_name_1.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_balance_name_1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_balance_name_1.Location = new System.Drawing.Point(3, 19);
      this.lbl_balance_name_1.Name = "lbl_balance_name_1";
      this.lbl_balance_name_1.Size = new System.Drawing.Size(153, 20);
      this.lbl_balance_name_1.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_balance_name_1.TabIndex = 66;
      this.lbl_balance_name_1.Text = "xBalance";
      this.lbl_balance_name_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_balance_value_1
      // 
      this.lbl_balance_value_1.BackColor = System.Drawing.Color.Transparent;
      this.lbl_balance_value_1.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_balance_value_1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_balance_value_1.Location = new System.Drawing.Point(152, 17);
      this.lbl_balance_value_1.Name = "lbl_balance_value_1";
      this.lbl_balance_value_1.Size = new System.Drawing.Size(153, 24);
      this.lbl_balance_value_1.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_balance_value_1.TabIndex = 52;
      this.lbl_balance_value_1.Text = "999,999,999.99";
      this.lbl_balance_value_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_casheable_value
      // 
      this.lbl_casheable_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_casheable_value.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_casheable_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_casheable_value.Location = new System.Drawing.Point(151, 370);
      this.lbl_casheable_value.Name = "lbl_casheable_value";
      this.lbl_casheable_value.Size = new System.Drawing.Size(153, 24);
      this.lbl_casheable_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_casheable_value.TabIndex = 63;
      this.lbl_casheable_value.Text = "999,999,999.99";
      this.lbl_casheable_value.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_aux1_value
      // 
      this.lbl_aux1_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_aux1_value.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_aux1_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_aux1_value.Location = new System.Drawing.Point(156, 279);
      this.lbl_aux1_value.Name = "lbl_aux1_value";
      this.lbl_aux1_value.Size = new System.Drawing.Size(148, 16);
      this.lbl_aux1_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_aux1_value.TabIndex = 61;
      this.lbl_aux1_value.Text = "0,00";
      this.lbl_aux1_value.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_won_value_2
      // 
      this.lbl_won_value_2.BackColor = System.Drawing.Color.Transparent;
      this.lbl_won_value_2.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_won_value_2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_won_value_2.Location = new System.Drawing.Point(156, 258);
      this.lbl_won_value_2.Name = "lbl_won_value_2";
      this.lbl_won_value_2.Size = new System.Drawing.Size(148, 16);
      this.lbl_won_value_2.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_won_value_2.TabIndex = 60;
      this.lbl_won_value_2.Text = "0,00";
      this.lbl_won_value_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_initial_cash_in_value
      // 
      this.lbl_initial_cash_in_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_initial_cash_in_value.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_initial_cash_in_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_initial_cash_in_value.Location = new System.Drawing.Point(156, 47);
      this.lbl_initial_cash_in_value.Name = "lbl_initial_cash_in_value";
      this.lbl_initial_cash_in_value.Size = new System.Drawing.Size(148, 16);
      this.lbl_initial_cash_in_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_initial_cash_in_value.TabIndex = 53;
      this.lbl_initial_cash_in_value.Text = "0,00";
      this.lbl_initial_cash_in_value.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_summary_not_redeemable_value
      // 
      this.lbl_summary_not_redeemable_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_summary_not_redeemable_value.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_summary_not_redeemable_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_summary_not_redeemable_value.Location = new System.Drawing.Point(156, 202);
      this.lbl_summary_not_redeemable_value.Name = "lbl_summary_not_redeemable_value";
      this.lbl_summary_not_redeemable_value.Size = new System.Drawing.Size(148, 16);
      this.lbl_summary_not_redeemable_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_summary_not_redeemable_value.TabIndex = 55;
      this.lbl_summary_not_redeemable_value.Text = "0,00";
      this.lbl_summary_not_redeemable_value.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_casheable_name
      // 
      this.lbl_casheable_name.BackColor = System.Drawing.Color.Transparent;
      this.lbl_casheable_name.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_casheable_name.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_casheable_name.Location = new System.Drawing.Point(0, 372);
      this.lbl_casheable_name.Name = "lbl_casheable_name";
      this.lbl_casheable_name.Size = new System.Drawing.Size(156, 20);
      this.lbl_casheable_name.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_casheable_name.TabIndex = 64;
      this.lbl_casheable_name.Text = "xCasheable";
      this.lbl_casheable_name.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_aux1_name
      // 
      this.lbl_aux1_name.BackColor = System.Drawing.Color.Transparent;
      this.lbl_aux1_name.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_aux1_name.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_aux1_name.Location = new System.Drawing.Point(0, 277);
      this.lbl_aux1_name.Name = "lbl_aux1_name";
      this.lbl_aux1_name.Size = new System.Drawing.Size(156, 20);
      this.lbl_aux1_name.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_aux1_name.TabIndex = 62;
      this.lbl_aux1_name.Text = "xTax1";
      this.lbl_aux1_name.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_summary_not_redeemable_name
      // 
      this.lbl_summary_not_redeemable_name.BackColor = System.Drawing.Color.Transparent;
      this.lbl_summary_not_redeemable_name.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_summary_not_redeemable_name.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_summary_not_redeemable_name.Location = new System.Drawing.Point(0, 200);
      this.lbl_summary_not_redeemable_name.Name = "lbl_summary_not_redeemable_name";
      this.lbl_summary_not_redeemable_name.Size = new System.Drawing.Size(156, 20);
      this.lbl_summary_not_redeemable_name.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_summary_not_redeemable_name.TabIndex = 56;
      this.lbl_summary_not_redeemable_name.Text = "xNot Redeemable";
      this.lbl_summary_not_redeemable_name.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_won_name_2
      // 
      this.lbl_won_name_2.BackColor = System.Drawing.Color.Transparent;
      this.lbl_won_name_2.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_won_name_2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_won_name_2.Location = new System.Drawing.Point(0, 256);
      this.lbl_won_name_2.Name = "lbl_won_name_2";
      this.lbl_won_name_2.Size = new System.Drawing.Size(156, 20);
      this.lbl_won_name_2.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_won_name_2.TabIndex = 59;
      this.lbl_won_name_2.Text = "xWon";
      this.lbl_won_name_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_initial_cash_in_name
      // 
      this.lbl_initial_cash_in_name.BackColor = System.Drawing.Color.Transparent;
      this.lbl_initial_cash_in_name.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_initial_cash_in_name.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_initial_cash_in_name.Location = new System.Drawing.Point(6, 47);
      this.lbl_initial_cash_in_name.Name = "lbl_initial_cash_in_name";
      this.lbl_initial_cash_in_name.Size = new System.Drawing.Size(150, 15);
      this.lbl_initial_cash_in_name.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_initial_cash_in_name.TabIndex = 54;
      this.lbl_initial_cash_in_name.Text = "xInitial Cash In";
      this.lbl_initial_cash_in_name.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_bucket_re_value
      // 
      this.lbl_bucket_re_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_bucket_re_value.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_bucket_re_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_bucket_re_value.Location = new System.Drawing.Point(156, 184);
      this.lbl_bucket_re_value.Name = "lbl_bucket_re_value";
      this.lbl_bucket_re_value.Size = new System.Drawing.Size(148, 15);
      this.lbl_bucket_re_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_bucket_re_value.TabIndex = 99;
      this.lbl_bucket_re_value.Text = "0,00";
      this.lbl_bucket_re_value.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_bucket_re
      // 
      this.lbl_bucket_re.BackColor = System.Drawing.Color.Transparent;
      this.lbl_bucket_re.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_bucket_re.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_bucket_re.Location = new System.Drawing.Point(21, 184);
      this.lbl_bucket_re.Name = "lbl_bucket_re";
      this.lbl_bucket_re.Size = new System.Drawing.Size(135, 15);
      this.lbl_bucket_re.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_bucket_re.TabIndex = 98;
      this.lbl_bucket_re.Text = "xBucketRE";
      this.lbl_bucket_re.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_bucket_nr_value
      // 
      this.lbl_bucket_nr_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_bucket_nr_value.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_bucket_nr_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_bucket_nr_value.Location = new System.Drawing.Point(156, 169);
      this.lbl_bucket_nr_value.Name = "lbl_bucket_nr_value";
      this.lbl_bucket_nr_value.Size = new System.Drawing.Size(148, 16);
      this.lbl_bucket_nr_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_bucket_nr_value.TabIndex = 97;
      this.lbl_bucket_nr_value.Text = "0,00";
      this.lbl_bucket_nr_value.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_bucket_nr
      // 
      this.lbl_bucket_nr.BackColor = System.Drawing.Color.Transparent;
      this.lbl_bucket_nr.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_bucket_nr.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_bucket_nr.Location = new System.Drawing.Point(0, 169);
      this.lbl_bucket_nr.Name = "lbl_bucket_nr";
      this.lbl_bucket_nr.Size = new System.Drawing.Size(156, 15);
      this.lbl_bucket_nr.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_bucket_nr.TabIndex = 96;
      this.lbl_bucket_nr.Text = "xBucketNR";
      this.lbl_bucket_nr.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_credit_line_balance_value
      // 
      this.lbl_credit_line_balance_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_credit_line_balance_value.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_credit_line_balance_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_credit_line_balance_value.Location = new System.Drawing.Point(156, 154);
      this.lbl_credit_line_balance_value.Name = "lbl_credit_line_balance_value";
      this.lbl_credit_line_balance_value.Size = new System.Drawing.Size(148, 15);
      this.lbl_credit_line_balance_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_credit_line_balance_value.TabIndex = 101;
      this.lbl_credit_line_balance_value.Text = "0,00";
      this.lbl_credit_line_balance_value.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      this.lbl_credit_line_balance_value.Visible = false;
      // 
      // lbl_credit_line_balance
      // 
      this.lbl_credit_line_balance.BackColor = System.Drawing.Color.Transparent;
      this.lbl_credit_line_balance.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_credit_line_balance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_credit_line_balance.Location = new System.Drawing.Point(21, 154);
      this.lbl_credit_line_balance.Name = "lbl_credit_line_balance";
      this.lbl_credit_line_balance.Size = new System.Drawing.Size(135, 19);
      this.lbl_credit_line_balance.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_credit_line_balance.TabIndex = 100;
      this.lbl_credit_line_balance.Text = "xCreditLineBalance";
      this.lbl_credit_line_balance.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      this.lbl_credit_line_balance.Visible = false;
      // 
      // uc_card_balance
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 24F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.pb_money);
      this.Controls.Add(this.lbl_reserved_value);
      this.Controls.Add(this.lbl_reserved);
      this.Controls.Add(this.lbl_custody_value);
      this.Controls.Add(this.lbl_tax_custody);
      this.Controls.Add(this.lbl_redeemable_value);
      this.Controls.Add(this.lbl_redeemable);
      this.Controls.Add(this.lbl_promo_redeemable_name);
      this.Controls.Add(this.lbl_service_value);
      this.Controls.Add(this.lbl_service_name);
      this.Controls.Add(this.lbl_promo_redeemable_value);
      this.Controls.Add(this.lbl_promo_not_redeemable_value);
      this.Controls.Add(this.lbl_promo_not_redeemable_name);
      this.Controls.Add(this.lbl_aux2_value);
      this.Controls.Add(this.lbl_aux2_name);
      this.Controls.Add(this.lbl_nr_won_lock_value);
      this.Controls.Add(this.lbl_nr_won_lock_name);
      this.Controls.Add(this.lbl_nr_withhold_value);
      this.Controls.Add(this.lbl_nr_withhold_name);
      this.Controls.Add(this.lbl_separator_2);
      this.Controls.Add(this.lbl_cash_in_value_2);
      this.Controls.Add(this.lbl_cash_in_name_2);
      this.Controls.Add(this.lbl_balance_name_1);
      this.Controls.Add(this.lbl_balance_value_1);
      this.Controls.Add(this.lbl_casheable_value);
      this.Controls.Add(this.lbl_aux1_value);
      this.Controls.Add(this.lbl_won_value_2);
      this.Controls.Add(this.lbl_initial_cash_in_value);
      this.Controls.Add(this.lbl_summary_not_redeemable_value);
      this.Controls.Add(this.lbl_casheable_name);
      this.Controls.Add(this.lbl_aux1_name);
      this.Controls.Add(this.lbl_summary_not_redeemable_name);
      this.Controls.Add(this.lbl_won_name_2);
      this.Controls.Add(this.lbl_initial_cash_in_name);
      this.Controls.Add(this.lbl_bucket_re_value);
      this.Controls.Add(this.lbl_bucket_re);
      this.Controls.Add(this.lbl_bucket_nr_value);
      this.Controls.Add(this.lbl_bucket_nr);
      this.Controls.Add(this.lbl_credit_line_balance_value);
      this.Controls.Add(this.lbl_credit_line_balance);
      this.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.Name = "uc_card_balance";
      this.Size = new System.Drawing.Size(313, 398);
      ((System.ComponentModel.ISupportInitialize)(this.pb_money)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private WSI.Cashier.Controls.uc_label lbl_casheable_value;
    private WSI.Cashier.Controls.uc_label lbl_aux1_value;
    private WSI.Cashier.Controls.uc_label lbl_won_value_2;
    private WSI.Cashier.Controls.uc_label lbl_initial_cash_in_value;
    private WSI.Cashier.Controls.uc_label lbl_summary_not_redeemable_value;
    private WSI.Cashier.Controls.uc_label lbl_balance_value_1;
    private WSI.Cashier.Controls.uc_label lbl_casheable_name;
    private WSI.Cashier.Controls.uc_label lbl_aux1_name;
    private WSI.Cashier.Controls.uc_label lbl_summary_not_redeemable_name;
    private WSI.Cashier.Controls.uc_label lbl_won_name_2;
    private WSI.Cashier.Controls.uc_label lbl_initial_cash_in_name;
    private WSI.Cashier.Controls.uc_label lbl_balance_name_1;
    private WSI.Cashier.Controls.uc_label lbl_cash_in_value_2;
    private WSI.Cashier.Controls.uc_label lbl_cash_in_name_2;
    private WSI.Cashier.Controls.uc_label lbl_separator_2;
    private WSI.Cashier.Controls.uc_label lbl_nr_withhold_name;
    private WSI.Cashier.Controls.uc_label lbl_nr_withhold_value;
    private WSI.Cashier.Controls.uc_label lbl_nr_won_lock_value;
    private WSI.Cashier.Controls.uc_label lbl_nr_won_lock_name;
    private WSI.Cashier.Controls.uc_label lbl_promo_redeemable_name;
    private WSI.Cashier.Controls.uc_label lbl_aux2_value;
    private WSI.Cashier.Controls.uc_label lbl_aux2_name;
    private WSI.Cashier.Controls.uc_label lbl_promo_not_redeemable_value;
    private WSI.Cashier.Controls.uc_label lbl_promo_not_redeemable_name;
    private WSI.Cashier.Controls.uc_label lbl_promo_redeemable_value;
    private WSI.Cashier.Controls.uc_label lbl_service_value;
    private WSI.Cashier.Controls.uc_label lbl_service_name;
    private WSI.Cashier.Controls.uc_label lbl_redeemable_value;
    private WSI.Cashier.Controls.uc_label lbl_redeemable;
    private WSI.Cashier.Controls.uc_label lbl_reserved_value;
    private WSI.Cashier.Controls.uc_label lbl_reserved;
    private System.Windows.Forms.PictureBox pb_money;
    private Controls.uc_label lbl_bucket_re_value;
    private Controls.uc_label lbl_bucket_re;
    private Controls.uc_label lbl_bucket_nr_value;
    private Controls.uc_label lbl_bucket_nr;
    private Controls.uc_label lbl_tax_custody;
    private Controls.uc_label lbl_custody_value;
    private Controls.uc_label lbl_credit_line_balance;
    private Controls.uc_label lbl_credit_line_balance_value;
    


  }
}
