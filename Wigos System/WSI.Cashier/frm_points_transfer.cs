//------------------------------------------------------------------------------
// Copyright � 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_points_transfer.cs
// 
//   DESCRIPTION: Implements the following classes:
//                1. frm_points_transfer
//
//        AUTHOR: AAC
// 
// CREATION DATE: 07-AUG-2007
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 07-AUG-2007 ACC     First release.
// 25-MAY-2012 SSC     Moved CardData, PlaySession and PlayerTrackingData classes to WSI.Common
// 17-MAR-2014 JRM     Added new functionality. Description contained in RequestPinCodeDUT.docx DUT
// 03-APR-2014 JRM     Fixed bug: WIGOSTITO-1187
// 18-SEP-2017 DPC     WIGOS-5268: Cashier & GUI - Icons - Implement
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WSI.Common;
using System.Data.SqlClient;
using System.Globalization;

namespace WSI.Cashier
{
  public partial class frm_points_transfer : Controls.frm_base_icon
  {
    #region Attributes

    //////private static Boolean m_cancel = true;
    
    //private static VoucherTypes m_voucher_type;
    //private static CardData m_card_data;
    //private static MBCardData m_mb_card_data;
    //private static Currency m_aux_amount;
    //private static CASH_MODE m_cash_mode;

    private static Points m_voucher_amount;
    private static Points m_out_amount;
    private static Points m_max_amount;

    private static CardData m_card_origin;
    private static CardData m_card_destiny;

    //private static Font m_redeemable_font;
    //private static Font m_non_redeemable_font;

    //private Boolean redeemable;

    ////private Decimal decimal_value;
    ////private Boolean is_voucher_loaded;


    #endregion

    #region Constructor

    /// <summary>
    /// Constructor method.
    /// 1. Initialize controls.
    /// 2. Initialize control�s resources. (NLS, Images, etc)
    /// </summary>
    public frm_points_transfer()
    {
      InitializeComponent();

      InitializeControlResources();

      uc_card_reader1.OnTrackNumberReadEvent += new uc_card_reader.TrackNumberReadEventHandler(uc_card_reader1_OnTrackNumberReadEvent);
      uc_card_reader1.OnTrackNumberReadingEvent += new uc_card_reader.TrackNumberReadingHandler(uc_card_reader1_OnTrackNumberReadingEvent);

      m_card_destiny = null;

    }

    #endregion
    
    #region Buttons

    private void btn_num_1_Click(object sender, EventArgs e)
    {
      AddNumber("1");
    }

    private void btn_num_2_Click(object sender, EventArgs e)
    {
      AddNumber("2");
    }

    private void btn_num_3_Click(object sender, EventArgs e)
    {
      AddNumber("3");
    }

    private void btn_num_4_Click(object sender, EventArgs e)
    {
      AddNumber("4");
    }

    private void btn_num_5_Click(object sender, EventArgs e)
    {
      AddNumber("5");
    }

    private void btn_num_6_Click(object sender, EventArgs e)
    {
      AddNumber("6");
    }

    private void btn_num_7_Click(object sender, EventArgs e)
    {
      AddNumber("7");
    }

    private void btn_num_8_Click(object sender, EventArgs e)
    {
      AddNumber("8");
    }

    private void btn_num_9_Click(object sender, EventArgs e)
    {
      AddNumber("9");
    }

    private void btn_num_10_Click(object sender, EventArgs e)
    {
      AddNumber("0");
    }

    private void btn_num_dot_Click(object sender, EventArgs e)
    {
      btn_intro.Focus();
    }

    private void btn_cancel_Click(object sender, EventArgs e)
    {
      ////m_cancel = true;

      Misc.WriteLog("[FORM CLOSE] frm_points_transfer (cancel)", Log.Type.Message);
      this.Close();
    }

    private void btn_back_Click(object sender, EventArgs e)
    {
      if (txt_amount.Text.Length > 0)
      {
        txt_amount.Text = txt_amount.Text.Substring(0, txt_amount.Text.Length-1);
      }
      btn_intro.Focus();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Handle the click on the ok button, which confirms all shown data
    //           and closes the screen.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //

    private void btn_ok_Click(object sender, EventArgs e)
    {
      DialogResult question_dialog;

      

      // TODO
      
      // Show confirmation message for non-redeemable credit
      question_dialog = frm_message.Show("Are you sure?", "O'RLY??", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, this);
      if (question_dialog != DialogResult.OK)
      {
        return;
      }
      else
      {
        //////////String error_str; 

        // Check if current user is allowed or not to add non-redeemable credit, 
        // otherwise request credentials of an authorized user

        // TODO

        //if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.CardAddNonRedeemableCredit, ProfilePermissions.TypeOperation.RequestPasswd, this, out error_str))
        //{
        //  return;
        //}
      }

      ////m_cancel = false;

      Misc.WriteLog("[FORM CLOSE] frm_points_transfer (ok)", Log.Type.Message);
      this.Close();
    } // btn_ok_Click

    //------------------------------------------------------------------------------
    // PURPOSE : Handle the click on the intro button, which confirms the amount on the
    //           numeric pad.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //

    private void btn_intro_Click(object sender, EventArgs e)
    {
      Points input_amount;

      btn_ok.Enabled = false;

      input_amount = 0;
      if (txt_amount.Text != "")
      {
        try
        {
          input_amount = Decimal.Parse(txt_amount.Text);
        }
        catch
        {
          input_amount = 0;
        }
      }

      if (input_amount > m_max_amount)
      {
        Log.Error("btn_ok_Click. Amount to transfer (" + input_amount + ") is greater than available amount (" + m_max_amount + ").");

        frm_message.Show(Resource.String("STR_FRM_POINTS_TRANSFER_ERROR_POINTS_MAX"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Warning, this.ParentForm);

        return;
      }

      if (m_card_destiny == null)
      {
        Log.Error("btn_ok_Click. Destination account missing.");

        frm_message.Show(Resource.String("STR_FRM_POINTS_TRANSFER_ERROR_CARD_MISSING"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Warning, this.ParentForm);

        uc_card_reader1.Focus();

        return;
      }

      if (!CashierBusinessLogic.RequestPin(this, Accounts.PinRequestOperationType.GIFT_REQUEST, m_card_destiny, 0))
      {
        return;
      }

      // Generate voucher in preview mode
      PreviewVoucher(input_amount);

      txt_amount.Text = "";
      btn_ok.Enabled = true;
      btn_ok.Focus();
    } // btn_intro_Click

    //------------------------------------------------------------------------------
    // PURPOSE : Generate a preview version of the voucher to be generated.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //

    private void PreviewVoucher(Points VoucherAmount)
    {
      SqlTransaction sql_trx;
      SqlConnection sql_conn;
      String voucher_file_name;
      VoucherCardTransferPoints voucher;
      Points current_points_dest;
      Points current_points_ori;
      Points diff_points;
      Points final_points_dest;
      Points final_points_ori;

      sql_conn = null;
      sql_trx = null;

      voucher_file_name = "";
      web_browser.Navigate(voucher_file_name);

      m_voucher_amount = VoucherAmount;
      
      try
      {

        // Get Sql Connection 
        sql_conn = WSI.Common.WGDB.Connection();
        sql_trx = sql_conn.BeginTransaction();

        voucher = new VoucherCardTransferPoints(PrintMode.Preview, sql_trx);
        voucher.AddString("VOUCHER_TRANSFER_POINTS_CARD_ID_ORIGIN", m_card_origin.VisibleTrackdata());
        voucher.AddString("VOUCHER_TRANSFER_POINTS_ACCOUNT_ID_ORIGIN", m_card_origin.AccountId.ToString());
        voucher.AddString("VOUCHER_TRANSFER_POINTS_CARD_ID_DESTINY", m_card_destiny.VisibleTrackdata());
        voucher.AddString("VOUCHER_TRANSFER_POINTS_ACCOUNT_ID_DESTINY", m_card_destiny.AccountId.ToString());

        diff_points = m_voucher_amount;
        current_points_dest = 0;
        current_points_ori = 0;
        final_points_dest = 0;
        final_points_ori = 0;


        switch (m_card_origin.PlayerTracking.SubType)
        {
          // Service (restaurant)
          case 2:
            {
              current_points_dest = m_card_destiny.PlayerTracking.CurrentPointsBar;
              current_points_ori = m_card_origin.PlayerTracking.CurrentPointsBar;
              final_points_dest = m_card_destiny.PlayerTracking.CurrentPointsBar + diff_points;
              final_points_ori = m_card_origin.PlayerTracking.CurrentPointsBar - diff_points;

              break;
            }

          // Service (playable credits)
          case 3:
            {
              current_points_dest = m_card_destiny.PlayerTracking.CurrentPointsCreditNR;
              current_points_ori = m_card_origin.PlayerTracking.CurrentPointsCreditNR;
              final_points_dest = m_card_destiny.PlayerTracking.CurrentPointsCreditNR + diff_points;
              final_points_ori = m_card_origin.PlayerTracking.CurrentPointsCreditNR - diff_points;
              
              break;
            }

          // Player (CJ)
          case 1:
          // Service (expired credits)
          case 4:
          default:
            // No posible
            break;
        }

        voucher.AddString("VOUCHER_CARD_TRANSFER_POINTS_BALANCE_INITIAL", current_points_dest.ToString());
        voucher.AddString("VOUCHER_CARD_TRANSFER_POINTS_TO_ADD", diff_points.ToString());
        voucher.AddString("VOUCHER_CARD_TRANSFER_POINTS_BALANCE_FINAL", final_points_dest.ToString());

        voucher.AddString("VOUCHER_CARD_TRANSFER_POINTS_ORIGIN_BALANCE_INITIAL", current_points_ori.ToString());
        voucher.AddString("VOUCHER_CARD_TRANSFER_POINTS_ORIGIN_TO_ADD", diff_points.ToString());
        voucher.AddString("VOUCHER_CARD_TRANSFER_POINTS_ORIGIN_BALANCE_FINAL", final_points_ori.ToString());

        voucher.AddString("VOUCHER_CARD_TRANSFER_POINTS_TOTAL_AMOUNT", diff_points.ToString());
        voucher_file_name = voucher.GetFileName();

      }
      catch (Exception ex)
      {
        sql_trx.Rollback();
        Log.Exception(ex);
      }

      finally
      {
        if (sql_trx != null)
        {
          if (sql_trx.Connection != null)
          {
            sql_trx.Rollback();
          }
          sql_trx.Dispose();
          sql_trx = null;
        }

        // Close connection
        if (sql_conn != null)
        {
          sql_conn.Close();
          sql_conn = null;
        }
      }

      m_out_amount = m_voucher_amount;
      web_browser.Navigate(voucher_file_name);
      ////is_voucher_loaded = true;
    } // PreviewVoucher

    #endregion Buttons

    #region Public Methods

    /// <summary>
    /// Init and Show the form
    /// </summary>
    /// <param name="CaptionText"></param>
    /// <param name="Amount"></param>
    /// <param name="Parent"></param>
    /// <returns></returns>
    public Boolean Show(CardData CardOrigin, Control Parent)
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_points_transfer", Log.Type.Message);

      switch (CardOrigin.PlayerTracking.SubType)
      {
        // Service (restaurant)
        case 2:
          {
            lbl_available_title.Text = Resource.String("STR_FRM_POINTS_TRANSFER_BAR_POINTS_AVAILABLE") + ":";
            lbl_available.Text = CardOrigin.PlayerTracking.CurrentPointsBar.ToString();
            m_max_amount = CardOrigin.PlayerTracking.CurrentPointsBar;

            break;
          }

        // Service (playable credits)
        case 3:
          {
            lbl_available_title.Text = Resource.String("STR_FRM_POINTS_TRANSFER_CREDIT_POINTS_AVAILABLE") + ":";
            lbl_available.Text = CardOrigin.PlayerTracking.CurrentPointsCreditNR.ToString();
            m_max_amount = CardOrigin.PlayerTracking.CurrentPointsCreditNR;

            break;
          }

        // Player (CJ)
        case 1:
        // Service (expired credits)
        case 4:
        default:
          // No posible
          break;
      }

      txt_amount.Text = "";

      m_card_origin = CardOrigin;
     
      btn_ok.Enabled = false;
      
      gp_digits_box.Enabled = true;
      gp_voucher_box.Enabled = false;
      
      this.ShowDialog(Parent);

      if (Parent != null)
      {
        Parent.Focus();
      }

      return true;
    } // Show

    #endregion
    
    #region Private Methods

    /// <summary>
    /// Initialize control resources (NLS strings, images, etc)
    /// </summary>
    public void InitializeControlResources()
    {
      // - NLS Strings:
      //   - Title: External set

      //   - Labels
      gp_digits_box.Text = Resource.String("STR_FRM_POINTS_TRANSFER_INSERT_POINTS");
      gp_destiny.Text = Resource.String("STR_FRM_POINTS_TRANSFER_IDESTINY_ACCOUNT");
      lbl_amount_input_title.Text = Resource.String("STR_FRM_POINTS_TRANSFER_TITLE");

      //   - Buttons
      btn_ok.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_OK");
      btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");

      btn_intro.Text = Resource.String("STR_FRM_AMOUNT_INPUT_BTN_ENTER");

      // - Images:

    } // InitializeControlResources

    /// <summary>
    /// Check and Add digit to amount
    /// </summary>
    /// <param name="NumberStr"></param>
    private void AddNumber(String NumberStr)
    {
      txt_amount.Text = txt_amount.Text + NumberStr;
      btn_intro.Focus();
    }

    private void frm_points_transfer_Shown(object sender, EventArgs e)
    {
      this.txt_amount.Focus();
    }

    private void frm_points_transfer_KeyPress(object sender, KeyPressEventArgs e)
    {
      String aux_str;
      char c;

      c = e.KeyChar;

      if (c >= '0' && c <= '9')
      {
        aux_str = "" + c;
        AddNumber(aux_str);

        e.Handled = true;
      }

      if (c == '\r')
      {
        btn_intro_Click(null, null);

        e.Handled = true;
      }

      if (c == '\b')
      {
        btn_back_Click(null, null);

        e.Handled = true;
      }

      if (c == '.')
      {
        btn_num_dot_Click(null, null);

        e.Handled = true;
      }

      e.Handled = false;
    }

    private void splitContainer1_KeyPress(object sender, KeyPressEventArgs e)
    {
      frm_points_transfer_KeyPress(sender, e);
    }

    private void frm_points_transfer_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
    {
      char c;

      c = Convert.ToChar(e.KeyCode);
      if (c == '\r')
      {
        e.IsInputKey = true;
      }
    }

    /// <summary>
    /// Event fired when a card is swiped.
    /// </summary>
    /// <param name="TrackNumber">Number read</param>
    /// <param name="IsValid">Track read is valid</param>
    private void uc_card_reader1_OnTrackNumberReadEvent(string TrackNumber, ref Boolean IsValid)
    {
      // Load and load track number
      LoadCardByTrackNumber(TrackNumber);

    }

    private void uc_card_reader1_OnTrackNumberReadingEvent()
    {
      // TODO EnableDisableButtons(false);
    }

    /// <summary>
    /// Check for input card in order to notify user to create card.
    /// </summary>
    /// <param name="TrackNumber"></param>
    private void LoadCardByTrackNumber(String TrackNumber)
    {
      // Check for no input card
      if (TrackNumber == "")
      {
        return;
      }

      CardData card_data = new CardData();

      // Get Card data
      if (!CardData.DB_CardGetAllData(TrackNumber, card_data, false))
      {
        // TODO ClearCardData();

        Log.Error("LoadCardByTrackNumber. DB_GetCardAllData: Error reading card.");

        return;
      }

      // Check the card belong to the site.
      if (!CardData.CheckTrackdataSiteId(new ExternalTrackData(TrackNumber)))
      {
        // TODO ClearCardData();

        Log.Error("LoadCardByTrackNumber. Card: " + TrackNumber + " doesn�t belong to this site.");

        frm_message.Show(Resource.String("STR_CARD_SITE_ID_ERROR"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Warning, this.ParentForm);

        uc_card_reader1.Focus();

        return;
      }

      if (card_data.AccountId == 0)
      {
        // Some sort of errors to define
      }
      else
      {
        LoadDestinyCard(card_data);
      }
    } // LoadCardByTrackNumber


    /// <summary>
    /// Load existing card content from DB
    /// </summary>
    private void LoadDestinyCard(CardData CardDestiny)
    {
      if ( CardDestiny.PlayerTracking.SubType != 1 )
      {
        Log.Error("LoadDestinyCard. Card: " + CardDestiny.TrackData + " is not a player card.");

        frm_message.Show(Resource.String("STR_FRM_POINTS_TRANSFER_ERROR_CARD_TYPE"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Warning, this.ParentForm);

        uc_card_reader1.Focus();

        return;
      }

      m_card_destiny = CardDestiny;

      gp_destiny.Text = Resource.String("STR_FRM_POINTS_TRANSFER_IDESTINY_ACCOUNT") + ": " + CardDestiny.AccountId.ToString();
      
    }

  }

  #endregion
}