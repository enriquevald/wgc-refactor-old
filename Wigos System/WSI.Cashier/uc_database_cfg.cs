//------------------------------------------------------------------------------
// Copyright � 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_database_cfg.cs
// 
//   DESCRIPTION: Displays the current database connection configuration and 
//                also allows its configuration
//
//        AUTHOR: Armando Alva
// 
// CREATION DATE: 03-NOV-2008
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 03-NOV-2008 AAV    Initial Draft.
// 28-MAY-2013 NMR    Deleting uses of StringNoVoucherMode() funcionality
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using WSI.Common;

namespace WSI.Cashier
{
  #region "Enumerates"

  public enum DatabaseCfgMode
  {
    EDIT = 0,
    READ = 1
  }

  #endregion
  
  public partial class uc_database_cfg : UserControl
  {


    #region Members

    DatabaseCfgMode current_mode;
    public event EventHandler ValuesChanged;

    #endregion

    #region Events

    protected virtual void OnValueChanged(EventArgs e)
    {
      if (ValuesChanged != null)
      {
        ValuesChanged(this, null);
      }
    }

    #endregion

    #region Constructor

    /// <summary>
    /// Default constructor
    /// </summary>
    public uc_database_cfg()
    {
      InitializeComponent();

      this.InitControls();
    }

    #endregion

    #region Properties

    /// <summary>
    /// Gets or sets the current control mode.
    /// </summary>
    public DatabaseCfgMode Mode
    {
      get
      {
        return current_mode;
      }

      set
      {
        if (current_mode == value)
        {
          return;
        }

        current_mode = value;
        SetCurrentMode();
      }
    }

    #endregion

    #region Private Functions

    /// <summary>
    /// Activates the current mode 
    /// </summary>
    private void SetCurrentMode()
    {
      switch (Mode)
      {
        case DatabaseCfgMode.READ:
        {
          WSIKeyboard.Hide();

          btn_edit.Visible = true;
          btn_save.Visible = false;

          txt_db_primary.Enabled    = false;
          txt_db_secondary.Enabled  = false;
          txt_db_id.Enabled         = false;

          txt_db_primary.Text   = ConfigurationFile.GetSetting("DBPrincipal");
          txt_db_secondary.Text = ConfigurationFile.GetSetting("DBMirror");
          txt_db_id.Text        = ConfigurationFile.GetSetting("DBId");

        }
        break;

        case DatabaseCfgMode.EDIT:
        {
          WSIKeyboard.Show();

          btn_edit.Visible = false;
          btn_save.Visible = true;

          txt_db_primary.Enabled    = true;
          txt_db_secondary.Enabled  = true;
          txt_db_id.Enabled         = true;

          txt_db_primary.Focus();
          txt_db_primary.SelectAll();
        }
        break;
      }
    }

    /// <summary>
    /// Button Edit click actions
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btn_edit_Click(object sender, EventArgs e)
    {
      // Check if current user is an authorized user
      String error_str;
      if (Cashier.UserId != 0)
      {
        if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.OptionsDBConfig, ProfilePermissions.TypeOperation.ShowMsg, this.ParentForm, out error_str))
        {
          return;
        }
      }
      this.Mode = DatabaseCfgMode.EDIT;
      // this.OnValueChanged(null);
    }

    /// <summary>
    /// Button: Edit/Save actions.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btn_save_Click(object sender, EventArgs e)
    {
      String default_xml = "<SiteConfig>" +
                            "<DBPrincipal>" + txt_db_primary.Text.Trim() + "</DBPrincipal>" +
                            "<DBMirror>" + txt_db_secondary.Text.Trim() + "</DBMirror>" +
                            "<DBId>" + txt_db_id.Text.Trim() + "</DBId>" +
                            "</SiteConfig>";

      if (ConfigurationFile.SaveConfigFile("WSI.Cashier.cfg", default_xml) == false)
      {
        MessageBox.Show("Configuration file could not be updated. Please check file attributes.");
      }

      this.Mode = DatabaseCfgMode.READ;
      this.OnValueChanged(null);
    }


    #endregion

    #region Public Functions

    /// <summary>
    /// Initializes all of the control's components
    /// </summary>
    public void InitControls()
    {
      try
      {
        this.BackColor = WSI.Cashier.CashierStyle.Colors.m_colors_dic[CashierStyle.Colors.ENUM_COLORS.HEX_F4F6F9];
        pb_db_config.Image = Resources.ResourceImages.black_terminal;
      }
      catch
      {
      }

      InitControlResources();

      Mode = DatabaseCfgMode.READ;
    }

    public void InitControlResources()
    {
      //lbl_db_config.Text    = Resource.String("STR_DB_CONFIG");
      btn_edit.Text         = Resource.String("STR_DB_EDIT");
      btn_save.Text         = Resource.String("STR_DB_SAVE");
      lbl_primary.Text      = Resource.String("STR_DB_PRIMARY");
      lbl_secondary.Text    = Resource.String("STR_DB_SECONDARY");
      lbl_db_id.Text        = Resource.String("STR_DB_ID");
    }

    #endregion
  }
}
