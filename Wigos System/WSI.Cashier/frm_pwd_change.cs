//------------------------------------------------------------------------------
// Copyright � 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_pwd_change.cs
// 
//   DESCRIPTION: Implements the following classes:
//                1. frm_pwd_change
//
//        AUTHOR: Jes�s �ngel Blanco
// 
// CREATION DATE: 10-AUG-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 10-AGU-2012 JAB    First release.
// 27-DEC-2012 ICS    Check privacy policy and password update.
// 10-JUL-2013 DHA    Changed the length from the field 'Password' from 15 to 22 characters
// 13-AUG-2013 DRV    Added support to show automatic OSK if it's enabled or open it if the keyboard button is clicked  
// 23-MAR-2015 YNM    Fixed Bug WIG-2156: Control Access Audit: No audits in the cashier when the user changes password.
// 10-FEB-2016 DDS    Bug 9112:No pueden cambiar las credenciales del cajero pasando la tarjeta mediante el lector de tarjetas
// 16-FEB-2017 ATB    Add logging system to the shutdowns to control it
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using WSI.Common;
using WSI.Cashier.Controls;

namespace WSI.Cashier
{
  public partial class frm_pwd_change : frm_base
  {

    #region Members

    UserLogin.PasswordChange m_current_PasswordChangeMode;
    private Int32 m_user_id;
    private static frm_yesno form_yes_no;

    private string m_old_password;
    private string m_new_password;
    private string m_confirm_password;
    private string m_user_name;

    // holds the result of the process
    private bool m_pwd_change_ok;

    public const int AUDIT_CODE_USERS = 7;
    public const int AUDIT_TITLE_ID = 6500 + 6;
    public const int AUDIT_FIELD_ID = 6500 + 6;

    #endregion

    #region Constructor

    public frm_pwd_change(int UserId,
                          string UserName,
                          UserLogin.PasswordChange PasswordChangeMode)
    {

      InitializeComponent();

      m_user_id = UserId;
      m_user_name = UserName;
      m_current_PasswordChangeMode = PasswordChangeMode;
      m_pwd_change_ok = false;

      form_yes_no = new frm_yesno();
      form_yes_no.Opacity = 0.6;

      switch (PasswordChangeMode)
      {
        case UserLogin.PasswordChange.PWD_CHANGE_REQUESTED:
          this.txt_old_password.Select();
          break;
        case UserLogin.PasswordChange.PWD_CHANGE_REQUIRED:
          this.txt_old_password.Enabled = false;
          this.txt_old_password.Text = "**********";
          this.txt_new_password.Select();
          break;
      }

    }

    #endregion

    #region ButtonHandling

    private void btn_shutdown_Click(object sender, EventArgs e)
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[PROMPTED SHUTDOWN] REASON: User action (Password change)", Log.Type.Message);
      Cashier.Shutdown(true);
    } // btn_shutdown_Click

    private void btn_keys_Click(object sender, EventArgs e)
    {
      //Shows OSK
      WSIKeyboard.ForceToggle();
      txt_new_password.SelectAll();
      txt_new_password.Focus();
    } // btn_keys_Click

    private void btn_cancel_Click(object sender, EventArgs e)
    {
      this.DialogResult = DialogResult.Cancel;

      Misc.WriteLog("[FORM CLOSE] frm_pwd_change (cancel)", Log.Type.Message);
      this.Close();
    } // btn_cancel_Click

    private void btn_ok_Click(object sender, EventArgs e)
    {
      UserLogin.ChangePasswordStatus _rc;
      PasswordPolicy _pwd_policy;
      Auditor _auditor;

      m_old_password = this.txt_old_password.Text;
      m_new_password = this.txt_new_password.Text;
      m_confirm_password = this.txt_confirm_password.Text;

      _pwd_policy = new PasswordPolicy();

      m_pwd_change_ok = false;

      // Only checks old password when is requested
      if (m_current_PasswordChangeMode == UserLogin.PasswordChange.PWD_CHANGE_REQUESTED)
      {
        // Confirm that it is not empty
        if (m_old_password == "")
        {
          frm_message.Show(Resource.String("STR_USER_PASSWORD_OLD_EMPTY"),
                           Resource.String("STR_APP_GEN_MSG_ERROR"),
                           MessageBoxButtons.OK,
                           MessageBoxIcon.Error,
                           this);
          ClearFields();

          return;
        }
      }

      if (m_new_password == "")
      {
        frm_message.Show(Resource.String("STR_USER_PASSWORD_NEW_EMPTY"),
                         Resource.String("STR_APP_GEN_MSG_ERROR"),
                         MessageBoxButtons.OK,
                         MessageBoxIcon.Error,
                         this);
        ClearFields();

        return;
      }
      if (m_confirm_password == "")
      {
        frm_message.Show(Resource.String("STR_USER_PASSWORD_CONFIRM_EMPTY"),
                         Resource.String("STR_APP_GEN_MSG_ERROR"),
                         MessageBoxButtons.OK,
                         MessageBoxIcon.Error,
                         this);
        ClearFields();

        return;
      }

      // Confirm that are equal
      if (m_new_password != m_confirm_password)
      {
        frm_message.Show(Resource.String("STR_USER_PASSWORD_CONFIRM_ERROR"),
                         Resource.String("STR_APP_GEN_MSG_ERROR"),
                         MessageBoxButtons.OK,
                         MessageBoxIcon.Error,
                         this);
        ClearFields();

        return;
      } // Confirm that are different

      if (m_new_password == m_old_password)
      {
        frm_message.Show(Resource.String("STR_USER_PASSWORD_EQUALS_ERROR"),
                         Resource.String("STR_APP_GEN_MSG_ERROR"),
                         MessageBoxButtons.OK,
                         MessageBoxIcon.Error,
                         this);
        ClearFields();

        return;
      }

      try
      {
        // Only checks old password when is requested
        if (m_current_PasswordChangeMode == UserLogin.PasswordChange.PWD_CHANGE_REQUESTED)
        {
          using (DB_TRX _db_trx = new DB_TRX())
          {
            // Check current password via pkg
            if (_pwd_policy.VerifyCurrentPassword(m_user_id, m_old_password, _db_trx.SqlTransaction))
            {
              _rc = UserLogin.ChangePasswordStatus.OK;
            }
            else
            {
              _rc = UserLogin.ChangePasswordStatus.WRONG_PASSWORD;
            }

            _db_trx.Commit();
          }

          if (_rc != UserLogin.ChangePasswordStatus.OK)
          {
            // Invalid current password, the user has to enter a valid password
            frm_message.Show(Resource.String("STR_USER_PASSWORD_OLD_ERROR"),
                           Resource.String("STR_APP_GEN_MSG_ERROR"),
                           MessageBoxButtons.OK,
                           MessageBoxIcon.Error,
                           this);
            ClearFields();

            return;
          }
        }

        // Check new password via pkg
        if (_pwd_policy.CheckPasswordRules(Environment.MachineName, m_user_name, m_user_id, m_new_password))
        {
          _rc = UserLogin.ChangePasswordStatus.OK;
        }
        else
        {
          _rc = UserLogin.ChangePasswordStatus.PASSWORD_ERROR;
        }

        if (_rc != UserLogin.ChangePasswordStatus.OK)
        {
          // Invalid new password, the user has to enter a valid password
          frm_message.Show(_pwd_policy.MessagePasswordPolicyInvalid(),
                         Resource.String("STR_APP_GEN_MSG_ERROR"),
                         MessageBoxButtons.OK,
                         MessageBoxIcon.Error,
                         this);
          ClearFields();

          return;
        }

        using (DB_TRX _db_trx = new DB_TRX())
        {
          // Then updates the new password
          _rc = UserLogin.ChangeUserPassword(m_user_id, m_new_password, _db_trx.SqlTransaction);

          if (_rc == UserLogin.ChangePasswordStatus.OK)
          {
            _db_trx.Commit();
            _auditor = new Auditor(AUDIT_CODE_USERS);
            _auditor.SetName(AUDIT_TITLE_ID, Resource.String("STR_FRM_USER_LOGIN_PASS_CHANGE"),"-",m_user_name, "", "");
            _auditor.SetField(AUDIT_FIELD_ID, Resource.String("STR_FRM_USER_LOGIN_PASSWORD_2"), "", "", "", "");
            _auditor.Notify(ENUM_GUI.CASHIER, m_user_id, m_user_name, Cashier.TerminalName);
          
          }
        }

        if (_rc != UserLogin.ChangePasswordStatus.OK)
        {
          // Error updating password          
          frm_message.Show(Resource.String("STR_APP_GEN_ERROR_DATABASE_ERROR_WRITING"),
                         Resource.String("STR_APP_GEN_MSG_ERROR"),
                         MessageBoxButtons.OK,
                         MessageBoxIcon.Error,
                         this);
          ClearFields();

          return;
        }

        m_pwd_change_ok = true;

        if (m_pwd_change_ok)
        {
          frm_message.Show(Resource.String("STR_USER_PASSWORD_CHANGE_OK"), " ",
                            MessageBoxButtons.OK, MessageBoxIcon.Information, this);

          this.DialogResult = DialogResult.OK;

          Misc.WriteLog("[FORM CLOSE] frm_pwd_change (ok)", Log.Type.Message);
          this.Close();
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        frm_message.Show(Resource.String("STR_APP_GEN_ERROR_DATABASE_ERROR_WRITING"),
                         Resource.String("STR_APP_GEN_MSG_ERROR"),
                         MessageBoxButtons.OK,
                         MessageBoxIcon.Error,
                         this);
        ClearFields();
      }

    }

    #endregion

    #region Public Methods

    public DialogResult Show(Form Parent)
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_pwd_change", Log.Type.Message);

      DialogResult _dr;

      InitializeControlResources();

      txt_old_password.MaxLength = PasswordPolicy.MAX_PASSWORD_LENGTH_CONST;
      txt_new_password.MaxLength = PasswordPolicy.MAX_PASSWORD_LENGTH_CONST;
      txt_confirm_password.MaxLength = PasswordPolicy.MAX_PASSWORD_LENGTH_CONST;

      form_yes_no.Show(Parent);

      //   - Form position
      if (WindowManager.StartUpMode == WindowManager.StartUpModes.CashDesk)
      {
        this.Location = new Point(1024 / 2 - this.Width / 2, 768 / 2 - this.Height);
      }
      else
      {
        this.Location = new Point(WindowManager.GetFormCenterLocation(form_yes_no).X - (this.Width / 2)
                                 , WindowManager.GetFormCenterLocation(form_yes_no).Y - (this.Height / 2));
      }

      _dr = this.ShowDialog(form_yes_no);

      form_yes_no.Close();

      return _dr;
     
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Dispose yes_no form.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //

    new public void Dispose()
    {
      form_yes_no.Dispose();
      form_yes_no = null;

      KeyboardMessageFilter.UnregisterHandler(this.txt_confirm_password, ConfirmPasswordReceived);
      KeyboardMessageFilter.UnregisterHandler(this.txt_new_password, NewPasswordReceived);
      KeyboardMessageFilter.UnregisterHandler(this.txt_old_password, OldPasswordReceived);

      base.Dispose();
    } // Dispose

    #endregion

    #region Private Methods

    private void InitializeControlResources()
    {
      // - NLS Strings:
      //   - Title
      switch (m_current_PasswordChangeMode)
      {
        case UserLogin.PasswordChange.PWD_CHANGE_REQUESTED:
          this.FormTitle = Resource.String("STR_FRM_USER_PWD_CHANGE_REQUESTED");
          break;

        case UserLogin.PasswordChange.PWD_CHANGE_REQUIRED:
          this.FormTitle = Resource.String("STR_FRM_USER_PWD_CHANGE_REQUIRED");
          break;

      }

      //   - TextBoxs
      lbl_user.Text = m_user_name;
      txt_old_password.UseSystemPasswordChar = false;
      txt_new_password.UseSystemPasswordChar = false;
      txt_confirm_password.UseSystemPasswordChar = false;

      //   - Labels
      lbl_username.Text = Resource.String("STR_FRM_USER_PWD_USERNAME");
      lbl_old_password.Text = Resource.String("STR_FRM_USER_PWD_OLD_PASSWORD");
      lbl_new_password.Text = Resource.String("STR_FRM_USER_PWD_NEW_PASSWORD");
      lbl_confirm_password.Text = Resource.String("STR_FRM_USER_PWD_CONFIRM_PASSWORD");

      //   - Buttons
      btn_ok.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_OK");
      btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");

      //   - Buttons - Images
      btn_shutdown.Image = Resources.ResourceImages.shutdown;  //  WSI.Cashier.Images.Get32x32(Images.CashierImage.Shutdown);

      KeyboardMessageFilter.RegisterHandler(this.txt_old_password, BarcodeType.Account, OldPasswordReceived);
      

    } // InitializeControlResources

    private void ClearFields()
    {
      this.txt_new_password.Text = String.Empty;
      this.txt_confirm_password.Text = String.Empty;
      this.txt_new_password.Focus();

      if (m_current_PasswordChangeMode == UserLogin.PasswordChange.PWD_CHANGE_REQUESTED)
      {
        this.txt_old_password.Text = String.Empty;
        this.txt_old_password.Focus();
      }

    } // CleanFields
    #endregion

    #region Events

    private void txt_old_password_KeyPress(object sender, KeyPressEventArgs e)
    {
      if (e.KeyChar == Convert.ToChar(Keys.Enter))
      {
        System.Windows.Forms.SendKeys.Send("{TAB}");
      }
    }

    private void txt_new_password_KeyPress(object sender, KeyPressEventArgs e)
    {
      if (e.KeyChar == Convert.ToChar(Keys.Enter))
      {
        System.Windows.Forms.SendKeys.Send("{TAB}");
      }
    }

    private void txt_confirm_password_KeyPress(object sender, KeyPressEventArgs e)
    {
      if (e.KeyChar == Convert.ToChar(Keys.Enter))
      {
        System.Windows.Forms.SendKeys.Send("{TAB}");
      }
    }

    private void OldPasswordReceived(KbdMsgEvent e, Object data)
    {
        Barcode _barcode;
        KeyPressEventArgs _event_args;

        if ((!String.IsNullOrEmpty(this.txt_old_password.Text)))
        {
          this.NewPasswordReceived(e, data);

          return;
        }

        if (e == KbdMsgEvent.EventBarcode)
        {
            _barcode = (Barcode)data;
            _event_args = new KeyPressEventArgs('\r');
            this.txt_old_password.Text = _barcode.ExternalCode;
            KeyboardMessageFilter.UnregisterHandler(this.txt_old_password, OldPasswordReceived);
            KeyboardMessageFilter.RegisterHandler(this.txt_new_password, BarcodeType.Account, NewPasswordReceived);
            txt_old_password_KeyPress(this, _event_args);
        }
    }
    private void NewPasswordReceived(KbdMsgEvent e, Object data)
    {
        Barcode _barcode;
        KeyPressEventArgs _event_args;

        if (e == KbdMsgEvent.EventBarcode)
        {
            _barcode = (Barcode)data;
            _event_args = new KeyPressEventArgs('\r');
            this.txt_new_password.Text = _barcode.ExternalCode;
            KeyboardMessageFilter.UnregisterHandler(this.txt_new_password, NewPasswordReceived);
            KeyboardMessageFilter.RegisterHandler(this.txt_confirm_password, BarcodeType.Account, ConfirmPasswordReceived);
            txt_new_password_KeyPress(this, _event_args);
        }
    }
    private void ConfirmPasswordReceived(KbdMsgEvent e, Object data)
    {
        Barcode _barcode;
        KeyPressEventArgs _event_args;

        if (e == KbdMsgEvent.EventBarcode)
        {
            _barcode = (Barcode)data;
            _event_args = new KeyPressEventArgs('\r');
            this.txt_confirm_password.Text = _barcode.ExternalCode;
            txt_confirm_password_KeyPress(this, _event_args);
            KeyboardMessageFilter.UnregisterHandler(this.txt_confirm_password, ConfirmPasswordReceived);
            KeyboardMessageFilter.RegisterHandler(this.txt_old_password, BarcodeType.Account, OldPasswordReceived);
            this.btn_ok_Click(this, EventArgs.Empty);
        }
    }

    #endregion Events

  }
}