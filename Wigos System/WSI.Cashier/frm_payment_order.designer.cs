namespace WSI.Cashier
{
  partial class frm_payment_order
  {  
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_payment_order));
      this.timer1 = new System.Windows.Forms.Timer(this.components);
      this.label3 = new WSI.Cashier.Controls.uc_label();
      this.label4 = new WSI.Cashier.Controls.uc_label();
      this.textBox1 = new WSI.Cashier.Controls.uc_round_textbox();
      this.comboBox1 = new WSI.Cashier.Controls.uc_round_combobox();
      this.label5 = new WSI.Cashier.Controls.uc_label();
      this.label6 = new WSI.Cashier.Controls.uc_label();
      this.label7 = new WSI.Cashier.Controls.uc_label();
      this.label8 = new WSI.Cashier.Controls.uc_label();
      this.label9 = new WSI.Cashier.Controls.uc_label();
      this.textBox2 = new WSI.Cashier.Controls.uc_round_textbox();
      this.tab_client = new WSI.Cashier.Controls.uc_round_tab_control();
      this.tab_payment = new System.Windows.Forms.TabPage();
      this.lbl_check_type = new WSI.Cashier.Controls.uc_label();
      this.cb_check_types = new WSI.Cashier.Controls.uc_round_combobox();
      this.btn_num_1 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_back = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_2 = new WSI.Cashier.Controls.uc_round_button();
      this.ef_bank_check = new WSI.Cashier.Controls.uc_round_textbox();
      this.btn_num_3 = new WSI.Cashier.Controls.uc_round_button();
      this.lbl_cash = new WSI.Cashier.Controls.uc_label();
      this.btn_num_4 = new WSI.Cashier.Controls.uc_round_button();
      this.ef_cash = new WSI.Cashier.Controls.uc_round_textbox();
      this.btn_num_10 = new WSI.Cashier.Controls.uc_round_button();
      this.lbl_total = new WSI.Cashier.Controls.uc_label();
      this.btn_num_5 = new WSI.Cashier.Controls.uc_round_button();
      this.lb_total_cashable = new WSI.Cashier.Controls.uc_label();
      this.btn_num_dot = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_8 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_6 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_7 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_9 = new WSI.Cashier.Controls.uc_round_button();
      this.lbl_Bank_Check = new WSI.Cashier.Controls.uc_label();
      this.tap_values = new System.Windows.Forms.TabPage();
      this.tlp_authorizations = new System.Windows.Forms.TableLayoutPanel();
      this.lb_authorization1 = new WSI.Cashier.Controls.uc_label();
      this.lb_authorization2 = new WSI.Cashier.Controls.uc_label();
      this.ef_authorization1_title = new WSI.Cashier.CharacterTextBox();
      this.ef_authorization2_title = new WSI.Cashier.CharacterTextBox();
      this.ef_authorization1_name = new WSI.Cashier.CharacterTextBox();
      this.ef_authorization2_name = new WSI.Cashier.CharacterTextBox();
      this.uc_aply_date = new WSI.Cashier.uc_datetime();
      this.lbl_account_number = new WSI.Cashier.Controls.uc_label();
      this.lbl_bank_name = new WSI.Cashier.Controls.uc_label();
      this.lbl_bank_acount = new WSI.Cashier.Controls.uc_label();
      this.cb_bank_account = new WSI.Cashier.Controls.uc_round_combobox();
      this.lb_authorization_name = new WSI.Cashier.Controls.uc_label();
      this.lb_authorization_title = new WSI.Cashier.Controls.uc_label();
      this.Main = new System.Windows.Forms.TabPage();
      this.lb_doc = new WSI.Cashier.Controls.uc_label();
      this.tlp_account = new System.Windows.Forms.TableLayoutPanel();
      this.cb_type_doc2 = new WSI.Cashier.Controls.uc_round_combobox();
      this.cb_type_doc1 = new WSI.Cashier.Controls.uc_round_combobox();
      this.ef_holder_id_02 = new WSI.Cashier.CharacterTextBox();
      this.ef_holder_id_01 = new WSI.Cashier.CharacterTextBox();
      this.ef_name_04 = new WSI.Cashier.CharacterTextBox();
      this.ef_name_03 = new WSI.Cashier.CharacterTextBox();
      this.lbl_name_04 = new WSI.Cashier.Controls.uc_label();
      this.ef_name_02 = new WSI.Cashier.CharacterTextBox();
      this.lbl_name_03 = new WSI.Cashier.Controls.uc_label();
      this.ef_name_01 = new WSI.Cashier.CharacterTextBox();
      this.lbl_name_02 = new WSI.Cashier.Controls.uc_label();
      this.lbl_name_01 = new WSI.Cashier.Controls.uc_label();
      this.lb_doc_type = new WSI.Cashier.Controls.uc_label();
      this.lbl_msg_blink = new WSI.Cashier.Controls.uc_label();
      this.btn_keyboard = new WSI.Cashier.Controls.uc_round_button();
      this.btn_cancel = new WSI.Cashier.Controls.uc_round_button();
      this.btn_ok = new WSI.Cashier.Controls.uc_round_button();
      this.pnl_data.SuspendLayout();
      this.tab_client.SuspendLayout();
      this.tab_payment.SuspendLayout();
      this.tap_values.SuspendLayout();
      this.tlp_authorizations.SuspendLayout();
      this.Main.SuspendLayout();
      this.tlp_account.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.Controls.Add(this.tab_client);
      this.pnl_data.Controls.Add(this.lbl_msg_blink);
      this.pnl_data.Controls.Add(this.btn_keyboard);
      this.pnl_data.Controls.Add(this.btn_cancel);
      this.pnl_data.Controls.Add(this.btn_ok);
      this.pnl_data.Size = new System.Drawing.Size(798, 449);
      this.pnl_data.TabIndex = 0;
      // 
      // timer1
      // 
      this.timer1.Interval = 4000;
      this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
      // 
      // label3
      // 
      this.label3.BackColor = System.Drawing.Color.Transparent;
      this.label3.Font = new System.Drawing.Font("Open Sans Semibold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.label3.Location = new System.Drawing.Point(365, 130);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(19, 38);
      this.label3.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.label3.TabIndex = 35;
      this.label3.Text = "/";
      this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // label4
      // 
      this.label4.BackColor = System.Drawing.Color.Transparent;
      this.label4.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.label4.Location = new System.Drawing.Point(6, 77);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(144, 38);
      this.label4.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.label4.TabIndex = 34;
      this.label4.Text = "xC�digo Elector";
      this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // textBox1
      // 
      this.textBox1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.textBox1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.textBox1.BackColor = System.Drawing.Color.White;
      this.textBox1.BorderColor = System.Drawing.Color.Empty;
      this.textBox1.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.textBox1.CornerRadius = 0;
      this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.textBox1.Location = new System.Drawing.Point(156, 76);
      this.textBox1.MaxLength = 20;
      this.textBox1.Multiline = false;
      this.textBox1.Name = "textBox1";
      this.textBox1.PasswordChar = '\0';
      this.textBox1.ReadOnly = false;
      this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.textBox1.SelectedText = "";
      this.textBox1.SelectionLength = 0;
      this.textBox1.SelectionStart = 0;
      this.textBox1.Size = new System.Drawing.Size(510, 38);
      this.textBox1.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.NONE;
      this.textBox1.TabIndex = 2;
      this.textBox1.TabStop = false;
      this.textBox1.Text = "AAAAAAAAAAAAAAAAAAAAAAAAAB";
      this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.textBox1.UseSystemPasswordChar = false;
      this.textBox1.WaterMark = null;
      this.textBox1.WaterMarkColor = System.Drawing.SystemColors.ControlText;
      // 
      // comboBox1
      // 
      this.comboBox1.ArrowColor = System.Drawing.Color.Empty;
      this.comboBox1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.comboBox1.BackColor = System.Drawing.Color.White;
      this.comboBox1.BorderColor = System.Drawing.Color.White;
      this.comboBox1.CornerRadius = 5;
      this.comboBox1.DataSource = null;
      this.comboBox1.DisplayMember = "";
      this.comboBox1.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
      this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.comboBox1.DropDownWidth = 174;
      this.comboBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.comboBox1.FormattingEnabled = true;
      this.comboBox1.IsDroppedDown = false;
      this.comboBox1.ItemHeight = 21;
      this.comboBox1.Location = new System.Drawing.Point(156, 238);
      this.comboBox1.MaxDropDownItems = 8;
      this.comboBox1.Name = "comboBox1";
      this.comboBox1.OnFocusOpenListBox = true;
      this.comboBox1.SelectedIndex = -1;
      this.comboBox1.SelectedItem = null;
      this.comboBox1.SelectedValue = null;
      this.comboBox1.SelectionLength = 0;
      this.comboBox1.SelectionStart = 0;
      this.comboBox1.Size = new System.Drawing.Size(174, 50);
      this.comboBox1.Sorted = false;
      this.comboBox1.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.NONE;
      this.comboBox1.TabIndex = 7;
      this.comboBox1.TabStop = false;
      this.comboBox1.ValueMember = "";
      // 
      // label5
      // 
      this.label5.BackColor = System.Drawing.Color.Transparent;
      this.label5.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.label5.Location = new System.Drawing.Point(6, 238);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(144, 38);
      this.label5.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.label5.TabIndex = 26;
      this.label5.Text = "xEstado Civil";
      this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // label6
      // 
      this.label6.BackColor = System.Drawing.Color.Transparent;
      this.label6.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.label6.Location = new System.Drawing.Point(6, 184);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(144, 38);
      this.label6.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.label6.TabIndex = 24;
      this.label6.Text = "xGenero";
      this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // label7
      // 
      this.label7.BackColor = System.Drawing.Color.Transparent;
      this.label7.Font = new System.Drawing.Font("Open Sans Semibold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.label7.Location = new System.Drawing.Point(248, 130);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(19, 38);
      this.label7.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.label7.TabIndex = 31;
      this.label7.Text = "/";
      this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // label8
      // 
      this.label8.BackColor = System.Drawing.Color.Transparent;
      this.label8.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.label8.Location = new System.Drawing.Point(6, 131);
      this.label8.Name = "label8";
      this.label8.Size = new System.Drawing.Size(144, 38);
      this.label8.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.label8.TabIndex = 25;
      this.label8.Text = "xNacimiento";
      this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // label9
      // 
      this.label9.BackColor = System.Drawing.Color.Transparent;
      this.label9.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.label9.Location = new System.Drawing.Point(6, 23);
      this.label9.Name = "label9";
      this.label9.Size = new System.Drawing.Size(144, 38);
      this.label9.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.label9.TabIndex = 1;
      this.label9.Text = "xName";
      this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // textBox2
      // 
      this.textBox2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.textBox2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.textBox2.BackColor = System.Drawing.Color.White;
      this.textBox2.BorderColor = System.Drawing.Color.Empty;
      this.textBox2.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.textBox2.CornerRadius = 0;
      this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.textBox2.Location = new System.Drawing.Point(156, 22);
      this.textBox2.MaxLength = 50;
      this.textBox2.Multiline = false;
      this.textBox2.Name = "textBox2";
      this.textBox2.PasswordChar = '\0';
      this.textBox2.ReadOnly = false;
      this.textBox2.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.textBox2.SelectedText = "";
      this.textBox2.SelectionLength = 0;
      this.textBox2.SelectionStart = 0;
      this.textBox2.Size = new System.Drawing.Size(510, 38);
      this.textBox2.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.NONE;
      this.textBox2.TabIndex = 1;
      this.textBox2.TabStop = false;
      this.textBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.textBox2.UseSystemPasswordChar = false;
      this.textBox2.WaterMark = null;
      this.textBox2.WaterMarkColor = System.Drawing.SystemColors.ControlText;
      // 
      // tab_client
      // 
      this.tab_client.Controls.Add(this.tab_payment);
      this.tab_client.Controls.Add(this.tap_values);
      this.tab_client.Controls.Add(this.Main);
      this.tab_client.DisplayStyle = WSI.Cashier.Controls.TabStyle.Rounded;
      // 
      // 
      // 
      this.tab_client.DisplayStyleProvider.BorderColor = System.Drawing.SystemColors.ControlDark;
      this.tab_client.DisplayStyleProvider.BorderColorHot = System.Drawing.SystemColors.ControlDark;
      this.tab_client.DisplayStyleProvider.BorderColorSelected = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(157)))), ((int)(((byte)(185)))));
      this.tab_client.DisplayStyleProvider.FocusTrack = true;
      this.tab_client.DisplayStyleProvider.HotTrack = true;
      this.tab_client.DisplayStyleProvider.Opacity = 1F;
      this.tab_client.DisplayStyleProvider.Overlap = 0;
      this.tab_client.DisplayStyleProvider.Padding = new System.Drawing.Point(6, 3);
      this.tab_client.DisplayStyleProvider.Radius = 10;
      this.tab_client.DisplayStyleProvider.TabColor = System.Drawing.SystemColors.ControlDark;
      this.tab_client.DisplayStyleProvider.TabColorDisabled = System.Drawing.SystemColors.ControlDark;
      this.tab_client.DisplayStyleProvider.TabColorHeader = System.Drawing.SystemColors.ControlDark;
      this.tab_client.DisplayStyleProvider.TabColorSelected = System.Drawing.SystemColors.ControlDark;
      this.tab_client.DisplayStyleProvider.TextColor = System.Drawing.SystemColors.ControlText;
      this.tab_client.DisplayStyleProvider.TextColorDisabled = System.Drawing.SystemColors.ControlDark;
      this.tab_client.DisplayStyleProvider.TextColorSelected = System.Drawing.SystemColors.ControlText;
      this.tab_client.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.tab_client.HotTrack = true;
      this.tab_client.ItemSize = new System.Drawing.Size(100, 40);
      this.tab_client.Location = new System.Drawing.Point(15, 14);
      this.tab_client.Name = "tab_client";
      this.tab_client.SelectedIndex = 0;
      this.tab_client.Size = new System.Drawing.Size(764, 344);
      this.tab_client.TabIndex = 0;
      this.tab_client.TabStop = false;
      // 
      // tab_payment
      // 
      this.tab_payment.Controls.Add(this.lbl_check_type);
      this.tab_payment.Controls.Add(this.cb_check_types);
      this.tab_payment.Controls.Add(this.btn_num_1);
      this.tab_payment.Controls.Add(this.btn_back);
      this.tab_payment.Controls.Add(this.btn_num_2);
      this.tab_payment.Controls.Add(this.ef_bank_check);
      this.tab_payment.Controls.Add(this.btn_num_3);
      this.tab_payment.Controls.Add(this.lbl_cash);
      this.tab_payment.Controls.Add(this.btn_num_4);
      this.tab_payment.Controls.Add(this.ef_cash);
      this.tab_payment.Controls.Add(this.btn_num_10);
      this.tab_payment.Controls.Add(this.lbl_total);
      this.tab_payment.Controls.Add(this.btn_num_5);
      this.tab_payment.Controls.Add(this.lb_total_cashable);
      this.tab_payment.Controls.Add(this.btn_num_dot);
      this.tab_payment.Controls.Add(this.btn_num_8);
      this.tab_payment.Controls.Add(this.btn_num_6);
      this.tab_payment.Controls.Add(this.btn_num_7);
      this.tab_payment.Controls.Add(this.btn_num_9);
      this.tab_payment.Controls.Add(this.lbl_Bank_Check);
      this.tab_payment.Location = new System.Drawing.Point(4, 45);
      this.tab_payment.Name = "tab_payment";
      this.tab_payment.Size = new System.Drawing.Size(756, 295);
      this.tab_payment.TabIndex = 2;
      this.tab_payment.Text = "xPayment";
      this.tab_payment.UseVisualStyleBackColor = true;
      // 
      // lbl_check_type
      // 
      this.lbl_check_type.BackColor = System.Drawing.Color.Transparent;
      this.lbl_check_type.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_check_type.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_check_type.Location = new System.Drawing.Point(91, 12);
      this.lbl_check_type.Name = "lbl_check_type";
      this.lbl_check_type.Size = new System.Drawing.Size(143, 26);
      this.lbl_check_type.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_check_type.TabIndex = 0;
      this.lbl_check_type.Text = "xCheckType";
      this.lbl_check_type.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // cb_check_types
      // 
      this.cb_check_types.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_check_types.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.cb_check_types.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.cb_check_types.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.cb_check_types.CornerRadius = 5;
      this.cb_check_types.DataSource = null;
      this.cb_check_types.DisplayMember = "";
      this.cb_check_types.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
      this.cb_check_types.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cb_check_types.DropDownWidth = 288;
      this.cb_check_types.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.cb_check_types.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_check_types.FormattingEnabled = true;
      this.cb_check_types.IsDroppedDown = false;
      this.cb_check_types.ItemHeight = 40;
      this.cb_check_types.Location = new System.Drawing.Point(87, 38);
      this.cb_check_types.MaxDropDownItems = 8;
      this.cb_check_types.Name = "cb_check_types";
      this.cb_check_types.OnFocusOpenListBox = true;
      this.cb_check_types.SelectedIndex = -1;
      this.cb_check_types.SelectedItem = null;
      this.cb_check_types.SelectedValue = null;
      this.cb_check_types.SelectionLength = 0;
      this.cb_check_types.SelectionStart = 0;
      this.cb_check_types.Size = new System.Drawing.Size(288, 40);
      this.cb_check_types.Sorted = false;
      this.cb_check_types.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.BASIC;
      this.cb_check_types.TabIndex = 1;
      this.cb_check_types.TabStop = false;
      this.cb_check_types.ValueMember = "";
      // 
      // btn_num_1
      // 
      this.btn_num_1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_1.CornerRadius = 0;
      this.btn_num_1.FlatAppearance.BorderSize = 0;
      this.btn_num_1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_1.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_1.Image = null;
      this.btn_num_1.IsSelected = false;
      this.btn_num_1.Location = new System.Drawing.Point(403, 12);
      this.btn_num_1.Name = "btn_num_1";
      this.btn_num_1.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_1.SelectedColor = System.Drawing.Color.Empty;
      this.btn_num_1.Size = new System.Drawing.Size(64, 64);
      this.btn_num_1.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_1.TabIndex = 8;
      this.btn_num_1.TabStop = false;
      this.btn_num_1.Text = "1";
      this.btn_num_1.UseVisualStyleBackColor = false;
      this.btn_num_1.Click += new System.EventHandler(this.btn_num_1_Click);
      // 
      // btn_back
      // 
      this.btn_back.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_back.CornerRadius = 0;
      this.btn_back.FlatAppearance.BorderSize = 0;
      this.btn_back.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_back.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_back.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_back.Image = ((System.Drawing.Image)(resources.GetObject("btn_back.Image")));
      this.btn_back.IsSelected = false;
      this.btn_back.Location = new System.Drawing.Point(613, 12);
      this.btn_back.Name = "btn_back";
      this.btn_back.SelectedColor = System.Drawing.Color.Empty;
      this.btn_back.Size = new System.Drawing.Size(128, 64);
      this.btn_back.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_BACKSPACE;
      this.btn_back.TabIndex = 19;
      this.btn_back.TabStop = false;
      this.btn_back.UseVisualStyleBackColor = false;
      this.btn_back.Click += new System.EventHandler(this.btn_back_Click);
      // 
      // btn_num_2
      // 
      this.btn_num_2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_2.CornerRadius = 0;
      this.btn_num_2.FlatAppearance.BorderSize = 0;
      this.btn_num_2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_2.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_2.Image = null;
      this.btn_num_2.IsSelected = false;
      this.btn_num_2.Location = new System.Drawing.Point(473, 12);
      this.btn_num_2.Name = "btn_num_2";
      this.btn_num_2.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_2.SelectedColor = System.Drawing.Color.Empty;
      this.btn_num_2.Size = new System.Drawing.Size(64, 64);
      this.btn_num_2.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_2.TabIndex = 9;
      this.btn_num_2.TabStop = false;
      this.btn_num_2.Text = "2";
      this.btn_num_2.UseVisualStyleBackColor = false;
      this.btn_num_2.Click += new System.EventHandler(this.btn_num_2_Click);
      // 
      // ef_bank_check
      // 
      this.ef_bank_check.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.ef_bank_check.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.ef_bank_check.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.ef_bank_check.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.ef_bank_check.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.ef_bank_check.CornerRadius = 5;
      this.ef_bank_check.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.ef_bank_check.Location = new System.Drawing.Point(87, 104);
      this.ef_bank_check.MaxLength = 20;
      this.ef_bank_check.Multiline = false;
      this.ef_bank_check.Name = "ef_bank_check";
      this.ef_bank_check.PasswordChar = '\0';
      this.ef_bank_check.ReadOnly = false;
      this.ef_bank_check.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.ef_bank_check.SelectedText = "";
      this.ef_bank_check.SelectionLength = 0;
      this.ef_bank_check.SelectionStart = 0;
      this.ef_bank_check.Size = new System.Drawing.Size(288, 40);
      this.ef_bank_check.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.ef_bank_check.TabIndex = 3;
      this.ef_bank_check.TabStop = false;
      this.ef_bank_check.Text = "0";
      this.ef_bank_check.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      this.ef_bank_check.UseSystemPasswordChar = false;
      this.ef_bank_check.WaterMark = null;
      this.ef_bank_check.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      this.ef_bank_check.TextChanged += new System.EventHandler(this.ef_bank_check_TextChanged);
      this.ef_bank_check.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ef_bank_check_KeyPress);
      this.ef_bank_check.Enter += new System.EventHandler(this.ef_bank_check_Enter);
      // 
      // btn_num_3
      // 
      this.btn_num_3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_3.CornerRadius = 0;
      this.btn_num_3.FlatAppearance.BorderSize = 0;
      this.btn_num_3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_3.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_3.Image = null;
      this.btn_num_3.IsSelected = false;
      this.btn_num_3.Location = new System.Drawing.Point(543, 12);
      this.btn_num_3.Name = "btn_num_3";
      this.btn_num_3.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_3.SelectedColor = System.Drawing.Color.Empty;
      this.btn_num_3.Size = new System.Drawing.Size(64, 64);
      this.btn_num_3.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_3.TabIndex = 10;
      this.btn_num_3.TabStop = false;
      this.btn_num_3.Text = "3";
      this.btn_num_3.UseVisualStyleBackColor = false;
      this.btn_num_3.Click += new System.EventHandler(this.btn_num_3_Click);
      // 
      // lbl_cash
      // 
      this.lbl_cash.BackColor = System.Drawing.Color.Transparent;
      this.lbl_cash.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_cash.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_cash.Location = new System.Drawing.Point(91, 144);
      this.lbl_cash.Name = "lbl_cash";
      this.lbl_cash.Size = new System.Drawing.Size(146, 26);
      this.lbl_cash.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_cash.TabIndex = 4;
      this.lbl_cash.Text = "xCash";
      this.lbl_cash.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // btn_num_4
      // 
      this.btn_num_4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_4.CornerRadius = 0;
      this.btn_num_4.FlatAppearance.BorderSize = 0;
      this.btn_num_4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_4.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_4.Image = null;
      this.btn_num_4.IsSelected = false;
      this.btn_num_4.Location = new System.Drawing.Point(403, 82);
      this.btn_num_4.Name = "btn_num_4";
      this.btn_num_4.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_4.SelectedColor = System.Drawing.Color.Empty;
      this.btn_num_4.Size = new System.Drawing.Size(64, 64);
      this.btn_num_4.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_4.TabIndex = 11;
      this.btn_num_4.TabStop = false;
      this.btn_num_4.Text = "4";
      this.btn_num_4.UseVisualStyleBackColor = false;
      this.btn_num_4.Click += new System.EventHandler(this.btn_num_4_Click);
      // 
      // ef_cash
      // 
      this.ef_cash.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.ef_cash.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.ef_cash.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.ef_cash.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.ef_cash.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.ef_cash.CornerRadius = 5;
      this.ef_cash.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.ef_cash.Location = new System.Drawing.Point(87, 170);
      this.ef_cash.MaxLength = 20;
      this.ef_cash.Multiline = false;
      this.ef_cash.Name = "ef_cash";
      this.ef_cash.PasswordChar = '\0';
      this.ef_cash.ReadOnly = false;
      this.ef_cash.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.ef_cash.SelectedText = "";
      this.ef_cash.SelectionLength = 0;
      this.ef_cash.SelectionStart = 0;
      this.ef_cash.Size = new System.Drawing.Size(288, 40);
      this.ef_cash.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.ef_cash.TabIndex = 5;
      this.ef_cash.TabStop = false;
      this.ef_cash.Text = "0";
      this.ef_cash.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      this.ef_cash.UseSystemPasswordChar = false;
      this.ef_cash.WaterMark = null;
      this.ef_cash.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      this.ef_cash.TextChanged += new System.EventHandler(this.ef_cash_TextChanged);
      this.ef_cash.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ef_cash_KeyPress);
      this.ef_cash.Enter += new System.EventHandler(this.ef_cash_Enter);
      // 
      // btn_num_10
      // 
      this.btn_num_10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_10.CornerRadius = 0;
      this.btn_num_10.FlatAppearance.BorderSize = 0;
      this.btn_num_10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_10.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_10.Image = null;
      this.btn_num_10.IsSelected = false;
      this.btn_num_10.Location = new System.Drawing.Point(403, 222);
      this.btn_num_10.Name = "btn_num_10";
      this.btn_num_10.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_10.SelectedColor = System.Drawing.Color.Empty;
      this.btn_num_10.Size = new System.Drawing.Size(134, 64);
      this.btn_num_10.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_10.TabIndex = 17;
      this.btn_num_10.TabStop = false;
      this.btn_num_10.Text = "0";
      this.btn_num_10.UseVisualStyleBackColor = false;
      this.btn_num_10.Click += new System.EventHandler(this.btn_num_10_Click);
      // 
      // lbl_total
      // 
      this.lbl_total.BackColor = System.Drawing.Color.Transparent;
      this.lbl_total.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_total.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_total.Location = new System.Drawing.Point(91, 210);
      this.lbl_total.Name = "lbl_total";
      this.lbl_total.Size = new System.Drawing.Size(149, 26);
      this.lbl_total.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_total.TabIndex = 6;
      this.lbl_total.Text = "xTotal";
      this.lbl_total.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // btn_num_5
      // 
      this.btn_num_5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_5.CornerRadius = 0;
      this.btn_num_5.FlatAppearance.BorderSize = 0;
      this.btn_num_5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_5.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_5.Image = null;
      this.btn_num_5.IsSelected = false;
      this.btn_num_5.Location = new System.Drawing.Point(473, 82);
      this.btn_num_5.Name = "btn_num_5";
      this.btn_num_5.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_5.SelectedColor = System.Drawing.Color.Empty;
      this.btn_num_5.Size = new System.Drawing.Size(64, 64);
      this.btn_num_5.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_5.TabIndex = 12;
      this.btn_num_5.TabStop = false;
      this.btn_num_5.Text = "5";
      this.btn_num_5.UseVisualStyleBackColor = false;
      this.btn_num_5.Click += new System.EventHandler(this.btn_num_5_Click);
      // 
      // lb_total_cashable
      // 
      this.lb_total_cashable.BackColor = System.Drawing.Color.Transparent;
      this.lb_total_cashable.Font = new System.Drawing.Font("Open Sans Semibold", 32F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lb_total_cashable.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lb_total_cashable.Location = new System.Drawing.Point(87, 236);
      this.lb_total_cashable.Name = "lb_total_cashable";
      this.lb_total_cashable.Size = new System.Drawing.Size(288, 44);
      this.lb_total_cashable.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_32PX_BLACK;
      this.lb_total_cashable.TabIndex = 7;
      this.lb_total_cashable.Text = "0";
      this.lb_total_cashable.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // btn_num_dot
      // 
      this.btn_num_dot.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_dot.CornerRadius = 0;
      this.btn_num_dot.FlatAppearance.BorderSize = 0;
      this.btn_num_dot.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_dot.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_dot.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_dot.Image = null;
      this.btn_num_dot.IsSelected = false;
      this.btn_num_dot.Location = new System.Drawing.Point(543, 222);
      this.btn_num_dot.Name = "btn_num_dot";
      this.btn_num_dot.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_dot.SelectedColor = System.Drawing.Color.Empty;
      this.btn_num_dot.Size = new System.Drawing.Size(64, 64);
      this.btn_num_dot.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_dot.TabIndex = 18;
      this.btn_num_dot.TabStop = false;
      this.btn_num_dot.Text = "�";
      this.btn_num_dot.UseVisualStyleBackColor = false;
      this.btn_num_dot.Click += new System.EventHandler(this.btn_num_dot_Click);
      // 
      // btn_num_8
      // 
      this.btn_num_8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_8.CornerRadius = 0;
      this.btn_num_8.FlatAppearance.BorderSize = 0;
      this.btn_num_8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_8.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_8.Image = null;
      this.btn_num_8.IsSelected = false;
      this.btn_num_8.Location = new System.Drawing.Point(473, 152);
      this.btn_num_8.Name = "btn_num_8";
      this.btn_num_8.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_8.SelectedColor = System.Drawing.Color.Empty;
      this.btn_num_8.Size = new System.Drawing.Size(64, 64);
      this.btn_num_8.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_8.TabIndex = 15;
      this.btn_num_8.TabStop = false;
      this.btn_num_8.Text = "8";
      this.btn_num_8.UseVisualStyleBackColor = false;
      this.btn_num_8.Click += new System.EventHandler(this.btn_num_8_Click);
      // 
      // btn_num_6
      // 
      this.btn_num_6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_6.CornerRadius = 0;
      this.btn_num_6.FlatAppearance.BorderSize = 0;
      this.btn_num_6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_6.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_6.Image = null;
      this.btn_num_6.IsSelected = false;
      this.btn_num_6.Location = new System.Drawing.Point(543, 82);
      this.btn_num_6.Name = "btn_num_6";
      this.btn_num_6.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_6.SelectedColor = System.Drawing.Color.Empty;
      this.btn_num_6.Size = new System.Drawing.Size(64, 64);
      this.btn_num_6.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_6.TabIndex = 13;
      this.btn_num_6.TabStop = false;
      this.btn_num_6.Text = "6";
      this.btn_num_6.UseVisualStyleBackColor = false;
      this.btn_num_6.Click += new System.EventHandler(this.btn_num_6_Click);
      // 
      // btn_num_7
      // 
      this.btn_num_7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_7.CornerRadius = 0;
      this.btn_num_7.FlatAppearance.BorderSize = 0;
      this.btn_num_7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_7.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_7.Image = null;
      this.btn_num_7.IsSelected = false;
      this.btn_num_7.Location = new System.Drawing.Point(403, 152);
      this.btn_num_7.Name = "btn_num_7";
      this.btn_num_7.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_7.SelectedColor = System.Drawing.Color.Empty;
      this.btn_num_7.Size = new System.Drawing.Size(64, 64);
      this.btn_num_7.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_7.TabIndex = 14;
      this.btn_num_7.TabStop = false;
      this.btn_num_7.Text = "7";
      this.btn_num_7.UseVisualStyleBackColor = false;
      this.btn_num_7.Click += new System.EventHandler(this.btn_num_7_Click);
      // 
      // btn_num_9
      // 
      this.btn_num_9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_9.CornerRadius = 0;
      this.btn_num_9.FlatAppearance.BorderSize = 0;
      this.btn_num_9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_9.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_9.Image = null;
      this.btn_num_9.IsSelected = false;
      this.btn_num_9.Location = new System.Drawing.Point(543, 152);
      this.btn_num_9.Name = "btn_num_9";
      this.btn_num_9.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_9.SelectedColor = System.Drawing.Color.Empty;
      this.btn_num_9.Size = new System.Drawing.Size(64, 64);
      this.btn_num_9.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_9.TabIndex = 16;
      this.btn_num_9.TabStop = false;
      this.btn_num_9.Text = "9";
      this.btn_num_9.UseVisualStyleBackColor = false;
      this.btn_num_9.Click += new System.EventHandler(this.btn_num_9_Click);
      // 
      // lbl_Bank_Check
      // 
      this.lbl_Bank_Check.BackColor = System.Drawing.Color.Transparent;
      this.lbl_Bank_Check.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_Bank_Check.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_Bank_Check.Location = new System.Drawing.Point(91, 78);
      this.lbl_Bank_Check.Name = "lbl_Bank_Check";
      this.lbl_Bank_Check.Size = new System.Drawing.Size(143, 26);
      this.lbl_Bank_Check.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_Bank_Check.TabIndex = 2;
      this.lbl_Bank_Check.Text = "xCheck";
      this.lbl_Bank_Check.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // tap_values
      // 
      this.tap_values.Controls.Add(this.tlp_authorizations);
      this.tap_values.Controls.Add(this.uc_aply_date);
      this.tap_values.Controls.Add(this.lbl_account_number);
      this.tap_values.Controls.Add(this.lbl_bank_name);
      this.tap_values.Controls.Add(this.lbl_bank_acount);
      this.tap_values.Controls.Add(this.cb_bank_account);
      this.tap_values.Controls.Add(this.lb_authorization_name);
      this.tap_values.Controls.Add(this.lb_authorization_title);
      this.tap_values.Location = new System.Drawing.Point(4, 45);
      this.tap_values.Name = "tap_values";
      this.tap_values.Size = new System.Drawing.Size(756, 295);
      this.tap_values.TabIndex = 3;
      this.tap_values.Text = "xValues";
      this.tap_values.UseVisualStyleBackColor = true;
      // 
      // tlp_authorizations
      // 
      this.tlp_authorizations.ColumnCount = 3;
      this.tlp_authorizations.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
      this.tlp_authorizations.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
      this.tlp_authorizations.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
      this.tlp_authorizations.Controls.Add(this.lb_authorization1, 0, 0);
      this.tlp_authorizations.Controls.Add(this.lb_authorization2, 0, 1);
      this.tlp_authorizations.Controls.Add(this.ef_authorization1_title, 1, 0);
      this.tlp_authorizations.Controls.Add(this.ef_authorization2_title, 1, 1);
      this.tlp_authorizations.Controls.Add(this.ef_authorization1_name, 2, 0);
      this.tlp_authorizations.Controls.Add(this.ef_authorization2_name, 2, 1);
      this.tlp_authorizations.Location = new System.Drawing.Point(13, 192);
      this.tlp_authorizations.Name = "tlp_authorizations";
      this.tlp_authorizations.RowCount = 2;
      this.tlp_authorizations.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_authorizations.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_authorizations.Size = new System.Drawing.Size(738, 72);
      this.tlp_authorizations.TabIndex = 13;
      // 
      // lb_authorization1
      // 
      this.lb_authorization1.BackColor = System.Drawing.Color.Transparent;
      this.lb_authorization1.Cursor = System.Windows.Forms.Cursors.Default;
      this.lb_authorization1.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lb_authorization1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lb_authorization1.Location = new System.Drawing.Point(3, 0);
      this.lb_authorization1.Name = "lb_authorization1";
      this.lb_authorization1.Size = new System.Drawing.Size(128, 26);
      this.lb_authorization1.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lb_authorization1.TabIndex = 7;
      this.lb_authorization1.Text = "xAuthorization 1";
      this.lb_authorization1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lb_authorization2
      // 
      this.lb_authorization2.BackColor = System.Drawing.Color.Transparent;
      this.lb_authorization2.Cursor = System.Windows.Forms.Cursors.Default;
      this.lb_authorization2.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lb_authorization2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lb_authorization2.Location = new System.Drawing.Point(3, 35);
      this.lb_authorization2.Name = "lb_authorization2";
      this.lb_authorization2.Size = new System.Drawing.Size(128, 26);
      this.lb_authorization2.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lb_authorization2.TabIndex = 10;
      this.lb_authorization2.Text = "xAuthorization 2";
      this.lb_authorization2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // ef_authorization1_title
      // 
      this.ef_authorization1_title.AllowSpace = true;
      this.ef_authorization1_title.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.ef_authorization1_title.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.ef_authorization1_title.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.ef_authorization1_title.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.ef_authorization1_title.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.ef_authorization1_title.CornerRadius = 5;
      this.ef_authorization1_title.Font = new System.Drawing.Font("Open Sans Semibold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.ef_authorization1_title.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.ExtendedMode;
      this.ef_authorization1_title.Location = new System.Drawing.Point(137, 3);
      this.ef_authorization1_title.MaxLength = 50;
      this.ef_authorization1_title.Multiline = false;
      this.ef_authorization1_title.Name = "ef_authorization1_title";
      this.ef_authorization1_title.PasswordChar = '\0';
      this.ef_authorization1_title.ReadOnly = false;
      this.ef_authorization1_title.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.ef_authorization1_title.SelectedText = "";
      this.ef_authorization1_title.SelectionLength = 0;
      this.ef_authorization1_title.SelectionStart = 0;
      this.ef_authorization1_title.Size = new System.Drawing.Size(230, 29);
      this.ef_authorization1_title.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC_CUSTOM;
      this.ef_authorization1_title.TabIndex = 8;
      this.ef_authorization1_title.TabStop = false;
      this.ef_authorization1_title.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.ef_authorization1_title.UseSystemPasswordChar = false;
      this.ef_authorization1_title.WaterMark = null;
      this.ef_authorization1_title.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // ef_authorization2_title
      // 
      this.ef_authorization2_title.AllowSpace = true;
      this.ef_authorization2_title.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.ef_authorization2_title.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.ef_authorization2_title.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.ef_authorization2_title.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.ef_authorization2_title.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.ef_authorization2_title.CornerRadius = 5;
      this.ef_authorization2_title.Font = new System.Drawing.Font("Open Sans Semibold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.ef_authorization2_title.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.ExtendedMode;
      this.ef_authorization2_title.Location = new System.Drawing.Point(137, 38);
      this.ef_authorization2_title.MaxLength = 50;
      this.ef_authorization2_title.Multiline = false;
      this.ef_authorization2_title.Name = "ef_authorization2_title";
      this.ef_authorization2_title.PasswordChar = '\0';
      this.ef_authorization2_title.ReadOnly = false;
      this.ef_authorization2_title.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.ef_authorization2_title.SelectedText = "";
      this.ef_authorization2_title.SelectionLength = 0;
      this.ef_authorization2_title.SelectionStart = 0;
      this.ef_authorization2_title.Size = new System.Drawing.Size(230, 29);
      this.ef_authorization2_title.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC_CUSTOM;
      this.ef_authorization2_title.TabIndex = 11;
      this.ef_authorization2_title.TabStop = false;
      this.ef_authorization2_title.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.ef_authorization2_title.UseSystemPasswordChar = false;
      this.ef_authorization2_title.WaterMark = null;
      this.ef_authorization2_title.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // ef_authorization1_name
      // 
      this.ef_authorization1_name.AllowSpace = true;
      this.ef_authorization1_name.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.ef_authorization1_name.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.ef_authorization1_name.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.ef_authorization1_name.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.ef_authorization1_name.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.ef_authorization1_name.CornerRadius = 5;
      this.ef_authorization1_name.Font = new System.Drawing.Font("Open Sans Semibold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.ef_authorization1_name.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.BaseMode;
      this.ef_authorization1_name.Location = new System.Drawing.Point(373, 3);
      this.ef_authorization1_name.MaxLength = 50;
      this.ef_authorization1_name.Multiline = false;
      this.ef_authorization1_name.Name = "ef_authorization1_name";
      this.ef_authorization1_name.PasswordChar = '\0';
      this.ef_authorization1_name.ReadOnly = false;
      this.ef_authorization1_name.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.ef_authorization1_name.SelectedText = "";
      this.ef_authorization1_name.SelectionLength = 0;
      this.ef_authorization1_name.SelectionStart = 0;
      this.ef_authorization1_name.Size = new System.Drawing.Size(358, 29);
      this.ef_authorization1_name.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC_CUSTOM;
      this.ef_authorization1_name.TabIndex = 9;
      this.ef_authorization1_name.TabStop = false;
      this.ef_authorization1_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.ef_authorization1_name.UseSystemPasswordChar = false;
      this.ef_authorization1_name.WaterMark = null;
      this.ef_authorization1_name.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // ef_authorization2_name
      // 
      this.ef_authorization2_name.AllowSpace = true;
      this.ef_authorization2_name.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.ef_authorization2_name.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.ef_authorization2_name.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.ef_authorization2_name.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.ef_authorization2_name.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.ef_authorization2_name.CornerRadius = 5;
      this.ef_authorization2_name.Font = new System.Drawing.Font("Open Sans Semibold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.ef_authorization2_name.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.BaseMode;
      this.ef_authorization2_name.Location = new System.Drawing.Point(373, 38);
      this.ef_authorization2_name.MaxLength = 50;
      this.ef_authorization2_name.Multiline = false;
      this.ef_authorization2_name.Name = "ef_authorization2_name";
      this.ef_authorization2_name.PasswordChar = '\0';
      this.ef_authorization2_name.ReadOnly = false;
      this.ef_authorization2_name.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.ef_authorization2_name.SelectedText = "";
      this.ef_authorization2_name.SelectionLength = 0;
      this.ef_authorization2_name.SelectionStart = 0;
      this.ef_authorization2_name.Size = new System.Drawing.Size(358, 29);
      this.ef_authorization2_name.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC_CUSTOM;
      this.ef_authorization2_name.TabIndex = 12;
      this.ef_authorization2_name.TabStop = false;
      this.ef_authorization2_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.ef_authorization2_name.UseSystemPasswordChar = false;
      this.ef_authorization2_name.WaterMark = null;
      this.ef_authorization2_name.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // uc_aply_date
      // 
      this.uc_aply_date.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
      this.uc_aply_date.BackColor = System.Drawing.Color.Transparent;
      this.uc_aply_date.DateText = "xFecha Aplicaci�n";
      this.uc_aply_date.DateTextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
      this.uc_aply_date.DateTextWidth = 242;
      this.uc_aply_date.DateValue = new System.DateTime(2014, 7, 9, 0, 0, 0, 0);
      this.uc_aply_date.FormatFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
      this.uc_aply_date.FormatWidth = 141;
      this.uc_aply_date.Invalid = false;
      this.uc_aply_date.Location = new System.Drawing.Point(13, 107);
      this.uc_aply_date.Name = "uc_aply_date";
      this.uc_aply_date.Size = new System.Drawing.Size(572, 44);
      this.uc_aply_date.TabIndex = 4;
      this.uc_aply_date.ValuesFont = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold);
      // 
      // lbl_account_number
      // 
      this.lbl_account_number.BackColor = System.Drawing.Color.Transparent;
      this.lbl_account_number.Cursor = System.Windows.Forms.Cursors.Default;
      this.lbl_account_number.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_account_number.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_account_number.Location = new System.Drawing.Point(237, 74);
      this.lbl_account_number.Name = "lbl_account_number";
      this.lbl_account_number.Size = new System.Drawing.Size(503, 23);
      this.lbl_account_number.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_account_number.TabIndex = 3;
      this.lbl_account_number.Text = "xAccount Number";
      this.lbl_account_number.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_bank_name
      // 
      this.lbl_bank_name.BackColor = System.Drawing.Color.Transparent;
      this.lbl_bank_name.Cursor = System.Windows.Forms.Cursors.Default;
      this.lbl_bank_name.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_bank_name.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_bank_name.Location = new System.Drawing.Point(237, 52);
      this.lbl_bank_name.Name = "lbl_bank_name";
      this.lbl_bank_name.Size = new System.Drawing.Size(503, 23);
      this.lbl_bank_name.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_bank_name.TabIndex = 2;
      this.lbl_bank_name.Text = "xBank Name";
      this.lbl_bank_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_bank_acount
      // 
      this.lbl_bank_acount.BackColor = System.Drawing.Color.Transparent;
      this.lbl_bank_acount.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_bank_acount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_bank_acount.Location = new System.Drawing.Point(8, 17);
      this.lbl_bank_acount.Name = "lbl_bank_acount";
      this.lbl_bank_acount.Size = new System.Drawing.Size(222, 25);
      this.lbl_bank_acount.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_bank_acount.TabIndex = 0;
      this.lbl_bank_acount.Text = "xCuenta de Cargo";
      this.lbl_bank_acount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // cb_bank_account
      // 
      this.cb_bank_account.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_bank_account.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.cb_bank_account.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.cb_bank_account.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.cb_bank_account.CornerRadius = 5;
      this.cb_bank_account.DataSource = null;
      this.cb_bank_account.DisplayMember = "";
      this.cb_bank_account.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
      this.cb_bank_account.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cb_bank_account.DropDownWidth = 390;
      this.cb_bank_account.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.cb_bank_account.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_bank_account.FormattingEnabled = true;
      this.cb_bank_account.IsDroppedDown = false;
      this.cb_bank_account.ItemHeight = 40;
      this.cb_bank_account.Location = new System.Drawing.Point(239, 10);
      this.cb_bank_account.MaxDropDownItems = 8;
      this.cb_bank_account.Name = "cb_bank_account";
      this.cb_bank_account.OnFocusOpenListBox = true;
      this.cb_bank_account.SelectedIndex = -1;
      this.cb_bank_account.SelectedItem = null;
      this.cb_bank_account.SelectedValue = null;
      this.cb_bank_account.SelectionLength = 0;
      this.cb_bank_account.SelectionStart = 0;
      this.cb_bank_account.Size = new System.Drawing.Size(390, 40);
      this.cb_bank_account.Sorted = false;
      this.cb_bank_account.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.BASIC;
      this.cb_bank_account.TabIndex = 1;
      this.cb_bank_account.TabStop = false;
      this.cb_bank_account.ValueMember = "";
      this.cb_bank_account.SelectedIndexChanged += new System.EventHandler(this.cb_bank_account_SelectedIndexChanged);
      // 
      // lb_authorization_name
      // 
      this.lb_authorization_name.BackColor = System.Drawing.Color.Transparent;
      this.lb_authorization_name.Cursor = System.Windows.Forms.Cursors.Default;
      this.lb_authorization_name.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lb_authorization_name.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lb_authorization_name.Location = new System.Drawing.Point(384, 163);
      this.lb_authorization_name.Name = "lb_authorization_name";
      this.lb_authorization_name.Size = new System.Drawing.Size(358, 26);
      this.lb_authorization_name.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lb_authorization_name.TabIndex = 6;
      this.lb_authorization_name.Text = "xName";
      this.lb_authorization_name.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lb_authorization_title
      // 
      this.lb_authorization_title.BackColor = System.Drawing.Color.Transparent;
      this.lb_authorization_title.Cursor = System.Windows.Forms.Cursors.Default;
      this.lb_authorization_title.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lb_authorization_title.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lb_authorization_title.Location = new System.Drawing.Point(149, 163);
      this.lb_authorization_title.Name = "lb_authorization_title";
      this.lb_authorization_title.Size = new System.Drawing.Size(230, 26);
      this.lb_authorization_title.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lb_authorization_title.TabIndex = 5;
      this.lb_authorization_title.Text = "xTitle";
      this.lb_authorization_title.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // Main
      // 
      this.Main.Controls.Add(this.lb_doc);
      this.Main.Controls.Add(this.tlp_account);
      this.Main.Controls.Add(this.lb_doc_type);
      this.Main.Location = new System.Drawing.Point(4, 45);
      this.Main.Name = "Main";
      this.Main.Padding = new System.Windows.Forms.Padding(3);
      this.Main.Size = new System.Drawing.Size(756, 295);
      this.Main.TabIndex = 0;
      this.Main.Text = "xClient";
      this.Main.UseVisualStyleBackColor = true;
      // 
      // lb_doc
      // 
      this.lb_doc.BackColor = System.Drawing.Color.Transparent;
      this.lb_doc.Cursor = System.Windows.Forms.Cursors.Default;
      this.lb_doc.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lb_doc.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lb_doc.Location = new System.Drawing.Point(278, 1);
      this.lb_doc.Name = "lb_doc";
      this.lb_doc.Size = new System.Drawing.Size(186, 25);
      this.lb_doc.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lb_doc.TabIndex = 10;
      this.lb_doc.Text = "xDocumento";
      this.lb_doc.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // tlp_account
      // 
      this.tlp_account.ColumnCount = 2;
      this.tlp_account.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 36F));
      this.tlp_account.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 64F));
      this.tlp_account.Controls.Add(this.cb_type_doc2, 0, 1);
      this.tlp_account.Controls.Add(this.cb_type_doc1, 0, 0);
      this.tlp_account.Controls.Add(this.ef_holder_id_02, 1, 1);
      this.tlp_account.Controls.Add(this.ef_holder_id_01, 1, 0);
      this.tlp_account.Controls.Add(this.ef_name_04, 1, 6);
      this.tlp_account.Controls.Add(this.ef_name_03, 1, 5);
      this.tlp_account.Controls.Add(this.lbl_name_04, 0, 6);
      this.tlp_account.Controls.Add(this.ef_name_02, 1, 4);
      this.tlp_account.Controls.Add(this.lbl_name_03, 0, 5);
      this.tlp_account.Controls.Add(this.ef_name_01, 1, 3);
      this.tlp_account.Controls.Add(this.lbl_name_02, 0, 4);
      this.tlp_account.Controls.Add(this.lbl_name_01, 0, 3);
      this.tlp_account.Location = new System.Drawing.Point(6, 24);
      this.tlp_account.Name = "tlp_account";
      this.tlp_account.RowCount = 7;
      this.tlp_account.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_account.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_account.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
      this.tlp_account.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_account.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_account.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_account.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_account.Size = new System.Drawing.Size(732, 267);
      this.tlp_account.TabIndex = 11;
      // 
      // cb_type_doc2
      // 
      this.cb_type_doc2.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_type_doc2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.cb_type_doc2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.cb_type_doc2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.cb_type_doc2.CornerRadius = 5;
      this.cb_type_doc2.DataSource = null;
      this.cb_type_doc2.DisplayMember = "";
      this.cb_type_doc2.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
      this.cb_type_doc2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cb_type_doc2.DropDownWidth = 257;
      this.cb_type_doc2.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.cb_type_doc2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_type_doc2.FormattingEnabled = true;
      this.cb_type_doc2.IsDroppedDown = false;
      this.cb_type_doc2.ItemHeight = 40;
      this.cb_type_doc2.Location = new System.Drawing.Point(3, 49);
      this.cb_type_doc2.MaxDropDownItems = 8;
      this.cb_type_doc2.Name = "cb_type_doc2";
      this.cb_type_doc2.OnFocusOpenListBox = true;
      this.cb_type_doc2.SelectedIndex = -1;
      this.cb_type_doc2.SelectedItem = null;
      this.cb_type_doc2.SelectedValue = null;
      this.cb_type_doc2.SelectionLength = 0;
      this.cb_type_doc2.SelectionStart = 0;
      this.cb_type_doc2.Size = new System.Drawing.Size(257, 40);
      this.cb_type_doc2.Sorted = false;
      this.cb_type_doc2.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.BASIC;
      this.cb_type_doc2.TabIndex = 3;
      this.cb_type_doc2.TabStop = false;
      this.cb_type_doc2.ValueMember = "";
      // 
      // cb_type_doc1
      // 
      this.cb_type_doc1.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_type_doc1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.cb_type_doc1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.cb_type_doc1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.cb_type_doc1.CornerRadius = 5;
      this.cb_type_doc1.DataSource = null;
      this.cb_type_doc1.DisplayMember = "";
      this.cb_type_doc1.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
      this.cb_type_doc1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cb_type_doc1.DropDownWidth = 257;
      this.cb_type_doc1.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.cb_type_doc1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_type_doc1.FormattingEnabled = true;
      this.cb_type_doc1.IsDroppedDown = false;
      this.cb_type_doc1.ItemHeight = 40;
      this.cb_type_doc1.Location = new System.Drawing.Point(3, 3);
      this.cb_type_doc1.MaxDropDownItems = 8;
      this.cb_type_doc1.Name = "cb_type_doc1";
      this.cb_type_doc1.OnFocusOpenListBox = true;
      this.cb_type_doc1.SelectedIndex = -1;
      this.cb_type_doc1.SelectedItem = null;
      this.cb_type_doc1.SelectedValue = null;
      this.cb_type_doc1.SelectionLength = 0;
      this.cb_type_doc1.SelectionStart = 0;
      this.cb_type_doc1.Size = new System.Drawing.Size(257, 40);
      this.cb_type_doc1.Sorted = false;
      this.cb_type_doc1.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.BASIC;
      this.cb_type_doc1.TabIndex = 1;
      this.cb_type_doc1.TabStop = false;
      this.cb_type_doc1.ValueMember = "";
      // 
      // ef_holder_id_02
      // 
      this.ef_holder_id_02.AllowSpace = true;
      this.ef_holder_id_02.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.ef_holder_id_02.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.ef_holder_id_02.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.ef_holder_id_02.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.ef_holder_id_02.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.ef_holder_id_02.CornerRadius = 5;
      this.ef_holder_id_02.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.ef_holder_id_02.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.ExtendedMode;
      this.ef_holder_id_02.Location = new System.Drawing.Point(266, 49);
      this.ef_holder_id_02.MaxLength = 20;
      this.ef_holder_id_02.Multiline = false;
      this.ef_holder_id_02.Name = "ef_holder_id_02";
      this.ef_holder_id_02.PasswordChar = '\0';
      this.ef_holder_id_02.ReadOnly = false;
      this.ef_holder_id_02.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.ef_holder_id_02.SelectedText = "";
      this.ef_holder_id_02.SelectionLength = 0;
      this.ef_holder_id_02.SelectionStart = 0;
      this.ef_holder_id_02.Size = new System.Drawing.Size(242, 40);
      this.ef_holder_id_02.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.ef_holder_id_02.TabIndex = 4;
      this.ef_holder_id_02.TabStop = false;
      this.ef_holder_id_02.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.ef_holder_id_02.UseSystemPasswordChar = false;
      this.ef_holder_id_02.WaterMark = null;
      this.ef_holder_id_02.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // ef_holder_id_01
      // 
      this.ef_holder_id_01.AllowSpace = true;
      this.ef_holder_id_01.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.ef_holder_id_01.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.ef_holder_id_01.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.ef_holder_id_01.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.ef_holder_id_01.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.ef_holder_id_01.CornerRadius = 5;
      this.ef_holder_id_01.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.ef_holder_id_01.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.ExtendedMode;
      this.ef_holder_id_01.Location = new System.Drawing.Point(266, 3);
      this.ef_holder_id_01.MaxLength = 20;
      this.ef_holder_id_01.Multiline = false;
      this.ef_holder_id_01.Name = "ef_holder_id_01";
      this.ef_holder_id_01.PasswordChar = '\0';
      this.ef_holder_id_01.ReadOnly = false;
      this.ef_holder_id_01.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.ef_holder_id_01.SelectedText = "";
      this.ef_holder_id_01.SelectionLength = 0;
      this.ef_holder_id_01.SelectionStart = 0;
      this.ef_holder_id_01.Size = new System.Drawing.Size(242, 40);
      this.ef_holder_id_01.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.ef_holder_id_01.TabIndex = 2;
      this.ef_holder_id_01.TabStop = false;
      this.ef_holder_id_01.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.ef_holder_id_01.UseSystemPasswordChar = false;
      this.ef_holder_id_01.WaterMark = null;
      this.ef_holder_id_01.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // ef_name_04
      // 
      this.ef_name_04.AllowSpace = true;
      this.ef_name_04.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.ef_name_04.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.ef_name_04.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.ef_name_04.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.ef_name_04.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.ef_name_04.CornerRadius = 5;
      this.ef_name_04.Font = new System.Drawing.Font("Open Sans Semibold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.ef_name_04.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.BaseMode;
      this.ef_name_04.Location = new System.Drawing.Point(266, 235);
      this.ef_name_04.MaxLength = 50;
      this.ef_name_04.Multiline = false;
      this.ef_name_04.Name = "ef_name_04";
      this.ef_name_04.PasswordChar = '\0';
      this.ef_name_04.ReadOnly = false;
      this.ef_name_04.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.ef_name_04.SelectedText = "";
      this.ef_name_04.SelectionLength = 0;
      this.ef_name_04.SelectionStart = 0;
      this.ef_name_04.Size = new System.Drawing.Size(463, 29);
      this.ef_name_04.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC_CUSTOM;
      this.ef_name_04.TabIndex = 8;
      this.ef_name_04.TabStop = false;
      this.ef_name_04.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.ef_name_04.UseSystemPasswordChar = false;
      this.ef_name_04.WaterMark = null;
      this.ef_name_04.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // ef_name_03
      // 
      this.ef_name_03.AllowSpace = true;
      this.ef_name_03.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.ef_name_03.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.ef_name_03.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.ef_name_03.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.ef_name_03.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.ef_name_03.CornerRadius = 5;
      this.ef_name_03.Font = new System.Drawing.Font("Open Sans Semibold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.ef_name_03.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.BaseMode;
      this.ef_name_03.Location = new System.Drawing.Point(266, 200);
      this.ef_name_03.MaxLength = 50;
      this.ef_name_03.Multiline = false;
      this.ef_name_03.Name = "ef_name_03";
      this.ef_name_03.PasswordChar = '\0';
      this.ef_name_03.ReadOnly = false;
      this.ef_name_03.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.ef_name_03.SelectedText = "";
      this.ef_name_03.SelectionLength = 0;
      this.ef_name_03.SelectionStart = 0;
      this.ef_name_03.Size = new System.Drawing.Size(463, 29);
      this.ef_name_03.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC_CUSTOM;
      this.ef_name_03.TabIndex = 7;
      this.ef_name_03.TabStop = false;
      this.ef_name_03.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.ef_name_03.UseSystemPasswordChar = false;
      this.ef_name_03.WaterMark = null;
      this.ef_name_03.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // lbl_name_04
      // 
      this.lbl_name_04.BackColor = System.Drawing.Color.Transparent;
      this.lbl_name_04.Cursor = System.Windows.Forms.Cursors.Default;
      this.lbl_name_04.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_name_04.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_name_04.Location = new System.Drawing.Point(3, 232);
      this.lbl_name_04.Name = "lbl_name_04";
      this.lbl_name_04.Size = new System.Drawing.Size(257, 26);
      this.lbl_name_04.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_name_04.TabIndex = 8;
      this.lbl_name_04.Text = "xHolderName04";
      this.lbl_name_04.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // ef_name_02
      // 
      this.ef_name_02.AllowSpace = true;
      this.ef_name_02.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.ef_name_02.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.ef_name_02.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.ef_name_02.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.ef_name_02.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.ef_name_02.CornerRadius = 5;
      this.ef_name_02.Font = new System.Drawing.Font("Open Sans Semibold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.ef_name_02.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.BaseMode;
      this.ef_name_02.Location = new System.Drawing.Point(266, 165);
      this.ef_name_02.MaxLength = 50;
      this.ef_name_02.Multiline = false;
      this.ef_name_02.Name = "ef_name_02";
      this.ef_name_02.PasswordChar = '\0';
      this.ef_name_02.ReadOnly = false;
      this.ef_name_02.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.ef_name_02.SelectedText = "";
      this.ef_name_02.SelectionLength = 0;
      this.ef_name_02.SelectionStart = 0;
      this.ef_name_02.Size = new System.Drawing.Size(463, 29);
      this.ef_name_02.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC_CUSTOM;
      this.ef_name_02.TabIndex = 6;
      this.ef_name_02.TabStop = false;
      this.ef_name_02.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.ef_name_02.UseSystemPasswordChar = false;
      this.ef_name_02.WaterMark = null;
      this.ef_name_02.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // lbl_name_03
      // 
      this.lbl_name_03.BackColor = System.Drawing.Color.Transparent;
      this.lbl_name_03.Cursor = System.Windows.Forms.Cursors.Default;
      this.lbl_name_03.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_name_03.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_name_03.Location = new System.Drawing.Point(3, 197);
      this.lbl_name_03.Name = "lbl_name_03";
      this.lbl_name_03.Size = new System.Drawing.Size(257, 26);
      this.lbl_name_03.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_name_03.TabIndex = 8;
      this.lbl_name_03.Text = "xHolderName03";
      this.lbl_name_03.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // ef_name_01
      // 
      this.ef_name_01.AllowSpace = true;
      this.ef_name_01.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.ef_name_01.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.ef_name_01.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.ef_name_01.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.ef_name_01.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.ef_name_01.CornerRadius = 5;
      this.ef_name_01.Font = new System.Drawing.Font("Open Sans Semibold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.ef_name_01.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.BaseMode;
      this.ef_name_01.Location = new System.Drawing.Point(266, 130);
      this.ef_name_01.MaxLength = 50;
      this.ef_name_01.Multiline = false;
      this.ef_name_01.Name = "ef_name_01";
      this.ef_name_01.PasswordChar = '\0';
      this.ef_name_01.ReadOnly = false;
      this.ef_name_01.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.ef_name_01.SelectedText = "";
      this.ef_name_01.SelectionLength = 0;
      this.ef_name_01.SelectionStart = 0;
      this.ef_name_01.Size = new System.Drawing.Size(463, 29);
      this.ef_name_01.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC_CUSTOM;
      this.ef_name_01.TabIndex = 5;
      this.ef_name_01.TabStop = false;
      this.ef_name_01.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.ef_name_01.UseSystemPasswordChar = false;
      this.ef_name_01.WaterMark = null;
      this.ef_name_01.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // lbl_name_02
      // 
      this.lbl_name_02.BackColor = System.Drawing.Color.Transparent;
      this.lbl_name_02.Cursor = System.Windows.Forms.Cursors.Default;
      this.lbl_name_02.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_name_02.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_name_02.Location = new System.Drawing.Point(3, 162);
      this.lbl_name_02.Name = "lbl_name_02";
      this.lbl_name_02.Size = new System.Drawing.Size(257, 26);
      this.lbl_name_02.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_name_02.TabIndex = 8;
      this.lbl_name_02.Text = "xHolderName02";
      this.lbl_name_02.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_name_01
      // 
      this.lbl_name_01.BackColor = System.Drawing.Color.Transparent;
      this.lbl_name_01.Cursor = System.Windows.Forms.Cursors.Default;
      this.lbl_name_01.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_name_01.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_name_01.Location = new System.Drawing.Point(3, 127);
      this.lbl_name_01.Name = "lbl_name_01";
      this.lbl_name_01.Size = new System.Drawing.Size(257, 26);
      this.lbl_name_01.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_name_01.TabIndex = 8;
      this.lbl_name_01.Text = "xHolderName01";
      this.lbl_name_01.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lb_doc_type
      // 
      this.lb_doc_type.BackColor = System.Drawing.Color.Transparent;
      this.lb_doc_type.Cursor = System.Windows.Forms.Cursors.Default;
      this.lb_doc_type.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lb_doc_type.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lb_doc_type.Location = new System.Drawing.Point(9, 0);
      this.lb_doc_type.Name = "lb_doc_type";
      this.lb_doc_type.Size = new System.Drawing.Size(257, 26);
      this.lb_doc_type.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lb_doc_type.TabIndex = 9;
      this.lb_doc_type.Text = "xTipo de Documento";
      this.lb_doc_type.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lbl_msg_blink
      // 
      this.lbl_msg_blink.BackColor = System.Drawing.Color.Transparent;
      this.lbl_msg_blink.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_msg_blink.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.lbl_msg_blink.Location = new System.Drawing.Point(102, 370);
      this.lbl_msg_blink.Name = "lbl_msg_blink";
      this.lbl_msg_blink.Size = new System.Drawing.Size(333, 60);
      this.lbl_msg_blink.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_RED;
      this.lbl_msg_blink.TabIndex = 2;
      this.lbl_msg_blink.Text = "xMESSAGE LINE";
      this.lbl_msg_blink.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.lbl_msg_blink.UseMnemonic = false;
      this.lbl_msg_blink.Visible = false;
      // 
      // btn_keyboard
      // 
      this.btn_keyboard.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(47)))), ((int)(((byte)(50)))));
      this.btn_keyboard.FlatAppearance.BorderSize = 0;
      this.btn_keyboard.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_keyboard.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_keyboard.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_keyboard.Image = ((System.Drawing.Image)(resources.GetObject("btn_keyboard.Image")));
      this.btn_keyboard.IsSelected = false;
      this.btn_keyboard.Location = new System.Drawing.Point(17, 370);
      this.btn_keyboard.Name = "btn_keyboard";
      this.btn_keyboard.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_keyboard.Size = new System.Drawing.Size(76, 60);
      this.btn_keyboard.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.TERTIARY;
      this.btn_keyboard.TabIndex = 1;
      this.btn_keyboard.TabStop = false;
      this.btn_keyboard.UseVisualStyleBackColor = false;
      this.btn_keyboard.Click += new System.EventHandler(this.btn_keyboard_Click);
      // 
      // btn_cancel
      // 
      this.btn_cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_cancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_cancel.FlatAppearance.BorderSize = 0;
      this.btn_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_cancel.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_cancel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_cancel.Image = null;
      this.btn_cancel.IsSelected = false;
      this.btn_cancel.Location = new System.Drawing.Point(447, 370);
      this.btn_cancel.Name = "btn_cancel";
      this.btn_cancel.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_cancel.Size = new System.Drawing.Size(155, 60);
      this.btn_cancel.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_cancel.TabIndex = 3;
      this.btn_cancel.Text = "CANCEL";
      this.btn_cancel.UseVisualStyleBackColor = false;
      this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
      // 
      // btn_ok
      // 
      this.btn_ok.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_ok.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_ok.FlatAppearance.BorderSize = 0;
      this.btn_ok.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_ok.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_ok.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_ok.Image = null;
      this.btn_ok.IsSelected = false;
      this.btn_ok.Location = new System.Drawing.Point(621, 370);
      this.btn_ok.Name = "btn_ok";
      this.btn_ok.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_ok.Size = new System.Drawing.Size(155, 60);
      this.btn_ok.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_ok.TabIndex = 4;
      this.btn_ok.Text = "OK";
      this.btn_ok.UseVisualStyleBackColor = false;
      this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
      // 
      // frm_payment_order
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.ClientSize = new System.Drawing.Size(798, 504);
      this.ControlBox = false;
      this.KeyPreview = true;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "frm_payment_order";
      this.ShowIcon = false;
      this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
      this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
      this.Style = WSI.Cashier.Controls.frm_base.BaseFormStyle.PRINCIPAL;
      this.Text = "frm_payment_order";
      this.Load += new System.EventHandler(this.frm_payment_order_Load);
      this.pnl_data.ResumeLayout(false);
      this.tab_client.ResumeLayout(false);
      this.tab_payment.ResumeLayout(false);
      this.tap_values.ResumeLayout(false);
      this.tlp_authorizations.ResumeLayout(false);
      this.Main.ResumeLayout(false);
      this.tlp_account.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    

    private WSI.Cashier.Controls.uc_round_button btn_cancel;
    private WSI.Cashier.Controls.uc_round_button btn_ok;
    private WSI.Cashier.Controls.uc_round_button btn_keyboard;
    internal System.Windows.Forms.Timer timer1;
    private WSI.Cashier.Controls.uc_label lbl_msg_blink;
    private WSI.Cashier.Controls.uc_label label3;
    private WSI.Cashier.Controls.uc_label label4;
    private WSI.Cashier.Controls.uc_round_textbox textBox1;
    private WSI.Cashier.Controls.uc_round_combobox comboBox1;
    private WSI.Cashier.Controls.uc_label label5;
    private WSI.Cashier.Controls.uc_label label6;
    private WSI.Cashier.Controls.uc_label label7;
    private WSI.Cashier.Controls.uc_label label8;
    private WSI.Cashier.Controls.uc_label label9;
    private WSI.Cashier.Controls.uc_round_textbox textBox2;
    private WSI.Cashier.Controls.uc_round_tab_control tab_client;
    private System.Windows.Forms.TabPage Main;
    private CharacterTextBox ef_holder_id_02;
    private CharacterTextBox ef_holder_id_01;
    private WSI.Cashier.Controls.uc_label lbl_name_03;
    private CharacterTextBox ef_name_03;
    private WSI.Cashier.Controls.uc_label lbl_name_02;
    private CharacterTextBox ef_name_02;
    private WSI.Cashier.Controls.uc_label lbl_name_01;
    private CharacterTextBox ef_name_01;
    private System.Windows.Forms.TabPage tab_payment;
    private WSI.Cashier.Controls.uc_label lbl_cash;
    private WSI.Cashier.Controls.uc_round_textbox ef_cash;
    private WSI.Cashier.Controls.uc_label lbl_total;
    private WSI.Cashier.Controls.uc_label lbl_Bank_Check;
    private WSI.Cashier.Controls.uc_round_textbox ef_bank_check;
    private WSI.Cashier.Controls.uc_round_button btn_num_1;
    private WSI.Cashier.Controls.uc_round_button btn_back;
    private WSI.Cashier.Controls.uc_round_button btn_num_2;
    private WSI.Cashier.Controls.uc_round_button btn_num_3;
    private WSI.Cashier.Controls.uc_round_button btn_num_4;
    private WSI.Cashier.Controls.uc_round_button btn_num_10;
    private WSI.Cashier.Controls.uc_round_button btn_num_5;
    private WSI.Cashier.Controls.uc_round_button btn_num_dot;
    private WSI.Cashier.Controls.uc_round_button btn_num_6;
    private WSI.Cashier.Controls.uc_round_button btn_num_9;
    private WSI.Cashier.Controls.uc_round_button btn_num_7;
    private WSI.Cashier.Controls.uc_round_button btn_num_8;
    private WSI.Cashier.Controls.uc_round_combobox cb_type_doc2;
    private WSI.Cashier.Controls.uc_round_combobox cb_type_doc1;
    private System.Windows.Forms.TabPage tap_values;
    private WSI.Cashier.Controls.uc_label lb_authorization2;
    private CharacterTextBox ef_authorization2_name;
    private WSI.Cashier.Controls.uc_label lb_authorization1;
    private CharacterTextBox ef_authorization1_name;
    private WSI.Cashier.Controls.uc_label lb_authorization_name;
    private CharacterTextBox ef_authorization2_title;
    private WSI.Cashier.Controls.uc_label lb_authorization_title;
    private CharacterTextBox ef_authorization1_title;
    private WSI.Cashier.Controls.uc_label lbl_bank_acount;
    private WSI.Cashier.Controls.uc_round_combobox cb_bank_account;
    private WSI.Cashier.Controls.uc_label lb_total_cashable;
    private WSI.Cashier.Controls.uc_label lbl_account_number;
    private WSI.Cashier.Controls.uc_label lbl_bank_name;
    private WSI.Cashier.Controls.uc_label lb_doc_type;
    private WSI.Cashier.Controls.uc_label lb_doc;
    private uc_datetime uc_aply_date;
    private System.Windows.Forms.TableLayoutPanel tlp_account;
    private WSI.Cashier.Controls.uc_label lbl_name_04;
    private CharacterTextBox ef_name_04;
    private System.Windows.Forms.TableLayoutPanel tlp_authorizations;
    private Controls.uc_round_combobox cb_check_types;
    private Controls.uc_label lbl_check_type;
  }
}