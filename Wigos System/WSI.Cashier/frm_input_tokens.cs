//------------------------------------------------------------------------------
// Copyright � 2009 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_input_tokens.cs
// 
//   DESCRIPTION: Implements the following classes:
//                1. frm_input_tokens
//
//        AUTHOR: APB
// 
// CREATION DATE: 23-SEP-2010
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 23-SEP-2010 APB    First release.
// 13-AUG-2013 DRV    Removed disabling OSK if it's in window mode. 
// 26-ENE-2015 ANM    Increase token number's length to 5 
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WSI.Common;

namespace WSI.Cashier
{
  public partial class frm_input_tokens : WSI.Cashier.Controls.frm_base
  {
    #region Attributes

    private String m_token_name;
    private Int32 m_min_tokens;
    private Int32 m_num_tokens;
    private static Boolean m_cancel = true;
    private const Int16 NUM_TOKENS_LENGTH = 5;

    #endregion

    #region Constructor

    public frm_input_tokens()
    {
      InitializeComponent();

      InitializeControlResources();
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Initialize control resources (NLS strings, images, etc)
    /// </summary>
    private void InitializeControlResources()
    {
      // - NLS Strings:
      //   - Title:
      this.FormTitle = Resource.String("STR_FRM_INPUT_TOKENS_TITLE").ToUpper();  

      //   - Labels
      // Initialized later, as token name value is required
      //lbl_enter_num_tokens.Text = Resource.String("STR_FRM_INPUT_TOKENS_001", token_name);
      
      //   - Buttons
      btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");
      btn_ok.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_OK");

      // - Images:

    } // InitializeControlResources

    private void Init()
    {
      //   - Labels
      lbl_enter_num_tokens.Text = Resource.String("STR_FRM_INPUT_TOKENS_001", m_token_name);

      this.Location = new Point(1024 / 2 - this.Width / 2, 768 / 2 - this.Height / 2 - 90);

      //Shows keyboard if Automatic OSK it's enabled
      WSIKeyboard.Toggle();
    } // Init

    private void btn_cancel_Click(object sender, EventArgs e)
    {
      m_cancel = true;
      //m_num_tokens = 0;
      WSIKeyboard.Hide(); 
      this.Dispose();
    } // btn_cancel_Click

    private void btn_ok_click(object sender, EventArgs e)
    {
      Int32.TryParse(this.txt_num_tokens.Text, out m_num_tokens);
      WSIKeyboard.Hide();
      this.Dispose();
    } // btn_ok_click

    private void frm_input_tokens_Shown(object sender, EventArgs e)
    {
      this.txt_num_tokens.Focus();
    } // frm_input_tokens_Shown

    /// <summary>
    /// Checks if the input is completely numeric.
    /// </summary>
    /// <param name="TrackNumber"></param>
    /// <returns></returns>
    private Boolean PartialNumberIsValid(String NumTokens)
    {
      Byte[] chars;

      chars = null;

      try
      {
        chars = Encoding.ASCII.GetBytes(NumTokens);
        foreach (byte b in chars)
        {
          if (b < '0')
          {
            return false;
          }
          if (b > '9')
          {
            return false;
          }
        }
      }
      finally
      {
        chars = null;
      }

      return true;
    } // PartialNumberIsValid

    /// <summary>
    /// Key pressed management.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void txt_num_tokens_KeyPress(object sender, KeyPressEventArgs e)
    {
      String num_tokens_str;
      Int32 num_tokens;

      num_tokens_str = txt_num_tokens.Text + e.KeyChar;

      switch (e.KeyChar)
      {
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
          if (PartialNumberIsValid(num_tokens_str) && (num_tokens_str.Length <= NUM_TOKENS_LENGTH))
          {
            if (Int32.TryParse(num_tokens_str, out num_tokens))
            {
              //txt_num_tokens.AppendText(e.KeyChar.ToString());
              txt_num_tokens.Text = txt_num_tokens.Text + e.KeyChar.ToString();
              txt_num_tokens.Text = num_tokens.ToString();
            }
          }
          e.Handled = true;
          break;

        case '\b':
          // Backspace is allowed to go through, others are not
          break;

        case '\r':
          btn_ok_click(null, null);
          break;

        default:
          e.KeyChar = '\0';
          break;
      } // switch
    } // txt_num_tokens_KeyPress

    #endregion

    #region Public Methods

    public void Show(String TokenName, Int32 MinTokens, out Int32 NumTokens, Form Parent)
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_input_tokens", Log.Type.Message);

      // This value is used to initialize texts in the form
      this.m_token_name = TokenName;
      this.m_min_tokens = MinTokens;
      this.Init();
      this.txt_num_tokens.Text = "";
      this.m_num_tokens = 0;
      m_cancel = false;

      this.ShowDialog(Parent);

      if (!m_cancel)
      {
        NumTokens = m_num_tokens;
      }
      else
      {
        NumTokens = -1;
      }
    }

    private void pnl_data_Paint(object sender, PaintEventArgs e)
    {

    } // Show

    #endregion

  }
}