﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WSI.Cashier.Controls;
using WSI.Common;
using WSI.Common.PinPad;

namespace WSI.Cashier
{
  public partial class frm_pinpad_config_edit : frm_base
  {

    #region " Properties "

    public DataTable PinPands { get; set; }
    private DISPLAY_MODE DisplayMode { get; set; }
    private PinPadCashierTerminal PinPad { get; set; }

    #endregion " Properties"

    #region " Enum "

    public enum DISPLAY_MODE
    {
      NEW = 0,
      EDIT = 1
    }

    #endregion " Enum "

    #region " Public Methods "

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="Mode"></param>
    /// <param name="PinPadCashier"></param>
    public frm_pinpad_config_edit(DISPLAY_MODE Mode, PinPadCashierTerminal PinPadCashier = null)
    {
      InitializeComponent();

      this.DisplayMode = Mode;
      this.PinPad = PinPadCashier;

      InitializeControlResources();
    } // frm_pinpad_config_edit

    #endregion " Public Methods "

    #region " Private Methods "

    /// <summary>
    /// Initialize control resources (NLS strings, images, etc)
    /// </summary>
    private void InitializeControlResources()
    {      
      // Buttons
      this.btn_ok.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_OK");
      this.btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");
      // Type
      this.cb_type.DataSource = EnumToDataTable(typeof(PinPadType));
      this.cb_type.ValueMember = "Id";
      this.cb_type.DisplayMember = "Desc";

      // Port
      this.cb_port.DataSource = EnumToDataTable(typeof(PinPadCashierTerminal.PINPAD_PORT));
      this.cb_port.ValueMember = "Id";
      this.cb_port.DisplayMember = "Desc";

      //Enabled
      this.chk_enabled.Text = Resource.String("STR_FRM_PINPAD_CONFIG_DGV_ACTIVE");

      // Display Mode
      switch (DisplayMode)
      {
        case DISPLAY_MODE.NEW:
          this.FormTitle = Resource.String("STR_FRM_PINPAD_CONFIG_EDIT_NEW");
          this.cb_type.Text = String.Empty;
          this.cb_type.SelectedItem = null;
          this.cb_port.Text = String.Empty;
          this.cb_port.SelectedItem = null;
          this.chk_enabled.Checked = true;
          break;

        case DISPLAY_MODE.EDIT:
          this.FormTitle = Resource.String("STR_FRM_PINPAD_CONFIG_EDIT_EDIT");
          this.cb_type.SelectedIndex = (Int32)PinPad.Type;
          this.cb_type.Enabled = false;
          this.cb_port.SelectedIndex = (Int32)PinPad.Port;
          this.chk_enabled.Checked = PinPad.Enabled;
          break;
      }

      this.btn_cancel.Focus();

    } // InitializeControlResources

    /// <summary>
    /// Convert Enum to DataTable
    /// </summary>
    /// <param name="enumType"></param>
    /// <returns></returns>
    public DataTable EnumToDataTable(Type enumType)
    {
      DataTable _table = new DataTable();

      //Column that contains the Captions/Keys of Enum        
      _table.Columns.Add("Desc", typeof(string));
      //Get the type of ENUM for DataColumn
      _table.Columns.Add("Id", Enum.GetUnderlyingType(enumType));
      //Add the items from the enum:
      foreach (string name in Enum.GetNames(enumType))
      {
        //Replace underscores with space from caption/key and add item to collection:

#if DEBUG
        //ETP 08-11-2016 Dummy is showed only for debug.
        if (GeneralParam.GetBoolean("PinPad", "Provider.001.Test.Enabled", false) && name == "DUMMY") //If test add Dummy Project
        {
          _table.Rows.Add(name.Replace('_', ' '), Enum.Parse(enumType, name));
        }
#endif

        if (name != "NONE" && name != "DUMMY")
        {
          _table.Rows.Add(name.Replace('_', ' '), Enum.Parse(enumType, name));
        }       

      }

      return _table;
    } // EnumToDataTable

    #region " Events "

    /// <summary>
    /// Button cancel
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btn_cancel_Click(object sender, EventArgs e)
    {
      switch (DisplayMode)
      {
        case DISPLAY_MODE.NEW:
          break;

        case DISPLAY_MODE.EDIT:
          break;
      }

      Misc.WriteLog("[FORM CLOSE] frm_pinpad_config_edit (cancel)", Log.Type.Message);
      this.Close();
    } // btn_cancel_Click

    /// <summary>
    /// Button Ok
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btn_ok_Click(object sender, EventArgs e)
    {
      DataRow[] _dr;

      switch (DisplayMode)
      {
        case DISPLAY_MODE.NEW:
          if (this.cb_type.SelectedItem == null) return;
          if (this.cb_port.SelectedItem == null) return;
          _dr = this.PinPands.Select("PCT_TYPE  = " + this.cb_type.SelectedIndex.ToString());
          if (_dr.Length > 0)
          {
            frm_message.Show("Ya existe un pinpad de este tipo", Resource.String("STR_VOUCHER_ERROR_TITLE"), MessageBoxButtons.OK, this.ParentForm);

            return;
          }
          this.PinPad = new PinPadCashierTerminal();
          this.PinPad.CashierId = (Int32)Cashier.TerminalId;
          this.PinPad.Type = (WSI.Common.PinPadType)this.cb_type.SelectedValue;
          this.PinPad.Port = (PinPadCashierTerminal.PINPAD_PORT)this.cb_port.SelectedValue;
          this.PinPad.Enabled = chk_enabled.Checked;
          this.PinPad.DB_Insert();
          break;

        case DISPLAY_MODE.EDIT:
          this.PinPad.Port = (PinPadCashierTerminal.PINPAD_PORT)this.cb_port.SelectedValue;
          this.PinPad.Enabled = chk_enabled.Checked;
          this.PinPad.DB_UpdateById();
          break;
      }

      Misc.WriteLog("[FORM CLOSE] frm_pinpad_config_edit (ok)", Log.Type.Message);
      this.Close();
    } // btn_ok_Click

    #endregion " Events "

    #endregion " Private Methods "
  }
}
