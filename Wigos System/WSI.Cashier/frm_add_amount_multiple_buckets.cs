﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_add_amount_multilple_buckets.cs 
// 
//   DESCRIPTION: Class with necessary frm to add amount for multiple buckets.
// 
//        AUTHOR: Enric Tomas
// 
// CREATION DATE: 19-JAN-2016
// 
// REVISION HISTORY:
// 
// Date        Author     Description
// ----------- ---------- ------------------------------------------------------
// 19-JAN-2016 ETP        First version.
// 02-FEB-2016 FGB        Now we control that the bucket has not changed its value (expired) while adding amount.
// 02-FEB-2016 ETP        Now we control that the bucket is visible in casher terminals.
// 04-FEB-2016 ETP        Now we can add and substract decimals with credit type buckets.
// 11-FEB-2016 FGB        Screen redesign. Changed position, sizes and replaced Add & Substract button with radio buttons.
// 03-JUN-2016 ETP        Fixed bug 14105: Error BBDD when comment is > 50 characters
// 02-AGO-2016 JRC        PBI 16257:Buckets - RankingLevelPoints_Generated - RankingLevelPoints_Discretional
// 22-SEP-2016 FJC        Bug 18019:PromoBOX: no se muestran los puntos negativos de la cuenta de un cliente
// 02-AUG-2017 FGB        Bug 29105: Bucket modification: If an error occurs it is ignored
// 17-APR-2018 AGS        Bug 32370:WIGOS-7796 The user is able to try to add or deduct the "Ranking level points" amount
// -----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Data.SqlClient;
using System.Windows.Forms;
using WSI.Common;

namespace WSI.Cashier
{
  public partial class frm_add_amount_multiple_buckets : Controls.frm_base
  {
    private enum ActionResult
    {
      OK,
      KO_InitialValueChanged,
      KO_FinalValueNegative,
      KO_VoucherError,
      KO_OtherError
    }

    #region Constants

    private const Int32 GRID_COLUMN_BUCKET_NAME = 0;
    private const Int32 GRID_COLUMN_BUCKET_NAME_SIZE = 400;
    private const Int32 GRID_COLUMN_BUCKET_AMOUNT_SIZE = 100;

    private const Int32 GRID_COLUMN_BUCKET_AMOUNT = 1;
    private const Int32 GRID_COLUMN_BUCKET_ID = 2;
    private const Int32 GRID_COLUMN_BUCKET_AMOUNT_STR = 3;
    private const Int32 GRID_COLUMN_BUCKET_TYPE = 4;

    private const Int32 MAX_REASON_TEXT_LENGTH = 50;
    private const String EMPTY_RESULT_TEXT = "---";

    #endregion //Constants

    #region Properties

    private static CardData m_card_data;
    private BucketsForm m_BucketsForm;
    private Boolean m_HasOperation;
    private Boolean m_RefreshingFromError;
    private Buckets.BucketOperation m_BucketOperation;
    private Decimal m_OperationAmount;

    #endregion //Properties

    #region Constructor
    //------------------------------------------------------------------------------
    // PURPOSE : Constructor
    //
    //  PARAMS :
    //      - INPUT : 
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //   NOTES :
    // 
    public frm_add_amount_multiple_buckets()
    {
      InitializeComponent();

      base.PerformOnExitClicked += CheckAndCloseWindow;
    }
    #endregion //Constructor

    #region PublicFunctions

    //------------------------------------------------------------------------------
    // PURPOSE : Init Form
    //
    //  PARAMS :
    //      - INPUT : 
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //   NOTES :
    // 
    public void Init()
    {
      m_BucketsForm = new BucketsForm();

      InicializeControlResources();

      SetInitValues(true);

      RefreshBucketsGrid();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Show form
    //
    //  PARAMS :
    //      - INPUT : cardData
    //                ParentForm 
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //   NOTES :
    // 
    public void Show(CardData cardData, Form parentForm)
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_add_amount_multiple_buckets", Log.Type.Message);
      // Initialize values.
      m_card_data = new CardData();
      m_card_data = cardData;

      Init();

      ShowDialog(parentForm);
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Inicialize control resourses
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //   NOTES :
    // 
    public void InicializeControlResources()
    {
      this.FormTitle = Resource.String("STR_BUCKET_BUTTON") + " " + m_card_data.AccountId + " - " + m_card_data.PlayerTracking.HolderName;

      gb_Buckets.HeaderText = Resource.String("STR_BUCKET_SELECT");
      gb_Action.HeaderText = Resource.String("STR_FRM_TITO_TICKETS_DISCARD_GRID_ACTION");
      uc_radioButton_add.Text = Resource.String("STR_BUCKET_ADD");
      uc_radioButton_sub.Text = Resource.String("STR_BUCKET_SUB");
      gb_total.HeaderText = Resource.String("STR_VOUCHER_TOTAL_AMOUNT");
      lbl_result.Text = String.Empty;
      lbl_operacion.Text = String.Empty;
      btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");
      btn_ok.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_OK");
    }

    #endregion //PublicFunctions

    #region PrivateFunctions

    /// <summary>
    /// Returns the message of error of the ActionResult
    /// </summary>
    /// <param name="Result">ActionResult that shows the result of the action</param>
    /// <param name="MessageError">Error message returned from the function</param>
    /// <returns></returns>
    private String GetActionErrorMessage(ActionResult Result, String MessageError)
    {
      String _message_error;

      switch (Result)
      {
        case ActionResult.OK:
          _message_error = Resource.String("STR_BUCKET_MODIFICATION_RESULT_OK");
          break;

        case ActionResult.KO_InitialValueChanged:
          _message_error = Resource.String("STR_BUCKET_MODIFICATION_RESULT_KO_INITIAL_VALUE_CHANGED");
          break;

        case ActionResult.KO_FinalValueNegative:
          _message_error = Resource.String("STR_BUCKET_MODIFICATION_RESULT_KO_FINAL_VALUE_NEGATIVE");
          break;

        case ActionResult.KO_VoucherError:
          _message_error = Resource.String("STR_BUCKET_MODIFICATION_RESULT_KO_PRINT_VOUCHER");
          break;

        case ActionResult.KO_OtherError:
        default:
          _message_error = Resource.String("STR_BUCKET_MODIFICATION_RESULT_KO_OTHER_ERROR");
          break;
      }

      //If MessageError is not empty we append it
      if (!String.IsNullOrEmpty(MessageError))
      {
        _message_error = _message_error + " - " + MessageError;
      }

      return _message_error;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Set titles of data grid columns and visibility
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //   NOTES :
    // 
    private void SetDataGridColums()
    {
      dgv_Bucket_Grid.Columns[GRID_COLUMN_BUCKET_NAME].Width = GRID_COLUMN_BUCKET_NAME_SIZE;
      dgv_Bucket_Grid.Columns[GRID_COLUMN_BUCKET_NAME].HeaderText = Resource.String("STR_FRM_MANAGE_PROMOTIONS_DATAGRID_NAME");

      dgv_Bucket_Grid.Columns[GRID_COLUMN_BUCKET_AMOUNT_STR].Width = GRID_COLUMN_BUCKET_AMOUNT_SIZE;
      dgv_Bucket_Grid.Columns[GRID_COLUMN_BUCKET_AMOUNT_STR].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
      dgv_Bucket_Grid.Columns[GRID_COLUMN_BUCKET_AMOUNT_STR].HeaderText = Resource.String("STR_VOUCHER_CAGESESSIONREPORT_QUANTITY");

      //Invisible colums:
      dgv_Bucket_Grid.Columns[GRID_COLUMN_BUCKET_AMOUNT].Width = 0;
      dgv_Bucket_Grid.Columns[GRID_COLUMN_BUCKET_AMOUNT].Visible = false;

      dgv_Bucket_Grid.Columns[GRID_COLUMN_BUCKET_ID].Width = 0;
      dgv_Bucket_Grid.Columns[GRID_COLUMN_BUCKET_ID].Visible = false;

      dgv_Bucket_Grid.Columns[GRID_COLUMN_BUCKET_TYPE].Width = 0;
      dgv_Bucket_Grid.Columns[GRID_COLUMN_BUCKET_TYPE].Visible = false;
    }

    /// <summary>
    /// Enable or disable add and substract buttons by Bucket Properties
    /// </summary>
    private void EnableDisableButtons()
    {
      DataGridViewSelectedCellCollection _SelectedCells;
      Buckets.BucketId _bucket_id;
      Buckets.BucketType _type;
      Bucket_Properties _properties;

      SetInitOperation();

      _SelectedCells = dgv_Bucket_Grid.SelectedCells;
      if (_SelectedCells.Count > 0)
      {
        _bucket_id = (Buckets.BucketId)(Int64)_SelectedCells[GRID_COLUMN_BUCKET_ID].Value;
        _type = (Buckets.BucketType)dgv_Bucket_Grid.SelectedCells[GRID_COLUMN_BUCKET_TYPE].Value;
        _properties = Buckets.GetProperties(_bucket_id);

        //Activamos o no los botones de las acciones
        uc_radioButton_add.Enabled = _properties.cashier_can_add;
        uc_radioButton_sub.Enabled = _properties.cashier_can_sub;
        uc_radioButton_add.AutoCheck = _properties.cashier_can_add;
        uc_radioButton_sub.AutoCheck = _properties.cashier_can_sub;
        //Activamos o no el teclado numérico
        uc_input_amount1.Enabled = uc_radioButton_add.Enabled || uc_radioButton_sub.Enabled;
        //Activamos o no el boton decimal
        uc_input_amount1.SetDotButtonEnabled(Buckets.GetBucketUnits(_type) == Buckets.BucketUnits.Money);
      }
    }

    /// <summary>
    /// Set initial values of the screen
    /// </summary>
    /// <param name="DoInitOperation"></param>
    private void SetInitValues(Boolean DoInitOperation)
    {
      if (DoInitOperation)
      {
        SetInitOperation();
      }

      SetInitResult();
    }

    /// <summary>
    /// Set the Initial values to the operation variables/values
    /// </summary>
    private void SetInitOperation()
    {
      m_HasOperation = false;
      m_RefreshingFromError = false;
      m_BucketOperation = Buckets.BucketOperation.EXPIRE;

      uc_radioButton_add.Checked = false;
      uc_radioButton_add.Enabled = false;
      uc_radioButton_sub.Checked = false;
      uc_radioButton_sub.Enabled = false;
    }

    /// <summary>
    /// Set the result and Operation
    /// </summary>
    private void SetInitResult()
    {
      m_OperationAmount = 0;
      uc_input_amount1.txt_amount.Text = String.Empty;
      lbl_operacion.Text = String.Empty;
      lbl_result.Text = EMPTY_RESULT_TEXT;
    }

    /// <summary>
    /// Set the Operation code
    /// </summary>
    /// <param name="BucketOp"></param>
    private void SetOperation(Buckets.BucketOperation BucketOp)
    {
      SetInitValues(false);

      if ((BucketOp == Buckets.BucketOperation.ADD_DISCRETIONAL) || (BucketOp == Buckets.BucketOperation.SUB_DISCRETIONAL))
      {
        m_HasOperation = true;
        m_BucketOperation = BucketOp;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Refresh the Buckets grid.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //   NOTES :
    // 
    private void RefreshBucketsGrid()
    {
      DataTable _DT;
      Int32 _current_idx;

      if (!m_RefreshingFromError)
      {
        SetInitOperation();
      }

      _current_idx = 0;
      //Get the current row
      if (dgv_Bucket_Grid.DataSource != null)
      {
        _current_idx = dgv_Bucket_Grid.CurrentCell.RowIndex;
      }

      if (m_BucketsForm.GetBucketsByUser(m_card_data.AccountId, Buckets.BucketVisibility.Cashier, out _DT))
      {
        dgv_Bucket_Grid.DataSource = _DT;
        SetDataGridColums();
      }

      //Set the current row
      if (dgv_Bucket_Grid.RowCount > 0)
      {
        dgv_Bucket_Grid.CurrentCell = this.dgv_Bucket_Grid[0, _current_idx];
      }
    }

    /// <summary>
    /// Get the reason text for the bucket operation
    /// </summary>
    /// <param name="Reason"></param>
    /// <returns>Boolean, it indicates whether the user entered text</returns>
    private Boolean GetReason(out String Reason)
    {
      Boolean IsOK;
      String _window_title = Resource.String("STR_BUCKET_MODIFICATION_TITLE");
      String _label_text = Resource.String("STR_BUCKET_MODIFICATION_REASON");
      frm_generic_string_input _frm_reason;

      _frm_reason = new frm_generic_string_input(_window_title, _label_text, MAX_REASON_TEXT_LENGTH);
      _frm_reason.Show(this, out IsOK, out Reason);

      return IsOK;
    }

    /// <summary>
    /// Modify the customer bucket amount
    /// </summary>
    /// <returns>If must close the form</returns>
    private Boolean ModifyBucketAmount()
    {
      DataGridViewSelectedCellCollection _selected_cells;
      Decimal _initial_value;
      String _reason;
      Buckets.BucketId _bucket_id;
      Buckets.BucketType _bucket_type;
      String _message_error;
      ActionResult _action_result;

      try
      {
        _selected_cells = dgv_Bucket_Grid.SelectedCells;

        if (_selected_cells.Count > 0)
        {
          _initial_value = (Decimal)_selected_cells[GRID_COLUMN_BUCKET_AMOUNT].Value;
          _bucket_id = (Buckets.BucketId)(Int64)_selected_cells[GRID_COLUMN_BUCKET_ID].Value;
          _bucket_type = (Buckets.BucketType)_selected_cells[GRID_COLUMN_BUCKET_TYPE].Value;

          if (!ValidateInput(_initial_value, m_OperationAmount, _bucket_type))
          {
            return false;
          }

          if (!GetReason(out _reason))
          {
            frm_message.Show(Resource.String("STR_BUCKET_MODIFICATION_CANCELED_BY_USER"),
                             Resource.String("STR_APP_GEN_MSG_WARNING"),
                             MessageBoxButtons.OK,
                             MessageBoxIcon.Warning,
                             ParentForm);
            return false;
          }

          using (DB_TRX _db_trx = new DB_TRX())
          {
            _action_result = ExecuteAction(_bucket_id
                                         , _bucket_type
                                         , _initial_value
                                         , _reason
                                         , out _message_error
                                         , _db_trx.SqlTransaction);

            if (_action_result == ActionResult.OK)
            {
              _db_trx.Commit();
              SetInitValues(true);

              return true;
            }
          }

          //_action_result != ActionResult.OK
          frm_message.Show(GetActionErrorMessage(_action_result, _message_error),
                           Resource.String("STR_VOUCHER_ERROR_TITLE"),
                           MessageBoxButtons.OK,
                           MessageBoxIcon.Error,
                           ParentForm);

          m_RefreshingFromError = true;
          RefreshBucketsGrid();
          ShowPreResult();
          m_RefreshingFromError = false;

          return false;
        }
      }
      catch (Exception Ex)
      {
        frm_message.Show(Resource.String("STR_UC_CARD_USER_MSG_GENERIC_ERROR"),
                         Resource.String("STR_VOUCHER_ERROR_TITLE"),
                         MessageBoxButtons.OK,
                         MessageBoxIcon.Error,
                         ParentForm);
        Log.Error("Btn ADD Add_amount_multiple_buckets: Couldn't add amount to bucket");
        Log.Exception(Ex);
      }

      return true;
    }

    /// <summary>
    /// Shows (on the screen) the value the bucket will have after the operation
    /// </summary>
    public void ShowPreResult()
    {
      Decimal _initial_value;
      Decimal _final_value;
      String _op;
      Buckets.BucketType _type;

      //Obtenemos datos del bucket
      _type = (Buckets.BucketType)dgv_Bucket_Grid.SelectedCells[GRID_COLUMN_BUCKET_TYPE].Value;
      _initial_value = (Decimal)dgv_Bucket_Grid.SelectedCells[GRID_COLUMN_BUCKET_AMOUNT].Value;

      if (!ValidateInput(_initial_value, m_OperationAmount, _type))
      {
        SetInitResult();

        return;
      }

      _final_value = CalculateFinalAmount(_initial_value, m_OperationAmount);
      _op = GetOperatorString();

      lbl_operacion.Text = m_BucketsForm.SetCurrencyValue(_initial_value, _type) + _op + m_BucketsForm.SetCurrencyValue(m_OperationAmount, _type) + " = ";
      lbl_result.Text = m_BucketsForm.SetCurrencyValue(_final_value, _type);

      if (_final_value < 0)
      {
        this.lbl_result.ForeColor = System.Drawing.Color.Red;
      }
      else
      {
        this.lbl_result.ForeColor = System.Drawing.Color.White;
      }
    }

    /// <summary>
    /// Validate the input before processing
    /// </summary>
    /// <param name="InitialValue"></param>
    /// <param name="IncrementValue"></param>
    /// <returns></returns>
    public Boolean ValidateInput(Decimal InitialValue, Decimal IncrementValue, Buckets.BucketType BucketType)
    {
      Decimal _final_value;

      if (!m_HasOperation)
      {
        frm_message.Show(Resource.String("STR_BUCKET_OPERATION_MISSING"),      //Indique accion
                         Resource.String("STR_APP_GEN_MSG_WARNING"),
                         MessageBoxButtons.OK,
                         MessageBoxIcon.Warning,
                         ParentForm);

        return false;
      }

      if (IncrementValue <= 0)
      {
        frm_message.Show(Resource.String("STR_FRM_VOUCHERS_REPRINT_065"),      //Debe introducir un valor
                         Resource.String("STR_APP_GEN_MSG_WARNING"),
                         MessageBoxButtons.OK,
                         MessageBoxIcon.Warning,
                         ParentForm);

        return false;
      }


      if (BucketType != Buckets.BucketType.RedemptionPoints) // 22-SEP-2016 FJC Bug 18019:PromoBOX: no se muestran los puntos negativos de la cuenta de un cliente
      {
        _final_value = CalculateFinalAmount(InitialValue, IncrementValue);

        if (_final_value < 0)
        {
          frm_message.Show(Resource.String("STR_BUCKET_NEGATIVE_FINAL_RESULT_MESSAGE"),    //Valor negativo
                           Resource.String("STR_APP_GEN_MSG_WARNING"),
                           MessageBoxButtons.OK,
                           MessageBoxIcon.Warning,
                           ParentForm);

          return false;
        }
      }

      return true;
    }

    /// <summary>
    /// Calculates the final amount, adding or substracting the increment to the initial value
    /// </summary>
    /// <param name="InitialValue"></param>
    /// <param name="IncrementValue"></param>
    /// <returns>Final Amount</returns>
    private decimal CalculateFinalAmount(Decimal InitialValue, Decimal IncrementValue)
    {
      Decimal _final_value;

      switch (m_BucketOperation)
      {
        case Buckets.BucketOperation.ADD_DISCRETIONAL:
          _final_value = InitialValue + IncrementValue;
          break;

        case Buckets.BucketOperation.SUB_DISCRETIONAL:
          _final_value = InitialValue - IncrementValue;
          break;

        default:
          Log.Error("BucketOperation not defined: " + m_BucketOperation);
          throw new Exception("BucketOperation not defined: " + m_BucketOperation + " in CalculateFinalAmount");
      }

      return _final_value;
    }

    /// <summary>
    /// Return string with the operatin to perform (+, -)
    /// </summary>
    /// <returns></returns>
    private string GetOperatorString()
    {
      switch (m_BucketOperation)
      {
        case Buckets.BucketOperation.ADD_DISCRETIONAL:
          return " + ";

        case Buckets.BucketOperation.SUB_DISCRETIONAL:
          return " - ";

        default:
          Log.Error("BucketOperation not defined: " + m_BucketOperation);
          throw new Exception("BucketOperation not defined: " + m_BucketOperation + " in GetOperatorString");
      }
    }

    /// <summary>
    /// Executes the Action to perform
    /// </summary>
    /// <param name="BucketId">Bucket ID</param>
    /// <param name="Type">Bucket Type</param>
    /// <param name="InitialValue">Initial Value of the bucket</param>
    /// <param name="Reason">Reason for the action</param>
    /// <param name="Trx">Transaction</param>
    /// <returns></returns>
    private ActionResult ExecuteAction(Buckets.BucketId BucketId, Buckets.BucketType Type, Decimal InitialValue, String Reason, out String MessageError, SqlTransaction Trx)
    {
      String _bucket_name;

      Decimal _initial_value;
      Decimal _increment_value;
      Decimal _final_value;

      OperationCode _operation_code;
      MovementType _movement_type;
      CASHIER_MOVEMENT _cashier_movement;
      Int64 _operation_id;

      MessageError = String.Empty;

      if (m_OperationAmount != 0)
      {
        _operation_code = OperationCode.MULTIPLE_BUCKETS_MANUAL;

        switch (m_BucketOperation)
        {
          case Buckets.BucketOperation.ADD_DISCRETIONAL:
            _increment_value = m_OperationAmount;
            _movement_type = MovementType.MULTIPLE_BUCKETS_Manual_Add + (Int32)BucketId;
            _cashier_movement = CASHIER_MOVEMENT.MULTIPLE_BUCKETS_MANUAL_ADD + (Int32)BucketId;
            break;

          case Buckets.BucketOperation.SUB_DISCRETIONAL:
            _increment_value = -m_OperationAmount;
            _movement_type = MovementType.MULTIPLE_BUCKETS_Manual_Sub + (Int32)BucketId;
            _cashier_movement = CASHIER_MOVEMENT.MULTIPLE_BUCKETS_MANUAL_SUB + (Int32)BucketId;
            break;

          default:
            MessageError = "BucketOperation not defined: " + m_BucketOperation;
            Log.Error(MessageError);
            return ActionResult.KO_OtherError;
        }

        //Update customer bucket
        if (!BucketsUpdate.UpdateCustomerBucket(m_card_data.AccountId, BucketId, InitialValue, m_OperationAmount, m_BucketOperation, true, true, out _initial_value, out _final_value, Trx))
        {
          return ActionResult.KO_InitialValueChanged;
        }
        else
        {
          if (Type != Buckets.BucketType.RedemptionPoints && _final_value < 0) // 22-SEP-2016 FJC Fixed Bug 18019:PromoBOX: no se muestran los puntos negativos de la cuenta de un cliente
          {
            return ActionResult.KO_FinalValueNegative;
          }
        }

        //Create operation and movements (cashier movs & accounts movs)
        if (!CreateOperationAndMovements(Reason, _initial_value, _increment_value, _final_value, _operation_code, _movement_type, _cashier_movement, out _operation_id, Trx))
        {
          return ActionResult.KO_OtherError;
        }

        //Print voucher
        _bucket_name = dgv_Bucket_Grid.SelectedCells[GRID_COLUMN_BUCKET_NAME].Value.ToString();
        if (!PrintVoucher(Type, _bucket_name, _initial_value, _increment_value, _final_value, _operation_id, Reason, Trx))
        {
          return ActionResult.KO_VoucherError;
        }
      }

      return ActionResult.OK;
    }

    /// <summary>
    /// Create Operation and Cashier & Account Movements
    /// </summary>
    /// <param name="Reason"></param>
    /// <param name="InitialValue"></param>
    /// <param name="IncrementValue"></param>
    /// <param name="FinalValue"></param>
    /// <param name="OpCode"></param>
    /// <param name="MoveType"></param>
    /// <param name="CashierMovement"></param>
    /// <param name="OperationId"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    private Boolean CreateOperationAndMovements(String Reason, Decimal InitialValue, Decimal IncrementValue, Decimal FinalValue, OperationCode OpCode, MovementType MoveType, CASHIER_MOVEMENT CashierMovement, out Int64 OperationId, SqlTransaction Trx)
    {
      CashierSessionInfo _cashier_session;
      _cashier_session = Cashier.CashierSessionInfo();

      if (!Operations.DB_InsertOperation(OpCode,
                                          m_card_data.AccountId,                  // AccountId
                                          _cashier_session.CashierSessionId,      // CashierSessionId
                                          0,                                      // MbAccountId
                                          IncrementValue,                         // Amount
                                          0,                                      // PromotionId
                                          0,                                      // NonRedeemable
                                          0,                                      // NonRedeemableWonLock
                                          0,                                      // OperationData 
                                          0,                                      // ReasonId
                                          Reason,                                 // Reason Comment
                                          out OperationId,                        // OperationId
                                          Trx))
      {
        return false;
      }

      // Save cashier movements.
      if (!m_BucketsForm.CreateCashierMovements(CashierMovement, _cashier_session, OperationId, m_OperationAmount, m_card_data.AccountId, m_card_data.TrackData, Reason, Trx))
      {
        return false;
      }

      // Save account movements.
      if (!m_BucketsForm.CreateAccountMovements(MoveType, _cashier_session, OperationId, InitialValue,
                                                (IncrementValue > 0 ? m_OperationAmount : 0),
                                                (IncrementValue < 0 ? m_OperationAmount : 0), FinalValue, m_card_data.AccountId, Reason, Trx))
      {
        return false;
      }

      return true;
    }

    /// <summary>
    /// Prints the Voucher
    /// </summary>
    /// <param name="Type"></param>
    /// <param name="BucketName"></param>
    /// <param name="InitialValue"></param>
    /// <param name="Increment"></param>
    /// <param name="FinalValue"></param>
    /// <param name="OperationId"></param>
    /// <param name="Reason"></param>
    /// <param name="Trx"></param>
    /// <returns>Printed the voucher correctly</returns>
    private Boolean PrintVoucher(Buckets.BucketType Type, String BucketName, Decimal InitialValue, Decimal Increment, Decimal FinalValue, Int64 OperationId, String Reason, SqlTransaction Trx)
    {
      String _initial;
      String _increment;
      String _final;

      BucketVoucher _voucher_data;
      VoucherBucketChange _voucher;

      _initial = m_BucketsForm.SetCurrencyValue(InitialValue, Type);
      _increment = m_BucketsForm.SetCurrencyValue(Increment, Type);
      _final = m_BucketsForm.SetCurrencyValue(FinalValue, Type);

      _voucher_data = new BucketVoucher(m_card_data.VoucherAccountInfo(),
                                        BucketName,
                                        _initial,
                                        _increment,
                                        _final,
                                        Reason);

      _voucher = new VoucherBucketChange(_voucher_data, PrintMode.Print, Trx);

      if (!_voucher.Save(OperationId, Trx))
      {
        return false;
      }

      VoucherBusinessLogic.VoucherSetHtml(_voucher.VoucherId, OperationId, _voucher.VoucherHTML, Trx);
      VoucherPrint.Print(_voucher);

      return true;
    }

    /// <summary>
    /// Check changes and close window
    /// </summary>
    private void CheckAndCloseWindow()
    {
      //String _text;
      //DialogResult _dr;

      //if ((m_HasOperation) || (m_OperationAmount != 0))
      //{
      //  _text = Resource.String("STR_BUCKET_LOST_CHANGES_MESSAGE");
      //  _text = _text.Replace("\\r\\n", "\r\n");

      //  _dr = frm_message.Show(_text,
      //                         Resource.String("STR_APP_GEN_MSG_WARNING"),
      //                         MessageBoxButtons.YesNo,
      //                         MessageBoxIcon.Warning,
      //                         ParentForm);
      //  if (_dr == DialogResult.OK)
      //  {
      //    this.Close();
      //  }
      //}
      //else
      //{
      //  this.Close();
      //}
      Misc.WriteLog("[FORM CLOSE] frm_add_amount_multiple_buckets (checkandclose)", Log.Type.Message);
      this.Close();
    }

    #endregion //PrivateFunctions

    #region Events

    private void dgv_Bucket_Grid_SelectionChanged(object sender, System.EventArgs e)
    {
      if (dgv_Bucket_Grid.SelectedCells.Count > 0)
      {
        if (!m_RefreshingFromError)
        {
          SetInitValues(true);
          EnableDisableButtons();
        }
      }
    }

    private void uc_radioButton_add_CheckedChanged(object sender, EventArgs e)
    {
      SetOperation(Buckets.BucketOperation.ADD_DISCRETIONAL);
    }

    private void uc_radioButton_sub_CheckedChanged(object sender, EventArgs e)
    {
      SetOperation(Buckets.BucketOperation.SUB_DISCRETIONAL);
    }

    private void btn_intro(decimal Amount)
    {
      m_OperationAmount = Amount;

      uc_input_amount1.txt_amount.Text = "";

      ShowPreResult();
    }

    private void btn_ok_Click(object sender, EventArgs e)
    {
      if (ModifyBucketAmount())
      {
        Misc.WriteLog("[FORM CLOSE] frm_add_amount_multiple_buckets (ok)", Log.Type.Message);
        this.Close();
      }
    }

    private void btn_cancel_Click(object sender, EventArgs e)
    {
      CheckAndCloseWindow();
    }

    #endregion //events
  }
}
