//-----------------------------------------------------------------------------
// Copyright © 2014 Win Systems Ltd. 
//-----------------------------------------------------------------------------
//
// MODULE NAME:   uc_button.cs
// DESCRIPTION:   This component allows to customize buttons.
// AUTHOR:        Javier Fernández
// CREATION DATE: 06-JUN-2014
// 
// REVISION HISTORY:
//
// Date         Author Description
// -----------  ------ ---------------------------------------------------------
// 06-JUN-2014  JFC    Initial version
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace WSI.Cashier
{
  public partial class uc_button : Button
  {
    private Color _default_color = Color.PaleTurquoise;
    private const string _default_color_string = "PaleTurquoise";

    public uc_button()
    {
      InitializeComponent();
    }

    public override void ResetBackColor()
    {
      this.BackColor = this._default_color;
    }

    [DefaultValue(typeof(Color), _default_color_string)]
    public override Color BackColor
    {
      get
      {
        return base.BackColor;
      }
      set
      {
        base.BackColor = value;
      }
    }

    // There is a problem with UseVisualStyleBackColor property.
    // When we drag a button on a form, this property is always setting to true,
    // and then the button is painted with the system color 
    // instead of color defined in BackColor property.
    // For this reason, we are shadowing this property to returns always false.

    [DefaultValue(false)]
    public new bool UseVisualStyleBackColor
    {
      get
      {
        return false;
      }
      set
      {
      }
    }
  }
}
