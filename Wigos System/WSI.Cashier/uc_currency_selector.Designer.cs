﻿namespace WSI.Cashier
{
  partial class uc_currency_selector
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.uc_round_panel = new WSI.Cashier.Controls.uc_round_panel();
      this.btn_selected_currency = new WSI.Cashier.Controls.uc_round_button();
      this.lbl_currency_iso_code = new WSI.Cashier.Controls.uc_label();
      this.lbl_paymet_method = new WSI.Cashier.Controls.uc_label();
      this.pb_currency = new System.Windows.Forms.PictureBox();
      this.uc_round_panel.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pb_currency)).BeginInit();
      this.SuspendLayout();
      // 
      // uc_round_panel
      // 
      this.uc_round_panel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.uc_round_panel.BackColor = System.Drawing.Color.Transparent;
      this.uc_round_panel.BorderColor = System.Drawing.Color.Empty;
      this.uc_round_panel.Controls.Add(this.btn_selected_currency);
      this.uc_round_panel.Controls.Add(this.lbl_currency_iso_code);
      this.uc_round_panel.Controls.Add(this.lbl_paymet_method);
      this.uc_round_panel.Controls.Add(this.pb_currency);
      this.uc_round_panel.CornerRadius = 10;
      this.uc_round_panel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.uc_round_panel.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.uc_round_panel.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.uc_round_panel.HeaderHeight = 35;
      this.uc_round_panel.HeaderSubText = null;
      this.uc_round_panel.HeaderText = "XTITLE";
      this.uc_round_panel.Location = new System.Drawing.Point(0, 0);
      this.uc_round_panel.Name = "uc_round_panel";
      this.uc_round_panel.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.uc_round_panel.Size = new System.Drawing.Size(375, 106);
      this.uc_round_panel.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.uc_round_panel.TabIndex = 51;
      // 
      // btn_selected_currency
      // 
      this.btn_selected_currency.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_selected_currency.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_selected_currency.FlatAppearance.BorderSize = 0;
      this.btn_selected_currency.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_selected_currency.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_selected_currency.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_selected_currency.Image = null;
      this.btn_selected_currency.IsSelected = false;
      this.btn_selected_currency.Location = new System.Drawing.Point(233, 45);
      this.btn_selected_currency.Name = "btn_selected_currency";
      this.btn_selected_currency.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_selected_currency.Size = new System.Drawing.Size(130, 50);
      this.btn_selected_currency.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_selected_currency.TabIndex = 48;
      this.btn_selected_currency.Text = "XCHANGE";
      this.btn_selected_currency.UseVisualStyleBackColor = false;
      this.btn_selected_currency.Click += new System.EventHandler(this.btn_selected_currency_Click);
      // 
      // lbl_currency_iso_code
      // 
      this.lbl_currency_iso_code.AutoSize = true;
      this.lbl_currency_iso_code.BackColor = System.Drawing.Color.Transparent;
      this.lbl_currency_iso_code.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_currency_iso_code.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_currency_iso_code.Location = new System.Drawing.Point(18, 83);
      this.lbl_currency_iso_code.Name = "lbl_currency_iso_code";
      this.lbl_currency_iso_code.Size = new System.Drawing.Size(35, 17);
      this.lbl_currency_iso_code.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_currency_iso_code.TabIndex = 49;
      this.lbl_currency_iso_code.Text = "xISO";
      // 
      // lbl_paymet_method
      // 
      this.lbl_paymet_method.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_paymet_method.BackColor = System.Drawing.Color.Transparent;
      this.lbl_paymet_method.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_paymet_method.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_paymet_method.Location = new System.Drawing.Point(71, 44);
      this.lbl_paymet_method.Name = "lbl_paymet_method";
      this.lbl_paymet_method.Size = new System.Drawing.Size(158, 47);
      this.lbl_paymet_method.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_paymet_method.TabIndex = 47;
      this.lbl_paymet_method.Text = "xCurrency";
      this.lbl_paymet_method.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // pb_currency
      // 
      this.pb_currency.BackColor = System.Drawing.Color.Transparent;
      this.pb_currency.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.pb_currency.Location = new System.Drawing.Point(15, 45);
      this.pb_currency.Name = "pb_currency";
      this.pb_currency.Size = new System.Drawing.Size(36, 36);
      this.pb_currency.TabIndex = 50;
      this.pb_currency.TabStop = false;
      // 
      // uc_currency_selector
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.uc_round_panel);
      this.Name = "uc_currency_selector";
      this.Size = new System.Drawing.Size(375, 106);
      this.Load += new System.EventHandler(this.uc_currency_selector_Load);
      this.uc_round_panel.ResumeLayout(false);
      this.uc_round_panel.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pb_currency)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private Controls.uc_label lbl_currency_iso_code;
    private System.Windows.Forms.PictureBox pb_currency;
    private Controls.uc_round_button btn_selected_currency;
    private Controls.uc_label lbl_paymet_method;
    private Controls.uc_round_panel uc_round_panel;

  }
}
