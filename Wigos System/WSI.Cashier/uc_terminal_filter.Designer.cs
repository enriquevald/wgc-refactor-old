namespace WSI.Cashier
{
  partial class uc_terminal_filter
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.splitContainer1 = new System.Windows.Forms.SplitContainer();
      this.dgv_providers = new WSI.Cashier.Controls.uc_DataGridView();
      this.xProviders = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dgv_terminals = new WSI.Cashier.Controls.uc_DataGridView();
      this.xTerminalID = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.xTerminalName = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.xTerminalExternal = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.xTerminalProvider = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.splitContainer1.Panel1.SuspendLayout();
      this.splitContainer1.Panel2.SuspendLayout();
      this.splitContainer1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dgv_providers)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.dgv_terminals)).BeginInit();
      this.SuspendLayout();
      // 
      // splitContainer1
      // 
      this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splitContainer1.Location = new System.Drawing.Point(0, 0);
      this.splitContainer1.Margin = new System.Windows.Forms.Padding(0);
      this.splitContainer1.Name = "splitContainer1";
      // 
      // splitContainer1.Panel1
      // 
      this.splitContainer1.Panel1.Controls.Add(this.dgv_providers);
      // 
      // splitContainer1.Panel2
      // 
      this.splitContainer1.Panel2.Controls.Add(this.dgv_terminals);
      this.splitContainer1.Panel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
      this.splitContainer1.Size = new System.Drawing.Size(581, 170);
      this.splitContainer1.SplitterDistance = 290;
      this.splitContainer1.SplitterWidth = 1;
      this.splitContainer1.TabIndex = 1;
      // 
      // dgv_providers
      // 
      this.dgv_providers.AllowUserToAddRows = false;
      this.dgv_providers.AllowUserToDeleteRows = false;
      this.dgv_providers.AllowUserToResizeColumns = false;
      this.dgv_providers.AllowUserToResizeRows = false;
      this.dgv_providers.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.dgv_providers.ColumnHeaderHeight = 35;
      this.dgv_providers.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
      this.dgv_providers.ColumnHeadersHeight = 35;
      this.dgv_providers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
      this.dgv_providers.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.xProviders});
      this.dgv_providers.CornerRadius = 10;
      this.dgv_providers.DefaultCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.dgv_providers.DefaultCellForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.dgv_providers.Dock = System.Windows.Forms.DockStyle.Fill;
      this.dgv_providers.EnableHeadersVisualStyles = false;
      this.dgv_providers.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.dgv_providers.GridColor = System.Drawing.Color.LightGray;
      this.dgv_providers.HeaderBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
      this.dgv_providers.HeaderDefaultCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.dgv_providers.HeaderDefaultCellForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.dgv_providers.Location = new System.Drawing.Point(0, 0);
      this.dgv_providers.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
      this.dgv_providers.MultiSelect = false;
      this.dgv_providers.Name = "dgv_providers";
      this.dgv_providers.ReadOnly = true;
      this.dgv_providers.RowHeadersVisible = false;
      this.dgv_providers.RowTemplate.Height = 35;
      this.dgv_providers.RowTemplateHeight = 35;
      this.dgv_providers.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
      this.dgv_providers.ShowEditingIcon = false;
      this.dgv_providers.Size = new System.Drawing.Size(290, 170);
      this.dgv_providers.Style = WSI.Cashier.Controls.uc_DataGridView.DataGridViewStyle.WITH_BOTTOM_ROUND_BORDERS;
      this.dgv_providers.TabIndex = 0;
      this.dgv_providers.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dgv_terminals_CellValidating);
      this.dgv_providers.SelectionChanged += new System.EventHandler(this.dgv_providers_SelectionChanged);
      // 
      // xProviders
      // 
      this.xProviders.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
      this.xProviders.HeaderText = "xProviders";
      this.xProviders.Name = "xProviders";
      this.xProviders.ReadOnly = true;
      this.xProviders.Resizable = System.Windows.Forms.DataGridViewTriState.False;
      this.xProviders.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
      // 
      // dgv_terminals
      // 
      this.dgv_terminals.AllowUserToAddRows = false;
      this.dgv_terminals.AllowUserToDeleteRows = false;
      this.dgv_terminals.AllowUserToOrderColumns = true;
      this.dgv_terminals.AllowUserToResizeColumns = false;
      this.dgv_terminals.AllowUserToResizeRows = false;
      this.dgv_terminals.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.dgv_terminals.ColumnHeaderHeight = 35;
      this.dgv_terminals.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
      this.dgv_terminals.ColumnHeadersHeight = 35;
      this.dgv_terminals.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
      this.dgv_terminals.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.xTerminalID,
            this.xTerminalName,
            this.xTerminalExternal,
            this.xTerminalProvider});
      this.dgv_terminals.CornerRadius = 10;
      this.dgv_terminals.DefaultCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.dgv_terminals.DefaultCellForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.dgv_terminals.Dock = System.Windows.Forms.DockStyle.Fill;
      this.dgv_terminals.EnableHeadersVisualStyles = false;
      this.dgv_terminals.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.dgv_terminals.GridColor = System.Drawing.Color.LightGray;
      this.dgv_terminals.HeaderBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
      this.dgv_terminals.HeaderDefaultCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.dgv_terminals.HeaderDefaultCellForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.dgv_terminals.Location = new System.Drawing.Point(0, 0);
      this.dgv_terminals.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
      this.dgv_terminals.MultiSelect = false;
      this.dgv_terminals.Name = "dgv_terminals";
      this.dgv_terminals.ReadOnly = true;
      this.dgv_terminals.RowHeadersVisible = false;
      this.dgv_terminals.RowTemplate.Height = 35;
      this.dgv_terminals.RowTemplateHeight = 35;
      this.dgv_terminals.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
      this.dgv_terminals.ShowEditingIcon = false;
      this.dgv_terminals.Size = new System.Drawing.Size(290, 170);
      this.dgv_terminals.Style = WSI.Cashier.Controls.uc_DataGridView.DataGridViewStyle.WITH_BOTTOM_ROUND_BORDERS;
      this.dgv_terminals.TabIndex = 0;
      this.dgv_terminals.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dgv_terminals_CellValidating);
      this.dgv_terminals.SelectionChanged += new System.EventHandler(this.dgv_terminals_SelectionChanged);
      // 
      // xTerminalID
      // 
      this.xTerminalID.HeaderText = "xTerminalID";
      this.xTerminalID.Name = "xTerminalID";
      this.xTerminalID.ReadOnly = true;
      this.xTerminalID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
      this.xTerminalID.Visible = false;
      // 
      // xTerminalName
      // 
      this.xTerminalName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
      this.xTerminalName.HeaderText = "xTerminalName";
      this.xTerminalName.Name = "xTerminalName";
      this.xTerminalName.ReadOnly = true;
      this.xTerminalName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
      // 
      // xTerminalExternal
      // 
      this.xTerminalExternal.HeaderText = "xTerminalExternal";
      this.xTerminalExternal.Name = "xTerminalExternal";
      this.xTerminalExternal.ReadOnly = true;
      this.xTerminalExternal.Visible = false;
      // 
      // xTerminalProvider
      // 
      this.xTerminalProvider.HeaderText = "xTerminalProvider";
      this.xTerminalProvider.Name = "xTerminalProvider";
      this.xTerminalProvider.ReadOnly = true;
      this.xTerminalProvider.Visible = false;
      // 
      // uc_terminal_filter
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 9F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.Color.Transparent;
      this.Controls.Add(this.splitContainer1);
      this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(2)));
      this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
      this.Name = "uc_terminal_filter";
      this.Size = new System.Drawing.Size(581, 170);
      this.splitContainer1.Panel1.ResumeLayout(false);
      this.splitContainer1.Panel2.ResumeLayout(false);
      this.splitContainer1.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.dgv_providers)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.dgv_terminals)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion


    private WSI.Cashier.Controls.uc_DataGridView dgv_providers;
    private WSI.Cashier.Controls.uc_DataGridView dgv_terminals; 
    private System.Windows.Forms.DataGridViewTextBoxColumn xProviders;
    private System.Windows.Forms.DataGridViewTextBoxColumn xTerminalID;
    private System.Windows.Forms.DataGridViewTextBoxColumn xTerminalName;
    private System.Windows.Forms.DataGridViewTextBoxColumn xTerminalExternal;
    private System.Windows.Forms.DataGridViewTextBoxColumn xTerminalProvider;
    private System.Windows.Forms.SplitContainer splitContainer1;
  }
}
