namespace WSI.Cashier
{
  partial class uc_options
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.pb_options = new System.Windows.Forms.PictureBox();
      this.label3 = new System.Windows.Forms.Label();
      this.gb_pinpad_panel = new WSI.Cashier.Controls.uc_round_panel();
      this.btn_pinpad_config = new WSI.Cashier.Controls.uc_round_button();
      this.btn_pinpad_disable = new WSI.Cashier.Controls.uc_round_button();
      this.btn_pinpad_enable = new WSI.Cashier.Controls.uc_round_button();
      this.uc_title = new WSI.Cashier.Controls.uc_label();
      this.lbl_separator_1 = new WSI.Cashier.Controls.uc_label();
      this.label1 = new WSI.Cashier.Controls.uc_label();
      this.groupBox1 = new WSI.Cashier.Controls.uc_round_panel();
      this.pictureBox1 = new System.Windows.Forms.PictureBox();
      this.btn_scan_disable = new WSI.Cashier.Controls.uc_round_button();
      this.btn_scan_enable = new WSI.Cashier.Controls.uc_round_button();
      this.groupBox2 = new WSI.Cashier.Controls.uc_round_panel();
      this.lbl_resolution_text = new WSI.Cashier.Controls.uc_label();
      this.lbl_resolution = new WSI.Cashier.Controls.uc_label();
      this.lbl_cpu_info = new WSI.Cashier.Controls.uc_label();
      this.lbl_architecture = new WSI.Cashier.Controls.uc_label();
      this.lbl_os_version = new WSI.Cashier.Controls.uc_label();
      this.lbl_os_version_text = new WSI.Cashier.Controls.uc_label();
      this.lbl_mem = new WSI.Cashier.Controls.uc_label();
      this.lbl_mem_text = new WSI.Cashier.Controls.uc_label();
      this.pb_sys_info = new System.Windows.Forms.PictureBox();
      this.gb_calibrate_touch = new WSI.Cashier.Controls.uc_round_panel();
      this.lbl_screen_resolution = new WSI.Cashier.Controls.uc_label();
      this.cb_screen_resolutions = new WSI.Cashier.Controls.uc_round_combobox();
      this.btn_reception_mode = new WSI.Cashier.Controls.uc_round_button();
      this.btn_table_mode = new WSI.Cashier.Controls.uc_round_button();
      this.pb_calibrate = new System.Windows.Forms.PictureBox();
      this.btn_calibrate = new WSI.Cashier.Controls.uc_round_button();
      this.btn_window_mode = new WSI.Cashier.Controls.uc_round_button();
      this.panel1 = new WSI.Cashier.Controls.uc_round_panel();
      this.lbl_splitline = new System.Windows.Forms.Label();
      this.label2 = new WSI.Cashier.Controls.uc_label();
      this.lbl_version_info_txt = new WSI.Cashier.Controls.uc_label();
      this.lbl_cashier_version = new WSI.Cashier.Controls.uc_label();
      this.pb_logo = new System.Windows.Forms.PictureBox();
      this.gb_lang_selector = new WSI.Cashier.Controls.uc_round_panel();
      this.pb_osk_logo = new System.Windows.Forms.PictureBox();
      this.btn_osk_disable = new WSI.Cashier.Controls.uc_round_button();
      this.btn_osk_enable = new WSI.Cashier.Controls.uc_round_button();
      this.pnl_db_config = new WSI.Cashier.Controls.uc_round_panel();
      ((System.ComponentModel.ISupportInitialize)(this.pb_options)).BeginInit();
      this.gb_pinpad_panel.SuspendLayout();
      this.groupBox1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
      this.groupBox2.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pb_sys_info)).BeginInit();
      this.gb_calibrate_touch.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pb_calibrate)).BeginInit();
      this.panel1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pb_logo)).BeginInit();
      this.gb_lang_selector.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pb_osk_logo)).BeginInit();
      this.SuspendLayout();
      // 
      // pb_options
      // 
      this.pb_options.Location = new System.Drawing.Point(10, 5);
      this.pb_options.Name = "pb_options";
      this.pb_options.Size = new System.Drawing.Size(50, 50);
      this.pb_options.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
      this.pb_options.TabIndex = 91;
      this.pb_options.TabStop = false;
      // 
      // label3
      // 
      this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.label3.BackColor = System.Drawing.Color.Black;
      this.label3.Location = new System.Drawing.Point(-1, 60);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(885, 1);
      this.label3.TabIndex = 0;
      // 
      // gb_pinpad_panel
      // 
      this.gb_pinpad_panel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.gb_pinpad_panel.BackColor = System.Drawing.Color.Transparent;
      this.gb_pinpad_panel.BorderColor = System.Drawing.Color.Empty;
      this.gb_pinpad_panel.Controls.Add(this.btn_pinpad_config);
      this.gb_pinpad_panel.Controls.Add(this.btn_pinpad_disable);
      this.gb_pinpad_panel.Controls.Add(this.btn_pinpad_enable);
      this.gb_pinpad_panel.CornerRadius = 10;
      this.gb_pinpad_panel.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_pinpad_panel.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gb_pinpad_panel.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_pinpad_panel.HeaderHeight = 35;
      this.gb_pinpad_panel.HeaderSubText = null;
      this.gb_pinpad_panel.HeaderText = "#HEADERTEXT";
      this.gb_pinpad_panel.Location = new System.Drawing.Point(446, 603);
      this.gb_pinpad_panel.Name = "gb_pinpad_panel";
      this.gb_pinpad_panel.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_pinpad_panel.Size = new System.Drawing.Size(425, 108);
      this.gb_pinpad_panel.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.gb_pinpad_panel.TabIndex = 103;
      // 
      // btn_pinpad_config
      // 
      this.btn_pinpad_config.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_pinpad_config.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_pinpad_config.FlatAppearance.BorderSize = 0;
      this.btn_pinpad_config.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_pinpad_config.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_pinpad_config.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_pinpad_config.Image = null;
      this.btn_pinpad_config.IsSelected = false;
      this.btn_pinpad_config.Location = new System.Drawing.Point(58, 47);
      this.btn_pinpad_config.Name = "btn_pinpad_config";
      this.btn_pinpad_config.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_pinpad_config.Size = new System.Drawing.Size(115, 50);
      this.btn_pinpad_config.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_pinpad_config.TabIndex = 98;
      this.btn_pinpad_config.Text = "XCONFIG";
      this.btn_pinpad_config.UseVisualStyleBackColor = false;
      this.btn_pinpad_config.Click += new System.EventHandler(this.btn_pinpad_config_Click);
      // 
      // btn_pinpad_disable
      // 
      this.btn_pinpad_disable.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_pinpad_disable.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_pinpad_disable.FlatAppearance.BorderSize = 0;
      this.btn_pinpad_disable.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_pinpad_disable.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_pinpad_disable.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_pinpad_disable.Image = null;
      this.btn_pinpad_disable.IsSelected = false;
      this.btn_pinpad_disable.Location = new System.Drawing.Point(302, 47);
      this.btn_pinpad_disable.Name = "btn_pinpad_disable";
      this.btn_pinpad_disable.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_pinpad_disable.Size = new System.Drawing.Size(115, 50);
      this.btn_pinpad_disable.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_pinpad_disable.TabIndex = 97;
      this.btn_pinpad_disable.Text = "XDISABLE";
      this.btn_pinpad_disable.UseVisualStyleBackColor = false;
      this.btn_pinpad_disable.Click += new System.EventHandler(this.btn_pinpad_disable_Click);
      // 
      // btn_pinpad_enable
      // 
      this.btn_pinpad_enable.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_pinpad_enable.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_pinpad_enable.FlatAppearance.BorderSize = 0;
      this.btn_pinpad_enable.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_pinpad_enable.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_pinpad_enable.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_pinpad_enable.Image = null;
      this.btn_pinpad_enable.IsSelected = false;
      this.btn_pinpad_enable.Location = new System.Drawing.Point(180, 47);
      this.btn_pinpad_enable.Name = "btn_pinpad_enable";
      this.btn_pinpad_enable.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_pinpad_enable.Size = new System.Drawing.Size(115, 50);
      this.btn_pinpad_enable.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_pinpad_enable.TabIndex = 96;
      this.btn_pinpad_enable.Text = "XENABLE";
      this.btn_pinpad_enable.UseVisualStyleBackColor = false;
      this.btn_pinpad_enable.Click += new System.EventHandler(this.btn_pinpad_enable_Click);
      // 
      // uc_title
      // 
      this.uc_title.AutoSize = true;
      this.uc_title.BackColor = System.Drawing.Color.Transparent;
      this.uc_title.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.uc_title.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.uc_title.Location = new System.Drawing.Point(65, 20);
      this.uc_title.Name = "uc_title";
      this.uc_title.Size = new System.Drawing.Size(51, 22);
      this.uc_title.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.uc_title.TabIndex = 102;
      this.uc_title.Text = "TITLE";
      // 
      // lbl_separator_1
      // 
      this.lbl_separator_1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_separator_1.BackColor = System.Drawing.Color.Transparent;
      this.lbl_separator_1.Font = new System.Drawing.Font("Open Sans Semibold", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_separator_1.ForeColor = System.Drawing.SystemColors.ControlText;
      this.lbl_separator_1.Location = new System.Drawing.Point(-3, 60);
      this.lbl_separator_1.Name = "lbl_separator_1";
      this.lbl_separator_1.Size = new System.Drawing.Size(884, 1);
      this.lbl_separator_1.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.lbl_separator_1.TabIndex = 92;
      // 
      // label1
      // 
      this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.label1.BackColor = System.Drawing.Color.Transparent;
      this.label1.Font = new System.Drawing.Font("Open Sans Semibold", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.label1.Location = new System.Drawing.Point(-3, 60);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(894, 1);
      this.label1.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.label1.TabIndex = 93;
      // 
      // groupBox1
      // 
      this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.groupBox1.BackColor = System.Drawing.Color.Transparent;
      this.groupBox1.BorderColor = System.Drawing.Color.Empty;
      this.groupBox1.Controls.Add(this.pictureBox1);
      this.groupBox1.Controls.Add(this.btn_scan_disable);
      this.groupBox1.Controls.Add(this.btn_scan_enable);
      this.groupBox1.CornerRadius = 10;
      this.groupBox1.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.groupBox1.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.groupBox1.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.groupBox1.HeaderHeight = 35;
      this.groupBox1.HeaderSubText = null;
      this.groupBox1.HeaderText = "#HEADERTEXT";
      this.groupBox1.Location = new System.Drawing.Point(10, 603);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.groupBox1.Size = new System.Drawing.Size(425, 108);
      this.groupBox1.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.groupBox1.TabIndex = 4;
      // 
      // pictureBox1
      // 
      this.pictureBox1.Location = new System.Drawing.Point(11, 47);
      this.pictureBox1.Name = "pictureBox1";
      this.pictureBox1.Size = new System.Drawing.Size(50, 50);
      this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
      this.pictureBox1.TabIndex = 95;
      this.pictureBox1.TabStop = false;
      // 
      // btn_scan_disable
      // 
      this.btn_scan_disable.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_scan_disable.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_scan_disable.FlatAppearance.BorderSize = 0;
      this.btn_scan_disable.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_scan_disable.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_scan_disable.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_scan_disable.Image = null;
      this.btn_scan_disable.IsSelected = false;
      this.btn_scan_disable.Location = new System.Drawing.Point(300, 47);
      this.btn_scan_disable.Name = "btn_scan_disable";
      this.btn_scan_disable.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_scan_disable.Size = new System.Drawing.Size(115, 50);
      this.btn_scan_disable.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_scan_disable.TabIndex = 1;
      this.btn_scan_disable.Text = "XDISABLE";
      this.btn_scan_disable.UseVisualStyleBackColor = false;
      this.btn_scan_disable.Click += new System.EventHandler(this.btn_scan_disable_Click);
      // 
      // btn_scan_enable
      // 
      this.btn_scan_enable.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_scan_enable.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_scan_enable.FlatAppearance.BorderSize = 0;
      this.btn_scan_enable.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_scan_enable.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_scan_enable.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_scan_enable.Image = null;
      this.btn_scan_enable.IsSelected = false;
      this.btn_scan_enable.Location = new System.Drawing.Point(178, 47);
      this.btn_scan_enable.Name = "btn_scan_enable";
      this.btn_scan_enable.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_scan_enable.Size = new System.Drawing.Size(115, 50);
      this.btn_scan_enable.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_scan_enable.TabIndex = 0;
      this.btn_scan_enable.Text = "XENABLE";
      this.btn_scan_enable.UseVisualStyleBackColor = false;
      this.btn_scan_enable.Click += new System.EventHandler(this.btn_scan_enable_Click);
      // 
      // groupBox2
      // 
      this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.groupBox2.BackColor = System.Drawing.Color.Transparent;
      this.groupBox2.BorderColor = System.Drawing.Color.Empty;
      this.groupBox2.Controls.Add(this.lbl_resolution_text);
      this.groupBox2.Controls.Add(this.lbl_resolution);
      this.groupBox2.Controls.Add(this.lbl_cpu_info);
      this.groupBox2.Controls.Add(this.lbl_architecture);
      this.groupBox2.Controls.Add(this.lbl_os_version);
      this.groupBox2.Controls.Add(this.lbl_os_version_text);
      this.groupBox2.Controls.Add(this.lbl_mem);
      this.groupBox2.Controls.Add(this.lbl_mem_text);
      this.groupBox2.Controls.Add(this.pb_sys_info);
      this.groupBox2.CornerRadius = 10;
      this.groupBox2.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.groupBox2.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.groupBox2.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.groupBox2.HeaderHeight = 35;
      this.groupBox2.HeaderSubText = null;
      this.groupBox2.HeaderText = "#HEADERTEXT";
      this.groupBox2.Location = new System.Drawing.Point(446, 322);
      this.groupBox2.Name = "groupBox2";
      this.groupBox2.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.groupBox2.Size = new System.Drawing.Size(425, 273);
      this.groupBox2.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.groupBox2.TabIndex = 7;
      // 
      // lbl_resolution_text
      // 
      this.lbl_resolution_text.BackColor = System.Drawing.Color.Transparent;
      this.lbl_resolution_text.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_resolution_text.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_resolution_text.Location = new System.Drawing.Point(20, 147);
      this.lbl_resolution_text.Name = "lbl_resolution_text";
      this.lbl_resolution_text.Size = new System.Drawing.Size(155, 23);
      this.lbl_resolution_text.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_resolution_text.TabIndex = 6;
      this.lbl_resolution_text.Text = "Screen Resolution";
      this.lbl_resolution_text.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_resolution
      // 
      this.lbl_resolution.AutoSize = true;
      this.lbl_resolution.BackColor = System.Drawing.Color.Transparent;
      this.lbl_resolution.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_resolution.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_resolution.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.lbl_resolution.Location = new System.Drawing.Point(184, 151);
      this.lbl_resolution.Name = "lbl_resolution";
      this.lbl_resolution.Size = new System.Drawing.Size(111, 17);
      this.lbl_resolution.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_resolution.TabIndex = 7;
      this.lbl_resolution.Text = "LBL_RESOLUTION";
      // 
      // lbl_cpu_info
      // 
      this.lbl_cpu_info.BackColor = System.Drawing.Color.Transparent;
      this.lbl_cpu_info.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_cpu_info.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_cpu_info.Location = new System.Drawing.Point(41, 175);
      this.lbl_cpu_info.Name = "lbl_cpu_info";
      this.lbl_cpu_info.Size = new System.Drawing.Size(134, 23);
      this.lbl_cpu_info.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_cpu_info.TabIndex = 4;
      this.lbl_cpu_info.Text = "CPU Information";
      this.lbl_cpu_info.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_architecture
      // 
      this.lbl_architecture.BackColor = System.Drawing.Color.Transparent;
      this.lbl_architecture.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_architecture.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_architecture.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.lbl_architecture.Location = new System.Drawing.Point(184, 177);
      this.lbl_architecture.Name = "lbl_architecture";
      this.lbl_architecture.Size = new System.Drawing.Size(218, 87);
      this.lbl_architecture.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_architecture.TabIndex = 5;
      this.lbl_architecture.Text = "label5";
      // 
      // lbl_os_version
      // 
      this.lbl_os_version.AutoSize = true;
      this.lbl_os_version.BackColor = System.Drawing.Color.Transparent;
      this.lbl_os_version.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_os_version.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_os_version.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.lbl_os_version.Location = new System.Drawing.Point(184, 102);
      this.lbl_os_version.Name = "lbl_os_version";
      this.lbl_os_version.Size = new System.Drawing.Size(39, 17);
      this.lbl_os_version.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_os_version.TabIndex = 1;
      this.lbl_os_version.Text = "lbl_os";
      // 
      // lbl_os_version_text
      // 
      this.lbl_os_version_text.BackColor = System.Drawing.Color.Transparent;
      this.lbl_os_version_text.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_os_version_text.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_os_version_text.Location = new System.Drawing.Point(23, 98);
      this.lbl_os_version_text.Name = "lbl_os_version_text";
      this.lbl_os_version_text.Size = new System.Drawing.Size(152, 23);
      this.lbl_os_version_text.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_os_version_text.TabIndex = 0;
      this.lbl_os_version_text.Text = "Sistema Operativo";
      this.lbl_os_version_text.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_mem
      // 
      this.lbl_mem.AutoSize = true;
      this.lbl_mem.BackColor = System.Drawing.Color.Transparent;
      this.lbl_mem.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_mem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_mem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.lbl_mem.Location = new System.Drawing.Point(184, 127);
      this.lbl_mem.Name = "lbl_mem";
      this.lbl_mem.Size = new System.Drawing.Size(42, 17);
      this.lbl_mem.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_mem.TabIndex = 3;
      this.lbl_mem.Text = "label5";
      // 
      // lbl_mem_text
      // 
      this.lbl_mem_text.BackColor = System.Drawing.Color.Transparent;
      this.lbl_mem_text.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_mem_text.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_mem_text.Location = new System.Drawing.Point(38, 123);
      this.lbl_mem_text.Name = "lbl_mem_text";
      this.lbl_mem_text.Size = new System.Drawing.Size(137, 23);
      this.lbl_mem_text.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_mem_text.TabIndex = 2;
      this.lbl_mem_text.Text = "Total Memory";
      this.lbl_mem_text.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // pb_sys_info
      // 
      this.pb_sys_info.Location = new System.Drawing.Point(6, 44);
      this.pb_sys_info.Name = "pb_sys_info";
      this.pb_sys_info.Size = new System.Drawing.Size(50, 50);
      this.pb_sys_info.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
      this.pb_sys_info.TabIndex = 96;
      this.pb_sys_info.TabStop = false;
      // 
      // gb_calibrate_touch
      // 
      this.gb_calibrate_touch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.gb_calibrate_touch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.gb_calibrate_touch.BorderColor = System.Drawing.Color.Empty;
      this.gb_calibrate_touch.Controls.Add(this.lbl_screen_resolution);
      this.gb_calibrate_touch.Controls.Add(this.cb_screen_resolutions);
      this.gb_calibrate_touch.Controls.Add(this.btn_reception_mode);
      this.gb_calibrate_touch.Controls.Add(this.btn_table_mode);
      this.gb_calibrate_touch.Controls.Add(this.pb_calibrate);
      this.gb_calibrate_touch.Controls.Add(this.btn_calibrate);
      this.gb_calibrate_touch.Controls.Add(this.btn_window_mode);
      this.gb_calibrate_touch.CornerRadius = 10;
      this.gb_calibrate_touch.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_calibrate_touch.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.gb_calibrate_touch.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gb_calibrate_touch.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_calibrate_touch.HeaderHeight = 35;
      this.gb_calibrate_touch.HeaderSubText = null;
      this.gb_calibrate_touch.HeaderText = "#HEADERTEXT";
      this.gb_calibrate_touch.Location = new System.Drawing.Point(10, 441);
      this.gb_calibrate_touch.Name = "gb_calibrate_touch";
      this.gb_calibrate_touch.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_calibrate_touch.Size = new System.Drawing.Size(425, 154);
      this.gb_calibrate_touch.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.gb_calibrate_touch.TabIndex = 3;
      // 
      // lbl_screen_resolution
      // 
      this.lbl_screen_resolution.BackColor = System.Drawing.Color.Transparent;
      this.lbl_screen_resolution.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_screen_resolution.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_screen_resolution.Location = new System.Drawing.Point(3, 94);
      this.lbl_screen_resolution.Name = "lbl_screen_resolution";
      this.lbl_screen_resolution.Size = new System.Drawing.Size(153, 23);
      this.lbl_screen_resolution.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_screen_resolution.TabIndex = 99;
      this.lbl_screen_resolution.Text = "xWindow Size";
      this.lbl_screen_resolution.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // cb_screen_resolutions
      // 
      this.cb_screen_resolutions.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_screen_resolutions.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.cb_screen_resolutions.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.cb_screen_resolutions.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.cb_screen_resolutions.CornerRadius = 5;
      this.cb_screen_resolutions.DataSource = null;
      this.cb_screen_resolutions.DisplayMember = "";
      this.cb_screen_resolutions.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
      this.cb_screen_resolutions.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cb_screen_resolutions.DropDownWidth = 153;
      this.cb_screen_resolutions.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.cb_screen_resolutions.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_screen_resolutions.FormattingEnabled = false;
      this.cb_screen_resolutions.IsDroppedDown = false;
      this.cb_screen_resolutions.ItemHeight = 40;
      this.cb_screen_resolutions.Location = new System.Drawing.Point(3, 118);
      this.cb_screen_resolutions.MaxDropDownItems = 8;
      this.cb_screen_resolutions.Name = "cb_screen_resolutions";
      this.cb_screen_resolutions.OnFocusOpenListBox = true;
      this.cb_screen_resolutions.SelectedIndex = -1;
      this.cb_screen_resolutions.SelectedItem = null;
      this.cb_screen_resolutions.SelectedValue = null;
      this.cb_screen_resolutions.SelectionLength = 0;
      this.cb_screen_resolutions.SelectionStart = 0;
      this.cb_screen_resolutions.Size = new System.Drawing.Size(153, 30);
      this.cb_screen_resolutions.Sorted = false;
      this.cb_screen_resolutions.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.CUSTOM;
      this.cb_screen_resolutions.TabIndex = 97;
      this.cb_screen_resolutions.TabStop = false;
      this.cb_screen_resolutions.ValueMember = "";
      this.cb_screen_resolutions.SelectedIndexChanged += new System.EventHandler(this.cb_resolutions_SelectedIndexChanged);
      // 
      // btn_reception_mode
      // 
      this.btn_reception_mode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_reception_mode.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_reception_mode.FlatAppearance.BorderSize = 0;
      this.btn_reception_mode.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_reception_mode.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_reception_mode.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_reception_mode.Image = null;
      this.btn_reception_mode.IsSelected = false;
      this.btn_reception_mode.Location = new System.Drawing.Point(300, 98);
      this.btn_reception_mode.Name = "btn_reception_mode";
      this.btn_reception_mode.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_reception_mode.Size = new System.Drawing.Size(115, 50);
      this.btn_reception_mode.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_reception_mode.TabIndex = 98;
      this.btn_reception_mode.Text = "XRECEPTIONMODECHANGE";
      this.btn_reception_mode.UseVisualStyleBackColor = false;
      this.btn_reception_mode.Click += new System.EventHandler(this.btn_reception_mode_Click);
      // 
      // btn_table_mode
      // 
      this.btn_table_mode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_table_mode.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_table_mode.FlatAppearance.BorderSize = 0;
      this.btn_table_mode.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_table_mode.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_table_mode.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_table_mode.Image = null;
      this.btn_table_mode.IsSelected = false;
      this.btn_table_mode.Location = new System.Drawing.Point(178, 98);
      this.btn_table_mode.Name = "btn_table_mode";
      this.btn_table_mode.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_table_mode.Size = new System.Drawing.Size(115, 50);
      this.btn_table_mode.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_table_mode.TabIndex = 1;
      this.btn_table_mode.Text = "XTABLEMODECHANGE";
      this.btn_table_mode.UseVisualStyleBackColor = false;
      this.btn_table_mode.Click += new System.EventHandler(this.btn_table_mode_Click);
      // 
      // pb_calibrate
      // 
      this.pb_calibrate.BackColor = System.Drawing.Color.Transparent;
      this.pb_calibrate.Location = new System.Drawing.Point(11, 44);
      this.pb_calibrate.Name = "pb_calibrate";
      this.pb_calibrate.Size = new System.Drawing.Size(50, 50);
      this.pb_calibrate.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
      this.pb_calibrate.TabIndex = 95;
      this.pb_calibrate.TabStop = false;
      // 
      // btn_calibrate
      // 
      this.btn_calibrate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_calibrate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_calibrate.FlatAppearance.BorderSize = 0;
      this.btn_calibrate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_calibrate.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_calibrate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_calibrate.Image = null;
      this.btn_calibrate.IsSelected = false;
      this.btn_calibrate.Location = new System.Drawing.Point(178, 44);
      this.btn_calibrate.Name = "btn_calibrate";
      this.btn_calibrate.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_calibrate.Size = new System.Drawing.Size(115, 50);
      this.btn_calibrate.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_calibrate.TabIndex = 2;
      this.btn_calibrate.Text = "XCALIBRATE";
      this.btn_calibrate.UseVisualStyleBackColor = false;
      this.btn_calibrate.Click += new System.EventHandler(this.btn_calibrate_Click);
      // 
      // btn_window_mode
      // 
      this.btn_window_mode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_window_mode.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_window_mode.FlatAppearance.BorderSize = 0;
      this.btn_window_mode.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_window_mode.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_window_mode.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_window_mode.Image = null;
      this.btn_window_mode.IsSelected = false;
      this.btn_window_mode.Location = new System.Drawing.Point(300, 44);
      this.btn_window_mode.Name = "btn_window_mode";
      this.btn_window_mode.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_window_mode.Size = new System.Drawing.Size(115, 50);
      this.btn_window_mode.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_window_mode.TabIndex = 0;
      this.btn_window_mode.Text = "XWINDOWMODECHANGE";
      this.btn_window_mode.UseVisualStyleBackColor = false;
      this.btn_window_mode.Click += new System.EventHandler(this.btn_window_mode_Click);
      // 
      // panel1
      // 
      this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.panel1.BackColor = System.Drawing.Color.White;
      this.panel1.BorderColor = System.Drawing.Color.Empty;
      this.panel1.Controls.Add(this.lbl_splitline);
      this.panel1.Controls.Add(this.label2);
      this.panel1.Controls.Add(this.lbl_version_info_txt);
      this.panel1.Controls.Add(this.lbl_cashier_version);
      this.panel1.Controls.Add(this.pb_logo);
      this.panel1.CornerRadius = 10;
      this.panel1.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.panel1.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.panel1.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.panel1.HeaderHeight = 35;
      this.panel1.HeaderSubText = null;
      this.panel1.HeaderText = "LOGO";
      this.panel1.Location = new System.Drawing.Point(446, 69);
      this.panel1.Name = "panel1";
      this.panel1.PanelBackColor = System.Drawing.Color.White;
      this.panel1.Size = new System.Drawing.Size(425, 245);
      this.panel1.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL_WHITE;
      this.panel1.TabIndex = 6;
      // 
      // lbl_splitline
      // 
      this.lbl_splitline.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_splitline.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(95)))), ((int)(((byte)(95)))));
      this.lbl_splitline.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(95)))), ((int)(((byte)(95)))));
      this.lbl_splitline.Location = new System.Drawing.Point(0, 173);
      this.lbl_splitline.Name = "lbl_splitline";
      this.lbl_splitline.Size = new System.Drawing.Size(425, 1);
      this.lbl_splitline.TabIndex = 1;
      // 
      // label2
      // 
      this.label2.BackColor = System.Drawing.Color.Transparent;
      this.label2.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.label2.Location = new System.Drawing.Point(2, 147);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(410, 25);
      this.label2.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.label2.TabIndex = 0;
      this.label2.Text = "Copyright � Win Systems 2008-2016";
      this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lbl_version_info_txt
      // 
      this.lbl_version_info_txt.BackColor = System.Drawing.Color.Transparent;
      this.lbl_version_info_txt.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_version_info_txt.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_version_info_txt.Location = new System.Drawing.Point(18, 214);
      this.lbl_version_info_txt.Name = "lbl_version_info_txt";
      this.lbl_version_info_txt.Size = new System.Drawing.Size(258, 28);
      this.lbl_version_info_txt.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_version_info_txt.TabIndex = 3;
      this.lbl_version_info_txt.Text = "WIGOS CASHIER VERSION CC.BBB";
      // 
      // lbl_cashier_version
      // 
      this.lbl_cashier_version.BackColor = System.Drawing.Color.Transparent;
      this.lbl_cashier_version.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_cashier_version.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_cashier_version.Location = new System.Drawing.Point(18, 183);
      this.lbl_cashier_version.Name = "lbl_cashier_version";
      this.lbl_cashier_version.Size = new System.Drawing.Size(258, 23);
      this.lbl_cashier_version.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_cashier_version.TabIndex = 2;
      this.lbl_cashier_version.Text = "xVersion Info";
      this.lbl_cashier_version.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // pb_logo
      // 
      this.pb_logo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.pb_logo.BackColor = System.Drawing.Color.Transparent;
      this.pb_logo.Location = new System.Drawing.Point(58, 42);
      this.pb_logo.Name = "pb_logo";
      this.pb_logo.Size = new System.Drawing.Size(310, 101);
      this.pb_logo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
      this.pb_logo.TabIndex = 0;
      this.pb_logo.TabStop = false;
      // 
      // gb_lang_selector
      // 
      this.gb_lang_selector.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.gb_lang_selector.BackColor = System.Drawing.Color.Transparent;
      this.gb_lang_selector.BorderColor = System.Drawing.Color.Empty;
      this.gb_lang_selector.Controls.Add(this.pb_osk_logo);
      this.gb_lang_selector.Controls.Add(this.btn_osk_disable);
      this.gb_lang_selector.Controls.Add(this.btn_osk_enable);
      this.gb_lang_selector.CornerRadius = 10;
      this.gb_lang_selector.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_lang_selector.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gb_lang_selector.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_lang_selector.HeaderHeight = 35;
      this.gb_lang_selector.HeaderSubText = null;
      this.gb_lang_selector.HeaderText = "#HEADERTEXT";
      this.gb_lang_selector.Location = new System.Drawing.Point(10, 322);
      this.gb_lang_selector.Name = "gb_lang_selector";
      this.gb_lang_selector.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_lang_selector.Size = new System.Drawing.Size(425, 112);
      this.gb_lang_selector.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.gb_lang_selector.TabIndex = 2;
      // 
      // pb_osk_logo
      // 
      this.pb_osk_logo.Location = new System.Drawing.Point(11, 46);
      this.pb_osk_logo.Name = "pb_osk_logo";
      this.pb_osk_logo.Size = new System.Drawing.Size(50, 50);
      this.pb_osk_logo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
      this.pb_osk_logo.TabIndex = 95;
      this.pb_osk_logo.TabStop = false;
      // 
      // btn_osk_disable
      // 
      this.btn_osk_disable.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_osk_disable.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_osk_disable.FlatAppearance.BorderSize = 0;
      this.btn_osk_disable.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_osk_disable.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_osk_disable.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_osk_disable.Image = null;
      this.btn_osk_disable.IsSelected = false;
      this.btn_osk_disable.Location = new System.Drawing.Point(178, 47);
      this.btn_osk_disable.Name = "btn_osk_disable";
      this.btn_osk_disable.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_osk_disable.Size = new System.Drawing.Size(115, 50);
      this.btn_osk_disable.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_osk_disable.TabIndex = 0;
      this.btn_osk_disable.Text = "XDISABLE";
      this.btn_osk_disable.UseVisualStyleBackColor = false;
      this.btn_osk_disable.Click += new System.EventHandler(this.btn_osk_disable_Click);
      // 
      // btn_osk_enable
      // 
      this.btn_osk_enable.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_osk_enable.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_osk_enable.FlatAppearance.BorderSize = 0;
      this.btn_osk_enable.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_osk_enable.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_osk_enable.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_osk_enable.Image = null;
      this.btn_osk_enable.IsSelected = false;
      this.btn_osk_enable.Location = new System.Drawing.Point(300, 47);
      this.btn_osk_enable.Name = "btn_osk_enable";
      this.btn_osk_enable.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_osk_enable.Size = new System.Drawing.Size(115, 50);
      this.btn_osk_enable.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_osk_enable.TabIndex = 1;
      this.btn_osk_enable.Text = "XENABLE";
      this.btn_osk_enable.UseVisualStyleBackColor = false;
      this.btn_osk_enable.Click += new System.EventHandler(this.btn_osk_enable_Click);
      // 
      // pnl_db_config
      // 
      this.pnl_db_config.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.pnl_db_config.BackColor = System.Drawing.Color.Transparent;
      this.pnl_db_config.BorderColor = System.Drawing.Color.Empty;
      this.pnl_db_config.CornerRadius = 10;
      this.pnl_db_config.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.pnl_db_config.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.pnl_db_config.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.pnl_db_config.HeaderHeight = 35;
      this.pnl_db_config.HeaderSubText = null;
      this.pnl_db_config.HeaderText = "#HEADERTEXT";
      this.pnl_db_config.Location = new System.Drawing.Point(10, 69);
      this.pnl_db_config.Name = "pnl_db_config";
      this.pnl_db_config.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.pnl_db_config.Size = new System.Drawing.Size(425, 245);
      this.pnl_db_config.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.pnl_db_config.TabIndex = 1;
      // 
      // uc_options
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.Color.Gainsboro;
      this.Controls.Add(this.gb_pinpad_panel);
      this.Controls.Add(this.uc_title);
      this.Controls.Add(this.label3);
      this.Controls.Add(this.lbl_separator_1);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.groupBox1);
      this.Controls.Add(this.groupBox2);
      this.Controls.Add(this.gb_calibrate_touch);
      this.Controls.Add(this.panel1);
      this.Controls.Add(this.pb_options);
      this.Controls.Add(this.gb_lang_selector);
      this.Controls.Add(this.pnl_db_config);
      this.Name = "uc_options";
      this.Size = new System.Drawing.Size(884, 717);
      this.SizeChanged += new System.EventHandler(this.uc_options_SizeChanged);
      ((System.ComponentModel.ISupportInitialize)(this.pb_options)).EndInit();
      this.gb_pinpad_panel.ResumeLayout(false);
      this.groupBox1.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
      this.groupBox2.ResumeLayout(false);
      this.groupBox2.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pb_sys_info)).EndInit();
      this.gb_calibrate_touch.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.pb_calibrate)).EndInit();
      this.panel1.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.pb_logo)).EndInit();
      this.gb_lang_selector.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.pb_osk_logo)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.PictureBox pb_options;
    private WSI.Cashier.Controls.uc_label lbl_separator_1;
    private WSI.Cashier.Controls.uc_round_panel panel1;
    private System.Windows.Forms.PictureBox pb_logo;
    private WSI.Cashier.Controls.uc_label lbl_version_info_txt;
    private WSI.Cashier.Controls.uc_label label1;
    private WSI.Cashier.Controls.uc_label lbl_cashier_version;
    private WSI.Cashier.Controls.uc_label label2;
    private WSI.Cashier.Controls.uc_round_panel groupBox2;
    private System.Windows.Forms.PictureBox pb_sys_info;
    private WSI.Cashier.Controls.uc_label lbl_mem;
    private WSI.Cashier.Controls.uc_label lbl_mem_text;
    private WSI.Cashier.Controls.uc_label lbl_os_version;
    private WSI.Cashier.Controls.uc_label lbl_os_version_text;
    private WSI.Cashier.Controls.uc_label lbl_architecture;
    private WSI.Cashier.Controls.uc_label lbl_cpu_info;
    private WSI.Cashier.Controls.uc_round_panel pnl_db_config;
    private WSI.Cashier.Controls.uc_round_panel gb_lang_selector;
    private System.Windows.Forms.PictureBox pb_osk_logo;
    private WSI.Cashier.Controls.uc_round_button btn_osk_disable;
    private WSI.Cashier.Controls.uc_round_button btn_osk_enable;
    private WSI.Cashier.Controls.uc_round_panel gb_calibrate_touch;
    private System.Windows.Forms.PictureBox pb_calibrate;
    private WSI.Cashier.Controls.uc_round_button btn_window_mode;
    private WSI.Cashier.Controls.uc_round_button btn_table_mode;
    private WSI.Cashier.Controls.uc_label lbl_resolution_text;
    private WSI.Cashier.Controls.uc_label lbl_resolution;
    private WSI.Cashier.Controls.uc_round_panel groupBox1;
    private System.Windows.Forms.PictureBox pictureBox1;
    private WSI.Cashier.Controls.uc_round_button btn_scan_disable;
    private WSI.Cashier.Controls.uc_round_button btn_scan_enable;
    private System.Windows.Forms.Label lbl_splitline;
    private Controls.uc_round_button btn_calibrate;
    private System.Windows.Forms.Label label3;
    private Controls.uc_label uc_title;
    private WSI.Cashier.Controls.uc_round_button btn_reception_mode;
    private Controls.uc_round_panel gb_pinpad_panel;
    private Controls.uc_round_button btn_pinpad_disable;
    private Controls.uc_round_button btn_pinpad_enable;
    private Controls.uc_round_button btn_pinpad_config;
    private Controls.uc_round_combobox cb_screen_resolutions;
    private Controls.uc_label lbl_screen_resolution;
  }
}
