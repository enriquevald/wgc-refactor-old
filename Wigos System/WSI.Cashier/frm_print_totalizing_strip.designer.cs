namespace WSI.Cashier
{
  partial class frm_print_totalizing_strip
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_print_totalizing_strip));
      this.panel1 = new System.Windows.Forms.Panel();
      this.btn_keyboard = new WSI.Cashier.Controls.uc_round_button();
      this.btn_print_selected = new WSI.Cashier.Controls.uc_round_button();
      this.btn_close = new WSI.Cashier.Controls.uc_round_button();
      this.panel2 = new System.Windows.Forms.Panel();
      this.txt_mins_until = new WSI.Cashier.NumericTextBox();
      this.txt_mins_since = new WSI.Cashier.NumericTextBox();
      this.txt_hour_until = new WSI.Cashier.NumericTextBox();
      this.uc_label3 = new WSI.Cashier.Controls.uc_label();
      this.lbl_sep2 = new WSI.Cashier.Controls.uc_label();
      this.txt_hour_since = new WSI.Cashier.NumericTextBox();
      this.uc_datetime_until = new WSI.Cashier.uc_datetime();
      this.uc_datetime_since = new WSI.Cashier.uc_datetime();
      this.lbl_txt_day_now = new WSI.Cashier.Controls.uc_label();
      this.pnl_data.SuspendLayout();
      this.panel1.SuspendLayout();
      this.panel2.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.Controls.Add(this.panel2);
      this.pnl_data.Controls.Add(this.panel1);
      this.pnl_data.Size = new System.Drawing.Size(513, 339);
      // 
      // panel1
      // 
      this.panel1.Controls.Add(this.btn_keyboard);
      this.panel1.Controls.Add(this.btn_print_selected);
      this.panel1.Controls.Add(this.btn_close);
      this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.panel1.Location = new System.Drawing.Point(0, 255);
      this.panel1.Name = "panel1";
      this.panel1.Padding = new System.Windows.Forms.Padding(8);
      this.panel1.Size = new System.Drawing.Size(513, 84);
      this.panel1.TabIndex = 10;
      // 
      // btn_keyboard
      // 
      this.btn_keyboard.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(47)))), ((int)(((byte)(50)))));
      this.btn_keyboard.FlatAppearance.BorderSize = 0;
      this.btn_keyboard.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_keyboard.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_keyboard.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_keyboard.Image = ((System.Drawing.Image)(resources.GetObject("btn_keyboard.Image")));
      this.btn_keyboard.IsSelected = false;
      this.btn_keyboard.Location = new System.Drawing.Point(14, 12);
      this.btn_keyboard.Name = "btn_keyboard";
      this.btn_keyboard.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_keyboard.Size = new System.Drawing.Size(70, 60);
      this.btn_keyboard.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.TERTIARY;
      this.btn_keyboard.TabIndex = 2;
      this.btn_keyboard.UseVisualStyleBackColor = false;
      this.btn_keyboard.Click += new System.EventHandler(this.btn_keyboard_Click);
      // 
      // btn_print_selected
      // 
      this.btn_print_selected.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_print_selected.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_print_selected.FlatAppearance.BorderSize = 0;
      this.btn_print_selected.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_print_selected.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_print_selected.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_print_selected.Image = null;
      this.btn_print_selected.IsSelected = false;
      this.btn_print_selected.Location = new System.Drawing.Point(342, 12);
      this.btn_print_selected.Name = "btn_print_selected";
      this.btn_print_selected.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_print_selected.Size = new System.Drawing.Size(155, 60);
      this.btn_print_selected.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_print_selected.TabIndex = 1;
      this.btn_print_selected.Text = "XPRINTSELECTED";
      this.btn_print_selected.UseVisualStyleBackColor = false;
      this.btn_print_selected.Click += new System.EventHandler(this.btn_print_selected_Click);
      // 
      // btn_close
      // 
      this.btn_close.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_close.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_close.FlatAppearance.BorderSize = 0;
      this.btn_close.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_close.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_close.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_close.Image = null;
      this.btn_close.IsSelected = false;
      this.btn_close.Location = new System.Drawing.Point(179, 12);
      this.btn_close.Name = "btn_close";
      this.btn_close.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_close.Size = new System.Drawing.Size(155, 60);
      this.btn_close.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_close.TabIndex = 0;
      this.btn_close.Text = "XCLOSE";
      this.btn_close.UseVisualStyleBackColor = false;
      this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
      // 
      // panel2
      // 
      this.panel2.AutoSize = true;
      this.panel2.Controls.Add(this.txt_mins_until);
      this.panel2.Controls.Add(this.txt_mins_since);
      this.panel2.Controls.Add(this.txt_hour_until);
      this.panel2.Controls.Add(this.uc_label3);
      this.panel2.Controls.Add(this.lbl_sep2);
      this.panel2.Controls.Add(this.txt_hour_since);
      this.panel2.Controls.Add(this.uc_datetime_until);
      this.panel2.Controls.Add(this.uc_datetime_since);
      this.panel2.Controls.Add(this.lbl_txt_day_now);
      this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panel2.Location = new System.Drawing.Point(0, 0);
      this.panel2.Name = "panel2";
      this.panel2.Size = new System.Drawing.Size(513, 255);
      this.panel2.TabIndex = 11;
      // 
      // txt_mins_until
      // 
      this.txt_mins_until.AccessibleName = "C";
      this.txt_mins_until.AllowSpace = false;
      this.txt_mins_until.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_mins_until.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_mins_until.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_mins_until.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_mins_until.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_mins_until.CornerRadius = 5;
      this.txt_mins_until.Cursor = System.Windows.Forms.Cursors.Default;
      this.txt_mins_until.FillWithCeros = false;
      this.txt_mins_until.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_mins_until.Location = new System.Drawing.Point(414, 152);
      this.txt_mins_until.Margin = new System.Windows.Forms.Padding(0);
      this.txt_mins_until.MaxLength = 2;
      this.txt_mins_until.Multiline = false;
      this.txt_mins_until.Name = "txt_mins_until";
      this.txt_mins_until.PasswordChar = '\0';
      this.txt_mins_until.ReadOnly = false;
      this.txt_mins_until.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_mins_until.SelectedText = "";
      this.txt_mins_until.SelectionLength = 0;
      this.txt_mins_until.SelectionStart = 0;
      this.txt_mins_until.Size = new System.Drawing.Size(49, 40);
      this.txt_mins_until.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_mins_until.TabIndex = 5;
      this.txt_mins_until.TabStop = false;
      this.txt_mins_until.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.txt_mins_until.UseSystemPasswordChar = false;
      this.txt_mins_until.WaterMark = "MM";
      this.txt_mins_until.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // txt_mins_since
      // 
      this.txt_mins_since.AccessibleName = "C";
      this.txt_mins_since.AllowSpace = false;
      this.txt_mins_since.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_mins_since.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_mins_since.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_mins_since.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_mins_since.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_mins_since.CornerRadius = 5;
      this.txt_mins_since.FillWithCeros = false;
      this.txt_mins_since.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_mins_since.Location = new System.Drawing.Point(414, 92);
      this.txt_mins_since.Margin = new System.Windows.Forms.Padding(0);
      this.txt_mins_since.MaxLength = 2;
      this.txt_mins_since.Multiline = false;
      this.txt_mins_since.Name = "txt_mins_since";
      this.txt_mins_since.PasswordChar = '\0';
      this.txt_mins_since.ReadOnly = false;
      this.txt_mins_since.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_mins_since.SelectedText = "";
      this.txt_mins_since.SelectionLength = 0;
      this.txt_mins_since.SelectionStart = 0;
      this.txt_mins_since.Size = new System.Drawing.Size(49, 40);
      this.txt_mins_since.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_mins_since.TabIndex = 2;
      this.txt_mins_since.TabStop = false;
      this.txt_mins_since.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.txt_mins_since.UseSystemPasswordChar = false;
      this.txt_mins_since.WaterMark = "MM";
      this.txt_mins_since.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // txt_hour_until
      // 
      this.txt_hour_until.AllowSpace = false;
      this.txt_hour_until.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_hour_until.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_hour_until.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_hour_until.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_hour_until.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_hour_until.CornerRadius = 5;
      this.txt_hour_until.FillWithCeros = false;
      this.txt_hour_until.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_hour_until.Location = new System.Drawing.Point(346, 152);
      this.txt_hour_until.Margin = new System.Windows.Forms.Padding(0);
      this.txt_hour_until.MaxLength = 2;
      this.txt_hour_until.Multiline = false;
      this.txt_hour_until.Name = "txt_hour_until";
      this.txt_hour_until.PasswordChar = '\0';
      this.txt_hour_until.ReadOnly = false;
      this.txt_hour_until.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_hour_until.SelectedText = "";
      this.txt_hour_until.SelectionLength = 0;
      this.txt_hour_until.SelectionStart = 0;
      this.txt_hour_until.Size = new System.Drawing.Size(49, 40);
      this.txt_hour_until.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_hour_until.TabIndex = 4;
      this.txt_hour_until.TabStop = false;
      this.txt_hour_until.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.txt_hour_until.UseSystemPasswordChar = false;
      this.txt_hour_until.WaterMark = "HH";
      this.txt_hour_until.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // uc_label3
      // 
      this.uc_label3.BackColor = System.Drawing.Color.Transparent;
      this.uc_label3.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.uc_label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.uc_label3.Location = new System.Drawing.Point(395, 153);
      this.uc_label3.Margin = new System.Windows.Forms.Padding(0);
      this.uc_label3.Name = "uc_label3";
      this.uc_label3.Size = new System.Drawing.Size(19, 40);
      this.uc_label3.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.uc_label3.TabIndex = 21;
      this.uc_label3.Text = ":";
      this.uc_label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lbl_sep2
      // 
      this.lbl_sep2.BackColor = System.Drawing.Color.Transparent;
      this.lbl_sep2.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_sep2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_sep2.Location = new System.Drawing.Point(395, 92);
      this.lbl_sep2.Margin = new System.Windows.Forms.Padding(0);
      this.lbl_sep2.Name = "lbl_sep2";
      this.lbl_sep2.Size = new System.Drawing.Size(19, 40);
      this.lbl_sep2.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_sep2.TabIndex = 20;
      this.lbl_sep2.Text = ":";
      this.lbl_sep2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // txt_hour_since
      // 
      this.txt_hour_since.AllowSpace = false;
      this.txt_hour_since.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_hour_since.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_hour_since.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_hour_since.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_hour_since.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_hour_since.CornerRadius = 5;
      this.txt_hour_since.FillWithCeros = false;
      this.txt_hour_since.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_hour_since.Location = new System.Drawing.Point(346, 92);
      this.txt_hour_since.Margin = new System.Windows.Forms.Padding(0);
      this.txt_hour_since.MaxLength = 2;
      this.txt_hour_since.Multiline = false;
      this.txt_hour_since.Name = "txt_hour_since";
      this.txt_hour_since.PasswordChar = '\0';
      this.txt_hour_since.ReadOnly = false;
      this.txt_hour_since.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_hour_since.SelectedText = "";
      this.txt_hour_since.SelectionLength = 0;
      this.txt_hour_since.SelectionStart = 0;
      this.txt_hour_since.Size = new System.Drawing.Size(49, 40);
      this.txt_hour_since.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_hour_since.TabIndex = 1;
      this.txt_hour_since.TabStop = false;
      this.txt_hour_since.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.txt_hour_since.UseSystemPasswordChar = false;
      this.txt_hour_since.WaterMark = "HH";
      this.txt_hour_since.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // uc_datetime_until
      // 
      this.uc_datetime_until.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
      this.uc_datetime_until.DateText = "xDateText";
      this.uc_datetime_until.DateTextFont = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.uc_datetime_until.DateTextWidth = 94;
      this.uc_datetime_until.DateValue = new System.DateTime(((long)(0)));
      this.uc_datetime_until.FormatFont = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.uc_datetime_until.FormatWidth = 39;
      this.uc_datetime_until.Invalid = false;
      this.uc_datetime_until.Location = new System.Drawing.Point(14, 152);
      this.uc_datetime_until.Name = "uc_datetime_until";
      this.uc_datetime_until.Size = new System.Drawing.Size(320, 41);
      this.uc_datetime_until.TabIndex = 3;
      this.uc_datetime_until.ValuesFont = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      // 
      // uc_datetime_since
      // 
      this.uc_datetime_since.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
      this.uc_datetime_since.DateText = "xDateText";
      this.uc_datetime_since.DateTextFont = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.uc_datetime_since.DateTextWidth = 94;
      this.uc_datetime_since.DateValue = new System.DateTime(((long)(0)));
      this.uc_datetime_since.FormatFont = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.uc_datetime_since.FormatWidth = 39;
      this.uc_datetime_since.Invalid = false;
      this.uc_datetime_since.Location = new System.Drawing.Point(14, 92);
      this.uc_datetime_since.Name = "uc_datetime_since";
      this.uc_datetime_since.Size = new System.Drawing.Size(320, 41);
      this.uc_datetime_since.TabIndex = 0;
      this.uc_datetime_since.ValuesFont = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      // 
      // lbl_txt_day_now
      // 
      this.lbl_txt_day_now.BackColor = System.Drawing.Color.Transparent;
      this.lbl_txt_day_now.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_txt_day_now.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_txt_day_now.Location = new System.Drawing.Point(10, 8);
      this.lbl_txt_day_now.Name = "lbl_txt_day_now";
      this.lbl_txt_day_now.Size = new System.Drawing.Size(331, 52);
      this.lbl_txt_day_now.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_txt_day_now.TabIndex = 6;
      this.lbl_txt_day_now.Text = "XTimeNow\r\nXOpenSession";
      // 
      // frm_print_totalizing_strip
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.AutoSize = true;
      this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
      this.ClientSize = new System.Drawing.Size(513, 394);
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "frm_print_totalizing_strip";
      this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Style = WSI.Cashier.Controls.frm_base.BaseFormStyle.PRINCIPAL;
      this.Text = "Message";
      this.pnl_data.ResumeLayout(false);
      this.pnl_data.PerformLayout();
      this.panel1.ResumeLayout(false);
      this.panel2.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.Panel panel2;
    private Controls.uc_round_button btn_print_selected;
    private Controls.uc_round_button btn_close;
    private Controls.uc_label lbl_txt_day_now;
    private uc_datetime uc_datetime_until;
    private uc_datetime uc_datetime_since;
    private NumericTextBox txt_hour_since;
    private NumericTextBox txt_mins_until;
    private NumericTextBox txt_mins_since;
    private NumericTextBox txt_hour_until;
    private Controls.uc_label uc_label3;
    private Controls.uc_label lbl_sep2;
    private Controls.uc_round_button btn_keyboard;


  }
}