//------------------------------------------------------------------------------
// Copyright � 2007-2009 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_mobile_bank.cs
// 
//   DESCRIPTION: Implements the uc_mobile_bank user control
//                Class: uc_mobile_bank
//
//        AUTHOR: Agust� Poch
// 
// CREATION DATE: 05-JUN-2009
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 05-JUN-2009 APB     First release.
// 29-MAR-2012 MPO     User session: Added a event for each button for set the last activity.
// 02-MAY-2012 JCM     Fixed Bug #267: Fixed focus in card reader.
// 22-MAY-2012 RCI     Reset authorized user at the uc_card/uc_mobile_bank level, not in internal routines (don't do it in DB_CardCreditAdd()).
// 07-AUG-2012 JCM     Add Set Sales Limit Button
// 28-MAY-2013 NMR     Preventing the focus losing on main window
// 03-JUL-2013 RRR     Moddified view parameters for Window Mode
// 13-SEP-2013 QMP     Added Work Shifts
// 11-OCT-2013 RMS     Fixed Bug WIG-272: Update Cashier Session Status label
// 26-MAR-2014 RRR     Added permissions check in Mobile Bank select by name button
// 21-JUL-2014 DRV     Fixed Bug WIG-1104: User data it's not shown if user changes his user card
// 28-NOV-2014 JML     Fixed Bug WIG-1758: Allows the change of recharge limits, a mobile bank, associated to a different cashier.
// 09-DEC-2014 JMV     DUT - WIGOS 201412: Logrand - Bancos m�viles, Dep�sitos
// 21-JAN-2015 OPC     Added new functionality for undo mb recharge.
// 21-JAN-2015 OPC     Added new functionality for undo mb deposit.
// 09-FEB-2015 OPC     Fixed Bug WIG-2027: check if the account user it's playing when try to undo recharge.
// 14-MAY-2015 YNM     Fixed Bug WIG-2331: Text missing in the PopUp to create a new movil bank account
// 14-MAY-2015 YNM     Fixed Bug WIG-2352: Cashier allows associate MB card to an account when cashier session is closed
// 04-NOV-2015 JRC     Backlog Item WIG-5432: Change designer
// 11-NOV-2015 FOS     Product Backlog Item: 3709 Payment tickets & handpays
// 02-FEB-2016 DHA     Bug 8903:Cashier - Movile bank: disable all buttons in the first time
// 15-NOV-2017 FGB     WIGOS-3372: MobiBank: Cashier management - Main screen
// 22-NOV-2017 FGB     WIGOS-3372: MobiBank: Cashier management - Main screen (when there's not cashier session id be able to open mobile banks browser though the search button).
//------------------------------------------------------------------------------
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WSI.Common;

namespace WSI.Cashier
{
  public partial class uc_mobile_bank : UserControl
  {
    #region Class Atributes

    private static frm_yesno m_form_yes_no;

    Int64 m_current_card_id;

    Currency m_current_card_balance;
    Currency m_pending_cash;
    Boolean m_mb_closed_session;
    frm_amount_input form_credit = new frm_amount_input();
    frm_last_movements form_last_plays = new frm_last_movements();
    String m_track_number;
    Int32 tick_count_tracknumber_read = CashierBusinessLogic.DURATION_BUTTONS_ENABLED_AFTER_OPERATION + 1;
    CardPrinter card_printer;
    AccountWatcher account_watcher;
    Boolean first_time = true;
    Boolean anonymous;

    private frm_container m_parent;
    private System.Globalization.NumberFormatInfo m_nfi;
    private Boolean m_mobibank_enabled;
    private Int32 m_mobibank_user_id;
    private uc_mobile_bank_user_info m_mobibank_user_info;
    #endregion

    #region Constructor

    /// <summary>
    /// Constructor
    /// </summary>
    public uc_mobile_bank()
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[UC LOAD] uc_mobile_bank", Log.Type.Message);

      InitializeComponent();

      m_form_yes_no = new frm_yesno();
      m_form_yes_no.Opacity = 0.6;

      card_printer = new CardPrinter();

      m_current_card_id = 0;

      InitializeMobiBankSettings();

      EventLastAction.AddEventLastAction(this.Controls);
    }
    #endregion

    #region Private Methods
    //------------------------------------------------------------------------------
    // PURPOSE: Initialize control resources. (NLS string, images, etc.).
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //

    public void InitializeControlResources()
    {
      // - NLS Strings:

      //   - Labels
      lbl_account_id_name.Text = Resource.String("STR_FORMAT_GENERAL_NAME_MB") + ":";
      gb_pin.Text = Resource.String("STR_UC_MB_CARD_PIN");
      gb_last_activity.Text = Resource.String("STR_UC_MB_CARD_SESSION_INFO_LAST_ACTIVITY");

      //   - Buttons
      btn_new_card.Text = Resource.String("STR_UC_MB_CARD_BTN_NEW_CARD");
      btn_account_edit.Text = Resource.String("STR_UC_MB_CARD_BTN_ACCOUNT_EDIT");
      btn_card_block.Text = Resource.String("STR_UC_MB_CARD_BTN_CARD_BLOCK");
      btn_change_pin.Text = Resource.String("STR_UC_MB_CARD_BTN_CHANGE_PIN");
      btn_limits_edit.Text = Resource.String("STR_UC_MB_CARD_BTN_LIMITS_EDIT");

      btn_change_limit.Text = Resource.String("STR_UC_MB_CARD_BTN_EXTEND_LIMIT");
      btn_cash_deposit.Text = Resource.String("STR_UC_MB_CARD_BTN_CASH_DEPOSIT");
      btn_set_limit.Text = Resource.String("STR_UC_MB_CARD_BTN_CHANGE_LIMIT");
      btn_close_sesion.Text = Resource.String("STR_UC_MB_CARD_BTN_CLOSE_SESSION");
      btn_last_movements.Text = Resource.String("STR_UC_MB_CARD_BTN_LAST_MOVEMENTS");
      btn_print_card.Text = Resource.String("STR_UC_CARD_BTN_PRINT_CARD");
      btn_undo_operation.Text = Resource.String("STR_UNDO_LAST_OPERATION_GENERIC");

      lbl_limit_by_session.Text = Resource.String("STR_FRM_MB_ACCOUNT_USER_LIMIT_BY_SESSION");
      lbl_limit_by_recharge.Text = Resource.String("STR_FRM_MB_ACCOUNT_USER_LIMIT_BY_RECHARGE");
      lbl_limit_by_number_of_recharges.Text = Resource.String("STR_FRM_MB_ACCOUNT_USER_LIMIT_BY_NUMBER_OF_RECHARGES");

      //  - Datagrids

      //  - Other
      gb_last_activity.Text = " " + Resource.String("STR_UC_MB_CARD_LAST_ACTIVITY") + " ";

      this.BackColor = WSI.Cashier.CashierStyle.Colors.m_colors_dic[CashierStyle.Colors.ENUM_COLORS.HEX_E3E7E9];
      lbl_separator_1.BackColor = WSI.Cashier.CashierStyle.Colors.m_colors_dic[CashierStyle.Colors.ENUM_COLORS.HEX_959595];

      this.uc_card_reader1.InitializeControlResources();
      this.uc_mb_card_balance1.InitializeControlResources();
      this.form_credit.InitializeControlResources();
      this.form_last_plays.InitializeControlResources();
      this.m_mobibank_user_info.InitializeControlResources();

      pb_limit.Image = Resources.ResourceImages.coins;
    } // InitializeControlResources

    //------------------------------------------------------------------------------
    // PURPOSE: Initialize controls. 
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    public void InitControls(frm_container Parent)
    {
      m_parent = Parent;

      if (first_time)
      {
        first_time = false;

        account_watcher = new AccountWatcher();
        account_watcher.Start();
        account_watcher.AccountChangedEvent += new AccountWatcher.AccountChangedHandler(account_watcher_AccountChangedEvent);

        uc_card_reader1.OnTrackNumberReadEvent += new uc_card_reader.TrackNumberReadEventHandler(uc_card_reader1_OnTrackNumberReadEvent);
        uc_card_reader1.OnTrackNumberReadingEvent += new uc_card_reader.TrackNumberReadingHandler(uc_card_reader1_OnTrackNumberReadingEvent);

        btn_mb_browse.Click += new EventHandler(btn_button_clicked);

        btn_new_card.Click += new EventHandler(btn_button_clicked);
        btn_account_edit.Click += new EventHandler(btn_button_clicked);
        btn_limits_edit.Click += new EventHandler(btn_button_clicked);
        btn_card_block.Click += new EventHandler(btn_button_clicked);
        btn_change_pin.Click += new EventHandler(btn_button_clicked);

        btn_change_limit.Click += new EventHandler(btn_button_clicked);
        btn_cash_deposit.Click += new EventHandler(btn_button_clicked);
        btn_last_movements.Click += new EventHandler(btn_button_clicked);
        btn_print_card.Click += new EventHandler(btn_button_clicked);
        btn_set_limit.Click += new EventHandler(btn_button_clicked);
        btn_close_sesion.Click += new EventHandler(btn_button_clicked);
        btn_undo_operation.Click += new EventHandler(btn_button_clicked);

        m_nfi = System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat;

        InitializeControlResources();
      }

      pb_user.Image = Resources.ResourceImages.userMB;
      pb_blocked.Image = WSI.Cashier.Properties.Resources.locked;
      pb_pin.Image = WSI.Cashier.Properties.Resources.pin;
      pb_terminal.Image = WSI.Cashier.Images.Get(Images.CashierImage.SlotMachine);

      lbl_terminal_name.Text = "";

      SwitchMobiBankControls();

      ClearCardData();

      // RCI & AJQ 18-FEB-2011: Expire the timer: Disable buttons.
      tick_count_tracknumber_read = CashierBusinessLogic.DURATION_BUTTONS_ENABLED_AFTER_OPERATION + 1;

      EnableDisableButtons(false);
    } // InitControls

    /// <summary>
    /// Initialize MobiBank settings
    /// </summary>
    private void InitializeMobiBankSettings()
    {
      m_mobibank_enabled = false;

      InitializeMobiBankUserId();

      m_mobibank_user_info = new uc_mobile_bank_user_info();

      this.Controls.Add(this.m_mobibank_user_info);

      SwitchMobiBankControls();
    }

    private void InitializeMobiBankUserId()
    {
      m_mobibank_user_id = MobileBankUserInfo.GetEmptyUserId();
    }

    /// <summary>
    /// Switch MobiBank control
    /// </summary>
    private void SwitchMobiBankControls()
    {
      m_mobibank_enabled = IsMobiBankEnabled();

      if (m_mobibank_enabled)
      {
        m_mobibank_user_info.Location = gb_card.Location;
        m_mobibank_user_info.Size = new System.Drawing.Size(gb_card.Size.Width, (gb_user_edit.Location.Y + gb_user_edit.Size.Height - gb_card.Location.Y));
      }

      SwitchEnabledMobiBank(m_mobibank_enabled);
    }

    /// <summary>
    /// Switch panel show depending on IsMobiBankEnabled
    /// </summary>
    /// <param name="MobiBankEnabled"></param>
    private void SwitchEnabledMobiBank(Boolean MobiBankEnabled)
    {
      m_mobibank_user_info.Enabled = MobiBankEnabled;
      m_mobibank_user_info.Visible = m_mobibank_user_info.Enabled;

      gb_card.Enabled = !MobiBankEnabled;
      gb_card.Visible = gb_card.Enabled;

      gb_user_edit.Enabled = gb_card.Enabled;
      gb_user_edit.Visible = gb_user_edit.Enabled;
    }

    /// <summary>
    /// Is MobiBank enabled
    /// </summary>
    /// <returns></returns>
    private Boolean IsMobiBankEnabled()
    {
      return (MobileBankConfig.IsMobileBankLinkedToADevice);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Handle the account changed event for mobile banks. 
    // 
    //  PARAMS:
    //      - INPUT:
    //          - AccountId
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    void account_watcher_AccountChangedEvent()
    {
      if (this.InvokeRequired)
      {
        this.BeginInvoke(new AccountWatcher.AccountChangedHandler(account_watcher_AccountChangedEvent));

        return;
      }

      RefreshData();
    } // account_watcher_AccountChangedEvent

    private void RestoreParentFocus()
    {
      this.ParentForm.Focus();
      this.ParentForm.BringToFront();
    }

    private void UndoMBOperation(OperationCode SelectedOperationCode)
    {
      MBCardData _mb_card_data;
      CardData _aux_card_data;
      CashierSessionInfo _cashier_session_info;
      AccountOperations.Operation _account_operation;
      CurrencyIsoType _iso_type;
      Decimal _iso_amount;
      OperationUndo.UndoError _undo_error;
      String _str_error;
      String _str_message;
      String _caption;
      OperationUndo.InputUndoOperation _input_undo_operation;
      ArrayList _voucher_list;
      DialogResult _dlg_rc;
      Currency _undo_money;

      _mb_card_data = account_watcher.MBCard;
      _cashier_session_info = Cashier.CashierSessionInfo();
      _aux_card_data = new CardData();
      _input_undo_operation = new OperationUndo.InputUndoOperation();
      _caption = Resource.String("STR_UNDO_LAST_OPERATION");

      switch (SelectedOperationCode)
      {
        case OperationCode.MB_CASH_IN:
          if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.MBUndoLastCashIn,
                                                   ProfilePermissions.TypeOperation.RequestPasswd, this.ParentForm, out _str_error))
          {
            return;
          }
          break;

        case OperationCode.MB_DEPOSIT:
          if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.MBUndoLastDeposit,
                                                   ProfilePermissions.TypeOperation.RequestPasswd, this.ParentForm, out _str_error))
          {
            return;
          }
          break;

        default:
          return;
      }

      if (!OperationUndo.GetLastReversibleOperation(SelectedOperationCode, _mb_card_data, _cashier_session_info.CashierSessionId,
                                                    out _account_operation, out _iso_type, out _iso_amount, out _undo_error))
      {
        _str_error = OperationUndo.GetErrorMessage(_undo_error);

        frm_message.Show(_str_error, Resource.String("STR_APP_GEN_MSG_ERROR"),
                         MessageBoxButtons.OK, MessageBoxIcon.Error, this.ParentForm);

        return;
      }

      _undo_money = ((Currency)_iso_amount);

      CardData.DB_CardGetAllData(_account_operation.AccountId, _aux_card_data);
      if (SelectedOperationCode == OperationCode.MB_CASH_IN)
      {
        if (_undo_money > _aux_card_data.AccountBalance.TotalRedeemable)
        {
          _str_error = Resource.String("STR_UNDO_OPERATION_NOT_ENOUGH_REDEEMABLE");
          _str_error = _str_error.Replace("\\r\\n", "\r\n");

          frm_message.Show(_str_error, Resource.String("STR_APP_GEN_MSG_ERROR"),
                           MessageBoxButtons.OK, MessageBoxIcon.Error, this.ParentForm);

          return;
        }
      }

      // Show information message
      switch (SelectedOperationCode)
      {
        case OperationCode.MB_CASH_IN:
          _str_message = Resource.String("STR_MB_UNDO_RECHARGE_MESSAGE", ((Currency)_iso_amount).ToString());
          break;

        case OperationCode.MB_DEPOSIT:
          _str_message = Resource.String("STR_MB_UNDO_DEPOSIT_MESSAGE", ((Currency)_iso_amount).ToString());
          _caption = Resource.String("STR_MB_UNDO_DEPOSIT").ToUpper();
          break;

        default:
          return;
      }

      _str_message = _str_message.Replace("\\r\\n", "\r\n");
      _dlg_rc = frm_message.Show(_str_message,
                                 _caption,
                                 MessageBoxButtons.YesNo, Images.CashierImage.Question, Cashier.MainForm);

      if (_dlg_rc != DialogResult.OK)
      {
        return;
      }

      _input_undo_operation.CardData = _aux_card_data;
      _input_undo_operation.MBCardData = _mb_card_data;
      _input_undo_operation.CashierSessionInfo = _cashier_session_info;
      _input_undo_operation.CodeOperation = _account_operation.Code;
      _input_undo_operation.GenerateVoucherForUndo = true;
      _input_undo_operation.OperationId = 0;
      _input_undo_operation.OperationIdForUndo = _account_operation.OperationId;

      if (!OperationUndo.UndoOperation(_input_undo_operation, out _voucher_list))
      {
        _str_error = Resource.String("STR_UNDO_OPERATION_ERROR");

        frm_message.Show(_str_error, Resource.String("STR_APP_GEN_MSG_ERROR"),
                         MessageBoxButtons.OK, MessageBoxIcon.Error, this.ParentForm);

        return;
      }

      VoucherPrint.Print(_voucher_list);
    }

    #endregion

    #region Buttons

    //------------------------------------------------------------------------------
    // PURPOSE: Load all relevant MB account data from the provided track data. 
    // 
    //  PARAMS:
    //      - INPUT:
    //          - StringId
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //

    public void LoadCardByTrackNumber(String TrackNumber)
    {
      // Check for no input card
      if (String.IsNullOrEmpty(TrackNumber))
      {
        return;
      }

      MBCardData _card_data = new MBCardData();
      m_track_number = String.Empty;

      // Get Card data
      if (!CashierBusinessLogic.DB_MBCardGetAllData(TrackNumber, _card_data))
      {
        ClearCardData();

        Log.Error("LoadCardByTrackNumber. DB_MBCardGetAllData: Error reading card.");

        return;
      }

      LoadMBCard(_card_data, TrackNumber);
    }

    public void LoadMBCard(MBCardData cardData, String TrackNumber)
      {
      Boolean _show_message;
      MBMovementType? _bm_last_movement_type_in_this_cs;
      MBMovementType? _bm_last_movement_type_in_other_cs;

      _show_message = false;

      if (cardData.CardId == 0)
      {
        String msgbox_text;
        DialogResult msbox_answer;

        // MBF 30-NOV-2009 - Check BEFORE asking if the user wants to create
        // Check provided trackdata is not already associated to a player account
        if (CashierBusinessLogic.DB_IsCardDefined(TrackNumber))
        {
          // MsgBox: Card already assigned to a Player account.
          //frm_message.Show(Resource.String("STR_UC_MB_CARD_USER_MSG_CARD_LINKED_TO_USER_ACCT"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this.ParentForm);

          // We are going to change the form shown, expire the timer to disable buttons in case of return to this form.
          tick_count_tracknumber_read = CashierBusinessLogic.DURATION_BUTTONS_ENABLED_AFTER_OPERATION + 1;

          m_parent.SwitchToCard(TrackNumber);
          ClearCardData();
          uc_card_reader1.ClearTrackNumber();

          //uc_card_reader1.Focus();

          return;
        }
       
        // Check cashier session is open
        if (CashierBusinessLogic.ReadCashierStatus(Cashier.SessionId) != CASHIER_STATUS.OPEN)
        {
          msgbox_text = Resource.String("STR_UC_CARD_USER_MSG_CARD_NOT_EXIST_AND_CASH_CLOSED", "\n\n" + TrackNumber + "\n\n");
          frm_message.Show(msgbox_text, Resource.String("STR_UC_CARD_USER_MSG_CARD_NOT_EXIST_TITLE"), MessageBoxButtons.OK, MessageBoxIcon.Warning, this.ParentForm);

          uc_card_reader1.Focus();

          return;
        }

        // Check the card belongs to the site.
        if (!CardData.CheckTrackdataSiteId(new ExternalTrackData(TrackNumber)))
        {
          ClearCardData();

          Log.Error("LoadCardByTrackNumber. Card: " + TrackNumber + " doesn't belong to this site.");

          frm_message.Show(Resource.String("STR_CARD_SITE_ID_ERROR"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Warning, this.ParentForm);

          uc_card_reader1.Focus();

          return;
        }

        if (!m_mobibank_enabled)
        {
        // Card does not exist: request confirmation to create a new one.
        msgbox_text = Resource.String("STR_UC_CARD_USER_MSG_CARD_MB_NOT_EXIST", "\n" + TrackNumber + "\n\n");
        msbox_answer = frm_message.Show(msgbox_text, Resource.String("STR_UC_CARD_USER_MSG_CARD_NOT_EXIST_TITLE"), MessageBoxButtons.YesNo, MessageBoxIcon.Warning, this.ParentForm);
        }
        else
        {
          // Card does not exist: Only inform and do not ask to create it.
          msgbox_text = Resource.String("STR_UC_CARD_USER_MSG_CARD_MB_NOT_EXIST_DO_NOT_ASK_TO_CREATE_IT", "\n" + TrackNumber);
          frm_message.Show(msgbox_text, Resource.String("STR_UC_CARD_USER_MSG_CARD_NOT_EXIST_TITLE"), MessageBoxButtons.OK, MessageBoxIcon.Warning, this.ParentForm);
          msbox_answer = DialogResult.Cancel;
        }

        if (msbox_answer == DialogResult.OK)
        {
          if (!CashierBusinessLogic.DB_MBCreateCard(TrackNumber))
          {
            Log.Error("LoadCardByTrackNumber. DB_MBCreateCard: Card already exists or error creating card.");

            // TODO: MsgBox: Card already exist or error creating card.
            //frm_message.Show(Resource.String("STR_FRM_AMOUNT_INPUT_MSG_AMOUNT_EXCEED_PAYABLE"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this);

            uc_card_reader1.Focus();

            return;
          }

          // Load data after create account.
          LoadCardByTrackNumber(TrackNumber);
        }
        else
        {
          this.ClearCardData();
          this.InitControls(m_parent);
        }
      }
      else
      {
        // Check the card belongs to the site.
        if (!CardData.CheckTrackdataSiteId(new ExternalTrackData(cardData.TrackData)))
        {
          ClearCardData();

          Log.Error("LoadCardByTrackNumber. Card: " + TrackNumber + " doesn't belong to this site.");

          frm_message.Show(Resource.String("STR_CARD_SITE_ID_ERROR"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Warning, this.ParentForm);

          uc_card_reader1.Focus();

          return;
        }

        if ((m_mobibank_enabled) && (cardData.UserId == 0))
        {
          ClearCardData();

          Log.Error("LoadCardByTrackNumber. Card: " + TrackNumber + " is not associated with any Mobile Bank User.");

          // Mobile bank is not associated with any mobile bank user
          String _msgbox_text;
          _msgbox_text = Resource.String("STR_UC_CARD_USER_MSG_CARD_MB_IS_NOT_ASSOCIATED_WITH_ANY_MOBIBANK_USER");
          frm_message.Show(_msgbox_text, Resource.String("STR_UC_CARD_USER_MSG_CARD_NOT_EXIST_TITLE"), MessageBoxButtons.OK, MessageBoxIcon.Warning, this.ParentForm);

          uc_card_reader1.Focus();

          return;
        }

        if (cardData.CurrentCashierSessionId != 0)
        {
          if (cardData.CurrentCashierSessionId != Cashier.SessionId)
          {
            switch (MobileBank.m_reopen_mode)
            {
              case ReopenMode.NoReopen:
              case ReopenMode.ReopenInThisSession:
                _show_message = true;

                break;

              case ReopenMode.ReopenInOtherSession:
              case ReopenMode.ReopenAlways:
                LastMovementInCashierSessions(cardData.CardId, out _bm_last_movement_type_in_this_cs, out _bm_last_movement_type_in_other_cs);
                if (_bm_last_movement_type_in_other_cs != (MBMovementType)(-1) && _bm_last_movement_type_in_other_cs != MBMovementType.CloseSession)
                {
                  _show_message = true;
                }

                break;
            }

            if (_show_message)
            {
              // MB Card is already associated to another cashier session: operations are disabled & show a message
              frm_message.Show(Resource.String("STR_UC_MB_CARD_CASHIER_SESSION_MISMATCH"), Resource.String("STR_APP_GEN_MSG_WARNING"), MessageBoxButtons.OK, MessageBoxIcon.Warning, this.ParentForm);
            }
          }
        }

        // Keep card read data.
        m_track_number = TrackNumber;
        m_current_card_id = cardData.CardId;

        if (m_mobibank_enabled)
        {
          m_mobibank_user_id = cardData.UserId;
      }

        account_watcher.SetAccount(cardData);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Check if card had movements in this cashier session. 
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    private Boolean HadMovementsInThisCashierSession(Int64 CardId)
    {
      StringBuilder _sb;
      Object _obj;

      if (CardId == 0)
      {
        return true;
      }

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("SELECT   COUNT(1) ");
        _sb.AppendLine("  FROM   MB_MOVEMENTS ");
        _sb.AppendLine(" WHERE   MBM_CASHIER_SESSION_ID = @pCashierSessionId ");
        _sb.AppendLine("   AND   MBM_MB_ID = @pMBCardId ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pCashierSessionId", SqlDbType.BigInt).Value = Cashier.SessionId;
            _sql_cmd.Parameters.Add("@pMBCardId", SqlDbType.BigInt).Value = CardId;

            _obj = _sql_cmd.ExecuteScalar();
          }
        }

        if (_obj != null && _obj != DBNull.Value)
        {
          return ((Int32)_obj > 0);
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // had_movements_in_this_cashier_session

    private Boolean LastMovementInCashierSessions(Int64 CardId, out MBMovementType? BMLastMovementTypeInThisCS, out MBMovementType? BMLastMovementTypeInOtherCS)
    {
      StringBuilder _sb;

      BMLastMovementTypeInThisCS = null; // No movements
      BMLastMovementTypeInOtherCS = null; // No movements

      if (CardId == 0)
      {
        return true;
      }

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("  SELECT   MAX(CASE A.MBM_CASHIER_SESSION_ID WHEN @pCashierSessionId THEN A.MBM_TYPE ELSE NULL END) AS BM_LAST_MOVEMENT_TYPE_IN_THIS_CS,  ");
        _sb.AppendLine("           MAX(CASE A.MBM_CASHIER_SESSION_ID WHEN @pCashierSessionId THEN NULL ELSE A.MBM_TYPE END) AS BM_LAST_MOVEMENT_TYPE_IN_OTHER_CS  ");
        _sb.AppendLine("    FROM   MB_MOVEMENTS  A                                                                          ");
        _sb.AppendLine("   WHERE   MBM_MOVEMENT_ID = (SELECT   MAX(MBM_MOVEMENT_ID)                                         ");
        _sb.AppendLine("                                FROM   MB_MOVEMENTS B                                               ");
        _sb.AppendLine("                          INNER JOIN   CASHIER_SESSIONS ON CS_SESSION_ID = B.MBM_CASHIER_SESSION_ID ");
        _sb.AppendLine("                               WHERE   CS_STATUS = @pCSStatus                                       ");
        _sb.AppendLine("                                 AND   A.MBM_MB_ID = @pMBCardId                                     ");
        _sb.AppendLine("                                 AND   CS_SESSION_ID = A.MBM_CASHIER_SESSION_ID                     ");
        _sb.AppendLine("                                 AND   B.MBM_MB_ID = A.MBM_MB_ID )                                  ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pCSStatus", SqlDbType.Int).Value = CASHIER_STATUS.OPEN;
            _sql_cmd.Parameters.Add("@pMBCardId", SqlDbType.BigInt).Value = CardId;
            _sql_cmd.Parameters.Add("@pCashierSessionId", SqlDbType.BigInt).Value = Cashier.SessionId;

            using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
            {
              if (!_reader.Read())
              {
                return false;
              }

              BMLastMovementTypeInThisCS = _reader.IsDBNull(0) ? BMLastMovementTypeInThisCS : (MBMovementType)_reader.GetInt32(0);
              BMLastMovementTypeInOtherCS = _reader.IsDBNull(1) ? BMLastMovementTypeInOtherCS : (MBMovementType)_reader.GetInt32(1);
            }
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // LastMovementInCashierSession

    //------------------------------------------------------------------------------
    // PURPOSE: Reload all relevant MB account data. 
    // 
    //  PARAMS:
    //      - INPUT:
    //          - StringId
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //    - Account identification comes from current AccountWatcher data

    private void RefreshData()
    {
      RefreshData(account_watcher.MBCard);
    }

    /// <summary>
    /// Creates mobile bank if it does not exist
    /// </summary>
    /// <param name="mbUserInfoData"></param>
    /// <returns>True if created the mobile bank</returns>
    private Boolean CreateMobileBankIfNotExistsForUserId(MobileBankUserInfoData mbUserInfoData)
    {
      if (mbUserInfoData != null)
      {
        if (MobileBankUserInfo.IsMobileBankIdEmpty(mbUserInfoData.MobileBankId))
        {
          //Create mobile bank if it does no exist
          MobileBankUserInfo _mb_user_info;
          _mb_user_info = new MobileBankUserInfo();

          if (!_mb_user_info.CreateMobileBankForUser(ref mbUserInfoData))
          {
            Log.Error("CheckMobileBankExistsForUserId. CreateMobileBankForUser: Error creating mobile bank for user: " + m_mobibank_user_id.ToString());
            return false;
          }

          return true;
        }
      }

      return false;
    }

    /// <summary>
    /// Refresh MobiBank data
    /// </summary>
    private void RefreshMobiBankData()
    {
      if (m_mobibank_enabled)
      {
        MobileBankUserInfoData _mb_userinfo_data;

        _mb_userinfo_data = GetMobileBankUserInfoData(m_mobibank_user_id);

        //Show mobi bank user
        m_mobibank_user_info.Show(_mb_userinfo_data);
      }
    }

    /// <summary>
    /// Get MobileBankUserInfoData info
    /// </summary>
    /// <param name="UserId"></param>
    /// <returns></returns>
    private MobileBankUserInfoData GetMobileBankUserInfoData(Int32 UserId)
    {
      MobileBankUserInfoData _temp_mb_userinfo_data;
      MobileBankUserInfo _mb_user_info;

      _mb_user_info = new MobileBankUserInfo();

      if (!_mb_user_info.GetMobileBankUserInfo(UserId, out _temp_mb_userinfo_data))
      {
        _temp_mb_userinfo_data = new MobileBankUserInfoData();
      }

      return _temp_mb_userinfo_data;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Reload all relevant MB account data. 
    // 
    //  PARAMS:
    //      - INPUT:
    //          - CardData
    //
    //      - OUTPUT:
    //          - CardData
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    private void RefreshData(MBCardData CardData)
    {
      MBCardData _card_data;
      Boolean _enabled_buttons_after_operation;

      _card_data = CardData;
      if (_card_data == null)
      {
        _card_data = new MBCardData();
      }

      m_track_number = _card_data.TrackData;
      m_current_card_id = _card_data.CardId;
      m_current_card_balance = _card_data.SalesLimit;
      m_pending_cash = _card_data.PendingCash;
      m_mb_closed_session = _card_data.SessionClosed;

      // Display card data
      lbl_track_number.Text = m_track_number;

      MobileBank.m_reopen_mode = MobileBank.GetReopenMode();

      _card_data.HadMovementsInThisCashierSession = HadMovementsInThisCashierSession(_card_data.CardId);

      if (_card_data.HolderName == "")
      {
        this.anonymous = true;
        lbl_holder_name.Text = Resource.String("STR_UC_CARD_CARD_HOLDER_ANONYMOUS");
        //pb_user.Image = Resources.ResourceImages.anonymous_user;
      }
      else
      {
        this.anonymous = false;
        lbl_holder_name.Text = _card_data.HolderName;
        //pb_user.Image = Resources.ResourceImages.male_user;
      }

      lbl_account_id_value.Text = m_current_card_id.ToString();

      if (_card_data.Blocked)
      {
        btn_card_block.Text = Resource.String("STR_UC_CARD_BLOCK_ACTION_UNLOCK");
        lbl_blocked.Text = Resource.String("STR_UC_CARD_BLOCK_STATUS_LOCKED");
        pb_blocked.Image = WSI.Cashier.Properties.Resources.locked;
      }
      else
      {
        btn_card_block.Text = Resource.String("STR_UC_CARD_BLOCK_ACTION_LOCK");
        lbl_blocked.Text = Resource.String("STR_UC_CARD_BLOCK_STATUS_UNLOCKED");
        pb_blocked.Image = WSI.Cashier.Properties.Resources.notLocked;
      }

      lbl_pin.Text = _card_data.Pin;
      HidePin();
      RefreshMobiBankData();

      lbl_terminal_name.Text = _card_data.LastTerminalName;

      // Check for last activity
      if (_card_data.LastActivity == DateTime.MinValue)
      {
        lbl_last_activity_value.Text = Resource.String("STR_UC_CARD_SESSION_LAST_ACTIVITY_NONE");
      }
      else
      {
        lbl_last_activity_value.Text = _card_data.LastActivity.ToString();
      }

      if (_card_data.TotalLimit > 0)
      {
        lbl_limit_by_session_amount.Text = _card_data.TotalLimit.ToString();
      }
      else
      {
        lbl_limit_by_session_amount.Text = "---";
      }

      if (_card_data.RechargeLimit > 0)
      {
        lbl_limit_by_recharge_amount.Text = _card_data.RechargeLimit.ToString();
      }
      else
      {
        lbl_limit_by_recharge_amount.Text = "---";
      }

      if (_card_data.NumberOfRechargesLimit > 0)
      {
        lbl_limit_by_number_of_recharges_amount.Text = _card_data.NumberOfRechargesLimit.ToString("#" + m_nfi.CurrencyGroupSeparator + "#");
      }
      else
      {
        lbl_limit_by_number_of_recharges_amount.Text = "---";
      }

      // Refresh Card Counters
      uc_mb_card_balance1.FillCardCounters(_card_data);

      _enabled_buttons_after_operation = (WSI.Common.Misc.ReadGeneralParams("Cashier", "KeepEnabledButtonsAfterOperation") == "1");

      Boolean _enable_buttons;
      _enable_buttons = (m_current_card_id > 0 &&
          (tick_count_tracknumber_read == 0 ||
                               (_enabled_buttons_after_operation &&
                                  tick_count_tracknumber_read < CashierBusinessLogic.DURATION_BUTTONS_ENABLED_AFTER_OPERATION)));

      EnableDisableButtons(_enable_buttons);
    } // RefreshData

    private void HidePin() 
    {
      int _visible_chars;
      StringBuilder _text_aux;

      _visible_chars = GeneralParam.GetInt32("MobileBank", "PIN.VisibleChars", 0);
      _text_aux = new StringBuilder(lbl_pin.Text);

      if (_visible_chars >= 0 && _visible_chars < lbl_pin.Text.Length)
      {
        for (int _i = 0; _i < lbl_pin.Text.Length - _visible_chars; _i++)
        {
          _text_aux[_i] = '*';
        }

        lbl_pin.Text = _text_aux.ToString();
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Edit MB sales limit value 
    // 
    //  PARAMS:
    //      - INPUT:
    //        -ExtendLimit :     True, if extends the new sales limit on current balance.  Final MB Limit = Current + NewSales
    //                           False, if set current balance = new sale limit            Final MB Limit = NewSales
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    private void ChangeLimit(MB_SALES_LIMIT_OPERATION OperationLimitUpdates)
    {
      Currency _amount_to_add;
      MBCardData _card_data = new MBCardData();
      Boolean _limit_reached;
      Currency _remain_credit;
      String _title;
      String _message;
      VoucherTypes _voucher_type;

      //// Check if current user is an authorized user
      //String error_str;
      //if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.MobileBank, ProfilePermissions.TypeOperation.RequestPasswd, this.ParentForm, out error_str))
      //{
      //  return;
      //}

      if (m_current_card_id == 0)
      {
        return;
      }

      // Get Card data
      if (!CashierBusinessLogic.DB_MBCardGetAllData(m_current_card_id, _card_data))
      {
        Log.Error("ChangeLimit: DB_MBCardGetAllData. Error reading card. Mobile Bank: " + m_current_card_id.ToString());

        return;
      }

      m_form_yes_no.Show(this.ParentForm);

      try
      {
        if (OperationLimitUpdates == MB_SALES_LIMIT_OPERATION.EXTEND_LIMIT)
        {
          _voucher_type = VoucherTypes.MBCardChangeLimit;
          _title = Resource.String("STR_UC_MB_CARD_BTN_EXTEND_LIMIT");
        }
        else
        {
          _voucher_type = VoucherTypes.MBCardSetLimit;
          _title = Resource.String("STR_UC_MB_CARD_BTN_CHANGE_LIMIT");
        }

        if (!form_credit.ShowMB(_title, out _amount_to_add, _voucher_type, _card_data, m_form_yes_no))
        {
          return;
        }

        // If is a Extension and Amount Input is 0, then return. No necessary continue
        if ((OperationLimitUpdates == MB_SALES_LIMIT_OPERATION.EXTEND_LIMIT) && _amount_to_add == 0)
        {
          return;
        }

        // Update the MB balance or sales limt
        if (!CashierBusinessLogic.DB_ChangeMBCardLimit(m_current_card_id, OperationLimitUpdates, _amount_to_add, out _limit_reached, out _remain_credit))
        {
          Log.Error("ChangeLimit: DB_ChangeMBCardLimit. Error changing MB card limit. Mobile Bank: " + m_current_card_balance.ToString());

          return;
        }

        if (_limit_reached)
        {
          if (OperationLimitUpdates == MB_SALES_LIMIT_OPERATION.EXTEND_LIMIT)
          {
            _message = Resource.String("STR_UC_MB_EXTEND_SALES_LIMIT_EXCEED", _remain_credit);
          }
          else
          {
            _message = Resource.String("STR_UC_MB_CHANGE_SALES_LIMIT_EXCEED", _remain_credit);
          }

          _message = _message.Replace("\\r\\n", "\r\n");

          frm_message.Show(_message, _title, MessageBoxButtons.OK, MessageBoxIcon.Question, this.ParentForm);
        }
      }
      finally
      {
        m_form_yes_no.Hide();
        this.RestoreParentFocus();
      }
    } // ChangeLimit

    //------------------------------------------------------------------------------
    // PURPOSE: High level handler for the Change MB limit functionality. 
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //

    private void btn_change_limit_Click(object sender, EventArgs e)
    {
      ChangeLimit(MB_SALES_LIMIT_OPERATION.EXTEND_LIMIT);
    } // btn_change_limit_Click

    //------------------------------------------------------------------------------
    // PURPOSE: High level handler for the Change MB limit functionality. 
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    private void btn_set_limit_Click(object sender, EventArgs e)
    {
      ChangeLimit(MB_SALES_LIMIT_OPERATION.CHANGE_LIMIT);
    } // btn_set_limit_Click

    //------------------------------------------------------------------------------
    // PURPOSE: High level handler for the Mobile Bank Close Cash Session
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    private void btn_close_cash_sesion_Click(object sender, EventArgs e)
    {
      MBCardData _mb;
      String _close_message;

      // Get Card data
      _mb = new MBCardData();
      _close_message = string.Empty;

      if (!CashierBusinessLogic.DB_MBCardGetAllData(m_current_card_id, _mb))
      {
        Log.Error("btn_close_cash_sesion_Click: DB_MBCardGetAllData. Error reading card. Mobile Bank: " + m_current_card_id.ToString());

        return;
      }

      if (m_current_card_id == 0)
      {
        return;
      }

      if (_mb.OverCash > 0)
      {
        _close_message += Resource.String("STR_UC_MB_CARD_DIALOG_CLOSE_SESSION_EXCESS", _mb.OverCash.ToString());
      }

      if (_mb.ShortFallCash > 0)
      {
        _close_message += Resource.String("STR_UC_MB_CARD_DIALOG_CLOSE_SESSION_SHORTFALL", _mb.ShortFallCash.ToString());
      }

      if (_mb.PendingCash > 0)
      {
        _close_message += Resource.String("STR_UC_MB_CARD_DIALOG_CLOSE_SESSION_PENDING", _mb.PendingCash.ToString());
      }

      if (!String.IsNullOrEmpty(_close_message))
      {
        _close_message = Resource.String("STR_UC_MB_CARD_DIALOG_CLOSE_SESSION", _close_message);

        //STR_UC_MB_CARD_DIALOG_CLOSE_SESSION
        if (DialogResult.Cancel == frm_message.Show(_close_message.Replace("\\r\\n", "\r\n"),
                                   Resource.String("STR_APP_GEN_MSG_WARNING"),
                                   MessageBoxButtons.YesNo,
                                   MessageBoxIcon.Stop,
                                   Cashier.MainForm))
        {
          return;
        }
      }
      else
      {
        //STR_UC_MB_CARD_DIALOG_CLOSE_SESSION
        if (DialogResult.Cancel == frm_message.Show(Resource.String("STR_FRM_CONTAINER_USER_MSG_LOG_OFF"),
                                   Resource.String("STR_APP_GEN_MSG_WARNING"),
                                   MessageBoxButtons.YesNo,
                                   MessageBoxIcon.Stop,
                                   Cashier.MainForm))
        {
          return;
        }
      }

      //Mobile Bank Close Sesion 
      if (!CashierBusinessLogic.DB_MBCloseCashSession(_mb))
      {
        Log.Error("btn_close_cash_sesion_Click: DB_MBCardCloseCashSession. Error returning cash. Mobile Bank: " + m_current_card_id.ToString());

        return;
      }

      // Refresh
      RefreshAccount();

    } //btn_close_cash_sesion_Click


    //------------------------------------------------------------------------------
    // PURPOSE: High level handler for the MB Cash deposit functionality. 
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //

    private void btn_cash_deposit_Click(object sender, EventArgs e)
    {
      MBCardData _mb;
      SortedDictionary<CurrencyIsoType, Decimal> _deposit_amount;
      Decimal _total_amount;
      WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS _dummy_session_stats;
      Boolean _dummy_check_redeem;

      // Get Card data
      _mb = new MBCardData();
      if (!CashierBusinessLogic.DB_MBCardGetAllData(m_current_card_id, _mb))
      {
        Log.Error("btn_cash_deposit_Click: DB_MBCardGetAllData. Error reading card. Mobile Bank: " + m_current_card_id.ToString());

        return;
      }

      if (m_current_card_id == 0)
      {
        return;
      }

      m_form_yes_no.Show(this.ParentForm);

      if (!form_credit.ShowMobileBanks(Resource.String("STR_UC_MB_CARD_BTN_CASH_DEPOSIT"), out _deposit_amount, out _total_amount, out _dummy_check_redeem, _mb, 0, CASH_MODE.TOTAL_REDEEM, m_form_yes_no, out _dummy_session_stats))
      {
        m_form_yes_no.Hide();
        this.RestoreParentFocus();
        return;
      }

      m_form_yes_no.Hide();

      this.RestoreParentFocus();

      // After the amount input form, Get Card data again
      if (!CashierBusinessLogic.DB_MBCardGetAllData(m_current_card_id, _mb))
      {
        Log.Error("btn_cash_deposit_Click: DB_MBCardGetAllData. Error reading card. Mobile Bank: " + m_current_card_id.ToString());

        return;
      }

      // Mobile Bank Deposit 
      if (!CashierBusinessLogic.DB_MBCardCashDeposit(_mb, _total_amount))
      {
        Log.Error("btn_cash_deposit_Click: DB_MBCardCashDeposit. Error returning cash. Mobile Bank: " + m_current_card_id.ToString() + ". Amount: " + _total_amount.ToString());

        return;
      }

      // Refresh
      RefreshAccount();

    } // btn_cash_deposit_Click

    //------------------------------------------------------------------------------
    // PURPOSE: High level handler for the MB show last movements functionality. 
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    private void btn_last_movements_Click(object sender, EventArgs e)
    {
      StringBuilder _sb;
      String anon_name;

      if (m_current_card_id == 0)
      {
        return;
      }

      // Query to obtain last cards movements: for current cashier session only!!!
      //sql_str = "	 SELECT TOP 40 MBM_TYPE, '  ' AS TYPE_NAME, MBM_DATETIME, (CASE WHEN (MBM_TYPE = 1) THEN MBM_PLAYER_TRACKDATA ELSE '  ' END) AS USER_NAME, (CASE WHEN (MBM_TYPE = 1) THEN MBM_TERMINAL_NAME ELSE MBM_CASHIER_NAME END) TERMINAL_NAME, (CASE WHEN (MBM_TYPE = 1) THEN MBM_TERMINAL_ID ELSE MBM_CASHIER_ID END) TERMINAL_ID, MBM_INITIAL_BALANCE, MBM_SUB_AMOUNT, MBM_ADD_AMOUNT, MBM_FINAL_BALANCE " +
      //             " FROM MB_MOVEMENTS " +
      //            " WHERE MBM_MB_ID = " + m_current_card_id.ToString() + 
      //            "   AND MBM_CASHIER_SESSION_ID = " + Cashier.SessionId.ToString() + 
      //         " ORDER BY MBM_DATETIME DESC, MBM_MOVEMENT_ID DESC ";

      anon_name = Resource.String("STR_UC_CARD_CARD_HOLDER_ANONYMOUS");

      _sb = new StringBuilder();
      _sb.AppendLine("          SELECT   TOP 150 MBM_TYPE                                                      ");
      _sb.AppendLine("                 , '  ' AS TYPE_NAME                                                     ");
      _sb.AppendLine("                 , MBM_DATETIME                                                          ");
      _sb.AppendLine("                 , (CASE WHEN (MBM_TYPE = 1)                                             ");
      _sb.AppendLine("                           THEN (CASE WHEN (AC_HOLDER_NAME IS NULL)                      ");
      _sb.AppendLine("                                        THEN '" + anon_name + "'                         ");
      _sb.AppendLine("                                        ELSE AC_HOLDER_NAME                              ");
      _sb.AppendLine("                                 END)                                                    ");
      _sb.AppendLine("                           ELSE GU_USERNAME                                              ");
      _sb.AppendLine("                    END)  AS  USER_NAME                                                  ");
      _sb.AppendLine("                 , (CASE WHEN (MBM_TYPE = 1)                                             ");
      _sb.AppendLine("                           THEN MBM_TERMINAL_NAME                                        ");
      _sb.AppendLine("                           ELSE MBM_CASHIER_NAME                                         ");
      _sb.AppendLine("                    END)  AS  TERMINAL_NAME                                              ");
      _sb.AppendLine("                 , (CASE WHEN (MBM_TYPE = 1)                                             ");
      _sb.AppendLine("                           THEN MBM_TERMINAL_ID                                          ");
      _sb.AppendLine("                           ELSE MBM_CASHIER_ID                                           ");
      _sb.AppendLine("                    END)  AS  TERMINAL_ID                                                ");
      _sb.AppendLine("                 , MBM_INITIAL_BALANCE                                                   ");
      _sb.AppendLine("                 , MBM_SUB_AMOUNT                                                        ");
      _sb.AppendLine("                 , MBM_ADD_AMOUNT                                                        ");
      _sb.AppendLine("                 , MBM_FINAL_BALANCE                                                     ");
      _sb.AppendLine("                 , MBM_UNDO_STATUS                                                       ");
      _sb.AppendLine("            FROM   MB_MOVEMENTS                                                          ");
      _sb.AppendLine("      INNER JOIN   CASHIER_SESSIONS  ON  MBM_CASHIER_SESSION_ID  =  CS_SESSION_ID        ");
      _sb.AppendLine("      INNER JOIN   GUI_USERS         ON  CS_USER_ID  =  GU_USER_ID                       ");
      _sb.AppendLine(" LEFT OUTER JOIN   ACCOUNTS          ON  MBM_PLAYER_ACCT_ID  =  ACCOUNTS.AC_ACCOUNT_ID   ");
      _sb.AppendLine("           WHERE   MBM_MB_ID = " + m_current_card_id.ToString());
      _sb.AppendLine("             AND   MBM_CASHIER_SESSION_ID = " + Cashier.SessionId.ToString());
      _sb.AppendLine("        ORDER BY   MBM_DATETIME DESC, MBM_MOVEMENT_ID DESC                               ");

      Cursor _previous_cursor;
      _previous_cursor = this.Cursor;
      this.Cursor = Cursors.WaitCursor;

      try
      {
        form_last_plays.Show(_sb.ToString(), frm_last_movements.MovementShow.Mobile_Bank, m_track_number);
      }
      finally
      {
        this.Cursor = _previous_cursor;
      }
    } // btn_last_movements_Click

    //------------------------------------------------------------------------------
    // PURPOSE: High level handler for the MB Browse functionality (select from the 
    //          list of mobile banks associated to the current cashier session). 
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    private Boolean btn_mb_browse_Click(object sender, EventArgs e)
    {
      Cursor _previous_cursor;
      String _error_str;

      // Check if current user is an authorized user
      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.MobileBankSearchByName,
                                               ProfilePermissions.TypeOperation.RequestPasswd,
                                               this.ParentForm,
                                               out _error_str))
      {
        return false;
      }

      if ((Cashier.SessionId == 0) && (!m_mobibank_enabled))
      {
        return false;
      }

      _previous_cursor = this.Cursor;
      this.Cursor = Cursors.WaitCursor;

      try
      {
        return LoadMobileBankData();
      }
      finally
      {
        this.Cursor = _previous_cursor;
      }
    } // btn_mb_browse_Click

    /// <summary>
    /// Load mobile bank data
    /// </summary>
    /// <returns></returns>
    private Boolean LoadMobileBankData()
    {
      if (m_mobibank_enabled)
      {
        //Get user id
        return LoadMobileBankDataFromUserId();
      }
      else
        {
        //Get track number from mobile bank
        return LoadMobileBankDataFromTrackData();
      }
        }

    private Boolean LoadMobileBankDataFromTrackData()
    {
      String _external_selected_track_number;

      //Get track number from mobile bank
      _external_selected_track_number = GetMobileBankTrackNumber();

      if (String.IsNullOrEmpty(_external_selected_track_number))
        {
        return false;
        }

      //Load card data
      LoadCardByTrackNumber(_external_selected_track_number);

      return true;
    }

    /// <summary>
    /// Get mobile bank data from user id
    /// </summary>
    /// <returns></returns>
    private Boolean LoadMobileBankDataFromUserId()
    {
      //Get user id from mobile bank
      m_mobibank_user_id = GetMobileBankUserId();

      if (MobileBankUserInfo.IsUserIdEmpty(m_mobibank_user_id))
        {
          return false;
        }

      MobileBankUserInfoData _mb_userinfo_data;
      _mb_userinfo_data = GetMobileBankUserInfoData(m_mobibank_user_id);

      CreateMobileBankIfNotExistsForUserId(_mb_userinfo_data);

      //Reset timer count
      tick_count_tracknumber_read = 0;

      MBCardData _mb_card_data;
      _mb_card_data = new MBCardData();
      CashierBusinessLogic.DB_MBCardGetAllData(_mb_userinfo_data.MobileBankId, _mb_card_data);

      //Load card data
      LoadMBCard(_mb_card_data, _mb_card_data.TrackData);

      return true;
    }

    /// <summary>
    /// Get mobile bank track number
    /// </summary>
    /// <returns></returns>
    private String GetMobileBankTrackNumber()
    {
      String _selected_track_number;

      // Obtain mobile bank accounts linked to current cashier session
      // SLE 12-MAR-2010: Fixed DefectId #75 --> Case 1
      using (frm_browse_mobile_banks _form_browse_mobile_banks = new frm_browse_mobile_banks())
      {
        _selected_track_number = _form_browse_mobile_banks.SelectMobileBank();
      }

      return _selected_track_number;
      }

    /// <summary>
    /// Get user id from MobiBank
    /// </summary>
    /// <returns></returns>
    private Int32 GetMobileBankUserId()
    {
      Int32 _user_id;

      using (frm_browse_mobile_banks_device _form_browse_mobile_banks_device = new frm_browse_mobile_banks_device())
      {
        _user_id = _form_browse_mobile_banks_device.SelectUserFromMobileBank();
      }

      return _user_id;
      }

    //------------------------------------------------------------------------------
    // PURPOSE: Enable / Disable main action buttons depending on the current cashier 
    //          session status. 
    // 
    //  PARAMS:
    //      - INPUT:
    //        - CashierStatus
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //

    internal void EnableDisableButtons(CASHIER_STATUS CashierStatus)
    {
      Boolean _is_enabled;

      switch (CashierStatus)
      {
        case CASHIER_STATUS.OPEN:
          _is_enabled = !(this.m_parent != null && this.m_parent.SystemInfo.WorkShiftExpired == SystemInfo.StatusInfo.EXPIRED ||
                           this.m_parent != null && this.m_parent.SystemInfo.GamingDayExpired == SystemInfo.StatusInfo.EXPIRED);

          break;

        case CASHIER_STATUS.CLOSE:
        default:
          _is_enabled = false;

          break;
      }

      SwitchOperationButtons(_is_enabled);

    } // EnableDisableButtons

    private void SwitchOperationButtons(Boolean IsEnabled)
    {
      btn_change_limit.Enabled = IsEnabled;
      btn_cash_deposit.Enabled = IsEnabled;
      btn_set_limit.Enabled = IsEnabled;
      btn_close_sesion.Enabled = IsEnabled;
      btn_undo_operation.Enabled = IsEnabled;
    }

    public void ClearCardData()
    {
      m_track_number = String.Empty;
      InitializeMobiBankUserId();

      RefreshData(new MBCardData());
    } // ClearCardData

    public void InitFocus()
    {
      uc_card_reader1.Focus();
    } // InitFocus

    //------------------------------------------------------------------------------
    // PURPOSE: Event fired when a card is swiped. 
    // 
    //  PARAMS:
    //      - INPUT:
    //        - TrackNumber
    //
    //      - OUTPUT:
    //        - IsValid
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    public void uc_card_reader1_OnTrackNumberReadEvent(string TrackNumber, ref Boolean IsValid)
    {
      tick_count_tracknumber_read = 0;

      // Load card data from track number
      LoadCardByTrackNumber(TrackNumber);

      if (m_track_number == "" || m_track_number == null)
      {
        IsValid = false;
      }

      if (!IsValid)
      {
        EnableDisableButtons(IsValid);
      }
    } // uc_card_reader1_OnTrackNumberReadEvent

    //------------------------------------------------------------------------------
    // PURPOSE: Change the status of the cashier buttons based on the current cashier 
    //          session status (open / not open), the account status (locked / in use) 
    //          and the incoming parameter.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - EnableButtons
    //              - true: try to enable buttons, actually enabled buttons depend on 
    //                      cashier and account status.
    //              - false: try to disable buttons
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //   CurrentCashierSessionId is set to NULL:
    //        When cashier session is closed and Reopen Mode is "No Reopen" or "Reopen in this session"
    //        When mobile bank session is closed and Reopen Mode is "Reopen in other session" or "Reopen always" 
    //
    private void EnableDisableButtons(Boolean EnableButtons)
    {
      Boolean _button_enable_flag;
      Boolean _label_enable_flag;
      CASHIER_STATUS _status;
      MBMovementType? _mb_last_movement_type_in_this_cs;
      MBMovementType? _mb_last_movement_type_in_other_cs;

      _button_enable_flag = EnableButtons;

      if (account_watcher != null)
      {
        _label_enable_flag = account_watcher.MBCard.SessionClosed;

        LastMovementInCashierSessions(account_watcher.MBCard.CardId, out _mb_last_movement_type_in_this_cs, out _mb_last_movement_type_in_other_cs);

        // Enable/Disable buttons & visible/hiden label "Session Closed", depend on the parameter 
        switch (MobileBank.m_reopen_mode)
        {
          case ReopenMode.NoReopen:
            _button_enable_flag = _button_enable_flag && !account_watcher.MBCard.SessionClosed;
            if (account_watcher.MBCard.CurrentCashierSessionId != 0 && account_watcher.MBCard.CurrentCashierSessionId != Cashier.SessionId)
            {
              // Disable buttons, the card is associate to other open cashier session
              _button_enable_flag = false;
            }

            break;

          case ReopenMode.ReopenInThisSession:
            if (_mb_last_movement_type_in_other_cs != null && account_watcher.MBCard.CurrentCashierSessionId != Cashier.SessionId)
            {
              _button_enable_flag = false;
            }
            if (account_watcher.MBCard.CurrentCashierSessionId != Cashier.SessionId)
            {
              _label_enable_flag = false;
            }

            break;

          case ReopenMode.ReopenInOtherSession:
            if ((account_watcher.MBCard.CurrentCashierSessionId != Cashier.SessionId && _mb_last_movement_type_in_other_cs != null && _mb_last_movement_type_in_other_cs != MBMovementType.CloseSession)
              || account_watcher.MBCard.SessionClosed)
            {
              // the card already has BM close session in this cashier session
              _button_enable_flag = false;
            }

            break;

          case ReopenMode.ReopenAlways:
            if (_mb_last_movement_type_in_other_cs != null && _mb_last_movement_type_in_other_cs != MBMovementType.CloseSession)
            {
              _button_enable_flag = false;
            }
            _label_enable_flag = false;

            break;
        }

        // Show/Hide label
        uc_mb_card_balance1.ShowClosedSessionLabel(_label_enable_flag);

        // Enable last movements button if it has anything show
        btn_last_movements.Enabled = EnableButtons & account_watcher.MBCard.HadMovementsInThisCashierSession;
      }
      else
      {
        // Not initialized any cards
        uc_mb_card_balance1.ShowClosedSessionLabel(false);
        btn_last_movements.Enabled = false;
        _button_enable_flag = false;
      }

      _status = CashierBusinessLogic.ReadCashierStatus(Cashier.SessionId);

      lbl_pin.Visible = (_button_enable_flag && _status == CASHIER_STATUS.OPEN);

      timer1.Enabled = EnableButtons;

      ToggleApparenceCtrl(this, EnableButtons);

      btn_new_card.Enabled = _button_enable_flag;
      btn_account_edit.Enabled = _button_enable_flag;
      //btn_card_block.Enabled = _button_enable_flag;
      //btn_change_pin.Enabled = _button_enable_flag;
      //btn_limits_edit.Enabled = _button_enable_flag;
      btn_change_limit.Enabled = _button_enable_flag;

      btn_card_block.Enabled = true;
      btn_change_pin.Enabled = true;
      btn_limits_edit.Enabled = true;

      if (account_watcher != null)
      {
        btn_close_sesion.Enabled = _button_enable_flag && (account_watcher.MBCard.CurrentCashierSessionId != 0) && (!account_watcher.MBCard.SessionClosed);
      }

      btn_undo_operation.Enabled = btn_close_sesion.Enabled;

      btn_set_limit.Enabled = _button_enable_flag;

      btn_print_card.Enabled = (_button_enable_flag && card_printer.PrinterExist());

      lbl_account_id_name.Visible = (!m_mobibank_enabled);
      lbl_account_id_value.Visible = (!m_mobibank_enabled);

      btn_cash_deposit.Enabled = _button_enable_flag;

      if (_button_enable_flag)
      {
        if (_status != CASHIER_STATUS.OPEN)
        {
          EnableDisableButtons(CASHIER_STATUS.CLOSE);

          m_parent.ChangeLabelStatus();
        }
      }

      // Check if work shift has expired
      if (this.m_parent != null && Cashier.SessionId != 0 && this.m_parent.SystemInfo.WorkShiftExpired == SystemInfo.StatusInfo.EXPIRED ||
          this.m_parent != null && this.m_parent.SystemInfo.GamingDayExpired == SystemInfo.StatusInfo.EXPIRED)
      {
        btn_new_card.Enabled = false;
        btn_account_edit.Enabled = false;
        btn_card_block.Enabled = false;
        btn_change_pin.Enabled = false;
        btn_limits_edit.Enabled = false;
        btn_change_limit.Enabled = false;
        btn_set_limit.Enabled = false;
        btn_print_card.Enabled = false;
        btn_last_movements.Enabled = false;
        btn_undo_operation.Enabled = false;
      }

      // JMV 09/12/14 - DUT - WIGOS 201412: Logrand - Bancos m�viles, Dep�sitos           
      if (btn_cash_deposit.Enabled)
      {
        if (account_watcher != null)
        {
          btn_cash_deposit.Enabled = (account_watcher.MBCard.PendingCash > 0);
        }
      }
    } // EnableDisableButtons

    //------------------------------------------------------------------------------
    // PURPOSE: Change the appearance of all controls contained in the specified container 
    //          (first parameter) based on the second parameter.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Ctrl
    //          - Enabled
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    private void ToggleApparenceCtrl(Control Ctrl, Boolean Enabled)
    {
      if (Ctrl.Equals(uc_card_reader1)
          || Ctrl is Button)
      {
        return;
      }

      if (!Ctrl.Equals(this))
      {
        Ctrl.Enabled = Enabled;
      }

      foreach (Control ctrl in Ctrl.Controls)
      {
        ToggleApparenceCtrl(ctrl, Enabled);
      }
    } // ToggleApparenceCtrl

    //------------------------------------------------------------------------------
    // PURPOSE: Handler for the timer1 tick event, which is used to disable action
    //          buttons after CashierBusinessLogic.DURATION_BUTTONS_ENABLED_AFTER_OPERATION seconds
    //          of introducing the track number.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //

    private void timer1_Tick(object sender, EventArgs e)
    {
      tick_count_tracknumber_read += 1;

      if (tick_count_tracknumber_read >= CashierBusinessLogic.DURATION_BUTTONS_ENABLED_AFTER_OPERATION)
      {
        // MBF 21-JAN-11 TO DO IN THE FUTURE
        // ClearCardData();
        EnableDisableButtons(false);

        //JCM 02-MAY-2012 Ensure focus is in the card reader after enable/disable buttons
        InitFocus();

        return;
      }
    } // timer1_Tick

    private void uc_card_reader1_OnTrackNumberReadingEvent()
    {
      EnableDisableButtons(false);
    } // uc_card_reader1_OnTrackNumberReadingEvent

    //------------------------------------------------------------------------------
    // PURPOSE: High level handler for the MB Block / Unblock feature.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //

    private void btn_block_Click(object sender, EventArgs e)
    {
      DialogResult dr;
      MBCardData card;

      card = account_watcher.MBCard;
      if (!card.Blocked)
      {
        dr = frm_message.Show(Resource.String("STR_UC_CARD_USER_MSG_ACTION_LOCK"), Resource.String("STR_UC_CARD_BLOCK_ACTION_LOCK"), MessageBoxButtons.YesNo, MessageBoxIcon.Question, this.ParentForm);
      }
      else
      {
        dr = frm_message.Show(Resource.String("STR_UC_CARD_USER_MSG_ACTION_UNLOCK"), Resource.String("STR_UC_CARD_BLOCK_ACTION_UNLOCK"), MessageBoxButtons.YesNo, MessageBoxIcon.Question, this.ParentForm);
      }

      if (dr != DialogResult.OK)
      {
        return;
      }

      card.Blocked = !card.Blocked;
      if (!CashierBusinessLogic.DB_MBCardUpdateInfo(card))
      {
        // TODO: Show Error
        ClearCardData();

        Log.Error("btn_block_Click: DB_MBCardUpdateInfo. Error updating card (" + card.CardId.ToString() + ").");

        return;
      }

      RefreshAccount();
    } // btn_block_Click

    private void RefreshAccount()
    {
      account_watcher.ForceRefresh();
    }

    //------------------------------------------------------------------------------
    // PURPOSE: High level handler for the change account mode and holder name MB 
    //          functionality.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Ctrl
    //          - Enabled
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //

    private void btn_edit_Click(object sender, EventArgs e)
    {
      frm_mb_account_user_edit account_edit = new frm_mb_account_user_edit(/*this*/);
      frm_yesno shader = new frm_yesno();

      shader.Opacity = 0.6;
      shader.Show();

      if (anonymous)
      {
        account_edit.GetUserData(this.lbl_account_id_value.Text);
      }
      else
      {
        account_edit.GetUserData(this.lbl_account_id_value.Text, this.lbl_holder_name.Text);
      }

      if (WindowManager.StartUpMode == WindowManager.StartUpModes.Explorer)
      {
        account_edit.Location = new Point(WindowManager.GetFormCenterLocation(shader).X - (account_edit.Width / 2), shader.Location.Y + 100);
      }
      account_edit.ShowDialog(shader);

      shader.Hide();
      shader.Dispose();

      account_edit.Hide();
      account_edit.Dispose();

      this.LoadCardByTrackNumber(lbl_track_number.Text);
    } // btn_edit_Click

    //------------------------------------------------------------------------------
    // PURPOSE: High level handler for the change account mode and holder name MB 
    //          functionality.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Ctrl
    //          - Enabled
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //

    private void btn_limits_edit_Click(object sender, EventArgs e)
    {
      String _error_str;
      frm_mb_account_limits_edit account_edit = new frm_mb_account_limits_edit(/*this*/);
      frm_yesno shader;

      // Check if current user is an authorized user
      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.MobileBankChangeLimits,
                                   ProfilePermissions.TypeOperation.ShowMsg,
                                   m_parent,
                                   out _error_str))
      {
        return;
      }

      shader = new frm_yesno();

      shader.Opacity = 0.6;
      shader.Show();

      account_edit.GetUserData(this.lbl_account_id_value.Text);

      if (WindowManager.StartUpMode == WindowManager.StartUpModes.Explorer)
      {
        account_edit.Location = new Point(WindowManager.GetFormCenterLocation(shader).X - (account_edit.Width / 2), shader.Location.Y + 100);
      }
      account_edit.ShowDialog(shader);

      shader.Hide();
      shader.Dispose();

      account_edit.Hide();
      account_edit.Dispose();

      this.LoadCardByTrackNumber(lbl_track_number.Text);
    } // btn_limits_edit_Click

    //------------------------------------------------------------------------------
    // PURPOSE: High level handler for the action buttons in the mobile banks screen.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //

    private void btn_button_clicked(object sender, EventArgs e)
    {
      Boolean _enabled_buttons_after_operation;
      Boolean _is_track_valid;

      Cashier.ResetAuthorizedByUser();

      if (!uc_card_reader1.Focused)
      {
        uc_card_reader1.Focus();
      }

      try
      {
        if (sender.Equals(btn_new_card))
        {
          // This one is externally called
          // btn_new_card_Click (sender, e);
        }
        if (sender.Equals(btn_account_edit))
        {
          btn_edit_Click(sender, e);
        }
        if (sender.Equals(btn_card_block))
        {
          btn_block_Click(sender, e);
        }
        if (sender.Equals(btn_change_pin))
        {
          btn_change_pin_Click(sender, e);
        }
        if (sender.Equals(btn_limits_edit))
        {
          btn_limits_edit_Click(sender, e);
        }
        if (sender.Equals(btn_change_limit))
        {
          btn_change_limit_Click(sender, e);
        }
        if (sender.Equals(btn_cash_deposit))
        {
          btn_cash_deposit_Click(sender, e);
        }
        if (sender.Equals(btn_set_limit))
        {
          btn_set_limit_Click(sender, e);
        }
        if (sender.Equals(btn_close_sesion))
        {
          btn_close_cash_sesion_Click(sender, e);
        }
        if (sender.Equals(btn_last_movements))
        {
          btn_last_movements_Click(sender, e);
        }
        if (sender.Equals(btn_print_card))
        {
          btn_print_card_Click(sender, e);
        }
        if (sender.Equals(btn_mb_browse))
        {
          _is_track_valid = btn_mb_browse_Click(sender, e);

          EnableDisableButtons(_is_track_valid);

          return;
        }
        if (sender.Equals(btn_undo_operation))
        {
          btn_undo_operation_click(sender, e);
        }

        _enabled_buttons_after_operation = (WSI.Common.Misc.ReadGeneralParams("Cashier", "KeepEnabledButtonsAfterOperation") == "1");

        if (_enabled_buttons_after_operation)
        {
          tick_count_tracknumber_read = 0;
          EnableDisableButtons(true);
        }
        else
        {
          EnableDisableButtons(false);
        }
      }
      finally
      {
        Cashier.ResetAuthorizedByUser();

        if (!uc_card_reader1.Focused)
        {
          uc_card_reader1.Focus();
        }
      }
    } // btn_button_clicked

    //------------------------------------------------------------------------------
    // PURPOSE: High level handler for the MB create new card functionality.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //

    private void btn_new_card_Click(object sender, EventArgs e)
    {
      using (frm_yesno _shader = new frm_yesno())
      {
        using (frm_mb_card_assign _mb_card_assign = new frm_mb_card_assign())
        {
          _shader.Opacity = 0.6f;
          _shader.Show();

          _mb_card_assign.GetCardData(Convert.ToInt64(this.lbl_account_id_value.Text), Convert.ToUInt64(this.lbl_track_number.Text));
          _mb_card_assign.ShowDialog(_shader);

          _mb_card_assign.Hide();
          _mb_card_assign.Dispose();
        }
      }

      RefreshAccount();
    } // btn_new_card_Click

    //------------------------------------------------------------------------------
    // PURPOSE: High level handler for the MB change pin functionality.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    private void btn_change_pin_Click(object sender, EventArgs e)
    {
      using (frm_yesno _shader = new frm_yesno())
      {
        using (frm_change_pin _change_pin = new frm_change_pin(_shader))
        {
          _shader.Opacity = 0.6f;
          _shader.Show();

          _change_pin.GetMBCardData(Convert.ToInt64(this.lbl_account_id_value.Text), this.lbl_track_number.Text, this.lbl_holder_name.Text, this.lbl_pin.Text);

          _change_pin.ShowDialog(_shader);
        }
      }

      RefreshAccount();
    } // btn_change_pin_Click

    private void btn_print_card_Click(object sender, EventArgs e)
    {
      // Check if current user is an authorized user
      if (!CheckIfCanPrintDocument())
      {
        return;
      }

      // Send document to print
      Cursor _previous_cursor;
      _previous_cursor = this.Cursor;
      this.Cursor = Cursors.WaitCursor;

      try
      {
        card_printer.PrintNameInCard(lbl_holder_name.Text);
      }
      finally
      {
        this.Cursor = _previous_cursor;
      }
    } // btn_print_card_Click

    /// <summary>
    /// Check if can print document
    /// </summary>
    /// <returns></returns>
    private Boolean CheckIfCanPrintDocument()
    {
      String error_str;
      DialogResult msg_answer;

      // Check if current user is an authorized user
      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.PrintCard,
                                                 ProfilePermissions.TypeOperation.ShowMsg,
                                                 this.ParentForm,
                                                 out error_str))
      {
        return false;
      }

      // Check the printer 
      if (!card_printer.PrinterExist())
      {
        frm_message.Show(Resource.String("STR_PRINT_CARD_PRINTER_IS_NOT_INSTALLED"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this.ParentForm);

        return false;
      }

      // Ask for confirmation before printing 
      msg_answer = frm_message.Show(Resource.String("STR_PRINT_CARD_PRINTER_INPUT_INFO"),
                                    Resource.String("STR_PRINT_CARD_PRINTER_INPUT_INFO_MSG"),
                                    MessageBoxButtons.YesNo,
                                    MessageBoxIcon.Warning,
                                    this.ParentForm);

      return (msg_answer != DialogResult.Cancel);
      }

    //------------------------------------------------------------------------------
    // PURPOSE: Undo the last operation 
    // 
    //  PARAMS:
    //      - INPUT: 
    //            - Object: sender
    //            - Eventargs: e
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    private void btn_undo_operation_click(object sender, EventArgs e)
    {
      List<frm_browse.ButtonType> _list_buttons;
      frm_browse.ButtonType _btn_type;
      OperationCode _selected_operation;
      Object _selected_value;
      Color _background_color;
      Color _button_color;

      _background_color = Color.Transparent; // Color.PaleGoldenrod;
      _button_color = Color.Transparent; // Color.LightGoldenrodYellow;

      _list_buttons = new List<frm_browse.ButtonType>();

      _btn_type = new frm_browse.ButtonType();
      _btn_type.Description = Resource.String("STR_MB_UNDO_RECHARGE");
      _btn_type.ReturnedElement = OperationCode.MB_CASH_IN;
      _btn_type.Enabled = true;
      _list_buttons.Add(_btn_type);

      _btn_type = new frm_browse.ButtonType();
      _btn_type.Description = Resource.String("STR_MB_UNDO_DEPOSIT");
      _btn_type.ReturnedElement = OperationCode.MB_DEPOSIT;
      _btn_type.Enabled = true;
      _list_buttons.Add(_btn_type);

      using (frm_browse _browser = new frm_browse(BrowserType.UndoOperation, _background_color, _button_color))
      {
        if (!_browser.Show(_list_buttons, out _selected_value))
        {
          return;
        }
      }

      _selected_operation = (OperationCode)_selected_value;

      UndoMBOperation(_selected_operation);
    }
    #endregion Buttons

  }
}
