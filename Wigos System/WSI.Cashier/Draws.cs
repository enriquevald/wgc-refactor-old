//------------------------------------------------------------------------------
// Copyright � 2011 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Draws.cs
// 
//   DESCRIPTION: 
//
//        AUTHOR: RCI
// 
// CREATION DATE: 27-APR-2011
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 27-APR-2011 RCI    First release.
// 20-OCT-2011 MPO    Added new fields: Limit per operation and first cash in constrained
// 21-DEC-2011 RCI    Added Bingo format flag
// 30-DEC-2011 MPO    Added new type of draw: CashInPosteriori, and generic function: GetGroupByOperations
// 02-JAN-2012 JMM    CashInPosteriori draws loading enabled
// 04-JAN-2012 JMM    CashInPosteriori draws small issues fixing
// 09-MAR-2012 SSC    Added new Operation Codes for Note Acceptor
// 25-MAY-2012 SSC    Moved CardData, PlaySession and PlayerTrackingData classes to WSI.Common
// 30-MAY-2012 SSC    Moved Draws, DrawNumbers and DrawNumberList classes to WSI.Common 
// 22-OCT-2013 LEM    Unify saving account and cashier movements using SharedMovementsTables clases
// 07-NOV-2013 ICS    Fixed Bug WIG-397: Error printing multiple vouchers of draw numbers
// 23-FEB-2018 FJC    Fixed Bug 31732:WIGOS-7370 [Ticket #11400] Antara - Folios de sorteos no asignados al cliente
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using WSI.Common;
using System.Data;

namespace WSI.Cashier
{

  public class DrawsCashier : Draws
  {

    //------------------------------------------------------------------------------
    // PURPOSE: Generate all the tickets for all the draws available for the account.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - CardData Card
    //          - Boolean OnlyCheck
    //          - DrawNumberList DrawsToGenerate
    //          - SqlTransaction SqlTrx
    //
    //      - OUTPUT:
    //          - DrawNumberList RealAwardedNumbersPerDraw
    //
    // RETURNS:
    //      - True: Generation was correctly. False: Otherwise.
    // 
    //   NOTES:
    public static Boolean GenerateTickets(CardData Card,
                                          Boolean OnlyCheck,
                                          DrawNumberList DrawsToGenerate,
                                          out DrawNumberList AwardedNumbersPerDraw)
    {

      List<DrawTicket> _tickets;
      Boolean _account_is_anonymous;
      Int64 _operation_id;

      CashierMovementsTable _cashier_mov_table;
      AccountMovementsTable _account_mov_table;
      CashierSessionInfo _session_info;

      AwardedNumbersPerDraw = new DrawNumberList();

      _account_is_anonymous = (String.IsNullOrEmpty(Card.PlayerTracking.HolderName.Trim()));

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {

          // Can't generate draw tickets if the Cashier is closed (only for draw tickets).
          if (!WSI.Common.Cashier.IsSessionOpen(Cashier.SessionId, _db_trx.SqlTransaction))
          {
            return false;
          }

          if (!CommonGenerateTickets(Card, OnlyCheck, DrawsToGenerate, Cashier.SessionId, _db_trx.SqlTransaction,
                                       out AwardedNumbersPerDraw, out _tickets, out _operation_id))
          {
            Log.Error(string.Format("GenerateTickets() . CashierSessionId: {0} ", Cashier.SessionId));

            return false;
          }

          if (!OnlyCheck)
          {
            _session_info = Cashier.CashierSessionInfo();

            _account_mov_table = new AccountMovementsTable(_session_info);
            _cashier_mov_table = new CashierMovementsTable(_session_info);

            for (int i = 0; i < _tickets.Count; i++)
            {
              if(!_account_mov_table.Add(_operation_id, Card.AccountId, MovementType.DrawTicketPrint, Card.CurrentBalance, 0, 0, Card.CurrentBalance)) 
              {
                return false;
              }
              
              if (!_cashier_mov_table.Add(_operation_id, CASHIER_MOVEMENT.DRAW_TICKET_PRINT, 0, Card.AccountId, Card.TrackData)) 
              {
                return false;
              }
            }
            if (!_account_mov_table.Save(_db_trx.SqlTransaction))
            {
              return false;
            }
            if (!_cashier_mov_table.Save(_db_trx.SqlTransaction))
            {
              return false;
            }
          }

          _db_trx.Commit();
        }

        foreach (DrawTicket _ticket in _tickets)
        {
          VoucherPrint.Print(_ticket.Voucher);
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }

    } // GenerateTickets

  }  // Draws

} // WSI.Cashier
