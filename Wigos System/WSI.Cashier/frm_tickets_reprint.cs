

//------------------------------------------------------------------------------
// Copyright � 2010 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_tickets_reprint.cs
// 
//   DESCRIPTION: Implements the following classes:
//                1. frm_tickets_reprint: List last 20 and current cashier sessions and,
//                   allows to print one or all operation tickets associated.
//
//        AUTHOR: JRM, cloned from frm_voucher_reprint
// 
// CREATION DATE: 17-SEP-2013
//
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 17-SEP-2013 JRM    First release.
// 20-SEP-2013 NMR    Deleted confirmation for Ticket Reprint
// 20-SEP-2013 JRM    Removed session table and functionallity, added date search filter for tickets
// 27-SEP-2013 NMR    Some aditional changes in design; changued search tickets condition
// 01-OCT-2013 NMR    Fixed error in dialog result; prevent printing when cashier is closed
// 03-OCT-2013 NMR    Deleted Ticket Movement
// 07-OCT-2013 HBB    Moved the function GetFilteredTicketsList from PrintMobileBankTickets
// 10-OCT-2013 NMR    Added new TITO permissions with tickets
// 24-OCT-2013 JBP    Redesign filter. 
// 11-NOV-2013 LEM    Show form in mode ticket list selection 
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using WSI.Common;
using WSI.Common.TITO;
using WSI.Cashier.TITO;
using System.IO;
using System.Drawing.Text;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Globalization;

namespace WSI.Cashier
{
  public partial class frm_tickets_reprint : Form
  {
    #region Constants

    // Number of columns
    private const Int32 GRID_COLUMN_MAX_COLUMNS = 5; // Wigos GUI Style, not really needed here

    // Columns defined
    private const Int32 GRID_COLUMN_TERMINAL_NAME = 0;
    private const Int32 GRID_COLUMN_FLOOR_ID = 1;
    private const Int32 GRID_COLUMN_LAST_ACTION_DATE = 2;
    private const Int32 GRID_COLUMN_VALIDATION_NUMBER = 3;
    private const Int32 GRID_COLUMN_AMOUNT = 4;
    private const Int32 GRID_COLUMN_TICKET_ID = 5;

    // Column widths
    private const Int32 GRID_COLUMN_WIDTH_TERMINAL_NAME = 160;
    private const Int32 GRID_COLUMN_WIDTH_FLOOR_ID = 100;
    private const Int32 GRID_COLUMN_WIDTH_LAST_ACTION_DATE = 130;
    private const Int32 GRID_COLUMN_WIDTH_VALIDATION_NUMBER = 220;
    private const Int32 GRID_COLUMN_WIDTH_AMOUNT = 145;
    private const Int32 GRID_COLUMN_WIDTH_TICKET_ID = 0;

    private const Int32 CREATED_IN_LAST_MINUTES = 60;
    #endregion // Constants

    #region Attributes

    private CASHIER_STATUS m_cashier_status;
    private frm_yesno form_yes_no;
    private Boolean m_ticket_selection_mode;
    private Int32 m_selected_row_index;


    #endregion // Attributes

    #region Constructor

    public frm_tickets_reprint()
    {
      InitializeComponent();
      InitializeControlResources();

      form_yes_no = new frm_yesno();
      form_yes_no.Opacity = 0.6;
    }

    #endregion // Constructor

    #region Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Initialize control resources (NLS strings, images, etc).
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public void InitializeControlResources()
    {
      // labels 
      lbl_tickets_reprint_title.Text = Resource.String("STR_FRM_VOUCHERS_REPRINT_054");        // Ticket Reprint
      lbl_terminal.Text = Resource.String("STR_FRM_VOUCHERS_REPRINT_066");        // Terminal
      lbl_floor_id.Text = Resource.String("STR_FRM_VOUCHERS_REPRINT_067");        // Name
      lbl_ticket_number.Text = Resource.String("STR_TICKET_NUMBER");                   // Ticket Number

      // Buttons
      btn_ticket_search.Text = Resource.String("STR_FRM_VOUCHERS_REPRINT_059");
      btn_close.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CLOSE");    // Close
      btn_print_selected.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_REPRINT");  // Manual Entry

      // Textboxes
      gb_search.Text = Resource.String("STR_FRM_VOUCHERS_REPRINT_053");

      // Groupboxes
      gb_last_operations.Text = "";// Resource.String("STR_FRM_VOUCHERS_REPRINT_054");        // Lastest tickets

    } // InitializeControlResources

    //------------------------------------------------------------------------------
    // PURPOSE : Initialize Operations Data Grid.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private void InitOperationsDataGrid()
    {
      String _date_format;
      Int32 _width_date_col;

      _date_format = System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern;

      // Terminal name
      dgv_last_operations.Columns[GRID_COLUMN_TERMINAL_NAME].Width = GRID_COLUMN_WIDTH_TERMINAL_NAME;
      dgv_last_operations.Columns[GRID_COLUMN_TERMINAL_NAME].HeaderCell.Value = Resource.String("STR_FRM_VOUCHERS_REPRINT_069");

      // Last Operations DataGrid
      dgv_last_operations.ColumnHeadersHeight = 30;
      dgv_last_operations.RowTemplate.Height = 30;
      dgv_last_operations.RowTemplate.DividerHeight = 0;

      // Floor ID
      dgv_last_operations.Columns[GRID_COLUMN_FLOOR_ID].Width = GRID_COLUMN_WIDTH_FLOOR_ID;
      dgv_last_operations.Columns[GRID_COLUMN_FLOOR_ID].HeaderCell.Value = Resource.String("STR_FRM_VOUCHERS_REPRINT_067");
      dgv_last_operations.Columns[GRID_COLUMN_FLOOR_ID].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
      dgv_last_operations.Columns[GRID_COLUMN_FLOOR_ID].Visible = !m_ticket_selection_mode;

      // last ticket operation date
      dgv_last_operations.Columns[GRID_COLUMN_LAST_ACTION_DATE].HeaderCell.Value = Resource.String("STR_FRM_VOUCHERS_REPRINT_055");
      if (m_ticket_selection_mode)
      {
        _date_format += " " + CultureInfo.InvariantCulture.DateTimeFormat.LongTimePattern;
        _width_date_col = GRID_COLUMN_WIDTH_LAST_ACTION_DATE + GRID_COLUMN_WIDTH_FLOOR_ID;
      }
      else
      {
        _date_format = CultureInfo.InvariantCulture.DateTimeFormat.LongTimePattern;
        _width_date_col = GRID_COLUMN_WIDTH_LAST_ACTION_DATE;
      }
      dgv_last_operations.Columns[GRID_COLUMN_LAST_ACTION_DATE].Width = _width_date_col;
      dgv_last_operations.Columns[GRID_COLUMN_LAST_ACTION_DATE].DefaultCellStyle.Format = _date_format;

      // ticket validation number
      dgv_last_operations.Columns[GRID_COLUMN_VALIDATION_NUMBER].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
      dgv_last_operations.Columns[GRID_COLUMN_VALIDATION_NUMBER].Width = GRID_COLUMN_WIDTH_VALIDATION_NUMBER;
      dgv_last_operations.Columns[GRID_COLUMN_VALIDATION_NUMBER].HeaderCell.Value = Resource.String("STR_FRM_VOUCHERS_REPRINT_056");

      // ticket amount
      dgv_last_operations.Columns[GRID_COLUMN_AMOUNT].Width = GRID_COLUMN_WIDTH_AMOUNT;
      dgv_last_operations.Columns[GRID_COLUMN_AMOUNT].HeaderCell.Value = Resource.String("STR_FRM_VOUCHERS_REPRINT_057");
      dgv_last_operations.Columns[GRID_COLUMN_AMOUNT].DefaultCellStyle.Format = "c";
      dgv_last_operations.Columns[GRID_COLUMN_AMOUNT].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

      // ticket id
      dgv_last_operations.Columns[GRID_COLUMN_TICKET_ID].Width = GRID_COLUMN_WIDTH_TICKET_ID;
      dgv_last_operations.Columns[GRID_COLUMN_TICKET_ID].HeaderCell.Value = "TI_TICKET_ID";
      dgv_last_operations.Columns[GRID_COLUMN_TICKET_ID].Visible = false;

    } // InitOperationsDataGrid

    //------------------------------------------------------------------------------
    // PURPOSE : Load last operations from a CashierSessionId to the LastOperations DataGridView.
    //
    //  PARAMS :
    //      - INPUT :
    //          - CashierSessionId
    //          - SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private void LoadTicketsList(DataTable TicketsTable)
    {
      String[] _message_params = { "" };
      DataColumn _col;
      TITO_VALIDATION_TYPE _validation_type;
      Int64 _ticket_number;

      try
      {
        //Code to Format the Validation Number
        _col = TicketsTable.Columns.Add("VALIDATION_NUMBER", typeof(String));
        _col.SetOrdinal(GRID_COLUMN_VALIDATION_NUMBER);
        foreach (DataRow _row in TicketsTable.Rows)
        {
          _ticket_number = ValidationNumberManager.UnFormatValidationNumber(_row["TI_VALIDATION_NUMBER"].ToString(), out _validation_type);
          _row["VALIDATION_NUMBER"] = ValidationNumberManager.FormatValidationNumber(_ticket_number, (Int32)_validation_type, true);
        }
        TicketsTable.Columns.Remove("TI_VALIDATION_NUMBER");

        dgv_last_operations.DataSource = TicketsTable;

        InitOperationsDataGrid();

        SetButtonStatus(TicketsTable.Rows.Count > 0);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } // LoadOperations

    #endregion // Private Methods


    //------------------------------------------------------------------------------
    // PURPOSE : Get DataTable with tickets
    //
    //  PARAMS :
    //      - INPUT :
    //          - String: SearchString
    //          - SqlTransaction: Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - DataTable: Ticket List
    //
    private DataTable GetFilteredTicketsList()
    {
      DataTable _dt_tickets;
      StringBuilder _sb;
      String _floor_id;
      String _ticket_number;
      String _provider_id;
      Int32 _terminal_id;

      _ticket_number = txt_ticket_number.Text.Trim();
      _floor_id = txt_floorID.Text.Trim();
      _terminal_id = uc_prov_terminals.TerminalSelected();
      _provider_id = uc_prov_terminals.ProviderSelected();
      _dt_tickets = new DataTable("TICKETS");
      _sb = new StringBuilder();

      try
      {
        _sb.AppendLine("    SELECT   TOP 100                                            ");
        _sb.AppendLine("             TE_NAME                                            ");
        _sb.AppendLine("           , TE_FLOOR_ID                                        ");
        _sb.AppendLine("           , TI_CREATED_DATETIME                                ");
        _sb.AppendLine("           , TI_VALIDATION_NUMBER                               ");
        _sb.AppendLine("           , TI_AMOUNT                                          ");
        _sb.AppendLine("           , TI_TICKET_ID                                       ");
        _sb.AppendLine("      FROM   TICKETS                                            ");
        _sb.AppendLine(" LEFT JOIN   TERMINALS                                          ");
        _sb.AppendLine("        ON   TE_TERMINAL_ID            = TI_CREATED_TERMINAL_ID ");
        _sb.AppendLine("     WHERE   TI_CREATED_DATETIME       > @pStartDate            ");
        _sb.AppendLine("       AND   TI_CREATED_TERMINAL_TYPE  = @pTerminalType         ");
        _sb.AppendLine("       AND   TI_STATUS                 = @pTicketStatus         ");
        _sb.AppendLine("       AND   TI_TYPE_ID     IN (@pCashable,@pPromoRE,@pPromoNR) ");

        if (_terminal_id > 0)
        {
          _sb.AppendLine("       AND   TE_TERMINAL_ID            = @pTerminalId           ");
        }
        if (!String.IsNullOrEmpty(_provider_id))
        {
          _sb.AppendLine("       AND   TE_PROVIDER_ID            = @pProviderId           ");
        }
        if (_floor_id.Length > 0)
        {
          _sb.AppendLine("       AND   TE_FLOOR_ID               LIKE '%'+@pFloorId+'%'   ");
        }
        if (_ticket_number.Length > 0)
        {
          _sb.AppendLine("       AND  RIGHT(TI_VALIDATION_NUMBER, LEN(@pValidationNumber)) = @pValidationNumber ");
        }

        _sb.AppendLine("  ORDER BY   TI_CREATED_DATETIME DESC                           ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            if (_terminal_id > 0)
            {
              _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = _terminal_id;
            }
            if (!String.IsNullOrEmpty(_provider_id))
            {
              _cmd.Parameters.Add("@pProviderId", SqlDbType.NVarChar, 50).Value = _provider_id;
            }
            if (!String.IsNullOrEmpty(_floor_id))
            {
              _cmd.Parameters.Add("@pFloorId", SqlDbType.NVarChar, 20).Value = _floor_id;
            }
            if (!String.IsNullOrEmpty(_ticket_number))
            {
              _cmd.Parameters.Add("@pValidationNumber", SqlDbType.NVarChar, 20).Value = _ticket_number;
            }

            _cmd.Parameters.Add("@pTicketStatus", SqlDbType.Int).Value = TITO_TICKET_STATUS.VALID;
            _cmd.Parameters.Add("@pStartDate", SqlDbType.DateTime).Value = WGDB.Now.AddMinutes(-CREATED_IN_LAST_MINUTES);
            _cmd.Parameters.Add("@pTerminalType", SqlDbType.Int).Value = TITO_TERMINAL_TYPE.TERMINAL;
            _cmd.Parameters.Add("@pCashable", SqlDbType.Int).Value = TITO_TICKET_TYPE.CASHABLE;
            _cmd.Parameters.Add("@pPromoRE", SqlDbType.Int).Value = TITO_TICKET_TYPE.PROMO_REDEEM;
            _cmd.Parameters.Add("@pPromoNR", SqlDbType.Int).Value = TITO_TICKET_TYPE.PROMO_NONREDEEM;


            _dt_tickets.Load(_db_trx.ExecuteReader(_cmd));

          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return _dt_tickets;
    } // GeTicketsFromCashierSessionId

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Init and Show the form.
    //
    //  PARAMS :
    //      - INPUT :
    //          - ParentForm
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public void Show(Form Parent)
    {
      try
      {
        uc_prov_terminals.InitControls("", " WHERE TE_TERMINAL_TYPE = " + (Int32)TerminalTypes.SAS_HOST, false);

        LoadTicketsList(GetFilteredTicketsList());

        m_cashier_status = CashierBusinessLogic.ReadCashierStatus(Cashier.SessionId);

        form_yes_no.Show(Parent);
        this.ShowDialog(form_yes_no);
        form_yes_no.Hide();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } // Show

    public Int32 ShowForTicketSelect(DataTable TicketsTable)
    {
      gb_search.Visible = false;

      m_ticket_selection_mode = true;
      m_selected_row_index = -1;

      this.Height = this.Height - gb_search.Height;
      gb_last_operations.Location = new Point(gb_last_operations.Location.X, gb_last_operations.Location.Y - gb_search.Height);
      btn_close.Location = new Point(btn_close.Location.X, btn_close.Location.Y - gb_search.Height);
      btn_print_selected.Location = new Point(btn_print_selected.Location.X, btn_print_selected.Location.Y - gb_search.Height);
      btn_print_selected.Text = Resource.String("STR_FRM_VOUCHERS_REPRINT_076");
      this.lbl_tickets_reprint_title.Text = Resource.String("STR_FRM_VOUCHERS_REPRINT_077");

      LoadTicketsList(TicketsTable);

      form_yes_no.Show(Parent);
      this.ShowDialog(form_yes_no);
      form_yes_no.Hide();

      return m_selected_row_index;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Dispose yes_no form.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    new public void Dispose()
    {
      base.Dispose();
    } // Dispose

    #endregion // Public Methods

    #region Events

    #region Buttons

    //------------------------------------------------------------------------------
    // PURPOSE : Handle the click on the Close/Cancel button.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private void btn_close_Click(object sender, EventArgs e)
    {
      // Close Button

      this.Close();

      return;

    } // btn_close_Click

    //------------------------------------------------------------------------------
    // PURPOSE: Set status of ALL buttons in control
    // 
    //  PARAMS:
    //      - INPUT:
    //        - Ticket: reference to a ticket instance
    //
    //      - OUTPUT:
    //
    private void SetButtonStatus(Boolean GridHasRows)
    {
      Boolean _allow_reprint;
      String _error_str;

      _allow_reprint = ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.TITO_ReprintTicket,
                                                           ProfilePermissions.TypeOperation.OnlyCheck,
                                                           this,
                                                           out _error_str);

      btn_print_selected.Enabled = _allow_reprint &&
                                   (TicketMgr.IsTITOCashierOpen || m_ticket_selection_mode) &&
                                   GridHasRows;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Handle the click on the Print Selected button.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private void btn_print_selected_Click(object sender, EventArgs e)
    {
      DataGridViewRow _view_row;
      DataRow _row;
      Int64 _ticket_id;
      String error_str;
      ENUM_TITO_OPERATIONS_RESULT _recreate_ticket_result;
      ParamsTicketOperation _operation_params;
      CardData _virtual_card;

      if (dgv_last_operations.SelectedRows.Count == 0)
      {
        return;
      }

      if (m_ticket_selection_mode)
      {
        m_selected_row_index = dgv_last_operations.SelectedRows[0].Index;
        btn_close_Click(null, null);

        return;
      }

      _recreate_ticket_result = ENUM_TITO_OPERATIONS_RESULT.OK;

      // Check if current user is an authorized user
      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.TITO_ReprintTicket,
                                               ProfilePermissions.TypeOperation.ShowMsg,
                                               this,
                                               out error_str))
      {
        return;
      }

      _view_row = dgv_last_operations.SelectedRows[0];
      _row = ((DataRowView)_view_row.DataBoundItem).Row;
      _ticket_id = (Int64)_row[GRID_COLUMN_TICKET_ID];

      _virtual_card = new CardData();

      if (!BusinessLogic.LoadVirtualAccount(_virtual_card))
      {
        _recreate_ticket_result = ENUM_TITO_OPERATIONS_RESULT.AUX_DATA_NOT_FOUND;
      }

      _operation_params = new ParamsTicketOperation();
      _operation_params.in_account = _virtual_card;
      _operation_params.in_window_parent = this;
      _operation_params.session_info = Cashier.CashierSessionInfo();

      if (_recreate_ticket_result == ENUM_TITO_OPERATIONS_RESULT.OK)
      {
        if (frm_message.Show(Resource.String("STR_TITO_TICKET_WARNING_REPRINT").Replace("\\r\\n", "\r\n"),
                             Resource.String("STR_APP_GEN_MSG_WARNING"),
                             MessageBoxButtons.YesNo,
                             MessageBoxIcon.Warning,
                             ParentForm) != DialogResult.OK)
        {
          _recreate_ticket_result = ENUM_TITO_OPERATIONS_RESULT.AUX_OPERATION_CANCELED;
        }
      }

      if (_recreate_ticket_result == ENUM_TITO_OPERATIONS_RESULT.OK)
      {
        _recreate_ticket_result = BusinessLogic.TicketReprint(_ticket_id, _operation_params, this.ParentForm);
      }

      if (_recreate_ticket_result != ENUM_TITO_OPERATIONS_RESULT.OK)
      {
        if (_recreate_ticket_result != ENUM_TITO_OPERATIONS_RESULT.AUX_OPERATION_CANCELED)
        {
          switch (_recreate_ticket_result)
          {
            case ENUM_TITO_OPERATIONS_RESULT.TICKET_NOT_VALID:
              frm_message.Show(Resource.String("STR_UC_TITO_TICKET_NO_REPRINT"),
                     Resource.String("STR_APP_GEN_MSG_ERROR"),
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error,
                     ParentForm);
              break;
            default:
              frm_message.Show(Resource.String("STR_TITO_TICKET_ERROR_REPRINT"),
                 Resource.String("STR_APP_GEN_MSG_ERROR"),
                 MessageBoxButtons.OK,
                 MessageBoxIcon.Error,
                 this.ParentForm);
              break;
          }
        }
        this.Focus();
      }

    } // btn_print_selected_Click

    #endregion // Buttons

    #region DataGridView

    #endregion // DataGridView
    //------------------------------------------------------------------------------
    // PURPOSE : Handle the reprint button click
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private void btn_ticket_search_Click(object sender, EventArgs e)
    {
      LoadTicketsList(GetFilteredTicketsList());
    }

    #endregion // Events

  } // frm_vouchers_reprint

} // WSI.Cashier
