//-------------------------------------------------------------------
// Copyright © 2014 Win Systems Ltd.
//-------------------------------------------------------------------
//
// MODULE NAME:   uc_scrollable_button_list.cs
// DESCRIPTION:   
// AUTHOR:        David Hernández
// CREATION DATE: 16-JUL-2014
//
// REVISION HISTORY:
//
// Date         Author Description
// -----------  ------ -----------------------------------------------
// 16-JUL-2014  DHA    Initial version.
// 25-JUL-2014  DHA    Fixed Bug #WIG-1119: set SetLastAction for player tracking gaming tables
// 30-SEP-2014  DHA    Fixed Bug #WIG-1350: corrected error on resize event
// 16-NOV-2016  LTC    Bug 18293: Tables: Cage movement to gaming tables with player tracking dosabled doesn't update closed table icon. 
// -------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using WSI.Common;

namespace WSI.Cashier
{
  public partial class uc_scrollable_button_list : UserControl
  {
    private Int32 m_scroll_value = 0;
    private Int32 m_control_position = 0;
    private Int32 m_visible_buttons;
    private Int32 m_min_button_witdh;
    private Int32 m_max_buttons;
    private Color m_button_color = Color.Transparent;
    private Color m_arrow_button_color = Color.Transparent;
    private Color m_seleted_button_color = Color.FromArgb(0x3F, 0x48, 0x53);  //Color.LightSkyBlue;
    private WSI.Cashier.Controls.uc_round_button.RoundButonStyle m_selected_button_style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
    private WSI.Cashier.Controls.uc_round_button.RoundButonStyle m_not_selected_button_style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
    private Dictionary<Int32, ScrollableButtonItem> m_buttons = new Dictionary<int, ScrollableButtonItem>();
    private Int32 m_selected_button_id = 0;
    private Int32 m_button_clicked_previous = 0; // LTC 16-NOV-2016

    public delegate void ButtonClickedEventHandler(Int32 Id);
    public event ButtonClickedEventHandler ButtonClicked;

    public Int32 MinButtonWidth
    {
      get
      {
        return m_min_button_witdh;
      }
      set
      {
        m_min_button_witdh = value;
      }
    }
    public Int32 MaxButtons
    {
      get
      {
        return m_max_buttons;
      }
      set
      {
        m_max_buttons = value;
      }
    }

    public Color ButtonColor
    {
      get
      {
        return m_button_color;
      }
      set
      {
        m_button_color = value;
      }
    }

    public Color ArrowButtonColor
    {
      get
      {
        return m_arrow_button_color;
      }
      set
      {
        m_arrow_button_color = value;
      }
    }

    public uc_scrollable_button_list()
    {
      InitializeComponent();

      this.btn_right.Click += new System.EventHandler(this.btn_right_Click);
      this.btn_left.Click += new System.EventHandler(this.btn_left_Click);
      this.SizeChanged += new EventHandler(uc_scrollable_button_list_SizeChanged);

      this.btn_right.Image = Resources.Resource.Input_right;
      this.btn_left.Image = Resources.Resource.Input_left;

      btn_left.Visible = false;
      btn_right.Visible = false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Add event handler to scan and configure buttons
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    private void InitControls()
    {
      EventLastAction.AddEventLastAction(this.pnl_buttons.Controls);

      foreach (Control _control in pnl_buttons.Controls)
      {
        if (_control is WSI.Cashier.Controls.uc_round_button)
        {
          _control.Click += new EventHandler(_button_Click);
        }
      }
    }

    public void LoadButtons(Dictionary<Int32, ScrollableButtonItem> Buttons)
    {
      Int32 _counter;
      Int32 _remainder = 0;
      Int32 _scale_image_button;
      Int32 _margin_top;
      Int32 _margin_bottom;
      Int32 _margin_left;
      Int32 _margin_right;

      _counter = 0;
      m_control_position = 0;
      _margin_top = 4;
      _margin_bottom = 4;
      _margin_left = 3;
      _margin_right = 3;

      m_selected_button_id = 0;

      if (m_min_button_witdh > 0)
      {
        m_visible_buttons = (Int32)(pnl_buttons.Width / m_min_button_witdh);
      }

      if (m_max_buttons < m_visible_buttons)
      {
        m_visible_buttons = m_max_buttons;
      }

      if (m_visible_buttons == 0)
      {
        m_visible_buttons = 1;
      }
      if (pnl_buttons.Controls.Count > 0)
      {
        pnl_buttons.Controls.Clear();
      }

      Math.DivRem(pnl_buttons.Width, m_visible_buttons, out _remainder);

      this.Width = this.Width - _remainder;

      m_scroll_value = (Int32)pnl_buttons.Width / m_visible_buttons;

      //pnl_buttons.Controls.Clear();

      // Load gambling tables
      foreach (KeyValuePair<Int32, ScrollableButtonItem> _item in Buttons)
      {
        WSI.Cashier.Controls.uc_round_button _button = new WSI.Cashier.Controls.uc_round_button();
        _scale_image_button = pnl_buttons.Height - _margin_top - _margin_bottom;

        if (_item.Value.ButtonImage != null)
        {
          _button.Image = new Bitmap(_item.Value.ButtonImage, new Size(_scale_image_button, _scale_image_button));
        }
        _button.ImageAlign = ContentAlignment.TopLeft;
        _button.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;

        _button.Text = _item.Value.ButtonLabel;
        _button.TextAlign = ContentAlignment.MiddleLeft;
        _button.Tag = _item.Value.ButtonId;
        _button.Height = pnl_buttons.Height;
        _button.Width = m_scroll_value;
        _button.Margin = new Padding(_margin_left, _margin_top, _margin_right, _margin_bottom);
        _button.Location = new Point((int)(m_scroll_value * _counter), 0);
        _button.TabStop = false;
        _button.BackColor = _item.Value.ButtonColor == Color.Transparent ? m_button_color : _item.Value.ButtonColor;
        _button.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;


        pnl_buttons.Controls.Add(_button);

        _counter++;
      }

      if (Buttons.Count >= m_visible_buttons)
      {
        btn_left.BackColor = m_arrow_button_color;
        btn_right.BackColor = m_arrow_button_color;

        DisableButtonByPosition();
      }

      m_buttons = Buttons;

      InitControls();
    }

    void _button_Click(object sender, EventArgs e)
    {
      Int32 _id;

      _id = 0;

      if (Int32.TryParse(((WSI.Cashier.Controls.uc_round_button)sender).Tag.ToString(), out _id))
      {
        // LTC 16-NOV-2016
        if (_id == m_button_clicked_previous)
          return;

        m_selected_button_id = _id;
        RefreshButtons(m_buttons);
        OnButtonClicked(_id);
        SetSelectedButton(m_selected_button_id);
        m_button_clicked_previous = _id;

      }
    }

    private void OnButtonClicked(Int32 Id)
    {
      if (ButtonClicked != null)
      {
        ButtonClicked(Id);
      }
    }

    private void MoveControls(Int32 Offset)
    {
      Control[] ControlsBuffer = new Control[pnl_buttons.Controls.Count];
      pnl_buttons.Controls.CopyTo(ControlsBuffer, 0);

      pnl_buttons.Controls.Clear();
      pnl_buttons.SuspendLayout();

      for (int _i = 0; _i < ControlsBuffer.Length; _i++)
      {
        if (ControlsBuffer[_i] is WSI.Cashier.Controls.uc_round_button)
        {
          ControlsBuffer[_i].Left += Offset;
        }
      }

      if (Offset <= 0)
      {
        m_control_position++;
      }
      else
      {
        m_control_position--;
      }

      pnl_buttons.Controls.AddRange(ControlsBuffer);
      DisableButtonByPosition();
      pnl_buttons.ResumeLayout();
    }

    private void DisableButtonByPosition()
    {
      btn_left.Visible = true;
      btn_right.Visible = true;

      if (m_control_position == 0)
      {
        btn_left.Visible = false;
        btn_left.BackColor = Color.Transparent;
      }
      else
      {
        btn_left.Visible = true;
        btn_right.BackColor = Color.Transparent;
      }

      if (m_control_position + m_visible_buttons >= pnl_buttons.Controls.Count)
      {
        btn_right.Visible = false;
        btn_right.BackColor = Color.Transparent;
      }
      else
      {
        btn_right.Visible = true;
        btn_right.BackColor = Color.Transparent;
      }
    }

    public void SetSelectedButton(Int32 Id)
    {
      Int32 _position;
      _position = 0;

      m_selected_button_id = Id;

      foreach (Control _button in pnl_buttons.Controls)
      {
        ScrollableButtonItem _button_properties;

        if (_button is WSI.Cashier.Controls.uc_round_button)
        {
          _button_properties = GetButtonProperties((Int32)_button.Tag);

          if (_button_properties != null)
          {
            ((WSI.Cashier.Controls.uc_round_button)_button).Style = m_not_selected_button_style;
          }
          else
          {
            ((WSI.Cashier.Controls.uc_round_button)_button).Style = m_selected_button_style; 
          }

          if ((Int32)_button.Tag == Id)
          {
            if (0 >= _button.Left || _button.Left >= pnl_buttons.Width)
            {
              if (pnl_buttons.Controls.Count - m_visible_buttons <= _position)
              {
                _position = pnl_buttons.Controls.Count - m_visible_buttons;
              }

              if (m_control_position - _position != 0)
              {
                MoveControls((m_scroll_value * (m_control_position - _position)));
              }
              m_control_position = _position;
            }

            _button.Focus();

            ((WSI.Cashier.Controls.uc_round_button)_button).Style = m_selected_button_style; //ControlPaint.Light(((WSI.Cashier.Controls.uc_round_button)_button).BackColor, 1); // m_seleted_button_color;

            DisableButtonByPosition();
            //break;
          }
        }
        _position++;
      }
    }

    public void RefreshButtons(Dictionary<Int32, ScrollableButtonItem> Buttons)
    {
      Boolean _reload_buttons;
      ScrollableButtonItem _button;
      Int32 _scale_image_button;

      _reload_buttons = false;

      ////// LTC 16-NOV-2016
      ////if (!WSI.Common.GamingTableBusinessLogic.IsEnabledGTPlayerTracking() && !WSI.Common.Cage.IsCageAutoMode())
      ////{
      ////  Buttons = CheckTableStatusOpenedChangeFromGUI(Buttons);
      ////}

      if (m_buttons.Count != Buttons.Count)
      {
        _reload_buttons = true;
      }
      else
      {
        m_buttons = Buttons;
        foreach (Control _control in pnl_buttons.Controls)
        {
          if (_control is WSI.Cashier.Controls.uc_round_button)
          {
            if (!m_buttons.TryGetValue((Int32)_control.Tag, out _button))
            {
              _reload_buttons = true;
              break;
            }
            _control.Text = m_buttons[(Int32)_control.Tag].ButtonLabel;
            _control.BackColor = m_buttons[(Int32)_control.Tag].ButtonColor == Color.Transparent ? m_button_color : m_buttons[(Int32)_control.Tag].ButtonColor;

            if (m_buttons[(Int32)_control.Tag].ButtonImage != null)
            {
              _scale_image_button = _control.Height - _control.Margin.Top - _control.Margin.Bottom;
              ((WSI.Cashier.Controls.uc_round_button)_control).Image = new Bitmap(m_buttons[(Int32)_control.Tag].ButtonImage, new Size(_scale_image_button, _scale_image_button)); ;
            }
            else
            {
              ((WSI.Cashier.Controls.uc_round_button)_control).Image = null;
            }

            if ((Int32)_control.Tag == m_selected_button_id)
            {
              ((WSI.Cashier.Controls.uc_round_button)_control).Style = m_selected_button_style; // ControlPaint.Light(_control.BackColor, 1);
            }
          }
        }
      }
      if (_reload_buttons)
      {
        this.MaxButtons = Buttons.Count;
        LoadButtons(Buttons);
      }
    }

    public void RefreshButton(ScrollableButtonItem Button)
    {
      Int32 _scale_image_button;

      m_buttons[Button.ButtonId] = Button;

      foreach (Control _control in pnl_buttons.Controls)
      {
        if (_control is WSI.Cashier.Controls.uc_round_button && Button.ButtonId == (Int32)_control.Tag)
        {
          _control.Text = Button.ButtonLabel;
          ((WSI.Cashier.Controls.uc_round_button)_control).Style = m_not_selected_button_style;

          if (Button.ButtonImage != null)
          {
            _scale_image_button = _control.Height - _control.Margin.Top - _control.Margin.Bottom;
            ((WSI.Cashier.Controls.uc_round_button)_control).Image = new Bitmap(Button.ButtonImage, new Size(_scale_image_button, _scale_image_button));
          }
          else
          {
            ((WSI.Cashier.Controls.uc_round_button)_control).Image = null;
          }
        }
        if ((Int32)_control.Tag == m_selected_button_id)
        {
          ((WSI.Cashier.Controls.uc_round_button)_control).Style = m_selected_button_style; // ControlPaint.Light(_control.BackColor, 1);
        }
      }
    }

    private ScrollableButtonItem GetButtonProperties(Int32 ButtonId)
    {
      foreach (KeyValuePair<Int32, ScrollableButtonItem> _button in m_buttons)
      {
        if (_button.Key == ButtonId)
        {
          return _button.Value;
        }
      }

      return null;
    }

    private void btn_left_Click(object sender, EventArgs e)
    {
      MoveControls(m_scroll_value);
    }

    private void btn_right_Click(object sender, EventArgs e)
    {
      MoveControls(-m_scroll_value);
    }

    private void uc_scrollable_button_list_SizeChanged(object sender, EventArgs e)
    {
      this.SizeChanged -= new EventHandler(uc_scrollable_button_list_SizeChanged);

      Int32 _counter;
      Int32 _remainder = 0;

      _counter = 0;
      m_control_position = 0;

      m_selected_button_id = 0;

      if (m_min_button_witdh > 0)
      {
        m_visible_buttons = (Int32)(pnl_buttons.Width / m_min_button_witdh);
      }

      if (m_max_buttons < m_visible_buttons)
      {
        m_visible_buttons = m_max_buttons;
      }

      if (m_visible_buttons == 0)
      {
        m_visible_buttons = 1;
      }

      Math.DivRem(pnl_buttons.Width, m_visible_buttons, out _remainder);

      this.Width = this.Width - _remainder;

      m_scroll_value = (Int32)pnl_buttons.Width / m_visible_buttons;

      // Load gambling tables
      foreach (Control _item in pnl_buttons.Controls)
      {
        if (_item is WSI.Cashier.Controls.uc_round_button)
        {
          _item.Width = m_scroll_value;
          _item.Location = new Point((int)(m_scroll_value * _counter), 0);
          _counter++;
        }
      }

      if (m_buttons.Count >= m_visible_buttons)
      {
        btn_left.BackColor = m_arrow_button_color;
        btn_right.BackColor = m_arrow_button_color;

        DisableButtonByPosition();
      }
      this.SizeChanged += new EventHandler(uc_scrollable_button_list_SizeChanged);
    }

    //// LTC 16-NOV-2016
    ////------------------------------------------------------------------------------
    //// PURPOSE : Update Image if table status was changed to Open from GUI by cage movements
    ////
    ////  PARAMS :
    ////      - INPUT : buttons- List of buttons
    ////
    ////      - OUTPUT :
    ////
    //// RETURNS : buttons- List of buttons with image modified
    ////
    ////   NOTES :
    //private Dictionary<Int32, ScrollableButtonItem> CheckTableStatusOpenedChangeFromGUI(Dictionary<Int32, ScrollableButtonItem> buttons)
    //{
    //  DataTable _dt;
    //  List<int> _lstButtons;

    //  _lstButtons = new List<int>();

    //  foreach (var _button in buttons)
    //  {
    //    _lstButtons.Add(_button.Key);
    //  }

    //  GamingTableBusinessLogic.GetGamingTablesOpenedFromGUI(_lstButtons, out _dt);

    //  foreach (DataRow _dr in _dt.Rows)
    //  {
    //    int _tableId = 0;
    //    _tableId = (int)_dr[0];

    //    if (buttons[_tableId].ButtonImage != null)
    //    {
    //      buttons[_tableId].ButtonImage = null;
    //    }
    //  }

    //  return buttons;
    //}
  }

  public class ScrollableButtonItem
  {
    #region members
    private Int32 m_button_id;
    private String m_button_label;
    private Color m_button_color;
    private Image m_button_image;

    #endregion members

    #region Properties

    public Int32 ButtonId
    {
      get { return m_button_id; }
      set { m_button_id = value; }
    }

    public String ButtonLabel
    {
      get { return m_button_label; }
      set { m_button_label = value; }
    }

    public Color ButtonColor
    {
      get { return m_button_color; }
      set { m_button_color = value; }
    }

    public Image ButtonImage
    {
      get { return m_button_image; }
      set { m_button_image = value; }
    }

    public ScrollableButtonItem()
    {
    }

    public ScrollableButtonItem(Int32 ButtonId, String ButtonLabel, Color ButtonColor, Image ButtonImage)
    {
      this.ButtonId = ButtonId;
      this.ButtonLabel = ButtonLabel;
      this.ButtonColor = ButtonColor;
      this.ButtonImage = ButtonImage;
    }

    #endregion Properties
  }
}
