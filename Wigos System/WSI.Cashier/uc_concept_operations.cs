//------------------------------------------------------------------------------
// Copyright � 2007-2009 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_container.cs
// 
//   DESCRIPTION: Implements the uc_concept_operations user control
//                Class: uc_concept_operations (Concept Operations)
//
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 09-SEP-2014 SMN     First release.
// 29-SEP-2014 RRR     Check if there's enougth money in cashier before undo concept operation
// 02-OCT-2014 SMN     Get Resource that is indicated in cstc_cashier_btn_group column
// 22-OCT-2014 DLL     Fixed Bug WIG-1549: Concepts operations not generate activity
// 24-NOV-2014 LRS     Fixed Bug WIG-1735: Enable / Disable buttons
// 11-MAR-2015 YNM     Fixed Bug WIG-2136: Cage Concepts: Update Concepts changes on Cashier login.
// 04-NOV-2015 SGB     Backlog Item WIG-5785: Change designer
// 16-NOV-2015 FAV     Fixed Bug 6602: Show a scrollbar when there are more of 10 buttons in a group box
// 23-NOV-2015 FOS     Bug 6771:Payment tickets & handpays
// 23-NOV-2015 FOS     Bug 10979: Cancel last operation (Undo an ouput concept)
// 27-APR-2016 DHA     Product Backlog Item 10825: added chips types to cage concepts
// 19-AUG-2016 RGR     Bug 15421: Cashier: Button Cancel last operation of Concepts cage enabled on closing
// 08-SEP-2017 EOR     Bug 29688: WIGOS-4985 Accounting - Generic report Monthly report: Undo unclaimed redeemed chips operation doesn't work
// 20-OCT-2017 RAB     Bug 30338:WIGOS-5198 [Ticket #8867] Reporte de Tickets de caja
// -----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Drawing.Printing;
using WSI.Common;
using WSI.Cashier.TITO;
using WSI.Common.TITO;
using System.Collections;

namespace WSI.Cashier
{
  public partial class uc_concept_operations : UserControl
  {
    public class ButtonConcept : WSI.Cashier.Controls.uc_round_button
    {
      public Int64 ConceptId;



      protected override void OnPaint(PaintEventArgs pe)
      {

        this.Enabled = Cashier.SessionId > 0; // enable / disable depending on the status of cashier(open / closed)
        base.OnPaint(pe);

      }

    } // class

    #region Atributes

    private Dictionary<Int64, CashierConceptOperationResult> m_last_cashier_concept_operation; // it is used to undo operations
    private DateTime m_last_cashier_concept_operation_date;
    private Dictionary<Int64, CageConceptInformation> m_cashier_concepts;
    private static frm_yesno form_yes_no;
    Button m_btn_undo;

    #endregion // Atributes

    #region Constants

    private const Int32 DEFAULT_BUTTON_WIDTH = 205; //PX

    #endregion // Constants

    #region Public methods

    public uc_concept_operations()
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[UC LOAD] uc_concept_operation", Log.Type.Message);

      InitializeComponent();

      form_yes_no = new frm_yesno();
      form_yes_no.Opacity = 0.6;

      Init();

      EventLastAction.AddEventLastAction(this.Controls);
    }

    public void SetButtonEnableUndo()
    {
      if (m_btn_undo == null)
      {
        return;
      }

      if (m_btn_undo.Enabled)
      {
        if (!Cashier.IsClosed())
        {
          m_btn_undo.Enabled = false;
        }
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Initialize controls.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //  
    public void Init()
    {
      DataTable _dt_cashier_concepts;

      _dt_cashier_concepts = new DataTable();
      m_cashier_concepts = new Dictionary<Int64, CageConceptInformation>();
      m_last_cashier_concept_operation = new Dictionary<Int64, CashierConceptOperationResult>();
      m_last_cashier_concept_operation_date = new DateTime();

      uc_title.Text = Resource.String("STR_CASH_CAGE_CONCEPTS");
      
      this.BackColor = WSI.Cashier.CashierStyle.Colors.m_colors_dic[CashierStyle.Colors.ENUM_COLORS.HEX_E3E7E9];

      if (!Cage.IsCageEnabled())
      {
        return;
      }

      // Get concepts
      if (!GetCashierConcepts(out _dt_cashier_concepts))
      {
        frm_message.Show(Resource.String("STR_AC_IMPORT_RETRY_END"), //Se ha producido un error, el proceso no puede continuar
          Resource.String("STR_APP_GEN_MSG_WARNING"),
          MessageBoxButtons.OK,
          MessageBoxIcon.Warning,
          ParentForm);

        return;
      }

      if (!CreateConceptButtons(_dt_cashier_concepts))
      {
        frm_message.Show(Resource.String("STR_AC_IMPORT_RETRY_END"), //Se ha producido un error, el proceso no puede continuar
          Resource.String("STR_APP_GEN_MSG_WARNING"),
          MessageBoxButtons.OK,
          MessageBoxIcon.Warning,
          ParentForm);

        return;
      }

    } // Init

    #endregion // Public methods

    #region Private methods

    private void RestoreParentFocus()
    {
      this.ParentForm.Focus();
      this.ParentForm.BringToFront();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Create dinamic buttons
    //
    //  PARAMS :
    //      - INPUT :
    //            - DataTable
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //          - True:               Executed succesfully
    //          - False:              Executed unsuccesfully
    //   NOTES :
    // 
    private Boolean CreateConceptButtons(DataTable DtCashierConcepts)
    {
      Boolean _output_concepts_only_national_currency;
      CageConceptInformation _cage_concept;
      String _cashier_btn_group;
      String _cashier_btn_group_old;
      String _cashier_btn_group_resource;
      Int32 _position_x;
      Int32 _position_y;
      Int32 _margin_top = 5;
      Int32 _margin_left = 5;
      Int32 _idx_row;
      Int32 _max_buttons_per_row;
      Int32 _width_btn;
      Int32 _heigth_btn;
      Int32 _container;
      WSI.Cashier.Controls.uc_round_panel _gb;
      Panel _pnl_separator;
      Panel _panel_scroll;
      Int32 _margin_left_panel_scroll = 15;

      pnl_container_0.Controls.Clear();
      pnl_container_1.Controls.Clear();
      pnl_container_2.Controls.Clear();
      pnl_container_3.Controls.Clear();
      pnl_container_4.Controls.Clear();

      _cashier_btn_group_old = String.Empty;
      _output_concepts_only_national_currency = GeneralParam.GetInt32("Cage", "Meters.OutputConcepts.OnlyNationalCurrency", 1) == 1;
      _max_buttons_per_row = GeneralParam.GetInt32("Cage", "Meters.Cashier.MaxItemsPerRow", 4, 1, 6);

      _heigth_btn = 50;
      _width_btn = ((pnl_container_0.Size.Width - (((2+_max_buttons_per_row)*_margin_left)+(2*_margin_left_panel_scroll))) / _max_buttons_per_row);

      if (_width_btn < 0)
      {
        // to much buttons per row
        _max_buttons_per_row = 4;
        _width_btn = ((pnl_container_0.Size.Width - (_max_buttons_per_row * _margin_left)) / _max_buttons_per_row);
      }
      
      _position_x = _margin_left;
      _position_y = _margin_top * 8;

      _idx_row = 0;

      _gb = new WSI.Cashier.Controls.uc_round_panel();
      _panel_scroll = new Panel();

      // Foreach concept, create one button
      foreach (DataRow _dr in DtCashierConcepts.Rows)
      {
        Boolean _string_found;
        _cashier_btn_group = (String)_dr["CSTC_CASHIER_BTN_GROUP"];

        _cashier_btn_group_resource = Resource.String(_cashier_btn_group, out _string_found);
        if (!_string_found)
        {
          _cashier_btn_group_resource = _cashier_btn_group;
        }
        _container = (Int32)_dr["CSTC_CASHIER_CONTAINER"];

        _cage_concept = new CageConceptInformation();
        _cage_concept.ConceptId = (Int64)_dr["CC_CONCEPT_ID"];
        _cage_concept.Name = (String)_dr["CC_NAME"];
        _cage_concept.Description = (String)_dr["CC_DESCRIPTION"];
        _cage_concept.UnitPrice = (Decimal)_dr["CC_UNIT_PRICE"];
        _cage_concept.CashierAction = (CageMeters.CageSourceTargetCashierAction)_dr["CSTC_CASHIER_ACTION"];
        _cage_concept.OnlyNationalCurrency = (Boolean)_dr["CSTC_ONLY_NATIONAL_CURRENCY"];
        _cage_concept.PriceFactor = (Decimal)_dr["CSTC_PRICE_FACTOR"];
        _cage_concept.SequenceId = (SequenceId)_dr["CC_SEQUENCE_ID"];
        _cage_concept.OperationCode = (OperationCode)_dr["CC_OPERATION_CODE"];
        if (_cage_concept.OperationCode == 0)
        {
          _cage_concept.OperationCode = OperationCode.CAGE_CONCEPTS + (Int32)_cage_concept.ConceptId;
        }
        _cage_concept.VoucherType = (CashierVoucherType)_dr["CC_VOUCHER_TYPE"];
        if (_cage_concept.VoucherType == 0)
        {
          _cage_concept.VoucherType = CashierVoucherType.CageConcepts + (Int32)_cage_concept.ConceptId;
        }

        if (_cage_concept.CashierAction == CageMeters.CageSourceTargetCashierAction.CashierOutput && _output_concepts_only_national_currency)
        {
          _cage_concept.OnlyNationalCurrency = true;
        }

        if (!ValidateConceptToCreateButton(_cage_concept))
        {
          continue;
        }

        if (!m_cashier_concepts.ContainsKey(_cage_concept.ConceptId))
        {
          m_cashier_concepts.Add(_cage_concept.ConceptId, _cage_concept);
        }

        if (_cashier_btn_group != _cashier_btn_group_old)
        {
          _pnl_separator = new Panel();
          _pnl_separator.Dock = DockStyle.Top;
          _pnl_separator.AutoSize = false;
          _pnl_separator.Size = new Size(1, _margin_top);

          // Print new group
          _gb = new WSI.Cashier.Controls.uc_round_panel();
          _gb.HeaderText = _cashier_btn_group_resource.ToUpper();
          _gb.Dock = DockStyle.Top;
          _gb.AutoSize = true;
          _gb.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
          _gb.HeaderHeight = 35;
          _gb.CornerRadius = 10;
          _gb.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
          _gb.AutoSizeMode = AutoSizeMode.GrowOnly;

          // Panel with scrollbars
          _panel_scroll = new Panel();
          _panel_scroll.AutoScroll = true;
          _panel_scroll.Top = 40;
          _panel_scroll.Left = _margin_left_panel_scroll;

          _gb.Controls.Add(_panel_scroll);

          _position_x = _margin_left;
          _position_y = 0;

          _idx_row = 0;

          this.pnl_container_0.Controls.Add(_pnl_separator);

          switch (_container)
          {
            case 0:
              this.pnl_container_0.Controls.Add(_gb);
              break;
            case 1:
              this.pnl_container_1.Controls.Add(_gb);
              break;
            case 2:
              this.pnl_container_2.Controls.Add(_gb);
              break;
            case 3:
              this.pnl_container_3.Controls.Add(_gb);
              break;
            default:
              this.pnl_container_0.Controls.Add(_gb);
              break;
          }
        }
        ButtonConcept _btn_concept = GetButtonConcept(_cage_concept.ConceptId, _cage_concept.Name, _position_x, _position_y, _width_btn, _heigth_btn);
        _panel_scroll.Controls.Add(_btn_concept);  

        // Scroll panel has a max size that depend of the group box (Width and Height)
        _panel_scroll.Width = _gb.Width - (_margin_left_panel_scroll);
        _panel_scroll.Height = (Int32)(Math.Ceiling((Decimal)_panel_scroll.Controls.Count / (Decimal)_max_buttons_per_row)) * (_heigth_btn + _margin_top);
        if ((_panel_scroll.Controls.Count / _max_buttons_per_row) > 9)
        {
          _panel_scroll.Height = pnl_container_right.Height - 40;
        }

        _idx_row = (++_idx_row) % _max_buttons_per_row;

        _position_y += (_idx_row % _max_buttons_per_row == 0) ? _heigth_btn + _margin_top : 0;
        _position_x += (_idx_row % _max_buttons_per_row == 0) ? -((_max_buttons_per_row - 1) * (_width_btn + _margin_left)) : _width_btn + _margin_left;

        _cashier_btn_group_old = _cashier_btn_group;
      } //foreach

      m_btn_undo = GetButtonUndo("_btn_undo", _position_x, _position_y, DEFAULT_BUTTON_WIDTH, _heigth_btn);
      EnableButton(false, m_btn_undo);

      this.pnl_container_4.Controls.Add(m_btn_undo);

      return true;
    } // CreateConceptButtons

    private ButtonConcept GetButtonConcept(Int64 ConceptId, String TextButton, Int32 PositionX, Int32 PositionY, Int32 Width, Int32 Heigth)
    {
      ButtonConcept _btn_concept;

      _btn_concept = new ButtonConcept();
      _btn_concept.ConceptId = ConceptId;
      _btn_concept.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
      _btn_concept.Name = "btn_cashier_concept_" + ConceptId.ToString();
      _btn_concept.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      _btn_concept.Size = new System.Drawing.Size(Width, Heigth);
      _btn_concept.Text = TextButton;
      _btn_concept.UseVisualStyleBackColor = false;
      _btn_concept.Visible = true;
      _btn_concept.Click += new System.EventHandler(this.BtnCashierConcept_Click);
      _btn_concept.Enabled = false;
      _btn_concept.Location = new System.Drawing.Point(PositionX, PositionY);

      return _btn_concept;
    } //GetButtonConcept

    private WSI.Cashier.Controls.uc_round_button GetButtonUndo(String TextButton, Int32 PositionX, Int32 PositionY, Int32 Width, Int32 Heigth)
    {
      WSI.Cashier.Controls.uc_round_button _btn_undo;

      _btn_undo = new WSI.Cashier.Controls.uc_round_button();
      _btn_undo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
      _btn_undo.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      _btn_undo.Name = TextButton;
      _btn_undo.Text = Resource.String("STR_UNDO_LAST_OPERATION_GENERIC");
      _btn_undo.UseVisualStyleBackColor = false;
      _btn_undo.Visible = true;
      _btn_undo.Click += new System.EventHandler(this.BtnUndo_Click);
      _btn_undo.Dock = DockStyle.Right;
      _btn_undo.Size = new System.Drawing.Size(Width, Heigth);

      _btn_undo.Location = new System.Drawing.Point(PositionX, PositionY);

      return _btn_undo;

    } // GetButtonUndo

    //------------------------------------------------------------------------------
    // PURPOSE : Validate a cage concept to create a button
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //            - DataTable
    //
    // RETURNS :
    //          - True:               Cage concept is ok to create a button
    //          - False:              Cage concept is not ok to create a button
    //   NOTES :
    // 
    private Boolean ValidateConceptToCreateButton(CageConceptInformation CageConcept)
    {
      if (String.IsNullOrEmpty(CageConcept.Name))
      {
        return false;
      }

      return true;
    } // ValidateCashierConcept

    //------------------------------------------------------------------------------
    // PURPOSE : Get concepts from database
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //            - DataTable
    //
    // RETURNS :
    //          - True:               Executed succesfully
    //          - False:              Executed unsuccesfully
    //   NOTES :
    // 
    private Boolean GetCashierConcepts(out DataTable DtCashierConcepts)
    {
      StringBuilder _sql_sb;

      DtCashierConcepts = new DataTable();
      try
      {
        _sql_sb = new StringBuilder();
        _sql_sb.AppendLine("     SELECT  CC_CONCEPT_ID ");
        _sql_sb.AppendLine("           , ISNULL(CC_NAME,'.') CC_NAME ");
        _sql_sb.AppendLine("           , CC_DESCRIPTION ");
        _sql_sb.AppendLine("           , ISNULL(CC_UNIT_PRICE, 0) CC_UNIT_PRICE ");
        _sql_sb.AppendLine("           , CSTC_PRICE_FACTOR ");
        _sql_sb.AppendLine("           , ISNULL(CSTC_CASHIER_ACTION, -1) CSTC_CASHIER_ACTION ");
        _sql_sb.AppendLine("           , CSTC_ONLY_NATIONAL_CURRENCY ");
        _sql_sb.AppendLine("           , ISNULL(CSTC_CASHIER_BTN_GROUP,'_') CSTC_CASHIER_BTN_GROUP ");
        _sql_sb.AppendLine("           , ISNULL(CSTC_CASHIER_CONTAINER,0) CSTC_CASHIER_CONTAINER ");
        _sql_sb.AppendLine("           , ISNULL(CC_SEQUENCE_ID,0) CC_SEQUENCE_ID ");
        _sql_sb.AppendLine("           , ISNULL(CC_OPERATION_CODE,0) CC_OPERATION_CODE ");
        _sql_sb.AppendLine("           , ISNULL(CC_VOUCHER_TYPE,0) CC_VOUCHER_TYPE ");
        _sql_sb.AppendLine("       FROM  CAGE_CONCEPTS ");
        _sql_sb.AppendLine(" INNER JOIN  CAGE_SOURCE_TARGET_CONCEPTS ON CSTC_CONCEPT_ID = CC_CONCEPT_ID ");
        _sql_sb.AppendLine("      WHERE  CSTC_SOURCE_TARGET_ID = @pSourceTargetId ");
        _sql_sb.AppendLine("        AND  ISNULL(CC_ENABLED,0) = 1 ");
        _sql_sb.AppendLine("        AND  ISNULL(CSTC_ENABLED,0) = 1 ");
        _sql_sb.AppendLine("        AND  CC_CONCEPT_ID <> 0 ");
        _sql_sb.AppendLine("        AND  ISNULL(CSTC_CASHIER_ACTION, @pCashierActionNone) <> @pCashierActionNone ");
        _sql_sb.AppendLine("   ORDER BY  CSTC_CASHIER_BTN_GROUP DESC, ISNULL(CC_NAME,'.') ASC ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sql_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pSourceTargetId", SqlDbType.BigInt).Value = CageMeters.CageSystemSourceTarget.Cashiers;
            _sql_cmd.Parameters.Add("@pCashierActionNone", SqlDbType.Int).Value = CageMeters.CageSourceTargetCashierAction.None;

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              _sql_da.Fill(DtCashierConcepts);
            }
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // GetCashierConcepts


    //------------------------------------------------------------------------------
    // PURPOSE : Undo last concept button's operation. Only undo one last.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    // 
    private void BtnUndo_Click(object sender, EventArgs e)
    {
      VoucherSpecials _voucher_file;
      Int32 _undo_allowed_minutes;
      CashierConceptOperationResult _res;

      _undo_allowed_minutes = GeneralParam.GetInt32("Cashier.Undo", "Allowed.Minutes", 60);

      try
      {
        if (m_last_cashier_concept_operation.Count == 0
          || DateTime.Compare(m_last_cashier_concept_operation_date.Add(new TimeSpan(0, _undo_allowed_minutes, 0)), WGDB.Now) < 0)
        {
          EnableButton(false, m_btn_undo);

          frm_message.Show(Resource.String("STR_UNDO_OPERATION_NO_REVERSIBLE"), // "No hay operaciones anulables."
            Resource.String("STR_APP_GEN_MSG_WARNING"),
            MessageBoxButtons.OK,
            MessageBoxIcon.Warning,
            ParentForm);

          return;
        }

        DialogResult _dlg_rc;
        _dlg_rc = frm_message.Show(Resource.String("STR_UNDO_OTHERS_OPERATIONS").Replace("\\r\\n", "\r\n"),
                     Resource.String("STR_UNDO_LAST_OPERATION_GENERIC"),
                     MessageBoxButtons.YesNo, Images.CashierImage.Question, Cashier.MainForm);

        if (_dlg_rc != DialogResult.OK)
        {
          return;
        }

        _res = new CashierConceptOperationResult();

        // 29-SEP-2014 RRR  Check if there's enougth money in cashier
        foreach (Int64 _curr in m_last_cashier_concept_operation.Keys)
        {
          _res.Amount = m_last_cashier_concept_operation[_curr].Amount;
          _res.ConceptId = m_last_cashier_concept_operation[_curr].ConceptId;
          _res.CurrencyType = m_last_cashier_concept_operation[_curr].CurrencyType;
          _res.IsoCode = m_last_cashier_concept_operation[_curr].IsoCode;
          _res.Quantity = m_last_cashier_concept_operation[_curr].Quantity;

          if (!ValidateCashierConceptOperationResult(_res, true))
          {
            return;
          }
        }


        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (!UndoLastOperation(out _voucher_file, _db_trx.SqlTransaction))
          {
            //Show error message
            frm_message.Show(Resource.String("STR_AC_IMPORT_RETRY_END"), //Se ha producido un error, el proceso no puede continuar
              Resource.String("STR_APP_GEN_MSG_WARNING"),
              MessageBoxButtons.OK,
              MessageBoxIcon.Warning,
              ParentForm);

            return;
          }
          _db_trx.Commit();

          m_last_cashier_concept_operation.Clear();

          EnableButton(false, m_btn_undo);

          if (_voucher_file != null)
          {
            WSI.Cashier.VoucherPrint.Print(_voucher_file);
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Undo Last operation
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    // 
    private Boolean UndoLastOperation(out VoucherSpecials VoucherFile, SqlTransaction Trx)
    {
      VoucherFile = null;

      try
      {
        foreach (KeyValuePair<Int64, CashierConceptOperationResult> _concept in m_last_cashier_concept_operation)
        {
          // Create negative movements
          _concept.Value.Amount *= -1;
          if (!CreateCashierMovements(_concept.Value, out VoucherFile, Trx))
          {
            return false;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // UndoLastOperation

    //------------------------------------------------------------------------------
    // PURPOSE : Concept Buttons action
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //            - DataTable
    //
    // RETURNS :
    //          - True:               Executed succesfully
    //          - False:              Executed unsuccesfully
    //   NOTES :
    // 
    private void BtnCashierConcept_Click(object sender, EventArgs e)
    {
      CashierConceptOperationResult _cashier_concept_operation_result;
      frm_concept_input _frm_concept_input;
      DialogResult _dialog_result;
      Int64 _concept_id;
      VoucherSpecials _voucher_file;
      Boolean _movements_created;

      _concept_id = ((ButtonConcept)sender).ConceptId;

      // Show formulary
      try
      {
        form_yes_no.Show(this.ParentForm);

        _frm_concept_input = new frm_concept_input();
        _dialog_result = _frm_concept_input.Show(_concept_id
                                   , m_cashier_concepts[_concept_id].Name
                                   , m_cashier_concepts[_concept_id].UnitPrice * m_cashier_concepts[_concept_id].PriceFactor
                                   , m_cashier_concepts[_concept_id].OnlyNationalCurrency
                                   , out _cashier_concept_operation_result);
        if (_dialog_result != DialogResult.OK)
        {
          return;
        }
      }
      finally
      {
        form_yes_no.Hide();
        this.RestoreParentFocus();
      }

      if (_cashier_concept_operation_result.Amount > 0)
      {
        if (!ValidateCashierConceptOperationResult(_cashier_concept_operation_result, false))
        {
          return;
        }

        using (DB_TRX _db_trx = new DB_TRX())
        {
          _movements_created = CreateCashierMovements(_cashier_concept_operation_result, out _voucher_file, _db_trx.SqlTransaction);

          _db_trx.Commit();
        }

        if (_movements_created)
        {
          m_last_cashier_concept_operation.Clear();
          m_last_cashier_concept_operation.Add(_cashier_concept_operation_result.ConceptId, _cashier_concept_operation_result);
          m_last_cashier_concept_operation_date = WGDB.Now;
          EnableButton(true, m_btn_undo);

          if (_voucher_file != null)
          {
            WSI.Cashier.VoucherPrint.Print(_voucher_file);
          }
        }
        else
        {
          Log.Error(String.Format("Error: BtnCashierConcept_Click - CreateCashierMovements. CashierSessionId={0} ConceptId={1} IsoCode={2} Amount={3} ", Cashier.SessionId, _concept_id, _cashier_concept_operation_result.IsoCode, _cashier_concept_operation_result.Amount));
          frm_message.Show(Resource.String("STR_AC_IMPORT_RETRY_END"), // "Se ha producido un error. El proceso no puede continuar."
               Resource.String("STR_APP_GEN_MSG_WARNING"),
               MessageBoxButtons.OK,
               MessageBoxIcon.Warning,
               ParentForm);

          return;
        }
      }
    } // btn_cashier_concept

    //------------------------------------------------------------------------------
    // PURPOSE : Create cashier movements
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //            - DataTable
    //
    // RETURNS :
    //          - True:               Executed succesfully
    //          - False:              Executed unsuccesfully
    //   NOTES :
    // 
    private Boolean CreateCashierMovements(CashierConceptOperationResult Result, out VoucherSpecials VoucherFile, SqlTransaction Trx)
    {
      CashierMovementsTable _cashier_movements_table;
      CashierSessionInfo _cashier_session_info;
      Int32 _cashier_movement;
      Int64 _operation_id;
      CageConceptInformation _cage_concept;
      Int64 _sequence_value;

      _sequence_value = 0;
      VoucherFile = null;

      _cage_concept = m_cashier_concepts[Result.ConceptId];

      _cashier_movement = (Int32)(_cage_concept.CashierAction == CageMeters.CageSourceTargetCashierAction.CashierInput
                                                                               ? CASHIER_MOVEMENT.CASHIER_BY_CONCEPT_IN
                                                                               : CASHIER_MOVEMENT.CASHIER_BY_CONCEPT_OUT);
      try
      {
        _cashier_session_info = Cashier.CashierSessionInfo();
        _cashier_movements_table = new CashierMovementsTable(_cashier_session_info);

        if (!Operations.DB_InsertOperation(_cage_concept.OperationCode, 0, _cashier_session_info.CashierSessionId, 0,
                                           0, Result.Amount, 0, 0, 0, string.Empty, out _operation_id, Trx))
        {
          return false;
        }

        // Insert cashier movements

        if (FeatureChips.IsChipsType(Result.CurrencyType))
        {
          _cashier_movements_table.Add(_operation_id, (CASHIER_MOVEMENT)_cashier_movement, Result.Amount, 0, String.Empty, Result.Comment
          , Result.IsoCode, (Decimal)Result.Quantity, 0, 0, -1, Result.CurrencyType, FeatureChips.ConvertToCageCurrencyType(Result.CurrencyType), null);
          _cashier_movements_table.Add(_operation_id, (CASHIER_MOVEMENT)(_cashier_movement + (Int32)Result.ConceptId), Result.Amount, 0, String.Empty, String.Empty
          , Result.IsoCode, (Decimal)Result.Quantity, 0, 0, -1, Result.CurrencyType, FeatureChips.ConvertToCageCurrencyType(Result.CurrencyType), null);
        }
        else
        {
          _cashier_movements_table.Add(_operation_id, (CASHIER_MOVEMENT)_cashier_movement, Result.Amount, 0, String.Empty, Result.Comment
            , Result.IsoCode, (Decimal)Result.Quantity);
          _cashier_movements_table.Add(_operation_id, (CASHIER_MOVEMENT)(_cashier_movement + (Int32)Result.ConceptId), Result.Amount, 0, String.Empty, String.Empty
            , Result.IsoCode, (Decimal)Result.Quantity);
        }

        if (!_cashier_movements_table.Save(Trx))
        {
          return false;
        }

        if (!Sequences.GetValue(Trx, SequenceId.CageConcepts + (Int32)_cage_concept.ConceptId, out _sequence_value))
        {
          return false;
        }
        _cage_concept.SequenceId = SequenceId.CageConcepts + (Int32)_cage_concept.ConceptId;

        // lmrs 16/09/2014
        VoucherFile = new VoucherSpecials(_operation_id, _cage_concept, Result, PrintMode.Print, Trx);

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // CreateCashierMovements

    //------------------------------------------------------------------------------
    // PURPOSE : Validate frm_concept_input formulari result
    //
    //  PARAMS :
    //      - INPUT :
    //             - ConceptId
    //             - Result
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //          - True:               Validation is ok
    //          - False:              Validation is not ok
    //   NOTES :
    // 
    private Boolean ValidateCashierConceptOperationResult(CashierConceptOperationResult Result, Boolean IsUndoOperation)
    {
      Currency _cashier_current_balance;
      String _iso_code;

      if ((m_cashier_concepts[Result.ConceptId].CashierAction == CageMeters.CageSourceTargetCashierAction.CashierOutput && !IsUndoOperation)
         || (m_cashier_concepts[Result.ConceptId].CashierAction != CageMeters.CageSourceTargetCashierAction.CashierOutput && IsUndoOperation))
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          //EOR 08-SEP-2017
          if (Result.CurrencyType == CurrencyExchangeType.CURRENCY & Result.IsoCode == CurrencyExchange.GetNationalCurrency())
          {
            _iso_code = String.Empty;
          }
          else
          {
            _iso_code = Result.IsoCode;
          }
            
          _cashier_current_balance = CashierBusinessLogic.UpdateSessionBalance(_db_trx.SqlTransaction
                                                                             , Cashier.SessionId
                                                                             , 0
                                                                             , _iso_code
                                                                             , Result.CurrencyType);
        }

        if (_cashier_current_balance < Result.Amount)
        {
          frm_message.Show(Resource.String("STR_FRM_AMOUNT_INPUT_MSG_AMOUNT_EXCEED_BANK"),
            Resource.String("STR_APP_GEN_MSG_WARNING"),
            MessageBoxButtons.OK,
            MessageBoxIcon.Warning,
            ParentForm);

          return false;
        }
      }

      return true;
    } // ValidateCashierConceptOperationResult

    //------------------------------------------------------------------------------
    // PURPOSE : Mark a button Enabled or disabled
    //
    //  PARAMS :
    //      - INPUT :
    //             - Boolean: True=Enable ; False = Disable
    //             - Button
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    // 
    private void EnableButton(Boolean EnableDisable, Button Btn)
    {
      m_btn_undo.Enabled = EnableDisable;
    }

    #endregion // Private methods
  }
} // namespace
