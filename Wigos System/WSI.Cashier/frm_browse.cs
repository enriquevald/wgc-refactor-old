//------------------------------------------------------------------------------
// Copyright � 2014 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_browse.cs
// 
//   DESCRIPTION: Form to show a list of buttons.
//
//        AUTHOR: David Lasdiez
// 
// CREATION DATE: 10-ENE-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 10-ENE-2014 DLL    First release.
// 21-JAN-2015 OPC    Added new functionality for undo mb recharge.
// 21-JAN-2015 OPC    Added new functionality for undo mb deposit.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WSI.Common;
using WSI.Cashier.Controls;

namespace WSI.Cashier
{
  public enum BrowserType
  {
    UndoOperation = 0,
  }

  public partial class frm_browse : frm_base
  {

    public class ButtonType
    {
      public String Description;
      public Object ReturnedElement;
      public Image Image = null;
      public Boolean Enabled = true;
    }

    #region Attributes
    Boolean m_cancel;
    private frm_yesno form_yes_no;
    BrowserType m_browser_type;
    Object m_return_value;
    Dictionary<String, ButtonType> m_all_buttons;
    #endregion

    #region Constructor
    /// <summary>
    /// Constructor method.
    /// 1. Initialize controls.
    /// 2. Initialize control�s resources. (NLS, Images, etc)
    /// </summary>
    public frm_browse(BrowserType Type)
    {
      InitializeComponent();

      m_browser_type = Type;

      InitializeControlResources();

      form_yes_no = new frm_yesno();
      form_yes_no.Opacity = 0.6;
    }

    public frm_browse(BrowserType Type, Color BackgroundColor, Color ButtonsColor)
      : this(Type)
    {
      // Background Color
      this.pnl_container.BackColor = BackgroundColor;

      // Buttons
      this.btn_cancel.BackColor = ButtonsColor;
    }

    #endregion


    #region Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Initialize control resources (NLS strings, images, etc).
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void InitializeControlResources()
    {
      // - NLS Strings:
      //   - Title:
      switch (m_browser_type)
      {
        case BrowserType.UndoOperation:
          this.FormTitle = Resource.String("STR_UNDO_OPERATION_TICKET");
          break;

        default:
          this.FormTitle = Resource.String("STR_FRM_VOUCHERS_REPRINT_030");
          break;
      }


      //   - Labels

      //   - Buttons
      btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Create buttons for each option
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void CreateButtons(List<ButtonType> ButtonList)
    {
      Int32 _position_x;
      Int32 _position_y;
      Int32 _width;
      Int32 _height;
      uc_round_button _btn;
      Int32 _initial_x;
      Int32 _initial_y;
      Int32 _idx;

      _initial_x = 4;
      _initial_y = 4;
      _position_x = _initial_x;
      _position_y = _initial_y;
      _width = 330;
      _height = 60;
      m_all_buttons = new Dictionary<String, ButtonType>();

      _idx = 0;
      foreach (ButtonType _button in ButtonList)
      {
        _btn = new uc_round_button();
        _btn.Enabled = _button.Enabled;
        _btn.Name = "name_" + _idx;
        _btn.TextAlign = ContentAlignment.MiddleCenter;

        if (_button.Image != null)
        {
          _btn.Image = _button.Image;
          _btn.TextAlign = ContentAlignment.BottomCenter;
        }
        _btn.ImageAlign = ContentAlignment.TopCenter;
        _btn.Style = uc_round_button.RoundButonStyle.PRINCIPAL;
        _btn.Text = _button.Description;
        _btn.Size = new Size(_width, _height);
        m_all_buttons.Add(_btn.Name, _button);
        _btn.Click += new EventHandler(_btn_Click);

        if (_position_x + _width + 3 >= pnl_container.Width)
        {
          _position_x = _initial_x;
        }
        _btn.Location = new Point(_position_x, _position_y);
        pnl_container.Controls.Add(_btn);
        _position_x += _width + 8;
        _position_y += _height + 10;

        //is used to set button name
        _idx++;
      }
    }

    private void _btn_Click(object sender, EventArgs e)
    {
      m_return_value = m_all_buttons[((Button)sender).Name].ReturnedElement;

      Misc.WriteLog("[FORM CLOSE] frm_browse", Log.Type.Message);
      this.Close();
    } // _btn_Click

    private void btn_cancel_Click(object sender, EventArgs e)
    {
      m_cancel = false;
      Misc.WriteLog("[FORM CLOSE] frm_browse (cancel)", Log.Type.Message);
      this.Close();
    } //btn_cancel_Click


    public override void pb_exit_Click(object sender, EventArgs e)
    {
      m_cancel = false;

      Misc.WriteLog("[FORM CLOSE] frm_browse (exit)", Log.Type.Message);
      this.Close();
    }


    #endregion

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Init and Show the form.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public Boolean Show(List<ButtonType> ListButton, out Object SelectedValue)
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_browse", Log.Type.Message);

      InitializeControlResources();

      CreateButtons(ListButton);

      form_yes_no.Show();

      m_cancel = true;

      this.ShowDialog();
      form_yes_no.Hide();
      SelectedValue = m_return_value;

      return m_cancel;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Dispose yes_no form.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    new public void Dispose()
    {
      form_yes_no.Dispose();
      form_yes_no = null;

      base.Dispose();
    }

    #endregion

  }
}