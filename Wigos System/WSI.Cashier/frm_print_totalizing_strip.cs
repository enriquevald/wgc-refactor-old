//------------------------------------------------------------------------------
// Copyright � 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_print_totalizing_strip.cs
// 
//   DESCRIPTION: 
//
//        AUTHOR: Ernesto Salazar
// 
// CREATION DATE: 14-JUL-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 28-JUL-2016 ESE    First release.
// 06-OCT-2016 FAV    Fixed Bug 18632:Televisa: The voucher should show only approved transactions
// 31-OCT-2016 ESE    Bug 19776: Show only the actual session movements
// 03-NOV-2016 ETP    Bug 18839: Televisa: Movements in datetime specified in until textbox are not showed.
// 31-OCT-2016 ESE    Bug 19776: Show only the actual session movements
// 01-FEB-2017 MS     Bug 21940:Tira Totalizadora: error con el filtro de horas
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Windows.Forms;
using WSI.Common;
using System.Threading;
using System.Drawing;
using WSI.Cashier.Controls;
using System.Collections;
using WSI.Common.PinPad;

namespace WSI.Cashier
{
  public partial class frm_print_totalizing_strip : frm_base
  {
    #region Members

    private frm_yesno m_form_yes_no;
    private CashierSessionInfo m_cashier_info;
    private DateTime m_cashier_session_open_date;

    #endregion

    #region Constructor

    /// <summary>
    /// Constructor method.
    /// 1. Initialize controls.
    /// 2. Initialize control�s resources. (NLS, Images, etc)
    /// </summary>
    public frm_print_totalizing_strip()
    {
      InitializeComponent();
      InitializeControlResources();
      m_form_yes_no = new frm_yesno();
      m_form_yes_no.Opacity = 0.6;
      
      //Get the session info
      m_cashier_info = Cashier.CashierSessionInfo();
    } // frm_print_totalizing_strip

    #endregion

    #region Private Methods

    /// <summary>
    /// Init resources
    /// </summary>
    private void InitializeControlResources()
    {
      // NLS Strings
      this.FormTitle = "";
    } // InitializeControlResources

    /// <summary>
    /// Init variables
    /// </summary>
    private void Init()
    {
      DateTime _time_now;

      _time_now = WGDB.Now;

      this.FormTitle = Resource.String("STR_CASHIER_TOTALIZING_STRIP");
      btn_print_selected.Text = Resource.String("PRINT_LABEL_PRINT");
      btn_close.Text = Resource.String("PRINT_LABEL_CLOSE");

      uc_datetime_since.DateText = Resource.String("STR_SINCE");
      uc_datetime_until.DateText = Resource.String("STR_UNTIL");

      if (getCashOpenDate())
      {
        uc_datetime_since.DateValue = m_cashier_session_open_date;
        uc_datetime_until.DateValue = _time_now.Date;
      }
      else
      {
        m_cashier_session_open_date = new DateTime();
      }

      txt_hour_since.Text = m_cashier_session_open_date.Hour.ToString().PadLeft(2, '0');
      txt_mins_since.Text = m_cashier_session_open_date.Minute.ToString().PadLeft(2, '0');

      lbl_txt_day_now.Text = Resource.String("STR_ACCOUNT_PERSONAL_DATA_DATE") + " " + _time_now.ToLongDateString() + "\n"
                           + Resource.String("STR_VOUCHER_CASH_DESK_OPEN_TITLE") + ": " + uc_datetime_since.DateValue.ToString("dd/MM/yyyy")
                           + " " + txt_hour_since.Text + ":" + txt_mins_since.Text;

      txt_hour_until.Text = _time_now.Hour.ToString().PadLeft(2, '0');
      txt_mins_until.Text = _time_now.Minute.ToString().PadLeft(2, '0');

      this.Location = new Point(1024 / 2 - this.Width / 2, 768 / 2 - this.Height / 2);
    } // Init

    /// <summary>
    /// Get opening date from cashier session
    /// </summary>
    /// <returns></returns>
    private Boolean getCashOpenDate()
    {
      StringBuilder _sb;
      Object _obj;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT   CONVERT(CHAR(20),CS_OPENING_DATE,120) ");
        _sb.AppendLine("   FROM   CASHIER_SESSIONS                      ");
        _sb.AppendLine("  WHERE   CS_SESSION_ID = @pSessionId           ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = m_cashier_info.CashierSessionId;

            _obj = _cmd.ExecuteScalar();

            if (_obj != null && _obj != DBNull.Value)
            {
              m_cashier_session_open_date = Format.DBDatetimeToLocalDateTime(_obj.ToString());
          }

            return true;
        }
      }

    }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // getCashOpenDate

    /// <summary>
    /// Select to the table [pinpad_transactions] 
    /// </summary>
    private void Get_PinpadTransactions_DB()
    {
      DateTime _date_since;
      DateTime _date_until;
      DataTable _dt_pinpad;
      Boolean _voucher_saved;
      StringBuilder _sb;
      Int32 _hour_since;
      Int32 _minute_since;
      Int32 _hour_until;
      Int32 _minute_until;

      try
      {
        if (uc_datetime_since.Invalid)
        {
          frm_message.Show(Resource.String("STR_SINCE") + " " + Resource.String("ERR_FECHA"), Resource.String("STR_APP_GEN_MSG_WARNING"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation, this);

        return;
      }

        if (!String.IsNullOrEmpty(txt_hour_since.Text))
      {
          if (Int32.TryParse(txt_hour_since.Text,out _hour_since))
          {
            if (_hour_since < 0 || _hour_since > 23)
            {
              frm_message.Show(Resource.String("STR_SINCE") + " " + Resource.String("ERR_HOUR"), Resource.String("STR_APP_GEN_MSG_WARNING"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation, this);

        return;
      }
          }
          else
          {
            frm_message.Show(Resource.String("STR_SINCE") + " " + Resource.String("ERR_HOUR"), Resource.String("STR_APP_GEN_MSG_WARNING"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation, this);

            return;
          }
        }
        else
        {
          frm_message.Show(Resource.String("STR_SINCE") + " " + Resource.String("ERR_HOUR"), Resource.String("STR_APP_GEN_MSG_WARNING"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation, this);
      
          return;
        }

        if (!String.IsNullOrEmpty(txt_mins_since.Text))
      {
          if (Int32.TryParse(txt_mins_since.Text, out _minute_since))
          {
            if (_minute_since < 0 || _minute_since > 59)
            {
              frm_message.Show(Resource.String("STR_SINCE") + " " + Resource.String("ERR_MINUTE"), Resource.String("STR_APP_GEN_MSG_WARNING"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation, this);

              return;
            }
          }
          else
          {
            frm_message.Show(Resource.String("STR_SINCE") + " " + Resource.String("ERR_MINUTE"), Resource.String("STR_APP_GEN_MSG_WARNING"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation, this);

            return;
          }
        }
        else
        {
          frm_message.Show(Resource.String("STR_SINCE") + " " + Resource.String("ERR_MINUTE"), Resource.String("STR_APP_GEN_MSG_WARNING"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation, this);

          return;
        }

        if (uc_datetime_until.Invalid)
          {
          frm_message.Show(Resource.String("STR_UNTIL") + " " + Resource.String("ERR_FECHA"), Resource.String("STR_APP_GEN_MSG_WARNING"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation, this);

          return;
        }

        if (!String.IsNullOrEmpty(txt_hour_until.Text))
            {
          if (Int32.TryParse(txt_hour_until.Text, out _hour_until))
          {
            if (_hour_until < 0 || _hour_until > 23)
            {
              frm_message.Show(Resource.String("STR_UNTIL") + " " + Resource.String("ERR_HOUR"), Resource.String("STR_APP_GEN_MSG_WARNING"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation, this);

              return;
            }
          }
          else
              {
            frm_message.Show(Resource.String("STR_UNTIL") + " " + Resource.String("ERR_HOUR"), Resource.String("STR_APP_GEN_MSG_WARNING"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation, this);

            return;
              }
        }
              else
              {
          frm_message.Show(Resource.String("STR_UNTIL") + " " + Resource.String("ERR_HOUR"), Resource.String("STR_APP_GEN_MSG_WARNING"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation, this);

          return;
              }

        if (!String.IsNullOrEmpty(txt_mins_until.Text))
        {
          if (Int32.TryParse(txt_mins_until.Text, out _minute_until))
          {
            if (_minute_until < 0 || _minute_until > 59)
            {
              frm_message.Show(Resource.String("STR_UNTIL") + " " + Resource.String("ERR_MINUTE"), Resource.String("STR_APP_GEN_MSG_WARNING"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation, this);

              return;
            }
          }
          else
          {
            frm_message.Show(Resource.String("STR_UNTIL") + " " + Resource.String("ERR_MINUTE"), Resource.String("STR_APP_GEN_MSG_WARNING"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation, this);

            return;
        }
      }
        else
        {
          frm_message.Show(Resource.String("STR_UNTIL") + " " + Resource.String("ERR_MINUTE"), Resource.String("STR_APP_GEN_MSG_WARNING"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation, this);

          return;
        }

        _date_since = new DateTime(uc_datetime_since.DateValue.Year,
                                   uc_datetime_since.DateValue.Month,
                                   uc_datetime_since.DateValue.Day,
                                   _hour_since,
                                   _minute_since,
                                   0);

        _date_until = new DateTime(uc_datetime_until.DateValue.Year,
                                   uc_datetime_until.DateValue.Month,
                                   uc_datetime_until.DateValue.Day,
                                   _hour_until,
                                   _minute_until,
                                   0);

        //_date_until can not be later tha NOW
        if (_date_until.CompareTo(WGDB.Now) == 1 /*later*/)
        {
          frm_message.Show(Resource.String("STR_CASHIER_TOTALIZING_STRIP_CURRENT"), Resource.String("STR_APP_GEN_MSG_WARNING"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation, this);

          return;
    }

        //_date_until must be later than _date_since
        if (_date_since.CompareTo(_date_until) == 1 /*later*/)
    {
          frm_message.Show(Resource.String("STR_CASHIER_TOTALIZING_STRIP_EARLIER"), Resource.String("STR_APP_GEN_MSG_WARNING"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation, this);

          return;
    }

        //_date_until must not be earlier than m_cashier_session_open_date
        //_date_since has 0 seconds so when we compare we subtract the seconds from m_cashier_session_open_date
        if (_date_since.CompareTo(m_cashier_session_open_date.AddSeconds(-m_cashier_session_open_date.Second)) < 0 /*earlier*/)
    {
          frm_message.Show(Resource.String("STR_CASHIER_TOTALIZING_STRIP_BEFORE_OPEN"), Resource.String("STR_APP_GEN_MSG_WARNING"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation, this);

          return;
        }

        //To cater for last minute registers
        _date_until = _date_until.AddSeconds(59);

        _voucher_saved = false;

        _dt_pinpad = new DataTable();

        _sb = new StringBuilder();
        _sb.AppendLine("     SELECT   PT_ID                                                    ");
        _sb.AppendLine("            , PT_USER_ID                                               ");
        _sb.AppendLine("            , CONVERT(CHAR(20),PT_CREATED,120) AS PT_CREATED           ");
        _sb.AppendLine("            , PT_CARD_NUMBER                                           ");
        _sb.AppendLine("            , PT_CARD_ISO_CODE                                         ");
        _sb.AppendLine("            , PT_TOTAL_AMOUNT                                          ");
        _sb.AppendLine("            , PT_REFERENCE                                             ");
        _sb.AppendLine("            , CONVERT(CHAR(20),CS_OPENING_DATE,120) AS CS_OPENING_DATE ");
        _sb.AppendLine("       FROM   PINPAD_TRANSACTIONS                                      ");
        _sb.AppendLine(" INNER JOIN   CASHIER_SESSIONS ON CS_USER_ID = PT_USER_ID              ");
        _sb.AppendLine("        AND   CS_SESSION_ID =  @pSessionId                             ");
        _sb.AppendLine("        AND   PT_STATUS     =  @pStatusApproved                        ");
        _sb.AppendLine("        AND   PT_CREATED    >= @pDateSince                             ");
        _sb.AppendLine("        AND   PT_CREATED    <  @pDateUntil                             ");
        _sb.AppendLine("        AND   PT_CREATED    >= CS_OPENING_DATE                         ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
      {
            _cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = m_cashier_info.CashierSessionId;
            _cmd.Parameters.Add("@pStatusApproved", SqlDbType.Int).Value = PinPadTransactions.STATUS.APPROVED;
            _cmd.Parameters.Add("@pDateSince", SqlDbType.DateTime).Value = _date_since;
            _cmd.Parameters.Add("@pDateUntil", SqlDbType.DateTime).Value = _date_until;

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
      {
              _sql_da.Fill(_dt_pinpad);

              if (_dt_pinpad.Rows.Count > 0)
        {
                _voucher_saved = PrintVoucher(_dt_pinpad, Format.DBDatetimeToLocalDateTime(_dt_pinpad.Rows[0]["CS_OPENING_DATE"].ToString()), _db_trx.SqlTransaction);
      }
      else
      {
                frm_message.Show(Resource.String("STR_CASHIER_TOTALIZING_STRIP_NO_MOV"), Resource.String("STR_APP_GEN_MSG_WARNING"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation, this);

                return;
              }
            }
          }

          if (!_voucher_saved)
        {
          frm_message.Show(Resource.String("STR_CASHIER_TOTALIZING_STRIP_MINUTES_RANGE"), Resource.String("STR_APP_GEN_MSG_WARNING"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation, this);

            return;
          }

          _db_trx.Commit();
        }
        }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      this.Close();
    } // Get_PinpadTransactions_DB

    /// <summary>
    /// Print voucher
    /// </summary>
    /// <param name="PinpadTable"></param>
    /// <param name="CashierSessionDatetime"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    private Boolean PrintVoucher(DataTable PinpadTable, DateTime CashierSessionDatetime, SqlTransaction Trx)
    {
      ArrayList _voucher_list;
      VoucherTotalizingStrip _voucher_strip;

      _voucher_list = new ArrayList();
      _voucher_strip = new VoucherTotalizingStrip(PinpadTable, CashierSessionDatetime, PrintMode.Print, Trx);

      _voucher_list.Add(_voucher_strip);

      VoucherPrint.Print(_voucher_list);

      // Save voucher in cashier_vouchers table
      if (!_voucher_strip.Save(Trx))
      {
        Log.Error("frm_print_totalizing_strip.PrintVoucher. Error on save totalizing stript voucher.");

        return false;
    }

      return true;
    } // PrintVoucher

    #endregion

    #region Public Methods

    /// <summary>
    /// Show dialog function
    /// </summary>
    /// <param name="Parent"></param>
    static public void Show(Form Parent)
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_print_totalizing_strip", Log.Type.Message);

      frm_print_totalizing_strip _wgc_automatic_print;

      _wgc_automatic_print = new frm_print_totalizing_strip();

      _wgc_automatic_print.Init();

      _wgc_automatic_print.m_form_yes_no.Show(Parent);
      
      _wgc_automatic_print.ShowDialog(_wgc_automatic_print.m_form_yes_no);
      _wgc_automatic_print.m_form_yes_no.Hide();

      if (Parent != null)
      {
        Parent.Focus();
      }

      _wgc_automatic_print.Dispose();
      _wgc_automatic_print = null;
    } // Show

    /// <summary>
    /// Dispose yes_no formDispose yes_no form
    /// </summary>
    new public void Dispose()
    {
      m_form_yes_no.Dispose();
      m_form_yes_no = null;

      base.Dispose();
    } // Dispose

    #endregion

    #region Events

    /// <summary>
    /// Print event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
      private void btn_print_selected_Click(object sender, EventArgs e)
      {
        Get_PinpadTransactions_DB();
    } // btn_print_selected_Click

    /// <summary>
    /// Close event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
      private void btn_close_Click(object sender, EventArgs e)
      {
        Misc.WriteLog("[FORM CLOSE] frm_print_totalizing_strip (close)", Log.Type.Message);
        this.Close();
    } // btn_close_Click

    /// <summary>
    /// Toggle on-screen keyboard upon pressing button
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btn_keyboard_Click(object sender, EventArgs e)
        {
      // show keyboard (if hidden)
      WSIKeyboard.ForceToggle();
    } // btn_keyboard_Click

    #endregion
  }
}
