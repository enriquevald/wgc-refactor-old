﻿//------------------------------------------------------------------------------
// Copyright © 2007-2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: PlayerEditDetailsBaseModel.cs
// 
//   DESCRIPTION: Model for cashier MVC
//                
//        AUTHOR: Scott Adamson.
// 
// CREATION DATE: 02-DEC-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 06-DEC-2017 RGR    Bug 31047:WIGOS-6629 [Ticket #10514] 03.007.0015 [v03.007] MKT: ERROR al realizar ALTA de cliente nuevo
// 05-MAR-2018 MS     Bug 31789: WIGOS-8441 Fields not marked as required in Cashier after edition in Account Data customization GUI screen
// ----------- ------ ----------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Drawing;
using WSI.Cashier.MVC.Interface;
using WSI.Common;
using WSI.Common.Entities.General;
using WSI.Common.Entrances;

namespace WSI.Cashier.MVC.Model.PlayerEditDetails.Common
{
  public abstract class PlayerEditDetailsBaseModel : BaseModel,ICustomerNameAndId
  {
    public MaritalStatus HolderMaritalStatus { get; set; }
    public List<MaritalStatus> MaritalStatuses { get; set; }

    public List<InvalidField> InvalidFields { get; set; }//InvalidFields
    public List<Nationality> Nationalities { get; set; }//Nationalities
    public List<DocumentType> DocumentTypes { get; set; }//DocumentTypes
    public List<Country> Countries { get; set; }//Countries
    public List<Gender> Genders { get; set; }//Genders
    public bool IsNew { get; set; }
    public bool DuplicatedID { get; set; }
    public bool DuplicatedDetails { get; set; }
    public int PinFailures { get; set; }//PinFailures
    public string Pin { get; set; }//Pin
    public int CardLevel { get; set; }//CardLevel
    public string CardLevelName { get; set; }//CardLevelName
    public bool HolderIsVip { get; set; }//HolderIsVip
    public Dictionary<int, string> IDDocuments { get; set; }

    protected override string[] FieldsBase
    {
      get
      {
        return new[]
          {
            /*"DocumentDuplicated",*/ "HolderName1", "HolderName2", "HolderName3", "HolderName4", "HolderDocumentType", "HolderDocument",
            "HolderAge",  "HolderBirthCountry", "HolderGender",
            "HolderNationality", "HolderMaritalStatus","HolderPhone1", "HolderPhone2", "HolderEmail1", "HolderAddress", "HolderAddress02",
            "HolderAddress03", "HolderAddressCountry", "HolderCity", "HolderFedEntity", "HolderZip", "HolderExtNum",
            "HolderTwitterAccount", "HolderPhoto"
          };
      }
    }


    /// <summary>
    ///   Get property value by name, future: normalize names and use reflection
    /// </summary>
    /// <param name="PropertyName"></param>
    /// <returns></returns>
    internal override object GetValueBase(string PropertyName)
    {
      switch (PropertyName)
      {
        case "HolderPhoto":
          return HolderPhoto;
        case "HolderName1":
          return HolderName1;
        case "HolderName2":
          return HolderName2;
        case "HolderName3":
          return HolderName3;
        case "HolderName4":
          return HolderName4;
        case "HolderDocumentType":
          return HolderDocumentType;
        case "HolderDocument":
          return HolderDocument;
        case "HolderBirthDate":
          return HolderBirthDate;
        case "HolderBirthCountry":
          return HolderBirthCountry;
        case "HolderGender":
          return HolderGender;
        case "HolderNationality":
          return HolderNationality;
        case "HolderMaritalStatus":
          return HolderMaritalStatus;
        case "HolderPhone1":
          return HolderPhone1;
        case "HolderPhone2":
          return HolderPhone2;
        case "HolderEmail1":
          return HolderEmail1;
        case "HolderAddress":
          return HolderAddressLine01;
        case "HolderAddress02":
          return HolderAddressLine02;
        case "HolderAddress03":
          return HolderAddressLine03;
        case "HolderAddressCountry":
          return HolderAddressCountry;
        case "HolderCity":
          return HolderCity;
        case "HolderFedEntity":
          return HolderFedEntity;
        case "HolderZip":
          return HolderZip;
        case "HolderExtNum":
          return HolderExtNum;
        case "HolderTwitterAccount":
          return HolderTwitterAccount;
        case "HolderDocument3Type":
          return HolderDocument3Type;
        case "BeneficiaryDocument3Type":
          return BeneficiaryDocument3Type;
        default:
          return null;
      }
    }//GetValueBase
    #region Main Details

    public Image HolderPhoto { get; set; }//
    public bool PhotoChanged { get; set; }//
    public bool HasPhoto { get; set; }//
    public long? AccountId { get; set; }//
    public string HolderName1 { get; set; } //
    public string HolderName2 { get; set; } //
    public string HolderName3 { get; set; } //
    public string HolderName4 { get; set; } //
    public string HolderFullName { get; set; } //
    public string HolderDocument3Type { get; set; }
    public string BeneficiaryDocument3Type { get; set; }
    public DocumentType HolderDocumentType { get; set; } //
    public string HolderDocument { get; set; } //
    public DateTime? HolderBirthDate { get; set; } //
    public Country HolderBirthCountry { get; set; }
    public DateTime CardCreationDate { get; set; }

    public Gender HolderGender { get; set; } //
    public Nationality HolderNationality { get; set; } //

    public HolderHappyBirthDayNotification HolderBirthDayDateShowNotification { get; set; } //

    #endregion
    #region Contact

    public string HolderPhone1 { get; set; } //
    public string HolderPhone2 { get; set; } //
    public string HolderEmail1 { get; set; } //
    public string HolderTwitterAccount { get; set; }

    #endregion
    #region Address

    public ADDRESS_VALIDATION HolderAddressValidation { get; set; }
    public int HolderAddressType { get; set; }
    public string HolderAddressLine01 { get; set; } //
    public string HolderAddressLine02 { get; set; } //
    public string HolderAddressLine03 { get; set; } //
    public string HolderExtNum { get; set; } //
    public string HolderCity { get; set; } //
    public State HolderFedEntity { get; set; }
    public Country HolderAddressCountry { get; set; } //
    public string HolderZip { get; set; } //
    #endregion
    #region Comments

    public string Comments { get; set; }
    public bool ShowComments { get; set; }

    #endregion
    #region Docs  

    public DocumentList HolderDocScan { get; set; }

    public bool MustShowComments { get; set; }

    #endregion
  }
}
