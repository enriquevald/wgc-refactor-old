//------------------------------------------------------------------------------
// Copyright � 2007-2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: PlayerEditDetailsBaseModelValidator.cs
// 
//   DESCRIPTION: Base extendable validator for player edit details mvc
//                
//        AUTHOR: Scott Adamson.
// 
// CREATION DATE: 02-DEC-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 02-DEC-2007 SJA    First release.
// 06-APR-2016 SJA    BUG 11493: fix behaviour of gp antilaundry has beneficiary Resolve
// 20-MAY-2016 JBP    Product Backlog Item 13262:Recepci�n - versi�n Junio
// 30-MAY-2016 FJC    Fixed Bug 13793:Recepci�n: Mensaje de error no esperado cuando das de alta un cliente con a�o de nacimiento incorrecto
// 13-JUN-2016 SMN    Fixed Bug 14233:Recepci�n: El campo Tipo de Documento vaci� es permitido cuando creas un nuevo cliente o editas uno existente.
// 24-NOV-2016 ETP    Fixed Bug 20852: Cashier: When creating account, missing occupacion message is incorrect
// 26-SEP-2017 ETP    Fixed Bug 29923:[WIGOS-4737] When the GP Account.RequestedField.DocScan have the value 1, is always asking to scan documents
// 04-OCT-2017 ETP    Fixed Bug Bug 29878:[WIGOS-5065][WIGOS-4646]: Exception when the user do any modification in the "Account data editor"
// 15-NOV-2017 RAB    Bug 30799:WIGOS-6127 Threshold - Tito: warning message "Debe introducir un valor para Entidad Federativa" when the required field is "Provincia" on input data screen
// 06-DEC-2017 RGR    Bug 31047:WIGOS-6629 [Ticket #10514] 03.007.0015 [v03.007] MKT: ERROR al realizar ALTA de cliente nuevo
// 05-MAR-2018 MS     Bug 31789: WIGOS-8441 Fields not marked as required in Cashier after edition in Account Data customization GUI screen
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using WSI.Common;
using WSI.Common.Entities.General;
using WSI.Common.Utilities.Extensions;

namespace WSI.Cashier.MVC.Model.PlayerEditDetails.Common.Validator
{
  public abstract class PlayerEditDetailsBaseModelValidator<T> : BaseModelValidator<T> where T : PlayerEditDetailsBaseModel
  {
    #region IMVCValidator Members

    #endregion
    protected PlayerEditDetailsBaseModelValidator(T Mdl)
      : base(Mdl)
    {
    }

    /// <summary>
    ///   utility method to validate an email
    /// </summary>
    /// <param name="StrIn"></param>
    /// <returns></returns>


    /// <summary>
    ///   base validation method
    /// </summary>
    /// <returns></returns>
    public override List<InvalidField> ValidateBase(List<string> HiddenFields, List<string> RequiredFields)
    {
      if (Model.MustShowComments)
      {
        return new List<InvalidField>();
      }
      return base.ValidateBase(HiddenFields, RequiredFields);
    }

    /// <summary>
    ///   Routine to validate fields
    /// </summary>
    /// <param name="PropertyName"></param>
    /// <param name="HiddenFields"></param>
    /// <param name="RequiredFields"></param>
    /// <param name="Mdl"></param>
    /// <returns></returns>
    internal override InvalidField ValidateField(string PropertyName, List<string> HiddenFields, List<string> RequiredFields,
                                        T Mdl)
    {
      String _typelist;
      String _selected_doc;

      if (!PropertyName.Contains("Age") && !NeedsValidating(PropertyName, HiddenFields, RequiredFields))
      {
        return null;
      }
      switch (PropertyName)
      {
        case "DocumentDuplicated":
          {
            Model.DuplicatedID = false || CardData.CheckIdAlreadyExists(Model.HolderDocument,
              Model.AccountId ?? -1,
              (ACCOUNT_HOLDER_ID_TYPE)(Model.HolderDocumentType ?? new DocumentType() { Id = -1 }).Id);
            return null;
          }

        case "HolderRFC":
          {
            if (ValidateFormat.WrongRFC((string)Model.GetValue("HolderRFC")))
            {
              return
                (new InvalidField { Id = "HolderRFC", Reason = Resource.String("STR_FRM_WITHOLDING_009", (string)Model.GetValue("HolderRFC")) });
            }
            if (Model.HolderBirthDate.HasValue && (!ValidateFormat.CheckRFC((string)Model.GetValue("HolderRFC"), Model.HolderName1, Model.HolderName2, Model.HolderName3 + " " + Model.HolderName4, Model.HolderBirthDate.Value)))
            {
              return new InvalidField
              {
                Id = "HolderRFC",
                Reason =
                  Resource.String("STR_FRM_ACCOUNT_USER_EDIT_EXPECTED_RFC",
                    ValidateFormat.ExpectedRFC(Model.HolderName1, Model.HolderName2, (Model.HolderName3 + " " + Model.HolderName4).Trim(),
                      Model.HolderBirthDate.Value))
              };
            }
            return null;
          }

        case "HolderCURP":
          {
            if (Model.HolderBirthDate.HasValue && !((string)Model.GetValue("HolderRFC")).IsNullOrWhiteSpace() && !ValidateFormat.CheckCURP((string)Model.GetValue("HolderCURP"), (string)Model.GetValue("HolderRFC"), Model.HolderGender.Id))
            {
              return new InvalidField
              {
                Id = "HolderCURP",
                Reason =
                  Resource.String("STR_FRM_ACCOUNT_USER_EDIT_EXPECTED_CURP",
                    ValidateFormat.ExpectedRFC(Model.HolderName1, Model.HolderName2, (Model.HolderName3 + " " + Model.HolderName4).Trim(),
                      Model.HolderBirthDate.Value).Substring(0, 10) + "------")
              };
            }
            return null;
          }
        case "HolderEmail1":
        case "HolderEmail2":
          {
            return !((string)Model.GetValue(PropertyName)).IsNullOrWhiteSpace() && IsValidEmail((string)Model.GetValue(PropertyName))
              ? null
              : new InvalidField { Id = PropertyName, Reason = GetReason(Model.GetValue(PropertyName), Resource.String(GetResourceString(PropertyName))) };
          }
        case "HolderTwitterAccount":
          {
            return !((string)Model.GetValue(PropertyName)).IsNullOrWhiteSpace() && IsValidTwitter((string)Model.GetValue(PropertyName))
              ? null
              : new InvalidField { Id = PropertyName, Reason = GetReason(Model.GetValue(PropertyName), Resource.String(GetResourceString(PropertyName))) };
          }

        case "HolderPhoto":
          {
            return Model.HasPhoto ? null : new InvalidField { Id = "HolderPhoto", Reason = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_PHOTO") };
          }
        case "HolderDocumentType":
        case "HolderDocument":
          {
            if (IsEmpty(Mdl.HolderDocumentType))
            {
              return new InvalidField
                {
                  Id = "HolderDocumentType",
                  Reason = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_NO_DOCUMENT_TYPE")
                };
            }
            if (IsEmpty(Mdl.HolderDocument))
            {
              return new InvalidField
                {
                  Id = PropertyName,
                  Reason = GetReason(Mdl.GetValue("HolderDocument"), Resource.String(GetResourceString("HolderDocument")))
                };
            }
            return null;
          }
        case "HolderWeddingDate":
          {
            DateTime? _birth_date = Mdl.GetValue("HolderBirthDate") as DateTime?;
            DateTime? _wedding_date = Mdl.GetValue("HolderWeddingDate") as DateTime?;
            MaritalStatus _marital_status = Mdl.GetValue("HolderMaritalStatus") as MaritalStatus;

            if ((_wedding_date.HasValue && ValidateFormat.IsValidDate(_wedding_date.Value)) &&
                (_marital_status != null && _marital_status.Id < 2))
            {
              return new InvalidField //TODO RLO Mensaje incorrecto
                {
                  Id = PropertyName,
                  Reason = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_WEDDING_ERROR_MARITAL_STATUS")
                };
            }
            if ((!_wedding_date.HasValue || !ValidateFormat.IsValidDate(_wedding_date.Value)) &&
                (_marital_status != null && _marital_status.Id < 2))
            {
              return new InvalidField
              {
                Id = PropertyName,
                Reason = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_WEDDING_ERROR")
              };
            }
            if ((!_wedding_date.HasValue || !ValidateFormat.IsValidDate(_wedding_date.Value)) &&
                (_marital_status != null && _marital_status.Id > 1))
            {
              return new InvalidField
                {
                  Id = PropertyName,
                  Reason = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_WEDDING_ERROR")
                };
            }

            if (_wedding_date != null &&
                (_birth_date.HasValue && NeedsValidating("HolderBirthDate", HiddenFields, RequiredFields) &&
                 !CardData.IsValidWeddingdate(_birth_date.Value, _wedding_date.Value)))
            {
              return new InvalidField
                {
                  Id = PropertyName,
                  Reason = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_WEDDING_ERROR")
                };
            }
            return null;
          }
        case "HolderAge":
          {
            if (!NeedsValidating("HolderBirthDate", HiddenFields, RequiredFields))
            {
              return null;
            }
            int _legal_age = 0;
            DateTime? _birth_date = Mdl.GetValue("HolderBirthDate") as DateTime?;
            if (!_birth_date.HasValue || !ValidateFormat.IsValidDate(_birth_date.Value))
            {
              return new InvalidField
                {
                  Id = "HolderBirthDate",
                  Reason =
                    Resource.String(GetResourceString("HolderBirthDate"))
                };
            }
            if (!CardData.IsValidBirthdate(_birth_date.Value, out _legal_age))
            {
              return new InvalidField
                {
                  Id = "HolderBirthDate",
                  Reason = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_NOT_ADULT", _legal_age)
                };
            }
            return null;
          }
        case "BeneficiaryAge":
          {
            if (!NeedsValidating("BeneficiaryBirthDate", HiddenFields, RequiredFields))
            {
              return null;
            }
            int _legal_age = 0;
            DateTime? _birth_date = Mdl.GetValue("BeneficiaryBirthDate") as DateTime?;
            if (!_birth_date.HasValue || !ValidateFormat.IsValidDate(_birth_date.Value))
            {
              return new InvalidField
                {
                  Id = "BeneficiaryBirthDate",
                  Reason =
                    Resource.String(GetResourceString("BeneficiaryBirthDate"))
                };
            }
            if (!CardData.IsValidBirthdate(_birth_date.Value, out _legal_age))
            {
              return new InvalidField
                {
                  Id = "BeneficiaryBirthDate",
                  Reason = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_NOT_ADULT_BENEFICIARY", _legal_age)
                };
            }

            return null;
          }
        case "HolderDocScan":
        case "BeneficiaryDocScan":
          string[] _separators;
          List<string> _inDocs;
          List<string> _requestedDocs;
          bool _contain;

          _contain = false;
          _inDocs = new List<string>();
          _separators = new string[] { "," };
          if (PropertyName == "HolderDocScan")
          {
            _selected_doc = (string)Mdl.GetValue("HolderDocument3Type");
            _typelist = GeneralParam.GetString("Account.RequestedField", "DocScanTypeList", "");
          }
          else
          {
            _selected_doc = (string)Mdl.GetValue("BeneficiaryDocument3Type");
            _typelist = GeneralParam.GetString("Beneficiary.RequestedField", "DocScanTypeList", "");
          }

          if (!string.IsNullOrEmpty(_selected_doc))
          {
            _inDocs = new List<string>(_selected_doc.Split(_separators, StringSplitOptions.RemoveEmptyEntries));
          }

          if (!string.IsNullOrEmpty(_typelist))
          {
            _requestedDocs = new List<string>(_typelist.Split(_separators, StringSplitOptions.RemoveEmptyEntries));

            foreach (var _item in _inDocs)
            {
              if (_requestedDocs.Contains(_item))
              {
                _contain = true;
                break;
              }
            }

            if (!_contain)
          {
            return new InvalidField
            {
              Id = PropertyName,
              Reason = GetReason(Mdl.GetValue(PropertyName), Resource.String(GetResourceString(PropertyName)))
            };
          }
          }

          return null;
      }

      if (IsEmpty(Mdl.GetValue(PropertyName)))
      {
        return new InvalidField
          {
            Id = PropertyName,
            Reason = GetReason(Mdl.GetValue(PropertyName), Resource.String(GetResourceString(PropertyName)))
          };
      }
      return null;
    }

    /// <summary>
    ///   Routine to validate field based on data type
    /// </summary>
    /// <param name="Value"></param>
    /// <returns></returns>
    protected override bool IsEmpty(object Value)
    {

      if (base.IsEmpty(Value))
        return true;

      switch (Value.GetType().Name)
      {

        case "Gender":
          return ((Gender)Value).Id < 1;
        case "State":
          return ((State)Value).Id < 0;
        case "Country":
          return ((Country)Value).Id < 1;
        case "OccupationWRK":
          return ((OccupationWRK)Value).Id < 1;
        case "Nationality":
          return ((Nationality)Value).Id < 1;
        case "MaritalStatus":
          return ((MaritalStatus)Value).Id < 1;
        case "DocumentList":
          return ((DocumentList)Value).Count == 0;
        case "DocumentType":
          return ((DocumentType)Value).Id < 0;
        default:
          return false;
      }
    }


    /// <summary>
    ///   routine to get friendly localized name for model field
    /// </summary>
    /// <param name="PropertyName"></param>
    /// <returns></returns>
    public override string GetResourceString(string PropertyName)
    {
      switch (PropertyName)
      {
        case "HolderPhoto":
          return "STR_FRM_PLAYER_EDIT_PHOTO";
        case "HolderName1":
          return "STR_FRM_PLAYER_EDIT_LAST_NAME1";
        case "HolderName2":
          return "STR_FRM_PLAYER_EDIT_LAST_NAME2";
        case "HolderName3":
          return "STR_FRM_ACCOUNT_USER_EDIT_NAME";
        case "HolderName4":
          return "STR_FRM_PLAYER_EDIT_NAME4";
        case "HolderDocumentType":
          return "STR_FRM_PLAYER_EDIT_TYPE_DOCUMENT";
        case "HolderDocument":
          return "STR_FRM_PLAYER_EDIT_DOCUMENT";
        case "HolderBirthDate":
          return "STR_FRM_ACCOUNT_USER_EDIT_BIRTHDATE_ERROR";
        case "HolderBirthCountry":
          return "STR_FRM_ACCOUNT_USER_EDIT_BIRTH_COUNTRY";
        case "HolderGender":
          return "STR_UC_PLAYER_TRACK_GENDER";
        case "HolderNationality":
          return "STR_FRM_ACCOUNT_USER_EDIT_NATIONALITY";
        case "HolderPhone1":
          return "STR_FRM_PLAYER_EDIT_PHONE";
        case "HolderPhone2":
          return "STR_FRM_PLAYER_EDIT_PHONE";
        case "HolderEmail1":
          return "STR_FRM_PLAYER_EDIT_EMAIL";
        case "HolderEmail2":
          return "STR_FRM_PLAYER_EDIT_EMAIL";
        case "HolderAddress":
          return "STR_UC_PLAYER_TRACK_ADDRESS_01";
        case "HolderAddress02":
          return "STR_UC_PLAYER_TRACK_ADDRESS_02";
        case "HolderAddress03":
          return "STR_UC_PLAYER_TRACK_ADDRESS_03";
        case "HolderAddressCountry":
          return "STR_FRM_ACCOUNT_USER_EDIT_COUNTRY";
        case "HolderCity":
          return "STR_UC_PLAYER_TRACK_CITY";
        case "HolderFedEntity":
          return "STR_FRM_ACCOUNT_USER_EDIT_FED_ENTITY";
        case "HolderZip":
          return "STR_UC_PLAYER_TRACK_ZIP";
        case "HolderExtNum":
          return "STR_FRM_ACCOUNT_USER_EDIT_EXT_NUM";
        case "BeneficiaryName1":
          return "STR_FRM_PLAYER_EDIT_LAST_NAME1";
        case "BeneficiaryName2":
          return "STR_FRM_PLAYER_EDIT_LAST_NAME2";
        case "BeneficiaryName3":
          return "STR_FRM_ACCOUNT_USER_EDIT_NAME";
        case "BeneficiaryBirthDate":
          return "STR_FRM_ACCOUNT_USER_EDIT_BENEFICIARY_BIRTH_DATE_ERROR";
        case "BeneficiaryGender":
          return "STR_UC_PLAYER_TRACK_GENDER";
        case "BeneficiaryRFC":
          return "STR_FRM_ACCOUNT_USER_EDIT_BENEFICIARY_RFC";
        case "BeneficiaryCURP":
          return "STR_FRM_ACCOUNT_USER_EDIT_BENEFICIARY_CURP";
        case "HolderOccupation":
          return "STR_FRM_PLAYER_EDIT_OCCUPATION";
        case "HolderMaritalStatus":
          return "STR_UC_PLAYER_TRACK_MARITAL_STATUS";
        case "HolderTwitterAccount":
          return "STR_UC_PLAYER_TRACK_TWITTER";
        case "BeneficiaryDocument":
          return "STR_ACCOUNT_PERSONAL_DATA_DOCUMENT_BEN";
        case "BeneficiaryOccupation":
          return "STR_FRM_PLAYER_EDIT_OCCUPATION";
        case "HolderDocScan":
          return "STR_FRM_ACCOUNT_USER_EDIT_NO_SCANNED_DOCUMENT";
        case "BeneficiaryDocScan":
          return "STR_FRM_ACCOUNT_USER_EDIT_NO_SCANNED_DOCUMENT";
        case "HolderRFC":
          return "STR_FRM_ACCOUNT_USER_EDIT_RFC";
        case "HolderCURP":
          return "STR_FRM_ACCOUNT_USER_EDIT_CURP";
        default:
          return "not defined:" + PropertyName;
      }
    }

    /// <summary>
    ///   method to get the localised reason for the validation failure
    /// </summary>
    /// <param name="Object"></param>
    /// <param name="FieldName"></param>
    /// <returns></returns>
    protected override string GetReason(object Object, string FieldName = "")
    {
      switch (Object.GetType().ToString())
      {
        case "System.String":
          {
            string _val = (string)Object;
            if (_val.IsNullOrWhiteSpace())
            {
              return Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC", FieldName);
            }
            return Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", FieldName);
          }
        case "WSI.Common.DocumentList":
          {
            return FieldName;
          }
        case "WSI.Common.Entities.General.State":
          {
            return Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC", AccountFields.GetStateName()); ;
          }
        default:
          {
            return Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC", FieldName);
          }
      }
    }
  }
}
