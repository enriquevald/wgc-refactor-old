//------------------------------------------------------------------------------
// Copyright � 2007-2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: PlayerEditdetailsReceptionModelValidator.cs
// 
//   DESCRIPTION: validator for reception model
//                
//                
//                
//                
//                
//                
// 
//        AUTHOR: Scott Adamson.
// 
// CREATION DATE: 02-DEC-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 02-DEC-2007 SJA    First release.
//------------------------------------------------------------------------------

using System.Collections.Generic;
using WSI.Cashier.MVC.Model.PlayerEditDetails.Common.Validator;
using WSI.Common;
using WSI.Common.Entities.General;

namespace WSI.Cashier.MVC.Model.PlayerEditDetails.Validator
{
  public class PlayerEditDetailsReceptionModelValidator :
    PlayerEditDetailsBaseModelValidator<PlayerEditDetailsReceptionModel>
  {
    public PlayerEditDetailsReceptionModelValidator(PlayerEditDetailsReceptionModel Mdl) : base(Mdl)
    {
    }

    /// <summary>
    ///   constructor
    /// </summary>
    /// <param name="Mdl"></param>


    /// <summary>
    ///   method to validate the model and return a list of errors if any
    /// </summary>
    /// <returns></returns>
    public override List<InvalidField> Validate(List<string> HiddenFields, List<string> RequiredFields)
    {
      var _retval = ValidateBase(HiddenFields, RequiredFields);
      if (Model.RecordExpirationDate < WGDB.Now.Date.AddDays(-1))
      {
        _retval.Add(new InvalidField
          {
            Id = "RecordExpirationDate",
            Reason = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_RECORD_EXPIRED")
          });
      }
      return _retval;
    }
  }
}
