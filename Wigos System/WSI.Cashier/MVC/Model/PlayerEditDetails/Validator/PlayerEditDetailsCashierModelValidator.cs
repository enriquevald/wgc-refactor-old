//------------------------------------------------------------------------------
// Copyright � 2007-2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: PlayerEditdetailsCashierModelValidator.cs
// 
//   DESCRIPTION: validator for cashier model
//                
//                
//                
//                
//                
//                
// 
//        AUTHOR: Scott Adamson.
// 
// CREATION DATE: 02-DEC-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 02-DEC-2007 SJA    First release.
// 06-APR-2016 SJA    Bug 11486: Cajero: no funciona el RFC ni el beneficiario
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using WSI.Cashier.MVC.Interface;
using WSI.Cashier.MVC.Model.PlayerEditDetails.Common.Validator;
using WSI.Common;
using WSI.Common.Entities.General;
using WSI.Common.Utilities.Extensions;

namespace WSI.Cashier.MVC.Model.PlayerEditDetails.Validator
{
  public class PlayerEditDetailsCashierModelValidator :
  PlayerEditDetailsBaseModelValidator<PlayerEditDetailsCashierModel>
  {
    /// <summary>
    ///   contrustor
    /// </summary>
    /// <param name="Mdl"></param>
    public PlayerEditDetailsCashierModelValidator(PlayerEditDetailsCashierModel Mdl) : base(Mdl)
    {
    }



    /// <summary>
    ///   validate the model and return a list of errors if any
    /// </summary>
    /// <returns></returns>
    public override List<InvalidField> Validate(List<string> HiddenFields, List<string> RequiredFields)
    {
      var _retval = ValidateBase(HiddenFields, RequiredFields);
      if (Model.HasBeneficiary)
      {
        List<string> _rqfields = new List<string>()
          {
            "BeneficiaryName1",
            "BeneficiaryName2",
            "BeneficiaryName3",
            "BeneficiaryGender",
            "BeneficiaryDocument",
            "BeneficiaryAge",
            "BeneficiaryOccupation",
            "BeneficiaryDocScan",
            "BeneficiaryRFC",
            "BeneficiaryCURP"
          };

        foreach (var _field in _rqfields)
        {

          if (!_field.Contains("Age") && !NeedsValidating(_field, HiddenFields, RequiredFields))
          {
            continue;
          }

          switch (_field)
          {
            case "BeneficiaryRFC":
            {
              if (ValidateFormat.WrongRFC(Model.BeneficiaryDocument))
              {
                _retval.Add
                  (new InvalidField
                    {
                      Id = "BeneficiaryRFC",
                      Reason = Resource.String("STR_FRM_WITHOLDING_009", Model.BeneficiaryDocument)
                    });
              }
              if (Model.BeneficiaryBirthDate.HasValue &&
                  (!ValidateFormat.CheckRFC(Model.BeneficiaryDocument, Model.BeneficiaryLastName, Model.BeneficiaryLastName2, Model.BeneficiaryName1,
                    Model.BeneficiaryBirthDate.Value)))
              {
                _retval.Add(new InvalidField
                  {
                    Id = "BeneficiaryRFC",
                    Reason =
                      Resource.String("STR_FRM_ACCOUNT_USER_EDIT_EXPECTED_RFC",
                        ValidateFormat.ExpectedRFC(Model.BeneficiaryLastName, Model.BeneficiaryLastName2, Model.BeneficiaryName1,
                          Model.BeneficiaryBirthDate.Value))
                  });
              }
              break;
            }

            case "BeneficiaryCURP":
            {
              if (Model.BeneficiaryBirthDate.HasValue && !Model.BeneficiaryDocument.IsNullOrWhiteSpace() &&
                  !ValidateFormat.CheckCURP(Model.BeneficiaryCURP, Model.BeneficiaryDocument, Model.BeneficiaryGender.Id))
              {
                _retval.Add(new InvalidField
                  {
                    Id = "BeneficiaryCURP",
                    Reason =
                      Resource.String("STR_FRM_ACCOUNT_USER_EDIT_EXPECTED_CURP",
                        ValidateFormat.ExpectedRFC(Model.BeneficiaryLastName, Model.BeneficiaryLastName2, Model.BeneficiaryName1,
                          Model.BeneficiaryBirthDate.Value).Substring(0, 10) + "------")
                  });
              }
              break;
            }
            default:
            {
              InvalidField _err = ValidateField(_field, HiddenFields, RequiredFields, Model);

              if (_err != null)
              {
                _retval.Add(_err);
              }
            }
            break;
          }
        }
      }
      return _retval;
    }
  }
}
