//------------------------------------------------------------------------------
// Copyright © 2007-2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: PlayerEditdetailsCashierModel.cs
// 
//   DESCRIPTION: Model for cashier MVC
//                
//                
//                
//                
//                
//                
// 
//        AUTHOR: Scott Adamson.
// 
// CREATION DATE: 02-DEC-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 02-DEC-2007 SJA    First release.
// 17-MAR-2017 ETP    WIGOS-110: Creditlines - Paybacks
// 06-JUL-2017 FJC    Bug: WIGOS-3429 Recepción: Al guardar una cuenta si modificar datos, aparece el popup de guardar cambios
// 04-OCT-2017 ETP    Fixed Bug Bug 29878:[WIGOS-5065][WIGOS-4646]: Exception when the user do any modification in the "Account data editor"
// 06-DEC-2017 RGR    Bug 31047:WIGOS-6629 [Ticket #10514] 03.007.0015 [v03.007] MKT: ERROR al realizar ALTA de cliente nuevo
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using WSI.Cashier.MVC.Interface;
using WSI.Cashier.MVC.Model.PlayerEditDetails.Common;
using WSI.Cashier.MVC.Model.PlayerEditDetails.Validator;
using WSI.Common;
using WSI.Common.Entities.General;
using WSI.Common.Entrances;
using WSI.Common.CreditLines;

namespace WSI.Cashier.MVC.Model.PlayerEditDetails
{
  public class PlayerEditDetailsCashierModel : PlayerEditDetailsBaseModel, IMVCModel
  {



    /// <summary>
    ///   get validator for model
    /// </summary>
    public override IMVCValidator Validator
    {
      get { return new PlayerEditDetailsCashierModelValidator(this); }
    }
    public string HolderCURP { get; set; }//
    public string HolderRFC { get; set; }//

    public bool HasBeneficiary { get; set; }

    public string BeneficiaryName { get; set; }

    public string BeneficiaryLastName { get; set; }

    public string BeneficiaryLastName2 { get; set; }

    public string BeneficiaryDocument { get; set; }

    public string BeneficiaryCURP { get; set; }

    public string BeneficiaryId3 { get; set; }

    public string BeneficiaryName1 { get; set; }

    public System.DateTime? BeneficiaryBirthDate { get; set; }

    //DLM 22-AUG-2005
    public System.DateTime? ExpirationDocDate { get; set; }

    public OccupationWRK BeneficiaryOccupation { get; set; }

    public Gender BeneficiaryGender { get; set; }

    public string HolderEmail2 { get; set; }

    public bool CardBlocked { get; set; }

    public string CardBlockDescription { get; set; }

    public AccountBlockReason CardBlockReason { get; set; }

    public System.DateTime? HolderWeddingDate { get; set; }

    public OccupationWRK HolderOccupation { get; set; }

    public List<OccupationWRK> Occupations { get; set; }


    public bool PointsNextLevelVisible { get; set; }

    public string PointsNextLevelName { get; set; }

    public string PointsToNextLevel { get; set; }

    public string PointsRequired { get; set; }

    public string PointsLevel { get; set; }

    public string PointsGenerated { get; set; }

    public string PointsDiscretionaries { get; set; }

    public string PointsToMaintain { get; set; }

    public string CurrentLevel { get; set; }

    public string CurrentPoints { get; set; }

    public string CurrentLevelExpiration { get; set; }

    public string CurrentLevelEntered { get; set; }
    public bool RFCVisible { get; set; }
    public bool CURPVisible { get; set; }

    public DocumentList BeneficiaryDocScan { get; set; }


    public bool HolderDocsChanged { get; set; }
    public bool BeneficiaryDocsChanged { get; set; }

    public CreditLine CreditLineData { get; set; }

    internal override string[] ModelFields
    {
      get
      {
        return new[]
          {
            "HolderOccupation", "HolderMaritalStatus", "HolderWeddingDate", "HolderEmail2", "HolderTwitterAccount",
            "HolderDocScan","HolderRFC", "HolderCURP"
          };
      }
    }

    public ExpiredDocumentsResult ExpiredDocumentsResult { get; set; }

    /// <summary>
    ///   get value form model by name, future: normalize names, use reflection
    /// </summary>
    /// <param name="PropertyName"></param>
    /// <returns></returns>
    public override object GetValue(string PropertyName)
    {
      object _o = GetValueBase(PropertyName);
      if (_o != null)
      {
        return _o;
      }
      switch (PropertyName)
      {
        case "BeneficiaryName1":
          return BeneficiaryLastName;
        case "BeneficiaryName2":
          return BeneficiaryLastName2;
        case "BeneficiaryName3":
          return BeneficiaryName;
        case "BeneficiaryBirthDate":
          return BeneficiaryBirthDate;
        case "ExpirationDocDate": 
          return ExpirationDocDate;
        case "BeneficiaryRFC":
          return BeneficiaryDocument;
        case "BeneficiaryDocument":
          return BeneficiaryDocument;
        case "BeneficiaryDocScan":
          return BeneficiaryDocScan;
        case "HolderDocScan":
          return HolderDocScan;
        case "BeneficiaryCURP":
          return BeneficiaryCURP;
        case "BeneficiaryGender":
          return BeneficiaryGender;
        case "BeneficiaryOccupation":
          return BeneficiaryOccupation;
        case "HolderOccupation":
          return HolderOccupation;
        case "HolderNationality":
          return HolderNationality;
        case "HolderMaritalStatus":
          return HolderMaritalStatus;
        case "HolderWeddingDate":
          return HolderWeddingDate;
        case "HolderEmail2":
          return HolderEmail2;
        case "HolderPhone2":
          return HolderPhone2;
        case "HolderRFC":
          return HolderRFC;
        case "HolderCURP":
          return HolderCURP;
        case "HolderDocumentType":
          return HolderDocumentType;
        case "HolderDocument3Type":
          return HolderDocument3Type;
        case "BeneficiaryDocument3Type":
          return BeneficiaryDocument3Type;
        default:
          return "";
      }
    }

    public System.ComponentModel.BindingList<ScannedDocument> ScannedDocuments { get; set; }

    public BlackListResult BlackListResult { get; set; }
    public DateTime? RecordExpirationDate { get; set; }
    public string Facebook { get; set; }
    public bool HasAceptedAgreement { get; set; }
    public bool SearchScannedDocuments { get; set; }
    public DateTime? CreationDate { get; set; }

    public DateTime AceptedAgreementDate { get; set; }

    public string HolderAlternativeAddressLine1 { get; set; }

    public string HolderAlternativeAddressLine2 { get; set; }

    public string HolderAlternativeAddressLine3 { get; set; }

    public string HolderAlternativeTown { get; set; }

    public State HolderAlternativeState { get; set; }

    public Country HolderAlternativeCountry { get; set; }

    public string HolderAlternativeHouseNumber { get; set; }

    public string HolderAlternativePostCode { get; set; }

    public bool ScannedDocumentsChanged { get; set; }
    private DocumentList CloneDocumentList(DocumentList Documents)
    {
      if (Documents == null)
      {
        return null;
      }
      DocumentList _retval = new DocumentList();
      foreach (IDocument _document in Documents)
      {
        IDocument _newdoc = new ReadOnlyDocument(_document.Name, _document.Content);
        _retval.Add(_newdoc);
      }
      return _retval;
    }

    /// <summary>
    ///   ancillary method for clone
    /// </summary>
    /// <param name="ScannedDocs"></param>
    /// <returns></returns>
    private BindingList<ScannedDocument> CloneScannedDocs(BindingList<ScannedDocument> ScannedDocs)
    {
      BindingList<ScannedDocument> _scanned_documents = new BindingList<ScannedDocument>();
      foreach (ScannedDocument _scanned_document in ScannedDocs)
      {
        ScannedDocument _sd = new ScannedDocument
        {
          DocumentTypeDescription = _scanned_document.DocumentTypeDescription,
          DocumentTypeId = _scanned_document.DocumentTypeId,
          ScannedDate = _scanned_document.ScannedDate,
          Expiration = _scanned_document.Expiration,
          Image = _scanned_document.Image
        };
        _scanned_documents.Add(_sd);
      }
      return _scanned_documents;
    }
    public PlayerEditDetailsCashierModel Clone()
    {
      PlayerEditDetailsCashierModel _retval = new PlayerEditDetailsCashierModel
      {
        CreationDate = CreationDate,
        HolderAlternativePostCode = HolderAlternativePostCode,
        HolderAlternativeTown = HolderAlternativeTown,
        HolderAlternativeState = HolderAlternativeState,
        HolderAlternativeHouseNumber = HolderAlternativeHouseNumber,
        HolderAlternativeAddressLine1 = HolderAlternativeAddressLine1,
        HolderAlternativeCountry = HolderAlternativeCountry,
        BlackListResult = BlackListResult,
        ExpiredDocumentsResult = ExpiredDocumentsResult,
        HolderAlternativeAddressLine2 = HolderAlternativeAddressLine2,
        HolderAlternativeAddressLine3 = HolderAlternativeAddressLine3,
        ScannedDocuments = CloneScannedDocs(ScannedDocuments),
        AccountId = AccountId,
        Comments = Comments,
        Countries = Countries,
        DocumentTypes = DocumentTypes,
        Genders = Genders,
        HolderAddressValidation = HolderAddressValidation,
        HolderExtNum = HolderExtNum,
        HolderAddressLine01 = HolderAddressLine01,
        HolderAddressLine02 = HolderAddressLine02,
        HolderAddressLine03 = HolderAddressLine03,
        HolderCity = HolderCity,
        HolderFedEntity = HolderFedEntity,
        HolderZip = HolderZip,
        HolderAddressCountry = HolderAddressCountry,
        HolderAddressType = HolderAddressType,
        HolderBirthCountry = HolderBirthCountry,
        HolderBirthDate = HolderBirthDate,
        HolderDocument = HolderDocument,
        HolderDocument3Type = HolderDocument3Type,
        HolderDocumentType = HolderDocumentType,
        HolderEmail1 = HolderEmail1,
        HolderMaritalStatus = HolderMaritalStatus,
        HolderFullName = HolderFullName,
        HolderGender = HolderGender,
        HolderPhone1 = HolderPhone1,
        HolderName1 = HolderName1,
        HolderName2 = HolderName2,
        HolderName3 = HolderName3,
        HolderName4 = HolderName4,
        HolderDocScan = CloneDocumentList(HolderDocScan),
        HolderPhone2 = HolderPhone2,
        HolderNationality = HolderNationality,
        InvalidFields = InvalidFields,
        MustShowComments = MustShowComments,
        Nationalities = Nationalities,
        HolderPhoto = HolderPhoto,
        HasPhoto = HasPhoto,
        Pin = Pin,
        PinFailures = PinFailures,
        ShowComments = ShowComments,
        Facebook = Facebook,
        CardLevel = CardLevel,
        HasAceptedAgreement = HasAceptedAgreement,
        AceptedAgreementDate = AceptedAgreementDate,
        SearchScannedDocuments = SearchScannedDocuments,
        HolderIsVip = HolderIsVip,
        CardLevelName = CardLevelName,
        HolderTwitterAccount = HolderTwitterAccount,
        CardCreationDate = CardCreationDate,
     IDDocuments = CloneIDDocuemnts(IDDocuments)
        };
      return _retval;
    }

    private Dictionary<int, string> CloneIDDocuemnts(Dictionary<int, string> IDDocuments)
    {
      Dictionary<int,string> _retval = new Dictionary<int, string>();
      foreach (var _id_document in IDDocuments)
      {
        _retval.Add(_id_document.Key, _id_document.Value);
      }
      return _retval;
    }


  }
}
