//------------------------------------------------------------------------------
// Copyright © 2007-2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: PlayerEditDetailsReceptionModel.cs
// 
//   DESCRIPTION: Model for reception MVC
//                
//                
//                
//                
//                
//                
// 
//        AUTHOR: Scott Adamson.
// 
// CREATION DATE: 02-DEC-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 02-DEC-2007 SJA    First release.
// 11-FEB-2015 AVZ    Product Backlog Item 9046:Visitas / Recepción: BlackLists
// 06-JUL-2017 FJC    Bug: WIGOS-3429 Recepción: Al guardar una cuenta si modificar datos, aparece el popup de guardar cambios
//------------------------------------------------------------------------------


using System;
using System.Collections.Generic;
using System.ComponentModel;
using WSI.Cashier.MVC.Interface;
using WSI.Cashier.MVC.Model.PlayerEditDetails.Common;
using WSI.Cashier.MVC.Model.PlayerEditDetails.Validator;
using WSI.Common;
using WSI.Common.Entities.General;
using WSI.Common.Entrances;
using WSI.Common.Entrances.Enums;

namespace WSI.Cashier.MVC.Model.PlayerEditDetails
{
  public class PlayerEditDetailsReceptionModel : PlayerEditDetailsBaseModel
  {
    public PlayerEditDetailsReceptionModel()
    {
      BlackListResultCustomer = new BlackListResultCustomer[0];
    }
    /// <summary>
    ///   get validator
    /// </summary>
    public override IMVCValidator Validator
    {
      get { return new PlayerEditDetailsReceptionModelValidator(this); }
    }

    public bool NoMoreResults { get; set; }
    public DateTime? CreationDate { get; set; }

    internal override string[] ModelFields
    {
      get { return new string[] {}; }
    }

    /// <summary>
    ///   return a clone of the current model
    /// </summary>
    /// <returns></returns>
    public PlayerEditDetailsReceptionModel Clone()
    {
      PlayerEditDetailsReceptionModel _retval = new PlayerEditDetailsReceptionModel
        {
          CreationDate = CreationDate,
          HolderAlternativePostCode = HolderAlternativePostCode,
          HolderAlternativeTown = HolderAlternativeTown,
          HolderAlternativeState = HolderAlternativeState,
          VisibleTrackData = VisibleTrackData,
          HolderAlternativeHouseNumber = HolderAlternativeHouseNumber,
          HolderAlternativeAddressLine1 = HolderAlternativeAddressLine1,
          HolderAlternativeCountry = HolderAlternativeCountry,
          BlackListResult = BlackListResult,
          BlackListResultCustomer = BlackListResultCustomer,
          GridData = GridData,
          ExpiredDocumentsResult = ExpiredDocumentsResult,
          HolderAlternativeAddressLine2 = HolderAlternativeAddressLine2,
          HolderAlternativeAddressLine3 = HolderAlternativeAddressLine3,
          HolderAlternativeAddressType = HolderAlternativeAddressType,
          NoMoreResults = NoMoreResults,
          ScannedDocuments = CloneScannedDocs(ScannedDocuments),
          SearchStringSQL = SearchStringSQL,
          AccountId = AccountId,
          Comments = Comments,
          Countries = Countries,
          DocumentTypes = DocumentTypes,
          Genders = Genders,
          HolderAddressValidation = HolderAddressValidation,
          HolderExtNum = HolderExtNum,
          HolderAddressLine01 = HolderAddressLine01,
          HolderAddressLine02 = HolderAddressLine02,
          HolderAddressLine03 = HolderAddressLine03,
          HolderCity = HolderCity,
          HolderFedEntity = HolderFedEntity,
          HolderZip = HolderZip,
          HolderAddressCountry = HolderAddressCountry,
          HolderAddressType = HolderAddressType,
          HolderBirthCountry = HolderBirthCountry,
          HolderBirthDate = HolderBirthDate,
          HolderDocument = HolderDocument,
          HolderDocument3Type = HolderDocument3Type,
          HolderDocumentType = HolderDocumentType,
          HolderEmail1 = HolderEmail1,
          HolderMaritalStatus = HolderMaritalStatus,
          HolderFullName = HolderFullName,
          HolderGender = HolderGender,
          HolderPhone1 = HolderPhone1,
          HolderName1 = HolderName1,
          HolderName2 = HolderName2,
          HolderName3 = HolderName3,
          HolderName4 = HolderName4,
          HolderDocScan = CloneDocumentList(HolderDocScan),
          HolderPhone2 = HolderPhone2,
          HolderNationality = HolderNationality,
          InvalidFields = InvalidFields,
          MustShowComments = MustShowComments,
          Nationalities = Nationalities,
          HolderPhoto = HolderPhoto,
          HasPhoto = HasPhoto,
          Pin = Pin,
          PinFailures = PinFailures,
          ShowComments = ShowComments,
          Facebook = Facebook,
          CardLevel = CardLevel,
          HasAceptedAgreement = HasAceptedAgreement,
          AceptedAgreementDate = AceptedAgreementDate,
          SearchScannedDocuments = SearchScannedDocuments,
          HolderIsVip = HolderIsVip,
          CardLevelName = CardLevelName,
          HolderTwitterAccount = HolderTwitterAccount,
          CardCreationDate = CardCreationDate,
          IDDocuments = CloneIDDocuemnts(IDDocuments)
        };
      return _retval;
    }

    private Dictionary<int, string> CloneIDDocuemnts(Dictionary<int, string> IDDocuments)
    {
      Dictionary<int,string> _retval = new Dictionary<int, string>();
      foreach (var _id_document in IDDocuments)
      {
        _retval.Add(_id_document.Key, _id_document.Value);
      }
      return _retval;
    }

    /// <summary>
    ///   ancillary method for clone
    /// </summary>
    /// <param name="Documents"></param>
    /// <returns></returns>
    private DocumentList CloneDocumentList(DocumentList Documents)
    {
      if (Documents == null)
      {
        return null;
      }
      DocumentList _retval = new DocumentList();
      foreach (IDocument _document in Documents)
      {
        IDocument _newdoc = new ReadOnlyDocument(_document.Name, _document.Content);
        _retval.Add(_newdoc);
      }
      return _retval;
    }

    /// <summary>
    ///   ancillary method for clone
    /// </summary>
    /// <param name="ScannedDocs"></param>
    /// <returns></returns>
    private BindingList<ScannedDocument> CloneScannedDocs(BindingList<ScannedDocument> ScannedDocs)
    {
      BindingList<ScannedDocument> _scanned_documents = new BindingList<ScannedDocument>();
      foreach (ScannedDocument _scanned_document in ScannedDocs)
      {
        ScannedDocument _sd = new ScannedDocument
          {
            DocumentTypeDescription = _scanned_document.DocumentTypeDescription,
            DocumentTypeId = _scanned_document.DocumentTypeId,
            ScannedDate = _scanned_document.ScannedDate,
            Expiration = _scanned_document.Expiration,
            Image = _scanned_document.Image
          };
        _scanned_documents.Add(_sd);
      }
      return _scanned_documents;
    }

    /// <summary>
    ///   get value for property name from model by name : future: normalize names and use reflection
    /// </summary>
    /// <param name="PropertyName"></param>
    /// <returns></returns>
    public override object GetValue(string PropertyName)
    {
      object _o = GetValueBase(PropertyName);
      if (_o != null)
      {
        return _o;
      }
      switch (PropertyName)
      {
        default:
          return "";
      }
    }
    #region Public properties

    public string VisibleTrackData { get; set; }
    public string TrackData { get; set; }
    public int HolderAlternativeAddressType { get; set; }
    public string HolderAlternativeAddressLine1 { get; set; } // Street
    public string HolderAlternativeAddressLine2 { get; set; } //Colonny
    public string HolderAlternativeAddressLine3 { get; set; } //Delegation
    public string HolderAlternativeHouseNumber { get; set; }
    public string HolderAlternativeTown { get; set; } //
    public State HolderAlternativeState { get; set; }
    public Country HolderAlternativeCountry { get; set; } //
    public string HolderAlternativePostCode { get; set; } //
    public string Facebook { get; set; } //
    public DateTime? RecordExpirationDate { get; set; } //
    public ExpiredDocumentsResult ExpiredDocumentsResult { get; set; } //
    public bool HasAceptedAgreement { get; set; } //
    public DateTime AceptedAgreementDate { get; set; } //
    public BlackListResult BlackListResult { get; set; } //
    public BlackListResultCustomer[] BlackListResultCustomer { get; set; } // 
    public BindingList<CustomerSearchResult> GridData { get; set; }
    public BindingList<ScannedDocument> ScannedDocuments { get; set; }
    public bool SearchScannedDocuments { get; set; }
    public string SearchStringSQL { get; set; }

    public bool ScannedDocumentsChanged { get; set; }
		public bool AcknowledgedBlackList { get; set; }
	  public bool BlackListEntryBlocked { get; set; }
		public string[] BlackListMessages { get; set; }
		public string[] BlackListLists { get; set; }

    #endregion


    public BlackListResultCustomer[] BlackListCustomerSelected { get; set; }
  }
}
