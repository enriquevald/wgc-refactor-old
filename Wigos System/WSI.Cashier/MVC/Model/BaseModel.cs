using System;
using WSI.Cashier.MVC.Interface;

namespace WSI.Cashier.MVC.Model
{
  public abstract class BaseModel : IMVCModel, IDisposable
  {
    public string[] Fields
    {
      get
      {
        var _return_fields = new string[FieldsBase.Length + ModelFields.Length];
        FieldsBase.CopyTo(_return_fields, 0);
        ModelFields.CopyTo(_return_fields, FieldsBase.Length);

        return _return_fields;
      }
    }
    internal abstract string[] ModelFields { get; }//ModelFields
    protected abstract string[] FieldsBase { get; }
    internal abstract object GetValueBase(string PropertyName);
    public abstract object GetValue(string PropertyName);
    public abstract IMVCValidator Validator { get; }//Validator


    public void Dispose()
    {
      //cleanup any hard objects
    }
  }
}