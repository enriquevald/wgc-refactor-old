﻿using System;
using System.Collections.Generic;
using System.Text;
using WSI.Cashier.MVC.Interface;
using WSI.Cashier.MVC.Model.TestMVC.Validator;

namespace WSI.Cashier.MVC.Model.TestMVC
{
  public class TestMVCModel : BaseModel
  {

    public string Name { get; set; }

    internal override string[] ModelFields
    {
      get { return FieldsBase; }
    }

    protected override string[] FieldsBase
    {
      get { return new[] {"Name"}; }
    }

    internal override object GetValueBase(string PropertyName)
    {
      if (PropertyName == "Name")
      {
        return Name;
      }
      return null;
    }

    public override object GetValue(string PropertyName)
    {
      return GetValueBase(PropertyName);
    }

    public override IMVCValidator Validator
    {
      get { return new TestMVCModelValidator(this); }
    }
  }
}
