using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using WSI.Cashier.MVC.Interface;
using WSI.Common.Entities.General;
using WSI.Common.Utilities.Extensions;

namespace WSI.Cashier.MVC.Model
{
  public abstract class BaseModelValidator<T> : IMVCValidator where T : IMVCModel
  {
    protected bool IsValidEmail(string StrIn)
    {
      // Return true if strIn is in valid e-mail format.
      return Regex.IsMatch(StrIn,
        @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
    }
    protected bool IsValidTwitter(string StrIn)
    {
      if (string.IsNullOrEmpty(StrIn) || StrIn.ToUpperInvariant().Contains("ADMIN") ||
          StrIn.ToUpperInvariant().Contains("TWITTER"))
        return false;

      // Return true if strIn is in valid twitter format.
      return Regex.IsMatch(StrIn,
        @"^@?(\w){1,15}$");
    }

    public BaseModelValidator(T Mdl)
    {
      Model = Mdl;
    }
    public abstract List<InvalidField> Validate(List<string> HiddenFields, List<string> RequiredFields);

    public virtual List<InvalidField> ValidateBase(List<string> HiddenFields, List<string> RequiredFields)
    {
      List<InvalidField> _errors = new List<InvalidField>();

      foreach (string _field in Model.Fields)
      {
        InvalidField _res;


        _res = ValidateField(_field, HiddenFields, RequiredFields, Model);
        if (_res != null)
        {
          _errors.Add(_res);
        }
      }
      return _errors;
    }

    /// <summary>
    ///   Routine that checks if a field is visible and required validation
    /// </summary>
    /// <param name="PropertyName"></param>
    /// <param name="HiddenFields"></param>
    /// <param name="RequiredFields"></param>
    /// <returns></returns>
    protected bool NeedsValidating(string PropertyName, List<string> HiddenFields, List<string> RequiredFields)
    {
      if (HiddenFields.Contains(PropertyName))
      {
        return false;
      }
      return !IsEmpty(Model.GetValue(PropertyName)) || (RequiredFields != null && RequiredFields.Contains(PropertyName));
    }

    protected T Model { get; set; }

    internal abstract InvalidField ValidateField(string PropertyName, List<string> HiddenFields,
                                                 List<string> RequiredFields, T Mdl);

    public abstract string GetResourceString(string PropertyName);
    protected abstract string GetReason(object Object, string FieldName = "");

    protected virtual bool IsEmpty(object Value)
    {
      if (Value == null)
        return true;

      switch (Value.GetType().Name)
      {
        case "String":
          return ((string) Value).IsNullOrWhiteSpace();
        case "DateTime":
          return ((DateTime) Value) == DateTime.MinValue;
        case "Bitmap":
          return ((Bitmap) Value).ToByteArray().Length == 0;
        default:
          return false;
      }
    }
  }
}