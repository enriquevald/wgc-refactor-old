﻿using System;
using System.Collections.Generic;
using System.Text;
using WSI.Common.Entities.General;

namespace WSI.Cashier.MVC.Model.BlackListMatches.Validator
{
  public class BlackListMatchesModelValidator: BaseModelValidator<BlackListMatchesModel>
  {

    public BlackListMatchesModelValidator(BlackListMatchesModel Mdl)
      : base(Mdl)
    {

    }
      
    public override List<InvalidField> Validate(List<string> HiddenFields, List<string> RequiredFields)
    {
      if (IsEmpty(Model.GetValue("Name")))
      {
        return new List<InvalidField>(){new InvalidField(){Id="Name", Reason=@"Name is empty"}};
      }
      return new List<InvalidField>();
    }

    internal override InvalidField ValidateField(string PropertyName, List<string> HiddenFields, List<string> RequiredFields, BlackListMatchesModel Mdl)
    {
      return null;
    }

    public override string GetResourceString(string PropertyName)
    {
      return PropertyName;
    }

    protected override string GetReason(object Object, string FieldName = "")
    {
      return "field " + GetResourceString(FieldName) + " is garbage";
    }
    
  }
}
