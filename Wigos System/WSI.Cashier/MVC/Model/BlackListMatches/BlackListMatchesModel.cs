﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Windows.Forms;
using WSI.Cashier.MVC.Interface;
using WSI.Cashier.MVC.Model.BlackListMatches.Validator;
using WSI.Common.Entities.General;
using WSI.Common.Entrances;

namespace WSI.Cashier.MVC.Model.BlackListMatches
{
  public class BlackListMatchesModel : BaseModel
  {
    public List<BlackListResultCustomer> CustomerSelected { get; set; }

    public BindingList<BlackListResultCustomer> GridData { get; set; }

    public bool NoMatches { get; set; }

    public bool Validated { get; set; }

    internal override string[] ModelFields
    {
      get { return FieldsBase; }
    }

    protected override string[] FieldsBase
    {
      get { return new[] { "CustomersGrid" }; }
    }

    internal override object GetValueBase(string PropertyName)
    {
     
      return null;
    }

    public override object GetValue(string PropertyName)
    {
      return GetValueBase(PropertyName);
    }

    public override IMVCValidator Validator
    {
      get { return new BlackListMatchesModelValidator(this); }
    }



    public BlackListResultCustomer ForcedItem { get; set; }
  }
}
