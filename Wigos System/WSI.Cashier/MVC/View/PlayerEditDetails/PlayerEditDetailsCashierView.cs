﻿//------------------------------------------------------------------------------
// Copyright © 2007-2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: PlayerEditDetailsCashierView.cs
// 
//   DESCRIPTION: Cashier view for MVC player edit details
//                
//                
//                
//                
//                
//                
// 
//        AUTHOR: Scott Adamson.
// 
// CREATION DATE: 02-DEC-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 02-DEC-2007 SJA    First release.
// 09-MAR-2016 FOS    BUG 10432: block/unblock account errors
// 06-APR-2016 SJA    Bug 11486: Cajero: no funciona el RFC ni el beneficiario
// 06-APR-2016 SJA    BUG 11496: fix behaviour of gp antilaundry has beneficiary Resolve
// 30-MAY-2016 JCA    Fixed Bug 13692:Recepción: error al crear un Nuevo Cliente con la caja cerrada
// 20-JUN-2016 FAV    Fixed Bug 14685: Change position LastName and Name by general param
// 28-NOV-2016 FJC    Fixed Bug 21043:Cashier: Error al crear cuenta y guardar datos de contacto
// 02-DEC-2016 ATB    Fixed Bug 21243:Televisa: At new account creation, the Wigos's country is not selected by default.
// 05-DEC-2016 ATB    Fixed Bug 21261:Televisa: At new account creation, the Wigos's country is not selected by default.
// 12-DEC-2016 ATB    Fixed Bug 21373: Cashier: Country's Federal State is not loading the right info at card register's form
// 12-ENE-2017 DPC    Bug 22421:Auditoría: mensajes erróneos al imprimir datos del cliente
// 12-ENE-2017 DPC    Bug 22657:Cashier: Tipo de documento, contador doble
// 19-JAN-2017 FOS    Fixed Bug 22330: Print ticket 2 times, when modify the user data.
// 17-MAR-2017 ETP    WIGOS-110: Creditlines - Paybacks
// 28-AUG-2017 AMF    Bug 29472:[WIGOS-3868] Photo is always enabled even it is disabled in Player data screen
// 03-OCT-2017 DHA    PBI 30017:WIGOS-4056 Payment threshold registration - Player registration
// 04-OCT-2017 ETP    Fixed Bug Bug 29878:[WIGOS-5065][WIGOS-4646]: Exception when the user do any modification in the "Account data editor"
// 29-MAR-2018 LTC    Bug 32101: WIGOS-8442 Account "Nationality" field appear empty in "Reception" module and populated with value in "Accounts" Cashier section 
// 18-JUN-2018 FJC    Fixed Bug: WIGOS-12957 Cashier - New accounts default document type shows "Other" document instead of the one configurated by default
// 27-JUN-2018 DLM    Bug 33349:WIGOS -13084 : Accounts - Country list should be displayed in order even there is a country selected.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using WSI.Cashier.MVC.Controller.PlayerEditDetails;
using WSI.Cashier.MVC.Model.PlayerEditDetails;
using WSI.Cashier.MVC.View.PlayerEditDetails.Common;
using WSI.Common;
using WSI.Common.CreditLines;
using WSI.Common.Entities.General;

namespace WSI.Cashier.MVC.View.PlayerEditDetails
{
  public partial class PlayerEditDetailsCashierView : VwBasePlayerEditDetailsCashierModel
  {
    public enum SCREEN_MODE
    {
      DEFAULT = 0,
      REGISTER_CODERE = 1
    }
    
    List<InvalidField> tmp_errors;
    
    //private read only PlayerEditAddressCommon<PlayerEditDetailsCashierModel> m_address_common;
    private PlayerEditDetailsCommonErrorDisplay<PlayerEditDetailsCashierModel> m_error_display;
    private Dictionary<string, object> m_beneficiary_data;
    //private PlayerEditDetailsCashierController m_controller;
    private CASHIER_STATUS m_cashier_status;
    private Dictionary<int, string> m_documents = new Dictionary<int, string>();

    private void UpdateDocTypeDescriptions()
    {
      int _docvalues = 0;

      if (cb_document_type.Items != null)
      foreach (var _doctype in cb_document_type.Items)
        {
          _docvalues = ProcessDocValues(_doctype, _docvalues);
        }

      UpdateDocumentTypesLabel(_docvalues);
    }

    private void UpdateDocumentTypesLabel(int Docvalues)
    {
      int _i = lbl_type_document.Text.IndexOf(" (", StringComparison.Ordinal);
      if (_i < 1)
        _i = lbl_type_document.Text.Length;
      lbl_type_document.Text = lbl_type_document.Text.Substring(0, _i);

      if (Docvalues > 1)
      {
        lbl_type_document.Text += string.Format(" ({0})", Docvalues);
    }

      lbl_docuemnt_counter.Text = Docvalues > 0 ? String.Format("x{0}", Docvalues.ToString()) : String.Empty;
    }

    private int ProcessDocValues(object Doctype, int Docvalues)
    {
        var _doc_type = (Doctype as DocumentType);
      if (_doc_type != null && _doc_type.Name.Length > 0)
      {
        if (!m_documents.ContainsKey(_doc_type.Id))
        {
          m_documents.Add(_doc_type.Id, "");
        }
        if (_doc_type.Name.Contains(" ("))
          _doc_type.Name = _doc_type.Name.Substring(0, _doc_type.Name.IndexOf(" (", StringComparison.Ordinal));
        if (cb_document_type.SelectedItem != null)
        {
          if (cb_document_type.SelectedItem != _doc_type)
          {
            if (m_documents.ContainsKey(_doc_type.Id) && (m_documents[_doc_type.Id] ?? "").Length > 0)
            {
              _doc_type.Name = _doc_type.Name.Trim() + " (" + (m_documents[_doc_type.Id] ?? "") + ")";
              Docvalues++; 
            }
          }
          else
          {
            cb_document_type.Text = _doc_type.Name;
            txt_document.Text = m_documents[_doc_type.Id];
            if (!string.IsNullOrEmpty(m_documents[_doc_type.Id]))
            {
              Docvalues++;
            }
          }
        }
      }
      else
      {
        txt_document.Text = "";
      }
      return Docvalues;
    }

    /// <summary>
    ///   constructor
    /// </summary>
    public PlayerEditDetailsCashierView()
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[UC LOAD] PlayerEditDetailsCashierView", Log.Type.Message);

      m_cashier_status = CashierBusinessLogic.ReadCashierStatus(Cashier.SessionId);

      AddressModified = new Zip.Address();
      InitializeComponent();
      
      //this.Size = new System.Drawing.Size(1024, 640); //DEL

      uc_dt_ben_birth.Init();
      uc_dt_birth.Init();
      //m_address_common = new PlayerEditAddressCommon<PlayerEditDetailsCashierModel>(this);
      m_error_display = new PlayerEditDetailsCommonErrorDisplay<PlayerEditDetailsCashierModel>(this, lbl_msg_count_blink,
        lbl_msg_blink);

      txt_name4.TextChanged += txt_name_TextChanged;
      txt_name.TextChanged += txt_name_TextChanged;
      txt_last_name1.TextChanged += txt_name_TextChanged;
      txt_last_name2.TextChanged += txt_name_TextChanged;
      SetLabelTexts();
      SetNameAndLastNamePostion();
    } //PlayerEditDetailsCashierView

    #region IPlayerEditDetailsView<PlayerEditDetailsCashierModel> Members

    /// <summary>
    ///   publicly exposed model, on get and set values are painted and retrieved from the display
    /// </summary>
    public override void DoFirstSet()
    {
      //load combo boxes from model

      PopulateComboBoxes();

      if (m_beneficiary_data == null)
      {
        m_beneficiary_data = new Dictionary<string, object>();
      }

      m_beneficiary_data.Add("txt_beneficiary_last_name1", m_model.BeneficiaryName1);
      m_beneficiary_data.Add("txt_beneficiary_last_name2", m_model.BeneficiaryLastName);
      m_beneficiary_data.Add("txt_beneficiary_name", m_model.BeneficiaryLastName2);
      m_beneficiary_data.Add("txt_beneficiary_RFC", m_model.BeneficiaryDocument);
      m_beneficiary_data.Add("txt_beneficiary_CURP", m_model.BeneficiaryCURP);
      m_beneficiary_data.Add("cb_beneficiary_occupation", m_model.BeneficiaryOccupation);

      m_beneficiary_data.Add("uc_dt_ben_birth", m_model.BeneficiaryBirthDate);

      m_beneficiary_data.Add("cb_beneficiary_gender", m_model.BeneficiaryGender);
      m_beneficiary_data.Add("BeneficiaryId3Type", m_model.BeneficiaryId3);
      m_beneficiary_data.Add("benscandocs", m_model.BeneficiaryDocScan);
    }

    //Model

    /// <summary>
    ///   method to return a list of controls linked to their labels so
    ///   they can be hidden as needed in the controller
    /// </summary>
    public override VisibleData VisibleFields
    {
      get
      {
        {
          VisibleData _visible_data;
          _visible_data = new VisibleData();
          _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.PHOTO, new Control[] { m_img_photo });
         
          _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.NAME3, new Control[] { lbl_name, txt_name });
          _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.NAME4, new Control[] { lbl_name4, txt_name4 });
          _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.NAME1, new Control[] { lbl_last_name1, txt_last_name1 });
          _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.NAME2, new Control[] { lbl_last_name2, txt_last_name2 });
          _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.ID1, new Control[] { btn_RFC, txt_RFC });
          _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.ID2, new Control[] { lbl_CURP, txt_CURP });
          _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.DOC_SCAN, new Control[] { btn_scan_docs });
          _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.GENDER, new Control[] { lbl_gender, cb_gender });
          _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.NATIONALITY, new Control[] { lbl_nationality, cb_nationality });
          _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.BIRTH_COUNTRY, new Control[] { lbl_birth_country, cb_birth_country });
          _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.OCCUPATION, new Control[] { lbl_occupation, cb_occupation });
          _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.MARITAL_STATUS, new Control[] { lbl_marital_status, cb_marital });
          _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.WEDDING_DATE, new Control[] { lbl_wedding_date, uc_dt_wedding });

          _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.PHONE1, new Control[] { lbl_phone_01, txt_phone_01 });
          _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.PHONE2, new Control[] { lbl_phone_02, txt_phone_02 });
          _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.EMAIL1, new Control[] { lbl_email_01, txt_email_01 });
          _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.EMAIL2, new Control[] { lbl_email_02, txt_email_02 });
          _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.TWITTER, new Control[] { lbl_twitter, txt_twitter });

          _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.DOC_TYPE, new Control[] { lbl_type_document, cb_document_type });
          _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.DOCUMENT, new Control[] { lbl_document, txt_document });
          _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.BIRTHDATE, new Control[] { lbl_birth_date, uc_dt_wedding });

          /*_visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.BIRTHDATE, new Control[] { lbl_birth_date, uc_dt_wedding });
          _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.BIRTHDATE, new Control[] { lbl_birth_date, uc_dt_wedding });
          _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.BIRTHDATE, new Control[] { lbl_birth_date, uc_dt_wedding });
          _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.BIRTHDATE, new Control[] { lbl_birth_date, uc_dt_wedding });*/

          _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.EXPIRATION_DOC_DATE, new Control[] { lbl_expiration_doc_date, uc_dt_expiration_doc });

          return _visible_data;
        }
      }
    } //VisibleFields

    /// <summary>
    ///   method to update status display for pin in case of error
    /// </summary>
    public override void ErrorUpdatingPin()
    {
      lbl_pin_msg.Visible = true;
      lbl_pin_msg.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_ERROR_UPDATING_PIN");
      lbl_pin_msg.ForeColor = Color.Red;
    } //ErrorUpdatingPin

    /// <summary>
    ///   method to update status display for pin if generation succeeded
    /// </summary>
    public override void RandomPinGeneratedOk()
    {
      lbl_pin_msg.Visible = true;
      lbl_pin_msg.ForeColor = Color.Green;
      lbl_pin_msg.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_PIN_GENERATED_CORRECTLY");
      txt_pin.Enabled = false;
      txt_confirm_pin.Enabled = false;
      btn_save_pin.Enabled = false;
      btn_generate_random_pin.Enabled = false;
    } //RandomPinGeneratedOk

    /// <summary>
    ///   method to update status display is saving of new pin succeeded
    /// </summary>
    public override void PinChangedOk()
    {
      lbl_pin_msg.Visible = true;
      lbl_pin_msg.ForeColor = Color.Green;
      lbl_pin_msg.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_PIN_CHANGED_CORRECTLY");
      txt_pin.Enabled = false;
      txt_confirm_pin.Enabled = false;
      btn_save_pin.Enabled = false;
      btn_generate_random_pin.Enabled = false;
    } //PinChangedOk

    #endregion

    /// <summary>
    ///   method to bind an populate combo boxes
    /// </summary>
    private void PopulateComboBoxes()
    {
      PlayerEditDetailsCashierController.BindDatasourceToCombo(m_model.Nationalities, cb_nationality);
      PlayerEditDetailsCashierController.BindDatasourceToCombo(m_model.DocumentTypes, cb_document_type);
      PlayerEditDetailsCashierController.BindDatasourceToCombo(m_model.Genders, cb_gender);
      PlayerEditDetailsCashierController.BindDatasourceToCombo(m_model.Genders, cb_beneficiary_gender);
      PlayerEditDetailsCashierController.BindDatasourceToCombo(m_model.Occupations, cb_beneficiary_occupation);
      PlayerEditDetailsCashierController.BindDatasourceToCombo(m_model.Occupations, cb_occupation);
      PlayerEditDetailsCashierController.BindDatasourceToCombo(m_model.MaritalStatuses, cb_marital);
      PlayerEditDetailsCashierController.BindDatasourceToCombo(m_model.Countries, cb_birth_country);
    } //PopulateComboBoxes

    // ATB 02-DEC-2016
    /// <summary>
    /// Selects a default country from the General Params
    /// </summary>
    /// <param name="GroupKey"></param>
    /// <param name="SubjectKey"></param>
    private void SelectDefaultCountryFromGeneralParam(String GroupKey, String SubjectKey)
    {
      if (!String.IsNullOrEmpty(GeneralParam.GetString(GroupKey, SubjectKey)))
      {
        // We always get the first (and expected) unique result
        System.Data.DataRow _country_to_select = CardData.GetCountriesList().Select("CO_ISO2 = '" + GeneralParam.GetString(GroupKey, SubjectKey) + "'")[0];
        String _country_name = _country_to_select.ItemArray[1].ToString();

        int _counter = 0;
        foreach (WSI.Common.Entities.General.Nationality option in (System.Collections.Generic.List<WSI.Common.Entities.General.Nationality>)cb_nationality.DataSource)
        {
          // ATB 12-DEC-2016
          if (option.Name == _country_name)
          {
            this.cb_nationality.SelectedIndex = _counter;
            this.cb_nationality.SelectedItem = _country_name;      

            this.cb_birth_country.SelectedIndex = _counter;
            this.cb_birth_country.SelectedItem = _country_name;

            // In HolderAddress it's mandatory to set the country id, so the uc_address.cb_country_SelectedIndexChanged
            // will be triggered and the combo will load successfully
            /*HolderAddress.Country.Id = (Int32)this.cb_birth_country.SelectedValue;
            HolderAddress.Country.Name = _country_name;*/

            m_model.HolderAddressCountry.Id = (Int32)this.cb_birth_country.SelectedValue;
            m_model.HolderAddressCountry.Name = _country_name;
            HolderAddress.Country = m_model.HolderAddressCountry; //= HolderAddress.Country as Country;

            HolderAddress.Country.Id = (Int32)this.cb_birth_country.SelectedValue;
            HolderAddress.Country.Name = _country_name;


          }
          _counter++;
        }
      }
    }

    // ATB 02-DEC-2016
    /// <summary>
    /// Selects a default document type from a General Param
    /// </summary>
    /// <param name="GroupKey"></param>
    /// <param name="SubjectKey"></param>
    private void SelectDefaultDocumentTypeFromGeneralParam(String GroupKey, String SubjectKey)
    {
      int _default_doc; 
      DocumentType _selected_document_type; 

      _default_doc = GeneralParam.GetInt32(GroupKey, SubjectKey);
      _selected_document_type = null;
      // Select the default document type.
      foreach (DocumentType _doc_type in cb_document_type.Items)
      {
        if (_doc_type.Id == _default_doc)
          _selected_document_type = _doc_type;
      }

      if (_selected_document_type != null)
      {
        cb_document_type.SelectedItem = _selected_document_type;
        return;
      }
      // If not selected, select the first one.
      cb_document_type.SelectedIndex = 0;
    }
    /// <summary>
    ///   set title to controller generated message
    /// </summary>
    private void SetFormTitle()
    {
      if (m_controller == null)
      {
        return;
      }
      FormTitle = ((PlayerEditDetailsCashierController)m_controller).SetFormTitle(m_model);
    } //SetFormTitle

    /// <summary>
    /// Change position of name and last name by general param
    /// </summary>
    private void SetNameAndLastNamePostion()
    {
      if (GeneralParam.GetBoolean("Account", "InverseNameOrder", false))
      {
        TableLayoutPanelCellPosition _last_name2_txt_pos = tlp_name.GetPositionFromControl(txt_last_name2);
        TableLayoutPanelCellPosition _last_name2_lbl_pos = tlp_name.GetPositionFromControl(lbl_last_name2);
        TableLayoutPanelCellPosition _last_name1_txt_pos = tlp_name.GetPositionFromControl(txt_last_name1);
        TableLayoutPanelCellPosition _last_name1_lbl_pos = tlp_name.GetPositionFromControl(lbl_last_name1);
        TableLayoutPanelCellPosition _name2_txt_pos = tlp_name.GetPositionFromControl(txt_name4);
        TableLayoutPanelCellPosition _name2_lbl_pos = tlp_name.GetPositionFromControl(lbl_name4);
        TableLayoutPanelCellPosition _name_txt_pos = tlp_name.GetPositionFromControl(txt_name);
        TableLayoutPanelCellPosition _name_lbl_pos = tlp_name.GetPositionFromControl(lbl_name);

        tlp_name.SetCellPosition(lbl_name, _last_name1_lbl_pos);
        tlp_name.SetCellPosition(txt_name, _last_name1_txt_pos);
        tlp_name.SetCellPosition(lbl_name4, _last_name2_lbl_pos);
        tlp_name.SetCellPosition(txt_name4, _last_name2_txt_pos);
        tlp_name.SetCellPosition(lbl_last_name1, _name_lbl_pos);
        tlp_name.SetCellPosition(txt_last_name1, _name_txt_pos);
        tlp_name.SetCellPosition(lbl_last_name2, _name2_lbl_pos);
        tlp_name.SetCellPosition(txt_last_name2, _name2_txt_pos);

        int _tab_index;

        _tab_index = txt_name.TabIndex;
        txt_name.TabIndex = txt_last_name1.TabIndex;
        txt_last_name1.TabIndex = _tab_index;

        _tab_index = txt_name4.TabIndex;
        txt_name4.TabIndex = txt_last_name2.TabIndex;
        txt_last_name2.TabIndex = _tab_index;


      }
    }

   

    /// <summary>
    /// method to validate if document has expired. If the document has expired add error to the list of form errors but not is mandatory.
    /// </summary>
    /// <param name="_errors"></param>
    /// <param name="displayError"></param>
    private void ValidateDocumentExpiration() 
    {

      if (lbl_expiration_doc_date.Visible)
      {
        if (uc_dt_expiration_doc.DateValue == DateTime.MinValue)
        {
          return;
        }

        lbl_expiration_doc_date.ForeColor = Color.Black;
        DateTime CurrentDate = DateTime.Now;
        int result = DateTime.Compare(CurrentDate, uc_dt_expiration_doc.DateValue);

        if (result > 0)
        {

          lbl_expiration_doc_date.ForeColor = Color.Red;

          InvalidField _error = new InvalidField();
          _error.Id = "ExpirationDoc";
          _error.Reason = "Documento ha expirado";

          Boolean errorExist = false;
          if (tmp_errors != null)
          {
            foreach (InvalidField error in tmp_errors)
            {
              if (error.Id == _error.Id)
              {
                errorExist = true;
                break;
              }
            }
          }
          else
          {
            tmp_errors = new List<InvalidField>();
          }


          if (!errorExist)
          {
            tmp_errors.Add(_error);
          }

        }
        else
        {

          if (tmp_errors != null)
          {
            // Remove error of expiration doc
            foreach (InvalidField error in tmp_errors)
            {

              if (error.Id == "ExpirationDoc")
              {
                tmp_errors.Remove(error);
                break;
              }
            }
          }
        }

        if (tmp_errors != null)
        {
          if (tmp_errors.Count == 0)
          {
            m_error_display.StopTimerAndResetLabels();
          }
          else
          {
            m_error_display.DisplayErrors(tmp_errors);
          }

        }
      }    
    }

    /// <summary>
    ///   method to update form controls with values from model
    /// </summary>
    public override void ModelToView()
    {
      int _docvalues = 0;
      string _AvailableAmount = "";

      //load data to view
      cb_gender.SelectedItem = m_model.HolderGender;
      // ATB 02-DEC-2016
      if (m_model.HolderNationality == null || String.IsNullOrEmpty(m_model.HolderNationality.Name))
      {
        SelectDefaultCountryFromGeneralParam("Account.DefaultValues", "Country");
      }
      else
      {
        cb_nationality.SelectedItem = m_model.HolderNationality;
      }
      txt_name.Text = string.IsNullOrEmpty(m_model.HolderName3) ? String.Empty : m_model.HolderName3.Trim();
      txt_name4.Text = string.IsNullOrEmpty(m_model.HolderName4) ? String.Empty : m_model.HolderName4.Trim();
      txt_last_name1.Text = string.IsNullOrEmpty(m_model.HolderName1) ? String.Empty : m_model.HolderName1.Trim();
      txt_last_name2.Text = string.IsNullOrEmpty(m_model.HolderName2) ? String.Empty : m_model.HolderName2.Trim();
      uc_dt_birth.DateValue = m_model.HolderBirthDate ?? default(DateTime);

      cb_birth_country.SelectedItem = m_model.HolderBirthCountry;
      txt_email_01.Text = m_model.HolderEmail1;
      txt_phone_01.Text = m_model.HolderPhone1;
      txt_phone_02.Text = m_model.HolderPhone2;
      // ATB 02-DEC-2016
      if (m_model.HolderDocumentType == null || String.IsNullOrEmpty(m_model.HolderDocumentType.Name) || !m_model.AccountId.HasValue)
      {
        SelectDefaultDocumentTypeFromGeneralParam("Account.DefaultValues", "DocumentType");
      }
      else
      {
        cb_document_type.SelectedItem = m_model.HolderDocumentType;
      }
      txt_document.Text = m_model.HolderDocument;
      m_documents.Clear();
      foreach (var _v in m_model.IDDocuments)
      {
        m_documents.Add(_v.Key, _v.Value);
        if (!string.IsNullOrEmpty(_v.Value))
        {
          _docvalues++;
        }
        lbl_type_document.Text = Resource.String("STR_FRM_PLAYER_EDIT_TYPE_DOCUMENT");
      }
      try
      {
        UpdateDocTypeDescriptions();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      txt_comments.Text = m_model.Comments;
      chk_show_comments.Checked = m_model.ShowComments;
      uc_dt_wedding.DateValue = m_model.HolderWeddingDate ?? DateTime.MinValue;
      txt_RFC.Text = m_model.HolderRFC;
      txt_CURP.Text = m_model.HolderCURP;
      cb_occupation.SelectedItem = m_model.HolderOccupation;
      cb_marital.SelectedItem = m_model.HolderMaritalStatus;
      txt_email_02.Text = m_model.HolderEmail2;
      txt_twitter.Text = m_model.HolderTwitterAccount;
      chk_holder_has_beneficiary.CheckState = m_model.HasBeneficiary ? CheckState.Checked : CheckState.Unchecked;

      txt_beneficiary_name.Text = m_model.BeneficiaryName1;
      txt_beneficiary_last_name1.Text = m_model.BeneficiaryLastName;
      txt_beneficiary_last_name2.Text = m_model.BeneficiaryLastName2;
      txt_beneficiary_CURP.Text = m_model.BeneficiaryCURP;
      txt_beneficiary_RFC.Text = m_model.BeneficiaryDocument;
      uc_dt_ben_birth.DateValue = m_model.BeneficiaryBirthDate ?? DateTime.MinValue;
      cb_beneficiary_gender.SelectedItem = m_model.BeneficiaryGender.Id;
      lbl_full_name.Text = m_model.HolderFullName;
      lbl_beneficiary_full_name.Text = m_model.BeneficiaryName;
      m_img_photo.Image = m_model.HolderPhoto;
      txt_pin.Text = m_model.Pin;
      txt_confirm_pin.Text = m_model.Pin;
      lbl_points_to_maintain.Text = m_model.PointsToMaintain;
      //lbl_points_to_maintain.TextAlign
      
      //DLM 23-AUG-2018
      uc_dt_expiration_doc.Enabled = GeneralParam.GetBoolean("Cashier", "Document.EnableExpirationDate", false);

      if (m_model.ScannedDocuments.Count > 0)
      {
        uc_dt_expiration_doc.DateValue = m_model.ScannedDocuments[0].Expiration.Value;
      }

      /*if (m_model.RecordExpirationDate != null)
      {
        //if (uc_dt_expiration_doc.DateValue != m_model.RecordExpirationDate)
        //{
          uc_dt_expiration_doc.DateValue = m_model.RecordExpirationDate ?? DateTime.MinValue;
        //}
      }*/

      //DLM 23-AUG-2018

      lbl_points_discretionaries.Text = m_model.PointsDiscretionaries;
      lbl_points_generated.Text = m_model.PointsGenerated;
      lbl_points_level.Text = m_model.PointsLevel;
      gb_next_level.Visible = m_model.PointsNextLevelVisible;
      if (m_model.PointsNextLevelVisible)
      {
        gb_next_level.Text = m_model.PointsNextLevelName;
        lbl_points_to_next_level.Text = m_model.PointsToNextLevel;
        lbl_points_req.Text = m_model.PointsRequired;
      }

      lbl_level.Text = m_model.CurrentLevel;
      lbl_level1.Text = m_model.CurrentLevel;
      lbl_points.Text = m_model.CurrentPoints;
      lbl_level_entered.Text = m_model.CurrentLevelEntered;
      lbl_level_expiration.Text = m_model.CurrentLevelExpiration;


      HolderAddress.HouseNumber = m_model.HolderExtNum;
      HolderAddress.AddressLine1 = m_model.HolderAddressLine01;
      HolderAddress.AddressLine2 = m_model.HolderAddressLine02;
      HolderAddress.AddressLine3 = m_model.HolderAddressLine03;
      HolderAddress.PostCode = m_model.HolderZip;
      HolderAddress.Town = m_model.HolderCity;
      HolderAddress.Country = m_model.HolderAddressCountry ?? new Country { Id = 0 };
      HolderAddress.State = m_model.HolderFedEntity ?? new State { Id = 0 };

      txt_block_description.Text = m_model.CardBlockDescription;

      txt_beneficiary_CURP.Enabled = m_model.HasBeneficiary;
      txt_beneficiary_RFC.Enabled = m_model.HasBeneficiary;
      txt_beneficiary_last_name1.Enabled = m_model.HasBeneficiary;
      txt_beneficiary_last_name2.Enabled = m_model.HasBeneficiary;
      txt_beneficiary_name.Enabled = m_model.HasBeneficiary;
      cb_beneficiary_gender.Enabled = m_model.HasBeneficiary;
      cb_beneficiary_occupation.Enabled = m_model.HasBeneficiary;
      uc_dt_ben_birth.Enabled = m_model.HasBeneficiary;
      btn_ben_scan_docs.Enabled = m_model.HasBeneficiary;
      lbl_beneficiary_CURP.Enabled = m_model.HasBeneficiary;
      lbl_beneficiary_RFC.Enabled = m_model.HasBeneficiary;
      lbl_beneficiary_birth_date.Enabled = m_model.HasBeneficiary;
      lbl_beneficiary_full_name.Enabled = m_model.HasBeneficiary;
      lbl_beneficiary_gender.Enabled = m_model.HasBeneficiary;
      lbl_beneficiary_last_name1.Enabled = m_model.HasBeneficiary;
      lbl_beneficiary_last_name2.Enabled = m_model.HasBeneficiary;
      lbl_beneficiary_name.Enabled = m_model.HasBeneficiary;
      lbl_beneficiary_occupation.Enabled = m_model.HasBeneficiary;

      bool _enabledscan = ((PlayerEditDetailsCashierController)m_controller).ScannerEnabled;
      btn_id_card_scan.Visible = _enabledscan;
      btn_id_card_scan.Enabled = _enabledscan && (m_cashier_status == CASHIER_STATUS.OPEN);

      if (((PlayerEditDetailsCashierController)m_controller).ScreenMode == SCREEN_MODE.REGISTER_CODERE)
      {
        btn_print_information.Visible = false;
      }

      int _i = lbl_type_document.Text.IndexOf(" (", StringComparison.Ordinal);
      if (_i < 1)
        _i = lbl_type_document.Text.Length;
      lbl_type_document.Text = lbl_type_document.Text.Substring(0, _i);

      if (_docvalues > 1)
      {
        lbl_type_document.Text += string.Format(" ({0})", _docvalues);
      }

      lbl_docuemnt_counter.ForeColor = Color.Green;

      //Credit Line Model To View
      if (m_model.CreditLineData.Exists)
      {
        // The values to be displayed
        lbl_credit_line_status_value.Text = CreditLine.StatusToString(m_model.CreditLineData.Status);
        lbl_credit_line_limit_value.Text = m_model.CreditLineData.LimitAmount.ToString();
        lbl_credit_line_spent_value.Text = m_model.CreditLineData.SpentAmount.ToString();

        //Available amount is base limit minus spent (no TTO) and display 0 if less than 0
        if (m_model.CreditLineData.AvailableAmountNoTTO < (decimal)0)
        {
          _AvailableAmount = "0.00";
        }
        else
        {
          _AvailableAmount = m_model.CreditLineData.AvailableAmountNoTTO.ToString();
        }

        lbl_credit_line_available_value.Text = _AvailableAmount;

        lbl_credit_line_extension_value.Text = m_model.CreditLineData.ExtensionAmount.ToString();
      }

      EnableRfcButton();
      SetFormTitle();
      if (m_controller == null)
        return;
      ((PlayerEditDetailsCashierController)m_controller).BlockInfo(m_model);

      // LTC 2018-MAR-29
      cb_birth_country.Set<Country>(m_model.HolderBirthCountry, m_model.AccountId == null ? "Country" : null);  //LTC 29-MAR-2018
      cb_nationality.Set<Nationality>(m_model.HolderNationality, m_model.AccountId == null ? "Country" : null);  //LTC 29-MAR-2018

    } //ModelToView

    /// <summary>
    ///   method to retrieve values from form and update model
    /// </summary>
    public override void ViewToModel()
    {
      m_model.HolderExtNum = HolderAddress.HouseNumber;
      m_model.HolderAddressLine01 = HolderAddress.AddressLine1;
      m_model.HolderAddressLine02 = HolderAddress.AddressLine2;
      m_model.HolderAddressLine03 = HolderAddress.AddressLine3;
      m_model.HolderZip = HolderAddress.PostCode;
      m_model.HolderCity = HolderAddress.Town;
      m_model.HolderAddressCountry = HolderAddress.Country as Country;
      m_model.HolderFedEntity = HolderAddress.State as State;
      m_model.HolderGender = (Gender)cb_gender.SelectedItem;
      m_model.HolderNationality = (Nationality)cb_nationality.SelectedItem;
      m_model.HolderName1 = txt_last_name1.Text.Trim();
      m_model.HolderName2 = txt_last_name2.Text.Trim();
      m_model.HolderName3 = txt_name.Text.Trim();
      m_model.HolderName4 = txt_name4.Text.Trim();
      m_model.HolderBirthDate = uc_dt_birth.DateValue;
      m_model.HolderEmail1 = txt_email_01.Text;
      m_model.HolderPhone1 = txt_phone_01.Text;
      m_model.HolderPhone2 = txt_phone_02.Text;
      m_model.HolderDocumentType = (DocumentType)cb_document_type.SelectedItem;
      m_model.HolderDocument = txt_document.Text;
      m_model.Comments = txt_comments.Text;
      m_model.ShowComments = chk_show_comments.Checked;
      m_model.HolderBirthCountry = (Country)cb_birth_country.SelectedItem;
      m_model.HolderWeddingDate = uc_dt_wedding.DateValue;
      m_model.HolderRFC = txt_RFC.Text;
      m_model.HolderCURP = txt_CURP.Text;
      m_model.HolderOccupation = (OccupationWRK)cb_occupation.SelectedItem;
      m_model.HolderMaritalStatus = (MaritalStatus)cb_marital.SelectedItem;
      m_model.HolderTwitterAccount = txt_twitter.Text;
      m_model.HolderEmail2 = txt_email_02.Text;
      m_model.HolderFullName = lbl_full_name.Text;

      m_model.HasBeneficiary = chk_holder_has_beneficiary.Checked;
      m_model.BeneficiaryName1 = txt_beneficiary_name.Text;
      m_model.BeneficiaryLastName = txt_beneficiary_last_name1.Text;
      m_model.BeneficiaryLastName2 = txt_beneficiary_last_name2.Text;
      m_model.BeneficiaryName = lbl_beneficiary_full_name.Text;
      m_model.BeneficiaryDocument = txt_beneficiary_RFC.Text;
      m_model.BeneficiaryCURP = txt_beneficiary_CURP.Text;
      m_model.BeneficiaryBirthDate = uc_dt_ben_birth.DateValue;
      m_model.BeneficiaryGender = (Gender)cb_beneficiary_gender.SelectedItem;
      m_model.BeneficiaryOccupation = (OccupationWRK)cb_beneficiary_occupation.SelectedItem;
      m_model.HolderPhoto = m_img_photo.Image;
      m_model.Pin = txt_pin.Text;
      m_model.HolderRFC = txt_RFC.Text;
      m_model.HolderCURP = txt_CURP.Text;

      /* DLM  17-09-2018 */
      if (m_model.ScannedDocuments.Count > 0 && uc_dt_expiration_doc.DateValue != null)
      {
        m_model.ScannedDocuments[0].Expiration = uc_dt_expiration_doc.DateValue;
      }

      //m_model.RecordExpirationDate = uc_dt_expiration_doc.DateValue;
      /* DLM  17-09-2018 */


      m_model.IDDocuments.Clear();
      foreach (var _v in m_documents)
      {
        m_model.IDDocuments.Add(_v.Key, _v.Value);
      }
    } //ViewToModel

    /// <summary>
    ///   method to set-up form on first view
    /// </summary>
    /// <param name="Sender"></param>
    /// <param name="E"></param>
    private void PlayerEditDetailsCashierView_Shown(object Sender, EventArgs E)
    {
      SetFormTitle();
      lbl_pin_msg.Text = "";
      if (CalculateNameColumnWidths())
      {
        return;
      }
      SpecialCaseNameChanging();
      btn_save_pin.Enabled = false;
      lbl_pin_msg.Visible = false;
      txt_confirm_pin.Enabled = false;
      string _error_str;
      if (m_model.MustShowComments)
      {
        tab_player_edit.SelectTab("comments");
        //HideTabs();
        SetContentForComments(
          ProfilePermissions.CheckPermissionList(
            ProfilePermissions.CashierFormFuncionality.EditCommentsRequiredReading,
            ProfilePermissions.TypeOperation.OnlyCheck, ParentForm, 0, out _error_str));
      }

      if (
        !ProfilePermissions.CheckPermissionList(ProfilePermissions.CashierFormFuncionality.AccountGenerateRFC,
          ProfilePermissions.TypeOperation.OnlyCheck, this, 0, out _error_str))
      {
        btn_RFC.Enabled = false;
      }
      if (!GeneralParam.GetBoolean("AntiMoneyLaundering", "HasBeneficiary", false))
      {
        tab_player_edit.TabPages.RemoveByKey("beneficiary");
      }

      if (!GeneralParam.GetBoolean("CreditLine", "Enabled", false) || !m_model.CreditLineData.Exists)
      {
        tab_player_edit.TabPages.RemoveByKey("credit_line");
      }

      if (((PlayerEditDetailsCashierController)m_controller).ScreenMode == SCREEN_MODE.REGISTER_CODERE)
      {
        tab_player_edit.TabPages.RemoveByKey("contact");
        tab_player_edit.TabPages.RemoveByKey("comments");
        tab_player_edit.TabPages.RemoveByKey("points");
        tab_player_edit.TabPages.RemoveByKey("pin");
        tab_player_edit.TabPages.RemoveByKey("block");
        tab_player_edit.TabPages.RemoveByKey("beneficiary");
        tab_player_edit.TabPages.RemoveByKey("credit_line");
      }

    } //PlayerEditDetailsCashierView_Shown

    /// <summary>
    ///   method to rename specific fields if they meet a given criteria
    /// </summary>
    private void SpecialCaseNameChanging()
    {
      if (txt_last_name1.Visible && !txt_last_name2.Visible)
      {
        lbl_last_name1.Text = Resource.String("STR_FRM_PLAYER_EDIT_LAST_NAME");
      }

      if (!GeneralParam.GetBoolean("Account.VisibleField", "Phone1", true) ||
          !GeneralParam.GetBoolean("Account.VisibleField", "Phone2", true))
      {
        if (GeneralParam.GetBoolean("Account.VisibleField", "Phone1", true))
        {
          lbl_phone_01.Text = Resource.String("STR_FRM_PLAYER_EDIT_PHONE");
        }
        if (GeneralParam.GetBoolean("Account.VisibleField", "Phone2", true))
        {
          lbl_phone_02.Text = Resource.String("STR_FRM_PLAYER_EDIT_PHONE");
        }
      }

      if (!GeneralParam.GetBoolean("Account.VisibleField", "Email1", true) ||
          !GeneralParam.GetBoolean("Account.VisibleField", "Email2", true))
      {
        if (GeneralParam.GetBoolean("Account.VisibleField", "Email1", true))
        {
          lbl_email_01.Text = Resource.String("STR_FRM_PLAYER_EDIT_EMAIL");
        }
        if (GeneralParam.GetBoolean("Account.VisibleField", "Email2", true))
        {
          lbl_email_02.Text = Resource.String("STR_FRM_PLAYER_EDIT_EMAIL");
        }
      }
    } //SpecialCaseNameChanging

    /// <summary>
    ///   method to adjust column widths depending on the
    ///   number of elements that make up the name
    /// </summary>
    /// <returns></returns>
    private bool CalculateNameColumnWidths()
    {
      int _count_name;
      int _count;
      float _width;

      _count = 0;
      _count_name = 0;
      _count_name += lbl_name.Visible ? 1 : 0;
      _count_name += lbl_name4.Visible ? 1 : 0;
      _count_name += lbl_last_name1.Visible ? 1 : 0;
      _count_name += lbl_last_name2.Visible ? 1 : 0;

      if (_count_name == 0)
      {
        return true;
      }

      _width = 100f / _count_name;

      bool _mode_last_name = GeneralParam.GetBoolean("Account", "InverseNameOrder", false);

      foreach (ColumnStyle _col_style in tlp_name.ColumnStyles)
      {
        if (_mode_last_name)
        {
          switch (_count)
          {
            case 0:
              _col_style.Width = lbl_last_name1.Visible ? _width : 0;
              break;
            case 1:
              _col_style.Width = lbl_last_name2.Visible ? _width : 0;
              break;
            case 2:
              _col_style.Width = lbl_name.Visible ? _width : 0;
              break;
            case 3:
              _col_style.Width = lbl_name4.Visible ? _width : 0;
              break;
          }
        }
        else
        {
          switch (_count)
          {
            case 0:
              _col_style.Width = lbl_name.Visible ? _width : 0;
              break;
            case 1:
              _col_style.Width = lbl_name4.Visible ? _width : 0;
              break;
            case 2:
              _col_style.Width = lbl_last_name1.Visible ? _width : 0;
              break;
            case 3:
              _col_style.Width = lbl_last_name2.Visible ? _width : 0;
              break;
          }
        }
        _count++;
      }
      return false;
    } //CalculateNameColumnWidths

    /// <summary>
    ///   set localised translations of labels
    /// </summary>
    private void SetLabelTexts()
    {
      chk_show_comments.Location = new Point(11, 360);
      lbl_block_description.Text = Resource.String("STR_FRM_BLOCK_ACCOUNT_LABEL_DESCRIPTION_UNBLOCK");
      lbl_name.Text = AccountFields.GetName();
      lbl_last_name1.Text = AccountFields.GetSurname1();// Resource.String("STR_FRM_PLAYER_EDIT_LAST_NAME1");
      lbl_last_name2.Text = AccountFields.GetSurname2();//Resource.String("STR_FRM_PLAYER_EDIT_LAST_NAME2");
      lbl_name4.Text = AccountFields.GetMiddleName();//Resource.String("STR_FRM_PLAYER_EDIT_NAME4");
      lbl_type_document.Text = Resource.String("STR_FRM_PLAYER_EDIT_TYPE_DOCUMENT");
      lbl_document.Text = Resource.String("STR_FRM_PLAYER_EDIT_DOCUMENT");
      lbl_nationality.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_NATIONALITY");
      btn_scan_docs.Text = Resource.String("STR_FRM_PLAYER_EDIT_SCAN");

      btn_RFC.Text = IdentificationTypes.DocIdTypeString(ACCOUNT_HOLDER_ID_TYPE.RFC);
      lbl_CURP.Text = IdentificationTypes.DocIdTypeString(ACCOUNT_HOLDER_ID_TYPE.CURP);
      btn_print_information.Text = Resource.String("STR_FRM_PRINT_INFORMATION");

      lbl_gender.Text = Resource.String("STR_UC_PLAYER_TRACK_GENDER");
      lbl_birth_date.Text = Resource.String("STR_FRM_PLAYER_EDIT_BIRTH_DATE");
      lbl_birth_country.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_BIRTH_COUNTRY");
      lbl_wedding_date.Text = Resource.String("STR_FRM_PLAYER_EDIT_WEDDING_DATE");
      lbl_marital_status.Text = Resource.String("STR_UC_PLAYER_TRACK_MARITAL_STATUS");
      lbl_occupation.Text = Resource.String("STR_FRM_PLAYER_EDIT_OCCUPATION");

      //FBA 16-SEP-2013

      tab_player_edit.TabPages["principal"].Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_TAB_MAIN");
      tab_player_edit.TabPages["address"].Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_TAB_ADDRESS");
      tab_player_edit.TabPages["contact"].Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_TAB_CONTACT");
      tab_player_edit.TabPages["comments"].Text = Resource.String("STR_UC_PLAYER_TRACK_COMMENTS");
      tab_player_edit.TabPages["points"].Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_TAB_POINTS");
      tab_player_edit.TabPages["pin"].Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_PIN");
      tab_player_edit.TabPages["block"].Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_TAB_BLOCK");
      tab_player_edit.TabPages["beneficiary"].Text = Resource.String("STR_FRM_PLAYER_EDIT_BENEFICIARY");
      tab_player_edit.TabPages["credit_line"].Text = Resource.String("STR_FRM_PLAYER_EDIT_CREDIT_LINE");

      //   - Buttons


      lbl_beneficiary_name.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_NAME");
      lbl_beneficiary_last_name1.Text = Resource.String("STR_FRM_PLAYER_EDIT_LAST_NAME1");
      lbl_beneficiary_last_name2.Text = Resource.String("STR_FRM_PLAYER_EDIT_LAST_NAME2");
      lbl_beneficiary_full_name.Text = Resource.String("STR_FRM_PLAYER_EDIT_FULL_NAME");
      lbl_beneficiary_RFC.Text = IdentificationTypes.DocIdTypeString(ACCOUNT_HOLDER_ID_TYPE.RFC);
      lbl_beneficiary_CURP.Text = IdentificationTypes.DocIdTypeString(ACCOUNT_HOLDER_ID_TYPE.CURP);
      lbl_beneficiary_gender.Text = Resource.String("STR_UC_PLAYER_TRACK_GENDER");
      lbl_beneficiary_birth_date.Text = Resource.String("STR_FRM_PLAYER_EDIT_BIRTH_DATE");
      lbl_beneficiary_occupation.Text = Resource.String("STR_FRM_PLAYER_EDIT_OCCUPATION");
      chk_holder_has_beneficiary.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_HOLDER_HAS_BENEF");
      btn_ben_scan_docs.Text = Resource.String("STR_FRM_PLAYER_EDIT_SCAN");


      btn_ok.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_OK");
      btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");
      btn_save_pin.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_SAVE_PIN");
      btn_generate_random_pin.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_GENERATE_RANDOM_PIN");

      txt_pin.MaxLength = Accounts.PIN_MAX_SAVE_LENGTH;
      txt_confirm_pin.MaxLength = Accounts.PIN_MAX_SAVE_LENGTH;
      lbl_email_01.Text = Resource.String("STR_UC_PLAYER_TRACK_EMAIL_01") + @" 1";
      lbl_email_02.Text = Resource.String("STR_UC_PLAYER_TRACK_EMAIL_01") + @" 2";
      lbl_twitter.Text = Resource.String("STR_UC_PLAYER_TRACK_TWITTER");
      lbl_phone_01.Text = Resource.String("STR_UC_PLAYER_TRACK_HOME_PHONE");
      lbl_phone_02.Text = Resource.String("STR_UC_PLAYER_TRACK_MOBILE_PHONE");

      btn_print_information.Text = Resource.String("STR_FRM_PRINT_INFORMATION");

      chk_show_comments.Text = Resource.String("STR_FRM_PLAYER_EDIT_SHOW_COMMENTS");
      lbl_marital_status.Text = Resource.String("STR_UC_PLAYER_TRACK_MARITAL_STATUS");

      lbl_level_prefix.Text = Resource.String("STR_UC_PLAYER_TRACK_LEVEL") + @":";
      lbl_level_prefix1.Text = Resource.String("STR_UC_PLAYER_TRACK_LEVEL");
      lbl_level_entered_prefix.Text = Resource.String("STR_UC_PLAYER_TRACK_LEVEL_ENTERED") + @":";
      lbl_level_expiration_prefix.Text = Resource.String("STR_UC_PLAYER_TRACK_LEVEL_EXPIRATION") + @":";
      lbl_points_prefix.Text = Resource.String("STR_UC_CARD_POINTS_BALANCE_TITLE") + @":";
      lbl_points_sufix.Text = Resource.String("STR_UC_CARD_POINTS_BALANCE_SUFFIX");
      //FBA 16-SEP-2013
      //gb_next_level.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_NEXT_LEVEL");
      lbl_points_level_prefix.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_LEVEL_POINTS");
      lbl_points_req_prefix.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_REQUIRED_POINTS");
      lbl_points_to_next_level_prefix.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_POINTS_TO_NEXT_LEVEL");
      lbl_points_discretionaries_prefix.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_DISCRETIONATED_POINTS");
      lbl_points_generated_prefix.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_GENERATED_POINTS");
      lbl_points_last_days.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_DAYS_COUNTING_POINTS", GeneralParam.GetString("PlayerTracking", "Levels.DaysCountingPoints"));
      lbl_pin.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_PIN");
      lbl_confirm_pin.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_CONFIRM_PIN");
      lbl_points_to_maintain_prefix.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_POINTS_TO_MAINTAIN_LEVEL");
      txt_pin.MaxLength = Accounts.PIN_MAX_SAVE_LENGTH;
      txt_confirm_pin.MaxLength = Accounts.PIN_MAX_SAVE_LENGTH;
      lbl_age.Text = String.Empty;

      //Credit Line labels
      lbl_credit_line_status.Text = Resource.String("STR_FRM_CREDIT_LINE_STATUS");
      lbl_credit_line_limit.Text = Resource.String("STR_FRM_CREDIT_LINE_LIMIT");
      lbl_credit_line_spent.Text = Resource.String("STR_FRM_CREDIT_LINE_SPENT");
      lbl_credit_line_available.Text = Resource.String("STR_FRM_CREDIT_LINE_AVAILABLE");
      lbl_credit_line_extension.Text = Resource.String("STR_FRM_CREDIT_LINE_TTO_EXTENSION");
      btn_credit_line_payback.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CREDIT_LINE_PAYBACK");
      //DLM 22-AUG-2018
      lbl_expiration_doc_date.Text = Resource.String("STR_FRM_PLAYER_EDIT_EXPIRATION_DOC_DATE");


    } //SetLabelTexts

    /// <summary>
    ///   set up comment field
    /// </summary>
    /// <param name="IsEditable"></param>
    private void SetContentForComments(bool IsEditable)
    {
      chk_show_comments.Enabled = IsEditable;

      btn_print_information.Visible = false;
      btn_ok.Visible = IsEditable;
      btn_keyboard.Visible = IsEditable;
      btn_id_card_scan.Visible = IsEditable;
      if (!IsEditable)
      {
        btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CLOSE");
      }
      txt_comments.ReadOnly = !IsEditable;
    } // SetContentForComments

    /// <summary>
    ///   hide all tabs except comments
    /// </summary>
    private void HideTabs()
    {
      if (tab_player_edit.TabCount > 0)
      {
        foreach (TabPage _tab in tab_player_edit.TabPages)
        {
          if (_tab.Name != "comments")
          {
            tab_player_edit.TabPages.Remove(_tab);
          }
        }
      }
    } // HideTabs


    public List<InvalidField> MarkBadDates(Control Ctrl)
    {
      List<InvalidField> _result = new List<InvalidField>();
      Type _type;
      _type = Ctrl.GetType();
      if (_type == typeof(uc_datetime))
      {
        uc_datetime _uc_datetime = Ctrl as uc_datetime;
        if (_uc_datetime != null && _uc_datetime.Invalid)
          _result.Add(new InvalidField() { Id = Ctrl.Tag.ToString(), Reason = Resource.String("ERR_FECHA") });
      }
      if (!Ctrl.HasChildren)
      {
        return _result;
      }
      foreach (Control _ctrl in Ctrl.Controls)
      {
        _result.AddRange(MarkBadDates(_ctrl));
      }
      return _result;
    }


    /// <summary>
    ///   call save routine, execute error display
    ///   method if any errors are returned
    /// </summary>
    /// <param name="Sender"></param>
    /// <param name="E"></param>
    private void btn_ok_Click(object Sender, EventArgs E)
    {
      List<InvalidField> _errors = new List<InvalidField>();

      HolderAddress.RemoveEventCboCountry();

      if (!ValidateChildren())
      {
        _errors.AddRange(MarkBadDates(this));
      }

      HolderAddress.AddEventCboCountry();

      m_error_display.StopTimerAndResetLabels();

      _errors = ((PlayerEditDetailsCashierController)m_controller).Save(_errors);

      if (_errors.Count == 0)
      {
        DialogResult = DialogResult.OK;
      }


      if (tmp_errors == null)
      {
        tmp_errors = new List<InvalidField>(_errors);

      } else {


        foreach (InvalidField error in _errors)
        {
          Boolean errorExist = false;
          foreach (InvalidField tmp_error in tmp_errors)
          {
            if (error.Id == tmp_error.Id)
            {
              errorExist = true;
              break;
            }
          }

          if (!errorExist)
          {
            tmp_errors.Add(error);
          }
        }

        

      }


      //ValidateDocumentExpiration(false); //_errors, false);
      m_error_display.DisplayErrors(tmp_errors);
      //m_error_display.DisplayErrors(_errors);
    } //btn_ok_Click

    /// <summary>
    ///   method to call controller method to format the name
    /// </summary>
    /// <param name="Sender"></param>
    /// <param name="E"></param>
    private void txt_beneficiary_name_or_last_names_TextChanged(object Sender, EventArgs E)
    {
      if (m_controller == null)
      {
        return;
      }
      lbl_beneficiary_full_name.Text =
        ((PlayerEditDetailsCashierController)m_controller).ReFormatFullNameName(new[] { txt_beneficiary_name.Text, txt_beneficiary_last_name1.Text, txt_beneficiary_last_name2.Text });
    } //txt_beneficiary_name_or_last_names_TextChanged

    /// <summary>
    ///   button to call controller method to toggle keyboard visibility
    /// </summary>
    /// <param name="Sender"></param>
    /// <param name="E"></param>
    private void btn_keyboard_Click(object Sender, EventArgs E)
    {
      ((PlayerEditDetailsCashierController)m_controller).ToggleKeyboard();
    } //btn_keyboard_Click

    /// <summary>
    ///   button to cancel changes and return to parent form
    /// </summary>
    /// <param name="Sender"></param>
    /// <param name="E"></param>
    private void btn_cancel_Click(object Sender, EventArgs E)
    {
      DialogResult = DialogResult.Cancel;
    } //btn_cancel_Click

    /// <summary>
    ///   method to clean beneficiary fields, usually used when no beneficiary is selected
    /// </summary>
    private void CleanBeneficiaryData()
    {
      txt_beneficiary_last_name1.Text = "";
      txt_beneficiary_last_name2.Text = "";
      txt_beneficiary_name.Text = "";
      txt_beneficiary_RFC.Text = "";
      txt_beneficiary_CURP.Text = "";
      uc_dt_ben_birth.DateValue = DateTime.MinValue;
      cb_beneficiary_gender.SelectedValue = 0;
      cb_beneficiary_occupation.SelectedItem = m_model.Occupations[0];

    } // CleanBeneficiaryData


    /// <summary>
    ///   method to recover beneficiary data is has beneficiary
    ///   is selected immediately after removing beneficiary
    /// </summary>
    private void RecoverEditedBeneficiaryData()
    {
      if (m_beneficiary_data == null)
      {
        return;
      }
      // Get beneficiary last_name1
      if (m_beneficiary_data.ContainsKey("txt_beneficiary_last_name1"))
      {
        txt_beneficiary_last_name1.Text = (string)m_beneficiary_data["txt_beneficiary_last_name1"];
      }

      // Get beneficiary last_name2
      if (m_beneficiary_data.ContainsKey("txt_beneficiary_last_name2"))
      {
        txt_beneficiary_last_name2.Text = (string)m_beneficiary_data["txt_beneficiary_last_name2"];
      }

      // Get beneficiary name
      if (m_beneficiary_data.ContainsKey("txt_beneficiary_name"))
      {
        txt_beneficiary_name.Text = (string)m_beneficiary_data["txt_beneficiary_name"];
      }

      // Get beneficiary RFC
      if (m_beneficiary_data.ContainsKey("txt_beneficiary_RFC"))
      {
        txt_beneficiary_RFC.Text = (string)m_beneficiary_data["txt_beneficiary_RFC"];
      }

      // Get beneficiary CURP
      if (m_beneficiary_data.ContainsKey("txt_beneficiary_CURP"))
      {
        txt_beneficiary_CURP.Text = (string)m_beneficiary_data["txt_beneficiary_CURP"];
      }

      // Get beneficiary birth day
      if (m_beneficiary_data.ContainsKey("uc_dt_ben_birth"))
      {
        uc_dt_ben_birth.DateValue = (DateTime)m_beneficiary_data["uc_dt_ben_birth"];
      }

      // Get beneficiary gender
      if (m_beneficiary_data.ContainsKey("cb_beneficiary_gender"))
      {
        cb_beneficiary_gender.SelectedItem = (Gender)m_beneficiary_data["cb_beneficiary_gender"];
      }

      // Get beneficiary gender
      if (m_beneficiary_data.ContainsKey("cb_beneficiary_occupation"))
      {
        cb_beneficiary_occupation.SelectedItem = (OccupationWRK)m_beneficiary_data["cb_beneficiary_occupation"];
      }
      if (m_beneficiary_data.ContainsKey("benscandocs"))
      {
        m_model.BeneficiaryDocScan = (DocumentList)m_beneficiary_data["benscandocs"];
      }
    } // RecoverEditedBeneficiaryData

    /// <summary>
    ///   method to react to changes in state of has beneficiary
    /// </summary>
    /// <param name="Sender"></param>
    /// <param name="E"></param>
    private void chk_holder_has_beneficiary_CheckedChanged(object Sender, EventArgs E)
    {
      if (!chk_holder_has_beneficiary.Checked)
      {
        CleanBeneficiaryData();
        m_model.BeneficiaryDocScan = new DocumentList();
        m_model.BeneficiaryDocsChanged = true;
      }
      else
      {
        RecoverEditedBeneficiaryData();
      }

      btn_ben_scan_docs.Enabled = chk_holder_has_beneficiary.Checked;
      lbl_beneficiary_last_name1.Enabled = chk_holder_has_beneficiary.Checked;
      txt_beneficiary_last_name1.Enabled = chk_holder_has_beneficiary.Checked;
      lbl_beneficiary_last_name2.Enabled = chk_holder_has_beneficiary.Checked;
      txt_beneficiary_last_name2.Enabled = chk_holder_has_beneficiary.Checked;
      txt_beneficiary_name.Enabled = chk_holder_has_beneficiary.Checked;
      lbl_beneficiary_name.Enabled = chk_holder_has_beneficiary.Checked;
      lbl_beneficiary_full_name.Enabled = chk_holder_has_beneficiary.Checked;
      lbl_beneficiary_RFC.Enabled = chk_holder_has_beneficiary.Checked;
      txt_beneficiary_RFC.Enabled = chk_holder_has_beneficiary.Checked;
      lbl_beneficiary_CURP.Enabled = chk_holder_has_beneficiary.Checked;
      txt_beneficiary_CURP.Enabled = chk_holder_has_beneficiary.Checked;
      lbl_beneficiary_occupation.Enabled = chk_holder_has_beneficiary.Checked;
      lbl_beneficiary_gender.Enabled = chk_holder_has_beneficiary.Checked;
      cb_beneficiary_gender.Enabled = chk_holder_has_beneficiary.Checked;
      lbl_beneficiary_birth_date.Enabled = chk_holder_has_beneficiary.Checked;
      uc_dt_ben_birth.Enabled = chk_holder_has_beneficiary.Checked;
      cb_beneficiary_occupation.Enabled = chk_holder_has_beneficiary.Checked;
    } //chk_holder_has_beneficiary_CheckedChanged

    /// <summary>
    ///   event fired when one of the fields that make up the
    ///   name has been changed to regenerate the full name
    /// </summary>
    /// <param name="Sender"></param>
    /// <param name="E"></param>
    private void txt_name_TextChanged(object Sender, EventArgs E)
    {
      if (string.IsNullOrEmpty(txt_name.Text) &&
          string.IsNullOrEmpty(txt_name4.Text) &&
          string.IsNullOrEmpty(txt_last_name2.Text) &&
          string.IsNullOrEmpty(txt_last_name1.Text))
        return;

      if (m_controller == null)
      {
        return;
      }
      lbl_full_name.Text =
        ((PlayerEditDetailsCashierController)m_controller).ReFormatFullNameName(new[] { txt_name.Text, txt_name4.Text, txt_last_name1.Text, txt_last_name2.Text });
      EnableRfcButton();
    } //txt_name_TextChanged

    /// <summary>
    ///   button to call controller perform id scan method
    /// </summary>
    /// <param name="Sender"></param>
    /// <param name="E"></param>
    private void btn_id_card_scan_Click(object Sender, EventArgs E)
    {
      ((PlayerEditDetailsCashierController)m_controller).IdScan();
    } //btn_id_card_scan_Click

    /// <summary>
    ///   button to perform controller scan documents method for holder
    /// </summary>
    /// <param name="Sender"></param>
    /// <param name="E"></param>
    private void btn_scan_docs_Click(object Sender, EventArgs E)
    {
      ((PlayerEditDetailsCashierController)m_controller).ScanId(ACCOUNT_SCANNED_OWNER.HOLDER);
    } //btn_scan_docs_Click

    /// <summary>
    ///   button to perform controller scan documents method for beneficiary
    /// </summary>
    /// <param name="Sender"></param>
    /// <param name="E"></param>
    private void btn_ben_scan_docs_Click(object Sender, EventArgs E)
    {
      ((PlayerEditDetailsCashierController)m_controller).ScanId(ACCOUNT_SCANNED_OWNER.BENEFICIARY);
    } //btn_ben_scan_docs_Click

    /// <summary>
    ///   button to call controller method to take a photo
    ///   using WSI.Camera
    /// </summary>
    /// <param name="Sender"></param>
    /// <param name="E"></param>
    private void m_img_photo_Click(object Sender, EventArgs E)
    {
      ((PlayerEditDetailsCashierController)m_controller).TakePhoto();
    } //m_img_photo_Click

    /// <summary>
    ///   button to call save pin method in controller
    /// </summary>
    /// <param name="Sender"></param>
    /// <param name="E"></param>
    private void btn_save_pin_Click(object Sender, EventArgs E)
    {
      m_error_display.StopTimerAndResetLabels();

      List<InvalidField> _errors = ((PlayerEditDetailsCashierController)m_controller).SavePin();

      m_error_display.DisplayErrors(_errors);
    } //btn_save_pin_Click

    /// <summary>
    ///   button to call generate random pin method in controller
    /// </summary>
    /// <param name="Sender"></param>
    /// <param name="E"></param>
    private void btn_generate_random_pin_Click(object Sender, EventArgs E)
    {
      m_error_display.StopTimerAndResetLabels();

      List<InvalidField> _errors = ((PlayerEditDetailsCashierController)m_controller).CreateRandomPin();

      m_error_display.DisplayErrors(_errors);
    } //btn_generate_random_pin_Click

    /// <summary>
    ///   method to clear pin and counter pin on entry to pin
    /// </summary>
    /// <param name="Sender"></param>
    /// <param name="E"></param>
    private void txt_pin_Enter(object Sender, EventArgs E)
    {
      txt_pin.Text = "";
      txt_confirm_pin.Text = "";
    } //txt_pin_Enter

    /// <summary>
    ///   method to clear counter pin on entry
    /// </summary>
    /// <param name="Sender"></param>
    /// <param name="E"></param>
    private void txt_confirm_pin_Enter(object Sender, EventArgs E)
    {
      txt_confirm_pin.Text = "";
    } //txt_confirm_pin_Enter

    /// <summary>
    ///   button to enable pin save on text changes in pin text box
    /// </summary>
    /// <param name="Sender"></param>
    /// <param name="E"></param>
    private void txt_pin_TextChanged(object Sender, EventArgs E)
    {
      btn_save_pin_enable();
    } //txt_pin_TextChanged

    /// <summary>
    ///   button to enable pin save on text changes in counter pin text box
    /// </summary>
    /// <param name="Sender"></param>
    /// <param name="E"></param>
    private void txt_confirm_pin_TextChanged(object Sender, EventArgs E)
    {
      btn_save_pin_enable();
    } //txt_confirm_pin_TextChanged

    /// <summary>
    ///   method that checks if the pin is valid and savable
    /// </summary>
    private void btn_save_pin_enable()
    {
      txt_confirm_pin.Enabled = (txt_pin.Text.Length >= Accounts.PIN_MIN_LENGTH);
      btn_save_pin.Enabled = ((txt_pin.Text.Length >= Accounts.PIN_MIN_LENGTH) &&
                              (txt_confirm_pin.Text.Length == txt_pin.Text.Length) &&
                              (txt_pin.Text == txt_confirm_pin.Text));

      if ((txt_pin.Text.Length >= Accounts.PIN_MIN_LENGTH) && (txt_confirm_pin.Text.Length == txt_pin.Text.Length) &&
          (txt_pin.Text != txt_confirm_pin.Text))
      {
        lbl_pin_msg.Visible = true;
        lbl_pin_msg.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_CONFIRMATION_PIN_DIFFERS");
        lbl_pin_msg.ForeColor = Color.Red;
      }
      else
      {
        lbl_pin_msg.Visible = false;
        lbl_pin_msg.Text = "";
      }
    } // btn_save_pin_enable

    /// <summary>
    ///   method that checks if the RFC bun should be enabled
    /// </summary>
    private void EnableRfcButton()
    {
      string _error_str;

      if (!string.IsNullOrEmpty(txt_name.Text) && !string.IsNullOrEmpty(txt_last_name1.Text) &&
          !string.IsNullOrEmpty(txt_last_name2.Text) && string.IsNullOrEmpty(txt_RFC.Text) && !uc_dt_birth.IsEmpty &&
          uc_dt_birth.DateValue != DateTime.MinValue)
      {
        btn_RFC.Enabled =
          ProfilePermissions.CheckPermissionList(ProfilePermissions.CashierFormFuncionality.AccountGenerateRFC,
            ProfilePermissions.TypeOperation.OnlyCheck, this, 0, out _error_str);
      }
      else
      {
        btn_RFC.Enabled = false;
      }
    } //EnableRfcButton

    /// <summary>
    ///   button to generate RFC code
    /// </summary>
    /// <param name="Sender"></param>
    /// <param name="E"></param>
    private void btn_RFC_Click(object Sender, EventArgs E)
    {
      string _error_str;

      if (ProfilePermissions.CheckPermissionList(ProfilePermissions.CashierFormFuncionality.AccountGenerateRFC,
        ProfilePermissions.TypeOperation.OnlyCheck, this, 0, out _error_str))
      {
        txt_RFC.Text = ValidateFormat.ExpectedRFC(txt_last_name1.Text, txt_last_name2.Text,
          (txt_name.Text + " " + txt_name4.Text).Trim(),
          uc_dt_birth.DateValue);
      }
    } //btn_RFC_Click


    /// <summary>
    ///  method to react in expiration date
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void uc_dt_expiration_doc_OnDateChanged(object sender, EventArgs e)
    {
      ValidateDocumentExpiration();
    }


    /// <summary>
    ///   method to react on change in birth date and enable RFC button if needed
    /// </summary>
    /// <param name="Sender"></param>
    /// <param name="E"></param>
    private void uc_dt_birth_OnDateChanged(object Sender, EventArgs E)
    {
      SetAge(uc_dt_birth.DateValue);
      EnableRfcButton();
    } //uc_dt_birth_OnDateChanged

    /// <summary>
    ///   method to react on change in RFC text box and enable RFC button if needed
    /// </summary>
    /// <param name="Sender"></param>
    /// <param name="E"></param>
    private void txt_RFC_TextChanged(object Sender, EventArgs E)
    {
      EnableRfcButton();
    } //txt_RFC_TextChanged

    /// <summary>
    ///   method to call print information routine in controller
    /// </summary>
    /// <param name="Sender"></param>
    /// <param name="E"></param>
    private void btn_print_information_Click(object Sender, EventArgs E)
    {
      ((PlayerEditDetailsCashierController)m_controller).PrintInformation();
    } //btn_print_information_Click

    private void btn_credit_line_payback_Click(object sender, EventArgs e)
    {
      List<InvalidField> _errors = ((PlayerEditDetailsCashierController)m_controller).CustomerPayback();
      ModelToView();
    } // btn_credit_line_payback_Click

    #region Properties

    public Zip.Address AddressModified { set; get; }

    private void btn_id_card_scan_LongClick(object Sender, EventArgs E)
    {
      ((PlayerEditDetailsCashierController)m_controller).IdScanAdvanced();
    }

    private void cb_document_type_SelectedIndexChanged(object Sender, EventArgs E)
    {
      UpdateDocTypeDescriptions();
    }
    private void txt_doc_leave(object Sender, EventArgs E)
    {
      var _v = (cb_document_type.SelectedItem as DocumentType);
      if (_v == null) return;
      if (m_documents.ContainsKey(_v.Id))
      {
        m_documents[_v.Id] = txt_document.Text;
      }
      else
      {
        m_documents.Add(_v.Id, txt_document.Text);
      }
    }
    //AddressModified

    #endregion

    internal void CleanUpForm()
    {
      m_error_display.CleanUp();
      m_error_display = null;
      cb_birth_country.DataSource = null;
      cb_document_type.DataSource = null;
      cb_document_type.DataSource = null;
      cb_gender.DataSource = null;
      cb_nationality.DataSource = null;
      cb_birth_country.Dispose();
      cb_document_type.Dispose();
      cb_document_type.Dispose();
      cb_gender.Dispose();
      cb_nationality.Dispose();
      cb_birth_country = null;
      cb_document_type = null;
      cb_document_type = null;
      cb_gender = null;
      cb_nationality = null;
      HolderAddress.CleanUp();
      Model = null;
      Controller = null;
    }
    private void SetAge(DateTime bday)
    {
      if (bday == default(DateTime))
      {
        lbl_age.Text = "";
        return;
      }
      var _today = DateTime.Today;
      // Calculate the age.
      var age = _today.Year - bday.Year;
      // Go back to the year the person was born in case of a leap year
      if (bday > _today.AddYears(-age)) age--;
      lbl_age.Text = string.Format(Resource.String("STR_AGE_X"), age);
    }



  }


  public class VwBasePlayerEditDetailsCashierModel : vw_base<PlayerEditDetailsCashierModel>
  {

    public override VisibleData VisibleFields
    {
      get { return ((PlayerEditDetailsCashierView)this).VisibleFields; }
    }

    public override void ModelToView()
    {
      ((PlayerEditDetailsCashierView)this).ModelToView();
    }

    public override void ViewToModel()
    {
      ((PlayerEditDetailsCashierView)this).ViewToModel();
    }

    public override void DoFirstSet()
    {
      ((PlayerEditDetailsCashierView)this).DoFirstSet();
    }

    public override void RandomPinGeneratedOk()
    {
      ((PlayerEditDetailsCashierView)this).RandomPinGeneratedOk();
    }

    public override void PinChangedOk()
    {
      ((PlayerEditDetailsCashierView)this).PinChangedOk();
    }

    public override void ErrorUpdatingPin()
    {
      ((PlayerEditDetailsCashierView)this).ErrorUpdatingPin();
    }

  }



}
