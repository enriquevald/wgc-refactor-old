//------------------------------------------------------------------------------
// Copyright � 2007-2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: PlayerEditDetailsCommonErrorDisplay.cs
// 
//   DESCRIPTION: Composition class for common error display routines
//                
//                
//                
//                
//                
//                
// 
//        AUTHOR: Scott Adamson.
// 
// CREATION DATE: 02-DEC-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 02-DEC-2007 SJA    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using WSI.Cashier.Controls;
using WSI.Cashier.MVC.Interface;
using WSI.Common.Entities.General;

namespace WSI.Cashier.MVC.View.PlayerEditDetails.Common
{



  public class PlayerEditDetailsCommonErrorDisplay<T> where T : IMVCModel
  {
   public void CleanUp()
   {
     m_timer.Stop();
     m_timer.Tick -= t_Tick;
     m_error_icon = null;
     m_host_view = null;
     m_lbl_count = null;
     m_lbl_msg = null;
     m_locker =  null;
     m_red_labeles = null;
     m_timer = null;
     m_errors = null;
     m_original_color = Color.Transparent;
   }   
    private  IMVCView<T> m_host_view;
    private  Label m_lbl_count;
    private  Label m_lbl_msg;
    private  object m_locker = new object();
    private  List<Control> m_red_labeles;
    private  Timer m_timer;
    private int m_errorpointer;
    private List<InvalidField> m_errors;
    private Color m_original_color = Color.Transparent;
    private  PictureBox m_error_icon;

    /// <summary>
    ///   contructor
    /// </summary>
    /// <param name="PlayerEditDetailsCashierView1"></param>
    /// <param name="Count"></param>
    /// <param name="Msg"></param>
    /// <param name="ErrorIcon"></param>
    public PlayerEditDetailsCommonErrorDisplay(IMVCView<T> PlayerEditDetailsCashierView1, Label Count,
                                               Label Msg, PictureBox ErrorIcon = null)
    {
      m_red_labeles = new List<Control>();
      m_timer = new Timer();
      m_host_view = PlayerEditDetailsCashierView1;
      m_lbl_count = Count;
      m_lbl_msg = Msg;
      m_error_icon = ErrorIcon;
    }

    /// <summary>
    ///   method to display errors in the target controls specified in the constructor
    ///   method also focuses on the contorl with the first error (dependent on order in validator)
    ///   control is identified by its tag
    /// </summary>
    /// <param name="Errors"></param>
    public void DisplayErrors(List<InvalidField> Errors)
    {
      lock (m_locker)
      {
        m_errors = Errors;

        bool _hit_first = false;
        foreach (var _error in m_errors)
        {
          var _control = FormHelper.FindControlByTag(_error.Id, m_host_view as Form);
          if (_control == null)
          {
            continue;
          }
          if (!_hit_first)
          {
            Control _pcontorl = _control.Parent;
            while (_pcontorl.Parent != null)
            {
              if (_pcontorl.GetType() == typeof (TabPage))
              {
                ((TabControl) _pcontorl.Parent).SelectedTab = (TabPage) _pcontorl;
              }
              _pcontorl = _pcontorl.Parent;
            }
            _control.Focus();
            _hit_first = true;
          }
          var _label_controls = (List<Control>) FormHelper.FindControlsByTag("l" + _control.Tag, m_host_view as Form);
          if (_label_controls.Count == 0)
          {
            continue;
          }
          foreach (var _label_control in _label_controls)
          {
            var _lc = _label_control as Label;
              if (_lc == null)
              {
                  var _rc = _label_control as uc_round_button;
                  if (_rc == null)
                      continue;
                  if (m_original_color == Color.Transparent)
                  {
                      m_original_color = _rc.ForeColor;
                  }
                  _rc.ForeColor = Color.Red;
                  m_red_labeles.Add((Button) _label_control);
                  continue;

              }
              if (m_original_color == Color.Transparent)
            {
              m_original_color = _label_control.ForeColor;
            }
            _label_control.ForeColor = Color.Red;
            m_red_labeles.Add((Label) _label_control);
          }
        }

        m_errorpointer = m_errors.Count;
        t_Tick(null, null);
        m_timer.Interval = 2000;
        m_timer.Tick -= t_Tick;
        m_timer.Tick += t_Tick;
        m_timer.Enabled = true;
        m_timer.Start();
      }
    }

    /// <summary>
    ///   Timer routine to rotate the error messages
    /// </summary>
    /// <param name="Sender"></param>
    /// <param name="E"></param>
    private void t_Tick(object Sender, EventArgs E)
    {
      m_errorpointer++;
      if (m_locker == null) return;
      lock (m_locker)
      {
        if (m_errors != null && m_errors.Count > 0)
        {
          if (m_errorpointer >= m_errors.Count)
          {
            m_errorpointer = 0;
          }
          m_lbl_count.Text = string.Format("({0}/{1})", m_errorpointer + 1, m_errors.Count);
          m_lbl_msg.Text = string.Format("{0}", m_errors[m_errorpointer].Reason);
          if (m_error_icon != null)
          {
            m_error_icon.Visible = true;
          }
          m_lbl_count.Visible = true;
          m_lbl_msg.Visible = true;
        }
      }
    }

    /// <summary>
    ///   method to reset messages and timer
    /// </summary>
    public void StopTimerAndResetLabels()
    {
      if (m_error_icon != null)
      {
        m_error_icon.Visible = false;
      }
      m_lbl_count.Visible = false;
      m_lbl_msg.Visible = false;
      m_timer.Stop();
      foreach (Control _lbl in m_red_labeles)
      {
        _lbl.ForeColor = m_original_color;
      }
    }

  }
}
