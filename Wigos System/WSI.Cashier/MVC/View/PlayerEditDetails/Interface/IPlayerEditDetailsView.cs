//------------------------------------------------------------------------------
// Copyright � 2007-2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: IPlayerEditDetailsView.cs
// 
//   DESCRIPTION: Interface to expose Visible Fields collection to controller
//                
//                
//                
//                
//                
//                
// 
//        AUTHOR: Scott Adamson.
// 
// CREATION DATE: 02-DEC-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 02-DEC-2007 SJA    First release.
//------------------------------------------------------------------------------

using WSI.Cashier.MVC.Interface;

namespace WSI.Cashier.MVC.View.PlayerEditDetails.Interface
{
  public interface IPlayerEditDetailsView<T> : IMVCView<T> where T : IMVCModel
  {
    /// <summary>
    ///   common pin methods exposed from views
    /// </summary>
    new void ErrorUpdatingPin();

    /// <summary>
    ///   common pin methods exposed from views
    /// </summary>
    new void PinChangedOk();

    /// <summary>
    ///   common pin methods exposed from views
    /// </summary>
    new void RandomPinGeneratedOk();
  }
}
