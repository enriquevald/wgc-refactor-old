﻿//------------------------------------------------------------------------------
// Copyright © 2007-2015 Win systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: PlayerEditDetailseReceptionView.cs
// 
//   DESCRIPTION: Implementation of reception view class
//                
//                
//                
//                he
//                
//                
// 
//        AUTHOR: Scott Adamson.
//
// CREATION DATE: 02-DEC-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 02-DEC-2007 SJA    First release.
// 11-FEB-2015 AVZ    Product Backlog Item  9046:Visitas / Recepción: BlackLists
// 10-MAY-2015 JBP    Product Backlog Item 12931:Visitas / Recepción: 2º Ticket LOPD
// 30-MAY-2016 JCA    Fixed Bug 13692:Recepción: error al crear un Nuevo Cliente con la caja cerrada
// 30-MAY-2016 JCA    Fixed BUG 13887 Recepción: El botón Guardar está deshabilitado y no permite guardar un nuevo cliente con todos los campos obligatorios cumplimentados.
// 30-MAY-2016 JBP    Fixed Bug 14002:Recepción: Al registrar la entrada con el GP -> Agreement.ShowMode = 0, imprime dos tickets, uno para cajero y otro para el cliente.
// 31-MAY-2016 JCA    Fixed Bug 14018:MEJORA -> Recepción: Mensaje de filtros en color rojo parece un error. Cambiar mensaje y/o color.
// 08-JUN-2016 JBP    Fixed Bug 14154:Errores de diseño de Recepción
// 26-JUL-2016 PDM    PBI 15444:Visitas / Recepción: MEJORAS - Cajero - Expandir formulario de resultados de búsqueda
// 02-AGO-2016 PDM    Task 16313:Visitas / Recepción - REVIEW - Multiples documentos - cajero
// 20-SEP-2016 JRC    PBI 17677:Visitas / Recepción: MEJORAS II - Ref. 48 - Reportes recepción - separar columna Nombre (GUI)
// 09-NOV-2016 PDM    Fixed Bug 18156:Recepción: Al seleccionar un cliente y luego dar a Crear, hay botones que se tiene que deshabilitar
// 08-DIC-2016 ESE    Bug 18298:Mejora recepción: añadir mensaje mensaje para cliente - caducará hoy el documento
// 21-DIC-2016 XGJ    Al marcar que no hay coincidencias en la blacklist, limpiar mensaje de error
// 10-ENE-2017 DPC    Bug 21943:Recepción: Permite guardar Accounts con el mismo HolderId desde la opción "GUARDAR PIN"
// 17-ENE-2017 DPC    Bug 18298:Mejora recepción: añadir mensaje mensaje para cliente - caducará hoy el documento
// 26-JUL-2017 RGR    Fixed Bug 28998:WIGOS-3606 The cashier ask to the user to save modifications when the user didn't any modification 
// 04-OCT-2017 ETP    Fixed Bug Bug 29878:[WIGOS-5065][WIGOS-4646]: Exception when the user do any modification in the "Account data editor"
// 10-OCT-2017 DPC    [WIGOS-5525]: Reception - Register customer output - Cashier button - Exit logic
// 20-FEB-2018 EOR    Bug 31587: WIGOS-3429 Recepción: Al guardar una cuenta si modificar datos, aparece el popup de guardar cambios 
// 29-MAR-2018 LTC    Bug 32101: WIGOS-8442 Account "Nationality" field appear empty in "Reception" module and populated with value in "Accounts" Cashier section 
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using WSI.Cashier.Controls;
using WSI.Cashier.MVC.Controller.PlayerEditDetails;
using WSI.Cashier.MVC.Model.PlayerEditDetails;
using WSI.Cashier.MVC.View.PlayerEditDetails.Common;
using WSI.Common;
using WSI.Common.Entities.General;
using WSI.Common.Entrances;
using WSI.Common.Entrances.Enums;
using WSI.Common.Utilities.Extensions;


namespace WSI.Cashier.MVC.View.PlayerEditDetails
{
  public partial class PlayerEditDetailsReceptionView : VwBasePlayerEditDetailsReceptionModel
  {
    private const int GRD_CUSTOMER_HEIGHT_NORMAL = 220;
    private const int GRD_CUSTOMER_HEIGHT_EXTENDED = 640;
    private const int GRD_CUSTOMER_WIDTH_NORMAL = 894;

    private const int COLUMN_INDEX_BIRTH_DATE = 4;

    private readonly Point m_location_normal_point = new Point(127, 445);
    private readonly Point m_location_extended_point = new Point(127, 8);

    private readonly Timer m_delay_background_validation_timer = new Timer { Interval = 250, Enabled = false };

    Dictionary<Int32, Object> m_header_images;
    private Int32 m_agreement_enabled;
    private bool m_shown_coments;
    private string m_value_old;

    //private read-only PlayerEditAddressCommon<PlayerEditDetailsReceptionModel> m_address_common;<
    private PlayerEditDetailsCommonErrorDisplay<PlayerEditDetailsReceptionModel> m_error_display;
    private CASHIER_STATUS m_cashier_status;
    private readonly List<string> m_ignored_controls = new List<string>() { "btn_ok", "btn_cancel", "btn_keyboard", "btn_view_list", "btn_new_customer",
                                                                            "btn_save_pin", "btn_generate_random_pin" ,"btn_card_functions","btn_scan_doc","lbl_blacklist_messages"};

    private readonly List<string> m_visibility_controls = new List<string>()
      {
        "pb_blacklist_status",
        "pb_record_expiration_status",
        "lbl_text_record_expiration_date",
        "lbl_record_expiration_date",
      };


    #region Properties

    /// <summary>
    ///   address container
    /// </summary>
    public Zip.Address AddressModified { set; get; }

    public Boolean IsResultsCustomersExtended
    {
      get
      {
        return grd_customers.Size.Height > GRD_CUSTOMER_HEIGHT_NORMAL;
      }
    }

    public Int32 ViewTopResults
    {
      get
      {
        return GeneralParam.GetBoolean("Reception", "ResultsView.Mode", false) ? ViewExtendedTopResults :
                                                                                 ViewCollapsedTopResults;
      }
    }

    private Int32 ViewExtendedTopResults
    {
      get
      {
        return GeneralParam.GetInt32("Reception", "ResultsView.Extended.TopResults", 50, 1, 100);
      }

    }

    private Int32 ViewCollapsedTopResults
    {
      get
      {
        return GeneralParam.GetInt32("Reception", "ResultsView.Collapsed.TopResults", 4, 1, 100);
      }
    }

    #endregion

    //  set { txt_last_name2 = value; }
    //{

    //public CharacterTextBox TxtLastName2
    //}
    //  get { return txt_last_name1; }
    //  set { txt_last_name1 = value; }
    //{

    //public CharacterTextBox TxtLastName1

    //public CharacterTextBox TxtName4

    //{

    #region Private Methods
    private void ShowHolderHappyBirthDay(HolderHappyBirthDayNotification HolderBirthDayNotification)
    {
      //if (m_model.HolderIsVip)
      //{
      //  tlpVipAndHappyBirthDay.SetColumn(lbl_happy_birthday, 2);
      //}
      //else
      //{
      //  tlpVipAndHappyBirthDay.SetColumn(lbl_happy_birthday, 1);
      //}

      switch (HolderBirthDayNotification)
      {
        case HolderHappyBirthDayNotification.HAPPY_BIRTHDAY_INCOMMING:
          lbl_happy_birthday.Text = Resource.String("STR_UC_PLAYER_TRACK_BIRTHDAY_IN_NEXT_WEEK");
          lbl_happy_birthday.Visible = true;

          break;

        case HolderHappyBirthDayNotification.HAPPY_BIRTHDAY_TODAY:
          lbl_happy_birthday.Text = Resource.String("STR_UC_PLAYER_TRACK_HAPPY_BIRTHDAY");
          lbl_happy_birthday.Visible = true;
          break;

        default:
          lbl_happy_birthday.Visible = false;

          break;
      }
      //
    }


    /// <summary>
    ///   populate the combo boxes
    /// </summary>
    private void PopulateComboBoxes()
    {
      PlayerEditDetailsReceptionController.BindDatasourceToCombo(m_model.Nationalities, cb_nationality);
      PlayerEditDetailsReceptionController.BindDatasourceToCombo(m_model.DocumentTypes, cb_document_type);
      PlayerEditDetailsReceptionController.BindDatasourceToCombo(m_model.Genders, cb_gender);
      PlayerEditDetailsReceptionController.BindDatasourceToCombo(m_model.Countries, cb_birth_country);
      PlayerEditDetailsCashierController.BindDatasourceToCombo(m_model.MaritalStatuses, cb_marital);

    } //PopulateComboBoxes

    /// <summary>
    ///   set the form title
    /// </summary>
    private void SetFormTitle()
    {
      if (m_controller == null)
      {
        return;
      }
      FormTitle = ((PlayerEditDetailsReceptionController)m_controller).SetFormTitle(m_model);
    } //SetFormTitle

    /// <summary>
    ///   populate fields on view from model
    /// </summary>
    public override void ModelToView()
    {
      DateTime _date_expiration;
      Boolean? _is_in_internal_black_list;

      // LTC 2018-MAR-29
      if (m_model.HolderNationality == null || String.IsNullOrEmpty(m_model.HolderNationality.Name))
      {
        SelectDefaultCountryFromGeneralParam("Account.DefaultValues", "Country");
      }
      else
      {
        cb_nationality.SelectedItem = m_model.HolderNationality;
      }

      txt_pin.Text = m_model.Pin;
      txt_confirm_pin.Text = m_model.Pin;

      AlternativeAddress.HouseNumber = m_model.HolderAlternativeHouseNumber;
      AlternativeAddress.AddressLine1 = m_model.HolderAlternativeAddressLine1;
      AlternativeAddress.AddressLine2 = m_model.HolderAlternativeAddressLine2;
      AlternativeAddress.AddressLine3 = m_model.HolderAlternativeAddressLine3;
      AlternativeAddress.PostCode = m_model.HolderAlternativePostCode;
      AlternativeAddress.Town = m_model.HolderAlternativeTown;
  


      //HolderAddress.HolderAddressValidation = m_model.HolderAddressValidation;
      HolderAddress.HouseNumber = m_model.HolderExtNum;
      HolderAddress.AddressLine1 = m_model.HolderAddressLine01;
      HolderAddress.AddressLine2 = m_model.HolderAddressLine02;
      HolderAddress.AddressLine3 = m_model.HolderAddressLine03;
      HolderAddress.PostCode = m_model.HolderZip;
      HolderAddress.Town = m_model.HolderCity;
     
                       
      lbl_track_number.Text = m_model.VisibleTrackData;
      btn_card_functions.Enabled = CashierBusinessLogic.ReceptionButtonsEnabledCashierSessionOpenAutomatically(m_cashier_status) &&
        m_model.AccountId > 1;

      btn_card_functions.Text = string.IsNullOrEmpty(m_model.VisibleTrackData) ||
                                m_model.VisibleTrackData.Contains("----")
        ? Resource.String("BTN_CARD_FUNC_ASSIGN")
        : Resource.String("BTN_CARD_FUNC_RECYCLE");
      
m_namesdictionsry.Clear();
      

      m_namesdictionsry.Add("txt_name", m_model.HolderName3);
      m_namesdictionsry.Add("txt_name4", m_model.HolderName4);
      m_namesdictionsry.Add("txt_last_name1", m_model.HolderName1);
      m_namesdictionsry.Add("txt_last_name2", m_model.HolderName2);



      txt_name.Text = m_model.HolderName3;
      txt_name4.Text = m_model.HolderName4;
      txt_last_name1.Text = m_model.HolderName1;
      txt_last_name2.Text = m_model.HolderName2;
      uc_dt_birth.DateValue = m_model.HolderBirthDate ?? default(DateTime);
      SetAge(m_model.HolderBirthDate ?? default(DateTime));
      ShowHolderHappyBirthDay(m_model.HolderBirthDayDateShowNotification);
      txt_email_01.Text = m_model.HolderEmail1;
      txt_phone_01.Text = m_model.HolderPhone1;
      txt_phone_02.Text = m_model.HolderPhone2;
      txt_twitter.Text = m_model.HolderTwitterAccount;
      txt_facebook.Text = m_model.Facebook;
      txt_comments.Text = m_model.Comments;
      chk_show_comments.Checked = m_model.ShowComments;
      m_documents.Clear();
      int _docvalues = 0;
      if (m_model.IDDocuments != null)
        {
        foreach (var _v in m_model.IDDocuments)
          {
          m_documents.Add(_v.Key, _v.Value);
          if (!string.IsNullOrEmpty(_v.Value))
          {
            _docvalues++;
          }
          lbl_type_document.Text = Resource.String("STR_FRM_PLAYER_EDIT_TYPE_DOCUMENT");
        }
      }

     
      txt_document.Text = m_model.HolderDocument;

      int _i = lbl_type_document.Text.IndexOf(" (", StringComparison.Ordinal);
      if (_i < 1)
        _i = lbl_type_document.Text.Length;
      lbl_type_document.Text = lbl_type_document.Text.Substring(0, _i);

      if (_docvalues > 1)
      {
        lbl_type_document.Text += string.Format("({0})", _docvalues);
      }

      lbl_type_doc_count.ForeColor = Color.Green;

      UpdateDocTypeDescriptions();
      m_agreement_enabled = GeneralParam.GetInt32("Reception", "Agreement.ShowMode");

      if (m_model.ShowComments && !m_shown_coments)
      {
        m_shown_coments = true;
        string _error_str;
        tab_player_edit.SelectTab("comments");
        SetContentForComments(
          ProfilePermissions.CheckPermissionList(
            ProfilePermissions.CashierFormFuncionality.EditCommentsRequiredReading,
            ProfilePermissions.TypeOperation.OnlyCheck, ParentForm, 0, out _error_str));
      }
      lbl_full_name.Text = m_model.HolderFullName;
      txt_name_TextChanged(null, null);
      cb_birth_country.SelectedItem = m_model.HolderBirthCountry;
      lbl_level_data.Text = m_model.CardLevelName.IsNullOrWhiteSpace() ? "---" : m_model.CardLevelName;
      lbl_vip_account.Visible = m_model.HolderIsVip;
      chk_data_agreement.Checked = m_model.HasAceptedAgreement;

      if (!btn_ok.Enabled && m_model.AccountId < 1)
      {
        btn_ok.Enabled = true;
      }

      // Only enabled in old accounts
      chk_data_agreement.Visible = m_agreement_enabled != 0;
      chk_data_agreement.Enabled = (m_agreement_enabled != 0 && !Entrance.IsNewCustomer(m_model.AccountId));

      btn_update_record_expiration.Enabled = CashierBusinessLogic.ReceptionButtonsEnabledCashierSessionOpenAutomatically(m_cashier_status) &&
                                             m_model.RecordExpirationDate.HasValue;
      lbl_text_record_expiration_date.Text = m_model.RecordExpirationDate.HasValue
        ? m_model.RecordExpirationDate.Value.ToString(
          System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern)
        : Customers.GetDefaultExpirationDate()
          .ToString(System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern);

      switch (m_model.ExpiredDocumentsResult)
      {
        case ExpiredDocumentsResult.HasExpiredDocuments:
        {
          pb_record_expiration_status.Image = Resources.ResourceImages.warning_big;
          lbl_expiration_date.Text = Resource.String("STR_FRM_PLAYER_EDIT_EXPIRATION_DATE");
          lbl_expiration_date.Visible = true;
          break;
        }

        case ExpiredDocumentsResult.DoesNotHaveExpiredDocuments:
        {
          pb_record_expiration_status.Image = Resources.ResourceImages.ok_big;
            lbl_record.ForeColor = Color.FromArgb(51, 51, 51);
            lbl_record_expiration_date.ForeColor = Color.FromArgb(51, 51, 51);
            lbl_text_record_expiration_date.ForeColor = Color.FromArgb(51, 51, 51);
          break;
        }
        case ExpiredDocumentsResult.Processing:
        {
          pb_record_expiration_status.Image = Resources.ResourceImages.loading_gif;
          
          if (DateTime.TryParse(lbl_text_record_expiration_date.Text, out _date_expiration))
          {
            if (_date_expiration.Date.Subtract(DateTime.Now.Date).Days > -1 && _date_expiration.Date.Subtract(DateTime.Now.Date).Days < 2)
            {
              pb_record_expiration_status.Image = Resources.ResourceImages.warning_big;
              lbl_expiration_date.Text = Resource.String("STR_FRM_PLAYER_EDIT_EXPIRATION_DATE");
              lbl_expiration_date.Visible = true;
            }
          }
          break;
        }
        case ExpiredDocumentsResult.Error:
        {
          pb_record_expiration_status.Image = Resources.ResourceImages.warning_big;
          break;
        }
      }

      _is_in_internal_black_list = IsInInternalBlackList(m_model);

      btn_add_to_blacklist.Enabled = CashierBusinessLogic.ReceptionButtonsEnabledCashierSessionOpenAutomatically(m_cashier_status) &&
                                     m_model.AccountId > 0 &&
                                     _is_in_internal_black_list == false;

      btn_remove_from_blacklist.Enabled = CashierBusinessLogic.ReceptionButtonsEnabledCashierSessionOpenAutomatically(m_cashier_status) &&
                                          _is_in_internal_black_list == true;


      btn_show_matches.Enabled = IsMatchesButtonVisisble(m_model.BlackListResultCustomer);
      
	    lbl_blacklist_messages.Text = string.Empty;
	    lbl_blacklist_messages.Visible = false;
			chk_acknowledged_blacklist.Visible = false;
      m_model.BlackListEntryBlocked = false;
      switch (m_model.BlackListResult)
      {
        case BlackListResult.InBlackList:
        {
          pb_blacklist_status.Image = Resources.ResourceImages.warning_big;
          pb_blacklist_data.Image = Resources.ResourceImages.warning_big;
          lbl_blacklist_data.Text = Resource.String("STR_FRM_RECEPTION_BLACKLIST_INBLACKLIST");
          if (m_model.BlackListCustomerSelected == null && m_model.BlackListResultCustomer.Length == 1)
            m_model.BlackListCustomerSelected = m_model.BlackListResultCustomer;
	        lbl_blacklist_messages.Text = GenerateBlackListMessages(m_model.BlackListCustomerSelected);
          m_model.BlackListEntryBlocked = false;
            foreach (var _blacklist_row in m_model.BlackListCustomerSelected)
          {
            if (_blacklist_row.NotPermitedToEnter) m_model.BlackListEntryBlocked = true;
          }
          lbl_blacklist_messages.Visible = true;
					chk_acknowledged_blacklist.Visible = true;
          break;
        }
        case BlackListResult.WithMatches:
        {
          lbl_blacklist_messages.Text = GenerateBlackListMessages(m_model.BlackListCustomerSelected);
          pb_blacklist_status.Image = Resources.ResourceImages.warning_big;
          pb_blacklist_data.Image = Resources.ResourceImages.warning_big;
          lbl_blacklist_data.Text = Resource.String("STR_FRM_RECEPTION_BLACKLIST_WITHMATCHES");
          break;
        }
        case BlackListResult.NotInBlackList:
        {
          pb_blacklist_status.Image = Resources.ResourceImages.ok_big;
          pb_blacklist_data.Image = Resources.ResourceImages.ok_big;
          lbl_blacklist_data.Text = Resource.String("STR_FRM_RECEPTION_BLACKLIST_NOTINBLACKLIST");
          break;
        }
        case BlackListResult.Processing:
        {
          pb_blacklist_status.Image = Resources.ResourceImages.loading_gif;
          pb_blacklist_data.Image = Resources.ResourceImages.loading_gif;
          lbl_blacklist_data.Text = Resource.String("STR_FRM_RECEPTION_BLACKLIST_PROCESSING");
          break;
        }
        case BlackListResult.Error:
        {
          pb_blacklist_status.Image = Resources.ResourceImages.warning_big;
          pb_blacklist_data.Image = Resources.ResourceImages.warning_big;
          lbl_blacklist_data.Text = Resource.String("STR_FRM_RECEPTION_BLACKLIST_ERROR");
          chk_acknowledged_blacklist.Visible = true; 
          break;
        }
        case BlackListResult.None:
        {
          pb_blacklist_status.Image = Resources.ResourceImages.warning_big;
          pb_blacklist_data.Image = Resources.ResourceImages.warning_big;
          lbl_blacklist_data.Text = Resource.String("STR_FRM_RECEPTION_BLACKLIST_NONE");
          break;
        }
      }
      grd_customers.DataSource = m_model.GridData;
      m_img_photo.Image = m_model.HolderPhoto;
      grd_scanned_docs.SelectionChanged -= grd_scanned_docs_SelectionChanged;
      grd_scanned_docs.DataSource = m_model.ScannedDocuments;

      if (m_model.ScannedDocuments != null && m_model.ScannedDocuments.Count > 0)
      {
        grd_scanned_docs.Rows[0].Selected = false;
      }
      grd_scanned_docs.SelectionChanged += grd_scanned_docs_SelectionChanged;
      if (m_model.ScannedDocuments != null && m_model.ScannedDocuments.Count > 0)
      {
        grd_scanned_docs.Rows[0].Selected = true;
      }

      if (m_model.NoMoreResults)
      {
        btn_view_list.Enabled = false;
      }
      //else
      //{
      //  btn_view_list.Enabled = ((!m_model.AccountId.HasValue) || (m_model.AccountId.HasValue && m_model.AccountId > 1));
      //}

      btn_entry.Enabled = CashierBusinessLogic.ReceptionButtonsEnabledCashierSessionOpenAutomatically(m_cashier_status) &&
                          (m_model.AccountId.HasValue || m_model.AccountId < 1);
      
      SetFormTitle();

      if (tab_address.TabPages.Count == 2 && !tab_address.TabPages[1].Enabled)
      {
        var _ctrl = tab_address.TabPages[0].Controls[0];
        tab_address.TabPages[0].Controls.Remove(_ctrl);
        _ctrl.Parent = tab_player_edit.TabPages["address"];
        _ctrl.Location = new Point(15, 30);
        tab_player_edit.TabPages["address"].Controls.Add(_ctrl);

        tab_player_edit.TabPages["address"].Controls.RemoveByKey("tab_address");
      }
      //HolderAddress.UpdateAddressValidation();
      //}
      //catch (Exception _sweep_it_under_the_carpet)
      //{
      //}
      bool _enabledscan = ((PlayerEditDetailsReceptionController)m_controller).ScannerEnabled &&
                          (
                            !Misc.IsReceptionEnabled() || (m_model.AccountId.HasValue && m_model.AccountId < 1)
                            );
      btn_id_card_scan.Visible = _enabledscan;
      btn_id_card_scan.Enabled = _enabledscan && (CashierBusinessLogic.ReceptionButtonsEnabledCashierSessionOpenAutomatically(m_cashier_status));
      btn_generate_random_pin.Enabled = m_model.AccountId.HasValue;
      chk_acknowledged_blacklist.Checked = m_model.AcknowledgedBlackList;

      if (m_model.HolderDocumentType == null || String.IsNullOrEmpty(m_model.HolderDocumentType.Name))
      {
        SelectDefaultDocumentTypeFromGeneralParam("Account.DefaultValues", "DocumentType");
      }
      else
      {
        cb_document_type.Set<DocumentType>(m_model.HolderDocumentType, "DocumentType");
      }
           
      cb_gender.Set<Gender>(m_model.HolderGender);
      cb_birth_country.Set<Country>(m_model.HolderBirthCountry, m_model.IsNew ? "Country" : null);  //EOR 20-FEB-2018
      cb_nationality.Set<Nationality>(m_model.HolderNationality, m_model.IsNew ? "Country": null);  //EOR 20-FEB-2018
      cb_marital.Set<MaritalStatus>(m_model.HolderMaritalStatus);
     
      HolderAddress.SetComboCountryValue<Country>(m_model.HolderAddressCountry, m_model.IsNew ? "Country" : null); //EOR 20-FEB-2018
      HolderAddress.SetComboFederalEntityValue<State>(m_model.HolderFedEntity);

      AlternativeAddress.SetComboCountryValue<Country>(m_model.HolderAlternativeCountry, m_model.IsNew ? "Country" : null);  //EOR 20-FEB-2018
      AlternativeAddress.SetComboFederalEntityValue<State>(m_model.HolderAlternativeState);

    }

    /// <summary>
    /// Selects a default document type from a General Param
    /// </summary>
    /// <param name="GroupKey"></param>
    /// <param name="SubjectKey"></param>
    private void SelectDefaultDocumentTypeFromGeneralParam(String GroupKey, String SubjectKey)
    {
      int _default_doc;
      DocumentType _selected_document_type;

      _default_doc = GeneralParam.GetInt32(GroupKey, SubjectKey);
      _selected_document_type = null;
      // Select the default document type.
      foreach (DocumentType _doc_type in cb_document_type.Items)
      {
        if (_doc_type.Id == _default_doc)
          _selected_document_type = _doc_type;
      }

      if (_selected_document_type != null)
      {
        cb_document_type.SelectedItem = _selected_document_type;
        return;
      }
      // If not selected, select the first one.
      cb_document_type.SelectedIndex = 0;
    }

    private void UpdateDocTypeDescriptions()
    {
      int _docvalues = 0;

      foreach (var _doctype in cb_document_type.Items)
      {

        var _doc_type = (_doctype as DocumentType);
        if (_doc_type != null && _doc_type.Name.Length > 0)
        {
          if (!m_documents.ContainsKey(_doc_type.Id))
          {
            m_documents.Add(_doc_type.Id, "");
      }
          if (_doc_type.Name.Contains(" ("))
            _doc_type.Name = _doc_type.Name.Substring(0, _doc_type.Name.IndexOf(" (", StringComparison.Ordinal));
          if (cb_document_type.SelectedItem != _doc_type)
          {
            if (m_documents.ContainsKey(_doc_type.Id) &&  m_documents[_doc_type.Id] != null && m_documents[_doc_type.Id].Length > 0)
            {
              _doc_type.Name = _doc_type.Name.Trim() + " (" + m_documents[_doc_type.Id] + ")";
              _docvalues++;
            }
          }
          else
          {
            cb_document_type.Text = _doc_type.Name;
            txt_document.Text = m_documents[_doc_type.Id];
            if (!string.IsNullOrEmpty(m_documents[_doc_type.Id]))
            {
              _docvalues++;
            }
          }
        }
        else
        {
          txt_document.Text = "";
        }
      }

      int _i = lbl_type_document.Text.IndexOf(" (", StringComparison.Ordinal);
      if (_i < 1)
        _i = lbl_type_document.Text.Length;
      lbl_type_document.Text = lbl_type_document.Text.Substring(0, _i);

      if (_docvalues > 1)
      {
        lbl_type_document.Text += string.Format(" ({0})", _docvalues);
      }

      if (_docvalues > 0)
      {
        lbl_type_doc_count.Text = String.Format("x{0}", _docvalues.ToString());
      }
      else
      {
        lbl_type_doc_count.Text = String.Empty;
      }

    }

    /// <summary>
    /// Check if player is in internal blacklist
    /// </summary>
    /// <returns>boolean? (true;false;null)</returns>
    readonly Dictionary<int, string> m_documents = new Dictionary<int, string>();

    private bool? IsInInternalBlackList(PlayerEditDetailsReceptionModel Mdl)
    {
      switch (Mdl.BlackListResult)
      {
        case BlackListResult.NotInBlackList:

          return false;

        case BlackListResult.InBlackList:
        case BlackListResult.WithMatches:

      foreach (var _black_list_result_customer in Mdl.BlackListResultCustomer)
      {
        if (_black_list_result_customer.Source == ENUM_BLACKLIST_SOURCE.LOCAL)

          return true;
      }

      return false;
    }

      return null;
    } // IsInInternalBlackList

    private string GenerateBlackListMessages(BlackListResultCustomer[] Blrc)
    {
      string _retval = string.Empty;
      if (Blrc == null)
        return _retval;

      foreach (var _black_list_result_customer in Blrc)
      {
        _retval +=
          string.Format("{1} - {2} ({0:dd/MM/yyyy})", _black_list_result_customer.InclusionDate,
            _black_list_result_customer.Source.GetDescriptionResource(),
            _black_list_result_customer.ReasonType.GetDescriptionResource()) + Environment.NewLine;
        if (_black_list_result_customer.Source != ENUM_BLACKLIST_SOURCE.LOCAL)
          for (int _i = 0; _i < _black_list_result_customer.Lists.Length; _i++)
          {

            _retval += "    " + _black_list_result_customer.Lists[_i] + Environment.NewLine + "        " +
                       _black_list_result_customer.Messages[_i] + Environment.NewLine;
          }
        _retval += _black_list_result_customer.ReasonDescription + Environment.NewLine + Environment.NewLine;
      }
      return _retval;


    }

    private bool IsMatchesButtonVisisble(BlackListResultCustomer[] Blrc)
    {
      if (Blrc == null) return false;
      if (Blrc.Length > 1) return true;
      return Blrc.Length == 1 && Blrc[0].Source != ENUM_BLACKLIST_SOURCE.LOCAL;
    }


    /// <summary>
    ///   recover values from view
    /// </summary>
    public override void ViewToModel()
    {
      //save view to data
      m_model.Pin = txt_pin.Text;

      //m_model.HolderAddressValidation = HolderAddress.HolderAddressValidation;
      m_model.HolderExtNum = HolderAddress.HouseNumber;
      m_model.HolderAddressLine01 = HolderAddress.AddressLine1;
      m_model.HolderAddressLine02 = HolderAddress.AddressLine2;
      m_model.HolderAddressLine03 = HolderAddress.AddressLine3;
      m_model.HolderZip = HolderAddress.PostCode;
      m_model.HolderCity = HolderAddress.Town;
      m_model.HolderAddressCountry = HolderAddress.Country as Country;
      m_model.HolderFedEntity = HolderAddress.State as State;

      m_model.HolderAlternativeHouseNumber = AlternativeAddress.HouseNumber;
      m_model.HolderAlternativeAddressLine1 = AlternativeAddress.AddressLine1;
      m_model.HolderAlternativeAddressLine2 = AlternativeAddress.AddressLine2;
      m_model.HolderAlternativeAddressLine3 = AlternativeAddress.AddressLine3;
      m_model.HolderAlternativePostCode = AlternativeAddress.PostCode;
      m_model.HolderAlternativeTown = AlternativeAddress.Town;
      m_model.HolderAlternativeCountry = AlternativeAddress.Country as Country;
      m_model.HolderAlternativeState = AlternativeAddress.State as State;
      m_model.HolderMaritalStatus = (MaritalStatus)cb_marital.SelectedItem;

      m_model.HolderGender = (Gender)cb_gender.SelectedItem;
      m_model.HolderNationality = (Nationality)cb_nationality.SelectedItem;
      m_model.HolderName1 = txt_last_name1.Text;
      m_model.HolderName2 = txt_last_name2.Text;
      m_model.HolderName3 = txt_name.Text;
      m_model.HolderName4 = txt_name4.Text;
      m_model.HolderBirthDate = uc_dt_birth.DateValue;
      m_model.HolderBirthCountry = (Country)cb_birth_country.SelectedItem;
      m_model.HolderEmail1 = txt_email_01.Text;
      m_model.HolderPhone1 = txt_phone_01.Text;
      m_model.HolderPhone2 = txt_phone_02.Text;
      m_model.HolderTwitterAccount = txt_twitter.Text;
      m_model.Facebook = txt_facebook.Text;
      m_model.HolderDocumentType = (DocumentType)cb_document_type.SelectedItem;
      m_model.HolderDocument = txt_document.Text;
      m_model.Comments = txt_comments.Text;
      m_model.ShowComments = chk_show_comments.Checked;
      m_model.HolderFullName = lbl_full_name.Text;
      m_model.HolderPhoto = m_img_photo.Image;
      m_model.AcknowledgedBlackList = chk_acknowledged_blacklist.Checked;
      if (m_model.IDDocuments == null) m_model.IDDocuments = new Dictionary<int, string>();
      m_model.IDDocuments.Clear();
      foreach (var _v in m_documents)
      {
        m_model.IDDocuments.Add(_v.Key, _v.Value);
      }

      // AceptedAgreementDate (Update only if is Acepted and AceptedAgreementDate have default value) 
      if (chk_data_agreement.Checked)
      {
        if (m_model.AceptedAgreementDate == default(DateTime))
        {
          m_model.AceptedAgreementDate = WGDB.Now;
        }
        // else no change value.
      }
      else
      {
        m_model.AceptedAgreementDate = DateTime.MinValue;
      }

      m_model.HasAceptedAgreement = chk_data_agreement.Checked;

    } //ViewToModel


    /// <summary>
    ///   initialize all the forms controls
    /// </summary>
    private void InitContols()
    {
      this.ShowCloseButton = false;
			chk_acknowledged_blacklist.Text = Resource.String("STR_FRM_CHK_BLACKLIST_READ");
      chk_show_comments.Location = new Point(14, 331);
      pb_scanned_photo.Size = new Size(208, 288);
      lbl_name.Text = AccountFields.GetName();
      lbl_last_name1.Text = AccountFields.GetSurname1();//Resource.String("STR_FRM_PLAYER_EDIT_LAST_NAME1");
      lbl_last_name2.Text = AccountFields.GetSurname2();//Resource.String("STR_FRM_PLAYER_EDIT_LAST_NAME2");
      lbl_facebook.Text = Resource.String("STR_FRM_PLAYER_EDIT_FACEBOOK");
      lbl_twitter.Text = Resource.String("STR_FRM_PLAYER_EDIT_TWITTER");

      lbl_name4.Text = AccountFields.GetMiddleName();//Resource.String("STR_FRM_PLAYER_EDIT_NAME4");

      lbl_type_document.Text = Resource.String("STR_FRM_PLAYER_EDIT_TYPE_DOCUMENT");
      lbl_type_doc_count.Text = String.Empty;

      lbl_document.Text = Resource.String("STR_FRM_PLAYER_EDIT_DOCUMENT");
      lbl_nationality.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_NATIONALITY");
      lbl_record_expiration_date.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_EXPIRATION");
      btn_scan_doc.Text = Resource.String("STR_FRM_SCAN_SCAN");
      btn_image_black_list.Image = Images.Get32x32(Images.CashierImage.Recycle);
      btn_copy_adddress.Text = Resource.String("STR_FRM_PLAYER_EDIT_COPY_ADDRESS");
      pb_record_expiration_status.Image = Resources.ResourceImages.ok_big;
      btn_add_to_blacklist.Text = Resource.String("STR_FRM_RECEPTION_ADD_TO_BLACKLIST");
      btn_remove_from_blacklist.Text = Resource.String("STR_FRM_RECEPTION_REMOVE_FROM_BLACKLIST");
      lbl_marital_status.Text = Resource.String("STR_UC_PLAYER_TRACK_MARITAL_STATUS");
      lbl_email_01.Text = Resource.String("STR_UC_PLAYER_TRACK_EMAIL_01");
      lbl_phone_01.Text = Resource.String("STR_UC_PLAYER_TRACK_HOME_PHONE");
      lbl_phone_02.Text = Resource.String("STR_UC_PLAYER_TRACK_MOBILE_PHONE");
      lbl_gender.Text = Resource.String("STR_UC_PLAYER_TRACK_GENDER");
      lbl_birth_date.Text = Resource.String("STR_FRM_PLAYER_EDIT_BIRTH_DATE");
      lbl_birth_country.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_BIRTH_COUNTRY");
      //FBA 16-SEP-2013

      tab_player_edit.TabPages["principal"].Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_TAB_MAIN");
      tab_player_edit.TabPages["address"].Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_TAB_ADDRESS");
      tab_player_edit.TabPages["contact"].Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_TAB_CONTACT");
      tab_player_edit.TabPages["comments"].Text = Resource.String("STR_UC_PLAYER_TRACK_COMMENTS");
      tab_player_edit.TabPages["scanned_docs"].Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_TAB_SCANNED_DOCS");
      tab_player_edit.TabPages["pin"].Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_PIN");
      tab_player_edit.TabPages["blacklist"].Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_TAB_BLACKLIST");
      tab_address.TabPages["home_address"].Text = string.Format("{0}{1}", Resource.String("STR_FRM_ACCOUNT_USER_EDIT_TAB_ADDRESS_HOME"), " ");
      tab_address.TabPages["alt_address"].Text = string.Format("{0}{1}",
        Resource.String("STR_FRM_ACCOUNT_USER_EDIT_TAB_ADDRESS_ALT"), " ");
      lbl_pin.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_PIN");
      lbl_confirm_pin.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_CONFIRM_PIN");


      txt_pin.MaxLength = Accounts.PIN_MAX_SAVE_LENGTH;
      txt_confirm_pin.MaxLength = Accounts.PIN_MAX_SAVE_LENGTH;

      btn_save_pin.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_SAVE_PIN");
      btn_generate_random_pin.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_GENERATE_RANDOM_PIN");

      lbl_track.Text = Resource.String("STR_FORMAT_GENERAL_NAME_CARD");
      //   - Buttons


      btn_ok.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_SAVE");
      btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CLOSE");
      btn_new_customer.Text = Resource.String("STR_FRM_SEARCH_CUSTOMER_ADD");
      btn_entry.Text = Resource.String("STR_FRM_SEARCH_RECEPTION_ENTRY");
      btn_exit.Text = Resource.String("STR_FRM_SEARCH_RECEPTION_EXIT");
      btn_view_list.Text = Resource.String("STR_FRM_SEARCH_RECEPTION_MORE_RESULTS");
      btn_update_record_expiration.Text = Resource.String("STR_FRM_RECEPTION_UPDATE_RECORD_EXPIRATION");
      btn_show_matches.Text = Resource.String("STR_FRM_RECEPTION_SHOW_MATCHES");

      chk_show_comments.Text = Resource.String("STR_FRM_PLAYER_EDIT_SHOW_COMMENTS");

      // Only enabled in old accounts
      chk_data_agreement.Enabled = (m_agreement_enabled != 0 && !Entrance.IsNewCustomer(m_model.AccountId));
      chk_data_agreement.Text = GeneralParam.GetString("Reception", "Agreement.CashierTex");

      lbl_record.Text = Resource.String("STR_FRM_CUSTOMER_RECEPTION_RECORD_STATE");
			lbl_black_list.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_TAB_BLACKLIST"); 
      //btn_id_card_scan.Visible =  false;
      btn_card_functions.Visible = ((PlayerEditDetailsReceptionController)m_controller).ReceptionCardFunctionsPermitted;

      lbl_vip_account.Text = Resource.String("STR_VIP_ACCOUNT");
      lbl_level.Text = Resource.String("STR_UC_CARD_POINTS_LEVEL_TITLE");

      //this.grd_customers.DataSource = m_model.GridData;
      m_delay_background_validation_timer.Tick += (Sender, Args) =>
      {
        m_delay_background_validation_timer.Enabled = false;
        var _errors = ((PlayerEditDetailsReceptionController)m_controller).Validate(m_model);

        // Bug 14018:MEJORA -> Recepción: Mensaje de filtros en color rojo parece un error. Cambiar mensaje y/o color.
        // Se quita el mensaje ya que no es de tipo "error" y previamente se ha mostrado un popup
        //if (m_model.GridData.Count > 1)
        //{
        //  _errors.Add(new InvalidField
        //    {
        //      Id = "GridCustomers",
        //      Reason = Resource.String("STR_FRM_CUSTOMER_RECEPTION_QUERY_MORE_THAN_ONE_ACCOUNT")
        //    });
        //}

        m_error_display.DisplayErrors(_errors);
        if (m_model.AccountId.HasValue)
        {
          ((PlayerEditDetailsReceptionController)m_controller).DelayedAccountPhotoLoad(m_model.AccountId.Value);
        }
      };


      btn_new_customer.Enabled = CashierBusinessLogic.ReceptionButtonsEnabledCashierSessionOpenAutomatically(m_cashier_status);

      btn_save_pin.Enabled = CashierBusinessLogic.ReceptionButtonsEnabledCashierSessionOpenAutomatically(m_cashier_status) && (m_model.AccountId.HasValue && m_model.AccountId > 1);
      btn_generate_random_pin.Enabled = CashierBusinessLogic.ReceptionButtonsEnabledCashierSessionOpenAutomatically(m_cashier_status) && (m_model.AccountId.HasValue && m_model.AccountId > 1);
      btn_scan_doc.Enabled = CashierBusinessLogic.ReceptionButtonsEnabledCashierSessionOpenAutomatically(m_cashier_status);
    }//InitContolsbtn_generate_random_pin



    /// <summary>
    ///   prepare form for comments only view
    /// </summary>
    /// <param name="IsEditable"></param>
    private void SetContentForComments(bool IsEditable)
    {
      chk_show_comments.Enabled = IsEditable;

      btn_ok.Visible = IsEditable;
      btn_keyboard.Visible = IsEditable;
      if (!IsEditable)
      {
        btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CLOSE");
      }
      txt_comments.ReadOnly = !IsEditable;
    } // SetContentForComments

    // HideTabs


    /// <summary>
    ///   save changes on form and optionally perform entry registration
    /// </summary>
    /// <param name="Sender"></param>
    /// <param name="E"></param>
    private void btn_ok_Click(object Sender, EventArgs E)
    {
      List<InvalidField> _errors;

      if (!ManagementCashierSessionOpenMessage())
      {
        return;
      }

      m_error_display.StopTimerAndResetLabels();

      _errors = ((PlayerEditDetailsReceptionController)m_controller).SavePlayer();
      if (_errors.Count > 0)
      {
        if (_errors[0].Id == "ContinueAccountsDuplicate")
      {
          _errors.Clear();
      }
      }

      if (_errors.Count == 0)
      {
        var _dlg_rc = frm_message.Show(Resource.String("STR_FRM_CUSTOMER_RECEPTION_REGISTRY_ENTRY"),
          Resource.String("STR_FRM_CUSTOMER_RECEPTION_SAVE_ENTRY_TITLE"), MessageBoxButtons.YesNo,
          Images.CashierImage.Question, this);
        if (_dlg_rc == DialogResult.OK)
        {
          btn_entry.PerformClick();
        }
        return;
      }

      m_error_display.DisplayErrors(_errors);
    } //btn_ok_Click

    /// <summary>
    ///   toggle the on screen keyboard
    /// </summary>
    /// <param name="Sender"></param>
    /// <param name="E"></param>
    private void btn_keyboard_Click(object Sender, EventArgs E)
    {

      ((PlayerEditDetailsReceptionController)m_controller).ToggleKeyboard();
    } //btn_keyboard_Click

    /// <summary>
    ///   cancel and close the form, check for changes first
    /// </summary>
    /// <param name="Sender"></param>
    /// <param name="E"></param>
    private void btn_cancel_Click(object Sender, EventArgs E)
    {
      Close();
    } //btn_cancel_Click

    /// <summary>
    ///   assign events on form show
    /// </summary>
    /// <param name="Sender"></param>
    /// <param name="E"></param>
    private void PlayerEditDetailsReceptionView_Shown(object Sender, EventArgs E)
    {
      //uc_dt_birth.Focus();
      ResumeLayout();
      CalculateNameColumnWidths();

      if (GeneralParam.GetBoolean("Account.VisibleField", "Phone1", true) &&
          GeneralParam.GetBoolean("Account.VisibleField", "Phone2", true))
      {
        return;
      }
      if (GeneralParam.GetBoolean("Account.VisibleField", "Phone1", true))
      {
        lbl_phone_01.Text = Resource.String("STR_FRM_PLAYER_EDIT_PHONE");
      }
      if (GeneralParam.GetBoolean("Account.VisibleField", "Phone2", true))
      {
        lbl_phone_02.Text = Resource.String("STR_FRM_PLAYER_EDIT_PHONE");
      }


    } //PlayerEditDetailsReceptionView_Shown

    private void CalculateNameColumnWidths()
    {
      int _count_name;
      int _count;
      float _width;

      _count = 0;
      _count_name = 0;
      _count_name += lbl_name.Visible ? 1 : 0;
      _count_name += lbl_name4.Visible ? 1 : 0;
      _count_name += lbl_last_name1.Visible ? 1 : 0;
      _count_name += lbl_last_name2.Visible ? 1 : 0;

      if (_count_name == 0)
      {
        return;
      }

      _width = 100f / _count_name;

      bool _mode_last_name = GeneralParam.GetBoolean("Account", "InverseNameOrder", false);

      foreach (ColumnStyle _col_style in tlp_name.ColumnStyles)
      {
        if (_mode_last_name)
        {
          switch (_count)
          {
            case 0:
              _col_style.Width = lbl_last_name1.Visible ? _width : 0;
              break;
            case 1:
              _col_style.Width = lbl_last_name2.Visible ? _width : 0;
              break;
            case 2:
              _col_style.Width = lbl_name.Visible ? _width : 0;
              break;
            case 3:
              _col_style.Width = lbl_name4.Visible ? _width : 0;
              break;
          }
        }
        else
        {
          switch (_count)
          {
            case 0:
              _col_style.Width = lbl_name.Visible ? _width : 0;
              break;
            case 1:
              _col_style.Width = lbl_name4.Visible ? _width : 0;
              break;
            case 2:
              _col_style.Width = lbl_last_name1.Visible ? _width : 0;
              break;
            case 3:
              _col_style.Width = lbl_last_name2.Visible ? _width : 0;
              break;
          }
        }
        _count++;
      }
    } //CalculateNameColumnWidths

    /// <summary>
    ///   reformat full name when name texts have been changed
    /// </summary>
    /// <param name="Sender"></param>
    /// <param name="E"></param>
    private void txt_name_TextChanged(object Sender, EventArgs E)
    {
      if (string.IsNullOrEmpty(txt_name.Text) &&
          string.IsNullOrEmpty(txt_name4.Text) &&
          string.IsNullOrEmpty(txt_last_name2.Text) &&
          string.IsNullOrEmpty(txt_last_name1.Text))
        return;

      if (m_controller == null)
      {
        return;
      }

      lbl_full_name.Text =
        ((PlayerEditDetailsReceptionController)m_controller).ReFormatFullNameName(new[] { txt_name.Text, txt_name4.Text, txt_last_name1.Text, txt_last_name2.Text });

    } //txt_name_TextChanged

    /// <summary>
    ///   load form an execute initialization
    /// </summary>
    /// <param name="Sender"></param>
    /// <param name="E"></param>
    private void PlayerEditDetailsReceptionView_Load(object Sender, EventArgs E)
    {
      InitContols();
      SuspendLayout();
      //grd_customers.Focus();
      txt_name.TextChanged -= txt_name_TextChanged;
      txt_name4.TextChanged -= txt_name_TextChanged;
      txt_last_name1.TextChanged -= txt_name_TextChanged;
      txt_last_name2.TextChanged -= txt_name_TextChanged;
      txt_name.TextChanged += txt_name_TextChanged;
      txt_name4.TextChanged += txt_name_TextChanged;
      txt_last_name1.TextChanged += txt_name_TextChanged;
      txt_last_name2.TextChanged += txt_name_TextChanged;
      grd_customers.SelectionChanged -= grd_customers_SelectionChanged;
      lbl_pin_msg.Text = "";
      btn_save_pin.Enabled = false;
      lbl_pin_msg.Visible = false;
      txt_confirm_pin.Enabled = false;

      if (grd_customers.Rows.Count > 0)
      {
        grd_customers.Rows[0].Selected = false;
        btn_view_list.Enabled = true;
      }
      grd_customers.SelectionChanged += grd_customers_SelectionChanged;
      if (grd_customers.Rows.Count == 1)
      {
        grd_customers.Rows[0].Selected = true;
        btn_view_list.Enabled = false;
      }

      if (grd_customers.Rows.Count < ViewCollapsedTopResults)
      {
        Model.NoMoreResults = true;
      }

      SetInitialSizeGrid();

      FormClosing += delegate(object SenderObj, FormClosingEventArgs Args)
      {
        if (m_cashier_status == CASHIER_STATUS.OPEN && ((PlayerEditDetailsReceptionController)m_controller).BlockIfNotSaved(m_error_display, true))
        {
          Args.Cancel = true;
          return;
        }
        Args.Cancel = false;
      };
    } //PlayerEditDetailsReceptionView_Load

    /// <summary>
    ///   show more results in principal grid
    /// </summary>
    /// <param name="Sender"></param>
    /// <param name="E"></param>
    private void btn_view_list_Click(object Sender, EventArgs E)
    {

      if (Model.NoMoreResults)
        return;

      int _v = grd_customers.RowCount;

      ((PlayerEditDetailsReceptionController)m_controller).ShowMoreResults(IsResultsCustomersExtended ?
                                                                            ViewExtendedTopResults :
                                                                            ViewCollapsedTopResults);
      if (grd_customers.SelectedRows.Count == 0)
      {
      if (_v > 0 && _v < grd_customers.RowCount)
      {
        grd_customers.FirstDisplayedScrollingRowIndex = _v;
      }
      }

    } //btn_view_list_Click

    /// <summary>
    ///   take a photo
    /// </summary>
    /// <param name="Sender"></param>
    /// <param name="E"></param>
    private void m_img_photo_Click(object Sender, EventArgs E)
    {
      ((PlayerEditDetailsReceptionController)m_controller).TakePhoto();
    } //m_img_photo_Click

    //public Label LblAddressValidation
    //{
    //  get { return lbl_address_validation; }
    //  set { lbl_address_validation = value; }
    //}

    /// <summary>
    ///   register an entry
    /// </summary>
    /// <param name="Sender"></param>
    /// <param name="E"></param>
    private void btn_entry_Click(object Sender, EventArgs E)
    {
      if (!ManagementCashierSessionOpenMessage())
      {
        return;
      }

      m_error_display.StopTimerAndResetLabels();

      List<InvalidField> _errors = ((PlayerEditDetailsReceptionController)m_controller).PerformEntry(m_error_display);

      if (_errors.Count != 0)
      {
        m_error_display.DisplayErrors(_errors);
      }
    } //btn_entry_Click

    /// <summary>
    ///   begin process of registering a new customer
    /// </summary>
    /// <param name="Sender"></param>
    /// <param name="E"></param>
    private void btn_new_customer_Click(object Sender, EventArgs E)
    {
      if (!ManagementCashierSessionOpenMessage())
      {
        return;
      }

      if (((PlayerEditDetailsReceptionController)m_controller).BlockIfNotSaved(m_error_display, true))
      {
        return;
      }


      m_suppress_select_row = true;
      if (grd_customers.SelectedRows.Count == 1)
      {
        grd_customers.SelectedRows[0].Selected = false;
      }
      m_suppress_select_row = false;

      if (IsResultsCustomersExtended)
      {
        ResizeGrid(false);
      }

      if (((PlayerEditDetailsReceptionController)m_controller).NewCustomer())
      {
        ((PlayerEditDetailsReceptionController)m_controller).SetControlsReadMode(this, m_ignored_controls, m_visibility_controls, false);
        btn_update_record_expiration.Enabled = false;

        btn_add_to_blacklist.Enabled = false;
        btn_remove_from_blacklist.Enabled = false;
        btn_show_matches.Enabled = false;

      }

    } //btn_new_customer_Click
    
    /// <summary>
    ///   react to change of selected customer in primary data grid
    /// </summary>
    /// <param name="Sender"></param>
    /// <param name="E"></param>
    private void grd_customers_SelectionChanged(object Sender, EventArgs E)
    {

      if (m_suppress_select_row)
      {
        return;
      }
      if (m_cashier_status == CASHIER_STATUS.OPEN && ((PlayerEditDetailsReceptionController)m_controller).BlockIfNotSaved(m_error_display, true))
      {
        m_suppress_select_row = true;
        if (grd_customers.SelectedRows.Count > 0)
          foreach (DataGridViewRow _selected_row in grd_customers.SelectedRows)
          {
            _selected_row.Selected = false;
          }
        grd_customers.Rows[m_last_selected_grid_index].Selected = true;
        m_suppress_select_row = false;
        return;
      }
      pb_scanned_photo.Image = null;
      if (grd_customers.CurrentRow == null)
      {
        return;
      }
      m_error_display.StopTimerAndResetLabels();
      m_shown_coments = false;
      CustomerSearchResult _selected_customer = grd_customers.CurrentRow.DataBoundItem as CustomerSearchResult;
      if (grd_customers.SelectedRows.Count == 1)
      {
        m_last_selected_grid_index = grd_customers.SelectedRows[0].Index;
      }
      else
      {
        m_last_selected_grid_index = -1;
      }
      if (_selected_customer == null)
      {
        return;
      }
      
      if (IsResultsCustomersExtended)
      {
        ResizeGrid(false);

        if (m_last_selected_grid_index > 0
          && m_last_selected_grid_index < grd_customers.RowCount)
        {
          grd_customers.FirstDisplayedScrollingRowIndex = m_last_selected_grid_index;
        }
      }

      ((PlayerEditDetailsReceptionController)m_controller).ShowCustomersDetails(_selected_customer);

      if (tab_player_edit.SelectedTab == scanned_docs)
      {
        ((PlayerEditDetailsReceptionController)m_controller).LoadScannedDocuments();
      }

      btn_ok.Enabled = CashierBusinessLogic.ReceptionButtonsEnabledCashierSessionOpenAutomatically(m_cashier_status);
      btn_card_functions.Enabled = CashierBusinessLogic.ReceptionButtonsEnabledCashierSessionOpenAutomatically(m_cashier_status) &&
                                   (m_model.AccountId.HasValue || m_model.AccountId < 1);
      btn_new_customer.Enabled = (CashierBusinessLogic.ReceptionButtonsEnabledCashierSessionOpenAutomatically(m_cashier_status)) &&
                                 (m_model.AccountId.HasValue || m_model.AccountId < 1);

      ((PlayerEditDetailsReceptionController)m_controller).SetControlsReadMode(this, m_ignored_controls, m_visibility_controls, false);
      m_delay_background_validation_timer.Stop();
      m_delay_background_validation_timer.Enabled = true;
      m_delay_background_validation_timer.Start();



    }//grd_customers_SelectionChanged

    /// <summary>
    ///   react to rescan button click
    /// </summary>
    /// <param name="Sender"></param>
    /// <param name="E"></param>
    private void grd_scanned_docs_CellClick(object Sender, DataGridViewCellEventArgs E)
    {
      if (m_cashier_status != CASHIER_STATUS.OPEN)
      {
        return;
      }

      if (grd_scanned_docs.Columns[E.ColumnIndex].Name != "columnScannedDocumentRescan")
      {
        return;
      }
      if (E.RowIndex == -1)
      {
        return;
      }

      ScannedDocument _selected_document = (ScannedDocument)grd_scanned_docs.Rows[E.RowIndex].DataBoundItem;
      ((PlayerEditDetailsReceptionController)m_controller).ScanDocument(_selected_document);
      //m_controller.ScanDocument(e.RowIndex);
      pb_scanned_photo.Image = _selected_document.Image.ToImage();
    }//grd_scanned_docs_CellClick

    /// <summary>
    ///   call controller routine to refresh blacklist check for current customer
    /// </summary>
    /// <param name="Sender"></param>
    /// <param name="E"></param>
    private void btn_image_black_list_Click(object Sender, EventArgs E)
    {
      m_model.BlackListCustomerSelected = null;
      ((PlayerEditDetailsReceptionController)m_controller).RefreshBlackListInfo(true);
    }//btn_image_black_list_Click

    /// <summary>
    ///   call controller routine to initiate document scan
    /// </summary>
    /// <param name="Sender"></param>
    /// <param name="E"></param>
    private void btn_scan_doc_Click(object Sender, EventArgs E)
    {
      if (!ManagementCashierSessionOpenMessage())
      {
        return;
      }

      if (m_cashier_status == CASHIER_STATUS.OPEN)
      {
      ((PlayerEditDetailsReceptionController)m_controller).ScanDocument();
      }
    }//btn_scan_doc_Click

    /// <summary>
    ///   call controller routine to initiate id scan
    /// </summary>
    /// <param name="Sender"></param>
    /// <param name="E"></param>
    private void btn_id_card_scan_Click(object Sender, EventArgs E)
    {
      if (!ManagementCashierSessionOpenMessage())
      {
        return;
      }

      ((PlayerEditDetailsReceptionController)m_controller).IdScan();
    }//btn_id_card_scan_Click

    /// <summary>
    ///   button to call save pin method in controller
    /// </summary>
    /// <param name="Sender"></param>
    /// <param name="E"></param>
    private void btn_save_pin_Click(object Sender, EventArgs E)
    {
      if (!ManagementCashierSessionOpenMessage())
      {
        return;
      }

      m_error_display.StopTimerAndResetLabels();

      List<InvalidField> _errors = ((PlayerEditDetailsReceptionController)m_controller).SavePin();

      m_error_display.DisplayErrors(_errors);
    }//btn_save_pin_Click

    /// <summary>
    ///   button to call generate random pin method in controller
    /// </summary>
    /// <param name="Sender"></param>
    /// <param name="E"></param>
    private void btn_generate_random_pin_Click(object Sender, EventArgs E)
    {
      m_error_display.StopTimerAndResetLabels();

      List<InvalidField> _errors = ((PlayerEditDetailsReceptionController)m_controller).CreateRandomPin();

      m_error_display.DisplayErrors(_errors);
    }//btn_generate_random_pin_Click

    /// <summary>
    ///   method to clear pin and counter pin on entry to pin
    /// </summary>
    /// <param name="Sender"></param>
    /// <param name="E"></param>
    private void txt_pin_Enter(object Sender, EventArgs E)
    {
      txt_pin.Text = "";
      txt_confirm_pin.Text = "";
    }//txt_pin_Enter

    /// <summary>
    ///   method to clear counter pin on entry
    /// </summary>
    /// <param name="Sender"></param>
    /// <param name="E"></param>
    private void txt_confirm_pin_Enter(object Sender, EventArgs E)
    {
      txt_confirm_pin.Text = "";
    }//txt_confirm_pin_Enter

    /// <summary>
    ///   button to enable pin save on text changes in pin text box
    /// </summary>
    /// <param name="Sender"></param>
    /// <param name="E"></param>
    private void txt_pin_TextChanged(object Sender, EventArgs E)
    {
      btn_save_pin_enable();
    }//txt_pin_TextChanged

    /// <summary>
    ///   button to enable pin save on text changes in counter pin text box
    /// </summary>
    /// <param name="Sender"></param>
    /// <param name="E"></param>
    private void txt_confirm_pin_TextChanged(object Sender, EventArgs E)
    {
      btn_save_pin_enable();
    }//txt_confirm_pin_TextChanged

    /// <summary>
    ///   method that checks if the pin is valid and savable
    /// </summary>
    private void btn_save_pin_enable()
    {
      txt_confirm_pin.Enabled = ((CashierBusinessLogic.ReceptionButtonsEnabledCashierSessionOpenAutomatically(m_cashier_status)) && txt_pin.Text.Length >= Accounts.PIN_MIN_LENGTH);
      btn_save_pin.Enabled = ((CashierBusinessLogic.ReceptionButtonsEnabledCashierSessionOpenAutomatically(m_cashier_status)) &&
                               (txt_pin.Text.Length >= Accounts.PIN_MIN_LENGTH) &&
                              (txt_confirm_pin.Text.Length == txt_pin.Text.Length) &&
                              (txt_pin.Text == txt_confirm_pin.Text));

      if ((txt_pin.Text.Length >= Accounts.PIN_MIN_LENGTH) && (txt_confirm_pin.Text.Length == txt_pin.Text.Length) &&
          (txt_pin.Text != txt_confirm_pin.Text))
      {
        lbl_pin_msg.Visible = true;
        lbl_pin_msg.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_CONFIRMATION_PIN_DIFFERS");
        lbl_pin_msg.ForeColor = Color.Red;
      }
      else
      {
        lbl_pin_msg.Visible = false;
        lbl_pin_msg.Text = "";
      }
    } // btn_save_pin_enablebtn_save_pin_enable

    /// <summary>
    ///   react to change in customer grid
    /// </summary>
    /// <param name="Sender"></param>
    /// <param name="E"></param>
    private void grd_scanned_docs_SelectionChanged(object Sender, EventArgs E)
    {
      if (grd_scanned_docs.CurrentRow == null)
      {
        return;
      }

      ScannedDocument _selected_scanned_document = (ScannedDocument)grd_scanned_docs.CurrentRow.DataBoundItem;
      if (_selected_scanned_document != null && _selected_scanned_document.Image != null)
      {
        pb_scanned_photo.Image = _selected_scanned_document.Image.ToImage();
      }
    }//grd_scanned_docs_SelectionChanged

    /// <summary>
    ///   zoom image to full screen
    /// </summary>
    /// <param name="Sender"></param>
    /// <param name="E"></param>
    private void pb_scanned_photo_Click(object Sender, EventArgs E)
    {
      if (pb_scanned_photo.Image != null)
      {
        ViewImage.ViewFullScreenImage(pb_scanned_photo.Image);
      }
    }//pb_scanned_photo_Click

    /// <summary>
    ///   show copy address to alternative or not on change of address tab
    /// </summary>
    /// <param name="Sender"></param>
    /// <param name="E"></param>
    private void tab_address_SelectedIndexChanged(object Sender, EventArgs E)
    {
      btn_copy_adddress.Visible = tab_address.SelectedIndex == 1;
    }//tab_address_SelectedIndexChanged

    /// <summary>
    ///   call controller routine to copy address to alternative
    /// </summary>
    /// <param name="Sender"></param>
    /// <param name="E"></param>
    private void btn_copy_adddress_Click(object Sender, EventArgs E)
    {
      ((PlayerEditDetailsReceptionController)m_controller).CopyAddressToAlternative();
    }//btn_copy_adddress_Click

    /// <summary>
    ///   update expiration of records
    /// </summary>
    /// <param name="Sender"></param>
    /// <param name="E"></param>
    private void btn_update_record_expiration_Click(object Sender, EventArgs E)
    {
      if (!ManagementCashierSessionOpenMessage())
      {
        return;
      }

      ((PlayerEditDetailsReceptionController)m_controller).UpdateRecordExpiration();
    }//btn_update_record_expiration_Click

    /// <summary>
    /// add account to blacklist
    /// </summary>
    /// <param name="Sender"></param>
    /// <param name="E"></param>
    private void btn_add_to_blacklist_Click(object Sender, EventArgs E)
    {
      if (!ManagementCashierSessionOpenMessage())
      {
        return;
      }
      ((PlayerEditDetailsReceptionController)m_controller).AddToBlackList();
      ((PlayerEditDetailsReceptionController)m_controller).RefreshBlackListInfo(true);
    }

    /// <summary>
    /// remove account from blacklist
    /// </summary>
    /// <param name="Sender"></param>
    /// <param name="E"></param>
    private void btn_remove_from_blacklist_Click(object Sender, EventArgs E)
    {
      if (!ManagementCashierSessionOpenMessage())
      {
        return;
      }

      ((PlayerEditDetailsReceptionController)m_controller).RemoveFromBlackList();
      ((PlayerEditDetailsReceptionController)m_controller).RefreshBlackListInfo(true);
    }

    /// <summary>
    ///   call controller functions for recycle and assign card depending on current track data
    /// </summary>
    /// <param name="Sender"></param>
    /// <param name="E"></param>
    private void btn_card_functions_Click(object Sender, EventArgs E)
    {
      if (!ManagementCashierSessionOpenMessage())
      {
        return;
      }

      if (string.IsNullOrEmpty(m_model.VisibleTrackData) || m_model.VisibleTrackData.Contains("----"))
      {
        ((PlayerEditDetailsReceptionController)m_controller).AssignCard();
      }
      else
      {
        ((PlayerEditDetailsReceptionController)m_controller).RecycleCard();
      }
    }//btn_card_functions_Click

    private void tab_player_edit_Selected(object Sender, TabControlEventArgs E)
    {
      if (E.TabPage == scanned_docs)
      {
        ((PlayerEditDetailsReceptionController)m_controller).LoadScannedDocuments();
      }
    }//tab_player_edit_Selected

    private void principal_Click(object Sender, EventArgs E)
    {
    }//principal_Click

    /// <summary>
    /// Change position of name and last name by general param
    /// </summary>
    private void SetNameAndLastNamePostion()
    {
      if (GeneralParam.GetBoolean("Account", "InverseNameOrder", false))
      {
        TableLayoutPanelCellPosition _last_name2_txt_pos = tlp_name.GetPositionFromControl(txt_last_name2);
        TableLayoutPanelCellPosition _last_name2_lbl_pos = tlp_name.GetPositionFromControl(lbl_last_name2);
        TableLayoutPanelCellPosition _last_name1_txt_pos = tlp_name.GetPositionFromControl(txt_last_name1);
        TableLayoutPanelCellPosition _last_name1_lbl_pos = tlp_name.GetPositionFromControl(lbl_last_name1);
        TableLayoutPanelCellPosition _name2_txt_pos = tlp_name.GetPositionFromControl(txt_name4);
        TableLayoutPanelCellPosition _name2_lbl_pos = tlp_name.GetPositionFromControl(lbl_name4);
        TableLayoutPanelCellPosition _name_txt_pos = tlp_name.GetPositionFromControl(txt_name);
        TableLayoutPanelCellPosition _name_lbl_pos = tlp_name.GetPositionFromControl(lbl_name);

        tlp_name.SetCellPosition(lbl_name, _last_name1_lbl_pos);
        tlp_name.SetCellPosition(txt_name, _last_name1_txt_pos);
        tlp_name.SetCellPosition(lbl_name4, _last_name2_lbl_pos);
        tlp_name.SetCellPosition(txt_name4, _last_name2_txt_pos);
        tlp_name.SetCellPosition(lbl_last_name1, _name_lbl_pos);
        tlp_name.SetCellPosition(txt_last_name1, _name_txt_pos);
        tlp_name.SetCellPosition(lbl_last_name2, _name2_lbl_pos);
        tlp_name.SetCellPosition(txt_last_name2, _name2_txt_pos);
        tlp_name.Refresh();

        var _tab_index = txt_name.TabIndex;
        txt_name.TabIndex = txt_last_name1.TabIndex;
        txt_last_name1.TabIndex = _tab_index;

        _tab_index = txt_name4.TabIndex;
        txt_name4.TabIndex = txt_last_name2.TabIndex;
        txt_last_name2.TabIndex = _tab_index;
      }
    }

    private bool ManagementCashierSessionOpenMessage()
    {
      Cashier.CashierOpenAutomaticallyResult _val_ret;

      _val_ret = (((PlayerEditDetailsReceptionController)m_controller).ManagementReception(m_cashier_status));

      if (_val_ret != Cashier.CashierOpenAutomaticallyResult.OK)
      {
        if (_val_ret == Cashier.CashierOpenAutomaticallyResult.ERROR)
        {
          frm_message.Show(Resource.String("STR_FRM_RECEPTION_CASHIER_SESION_OPEN_AUTOMATICALLY_ERROR"),
                            Resource.String("STR_APP_GEN_MSG_ERROR"),
                            MessageBoxButtons.OK,
                            Images.CashierImage.Error,
                            this);
        }

        return false;
      }

      m_cashier_status = CASHIER_STATUS.OPEN;

      return true;
    }

    /// <summary>
    /// Initilize the size grid
    /// </summary>
    private void SetInitialSizeGrid()
    {
      Boolean _extend_results;

      _extend_results = (GeneralParam.GetBoolean("Reception", "ResultsView.Mode", true)
                       && grd_customers.Rows.Count > 1);

      ResizeGrid(_extend_results);
    }//SetInitialSizeGrid

    /// <summary>
    /// Resize the grid
    /// </summary>
    private void ResizeGrid(bool ExtendResults)
    {
      if (m_header_images == null)
      {
        m_header_images = new Dictionary<int, object>();
      }

      if (ExtendResults)
      {
        pnl_results.Location = m_location_extended_point;
        pnl_results.Size = new Size(GRD_CUSTOMER_WIDTH_NORMAL, GRD_CUSTOMER_HEIGHT_EXTENDED);

        if (m_header_images.ContainsKey(COLUMN_INDEX_BIRTH_DATE))
        {
          m_header_images[COLUMN_INDEX_BIRTH_DATE] = Resources.ResourceImages.contract_grid_white;
        }
        else
        {
          m_header_images.Add(COLUMN_INDEX_BIRTH_DATE, Resources.ResourceImages.contract_grid_white);
        }
      }
      else
      {
        pnl_results.Location = m_location_normal_point;
        pnl_results.Size = new Size(GRD_CUSTOMER_WIDTH_NORMAL, GRD_CUSTOMER_HEIGHT_NORMAL);

        if (m_header_images.ContainsKey(COLUMN_INDEX_BIRTH_DATE))
        {
          m_header_images[COLUMN_INDEX_BIRTH_DATE] = Resources.ResourceImages.expand_grid_white;
        }
        else
        {
          m_header_images.Add(COLUMN_INDEX_BIRTH_DATE, Resources.ResourceImages.expand_grid_white);
        }
      }

      if (grd_customers.HeaderImages == null)
      {
        grd_customers.HeaderImages = m_header_images;
    }

      grd_customers.Refresh();
    }//ResizeGrid

    #endregion

    #region Instance Fields

    /// <summary>
    ///   initial selected item in grid
    /// </summary>
    private int m_last_selected_grid_index = -1;

    /// <summary>
    ///   flag to suppress executing methods on programmatic change of customer
    /// </summary>
    private bool m_suppress_select_row;

    #endregion

    #region Methods

    /// <summary>
    ///   general constructor
    /// </summary>
    public PlayerEditDetailsReceptionView() //DataSet DsCustomers
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[UC LOAD] PlayerEditDetailsReceptionView", Log.Type.Message);

      InitializeComponent();
      m_error_display = new PlayerEditDetailsCommonErrorDisplay<PlayerEditDetailsReceptionModel>(this,
        lbl_msg_count_blink, lbl_msg_blink, pictureBox1);

      SetNameAndLastNamePostion();
    }

    public override void DoFirstSet()
        {
      ((PlayerEditDetailsReceptionController)m_controller).SetFormatDataGridPrincipal(grd_customers);
      ((PlayerEditDetailsReceptionController)m_controller).SetFormatDataGridScanned(grd_scanned_docs);
          m_cashier_status = CashierBusinessLogic.ReadCashierStatus(Cashier.SessionId);
          //load combo boxes from model
          PopulateComboBoxes();
          if (m_model.GridData != null && m_model.GridData.Count > 1)
          {
            frm_message.Show(Resource.String("STR_FRM_CUSTOMER_RECEPTION_QUERY_MORE_THAN_ONE_ACCOUNT"), "",
              MessageBoxButtons.OK, this);
        ((PlayerEditDetailsReceptionController)m_controller).SetControlsReadMode(this, m_ignored_controls, m_visibility_controls, true);
            btn_cancel.Enabled = true;
            btn_keyboard.Enabled = true;
          }
        }


    /// <summary>
    ///   collection of fields for visibility manipulation
    /// </summary>
    public override VisibleData VisibleFields
    {
      get
      {
        {
          VisibleData _visible_data;
          _visible_data = new VisibleData();
          _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.PHOTO, new Control[] { m_img_photo });
          _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.NAME3, new Control[] { lbl_name, txt_name });
          _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.NAME4, new Control[] { lbl_name4, txt_name4 });
          _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.NAME1, new Control[] { lbl_last_name1, txt_last_name1 });
          _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.NAME2, new Control[] { lbl_last_name2, txt_last_name2 });
          _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.GENDER, new Control[] { lbl_gender, cb_gender });
          _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.NATIONALITY, new Control[] { lbl_nationality, cb_nationality });
          _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.BIRTH_COUNTRY,
            new Control[] { lbl_birth_country, cb_birth_country });
          _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.MARITAL_STATUS, new Control[] { lbl_marital_status, cb_marital });

          _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.PHONE1, new Control[] { lbl_phone_01, txt_phone_01 });
          _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.PHONE2, new Control[] { lbl_phone_02, txt_phone_02 });
          _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.EMAIL1, new Control[] { lbl_email_01, txt_email_01 });
          _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.FACEBOOK, new Control[] { lbl_facebook, txt_facebook });
          _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.ALTERNATIVEADDRESS, new Control[] { alt_address });

          return _visible_data;
        }
      }
    }

    /// <summary>
    /// Check if document id already exist
    /// </summary>
    /// <returns></returns>
    private bool CheckIdAlreadyExist()
    {
      if (m_error_display == null) return false;
      m_error_display.StopTimerAndResetLabels();

      List<InvalidField> _errors = ((PlayerEditDetailsReceptionController)m_controller).CheckIdAlreadyExists(false);

      if (_errors.Count != 0)
      {
        m_error_display.DisplayErrors(_errors);
        return false;
      }

      return true;
    }

    /// <summary>
    ///   method to update status display for pin in case of error
    /// </summary>
    public override void ErrorUpdatingPin()
    {
      lbl_pin_msg.Visible = true;
      lbl_pin_msg.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_ERROR_UPDATING_PIN");
      lbl_pin_msg.ForeColor = Color.Red;
    }

    /// <summary>
    ///   method to update status display for pin if generation succeed
    /// </summary>
    public override void RandomPinGeneratedOk()
    {
      lbl_pin_msg.Visible = true;
      lbl_pin_msg.ForeColor = Color.Green;
      lbl_pin_msg.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_PIN_GENERATED_CORRECTLY");
      txt_pin.Enabled = false;
      txt_confirm_pin.Enabled = false;
      btn_save_pin.Enabled = false;
      btn_generate_random_pin.Enabled = false;
    }

    /// <summary>
    ///   method to update status display is saving of new pin succeed
    /// </summary>
    public override void PinChangedOk()
    {
      lbl_pin_msg.Visible = true;
      lbl_pin_msg.ForeColor = Color.Green;
      lbl_pin_msg.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_PIN_CHANGED_CORRECTLY");
      txt_pin.Enabled = false;
      txt_confirm_pin.Enabled = false;
      btn_save_pin.Enabled = false;
      btn_generate_random_pin.Enabled = false;
    }

    #endregion

    /// <summary>
    /// Validate if document exist
    /// </summary>
    /// <param name="Sender"></param>
    /// <param name="E"></param>
    private void txt_document_Leave(object Sender, EventArgs E)
    {
      String _value_new;
      _value_new = txt_document.Text;

      if (!((PlayerEditDetailsReceptionController)m_controller)._showndupdetails)
      {
        CheckIdAlreadyExist();
      }
      else
      {
        if (m_value_old != String.Empty && _value_new != m_value_old)
        {
          ((PlayerEditDetailsReceptionController)m_controller)._showndupdetails = false;
          CheckIdAlreadyExist();
        }
      }
      m_value_old = String.Empty;
    }
    private Dictionary<string, string> m_namesdictionsry = new Dictionary<string, string>();
    /// <summary>
    /// Allows to validate duplicate Accounts 
    /// </summary>
    /// <param name="Sender"></param>
    /// <param name="E"></param>
    private void txt_name_Leave(object Sender, EventArgs E)
    {
      SetAge(uc_dt_birth.DateValue);
      List<InvalidField> _errors = null;
      String _value_new;
      _value_new = string.Empty;

      if (m_error_display == null) return;

      if (Sender is CharacterTextBox)
      {
        _value_new = ((CharacterTextBox)Sender).Text;
      }

      if (Sender is uc_datetime)
      {
        _value_new = ((uc_datetime)Sender).DateValue.ToShortDateString();
      }

      if (Sender is uc_round_auto_combobox)
      {
        _value_new = ((uc_round_auto_combobox)Sender).Text;
      }

      //DPC
      if (!((PlayerEditDetailsReceptionController)m_controller)._showndupdetails)
      {
        m_error_display.StopTimerAndResetLabels();
        _errors = ((PlayerEditDetailsReceptionController)m_controller).CheckDuplicatePlayer(false);
      }
      else
      {
        if (m_value_old != String.Empty && _value_new != m_value_old)
        {
      m_error_display.StopTimerAndResetLabels();
          ((PlayerEditDetailsReceptionController)m_controller)._showndupdetails = false;
          _errors = ((PlayerEditDetailsReceptionController)m_controller).CheckDuplicatePlayer(false);
        }
      }

      if (_errors != null && _errors.Count != 0)
      {
        if (_errors[0].Id == "ContinueAccountsDuplicate")
      {
          _errors.Clear();
        }
        m_error_display.DisplayErrors(_errors);
      }

      m_value_old = String.Empty;

      uc_round_textbox _sender_casted = Sender as uc_round_textbox;

      if (_sender_casted == null) return;


      if (!m_namesdictionsry.ContainsKey(_sender_casted.Name))
        return;

      if (m_namesdictionsry[_sender_casted.Name] == _sender_casted.Text)
        return;
      m_namesdictionsry[_sender_casted.Name] = _sender_casted.Text;


      ((PlayerEditDetailsReceptionController)m_controller).RefreshBlackListInfo(true);
      
    }

    private void SetAge(DateTime bday)
    {
      if (bday == default(DateTime))
      {
        lbl_age.Text = "";
        return;
      }
      var _today = DateTime.Today;
      // Calculate the age.
      var age = _today.Year - bday.Year;
      // Go back to the year the person was born in case of a leap year
      if (bday > _today.AddYears(-age)) age--;
      lbl_age.Text = string.Format(Resource.String("STR_AGE_X"), age);
    }

    private void SelectDefaultCountryFromGeneralParam(String GroupKey, String SubjectKey)
    {
      if (!String.IsNullOrEmpty(GeneralParam.GetString(GroupKey, SubjectKey)))
      {
        // We always get the first (and expected) unique result
        System.Data.DataRow _country_to_select = CardData.GetCountriesList().Select("CO_ISO2 = '" + GeneralParam.GetString(GroupKey, SubjectKey) + "'")[0];
        String _country_name = _country_to_select.ItemArray[1].ToString();

        int _counter = 0;
        foreach (Nationality _option in (List<Nationality>)cb_nationality.DataSource)
        {
          if (_option.Name == _country_name)
          {
            cb_nationality.SelectedIndex = _counter;
            cb_nationality.SelectedItem = _country_name;
            cb_nationality.SelectedValue = _country_name;

            cb_birth_country.SelectedIndex = _counter;
            cb_birth_country.SelectedItem = _country_name;
            cb_birth_country.SelectedValue = _country_name;

            HolderAddress.Country.Id = _counter;
            HolderAddress.Country.Name = _country_name;
          }
          _counter++;
        }
      }
    }
    private void btn_id_card_scan_LongClick(object Sender, EventArgs E)
    {
      ((PlayerEditDetailsReceptionController)m_controller).IdScanAdvanced();
    }

    /// <summary>
    /// allows to show coincidences inBlacklist
    /// </summary>
    /// <param name="Sender"></param>
    /// <param name="E"></param>
    private void btn_show_matches_Click(object Sender, EventArgs E)
    {
      if (m_controller == null)
        return;
      ((PlayerEditDetailsReceptionController)m_controller).BlacklistValidateMatches();

      // XGJ 21-DIC-2016
      m_error_display.StopTimerAndResetLabels();
    
    }

		internal void ShowBlackListTab()
		{
			throw new NotImplementedException();
		}

    private void tab_player_edit_Click(object Sender, EventArgs E)
    {
      TabsPlayerEditAutoSelectControl();
    }

    private void TabsPlayerEditAutoSelectControl()
    {
      switch (tab_player_edit.SelectedTab.Name)
      {
        case "principal":
          ActiveControl = txt_name;
          break;

        case "contact":
          ActiveControl = txt_phone_01;
          break;

        case "address":
          AddressAutoSelectControlManager();
          break;

        case "comments":
          ActiveControl = txt_comments;
          break;

        case "pin":
          ActiveControl = txt_pin;
          break;

        case "scanned_docs":
          ActiveControl = btn_scan_doc;
          break;

        case "blacklist":
          ActiveControl = chk_acknowledged_blacklist;
          break;
      }
    }

    private void AddressAutoSelectControlManager()
    {
      if (tab_address.SelectedTab.Name == "home_address")
      {
        HolderAddress.SelectFirstControl();
      }
      else
      {
        AlternativeAddress.SelectFirstControl(); 
      }
    }

    private void tab_address_Click(object Sender, EventArgs E)
    {
      TabsPlayerEditAutoSelectControl();
    }

    private void cb_document_type_SelectedIndexChanged(object Sender, EventArgs E)
    {
      UpdateDocTypeDescriptions();
    }

    private void txt_doc_leave(object Sender, EventArgs E)
    {
      txt_document_Leave(null, null);
      var _doc_types = (cb_document_type.SelectedItem as DocumentType);
      if (_doc_types == null) return;
      if (m_documents.ContainsKey(_doc_types.Id))
      {
        m_documents[_doc_types.Id] = txt_document.Text;
      }
      else
      {
        m_documents.Add(_doc_types.Id, txt_document.Text);
      }


      int _docvalues = 0;
      foreach (var _docs in m_documents)
      {
        if (!string.IsNullOrEmpty(_docs.Value))
        {
          _docvalues++;
        }


      }
      int _i = lbl_type_document.Text.IndexOf(" (", StringComparison.Ordinal);
      if (_i < 1)
        _i = lbl_type_document.Text.Length;
      lbl_type_document.Text = lbl_type_document.Text.Substring(0, _i);

      if (_docvalues > 1)
      {
        lbl_type_document.Text += string.Format(" ({0})", _docvalues);
      }
      if (!string.IsNullOrEmpty(txt_document.Text))
        ((PlayerEditDetailsReceptionController)m_controller).RefreshBlackListInfo(true);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="Sender"></param>
    /// <param name="E"></param>
    private void grd_customers_CellClick(object Sender, DataGridViewCellEventArgs E)
    {
      if (E.ColumnIndex == COLUMN_INDEX_BIRTH_DATE && E.RowIndex == -1)
      {
        ResizeGrid(!IsResultsCustomersExtended);

        if (IsResultsCustomersExtended
          && ViewExtendedTopResults > ViewCollapsedTopResults
          && grd_customers.RowCount < ViewExtendedTopResults)
        {
          ((PlayerEditDetailsReceptionController)m_controller).ShowMoreResults(ViewExtendedTopResults - ViewCollapsedTopResults);
        }

        if (!IsResultsCustomersExtended && grd_customers.SelectedRows.Count == 1)
        {
          if (m_last_selected_grid_index > 0 && m_last_selected_grid_index < grd_customers.RowCount)
          {
            grd_customers.FirstDisplayedScrollingRowIndex = m_last_selected_grid_index;
          }
        }
      }
      else if (E.RowIndex >= 0 && IsResultsCustomersExtended) //When we are clicking in the same register we need show customer data.
      {
        ResizeGrid(false);
      }
    }

    internal void CleanUpForm()
    {
      m_error_display.CleanUp();
      m_error_display = null;
      cb_birth_country.DataSource = null;
      cb_document_type.DataSource = null;
      cb_document_type.DataSource = null;
      cb_gender.DataSource = null;
      cb_nationality.DataSource = null;
      cb_birth_country.Dispose();
      cb_document_type.Dispose();
      cb_document_type.Dispose();
      cb_gender.Dispose();
      cb_nationality.Dispose();
      cb_birth_country = null;
      cb_document_type = null;
      cb_document_type = null;
      cb_gender = null;
      cb_nationality = null;
      AlternativeAddress.CleanUp();
      HolderAddress.CleanUp();
      Model = null;
      Controller = null;
    }

    private void lbl_record_Click(object Sender, EventArgs E)
    {

    }

    private void txt_name_enter(object sender, EventArgs e)
    {
      if (sender is CharacterTextBox)
      {
        m_value_old = ((CharacterTextBox)sender).Text;
      }

      if (sender is uc_datetime)
    {
        m_value_old = ((uc_datetime)sender).DateValue.ToShortDateString();
      }

      if (sender is uc_round_auto_combobox)
      {
        m_value_old = ((uc_round_auto_combobox)sender).Text;
      }
    }

    private void tlp_principal1_Paint(object sender, PaintEventArgs e)
    {

    }

    private void btn_exit_Click(object sender, EventArgs e)
    {
      if (!ManagementCashierSessionOpenMessage())
      {
        return;
      }

      m_error_display.StopTimerAndResetLabels();

      List<InvalidField> _errors = ((PlayerEditDetailsReceptionController)m_controller).PerformExit(m_error_display);

      if (_errors.Count != 0)
      {
        frm_message.Show(_errors[0].Reason, Resource.String("STR_APP_GEN_MSG_ERROR"),
                            MessageBoxButtons.OK,
                            Images.CashierImage.Error,
                            this);
      }
    }
  }

  public class VwBasePlayerEditDetailsReceptionModel : vw_base<PlayerEditDetailsReceptionModel>
  {

    public override VisibleData VisibleFields
    {
      get { return ((PlayerEditDetailsReceptionView)this).VisibleFields; }
    }

    public override void ModelToView()
    {
			((PlayerEditDetailsReceptionView)this).ModelToView();
    }

    public override void ViewToModel()
    {
			((PlayerEditDetailsReceptionView)this).ViewToModel();
    }

    public override void DoFirstSet()
    {
			((PlayerEditDetailsReceptionView)this).DoFirstSet();
    }

		public override void RandomPinGeneratedOk()
		{
			((PlayerEditDetailsReceptionView)this).RandomPinGeneratedOk();
		}

	  public override void PinChangedOk()
	  {
			((PlayerEditDetailsReceptionView)this).PinChangedOk();
	  }

	  public override void ErrorUpdatingPin()
		{
			((PlayerEditDetailsReceptionView)this).ErrorUpdatingPin();
    }
  }
}
