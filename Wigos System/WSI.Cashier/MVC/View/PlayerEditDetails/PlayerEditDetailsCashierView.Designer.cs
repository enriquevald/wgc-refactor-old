﻿using System.Windows.Forms;
using WSI.Cashier.Controls;
using WSI.Cashier.MVC.Model.PlayerEditDetails;
using WSI.Common;

namespace WSI.Cashier.MVC.View.PlayerEditDetails
{
  partial class PlayerEditDetailsCashierView
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PlayerEditDetailsCashierView));
      this.lbl_account_edit_title = new System.Windows.Forms.Label();
      this.m_img_photo = new WSI.Cashier.Controls.UcPhotobox();
      this.lbl_msg_count_blink = new System.Windows.Forms.Label();
      this.lbl_msg_blink = new System.Windows.Forms.Label();
      this.btn_keyboard = new WSI.Cashier.Controls.uc_round_button();
      this.btn_id_card_scan = new WSI.Cashier.Controls.uc_round_button_long_press();
      this.btn_print_information = new WSI.Cashier.Controls.uc_round_button();
      this.btn_cancel = new WSI.Cashier.Controls.uc_round_button();
      this.btn_ok = new WSI.Cashier.Controls.uc_round_button();
      this.tab_player_edit = new WSI.Cashier.Controls.uc_round_tab_control();
      this.principal = new System.Windows.Forms.TabPage();
      this.tlp_name = new System.Windows.Forms.TableLayoutPanel();
      this.txt_name4 = new WSI.Cashier.CharacterTextBox();
      this.lbl_name4 = new System.Windows.Forms.Label();
      this.txt_name = new WSI.Cashier.CharacterTextBox();
      this.lbl_name = new System.Windows.Forms.Label();
      this.lbl_last_name1 = new System.Windows.Forms.Label();
      this.txt_last_name1 = new WSI.Cashier.CharacterTextBox();
      this.txt_last_name2 = new WSI.Cashier.CharacterTextBox();
      this.lbl_last_name2 = new System.Windows.Forms.Label();
      this.tlp_principal = new System.Windows.Forms.TableLayoutPanel();
      this.lbl_type_document = new System.Windows.Forms.Label();
      this.cb_document_type = new WSI.Cashier.Controls.uc_round_auto_combobox();
      this.lbl_docuemnt_counter = new System.Windows.Forms.Label();
      this.lbl_document = new System.Windows.Forms.Label();
      this.txt_document = new WSI.Cashier.CharacterTextBox();
      this.lbl_expiration_doc_date = new System.Windows.Forms.Label();
      this.uc_dt_expiration_doc = new WSI.Cashier.uc_datetime();
      this.txt_RFC = new WSI.Cashier.CharacterTextBox();
      this.btn_RFC = new WSI.Cashier.Controls.uc_round_button();
      this.lbl_CURP = new System.Windows.Forms.Label();
      this.txt_CURP = new WSI.Cashier.CharacterTextBox();
      this.uc_dt_birth = new WSI.Cashier.uc_datetime();
      this.lbl_birth_date = new System.Windows.Forms.Label();
      this.lbl_age = new WSI.Cashier.Controls.uc_label();
      this.lbl_wedding_date = new System.Windows.Forms.Label();
      this.uc_dt_wedding = new WSI.Cashier.uc_datetime();
      this.tlp_principal1 = new System.Windows.Forms.TableLayoutPanel();
      this.btn_scan_docs = new WSI.Cashier.Controls.uc_round_button();
      this.cb_marital = new WSI.Cashier.Controls.uc_round_auto_combobox();
      this.lbl_marital_status = new System.Windows.Forms.Label();
      this.cb_occupation = new WSI.Cashier.Controls.uc_round_auto_combobox();
      this.lbl_occupation = new System.Windows.Forms.Label();
      this.cb_birth_country = new WSI.Cashier.Controls.uc_round_auto_combobox();
      this.lbl_birth_country = new System.Windows.Forms.Label();
      this.cb_nationality = new WSI.Cashier.Controls.uc_round_auto_combobox();
      this.lbl_nationality = new System.Windows.Forms.Label();
      this.cb_gender = new WSI.Cashier.Controls.uc_round_auto_combobox();
      this.lbl_gender = new System.Windows.Forms.Label();
      this.lbl_full_name = new System.Windows.Forms.Label();
      this.contact = new System.Windows.Forms.TabPage();
      this.tlp_contact = new System.Windows.Forms.TableLayoutPanel();
      this.lbl_phone_01 = new System.Windows.Forms.Label();
      this.txt_phone_01 = new WSI.Cashier.PhoneNumberTextBox();
      this.lbl_phone_02 = new System.Windows.Forms.Label();
      this.txt_phone_02 = new WSI.Cashier.PhoneNumberTextBox();
      this.txt_email_01 = new WSI.Cashier.EmailTextBox();
      this.lbl_email_01 = new System.Windows.Forms.Label();
      this.lbl_twitter = new System.Windows.Forms.Label();
      this.txt_email_02 = new WSI.Cashier.EmailTextBox();
      this.lbl_email_02 = new System.Windows.Forms.Label();
      this.txt_twitter = new WSI.Cashier.Controls.uc_round_textbox();
      this.address = new System.Windows.Forms.TabPage();
      this.HolderAddress = new WSI.Cashier.uc_address();
      this.beneficiary = new System.Windows.Forms.TabPage();
      this.tlp_ben_principal = new System.Windows.Forms.TableLayoutPanel();
      this.uc_dt_ben_birth = new WSI.Cashier.uc_datetime();
      this.lbl_beneficiary_RFC = new System.Windows.Forms.Label();
      this.txt_beneficiary_RFC = new WSI.Cashier.CharacterTextBox();
      this.lbl_beneficiary_CURP = new System.Windows.Forms.Label();
      this.txt_beneficiary_CURP = new WSI.Cashier.CharacterTextBox();
      this.lbl_beneficiary_birth_date = new System.Windows.Forms.Label();
      this.tlp_ben_principal1 = new System.Windows.Forms.TableLayoutPanel();
      this.lbl_beneficiary_gender = new System.Windows.Forms.Label();
      this.cb_beneficiary_gender = new WSI.Cashier.Controls.uc_round_auto_combobox();
      this.lbl_beneficiary_occupation = new System.Windows.Forms.Label();
      this.cb_beneficiary_occupation = new WSI.Cashier.Controls.uc_round_auto_combobox();
      this.btn_ben_scan_docs = new WSI.Cashier.Controls.uc_round_button();
      this.label1 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.tlp_ben_name = new System.Windows.Forms.TableLayoutPanel();
      this.lbl_beneficiary_name = new System.Windows.Forms.Label();
      this.txt_beneficiary_name = new WSI.Cashier.CharacterTextBox();
      this.lbl_beneficiary_last_name1 = new System.Windows.Forms.Label();
      this.txt_beneficiary_last_name1 = new WSI.Cashier.CharacterTextBox();
      this.lbl_beneficiary_last_name2 = new System.Windows.Forms.Label();
      this.txt_beneficiary_last_name2 = new WSI.Cashier.CharacterTextBox();
      this.chk_holder_has_beneficiary = new WSI.Cashier.Controls.uc_checkBox();
      this.lbl_beneficiary_full_name = new System.Windows.Forms.Label();
      this.points = new System.Windows.Forms.TabPage();
      this.lbl_points_to_maintain_prefix = new WSI.Cashier.Controls.uc_label();
      this.lbl_points_to_maintain = new WSI.Cashier.Controls.uc_label();
      this.lbl_level_prefix = new WSI.Cashier.Controls.uc_label();
      this.gb_next_level = new WSI.Cashier.Controls.uc_round_panel();
      this.lbl_points_last_days = new WSI.Cashier.Controls.uc_label();
      this.lbl_points_discretionaries_prefix = new WSI.Cashier.Controls.uc_label();
      this.lbl_points_discretionaries = new WSI.Cashier.Controls.uc_label();
      this.lbl_points_generated_prefix = new WSI.Cashier.Controls.uc_label();
      this.lbl_points_generated = new WSI.Cashier.Controls.uc_label();
      this.lbl_points_to_next_level = new WSI.Cashier.Controls.uc_label();
      this.lbl_points_to_next_level_prefix = new WSI.Cashier.Controls.uc_label();
      this.lbl_points_level_prefix = new WSI.Cashier.Controls.uc_label();
      this.lbl_points_level = new WSI.Cashier.Controls.uc_label();
      this.lbl_points_req_prefix = new WSI.Cashier.Controls.uc_label();
      this.lbl_points_req = new WSI.Cashier.Controls.uc_label();
      this.lbl_level_entered = new WSI.Cashier.Controls.uc_label();
      this.lbl_points_sufix = new WSI.Cashier.Controls.uc_label();
      this.lbl_level_entered_prefix = new WSI.Cashier.Controls.uc_label();
      this.lbl_points = new WSI.Cashier.Controls.uc_label();
      this.lbl_level_expiration = new WSI.Cashier.Controls.uc_label();
      this.lbl_points_prefix = new WSI.Cashier.Controls.uc_label();
      this.lbl_level_expiration_prefix = new WSI.Cashier.Controls.uc_label();
      this.lbl_level = new WSI.Cashier.Controls.uc_label();
      this.block = new System.Windows.Forms.TabPage();
      this.pb_unblocked = new System.Windows.Forms.PictureBox();
      this.pb_blocked = new System.Windows.Forms.PictureBox();
      this.lbl_block_description = new System.Windows.Forms.Label();
      this.txt_block_description = new WSI.Cashier.Controls.uc_round_textbox();
      this.lbl_blocked = new System.Windows.Forms.Label();
      this.comments = new System.Windows.Forms.TabPage();
      this.chk_show_comments = new WSI.Cashier.Controls.uc_checkBox();
      this.txt_comments = new WSI.Cashier.Controls.uc_round_textbox();
      this.pin = new System.Windows.Forms.TabPage();
      this.lbl_pin_msg = new System.Windows.Forms.Label();
      this.txt_confirm_pin = new WSI.Cashier.NumericTextBox();
      this.txt_pin = new WSI.Cashier.NumericTextBox();
      this.btn_generate_random_pin = new WSI.Cashier.Controls.uc_round_button();
      this.btn_save_pin = new WSI.Cashier.Controls.uc_round_button();
      this.lbl_confirm_pin = new System.Windows.Forms.Label();
      this.lbl_pin = new System.Windows.Forms.Label();
      this.credit_line = new System.Windows.Forms.TabPage();
      this.lbl_credit_line_available = new System.Windows.Forms.Label();
      this.lbl_credit_line_available_value = new System.Windows.Forms.Label();
      this.lbl_credit_line_extension = new System.Windows.Forms.Label();
      this.lbl_credit_line_extension_value = new System.Windows.Forms.Label();
      this.lbl_credit_line_status = new System.Windows.Forms.Label();
      this.lbl_credit_line_status_value = new System.Windows.Forms.Label();
      this.lbl_credit_line_spent = new System.Windows.Forms.Label();
      this.lbl_credit_line_spent_value = new System.Windows.Forms.Label();
      this.lbl_credit_line_limit = new System.Windows.Forms.Label();
      this.btn_credit_line_payback = new WSI.Cashier.Controls.uc_round_button();
      this.lbl_credit_line_limit_value = new System.Windows.Forms.Label();
      this.lbl_level_prefix1 = new WSI.Cashier.Controls.uc_label();
      this.lbl_level1 = new WSI.Cashier.Controls.uc_label();
      this.pnl_data.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.m_img_photo)).BeginInit();
      this.tab_player_edit.SuspendLayout();
      this.principal.SuspendLayout();
      this.tlp_name.SuspendLayout();
      this.tlp_principal.SuspendLayout();
      this.tlp_principal1.SuspendLayout();
      this.contact.SuspendLayout();
      this.tlp_contact.SuspendLayout();
      this.address.SuspendLayout();
      this.beneficiary.SuspendLayout();
      this.tlp_ben_principal.SuspendLayout();
      this.tlp_ben_principal1.SuspendLayout();
      this.tlp_ben_name.SuspendLayout();
      this.points.SuspendLayout();
      this.gb_next_level.SuspendLayout();
      this.block.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pb_unblocked)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.pb_blocked)).BeginInit();
      this.comments.SuspendLayout();
      this.pin.SuspendLayout();
      this.credit_line.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.Controls.Add(this.lbl_level1);
      this.pnl_data.Controls.Add(this.lbl_level_prefix1);
      this.pnl_data.Controls.Add(this.m_img_photo);
      this.pnl_data.Controls.Add(this.lbl_msg_count_blink);
      this.pnl_data.Controls.Add(this.lbl_msg_blink);
      this.pnl_data.Controls.Add(this.btn_keyboard);
      this.pnl_data.Controls.Add(this.btn_id_card_scan);
      this.pnl_data.Controls.Add(this.btn_print_information);
      this.pnl_data.Controls.Add(this.btn_cancel);
      this.pnl_data.Controls.Add(this.btn_ok);
      this.pnl_data.Controls.Add(this.tab_player_edit);
      this.pnl_data.Size = new System.Drawing.Size(1018, 565);
      // 
      // lbl_account_edit_title
      // 
      this.lbl_account_edit_title.AutoSize = true;
      this.lbl_account_edit_title.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_account_edit_title.ForeColor = System.Drawing.Color.White;
      this.lbl_account_edit_title.Location = new System.Drawing.Point(3, 5);
      this.lbl_account_edit_title.Name = "lbl_account_edit_title";
      this.lbl_account_edit_title.Size = new System.Drawing.Size(183, 16);
      this.lbl_account_edit_title.TabIndex = 0;
      this.lbl_account_edit_title.Text = "xAccount Data Edition (C)";
      // 
      // m_img_photo
      // 
      this.m_img_photo.BackgroundImage = global::WSI.Cashier.Properties.Resources.anonymous_user;
      this.m_img_photo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
      this.m_img_photo.Location = new System.Drawing.Point(3, 6);
      this.m_img_photo.Name = "m_img_photo";
      this.m_img_photo.Size = new System.Drawing.Size(98, 100);
      this.m_img_photo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
      this.m_img_photo.TabIndex = 39;
      this.m_img_photo.TabStop = false;
      this.m_img_photo.Tag = "HolderPhoto";
      this.m_img_photo.Click += new System.EventHandler(this.m_img_photo_Click);
      // 
      // lbl_msg_count_blink
      // 
      this.lbl_msg_count_blink.BackColor = System.Drawing.Color.Red;
      this.lbl_msg_count_blink.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_msg_count_blink.ForeColor = System.Drawing.Color.White;
      this.lbl_msg_count_blink.Location = new System.Drawing.Point(57, 527);
      this.lbl_msg_count_blink.Name = "lbl_msg_count_blink";
      this.lbl_msg_count_blink.Size = new System.Drawing.Size(62, 23);
      this.lbl_msg_count_blink.TabIndex = 38;
      this.lbl_msg_count_blink.Text = "xPERSONAL NAME CANNOT BE BLANK";
      this.lbl_msg_count_blink.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.lbl_msg_count_blink.UseMnemonic = false;
      this.lbl_msg_count_blink.Visible = false;
      // 
      // lbl_msg_blink
      // 
      this.lbl_msg_blink.BackColor = System.Drawing.Color.Red;
      this.lbl_msg_blink.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_msg_blink.ForeColor = System.Drawing.Color.White;
      this.lbl_msg_blink.Location = new System.Drawing.Point(118, 527);
      this.lbl_msg_blink.Name = "lbl_msg_blink";
      this.lbl_msg_blink.Size = new System.Drawing.Size(465, 23);
      this.lbl_msg_blink.TabIndex = 37;
      this.lbl_msg_blink.Text = "xPERSONAL NAME CANNOT BE BLANK";
      this.lbl_msg_blink.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.lbl_msg_blink.UseMnemonic = false;
      this.lbl_msg_blink.Visible = false;
      // 
      // btn_keyboard
      // 
      this.btn_keyboard.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(47)))), ((int)(((byte)(50)))));
      this.btn_keyboard.FlatAppearance.BorderSize = 0;
      this.btn_keyboard.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_keyboard.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_keyboard.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_keyboard.Image = global::WSI.Cashier.Properties.Resources.keyboard;
      this.btn_keyboard.IsSelected = false;
      this.btn_keyboard.Location = new System.Drawing.Point(6, 522);
      this.btn_keyboard.Name = "btn_keyboard";
      this.btn_keyboard.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_keyboard.Size = new System.Drawing.Size(45, 34);
      this.btn_keyboard.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.TERTIARY;
      this.btn_keyboard.TabIndex = 36;
      this.btn_keyboard.TabStop = false;
      this.btn_keyboard.UseVisualStyleBackColor = false;
      this.btn_keyboard.Click += new System.EventHandler(this.btn_keyboard_Click);
      // 
      // btn_id_card_scan
      // 
      this.btn_id_card_scan.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(47)))), ((int)(((byte)(50)))));
      this.btn_id_card_scan.FlatAppearance.BorderSize = 0;
      this.btn_id_card_scan.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_id_card_scan.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_id_card_scan.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_id_card_scan.Image = null;
      this.btn_id_card_scan.IsSelected = false;
      this.btn_id_card_scan.Location = new System.Drawing.Point(590, 515);
      this.btn_id_card_scan.Name = "btn_id_card_scan";
      this.btn_id_card_scan.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_id_card_scan.Size = new System.Drawing.Size(100, 48);
      this.btn_id_card_scan.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.TERTIARY;
      this.btn_id_card_scan.TabIndex = 35;
      this.btn_id_card_scan.TabStop = false;
      this.btn_id_card_scan.Text = "ID CARD SCAN";
      this.btn_id_card_scan.UseVisualStyleBackColor = false;
      this.btn_id_card_scan.LongClick += new System.EventHandler(this.btn_id_card_scan_LongClick);
      this.btn_id_card_scan.Click += new System.EventHandler(this.btn_id_card_scan_Click);
      // 
      // btn_print_information
      // 
      this.btn_print_information.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(47)))), ((int)(((byte)(50)))));
      this.btn_print_information.FlatAppearance.BorderSize = 0;
      this.btn_print_information.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_print_information.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_print_information.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_print_information.Image = null;
      this.btn_print_information.IsSelected = false;
      this.btn_print_information.Location = new System.Drawing.Point(696, 515);
      this.btn_print_information.Name = "btn_print_information";
      this.btn_print_information.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_print_information.Size = new System.Drawing.Size(100, 48);
      this.btn_print_information.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.TERTIARY;
      this.btn_print_information.TabIndex = 32;
      this.btn_print_information.TabStop = false;
      this.btn_print_information.Text = "XPRINT INFORMATION";
      this.btn_print_information.UseVisualStyleBackColor = false;
      this.btn_print_information.Click += new System.EventHandler(this.btn_print_information_Click);
      // 
      // btn_cancel
      // 
      this.btn_cancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btn_cancel.FlatAppearance.BorderSize = 0;
      this.btn_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_cancel.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_cancel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_cancel.Image = null;
      this.btn_cancel.IsSelected = false;
      this.btn_cancel.Location = new System.Drawing.Point(801, 515);
      this.btn_cancel.Name = "btn_cancel";
      this.btn_cancel.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_cancel.Size = new System.Drawing.Size(100, 48);
      this.btn_cancel.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_cancel.TabIndex = 34;
      this.btn_cancel.TabStop = false;
      this.btn_cancel.Text = "XCANCEL";
      this.btn_cancel.UseVisualStyleBackColor = false;
      this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
      // 
      // btn_ok
      // 
      this.btn_ok.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_ok.FlatAppearance.BorderSize = 0;
      this.btn_ok.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_ok.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_ok.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_ok.Image = null;
      this.btn_ok.IsSelected = false;
      this.btn_ok.Location = new System.Drawing.Point(907, 515);
      this.btn_ok.Name = "btn_ok";
      this.btn_ok.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_ok.Size = new System.Drawing.Size(100, 48);
      this.btn_ok.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_ok.TabIndex = 33;
      this.btn_ok.TabStop = false;
      this.btn_ok.Text = "XOK";
      this.btn_ok.UseVisualStyleBackColor = false;
      this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
      // 
      // tab_player_edit
      // 
      this.tab_player_edit.Controls.Add(this.principal);
      this.tab_player_edit.Controls.Add(this.contact);
      this.tab_player_edit.Controls.Add(this.address);
      this.tab_player_edit.Controls.Add(this.beneficiary);
      this.tab_player_edit.Controls.Add(this.points);
      this.tab_player_edit.Controls.Add(this.block);
      this.tab_player_edit.Controls.Add(this.comments);
      this.tab_player_edit.Controls.Add(this.pin);
      this.tab_player_edit.Controls.Add(this.credit_line);
      this.tab_player_edit.DisplayStyle = WSI.Cashier.Controls.TabStyle.Rounded;
      // 
      // 
      // 
      this.tab_player_edit.DisplayStyleProvider.BorderColor = System.Drawing.SystemColors.ControlDark;
      this.tab_player_edit.DisplayStyleProvider.BorderColorHot = System.Drawing.SystemColors.ControlDark;
      this.tab_player_edit.DisplayStyleProvider.BorderColorSelected = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(157)))), ((int)(((byte)(185)))));
      this.tab_player_edit.DisplayStyleProvider.FocusTrack = true;
      this.tab_player_edit.DisplayStyleProvider.HotTrack = true;
      this.tab_player_edit.DisplayStyleProvider.Opacity = 1F;
      this.tab_player_edit.DisplayStyleProvider.Overlap = 0;
      this.tab_player_edit.DisplayStyleProvider.Padding = new System.Drawing.Point(6, 3);
      this.tab_player_edit.DisplayStyleProvider.Radius = 10;
      this.tab_player_edit.DisplayStyleProvider.TabColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.tab_player_edit.DisplayStyleProvider.TabColorHeader = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.tab_player_edit.DisplayStyleProvider.TabColorSelected = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.tab_player_edit.DisplayStyleProvider.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.tab_player_edit.DisplayStyleProvider.TextColorSelected = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.tab_player_edit.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.tab_player_edit.HotTrack = true;
      this.tab_player_edit.ItemSize = new System.Drawing.Size(449, 40);
      this.tab_player_edit.Location = new System.Drawing.Point(102, 3);
      this.tab_player_edit.Name = "tab_player_edit";
      this.tab_player_edit.SelectedIndex = 0;
      this.tab_player_edit.Size = new System.Drawing.Size(909, 502);
      this.tab_player_edit.TabIndex = 2;
      this.tab_player_edit.TabStop = false;
      this.tab_player_edit.Tag = "";
      // 
      // principal
      // 
      this.principal.Controls.Add(this.tlp_name);
      this.principal.Controls.Add(this.tlp_principal);
      this.principal.Controls.Add(this.tlp_principal1);
      this.principal.Controls.Add(this.lbl_full_name);
      this.principal.Location = new System.Drawing.Point(4, 45);
      this.principal.Name = "principal";
      this.principal.Padding = new System.Windows.Forms.Padding(3);
      this.principal.Size = new System.Drawing.Size(901, 453);
      this.principal.TabIndex = 0;
      this.principal.Text = "xPrincipal";
      this.principal.UseVisualStyleBackColor = true;
      // 
      // tlp_name
      // 
      this.tlp_name.ColumnCount = 4;
      this.tlp_name.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
      this.tlp_name.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
      this.tlp_name.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
      this.tlp_name.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
      this.tlp_name.Controls.Add(this.txt_name4, 1, 1);
      this.tlp_name.Controls.Add(this.lbl_name4, 1, 0);
      this.tlp_name.Controls.Add(this.txt_name, 0, 1);
      this.tlp_name.Controls.Add(this.lbl_name, 0, 0);
      this.tlp_name.Controls.Add(this.lbl_last_name1, 2, 0);
      this.tlp_name.Controls.Add(this.txt_last_name1, 2, 1);
      this.tlp_name.Controls.Add(this.txt_last_name2, 3, 1);
      this.tlp_name.Controls.Add(this.lbl_last_name2, 3, 0);
      this.tlp_name.Location = new System.Drawing.Point(4, 21);
      this.tlp_name.Name = "tlp_name";
      this.tlp_name.RowCount = 2;
      this.tlp_name.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_name.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_name.Size = new System.Drawing.Size(887, 71);
      this.tlp_name.TabIndex = 0;
      // 
      // txt_name4
      // 
      this.txt_name4.AllowSpace = true;
      this.txt_name4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
      this.txt_name4.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_name4.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_name4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_name4.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_name4.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.txt_name4.CornerRadius = 5;
      this.txt_name4.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_name4.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.BaseMode;
      this.txt_name4.Location = new System.Drawing.Point(224, 25);
      this.txt_name4.MaxLength = 50;
      this.txt_name4.Multiline = false;
      this.txt_name4.Name = "txt_name4";
      this.txt_name4.PasswordChar = '\0';
      this.txt_name4.ReadOnly = false;
      this.txt_name4.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_name4.SelectedText = "";
      this.txt_name4.SelectionLength = 0;
      this.txt_name4.SelectionStart = 0;
      this.txt_name4.Size = new System.Drawing.Size(215, 40);
      this.txt_name4.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_name4.TabIndex = 1;
      this.txt_name4.TabStop = false;
      this.txt_name4.Tag = "HolderName4";
      this.txt_name4.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_name4.UseSystemPasswordChar = false;
      this.txt_name4.WaterMark = null;
      this.txt_name4.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // lbl_name4
      // 
      this.lbl_name4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_name4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
      this.lbl_name4.Location = new System.Drawing.Point(224, 0);
      this.lbl_name4.Name = "lbl_name4";
      this.lbl_name4.Size = new System.Drawing.Size(215, 18);
      this.lbl_name4.TabIndex = 1002;
      this.lbl_name4.Tag = "lHolderName4";
      this.lbl_name4.Text = "xHolderName4";
      this.lbl_name4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // txt_name
      // 
      this.txt_name.AllowSpace = true;
      this.txt_name.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
      this.txt_name.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_name.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_name.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_name.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_name.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.txt_name.CornerRadius = 5;
      this.txt_name.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_name.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.BaseMode;
      this.txt_name.Location = new System.Drawing.Point(3, 25);
      this.txt_name.MaxLength = 50;
      this.txt_name.Multiline = false;
      this.txt_name.Name = "txt_name";
      this.txt_name.PasswordChar = '\0';
      this.txt_name.ReadOnly = false;
      this.txt_name.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_name.SelectedText = "";
      this.txt_name.SelectionLength = 0;
      this.txt_name.SelectionStart = 0;
      this.txt_name.Size = new System.Drawing.Size(215, 40);
      this.txt_name.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_name.TabIndex = 0;
      this.txt_name.TabStop = false;
      this.txt_name.Tag = "HolderName3";
      this.txt_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_name.UseSystemPasswordChar = false;
      this.txt_name.WaterMark = null;
      this.txt_name.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      this.txt_name.TextChanged += new System.EventHandler(this.txt_name_TextChanged);
      // 
      // lbl_name
      // 
      this.lbl_name.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
      this.lbl_name.Location = new System.Drawing.Point(3, 0);
      this.lbl_name.Name = "lbl_name";
      this.lbl_name.Size = new System.Drawing.Size(215, 18);
      this.lbl_name.TabIndex = 1001;
      this.lbl_name.Tag = "lHolderName3";
      this.lbl_name.Text = "xHolderName3";
      this.lbl_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_last_name1
      // 
      this.lbl_last_name1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_last_name1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
      this.lbl_last_name1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.lbl_last_name1.Location = new System.Drawing.Point(445, 0);
      this.lbl_last_name1.Name = "lbl_last_name1";
      this.lbl_last_name1.Size = new System.Drawing.Size(215, 19);
      this.lbl_last_name1.TabIndex = 1001;
      this.lbl_last_name1.Tag = "lHolderName1";
      this.lbl_last_name1.Text = "xHolderName1";
      this.lbl_last_name1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // txt_last_name1
      // 
      this.txt_last_name1.AllowSpace = true;
      this.txt_last_name1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
      this.txt_last_name1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_last_name1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_last_name1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_last_name1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_last_name1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.txt_last_name1.CornerRadius = 5;
      this.txt_last_name1.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_last_name1.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.BaseMode;
      this.txt_last_name1.Location = new System.Drawing.Point(445, 25);
      this.txt_last_name1.MaxLength = 50;
      this.txt_last_name1.Multiline = false;
      this.txt_last_name1.Name = "txt_last_name1";
      this.txt_last_name1.PasswordChar = '\0';
      this.txt_last_name1.ReadOnly = false;
      this.txt_last_name1.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_last_name1.SelectedText = "";
      this.txt_last_name1.SelectionLength = 0;
      this.txt_last_name1.SelectionStart = 0;
      this.txt_last_name1.Size = new System.Drawing.Size(215, 40);
      this.txt_last_name1.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_last_name1.TabIndex = 2;
      this.txt_last_name1.TabStop = false;
      this.txt_last_name1.Tag = "HolderName1";
      this.txt_last_name1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_last_name1.UseSystemPasswordChar = false;
      this.txt_last_name1.WaterMark = null;
      this.txt_last_name1.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // txt_last_name2
      // 
      this.txt_last_name2.AllowSpace = true;
      this.txt_last_name2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
      this.txt_last_name2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_last_name2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_last_name2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_last_name2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_last_name2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.txt_last_name2.CornerRadius = 5;
      this.txt_last_name2.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_last_name2.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.BaseMode;
      this.txt_last_name2.Location = new System.Drawing.Point(666, 25);
      this.txt_last_name2.MaxLength = 50;
      this.txt_last_name2.Multiline = false;
      this.txt_last_name2.Name = "txt_last_name2";
      this.txt_last_name2.PasswordChar = '\0';
      this.txt_last_name2.ReadOnly = false;
      this.txt_last_name2.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_last_name2.SelectedText = "";
      this.txt_last_name2.SelectionLength = 0;
      this.txt_last_name2.SelectionStart = 0;
      this.txt_last_name2.Size = new System.Drawing.Size(218, 40);
      this.txt_last_name2.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_last_name2.TabIndex = 3;
      this.txt_last_name2.TabStop = false;
      this.txt_last_name2.Tag = "HolderName2";
      this.txt_last_name2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_last_name2.UseSystemPasswordChar = false;
      this.txt_last_name2.WaterMark = null;
      this.txt_last_name2.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // lbl_last_name2
      // 
      this.lbl_last_name2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_last_name2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
      this.lbl_last_name2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.lbl_last_name2.Location = new System.Drawing.Point(666, 0);
      this.lbl_last_name2.Name = "lbl_last_name2";
      this.lbl_last_name2.Size = new System.Drawing.Size(218, 19);
      this.lbl_last_name2.TabIndex = 1001;
      this.lbl_last_name2.Tag = "lHolderName2";
      this.lbl_last_name2.Text = "xHolderName4";
      this.lbl_last_name2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // tlp_principal
      // 
      this.tlp_principal.ColumnCount = 3;
      this.tlp_principal.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
      this.tlp_principal.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 222F));
      this.tlp_principal.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 128F));
      this.tlp_principal.Controls.Add(this.lbl_type_document, 0, 0);
      this.tlp_principal.Controls.Add(this.cb_document_type, 1, 0);
      this.tlp_principal.Controls.Add(this.lbl_docuemnt_counter, 0, 1);
      this.tlp_principal.Controls.Add(this.lbl_document, 0, 2);
      this.tlp_principal.Controls.Add(this.txt_document, 1, 2);
      this.tlp_principal.Controls.Add(this.lbl_expiration_doc_date, 0, 3);
      this.tlp_principal.Controls.Add(this.uc_dt_expiration_doc, 1, 3);
      this.tlp_principal.Controls.Add(this.txt_RFC, 1, 4);
      this.tlp_principal.Controls.Add(this.btn_RFC, 0, 4);
      this.tlp_principal.Controls.Add(this.lbl_CURP, 0, 5);
      this.tlp_principal.Controls.Add(this.txt_CURP, 1, 5);
      this.tlp_principal.Controls.Add(this.uc_dt_birth, 1, 6);
      this.tlp_principal.Controls.Add(this.lbl_birth_date, 0, 6);
      this.tlp_principal.Controls.Add(this.lbl_age, 2, 6);
      this.tlp_principal.Controls.Add(this.lbl_wedding_date, 0, 7);
      this.tlp_principal.Controls.Add(this.uc_dt_wedding, 1, 7);
      this.tlp_principal.Location = new System.Drawing.Point(7, 118);
      this.tlp_principal.Name = "tlp_principal";
      this.tlp_principal.RowCount = 9;
      this.tlp_principal.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
      this.tlp_principal.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
      this.tlp_principal.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_principal.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_principal.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_principal.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_principal.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_principal.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_principal.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_principal.Size = new System.Drawing.Size(468, 358);
      this.tlp_principal.TabIndex = 1;
      // 
      // lbl_type_document
      // 
      this.lbl_type_document.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_type_document.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
      this.lbl_type_document.Location = new System.Drawing.Point(3, 0);
      this.lbl_type_document.Name = "lbl_type_document";
      this.lbl_type_document.Size = new System.Drawing.Size(144, 23);
      this.lbl_type_document.TabIndex = 1001;
      this.lbl_type_document.Tag = "lHolderDocumentType";
      this.lbl_type_document.Text = "xDocumentType";
      this.lbl_type_document.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // cb_document_type
      // 
      this.cb_document_type.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_document_type.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.cb_document_type.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.cb_document_type.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.tlp_principal.SetColumnSpan(this.cb_document_type, 2);
      this.cb_document_type.CornerRadius = 5;
      this.cb_document_type.DataSource = null;
      this.cb_document_type.DisplayMember = "";
      this.cb_document_type.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
      this.cb_document_type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cb_document_type.DropDownWidth = 258;
      this.cb_document_type.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.cb_document_type.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_document_type.FormattingEnabled = true;
      this.cb_document_type.FormatValidator = null;
      this.cb_document_type.IsDroppedDown = false;
      this.cb_document_type.ItemHeight = 40;
      this.cb_document_type.Location = new System.Drawing.Point(153, 3);
      this.cb_document_type.MaxDropDownItems = 8;
      this.cb_document_type.Name = "cb_document_type";
      this.cb_document_type.OnFocusOpenListBox = true;
      this.cb_document_type.SelectedIndex = -1;
      this.cb_document_type.SelectedItem = null;
      this.cb_document_type.SelectedValue = null;
      this.cb_document_type.SelectionLength = 0;
      this.cb_document_type.SelectionStart = 0;
      this.cb_document_type.Size = new System.Drawing.Size(258, 40);
      this.cb_document_type.Sorted = false;
      this.cb_document_type.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.BASIC;
      this.cb_document_type.TabIndex = 0;
      this.cb_document_type.TabStop = false;
      this.cb_document_type.Tag = "HolderDocumentType";
      this.cb_document_type.ValueMember = "";
      this.cb_document_type.SelectedIndexChanged += new System.EventHandler(this.cb_document_type_SelectedIndexChanged);
      // 
      // lbl_docuemnt_counter
      // 
      this.lbl_docuemnt_counter.AutoSize = true;
      this.lbl_docuemnt_counter.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_docuemnt_counter.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
      this.lbl_docuemnt_counter.Location = new System.Drawing.Point(3, 23);
      this.lbl_docuemnt_counter.Name = "lbl_docuemnt_counter";
      this.lbl_docuemnt_counter.Size = new System.Drawing.Size(144, 23);
      this.lbl_docuemnt_counter.TabIndex = 1005;
      this.lbl_docuemnt_counter.Text = "xDoc";
      this.lbl_docuemnt_counter.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_document
      // 
      this.lbl_document.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_document.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
      this.lbl_document.Location = new System.Drawing.Point(3, 46);
      this.lbl_document.Name = "lbl_document";
      this.lbl_document.Size = new System.Drawing.Size(144, 46);
      this.lbl_document.TabIndex = 1001;
      this.lbl_document.Tag = "lHolderDocument";
      this.lbl_document.Text = "xDocument";
      this.lbl_document.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // txt_document
      // 
      this.txt_document.AllowSpace = true;
      this.txt_document.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_document.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_document.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_document.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_document.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.tlp_principal.SetColumnSpan(this.txt_document, 2);
      this.txt_document.CornerRadius = 5;
      this.txt_document.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_document.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.ExtendedMode;
      this.txt_document.Location = new System.Drawing.Point(153, 49);
      this.txt_document.MaxLength = 20;
      this.txt_document.Multiline = false;
      this.txt_document.Name = "txt_document";
      this.txt_document.PasswordChar = '\0';
      this.txt_document.ReadOnly = false;
      this.txt_document.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_document.SelectedText = "";
      this.txt_document.SelectionLength = 0;
      this.txt_document.SelectionStart = 0;
      this.txt_document.Size = new System.Drawing.Size(258, 40);
      this.txt_document.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_document.TabIndex = 1;
      this.txt_document.TabStop = false;
      this.txt_document.Tag = "HolderDocument";
      this.txt_document.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_document.UseSystemPasswordChar = false;
      this.txt_document.WaterMark = null;
      this.txt_document.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      this.txt_document.Leave += new System.EventHandler(this.txt_doc_leave);
      // 
      // lbl_expiration_doc_date
      // 
      this.lbl_expiration_doc_date.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_expiration_doc_date.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
      this.lbl_expiration_doc_date.Location = new System.Drawing.Point(3, 92);
      this.lbl_expiration_doc_date.Name = "lbl_expiration_doc_date";
      this.lbl_expiration_doc_date.Size = new System.Drawing.Size(144, 54);
      this.lbl_expiration_doc_date.TabIndex = 1007;
      this.lbl_expiration_doc_date.Tag = "lHolderExpirationDocDate";
      this.lbl_expiration_doc_date.Text = "xExpiration Date";
      this.lbl_expiration_doc_date.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // uc_dt_expiration_doc
      // 
      this.uc_dt_expiration_doc.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
      this.uc_dt_expiration_doc.DateText = "";
      this.uc_dt_expiration_doc.DateTextFont = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.uc_dt_expiration_doc.DateTextWidth = 0;
      this.uc_dt_expiration_doc.DateValue = new System.DateTime(((long)(0)));
      this.uc_dt_expiration_doc.Dock = System.Windows.Forms.DockStyle.Fill;
      this.uc_dt_expiration_doc.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.uc_dt_expiration_doc.ForeColor = System.Drawing.SystemColors.ControlLightLight;
      this.uc_dt_expiration_doc.FormatFont = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.uc_dt_expiration_doc.FormatWidth = 74;
      this.uc_dt_expiration_doc.Invalid = false;
      this.uc_dt_expiration_doc.Location = new System.Drawing.Point(153, 95);
      this.uc_dt_expiration_doc.Name = "uc_dt_expiration_doc";
      this.uc_dt_expiration_doc.Size = new System.Drawing.Size(216, 48);
      this.uc_dt_expiration_doc.TabIndex = 6;
      this.uc_dt_expiration_doc.Tag = "HolderExpirationDocDate";
      this.uc_dt_expiration_doc.ValuesFont = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.uc_dt_expiration_doc.OnDateChanged += new System.EventHandler(this.uc_dt_expiration_doc_OnDateChanged);
      // 
      // txt_RFC
      // 
      this.txt_RFC.AllowSpace = true;
      this.txt_RFC.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_RFC.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_RFC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_RFC.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_RFC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.tlp_principal.SetColumnSpan(this.txt_RFC, 2);
      this.txt_RFC.CornerRadius = 5;
      this.txt_RFC.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_RFC.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.ExtendedMode;
      this.txt_RFC.Location = new System.Drawing.Point(153, 149);
      this.txt_RFC.MaxLength = 13;
      this.txt_RFC.Multiline = false;
      this.txt_RFC.Name = "txt_RFC";
      this.txt_RFC.PasswordChar = '\0';
      this.txt_RFC.ReadOnly = false;
      this.txt_RFC.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_RFC.SelectedText = "";
      this.txt_RFC.SelectionLength = 0;
      this.txt_RFC.SelectionStart = 0;
      this.txt_RFC.Size = new System.Drawing.Size(258, 40);
      this.txt_RFC.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_RFC.TabIndex = 2;
      this.txt_RFC.TabStop = false;
      this.txt_RFC.Tag = "HolderRFC";
      this.txt_RFC.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_RFC.UseSystemPasswordChar = false;
      this.txt_RFC.WaterMark = null;
      this.txt_RFC.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      this.txt_RFC.TextChanged += new System.EventHandler(this.txt_RFC_TextChanged);
      // 
      // btn_RFC
      // 
      this.btn_RFC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_RFC.Dock = System.Windows.Forms.DockStyle.Fill;
      this.btn_RFC.FlatAppearance.BorderSize = 0;
      this.btn_RFC.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_RFC.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_RFC.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_RFC.Image = null;
      this.btn_RFC.IsSelected = false;
      this.btn_RFC.Location = new System.Drawing.Point(3, 149);
      this.btn_RFC.Name = "btn_RFC";
      this.btn_RFC.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_RFC.Size = new System.Drawing.Size(144, 40);
      this.btn_RFC.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_RFC.TabIndex = 5;
      this.btn_RFC.Tag = "lHolderRFC";
      this.btn_RFC.Text = "XRFC";
      this.btn_RFC.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      this.btn_RFC.UseVisualStyleBackColor = false;
      this.btn_RFC.Click += new System.EventHandler(this.btn_RFC_Click);
      // 
      // lbl_CURP
      // 
      this.lbl_CURP.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_CURP.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
      this.lbl_CURP.Location = new System.Drawing.Point(3, 192);
      this.lbl_CURP.Name = "lbl_CURP";
      this.lbl_CURP.Size = new System.Drawing.Size(144, 46);
      this.lbl_CURP.TabIndex = 1004;
      this.lbl_CURP.Tag = "lHolderCURP";
      this.lbl_CURP.Text = "xCURP";
      this.lbl_CURP.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // txt_CURP
      // 
      this.txt_CURP.AllowSpace = true;
      this.txt_CURP.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_CURP.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_CURP.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_CURP.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_CURP.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.tlp_principal.SetColumnSpan(this.txt_CURP, 2);
      this.txt_CURP.CornerRadius = 5;
      this.txt_CURP.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_CURP.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.ExtendedMode;
      this.txt_CURP.Location = new System.Drawing.Point(153, 195);
      this.txt_CURP.MaxLength = 20;
      this.txt_CURP.Multiline = false;
      this.txt_CURP.Name = "txt_CURP";
      this.txt_CURP.PasswordChar = '\0';
      this.txt_CURP.ReadOnly = false;
      this.txt_CURP.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_CURP.SelectedText = "";
      this.txt_CURP.SelectionLength = 0;
      this.txt_CURP.SelectionStart = 0;
      this.txt_CURP.Size = new System.Drawing.Size(258, 40);
      this.txt_CURP.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_CURP.TabIndex = 3;
      this.txt_CURP.TabStop = false;
      this.txt_CURP.Tag = "HolderCURP";
      this.txt_CURP.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_CURP.UseSystemPasswordChar = false;
      this.txt_CURP.WaterMark = null;
      this.txt_CURP.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // uc_dt_birth
      // 
      this.uc_dt_birth.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
      this.uc_dt_birth.DateText = "";
      this.uc_dt_birth.DateTextFont = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.uc_dt_birth.DateTextWidth = 0;
      this.uc_dt_birth.DateValue = new System.DateTime(((long)(0)));
      this.uc_dt_birth.Dock = System.Windows.Forms.DockStyle.Fill;
      this.uc_dt_birth.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.uc_dt_birth.ForeColor = System.Drawing.SystemColors.ControlLightLight;
      this.uc_dt_birth.FormatFont = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.uc_dt_birth.FormatWidth = 74;
      this.uc_dt_birth.Invalid = false;
      this.uc_dt_birth.Location = new System.Drawing.Point(153, 241);
      this.uc_dt_birth.Name = "uc_dt_birth";
      this.uc_dt_birth.Size = new System.Drawing.Size(216, 48);
      this.uc_dt_birth.TabIndex = 4;
      this.uc_dt_birth.Tag = "HolderBirthDate";
      this.uc_dt_birth.ValuesFont = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.uc_dt_birth.OnDateChanged += new System.EventHandler(this.uc_dt_birth_OnDateChanged);
      // 
      // lbl_birth_date
      // 
      this.lbl_birth_date.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_birth_date.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
      this.lbl_birth_date.Location = new System.Drawing.Point(3, 238);
      this.lbl_birth_date.Name = "lbl_birth_date";
      this.lbl_birth_date.Size = new System.Drawing.Size(144, 54);
      this.lbl_birth_date.TabIndex = 1001;
      this.lbl_birth_date.Tag = "lHolderBirthDate";
      this.lbl_birth_date.Text = "xBirthDay";
      this.lbl_birth_date.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_age
      // 
      this.lbl_age.AutoSize = true;
      this.lbl_age.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_age.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_age.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_age.Location = new System.Drawing.Point(375, 238);
      this.lbl_age.Name = "lbl_age";
      this.lbl_age.Size = new System.Drawing.Size(122, 54);
      this.lbl_age.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_age.TabIndex = 1006;
      this.lbl_age.Text = "uc_label1";
      this.lbl_age.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_wedding_date
      // 
      this.lbl_wedding_date.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_wedding_date.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
      this.lbl_wedding_date.Location = new System.Drawing.Point(3, 292);
      this.lbl_wedding_date.Name = "lbl_wedding_date";
      this.lbl_wedding_date.Size = new System.Drawing.Size(144, 54);
      this.lbl_wedding_date.TabIndex = 1001;
      this.lbl_wedding_date.Tag = "lHolderWeddingDate";
      this.lbl_wedding_date.Text = "xWedding Date";
      this.lbl_wedding_date.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // uc_dt_wedding
      // 
      this.uc_dt_wedding.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
      this.uc_dt_wedding.DateText = "";
      this.uc_dt_wedding.DateTextFont = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.uc_dt_wedding.DateTextWidth = 0;
      this.uc_dt_wedding.DateValue = new System.DateTime(((long)(0)));
      this.uc_dt_wedding.Dock = System.Windows.Forms.DockStyle.Fill;
      this.uc_dt_wedding.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.uc_dt_wedding.ForeColor = System.Drawing.SystemColors.ControlLightLight;
      this.uc_dt_wedding.FormatFont = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.uc_dt_wedding.FormatWidth = 74;
      this.uc_dt_wedding.Invalid = false;
      this.uc_dt_wedding.Location = new System.Drawing.Point(153, 295);
      this.uc_dt_wedding.Name = "uc_dt_wedding";
      this.uc_dt_wedding.Size = new System.Drawing.Size(216, 48);
      this.uc_dt_wedding.TabIndex = 5;
      this.uc_dt_wedding.Tag = "HolderWeddingDate";
      this.uc_dt_wedding.ValuesFont = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      // 
      // tlp_principal1
      // 
      this.tlp_principal1.ColumnCount = 2;
      this.tlp_principal1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
      this.tlp_principal1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 300F));
      this.tlp_principal1.Controls.Add(this.btn_scan_docs, 1, 0);
      this.tlp_principal1.Controls.Add(this.cb_marital, 1, 5);
      this.tlp_principal1.Controls.Add(this.lbl_marital_status, 0, 5);
      this.tlp_principal1.Controls.Add(this.cb_occupation, 1, 4);
      this.tlp_principal1.Controls.Add(this.lbl_occupation, 0, 4);
      this.tlp_principal1.Controls.Add(this.cb_birth_country, 1, 3);
      this.tlp_principal1.Controls.Add(this.lbl_birth_country, 0, 3);
      this.tlp_principal1.Controls.Add(this.cb_nationality, 1, 2);
      this.tlp_principal1.Controls.Add(this.lbl_nationality, 0, 2);
      this.tlp_principal1.Controls.Add(this.cb_gender, 1, 1);
      this.tlp_principal1.Controls.Add(this.lbl_gender, 0, 1);
      this.tlp_principal1.Location = new System.Drawing.Point(480, 118);
      this.tlp_principal1.Name = "tlp_principal1";
      this.tlp_principal1.RowCount = 7;
      this.tlp_principal1.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_principal1.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_principal1.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_principal1.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_principal1.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_principal1.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_principal1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
      this.tlp_principal1.Size = new System.Drawing.Size(412, 267);
      this.tlp_principal1.TabIndex = 2;
      this.tlp_principal1.Tag = "lHolderDocScan";
      // 
      // btn_scan_docs
      // 
      this.btn_scan_docs.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(47)))), ((int)(((byte)(50)))));
      this.btn_scan_docs.FlatAppearance.BorderSize = 0;
      this.btn_scan_docs.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_scan_docs.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_scan_docs.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_scan_docs.Image = null;
      this.btn_scan_docs.IsSelected = false;
      this.btn_scan_docs.Location = new System.Drawing.Point(153, 3);
      this.btn_scan_docs.Name = "btn_scan_docs";
      this.btn_scan_docs.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_scan_docs.Size = new System.Drawing.Size(257, 34);
      this.btn_scan_docs.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.TERTIARY;
      this.btn_scan_docs.TabIndex = 0;
      this.btn_scan_docs.TabStop = false;
      this.btn_scan_docs.Tag = "HolderDocScan";
      this.btn_scan_docs.Text = "XDOCUMENT DIGITIZING";
      this.btn_scan_docs.UseVisualStyleBackColor = false;
      this.btn_scan_docs.Click += new System.EventHandler(this.btn_scan_docs_Click);
      // 
      // cb_marital
      // 
      this.cb_marital.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_marital.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.cb_marital.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.cb_marital.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.cb_marital.CornerRadius = 5;
      this.cb_marital.DataSource = null;
      this.cb_marital.DisplayMember = "";
      this.cb_marital.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
      this.cb_marital.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cb_marital.DropDownWidth = 258;
      this.cb_marital.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.cb_marital.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_marital.FormattingEnabled = true;
      this.cb_marital.FormatValidator = null;
      this.cb_marital.IsDroppedDown = false;
      this.cb_marital.ItemHeight = 40;
      this.cb_marital.Location = new System.Drawing.Point(153, 227);
      this.cb_marital.MaxDropDownItems = 8;
      this.cb_marital.Name = "cb_marital";
      this.cb_marital.OnFocusOpenListBox = true;
      this.cb_marital.SelectedIndex = -1;
      this.cb_marital.SelectedItem = null;
      this.cb_marital.SelectedValue = null;
      this.cb_marital.SelectionLength = 0;
      this.cb_marital.SelectionStart = 0;
      this.cb_marital.Size = new System.Drawing.Size(258, 40);
      this.cb_marital.Sorted = false;
      this.cb_marital.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.BASIC;
      this.cb_marital.TabIndex = 4;
      this.cb_marital.TabStop = false;
      this.cb_marital.Tag = "HolderMaritalStatus";
      this.cb_marital.ValueMember = "";
      // 
      // lbl_marital_status
      // 
      this.lbl_marital_status.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
      this.lbl_marital_status.Location = new System.Drawing.Point(3, 224);
      this.lbl_marital_status.Name = "lbl_marital_status";
      this.lbl_marital_status.Size = new System.Drawing.Size(144, 46);
      this.lbl_marital_status.TabIndex = 1001;
      this.lbl_marital_status.Tag = "lHolderMaritalStatus";
      this.lbl_marital_status.Text = "xMarital Status";
      this.lbl_marital_status.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // cb_occupation
      // 
      this.cb_occupation.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_occupation.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.cb_occupation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.cb_occupation.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.cb_occupation.CornerRadius = 5;
      this.cb_occupation.DataSource = null;
      this.cb_occupation.DisplayMember = "";
      this.cb_occupation.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
      this.cb_occupation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
      this.cb_occupation.DropDownWidth = 258;
      this.cb_occupation.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.cb_occupation.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_occupation.FormattingEnabled = true;
      this.cb_occupation.FormatValidator = null;
      this.cb_occupation.IsDroppedDown = false;
      this.cb_occupation.ItemHeight = 40;
      this.cb_occupation.Location = new System.Drawing.Point(153, 181);
      this.cb_occupation.MaxDropDownItems = 8;
      this.cb_occupation.Name = "cb_occupation";
      this.cb_occupation.OnFocusOpenListBox = true;
      this.cb_occupation.SelectedIndex = -1;
      this.cb_occupation.SelectedItem = null;
      this.cb_occupation.SelectedValue = null;
      this.cb_occupation.SelectionLength = 0;
      this.cb_occupation.SelectionStart = 0;
      this.cb_occupation.Size = new System.Drawing.Size(258, 40);
      this.cb_occupation.Sorted = false;
      this.cb_occupation.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.BASIC;
      this.cb_occupation.TabIndex = 3;
      this.cb_occupation.TabStop = false;
      this.cb_occupation.Tag = "HolderOccupation";
      this.cb_occupation.ValueMember = "";
      // 
      // lbl_occupation
      // 
      this.lbl_occupation.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_occupation.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
      this.lbl_occupation.Location = new System.Drawing.Point(3, 178);
      this.lbl_occupation.Name = "lbl_occupation";
      this.lbl_occupation.Size = new System.Drawing.Size(144, 46);
      this.lbl_occupation.TabIndex = 1001;
      this.lbl_occupation.Tag = "lHolderOccupation";
      this.lbl_occupation.Text = "xOccupation";
      this.lbl_occupation.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // cb_birth_country
      // 
      this.cb_birth_country.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_birth_country.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.cb_birth_country.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.cb_birth_country.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.cb_birth_country.CornerRadius = 5;
      this.cb_birth_country.DataSource = null;
      this.cb_birth_country.DisplayMember = "";
      this.cb_birth_country.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
      this.cb_birth_country.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
      this.cb_birth_country.DropDownWidth = 258;
      this.cb_birth_country.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.cb_birth_country.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_birth_country.FormattingEnabled = true;
      this.cb_birth_country.FormatValidator = null;
      this.cb_birth_country.IsDroppedDown = false;
      this.cb_birth_country.ItemHeight = 40;
      this.cb_birth_country.Location = new System.Drawing.Point(153, 135);
      this.cb_birth_country.MaxDropDownItems = 8;
      this.cb_birth_country.Name = "cb_birth_country";
      this.cb_birth_country.OnFocusOpenListBox = true;
      this.cb_birth_country.SelectedIndex = -1;
      this.cb_birth_country.SelectedItem = null;
      this.cb_birth_country.SelectedValue = null;
      this.cb_birth_country.SelectionLength = 0;
      this.cb_birth_country.SelectionStart = 0;
      this.cb_birth_country.Size = new System.Drawing.Size(258, 40);
      this.cb_birth_country.Sorted = false;
      this.cb_birth_country.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.BASIC;
      this.cb_birth_country.TabIndex = 2;
      this.cb_birth_country.TabStop = false;
      this.cb_birth_country.Tag = "HolderBirthCountry";
      this.cb_birth_country.ValueMember = "";
      // 
      // lbl_birth_country
      // 
      this.lbl_birth_country.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_birth_country.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
      this.lbl_birth_country.Location = new System.Drawing.Point(3, 132);
      this.lbl_birth_country.Name = "lbl_birth_country";
      this.lbl_birth_country.Size = new System.Drawing.Size(144, 46);
      this.lbl_birth_country.TabIndex = 1001;
      this.lbl_birth_country.Tag = "lHolderBirthCountry";
      this.lbl_birth_country.Text = "xBirth Country";
      this.lbl_birth_country.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // cb_nationality
      // 
      this.cb_nationality.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_nationality.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.cb_nationality.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.cb_nationality.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.cb_nationality.CornerRadius = 5;
      this.cb_nationality.DataSource = null;
      this.cb_nationality.DisplayMember = "";
      this.cb_nationality.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
      this.cb_nationality.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
      this.cb_nationality.DropDownWidth = 258;
      this.cb_nationality.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.cb_nationality.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_nationality.FormattingEnabled = true;
      this.cb_nationality.FormatValidator = null;
      this.cb_nationality.IsDroppedDown = false;
      this.cb_nationality.ItemHeight = 40;
      this.cb_nationality.Location = new System.Drawing.Point(153, 89);
      this.cb_nationality.MaxDropDownItems = 8;
      this.cb_nationality.Name = "cb_nationality";
      this.cb_nationality.OnFocusOpenListBox = true;
      this.cb_nationality.SelectedIndex = -1;
      this.cb_nationality.SelectedItem = null;
      this.cb_nationality.SelectedValue = null;
      this.cb_nationality.SelectionLength = 0;
      this.cb_nationality.SelectionStart = 0;
      this.cb_nationality.Size = new System.Drawing.Size(258, 40);
      this.cb_nationality.Sorted = false;
      this.cb_nationality.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.BASIC;
      this.cb_nationality.TabIndex = 1;
      this.cb_nationality.TabStop = false;
      this.cb_nationality.Tag = "HolderNationality";
      this.cb_nationality.ValueMember = "";
      // 
      // lbl_nationality
      // 
      this.lbl_nationality.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_nationality.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
      this.lbl_nationality.Location = new System.Drawing.Point(3, 86);
      this.lbl_nationality.Name = "lbl_nationality";
      this.lbl_nationality.Size = new System.Drawing.Size(144, 46);
      this.lbl_nationality.TabIndex = 1001;
      this.lbl_nationality.Tag = "lHolderNationality";
      this.lbl_nationality.Text = "xNationality";
      this.lbl_nationality.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // cb_gender
      // 
      this.cb_gender.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_gender.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.cb_gender.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.cb_gender.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.cb_gender.CornerRadius = 5;
      this.cb_gender.DataSource = null;
      this.cb_gender.DisplayMember = "";
      this.cb_gender.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
      this.cb_gender.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cb_gender.DropDownWidth = 258;
      this.cb_gender.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.cb_gender.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_gender.FormattingEnabled = true;
      this.cb_gender.FormatValidator = null;
      this.cb_gender.IsDroppedDown = false;
      this.cb_gender.ItemHeight = 40;
      this.cb_gender.Location = new System.Drawing.Point(153, 43);
      this.cb_gender.MaxDropDownItems = 8;
      this.cb_gender.Name = "cb_gender";
      this.cb_gender.OnFocusOpenListBox = true;
      this.cb_gender.SelectedIndex = -1;
      this.cb_gender.SelectedItem = null;
      this.cb_gender.SelectedValue = null;
      this.cb_gender.SelectionLength = 0;
      this.cb_gender.SelectionStart = 0;
      this.cb_gender.Size = new System.Drawing.Size(258, 40);
      this.cb_gender.Sorted = false;
      this.cb_gender.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.BASIC;
      this.cb_gender.TabIndex = 0;
      this.cb_gender.TabStop = false;
      this.cb_gender.Tag = "HolderGender";
      this.cb_gender.ValueMember = "";
      // 
      // lbl_gender
      // 
      this.lbl_gender.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_gender.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
      this.lbl_gender.Location = new System.Drawing.Point(3, 40);
      this.lbl_gender.Name = "lbl_gender";
      this.lbl_gender.Size = new System.Drawing.Size(144, 46);
      this.lbl_gender.TabIndex = 1001;
      this.lbl_gender.Tag = "lHolderGender";
      this.lbl_gender.Text = "xGender";
      this.lbl_gender.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_full_name
      // 
      this.lbl_full_name.Cursor = System.Windows.Forms.Cursors.Default;
      this.lbl_full_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_full_name.ForeColor = System.Drawing.SystemColors.HotTrack;
      this.lbl_full_name.Location = new System.Drawing.Point(10, 95);
      this.lbl_full_name.Name = "lbl_full_name";
      this.lbl_full_name.Size = new System.Drawing.Size(870, 20);
      this.lbl_full_name.TabIndex = 1001;
      this.lbl_full_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.lbl_full_name.UseMnemonic = false;
      // 
      // contact
      // 
      this.contact.Controls.Add(this.tlp_contact);
      this.contact.Location = new System.Drawing.Point(4, 45);
      this.contact.Name = "contact";
      this.contact.Size = new System.Drawing.Size(901, 453);
      this.contact.TabIndex = 2;
      this.contact.Text = "xContacto";
      this.contact.UseVisualStyleBackColor = true;
      // 
      // tlp_contact
      // 
      this.tlp_contact.AutoSize = true;
      this.tlp_contact.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
      this.tlp_contact.ColumnCount = 2;
      this.tlp_contact.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
      this.tlp_contact.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.tlp_contact.Controls.Add(this.lbl_phone_01, 0, 0);
      this.tlp_contact.Controls.Add(this.txt_phone_01, 1, 0);
      this.tlp_contact.Controls.Add(this.lbl_phone_02, 0, 1);
      this.tlp_contact.Controls.Add(this.txt_phone_02, 1, 1);
      this.tlp_contact.Controls.Add(this.txt_email_01, 1, 2);
      this.tlp_contact.Controls.Add(this.lbl_email_01, 0, 2);
      this.tlp_contact.Controls.Add(this.lbl_twitter, 0, 6);
      this.tlp_contact.Controls.Add(this.txt_email_02, 1, 5);
      this.tlp_contact.Controls.Add(this.lbl_email_02, 0, 5);
      this.tlp_contact.Controls.Add(this.txt_twitter, 1, 6);
      this.tlp_contact.Location = new System.Drawing.Point(6, 30);
      this.tlp_contact.Name = "tlp_contact";
      this.tlp_contact.RowCount = 7;
      this.tlp_contact.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_contact.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_contact.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_contact.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_contact.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_contact.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_contact.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_contact.Size = new System.Drawing.Size(874, 300);
      this.tlp_contact.TabIndex = 20;
      // 
      // lbl_phone_01
      // 
      this.lbl_phone_01.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_phone_01.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
      this.lbl_phone_01.Location = new System.Drawing.Point(3, 0);
      this.lbl_phone_01.Name = "lbl_phone_01";
      this.lbl_phone_01.Size = new System.Drawing.Size(144, 60);
      this.lbl_phone_01.TabIndex = 15;
      this.lbl_phone_01.Tag = "lHolderPhone1";
      this.lbl_phone_01.Text = "xTel.Movil";
      this.lbl_phone_01.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // txt_phone_01
      // 
      this.txt_phone_01.AllowSpace = true;
      this.txt_phone_01.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.txt_phone_01.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_phone_01.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_phone_01.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_phone_01.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_phone_01.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_phone_01.CornerRadius = 5;
      this.txt_phone_01.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_phone_01.Location = new System.Drawing.Point(153, 10);
      this.txt_phone_01.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
      this.txt_phone_01.MaxLength = 20;
      this.txt_phone_01.Multiline = false;
      this.txt_phone_01.Name = "txt_phone_01";
      this.txt_phone_01.PasswordChar = '\0';
      this.txt_phone_01.ReadOnly = false;
      this.txt_phone_01.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_phone_01.SelectedText = "";
      this.txt_phone_01.SelectionLength = 0;
      this.txt_phone_01.SelectionStart = 0;
      this.txt_phone_01.Size = new System.Drawing.Size(718, 40);
      this.txt_phone_01.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_phone_01.TabIndex = 0;
      this.txt_phone_01.TabStop = false;
      this.txt_phone_01.Tag = "HolderPhone1";
      this.txt_phone_01.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_phone_01.UseSystemPasswordChar = false;
      this.txt_phone_01.WaterMark = null;
      this.txt_phone_01.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // lbl_phone_02
      // 
      this.lbl_phone_02.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_phone_02.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
      this.lbl_phone_02.Location = new System.Drawing.Point(3, 60);
      this.lbl_phone_02.Name = "lbl_phone_02";
      this.lbl_phone_02.Size = new System.Drawing.Size(144, 60);
      this.lbl_phone_02.TabIndex = 14;
      this.lbl_phone_02.Tag = "lHolderPhone2";
      this.lbl_phone_02.Text = "xTel.Fijo";
      this.lbl_phone_02.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // txt_phone_02
      // 
      this.txt_phone_02.AllowSpace = true;
      this.txt_phone_02.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.txt_phone_02.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_phone_02.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_phone_02.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_phone_02.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_phone_02.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_phone_02.CornerRadius = 5;
      this.txt_phone_02.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_phone_02.Location = new System.Drawing.Point(153, 70);
      this.txt_phone_02.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
      this.txt_phone_02.MaxLength = 20;
      this.txt_phone_02.Multiline = false;
      this.txt_phone_02.Name = "txt_phone_02";
      this.txt_phone_02.PasswordChar = '\0';
      this.txt_phone_02.ReadOnly = false;
      this.txt_phone_02.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_phone_02.SelectedText = "";
      this.txt_phone_02.SelectionLength = 0;
      this.txt_phone_02.SelectionStart = 0;
      this.txt_phone_02.Size = new System.Drawing.Size(718, 40);
      this.txt_phone_02.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_phone_02.TabIndex = 1;
      this.txt_phone_02.TabStop = false;
      this.txt_phone_02.Tag = "HolderPhone2";
      this.txt_phone_02.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_phone_02.UseSystemPasswordChar = false;
      this.txt_phone_02.WaterMark = null;
      this.txt_phone_02.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // txt_email_01
      // 
      this.txt_email_01.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.txt_email_01.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_email_01.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_email_01.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_email_01.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_email_01.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_email_01.CornerRadius = 5;
      this.txt_email_01.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_email_01.Location = new System.Drawing.Point(153, 130);
      this.txt_email_01.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
      this.txt_email_01.MaxLength = 50;
      this.txt_email_01.Multiline = false;
      this.txt_email_01.Name = "txt_email_01";
      this.txt_email_01.PasswordChar = '\0';
      this.txt_email_01.ReadOnly = false;
      this.txt_email_01.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_email_01.SelectedText = "";
      this.txt_email_01.SelectionLength = 0;
      this.txt_email_01.SelectionStart = 0;
      this.txt_email_01.Size = new System.Drawing.Size(718, 40);
      this.txt_email_01.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_email_01.TabIndex = 2;
      this.txt_email_01.TabStop = false;
      this.txt_email_01.Tag = "HolderEmail1";
      this.txt_email_01.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_email_01.UseSystemPasswordChar = false;
      this.txt_email_01.WaterMark = null;
      this.txt_email_01.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // lbl_email_01
      // 
      this.lbl_email_01.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_email_01.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
      this.lbl_email_01.Location = new System.Drawing.Point(3, 120);
      this.lbl_email_01.Name = "lbl_email_01";
      this.lbl_email_01.Size = new System.Drawing.Size(144, 60);
      this.lbl_email_01.TabIndex = 17;
      this.lbl_email_01.Tag = "lHolderEmail1";
      this.lbl_email_01.Text = "xEmail1";
      this.lbl_email_01.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_twitter
      // 
      this.lbl_twitter.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_twitter.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
      this.lbl_twitter.Location = new System.Drawing.Point(3, 240);
      this.lbl_twitter.Name = "lbl_twitter";
      this.lbl_twitter.Size = new System.Drawing.Size(144, 60);
      this.lbl_twitter.TabIndex = 22;
      this.lbl_twitter.Tag = "lHolderTwitterAccount";
      this.lbl_twitter.Text = "xTwitter";
      this.lbl_twitter.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // txt_email_02
      // 
      this.txt_email_02.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.txt_email_02.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_email_02.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_email_02.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_email_02.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_email_02.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_email_02.CornerRadius = 5;
      this.txt_email_02.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_email_02.Location = new System.Drawing.Point(153, 190);
      this.txt_email_02.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
      this.txt_email_02.MaxLength = 50;
      this.txt_email_02.Multiline = false;
      this.txt_email_02.Name = "txt_email_02";
      this.txt_email_02.PasswordChar = '\0';
      this.txt_email_02.ReadOnly = false;
      this.txt_email_02.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_email_02.SelectedText = "";
      this.txt_email_02.SelectionLength = 0;
      this.txt_email_02.SelectionStart = 0;
      this.txt_email_02.Size = new System.Drawing.Size(718, 40);
      this.txt_email_02.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_email_02.TabIndex = 3;
      this.txt_email_02.TabStop = false;
      this.txt_email_02.Tag = "HolderEmail2";
      this.txt_email_02.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_email_02.UseSystemPasswordChar = false;
      this.txt_email_02.WaterMark = null;
      this.txt_email_02.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // lbl_email_02
      // 
      this.lbl_email_02.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_email_02.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
      this.lbl_email_02.Location = new System.Drawing.Point(3, 180);
      this.lbl_email_02.Name = "lbl_email_02";
      this.lbl_email_02.Size = new System.Drawing.Size(144, 60);
      this.lbl_email_02.TabIndex = 23;
      this.lbl_email_02.Tag = "lHolderEmail2";
      this.lbl_email_02.Text = "xEmail2";
      this.lbl_email_02.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // txt_twitter
      // 
      this.txt_twitter.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.txt_twitter.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_twitter.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_twitter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_twitter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_twitter.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_twitter.CornerRadius = 5;
      this.txt_twitter.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_twitter.Location = new System.Drawing.Point(153, 250);
      this.txt_twitter.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
      this.txt_twitter.MaxLength = 50;
      this.txt_twitter.Multiline = false;
      this.txt_twitter.Name = "txt_twitter";
      this.txt_twitter.PasswordChar = '\0';
      this.txt_twitter.ReadOnly = false;
      this.txt_twitter.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_twitter.SelectedText = "";
      this.txt_twitter.SelectionLength = 0;
      this.txt_twitter.SelectionStart = 0;
      this.txt_twitter.Size = new System.Drawing.Size(718, 40);
      this.txt_twitter.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_twitter.TabIndex = 4;
      this.txt_twitter.TabStop = false;
      this.txt_twitter.Tag = "HolderTwitterAccount";
      this.txt_twitter.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_twitter.UseSystemPasswordChar = false;
      this.txt_twitter.WaterMark = null;
      this.txt_twitter.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // address
      // 
      this.address.Controls.Add(this.HolderAddress);
      this.address.Location = new System.Drawing.Point(4, 45);
      this.address.Name = "address";
      this.address.Padding = new System.Windows.Forms.Padding(3);
      this.address.Size = new System.Drawing.Size(901, 453);
      this.address.TabIndex = 1;
      this.address.Text = "xDirección";
      this.address.UseVisualStyleBackColor = true;
      // 
      // HolderAddress
      // 
      this.HolderAddress.AddressLine1 = "";
      this.HolderAddress.AddressLine2 = "";
      this.HolderAddress.AddressLine3 = "";
      this.HolderAddress.Automatic = true;
      this.HolderAddress.Country = null;
      this.HolderAddress.HouseNumber = "";
      this.HolderAddress.Location = new System.Drawing.Point(0, 0);
      this.HolderAddress.Name = "HolderAddress";
      this.HolderAddress.PostCode = "";
      this.HolderAddress.Size = new System.Drawing.Size(870, 408);
      this.HolderAddress.State = null;
      this.HolderAddress.TabIndex = 40;
      this.HolderAddress.Tag = "Holder";
      this.HolderAddress.Town = "";
      // 
      // beneficiary
      // 
      this.beneficiary.Controls.Add(this.tlp_ben_principal);
      this.beneficiary.Controls.Add(this.tlp_ben_principal1);
      this.beneficiary.Controls.Add(this.tlp_ben_name);
      this.beneficiary.Controls.Add(this.chk_holder_has_beneficiary);
      this.beneficiary.Controls.Add(this.lbl_beneficiary_full_name);
      this.beneficiary.Location = new System.Drawing.Point(4, 45);
      this.beneficiary.Name = "beneficiary";
      this.beneficiary.Padding = new System.Windows.Forms.Padding(3);
      this.beneficiary.Size = new System.Drawing.Size(901, 453);
      this.beneficiary.TabIndex = 8;
      this.beneficiary.Text = "xBeneficiary";
      this.beneficiary.UseVisualStyleBackColor = true;
      // 
      // tlp_ben_principal
      // 
      this.tlp_ben_principal.ColumnCount = 2;
      this.tlp_ben_principal.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
      this.tlp_ben_principal.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 350F));
      this.tlp_ben_principal.Controls.Add(this.uc_dt_ben_birth, 1, 2);
      this.tlp_ben_principal.Controls.Add(this.lbl_beneficiary_RFC, 0, 0);
      this.tlp_ben_principal.Controls.Add(this.txt_beneficiary_RFC, 1, 0);
      this.tlp_ben_principal.Controls.Add(this.lbl_beneficiary_CURP, 0, 1);
      this.tlp_ben_principal.Controls.Add(this.txt_beneficiary_CURP, 1, 1);
      this.tlp_ben_principal.Controls.Add(this.lbl_beneficiary_birth_date, 0, 2);
      this.tlp_ben_principal.Location = new System.Drawing.Point(9, 115);
      this.tlp_ben_principal.Name = "tlp_ben_principal";
      this.tlp_ben_principal.RowCount = 4;
      this.tlp_ben_principal.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_ben_principal.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_ben_principal.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_ben_principal.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_ben_principal.Size = new System.Drawing.Size(470, 168);
      this.tlp_ben_principal.TabIndex = 1;
      // 
      // uc_dt_ben_birth
      // 
      this.uc_dt_ben_birth.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
      this.uc_dt_ben_birth.DateText = "";
      this.uc_dt_ben_birth.DateTextFont = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.uc_dt_ben_birth.DateTextWidth = 0;
      this.uc_dt_ben_birth.DateValue = new System.DateTime(((long)(0)));
      this.uc_dt_ben_birth.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.uc_dt_ben_birth.FormatFont = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.uc_dt_ben_birth.FormatWidth = 183;
      this.uc_dt_ben_birth.Invalid = false;
      this.uc_dt_ben_birth.Location = new System.Drawing.Point(153, 95);
      this.uc_dt_ben_birth.Name = "uc_dt_ben_birth";
      this.uc_dt_ben_birth.Size = new System.Drawing.Size(324, 42);
      this.uc_dt_ben_birth.TabIndex = 2;
      this.uc_dt_ben_birth.Tag = "BeneficiaryBirthDate";
      this.uc_dt_ben_birth.ValuesFont = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      // 
      // lbl_beneficiary_RFC
      // 
      this.lbl_beneficiary_RFC.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_beneficiary_RFC.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
      this.lbl_beneficiary_RFC.Location = new System.Drawing.Point(3, 0);
      this.lbl_beneficiary_RFC.Name = "lbl_beneficiary_RFC";
      this.lbl_beneficiary_RFC.Size = new System.Drawing.Size(144, 46);
      this.lbl_beneficiary_RFC.TabIndex = 1001;
      this.lbl_beneficiary_RFC.Tag = "lBeneficiaryDocument";
      this.lbl_beneficiary_RFC.Text = "xRFC";
      this.lbl_beneficiary_RFC.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // txt_beneficiary_RFC
      // 
      this.txt_beneficiary_RFC.AllowSpace = true;
      this.txt_beneficiary_RFC.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_beneficiary_RFC.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_beneficiary_RFC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_beneficiary_RFC.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_beneficiary_RFC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.txt_beneficiary_RFC.CornerRadius = 5;
      this.txt_beneficiary_RFC.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_beneficiary_RFC.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.ExtendedMode;
      this.txt_beneficiary_RFC.Location = new System.Drawing.Point(153, 3);
      this.txt_beneficiary_RFC.MaxLength = 20;
      this.txt_beneficiary_RFC.Multiline = false;
      this.txt_beneficiary_RFC.Name = "txt_beneficiary_RFC";
      this.txt_beneficiary_RFC.PasswordChar = '\0';
      this.txt_beneficiary_RFC.ReadOnly = false;
      this.txt_beneficiary_RFC.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_beneficiary_RFC.SelectedText = "";
      this.txt_beneficiary_RFC.SelectionLength = 0;
      this.txt_beneficiary_RFC.SelectionStart = 0;
      this.txt_beneficiary_RFC.Size = new System.Drawing.Size(258, 40);
      this.txt_beneficiary_RFC.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_beneficiary_RFC.TabIndex = 0;
      this.txt_beneficiary_RFC.TabStop = false;
      this.txt_beneficiary_RFC.Tag = "BeneficiaryDocument";
      this.txt_beneficiary_RFC.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_beneficiary_RFC.UseSystemPasswordChar = false;
      this.txt_beneficiary_RFC.WaterMark = null;
      this.txt_beneficiary_RFC.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // lbl_beneficiary_CURP
      // 
      this.lbl_beneficiary_CURP.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_beneficiary_CURP.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
      this.lbl_beneficiary_CURP.Location = new System.Drawing.Point(3, 46);
      this.lbl_beneficiary_CURP.Name = "lbl_beneficiary_CURP";
      this.lbl_beneficiary_CURP.Size = new System.Drawing.Size(144, 46);
      this.lbl_beneficiary_CURP.TabIndex = 1001;
      this.lbl_beneficiary_CURP.Tag = "lBeneficiaryCURP";
      this.lbl_beneficiary_CURP.Text = "xCURP";
      this.lbl_beneficiary_CURP.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // txt_beneficiary_CURP
      // 
      this.txt_beneficiary_CURP.AllowSpace = true;
      this.txt_beneficiary_CURP.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_beneficiary_CURP.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_beneficiary_CURP.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_beneficiary_CURP.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_beneficiary_CURP.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.txt_beneficiary_CURP.CornerRadius = 5;
      this.txt_beneficiary_CURP.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_beneficiary_CURP.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.ExtendedMode;
      this.txt_beneficiary_CURP.Location = new System.Drawing.Point(153, 49);
      this.txt_beneficiary_CURP.MaxLength = 20;
      this.txt_beneficiary_CURP.Multiline = false;
      this.txt_beneficiary_CURP.Name = "txt_beneficiary_CURP";
      this.txt_beneficiary_CURP.PasswordChar = '\0';
      this.txt_beneficiary_CURP.ReadOnly = false;
      this.txt_beneficiary_CURP.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_beneficiary_CURP.SelectedText = "";
      this.txt_beneficiary_CURP.SelectionLength = 0;
      this.txt_beneficiary_CURP.SelectionStart = 0;
      this.txt_beneficiary_CURP.Size = new System.Drawing.Size(258, 40);
      this.txt_beneficiary_CURP.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_beneficiary_CURP.TabIndex = 1;
      this.txt_beneficiary_CURP.TabStop = false;
      this.txt_beneficiary_CURP.Tag = "BeneficiaryCURP";
      this.txt_beneficiary_CURP.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_beneficiary_CURP.UseSystemPasswordChar = false;
      this.txt_beneficiary_CURP.WaterMark = null;
      this.txt_beneficiary_CURP.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // lbl_beneficiary_birth_date
      // 
      this.lbl_beneficiary_birth_date.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_beneficiary_birth_date.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
      this.lbl_beneficiary_birth_date.Location = new System.Drawing.Point(3, 92);
      this.lbl_beneficiary_birth_date.Name = "lbl_beneficiary_birth_date";
      this.lbl_beneficiary_birth_date.Size = new System.Drawing.Size(144, 49);
      this.lbl_beneficiary_birth_date.TabIndex = 1001;
      this.lbl_beneficiary_birth_date.Tag = "lBeneficiaryBirthDate";
      this.lbl_beneficiary_birth_date.Text = "xBirthDay";
      this.lbl_beneficiary_birth_date.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // tlp_ben_principal1
      // 
      this.tlp_ben_principal1.ColumnCount = 2;
      this.tlp_ben_principal1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
      this.tlp_ben_principal1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 300F));
      this.tlp_ben_principal1.Controls.Add(this.lbl_beneficiary_gender, 0, 1);
      this.tlp_ben_principal1.Controls.Add(this.cb_beneficiary_gender, 1, 1);
      this.tlp_ben_principal1.Controls.Add(this.lbl_beneficiary_occupation, 0, 2);
      this.tlp_ben_principal1.Controls.Add(this.cb_beneficiary_occupation, 1, 2);
      this.tlp_ben_principal1.Controls.Add(this.btn_ben_scan_docs, 1, 0);
      this.tlp_ben_principal1.Controls.Add(this.label1, 0, 4);
      this.tlp_ben_principal1.Controls.Add(this.label2, 0, 0);
      this.tlp_ben_principal1.Location = new System.Drawing.Point(483, 115);
      this.tlp_ben_principal1.Name = "tlp_ben_principal1";
      this.tlp_ben_principal1.RowCount = 5;
      this.tlp_ben_principal1.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_ben_principal1.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_ben_principal1.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_ben_principal1.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_ben_principal1.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_ben_principal1.Size = new System.Drawing.Size(412, 159);
      this.tlp_ben_principal1.TabIndex = 2;
      // 
      // lbl_beneficiary_gender
      // 
      this.lbl_beneficiary_gender.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_beneficiary_gender.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
      this.lbl_beneficiary_gender.Location = new System.Drawing.Point(3, 46);
      this.lbl_beneficiary_gender.Name = "lbl_beneficiary_gender";
      this.lbl_beneficiary_gender.Size = new System.Drawing.Size(144, 46);
      this.lbl_beneficiary_gender.TabIndex = 1001;
      this.lbl_beneficiary_gender.Tag = "lBeneficiaryGender";
      this.lbl_beneficiary_gender.Text = "xGender";
      this.lbl_beneficiary_gender.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // cb_beneficiary_gender
      // 
      this.cb_beneficiary_gender.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_beneficiary_gender.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.cb_beneficiary_gender.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.cb_beneficiary_gender.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.cb_beneficiary_gender.CornerRadius = 5;
      this.cb_beneficiary_gender.DataSource = null;
      this.cb_beneficiary_gender.DisplayMember = "";
      this.cb_beneficiary_gender.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
      this.cb_beneficiary_gender.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cb_beneficiary_gender.DropDownWidth = 258;
      this.cb_beneficiary_gender.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.cb_beneficiary_gender.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_beneficiary_gender.FormattingEnabled = true;
      this.cb_beneficiary_gender.FormatValidator = null;
      this.cb_beneficiary_gender.IsDroppedDown = false;
      this.cb_beneficiary_gender.ItemHeight = 40;
      this.cb_beneficiary_gender.Location = new System.Drawing.Point(153, 49);
      this.cb_beneficiary_gender.MaxDropDownItems = 8;
      this.cb_beneficiary_gender.Name = "cb_beneficiary_gender";
      this.cb_beneficiary_gender.OnFocusOpenListBox = true;
      this.cb_beneficiary_gender.SelectedIndex = -1;
      this.cb_beneficiary_gender.SelectedItem = null;
      this.cb_beneficiary_gender.SelectedValue = null;
      this.cb_beneficiary_gender.SelectionLength = 0;
      this.cb_beneficiary_gender.SelectionStart = 0;
      this.cb_beneficiary_gender.Size = new System.Drawing.Size(258, 40);
      this.cb_beneficiary_gender.Sorted = false;
      this.cb_beneficiary_gender.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.BASIC;
      this.cb_beneficiary_gender.TabIndex = 0;
      this.cb_beneficiary_gender.TabStop = false;
      this.cb_beneficiary_gender.Tag = "BeneficiaryGender";
      this.cb_beneficiary_gender.ValueMember = "";
      // 
      // lbl_beneficiary_occupation
      // 
      this.lbl_beneficiary_occupation.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_beneficiary_occupation.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
      this.lbl_beneficiary_occupation.Location = new System.Drawing.Point(3, 92);
      this.lbl_beneficiary_occupation.Name = "lbl_beneficiary_occupation";
      this.lbl_beneficiary_occupation.Size = new System.Drawing.Size(144, 46);
      this.lbl_beneficiary_occupation.TabIndex = 1001;
      this.lbl_beneficiary_occupation.Tag = "lBeneficiaryOccupation";
      this.lbl_beneficiary_occupation.Text = "xOccupation";
      this.lbl_beneficiary_occupation.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // cb_beneficiary_occupation
      // 
      this.cb_beneficiary_occupation.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_beneficiary_occupation.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.cb_beneficiary_occupation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.cb_beneficiary_occupation.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.cb_beneficiary_occupation.CornerRadius = 5;
      this.cb_beneficiary_occupation.DataSource = null;
      this.cb_beneficiary_occupation.DisplayMember = "";
      this.cb_beneficiary_occupation.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
      this.cb_beneficiary_occupation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
      this.cb_beneficiary_occupation.DropDownWidth = 258;
      this.cb_beneficiary_occupation.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.cb_beneficiary_occupation.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_beneficiary_occupation.FormattingEnabled = true;
      this.cb_beneficiary_occupation.FormatValidator = null;
      this.cb_beneficiary_occupation.IsDroppedDown = false;
      this.cb_beneficiary_occupation.ItemHeight = 40;
      this.cb_beneficiary_occupation.Location = new System.Drawing.Point(153, 95);
      this.cb_beneficiary_occupation.MaxDropDownItems = 8;
      this.cb_beneficiary_occupation.Name = "cb_beneficiary_occupation";
      this.cb_beneficiary_occupation.OnFocusOpenListBox = true;
      this.cb_beneficiary_occupation.SelectedIndex = -1;
      this.cb_beneficiary_occupation.SelectedItem = null;
      this.cb_beneficiary_occupation.SelectedValue = null;
      this.cb_beneficiary_occupation.SelectionLength = 0;
      this.cb_beneficiary_occupation.SelectionStart = 0;
      this.cb_beneficiary_occupation.Size = new System.Drawing.Size(258, 40);
      this.cb_beneficiary_occupation.Sorted = false;
      this.cb_beneficiary_occupation.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.BASIC;
      this.cb_beneficiary_occupation.TabIndex = 1;
      this.cb_beneficiary_occupation.TabStop = false;
      this.cb_beneficiary_occupation.Tag = "BeneficiaryOccupation";
      this.cb_beneficiary_occupation.ValueMember = "";
      // 
      // btn_ben_scan_docs
      // 
      this.btn_ben_scan_docs.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(47)))), ((int)(((byte)(50)))));
      this.btn_ben_scan_docs.FlatAppearance.BorderSize = 0;
      this.btn_ben_scan_docs.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_ben_scan_docs.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_ben_scan_docs.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_ben_scan_docs.Image = null;
      this.btn_ben_scan_docs.IsSelected = false;
      this.btn_ben_scan_docs.Location = new System.Drawing.Point(153, 3);
      this.btn_ben_scan_docs.Name = "btn_ben_scan_docs";
      this.btn_ben_scan_docs.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_ben_scan_docs.Size = new System.Drawing.Size(257, 34);
      this.btn_ben_scan_docs.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.TERTIARY;
      this.btn_ben_scan_docs.TabIndex = 0;
      this.btn_ben_scan_docs.TabStop = false;
      this.btn_ben_scan_docs.Tag = "BeneficiaryDocScan";
      this.btn_ben_scan_docs.Text = "XDOCUMENT DIGITIZING";
      this.btn_ben_scan_docs.UseVisualStyleBackColor = false;
      this.btn_ben_scan_docs.Click += new System.EventHandler(this.btn_ben_scan_docs_Click);
      // 
      // label1
      // 
      this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
      this.label1.Location = new System.Drawing.Point(3, 138);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(144, 46);
      this.label1.TabIndex = 1002;
      this.label1.Tag = "lHolderDocScan";
      this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // label2
      // 
      this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
      this.label2.Location = new System.Drawing.Point(3, 0);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(144, 46);
      this.label2.TabIndex = 1003;
      this.label2.Tag = "lBeneficiaryDocScan";
      this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // tlp_ben_name
      // 
      this.tlp_ben_name.ColumnCount = 3;
      this.tlp_ben_name.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
      this.tlp_ben_name.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
      this.tlp_ben_name.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
      this.tlp_ben_name.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
      this.tlp_ben_name.Controls.Add(this.lbl_beneficiary_name, 0, 0);
      this.tlp_ben_name.Controls.Add(this.txt_beneficiary_name, 0, 1);
      this.tlp_ben_name.Controls.Add(this.lbl_beneficiary_last_name1, 1, 0);
      this.tlp_ben_name.Controls.Add(this.txt_beneficiary_last_name1, 1, 1);
      this.tlp_ben_name.Controls.Add(this.lbl_beneficiary_last_name2, 2, 0);
      this.tlp_ben_name.Controls.Add(this.txt_beneficiary_last_name2, 2, 1);
      this.tlp_ben_name.Location = new System.Drawing.Point(6, 21);
      this.tlp_ben_name.Name = "tlp_ben_name";
      this.tlp_ben_name.RowCount = 2;
      this.tlp_ben_name.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_ben_name.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_ben_name.Size = new System.Drawing.Size(887, 65);
      this.tlp_ben_name.TabIndex = 0;
      // 
      // lbl_beneficiary_name
      // 
      this.lbl_beneficiary_name.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_beneficiary_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
      this.lbl_beneficiary_name.Location = new System.Drawing.Point(3, 0);
      this.lbl_beneficiary_name.Name = "lbl_beneficiary_name";
      this.lbl_beneficiary_name.Size = new System.Drawing.Size(289, 19);
      this.lbl_beneficiary_name.TabIndex = 1001;
      this.lbl_beneficiary_name.Tag = "lBeneficiaryName3";
      this.lbl_beneficiary_name.Text = "xBeneficiaryName3";
      this.lbl_beneficiary_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // txt_beneficiary_name
      // 
      this.txt_beneficiary_name.AllowSpace = true;
      this.txt_beneficiary_name.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
      this.txt_beneficiary_name.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_beneficiary_name.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_beneficiary_name.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_beneficiary_name.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_beneficiary_name.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.txt_beneficiary_name.CornerRadius = 5;
      this.txt_beneficiary_name.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_beneficiary_name.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.BaseMode;
      this.txt_beneficiary_name.Location = new System.Drawing.Point(3, 22);
      this.txt_beneficiary_name.MaxLength = 50;
      this.txt_beneficiary_name.Multiline = false;
      this.txt_beneficiary_name.Name = "txt_beneficiary_name";
      this.txt_beneficiary_name.PasswordChar = '\0';
      this.txt_beneficiary_name.ReadOnly = false;
      this.txt_beneficiary_name.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_beneficiary_name.SelectedText = "";
      this.txt_beneficiary_name.SelectionLength = 0;
      this.txt_beneficiary_name.SelectionStart = 0;
      this.txt_beneficiary_name.Size = new System.Drawing.Size(289, 40);
      this.txt_beneficiary_name.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_beneficiary_name.TabIndex = 0;
      this.txt_beneficiary_name.TabStop = false;
      this.txt_beneficiary_name.Tag = "BeneficiaryName3";
      this.txt_beneficiary_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_beneficiary_name.UseSystemPasswordChar = false;
      this.txt_beneficiary_name.WaterMark = null;
      this.txt_beneficiary_name.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      this.txt_beneficiary_name.TextChanged += new System.EventHandler(this.txt_beneficiary_name_or_last_names_TextChanged);
      // 
      // lbl_beneficiary_last_name1
      // 
      this.lbl_beneficiary_last_name1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_beneficiary_last_name1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
      this.lbl_beneficiary_last_name1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.lbl_beneficiary_last_name1.Location = new System.Drawing.Point(298, 0);
      this.lbl_beneficiary_last_name1.Name = "lbl_beneficiary_last_name1";
      this.lbl_beneficiary_last_name1.Size = new System.Drawing.Size(289, 19);
      this.lbl_beneficiary_last_name1.TabIndex = 1001;
      this.lbl_beneficiary_last_name1.Tag = "lBeneficiaryName1";
      this.lbl_beneficiary_last_name1.Text = "xBeneficiaryName1";
      this.lbl_beneficiary_last_name1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // txt_beneficiary_last_name1
      // 
      this.txt_beneficiary_last_name1.AllowSpace = true;
      this.txt_beneficiary_last_name1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
      this.txt_beneficiary_last_name1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_beneficiary_last_name1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_beneficiary_last_name1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_beneficiary_last_name1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_beneficiary_last_name1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.txt_beneficiary_last_name1.CornerRadius = 5;
      this.txt_beneficiary_last_name1.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_beneficiary_last_name1.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.BaseMode;
      this.txt_beneficiary_last_name1.Location = new System.Drawing.Point(298, 22);
      this.txt_beneficiary_last_name1.MaxLength = 50;
      this.txt_beneficiary_last_name1.Multiline = false;
      this.txt_beneficiary_last_name1.Name = "txt_beneficiary_last_name1";
      this.txt_beneficiary_last_name1.PasswordChar = '\0';
      this.txt_beneficiary_last_name1.ReadOnly = false;
      this.txt_beneficiary_last_name1.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_beneficiary_last_name1.SelectedText = "";
      this.txt_beneficiary_last_name1.SelectionLength = 0;
      this.txt_beneficiary_last_name1.SelectionStart = 0;
      this.txt_beneficiary_last_name1.Size = new System.Drawing.Size(289, 40);
      this.txt_beneficiary_last_name1.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_beneficiary_last_name1.TabIndex = 1;
      this.txt_beneficiary_last_name1.TabStop = false;
      this.txt_beneficiary_last_name1.Tag = "BeneficiaryName1";
      this.txt_beneficiary_last_name1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_beneficiary_last_name1.UseSystemPasswordChar = false;
      this.txt_beneficiary_last_name1.WaterMark = null;
      this.txt_beneficiary_last_name1.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      this.txt_beneficiary_last_name1.TextChanged += new System.EventHandler(this.txt_beneficiary_name_or_last_names_TextChanged);
      // 
      // lbl_beneficiary_last_name2
      // 
      this.lbl_beneficiary_last_name2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_beneficiary_last_name2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
      this.lbl_beneficiary_last_name2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.lbl_beneficiary_last_name2.Location = new System.Drawing.Point(593, 0);
      this.lbl_beneficiary_last_name2.Name = "lbl_beneficiary_last_name2";
      this.lbl_beneficiary_last_name2.Size = new System.Drawing.Size(291, 19);
      this.lbl_beneficiary_last_name2.TabIndex = 1001;
      this.lbl_beneficiary_last_name2.Tag = "lBeneficiaryName2";
      this.lbl_beneficiary_last_name2.Text = "xBeneficiaryName2";
      this.lbl_beneficiary_last_name2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // txt_beneficiary_last_name2
      // 
      this.txt_beneficiary_last_name2.AllowSpace = true;
      this.txt_beneficiary_last_name2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
      this.txt_beneficiary_last_name2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_beneficiary_last_name2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_beneficiary_last_name2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_beneficiary_last_name2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_beneficiary_last_name2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.txt_beneficiary_last_name2.CornerRadius = 5;
      this.txt_beneficiary_last_name2.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_beneficiary_last_name2.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.BaseMode;
      this.txt_beneficiary_last_name2.Location = new System.Drawing.Point(593, 22);
      this.txt_beneficiary_last_name2.MaxLength = 50;
      this.txt_beneficiary_last_name2.Multiline = false;
      this.txt_beneficiary_last_name2.Name = "txt_beneficiary_last_name2";
      this.txt_beneficiary_last_name2.PasswordChar = '\0';
      this.txt_beneficiary_last_name2.ReadOnly = false;
      this.txt_beneficiary_last_name2.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_beneficiary_last_name2.SelectedText = "";
      this.txt_beneficiary_last_name2.SelectionLength = 0;
      this.txt_beneficiary_last_name2.SelectionStart = 0;
      this.txt_beneficiary_last_name2.Size = new System.Drawing.Size(291, 40);
      this.txt_beneficiary_last_name2.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_beneficiary_last_name2.TabIndex = 2;
      this.txt_beneficiary_last_name2.TabStop = false;
      this.txt_beneficiary_last_name2.Tag = "BeneficiaryName2";
      this.txt_beneficiary_last_name2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_beneficiary_last_name2.UseSystemPasswordChar = false;
      this.txt_beneficiary_last_name2.WaterMark = null;
      this.txt_beneficiary_last_name2.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      this.txt_beneficiary_last_name2.TextChanged += new System.EventHandler(this.txt_beneficiary_name_or_last_names_TextChanged);
      // 
      // chk_holder_has_beneficiary
      // 
      this.chk_holder_has_beneficiary.AutoSize = true;
      this.chk_holder_has_beneficiary.BackColor = System.Drawing.Color.Transparent;
      this.chk_holder_has_beneficiary.Cursor = System.Windows.Forms.Cursors.Default;
      this.chk_holder_has_beneficiary.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.chk_holder_has_beneficiary.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.chk_holder_has_beneficiary.Location = new System.Drawing.Point(9, 361);
      this.chk_holder_has_beneficiary.MinimumSize = new System.Drawing.Size(232, 30);
      this.chk_holder_has_beneficiary.Name = "chk_holder_has_beneficiary";
      this.chk_holder_has_beneficiary.Padding = new System.Windows.Forms.Padding(5);
      this.chk_holder_has_beneficiary.Size = new System.Drawing.Size(232, 33);
      this.chk_holder_has_beneficiary.TabIndex = 39;
      this.chk_holder_has_beneficiary.Text = "xHolder has a beneficiary";
      this.chk_holder_has_beneficiary.UseVisualStyleBackColor = true;
      this.chk_holder_has_beneficiary.CheckedChanged += new System.EventHandler(this.chk_holder_has_beneficiary_CheckedChanged);
      // 
      // lbl_beneficiary_full_name
      // 
      this.lbl_beneficiary_full_name.Cursor = System.Windows.Forms.Cursors.Default;
      this.lbl_beneficiary_full_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_beneficiary_full_name.ForeColor = System.Drawing.SystemColors.HotTrack;
      this.lbl_beneficiary_full_name.Location = new System.Drawing.Point(9, 89);
      this.lbl_beneficiary_full_name.Name = "lbl_beneficiary_full_name";
      this.lbl_beneficiary_full_name.RightToLeft = System.Windows.Forms.RightToLeft.No;
      this.lbl_beneficiary_full_name.Size = new System.Drawing.Size(870, 20);
      this.lbl_beneficiary_full_name.TabIndex = 1001;
      this.lbl_beneficiary_full_name.Text = "xFull Name";
      this.lbl_beneficiary_full_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.lbl_beneficiary_full_name.UseMnemonic = false;
      // 
      // points
      // 
      this.points.Controls.Add(this.lbl_points_to_maintain_prefix);
      this.points.Controls.Add(this.lbl_points_to_maintain);
      this.points.Controls.Add(this.lbl_level_prefix);
      this.points.Controls.Add(this.gb_next_level);
      this.points.Controls.Add(this.lbl_level_entered);
      this.points.Controls.Add(this.lbl_points_sufix);
      this.points.Controls.Add(this.lbl_level_entered_prefix);
      this.points.Controls.Add(this.lbl_points);
      this.points.Controls.Add(this.lbl_level_expiration);
      this.points.Controls.Add(this.lbl_points_prefix);
      this.points.Controls.Add(this.lbl_level_expiration_prefix);
      this.points.Controls.Add(this.lbl_level);
      this.points.Location = new System.Drawing.Point(4, 45);
      this.points.Name = "points";
      this.points.Padding = new System.Windows.Forms.Padding(3);
      this.points.Size = new System.Drawing.Size(901, 453);
      this.points.TabIndex = 11;
      this.points.Text = "xPuntos";
      this.points.UseVisualStyleBackColor = true;
      // 
      // lbl_points_to_maintain_prefix
      // 
      this.lbl_points_to_maintain_prefix.BackColor = System.Drawing.Color.Transparent;
      this.lbl_points_to_maintain_prefix.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_points_to_maintain_prefix.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_points_to_maintain_prefix.Location = new System.Drawing.Point(8, 152);
      this.lbl_points_to_maintain_prefix.Name = "lbl_points_to_maintain_prefix";
      this.lbl_points_to_maintain_prefix.Size = new System.Drawing.Size(273, 38);
      this.lbl_points_to_maintain_prefix.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_points_to_maintain_prefix.TabIndex = 81;
      this.lbl_points_to_maintain_prefix.Text = "xPuntos Mantener";
      this.lbl_points_to_maintain_prefix.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_points_to_maintain
      // 
      this.lbl_points_to_maintain.BackColor = System.Drawing.Color.Transparent;
      this.lbl_points_to_maintain.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_points_to_maintain.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_points_to_maintain.Location = new System.Drawing.Point(309, 152);
      this.lbl_points_to_maintain.Name = "lbl_points_to_maintain";
      this.lbl_points_to_maintain.Size = new System.Drawing.Size(125, 38);
      this.lbl_points_to_maintain.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_points_to_maintain.TabIndex = 80;
      this.lbl_points_to_maintain.Text = "---";
      this.lbl_points_to_maintain.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_level_prefix
      // 
      this.lbl_level_prefix.BackColor = System.Drawing.Color.Transparent;
      this.lbl_level_prefix.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_level_prefix.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_level_prefix.Location = new System.Drawing.Point(8, 38);
      this.lbl_level_prefix.Name = "lbl_level_prefix";
      this.lbl_level_prefix.Size = new System.Drawing.Size(273, 38);
      this.lbl_level_prefix.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_level_prefix.TabIndex = 71;
      this.lbl_level_prefix.Text = "xNivel";
      this.lbl_level_prefix.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // gb_next_level
      // 
      this.gb_next_level.BackColor = System.Drawing.Color.Transparent;
      this.gb_next_level.BorderColor = System.Drawing.Color.Empty;
      this.gb_next_level.Controls.Add(this.lbl_points_last_days);
      this.gb_next_level.Controls.Add(this.lbl_points_discretionaries_prefix);
      this.gb_next_level.Controls.Add(this.lbl_points_discretionaries);
      this.gb_next_level.Controls.Add(this.lbl_points_generated_prefix);
      this.gb_next_level.Controls.Add(this.lbl_points_generated);
      this.gb_next_level.Controls.Add(this.lbl_points_to_next_level);
      this.gb_next_level.Controls.Add(this.lbl_points_to_next_level_prefix);
      this.gb_next_level.Controls.Add(this.lbl_points_level_prefix);
      this.gb_next_level.Controls.Add(this.lbl_points_level);
      this.gb_next_level.Controls.Add(this.lbl_points_req_prefix);
      this.gb_next_level.Controls.Add(this.lbl_points_req);
      this.gb_next_level.CornerRadius = 10;
      this.gb_next_level.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_next_level.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gb_next_level.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_next_level.HeaderHeight = 35;
      this.gb_next_level.HeaderSubText = null;
      this.gb_next_level.HeaderText = null;
      this.gb_next_level.Location = new System.Drawing.Point(508, 20);
      this.gb_next_level.Name = "gb_next_level";
      this.gb_next_level.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_next_level.Size = new System.Drawing.Size(355, 227);
      this.gb_next_level.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.gb_next_level.TabIndex = 79;
      this.gb_next_level.Text = "xSiguiente Nivel";
      // 
      // lbl_points_last_days
      // 
      this.lbl_points_last_days.BackColor = System.Drawing.Color.Transparent;
      this.lbl_points_last_days.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_points_last_days.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_points_last_days.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
      this.lbl_points_last_days.Location = new System.Drawing.Point(101, 204);
      this.lbl_points_last_days.Name = "lbl_points_last_days";
      this.lbl_points_last_days.Size = new System.Drawing.Size(248, 20);
      this.lbl_points_last_days.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_points_last_days.TabIndex = 68;
      this.lbl_points_last_days.Text = "*xPuntos obtenidos en los últimos XX días";
      this.lbl_points_last_days.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_points_discretionaries_prefix
      // 
      this.lbl_points_discretionaries_prefix.BackColor = System.Drawing.Color.Transparent;
      this.lbl_points_discretionaries_prefix.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_points_discretionaries_prefix.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_points_discretionaries_prefix.Location = new System.Drawing.Point(7, 102);
      this.lbl_points_discretionaries_prefix.Name = "lbl_points_discretionaries_prefix";
      this.lbl_points_discretionaries_prefix.Size = new System.Drawing.Size(253, 20);
      this.lbl_points_discretionaries_prefix.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_points_discretionaries_prefix.TabIndex = 66;
      this.lbl_points_discretionaries_prefix.Text = "xPuntos Discrecionales";
      this.lbl_points_discretionaries_prefix.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_points_discretionaries
      // 
      this.lbl_points_discretionaries.BackColor = System.Drawing.Color.Transparent;
      this.lbl_points_discretionaries.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_points_discretionaries.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_points_discretionaries.Location = new System.Drawing.Point(266, 102);
      this.lbl_points_discretionaries.Name = "lbl_points_discretionaries";
      this.lbl_points_discretionaries.Size = new System.Drawing.Size(83, 20);
      this.lbl_points_discretionaries.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_points_discretionaries.TabIndex = 65;
      this.lbl_points_discretionaries.Text = "x10.000";
      this.lbl_points_discretionaries.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_points_generated_prefix
      // 
      this.lbl_points_generated_prefix.BackColor = System.Drawing.Color.Transparent;
      this.lbl_points_generated_prefix.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_points_generated_prefix.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_points_generated_prefix.Location = new System.Drawing.Point(7, 82);
      this.lbl_points_generated_prefix.Name = "lbl_points_generated_prefix";
      this.lbl_points_generated_prefix.Size = new System.Drawing.Size(253, 20);
      this.lbl_points_generated_prefix.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_points_generated_prefix.TabIndex = 64;
      this.lbl_points_generated_prefix.Text = "xPuntos Generados";
      this.lbl_points_generated_prefix.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_points_generated
      // 
      this.lbl_points_generated.BackColor = System.Drawing.Color.Transparent;
      this.lbl_points_generated.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_points_generated.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_points_generated.Location = new System.Drawing.Point(266, 82);
      this.lbl_points_generated.Name = "lbl_points_generated";
      this.lbl_points_generated.Size = new System.Drawing.Size(83, 20);
      this.lbl_points_generated.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_points_generated.TabIndex = 63;
      this.lbl_points_generated.Text = "x10.000";
      this.lbl_points_generated.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_points_to_next_level
      // 
      this.lbl_points_to_next_level.BackColor = System.Drawing.Color.Transparent;
      this.lbl_points_to_next_level.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_points_to_next_level.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_points_to_next_level.Location = new System.Drawing.Point(266, 152);
      this.lbl_points_to_next_level.Name = "lbl_points_to_next_level";
      this.lbl_points_to_next_level.Size = new System.Drawing.Size(84, 38);
      this.lbl_points_to_next_level.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_points_to_next_level.TabIndex = 62;
      this.lbl_points_to_next_level.Text = "---";
      this.lbl_points_to_next_level.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_points_to_next_level_prefix
      // 
      this.lbl_points_to_next_level_prefix.BackColor = System.Drawing.Color.Transparent;
      this.lbl_points_to_next_level_prefix.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_points_to_next_level_prefix.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_points_to_next_level_prefix.Location = new System.Drawing.Point(7, 152);
      this.lbl_points_to_next_level_prefix.Name = "lbl_points_to_next_level_prefix";
      this.lbl_points_to_next_level_prefix.Size = new System.Drawing.Size(253, 38);
      this.lbl_points_to_next_level_prefix.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_points_to_next_level_prefix.TabIndex = 61;
      this.lbl_points_to_next_level_prefix.Text = "xPuntos Faltantes";
      this.lbl_points_to_next_level_prefix.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_points_level_prefix
      // 
      this.lbl_points_level_prefix.BackColor = System.Drawing.Color.Transparent;
      this.lbl_points_level_prefix.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_points_level_prefix.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_points_level_prefix.Location = new System.Drawing.Point(7, 118);
      this.lbl_points_level_prefix.Name = "lbl_points_level_prefix";
      this.lbl_points_level_prefix.Size = new System.Drawing.Size(253, 38);
      this.lbl_points_level_prefix.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_points_level_prefix.TabIndex = 60;
      this.lbl_points_level_prefix.Text = "xPuntos Nivel*";
      this.lbl_points_level_prefix.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_points_level
      // 
      this.lbl_points_level.BackColor = System.Drawing.Color.Transparent;
      this.lbl_points_level.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_points_level.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_points_level.Location = new System.Drawing.Point(269, 118);
      this.lbl_points_level.Name = "lbl_points_level";
      this.lbl_points_level.Size = new System.Drawing.Size(83, 38);
      this.lbl_points_level.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_points_level.TabIndex = 59;
      this.lbl_points_level.Text = "x10.000";
      this.lbl_points_level.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_points_req_prefix
      // 
      this.lbl_points_req_prefix.BackColor = System.Drawing.Color.Transparent;
      this.lbl_points_req_prefix.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_points_req_prefix.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_points_req_prefix.Location = new System.Drawing.Point(6, 46);
      this.lbl_points_req_prefix.Name = "lbl_points_req_prefix";
      this.lbl_points_req_prefix.Size = new System.Drawing.Size(254, 38);
      this.lbl_points_req_prefix.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_points_req_prefix.TabIndex = 58;
      this.lbl_points_req_prefix.Text = "xPuntos Requeridos";
      this.lbl_points_req_prefix.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_points_req
      // 
      this.lbl_points_req.BackColor = System.Drawing.Color.Transparent;
      this.lbl_points_req.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_points_req.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_points_req.Location = new System.Drawing.Point(263, 46);
      this.lbl_points_req.Name = "lbl_points_req";
      this.lbl_points_req.Size = new System.Drawing.Size(86, 38);
      this.lbl_points_req.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_points_req.TabIndex = 57;
      this.lbl_points_req.Text = "---";
      this.lbl_points_req.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_level_entered
      // 
      this.lbl_level_entered.BackColor = System.Drawing.Color.Transparent;
      this.lbl_level_entered.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_level_entered.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_level_entered.Location = new System.Drawing.Point(289, 76);
      this.lbl_level_entered.Name = "lbl_level_entered";
      this.lbl_level_entered.Size = new System.Drawing.Size(145, 38);
      this.lbl_level_entered.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_level_entered.TabIndex = 72;
      this.lbl_level_entered.Text = "x10-10-2010";
      this.lbl_level_entered.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_points_sufix
      // 
      this.lbl_points_sufix.BackColor = System.Drawing.Color.Transparent;
      this.lbl_points_sufix.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_points_sufix.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_points_sufix.Location = new System.Drawing.Point(443, 190);
      this.lbl_points_sufix.Name = "lbl_points_sufix";
      this.lbl_points_sufix.Size = new System.Drawing.Size(93, 38);
      this.lbl_points_sufix.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_points_sufix.TabIndex = 78;
      this.lbl_points_sufix.Text = "xPuntos";
      this.lbl_points_sufix.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_level_entered_prefix
      // 
      this.lbl_level_entered_prefix.BackColor = System.Drawing.Color.Transparent;
      this.lbl_level_entered_prefix.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_level_entered_prefix.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_level_entered_prefix.Location = new System.Drawing.Point(8, 76);
      this.lbl_level_entered_prefix.Name = "lbl_level_entered_prefix";
      this.lbl_level_entered_prefix.Size = new System.Drawing.Size(273, 38);
      this.lbl_level_entered_prefix.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_level_entered_prefix.TabIndex = 73;
      this.lbl_level_entered_prefix.Text = "xFecha Entrada";
      this.lbl_level_entered_prefix.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_points
      // 
      this.lbl_points.BackColor = System.Drawing.Color.Transparent;
      this.lbl_points.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_points.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_points.Location = new System.Drawing.Point(309, 190);
      this.lbl_points.Name = "lbl_points";
      this.lbl_points.Size = new System.Drawing.Size(125, 38);
      this.lbl_points.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_points.TabIndex = 76;
      this.lbl_points.Text = "x10.000";
      this.lbl_points.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_level_expiration
      // 
      this.lbl_level_expiration.BackColor = System.Drawing.Color.Transparent;
      this.lbl_level_expiration.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_level_expiration.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_level_expiration.Location = new System.Drawing.Point(289, 114);
      this.lbl_level_expiration.Name = "lbl_level_expiration";
      this.lbl_level_expiration.Size = new System.Drawing.Size(221, 38);
      this.lbl_level_expiration.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_level_expiration.TabIndex = 74;
      this.lbl_level_expiration.Text = "x10-10-2010";
      this.lbl_level_expiration.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_points_prefix
      // 
      this.lbl_points_prefix.BackColor = System.Drawing.Color.Transparent;
      this.lbl_points_prefix.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_points_prefix.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_points_prefix.Location = new System.Drawing.Point(8, 190);
      this.lbl_points_prefix.Name = "lbl_points_prefix";
      this.lbl_points_prefix.Size = new System.Drawing.Size(273, 38);
      this.lbl_points_prefix.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_points_prefix.TabIndex = 77;
      this.lbl_points_prefix.Text = "xBalance";
      this.lbl_points_prefix.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_level_expiration_prefix
      // 
      this.lbl_level_expiration_prefix.BackColor = System.Drawing.Color.Transparent;
      this.lbl_level_expiration_prefix.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_level_expiration_prefix.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_level_expiration_prefix.Location = new System.Drawing.Point(8, 114);
      this.lbl_level_expiration_prefix.Name = "lbl_level_expiration_prefix";
      this.lbl_level_expiration_prefix.Size = new System.Drawing.Size(273, 38);
      this.lbl_level_expiration_prefix.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_level_expiration_prefix.TabIndex = 75;
      this.lbl_level_expiration_prefix.Text = "xFecha Caducidad";
      this.lbl_level_expiration_prefix.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_level
      // 
      this.lbl_level.BackColor = System.Drawing.Color.Transparent;
      this.lbl_level.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_level.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_level.Location = new System.Drawing.Point(289, 38);
      this.lbl_level.Name = "lbl_level";
      this.lbl_level.Size = new System.Drawing.Size(221, 38);
      this.lbl_level.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_level.TabIndex = 70;
      this.lbl_level.Text = "xSelección";
      this.lbl_level.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // block
      // 
      this.block.Controls.Add(this.pb_unblocked);
      this.block.Controls.Add(this.pb_blocked);
      this.block.Controls.Add(this.lbl_block_description);
      this.block.Controls.Add(this.txt_block_description);
      this.block.Controls.Add(this.lbl_blocked);
      this.block.Location = new System.Drawing.Point(4, 45);
      this.block.Name = "block";
      this.block.Padding = new System.Windows.Forms.Padding(3);
      this.block.Size = new System.Drawing.Size(901, 453);
      this.block.TabIndex = 10;
      this.block.Text = "xBloqueo";
      this.block.UseVisualStyleBackColor = true;
      // 
      // pb_unblocked
      // 
      this.pb_unblocked.Enabled = false;
      this.pb_unblocked.Image = ((System.Drawing.Image)(resources.GetObject("pb_unblocked.Image")));
      this.pb_unblocked.Location = new System.Drawing.Point(15, 12);
      this.pb_unblocked.Name = "pb_unblocked";
      this.pb_unblocked.Size = new System.Drawing.Size(48, 46);
      this.pb_unblocked.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
      this.pb_unblocked.TabIndex = 83;
      this.pb_unblocked.TabStop = false;
      // 
      // pb_blocked
      // 
      this.pb_blocked.Enabled = false;
      this.pb_blocked.Image = ((System.Drawing.Image)(resources.GetObject("pb_blocked.Image")));
      this.pb_blocked.Location = new System.Drawing.Point(15, 13);
      this.pb_blocked.Name = "pb_blocked";
      this.pb_blocked.Size = new System.Drawing.Size(48, 46);
      this.pb_blocked.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
      this.pb_blocked.TabIndex = 82;
      this.pb_blocked.TabStop = false;
      // 
      // lbl_block_description
      // 
      this.lbl_block_description.AutoSize = true;
      this.lbl_block_description.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_block_description.ForeColor = System.Drawing.Color.Black;
      this.lbl_block_description.Location = new System.Drawing.Point(40, 71);
      this.lbl_block_description.Name = "lbl_block_description";
      this.lbl_block_description.Size = new System.Drawing.Size(119, 16);
      this.lbl_block_description.TabIndex = 78;
      this.lbl_block_description.Text = "xMotivoBloqueo";
      this.lbl_block_description.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // txt_block_description
      // 
      this.txt_block_description.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_block_description.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_block_description.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_block_description.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_block_description.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_block_description.CornerRadius = 5;
      this.txt_block_description.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_block_description.Location = new System.Drawing.Point(43, 90);
      this.txt_block_description.MaxLength = 100;
      this.txt_block_description.Multiline = true;
      this.txt_block_description.Name = "txt_block_description";
      this.txt_block_description.PasswordChar = '\0';
      this.txt_block_description.ReadOnly = true;
      this.txt_block_description.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
      this.txt_block_description.SelectedText = "";
      this.txt_block_description.SelectionLength = 0;
      this.txt_block_description.SelectionStart = 0;
      this.txt_block_description.Size = new System.Drawing.Size(820, 286);
      this.txt_block_description.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.MULTILINE;
      this.txt_block_description.TabIndex = 79;
      this.txt_block_description.TabStop = false;
      this.txt_block_description.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_block_description.UseSystemPasswordChar = false;
      this.txt_block_description.WaterMark = null;
      this.txt_block_description.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // lbl_blocked
      // 
      this.lbl_blocked.AutoSize = true;
      this.lbl_blocked.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold);
      this.lbl_blocked.ForeColor = System.Drawing.Color.Black;
      this.lbl_blocked.Location = new System.Drawing.Point(69, 18);
      this.lbl_blocked.MaximumSize = new System.Drawing.Size(800, 0);
      this.lbl_blocked.MinimumSize = new System.Drawing.Size(0, 35);
      this.lbl_blocked.Name = "lbl_blocked";
      this.lbl_blocked.Size = new System.Drawing.Size(136, 35);
      this.lbl_blocked.TabIndex = 77;
      this.lbl_blocked.Text = "xBloqueado";
      this.lbl_blocked.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // comments
      // 
      this.comments.Controls.Add(this.chk_show_comments);
      this.comments.Controls.Add(this.txt_comments);
      this.comments.Location = new System.Drawing.Point(4, 45);
      this.comments.Name = "comments";
      this.comments.Size = new System.Drawing.Size(901, 453);
      this.comments.TabIndex = 3;
      this.comments.Text = "xComentarios";
      this.comments.UseVisualStyleBackColor = true;
      // 
      // chk_show_comments
      // 
      this.chk_show_comments.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.chk_show_comments.AutoSize = true;
      this.chk_show_comments.BackColor = System.Drawing.Color.Transparent;
      this.chk_show_comments.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.chk_show_comments.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.chk_show_comments.Location = new System.Drawing.Point(11, -27029);
      this.chk_show_comments.MinimumSize = new System.Drawing.Size(370, 30);
      this.chk_show_comments.Name = "chk_show_comments";
      this.chk_show_comments.Padding = new System.Windows.Forms.Padding(5);
      this.chk_show_comments.Size = new System.Drawing.Size(370, 33);
      this.chk_show_comments.TabIndex = 40;
      this.chk_show_comments.Text = "xShow comments when reading player card";
      this.chk_show_comments.UseVisualStyleBackColor = true;
      // 
      // txt_comments
      // 
      this.txt_comments.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_comments.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_comments.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_comments.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_comments.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_comments.CornerRadius = 5;
      this.txt_comments.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_comments.Location = new System.Drawing.Point(11, 17);
      this.txt_comments.MaxLength = 100;
      this.txt_comments.Multiline = true;
      this.txt_comments.Name = "txt_comments";
      this.txt_comments.PasswordChar = '\0';
      this.txt_comments.ReadOnly = false;
      this.txt_comments.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
      this.txt_comments.SelectedText = "";
      this.txt_comments.SelectionLength = 0;
      this.txt_comments.SelectionStart = 0;
      this.txt_comments.Size = new System.Drawing.Size(874, 338);
      this.txt_comments.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.MULTILINE;
      this.txt_comments.TabIndex = 1;
      this.txt_comments.TabStop = false;
      this.txt_comments.Tag = "HolderComments";
      this.txt_comments.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_comments.UseSystemPasswordChar = false;
      this.txt_comments.WaterMark = null;
      this.txt_comments.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // pin
      // 
      this.pin.Controls.Add(this.lbl_pin_msg);
      this.pin.Controls.Add(this.txt_confirm_pin);
      this.pin.Controls.Add(this.txt_pin);
      this.pin.Controls.Add(this.btn_generate_random_pin);
      this.pin.Controls.Add(this.btn_save_pin);
      this.pin.Controls.Add(this.lbl_confirm_pin);
      this.pin.Controls.Add(this.lbl_pin);
      this.pin.Location = new System.Drawing.Point(4, 45);
      this.pin.Name = "pin";
      this.pin.Padding = new System.Windows.Forms.Padding(3);
      this.pin.Size = new System.Drawing.Size(901, 453);
      this.pin.TabIndex = 9;
      this.pin.Text = "xPin";
      this.pin.UseVisualStyleBackColor = true;
      // 
      // lbl_pin_msg
      // 
      this.lbl_pin_msg.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
      this.lbl_pin_msg.Location = new System.Drawing.Point(473, 68);
      this.lbl_pin_msg.Name = "lbl_pin_msg";
      this.lbl_pin_msg.Size = new System.Drawing.Size(285, 57);
      this.lbl_pin_msg.TabIndex = 11;
      this.lbl_pin_msg.Text = "xPinMsg";
      // 
      // txt_confirm_pin
      // 
      this.txt_confirm_pin.AllowSpace = false;
      this.txt_confirm_pin.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_confirm_pin.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_confirm_pin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_confirm_pin.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_confirm_pin.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_confirm_pin.CornerRadius = 5;
      this.txt_confirm_pin.FillWithCeros = false;
      this.txt_confirm_pin.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_confirm_pin.Location = new System.Drawing.Point(277, 87);
      this.txt_confirm_pin.MaxLength = 12;
      this.txt_confirm_pin.Multiline = false;
      this.txt_confirm_pin.Name = "txt_confirm_pin";
      this.txt_confirm_pin.PasswordChar = '*';
      this.txt_confirm_pin.ReadOnly = false;
      this.txt_confirm_pin.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_confirm_pin.SelectedText = "";
      this.txt_confirm_pin.SelectionLength = 0;
      this.txt_confirm_pin.SelectionStart = 0;
      this.txt_confirm_pin.Size = new System.Drawing.Size(163, 40);
      this.txt_confirm_pin.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_confirm_pin.TabIndex = 1;
      this.txt_confirm_pin.TabStop = false;
      this.txt_confirm_pin.Text = "0000";
      this.txt_confirm_pin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.txt_confirm_pin.UseSystemPasswordChar = false;
      this.txt_confirm_pin.WaterMark = "****";
      this.txt_confirm_pin.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      this.txt_confirm_pin.TextChanged += new System.EventHandler(this.txt_confirm_pin_TextChanged);
      this.txt_confirm_pin.Enter += new System.EventHandler(this.txt_confirm_pin_Enter);
      // 
      // txt_pin
      // 
      this.txt_pin.AllowSpace = false;
      this.txt_pin.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_pin.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_pin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_pin.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_pin.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_pin.CornerRadius = 5;
      this.txt_pin.FillWithCeros = false;
      this.txt_pin.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_pin.Location = new System.Drawing.Point(277, 46);
      this.txt_pin.MaxLength = 12;
      this.txt_pin.Multiline = false;
      this.txt_pin.Name = "txt_pin";
      this.txt_pin.PasswordChar = '*';
      this.txt_pin.ReadOnly = false;
      this.txt_pin.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_pin.SelectedText = "";
      this.txt_pin.SelectionLength = 0;
      this.txt_pin.SelectionStart = 0;
      this.txt_pin.Size = new System.Drawing.Size(163, 40);
      this.txt_pin.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_pin.TabIndex = 0;
      this.txt_pin.TabStop = false;
      this.txt_pin.Text = "0000";
      this.txt_pin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.txt_pin.UseSystemPasswordChar = false;
      this.txt_pin.WaterMark = "****";
      this.txt_pin.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      this.txt_pin.TextChanged += new System.EventHandler(this.txt_pin_TextChanged);
      this.txt_pin.Enter += new System.EventHandler(this.txt_pin_Enter);
      // 
      // btn_generate_random_pin
      // 
      this.btn_generate_random_pin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_generate_random_pin.FlatAppearance.BorderSize = 0;
      this.btn_generate_random_pin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_generate_random_pin.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_generate_random_pin.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_generate_random_pin.Image = null;
      this.btn_generate_random_pin.IsSelected = false;
      this.btn_generate_random_pin.Location = new System.Drawing.Point(458, 146);
      this.btn_generate_random_pin.Name = "btn_generate_random_pin";
      this.btn_generate_random_pin.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_generate_random_pin.Size = new System.Drawing.Size(163, 48);
      this.btn_generate_random_pin.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_generate_random_pin.TabIndex = 10;
      this.btn_generate_random_pin.TabStop = false;
      this.btn_generate_random_pin.Text = "XGENERATEPIN";
      this.btn_generate_random_pin.UseVisualStyleBackColor = false;
      this.btn_generate_random_pin.Click += new System.EventHandler(this.btn_generate_random_pin_Click);
      // 
      // btn_save_pin
      // 
      this.btn_save_pin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_save_pin.FlatAppearance.BorderSize = 0;
      this.btn_save_pin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_save_pin.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_save_pin.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_save_pin.Image = null;
      this.btn_save_pin.IsSelected = false;
      this.btn_save_pin.Location = new System.Drawing.Point(277, 147);
      this.btn_save_pin.Name = "btn_save_pin";
      this.btn_save_pin.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_save_pin.Size = new System.Drawing.Size(163, 47);
      this.btn_save_pin.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_save_pin.TabIndex = 9;
      this.btn_save_pin.TabStop = false;
      this.btn_save_pin.Text = "XSAVEPIN";
      this.btn_save_pin.UseVisualStyleBackColor = false;
      this.btn_save_pin.Click += new System.EventHandler(this.btn_save_pin_Click);
      // 
      // lbl_confirm_pin
      // 
      this.lbl_confirm_pin.AutoSize = true;
      this.lbl_confirm_pin.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
      this.lbl_confirm_pin.Location = new System.Drawing.Point(147, 100);
      this.lbl_confirm_pin.Name = "lbl_confirm_pin";
      this.lbl_confirm_pin.Size = new System.Drawing.Size(104, 20);
      this.lbl_confirm_pin.TabIndex = 7;
      this.lbl_confirm_pin.Text = "xConfirmPin";
      // 
      // lbl_pin
      // 
      this.lbl_pin.AutoSize = true;
      this.lbl_pin.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
      this.lbl_pin.Location = new System.Drawing.Point(147, 59);
      this.lbl_pin.Name = "lbl_pin";
      this.lbl_pin.Size = new System.Drawing.Size(42, 20);
      this.lbl_pin.TabIndex = 6;
      this.lbl_pin.Text = "xPin";
      // 
      // credit_line
      // 
      this.credit_line.Controls.Add(this.lbl_credit_line_available);
      this.credit_line.Controls.Add(this.lbl_credit_line_available_value);
      this.credit_line.Controls.Add(this.lbl_credit_line_extension);
      this.credit_line.Controls.Add(this.lbl_credit_line_extension_value);
      this.credit_line.Controls.Add(this.lbl_credit_line_status);
      this.credit_line.Controls.Add(this.lbl_credit_line_status_value);
      this.credit_line.Controls.Add(this.lbl_credit_line_spent);
      this.credit_line.Controls.Add(this.lbl_credit_line_spent_value);
      this.credit_line.Controls.Add(this.lbl_credit_line_limit);
      this.credit_line.Controls.Add(this.btn_credit_line_payback);
      this.credit_line.Controls.Add(this.lbl_credit_line_limit_value);
      this.credit_line.Location = new System.Drawing.Point(4, 45);
      this.credit_line.Name = "credit_line";
      this.credit_line.Padding = new System.Windows.Forms.Padding(3);
      this.credit_line.Size = new System.Drawing.Size(901, 453);
      this.credit_line.TabIndex = 12;
      this.credit_line.Text = "xCreditLine";
      this.credit_line.UseVisualStyleBackColor = true;
      // 
      // lbl_credit_line_available
      // 
      this.lbl_credit_line_available.AutoSize = true;
      this.lbl_credit_line_available.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
      this.lbl_credit_line_available.Location = new System.Drawing.Point(147, 145);
      this.lbl_credit_line_available.Name = "lbl_credit_line_available";
      this.lbl_credit_line_available.Size = new System.Drawing.Size(89, 20);
      this.lbl_credit_line_available.TabIndex = 20;
      this.lbl_credit_line_available.Text = "xAvailable";
      // 
      // lbl_credit_line_available_value
      // 
      this.lbl_credit_line_available_value.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
      this.lbl_credit_line_available_value.Location = new System.Drawing.Point(304, 145);
      this.lbl_credit_line_available_value.Name = "lbl_credit_line_available_value";
      this.lbl_credit_line_available_value.Size = new System.Drawing.Size(333, 20);
      this.lbl_credit_line_available_value.TabIndex = 19;
      this.lbl_credit_line_available_value.Text = "xAvailableValue";
      this.lbl_credit_line_available_value.TextAlign = System.Drawing.ContentAlignment.TopRight;
      // 
      // lbl_credit_line_extension
      // 
      this.lbl_credit_line_extension.AutoSize = true;
      this.lbl_credit_line_extension.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
      this.lbl_credit_line_extension.Location = new System.Drawing.Point(147, 211);
      this.lbl_credit_line_extension.Name = "lbl_credit_line_extension";
      this.lbl_credit_line_extension.Size = new System.Drawing.Size(144, 20);
      this.lbl_credit_line_extension.TabIndex = 18;
      this.lbl_credit_line_extension.Text = "xCreditExtension";
      // 
      // lbl_credit_line_extension_value
      // 
      this.lbl_credit_line_extension_value.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
      this.lbl_credit_line_extension_value.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
      this.lbl_credit_line_extension_value.Location = new System.Drawing.Point(304, 211);
      this.lbl_credit_line_extension_value.Name = "lbl_credit_line_extension_value";
      this.lbl_credit_line_extension_value.Size = new System.Drawing.Size(333, 20);
      this.lbl_credit_line_extension_value.TabIndex = 17;
      this.lbl_credit_line_extension_value.Text = "xCreditExtensionValue";
      this.lbl_credit_line_extension_value.TextAlign = System.Drawing.ContentAlignment.TopRight;
      // 
      // lbl_credit_line_status
      // 
      this.lbl_credit_line_status.AutoSize = true;
      this.lbl_credit_line_status.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
      this.lbl_credit_line_status.Location = new System.Drawing.Point(147, 54);
      this.lbl_credit_line_status.Name = "lbl_credit_line_status";
      this.lbl_credit_line_status.Size = new System.Drawing.Size(118, 20);
      this.lbl_credit_line_status.TabIndex = 16;
      this.lbl_credit_line_status.Text = "xCreditStatus";
      // 
      // lbl_credit_line_status_value
      // 
      this.lbl_credit_line_status_value.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
      this.lbl_credit_line_status_value.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
      this.lbl_credit_line_status_value.Location = new System.Drawing.Point(304, 54);
      this.lbl_credit_line_status_value.Name = "lbl_credit_line_status_value";
      this.lbl_credit_line_status_value.RightToLeft = System.Windows.Forms.RightToLeft.No;
      this.lbl_credit_line_status_value.Size = new System.Drawing.Size(333, 20);
      this.lbl_credit_line_status_value.TabIndex = 15;
      this.lbl_credit_line_status_value.Text = "xCreditStatusValue";
      this.lbl_credit_line_status_value.TextAlign = System.Drawing.ContentAlignment.TopRight;
      // 
      // lbl_credit_line_spent
      // 
      this.lbl_credit_line_spent.AutoSize = true;
      this.lbl_credit_line_spent.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
      this.lbl_credit_line_spent.Location = new System.Drawing.Point(147, 114);
      this.lbl_credit_line_spent.Name = "lbl_credit_line_spent";
      this.lbl_credit_line_spent.Size = new System.Drawing.Size(127, 20);
      this.lbl_credit_line_spent.TabIndex = 14;
      this.lbl_credit_line_spent.Text = "xAmountSpent";
      // 
      // lbl_credit_line_spent_value
      // 
      this.lbl_credit_line_spent_value.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
      this.lbl_credit_line_spent_value.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
      this.lbl_credit_line_spent_value.Location = new System.Drawing.Point(304, 114);
      this.lbl_credit_line_spent_value.Name = "lbl_credit_line_spent_value";
      this.lbl_credit_line_spent_value.Size = new System.Drawing.Size(333, 20);
      this.lbl_credit_line_spent_value.TabIndex = 13;
      this.lbl_credit_line_spent_value.Text = "xAmountSpentValue";
      this.lbl_credit_line_spent_value.TextAlign = System.Drawing.ContentAlignment.TopRight;
      // 
      // lbl_credit_line_limit
      // 
      this.lbl_credit_line_limit.AutoSize = true;
      this.lbl_credit_line_limit.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
      this.lbl_credit_line_limit.Location = new System.Drawing.Point(147, 85);
      this.lbl_credit_line_limit.Name = "lbl_credit_line_limit";
      this.lbl_credit_line_limit.Size = new System.Drawing.Size(103, 20);
      this.lbl_credit_line_limit.TabIndex = 12;
      this.lbl_credit_line_limit.Text = "xCreditLimit";
      // 
      // btn_credit_line_payback
      // 
      this.btn_credit_line_payback.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_credit_line_payback.FlatAppearance.BorderSize = 0;
      this.btn_credit_line_payback.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_credit_line_payback.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_credit_line_payback.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_credit_line_payback.Image = null;
      this.btn_credit_line_payback.IsSelected = false;
      this.btn_credit_line_payback.Location = new System.Drawing.Point(716, 331);
      this.btn_credit_line_payback.Name = "btn_credit_line_payback";
      this.btn_credit_line_payback.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_credit_line_payback.Size = new System.Drawing.Size(163, 47);
      this.btn_credit_line_payback.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_credit_line_payback.TabIndex = 11;
      this.btn_credit_line_payback.TabStop = false;
      this.btn_credit_line_payback.Text = "XPAYBACK";
      this.btn_credit_line_payback.UseVisualStyleBackColor = false;
      this.btn_credit_line_payback.Click += new System.EventHandler(this.btn_credit_line_payback_Click);
      // 
      // lbl_credit_line_limit_value
      // 
      this.lbl_credit_line_limit_value.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
      this.lbl_credit_line_limit_value.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
      this.lbl_credit_line_limit_value.Location = new System.Drawing.Point(304, 85);
      this.lbl_credit_line_limit_value.Name = "lbl_credit_line_limit_value";
      this.lbl_credit_line_limit_value.Size = new System.Drawing.Size(333, 20);
      this.lbl_credit_line_limit_value.TabIndex = 10;
      this.lbl_credit_line_limit_value.Text = "xCreditLimitValue";
      this.lbl_credit_line_limit_value.TextAlign = System.Drawing.ContentAlignment.TopRight;
      // 
      // lbl_level_prefix1
      // 
      this.lbl_level_prefix1.BackColor = System.Drawing.Color.Transparent;
      this.lbl_level_prefix1.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_level_prefix1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_level_prefix1.Location = new System.Drawing.Point(3, 114);
      this.lbl_level_prefix1.Name = "lbl_level_prefix1";
      this.lbl_level_prefix1.Size = new System.Drawing.Size(98, 26);
      this.lbl_level_prefix1.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_level_prefix1.TabIndex = 72;
      this.lbl_level_prefix1.Text = "xNivel";
      this.lbl_level_prefix1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // lbl_level1
      // 
      this.lbl_level1.BackColor = System.Drawing.Color.Transparent;
      this.lbl_level1.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_level1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_level1.Location = new System.Drawing.Point(3, 140);
      this.lbl_level1.Name = "lbl_level1";
      this.lbl_level1.Size = new System.Drawing.Size(98, 22);
      this.lbl_level1.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_level1.TabIndex = 73;
      this.lbl_level1.Text = "xSelección";
      this.lbl_level1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // PlayerEditDetailsCashierView
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.ClientSize = new System.Drawing.Size(1018, 620);
      this.ControlBox = false;
      this.DoubleBuffered = true;
      this.KeyPreview = true;
      this.MaximizeBox = false;
      this.MaximumSize = new System.Drawing.Size(0, 0);
      this.MinimizeBox = false;
      this.MinimumSize = new System.Drawing.Size(0, 0);
      this.Name = "PlayerEditDetailsCashierView";
      this.ShowIcon = false;
      this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
      this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
      this.Style = WSI.Cashier.Controls.frm_base.BaseFormStyle.PRINCIPAL;
      this.Text = "vw_PlayerEditDetailsReception";
      this.Shown += new System.EventHandler(this.PlayerEditDetailsCashierView_Shown);
      this.pnl_data.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.m_img_photo)).EndInit();
      this.tab_player_edit.ResumeLayout(false);
      this.principal.ResumeLayout(false);
      this.tlp_name.ResumeLayout(false);
      this.tlp_principal.ResumeLayout(false);
      this.tlp_principal.PerformLayout();
      this.tlp_principal1.ResumeLayout(false);
      this.contact.ResumeLayout(false);
      this.contact.PerformLayout();
      this.tlp_contact.ResumeLayout(false);
      this.address.ResumeLayout(false);
      this.beneficiary.ResumeLayout(false);
      this.beneficiary.PerformLayout();
      this.tlp_ben_principal.ResumeLayout(false);
      this.tlp_ben_principal1.ResumeLayout(false);
      this.tlp_ben_name.ResumeLayout(false);
      this.points.ResumeLayout(false);
      this.gb_next_level.ResumeLayout(false);
      this.block.ResumeLayout(false);
      this.block.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pb_unblocked)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.pb_blocked)).EndInit();
      this.comments.ResumeLayout(false);
      this.comments.PerformLayout();
      this.pin.ResumeLayout(false);
      this.pin.PerformLayout();
      this.credit_line.ResumeLayout(false);
      this.credit_line.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Label lbl_account_edit_title;
    private System.Windows.Forms.Label lbl_msg_count_blink;
    private System.Windows.Forms.Label lbl_msg_blink;
    private WSI.Cashier.Controls.uc_round_button btn_keyboard;
    private WSI.Cashier.Controls.uc_round_button_long_press btn_id_card_scan;
    private WSI.Cashier.Controls.uc_round_button btn_print_information;
    private WSI.Cashier.Controls.uc_round_button btn_cancel;
    private WSI.Cashier.Controls.uc_round_button btn_ok;
    private UcPhotobox m_img_photo;
    private uc_round_tab_control tab_player_edit;
    private System.Windows.Forms.TabPage contact;
    private System.Windows.Forms.TableLayoutPanel tlp_contact;
    private System.Windows.Forms.Label lbl_phone_01;
    private PhoneNumberTextBox txt_phone_01;
    private System.Windows.Forms.Label lbl_phone_02;
    private PhoneNumberTextBox txt_phone_02;
    private EmailTextBox txt_email_01;
    private System.Windows.Forms.Label lbl_email_01;
    private System.Windows.Forms.TabPage address;
    private System.Windows.Forms.TabPage beneficiary;
    private System.Windows.Forms.TableLayoutPanel tlp_ben_principal1;
    private System.Windows.Forms.Label lbl_beneficiary_gender;
    private uc_round_auto_combobox cb_beneficiary_gender;
    private System.Windows.Forms.Label lbl_beneficiary_occupation;
    private uc_round_auto_combobox cb_beneficiary_occupation;
    private WSI.Cashier.Controls.uc_round_button btn_ben_scan_docs;
    private System.Windows.Forms.TableLayoutPanel tlp_ben_principal;
    private uc_datetime uc_dt_ben_birth;
    private System.Windows.Forms.Label lbl_beneficiary_RFC;
    private CharacterTextBox txt_beneficiary_RFC;
    private System.Windows.Forms.Label lbl_beneficiary_CURP;
    private CharacterTextBox txt_beneficiary_CURP;
    private System.Windows.Forms.Label lbl_beneficiary_birth_date;
    private System.Windows.Forms.TableLayoutPanel tlp_ben_name;
    private System.Windows.Forms.Label lbl_beneficiary_name;
    private CharacterTextBox txt_beneficiary_name;
    private System.Windows.Forms.Label lbl_beneficiary_last_name1;
    private CharacterTextBox txt_beneficiary_last_name1;
    private System.Windows.Forms.Label lbl_beneficiary_last_name2;
    private CharacterTextBox txt_beneficiary_last_name2;
    private uc_checkBox chk_holder_has_beneficiary;
    private System.Windows.Forms.Label lbl_beneficiary_full_name;
    private System.Windows.Forms.TabPage comments;
    private uc_checkBox chk_show_comments;
    private WSI.Cashier.Controls.uc_round_textbox txt_comments;
    private System.Windows.Forms.TabPage pin;
    private System.Windows.Forms.Label lbl_pin_msg;
    private NumericTextBox txt_confirm_pin;
    private NumericTextBox txt_pin;
    private WSI.Cashier.Controls.uc_round_button btn_generate_random_pin;
    private WSI.Cashier.Controls.uc_round_button btn_save_pin;
    private System.Windows.Forms.Label lbl_confirm_pin;
    private System.Windows.Forms.Label lbl_pin;
    private System.Windows.Forms.TabPage block;
    public System.Windows.Forms.Label lbl_block_description;
    public WSI.Cashier.Controls.uc_round_textbox txt_block_description;
    public System.Windows.Forms.Label lbl_blocked;
    private System.Windows.Forms.TabPage points;
    private System.Windows.Forms.Label lbl_email_02;
    private EmailTextBox txt_email_02;
    private System.Windows.Forms.Label lbl_twitter;
    private WSI.Cashier.Controls.uc_round_textbox txt_twitter;
    private uc_address HolderAddress;
    public PictureBox pb_unblocked;
    public PictureBox pb_blocked;
    private uc_label lbl_points_to_maintain_prefix;
    private uc_label lbl_points_to_maintain;
    private uc_label lbl_level_prefix;
    private uc_round_panel gb_next_level;
    private uc_label lbl_points_last_days;
    private uc_label lbl_points_discretionaries_prefix;
    private uc_label lbl_points_discretionaries;
    private uc_label lbl_points_generated_prefix;
    private uc_label lbl_points_generated;
    private uc_label lbl_points_to_next_level;
    private uc_label lbl_points_to_next_level_prefix;
    private uc_label lbl_points_level_prefix;
    private uc_label lbl_points_level;
    private uc_label lbl_points_req_prefix;
    private uc_label lbl_points_req;
    private uc_label lbl_level_entered;
    private uc_label lbl_points_sufix;
    private uc_label lbl_level_entered_prefix;
    private uc_label lbl_points;
    private uc_label lbl_level_expiration;
    private uc_label lbl_points_prefix;
    private uc_label lbl_level_expiration_prefix;
    private uc_label lbl_level;
    private Label label1;
    private Label label2;
    private TabPage credit_line;
    private Label lbl_credit_line_available;
    private Label lbl_credit_line_available_value;
    private Label lbl_credit_line_extension;
    private Label lbl_credit_line_extension_value;
    private Label lbl_credit_line_status;
    private Label lbl_credit_line_status_value;
    private Label lbl_credit_line_spent;
    private Label lbl_credit_line_spent_value;
    private Label lbl_credit_line_limit;
    private Label lbl_credit_line_limit_value;
    private uc_round_button btn_credit_line_payback;
    
    private uc_label lbl_level1;
    private uc_label lbl_level_prefix1;
    private TabPage principal;
    private TableLayoutPanel tlp_name;
    private CharacterTextBox txt_name4;
    private Label lbl_name4;
    private CharacterTextBox txt_name;
    private Label lbl_name;
    private Label lbl_last_name1;
    private CharacterTextBox txt_last_name1;
    private CharacterTextBox txt_last_name2;
    private Label lbl_last_name2;
    private TableLayoutPanel tlp_principal;
    private uc_datetime uc_dt_birth;
    private CharacterTextBox txt_CURP;
    private Label lbl_wedding_date;
    private Label lbl_birth_date;
    private CharacterTextBox txt_RFC;
    private Label lbl_CURP;
    private uc_round_button btn_RFC;
    private uc_round_auto_combobox cb_document_type;
    private Label lbl_document;
    private Label lbl_type_document;
    private CharacterTextBox txt_document;
    private uc_datetime uc_dt_wedding;
    private Label lbl_docuemnt_counter;
    private uc_label lbl_age;
    private Label lbl_expiration_doc_date;
    private uc_datetime uc_dt_expiration_doc;
    private TableLayoutPanel tlp_principal1;
    private uc_round_button btn_scan_docs;
    private uc_round_auto_combobox cb_marital;
    private Label lbl_marital_status;
    private uc_round_auto_combobox cb_occupation;
    private Label lbl_occupation;
    private uc_round_auto_combobox cb_birth_country;
    private Label lbl_birth_country;
    private uc_round_auto_combobox cb_nationality;
    private Label lbl_nationality;
    private uc_round_auto_combobox cb_gender;
    private Label lbl_gender;
    private Label lbl_full_name;

  }
}