﻿//------------------------------------------------------------------------------
// Copyright © 2007-2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: PlayerEditDetailsReceptionView.cs
// 
//   DESCRIPTION: Implementation of cachier controller class
//                
//                
//                
//                
//                
//                
// 
//        AUTHOR: Scott Adamson.
// 
// CREATION DATE: 02-DEC-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 02-DEC-2007 SJA    First release.
// 10-MAY-2015 JBP    Product Backlog Item 12931:Visitas / Recepción: 2º Ticket LOPD
// 30-MAY-2016 JCA    Fixed Bug 13692:Recepción: error al crear un Nuevo Cliente con la caja cerrada
// 08-JUN-2016 JBP    Fixed Bug 14154:Errores de diseño de Recepción
// 08-DEC-2016 ESE    Bug 18298:Mejora recepción: añadir mensaje mensaje para cliente - caducará hoy el documento
// 10-ENE-2017 DPC    Bug 21943:Recepción: Permite guardar Accounts con el mismo HolderId desde la opción "GUARDAR PIN"
//------------------------------------------------------------------------------
using WSI.Cashier.Controls;
using WSI.Cashier.Resources;
using WSI.Common;

namespace WSI.Cashier.MVC.View.PlayerEditDetails
{
  partial class PlayerEditDetailsReceptionView
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
      this.lbl_account_edit_title = new WSI.Cashier.Controls.uc_label();
      this.lbl_msg_count_blink = new WSI.Cashier.Controls.uc_label();
      this.lbl_msg_blink = new WSI.Cashier.Controls.uc_label();
      this.btn_image_black_list = new WSI.Cashier.Controls.uc_round_button();
      this.lbl_black_list = new WSI.Cashier.Controls.uc_label();
      this.lbl_record = new WSI.Cashier.Controls.uc_label();
      this.grd_customers = new WSI.Cashier.Controls.uc_DataGridView();
      this.btn_view_list = new WSI.Cashier.Controls.uc_round_button();
      this.btn_entry = new WSI.Cashier.Controls.uc_round_button();
      this.m_img_photo = new WSI.Cashier.Controls.UcPhotobox();
      this.btn_id_card_scan = new WSI.Cashier.Controls.uc_round_button_long_press();
      this.btn_new_customer = new WSI.Cashier.Controls.uc_round_button();
      this.btn_ok = new WSI.Cashier.Controls.uc_round_button();
      this.tab_player_edit = new WSI.Cashier.Controls.uc_round_tab_control();
      this.principal = new System.Windows.Forms.TabPage();
      this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
      this.btn_update_record_expiration = new WSI.Cashier.Controls.uc_round_button();
      this.btn_card_functions = new WSI.Cashier.Controls.uc_round_button();
      this.tlp_name = new System.Windows.Forms.TableLayoutPanel();
      this.txt_name4 = new WSI.Cashier.CharacterTextBox();
      this.lbl_name4 = new WSI.Cashier.Controls.uc_label();
      this.txt_name = new WSI.Cashier.CharacterTextBox();
      this.lbl_last_name1 = new WSI.Cashier.Controls.uc_label();
      this.txt_last_name1 = new WSI.Cashier.CharacterTextBox();
      this.txt_last_name2 = new WSI.Cashier.CharacterTextBox();
      this.lbl_last_name2 = new WSI.Cashier.Controls.uc_label();
      this.lbl_name = new WSI.Cashier.Controls.uc_label();
      this.tlp_principal = new System.Windows.Forms.TableLayoutPanel();
      this.lbl_birth_date = new WSI.Cashier.Controls.uc_label();
      this.lbl_document = new WSI.Cashier.Controls.uc_label();
      this.cb_document_type = new WSI.Cashier.Controls.uc_round_auto_combobox();
      this.txt_document = new WSI.Cashier.CharacterTextBox();
      this.uc_dt_birth = new WSI.Cashier.uc_datetime();
      this.lbl_track_number = new WSI.Cashier.Controls.uc_label();
      this.lbl_track = new WSI.Cashier.Controls.uc_label();
      this.pb_record_expiration_status = new System.Windows.Forms.PictureBox();
      this.pb_blacklist_status = new System.Windows.Forms.PictureBox();
      this.lbl_record_expiration_date = new WSI.Cashier.Controls.uc_label();
      this.lbl_text_record_expiration_date = new WSI.Cashier.Controls.uc_label();
      this.tbl_type_doc = new System.Windows.Forms.TableLayoutPanel();
      this.lbl_type_doc_count = new WSI.Cashier.Controls.uc_label();
      this.lbl_type_document = new WSI.Cashier.Controls.uc_label();
      this.lbl_age = new WSI.Cashier.Controls.uc_label();
      this.tlp_principal1 = new System.Windows.Forms.TableLayoutPanel();
      this.cb_birth_country = new WSI.Cashier.Controls.uc_round_auto_combobox();
      this.lbl_birth_country = new WSI.Cashier.Controls.uc_label();
      this.cb_gender = new WSI.Cashier.Controls.uc_round_auto_combobox();
      this.lbl_gender = new WSI.Cashier.Controls.uc_label();
      this.cb_nationality = new WSI.Cashier.Controls.uc_round_auto_combobox();
      this.lbl_nationality = new WSI.Cashier.Controls.uc_label();
      this.cb_marital = new WSI.Cashier.Controls.uc_round_auto_combobox();
      this.lbl_marital_status = new WSI.Cashier.Controls.uc_label();
      this.lbl_full_name = new WSI.Cashier.Controls.uc_label();
      this.contact = new System.Windows.Forms.TabPage();
      this.tlp_contact = new System.Windows.Forms.TableLayoutPanel();
      this.lbl_phone_01 = new WSI.Cashier.Controls.uc_label();
      this.txt_phone_01 = new WSI.Cashier.PhoneNumberTextBox();
      this.lbl_phone_02 = new WSI.Cashier.Controls.uc_label();
      this.txt_phone_02 = new WSI.Cashier.PhoneNumberTextBox();
      this.txt_email_01 = new WSI.Cashier.Controls.uc_round_textbox();
      this.lbl_email_01 = new WSI.Cashier.Controls.uc_label();
      this.txt_twitter = new WSI.Cashier.Controls.uc_round_textbox();
      this.txt_facebook = new WSI.Cashier.Controls.uc_round_textbox();
      this.lbl_twitter = new WSI.Cashier.Controls.uc_label();
      this.lbl_facebook = new WSI.Cashier.Controls.uc_label();
      this.chk_data_agreement = new WSI.Cashier.Controls.uc_checkBox();
      this.address = new System.Windows.Forms.TabPage();
      this.btn_copy_adddress = new WSI.Cashier.Controls.uc_round_button();
      this.tab_address = new WSI.Cashier.Controls.uc_round_tab_control();
      this.home_address = new System.Windows.Forms.TabPage();
      this.HolderAddress = new WSI.Cashier.uc_address();
      this.alt_address = new System.Windows.Forms.TabPage();
      this.AlternativeAddress = new WSI.Cashier.uc_address();
      this.comments = new System.Windows.Forms.TabPage();
      this.chk_show_comments = new WSI.Cashier.Controls.uc_checkBox();
      this.txt_comments = new WSI.Cashier.Controls.uc_round_textbox();
      this.scanned_docs = new System.Windows.Forms.TabPage();
      this.pnl_container = new System.Windows.Forms.Panel();
      this.pnl_grd_docs = new System.Windows.Forms.Panel();
      this.grd_scanned_docs = new WSI.Cashier.Controls.uc_DataGridView();
      this.pnl_photo = new System.Windows.Forms.Panel();
      this.btn_scan_doc = new WSI.Cashier.Controls.uc_round_button();
      this.pb_scanned_photo = new System.Windows.Forms.PictureBox();
      this.pin = new System.Windows.Forms.TabPage();
      this.lbl_pin_msg = new WSI.Cashier.Controls.uc_label();
      this.txt_confirm_pin = new WSI.Cashier.NumericTextBox();
      this.txt_pin = new WSI.Cashier.NumericTextBox();
      this.btn_generate_random_pin = new WSI.Cashier.Controls.uc_round_button();
      this.btn_save_pin = new WSI.Cashier.Controls.uc_round_button();
      this.lbl_confirm_pin = new WSI.Cashier.Controls.uc_label();
      this.lbl_pin = new WSI.Cashier.Controls.uc_label();
      this.blacklist = new System.Windows.Forms.TabPage();
      this.chk_acknowledged_blacklist = new WSI.Cashier.Controls.uc_checkBox();
      this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
      this.btn_add_to_blacklist = new WSI.Cashier.Controls.uc_round_button();
      this.btn_remove_from_blacklist = new WSI.Cashier.Controls.uc_round_button();
      this.btn_show_matches = new WSI.Cashier.Controls.uc_round_button();
      this.lbl_blacklist_data = new WSI.Cashier.Controls.uc_label();
      this.pb_blacklist_data = new System.Windows.Forms.PictureBox();
      this.lbl_blacklist_messages = new WSI.Cashier.Controls.uc_round_textbox();
      this.lbl_expiration_date = new WSI.Cashier.Controls.uc_label();
      this.lbl_vip_account = new WSI.Cashier.Controls.uc_label();
      this.lbl_happy_birthday = new WSI.Cashier.Controls.uc_label();
      this.lbl_level_data = new WSI.Cashier.Controls.uc_label();
      this.lbl_level = new WSI.Cashier.Controls.uc_label();
      this.btn_keyboard = new WSI.Cashier.Controls.uc_round_button();
      this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
      this.btn_exit = new WSI.Cashier.Controls.uc_round_button();
      this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
      this.btn_cancel = new WSI.Cashier.Controls.uc_round_button();
      this.pictureBox1 = new System.Windows.Forms.PictureBox();
      this.pnl_results = new System.Windows.Forms.Panel();
      this.pnl_data.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.grd_customers)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.m_img_photo)).BeginInit();
      this.tab_player_edit.SuspendLayout();
      this.principal.SuspendLayout();
      this.tableLayoutPanel3.SuspendLayout();
      this.tlp_name.SuspendLayout();
      this.tlp_principal.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pb_record_expiration_status)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.pb_blacklist_status)).BeginInit();
      this.tbl_type_doc.SuspendLayout();
      this.tlp_principal1.SuspendLayout();
      this.contact.SuspendLayout();
      this.tlp_contact.SuspendLayout();
      this.address.SuspendLayout();
      this.tab_address.SuspendLayout();
      this.home_address.SuspendLayout();
      this.alt_address.SuspendLayout();
      this.comments.SuspendLayout();
      this.scanned_docs.SuspendLayout();
      this.pnl_container.SuspendLayout();
      this.pnl_grd_docs.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.grd_scanned_docs)).BeginInit();
      this.pnl_photo.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pb_scanned_photo)).BeginInit();
      this.pin.SuspendLayout();
      this.blacklist.SuspendLayout();
      this.flowLayoutPanel1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pb_blacklist_data)).BeginInit();
      this.tableLayoutPanel1.SuspendLayout();
      this.tableLayoutPanel2.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
      this.pnl_results.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.Controls.Add(this.pnl_results);
      this.pnl_data.Controls.Add(this.tableLayoutPanel2);
      this.pnl_data.Controls.Add(this.tableLayoutPanel1);
      this.pnl_data.Controls.Add(this.tab_player_edit);
      this.pnl_data.Controls.Add(this.lbl_msg_blink);
      this.pnl_data.Controls.Add(this.lbl_msg_count_blink);
      this.pnl_data.Controls.Add(this.pictureBox1);
      this.pnl_data.Size = new System.Drawing.Size(1024, 658);
      // 
      // lbl_account_edit_title
      // 
      this.lbl_account_edit_title.AutoSize = true;
      this.lbl_account_edit_title.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_account_edit_title.ForeColor = System.Drawing.Color.White;
      this.lbl_account_edit_title.Location = new System.Drawing.Point(2, 4);
      this.lbl_account_edit_title.Name = "lbl_account_edit_title";
      this.lbl_account_edit_title.Size = new System.Drawing.Size(183, 16);
      this.lbl_account_edit_title.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.lbl_account_edit_title.TabIndex = 0;
      this.lbl_account_edit_title.Text = "xAccount Data Edition (C)";
      // 
      // lbl_msg_count_blink
      // 
      this.lbl_msg_count_blink.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.lbl_msg_count_blink.BackColor = System.Drawing.Color.Transparent;
      this.lbl_msg_count_blink.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_msg_count_blink.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.lbl_msg_count_blink.Location = new System.Drawing.Point(137, 419);
      this.lbl_msg_count_blink.Name = "lbl_msg_count_blink";
      this.lbl_msg_count_blink.Size = new System.Drawing.Size(82, 23);
      this.lbl_msg_count_blink.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_RED;
      this.lbl_msg_count_blink.TabIndex = 41;
      this.lbl_msg_count_blink.Text = "xPERSONAL NAME CANNOT BE BLANK";
      this.lbl_msg_count_blink.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.lbl_msg_count_blink.UseMnemonic = false;
      this.lbl_msg_count_blink.Visible = false;
      // 
      // lbl_msg_blink
      // 
      this.lbl_msg_blink.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.lbl_msg_blink.BackColor = System.Drawing.Color.Transparent;
      this.lbl_msg_blink.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_msg_blink.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.lbl_msg_blink.Location = new System.Drawing.Point(219, 420);
      this.lbl_msg_blink.Name = "lbl_msg_blink";
      this.lbl_msg_blink.Size = new System.Drawing.Size(798, 23);
      this.lbl_msg_blink.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_RED;
      this.lbl_msg_blink.TabIndex = 40;
      this.lbl_msg_blink.Text = "xPERSONAL NAME CANNOT BE BLANK";
      this.lbl_msg_blink.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.lbl_msg_blink.UseMnemonic = false;
      this.lbl_msg_blink.Visible = false;
      // 
      // btn_image_black_list
      // 
      this.btn_image_black_list.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_image_black_list.FlatAppearance.BorderSize = 0;
      this.btn_image_black_list.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_image_black_list.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_image_black_list.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_image_black_list.Image = global::WSI.Cashier.Properties.Resources.Input_hide;
      this.btn_image_black_list.IsSelected = false;
      this.btn_image_black_list.Location = new System.Drawing.Point(178, 204);
      this.btn_image_black_list.Name = "btn_image_black_list";
      this.btn_image_black_list.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_image_black_list.Size = new System.Drawing.Size(32, 32);
      this.btn_image_black_list.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_image_black_list.TabIndex = 51;
      this.btn_image_black_list.TabStop = false;
      this.btn_image_black_list.UseVisualStyleBackColor = false;
      this.btn_image_black_list.Click += new System.EventHandler(this.btn_image_black_list_Click);
      // 
      // lbl_black_list
      // 
      this.lbl_black_list.AutoSize = true;
      this.lbl_black_list.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_black_list.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_black_list.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_black_list.Location = new System.Drawing.Point(3, 201);
      this.lbl_black_list.Name = "lbl_black_list";
      this.lbl_black_list.Size = new System.Drawing.Size(131, 38);
      this.lbl_black_list.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_black_list.TabIndex = 47;
      this.lbl_black_list.Text = "xListasNegras";
      this.lbl_black_list.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_record
      // 
      this.lbl_record.AutoSize = true;
      this.lbl_record.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_record.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_record.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_record.Location = new System.Drawing.Point(3, 163);
      this.lbl_record.Name = "lbl_record";
      this.lbl_record.Size = new System.Drawing.Size(131, 38);
      this.lbl_record.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_record.TabIndex = 46;
      this.lbl_record.Tag = "lRecordExpirationDate";
      this.lbl_record.Text = "xExpediente";
      this.lbl_record.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      this.lbl_record.Click += new System.EventHandler(this.lbl_record_Click);
      // 
      // grd_customers
      // 
      this.grd_customers.AllowUserToAddRows = false;
      this.grd_customers.AllowUserToDeleteRows = false;
      this.grd_customers.AllowUserToResizeColumns = false;
      this.grd_customers.AllowUserToResizeRows = false;
      this.grd_customers.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.grd_customers.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.grd_customers.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.grd_customers.ColumnHeaderHeight = 35;
      this.grd_customers.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
      dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      dataGridViewCellStyle1.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      dataGridViewCellStyle1.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
      dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
      dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
      this.grd_customers.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
      this.grd_customers.ColumnHeadersHeight = 35;
      this.grd_customers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
      this.grd_customers.CornerRadius = 10;
      this.grd_customers.DefaultCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.grd_customers.DefaultCellForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.grd_customers.DefaultCellSelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.grd_customers.DefaultCellSelectionForeColor = System.Drawing.Color.White;
      dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      dataGridViewCellStyle2.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(211)))), ((int)(((byte)(216)))));
      dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
      this.grd_customers.DefaultCellStyle = dataGridViewCellStyle2;
      this.grd_customers.EnableHeadersVisualStyles = false;
      this.grd_customers.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.grd_customers.GridColor = System.Drawing.Color.LightGray;
      this.grd_customers.HeaderBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
      this.grd_customers.HeaderDefaultCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.grd_customers.HeaderDefaultCellForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.grd_customers.HeaderImages = null;
      this.grd_customers.Location = new System.Drawing.Point(1, 1);
      this.grd_customers.Name = "grd_customers";
      this.grd_customers.ReadOnly = true;
      dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle3.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
      dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
      dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
      dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
      this.grd_customers.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
      this.grd_customers.RowHeadersVisible = false;
      this.grd_customers.RowHeadersWidth = 20;
      this.grd_customers.RowTemplateHeight = 35;
      this.grd_customers.Size = new System.Drawing.Size(891, 207);
      this.grd_customers.Style = WSI.Cashier.Controls.uc_DataGridView.DataGridViewStyle.WITH_ALL_ROUND_CORNERS;
      this.grd_customers.TabIndex = 4;
      this.grd_customers.Tag = "GridCustomers";
      this.grd_customers.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grd_customers_CellClick);
      // 
      // btn_view_list
      // 
      this.btn_view_list.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(47)))), ((int)(((byte)(50)))));
      this.btn_view_list.FlatAppearance.BorderSize = 0;
      this.btn_view_list.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_view_list.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_view_list.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_view_list.Image = null;
      this.btn_view_list.IsSelected = false;
      this.btn_view_list.Location = new System.Drawing.Point(3, 3);
      this.btn_view_list.Name = "btn_view_list";
      this.btn_view_list.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_view_list.Size = new System.Drawing.Size(122, 59);
      this.btn_view_list.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.TERTIARY;
      this.btn_view_list.TabIndex = 0;
      this.btn_view_list.TabStop = false;
      this.btn_view_list.Text = "XMORE RESULTS";
      this.btn_view_list.UseVisualStyleBackColor = false;
      this.btn_view_list.Click += new System.EventHandler(this.btn_view_list_Click);
      // 
      // btn_entry
      // 
      this.btn_entry.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_entry.FlatAppearance.BorderSize = 0;
      this.btn_entry.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_entry.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_entry.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_entry.Image = null;
      this.btn_entry.IsSelected = false;
      this.btn_entry.Location = new System.Drawing.Point(3, 280);
      this.btn_entry.Name = "btn_entry";
      this.btn_entry.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_entry.Size = new System.Drawing.Size(106, 59);
      this.btn_entry.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_entry.TabIndex = 1;
      this.btn_entry.TabStop = false;
      this.btn_entry.Text = "XENTRY";
      this.btn_entry.UseVisualStyleBackColor = false;
      this.btn_entry.Click += new System.EventHandler(this.btn_entry_Click);
      // 
      // m_img_photo
      // 
      this.m_img_photo.BackgroundImage = global::WSI.Cashier.Properties.Resources.anonymous_user;
      this.m_img_photo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
      this.m_img_photo.Location = new System.Drawing.Point(3, 3);
      this.m_img_photo.Name = "m_img_photo";
      this.m_img_photo.Size = new System.Drawing.Size(106, 105);
      this.m_img_photo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
      this.m_img_photo.TabIndex = 39;
      this.m_img_photo.TabStop = false;
      this.m_img_photo.Tag = "HolderPhoto";
      this.m_img_photo.Click += new System.EventHandler(this.m_img_photo_Click);
      // 
      // btn_id_card_scan
      // 
      this.btn_id_card_scan.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_id_card_scan.FlatAppearance.BorderSize = 0;
      this.btn_id_card_scan.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_id_card_scan.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_id_card_scan.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_id_card_scan.Image = null;
      this.btn_id_card_scan.IsSelected = false;
      this.btn_id_card_scan.Location = new System.Drawing.Point(3, 3);
      this.btn_id_card_scan.Name = "btn_id_card_scan";
      this.btn_id_card_scan.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_id_card_scan.Size = new System.Drawing.Size(110, 59);
      this.btn_id_card_scan.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_id_card_scan.TabIndex = 0;
      this.btn_id_card_scan.Text = "ID CARD SCAN";
      this.btn_id_card_scan.UseVisualStyleBackColor = false;
      this.btn_id_card_scan.LongClick += new System.EventHandler(this.btn_id_card_scan_LongClick);
      this.btn_id_card_scan.Click += new System.EventHandler(this.btn_id_card_scan_Click);
      // 
      // btn_new_customer
      // 
      this.btn_new_customer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_new_customer.FlatAppearance.BorderSize = 0;
      this.btn_new_customer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_new_customer.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_new_customer.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_new_customer.Image = null;
      this.btn_new_customer.IsSelected = false;
      this.btn_new_customer.Location = new System.Drawing.Point(3, 68);
      this.btn_new_customer.Name = "btn_new_customer";
      this.btn_new_customer.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_new_customer.Size = new System.Drawing.Size(122, 59);
      this.btn_new_customer.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_new_customer.TabIndex = 1;
      this.btn_new_customer.TabStop = false;
      this.btn_new_customer.Text = "XNEW CUSTOMER";
      this.btn_new_customer.UseVisualStyleBackColor = false;
      this.btn_new_customer.Click += new System.EventHandler(this.btn_new_customer_Click);
      // 
      // btn_ok
      // 
      this.btn_ok.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_ok.Enabled = false;
      this.btn_ok.FlatAppearance.BorderSize = 0;
      this.btn_ok.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_ok.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_ok.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_ok.Image = null;
      this.btn_ok.IsSelected = false;
      this.btn_ok.Location = new System.Drawing.Point(3, 215);
      this.btn_ok.Name = "btn_ok";
      this.btn_ok.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_ok.Size = new System.Drawing.Size(106, 59);
      this.btn_ok.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_ok.TabIndex = 0;
      this.btn_ok.TabStop = false;
      this.btn_ok.Text = "XOK";
      this.btn_ok.UseVisualStyleBackColor = false;
      this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
      // 
      // tab_player_edit
      // 
      this.tab_player_edit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.tab_player_edit.Controls.Add(this.principal);
      this.tab_player_edit.Controls.Add(this.contact);
      this.tab_player_edit.Controls.Add(this.address);
      this.tab_player_edit.Controls.Add(this.comments);
      this.tab_player_edit.Controls.Add(this.scanned_docs);
      this.tab_player_edit.Controls.Add(this.pin);
      this.tab_player_edit.Controls.Add(this.blacklist);
      this.tab_player_edit.DisplayStyle = WSI.Cashier.Controls.TabStyle.Rounded;
      // 
      // 
      // 
      this.tab_player_edit.DisplayStyleProvider.BorderColor = System.Drawing.SystemColors.ControlDark;
      this.tab_player_edit.DisplayStyleProvider.BorderColorHot = System.Drawing.SystemColors.ControlDark;
      this.tab_player_edit.DisplayStyleProvider.BorderColorSelected = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(157)))), ((int)(((byte)(185)))));
      this.tab_player_edit.DisplayStyleProvider.FocusTrack = true;
      this.tab_player_edit.DisplayStyleProvider.HotTrack = true;
      this.tab_player_edit.DisplayStyleProvider.Opacity = 1F;
      this.tab_player_edit.DisplayStyleProvider.Overlap = 0;
      this.tab_player_edit.DisplayStyleProvider.Padding = new System.Drawing.Point(6, 3);
      this.tab_player_edit.DisplayStyleProvider.Radius = 10;
      this.tab_player_edit.DisplayStyleProvider.TabColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.tab_player_edit.DisplayStyleProvider.TabColorHeader = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.tab_player_edit.DisplayStyleProvider.TabColorSelected = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.tab_player_edit.DisplayStyleProvider.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.tab_player_edit.DisplayStyleProvider.TextColorSelected = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.tab_player_edit.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.tab_player_edit.HotTrack = true;
      this.tab_player_edit.ItemSize = new System.Drawing.Size(414, 40);
      this.tab_player_edit.Location = new System.Drawing.Point(124, 8);
      this.tab_player_edit.Margin = new System.Windows.Forms.Padding(0);
      this.tab_player_edit.Name = "tab_player_edit";
      this.tab_player_edit.SelectedIndex = 0;
      this.tab_player_edit.Size = new System.Drawing.Size(924, 414);
      this.tab_player_edit.TabIndex = 3;
      this.tab_player_edit.TabStop = false;
      this.tab_player_edit.Tag = "";
      this.tab_player_edit.Selected += new System.Windows.Forms.TabControlEventHandler(this.tab_player_edit_Selected);
      this.tab_player_edit.Click += new System.EventHandler(this.tab_player_edit_Click);
      // 
      // principal
      // 
      this.principal.Controls.Add(this.tableLayoutPanel3);
      this.principal.Controls.Add(this.tlp_name);
      this.principal.Controls.Add(this.tlp_principal);
      this.principal.Controls.Add(this.tlp_principal1);
      this.principal.Controls.Add(this.lbl_full_name);
      this.principal.Location = new System.Drawing.Point(4, 45);
      this.principal.Name = "principal";
      this.principal.Padding = new System.Windows.Forms.Padding(3);
      this.principal.Size = new System.Drawing.Size(916, 365);
      this.principal.TabIndex = 0;
      this.principal.Text = "xPrincipal";
      this.principal.UseVisualStyleBackColor = true;
      this.principal.Click += new System.EventHandler(this.principal_Click);
      // 
      // tableLayoutPanel3
      // 
      this.tableLayoutPanel3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.tableLayoutPanel3.AutoSize = true;
      this.tableLayoutPanel3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
      this.tableLayoutPanel3.ColumnCount = 3;
      this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
      this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
      this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
      this.tableLayoutPanel3.Controls.Add(this.btn_id_card_scan, 0, 0);
      this.tableLayoutPanel3.Controls.Add(this.btn_update_record_expiration, 1, 0);
      this.tableLayoutPanel3.Controls.Add(this.btn_card_functions, 2, 0);
      this.tableLayoutPanel3.Location = new System.Drawing.Point(524, 293);
      this.tableLayoutPanel3.Name = "tableLayoutPanel3";
      this.tableLayoutPanel3.RowCount = 1;
      this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tableLayoutPanel3.Size = new System.Drawing.Size(367, 65);
      this.tableLayoutPanel3.TabIndex = 1004;
      // 
      // btn_update_record_expiration
      // 
      this.btn_update_record_expiration.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_update_record_expiration.FlatAppearance.BorderSize = 0;
      this.btn_update_record_expiration.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_update_record_expiration.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_update_record_expiration.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_update_record_expiration.Image = null;
      this.btn_update_record_expiration.IsSelected = false;
      this.btn_update_record_expiration.Location = new System.Drawing.Point(119, 3);
      this.btn_update_record_expiration.Name = "btn_update_record_expiration";
      this.btn_update_record_expiration.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_update_record_expiration.Size = new System.Drawing.Size(129, 59);
      this.btn_update_record_expiration.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_update_record_expiration.TabIndex = 1;
      this.btn_update_record_expiration.Text = "XUPDATE RECORD EXPIRATION";
      this.btn_update_record_expiration.UseVisualStyleBackColor = false;
      this.btn_update_record_expiration.Click += new System.EventHandler(this.btn_update_record_expiration_Click);
      // 
      // btn_card_functions
      // 
      this.btn_card_functions.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_card_functions.FlatAppearance.BorderSize = 0;
      this.btn_card_functions.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_card_functions.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_card_functions.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_card_functions.Image = null;
      this.btn_card_functions.IsSelected = false;
      this.btn_card_functions.Location = new System.Drawing.Point(254, 3);
      this.btn_card_functions.Name = "btn_card_functions";
      this.btn_card_functions.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_card_functions.Size = new System.Drawing.Size(110, 59);
      this.btn_card_functions.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_card_functions.TabIndex = 2;
      this.btn_card_functions.Text = "XBUTTONCARDFUNCTIONS";
      this.btn_card_functions.UseVisualStyleBackColor = false;
      this.btn_card_functions.Click += new System.EventHandler(this.btn_card_functions_Click);
      // 
      // tlp_name
      // 
      this.tlp_name.ColumnCount = 4;
      this.tlp_name.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
      this.tlp_name.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
      this.tlp_name.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
      this.tlp_name.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
      this.tlp_name.Controls.Add(this.txt_name4, 1, 1);
      this.tlp_name.Controls.Add(this.lbl_name4, 1, 0);
      this.tlp_name.Controls.Add(this.txt_name, 0, 1);
      this.tlp_name.Controls.Add(this.lbl_last_name1, 2, 0);
      this.tlp_name.Controls.Add(this.txt_last_name1, 2, 1);
      this.tlp_name.Controls.Add(this.txt_last_name2, 3, 1);
      this.tlp_name.Controls.Add(this.lbl_last_name2, 3, 0);
      this.tlp_name.Controls.Add(this.lbl_name, 0, 0);
      this.tlp_name.Location = new System.Drawing.Point(3, 7);
      this.tlp_name.Name = "tlp_name";
      this.tlp_name.RowCount = 2;
      this.tlp_name.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_name.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_name.Size = new System.Drawing.Size(861, 65);
      this.tlp_name.TabIndex = 0;
      // 
      // txt_name4
      // 
      this.txt_name4.AllowSpace = true;
      this.txt_name4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
      this.txt_name4.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_name4.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_name4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_name4.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_name4.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.txt_name4.CornerRadius = 5;
      this.txt_name4.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_name4.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.BaseMode;
      this.txt_name4.Location = new System.Drawing.Point(218, 22);
      this.txt_name4.MaxLength = 50;
      this.txt_name4.Multiline = false;
      this.txt_name4.Name = "txt_name4";
      this.txt_name4.PasswordChar = '\0';
      this.txt_name4.ReadOnly = false;
      this.txt_name4.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_name4.SelectedText = "";
      this.txt_name4.SelectionLength = 0;
      this.txt_name4.SelectionStart = 0;
      this.txt_name4.Size = new System.Drawing.Size(209, 40);
      this.txt_name4.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_name4.TabIndex = 1;
      this.txt_name4.TabStop = false;
      this.txt_name4.Tag = "HolderName4";
      this.txt_name4.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_name4.UseSystemPasswordChar = false;
      this.txt_name4.WaterMark = null;
      this.txt_name4.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // lbl_name4
      // 
      this.lbl_name4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_name4.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_name4.Location = new System.Drawing.Point(218, 0);
      this.lbl_name4.Name = "lbl_name4";
      this.lbl_name4.Size = new System.Drawing.Size(209, 18);
      this.lbl_name4.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.lbl_name4.TabIndex = 1002;
      this.lbl_name4.Tag = "lHolderName4";
      this.lbl_name4.Text = "xHolderName4";
      this.lbl_name4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // txt_name
      // 
      this.txt_name.AllowSpace = true;
      this.txt_name.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
      this.txt_name.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_name.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_name.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_name.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_name.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.txt_name.CornerRadius = 5;
      this.txt_name.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_name.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.BaseMode;
      this.txt_name.Location = new System.Drawing.Point(3, 22);
      this.txt_name.MaxLength = 50;
      this.txt_name.Multiline = false;
      this.txt_name.Name = "txt_name";
      this.txt_name.PasswordChar = '\0';
      this.txt_name.ReadOnly = false;
      this.txt_name.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_name.SelectedText = "";
      this.txt_name.SelectionLength = 0;
      this.txt_name.SelectionStart = 0;
      this.txt_name.Size = new System.Drawing.Size(209, 40);
      this.txt_name.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_name.TabIndex = 0;
      this.txt_name.TabStop = false;
      this.txt_name.Tag = "HolderName3";
      this.txt_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_name.UseSystemPasswordChar = false;
      this.txt_name.WaterMark = null;
      this.txt_name.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      this.txt_name.TextChanged += new System.EventHandler(this.txt_name_TextChanged);
      // 
      // lbl_last_name1
      // 
      this.lbl_last_name1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_last_name1.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_last_name1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.lbl_last_name1.Location = new System.Drawing.Point(433, 0);
      this.lbl_last_name1.Name = "lbl_last_name1";
      this.lbl_last_name1.Size = new System.Drawing.Size(209, 19);
      this.lbl_last_name1.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.lbl_last_name1.TabIndex = 1001;
      this.lbl_last_name1.Tag = "lHolderName1";
      this.lbl_last_name1.Text = "xHolderName1";
      this.lbl_last_name1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // txt_last_name1
      // 
      this.txt_last_name1.AllowSpace = true;
      this.txt_last_name1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
      this.txt_last_name1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_last_name1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_last_name1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_last_name1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_last_name1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.txt_last_name1.CornerRadius = 5;
      this.txt_last_name1.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_last_name1.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.BaseMode;
      this.txt_last_name1.Location = new System.Drawing.Point(433, 22);
      this.txt_last_name1.MaxLength = 50;
      this.txt_last_name1.Multiline = false;
      this.txt_last_name1.Name = "txt_last_name1";
      this.txt_last_name1.PasswordChar = '\0';
      this.txt_last_name1.ReadOnly = false;
      this.txt_last_name1.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_last_name1.SelectedText = "";
      this.txt_last_name1.SelectionLength = 0;
      this.txt_last_name1.SelectionStart = 0;
      this.txt_last_name1.Size = new System.Drawing.Size(209, 40);
      this.txt_last_name1.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_last_name1.TabIndex = 2;
      this.txt_last_name1.TabStop = false;
      this.txt_last_name1.Tag = "HolderName1";
      this.txt_last_name1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_last_name1.UseSystemPasswordChar = false;
      this.txt_last_name1.WaterMark = null;
      this.txt_last_name1.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      this.txt_last_name1.Enter += new System.EventHandler(this.txt_name_enter);
      this.txt_last_name1.Leave += new System.EventHandler(this.txt_name_Leave);
      // 
      // txt_last_name2
      // 
      this.txt_last_name2.AllowSpace = true;
      this.txt_last_name2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
      this.txt_last_name2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_last_name2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_last_name2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_last_name2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_last_name2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.txt_last_name2.CornerRadius = 5;
      this.txt_last_name2.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_last_name2.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.BaseMode;
      this.txt_last_name2.Location = new System.Drawing.Point(648, 22);
      this.txt_last_name2.MaxLength = 50;
      this.txt_last_name2.Multiline = false;
      this.txt_last_name2.Name = "txt_last_name2";
      this.txt_last_name2.PasswordChar = '\0';
      this.txt_last_name2.ReadOnly = false;
      this.txt_last_name2.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_last_name2.SelectedText = "";
      this.txt_last_name2.SelectionLength = 0;
      this.txt_last_name2.SelectionStart = 0;
      this.txt_last_name2.Size = new System.Drawing.Size(210, 40);
      this.txt_last_name2.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_last_name2.TabIndex = 3;
      this.txt_last_name2.TabStop = false;
      this.txt_last_name2.Tag = "HolderName2";
      this.txt_last_name2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_last_name2.UseSystemPasswordChar = false;
      this.txt_last_name2.WaterMark = null;
      this.txt_last_name2.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // lbl_last_name2
      // 
      this.lbl_last_name2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_last_name2.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_last_name2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.lbl_last_name2.Location = new System.Drawing.Point(648, 0);
      this.lbl_last_name2.Name = "lbl_last_name2";
      this.lbl_last_name2.Size = new System.Drawing.Size(210, 19);
      this.lbl_last_name2.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.lbl_last_name2.TabIndex = 1001;
      this.lbl_last_name2.Tag = "lHolderName2";
      this.lbl_last_name2.Text = "xHolderName4";
      this.lbl_last_name2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_name
      // 
      this.lbl_name.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_name.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_name.Location = new System.Drawing.Point(3, 0);
      this.lbl_name.Name = "lbl_name";
      this.lbl_name.Size = new System.Drawing.Size(209, 18);
      this.lbl_name.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.lbl_name.TabIndex = 1001;
      this.lbl_name.Tag = "lHolderName3";
      this.lbl_name.Text = "xHolderName3";
      this.lbl_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // tlp_principal
      // 
      this.tlp_principal.ColumnCount = 5;
      this.tlp_principal.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 137F));
      this.tlp_principal.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 38F));
      this.tlp_principal.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 106F));
      this.tlp_principal.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 58F));
      this.tlp_principal.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 108F));
      this.tlp_principal.Controls.Add(this.lbl_birth_date, 0, 5);
      this.tlp_principal.Controls.Add(this.lbl_document, 0, 2);
      this.tlp_principal.Controls.Add(this.cb_document_type, 1, 1);
      this.tlp_principal.Controls.Add(this.txt_document, 1, 2);
      this.tlp_principal.Controls.Add(this.uc_dt_birth, 1, 5);
      this.tlp_principal.Controls.Add(this.lbl_track_number, 1, 0);
      this.tlp_principal.Controls.Add(this.lbl_track, 0, 0);
      this.tlp_principal.Controls.Add(this.lbl_record, 0, 7);
      this.tlp_principal.Controls.Add(this.lbl_black_list, 0, 8);
      this.tlp_principal.Controls.Add(this.pb_record_expiration_status, 1, 7);
      this.tlp_principal.Controls.Add(this.pb_blacklist_status, 1, 8);
      this.tlp_principal.Controls.Add(this.lbl_record_expiration_date, 2, 7);
      this.tlp_principal.Controls.Add(this.lbl_text_record_expiration_date, 3, 7);
      this.tlp_principal.Controls.Add(this.btn_image_black_list, 2, 8);
      this.tlp_principal.Controls.Add(this.tbl_type_doc, 0, 1);
      this.tlp_principal.Controls.Add(this.lbl_age, 4, 5);
      this.tlp_principal.Location = new System.Drawing.Point(3, 101);
      this.tlp_principal.Name = "tlp_principal";
      this.tlp_principal.RowCount = 11;
      this.tlp_principal.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
      this.tlp_principal.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_principal.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_principal.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_principal.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_principal.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_principal.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_principal.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
      this.tlp_principal.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
      this.tlp_principal.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
      this.tlp_principal.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
      this.tlp_principal.Size = new System.Drawing.Size(447, 272);
      this.tlp_principal.TabIndex = 1;
      // 
      // lbl_birth_date
      // 
      this.lbl_birth_date.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_birth_date.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_birth_date.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_birth_date.Location = new System.Drawing.Point(3, 112);
      this.lbl_birth_date.Name = "lbl_birth_date";
      this.lbl_birth_date.Size = new System.Drawing.Size(131, 51);
      this.lbl_birth_date.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_birth_date.TabIndex = 1001;
      this.lbl_birth_date.Tag = "lHolderBirthDate";
      this.lbl_birth_date.Text = "xBirthDay";
      this.lbl_birth_date.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_document
      // 
      this.lbl_document.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_document.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_document.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_document.Location = new System.Drawing.Point(3, 66);
      this.lbl_document.Name = "lbl_document";
      this.lbl_document.Size = new System.Drawing.Size(131, 46);
      this.lbl_document.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_document.TabIndex = 1001;
      this.lbl_document.Tag = "lHolderDocument";
      this.lbl_document.Text = "xDocument";
      this.lbl_document.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // cb_document_type
      // 
      this.cb_document_type.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_document_type.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.cb_document_type.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.cb_document_type.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.tlp_principal.SetColumnSpan(this.cb_document_type, 4);
      this.cb_document_type.CornerRadius = 5;
      this.cb_document_type.DataSource = null;
      this.cb_document_type.DisplayMember = "";
      this.cb_document_type.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
      this.cb_document_type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cb_document_type.DropDownWidth = 258;
      this.cb_document_type.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.cb_document_type.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_document_type.FormattingEnabled = true;
      this.cb_document_type.FormatValidator = null;
      this.cb_document_type.IsDroppedDown = false;
      this.cb_document_type.ItemHeight = 40;
      this.cb_document_type.Location = new System.Drawing.Point(140, 23);
      this.cb_document_type.MaxDropDownItems = 8;
      this.cb_document_type.Name = "cb_document_type";
      this.cb_document_type.OnFocusOpenListBox = true;
      this.cb_document_type.SelectedIndex = -1;
      this.cb_document_type.SelectedItem = null;
      this.cb_document_type.SelectedValue = null;
      this.cb_document_type.SelectionLength = 0;
      this.cb_document_type.SelectionStart = 0;
      this.cb_document_type.Size = new System.Drawing.Size(258, 40);
      this.cb_document_type.Sorted = false;
      this.cb_document_type.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.BASIC;
      this.cb_document_type.TabIndex = 0;
      this.cb_document_type.TabStop = false;
      this.cb_document_type.Tag = "HolderDocumentType";
      this.cb_document_type.ValueMember = "";
      this.cb_document_type.SelectedIndexChanged += new System.EventHandler(this.cb_document_type_SelectedIndexChanged);
      this.cb_document_type.Leave += new System.EventHandler(this.txt_document_Leave);
      // 
      // txt_document
      // 
      this.txt_document.AllowSpace = true;
      this.txt_document.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_document.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_document.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_document.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_document.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.tlp_principal.SetColumnSpan(this.txt_document, 4);
      this.txt_document.CornerRadius = 5;
      this.txt_document.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_document.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.ExtendedMode;
      this.txt_document.Location = new System.Drawing.Point(140, 69);
      this.txt_document.MaxLength = 20;
      this.txt_document.Multiline = false;
      this.txt_document.Name = "txt_document";
      this.txt_document.PasswordChar = '\0';
      this.txt_document.ReadOnly = false;
      this.txt_document.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_document.SelectedText = "";
      this.txt_document.SelectionLength = 0;
      this.txt_document.SelectionStart = 0;
      this.txt_document.Size = new System.Drawing.Size(258, 40);
      this.txt_document.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_document.TabIndex = 1;
      this.txt_document.TabStop = false;
      this.txt_document.Tag = "HolderDocument";
      this.txt_document.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_document.UseSystemPasswordChar = false;
      this.txt_document.WaterMark = null;
      this.txt_document.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      this.txt_document.Enter += new System.EventHandler(this.txt_name_enter);
      this.txt_document.Leave += new System.EventHandler(this.txt_doc_leave);
      // 
      // uc_dt_birth
      // 
      this.uc_dt_birth.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
      this.uc_dt_birth.BackColor = System.Drawing.Color.Transparent;
      this.tlp_principal.SetColumnSpan(this.uc_dt_birth, 3);
      this.uc_dt_birth.DateText = "";
      this.uc_dt_birth.DateTextFont = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.uc_dt_birth.DateTextWidth = 0;
      this.uc_dt_birth.DateValue = new System.DateTime(((long)(0)));
      this.uc_dt_birth.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.uc_dt_birth.FormatFont = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.uc_dt_birth.FormatWidth = 13;
      this.uc_dt_birth.Invalid = false;
      this.uc_dt_birth.Location = new System.Drawing.Point(140, 115);
      this.uc_dt_birth.Name = "uc_dt_birth";
      this.uc_dt_birth.Size = new System.Drawing.Size(196, 45);
      this.uc_dt_birth.TabIndex = 2;
      this.uc_dt_birth.Tag = "HolderBirthDate";
      this.uc_dt_birth.ValuesFont = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.uc_dt_birth.Enter += new System.EventHandler(this.txt_name_enter);
      this.uc_dt_birth.Leave += new System.EventHandler(this.txt_name_Leave);
      // 
      // lbl_track_number
      // 
      this.lbl_track_number.BackColor = System.Drawing.Color.Transparent;
      this.tlp_principal.SetColumnSpan(this.lbl_track_number, 3);
      this.lbl_track_number.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_track_number.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_track_number.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_track_number.Location = new System.Drawing.Point(140, 0);
      this.lbl_track_number.Name = "lbl_track_number";
      this.lbl_track_number.Size = new System.Drawing.Size(196, 20);
      this.lbl_track_number.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_track_number.TabIndex = 1012;
      this.lbl_track_number.Text = "88888888888888888812";
      this.lbl_track_number.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_track
      // 
      this.lbl_track.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_track.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_track.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_track.Location = new System.Drawing.Point(3, 0);
      this.lbl_track.Name = "lbl_track";
      this.lbl_track.Size = new System.Drawing.Size(131, 20);
      this.lbl_track.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_track.TabIndex = 1013;
      this.lbl_track.Tag = "lDocumentTpye";
      this.lbl_track.Text = "xDocumentType";
      this.lbl_track.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // pb_record_expiration_status
      // 
      this.pb_record_expiration_status.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pb_record_expiration_status.Location = new System.Drawing.Point(140, 166);
      this.pb_record_expiration_status.Name = "pb_record_expiration_status";
      this.pb_record_expiration_status.Size = new System.Drawing.Size(32, 32);
      this.pb_record_expiration_status.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
      this.pb_record_expiration_status.TabIndex = 1015;
      this.pb_record_expiration_status.TabStop = false;
      this.pb_record_expiration_status.Tag = "RecordExpirationDate";
      // 
      // pb_blacklist_status
      // 
      this.pb_blacklist_status.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pb_blacklist_status.Location = new System.Drawing.Point(140, 204);
      this.pb_blacklist_status.Name = "pb_blacklist_status";
      this.pb_blacklist_status.Size = new System.Drawing.Size(32, 32);
      this.pb_blacklist_status.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
      this.pb_blacklist_status.TabIndex = 1016;
      this.pb_blacklist_status.TabStop = false;
      // 
      // lbl_record_expiration_date
      // 
      this.lbl_record_expiration_date.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_record_expiration_date.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_record_expiration_date.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_record_expiration_date.Location = new System.Drawing.Point(178, 163);
      this.lbl_record_expiration_date.Name = "lbl_record_expiration_date";
      this.lbl_record_expiration_date.Size = new System.Drawing.Size(100, 38);
      this.lbl_record_expiration_date.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_record_expiration_date.TabIndex = 1014;
      this.lbl_record_expiration_date.Tag = "lRecordExpirationDate";
      this.lbl_record_expiration_date.Text = "xRecordExpirationDate";
      this.lbl_record_expiration_date.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_text_record_expiration_date
      // 
      this.tlp_principal.SetColumnSpan(this.lbl_text_record_expiration_date, 2);
      this.lbl_text_record_expiration_date.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_text_record_expiration_date.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_text_record_expiration_date.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_text_record_expiration_date.Location = new System.Drawing.Point(284, 163);
      this.lbl_text_record_expiration_date.Name = "lbl_text_record_expiration_date";
      this.lbl_text_record_expiration_date.Size = new System.Drawing.Size(160, 38);
      this.lbl_text_record_expiration_date.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_text_record_expiration_date.TabIndex = 1014;
      this.lbl_text_record_expiration_date.Tag = "lRecordExpirationDate";
      this.lbl_text_record_expiration_date.Text = "xRecordExpirationDate";
      this.lbl_text_record_expiration_date.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // tbl_type_doc
      // 
      this.tbl_type_doc.ColumnCount = 1;
      this.tbl_type_doc.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.tbl_type_doc.Controls.Add(this.lbl_type_doc_count, 0, 1);
      this.tbl_type_doc.Controls.Add(this.lbl_type_document, 0, 0);
      this.tbl_type_doc.Location = new System.Drawing.Point(3, 23);
      this.tbl_type_doc.Name = "tbl_type_doc";
      this.tbl_type_doc.RowCount = 2;
      this.tbl_type_doc.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.tbl_type_doc.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 18F));
      this.tbl_type_doc.Size = new System.Drawing.Size(131, 40);
      this.tbl_type_doc.TabIndex = 1017;
      // 
      // lbl_type_doc_count
      // 
      this.lbl_type_doc_count.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_type_doc_count.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_type_doc_count.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_type_doc_count.Location = new System.Drawing.Point(3, 22);
      this.lbl_type_doc_count.Name = "lbl_type_doc_count";
      this.lbl_type_doc_count.Size = new System.Drawing.Size(125, 18);
      this.lbl_type_doc_count.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_type_doc_count.TabIndex = 1003;
      this.lbl_type_doc_count.Tag = "lHolderDocumentType";
      this.lbl_type_doc_count.Text = "xN";
      this.lbl_type_doc_count.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_type_document
      // 
      this.lbl_type_document.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_type_document.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_type_document.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_type_document.Location = new System.Drawing.Point(3, 0);
      this.lbl_type_document.Name = "lbl_type_document";
      this.lbl_type_document.Size = new System.Drawing.Size(125, 22);
      this.lbl_type_document.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_type_document.TabIndex = 1002;
      this.lbl_type_document.Tag = "lHolderDocumentType";
      this.lbl_type_document.Text = "xDocumentType";
      this.lbl_type_document.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_age
      // 
      this.lbl_age.AutoSize = true;
      this.lbl_age.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_age.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_age.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_age.Location = new System.Drawing.Point(342, 112);
      this.lbl_age.Name = "lbl_age";
      this.lbl_age.Size = new System.Drawing.Size(102, 51);
      this.lbl_age.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_age.TabIndex = 1018;
      this.lbl_age.Text = "uc_label1";
      this.lbl_age.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // tlp_principal1
      // 
      this.tlp_principal1.ColumnCount = 2;
      this.tlp_principal1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
      this.tlp_principal1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 300F));
      this.tlp_principal1.Controls.Add(this.cb_birth_country, 1, 1);
      this.tlp_principal1.Controls.Add(this.lbl_birth_country, 0, 1);
      this.tlp_principal1.Controls.Add(this.cb_gender, 1, 0);
      this.tlp_principal1.Controls.Add(this.lbl_gender, 0, 0);
      this.tlp_principal1.Controls.Add(this.cb_nationality, 1, 2);
      this.tlp_principal1.Controls.Add(this.lbl_nationality, 0, 2);
      this.tlp_principal1.Controls.Add(this.cb_marital, 1, 3);
      this.tlp_principal1.Controls.Add(this.lbl_marital_status, 0, 3);
      this.tlp_principal1.Location = new System.Drawing.Point(456, 95);
      this.tlp_principal1.Name = "tlp_principal1";
      this.tlp_principal1.RowCount = 7;
      this.tlp_principal1.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_principal1.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_principal1.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_principal1.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_principal1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
      this.tlp_principal1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
      this.tlp_principal1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
      this.tlp_principal1.Size = new System.Drawing.Size(408, 195);
      this.tlp_principal1.TabIndex = 2;
      this.tlp_principal1.Paint += new System.Windows.Forms.PaintEventHandler(this.tlp_principal1_Paint);
      // 
      // cb_birth_country
      // 
      this.cb_birth_country.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_birth_country.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.cb_birth_country.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.cb_birth_country.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.cb_birth_country.CornerRadius = 5;
      this.cb_birth_country.DataSource = null;
      this.cb_birth_country.DisplayMember = "";
      this.cb_birth_country.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
      this.cb_birth_country.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
      this.cb_birth_country.DropDownWidth = 252;
      this.cb_birth_country.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.cb_birth_country.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_birth_country.FormattingEnabled = true;
      this.cb_birth_country.FormatValidator = null;
      this.cb_birth_country.IsDroppedDown = false;
      this.cb_birth_country.ItemHeight = 40;
      this.cb_birth_country.Location = new System.Drawing.Point(153, 49);
      this.cb_birth_country.MaxDropDownItems = 8;
      this.cb_birth_country.Name = "cb_birth_country";
      this.cb_birth_country.OnFocusOpenListBox = true;
      this.cb_birth_country.SelectedIndex = -1;
      this.cb_birth_country.SelectedItem = null;
      this.cb_birth_country.SelectedValue = null;
      this.cb_birth_country.SelectionLength = 0;
      this.cb_birth_country.SelectionStart = 0;
      this.cb_birth_country.Size = new System.Drawing.Size(252, 40);
      this.cb_birth_country.Sorted = false;
      this.cb_birth_country.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.BASIC;
      this.cb_birth_country.TabIndex = 1;
      this.cb_birth_country.TabStop = false;
      this.cb_birth_country.Tag = "HolderBirthCountry";
      this.cb_birth_country.ValueMember = "";
      this.cb_birth_country.Enter += new System.EventHandler(this.txt_name_enter);
      this.cb_birth_country.Leave += new System.EventHandler(this.txt_name_Leave);
      // 
      // lbl_birth_country
      // 
      this.lbl_birth_country.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_birth_country.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_birth_country.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_birth_country.Location = new System.Drawing.Point(3, 46);
      this.lbl_birth_country.Name = "lbl_birth_country";
      this.lbl_birth_country.Size = new System.Drawing.Size(144, 46);
      this.lbl_birth_country.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_birth_country.TabIndex = 1004;
      this.lbl_birth_country.Tag = "lHolderBirthCountry";
      this.lbl_birth_country.Text = "xBirth Country";
      this.lbl_birth_country.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // cb_gender
      // 
      this.cb_gender.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_gender.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.cb_gender.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.cb_gender.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.cb_gender.CornerRadius = 5;
      this.cb_gender.DataSource = null;
      this.cb_gender.DisplayMember = "";
      this.cb_gender.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
      this.cb_gender.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cb_gender.DropDownWidth = 252;
      this.cb_gender.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.cb_gender.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_gender.FormattingEnabled = true;
      this.cb_gender.FormatValidator = null;
      this.cb_gender.IsDroppedDown = false;
      this.cb_gender.ItemHeight = 40;
      this.cb_gender.Location = new System.Drawing.Point(153, 3);
      this.cb_gender.MaxDropDownItems = 8;
      this.cb_gender.Name = "cb_gender";
      this.cb_gender.OnFocusOpenListBox = true;
      this.cb_gender.SelectedIndex = -1;
      this.cb_gender.SelectedItem = null;
      this.cb_gender.SelectedValue = null;
      this.cb_gender.SelectionLength = 0;
      this.cb_gender.SelectionStart = 0;
      this.cb_gender.Size = new System.Drawing.Size(252, 40);
      this.cb_gender.Sorted = false;
      this.cb_gender.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.BASIC;
      this.cb_gender.TabIndex = 0;
      this.cb_gender.TabStop = false;
      this.cb_gender.Tag = "HolderGender";
      this.cb_gender.ValueMember = "";
      this.cb_gender.Enter += new System.EventHandler(this.txt_name_enter);
      this.cb_gender.Leave += new System.EventHandler(this.txt_name_Leave);
      // 
      // lbl_gender
      // 
      this.lbl_gender.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_gender.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_gender.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_gender.Location = new System.Drawing.Point(3, 0);
      this.lbl_gender.Name = "lbl_gender";
      this.lbl_gender.Size = new System.Drawing.Size(144, 46);
      this.lbl_gender.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_gender.TabIndex = 1000;
      this.lbl_gender.Tag = "lHolderGender";
      this.lbl_gender.Text = "xGender";
      this.lbl_gender.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // cb_nationality
      // 
      this.cb_nationality.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_nationality.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.cb_nationality.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.cb_nationality.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.cb_nationality.CornerRadius = 5;
      this.cb_nationality.DataSource = null;
      this.cb_nationality.DisplayMember = "";
      this.cb_nationality.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
      this.cb_nationality.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
      this.cb_nationality.DropDownWidth = 252;
      this.cb_nationality.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.cb_nationality.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_nationality.FormattingEnabled = true;
      this.cb_nationality.FormatValidator = null;
      this.cb_nationality.IsDroppedDown = false;
      this.cb_nationality.ItemHeight = 40;
      this.cb_nationality.Location = new System.Drawing.Point(153, 95);
      this.cb_nationality.MaxDropDownItems = 8;
      this.cb_nationality.Name = "cb_nationality";
      this.cb_nationality.OnFocusOpenListBox = true;
      this.cb_nationality.SelectedIndex = -1;
      this.cb_nationality.SelectedItem = null;
      this.cb_nationality.SelectedValue = null;
      this.cb_nationality.SelectionLength = 0;
      this.cb_nationality.SelectionStart = 0;
      this.cb_nationality.Size = new System.Drawing.Size(252, 40);
      this.cb_nationality.Sorted = false;
      this.cb_nationality.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.BASIC;
      this.cb_nationality.TabIndex = 2;
      this.cb_nationality.TabStop = false;
      this.cb_nationality.Tag = "HolderNationality";
      this.cb_nationality.ValueMember = "";
      // 
      // lbl_nationality
      // 
      this.lbl_nationality.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_nationality.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_nationality.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_nationality.Location = new System.Drawing.Point(3, 92);
      this.lbl_nationality.Name = "lbl_nationality";
      this.lbl_nationality.Size = new System.Drawing.Size(144, 46);
      this.lbl_nationality.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_nationality.TabIndex = 1001;
      this.lbl_nationality.Tag = "lHolderNationality";
      this.lbl_nationality.Text = "xNationality";
      this.lbl_nationality.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // cb_marital
      // 
      this.cb_marital.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_marital.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.cb_marital.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.cb_marital.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.cb_marital.CornerRadius = 5;
      this.cb_marital.DataSource = null;
      this.cb_marital.DisplayMember = "";
      this.cb_marital.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
      this.cb_marital.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cb_marital.DropDownWidth = 252;
      this.cb_marital.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.cb_marital.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_marital.FormattingEnabled = true;
      this.cb_marital.FormatValidator = null;
      this.cb_marital.IsDroppedDown = false;
      this.cb_marital.ItemHeight = 40;
      this.cb_marital.Location = new System.Drawing.Point(153, 141);
      this.cb_marital.MaxDropDownItems = 8;
      this.cb_marital.Name = "cb_marital";
      this.cb_marital.OnFocusOpenListBox = true;
      this.cb_marital.SelectedIndex = -1;
      this.cb_marital.SelectedItem = null;
      this.cb_marital.SelectedValue = null;
      this.cb_marital.SelectionLength = 0;
      this.cb_marital.SelectionStart = 0;
      this.cb_marital.Size = new System.Drawing.Size(252, 40);
      this.cb_marital.Sorted = false;
      this.cb_marital.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.BASIC;
      this.cb_marital.TabIndex = 1006;
      this.cb_marital.TabStop = false;
      this.cb_marital.Tag = "HolderMaritalStatus";
      this.cb_marital.ValueMember = "";
      // 
      // lbl_marital_status
      // 
      this.lbl_marital_status.AutoSize = true;
      this.lbl_marital_status.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_marital_status.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_marital_status.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_marital_status.Location = new System.Drawing.Point(3, 138);
      this.lbl_marital_status.Name = "lbl_marital_status";
      this.lbl_marital_status.Size = new System.Drawing.Size(144, 46);
      this.lbl_marital_status.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_marital_status.TabIndex = 1007;
      this.lbl_marital_status.Tag = "lHolderMaritalStatus";
      this.lbl_marital_status.Text = "xMarital";
      this.lbl_marital_status.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_full_name
      // 
      this.lbl_full_name.Cursor = System.Windows.Forms.Cursors.Default;
      this.lbl_full_name.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_full_name.ForeColor = System.Drawing.SystemColors.HotTrack;
      this.lbl_full_name.Location = new System.Drawing.Point(6, 72);
      this.lbl_full_name.Name = "lbl_full_name";
      this.lbl_full_name.Size = new System.Drawing.Size(875, 20);
      this.lbl_full_name.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.lbl_full_name.TabIndex = 1001;
      this.lbl_full_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.lbl_full_name.UseMnemonic = false;
      // 
      // contact
      // 
      this.contact.Controls.Add(this.tlp_contact);
      this.contact.Location = new System.Drawing.Point(4, 45);
      this.contact.Name = "contact";
      this.contact.Size = new System.Drawing.Size(916, 365);
      this.contact.TabIndex = 2;
      this.contact.Text = "xContacto";
      this.contact.UseVisualStyleBackColor = true;
      // 
      // tlp_contact
      // 
      this.tlp_contact.ColumnCount = 2;
      this.tlp_contact.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
      this.tlp_contact.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.tlp_contact.Controls.Add(this.lbl_phone_01, 0, 0);
      this.tlp_contact.Controls.Add(this.txt_phone_01, 1, 0);
      this.tlp_contact.Controls.Add(this.lbl_phone_02, 0, 1);
      this.tlp_contact.Controls.Add(this.txt_phone_02, 1, 1);
      this.tlp_contact.Controls.Add(this.txt_email_01, 1, 2);
      this.tlp_contact.Controls.Add(this.lbl_email_01, 0, 2);
      this.tlp_contact.Controls.Add(this.txt_twitter, 1, 6);
      this.tlp_contact.Controls.Add(this.txt_facebook, 1, 7);
      this.tlp_contact.Controls.Add(this.lbl_twitter, 0, 6);
      this.tlp_contact.Controls.Add(this.lbl_facebook, 0, 7);
      this.tlp_contact.Controls.Add(this.chk_data_agreement, 1, 8);
      this.tlp_contact.Location = new System.Drawing.Point(6, 30);
      this.tlp_contact.Name = "tlp_contact";
      this.tlp_contact.RowCount = 9;
      this.tlp_contact.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_contact.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_contact.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_contact.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_contact.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_contact.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_contact.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 59F));
      this.tlp_contact.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 57F));
      this.tlp_contact.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8F));
      this.tlp_contact.Size = new System.Drawing.Size(866, 330);
      this.tlp_contact.TabIndex = 20;
      // 
      // lbl_phone_01
      // 
      this.lbl_phone_01.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_phone_01.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_phone_01.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_phone_01.Location = new System.Drawing.Point(3, 0);
      this.lbl_phone_01.Name = "lbl_phone_01";
      this.lbl_phone_01.Size = new System.Drawing.Size(144, 60);
      this.lbl_phone_01.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_phone_01.TabIndex = 15;
      this.lbl_phone_01.Tag = "lHolderPhone1";
      this.lbl_phone_01.Text = "xTel.Movil";
      this.lbl_phone_01.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // txt_phone_01
      // 
      this.txt_phone_01.AllowSpace = true;
      this.txt_phone_01.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.txt_phone_01.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_phone_01.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_phone_01.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_phone_01.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_phone_01.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_phone_01.CornerRadius = 5;
      this.txt_phone_01.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_phone_01.Location = new System.Drawing.Point(153, 10);
      this.txt_phone_01.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
      this.txt_phone_01.MaxLength = 20;
      this.txt_phone_01.Multiline = false;
      this.txt_phone_01.Name = "txt_phone_01";
      this.txt_phone_01.PasswordChar = '\0';
      this.txt_phone_01.ReadOnly = false;
      this.txt_phone_01.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_phone_01.SelectedText = "";
      this.txt_phone_01.SelectionLength = 0;
      this.txt_phone_01.SelectionStart = 0;
      this.txt_phone_01.Size = new System.Drawing.Size(710, 40);
      this.txt_phone_01.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_phone_01.TabIndex = 0;
      this.txt_phone_01.TabStop = false;
      this.txt_phone_01.Tag = "HolderPhone1";
      this.txt_phone_01.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_phone_01.UseSystemPasswordChar = false;
      this.txt_phone_01.WaterMark = null;
      this.txt_phone_01.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // lbl_phone_02
      // 
      this.lbl_phone_02.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_phone_02.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_phone_02.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_phone_02.Location = new System.Drawing.Point(3, 60);
      this.lbl_phone_02.Name = "lbl_phone_02";
      this.lbl_phone_02.Size = new System.Drawing.Size(144, 60);
      this.lbl_phone_02.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_phone_02.TabIndex = 14;
      this.lbl_phone_02.Tag = "lHolderPhone2";
      this.lbl_phone_02.Text = "xTel.Fijo";
      this.lbl_phone_02.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // txt_phone_02
      // 
      this.txt_phone_02.AllowSpace = true;
      this.txt_phone_02.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.txt_phone_02.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_phone_02.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_phone_02.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_phone_02.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_phone_02.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_phone_02.CornerRadius = 5;
      this.txt_phone_02.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_phone_02.Location = new System.Drawing.Point(153, 70);
      this.txt_phone_02.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
      this.txt_phone_02.MaxLength = 20;
      this.txt_phone_02.Multiline = false;
      this.txt_phone_02.Name = "txt_phone_02";
      this.txt_phone_02.PasswordChar = '\0';
      this.txt_phone_02.ReadOnly = false;
      this.txt_phone_02.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_phone_02.SelectedText = "";
      this.txt_phone_02.SelectionLength = 0;
      this.txt_phone_02.SelectionStart = 0;
      this.txt_phone_02.Size = new System.Drawing.Size(710, 40);
      this.txt_phone_02.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_phone_02.TabIndex = 1;
      this.txt_phone_02.TabStop = false;
      this.txt_phone_02.Tag = "HolderPhone2";
      this.txt_phone_02.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_phone_02.UseSystemPasswordChar = false;
      this.txt_phone_02.WaterMark = null;
      this.txt_phone_02.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // txt_email_01
      // 
      this.txt_email_01.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.txt_email_01.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_email_01.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_email_01.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_email_01.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_email_01.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_email_01.CornerRadius = 5;
      this.txt_email_01.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_email_01.Location = new System.Drawing.Point(153, 130);
      this.txt_email_01.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
      this.txt_email_01.MaxLength = 50;
      this.txt_email_01.Multiline = false;
      this.txt_email_01.Name = "txt_email_01";
      this.txt_email_01.PasswordChar = '\0';
      this.txt_email_01.ReadOnly = false;
      this.txt_email_01.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_email_01.SelectedText = "";
      this.txt_email_01.SelectionLength = 0;
      this.txt_email_01.SelectionStart = 0;
      this.txt_email_01.Size = new System.Drawing.Size(710, 40);
      this.txt_email_01.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_email_01.TabIndex = 2;
      this.txt_email_01.TabStop = false;
      this.txt_email_01.Tag = "HolderEmail1";
      this.txt_email_01.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_email_01.UseSystemPasswordChar = false;
      this.txt_email_01.WaterMark = null;
      this.txt_email_01.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // lbl_email_01
      // 
      this.lbl_email_01.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_email_01.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_email_01.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_email_01.Location = new System.Drawing.Point(3, 120);
      this.lbl_email_01.Name = "lbl_email_01";
      this.lbl_email_01.Size = new System.Drawing.Size(144, 60);
      this.lbl_email_01.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_email_01.TabIndex = 17;
      this.lbl_email_01.Tag = "lHolderEmail1";
      this.lbl_email_01.Text = "xEmail1";
      this.lbl_email_01.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // txt_twitter
      // 
      this.txt_twitter.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.txt_twitter.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_twitter.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_twitter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_twitter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_twitter.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_twitter.CornerRadius = 5;
      this.txt_twitter.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_twitter.Location = new System.Drawing.Point(153, 190);
      this.txt_twitter.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
      this.txt_twitter.MaxLength = 50;
      this.txt_twitter.Multiline = false;
      this.txt_twitter.Name = "txt_twitter";
      this.txt_twitter.PasswordChar = '\0';
      this.txt_twitter.ReadOnly = false;
      this.txt_twitter.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_twitter.SelectedText = "";
      this.txt_twitter.SelectionLength = 0;
      this.txt_twitter.SelectionStart = 0;
      this.txt_twitter.Size = new System.Drawing.Size(710, 40);
      this.txt_twitter.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_twitter.TabIndex = 3;
      this.txt_twitter.TabStop = false;
      this.txt_twitter.Tag = "HolderTwitterAccount";
      this.txt_twitter.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_twitter.UseSystemPasswordChar = false;
      this.txt_twitter.WaterMark = null;
      this.txt_twitter.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // txt_facebook
      // 
      this.txt_facebook.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.txt_facebook.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_facebook.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_facebook.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_facebook.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_facebook.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_facebook.CornerRadius = 5;
      this.txt_facebook.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_facebook.Location = new System.Drawing.Point(153, 249);
      this.txt_facebook.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
      this.txt_facebook.MaxLength = 50;
      this.txt_facebook.Multiline = false;
      this.txt_facebook.Name = "txt_facebook";
      this.txt_facebook.PasswordChar = '\0';
      this.txt_facebook.ReadOnly = false;
      this.txt_facebook.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_facebook.SelectedText = "";
      this.txt_facebook.SelectionLength = 0;
      this.txt_facebook.SelectionStart = 0;
      this.txt_facebook.Size = new System.Drawing.Size(710, 40);
      this.txt_facebook.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_facebook.TabIndex = 4;
      this.txt_facebook.TabStop = false;
      this.txt_facebook.Tag = "HolderFacebook";
      this.txt_facebook.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_facebook.UseSystemPasswordChar = false;
      this.txt_facebook.WaterMark = null;
      this.txt_facebook.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // lbl_twitter
      // 
      this.lbl_twitter.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_twitter.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_twitter.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_twitter.Location = new System.Drawing.Point(3, 180);
      this.lbl_twitter.Name = "lbl_twitter";
      this.lbl_twitter.Size = new System.Drawing.Size(144, 59);
      this.lbl_twitter.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_twitter.TabIndex = 20;
      this.lbl_twitter.Tag = "lHolderTwitterAccount";
      this.lbl_twitter.Text = "xTwitter";
      this.lbl_twitter.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_facebook
      // 
      this.lbl_facebook.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_facebook.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_facebook.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_facebook.Location = new System.Drawing.Point(3, 239);
      this.lbl_facebook.Name = "lbl_facebook";
      this.lbl_facebook.Size = new System.Drawing.Size(144, 57);
      this.lbl_facebook.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_facebook.TabIndex = 21;
      this.lbl_facebook.Tag = "lHolderFacebook";
      this.lbl_facebook.Text = "xFacebook";
      this.lbl_facebook.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // chk_data_agreement
      // 
      this.chk_data_agreement.BackColor = System.Drawing.Color.Transparent;
      this.chk_data_agreement.Enabled = false;
      this.chk_data_agreement.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.chk_data_agreement.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.chk_data_agreement.Location = new System.Drawing.Point(153, 299);
      this.chk_data_agreement.MinimumSize = new System.Drawing.Size(483, 30);
      this.chk_data_agreement.Name = "chk_data_agreement";
      this.chk_data_agreement.Padding = new System.Windows.Forms.Padding(5);
      this.chk_data_agreement.Size = new System.Drawing.Size(483, 30);
      this.chk_data_agreement.TabIndex = 1009;
      this.chk_data_agreement.Text = "XAgreement Text";
      this.chk_data_agreement.UseVisualStyleBackColor = false;
      // 
      // address
      // 
      this.address.Controls.Add(this.btn_copy_adddress);
      this.address.Controls.Add(this.tab_address);
      this.address.Location = new System.Drawing.Point(4, 45);
      this.address.Name = "address";
      this.address.Padding = new System.Windows.Forms.Padding(3);
      this.address.Size = new System.Drawing.Size(916, 365);
      this.address.TabIndex = 1;
      this.address.Text = "xDirección";
      this.address.UseVisualStyleBackColor = true;
      // 
      // btn_copy_adddress
      // 
      this.btn_copy_adddress.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_copy_adddress.FlatAppearance.BorderSize = 0;
      this.btn_copy_adddress.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_copy_adddress.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_copy_adddress.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_copy_adddress.Image = null;
      this.btn_copy_adddress.IsSelected = false;
      this.btn_copy_adddress.Location = new System.Drawing.Point(777, 5);
      this.btn_copy_adddress.Name = "btn_copy_adddress";
      this.btn_copy_adddress.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_copy_adddress.Size = new System.Drawing.Size(114, 39);
      this.btn_copy_adddress.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_copy_adddress.TabIndex = 46;
      this.btn_copy_adddress.TabStop = false;
      this.btn_copy_adddress.Text = "XCOPYADDRESS";
      this.btn_copy_adddress.UseVisualStyleBackColor = false;
      this.btn_copy_adddress.Visible = false;
      this.btn_copy_adddress.Click += new System.EventHandler(this.btn_copy_adddress_Click);
      // 
      // tab_address
      // 
      this.tab_address.Controls.Add(this.home_address);
      this.tab_address.Controls.Add(this.alt_address);
      this.tab_address.DisplayStyle = WSI.Cashier.Controls.TabStyle.Rounded;
      // 
      // 
      // 
      this.tab_address.DisplayStyleProvider.BorderColor = System.Drawing.SystemColors.ControlDark;
      this.tab_address.DisplayStyleProvider.BorderColorHot = System.Drawing.SystemColors.ControlDark;
      this.tab_address.DisplayStyleProvider.BorderColorSelected = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(157)))), ((int)(((byte)(185)))));
      this.tab_address.DisplayStyleProvider.FocusTrack = true;
      this.tab_address.DisplayStyleProvider.HotTrack = true;
      this.tab_address.DisplayStyleProvider.Opacity = 1F;
      this.tab_address.DisplayStyleProvider.Overlap = 0;
      this.tab_address.DisplayStyleProvider.Padding = new System.Drawing.Point(0, 0);
      this.tab_address.DisplayStyleProvider.Radius = 10;
      this.tab_address.DisplayStyleProvider.TabColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.tab_address.DisplayStyleProvider.TabColorHeader = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.tab_address.DisplayStyleProvider.TabColorSelected = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.tab_address.DisplayStyleProvider.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.tab_address.DisplayStyleProvider.TextColorSelected = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.tab_address.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tab_address.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.tab_address.HotTrack = true;
      this.tab_address.ItemSize = new System.Drawing.Size(100, 40);
      this.tab_address.Location = new System.Drawing.Point(3, 3);
      this.tab_address.Margin = new System.Windows.Forms.Padding(0);
      this.tab_address.Name = "tab_address";
      this.tab_address.SelectedIndex = 0;
      this.tab_address.Size = new System.Drawing.Size(910, 359);
      this.tab_address.TabIndex = 1;
      this.tab_address.SelectedIndexChanged += new System.EventHandler(this.tab_address_SelectedIndexChanged);
      this.tab_address.Click += new System.EventHandler(this.tab_address_Click);
      // 
      // home_address
      // 
      this.home_address.Controls.Add(this.HolderAddress);
      this.home_address.Location = new System.Drawing.Point(4, 45);
      this.home_address.Name = "home_address";
      this.home_address.Size = new System.Drawing.Size(902, 310);
      this.home_address.TabIndex = 0;
      this.home_address.Text = "x HomeAddress   ";
      this.home_address.UseVisualStyleBackColor = true;
      // 
      // HolderAddress
      // 
      this.HolderAddress.AddressLine1 = "";
      this.HolderAddress.AddressLine2 = "";
      this.HolderAddress.AddressLine3 = "";
      this.HolderAddress.Automatic = true;
      this.HolderAddress.Country = null;
      this.HolderAddress.HouseNumber = "";
      this.HolderAddress.Location = new System.Drawing.Point(0, 0);
      this.HolderAddress.Margin = new System.Windows.Forms.Padding(0);
      this.HolderAddress.Name = "HolderAddress";
      this.HolderAddress.PostCode = "";
      this.HolderAddress.Size = new System.Drawing.Size(872, 354);
      this.HolderAddress.State = null;
      this.HolderAddress.TabIndex = 0;
      this.HolderAddress.Tag = "Holder";
      this.HolderAddress.Town = "";
      // 
      // alt_address
      // 
      this.alt_address.Controls.Add(this.AlternativeAddress);
      this.alt_address.Location = new System.Drawing.Point(4, 45);
      this.alt_address.Name = "alt_address";
      this.alt_address.Size = new System.Drawing.Size(178, 0);
      this.alt_address.TabIndex = 1;
      this.alt_address.Text = "xAlternativeAddress";
      this.alt_address.UseVisualStyleBackColor = true;
      // 
      // AlternativeAddress
      // 
      this.AlternativeAddress.AddressLine1 = "";
      this.AlternativeAddress.AddressLine2 = "";
      this.AlternativeAddress.AddressLine3 = "";
      this.AlternativeAddress.Automatic = true;
      this.AlternativeAddress.Country = null;
      this.AlternativeAddress.HouseNumber = "";
      this.AlternativeAddress.Location = new System.Drawing.Point(0, 0);
      this.AlternativeAddress.Margin = new System.Windows.Forms.Padding(0);
      this.AlternativeAddress.Name = "AlternativeAddress";
      this.AlternativeAddress.PostCode = "";
      this.AlternativeAddress.Size = new System.Drawing.Size(870, 354);
      this.AlternativeAddress.State = null;
      this.AlternativeAddress.TabIndex = 1;
      this.AlternativeAddress.Tag = "Alternative";
      this.AlternativeAddress.Town = "";
      // 
      // comments
      // 
      this.comments.Controls.Add(this.chk_show_comments);
      this.comments.Controls.Add(this.txt_comments);
      this.comments.Location = new System.Drawing.Point(4, 45);
      this.comments.Name = "comments";
      this.comments.Size = new System.Drawing.Size(916, 365);
      this.comments.TabIndex = 3;
      this.comments.Text = "xComentarios";
      this.comments.UseVisualStyleBackColor = true;
      // 
      // chk_show_comments
      // 
      this.chk_show_comments.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.chk_show_comments.AutoSize = true;
      this.chk_show_comments.BackColor = System.Drawing.Color.Transparent;
      this.chk_show_comments.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.chk_show_comments.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.chk_show_comments.Location = new System.Drawing.Point(14, -23847);
      this.chk_show_comments.MinimumSize = new System.Drawing.Size(348, 30);
      this.chk_show_comments.Name = "chk_show_comments";
      this.chk_show_comments.Padding = new System.Windows.Forms.Padding(5);
      this.chk_show_comments.Size = new System.Drawing.Size(348, 33);
      this.chk_show_comments.TabIndex = 40;
      this.chk_show_comments.Text = "xShow comments when reading player card";
      this.chk_show_comments.UseVisualStyleBackColor = true;
      // 
      // txt_comments
      // 
      this.txt_comments.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_comments.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_comments.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_comments.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_comments.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_comments.CornerRadius = 5;
      this.txt_comments.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_comments.Location = new System.Drawing.Point(14, 13);
      this.txt_comments.MaxLength = 100;
      this.txt_comments.Multiline = true;
      this.txt_comments.Name = "txt_comments";
      this.txt_comments.PasswordChar = '\0';
      this.txt_comments.ReadOnly = false;
      this.txt_comments.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
      this.txt_comments.SelectedText = "";
      this.txt_comments.SelectionLength = 0;
      this.txt_comments.SelectionStart = 0;
      this.txt_comments.Size = new System.Drawing.Size(873, 316);
      this.txt_comments.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.MULTILINE;
      this.txt_comments.TabIndex = 1;
      this.txt_comments.TabStop = false;
      this.txt_comments.Tag = "HolderComments";
      this.txt_comments.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_comments.UseSystemPasswordChar = false;
      this.txt_comments.WaterMark = null;
      this.txt_comments.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // scanned_docs
      // 
      this.scanned_docs.Controls.Add(this.pnl_container);
      this.scanned_docs.Location = new System.Drawing.Point(4, 45);
      this.scanned_docs.Name = "scanned_docs";
      this.scanned_docs.Padding = new System.Windows.Forms.Padding(3);
      this.scanned_docs.Size = new System.Drawing.Size(916, 365);
      this.scanned_docs.TabIndex = 4;
      this.scanned_docs.Tag = "";
      this.scanned_docs.Text = "xEscaneado";
      this.scanned_docs.UseVisualStyleBackColor = true;
      // 
      // pnl_container
      // 
      this.pnl_container.Controls.Add(this.pnl_grd_docs);
      this.pnl_container.Controls.Add(this.pnl_photo);
      this.pnl_container.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pnl_container.Location = new System.Drawing.Point(3, 3);
      this.pnl_container.Name = "pnl_container";
      this.pnl_container.Size = new System.Drawing.Size(910, 359);
      this.pnl_container.TabIndex = 0;
      // 
      // pnl_grd_docs
      // 
      this.pnl_grd_docs.Controls.Add(this.grd_scanned_docs);
      this.pnl_grd_docs.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pnl_grd_docs.Location = new System.Drawing.Point(0, 0);
      this.pnl_grd_docs.Name = "pnl_grd_docs";
      this.pnl_grd_docs.Size = new System.Drawing.Size(676, 359);
      this.pnl_grd_docs.TabIndex = 1;
      // 
      // grd_scanned_docs
      // 
      this.grd_scanned_docs.AllowUserToAddRows = false;
      this.grd_scanned_docs.AllowUserToDeleteRows = false;
      this.grd_scanned_docs.AllowUserToResizeColumns = false;
      this.grd_scanned_docs.AllowUserToResizeRows = false;
      this.grd_scanned_docs.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.grd_scanned_docs.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.grd_scanned_docs.ColumnHeaderHeight = 35;
      this.grd_scanned_docs.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
      dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      dataGridViewCellStyle4.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      dataGridViewCellStyle4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      dataGridViewCellStyle4.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
      dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
      dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
      this.grd_scanned_docs.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
      this.grd_scanned_docs.ColumnHeadersHeight = 35;
      this.grd_scanned_docs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
      this.grd_scanned_docs.CornerRadius = 10;
      this.grd_scanned_docs.DefaultCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.grd_scanned_docs.DefaultCellForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.grd_scanned_docs.DefaultCellSelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.grd_scanned_docs.DefaultCellSelectionForeColor = System.Drawing.Color.White;
      dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      dataGridViewCellStyle5.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      dataGridViewCellStyle5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(211)))), ((int)(((byte)(216)))));
      dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
      this.grd_scanned_docs.DefaultCellStyle = dataGridViewCellStyle5;
      this.grd_scanned_docs.Dock = System.Windows.Forms.DockStyle.Fill;
      this.grd_scanned_docs.EnableHeadersVisualStyles = false;
      this.grd_scanned_docs.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.grd_scanned_docs.GridColor = System.Drawing.Color.LightGray;
      this.grd_scanned_docs.HeaderBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
      this.grd_scanned_docs.HeaderDefaultCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.grd_scanned_docs.HeaderDefaultCellForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.grd_scanned_docs.HeaderImages = null;
      this.grd_scanned_docs.Location = new System.Drawing.Point(0, 0);
      this.grd_scanned_docs.MultiSelect = false;
      this.grd_scanned_docs.Name = "grd_scanned_docs";
      this.grd_scanned_docs.ReadOnly = true;
      dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle6.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
      dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
      dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
      dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
      this.grd_scanned_docs.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
      this.grd_scanned_docs.RowHeadersVisible = false;
      this.grd_scanned_docs.RowHeadersWidth = 20;
      this.grd_scanned_docs.RowTemplateHeight = 35;
      this.grd_scanned_docs.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
      this.grd_scanned_docs.Size = new System.Drawing.Size(676, 359);
      this.grd_scanned_docs.Style = WSI.Cashier.Controls.uc_DataGridView.DataGridViewStyle.WITH_ALL_ROUND_CORNERS;
      this.grd_scanned_docs.TabIndex = 4;
      this.grd_scanned_docs.Tag = "ScannedDocumentsGrid";
      this.grd_scanned_docs.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grd_scanned_docs_CellClick);
      // 
      // pnl_photo
      // 
      this.pnl_photo.Controls.Add(this.btn_scan_doc);
      this.pnl_photo.Controls.Add(this.pb_scanned_photo);
      this.pnl_photo.Dock = System.Windows.Forms.DockStyle.Right;
      this.pnl_photo.Location = new System.Drawing.Point(676, 0);
      this.pnl_photo.Name = "pnl_photo";
      this.pnl_photo.Size = new System.Drawing.Size(234, 359);
      this.pnl_photo.TabIndex = 0;
      // 
      // btn_scan_doc
      // 
      this.btn_scan_doc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_scan_doc.FlatAppearance.BorderSize = 0;
      this.btn_scan_doc.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_scan_doc.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_scan_doc.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_scan_doc.Image = null;
      this.btn_scan_doc.IsSelected = false;
      this.btn_scan_doc.Location = new System.Drawing.Point(7, 298);
      this.btn_scan_doc.Name = "btn_scan_doc";
      this.btn_scan_doc.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_scan_doc.Size = new System.Drawing.Size(208, 58);
      this.btn_scan_doc.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_scan_doc.TabIndex = 34;
      this.btn_scan_doc.TabStop = false;
      this.btn_scan_doc.Text = "XSCANDOC";
      this.btn_scan_doc.UseVisualStyleBackColor = false;
      this.btn_scan_doc.Click += new System.EventHandler(this.btn_scan_doc_Click);
      // 
      // pb_scanned_photo
      // 
      this.pb_scanned_photo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.pb_scanned_photo.BackColor = System.Drawing.Color.Transparent;
      this.pb_scanned_photo.BackgroundImage = global::WSI.Cashier.Properties.Resources.card_header;
      this.pb_scanned_photo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
      this.pb_scanned_photo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.pb_scanned_photo.Cursor = System.Windows.Forms.Cursors.Default;
      this.pb_scanned_photo.Location = new System.Drawing.Point(7, 3);
      this.pb_scanned_photo.Name = "pb_scanned_photo";
      this.pb_scanned_photo.Size = new System.Drawing.Size(208, 314);
      this.pb_scanned_photo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
      this.pb_scanned_photo.TabIndex = 0;
      this.pb_scanned_photo.TabStop = false;
      this.pb_scanned_photo.Click += new System.EventHandler(this.pb_scanned_photo_Click);
      // 
      // pin
      // 
      this.pin.Controls.Add(this.lbl_pin_msg);
      this.pin.Controls.Add(this.txt_confirm_pin);
      this.pin.Controls.Add(this.txt_pin);
      this.pin.Controls.Add(this.btn_generate_random_pin);
      this.pin.Controls.Add(this.btn_save_pin);
      this.pin.Controls.Add(this.lbl_confirm_pin);
      this.pin.Controls.Add(this.lbl_pin);
      this.pin.Location = new System.Drawing.Point(4, 45);
      this.pin.Name = "pin";
      this.pin.Padding = new System.Windows.Forms.Padding(3);
      this.pin.Size = new System.Drawing.Size(916, 365);
      this.pin.TabIndex = 10;
      this.pin.Text = "xPin";
      this.pin.UseVisualStyleBackColor = true;
      // 
      // lbl_pin_msg
      // 
      this.lbl_pin_msg.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_pin_msg.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_pin_msg.Location = new System.Drawing.Point(473, 68);
      this.lbl_pin_msg.Name = "lbl_pin_msg";
      this.lbl_pin_msg.Size = new System.Drawing.Size(285, 57);
      this.lbl_pin_msg.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_pin_msg.TabIndex = 11;
      this.lbl_pin_msg.Text = "xPinMsg";
      // 
      // txt_confirm_pin
      // 
      this.txt_confirm_pin.AllowSpace = false;
      this.txt_confirm_pin.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_confirm_pin.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_confirm_pin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_confirm_pin.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_confirm_pin.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_confirm_pin.CornerRadius = 5;
      this.txt_confirm_pin.FillWithCeros = false;
      this.txt_confirm_pin.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_confirm_pin.Location = new System.Drawing.Point(277, 87);
      this.txt_confirm_pin.MaxLength = 12;
      this.txt_confirm_pin.Multiline = false;
      this.txt_confirm_pin.Name = "txt_confirm_pin";
      this.txt_confirm_pin.PasswordChar = '*';
      this.txt_confirm_pin.ReadOnly = false;
      this.txt_confirm_pin.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_confirm_pin.SelectedText = "";
      this.txt_confirm_pin.SelectionLength = 0;
      this.txt_confirm_pin.SelectionStart = 0;
      this.txt_confirm_pin.Size = new System.Drawing.Size(163, 40);
      this.txt_confirm_pin.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_confirm_pin.TabIndex = 1;
      this.txt_confirm_pin.TabStop = false;
      this.txt_confirm_pin.Text = "0000";
      this.txt_confirm_pin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.txt_confirm_pin.UseSystemPasswordChar = false;
      this.txt_confirm_pin.WaterMark = "****";
      this.txt_confirm_pin.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      this.txt_confirm_pin.TextChanged += new System.EventHandler(this.txt_confirm_pin_TextChanged);
      this.txt_confirm_pin.Enter += new System.EventHandler(this.txt_confirm_pin_Enter);
      // 
      // txt_pin
      // 
      this.txt_pin.AllowSpace = false;
      this.txt_pin.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_pin.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_pin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_pin.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_pin.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_pin.CornerRadius = 5;
      this.txt_pin.FillWithCeros = false;
      this.txt_pin.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_pin.Location = new System.Drawing.Point(277, 46);
      this.txt_pin.MaxLength = 12;
      this.txt_pin.Multiline = false;
      this.txt_pin.Name = "txt_pin";
      this.txt_pin.PasswordChar = '*';
      this.txt_pin.ReadOnly = false;
      this.txt_pin.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_pin.SelectedText = "";
      this.txt_pin.SelectionLength = 0;
      this.txt_pin.SelectionStart = 0;
      this.txt_pin.Size = new System.Drawing.Size(163, 40);
      this.txt_pin.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_pin.TabIndex = 0;
      this.txt_pin.TabStop = false;
      this.txt_pin.Text = "0000";
      this.txt_pin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.txt_pin.UseSystemPasswordChar = false;
      this.txt_pin.WaterMark = "****";
      this.txt_pin.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      this.txt_pin.TextChanged += new System.EventHandler(this.txt_pin_TextChanged);
      this.txt_pin.Enter += new System.EventHandler(this.txt_pin_Enter);
      // 
      // btn_generate_random_pin
      // 
      this.btn_generate_random_pin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_generate_random_pin.FlatAppearance.BorderSize = 0;
      this.btn_generate_random_pin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_generate_random_pin.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_generate_random_pin.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_generate_random_pin.Image = null;
      this.btn_generate_random_pin.IsSelected = false;
      this.btn_generate_random_pin.Location = new System.Drawing.Point(456, 147);
      this.btn_generate_random_pin.Name = "btn_generate_random_pin";
      this.btn_generate_random_pin.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_generate_random_pin.Size = new System.Drawing.Size(163, 47);
      this.btn_generate_random_pin.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_generate_random_pin.TabIndex = 3;
      this.btn_generate_random_pin.TabStop = false;
      this.btn_generate_random_pin.Text = "XGENERATEPIN";
      this.btn_generate_random_pin.UseVisualStyleBackColor = false;
      this.btn_generate_random_pin.Click += new System.EventHandler(this.btn_generate_random_pin_Click);
      // 
      // btn_save_pin
      // 
      this.btn_save_pin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_save_pin.FlatAppearance.BorderSize = 0;
      this.btn_save_pin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_save_pin.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_save_pin.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_save_pin.Image = null;
      this.btn_save_pin.IsSelected = false;
      this.btn_save_pin.Location = new System.Drawing.Point(277, 147);
      this.btn_save_pin.Name = "btn_save_pin";
      this.btn_save_pin.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_save_pin.Size = new System.Drawing.Size(163, 47);
      this.btn_save_pin.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_save_pin.TabIndex = 2;
      this.btn_save_pin.TabStop = false;
      this.btn_save_pin.Text = "XSAVEPIN";
      this.btn_save_pin.UseVisualStyleBackColor = false;
      this.btn_save_pin.Click += new System.EventHandler(this.btn_save_pin_Click);
      // 
      // lbl_confirm_pin
      // 
      this.lbl_confirm_pin.AutoSize = true;
      this.lbl_confirm_pin.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_confirm_pin.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_confirm_pin.Location = new System.Drawing.Point(147, 100);
      this.lbl_confirm_pin.Name = "lbl_confirm_pin";
      this.lbl_confirm_pin.Size = new System.Drawing.Size(96, 19);
      this.lbl_confirm_pin.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_confirm_pin.TabIndex = 7;
      this.lbl_confirm_pin.Text = "xConfirmPin";
      // 
      // lbl_pin
      // 
      this.lbl_pin.AutoSize = true;
      this.lbl_pin.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_pin.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_pin.Location = new System.Drawing.Point(147, 59);
      this.lbl_pin.Name = "lbl_pin";
      this.lbl_pin.Size = new System.Drawing.Size(39, 19);
      this.lbl_pin.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_pin.TabIndex = 6;
      this.lbl_pin.Text = "xPin";
      // 
      // blacklist
      // 
      this.blacklist.Controls.Add(this.chk_acknowledged_blacklist);
      this.blacklist.Controls.Add(this.flowLayoutPanel1);
      this.blacklist.Controls.Add(this.lbl_blacklist_data);
      this.blacklist.Controls.Add(this.pb_blacklist_data);
      this.blacklist.Controls.Add(this.lbl_blacklist_messages);
      this.blacklist.Location = new System.Drawing.Point(4, 45);
      this.blacklist.Name = "blacklist";
      this.blacklist.Padding = new System.Windows.Forms.Padding(3);
      this.blacklist.Size = new System.Drawing.Size(916, 365);
      this.blacklist.TabIndex = 11;
      this.blacklist.Text = "xBlackList";
      this.blacklist.UseVisualStyleBackColor = true;
      // 
      // chk_acknowledged_blacklist
      // 
      this.chk_acknowledged_blacklist.AutoSize = true;
      this.chk_acknowledged_blacklist.BackColor = System.Drawing.Color.Transparent;
      this.chk_acknowledged_blacklist.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.chk_acknowledged_blacklist.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.chk_acknowledged_blacklist.Location = new System.Drawing.Point(34, 292);
      this.chk_acknowledged_blacklist.MinimumSize = new System.Drawing.Size(206, 30);
      this.chk_acknowledged_blacklist.Name = "chk_acknowledged_blacklist";
      this.chk_acknowledged_blacklist.Padding = new System.Windows.Forms.Padding(5);
      this.chk_acknowledged_blacklist.Size = new System.Drawing.Size(206, 33);
      this.chk_acknowledged_blacklist.TabIndex = 96;
      this.chk_acknowledged_blacklist.Tag = "AcknowledgedBlackList";
      this.chk_acknowledged_blacklist.Text = "xAcknowledgedBlackList";
      this.chk_acknowledged_blacklist.UseVisualStyleBackColor = false;
      // 
      // flowLayoutPanel1
      // 
      this.flowLayoutPanel1.Controls.Add(this.btn_add_to_blacklist);
      this.flowLayoutPanel1.Controls.Add(this.btn_remove_from_blacklist);
      this.flowLayoutPanel1.Controls.Add(this.btn_show_matches);
      this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
      this.flowLayoutPanel1.Location = new System.Drawing.Point(508, 292);
      this.flowLayoutPanel1.Name = "flowLayoutPanel1";
      this.flowLayoutPanel1.Size = new System.Drawing.Size(386, 67);
      this.flowLayoutPanel1.TabIndex = 93;
      // 
      // btn_add_to_blacklist
      // 
      this.btn_add_to_blacklist.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_add_to_blacklist.FlatAppearance.BorderSize = 0;
      this.btn_add_to_blacklist.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_add_to_blacklist.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_add_to_blacklist.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_add_to_blacklist.Image = null;
      this.btn_add_to_blacklist.IsSelected = false;
      this.btn_add_to_blacklist.Location = new System.Drawing.Point(259, 3);
      this.btn_add_to_blacklist.Name = "btn_add_to_blacklist";
      this.btn_add_to_blacklist.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_add_to_blacklist.Size = new System.Drawing.Size(124, 59);
      this.btn_add_to_blacklist.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_add_to_blacklist.TabIndex = 93;
      this.btn_add_to_blacklist.TabStop = false;
      this.btn_add_to_blacklist.Text = "XADD TO BLACKLIST";
      this.btn_add_to_blacklist.UseVisualStyleBackColor = false;
      this.btn_add_to_blacklist.Click += new System.EventHandler(this.btn_add_to_blacklist_Click);
      // 
      // btn_remove_from_blacklist
      // 
      this.btn_remove_from_blacklist.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_remove_from_blacklist.FlatAppearance.BorderSize = 0;
      this.btn_remove_from_blacklist.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_remove_from_blacklist.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_remove_from_blacklist.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_remove_from_blacklist.Image = null;
      this.btn_remove_from_blacklist.IsSelected = false;
      this.btn_remove_from_blacklist.Location = new System.Drawing.Point(137, 3);
      this.btn_remove_from_blacklist.Name = "btn_remove_from_blacklist";
      this.btn_remove_from_blacklist.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_remove_from_blacklist.Size = new System.Drawing.Size(116, 59);
      this.btn_remove_from_blacklist.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_remove_from_blacklist.TabIndex = 94;
      this.btn_remove_from_blacklist.TabStop = false;
      this.btn_remove_from_blacklist.Text = "XREMOVE FROM BLACKLIST";
      this.btn_remove_from_blacklist.UseVisualStyleBackColor = false;
      this.btn_remove_from_blacklist.Click += new System.EventHandler(this.btn_remove_from_blacklist_Click);
      // 
      // btn_show_matches
      // 
      this.btn_show_matches.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_show_matches.FlatAppearance.BorderSize = 0;
      this.btn_show_matches.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_show_matches.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_show_matches.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_show_matches.Image = null;
      this.btn_show_matches.IsSelected = false;
      this.btn_show_matches.Location = new System.Drawing.Point(7, 3);
      this.btn_show_matches.Name = "btn_show_matches";
      this.btn_show_matches.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_show_matches.Size = new System.Drawing.Size(124, 59);
      this.btn_show_matches.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_show_matches.TabIndex = 95;
      this.btn_show_matches.TabStop = false;
      this.btn_show_matches.Text = "XSHOW MATCHES";
      this.btn_show_matches.UseVisualStyleBackColor = false;
      this.btn_show_matches.Click += new System.EventHandler(this.btn_show_matches_Click);
      // 
      // lbl_blacklist_data
      // 
      this.lbl_blacklist_data.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_blacklist_data.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_blacklist_data.Location = new System.Drawing.Point(52, 31);
      this.lbl_blacklist_data.Name = "lbl_blacklist_data";
      this.lbl_blacklist_data.Size = new System.Drawing.Size(519, 33);
      this.lbl_blacklist_data.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_blacklist_data.TabIndex = 87;
      this.lbl_blacklist_data.Text = "xInBlackList";
      // 
      // pb_blacklist_data
      // 
      this.pb_blacklist_data.Location = new System.Drawing.Point(14, 28);
      this.pb_blacklist_data.Name = "pb_blacklist_data";
      this.pb_blacklist_data.Size = new System.Drawing.Size(32, 32);
      this.pb_blacklist_data.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
      this.pb_blacklist_data.TabIndex = 86;
      this.pb_blacklist_data.TabStop = false;
      // 
      // lbl_blacklist_messages
      // 
      this.lbl_blacklist_messages.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.lbl_blacklist_messages.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.lbl_blacklist_messages.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.lbl_blacklist_messages.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.lbl_blacklist_messages.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.lbl_blacklist_messages.CornerRadius = 5;
      this.lbl_blacklist_messages.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_blacklist_messages.Location = new System.Drawing.Point(35, 66);
      this.lbl_blacklist_messages.MaxLength = 100;
      this.lbl_blacklist_messages.Multiline = true;
      this.lbl_blacklist_messages.Name = "lbl_blacklist_messages";
      this.lbl_blacklist_messages.PasswordChar = '\0';
      this.lbl_blacklist_messages.ReadOnly = true;
      this.lbl_blacklist_messages.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
      this.lbl_blacklist_messages.SelectedText = "";
      this.lbl_blacklist_messages.SelectionLength = 0;
      this.lbl_blacklist_messages.SelectionStart = 0;
      this.lbl_blacklist_messages.Size = new System.Drawing.Size(829, 220);
      this.lbl_blacklist_messages.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.MULTILINE;
      this.lbl_blacklist_messages.TabIndex = 97;
      this.lbl_blacklist_messages.TabStop = false;
      this.lbl_blacklist_messages.Tag = "HolderComments";
      this.lbl_blacklist_messages.Text = "XBLACKLISTREASONDESCRIPTION";
      this.lbl_blacklist_messages.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.lbl_blacklist_messages.UseSystemPasswordChar = false;
      this.lbl_blacklist_messages.WaterMark = null;
      this.lbl_blacklist_messages.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // lbl_expiration_date
      // 
      this.lbl_expiration_date.AutoSize = true;
      this.lbl_expiration_date.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.lbl_expiration_date.CornerRadius = 5;
      this.lbl_expiration_date.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_expiration_date.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_expiration_date.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.lbl_expiration_date.Location = new System.Drawing.Point(3, 458);
      this.lbl_expiration_date.Name = "lbl_expiration_date";
      this.lbl_expiration_date.Padding = new System.Windows.Forms.Padding(0, 1, 0, 3);
      this.lbl_expiration_date.Size = new System.Drawing.Size(106, 42);
      this.lbl_expiration_date.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_WHITE_BOLD;
      this.lbl_expiration_date.TabIndex = 1009;
      this.lbl_expiration_date.Text = "xExpirationDate";
      this.lbl_expiration_date.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      this.lbl_expiration_date.Visible = false;
      // 
      // lbl_vip_account
      // 
      this.lbl_vip_account.AutoSize = true;
      this.lbl_vip_account.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.lbl_vip_account.CornerRadius = 5;
      this.lbl_vip_account.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_vip_account.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_vip_account.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.lbl_vip_account.Location = new System.Drawing.Point(3, 147);
      this.lbl_vip_account.Name = "lbl_vip_account";
      this.lbl_vip_account.Padding = new System.Windows.Forms.Padding(0, 1, 0, 3);
      this.lbl_vip_account.Size = new System.Drawing.Size(106, 23);
      this.lbl_vip_account.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_WHITE_BOLD;
      this.lbl_vip_account.TabIndex = 1005;
      this.lbl_vip_account.Text = "xVip Account";
      this.lbl_vip_account.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // lbl_happy_birthday
      // 
      this.lbl_happy_birthday.AutoSize = true;
      this.lbl_happy_birthday.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(166)))), ((int)(((byte)(81)))));
      this.lbl_happy_birthday.CornerRadius = 5;
      this.lbl_happy_birthday.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_happy_birthday.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_happy_birthday.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.lbl_happy_birthday.Location = new System.Drawing.Point(3, 170);
      this.lbl_happy_birthday.Name = "lbl_happy_birthday";
      this.lbl_happy_birthday.Padding = new System.Windows.Forms.Padding(0, 1, 0, 3);
      this.lbl_happy_birthday.Size = new System.Drawing.Size(106, 42);
      this.lbl_happy_birthday.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_WHITE_BOLD;
      this.lbl_happy_birthday.TabIndex = 1008;
      this.lbl_happy_birthday.Text = "xHappy Birthday!";
      this.lbl_happy_birthday.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      this.lbl_happy_birthday.Visible = false;
      // 
      // lbl_level_data
      // 
      this.lbl_level_data.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_level_data.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_level_data.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_level_data.Location = new System.Drawing.Point(3, 129);
      this.lbl_level_data.Name = "lbl_level_data";
      this.lbl_level_data.Size = new System.Drawing.Size(106, 18);
      this.lbl_level_data.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_level_data.TabIndex = 1007;
      this.lbl_level_data.Tag = "";
      this.lbl_level_data.Text = "---";
      this.lbl_level_data.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // lbl_level
      // 
      this.lbl_level.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_level.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_level.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_level.Location = new System.Drawing.Point(3, 111);
      this.lbl_level.Name = "lbl_level";
      this.lbl_level.Size = new System.Drawing.Size(106, 18);
      this.lbl_level.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_level.TabIndex = 1006;
      this.lbl_level.Tag = "";
      this.lbl_level.Text = "xLevel";
      this.lbl_level.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // btn_keyboard
      // 
      this.btn_keyboard.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
      this.btn_keyboard.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(47)))), ((int)(((byte)(50)))));
      this.btn_keyboard.FlatAppearance.BorderSize = 0;
      this.btn_keyboard.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_keyboard.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_keyboard.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_keyboard.Image = global::WSI.Cashier.Properties.Resources.keyboard;
      this.btn_keyboard.IsSelected = false;
      this.btn_keyboard.Location = new System.Drawing.Point(18, 410);
      this.btn_keyboard.Name = "btn_keyboard";
      this.btn_keyboard.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_keyboard.Size = new System.Drawing.Size(75, 45);
      this.btn_keyboard.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.TERTIARY;
      this.btn_keyboard.TabIndex = 2;
      this.btn_keyboard.TabStop = false;
      this.btn_keyboard.UseVisualStyleBackColor = false;
      this.btn_keyboard.Click += new System.EventHandler(this.btn_keyboard_Click);
      // 
      // tableLayoutPanel1
      // 
      this.tableLayoutPanel1.ColumnCount = 1;
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.tableLayoutPanel1.Controls.Add(this.lbl_happy_birthday, 0, 4);
      this.tableLayoutPanel1.Controls.Add(this.m_img_photo, 0, 0);
      this.tableLayoutPanel1.Controls.Add(this.lbl_vip_account, 0, 3);
      this.tableLayoutPanel1.Controls.Add(this.lbl_level_data, 0, 2);
      this.tableLayoutPanel1.Controls.Add(this.lbl_level, 0, 1);
      this.tableLayoutPanel1.Controls.Add(this.lbl_expiration_date, 0, 9);
      this.tableLayoutPanel1.Controls.Add(this.btn_keyboard, 0, 8);
      this.tableLayoutPanel1.Controls.Add(this.btn_entry, 0, 6);
      this.tableLayoutPanel1.Controls.Add(this.btn_exit, 0, 7);
      this.tableLayoutPanel1.Controls.Add(this.btn_ok, 0, 5);
      this.tableLayoutPanel1.Location = new System.Drawing.Point(9, 9);
      this.tableLayoutPanel1.Name = "tableLayoutPanel1";
      this.tableLayoutPanel1.RowCount = 10;
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tableLayoutPanel1.Size = new System.Drawing.Size(112, 433);
      this.tableLayoutPanel1.TabIndex = 0;
      // 
      // btn_exit
      // 
      this.btn_exit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_exit.FlatAppearance.BorderSize = 0;
      this.btn_exit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_exit.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_exit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_exit.Image = null;
      this.btn_exit.IsSelected = false;
      this.btn_exit.Location = new System.Drawing.Point(3, 345);
      this.btn_exit.Name = "btn_exit";
      this.btn_exit.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_exit.Size = new System.Drawing.Size(106, 59);
      this.btn_exit.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_exit.TabIndex = 1;
      this.btn_exit.TabStop = false;
      this.btn_exit.Text = "XEXIT";
      this.btn_exit.UseVisualStyleBackColor = false;
      this.btn_exit.Click += new System.EventHandler(this.btn_exit_Click);
      // 
      // tableLayoutPanel2
      // 
      this.tableLayoutPanel2.ColumnCount = 1;
      this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.tableLayoutPanel2.Controls.Add(this.btn_cancel, 0, 2);
      this.tableLayoutPanel2.Controls.Add(this.btn_new_customer, 0, 1);
      this.tableLayoutPanel2.Controls.Add(this.btn_view_list, 0, 0);
      this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 454);
      this.tableLayoutPanel2.Name = "tableLayoutPanel2";
      this.tableLayoutPanel2.RowCount = 3;
      this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tableLayoutPanel2.Size = new System.Drawing.Size(128, 196);
      this.tableLayoutPanel2.TabIndex = 47;
      // 
      // btn_cancel
      // 
      this.btn_cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
      this.btn_cancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btn_cancel.FlatAppearance.BorderSize = 0;
      this.btn_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_cancel.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_cancel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_cancel.Image = null;
      this.btn_cancel.IsSelected = false;
      this.btn_cancel.Location = new System.Drawing.Point(3, 133);
      this.btn_cancel.Name = "btn_cancel";
      this.btn_cancel.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_cancel.Size = new System.Drawing.Size(121, 60);
      this.btn_cancel.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_cancel.TabIndex = 13;
      this.btn_cancel.TabStop = false;
      this.btn_cancel.Text = "XCANCEL";
      this.btn_cancel.UseVisualStyleBackColor = false;
      this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
      // 
      // pictureBox1
      // 
      this.pictureBox1.Image = global::WSI.Cashier.Resources.ResourceImages.warning_black1;
      this.pictureBox1.Location = new System.Drawing.Point(978, 12);
      this.pictureBox1.Name = "pictureBox1";
      this.pictureBox1.Size = new System.Drawing.Size(38, 35);
      this.pictureBox1.TabIndex = 48;
      this.pictureBox1.TabStop = false;
      this.pictureBox1.Visible = false;
      // 
      // pnl_results
      // 
      this.pnl_results.Controls.Add(this.grd_customers);
      this.pnl_results.Location = new System.Drawing.Point(127, 440);
      this.pnl_results.Name = "pnl_results";
      this.pnl_results.Size = new System.Drawing.Size(894, 210);
      this.pnl_results.TabIndex = 49;
      // 
      // PlayerEditDetailsReceptionView
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.ClientSize = new System.Drawing.Size(1024, 713);
      this.ControlBox = false;
      this.DoubleBuffered = true;
      this.KeyPreview = true;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "PlayerEditDetailsReceptionView";
      this.ShowIcon = false;
      this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
      this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
      this.Style = WSI.Cashier.Controls.frm_base.BaseFormStyle.PRINCIPAL;
      this.Text = "vw_PlayerEditDetailsReception";
      this.Load += new System.EventHandler(this.PlayerEditDetailsReceptionView_Load);
      this.Shown += new System.EventHandler(this.PlayerEditDetailsReceptionView_Shown);
      this.pnl_data.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.grd_customers)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.m_img_photo)).EndInit();
      this.tab_player_edit.ResumeLayout(false);
      this.principal.ResumeLayout(false);
      this.principal.PerformLayout();
      this.tableLayoutPanel3.ResumeLayout(false);
      this.tlp_name.ResumeLayout(false);
      this.tlp_principal.ResumeLayout(false);
      this.tlp_principal.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pb_record_expiration_status)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.pb_blacklist_status)).EndInit();
      this.tbl_type_doc.ResumeLayout(false);
      this.tlp_principal1.ResumeLayout(false);
      this.tlp_principal1.PerformLayout();
      this.contact.ResumeLayout(false);
      this.tlp_contact.ResumeLayout(false);
      this.address.ResumeLayout(false);
      this.tab_address.ResumeLayout(false);
      this.home_address.ResumeLayout(false);
      this.alt_address.ResumeLayout(false);
      this.comments.ResumeLayout(false);
      this.comments.PerformLayout();
      this.scanned_docs.ResumeLayout(false);
      this.pnl_container.ResumeLayout(false);
      this.pnl_grd_docs.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.grd_scanned_docs)).EndInit();
      this.pnl_photo.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.pb_scanned_photo)).EndInit();
      this.pin.ResumeLayout(false);
      this.pin.PerformLayout();
      this.blacklist.ResumeLayout(false);
      this.blacklist.PerformLayout();
      this.flowLayoutPanel1.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.pb_blacklist_data)).EndInit();
      this.tableLayoutPanel1.ResumeLayout(false);
      this.tableLayoutPanel1.PerformLayout();
      this.tableLayoutPanel2.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
      this.pnl_results.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion
    private WSI.Cashier.Controls.uc_round_auto_combobox cb_birth_country;
    private uc_label lbl_account_edit_title;
    private WSI.Cashier.Controls.uc_round_button_long_press btn_id_card_scan;
    private WSI.Cashier.Controls.uc_round_button btn_new_customer;
    private WSI.Cashier.Controls.uc_round_button btn_ok;
    private UcPhotobox m_img_photo;
    private uc_round_tab_control tab_player_edit;
    private System.Windows.Forms.TabPage principal;
    private System.Windows.Forms.TabPage contact;
    private System.Windows.Forms.TableLayoutPanel tlp_contact;
    private uc_label lbl_phone_01;
    private PhoneNumberTextBox txt_phone_01;
    private uc_label lbl_phone_02;
    private PhoneNumberTextBox txt_phone_02;
    private WSI.Cashier.Controls.uc_round_textbox txt_email_01;
    private uc_label lbl_email_01;
    private System.Windows.Forms.TabPage address;
    private System.Windows.Forms.TabPage comments;
    private uc_checkBox chk_show_comments;
    private WSI.Cashier.Controls.uc_round_textbox txt_comments;
    private System.Windows.Forms.TableLayoutPanel tlp_name;
    private CharacterTextBox txt_name4;
    private uc_label lbl_name4;
    private CharacterTextBox txt_name;
    private uc_label lbl_name;
    private uc_label lbl_last_name1;
    private CharacterTextBox txt_last_name1;
    private CharacterTextBox txt_last_name2;
    private uc_label lbl_last_name2;
    private System.Windows.Forms.TableLayoutPanel tlp_principal;
    private uc_datetime uc_dt_birth;
    private uc_label lbl_birth_date;
    private WSI.Cashier.Controls.uc_round_auto_combobox cb_document_type;
    private uc_label lbl_document;
    private CharacterTextBox txt_document;
    private System.Windows.Forms.TableLayoutPanel tlp_principal1;
    private WSI.Cashier.Controls.uc_round_auto_combobox cb_nationality;
    private uc_label lbl_nationality;
    private uc_label lbl_full_name;
    private WSI.Cashier.Controls.uc_round_auto_combobox cb_gender;
    private uc_label lbl_gender;
    // private uc_auto_combobox cb_birth_country;
    private uc_label lbl_birth_country;
    private WSI.Cashier.Controls.uc_round_button btn_view_list;
    private WSI.Cashier.Controls.uc_round_button btn_entry;
    private WSI.Cashier.Controls.uc_DataGridView grd_customers;
    private System.Windows.Forms.TabPage scanned_docs;
    private System.Windows.Forms.Panel pnl_container;
    private System.Windows.Forms.Panel pnl_grd_docs;
    private System.Windows.Forms.Panel pnl_photo;
    private System.Windows.Forms.PictureBox pb_scanned_photo;
    private WSI.Cashier.Controls.uc_round_button btn_scan_doc;
    private uc_label lbl_msg_blink;
    private uc_label lbl_black_list;
    private uc_label lbl_record;
    private WSI.Cashier.Controls.uc_round_button btn_image_black_list;
    private uc_label lbl_msg_count_blink;
    private uc_round_tab_control tab_address;
    private System.Windows.Forms.TabPage home_address;
    private System.Windows.Forms.TabPage alt_address;
    private uc_address HolderAddress;
    private uc_address AlternativeAddress;
    private System.Windows.Forms.TabPage pin;
    private uc_label lbl_pin_msg;
    private NumericTextBox txt_confirm_pin;
    private NumericTextBox txt_pin;
    private WSI.Cashier.Controls.uc_round_button btn_generate_random_pin;
    private WSI.Cashier.Controls.uc_round_button btn_save_pin;
    private uc_label lbl_confirm_pin;
    private uc_label lbl_pin;
    private uc_round_textbox txt_twitter;
    private uc_round_textbox txt_facebook;
    private uc_label lbl_twitter;
    private uc_label lbl_facebook;
    private uc_round_button btn_copy_adddress;
    private uc_label lbl_track_number;
    private uc_label lbl_track;
    private uc_label lbl_text_record_expiration_date;
    private uc_label lbl_record_expiration_date;
    private System.Windows.Forms.PictureBox pb_record_expiration_status;
    private System.Windows.Forms.PictureBox pb_blacklist_status;
    private uc_round_button btn_update_record_expiration;
    private uc_round_button btn_card_functions;
    private uc_round_button btn_keyboard;
    private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
    private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
    private uc_label lbl_vip_account;
    private uc_label lbl_level;
    private uc_label lbl_level_data;
    private System.Windows.Forms.TabPage blacklist;
    private System.Windows.Forms.PictureBox pb_blacklist_data;
    private uc_label lbl_blacklist_data;
    private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
    private uc_round_button btn_add_to_blacklist;
    private uc_round_button btn_remove_from_blacklist;
    private uc_DataGridView grd_scanned_docs;
    private System.Windows.Forms.PictureBox pictureBox1;
    private uc_round_button btn_show_matches;
    private uc_checkBox chk_acknowledged_blacklist;
    private uc_checkBox chk_data_agreement;
    private uc_round_textbox lbl_blacklist_messages;
    private uc_label lbl_happy_birthday;
    private System.Windows.Forms.Panel pnl_results;
    private uc_round_button btn_cancel;
    private uc_label lbl_type_document;
    private System.Windows.Forms.TableLayoutPanel tbl_type_doc;
    private uc_label lbl_type_doc_count;
    private uc_label lbl_expiration_date;
    private uc_round_auto_combobox cb_marital;
    private uc_label lbl_marital_status;
    private uc_label lbl_age;
    private uc_round_button btn_exit;
    //private uc_auto_combobox cb_nationality;



  }
}