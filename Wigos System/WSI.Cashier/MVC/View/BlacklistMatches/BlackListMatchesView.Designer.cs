﻿namespace WSI.Cashier.MVC.View.BlacklistMatches
{
  partial class BlackListMatchesView
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
      this.btn_ok = new WSI.Cashier.Controls.uc_round_button();
      this.btn_cancel = new WSI.Cashier.Controls.uc_round_button();
      this.grd_customers = new WSI.Cashier.Controls.uc_DataGridView();
      this.lbl_matches = new WSI.Cashier.Controls.uc_label();
      this.chk_no_matches = new WSI.Cashier.Controls.uc_checkBox();
      this.pnl_data.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.grd_customers)).BeginInit();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.Controls.Add(this.grd_customers);
      this.pnl_data.Controls.Add(this.chk_no_matches);
      this.pnl_data.Controls.Add(this.lbl_matches);
      this.pnl_data.Controls.Add(this.btn_cancel);
      this.pnl_data.Controls.Add(this.btn_ok);
      this.pnl_data.Size = new System.Drawing.Size(892, 393);
      // 
      // btn_ok
      // 
      this.btn_ok.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_ok.DialogResult = System.Windows.Forms.DialogResult.OK;
      this.btn_ok.FlatAppearance.BorderColor = System.Drawing.SystemColors.Control;
      this.btn_ok.FlatAppearance.BorderSize = 0;
      this.btn_ok.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_ok.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_ok.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_ok.Image = null;
      this.btn_ok.IsSelected = false;
      this.btn_ok.Location = new System.Drawing.Point(596, 321);
      this.btn_ok.Name = "btn_ok";
      this.btn_ok.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_ok.Size = new System.Drawing.Size(139, 63);
      this.btn_ok.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_ok.TabIndex = 2;
      this.btn_ok.Text = "XSELECT";
      this.btn_ok.UseVisualStyleBackColor = false;
      // 
      // btn_cancel
      // 
      this.btn_cancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btn_cancel.FlatAppearance.BorderColor = System.Drawing.SystemColors.Control;
      this.btn_cancel.FlatAppearance.BorderSize = 0;
      this.btn_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_cancel.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_cancel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_cancel.Image = null;
      this.btn_cancel.IsSelected = false;
      this.btn_cancel.Location = new System.Drawing.Point(741, 321);
      this.btn_cancel.Name = "btn_cancel";
      this.btn_cancel.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_cancel.Size = new System.Drawing.Size(139, 63);
      this.btn_cancel.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_cancel.TabIndex = 3;
      this.btn_cancel.Text = "XCANCEL";
      this.btn_cancel.UseVisualStyleBackColor = false;
      // 
      // grd_customers
      // 
      this.grd_customers.AllowUserToAddRows = false;
      this.grd_customers.AllowUserToDeleteRows = false;
      this.grd_customers.AllowUserToResizeColumns = false;
      this.grd_customers.AllowUserToResizeRows = false;
      this.grd_customers.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.grd_customers.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.grd_customers.ColumnHeaderHeight = 35;
      this.grd_customers.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
      dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      dataGridViewCellStyle1.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      dataGridViewCellStyle1.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
      dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
      dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
      this.grd_customers.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
      this.grd_customers.ColumnHeadersHeight = 35;
      this.grd_customers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
      this.grd_customers.CornerRadius = 10;
      this.grd_customers.DefaultCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.grd_customers.DefaultCellForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.grd_customers.DefaultCellSelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.grd_customers.DefaultCellSelectionForeColor = System.Drawing.Color.White;
      dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      dataGridViewCellStyle2.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(211)))), ((int)(((byte)(216)))));
      dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
      this.grd_customers.DefaultCellStyle = dataGridViewCellStyle2;
      this.grd_customers.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
      this.grd_customers.EnableHeadersVisualStyles = false;
      this.grd_customers.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.grd_customers.GridColor = System.Drawing.Color.LightGray;
      this.grd_customers.HeaderBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
      this.grd_customers.HeaderDefaultCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.grd_customers.HeaderDefaultCellForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.grd_customers.Location = new System.Drawing.Point(12, 74);
      this.grd_customers.Name = "grd_customers";
      this.grd_customers.ReadOnly = true;
      dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle3.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
      dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
      dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
      dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
      this.grd_customers.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
      this.grd_customers.RowHeadersVisible = false;
      this.grd_customers.RowHeadersWidth = 20;
      this.grd_customers.RowTemplateHeight = 35;
      this.grd_customers.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
      this.grd_customers.Size = new System.Drawing.Size(868, 242);
      this.grd_customers.Style = WSI.Cashier.Controls.uc_DataGridView.DataGridViewStyle.WITH_ALL_ROUND_CORNERS;
      this.grd_customers.TabIndex = 5;
      this.grd_customers.Tag = "GridCustomers";
      this.grd_customers.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.grd_customers_CellMouseClick);
      this.grd_customers.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.grd_customers_CellMouseDown);
      // 
      // lbl_matches
      // 
      this.lbl_matches.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_matches.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_matches.Location = new System.Drawing.Point(12, 20);
      this.lbl_matches.Name = "lbl_matches";
      this.lbl_matches.Size = new System.Drawing.Size(868, 50);
      this.lbl_matches.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_matches.TabIndex = 1021;
      this.lbl_matches.Text = "xLbl_Matches";
      // 
      // chk_no_matches
      // 
      this.chk_no_matches.AutoSize = true;
      this.chk_no_matches.BackColor = System.Drawing.Color.Transparent;
      this.chk_no_matches.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.chk_no_matches.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.chk_no_matches.Location = new System.Drawing.Point(16, 322);
      this.chk_no_matches.MinimumSize = new System.Drawing.Size(144, 30);
      this.chk_no_matches.Name = "chk_no_matches";
      this.chk_no_matches.Padding = new System.Windows.Forms.Padding(5);
      this.chk_no_matches.Size = new System.Drawing.Size(144, 33);
      this.chk_no_matches.TabIndex = 1022;
      this.chk_no_matches.Text = "XNO MATCHES";
      this.chk_no_matches.UseVisualStyleBackColor = false;
      this.chk_no_matches.CheckedChanged += new System.EventHandler(this.chk_no_matches_CheckedChanged);
      // 
      // BlackListMatchesView
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.ClientSize = new System.Drawing.Size(892, 448);
      this.Name = "BlackListMatchesView";
      this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
      this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.BlackListMatchesView_FormClosing);
      this.Load += new System.EventHandler(this.BlackListMatchesView_Load);
      this.pnl_data.ResumeLayout(false);
      this.pnl_data.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.grd_customers)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private Controls.uc_round_button btn_cancel;
    private Controls.uc_round_button btn_ok;
    private Controls.uc_DataGridView grd_customers;
    private Controls.uc_label lbl_matches;
    private Controls.uc_checkBox chk_no_matches;
  }
}
