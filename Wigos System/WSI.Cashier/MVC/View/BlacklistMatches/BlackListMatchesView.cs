﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using WSI.Cashier.MVC.Controller.BlackListMatches;
using WSI.Cashier.MVC.Interface;
using WSI.Cashier.MVC.Model.BlackListMatches;
using WSI.Cashier.MVC.View.PlayerEditDetails;
using WSI.Common;
using WSI.Common.Entrances;
using WSI.Common.Entrances.Enums;
using WSI.Common.Utilities;

namespace WSI.Cashier.MVC.View.BlacklistMatches
{
  public partial class BlackListMatchesView : vwBaseBlackListMatchesModel
  {
    #region Members
    Boolean m_freeze_checkedChange = false;
    #endregion


    public BlackListMatchesView()
    {
      InitializeComponent();
      this.ShowCloseButton = false;
    }

    public override void DoFirstSet()
    {
      ((BlackListMatchesController)m_controller).SetFormatDataGridPrincipal(grd_customers);
      return;
    }

    public override void ModelToView()
    {
      if (m_model == null)
        return;

      grd_customers.DataSource = m_model.GridData;
      chk_no_matches.Checked = m_model.NoMatches;
    }

    public override VisibleData VisibleFields
    {
      get { return new VisibleData(); }
    }

    public override void ViewToModel()
    {
      if (m_model == null)
        return;
      m_model.NoMatches = chk_no_matches.Checked; 
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void BlackListMatchesView_Load(object sender, EventArgs e)
    {
      InitControls();
    }

    /// <summary>
    /// Init Controls
    /// </summary>
    private void InitControls()
    {
      this.FormTitle = Resource.String("STR_FRM_BLACKLIST_MATCHES_FORM_TITLE");
      lbl_matches.Text = Resource.String("STR_FRM_BLACKLIST_MATCHES_SELECT").Replace("\\r\\n", "\r\n");
      btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CLOSE");
      btn_ok.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_OK");
      chk_no_matches.Text =  Resource.String("STR_FRM_BLACKLIST_MATCHES_NOTMATCHES");
      chk_no_matches.Checked = false;

      if (grd_customers.SelectedRows.Count != 0)
      {
        grd_customers.SelectedRows[0].Selected = false;
      }
      BuildList();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void BlackListMatchesView_FormClosing(object sender, FormClosingEventArgs e)
    {
      if ((m_model.CustomerSelected == null || m_model.CustomerSelected.Count==0 ) && !m_model.NoMatches)
      {
        DialogResult _dlr;
        frm_message _frm = new frm_message();

        _dlr = frm_message.Show(Resource.String("STR_FRM_BLACKLIST_MATCHES_CANCELLED").Replace("\\r\\n", "\r\n"),
                             Resource.String("STR_APP_GEN_MSG_ERROR"),
                             MessageBoxButtons.YesNo,
                             MessageBoxIcon.Error,
                             Cashier.MainForm);
        if (_dlr != DialogResult.OK)
        {
          e.Cancel = true;
        }
        else
        {
          this.DialogResult = DialogResult.Cancel;
        }
      }
    }
    DataGridViewRow[] old = {} ;

    private void grd_customers_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
    {
      old = new DataGridViewRow[grd_customers.SelectedRows.Count];
      grd_customers.SelectedRows.CopyTo(old, 0);
    }

    private void chk_no_matches_CheckedChanged(object sender, EventArgs e)
    {
      string _error_str;

      _error_str = "";
      if (this.m_freeze_checkedChange)
      {
        return;
      }

      //Only continue if the user has the permission
      if (!ProfilePermissions.CheckPermissionList(ProfilePermissions.CashierFormFuncionality.Reception_DeleteCoincidenceInBlackList,
                                                  ProfilePermissions.TypeOperation.RequestPasswd,
                                                  this,
                                                  0,
                                                  out _error_str))
      {        
        this.m_freeze_checkedChange = true;
        this.chk_no_matches.Checked = false;
        this.m_freeze_checkedChange = false;

        frm_message.Show(_error_str, Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, Images.CashierImage.Error, this);

        return;
      }

      if (chk_no_matches.Checked && grd_customers.SelectedRows.Count != 0)
      {
        grd_customers.SelectedRows[0].Selected = false;
      }

      m_model.NoMatches = chk_no_matches.Checked;
      Model = m_model;
    }
    class DrawingControl
    {
      [DllImport("user32.dll")]
      public static extern int SendMessage(IntPtr hWnd, Int32 wMsg, bool wParam, Int32 lParam);

      private const int WM_SETREDRAW = 11;

      public static void SuspendDrawing(Control parent)
      {
        SendMessage(parent.Handle, WM_SETREDRAW, false, 0);
      }

      public static void ResumeDrawing(Control parent)
      {
        SendMessage(parent.Handle, WM_SETREDRAW, true, 0);
        parent.Refresh();
      }
    }

    private void grd_customers_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
    {
      foreach (DataGridViewRow gr in old)
      {
        gr.Selected = gr != grd_customers.CurrentRow;
      }
      chk_no_matches.Checked = false;
      BuildList();
      //Model = m_model;
    }

    private void BuildList()
    {
      m_model.CustomerSelected = new List<BlackListResultCustomer>();

      if (m_model.ForcedItem != null)
        m_model.CustomerSelected.Add(m_model.ForcedItem);
      foreach (DataGridViewRow _selected_row in grd_customers.SelectedRows)
      {
        m_model.CustomerSelected.Add(_selected_row.DataBoundItem as BlackListResultCustomer);
      }
      m_model.NoMatches = chk_no_matches.Checked;
    }

    private void grd_customers_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
    {
      DrawingControl.ResumeDrawing(this);

    }

  }
  public class vwBaseBlackListMatchesModel: vw_base<BlackListMatchesModel>
  {
	  public override void RandomPinGeneratedOk()
	  {
		  throw new NotImplementedException();
	  }

	  public override void PinChangedOk()
	  {
		  throw new NotImplementedException();
	  }

	  public override void ErrorUpdatingPin()
	  {
		  throw new NotImplementedException();
	  }

	  public override VisibleData VisibleFields
    {
			get { return ((BlackListMatchesView)this).VisibleFields ; }
    }

    public override void ModelToView()
    {
			((BlackListMatchesView)this).ModelToView();
    }

    public override void ViewToModel()
    {
      ((BlackListMatchesView)this).ViewToModel();
    }

    public override void DoFirstSet()
    {
			((BlackListMatchesView)this).DoFirstSet();
    }
  }
}
