﻿namespace WSI.Cashier.MVC.View.TestMVC
{
  partial class TestMVCView
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.uc_round_textbox1 = new WSI.Cashier.Controls.uc_round_textbox();
      this.uc_round_button1 = new WSI.Cashier.Controls.uc_round_button();
      this.pnl_data.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.Controls.Add(this.uc_round_button1);
      this.pnl_data.Controls.Add(this.uc_round_textbox1);
      this.pnl_data.Size = new System.Drawing.Size(718, 485);
      // 
      // uc_round_textbox1
      // 
      this.uc_round_textbox1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.uc_round_textbox1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.uc_round_textbox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.uc_round_textbox1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.uc_round_textbox1.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.uc_round_textbox1.CornerRadius = 5;
      this.uc_round_textbox1.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.uc_round_textbox1.Location = new System.Drawing.Point(86, 31);
      this.uc_round_textbox1.MaxLength = 32767;
      this.uc_round_textbox1.Multiline = false;
      this.uc_round_textbox1.Name = "uc_round_textbox1";
      this.uc_round_textbox1.PasswordChar = '\0';
      this.uc_round_textbox1.ReadOnly = false;
      this.uc_round_textbox1.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.uc_round_textbox1.SelectedText = "";
      this.uc_round_textbox1.SelectionLength = 0;
      this.uc_round_textbox1.SelectionStart = 0;
      this.uc_round_textbox1.Size = new System.Drawing.Size(294, 40);
      this.uc_round_textbox1.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.uc_round_textbox1.TabIndex = 0;
      this.uc_round_textbox1.TabStop = false;
      this.uc_round_textbox1.Text = "uc_round_textbox1";
      this.uc_round_textbox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.uc_round_textbox1.UseSystemPasswordChar = false;
      this.uc_round_textbox1.WaterMark = null;
      this.uc_round_textbox1.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // uc_round_button1
      // 
      this.uc_round_button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.uc_round_button1.FlatAppearance.BorderColor = System.Drawing.SystemColors.Control;
      this.uc_round_button1.FlatAppearance.BorderSize = 0;
      this.uc_round_button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.uc_round_button1.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.uc_round_button1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.uc_round_button1.Image = null;
      this.uc_round_button1.IsSelected = false;
      this.uc_round_button1.Location = new System.Drawing.Point(471, 110);
      this.uc_round_button1.Name = "uc_round_button1";
      this.uc_round_button1.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.uc_round_button1.Size = new System.Drawing.Size(158, 95);
      this.uc_round_button1.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.uc_round_button1.TabIndex = 1;
      this.uc_round_button1.Text = "UC_ROUND_BUTTON1";
      this.uc_round_button1.UseVisualStyleBackColor = false;
      this.uc_round_button1.Click += new System.EventHandler(this.uc_round_button1_Click);
      // 
      // TestMVCView
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(718, 540);
      this.Name = "TestMVCView";
      this.Text = "TestMVCView";
      this.pnl_data.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private Controls.uc_round_textbox uc_round_textbox1;
    private Controls.uc_round_button uc_round_button1;
  }
}