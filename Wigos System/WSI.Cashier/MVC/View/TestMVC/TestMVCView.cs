﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WSI.Cashier.MVC.Controller.TestMVC;
using WSI.Cashier.MVC.Interface;
using WSI.Cashier.MVC.Model.TestMVC;
using WSI.Common;

namespace WSI.Cashier.MVC.View.TestMVC
{
  public partial class TestMVCView : vwBaseTestMvcModel
  {
    public TestMVCView()
    {
      InitializeComponent();
    }

    public override void DoFirstSet()
    {
      return;
    }

    public override void ModelToView()
    {
      if (m_model == null)
        return;
      uc_round_textbox1.Text = m_model.Name;
    }

    public override VisibleData VisibleFields
    {
      get { return new VisibleData(); }
    }

    public override void ViewToModel()
    {
      if (m_model == null)
        return;
      m_model.Name = uc_round_textbox1.Text;
    }

    private void uc_round_button1_Click(object sender, EventArgs e)
    {
      MessageBox.Show(((TestMVCController) m_controller).SaveData() ? "saved ok" : "error");
    }
  }

  public class vwBaseTestMvcModel : vw_base<TestMVCModel>
  {
    public override VisibleData VisibleFields
    {
      get { throw new NotImplementedException(); }
    }

    public override void ModelToView()
    {
      throw new NotImplementedException();
    }

    public override void ViewToModel()
    {
      throw new NotImplementedException();
    }

    public override void DoFirstSet()
    {
      throw new NotImplementedException();
    }

		public override void RandomPinGeneratedOk()
		{
			throw new NotImplementedException();
		}

		public override void PinChangedOk()
		{
			throw new NotImplementedException();
		}

		public override void ErrorUpdatingPin()
		{
			throw new NotImplementedException();
		}
	}
}
