﻿using System;
using WSI.Cashier.Controls;
using WSI.Cashier.MVC.Interface;

namespace WSI.Cashier.MVC.View
{
  public abstract partial class vw_base<T> : frm_base where T : IMVCModel
  {
    public vw_base()
    {
      InitializeComponent();
    }

    private bool m_first_set_done = false;
    protected IMVCController<T> m_controller;
    protected T m_model;
    public T Model
    {
      get
      {
        if (m_model == null)
          return default(T);
        ViewToModel();
        return m_model;
      }
      set
      {
        m_model = value;
        if (m_model == null)
          return;
        if (!m_first_set_done)
        {
          DoFirstSet();
          m_first_set_done = true;
        }
        ModelToView();
      }
    }

	  public abstract void RandomPinGeneratedOk();

	  public abstract void PinChangedOk();

	  public abstract void ErrorUpdatingPin();

	  public IMVCController<T> Controller
    {
      get { return m_controller; }
      set { m_controller = value; }
    } //Controller
    public abstract Common.VisibleData VisibleFields { get; }
    public abstract void ModelToView();
    public abstract void ViewToModel();
    public abstract void DoFirstSet();

  }
}

