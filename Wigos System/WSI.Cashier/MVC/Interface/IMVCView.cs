﻿using WSI.Common;

namespace WSI.Cashier.MVC.Interface
{
  public interface IMVCView<T> where T : IMVCModel
  {
    /// <summary>
    ///   collection of fields that are visible
    /// </summary>
    VisibleData VisibleFields { get; }

    /// <summary>
    ///   Property reference of the controller
    /// </summary>
    IMVCController<T> Controller { get; set; }

    /// <summary>
    ///   Property reference of the model
    /// </summary>
    T Model { get; set; }

		void RandomPinGeneratedOk();

		void PinChangedOk();

		void ErrorUpdatingPin();
	}
}
