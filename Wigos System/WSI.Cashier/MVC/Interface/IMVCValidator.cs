using System.Collections.Generic;
using WSI.Common.Entities.General;

namespace WSI.Cashier.MVC.Interface
{
  public interface IMVCValidator
  {
    /// <summary>
    ///   Validate routine, return list of InvalidField containing all fields with errors
    /// </summary>
    /// <param name="HiddenFields"></param>
    /// <param name="RequiredFields"></param>
    /// <returns></returns>
    List<InvalidField> Validate(List<string> HiddenFields, List<string> RequiredFields);
  }
}
