namespace WSI.Cashier.MVC.Interface
{
  public interface IMVCModel
  {
    string[] Fields { get; }
    object GetValue(string PropertyName);
  }
  public interface IMVCModel<T> where T:IMVCModel
  {
  }
}
