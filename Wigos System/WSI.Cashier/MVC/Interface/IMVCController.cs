﻿using System;

namespace WSI.Cashier.MVC.Interface
{
  public interface IMVCController<T> : IDisposable where T : IMVCModel
  {
  }
}
