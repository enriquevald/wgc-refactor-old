﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WSI.Cashier.MVC.Interface;
using WSI.Cashier.MVC.Model.BlackListMatches;
using WSI.Cashier.MVC.View.BlacklistMatches;
using WSI.Common;
using WSI.Common.Entrances;
using WSI.Common.Entrances.Enums;
using WSI.Common.Utilities;

namespace WSI.Cashier.MVC.Controller.BlackListMatches
{
  public class BlackListMatchesController: BaseController<BlackListMatchesModel>
  {

    public BlackListMatchesController(BlackListResultCustomer[] DsCustomersResults
                                    , bool Validated
                                    , IMVCView<BlackListMatchesModel> ViewForm)
    {
      View = ViewForm;
      View.Controller = this;
      
      var _mdl = CreateModel(DsCustomersResults, Validated);      
      Model = _mdl;

    }
 
    internal void Run()
    {
      ViewForm.ShowDialog();
    }

    private BlackListMatchesModel CreateModel(BlackListResultCustomer[] DsCustomersResults
                                    , bool Validated)
    {
      BlackListMatchesModel _mdl = new BlackListMatchesModel();
      _mdl.NoMatches = false;
      if (DsCustomersResults == null)
      {
        return _mdl;
      }

      var _grid_data = FillGridData(DsCustomersResults,_mdl);

      _mdl.GridData = _grid_data;
      _mdl.Validated = Validated;

      return _mdl;
    }

    private BindingList<BlackListResultCustomer> FillGridData(BlackListResultCustomer[] DsCustomersResults, BlackListMatchesModel Mdl)
    {
      BindingList<BlackListResultCustomer> _grid_data = new BindingList<BlackListResultCustomer>();
  
      foreach (BlackListResultCustomer _row in DsCustomersResults)
      {
        if (_row.Source == ENUM_BLACKLIST_SOURCE.LOCAL)
        {
          Mdl.ForcedItem = _row;
          continue;
        }
        _grid_data.Add(_row);
      }
      return _grid_data;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="grd_customers"></param>
    public void SetFormatDataGridPrincipal(DataGridView DataGrid)
    {
      string _date_format;
      try
      {
        DataGrid.AutoGenerateColumns = false;
        DataGrid.MultiSelect = true;
        DataGrid.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

        _date_format = System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern;

        // Set row height for bigger font sizes

        DataGrid.ColumnHeadersHeight = 30;
        DataGrid.RowTemplate.Height = 45;
        DataGrid.RowTemplate.MinimumHeight = 45;
        DataGrid.RowTemplate.DividerHeight = 0;

        // Columns
        //CustomerFullName { get; set; }
        //CustumerDocNmbr { get; set; }
        //InclusionDate { get; set; }
        //ReasonType { get; set; }


        DataGridViewColumn _col1 = new DataGridViewTextBoxColumn
        {
          DataPropertyName = "CustomerFullName",
          Name = "CustomerFullName",
          HeaderText = Resource.String("STR_FRM_PLAYER_EDIT_FULL_NAME"),
          AutoSizeMode =  DataGridViewAutoSizeColumnMode.Fill
        };
        _col1.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
        _col1.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
        DataGridViewColumn _col2 = new DataGridViewTextBoxColumn
        {
          DataPropertyName = "CustumerDocNmbr",
          Name = "CustumerDocNmbr",
          HeaderText = Resource.String("STR_FRM_PLAYER_EDIT_DOCUMENT"),
          Width = 110
        };
        _col2.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
        _col2.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        DataGridViewColumn _col3 = new DataGridViewTextBoxColumn
        {
          DataPropertyName = "InclusionDate",
          Name = "InclusionDate",
          HeaderText = Resource.String("STR_FRM_BLACKLIST_MATCHES_EXCLUSION_DATE"),
          Width = 130
        };
        _col3.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
        _col3.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
        _col3.DefaultCellStyle.Format = _date_format;
        DataGridViewColumn _col4 = new DataGridViewTextBoxColumn
        {
          DataPropertyName = "ReasonType",
          Name = "ReasonType",
          HeaderText = Resource.String("STR_FRM_BLACKLIST_MATCHES_EXCLUSION_REASON"),
          Width = 150
          
        };
        _col4.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
        _col4.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
        DataGridViewColumn _col5 = new DataGridViewTextBoxColumn
        {
          DataPropertyName = "Source",
          Name = "Source",
          HeaderText = Resource.String("STR_FRM_RECEPTION_BLACKLIST_SOURCE"),
          Width = 100

        };
        _col5.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
        _col5.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

        DataGrid.Columns.Add(_col1);
        DataGrid.Columns.Add(_col2);
        DataGrid.Columns.Add(_col3);
        DataGrid.Columns.Add(_col4);
        DataGrid.Columns.Add(_col5);
        DataGrid.CellFormatting += grd_customers_CellFormatting;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

    } //SetFormatDataGridPrincipal  

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void grd_customers_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
    {
      ((DataGridView)sender).Rows[e.RowIndex].DefaultCellStyle.SelectionBackColor = Color.FromArgb(45, 47, 50);
      ((DataGridView)sender).Rows[e.RowIndex].DefaultCellStyle.SelectionForeColor = Color.FromArgb(244, 246, 249);


      if (((DataGridView)sender).Columns[e.ColumnIndex].Name == "ReasonType")
      {
        Dictionary<Enum, string> _enum_values = EnumHelper.ToDictionary2<ENUM_BLACKLIST_REASON>();
        e.Value = _enum_values[(ENUM_BLACKLIST_REASON)e.Value];
      }

      if (((DataGridView)sender).Columns[e.ColumnIndex].Name == "Source")
      {
        Dictionary<Enum, string> _enum_values = EnumHelper.ToDictionary2<ENUM_BLACKLIST_SOURCE>();
        if(_enum_values.ContainsKey((ENUM_BLACKLIST_SOURCE)e.Value))
        {
          e.Value = _enum_values[(ENUM_BLACKLIST_SOURCE) e.Value];
        }
        else
        {
          e.Value = "";
        }
      }


    } //grd_customers_CellFormatting
       
  }
}
