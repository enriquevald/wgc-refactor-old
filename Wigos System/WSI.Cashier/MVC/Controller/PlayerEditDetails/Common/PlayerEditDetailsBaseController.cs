//------------------------------------------------------------------------------
// Copyright � 2007-2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: PlayerEditdetailsBaseController.cs
// 
//   DESCRIPTION: Base class for controlling common customer information forms
//                
//                
//                
//                
//                
//                
// 
//        AUTHOR: Scott Adamson.
// 
// CREATION DATE: 02-DEC-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 02-DEC-2007 SJA    First release.
// 31-MAY-2016 FJC    Bug 13982:Recepci�n: Crear un nuevo cliente con PIN y modificarlo es permitido desde el m�dulo de Recepci�n.
// 01-JUN-2016 SMN    Bug 14055:Recepci�n: Persistencia de los datos introducidos anteriormente cuando creas mas de un cliente seguido, en los campos Tipo Documento, Pa�s 
// 02-DEC-2016 JBC    Fixed Bug 21241:The field Birthdate doesn't appear on the player's edit voucher for new users.
// 10-ENE-2017 DPC    Bug 21943:Recepci�n: Permite guardar Accounts con el mismo HolderId desde la opci�n "GUARDAR PIN"
// 12-ENE-2017 DPC    Bug 22421:Auditor�a: mensajes err�neos al imprimir datos del cliente
// 19-JAN-2017 FOS    Fixed Bug 22330: Print ticket 2 times, when modify the user data.
// 24-MAR-2017 ETP    WIGOS-110: Creditlines - Paybacks
// 24-MAY-2017 RGR    Fixed Bug 24542: Audit: Misinformed records when creating a new account
// 03-JUL-2017 FJC    WIGOS-3354 Reception -MaritalStatus- Error in old accounts.
// 30-AUG-2017 AMF    Bug 29493:[WIGOS-3623] Log error in Cashier when trying to create an account.
// 03-OCT-2017 DHA    PBI 30017:WIGOS-4056 Payment threshold registration - Player registration
// 13-FEB-2018 EOR    Bug 31515: WIGOS-8032 [Ticket #12229] Formulario de inscripci�n de jugadores en registro por l�mite de pago 
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using DirectShowLib.BDA;
using WSI.Cashier.Controls;
using WSI.Cashier.MVC.Interface;
using WSI.Cashier.MVC.Model.PlayerEditDetails;
using WSI.Cashier.MVC.Model.PlayerEditDetails.Common;
using WSI.Cashier.MVC.View;
using WSI.Cashier.MVC.View.PlayerEditDetails;
using WSI.Cashier.MVC.View.PlayerEditDetails.Common;
using WSI.Common;
using WSI.Common.CreditLines;
using WSI.Common.Entities.General;
using WSI.Common.Utilities.Extensions;
using WSI.IDCamera;
using WSI.IDCamera.Model.Exceptions;
using WSI.IDScanner.Model;

namespace WSI.Cashier.MVC.Controller.PlayerEditDetails.Common
{
  /// <summary>
  ///   Abstact base controller, extend for use
  /// </summary>
  /// <typeparam name="T"></typeparam>
  public abstract class PlayerEditDetailsBaseController<T> : BaseController<T>
    where T : PlayerEditDetailsBaseModel, new()
  {
    #region Constants

    public const int AUDIT_CODE_ACCOUNTS = 27;
    public const int AUDIT_NEW_TITLE_ID = 6500 + 3;
    public const int AUDIT_MODIFY_TITLE_ID = 6500 + 4;
    public const int AUDIT_FIELD_ID_MODIFY = 6500 + 2;
    public const int AUDIT_FIELD_ID_NEW = 6500 + 1;
    public PlayerEditDetailsCashierView.SCREEN_MODE ScreenMode { get; set; }

    #endregion


    /// <summary>
    ///   add marital status list to model
    /// </summary>
    /// <param name="Retval"></param>
    /// <returns></returns>
    public T AddMartialStatuses(T Retval)
    {
      Retval.MaritalStatuses = new List<MaritalStatus>
        {
          new MaritalStatus {Id = 0, Name = ""},
          new MaritalStatus {Id = 1, Name = Resource.String("STR_FRM_PLAYER_TRACKING_CB_MARITAL_SINGLE")},
          new MaritalStatus {Id = 2, Name = Resource.String("STR_FRM_PLAYER_TRACKING_CB_MARITAL_MARRIED")},
          new MaritalStatus {Id = 3, Name = Resource.String("STR_FRM_PLAYER_TRACKING_CB_MARITAL_DIVORCED")},
          new MaritalStatus {Id = 4, Name = Resource.String("STR_FRM_PLAYER_TRACKING_CB_MARITAL_WIDOWED")},
          new MaritalStatus {Id = 5, Name = Resource.String("STR_FRM_PLAYER_TRACKING_CB_MARITAL_UNMARRIED")},
          new MaritalStatus {Id = 8, Name = Resource.String("STR_FRM_PLAYER_TRACKING_CB_MARITAL_ANNULED")},
          new MaritalStatus {Id = 6, Name = Resource.String("STR_FRM_PLAYER_TRACKING_CB_MARITAL_OTHER")},
          new MaritalStatus {Id = 7, Name = Resource.String("STR_FRM_PLAYER_TRACKING_CB_MARITAL_UNSPECIFIED"),
          }
        };
      return Retval;
    }


    #region Private Fields

    private List<string> m_anti_laundering_fields;


    /// <summary>
    ///   read once return many property for antimoney laundering check enabled
    /// </summary>
    private bool? m_antimoneylaundering_enabled;

    private List<string> m_hidden_fields;

    private bool m_must_show_comments;


    private Thread m_picture_retreival_thread;
    private List<string> m_required_fields;


    private bool? m_scanner_enabled;

    private bool m_has_changes_printed;
    #endregion    internal object ModelLocker = null;

    #region Delegates

    public delegate T LoadExtentionDelegate(T Model);

    public delegate List<InvalidField> SaveExtentionDelegate(T Model, SqlTransaction Transaction);

    #endregion

    /// <summary>
    ///   dictionary used by audit player data to convert internal field names to localized field names
    /// </summary>
    private readonly Dictionary<string, string> m_localized_descriptions = new Dictionary<string, string>
      {
        {"HolderName", AccountFields.GetName()},
        {"HolderName3", AccountFields.GetName()},
        {"HolderName4", AccountFields.GetMiddleName()},
        {"HolderName1", AccountFields.GetSurname1()},
        {"HolderName2", AccountFields.GetSurname2()},
        {"HolderId", Resource.String("STR_FRM_PLAYER_EDIT_DOCUMENT")},
        {"HolderId1", IdentificationTypes.DocIdTypeString(ACCOUNT_HOLDER_ID_TYPE.RFC)},
        {"HolderId2", IdentificationTypes.DocIdTypeString(ACCOUNT_HOLDER_ID_TYPE.CURP)},
        {"HolderBirthDateText", Resource.String("STR_FRM_PLAYER_EDIT_BIRTH_DATE")},
        {"HolderIdType", Resource.String("STR_FRM_PLAYER_EDIT_TYPE_DOCUMENT")},
        {"HolderId1Type", Resource.String("STR_FRM_PLAYER_EDIT_TYPE_DOCUMENT")},
        {"HolderId2Type", Resource.String("STR_FRM_PLAYER_EDIT_TYPE_DOCUMENT")},
        {"HolderId3Type",IdentificationTypes.DocIdTypeStringList(IdentificationTypes.DocIdTypeString(ACCOUNT_HOLDER_ID_TYPE.UNKNOWN))},
        {"HolderGender", Resource.String("STR_UC_PLAYER_TRACK_GENDER")},
        {"HolderMaritalStatus", Resource.String("STR_UC_PLAYER_TRACK_MARITAL_STATUS")},
        {"HolderWeddingDateText", Resource.String("STR_FRM_PLAYER_EDIT_WEDDING_DATE")},
        {"HolderOccupationId", Resource.String("STR_FRM_PLAYER_EDIT_OCCUPATION")},
        {"HolderBirthCountry", Resource.String("STR_FRM_ACCOUNT_USER_EDIT_BIRTH_COUNTRY")},
        {"HolderNationality", Resource.String("STR_FRM_ACCOUNT_USER_EDIT_NATIONALITY")},
        {"HolderPhone01", Resource.String("STR_UC_PLAYER_TRACK_PHONE_01") + " 1"},
        {"HolderPhone02", Resource.String("STR_UC_PLAYER_TRACK_PHONE_01") + " 2"},
        {"HolderEmail01", Resource.String("STR_UC_PLAYER_TRACK_EMAIL_01") + " 1"},
        {"HolderEmail02", Resource.String("STR_UC_PLAYER_TRACK_EMAIL_01") + " 2"},
        {"HolderTwitter", Resource.String("STR_UC_PLAYER_TRACK_TWITTER")},
        {"HolderAddress01", AccountFields.GetAddressName() /*Resource.String("STR_UC_PLAYER_TRACK_ADDRESS_01")*/},
        {"HolderAddress02", AccountFields.GetColonyName() /*Resource.String("STR_UC_PLAYER_TRACK_ADDRESS_02")*/},
        {"HolderAddress03", AccountFields.GetDelegationName() /*Resource.String("STR_UC_PLAYER_TRACK_ADDRESS_03")*/},
        {"HolderCity", AccountFields.GetTownshipName() /*Resource.String("STR_UC_PLAYER_TRACK_CITY")*/},
        {"HolderZip", AccountFields.GetZipName() /*Resource.String("STR_UC_PLAYER_TRACK_ZIP")*/ },
        {"HolderFedEntity", AccountFields.GetStateName() /*AccountFields.GetStateName()*/ },
        {"HolderAddressValidation", Resource.String("STR_FRM_ACCOUNT_USER_EDIT_ADDRESS_VALIDATION")},
        {"HolderAddressCountry", Resource.String("STR_FRM_ACCOUNT_USER_EDIT_COUNTRY")},
        {"HolderExtNumber", AccountFields.GetNumExtName() /*Resource.String("STR_FRM_ACCOUNT_USER_EDIT_EXT_NUM")*/ },
        {"HolderComments", Resource.String("STR_UC_PLAYER_TRACK_COMMENTS")},
        {"HolderHasBeneficiary", Resource.String("STR_FRM_ACCOUNT_USER_EDIT_HOLDER_HAS_BENEF")},
        {
          "BeneficiaryName",
          Resource.String("STR_FRM_ACCOUNT_USER_EDIT_NAME") + " " + Resource.String("STR_FRM_PLAYER_EDIT_BENEFICIARY")
        },
        {
          "BeneficiaryName3",
          Resource.String("STR_FRM_ACCOUNT_USER_EDIT_NAME") + " " + Resource.String("STR_FRM_PLAYER_EDIT_BENEFICIARY")
        },
        {
          "BeneficiaryName1",
          Resource.String("STR_FRM_PLAYER_EDIT_LAST_NAME1") + " " + Resource.String("STR_FRM_PLAYER_EDIT_BENEFICIARY")
        },
        {
          "BeneficiaryName2",
          Resource.String("STR_FRM_PLAYER_EDIT_LAST_NAME2") + " " + Resource.String("STR_FRM_PLAYER_EDIT_BENEFICIARY")
        },
        {
          "BeneficiaryGender",
          Resource.String("STR_UC_PLAYER_TRACK_GENDER") + " " + Resource.String("STR_FRM_PLAYER_EDIT_BENEFICIARY")
        },
        {
          "BeneficiaryOccupationId",
          Resource.String("STR_FRM_PLAYER_EDIT_OCCUPATION") + " " + Resource.String("STR_FRM_PLAYER_EDIT_BENEFICIARY")
        },
        {
          "BeneficiaryBirthDateText",
          Resource.String("STR_FRM_PLAYER_EDIT_BIRTH_DATE") + " " + Resource.String("STR_FRM_PLAYER_EDIT_BENEFICIARY")
        },
        {
          "BeneficiaryId3",
          Resource.String("STR_FRM_PLAYER_EDIT_SCAN_DOCUMENT") + " " + Resource.String("STR_FRM_PLAYER_EDIT_BENEFICIARY")
        },
        {
          "BeneficiaryId3Type",
          Resource.String("STR_FRM_PLAYER_EDIT_SCAN_DOCUMENT") + " " + Resource.String("STR_FRM_PLAYER_EDIT_BENEFICIARY")
        },
        {
          "BeneficiaryId1",
          IdentificationTypes.DocIdTypeString(ACCOUNT_HOLDER_ID_TYPE.RFC) + " " +
          Resource.String("STR_FRM_PLAYER_EDIT_BENEFICIARY")
        },
        {
          "BeneficiaryId2",
          IdentificationTypes.DocIdTypeString(ACCOUNT_HOLDER_ID_TYPE.CURP) + " " +
          Resource.String("STR_FRM_PLAYER_EDIT_BENEFICIARY")
        },
        {
          "CreditLineData",
          Resource.String("STR_FRM_ACTION_CREDITLINE_PAYBACK_AUDIT")
        }
      };

    public LoadExtentionDelegate LoadExtention;

    public SaveExtentionDelegate SaveExtention;

    public void IdScanAdvanced()
    {
      if (m_idscanner == null)
      {
        return;
      }
      string[] _scanner = m_idscanner.AllAvailable();
      if (_scanner.Length == 1)
        m_idscanner.AdvancedScan(_scanner[0]);
    }

    /// <summary>
    ///   Constructs instance, pass card data and target view
    /// </summary>
    /// <param name="CrdData"></param>
    /// <param name="View"></param>
    protected PlayerEditDetailsBaseController(CardData CrdData, IMVCView<T> View, IDScanner.IDScanner IDScanner)
    {
      this.IDScanner = IDScanner;
      DocsHolderChanged = false;
      DocsBeneficiaryChanged = false;
      _AllowAccountsDuplicates = GeneralParam.GetBoolean("Account", "AllowDuplicate", false);
    }

    /// <summary>
    ///   Empty contructor for compatibility
    /// </summary>
    protected PlayerEditDetailsBaseController()
    {
      DocsHolderChanged = false;
      DocsBeneficiaryChanged = false;
      _AllowAccountsDuplicates = GeneralParam.GetBoolean("Account", "AllowDuplicate", false);
    }

    /// <summary>
    ///   Controls if the view should force view comments
    /// </summary>
    public bool MustShowComments
    {
      get { return m_must_show_comments; }
      set
      {
        m_must_show_comments = value;
        if (Model != null)
        {
          Model.MustShowComments = m_must_show_comments;
        }
      }
    }
    
    public bool ScannerEnabled
    {
      get
      {
        if (m_scanner_enabled.HasValue)
        {
          return m_scanner_enabled.Value;
        }
        if (m_idscanner != null)
        {
          m_scanner_enabled = m_idscanner.AttatchToAllAvailable(HandleIDScannerEvent) > 0;
        }
        else
        {
          m_scanner_enabled = false;
        }
        return m_scanner_enabled.Value;
      }
    }


    public bool AntiMoneyLaunderingEnabled
    {
      get
      {
        if (Model is PlayerEditDetailsReceptionModel)
          return false;
        if (!m_antimoneylaundering_enabled.HasValue)
        {
          m_antimoneylaundering_enabled = GeneralParam.GetBoolean("AntiMoneyLaundering", "Enabled");
        }
        return m_antimoneylaundering_enabled.Value;
      }
    }

    public CameraModule CameraModule { get; set; }

    public Boolean Dialog_Cancel { get; set; }

    public CardData CardData { get; set; }

    internal T LastModel { get; set; }

    protected bool DocsBeneficiaryChanged { get; set; }

    protected bool DocsHolderChanged { get; set; }

    public IDScanner.IDScanner IDScanner
    {
      set { m_idscanner = value; }
      get { return m_idscanner; }
    }

    public uc_bank UcBank
    { get; set; }

    public bool _AllowAccountsDuplicates = false;
    public bool _showndupdetails = false;

    #region IMVCController<T> Members

    public override void Dispose()
    {
      base.Dispose();
      if (m_idscanner != null)
      {
        m_idscanner.DetatchFromAllAvailable(HandleIDScannerEvent);
      }

      if (CameraModule != null)
      {
        CameraModule.UnRegisterHandler(PhotoHandler);
      }
    }

    #endregion
    /// <summary>
    ///   abstract forced implementation for each derrived type
    ///   accepts card data returns Model
    /// </summary>
    /// <param name="CrdData"></param>
    /// <returns></returns>
    public abstract T CreateModelFromCardData(CardData CrdData);

    /// <summary>
    ///   forced implementation for printing of information in derrived type
    /// </summary>
    /// <returns></returns>
    public abstract List<InvalidField> PrintInformation();

    /// <summary>
    ///   forced implementation of chgeking for changes in model
    /// </summary>
    /// <param name="Old"></param>
    /// <param name="New"></param>
    /// <returns></returns>
    internal abstract bool HasChanges(T Old, T New);

    /// <summary>
    ///   forced implementation in derrived type for updating cadrd data from model
    /// </summary>
    /// <param name="CrdData"></param>
    /// <param name="Model"></param>
    /// <returns></returns>
    protected abstract CardData UpdateCardFromModel(CardData CrdData, T Model);

    /// <summary>
    ///   forced implementation method to save scanned image from id scanner
    /// </summary>
    /// <param name="Image"></param>
    /// <param name="DocumentTypeId"></param>
    /// <param name="Expiry"></param>
    /// <param name="Model"></param>
    /// <returns></returns>
    public abstract T SaveIdScanImage(Image Image, int DocumentTypeId, DateTime Expiry, T Model);

    /// <summary>
    ///   forced implementation for updating a model from the card data
    /// </summary>
    /// <param name="CrdData"></param>
    /// <param name="Model"></param>
    /// <returns></returns>
    protected abstract T UpdateModelFromCardData(CardData CrdData, T Model);

    /// <summary>
    ///   method to delay loading the account photo
    /// </summary>
    /// <param name="AccountId"></param>
    public void DelayedAccountPhotoLoad(long AccountId)
    {
      if (m_picture_retreival_thread != null && m_picture_retreival_thread.ThreadState == ThreadState.Running)
      {
        m_picture_retreival_thread.Interrupt();
        m_picture_retreival_thread.Join();
      }
      m_picture_retreival_thread = null;

      ParameterizedThreadStart _threadstart = delegate
      {
        try
        {
          Image _photo;
          using (var _trx = new DB_TRX())
          {
            AccountPhotoFunctions.LoadAccountPhoto(AccountId, out _photo, _trx.SqlTransaction);
          }
          T _mdl = (T)((View as Control).GetControlPropertyThreadSafe("Model"));
          if (!_mdl.AccountId.HasValue || _mdl.AccountId != AccountId)
          {
            return;
          }
          _mdl.HolderPhoto = _photo;
          (View as Control).SetControlPropertyThreadSafe("Model", _mdl);
          if (LastModel == null)
          {
            return;
          }
          LastModel.HolderPhoto = _mdl.HolderPhoto;
        }
        catch (ThreadInterruptedException)
        {
          //nothing to see here...
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }
      };
      m_picture_retreival_thread = new Thread(_threadstart);

      m_picture_retreival_thread.Start(AccountId);
    }


    /// <summary>
    ///   show dialog for saving coonfirmation if criteria is met
    /// </summary>
    /// <param name="CheckNewAccount"></param>
    /// <param name="CheckChanges"></param>
    /// <param name="Message"></param>
    /// <returns></returns>
    protected DialogResult SavingCardConfirmation(bool CheckNewAccount, bool CheckChanges, string Message)
    {
      string _message;
      DialogResult _dlg_rc;
      bool _show_dlg;
      _dlg_rc = DialogResult.None;
      _show_dlg = (CheckNewAccount && CardData.AccountId == 0);

      if (!_show_dlg && CheckChanges)
      {
        CardData _card_data = UpdateCardFromModel((CardData)CardData.Clone(), Model);
        _show_dlg = PlayerDataHasChanges(_card_data.PlayerTracking, CardData.PlayerTracking);
      }

      if (!_show_dlg)
      {
        return _dlg_rc;
      }

      _message = Message.Replace("\\r\\n", "\r\n");
      _dlg_rc = frm_message.Show(_message,
                                 Resource.String("STR_APP_GEN_MSG_WARNING"),
                                 MessageBoxButtons.YesNo,
                                 Images.CashierImage.Warning,
                                 (Form)View);

      return _dlg_rc;
    }


    /// <summary>
    ///   minimal common funcionality to print information, return s a list fo invalid fields
    /// </summary>
    /// <returns></returns>
    public List<InvalidField> PrintInformationCommon()
    {
      DialogResult _dlg_rc;

      _dlg_rc = SavingCardConfirmation(true, true, Resource.String("STR_FRM_ACCOUNT_USER_EDIT_PRINT_DATA"));
      m_has_changes_printed = false;

      switch (_dlg_rc)
      {
        case DialogResult.OK:
          List<InvalidField> _errors;
          _errors = Save(true);
          if (_errors.Count > 0)
          {
            return _errors;
          }
          break;
        case DialogResult.Cancel:
          return new List<InvalidField>();
      }


      if (!m_has_changes_printed)
      {
        //Print Data
        PrintData();
      }

      return new List<InvalidField>();
    }

    /// <summary>
    ///   Routine to print data
    /// </summary>
    private void PrintData()
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        VoucherCardHolderUpdate _voucher;
        _voucher = new VoucherCardHolderUpdate(CardData,WSI.Common.OperationCode.NOT_SET , PrintMode.Print, _db_trx.SqlTransaction);
        _voucher.AddString("VOUCHER_CARD_HOLDERNAME", CardData.PlayerTracking.HolderName);
        VoucherPrint.Print(_voucher);
      }
    }

    /// <summary>
    ///   helper routine to set form title dependent on model state
    /// </summary>
    /// <param name="Mdl"></param>
    /// <returns></returns>
    public string SetFormTitle(T Mdl)
    {
      //   - Title
      string _retval;
      PlayerEditDetailsBaseModel _mdl = Mdl;
      if (_mdl == null)
        return "";

      _retval = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_TITLE");

      if (_mdl.MustShowComments)
      {
        _retval = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_TITLE_COMMENTS");
      }

      if (string.IsNullOrEmpty(_mdl.HolderFullName))
      {
        return _retval;
      }
      if (_mdl.AccountId.HasValue)
      {
        _retval += " " + _mdl.AccountId + " - " + _mdl.HolderFullName;
      }
      else
      {
        _retval += " " + _mdl.HolderFullName;
      }
      return _retval;
    }

    /// <summary>
    ///   helper routine that sets the value of an ISelectable type field in the model to the matching item in teh colleciton
    ///   provided
    /// </summary>
    /// <typeparam name="TU"></typeparam>
    /// <param name="List"></param>
    /// <param name="ItemId"></param>
    /// <param name="BlankValue"></param>
    /// <returns></returns>
    public TU GetSelectableOption<TU>(List<TU> List, int ItemId, string BlankValue) where TU : ISelectableOption, new()
    {
      TU _option;
      _option = new TU { Id = ItemId, Name = BlankValue };

      if (List != null)
      {
        if (ItemId > List[0].Id)
        {
          foreach (TU _opt in List)
          {
            if (_opt.Id != ItemId)
            {
              continue;
            }
            _option = _opt;
            break;
          }
        }
        else
        {
          _option = List[0];
        }
      }

      return _option;
    }

    /// <summary>
    ///   common base minimal funcionality to create abase model form teh provided card data
    /// </summary>
    /// <param name="CrdData"></param>
    /// <returns></returns>
    public T CreateModelFromCardDataCommon(CardData CrdData)
    {
      T _retval = new T();
      _retval = AddDocumentTypes(_retval);
      _retval = AddGenders(_retval);
      _retval = AddNationalities(_retval);
      _retval = AddCountries(_retval);
      _retval = AddMartialStatuses(_retval);
      _retval = PlayerDataToModel(CrdData, _retval);
      return _retval;
    }

    /// <summary>
    ///   copies card data to the model
    /// </summary>
    /// <param name="CrdData"></param>
    /// <param name="Retval"></param>
    /// <returns></returns>
    private T PlayerDataToModel(CardData CrdData, T Retval)
    {
      CardData.PlayerTrackingData _player = CrdData.PlayerTracking;

      if (Retval.IDDocuments == null)
        Retval.IDDocuments = new Dictionary<int, string>();
      else
      {
        Retval.IDDocuments.Clear();
      }



      Retval.Pin = _player.Pin;
      Retval.PinFailures = _player.PinFailures;

      Retval.HolderDocScan = _player.HolderScannedDocs;
      Retval.HolderDocument = _player.HolderId;

      Retval.HolderAddressValidation = _player.HolderAddressValidation;
      Retval.HolderExtNum = _player.HolderExtNumber;
      Retval.HolderAddressLine01 = _player.HolderAddress01;
      Retval.HolderAddressLine02 = _player.HolderAddress02;
      Retval.HolderAddressLine03 = _player.HolderAddress03;
      Retval.HolderCity = _player.HolderCity;
      Retval.HolderFedEntity = new State { Id = _player.HolderFedEntity };
      Retval.HolderAddressCountry = new Country { Id = _player.HolderAddressCountry };
      Retval.HolderZip = _player.HolderZip;

      Retval.HolderEmail1 = _player.HolderEmail01;
      Retval.HolderBirthDate = _player.HolderBirthDate;
      Retval.HolderBirthDayDateShowNotification = GetHolderBirthDayNotification(CrdData.AccountId, _player.HolderName, _player.HolderBirthDate);

      Retval.HolderBirthCountry = GetSelectableOption(Retval.Countries, (int)_player.HolderBirthCountry, "");
      Retval.HolderTwitterAccount = _player.HolderTwitter;

      Retval.HolderDocumentType = GetSelectableOption(Retval.DocumentTypes, (int)_player.HolderIdType, "");

      Retval.HolderMaritalStatus = GetSelectableOption(Retval.MaritalStatuses, CrdData.PlayerTracking.HolderMaritalStatus, "");


      Retval.HolderPhone2 = _player.HolderPhone02;
      Retval.HolderPhone1 = _player.HolderPhone01;

      Retval.HolderGender = GetSelectableOption(Retval.Genders, _player.HolderGender, "");

      Retval.Comments = _player.HolderComments;

      Retval.ShowComments = _player.ShowCommentsOnCashier;


      Retval.HolderNationality = GetSelectableOption(Retval.Nationalities, (int)_player.HolderNationality, "");

      if (CrdData.AccountId > 1)
      {
        using (var _trx = new DB_TRX())
        {
          Retval.HasPhoto = AccountPhotoFunctions.HasAccountPhoto(CrdData.AccountId, _trx.SqlTransaction);
          Retval.CardCreationDate = Accounts.GetAccountCreationDate(CrdData.AccountId, _trx.SqlTransaction);
        }
        Retval.AccountId = CrdData.AccountId;
        Retval.HolderPhoto = null;
        Retval.PhotoChanged = false;
      }
      else
      {
        Retval.AccountId = null;
        Retval.HolderPhoto = null;
        Retval.PhotoChanged = false;
        Retval.HasPhoto = false;
        Retval.CardCreationDate = WGDB.Now;
      }

      Retval.HolderName1 = _player.HolderName1;
      Retval.HolderName2 = _player.HolderName2;
      Retval.HolderName3 = _player.HolderName3;
      Retval.HolderName4 = _player.HolderName4;
      Retval.HolderFullName = _player.HolderName;
      Retval.HolderDocument3Type = _player.HolderId3Type;
      Retval.BeneficiaryDocument3Type = _player.BeneficiaryId3Type;
      Retval.CardLevel = _player.CardLevel;
      Retval.CardLevelName = LoyaltyProgram.LevelName(Retval.CardLevel);
      Retval.HolderIsVip = _player.HolderIsVIP;

      if (LoadExtention != null)
      {
        Retval = LoadExtention(Retval);
      }
      return Retval;
    }

    /// <summary>
    ///   minimal funcionality to update a model card data provided
    /// </summary>
    /// <param name="CrdData"></param>
    /// <param name="Mdl"></param>
    /// <returns></returns>
    protected T UpdateModelFromCardDataCommon(CardData CrdData, T Mdl)
    {
      Mdl = PlayerDataToModel(CrdData, Mdl);
      return Mdl;
    }

    /// <summary>
    ///   add nationalities collection to the model
    /// </summary>
    /// <param name="Retval"></param>
    /// <returns></returns>
    protected T AddNationalities(T Retval)
    {
      DataTable _dt_nationalities;
      DataRow _dr_empty_row;
      _dt_nationalities = CardData.GetNationalitiesList();
      // Non-mandatory field.  An empty row is needed.
      _dr_empty_row = _dt_nationalities.NewRow();
      _dr_empty_row["CO_COUNTRY_ID"] = 0;
      _dr_empty_row["CO_ADJECTIVE"] = "";
      _dt_nationalities.Rows.InsertAt(_dr_empty_row, 0);
      Retval.Nationalities = _dt_nationalities.ToSelectableOptionList<Nationality>();
      return Retval;
    }

    /// <summary>
    ///   add document types collection to the model
    /// </summary>
    /// <param name="Retval"></param>
    /// <returns></returns>
    protected T AddDocumentTypes(T Retval)
    {
      DataTable _dt_doc_types;
      DataRow _dr_empty_row;
      _dt_doc_types = IdentificationTypes.GetIdentificationTypes(true);
      _dr_empty_row = _dt_doc_types.NewRow();
      _dr_empty_row[0] =-1;
      _dr_empty_row[1] = "";
      _dt_doc_types.Rows.InsertAt(_dr_empty_row, 0);
      Retval.DocumentTypes = _dt_doc_types.ToSelectableOptionList<DocumentType>();
      return Retval;
    }

    /// <summary>
    /// Set form's controls to disable when results of search is more than one
    /// </summary>
    /// <param name="Ctrl"></param>
    /// <param name="IgnoredControls"></param>
    /// <param name="AffectsVisibility"></param>
    /// <param name="ReadMode"></param>
    public void SetControlsReadMode(Control Ctrl, List<string> IgnoredControls, List<string> AffectsVisibility, bool ReadMode)
    {
      SetControlsReadModeLoop(Ctrl, IgnoredControls, AffectsVisibility, ReadMode);
    } //SetControlsToReadMode


    /// <summary>
    /// Set form's controls to disable when results of search is more than one
    /// </summary>
    /// <param name="Ctrl"></param>
    /// <param name="IgnoredControls"></param>
    /// <param name="AffectVisibility"></param>
    /// <param name="ReadMode"></param>
    public void SetControlsReadModeLoop(Control Ctrl, List<string> IgnoredControls, List<string> AffectVisibility, bool ReadMode)
    {

      if (!IgnoredControls.Contains(Ctrl.Name))
      {
        Type _type;
        _type = Ctrl.GetType();

        switch (_type.Name)
        {
          case "CharacterTextBox":
          case "uc_round_textbox":
          case "uc_datetime":
          case "PhoneNumberTextBox":
          case "NumericTextBox":
          case "uc_round_button":
          case "uc_round_auto_combobox":
          case "uc_address":
          case "uc_checkBox":
          case "UcPhotobox":
            Ctrl.Enabled = !ReadMode;
            break;
        }
      }
      if (AffectVisibility.Contains(Ctrl.Name))
      {
        Ctrl.Visible = !ReadMode;
      }
      if (!Ctrl.HasChildren)
      {
        return;
      }
      foreach (Control _ctrl in Ctrl.Controls)
      {
        SetControlsReadModeLoop(_ctrl, IgnoredControls, AffectVisibility, ReadMode);
      }

    }//SetControlsToReadModeLoop

    /// <summary>
    ///   add countries collection to the model
    /// </summary>
    /// <param name="Retval"></param>
    /// <returns></returns>
    protected T AddCountries(T Retval)
    {
      DataTable _dt_countries;
      DataRow _dr_empty_row;

      _dt_countries = CardData.GetCountriesList();
      // Non-mandatory field.  An empty row is needed.
      _dr_empty_row = _dt_countries.NewRow();
      _dr_empty_row["CO_COUNTRY_ID"] = 0;
      _dr_empty_row["CO_NAME"] = "";

      _dt_countries.Rows.InsertAt(_dr_empty_row, 0);

      Retval.Countries = _dt_countries.ToSelectableOptionList<Country>();
      return Retval;
    }

    /// <summary>
    ///   add genders collection to the model
    /// </summary>
    /// <param name="Retval"></param>
    /// <returns></returns>
    protected T AddGenders(T Retval)
    {
      Retval.Genders = new List<Gender>
        {
          new Gender {Id = 0, Name = ""},
          new Gender {Id = 1, Name = Resource.String("STR_FRM_PLAYER_TRACKING_CB_GENDER_MALE")},
          new Gender {Id = 2, Name = Resource.String("STR_FRM_PLAYER_TRACKING_CB_GENDER_FEMALE")}
        };
      return Retval;
    }


    public bool BlockIfNotSaved(PlayerEditDetailsCommonErrorDisplay<T> ErrorDisplay, bool ForceNoOption = false)
    {
      var _model = Model;

      if (_model == null || LastModel == null || _model.IsNew || !HasChanges(LastModel, _model))
      {
        return false;
      }
      DialogResult _dlg_rc2;
      if (!ForceNoOption && (!_model.AccountId.HasValue || _model.AccountId < 1))
      {
        _dlg_rc2 =
          frm_message.Show(
            string.Format(Resource.String("STR_RECEPTION_VIEW_DIRTY_FORM"), _model.HolderFullName, Environment.NewLine),
            Resource.String("STR_APP_GEN_MSG_WARNING"), MessageBoxButtons.OKCancel, Images.CashierImage.Warning,
            (Form)View);
      }
      else
      {

        _dlg_rc2 =
          frm_message.Show(
            string.Format(Resource.String("STR_RECEPTION_VIEW_DIRTY_FORM"), _model.HolderFullName, Environment.NewLine),
            Resource.String("STR_APP_GEN_MSG_WARNING"), MessageBoxButtons.YesNoCancel, Images.CashierImage.Warning,
            (Form)View, Resource.String("STR_FORMAT_GENERAL_BUTTONS_NO"));
      }
      switch (_dlg_rc2)
      {
        case DialogResult.OK:
          {
            ErrorDisplay.StopTimerAndResetLabels();

            List<InvalidField> _errors = SavePlayer();

            if (_errors.Count > 0)
            {
              ErrorDisplay.DisplayErrors(_errors);
              return true;
            }
          }
          break;
        case DialogResult.Cancel:
          return true;
        case DialogResult.No:
          Model = LastModel;
          return false;
      }
      return false;
    }

    /// <summary>
    ///   helper method to bind a collection to a combo,
    ///   this creates a new instance of the collection
    ///   so referencial linking is does not create a link
    ///   between visual elements
    /// </summary>
    /// <typeparam name="TU"></typeparam>
    /// <param name="List"></param>
    /// <param name="Combo"></param>
    public static void BindDatasourceToCombo<TU>(List<TU> List, uc_auto_combobox Combo) where TU : ISelectableOption
    {
      Combo.DataSource = DatasourceFromList(List);
      Combo.ValueMember = "Id";
      Combo.DisplayMember = "Name";
      Combo.AllowUnlistedValues = false;
      Combo.FormatValidator = new CharacterTextBox();
    }

    public static void BindDatasourceToCombo<TU>(List<TU> List, uc_round_auto_combobox Combo)
      where TU : ISelectableOption
    {

      Combo.DataSource = DatasourceFromList(List);
      Combo.ValueMember = "Id";
      Combo.DisplayMember = "Name";
      
      Combo.AllowUnlistedValues = false;
      Combo.FormatValidator = new CharacterTextBox();
    }

    /// <summary>
    ///   helper method to bind a collection to a combo,
    ///   this creates a new instance of the collection
    ///   so referencial linking is does not create a link
    ///   between visual elements
    /// </summary>
    /// <typeparam name="TU"></typeparam>
    /// <param name="List"></param>
    /// <param name="Combo"></param>
    public static void BindDatasourceToCombo<TU>(List<TU> List, ComboBox Combo) where TU : ISelectableOption
    {
      Combo.DataSource = DatasourceFromList(List);
      Combo.ValueMember = "Id";
      Combo.DisplayMember = "Name";
    }

    public static void BindDatasourceToCombo<TU>(List<TU> List, uc_round_combobox Combo) where TU : ISelectableOption
    {
      Combo.DataSource = DatasourceFromList(List);
      Combo.ValueMember = "Id";
      Combo.DisplayMember = "Name";
    }

    /// <summary>
    ///   method used primarily by bind datasource to combo
    ///   creates a clone of the list to avoid referenced linking of controls
    /// </summary>
    /// <typeparam name="TU"></typeparam>
    /// <param name="List"></param>
    /// <returns></returns>
    public static List<TU> DatasourceFromList<TU>(List<TU> List) where TU : ISelectableOption
    {
      List<TU> _new_ds = new List<TU>();
      if (List == null)
      {
        return _new_ds;
      }
      foreach (var _selectable_option in List)
      {
        _new_ds.Add(_selectable_option);
      }
      return _new_ds;
    }
    public static Dictionary<int, string> DictionaryFromList<TU>(List<TU> List) where TU : ISelectableOption
    {
      Dictionary<int, string> _new_ds = new Dictionary<int, string>();
      if (List == null)
      {
        return _new_ds;
      }
      foreach (var _selectable_option in List)
      {
        _new_ds.Add(_selectable_option.Id, _selectable_option.Name);
      }
      return _new_ds;
    }

    /// <summary>
    ///   minimal base funcionality to update a card data from a model
    /// </summary>
    /// <param name="CrdData"></param>
    /// <param name="Mdl"></param>
    /// <returns></returns>
    protected CardData UpdateCardFromModelCommon(CardData CrdData, T Mdl)
    {
      CardData.PlayerTrackingData _player = CrdData.PlayerTracking;
      _player.PinFailures = Mdl.PinFailures;
      _player.HolderMaritalStatus = (Mdl.HolderMaritalStatus ?? new MaritalStatus { Id = 0, Name = String.Empty }).Id;
      _player.HolderScannedDocs = Mdl.HolderDocScan;
      _player.HolderAddressValidation = Mdl.HolderAddressValidation;
      _player.HolderAddress01 = Mdl.HolderAddressLine01;
      _player.HolderAddress02 = Mdl.HolderAddressLine02;
      _player.HolderAddress03 = Mdl.HolderAddressLine03;
      _player.HolderAddressCountry = (Mdl.HolderAddressCountry ?? new Country { Id = 0 }).Id;

      if (_player.HolderAddressCountry == -1)
      {
        _player.HolderAddressCountry = 0;
      }

      _player.HolderEmail01 = Mdl.HolderEmail1;
      _player.HolderBirthDate = Mdl.HolderBirthDate ?? DateTime.MinValue;

      if (_player.HolderBirthDate != DateTime.MinValue)
      {
        _player.HolderBirthDateText = _player.HolderBirthDate.ToShortDateString();
      }

      _player.HolderBirthCountry = (Mdl.HolderBirthCountry ?? new Country { Id = 0 }).Id;
      _player.HolderIdType =
        (ACCOUNT_HOLDER_ID_TYPE)(Mdl.HolderDocumentType ?? new DocumentType { Id = 0, Name = "" }).Id;
      _player.HolderId = Mdl.HolderDocument;

      _player.HolderZip = Mdl.HolderZip;
      _player.HolderFedEntity = (Mdl.HolderFedEntity ?? new State { Id = 0 }).Id;
      _player.HolderPhone02 = Mdl.HolderPhone2;
      _player.HolderPhone01 = Mdl.HolderPhone1;
      _player.HolderExtNumber = Mdl.HolderExtNum;
      _player.HolderGender = (Mdl.HolderGender ?? new Gender { Id = 0 }).Id;
      _player.HolderComments = Mdl.Comments;
      _player.ShowCommentsOnCashier = Mdl.ShowComments;
      _player.HolderCity = Mdl.HolderCity;
      _player.HolderNationality = (Mdl.HolderNationality ?? new Nationality { Id = 0 }).Id;
      _player.HolderName1 = Mdl.HolderName1;
      _player.HolderName2 = Mdl.HolderName2;
      _player.HolderName3 = Mdl.HolderName3;
      _player.HolderName4 = Mdl.HolderName4;
      _player.HolderName = Mdl.HolderFullName;
      _player.HolderId3Type = Mdl.HolderDocument3Type;
      _player.BeneficiaryId3Type = Mdl.BeneficiaryDocument3Type;
      _player.HolderTwitter = Mdl.HolderTwitterAccount;
      return CrdData;
    }

    /// <summary>
    ///   cuntionality to load data into the plus assign events and filters
    /// </summary>
    /// <param name="CrdData"></param>
    /// <param name="PassedView"></param>
    protected void LoadView(CardData CrdData, IMVCView<T> PassedView)
    {
      CrdData.LoadAccountDocuments();
      CardData = CrdData;
      View = PassedView;
      View.VisibleFields.HideControls();

      Model = CreateModelFromCardData(CardData);
      View.Controller = this;
      ThreadPool.QueueUserWorkItem(delegate { DelayedAccountPhotoLoad(CrdData.AccountId); });
    }

    private IDScanner.IDScanner m_idscanner;


    public void HandleIDScannerEvent(IDScannerInfoEvent Ev)
    {

      T _model = (View as Control).GetControlPropertyThreadSafe("Model") as T;

      IDScannerInfo _info = Ev.info;

      ApplyIDScannerInfoToModel(_info, _model);
      (View as Control).SetControlPropertyThreadSafe("Model", _model);
    }

    protected void ApplyIDScannerInfoToModel(IDScannerInfo Info, T Mdl)
    {
      bool _is_front = GetBooleanField(Info.Fields, Info.IDScannerFixedFieldMapping[ENUM_IDScannerFixedField.IS_FRONT],
        false);
      if (Info.Fields.ContainsKey("PHOTO") && _is_front)
      {
        string _filename = Info.Fields["PHOTO"] as string;
        if (!string.IsNullOrEmpty(_filename) && File.Exists(_filename))
        {
          Image _img;
          using (var _bmp_temp = new Bitmap(_filename))
          {
            _img = new Bitmap(_bmp_temp);
          }
          PhotoHandler(new CameraPhotoEvent
          {
            Image = _img
          });
        }
      }
      Mdl.HolderName1 = GetStringField(Info.Fields, Info.IDScannerFixedFieldMapping[ENUM_IDScannerFixedField.SURNAME],
        Mdl.HolderName1);
      Mdl.HolderName2 = GetStringField(Info.Fields, Info.IDScannerFixedFieldMapping[ENUM_IDScannerFixedField.SURNAME2],
        Mdl.HolderName2);
      Mdl.HolderName3 = GetStringField(Info.Fields, Info.IDScannerFixedFieldMapping[ENUM_IDScannerFixedField.NAME],
        Mdl.HolderName3);
      Mdl.HolderName4 = GetStringField(Info.Fields, Info.IDScannerFixedFieldMapping[ENUM_IDScannerFixedField.NAME2],
        Mdl.HolderName4);

      Mdl.HolderFullName = ReFormatFullNameName(new[] { Mdl.HolderName3, Mdl.HolderName4, Mdl.HolderName1, Mdl.HolderName2 });
      Mdl.HolderBirthDate = GetDateField(Info.Fields,
        Info.IDScannerFixedFieldMapping[ENUM_IDScannerFixedField.DATE_OF_BIRTH],
        Mdl.HolderBirthDate ?? default(DateTime));
      Mdl.HolderNationality = GetSelectableOption(Mdl.Nationalities,
        GetIntField(Info.Fields, Info.IDScannerFixedFieldMapping[ENUM_IDScannerFixedField.NATIONALITY]), "");
      Mdl.HolderBirthCountry = GetSelectableOption(Mdl.Countries,
        GetIntField(Info.Fields, Info.IDScannerFixedFieldMapping[ENUM_IDScannerFixedField.BIRTHPLACE]), "");

      Mdl.HolderGender = GetSelectableOption(Mdl.Genders,
        GetIntField(Info.Fields, Info.IDScannerFixedFieldMapping[ENUM_IDScannerFixedField.SEX],
          (Mdl.HolderGender ?? new Gender() { Id = 0 }).Id), "");
      int _id_type = (Mdl.HolderDocumentType ?? new DocumentType() { Id = 0 }).Id;
      if (Info.Fields.ContainsKey(Info.IDScannerFixedFieldMapping[ENUM_IDScannerFixedField.ID_TYPE]))
      {
        ENUM_DetectableIDClass _found_doc_type = GetEnumField<ENUM_DetectableIDClass>(Info.Fields,
          Info.IDScannerFixedFieldMapping[ENUM_IDScannerFixedField.ID_TYPE]);

        _id_type = IdentificationTypes.ScannedTypeToID(_found_doc_type.ToString());
      }
      string _docnum = GetStringField(Info.Fields, Info.IDScannerFixedFieldMapping[ENUM_IDScannerFixedField.ID_NUMBER]);

      if (!string.IsNullOrEmpty(_docnum) && _id_type == 0)
      {
        _id_type = Misc.DefaultDocumentTypeId();

      }

      Mdl.HolderDocumentType = GetSelectableOption(Mdl.DocumentTypes, _id_type, "");
      if (Mdl.IDDocuments.ContainsKey(_id_type))
        Mdl.IDDocuments[_id_type] = GetStringField(Info.Fields,
          Info.IDScannerFixedFieldMapping[ENUM_IDScannerFixedField.ID_NUMBER]);
      else
        Mdl.IDDocuments.Add(_id_type, GetStringField(Info.Fields,
          Info.IDScannerFixedFieldMapping[ENUM_IDScannerFixedField.ID_NUMBER]));


      Mdl.HolderDocument = GetStringField(Info.Fields,
  Info.IDScannerFixedFieldMapping[ENUM_IDScannerFixedField.ID_NUMBER], Mdl.HolderDocument);


      Mdl.HolderAddressLine01 = GetStringField(Info.Fields,
        Info.IDScannerFixedFieldMapping[ENUM_IDScannerFixedField.ADDRESS], Mdl.HolderAddressLine01);
      Mdl.HolderCity = GetStringField(Info.Fields, Info.IDScannerFixedFieldMapping[ENUM_IDScannerFixedField.CITY],
        Mdl.HolderCity);
      try
      {
        Mdl.HolderAddressLine03 = GetStringField(Info.Fields,
          Info.IDScannerFixedFieldMapping[ENUM_IDScannerFixedField.COUNTY],
          Mdl.HolderAddressLine03);
      }
      catch
      {
        //swallow
      }
      try
      {
        string _ctryiso = GetStringField(Info.Fields, Info.IDScannerFixedFieldMapping[ENUM_IDScannerFixedField.COUNTRY],
          "");
        if (!string.IsNullOrEmpty(_ctryiso))
        {
          int _cntry_id = 0;

          Countries.ISO2ToCountryId(_ctryiso, out _cntry_id);

          if (_cntry_id > 0)
          {
            Mdl.HolderAddressCountry = new Country() { Id = _cntry_id };
          }

        }
      }
      catch
      {
        //swallow
      }

      try
      {
        string _statestr = GetStringField(Info.Fields,
            Info.IDScannerFixedFieldMapping[ENUM_IDScannerFixedField.STATE],
            "");
        if (!string.IsNullOrEmpty(_statestr))
        {
          string _ctryiso = GetStringField(Info.Fields,
            Info.IDScannerFixedFieldMapping[ENUM_IDScannerFixedField.COUNTRY],
            "");
          if (!string.IsNullOrEmpty(_ctryiso))
          {
            DataTable _dt = null;
            States.GetStates(out _dt, _ctryiso);
            DataRow[] _dt2 = _dt.Select("FS_NAME='" + ConvertToNamedUsState(_statestr) + "'");
            int _state_id = 0;

            if (_dt2.Length > 0)
              _state_id = (int)_dt2[0]["FS_STATE_ID"];

            if (_state_id > 0)
            {
              Mdl.HolderFedEntity = new State() { Id = _state_id };
            }

          }
        }
      }
      catch
      {
        //swallow
      }


      Mdl.HolderZip = GetStringField(Info.Fields, Info.IDScannerFixedFieldMapping[ENUM_IDScannerFixedField.ZIP],
        Mdl.HolderZip);
      if (Info.Fields.ContainsKey(Info.IDScannerFixedFieldMapping[ENUM_IDScannerFixedField.FRONT]) && _is_front)
      {
        string _filename = Info.Fields[Info.IDScannerFixedFieldMapping[ENUM_IDScannerFixedField.FRONT]] as string;
        if (!string.IsNullOrEmpty(_filename) && File.Exists(_filename))
        {
          Image _img;
          using (var _bmp_temp = new Bitmap(_filename))
          {
            _img = new Bitmap(_bmp_temp);
          }

          SaveIdScanImage(_img,
            _id_type,
            GetDateField(Info.Fields, Info.IDScannerFixedFieldMapping[ENUM_IDScannerFixedField.DOCUMENT_EXPIRY]) ??
            default(DateTime),
            Mdl);
        }

      }
    }

    private string ConvertToNamedUsState(string _statestr)
    {
      Dictionary<string, string> states = new Dictionary<string, string>()
      {
        {"AL", "Alabama"},
        {"AK", "Alaska"},
        {"AZ", "Arizona"},
        {"AR", "Arkansas"},
        {"CA", "California"},
        {"CO", "Colorado"},
        {"CT", "Connecticut"},
        {"DE", "Delaware"},
        {"FL", "Florida"},
        {"GA", "Georgia"},
        {"HI", "Hawaii"},
        {"ID", "Idaho"},
        {"IL", "Illinois"},
        {"IN", "Indiana"},
        {"IA", "Iowa"},
        {"KS", "Kansas"},
        {"KY", "Kentucky"},
        {"LA", "Louisiana"},
        {"ME", "Maine"},
        {"MD", "Maryland"},
        {"MA", "Massachusetts"},
        {"MI", "Michigan"},
        {"MN", "Minnesota"},
        {"MS", "Mississippi"},
        {"MO", "Missouri"},
        {"MT", "Montana"},
        {"NE", "Nebraska"},
        {"NV", "Nevada"},
        {"NH", "New Hampshire"},
        {"NJ", "New Jersey"},
        {"NM", "New Mexico"},
        {"NY", "New York"},
        {"NC", "North Carolina"},
        {"ND", "North Dakota"},
        {"OH", "Ohio"},
        {"OK", "Oklahoma"},
        {"OR", "Oregon"},
        {"PA", "Pennsylvania"},
        {"RI", "Rhode Island"},
        {"SC", "South Carolina"},
        {"SD", "South Dakota"},
        {"TN", "Tennessee"},
        {"TX", "Texas"},
        {"UT", "Utah"},
        {"VT", "Vermont"},
        {"VA", "Virginia"},
        {"WA", "Washington"},
        {"WV", "West Virginia"},
        {"WI", "Wisconsin"},
        {"WY", "Wyoming"},
        {"AS", "American Samoa"},
        {"DC", "District of Columbia"},
        {"FM", "Federated States of Micronesia"},
        {"GU", "Guam"},
        {"MH", "Marshall Islands"},
        {"MP", "Northern Mariana Islands"},
        {"PW", "Palau"},
        {"PR", "Puerto Rico"},
        {"VI", "Virgin Islands"}
      };

      if (states.ContainsKey(_statestr)) return states[_statestr];
      return "";

    }

    private bool GetBooleanField(Dictionary<string, object> Fields, string FieldName, bool Default)
    {
      if (!Fields.ContainsKey(FieldName)) return Default;

      bool _result;
      return bool.TryParse(Fields[FieldName].ToString(), out _result) ? _result : Default;
    }

    private TE GetEnumField<TE>(Dictionary<string, object> Fields, string FieldName)
    {
      TE _retval = default(TE);
      if (!Fields.ContainsKey(FieldName))
      {
        return _retval;
      }
      _retval = (TE)Fields[FieldName];
      return _retval;
    }

    private int GetIntField(Dictionary<string, object> Fields, string FieldName, int Default = default(int))
    {
      if (!Fields.ContainsKey(FieldName) || !(Fields[FieldName] as int?).HasValue)
        return Default;
      return (int)Fields[FieldName];
    }

    private DateTime? GetDateField(Dictionary<string, object> Fields, string FieldName, DateTime Default = default(DateTime))
    {
      if (!Fields.ContainsKey(FieldName) || !(Fields[FieldName] as DateTime?).HasValue)
        return Default;
      return Fields[FieldName] as DateTime?;
    }

    private string GetStringField(Dictionary<string, object> Fields, string FieldName, string Default = "")
    {
      if (!Fields.ContainsKey(FieldName) || string.IsNullOrEmpty(Fields[FieldName] as string))
        return Default;
      return Fields[FieldName] as string;
    }

    protected bool PlayerDataHasChanges(CardData.PlayerTrackingData Old, CardData.PlayerTrackingData New)
    {
      Dictionary<string, Variance> _variance_player_tracking = Old.DetailedCompare(New);
      return _variance_player_tracking.Count > 0;
    }

    /// <summary>
    /// Calls cardData to validate if ID already Exist
    /// </summary>
    public List<InvalidField> CheckIdAlreadyExists(bool _bSave)
    {
      DialogResult _rc;
      List<InvalidField> _errors = new List<InvalidField>();
      var _model = Model;

      if (_model == null || _model.HolderDocument == null ||
          ((_model.HolderDocumentType ?? new DocumentType() { Id = 0 }).Id <= 0)) return _errors;
      if (CardData.CheckIdAlreadyExists(_model.HolderDocument,
        _model.AccountId ?? -1,
        (ACCOUNT_HOLDER_ID_TYPE)(_model.HolderDocumentType ?? new DocumentType() { Id = 0 }).Id))
      {
        _errors.Add(new InvalidField
        {
          Id = "DocumentDuplicated",
          Reason = Resource.String("STR_HOLDER_ID_ALREADY_EXISTS")
        });

        if (!_bSave || !_AllowAccountsDuplicates)
        {
          frm_message.Show(Resource.String("STR_HOLDER_ID_ALREADY_EXISTS"), Resource.String("STR_APP_GEN_MSG_WARNING"), MessageBoxButtons.OK, ViewForm);
        }
        else
        {
          _rc = frm_message.Show(Resource.String("STR_HOLDER_ID_ALREADY_EXISTS_CONFIRMATION").Replace("\\r\\n", "\r\n"), Resource.String("STR_APP_GEN_MSG_WARNING"), MessageBoxButtons.YesNo, MessageBoxIcon.Warning, View as Form);

          if (_rc == DialogResult.Cancel)
          {
            return new List<InvalidField>() { new InvalidField() { Id = "ContinueAccountsDuplicate", Reason = "0" } };
          }
          else
          {
            return new List<InvalidField>() { new InvalidField() { Id = "ContinueAccountsDuplicate", Reason = "1" } };
          }
        }
        _showndupdetails = true;
      }

      return _errors;
    }

    /// <summary>
    /// Calls cardData to validate if Account already Exist based on Account.UniqueFields
    /// </summary>

    public abstract List<InvalidField> CheckDuplicateAccounts();

    public List<InvalidField> Save(List<InvalidField> _errors)
    {
      return Save(_errors, false);

    }

    public List<InvalidField> Save(bool _print = false)
    {
      return Save(new List<InvalidField>(), _print);
    }

    private KeyValuePair<int, string> FirstNonBlankId(Dictionary<int, string> ids)
    {
      KeyValuePair<int, string> _retval = new KeyValuePair<int, string>(-999, "");
      foreach (var _id in ids)
      {
        if (!string.IsNullOrEmpty(_id.Value))
        {
          _retval = _id;
          break;
        }
      }
      return _retval;
    }

    /// <summary>
    ///   routine to save changes to database, can call extended funcionality in
    ///   SaveExtention delegate, returns a list of errors for display using the
    ///   target forms display error procedure
    /// </summary>
    /// <returns></returns>
    public List<InvalidField> Save(List<InvalidField> _errors, bool PrintInformation)
    {
      bool _saved_ok;

      T _model;
      _model = Model;


      //  if (_model.DuplicatedID && _model.AccountId < 1)
      //  {
      //    _dlgres =
      //      frm_message.Show(Resource.String("STR_HOLDER_ID_ALREADY_EXISTS_CONFIRMATION").Replace("\\r\\n", "\r\n"), Resource.String("STR_APP_GEN_MSG_WARNING"), MessageBoxButtons.YesNo,
      //        MessageBoxIcon.Question, ViewForm, true);

      //  }
      //  else if (_model.DuplicatedDetails && _model.AccountId < 1)
      //  {
      //    _dlgres =
      //      frm_message.Show(Resource.String("STR_DUPLICATE_ACCOUNTS_CONFIRMATION_NO_RESUME").Replace("\\r\\n", "\r\n"), Resource.String("STR_APP_GEN_MSG_WARNING"), MessageBoxButtons.YesNo, MessageBoxIcon.Question, ViewForm, true);
      //  }
      //  if (_dlgres != DialogResult.OK)
      //  {
      //    return new List<InvalidField>()
      //    {
      //      new InvalidField
      //      {
      //        Id = "DocumentDuplicated",
      //        Reason = Resource.String("STR_DUPLICATE_ACCOUNTS_WARNING")
      //          .Replace("@p0", "")
      //          .Replace("\\n", "")
      //          .Replace("\\r", "")
      //          .Replace(":", "").Trim()
      //      }
      //    };
      //  }
      //}

      var _firstnonblankId = FirstNonBlankId(_model.IDDocuments);
      if (string.IsNullOrEmpty(_model.HolderDocument) && !string.IsNullOrEmpty(_firstnonblankId.Value))
      {
        _model.HolderDocument = _firstnonblankId.Value;
        _model.HolderDocumentType = _model.DocumentTypes.Find(x => x.Id == _firstnonblankId.Key);
      }
      CardData _card_data = UpdateCardFromModel((CardData)CardData.Clone(), _model);
      bool _player_data_changed = PlayerDataHasChanges(CardData.PlayerTracking, _card_data.PlayerTracking);



      _errors.AddRange(Validate(_model));
      if (_errors.Count != 0)
      {
        Model = _model;
        return _errors;
      }

      // Avoid to save data Payment Register
      if (this.ScreenMode == PlayerEditDetailsCashierView.SCREEN_MODE.REGISTER_CODERE)
      {
        CardData = _card_data;
        return _errors;
      }

      if (AntiMoneyLaunderingEnabled)
      {
        List<InvalidField> _antilaundering_errors = Validate(_model, true);
        if (_antilaundering_errors.Count > 0)
        {
          string _message;
          _message =
            Resource.String(
              PrintInformation ? "STR_FRM_ACCOUNT_USER_EDIT_PRINT_DATA_AML" : "STR_ANTIMONEYLAUNDERING_MANDATORY_FIELDS",
              Accounts.getAMLName());

          _message = _message.Replace("\\r\\n", "\r\n");
          var _dlg_rc = frm_message.Show(_message,
            Resource.String("STR_ANTIMONEYLAUNDERING_MANDATORY_FIELDS_CAPTION", Accounts.getAMLName()),
            MessageBoxButtons.YesNo, Images.CashierImage.Warning, View as Form);

          if (_dlg_rc != DialogResult.OK)
          {
            return new List<InvalidField>
              {
                new InvalidField
                  {
                    Id = "",
                    Reason = Resource.String("STR_ANTIMONEYLAUNDERING_MANDATORY_FIELDS_CAPTION", Accounts.getAMLName())
                  }
              };
          }
        }
      }
      using (DB_TRX _db_trx = new DB_TRX())
      {
        if (_card_data.AccountId <= 0) //Creating New Account
        {
          Int64 _account_id;
          String _duplicate_msg;
          Boolean _allow_duplicate;
          String _error_str;
          DialogResult _question_dialog;

          CardData.ContainNewAccount = true;
          _card_data.ContainNewAccount = true;

          if (ViewForm is PlayerEditDetailsCashierView)
          {
            if (!CardData.CheckDuplicateAccounts(_card_data, out _duplicate_msg, out _allow_duplicate, _db_trx.SqlTransaction))
            {
              if (_allow_duplicate)
              {
                _question_dialog = frm_message.Show(_duplicate_msg,
                  Resource.String("STR_APP_GEN_MSG_WARNING"),
                  MessageBoxButtons.YesNo,
                  MessageBoxIcon.Warning,
                  (Form)View);

                if (_question_dialog == DialogResult.Cancel)
                {
                  return new List<InvalidField>
                  {
                    new InvalidField
                    {
                      Id = "",
                      Reason = _duplicate_msg.Replace(":", "")
                    }
                  };
                }
                if (!ProfilePermissions.CheckPermissionList(ProfilePermissions.CashierFormFuncionality.DuplicateAccount,
                  ProfilePermissions.TypeOperation.RequestPasswd,
                  (Form)View, 0,
                  out _error_str))
                {
                  return new List<InvalidField>
                  {
                    new InvalidField
                    {
                      Id = "",
                      Reason = _error_str.Replace(":", "")
                    }
                  };

                }
              }
              else
              {
                return new List<InvalidField>
                {
                  new InvalidField
                  {
                    Id = "",
                    Reason = _duplicate_msg.Replace(":", "")
                  }
                };

              }

            }
          } // Creates CardData into DB

          if (CashierBusinessLogic.DB_CreateCard(_card_data.TrackData, out _account_id, _db_trx.SqlTransaction))
          {
            bool _card_paid;
            if (CashierBusinessLogic.DB_UpdateCardPayAfterCreated(_account_id, ACCOUNT_USER_TYPE.PERSONAL,
              out _card_paid, _db_trx.SqlTransaction))
            {
              _card_data.CardPaid = _card_paid;
            }

            // Set the new account id.
            _card_data.AccountId = _account_id;
            _model.AccountId = _account_id;
          }
        }

        _saved_ok = false;
        bool _holder_docs_changed = false;
        bool _beneficiary_docs_changed = false;

        var _mdl = _model as PlayerEditDetailsCashierModel;
        if (_mdl != null)
        {
          _holder_docs_changed = _mdl.HolderDocsChanged;
          _beneficiary_docs_changed = _mdl.BeneficiaryDocsChanged;
        }
        if (_card_data.AccountId != 0 && _player_data_changed)
        {
          _saved_ok = CashierBusinessLogic.DB_UpdateCardholder(_card_data, CardData,
            _holder_docs_changed || _beneficiary_docs_changed, false, _db_trx.SqlTransaction);
        }
        if (_saved_ok || !_player_data_changed)
        {
          //check id photo changed
          if (_model.PhotoChanged)
          {
            AccountPhotoFunctions.SaveAccountPhoto(_card_data.AccountId, _model.HolderPhoto, _db_trx.SqlTransaction);
            _model.PhotoChanged = false;
          }
          if (SaveExtention != null)
          {
            _errors.AddRange(SaveExtention.Invoke(_model, _db_trx.SqlTransaction));
          }
          if (_errors.Count != 0)
          {
            return _errors;
          }
          _db_trx.Commit();
          AuditPlayerData(CardData, _card_data);
          CardData = _card_data;
        }
        else
        {
          _errors.Add(new InvalidField { Id = "", Reason = "Critical error" });
        }
      }

      _model.IsNew = _saved_ok;
      Model = _model;
      if (ViewForm is PlayerEditDetailsReceptionView)
      {
        (ViewForm as PlayerEditDetailsReceptionView).Model = _model as PlayerEditDetailsReceptionModel;
      }
      if (ViewForm is PlayerEditDetailsCashierView)
      {
        (ViewForm as PlayerEditDetailsCashierView).Model = _model as PlayerEditDetailsCashierModel;
      }
      m_has_changes_printed = _player_data_changed;

      return _errors;
    }

    public List<InvalidField> Validate(T Mdl, bool LaunderingCheck = false)
    {
      LoadHiddenFields();

      LoadAntiMoneyLaunderingFields();

      LoadRequiredFields();

      return Mdl.Validator.Validate(m_hidden_fields, !LaunderingCheck ? m_required_fields : m_anti_laundering_fields);
    }

    private void LoadHiddenFields()
    {
      if (m_hidden_fields != null)
      {
        return;
      }
      m_hidden_fields = new List<string>();
      foreach (var _field in View.VisibleFields.Controls)
      {
        var _arobj = _field.Value as object[];
        if (_arobj != null)
        {
          if (_arobj.Length != 2)
          {
            continue;
          }
          if (!((Control)_arobj[1]).Enabled)
          {
            m_hidden_fields.Add(((Control)_arobj[1]).Tag as string);
          }
        }
      }
    }

    private void LoadRequiredFields()
    {
      if (m_required_fields != null)
      {
        return;
      }
      m_required_fields = new List<string>();
      m_required_fields.Add("DocumentDuplicated");
      var _param_dictionary = GeneralParam.GetDictionary();
      foreach (var _param in _param_dictionary)
      {
        string _key = "";
        if (_param.Key.StartsWith("Account.RequestedField.") &&
            !_param.Key.StartsWith("Account.RequestedField.AntiMoneyLaundering") && _param.Value == "1")
        {
          _key = "Holder" + _param.Key.Replace("Account.RequestedField.", "").Replace("AntiMoneyLaundering.", "");
        }
        if (this.ScreenMode != PlayerEditDetailsCashierView.SCREEN_MODE.REGISTER_CODERE && _param.Key.StartsWith("Beneficiary.RequestedField.") &&
            !_param.Key.StartsWith("Beneficiary.RequestedField.AntiMoneyLaundering") && _param.Value == "1")
        {
          _key = "Beneficiary" +
                 _param.Key.Replace("Beneficiary.RequestedField.", "").Replace("AntiMoneyLaundering.", "");
        }
        if (_key != "" && !m_required_fields.Contains(_key))
        {
          m_required_fields.Add(_key);
        }
      }
      if (IdentificationTypes.GetIdentificationTypes().ContainsKey((Int32)ACCOUNT_HOLDER_ID_TYPE.RFC) ||
           IdentificationTypes.GetIdentificationTypes().ContainsKey((Int32)ACCOUNT_HOLDER_ID_TYPE.CURP))
      {
        if (GeneralParam.GetString("Account.RequestedField", "DocumentTypeList", "").Contains("001"))
          m_required_fields.Add("HolderRFC");
        if (
          GeneralParam.GetString("Beneficiary.RequestedField", "DocumentTypeList", "")
            .Contains("001"))
          m_required_fields.Add("BeneficiaryRFC");
        if (GeneralParam.GetString("Account.RequestedField", "DocumentTypeList", "").Contains("002"))
          m_required_fields.Add("HolderCURP");
        if (
          GeneralParam.GetString("Beneficiary.RequestedField", "DocumentTypeList", "")
            .Contains("002"))
          m_required_fields.Add("BeneficiaryCURP");
      }

      if(this.ScreenMode == PlayerEditDetailsCashierView.SCREEN_MODE.REGISTER_CODERE)
      {
        List<String> _no_visible_fields = new List<string> { "HolderEmail1", "HolderEmail2", "HolderPhone1", "HolderPhone2", "HolderTwitterAccount", "BeneficiaryRFC", "BeneficiaryCURP" };

        foreach (String _field in _no_visible_fields)
        {
          m_required_fields.Remove(_field);
        }
      }
    }

    private void LoadAntiMoneyLaunderingFields()
    {
      if (m_anti_laundering_fields != null)
      {
        return;
      }
      m_anti_laundering_fields = new List<string>();

      var _param_dictionary = GeneralParam.GetDictionary();
      foreach (var _param in _param_dictionary)
      {
        string _key = "";
        if (_param.Key.StartsWith("Account.RequestedField.AntiMoneyLaundering") && _param.Value == "1")
        {
          _key = "Holder" + _param.Key.Replace("Account.RequestedField.", "").Replace("AntiMoneyLaundering.", "");
        }
        if (_param.Key.StartsWith("Beneficiary.RequestedField.AntiMoneyLaundering") && _param.Value == "1")
        {
          _key = "Beneficiary" +
                 _param.Key.Replace("Beneficiary.RequestedField.", "").Replace("AntiMoneyLaundering.", "");
        }
        if (_key != "" && !m_anti_laundering_fields.Contains(_key))
        {
          m_anti_laundering_fields.Add(_key);
        }
      }
      if (IdentificationTypes.GetIdentificationTypes().ContainsKey((Int32)ACCOUNT_HOLDER_ID_TYPE.RFC) ||
          IdentificationTypes.GetIdentificationTypes().ContainsKey((Int32)ACCOUNT_HOLDER_ID_TYPE.CURP))
      {
        if (GeneralParam.GetString("Account.RequestedField.AntiMoneyLaundering", "DocumentTypeList", "").Contains("001"))
          m_anti_laundering_fields.Add("HolderRFC");
        if (
          GeneralParam.GetString("Beneficiary.RequestedField.AntiMoneyLaundering", "DocumentTypeList", "")
            .Contains("001"))
          m_anti_laundering_fields.Add("BeneficiaryRFC");
        if (GeneralParam.GetString("Account.RequestedField.AntiMoneyLaundering", "DocumentTypeList", "").Contains("002"))
          m_anti_laundering_fields.Add("HolderCURP");
        if (
          GeneralParam.GetString("Beneficiary.RequestedField.AntiMoneyLaundering", "DocumentTypeList", "")
            .Contains("002"))
          m_anti_laundering_fields.Add("BeneficiaryCURP");
      }

    }

    /// <summary>
    ///   method used to audit player data to get the localized name for the filed being saved
    /// </summary>
    /// <param name="FieldName"></param>
    /// <returns></returns>
    private string GetLocalisedResourseNameForField(string FieldName)
    {
      string _translation;
      return m_localized_descriptions.TryGetValue(FieldName, out _translation) ? _translation : FieldName;
    }

    /// <summary>
    ///   routine used by audit player data to create a description of the
    ///   change that will be redorded in the audit trail
    /// </summary>
    /// <param name="Value"></param>
    /// <param name="Type"></param>
    /// <returns></returns>
    private string CreateVarianceDescription(object Value,string Type)
    {
      if (Value == null)
      {
        return Resource.String("STR_AUDIT_NONE_STRING");
      }
      string _value = null;
      switch (Type)
      {
        case "HolderGender":
        case "BeneficiaryGender":
          _value = CardData.GenderString((int) Value);
          break;
        case "HolderMaritalStatus":
          _value = CardData.MaritalStatusString((ACCOUNT_MARITAL_STATUS) System.Convert.ToInt32( Value));
          break;
        case "HolderIdType":
          _value = IdentificationTypes.DocIdTypeString(System.Convert.ToInt32( Value));
          break;
        case "BeneficiaryOccupationId":
        case "HolderOccupationId":
          _value = CardData.OccupationsDescriptionString(System.Convert.ToInt32( Value));
          break;
        case "HolderNationality":
          _value = CardData.NationalitiesString(System.Convert.ToInt32(Value));
          break;
        case "HolderBirthCountry":
        case "HolderAddressCountry":
          _value = CardData.CountriesString(System.Convert.ToInt32(Value));
          break;
        case "HolderFedEntity":
          _value = CardData.FedEntitiesString(System.Convert.ToInt32( Value));
          break;
        case "HolderHasBeneficiary":
          _value = CardData.GetBoolString(System.Convert.ToBoolean(Value));
          break;

        default:
          _value = Value.ToString() ;

          break;
      }
      return string.IsNullOrEmpty(_value) ? Resource.String("STR_AUDIT_NONE_STRING") : _value;
    }

    /// <summary>
    ///   routine to record changes in player account data, streamline from original 500 line routine to be more dynamic
    /// </summary>
    /// <param name="OldCardData"></param>
    /// <param name="NewCardData"></param>
    private void AuditPlayerData(CardData OldCardData, CardData NewCardData)
    {
      Auditor _auditor;

      _auditor = new Auditor(AUDIT_CODE_ACCOUNTS);
      if (OldCardData.ContainNewAccount)
        _auditor.SetName(AUDIT_NEW_TITLE_ID, Resource.String("STR_FORMAT_GENERAL_NAME_ACCOUNT"),
          NewCardData.AccountId + " - " + NewCardData.PlayerTracking.HolderName, "", "", "");
      else
      _auditor.SetName(AUDIT_MODIFY_TITLE_ID, Resource.String("STR_FORMAT_GENERAL_NAME_ACCOUNT"),
        NewCardData.AccountId + " - " + NewCardData.PlayerTracking.HolderName, "", "", "");

      Dictionary<string, Variance> _variance = OldCardData.DetailedCompare(NewCardData);
      _variance.Remove("Cancellable");
      _variance.Remove("NumberOfPromosByCreditType");
      _variance.Remove("NumberOfCoverCouponByCreditType");
      _variance.Remove("PlayerTracking");
      _variance.Remove("CurrentPlaySession");
      _variance.Remove("IsNew");
      _variance.Remove("AccountId");
      _variance.Remove("CardPaid");

      Dictionary<string, Variance> _variance_player_tracking =
        OldCardData.PlayerTracking.DetailedCompare(NewCardData.PlayerTracking);
      Dictionary<string, Variance> _variance_cancellable =
        OldCardData.Cancellable.DetailedCompare(NewCardData.Cancellable);
      _variance_cancellable.Remove("Operations");
      _variance_player_tracking.Remove("PreferredHolderId");
      _variance_player_tracking.Remove("AccountId");
      _variance_player_tracking.Remove("PreferredHolderIdType");
      _variance_player_tracking.Remove("CardPaid");
      _variance_player_tracking.Remove("HolderBirthDate");
      _variance_player_tracking.Remove("BeneficiaryBirthDate");
      _variance_player_tracking.Remove("HolderWeddingDate");

      foreach (var _field_variance in _variance)
      {
        _auditor.SetField(OldCardData.ContainNewAccount ? AUDIT_FIELD_ID_NEW : AUDIT_FIELD_ID_MODIFY, GetLocalisedResourseNameForField(_field_variance.Key),
          CreateVarianceDescription(_field_variance.Value.ValB, _field_variance.Key), CreateVarianceDescription(_field_variance.Value.ValA, _field_variance.Key),
          "", "");
      }


      foreach (var _field_variance in _variance_player_tracking)
      {
        
        if (string.IsNullOrEmpty((_field_variance.Value.ValA??"").ToString()) && string.IsNullOrEmpty((_field_variance.Value.ValB??"").ToString()))
          continue;

        _auditor.SetField(OldCardData.ContainNewAccount ? AUDIT_FIELD_ID_NEW : AUDIT_FIELD_ID_MODIFY, GetLocalisedResourseNameForField(_field_variance.Key),
          CreateVarianceDescription(_field_variance.Value.ValB, _field_variance.Key), CreateVarianceDescription(_field_variance.Value.ValA,_field_variance.Key),
          "", "");
      }

      if (_auditor.NumFields > 1)
      {
        _auditor.Notify(ENUM_GUI.CASHIER, Cashier.UserId, Cashier.UserName, Cashier.TerminalName);
      }
    }

    /// <summary>
    ///   consumable method to toggle on screen keyboard
    /// </summary>
    public void ToggleKeyboard()
    {
      WSIKeyboard.ForceToggle();
    }

    /// <summary>
    ///   consumable method to format the full name of the player
    /// </summary>
    /// <param name="NameParts"></param>
    /// <returns></returns>
    public string ReFormatFullNameName(string[] NameParts)
    {
      switch (NameParts.Length)
      {
        case 4:
          return
            CardData.FormatFullName(FULL_NAME_TYPE.ACCOUNT, NameParts[0], NameParts[1], NameParts[2], NameParts[3])
              .Trim();
        case 3:
          return CardData.FormatFullName(FULL_NAME_TYPE.ACCOUNT, NameParts[0], NameParts[1], NameParts[2]).Trim();
      }
      string _retval = "";
      foreach (var _name_part in NameParts)
      {
        _retval += (string.IsNullOrEmpty(_retval) ? "" : " ") + _name_part;
      }
      return _retval;
    }


    /// <summary>
    ///   routine to fill common lists in model
    /// </summary>
    /// <param name="Mdl"></param>
    /// <returns></returns>
    protected T PopulateLists(T Mdl)
    {
      Mdl = AddGenders(Mdl);
      Mdl = AddNationalities(Mdl);
      Mdl = AddDocumentTypes(Mdl);
      Mdl = AddCountries(Mdl);
      Mdl = AddMartialStatuses(Mdl);

      return Mdl;
    }

    /// <summary>
    ///   comsumable method to invoke the card scanner
    /// </summary>
    public void IdScan()
    {
      if (m_idscanner != null)
      {
        using (frm_yesno _shader = new frm_yesno())
        {
          _shader.Opacity = 0.6;
          _shader.Show();
          m_idscanner.ForceScan();
        }
      }
    }

    /// <summary>
    ///   consumable method to take a photo
    /// </summary>
    public void TakePhoto()
    {
      bool _exit;
      int _tries = 0;
      _exit = false;
      while (!_exit)
      {
        try
        {
          Image _img;
          if (CameraModule == null)
          {
            CameraModule = new CameraModule((Form)View, typeof(frmCapturePhoto), typeof(SelectDevice));
          }

          if (CameraModule.EventMode)
          {
            CameraModule.RegisterHandler((Form)View, PhotoHandler);
          }

          Dialog_Cancel = true;
          _img = CameraModule.GetImage((Form)View);

          if (_img != null)
          {
            Dialog_Cancel = false;
            PhotoHandler(new CameraPhotoEvent { Image = _img });
          }

          if (!CameraModule.EventMode)
          {
            CameraModule = null;
          }
          _exit = true;
        }
        catch (CameraDeviceNotAvaiableException _ex)
        {
          Log.Error(string.Format("Camera missing error in {0}", ((Form)View).Name));
          Log.Exception(_ex);
          frm_message.Show(Resource.String("STR_CAMERA_MISSING_MESSAGE_TEXT"),
          Resource.String("STR_CAMERA_MISSING_MESSAGE_CAPTION"), MessageBoxButtons.OK, ((Form)View));
          CameraModule = null;
          if (_tries == 1)
            _exit = true;
          else
          {
            _exit = false;
            _tries = 1;
          }
        }
        catch (NoCameraDevicesAvailable _ex)
        {
          Log.Error(string.Format("No camera devices available {0}", ((Form)View).Name));
          Log.Exception(_ex);
          frm_message.Show(Resource.String("STR_CAMERA_NO_DEVICES_MESSAGE_TEXT"),
            Resource.String("STR_CAMERA_NO_DEVICES_MESSAGE_CAPTION"), MessageBoxButtons.OK, ((Form)View));
          CameraModule = null;

          _exit = true;
        }
        catch (CameraDeviceTimeoutException _ex)
        {
          _tries = 1;
          Log.Error(string.Format("Camera Timeout {0}", ((Form)View).Name));
          Log.Exception(_ex);
          frm_message.Show(Resource.String("STR_CAMERA_TIMEOUT_MESSAGE_TEXT"),
          Resource.String("STR_CAMERA_TIMEOUT_MESSAGE_CAPTION"), MessageBoxButtons.OK, ((Form)View));
          CameraModule = null;
          if (_tries == 1)
            _exit = true;
          else
          {
            _exit = false;
            _tries = 1;
          }
        }
        catch (Exception _ex)
        {
          Log.Error(string.Format("Unspecified Exception in {0}", ((Form)View).Name));
          Log.Exception(_ex);
          CameraModule = null;
          _exit = true;
        }
      }
    }

    /// <summary>
    ///   return method to hande taken photo in event photo mode
    /// </summary>
    /// <param name="Event"></param>
    private void PhotoHandler(CameraPhotoEvent Event)
    {
      if (Event.StatusFoobar)
      {
        CameraModule.UnRegisterHandler(PhotoHandler);
        CameraModule.Dispose();
        CameraModule = null;
        frm_message.Show(Resource.String("STR_CAMERA_TIMEOUT_MESSAGE_TEXT"),
          Resource.String("STR_CAMERA_TIMEOUT_MESSAGE_CAPTION"), MessageBoxButtons.OK, ((Form)View));
        Log.Error(string.Format("Camera Timeout {0}", ((Form)View).Name));
        CameraModule = new CameraModule((Form)View, typeof(frmCapturePhoto), typeof(SelectDevice)) { EventMode = true };
        TakePhoto();
        return;
      }
      Stamp(Event.Image, WGDB.Now, "dd/M/yyyy");

      T _model = Model;
      _model.HolderPhoto = Event.Image;
      _model.HasPhoto = _model.HolderPhoto != null;
      _model.PhotoChanged = true;
      Model = _model;
    }

    /// <summary>
    ///  Stamp DateTime on Photo
    /// </summary>
    /// <param name="ImageStamp"></param>
    /// <param name="DtStamp"></param>
    /// <param name="Format"></param>
    protected void Stamp(Image ImageStamp, DateTime DtStamp, string Format)
    {
      string _stamp_string;
      Graphics _graphics;
      GraphicsPath _graphics_path;
      Matrix _translate_matrix;
      Matrix _translate_matrix2;

      try
      {
        if (!((ImageStamp.Width == 640 || ImageStamp.Width == 320) &&
            (ImageStamp.Height == 480 || ImageStamp.Height == 240)))
          return;
        
        _graphics_path = new GraphicsPath();
        _stamp_string = !string.IsNullOrEmpty(Format) ? DtStamp.ToString(Format) : DtStamp.ToString(); 
        _graphics = Graphics.FromImage(ImageStamp);
        _translate_matrix = new Matrix();
        _translate_matrix2 = new Matrix();

         

        _graphics_path.AddString(_stamp_string.Trim(), FontFamily.GenericSansSerif,
                                   (int)FontStyle.Bold, _graphics.DpiY * 60f / 72f,
                                  new Point(320, 0), new StringFormat() { Alignment = StringAlignment.Center, LineAlignment = StringAlignment.Center });

          _translate_matrix.Scale(ImageStamp.Width / 640f, ImageStamp.Height / 480f);
          _graphics_path.Transform(_translate_matrix);
        _graphics_path.Flatten();
        _translate_matrix2.Translate(0,
          ImageStamp.Height - (_graphics_path.GetBounds().Size.Height));
        _graphics_path.Transform(_translate_matrix2);

        _graphics.SmoothingMode = SmoothingMode.AntiAlias;
        _graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
        _graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;
        _graphics.DrawPath(new Pen(Color.Black, 5), _graphics_path);
        _graphics.FillPath(new SolidBrush(Color.White), _graphics_path);
//        _graphics.DrawString(_stamp_string, new Font("Tahoma",32), Brushes.Black, rectf);

        _graphics.Flush();
      }
      catch (Exception _ex)
      {

        Log.Exception(_ex);
      }

    }


    /// <summary>
    ///   method to save changes to pin
    /// </summary>
    public List<InvalidField> SavePin()
    {
      return pin_save(Model.Pin, false);
    }

    /// <summary>
    ///   method to generate random pin
    /// </summary>
    public List<InvalidField> CreateRandomPin()
    {
      return pin_save("", true);
    }

    public List<InvalidField> CheckDuplicatePlayer(bool _bSave)
    {
      CardData _card_data;
      var _model = Model;
      List<InvalidField> _errors = null;
      String _duplicate_msg;
      Boolean _allow_duplicate;

      _errors = CheckIdAlreadyExists(_bSave);

      if (_errors.Count > 0) return _errors;

      _card_data = UpdateCardFromModel((CardData)CardData.Clone(), _model);

      if (_card_data == null) return _errors;

      if (!CardData.CheckDuplicateAccountsReception(_card_data, out _duplicate_msg, out _allow_duplicate, _bSave))
      {
        return manageMessages(_bSave, _duplicate_msg);
      }

      return _errors;
    }

    private List<InvalidField> manageMessages(bool _bSave, String _duplicate_msg)
    {
      DialogResult _rc;

      if (!_bSave || !_AllowAccountsDuplicates)
      {
        _showndupdetails = true;
        frm_message.Show(_duplicate_msg, Resource.String("STR_APP_GEN_MSG_WARNING"), MessageBoxButtons.OK, ViewForm);

        return new List<InvalidField>()
            {
              new InvalidField
              {
                Id = "DocumentDuplicated",
                Reason = Resource.String("STR_DUPLICATE_ACCOUNTS_WARNING")
                  .Replace("@p0", "")
                  .Replace("\\n", "")
                  .Replace("\\r", "")
                  .Replace(":", "").Trim()
              }
            };
      }
      else
      {
        _rc = frm_message.Show(_duplicate_msg, Resource.String("STR_APP_GEN_MSG_WARNING"), MessageBoxButtons.YesNo, MessageBoxIcon.Warning, View as Form);

        if (_rc == DialogResult.Cancel)
        {

          return new List<InvalidField>() { new InvalidField() { Id = "ContinueAccountsDuplicate", Reason = "0" } };
        }
        else
        {

          return new List<InvalidField>() { new InvalidField() { Id = "ContinueAccountsDuplicate", Reason = "1" } };
        }
      }
    }

    public List<InvalidField> SavePlayer()
    {
      List<InvalidField> _errors = null;
      bool _bSave = false;
      try
      {
        _errors = CheckDuplicatePlayer(true);

        if (_errors.Count > 0)
        {
          if (_errors[0].Id == "ContinueAccountsDuplicate")
          {
            _bSave = (_errors[0].Reason == "1");
          }
          else
          {
            return _errors;
          }
        }
        else
        {
          _bSave = true;
        }

        if (_bSave)
        {
          _errors = Save();
          if (_errors == null)
        {
            _errors.Add(new InvalidField { Id = "", Reason = Resource.String("STR_APP_GEN_MSG_ERROR") });
          }
        }
      }
      catch (Exception _ex)
      {
        _errors.Add(new InvalidField { Id = "Ex", Reason = _ex.Message });
      }

      return _errors;
    }

    /// <summary>
    ///   method to save changes in model as a card data
    /// </summary>
    /// <param name="NewPin"></param>
    /// <param name="RandomGeneration"></param>
    /// <returns></returns>
    private List<InvalidField> pin_save(string NewPin, bool RandomGeneration)
    {
      IMVCView<T> _player_edit_details_view = View;
      if (_player_edit_details_view == null)
      {
        return new List<InvalidField>();
      }
      DialogResult _dlg_rc;

      _dlg_rc = SavingCardConfirmation(true, false, Resource.String("STR_FRM_ACCOUNT_USER_EDIT_SAVE_CARD_PIN"));

      switch (_dlg_rc)
      {
        case DialogResult.Cancel:
          return new List<InvalidField>();
        case DialogResult.OK:
          List<InvalidField> _errors;
          _errors = SavePlayer();

          if (_errors.Count > 0)
          {
            if (_errors[0].Id == "ContinueAccountsDuplicate")
            {
              if (_errors[0].Reason == "0")
              {
                _errors.Clear();
                return _errors;
              }
            }
            else
            {
              return _errors;
            }
          }
          break;
      }

      if (CashierBusinessLogic.DB_UpdateCardPin(CardData, NewPin, CardData.PlayerTracking.Pin, RandomGeneration))
      {
        CardData.PlayerTracking.Pin = NewPin;
        if (RandomGeneration)
        {
          _player_edit_details_view.RandomPinGeneratedOk();
        }
        else
        {
          _player_edit_details_view.PinChangedOk();
        }
      }
      else
      {
        _player_edit_details_view.ErrorUpdatingPin();
      }
      return new List<InvalidField>();
    } // pin_save

    /// <summary>
    /// 
    /// </summary>
    /// <param name="HolderBirthdayDate"></param>
    /// <returns></returns>
    private HolderHappyBirthDayNotification GetHolderBirthDayNotification(Int64 AccountId, String HolderName, DateTime? HolderBirthdayDate)
    {
      DateTime _now_date;
      TimeSpan _tspam;
      DateTime _birth_date;

      // FJC 11-MAY-2016: IMPORTANT!!!!!! If make changes in this function, we have to make changes in UcCard.cs (//RCI 19-AUG-2010: Happy Birthday!) 
      
      //JBC 30-01-2015 BirthDay alarm feature
      if (!Alarm.CheckBirthdayAlarm(AccountId, HolderName,
        CommonCashierInformation.CashierSessionInfo().TerminalId + " - " + CommonCashierInformation.CashierSessionInfo().TerminalName, TerminalTypes.UNKNOWN))
      {
        Log.Error("GetHolderBirthDayNotification(): Error checking Birthday Alarm.");
      }

      _now_date = new DateTime(WGDB.Now.Year, WGDB.Now.Month, WGDB.Now.Day);
      _birth_date = HolderBirthdayDate ?? default(DateTime);

      // MBF 26-OCT-2010 - People born in 29 of Feb have birthdays too.
      if (_birth_date.Month == 2
        && _birth_date.Day == 29
        && !DateTime.IsLeapYear(WGDB.Now.Year))
      {
        _birth_date = new DateTime(WGDB.Now.Year, _birth_date.Month, _birth_date.AddDays(-1).Day);
      }
      else
      {
        _birth_date = new DateTime(WGDB.Now.Year, _birth_date.Month, _birth_date.Day);
      }

      _tspam = (_birth_date - _now_date);

      if (_tspam.TotalDays > 0 && _tspam.TotalDays <= GeneralParam.GetInt32("Accounts", "Player.BirthdayWarningDays", 7))
      {
        return HolderHappyBirthDayNotification.HAPPY_BIRTHDAY_INCOMMING;
      }
      else if (_tspam.TotalDays == 0)
      {
        return HolderHappyBirthDayNotification.HAPPY_BIRTHDAY_TODAY;
      }

      return HolderHappyBirthDayNotification.NONE;
    }


  }
}
