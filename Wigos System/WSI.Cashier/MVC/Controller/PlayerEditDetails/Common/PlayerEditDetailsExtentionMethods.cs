//------------------------------------------------------------------------------
// Copyright � 2007-2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: PlayerEditDetailsExtentionMethods.cs
// 
//   DESCRIPTION: Various extention methods used by player edit details mvc
//                
//                
//                
//                
//                
//                
// 
//        AUTHOR: Scott Adamson.
// 
// CREATION DATE: 02-DEC-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 02-DEC-2007 SJA    First release.
// 15-MAR-2016 ETP    Fixed Bug 10611: Recepci�n: Excepci�n no controlada al introducir CP.
// 30-MAY-2016 JCA    Fixed Bug 13692:Recepci�n: error al crear un Nuevo Cliente con la caja cerrada
// 12-ENE-2017 DPC    Bug 22421:Auditor�a: mensajes err�neos al imprimir datos del cliente
//------------------------------------------------------------------------------

using System;
using System.CodeDom;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using System.Windows.Forms;
using WSI.Common;
using WSI.Common.Entities.General;

namespace WSI.Cashier.MVC.Controller.PlayerEditDetails.Common
{
  public static class PlayerEditDetailsExtentionMethods
  {

    /// <summary>
    ///   method to set properties across threads safely
    /// </summary>
    public static void SetControlPropertyThreadSafe(this Control Control, string PropertyName, object PropertyValue)
    {
      if (Control.InvokeRequired)
      {
        Control.Invoke(new SetControlPropertyThreadSafeDelegate(SetControlPropertyThreadSafe), Control, PropertyName,
          PropertyValue);
      }
      else
      {
        var t = Control.GetType();

        t.InvokeMember(PropertyName, BindingFlags.SetProperty, null, Control, new[] { PropertyValue });
      }
    }

    /// <summary>
    ///   method to get property values across threads safely
    /// </summary>
    public static object GetControlPropertyThreadSafe(this Control Control, string PropertyName)
    {
      if (Control.InvokeRequired)
      {
        return Control.Invoke(new GetControlPropertyThreadSafeDelegate(GetControlPropertyThreadSafe), Control,
          PropertyName);
      }
      return Control.GetType().InvokeMember(PropertyName, BindingFlags.GetProperty, null, Control, null);
    }


    /// <summary>
    ///   convert a datatable to a list of states for a given country
    /// </summary>
    /// <param name="DataTable"></param>
    /// <param name="IdCountry"></param>
    /// <param name="SupVal"></param>
    /// <returns></returns>
    public static List<State> ToFederalEntityList(this DataTable DataTable, int IdCountry, string SupVal = null)
    {
      string _name;
      string _adj;
      string _iso2;
      DataTable _dt_fed_states;
      Countries.GetCountryInfo(IdCountry, out _name, out _adj, out _iso2);
      States.GetStates(out _dt_fed_states, _iso2);
      var _states = _dt_fed_states.ToSelectableOptionList<State>();
      List<State> _retval = new List<State>();
      if (!string.IsNullOrEmpty(SupVal))
      {
        _retval.Add(new State { Id = 0, Name = SupVal });
      }

      foreach (DataRow _row in DataTable.Rows)
      {
        int _id = (int)_row.ItemArray.GetValue(1);
        foreach (var _state in _states)
        {
          if (_state.Name != _row.ItemArray.GetValue(0).ToString())
          {
            continue;
          }
          _id = _state.Id;
          break;
        }
        if (_row.ItemArray.GetValue(0) != DBNull.Value)
        {
          if (!((string)_row.ItemArray.GetValue(0)).Equals((SupVal ?? "unmatched")))
          {
            _retval.Add(new State { Id = _id, Name = ((string)_row.ItemArray.GetValue(0)) });
          }
          else
          {
            _retval[0].Id = _id;
          }
        }
        else
        {
          _retval.Add(new State { Id = 0, Name = String.Empty });
        }
      }
      return _retval;
    }

    /// <summary>
    ///   convert a datatable to a list of a given type based on ISelectableOption
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="DataTable"></param>
    /// <param name="SupVal"></param>
    /// <returns></returns>
    public static List<T> ToSelectableOptionList<T>(this DataTable DataTable, string SupVal = null)
      where T : ISelectableOption, new()
    {
      List<T> _retval = new List<T>();
      int _id;
      String _name;
      Boolean _parsed;

      if (!string.IsNullOrEmpty(SupVal))
      {
        _retval.Add(new T { Id = 0, Name = SupVal });
      }
      foreach (DataRow _row in DataTable.Rows)
      {
        string _string = _row.ItemArray.GetValue(0) as string;
        if (_string != null)
        {
          if (!_string.Equals((SupVal ?? "unmatched")))
          {
            _retval.Add(new T { Id = (int)_row.ItemArray.GetValue(1), Name = _string });
          }
          else
          {
            _retval[0].Id = (int)_row.ItemArray.GetValue(1);
          }
        }
        else
        {
          if (
            !(_row.ItemArray.GetValue(1).ToString()).Equals((SupVal ?? "unmatched")))
          {
            _parsed = Int32.TryParse(_row.ItemArray.GetValue(0).ToString(), out _id);

            if (!_parsed)
            {
              _id = 1;
              _name = "";
            }
            else
            {
              _name = _row.ItemArray.GetValue(1).ToString();
            }

            _retval.Add(new T
            {
              Id = _id,
              Name = _name
            });
          }
          else
          {
            _retval[0].Id = (int)_row.ItemArray.GetValue(0);
          }
        }
      }

      return _retval;
    }

    /// <summary>
    ///   create a list of value differences in properties between 2 classes
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="Val1"></param>
    /// <param name="Val2"></param>
    /// <returns></returns>
    public static Dictionary<string, Variance> DetailedCompare<T>(this T Val1, T Val2)
    {
      Dictionary<string, Variance> _variances = new Dictionary<string, Variance>();
      Type _type = Val1.GetType();
      FieldInfo[] _fields = _type.GetFields();
      PropertyInfo[] _properties = _type.GetProperties();
      foreach (FieldInfo _field in _fields)
      {
        Variance _variance = new Variance { ValA = _field.GetValue(Val1), ValB = _field.GetValue(Val2) };
        if (_variance.ValA is Enum )
          _variance.ValA = (int) _variance.ValA;
        if (_variance.ValB != null && _variance.ValB is Enum)
          _variance.ValB = (int)_variance.ValB;

        if (Equals(_variance.ValA, _variance.ValB))
        {
          continue;
        }

        if (_variance.ValA != null && _variance.ValA.GetType().FullName == "WSI.Common.DocumentList")
        {
          WSI.Common.DocumentList _list_a = (WSI.Common.DocumentList)_variance.ValA ?? new DocumentList();
          WSI.Common.DocumentList _list_b = (WSI.Common.DocumentList)_variance.ValB ?? new DocumentList();

          if (_list_a.Count == 0 && _list_b.Count == 0)
          {
            continue;
          }
          if (_list_a.Count == _list_b.Count)
          {
            var _first_not_second = _list_a.Except(_list_b);
            var _second_not_first = _list_b.Except(_list_a);

            if (_first_not_second.Count == 0 && _second_not_first.Count == 0)
            {
              continue;
            }
          }
        }

        var _selectable_option = _variance.ValA as ISelectableOption;
        if (_selectable_option != null && _variance.ValB is ISelectableOption)
        {
          if (_selectable_option.Id == ((ISelectableOption)_variance.ValB).Id)
          {
            continue;
          }
        }
        _variances.Add(_field.Name, _variance);
      }
      foreach (PropertyInfo _property in _properties)
      {

        Variance _variance = new Variance { ValA = _property.GetValue(Val1, null), ValB = _property.GetValue(Val2, null) };
        if (Equals(_variance.ValA, _variance.ValB))
        {
          continue;
        }

        if (_property.PropertyType == typeof(string))
        {
          if (String.IsNullOrEmpty((_variance.ValA ?? String.Empty).ToString()) && String.IsNullOrEmpty((_variance.ValB ?? String.Empty).ToString()))
          {
            continue;
          }
        }

        var _selectable_option = _variance.ValA as ISelectableOption;
        if (_selectable_option != null && _variance.ValB is ISelectableOption)
        {
          int _inta = _selectable_option.Id;
          _inta = _inta < 1 ? 0 : _inta;
          int _intb = ((ISelectableOption)_variance.ValB).Id;
          _intb = _intb < 1 ? 0 : _intb;
          if (_intb.Equals(_inta))
          {
            continue;
          }
        }

        _variances.Add(_property.Name, _variance);
      }
      return _variances;
    }

    private static DocumentList Except(this DocumentList ListA, DocumentList ListB)
    {
      var _retval = new DocumentList();
      foreach (IDocument _item in ListA)
      {
        if (!ListB.Contains(_item))
          _retval.Add(_item);
      }
      return _retval;
    }
    #region Nested type: GetControlPropertyThreadSafeDelegate

    /// <summary>
    ///   delegate to get property values across threads safely
    /// </summary>
    private delegate object GetControlPropertyThreadSafeDelegate(Control Control, string PropertyName);

    #endregion
    #region Nested type: SetControlPropertyThreadSafeDelegate

    /// <summary>
    ///   delegate to set properties across threads safely
    /// </summary>
    private delegate void SetControlPropertyThreadSafeDelegate(
      Control Control, string PropertyName, object PropertyValue);

    #endregion
  }

  /// <summary>
  ///   class to hold variance differences
  /// </summary>
  public class Variance
  {
    public object ValA { get; set; }
    public object ValB { get; set; }
  }
}
