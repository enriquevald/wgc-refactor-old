//------------------------------------------------------------------------------
// Copyright � 2007-2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: PlayerEditDetailsREceptionController.cs
// 
//   DESCRIPTION: Implementation of reception controller class
//                
//                
//                
//                
//                
//                
// 
//        AUTHOR: Scott Adamson.
// 
// CREATION DATE: 02-DEC-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 02-DEC-2007 SJA    First release.
// 25-JAN-2016 YNM    Product Backlog Item 6656:Visitas / Recepci�n: Crear ticket de entrada
// 28-JAN-2016 AVZ    Product Backlog Item 8673:Visitas / Recepci�n: Configuraci�n de Modo de Recepci�n
// 11-FEB-2016 AVZ    Product Backlog Item 9046:Visitas / Recepci�n: BlackLists
// 22-FEB-2016 YNM    Fixed Bug Bug 9762: btn de "MORE RESULTS" is enabled when theres no more results
// 20-MAY-2016 JBP    Product Backlog Item 13262:Recepci�n - versi�n Junio
// 30-MAY-2016 JCA    Fixed Bug 13692:Recepci�n: error al crear un Nuevo Cliente con la caja cerrada
// 31-MAY-2015 FJC    Bug 13982:Recepci�n: Crear un nuevo cliente con PIN y modificarlo es permitido desde el m�dulo de Recepci�n.
// 14-JUL-2016 PDM    PBI 15448:Visitas / Recepci�n: MEJORAS - Cajero & GUI - Nuevo motivo de bloqueo
// 18-JUL-2016 FJC    PBI 15447:Visitas / Recepci�n: MEJORAS - Cajero - Mejorar mensaje de error "...documentos expirados o lista de prohibidos..."
// 25-JUL-2016 FJC    PBI 15446:Visitas / Recepci�n: MEJORAS - Cajero - Apertura sesi�n de caja
// 26-JUL-2016 PDM    PBI 15444:Visitas / Recepci�n: MEJORAS - Cajero - Expandir formulario de resultados de b�squeda
// 03-AGO-2016 FJC    Bug 16072:Recepci�n: Errores Varios
// 04-AGO-2016 FJC    Bug 16408:Mensaje black list: NLs incorrecta con el cajero en ingl�s
// 20-SEP-2016 JRC    PBI 17677:Visitas / Recepci�n: MEJORAS II - Ref. 48 - Reportes recepci�n - separar columna Nombre (GUI)
// 19-DIC-2016 FJC    Bug 21693:Recepci�n: Errores encadenados al crear cuenta y registrar entrada
// 20-DIC-2016 FJC    Bug 14055:Recepci�n: Persistencia de los datos introducidos anteriormente cuando creas mas de un cliente seguido, en los campos Tipo Documento, Pa�s Nacimiento, Nacionalidad, CP (Direcci�n2).
// 06-JUL-2017 FJC    Bug: WIGOS-3429 Recepci�n: Al guardar una cuenta si modificar datos, aparece el popup de guardar cambios
// 12-JUL-2017 DPC    WIGOS-3564: [Ticket #7077] Incidencia en San Sebasti�n. No se puede registrar clientes "Lista de Prohibidos deniega acceso"
// 10-OCT-2017 DPC    [WIGOS-5525]: Reception - Register customer output - Cashier button - Exit logic
// 20-FEB-2018 EOR    Bug 31587: WIGOS-3429 Recepci�n: Al guardar una cuenta si modificar datos, aparece el popup de guardar cambios 
// 29-MAR-2018 LTC    Bug 32101: WIGOS-8442 Account "Nationality" field appear empty in "Reception" module and populated with value in "Accounts" Cashier section 
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;
using System.Text;
using WSI.Cashier.Controls;
using WSI.Cashier.MVC.Controller.PlayerEditDetails.Common;
using WSI.Cashier.MVC.Interface;
using WSI.Cashier.MVC.Model.PlayerEditDetails;
using WSI.Cashier.MVC.View.PlayerEditDetails.Common;
using WSI.Cashier.MVC.View.PlayerEditDetails.Interface;
using WSI.Cashier.Strategy.ReceptionMode;
using WSI.Common;
using WSI.Common.Entities.General;
using WSI.Common.Entrances;
using WSI.Common.Entrances.Entities;
using WSI.Common.Entrances.Enums;
using WSI.Common.Utilities.Extensions;
using WSI.Cashier.MVC.View.BlacklistMatches;
using WSI.Cashier.MVC.Controller.BlackListMatches;
using WSI.Cashier.MVC.View.PlayerEditDetails;
using WSI.IDScanner.Model;
using WSI.Common.Junkets;
using WSI.Cashier.JunketsCashier;
using WSI.Entrances.BlackList.Model.DTO;

namespace WSI.Cashier.MVC.Controller.PlayerEditDetails
{
  public class PlayerEditDetailsReceptionController : PlayerEditDetailsBaseController<PlayerEditDetailsReceptionModel>
  {
    #region Delegates

    public delegate void AsyncWorkerBlackListDelegate(ICustomerNameAndId CustomerDetails);

    public delegate void AsyncWorkerExpiredDocumentsDelegate(long CustomerId);

    #endregion

    private readonly int m_grid_results_max = 4;

    /// <summary>
    ///   Read once return many property for enabled state of card functions in reception view
    /// </summary>
    private bool? m_card_funcs_enabled;
    private string m_tracknumber;
    private readonly Color m_expired_color = Color.Salmon;
    private readonly Color m_expired_color_highlight = Color.Red;
    private readonly Color m_expired_font_color = Color.White;
    private JunketsShared m_junket_info;

    public PlayerEditDetailsReceptionController(DataSet DsCustomersReception, string SqlSearch, bool NewCustomer,
      IDScannerInfo ScannedInfo,
      IMVCView<PlayerEditDetailsReceptionModel> ViewForm, WSI.IDScanner.IDScanner IDScannerFunc,
      uc_bank UcBank,
      string TrackNumber = null,
      Int32 NumRecordsShow = 4)
    {
      m_grid_results_max = NumRecordsShow;
      this.AbortOpen = false;
      m_tracknumber = TrackNumber;
      IDScanner = IDScannerFunc;
      this.UcBank = UcBank;
      this.ScannedInfo = ScannedInfo;
      SaveExtention += SaveCustomerInformation;
      LoadExtention += LoadCustomerInformation;
      View = ViewForm;
      View.VisibleFields.HideControls();
      var _mdl = CreateModel(DsCustomersReception);
      _mdl.IsNew = NewCustomer; //EOR 20-FEB-2018
      _mdl.SearchStringSQL = SqlSearch;
      View.Controller = this;
      Model = _mdl;
      if (NewCustomer)
      {
        if (!this.NewCustomer())
        {
          this.AbortOpen = true;
          ((Form)View).Close();
        }
      }
      Customers.RegisterHandler((Form)View, HandleEventBlackList);
      Customers.RegisterHandlerExpiredDocuments((Form)View, HandleEventExpiredDocuments);
    }

    public bool AbortOpen { get; set; }

    public IDScannerInfo ScannedInfo { get; set; }


    public bool ReceptionCardFunctionsPermitted
    {
      get
      {
        if (!m_card_funcs_enabled.HasValue)
        {
          m_card_funcs_enabled = GeneralParam.GetBoolean("Reception", "CardFunctionsEnabled", true);
        }
        return m_card_funcs_enabled.Value;
      }
    }

    public string SearchedDocumentId { get; set; }

    public void ShowCustomersDetails(CustomerSearchResult Account)
    {
      var _old_model = Model;
      CardData = new CardData { AccountId = Account.AccountNumber };
      CardData.DB_CardGetAllData(CardData.AccountId, CardData, true);
      var _model = UpdateModelFromCardData(CardData, _old_model);
      _model.TrackData = null;
      LastModel = _model.Clone();

      if (!string.IsNullOrEmpty(SearchedDocumentId))
      {
        foreach (var _document in
          _model.IDDocuments)
        {
          if (_document.Value == SearchedDocumentId)
          {
            SearchedDocumentId = "";
            _model.HolderDocumentType = _model.DocumentTypes.Find(x => x.Id == _document.Key);
            _model.HolderDocument = _document.Value;
          }
        }
      }

      Model = _model;
    }

    /// <summary>
    ///   implementation of method to detect changes
    /// </summary>
    /// <param name="Old"></param>
    /// <param name="New"></param>
    /// <returns></returns>
    internal override bool HasChanges(PlayerEditDetailsReceptionModel Old, PlayerEditDetailsReceptionModel New)
    {
      if (New.ScannedDocumentsChanged)
      {
        return true;
      }
      var _changes = Old.DetailedCompare(New);
      _changes.Remove("BlackListResult");
      _changes.Remove("BlackListInclusionDate");
      _changes.Remove("BlackListSource");
      _changes.Remove("BlackListReason");
      _changes.Remove("BlackListResultCustomer");
      _changes.Remove("BlackListReasonDescription");
      _changes.Remove("Validator");
      _changes.Remove("ScannedDocuments");
      _changes.Remove("NoMoreResults");
      _changes.Remove("RecordExpirationDate");
      _changes.Remove("ExpiredDocumentsResult");
      _changes.Remove("CreationDate");
      _changes.Remove("HolderFullName");
      _changes.Remove("SearchScannedDocuments");
      _changes.Remove("Photo");
      _changes.Remove("Fields");
      _changes.Remove("CardCreationDate");
      _changes.Remove("TrackData");
      _changes.Remove("AcknowledgedBlackList");
      _changes.Remove("BlackListEntryBlocked");
      _changes.Remove("BlackListMessages");
      _changes.Remove("BlackListLists");
      _changes.Remove("HolderBirthDayDateShowNotification");
      _changes.Remove("BlackListCustomerSelected");
      _changes.Remove("Pin");
      _changes.Remove("DocumentDuplicated");
      _changes.Remove("IsNew");
      _changes.Remove("DuplicatedID");
      _changes.Remove("DuplicatedDetails");
      _changes.Remove("IDDocuments");
      _changes.Remove("BeneficiaryDocument3Type");
      _changes.Remove("MaritalStatuses");
      return _changes.Count > 0;
    }

    public void RecycleCard()
    {
      DialogResult _rc;
      string _error_str;
      string _message;

      if (
        !ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.RecyleCard,
          ProfilePermissions.TypeOperation.RequestPasswd, View as Form, out _error_str))
      {
        return;
      }
      _message = Resource.String("STR_RECYCLE_CARD_ALERT");
      _message = _message.Replace("\\r\\n", "\r\n");

      _rc = frm_message.Show(_message, Resource.String("STR_RECYCLE_CARD_TITLE"), MessageBoxButtons.YesNo,
        MessageBoxIcon.Warning, View as Form);

      switch (_rc)
      {
        case DialogResult.OK:
        case DialogResult.Yes:
          break;
        default:
          return;
      }

      //Recycle

      var _recycle_result = CashierBusinessLogic.DB_CardRecycleWithFeeback(CardData.TrackData, CardData.AccountId);

      if (_recycle_result != ENUM_CARD_RECYCLE_STATUS.Recycled)
      {
        frm_message.Show(
          Resource.String("STR_APP_ERROR_RECYCLE_CARD_RESULT_" + _recycle_result.ToValueString()),
          Resource.String("STR_RECYCLE_CARD_TITLE"), MessageBoxButtons.OK, MessageBoxIcon.Error, View as Form);
        return;
      }

      CardData _carddata2 = new CardData();
      CardData.DB_CardGetAllData(CardData.AccountId, _carddata2, true);

      CardData.TrackData = _carddata2.TrackData;

      var _mdl = Model;

      _mdl.TrackData = CardData.TrackData;
      _mdl.VisibleTrackData = CardData.VisibleTrackdata();

      LastModel.TrackData = CardData.TrackData;
      LastModel.VisibleTrackData = CardData.VisibleTrackdata();

      Model = _mdl;
    } //btn_recycle_card_Click

    public void AssignCard()
    {
      Cashier.ResetAuthorizedByUser();

      try
      {
        // Check if current user is an authorized user
        string _error_str;
        if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.AccountAsociateCard,
            ProfilePermissions.TypeOperation.RequestPasswd, View as Form, out _error_str))
        {
          return;
        }

        using (frm_card_assign _card_assign = new frm_card_assign())
        {
          using (frm_yesno _shader = new frm_yesno())
          {
            _shader.Opacity = 0.6f;
            _shader.Show();
            CurrencyExchangeResult _exchange_result;
            _card_assign.CardAssignParams(CardData, 0, CardData.VisibleTrackdata(), out _exchange_result);
            _card_assign.ShowDialog(_shader);
          }
        }
      }
      finally
      {
        Cashier.ResetAuthorizedByUser();
      }
      CardData _carddata2 = new CardData();
      CardData.DB_CardGetAllData(CardData.AccountId, _carddata2, true);

      CardData.TrackData = _carddata2.TrackData;

      var _mdl = Model;

      _mdl.TrackData = CardData.TrackData;
      _mdl.VisibleTrackData = CardData.VisibleTrackdata();

      LastModel.TrackData = CardData.TrackData;
      LastModel.VisibleTrackData = CardData.VisibleTrackdata();

      Model = _mdl;
    } // btn_new_card_Click

    /// <summary>
    ///   event handler for return information from black list service
    /// </summary>
    /// <param name="Ev"></param>
    public void HandleEventBlackList(BlackListResultInfo Ev)
    {
      PlayerEditDetailsReceptionModel _model = Model;
      UpdateModelBlackListInfo(_model, Ev);
      Model = _model;
    }

    private void UpdateModelBlackListInfo(PlayerEditDetailsReceptionModel Mdl, BlackListResultInfo BlacklistInfo)
    {
      Mdl.BlackListResult = BlacklistInfo.Result;
      if (Mdl.BlackListResult == BlackListResult.Processing) return;
      if (Mdl.GridData != null)
      {
        CustomerSearchResult _selected_customer = Mdl.GridData.Find("AccountNumber", BlacklistInfo.AccountId);
        if (_selected_customer != null)
          _selected_customer.BlackListResultInfo = BlacklistInfo;
      }

      if (BlacklistInfo.AccountId != Mdl.AccountId && BlacklistInfo.AccountId != -1) return;

      Mdl.BlackListResultCustomer = BlacklistInfo.CustomerResults ?? new BlackListResultCustomer[] { };

      List<BlackListResultCustomer> _externalblacklsitResults = new List<BlackListResultCustomer>();
      BlackListResultCustomer _localblacklistresult = null;
      if (Mdl.BlackListResultCustomer.Length > 0)
      {
        foreach (BlackListResultCustomer _variable in Mdl.BlackListResultCustomer)
        {
          if (_variable.Source == ENUM_BLACKLIST_SOURCE.LOCAL)
          {
            _localblacklistresult = _variable;
          }
          if (_variable.Source != ENUM_BLACKLIST_SOURCE.LOCAL)
          {
            _externalblacklsitResults.Add(_variable);
          }
        }
      }

      if (_externalblacklsitResults.Count == 1)
      {
        if (_localblacklistresult != null)
        {
          var x = _externalblacklsitResults.ToArray();
          _externalblacklsitResults.Clear();
          _externalblacklsitResults.Add(_localblacklistresult);
          _externalblacklsitResults.AddRange(x);
        }
        Mdl.BlackListCustomerSelected = _externalblacklsitResults.ToArray();
        Mdl.BlackListResult = BlackListResult.InBlackList;
      }
      else if (_localblacklistresult != null)
      {
        Mdl.BlackListCustomerSelected = new[] { _localblacklistresult };
        Mdl.BlackListResult = BlackListResult.InBlackList;
      }
    }

    /// <summary>
    ///   event handler for return information from expired documents service
    /// </summary>
    /// <param name="Ev"></param>
    public void HandleEventExpiredDocuments(ExpiredDocumentsResultInfo Ev)
    {
      var _model = Model;
      _model.ExpiredDocumentsResult = Ev.Result;

      Model = _model;
    }

    private List<InvalidField> SaveCustomerInformation(PlayerEditDetailsReceptionModel Mdl, SqlTransaction Transaction)
    {
      AccountsReception _accounts_reception;

      try
      {
        if (!Mdl.CreationDate.HasValue)
        {
          Mdl.CreationDate = WGDB.Now;
        }
        _accounts_reception = CreateAccountsReceptionFromModel(Mdl);

        if (Customers.SaveCustomer(_accounts_reception, Transaction))
        {
          var _mdl = Model;
          _mdl.ScannedDocumentsChanged = false;
          LastModel = _mdl.Clone();
          Model = _mdl;

          return new List<InvalidField>();
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return new List<InvalidField> { new InvalidField { Id = "NONE", Reason = "Error not controlled" } };
    }

    private AccountsReception CreateAccountsReceptionFromModel(PlayerEditDetailsReceptionModel Mdl)
    {
      AccountsReception _account_reception;
      CustomerAddress _customer_home_address;
      CustomerAddress _customer_secondary_address;

      _account_reception = new AccountsReception();


      if (this.m_junket_info != null)
      {
        _account_reception.JunketsBusinessLogic = this.m_junket_info.BusinessLogic;
      }


      _customer_home_address = new CustomerAddress();
      _customer_secondary_address = new CustomerAddress();

      try
      {
        if (Mdl.AccountId.HasValue)
        {
          //Account
          _account_reception.CreationDate = Mdl.CreationDate;
          _account_reception.AccountId = Mdl.AccountId.Value;
          _account_reception.BirthDate = Mdl.HolderBirthDate;
          _account_reception.Comments = Mdl.Comments;

          if (Mdl.HolderAddressCountry != null)
            _account_reception.Country = Mdl.HolderAddressCountry.Id;

          _account_reception.Document = Mdl.HolderDocument;

          if (Mdl.HolderDocumentType != null)
            _account_reception.DocumentType = Mdl.HolderDocumentType.Id;

          _account_reception.FirstName = Mdl.HolderName3;
          _account_reception.SecondName = Mdl.HolderName4;
          _account_reception.FirstSurname = Mdl.HolderName1;
          _account_reception.SecondSurname = Mdl.HolderName2;

          if (Mdl.HolderNationality != null)
            _account_reception.Nacionality = Mdl.HolderNationality.Id;

          if (Mdl.HolderGender != null)
            _account_reception.Gender = Mdl.HolderGender.Id;

          //Contact
          _account_reception.Contact.Email = Mdl.HolderEmail1;
          _account_reception.Contact.FirstTelephone = Mdl.HolderPhone1;
          _account_reception.Contact.SecondTelephone = Mdl.HolderPhone2;
          _account_reception.Contact.Twitter = Mdl.HolderTwitterAccount;
          _account_reception.Contact.Facebook = Mdl.Facebook;

          _account_reception.AceptedAgreement = Mdl.HasAceptedAgreement;
          _account_reception.AceptedAgreementDate = Mdl.AceptedAgreementDate;
          _account_reception.InBlackList = Mdl.BlackListResult;
          _account_reception.OpAcknowledgedBlackList = Mdl.AcknowledgedBlackList;
          //Home Address
          _customer_home_address.AddressType = ENUM_ADDRESS.HOME_ADRESS;
          _customer_home_address.City = Mdl.HolderCity;

          if (Mdl.HolderAddressCountry != null)
            _customer_home_address.CountryAddress = Mdl.HolderAddressCountry.Id;

          _customer_home_address.Colony = Mdl.HolderAddressLine02;
          _customer_home_address.Cp = Mdl.HolderZip;
          _customer_home_address.Delegation = Mdl.HolderAddressLine03;

          _customer_home_address.Number = Mdl.HolderExtNum;
          _customer_home_address.Street = Mdl.HolderAddressLine01;

          if (Mdl.HolderFedEntity != null)
            _customer_home_address.FederalEntity = Mdl.HolderFedEntity.Id;

          _account_reception.Addresses.Add(_customer_home_address);

          //Secondary Address
          _customer_secondary_address.AddressType = ENUM_ADDRESS.SECONDARY_ADRESS;
          _customer_secondary_address.City = Mdl.HolderAlternativeTown;
          if (Mdl.HolderAlternativeCountry != null)
          {
            _customer_secondary_address.CountryAddress = Mdl.HolderAlternativeCountry.Id;
          }
          _customer_secondary_address.Colony = Mdl.HolderAlternativeAddressLine2;
          _customer_secondary_address.Cp = Mdl.HolderAlternativePostCode;
          _customer_secondary_address.Delegation = Mdl.HolderAlternativeAddressLine3;
          _customer_secondary_address.Number = Mdl.HolderAlternativeHouseNumber;
          _customer_secondary_address.Street = Mdl.HolderAlternativeAddressLine1;
          if (Mdl.HolderAlternativeState != null)
          {
            _customer_secondary_address.FederalEntity = Mdl.HolderAlternativeState.Id;
          }
          _account_reception.Addresses.Add(_customer_secondary_address);

          //Scanned Documents
          LoadScannedDocuments();

          foreach (var _scanned_document in Mdl.ScannedDocuments)
          {
            _account_reception.ScannedDocuments.Add(new CustomerScannedDocument
            {
              DocumentTypeDescription = _scanned_document.DocumentTypeDescription,
              DocumentTypeId = _scanned_document.DocumentTypeId,
              Expiration = _scanned_document.Expiration,
              Image = _scanned_document.Image,
              ScannedDate = _scanned_document.ScannedDate
            });
          }
          if (_account_reception.DocumentTypes == null)
            _account_reception.DocumentTypes = new Dictionary<int, string>();
          else
          {
            _account_reception.DocumentTypes.Clear();
          }
          foreach (var v in Mdl.IDDocuments)
          {
            if (!string.IsNullOrEmpty(v.Value))
              _account_reception.DocumentTypes.Add(v.Key, v.Value);
          }

          _account_reception.RecordExpiration = Mdl.RecordExpirationDate;

          return _account_reception;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      return null;
    }

    private PlayerEditDetailsReceptionModel LoadCustomerInformation(PlayerEditDetailsReceptionModel Mdl)
    {
      AccountsReception _account_reception = null;

      if (Mdl.AccountId.HasValue)
      {
        _account_reception = Customers.GetCustomerData(Mdl.AccountId.Value);
      }

      bool _found_secondary_address = false;

      Mdl.ExpiredDocumentsResult = ExpiredDocumentsResult.DoesNotHaveExpiredDocuments;
      Mdl.ScannedDocuments = new BindingList<ScannedDocument>();
      Mdl.BlackListResult = BlackListResult.None;
      Mdl.RecordExpirationDate = null;
      Mdl.HolderTwitterAccount = string.Empty;
      Mdl.Facebook = string.Empty;
      Mdl.HasAceptedAgreement = false;
      Mdl.SearchScannedDocuments = true;

      if (_account_reception != null)
      {
        Mdl.CreationDate = _account_reception.CreationDate;
        Mdl.HolderTwitterAccount = _account_reception.Contact.Twitter ?? "";
        Mdl.Facebook = _account_reception.Contact.Facebook ?? "";
        Mdl.RecordExpirationDate = _account_reception.RecordExpiration;
        Mdl.HasAceptedAgreement = CustomerHasAcceptedAgreement(_account_reception);
        Mdl.AceptedAgreementDate = _account_reception.AceptedAgreementDate;

        // get alternative address
        foreach (var _customer_address in _account_reception.Addresses)
        {
          if (_customer_address.AddressType != ENUM_ADDRESS.SECONDARY_ADRESS)
          {
            continue;
          }

          _found_secondary_address = true;
          Mdl.HolderAlternativeAddressLine1 = _customer_address.Street;
          Mdl.HolderAlternativeAddressLine2 = _customer_address.Colony;
          Mdl.HolderAlternativeAddressLine3 = _customer_address.Delegation;
          Mdl.HolderAlternativeTown = _customer_address.City;
          Mdl.HolderAlternativeState = new State { Id = _customer_address.FederalEntity };
          Mdl.HolderAlternativeCountry = new Country { Id = _customer_address.CountryAddress };
          Mdl.HolderAlternativeHouseNumber = _customer_address.Number;
          Mdl.HolderAlternativePostCode = _customer_address.Cp;
        }
        if (Mdl.IDDocuments == null)
          Mdl.IDDocuments = new Dictionary<int, string>();
        else
        {
          Mdl.IDDocuments.Clear();
        }
        if (_account_reception.DocumentTypes != null)
          foreach (var v in Mdl.DocumentTypes)
          {
            if (_account_reception.DocumentTypes.ContainsKey(v.Id))
              if (Mdl.IDDocuments.ContainsKey(v.Id))
                Mdl.IDDocuments[v.Id] = _account_reception.DocumentTypes[v.Id];
              else
              Mdl.IDDocuments.Add(v.Id, _account_reception.DocumentTypes[v.Id]);
          }

        //scanned documents
        foreach (var _scanned_document in _account_reception.ScannedDocuments)
        {
          Mdl.ScannedDocuments.Add(new ScannedDocument
          {
            DocumentTypeId = _scanned_document.DocumentTypeId,
            DocumentTypeDescription = _scanned_document.DocumentTypeDescription,
            Expiration = _scanned_document.Expiration,
            Image = _scanned_document.Image,
            ScannedDate = _scanned_document.ScannedDate
          });
        }

        CheckCustomerExpiredDocuments(Mdl.AccountId.Value);
      }
      if (Mdl.IDDocuments.ContainsKey(Mdl.HolderDocumentType.Id))
      {
        Mdl.IDDocuments[Mdl.HolderDocumentType.Id] = Mdl.HolderDocument;
      }
      else
      {
        Mdl.IDDocuments.Add(Mdl.HolderDocumentType.Id, Mdl.HolderDocument);
      }
      if (!_found_secondary_address)
      {
        Mdl.HolderAlternativeAddressLine1 = "";
        Mdl.HolderAlternativeAddressLine2 = "";
        Mdl.HolderAlternativeAddressLine3 = "";
        Mdl.HolderAlternativeTown = "";
        Mdl.HolderAlternativeState = new State { Id = 0 };
        Mdl.HolderAlternativeCountry = new Country { Id = 0 };
        Mdl.HolderAlternativeHouseNumber = "";
        Mdl.HolderAlternativePostCode = string.Empty;
      }

      if (!Mdl.AccountId.HasValue)
      {
        return Mdl;
      }

      CustomerSearchResult _selected_customer = Mdl.GridData.Find("AccountNumber", Mdl.AccountId.Value);
      if (_selected_customer.BlackListResultInfo != null &&
          _selected_customer.BlackListResultInfo.Result != BlackListResult.Error &&
          _selected_customer.BlackListResultInfo.Result != BlackListResult.None)
      {
        Mdl.BlackListResult = _selected_customer.BlackListResultInfo.Result;
        Mdl.BlackListResultCustomer = _selected_customer.BlackListResultInfo.CustomerResults;
      }
      else
      {
        CheckCustomerInBlackList(Mdl);
      }

      return Mdl;
    }

    private void CheckCustomerInBlackList(ICustomerNameAndId CustomerDetails)
    {
      AsyncWorkerBlackListDelegate _w = AsyncWorkerBlackList;
      _w.BeginInvoke(CustomerDetails, null, null);
    }

    private void AsyncWorkerBlackList(ICustomerNameAndId CustomerDetails)
    {
      Customers.CheckAccountInBlackList(CustomerDetails);
    }

    private void CheckCustomerExpiredDocuments(long AccountId)
    {
      AsyncWorkerExpiredDocumentsDelegate _w = AsyncWorkerExpiredDocuments;
      _w.BeginInvoke(AccountId, null, null);
    }

    private void AsyncWorkerExpiredDocuments(long AccountId)
    {
      Customers.CheckCustomerHasExpiredDocuments(AccountId);
    }

    private bool CustomerHasExpiredDocuments(PlayerEditDetailsReceptionModel Mdl)
    {
      DateTime _now = WGDB.Now;
      //Chequear que el expediente del cliente no est� caducado
      if (Mdl.RecordExpirationDate != null && Mdl.RecordExpirationDate.Value < _now.Date.AddDays(-1))
      {
        return true;
      }

      //Chequear que ning�n documento adjunto al expediente est� caducado
      foreach (ScannedDocument _document in Mdl.ScannedDocuments)
      {
        if (!_document.Expiration.HasValue)
        {
          continue;
        }

        if (_document.Expiration.Value.Date < _now.Date.AddDays(-1))
        {
          return true;
        }
      }

      return false;
    }

    /// <summary>
    ///   Check if customer has expired documents
    /// </summary>
    /// <returns></returns>
    private bool CustomerHasAcceptedAgreement(AccountsReception AccountReceptionInfo)
    {
      return (AccountReceptionInfo.AceptedAgreementDate != DateTime.MinValue);
    }

    /// <summary>
    ///   dispose and clean up, elimate assigned events
    /// </summary>
    public override void Dispose()
    {
      if (SaveExtention != null)
      {
        SaveExtention -= SaveCustomerInformation;
      }
      if (LoadExtention != null)
      {
        LoadExtention -= LoadCustomerInformation;
      }
      if (IDScanner != null)
        IDScanner.DetatchFromAllAvailable(HandleIDScannerEvent);
      Customers.UnregisterHandlers(ViewForm, HandleEventBlackList, HandleEventExpiredDocuments);

      (ViewForm as PlayerEditDetailsReceptionView).CleanUpForm();
      View = null;
      if (Model != null)
      {
        Model.DocumentTypes = null;
        Model.Nationalities = null;
        Model.BlackListMessages = null;
        Model.BlackListResultCustomer = null;
        Model.Countries = null;
        Model.BlackListLists = null;
        Model.Genders = null;
      }
      Model = null;

      base.Dispose();
    }

    /// <summary>
    ///   create the initial model
    /// </summary>
    /// <param name="DsCustomersReception"></param>
    /// <returns></returns>
    private PlayerEditDetailsReceptionModel CreateModel(DataSet DsCustomersReception)
    {
      var _model = new PlayerEditDetailsReceptionModel();
      _model = PopulateLists(_model);
      if (DsCustomersReception == null)
      {
        return _model;
      }

      var _grid_data = FillGridData(DsCustomersReception);

      //NO ENTIENDO ESTA LINEA
      //if (DsCustomersReception.Tables["Accounts"].Rows.Count <= m_grid_results_max)
      //{
      //  _model.NoMoreResults = true;
      //}
      _model.GridData = _grid_data;

      return _model;
    }

    /// <summary>
    ///   helper to bind the grid with information
    /// </summary>
    /// <param name="DsCustomersReception"></param>
    /// <returns></returns>
    private BindingList<CustomerSearchResult> FillGridData(DataSet DsCustomersReception)
    {
      BindingList<CustomerSearchResult> _grid_data = new BindingList<CustomerSearchResult>();
      int _rowcount = 0;
      foreach (DataRow _row in DsCustomersReception.Tables["Accounts"].Rows)
      {
        _rowcount++;
        CustomerSearchResult _csr = new CustomerSearchResult
        {
          AccountNumber = (long)(_row.IsNull(2) ? default(long) : _row[0]),
          Name = (string)(_row.IsNull(2) ? default(string) : _row[2]),
          Lastname = (string)(_row.IsNull(3) ? default(string) : _row[3]),
          Document = (string)(_row.IsNull(4) ? default(string) : _row[4]),
          BirthDate = (DateTime)(_row.IsNull(5) ? default(DateTime) : _row[5])
        };
        _grid_data.Add(_csr);
        if (_rowcount == m_grid_results_max)
        {
          break;
        }
      }
      return _grid_data;
    }

    //    public override PlayerEditDetailsReceptionModel PopulateLists(PlayerEditDetailsReceptionModel Model)

    /// <summary>
    ///   update the card with changes in the model
    /// </summary>
    /// <param name="CrdData"></param>
    /// <param name="Mdl"></param>
    /// <returns></returns>
    protected override CardData UpdateCardFromModel(CardData CrdData, PlayerEditDetailsReceptionModel Mdl)
    {
      CardData _retval = UpdateCardFromModelCommon(CrdData, Mdl);
      if (!string.IsNullOrEmpty(Mdl.TrackData))
      {
        _retval.TrackData = Mdl.TrackData;
      }

      //CardData.PlayerTrackingData _player = _retval.PlayerTracking;
      return _retval;
    }

    /// <summary>
    ///   save the id scan full card image to the expedients
    /// </summary>
    /// <param name="Image"></param>
    /// <param name="DocumentTypeId"></param>
    /// <param name="Expiry"></param>
    /// <param name="Mdl"></param>
    /// <returns></returns>
    public override PlayerEditDetailsReceptionModel SaveIdScanImage(Image Image, int DocumentTypeId, DateTime Expiry,
      PlayerEditDetailsReceptionModel Mdl)
    {
      if ((View as Control).InvokeRequired)
      {
        ((Control)View).Invoke((MethodInvoker)delegate
        {
          Mdl = SaveIdScanImage(Image, DocumentTypeId, Expiry, Mdl);
        }
          );
      }
      else
      {

        bool _is_new = false;
        int _doc_type = DocumentTypeId;
        ScannedDocument _current_document = Mdl.ScannedDocuments.Find("DocumentTypeId", _doc_type);

        if (_current_document == null)
        {
          _is_new = true;
          _current_document = new ScannedDocument();
        }

        _current_document.DocumentTypeId = _doc_type;
        _current_document.DocumentTypeDescription = IdentificationTypes.DocIdTypeString(_doc_type);
        _current_document.Expiration = Expiry == default(DateTime) ? WGDB.Now.AddMonths(3) : Expiry;
        _current_document.Image = Image.ToByteArray();
        _current_document.ScannedDate = WGDB.Now;

        if (_is_new)
        {
          Mdl.ScannedDocuments.Add(_current_document);
        }

        Mdl.ScannedDocumentsChanged = true;
      }
      return Mdl;
    }

    /// <summary>
    ///   create a new model for the current customer
    /// </summary>
    /// <param name="CrdData"></param>
    /// <returns></returns>
    public override PlayerEditDetailsReceptionModel CreateModelFromCardData(CardData CrdData)
    {
      var _model = CreateModelFromCardDataCommon(CrdData);
      _model.VisibleTrackData = CrdData.VisibleTrackdata();

      return _model;
    }

    /// <summary>
    ///   update the model to represent the currently selected customer
    /// </summary>
    /// <param name="CrdData"></param>
    /// <param name="Mdl"></param>
    /// <returns></returns>
    protected override PlayerEditDetailsReceptionModel UpdateModelFromCardData(CardData CrdData,
      PlayerEditDetailsReceptionModel Mdl)
    {
      Mdl.VisibleTrackData = CrdData.VisibleTrackdata();
      Mdl.CreationDate = null;
      return UpdateModelFromCardDataCommon(CrdData, Mdl);
    }

    public override List<InvalidField> CheckDuplicateAccounts()
    {
      List<InvalidField> _errors = new List<InvalidField>();
      var _model = Model;

      if (_model == null || String.IsNullOrEmpty(_model.HolderName1) || String.IsNullOrEmpty(_model.HolderName3) ||
          Equals(_model.HolderBirthDate, DateTime.MinValue) || _model.HolderGender == null) return _errors;
      CardData _card_data = UpdateCardFromModel((CardData)CardData.Clone(), _model);

      if (_card_data == null) return _errors;
      String _duplicate_msg;
      Boolean _allow_duplicate;

      if (!CardData.CheckDuplicateAccountsReception(_card_data, out _duplicate_msg, out _allow_duplicate))
      {
        _errors.Add(new InvalidField
        {
          Id = "AccountDuplicated",
          Reason = _duplicate_msg.Replace(":", "")
        });
        if (_model.AccountId < 0 && !_showndupdetails)
        {
          frm_message.Show(_duplicate_msg.Replace(":", ""), Resource.String("STR_APP_GEN_MSG_WARNING"),
            MessageBoxButtons.OK, ViewForm);
          _showndupdetails = true;
        }
        if (!_model.DuplicatedDetails)
        {
          Model.DuplicatedDetails = true;
        }
      }
      else
      {
        if (_model.DuplicatedDetails)
        {
          Model.DuplicatedDetails = false;
        }
      }

      return _errors;
    }

    /// <summary>
    ///   show more results in principal grid view if available
    /// </summary>
    internal void ShowMoreResults()
    {
      try
      {
        var _mdl = Model;
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand())
          {
            _cmd.CommandText = string.Format(_mdl.SearchStringSQL,
              " and AC_ACCOUNT_ID>" + _mdl.GridData[_mdl.GridData.Count - 1].AccountNumber + " ");
            _cmd.Connection = _db_trx.SqlTransaction.Connection;
            _cmd.Transaction = _db_trx.SqlTransaction;
            using (SqlDataReader _sql_rdr = _cmd.ExecuteReader())
            {
              int _results_count = 0;
              while (_sql_rdr.Read())
              {
                _mdl.GridData.Add(new CustomerSearchResult
                {
                  AccountNumber = _sql_rdr.IsDBNull(0) ? -1 : _sql_rdr.GetInt64(0),
                  Name = _sql_rdr.IsDBNull(2) ? "" : _sql_rdr.GetString(2),
                  Lastname = _sql_rdr.IsDBNull(3) ? "" : _sql_rdr.GetString(3),
                  Document = _sql_rdr.IsDBNull(4) ? "" : _sql_rdr.GetString(4),
                  BirthDate = _sql_rdr.IsDBNull(5) ? DateTime.MinValue : _sql_rdr.GetDateTime(5)
                });
                _results_count++;
                if (_results_count == m_grid_results_max)
                {
                  break;
                }
              }
              _mdl.NoMoreResults = !_sql_rdr.Read();
            }
          }
        }
        Model = _mdl;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    }

    /// <summary>
    /// show more results in principal grid view if available
    /// </summary>
    /// <param name="TopRecords"></param>
    internal void ShowMoreResults(int TopRecords)
    {
      try
      {
        var _mdl = Model;
        string _query;

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand())
          {

            _query = _mdl.SearchStringSQL.Replace("@N", TopRecords.ToString());

            _cmd.CommandText = string.Format(_query,
              " and AC_ACCOUNT_ID>" + _mdl.GridData[_mdl.GridData.Count - 1].AccountNumber + " ");

            _cmd.Connection = _db_trx.SqlTransaction.Connection;
            _cmd.Transaction = _db_trx.SqlTransaction;
            using (SqlDataReader _sql_rdr = _cmd.ExecuteReader())
            {
              int _results_count = 0;
              while (_sql_rdr.Read())
              {
                _mdl.GridData.Add(new CustomerSearchResult
                {
                  AccountNumber = _sql_rdr.IsDBNull(0) ? -1 : _sql_rdr.GetInt64(0),
                  Name = _sql_rdr.IsDBNull(2) ? "" : _sql_rdr.GetString(2),
                  Lastname = _sql_rdr.IsDBNull(3) ? "" : _sql_rdr.GetString(3),
                  Document = _sql_rdr.IsDBNull(4) ? "" : _sql_rdr.GetString(4),
                  BirthDate = _sql_rdr.IsDBNull(5) ? DateTime.MinValue : _sql_rdr.GetDateTime(5)
                });
                _results_count++;
                if (_results_count == TopRecords)
                {
                  break;
                }
              }

              if (_results_count < TopRecords)
              {
                _mdl.NoMoreResults = true;
              }
            }
          }
        }
        Model = _mdl;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } //ShowMoreResults

    /// <summary>
    ///   procedure to create a new customer
    /// </summary>
    /// <returns></returns>
    internal bool NewCustomer()
    {
      try
      {
        if (GeneralParam.GetBoolean("Reception", "RequireNewCard", true) && m_tracknumber == null)
        {
          using (frm_yesno _shader = new frm_yesno())
          {
            _shader.Opacity = 0.6;
            _shader.Show((Form)View);

            using (frm_new_card_prompt _frm_new_card_prompt = new frm_new_card_prompt())
            {
              _frm_new_card_prompt.StartPosition = FormStartPosition.CenterParent;
              var _dlg_result = _frm_new_card_prompt.ShowDialog(_shader);
              //prompt to scan card
              switch (_dlg_result)
              {
                case DialogResult.OK:
                  if (!string.IsNullOrEmpty(_frm_new_card_prompt.TrackData))
                  {
                    var _old_model = Model;
                    CardData = new CardData { TrackData = _frm_new_card_prompt.TrackData };
                    CardData.PlayerTracking.CardLevel = 1;
                    var _model = InjectScannedInfoToNewRecord(CardData, _old_model, ScannedInfo);
                    _model.IsNew = CardData.AccountId == 0; //EOR 20-FEB-2018
                    Model = _model;
                    LastModel = _model.Clone();
                    return true;
                  }
                  break;
                case DialogResult.Abort:
                  frm_message.Show(Resource.String("MSG_CARD_ALREADY_ASSIGNED"),
                    Resource.String("STR_APP_GEN_MSG_WARNING"), MessageBoxButtons.OK, Images.CashierImage.Warning,
                    (Form)View);
                  break;
                //card not blank
              }
            }
          }
        }
        else
        {
          var _old_model = Model;
          CardData = new CardData();
          CardData.TrackData = m_tracknumber;
          CardData.PlayerTracking.CardLevel = 1;
          m_tracknumber = null;
          var _model = InjectScannedInfoToNewRecord(CardData, _old_model, ScannedInfo);

          _model.IsNew = CardData.AccountId == 0; // LTC 2018-MAR-29
          Model = _model;
          LastModel = _model.Clone();
          return true;
        }
      }

      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      return false;
    }

    private PlayerEditDetailsReceptionModel InjectScannedInfoToNewRecord(CardData CrdData, PlayerEditDetailsReceptionModel _old_model, IDScannerInfo Info)
    {
      var _model = UpdateModelFromCardData(CrdData, _old_model);
      _model.AccountId = -1;
      _model.GridData = null;
      _model.HolderDocumentType = _model.DocumentTypes[0];
      _model.IDDocuments = new Dictionary<int, string>();
      _model.IsNew = false;
      if (Info != null)
      {
        ApplyIDScannerInfoToModel(Info, _model);

      }
      return _model;
    }

    /// <summary>
    ///   print the current customers information
    /// </summary>
    /// <returns></returns>
    public override List<InvalidField> PrintInformation()
    {
      return PrintInformationCommon();
    }

    /// <summary>
    ///   refresh the black list information for the current customer
    /// </summary>
    public void RefreshBlackListInfo(bool Async)
    {
      PlayerEditDetailsReceptionModel _model = Model;
      if (Async)
      {
        CheckCustomerInBlackList(_model);
      }
      else
      {
        BlackListResultInfo _blacklist_result = Customers.CheckAccountInBlackList(_model);
        UpdateModelBlackListInfo(_model, _blacklist_result);
      }
    }

    /// <summary>
    ///   scan document facade
    /// </summary>
    public void ScanDocument()
    {
      try
      {
        ScanDocument(null);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    }

    /// <summary>
    ///   scan document (false method await scanner to test)
    /// </summary>
    /// <param name="CurrentDocument"></param>
    public void ScanDocument(ScannedDocument CurrentDocument)
    {
      bool _is_new = CurrentDocument == null;
      var _model = Model;
      if (IDScanner != null)
        IDScanner.DetatchFromAllAvailable(HandleIDScannerEvent);
      using (frm_reception_scan_document _frm_scan_document = new frm_reception_scan_document())
      {
        DialogResult _result = _frm_scan_document.Show(_is_new, CurrentDocument,
          new List<ScannedDocument>(_model.ScannedDocuments), IDScanner);
        if (_result != DialogResult.OK)
        {
          return;
        }

        if (_is_new)
        {
          CurrentDocument = new ScannedDocument();
        }

        CurrentDocument.DocumentTypeId = _frm_scan_document.DocumentTypeId;
        CurrentDocument.DocumentTypeDescription = _frm_scan_document.DocumentTypeDescription;
        CurrentDocument.Expiration = _frm_scan_document.ExpirationDate;
        CurrentDocument.Image = _frm_scan_document.ScannedImage.ToByteArray();
        CurrentDocument.ScannedDate = WGDB.Now;
        if (_is_new)
        {
          _model.ScannedDocuments.Add(CurrentDocument);
        }
        _model.RecordExpirationDate = Customers.GetDefaultExpirationDate();

        _model.ExpiredDocumentsResult = CustomerHasExpiredDocuments(_model)
          ? ExpiredDocumentsResult.HasExpiredDocuments
          : ExpiredDocumentsResult.DoesNotHaveExpiredDocuments;

        _model.ScannedDocumentsChanged = true;
        Model = _model;
      }
      if (IDScanner != null)
        IDScanner.AttatchToAllAvailable(HandleIDScannerEvent);
    }

    /// <summary>
    ///   copy fiscal address to alternative address
    /// </summary>
    internal void CopyAddressToAlternative()
    {
      var _model = Model;
      _model.HolderAlternativeAddressLine1 = _model.HolderAddressLine01;
      _model.HolderAlternativeAddressLine2 = _model.HolderAddressLine02;
      _model.HolderAlternativeAddressLine3 = _model.HolderAddressLine03;
      _model.HolderAlternativeCountry = _model.HolderAddressCountry;

      _model.HolderAlternativeHouseNumber = _model.HolderExtNum;
      _model.HolderAlternativePostCode = _model.HolderZip;
      _model.HolderAlternativeState = _model.HolderFedEntity;
      _model.HolderAlternativeTown = _model.HolderCity;
      Model = _model;
    }

    /// <summary>
    ///   update record expiration
    /// </summary>
    internal void UpdateRecordExpiration()
    {
      var _model = Model;

      if (_model.AccountId.HasValue)
      {
        DateTime? _record_expiration = Customers.UpdateRecordExpiration(_model.AccountId.Value);
        if (_record_expiration.HasValue)
        {
          LoadScannedDocuments();
          _model.RecordExpirationDate = _record_expiration.Value;

          _model.ExpiredDocumentsResult = CustomerHasExpiredDocuments(_model)
            ? ExpiredDocumentsResult.HasExpiredDocuments
            : ExpiredDocumentsResult.DoesNotHaveExpiredDocuments;

          Model = _model;
        }
        else
        {
          frm_message.Show(Resource.String("MSG_ERROR_UPDATE_RECORD_EXPIRATION"),
            Resource.String("STR_APP_GEN_MSG_WARNING"), MessageBoxButtons.OK, Images.CashierImage.Warning, (Form)View);
        }
      }
    }

    /// <summary>
    ///   Perform entry
    /// </summary>
    /// <param name="ErrorDisplay"></param>
    internal List<InvalidField> PerformEntry(
      PlayerEditDetailsCommonErrorDisplay<PlayerEditDetailsReceptionModel> ErrorDisplay)
    {
      string _error_str;
      CashierSessionInfo _csi;
      int _time_before_entry;
      DateTime _customer_created;
      DateTime _account_created;
      TimeSpan _ts;
      StringBuilder _error_str_without_confirmation;
      StringBuilder _error_str_with_confirmation;
      List<InvalidField> _list_invalid_field;
      List<InvalidField> _errors;
      List<ProfilePermissions.CashierFormFuncionality> _list_profile_permissions;
      string _newline;

      _error_str_with_confirmation = new StringBuilder();
      _error_str_without_confirmation = new StringBuilder();
      _list_invalid_field = new List<InvalidField>();
      _errors = new List<InvalidField>();
      _csi = Cashier.CashierSessionInfo();
      _list_profile_permissions = new List<ProfilePermissions.CashierFormFuncionality>();
      _newline = "\n";

      // 1. Check Cashier Session
      if (_csi.CashierSessionId == 0)
      {
        frm_message.Show(Resource.String("STR_FRM_RECEPTION_ENTRY_ERROR_CASHIER_SESSION"),
          Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK,
          Images.CashierImage.Error, (View as Form));

        return _list_invalid_field;
      }

      if (BlockIfNotSaved(ErrorDisplay))
      {
        return _list_invalid_field;
      }
      var _model = Model;
      var oldblackliststate = _model.BlackListResult;

      // 2. Check Is in blackList
      if (_model.BlackListEntryBlocked)
      {
        _error_str_without_confirmation.AppendLine(Resource.String("STR_FRM_RECEPTION_IN_BLACKLIST") + _newline);
        _list_invalid_field.Add(new InvalidField()
        {
          Id = "AcknowledgedBlackList",
          Reason = Resource.String("STR_FRM_RECEPTION_IN_BLACKLIST")
        });
      }

      //3. Check customer creation date and TimeToEntry GP
      _time_before_entry = GeneralParam.GetInt32("Reception", "TimeBeforeEntry", 0); //Hours
      _customer_created = _model.CreationDate ?? WGDB.Now;
      _account_created = _model.CardCreationDate == default(DateTime) ? _customer_created : _model.CardCreationDate;

      _ts = WGDB.Now - _account_created;
      if (_time_before_entry > 0 && _ts.TotalHours < _time_before_entry)
      {
        _error_str_without_confirmation.AppendLine(Resource.String(
          "STR_FRM_RECEPTION_PERFORM_ENTRY_ERROR_TIMEBEFOREENTRY", _customer_created.ToShortDateString(),
          _time_before_entry).Replace("\\r\\n", "\r\n"));
      }

      //4. Searching in expired documents
      //5. Expired documents - ERROR
      if (_model.ExpiredDocumentsResult == ExpiredDocumentsResult.None
          || _model.ExpiredDocumentsResult == ExpiredDocumentsResult.Processing
          || _model.ExpiredDocumentsResult == ExpiredDocumentsResult.Error)
      {
        _error_str_with_confirmation.AppendLine(
          Resource.String("STR_FRM_RECEPTION_PERFORM_ENTRY_ERROR_EXPIRED_DOCUMENTS") + _newline);
      }

      //6. Search in blacklist and refresh blacklist info
      if (_model.BlackListResult == BlackListResult.None)
      {
        RefreshBlackListInfo(false);
        oldblackliststate = _model.BlackListResult;
      }

      if (
        !(_model.BlackListResult == BlackListResult.NotInBlackList || _model.BlackListResult == BlackListResult.None) &&
        !_model.AcknowledgedBlackList)
      {
        if (_model.BlackListResult == BlackListResult.InBlackList
            && !_model.BlackListEntryBlocked)
        {
          _error_str_without_confirmation.AppendLine(Resource.String("STR_FRM_RECEPTION_IN_BLACKLIST"));

          _list_invalid_field.Add(new InvalidField()
          {
            Id = "AcknowledgedBlackList",
            Reason = Resource.String("STR_FRM_RECEPTION_IN_BLACKLIST")
          });
        }

        //7. Searching in black list 
        //8. Black list - ERROR
        if (_model.BlackListResult == BlackListResult.Error)
        {
          _error_str_with_confirmation.AppendLine(Resource.String("STR_FRM_RECEPTION_PERFORM_ENTRY_ERROR_BLACKLIST") +
                                                  _newline);
        }
      }
      //9. Check Expired Documents
      if (_model.ExpiredDocumentsResult == ExpiredDocumentsResult.HasExpiredDocuments)
      {
        _error_str_with_confirmation.AppendLine(Resource.String("STR_FRM_RECEPTION_SEARCH_EXPIRED_DOCUMENTS") + _newline);
        _errors.Add(new InvalidField
        {
          Id = "ScannedDocumentsGrid",
          Reason = Resource.String("STR_FRM_RECEPTION_SEARCH_EXPIRED_DOCUMENTS")
        });
      }

      //10. Check blacklist result
      if (!CheckIfError(_model.BlackListResultCustomer) && (_model.BlackListResult == BlackListResult.InBlackList || _model.BlackListResult == BlackListResult.WithMatches))
      {
        _error_str_with_confirmation.AppendLine(Resource.String("STR_FRM_RECEPTION_IN_BLACKLIST") + _newline);
      }
      if (_model.BlackListResult == BlackListResult.Processing)
      {
        _error_str_with_confirmation.AppendLine(
          Resource.String("STR_FRM_RECEPTION_PERFORM_ENTRY_ERROR_BLACKLIST_PROCESSING") + _newline);
      }
      //11. SHOW Message And Manage Permissions
      // 11.1. Messages WITHOUT confirmation to continue
      if (!String.IsNullOrEmpty(_error_str_without_confirmation.ToString()))
      {
        DialogResult _result = frm_message.ShowReceptionAllowEntry((View as Form),
          Resource.String("STR_FRM_RECEPTION_PERFORM_ENTRY_NOT_REGISTERED") +
          _newline + _newline + Resource.String("STR_FRM_RECEPTION_PERFORM_ENTRY_ISSUES") + "\n" + _newline +
          _error_str_without_confirmation.ToString(), MessageBoxButtons.OK);

        return _list_invalid_field;
      }
      // 11.2. Messages WITH confirmation to continue
      if (!String.IsNullOrEmpty(_error_str_with_confirmation.ToString()))
      {
        //11.2.1 Show Message
        DialogResult _result = frm_message.ShowReceptionAllowEntry((View as Form),
          _error_str_with_confirmation.ToString(), MessageBoxButtons.OKCancel);
        if (_result != DialogResult.OK)
        {
          return _list_invalid_field;
        }
        if (oldblackliststate != _model.BlackListResult)
        {
          return new List<InvalidField>() { new InvalidField() { Id = "", Reason = Resource.String("STR_FRM_RECEPTION_PERFORM_ENTRY_BLACKLIST_STATUS_CHANGED") } };
        }

        // 11.2.2 Check Permissison (Expired Documents)
        if (_model.ExpiredDocumentsResult == ExpiredDocumentsResult.HasExpiredDocuments)
        {
          _list_profile_permissions.Add(
            ProfilePermissions.CashierFormFuncionality.Reception_AllowEntryWithExpiredDocuments);
        }
        // 11.2.3 Check Permissison (Is in BlackList)
        if (_model.BlackListResult == BlackListResult.InBlackList
            || _model.BlackListResult == BlackListResult.WithMatches
            || _model.BlackListResult == BlackListResult.Processing)
        {
          _list_profile_permissions.Add(ProfilePermissions.CashierFormFuncionality.Reception_AllowEntryBeingInBlackList);
        }

        if (_list_profile_permissions.Count > 0)
        {
          if (!ProfilePermissions.CheckPermissionList(_list_profile_permissions,
            ProfilePermissions.TypeOperation.RequestPasswd, (View as Form), 0, out _error_str))
          {
            frm_message.Show(_error_str, Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK,
              Images.CashierImage.Error, (View as Form));

            return _errors;
          }
        }
      }
      if (oldblackliststate != _model.BlackListResult)
      {
        return new List<InvalidField>() { new InvalidField() { Id = "", Reason = Resource.String("STR_FRM_RECEPTION_PERFORM_ENTRY_BLACKLIST_STATUS_CHANGED") } };
      }

      // 12.
      ENUM_RECEPTION_MODE _reception_mode = Entrance.GetReceptionMode();

      //  Flyers
      if (Entrance.GetReceptionMode() == ENUM_RECEPTION_MODE.TicketOnly || Entrance.GetReceptionMode() == ENUM_RECEPTION_MODE.Lite)
      {

        //Put this here or in ReceptionModeStrategy????
        if (JunketsBusinessLogic.IsJunketsEnabled())
        {
          frm_input_flyer _input_flyer = new frm_input_flyer();
          frm_yesno _shader = new frm_yesno();

          _shader.Opacity = 0.6f;
          _shader.Show();

          // Form requesting to enter flyer code
          _input_flyer.Show((long)_model.AccountId, null, out m_junket_info);

          _shader.Hide();
          _shader.Dispose();

          _input_flyer.Hide();
          _input_flyer.Dispose(); //Dispose of Flyer

        }
      } // Flyers

      EntryManager _entry_manager = new EntryManager();

      if (Entrance.GetReceptionMode() == ENUM_RECEPTION_MODE.TicketOnly || Entrance.GetReceptionMode() == ENUM_RECEPTION_MODE.Lite)
      {
        if (this.m_junket_info != null)
        {
        _entry_manager.FlyerCode = m_junket_info.BusinessLogic.FlyerCode;
      }
      }

      _entry_manager.SetStrategy(_reception_mode);
      var _entry_result = _entry_manager.PerformEntry(_model, CreateAccountsReceptionFromModel(_model), CardData);
      Model = _model;


      if (!_entry_result.HasValue)
      {
        return new List<InvalidField>();
      }
      if (_entry_result.Value)
      {
        ((Form)View).DialogResult = DialogResult.OK; //close form

        return new List<InvalidField>();
      }

      frm_message.Show(Resource.String("STR_FRM_RECEPTION_PERFORM_ENTRY_NOT_FINISHED"),
        Resource.String("STR_APP_GEN_MSG_WARNING"), MessageBoxButtons.OK, Images.CashierImage.Error, (View as Form));

      return new List<InvalidField>();
    }

    /// <summary>
    /// Set exit
    /// </summary>
    /// <param name="ErrorDisplay"></param>
    /// <returns></returns>
    internal List<InvalidField> PerformExit(PlayerEditDetailsCommonErrorDisplay<PlayerEditDetailsReceptionModel> ErrorDisplay)
    {
      var _model = Model;
      Int32 _visits_updated;
      EntryManager _entry_manager;
      ENUM_RECEPTION_MODE _reception_mode;

      try
      {
        _reception_mode = Entrance.GetReceptionMode();
        _entry_manager = new EntryManager();

        _entry_manager.SetStrategy(_reception_mode);

        _entry_manager.PerformExit(_model, CreateAccountsReceptionFromModel(_model), out _visits_updated);

        if (_visits_updated == 0)
        {
          return new List<InvalidField>() { new InvalidField() { Id = "", Reason = Resource.String("STR_FRM_RECEPTION_NO_ENTRANCE") } };
        }

        ((Form)View).DialogResult = DialogResult.OK; //close form     

        return new List<InvalidField>();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return new List<InvalidField>() { new InvalidField() { Id = "", Reason = Resource.String("STR_FRM_RECEPTION_NO_ENTRANCE") } };
    } // PerformExit

    /// <summary>
    ///   Load Scanned documents if necessary
    /// </summary>
    internal void LoadScannedDocuments()
    {
      try
      {
        var _model = Model;

        if (!_model.AccountId.HasValue)
        {
          return;
        }

        if (!_model.SearchScannedDocuments)
        {
          return;
        }
        List<CustomerScannedDocument> _scanned_documents = Customers.GetScannedDocuments(_model.AccountId.Value);
        foreach (var _scanned_document in _scanned_documents)
        {
          _model.ScannedDocuments.Add(new ScannedDocument
          {
            DocumentTypeId = _scanned_document.DocumentTypeId,
            DocumentTypeDescription = _scanned_document.DocumentTypeDescription,
            Expiration = _scanned_document.Expiration,
            Image = _scanned_document.Image,
            ScannedDate = _scanned_document.ScannedDate
          });
        }

        _model.SearchScannedDocuments = false;
      }
      catch (Exception)
      {
        frm_message.Show(Resource.String("STR_APP_GEN_ERROR_DATABASE_ERROR_READING"),
          Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, Images.CashierImage.Error, (View as Form));
      }
    }

    internal void AddToBlackList()
    {
      try
      {
        string _error_str;
        //Only continue if the user has the permission
        if (
          !ProfilePermissions.CheckPermissionList(
            ProfilePermissions.CashierFormFuncionality.Reception_AllowAddAccountToLocalBlackList,
            ProfilePermissions.TypeOperation.RequestPasswd, (View as Form), 0, out _error_str))
        {
          frm_message.Show(_error_str, Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK,
            Images.CashierImage.Error, (View as Form));
          return;
        }

        var _model = Model;

        if (!_model.AccountId.HasValue)
        {
          return;
        }

        using (frm_add_account_blacklist _frm_add_account_blacklist = new frm_add_account_blacklist())
        {
          DialogResult _result = _frm_add_account_blacklist.Show(_model.AccountId.Value);

          if (_result == DialogResult.OK)
          {
            CheckCustomerInBlackList(_model);
          }
        }
      }
      catch (Exception)
      {
        frm_message.Show(Resource.String("STR_APP_GEN_ERROR_DATABASE_ERROR_WRITING"),
          Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, Images.CashierImage.Error, (View as Form));
      }

    }

    /// <summary>
    /// Remove form black list
    /// </summary>
    internal void RemoveFromBlackList()
    {
      CardData _card;
      AccountBlockUnblock _account_block_unblock;
      String _error_str;

      if (!Misc.IsReceptionEnabled())
      {
        return;
      }

      _card = new CardData();

      try
      {
        //Only continue if the user has the permission
        if (!ProfilePermissions.CheckPermissionList(
             ProfilePermissions.CashierFormFuncionality.Reception_AllowRemoveAccountFromLocalBlackList,
             ProfilePermissions.TypeOperation.RequestPasswd, (View as Form), 0, out _error_str))
        {
          frm_message.Show(_error_str, Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, Images.CashierImage.Error, (View as Form));

          return;
        }

        var _model = Model;

        if (!_model.AccountId.HasValue)
        {
          return;
        }

        if (!CardData.DB_CardGetAllData(_model.AccountId.Value, _card))
        {
          return;
        }

        if (_card.Blocked)
        {
          _account_block_unblock = new AccountBlockUnblock(_card, AccountBlockUnblockSource.RECEPTION_BLACKLIST, "", false);

          try
          {
            using (DB_TRX _db_trx = new DB_TRX())
            {
              if (_account_block_unblock.ProceedToBlockUnblockAccount(Cashier.CashierSessionInfo(), false, _db_trx.SqlTransaction, out _card))
              {
                Customers.RemoveFromBlacklist(_card, Cashier.CashierSessionInfo(), _db_trx.SqlTransaction);
                _account_block_unblock.AddCashierMovementsBlacklist(Cashier.CashierSessionInfo(), CASHIER_MOVEMENT.ACCOUNT_REMOVE_BLACKLIST, _db_trx.SqlTransaction);

                _db_trx.Commit();
              }
            }
          }
          catch (Exception _ex)
          {
            Log.Exception(_ex);
          }
        }
        else
        {
          Customers.RemoveFromBlacklist(_card, Cashier.CashierSessionInfo());
        }

        //Update black list information
        CheckCustomerInBlackList(_model);
      }
      catch (Exception)
      {
        frm_message.Show(Resource.String("STR_APP_GEN_ERROR_DATABASE_ERROR_WRITING")
                        , Resource.String("STR_APP_GEN_MSG_ERROR")
                        , MessageBoxButtons.OK
                        , Images.CashierImage.Error
                        , (View as Form));
      }
    } // RemoveFromBlackList

    /// <summary>
    ///   set the format of the scanned documents data grid
    /// </summary>
    public void SetFormatDataGridScanned(DataGridView DataGrid)
    {
      string _date_format;
      DataGrid.AutoGenerateColumns = false;

      _date_format = System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern;

      // Set row height for bigger font sizes
      DataGrid.ColumnHeadersHeight = 30;
      DataGrid.RowTemplate.Height = 45;
      DataGrid.RowTemplate.MinimumHeight = 45;
      DataGrid.RowTemplate.DividerHeight = 0;

      // Columns
      DataGridViewColumn _col1 = new DataGridViewTextBoxColumn
      {
        DataPropertyName = "DocumentTypeDescription",
        Name = "columnScannedDocumentDocumentType",
        HeaderText = Resource.String("STR_FRM_PLAYER_SEARCH_SCANNED_DOCUMENTS_DOCUMENT_TYPE"),
        AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
      };
      _col1.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

      DataGridViewColumn _col2 = new DataGridViewTextBoxColumn
      {
        DataPropertyName = "ScannedDate",
        Name = "columnScannedDocumentDate",
        HeaderText = Resource.String("STR_FRM_PLAYER_SEARCH_SCANNED_DOCUMENTS_SCANNED_DATE"),
        Width = 150
      };
      _col2.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
      _col2.DefaultCellStyle.Format = _date_format;

      DataGridViewColumn _col3 = new DataGridViewTextBoxColumn
      {
        DataPropertyName = "Expiration",
        Name = "columnScannedDocumentExpiration",
        HeaderText = Resource.String("STR_FRM_PLAYER_SEARCH_SCANNED_DOCUMENTS_EXPIRATION_DATE"),
        Width = 150
      };
      _col3.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
      _col3.DefaultCellStyle.Format = _date_format;

      //DataGridViewButtonColumn col4 = new DataGridViewButtonColumn()
      //{
      //  Name = "columnScannedDocumentRescan",
      //  HeaderText = Resource.String("STR_FRM_PLAYER_SEARCH_SCANNED_DOCUMENTS_SCAN"),
      //  Text = Resource.String("STR_FRM_PLAYER_SEARCH_SCANNED_DOCUMENTS_SCAN"),
      //  UseColumnTextForButtonValue = true,
      //  Width = 100,
      //};
      DataGridViewButtonColumn _col4 = new DataGridViewButtonColumn
      {
        Name = "columnScannedDocumentRescan",
        HeaderText = Resource.String("STR_FRM_PLAYER_SEARCH_SCANNED_DOCUMENTS_SCAN"),
        Text = Resource.String("STR_FRM_PLAYER_SEARCH_SCANNED_DOCUMENTS_SCAN"),
        UseColumnTextForButtonValue = true,
        CellTemplate = new ScanButtonCell(),
        Width = 100,
        DefaultCellStyle = { Alignment = DataGridViewContentAlignment.MiddleCenter }
      };

      DataGrid.Columns.Add(_col1);
      DataGrid.Columns.Add(_col2);
      DataGrid.Columns.Add(_col3);
      DataGrid.Columns.Add(_col4);
      DataGrid.CellFormatting += grd_scanned_docs_CellFormatting;
    } //SetFormatDataGridScanned

    /// <summary>
    ///   dynamically format grid row based on expiry date
    /// </summary>
    /// <param name="Sender"></param>
    /// <param name="E"></param>
    private void grd_scanned_docs_CellFormatting(object Sender, DataGridViewCellFormattingEventArgs E)
    {
      ScannedDocument _selected_scanned_document =
        (ScannedDocument)((DataGridView)Sender).Rows[E.RowIndex].DataBoundItem;

      if (_selected_scanned_document.IsExpired)
      {
        E.CellStyle.BackColor = m_expired_color;
        E.CellStyle.SelectionForeColor = m_expired_font_color;
        E.CellStyle.SelectionBackColor = m_expired_color_highlight;
      }
    } //grd_scanned_docs_CellFormatting

    /// <summary>
    ///   set the format of the main data grid
    /// </summary>
    public void SetFormatDataGridPrincipal(DataGridView DataGrid)
    {
      string _date_format;
      try
      {
        DataGrid.AutoGenerateColumns = false;
        DataGrid.MultiSelect = false;
        DataGrid.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

        _date_format = System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern;

        // Set row height for bigger font sizes
        DataGrid.ColumnHeadersHeight = 35;
        DataGrid.RowTemplate.Height = 45;
        DataGrid.RowTemplate.MinimumHeight = 45;
        DataGrid.RowTemplate.DividerHeight = 0;

        // Columns
        //AccountNumber { get; set; }
        //Name { get; set; }
        //Document { get; set; }
        //BirthDate { get; set; }

        DataGridViewColumn _col1 = new DataGridViewTextBoxColumn
        {
          DataPropertyName = "AccountNumber",
          Name = "AccountNumber",
          HeaderText = Resource.String("STR_FRM_PLAYER_SEARCH_RESULTS_002"),
          Width = 90
        };
        _col1.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        _col1.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

        DataGridViewColumn _col2 = new DataGridViewTextBoxColumn
        {
          DataPropertyName = "Name",
          Name = "Name",
          HeaderText = Resource.String("STR_FRM_PLAYER_SEARCH_RESULTS_003"),
          AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        };
        _col2.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
        _col2.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;


        DataGridViewColumn _col5 = new DataGridViewTextBoxColumn
        {
          DataPropertyName = "Lastname",
          Name = "Lastname",
          HeaderText = Resource.String("STR_FRM_PLAYER_SEARCH_RESULTS_007"),
          AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        };
        _col2.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
        _col2.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;


        DataGridViewColumn _col3 = new DataGridViewTextBoxColumn
        {
          DataPropertyName = "Document",
          Name = "Document",
          HeaderText = Resource.String("STR_FRM_PLAYER_SEARCH_RESULTS_004"),
          Width = 200
        };
        _col3.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
        _col3.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

        DataGridViewColumn _col4 = new DataGridViewTextBoxColumn
        {
          DataPropertyName = "BirthDate",
          Name = "BirthDate",
          HeaderText = Resource.String("STR_FRM_PLAYER_SEARCH_RESULTS_005"),
          Width = 170
        };
        _col4.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
        _col4.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
        _col4.DefaultCellStyle.Format = _date_format;

        DataGrid.Columns.Add(_col1);
        DataGrid.Columns.Add(_col2);
        DataGrid.Columns.Add(_col5);
        DataGrid.Columns.Add(_col3);
        DataGrid.Columns.Add(_col4);
        DataGrid.CellFormatting += grd_customers_CellFormatting;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } //SetFormatDataGridPrincipal  

    /// <summary>
    ///   Override default cell highlighting, beating the selected style
    ///   to it at the last possible point before render
    /// </summary>
    /// <param name="Sender"></param>
    /// <param name="E"></param>
    private void grd_customers_CellFormatting(object Sender, DataGridViewCellFormattingEventArgs E)
    {
      ((DataGridView)Sender).Rows[E.RowIndex].DefaultCellStyle.SelectionBackColor = Color.FromArgb(45, 47, 50);
      ((DataGridView)Sender).Rows[E.RowIndex].DefaultCellStyle.SelectionForeColor = Color.FromArgb(244, 246, 249);
    } //grd_customers_CellFormatting

    /// <summary>
    /// Allows to validate id customer is in blacklist if there is more than one match on the blacklist 
    /// </summary>
    public void BlacklistValidateMatches()
    {
      DialogResult _dlg;
      var _model = Model;
      bool _validated = (_model.BlackListResult == BlackListResult.InBlackList ||
                         _model.BlackListResult == BlackListResult.NotInBlackList);

      try
      {
        using (frm_yesno _shader = new frm_yesno())
        {
          using (BlackListMatchesView _blm_view = new BlackListMatchesView())
          {
            using (new BlackListMatchesController(_model.BlackListResultCustomer, _validated, _blm_view))
            {
              _shader.Opacity = 0.6;
              _shader.Show();
              if (WindowManager.StartUpMode == WindowManager.StartUpModes.Explorer)
              {
                _blm_view.Location = new Point(WindowManager.GetFormCenterLocation(_shader).X - (_blm_view.Width / 2),
                  _shader.Location.Y + 2);
              }
              _dlg = _blm_view.ShowDialog(_shader);

              if (_dlg == DialogResult.OK)
              {
                var _blrmodel = _blm_view.Model;
                List<BlackListResultCustomer> _blrc = _blrmodel.CustomerSelected ?? new List<BlackListResultCustomer>();
                _model.BlackListCustomerSelected = _blrc.ToArray();
                if (_blrc.Count > 0)
                {
                  _model.BlackListResult = BlackListResult.InBlackList;
                }
                else
                {
                  _model.BlackListResult = BlackListResult.NotInBlackList;
                }
              }
              Model = _model;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error("There was an error showing matches from Blacklist " + _ex.Message);
      }
    }

    internal BindingList<CustomerSearchResult> GenerateDuplicateDocumentMatches(long[] AccountIDs)
    {
      BindingList<CustomerSearchResult> _retval = new BindingList<CustomerSearchResult>();

      foreach (long _account_id in AccountIDs)
      {
        CardData _cd = new CardData();
        if (CardData.DB_CardGetAllData(_account_id, _cd, true))
        {
          _retval.Add(new CustomerSearchResult()
          {
            BlackListResultInfo = null,
            Document = _cd.PlayerTracking.HolderId,
            BirthDate = _cd.PlayerTracking.HolderBirthDate,
            AccountNumber = _account_id,
            Name = _cd.PlayerTracking.HolderName
            //Lastname ???
          });
        }
      }
      return _retval;
    }

    public Cashier.CashierOpenAutomaticallyResult ManagementReception(CASHIER_STATUS CashierStatus)
    {
      return CashierBusinessLogic.ManagementCashierSessionOpen(this.UcBank, (View as Form), CashierStatus);
    }

    private Boolean CheckIfError(List<BlackListResultCustomer> ListBlackListResultCustomer)
    {
      return CheckIfError(ListBlackListResultCustomer.ToArray());
    }

    /// <summary>
    /// Check if list  of black list contents a error type
    /// </summary>
    /// <param name="ListBlackListResultCustomer"></param>
    /// <returns></returns>
    private Boolean CheckIfError(BlackListResultCustomer[] ListBlackListResultCustomer)
    {
      try
      {
        foreach (BlackListResultCustomer _customer in ListBlackListResultCustomer)
        {
          if (_customer.ReasonType == ENUM_BLACKLIST_REASON.GENERIC_ERROR)
          {
            return true;
          }
        }

        return false;
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("CheckIfError: There was an error checking service response of blacklist. Details: {0}", _ex.Message));
      }

      return true;
    } // CheckIfError

  }

  /// <summary>
  ///   special cell type to create a data grid button column on the new style
  /// </summary>
  public class ScanButtonCell : DataGridViewButtonCell
  {
    /// <summary>
    ///   routine to paint special button on screen
    /// </summary>
    /// <param name="Graphics"></param>
    /// <param name="ClipBounds"></param>
    /// <param name="CellBounds"></param>
    /// <param name="RowIndx"></param>
    /// <param name="ElementState"></param>
    /// <param name="VValue"></param>
    /// <param name="FFormattedValue"></param>
    /// <param name="EErrorText"></param>
    /// <param name="CellStyle"></param>
    /// <param name="AdvancedBorderStyle"></param>
    /// <param name="PaintParts"></param>
    protected override void Paint(Graphics Graphics, Rectangle ClipBounds, Rectangle CellBounds, int RowIndx,
      DataGridViewElementStates ElementState, object VValue, object FFormattedValue,
      string EErrorText, DataGridViewCellStyle CellStyle,
      DataGridViewAdvancedBorderStyle AdvancedBorderStyle, DataGridViewPaintParts PaintParts)
    {
      var _btn = new uc_round_button
      {
        Style = uc_round_button.RoundButonStyle.PRINCIPAL,
        Width = ClipBounds.Width - 1,
        Height = ClipBounds.Height - 1,
        Text = Resource.String("STR_FRM_PLAYER_SEARCH_SCANNED_DOCUMENTS_SCAN")
      };
      Bitmap _bmp = new Bitmap(_btn.Width, _btn.Height);
      _btn.DrawToBitmap(_bmp, new Rectangle(0, 0, _btn.Width, _btn.Height));
      Graphics.DrawImage(_bmp, new Rectangle(ClipBounds.X + 1, ClipBounds.Y + 1, _btn.Width, _btn.Height));
    }
  }
}
