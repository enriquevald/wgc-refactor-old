﻿//------------------------------------------------------------------------------
// Copyright © 2007-2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: PlayerEditDetailsCashierController.cs
// 
//   DESCRIPTION: Implementation of cashier controller class
//                
//                
//                
//                
//                
//                
// 
//        AUTHOR: Scott Adamson.
// 
// CREATION DATE: 02-DEC-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 02-DEC-2007 SJA    First release.
// 06-APR-2016 SJA    Bug 11486: Cajero: no funciona el RFC ni el beneficiario
// 03-AGO-2016 FJC    Bug 16072:Recepción: Errores Varios
// 02-AGO-2016 JRC    PBI 16257:Buckets - RankingLevelPoints_Generated - RankingLevelPoints_Discretional
// 28-NOV-2016 SMN    Bug 21045:Cashier: Error al guardar documento RFC de una cuenta
// 19-JAN-2017 FOS    Fixed Bug 22330: Print ticket 2 times, when modify the user data.
// 17-MAR-2017 ETP    WIGOS-110: Creditlines - Paybacks
// 21-APR-2017 ETP    WIGOS-706: Junkets - Technical story - Comissions by Enrolled
// 03-OCT-2017 DHA    PBI 30017:WIGOS-4056 Payment threshold registration - Player registration
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Forms;
using WSI.Cashier.MVC.Controller.PlayerEditDetails.Common;
using WSI.Cashier.MVC.Interface;
using WSI.Cashier.MVC.Model.PlayerEditDetails;
using WSI.Cashier.MVC.View.PlayerEditDetails;
using WSI.Cashier.MVC.View.PlayerEditDetails.Interface;
using WSI.Common;
using WSI.Common.Entities.General;
using WSI.Common.Entrances;
using WSI.Common.Entrances.Entities;
using WSI.Common.Entrances.Enums;
using WSI.Common.CreditLines;
using WSI.Common.Junkets;

namespace WSI.Cashier.MVC.Controller.PlayerEditDetails
{
  public class PlayerEditDetailsCashierController : PlayerEditDetailsBaseController<PlayerEditDetailsCashierModel>
  {

    /// <summary>
    ///   constructor calling base constructor
    /// </summary>
    /// <param name="CrdData"></param>
    /// <param name="View"></param>
    public PlayerEditDetailsCashierController(CardData CrdData,
      IMVCView<PlayerEditDetailsCashierModel> View, IDScanner.IDScanner IDScanner, PlayerEditDetailsCashierView.SCREEN_MODE ScreenMode)
    {
      this.IDScanner = IDScanner;
      SaveExtention += SaveCustomerInformation;
      LoadExtention += LoadCustomerInformation;
      this.ScreenMode = ScreenMode;
      LoadView(CrdData, View);
    }

    //public PlayerEditDetailsCashierController(CardData _card_data, PlayerEditDetailsCashierView _view)
    //{
    //  // TODO: Complete member initialization
    //  this.IDScanner = IDScanner;
    //  this._card_data = _card_data;
    //  this._view = _view;
    //  this.IDScanner = IDScanner;
    //}



    private AccountsReception CreateAccountsReceptionFromModel(PlayerEditDetailsCashierModel Mdl)
    {
      AccountsReception _account_reception;
      CustomerAddress _customer_home_address;
      CustomerAddress _customer_secondary_address;

      _account_reception = new AccountsReception();
      _customer_home_address = new CustomerAddress();
      _customer_secondary_address = new CustomerAddress();

      try
      {
        if (Mdl.AccountId.HasValue)
        {
          //Account
          _account_reception.CreationDate = Mdl.CreationDate;
          _account_reception.AccountId = Mdl.AccountId.Value;
          _account_reception.BirthDate = Mdl.HolderBirthDate;
          _account_reception.Comments = Mdl.Comments;

          if (Mdl.HolderAddressCountry != null)
            _account_reception.Country = Mdl.HolderAddressCountry.Id;


          if (Mdl.HolderAddressCountry.Id == -1)
          {
            Mdl.HolderAddressCountry.Id = 0;
          }

          _account_reception.Document = Mdl.HolderDocument;

          if (Mdl.HolderDocumentType != null)
            _account_reception.DocumentType = Mdl.HolderDocumentType.Id;
          
          _account_reception.FirstName = Mdl.HolderName3;
          _account_reception.SecondName = Mdl.HolderName4;
          _account_reception.FirstSurname = Mdl.HolderName1;
          _account_reception.SecondSurname = Mdl.HolderName2;

          if (Mdl.HolderNationality != null)
            _account_reception.Nacionality = Mdl.HolderNationality.Id;

          if (Mdl.HolderGender != null)
            _account_reception.Gender = Mdl.HolderGender.Id;

          //Contact
          _account_reception.Contact.Email = Mdl.HolderEmail1;
          _account_reception.Contact.FirstTelephone = Mdl.HolderPhone1;
          _account_reception.Contact.SecondTelephone = Mdl.HolderPhone2;
          _account_reception.Contact.Twitter = Mdl.HolderTwitterAccount;
          _account_reception.Contact.Facebook = Mdl.Facebook;

          _account_reception.AceptedAgreement = Mdl.HasAceptedAgreement;
          _account_reception.AceptedAgreementDate = Mdl.AceptedAgreementDate;
          _account_reception.InBlackList = Mdl.BlackListResult;
          //Home Address
          _customer_home_address.AddressType = ENUM_ADDRESS.HOME_ADRESS;
          _customer_home_address.City = Mdl.HolderCity;

          if (Mdl.HolderAddressCountry != null)
            _customer_home_address.CountryAddress = Mdl.HolderAddressCountry.Id;

          _customer_home_address.Colony = Mdl.HolderAddressLine02;
          _customer_home_address.Cp = Mdl.HolderZip;
          _customer_home_address.Delegation = Mdl.HolderName3;
          _customer_home_address.Number = Mdl.HolderExtNum;
          _customer_home_address.Street = Mdl.HolderAddressLine01;

          if (Mdl.HolderFedEntity != null)
            _customer_home_address.FederalEntity = Mdl.HolderFedEntity.Id;

          _account_reception.Addresses.Add(_customer_home_address);

          //Secondary Address
          _customer_secondary_address.AddressType = ENUM_ADDRESS.SECONDARY_ADRESS;
          _customer_secondary_address.City = Mdl.HolderAlternativeTown;
          if (Mdl.HolderAlternativeCountry != null)
          {
            _customer_secondary_address.CountryAddress = Mdl.HolderAlternativeCountry.Id;
          }
          _customer_secondary_address.Colony = Mdl.HolderAlternativeAddressLine2;
          _customer_secondary_address.Cp = Mdl.HolderAlternativePostCode;
          _customer_secondary_address.Delegation = Mdl.HolderAlternativeAddressLine3;
          _customer_secondary_address.Number = Mdl.HolderAlternativeHouseNumber;
          _customer_secondary_address.Street = Mdl.HolderAlternativeAddressLine1;
          if (Mdl.HolderAlternativeState != null)
          {
            _customer_secondary_address.FederalEntity = Mdl.HolderAlternativeState.Id;
          }
          _account_reception.Addresses.Add(_customer_secondary_address);

          //Scanned Documents
          LoadScannedDocuments();

          foreach (var _scanned_document in Mdl.ScannedDocuments)
          {
            _account_reception.ScannedDocuments.Add(new CustomerScannedDocument
            {
              DocumentTypeDescription = _scanned_document.DocumentTypeDescription,
              DocumentTypeId = _scanned_document.DocumentTypeId,
              Expiration = _scanned_document.Expiration,
              Image = _scanned_document.Image,
              ScannedDate = _scanned_document.ScannedDate
            });
          }
          if (_account_reception.DocumentTypes == null)
            _account_reception.DocumentTypes = new Dictionary<int, string>();
          else
          {
            _account_reception.DocumentTypes.Clear();
          }
          foreach (var v in Mdl.IDDocuments)
          {
            if (!string.IsNullOrEmpty(v.Value))
              _account_reception.DocumentTypes.Add(v.Key, v.Value);
          }

          _account_reception.RecordExpiration = Mdl.RecordExpirationDate;

          return _account_reception;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      return null;
    }

    internal void LoadScannedDocuments()
    {
      try
      {
        var _model = Model;

        if (!_model.AccountId.HasValue)
        {
          return;
        }

        if (!_model.SearchScannedDocuments)
        {
          return;
        }
        List<CustomerScannedDocument> _scanned_documents = Customers.GetScannedDocuments(_model.AccountId.Value);
        foreach (var _scanned_document in _scanned_documents)
        {
          _model.ScannedDocuments.Add(new ScannedDocument
          {
            DocumentTypeId = _scanned_document.DocumentTypeId,
            DocumentTypeDescription = _scanned_document.DocumentTypeDescription,
            Expiration = _scanned_document.Expiration,
            Image = _scanned_document.Image,
            ScannedDate = _scanned_document.ScannedDate
          });
        }

        _model.SearchScannedDocuments = false;
      }
      catch (Exception)
      {
        frm_message.Show(Resource.String("STR_APP_GEN_ERROR_DATABASE_ERROR_READING"),
          Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, Images.CashierImage.Error, (View as Form));
      }
    }



    private List<InvalidField> SaveCustomerInformation(PlayerEditDetailsCashierModel Mdl, SqlTransaction Transaction)
    {
      AccountsReception _accounts_reception;

      try
      {
        if (!Mdl.CreationDate.HasValue)
        {
          Mdl.CreationDate = WGDB.Now;
        }
        _accounts_reception = CreateAccountsReceptionFromModel(Mdl);

        if (Customers.SaveCustomer(_accounts_reception, Transaction))
        {
          var _mdl = Model;
          _mdl.ScannedDocumentsChanged = false;
          LastModel = _mdl.Clone();
          Model = _mdl;

          if (JunketsBusinessLogic.ProcessCommissionEnrolled(_accounts_reception.AccountId, (SqlTransaction)Transaction))
          {
            return new List<InvalidField>();
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      return new List<InvalidField> { new InvalidField { Id = "NONE", Reason = WSI.Common.Resource.String("STR_VOUCHER_ERROR_MSG") } };
    }
    private bool CustomerHasAcceptedAgreement(AccountsReception AccountReceptionInfo)
    {
      return (AccountReceptionInfo.AceptedAgreementDate != DateTime.MinValue);
    }

    private PlayerEditDetailsCashierModel LoadCustomerInformation(PlayerEditDetailsCashierModel Mdl)
    {
      AccountsReception _account_reception = null;
      if (Mdl.IDDocuments == null)
        Mdl.IDDocuments = new Dictionary<int, string>();
      else
      {
        Mdl.IDDocuments.Clear();
      }

      if (Mdl.AccountId.HasValue)
      {
        _account_reception = Customers.GetCustomerData(Mdl.AccountId.Value);
      }
      bool _found_secondary_address = false;
      Mdl.ExpiredDocumentsResult = ExpiredDocumentsResult.DoesNotHaveExpiredDocuments;
      Mdl.ScannedDocuments = new BindingList<ScannedDocument>();
      Mdl.BlackListResult = BlackListResult.None;
      Mdl.RecordExpirationDate = null;
      Mdl.HolderTwitterAccount = string.Empty;
      Mdl.Facebook = string.Empty;
      Mdl.HasAceptedAgreement = false;
      Mdl.SearchScannedDocuments = true;

      if (_account_reception != null)
      {
        Mdl.CreationDate = _account_reception.CreationDate;
        Mdl.HolderTwitterAccount = _account_reception.Contact.Twitter ?? "";
        Mdl.Facebook = _account_reception.Contact.Facebook ?? "";
        Mdl.HasAceptedAgreement = CustomerHasAcceptedAgreement(_account_reception);
        Mdl.AceptedAgreementDate = _account_reception.AceptedAgreementDate;

        // get alternative address
        foreach (var _customer_address in _account_reception.Addresses)
        {
          if (_customer_address.AddressType != ENUM_ADDRESS.SECONDARY_ADRESS)
          {
            continue;
          }

          _found_secondary_address = true;
          Mdl.HolderAlternativeAddressLine1 = _customer_address.Street;
          Mdl.HolderAlternativeAddressLine2 = _customer_address.Colony;
          Mdl.HolderAlternativeAddressLine3 = _customer_address.Delegation;
          Mdl.HolderAlternativeTown = _customer_address.City;
          Mdl.HolderAlternativeState = new State { Id = _customer_address.FederalEntity };
          Mdl.HolderAlternativeCountry = new Country { Id = _customer_address.CountryAddress };
          Mdl.HolderAlternativeHouseNumber = _customer_address.Number;
          Mdl.HolderAlternativePostCode = _customer_address.Cp;
        }
        if (_account_reception.DocumentTypes != null)
          foreach (var v in Mdl.DocumentTypes)
          {
            if (_account_reception.DocumentTypes.ContainsKey(v.Id))
            {
              if(Mdl.IDDocuments.ContainsKey(v.Id))
                Mdl.IDDocuments[v.Id]= _account_reception.DocumentTypes[v.Id];
              else
              Mdl.IDDocuments.Add(v.Id, _account_reception.DocumentTypes[v.Id]);
          }
          }


        //Scanned Documents
        LoadScannedDocumentsAccount(Mdl);  //DLM

        /* DLM 
        //Mdl.RecordExpirationDate = _account_reception.RecordExpiration;          
        if (Mdl.ScannedDocuments.Count > 0)
        {
          Mdl.RecordExpirationDate = Mdl.ScannedDocuments[0].Expiration;
        }
        DLM */ 

        //scanned documents
        /*foreach (var _scanned_document in _account_reception.ScannedDocuments)
        {
          Mdl.ScannedDocuments.Add(new ScannedDocument
          {
            DocumentTypeId = _scanned_document.DocumentTypeId,
            DocumentTypeDescription = _scanned_document.DocumentTypeDescription,
            Expiration = _scanned_document.Expiration,
            Image = _scanned_document.Image,
            ScannedDate = _scanned_document.ScannedDate
          });
        }*/

      }
      if (Mdl.IDDocuments.ContainsKey(Mdl.HolderDocumentType.Id))
      {
        Mdl.IDDocuments[Mdl.HolderDocumentType.Id] = Mdl.HolderDocument;
      }
      else
      {
        Mdl.IDDocuments.Add(Mdl.HolderDocumentType.Id, Mdl.HolderDocument);
      }

      if (!_found_secondary_address)
      {
        Mdl.HolderAlternativeAddressLine1 = "";
        Mdl.HolderAlternativeAddressLine2 = "";
        Mdl.HolderAlternativeAddressLine3 = "";
        Mdl.HolderAlternativeTown = "";
        Mdl.HolderAlternativeState = new State { Id = 0 };
        Mdl.HolderAlternativeCountry = new Country { Id = 0 };
        Mdl.HolderAlternativeHouseNumber = "";
      }

      if (!Mdl.AccountId.HasValue)
    {
        return Mdl;
      }


      return Mdl;
    }


    internal void LoadScannedDocumentsAccount(PlayerEditDetailsCashierModel Mdl)
    {
      try
      {
        var _model = Mdl;

        if (!_model.AccountId.HasValue)
        {
          return;
        }

        if (!_model.SearchScannedDocuments)
        {
          return;
        }
        List<CustomerScannedDocument> _scanned_documents = Customers.GetScannedDocuments(_model.AccountId.Value);
        foreach (var _scanned_document in _scanned_documents)
        {
          _model.ScannedDocuments.Add(new ScannedDocument
          {
            DocumentTypeId = _scanned_document.DocumentTypeId,
            DocumentTypeDescription = _scanned_document.DocumentTypeDescription,
            Expiration = _scanned_document.Expiration,
            Image = _scanned_document.Image,
            ScannedDate = _scanned_document.ScannedDate
          });
        }

        _model.SearchScannedDocuments = false;
      }
      catch (Exception)
      {
        frm_message.Show(Resource.String("STR_APP_GEN_ERROR_DATABASE_ERROR_READING"),
          Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, Images.CashierImage.Error, (View as Form));
      }
    }

    /// <summary>
    ///   implementation of method to detect changes
    /// </summary>
    /// <param name="Old"></param>
    /// <param name="New"></param>
    /// <returns></returns>
    internal override bool HasChanges(PlayerEditDetailsCashierModel Old, PlayerEditDetailsCashierModel New)
    {
      var _changes = Old.DetailedCompare(New);

      _changes.Remove("BlackListResult");
      _changes.Remove("Validator");
      _changes.Remove("ScannedDocuments");
      _changes.Remove("NoMoreResults");
      _changes.Remove("RecordExpirationDate");
      _changes.Remove("HolderFullName");
      _changes.Remove("Fields");
      _changes.Remove("CardCreationDate");
      _changes.Remove("TrackData");


      return _changes.Count > 0;
    }

    /// <summary>
    ///   utility method to fill collections in model
    /// </summary>
    /// <param name="Mdl"></param>
    /// <returns></returns>
    protected PlayerEditDetailsCashierModel PopulateExtraLists(PlayerEditDetailsCashierModel Mdl)
    {
      Mdl = AddOccupations(Mdl);
      return Mdl;
    }

    /// <summary>
    ///   utility method to create a cashier model form a provided card data
    /// </summary>
    /// <param name="CrdData"></param>
    /// <returns></returns>
    public override PlayerEditDetailsCashierModel CreateModelFromCardData(CardData CrdData)
    {
      PlayerEditDetailsCashierModel _retval = CreateModelFromCardDataCommon(CrdData);
      CardData.PlayerTrackingData _player = CrdData.PlayerTracking;
        _retval.HolderDocument = _player.HolderId;
        _retval.HolderRFC = _player.HolderId1;
        _retval.HolderCURP = _player.HolderId2;

      _retval.RFCVisible = IdentificationTypes.IsIdentificationEnabled(ACCOUNT_HOLDER_ID_TYPE.RFC);
      _retval.CURPVisible = IdentificationTypes.IsIdentificationEnabled(ACCOUNT_HOLDER_ID_TYPE.CURP);
      _retval = PopulateExtraLists(_retval);
      _retval = PlayerEditDetailsCashierModelFill(CrdData, _retval, _player);
      _retval = GetNextLevelInfo(_retval, CrdData);
      _retval = GetCreditLineInfo(_retval);
      return _retval;
    }


    /// <summary>
    ///   update block status info
    /// </summary>
    public void BlockInfo(PlayerEditDetailsCashierModel Mdl)
    {
      var _vw = View as PlayerEditDetailsCashierView;
      if (_vw == null)
      {
        return;
      }

      _vw.lbl_blocked.Text = Resource.String("STR_UC_CARD_BLOCK_STATUS_UNLOCKED");
      _vw.txt_block_description.Text = "";
      _vw.txt_block_description.Visible = Mdl.CardBlocked;
      _vw.lbl_block_description.Visible = Mdl.CardBlocked;
      if (Mdl.CardBlocked)
      {
        _vw.pb_unblocked.Hide();
      }
      else
      {
        _vw.pb_blocked.Hide();
      }

      if (Mdl.CardBlocked)
      {
        string _coma = string.Empty;
        _vw.lbl_blocked.Text = Resource.String("STR_UC_CARD_BLOCK_STATUS_LOCKED") + @" ";

        foreach (KeyValuePair<AccountBlockReason, string> _block_reason_ in
          Accounts.GetBlockReason((int)Mdl.CardBlockReason))
        {
          _vw.lbl_blocked.Text += _coma + _block_reason_.Value;
          _coma = ", ";
        }
        _vw.txt_block_description.Text = Mdl.CardBlockDescription;
      }
      else if (!string.IsNullOrEmpty(Mdl.CardBlockDescription))
      {
        _vw.lbl_block_description.Visible = true;
        _vw.txt_block_description.Visible = true;
        _vw.lbl_block_description.Text = Resource.String("STR_FRM_BLOCK_ACCOUNT_LABEL_DESCRIPTION_UNBLOCK_SEC");
        _vw.txt_block_description.Text = Mdl.CardBlockDescription;
      }
    }//BlockInfo

    /// <summary>
    ///   consumable method to store scanned information in model
    /// </summary>
    /// <param name="DocsOwner"></param>
    public void ScanId(ACCOUNT_SCANNED_OWNER DocsOwner)
    {
      PlayerEditDetailsCashierModel _model = Model;
      try
      {
        string _scan_title;
        switch (DocsOwner)
        {
          case ACCOUNT_SCANNED_OWNER.HOLDER:
            _scan_title = string.Format("- {0}", Resource.String("STR_FRM_SCAN_HOLDER"));
            break;
          case ACCOUNT_SCANNED_OWNER.BENEFICIARY:
            _scan_title = string.Format("- {0}", Resource.String("STR_FRM_PLAYER_EDIT_BENEFICIARY"));
            break;
          default:
            _scan_title = "";
            break;
        }
        CardData _card_data = UpdateCardFromModel((CardData)CardData.Clone(), _model);

          frm_scan_document _frm_scan_document = new frm_scan_document(DocsOwner, _scan_title, _card_data,
          DocsOwner == ACCOUNT_SCANNED_OWNER.HOLDER ? _model.HolderDocsChanged : _model.BeneficiaryDocsChanged, false);
          
        {
          //lbl_msg_blink.Visible = false;

          if (_frm_scan_document.Show())
          {
            ((Form)View).Cursor = Cursors.WaitCursor;

            switch (DocsOwner)
            {
              case ACCOUNT_SCANNED_OWNER.HOLDER:
                _model.HolderDocScan = _frm_scan_document.DocumentsList;
                _model.HolderDocument3Type = _frm_scan_document.Id3Type;
                _model.HolderDocsChanged = _frm_scan_document.DocumentsChanged;
                break;

              case ACCOUNT_SCANNED_OWNER.BENEFICIARY:
                _model.BeneficiaryDocScan = _frm_scan_document.DocumentsList;
                _model.BeneficiaryDocument3Type = _frm_scan_document.Id3Type;
                _model.BeneficiaryDocsChanged = _frm_scan_document.DocumentsChanged;
                break;
            }
          }
        }
        _frm_scan_document.CleanupForm();
        _frm_scan_document.Dispose();

        //Show keyboard if Automatic OSK is enabled
        WSIKeyboard.Show();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {
        ((Form)View).Cursor = Cursors.Default;
      }
      Model = _model;
    } // ScanId

    /// <summary>
    ///   fill model with information form card data
    /// </summary>
    /// <param name="CrdData"></param>
    /// <param name="Retval"></param>
    /// <param name="Player"></param>
    /// <returns></returns>
    private PlayerEditDetailsCashierModel PlayerEditDetailsCashierModelFill(CardData CrdData,
                                                                            PlayerEditDetailsCashierModel Retval,
                                                                            CardData.PlayerTrackingData Player)
    {
      Retval.HolderOccupation = GetSelectableOption(Retval.Occupations, Player.HolderOccupationId,
        Player.HolderOccupation);
      Retval.HolderWeddingDate = Player.HolderWeddingDate;

      Retval.HolderBirthCountry = GetSelectableOption(Retval.Countries, (int)Player.HolderBirthCountry, "");

      Retval.HolderRFC = Player.HolderId1;
      Retval.HolderCURP = Player.HolderId2;
      Retval.HasBeneficiary = Player.HolderHasBeneficiary;
      if (Retval.HasBeneficiary)
      {
        Retval.BeneficiaryName1 = Player.BeneficiaryName3;
        Retval.BeneficiaryName = Player.BeneficiaryName;
        Retval.BeneficiaryLastName = Player.BeneficiaryName1;
        Retval.BeneficiaryLastName2 = Player.BeneficiaryName2;
        Retval.BeneficiaryDocument = Player.BeneficiaryId1;
        Retval.BeneficiaryCURP = Player.BeneficiaryId2;
        Retval.BeneficiaryId3 = Player.BeneficiaryId3;
        Retval.BeneficiaryOccupation = GetSelectableOption(Retval.Occupations, Player.BeneficiaryOccupationId,
          Player.BeneficiaryOccupation);
        Retval.BeneficiaryGender = GetSelectableOption(Retval.Genders, Player.BeneficiaryGender, "");
        Retval.BeneficiaryDocScan = Player.BeneficiaryScannedDocs;
        Retval.BeneficiaryBirthDate = Player.BeneficiaryBirthDate;
      }
      else
      {
        Retval.BeneficiaryName1 = "";
        Retval.BeneficiaryName = "";
        Retval.BeneficiaryLastName = "";
        Retval.BeneficiaryLastName2 = "";
        Retval.BeneficiaryDocument = "";
        Retval.BeneficiaryCURP = "";
        Retval.BeneficiaryId3 = "";
        Retval.BeneficiaryOccupation = GetSelectableOption(Retval.Occupations, 0, "");
        Retval.BeneficiaryGender = GetSelectableOption(Retval.Genders,0, "");
        Retval.BeneficiaryDocScan = new DocumentList();
        Retval.BeneficiaryBirthDate = default(DateTime);

      }
      Retval.HolderDocScan = Player.HolderScannedDocs;

      Retval.HolderMaritalStatus = GetSelectableOption(Retval.MaritalStatuses, Player.HolderMaritalStatus, "");

      Retval.HolderEmail2 = Player.HolderEmail02;

      Retval.CardBlocked = CrdData.Blocked;
      Retval.CardBlockDescription = CrdData.BlockDescription;
      Retval.CardBlockReason = CrdData.BlockReason;

      Retval.HolderEmail2 = Player.HolderEmail02;

      return Retval;
    }

    /// <summary>
    ///   add occupations list to model
    /// </summary>
    /// <param name="Retval"></param>
    /// <returns></returns>
    private PlayerEditDetailsCashierModel AddOccupations(PlayerEditDetailsCashierModel Retval)
    {

      DataTable _dt_occupations = CardData.GetOccupationsList();
      DataRow _dr_empty_row = _dt_occupations.NewRow();
      _dr_empty_row["OC_ID"] = 0;
      _dr_empty_row["OC_DESCRIPTION"] = "";
      _dt_occupations.Rows.InsertAt(_dr_empty_row, 0);
      Retval.Occupations = _dt_occupations.ToSelectableOptionList<OccupationWRK>();
      return Retval;
    }

    /// <summary>
    ///   utility method to update a card data from a given model
    /// </summary>
    /// <param name="CrdData"></param>
    /// <param name="Mdl"></param>
    /// <returns></returns>
    protected override CardData UpdateCardFromModel(CardData CrdData, PlayerEditDetailsCashierModel Mdl)
    {
      CardData _retval = UpdateCardFromModelCommon(CrdData, Mdl);
      CardData.PlayerTrackingData _player = _retval.PlayerTracking;

      _player.HolderOccupationId = Mdl.HolderOccupation.Id;
      _player.HolderWeddingDate = Mdl.HolderWeddingDate ?? DateTime.MinValue;
      _player.HolderBirthCountry = Mdl.HolderBirthCountry.Id;
      _player.HolderId1 = Mdl.HolderRFC;
      _player.HolderId2 = Mdl.HolderCURP;
      _player.HolderId1Type = ACCOUNT_HOLDER_ID_TYPE.RFC;
      _player.HolderId2Type = ACCOUNT_HOLDER_ID_TYPE.CURP;

      _player.HolderHasBeneficiary = Mdl.HasBeneficiary;

      _player.BeneficiaryName3 = Mdl.BeneficiaryName1;
      _player.BeneficiaryName = Mdl.BeneficiaryName;
      _player.BeneficiaryName1 = Mdl.BeneficiaryLastName;
      _player.BeneficiaryName2 = Mdl.BeneficiaryLastName2;
      _player.BeneficiaryId1 = Mdl.BeneficiaryDocument;
      _player.BeneficiaryId2 = Mdl.BeneficiaryCURP;
      _player.BeneficiaryId3 = Mdl.BeneficiaryId3;
      _player.BeneficiaryBirthDate = Mdl.BeneficiaryBirthDate ?? DateTime.MinValue;
      _player.BeneficiaryOccupationId = (Mdl.BeneficiaryOccupation ?? new OccupationWRK { Id = 0 }).Id;
      _player.BeneficiaryGender = (Mdl.BeneficiaryGender ?? new Gender { Id = 0 }).Id; 
      _player.BeneficiaryScannedDocs = Mdl.BeneficiaryDocScan;
      _player.HolderScannedDocs = Mdl.HolderDocScan;

      _retval.Blocked = Mdl.CardBlocked;
      _retval.BlockDescription = Mdl.CardBlockDescription;
      _retval.BlockReason = Mdl.CardBlockReason;

      _retval.CreditLineData = Mdl.CreditLineData;

      _player.HolderEmail02 = Mdl.HolderEmail2;
      //switch (CrdData.PlayerTracking.HolderIdType)
      //{
      //  case ACCOUNT_HOLDER_ID_TYPE.RFC:
      //    _player.HolderId = Mdl.HolderRFC;
      //    _player.HolderId2 = Mdl.HolderCURP;
      //    break;
      //  case ACCOUNT_HOLDER_ID_TYPE.CURP:
      //    _player.HolderId2 = Mdl.HolderRFC;
      //    _player.HolderId = Mdl.HolderCURP;
      //    break;
      //  default:
      //    _player.HolderId = Mdl.HolderDocument;
      //    _player.HolderId1 = Mdl.HolderRFC;
      //    _player.HolderId2 = Mdl.HolderCURP;
      //    break;
      //}

      if (_player.HolderWeddingDate != DateTime.MinValue)
      {
        _player.HolderWeddingDateText = _player.HolderWeddingDate.ToShortDateString();
      }
      if (_player.BeneficiaryBirthDate != DateTime.MinValue)
      {
        _player.BeneficiaryBirthDateText = _player.BeneficiaryBirthDate.ToShortDateString();
      }

      return _retval;
    }

    /// <summary>
    ///   save scanned id photo to holder documents
    /// </summary>
    /// <param name="Image"></param>
    /// <param name="DocumentTypeId"></param>
    /// <param name="Expiry"></param>
    /// <param name="Mdl"></param>
    /// <returns></returns>
    public override PlayerEditDetailsCashierModel SaveIdScanImage(Image Image, int DocumentTypeId, DateTime Expiry,
                                                                  PlayerEditDetailsCashierModel Mdl)
    {
      if (Mdl.HolderDocScan == null)
      {
        Mdl.HolderDocScan = new DocumentList();
      }
      IDocument _doc_item;
      using (MemoryStream _memory_stream = new MemoryStream())
      {
        Image _img = Image;
        //Image _img = Bitmap.FromFile("c:\\scan.jpg"); 

        _img.Save(_memory_stream, ImageFormat.Bmp);
        //_img = ImageCrop.AutoCrop((Bitmap)_img);
        //_img.Save(_memory_stream, ImageFormat.Bmp);
//        string _doc_type = string.IsNullOrEmpty(DocumentTypeId) ? "" : DocumentTypeId;

				int _scanned_type_id = DocumentTypeId;
        string _doc_name;
        if (!CardData.GenerateDocName(ACCOUNT_SCANNED_OWNER.HOLDER, _scanned_type_id.ToString(), out _doc_name))
        {
          return Mdl;
        }
        _doc_item = new ReadOnlyDocument(_doc_name, _memory_stream.ToArray());
        Mdl.HolderDocScan.Add(_doc_item);
      }
      return Mdl;
    }


    /// <summary>
    ///   utility method to update model from a given card data
    /// </summary>
    /// <param name="CrdData"></param>
    /// <param name="Mdl"></param>
    /// <returns></returns>
    protected override PlayerEditDetailsCashierModel UpdateModelFromCardData(CardData CrdData,
                                                                             PlayerEditDetailsCashierModel Mdl)
    {
      CardData.PlayerTrackingData _player = CrdData.PlayerTracking;
      Mdl = UpdateModelFromCardDataCommon(CrdData, Mdl);
      Mdl = PlayerEditDetailsCashierModelFill(CrdData, Mdl, _player);

      Mdl = GetNextLevelInfo(Mdl, CrdData);

      return Mdl;
    }

    public override List<InvalidField> CheckDuplicateAccounts()
    {
      List<InvalidField> _errors = new List<InvalidField>();
      var _model = Model;

      if (_model == null || String.IsNullOrEmpty(_model.HolderName1) || String.IsNullOrEmpty(_model.HolderName3) ||
          Equals(_model.HolderBirthDate, DateTime.MinValue) || _model.HolderGender == null) return _errors;
      CardData _card_data = UpdateCardFromModel((CardData)CardData.Clone(), _model);

      if (_card_data == null) return _errors;
      String _duplicate_msg;
      Boolean _allow_duplicate;

      if (!CardData.CheckDuplicateAccounts(_card_data, out _duplicate_msg, out _allow_duplicate))
      {
        _errors.Add(new InvalidField
        {
          Id = "AccountDuplicated",
          Reason = _duplicate_msg.Replace(":", "")
        });
        if (_model.AccountId < 0 && !_showndupdetails)
        {
          frm_message.Show(_duplicate_msg.Replace(":", ""), Resource.String("STR_APP_GEN_MSG_WARNING"), MessageBoxButtons.OK, ViewForm);
          _showndupdetails = true;
        }
        if (!_model.DuplicatedDetails)
        {
          Model.DuplicatedDetails = true;
        }
      }
      else
      {
        if (_model.DuplicatedDetails)
        {
          Model.DuplicatedDetails = false;
        }
      }

      return _errors;
    }
    /// <summary>
    ///   method to add information about the next player level to the model
    /// </summary>
    /// <param name="Mdl"></param>
    /// <param name="CrdData"></param>
    /// <returns></returns>
    private PlayerEditDetailsCashierModel GetNextLevelInfo(PlayerEditDetailsCashierModel Mdl, CardData CrdData)
    {
      AccountNextLevel _next_level_info;
      Points _generated;
      Points _discretionaries;
      Points _level_points;
      Points _required;
      Points _maintain;
      Mdl.CurrentLevel = LoyaltyProgram.LevelName(CrdData.PlayerTracking.CardLevel);
      Mdl.CurrentPoints = CrdData.PlayerTracking.TruncatedPoints.ToString();
      Mdl.CurrentLevelEntered = CrdData.PlayerTracking.HolderLevelEntered == DateTime.MinValue
        ? "---"
        : Format.CustomFormatDateTime(CrdData.PlayerTracking.HolderLevelEntered, true);
      Mdl.CurrentLevelExpiration = CrdData.PlayerTracking.HolderLevelExpiration == DateTime.MinValue
        ? "---"
        : Format.CustomFormatDateTime(CrdData.PlayerTracking.HolderLevelExpiration, true);


      if (!Mdl.AccountId.HasValue)
      {
        return Mdl;
      }
      _next_level_info = new AccountNextLevel(Mdl.AccountId.Value);

      //29-MAY-2013: For the moment, is not possible no calculate the amount of points left to reach next level on multi-site mode.
      if (_next_level_info.LevelId >= 0 && !GeneralParam.GetBoolean("Site", "MultiSiteMember", false))
      {
        _generated = Math.Truncate(_next_level_info.PointsGenerated);
        _discretionaries = Math.Truncate(_next_level_info.PointsDiscretionaries);
        _level_points = _generated + _discretionaries;
        _maintain = GeneralParam.GetInt32("PlayerTracking",
          "Level0" + CrdData.PlayerTracking.CardLevel + ".PointsToKeep");
        _maintain = Math.Max(_maintain - (_level_points), 0);
        Mdl.PointsToMaintain = CrdData.PlayerTracking.CardLevel <= 1 ? "---" : _maintain.ToString();
        //Model.lbl_points_to_maintain.TextAlign = (m_card_data.PlayerTracking.CardLevel <= 1) ? ContentAlignment.MiddleLeft : ContentAlignment.MiddleRight;
        _required = GeneralParam.GetInt32("PlayerTracking", "Level0" + _next_level_info.LevelId + ".PointsToEnter");
        Mdl.PointsDiscretionaries = (_discretionaries <= 0) ? "---" : _discretionaries.ToString();
        Mdl.PointsGenerated = (_generated <= 0) ? "---" : _generated.ToString();
        Mdl.PointsLevel = (_level_points <= 0) ? "---" : _level_points.ToString();

        if (_next_level_info.LevelId == CrdData.PlayerTracking.CardLevel)
        {
          return Mdl;
        }
        Mdl.PointsNextLevelVisible = true;
        Mdl.PointsNextLevelName = _next_level_info.LevelName;
        Mdl.PointsToNextLevel = _next_level_info.PointsToReach.ToString();
        Mdl.PointsRequired = _required.ToString();
      }
      else
      {
        Mdl.PointsNextLevelVisible = false;
      }
      return Mdl;
    }

    /// <summary>
    /// Get Credit Line Info by Account Id.
    /// </summary>
    /// <param name="_retval"></param>
    /// <returns></returns>
    private PlayerEditDetailsCashierModel GetCreditLineInfo(PlayerEditDetailsCashierModel _retval)
    {
      CreditLine _credit_line;
      Int64 _account_id;


      _credit_line = new CreditLine();
      _account_id = _retval.AccountId == null ? 0 : (Int64)_retval.AccountId;

      using (DB_TRX _db_trx = new DB_TRX())
      {
        _credit_line.DB_GetByAccountId(_account_id, _db_trx.SqlTransaction);
        _retval.CreditLineData = _credit_line;
      }

      return _retval;
    } // GetCreditLineInfo

    /// <summary>
    /// method to make a payback for a customer
    /// </summary>
    /// <returns></returns>
    public List<InvalidField> CustomerPayback()
    {
      frm_creditline_payback _frm;
      Int64 _account_id;
      CreditLine _credit_line;
      PlayerEditDetailsCashierModel _model;
      _account_id = Model.AccountId != null ? (Int64)Model.AccountId : 0;
      _model = Model;

        using (frm_yesno _frm_yes_no = new frm_yesno())
        {
          _frm_yes_no.Opacity = 0.6;
          _frm_yes_no.Show(ViewForm);
          _frm = new frm_creditline_payback(ViewForm);
        _frm.PaybackShow(_account_id, out _credit_line);
        }
        _model.CreditLineData = _credit_line;
        
       return new List<InvalidField>();
    } // CustomerPayback


    public override void Dispose()
    {
      if (IDScanner != null)
        IDScanner.DetatchFromAllAvailable(HandleIDScannerEvent);


      (ViewForm as PlayerEditDetailsCashierView).CleanUpForm();
      View = null;
      if (Model != null)
      {
        Model.DocumentTypes = null;
        Model.Nationalities = null;
        Model.Countries = null;
        Model.Genders = null;
      }
      Model = null;
      base.Dispose();
    }

    
    /// <summary>
    ///   method to print information
    /// </summary>
    /// <returns></returns>
    public override List<InvalidField> PrintInformation()
    {
      return PrintInformationCommon();
    }
  }
}
