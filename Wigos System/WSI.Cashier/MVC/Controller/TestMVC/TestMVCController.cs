﻿using System;
using System.Collections.Generic;
using System.Text;
using WSI.Cashier.MVC.Model.TestMVC;

namespace WSI.Cashier.MVC.Controller.TestMVC
{
  public class TestMVCController:BaseController<TestMVCModel>
  {
    internal void Run()
    {
      ViewForm.ShowDialog();
    }

    public bool SaveData()
    {
      var _mdl = Model;
      return Model.Validator.Validate(new List<string>(), new List<string>() {"Name"}).Count == 0;
    }
  }
}
