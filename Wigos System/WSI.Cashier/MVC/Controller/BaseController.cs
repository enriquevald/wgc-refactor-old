using System.Windows.Forms;
using WSI.Cashier.MVC.Controller.PlayerEditDetails.Common;
using WSI.Cashier.MVC.Interface;

namespace WSI.Cashier.MVC.Controller
{
  public abstract class BaseController<T> : IMVCController<T> where T : IMVCModel
  {
    private T m_model;
    private IMVCView<T> m_view;
    private bool m_applied_event_last_action;

    public virtual void Dispose()
    {
      if (!m_applied_event_last_action)
      {
        return;
      }
      if (View is Form)
      {
        EventLastAction.RemoveEventLastAction(((Form) View).Controls);
      }
      else if (View is UserControl)
      {
        EventLastAction.RemoveEventLastAction(((UserControl) View).Controls);
      }
    }

    public T Model
    {
      get { return View != null ? (T)(View as Control).GetControlPropertyThreadSafe("Model") : m_model; }
      set
      {
        if (View != null)
        {
          (View as Control).SetControlPropertyThreadSafe("Model",value);
        }
        else
        {
          m_model = value;
        }
      }
    }
    public IMVCView<T> View
    {
      get { return m_view; }
      set
      {
        if (value == null) {
          m_view = null;
          return;
        }
        if (value.Model == null && m_model != null)
          value.Model = m_model;
        if (value.Controller == null)
          value.Controller = this;
        m_view = value;

        if (!(m_view is Form || m_view is UserControl) || m_applied_event_last_action)
        {
          return;
        }
        if (m_view is Form)
          EventLastAction.AddEventLastAction(((Form) m_view).Controls);
        else 
          EventLastAction.AddEventLastAction(((UserControl) m_view).Controls);
        m_applied_event_last_action = true;
      }
    }

    public Form ViewForm
    {
      get { return m_view as Form; }
    }
  
  }
}