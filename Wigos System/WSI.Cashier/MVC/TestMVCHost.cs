﻿using System;
using System.Collections.Generic;
using System.Text;
using WSI.Cashier.MVC.Controller.TestMVC;
using WSI.Cashier.MVC.Model.TestMVC;
using WSI.Cashier.MVC.View.TestMVC;

namespace WSI.Cashier.MVC
{
  public class TestMVCHost
  {
    public void TestMVCHostRun()
    {
      using (TestMVCModel mdl = new TestMVCModel())
      {
        mdl.Name = "yeehhaaawww";
        using (TestMVCView view = new TestMVCView())
        {
          using (TestMVCController controller = new TestMVCController())
          {
            controller.Model = mdl;
            controller.View = view;
            controller.Run();
          }
        }
      }
    }      
  }
}
