//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_show_cash_desk.cs
// 
//   DESCRIPTION: 
//
//        AUTHOR: DMR
// 
// CREATION DATE: 08-AUG-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 08-AUG-2013 DMR    First version.

using WSI.Cashier.Controls;
namespace WSI.Cashier
{
  partial class frm_preview_cash_desk
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.btn_ok = new WSI.Cashier.Controls.uc_round_button();
      this.gp_voucher_box = new WSI.Cashier.Controls.uc_round_panel();
      this.web_browser = new System.Windows.Forms.WebBrowser();
      this.pnl_data.SuspendLayout();
      this.gp_voucher_box.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.Controls.Add(this.btn_ok);
      this.pnl_data.Controls.Add(this.gp_voucher_box);
      this.pnl_data.Size = new System.Drawing.Size(408, 334);
      // 
      // btn_ok
      // 
      this.btn_ok.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
      this.btn_ok.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.btn_ok.FlatAppearance.BorderSize = 0;
      this.btn_ok.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_ok.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_ok.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.btn_ok.Image = null;
      this.btn_ok.Location = new System.Drawing.Point(127, 261);
      this.btn_ok.Name = "btn_ok";
      this.btn_ok.Size = new System.Drawing.Size(155, 60);
      this.btn_ok.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_ok.TabIndex = 20;
      this.btn_ok.Text = "OK";
      this.btn_ok.UseVisualStyleBackColor = false;
      this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
      // 
      // gp_voucher_box
      // 
      this.gp_voucher_box.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.gp_voucher_box.BackColor = System.Drawing.Color.Transparent;
      this.gp_voucher_box.Controls.Add(this.web_browser);
      this.gp_voucher_box.CornerRadius = 1;
      this.gp_voucher_box.HeaderBackColor = System.Drawing.Color.Empty;
      this.gp_voucher_box.HeaderForeColor = System.Drawing.Color.Empty;
      this.gp_voucher_box.HeaderHeight = 0;
      this.gp_voucher_box.HeaderSubText = null;
      this.gp_voucher_box.HeaderText = null;
      this.gp_voucher_box.Location = new System.Drawing.Point(12, 15);
      this.gp_voucher_box.Name = "gp_voucher_box";
      this.gp_voucher_box.PanelBackColor = System.Drawing.Color.Empty;
      this.gp_voucher_box.Size = new System.Drawing.Size(386, 231);
      this.gp_voucher_box.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NONE;
      this.gp_voucher_box.TabIndex = 21;
      // 
      // web_browser
      // 
      this.web_browser.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.web_browser.Location = new System.Drawing.Point(0, 0);
      this.web_browser.MinimumSize = new System.Drawing.Size(20, 20);
      this.web_browser.Name = "web_browser";
      this.web_browser.Size = new System.Drawing.Size(386, 231);
      this.web_browser.TabIndex = 16;
      this.web_browser.TabStop = false;
      // 
      // frm_preview_cash_desk
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(408, 389);
      this.HeaderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "frm_preview_cash_desk";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Style = WSI.Cashier.Controls.frm_base.BaseFormStyle.PRINCIPAL;
      this.Text = "frm_preview_cash_desk";
      this.pnl_data.ResumeLayout(false);
      this.gp_voucher_box.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private uc_round_button btn_ok;
    private uc_round_panel gp_voucher_box;
    public System.Windows.Forms.WebBrowser web_browser;


  }
}