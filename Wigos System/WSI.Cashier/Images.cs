using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using WSI.Common;

namespace WSI.Cashier
{
  public static class Images
  {
    public enum CashierImage
    {
        Empty = -1
      , SwipeCard = 0
      , Card = 1
      , Information = 2
      , Keyboard = 3
      , Money = 4
      , Online = 5
      , Shutdown = 6
      , SingleUser = 7
      , Users = 8
      , Warning = 9
      , Error = 10
      , Question = 11
      , Cashier = 12
      , Terminal = 13
      , UserMale = 14
      , UserFemale = 15
      , DbOK = 16
      , DbError = 17
      , CashierOpen = 18
      , CashierClose = 19
      , CashierReset = 20
      , ClockCalendar = 21
      , DbConnecting = 22
      , UserAnonymous = 23
      , Locked = 24
      , Unlocked = 25
      , SlotMachine = 26
      , LogOff = 27
      , Printer = 28
      , ConnError = 29
      , Options = 30
      , Logo = 31
      , Language = 32
      , NonRedeemable = 33
      , Redeemable = 34
      , RoundInfo = 35
      , DbConfig = 36 
      , DbAnim00 = 37 
      , DbAnim01 = 38 
      , DbAnim02 = 39 
      , DbAnim03 = 40 
      , DbAnim04 = 41 
      , DbAnim05 = 42 
      , DbAnim06 = 43 
      , DbAnim07 = 44 
      , TouchCalibrate = 45
      , MBCard = 46
      , ChangePin = 47
    , Points = 48
    , Gift = 49
    , MoneyLocked = 50
    , MoneyUnlocked = 51
    , Recycle = 52
    , CreditCard = 53
    , OSKLogo = 54
    , Back = 55
    , CancelTicket = 56
    , ValidateTicket = 57
    , Forward = 58
    , GT_Close = 59
    , GT_Ok = 60
    , GT_Pause = 61
    , GT_Play = 62
    , GT_Swap = 63
    , Input_hide = 64
    , Input_show = 65
    , GT_Player_Tracking = 66
    , GT_Free_Seat = 67
    , GT_Taken_Seat = 68
    , GT_Away_Player = 69
    , GT_Disabled_Seat = 70
    , GT_Legend = 71
    , Input_left = 72
    , Input_right = 73
    , GT_menu = 74
    , Calc_Close = 75
    , GT_Free_Seat_clean = 76
    , GT_Taken_Seat_clean = 77
    , GT_Away_Player_clean = 78
    , GT_Disabled_Seat_clean = 79
    , GT_Closed_Session = 80
    , GT_Table_Paused = 81
    , GT_Table_Warning = 82
    , GT_Closed_Session_Large = 83
    , GT_Table_Paused_Large = 84
    , Gaming_Table_Open = 85
    , Gaming_Table_Close = 86
    , Gaming_Table_in = 87
    , Gaming_Table_Out = 88
    , Gaming_Table_last_movements = 89
    , Gaming_Table_Table = 90
    , Handpay = 91
    , Collect = 92
    , Fill = 93
    , Read = 94
    , PromoHeader = 95
    , Calc_Header_Close = 96
    , Circular_Progress_Bar = 97
    , GH_Error = 98
  };

    public static Bitmap Get32x32(CashierImage ImageId)
    {
      Bitmap b;

      b = (Bitmap)Get(ImageId);

      return Get(b, 32);
    }

    public static Bitmap Get32x32(Bitmap ImageId)
    {
      return Get(ImageId, 32);
    }
    
    public static Bitmap Get(Bitmap Image, int Size)
    {

      Decimal ratioX = Math.Max(Image.Height, Image.Width);

      return (Bitmap)Image.GetThumbnailImage((int)(Size * Image.Width / ratioX), (int)(Size * Image.Height / ratioX), null, new IntPtr(0));
    }

    public static Bitmap Get (CashierImage ImageId)
    {
      try
      {
        switch (ImageId)
        {
          case CashierImage.Question:
            return System.Drawing.SystemIcons.Question.ToBitmap();

          case CashierImage.Error:
            return System.Drawing.SystemIcons.Error.ToBitmap();

          case CashierImage.Information:
            return System.Drawing.SystemIcons.Information.ToBitmap();

          case CashierImage.Warning:
            return System.Drawing.SystemIcons.Warning.ToBitmap();

          case CashierImage.Card:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("Card_Only");

          case CashierImage.Keyboard:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("keyboard");

          case CashierImage.Money:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("money");

          case CashierImage.Online:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("online");

          case CashierImage.Shutdown:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("shutdown");

          case CashierImage.SwipeCard:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("Card");

          case CashierImage.Users:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("users");

          case CashierImage.Cashier:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("cashier");

          case CashierImage.Terminal:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("Terminal");

          case CashierImage.SingleUser:
          case CashierImage.UserMale:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("user_male");

          case CashierImage.UserFemale:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("user_female");

          case CashierImage.UserAnonymous:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("user_anonymous");

          case CashierImage.DbError:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("db_close");

          case CashierImage.DbOK:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("db_open");

          case CashierImage.CashierClose:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("cashier_close");

          case CashierImage.CashierOpen:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("cashier_open");

          case CashierImage.CashierReset:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("cashier_reset");

          case CashierImage.ClockCalendar:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("clock_calendar");

          case CashierImage.DbConnecting:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("db_connecting");

          case CashierImage.Locked:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("LOCK_CLOSE");

          case CashierImage.Unlocked:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("LOCK_OPEN");

          case CashierImage.SlotMachine:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("slotmachine");

          case CashierImage.LogOff:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("logoff");

          case CashierImage.Printer:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("printer");

          case CashierImage.ConnError:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("connection_error");

          case CashierImage.Options:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("options");

          case CashierImage.Logo:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("IMG_LOGO_VOUCHER");

          case CashierImage.Language:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("language");

          case CashierImage.NonRedeemable:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("cash_non_redeemable");

          case CashierImage.Redeemable:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("cash_redeemable");

          case CashierImage.RoundInfo:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("info");

          case CashierImage.DbConfig:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("db_config");

          case CashierImage.DbAnim00:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("db_anim_00");

          case CashierImage.DbAnim01:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("db_anim_01");

          case CashierImage.DbAnim02:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("db_anim_02");

          case CashierImage.DbAnim03:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("db_anim_03");

          case CashierImage.DbAnim04:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("db_anim_04");

          case CashierImage.DbAnim05:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("db_anim_05");

          case CashierImage.DbAnim06:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("db_anim_06");

          case CashierImage.DbAnim07:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("db_anim_07");

          case CashierImage.TouchCalibrate:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("TouchCalibrate");

          case CashierImage.MBCard:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("MBCard_Only");

          case CashierImage.Points:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("points");

          case CashierImage.ChangePin:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("Change_Pin");

          case CashierImage.Gift:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("Gift");

          case CashierImage.MoneyLocked:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("MONEY_LOCK_CLOSE");

          case CashierImage.MoneyUnlocked:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("MONEY_LOCK_OPEN");
          
          case CashierImage.Recycle:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("Recycle");
          
          case CashierImage.CreditCard:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("credit_card");

          case CashierImage.OSKLogo:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("OSKLogo");

          case CashierImage.Back:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("Back");

          case CashierImage.CancelTicket:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("CancelTicket");

          case CashierImage.ValidateTicket:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("ValidateTicket");

          case CashierImage.Forward:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("Forward");

          case CashierImage.GT_Close:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("GT_Close");

          case CashierImage.GT_Ok:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("GT_Ok");

          case CashierImage.GT_Pause:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("GT_Pause");

          case CashierImage.GT_Play:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("GT_Play");

          case CashierImage.GT_Swap:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("GT_Swap");

          case CashierImage.Input_hide:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("Input_hide");

          case CashierImage.Input_show:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("Input_show");

          case CashierImage.GT_Player_Tracking:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("GT_Player_tracking");

          case CashierImage.GT_Free_Seat:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("GT_Free_Seat");

          case CashierImage.GT_Taken_Seat:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("GT_Taken_Seat");

          case CashierImage.GT_Away_Player:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("GT_Away_Player");

          case CashierImage.GT_Disabled_Seat:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("GT_Disabled_Seat");

          case CashierImage.GT_Legend:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("GT_info");

          case CashierImage.Input_left:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("Input_left");

          case CashierImage.Input_right:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("Input_right");

          case CashierImage.GT_menu:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("GT_menu");

          case CashierImage.Calc_Close:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("Calc_Close");

          case CashierImage.GT_Free_Seat_clean:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("GT_Free_Seat_clean");

          case CashierImage.GT_Taken_Seat_clean:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("GT_Taken_Seat_clean");

          case CashierImage.GT_Away_Player_clean:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("GT_Away_Player_clean");

          case CashierImage.GT_Disabled_Seat_clean:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("GT_Disabled_Seat_clean");

          case CashierImage.GT_Closed_Session:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("GT_Closed_Session");

          case CashierImage.GT_Table_Paused:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("GT_Pause_Table");

          case CashierImage.GT_Table_Warning:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("GT_Table_Warning");

          case CashierImage.GT_Closed_Session_Large:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("GT_Closed_Session_Large");

          case CashierImage.GT_Table_Paused_Large:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("GT_Pause_Table_Large");

          case CashierImage.Gaming_Table_Open:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("Gaming_Table_Open");

          case CashierImage.Gaming_Table_Close:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("Gaming_Table_Close");

          case CashierImage.Gaming_Table_in:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("Gaming_Table_in");

          case CashierImage.Gaming_Table_Out:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("Gaming_Table_Out");

          case CashierImage.Gaming_Table_last_movements:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("Gaming_Table_last_movements");

          case CashierImage.Gaming_Table_Table:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("Gaming_Table_Table");

          case CashierImage.Handpay:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("handpay");

          case CashierImage.Fill:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("fill");
         
          case CashierImage.Collect:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("collect");
         
          case CashierImage.Read:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("read");

          case CashierImage.Circular_Progress_Bar:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("circular_progress_bar");

          case CashierImage.PromoHeader:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("promotions_header");

          case CashierImage.Calc_Header_Close:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("btnClose");

          case CashierImage.GH_Error:
            return (Bitmap)WSI.Cashier.Resource.rm.GetObject("gh_error_icon");

          default:
            return System.Drawing.SystemIcons.Application.ToBitmap();
        }
      }
      catch
      {
        return null;
      }
    }
  }
}
