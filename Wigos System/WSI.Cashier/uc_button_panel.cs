//------------------------------------------------------------------------------
// Copyright � 2007-2008 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_button_panel.cs
// 
//   DESCRIPTION: Implements the class uc_button_panel (Cashier submenus)
// 
//        AUTHOR: Joan Marc Pepi�
// 
// CREATION DATE: 28-NOV-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 28-NOV-2013 JPJ    Inital Draft.
// 09-DEC-2013 AMF    Hide btn_close
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using WSI.Cashier;

namespace WSI.Cashier
{

  public partial class uc_button_panel : UserControl, IDisposable
  {

    public class UcButtonPanelMenuElement
    {

      #region Members

      private String m_name;                            // Internal name of the element.
      private String m_text;                            // Text to show up in the menu element.
      private Boolean m_enabled;                        // Element is enabled or disabled.
      private Images.CashierImage m_image;              // Image to show in the menu element.
      private Object m_parent;                          // Who's the caller.
      private Delegate m_ButtonAction;                  // Action to be execute when button is clicked.

      #endregion

      #region Properties

      public String Name
      {
        get
        {
          return m_name;
        }
        set
        {
          m_name = value;
        }
      }

      public String Text
      {
        get
        {
          return m_text;
        }
        set
        {
          m_text = value;
        }
      }

      public Boolean Enabled
      {
        get
        {
          return m_enabled;
        }
        set
        {
          m_enabled = value;
        }
      }

      public Images.CashierImage Image
      {
        get
        {
          return m_image;
        }
        set
        {
          m_image = value;
        }
      }

      public Object Parent
      {
        get
        {
          return m_parent;
        }
        set
        {
          m_parent = value;
        }
      }

      public Delegate ButtonAction
      {
        get
        {
          return m_ButtonAction;
        }
        set
        {
          m_ButtonAction = value;
        }
      }

      #endregion

    }

    public class UcButtonpanelEventArgs : EventArgs
    {
      private Button m_ButtonClicked = null;
      private Boolean m_AutoCollapsed = false;
      private Delegate m_ButtonAction = null;

      public Boolean AutoCollapsed
      {
        get
        {
          return m_AutoCollapsed;
        }
      }

      public Button ButtonClicked
      {
        get
        {
          return m_ButtonClicked;
        }
      }

      public Delegate ButtonAction
      {
        get
        {
          return m_ButtonAction;
        }
      }

      public UcButtonpanelEventArgs(Button ButtonClicked, Boolean AutoCollapsed, Delegate ButtonAction)
      {
        this.m_AutoCollapsed = AutoCollapsed;
        this.m_ButtonClicked = ButtonClicked;
        this.m_ButtonAction = ButtonAction;
      }
    }

    #region Members

    private Dictionary<String, UcButtonPanelMenuElement> m_button_list;
    private System.Windows.Forms.Timer m_timer = new System.Windows.Forms.Timer();
    private Int32 m_time_to_collapse;

    #endregion

    #region Delegates & Events

    public delegate void TimerPanel(object sender, EventArgs e);
    public delegate void EventButton(object sender, UcButtonpanelEventArgs e);
    public event EventButton ButtonClick;

    //------------------------------------------------------------------------------
    // PURPOSE : This is the method to run when the timer is raised.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    protected void TimerEvent(Object myObject, EventArgs myEventArgs)
    {
      DisposeTimer();
      //bubble the event up to the parent
      if (this.ButtonClick != null)
        this.ButtonClick(btn_close, new UcButtonpanelEventArgs(btn_close, true, null));
    }

    //------------------------------------------------------------------------------
    // PURPOSE : This is the method to run when the event click of the button is raised.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    protected void Button_Click(object sender, EventArgs e)
    {
      Button _button;

      _button = (Button)sender;

      DisposeTimer();
      //bubble the event up to the parent
      if (this.ButtonClick != null)
        this.ButtonClick(sender, new UcButtonpanelEventArgs(_button, false, m_button_list[_button.Name].ButtonAction));
    }
    #endregion

    #region Properties

    public Dictionary<String, UcButtonPanelMenuElement> ButtonList
    {
      get { return m_button_list; }
      set { m_button_list = value; }
    }

    public String CloseText
    {
      get { return this.btn_close.Text; }
      set { this.btn_close.Text = value; }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Enables the timer and adds the timer handler.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private void PrepareAutoCollapse()
    {
      // Adds the event and the event handler for the method that will 
      // process the timer event to the timer. 
      m_timer.Tick += new EventHandler(TimerEvent);
      // Sets the timer interval to TimeToCollapse seconds.
      m_timer.Interval = this.m_time_to_collapse;
      m_timer.Start();

    }

    //------------------------------------------------------------------------------
    // PURPOSE : Interval in seconds to fire close event. If set to zero no event is fired.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public Int32 TimeToCollapse
    {
      get
      {
        return m_time_to_collapse;
      }
      set
      {
        if (value < 0)
        {
          throw new ArgumentOutOfRangeException("TimeToCollapse", "Timer must be set with a value greater than 0.");
        }

        m_time_to_collapse = value;
      }
    }

    #endregion

    #region Private

    //------------------------------------------------------------------------------
    // PURPOSE : Creates a new buttonInterval in seconds to fire close event. If set to zero no event is fired.
    //
    //  PARAMS :
    //      - INPUT :
    //        - Name: Button name.
    //        - Text: Text to show.
    //        - Enabled: Can click on it.
    //        - Image: Image resource to show with the button.
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private Button NewButton(UcButtonPanelMenuElement MenuElement)
    {
      Button _newbutton;

      _newbutton = new Button();

      _newbutton.BackColor = System.Drawing.Color.LightSkyBlue;
      _newbutton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
      _newbutton.Dock = System.Windows.Forms.DockStyle.Top;
      _newbutton.FlatAppearance.BorderSize = 0;
      _newbutton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      _newbutton.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
      _newbutton.ImageIndex = 0;
      _newbutton.Location = new System.Drawing.Point(0, 178);
      _newbutton.Margin = new System.Windows.Forms.Padding(0);
      _newbutton.Name = MenuElement.Name;
      _newbutton.Padding = new System.Windows.Forms.Padding(0, 0, 4, 0);
      _newbutton.Size = new System.Drawing.Size(112, 48);
      _newbutton.TabIndex = 1;
      _newbutton.Text = MenuElement.Text;
      _newbutton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      _newbutton.Enabled = MenuElement.Enabled;
      if (MenuElement.Image != Images.CashierImage.Empty)
      {
        _newbutton.Image = WSI.Cashier.Images.Get32x32(MenuElement.Image);
      }
      _newbutton.UseVisualStyleBackColor = false;

      return _newbutton;
    }

    protected override void OnLoad(EventArgs e)
    {

      base.OnLoad(e);

      if (this.Visible && m_time_to_collapse > 0)
      {
        PrepareAutoCollapse();
      }
    }

    /// <summary>
    /// Stops timer and disposes it.
    /// </summary>
    public void DisposeTimer()
    {
      m_timer.Stop();
      m_timer.Dispose();
    }

    #endregion

    #region Public

    //------------------------------------------------------------------------------
    // PURPOSE : Constructor.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public uc_button_panel()
    {

      InitializeComponent();

    }

    //------------------------------------------------------------------------------
    // PURPOSE : Creates a list of buttons in the control.
    //
    //  PARAMS :
    //      - INPUT :
    //        -ButtonList: List of buttons names.
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public void SetButtons(Dictionary<String, UcButtonPanelMenuElement> ButtonList)
    {
      Button _newbutton;
      Int32 _idx_button;
      Int32 _heigth;

      _heigth = 0;
      m_button_list = ButtonList;

      _idx_button = 0;

      // Close button
      //if (true)
      //{
      //  _heigth += btn_close.Height;
      //  this.btn_close.Text = Resource.String("STR_MENU_BACK"); ;
      //  this.btn_close.Image = WSI.Cashier.Images.Get32x32(Images.CashierImage.Back);
      //}

      foreach (KeyValuePair<String, UcButtonPanelMenuElement> _menu_element in ButtonList)
      {
        _newbutton = NewButton(_menu_element.Value);
        _newbutton.Click += new EventHandler(Button_Click);
        this.Controls[0].Controls.Add(_newbutton);
        _idx_button++;
        _heigth += _newbutton.Height;
      }

      this.btn_close.Height = 0;
      this.Height = _heigth;
    }

    #region IDisposable Members

    //------------------------------------------------------------------------------
    // PURPOSE : Clean Resources.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    void IDisposable.Dispose()
    {
      DisposeTimer();
      this.Dispose();
    }

    #endregion
  }
    #endregion

}
