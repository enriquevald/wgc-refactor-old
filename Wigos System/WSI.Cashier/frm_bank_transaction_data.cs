//------------------------------------------------------------------------------
// Copyright � 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_bank_transaction_data.cs
// 
//   DESCRIPTION: Implements form to input data from cards and checks
//
//        AUTHOR: JBC
// 
// CREATION DATE: 01-SEP-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 01-SEP-2014 JBC     First release.
// 29-OCT-2014 JBC     Fixed Bug WIG-1594: Location
// 18-SEP-2015 YNM     Fixed Bug TFS-4514: recharge with check as payment metod
// 25-SEP-2015 DHA     Fixed Bug TFS-4687: recharge with check wrong total
// 29-MAR-2016 ESE     Fixed Bug 10872: Check/unchek manual entry control
// 08-MAY-2016 ETP     Fixed Bug 14294: Window need focus for credit card reader.
// 20-SEP-2016 ETP     Fixed Bug 17874: Card prize not payed in creditcard payment.
// 06-OCT-2016 LTC     Bug 17617:Cashier: Cash Advance: Transact information with bank card - wrong control alignment
// 26-OCT-2017 RAB     Bug 30433:WIGOS-6048 Threshold: Check type is not required even the output threshold is exceeded
// ----------- ------ ----------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WSI.Common;
using System.Data.SqlClient;
using WSI.Cashier.Controls;

namespace WSI.Cashier
{
  public partial class frm_bank_transaction_data : frm_base
  {

    #region Attributes

    private CurrencyExchangeType m_type;
    private BankTransactionData m_bank_transaction_data;
    private CurrencyExchangeResult m_exchange_result;
    private String m_card_track_data;
    private Boolean m_manual_entry;
    private Boolean m_reader;

    private enum CARD_STATUS
    {
      ALL_DISABLED = 0,
      READED = 1,
      MANUAL_ENTRY = 2
    }

    #endregion

    #region Constructor

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="Parent"></param>
    /// <param name="Card"></param>
    public frm_bank_transaction_data(CurrencyExchangeType ExchangeType)
    {
      this.FormTitle = "frm_bank_transaction_data";

      m_type = ExchangeType;

      m_bank_transaction_data = new BankTransactionData();

      InitializeComponent();

      InitializeControlResources();

      Init();

      if (ExchangeType == CurrencyExchangeType.CARD)
      {
        EnableCardControls(CARD_STATUS.ALL_DISABLED);
      }

    }

    //------------------------------------------------------------------------------
    // PURPOSE: Init the properties of the class.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    private void Init()
    {

      // Show keyboard if automatic OSK it's enabled
      WSIKeyboard.Toggle();


      LoadNationalitiesCombo();

      LoadBankNameData();

      EventLastAction.AddEventLastAction(this.Controls);

    }

    //------------------------------------------------------------------------------
    // PURPOSE: Show form.
    // 
    //  PARAMS:
    //      - INPUT: ExchangeResult
    //
    //      - OUTPUT: ExchangeResult
    //
    // RETURNS: DialogResult
    // 
    public DialogResult Show(ref CurrencyExchangeResult ExchangeResult)
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_bank_transaction_data", Log.Type.Message);
      DialogResult _rc;
      TransactionType _type;

      _type = ExchangeResult.BankTransactionData.TransactionType;
      m_exchange_result = ExchangeResult;

      SetScreenData();

      LoadIcon();

      _rc = this.ShowDialog();

      m_bank_transaction_data.TransactionType = _type;
      m_exchange_result.BankTransactionData = m_bank_transaction_data;

      ExchangeResult = m_exchange_result;

      return _rc;
    }

    #endregion

    #region Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE: Fill object with the screen data.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    private Boolean GetScreenData()
    {
      CurrencyExchangeSubType _card_type;

      try
      {
        switch (m_type)
        {
          case CurrencyExchangeType.CARD:
            //Mandatory data

            if (this.ef_card_number.Text.Contains("-"))
            {
              m_bank_transaction_data.DocumentNumber = MagneticStripeBankCard.UndecorateCardNumber(this.ef_card_number.Text.Trim());
            }
            else
            {
              m_bank_transaction_data.DocumentNumber = this.ef_card_number.Text.Trim();
            }

            m_bank_transaction_data.HolderName = this.ef_holder_name.Text.Trim();
            m_bank_transaction_data.ExpirationDate = this.ef_mm.Text.Trim();

            //Card Type
            switch ((Int64)this.cb_card_types.SelectedIndex)
            {
              case 0:
                _card_type = CurrencyExchangeSubType.BANK_CARD;
                break;
              case 1:
                _card_type = CurrencyExchangeSubType.DEBIT_CARD;
                break;
              case 2:
                _card_type = CurrencyExchangeSubType.CREDIT_CARD;
                break;
              default:
                _card_type = CurrencyExchangeSubType.NONE;
                break;

            }

            // To specify card pays when SpecificaBankCardTypes is enabled
            if (!GeneralParam.GetBoolean("Cashier.PaymentMethod", "BankCard.DifferentiateType", false))
            {
              m_bank_transaction_data.Type = CurrencyExchangeSubType.BANK_CARD;
            }
            else
            {
              m_bank_transaction_data.Type = _card_type;
              m_exchange_result.SubType = _card_type;
            }

            m_bank_transaction_data.CardEdited = this.chk_manual_entry.Checked;
            m_bank_transaction_data.TrackData = m_card_track_data;

            break;

          case CurrencyExchangeType.CHECK:
            m_bank_transaction_data.DocumentNumber = this.ef_check_number.Text.Trim();
            m_bank_transaction_data.AccountNumber = this.ef_account_number.Text.Trim();
            m_bank_transaction_data.HolderName = this.ef_check_payee.Text.Trim();
            m_bank_transaction_data.CheckDate = this.uc_check_date.DateValue;
            m_bank_transaction_data.RoutingNumber = this.ef_routing_number.Text.Trim();
            //Check Type
            switch ((Int64)this.cb_check_types.SelectedIndex)
            {
              case 0:
                _card_type = CurrencyExchangeSubType.CHECK;
                break;
              case 1:
                _card_type = CurrencyExchangeSubType.BEARER_CHECK;
                break;
              case 2:
                _card_type = CurrencyExchangeSubType.NOMINAL_CHECK;
                break;
              default:
                _card_type = CurrencyExchangeSubType.CHECK;
                break;
            }
            m_bank_transaction_data.Type = _card_type;
            m_exchange_result.SubType = _card_type;

            break;

          default:
            break;
        }

        m_bank_transaction_data.BankName = this.ef_bank_name.Text.Trim();
        m_bank_transaction_data.BankCountry = (Int32)this.cb_nationality.SelectedValue;
        m_bank_transaction_data.Comments = this.txt_comments.Text.Trim();

        return true;
      }
      catch (Exception _ex)
      {

        Log.Exception(_ex);
      }

      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Checks data from form.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    private Boolean IsScreenDataOk()
    {
      Decimal _threshold;

      try
      {
        switch (m_type)
        {
          case CurrencyExchangeType.CARD:
            if (String.IsNullOrEmpty(this.ef_card_number.Text.Trim()))
            {
              this.lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC", this.lbl_card_number.Text);//"Falta n�mero de  tarjeta";
              this.ef_card_number.Focus();

              return false;
            }
            break;

          case CurrencyExchangeType.CHECK:
          default:
            if (String.IsNullOrEmpty(this.ef_check_number.Text.Trim()))
            {
              this.lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC", this.lbl_check_number.Text);//"Falta n�mero de cheque";
              this.ef_check_number.Focus();

              return false;
            }

            if (this.uc_check_date.DateText != string.Empty)
            {
              if (!this.uc_check_date.ValidateFormat())
              {
                this.lbl_msg_blink.Text = Resource.String("STR_UC_TICKET_OFFLINE_DATE");//"Fecha incorrecta";
                this.uc_check_date.Focus();

                return false;
              }
            }

            if (m_exchange_result.BankTransactionData.TransactionType == TransactionType.CASH_ADVANCE)
            {
              _threshold = PaymentThresholdAuthorization.GetOutputThreshold();
            }
            else
            {
              _threshold = PaymentThresholdAuthorization.GetInputThreshold();
            }
            
            if (WSI.Common.TITO.Utils.IsTitoMode() && _threshold > 0 && _threshold <= m_exchange_result.InAmount)
            {
              if (this.cb_check_types.SelectedIndex <= 0)
              {
                this.lbl_msg_blink.Text = Resource.String("STR_BANK_TRANSACTION_CHECK_TYPE_ERROR", this.lbl_check_number.Text); // Select check type
                this.cb_check_types.Focus();

                return false;
              }
            }

            break;
        }

        return true;

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Fill screen data.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    private void SetScreenData()
    {
      String _total_amount;
      try
      {
        // Sets amount and card type
        _total_amount = ((Currency)(m_exchange_result.GrossAmount)).ToString();

        if (m_exchange_result.InType == CurrencyExchangeType.CARD)
        {
          // DHA: in CARD case add card price
          _total_amount = ((Currency)(m_exchange_result.GrossAmount + m_exchange_result.CardPrice)).ToString();

          switch (m_exchange_result.SubType)
          {
            case CurrencyExchangeSubType.CREDIT_CARD:
              this.cb_card_types.SelectedIndex = 2;
              break;

            case CurrencyExchangeSubType.DEBIT_CARD:
              this.cb_card_types.SelectedIndex = 1;
              break;

            case CurrencyExchangeSubType.NONE:
            case CurrencyExchangeSubType.BANK_CARD:
              this.cb_card_types.SelectedIndex = 0;
              break;

            default:
              this.cb_card_types.SelectedIndex = 0;
              break;
          }
        }

        this.ef_total_amount.Text = _total_amount;

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Set screen properties after swip card.
    // 
    //  PARAMS:
    //      - INPUT: Card Status
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    private void EnableCardControls(CARD_STATUS Status)
    {

      switch (Status)
      {
        case CARD_STATUS.ALL_DISABLED:
          this.ef_card_number.Enabled = false;
          this.ef_holder_name.Enabled = false;
          this.ef_mm.Enabled = false;

          this.ef_holder_name.BackColor = Color.WhiteSmoke;

          break;

        case CARD_STATUS.READED:
          this.ef_card_number.Enabled = false;
          this.ef_holder_name.Enabled = false;
          this.ef_mm.Enabled = false;

          this.ef_holder_name.BackColor = Color.WhiteSmoke;

          break;

        case CARD_STATUS.MANUAL_ENTRY:
          this.ef_card_number.Enabled = true;
          this.ef_holder_name.Enabled = true;
          this.ef_mm.Enabled = true;

          this.ef_holder_name.BackColor = Color.White;

          break;

      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Initialize Control Resources.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    private void InitializeControlResources()
    {
      Boolean _specific_card_types;

      _specific_card_types = GeneralParam.GetBoolean("Cashier.PaymentMethod", "BankCard.DifferentiateType", false);

      this.lbl_total_amount.Text = Resource.String("STR_UC_TICKET_TOTAL_AMOUNT");// Total Amount
      this.lbl_bank_name.Text = Resource.String("STR_BANK_TRANSACTION_BANK_NAME");//"Bank Name";
      this.lbl_nationality.Text = Resource.String("STR_BANK_TRANSACTION_BANK_CONTRY");//"Nationality";
      this.tab_bank_transaction_data.TabPages[1].Text = Resource.String("STR_UC_PLAYER_TRACK_COMMENTS");//"Comentarios";
      this.btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");
      this.btn_ok.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_OK");

      //format total amount textbox
      this.ef_total_amount.Enabled = false;
      this.ef_total_amount.ReadOnly = true;

      switch (m_type)
      {
        case CurrencyExchangeType.CHECK:
          this.FormTitle = Resource.String("STR_BANK_TRANSACTION_FORM_TITLE_CHECK");//"Check Transaction Data";
          this.lbl_check_number.Text = Resource.String("STR_BANK_TRANSACTION_CHECK_NUM");//"Check Number";
          this.lbl_check_account_number.Text = Resource.String("STR_BANK_TRANSACTION_ACC_NUM");//"Account Number";
          this.lbl_check_routing_number.Text = Resource.String("STR_BANK_TRANSACTION_ROUTING_NUM");//"Routing Number";
          this.lbl_check_date.Text = Resource.String("STR_SESSION_DATE");//"Date";
          this.lbl_check_payee.Text = Resource.String("STR_BANK_TRANSACTION_PAYEE");//"Payee";
          this.lbl_swip_card.Text = Resource.String("STR_BANK_TRANSACTION_INPUT_CHECK_DATA");//"Input Data from Check";
          this.tab_bank_transaction_data.TabPages[0].Text = Resource.String("STR_BANK_TRANSACTION_CHECK_DATA");//"Check Info";
          this.lbl_check_type.Text = Resource.String("STR_BANK_TRANSACTION_CHECK_TYPE");//"Check Type";
          this.cb_check_types.Items.Add("");// None
          this.cb_check_types.Items.Add(Resource.String("STR_BANK_TRANSACTION_CHECK_TYPE_BEARER_CHECK"));
          this.cb_check_types.Items.Add(Resource.String("STR_BANK_TRANSACTION_CHECK_TYPE_NOMINAL_CHECK"));


          this.tlp_card.Visible = false;
          this.chk_manual_entry.Visible = false;
          this.uc_check_date.Init();
          this.cb_card_types.Visible = false;
          this.lbl_card_type.Visible = false;

          this.ef_check_number.Focus();

          //Redim form
          this.Size = new System.Drawing.Size(this.Size.Width, this.Size.Height - 120);

          break;

        case CurrencyExchangeType.CARD:
        default:
          this.FormTitle = Resource.String("STR_BANK_TRANSACTION_FORM_TITLE_CARD");//"Bank Card Transaction Data";
          this.lbl_card_number.Text = Resource.String("STR_BANK_TRANSACTION_BANK_CARD_NUMBER");//"Card Number";
          this.lbl_card_holder_name.Text = Resource.String("STR_BANK_TRANSACTION_BANK_HOLDER_NAME");//"Holder Name";
          this.lbl_expiration_date.Text = Resource.String("STR_UC_PLAYER_TRACK_LEVEL_EXPIRATION");//"Expiration Date";
          this.chk_manual_entry.Text = Resource.String("STR_FRM_HANDPAYS_008");//"Entrada Manual";
          this.lbl_swip_card.Text = Resource.String("STR_BANK_TRANSACTION_SWIPE_CARD");//"Swipe Card";
          this.lbl_card_type.Text = Resource.String("STR_BANK_TRANSACTION_BANK_CARD_TYPE");//"Card Type";
          this.cb_card_types.Items.Add(Resource.String("STR_BANK_TRANSACTION_BANK_CARD_TYPE_CMB_OTHER"));//("None");
          this.cb_card_types.Items.Add(Resource.String("STR_BANK_TRANSACTION_BANK_CARD_TYPE_CMB_DEBIT"));//("Debit Card");
          this.cb_card_types.Items.Add(Resource.String("STR_BANK_TRANSACTION_BANK_CARD_TYPE_CMB_CREDIT"));//("Credit Card");
          this.tab_bank_transaction_data.TabPages[0].Text = Resource.String("STR_BANK_TRANSACTION_CARD_DATA");//"Card Info";
          this.lbl_exp.Text = Resource.String("STR_BANK_TRANSACTION_BANK_CARD_EXP_DATE");

          this.ef_card_number.Focus();

          this.lbl_card_type.Visible = _specific_card_types; //GP
          this.cb_card_types.Visible = _specific_card_types; //GP
          this.m_manual_entry = false;

          this.tlp_check.Visible = false;

          this.Size = new System.Drawing.Size(this.Size.Width, this.Size.Height - 170);

          KeyboardMessageFilter.RegisterHandler(this, BarcodeType.FilterBankCard, this.ProcessCardDataInput);

          break;
      }//switch (m_type)

      // LTC 06-OCT-2016
      this.ef_total_amount.TextAlign = HorizontalAlignment.Right;
      this.lbl_card_number.TextAlign = ContentAlignment.MiddleRight;
      this.lbl_card_holder_name.TextAlign = ContentAlignment.MiddleRight;
      this.lbl_expiration_date.TextAlign = ContentAlignment.MiddleRight;
      this.lbl_check_payee.TextAlign = ContentAlignment.MiddleRight;
      this.lbl_check_account_number.TextAlign = ContentAlignment.MiddleRight;
      this.lbl_check_routing_number.TextAlign = ContentAlignment.MiddleRight;
      this.lbl_bank_name.TextAlign = ContentAlignment.MiddleRight;
      this.lbl_nationality.TextAlign = ContentAlignment.MiddleRight;
      this.lbl_check_type.TextAlign = ContentAlignment.MiddleRight;

      this.lbl_exp.TextAlign = ContentAlignment.MiddleRight;
      this.chk_manual_entry.TextAlign = ContentAlignment.MiddleRight;
      this.lbl_card_type.TextAlign = ContentAlignment.MiddleRight;

    }

    //------------------------------------------------------------------------------
    // PURPOSE: Load Nationalities Combo.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    private void LoadNationalitiesCombo()
    {
      DataTable _dt_nationalities;
      DataRow _dr_empty_row;

      _dt_nationalities = CardData.GetNationalitiesList();

      // Non-mandatory field.  An empty row is needed.
      _dr_empty_row = _dt_nationalities.NewRow();
      _dr_empty_row["CO_COUNTRY_ID"] = -1;
      _dr_empty_row["CO_ADJECTIVE"] = "";

      _dt_nationalities.Rows.InsertAt(_dr_empty_row, 0);

      cb_nationality.DataSource = _dt_nationalities;
      cb_nationality.ValueMember = "CO_COUNTRY_ID";
      cb_nationality.DisplayMember = "CO_ADJECTIVE";
      cb_nationality.AllowUnlistedValues = false;
      cb_nationality.FormatValidator = new CharacterTextBox();
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Load Bank Name Data.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    private void LoadBankNameData()
    {
      List<String> _list;
      DataTable _dt;
      AutoCompleteStringCollection _collection;

      _list = new List<string>();
      _collection = new AutoCompleteStringCollection();

      _dt = GetBankNamesList();

      if (_dt != null && _dt.Rows.Count > 0)
      {
        foreach (DataRow _dr in _dt.Rows)
        {
          _list.Add(_dr[0].ToString());
        }

        _collection.AddRange(_list.ToArray());

        this.ef_bank_name.AutoCompleteCustomSource = _collection;
        this.ef_bank_name.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
        this.ef_bank_name.AutoCompleteSource = AutoCompleteSource.CustomSource;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get the list of bank introduced previosly by the user.
    // 
    //  PARAMS:
    //
    // RETURNS: DataTable
    // 
    private DataTable GetBankNamesList()
    {
      try
      {
        using (DB_TRX _trx = new DB_TRX())
        {
          DataTable _table;
          StringBuilder _sb;

          _table = new DataTable();

          _sb = new StringBuilder();
          _sb.AppendLine("   SELECT   DISTINCT BT_BANK_NAME          ");
          _sb.AppendLine("     FROM   BANK_TRANSACTIONS              ");
          _sb.AppendLine(" ORDER BY   BT_BANK_NAME                   ");

          using (DB_TRX _db_trx = new DB_TRX())
          {
            using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
            {
              using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
              {
                _sql_da.Fill(_table);
              }
            }
          }

          return _table;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return null;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Load Icon.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    private void LoadIcon()
    {
      CurrencyExchangeProperties _properties;

      _properties = CurrencyExchangeProperties.GetProperties(m_exchange_result.InCurrencyCode);

      this.pictureBox1.Image = _properties.GetIcon(m_exchange_result.InType, pictureBox1.Size.Width, pictureBox1.Size.Height);

    }

    #endregion

    #region Handlers

    //------------------------------------------------------------------------------
    // PURPOSE: Handles the operations with card reader.
    // 
    //  PARAMS:
    //      - INPUT:
    //          -   KbdMsgEvent Type: Type of event sended by MessageFilter
    //          -   Object Barcode: Casted to MagneticStripeBankCard
    //
    // RETURNS:
    // 
    private void ProcessCardDataInput(KbdMsgEvent Type, Object Barcode)
    {
      if (Type == KbdMsgEvent.EventReading0 || Type == KbdMsgEvent.EventReading1)
      {
        m_reader = false;
        this.lbl_swip_card.Text = Resource.String("STR_AC_IMPORT_PROCESSING");
        this.lbl_swip_card.Visible = !this.lbl_swip_card.Visible;
      }
      else
      {
        if (Type != KbdMsgEvent.EventReadEnd)
        {
          m_reader = true;
          this.lbl_swip_card.Visible = false;

          this.m_manual_entry = false;
          this.chk_manual_entry.Checked = false;

          EnableCardControls(CARD_STATUS.READED);

          if (!this.chk_manual_entry.Checked)
          {
            MagneticStripeBankCard _barcode = (MagneticStripeBankCard)Barcode;

            this.ef_card_number.Text = MagneticStripeBankCard.DecorateCardNumber(_barcode.CardNumber);
            this.ef_holder_name.Text = _barcode.HolderName;
            this.ef_mm.Text = _barcode.ExpirationYYMM;
            m_card_track_data = _barcode.Stripe;

            this.ef_bank_name.Focus();
          }
        }
        else
        {
          if (String.IsNullOrEmpty(this.ef_card_number.Text) || !m_reader)
          {
            this.lbl_swip_card.Text = Resource.String("STR_BANK_TRANSACTION_SWIPE_CARD");//"Swipe Card";
            this.lbl_swip_card.Visible = true;
          }
        }
      }
    }

    private void btn_ok_Click(object sender, EventArgs e)
    {
      if (!IsScreenDataOk())
      {
        this.lbl_msg_blink.Visible = true;

        return;
      }

      if (GetScreenData())
      {
        // hide keyboard and close
        WSIKeyboard.Hide();

        this.DialogResult = DialogResult.OK;

        this.Dispose();
      }
    }

    private void btn_cancel_Click(object sender, EventArgs e)
    {
      // hide keyboard and close
      WSIKeyboard.Hide();

      this.DialogResult = DialogResult.Cancel;

      this.Dispose();
    }

    private void chk_manual_entry_CheckedChanged(object sender, EventArgs e)
    {
      List<ProfilePermissions.CashierFormFuncionality> _permissions_list;
      String _error_str;

      try
      {
        if (!this.m_manual_entry && this.m_manual_entry != this.chk_manual_entry.Checked)
        {
          if (!this.ef_card_number.Enabled)
          {
            _permissions_list = new List<ProfilePermissions.CashierFormFuncionality>();
            _permissions_list.Add(ProfilePermissions.CashierFormFuncionality.EditBankCardTransactionData);

            // Check if current user is an authorized user
            if (ProfilePermissions.CheckPermissionList(_permissions_list,
                                                        ProfilePermissions.TypeOperation.RequestPasswd,
                                                        this,
                                                        Cashier.AuthorizedByUserId,
                                                        out _error_str))
            {

              EnableCardControls(CARD_STATUS.MANUAL_ENTRY);
              this.ef_card_number.Text = MagneticStripeBankCard.UndecorateCardNumber(this.ef_card_number.Text);
              this.ef_card_number.Focus();
              this.m_manual_entry = true;
            }
            else
            {
              this.chk_manual_entry.Checked = false;
            }
          }
          else
          {
            this.chk_manual_entry.Checked = true;
            this.m_manual_entry = true;
          }
        }
        else
        {
          //Unchek and disable controls if its ENABLED
          if (this.m_manual_entry)
          {
            this.chk_manual_entry.Checked = false;
            this.m_manual_entry = false;

            this.ef_card_number.Enabled = false;
            this.ef_holder_name.Enabled = false;
            this.ef_mm.Enabled = false;

            this.ef_holder_name.BackColor = Color.WhiteSmoke;
          }
        }
      }
      catch (Exception _ex)
      {

        Log.Exception(_ex);
      }
    }

    private void ef_mm_Click(object sender, EventArgs e)
    {
      if (this.ef_mm.Enabled)
      {
        this.ef_mm.SelectAll();
      }
    }

    private void btn_keyboard_Click(object sender, EventArgs e)
    {
      //Shows OSK
      WSIKeyboard.ForceToggle();

      this.ef_bank_name.Focus();
    }

    private void frm_bank_transaction_data_FormClosed(object sender, FormClosedEventArgs e)
    {
      if (this.m_type == CurrencyExchangeType.CARD)
      {
        KeyboardMessageFilter.UnregisterHandler(this, this.ProcessCardDataInput);
      }
    }

    private void frm_bank_transaction_data_Shown(object sender, EventArgs e)
    {
      // Fixed Bug 14294: Window need focus for credit card reader.
      tabPage1.Focus();
    }


    #endregion



  }
}
