using WSI.Cashier.Controls;
namespace WSI.Cashier
{
  partial class frm_change_pin
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.txt_confirm_pin = new WSI.Cashier.NumericTextBox();
      this.txt_pin = new WSI.Cashier.NumericTextBox();
      this.lbl_confirm_pin = new WSI.Cashier.Controls.uc_label();
      this.lbl_enter_pin = new WSI.Cashier.Controls.uc_label();
      this.btn_ok = new WSI.Cashier.Controls.uc_round_button();
      this.btn_cancel = new WSI.Cashier.Controls.uc_round_button();
      this.gp_card_box = new WSI.Cashier.Controls.uc_round_panel();
      this.lbl_holder_value = new WSI.Cashier.Controls.uc_label();
      this.lbl_holder = new WSI.Cashier.Controls.uc_label();
      this.lbl_trackdata = new WSI.Cashier.Controls.uc_label();
      this.lbl_account_value = new WSI.Cashier.Controls.uc_label();
      this.lbl_card = new WSI.Cashier.Controls.uc_label();
      this.lbl_account = new WSI.Cashier.Controls.uc_label();
      this.pnl_data.SuspendLayout();
      this.gp_card_box.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.Controls.Add(this.txt_confirm_pin);
      this.pnl_data.Controls.Add(this.txt_pin);
      this.pnl_data.Controls.Add(this.lbl_confirm_pin);
      this.pnl_data.Controls.Add(this.lbl_enter_pin);
      this.pnl_data.Controls.Add(this.btn_ok);
      this.pnl_data.Controls.Add(this.btn_cancel);
      this.pnl_data.Controls.Add(this.gp_card_box);
      this.pnl_data.Size = new System.Drawing.Size(526, 327);
      this.pnl_data.TabIndex = 0;
      // 
      // txt_confirm_pin
      // 
      this.txt_confirm_pin.AllowSpace = false;
      this.txt_confirm_pin.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_confirm_pin.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_confirm_pin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_confirm_pin.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_confirm_pin.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_confirm_pin.CornerRadius = 5;
      this.txt_confirm_pin.FillWithCeros = false;
      this.txt_confirm_pin.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_confirm_pin.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.txt_confirm_pin.Location = new System.Drawing.Point(288, 60);
      this.txt_confirm_pin.MaxLength = 12;
      this.txt_confirm_pin.Multiline = false;
      this.txt_confirm_pin.Name = "txt_confirm_pin";
      this.txt_confirm_pin.PasswordChar = '*';
      this.txt_confirm_pin.ReadOnly = false;
      this.txt_confirm_pin.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_confirm_pin.SelectedText = "";
      this.txt_confirm_pin.SelectionLength = 0;
      this.txt_confirm_pin.SelectionStart = 0;
      this.txt_confirm_pin.Size = new System.Drawing.Size(100, 40);
      this.txt_confirm_pin.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_confirm_pin.TabIndex = 3;
      this.txt_confirm_pin.TabStop = false;
      this.txt_confirm_pin.Text = "x1234";
      this.txt_confirm_pin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.txt_confirm_pin.UseSystemPasswordChar = false;
      this.txt_confirm_pin.WaterMark = null;
      this.txt_confirm_pin.TextChanged += new System.EventHandler(this.txt_confirm_pin_TextChanged);
      // 
      // txt_pin
      // 
      this.txt_pin.AllowSpace = false;
      this.txt_pin.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_pin.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_pin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_pin.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_pin.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_pin.CornerRadius = 5;
      this.txt_pin.FillWithCeros = false;
      this.txt_pin.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_pin.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.txt_pin.Location = new System.Drawing.Point(288, 10);
      this.txt_pin.MaxLength = 12;
      this.txt_pin.Multiline = false;
      this.txt_pin.Name = "txt_pin";
      this.txt_pin.PasswordChar = '*';
      this.txt_pin.ReadOnly = false;
      this.txt_pin.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_pin.SelectedText = "";
      this.txt_pin.SelectionLength = 0;
      this.txt_pin.SelectionStart = 0;
      this.txt_pin.Size = new System.Drawing.Size(100, 40);
      this.txt_pin.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_pin.TabIndex = 1;
      this.txt_pin.TabStop = false;
      this.txt_pin.Text = "x1234";
      this.txt_pin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.txt_pin.UseSystemPasswordChar = false;
      this.txt_pin.WaterMark = null;
      this.txt_pin.TextChanged += new System.EventHandler(this.txt_pin_TextChanged);
      // 
      // lbl_confirm_pin
      // 
      this.lbl_confirm_pin.BackColor = System.Drawing.Color.Transparent;
      this.lbl_confirm_pin.Font = new System.Drawing.Font("Montserrat", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_confirm_pin.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_confirm_pin.Location = new System.Drawing.Point(12, 66);
      this.lbl_confirm_pin.Name = "lbl_confirm_pin";
      this.lbl_confirm_pin.Size = new System.Drawing.Size(266, 25);
      this.lbl_confirm_pin.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLUE;
      this.lbl_confirm_pin.TabIndex = 2;
      this.lbl_confirm_pin.Text = "xConfirm PIN";
      this.lbl_confirm_pin.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_enter_pin
      // 
      this.lbl_enter_pin.BackColor = System.Drawing.Color.Transparent;
      this.lbl_enter_pin.Font = new System.Drawing.Font("Montserrat", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_enter_pin.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_enter_pin.Location = new System.Drawing.Point(8, 14);
      this.lbl_enter_pin.Name = "lbl_enter_pin";
      this.lbl_enter_pin.Size = new System.Drawing.Size(270, 25);
      this.lbl_enter_pin.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLUE;
      this.lbl_enter_pin.TabIndex = 0;
      this.lbl_enter_pin.Text = "xEnter the new PIN code";
      this.lbl_enter_pin.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // btn_ok
      // 
      this.btn_ok.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_ok.FlatAppearance.BorderSize = 0;
      this.btn_ok.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_ok.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_ok.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_ok.Image = null;
      this.btn_ok.Location = new System.Drawing.Point(356, 251);
      this.btn_ok.Name = "btn_ok";
      this.btn_ok.Size = new System.Drawing.Size(155, 60);
      this.btn_ok.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_ok.TabIndex = 6;
      this.btn_ok.Text = "XOK";
      this.btn_ok.UseVisualStyleBackColor = false;
      this.btn_ok.Click += new System.EventHandler(this.btn_ok_click);
      // 
      // btn_cancel
      // 
      this.btn_cancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btn_cancel.FlatAppearance.BorderSize = 0;
      this.btn_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_cancel.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_cancel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_cancel.Image = null;
      this.btn_cancel.Location = new System.Drawing.Point(188, 251);
      this.btn_cancel.Name = "btn_cancel";
      this.btn_cancel.Size = new System.Drawing.Size(155, 60);
      this.btn_cancel.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_cancel.TabIndex = 5;
      this.btn_cancel.Text = "XCANCEL";
      this.btn_cancel.UseVisualStyleBackColor = false;
      this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
      // 
      // gp_card_box
      // 
      this.gp_card_box.BackColor = System.Drawing.Color.Transparent;
      this.gp_card_box.Controls.Add(this.lbl_holder_value);
      this.gp_card_box.Controls.Add(this.lbl_holder);
      this.gp_card_box.Controls.Add(this.lbl_trackdata);
      this.gp_card_box.Controls.Add(this.lbl_account_value);
      this.gp_card_box.Controls.Add(this.lbl_card);
      this.gp_card_box.Controls.Add(this.lbl_account);
      this.gp_card_box.CornerRadius = 10;
      this.gp_card_box.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gp_card_box.ForeColor = System.Drawing.Color.Black;
      this.gp_card_box.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gp_card_box.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gp_card_box.HeaderHeight = 35;
      this.gp_card_box.HeaderSubText = null;
      this.gp_card_box.HeaderText = null;
      this.gp_card_box.Location = new System.Drawing.Point(14, 110);
      this.gp_card_box.Name = "gp_card_box";
      this.gp_card_box.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gp_card_box.Size = new System.Drawing.Size(497, 127);
      this.gp_card_box.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.gp_card_box.TabIndex = 4;
      this.gp_card_box.Text = "xCard Data";
      // 
      // lbl_holder_value
      // 
      this.lbl_holder_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_holder_value.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_holder_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_holder_value.Location = new System.Drawing.Point(204, 97);
      this.lbl_holder_value.Name = "lbl_holder_value";
      this.lbl_holder_value.Size = new System.Drawing.Size(290, 20);
      this.lbl_holder_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_holder_value.TabIndex = 5;
      this.lbl_holder_value.Text = "XXXXXXXXXXXXXXXXXX";
      this.lbl_holder_value.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.lbl_holder_value.UseMnemonic = false;
      // 
      // lbl_holder
      // 
      this.lbl_holder.BackColor = System.Drawing.Color.Transparent;
      this.lbl_holder.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_holder.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_holder.Location = new System.Drawing.Point(24, 99);
      this.lbl_holder.Name = "lbl_holder";
      this.lbl_holder.Size = new System.Drawing.Size(169, 16);
      this.lbl_holder.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_holder.TabIndex = 4;
      this.lbl_holder.Text = "xHolder Name";
      this.lbl_holder.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_trackdata
      // 
      this.lbl_trackdata.BackColor = System.Drawing.Color.Transparent;
      this.lbl_trackdata.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_trackdata.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_trackdata.Location = new System.Drawing.Point(204, 71);
      this.lbl_trackdata.Name = "lbl_trackdata";
      this.lbl_trackdata.Size = new System.Drawing.Size(290, 16);
      this.lbl_trackdata.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_trackdata.TabIndex = 3;
      this.lbl_trackdata.Text = "XXXXXXXXXXXXXXXXXX";
      this.lbl_trackdata.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_account_value
      // 
      this.lbl_account_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_account_value.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_account_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_account_value.Location = new System.Drawing.Point(204, 44);
      this.lbl_account_value.Name = "lbl_account_value";
      this.lbl_account_value.Size = new System.Drawing.Size(290, 16);
      this.lbl_account_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_account_value.TabIndex = 1;
      this.lbl_account_value.Text = "XXXXXXXXXXXXXXXXXX";
      this.lbl_account_value.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_card
      // 
      this.lbl_card.BackColor = System.Drawing.Color.Transparent;
      this.lbl_card.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_card.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_card.Location = new System.Drawing.Point(24, 71);
      this.lbl_card.Name = "lbl_card";
      this.lbl_card.Size = new System.Drawing.Size(169, 16);
      this.lbl_card.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_card.TabIndex = 2;
      this.lbl_card.Text = "xCard Number";
      this.lbl_card.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_account
      // 
      this.lbl_account.BackColor = System.Drawing.Color.Transparent;
      this.lbl_account.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_account.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_account.Location = new System.Drawing.Point(24, 44);
      this.lbl_account.Name = "lbl_account";
      this.lbl_account.Size = new System.Drawing.Size(169, 16);
      this.lbl_account.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_account.TabIndex = 0;
      this.lbl_account.Text = "xAccount";
      this.lbl_account.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // frm_change_pin
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(526, 382);
      this.ControlBox = false;
      this.Name = "frm_change_pin";
      this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
      this.Style = WSI.Cashier.Controls.frm_base.BaseFormStyle.PRINCIPAL;
      this.Text = "xChange Pin";
      this.Shown += new System.EventHandler(this.frm_change_pin_Shown);
      this.pnl_data.ResumeLayout(false);
      this.gp_card_box.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private NumericTextBox txt_confirm_pin;
    private NumericTextBox txt_pin;
    private uc_label lbl_confirm_pin;
    private uc_label lbl_enter_pin;
    private uc_round_button btn_ok;
    private uc_round_button btn_cancel;
    private uc_round_panel gp_card_box;
    private uc_label lbl_holder_value;
    private uc_label lbl_holder;
    private uc_label lbl_trackdata;
    private uc_label lbl_account_value;
    private uc_label lbl_card;
    private uc_label lbl_account;
  }
}