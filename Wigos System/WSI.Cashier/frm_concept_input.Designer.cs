namespace WSI.Cashier
{
  partial class frm_concept_input
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_concept_input));
      this.lbl_price_per_unit = new WSI.Cashier.Controls.uc_label();
      this.gp_digits_box = new WSI.Cashier.Controls.uc_round_panel();
      this.lbl_currency = new WSI.Cashier.Controls.uc_label();
      this.btn_money_4 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_money_3 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_money_2 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_money_1 = new WSI.Cashier.Controls.uc_round_button();
      this.txt_amount = new WSI.Cashier.Controls.uc_round_textbox();
      this.btn_num_1 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_back = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_2 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_intro = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_3 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_4 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_10 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_5 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_dot = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_6 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_9 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_7 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_8 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_cancel = new WSI.Cashier.Controls.uc_round_button();
      this.btn_ok = new WSI.Cashier.Controls.uc_round_button();
      this.gb_total = new WSI.Cashier.Controls.uc_round_panel();
      this.lbl_total_amount = new WSI.Cashier.Controls.uc_label();
      this.uc_payment_method1 = new WSI.Cashier.uc_payment_method();
      this.pnl_data.SuspendLayout();
      this.gp_digits_box.SuspendLayout();
      this.gb_total.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.Controls.Add(this.uc_payment_method1);
      this.pnl_data.Controls.Add(this.btn_cancel);
      this.pnl_data.Controls.Add(this.btn_ok);
      this.pnl_data.Controls.Add(this.lbl_price_per_unit);
      this.pnl_data.Controls.Add(this.gp_digits_box);
      this.pnl_data.Controls.Add(this.gb_total);
      this.pnl_data.Size = new System.Drawing.Size(780, 409);
      this.pnl_data.TabIndex = 0;
      // 
      // lbl_price_per_unit
      // 
      this.lbl_price_per_unit.AutoSize = true;
      this.lbl_price_per_unit.BackColor = System.Drawing.Color.Transparent;
      this.lbl_price_per_unit.Font = new System.Drawing.Font("Open Sans", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_price_per_unit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_price_per_unit.Location = new System.Drawing.Point(6, 8);
      this.lbl_price_per_unit.Name = "lbl_price_per_unit";
      this.lbl_price_per_unit.Size = new System.Drawing.Size(78, 17);
      this.lbl_price_per_unit.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK_BOLD;
      this.lbl_price_per_unit.TabIndex = 0;
      this.lbl_price_per_unit.Text = "xUnit: $0.00";
      // 
      // gp_digits_box
      // 
      this.gp_digits_box.BackColor = System.Drawing.Color.Transparent;
      this.gp_digits_box.BorderColor = System.Drawing.Color.Empty;
      this.gp_digits_box.Controls.Add(this.lbl_currency);
      this.gp_digits_box.Controls.Add(this.btn_money_4);
      this.gp_digits_box.Controls.Add(this.btn_money_3);
      this.gp_digits_box.Controls.Add(this.btn_money_2);
      this.gp_digits_box.Controls.Add(this.btn_money_1);
      this.gp_digits_box.Controls.Add(this.txt_amount);
      this.gp_digits_box.Controls.Add(this.btn_num_1);
      this.gp_digits_box.Controls.Add(this.btn_back);
      this.gp_digits_box.Controls.Add(this.btn_num_2);
      this.gp_digits_box.Controls.Add(this.btn_intro);
      this.gp_digits_box.Controls.Add(this.btn_num_3);
      this.gp_digits_box.Controls.Add(this.btn_num_4);
      this.gp_digits_box.Controls.Add(this.btn_num_10);
      this.gp_digits_box.Controls.Add(this.btn_num_5);
      this.gp_digits_box.Controls.Add(this.btn_num_dot);
      this.gp_digits_box.Controls.Add(this.btn_num_6);
      this.gp_digits_box.Controls.Add(this.btn_num_9);
      this.gp_digits_box.Controls.Add(this.btn_num_7);
      this.gp_digits_box.Controls.Add(this.btn_num_8);
      this.gp_digits_box.CornerRadius = 10;
      this.gp_digits_box.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gp_digits_box.ForeColor = System.Drawing.Color.Black;
      this.gp_digits_box.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.gp_digits_box.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gp_digits_box.HeaderHeight = 55;
      this.gp_digits_box.HeaderSubText = null;
      this.gp_digits_box.HeaderText = "";
      this.gp_digits_box.Location = new System.Drawing.Point(9, 26);
      this.gp_digits_box.Name = "gp_digits_box";
      this.gp_digits_box.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gp_digits_box.Size = new System.Drawing.Size(380, 378);
      this.gp_digits_box.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NONE;
      this.gp_digits_box.TabIndex = 1;
      // 
      // lbl_currency
      // 
      this.lbl_currency.BackColor = System.Drawing.Color.Transparent;
      this.lbl_currency.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_currency.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_currency.Location = new System.Drawing.Point(1, 63);
      this.lbl_currency.Name = "lbl_currency";
      this.lbl_currency.Size = new System.Drawing.Size(231, 18);
      this.lbl_currency.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_currency.TabIndex = 24;
      this.lbl_currency.Text = "xMsgCurrency";
      this.lbl_currency.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // btn_money_4
      // 
      this.btn_money_4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_money_4.CornerRadius = 0;
      this.btn_money_4.FlatAppearance.BorderSize = 0;
      this.btn_money_4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_money_4.Font = new System.Drawing.Font("Open Sans Semibold", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_money_4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_money_4.Image = null;
      this.btn_money_4.IsSelected = false;
      this.btn_money_4.Location = new System.Drawing.Point(286, 85);
      this.btn_money_4.Name = "btn_money_4";
      this.btn_money_4.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_money_4.SelectedColor = System.Drawing.Color.Empty;
      this.btn_money_4.Size = new System.Drawing.Size(90, 60);
      this.btn_money_4.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_MONEY;
      this.btn_money_4.TabIndex = 19;
      this.btn_money_4.TabStop = false;
      this.btn_money_4.Text = "2000 $";
      this.btn_money_4.UseVisualStyleBackColor = false;
      this.btn_money_4.Click += new System.EventHandler(this.btn_money_4_Click);
      this.btn_money_4.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_amount_input_KeyPress);
      this.btn_money_4.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_amount_input_PreviewKeyDown);
      // 
      // btn_money_3
      // 
      this.btn_money_3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_money_3.CornerRadius = 0;
      this.btn_money_3.FlatAppearance.BorderSize = 0;
      this.btn_money_3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_money_3.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_money_3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_money_3.Image = null;
      this.btn_money_3.IsSelected = false;
      this.btn_money_3.Location = new System.Drawing.Point(192, 85);
      this.btn_money_3.Name = "btn_money_3";
      this.btn_money_3.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_money_3.SelectedColor = System.Drawing.Color.Empty;
      this.btn_money_3.Size = new System.Drawing.Size(90, 60);
      this.btn_money_3.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_MONEY;
      this.btn_money_3.TabIndex = 18;
      this.btn_money_3.TabStop = false;
      this.btn_money_3.Text = "100";
      this.btn_money_3.UseVisualStyleBackColor = false;
      this.btn_money_3.Click += new System.EventHandler(this.btn_money_3_Click);
      this.btn_money_3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_amount_input_KeyPress);
      this.btn_money_3.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_amount_input_PreviewKeyDown);
      // 
      // btn_money_2
      // 
      this.btn_money_2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_money_2.CornerRadius = 0;
      this.btn_money_2.FlatAppearance.BorderSize = 0;
      this.btn_money_2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_money_2.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_money_2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_money_2.Image = null;
      this.btn_money_2.IsSelected = false;
      this.btn_money_2.Location = new System.Drawing.Point(98, 85);
      this.btn_money_2.Name = "btn_money_2";
      this.btn_money_2.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_money_2.SelectedColor = System.Drawing.Color.Empty;
      this.btn_money_2.Size = new System.Drawing.Size(90, 60);
      this.btn_money_2.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_MONEY;
      this.btn_money_2.TabIndex = 17;
      this.btn_money_2.TabStop = false;
      this.btn_money_2.Text = "50";
      this.btn_money_2.UseVisualStyleBackColor = false;
      this.btn_money_2.Click += new System.EventHandler(this.btn_money_2_Click);
      this.btn_money_2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_amount_input_KeyPress);
      this.btn_money_2.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_amount_input_PreviewKeyDown);
      // 
      // btn_money_1
      // 
      this.btn_money_1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_money_1.CornerRadius = 0;
      this.btn_money_1.FlatAppearance.BorderSize = 0;
      this.btn_money_1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_money_1.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_money_1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_money_1.Image = null;
      this.btn_money_1.IsSelected = false;
      this.btn_money_1.Location = new System.Drawing.Point(4, 85);
      this.btn_money_1.Name = "btn_money_1";
      this.btn_money_1.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_money_1.SelectedColor = System.Drawing.Color.Empty;
      this.btn_money_1.Size = new System.Drawing.Size(90, 60);
      this.btn_money_1.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_MONEY;
      this.btn_money_1.TabIndex = 16;
      this.btn_money_1.TabStop = false;
      this.btn_money_1.Text = "10";
      this.btn_money_1.UseVisualStyleBackColor = false;
      this.btn_money_1.Click += new System.EventHandler(this.btn_money_1_Click);
      this.btn_money_1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_amount_input_KeyPress);
      this.btn_money_1.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_amount_input_PreviewKeyDown);
      // 
      // txt_amount
      // 
      this.txt_amount.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_amount.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_amount.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.txt_amount.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.txt_amount.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_amount.CornerRadius = 0;
      this.txt_amount.Font = new System.Drawing.Font("Open Sans Semibold", 40F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_amount.Location = new System.Drawing.Point(3, 5);
      this.txt_amount.MaxLength = 18;
      this.txt_amount.Multiline = false;
      this.txt_amount.Name = "txt_amount";
      this.txt_amount.PasswordChar = '\0';
      this.txt_amount.ReadOnly = true;
      this.txt_amount.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_amount.SelectedText = "";
      this.txt_amount.SelectionLength = 0;
      this.txt_amount.SelectionStart = 0;
      this.txt_amount.Size = new System.Drawing.Size(374, 45);
      this.txt_amount.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.CALCULATOR;
      this.txt_amount.TabIndex = 14;
      this.txt_amount.TabStop = false;
      this.txt_amount.Text = "123";
      this.txt_amount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      this.txt_amount.UseSystemPasswordChar = false;
      this.txt_amount.WaterMark = null;
      this.txt_amount.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(153)))), ((int)(((byte)(49)))));
      this.txt_amount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_amount_input_KeyPress);
      this.txt_amount.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_amount_input_PreviewKeyDown);
      // 
      // btn_num_1
      // 
      this.btn_num_1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_1.CornerRadius = 0;
      this.btn_num_1.FlatAppearance.BorderSize = 0;
      this.btn_num_1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_1.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_1.Image = null;
      this.btn_num_1.IsSelected = false;
      this.btn_num_1.Location = new System.Drawing.Point(4, 150);
      this.btn_num_1.Name = "btn_num_1";
      this.btn_num_1.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_1.SelectedColor = System.Drawing.Color.Empty;
      this.btn_num_1.Size = new System.Drawing.Size(70, 50);
      this.btn_num_1.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_1.TabIndex = 0;
      this.btn_num_1.TabStop = false;
      this.btn_num_1.Text = "1";
      this.btn_num_1.UseVisualStyleBackColor = false;
      this.btn_num_1.Click += new System.EventHandler(this.btn_num_1_Click);
      this.btn_num_1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_amount_input_KeyPress);
      this.btn_num_1.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_amount_input_PreviewKeyDown);
      // 
      // btn_back
      // 
      this.btn_back.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_back.CornerRadius = 0;
      this.btn_back.FlatAppearance.BorderSize = 0;
      this.btn_back.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_back.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_back.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_back.Image = ((System.Drawing.Image)(resources.GetObject("btn_back.Image")));
      this.btn_back.IsSelected = false;
      this.btn_back.Location = new System.Drawing.Point(235, 150);
      this.btn_back.Name = "btn_back";
      this.btn_back.SelectedColor = System.Drawing.Color.Empty;
      this.btn_back.Size = new System.Drawing.Size(141, 50);
      this.btn_back.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_BACKSPACE;
      this.btn_back.TabIndex = 13;
      this.btn_back.TabStop = false;
      this.btn_back.UseVisualStyleBackColor = false;
      this.btn_back.Click += new System.EventHandler(this.btn_back_Click);
      this.btn_back.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_amount_input_KeyPress);
      this.btn_back.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_amount_input_PreviewKeyDown);
      // 
      // btn_num_2
      // 
      this.btn_num_2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_2.CornerRadius = 0;
      this.btn_num_2.FlatAppearance.BorderSize = 0;
      this.btn_num_2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_2.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_2.Image = null;
      this.btn_num_2.IsSelected = false;
      this.btn_num_2.Location = new System.Drawing.Point(81, 150);
      this.btn_num_2.Name = "btn_num_2";
      this.btn_num_2.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_2.SelectedColor = System.Drawing.Color.Empty;
      this.btn_num_2.Size = new System.Drawing.Size(70, 50);
      this.btn_num_2.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_2.TabIndex = 1;
      this.btn_num_2.TabStop = false;
      this.btn_num_2.Text = "2";
      this.btn_num_2.UseVisualStyleBackColor = false;
      this.btn_num_2.Click += new System.EventHandler(this.btn_num_2_Click);
      this.btn_num_2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_amount_input_KeyPress);
      this.btn_num_2.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_amount_input_PreviewKeyDown);
      // 
      // btn_intro
      // 
      this.btn_intro.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(153)))), ((int)(((byte)(49)))));
      this.btn_intro.CornerRadius = 0;
      this.btn_intro.FlatAppearance.BorderSize = 0;
      this.btn_intro.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_intro.Font = new System.Drawing.Font("Open Sans Semibold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_intro.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_intro.Image = ((System.Drawing.Image)(resources.GetObject("btn_intro.Image")));
      this.btn_intro.IsSelected = false;
      this.btn_intro.Location = new System.Drawing.Point(235, 205);
      this.btn_intro.Name = "btn_intro";
      this.btn_intro.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_intro.SelectedColor = System.Drawing.Color.Empty;
      this.btn_intro.Size = new System.Drawing.Size(141, 160);
      this.btn_intro.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_ENTER;
      this.btn_intro.TabIndex = 12;
      this.btn_intro.UseVisualStyleBackColor = false;
      this.btn_intro.Click += new System.EventHandler(this.btn_intro_Click);
      this.btn_intro.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_amount_input_KeyPress);
      this.btn_intro.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_amount_input_PreviewKeyDown);
      // 
      // btn_num_3
      // 
      this.btn_num_3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_3.CornerRadius = 0;
      this.btn_num_3.FlatAppearance.BorderSize = 0;
      this.btn_num_3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_3.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_3.Image = null;
      this.btn_num_3.IsSelected = false;
      this.btn_num_3.Location = new System.Drawing.Point(158, 150);
      this.btn_num_3.Name = "btn_num_3";
      this.btn_num_3.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_3.SelectedColor = System.Drawing.Color.Empty;
      this.btn_num_3.Size = new System.Drawing.Size(70, 50);
      this.btn_num_3.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_3.TabIndex = 2;
      this.btn_num_3.TabStop = false;
      this.btn_num_3.Text = "3";
      this.btn_num_3.UseVisualStyleBackColor = false;
      this.btn_num_3.Click += new System.EventHandler(this.btn_num_3_Click);
      this.btn_num_3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_amount_input_KeyPress);
      this.btn_num_3.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_amount_input_PreviewKeyDown);
      // 
      // btn_num_4
      // 
      this.btn_num_4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_4.CornerRadius = 0;
      this.btn_num_4.FlatAppearance.BorderSize = 0;
      this.btn_num_4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_4.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_4.Image = null;
      this.btn_num_4.IsSelected = false;
      this.btn_num_4.Location = new System.Drawing.Point(4, 205);
      this.btn_num_4.Name = "btn_num_4";
      this.btn_num_4.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_4.SelectedColor = System.Drawing.Color.Empty;
      this.btn_num_4.Size = new System.Drawing.Size(70, 50);
      this.btn_num_4.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_4.TabIndex = 3;
      this.btn_num_4.TabStop = false;
      this.btn_num_4.Text = "4";
      this.btn_num_4.UseVisualStyleBackColor = false;
      this.btn_num_4.Click += new System.EventHandler(this.btn_num_4_Click);
      this.btn_num_4.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_amount_input_KeyPress);
      this.btn_num_4.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_amount_input_PreviewKeyDown);
      // 
      // btn_num_10
      // 
      this.btn_num_10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_10.CornerRadius = 0;
      this.btn_num_10.FlatAppearance.BorderSize = 0;
      this.btn_num_10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_10.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_10.Image = null;
      this.btn_num_10.IsSelected = false;
      this.btn_num_10.Location = new System.Drawing.Point(4, 315);
      this.btn_num_10.Name = "btn_num_10";
      this.btn_num_10.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_10.SelectedColor = System.Drawing.Color.Empty;
      this.btn_num_10.Size = new System.Drawing.Size(145, 50);
      this.btn_num_10.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_10.TabIndex = 10;
      this.btn_num_10.TabStop = false;
      this.btn_num_10.Text = "0";
      this.btn_num_10.UseVisualStyleBackColor = false;
      this.btn_num_10.Click += new System.EventHandler(this.btn_num_10_Click);
      this.btn_num_10.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_amount_input_KeyPress);
      this.btn_num_10.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_amount_input_PreviewKeyDown);
      // 
      // btn_num_5
      // 
      this.btn_num_5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_5.CornerRadius = 0;
      this.btn_num_5.FlatAppearance.BorderSize = 0;
      this.btn_num_5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_5.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_5.Image = null;
      this.btn_num_5.IsSelected = false;
      this.btn_num_5.Location = new System.Drawing.Point(81, 205);
      this.btn_num_5.Name = "btn_num_5";
      this.btn_num_5.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_5.SelectedColor = System.Drawing.Color.Empty;
      this.btn_num_5.Size = new System.Drawing.Size(70, 50);
      this.btn_num_5.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_5.TabIndex = 4;
      this.btn_num_5.TabStop = false;
      this.btn_num_5.Text = "5";
      this.btn_num_5.UseVisualStyleBackColor = false;
      this.btn_num_5.Click += new System.EventHandler(this.btn_num_5_Click);
      this.btn_num_5.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_amount_input_KeyPress);
      this.btn_num_5.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_amount_input_PreviewKeyDown);
      // 
      // btn_num_dot
      // 
      this.btn_num_dot.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_dot.CornerRadius = 0;
      this.btn_num_dot.FlatAppearance.BorderSize = 0;
      this.btn_num_dot.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_dot.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_dot.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_dot.Image = null;
      this.btn_num_dot.IsSelected = false;
      this.btn_num_dot.Location = new System.Drawing.Point(158, 315);
      this.btn_num_dot.Name = "btn_num_dot";
      this.btn_num_dot.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_dot.SelectedColor = System.Drawing.Color.Empty;
      this.btn_num_dot.Size = new System.Drawing.Size(70, 50);
      this.btn_num_dot.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_dot.TabIndex = 9;
      this.btn_num_dot.TabStop = false;
      this.btn_num_dot.Text = "�";
      this.btn_num_dot.UseVisualStyleBackColor = false;
      this.btn_num_dot.Click += new System.EventHandler(this.btn_num_dot_Click);
      this.btn_num_dot.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_amount_input_KeyPress);
      this.btn_num_dot.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_amount_input_PreviewKeyDown);
      // 
      // btn_num_6
      // 
      this.btn_num_6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_6.CornerRadius = 0;
      this.btn_num_6.FlatAppearance.BorderSize = 0;
      this.btn_num_6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_6.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_6.Image = null;
      this.btn_num_6.IsSelected = false;
      this.btn_num_6.Location = new System.Drawing.Point(158, 205);
      this.btn_num_6.Name = "btn_num_6";
      this.btn_num_6.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_6.SelectedColor = System.Drawing.Color.Empty;
      this.btn_num_6.Size = new System.Drawing.Size(70, 50);
      this.btn_num_6.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_6.TabIndex = 5;
      this.btn_num_6.TabStop = false;
      this.btn_num_6.Text = "6";
      this.btn_num_6.UseVisualStyleBackColor = false;
      this.btn_num_6.Click += new System.EventHandler(this.btn_num_6_Click);
      this.btn_num_6.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_amount_input_KeyPress);
      this.btn_num_6.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_amount_input_PreviewKeyDown);
      // 
      // btn_num_9
      // 
      this.btn_num_9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_9.CornerRadius = 0;
      this.btn_num_9.FlatAppearance.BorderSize = 0;
      this.btn_num_9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_9.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_9.Image = null;
      this.btn_num_9.IsSelected = false;
      this.btn_num_9.Location = new System.Drawing.Point(158, 260);
      this.btn_num_9.Name = "btn_num_9";
      this.btn_num_9.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_9.SelectedColor = System.Drawing.Color.Empty;
      this.btn_num_9.Size = new System.Drawing.Size(70, 50);
      this.btn_num_9.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_9.TabIndex = 8;
      this.btn_num_9.TabStop = false;
      this.btn_num_9.Text = "9";
      this.btn_num_9.UseVisualStyleBackColor = false;
      this.btn_num_9.Click += new System.EventHandler(this.btn_num_9_Click);
      this.btn_num_9.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_amount_input_KeyPress);
      this.btn_num_9.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_amount_input_PreviewKeyDown);
      // 
      // btn_num_7
      // 
      this.btn_num_7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_7.CornerRadius = 0;
      this.btn_num_7.FlatAppearance.BorderSize = 0;
      this.btn_num_7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_7.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_7.Image = null;
      this.btn_num_7.IsSelected = false;
      this.btn_num_7.Location = new System.Drawing.Point(4, 260);
      this.btn_num_7.Name = "btn_num_7";
      this.btn_num_7.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_7.SelectedColor = System.Drawing.Color.Empty;
      this.btn_num_7.Size = new System.Drawing.Size(70, 50);
      this.btn_num_7.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_7.TabIndex = 6;
      this.btn_num_7.TabStop = false;
      this.btn_num_7.Text = "7";
      this.btn_num_7.UseVisualStyleBackColor = false;
      this.btn_num_7.Click += new System.EventHandler(this.btn_num_7_Click);
      this.btn_num_7.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_amount_input_KeyPress);
      this.btn_num_7.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_amount_input_PreviewKeyDown);
      // 
      // btn_num_8
      // 
      this.btn_num_8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_8.CornerRadius = 0;
      this.btn_num_8.FlatAppearance.BorderSize = 0;
      this.btn_num_8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_8.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_8.Image = null;
      this.btn_num_8.IsSelected = false;
      this.btn_num_8.Location = new System.Drawing.Point(81, 260);
      this.btn_num_8.Name = "btn_num_8";
      this.btn_num_8.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_8.SelectedColor = System.Drawing.Color.Empty;
      this.btn_num_8.Size = new System.Drawing.Size(70, 50);
      this.btn_num_8.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_8.TabIndex = 7;
      this.btn_num_8.TabStop = false;
      this.btn_num_8.Text = "8";
      this.btn_num_8.UseVisualStyleBackColor = false;
      this.btn_num_8.Click += new System.EventHandler(this.btn_num_8_Click);
      this.btn_num_8.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_amount_input_KeyPress);
      this.btn_num_8.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_amount_input_PreviewKeyDown);
      // 
      // btn_cancel
      // 
      this.btn_cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.btn_cancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btn_cancel.FlatAppearance.BorderSize = 0;
      this.btn_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_cancel.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_cancel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_cancel.Image = null;
      this.btn_cancel.IsSelected = false;
      this.btn_cancel.Location = new System.Drawing.Point(451, 331);
      this.btn_cancel.Name = "btn_cancel";
      this.btn_cancel.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_cancel.Size = new System.Drawing.Size(155, 60);
      this.btn_cancel.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_cancel.TabIndex = 0;
      this.btn_cancel.Text = "XCANCEL";
      this.btn_cancel.UseVisualStyleBackColor = false;
      this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
      this.btn_cancel.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_amount_input_KeyPress);
      // 
      // btn_ok
      // 
      this.btn_ok.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.btn_ok.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_ok.FlatAppearance.BorderSize = 0;
      this.btn_ok.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_ok.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_ok.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_ok.Image = null;
      this.btn_ok.IsSelected = false;
      this.btn_ok.Location = new System.Drawing.Point(617, 331);
      this.btn_ok.Name = "btn_ok";
      this.btn_ok.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_ok.Size = new System.Drawing.Size(155, 60);
      this.btn_ok.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_ok.TabIndex = 1;
      this.btn_ok.Text = "XOK";
      this.btn_ok.UseVisualStyleBackColor = false;
      this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
      this.btn_ok.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_amount_input_KeyPress);
      // 
      // gb_total
      // 
      this.gb_total.BackColor = System.Drawing.Color.Transparent;
      this.gb_total.BorderColor = System.Drawing.Color.Empty;
      this.gb_total.Controls.Add(this.lbl_total_amount);
      this.gb_total.CornerRadius = 10;
      this.gb_total.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_total.ForeColor = System.Drawing.Color.Black;
      this.gb_total.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.gb_total.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_total.HeaderHeight = 35;
      this.gb_total.HeaderSubText = null;
      this.gb_total.HeaderText = "XTOTAL";
      this.gb_total.Location = new System.Drawing.Point(393, 147);
      this.gb_total.Name = "gb_total";
      this.gb_total.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.gb_total.Size = new System.Drawing.Size(382, 115);
      this.gb_total.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.TOTAL;
      this.gb_total.TabIndex = 3;
      // 
      // lbl_total_amount
      // 
      this.lbl_total_amount.BackColor = System.Drawing.Color.Transparent;
      this.lbl_total_amount.Font = new System.Drawing.Font("Open Sans Semibold", 32F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_total_amount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.lbl_total_amount.Location = new System.Drawing.Point(5, 50);
      this.lbl_total_amount.Name = "lbl_total_amount";
      this.lbl_total_amount.Size = new System.Drawing.Size(372, 46);
      this.lbl_total_amount.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_32PX_WHITE;
      this.lbl_total_amount.TabIndex = 26;
      this.lbl_total_amount.Text = "xTotal: $XXX.XX";
      this.lbl_total_amount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // uc_payment_method1
      // 
      this.uc_payment_method1.BtnSelectedCurrencyEnabled = true;
      this.uc_payment_method1.Location = new System.Drawing.Point(393, 26);
      this.uc_payment_method1.Name = "uc_payment_method1";
      this.uc_payment_method1.Size = new System.Drawing.Size(382, 116);
      this.uc_payment_method1.TabIndex = 4;
      this.uc_payment_method1.CurrencyChanged += new WSI.Cashier.uc_payment_method.CurrencyChangedHandler(this.uc_payment_method1_CurrencyChanged);
      // 
      // frm_concept_input
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.btn_cancel;
      this.ClientSize = new System.Drawing.Size(780, 464);
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "frm_concept_input";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Style = WSI.Cashier.Controls.frm_base.BaseFormStyle.PRINCIPAL;
      this.Text = "frm_add_credit";
      this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_amount_input_KeyPress);
      this.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_amount_input_PreviewKeyDown);
      this.pnl_data.ResumeLayout(false);
      this.pnl_data.PerformLayout();
      this.gp_digits_box.ResumeLayout(false);
      this.gb_total.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private WSI.Cashier.Controls.uc_round_button btn_num_1;
    private WSI.Cashier.Controls.uc_round_button btn_num_10;
    private WSI.Cashier.Controls.uc_round_button btn_num_dot;
    private WSI.Cashier.Controls.uc_round_button btn_num_9;
    private WSI.Cashier.Controls.uc_round_button btn_num_8;
    private WSI.Cashier.Controls.uc_round_button btn_num_7;
    private WSI.Cashier.Controls.uc_round_button btn_num_6;
    private WSI.Cashier.Controls.uc_round_button btn_num_5;
    private WSI.Cashier.Controls.uc_round_button btn_num_4;
    private WSI.Cashier.Controls.uc_round_button btn_num_3;
    private WSI.Cashier.Controls.uc_round_button btn_num_2;
    private WSI.Cashier.Controls.uc_round_button btn_back;
    private WSI.Cashier.Controls.uc_round_button btn_intro;
    private WSI.Cashier.Controls.uc_round_textbox txt_amount;
    private WSI.Cashier.Controls.uc_round_panel gp_digits_box;
    private WSI.Cashier.Controls.uc_round_button btn_cancel;
    private WSI.Cashier.Controls.uc_round_button btn_ok;
    private WSI.Cashier.Controls.uc_round_button btn_money_4;
    private WSI.Cashier.Controls.uc_round_button btn_money_3;
    private WSI.Cashier.Controls.uc_round_button btn_money_2;
    private WSI.Cashier.Controls.uc_round_button btn_money_1;
    private WSI.Cashier.Controls.uc_round_panel gb_total;
    private WSI.Cashier.Controls.uc_label lbl_total_amount;
    private WSI.Cashier.Controls.uc_label lbl_currency;
    private WSI.Cashier.Controls.uc_label lbl_price_per_unit;
    private uc_payment_method uc_payment_method1;
  }
}