//////namespace WSI.Cashier
//////{
//////  partial class uc_points_balance
//////  {
//////    /// <summary> 
//////    /// Required designer variable.
//////    /// </summary>
//////    private System.ComponentModel.IContainer components = null;

//////    /// <summary> 
//////    /// Clean up any resources being used.
//////    /// </summary>
//////    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
//////    protected override void Dispose(bool disposing)
//////    {
//////      if (disposing && (components != null))
//////      {
//////        components.Dispose();
//////      }
//////      base.Dispose(disposing);
//////    }

//////    #region Component Designer generated code

//////    /// <summary> 
//////    /// Required method for Designer support - do not modify 
//////    /// the contents of this method with the code editor.
//////    /// </summary>
//////    private void InitializeComponent()
//////    {
//////      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(uc_points_balance));
//////      this.pictureBox2 = new System.Windows.Forms.PictureBox();
//////      this.lbl_credit = new System.Windows.Forms.Label();
//////      this.lbl_credit_title = new System.Windows.Forms.Label();
//////      this.lbl_points_title = new System.Windows.Forms.Label();
//////      this.lbl_bar = new System.Windows.Forms.Label();
//////      this.lbl_bar_title = new System.Windows.Forms.Label();
//////      this.lbl_expiration = new System.Windows.Forms.Label();
//////      this.lbl_expiration_title = new System.Windows.Forms.Label();
//////      this.lbl_points = new System.Windows.Forms.Label();
//////      this.lbl_card_title = new System.Windows.Forms.Label();
//////      this.lbl_card_type = new System.Windows.Forms.Label();
//////      this.gb_points = new System.Windows.Forms.GroupBox();
//////      ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
//////      this.gb_points.SuspendLayout();
//////      this.SuspendLayout();
//////      // 
//////      // pictureBox2
//////      // 
//////      this.pictureBox2.Image = global::WSI.Cashier.Properties.Resources.points_small;
//////      this.pictureBox2.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox2.InitialImage")));
//////      this.pictureBox2.Location = new System.Drawing.Point(3, 3);
//////      this.pictureBox2.Name = "pictureBox2";
//////      this.pictureBox2.Size = new System.Drawing.Size(64, 59);
//////      this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
//////      this.pictureBox2.TabIndex = 68;
//////      this.pictureBox2.TabStop = false;
//////      // 
//////      // lbl_credit
//////      // 
//////      this.lbl_credit.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
//////      this.lbl_credit.Location = new System.Drawing.Point(173, 57);
//////      this.lbl_credit.Name = "lbl_credit";
//////      this.lbl_credit.Size = new System.Drawing.Size(126, 20);
//////      this.lbl_credit.TabIndex = 85;
//////      this.lbl_credit.Text = "0,00";
//////      this.lbl_credit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
//////      // 
//////      // lbl_credit_title
//////      // 
//////      this.lbl_credit_title.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//////      this.lbl_credit_title.Location = new System.Drawing.Point(3, 57);
//////      this.lbl_credit_title.Name = "lbl_credit_title";
//////      this.lbl_credit_title.Size = new System.Drawing.Size(167, 20);
//////      this.lbl_credit_title.TabIndex = 84;
//////      this.lbl_credit_title.Text = "xCortesias Cr�dito";
//////      this.lbl_credit_title.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
//////      // 
//////      // lbl_points_title
//////      // 
//////      this.lbl_points_title.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
//////      this.lbl_points_title.Location = new System.Drawing.Point(9, 16);
//////      this.lbl_points_title.Name = "lbl_points_title";
//////      this.lbl_points_title.Size = new System.Drawing.Size(161, 20);
//////      this.lbl_points_title.TabIndex = 83;
//////      this.lbl_points_title.Text = "xPoints";
//////      this.lbl_points_title.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
//////      // 
//////      // lbl_bar
//////      // 
//////      this.lbl_bar.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
//////      this.lbl_bar.Location = new System.Drawing.Point(173, 36);
//////      this.lbl_bar.Name = "lbl_bar";
//////      this.lbl_bar.Size = new System.Drawing.Size(126, 20);
//////      this.lbl_bar.TabIndex = 81;
//////      this.lbl_bar.Text = "0,00";
//////      this.lbl_bar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
//////      // 
//////      // lbl_bar_title
//////      // 
//////      this.lbl_bar_title.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
//////      this.lbl_bar_title.Location = new System.Drawing.Point(9, 37);
//////      this.lbl_bar_title.Name = "lbl_bar_title";
//////      this.lbl_bar_title.Size = new System.Drawing.Size(161, 20);
//////      this.lbl_bar_title.TabIndex = 82;
//////      this.lbl_bar_title.Text = "xCortesias Bar";
//////      this.lbl_bar_title.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
//////      // 
//////      // lbl_expiration
//////      // 
//////      this.lbl_expiration.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
//////      this.lbl_expiration.Location = new System.Drawing.Point(177, 91);
//////      this.lbl_expiration.Name = "lbl_expiration";
//////      this.lbl_expiration.Size = new System.Drawing.Size(123, 38);
//////      this.lbl_expiration.TabIndex = 87;
//////      this.lbl_expiration.Text = "99/99/9999\r\nFaltan XXX d�as";
//////      this.lbl_expiration.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
//////      // 
//////      // lbl_expiration_title
//////      // 
//////      this.lbl_expiration_title.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//////      this.lbl_expiration_title.Location = new System.Drawing.Point(46, 91);
//////      this.lbl_expiration_title.Name = "lbl_expiration_title";
//////      this.lbl_expiration_title.Size = new System.Drawing.Size(124, 20);
//////      this.lbl_expiration_title.TabIndex = 86;
//////      this.lbl_expiration_title.Text = "xCaducidad";
//////      this.lbl_expiration_title.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
//////      // 
//////      // lbl_points
//////      // 
//////      this.lbl_points.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
//////      this.lbl_points.Location = new System.Drawing.Point(173, 16);
//////      this.lbl_points.Name = "lbl_points";
//////      this.lbl_points.Size = new System.Drawing.Size(126, 20);
//////      this.lbl_points.TabIndex = 88;
//////      this.lbl_points.Text = "0,00";
//////      this.lbl_points.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
//////      // 
//////      // lbl_card_title
//////      // 
//////      this.lbl_card_title.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
//////      this.lbl_card_title.Location = new System.Drawing.Point(73, 42);
//////      this.lbl_card_title.Name = "lbl_card_title";
//////      this.lbl_card_title.Size = new System.Drawing.Size(97, 20);
//////      this.lbl_card_title.TabIndex = 89;
//////      this.lbl_card_title.Text = "xTarjeta";
//////      this.lbl_card_title.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
//////      // 
//////      // lbl_card_type
//////      // 
//////      this.lbl_card_type.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//////      this.lbl_card_type.Location = new System.Drawing.Point(176, 36);
//////      this.lbl_card_type.Name = "lbl_card_type";
//////      this.lbl_card_type.Size = new System.Drawing.Size(118, 26);
//////      this.lbl_card_type.TabIndex = 90;
//////      this.lbl_card_type.Text = "PLATINIUM";
//////      this.lbl_card_type.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
//////      // 
//////      // gb_points
//////      // 
//////      this.gb_points.Controls.Add(this.lbl_points);
//////      this.gb_points.Controls.Add(this.lbl_bar_title);
//////      this.gb_points.Controls.Add(this.lbl_bar);
//////      this.gb_points.Controls.Add(this.lbl_expiration);
//////      this.gb_points.Controls.Add(this.lbl_expiration_title);
//////      this.gb_points.Controls.Add(this.lbl_points_title);
//////      this.gb_points.Controls.Add(this.lbl_credit_title);
//////      this.gb_points.Controls.Add(this.lbl_credit);
//////      this.gb_points.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//////      this.gb_points.Location = new System.Drawing.Point(0, 68);
//////      this.gb_points.Name = "gb_points";
//////      this.gb_points.Size = new System.Drawing.Size(305, 134);
//////      this.gb_points.TabIndex = 91;
//////      this.gb_points.TabStop = false;
//////      this.gb_points.Text = "xPuntos";
//////      // 
//////      // uc_points_balance
//////      // 
//////      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
//////      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
//////      this.Controls.Add(this.gb_points);
//////      this.Controls.Add(this.lbl_card_type);
//////      this.Controls.Add(this.lbl_card_title);
//////      this.Controls.Add(this.pictureBox2);
//////      this.Name = "uc_points_balance";
//////      this.Size = new System.Drawing.Size(305, 332);
//////      ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
//////      this.gb_points.ResumeLayout(false);
//////      this.ResumeLayout(false);

//////    }

//////    #endregion

//////    private System.Windows.Forms.PictureBox pictureBox2;
//////    private System.Windows.Forms.Label lbl_credit;
//////    private System.Windows.Forms.Label lbl_credit_title;
//////    private System.Windows.Forms.Label lbl_points_title;
//////    private System.Windows.Forms.Label lbl_bar;
//////    private System.Windows.Forms.Label lbl_bar_title;
//////    private System.Windows.Forms.Label lbl_expiration;
//////    private System.Windows.Forms.Label lbl_expiration_title;
//////    private System.Windows.Forms.Label lbl_points;
//////    private System.Windows.Forms.Label lbl_card_title;
//////    private System.Windows.Forms.Label lbl_card_type;
//////    private System.Windows.Forms.GroupBox gb_points;

//////  }
//////}
