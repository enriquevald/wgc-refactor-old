//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : frm_check_info.cs
// 
//   DESCRIPTION : 
// 
// REVISION HISTORY :
// 
// Date         Author  Description
// -----------  ------  ----------------------------------------------------------
// 06-MAR-2017  FGB     Initial version
//------------------------------------------------------------------------------

using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using WSI.Cashier.Controls;
using WSI.Common;

namespace WSI.Cashier
{
  public partial class frm_bankcheck_info : frm_base
  {
    #region Constants
    const Int32 TAB_PAYMENT = 0;
    #endregion

    #region Attributes

    Decimal m_total_amount;
    Decimal m_max_amount_in_cash;

    Decimal? m_bankcheck_amount;
    String m_bankcheck_number;
    Decimal? m_cash_amount;

    uc_round_textbox m_editable_textbox;
    uc_label m_calculated_label;

    private static String m_decimal_str = System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencyDecimalSeparator;

    #endregion

    #region Constructor

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="TotalAmount"></param>
    /// <param name="MaxAmountInCash"></param>
    public frm_bankcheck_info(Decimal TotalAmount, Decimal MaxAmountInCash)
    {
      this.m_total_amount = TotalAmount;
      this.m_max_amount_in_cash = MaxAmountInCash;

      InitializeOuputValues();

      InitializeComponent();

      InitializeControlResources();

      Init();
    }

    public DialogResult Show(out Decimal? CheckAmount, out String CheckNumber)
    {
      Misc.WriteLog("[FORM LOAD] frm_bankcheck_info", Log.Type.Message);
      DialogResult _rc;

      _rc = this.ShowDialog();

      CheckAmount = m_bankcheck_amount;
      CheckNumber = m_bankcheck_number;

      return _rc;
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Initialize values of output
    /// </summary>
    private void InitializeOuputValues()
    {
      this.m_bankcheck_amount = null;
      this.m_bankcheck_number = String.Empty;
      this.m_cash_amount = null;
    }

    /// <summary>
    /// Initialize the controls
    /// </summary>
    private void InitializeControlResources()
    {
      // Title
      this.FormTitle = Resource.String("STR_FRM_BANKCHECK_INFO_TITLE");

      // Buttons
      btn_ok.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_OK");
      btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");

      // Labels
      lbl_bankcheck_amount.Text = Resource.String("STR_FRM_BANKCHECK_INFO_TITLE_BANKCHECK_AMOUNT");
      lbl_cashamount_caption.Text = Resource.String("STR_FRM_BANKCHECK_INFO_TITLE_CASH");
      lbl_totalcashable_caption.Text = Resource.String("STR_FRM_BANKCHECK_INFO_TITLE_TOTAL");
      lbl_bankcheck_number.Text = Resource.String("STR_FRM_BANKCHECK_INFO_TITLE_BANKCHECK_NUMBER");

      ef_bankcheck_amount.ReadOnly = false;
    }

    /// <summary>
    /// Init the properties of the class.
    /// </summary>
    private void Init()
    {
      // Center screen
      this.Location = new Point(1024 / 2 - this.Width / 2, 0);

      // Show keyboard if automatic OSK it's enabled
      WSIKeyboard.Toggle();

      ef_bankcheck_amount.Enabled = true;
      lbl_totalcashable_value.Enabled = true;
      lbl_cashamount_value.Enabled = true;

      m_editable_textbox = ef_bankcheck_amount;
      m_calculated_label = lbl_cashamount_value;

      m_editable_textbox.BackColor = System.Drawing.Color.LightGoldenrodYellow;
      m_calculated_label.Style = uc_label.LabelStyle.NONE;

      lbl_totalcashable_value.Text = ShowAmount(m_total_amount);
      ef_bankcheck_amount.Text = ShowAmountWithoutSymbols(m_total_amount);
      lbl_cashamount_value.Text = ShowAmount(Currency.Zero());
    }

    private String ShowAmount(Currency AmountToShow)
    {
      return AmountToShow.ToString();
    }

    private String ShowAmount(Decimal AmountToShow)
    {
      Currency _currency_to_show;
      _currency_to_show = (Currency)AmountToShow;

      return ShowAmount(_currency_to_show);
    }

    private String ShowAmountWithoutSymbols(Currency AmountToShow)
    {
      return AmountToShow.ToString(false);
    }

    private String ShowAmountWithoutSymbols(Decimal AmountToShow)
    {
      Currency _currency_to_show;
      _currency_to_show = (Currency)AmountToShow;

      return ShowAmountWithoutSymbols(_currency_to_show);
    }

    /// <summary>
    /// Check the entered data
    /// </summary>
    /// <returns>True if all the information in text controls is correct.</returns>
    private Boolean CheckData()
    {
      Currency _bankcheck_amount;
      Currency _cash_amount;
      String _bankcheck_number;

      lbl_msg_blink.Visible = false;
      lbl_msg_blink.Text = String.Empty;

      try
      {
        _bankcheck_amount = Format.ParseCurrency(this.ef_bankcheck_amount.Text);
        _cash_amount = (m_total_amount - _bankcheck_amount);

        // Validate that bank check amount is less or equal than the total amount
        if (_bankcheck_amount > m_total_amount)
        {
          lbl_msg_blink.Text = Resource.String("STR_FRM_BANKCHECK_INFO_ERROR_AMOUNT_MUST_BE_LESS_OR_EQUAL", lbl_bankcheck_amount.Text, m_total_amount.ToString());

          ef_bankcheck_amount.Focus();

          return false;
        }

        // Validate that bank check amount is not negative
        if (_bankcheck_amount < 0)
        {
          lbl_msg_blink.Text = Resource.String("STR_FRM_BANKCHECK_INFO_ERROR_CHECK_AMOUNT_NEGATIVE");

          ef_bankcheck_amount.Focus();

          return false;
        }

        // Validate that cash amount is not negative
        if (_cash_amount < 0)
        {
          lbl_msg_blink.Text = Resource.String("STR_FRM_BANKCHECK_INFO_ERROR_CASH_AMOUNT_NEGATIVE");

          ef_bankcheck_amount.Focus();

          return false;
        }

        // Validate that cash amount is not bigger than maximum cash amount allowed
        // if m_max_amount_in_cash = 0 then there is no cash limit
        if ((m_max_amount_in_cash > 0) && (_cash_amount > m_max_amount_in_cash))
        {
          lbl_msg_blink.Text = Resource.String("STR_FRM_BANKCHECK_INFO_ERROR_AMOUNT_MUST_BE_LESS_OR_EQUAL", lbl_cashamount_caption.Text, m_max_amount_in_cash.ToString());

          ef_bankcheck_amount.Focus();

          return false;
        }

        // Validate that cash pay is bigger than available cash in the caisher
        _bankcheck_number = ef_bankcheck_number.Text;
        if (String.IsNullOrEmpty(_bankcheck_number))
        {
          lbl_msg_blink.Text = Resource.String("STR_FRM_BANKCHECK_INFO_ERROR_CHECK_NUMBER_EMPTY");

          ef_bankcheck_number.Focus();

          return false;
        }

        m_bankcheck_amount = _bankcheck_amount;
        m_bankcheck_number = _bankcheck_number;
        m_cash_amount = _cash_amount;

        return true;
      }
      finally
      {
        if (!String.IsNullOrEmpty(lbl_msg_blink.Text))
        {
          lbl_msg_blink.Visible = true;
        }
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE :  Check and Add digit to amount
    // 
    //  PARAMS :
    //      - INPUT :
    //          - NumberStr : a number in string format
    //
    //      - OUTPUT :
    //
    // RETURNS:
    // 
    //   NOTES:
    private void AddNumber(String NumberStr)
    {
      String _aux_str;
      String _tentative_number;
      Currency _input_amount;
      Int32 _dec_symbol_pos;

      // Prevent exceeding maximum number of decimals (2)
      _dec_symbol_pos = this.m_editable_textbox.Text.IndexOf(m_decimal_str);
      if (_dec_symbol_pos > -1)
      {
        _aux_str = this.m_editable_textbox.Text.Substring(_dec_symbol_pos);
        if (_aux_str.Length > 2)
        {
          return;
        }
      }

      // Prevent exceeding maximum amount length
      if (this.m_editable_textbox.Text.Length >= Cashier.MAX_ALLOWED_AMOUNT_LENGTH)
      {
        return;
      }

      _tentative_number = this.m_editable_textbox.Text + NumberStr;

      try
      {
        _input_amount = Format.ParseCurrency(_tentative_number);
        if (_input_amount > Cashier.MAX_ALLOWED_AMOUNT)
        {
          return;
        }
      }
      catch
      {
        _input_amount = 0;

        return;
      }

      // Remove unneeded leading zeros
      // Unless we are entering a decimal value, there must not be any leading 0
      if (_dec_symbol_pos == -1)
      {
        this.m_editable_textbox.Text = this.m_editable_textbox.Text.TrimStart('0') + NumberStr;
      }
      else
      {
        // Accept new symbol/digit 
        this.m_editable_textbox.Text = this.m_editable_textbox.Text + NumberStr;
      }
    } // AddNumber

    #endregion

    #region Handlers
    private void btn_ok_Click(object sender, EventArgs e)
    {
      // Check out whether the data is right
      if (CheckData())
      {
        // hide keyboard and close
        WSIKeyboard.Hide();

        this.DialogResult = DialogResult.OK;

        this.Dispose();
      }
    }

    private void btn_cancel_Click(object sender, EventArgs e)
    {
      // hide keyboard and close
      WSIKeyboard.Hide();

      this.DialogResult = DialogResult.Cancel;

      this.Dispose();
    }

    private void btn_keyboard_Click(object sender, EventArgs e)
    {
      // show keyboard (if hidden)
      WSIKeyboard.ForceToggle();
    }

    private void btn_num_1_Click(object sender, EventArgs e)
    {
      AddNumber("1");
    }

    private void btn_num_2_Click(object sender, EventArgs e)
    {
      AddNumber("2");
    }

    private void btn_num_3_Click(object sender, EventArgs e)
    {
      AddNumber("3");
    }

    private void btn_num_4_Click(object sender, EventArgs e)
    {
      AddNumber("4");
    }

    private void btn_num_5_Click(object sender, EventArgs e)
    {
      AddNumber("5");
    }

    private void btn_num_6_Click(object sender, EventArgs e)
    {
      AddNumber("6");
    }

    private void btn_num_7_Click(object sender, EventArgs e)
    {
      AddNumber("7");
    }

    private void btn_num_8_Click(object sender, EventArgs e)
    {
      AddNumber("8");
    }

    private void btn_num_9_Click(object sender, EventArgs e)
    {
      AddNumber("9");
    }

    private void btn_num_10_Click(object sender, EventArgs e)
    {
      AddNumber("0");
    }

    private void btn_num_dot_Click(object sender, EventArgs e)
    {
      if (this.m_editable_textbox.Text.IndexOf(m_decimal_str) == -1)
      {
        this.m_editable_textbox.Text = this.m_editable_textbox.Text + m_decimal_str;
      }
    }

    private void btn_back_Click(object sender, EventArgs e)
    {
      if (this.m_editable_textbox.Text.Length > 0)
      {
        this.m_editable_textbox.Text = this.m_editable_textbox.Text.Substring(0, this.m_editable_textbox.Text.Length - 1);
        if (WSI.Common.GeneralParam.GetString("WigosGUI", "CurrencySymbol") == this.m_editable_textbox.Text || String.IsNullOrEmpty(this.m_editable_textbox.Text))
        {
          this.m_editable_textbox.Text = "0";
        }
      }
    }

    private void ef_TextChanged(object sender, EventArgs e)
    {
      Currency _calculated;
      Currency _edited;
      Currency _total;

      if (String.IsNullOrEmpty(m_editable_textbox.Text))
      {
        return;
      }

      _total = m_total_amount;

      _edited = Format.ParseCurrency(this.m_editable_textbox.Text);

      _calculated = _total - _edited;

      if (_calculated < 0)
      {
        this.m_calculated_label.Text = "---";

        return;
      }

      this.m_calculated_label.Text = _calculated.ToString();
    }

    private void ef_KeyPress(object sender, KeyPressEventArgs e)
    {
      String _aux_str;
      Char _c;

      _c = e.KeyChar;

      if (_c >= '0' && _c <= '9')
      {
        _aux_str = "" + _c;
        AddNumber(_aux_str);

        e.Handled = true;
      }

      if (_c == '\r')
      {
        btn_ok_Click(null, null);

        e.Handled = true;
      }

      if (_c == '\b')
      {
        btn_back_Click(null, null);

        e.Handled = true;
      }

      if (_c == '.')
      {
        btn_num_dot_Click(null, null);

        e.Handled = true;
      }

      this.m_editable_textbox.SelectionStart = this.m_editable_textbox.Text.Length;
      this.m_editable_textbox.SelectionLength = 0;

      e.Handled = true;
    }

    private void ef_bankcheck_amount_KeyPress(object sender, KeyPressEventArgs e)
    {
      ef_KeyPress(sender, e);
    }

    private void ef_bank_check_TextChanged(object sender, EventArgs e)
    {
      ef_TextChanged(sender, e);
    }

    private void ef_Enter(object sender, EventArgs e)
    {
      Currency _value;

      m_editable_textbox.BackColor = System.Drawing.Color.LightGoldenrodYellow;

      if (m_editable_textbox.Text == "---")
      {
        m_editable_textbox.Text = String.Empty;
      }
      else
      {
        _value = Format.ParseCurrency(this.m_editable_textbox.Text);
        m_editable_textbox.Text = _value.ToStringWithoutSymbols();
      }

      _value = Format.ParseCurrency(this.m_calculated_label.Text);
      m_calculated_label.Text = _value.ToString();
    }

    #endregion
  }

}