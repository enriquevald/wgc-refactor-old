namespace WSI.Cashier
{
  partial class frm_points_transfer
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.splitContainer1 = new System.Windows.Forms.SplitContainer();
      this.lbl_amount_input_title = new System.Windows.Forms.Label();
      this.gp_destiny = new System.Windows.Forms.GroupBox();
      this.uc_card_reader1 = new WSI.Cashier.uc_card_reader();
      this.gp_voucher_box = new System.Windows.Forms.GroupBox();
      this.web_browser = new System.Windows.Forms.WebBrowser();
      this.btn_ok = new System.Windows.Forms.Button();
      this.btn_cancel = new System.Windows.Forms.Button();
      this.gp_digits_box = new System.Windows.Forms.GroupBox();
      this.lbl_available = new System.Windows.Forms.Label();
      this.lbl_available_title = new System.Windows.Forms.Label();
      this.txt_amount = new System.Windows.Forms.TextBox();
      this.btn_num_1 = new System.Windows.Forms.Button();
      this.btn_back = new System.Windows.Forms.Button();
      this.btn_num_2 = new System.Windows.Forms.Button();
      this.btn_intro = new System.Windows.Forms.Button();
      this.btn_num_3 = new System.Windows.Forms.Button();
      this.btn_num_4 = new System.Windows.Forms.Button();
      this.btn_num_10 = new System.Windows.Forms.Button();
      this.btn_num_5 = new System.Windows.Forms.Button();
      this.btn_num_dot = new System.Windows.Forms.Button();
      this.btn_num_6 = new System.Windows.Forms.Button();
      this.btn_num_9 = new System.Windows.Forms.Button();
      this.btn_num_7 = new System.Windows.Forms.Button();
      this.btn_num_8 = new System.Windows.Forms.Button();
      this.splitContainer1.Panel1.SuspendLayout();
      this.splitContainer1.Panel2.SuspendLayout();
      this.splitContainer1.SuspendLayout();
      this.gp_destiny.SuspendLayout();
      this.gp_voucher_box.SuspendLayout();
      this.gp_digits_box.SuspendLayout();
      this.SuspendLayout();
      // 
      // splitContainer1
      // 
      this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
      this.splitContainer1.IsSplitterFixed = true;
      this.splitContainer1.Location = new System.Drawing.Point(0, 0);
      this.splitContainer1.Name = "splitContainer1";
      this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
      // 
      // splitContainer1.Panel1
      // 
      this.splitContainer1.Panel1.BackColor = System.Drawing.Color.CornflowerBlue;
      this.splitContainer1.Panel1.Controls.Add(this.lbl_amount_input_title);
      // 
      // splitContainer1.Panel2
      // 
      this.splitContainer1.Panel2.Controls.Add(this.gp_destiny);
      this.splitContainer1.Panel2.Controls.Add(this.gp_voucher_box);
      this.splitContainer1.Panel2.Controls.Add(this.btn_ok);
      this.splitContainer1.Panel2.Controls.Add(this.btn_cancel);
      this.splitContainer1.Panel2.Controls.Add(this.gp_digits_box);
      this.splitContainer1.Size = new System.Drawing.Size(705, 632);
      this.splitContainer1.SplitterDistance = 25;
      this.splitContainer1.TabIndex = 0;
      this.splitContainer1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.splitContainer1_KeyPress);
      // 
      // lbl_amount_input_title
      // 
      this.lbl_amount_input_title.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_amount_input_title.ForeColor = System.Drawing.Color.White;
      this.lbl_amount_input_title.Location = new System.Drawing.Point(3, 5);
      this.lbl_amount_input_title.Name = "lbl_amount_input_title";
      this.lbl_amount_input_title.Size = new System.Drawing.Size(324, 16);
      this.lbl_amount_input_title.TabIndex = 1;
      this.lbl_amount_input_title.Text = "xNone";
      // 
      // gp_destiny
      // 
      this.gp_destiny.Controls.Add(this.uc_card_reader1);
      this.gp_destiny.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
      this.gp_destiny.Location = new System.Drawing.Point(7, 5);
      this.gp_destiny.Name = "gp_destiny";
      this.gp_destiny.Size = new System.Drawing.Size(352, 150);
      this.gp_destiny.TabIndex = 106;
      this.gp_destiny.TabStop = false;
      this.gp_destiny.Text = "groupBox1";
      // 
      // uc_card_reader1
      // 
      this.uc_card_reader1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.uc_card_reader1.Location = new System.Drawing.Point(8, 25);
      this.uc_card_reader1.Name = "uc_card_reader1";
      this.uc_card_reader1.Size = new System.Drawing.Size(338, 119);
      this.uc_card_reader1.TabIndex = 89;
      // 
      // gp_voucher_box
      // 
      this.gp_voucher_box.Controls.Add(this.web_browser);
      this.gp_voucher_box.Enabled = false;
      this.gp_voucher_box.Location = new System.Drawing.Point(365, 5);
      this.gp_voucher_box.Name = "gp_voucher_box";
      this.gp_voucher_box.Size = new System.Drawing.Size(332, 536);
      this.gp_voucher_box.TabIndex = 19;
      this.gp_voucher_box.TabStop = false;
      // 
      // web_browser
      // 
      this.web_browser.Location = new System.Drawing.Point(6, 11);
      this.web_browser.MinimumSize = new System.Drawing.Size(20, 20);
      this.web_browser.Name = "web_browser";
      this.web_browser.Size = new System.Drawing.Size(320, 520);
      this.web_browser.TabIndex = 16;
      // 
      // btn_ok
      // 
      this.btn_ok.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.btn_ok.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btn_ok.Location = new System.Drawing.Point(395, 549);
      this.btn_ok.Name = "btn_ok";
      this.btn_ok.Size = new System.Drawing.Size(112, 48);
      this.btn_ok.TabIndex = 18;
      this.btn_ok.Text = "Ok";
      this.btn_ok.UseVisualStyleBackColor = false;
      this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
      this.btn_ok.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_points_transfer_KeyPress);
      // 
      // btn_cancel
      // 
      this.btn_cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.btn_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btn_cancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btn_cancel.Location = new System.Drawing.Point(549, 549);
      this.btn_cancel.Name = "btn_cancel";
      this.btn_cancel.Size = new System.Drawing.Size(112, 48);
      this.btn_cancel.TabIndex = 17;
      this.btn_cancel.Text = "Cancel";
      this.btn_cancel.UseVisualStyleBackColor = false;
      this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
      this.btn_cancel.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_points_transfer_KeyPress);
      // 
      // gp_digits_box
      // 
      this.gp_digits_box.Controls.Add(this.lbl_available);
      this.gp_digits_box.Controls.Add(this.lbl_available_title);
      this.gp_digits_box.Controls.Add(this.txt_amount);
      this.gp_digits_box.Controls.Add(this.btn_num_1);
      this.gp_digits_box.Controls.Add(this.btn_back);
      this.gp_digits_box.Controls.Add(this.btn_num_2);
      this.gp_digits_box.Controls.Add(this.btn_intro);
      this.gp_digits_box.Controls.Add(this.btn_num_3);
      this.gp_digits_box.Controls.Add(this.btn_num_4);
      this.gp_digits_box.Controls.Add(this.btn_num_10);
      this.gp_digits_box.Controls.Add(this.btn_num_5);
      this.gp_digits_box.Controls.Add(this.btn_num_dot);
      this.gp_digits_box.Controls.Add(this.btn_num_6);
      this.gp_digits_box.Controls.Add(this.btn_num_9);
      this.gp_digits_box.Controls.Add(this.btn_num_7);
      this.gp_digits_box.Controls.Add(this.btn_num_8);
      this.gp_digits_box.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.gp_digits_box.ForeColor = System.Drawing.Color.Black;
      this.gp_digits_box.Location = new System.Drawing.Point(7, 161);
      this.gp_digits_box.Name = "gp_digits_box";
      this.gp_digits_box.Size = new System.Drawing.Size(352, 429);
      this.gp_digits_box.TabIndex = 15;
      this.gp_digits_box.TabStop = false;
      this.gp_digits_box.Text = "xInput Amount";
      // 
      // lbl_available
      // 
      this.lbl_available.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_available.ForeColor = System.Drawing.Color.Black;
      this.lbl_available.Location = new System.Drawing.Point(257, 38);
      this.lbl_available.Name = "lbl_available";
      this.lbl_available.Size = new System.Drawing.Size(89, 30);
      this.lbl_available.TabIndex = 107;
      this.lbl_available.Text = "12.999.999";
      this.lbl_available.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_available_title
      // 
      this.lbl_available_title.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_available_title.Location = new System.Drawing.Point(8, 38);
      this.lbl_available_title.Name = "lbl_available_title";
      this.lbl_available_title.Size = new System.Drawing.Size(243, 30);
      this.lbl_available_title.TabIndex = 106;
      this.lbl_available_title.Text = "xAvailable";
      this.lbl_available_title.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // txt_amount
      // 
      this.txt_amount.BackColor = System.Drawing.Color.LightGoldenrodYellow;
      this.txt_amount.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.txt_amount.Location = new System.Drawing.Point(29, 85);
      this.txt_amount.MaxLength = 18;
      this.txt_amount.Name = "txt_amount";
      this.txt_amount.ReadOnly = true;
      this.txt_amount.Size = new System.Drawing.Size(291, 44);
      this.txt_amount.TabIndex = 14;
      this.txt_amount.TabStop = false;
      this.txt_amount.Text = "123";
      this.txt_amount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      this.txt_amount.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_points_transfer_PreviewKeyDown);
      this.txt_amount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_points_transfer_KeyPress);
      // 
      // btn_num_1
      // 
      this.btn_num_1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btn_num_1.Location = new System.Drawing.Point(6, 148);
      this.btn_num_1.Name = "btn_num_1";
      this.btn_num_1.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_1.Size = new System.Drawing.Size(64, 64);
      this.btn_num_1.TabIndex = 0;
      this.btn_num_1.TabStop = false;
      this.btn_num_1.Text = "1";
      this.btn_num_1.UseVisualStyleBackColor = false;
      this.btn_num_1.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_points_transfer_PreviewKeyDown);
      this.btn_num_1.Click += new System.EventHandler(this.btn_num_1_Click);
      this.btn_num_1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_points_transfer_KeyPress);
      // 
      // btn_back
      // 
      this.btn_back.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_back.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btn_back.Location = new System.Drawing.Point(216, 148);
      this.btn_back.Name = "btn_back";
      this.btn_back.Size = new System.Drawing.Size(128, 64);
      this.btn_back.TabIndex = 13;
      this.btn_back.TabStop = false;
      this.btn_back.Text = "�--";
      this.btn_back.UseVisualStyleBackColor = false;
      this.btn_back.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_points_transfer_PreviewKeyDown);
      this.btn_back.Click += new System.EventHandler(this.btn_back_Click);
      this.btn_back.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_points_transfer_KeyPress);
      // 
      // btn_num_2
      // 
      this.btn_num_2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btn_num_2.Location = new System.Drawing.Point(76, 148);
      this.btn_num_2.Name = "btn_num_2";
      this.btn_num_2.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_2.Size = new System.Drawing.Size(64, 64);
      this.btn_num_2.TabIndex = 1;
      this.btn_num_2.TabStop = false;
      this.btn_num_2.Text = "2";
      this.btn_num_2.UseVisualStyleBackColor = false;
      this.btn_num_2.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_points_transfer_PreviewKeyDown);
      this.btn_num_2.Click += new System.EventHandler(this.btn_num_2_Click);
      this.btn_num_2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_points_transfer_KeyPress);
      // 
      // btn_intro
      // 
      this.btn_intro.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_intro.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btn_intro.Location = new System.Drawing.Point(216, 288);
      this.btn_intro.Name = "btn_intro";
      this.btn_intro.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_intro.Size = new System.Drawing.Size(128, 134);
      this.btn_intro.TabIndex = 12;
      this.btn_intro.Text = "xEnter";
      this.btn_intro.UseVisualStyleBackColor = false;
      this.btn_intro.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_points_transfer_PreviewKeyDown);
      this.btn_intro.Click += new System.EventHandler(this.btn_intro_Click);
      this.btn_intro.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_points_transfer_KeyPress);
      // 
      // btn_num_3
      // 
      this.btn_num_3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_3.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btn_num_3.Location = new System.Drawing.Point(146, 148);
      this.btn_num_3.Name = "btn_num_3";
      this.btn_num_3.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_3.Size = new System.Drawing.Size(64, 64);
      this.btn_num_3.TabIndex = 2;
      this.btn_num_3.TabStop = false;
      this.btn_num_3.Text = "3";
      this.btn_num_3.UseVisualStyleBackColor = false;
      this.btn_num_3.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_points_transfer_PreviewKeyDown);
      this.btn_num_3.Click += new System.EventHandler(this.btn_num_3_Click);
      this.btn_num_3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_points_transfer_KeyPress);
      // 
      // btn_num_4
      // 
      this.btn_num_4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_4.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btn_num_4.Location = new System.Drawing.Point(6, 218);
      this.btn_num_4.Name = "btn_num_4";
      this.btn_num_4.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_4.Size = new System.Drawing.Size(64, 64);
      this.btn_num_4.TabIndex = 3;
      this.btn_num_4.TabStop = false;
      this.btn_num_4.Text = "4";
      this.btn_num_4.UseVisualStyleBackColor = false;
      this.btn_num_4.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_points_transfer_PreviewKeyDown);
      this.btn_num_4.Click += new System.EventHandler(this.btn_num_4_Click);
      this.btn_num_4.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_points_transfer_KeyPress);
      // 
      // btn_num_10
      // 
      this.btn_num_10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_10.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btn_num_10.Location = new System.Drawing.Point(6, 358);
      this.btn_num_10.Name = "btn_num_10";
      this.btn_num_10.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_10.Size = new System.Drawing.Size(134, 64);
      this.btn_num_10.TabIndex = 10;
      this.btn_num_10.TabStop = false;
      this.btn_num_10.Text = "0";
      this.btn_num_10.UseVisualStyleBackColor = false;
      this.btn_num_10.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_points_transfer_PreviewKeyDown);
      this.btn_num_10.Click += new System.EventHandler(this.btn_num_10_Click);
      this.btn_num_10.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_points_transfer_KeyPress);
      // 
      // btn_num_5
      // 
      this.btn_num_5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_5.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btn_num_5.Location = new System.Drawing.Point(76, 218);
      this.btn_num_5.Name = "btn_num_5";
      this.btn_num_5.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_5.Size = new System.Drawing.Size(64, 64);
      this.btn_num_5.TabIndex = 4;
      this.btn_num_5.TabStop = false;
      this.btn_num_5.Text = "5";
      this.btn_num_5.UseVisualStyleBackColor = false;
      this.btn_num_5.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_points_transfer_PreviewKeyDown);
      this.btn_num_5.Click += new System.EventHandler(this.btn_num_5_Click);
      this.btn_num_5.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_points_transfer_KeyPress);
      // 
      // btn_num_dot
      // 
      this.btn_num_dot.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_dot.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btn_num_dot.Location = new System.Drawing.Point(146, 358);
      this.btn_num_dot.Name = "btn_num_dot";
      this.btn_num_dot.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_dot.Size = new System.Drawing.Size(64, 64);
      this.btn_num_dot.TabIndex = 9;
      this.btn_num_dot.TabStop = false;
      this.btn_num_dot.Text = "�";
      this.btn_num_dot.UseVisualStyleBackColor = false;
      this.btn_num_dot.Visible = false;
      this.btn_num_dot.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_points_transfer_PreviewKeyDown);
      this.btn_num_dot.Click += new System.EventHandler(this.btn_num_dot_Click);
      this.btn_num_dot.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_points_transfer_KeyPress);
      // 
      // btn_num_6
      // 
      this.btn_num_6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_6.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btn_num_6.Location = new System.Drawing.Point(146, 218);
      this.btn_num_6.Name = "btn_num_6";
      this.btn_num_6.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_6.Size = new System.Drawing.Size(64, 64);
      this.btn_num_6.TabIndex = 5;
      this.btn_num_6.TabStop = false;
      this.btn_num_6.Text = "6";
      this.btn_num_6.UseVisualStyleBackColor = false;
      this.btn_num_6.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_points_transfer_PreviewKeyDown);
      this.btn_num_6.Click += new System.EventHandler(this.btn_num_6_Click);
      this.btn_num_6.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_points_transfer_KeyPress);
      // 
      // btn_num_9
      // 
      this.btn_num_9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_9.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btn_num_9.Location = new System.Drawing.Point(146, 288);
      this.btn_num_9.Name = "btn_num_9";
      this.btn_num_9.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_9.Size = new System.Drawing.Size(64, 64);
      this.btn_num_9.TabIndex = 8;
      this.btn_num_9.TabStop = false;
      this.btn_num_9.Text = "9";
      this.btn_num_9.UseVisualStyleBackColor = false;
      this.btn_num_9.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_points_transfer_PreviewKeyDown);
      this.btn_num_9.Click += new System.EventHandler(this.btn_num_9_Click);
      this.btn_num_9.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_points_transfer_KeyPress);
      // 
      // btn_num_7
      // 
      this.btn_num_7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_7.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btn_num_7.Location = new System.Drawing.Point(6, 288);
      this.btn_num_7.Name = "btn_num_7";
      this.btn_num_7.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_7.Size = new System.Drawing.Size(64, 64);
      this.btn_num_7.TabIndex = 6;
      this.btn_num_7.TabStop = false;
      this.btn_num_7.Text = "7";
      this.btn_num_7.UseVisualStyleBackColor = false;
      this.btn_num_7.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_points_transfer_PreviewKeyDown);
      this.btn_num_7.Click += new System.EventHandler(this.btn_num_7_Click);
      this.btn_num_7.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_points_transfer_KeyPress);
      // 
      // btn_num_8
      // 
      this.btn_num_8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_8.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btn_num_8.Location = new System.Drawing.Point(76, 288);
      this.btn_num_8.Name = "btn_num_8";
      this.btn_num_8.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_8.Size = new System.Drawing.Size(64, 64);
      this.btn_num_8.TabIndex = 7;
      this.btn_num_8.TabStop = false;
      this.btn_num_8.Text = "8";
      this.btn_num_8.UseVisualStyleBackColor = false;
      this.btn_num_8.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_points_transfer_PreviewKeyDown);
      this.btn_num_8.Click += new System.EventHandler(this.btn_num_8_Click);
      this.btn_num_8.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_points_transfer_KeyPress);
      // 
      // frm_points_transfer
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.btn_cancel;
      this.ClientSize = new System.Drawing.Size(705, 632);
      this.Controls.Add(this.splitContainer1);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "frm_points_transfer";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Points Transfer";
      this.Shown += new System.EventHandler(this.frm_points_transfer_Shown);
      this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_points_transfer_KeyPress);
      this.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_points_transfer_PreviewKeyDown);
      this.splitContainer1.Panel1.ResumeLayout(false);
      this.splitContainer1.Panel2.ResumeLayout(false);
      this.splitContainer1.ResumeLayout(false);
      this.gp_destiny.ResumeLayout(false);
      this.gp_voucher_box.ResumeLayout(false);
      this.gp_digits_box.ResumeLayout(false);
      this.gp_digits_box.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.SplitContainer splitContainer1;
    private System.Windows.Forms.Button btn_num_1;
    private System.Windows.Forms.Button btn_num_10;
    private System.Windows.Forms.Button btn_num_dot;
    private System.Windows.Forms.Button btn_num_9;
    private System.Windows.Forms.Button btn_num_8;
    private System.Windows.Forms.Button btn_num_7;
    private System.Windows.Forms.Button btn_num_6;
    private System.Windows.Forms.Button btn_num_5;
    private System.Windows.Forms.Button btn_num_4;
    private System.Windows.Forms.Button btn_num_3;
    private System.Windows.Forms.Button btn_num_2;
    private System.Windows.Forms.Label lbl_amount_input_title;
    private System.Windows.Forms.Button btn_back;
    private System.Windows.Forms.Button btn_intro;
    private System.Windows.Forms.TextBox txt_amount;
    private System.Windows.Forms.GroupBox gp_digits_box;
    public System.Windows.Forms.WebBrowser web_browser;
    private System.Windows.Forms.Button btn_cancel;
    private System.Windows.Forms.Button btn_ok;
    private System.Windows.Forms.GroupBox gp_voucher_box;
    private uc_card_reader uc_card_reader1;
    private System.Windows.Forms.GroupBox gp_destiny;
    private System.Windows.Forms.Label lbl_available;
    private System.Windows.Forms.Label lbl_available_title;
  }
}