//------------------------------------------------------------------------------
// Copyright � 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_cash_advance_undo.cs
// 
//   DESCRIPTION: Implements the following classes:
//                1. frm_cash_advance_undo
//
//        AUTHOR: FAV
// 
// CREATION DATE: 09-MAY-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 27-OCT-2010 FAV     First release.
// 01-JUN-2016 FAV     Fixed Bug 14036: The "Undo Cash Advance" Form doesn't show the confirmation popup
// 21-SEP-2016 ETP     Fixed Bug 17846: Reported error messages are incorrect.
// 05-OCT-2017 RAB     PBI 30021:WIGOS-4048 Payment threshold authorisation - Recharge authorisation
// 09-OCT-2017 JML     PBI 30022:WIGOS-4052 Payment threshold registration - Recharge with Check payment
//------------------------------------------------------------------------------
using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using WSI.Common;
using WSI.Cashier.Controls;
using System.Collections.Generic;
using System.Drawing;
using System.Collections;

namespace WSI.Cashier
{
  public partial class frm_cash_advance_undo : frm_base
  {
    #region Attributes
    private const Int32 GRID_MAX_ROWS = 15;
    private const Int32 GRID_MAX_COLUMNS = 9;

    private const Int32 GRID_COLUMN_CM_MOVEMENT_ID = 0;
    private const Int32 GRID_COLUMN_CM_OPERATION_ID = 1;
    private const Int32 GRID_COLUMN_CM_DATETIME = 2;
    private const Int32 GRID_COLUMN_CM_TYPE = 3;
    private const Int32 GRID_COLUMN_CM_DESCRIPION = 4;
    private const Int32 GRID_COLUMN_CM_AMOUNT = 5;
    private const Int32 GRID_COLUMN_CM_COMISSION = 6;
    private const Int32 GRID_COLUMN_CM_AMOUNT_PAID = 7;
    private const Int32 GRID_COLUMN_CM_CASHIER_NAME = 8;

    private frm_yesno form_yes_no = new frm_yesno();
    private CashAdvanceItem m_selected_item;
    private CardData m_card_data;
    private List<CashAdvanceItem> cash_advance_list;
    private Form m_parent_form;
    #endregion

    #region Internal Class

    private class CashAdvanceItem
    {
      public Int64 MovementId { get; set; }
      public Int64 OperationId { get; set; }
      public DateTime Date { get; set; }
      public CASHIER_MOVEMENT Type { get; set; }
      public String Description { get; set; }
      public String Amount { get; set; }
      public String Comission { get; set; }
      public String Paid { get; set; }
      public String CashierName { get; set; }
    }

    #endregion

    #region Constructor

    public frm_cash_advance_undo()
    {
      InitializeComponent();

      InitializeControlResources();
      
      form_yes_no.Opacity = 0.6;

      cash_advance_list = new List<CashAdvanceItem>();

    }

    #endregion

    #region Private Methods

    private Boolean UndoCashAdvance()
    {
      CashierSessionInfo _cashier_session_info;
      String _str_message;
      String _str_error;
      String _caption;
      String _str_amount;
      DialogResult _dlg_rc;
      ArrayList _voucher_list;

      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.UndoLastCashAdvance,
                                                ProfilePermissions.TypeOperation.RequestPasswd, m_parent_form, out _str_error))
      {
        //Log.Warning(" The user doesn't have permissions to do an undo to a cash advance");
        return false;
      }

      _cashier_session_info = Cashier.CashierSessionInfo();

      _str_message = "";

      _str_amount = m_selected_item.Paid;

      // Check if type is not Currency
      switch (m_selected_item.Type)
      {
        case CASHIER_MOVEMENT.CASH_ADVANCE_CREDIT_BANK_CARD:
        case CASHIER_MOVEMENT.CASH_ADVANCE_DEBIT_BANK_CARD:
        case CASHIER_MOVEMENT.CASH_ADVANCE_GENERIC_BANK_CARD:
          _str_amount += " (" + Resource.String("STR_FRM_AMOUNT_INPUT_CREDIT_CARD_PAYMENT") + ")";
          break;

        case CASHIER_MOVEMENT.CASH_ADVANCE_CHECK:
          _str_amount += " (" + Resource.String("STR_FRM_AMOUNT_INPUT_CHECK_PAYMENT") + ")";
          break;

        default:
          break;
      }

      _caption = Resource.String("STR_REFUND_LAST_OPERATION_CASH_ADVANCE");
      _str_message = Resource.String("STR_REFUND_CASH_ADVANCE", _str_amount);

      // Show information message
      _str_message = _str_message.Replace("\\r\\n", "\r\n");
      _dlg_rc = frm_message.Show(_str_message,
                                 _caption,
                                 MessageBoxButtons.YesNo, Images.CashierImage.Question, Cashier.MainForm);

      if (_dlg_rc != DialogResult.OK)
      {
        return false;
      }

      OperationUndo.InputUndoOperation _params = new OperationUndo.InputUndoOperation();
      _params.CodeOperation = OperationCode.CASH_ADVANCE;
      _params.CardData = m_card_data;
      _params.OperationIdForUndo = m_selected_item.OperationId;
      _params.CashierSessionInfo = _cashier_session_info;
      _params.UseTodayOpening = true;

      OperationUndo.UndoError _error;

      if (!OperationUndo.UndoOperation(_params, out _voucher_list, out _error))
      {        
        frm_message.Show(OperationUndo.GetErrorMessage(_error), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, m_parent_form);

        return false;
      }

      // Print vouchers
      VoucherPrint.Print(_voucher_list);
      return true;
    }

    private void InitializeControlResources()
    {
      this.FormTitle = Resource.String("STR_FRM_PLAYER_SEARCH_RESULTS_001");
      btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");
      btn_undo.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_REFUND");
    }

    private void SetFormatDataGrid()
    {
      String _date_format;

      dataGridView1.MultiSelect = false;
      dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;

      _date_format = System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern;

      // Set row height for bigger font sizes
      dataGridView1.ColumnHeadersHeight = 30;
      dataGridView1.RowTemplate.Height = 30;
      dataGridView1.RowTemplate.DividerHeight = 0;

      // Columns
      dataGridView1.Columns[GRID_COLUMN_CM_MOVEMENT_ID].Width = 0;
      dataGridView1.Columns[GRID_COLUMN_CM_MOVEMENT_ID].Visible = false;

      dataGridView1.Columns[GRID_COLUMN_CM_OPERATION_ID].Width = 0;
      dataGridView1.Columns[GRID_COLUMN_CM_OPERATION_ID].Visible = false;
      
      dataGridView1.Columns[GRID_COLUMN_CM_DATETIME].Width = 0;
      dataGridView1.Columns[GRID_COLUMN_CM_DATETIME].Visible = false;
      dataGridView1.Columns[GRID_COLUMN_CM_DATETIME].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
      dataGridView1.Columns[GRID_COLUMN_CM_DATETIME].DefaultCellStyle.Format = _date_format;

      dataGridView1.Columns[GRID_COLUMN_CM_TYPE].Width = 0;
      dataGridView1.Columns[GRID_COLUMN_CM_TYPE].Visible = false;

      dataGridView1.Columns[GRID_COLUMN_CM_DESCRIPION].Width = 260;
      dataGridView1.Columns[GRID_COLUMN_CM_DESCRIPION].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

      dataGridView1.Columns[GRID_COLUMN_CM_AMOUNT].Width = 145;
      dataGridView1.Columns[GRID_COLUMN_CM_AMOUNT].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

      dataGridView1.Columns[GRID_COLUMN_CM_COMISSION].Width = 130;
      dataGridView1.Columns[GRID_COLUMN_CM_COMISSION].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

      dataGridView1.Columns[GRID_COLUMN_CM_AMOUNT_PAID].Width = 145;
      dataGridView1.Columns[GRID_COLUMN_CM_AMOUNT_PAID].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

      dataGridView1.Columns[GRID_COLUMN_CM_CASHIER_NAME].Width = 180;
      dataGridView1.Columns[GRID_COLUMN_CM_CASHIER_NAME].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

      // Datagrid Headers
      dataGridView1.Columns[GRID_COLUMN_CM_DESCRIPION].HeaderCell.Value = Resource.String("STR_VOUCHER_CAGE_NEW_OPERATION_MOVEMENT_TYPE");
      dataGridView1.Columns[GRID_COLUMN_CM_AMOUNT].HeaderCell.Value = Resource.String("STR_UC_TICKET_QUANTITY");
      dataGridView1.Columns[GRID_COLUMN_CM_COMISSION].HeaderCell.Value = Resource.String("STR_FRM_LAST_MOVEMENTS_COMMISSION");
      dataGridView1.Columns[GRID_COLUMN_CM_AMOUNT_PAID].HeaderCell.Value = Resource.String("STR_FRM_HANDPAYS_035");
      dataGridView1.Columns[GRID_COLUMN_CM_CASHIER_NAME].HeaderCell.Value = Resource.String("STR_VOUCHER_TITO_TICKETS_CASHIER_TITLE");

      // Avoid selection from an empty grid
      if (dataGridView1.RowCount <= 0)
      {
        btn_undo.Enabled = false;
      }
      else
      {
        btn_undo.Enabled = true;
      }

      form_yes_no.Show();

      // Set focus to the DataGridView just after the form_yes_no.Show().
      dataGridView1.Focus();
      this.ShowDialog();

      form_yes_no.Hide();
    }

    private String GetDescriptionByType(CASHIER_MOVEMENT Type)
    {
      String _str_value = String.Empty;

      switch (Type)
      {
        case CASHIER_MOVEMENT.CASH_ADVANCE_GENERIC_BANK_CARD:
          _str_value = Resource.String("STR_FRM_LAST_MOVEMENTS_096");
          break;
        case CASHIER_MOVEMENT.CASH_ADVANCE_CREDIT_BANK_CARD:
          _str_value = Resource.String("STR_FRM_LAST_MOVEMENTS_097");
          break;
        case CASHIER_MOVEMENT.CASH_ADVANCE_DEBIT_BANK_CARD:
          _str_value = Resource.String("STR_FRM_LAST_MOVEMENTS_098");
          break;
        case CASHIER_MOVEMENT.CASH_ADVANCE_CHECK:
          _str_value = Resource.String("STR_FRM_LAST_MOVEMENTS_099");
          break;
      }

      return _str_value;
    }

    #endregion

    #region Public Methods

    public Int64 Show(CardData CardData, List<CashierMovementsInfo> CashierMovementsInfoList, Form ParentForm)
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_cash_advance_undo", Log.Type.Message);

      try
      {
        m_parent_form = ParentForm;

        cash_advance_list.Clear();

        this.FormTitle = Resource.String("STR_REFUND_CARD_CHECK") + " - " + CardData.PlayerTracking.HolderName;
        m_card_data = CardData;

        foreach (CashierMovementsInfo _cahier_movement in CashierMovementsInfoList)
        { 
          CashAdvanceItem _item = new CashAdvanceItem()
          {
            MovementId = _cahier_movement.MovementId,
            OperationId = (Int64)_cahier_movement.OperationId,
            Date = _cahier_movement.Date,
            Type = _cahier_movement.Type,
            Description = GetDescriptionByType(_cahier_movement.Type),
            Amount = Currency.Format(_cahier_movement.FinalBalance, _cahier_movement.CurrencyIsoCode),
            Comission =  Currency.Format(_cahier_movement.SubAmount, _cahier_movement.CurrencyIsoCode),
            Paid = Currency.Format(_cahier_movement.AddAmount, _cahier_movement.CurrencyIsoCode),
            CashierName = _cahier_movement.CashierName
          };

          cash_advance_list.Add(_item);
        }

        dataGridView1.DataSource = cash_advance_list;
        this.SetFormatDataGrid();
        return 0;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      return -1;
    }

    new public void Dispose()
    {
      form_yes_no.Dispose();
      form_yes_no = null;

      base.Dispose();
    }

    #endregion Public methods

    #region Events
    private void dataGridView1_SelectionChanged(object sender, EventArgs e)
    {
      CashAdvanceItem _item = null;

      try
      {
        if (dataGridView1.SelectedRows.Count > 0)
        {
          int _index = dataGridView1.SelectedRows[0].Index;
          int _movement_id;

          if (dataGridView1.Rows[_index].Cells[GRID_COLUMN_CM_MOVEMENT_ID].Value == null)
            return;

          _movement_id = int.Parse(dataGridView1.Rows[_index].Cells[GRID_COLUMN_CM_MOVEMENT_ID].Value.ToString());
          _item = cash_advance_list.Find(delegate(CashAdvanceItem x) { return x.MovementId == _movement_id; });

          if (_item != null)
          {
            m_selected_item = _item;
          }
        }
        else
        {
          m_selected_item = null;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    }

    private void btn_cancel_Click(object sender, EventArgs e)
    {
      m_selected_item = null;

      Misc.WriteLog("[FORM CLOSE] frm_cash_advance_undo (cancel)", Log.Type.Message);
      this.Close();
    }

    private void btn_undo_Click(object sender, EventArgs e)
    {
      try
      {
        if (UndoCashAdvance())
        {

          Misc.WriteLog("[FORM CLOSE] frm_cash_advance_undo (undo - undocashadvance)", Log.Type.Message);
          this.Close();
        }
        else
        {
          this.Focus();
        }


        Misc.WriteLog("[FORM CLOSE] frm_cash_advance_undo (undo)", Log.Type.Message);
        this.Close();
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
    }

    #endregion

  }
}
