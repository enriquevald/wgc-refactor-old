﻿//------------------------------------------------------------------------------
// Copyright © 2015-2020 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_label.cs
// 
//   DESCRIPTION: Implements the uc_label user control
//                Class: uc_label
//
//        AUTHOR: José Martínez
//                David Hernández
// 
// CREATION DATE: 20-OCT-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 20-OCT-2015 JML     First release.
// 20-OCT-2015 DHA     First release.
// 14-MAR-2016 FOS     Fixed Bug 9357: New design corrections
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Xml.Serialization;
using System.Drawing.Drawing2D;
using System.ComponentModel;

namespace WSI.Cashier.Controls
{
  public class uc_label : Label
  {
    public enum LabelStyle
    {
      // ----------------------------------------------
      // Head bar
      // RMK: Labels for head screen
      // ----------------------------------------------
      NONE = 0,
      USER_NAME = 1,
      ROL = 2,
      STATUS_CONNECTED = 3,
      STATUS_DISCONNECTED = 4,
      DATE = 5,
      TIME = 6,
      TERMINAL = 7,
      STATUS_WARNING = 8,

      // ----------------------------------------------
      // Basic controls
      // RMK: Labels for new controls
      // ----------------------------------------------
      BOX_TITLE = 20,
      BOX_TITLE_WITH_DATA_UP = 21,
      BOX_TITLE_WITH_DATA_DOWN = 22,
      TABLE_HEADER = 23,
      POPUP_HEADER = 24,
      POPUP_FULL_SCREEN_LEFT = 25,
      POPUP_FULL_SCREEN_PLAYER = 26,

      // ----------------------------------------------
      // General controls
      // RMK: Only this labels should be used in general screens
      // ----------------------------------------------
      TITLE_PRINCIPAL_16PX_BLACK = 50,
      TITLE_PRINCIPAL_16PX_BLUE = 51,

      TITLE_SECONDARY_14PX_BLUE = 52,

      TEXT_INFO_8PX_BLACK_BOLD = 53,

      TEXT_INFO_10PX_BLACK = 54,

      TEXT_INFO_12PX_BLACK = 55,
      TEXT_INFO_12PX_BLACK_BOLD = 56,
      TEXT_INFO_12PX_BLUE = 57,

      TEXT_INFO_14PX_BLACK = 58,
      TEXT_INFO_14PX_BLACK_BOLD = 59,
      TEXT_INFO_14PX_RED = 60,
      TEXT_INFO_14PX_GREEN = 61,
      TEXT_INFO_14PX_BLUE = 62,
      TEXT_INFO_14PX_WHITE = 63,
      TEXT_INFO_14PX_WHITE_BOLD = 64,

      TEXT_INFO_32PX_WHITE = 65,
      TEXT_INFO_32PX_BLACK = 66,
      TEXT_INFO_32PX_BLACK_BOLD = 67,

      TITLE_PRINCIPAL_24PX_BLACK = 68,

      

    }


    private LabelStyle m_style;
    private Int32 m_corners_radius;

    public LabelStyle Style
    {
      get
      {
        return m_style;
      }
      set
      {
        m_style = value;

        SetLabelStyle();
      }
    }

    [Category("Appearance"), DefaultValue(0)]
    public Int32 CornerRadius
    {
      get
      {
        return m_corners_radius;
      }
      set
      {
        m_corners_radius = value;

        if (DesignMode && this.Parent != null)
        {
          this.Parent.Refresh(); // Refreshes the client area of the parent control
        }
      }
    }

    public uc_label()
    {      
      SetStyle(ControlStyles.SupportsTransparentBackColor, true);
      SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
      
      if (DesignMode)
      {
        m_style = LabelStyle.NONE;
      }
    }

    protected override void InitLayout()
    {
      base.InitLayout();
    }

    private void SetLabelStyle()
    {
      CashierStyle.ControlStyle.InitStyle(this);
    }

    protected void PaintTransparentBackground(Graphics Graphics, Rectangle ClipRect)
    {
      Graphics.Clear(Color.Transparent);
      if ((this.Parent != null))
      {
        ClipRect.Offset(this.Location);
        PaintEventArgs e = new PaintEventArgs(Graphics, ClipRect);
        GraphicsState state = Graphics.Save();
        Graphics.SmoothingMode = SmoothingMode.HighSpeed;
        try
        {
          Graphics.TranslateTransform((float)-this.Location.X, (float)-this.Location.Y);
          this.InvokePaintBackground(this.Parent, e);
          this.InvokePaint(this.Parent, e);
        }
        finally
        {
          Graphics.Restore(state);
          ClipRect.Offset(-this.Location.X, -this.Location.Y);
        }
      }
    }

    private void DrawBackground(Graphics graphics)
    {
      GraphicsPath _gr_path;
      float _top;
      float _left;
      float _right;
      float _bottom;

      graphics.SmoothingMode = SmoothingMode.AntiAlias;

      if (this.BackColor != Color.Transparent)
      {
        _gr_path = new GraphicsPath();

        _top = 0;
        _left = 0;
        _right = this.ClientRectangle.Width - 1;
        _bottom = this.ClientRectangle.Height - 1;

        if (m_corners_radius <= 0)
        {
          _gr_path.AddRectangle(new RectangleF(_left, _top, _right, _bottom));
        }
        else
        {
          _gr_path.AddArc(_left, _top, m_corners_radius, m_corners_radius, 180, 90);
          _gr_path.AddArc(_right - m_corners_radius, _top, m_corners_radius, m_corners_radius, 270, 90);
          _gr_path.AddArc(_right - m_corners_radius, _bottom - m_corners_radius, m_corners_radius, m_corners_radius, 0, 90);
          _gr_path.AddArc(_left, _bottom - m_corners_radius, m_corners_radius, m_corners_radius, 90, 90);
        }

        _gr_path.CloseAllFigures();

        graphics.FillPath(new SolidBrush(BackColor), _gr_path);
      }

    }

    protected override void OnPaint(PaintEventArgs e)
    {
      TextFormatFlags _format_flags;
      Rectangle _text_location;

      this.SuspendLayout();
      _format_flags = TextFormatFlags.WordBreak;

      PaintTransparentBackground(e.Graphics, this.ClientRectangle);

      // Draw background
      DrawBackground(e.Graphics);

      if (this.RightToLeft == System.Windows.Forms.RightToLeft.Yes)
      {
        _format_flags = _format_flags | TextFormatFlags.RightToLeft;
      }

      _text_location = new Rectangle(new Point(0, 0), this.Size);

        if (TextAlign == ContentAlignment.TopLeft ||
          TextAlign == ContentAlignment.TopCenter ||
          TextAlign == ContentAlignment.TopRight)
        {
          _format_flags = _format_flags | TextFormatFlags.Top;
        }
        else if(TextAlign == ContentAlignment.BottomLeft ||
            TextAlign == ContentAlignment.BottomCenter ||
            TextAlign == ContentAlignment.BottomRight)
        {
          _format_flags = _format_flags | TextFormatFlags.Bottom;
        }
        else if (TextAlign == ContentAlignment.MiddleLeft ||
            TextAlign == ContentAlignment.MiddleCenter ||
            TextAlign == ContentAlignment.MiddleRight)
        {
          _format_flags = _format_flags | TextFormatFlags.VerticalCenter;
        }

        if (TextAlign == ContentAlignment.TopLeft ||
         TextAlign == ContentAlignment.BottomLeft ||
         TextAlign == ContentAlignment.MiddleLeft)
        {
          _format_flags = _format_flags | TextFormatFlags.Left;
        }
        else if (TextAlign == ContentAlignment.TopCenter ||
         TextAlign == ContentAlignment.BottomCenter ||
         TextAlign == ContentAlignment.MiddleCenter)
        {
          _format_flags = _format_flags | TextFormatFlags.HorizontalCenter;
        }
        else if (TextAlign == ContentAlignment.TopRight ||
         TextAlign == ContentAlignment.BottomRight ||
         TextAlign == ContentAlignment.MiddleRight)
        {
          _format_flags = _format_flags | TextFormatFlags.Right;
        }                

        // Paint the text
        TextRenderer.DrawText(e.Graphics, Text, Font, _text_location, ForeColor, Color.Transparent, _format_flags);

        this.ResumeLayout();
    }

    protected override void OnEnabledChanged(EventArgs e)
    {
      base.OnEnabledChanged(e);

      SetLabelStyle();
    }
  }
}



