﻿//------------------------------------------------------------------------------
// Copyright © 2015-2020 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_top_bar.cs
// 
//   DESCRIPTION: Implements the user control for show the top bar
//
//        AUTHOR: Didac Campanals
// 
// CREATION DATE: 28-OCT-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 28-OCT-2015 DCS     First release.
// 19-NOV-2015 DCS     Change timer in frm_container to a generic thread, add class for create an asyncchronous thread
// 14-MAR-2016 FOS     Fixed Bug 9357: New design corrections
// 24-OCT-2016 JBC     Fixed Bug 19364: Now lbl_multisite hidden when is not multisite member.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using WSI.Common;

namespace WSI.Cashier.Controls
{
  public partial class uc_top_bar : UserControl
  {
    #region Properties

    private SystemInfo m_info;
    private uc_round_button.RoundButonStyle m_keyboard_style;
    private Image m_user_image;
    private String m_user_name;
    private String m_user_rol;
    private Boolean m_show_rol;
    private String m_terminal_name;
    private SystemInfo.StatusInfo m_cashier_status;
    private SystemInfo.StatusInfo m_multisite_status;
    private SystemInfo.StatusInfo m_database_status;
    private Timer m_timer;

    public SystemInfo SystemInfo
    {
      get
      {
        return m_info;
      }
      set
      {
        m_info = value;
      }
    }

    public uc_round_button.RoundButonStyle ButtonKeyboardStyle
    {
      get
      {
        return m_keyboard_style;
      }
      set
      {
        m_keyboard_style = value;
      }
    }

    public String UserName
    {
      get
      {
        return m_user_name;
      }
      set
      {
        m_user_name = value;
      }
    }

    public String UserRol
    {
      get
      {
        return m_user_rol;
      }
      set
      {
        m_user_rol = value;
      }
    }

    public Boolean ShowRol
    {
      get
      {
        return m_show_rol;
      }
      set
      {
        m_show_rol = value;
      }
    }

    public String TerminalName
    {
      get
      {
        return m_terminal_name;
      }
      set
      {
        m_terminal_name = value;
      }
    }

    public SystemInfo.StatusInfo CashierStatus
    {
      get
      {
        return m_cashier_status;
      }
      set
      {
        m_cashier_status = value;
      }
    }

    public SystemInfo.StatusInfo MultisiteStatus
    {
      get
      {
        return m_multisite_status;
      }
      set
      {
        m_multisite_status = value;
      }
    }

    public SystemInfo.StatusInfo DatabaseStatus
    {
      get
      {
        return m_database_status;
      }
      set
      {
        m_database_status = value;
      }
    }

    public Image UserImage
    {
      get
      {
        return m_user_image;
      }
      set
      {
        m_user_image = value;
      }
    }

    #endregion

    #region Public Methods

    public uc_top_bar()
    {
      InitializeComponent();
      SetStyle();
      HandlerEvents();
    }

    public Boolean GetInfo()
    {
      if (SystemInfo == null)
      {
        return false;
      }

      if (this.m_timer == null)
      {
        m_timer = new Timer();
        m_timer.Interval = 1000;
        m_timer.Tick += new EventHandler(Timer_Tick);
        m_timer.Start();
      }

      TerminalName = SystemInfo.GetAliasTerminal();
      CashierStatus = SystemInfo.GetCashierStatus();
      UserName = Cashier.UserName;

      lbl_version.Text = "v" + VersionControl.VersionStr;

      UpdateUser();
      UpdateStatus();

      return true;
    }

    #endregion Public Methods

    #region Private Methods

    private void Timer_Tick(object sender, EventArgs e)
    {

      lbl_date.Text = SystemInfo.GetDateClock();
      lbl_hour.Text = SystemInfo.GetTimeClock();

    }

    private void HandlerEvents()
    {
      WGDB.OnMultiSiteStatusChanged += new WGDB.MulitSiteStatusChangedHandler(WGDB_OnMultiSiteStatusChanged);
      WGDB.OnStatusChanged += new WGDB.StatusChangedHandler(WGDB_OnStatusChanged);
      WGDB_OnStatusChanged();
    }

    private void SetStyle()
    {
      CashierStyle.ControlStyle.InitStyle(this);

      SetImages();

    }

    private void SetImages()
    {
      pb_clock.Image = Resources.ResourceImages.clock;
      pb_user_image.Image = Resources.ResourceImages.iconoUsuario;
      pb_terminal.Image = Resources.ResourceImages.terminal;
      pb_image.Image = Resources.ResourceImages.wigosLogo;
      pb_image.SizeMode = PictureBoxSizeMode.CenterImage;
      btn_keyboard.Image = Resources.ResourceImages.keyboard;
    }

    private void UpdateUser()
    {
      lbl_user_name.Text = UserName;
      lbl_user_rol.Text = UserRol;
      lbl_user_rol.Visible = ShowRol;
      lbl_cashier_connected.Text = SystemInfo.GetTextStatusCashier();
      lbl_cashier_connected.Style = GetColorByStatus(CashierStatus);
    }

    private void UpdateStatus()
    {
      lbl_terminal.Text = TerminalName;
      lbl_multisite.Visible = MultisiteStatus != (SystemInfo.StatusInfo.UNKNOWN);
      lbl_multisite.Style = GetColorByStatus(MultisiteStatus);
      lbl_multisite.Text = Resource.String("STR_MULTISITE_CONN_STATUS");
      lbl_db.Style = GetColorByStatus(DatabaseStatus);
      lbl_db.Text = Resource.String("STR_DATABASE_CONN_STATUS");
      btn_keyboard.Text = (Resource.String("STR_FRM_CONTAINER_BTN_KEYBOARD")).ToUpper();

    }

    private uc_label.LabelStyle GetColorByStatus(SystemInfo.StatusInfo StatusInfo)
    {
      uc_label.LabelStyle _return_style;

      switch (StatusInfo)
      {
        case SystemInfo.StatusInfo.OK:
          _return_style = uc_label.LabelStyle.STATUS_CONNECTED;
          break;
        case SystemInfo.StatusInfo.WARNING:
          _return_style = uc_label.LabelStyle.STATUS_WARNING;
          break;
        case SystemInfo.StatusInfo.ERROR:
          _return_style = uc_label.LabelStyle.STATUS_DISCONNECTED;
          break;
        default:
          _return_style = uc_label.LabelStyle.STATUS_CONNECTED;
          break;
      }

      return _return_style;
    }

    #endregion Private Methods

    #region Events

    private void btn_keyboard_Click(object sender, EventArgs e)
    {
      WSIKeyboard.ForceToggle();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Update title depending of multisite status
    //
    //  PARAMS :
    //      - INPUT : OldStatus WGDB.MULTISITE_STATUS
    //                NewStatus WGDB.MULTISITE_STATUS
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void WGDB_OnMultiSiteStatusChanged(WGDB.MULTISITE_STATUS OldStatus, WGDB.MULTISITE_STATUS NewStatus)
    {
      if (lbl_multisite.InvokeRequired)
      {
        lbl_multisite.Invoke(new WGDB.MulitSiteStatusChangedHandler(WGDB_OnMultiSiteStatusChanged), new object[] { OldStatus, NewStatus });

        return;
      }

      switch (NewStatus)
      {
        case WGDB.MULTISITE_STATUS.MEMBER_CONNECTED:
          {
            MultisiteStatus = WSI.Cashier.SystemInfo.StatusInfo.OK;
          }
          break;
        case WGDB.MULTISITE_STATUS.MEMBER_NOT_CONNECTED:
          {
            MultisiteStatus = WSI.Cashier.SystemInfo.StatusInfo.ERROR;
          }
          break;

        case WGDB.MULTISITE_STATUS.NOT_MEMBER:
          {
            MultisiteStatus = WSI.Cashier.SystemInfo.StatusInfo.UNKNOWN;
            lbl_multisite.Visible = false;
          }
          break;


        default:
          {
            MultisiteStatus = WSI.Cashier.SystemInfo.StatusInfo.UNKNOWN;
          }
          break;
      }

      lbl_multisite.Style = GetColorByStatus(MultisiteStatus);
    }

    private void WGDB_OnStatusChanged()
    {
      if (lbl_db.InvokeRequired)
      {
        lbl_db.Invoke(new WGDB.StatusChangedHandler(WGDB_OnStatusChanged));

        return;
      }

      // DHA 09-APR-2013: Changed database status from title bar label
      switch (WGDB.ConnectionState)
      {
        case ConnectionState.Open:
        case ConnectionState.Executing:
        case ConnectionState.Fetching:
          {
            DatabaseStatus = WSI.Cashier.SystemInfo.StatusInfo.OK;
          }
          break;

        case ConnectionState.Connecting:
          {
            DatabaseStatus = WSI.Cashier.SystemInfo.StatusInfo.WARNING;
          }
          break;

        case ConnectionState.Closed:
        case ConnectionState.Broken:
        default:
          {
            DatabaseStatus = WSI.Cashier.SystemInfo.StatusInfo.ERROR;
          }
          break;
      }
    }

    #endregion Events

  }
}
