﻿//------------------------------------------------------------------------------
// Copyright © 2015-2020 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_round_textbox.cs
// 
//   DESCRIPTION: Implements the uc_round_textbox user control
//                Class: uc_round_textbox
//
//        AUTHOR: David Hernández
//                José Martínez
// 
// CREATION DATE: 20-OCT-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 20-OCT-2015 DHA     First release.
// 20-OCT-2015 JML     First release.
// 02-JUN-2016 DHA     Fixed Bug 14009: Tab between textboxes (uc_datetime)
// 25-JUL-2017 ETP     Fixed Bug WIGOS-3953 Amount requested twice when amortizing a credit line
//------------------------------------------------------------------------------

using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using System.Xml.Serialization;
using WSI.Common;

namespace WSI.Cashier.Controls
{
  public class uc_round_textbox : Control
  {
    #region Enums
    public enum RoundTextBoxStyle
    {
      NONE = 0,
      BASIC = 1,
      MULTILINE = 2,
      CALCULATOR = 3,
      BASIC_CUSTOM = 4,
    }

    public enum ENUM_FORMAT_MODE
    {
      BaseMode,
      ExtendedMode
    }
    #endregion

    #region Delegates

    [Category("Behavior")]
    //public event EventHandler TextChangedEvent;

    #endregion

    #region Properties

    private TextBox m_textbox = new TextBox();
    private Label m_label = new Label();
    private Boolean m_resize = false;
    private Control m_container = new ContainerControl();
    Int32 _margin = 6;
    private Rectangle m_rect_content = new Rectangle(0, 0, 1, 1);
    private Radius m_radius = new Radius();

    public Int32 MaxLength
    {
      get
      {
        return m_textbox.MaxLength;
      }
      set
      {
        m_textbox.MaxLength = value;
      }
    }

    public Char PasswordChar
    {
      get
      {
        return m_textbox.PasswordChar;
      }
      set
      {
        m_textbox.PasswordChar = value;
      }
    }

    public Boolean ReadOnly
    {
      get
      {
        return m_textbox.ReadOnly;
      }
      set
      {
        m_textbox.ReadOnly = value;
        m_textbox.TabStop = !value;
      }
    }

    public AutoCompleteStringCollection AutoCompleteCustomSource
    {
      get
      {
        return m_textbox.AutoCompleteCustomSource;
      }
      set
      {
        m_textbox.AutoCompleteCustomSource = value;
      }
    }

    public AutoCompleteMode AutoCompleteMode
    {
      get
      {
        return m_textbox.AutoCompleteMode;
      }
      set
      {
        m_textbox.AutoCompleteMode = value;
      }
    }

    public AutoCompleteSource AutoCompleteSource
    {
      get
      {
        return m_textbox.AutoCompleteSource;
      }
      set
      {
        m_textbox.AutoCompleteSource = value;
      }
    }

    public CharacterCasing CharacterCasing
    {
      get
      {
        return m_textbox.CharacterCasing;
      }
      set
      {
        m_textbox.CharacterCasing = value;
      }
    }

    public Int32 SelectionLength
    {
      get
      {
        return m_textbox.SelectionLength;
      }
      set
      {
        m_textbox.SelectionLength = value;
      }
    }

    public Int32 SelectionStart
    {
      get
      {
        return m_textbox.SelectionStart;
      }
      set
      {
        m_textbox.SelectionStart = value;
      }
    }

    public Boolean UseSystemPasswordChar
    {
      get
      {
        return m_textbox.UseSystemPasswordChar;
      }
      set
      {
        m_textbox.UseSystemPasswordChar = value;
      }
    }

    public HorizontalAlignment TextAlign
    {
      get
      {
        return m_textbox.TextAlign;
      }
      set
      {
        m_textbox.TextAlign = value;
      }
    }

    public ScrollBars ScrollBars
    {
      get
      {
        return m_textbox.ScrollBars;
      }
      set
      {
        m_textbox.ScrollBars = value;
      }
    }

    public Boolean TextBox_Focused
    {
      get { return this.m_textbox.Focused; }
    }

    [Category("Appearance")]
    public Int32 CornerRadius
    {
      get
      {
        return m_radius.TopLeft;
      }
      set
      {
        m_radius.TopLeft = value;
        m_radius.TopRight = value;
        m_radius.BottomLeft = value;
        m_radius.BottomRight = value;

        if (DesignMode && this.Parent != null)
        {
          this.Parent.Refresh(); // Refreshes the client area of the parent control
        }
      }
    }

    private RoundTextBoxStyle m_style;

    [Category("Appearance")]
    public RoundTextBoxStyle Style
    {
      get
      {
        return m_style;
      }
      set
      {
        m_style = value;

        SetTextBoxStyle();

        if (DesignMode && this.Parent != null)
        {
          OnSizeChanged(null);
          this.Parent.Refresh(); // Refreshes the client area of the parent control
        }
      }
    }

    private Color m_border_color;
    [Category("Appearance")]
    public Color BorderColor
    {
      get
      {
        return m_border_color;
      }
      set
      {
        m_border_color = value;

        if (DesignMode && this.Parent != null)
        {
          this.Parent.Refresh(); // Refreshes the client area of the parent control
        }

        //SetButtonStyle();
      }
    }

    private Color m_backcolor;
    public new Color BackColor
    {
      get { return m_backcolor; }
      set
      {
        this.m_backcolor = value;
        m_textbox.BackColor = value;
        Invalidate(true);
      }
    }

    public override Color ForeColor
    {
      get
      {
        return this.m_textbox.ForeColor;
      }
      set
      {
        this.m_textbox.ForeColor = value;
        Invalidate(true);
      }
    }

    [EditorBrowsable(EditorBrowsableState.Always)]
    [Browsable(true)]
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
    [Bindable(true)]
    public override string Text
    {
      get
      {
        if (this.m_textbox.Text == this.m_water_mark)
        {
          return String.Empty;
        }
        else
        {
          return m_textbox.Text;
        }
      }
      set
      {
        m_textbox.Text = value;
      }
    }

    [Category("Appearance")]
    public Boolean Multiline
    {
      get
      {
        return m_textbox.Multiline;
      }
      set
      {
        m_textbox.Multiline = value;
      }
    }

    public string SelectedText
    {
      get
      {
        return m_textbox.SelectedText;
      }
      set
      {
        m_textbox.SelectedText = value;
      }
    }

    private String m_water_mark;
    public String WaterMark
    {
      get { return m_water_mark; }
      set
      {
        this.m_water_mark = value;
        Invalidate(true);
      }
    }

    public Color WaterMarkColor
    {
      get
      {
        return this.m_label.ForeColor;
      }
      set
      {
        this.m_label.ForeColor = value;
        Invalidate(true);
      }
    }

    public Int32 TextLength
    {
      get
      {
        return this.m_textbox.TextLength;
      }
    }

    #endregion

    #region Events
    [Browsable(true)]
    public new event EventHandler TextChanged;

    [Browsable(true)]
    public new event KeyPressEventHandler KeyPress;

    [Browsable(true)]
    public new event EventHandler Enter;

    [Browsable(true)]
    public new event EventHandler Leave;

    [Browsable(true)]
    public new event EventHandler GotFocus;
    #endregion

    #region Constructors
    public uc_round_textbox()
    {
      SetStyle(ControlStyles.AllPaintingInWmPaint, true);
      SetStyle(ControlStyles.ContainerControl, true);
      SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
      SetStyle(ControlStyles.ResizeRedraw, true);
      SetStyle(ControlStyles.Selectable, true);
      SetStyle(ControlStyles.SupportsTransparentBackColor, true);
      SetStyle(ControlStyles.UserMouse, true);
      SetStyle(ControlStyles.UserPaint, true);
      SetStyle(ControlStyles.Selectable, true);

      base.BackColor = Color.Transparent;

      this.TabStop = false;

      this.SuspendLayout();
      m_textbox.BorderStyle = System.Windows.Forms.BorderStyle.None;
      m_textbox.Location = new System.Drawing.Point(3, 4);
      m_textbox.Size = new System.Drawing.Size(60, 13);
      m_textbox.TabIndex = 0;
      m_textbox.Margin = new Padding(0);
      m_textbox.Padding = new Padding(0);
      m_textbox.TextAlign = HorizontalAlignment.Left;
      m_textbox.Enabled = this.Enabled;
      m_textbox.TabStop = this.ReadOnly ? false : true;
      
      this.Controls.Add(m_textbox);

      m_label.AutoSize = false;
      m_label.Location = new System.Drawing.Point(3, 4);
      m_label.Size = new System.Drawing.Size(60, 13);
      m_label.TabIndex = 0;
      m_label.Margin = new Padding(0);
      m_label.Padding = new Padding(0);
      m_label.TextAlign = ContentAlignment.MiddleLeft;
      m_label.Enabled = this.Enabled;
      m_label.Text = WaterMark;
      m_label.TabStop = true;

      this.Controls.Add(m_label);

      SetTextBoxStyle();

      this.ResumeLayout(false);

      this.m_textbox.TextChanged += new EventHandler(TextBox_TextChanged);
      this.m_textbox.KeyPress += new KeyPressEventHandler(TextBox_KeyPress);
      this.m_textbox.Enter += new EventHandler(TextBox_Enter);
      this.m_textbox.Leave += new EventHandler(TextBox_Leave);
      this.m_textbox.Click += new EventHandler(TextBox_Click);
      this.m_textbox.MouseDown += new MouseEventHandler(m_textbox_MouseDown);
      this.m_textbox.KeyDown += new KeyEventHandler(TextBox_KeyDown);

      this.m_label.KeyPress += new KeyPressEventHandler(TextBox_KeyPress);
      this.m_label.Enter += new EventHandler(TextBox_Enter);
      this.m_label.Leave += new EventHandler(TextBox_Leave);
      this.m_label.Click += new EventHandler(TextBox_Click);
      this.m_label.MouseDown += new MouseEventHandler(m_textbox_MouseDown);
    }

    #endregion

    #region OverrideMethods

    protected override void OnCreateControl()
    {
      m_textbox.Enabled = this.Enabled;

      m_label.Text = WaterMark;

      this.TabStop = false;

      if (String.IsNullOrEmpty(this.Text) && !String.IsNullOrEmpty(this.WaterMark))
      {
        this.m_textbox.SendToBack();
      }
      else
      {
        this.m_textbox.BringToFront();
      }

      if (this.m_textbox.TextAlign == HorizontalAlignment.Center)
      {
        this.m_label.TextAlign = ContentAlignment.MiddleCenter;
      }
      else if (this.m_textbox.TextAlign == HorizontalAlignment.Left)
      {
        this.m_label.TextAlign = ContentAlignment.MiddleLeft;
      }
      else if (this.m_textbox.TextAlign == HorizontalAlignment.Right)
      {
        this.m_label.TextAlign = ContentAlignment.MiddleRight;
      }

      base.OnCreateControl();
    }

    protected override void OnEnabledChanged(EventArgs e)
    {
      base.OnEnabledChanged(e);

      this.m_textbox.Enabled = this.Enabled;

      SetTextBoxStyle();
    }

    protected override void OnResize(EventArgs e)
    {
      if (m_resize)
      {
        m_resize = false;
        AdjustControls();

        Invalidate(true);
      }
      else
        Invalidate(true);
    }

    protected override void OnForeColorChanged(EventArgs e)
    {
      m_textbox.ForeColor = this.ForeColor;
      base.OnForeColorChanged(e);
    }

    protected override void OnPaint(PaintEventArgs e)
    {
      e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;

      Brush _br_background;
      Rectangle _rect_cont;
      Rectangle _rect_outer;
      GraphicsPath _path_content_border;
      GraphicsPath _path_outer_border;
      Pen _pen_outer_border;
      
      e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;

      //content border
      _rect_cont = m_rect_content;
      _rect_cont.X += 1;
      _rect_cont.Y += 1;
      _rect_cont.Width -= 3;
      _rect_cont.Height -= 3;
      _path_content_border = CreateRoundRectangle(_rect_cont, m_radius.TopLeft, m_radius.TopRight, m_radius.BottomRight,
          m_radius.BottomLeft);

      //outer border
      _rect_outer = m_rect_content;
      _rect_outer.Width -= 1;
      _rect_outer.Height -= 1;
      _path_outer_border = CreateRoundRectangle(_rect_outer, m_radius.TopLeft, m_radius.TopRight, m_radius.BottomRight,
          m_radius.BottomLeft);

      _br_background = new SolidBrush(BackColor);
      _pen_outer_border = new Pen(BorderColor, 2);

      //draw
      e.Graphics.FillPath(_br_background, _path_content_border);

      e.Graphics.DrawPath(_pen_outer_border, _path_outer_border);

      _path_content_border.Dispose();
      _path_outer_border.Dispose();
      _pen_outer_border.Dispose();
      _br_background.Dispose();

    }

    #endregion

    #region PrivateMethods
    protected virtual void OnTextChanged(object sender, EventArgs e)
    {
      Int32 _cursor_position;

      if (!string.IsNullOrEmpty(m_water_mark))
      {
        if (!string.IsNullOrEmpty(m_textbox.Text))
        {
          m_textbox.BringToFront();
        }
        else
        {
          m_textbox.SendToBack();
        }
      }

      if (TextChanged != null)
      {
        if (m_style == RoundTextBoxStyle.BASIC)
        {
          _cursor_position = m_textbox.SelectionStart;
          this.Text = this.Text.ToUpper();
          m_textbox.SelectionStart = _cursor_position;
        }
        TextChanged(this, e);
      }

    }

    private void TextBox_TextChanged(object sender, EventArgs e)
    {
      OnTextChanged(this, e);
    }

    private void TextBox_Click(object sender, EventArgs e)
    {
      OnClick(e);
    }

    void m_textbox_MouseDown(object sender, MouseEventArgs e)
    {

      base.OnMouseDown(e);
    }

    void TextBox_KeyDown(object sender, KeyEventArgs e)
    {
      OnKeyDown(e);
    }

    protected override void OnClick(EventArgs e)
    {
      this.m_textbox.BringToFront();
      if (!m_textbox.ReadOnly)
      {
        this.m_textbox.Focus();
      }

      base.OnClick(e);
    }

    protected virtual void OnClick(object sender, EventArgs e)
    {
      this.m_textbox.BringToFront();
      this.m_textbox.Focus();

      OnClick(sender, e);
    }

    protected override void OnKeyDown(KeyEventArgs e)
    {
      base.OnKeyDown(e);

      switch (e.KeyData)
      {
        case Keys.Shift | Keys.Tab:
          this.SelectNextControl(this, false, true, true, true);
          break;
      }

    }

    protected virtual void OnKeyPress(object sender, KeyPressEventArgs e)
    {

      if (KeyPress != null)
      {
        KeyPress(this, e);
      }
    }

    private void TextBox_KeyPress(object sender, KeyPressEventArgs e)
    {
      OnKeyPress(this, e);
    }

    protected virtual void OnEnter(object sender, EventArgs e)
    {
      this.m_textbox.BringToFront();

      if (Enter != null)
      {
        Enter(this, e);
      }
    }

    private void TextBox_Enter(object sender, EventArgs e)
    {
      OnEnter(this, e);
    }

    protected virtual void OnLeave(object sender, EventArgs e)
    {
      if (String.IsNullOrEmpty(m_textbox.Text) && !String.IsNullOrEmpty(this.m_water_mark))
      {
        this.m_textbox.SendToBack();
      }
      else
      {
        this.m_textbox.BringToFront();
      }

      if (Leave != null)
      {
        Leave(this, e);
      }
    }

    private void TextBox_Leave(object sender, EventArgs e)
    {
      OnLeave(this, e);
    }

    protected virtual void OnGotFocus(object sender, EventArgs e)
    {
      if (m_textbox.Text == this.m_water_mark)
      {
        this.m_textbox.BringToFront();
      }
      else
      {
        this.m_textbox.SendToBack();
      }

      if (GotFocus != null)
      {
        GotFocus(sender, e);
      }
    }

    protected override void OnGotFocus(EventArgs e)
    {
      this.m_textbox.BringToFront();
      this.m_textbox.Focus();

      base.OnGotFocus(e);
    }

    private void AdjustControls()
    {
      this.SuspendLayout();

      m_resize = true;

      // Localización del textbox
      m_textbox.Left = _margin;
      m_label.Left = _margin;

      if (m_style != RoundTextBoxStyle.MULTILINE)
      {
        if (m_style == RoundTextBoxStyle.BASIC)
        {
          this.Height = 40;
        }

        m_textbox.Top = (this.Height - m_textbox.Height) / 2;
        m_label.Top = (this.Height - m_textbox.Height) / 2;
        m_textbox.WordWrap = false;
      }
      else
      {
        m_textbox.Top = _margin;
        m_label.Top = _margin;

        m_textbox.Height = this.Height - 2 * m_textbox.Top;
        m_label.Height = this.Height - 2 * m_textbox.Top;
        m_textbox.WordWrap = true;
      }

      m_label.Height = m_textbox.Height;
      m_textbox.Width = this.Width - 2 * m_textbox.Left;
      m_label.Width = this.Width - 2 * m_textbox.Left;

      m_rect_content = new Rectangle(ClientRectangle.Left + 1, ClientRectangle.Top,
          ClientRectangle.Width - 2, this.Height);

      this.ResumeLayout();

      Invalidate(true);


    }

    private void SetTextBoxStyle()
    {
      CashierStyle.ControlStyle.InitStyle(this);

      AdjustControls();
    }
    #endregion

    #region PublicMethods
    public void Select(Int32 Start, Int32 Length)
    {
      this.m_textbox.Select(Start, Length);
    }

    public void SelectAll()
    {
      this.m_textbox.Focus();
      this.m_textbox.SelectAll();      
    }
    #endregion

    #region Render

    public static GraphicsPath CreateRoundRectangle(Rectangle Rectangle, Int32 TopLeftRadius, Int32 TopRightRadius, Int32 BottomRightRadius, Int32 BottomLeftRadius)
    {
      GraphicsPath _path = new GraphicsPath();
      Int32 _left;
      Int32 _top;
      Int32 _width;
      Int32 _height;

      _left = Rectangle.Left;
      _top = Rectangle.Top;
      _width = Rectangle.Width;
      _height = Rectangle.Height;

      if (TopLeftRadius > 0)
      {
        _path.AddArc(_left, _top, TopLeftRadius * 2, TopLeftRadius * 2, 180, 90);
      }
      _path.AddLine(_left + TopLeftRadius, _top, _left + _width - TopRightRadius, _top);
      if (TopRightRadius > 0)
      {
        _path.AddArc(_left + _width - TopRightRadius * 2, _top, TopRightRadius * 2, TopRightRadius * 2, 270, 90);
      }
      _path.AddLine(_left + _width, _top + TopRightRadius, _left + _width, _top + _height - BottomRightRadius);
      if (BottomRightRadius > 0)
      {
        _path.AddArc(_left + _width - BottomRightRadius * 2, _top + _height - BottomRightRadius * 2,
            BottomRightRadius * 2, BottomRightRadius * 2, 0, 90);
      }
      _path.AddLine(_left + _width - BottomRightRadius, _top + _height, _left + BottomLeftRadius, _top + _height);
      if (BottomLeftRadius > 0)
      {
        _path.AddArc(_left, _top + _height - BottomLeftRadius * 2, BottomLeftRadius * 2, BottomLeftRadius * 2, 90, 90);
      }
      _path.AddLine(_left, _top + _height - BottomLeftRadius, _left, _top + TopLeftRadius);
      _path.CloseFigure();
      return _path;
    }

    #endregion

    public class Radius
    {
      private int _topLeft = 0;

      public int TopLeft
      {
        get { return _topLeft; }
        set { _topLeft = value; }
      }

      private int _topRight = 0;

      public int TopRight
      {
        get { return _topRight; }
        set { _topRight = value; }
      }

      private int _bottomLeft = 0;

      public int BottomLeft
      {
        get { return _bottomLeft; }
        set { _bottomLeft = value; }
      }

      private int _bottomRight = 0;

      public int BottomRight
      {
        get { return _bottomRight; }
        set { _bottomRight = value; }
      }
    }
  }
}
