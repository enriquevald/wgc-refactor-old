﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Windows.Forms;
using WSI.Common;

namespace WSI.Cashier.Controls
{
  public class UcPhotobox:PictureBox
  {
    private bool m_mouseover;

    protected override void OnPaint(PaintEventArgs Pe)
    {
      Pe.Graphics.DrawImage(
        Image == null ? BackgroundImage : FixedSize(Image, Width, Height, true),
        new Rectangle(0, 0, Width, Height));
      if (m_mouseover)
        Pe.Graphics.DrawImage(SetImageOpacity(Properties.Resources.magnify, .6f),
          new Rectangle(Width/2 - 22, Height/2 - 22, 45, 45));
      else
        Pe.Graphics.DrawImage(SetImageOpacity(Properties.Resources.compact_camera_128, .6f),
          new Rectangle(Width - 21, 0, 20, 20));
    }


    public static Image FixedSize(Image Img, int Width, int Height, bool NeedToFill)
    {
      int _source_width = Img.Width;
      int _source_height = Img.Height;
      int _source_x = 0;
      int _source_y = 0;
      double _dest_x = 0;
      double _dest_y = 0;

      double _n_scale;
      double _n_scale_w;
      double _n_scale_h;

      _n_scale_w = (Width / (double)_source_width);
      _n_scale_h = (Height / (double)_source_height);
      if (!NeedToFill)
      {
        _n_scale = Math.Min(_n_scale_h, _n_scale_w);
      }
      else
      {
        _n_scale = Math.Max(_n_scale_h, _n_scale_w);
        _dest_y = (Height - _source_height * _n_scale) / 2;
        _dest_x = (Width - _source_width * _n_scale) / 2;
      }

      if (_n_scale > 1)
        _n_scale = 1;

      int _dest_width = (int)Math.Round(_source_width * _n_scale);
      int _dest_height = (int)Math.Round(_source_height * _n_scale);

      Bitmap _bm_photo;
      try
      {
        _bm_photo = new Bitmap(_dest_width + (int)Math.Round(2 * _dest_x), _dest_height + (int)Math.Round(2 * _dest_y));
      }
      catch (Exception _ex)
      {
        throw new ApplicationException(string.Format("destWidth:{0}, destX:{1}, destHeight:{2}, desxtY:{3}, Width:{4}, Height:{5}",
            _dest_width, _dest_x, _dest_height, _dest_y, Width, Height), _ex);
      }
      using (Graphics _gr_photo = Graphics.FromImage(_bm_photo))
      {
        _gr_photo.InterpolationMode =  InterpolationMode.HighQualityBicubic;
        _gr_photo.CompositingQuality =  CompositingQuality.HighQuality;
        _gr_photo.SmoothingMode = SmoothingMode.HighQuality;

        Rectangle _to = new Rectangle((int)Math.Round(_dest_x), (int)Math.Round(_dest_y), _dest_width, _dest_height);
        Rectangle _from = new Rectangle(_source_x, _source_y, _source_width, _source_height);
        //Console.WriteLine("From: " + from.ToString());
        //Console.WriteLine("To: " + to.ToString());
        _gr_photo.DrawImage(Img, _to, _from, GraphicsUnit.Pixel);

        return _bm_photo;
      }
    }

    readonly Timer m_t = new Timer {Interval = 2000};

    public UcPhotobox()
    {
      m_t.Tick+=t_Tick;
      m_t.Enabled = false;
    }

    void t_Tick(object Sender, EventArgs E)
    {
      m_t.Enabled = false;
      m_isaclick = false;
      if (MouseButtons == MouseButtons.Left)
      {
        ViewImage.ViewFullScreenImage(Image);
      }
      
    }

    private bool m_isaclick;

    protected override void OnMouseDown(MouseEventArgs E)
    {
      if (!Enabled)
        return;
      m_isaclick = true;
      m_mouseover = true;
      m_t.Enabled = true;
      Refresh();
      base.OnMouseDown(E);
    }

    protected override void OnMouseClick(MouseEventArgs E)
    {
      if (!Enabled)
        return;

    }

    protected override void OnMouseUp(MouseEventArgs E)
    {
      if (!Enabled)
        return;

      m_mouseover = false;
      m_t.Enabled = false;
      Refresh();
      if (m_isaclick) base.OnMouseClick(E);
      base.OnMouseUp(E);
    }

    /// <summary>  
    /// method for changing the opacity of an image  
    /// </summary>  
    /// <param name="Img">image to set opacity on</param>  
    /// <param name="Opacity">percentage of opacity</param>  
    /// <returns></returns>  
    public Image SetImageOpacity(Image Img, float Opacity)
    {
      try
      {
        //create a Bitmap the size of the image provided  
        Bitmap _bmp = new Bitmap(Img.Width, Img.Height);

        //create a graphics object from the image  
        using (Graphics _gfx = Graphics.FromImage(_bmp))
        {

          //create a colour matrix object  
          ColorMatrix _matrix = new ColorMatrix {Matrix33 = Opacity};

          //set the opacity  

          //create image attributes  
          ImageAttributes _attributes = new ImageAttributes();

          //set the colour(opacity) of the image  
          _attributes.SetColorMatrix(_matrix, ColorMatrixFlag.Default, ColorAdjustType.Bitmap);

          //now draw the image  
          _gfx.DrawImage(Img, new Rectangle(0, 0, _bmp.Width, _bmp.Height), 0, 0, Img.Width, Img.Height, GraphicsUnit.Pixel, _attributes);
        }
        return _bmp;
      }
      catch (Exception _ex)
      {
        MessageBox.Show(_ex.Message);
        return null;
      }
    } 
  }
}
