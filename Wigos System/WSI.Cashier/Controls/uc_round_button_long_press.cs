using System;
using System.Windows.Forms;

namespace WSI.Cashier.Controls
{
	public class uc_round_button_long_press : uc_round_button
	{
		readonly Timer m_t = new Timer { Interval = 2000 };
		public event EventHandler LongClick;

		public uc_round_button_long_press()
		{
			m_t.Tick += t_Tick;
			m_t.Enabled = false;
		}

		void t_Tick(object Sender, EventArgs E)
		{
			IsSelected = false;
			m_t.Enabled = false;
			m_isaclick = false;
			if (MouseButtons == MouseButtons.Left)
			{
				OnLongClick(E);
			}
		}

		private bool m_isaclick;
		protected override void OnMouseDown(MouseEventArgs E)
		{
			if (!Enabled)
				return;
			IsSelected = true;
			m_isaclick = true;
			m_t.Enabled = true;
			Refresh();
			base.OnMouseDown(E);
		}

		protected override void OnClick(EventArgs E)
		{
			if (!Enabled)
				return;

		}

		protected void OnLongClick(EventArgs E)
		{
			if (LongClick != null)
			{
				LongClick(this, E);
			}
		}

		protected override void OnMouseUp(MouseEventArgs E)
		{
			if (!Enabled)
				return;
			IsSelected = false;
			m_t.Enabled = false;
			Refresh();
			if (m_isaclick) base.OnClick(E);
			base.OnMouseUp(E);
		}

		//private new void DrawButton(Graphics graphics)
		//{
		//  GraphicsPath _gr_path;
		//  GraphicsPath _gr_path_is_selected;
		//  GraphicsPath _gr_path_clip;
		//  Int32 _selected_border_size;
		//  float _top;
		//  float _left;
		//  float _right;
		//  float _bottom;

		//  graphics.SmoothingMode = SmoothingMode.AntiAlias;

		//  _gr_path = new GraphicsPath();
		//  _gr_path_clip = new GraphicsPath();
		//  _gr_path_is_selected = new GraphicsPath();
		//  _selected_border_size = 5;

		//  // Set clip area
		//  _top = -1;
		//  _left = -1;
		//  _right = Width;
		//  _bottom = Height;

		//  if (CornerRadius <= 0)
		//  {
		//    _gr_path_clip.AddRectangle(new RectangleF(_left, _top, _right, _bottom));
		//  }
		//  else
		//  {
		//    _gr_path_clip.AddArc(_left, _top, CornerRadius, CornerRadius, 180, 90);
		//    _gr_path_clip.AddArc(_right - CornerRadius, _top, CornerRadius, CornerRadius, 270, 90);
		//    _gr_path_clip.AddArc(_right - CornerRadius, _bottom - CornerRadius, CornerRadius, CornerRadius, 0, 90);
		//    _gr_path_clip.AddArc(_left, _bottom - CornerRadius, CornerRadius, CornerRadius, 90, 90);
		//  }

		//  _gr_path_clip.CloseAllFigures();
		//  graphics.SetClip(_gr_path_clip);

		//  // Draw button figure
		//  _top = 0;
		//  _left = 0;
		//  _right = Width - 1;
		//  _bottom = Height - 1;

		//  if (CornerRadius <= 0)
		//  {
		//    _gr_path.AddRectangle(new RectangleF(_left, _top, _right, _bottom));
		//    _gr_path_is_selected.AddRectangle(new RectangleF(_left + _selected_border_size, _top + _selected_border_size, _right - _selected_border_size, _bottom - _selected_border_size));

		//  }
		//  else
		//  {
		//    _gr_path.AddArc(_left, _top, CornerRadius, CornerRadius, 180, 90);
		//    _gr_path.AddArc(_right - CornerRadius, _top, CornerRadius, CornerRadius, 270, 90);
		//    _gr_path.AddArc(_right - CornerRadius, _bottom - CornerRadius, CornerRadius, CornerRadius, 0, 90);
		//    _gr_path.AddArc(_left, _bottom - CornerRadius, CornerRadius, CornerRadius, 90, 90);

		//    _gr_path_is_selected.AddArc(_left + _selected_border_size, _top + _selected_border_size, CornerRadius, CornerRadius, 180, 90);
		//    _gr_path_is_selected.AddArc(_right - CornerRadius - _selected_border_size, _top + _selected_border_size, CornerRadius, CornerRadius, 270, 90);
		//    _gr_path_is_selected.AddArc(_right - CornerRadius - _selected_border_size, _bottom - CornerRadius - _selected_border_size, CornerRadius, CornerRadius, 0, 90);
		//    _gr_path_is_selected.AddArc(_left + _selected_border_size, _bottom - CornerRadius - _selected_border_size, CornerRadius, CornerRadius, 90, 90);
		//  }

		//  _gr_path.CloseAllFigures();
		//  _gr_path_is_selected.CloseAllFigures();

		//  if (m_is_selected && m_selected_color != null && Enabled)
		//  {
		//    graphics.FillPath(new SolidBrush(SelectedColor), _gr_path);
		//    graphics.FillPath(new SolidBrush(BackColor), _gr_path_is_selected);
		//  }
		//  else
		//  {
		//    graphics.FillPath(new SolidBrush(BackColor), _gr_path);
		//  }
		//}

}
}