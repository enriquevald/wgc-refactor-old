﻿//------------------------------------------------------------------------------
// Copyright © 2015-2020 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_checkBox.cs
// 
//   DESCRIPTION: Implements the uc_checkBox user control
//                Class: uc_checkBox
//
//        AUTHOR: José Martínez
//                David Hernández
// 
// CREATION DATE: 20-OCT-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 20-OCT-2015 JML     First release.
// 20-OCT-2015 DHA     First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace WSI.Cashier.Controls
{
  public class uc_checkBox : CheckBox
  {
    private Image m_image;

    public override bool AutoSize
    {
      get
      {
        return base.AutoSize;
      }
      set
      {
        base.AutoSize = value;
      }
    }

    public uc_checkBox()
    {
      this.SetStyle(ControlStyles.UserPaint, true);
      this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
      SetStyle(ControlStyles.SupportsTransparentBackColor, true);

      base.BackColor = Color.Transparent;
      this.TextAlign = ContentAlignment.MiddleLeft;

      m_image = Resources.ResourceImages.checkBox;
    }

    protected override void InitLayout()
    {
      base.InitLayout();
    }

    private void SetCheckBoxStyle()
    {
      CashierStyle.ControlStyle.InitStyle(this);
    }

    protected override void OnEnabledChanged(EventArgs e)
    {
      base.OnEnabledChanged(e);

      SetCheckBoxStyle();
    }

    protected override void OnSizeChanged(EventArgs e)
    {
      base.OnSizeChanged(e);
      this.MinimumSize = new Size(this.Width, 30);
    }

    protected override void OnCreateControl()
    {
      base.OnCreateControl();

      this.Padding = new Padding(5);

      SetCheckBoxStyle();
    }

    protected void PaintTransparentBackground(Graphics Graphics, Rectangle ClipRect)
    {
      Graphics.Clear(Color.Transparent);
      if ((this.Parent != null))
      {
        ClipRect.Offset(this.Location);
        PaintEventArgs e = new PaintEventArgs(Graphics, ClipRect);
        GraphicsState state = Graphics.Save();
        Graphics.SmoothingMode = SmoothingMode.HighSpeed;
        try
        {
          Graphics.TranslateTransform((float)-this.Location.X, (float)-this.Location.Y);
          this.InvokePaintBackground(this.Parent, e);
          this.InvokePaint(this.Parent, e);
        }
        finally
        {
          Graphics.Restore(state);
          ClipRect.Offset(-this.Location.X, -this.Location.Y);
        }
      }
    }

    protected override void OnPaint(PaintEventArgs e)
    {
      TextFormatFlags _format_flags;
      Rectangle _text_area;
      Size _image_size;
      int _x;
      int _y;

      _image_size = new System.Drawing.Size(0, 0);
      _text_area = new Rectangle(new Point(0, 0), this.ClientRectangle.Size);      

      _x = 0;
      _y = 0;

      PaintTransparentBackground(e.Graphics, this.ClientRectangle);
      
      _text_area.Size = new Size(_text_area.Size.Width - m_image.Width, _text_area.Size.Height);

      switch (this.CheckAlign)
      {
        case ContentAlignment.BottomCenter:
          _x = (this.Width / 2) - (m_image.Width / 2);
          _y = this.Height - m_image.Height;
          _text_area.Location = new Point(0, 0);
          _text_area.Size = new Size(_text_area.Size.Width, 15);

          break;
        case ContentAlignment.BottomLeft:
          _x = 0;
          _y = this.Height - m_image.Height;
          _text_area.Location = new Point(m_image.Width, _text_area.Location.Y);

          break;
        case ContentAlignment.BottomRight:
          _x = this.Width - m_image.Width;
          _y = this.Height - m_image.Height;
          _text_area.Location = new Point(0, _text_area.Location.Y);

          break;
        case ContentAlignment.MiddleCenter:
          _x = (this.Width / 2) - (m_image.Width / 2);
          _y = (this.Height / 2) - (m_image.Height / 2);

          break;
        case ContentAlignment.MiddleLeft:
          _x = 0;
          _y = (this.Height / 2) - (m_image.Height / 2);
          _text_area.Location = new Point(m_image.Width, _text_area.Location.Y);

          break;
        case ContentAlignment.MiddleRight:
          _x = this.Width - m_image.Width;
          _y = (this.Height / 2) - (m_image.Height / 2);
          _text_area.Location = new Point(0, _text_area.Location.Y);

          break;
        case ContentAlignment.TopCenter:
          _x = (this.Width / 2) - (m_image.Width / 2);
          _y = 0;
          _text_area.Location = new Point(0, 15);

          break;
        case ContentAlignment.TopLeft:
          _x = 0;
          _y = 0;
          _text_area.Location = new Point(m_image.Width, _text_area.Location.Y);

          break;
        case ContentAlignment.TopRight:
          _x = this.Width - m_image.Width;
          _y = 0;
          _text_area.Location = new Point(0, _text_area.Location.Y);

          break;
      }
      
      if (this.Enabled)
      {
        if (this.Checked)
        {
          e.Graphics.DrawImageUnscaled(Resources.ResourceImages.checkBoxSelected, _x, _y);
          _image_size = Resources.ResourceImages.checkBoxSelected.Size;
        }
        else
        {
          e.Graphics.DrawImageUnscaled(Resources.ResourceImages.checkBox, _x, _y);
          _image_size = Resources.ResourceImages.checkBox.Size;
        }
      }
      else
      {
        if (this.Checked)
        {
          e.Graphics.DrawImageUnscaled(Resources.ResourceImages.checkBoxSelected_disabled, _x, _y);
          _image_size = Resources.ResourceImages.checkBoxSelected_disabled.Size;
        }
        else
        {
          e.Graphics.DrawImageUnscaled(Resources.ResourceImages.checkBox_disabled, _x, _y);
          _image_size = Resources.ResourceImages.checkBox_disabled.Size;
        }
      }

      // Set text alignment     
      _format_flags = TextFormatFlags.WordBreak;

      if (this.RightToLeft == System.Windows.Forms.RightToLeft.Yes)
      {
        _format_flags = _format_flags | TextFormatFlags.RightToLeft;
      }

      if (TextAlign == ContentAlignment.TopLeft ||
        TextAlign == ContentAlignment.TopCenter ||
        TextAlign == ContentAlignment.TopRight)
      {
        _format_flags = _format_flags | TextFormatFlags.Top;
      }
      else if (TextAlign == ContentAlignment.BottomLeft ||
          TextAlign == ContentAlignment.BottomCenter ||
          TextAlign == ContentAlignment.BottomRight)
      {
        _format_flags = _format_flags | TextFormatFlags.Bottom;
      }
      else if (TextAlign == ContentAlignment.MiddleLeft ||
          TextAlign == ContentAlignment.MiddleCenter ||
          TextAlign == ContentAlignment.MiddleRight)
      {
        _format_flags = _format_flags | TextFormatFlags.VerticalCenter;
      }

      if (TextAlign == ContentAlignment.TopLeft ||
       TextAlign == ContentAlignment.BottomLeft ||
       TextAlign == ContentAlignment.MiddleLeft)
      {
        _format_flags = _format_flags | TextFormatFlags.Left;
      }
      else if (TextAlign == ContentAlignment.TopCenter ||
       TextAlign == ContentAlignment.BottomCenter ||
       TextAlign == ContentAlignment.MiddleCenter)
      {
        _format_flags = _format_flags | TextFormatFlags.HorizontalCenter;
      }
      else if (TextAlign == ContentAlignment.TopRight ||
       TextAlign == ContentAlignment.BottomRight ||
       TextAlign == ContentAlignment.MiddleRight)
      {
        _format_flags = _format_flags | TextFormatFlags.Right;
      }

      // Paint the text
      TextRenderer.DrawText(e.Graphics, Text, Font, _text_area, ForeColor, Color.Transparent, _format_flags);
    }

  }
}
