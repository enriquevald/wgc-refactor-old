﻿////------------------------------------------------------------------------------
//// Copyright © 2015-2020 Win Systems International.
////------------------------------------------------------------------------------
//// 
////   MODULE NAME: uc_tabControl.cs
//// 
////   DESCRIPTION: Implements the uc_tabControl user control
////                Class: uc_tabControl
////
////        AUTHOR: José Martínez
////                David Hernández
//// 
//// CREATION DATE: 04-NOV-2015
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 04-NOV-2015 JML     First release.
//// 04-NOV-2015 DHA     First release.
////------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.ComponentModel;
using WSI.Cashier.Controls;
using System.Security.Permissions;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows.Forms.VisualStyles;
using System.Drawing.Imaging;

namespace WSI.Cashier.Controls
{
  public class uc_round_tab_control : TabControl
  {

    #region	Construction

    public uc_round_tab_control()
    {

      this.SetStyle(ControlStyles.UserPaint | ControlStyles.AllPaintingInWmPaint | ControlStyles.Opaque | ControlStyles.ResizeRedraw, true);

      this._BackBuffer = new Bitmap(this.Width, this.Height);
      this._BackBufferGraphics = Graphics.FromImage(this._BackBuffer);
      this._TabBuffer = new Bitmap(this.Width, this.Height);
      this._TabBufferGraphics = Graphics.FromImage(this._TabBuffer);

      this.DisplayStyle = TabStyle.Rounded;

      CashierStyle.ControlStyle.InitStyle(this);

    }

    protected override void OnCreateControl()
    {
      base.OnCreateControl();
      this.OnFontChanged(EventArgs.Empty);

      CashierStyle.ControlStyle.InitStyle(this);
    }


    protected override CreateParams CreateParams
    {
      [SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.UnmanagedCode)]
      get
      {
        CreateParams cp = base.CreateParams;
        if (this.RightToLeftLayout)
          cp.ExStyle = cp.ExStyle | NativeMethods.WS_EX_LAYOUTRTL | NativeMethods.WS_EX_NOINHERITLAYOUT;
        return cp;
      }
    }

    protected override void Dispose(bool disposing)
    {
      base.Dispose(disposing);
      if (disposing)
      {
        if (this._BackImage != null)
        {
          this._BackImage.Dispose();
        }
        if (this._BackBufferGraphics != null)
        {
          this._BackBufferGraphics.Dispose();
        }
        if (this._BackBuffer != null)
        {
          this._BackBuffer.Dispose();
        }
        if (this._TabBufferGraphics != null)
        {
          this._TabBufferGraphics.Dispose();
        }
        if (this._TabBuffer != null)
        {
          this._TabBuffer.Dispose();
        }

        if (this._StyleProvider != null)
        {
          this._StyleProvider.Dispose();
        }
      }
    }

    #endregion

    #region Private variables

    private Bitmap _BackImage;
    private Bitmap _BackBuffer;
    private Graphics _BackBufferGraphics;
    private Bitmap _TabBuffer;
    private Graphics _TabBufferGraphics;

    private int _oldValue;
    private Point _dragStartPosition = Point.Empty;

    private TabStyle _Style;
    private TabStyleProvider _StyleProvider;

    private List<TabPage> _TabPages;

    #endregion

    #region Public properties

    [Category("Appearance"), DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
    public TabStyleProvider DisplayStyleProvider
    {
      get
      {
        if (this._StyleProvider == null)
        {
          this.DisplayStyle = TabStyle.Rounded;
        }

        return this._StyleProvider;
      }
      set
      {
        this._StyleProvider = value;
      }
    }

    [Category("Appearance"), DefaultValue(typeof(TabStyle), "Default"), RefreshProperties(RefreshProperties.All)]
    public TabStyle DisplayStyle
    {
      get { return this._Style; }
      set
      {
        if (this._Style != value)
        {
          this._Style = value;
          this._StyleProvider = TabStyleProvider.CreateProvider(this);
          this.Invalidate();
        }
      }
    }

    [Category("Appearance"), RefreshProperties(RefreshProperties.All)]
    public new bool Multiline
    {
      get
      {
        return base.Multiline;
      }
      set
      {
        base.Multiline = value;
        this.Invalidate();
      }
    }

    //	Hide the Padding attribute so it can not be changed
    //	We are handling this on the Style Provider
    [Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
    public new Point Padding
    {
      get { return this.DisplayStyleProvider.Padding; }
      set
      {
        this.DisplayStyleProvider.Padding = value;
      }
    }

    public override bool RightToLeftLayout
    {
      get { return base.RightToLeftLayout; }
      set
      {
        base.RightToLeftLayout = value;
        this.UpdateStyles();
      }
    }


    //	Hide the HotTrack attribute so it can not be changed
    //	We are handling this on the Style Provider
    [Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
    public new bool HotTrack
    {
      get { return this.DisplayStyleProvider.HotTrack; }
      set
      {
        this.DisplayStyleProvider.HotTrack = value;
      }
    }

    [Category("Appearance")]
    public new TabAlignment Alignment
    {
      get { return base.Alignment; }
      set
      {
        base.Alignment = value;
        switch (value)
        {
          case TabAlignment.Top:
          case TabAlignment.Bottom:
            this.Multiline = false;
            break;
          case TabAlignment.Left:
          case TabAlignment.Right:
            this.Multiline = true;
            break;
        }

      }
    }

    //	Hide the Appearance attribute so it can not be changed
    //	We don't want it as we are doing all the painting.
    [Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "value")]
    public new TabAppearance Appearance
    {
      get
      {
        return base.Appearance;
      }
      set
      {
        //	Don't permit setting to other appearances as we are doing all the painting
        base.Appearance = TabAppearance.Normal;
      }
    }

    public override Rectangle DisplayRectangle
    {
      get
      {
        int tabStripHeight = 0;
        int itemHeight = 0;

        if (this.Alignment <= TabAlignment.Bottom)
        {
          itemHeight = this.ItemSize.Height;
        }
        else
        {
          itemHeight = this.ItemSize.Width;
        }

        tabStripHeight = 5 + (itemHeight * this.RowCount);

        Rectangle rect = new Rectangle(4, tabStripHeight, Width - 8, Height - tabStripHeight - 4);
        switch (this.Alignment)
        {
          case TabAlignment.Top:
            rect = new Rectangle(4, tabStripHeight, Width - 8, Height - tabStripHeight - 4);
            break;
          case TabAlignment.Bottom:
            rect = new Rectangle(4, 4, Width - 8, Height - tabStripHeight - 4);
            break;
          case TabAlignment.Left:
            rect = new Rectangle(tabStripHeight, 4, Width - tabStripHeight - 4, Height - 8);
            break;
          case TabAlignment.Right:
            rect = new Rectangle(4, 4, Width - tabStripHeight - 4, Height - 8);
            break;
        }
        return rect;
      }
    }

    [Browsable(false)]
    public int ActiveIndex
    {
      get
      {
        NativeMethods.TCHITTESTINFO hitTestInfo = new NativeMethods.TCHITTESTINFO(this.PointToClient(Control.MousePosition));
        int index = NativeMethods.SendMessage(this.Handle, NativeMethods.TCM_HITTEST, IntPtr.Zero, NativeMethods.ToIntPtr(hitTestInfo)).ToInt32();
        if (index == -1)
        {
          return -1;
        }
        else
        {
          if (this.TabPages[index].Enabled)
          {
            return index;
          }
          else
          {
            return -1;
          }
        }
      }
    }

    [Browsable(false)]
    public TabPage ActiveTab
    {
      get
      {
        int activeIndex = this.ActiveIndex;
        if (activeIndex > -1)
        {
          return this.TabPages[activeIndex];
        }
        else
        {
          return null;
        }
      }
    }

    #endregion

    #region	Extension methods

    public void HideTab(TabPage page)
    {
      if (page != null && this.TabPages.Contains(page))
      {
        this.BackupTabPages();
        this.TabPages.Remove(page);
      }
    }

    public void HideTab(int index)
    {
      if (this.IsValidTabIndex(index))
      {
        this.HideTab(this._TabPages[index]);
      }
    }

    public void HideTab(string key)
    {
      if (this.TabPages.ContainsKey(key))
      {
        this.HideTab(this.TabPages[key]);
      }
    }

    public void ShowTab(TabPage page)
    {
      if (page != null)
      {
        if (this._TabPages != null)
        {
          if (!this.TabPages.Contains(page)
              && this._TabPages.Contains(page))
          {

            //	Get insert point from backup of pages
            int pageIndex = this._TabPages.IndexOf(page);
            if (pageIndex > 0)
            {
              int start = pageIndex - 1;

              //	Check for presence of earlier pages in the visible tabs
              for (int index = start; index >= 0; index--)
              {
                if (this.TabPages.Contains(this._TabPages[index]))
                {

                  //	Set insert point to the right of the last present tab
                  pageIndex = this.TabPages.IndexOf(this._TabPages[index]) + 1;
                  break;
                }
              }
            }

            //	Insert the page, or add to the end
            if ((pageIndex >= 0) && (pageIndex < this.TabPages.Count))
            {
              this.TabPages.Insert(pageIndex, page);
            }
            else
            {
              this.TabPages.Add(page);
            }
          }
        }
        else
        {

          //	If the page is not found at all then just add it
          if (!this.TabPages.Contains(page))
          {
            this.TabPages.Add(page);
          }
        }
      }
    }

    public void ShowTab(int index)
    {
      if (this.IsValidTabIndex(index))
      {
        this.ShowTab(this._TabPages[index]);
      }
    }

    public void ShowTab(string key)
    {
      if (this._TabPages != null)
      {
        TabPage tab = this._TabPages.Find(delegate(TabPage page) { return page.Name.Equals(key, StringComparison.OrdinalIgnoreCase); });
        this.ShowTab(tab);
      }
    }

    private bool IsValidTabIndex(int index)
    {
      this.BackupTabPages();
      return ((index >= 0) && (index < this._TabPages.Count));
    }

    private void BackupTabPages()
    {
      if (this._TabPages == null)
      {
        this._TabPages = new List<TabPage>();
        foreach (TabPage page in this.TabPages)
        {
          this._TabPages.Add(page);
        }
      }
    }

    #endregion

    #region Drag 'n' Drop

    protected override void OnMouseDown(MouseEventArgs e)
    {
      base.OnMouseDown(e);
      if (this.AllowDrop)
      {
        this._dragStartPosition = new Point(e.X, e.Y);
      }
    }

    protected override void OnMouseUp(MouseEventArgs e)
    {
      base.OnMouseUp(e);
      if (this.AllowDrop)
      {
        this._dragStartPosition = Point.Empty;
      }
    }

    //protected override void OnDragOver(DragEventArgs drgevent)
    //{
    //  base.OnDragOver(drgevent);

    //  if (drgevent.Data.GetDataPresent(typeof(TabPage)))
    //  {
    //    drgevent.Effect = DragDropEffects.Move;
    //  }
    //  else
    //  {
    //    drgevent.Effect = DragDropEffects.None;
    //  }
    //}

    //protected override void OnDragDrop(DragEventArgs drgevent)
    //{
    //  base.OnDragDrop(drgevent);
    //  if (drgevent.Data.GetDataPresent(typeof(TabPage)))
    //  {
    //    drgevent.Effect = DragDropEffects.Move;

    //    TabPage dragTab = (TabPage)drgevent.Data.GetData(typeof(TabPage));

    //    if (this.ActiveTab == dragTab)
    //    {
    //      return;
    //    }

    //    //	Capture insert point and adjust for removal of tab
    //    //	We cannot assess this after removal as differeing tab sizes will cause
    //    //	inaccuracies in the activeTab at insert point.
    //    int insertPoint = this.ActiveIndex;
    //    if (dragTab.Parent.Equals(this) && this.TabPages.IndexOf(dragTab) < insertPoint)
    //    {
    //      insertPoint--;
    //    }
    //    if (insertPoint < 0)
    //    {
    //      insertPoint = 0;
    //    }

    //    //	Remove from current position (could be another tabcontrol)
    //    ((TabControl)dragTab.Parent).TabPages.Remove(dragTab);

    //    //	Add to current position
    //    this.TabPages.Insert(insertPoint, dragTab);
    //    this.SelectedTab = dragTab;

    //    //	deal with hidden tab handling?
    //  }
    //}

    //private void StartDragDrop()
    //{
    //  if (!this._dragStartPosition.IsEmpty)
    //  {
    //    TabPage dragTab = this.SelectedTab;
    //    if (dragTab != null)
    //    {
    //      //	Test for movement greater than the drag activation trigger area
    //      Rectangle dragTestRect = new Rectangle(this._dragStartPosition, Size.Empty);
    //      dragTestRect.Inflate(SystemInformation.DragSize);
    //      Point pt = this.PointToClient(Control.MousePosition);
    //      if (!dragTestRect.Contains(pt))
    //      {
    //        this.DoDragDrop(dragTab, DragDropEffects.All);
    //        this._dragStartPosition = Point.Empty;
    //      }
    //    }
    //  }
    //}

    #endregion

    #region Events

    [Category("Action")]
    public event ScrollEventHandler HScroll;
    //[Category("Action")]
    //public event EventHandler<TabControlEventArgs> TabImageClick;
    //[Category("Action")]
    //public event EventHandler<TabControlCancelEventArgs> TabClosing;

    #endregion

    #region	Base class event processing

    protected override void OnFontChanged(EventArgs e)
    {
      IntPtr hFont = this.Font.ToHfont();
      NativeMethods.SendMessage(this.Handle, NativeMethods.WM_SETFONT, hFont, (IntPtr)(-1));
      NativeMethods.SendMessage(this.Handle, NativeMethods.WM_FONTCHANGE, IntPtr.Zero, IntPtr.Zero);
      this.UpdateStyles();
      if (this.Visible)
      {
        this.Invalidate();
      }
    }

    protected override void OnResize(EventArgs e)
    {
      //	Recreate the buffer for manual double buffering
      if (this.Width > 0 && this.Height > 0)
      {
        if (this._BackImage != null)
        {
          this._BackImage.Dispose();
          this._BackImage = null;
        }
        if (this._BackBufferGraphics != null)
        {
          this._BackBufferGraphics.Dispose();
        }
        if (this._BackBuffer != null)
        {
          this._BackBuffer.Dispose();
        }

        this._BackBuffer = new Bitmap(this.Width, this.Height);
        this._BackBufferGraphics = Graphics.FromImage(this._BackBuffer);

        if (this._TabBufferGraphics != null)
        {
          this._TabBufferGraphics.Dispose();
        }
        if (this._TabBuffer != null)
        {
          this._TabBuffer.Dispose();
        }

        this._TabBuffer = new Bitmap(this.Width, this.Height);
        this._TabBufferGraphics = Graphics.FromImage(this._TabBuffer);

        if (this._BackImage != null)
        {
          this._BackImage.Dispose();
          this._BackImage = null;
        }

      }
      base.OnResize(e);
    }

    protected override void OnParentBackColorChanged(EventArgs e)
    {
      if (this._BackImage != null)
      {
        this._BackImage.Dispose();
        this._BackImage = null;
      }
      base.OnParentBackColorChanged(e);
    }

    protected override void OnParentBackgroundImageChanged(EventArgs e)
    {
      if (this._BackImage != null)
      {
        this._BackImage.Dispose();
        this._BackImage = null;
      }
      base.OnParentBackgroundImageChanged(e);
    }

    private void OnParentResize(object sender, EventArgs e)
    {
      if (this.Visible)
      {
        this.Invalidate();
      }
    }

    protected override void OnParentChanged(EventArgs e)
    {
      base.OnParentChanged(e);
      if (this.Parent != null)
      {
        this.Parent.Resize += this.OnParentResize;
      }
    }

    protected override void OnSelecting(TabControlCancelEventArgs e)
    {
      base.OnSelecting(e);

      //	Do not allow selecting of disabled tabs
      if (e.Action == TabControlAction.Selecting && e.TabPage != null && !e.TabPage.Enabled)
      {
        e.Cancel = true;
      }
    }

    protected override void OnMove(EventArgs e)
    {
      if (this.Width > 0 && this.Height > 0)
      {
        if (this._BackImage != null)
        {
          this._BackImage.Dispose();
          this._BackImage = null;
        }
      }
      base.OnMove(e);
      this.Invalidate();
    }

    protected override void OnControlAdded(ControlEventArgs e)
    {
      base.OnControlAdded(e);
      if (this.Visible)
      {
        this.Invalidate();
      }
    }

    protected override void OnControlRemoved(ControlEventArgs e)
    {
      base.OnControlRemoved(e);
      if (this.Visible)
      {
        this.Invalidate();
      }
    }

    [UIPermission(SecurityAction.LinkDemand, Window = UIPermissionWindow.AllWindows)]
    protected override bool ProcessMnemonic(char charCode)
    {
      foreach (TabPage page in this.TabPages)
      {
        if (IsMnemonic(charCode, page.Text))
        {
          this.SelectedTab = page;
          return true;
        }
      }
      return base.ProcessMnemonic(charCode);
    }

    protected override void OnSelectedIndexChanged(EventArgs e)
    {
      base.OnSelectedIndexChanged(e);
    }

    [SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.UnmanagedCode)]
    [System.Diagnostics.DebuggerStepThrough()]
    protected override void WndProc(ref Message m)
    {

      switch (m.Msg)
      {
        case NativeMethods.WM_HSCROLL:

          //	Raise the scroll event when the scroller is scrolled
          base.WndProc(ref m);
          this.OnHScroll(new ScrollEventArgs(((ScrollEventType)NativeMethods.LoWord(m.WParam)), _oldValue, NativeMethods.HiWord(m.WParam), ScrollOrientation.HorizontalScroll));
          break;
        //				case NativeMethods.WM_PAINT:
        //					
        //					//	Handle painting ourselves rather than call the base OnPaint.
        //					CustomPaint(ref m);
        //					break;

        default:
          base.WndProc(ref m);
          break;

      }
    }

    protected virtual void OnHScroll(ScrollEventArgs e)
    {
      //	repaint the moved tabs
      this.Invalidate();

      //	Raise the event
      if (this.HScroll != null)
      {
        this.HScroll(this, e);
      }

      if (e.Type == ScrollEventType.EndScroll)
      {
        this._oldValue = e.NewValue;
      }
    }

    #endregion

    #region	Basic drawing methods

    //		private void CustomPaint(ref Message m){
    //			NativeMethods.PAINTSTRUCT paintStruct = new NativeMethods.PAINTSTRUCT();
    //			NativeMethods.BeginPaint(m.HWnd, ref paintStruct);
    //			using (Graphics screenGraphics = this.CreateGraphics()) {
    //				this.CustomPaint(screenGraphics);
    //			}
    //			NativeMethods.EndPaint(m.HWnd, ref paintStruct);
    //		}

    protected override void OnPaint(PaintEventArgs e)
    {
      //	We must always paint the entire area of the tab control
      //if (e.ClipRectangle.Equals(this.ClientRectangle))
      //{
        this.CustomPaint(e.Graphics);
      //}
      //else
      //{
      //  //	it is less intensive to just reinvoke the paint with the whole surface available to draw on.
      //  this.Invalidate();
      //}
    }

    private void CustomPaint(Graphics screenGraphics)
    {

      if (this.Width > 0 && this.Height > 0)
      {
        //if (this._BackImage == null)
        //{
          //	Cached Background Image
          this._BackImage = new Bitmap(this.Width, this.Height);
          Graphics backGraphics = Graphics.FromImage(this._BackImage);
          backGraphics.Clear(Color.Transparent);
          this.PaintTransparentBackground(backGraphics, this.ClientRectangle);
        //}

        this._BackBufferGraphics.Clear(Color.Transparent);
        this._BackBufferGraphics.DrawImageUnscaled(this._BackImage, 0, 0);

        this._TabBufferGraphics.Clear(Color.Transparent);

        if (this.TabCount > 0)
        {

          //	When top or bottom and scrollable we need to clip the sides from painting the tabs.
          //	Left and right are always multiline.
          if (this.Alignment <= TabAlignment.Bottom && !this.Multiline)
          {
            this._TabBufferGraphics.Clip = new Region(new RectangleF(this.ClientRectangle.X + 3, this.ClientRectangle.Y, this.ClientRectangle.Width - 6, this.ClientRectangle.Height));
          }

          //	Draw each tabpage from right to left.  We do it this way to handle
          //	the overlap correctly.
          if (this.Multiline)
          {
            for (int row = 0; row < this.RowCount; row++)
            {
              for (int index = this.TabCount - 1; index >= 0; index--)
              {
                if (index != this.SelectedIndex && (this.RowCount == 1 || this.GetTabRow(index) == row))
                {
                  this.DrawTabPage(index, this._TabBufferGraphics);
                }
              }
            }
          }
          else
          {
            for (int index = this.TabCount - 1; index >= 0; index--)
            {
              if (index != this.SelectedIndex)
              {
                this.DrawTabPage(index, this._TabBufferGraphics);
              }
            }
          }

          //	The selected tab must be drawn last so it appears on top.
          if (this.SelectedIndex > -1)
          {
            this.DrawTabPage(this.SelectedIndex, this._TabBufferGraphics);
          }
        }
        this._TabBufferGraphics.Flush();

        //	Paint the tabs on top of the background

        // Create a new color matrix and set the alpha value to 0.5
        ColorMatrix alphaMatrix = new ColorMatrix();
        alphaMatrix.Matrix00 = alphaMatrix.Matrix11 = alphaMatrix.Matrix22 = alphaMatrix.Matrix44 = 1;
        alphaMatrix.Matrix33 = this._StyleProvider.Opacity;

        // Create a new image attribute object and set the color matrix to
        // the one just created
        using (ImageAttributes alphaAttributes = new ImageAttributes())
        {

          alphaAttributes.SetColorMatrix(alphaMatrix);

          // Draw the original image with the image attributes specified
          this._BackBufferGraphics.DrawImage(this._TabBuffer,
                                             new Rectangle(0, 0, this._TabBuffer.Width, this._TabBuffer.Height),
                                             0, 0, this._TabBuffer.Width, this._TabBuffer.Height, GraphicsUnit.Pixel,
                                             alphaAttributes);
        }

        this._BackBufferGraphics.Flush();

        //	Now paint this to the screen


        //	We want to paint the whole tabstrip and border every time
        //	so that the hot areas update correctly, along with any overlaps

        //	paint the tabs etc.
        if (this.RightToLeftLayout)
        {
          screenGraphics.DrawImageUnscaled(this._BackBuffer, -1, 0);
        }
        else
        {
          screenGraphics.DrawImageUnscaled(this._BackBuffer, 0, 0);
        }
      }
    }

    protected void PaintTransparentBackground(Graphics graphics, Rectangle clipRect)
    {

      if ((this.Parent != null))
      {

        //	Set the cliprect to be relative to the parent
        clipRect.Offset(this.Location);

        //	Save the current state before we do anything.
        GraphicsState state = graphics.Save();

        //	Set the graphicsobject to be relative to the parent
        graphics.TranslateTransform((float)-this.Location.X, (float)-this.Location.Y);
        graphics.SmoothingMode = SmoothingMode.HighSpeed;

        //	Paint the parent
        PaintEventArgs e = new PaintEventArgs(graphics, clipRect);
        try
        {
          this.InvokePaintBackground(this.Parent, e);
          this.InvokePaint(this.Parent, e);
        }
        finally
        {
          //	Restore the graphics state and the clipRect to their original locations
          graphics.Restore(state);
          clipRect.Offset(-this.Location.X, -this.Location.Y);
        }
      }
    }

    private void DrawTabPage(int index, Graphics graphics)
    {
      GraphicsPath _path_page;

      graphics.SmoothingMode = SmoothingMode.HighSpeed;
      _path_page = new GraphicsPath();
      
      //	Get TabPageBorder
      using (GraphicsPath tabPageBorderPath = this.GetTabPageBorder(index, ref _path_page))
      {

        //	Paint the background
        using (Brush fillBrush = this._StyleProvider.GetPageBackgroundBrush(index))
        {
          //graphics.FillPath(fillBrush, tabPageBorderPath);
          graphics.FillPath(fillBrush, tabPageBorderPath);
        }

        //	Paint the tab
        this._StyleProvider.PaintTab(index, graphics);

        //	Draw the text
        this.DrawTabText(index, graphics);

        //	Paint the border
        this.DrawTabBorder(tabPageBorderPath, index, graphics);

        graphics.DrawPath(new Pen(this.DisplayStyleProvider.TabColor, 2), _path_page);

      }
    }

    private void DrawTabBorder(GraphicsPath path, int index, Graphics graphics)
    {
      graphics.SmoothingMode = SmoothingMode.HighQuality;
      Color borderColor;
      if (index == this.SelectedIndex)
      {
        borderColor = this._StyleProvider.BorderColorSelected;
      }
      else if (this._StyleProvider.HotTrack && index == this.ActiveIndex)
      {
        borderColor = this._StyleProvider.BorderColorHot;
      }
      else
      {
        borderColor = this._StyleProvider.BorderColor;
      }

      //using (Pen borderPen = new Pen(this._StyleProvider.TabColor))
      using (Pen borderPen = new Pen(CashierStyle.Colors.m_colors_dic[CashierStyle.Colors.ENUM_COLORS.HEX_78A3C2]))
      {
        graphics.DrawPath(borderPen, path);
      }
    }

    private void DrawTabText(int index, Graphics graphics)
    {
      graphics.SmoothingMode = SmoothingMode.HighQuality;
      graphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;
      Rectangle tabBounds = this.GetTabTextRect(index);

      if (this.SelectedIndex == index)
      {
        using (Brush textBrush = new SolidBrush(this._StyleProvider.TextColorSelected))
        {
          graphics.DrawString(this.TabPages[index].Text, this.Font, textBrush, tabBounds, this.GetStringFormat());
        }
      }
      else
      {
        if (this.TabPages[index].Enabled)
        {
          using (Brush textBrush = new SolidBrush(this._StyleProvider.TextColor))
          {
            graphics.DrawString(this.TabPages[index].Text, this.Font, textBrush, tabBounds, this.GetStringFormat());
          }
        }
        else
        {
          using (Brush textBrush = new SolidBrush(this._StyleProvider.TextColorDisabled))
          {
            graphics.DrawString(this.TabPages[index].Text, this.Font, textBrush, tabBounds, this.GetStringFormat());
          }
        }
      }
    }

    #endregion

    #region String formatting

    private StringFormat GetStringFormat()
    {
      StringFormat format = null;

      //	Rotate Text by 90 degrees for left and right tabs
      switch (this.Alignment)
      {
        case TabAlignment.Top:
        case TabAlignment.Bottom:
          format = new StringFormat();
          break;
        case TabAlignment.Left:
        case TabAlignment.Right:
          format = new StringFormat(StringFormatFlags.DirectionVertical);
          break;
      }
      format.Alignment = StringAlignment.Center;
      format.LineAlignment = StringAlignment.Center;
      if (this.FindForm() != null && this.FindForm().KeyPreview)
      {
        format.HotkeyPrefix = System.Drawing.Text.HotkeyPrefix.Show;
      }
      else
      {
        format.HotkeyPrefix = System.Drawing.Text.HotkeyPrefix.Hide;
      }
      if (this.RightToLeft == RightToLeft.Yes)
      {
        format.FormatFlags = format.FormatFlags | StringFormatFlags.DirectionRightToLeft;
      }
      return format;
    }

    #endregion

    #region Tab borders and bounds properties

    private GraphicsPath GetTabPageBorder(int index, ref GraphicsPath PathPage)
    {
      Boolean _is_first_tab;
      Boolean _is_last_tab;
      TabStyleRoundedProvider.TAB_POSITION _tab_position;

      _tab_position = TabStyleRoundedProvider.TAB_POSITION.OTHER;

      _is_first_tab = this.IsFirstTabInRow(index);
      _is_last_tab = this.IsLastTabInRow(index);
      if (_is_first_tab && _is_last_tab)
      {
        _tab_position = TabStyleRoundedProvider.TAB_POSITION.FIRST_AND_LAST;
      }
      else if(_is_first_tab)
      {
        _tab_position = TabStyleRoundedProvider.TAB_POSITION.FIRST;
      }
      else if(_is_last_tab)
      {
        _tab_position = TabStyleRoundedProvider.TAB_POSITION.LAST;
      }

      GraphicsPath path = new GraphicsPath();
      Rectangle pageBounds = this.GetPageBounds(index);
      Rectangle tabBounds = this._StyleProvider.GetTabRect(index);
      this._StyleProvider.AddTabBorder(path, tabBounds, _tab_position);
      this.AddPageBorder(path, pageBounds, tabBounds, ref PathPage);

      path.CloseFigure();
      return path;
    }

    public Rectangle GetPageBounds(int index)
    {
      Rectangle pageBounds = this.TabPages[index].Bounds;
      pageBounds.Width += 1;
      // CAMBIADO DHA
      pageBounds.Height += 10;
      pageBounds.X -= 1;
      pageBounds.Y -= 1;

      //if (pageBounds.Bottom > this.Height - 4)
      //{
      //  pageBounds.Height -= (pageBounds.Bottom - this.Height + 4);
      //}

      return pageBounds;
    }

    public GraphicsPath GetPagePath(int index)
    {
      Rectangle pageBounds = this.TabPages[index].Bounds;
      pageBounds.Width += 1;
      pageBounds.Height += 1;
      pageBounds.X -= 1;
      pageBounds.Y -= 1;

      if (pageBounds.Bottom > this.Height - 4)
      {
        pageBounds.Height -= (pageBounds.Bottom - this.Height + 4);
      }
      //return pageBounds;

      GraphicsPath _path;

      _path = new GraphicsPath();

      _path.AddLine(pageBounds.X, pageBounds.Y, pageBounds.X, pageBounds.Height - 10);
      _path.AddLine(pageBounds.X, pageBounds.Height - 10, pageBounds.X + 10, pageBounds.Height - 10);
      _path.AddLine(pageBounds.X + 10, pageBounds.Height - 10, pageBounds.Width, pageBounds.Height);
      _path.AddLine(pageBounds.Width, pageBounds.Height, pageBounds.Width, pageBounds.Y);

      return _path;
    }

    private Rectangle GetTabTextRect(int index)
    {
      Rectangle textRect = new Rectangle();
      using (GraphicsPath path = this._StyleProvider.GetTabBorder(index))
      {
        RectangleF tabBounds = path.GetBounds();

        textRect = new Rectangle((int)tabBounds.X, (int)tabBounds.Y, (int)tabBounds.Width, (int)tabBounds.Height);

        //	Make it shorter or thinner to fit the height or width because of the padding added to the tab for painting
        switch (this.Alignment)
        {
          case TabAlignment.Top:
            textRect.Y += 4;
            textRect.Height -= 6;
            break;
          case TabAlignment.Bottom:
            textRect.Y += 2;
            textRect.Height -= 6;
            break;
          case TabAlignment.Left:
            textRect.X += 4;
            textRect.Width -= 6;
            break;
          case TabAlignment.Right:
            textRect.X += 2;
            textRect.Width -= 6;
            break;
        }

        //	Ensure it fits inside the path at the centre line
        if (this.Alignment <= TabAlignment.Bottom)
        {
          while (!path.IsVisible(textRect.Right, textRect.Y) && textRect.Width > 0)
          {
            textRect.Width -= 1;
          }
          while (!path.IsVisible(textRect.X, textRect.Y) && textRect.Width > 0)
          {
            textRect.X += 1;
            textRect.Width -= 1;
          }
        }
        else
        {
          while (!path.IsVisible(textRect.X, textRect.Bottom) && textRect.Height > 0)
          {
            textRect.Height -= 1;
          }
          while (!path.IsVisible(textRect.X, textRect.Y) && textRect.Height > 0)
          {
            textRect.Y += 1;
            textRect.Height -= 1;
          }
        }
      }
      return textRect;
    }

    public int GetTabRow(int index)
    {
      //	All calculations will use this rect as the base point
      //	because the itemsize does not return the correct width.
      Rectangle rect = this.GetTabRect(index);

      int row = -1;

      switch (this.Alignment)
      {
        case TabAlignment.Top:
          row = (rect.Y - 2) / rect.Height;
          break;
        case TabAlignment.Bottom:
          row = ((this.Height - rect.Y - 2) / rect.Height) - 1;
          break;
        case TabAlignment.Left:
          row = (rect.X - 2) / rect.Width;
          break;
        case TabAlignment.Right:
          row = ((this.Width - rect.X - 2) / rect.Width) - 1;
          break;
      }
      return row;
    }

    public Point GetTabPosition(int index)
    {

      //	If we are not multiline then the column is the index and the row is 0.
      if (!this.Multiline)
      {
        return new Point(0, index);
      }

      //	If there is only one row then the column is the index
      if (this.RowCount == 1)
      {
        return new Point(0, index);
      }

      //	We are in a true multi-row scenario
      int row = this.GetTabRow(index);
      Rectangle rect = this.GetTabRect(index);
      int column = -1;

      //	Scan from left to right along rows, skipping to next row if it is not the one we want.
      for (int testIndex = 0; testIndex < this.TabCount; testIndex++)
      {
        Rectangle testRect = this.GetTabRect(testIndex);
        if (this.Alignment <= TabAlignment.Bottom)
        {
          if (testRect.Y == rect.Y)
          {
            column += 1;
          }
        }
        else
        {
          if (testRect.X == rect.X)
          {
            column += 1;
          }
        }

        if (testRect.Location.Equals(rect.Location))
        {
          return new Point(row, column);
        }
      }

      return new Point(0, 0);
    }

    public bool IsFirstTabInRow(int index)
    {
      if (index < 0)
      {
        return false;
      }
      bool firstTabinRow = (index == 0);
      if (!firstTabinRow)
      {
        if (this.Alignment <= TabAlignment.Bottom)
        {
          if (this.GetTabRect(index).X == 2)
          {
            firstTabinRow = true;
          }
        }
        else
        {
          if (this.GetTabRect(index).Y == 2)
          {
            firstTabinRow = true;
          }
        }
      }
      return firstTabinRow;
    }

    public bool IsLastTabInRow(int index)
    {
      if (index < 0)
      {
        return false;
      }
      bool LastTabinRow = (index == this.TabCount - 1);
      //if (!LastTabinRow)
      //{
      //  if (this.Alignment <= TabAlignment.Bottom)
      //  {
      //    if (this.GetTabRect(index).X == 2)
      //    {
      //      LastTabinRow = true;
      //    }
      //  }
      //  else
      //  {
      //    if (this.GetTabRect(index).Y == 2)
      //    {
      //      LastTabinRow = true;
      //    }
      //  }
      //}
      return LastTabinRow;
    }

    private void AddPageBorder(GraphicsPath path, Rectangle pageBounds, Rectangle tabBounds, ref GraphicsPath pathPage)
    {
      pathPage = new GraphicsPath();

      switch (this.Alignment)
      {
        case TabAlignment.Top:
          path.AddLine(pageBounds.X, pageBounds.Y, pageBounds.Right, pageBounds.Y);
          pathPage.AddLine(pageBounds.X, pageBounds.Y, pageBounds.Right, pageBounds.Y);
          path.AddArc(pageBounds.Right - 10, pageBounds.Bottom - 13, 10, 6, 0, 90);
          pathPage.AddArc(pageBounds.Right - 10, pageBounds.Bottom - 13, 10, 6, 0, 90);
          path.AddArc(pageBounds.X, pageBounds.Bottom - 13, 10, 6, 90, 90);
          pathPage.AddArc(pageBounds.X, pageBounds.Bottom - 13, 10, 6, 90, 90);

          pathPage.AddLine(pageBounds.X, pageBounds.Bottom - 13, pageBounds.X, pageBounds.Y);
          
          break;
        //case TabAlignment.Bottom:
        //  path.AddLine(tabBounds.X, pageBounds.Bottom, pageBounds.X, pageBounds.Bottom);
        //  path.AddLine(pageBounds.X, pageBounds.Bottom, pageBounds.X, pageBounds.Y);
        //  path.AddLine(pageBounds.X, pageBounds.Y, pageBounds.Right, pageBounds.Y);
        //  path.AddLine(pageBounds.Right, pageBounds.Y, pageBounds.Right, pageBounds.Bottom);
        //  path.AddLine(pageBounds.Right, pageBounds.Bottom, tabBounds.Right, pageBounds.Bottom);
        //  break;
        //case TabAlignment.Left:
        //  path.AddLine(pageBounds.X, tabBounds.Y, pageBounds.X, pageBounds.Y);
        //  path.AddLine(pageBounds.X, pageBounds.Y, pageBounds.Right, pageBounds.Y);
        //  path.AddLine(pageBounds.Right, pageBounds.Y, pageBounds.Right, pageBounds.Bottom);
        //  path.AddLine(pageBounds.Right, pageBounds.Bottom, pageBounds.X, pageBounds.Bottom);
        //  path.AddLine(pageBounds.X, pageBounds.Bottom, pageBounds.X, tabBounds.Bottom);
        //  break;
        //case TabAlignment.Right:
        //  path.AddLine(pageBounds.Right, tabBounds.Bottom, pageBounds.Right, pageBounds.Bottom);
        //  path.AddLine(pageBounds.Right, pageBounds.Bottom, pageBounds.X, pageBounds.Bottom);
        //  path.AddLine(pageBounds.X, pageBounds.Bottom, pageBounds.X, pageBounds.Y);
        //  path.AddLine(pageBounds.X, pageBounds.Y, pageBounds.Right, pageBounds.Y);
        //  path.AddLine(pageBounds.Right, pageBounds.Y, pageBounds.Right, tabBounds.Y);
        //  break;
      }
    }

    public new Point MousePosition
    {
      get
      {
        Point loc = this.PointToClient(Control.MousePosition);
        if (this.RightToLeftLayout)
        {
          loc.X = (this.Width - loc.X);
        }
        return loc;
      }
    }

    #endregion

  }

  [System.ComponentModel.ToolboxItem(false)]
  public abstract class TabStyleProvider : Component
  {
    #region Constructor

    protected TabStyleProvider(uc_round_tab_control tabControl)
    {
      this._TabControl = tabControl;

      this._BorderColor = Color.Empty;
      this._BorderColorSelected = Color.Empty;

      if (this._TabControl.RightToLeftLayout)
      {
        this._ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
      }
      else
      {
        this._ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      }

      this.HotTrack = true;

      //	Must set after the _Overlap as this is used in the calculations of the actual padding
      this.Padding = new Point(6, 3);
    }

    #endregion

    #region Factory Methods

    public static TabStyleProvider CreateProvider(uc_round_tab_control tabControl)
    {
      TabStyleProvider provider;

      //	Depending on the display style of the tabControl generate an appropriate provider.
      provider = new TabStyleRoundedProvider(tabControl);

      provider._Style = tabControl.DisplayStyle;
      return provider;
    }

    #endregion

    #region	Protected variables

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1051:DoNotDeclareVisibleInstanceFields")]
    protected uc_round_tab_control _TabControl;

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1051:DoNotDeclareVisibleInstanceFields")]
    protected Point _Padding;
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1051:DoNotDeclareVisibleInstanceFields")]
    protected bool _HotTrack;
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1051:DoNotDeclareVisibleInstanceFields")]
    protected TabStyle _Style = TabStyle.Rounded;

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1051:DoNotDeclareVisibleInstanceFields")]
    protected System.Drawing.ContentAlignment _ImageAlign;
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1051:DoNotDeclareVisibleInstanceFields")]
    protected int _Radius = 1;
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1051:DoNotDeclareVisibleInstanceFields")]
    protected int _Overlap;
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1051:DoNotDeclareVisibleInstanceFields")]
    protected bool _FocusTrack;
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1051:DoNotDeclareVisibleInstanceFields")]
    protected float _Opacity = 1;
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1051:DoNotDeclareVisibleInstanceFields")]
    protected bool _ShowTabCloser;

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1051:DoNotDeclareVisibleInstanceFields")]
    protected Color _BorderColorSelected = Color.Empty;
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1051:DoNotDeclareVisibleInstanceFields")]
    protected Color _BorderColor = Color.Empty;
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1051:DoNotDeclareVisibleInstanceFields")]
    protected Color _BorderColorHot = Color.Empty;
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1051:DoNotDeclareVisibleInstanceFields")]

    protected Color _TextColor = Color.Empty;
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1051:DoNotDeclareVisibleInstanceFields")]
    protected Color _TextColorSelected = Color.Empty;
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1051:DoNotDeclareVisibleInstanceFields")]
    protected Color _TextColorDisabled = Color.Empty;

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1051:DoNotDeclareVisibleInstanceFields")]
    protected Color m_tab_color_selected = Color.Empty;
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1051:DoNotDeclareVisibleInstanceFields")]
    protected Color m_tab_color_disabled = Color.Empty;
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1051:DoNotDeclareVisibleInstanceFields")]
    protected Color m_tab_color_header = Color.Empty;
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1051:DoNotDeclareVisibleInstanceFields")]
    protected Color m_tab_color = Color.Empty;



    #endregion

    #region overridable Methods

    public abstract void AddTabBorder(GraphicsPath path, Rectangle tabBounds, TabStyleRoundedProvider.TAB_POSITION TabPosition);

    public virtual Rectangle GetTabRect(int index)
    {

      if (index < 0)
      {
        return new Rectangle();
      }
      Rectangle tabBounds = this._TabControl.GetTabRect(index);
      if (this._TabControl.RightToLeftLayout)
      {
        tabBounds.X = this._TabControl.Width - tabBounds.Right;
      }
      bool firstTabinRow = this._TabControl.IsFirstTabInRow(index);

      //	Expand to overlap the tabpage
      switch (this._TabControl.Alignment)
      {
        case TabAlignment.Top:
          tabBounds.Height += 2;
          break;
        case TabAlignment.Bottom:
          tabBounds.Height += 2;
          tabBounds.Y -= 2;
          break;
        case TabAlignment.Left:
          tabBounds.Width += 2;
          break;
        case TabAlignment.Right:
          tabBounds.X -= 2;
          tabBounds.Width += 2;
          break;
      }


      //	Greate Overlap unless first tab in the row to align with tabpage
      if ((!firstTabinRow || this._TabControl.RightToLeftLayout) && this._Overlap > 0)
      {
        if (this._TabControl.Alignment <= TabAlignment.Bottom)
        {
          tabBounds.X -= this._Overlap;
          tabBounds.Width += this._Overlap;
        }
        else
        {
          tabBounds.Y -= this._Overlap;
          tabBounds.Height += this._Overlap;
        }
      }

      //	Adjust first tab in the row to align with tabpage
      this.EnsureFirstTabIsInView(ref tabBounds, index);

      return tabBounds;
    }


    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId = "0#")]
    protected virtual void EnsureFirstTabIsInView(ref Rectangle tabBounds, int index)
    {
      //	Adjust first tab in the row to align with tabpage
      //	Make sure we only reposition visible tabs, as we may have scrolled out of view.

      bool firstTabinRow = this._TabControl.IsFirstTabInRow(index);

      if (firstTabinRow)
      {
        if (this._TabControl.Alignment <= TabAlignment.Bottom)
        {
          if (this._TabControl.RightToLeftLayout)
          {
            if (tabBounds.Left < this._TabControl.Right)
            {
              int tabPageRight = this._TabControl.GetPageBounds(index).Right;
              if (tabBounds.Right > tabPageRight)
              {
                tabBounds.Width -= (tabBounds.Right - tabPageRight);
              }
            }
          }
          else
          {
            if (tabBounds.Right > 0)
            {
              int tabPageX = this._TabControl.GetPageBounds(index).X;
              if (tabBounds.X < tabPageX)
              {
                tabBounds.Width -= (tabPageX - tabBounds.X);
                tabBounds.X = tabPageX;
              }
            }
          }
        }
        else
        {
          if (this._TabControl.RightToLeftLayout)
          {
            if (tabBounds.Top < this._TabControl.Bottom)
            {
              int tabPageBottom = this._TabControl.GetPageBounds(index).Bottom;
              if (tabBounds.Bottom > tabPageBottom)
              {
                tabBounds.Height -= (tabBounds.Bottom - tabPageBottom);
              }
            }
          }
          else
          {
            if (tabBounds.Bottom > 0)
            {
              int tabPageY = this._TabControl.GetPageBounds(index).Location.Y;
              if (tabBounds.Y < tabPageY)
              {
                tabBounds.Height -= (tabPageY - tabBounds.Y);
                tabBounds.Y = tabPageY;
              }
            }
          }
        }
      }
    }

    protected virtual Brush GetTabBackgroundBrush(int index)
    {
      Color _color_tab;
      
      _color_tab = m_tab_color_header;

      if (this._TabControl.SelectedIndex == index)
      {
        _color_tab = m_tab_color_selected;
      }
      else if (!this._TabControl.TabPages[index].Enabled)
      {
        _color_tab = m_tab_color_disabled;
      }

      return new SolidBrush(_color_tab);
    }

    #endregion

    #region	Base Properties

    [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    public TabStyle DisplayStyle
    {
      get { return this._Style; }
      set { this._Style = value; }
    }

    //[Category("Appearance")]
    //public System.Drawing.ContentAlignment ImageAlign
    //{
    //  get { return this._ImageAlign; }
    //  set
    //  {
    //    this._ImageAlign = value;
    //    this._TabControl.Invalidate();
    //  }
    //}

    [Category("Appearance")]
    public Point Padding
    {
      get { return this._Padding; }
      set
      {
        this._Padding = value;
        //	This line will trigger the handle to recreate, therefore invalidating the control
        if (this._ShowTabCloser)
        {
          if (value.X + (int)(this._Radius / 2) < -6)
          {
            ((TabControl)this._TabControl).Padding = new Point(0, value.Y);
          }
          else
          {
            ((TabControl)this._TabControl).Padding = new Point(value.X + (int)(this._Radius / 2) + 6, value.Y);
          }
        }
        else
        {
          if (value.X + (int)(this._Radius / 2) < 1)
          {
            ((TabControl)this._TabControl).Padding = new Point(0, value.Y);
          }
          else
          {
            ((TabControl)this._TabControl).Padding = new Point(value.X + (int)(this._Radius / 2) - 1, value.Y);
          }
        }
      }
    }

    [Category("Appearance"), DefaultValue(1), Browsable(true)]
    public int Radius
    {
      get { return this._Radius; }
      set
      {
        if (value < 1)
        {
          throw new ArgumentException("The radius must be greater than 1", "value");
        }
        this._Radius = value;
        //	Adjust padding
        this.Padding = this._Padding;
      }
    }

    [Category("Appearance")]
    public int Overlap
    {
      get { return this._Overlap; }
      set
      {
        if (value < 0)
        {
          throw new ArgumentException("The tabs cannot have a negative overlap", "value");
        }
        this._Overlap = value;

      }
    }

    [Category("Appearance")]
    public bool FocusTrack
    {
      get { return this._FocusTrack; }
      set
      {
        this._FocusTrack = value;
        this._TabControl.Invalidate();
      }
    }

    [Category("Appearance")]
    public bool HotTrack
    {
      get { return this._HotTrack; }
      set
      {
        this._HotTrack = value;
        ((TabControl)this._TabControl).HotTrack = value;
      }
    }

    [Category("Appearance")]
    public float Opacity
    {
      get { return this._Opacity; }
      set
      {
        if (value < 0)
        {
          throw new ArgumentException("The opacity must be between 0 and 1", "value");
        }
        if (value > 1)
        {
          throw new ArgumentException("The opacity must be between 0 and 1", "value");
        }
        this._Opacity = value;
        this._TabControl.Invalidate();
      }
    }

    [Category("Appearance"), DefaultValue(typeof(Color), "")]
    public Color BorderColorSelected
    {
      get
      {
        if (this._BorderColorSelected.IsEmpty)
        {
          return ThemedColors.ToolBorder;
        }
        else
        {
          return this._BorderColorSelected;
        }
      }
      set
      {
        if (value.Equals(ThemedColors.ToolBorder))
        {
          this._BorderColorSelected = Color.Empty;
        }
        else
        {
          this._BorderColorSelected = value;
        }
        this._TabControl.Invalidate();
      }
    }

    [Category("Appearance"), DefaultValue(typeof(Color), "")]
    public Color BorderColorHot
    {
      get
      {
        if (this._BorderColorHot.IsEmpty)
        {
          return SystemColors.ControlDark;
        }
        else
        {
          return this._BorderColorHot;
        }
      }
      set
      {
        if (value.Equals(SystemColors.ControlDark))
        {
          this._BorderColorHot = Color.Empty;
        }
        else
        {
          this._BorderColorHot = value;
        }
        this._TabControl.Invalidate();
      }
    }

    [Category("Appearance"), DefaultValue(typeof(Color), "")]
    public Color BorderColor
    {
      get
      {
        if (this._BorderColor.IsEmpty)
        {
          return SystemColors.ControlDark;
        }
        else
        {
          return this._BorderColor;
        }
      }
      set
      {
        if (value.Equals(SystemColors.ControlDark))
        {
          this._BorderColor = Color.Empty;
        }
        else
        {
          this._BorderColor = value;
        }
        this._TabControl.Invalidate();
      }
    }

    [Category("Appearance"), DefaultValue(typeof(Color), "")]
    public Color TextColor
    {
      get
      {
        if (this._TextColor.IsEmpty)
        {
          return SystemColors.ControlText;
        }
        else
        {
          return this._TextColor;
        }
      }
      set
      {
        if (value.Equals(SystemColors.ControlText))
        {
          this._TextColor = Color.Empty;
        }
        else
        {
          this._TextColor = value;
        }
        this._TabControl.Invalidate();
      }
    }

    [Category("Appearance"), DefaultValue(typeof(Color), "")]
    public Color TextColorSelected
    {
      get
      {
        if (this._TextColorSelected.IsEmpty)
        {
          return SystemColors.ControlText;
        }
        else
        {
          return this._TextColorSelected;
        }
      }
      set
      {
        if (value.Equals(SystemColors.ControlText))
        {
          this._TextColorSelected = Color.Empty;
        }
        else
        {
          this._TextColorSelected = value;
        }
        this._TabControl.Invalidate();
      }
    }

    [Category("Appearance"), DefaultValue(typeof(Color), "")]
    public Color TextColorDisabled
    {
      get
      {
        if (this._TextColor.IsEmpty)
        {
          return SystemColors.ControlDark;
        }
        else
        {
          return this._TextColorDisabled;
        }
      }
      set
      {
        if (value.Equals(SystemColors.ControlDark))
        {
          this._TextColorDisabled = Color.Empty;
        }
        else
        {
          this._TextColorDisabled = value;
        }
        this._TabControl.Invalidate();
      }
    }

    [Category("Appearance"), DefaultValue(typeof(Color), "")]
    public Color TabColorSelected
    {
      get
      {
        if (this._TextColor.IsEmpty)
        {
          return SystemColors.ControlDark;
        }
        else
        {
          return this.m_tab_color_selected;
        }
      }
      set
      {
        if (value.Equals(SystemColors.ControlDark))
        {
          this.m_tab_color_selected = Color.Empty;
        }
        else
        {
          this.m_tab_color_selected = value;
        }
        this._TabControl.Invalidate();
      }
    }

    [Category("Appearance"), DefaultValue(typeof(Color), "")]
    public Color TabColorDisabled
    {
      get
      {
        if (this._TextColor.IsEmpty)
        {
          return SystemColors.ControlDark;
        }
        else
        {
          return this.m_tab_color_disabled;
        }
      }
      set
      {
        if (value.Equals(SystemColors.ControlDark))
        {
          this.m_tab_color_disabled = Color.Empty;
        }
        else
        {
          this.m_tab_color_disabled = value;
        }
        this._TabControl.Invalidate();
      }
    }

    [Category("Appearance"), DefaultValue(typeof(Color), "")]
    public Color TabColorHeader
    {
      get
      {
        if (this._TextColor.IsEmpty)
        {
          return SystemColors.ControlDark;
        }
        else
        {
          return this.m_tab_color_header;
        }
      }
      set
      {
        if (value.Equals(SystemColors.ControlDark))
        {
          this.m_tab_color_header = Color.Empty;
        }
        else
        {
          this.m_tab_color_header = value;
        }
        this._TabControl.Invalidate();
      }
    }

    [Category("Appearance"), DefaultValue(typeof(Color), "")]
    public Color TabColor
    {
      get
      {
        if (this._TextColor.IsEmpty)
        {
          return SystemColors.ControlDark;
        }
        else
        {
          return this.m_tab_color;
        }
      }
      set
      {
        if (value.Equals(SystemColors.ControlDark))
        {
          this.m_tab_color = Color.Empty;
        }
        else
        {
          this.m_tab_color = value;
        }
        this._TabControl.Invalidate();
      }
    }

    #endregion

    #region Painting

    public void PaintTab(int index, Graphics graphics)
    {
      using (GraphicsPath tabpath = this.GetTabBorder(index))
      {
        using (Brush fillBrush = this.GetTabBackgroundBrush(index))
        {
          //	Paint the background
          graphics.FillPath(fillBrush, tabpath);

          //	Paint a focus indication
          //if (this._TabControl.Focused)
          //{
            this.DrawTabFocusIndicator(tabpath, index, graphics);
          //}
        }
      }
    }

    private void DrawTabFocusIndicator(GraphicsPath tabpath, int index, Graphics graphics)
    {
      if (this._FocusTrack && index == this._TabControl.SelectedIndex)
      {
        Brush focusBrush = null;
        RectangleF pathRect = tabpath.GetBounds();
        GraphicsPath _path;

        _path = new GraphicsPath();
        
        switch (this._TabControl.Alignment)
        {
          case TabAlignment.Top:
            //focusRect = new Rectangle((int)pathRect.X, (int)pathRect.Y, (int)pathRect.Width, 4);
            _path.AddLine((int)pathRect.X + (int)pathRect.Width / 2 - 4, (int)pathRect.Height + 3, (int)pathRect.X + (int)pathRect.Width / 2 + 4, (int)pathRect.Height + 3);
            _path.AddLine((int)pathRect.X + (int)pathRect.Width / 2 + 4, (int)pathRect.Height + 3, (int)pathRect.X + (int)pathRect.Width / 2, (int)pathRect.Height - 5);
            _path.CloseFigure();
            focusBrush = new SolidBrush(m_tab_color);


            //focusBrush = new LinearGradientBrush(focusRect, this._FocusColor, Color.Yellow, LinearGradientMode.Vertical);
            break;
          //case TabAlignment.Bottom:
          //  focusRect = new Rectangle((int)pathRect.X, (int)pathRect.Bottom - 4, (int)pathRect.Width, 4);
          //  focusBrush = new LinearGradientBrush(focusRect, SystemColors.ControlLight, this._FocusColor, LinearGradientMode.Vertical);
          //  break;
          //case TabAlignment.Left:
          //  focusRect = new Rectangle((int)pathRect.X, (int)pathRect.Y, 4, (int)pathRect.Height);
          //  focusBrush = new LinearGradientBrush(focusRect, this._FocusColor, SystemColors.ControlLight, LinearGradientMode.Horizontal);
          //  break;
          //case TabAlignment.Right:
          //  focusRect = new Rectangle((int)pathRect.Right - 4, (int)pathRect.Y, 4, (int)pathRect.Height);
          //  focusBrush = new LinearGradientBrush(focusRect, SystemColors.ControlLight, this._FocusColor, LinearGradientMode.Horizontal);
          //  break;
        }

        //	Ensure the focus stip does not go outside the tab
        //Region focusRegion = new Region(_path);
        //focusRegion.Intersect(tabpath);
        //graphics.FillRegion(focusBrush, focusRegion);
        //focusRegion.Dispose();
        graphics.FillPath(new SolidBrush(Color.White), _path);
        focusBrush.Dispose();
      }
    }

    #endregion

    #region Background brushes

    //private Blend GetBackgroundBlend()
    //{
    //  float[] relativeIntensities = new float[] { 0f, 0.7f, 1f };
    //  float[] relativePositions = new float[] { 0f, 0.6f, 1f };

    //  //	Glass look to top aligned tabs
    //  if (this._TabControl.Alignment == TabAlignment.Top)
    //  {
    //    relativeIntensities = new float[] { 0f, 0.5f, 1f, 1f };
    //    relativePositions = new float[] { 0f, 0.5f, 0.51f, 1f };
    //  }

    //  Blend blend = new Blend();
    //  blend.Factors = relativeIntensities;
    //  blend.Positions = relativePositions;

    //  return blend;
    //}

    public virtual Brush GetPageBackgroundBrush(int index)
    {

      //	Capture the colours dependant on selection state of the tab
      //Color light = Color.FromArgb(242, 242, 242);
      //if (this._TabControl.Alignment == TabAlignment.Top)
      //{
      //  light = Color.FromArgb(207, 207, 207);
      //}

      //if (this._TabControl.SelectedIndex == index)
      //{
      //  light = SystemColors.Window;
      //}
      //else if (!this._TabControl.TabPages[index].Enabled)
      //{
      //  light = Color.FromArgb(207, 207, 207);
      //}
      //else if (this._HotTrack && index == this._TabControl.ActiveIndex)
      //{
      //  //	Enable hot tracking
      //  light = Color.FromArgb(234, 246, 253);
      //}

      Color light = m_tab_color;

      return new SolidBrush(light);
    }

    #endregion

    #region Tab border and rect

    public GraphicsPath GetTabBorder(int index)
    {
      Boolean _is_first_tab;
      Boolean _is_last_tab;
      TabStyleRoundedProvider.TAB_POSITION _tab_position;

      _tab_position = TabStyleRoundedProvider.TAB_POSITION.OTHER;

      _is_first_tab = this._TabControl.IsFirstTabInRow(index);
      _is_last_tab = this._TabControl.IsLastTabInRow(index);
      if (_is_first_tab && _is_last_tab)
      {
        _tab_position = TabStyleRoundedProvider.TAB_POSITION.FIRST_AND_LAST;
      }
      else if (_is_first_tab)
      {
        _tab_position = TabStyleRoundedProvider.TAB_POSITION.FIRST;
      }
      else if (_is_last_tab)
      {
        _tab_position = TabStyleRoundedProvider.TAB_POSITION.LAST;
      }
      
      GraphicsPath path = new GraphicsPath();
      Rectangle tabBounds = this.GetTabRect(index);

      this.AddTabBorder(path, tabBounds, _tab_position);

      path.CloseFigure();
      return path;
    }

    #endregion

  }

  public enum TabStyle
  {
    Rounded = 3,
  }

  internal sealed class NativeMethods
  {
    private NativeMethods() { }

    #region Windows Constants

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
    public const int WM_GETTABRECT = 0x130a;
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
    public const int WS_EX_TRANSPARENT = 0x20;
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
    public const int WM_SETFONT = 0x30;
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
    public const int WM_FONTCHANGE = 0x1d;
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
    public const int WM_HSCROLL = 0x114;
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
    public const int TCM_HITTEST = 0x130D;
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
    public const int WM_PAINT = 0xf;
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
    public const int WS_EX_LAYOUTRTL = 0x400000;
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
    public const int WS_EX_NOINHERITLAYOUT = 0x100000;


    #endregion

    #region Content Alignment

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
    public static readonly System.Drawing.ContentAlignment AnyRightAlign = System.Drawing.ContentAlignment.BottomRight | System.Drawing.ContentAlignment.MiddleRight | System.Drawing.ContentAlignment.TopRight;
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
    public static readonly System.Drawing.ContentAlignment AnyLeftAlign = System.Drawing.ContentAlignment.BottomLeft | System.Drawing.ContentAlignment.MiddleLeft | System.Drawing.ContentAlignment.TopLeft;
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
    public static readonly System.Drawing.ContentAlignment AnyTopAlign = System.Drawing.ContentAlignment.TopRight | System.Drawing.ContentAlignment.TopCenter | System.Drawing.ContentAlignment.TopLeft;
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
    public static readonly System.Drawing.ContentAlignment AnyBottomAlign = System.Drawing.ContentAlignment.BottomRight | System.Drawing.ContentAlignment.BottomCenter | System.Drawing.ContentAlignment.BottomLeft;
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
    public static readonly System.Drawing.ContentAlignment AnyMiddleAlign = System.Drawing.ContentAlignment.MiddleRight | System.Drawing.ContentAlignment.MiddleCenter | System.Drawing.ContentAlignment.MiddleLeft;
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
    public static readonly System.Drawing.ContentAlignment AnyCenterAlign = System.Drawing.ContentAlignment.BottomCenter | System.Drawing.ContentAlignment.MiddleCenter | System.Drawing.ContentAlignment.TopCenter;

    #endregion

    #region User32.dll

    //        [DllImport("user32.dll"), SecurityPermission(SecurityAction.Demand)]
    //		public static extern IntPtr SendMessage(IntPtr hWnd, UInt32 msg, IntPtr wParam, IntPtr lParam);

    public static IntPtr SendMessage(IntPtr hWnd, int msg, IntPtr wParam, IntPtr lParam)
    {
      //	This Method replaces the User32 method SendMessage, but will only work for sending
      //	messages to Managed controls.
      Control control = Control.FromHandle(hWnd);
      if (control == null)
      {
        return IntPtr.Zero;
      }

      Message message = new Message();
      message.HWnd = hWnd;
      message.LParam = lParam;
      message.WParam = wParam;
      message.Msg = msg;

      MethodInfo wproc = control.GetType().GetMethod("WndProc"
                                                     , BindingFlags.NonPublic
                                                      | BindingFlags.InvokeMethod
                                                      | BindingFlags.FlattenHierarchy
                                                      | BindingFlags.IgnoreCase
                                                      | BindingFlags.Instance);

      object[] args = new object[] { message };
      wproc.Invoke(control, args);

      return ((Message)args[0]).Result;
    }


    //		[DllImport("user32.dll")]
    //		public static extern IntPtr BeginPaint(IntPtr hWnd, ref PAINTSTRUCT paintStruct);
    //		
    //		[DllImport("user32.dll")]
    //		[return: MarshalAs(UnmanagedType.Bool)]
    //		public static extern bool EndPaint(IntPtr hWnd, ref PAINTSTRUCT paintStruct);
    //
    #endregion

    #region Misc Functions

    public static int LoWord(IntPtr dWord)
    {
      return dWord.ToInt32() & 0xffff;
    }

    public static int HiWord(IntPtr dWord)
    {
      if ((dWord.ToInt32() & 0x80000000) == 0x80000000)
        return (dWord.ToInt32() >> 16);
      else
        return (dWord.ToInt32() >> 16) & 0xffff;
    }

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2106:SecureAsserts")]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2122:DoNotIndirectlyExposeMethodsWithLinkDemands")]
    public static IntPtr ToIntPtr(object structure)
    {
      IntPtr lparam = IntPtr.Zero;
      lparam = Marshal.AllocCoTaskMem(Marshal.SizeOf(structure));
      Marshal.StructureToPtr(structure, lparam, false);
      return lparam;
    }


    #endregion

    #region Windows Structures and Enums

    [Flags()]
    public enum TCHITTESTFLAGS
    {
      TCHT_NOWHERE = 1,
      TCHT_ONITEMICON = 2,
      TCHT_ONITEMLABEL = 4,
      TCHT_ONITEM = TCHT_ONITEMICON | TCHT_ONITEMLABEL
    }



    [StructLayout(LayoutKind.Sequential)]
    public struct TCHITTESTINFO
    {

      public TCHITTESTINFO(Point location)
      {
        pt = location;
        flags = TCHITTESTFLAGS.TCHT_ONITEM;
      }

      public Point pt;
      public TCHITTESTFLAGS flags;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 4)]
    public struct PAINTSTRUCT
    {
      public IntPtr hdc;
      public int fErase;
      public RECT rcPaint;
      public int fRestore;
      public int fIncUpdate;
      [MarshalAs(UnmanagedType.ByValArray, SizeConst = 32)]
      public byte[] rgbReserved;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct RECT
    {
      public int left;
      public int top;
      public int right;
      public int bottom;

      public RECT(int left, int top, int right, int bottom)
      {
        this.left = left;
        this.top = top;
        this.right = right;
        this.bottom = bottom;
      }

      public RECT(Rectangle r)
      {
        this.left = r.Left;
        this.top = r.Top;
        this.right = r.Right;
        this.bottom = r.Bottom;
      }

      public static RECT FromXYWH(int x, int y, int width, int height)
      {
        return new RECT(x, y, x + width, y + height);
      }

      public static RECT FromIntPtr(IntPtr ptr)
      {
        RECT rect = (RECT)Marshal.PtrToStructure(ptr, typeof(RECT));
        return rect;
      }

      public Size Size
      {
        get
        {
          return new Size(this.right - this.left, this.bottom - this.top);
        }
      }
    }


    #endregion

  }

  internal sealed class ThemedColors
  {

    #region "    Variables and Constants "

    private const string NormalColor = "NormalColor";
    private const string HomeStead = "HomeStead";
    private const string Metallic = "Metallic";
    private const string NoTheme = "NoTheme";

    private static Color[] _toolBorder;
    #endregion

    #region "    Properties "

    [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
    public static ColorScheme CurrentThemeIndex
    {
      get { return ThemedColors.GetCurrentThemeIndex(); }
    }

    [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
    public static Color ToolBorder
    {
      get { return ThemedColors._toolBorder[(int)ThemedColors.CurrentThemeIndex]; }
    }

    #endregion

    #region "    Constructors "

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline")]
    static ThemedColors()
    {
      ThemedColors._toolBorder = new Color[] { Color.FromArgb(127, 157, 185), Color.FromArgb(164, 185, 127), Color.FromArgb(165, 172, 178), Color.FromArgb(132, 130, 132) };
    }

    private ThemedColors() { }

    #endregion

    private static ColorScheme GetCurrentThemeIndex()
    {
      ColorScheme theme = ColorScheme.NoTheme;

      if (VisualStyleInformation.IsSupportedByOS && VisualStyleInformation.IsEnabledByUser && Application.RenderWithVisualStyles)
      {


        switch (VisualStyleInformation.ColorScheme)
        {
          case NormalColor:
            theme = ColorScheme.NormalColor;
            break;
          case HomeStead:
            theme = ColorScheme.HomeStead;
            break;
          case Metallic:
            theme = ColorScheme.Metallic;
            break;
          default:
            theme = ColorScheme.NoTheme;
            break;
        }
      }

      return theme;
    }

    public enum ColorScheme
    {
      NormalColor = 0,
      HomeStead = 1,
      Metallic = 2,
      NoTheme = 3
    }

  }


  [System.ComponentModel.ToolboxItem(false)]
  public class TabStyleRoundedProvider : TabStyleProvider
  {

    public enum TAB_POSITION
    {
      FIRST = 0,
      LAST = 1,
      FIRST_AND_LAST = 2,
      OTHER = 3,
    };

    public TabStyleRoundedProvider(uc_round_tab_control tabControl)
      : base(tabControl)
    {
      
      this._Radius = 10;
      //	Must set after the _Radius as this is used in the calculations of the actual padding
      this.Padding = new Point(6, 3);
    }

    public override void AddTabBorder(System.Drawing.Drawing2D.GraphicsPath path, System.Drawing.Rectangle tabBounds, TAB_POSITION TabPosition)
    {      
      switch (this._TabControl.Alignment)
      {
        case TabAlignment.Top:          
          
          if (TabPosition == TAB_POSITION.FIRST)
          {
            path.AddLine(tabBounds.X, tabBounds.Bottom, tabBounds.X, tabBounds.Y + this._Radius);
            path.AddArc(tabBounds.X, tabBounds.Y, this._Radius * 2, this._Radius * 2, 180, 90);
            path.AddLine(tabBounds.X + this._Radius, tabBounds.Y, tabBounds.Right, tabBounds.Y);
            path.AddLine(tabBounds.Right, tabBounds.Y, tabBounds.Right, tabBounds.Bottom);
          }
          else if (TabPosition == TAB_POSITION.LAST)
          {
            path.AddLine(tabBounds.X, tabBounds.Bottom, tabBounds.X, tabBounds.Y);
            path.AddLine(tabBounds.X, tabBounds.Y, tabBounds.Right - this._Radius, tabBounds.Y);
            path.AddArc(tabBounds.Right - this._Radius * 2, tabBounds.Y, this._Radius * 2, this._Radius * 2, 270, 90);
            path.AddLine(tabBounds.Right, tabBounds.Y + this._Radius, tabBounds.Right, tabBounds.Bottom);
          }
          else if (TabPosition == TAB_POSITION.FIRST_AND_LAST)
          {
            path.AddLine(tabBounds.X, tabBounds.Bottom, tabBounds.X, tabBounds.Y + this._Radius);
            path.AddArc(tabBounds.X, tabBounds.Y, this._Radius * 2, this._Radius * 2, 180, 90);
            path.AddLine(tabBounds.X + this._Radius, tabBounds.Y, tabBounds.Right - this._Radius, tabBounds.Y);
            path.AddArc(tabBounds.Right - this._Radius * 2, tabBounds.Y, this._Radius * 2, this._Radius * 2, 270, 90);
            path.AddLine(tabBounds.Right, tabBounds.Y + this._Radius, tabBounds.Right, tabBounds.Bottom);
          }
          else
          {
            path.AddLine(tabBounds.X, tabBounds.Bottom, tabBounds.X, tabBounds.Y);
            path.AddLine(tabBounds.X, tabBounds.Y, tabBounds.Right, tabBounds.Y);
            path.AddLine(tabBounds.Right, tabBounds.Y, tabBounds.Right, tabBounds.Bottom);
          }
          break;
        //case TabAlignment.Bottom:
        //  path.AddLine(tabBounds.Right, tabBounds.Y, tabBounds.Right, tabBounds.Bottom - this._Radius);
        //  path.AddArc(tabBounds.Right - this._Radius * 2, tabBounds.Bottom - this._Radius * 2, this._Radius * 2, this._Radius * 2, 0, 90);
        //  path.AddLine(tabBounds.Right - this._Radius, tabBounds.Bottom, tabBounds.X + this._Radius, tabBounds.Bottom);
        //  path.AddArc(tabBounds.X, tabBounds.Bottom - this._Radius * 2, this._Radius * 2, this._Radius * 2, 90, 90);
        //  path.AddLine(tabBounds.X, tabBounds.Bottom - this._Radius, tabBounds.X, tabBounds.Y);
        //  break;
        //case TabAlignment.Left:
        //  path.AddLine(tabBounds.Right, tabBounds.Bottom, tabBounds.X + this._Radius, tabBounds.Bottom);
        //  path.AddArc(tabBounds.X, tabBounds.Bottom - this._Radius * 2, this._Radius * 2, this._Radius * 2, 90, 90);
        //  path.AddLine(tabBounds.X, tabBounds.Bottom - this._Radius, tabBounds.X, tabBounds.Y + this._Radius);
        //  path.AddArc(tabBounds.X, tabBounds.Y, this._Radius * 2, this._Radius * 2, 180, 90);
        //  path.AddLine(tabBounds.X + this._Radius, tabBounds.Y, tabBounds.Right, tabBounds.Y);
        //  break;
        //case TabAlignment.Right:
        //  path.AddLine(tabBounds.X, tabBounds.Y, tabBounds.Right - this._Radius, tabBounds.Y);
        //  path.AddArc(tabBounds.Right - this._Radius * 2, tabBounds.Y, this._Radius * 2, this._Radius * 2, 270, 90);
        //  path.AddLine(tabBounds.Right, tabBounds.Y + this._Radius, tabBounds.Right, tabBounds.Bottom - this._Radius);
        //  path.AddArc(tabBounds.Right - this._Radius * 2, tabBounds.Bottom - this._Radius * 2, this._Radius * 2, this._Radius * 2, 0, 90);
        //  path.AddLine(tabBounds.Right - this._Radius, tabBounds.Bottom, tabBounds.X, tabBounds.Bottom);
        //  break;
      }
    }
  }

}

