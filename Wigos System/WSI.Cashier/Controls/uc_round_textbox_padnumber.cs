﻿//------------------------------------------------------------------------------
// Copyright © 2015-2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_round_textbox_padnumber.cs
// 
//   DESCRIPTION: Implements the uc_round_textbox_padnumber user control
//                Class: uc_round_textbox_padnumber. Inherits from uc_round_textbox. Shows a Pad Number on enter
//
//        AUTHOR: Daniel Dubé
// 
// CREATION DATE: 27-JAN-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 27-JAN-2016 DDS     First release.
//------------------------------------------------------------------------------

using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using System.Xml.Serialization;
using WSI.Common;

namespace WSI.Cashier.Controls
{
    public enum PadNumberHorizontalPosition
    {
        NONE = 0,
        LEFT = 1,
        RIGHT = 2
    };
    public enum PadNumberVerticalPosition
    {
        NONE = 0,
        TOP = 1,
        MIDDLE = 2,
        BOTTOM = 3
    };

    public class uc_round_textbox_padnumber : uc_round_textbox
    {
        private static frm_yesno form_yes_no;

        #region Members
        private Boolean m_show_padnumber_on_enter;
        private FormStartPosition m_padnumber_start_position;
        private Point? m_padnumber_location;
        private PadNumberHorizontalPosition m_padnumber_horizontal_position;
        private PadNumberVerticalPosition m_padnumber_vertical_position;
        private GENERIC_AMOUNT_INPUT_TYPE m_padnumber_amount_input_type;
        #endregion

        #region Properties

        public Boolean ShowPadNumberOnEnter
        {
            get { return this.m_show_padnumber_on_enter; }
            set { this.m_show_padnumber_on_enter = value; }
        }

        public FormStartPosition PadNumberStartPosition
        {
            get { return this.m_padnumber_start_position; }
            set { this.m_padnumber_start_position = value; }
        }

        public Point? PadNumberLocation
        {
            get { return this.m_padnumber_location; }
            set { this.m_padnumber_location = value; }
        }

        public PadNumberHorizontalPosition PadNumberHorizontalPosition
        {
            get { return this.m_padnumber_horizontal_position; }
            set { this.m_padnumber_horizontal_position = value; }
        }

        public PadNumberVerticalPosition PadNumberVerticalPosition
        {
            get { return this.m_padnumber_vertical_position; }
            set { this.m_padnumber_vertical_position = value; }
        }

        public GENERIC_AMOUNT_INPUT_TYPE PadNumberAmountInputType
        {
            get { return this.m_padnumber_amount_input_type; }
            set { this.m_padnumber_amount_input_type = value; }
        }


        #endregion

        #region Constructors
        public uc_round_textbox_padnumber()
        {
            this.m_padnumber_amount_input_type = GENERIC_AMOUNT_INPUT_TYPE.AMOUNT;
            this.m_padnumber_start_position = FormStartPosition.CenterScreen;
        }

        #endregion

        #region Events
        protected override void OnClick(EventArgs e)
        {
            base.OnClick(e);
            ShowPadNumber();
        }
        protected override void OnEnter(object sender, EventArgs e)
        {
            base.OnEnter(sender, e);
            ShowPadNumber();
        }
        #endregion

        #region PrivateMethods


        private void ShowPadNumber()
        {
            Boolean _result;
            Decimal _value;

            if (this.ReadOnly || !this.m_show_padnumber_on_enter)
                return;

            if (!Decimal.TryParse(this.Text, out _value))
                _value = 0;

            using (frm_generic_amount_input _padnumber = new frm_generic_amount_input())
            {
                if (this.m_padnumber_location.HasValue)
                {
                    _padnumber.StartPosition = FormStartPosition.Manual;
                    _padnumber.CustomLocation = true;
                    _padnumber.Location = m_padnumber_location.Value;
                }
                else
                    if (this.m_padnumber_horizontal_position != WSI.Cashier.Controls.PadNumberHorizontalPosition.NONE
                        || this.m_padnumber_vertical_position != WSI.Cashier.Controls.PadNumberVerticalPosition.NONE)
                    {

                        Int32 _horizontal_position = 0;
                        Int32 _vertical_position = 0;

                        switch (this.m_padnumber_horizontal_position)
                        {
                            case WSI.Cashier.Controls.PadNumberHorizontalPosition.LEFT:
                                _horizontal_position = this.PointToScreen(Point.Empty).X - _padnumber.Size.Width;
                                break;
                            case WSI.Cashier.Controls.PadNumberHorizontalPosition.RIGHT:
                                _horizontal_position = this.PointToScreen(Point.Empty).X + this.Size.Width;
                                break;
                            default:
                                break;
                        }

                        switch (this.m_padnumber_vertical_position)
                        {
                            case WSI.Cashier.Controls.PadNumberVerticalPosition.TOP:
                                _vertical_position = this.PointToScreen(Point.Empty).Y - _padnumber.Size.Height + this.Size.Height;
                                break;
                            case WSI.Cashier.Controls.PadNumberVerticalPosition.MIDDLE:
                                _vertical_position = this.PointToScreen(Point.Empty).Y - _padnumber.Size.Height / 2;
                                break;
                            case WSI.Cashier.Controls.PadNumberVerticalPosition.BOTTOM:
                                _vertical_position = this.PointToScreen(Point.Empty).Y;
                                break;
                            default:
                                break;
                        }
                        _padnumber.StartPosition = FormStartPosition.Manual;
                        _padnumber.CustomLocation = true;
                        _padnumber.Location = new Point(_horizontal_position, _vertical_position);
                        // TODO: Check if PadNumber is out of the screen
                    }
                form_yes_no = new frm_yesno();
                form_yes_no.Opacity = 0.6;
                form_yes_no.Show(this.Parent);
                _result = _padnumber.Show(this.m_padnumber_amount_input_type, string.Empty, ref _value);
                form_yes_no.Close();
                if (_padnumber.DialogResult == DialogResult.OK)
                {
                    this.Text = _value.ToString();
                }
            }
        }

        //private void SetTextBoxStyle()
        //{
        //    CashierStyle.ControlStyle.InitStyle((uc_round_textbox)this);

        //    base.AdjustControls();
        //}
        #endregion


    }
}
