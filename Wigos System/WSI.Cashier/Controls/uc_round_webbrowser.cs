﻿//------------------------------------------------------------------------------
// Copyright © 2015-2020 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_round_webbrowser.cs
// 
//   DESCRIPTION: Implements the uc_round_webbrowser user control
//                Class: uc_round_webbrowser
//
//        AUTHOR: José Martínez
// 
// CREATION DATE: 06-SEP-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 06-SEP-2016 JML     First release.
// 04-OCT-2016 JML     Bug 18456: Gaming tables: the report with multiple currency does not show all currencies in the cashier
//------------------------------------------------------------------------------


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;


namespace WSI.Cashier.Controls
{
  public partial class uc_round_webbrowser : UserControl
  {

    #region Enums

    public enum BUTTON_DOWN
    {
      NONE = 0,
      TOP = 1,
      BOTTOM = 2,
      LEFT = 3,
      RIGHT = 4
    }

    #endregion // Enums

    private Timer m_button_down_timer = new Timer();

    private BUTTON_DOWN m_button_down;

    private Boolean m_web_browser_shortcuts_enabled;

    [Category("Url")]
    public Boolean WebBrowserShortcutsEnabled
    {
      get
      {
        return m_web_browser_shortcuts_enabled;
      }
      set
      {
        m_web_browser_shortcuts_enabled = value;
      }
    }

    private Boolean m_allow_web_browser_drop;

    [Category("")]
    public Boolean AllowWebBrowserDrop
    {
      get
      {
        return m_allow_web_browser_drop;
      }
      set
      {
        m_allow_web_browser_drop = value;
      }
    }

    public uc_round_webbrowser()
    {
      InitializeComponent();
      m_button_down_timer = new Timer();
      m_button_down_timer.Interval = 150;
      m_button_down_timer.Tick += new EventHandler(RepeatClickAction);
    }

    public void Navigate(string urlString)
    {
      web_browser.Navigate(urlString);
    }

    #region Private Methods

    private void vertical_top()
    {
      HtmlDocument _html_doc;
      int _scroll_top;
      int _scroll_left;
      Rectangle _scroll_rectangle;

      _html_doc = web_browser.Document;

      if (_html_doc != null)
      {
        _scroll_top = _html_doc.Body.ScrollTop;
        _scroll_left = _html_doc.Body.ScrollLeft;
        _scroll_rectangle = _html_doc.Body.ClientRectangle;

        _html_doc.Body.ScrollTop -= 10;
      }

    }

    private void vertical_bottom()
    {
      HtmlDocument _html_doc;
      int _scroll_top;
      int _scroll_left;
      Rectangle _scroll_rectangle;

      _html_doc = web_browser.Document;

      if (_html_doc != null)
      {
        _scroll_top = _html_doc.Body.ScrollTop;
        _scroll_left = _html_doc.Body.ScrollLeft;
        _scroll_rectangle = _html_doc.Body.ClientRectangle;

        _html_doc.Body.ScrollTop += 10;
      }

    }

    private void horizontal_left()
    {
      HtmlDocument _html_doc;
      int _scroll_top;
      int _scroll_left;
      Rectangle _scroll_rectangle;

      _html_doc = web_browser.Document;

      if (_html_doc != null)
      {
        _scroll_top = _html_doc.Body.ScrollTop;
        _scroll_left = _html_doc.Body.ScrollLeft;
        _scroll_rectangle = _html_doc.Body.ClientRectangle;

        _html_doc.Body.ScrollLeft -= 10;
      }

    }

    private void horizontal_right()
    {
      HtmlDocument _html_doc;
      int _scroll_top;
      int _scroll_left;
      Rectangle _scroll_rectangle;

      _html_doc = web_browser.Document;

      if (_html_doc != null)
      {
        _scroll_top = _html_doc.Body.ScrollTop;
        _scroll_left = _html_doc.Body.ScrollLeft;
        _scroll_rectangle = _html_doc.Body.ClientRectangle;

        _html_doc.Body.ScrollLeft += 10;
      }

    }

    private void RepeatClickAction(object sender, EventArgs e)
    {
      switch (m_button_down)
      {
        case BUTTON_DOWN.TOP:
          vertical_top();
          break;

        case BUTTON_DOWN.BOTTOM:
          vertical_bottom();
          break;

        case BUTTON_DOWN.LEFT:
          horizontal_left();
          break;

        case BUTTON_DOWN.RIGHT:
          horizontal_right();
          break;

        default:
          // Do nothing.
          break;
      }
    }

    #endregion // Private Methods

    #region Events

    private void btn_vertical_top_Click(object sender, EventArgs e)
    {
      vertical_top();
    }

    private void btn_vertical_bottom_Click(object sender, EventArgs e)
    {
      vertical_bottom();
    }

    private void btn_horizontal_left_Click(object sender, EventArgs e)
    {
      horizontal_left();
    }

    private void btn_horizontal_right_Click(object sender, EventArgs e)
    {
      horizontal_right();
    }

    private void web_browser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
    {
      HtmlDocument _html_doc;
      Rectangle _data_rectangle;
      Point _point;

      // Hidden system scrollbar
      web_browser.Document.Body.Style = "overflow:hidden";

      // Hidden scroll-bottoms when not necesary
      _html_doc = web_browser.Document;

      if (_html_doc != null)
      {
        _data_rectangle = _html_doc.Body.ScrollRectangle;

        // First: Set web browser size

        // Hidden vertical-scroll-bottoms
        if (_data_rectangle.Height > pnl_background.Height)
        {
          btn_vertical_top.Visible = true;
          btn_vertical_bottom.Visible = true;
          web_browser.Width = pnl_background.Width - btn_vertical_top.Width;
        }
        else
        {
          btn_vertical_top.Visible = false;
          btn_vertical_bottom.Visible = false;
          web_browser.Width = pnl_background.Width;
        }

        // Hidden horizontal-scroll-bottoms
        if (_data_rectangle.Width > pnl_background.Width)
        {
          btn_horizontal_right.Visible = true;
          btn_horizontal_left.Visible = true;
          web_browser.Height = pnl_background.Height - btn_horizontal_right.Height;
        }
        else
        {
          btn_horizontal_right.Visible = false;
          btn_horizontal_left.Visible = false;
          web_browser.Height = pnl_background.Height;
        }

        // Second: Set buttons position

        // Set horizontal-rigth-scroll button position
        if (_data_rectangle.Height > pnl_background.Height)
        {
          _point = btn_horizontal_right.Location;
          _point.X = web_browser.Width - btn_horizontal_right.Width;
          _point.Y = web_browser.Height - 1;
          btn_horizontal_right.Location = _point;
        }
        else
        {
          _point = btn_horizontal_right.Location;
          _point.X = pnl_background.Width - btn_horizontal_right.Width;
          _point.Y = web_browser.Height - 1;
          btn_horizontal_right.Location = _point;
        }

        // Set vertical-bottom-scroll button position
        if (_data_rectangle.Width > pnl_background.Width)
        {
          _point = btn_vertical_bottom.Location;
          _point.X = web_browser.Width - 1;
          _point.Y = web_browser.Height - btn_vertical_bottom.Height;
          btn_vertical_bottom.Location = _point;
        }
        else
        {
          _point = btn_vertical_bottom.Location;
          _point.X = web_browser.Width - 1;
          _point.Y = pnl_background.Height - btn_vertical_bottom.Height;
          btn_vertical_bottom.Location = _point;
        }

        if (_html_doc.Body.InnerHtml == null)
        {
          web_browser.Document.Body.Style = "background-color:#E3E7E9";
        }
      }

    }

    private void btn_vertical_top_MouseDown(object sender, MouseEventArgs e)
    {
      m_button_down = BUTTON_DOWN.TOP;
      m_button_down_timer.Start();
    }

    private void btn_vertical_top_MouseUp(object sender, MouseEventArgs e)
    {
      m_button_down_timer.Stop();
      m_button_down = BUTTON_DOWN.NONE;
    }

    private void btn_vertical_bottom_MouseDown(object sender, MouseEventArgs e)
    {
      m_button_down = BUTTON_DOWN.BOTTOM;
      m_button_down_timer.Start();
    }

    private void btn_vertical_bottom_MouseUp(object sender, MouseEventArgs e)
    {
      m_button_down_timer.Stop();
      m_button_down = BUTTON_DOWN.NONE;
    }

    private void btn_horizontal_left_MouseDown(object sender, MouseEventArgs e)
    {
      m_button_down = BUTTON_DOWN.LEFT;
      m_button_down_timer.Start();
    }

    private void btn_horizontal_left_MouseUp(object sender, MouseEventArgs e)
    {
      m_button_down_timer.Stop();
      m_button_down = BUTTON_DOWN.NONE;
    }

    private void btn_horizontal_right_MouseDown(object sender, MouseEventArgs e)
    {
      m_button_down = BUTTON_DOWN.RIGHT;
      m_button_down_timer.Start();
    }

    private void btn_horizontal_right_MouseUp(object sender, MouseEventArgs e)
    {
      m_button_down_timer.Stop();
      m_button_down = BUTTON_DOWN.NONE;
    }

    #endregion // events

  }
}
