﻿//------------------------------------------------------------------------------
// Copyright © 2015-2020 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_radioButton.cs
// 
//   DESCRIPTION: Implements the uc_radioButton user control
//                Class: uc_radioButton
//
//        AUTHOR: José Martínez
//                David Hernández
// 
// CREATION DATE: 20-OCT-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 20-OCT-2015 JML     First release.
// 20-OCT-2015 DHA     First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace WSI.Cashier.Controls
{
  public class uc_radioButton : RadioButton
  {
    public uc_radioButton()
    {
      SetStyle(ControlStyles.SupportsTransparentBackColor, true);

      base.BackColor = Color.Transparent;


      SetRadioButtonStyle();
    }

    protected override void InitLayout()
    {
      base.InitLayout();
    }

    private void SetRadioButtonStyle()
    {
      CashierStyle.ControlStyle.InitStyle(this);
    }

    protected override void OnEnabledChanged(EventArgs e)
    {
      base.OnEnabledChanged(e);

      SetRadioButtonStyle();
    }

    protected override void OnSizeChanged(EventArgs e)
    {
      base.OnSizeChanged(e);
      this.Size = new Size(this.Width, 30);
    }

    protected override void OnCreateControl()
    {
      base.OnCreateControl();

      this.Padding = new Padding(10);

      SetRadioButtonStyle();
    }

    protected override void OnPaint(PaintEventArgs e)
    {
      TextFormatFlags _format_flags;
      Rectangle _text_location;
      Size _image_size;

      _image_size = new System.Drawing.Size(0, 0);

      PaintTransparentBackground(e.Graphics, this.ClientRectangle);

      if (this.Enabled)
      {
        if (this.Checked)
        {
          e.Graphics.DrawImageUnscaled(Resources.ResourceImages.radiobtnSelected, -2, -2);
          _image_size = Resources.ResourceImages.radiobtnSelected.Size;
        }
        else
        {
          e.Graphics.DrawImageUnscaled(Resources.ResourceImages.radiobtn, -2, -2);
          _image_size = Resources.ResourceImages.radiobtn.Size;
        }
      }
      else
      {
        if (this.Checked)
        {
          e.Graphics.DrawImageUnscaled(Resources.ResourceImages.radiobtnSelected_disabled, -2, -2);
          _image_size = Resources.ResourceImages.radiobtnSelected_disabled.Size;
        }
        else
        {
          e.Graphics.DrawImageUnscaled(Resources.ResourceImages.radiobtn_disabled, -2, -2);
          _image_size = Resources.ResourceImages.radiobtn_disabled.Size;
        }
      }
      
      // Set text alignment     
      _format_flags = TextFormatFlags.WordBreak;

      if (this.RightToLeft == System.Windows.Forms.RightToLeft.Yes)
      {
        _format_flags = _format_flags | TextFormatFlags.RightToLeft;
      }

      _text_location = new Rectangle(new Point(_image_size.Width - 2, 0), new Size(this.Width - _image_size.Width, this.Height));

      if (TextAlign == ContentAlignment.TopLeft ||
        TextAlign == ContentAlignment.TopCenter ||
        TextAlign == ContentAlignment.TopRight)
      {
        _format_flags = _format_flags | TextFormatFlags.Top;
      }
      else if (TextAlign == ContentAlignment.BottomLeft ||
          TextAlign == ContentAlignment.BottomCenter ||
          TextAlign == ContentAlignment.BottomRight)
      {
        _format_flags = _format_flags | TextFormatFlags.Bottom;
      }
      else if (TextAlign == ContentAlignment.MiddleLeft ||
          TextAlign == ContentAlignment.MiddleCenter ||
          TextAlign == ContentAlignment.MiddleRight)
      {
        _format_flags = _format_flags | TextFormatFlags.VerticalCenter;
      }

      if (TextAlign == ContentAlignment.TopLeft ||
       TextAlign == ContentAlignment.BottomLeft ||
       TextAlign == ContentAlignment.MiddleLeft)
      {
        _format_flags = _format_flags | TextFormatFlags.Left;
      }
      else if (TextAlign == ContentAlignment.TopCenter ||
       TextAlign == ContentAlignment.BottomCenter ||
       TextAlign == ContentAlignment.MiddleCenter)
      {
        _format_flags = _format_flags | TextFormatFlags.HorizontalCenter;
      }
      else if (TextAlign == ContentAlignment.TopRight ||
       TextAlign == ContentAlignment.BottomRight ||
       TextAlign == ContentAlignment.MiddleRight)
      {
        _format_flags = _format_flags | TextFormatFlags.Right;
      }

      // Paint the text
      TextRenderer.DrawText(e.Graphics, Text, Font, _text_location, ForeColor, Color.Transparent, _format_flags);
    }

    protected void PaintTransparentBackground(Graphics Graphics, Rectangle ClipRect)
    {
      Graphics.Clear(Color.Transparent);
      if ((this.Parent != null))
      {
        ClipRect.Offset(this.Location);
        PaintEventArgs e = new PaintEventArgs(Graphics, ClipRect);
        GraphicsState state = Graphics.Save();
        Graphics.SmoothingMode = SmoothingMode.HighSpeed;
        try
        {
          Graphics.TranslateTransform((float)-this.Location.X, (float)-this.Location.Y);
          this.InvokePaintBackground(this.Parent, e);
          this.InvokePaint(this.Parent, e);
        }
        finally
        {
          Graphics.Restore(state);
          ClipRect.Offset(-this.Location.X, -this.Location.Y);
        }
      }
    }

  }
}
