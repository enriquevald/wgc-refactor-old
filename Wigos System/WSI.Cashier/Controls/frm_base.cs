﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Security.Policy;
using System.Text;
using System.Windows.Forms;
using WSI.Common;

namespace WSI.Cashier.Controls
{
  public class frm_base : frm_base_icon
  {
    //SJA 18/01/2016 - added to allow extra funcionality on form close
    public delegate void ExitClickDelegate();
    public enum BaseFormStyle
    {
      NONE = 0,
      PRINCIPAL = 1,
    }

    private Boolean m_form_title_arrow_visible;
    private Boolean m_form_header_visible;

    private PictureBox pb_exit;
    private Panel pnl_exit;
    private uc_label lbl_title;
    private Panel pnl_title;
    private PictureBox pb_title_arrow;
    private uc_label lbl_sub_title;
    public Panel pnl_data;
    private Panel pnl_header;
    private Boolean m_custom_location;

    private Boolean m_show_close_button;

    private BaseFormStyle m_style;

    [Category("Appearance")]
    public BaseFormStyle Style
    {
      get
      {
        return m_style;
      }
      set
      {
        m_style = value;

        SetFormStyle();
      }
    }

    [Category("Appearance")]
    public Color HeaderColor
    {
      get
      {
        return pnl_header.BackColor;
      }
      set
      {
        pnl_header.BackColor = value;
      }
    }

    [Category("Appearance")]
    public String FormTitle
    {
      get
      {
        return lbl_title.Text;
      }
      set
      {
        if (value != string.Empty)
        {
          // ATB 16-FEB-2017
          Misc.WriteLog("[FORM OPEN] frm_base: " + value, Log.Type.Message);
        }
        lbl_title.Text = value;

        if (this.Visible)
        {
          AdjustControls();
        }
      }
    }

    [Category("Appearance")]
    public Boolean FormTitleArrowVisible
    {
      get
      {
        return m_form_title_arrow_visible;
      }
      set
      {
        m_form_title_arrow_visible = value;

        if (this.Visible)
        {
          AdjustControls();
        }
      }
    }

    [Category("Appearance")]
    public Boolean FormHeaderVisible
    {
      get
      {
        return m_form_header_visible;
      }
      set
      {
        m_form_header_visible = value;

        if (m_form_header_visible)
        {
          pnl_header.Height = 55;
        }
        else
        {
          pnl_header.Height = 0;
        }

      }
    }

    [Category("Appearance")]
    public String FormSubTitle
    {
      get
      {
        return lbl_title.Text;
      }
      set
      {
        lbl_sub_title.Text = value;

        if (this.Visible)
        {
          AdjustControls();
        }
      }
    }

    public Boolean CustomLocation
    {
      get
      {
        return m_custom_location;
      }
      set
      {
        m_custom_location = value;

        if (this.Visible && !DesignMode)
        {
          AdjustControls();
        }
      }
    }

    [Category("Appearance")]
    public Boolean ShowCloseButton
    {
      get
      {
        return m_show_close_button;
      }
      set
      {
        m_show_close_button = value;

        if (this.Visible && !DesignMode)
        {
          AdjustControls();
        }
      }
    }

    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_base));
      this.pnl_header = new System.Windows.Forms.Panel();
      this.pnl_title = new System.Windows.Forms.Panel();
      this.lbl_sub_title = new WSI.Cashier.Controls.uc_label();
      this.pb_title_arrow = new System.Windows.Forms.PictureBox();
      this.lbl_title = new WSI.Cashier.Controls.uc_label();
      this.pnl_exit = new System.Windows.Forms.Panel();
      this.pb_exit = new System.Windows.Forms.PictureBox();
      this.pnl_data = new System.Windows.Forms.Panel();
      this.pnl_header.SuspendLayout();
      this.pnl_title.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pb_title_arrow)).BeginInit();
      this.pnl_exit.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pb_exit)).BeginInit();
      this.SuspendLayout();
      // 
      // pnl_header
      // 
      this.pnl_header.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.pnl_header.Controls.Add(this.pnl_title);
      this.pnl_header.Controls.Add(this.pnl_exit);
      this.pnl_header.Dock = System.Windows.Forms.DockStyle.Top;
      this.pnl_header.Location = new System.Drawing.Point(0, 0);
      this.pnl_header.Name = "pnl_header";
      this.pnl_header.Size = new System.Drawing.Size(300, 55);
      this.pnl_header.TabIndex = 0;
      // 
      // pnl_title
      // 
      this.pnl_title.Controls.Add(this.lbl_sub_title);
      this.pnl_title.Controls.Add(this.pb_title_arrow);
      this.pnl_title.Controls.Add(this.lbl_title);
      this.pnl_title.Dock = System.Windows.Forms.DockStyle.Top;
      this.pnl_title.Location = new System.Drawing.Point(0, 0);
      this.pnl_title.Name = "pnl_title";
      this.pnl_title.Size = new System.Drawing.Size(100, 55);
      this.pnl_title.TabIndex = 2;
      // 
      // lbl_sub_title
      // 
      this.lbl_sub_title.AutoSize = true;
      this.lbl_sub_title.BackColor = System.Drawing.Color.Transparent;
      this.lbl_sub_title.Font = new System.Drawing.Font("Open Sans Semibold", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_sub_title.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.lbl_sub_title.Location = new System.Drawing.Point(92, 16);
      this.lbl_sub_title.Name = "lbl_sub_title";
      this.lbl_sub_title.Size = new System.Drawing.Size(110, 22);
      this.lbl_sub_title.Style = WSI.Cashier.Controls.uc_label.LabelStyle.POPUP_FULL_SCREEN_PLAYER;
      this.lbl_sub_title.TabIndex = 3;
      this.lbl_sub_title.Text = "XSUBTITLE";
      // 
      // pb_title_arrow
      // 
      this.pb_title_arrow.Image = ((System.Drawing.Image)(resources.GetObject("pb_title_arrow.Image")));
      this.pb_title_arrow.Location = new System.Drawing.Point(66, 17);
      this.pb_title_arrow.Name = "pb_title_arrow";
      this.pb_title_arrow.Size = new System.Drawing.Size(20, 20);
      this.pb_title_arrow.TabIndex = 3;
      this.pb_title_arrow.TabStop = false;
      // 
      // lbl_title
      // 
      this.lbl_title.AutoSize = true;
      this.lbl_title.BackColor = System.Drawing.Color.Transparent;
      this.lbl_title.Font = new System.Drawing.Font("Open Sans Semibold", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_title.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(167)))), ((int)(((byte)(221)))), ((int)(((byte)(241)))));
      this.lbl_title.Location = new System.Drawing.Point(0, 16);
      this.lbl_title.Name = "lbl_title";
      this.lbl_title.Size = new System.Drawing.Size(59, 22);
      this.lbl_title.Style = WSI.Cashier.Controls.uc_label.LabelStyle.POPUP_FULL_SCREEN_LEFT;
      this.lbl_title.TabIndex = 2;
      this.lbl_title.Text = "xTitle";
      // 
      // pnl_exit
      // 
      this.pnl_exit.Controls.Add(this.pb_exit);
      this.pnl_exit.Dock = System.Windows.Forms.DockStyle.Right;
      this.pnl_exit.Location = new System.Drawing.Point(245, 0);
      this.pnl_exit.Name = "pnl_exit";
      this.pnl_exit.Size = new System.Drawing.Size(55, 55);
      this.pnl_exit.TabIndex = 1;
      // 
      // pb_exit
      // 
      this.pb_exit.Image = ((System.Drawing.Image)(resources.GetObject("pb_exit.Image")));
      this.pb_exit.Location = new System.Drawing.Point(10, 10);
      this.pb_exit.Margin = new System.Windows.Forms.Padding(0, 10, 45, 0);
      this.pb_exit.Name = "pb_exit";
      this.pb_exit.Size = new System.Drawing.Size(35, 35);
      this.pb_exit.TabIndex = 0;
      this.pb_exit.TabStop = false;
      this.pb_exit.Click += new System.EventHandler(this.pb_exit_Click);
      // 
      // pnl_data
      // 
      this.pnl_data.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pnl_data.Location = new System.Drawing.Point(0, 55);
      this.pnl_data.Name = "pnl_data";
      this.pnl_data.Size = new System.Drawing.Size(300, 245);
      this.pnl_data.TabIndex = 1;
      // 
      // frm_base
      // 
      this.AutoSize = true;
      this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.ClientSize = new System.Drawing.Size(300, 300);
      this.Controls.Add(this.pnl_data);
      this.Controls.Add(this.pnl_header);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
      this.MaximumSize = new System.Drawing.Size(1024, 713);
      this.MinimumSize = new System.Drawing.Size(100, 100);
      this.Name = "frm_base";
      this.pnl_header.ResumeLayout(false);
      this.pnl_title.ResumeLayout(false);
      this.pnl_title.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pb_title_arrow)).EndInit();
      this.pnl_exit.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.pb_exit)).EndInit();
      this.ResumeLayout(false);

    }

    public frm_base()
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_base", Log.Type.Message);

      InitializeComponent();

      FormTitle = String.Empty;
      FormSubTitle = String.Empty;
      ShowCloseButton = true;
      FormHeaderVisible = true;      
    }

    private void AdjustControls()
    {
      Int32 _header_location_control_X;
      Int32 _form_location_X;
      Int32 _form_location_Y;
      Point _main_form_location;
      Size _main_form_size;
      Int32 _main_form_header_height;

      _main_form_header_height = 55;
      _header_location_control_X = 10;
      _form_location_X = 0;
      _form_location_Y = _main_form_header_height;

      _main_form_size = WindowManager.GetMainFormAreaSize();

      // Set form location
      if (this.StartPosition == FormStartPosition.Manual && !m_custom_location)
      {
        _main_form_location = WindowManager.GetMainFormAreaLocation();
        _form_location_X += _main_form_location.X + (_main_form_size.Width - this.Width) / 2;
        _form_location_Y += _main_form_location.Y;
        this.Location = new Point(_form_location_X, _form_location_Y);
      }

      // Set form size
      if (this.Width > _main_form_size.Width)
      {
        this.Width = _main_form_size.Width;
      }

      if (this.Height > _main_form_size.Height - _main_form_header_height)
      {
        this.Height = _main_form_size.Height - _main_form_header_height;
      }

      lbl_title.Location = new Point(_header_location_control_X, 16);

      if (String.IsNullOrEmpty(lbl_title.Text))
      {
        _header_location_control_X += 0;
        lbl_title.Visible = false;
      }
      else
      {
        _header_location_control_X += lbl_title.Width;
        lbl_title.Visible = true;
      }

      pb_title_arrow.Location = new Point(_header_location_control_X, 17);
      pb_title_arrow.Visible = m_form_title_arrow_visible;

      if (m_form_title_arrow_visible)
      {
        _header_location_control_X += pb_title_arrow.Width;
      }
      else
      {
        _header_location_control_X += 0;
      }

      lbl_sub_title.Location = new Point(_header_location_control_X, 16);

      pb_exit.Visible = m_show_close_button;

      // Panel Header
      if (m_form_header_visible)
      {
        pnl_header.Height = _main_form_header_height;
      }
      else
      {
        pnl_header.Height = 0;
      }

    }

    private void SetFormStyle()
    {
      CashierStyle.ControlStyle.InitStyle(this);
    }

    //SJA 18/01/2016 - added to allow extra funcionality on form close
    public event ExitClickDelegate PerformOnExitClicked;
    public virtual void pb_exit_Click(object sender, EventArgs e)
    {
      if (this.PerformOnExitClicked != null)
      {
        PerformOnExitClicked();
      }

      Misc.WriteLog("[FORM CLOSE] frm_base: " + FormTitle, Log.Type.Message);
      this.Close();
    }

    protected override void InitLayout()
    {
      base.InitLayout();

      AdjustControls();

    }

    protected override void OnLoad(EventArgs e)
    {
      base.OnLoad(e);

      AdjustControls();
    }
  }

}
