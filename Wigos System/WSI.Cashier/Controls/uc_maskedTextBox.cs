﻿//------------------------------------------------------------------------------
// Copyright © 2015-2020 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_maskedTextBox.cs
// 
//   DESCRIPTION: Implements the uc_round_textbox user control
//                Class: uc_round_textbox
//
//        AUTHOR: David Hernández
//                José Martínez
// 
// CREATION DATE: 20-OCT-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 20-OCT-2015 DHA     First release.
// 20-OCT-2015 JML     First release.
//------------------------------------------------------------------------------

using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using System.Xml.Serialization;
using WSI.Common;

namespace WSI.Cashier.Controls
{
  public class uc_maskedTextBox : Control
  {
    #region Enums
    public enum RoundMaskedTextBoxStyle
    {
      NONE = 0,
      BASIC = 1,
    }
    #endregion

    #region Delegates

    #endregion

    #region Variables

    private MaskedTextBox m_maskedtextbox = new MaskedTextBox();
    private Boolean m_resize = false;
    private Control m_container = new ContainerControl();
    private Int32 _margin = 6;
    private Rectangle m_rect_content = new Rectangle(0, 0, 1, 1);
    private Radius m_radius = new Radius();
    private Color m_forecolor;

    #endregion

    #region Properties
    public Int32 MaxLength
    {
      get
      {
        return m_maskedtextbox.MaxLength;
      }
      set
      {
        m_maskedtextbox.MaxLength = value;
      }
    }

    public Boolean ReadOnly
    {
      get
      {
        return m_maskedtextbox.ReadOnly;
      }
      set
      {
        m_maskedtextbox.ReadOnly = value;
      }
    }

    public Int32 SelectionLength
    {
      get
      {
        return m_maskedtextbox.SelectionLength;
      }
      set
      {
        m_maskedtextbox.SelectionLength = value;
      }
    }

    public Int32 SelectionStart
    {
      get
      {
        return m_maskedtextbox.SelectionStart;
      }
      set
      {
        m_maskedtextbox.SelectionStart = value;
      }
    }

    public HorizontalAlignment TextAlign
    {
      get
      {
        return m_maskedtextbox.TextAlign;
      }
      set
      {
        m_maskedtextbox.TextAlign = value;
      }
    }

    [Category("Appearance")]
    public Int32 CornerRadius
    {
      get
      {
        return m_radius.TopLeft;
      }
      set
      {
        m_radius.TopLeft = value;
        m_radius.TopRight = value;
        m_radius.BottomLeft = value;
        m_radius.BottomRight = value;

        if (DesignMode && this.Parent != null)
        {
          this.Parent.Refresh(); // Refreshes the client area of the parent control
        }
      }
    }

    private RoundMaskedTextBoxStyle m_style;

    [Category("Appearance")]
    public RoundMaskedTextBoxStyle Style
    {
      get
      {
        return m_style;
      }
      set
      {
        m_style = value;

        SetMaskedTextBoxStyle();

        if (DesignMode && this.Parent != null)
        {
          OnSizeChanged(null);
          this.Parent.Refresh(); // Refreshes the client area of the parent control
        }
      }
    }

    private Color m_border_color;
    [Category("Appearance")]
    public Color BorderColor
    {
      get
      {
        return m_border_color;
      }
      set
      {
        m_border_color = value;

        if (DesignMode && this.Parent != null)
        {
          this.Parent.Refresh(); // Refreshes the client area of the parent control
        }

        //SetButtonStyle();
      }
    }

    private Color m_backcolor;
    public new Color BackColor
    {
      get { return m_backcolor; }
      set
      {
        this.m_backcolor = value;
        m_maskedtextbox.BackColor = value;
        Invalidate(true);
      }
    }

    private Color m_text_forecolor;
    [Category("Appearance")]
    public Color TextForeColor
    {
      get
      {
        return m_text_forecolor;
      }
      set
      {
        m_text_forecolor = value;

        if (DesignMode && this.Parent != null)
        {
          this.Parent.Refresh(); // Refreshes the client area of the parent control
        }

        //SetButtonStyle();
      }
    }

    [Category("Appearance")]
    public String Mask
    {
      get
      {
        return this.m_maskedtextbox.Mask;
      }
      set
      {
        this.m_maskedtextbox.Mask = value;
      }
    }

    [EditorBrowsable(EditorBrowsableState.Always)]
    [Browsable(true)]
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
    [Bindable(true)]
    public override string Text
    {
      get
      {
        return m_maskedtextbox.Text;
      }
      set
      {
        m_maskedtextbox.Text = value;
      }
    }

    public string SelectedText
    {
      get
      {
        return m_maskedtextbox.SelectedText;
      }
      set
      {
        m_maskedtextbox.SelectedText = value;
      }
    }

    #endregion

    #region Events
    [Browsable(true)]
    public new event EventHandler TextChanged;

    [Browsable(true)]
    public new event KeyPressEventHandler KeyPress;

    [Browsable(true)]
    public new event EventHandler Enter;

    [Browsable(true)]
    public new event EventHandler Leave;

    [Browsable(true)]
    public new event EventHandler GotFocus;
    #endregion

    #region Constructors
    public uc_maskedTextBox()
    {
      SetStyle(ControlStyles.AllPaintingInWmPaint, true);
      SetStyle(ControlStyles.ContainerControl, true);
      SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
      SetStyle(ControlStyles.ResizeRedraw, true);
      SetStyle(ControlStyles.Selectable, true);
      SetStyle(ControlStyles.SupportsTransparentBackColor, true);
      SetStyle(ControlStyles.UserMouse, true);
      SetStyle(ControlStyles.UserPaint, true);
      SetStyle(ControlStyles.Selectable, true);

      base.BackColor = Color.Transparent;

      this.TabStop = false;

      this.SuspendLayout();
      m_maskedtextbox.BorderStyle = System.Windows.Forms.BorderStyle.None;
      m_maskedtextbox.Location = new System.Drawing.Point(3, 4);
      m_maskedtextbox.Size = new System.Drawing.Size(60, 13);
      m_maskedtextbox.TabIndex = 0;
      m_maskedtextbox.Margin = new Padding(0);
      m_maskedtextbox.Padding = new Padding(0);
      m_maskedtextbox.TextAlign = HorizontalAlignment.Left;
      m_maskedtextbox.Enabled = this.Enabled;
      m_maskedtextbox.TabStop = true;

      this.Controls.Add(m_maskedtextbox);

      SetMaskedTextBoxStyle();

      this.ResumeLayout(false);

      this.m_maskedtextbox.TextChanged += new EventHandler(TextBox_TextChanged);
      this.m_maskedtextbox.KeyPress += new KeyPressEventHandler(TextBox_KeyPress);
      this.m_maskedtextbox.Enter += new EventHandler(TextBox_Enter);
      this.m_maskedtextbox.Leave += new EventHandler(TextBox_Leave);
      this.m_maskedtextbox.Click += new EventHandler(TextBox_Click);
      this.m_maskedtextbox.MouseDown += new MouseEventHandler(m_textbox_MouseDown);
      this.m_maskedtextbox.KeyDown += new KeyEventHandler(TextBox_KeyDown);
    }

    #endregion

    #region OverrideMethods

    protected override void OnCreateControl()
    {
      m_forecolor = this.ForeColor;
      m_maskedtextbox.Enabled = this.Enabled;

      this.TabStop = false;

      base.OnCreateControl();
    }

    protected override void OnEnabledChanged(EventArgs e)
    {
      base.OnEnabledChanged(e);

      this.m_maskedtextbox.Enabled = this.Enabled;

      SetMaskedTextBoxStyle();
    }

    protected override void OnResize(EventArgs e)
    {
      if (m_resize)
      {

        m_resize = false;
        AdjustControls();

        Invalidate(true);
      }
      else
        Invalidate(true);
    }

    protected override void OnForeColorChanged(EventArgs e)
    {
      m_maskedtextbox.ForeColor = this.ForeColor;
      base.OnForeColorChanged(e);
    }

    protected override void OnPaint(PaintEventArgs e)
    {
      e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;

      Brush _br_background;
      Rectangle _rect_cont;
      Rectangle _rect_outer;
      GraphicsPath _path_content_border;
      GraphicsPath _path_outer_border;
      Pen _pen_outer_border;

      e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;

      //content border
      _rect_cont = m_rect_content;
      _rect_cont.X += 1;
      _rect_cont.Y += 1;
      _rect_cont.Width -= 3;
      _rect_cont.Height -= 3;
      _path_content_border = CreateRoundRectangle(_rect_cont, m_radius.TopLeft, m_radius.TopRight, m_radius.BottomRight,
          m_radius.BottomLeft);

      //outer border
      _rect_outer = m_rect_content;
      _rect_outer.Width -= 1;
      _rect_outer.Height -= 1;
      _path_outer_border = CreateRoundRectangle(_rect_outer, m_radius.TopLeft, m_radius.TopRight, m_radius.BottomRight,
          m_radius.BottomLeft);

      _br_background = new SolidBrush(BackColor);
      _pen_outer_border = new Pen(BorderColor, 2);

      //draw
      e.Graphics.FillPath(_br_background, _path_content_border);

      e.Graphics.DrawPath(_pen_outer_border, _path_outer_border);

      _path_content_border.Dispose();
      _path_outer_border.Dispose();
      _pen_outer_border.Dispose();
      _br_background.Dispose();

    }

    #endregion

    #region PrivateMethods
    protected virtual void OnTextChanged(object sender, EventArgs e)
    {
      Int32 _cursor_position;

      if (TextChanged != null)
      {
        if (m_style == RoundMaskedTextBoxStyle.BASIC)
        {
          _cursor_position = m_maskedtextbox.SelectionStart;
          this.Text = this.Text.ToUpper();
          m_maskedtextbox.SelectionStart = _cursor_position;
        }
        TextChanged(this, e);
      }

    }

    private void TextBox_TextChanged(object sender, EventArgs e)
    {
      OnTextChanged(this, e);
    }

    private void TextBox_Click(object sender, EventArgs e)
    {
      OnClick(e);
    }

    void m_textbox_MouseDown(object sender, MouseEventArgs e)
    {

      base.OnMouseDown(e);
    }

    void TextBox_KeyDown(object sender, KeyEventArgs e)
    {
      OnKeyDown(e);
    }

    protected override void OnClick(EventArgs e)
    {
      this.m_maskedtextbox.BringToFront();
      this.m_maskedtextbox.Focus();

      base.OnClick(e);
    }

    protected virtual void OnClick(object sender, EventArgs e)
    {
      this.m_maskedtextbox.BringToFront();
      this.m_maskedtextbox.Focus();

      OnClick(sender, e);
    }

    protected override void OnKeyDown(KeyEventArgs e)
    {
      base.OnKeyDown(e);

      switch (e.KeyData)
      {
        case Keys.Shift | Keys.Tab:
          this.SelectNextControl(this, false, true, true, true);
          break;
      }

    }

    protected virtual void OnKeyPress(object sender, KeyPressEventArgs e)
    {

      if (KeyPress != null)
      {
        KeyPress(this, e);
      }
    }

    private void TextBox_KeyPress(object sender, KeyPressEventArgs e)
    {
      OnKeyPress(this, e);
    }

    protected virtual void OnEnter(object sender, EventArgs e)
    {
      this.m_maskedtextbox.BringToFront();

      if (Enter != null)
      {
        Enter(this, e);
      }
    }

    private void TextBox_Enter(object sender, EventArgs e)
    {
      OnEnter(this, e);
    }

    protected virtual void OnLeave(object sender, EventArgs e)
    {
      if (Leave != null)
      {
        Leave(this, e);
      }
    }

    private void TextBox_Leave(object sender, EventArgs e)
    {
      OnLeave(this, e);
    }

    protected virtual void OnGotFocus(object sender, EventArgs e)
    {
      if (GotFocus != null)
      {
        GotFocus(sender, e);
      }
    }

    protected override void OnGotFocus(EventArgs e)
    {
      this.m_maskedtextbox.BringToFront();
      this.m_maskedtextbox.Focus();

      base.OnGotFocus(e);
    }

    private void AdjustControls()
    {
      this.SuspendLayout();

      m_resize = true;

      // Localización del textbox
      m_maskedtextbox.Left = _margin;

      if (m_style == RoundMaskedTextBoxStyle.BASIC)
      {
        this.Height = 40;
      }

      m_maskedtextbox.Top = (this.Height - m_maskedtextbox.Height) / 2;
      m_maskedtextbox.WordWrap = false;

      m_maskedtextbox.Width = this.Width - 2 * m_maskedtextbox.Left;

      m_rect_content = new Rectangle(ClientRectangle.Left + 1, ClientRectangle.Top,
          ClientRectangle.Width - 2, this.Height);

      this.ResumeLayout();

      Invalidate(true);


    }

    private void SetMaskedTextBoxStyle()
    {
      CashierStyle.ControlStyle.InitStyle(this);

      AdjustControls();
    }
    #endregion

    #region PublicMethods
    public void Select(Int32 Start, Int32 Length)
    {
      this.m_maskedtextbox.Select(Start, Length);
    }

    public void SelectAll()
    {
      this.m_maskedtextbox.SelectAll();
    }
    #endregion

    #region Render

    public static GraphicsPath CreateRoundRectangle(Rectangle Rectangle, Int32 TopLeftRadius, Int32 TopRightRadius, Int32 BottomRightRadius, Int32 BottomLeftRadius)
    {
      GraphicsPath _path = new GraphicsPath();
      Int32 _left;
      Int32 _top;
      Int32 _width;
      Int32 _height;

      _left = Rectangle.Left;
      _top = Rectangle.Top;
      _width = Rectangle.Width;
      _height = Rectangle.Height;

      if (TopLeftRadius > 0)
      {
        _path.AddArc(_left, _top, TopLeftRadius * 2, TopLeftRadius * 2, 180, 90);
      }
      _path.AddLine(_left + TopLeftRadius, _top, _left + _width - TopRightRadius, _top);
      if (TopRightRadius > 0)
      {
        _path.AddArc(_left + _width - TopRightRadius * 2, _top, TopRightRadius * 2, TopRightRadius * 2, 270, 90);
      }
      _path.AddLine(_left + _width, _top + TopRightRadius, _left + _width, _top + _height - BottomRightRadius);
      if (BottomRightRadius > 0)
      {
        _path.AddArc(_left + _width - BottomRightRadius * 2, _top + _height - BottomRightRadius * 2,
            BottomRightRadius * 2, BottomRightRadius * 2, 0, 90);
      }
      _path.AddLine(_left + _width - BottomRightRadius, _top + _height, _left + BottomLeftRadius, _top + _height);
      if (BottomLeftRadius > 0)
      {
        _path.AddArc(_left, _top + _height - BottomLeftRadius * 2, BottomLeftRadius * 2, BottomLeftRadius * 2, 90, 90);
      }
      _path.AddLine(_left, _top + _height - BottomLeftRadius, _left, _top + TopLeftRadius);
      _path.CloseFigure();
      return _path;
    }

    #endregion

    public class Radius
    {
      private int _topLeft = 0;

      public int TopLeft
      {
        get { return _topLeft; }
        set { _topLeft = value; }
      }

      private int _topRight = 0;

      public int TopRight
      {
        get { return _topRight; }
        set { _topRight = value; }
      }

      private int _bottomLeft = 0;

      public int BottomLeft
      {
        get { return _bottomLeft; }
        set { _bottomLeft = value; }
      }

      private int _bottomRight = 0;

      public int BottomRight
      {
        get { return _bottomRight; }
        set { _bottomRight = value; }
      }
    }
  }
}
