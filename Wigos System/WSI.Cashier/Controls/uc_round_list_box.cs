﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using System.Xml.Serialization;
using WSI.Common;

namespace WSI.Cashier.Controls
{
  public class uc_round_list_box : ListControl
  {
    #region Enums

    public enum RoundListBoxStyle
    {
      NONE = 0,
      BASIC = 1,
      COMBOBOX_LIST = 2,
      COMBOBOX_LIST_CUSTOM = 3,
    }

    #endregion

    #region Delegates

    public delegate void RLBDrawItemEventHandler(object sender, DrawItemEventArgs e);
    public delegate void RLBMeasureItemEventHandler(object sender, MeasureItemEventArgs e);

    #endregion

    #region Variables

    private ListBox m_list_box = new ListBox();
    private Boolean m_resize = false;
    private Int32 m_selectedIndex = -1;
    private Rectangle m_rect_content = new Rectangle(0, 0, 1, 1);
    private Color m_back_color;
    private Color m_border_color;
    private RoundListBoxStyle m_style;
    private Radius m_radius = new Radius();

    #endregion

    #region Delegates

    [Category("Behavior")]
    public event EventHandler SelectedIndexChanged;

    [Category("Behavior")]
    public event RLBMeasureItemEventHandler MeasureItem;

    public new event EventHandler DataSourceChanged;
    public new event EventHandler DisplayMemberChanged;
    public new event EventHandler ValueMemberChanged;
    
    #endregion

    #region Properties
    [Category("Appearance")]
    public Int32 CornerRadius
    {
      get
      {
        return m_radius.TopLeft;
      }
      set
      {
        m_radius.TopLeft = value;
        m_radius.TopRight = value;
        m_radius.BottomLeft = value;
        m_radius.BottomRight = value;

        if (DesignMode && this.Parent != null)
        {
          this.Parent.Refresh(); // Refreshes the client area of the parent control
        }
      }
    }

    [Category("Appearance")]
    public RoundListBoxStyle Style
    {
      get
      {
        return m_style;
      }
      set
      {
        m_style = value;

        SetListBoxStyle();

        if (DesignMode && this.Parent != null)
        {
          this.Parent.Refresh(); // Refreshes the client area of the parent control
        }
      }
    }

    [Category("Appearance")]
    public Color BorderColor
    {
      get
      {
        return m_border_color;
      }
      set
      {
        m_border_color = value;

        if (DesignMode && this.Parent != null)
        {
          this.Parent.Refresh(); // Refreshes the client area of the parent control
        }
      }
    }

    [Category("Behavior")]
    public DrawMode DrawMode
    {
      get { return m_list_box.DrawMode; }
      set
      {
        m_list_box.DrawMode = value;
      }
    }

    [Category("Appearance")]
    public new Color BackColor
    {
      get { return m_back_color; }
      set
      {
        this.m_back_color = value;
        m_list_box.BackColor = value;
        Invalidate(true);
      }
    }

    public new object DataSource
    {
      get { return base.DataSource; }
      set
      {
        m_list_box.DataSource = value;
        base.DataSource = value;
        OnDataSourceChanged(System.EventArgs.Empty);
      }
    }

    public ListBox.ObjectCollection Items
    {
      get { return m_list_box.Items; }
    }

    public ListBox.SelectedObjectCollection SelectedItems
    {
      get { return m_list_box.SelectedItems; }
    }

    public Boolean Sorted
    {
      get { return m_list_box.Sorted; }
      set { m_list_box.Sorted = value; }
    }

    public Int32 ItemHeight
    {
      get { return m_list_box.ItemHeight; }
      set { m_list_box.ItemHeight = value; }
    }

    public Int32 TopIndex 
    {
      get { return this.m_list_box.TopIndex; }
      set { this.m_list_box.TopIndex = value; }
    }

    #endregion

    #region Contructors

    public uc_round_list_box()
    {
      SetStyle(ControlStyles.AllPaintingInWmPaint, true);
      SetStyle(ControlStyles.ContainerControl, true);
      SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
      SetStyle(ControlStyles.ResizeRedraw, true);
      SetStyle(ControlStyles.SupportsTransparentBackColor, true);
      SetStyle(ControlStyles.UserMouse, true);
      SetStyle(ControlStyles.UserPaint, true);
      SetStyle(ControlStyles.Selectable, true);

      this.ItemHeight = 40;

      SetListBoxStyle();

      base.BackColor = Color.Transparent;

      this.SuspendLayout();
      m_list_box = new ListBox();
      m_list_box.DrawMode = System.Windows.Forms.DrawMode.Normal;
      m_list_box.IntegralHeight = true;
      m_list_box.ItemHeight = this.ItemHeight;
      m_list_box.BorderStyle = BorderStyle.None;
      m_list_box.SelectionMode = SelectionMode.One;
      m_list_box.BindingContext = new BindingContext();
      m_list_box.Dock = DockStyle.None;
      this.Controls.Add(m_list_box);
      this.ResumeLayout(false);

      m_list_box.MeasureItem += new MeasureItemEventHandler(_listBox_MeasureItem);
      m_list_box.DrawItem += new DrawItemEventHandler(_listBox_DrawItem);
      m_list_box.MouseClick += new MouseEventHandler(_listBox_MouseClick);
      m_list_box.MouseWheel += new MouseEventHandler(_listBox_MouseWheel);
    }

    #endregion

    #region Overrides

    protected override void OnDataSourceChanged(EventArgs e)
    {
      this.SelectedIndex = 0;
      base.OnDataSourceChanged(e);

      if (DataSourceChanged != null)
      {
        DataSourceChanged(this, e);
      }
    }

    protected override void OnDisplayMemberChanged(EventArgs e)
    {
      m_list_box.DisplayMember = this.DisplayMember;
      this.SelectedIndex = this.SelectedIndex;
      base.OnDisplayMemberChanged(e);

      if (DisplayMemberChanged != null)
      {
        DisplayMemberChanged(this, e);
      }
    }

    protected override void OnEnabledChanged(EventArgs e)
    {
      Invalidate(true);
      base.OnEnabledChanged(e);

      SetListBoxStyle();
    }

    protected override void OnCreateControl()
    {
      base.OnCreateControl();
    }

    protected override void OnForeColorChanged(EventArgs e)
    {
      m_list_box.ForeColor = this.ForeColor;
      base.OnForeColorChanged(e);
    }

    protected override void OnFormatInfoChanged(EventArgs e)
    {
      m_list_box.FormatInfo = this.FormatInfo;
      base.OnFormatInfoChanged(e);
    }

    protected override void OnFormatStringChanged(EventArgs e)
    {
      m_list_box.FormatString = this.FormatString;
      base.OnFormatStringChanged(e);
    }

    protected override void OnFormattingEnabledChanged(EventArgs e)
    {
      m_list_box.FormattingEnabled = this.FormattingEnabled;
      base.OnFormattingEnabledChanged(e);
    }

    public override Font Font
    {
      get
      {
        return base.Font;
      }
      set
      {
        m_resize = true;
        m_list_box.Font = value;
        base.Font = value;
        Invalidate(true);
      }
    }

    protected override void OnSelectedIndexChanged(EventArgs e)
    {
      if (SelectedIndexChanged != null)
        SelectedIndexChanged(this, e);

      m_list_box.TopIndex = m_list_box.SelectedIndex;

      base.OnSelectedIndexChanged(e);
    }

    protected override void OnValueMemberChanged(EventArgs e)
    {
      m_list_box.ValueMember = this.ValueMember;
      this.SelectedIndex = this.SelectedIndex;
      base.OnValueMemberChanged(e);

      if (ValueMemberChanged != null)
      {
        ValueMemberChanged(this, e);
      }
    }

    protected override void OnResize(EventArgs e)
    {
      if (m_resize)
      {

        m_resize = false;
        AdjustControls();

        Invalidate(true);
      }
      else
        Invalidate(true);
    }
    
    //SJA 23-02-2016 Added to push shift tab to host control

    protected override bool ProcessCmdKey(ref Message Msg, Keys KeyData)
    {
      bool _base_result = base.ProcessCmdKey(ref Msg, KeyData);

      if (KeyData == Keys.Tab && Focused)
      {
        OnKeyDown(new KeyEventArgs(Keys.Tab));
        return true;
      }

      if (KeyData != (Keys.Tab | Keys.Shift) || !Focused)
      {
        return _base_result;
      }
      OnKeyDown(new KeyEventArgs(Keys.Shift | Keys.Tab));
      return true;
    }


    protected override void OnPaint(PaintEventArgs e)
    {
      Brush _br_background;
      Rectangle _rect_cont;
      Rectangle _rect_outer;
      GraphicsPath _path_content_border;
      GraphicsPath _path_outer_border;
      Pen _pen_outer_border;

      e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
      
      //content border
      _rect_cont = m_rect_content;
      _rect_cont.X += 1;
      _rect_cont.Y += 1;
      _rect_cont.Width -= 3;
      _rect_cont.Height -= 3;
      _path_content_border = CreateRoundRectangle(_rect_cont, m_radius.TopLeft, m_radius.TopRight, m_radius.BottomRight,
          m_radius.BottomLeft);

      //outer border
      _rect_outer = m_rect_content;
      _rect_outer.Width -= 1;
      _rect_outer.Height -= 1;
      _path_outer_border = CreateRoundRectangle(_rect_outer, m_radius.TopLeft, m_radius.TopRight, m_radius.BottomRight, m_radius.BottomLeft);

      _br_background = new SolidBrush(BackColor);
      _pen_outer_border = new Pen(BorderColor, 2);

      //draw
      e.Graphics.FillPath(_br_background, _path_content_border);

      e.Graphics.DrawPath(_pen_outer_border, _path_outer_border);

      e.Graphics.SetClip(_path_outer_border);

      _path_content_border.Dispose();
      _path_outer_border.Dispose();
      _pen_outer_border.Dispose();
      _br_background.Dispose();
    }

    protected override void OnKeyDown(KeyEventArgs e)
    {
      base.OnKeyDown(e);
    }

    protected override bool IsInputKey(Keys keyData)
    {
      switch (keyData)
      {
        case Keys.Right:
        case Keys.Left:
        case Keys.Up:
        case Keys.Down:
        case Keys.Tab:
          return true;
        case Keys.Shift | Keys.Right:
        case Keys.Shift | Keys.Left:
        case Keys.Shift | Keys.Up:
        case Keys.Shift | Keys.Down:
          return true;
      }
      return base.IsInputKey(keyData);
    }

    protected override void OnMouseWheel(MouseEventArgs e)
    {
      if (e.Delta < 0)
        this.SelectedIndex = m_list_box.SelectedIndex + 1;
      else if (e.Delta > 0)
      {
        if (this.SelectedIndex > 0)
          this.SelectedIndex = m_list_box.SelectedIndex - 1;
      }

      base.OnMouseWheel(e);
    }

    protected override void OnKeyPress(KeyPressEventArgs e)
    {
      base.OnKeyPress(e);
    }

    protected override void OnKeyUp(KeyEventArgs e)
    {
      base.OnKeyUp(e);
    }

    protected override void OnMouseClick(MouseEventArgs e)
    {
      if (m_list_box.Items.Count == 0)
      {
        return;
      }

      if (m_list_box.SelectedItems.Count != 1)
      {
        return;
      }

      this.SelectedIndex = m_list_box.SelectedIndex;

      base.OnMouseClick(e);
    }

    public override int SelectedIndex
    {
      get { return m_selectedIndex; }
      set
      {
        if (m_list_box != null)
        {
          if (m_list_box.Items.Count == 0)
            return;

          if ((this.DataSource != null) && value == -1)
            return;

          if (value <= (m_list_box.Items.Count - 1) && value >= -1)
          {
            try
            {
              m_list_box.SelectedIndex = value;
              m_selectedIndex = value;
              this.m_list_box.TopIndex = value;
              OnSelectedIndexChanged(EventArgs.Empty);
            }
            catch (Exception)
            {

            }
          }
        }
      }
    }

    public object SelectedItem
    {
      get { return m_list_box.SelectedItem; }
      set
      {
        m_list_box.SelectedItem = value;
        this.SelectedIndex = m_list_box.SelectedIndex;
      }
    }

    protected override void RefreshItem(int index)
    {
    }

    public new void RefreshItems()
    {
      base.RefreshItems();
    }

    protected override void SetItemCore(int index, object value)
    {
      base.SetItemCore(index, value);
    }

    protected override void SetItemsCore(System.Collections.IList items)
    {
    }

    #endregion

    #region NestedControlsEvents

    void _listBox_MouseClick(object sender, MouseEventArgs e)
    {
      OnMouseClick(e);
    }

    void _listBox_MouseWheel(object sender, MouseEventArgs e)
    {
      OnMouseWheel(e);
    }
    
    void _listBox_DrawItem(object sender, DrawItemEventArgs e)
    {
      Point _point1;
      Point _point2;
      Int32 _margin;
      Rectangle _rect_string;
      StringFormat _string_format;

      _margin = 5;
      _string_format = new StringFormat();
      _string_format.Alignment = StringAlignment.Near;
      _string_format.LineAlignment = StringAlignment.Center;
      _string_format.Trimming = StringTrimming.None;
      _string_format.FormatFlags = StringFormatFlags.NoWrap;

      if (e.Index >= 0)
      {
        _rect_string = new Rectangle(e.Bounds.X + _margin, e.Bounds.Y, e.Bounds.Width - _margin, e.Bounds.Height);
        _point1 = new Point(0, e.Bounds.Y + e.Bounds.Height - 1);
        _point2 = new Point(e.Bounds.Width, e.Bounds.Y + e.Bounds.Height - 1);

        if ((e.State & DrawItemState.Selected) == DrawItemState.Selected)
        {

          e.Graphics.FillRectangle(new SolidBrush(ControlPaint.LightLight(BackColor)), e.Bounds);
          e.DrawFocusRectangle();

        }
        else
        {
          e.Graphics.FillRectangle(new SolidBrush(BackColor), e.Bounds);
        }

        e.Graphics.DrawString(
             (string)this.GetItemText(this.m_list_box.Items[e.Index]),
             this.Font,
             new SolidBrush(this.ForeColor),
             _rect_string,
             _string_format);

        if (e.Index < this.Items.Count - 1)
        {
          e.Graphics.DrawLine(new Pen(this.BorderColor, 1), _point1, _point2);
        }

      }
    }

    void _listBox_MeasureItem(object sender, MeasureItemEventArgs e)
    {
      if (MeasureItem != null)
      {
        MeasureItem(this, e);
      }
    }

    #endregion

    #region PrivateMethods
    private void AdjustControls()
    {
      this.SuspendLayout();

      m_resize = true;

      m_rect_content = new Rectangle(ClientRectangle.Left + 1, ClientRectangle.Top + 1,
          ClientRectangle.Width - 2, ClientRectangle.Height - 2);

      m_list_box.Location = new Point(m_rect_content.X + 3, m_rect_content.Y + 3);
      m_list_box.Width = m_rect_content.Width - 6;
      m_list_box.Height = m_rect_content.Height - 6;

      this.ResumeLayout();

      Invalidate(true);
    }

    private void SetListBoxStyle()
    {
      CashierStyle.ControlStyle.InitStyle(this);
    }
    #endregion

    #region PublicMethods

    public Int32 GetItemHeight(Int32 Index)
    {
      return m_list_box.GetItemHeight(Index);
    }

    public Rectangle GetItemRectangle(Int32 Index)
    {
      return m_list_box.GetItemRectangle(Index);
    }

    #endregion

    #region Render

    public static GraphicsPath CreateRoundRectangle(Rectangle Rectangle, Int32 TopLeftRadius, Int32 TopRightRadius, Int32 BottomRightRadius, Int32 BottomLeftRadius)
    {
      GraphicsPath _path = new GraphicsPath();
      Int32 _left;
      Int32 _top;
      Int32 _width;
      Int32 _height;

      _left = Rectangle.Left;
      _top = Rectangle.Top;
      _width = Rectangle.Width;
      _height = Rectangle.Height;

      if (TopLeftRadius > 0)
      {
        _path.AddArc(_left, _top, TopLeftRadius * 2, TopLeftRadius * 2, 180, 90);
      }
      _path.AddLine(_left + TopLeftRadius, _top, _left + _width - TopRightRadius, _top);
      if (TopRightRadius > 0)
      {
        _path.AddArc(_left + _width - TopRightRadius * 2, _top, TopRightRadius * 2, TopRightRadius * 2, 270, 90);
      }
      _path.AddLine(_left + _width, _top + TopRightRadius, _left + _width, _top + _height - BottomRightRadius);
      if (BottomRightRadius > 0)
      {
        _path.AddArc(_left + _width - BottomRightRadius * 2, _top + _height - BottomRightRadius * 2,
            BottomRightRadius * 2, BottomRightRadius * 2, 0, 90);
      }
      _path.AddLine(_left + _width - BottomRightRadius, _top + _height, _left + BottomLeftRadius, _top + _height);
      if (BottomLeftRadius > 0)
      {
        _path.AddArc(_left, _top + _height - BottomLeftRadius * 2, BottomLeftRadius * 2, BottomLeftRadius * 2, 90, 90);
      }
      _path.AddLine(_left, _top + _height - BottomLeftRadius, _left, _top + TopLeftRadius);
      _path.CloseFigure();
      return _path;
    }

    #endregion

    public class Radius
    {
      private int _topLeft = 0;

      public int TopLeft
      {
        get { return _topLeft; }
        set { _topLeft = value; }
      }

      private int _topRight = 0;

      public int TopRight
      {
        get { return _topRight; }
        set { _topRight = value; }
      }

      private int _bottomLeft = 0;

      public int BottomLeft
      {
        get { return _bottomLeft; }
        set { _bottomLeft = value; }
      }

      private int _bottomRight = 0;

      public int BottomRight
      {
        get { return _bottomRight; }
        set { _bottomRight = value; }
      }
    }
  }
}
