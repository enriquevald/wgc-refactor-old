﻿namespace WSI.Cashier.Controls
{
  partial class uc_round_webbrowser
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(uc_round_webbrowser));
      this.web_browser = new System.Windows.Forms.WebBrowser();
      this.pnl_background = new System.Windows.Forms.Panel();
      this.btn_horizontal_left = new WSI.Cashier.Controls.uc_round_button();
      this.btn_horizontal_right = new WSI.Cashier.Controls.uc_round_button();
      this.btn_vertical_bottom = new WSI.Cashier.Controls.uc_round_button();
      this.btn_vertical_top = new WSI.Cashier.Controls.uc_round_button();
      this.pnl_background.SuspendLayout();
      this.SuspendLayout();
      // 
      // web_browser
      // 
      this.web_browser.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.web_browser.IsWebBrowserContextMenuEnabled = false;
      this.web_browser.Location = new System.Drawing.Point(0, 0);
      this.web_browser.Margin = new System.Windows.Forms.Padding(0);
      this.web_browser.Name = "web_browser";
      this.web_browser.ScrollBarsEnabled = false;
      this.web_browser.Size = new System.Drawing.Size(372, 214);
      this.web_browser.TabIndex = 0;
      this.web_browser.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.web_browser_DocumentCompleted);
      // 
      // pnl_background
      // 
      this.pnl_background.Controls.Add(this.web_browser);
      this.pnl_background.Controls.Add(this.btn_horizontal_left);
      this.pnl_background.Controls.Add(this.btn_horizontal_right);
      this.pnl_background.Controls.Add(this.btn_vertical_bottom);
      this.pnl_background.Controls.Add(this.btn_vertical_top);
      this.pnl_background.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pnl_background.Location = new System.Drawing.Point(0, 0);
      this.pnl_background.Margin = new System.Windows.Forms.Padding(0);
      this.pnl_background.Name = "pnl_background";
      this.pnl_background.Size = new System.Drawing.Size(398, 240);
      this.pnl_background.TabIndex = 8;
      // 
      // btn_horizontal_left
      // 
      this.btn_horizontal_left.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.btn_horizontal_left.BackColor = System.Drawing.Color.Transparent;
      this.btn_horizontal_left.CornerRadius = 0;
      this.btn_horizontal_left.FlatAppearance.BorderColor = System.Drawing.SystemColors.Control;
      this.btn_horizontal_left.FlatAppearance.BorderSize = 0;
      this.btn_horizontal_left.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_horizontal_left.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_horizontal_left.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_horizontal_left.Image = ((System.Drawing.Image)(resources.GetObject("btn_horizontal_left.Image")));
      this.btn_horizontal_left.IsSelected = false;
      this.btn_horizontal_left.Location = new System.Drawing.Point(0, 214);
      this.btn_horizontal_left.Margin = new System.Windows.Forms.Padding(0);
      this.btn_horizontal_left.Name = "btn_horizontal_left";
      this.btn_horizontal_left.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_horizontal_left.Size = new System.Drawing.Size(25, 25);
      this.btn_horizontal_left.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.NONE;
      this.btn_horizontal_left.TabIndex = 3;
      this.btn_horizontal_left.UseVisualStyleBackColor = false;
      this.btn_horizontal_left.Click += new System.EventHandler(this.btn_horizontal_left_Click);
      this.btn_horizontal_left.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_horizontal_left_MouseDown);
      this.btn_horizontal_left.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_horizontal_left_MouseUp);
      // 
      // btn_horizontal_right
      // 
      this.btn_horizontal_right.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_horizontal_right.BackColor = System.Drawing.Color.Transparent;
      this.btn_horizontal_right.CornerRadius = 0;
      this.btn_horizontal_right.FlatAppearance.BorderColor = System.Drawing.SystemColors.Control;
      this.btn_horizontal_right.FlatAppearance.BorderSize = 0;
      this.btn_horizontal_right.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_horizontal_right.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_horizontal_right.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_horizontal_right.Image = ((System.Drawing.Image)(resources.GetObject("btn_horizontal_right.Image")));
      this.btn_horizontal_right.IsSelected = false;
      this.btn_horizontal_right.Location = new System.Drawing.Point(347, 214);
      this.btn_horizontal_right.Margin = new System.Windows.Forms.Padding(0);
      this.btn_horizontal_right.MaximumSize = new System.Drawing.Size(25, 25);
      this.btn_horizontal_right.Name = "btn_horizontal_right";
      this.btn_horizontal_right.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_horizontal_right.Size = new System.Drawing.Size(25, 25);
      this.btn_horizontal_right.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.NONE;
      this.btn_horizontal_right.TabIndex = 4;
      this.btn_horizontal_right.UseVisualStyleBackColor = false;
      this.btn_horizontal_right.Click += new System.EventHandler(this.btn_horizontal_right_Click);
      this.btn_horizontal_right.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_horizontal_right_MouseDown);
      this.btn_horizontal_right.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_horizontal_right_MouseUp);
      // 
      // btn_vertical_bottom
      // 
      this.btn_vertical_bottom.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_vertical_bottom.BackColor = System.Drawing.Color.Transparent;
      this.btn_vertical_bottom.CornerRadius = 0;
      this.btn_vertical_bottom.FlatAppearance.BorderColor = System.Drawing.SystemColors.Control;
      this.btn_vertical_bottom.FlatAppearance.BorderSize = 0;
      this.btn_vertical_bottom.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_vertical_bottom.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_vertical_bottom.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_vertical_bottom.Image = ((System.Drawing.Image)(resources.GetObject("btn_vertical_bottom.Image")));
      this.btn_vertical_bottom.IsSelected = false;
      this.btn_vertical_bottom.Location = new System.Drawing.Point(372, 189);
      this.btn_vertical_bottom.Name = "btn_vertical_bottom";
      this.btn_vertical_bottom.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_vertical_bottom.Size = new System.Drawing.Size(25, 25);
      this.btn_vertical_bottom.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.NONE;
      this.btn_vertical_bottom.TabIndex = 7;
      this.btn_vertical_bottom.UseVisualStyleBackColor = false;
      this.btn_vertical_bottom.Click += new System.EventHandler(this.btn_vertical_bottom_Click);
      this.btn_vertical_bottom.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_vertical_bottom_MouseDown);
      this.btn_vertical_bottom.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_vertical_bottom_MouseUp);
      // 
      // btn_vertical_top
      // 
      this.btn_vertical_top.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_vertical_top.BackColor = System.Drawing.Color.Transparent;
      this.btn_vertical_top.CornerRadius = 0;
      this.btn_vertical_top.FlatAppearance.BorderColor = System.Drawing.SystemColors.Control;
      this.btn_vertical_top.FlatAppearance.BorderSize = 0;
      this.btn_vertical_top.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_vertical_top.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_vertical_top.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_vertical_top.Image = ((System.Drawing.Image)(resources.GetObject("btn_vertical_top.Image")));
      this.btn_vertical_top.IsSelected = false;
      this.btn_vertical_top.Location = new System.Drawing.Point(372, 0);
      this.btn_vertical_top.Margin = new System.Windows.Forms.Padding(0);
      this.btn_vertical_top.Name = "btn_vertical_top";
      this.btn_vertical_top.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_vertical_top.Size = new System.Drawing.Size(25, 25);
      this.btn_vertical_top.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.NONE;
      this.btn_vertical_top.TabIndex = 6;
      this.btn_vertical_top.UseVisualStyleBackColor = false;
      this.btn_vertical_top.Click += new System.EventHandler(this.btn_vertical_top_Click);
      this.btn_vertical_top.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_vertical_top_MouseDown);
      this.btn_vertical_top.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_vertical_top_MouseUp);
      // 
      // uc_round_webbrowser
      // 
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
      this.AutoSize = true;
      this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.Controls.Add(this.pnl_background);
      this.Margin = new System.Windows.Forms.Padding(0);
      this.MinimumSize = new System.Drawing.Size(105, 105);
      this.Name = "uc_round_webbrowser";
      this.Size = new System.Drawing.Size(398, 240);
      this.pnl_background.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.WebBrowser web_browser;
    private uc_round_button btn_horizontal_right;
    private uc_round_button btn_horizontal_left;
    private uc_round_button btn_vertical_top;
    private uc_round_button btn_vertical_bottom;
    private System.Windows.Forms.Panel pnl_background;
  }
}
