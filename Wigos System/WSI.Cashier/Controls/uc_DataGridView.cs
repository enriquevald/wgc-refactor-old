﻿//------------------------------------------------------------------------------
// Copyright © 2015-2020 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_DataGridView.cs
// 
//   DESCRIPTION: Implements the uc_DataGridView user control
//                Class: uc_DataGridView
//
//        AUTHOR: José Martínez
//                David Hernández
// 
// CREATION DATE: 20-OCT-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 20-OCT-2015 JML     First release.
// 20-OCT-2015 DHA     First release.
// 26-JUL-2016 PDM     PBI 15444:Visitas / Recepción: MEJORAS - Cajero - Expandir formulario de resultados de búsqueda
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;


namespace WSI.Cashier.Controls
{
  public class uc_DataGridView : DataGridView
  {
    public enum DataGridViewStyle
    {
      NONE = 0,
      WITH_ALL_ROUND_CORNERS = 1,
      WITH_TOP_ROUND_BORDERS = 2,
      WITH_BOTTOM_ROUND_BORDERS = 3,
      WITHOUT_ROUND_BORDERS = 4,
      PROMOTIONS = 5,
    }

    private Color m_parent_back_color;

    private DataGridViewStyle m_style;

    [XmlElement(ElementName = "Style"), Category("Appearance")]
    public DataGridViewStyle Style
    {
      get
      {
        return m_style;
      }
      set
      {
        m_style = value;

        SetDataGridViewStyle();
      }
    }

    private Int32 m_corners_radius;

    [XmlElement(ElementName = "CornerRadius"), Category("Appearance")]
    public Int32 CornerRadius
    {
      get
      {
        return m_corners_radius;
      }
      set
      {
        if (value <= 0)
        {
          value = 1;
        }

        m_corners_radius = value;

        if (DesignMode && this.Parent != null)
        {
          this.Parent.Refresh(); // Refreshes the client area of the parent control
        }
      }
    }

    private DataGridViewHeaderBorderStyle m_header_border_style;

    [XmlElement(ElementName = "HeaderBorderStyle"), Category("Appearance")]
    public DataGridViewHeaderBorderStyle HeaderBorderStyle
    {
      get
      {
        return m_header_border_style;
      }
      set
      {
        m_header_border_style = value;
      }
    }

    private Color m_header_default_cell_backcolor;

    [XmlElement(ElementName = "HeaderDefaultCellBackColor"), Category("Appearance")]
    public Color HeaderDefaultCellBackColor
    {
      get
      {
        return m_header_default_cell_backcolor;
      }
      set
      {
        m_header_default_cell_backcolor = value;

      }
    }

    private Color m_header_default_cell_forecolor;

    [XmlElement(ElementName = "HeaderDefaultCellForeColor"), Category("Appearance")]
    public Color HeaderDefaultCellForeColor
    {
      get
      {
        return m_header_default_cell_forecolor;
      }
      set
      {
        m_header_default_cell_forecolor = value;

      }
    }

    private Color m_default_cell_selection_backcolor;

    [XmlElement(ElementName = "DefaultCellSelectionBackColor"), Category("Appearance")]
    public Color DefaultCellSelectionBackColor
    {
      get
      {
        return m_default_cell_selection_backcolor;
      }
      set
      {
        m_default_cell_selection_backcolor = value;
      }
    }

    private Color m_default_cell_selection_forecolor;

    [XmlElement(ElementName = "DefaultCellSelectionForeColor"), Category("Appearance")]
    public Color DefaultCellSelectionForeColor
    {
      get
      {
        return m_default_cell_selection_forecolor;
      }
      set
      {
        m_default_cell_selection_forecolor = value;
      }
    }

    private Color m_default_cell_backcolor;

    [XmlElement(ElementName = "DefaultCellBackColor"), Category("Appearance")]
    public Color DefaultCellBackColor
    {
      get
      {
        return m_default_cell_backcolor;
      }
      set
      {
        m_default_cell_backcolor = value;
      }
    }

    private Color m_default_cell_forecolor;

    [XmlElement(ElementName = "DefaultCellForeColor"), Category("Appearance")]
    public Color DefaultCellForeColor
    {
      get
      {
        return m_default_cell_forecolor;
      }
      set
      {
        m_default_cell_forecolor = value;
      }
    }

    private Int32 m_column_headers_height;

    [XmlElement(ElementName = "ColumnHeaderHeight"), Category("Appearance")]
    public Int32 ColumnHeaderHeight
    {
      get
      {
        return m_column_headers_height;
      }
      set
      {
        m_column_headers_height = value;
      }
    }

    private Int32 m_row_template_height;

    [XmlElement(ElementName = "RowTemplateHeight"), Category("Appearance")]
    public Int32 RowTemplateHeight
    {
      get
      {
        return m_row_template_height;
      }
      set
      {
        m_row_template_height = value;
      }
    }

    private Color m_border_color;

    [Category("Appearance")]
    public Color BorderColor
    {
      get
      {
        return m_border_color;
      }
      set
      {
        m_border_color = value;
      }
    }


    /// <summary>
    /// Contains the images to add in the column headers. 
    /// Int32: Index of column. 
    /// Object: Image to add in the column. 
    /// </summary>
    public Dictionary<Int32, Object> HeaderImages
    {
      get;
      set;
    }


    public uc_DataGridView()
    {
      this.m_corners_radius = 10;
      this.ColumnHeaderHeight = 35;
      this.RowTemplate.Height = 35;
      m_style = DataGridViewStyle.WITH_ALL_ROUND_CORNERS;
      SetDataGridViewStyle();

      m_parent_back_color = Color.Transparent;
      HeaderImages = null;
    }

    private void SetDataGridViewStyle()
    {
      CashierStyle.ControlStyle.InitStyle(this);
    }

    protected override void OnCreateControl()
    {
      base.OnCreateControl();

      SetStyleGrid();
    }

    protected override void OnCellPainting(DataGridViewCellPaintingEventArgs e)
    {
      e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;

      GraphicsPath _glyph;
      Int32 _margin_glyph_right;
      Size _size_glyph;

      _margin_glyph_right = 5;
      _size_glyph = new Size(10, 8);

      // Header borders
      if (e.RowIndex == -1 && e.ColumnIndex >= 0)
      {
        e.Paint(e.CellBounds, DataGridViewPaintParts.All);
        using (Pen customPen = new Pen(e.CellStyle.BackColor, 1))
        {
          Rectangle rect = e.CellBounds;
          e.Graphics.DrawRectangle(customPen, rect);
        }
        e.Handled = true;
      }

      // Grid borders
      if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
      {
        e.Paint(e.CellBounds, DataGridViewPaintParts.All);
        using (Pen customPen = new Pen(m_border_color, 1))
        {
          Rectangle rect = e.CellBounds;
          e.Graphics.DrawRectangle(customPen, rect);
        }
        e.Handled = true;
      }

      // Sorting Gliph
      if (e.RowIndex == -1 && e.ColumnIndex >= 0 && this.Columns[e.ColumnIndex].HeaderCell.SortGlyphDirection != SortOrder.None)
      {
        _glyph = new GraphicsPath();

        if (this.Columns[e.ColumnIndex].HeaderCell.SortGlyphDirection == SortOrder.Ascending)
        {
          PointF[] _points = { new PointF(e.CellBounds.X + e.CellBounds.Width - _margin_glyph_right, e.CellBounds.Height / 2), new PointF(e.CellBounds.X + e.CellBounds.Width - _size_glyph.Width - _margin_glyph_right, e.CellBounds.Height / 2), new PointF(e.CellBounds.X + e.CellBounds.Width - _size_glyph.Width / 2 - _margin_glyph_right, e.CellBounds.Height / 2 - _size_glyph.Height) };
          _glyph.AddPolygon(_points);
        }
        else if (this.Columns[e.ColumnIndex].HeaderCell.SortGlyphDirection == SortOrder.Descending)
        {
          PointF[] _points = { new PointF(e.CellBounds.X + e.CellBounds.Width - _size_glyph.Width - _margin_glyph_right, e.CellBounds.Height / 2 - 2), new PointF(e.CellBounds.X + e.CellBounds.Width - _margin_glyph_right, e.CellBounds.Height / 2 - 2), new PointF(e.CellBounds.X + e.CellBounds.Width - _size_glyph.Width / 2 - _margin_glyph_right, e.CellBounds.Height / 2 + _size_glyph.Height - 2) };
          _glyph.AddPolygon(_points);
        }

        SolidBrush brush = new SolidBrush(e.CellStyle.BackColor);
        e.Graphics.FillRectangle(brush, e.CellBounds);
        e.Paint(e.ClipBounds, (DataGridViewPaintParts.All & ~DataGridViewPaintParts.ContentBackground));
        e.Graphics.FillPath(new SolidBrush(HeaderDefaultCellForeColor), _glyph);
        e.Handled = true;
      }


      // Add images in column header
      if (HeaderImages != null)
      {
        if (e.RowIndex == -1)
        {
          if (HeaderImages.ContainsKey(e.ColumnIndex))
          {
            // should be prepared, maybe in an imagelist!!
            Image img = (Image)HeaderImages[e.ColumnIndex];
            Rectangle r32 = new Rectangle(e.CellBounds.Left + e.CellBounds.Width - this.ColumnHeaderHeight + 2, 4, this.ColumnHeaderHeight - 5 , this.ColumnHeaderHeight - 5);
            Rectangle r96 = new Rectangle(0, 0, this.ColumnHeaderHeight, this.ColumnHeaderHeight);
            //string header = this.Columns[e.ColumnIndex].HeaderText;
            e.PaintBackground(e.CellBounds, true);  // or maybe false ie no selection?
            e.PaintContent(e.CellBounds);
            e.Graphics.DrawImage(img, r32, r96, GraphicsUnit.Pixel);
            e.Handled = true;
          }
        }
      }

    }

    protected override void OnPaint(PaintEventArgs e)
    {
      GraphicsPath _gr_path;
      GraphicsPath _gr_path_border;
      Int32 _top;
      Int32 _left;
      Int32 _right;
      Int32 _bottom;

      base.OnPaint(e);

      e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;

      GetParentColor();

      //VerticalScrollBar.Margin = new Padding(this.m_corners_radius);
      //this.VerticalScrollBar.Size = new Size(this.VerticalScrollBar.Size.Width + 50, this.VerticalScrollBar.Size.Height - 50);
      //this.VerticalScrollBar.Location = new Point(this.VerticalScrollBar.Location.X - 50, this.VerticalScrollBar.Location.Y + 50);

      _gr_path = new GraphicsPath();
      _gr_path_border = new GraphicsPath();


      //this.GridColor = Color.White; 
      //this.CellBorderStyle = DataGridViewCellBorderStyle.Single;
      //this.RowHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;
      //this.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;


      _top = 0;
      _left = 0;
      _right = Width - 1;
      _bottom = Height - 1;

      switch (Style)
      {
        case DataGridViewStyle.WITH_ALL_ROUND_CORNERS:
          // TOP
          _gr_path.AddArc(_left, _top, m_corners_radius, m_corners_radius, 180, 90);
          _gr_path.AddArc(_right - m_corners_radius, _top, m_corners_radius, m_corners_radius, 270, 90);

          //BOTTOM
          _gr_path.AddArc(_right - m_corners_radius, _bottom - m_corners_radius, m_corners_radius, m_corners_radius, 0, 90);
          _gr_path.AddArc(_left, _bottom - m_corners_radius, m_corners_radius, m_corners_radius, 90, 90);

          // Set border
          _gr_path_border = _gr_path;

          break;
        case DataGridViewStyle.WITH_TOP_ROUND_BORDERS:
          // TOP
          _gr_path.AddArc(_left, _top, m_corners_radius, m_corners_radius, 180, 90);
          _gr_path.AddArc(_right - m_corners_radius, _top, m_corners_radius, m_corners_radius, 270, 90);

          //BOTTOM
          _gr_path.AddLine(_right, _bottom, _left, _bottom);

          // Set border
          _gr_path_border.AddLine(_left, _bottom, _left, _top - m_corners_radius);
          _gr_path_border.AddArc(_left, _top, m_corners_radius, m_corners_radius, 180, 90);
          _gr_path_border.AddArc(_right - m_corners_radius, _top, m_corners_radius, m_corners_radius, 270, 90);
          _gr_path_border.AddLine(_right, _top - m_corners_radius, _right, _bottom);


          break;
        case DataGridViewStyle.WITH_BOTTOM_ROUND_BORDERS:
          //TOP
          _gr_path.AddLine(_left, _top, _right, _top);

          //BOTTOM
          _gr_path.AddArc(_right - m_corners_radius, _bottom - m_corners_radius, m_corners_radius, m_corners_radius, 0, 90);
          _gr_path.AddArc(_left, _bottom - m_corners_radius, m_corners_radius, m_corners_radius, 90, 90);

          // Set border
          _gr_path_border.AddLine(_right, _top, _right, _bottom - m_corners_radius);
          _gr_path_border.AddArc(_right - m_corners_radius, _bottom - m_corners_radius, m_corners_radius, m_corners_radius, 0, 90);
          _gr_path_border.AddArc(_left, _bottom - m_corners_radius, m_corners_radius, m_corners_radius, 90, 90);
          _gr_path_border.AddLine(_left, _bottom - m_corners_radius, _left, _top);

          break;
        case DataGridViewStyle.WITHOUT_ROUND_BORDERS:
          //TOP
          _gr_path.AddLine(_left, _top, _right, _top);

          //BOTTOM
          _gr_path.AddLine(_right, _bottom, _left, _bottom);

          break;
        case DataGridViewStyle.PROMOTIONS:
          // TOP
          _gr_path.AddArc(_left, _top, m_corners_radius, m_corners_radius, 180, 90);
          _gr_path.AddArc(_right - m_corners_radius, _top, m_corners_radius, m_corners_radius, 270, 90);

          //BOTTOM
          _gr_path.AddLine(_right, _bottom, _left, _bottom);

          // Set border
          _gr_path_border.AddLine(_left, _bottom, _left, _top - m_corners_radius);
          _gr_path_border.AddArc(_left, _top, m_corners_radius, m_corners_radius, 180, 90);
          _gr_path_border.AddArc(_right - m_corners_radius, _top, m_corners_radius, m_corners_radius, 270, 90);
          _gr_path_border.AddLine(_right, _top - m_corners_radius, _right, _bottom);

          break;
        default:
          // TOP
          _gr_path.AddArc(_left, _top, m_corners_radius, m_corners_radius, 180, 90);
          _gr_path.AddArc(_right - m_corners_radius, _top, m_corners_radius, m_corners_radius, 270, 90);

          //BOTTOM
          _gr_path.AddArc(_right - m_corners_radius, _bottom - m_corners_radius, m_corners_radius, m_corners_radius, 0, 90);
          _gr_path.AddArc(_left, _bottom - m_corners_radius, m_corners_radius, m_corners_radius, 90, 90);

          // Set border
          _gr_path_border.AddLine(_left, _bottom, _left, _top - m_corners_radius);
          _gr_path_border.AddArc(_left, _top, m_corners_radius, m_corners_radius, 180, 90);
          _gr_path_border.AddArc(_right - m_corners_radius, _top, m_corners_radius, m_corners_radius, 270, 90);
          _gr_path_border.AddLine(_right, _top - m_corners_radius, _right, _bottom);

          break;
      }

      _gr_path.CloseFigure();

      //e.Graphics.DrawPath(new Pen(((DataGridView)(this)).ColumnHeadersDefaultCellStyle.BackColor, 1), _gr_path);

      e.Graphics.DrawPath(new Pen(m_parent_back_color, 1), _gr_path_border);
      this.Region = new Region(_gr_path);

      //e.Graphics.DrawPath(new Pen(Color.Red, 2), _gr_path);
    }

    protected override void OnEnabledChanged(EventArgs e)
    {
      SetStyleGrid();
    }

    private void SetStyleGrid()
    {
      SetDataGridViewStyle();

      this.ColumnHeadersBorderStyle = HeaderBorderStyle;

      this.ColumnHeadersHeight = ColumnHeaderHeight;
      this.RowTemplate.Height = RowTemplateHeight;

      this.DefaultCellStyle.SelectionBackColor = DefaultCellSelectionBackColor;
      this.DefaultCellStyle.SelectionForeColor = DefaultCellSelectionForeColor;
      this.DefaultCellStyle.BackColor = DefaultCellBackColor;
      this.DefaultCellStyle.ForeColor = DefaultCellForeColor;

      this.ColumnHeadersDefaultCellStyle.BackColor = HeaderDefaultCellBackColor;
      this.ColumnHeadersDefaultCellStyle.ForeColor = HeaderDefaultCellForeColor;

    }

    private void GetParentColor()
    {
      if (m_parent_back_color == Color.Transparent)
      {
        if (this.Parent != null)
        {
          m_parent_back_color = GetParentColor(this.Parent);
        }
        else
        {
          m_parent_back_color = Color.Transparent;
        }
      }
    }

    private Color GetParentColor(Control Ctrl)
    {
      Color _color;

      if (Ctrl.BackColor == Color.Transparent && Ctrl.Parent != null)
      {
        _color = GetParentColor(Ctrl.Parent);
      }
      else
      {
        _color = Ctrl.BackColor;
      }

      return _color;
    }

  }

}
