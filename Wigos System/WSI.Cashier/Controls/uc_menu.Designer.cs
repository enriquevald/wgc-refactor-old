﻿namespace WSI.Cashier.Controls
{
  partial class uc_menu
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.tlp_menu = new System.Windows.Forms.TableLayoutPanel();
      this.pb_image = new System.Windows.Forms.PictureBox();
      ((System.ComponentModel.ISupportInitialize)(this.pb_image)).BeginInit();
      this.SuspendLayout();
      // 
      // tlp_menu
      // 
      this.tlp_menu.AutoSize = true;
      this.tlp_menu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.tlp_menu.ColumnCount = 1;
      this.tlp_menu.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
      this.tlp_menu.Location = new System.Drawing.Point(0, 0);
      this.tlp_menu.Margin = new System.Windows.Forms.Padding(0);
      this.tlp_menu.Name = "tlp_menu";
      this.tlp_menu.RowCount = 1;
      this.tlp_menu.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_menu.Size = new System.Drawing.Size(130, 643);
      this.tlp_menu.TabIndex = 0;
      // 
      // pb_image
      // 
      this.pb_image.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.pb_image.Location = new System.Drawing.Point(0, 637);
      this.pb_image.Name = "pb_image";
      this.pb_image.Size = new System.Drawing.Size(130, 71);
      this.pb_image.TabIndex = 1;
      this.pb_image.TabStop = false;
      // 
      // uc_menu
      // 
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
      this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.Controls.Add(this.tlp_menu);
      this.Controls.Add(this.pb_image);
      this.Margin = new System.Windows.Forms.Padding(0);
      this.Name = "uc_menu";
      this.Size = new System.Drawing.Size(130, 713);
      ((System.ComponentModel.ISupportInitialize)(this.pb_image)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.TableLayoutPanel tlp_menu;
    private System.Windows.Forms.PictureBox pb_image;
  }
}
