//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : uc_round_auto_combobox.cs
// 
//   DESCRIPTION : Contains an autocomplete combobox 
//                 able to work with on screen keyboard
//
// REVISION HISTORY:
// 
// Special considerations:
//  - In order to be able to work in on screen keyboard
//    autocomplete code is in OnKeyUp() event.
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 26-JUN-2013 ANG    First release
// 14-NOV-2013 ANG    Refactor to be able to work in WigosGUI
// 26-NOV-2013 ANG    Fix some problems with AllowUnlistedValues property.
//                    Not affected by current instances ( currently feature not in use )
// 02-JAN-2014 RMS    Corrected validation with empty item list.
//                    Controlled correct selection index change
// -----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Data;
using System.Drawing;
using System.ComponentModel;
using WSI.Common;

namespace WSI.Cashier.Controls
{
  public class uc_round_auto_combobox : uc_round_combobox
  {
    #region "Properties"

    private Stack<char> m_typped_chars;   // Stack with user typped keys
    private String m_typped_text;
    private Boolean m_allow_unlisted_values;
    private Int32 m_prev_match_item;
    private ICustomFormatValidable m_format_validator;
    
    [CategoryAttribute("Data")]
    public ICustomFormatValidable FormatValidator
    {
      get { return this.m_format_validator; }
      set { this.m_format_validator = value; }
    }

    [CategoryAttribute("Data")]
    [DefaultValue(false)]
    public Boolean AllowUnlistedValues
    {
      get { return this.m_allow_unlisted_values; }
      set { this.m_allow_unlisted_values = value; }
    }

    #endregion

    #region "Constructor"

    // PURPOSE: Class default constructor
    //          Call auto combobox without custom format validator.m  
    //  PARAMS:
    //     - INPUT:
    //     - OUTPUT:
    // RETURNS:
    public uc_round_auto_combobox()
      : this(null)
    {
      // Call contructor without format validator
      this.DropDownStyle = ComboBoxStyle.DropDown;
      this.AutoCompleteMode = AutoCompleteMode.None;
      this.m_allow_unlisted_values = false;
      this.m_prev_match_item = Int32.MinValue;
      this.m_typped_chars = new Stack<char>();

      ClearTyppedText();
    } // uc_round_auto_combobox

    // PURPOSE: Class default constructor
    //          Call auto combobox without custom format validator.m  
    //  PARAMS:
    //     - INPUT: CustomFormatValidator ,
    //              Object that implements ICustomFormatValidable interface 
    //             and knows how to validate format, like CharacterTextBox or NumericTextBox
    //     - OUTPUT:
    // RETURNS:
    public uc_round_auto_combobox(ICustomFormatValidable CustomFormatValidator)
      : base()
    {
      this.m_format_validator = CustomFormatValidator;
    }

    #endregion

    #region "Events"

    // PURPOSE: On key down event handler
    //
    //  PARAMS:
    //     - INPUT:
    //           - KeyEventArgs e
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - void
    // NOTE : Util to handle not printable keys ;)
    protected override void OnKeyDown(KeyEventArgs e)
    {
      base.OnKeyDown(e);

      switch (e.KeyData)
      {
        case Keys.Up:
        case Keys.Down:
        case Keys.PageUp:
        case Keys.PageDown:
        case Keys.Home:
          {
            if (m_allow_unlisted_values)
            {

              break;
            }
            // We are navigating trough menu, 
            // ignore typped keys ;)
            ClearTyppedText();
            break;
          }
        case Keys.Right:
        case Keys.End:
          {
            if (m_allow_unlisted_values)
            {

              break;
            }
            if (this.SelectionLength != this.Text.Length)
            {
              // Only handle right key when all text are selected ;)
              break;
            }
            // Trick : Simulate user typped all combo content ;)
            this.m_typped_chars.Clear();
            foreach (char c in this.Text.ToCharArray())
            {
              this.m_typped_chars.Push(c);
            }
            break;
          }
        case Keys.Escape:
          {
            this.IsDroppedDown = false;
            break;
          }

        case Keys.Back:
          this.m_typped_chars.Clear();
          foreach (char c in this.Text.ToCharArray())
          {
            this.m_typped_chars.Push(c);
          }

          m_typped_text = GetReversed(m_typped_chars);
          m_prev_match_item = this.FindString(m_typped_text);

          break;
      }
    }

    // PURPOSE: On key press event handler
    //
    //  PARAMS:
    //     - INPUT:
    //           - KeyEventArgs e
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - void
    // NOTE :  Only printable chars keys will be handled by this event.
    protected override void OnKeyPress(KeyPressEventArgs e)
    {
      base.OnKeyPress(e);

      if (this.DropDownStyle != ComboBoxStyle.DropDown)
      {
        return;
      }

      if (m_format_validator != null)
      {
        m_format_validator.Text = String.Empty + (char)e.KeyChar;
        if (!m_format_validator.ValidateFormat())
        {
          e.Handled = true; // Cancell user input!
          return;
        }
      }

      // Check for a valid key that match with content list.
      if (!m_allow_unlisted_values)
      {
        // buffer user typped keys
        if (e.KeyChar == (char)Keys.Back)
        {
          if (this.SelectionLength == 0 && m_typped_chars.Count > 0)
          {
            m_typped_chars.Pop();
          }
        }
        else
        {
          m_typped_chars.Push(e.KeyChar);
        }
        // buffer user typped keys
        m_typped_text = GetReversed(m_typped_chars);
        m_prev_match_item = this.FindString(m_typped_text);

        // If not mach with any element...
        if (m_prev_match_item < 0)
        {
          // Stop the character from being entered into the control
          e.Handled = true;

          // On user type not accepted char, remove then and back to previous right selection.
          if (m_typped_chars.Count > 0)
          {
            m_typped_chars.Pop();
          }
          if (m_typped_chars.Count > 0)
          {
            OnKeyPress(new KeyPressEventArgs(m_typped_chars.Pop()));
          }
        }
      }

    }  // OnKeyPress

    // PURPOSE: On Key up event handler
    //          Autocomplete user input and higlight match text.
    //          Order : keyDown - Key Press - Key Up
    //  PARAMS:
    //     - INPUT:
    //           - KeyEventArgs e
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - void
    protected override void OnKeyUp(KeyEventArgs e)
    {
      base.OnKeyUp(e);

      if (this.DropDownStyle != ComboBoxStyle.DropDown)
      {

        return;
      }

      if (m_allow_unlisted_values)
      {

        return;
      }

      switch (e.KeyCode)
      {
        case Keys.Delete:
        case Keys.Back:
        case Keys.Tab:
        case Keys.Escape:
        case Keys.Up:
        case Keys.Down:
        case Keys.Enter:            // Added to check in WigosGUI validate selection with INTRO ;)

          if (e.KeyCode == Keys.Enter)
          {
            this.IsDroppedDown = false;
            this.SelectionLength = 0;
            this.SelectionStart = this.Text.Length;
          }

          e.Handled = true;
          break;
        default:
          {
            if (m_prev_match_item >= 0)
            {
              this.IsDroppedDown = true;
              this.SelectedIndex = m_prev_match_item;
              this.SelectionStart = m_typped_text.Length;
              this.SelectionLength = Math.Max(this.Text.Length - m_typped_text.Length, 0);
            } // Autocomplete

            m_prev_match_item = Int32.MinValue;
            e.Handled = true;
            break;
          }
      }

      // Autocomplete

    } // OnKeyUp

    // PURPOSE: OnDropDownClosed
    // Util to avoid problems with on-screen keyboard
    // Retains auto-complete selection on drop down closed
    //  PARAMS:
    //     - INPUT:
    //           - EventArgs e
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - void
    protected override void OnDropDownClosed(EventArgs e)
    {
      base.OnDropDownClosed(e);

      if (this.DropDownStyle != ComboBoxStyle.DropDown)
      {

        return;
      }

      if (m_allow_unlisted_values)
      {

        return;
      }

      if (!String.IsNullOrEmpty(m_typped_text))
      {
        this.SelectionStart = this.m_typped_text.Length;
        this.SelectionLength = Math.Max(this.Text.Length - m_typped_text.Length, 0);
      }

    } // OnDropDownClosed

    // PURPOSE: OnClick
    //          Drop down combo on click at content.
    //  PARAMS:
    //     - INPUT:
    //           - EventArgs e
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - void 
    protected override void OnClick(EventArgs e)
    {
      MouseEventArgs _mouse_event;
      int _dropDownButtonWidth;

      base.OnClick(e);

      if (this.DropDownStyle != ComboBoxStyle.DropDown)
      {
        return;
      }
      _mouse_event = (MouseEventArgs)e;
      _dropDownButtonWidth = System.Windows.Forms.SystemInformation.HorizontalScrollBarArrowWidth;

    } // OnClick

    // PURPOSE: Control when item selected on dropdown, and reset typped text
    //  PARAMS:
    //     - INPUT:
    //           - CancelEventArgs e
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - void 
    public override void OnSelectionChangeCommitted(object sender, EventArgs e)
    {
      base.OnSelectionChangeCommitted(sender, e);
      if (this.IsDroppedDown)
      {
        if (this.SelectedIndex != -1)
        {
          m_typped_text = "";
        }
      }
    }

    // PURPOSE: Validating combobox content
    //          If AllowUnlistedValues property is set
    //          ensure that selected value match to some element in datasource.
    //  PARAMS:
    //     - INPUT:
    //           - CancelEventArgs e
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - void 
    protected override void OnValidating(CancelEventArgs e)
    {
      Int32 _candidate_idx;
      String _text_to_validate;

      base.OnValidating(e);

      if (this.DropDownStyle == ComboBoxStyle.DropDown)
      {
        if (!m_allow_unlisted_values)
        {
          if (this.IsDroppedDown)
          {
            this.IsDroppedDown = false;
          }

          _text_to_validate = this.Text;
          if (!String.IsNullOrEmpty(m_typped_text))
          {
            _text_to_validate = m_typped_text;
          }

          _candidate_idx = this.FindString(_text_to_validate);

          if (this.SelectedIndex == _candidate_idx)
          {
            // Force to refresh items ( for ex : if user proviously removed content )
            this.RefreshItems();
          }

          if (_candidate_idx >= 0)
          {
            this.SelectedIndex = _candidate_idx;
          }
          else
          {
            if (this.Items.Count > 0)
            {
              // To force to reload combo text.
              this.RefreshItems();
            }
          }

          ClearTyppedText();
        }
      }

    } //OnValidating

    // PURPOSE: On key press event handler
    //
    //  PARAMS:
    //     - INPUT:
    //           - KeyEventArgs e
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - void
    // NOTE : 
    // WinForms: SelectedIndexChanged event does not fire when the user tabs off of an open DropDownList ComboBox
    // http://support.microsoft.com/kb/948869
    protected override bool ProcessDialogKey(Keys keyData)
    {

      switch (keyData)
      {
        case Keys.Tab:
        case Keys.Return:
          this.IsDroppedDown = false;
          break;
      }

      return base.ProcessDialogKey(keyData);
    } // ProcessDialogKey

    // PURPOSE: On selected index changed
    //
    //  PARAMS:
    //     - INPUT:
    //           - KeyEventArgs e
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - void
    // NOTE : Keep last valid selected index to be able to restore in invalid selection.


    #endregion

    #region "Overwritted methods"

    // PURPOSE: Find String.
    //          First look for exact match otherwise look for a similar text..
    //  PARAMS:
    //     - INPUT:
    //           - KeyPressEventArgs e
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - void 
    protected int FindString(String InputText)
    {
      int _candidate;

      _candidate = this.FindStringExact(InputText);

      if (_candidate < 0)
      {
        _candidate = this.FindStringStartsWith(InputText);
      }

      return _candidate;
    } // FindString

    protected override void OnCreateControl()
    {
      base.OnCreateControl();

      this.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
    }

    #endregion

    #region "Private methods"

    // PURPOSE: Get a String representation of stack chars
    //  PARAMS:
    //     - INPUT:
    //           - KeyPressEventArgs e
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - void 
    // NOTE : Create new stack from existing stack revese his content ;)
    private static String GetReversed(Stack<char> Stack)
    {
      Stack<char> _internal_stack = new Stack<char>(Stack);
      return new String(_internal_stack.ToArray());
    } // GetReversed


    // PURPOSE: Clear typped text buffers
    //  PARAMS:
    //     - INPUT:
    //           - KeyPressEventArgs e
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - void 
    private void ClearTyppedText()
    {
      this.m_typped_chars.Clear();
      this.m_typped_text = String.Empty;
    }

    #endregion


  }

}