﻿namespace WSI.Cashier.Controls
{
  partial class uc_top_bar
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.pb_image = new System.Windows.Forms.PictureBox();
      this.lbl_version = new WSI.Cashier.Controls.uc_label();
      this.panel_time = new WSI.Cashier.Controls.uc_round_panel();
      this.pb_clock = new System.Windows.Forms.PictureBox();
      this.lbl_date = new WSI.Cashier.Controls.uc_label();
      this.lbl_hour = new WSI.Cashier.Controls.uc_label();
      this.panel_status = new WSI.Cashier.Controls.uc_round_panel();
      this.lbl_db = new WSI.Cashier.Controls.uc_label();
      this.lbl_terminal = new WSI.Cashier.Controls.uc_label();
      this.lbl_multisite = new WSI.Cashier.Controls.uc_label();
      this.pb_terminal = new System.Windows.Forms.PictureBox();
      this.panel_user = new WSI.Cashier.Controls.uc_round_panel();
      this.pb_user_image = new System.Windows.Forms.PictureBox();
      this.lbl_cashier_connected = new WSI.Cashier.Controls.uc_label();
      this.lbl_user_name = new WSI.Cashier.Controls.uc_label();
      this.lbl_user_rol = new WSI.Cashier.Controls.uc_label();
      this.btn_keyboard = new WSI.Cashier.Controls.uc_round_button();
      ((System.ComponentModel.ISupportInitialize)(this.pb_image)).BeginInit();
      this.panel_time.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pb_clock)).BeginInit();
      this.panel_status.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pb_terminal)).BeginInit();
      this.panel_user.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pb_user_image)).BeginInit();
      this.SuspendLayout();
      // 
      // pb_image
      // 
      this.pb_image.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(113)))), ((int)(((byte)(137)))));
      this.pb_image.Dock = System.Windows.Forms.DockStyle.Left;
      this.pb_image.Location = new System.Drawing.Point(0, 0);
      this.pb_image.Name = "pb_image";
      this.pb_image.Size = new System.Drawing.Size(130, 55);
      this.pb_image.TabIndex = 0;
      this.pb_image.TabStop = false;
      // 
      // lbl_version
      // 
      this.lbl_version.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_version.AutoSize = true;
      this.lbl_version.BackColor = System.Drawing.Color.Transparent;
      this.lbl_version.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_version.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.lbl_version.Location = new System.Drawing.Point(580, 31);
      this.lbl_version.Name = "lbl_version";
      this.lbl_version.Size = new System.Drawing.Size(69, 19);
      this.lbl_version.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TERMINAL;
      this.lbl_version.TabIndex = 12;
      this.lbl_version.Text = "v. 00.000";
      this.lbl_version.TextAlign = System.Drawing.ContentAlignment.TopRight;
      // 
      // panel_time
      // 
      this.panel_time.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.panel_time.BackColor = System.Drawing.Color.Transparent;
      this.panel_time.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.panel_time.Controls.Add(this.pb_clock);
      this.panel_time.Controls.Add(this.lbl_date);
      this.panel_time.Controls.Add(this.lbl_hour);
      this.panel_time.CornerRadius = 1;
      this.panel_time.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.panel_time.HeaderBackColor = System.Drawing.Color.Empty;
      this.panel_time.HeaderForeColor = System.Drawing.Color.Empty;
      this.panel_time.HeaderHeight = 0;
      this.panel_time.HeaderSubText = null;
      this.panel_time.HeaderText = null;
      this.panel_time.Location = new System.Drawing.Point(928, -2);
      this.panel_time.Margin = new System.Windows.Forms.Padding(0);
      this.panel_time.Name = "panel_time";
      this.panel_time.PanelBackColor = System.Drawing.Color.Transparent;
      this.panel_time.Size = new System.Drawing.Size(99, 58);
      this.panel_time.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.ONLY_BORDER;
      this.panel_time.TabIndex = 11;
      // 
      // pb_clock
      // 
      this.pb_clock.Location = new System.Drawing.Point(6, 5);
      this.pb_clock.Name = "pb_clock";
      this.pb_clock.Size = new System.Drawing.Size(27, 25);
      this.pb_clock.TabIndex = 7;
      this.pb_clock.TabStop = false;
      // 
      // lbl_date
      // 
      this.lbl_date.BackColor = System.Drawing.Color.Transparent;
      this.lbl_date.Font = new System.Drawing.Font("Open Sans", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_date.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.lbl_date.Location = new System.Drawing.Point(7, 34);
      this.lbl_date.Name = "lbl_date";
      this.lbl_date.Size = new System.Drawing.Size(98, 17);
      this.lbl_date.Style = WSI.Cashier.Controls.uc_label.LabelStyle.DATE;
      this.lbl_date.TabIndex = 6;
      this.lbl_date.Text = "DDD 00/MMM";
      // 
      // lbl_hour
      // 
      this.lbl_hour.BackColor = System.Drawing.Color.Transparent;
      this.lbl_hour.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_hour.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.lbl_hour.Location = new System.Drawing.Point(41, 10);
      this.lbl_hour.Name = "lbl_hour";
      this.lbl_hour.Size = new System.Drawing.Size(50, 17);
      this.lbl_hour.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TIME;
      this.lbl_hour.TabIndex = 0;
      this.lbl_hour.Text = "00:00";
      // 
      // panel_status
      // 
      this.panel_status.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.panel_status.BackColor = System.Drawing.Color.Transparent;
      this.panel_status.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.panel_status.Controls.Add(this.lbl_db);
      this.panel_status.Controls.Add(this.lbl_terminal);
      this.panel_status.Controls.Add(this.lbl_multisite);
      this.panel_status.Controls.Add(this.pb_terminal);
      this.panel_status.CornerRadius = 1;
      this.panel_status.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.panel_status.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.panel_status.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.panel_status.HeaderHeight = 0;
      this.panel_status.HeaderSubText = null;
      this.panel_status.HeaderText = null;
      this.panel_status.Location = new System.Drawing.Point(652, -2);
      this.panel_status.Margin = new System.Windows.Forms.Padding(0);
      this.panel_status.Name = "panel_status";
      this.panel_status.PanelBackColor = System.Drawing.Color.Transparent;
      this.panel_status.Size = new System.Drawing.Size(205, 58);
      this.panel_status.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.ONLY_BORDER;
      this.panel_status.TabIndex = 10;
      // 
      // lbl_db
      // 
      this.lbl_db.BackColor = System.Drawing.Color.Transparent;
      this.lbl_db.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_db.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(9)))), ((int)(((byte)(220)))), ((int)(((byte)(9)))));
      this.lbl_db.Location = new System.Drawing.Point(152, 35);
      this.lbl_db.Name = "lbl_db";
      this.lbl_db.Size = new System.Drawing.Size(47, 17);
      this.lbl_db.Style = WSI.Cashier.Controls.uc_label.LabelStyle.STATUS_CONNECTED;
      this.lbl_db.TabIndex = 7;
      this.lbl_db.Text = "DB";
      this.lbl_db.TextAlign = System.Drawing.ContentAlignment.TopRight;
      // 
      // lbl_terminal
      // 
      this.lbl_terminal.BackColor = System.Drawing.Color.Transparent;
      this.lbl_terminal.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_terminal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.lbl_terminal.Location = new System.Drawing.Point(38, 8);
      this.lbl_terminal.Name = "lbl_terminal";
      this.lbl_terminal.Size = new System.Drawing.Size(161, 17);
      this.lbl_terminal.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TERMINAL;
      this.lbl_terminal.TabIndex = 0;
      this.lbl_terminal.Text = "TERMINAL NAME";
      this.lbl_terminal.TextAlign = System.Drawing.ContentAlignment.TopRight;
      // 
      // lbl_multisite
      // 
      this.lbl_multisite.BackColor = System.Drawing.Color.Transparent;
      this.lbl_multisite.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_multisite.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.lbl_multisite.Location = new System.Drawing.Point(2, 35);
      this.lbl_multisite.Name = "lbl_multisite";
      this.lbl_multisite.Size = new System.Drawing.Size(100, 17);
      this.lbl_multisite.Style = WSI.Cashier.Controls.uc_label.LabelStyle.STATUS_DISCONNECTED;
      this.lbl_multisite.TabIndex = 6;
      this.lbl_multisite.Text = "MULTISITE";
      // 
      // pb_terminal
      // 
      this.pb_terminal.Location = new System.Drawing.Point(7, 8);
      this.pb_terminal.Name = "pb_terminal";
      this.pb_terminal.Size = new System.Drawing.Size(27, 25);
      this.pb_terminal.TabIndex = 5;
      this.pb_terminal.TabStop = false;
      // 
      // panel_user
      // 
      this.panel_user.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.panel_user.BorderColor = System.Drawing.Color.Empty;
      this.panel_user.Controls.Add(this.pb_user_image);
      this.panel_user.Controls.Add(this.lbl_cashier_connected);
      this.panel_user.Controls.Add(this.lbl_user_name);
      this.panel_user.Controls.Add(this.lbl_user_rol);
      this.panel_user.CornerRadius = 1;
      this.panel_user.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.panel_user.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.panel_user.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.panel_user.HeaderHeight = 0;
      this.panel_user.HeaderSubText = null;
      this.panel_user.HeaderText = null;
      this.panel_user.Location = new System.Drawing.Point(129, -3);
      this.panel_user.Name = "panel_user";
      this.panel_user.PanelBackColor = System.Drawing.Color.Transparent;
      this.panel_user.Size = new System.Drawing.Size(289, 59);
      this.panel_user.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NONE;
      this.panel_user.TabIndex = 9;
      // 
      // pb_user_image
      // 
      this.pb_user_image.Location = new System.Drawing.Point(3, 5);
      this.pb_user_image.Name = "pb_user_image";
      this.pb_user_image.Size = new System.Drawing.Size(55, 51);
      this.pb_user_image.TabIndex = 1;
      this.pb_user_image.TabStop = false;
      // 
      // lbl_cashier_connected
      // 
      this.lbl_cashier_connected.BackColor = System.Drawing.Color.Transparent;
      this.lbl_cashier_connected.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_cashier_connected.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(9)))), ((int)(((byte)(220)))), ((int)(((byte)(9)))));
      this.lbl_cashier_connected.Location = new System.Drawing.Point(59, 32);
      this.lbl_cashier_connected.Name = "lbl_cashier_connected";
      this.lbl_cashier_connected.Size = new System.Drawing.Size(227, 17);
      this.lbl_cashier_connected.Style = WSI.Cashier.Controls.uc_label.LabelStyle.STATUS_CONNECTED;
      this.lbl_cashier_connected.TabIndex = 7;
      this.lbl_cashier_connected.Text = "Cash OPEN 00:00";
      // 
      // lbl_user_name
      // 
      this.lbl_user_name.BackColor = System.Drawing.Color.Transparent;
      this.lbl_user_name.Font = new System.Drawing.Font("Open Sans", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_user_name.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.lbl_user_name.Location = new System.Drawing.Point(58, 10);
      this.lbl_user_name.Name = "lbl_user_name";
      this.lbl_user_name.Size = new System.Drawing.Size(228, 20);
      this.lbl_user_name.Style = WSI.Cashier.Controls.uc_label.LabelStyle.USER_NAME;
      this.lbl_user_name.TabIndex = 2;
      this.lbl_user_name.Text = "USER NAME";
      // 
      // lbl_user_rol
      // 
      this.lbl_user_rol.AutoSize = true;
      this.lbl_user_rol.BackColor = System.Drawing.Color.Transparent;
      this.lbl_user_rol.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_user_rol.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(130)))), ((int)(((byte)(210)))), ((int)(((byte)(227)))));
      this.lbl_user_rol.Location = new System.Drawing.Point(62, 33);
      this.lbl_user_rol.Name = "lbl_user_rol";
      this.lbl_user_rol.Size = new System.Drawing.Size(76, 19);
      this.lbl_user_rol.Style = WSI.Cashier.Controls.uc_label.LabelStyle.ROL;
      this.lbl_user_rol.TabIndex = 3;
      this.lbl_user_rol.Text = "USER ROL";
      // 
      // btn_keyboard
      // 
      this.btn_keyboard.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_keyboard.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(113)))), ((int)(((byte)(137)))));
      this.btn_keyboard.CornerRadius = 0;
      this.btn_keyboard.FlatAppearance.BorderColor = System.Drawing.SystemColors.Control;
      this.btn_keyboard.FlatAppearance.BorderSize = 0;
      this.btn_keyboard.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_keyboard.Font = new System.Drawing.Font("Open Sans", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.btn_keyboard.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.btn_keyboard.Image = null;
      this.btn_keyboard.IsSelected = false;
      this.btn_keyboard.Location = new System.Drawing.Point(855, -11);
      this.btn_keyboard.Margin = new System.Windows.Forms.Padding(0, 0, 0, 30);
      this.btn_keyboard.Name = "btn_keyboard";
      this.btn_keyboard.SelectedColor = System.Drawing.Color.Blue;
      this.btn_keyboard.Size = new System.Drawing.Size(75, 66);
      this.btn_keyboard.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.TOP_BAR;
      this.btn_keyboard.TabIndex = 5;
      this.btn_keyboard.Text = "KEYBOARD";
      this.btn_keyboard.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
      this.btn_keyboard.UseVisualStyleBackColor = false;
      this.btn_keyboard.Click += new System.EventHandler(this.btn_keyboard_Click);
      // 
      // uc_top_bar
      // 
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
      this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.Controls.Add(this.lbl_version);
      this.Controls.Add(this.panel_time);
      this.Controls.Add(this.panel_status);
      this.Controls.Add(this.panel_user);
      this.Controls.Add(this.pb_image);
      this.Controls.Add(this.btn_keyboard);
      this.Margin = new System.Windows.Forms.Padding(0);
      this.Name = "uc_top_bar";
      this.Size = new System.Drawing.Size(1024, 55);
      ((System.ComponentModel.ISupportInitialize)(this.pb_image)).EndInit();
      this.panel_time.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.pb_clock)).EndInit();
      this.panel_status.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.pb_terminal)).EndInit();
      this.panel_user.ResumeLayout(false);
      this.panel_user.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pb_user_image)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.PictureBox pb_image;
    private System.Windows.Forms.PictureBox pb_user_image;
    private uc_label lbl_user_name;
    private uc_label lbl_user_rol;
    private uc_label lbl_multisite;
    private System.Windows.Forms.PictureBox pb_terminal;
    private uc_label lbl_terminal;
    private uc_round_button btn_keyboard;
    private System.Windows.Forms.PictureBox pb_clock;
    private uc_label lbl_date;
    private uc_label lbl_hour;
    private uc_label lbl_cashier_connected;
    private uc_label lbl_db;
    private uc_round_panel panel_user;
    private uc_round_panel panel_status;
    private uc_round_panel panel_time;
    private uc_label lbl_version;
  }
}
