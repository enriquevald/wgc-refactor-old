﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace WSI.Cashier.Controls
{
  public class frm_base_icon : Form
  {
    public frm_base_icon()
    {
      this.Icon = Properties.Resources.CashDesk;
    }
  }
}
