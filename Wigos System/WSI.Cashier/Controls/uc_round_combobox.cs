﻿//-------------------------------------------------------------------
// Copyright © 2013 Win Systems Ltd.
//-------------------------------------------------------------------
//
// MODULE NAME:   uc_round_combobox.cs
// DESCRIPTION:   
// AUTHOR:        ??????
// CREATION DATE: ???????
//
// REVISION HISTORY:
//
// Date         Author Description
// -----------  ------ -----------------------------------------------

// 16-MAR-2016  SJA    Fixed Bug WIG-1697: Player edit revealed that uc roundcombo 
//                        does not accept any other type of datasource apart from 
//                        datatable for autocomplete, enabled the use of list<T> and binding datasources
//                        
// 26-JUL-2017  RGR    Fixed Bug 28998:WIGOS-3606 The cashier ask to the user to save modifications when the user didn't any modification 
//--------------------------------------------------------------------

using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.ComponentModel;
using System;
using System.Data;
using System.Collections;
using WSI.Common;
using System.Collections.Generic;
using WSI.Common.Entities.General;

namespace WSI.Cashier.Controls
{
  public class UTextBox:TextBox
  {
    protected override bool ProcessCmdKey(ref Message Msg, Keys KeyData)
    {
      bool _base_result = base.ProcessCmdKey(ref Msg, KeyData);

      if (KeyData == Keys.Tab && Focused)
      {
        OnKeyDown(new KeyEventArgs(Keys.Tab));
        return true;
      }

      if (KeyData != (Keys.Tab | Keys.Shift) || !Focused)
      {
        return _base_result;
      }
      OnKeyDown(new KeyEventArgs(Keys.Shift | Keys.Tab));
      return true;
    }
  }

  public class uc_round_combobox : Control
  {
    #region Enums

    public enum RoundComboBoxStyle
    {
      NONE = 0,
      BASIC = 1,
      CUSTOM = 2,
    }

    #endregion

    public delegate void BNDroppedDownEventHandler(object sender, EventArgs e);

    #region Variables

    private bool resize = false;

    private Color _backColor = Color.White;
    private Color _border_color = Color.White;
    private BNRadius m_radius = new BNRadius();

    private int _dropDownHeight = 200;
    private int _dropDownWidth = 0;
    private int _maxDropDownItems = 8;
    //SJA 27 JAN 2016 - Added margin so text in controls lines up well 
    //                  in uc_address when switching between auto and manual mode
    Int32 _margin = 6;
    private int _selectedIndex = -1;

    private bool _isDroppedDown = false;

    private ComboBoxStyle _dropDownStyle = ComboBoxStyle.DropDownList;

    private Rectangle rectBtn = new Rectangle(0, 0, 1, 1);
    private Rectangle rectContent = new Rectangle(0, 0, 1, 1);

    private ToolStripControlHost _controlHost;
    private uc_round_list_box _listBox;
    private uc_ToolStripDropDown _popupControl;
    private UTextBox _textBox;

    private RoundComboBoxStyle m_style;
    private AutoCompleteMode m_auto_complete_mode;
    private AutoCompleteStringCollection m_auto_complete_string;

    private Color m_arrow_color;
    private bool m_tab_stop;

    #endregion

    #region Delegates

    [Category("Behavior"), Description("Occurs when IsDroppedDown changed to True.")]
    public event BNDroppedDownEventHandler DroppedDown;

    [Category("Behavior"), Description("Occurs when the SelectedIndex property changes.")]
    public event EventHandler SelectedIndexChanged;

    [Category("Behavior")]
    public event EventHandler DropDownClosed;

    [Category("Behavior")]
    public event EventHandler SelectionChangeCommitted;

    #endregion

    #region Properties

    [Category("Appearance")]
    public RoundComboBoxStyle Style
    {
      get
      {
        return m_style;
      }
      set
      {
        m_style = value;

        if (m_style == RoundComboBoxStyle.CUSTOM)
        {
          _listBox.Style = uc_round_list_box.RoundListBoxStyle.COMBOBOX_LIST_CUSTOM;
        }
        else
        {
          _listBox.Style = uc_round_list_box.RoundListBoxStyle.COMBOBOX_LIST;
        }

        if (m_style == RoundComboBoxStyle.BASIC)
        {
          _listBox.ItemHeight = this.Height;
        }

        _dropDownHeight = _maxDropDownItems * _listBox.ItemHeight;

        SetComboBoxStyle();

        AdjustControls();

        if (DesignMode && this.Parent != null)
        {
          this.Parent.Refresh(); // Refreshes the client area of the parent control
        }
      }
    }

    public bool OnFocusOpenListBox
    {
      get { return m_on_focus_open_list_box; }
      set { m_on_focus_open_list_box = value; }
    }

    public Color BorderColor
    {
      get { return _border_color; }
      set { _border_color = value; Invalidate(true); }
    }

    public Int32 ItemHeight
    {
      get { return _listBox.ItemHeight; }
      set
      {
        if (m_style != RoundComboBoxStyle.BASIC)
        {
          _listBox.ItemHeight = value;
        }
      }
    }

    public ListBox.ObjectCollection Items
    {
      get { return _listBox.Items; }
    }

    public int DropDownWidth
    {
      get { return _dropDownWidth; }
      set { _dropDownWidth = value; }
    }

    public int MaxDropDownItems
    {
      get { return _maxDropDownItems; }
      set
      {
        _maxDropDownItems = value;
        _dropDownHeight = _maxDropDownItems * _listBox.ItemHeight;
      }
    }

    public object DataSource
    {
      get { return _listBox.DataSource; }
      set
      {
        _listBox.DataSource = value;
        m_table = null;
      }
    }

    public bool Sorted
    {
      get
      {
        return _listBox.Sorted;
      }
      set
      {
        _listBox.Sorted = value;
      }
    }

    [Category("Behavior")]
    public DrawMode DrawMode
    {
      get { return _listBox.DrawMode; }
      set
      {
        _listBox.DrawMode = value;
      }
    }

    public ComboBoxStyle DropDownStyle
    {
      get { return _dropDownStyle; }
      set
      {
        _dropDownStyle = value;

        if (_dropDownStyle == ComboBoxStyle.DropDownList)
        {
          _textBox.Visible = false;
        }
        else
        {
          _textBox.Visible = true;
        }
        Invalidate(true);
      }
    }

    public new Color BackColor
    {
      get { return _backColor; }
      set
      {
        this._backColor = value;
        _textBox.BackColor = value;
        Invalidate(true);
      }
    }

    public Color ArrowColor
    {
      get { return m_arrow_color; }
      set
      {
        this.m_arrow_color = value;
        Invalidate(true);
      }
    }

    public bool IsDroppedDown
    {
      get { return _isDroppedDown; }
      set
      {
        if (_isDroppedDown == true && value == false)
        {
          if (_popupControl.IsDropDown)
          {
            _popupControl.Close();
          }
        }

        _isDroppedDown = value;

        if (_isDroppedDown)
        {
          _controlHost.Control.Width = _dropDownWidth;

          _listBox.Refresh();

          if (_listBox.Items.Count > 0)
          {
            int h = 0;
            int i = 0;
            int maxItemHeight = 0;
            int highestItemHeight = 0;
            foreach (object item in _listBox.Items)
            {
              int itHeight = _listBox.GetItemHeight(i);
              if (highestItemHeight < itHeight)
              {
                highestItemHeight = itHeight;
              }
              h = h + itHeight;
              if (i <= (_maxDropDownItems - 1))
              {
                maxItemHeight = h;
              }
              i = i + 1;
            }

            if (maxItemHeight > _dropDownHeight)
              _listBox.Height = _dropDownHeight + 2 * 4;
            else
            {
              if (maxItemHeight > highestItemHeight)
                _listBox.Height = maxItemHeight + 2 * 4;
              else
                _listBox.Height = highestItemHeight + 2 * 4;
            }
          }
          else
          {
            _listBox.Height = 40;
          }

          _popupControl.Show(this, CalculateDropPosition(), ToolStripDropDownDirection.BelowRight);
        }

        Invalidate();
        if (_isDroppedDown)
          OnDroppedDown(this, EventArgs.Empty);

      }
    }

    [Category("Appearance")]
    public Int32 CornerRadius
    {
      get
      {
        return m_radius.TopLeft;
      }
      set
      {
        m_radius.TopLeft = value;
        m_radius.TopRight = value;
        m_radius.BottomLeft = value;
        m_radius.BottomRight = value;

        if (DesignMode && this.Parent != null)
        {
          this.Parent.Refresh(); // Refreshes the client area of the parent control
        }
      }
    }

    [Category("Appearance")]
    public AutoCompleteMode AutoCompleteMode
    {
      get
      {
        return m_auto_complete_mode;
      }
      set
      {
        m_auto_complete_mode = value;
      }
    }

    public String ValueMember
    {
      get
      { return _listBox.ValueMember; }
      set
      { _listBox.ValueMember = value; }
    }

    public String DisplayMember
    {
      get
      { return _listBox.DisplayMember; }
      set
      { _listBox.DisplayMember = value; }
    }

    public Object SelectedValue
    {
      get
      { return _listBox.SelectedValue; }
      set
      { _listBox.SelectedValue = value; }
    }

    public Object SelectedItem
    {
      get
      { return _listBox.SelectedItem; }
      set
      { _listBox.SelectedItem = value; }
    }

    public Int32 SelectedIndex
    {
      get
      { return _listBox.SelectedIndex; }
      set
      { _listBox.SelectedIndex = value; }
    }

    public Boolean FormattingEnabled
    {
      get
      { return _listBox.FormattingEnabled; }
      set
      { _listBox.FormattingEnabled = value; }
    }

    public Int32 SelectionStart
    {
      get { return this._textBox.SelectionStart; }
      set
      {
        this._textBox.SelectionStart = value;
      }
    }

    public Int32 SelectionLength
    {
      get { return this._textBox.SelectionLength; }
      set
      {
        this._textBox.SelectionLength = value;
      }
    }

    #endregion

    protected override void Dispose(bool disposing)
    {
      if (_popupControl != null)
      {
      _popupControl.Dispose();
      }
      _popupControl = null;
      //DataSource = null;
      base.Dispose(disposing);
    }

    #region Constructor
    public uc_round_combobox()
    {
      SetStyle(ControlStyles.AllPaintingInWmPaint, true);
      SetStyle(ControlStyles.ContainerControl, true);
      SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
      SetStyle(ControlStyles.ResizeRedraw, true);
      SetStyle(ControlStyles.SupportsTransparentBackColor, true);
      SetStyle(ControlStyles.UserMouse, true);
      SetStyle(ControlStyles.UserPaint, true);
      SetStyle(ControlStyles.Selectable, true);

      SetComboBoxStyle();

      this.TabStop = false;

      base.BackColor = Color.Transparent;
      m_radius.BottomLeft = 5;
      m_radius.BottomRight = 5;
      m_radius.TopLeft = 5;
      m_radius.TopRight = 5;

      this.Height = 21;
      this.Width = 95;

      this.SuspendLayout();
      _textBox = new UTextBox();
      _textBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
      _textBox.Location = new System.Drawing.Point(3, 4);
      _textBox.Size = new System.Drawing.Size(60, 13);
      _textBox.TabIndex = 1;
      _textBox.TabStop = true;
      _textBox.WordWrap = false;
      _textBox.Margin = new Padding(0);
      _textBox.Padding = new Padding(0);
      _textBox.TextAlign = HorizontalAlignment.Left;

      this.Controls.Add(_textBox);
      this.ResumeLayout(false);

      AdjustControls();

      _listBox = new uc_round_list_box();
      _listBox.ItemHeight = this.Height;
      _listBox.Font = this.Font;
      _listBox.BindingContext = new BindingContext();

      _listBox.TabStop = false;

      _controlHost = new ToolStripControlHost(_listBox);
      _controlHost.Padding = Padding.Empty;
      _controlHost.Margin = Padding.Empty;
      _controlHost.BackColor = base.BackColor;

      _popupControl = new uc_ToolStripDropDown();
      _popupControl.AutoClose = false;
      _popupControl.Padding = new Padding(0);
      _popupControl.Margin = new Padding(0);
      _popupControl.DropShadowEnabled = false;
      _popupControl.Items.Add(_controlHost);
      _popupControl.TabStop = false;
      _popupControl.BackColor = Color.Transparent;

      _dropDownWidth = this.Width;

      _listBox.SelectedIndexChanged += _listBox_SelectedIndexChanged;
      _listBox.MouseClick += new MouseEventHandler(_listBox_MouseClick);
      _listBox.MouseDown += new MouseEventHandler(_listBox_MouseDown);
      _listBox.DataSourceChanged += new EventHandler(_listBox_DataSourceChanged);
      _listBox.DisplayMemberChanged += new EventHandler(_listBox_DisplayMemberChanged);
      _listBox.ValueMemberChanged += new EventHandler(_listBox_ValueMemberChanged);
      _listBox.KeyDown += new KeyEventHandler(_listBox_KeyDown);
      _listBox.MouseLeave += new EventHandler(_listBox_MouseLeave);

      _popupControl.Closed += new ToolStripDropDownClosedEventHandler(_popupControl_Closed);
      _popupControl.MouseLeave += new EventHandler(_listBox_MouseLeave);

      _controlHost.MouseLeave += new EventHandler(_listBox_MouseLeave);

      _textBox.Resize += new EventHandler(_textBox_Resize);
      _textBox.TextChanged += new EventHandler(_textBox_TextChanged);
    }

    #endregion

    #region Overrides

    //SJA 23/02/2016 - Added to enable focus and cause drop down on got focus
    public new bool TabStop
    {
      get { return _textBox != null && _textBox.Visible ? _textBox.TabStop : m_tab_stop; }
      set
      {
        m_tab_stop = value;
        if (_textBox != null && _textBox.Visible)
        {
          _textBox.TabStop = value;
        }
      }
    }
    protected override void OnCreateControl()
    {
      if (this.DropDownStyle == ComboBoxStyle.DropDown)
      {
        _textBox.AutoCompleteCustomSource = m_auto_complete_string;
        _textBox.AutoCompleteMode = m_auto_complete_mode;
        _textBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
      }

      if (m_style != RoundComboBoxStyle.BASIC)
      {
        _listBox.Font = this.Font;
      }

      base.OnCreateControl();
    }

    protected override void OnKeyPress(KeyPressEventArgs e)
    {
      base.OnKeyPress(e);
    }

    protected override void OnKeyUp(KeyEventArgs e)
    {
      base.OnKeyUp(e);
    }

    protected override void OnEnabledChanged(EventArgs e)
    {
      Invalidate(true);
      base.OnEnabledChanged(e);

      SetComboBoxStyle();
    }

    protected override void OnForeColorChanged(EventArgs e)
    {
      _textBox.ForeColor = this.ForeColor;
      base.OnForeColorChanged(e);
    }

    public override Font Font
    {
      get
      {
        return base.Font;
      }
      set
      {
        resize = true;
        _textBox.Font = value;
        base.Font = value;
        Invalidate(true);
      }
    }

    protected override void OnControlAdded(ControlEventArgs e)
    {
      e.Control.MouseDown += new MouseEventHandler(Control_MouseDown);
      e.Control.MouseEnter += new EventHandler(Control_MouseEnter);
      e.Control.MouseLeave += new EventHandler(Control_MouseLeave);
      e.Control.GotFocus += new EventHandler(Control_GotFocus);
      e.Control.LostFocus += new EventHandler(Control_LostFocus);
      e.Control.KeyPress += new KeyPressEventHandler(Control_KeyPress);
      e.Control.KeyDown += new KeyEventHandler(Control_KeyDown);
      e.Control.KeyUp += new KeyEventHandler(Control_KeyUp);
      base.OnControlAdded(e);
    }

    protected override void OnMouseEnter(EventArgs e)
    {
      this.Invalidate(true);
      base.OnMouseEnter(e);
    }

    protected override void OnMouseLeave(EventArgs e)
    {
      Rectangle _control_area;
      Point _mouse_postion = MousePosition;

      _control_area = this.ClientRectangle;

      if (this._popupControl.Visible)
      {
        if ((this.PointToScreen(new Point(0, 0)).Y + this.Height + _controlHost.Height) > Screen.GetWorkingArea(this).Height)
        {
          _control_area = new Rectangle(new Point(0, this.ClientRectangle.Location.Y - _controlHost.Height), new Size(this.ClientRectangle.Width, this.ClientRectangle.Height + _controlHost.Height));
        }
        else
        {
          _control_area = new Rectangle(new Point(0, this.ClientRectangle.Location.Y), new Size(this.ClientRectangle.Width, this.ClientRectangle.Height + _controlHost.Height));
        }
      }

      if (!this.RectangleToScreen(_control_area).Contains(_mouse_postion))
      {
        Invalidate(true);
        this.IsDroppedDown = false;
      }

      base.OnMouseLeave(e);
    }

    private bool m_focus_opened = false;
    private bool m_on_focus_open_list_box =true;

    protected override void OnMouseDown(MouseEventArgs e)
    {
      //SJA 23/02/2016 - Added to enable focus and cause drop down on got focus
      if (m_focus_opened)
      {
        m_focus_opened = false;
        return;
      }
      if (this.RectangleToScreen(rectContent).Contains(MousePosition))
      {
        this.Invalidate(true);
        this.IsDroppedDown = !this.IsDroppedDown;

      }
      if (DropDownStyle == ComboBoxStyle.DropDownList)
      {
        this._listBox.Focus();
      }
      else
      {
        this._textBox.Focus();
      }
    }

    protected override void OnMouseUp(MouseEventArgs e)
    {
      Invalidate();
    }


    protected override void OnMouseWheel(MouseEventArgs e)
    {
      if (e.Delta < 0)
        this.SelectedIndex = this._listBox.SelectedIndex + 1;
      else if (e.Delta > 0)
      {
        if (this.SelectedIndex > 0)
          this.SelectedIndex = this._listBox.SelectedIndex - 1;
      }

      this.Invalidate(true);

      base.OnMouseWheel(e);
    }

    protected override void OnKeyDown(KeyEventArgs e)
    {
      base.OnKeyDown(e);

      switch (e.KeyData)
      {
        case Keys.Up:
          this.SelectedIndex = this._listBox.SelectedIndex - 1;
          break;
        case Keys.Down:
          this.SelectedIndex = this._listBox.SelectedIndex + 1;
          break;
        //SJA 23-02-2016 Added to push shift tab to host control
        case Keys.Shift | Keys.Tab:
          this.IsDroppedDown = false;
          //if (DropDownStyle == ComboBoxStyle.DropDownList)
        {
          Form _frm = this.FindForm();
          if (_frm != null)
          {
            _frm.SelectNextControl(this, false, false, true, true);
          }
        }
          break;
        case Keys.Enter:
        case Keys.Tab:
          this.IsDroppedDown = false;
        {
          Form _frm = this.FindForm();
          if (_frm != null)
          {
            if (DropDownStyle == ComboBoxStyle.DropDownList || DropDownStyle == ComboBoxStyle.Simple)
            {
              _frm.SelectNextControl(this, true, false, true, true);
            }
            else
            {
              var _ctrl =  this.GetNextControl(this, true);
              _frm.SelectNextControl(_ctrl, true, false, true, true);
            }
          }
        }
          break;
        default:
          break;
      }
    }

    protected override void OnGotFocus(EventArgs e)
    {
      Invalidate(true);
      base.OnGotFocus(e);
      //TabStop = false;
      //SJA 23/02/2016 - Added to enable focus and cause drop down on got focus
      if (m_on_focus_open_list_box && ( _textBox == null || !_textBox.Visible))
      {
        this.IsDroppedDown = !this.IsDroppedDown;
        this._listBox.Focus();
        m_focus_opened = true;
      }
      else
      {
        this._textBox.Focus();
      }
    }

    protected override void OnLeave(EventArgs e)
    {
      base.OnLeave(e);
      m_focus_opened = false;
    }

    protected override void OnLostFocus(EventArgs e)
    {
      if (!this.ContainsFocus)
      {
        Invalidate();
      }
      base.OnLostFocus(e);
    }

    protected virtual void OnDropDownClosed(EventArgs e)
    {
      if (DropDownClosed != null)
      {
        DropDownClosed(this, e);
      }
      m_focus_opened = false;
    }

    protected override void OnResize(EventArgs e)
    {
      if (resize)
      {

        resize = false;
        AdjustControls();

        Invalidate(true);
      }
      else
        Invalidate(true);

      if (DesignMode)
        _dropDownWidth = this.Width;
    }

    public override String Text
    {
      get
      {
        return _textBox.Text;
      }
      set
      {
        _textBox.Text = value;
        base.Text = _textBox.Text;
        OnTextChanged(EventArgs.Empty);
      }
    }

    protected void PaintTransparentBackground(Graphics Graphics, Rectangle ClipRect)
    {
      Graphics.Clear(Color.Transparent);
      if ((this.Parent != null))
      {
        ClipRect.Offset(this.Location);
        PaintEventArgs e = new PaintEventArgs(Graphics, ClipRect);
        GraphicsState state = Graphics.Save();
        Graphics.SmoothingMode = SmoothingMode.HighSpeed;
        try
        {
          Graphics.TranslateTransform((float)-this.Location.X, (float)-this.Location.Y);
          this.InvokePaintBackground(this.Parent, e);
          this.InvokePaint(this.Parent, e);
        }
        finally
        {
          Graphics.Restore(state);
          ClipRect.Offset(-this.Location.X, -this.Location.Y);
        }
      }
    }

    protected override void OnPaint(PaintEventArgs e)
    {
      e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;

      //content border
      Rectangle rectCont = rectContent;
      rectCont.X += 1;
      rectCont.Y += 1;
      rectCont.Width -= 3;
      rectCont.Height -= 3;
      GraphicsPath pathContentBorder = CreateRoundRectangle(rectCont, m_radius.TopLeft, m_radius.TopRight, m_radius.BottomRight,
          m_radius.BottomLeft);

      //button border
      Rectangle rectButton = rectBtn;
      rectButton.X += 1;
      rectButton.Y += 1;
      rectButton.Width -= 10;
      rectButton.Height -= 3;
      GraphicsPath pathBtnBorder = CreateRoundRectangle(rectButton, 0, m_radius.TopRight, m_radius.BottomRight, 0);

      //outer border
      Rectangle rectOuter = rectContent;
      rectOuter.Width -= 1;
      rectOuter.Height -= 1;
      GraphicsPath pathOuterBorder = CreateRoundRectangle(rectOuter, m_radius.TopLeft, m_radius.TopRight, m_radius.BottomRight,
          m_radius.BottomLeft);

      //inner border
      Rectangle rectInner = rectContent;
      rectInner.X += 1;
      rectInner.Y += 1;
      rectInner.Width -= 3;
      rectInner.Height -= 3;
      GraphicsPath pathInnerBorder = CreateRoundRectangle(rectInner, m_radius.TopLeft, m_radius.TopRight, m_radius.BottomRight,
          m_radius.BottomLeft);

      Brush brBackground;
      brBackground = new SolidBrush(BackColor);
      Pen penOuterBorder = new Pen(BorderColor, 2);

      //draw
      e.Graphics.FillPath(brBackground, pathContentBorder);
      e.Graphics.DrawPath(penOuterBorder, pathOuterBorder);

      //Glimph
      Rectangle rectGlimph = rectButton;
      rectButton.Width -= 4;
      e.Graphics.TranslateTransform(rectGlimph.Left + rectGlimph.Width / 2.0f, rectGlimph.Top + rectGlimph.Height / 2.0f);
      GraphicsPath path = new GraphicsPath();
      PointF[] points = new PointF[3];
      points[0] = new PointF(-6 / 2.0f, -10 / 2.0f);
      points[1] = new PointF(18 / 2.0f, -10 / 2.0f);
      points[2] = new PointF(6 / 2.0f, 10 / 2.0f);

      Pen _arrow = new Pen(m_arrow_color, 2);
      e.Graphics.DrawLine(_arrow, points[0], points[2]);
      e.Graphics.DrawLine(_arrow, points[1], points[2]);
      e.Graphics.ResetTransform();
      _arrow.Dispose();
      path.Dispose();

      //text
      if (DropDownStyle == ComboBoxStyle.DropDownList)
      {
        StringFormat sf = new StringFormat(StringFormatFlags.NoWrap);
        sf.Alignment = StringAlignment.Near;

        Rectangle rectText = _textBox.Bounds;
        rectText.Offset(-3, 0);

        SolidBrush foreBrush = new SolidBrush(ForeColor);

        e.Graphics.DrawString(_textBox.Text, this.Font, foreBrush, rectText.Location);

      }

      pathContentBorder.Dispose();
      pathOuterBorder.Dispose();
      pathInnerBorder.Dispose();
      pathBtnBorder.Dispose();

      penOuterBorder.Dispose();

      brBackground.Dispose();
    }

    #endregion

    #region NestedControlsEvents

    void Control_LostFocus(object sender, EventArgs e)
    {
      OnLostFocus(e);
    }

    void Control_KeyDown(object sender, KeyEventArgs e)
    {
      OnKeyDown(e);
    }

    void Control_GotFocus(object sender, EventArgs e)
    {
      _textBox.Focus();
    }

    void Control_MouseLeave(object sender, EventArgs e)
    {
      OnMouseLeave(e);
    }

    void Control_MouseEnter(object sender, EventArgs e)
    {
      OnMouseEnter(e);
    }

    void Control_MouseDown(object sender, MouseEventArgs e)
    {
      OnMouseDown(e);
    }

    void _listBox_MouseClick(object sender, MouseEventArgs e)
    {
      if (DropDownStyle == ComboBoxStyle.DropDownList)
      {
        this.Invalidate(true);
      }

      IsDroppedDown = false;
    }

    void _listBox_MouseDown(object sender, MouseEventArgs e)
    {
      this.IsDroppedDown = false;
    }

    void _listBox_DataSourceChanged(object sender, EventArgs e)
    {
      RefreshAutoCompleteStringCollection();
    }

    void _listBox_DisplayMemberChanged(object sender, EventArgs e)
    {
      RefreshAutoCompleteStringCollection();
    }

    void _listBox_ValueMemberChanged(object sender, EventArgs e)
    {
      RefreshAutoCompleteStringCollection();
    }

    void _popupControl_Closed(object sender, ToolStripDropDownClosedEventArgs e)
    {
      _isDroppedDown = false;

      Invalidate(true);

      OnDropDownClosed(new EventArgs());
    }

    void _textBox_Resize(object sender, EventArgs e)
    {
      this.AdjustControls();
    }

    void _textBox_TextChanged(object sender, EventArgs e)
    {
      OnTextChanged(e);
    }

    void _listBox_MouseLeave(object sender, EventArgs e)
    {
      OnMouseLeave(e);
    }

    void _listBox_LostFocus(object sender, EventArgs e)
    {
      OnLostFocus(e);
    }

    void _listBox_KeyDown(object sender, KeyEventArgs e)
    {
      OnKeyDown(e);
    }

    void Control_KeyUp(object sender, KeyEventArgs e)
    {
      OnKeyUp(e);
    }

    void Control_KeyPress(object sender, KeyPressEventArgs e)
    {
      OnKeyPress(e);
    }

    void _listBox_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (_listBox != null)
      {
        if (_listBox.Items.Count == 0)
          return;

        if ((this.DataSource != null) && _listBox.SelectedIndex == -1)
          return;

        if (_listBox.SelectedIndex <= (_listBox.Items.Count - 1) && _listBox.SelectedIndex >= -1)
        {
          _selectedIndex = _listBox.SelectedIndex;
          _textBox.Text = _listBox.GetItemText(_listBox.SelectedItem);
        }

        if (DropDownStyle == ComboBoxStyle.DropDownList)
        {
          this.Invalidate(true);
        }

        OnSelectedIndexChanged(this, e);
      }
    }

    private void _listBox_MouseEnter(object sender, EventArgs e)
    {
      OnMouseEnter(e);
    }

    #endregion

    #region PrivateMethods

    private void AdjustControls()
    {
      this.SuspendLayout();

      resize = true;

      if (m_style == RoundComboBoxStyle.BASIC)
      {
        this.Height = 40;
      }
      else
      {
        if (this.Height <= _textBox.Height)
        {
          this.Height = _textBox.Height + 4;
        }
      }

      // Localización del textbox
      _textBox.Top = Math.Max(0, (this.Height - _textBox.Height) / 2);
      //SJA 27 JAN 2016 - Added margin so text in controls lines up well 
      //                  in uc_address when switching between auto and manual mode
      _textBox.Left = _margin;

      rectBtn =
              new System.Drawing.Rectangle(this.ClientRectangle.Width - 25,
              this.ClientRectangle.Top, 18, _textBox.Height + 2 * _textBox.Top);


      _textBox.Width = rectBtn.Left - 1 - _textBox.Left;

      rectContent = new Rectangle(ClientRectangle.Left + 1, ClientRectangle.Top,
          ClientRectangle.Width - 2, _textBox.Height + 2 * _textBox.Top);

      this.ResumeLayout();

      Invalidate(true);
    }

    private Point CalculateDropPosition()
    {
      Point point = new Point(0, this.Height);
      if ((this.PointToScreen(new Point(0, 0)).Y + this.Height + _controlHost.Height) > Screen.GetWorkingArea(this).Height)
      {
        point.Y = -this._controlHost.Height - 1;
      }
      return point;
    }

    private Point CalculateDropPosition(int myHeight, int controlHostHeight)
    {
      Point point = new Point(0, myHeight);
      if ((this.PointToScreen(new Point(0, 0)).Y + this.Height + controlHostHeight) > Screen.GetWorkingArea(this).Height)
      {
        point.Y = -controlHostHeight - 7;
      }
      return point;
    }

    private void SetComboBoxStyle()
    {
      CashierStyle.ControlStyle.InitStyle(this);
    }

    private void RefreshAutoCompleteStringCollection()
    {
      m_auto_complete_string = new AutoCompleteStringCollection();

      if (this.DataSource == null || String.IsNullOrEmpty(this.DisplayMember) || String.IsNullOrEmpty(this.ValueMember))
      {
        return;
      }
      if (this.DataSource is DataTable)
      {
        foreach (DataRow _row in ((DataTable)this.DataSource).Rows)
        {
          m_auto_complete_string.Add(_row[this.DisplayMember].ToString());
        }
      }
      else if (this.DataSource is BindingSource)
      {
        if (((BindingSource)this.DataSource).List.Count <= 0)
        {
          return;
        }

        if (m_table == null)

          m_table = ToDataTableOfProperties<IList>(((BindingSource)DataSource).List);

        foreach (DataRow _row in m_table.Rows)
        {
          m_auto_complete_string.Add(_row[this.DisplayMember].ToString());
        }
      }
      else if (this.DataSource is IList)
      {
        if (((IList)this.DataSource).Count <= 0)
        {
          return;
        }


        if (m_table == null)
          m_table = ToDataTableOfProperties<IList>(DataSource);

        foreach (DataRow _row in m_table.Rows)
        {
          m_auto_complete_string.Add(_row[this.DisplayMember].ToString());
        }
      }
      else
      {
        throw new Exception("Type not supported");
      }
    }


    #endregion

    #region VirtualMethods

    public virtual void OnDroppedDown(object sender, EventArgs e)
    {
      if (DroppedDown != null)
      {
        DroppedDown(this, e);
      }
    }

    public virtual void OnSelectionChangeCommitted(object sender, EventArgs e)
    {
      if (SelectionChangeCommitted != null)
      {
        SelectionChangeCommitted(this, e);
      }
    }

    public virtual void OnSelectedIndexChanged(object sender, EventArgs e)
    {
      if (SelectedIndexChanged != null)
      {
        SelectedIndexChanged(this, e);
      }
    }

    #endregion

    #region PublicMethods
    public void RefreshItems()
    {
      this._listBox.RefreshItems();
    }

    public Int32 FindStringExact(String InputText)
    {
      Int32 _candidate;

      _candidate = -1;

      if (DataSource != null)
      {
        if (DataSource is DataTable)
        {
          for (int _idx = 0; _idx < _listBox.Items.Count; _idx++)
          {
            DataRow _row = ((DataTable)this.DataSource).Rows[_idx];
            if (String.Compare(_row[DisplayMember].ToString().ToUpper(), InputText.ToUpper()) == 0)
            {
              _candidate = _idx;
              break;
            }
          }
        }
        else if (DataSource is BindingSource)
        {
          if (((BindingSource)this.DataSource).List.Count <= 0)
          {
            return -1;
          }


          if (m_table == null)
            m_table = ToDataTableOfProperties<IList>(((BindingSource)DataSource).List);
          for (int _idx = 0; _idx < _listBox.Items.Count; _idx++)
          {
            DataRow _row = ((DataTable)m_table).Rows[_idx];
            if (String.Compare(_row[DisplayMember].ToString().ToUpper(), InputText.ToUpper()) == 0)
            {
              _candidate = _idx;
              break;
            }
          }
        }
        else if (DataSource is IList) 
        {
          if (((IList)this.DataSource).Count <= 0)
          {
            return -1;
          }


          if (m_table == null)
            m_table = ToDataTableOfProperties<IList>(DataSource);
          for (int _idx = 0; _idx < _listBox.Items.Count; _idx++)
          {
            DataRow _row = ((DataTable)m_table).Rows[_idx];
            if (String.Compare(_row[DisplayMember].ToString().ToUpper(), InputText.ToUpper()) == 0)
            {
              _candidate = _idx;
              break;
            }
          }
        }
      }
      return _candidate;
    }

    private DataTable ToDataTableOfProperties<T>(object DataSource)
    {
      PropertyDescriptorCollection _props;
      DataTable _table = new DataTable();
      try
      {
        _props = TypeDescriptor.GetProperties(((T)DataSource).GetType().GetGenericArguments()[0]);
      }
      catch (Exception)
      {
        return _table;
      }
      for (int i = 0; i < _props.Count; i++)
      {
        PropertyDescriptor prop = _props[i];
        _table.Columns.Add(prop.Name, prop.PropertyType);
      }

      object[] values = new object[_props.Count];
      foreach (var item in ((IList) this.DataSource))
      {
        for (int i = 0; i < values.Length; i++)
        {
          values[i] = _props[i].GetValue(item);
        }
        _table.Rows.Add(values);
      }
      return _table;
    }

    private DataTable m_table = null;

    public Int32 FindStringStartsWith(String InputText)
    {
      Int32 _candidate;

      _candidate = -1;

      if (DataSource != null)
      {
        if (DataSource is DataTable)
        {
          for (int _idx = 0; _idx < _listBox.Items.Count; _idx++)
          {
            DataRow _row = ((DataTable)this.DataSource).Rows[_idx];
            if (_row[DisplayMember].ToString().ToUpper().StartsWith(InputText.ToUpper()))
            {
              _candidate = _idx;
              break;
            }
          }
        }
        else if (DataSource is BindingSource)
        {
          if (((BindingSource)this.DataSource).List.Count <= 0)
          {
            return -1;
          }


          if (m_table == null)
            m_table = ToDataTableOfProperties<IList>(((BindingSource)DataSource).List);
          for (int _idx = 0; _idx < _listBox.Items.Count; _idx++)
          {
            DataRow _row = ((DataTable)m_table).Rows[_idx];
            if (!_row[DisplayMember].ToString().ToUpper().StartsWith(InputText.ToUpper()))
            {
              continue;
            }
            _candidate = _idx;
            break;
          }
        }
        else if (DataSource is IList)
        {
          if (((IList)this.DataSource).Count <= 0)
          {
            return -1;
          }

         
          if(m_table== null)
            m_table = ToDataTableOfProperties<IList>(DataSource);
          for (int _idx = 0; _idx < _listBox.Items.Count; _idx++)
          {
            DataRow _row = ((DataTable)m_table).Rows[_idx];
            if (!_row[DisplayMember].ToString().ToUpper().StartsWith(InputText.ToUpper()))
            {
              continue;
            }
            _candidate = _idx;
            break;
          }
        }
      }
      return _candidate;
    }

    public void Set<T>(T setValue, string subjectGP = null) where T : ISelectableOption
    {
      string _defaultParam;
      int _defaultIndex;
      IList<T> _data;
      T _item;

      _defaultIndex = 0;
      subjectGP = subjectGP == null ? string.Empty : subjectGP;
      _defaultParam = GeneralParam.GetString("Account.DefaultValues", subjectGP, "");

      if (this.DataSource != null)
      {
        _data = (IList<T>)this.DataSource;
        if (!string.IsNullOrEmpty(_defaultParam))
        {
          switch (subjectGP)
          {
            case "Country":
              _defaultIndex = this.GetParamCountryIndex<T>(_data, _defaultParam);
              break;
            case "DocumentType":
              _defaultIndex = this.GetParamDocumentTypeIndex(_data, _defaultParam);
              break;
          }
        }
        _defaultIndex = this.GetIndex<T>(_data, setValue, subjectGP, _defaultIndex);
        _item = _data[_defaultIndex];

        this.SelectedIndex = _defaultIndex;
        this.SelectedItem = _item.Name;
        this.SelectedValue = _item.Id;

      }
     
    }

    private int GetParamCountryIndex<T>(IList<T> data, string defaultParam) where T : ISelectableOption
    {
      DataTable _isoCodes;
      DataRow[] _rows;
      string _countryName;  
      int _index;

      _index = 0;
      _isoCodes = CardData.GetCountriesList();
      _rows = _isoCodes.Select(string.Format("CO_ISO2 = '{0}'", defaultParam));
      if (_rows.Length > 0)
      {
        _countryName = _rows[0][1].ToString();
        for (int _i = 0; _i < data.Count; _i++)
        {       
          if (data[_i].Name == _countryName)
          {
            _index = _i;
            break;

          }
        }
      }

      return _index;

    }

    private int GetParamDocumentTypeIndex<T>(IList<T> data, string defaultParam) where T : ISelectableOption
    {
      int _index;
      int _valueID;

      _index = 0;
      if (int.TryParse(defaultParam, out _valueID))
      {
        for (int _i = 0; _i < data.Count; _i++)
        {
          if (data[_i].Id == _valueID)
          {
            _index = _i;
            break;

          }
        }
      }
      return _index;
    }

    private int GetIndex<T>(IList<T> data, T setValue, string subjectGP, int defaultIndex) where T : ISelectableOption
    {
      int _minIndex;

      _minIndex = subjectGP == "DocumentType" ? -1 : 0;
      if (setValue != null && setValue.Id > _minIndex)
      {

        for (int _i = 0; _i < data.Count; _i++)
        {
          if (data[_i].Id == setValue.Id)
          {
            defaultIndex = _i;
            break;
          }
        }

      }

      return defaultIndex;
    }

    #endregion

    #region Render

    public static GraphicsPath CreateRoundRectangle(Rectangle rectangle, int topLeftRadius, int topRightRadius,
        int bottomRightRadius, int bottomLeftRadius)
    {
      GraphicsPath path = new GraphicsPath();
      int l = rectangle.Left;
      int t = rectangle.Top;
      int w = rectangle.Width;
      int h = rectangle.Height;

      if (topLeftRadius > 0)
      {
        path.AddArc(l, t, topLeftRadius * 2, topLeftRadius * 2, 180, 90);
      }
      path.AddLine(l + topLeftRadius, t, l + w - topRightRadius, t);
      if (topRightRadius > 0)
      {
        path.AddArc(l + w - topRightRadius * 2, t, topRightRadius * 2, topRightRadius * 2, 270, 90);
      }
      path.AddLine(l + w, t + topRightRadius, l + w, t + h - bottomRightRadius);
      if (bottomRightRadius > 0)
      {
        path.AddArc(l + w - bottomRightRadius * 2, t + h - bottomRightRadius * 2,
            bottomRightRadius * 2, bottomRightRadius * 2, 0, 90);
      }
      path.AddLine(l + w - bottomRightRadius, t + h, l + bottomLeftRadius, t + h);
      if (bottomLeftRadius > 0)
      {
        path.AddArc(l, t + h - bottomLeftRadius * 2, bottomLeftRadius * 2, bottomLeftRadius * 2, 90, 90);
      }
      path.AddLine(l, t + h - bottomLeftRadius, l, t + topLeftRadius);
      path.CloseFigure();
      return path;
    }

    #endregion

    public class BNRadius
    {
      private int _topLeft = 0;

      public int TopLeft
      {
        get { return _topLeft; }
        set { _topLeft = value; }
      }

      private int _topRight = 0;

      public int TopRight
      {
        get { return _topRight; }
        set { _topRight = value; }
      }

      private int _bottomLeft = 0;

      public int BottomLeft
      {
        get { return _bottomLeft; }
        set { _bottomLeft = value; }
      }

      private int _bottomRight = 0;

      public int BottomRight
      {
        get { return _bottomRight; }
        set { _bottomRight = value; }
      }
    }
  }

  class uc_ToolStripDropDown : ToolStripDropDown
  {
    private Rectangle m_rect_content = new Rectangle(0, 0, 1, 1);
    private WSI.Cashier.Controls.uc_round_list_box.Radius m_radius = new WSI.Cashier.Controls.uc_round_list_box.Radius();

    public uc_ToolStripDropDown()
      : base()
    {
      m_radius.TopLeft = 3;
      m_radius.TopRight = 5;
      m_radius.BottomLeft = 5;
      m_radius.BottomRight = 5;

      m_rect_content = new Rectangle(ClientRectangle.Left - 1, ClientRectangle.Top - 1,
          ClientRectangle.Width - 2, ClientRectangle.Height - 2);
    }

    protected override void OnPaint(PaintEventArgs e)
    {
      base.OnPaint(e);
      Rectangle _rect_outer;
      GraphicsPath _path_outer_border;

      e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;

      _rect_outer = m_rect_content;

      _path_outer_border = CreateRoundRectangle(_rect_outer, m_radius.TopLeft, m_radius.TopRight, m_radius.BottomRight, m_radius.BottomLeft);

      this.Region = new Region(_path_outer_border);

    }

    protected override void OnSizeChanged(EventArgs e)
    {
      base.OnSizeChanged(e);

      m_rect_content = new Rectangle(ClientRectangle.Left + 1, ClientRectangle.Top + 1,
          ClientRectangle.Width - 2, ClientRectangle.Height - 2);
    }

    public static GraphicsPath CreateRoundRectangle(Rectangle Rectangle, Int32 TopLeftRadius, Int32 TopRightRadius, Int32 BottomRightRadius, Int32 BottomLeftRadius)
    {
      GraphicsPath _path = new GraphicsPath();
      Int32 _left;
      Int32 _top;
      Int32 _width;
      Int32 _height;

      _left = Rectangle.Left;
      _top = Rectangle.Top;
      _width = Rectangle.Width;
      _height = Rectangle.Height;

      if (TopLeftRadius > 0)
      {
        _path.AddArc(_left, _top, TopLeftRadius * 2, TopLeftRadius * 2, 180, 90);
      }
      _path.AddLine(_left + TopLeftRadius, _top, _left + _width - TopRightRadius, _top);
      if (TopRightRadius > 0)
      {
        _path.AddArc(_left + _width - TopRightRadius * 2, _top, TopRightRadius * 2, TopRightRadius * 2, 270, 90);
      }
      _path.AddLine(_left + _width, _top + TopRightRadius, _left + _width, _top + _height - BottomRightRadius);
      if (BottomRightRadius > 0)
      {
        _path.AddArc(_left + _width - BottomRightRadius * 2, _top + _height - BottomRightRadius * 2,
            BottomRightRadius * 2, BottomRightRadius * 2, 0, 90);
      }
      _path.AddLine(_left + _width - BottomRightRadius, _top + _height, _left + BottomLeftRadius, _top + _height);
      if (BottomLeftRadius > 0)
      {
        _path.AddArc(_left, _top + _height - BottomLeftRadius * 2, BottomLeftRadius * 2, BottomLeftRadius * 2, 90, 90);
      }
      _path.AddLine(_left, _top + _height - BottomLeftRadius, _left, _top + TopLeftRadius);
      _path.CloseFigure();
      return _path;
    }

    protected override void OnPaintBackground(PaintEventArgs e)
    {
      base.OnPaintBackground(e);

    }
  }


}
