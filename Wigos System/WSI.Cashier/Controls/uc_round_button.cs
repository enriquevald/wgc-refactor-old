﻿//------------------------------------------------------------------------------
// Copyright © 2015-2020 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_round_button.cs
// 
//   DESCRIPTION: Implements the uc_round_button user control
//                Class: uc_round_button
//
//        AUTHOR: David Hernández
// 
// CREATION DATE: 20-OCT-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 20-OCT-2015 DHA     First release.
// 09-NOV-2015 FOS     Product Backlog Item 5850:New button Type
// 22-JAN-2015 FOS     Bug 8587: Change Font "Montserrat" to "Open Sans"
// 09-MAR-2016 FOS    Fixed Bug 10139: Text on button doesn't show totally
//------------------------------------------------------------------------------

using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Windows.Forms;
using System.Xml.Serialization;


namespace WSI.Cashier.Controls
{
	public class uc_round_button : Button
  {
    public enum RoundButonStyle
    {
      NONE = 0,
      PRINCIPAL = 1,
      SECONDARY = 2,
      TERTIARY = 3,
      CALC_MONEY = 4,
      CALC_NUMBER = 5,
      CALC_BACKSPACE = 6,
      CALC_ENTER = 7,
      MENU = 8,
      MENU_SELECTED = 9,
      SUB_MENU = 10,
      SUB_MENU_SELECTED = 11,
      TOP_BAR = 12,
      PAYMENT_METHOD = 13,
      PAYMENT_METHOD_SELECTED = 14,
      CURRENCY_CHANGE = 15,
      GAMING_TABLE = 16,
    }

    const Int32 m_internal_margin = 5;
    const Int32 m_internal_margin_image_above = 5;
    
    private Int32 m_corners_radius;
    private TextFormatFlags _format_flags;

    private float m_text_size;

    [XmlElement(ElementName = "CornerRadius"), Category("Appearance"), DefaultValue(10)]
    public Int32 CornerRadius
    {
      get
      {
        return m_corners_radius;
      }
      set
      {
        m_corners_radius = value;

        if (DesignMode && this.Parent != null)
        {
          this.Parent.Refresh(); // Refreshes the client area of the parent control
        }
      }
    }

    private Boolean m_is_selected;

    public Boolean IsSelected
    {
      get
      {
        return m_is_selected;
      }
      set
      {
        m_is_selected = value;
        this.Refresh();
      }
    }

    private Color m_selected_color;

    public Color SelectedColor
    {
      get
      {
        return m_selected_color;
      }
      set
      {
        m_selected_color = value;
      }
    }

    private RoundButonStyle m_style;

    [XmlElement(ElementName = "Style"), Category("Appearance")]
    public RoundButonStyle Style
    {
      get
      {
        return m_style;
      }
      set
      {
        m_style = value;

        SetButtonStyle();
      }
    }

    public new Image Image
    {
      get { return base.Image; }
      set
      {
        base.Image = value;

        AdjustFontSize();
      }
    }

    public new TextImageRelation TextImageRelation
    {
      get { return base.TextImageRelation; }
      set
      {
        base.TextImageRelation = value;

        SetFormatFlags();
        AdjustFontSize();
      }
    }

    public new ContentAlignment TextAlign
    {
      get { return base.TextAlign; }
      set
      {
        base.TextAlign = value;

        SetFormatFlags();
        AdjustFontSize();
      }
    }

    public uc_round_button()
    {
      SetStyle(ControlStyles.SupportsTransparentBackColor, true);
      SetStyle(ControlStyles.OptimizedDoubleBuffer, true);

      this.FlatStyle = FlatStyle.Flat;
      this.FlatAppearance.BorderColor = BackColor;
      this.FlatAppearance.BorderSize = 0;

      m_text_size = this.Font.Size;
      
      SetFormatFlags();

    }

    private void SetButtonStyle()
    {
      CashierStyle.ControlStyle.InitStyle(this);

      m_text_size = this.Font.Size;

      AdjustFontSize();
    }

    protected override void OnSizeChanged(EventArgs e)
    {
      base.OnSizeChanged(e);

      AdjustFontSize();
    }

    protected override void OnCreateControl()
    {
      base.OnCreateControl();

      SetButtonStyle();

      SetFormatFlags();
    }

    protected override void OnVisibleChanged(EventArgs e)
    {
      base.OnVisibleChanged(e);
    }

    protected override void OnParentBackColorChanged(EventArgs e)
    {
      base.OnParentBackColorChanged(e);

      SetButtonStyle();
    }

    protected override void OnParentChanged(EventArgs e)
    {
      base.OnParentChanged(e);

      SetButtonStyle();
    }

    protected override void OnPaint(PaintEventArgs e)
    {
      Rectangle _text_area;
      SizeF _measure_string_area;
      Point _image_location;

      e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;

      this.SuspendLayout();

      PaintTransparentBackground(e.Graphics, this.ClientRectangle);

      DrawButton(e.Graphics);


      _text_area = new Rectangle(new Point(0, 0), this.Size);
      _image_location = new Point(0, 0);

      // Calculate Image position
      if (this.Image != null)
      {
        _measure_string_area = TextRenderer.MeasureText(this.Text,this.Font, new Size(this.Width, this.Height), _format_flags);
        
        if (this.TextImageRelation == System.Windows.Forms.TextImageRelation.TextAboveImage)
        {
          _text_area.Location = new Point(Margin.Left, Margin.Top + m_internal_margin);

          if (this.Height <= (Int32)_measure_string_area.Height + 2 * m_internal_margin + this.Image.Height || this.Height / 2 <= this.Image.Height)
          {
            _text_area.Size = new Size(_text_area.Size.Width, (Int32)_measure_string_area.Height);
            _image_location = new Point(this.Width / 2 - this.Image.Width / 2, _text_area.Location.Y + _text_area.Height + m_internal_margin);
          }
          else
          {
            _text_area.Size = new Size(_text_area.Size.Width, Math.Max((Int32)_measure_string_area.Height, this.Height / 2));
            _image_location = new Point(this.Width / 2 - this.Image.Width / 2, this.Height / 2);
          }
        }
        else if (this.TextImageRelation == System.Windows.Forms.TextImageRelation.ImageAboveText)
        {
          _image_location = new Point(this.Width / 2 - this.Image.Width / 2, Math.Max(0, this.Height / 2 - this.Image.Height) + m_internal_margin + m_internal_margin_image_above);

          _text_area.Size = new Size(_text_area.Size.Width, (Int32)_measure_string_area.Height);
          if (this.Height <= this.Image.Height + (Int32)_measure_string_area.Height + m_internal_margin)
          {
            _text_area.Location = new Point(0, Math.Max(m_internal_margin, this.Height - (Int32)_measure_string_area.Height - m_internal_margin) + m_internal_margin_image_above);
          }
          else
          {
            _text_area.Location = new Point(0, Math.Max(this.Image.Height, this.Height / 2) + m_internal_margin + m_internal_margin_image_above);
          }
        }
        else if (this.TextImageRelation == System.Windows.Forms.TextImageRelation.TextBeforeImage)
        {
          _text_area.Size = new Size(_text_area.Size.Width - this.Image.Width - Margin.Right - 2 * m_internal_margin, _text_area.Size.Height);
          _text_area.Location = new Point(Margin.Left + m_internal_margin, 0);
          _image_location = new Point(this.Width - this.Image.Width - this.Margin.Right - m_internal_margin, this.Height / 2 - this.Image.Height / 2);
        }
        else if (this.TextImageRelation == System.Windows.Forms.TextImageRelation.ImageBeforeText)
        {
          _text_area.Size = new Size(_text_area.Size.Width - this.Image.Width - Margin.Right - 2 * m_internal_margin, _text_area.Size.Height);
          _text_area.Location = new Point(this.Image.Width + Margin.Left + m_internal_margin, 0);
          _image_location = new Point(Margin.Left + m_internal_margin, this.Height / 2 - this.Image.Height / 2);
        }
        else
        {
          _image_location = new Point(this.Width / 2 - this.Image.Width / 2, this.Height / 2 - this.Image.Height / 2);
        }

        // Define tranform image Matrix
        float[][] _transform_img_matrix = new float[][] { 
                new float[] { 1, 0, 0, 0, 0 }, 
                new float[] { 0, 1, 0, 0, 0 }, 
                new float[] { 0, 0, 1, 0, 0 }, 
                new float[] { 0, 0, 0, 1, 0 }, 
                new float[] { 0, 0, 0, 0, 1 } 
            };

        // Paint Image
        if (!this.Enabled)
        {
          // Set gray scale when is disabled
          _transform_img_matrix = new float[][] { 
                new float[] { 0.299f, 0.299f, 0.299f, 0, 0 }, 
                new float[] { 0.587f, 0.587f, 0.587f, 0, 0 }, 
                new float[] { 0.114f, 0.114f, 0.114f, 0, 0 }, 
                new float[] { 0,      0,      0,      1, 0 }, 
                new float[] { 0,      0,      0,      0, 1 } 
            };
        }

        ImageAttributes _img_att = new ImageAttributes();
        _img_att.SetColorMatrix(new System.Drawing.Imaging.ColorMatrix(_transform_img_matrix));

        Rectangle _rect_img = new Rectangle(_image_location.X, _image_location.Y, this.Image.Width, this.Image.Height);

        e.Graphics.DrawImage(this.Image, _rect_img, 0, 0, this.Image.Width, this.Image.Height, GraphicsUnit.Pixel, _img_att);
      }

      // Paint the text
      TextRenderer.DrawText(e.Graphics, Text, Font, _text_area, this.ForeColor, Color.Transparent, _format_flags);

      this.ResumeLayout();
    }

    protected void DrawButton(Graphics graphics)
    {
      GraphicsPath _gr_path;
      GraphicsPath _gr_path_is_selected;
      GraphicsPath _gr_path_clip;
      Int32 _selected_border_size;
      float _top;
      float _left;
      float _right;
      float _bottom;

      graphics.SmoothingMode = SmoothingMode.AntiAlias;

      _gr_path = new GraphicsPath();
      _gr_path_clip = new GraphicsPath();
      _gr_path_is_selected = new GraphicsPath();
      _selected_border_size = 5;

      // Set clip area
      _top = -1;
      _left = -1;
      _right = Width;
      _bottom = Height;

      if (m_corners_radius <= 0)
      {
        _gr_path_clip.AddRectangle(new RectangleF(_left, _top, _right, _bottom));
      }
      else
      {
        _gr_path_clip.AddArc(_left, _top, m_corners_radius, m_corners_radius, 180, 90);
        _gr_path_clip.AddArc(_right - m_corners_radius, _top, m_corners_radius, m_corners_radius, 270, 90);
        _gr_path_clip.AddArc(_right - m_corners_radius, _bottom - m_corners_radius, m_corners_radius, m_corners_radius, 0, 90);
        _gr_path_clip.AddArc(_left, _bottom - m_corners_radius, m_corners_radius, m_corners_radius, 90, 90);
      }

      _gr_path_clip.CloseAllFigures();
      graphics.SetClip(_gr_path_clip);

      // Draw button figure
      _top = 0;
      _left = 0;
      _right = Width - 1;
      _bottom = Height - 1;
      
      if (m_corners_radius <= 0)
      {
        _gr_path.AddRectangle(new RectangleF(_left, _top, _right, _bottom));
        _gr_path_is_selected.AddRectangle(new RectangleF(_left + _selected_border_size, _top + _selected_border_size, _right - _selected_border_size, _bottom - _selected_border_size));

      }
      else
      {
        _gr_path.AddArc(_left, _top, m_corners_radius, m_corners_radius, 180, 90);
        _gr_path.AddArc(_right - m_corners_radius, _top, m_corners_radius, m_corners_radius, 270, 90);
        _gr_path.AddArc(_right - m_corners_radius, _bottom - m_corners_radius, m_corners_radius, m_corners_radius, 0, 90);
        _gr_path.AddArc(_left, _bottom - m_corners_radius, m_corners_radius, m_corners_radius, 90, 90);

        _gr_path_is_selected.AddArc(_left + _selected_border_size, _top + _selected_border_size, m_corners_radius, m_corners_radius, 180, 90);
        _gr_path_is_selected.AddArc(_right - m_corners_radius - _selected_border_size, _top + _selected_border_size, m_corners_radius, m_corners_radius, 270, 90);
        _gr_path_is_selected.AddArc(_right - m_corners_radius - _selected_border_size, _bottom - m_corners_radius - _selected_border_size, m_corners_radius, m_corners_radius, 0, 90);
        _gr_path_is_selected.AddArc(_left + _selected_border_size, _bottom - m_corners_radius - _selected_border_size, m_corners_radius, m_corners_radius, 90, 90);
      }

      _gr_path.CloseAllFigures();
      _gr_path_is_selected.CloseAllFigures();

      if (m_is_selected && m_selected_color != null && Enabled)
      {
        graphics.FillPath(new SolidBrush(SelectedColor), _gr_path);
        graphics.FillPath(new SolidBrush(BackColor), _gr_path_is_selected);
      }
      else
      {
        graphics.FillPath(new SolidBrush(BackColor), _gr_path);
      }
    }

    private void SetFormatFlags()
    {
      // Calculate Text position
      _format_flags = TextFormatFlags.WordBreak | TextFormatFlags.TextBoxControl;

      if (this.RightToLeft == System.Windows.Forms.RightToLeft.Yes)
      {
        _format_flags = _format_flags | TextFormatFlags.RightToLeft;
      }

      if (TextAlign == ContentAlignment.TopLeft ||
        TextAlign == ContentAlignment.TopCenter ||
        TextAlign == ContentAlignment.TopRight)
      {
        _format_flags = _format_flags | TextFormatFlags.Top;
      }
      else if (TextAlign == ContentAlignment.BottomLeft ||
          TextAlign == ContentAlignment.BottomCenter ||
          TextAlign == ContentAlignment.BottomRight)
      {
        _format_flags = _format_flags | TextFormatFlags.Bottom;
      }
      else if (TextAlign == ContentAlignment.MiddleLeft ||
          TextAlign == ContentAlignment.MiddleCenter ||
          TextAlign == ContentAlignment.MiddleRight)
      {
        _format_flags = _format_flags | TextFormatFlags.VerticalCenter;
      }

      if (TextAlign == ContentAlignment.TopLeft ||
       TextAlign == ContentAlignment.BottomLeft ||
       TextAlign == ContentAlignment.MiddleLeft)
      {
        _format_flags = _format_flags | TextFormatFlags.Left;
      }
      else if (TextAlign == ContentAlignment.TopCenter ||
       TextAlign == ContentAlignment.BottomCenter ||
       TextAlign == ContentAlignment.MiddleCenter)
      {
        _format_flags = _format_flags | TextFormatFlags.HorizontalCenter;
      }
      else if (TextAlign == ContentAlignment.TopRight ||
       TextAlign == ContentAlignment.BottomRight ||
       TextAlign == ContentAlignment.MiddleRight)
      {
        _format_flags = _format_flags | TextFormatFlags.Right;
      }
    }

    protected override void OnEnabledChanged(EventArgs e)
    {
      base.OnEnabledChanged(e);

      SetButtonStyle();

    }

    protected override void OnTextChanged(EventArgs e)
    {
      base.OnTextChanged(e);
      CashierStyle.ControlStyle.InitStyle(this);

      AdjustFontSize();
    }

    protected void PaintTransparentBackground(Graphics Graphics, Rectangle ClipRect)
    {
      Graphics.Clear(Color.Transparent);
      if ((this.Parent != null))
      {
        ClipRect.Offset(this.Location);
        PaintEventArgs e = new PaintEventArgs(Graphics, ClipRect);
        GraphicsState state = Graphics.Save();
        Graphics.SmoothingMode = SmoothingMode.HighSpeed;
        try
        {
          Graphics.TranslateTransform((float)-this.Location.X, (float)-this.Location.Y);
          this.InvokePaintBackground(this.Parent, e);
          this.InvokePaint(this.Parent, e);
        }
        finally
        {
          Graphics.Restore(state);
          ClipRect.Offset(-this.Location.X, -this.Location.Y);
        }
      }
    }

    private void AdjustFontSize()
    {
      Size _text_size;
      Graphics _graphics;
      SizeF _measure_string_area;

      _text_size = this.Size;
      _graphics = this.CreateGraphics();

      if (String.IsNullOrEmpty(this.Text))
      {
        return;
      }

      _measure_string_area = TextRenderer.MeasureText(this.Text, CashierStyle.Fonts.ChangeFontSize(m_text_size, this.Font.Bold), new Size(this.Width, Int32.MaxValue), _format_flags);

      if (this.Image != null)
      {
        if (this.TextImageRelation == System.Windows.Forms.TextImageRelation.ImageAboveText ||
          this.TextImageRelation == System.Windows.Forms.TextImageRelation.TextAboveImage)
        {
          _text_size = new System.Drawing.Size(this.Width, this.Height - this.Image.Height);
          _measure_string_area = TextRenderer.MeasureText(this.Text, CashierStyle.Fonts.ChangeFontSize(m_text_size, this.Font.Bold), _text_size, _format_flags);
          _text_size = new Size(_text_size.Width, (Int32)_measure_string_area.Height);
        }
        else if (this.TextImageRelation == System.Windows.Forms.TextImageRelation.ImageBeforeText ||
          this.TextImageRelation == System.Windows.Forms.TextImageRelation.TextBeforeImage)
        {
          _text_size = new Size(_text_size.Width - this.Image.Width, _text_size.Height);
          _measure_string_area = TextRenderer.MeasureText(this.Text, CashierStyle.Fonts.ChangeFontSize(m_text_size, this.Font.Bold), _text_size, _format_flags);
        }
      }
      
      if (this.Font.Size != 8 && (_measure_string_area.Height > _text_size.Height || _measure_string_area.Width > _text_size.Width))
      {
        CashierStyle.Fonts.ChangeFontControl(this, 8f);
      }
      else if (this.Font.Size != m_text_size && !(_measure_string_area.Height > _text_size.Height || _measure_string_area.Width > _text_size.Width))
      {
        CashierStyle.Fonts.ChangeFontControl(this, m_text_size);
      }
    }
  }
}
