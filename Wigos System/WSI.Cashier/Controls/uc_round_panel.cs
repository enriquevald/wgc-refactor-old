﻿//------------------------------------------------------------------------------
// Copyright © 2015-2020 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_round_button.cs
// 
//   DESCRIPTION: Implements the uc_round_button user control
//                Class: uc_round_button
//
//        AUTHOR: David Hernández
//                José Martínez
// 
// CREATION DATE: 22-OCT-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 22-OCT-2015 DHA     First release.
// 22-OCT-2015 JML     First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Xml.Serialization;
using System.ComponentModel;

namespace WSI.Cashier.Controls
{
  public class uc_round_panel : Panel
  {

    public enum RoundPanelStyle
    {
      NONE = 0,
      NORMAL = 1,
      DOUBLE = 2,
      TOTAL = 3,
      WITHOUT_HEAD = 4,
      ONLY_BORDER = 5,
      GAMING_TABLE = 6,
      MIN_HEAD = 7,
      NORMAL_WHITE = 8
    }

    private Int32 m_header_height;

    [Category("Appearance")]
    public Int32 HeaderHeight
    {
      get
      {
        return m_header_height;
      }
      set
      {
        m_header_height = value;

        if (DesignMode && this.Parent != null)
        {
          this.Parent.Refresh(); // Refreshes the client area of the parent control
        }
      }
    }

    private Color m_header_back_color;

    [Category("Appearance")]
    public Color HeaderBackColor
    {
      get
      {
        return m_header_back_color;
      }
      set
      {
        m_header_back_color = value;
      }
    }

    private Color m_border_color;

    [Category("Appearance")]
    public Color BorderColor
    {
      get
      {
        return m_border_color;
      }
      set
      {
        m_border_color = value;
      }
    }

    private String m_header_text;

    [Category("Appearance")]
    public String HeaderText
    {
      get
      {
        return m_header_text;
      }
      set
      {
        if (!String.IsNullOrEmpty(value))
        {
          m_header_text = value.ToUpper();
        }
        else
        {
          m_header_text = value;
        }

        if (DesignMode && this.Parent != null)
        {
          this.Parent.Refresh(); // Refreshes the client area of the parent control
        }
      }
    }

    private String m_header_sub_text;

    [Category("Appearance")]
    public String HeaderSubText
    {
      get
      {
        return m_header_sub_text;
      }
      set
      {
        if (!String.IsNullOrEmpty(value))
        {
          m_header_sub_text = value.ToUpper();
        }
        else
        {
          m_header_sub_text = value;
        }

        if (DesignMode && this.Parent != null)
        {
          this.Parent.Refresh(); // Refreshes the client area of the parent control
        }
      }
    }

    private Color m_header_fore_color;

    [Category("Appearance")]
    public Color HeaderForeColor
    {
      get
      {
        return m_header_fore_color;
      }
      set
      {
        m_header_fore_color = value;
      }
    }

    private Int32 m_corners_radius;

    [Category("Appearance")]
    public Int32 CornerRadius
    {
      get
      {
        return m_corners_radius;
      }
      set
      {
        if (value <= 0)
        {
          value = 1;
        }

        m_corners_radius = value;

        if (DesignMode && this.Parent != null)
        {
          this.Parent.Refresh(); // Refreshes the client area of the parent control
        }
      }
    }

    private Color m_panel_back_color;

    [Category("Appearance")]
    public Color PanelBackColor
    {
      get
      {
        return m_panel_back_color;
      }
      set
      {
        m_panel_back_color = value;
      }
    }

    private RoundPanelStyle m_style;

    [Category("Appearance")]
    public RoundPanelStyle Style
    {
      get
      {
        return m_style;
      }
      set
      {
        m_style = value;

        SetPanelStyle();
      }
    }

    public uc_round_panel()
    {
      SetStyle(ControlStyles.SupportsTransparentBackColor, true);
      SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
      base.BackColor = Color.Transparent;

      SetPanelStyle();

    }

    protected override void OnSizeChanged(EventArgs e)
    {
      base.OnSizeChanged(e);

      // Set Region
      using (Graphics _gr = this.CreateGraphics())
      {
        SetClipRegion(_gr);
      }
    }

    protected override void InitLayout()
    {
      base.InitLayout();
    }

    private void SetPanelStyle()
    {
      CashierStyle.ControlStyle.InitStyle(this);
    }

    protected override void OnParentBackColorChanged(EventArgs e)
    {
      base.OnParentBackColorChanged(e);
    }  

    protected override void OnPaint(PaintEventArgs e)
    {
      e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;

      this.SuspendLayout();

      // Draw panel
      DrawPanel(e.Graphics);

      //Draw border;
      if (m_header_height > 0)
      {
        DrawHeader(e.Graphics);
      }

      this.ResumeLayout();
    }

    protected override void OnEnabledChanged(EventArgs e)
    {
      SetPanelStyle();

      base.OnEnabledChanged(e);

      this.Invalidate(true);
    }

    private void DrawHeader(Graphics graphics)
    {
      GraphicsPath _gr_path;
      Int32 _top;
      Int32 _left;
      Int32 _right;
      Int32 _bottom;
      Font _font;

      _gr_path = new GraphicsPath();

      _top = 0;
      _left = 0;
      _right = Width - 1;
      _bottom = Height - 1;

      _font = this.Font;

      if (m_corners_radius <= 0)
      {
        _gr_path.AddLine(_left, _top, _right, _top);
        _gr_path.AddLine(_right, m_header_height, _left, m_header_height);
      }
      else
      {
        _gr_path.AddArc(_left, _top, m_corners_radius, m_corners_radius, 180, 90);
        _gr_path.AddArc(_right - m_corners_radius, _top, m_corners_radius, m_corners_radius, 270, 90);
        _gr_path.AddLine(_right, m_header_height, _left, m_header_height);
      }

      _gr_path.CloseFigure();

      graphics.FillPath(new SolidBrush(HeaderBackColor), _gr_path);

      if (m_style != RoundPanelStyle.DOUBLE)
      {
        if (!string.IsNullOrEmpty(m_header_text))
        {
          RectangleF _rect = new RectangleF(new PointF(0, 1), new SizeF(this.Width, m_header_height));
          using (Brush brush = new SolidBrush(this.HeaderForeColor))
          {
            StringFormat _str_format = new StringFormat();
            _str_format.Alignment = StringAlignment.Center;
            _str_format.LineAlignment = StringAlignment.Center;
            graphics.DrawString(m_header_text, _font, brush, _rect, _str_format);
          }
        }
      }
      else
      {
        Int32 _header_height;

        _header_height = 0;
        if (!string.IsNullOrEmpty(m_header_text))
        {
          if (m_style != RoundPanelStyle.MIN_HEAD)
          {
            _header_height = 35;
          }
          else
          {
            _header_height = 15;
          }
          using (Brush brush = new SolidBrush(this.HeaderForeColor))
          {
            RectangleF _rect = new RectangleF(new PointF(0, 0), new SizeF(this.Width, _header_height));
            StringFormat _str_format = new StringFormat();
            _str_format.Alignment = StringAlignment.Center;
            _str_format.LineAlignment = StringAlignment.Center;
            graphics.DrawString(m_header_text, _font, brush, _rect, _str_format);
          }
        }

        if (!string.IsNullOrEmpty(m_header_sub_text))
        {
          using (Brush brush = new SolidBrush(this.HeaderForeColor))
          {
            Font _sub_title_font = this.Font;
            _sub_title_font = CashierStyle.Fonts.ChangeFontSize(36, false);

            RectangleF _rect = new RectangleF(new PointF(0, _header_height), new SizeF(this.Width, this.HeaderHeight - _header_height));
            StringFormat _str_format = new StringFormat();
            _str_format.Alignment = StringAlignment.Center;
            _str_format.LineAlignment = StringAlignment.Center;
            graphics.DrawString(m_header_sub_text, _sub_title_font, brush, _rect, _str_format);
          }
        }
      }

    }

    private void DrawPanel(Graphics graphics)
    {
      GraphicsPath _gr_path;
      Int32 _top;
      Int32 _left;
      Int32 _right;
      Int32 _bottom;

      _gr_path = new GraphicsPath();

      _top = 0;
      _left = 0;
      _right = Width - 1;
      _bottom = Height - 1;

      if (m_corners_radius <= 0)
      {
        _gr_path.AddLine(_left, _top + m_header_height - 2, _right, _top + m_header_height);
        _gr_path.AddLine(_right, _bottom, _left, _bottom);
      }
      else
      {
        if (m_header_height == 0)
        {
          _gr_path.AddArc(_left, _top, m_corners_radius, m_corners_radius, 180, 90);
          _gr_path.AddArc(_right - m_corners_radius, _top, m_corners_radius, m_corners_radius, 270, 90);
        }
        else
        {
          _gr_path.AddLine(_left, _top + m_header_height - 2, _right, _top + m_header_height - 2);
        }
        _gr_path.AddArc(_right - m_corners_radius, _bottom - m_corners_radius, m_corners_radius, m_corners_radius, 0, 90);
        _gr_path.AddArc(_left, _bottom - m_corners_radius, m_corners_radius, m_corners_radius, 90, 90);
      }

      _gr_path.CloseFigure();

      graphics.FillPath(new SolidBrush(PanelBackColor), _gr_path);

      if (m_border_color != Color.Transparent)
      {
        graphics.DrawPath(new Pen(m_border_color, 1), _gr_path);
      }
    }

    private void SetClipRegion(Graphics graphics)
    {
      GraphicsPath _gr_path;
      Int32 _top;
      Int32 _left;
      Int32 _right;
      Int32 _bottom;

      _gr_path = new GraphicsPath();

      _top = 0;
      _left = 0;
      _right = Width;
      _bottom = Height;

      if (m_corners_radius <= 0)
      {
        _gr_path.AddLine(_left, _top, _right, _top);
        _gr_path.AddLine(_right, _bottom, _left, _bottom);
      }
      else
      {
        _gr_path.AddArc(_left, _top, m_corners_radius, m_corners_radius, 180, 90);
        _gr_path.AddArc(_right - m_corners_radius, _top, m_corners_radius, m_corners_radius, 270, 90);
        _gr_path.AddArc(_right - m_corners_radius, _bottom - m_corners_radius, m_corners_radius, m_corners_radius, 0, 90);
        _gr_path.AddArc(_left, _bottom - m_corners_radius, m_corners_radius, m_corners_radius, 90, 90);
      }

      _gr_path.CloseFigure();

      graphics.SetClip(_gr_path);

      this.Region = new Region(_gr_path);

    }
  }
}