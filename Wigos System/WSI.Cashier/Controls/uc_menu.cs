﻿//------------------------------------------------------------------------------
// Copyright © 2015-2020 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_menu.cs
// 
//   DESCRIPTION: Implements the user control for show the menu and sub menu
//
//        AUTHOR: Didac Campanals
// 
// CREATION DATE: 28-OCT-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 28-OCT-2015 DCS     First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using WSI.Common;

namespace WSI.Cashier.Controls
{
  public partial class uc_menu : UserControl
  {
    public enum MenuStyle
    {
      NONE = 0,
      MAIN_MENU = 1,
      SUB_MENU = 2,
    }

    #region Properties
    
    private MenuStyle m_style;
    private uc_round_button.RoundButonStyle m_menu_buton_style;
    private uc_round_button.RoundButonStyle m_menu_buton_hover_style;
    private Boolean m_show_logo;
    private Boolean m_auto_adjust_height;
    private Boolean m_auto_collapsed;
    private System.Windows.Forms.Timer m_timer;
    private Int32 m_time_to_collapse;
    public delegate void TimerPanel(object sender, EventArgs e);
    
    [Category("Appearance")]
    public MenuStyle Style
    {
      get
      {
        return m_style;
      }
      set
      {
        m_style = value;

        SetStyle();
      }
    }

    [Category("Appearance")]
    public Boolean AutoAdjustHeight
    {
      get
      {
        return m_auto_adjust_height;
      }
      set
      {
        m_auto_adjust_height = value;
      }
    }
   
    [Category("Appearance")]
    public uc_round_button.RoundButonStyle MenuButtonStyle
    {
      get
      {
        return m_menu_buton_style;
      }
      set
      {
        m_menu_buton_style = value;
      }
    }

    [Category("Appearance")]
    public uc_round_button.RoundButonStyle MenuButtonHoverStyle
    {
      get
      {
        return m_menu_buton_hover_style;
      }
      set
      {
        m_menu_buton_hover_style = value;
      }
    }

    [Category("Appearance")]
    public Boolean ShowLogo
    {
      get
      {
        return m_show_logo;
      }
      set
      {
        m_show_logo = value;
      }
    }

    [Category("Appearance")]
    public Boolean AutoCollapsed
    {
      get
      {
        return m_auto_collapsed;
      }
      set
      {
        m_auto_collapsed = value;
      }
    }

    public Int32 TimeToCollapse
    {
      get
      {
        return m_time_to_collapse;
      }
      set
      {
        if (value <= 0)
        {
          m_time_to_collapse = -1;
        }

        m_time_to_collapse = value;
      }
    }
   
    #endregion //Properties

    #region Private Methods
    
    private void SetStyle()
    {
      CashierStyle.ControlStyle.InitStyle(this, m_style);

      if (m_show_logo)
      {
        pb_image.Image = Resources.ResourceImages.menu_Win_Logo;
        pb_image.SizeMode = PictureBoxSizeMode.CenterImage;
        pb_image.Visible = m_show_logo;
        pb_image.Show();
       
      }
      
      if (this.Visible && m_time_to_collapse > 0)
      {
        PrepareAutoCollapse();
      }
    }

    private void AddMenuItem(String NameButton, String TextButton, System.Drawing.Bitmap ImageButton, CreateMenu.Menu.MyEventHandler ActionEvent, Boolean IsSelectable, Boolean IsEnabled, uc_round_button SubMenuButtonParent)
    {
      uc_round_button _btn = new uc_round_button();
      _btn.Name = NameButton;
      if (ImageButton != null) { 
        _btn.Image = WSI.Cashier.Images.Get32x32(ImageButton);
      }
      _btn.Text = TextButton;
      _btn.Style = MenuButtonStyle;
      if (Style != MenuStyle.SUB_MENU)
      {
        _btn.MouseCaptureChanged += btn_menu_Click;
        _btn.Tag = IsSelectable;
      }
      else
      {
        if (SubMenuButtonParent == null)
        {
          _btn.MouseCaptureChanged += btn_sub_menu_Click;
          _btn.Tag = IsSelectable;
        }
        else
        {
          if (IsSelectable)
          {
            _btn.MouseCaptureChanged += btn_sub_menu_Click;
            _btn.Tag = SubMenuButtonParent;
          }
        }
      }

      _btn.MouseCaptureChanged += ActionEvent.Invoke;
      _btn.Enabled = IsEnabled;
      
      if (ImageButton != null)
      {
        _btn.TextImageRelation = TextImageRelation.TextBeforeImage;
      }
      
      tlp_menu.Controls.Add(_btn, 0, tlp_menu.Controls.Count);

      tlp_menu.CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;
    }
        
    private void btn_menu_Click(object sender, EventArgs e)
    {
      if ((Boolean)((uc_round_button)sender).Tag)
      { 
        foreach (uc_round_button _btn in tlp_menu.Controls)
        {
          _btn.Style = MenuButtonStyle;
        }

        ((uc_round_button)sender).Style = MenuButtonHoverStyle;
      }
    }

    private void btn_sub_menu_Click(object sender, EventArgs e)
    {
      if (((uc_round_button)sender).Tag is uc_round_button)
      {
        foreach (uc_round_button _btn in ((uc_round_button)((uc_round_button)sender).Tag).Parent.Controls)
        {
          _btn.Style = MenuButtonStyle;
        }

        ((uc_round_button)((uc_round_button)sender).Tag).Style = MenuButtonHoverStyle;
      }
      CloseAndDispose();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : This is the method to run when the timer is raised.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private void TimerEvent(Object myObject, EventArgs myEventArgs)
    {
      CloseAndDispose();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Enables the timer and adds the timer handler.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private void PrepareAutoCollapse()
    {
      m_timer = new System.Windows.Forms.Timer();

      // Adds the event and the event handler for the method that will 
      // process the timer event to the timer. 
      m_timer.Tick += new EventHandler(TimerEvent);

      // Sets the timer interval to TimeToCollapse seconds.
      m_timer.Interval = this.m_time_to_collapse;
      m_timer.Start();

    }
    
    //------------------------------------------------------------------------------
    // PURPOSE : Clean Resources.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private void CloseAndDispose()
    {
      DisposeTimer();
      if (this.Parent != null)
      {
        this.Parent.Dispose();
      }
      else
      {
        this.Dispose();
      }
    }
    
    #endregion Private Methods
    
    #region public Methods
    
    public void FillMenu(List<CreateMenu.MenuItem> MenuList)
    {
      switch (Style)
      {
        case uc_menu.MenuStyle.MAIN_MENU:
          MenuButtonStyle = uc_round_button.RoundButonStyle.MENU;
          MenuButtonHoverStyle = uc_round_button.RoundButonStyle.MENU_SELECTED;
          break;
        case uc_menu.MenuStyle.SUB_MENU:
          MenuButtonStyle = uc_round_button.RoundButonStyle.SUB_MENU;
          MenuButtonHoverStyle = uc_round_button.RoundButonStyle.SUB_MENU_SELECTED;
          break;
      }

      foreach (CreateMenu.MenuItem _item in MenuList)
      {
        AddMenuItem(_item.Name, _item.Text, _item.Image, _item.Action, _item.IsSelectable, _item.IsEnabled, _item.SubMenuButtonParent);
      }

      tlp_menu.Height = tlp_menu.Controls.Count * Math.Abs(tlp_menu.Controls[0].Bottom - tlp_menu.Controls[1].Bottom);

      if (m_auto_adjust_height)
      {
        this.Height = tlp_menu.Height;
      }
    }

    public uc_menu()
    {
      InitializeComponent();


    }

    /// <summary>
    /// Stops timer and disposes it.
    /// </summary>
    public void DisposeTimer()
    {
      if (m_timer != null)
      { 
        m_timer.Stop();
        m_timer.Dispose();
      }
    }

    public void ForceButtonClick(String NameButton)
    {
      uc_round_button _btn;

      if (tlp_menu.Controls.ContainsKey(NameButton))
      { 
        _btn = (uc_round_button)tlp_menu.Controls.Find(NameButton, false)[0];
        _btn.PerformClick();
        btn_menu_Click(_btn, null);
      }
    }

    public void ForceParentButtonClick(String NameButton)
    {
      uc_round_button _btn;

      if (tlp_menu.Controls.ContainsKey(NameButton))
    {
        _btn = (uc_round_button)tlp_menu.Controls.Find(NameButton, false)[0];
        _btn.PerformClick();
        btn_menu_Click(_btn, null);
      }
    }

    public void SetEnabledButton(String NameButton, Boolean Value)
    {
      uc_round_button _btn;

      if (tlp_menu.Controls.ContainsKey(NameButton))
      {
        //_btn = (uc_round_button)tlp_menu.Controls.Find(NameButton, false)[0];
        _btn = (uc_round_button)tlp_menu.Controls[NameButton];
        _btn.Enabled = Value;
      }
      //revisar submenu.
    }
    
    #endregion public Methods

  }
}
