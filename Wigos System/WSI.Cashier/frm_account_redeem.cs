//------------------------------------------------------------------------------
// Copyright � 2014 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : frm_account_redeem.cs
// 
//   DESCRIPTION : Partial and total redeem
//                
//
// REVISION HISTORY :
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 07-JAN-2014 ACM    First release.
// 04-NOV-2015 SGB    Backlog Item WIG-5841: Change designer
// 11-NOV-2015 FOS    Product Backlog Item: 3709 Payment tickets & handpays
// 09-DEC-2015 DDS    Backlog Item 7423: Detect card is possibly used as automatic cashier
// 27-APR-2016 FAV    Fixed Bug 6368: Added message box if there is an error in 
// 18-JUL-2016 JMV    Product Backlog Item 15551:TPV Televisa: Uso del cajero como banco
// 19-SEP-2016 FGB    PBI 15985: Exped System: Cashier: Check if the payment/sale can be done
// 04-OCT-2016 LTC    PBItem 17964:Televisa - Cashdesk draw (Participation prize) - Configuration
// 07-OCT-2016 LTC    PBItem 17964:Televisa - Cashdesk draw (Participation prize) - Configuration
// 17-MAR-2017 ETP    WIGOS-194: Credit line: Add logic to avoid withdraw with debt.
// 24-MAR-2017 ATB    PBI 25262: Exped - Payment authorisation
// 17-MAR-2017 ETP    WIGOS-194: Credit line: Add logic to avoid withdraw with debt.
// 19-JUL-2017 LTC    Bug 28644:WIGOS-3468 Cashier.Withdrawal � BankCardRechargeVSCash.Pct --> Doesn't apply properly
// 26-JUL-2017 DPC    Bug 28977:[WIGOS-3902] Cashier - Incorrect prize amount when the user cancel a prize payment and try to do a total refund
// 14-AUG-2017 AMF    Bug 29341:[WIGOS-4428] Cashier is locked when cancelling a Prize Payout
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using WSI.Common;
using System.Collections;
using WSI.Cashier.TITO;
using WSI.Common.ExternalPaymentAndSaleValidation;

namespace WSI.Cashier
{
  public partial class frm_account_redeem : Controls.frm_base
  {

    #region Attributes

    private static frm_yesno form_yes_no;
    private frm_amount_input form_credit;
    private CardData m_card;
    private frm_amount_input.CheckPermissionsToRedeemDelegate m_check_permissions_to_redeem;
    private frm_amount_input.CheckAntiMoneyLaunderingDelegate m_check_anti_money_laundering;

    private Form m_parent_form;

    #endregion

    #region Constructor

    public frm_account_redeem(CardData RelatedCard, frm_amount_input.CheckPermissionsToRedeemDelegate CheckPermissionsToRedeem, frm_amount_input.CheckAntiMoneyLaunderingDelegate CheckAntiMoneyLaundering)
    {
      Boolean _redeem_allowed;

      InitializeComponent();

      form_yes_no = new frm_yesno();
      form_yes_no.Opacity = 0.6;

      // RCI 03-FEB-2014: Read all data (need the cancellable operations)
      if (!CardData.DB_CardGetAllData(RelatedCard.AccountId, RelatedCard))
      {
        throw new Exception(String.Format("frm_account_redeem: Can't read AccountId {0}", m_card.AccountId));
      }

      this.m_card = RelatedCard;
      this.m_check_permissions_to_redeem = CheckPermissionsToRedeem;
      this.m_check_anti_money_laundering = CheckAntiMoneyLaundering;

      InitControls();

      InitializeControlResources();

      // Balance
      this.uc_card_balance.FillCardCountersAll(m_card);
      // RCI 26-JUL-2012
      _redeem_allowed = (uc_card_balance.TotalPaid > 0);
      this.btn_credit_redeem_total.Enabled = _redeem_allowed;
      this.btn_credit_redeem_partial.Enabled = _redeem_allowed && uc_card_balance.PartialRedeemAllowed;

      // RCI & AJQ 24-SEP-2010: Also Redeem Total if CardDeposit > 0 and Anonymous Card and... Refundable (GP)
      if ((m_card.Deposit > 0 && m_card.PlayerTracking.HolderName == ""))
      {
        if (GeneralParam.GetInt32("Cashier", "CardRefundable") == 1) // ANONYMOUS_CARD_REFUNDABLE
        {
          btn_credit_redeem_total.Enabled = true;
        }
      }
    }

    #endregion

    #region Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Add event handler to scan and configure buttons
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    private void InitControls()
    {
      EventLastAction.AddEventLastAction(this.Controls);

      // Buttons events
      this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
      this.btn_credit_redeem_partial.Click += new EventHandler(this.btn_partial_redeem_Click);
      this.btn_credit_redeem_total.Click += new EventHandler(this.btn_total_redeem_Click);

      form_credit = new frm_amount_input(m_check_permissions_to_redeem, m_check_anti_money_laundering);
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Initialize control resources (NLS strings, images, etc).
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    private void InitializeControlResources()
    {
      //   - Title
      this.FormTitle = Resource.String("STR_ACCOUNT_REDEEM_001");

      //   - Buttons    
      this.btn_close.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CLOSE");
      this.btn_credit_redeem_partial.Text = Resource.String("STR_UC_CARD_BTN_CREDIT_REDEEM_PARTIAL");
      this.btn_credit_redeem_total.Text = Resource.String("STR_UC_CARD_BTN_CREDIT_REDEEM_TOTAL");
      this.btn_prize_payout.Text = Resource.String("STR_UC_CARD_BTN_PRIZE_PAYOUT");

      this.uc_card_balance.InitializeControlResources();

      this.form_credit.InitializeControlResources();

    } // InitializeControlResources

    private void btn_close_Click(object sender, EventArgs e)
    {
      Misc.WriteLog("[FORM CLOSE] frm_account_redeem: " + FormTitle, Log.Type.Message);
      this.Close();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Partial Redeem
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    private void btn_partial_redeem_Click(object sender, EventArgs e)
    {
      Currency amount_to_subtract;
      Boolean _check_redeem;
      PaymentOrder _payment_order;
      String _error_str;
      CardData _card_data;

      // Check if current user is an authorized user
      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.CardPartialRedeemCredit,
                                               ProfilePermissions.TypeOperation.RequestPasswd,
                                               m_parent_form,
                                               out _error_str))
      {
        return;
      }

      form_yes_no.Show(m_parent_form);

      try
      {
        // DDS 15-DEC-2015 Check Card is used as Automatic Bank Card
        //_request_result = m_card.CheckSuspiciousActivity(out _suspicious_result);
        //if (!_request_result)
        //{
        //  frm_message.Show(Resource.String("STR_UC_CARD_BANK_DB_ERROR"),
        //                     Resource.String("STR_APP_GEN_MSG_WARNING"),
        //                     MessageBoxButtons.OK,
        //                     MessageBoxIcon.Error,
        //                     m_parent_form);
        //  return;
        //}

        //if (_suspicious_result)
        //{
        //  _dialog_result = frm_message.Show(
        //                     Resource.String("STR_UC_CARD_BANK_SUSPICIOUS_USE"),
        //                     Resource.String("STR_APP_GEN_MSG_WARNING"),
        //                     MessageBoxButtons.YesNo,
        //                     MessageBoxIcon.Warning,
        //                     m_parent_form);
        //  if (_dialog_result != System.Windows.Forms.DialogResult.OK)
        //    return;
        //  // Check if current user is an authorized user
        //  if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.CardRedeemSuspiciousActivity,
        //                                           ProfilePermissions.TypeOperation.RequestPasswd, m_parent_form, out error_str))
        //  {
        //    return;
        //  }
        //}

        if (!CreditLine_CheckUserDebt(uc_card_balance.CreditLineSpent))
        {
          return;
        }

        if (!form_credit.Show(Resource.String("STR_UC_CARD_BTN_CREDIT_REDEEM_PARTIAL"),
                              out amount_to_subtract,
                              out _check_redeem,
                              VoucherTypes.CardRedeem,
                              m_card,
                              uc_card_balance.Cashable,
                              CASH_MODE.PARTIAL_REDEEM,
                              form_yes_no))
        {
          return;
        }

        // LTC 2017-JUL-19
        if ( WSI.Common.Cashier.ShowWithdrawalWarning(m_card.AccountId, amount_to_subtract)
          && frm_message.Show(Resource.String("STR_WITHDRAW_WARNING"), Resource.String("STR_APP_GEN_MSG_WARNING"), MessageBoxButtons.OKCancel, this) == DialogResult.Cancel)
        {
          return;
        }

        // DLL & RCI 28-OCT-2013: Moved CheckAntiMoneyLaundering() to frm_amount_input - btn_ok_Click() to add a permission to exceed report limit.
        _payment_order = new PaymentOrder();
        if (_check_redeem)
        {
          // Enter required data to generate the Witholding documents
          if (!CashierBusinessLogic.EnterOrderPaymentData(m_parent_form, m_card, amount_to_subtract, amount_to_subtract, null, out _payment_order))
          {
            // Data entering process was cancelled => no further process
            return;
          }
        }

        _card_data = new CardData();
        CardData.DB_CardGetAllData(m_card.AccountId, _card_data);

        if (_card_data.IsLoggedIn)
        {
          frm_message.Show(Resource.String("STR_UC_CARD_IN_USE_ERROR"),
                           Resource.String("STR_APP_GEN_MSG_WARNING"),
                           MessageBoxButtons.OK,
                           MessageBoxIcon.Warning,
                           m_parent_form);

          return;
        }
      }
      finally
      {
        form_yes_no.Hide();
      }

      if (amount_to_subtract == 0)
      {
        return;
      }

      // Redeem the credit
      if (!CashierBusinessLogic.DB_CardCreditRedeem(m_card.AccountId, null, _payment_order, amount_to_subtract, OperationCode.CASH_OUT, CASH_MODE.PARTIAL_REDEEM, out _error_str, true, m_card.PlayerTracking.HolderName))
      {
        Log.Error("btn_subtract_Click: DB_CardCreditRedeem. Partial. Error subtracting credit to card" + _error_str);

        if (!String.IsNullOrEmpty(_error_str))
        {
          _error_str += "\\r\\n" + "\\r\\n";
        }

        _error_str += Resource.String("STR_CREDIT_CARD_REDEEM_PARTIAL_ERROR");

        _error_str = _error_str.Replace("\\r\\n", "\r\n");

        if (ExternalPaymentAndSaleValidationCommon.ExpedMode != EnumExternalPaymentAndSaleValidationMode.MODE_EXPED
            || Misc.ExternalWS.OutputParams.ValidationResult != EnumExternalPaymentAndSaleValidationResult.RESULT_AUTHORIZED)
        {
          frm_message.Show(_error_str
                         , Resource.String("STR_APP_GEN_MSG_ERROR")
                         , MessageBoxButtons.OK
                         , Images.CashierImage.Error
                         , m_parent_form);
        }

        return;
      }

      Misc.WriteLog("[FORM CLOSE] frm_account_redeem (partial_redeem): " + FormTitle, Log.Type.Message);
      this.Close();
    } // btn_partial_redeem_Click

    //------------------------------------------------------------------------------
    // PURPOSE : Total Redeem
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    private void btn_total_redeem_Click(object sender, EventArgs e)
    {
      Currency _amount_to_subtract;
      Boolean _check_redeem;
      PaymentOrder _payment_order;
      ParamsTicketOperation _operations_params;
      String _error_str;

      //DialogResult _dialog_result;
      //Boolean _suspicious_result;
      //Boolean _data_request_result;

      // RCI 16-DEC-2013: Unifying code...
      _operations_params = new ParamsTicketOperation();
      _operations_params.in_account = m_card;
      _operations_params.in_window_parent = m_parent_form;
      if (WSI.Common.Misc.IsCashlessMode() || WSI.Common.Misc.IsMico2Mode())
      {
        _operations_params.is_apply_tax = true;
      }
      if (CashierBusinessLogic.WithholdAndPaymentOrderDialog(OperationCode.CASH_OUT,
                                                             ref _operations_params,
                                                             CashierBusinessLogic.EnterWitholdingData,
                                                             null,
                                                             m_check_permissions_to_redeem) != ENUM_TITO_OPERATIONS_RESULT.OK)
      {
        return;
      }

      // Allow the cashier redeem the whole account credit
      form_yes_no.Show(m_parent_form);

      try
      {
        // DDS 15-DEC-2015 Check Card is used as Automatic Bank Card
        //_data_request_result = m_card.CheckSuspiciousActivity(out _suspicious_result);
        //if (!_data_request_result)
        //{
        //  frm_message.Show(  Resource.String("STR_UC_CARD_BANK_DB_ERROR"),
        //                     Resource.String("STR_APP_GEN_MSG_WARNING"),
        //                     MessageBoxButtons.OK,
        //                     MessageBoxIcon.Error,
        //                     m_parent_form);
        //  return;
        //}

        //if (_suspicious_result)
        //{
        //  _dialog_result = frm_message.Show(
        //                     Resource.String("STR_UC_CARD_BANK_SUSPICIOUS_USE"),
        //                     Resource.String("STR_APP_GEN_MSG_WARNING"),
        //                     MessageBoxButtons.YesNo,
        //                     MessageBoxIcon.Warning,
        //                     m_parent_form);

        //  if (_dialog_result != System.Windows.Forms.DialogResult.OK)
        //    return;
        //  // Check if current user is an authorized user
        //  if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.CardRedeemSuspiciousActivity,
        //                                           ProfilePermissions.TypeOperation.RequestPasswd, m_parent_form, out error_str))
        //  {
        //    return;
        //  }
        //}

        // LTC 04-OCT-2016 CHECK IF CHASH IN IS THE LAST MOVEMENT | CHECK IF PARTICIPATION PRICE HAS REGISTERED
        if (Tax.ServiceChageOnlyCancellable == true)
        {
          //CHECK PERMMISIONS
          String _str_error;

          //MESSAGE
          //LTC 07-OCT-2016
          if (GeneralParam.GetBoolean("Cashier.TotalRedeem", "RedeemWithoutPlay.Enabled", false))
          {
            if (DialogResult.OK == frm_message.Show(Resource.String("STR_REDEEM_AUTHORIZATION_MSG"),
                           Resource.String("STR_APP_GEN_MSG_WARNING"),
                           MessageBoxButtons.YesNo,
                           MessageBoxIcon.Warning,
                           m_parent_form))
            {
              if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.RedeemWithoutPlay,
                                                  ProfilePermissions.TypeOperation.RequestPasswd, this, out _str_error))
              {
                return;
              }
            }
            else
            {
              return;
            }
          }
        }

        if (!CreditLine_CheckUserDebt(uc_card_balance.CreditLineSpent))
        {
          return;
        }

        if (!form_credit.Show(Resource.String("STR_UC_CARD_BTN_CREDIT_REDEEM_TOTAL"),
                              out _amount_to_subtract,
                              out _check_redeem,
                              VoucherTypes.CardRedeem,
                              m_card,
                              uc_card_balance.Cashable,
                              CASH_MODE.TOTAL_REDEEM,
                              form_yes_no))
        {
          return;
        }

        // LTC 2017-JUL-19
        if ( WSI.Common.Cashier.ShowWithdrawalWarning(m_card.AccountId, uc_card_balance.TotalPaid)
          && frm_message.Show(Resource.String("STR_WITHDRAW_WARNING"), Resource.String("STR_APP_GEN_MSG_WARNING"), MessageBoxButtons.OKCancel, this) == DialogResult.Cancel)
        {
          return;
        }

        _payment_order = new PaymentOrder();
        if (_check_redeem)
        {
          // Enter required data to generate the Witholding documents
          if (!CashierBusinessLogic.EnterOrderPaymentData(m_parent_form, m_card, _operations_params.cash_redeem_total_paid, _operations_params.cash_redeem_total_paid, _operations_params.withhold, out _payment_order))
          {
            // Data entering process was cancelled => no further process
            return;
          }
        }

        CardData.DB_CardGetAllData(m_card.AccountId, m_card);

        if (m_card.IsLoggedIn)
        {
          frm_message.Show(Resource.String("STR_UC_CARD_IN_USE_ERROR"),
                           Resource.String("STR_APP_GEN_MSG_WARNING"),
                           MessageBoxButtons.OK,
                           MessageBoxIcon.Warning,
                           m_parent_form);

          return;
        }
      }
      finally
      {
        form_yes_no.Hide();
      }

      // Redeem the credit
      if (!CashierBusinessLogic.DB_CardCreditRedeem(m_card.AccountId
                                                  , _operations_params.withhold
                                                  , _payment_order
                                                  , uc_card_balance.Cashable
                                                  , OperationCode.CASH_OUT
                                                  , CASH_MODE.TOTAL_REDEEM
                                                  , out _error_str
                                                  , true
                                                  , m_card.PlayerTracking.HolderName))
      {
        Log.Error("btn_subtract_all_Click: DB_CardCreditRedeem. Total. Error subtracting all credit to card. " + _error_str);

        if (!String.IsNullOrEmpty(_error_str))
        {
          _error_str += "\\r\\n" + "\\r\\n";
        }

        _error_str += Resource.String("STR_CREDIT_CARD_REDEEM_TOTAL_ERROR");

        _error_str = _error_str.Replace("\\r\\n", "\r\n");

        if (ExternalPaymentAndSaleValidationCommon.ExpedMode != EnumExternalPaymentAndSaleValidationMode.MODE_EXPED
            || Misc.ExternalWS.OutputParams.ValidationResult != EnumExternalPaymentAndSaleValidationResult.RESULT_AUTHORIZED)
        {
          frm_message.Show(_error_str
                         , Resource.String("STR_APP_GEN_MSG_ERROR")
                         , MessageBoxButtons.OK
                         , Images.CashierImage.Error
                         , m_parent_form);
        }
        return;
      }

      Misc.WriteLog("[FORM CLOSE] frm_account_redeem (total_redeem): " + FormTitle, Log.Type.Message);
      this.Close();

    } // btn_total_redeem_Click


    private void OnShown(object sender, EventArgs e)
    {
      uc_card_balance.Focus();
    } // Show


    /// <summary>
    /// Check if user can withdraw with credit line debt.
    /// </summary>
    /// <param name="Debt"></param>
    /// <returns></returns>
    private Boolean CreditLine_CheckUserDebt(Currency Debt)
    {
      String _message;

      //No Debt or CreditLine Disabled.
      if (!GeneralParam.GetBoolean("CreditLine", "Enabled", false) || Debt == 0)
      {
        return true;
      }

      _message = Resource.String("STR_CREDIT_LINE_WITH_DEBT_MESSAGE", Debt.ToString());
      _message = _message.Replace("\\r\\n", "\r\n");
      if (!(frm_message.Show(_message, Resource.String("STR_APP_GEN_MSG_WARNING"),
                        MessageBoxButtons.YesNo, MessageBoxIcon.Error, m_parent_form) == DialogResult.OK))
      {
        return false;
      }


      return true;
    } // CreditLine_CheckUserDebt

    #endregion " Private Methods "

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Init and Show the form.
    //
    //  PARAMS :
    //      - INPUT : ParentForm
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public void Show(Form ParentForm)
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_account_redeem", Log.Type.Message);
      m_parent_form = ParentForm;

      this.ShowDialog(ParentForm);
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Dispose yes_no form.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    new public void Dispose()
    {
      form_yes_no.Dispose();
      form_yes_no = null;

      base.Dispose();
    }

    #endregion

    private void btn_prize_payout_Click(object sender, EventArgs e)
    {
      DialogResult _d_result;
      CardData _card;
      string error_str;

      // TODO: Check if current user is an authorized user
      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.PrizePayout,
                                               ProfilePermissions.TypeOperation.RequestPasswd, m_parent_form, out error_str))
      {
        return;
      }

      try
      {
        form_yes_no.Show(m_parent_form);

        if (!CreditLine_CheckUserDebt(uc_card_balance.CreditLineSpent))
        {
          return;
        }

        _card = (CardData)m_card.Clone();

        frm_prize_payout form_prize_payout = new frm_prize_payout(m_card, this.m_check_permissions_to_redeem);
        _d_result = form_prize_payout.Show(this);
        if (_d_result == DialogResult.OK)
        {
          Misc.WriteLog("[FORM CLOSE] frm_account_redeem (prize_payout)", Log.Type.Message);
          this.Close();
        }

        if (_d_result == DialogResult.Cancel)
        {
          m_card = (CardData)_card.Clone();
        }

      }
      finally
      {
        form_yes_no.Hide();
      }
    }

  }
}