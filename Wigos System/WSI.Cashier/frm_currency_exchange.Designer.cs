﻿namespace WSI.Cashier
{
  partial class frm_currency_exchange
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.btn_ok = new WSI.Cashier.Controls.uc_round_button();
      this.btn_cancel = new WSI.Cashier.Controls.uc_round_button();
      this.lbl_foreign_currency = new WSI.Cashier.Controls.uc_label();
      this.lbl_exchange = new WSI.Cashier.Controls.uc_label();
      this.lbl_comission = new WSI.Cashier.Controls.uc_label();
      this.lbl_amount_to_give = new WSI.Cashier.Controls.uc_label();
      this.lbl_exchange_amount = new WSI.Cashier.Controls.uc_label();
      this.lbl_comission_amount = new WSI.Cashier.Controls.uc_label();
      this.lbl_foreign_iso = new WSI.Cashier.Controls.uc_label();
      this.uc_foreign_currency = new WSI.Cashier.Controls.uc_round_textbox();
      this.lbl_amount_to_give_amount = new WSI.Cashier.Controls.uc_label();
      this.lbl_exchange_rate_amount = new WSI.Cashier.Controls.uc_label();
      this.lbl_exchange_rate = new WSI.Cashier.Controls.uc_label();
      this.chk_without_comissions = new WSI.Cashier.Controls.uc_round_button();
      this.uc_payment_method1 = new WSI.Cashier.uc_payment_method();
      this.pnl_data.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.Controls.Add(this.uc_payment_method1);
      this.pnl_data.Controls.Add(this.chk_without_comissions);
      this.pnl_data.Controls.Add(this.lbl_exchange_rate_amount);
      this.pnl_data.Controls.Add(this.lbl_exchange_rate);
      this.pnl_data.Controls.Add(this.lbl_amount_to_give_amount);
      this.pnl_data.Controls.Add(this.uc_foreign_currency);
      this.pnl_data.Controls.Add(this.lbl_foreign_iso);
      this.pnl_data.Controls.Add(this.lbl_comission_amount);
      this.pnl_data.Controls.Add(this.lbl_exchange_amount);
      this.pnl_data.Controls.Add(this.lbl_amount_to_give);
      this.pnl_data.Controls.Add(this.lbl_comission);
      this.pnl_data.Controls.Add(this.lbl_exchange);
      this.pnl_data.Controls.Add(this.lbl_foreign_currency);
      this.pnl_data.Controls.Add(this.btn_ok);
      this.pnl_data.Controls.Add(this.btn_cancel);
      this.pnl_data.Size = new System.Drawing.Size(495, 448);
      // 
      // btn_ok
      // 
      this.btn_ok.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_ok.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_ok.FlatAppearance.BorderSize = 0;
      this.btn_ok.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_ok.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_ok.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_ok.Image = null;
      this.btn_ok.IsSelected = false;
      this.btn_ok.Location = new System.Drawing.Point(327, 375);
      this.btn_ok.Name = "btn_ok";
      this.btn_ok.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_ok.Size = new System.Drawing.Size(155, 60);
      this.btn_ok.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_ok.TabIndex = 8;
      this.btn_ok.Text = "XOK";
      this.btn_ok.UseVisualStyleBackColor = false;
      this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
      // 
      // btn_cancel
      // 
      this.btn_cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_cancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btn_cancel.FlatAppearance.BorderSize = 0;
      this.btn_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_cancel.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_cancel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_cancel.Image = null;
      this.btn_cancel.IsSelected = false;
      this.btn_cancel.Location = new System.Drawing.Point(160, 375);
      this.btn_cancel.Name = "btn_cancel";
      this.btn_cancel.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_cancel.Size = new System.Drawing.Size(155, 60);
      this.btn_cancel.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_cancel.TabIndex = 7;
      this.btn_cancel.Text = "XCANCEL";
      this.btn_cancel.UseVisualStyleBackColor = false;
      this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
      // 
      // lbl_foreign_currency
      // 
      this.lbl_foreign_currency.BackColor = System.Drawing.Color.Transparent;
      this.lbl_foreign_currency.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_foreign_currency.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_foreign_currency.Location = new System.Drawing.Point(12, 186);
      this.lbl_foreign_currency.Name = "lbl_foreign_currency";
      this.lbl_foreign_currency.Size = new System.Drawing.Size(214, 32);
      this.lbl_foreign_currency.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_foreign_currency.TabIndex = 9;
      this.lbl_foreign_currency.Text = "xAmount foreign currency";
      this.lbl_foreign_currency.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_exchange
      // 
      this.lbl_exchange.BackColor = System.Drawing.Color.Transparent;
      this.lbl_exchange.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_exchange.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_exchange.Location = new System.Drawing.Point(99, 239);
      this.lbl_exchange.Name = "lbl_exchange";
      this.lbl_exchange.Size = new System.Drawing.Size(127, 25);
      this.lbl_exchange.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_exchange.TabIndex = 11;
      this.lbl_exchange.Text = "Exchange:";
      this.lbl_exchange.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_comission
      // 
      this.lbl_comission.BackColor = System.Drawing.Color.Transparent;
      this.lbl_comission.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_comission.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_comission.Location = new System.Drawing.Point(120, 274);
      this.lbl_comission.Name = "lbl_comission";
      this.lbl_comission.Size = new System.Drawing.Size(106, 25);
      this.lbl_comission.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_comission.TabIndex = 12;
      this.lbl_comission.Text = "Comission:";
      this.lbl_comission.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_amount_to_give
      // 
      this.lbl_amount_to_give.BackColor = System.Drawing.Color.Transparent;
      this.lbl_amount_to_give.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_amount_to_give.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_amount_to_give.Location = new System.Drawing.Point(12, 321);
      this.lbl_amount_to_give.Name = "lbl_amount_to_give";
      this.lbl_amount_to_give.Size = new System.Drawing.Size(214, 25);
      this.lbl_amount_to_give.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_amount_to_give.TabIndex = 13;
      this.lbl_amount_to_give.Text = "xAmount to give:";
      this.lbl_amount_to_give.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_exchange_amount
      // 
      this.lbl_exchange_amount.BackColor = System.Drawing.Color.Transparent;
      this.lbl_exchange_amount.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_exchange_amount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_exchange_amount.Location = new System.Drawing.Point(227, 239);
      this.lbl_exchange_amount.Name = "lbl_exchange_amount";
      this.lbl_exchange_amount.Size = new System.Drawing.Size(213, 25);
      this.lbl_exchange_amount.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_exchange_amount.TabIndex = 21;
      this.lbl_exchange_amount.Text = "xxxx";
      this.lbl_exchange_amount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_comission_amount
      // 
      this.lbl_comission_amount.BackColor = System.Drawing.Color.Transparent;
      this.lbl_comission_amount.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_comission_amount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_comission_amount.Location = new System.Drawing.Point(227, 274);
      this.lbl_comission_amount.Name = "lbl_comission_amount";
      this.lbl_comission_amount.Size = new System.Drawing.Size(213, 25);
      this.lbl_comission_amount.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_comission_amount.TabIndex = 22;
      this.lbl_comission_amount.Text = "xxxx";
      this.lbl_comission_amount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_foreign_iso
      // 
      this.lbl_foreign_iso.BackColor = System.Drawing.Color.Transparent;
      this.lbl_foreign_iso.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_foreign_iso.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_foreign_iso.Location = new System.Drawing.Point(446, 191);
      this.lbl_foreign_iso.Name = "lbl_foreign_iso";
      this.lbl_foreign_iso.Size = new System.Drawing.Size(43, 25);
      this.lbl_foreign_iso.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_foreign_iso.TabIndex = 23;
      this.lbl_foreign_iso.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // uc_foreign_currency
      // 
      this.uc_foreign_currency.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.uc_foreign_currency.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.uc_foreign_currency.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.uc_foreign_currency.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.uc_foreign_currency.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.uc_foreign_currency.CornerRadius = 5;
      this.uc_foreign_currency.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.uc_foreign_currency.Location = new System.Drawing.Point(237, 177);
      this.uc_foreign_currency.MaxLength = 12;
      this.uc_foreign_currency.Multiline = false;
      this.uc_foreign_currency.Name = "uc_foreign_currency";
      this.uc_foreign_currency.PasswordChar = '\0';
      this.uc_foreign_currency.ReadOnly = false;
      this.uc_foreign_currency.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.uc_foreign_currency.SelectedText = "";
      this.uc_foreign_currency.SelectionLength = 0;
      this.uc_foreign_currency.SelectionStart = 0;
      this.uc_foreign_currency.Size = new System.Drawing.Size(204, 50);
      this.uc_foreign_currency.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC_CUSTOM;
      this.uc_foreign_currency.TabIndex = 25;
      this.uc_foreign_currency.TabStop = false;
      this.uc_foreign_currency.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      this.uc_foreign_currency.UseSystemPasswordChar = false;
      this.uc_foreign_currency.WaterMark = null;
      this.uc_foreign_currency.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      this.uc_foreign_currency.TextChanged += new System.EventHandler(this.uc_foreign_currency_TextChanged);
      this.uc_foreign_currency.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.uc_foreign_currency_KeyPress);
      this.uc_foreign_currency.Enter += new System.EventHandler(this.uc_foreign_currency_Enter);
      this.uc_foreign_currency.Leave += new System.EventHandler(this.uc_foreign_currency_Leave);
      this.uc_foreign_currency.Click += new System.EventHandler(this.uc_foreign_currency_Click);
      this.uc_foreign_currency.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.uc_foreign_currency_PreviewKeyDown);
      // 
      // lbl_amount_to_give_amount
      // 
      this.lbl_amount_to_give_amount.BackColor = System.Drawing.Color.Transparent;
      this.lbl_amount_to_give_amount.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_amount_to_give_amount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_amount_to_give_amount.Location = new System.Drawing.Point(227, 312);
      this.lbl_amount_to_give_amount.Name = "lbl_amount_to_give_amount";
      this.lbl_amount_to_give_amount.Size = new System.Drawing.Size(258, 38);
      this.lbl_amount_to_give_amount.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_24PX_BLACK;
      this.lbl_amount_to_give_amount.TabIndex = 26;
      this.lbl_amount_to_give_amount.Text = "xxxx";
      this.lbl_amount_to_give_amount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_exchange_rate_amount
      // 
      this.lbl_exchange_rate_amount.BackColor = System.Drawing.Color.Transparent;
      this.lbl_exchange_rate_amount.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_exchange_rate_amount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_exchange_rate_amount.Location = new System.Drawing.Point(237, 137);
      this.lbl_exchange_rate_amount.Name = "lbl_exchange_rate_amount";
      this.lbl_exchange_rate_amount.Size = new System.Drawing.Size(255, 25);
      this.lbl_exchange_rate_amount.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_exchange_rate_amount.TabIndex = 28;
      this.lbl_exchange_rate_amount.Text = "xxxx";
      this.lbl_exchange_rate_amount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_exchange_rate
      // 
      this.lbl_exchange_rate.BackColor = System.Drawing.Color.Transparent;
      this.lbl_exchange_rate.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_exchange_rate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_exchange_rate.Location = new System.Drawing.Point(104, 137);
      this.lbl_exchange_rate.Name = "lbl_exchange_rate";
      this.lbl_exchange_rate.Size = new System.Drawing.Size(127, 25);
      this.lbl_exchange_rate.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_exchange_rate.TabIndex = 27;
      this.lbl_exchange_rate.Text = "xExchangeRate:";
      this.lbl_exchange_rate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // chk_without_comissions
      // 
      this.chk_without_comissions.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.chk_without_comissions.FlatAppearance.BorderColor = System.Drawing.SystemColors.Control;
      this.chk_without_comissions.FlatAppearance.BorderSize = 0;
      this.chk_without_comissions.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.chk_without_comissions.Font = new System.Drawing.Font("Open Sans Semibold", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.chk_without_comissions.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.chk_without_comissions.Image = null;
      this.chk_without_comissions.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
      this.chk_without_comissions.IsSelected = false;
      this.chk_without_comissions.Location = new System.Drawing.Point(13, 266);
      this.chk_without_comissions.Margin = new System.Windows.Forms.Padding(0);
      this.chk_without_comissions.Name = "chk_without_comissions";
      this.chk_without_comissions.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.chk_without_comissions.Size = new System.Drawing.Size(112, 42);
      this.chk_without_comissions.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PAYMENT_METHOD_SELECTED;
      this.chk_without_comissions.TabIndex = 30;
      this.chk_without_comissions.Text = "XWITHOUTCOMISSIONS";
      this.chk_without_comissions.UseVisualStyleBackColor = false;
      this.chk_without_comissions.Click += new System.EventHandler(this.chk_without_comissions_Click);
      // 
      // uc_payment_method1
      // 
      this.uc_payment_method1.BtnSelectedCurrencyEnabled = true;
      this.uc_payment_method1.Location = new System.Drawing.Point(55, 21);
      this.uc_payment_method1.Name = "uc_payment_method1";
      this.uc_payment_method1.Size = new System.Drawing.Size(385, 116);
      this.uc_payment_method1.TabIndex = 47;
      this.uc_payment_method1.CurrencyChanged += new WSI.Cashier.uc_payment_method.CurrencyChangedHandler(this.uc_payment_method1_CurrencyChanged);
      // 
      // frm_currency_exchange
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(495, 503);
      this.Name = "frm_currency_exchange";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "frm_currency_exchange";
      this.Activated += new System.EventHandler(this.frm_currency_exchange_Activated_1);
      this.Shown += new System.EventHandler(this.frm_currency_exchange_Shown);
      this.pnl_data.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private Controls.uc_round_button btn_ok;
    private Controls.uc_round_button btn_cancel;
    private Controls.uc_label lbl_foreign_currency;
    private Controls.uc_label lbl_amount_to_give;
    private Controls.uc_label lbl_comission;
    private Controls.uc_label lbl_exchange;
    private Controls.uc_label lbl_comission_amount;
    private Controls.uc_label lbl_exchange_amount;
    private Controls.uc_label lbl_foreign_iso;
    private Controls.uc_round_textbox uc_foreign_currency;
    private Controls.uc_label lbl_amount_to_give_amount;
    private Controls.uc_label lbl_exchange_rate_amount;
    private Controls.uc_label lbl_exchange_rate;
    private Controls.uc_round_button chk_without_comissions;
    private uc_payment_method uc_payment_method1;


  }
}