//------------------------------------------------------------------------------
// Copyright � 2007-2014 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_gt_card_reader.cs
// 
//   DESCRIPTION: Implements the uc_gt_card_reader user control
//                Class: uc_gt_card_reader
//
//        AUTHOR: Jos� Mart�nez
// 
// CREATION DATE: 05-JUN-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 05-JUN-2014 JML     First release.
// 05-FEB-2016 RAB     Bug 8603:Nuevo Dise�o de Cajero - Mesas de Juego - No es posible asignar asientos a jugadores mediante Teclado
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Threading;
using WSI.Common;

namespace WSI.Cashier
{
  public partial class uc_gt_card_reader : UserControl
  {
    #region Class Atributes

    private Int32 tick_last_change = 0;
    private Boolean all_data_is_entered = false;
    private Boolean ignore_additional_tracks = false;

    private frm_container m_parent;

    frm_search_players form_search_players = new frm_search_players();
    AccountWatcher account_watcher;

    public delegate void TrackNumberReadEventHandler(CardData CardData, Boolean IsValid);
    public event TrackNumberReadEventHandler AccountReadWithTrackNumber;

    private const Int32 TRACK_NUMBER_LENGTH = 20;

    private Seat m_seat;
    private Boolean is_kbd_msg_filter_event = false;
    private String received_track_data;

    #endregion  // Class Atributes

    #region Constructor

    /// <summary>
    /// Constructor
    /// </summary>
    public uc_gt_card_reader()
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[UC LOAD] uc_gt_card_reader", Log.Type.Message);

      InitializeComponent();

      InitializeControlResources();

      if (!this.DesignMode)
      {
        tmr_clear.Enabled = true;
        ClearTrackNumber();
        txt_track_number.MaxLength = TRACK_NUMBER_LENGTH;
      }

      btn_player_browse.Click += new EventHandler(btn_button_clicked);
      btn_player_unknown.Click += new EventHandler(btn_button_clicked);

    } // uc_gt_card_reader

    #endregion // Constructor

    #region Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE: Checks is the input is completely numeric. 
    // 
    //  PARAMS:
    //      - INPUT: TrackNumber
    //
    //      - OUTPUT:
    //
    // RETURNS: True if ok, false otherwise
    // 
    //   NOTES:
    //  
    private Boolean PartialTrackNumberIsValid(String TrackNumber)
    {
      Byte[] chars;

      chars = null;

      try
      {
        chars = Encoding.ASCII.GetBytes(TrackNumber);
        foreach (byte b in chars)
        {
          if (b < '0')
          {
            return false;
          }
          if (b > '9')
          {
            return false;
          }
        }
      }
      finally
      {
        chars = null;
      }

      return true;
    } // PartialTrackNumberIsValid

    //------------------------------------------------------------------------------
    // PURPOSE: Track number checking. 
    // 
    //  PARAMS:
    //      - INPUT: TrackNumber
    //
    //      - OUTPUT:
    //
    // RETURNS: True if ok, false otherwise
    // 
    //   NOTES:
    //  
    private Boolean TrackNumberIsValid(String TrackNumber)
    {
      if (TrackNumber.Length == TRACK_NUMBER_LENGTH)
      {
        if (PartialTrackNumberIsValid(TrackNumber))
        {
          String _internal_track;
          int _card_type;

          if (WSI.Common.CardNumber.TrackDataToInternal(TrackNumber, out _internal_track, out _card_type))
          {
            if (_card_type == 0
                || _card_type == 1)
            {
              return true;
            }
          }
        }
      }
      return false;
    } // TrackNumberIsValid

    private void AccountReceivedEvent(KbdMsgEvent e, Object data)
    {
      Barcode _barcode;
      KeyPressEventArgs _event_args;
      String _error_str = "";

      if (e == KbdMsgEvent.EventBarcode)
      {
        // RAB 05-FEB-2016
        if (ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.PlayerTrackingExecute,
                                   ProfilePermissions.TypeOperation.OnlyCheck,
                                   m_parent,
                                   out _error_str))
        {
          _barcode = (Barcode)data;
          received_track_data = _barcode.ExternalCode;

          is_kbd_msg_filter_event = true;
          txt_track_number_TextChanged(this, new EventArgs());
          _event_args = new KeyPressEventArgs('\r');
          txt_track_number_KeyPress(this, _event_args);
        }
        else
        {
          frm_message.Show(Resource.String("STR_FRM_USER_LOGIN_ERROR_LOGIN_PERMS"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this.ParentForm);
        }
      }
    }

    #endregion // Private Methods

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE: Initialize control resources. (NLS string, images, etc.)
    // 
    //  PARAMS:
    //      - INPUT: 
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    // 
    //   NOTES:
    //  
    public void InitializeControlResources()
    {
      // NLS Strings
      //      - Title
      //      - Labels
      //      - Buttons
      //      - Resources

      //      - Title

      //   - Labels
      lbl_swipe_card.Text = Resource.String("STR_UC_CARD_READER_SWIPE_CARD");

      //      - Buttons
      btn_player_unknown.Text = Resource.String("STR_CASHIER_GAMING_TABLE_PLAYER_UNKNOWN_BUTTON");

      //      - Resources
      this.form_search_players.InitializeControlResources();

    }  // InitializeControlResources

    //------------------------------------------------------------------------------
    // PURPOSE: Initialize controls
    // 
    //  PARAMS:
    //      - INPUT: TrackNumber
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    // 
    //   NOTES:
    //  
    public void InitControls(frm_container Parent, Seat Seat)
    {
      m_parent = Parent;
      m_seat = Seat;

      ClearCardData();

      account_watcher = new AccountWatcher();
      account_watcher.Start();
      account_watcher.AccountChangedEvent += new AccountWatcher.AccountChangedHandler(account_watcher_AccountChangedEvent);

      InitializeControlResources();
    } // InitControls

    //------------------------------------------------------------------------------
    // PURPOSE: Set initial focus. 
    // 
    //  PARAMS:
    //      - INPUT: 
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //   NOTES:
    //  
    public void InitFocus()
    {
      txt_track_number.Focus();
    } // InitFocus

    //------------------------------------------------------------------------------
    // PURPOSE: Clear track number
    // 
    //  PARAMS:
    //      - INPUT: 
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    // 
    //   NOTES:
    //  
    public void ClearCardData()
    {
      ClearTrackNumber();
    }  // ClearCardData

    //------------------------------------------------------------------------------
    // PURPOSE: Clear input for track number actions.
    // 
    //  PARAMS:
    //      - INPUT: 
    //
    //      - OUTPUT:
    //
    // RETURNS: True if ok, false otherwise
    // 
    //   NOTES:
    //  
    public void ClearTrackNumber()
    {
      txt_track_number.Text = "";
      tick_last_change = 0;

      lbl_invalid_card.Text = "";
      lbl_invalid_card.Visible = false;
      all_data_is_entered = false;
      ignore_additional_tracks = false;

    } // ClearTrackNumber

    #endregion  // Public Methods

    #region Events

    private void btn_button_clicked(object sender, EventArgs e)
    {
      Boolean _is_track_valid;

      if (!txt_track_number.TextBox_Focused)
      {
        txt_track_number.Focus();
      }

      try
      {

        if (sender.Equals(btn_player_browse))
        {
          _is_track_valid = btn_player_browse_Click(sender, e);

          return;
        }
        if (sender.Equals(btn_player_unknown))
        {
          _is_track_valid = btn_player_unknown_Click(sender, e);

          return;
        }

      }
      finally
      {
        if (!txt_track_number.TextBox_Focused)
        {
          txt_track_number.Focus();
        }

        GC.Collect();
        ThreadPool.QueueUserWorkItem(delegate
        {
          GC.WaitForPendingFinalizers();
          GC.Collect();
        });

      }
    } // btn_button_clicked

    private Boolean btn_player_browse_Click(object sender, EventArgs e)
    {
      Int64 _selected_account_id;
      Boolean _is_track_valid = true;
      Cursor _previous_cursor;
      String _error_str;

      _selected_account_id = -1;

      // Check if current user is an authorized user
      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.AccountSearchByName,
                                               ProfilePermissions.TypeOperation.RequestPasswd,
                                               this.ParentForm,
                                               out _error_str))
      {
        return false;
      }

      _previous_cursor = this.Cursor;

      try
      {
        this.Cursor = Cursors.WaitCursor;

        _selected_account_id = form_search_players.Show(this.ParentForm);

        if (_selected_account_id < 0)
        {
          return false;
        }

        // Return CardData to caller
        CardData _card_data = new CardData();

        if (!CardData.DB_CardGetAllData(_selected_account_id, _card_data, false))
        {
          this.ClearCardData();
          Log.Error("LoadCardByAccountId. DB_GetCardAllData: Error reading card.");
          return false;
        }

        if (_card_data.Blocked)
        {
          lbl_invalid_card.Text = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_MSG_005");
          lbl_invalid_card.Visible = true;

          tmr_clear.Enabled = true;
          return false;
        }
        if (_card_data.TrackData.Length == 0)
        {
          lbl_invalid_card.Text = Resource.String("STR_UC_CARD_READER_INVALID_CARD");
          lbl_invalid_card.Visible = !PartialTrackNumberIsValid(_card_data.TrackData);

          tmr_clear.Enabled = true;
          return false;
        }

        AccountReadWithTrackNumber(_card_data, _is_track_valid);

        return _is_track_valid;
      }
      finally
      {
        this.Cursor = _previous_cursor;
      }
    } // btn_player_browse_Click

    private Boolean btn_player_unknown_Click(object sender, EventArgs e)
    {
      Boolean _is_track_valid = true;
      Cursor _previous_cursor;

      _previous_cursor = this.Cursor;

      try
      {
        this.Cursor = Cursors.WaitCursor;

        // Return CardData to caller
        AccountReadWithTrackNumber(new CardData(), _is_track_valid);

        return _is_track_valid;
      }
      finally
      {
        this.Cursor = _previous_cursor;
      }
    } // btn_player_unknown_Click

    private void account_watcher_AccountChangedEvent()
    {
      if (this.InvokeRequired)
      {
        Delegate _method;

        _method = null;

        try
        {
          _method = new AccountWatcher.AccountChangedHandler(account_watcher_AccountChangedEvent);
          if (_method != null)
          {
            this.BeginInvoke(_method);
          }
          else
          {
            Log.Error("Account changed: nobody taking care of the event, the method is null.");
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }
        return;
      }

      ClearTrackNumber();
    } // account_watcher_AccountChangedEvent

    private void pictureBox1_Click(object sender, EventArgs e)
    {
      ClearTrackNumber();
      txt_track_number.Focus();
    } // pictureBox1_Click

    private void txt_track_number_TextChanged(object sender, EventArgs e)
    {
      String track_number;
      //if card readed meanwhile disconnected message, do nothing
      if (WSI.Common.WGDB.ConnectionState != ConnectionState.Open
          && WSI.Common.WGDB.ConnectionState != ConnectionState.Executing
          && WSI.Common.WGDB.ConnectionState != ConnectionState.Fetching)
      {
        ClearTrackNumber();
        return;
      }

      // Variables initialization
      tick_last_change = 0;
      if (is_kbd_msg_filter_event)
      {
        track_number = received_track_data;
      }
      else
      {
        track_number = txt_track_number.Text;
      }
      all_data_is_entered = (track_number.Length >= TRACK_NUMBER_LENGTH);

      if (track_number.Length > 0)
      {
        lbl_invalid_card.Text = Resource.String("STR_UC_CARD_READER_INVALID_CARD");
        lbl_invalid_card.Visible = !PartialTrackNumberIsValid(track_number);
      }
    } // txt_track_number_TextChanged

    private void txt_track_number_KeyPress(object sender, KeyPressEventArgs e)
    {
      String track_number;
      Boolean is_valid;
      tick_last_change = 0;
      CardData _card_data;

      if (is_kbd_msg_filter_event)
      {
        track_number = received_track_data;
        is_kbd_msg_filter_event = false;
      }
      else
      {
        track_number = txt_track_number.Text;
      }
      switch (e.KeyChar)
      {
        case '%':
          // Ignore track start/end markers
          ignore_additional_tracks = false;
          e.KeyChar = '\0';
          break;

        case ';':
          // Ignore track start/end markers
          ignore_additional_tracks = true;
          e.KeyChar = '\0';
          break;

        case '?':
          // Ignore track start/end markers
          e.KeyChar = '\0';
          break;

        case '\r':
          if (!ignore_additional_tracks)
          {
            if (all_data_is_entered)
            {
              if (track_number.Length >= TRACK_NUMBER_LENGTH)
              {
                is_valid = TrackNumberIsValid(track_number);

                if (is_valid)
                {
                  tmr_clear.Enabled = false;


                  try
                  {
                    WSI.Common.Users.SetLastAction("ReadCard", "");
                    _card_data = new CardData();
                    //Get Data by TrackNumber
                    CardData.DB_CardGetAllData(track_number, _card_data, false);
                    if (!CardData.CheckTrackdataSiteId(new ExternalTrackData(track_number)))
                    {
                      Log.Error("LoadCardByAccountId. Card: " + _card_data.TrackData + " doesn�t belong to this site.");
                      frm_message.Show(Resource.String("STR_CARD_SITE_ID_ERROR"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK,
                                          MessageBoxIcon.Warning, this.ParentForm);
                      is_valid = false;
                    }
                    else if (_card_data.AccountId == 0)
                    {
                      lbl_invalid_card.Text = Resource.String("STR_UC_CARD_USER_MSG_CARD_NOT_EXIST_GAMING_TABLE", "");
                      lbl_invalid_card.Visible = true;

                      tmr_clear.Enabled = true;
                      return;
                    }
                    else if (_card_data.AccountId > 0)
                    {
                      if (_card_data.Blocked)
                      {
                        lbl_invalid_card.Text = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_MSG_005");
                        lbl_invalid_card.Visible = true;

                        tmr_clear.Enabled = true;
                        return;
                      }

                      AccountReadWithTrackNumber(_card_data, is_valid);
                    }
                  }
                  catch
                  {
                    is_valid = false;
                  }

                  tmr_clear.Enabled = true;
                }

                if (!is_valid)
                {
                  txt_track_number.SelectAll();

                  lbl_invalid_card.Text = Resource.String("STR_UC_CARD_READER_INVALID_CARD");
                  lbl_invalid_card.Visible = true;

                }
                else
                {
                  ClearTrackNumber();

                }
              }
            }
          }
          else
          {
            // Allow processing additional keystrokes
            ignore_additional_tracks = false;
          }

          e.Handled = true;
          break;

        default:
          if (ignore_additional_tracks)
          {
            // All keystrokes must be ignored until a valid sequence starts
            e.KeyChar = '\0';
          }
          break;
      } // switch
    } // txt_track_number_KeyPress

    private void tmr_clear_Tick(object sender, EventArgs e)
    {
      tick_last_change += 1;
      if (tick_last_change > 20) // 5 seconds
      {
        ClearTrackNumber();
        tick_last_change = 0;
      }
    } // tmr_clear_Tick

    private void OnAccountChanged(CardData CardData, Boolean IsValid)
    {
      if (AccountReadWithTrackNumber != null)
      {
        AccountReadWithTrackNumber(CardData, IsValid);
      }
    } // OnAccountChanged

    private void lbl_invalid_card_Click(object sender, EventArgs e)
    {

    }

    private void uc_gt_card_reader_VisibleChanged(object sender, EventArgs e)
    {
      if (this.Visible)
      {
        KeyboardMessageFilter.RegisterHandler(this, BarcodeType.Account, AccountReceivedEvent);
      }
    }

    #endregion  // Events

  } // class uc_gt_card_reader
}

