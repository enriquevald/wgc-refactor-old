//------------------------------------------------------------------------------
// Copyright � 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_amount_input.cs
// 
//   DESCRIPTION: Implements the following classes:
//                1. frm_amount_input
//
//        AUTHOR: AAC
// 
// CREATION DATE: 07-AUG-2007
//  
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 07-AUG-2007 ACC     First release.
// 23-NOV-2011 RCI     Request PIN if configured.
// 18-JAN-2012 ACC     Calculate spent per provider.
// 20-FEB-2012 RCI     PreviewVoucher for CashIn takes in account the new NotRedeemable2 (Cover Coupon) amount.
// 08-JUN-2012 SSC & RCI      In UpdatePromoInputValues() control m_selected_promo_data can be null (due to change it from struct to class).
// 06-AUG-2012 MPO     Distinguish the types of credit of promotion.
// 07-AUG-2012 JCM     Add MB Sales Limit Edit.
// 13-AUG-2012 MPO     New feature: 
//                          * Points reward .
//                          * Improved interface for selection of promotions (button expand/collapse).
// 18-FEB-2013 ICS & RCI    Since 10-AUG-2012, it is possible to cash in even if the promotion has WonLock.
// 18-MAR-2013 ICS     Fixed Bug #646: Check if it has not exceeded the available time to cancel the promotion.
// 02-APR-2013 RBG     Fixed Bug #294: Show popup if recharge exceeds limit.
// 14-MAY-2013 ACM     Add withdrawal permission. Shows pop-up if withdrawal exceeds limit.
// 02-JUL-2013 DLL     Add Currency Exchange support.
// 03-JUL-2013 DLL     Rename dgv_promotions to dgv_common and gb_promotions to gb_common.
// 04-JUL-2013 DLL     Add currency exchange CashIn & CloseCash.
// 05-AUG-2013 ICS     Added payment method group box.
// 09-AUG-2013 ICS & DLL    Fixed Bug WIG-105: Bad formatted data in PreviewVoucher when Bank Card is enabled.
// 21-AUG-2013 DLL     WIG-136: In CloseCash with currency. When currency balance and collected are zero skip message.
// 22-AUG-2013 DLL     In CloseCash & FillOut with currency take currencies with balance.
// 27-AUG-2013 RMS     Removed unnecessary parameters in CashDeskClose functions ( SessionId and OperationId ).
// 28-AUG-2013 ACM     Fixed Bug WIG-171: Permission to add non redeemable credit.
// 03-SEP-2013 DLL     Fixed Bug WIG-179: Deposit ticket wrong with currencies.
// 04-SEP-2013 DLL     Fixed Bug WIG-187: Delete message when national currency balance is zero.
// 06-SEP-2013 ACM     Implements new GP RequestPin.DisabledForAnonymous.
// 10-SEP-2013 DLL     Add currency Check support.
// 20-SEP-2013 DLL     Added Bank Card and Check in Withdraws and CloseCash operations.
// 27-SEP-2013 NMR     Changing Input amount for TITO operations
// 01-NOV-2013 DLL     Fixed Bug WIG-245: Force inform for all currencies.
// 04-OCT-2013 DDM & DHA    Fixed Bug #WIG-262: Error cancelling UNR promotions.
// 28-OCT-2013 QMP & ICS    Fixed Bug #WIG-299: "Player gets" column appears blank for certain promotions.
// 08-NOV-2013 JAB     Added cage functionality.
// 12-NOV-2013 DLL     Fixed Bug #WIG-404: unify messages when incorrect amount.
// 26-NOV-2013 JAB     Fixed Bug #WIG-429: Labels "V01" & "V02" are showed in "CardIn" & "CancelPromotion" operations.
// 27-NOV-2013 JAB     Fixed Bug #WIG-457: Show currencies with cash on cashier although are disabled.
// 09-DEC-2013 JAB     Fixed Bug #WIG-470: Enabled "btn_intro" in OpenCash form.
// 13-DEC-2013 JAB     Added tips in CHIPs.
// 14-DEC-2013 JCA     Added FillIn when CAGE Movement Sent by Terminal.
// 31-DEC-2013 DLL     Fixed Bug #WIG-498: Incorrect collected values in CloseCash (always close with zero)
// 31-DEC-2013 DLL     Fixed Bug #WIG-500
// 02-JAN-2014 ICS     Fixed Bug #WIGOSTITO-944
// 08-JAN-2014 JAB     Fixed Bug #WIG-507
// 24-JAN-2014 JCA     Added Image in dgv_common for Chips.
// 24-JAN-2014 RRR     Let close when chip's StockControl is false after a determined number of questions
// 29-JAN-2014 RCI     Added CashIn tax support.
// 29-JAN-2014 LJM     Recharges payed with credit card or check, should not be accounted for AML
// 05-FEB-2014 JAB     Fixed Bug #WIG-593: Wrong values inserted after click in clear button.
//                                         "m_currencies_amount" variable  is not updated.
// 17-FEB-2014 JFC     Zero amount recharge are permitted now.
// 18-FEB-2014 JCA     Show Tickets TITO row for FillOut and CloseCash.
// 24-FEB-2014 JAB     Fixed Bug #WIG-667: Recharges payed with credit card or check, should not be accounted for AML.
// 26-FEB-2014 XIT     Deleted SmartParams uses
// 27-FEB-2014 JBC     Fixed Bug #WIG-680: Variable added in CardAdd_ResetVoucher
// 20-MAR-2014 QMP     Fixed Bug #WIG-757: Cashier freezes when closing cash session with credit card or check recharges.
// 17-MAR-2014 JRM     Added new functionality. Description contained in RequestPinCodeDUT.docx DUT
// 24-MAR-2014 CCG     Fixed Bug #WIG-759: When Cash Cage is activated, the exchange amount isn't calculated properly.
// 24-MAR-2014 FBA     Fixed Bug #WIG-761 
// 27-MAR-2014 FBA     Fixed Bug #WIG-774 
// 15-APR-2014 DLL     Fixed Bug #WIG-826: dont't show quantity button when cancel promotion
// 16-APR-2014 JCA     Fixed Bug #WIG-827: show cage request cancel button in "corte de caja"
// 16-APR-2014 ICS, MPO & RCI    Fixed Bug WIG-830: Not enough available storage exception.
// 17-APR-2014 ICS     Fixed Bug WIG-841: Object reference exception
// 22-APR-2014 ICS     Fixed Bug #WIGOSTITO-1206: Order payment: the pay amount is incorrect when the tickets have different types
// 24-APR-2014 CCG     Fixed Bug #WIG-759: Show currency exchange for total amount, not for each denomination.
// 25-APR-2014 CCG     Fixed Bug #WIG-856: Unhandled exception when change the currency. 
// 22-APR-2014 DLL     Added new functionality for print specific voucher for the Dealer
// 12-MAY-2014 CCG     Fixed Bug #WIG-916: Set the default visibility of label lbl_cage_currency_total as not visible.
// 16-MAY-2014 JAB     Added new functionality: Cage auto mode.
// 27-MAY-2014 DRV     Fixed Bug #WIGOSTITO-1228: Changed the title of form in Creation ticket mode.
// 30-MAY-2014 DLL     Fixed Bug #WIG-968: When CardAdd don't check antimoney laundering 
// 05-JUN-2014 DLL     Fixed Bug #WIG-1017: When ShowDenominations is disabled don't check chips stock control
// 05-JUN-2014 HBB & DLL     Fixed Bug #WIG-1019: Buttons visibility are not correct.
// 06-JUN-2014 JAB     Added two decimal characters in denominations fields.
// 19-JUN-2014 MPO     The label lbl_cage_currency_total shouldn't show it when expand in mode cardadd
// 20-JUN-2014 DLL     Fixed Bug WIG-1041: Unhandled exception when no denomination is selected.
// 26-JUN-2014 LEM     Fixed Bug WIG-1052: CashDraw in TITO Mode: Always participates, even selecting confirmation option NO.
// 08-JUL-2014 AMF     Personal Card Replacement
// 11-JUL-2014 JMM     Fixed Bug WIG-1069: Is not possible to sell chips using decimal amounts
// 17-JUL-2014 DRV     Fixed Bug WIG-1091: Error on Chips Sale: The chips amount delivered mismatch the cashed amount 
// 28-JUL-2014 SGB     Fixed Bug WIG-1122: Button Ok in mode disable, in different situation, not same color
// 28-JUL-2014 SGB     Fixed Bug WIG-1079: Is possible take a import over to cage for maximum permission
// 29-JUL-2014 DLL     Chips are moved to a separate table
// 30-JUL-2014 SGB     Fixed Bug WIGOSTITO-1247: Permission to add create ticket in btn_ok and delete to uc_ticket_create
// 31-JUL-2014 SGB     Fixed Bug WIG-1136: Can't make sell chips register in cashless mode
// 08-AGO-2014 SGB     Fixed Bug WIG-1077: Show only one time message error to exceed the maximum order to cage. Use m_show_msg for this bug.
// 08-AUG-2014 RMS     Added supoort for Specific Bank Card Types
// 27-AUG-2014 DRV     Fixed Bug WIG-1211: Chips Sale/Purchase doesn't work with decimals
// 28-AUG-2014 LEM & DLL     Fixed Bug WIG-1218: Cage stock mismatch with tips
// 02-SEP-2014 FJC     Fixed Bug WIG-1173: Change closing messages from CASH CLOSING and GAMBLING TABLE CLOSING.
// 02-SEP-2014 MPO     Fixed Bug WIG-1226:
// 29-SEP-2014 RRR     Don't check stock if Meters Enabled
// 03-OCT-2014 SGB     Modified call to VoucherMBCardDepositCash for new footer voucher
// 07-OCT-2014 SGB     Modified call to VoucherCashDeskOpen for new footer voucher
// 09-OCT-2014 SGB     Fixed Bug WIG-1476: Check if input amount is bigger than cashier current balance for chips sale or purchase amount input.
// 17-OCT-2014 SMN & LEM     Fixed Bug WIG-1520: Wrong cash close over/short cashier movement when MXN balance = 0.
// 28-OCT-2014 MPO     WIG-1582/WIG-1581 When Ticket is redeemed with payment order doesn't show it and "resultado de caja" it's wrong
// 13-NOV-2014 LRS     Add currency type functionality
// 24-NOV-2014 MPO & OPC     LOGRAND CON02: Added IVA calculated (when needed) and input params class for cashin.
// 01-DEC-2014 DLL     Fixed Bug WIG-1778: If general paramas GamingTables->Cashier.Mode is true, don't check chips balance
// 10-DEC-2014 DRV    Decimal currency formatting: DUT 201411 - Tito Chile - Currency formatting without decimals
// 11-DEC-2014 OPC    Fixed Bug WIG-1823: Show only chips denomination allowed.
// 11-DEC-2014 JMV    DUT - WIGOS 201412: Logrand - Bancos m�viles, Dep�sitos. 
// 31-DEC-2014 MPO    WIG-1900: Error when preview voucher cashout with tickets TITO
// 09-FEB-2015 YNM    WIG-1820: Unable to print ticket in "corte de caja"
// 12-FEB-2015 YNM    WIG-1887: Fixed error in Register Sell of Chips. The card's payment was working incorrectly.
// 17-FEB-2015 HBB    WIG-2071: Permission "Accounts: Add Redeemable Credit" is been using incorrectly when a recharge with $0 is made
// 23-APR-2015 YNM    Fixed Bug WIG-2240: Payment Order + Chips Buy take cashier taxes 
// 25-JUN-2015 ANM    Automatically appears currency selection if specific GP is activated
// 30-JUN-2015 DHA    Added card payment method
// 03-AUG-2015 ETP    Fixed BUG 3496 Use of common header for card replacement.
// 01-OCT-2015 FAV    Fixed BUG 4795: Check max allowed in a chips sale.
// 01-OCT-2015 ETP    Backlog Item 4893 Add new PLD control
// 22-OCT-2015 SDS    Add promotion by played - Backlog Item 3856
// 11-NOV-2015 FOS    Product Backlog Item: 3709 Payment tickets & handpays
// 23-NOV-2015 FOS    Bug 6771:Payment tickets & handpays
// 25-NOV-2015  FOS    Product Backlog Item: 3709 Payment tickets & handpays, applied new design
// 26-NOV-2015 MPO    Fixed bug 7053: Gaming table exception 
// 27-NOV-2015 YNM    Fixed Bug 7053:Gaming Table: unhandled exception on click Buy IN and register chips sale at table
// 01-DEC-2015 DLL    Fixed Bug TFS-7089: Don't show payment mode selection
// 01-DEC-2015 FOS    Fixed Bug 7205: Protected taxWaiverMode and renamed function ApplyTaxCollection to EnableTitoTaxWaiver
// 02-DEC-2015 FOS    Fixed Bug 7208: Incorrect Tax Value on Handpays
// 26-NOV-2015 MPO    Fixed bug 7053: Gaming table exception 
// 27-NOV-2015 YNM    Fixed Bug 7053:Gaming Table: unhandled exception on click Buy IN and register chips sale at table
// 01-DEC-2015 DLL    Fixed Bug TFS-7089: Don't show payment mode selection
// 07-JAN-2016 DLL    Hide Promotions by played. Not implemented
// 29-JAN-2016 DHA    Bug 8780:Cajero: en Registro de Venta, el apartado Total est� solapado con el apartado de Tokens
// 03-FEB-2016 FOS    Bug 8924:Floor Dual Currency: Promotions grouped by provider and iso_code
// 01-MAR-2016 DHA    Product Backlog Item 10083: Winpot - Provisioning Tax
// 01-MAR-2016 JML    Product Backlog Item 10084: Winpot - Provisioning Tax: Cashdesk draw
// 07-MAR-2016 LTC    Product Backlog Item 10227:TITA: TITO ticket exchange for gambling chips
// 14-ABR-2016 LTC    Product Backlog Item 11295: Codere - Custody Tax: Implement tax
// 09-MAR-2016 FOS    Bug 10137-10459:Design errors when depositing with mobile bank
// 03-FOS-2016 FOS    Bug 8924:Floor Dual Currency: Promotions grouped by provider and iso_code
// 11-MAR-2016 DDS    Bug 10598: ShowDenominations value was not setted
// 24-MAR-2016 RAB    Bug 10745: Reception: with the GP requiereNewCard to "0" when assigning a card client is counted as "replacement"
// 04-APR-2016 DHA    Product Backlog Item 9756: added chips (RE, NR and Color)
// 13-APR-2016 DHA    Product Backlog Item 9950: added chips operation (sales and purchases)
// 13-APR-2016 RAB    Product Backlog Item 10855: Tables (Phase 1): Currency/chip Selector.
// 21-APR-2016 FOS    Fixed Bug 11698: Jackpot by level
// 05-MAY-2016 RAB    PBI 12736: Review 23: Changes and improvements (cashier operations in gaming tables)
// 09-JUN-2016 RAB    PBI 11755: Tables (Phase 1): Correction errors as a result of the backward compatibility of the chips group
// 09-JUN-2016 DHA    Product Backlog Item 13402 & 9853: Rolling & Fixed closing stock
// 20-JUN-2016 ETP    Fixed Bug : MultiCurrencyExchange: Added flag for national currency.
// 21-JUN-2016 RAB    PBI 14486: Add new types on VoucherTypes enum
// 21-JUN-2016 RAB    PBI 14486: Add new types on VoucherTypes enum
// 28-JUN-2016 RAB    PBI 14524: GamingTables (Phase 4): Management from deposit and withdrawals - Multicurrency in chips.
// 05-JUL-2016 ETP    PBI 15060: Cambio de multidivisa: Identificar al cliente en cualquier operaci�n con divisas
// 06-JUL-2016 DHA    Product Backlog Item 15064: multicurrency chips sale/purchase
// 13-JUL-2016 EOR    Product Backlog Item 14984: TPV Televisa: Limit of recharge/cash advance in card bank
// 20-JUL-2016 RGR    Product Backlog Item 15318: Table-21: Sales of chips A and B now
// 05-AGO-2016 LTC    Product Backlog Item 14990:TPV Televisa: Cashier Withdrawal
// 05-AGO-2016 LTC    Product Backlog Item 16198:TPV Televisa: Closing Cash
// 27-JUL-2016 EOR    Product Backlog Item 15248: TPV Televisa: Cashier, Vouchers and Tickets
// 02-AUG-2016 EOR    Product Backlog Item 16149: Codere - Tax Custody: Add tax custody concept in ticket recharge
// 11-AUG-2016 RAB    Product Backlog Item 15910: GamingTables (Phase 5): Deposits, applications and withdrawals filter rows with values
// 18-AUG-2016 RAB    Product Backlog Item 15914: Gaming tables (Phase 5): Order summary box and voucher currency
// 23-AUG-2016 LTC    Product Backlog Item 16742:TPV Televisa: Reconciliation modification of cashier vouchers
// 24-AUG-2016 LTC    Bug 16987:Cashier: Unhandled exception while closing cashier after an entry operation
// 28-JUN-2016 DHA    Product Backlog Item 16766: added check and bank card on gaming tables collections
// 02-AGO-2016 ESE    Product Backlog Item 15248:TPV Televisa: Hide voucher recharge
// 06-SEP-2016 LTC    Bug 17354: Wrong information is shown in cashier resume about vouchers
// 07-SEP-2016 EOR    Product Backlog Item 15335: TPV Televisa: Cash Summary
// 15-SEP-2016 FOS    Fixed Bug 17680: Exception when close cash.
// 22-SEP-2016 LTC    Product Backlog Item 17461: Gamingtables 46 - Cash desk draw for gamblign tables: Chips purchase
// 27-SEP-2016 ETP    Fixed Bug 18219: Exception when attempt to recharge with empty or zero value
// 27-SEP-2016 ESE    Bug 17833: Televisa voucher, status cash
// 29-SEP-2016 DHA    Fixed Bug 16759: exception selling chips in TITO
// 07-OCT-2016 LTC    PBI 17964:Televisa - Cashdesk draw (Participation prize) - Configuration
// 10-OCT-2016 LTC    PBI 17960: Televisa - Draw Cash for gaming tables - Configuration
// 10-OCT-2016 LTC    Bug 18843:Cajero - Cuentas: Reintegro de pago de premio en "Efectivo en cuenta" muestra un error y no permite la operaci�n
// 10-OCT-2016 JML    Bug 18840: Improvements opening and closing gaming tables 
// 10-OCT-2016 ETP    Bug 18959:Televisa: Show ok message when vouchers are correctly conciliated
// 14-OCT-2016 ETP    Fixed Bug 18350: Log error is showed when opening with pending deposit
// 07-OCT-2016 LTC    PBItem 17964:Televisa - Cashdesk draw (Participation prize) - Configuration
// 31-OCT-2016 JML    Fixed Bug 19865: Gamming tables stock control - not displayed in the cashier
// 31-OCT-2016 JML    Fixed Bug 19866: Chips shange control - not displayed in the cashier
// 27-OCT-2016 ATB    Bug 19283:Cajero: Funcionalidad incorrecta al realizar un retiro
// 03-NOV-2016 ATB    Bug 20026:Cajero: No se muestra automaticamente la ventana "Selecci�n de devisa" al presionar el bot�n "Recargar" y teniendo 4 divisas activas
// 08-NOV-2016 JMV    Bug 20161:Error in cashier whithdrawal with gp BankCard.Conciliate = 1
// 14-NOV-2016 ETP    Fixed Bug 20285: Exception when cash session is already closed.
// 15/NOV/2016 ETP    PBI 20630: Mico2 Belice - Handpay payment.
// 16-NOV-2016 DHA    Fixed Bug 20678: error closing gaming table with "Fixed Bank", stock sleep on table and Not auto cage mode
// 17-NOV-2016 ATB    Bug 19275:Televisa: the closing cash voucher does not match with Televisa's requirements
// 05-DEC-2016 XGJ    Bug 21174:Cajero ==> Cerrar Caja: El bot�n de decimales no se activa / desactiva de forma correcta.
// 09-DEC-2016 JML    Fixed Bug 5345: Sales of chips with redeemable promotions not possible
// 14-DEC-2016 JML    Fixed Bug 5345: Sales of chips with redeemable promotions not possible. PBI 21447 The voucher has an incorrect amount
// 16-DEC-2016 DHA    Fixed Bug 5531: gaming tables avoid to do a closing or credits with tickets TITO
// 19-DEC-2016 DHA    Bug 21760: currencies are not sorted by type. Error sorting by denomination
// 19-DEC-2016 ETP    Bug 21793: Incorrect client data needed.
// 22-DEC-2016 DHA    Bug 21958: chips sale no apply comissions to bank card and check
// 27-DEC-2016 JCA    Bug 22031: Compra de fichas con tarjeta bancaria puede realizarse aunque el cliente no se encuentre en la sala - Mallorca 
// 10-JAN-2017 RAB    Bug 22352: Not can close a GamingTable
// 11-JAN-2017 ATB    Bug 20508: Cashier: Malfunction after doing a second voucher and cash fill out
// 16/JAN/2017 ETP    Fixed Bug 22747: Multivisa: needed valid visid.
// 18-JAN-2017 JML    Fixed Bug 22699: Cashier: can not sell chips to a gamming table
// 27-JAN-2017 JML    Fixed Bug 23692: Cashless - Sale of chips: error in integrated mode with promotion
// 30-JAN-2017 JML    Fixed Bug 23860: Cashier: Closing table: The error message is repeated several times
// 16-FEB-2017 DPC    Bug 24589:Pago de tarjeta: se tiene en cuenta el impuesto de la empresa B
// 17-FEB-2017 DPC    Bug 24752:Televisa - Cajero - Vista previa del t�cket de cr�ditos del cliente: valor incorrecto en el concepto del cup�n promocional
// 01-MAR-2017 ETP    Fixed Bug 25225: WigosGUI: Se estan considerando las monedas como billetes en la recaudaci�n si el cajero esta en ingles.
// 09-MAR-2017 FGB    PBI 25269: Third TAX - Cash session summary
// 05-APR-2017 JML    Bug 26568:Custodia - add new general param to configure tax custody for gaming tables operations
// 18-APR-2017 RGR    Bug 26575:Fallo en pantalla Deposito en Bancos M�viles desde cajero
// 21-MAR-2017 DPC    PBI 25788:CreditLine - Get a Marker
// 28-MAR-2017 MS     Added change for Creditline Voucher Preview Spent Amount
// 15-MAY-2017 EOR    Fixed Bug 27446: Mobile Bank. The change limit screen has an incorrect format
// 25-MAY-2017 DHA    Bug 27679:[Ticket #4870] Duplicate recharges
// 26-MAY-2017 ETP    Bug 26134: Shows search account form with card readed
// 30-MAY-2017 JML    PBI 27486: WIGOS-1227 Win / loss - Introduce the WinLoss - EDITION SCREEN - ACCUMULATED
// 31-MAY-2017 JML    PBI 27810 - WIGOS-2423 Win - loss - Change request
// 15-JUN-2017 JML    PBI 27998: WIGOS-2695 - Rounding - Apply rounding according to the minimum chip
// 16-JUN-2017 ATB    PBI 28030: PalacioDeLosNumeros - New design in recharge vouchers
// 18-APR-2017 MS     WIGOS-229: Junkets - Redeem the promotion for the given flyer
// 04-MAY-2017 DPC    WIGOS-1574: Junkets - Flyers - New field "Text in Promotional Voucher" - Change voucher
// 08-MAY-2017 MS     PBI 27045: Junket Vouchers
// 23-MAY-2017 DHA    PBI 27487: WIGOS-83 MES21 - Sales selection
// 24-MAY-2017 JML    PBI 27484:WIGOS-1226 Win / loss - Introduce the WinLoss - EDITION SCREEN - PER DENOMINATION
// 24-MAY-2017 JML    PBI 27486:WIGOS-1227 Win / loss - Introduce the WinLoss - EDITION SCREEN - ACCUMULATED
// 24-MAY-2017 RAB    PBI 27485: WIGOS-983 Win / loss - Introduce the WinLoss - SELECTION SCREEN
// 25-MAY-2017 JML    Bug 27690:WIGOS-2349 Win/Loss. After edit the register it is not possible to enter a decimal value as amount.
// 30-MAY-2017 JML    PBI 27486: WIGOS-1227 Win / loss - Introduce the WinLoss - EDITION SCREEN - ACCUMULATED
// 25-MAY-2017 JML    Bug 27690:WIGOS-2349 Win/Loss. After edit the register it is not possible to enter a decimal value as amount.
// 31-MAY-2017 JML    PBI 27810 - WIGOS-2423 Win - loss - Change request
// 15-JUN-2017 JML    PBI 27998: WIGOS-2695 - Rounding - Apply rounding according to the minimum chip
// 16-JUN-2017 ATB    PBI 28030: PalacioDeLosNumeros - New design in recharge vouchers
// 19-JUN-2017 JML    PBI 27999:WIGOS-2699 - Rounding - Accumulate the remaining amount
// 06-JUL-2017 JML    PBI 28422: WIGOS-2702 Rounding - Consume the remaining amount
// 12-JUL-2017 RGR    Fixed Bug 28666: WIGOS-3431 Error malfunction in withdrawal / close voucher / ticket bank card, Error information closing tickets
// 13-JUL-2017 DHA    Bug 28672:WIGOS-3512 Not possible to send bills from Cage to Cash
// 17-JUL-2017 RAB    Bug 28720:WIGOS-3655 Not possible to send chips from Cage to Cash
// 17-JUL-2017 RAB    Bug 28721:WIGOS-3659 Cashier - Is showing an error when i click on "Mostrar Filas sin Cantidad"
// 19-JUL-2017 DHA    Bug 28861:WIGOS-3811 Cashier - Exception is displayed when close cash session with chips.
// 19-JUL-2017 DHA    Bug 28860:WIGOS-3793 Cashier - is displaying an error when i try to make a withdrawal
// 27-JUL-2017 JML    Bug 29012: WIGOS-4006 Cashier - multicurrency: It is not allowed to sell chips with GBP currency in ticket sales mode.
//                               WIGOS-4008 Cashier - Multicurrency: discrepancies on voucher of change of currency after sales chips with foreign currency.
//                               WIGOS-4013 MultiCurrency - Sales chips with recharge. The change of currency is done two times according to cash movements. 
//                               WIGOS-4017 Multicurrency - Tables: dealer copy ticket conciliantion amount is changed of currency.
// 28-JUL-2017 DHA    Bug 29039:WIGOS-3469 [Ticket #6619] Informaci�n de reportes de mesas con montos duplicados / Information of tables reports with duplicate amounts
// 09-AUG-2017 AMF    Bug 29275:[WIGOS-3912] It is not possible to type decimals starting with "0.xx" when closing cashier session.
// 09-AUG-2017 AMF    Bug 29278:[WIGOS-4032] Junkets: wrong position of Flyer button when selling chips
// 09-AUG-2017 RAB    Bug 29071:WIGOS-4070 Tables - It is no allowed to close the gaming table with integrated cashier including the bankcard and check operations
// 24-AUG-2017 DHA    Bug 29441:WIGOS-4674 Tables - It is not allowed to close the gaming table with integrated cashier including bankcard and check operations
// 04-SEP-2017 RGR    Bug 29537:WIGOS-4689 Cashier - credit card amount column is missing on close cashier session form
// 07-SEP-2017 DHA    Bug 29629:WIGOS-4736 Placeholder dispayed in the screen "Cancelacion de promociones"
// 15-SEP-2017 JML    Bug 29790:WIGOS-5165 [Ticket #8881] Release 03.005.105 - POS Integrado a Mesa - Cierre de mesa - Debe aparecer fichas inicialmente
// 18-SEP-2017 RAB    Bug 29797:WIGOS-5229 Cashier - Unhandled expecion with withdraw of redeemed chips
// 28-sep-2017 dha    Bug 29964:WIGOS-5456 Cashier: unhandled exception with cash deposit operation
// 05-OCT-2017 RAB    PBI 30021:WIGOS-4048 Payment threshold authorisation - Recharge authorisation
// 05-OCT-2017 RAB    PBI 30018:WIGOS-4036 Payment threshold authorization - Payment authorisation
// 09-OCT-2017 JML    PBI 30022:WIGOS-4052 Payment threshold registration - Recharge with Check payment
// 10-OCT-2017 JML    PBI 30023:WIGOS-4068 Payment threshold registration - Transaction information
// 18-OCT-2017 DHA    Bug 30311:WIGOS-5228 [Ticket #8683] Venta de Fichas con Recarga sin Redondeo de Centavos En Mesa de Juego V03.06.0023
// 26-OCT-2017 RAB    Bug 30427:WIGOS-6008 Cashier - error when a promotion is deleted after sale chips with recharge
// 26-OCT-2017 RAB    Bug 30430:WIGOS-6045 Threshold: No permission requested when creating tickets exceeding input threshold
// 30-OCT-2017 RAB    Bug 30446:WIGOS-6052 Threshold: Screen order is incorrect in HP and dispute (paying with PO) exceeding threshold and without permissions
// 31-OCT-2017 RAB    Bug 30488:WIGOS-6058 Threshold: missing check data screen when exceeding input threshold
// 02-NOV-2017 AMF    Bug 30533:[WIGOS-6201] Different screens in a cashless cashier is displaying the field "Tickets tito"
// 02-NOV-2017 AMF    Bug 30534:[WIGOS-6199] The deposit screen in the cashier is displaying a field to do a deposit of "Tickets tito"
// 02-NOV-2017 RAB    Bug 30535: WIGOS-6202 Linked gaming table - multicurrency: it is allowed to validate a ticket copia dealer from foreign currency and after unhandled exception is displayed
// 07-NOV-2017 RAB    Bug 30598:WIGOS-6343 [Ticket #10081] v03.005.0128 QA - Registro Erroneo "cierre caja - faltante" en Movimientos de Caja
// 13-NOV-2017 DHA    Bug 30769:WIGOS-6514 Recharge with bank card and checks are showing the data entry screen with the parameter disabled
// 30-JAN-2018 AGS    Bug 31366:WIGOS-7951 [Ticket #12101] CAJA ABIERTA
// 21-FEB-2018 DHA    Bug 31619:WIGOS-8109 [Ticket #12335] Custodia
// 07-FEB-2018 AGS    Fixed Bug 31619:WIGOS-8109 [Ticket #12335] Custodia
// 01-MAR-2018 EOR    Bug 31765: WIGOS-8491 Closing Cash message gets error when cancelling message when closing unbalance 
// 17-JUN-2018 LQB    Bug 33107:WIGOS-13012 Gambling tables: Close cash session error with gaming table with integrated cashier when there are operations with check and credit card.
// 26-JUN-2018 AGS    Bug 33314:WIGOS-13109 Some card bank or check amounts in close session
// 27-JUN-2018 JML    Bug 33363:WIGOS-13262 Cashier: account balance in negative after sale chips from account.
// 06-JUL-2018 FOS    Bug 33488:WIGOS-13374 Extra Handpay confirmation screen at the end of Handpay payment
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WSI.Common;
using System.Data.SqlClient;
using System.Globalization;
using System.Collections;
using System.Reflection;
using System.Threading;
using WSI.Cashier.TITO;
using WSI.Common.TITO;
using System.IO;
using WSI.Common.Entrances;
using WSI.Common.Entrances.Enums;
using WSI.Cashier.JunketsCashier;
using WSI.Common.Junkets;

namespace WSI.Cashier
{
  public enum VoucherTypes
  {
    CardAdd = 0,
    CardRedeem = 1,
    FillIn = 2,
    FillOut = 3,
    OpenCash = 4,
    CloseCash = 5,
    MBCardChangeLimit = 6,
    MBCardCashDeposit = 7,
    StatusCash = 8,
    CardReplacement = 9,
    MBCardSetLimit = 10,
    CancelPromotions = 11,
    TicketRedeem = 12,
    ChipsSaleAmountInput = 13,
    ChipsSaleWithRecharge = 14,
    GenericAmountInput = 15,
    AmountRequest = 16,
    ChipsPurchaseAmountInput = 17,
    ChipsSwap = 18,
    ChipsStock = 19,
    CageConcepts = 20,
    ChangeChipsIn = 21,
    ChangeChipsOut = 22,
    DropBoxCount = 23,
    CreditLineGetMarker = 24,
    WinLoss = 25,
  };

  public partial class frm_amount_input : Controls.frm_base
  {
    //Constant only for promotions
    private const Int16 GRID_COLUMN_PROMO_CREDIT_TYPE_IMAGE = 0;
    private const Int16 GRID_COLUMN_PROMO_ID = 1;
    private const Int16 GRID_COLUMN_PROMO_NAME = 2;
    private const Int16 GRID_COLUMN_PROMO_WON_LOCK = 3;
    private const Int16 GRID_COLUMN_PROMO_CREDIT_TYPE = 4;
    private const Int16 GRID_COLUMN_PROMO_MIN_RECHARGE = 5;
    private const Int16 GRID_COLUMN_PROMO_MIN_RECHARGE_TEXT = 6;
    private const Int16 GRID_COLUMN_PROMO_REWARD = 7;
    private const Int16 GRID_COLUMN_PROMO_SORT = 8;

    // Cage amounts
    private const Int16 SQL_COLUMN_CAGE_AMOUNTS_IMAGE = 0;
    private const Int16 SQL_COLUMN_CAGE_AMOUNTS_CAA_TYPE = 1; //IF THIS VALUE IS CHANGED, CHANGE IN UC_BANK TO
    private const Int16 SQL_COLUMN_CAGE_AMOUNTS_CAA_DENOMINATION_SHOWED = 2;
    private const Int16 SQL_COLUMN_CAGE_AMOUNTS_CAA_CHIP_NAME = 3;
    private const Int16 SQL_COLUMN_CAGE_AMOUNTS_CAA_CHIP_DRAW = 4;
    private const Int16 SQL_COLUMN_CAGE_AMOUNTS_CAA_COLOR = 5;
    private const Int16 SQL_COLUMN_CAGE_AMOUNTS_CAA_ISO_CODE = 6; //IF THIS VALUE IS CHANGED, CHANGE IN UC_BANK TO    
    private const Int16 SQL_COLUMN_CAGE_AMOUNTS_CAA_ALLOWED = 7;
    private const Int16 SQL_COLUMN_CAGE_AMOUNTS_CAA_DENOMINATION = 8; //IF THIS VALUE IS CHANGED, CHANGE IN UC_BANK TO        
    private const Int16 SQL_COLUMN_CAGE_AMOUNTS_CAA_UN = 9; //IF THIS VALUE IS CHANGED, CHANGE IN UC_BANK TO
    private const Int16 SQL_COLUMN_CAGE_AMOUNTS_CAA_TOTAL = 10; //IF THIS VALUE IS CHANGED, CHANGE IN UC_BANK TO
    private const Int16 SQL_COLUMN_CAGE_AMOUNTS_CAA_CAGE_CURRENCY_TYPE = 11;
    private const Int16 SQL_COLUMN_CAGE_AMOUNTS_CAA_CHIP_ID = 12;

    private const Int16 GRID_COLUMN_CAGE_AMOUNTS_IMAGE = 0;
    private const Int16 GRID_COLUMN_CAGE_AMOUNTS_CURRENCY_TYPE = 1;
    private const Int16 GRID_COLUMN_CAGE_AMOUNTS_DENOMINATION_SHOWED = 2;
    private const Int16 GRID_COLUMN_CAGE_AMOUNTS_CHIP_NAME = 3;
    private const Int16 GRID_COLUMN_CAGE_AMOUNTS_CHIP_DRAW = 4;
    private const Int16 GRID_COLUMN_CAGE_AMOUNTS_COLOR = 5;
    private const Int16 GRID_COLUMN_CAGE_AMOUNTS_ISO_CODE = 6;
    private const Int16 GRID_COLUMN_CAGE_AMOUNTS_ALLOWED = 7;
    private const Int16 GRID_COLUMN_CAGE_AMOUNTS_DENOMINATION = 8;
    private const Int16 GRID_COLUMN_CAGE_AMOUNTS_UN = 9;
    private const Int16 GRID_COLUMN_CAGE_AMOUNTS_TOTAL = 10;
    private const Int16 GRID_COLUMN_CAGE_AMOUNTS_CURRENCY_TYPE_ENUM = 11;
    private const Int16 GRID_COLUMN_CAGE_AMOUNTS_CHIP_ID = 12;
    private const Int16 GRID_COLUMN_CAGE_AMOUNTS_CHIP_SET_ID = 13;

    // Cage amounts - Width
    private const Int16 WIDTH_COLUMN_CAGE_AMOUNTS_NOT_SHOWED = 0;
    private const Int16 WIDTH_COLUMN_CAGE_AMOUNTS_ISO_CODE = 0;
    private const Int16 WIDTH_COLUMN_CAGE_AMOUNTS_DENOMINATION = 0;
    private const Int16 WIDTH_COLUMN_CAGE_AMOUNTS_ALLOWED = 0;
    private const Int16 WIDTH_COLUMN_CAGE_AMOUNTS_UN = 80;
    private const Int16 WIDTH_COLUMN_CAGE_AMOUNTS_TOTAL = 150;
    private const Int16 WIDTH_COLUMN_CAGE_AMOUNTS_DENOMINATION_SHOWED = 130;
    private const Int16 WIDTH_COLUMN_CAGE_AMOUNTS_DENOMINATION_CURRENCY_TYPE = 28;
    private const Int16 WIDTH_COLUMN_CAGE_AMOUNTS_COLOR = 25;
    private const Int16 WIDTH_COLUMN_CAGE_AMOUNTS_IMAGE = 35;
    private const Int16 WIDTH_COLUMN_CAGE_AMOUNTS_DRAWING = 99;
    private const Int16 WIDTH_COLUMN_CAGE_AMOUNTS_CHIP_NAME = 99;
    private const Int16 WIDTH_GRID = 327;
    private const Int16 HEIGHT_GRID = 390;
    private const Int16 LOCATION_GRID_X = 4;
    private const Int16 LOCATION_GRID_Y = 17;

    // Pending movements
    private const Int16 SQL_COLUMN_CPM_MOVEMENT_ID = 0;
    private const Int16 SQL_COLUMN_CPM_USER_ID = 1;
    private const Int16 SQL_COLUMN_CMD_CAGE_MOVEMENT_DETAIL_ID = 2;
    private const Int16 SQL_COLUMN_CMD_MOVEMENT_ID = 3;
    private const Int16 SQL_COLUMN_CMD_QUANTITY = 4;
    private const Int16 SQL_COLUMN_CMD_DENOMINATION = 5;
    private const Int16 SQL_COLUMN_CMD_ISO_CODE = 6;
    private const Int16 SQL_COLUMN_CMD_CURRENCY_TYPE = 7;
    private const Int16 SQL_COLUMN_CMD_CHIP_ID = 8;

    private const Int16 SQL_COLUMN_CUR_ISO_CODE = 7;
    private const Int16 SQL_COLUMN_CUR_ALLOWED = 8;
    private const Int16 SQL_COLUMN_CUR_NAME = 9;
    private const Int16 SQL_COLUMN_CUR_ALIAS1 = 10;
    private const Int16 SQL_COLUMN_CUR_ALIAS2 = 11;
    private const Int16 SQL_COLUMN_CUR_CURRENCY_TYPE = 12;
    private const Int16 SQL_COLUMN_CUR_CHIP_ID = 13;

    private const Int16 MAX_NUMBER_OF_QUESTIONS = 2;

    //private const Int32 Cage.COINS_CODE = -100;              // Monedas.
    //private const Int32 Cage.TICKETS_CODE = -200;             // Ticket.
    //private const Int32 Cage.BANK_CARD_CODE = -1;             // Tarjeta bancaria.
    //private const Int32 Cage.CHECK_CODE = -2;                // Cheque.

    public delegate Boolean CheckPermissionsToRedeemDelegate(Int64 AccountId, Currency TotalToPay, Currency PrizeAmount, List<ProfilePermissions.CashierFormFuncionality> PermissionList);
    public delegate Boolean CheckAntiMoneyLaunderingDelegate(Int64 AccountId, ENUM_CREDITS_DIRECTION CreditsDirection, Currency Amount,
                                                             out ENUM_ANTI_MONEY_LAUNDERING_LEVEL AntiMlLevel, out Boolean ThresholdCrossed);


    private frm_catalog m_form_catalog;
    private Decimal m_pct_tax_1;
    private Decimal m_pct_tax_2;
    private Decimal m_pct_tax_3;
    private Boolean m_is_apply_tax;
    private Boolean m_show_apply_tax_button;
    private Boolean m_is_apply_tax_result;
    private Int64 m_account_operation_reason_id;
    private String m_account_operation_comment;
    private Decimal m_total_currency_amount;
    private Boolean m_show_rows_without_amount;

    // ATB 02-NOV-2016
    // Variable set for the vouchers
    private Voucher m_voucher_card;

    private JunketsShared m_junket_info;
    private string m_barcode;


    private enum GridType
    {
      None = 0,
      Promotions = 1,
      CageDenominations = 2,
    };

    //public class CageAmountsDictionary : Dictionary<String, CageAmountsDictionaryItem>
    //{
    //  public new void Add(String Key, CageAmountsDictionaryItem Value)
    //  {
    //    base.Add(Key.Trim().ToUpper(), Value);
    //  }
    //}

    //public struct CageAmountsDictionaryItem
    //{
    //  public DataTable ItemAmounts;
    //  public Decimal TotalCurrency;
    //  public Boolean AllowedFillOut;
    //}

    public Decimal set_cage_total_value
    {
      // GeneralParam.GetString("RegionalOptions", "CurrencyISOCode")
      set
      {
        CurrencyExchangeResult _exchange_result;
        if (uc_payment_method1.CurrencyExchange == null || m_national_currency.CurrencyCode == uc_payment_method1.CurrencyExchange.CurrencyCode && !FeatureChips.IsChipsType(uc_payment_method1.CurrencyExchange.Type))
        {
          if (uc_payment_method1.CurrencyExchange != null)
          {
            if (Misc.IsBankCardWithdrawalEnabled() &&
                uc_payment_method1.CurrencyExchange.Type == CurrencyExchangeType.CARD)
            {
              lbl_cage_total_value.Text = ((Int32)value).ToString();
            }
            else
            {
              lbl_cage_total_value.Text = ((Currency)value).ToString();
            }
          }
          else
          {
            lbl_cage_total_value.Text = ((Currency)value).ToString();
          }

        }
        else
        {
          if (uc_payment_method1.CurrencyExchange.Type == CurrencyExchangeType.CURRENCY || FeatureChips.IsChipsType(uc_payment_method1.CurrencyExchange.Type))
          {
            switch (uc_payment_method1.CurrencyExchange.Type)
            {
              case CurrencyExchangeType.CASINOCHIP:
              case CurrencyExchangeType.CASINO_CHIP_RE:
              case CurrencyExchangeType.CASINO_CHIP_NRE:
                if (m_national_currency.CurrencyCode == uc_payment_method1.CurrencyExchange.CurrencyCode)
                {
                  lbl_cage_total_value.Text = String.Format("{0} {1}", ((Currency)value).ToString(), Resource.String("STR_CAGE_IN_CASINO_CHIPS"));
                }
                else
                {
                  lbl_cage_total_value.Text = String.Format("{0} {1}", Currency.Format(value, uc_payment_method1.CurrencyExchange.CurrencyCode), Resource.String("STR_CAGE_IN_CASINO_CHIPS"));
                }
                break;

              case CurrencyExchangeType.CASINO_CHIP_COLOR:
                lbl_cage_total_value.Text = String.Format("{0} {1}", ((Int32)value).ToString(), Resource.String("STR_CAGE_IN_CASINO_CHIPS"));
                break;

              default:
                lbl_cage_total_value.Text = Currency.Format(value, uc_payment_method1.CurrencyExchange.CurrencyCode);

                break;
            }
          }
          else
          {
            lbl_cage_total_value.Text = Currency.Format(value, uc_payment_method1.CurrencyExchange.CurrencyCode);
          }

          // Update total Exchange:
          if (m_is_exchange == true && (value > 0 && m_national_currency.CurrencyCode != uc_payment_method1.CurrencyExchange.CurrencyCode))
          {
            uc_payment_method1.CurrencyExchange.ApplyExchange(value, out _exchange_result);
            lbl_cage_currency_total.Text = Resource.String("STR_FRM_AMOUNT_INPUT_EQUALS") + " " + Currency.Format(_exchange_result.GrossAmount, m_national_currency.CurrencyCode);
          }
          else
          {
            lbl_cage_currency_total.Text = "";
          }
        }
        //Set total current currency in a member variable
        m_total_currency_amount = value;
      }
    }

    public Decimal get_cage_total_value
    {
      get
      {
        return m_total_currency_amount;
      }
    }

    public CashierSessionInfo CashierSessionInfo
    {
      get
      {
        if (m_cashier_session_info == null || m_cashier_session_info.TerminalId == Cashier.CashierSessionInfo().TerminalId)
        {
          // Set cashier session info if is not defined
          IsGamingTable = GamingTablesSessions.GamingTableInfo.IsGamingTable;
          return Cashier.CashierSessionInfo();
        }
        return m_cashier_session_info;
      }

      set
      {
        m_cashier_session_info = value;
      }
    }

    public Boolean IsGamingTable
    {
      get
      {
        if (!m_is_gaming_table)
        {
          // Set gaming table info if is not defined
          return GamingTablesSessions.GamingTableInfo.IsGamingTable;
        }
        return m_is_gaming_table;
      }

      set
      {
        m_is_gaming_table = value;
      }
    }

    public JunketsShared JunketInfo
    {
      get
      {
        return m_junket_info;
      }

      set
      {
        m_junket_info = value;
      }
    }

    public Int64? GamingTableId
    {
      get
      {
        return m_gaming_table_id;
      }

      set
      {
        m_gaming_table_id = value;
      }
    }

    //Properties used in frm_gaming_tables_win_loss
    #region Specific properties

    public Int16 sql_column_cage_amount_denomination
    {
      get
      {
        return GRID_COLUMN_CAGE_AMOUNTS_DENOMINATION_SHOWED;
      }
    }

    public Int16 sql_column_cage_amount_quantity
    {
      get
      {
        return GRID_COLUMN_CAGE_AMOUNTS_UN;
      }
    }

    public Int16 sql_column_cage_amount_total_amount
    {
      get
      {
        return GRID_COLUMN_CAGE_AMOUNTS_TOTAL;
      }
    }

    public Int16 sql_column_cage_amount_chip_id
    {
      get
      {
        return GRID_COLUMN_CAGE_AMOUNTS_CHIP_ID;
      }
    }

    #endregion

    #region Attributes

    private static Boolean m_cancel = true;
    private static String m_decimal_str = System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencyDecimalSeparator;

    private static VoucherTypes m_voucher_type;
    private static CardData m_card_data;
    private static MBCardData m_mb_card_data;
    private static Currency m_aux_amount;
    private static CASH_MODE m_cash_mode;

    private static Currency m_voucher_amount;
    private static Currency m_out_amount;

    private Boolean is_voucher_loaded;
    private Int32 m_selected_promo_index;
    private Int64 m_selected_promo_id;
    private String m_selected_promo_name;
    private Promotion.TYPE_PROMOTION_DATA m_selected_promo_data;
    private Currency m_available_limit;

    private Currency m_promo_reward_amount;
    private Currency m_promo_won_lock;
    private Int32 m_promo_num_tokens;
    private Boolean m_promo_warning_shown;

    private MultiPromos.PromoBalance m_promo_lost;

    // RCI 17-NOV-2010
    private Currency m_spent_used;
    private Currency m_input_spent;
    private Currency m_input_played;
    private Currency? m_input_amount;
    private Currency m_input_amount_exchange;

    private Currency m_total_to_pay;

    DataTable m_dt_remaining_spent_per_provider;
    DataTable m_dt_remaining_spent_per_terminal;

    private Boolean m_promos_expanded_view;
    private List<Point> _prev_loc = new List<Point>();
    private List<Size> _prev_size = new List<Size>();
    private Boolean m_previously_calculated = false;
    private Currency m_cash_in_calculated = 0;
    private String M_STR_WONLOCK = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_021");

    private Font m_font_filter_credit_checked = null;
    private Font m_font_filter_credit_unchecked = null;
    private Boolean m_contracting = false;

    private static Boolean m_out_check_redeem = false;        // For the output parameter in the Show() function
    private static Boolean m_payment_order_enabled = false;   // To read GP PaymentOrder.Enabled
    private static Currency m_payment_order_min_amount = 0;   // To read GP PaymentOrder.MinAmount

    // DLL 20-JUN-2013
    private List<CurrencyExchange> m_currencies = null;
    private CurrencyExchange m_voucher_currency_exchange = null;
    private CurrencyExchange m_national_currency = null;
    private Boolean m_with_currency_exchange = false;
    private CurrencyExchangeResult m_exchange_result;
    private PaymentThresholdAuthorization m_payment_threshold_authorization;
    private PaymentAndRechargeThresholdData m_payment_and_recharge_threshold_data;

    private WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS m_session_stats;
    private GridType m_grid_type;
    // DDM 19-AUG-2013
    private ParticipateInCashDeskDraw m_participate_in_cashdesk_draw = ParticipateInCashDeskDraw.Default;

    private SortedDictionary<CurrencyIsoType, Decimal> m_currencies_amount = new SortedDictionary<CurrencyIsoType, Decimal>(new CurrencyIsoType.SortComparer());

    private Currency m_cash_redeem_prize_amount;

    private CheckPermissionsToRedeemDelegate m_dCheckPermissionsToRedeem;
    private CheckAntiMoneyLaunderingDelegate m_dCheckAntiMoneyLaundering;

    // JAB 08-NOV-2013
    private Boolean m_cage_enabled;
    private Boolean m_cage_automode;
    private Boolean m_cage_allow_denomination_in_balance;
    private Boolean m_tito_enabled;
    private Boolean m_tito_tickets_collection;
    private Boolean m_show_voucher = false;
    private Boolean m_allowed_entries = true;
    public DataTable m_dt_cage_pending_movements;
    // MPO 12-SEP-2014 Default values: Empty cage amounts --> WIG-1238
    public CageDenominationsItems.CageDenominationsDictionary m_cage_amounts = new CageDenominationsItems.CageDenominationsDictionary();
    public Int64 m_current_pending_movement;
    public Cage.CageStatus m_cage_movement_status;
    private Boolean m_from_clear = false;
    private Int16 m_num_of_retries;

    private Currency m_tito_max_devolution;

    private Boolean m_is_chips_operation = false;
    private Boolean m_is_chips_sale_at_table_register = false;
    private bool m_is_chips_sale_with_recharge = false;
    private Currency m_total_redeemable;
    private FeatureChips.ChipsOperation m_chips_amounts;
    private Decimal m_chips_promo_re = 0;

    private List<VoucherValidationNumber> m_validation_numbers;
    public List<VoucherValidationNumber> m_jackpot_handpay_validation_numbers;

    private HandPay m_handpay;

    private Boolean m_is_pending_request;
    private Int64 m_request_movement_id;
    private Boolean m_is_exchange = false;
    private Boolean m_has_pending_movements = false;

    private Boolean m_show_msg = false;

    // RMS 08-AUG-2014
    private CurrencyExchangeSubType m_selected_bank_card_type = CurrencyExchangeSubType.NONE;

    // DHA 02-MAR-2015
    private CashierSessionInfo m_cashier_session_info;
    private Boolean m_is_gaming_table;
    private Int64? m_gaming_table_id;

    // DHA 09-JUN-2016
    private ClosingStocks m_closing_stock;

    private Boolean m_show_currency_exchange;

    // MS 28-MAR-2017
    Currency m_creditline_spent;

    GamingTableBusinessLogic.GT_WIN_LOSS_MODE m_win_loss_mode;
    List<Int64> m_gt_win_loss_ids;
    Boolean m_win_loss_amount_entry_screen_is_open;

    #endregion

    #region Constructor

    /// <summary>
    /// Constructor method.
    /// 1. Initialize controls.
    /// 2. Initialize control�s resources. (NLS, Images, etc)
    /// </summary>
    public frm_amount_input()
      : this(null, null)
    {
    }

    public frm_amount_input(CheckPermissionsToRedeemDelegate dCheckPermissionsToRedeem, CheckAntiMoneyLaunderingDelegate dCheckAntiMoneyLaundering)
    {
      m_dCheckPermissionsToRedeem = dCheckPermissionsToRedeem;
      m_dCheckAntiMoneyLaundering = dCheckAntiMoneyLaundering;

      InitializeComponent();
      InitializeControlResources();

      EventLastAction.AddEventLastAction(this.Controls);

      KeyboardMessageFilter.RegisterHandler(this, BarcodeType.Flyer, FlyerReceivedEvent);
    }

    #endregion

    private void CardListener(KbdMsgEvent Type, Object Data)
    {
      Barcode _barcode;
      DialogResult _rc;

      m_cancel = true;
      _barcode = (Barcode)Data;

      if (Type == KbdMsgEvent.EventBarcode)
      {
        Log.Message(String.Format("Card reading during operation: {0}", _barcode.ExternalCode.ToString()));

        // Show message        
        _rc = frm_message.Show(Resource.String("STR_CARD_READING_NOT_VALID"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Warning, ParentForm);

        this.DialogResult = DialogResult.Cancel;

        this.Close();
      }
    }

    #region Buttons

    //private Boolean flag_money = false;
    private Currency btn_money_1_amount;
    private Currency btn_money_2_amount;
    private Currency btn_money_3_amount;
    private Currency btn_money_4_amount;
    private string btn_money_1_value;
    private string btn_money_2_value;
    private string btn_money_3_value;
    private string btn_money_4_value;

    //------------------------------------------------------------------------------
    // PURPOSE : Initialize Amount Buttons money.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void InitMoneyButtons()
    {
      String _currency_format = ""; //"C0";
      Decimal _tmp_decimal;

      btn_money_1_amount = 0;
      btn_money_1_value = "";
      btn_money_2_amount = 0;
      btn_money_2_value = "";
      btn_money_3_amount = 0;
      btn_money_3_value = "";
      btn_money_4_amount = 0;
      btn_money_4_value = "";

      this.btn_money_1.Enabled = true;
      this.btn_money_2.Enabled = true;
      this.btn_money_3.Enabled = true;
      this.btn_money_4.Enabled = true;
      this.btn_num_1.Enabled = true;
      this.btn_num_2.Enabled = true;
      this.btn_num_3.Enabled = true;
      this.btn_num_4.Enabled = true;
      this.btn_num_5.Enabled = true;
      this.btn_num_6.Enabled = true;
      this.btn_num_7.Enabled = true;
      this.btn_num_8.Enabled = true;
      this.btn_num_9.Enabled = true;
      this.btn_num_10.Enabled = true;
      this.btn_num_dot.Enabled = true;
      this.btn_intro.Enabled = true;
      this.btn_back.Enabled = true;

      this.btn_money_1.Visible = true;
      this.btn_money_2.Visible = true;
      this.btn_money_3.Visible = true;
      this.btn_money_4.Visible = true;
      this.btn_num_1.Visible = true;
      this.btn_num_2.Visible = true;
      this.btn_num_3.Visible = true;
      this.btn_num_4.Visible = true;
      this.btn_num_5.Visible = true;
      this.btn_num_6.Visible = true;
      this.btn_num_7.Visible = true;
      this.btn_num_8.Visible = true;
      this.btn_num_9.Visible = true;
      this.btn_num_10.Visible = true;
      this.btn_num_dot.Visible = true;
      this.btn_intro.Visible = true;
      this.btn_back.Visible = true;

      LoadDefaultAmountButtonsSQL();

      btn_money_1.Text = "+" + btn_money_1_amount.ToString(_currency_format);
      if ((Decimal)btn_money_1_amount == 0)
      {
        this.btn_money_1.Enabled = false;
        this.btn_money_1.Visible = false;
      }
      if ((Decimal)btn_money_1_amount >= 1000)
      {
        CashierStyle.Fonts.ChangeFontControl(this.btn_money_1, 11f, true);
      }

      btn_money_2.Text = "+" + btn_money_2_amount.ToString(_currency_format);
      if ((Decimal)btn_money_2_amount == 0)
      {
        this.btn_money_2.Enabled = false;
        this.btn_money_2.Visible = false;
      }
      if ((Decimal)btn_money_2_amount >= 1000)
      {
        CashierStyle.Fonts.ChangeFontControl(this.btn_money_2, 11f, true);
      }

      btn_money_3.Text = "+" + btn_money_3_amount.ToString(_currency_format);
      if ((Decimal)btn_money_3_amount == 0)
      {
        this.btn_money_3.Enabled = false;
        this.btn_money_3.Visible = false;
      }
      if ((Decimal)btn_money_3_amount >= 1000)
      {
        CashierStyle.Fonts.ChangeFontControl(this.btn_money_3, 11f, true);
      }

      btn_money_4.Text = "+" + btn_money_4_amount.ToString(_currency_format);
      if ((Decimal)btn_money_4_amount == 0)
      {
        this.btn_money_4.Enabled = false;
        this.btn_money_4.Visible = false;
      }
      if ((Decimal)btn_money_4_amount >= 1000)
      {
        CashierStyle.Fonts.ChangeFontControl(this.btn_money_4, 11f, true);
      }

      if (Format.GetFormattedDecimals(CurrencyExchange.GetNationalCurrency()) == 0)
      {
        this.btn_num_dot.Enabled = false;
        this.btn_num_dot.BackColor = System.Drawing.Color.LightGray;
      }

      if (m_cage_enabled && dgv_common.Rows.Count > 0 && dgv_common.SelectedRows.Count > 0 && m_voucher_type != VoucherTypes.StatusCash && m_voucher_type != VoucherTypes.WinLoss)
      {
        Decimal.TryParse(dgv_common.SelectedRows[0].Cells[GRID_COLUMN_CAGE_AMOUNTS_DENOMINATION].Value.ToString(), out _tmp_decimal);
        if (Format.GetFormattedDecimals(CurrencyExchange.GetNationalCurrency()) == 0)
        {
          this.btn_num_dot.Enabled = false;
          this.btn_num_dot.BackColor = System.Drawing.Color.LightGray;
        }
        else
        {
          this.btn_num_dot.Enabled = (_tmp_decimal == Cage.COINS_CODE || _tmp_decimal == Cage.BANK_CARD_CODE || _tmp_decimal == Cage.CHECK_CODE);
        }
      }

    }

    //------------------------------------------------------------------------------
    // PURPOSE : Load Default Amount Buttons money.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void LoadDefaultAmountButtonsSQL()
    {
      SqlConnection sql_conn;
      SqlDataAdapter da;
      DataSet ds;
      String sql_str;
      Decimal convert_money;

      sql_conn = null;
      ds = null;
      da = null;

      try
      {
        sql_str = " SELECT  gp_subject_key, gp_key_value        " +
                      "   FROM  general_params                  " +
                      "  WHERE  gp_group_key    = 'Cashier.AddAmount' ";

        sql_conn = WGDB.Connection();
        ds = new DataSet();
        da = new SqlDataAdapter(sql_str, sql_conn);
        da.Fill(ds);

        if (ds.Tables[0].Rows.Count > 0)
        {
          foreach (DataRow row in ds.Tables[0].Rows)
          {
            switch ((String)row[0])
            {
              case "CustomButton1":
                try
                {
                  btn_money_1_value = (String)row[1];
                  convert_money = Decimal.Parse(btn_money_1_value);
                  btn_money_1_amount = (Currency)convert_money;
                }
                catch (Exception ex)
                {
                  Log.Exception(ex);
                }
                break;

              case "CustomButton2":
                try
                {
                  btn_money_2_value = (String)row[1];
                  convert_money = Decimal.Parse(btn_money_2_value);
                  btn_money_2_amount = (Currency)convert_money;
                }
                catch (Exception ex)
                {
                  Log.Exception(ex);
                }
                break;

              case "CustomButton3":
                try
                {
                  btn_money_3_value = (String)row[1];
                  convert_money = Decimal.Parse(btn_money_3_value);
                  btn_money_3_amount = (Currency)convert_money;
                }
                catch (Exception ex)
                {
                  Log.Exception(ex);
                }
                break;

              case "CustomButton4":
                try
                {
                  btn_money_4_value = (String)row[1];
                  convert_money = Decimal.Parse(btn_money_4_value);
                  btn_money_4_amount = (Currency)convert_money;
                }
                catch (Exception ex)
                {
                  Log.Exception(ex);
                }
                break;

              default:
                break;
            }
          }
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
      finally
      {
        // Close connection
        if (da != null)
        {
          da.Dispose();
          da = null;
        }

        if (ds != null)
        {
          ds.Dispose();
          ds = null;
        }

        if (sql_conn != null)
        {
          sql_conn.Close();
          sql_conn = null;
        }
      }
    } // LoadDefaultAmountButtonsSQL

    private void btn_num_1_Click(object sender, EventArgs e)
    {
      AddNumber(((System.Windows.Forms.ButtonBase)(sender)).Text);
    }

    private void btn_num_2_Click(object sender, EventArgs e)
    {
      AddNumber(((System.Windows.Forms.ButtonBase)(sender)).Text);
    }

    private void btn_num_3_Click(object sender, EventArgs e)
    {
      AddNumber(((System.Windows.Forms.ButtonBase)(sender)).Text);
    }

    private void btn_num_4_Click(object sender, EventArgs e)
    {
      AddNumber(((System.Windows.Forms.ButtonBase)(sender)).Text);
    }

    private void btn_num_5_Click(object sender, EventArgs e)
    {
      AddNumber(((System.Windows.Forms.ButtonBase)(sender)).Text);
    }

    private void btn_num_6_Click(object sender, EventArgs e)
    {
      AddNumber(((System.Windows.Forms.ButtonBase)(sender)).Text);
    }

    private void btn_num_7_Click(object sender, EventArgs e)
    {
      AddNumber(((System.Windows.Forms.ButtonBase)(sender)).Text);
    }

    private void btn_num_8_Click(object sender, EventArgs e)
    {
      AddNumber(((System.Windows.Forms.ButtonBase)(sender)).Text);
    }

    private void btn_num_9_Click(object sender, EventArgs e)
    {
      AddNumber(((System.Windows.Forms.ButtonBase)(sender)).Text);
    }

    private void btn_num_10_Click(object sender, EventArgs e)
    {
      AddNumber(((System.Windows.Forms.ButtonBase)(sender)).Text);
    }

    private void btn_money_1_Click(object sender, EventArgs e)
    {
      Decimal _money_value;
      String _money_text;

      try
      {
        _money_value = Decimal.Parse(btn_money_1_value);
        if (txt_amount.Text.Length > 0)
        {
          _money_value = _money_value + Decimal.Parse(txt_amount.Text);
        }
      }
      catch
      {
        _money_value = 0;
      }

      if (_money_value <= Cashier.MAX_ALLOWED_AMOUNT)
      {
        _money_text = _money_value.ToString();

        txt_amount.Text = "";
        AddNumber(_money_text);
      }
    }

    private void btn_money_2_Click(object sender, EventArgs e)
    {
      Decimal _money_value;
      String _money_text;

      try
      {
        _money_value = Decimal.Parse(btn_money_2_value);
        if (txt_amount.Text.Length > 0)
        {
          _money_value = _money_value + Decimal.Parse(txt_amount.Text);
        }
      }
      catch
      {
        _money_value = 0;
      }

      if (_money_value <= Cashier.MAX_ALLOWED_AMOUNT)
      {
        _money_text = _money_value.ToString();

        txt_amount.Text = "";
        AddNumber(_money_text);
      }
    }

    private void btn_money_3_Click(object sender, EventArgs e)
    {
      Decimal _money_value;
      String _money_text;

      try
      {
        _money_value = Decimal.Parse(btn_money_3_value);
        if (txt_amount.Text.Length > 0)
        {
          _money_value = _money_value + Decimal.Parse(txt_amount.Text);
        }
      }
      catch
      {
        _money_value = 0;
      }

      if (_money_value <= Cashier.MAX_ALLOWED_AMOUNT)
      {
        _money_text = _money_value.ToString();

        txt_amount.Text = "";
        AddNumber(_money_text);
      }
    }

    private void btn_money_4_Click(object sender, EventArgs e)
    {
      Decimal _money_value;
      String _money_text;

      try
      {
        _money_value = Decimal.Parse(btn_money_4_value);
        if (txt_amount.Text.Length > 0)
        {
          _money_value = _money_value + Decimal.Parse(txt_amount.Text);
        }
      }
      catch
      {
        _money_value = 0;
      }

      if (_money_value <= Cashier.MAX_ALLOWED_AMOUNT)
      {
        _money_text = _money_value.ToString();

        txt_amount.Text = "";
        AddNumber(_money_text);
      }
    }

    private void btn_num_dot_Click(object sender, EventArgs e)
    {
      if (txt_amount.Text.IndexOf(m_decimal_str) == -1)
      {
        txt_amount.Text = txt_amount.Text + m_decimal_str;
      }
      btn_intro.Focus();
    }

    private void btn_cancel_Click(object sender, EventArgs e)
    {
      m_cancel = true;

      // Clean To Pay label
      lbl_total_to_pay.Text = "";
      lbl_currency_to_pay.Text = "";

      //if (m_cage_enabled && m_allowed_entries)
      //{
      //  // Restart movement status
      //  CancelPendingMovements();
      //}

      Misc.WriteLog("[FORM CLOSE] frm_amount_input (cancel)", Log.Type.Message);
      this.Close();
    }

    private void btn_back_Click(object sender, EventArgs e)
    {
      if (txt_amount.Text.Length > 0)
      {
        txt_amount.Text = txt_amount.Text.Substring(0, txt_amount.Text.Length - 1);
      }
      btn_intro.Focus();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Handle the click on the ok button, which confirms all shown data
    //           and closes the screen.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void btn_ok_Click(object sender, EventArgs e)
    {

      Int32 _max_allowed_int;
      Currency _max_allowed;
      String _error_str;
      List<ProfilePermissions.CashierFormFuncionality> _list_permissions;

      Currency _min_limit_exceded;
      ProfilePermissions.CashierFormFuncionality _type_limit_exceded;
      String _message;
      CurrencyIsoType _iso_type;

      ENUM_ANTI_MONEY_LAUNDERING_LEVEL _aml_level;
      Boolean _threshold_crossed;

      DialogResult _dlg_participate_cash_desk_draw;

      Int32 _max_retries;
      Boolean _is_stock_control_ok;

      Decimal _default_currency_input_amount;

      frm_bank_transaction_data _frm_bank_transaction;
      frm_yesno _frm_shader;

      //EOR 13-JUL-2016
      Decimal _recharge_limit_amount;
      List<ProfilePermissions.CashierFormFuncionality> _permissions_list;

      CurrencyExchange _currency_copy;

      CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION _cashDeskId = CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION.DRAW_00; //LTC 22-SEP-2016

      _list_permissions = new List<ProfilePermissions.CashierFormFuncionality>();
      _min_limit_exceded = -1;
      _iso_type = new CurrencyIsoType();

      _is_stock_control_ok = false;

      _default_currency_input_amount = 0;

      _max_retries = GeneralParam.GetInt32("Cashier", "UnbalancedCashClosing.MaxAttempts", 3);

      // RMS 08-AUG-2014
      if (m_exchange_result != null && GeneralParam.GetBoolean("Cashier.PaymentMethod", "BankCard.DifferentiateType", false))
      {
        m_exchange_result.SubType = uc_payment_method1.SelectedBankCardType;
      }

      // FJC 02-SEP-2014: Fixed Bug WIG-1173: Change closing messages from CASH CLOSING and GAMBLING TABLE CLOSING.
      if (m_voucher_type == VoucherTypes.CloseCash)
      {
        if (m_session_stats.currencies_balance == null)
        {
          frm_message.Show(Resource.String("STR_UC_BANK_CASH_CLOSED").Replace("\\r\\n", "\r\n"), Resource.String("STR_UC_BANK_CASH_CLOSED_TITLE"),
            MessageBoxButtons.OK, MessageBoxIcon.Warning, this.ParentForm);
          return;
        }

        // Check that no empty amounts 
        foreach (CurrencyExchange _currency in m_currencies)
        {
          _currency_copy = _currency.Copy();

          if (_currency.CurrencyCode == Cage.CHIPS_ISO_CODE && _currency.Type == CurrencyExchangeType.CASINOCHIP)
          {
            _iso_type.IsoCode = CurrencyExchange.GetNationalCurrency();
            _iso_type.Type = CurrencyExchangeType.CASINO_CHIP_RE;

            foreach (CurrencyExchange _ce in m_currencies)
            {
              if ((_iso_type.IsoCode == _ce.CurrencyCode) && (_iso_type.Type == _ce.Type))
              {
                _currency_copy = _ce;
                break;
              }
            }
          }
          else
          {
            _iso_type.IsoCode = _currency.CurrencyCode;
            _iso_type.Type = _currency.Type;
          }

          //LTC 05-AGO-2016
          if (Misc.IsBankCardCloseCashEnabled())
          {
            if (!m_currencies_amount.ContainsKey(_iso_type) &&
                ((m_session_stats.currencies_balance.ContainsKey(_iso_type) && m_session_stats.currencies_balance[_iso_type] > 0) ||
                        (_currency.CurrencyCode == m_national_currency.CurrencyCode && m_session_stats.final_balance > 0 && _currency.Type == CurrencyExchangeType.CURRENCY)) &&
                            (_currency.Type != CurrencyExchangeType.CARD))
            {
              frm_message.Show(Resource.String("STR_FRM_AMOUNT_INPUT_MSG_CLOSE_CASH_PENDING_CURRENCY_AMOUNT", _currency_copy.Description), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this);

              uc_payment_method1.CurrencyExchange = _currency_copy;

              return;
            }

            // 24-AUG-2016 LTC
            if (_currency.Type == CurrencyExchangeType.CARD)
            {
              // LTC 05-AGO-2016
              Withdrawal.GetPinpanMovements(CashierSessionInfo.CashierSessionId);
              Withdrawal.GetPinpadMovementsNoReconciliated(CashierSessionInfo.CashierSessionId);

              if (m_currencies_amount.ContainsKey(_iso_type))
              {
                Withdrawal.CountDelivered = Convert.ToInt32(m_currencies_amount[_iso_type]);
                Withdrawal.Type = CurrencyExchangeType.CARD;

                if (Withdrawal.ToRecon != 0)
                {
                  frm_message.Show(Resource.String("STR_WITHDRAWAL_CHECK_ERROR_TO_RECON_AMOUNT"),
                                                  Resource.String("STR_APP_GEN_MSG_ERROR"),
                                                  MessageBoxButtons.OK,
                                                  MessageBoxIcon.Error, this);
                  return;
                }

                if (m_currencies_amount[_iso_type] == 0 && Withdrawal.Count != 0)
                {
                  frm_message.Show(Resource.String("STR_FRM_AMOUNT_INPUT_MSG_CLOSE_CASH_CARD_VOUCHERS_COUNT", _currency.Description), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this);

                  uc_payment_method1.CurrencyExchange = _currency;

                  return;
                }

                if (m_currencies_amount[_iso_type] == Withdrawal.Count)
                {

                  frm_message.Show(Resource.String("STR_CLOSECASH_CHECK_CORRECT_AMOUNT"), // Se concilia correctamente.
                                  Resource.String("STR_VOUCHER_INFO_TITLE"),
                                  MessageBoxButtons.OK,
                                  MessageBoxIcon.Information, this);

                  Withdrawal.Reconciliated = true;
                }
                else
                {
                  if (m_currencies_amount[_iso_type] > Withdrawal.Count)
                  {
                    frm_message.Show(Resource.String("STR_WITHDRAWAL_CHECK_ERROR_MAJOR_AMOUNT"),
                                   Resource.String("STR_APP_GEN_MSG_ERROR"),
                                   MessageBoxButtons.OK,
                                   MessageBoxIcon.Error, this);

                    Withdrawal.Reconciliated = false;
                    return;
                  }

                  if (m_currencies_amount[_iso_type] < Withdrawal.Count)
                  {
                    frm_message.Show(Resource.String("STR_CLOSECASH_CHECK_ERROR_MINOR_AMOUNT"), // El monto introducido menor que el contabilizado se debe conciliar
                                   Resource.String("STR_APP_GEN_MSG_ERROR"),
                                   MessageBoxButtons.OK,
                                   MessageBoxIcon.Error, this);

                    Withdrawal.Reconciliated = false;
                    return;
                  }
                }
              }
              else
              {
                if (Withdrawal.Count > 0)
                {
                  frm_message.Show(Resource.String("STR_FRM_AMOUNT_INPUT_MSG_CLOSE_CASH_PENDING_CURRENCY_AMOUNT", _currency.Description), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this);
                  Withdrawal.Reconciliated = false;

                  uc_payment_method1.CurrencyExchange = _currency;

                  return;
                }
              }
            }
          }
          else
          {
            if (!m_currencies_amount.ContainsKey(_iso_type) &&
              ((m_session_stats.currencies_balance.ContainsKey(_iso_type) && m_session_stats.currencies_balance[_iso_type] > 0) ||
               (_currency.CurrencyCode == m_national_currency.CurrencyCode && m_session_stats.final_balance > 0 && _currency.Type == CurrencyExchangeType.CURRENCY)))
            {
              frm_message.Show(Resource.String("STR_FRM_AMOUNT_INPUT_MSG_CLOSE_CASH_PENDING_CURRENCY_AMOUNT", _currency.Description), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this);

              uc_payment_method1.CurrencyExchange = _currency;

              return;
            }
          }
        }

        Currency pending_amount;

        // Check that no amounts are pending from mobile banks

        pending_amount = CashierBusinessLogic.CalculatePendingAmount(CashierSessionInfo.CashierSessionId);
        if (pending_amount > 0)
        {
          frm_message.Show(Resource.String("STR_FRM_AMOUNT_INPUT_MSG_MBCARD_PENDING_AMOUNT", pending_amount), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this);

          m_cancel = true;
          Misc.WriteLog("[FORM CLOSE] frm_amount_input (ok)", Log.Type.Message);
          this.Close();

          return;
        }
      }

      if (m_cage_enabled)
      {
        switch (m_voucher_type)
        {
          case VoucherTypes.OpenCash:
          case VoucherTypes.FillIn:
            // if cage movements aren't right then show error
            if (m_allowed_entries)
            {
              // DHA 09-JUN-2016: validating closing cash
              if (m_closing_stock != null)
              {
                m_closing_stock.ValidateOpeningStock(m_cage_amounts, m_cashier_session_info, "ISO_CODE", "DENOMINATION", "CAGE_CURRENCY_TYPE_ENUM", "CHIP_ID", "UN");
              }
              else if ((!m_cage_automode || (m_cage_automode && m_has_pending_movements)) && !AllowedEntriesStatus())
              {
                if (m_cage_movement_status != Cage.CageStatus.UserCancelToConfirmAmounts)
                {
                  if (m_cage_movement_status == Cage.CageStatus.OkDenominationImbalance)
                  {
                    _message = Resource.String("STR_CAGE_DENOMINATION_DEPOSIT_NOT_ALLOWED").Replace("\\r\\n", "\r\n");
                  }
                  else
                  {
                    _message = Resource.String("STR_CAGE_DEPOSIT_NOT_ALLOWED").Replace("\\r\\n", "\r\n");
                  }

                  frm_message.Show(_message,
                                   Resource.String("STR_CAGE_MOVEMENTS"),
                                   MessageBoxButtons.OK,
                                   MessageBoxIcon.Error, this);
                  //Log.Error("Intento de apertura de caja [cage mode]. SessionId: " + Cashier.SessionId.ToString() + " UserId: " + Cashier.UserId);
                }

                return;
              }
            }

            break;

          case VoucherTypes.CloseCash:
            Decimal _tmp_current_balance;
            Boolean _allow_closed;
            String _tmp_isocode;
            CurrencyIsoType _national_iso_type;

            _allow_closed = true;

            //sql_conn = WSI.Common.WGDB.Connection();
            //sql_trx = sql_conn.BeginTransaction();

            // DLL 31-DEC-2013: ReadCashierSessionData is called at btn_intro_click once time. If you call you will delete collected
            //WSI.Common.Cashier.ReadCashierSessionData(sql_trx, Cashier.SessionId, ref m_session_stats);
            if (m_session_stats.currencies_balance.Count == 0)
            {
              if (m_session_stats.final_balance.SqlMoney.Value != get_cage_total_value)
              {
                _allow_closed = false;
              }
            }
            else
            {
              _tmp_isocode = "";
              foreach (KeyValuePair<CurrencyIsoType, Decimal> _balance in m_session_stats.currencies_balance)
              {
                switch (_balance.Key.Type)
                {
                  case CurrencyExchangeType.CURRENCY:
                  default:
                    _tmp_isocode = _balance.Key.IsoCode;
                    break;

                  case CurrencyExchangeType.CHECK:
                    _tmp_isocode = _balance.Key.IsoCode + Cage.CHECK_CODE;
                    break;

                  case CurrencyExchangeType.CARD:
                    _tmp_isocode = _balance.Key.IsoCode + Cage.BANK_CARD_CODE;
                    break;
                }

                //DPC 22-MAR-2017: Credit line don't check the balance
                if (_balance.Key.Type != CurrencyExchangeType.CREDITLINE)
                {
                  //LTC 05-AGO-2016
                  if (_balance.Key.Type == CurrencyExchangeType.CARD && Misc.IsBankCardCloseCashEnabled())
                  {
                    if (m_session_stats.collected_diff.ContainsKey(_iso_type))
                    {
                      m_session_stats.collected_diff[_iso_type] -= Withdrawal.Count;
                    }
                  }
                  else
                  {
                    _tmp_current_balance = GetCurrentBalance(_tmp_isocode, _balance.Key.Type);
                    if (_tmp_current_balance != _balance.Value)
                    {
                      _allow_closed = false;
                    }
                  }
                }
              }

              _tmp_isocode = "";
              switch (m_national_currency.Type)
              {
                case CurrencyExchangeType.CURRENCY:
                default:
                  _tmp_isocode = m_national_currency.CurrencyCode;
                  break;

                case CurrencyExchangeType.CHECK:
                  _tmp_isocode = m_national_currency.CurrencyCode + Cage.CHECK_CODE;
                  break;

                case CurrencyExchangeType.CARD:
                  _tmp_isocode = m_national_currency.CurrencyCode + Cage.BANK_CARD_CODE;
                  break;
              }

              //LTC 05-AGO-2016
              if (m_national_currency.Type == CurrencyExchangeType.CARD && Misc.IsBankCardCloseCashEnabled())
              {
              }
              else
              {
                _default_currency_input_amount = GetCurrentBalance(_tmp_isocode, m_national_currency.Type);

                if ((m_session_stats.final_balance.SqlMoney.Value != _default_currency_input_amount)) // && !GamingTablesSessions.GamingTableInfo.IsGamingTable)
                {
                  _allow_closed = false;
                }
              }
            }

            // FJC 02-SEP-2014: Fixed Bug WIG-1173: Change closing messages from CASH CLOSING and GAMBLING TABLE CLOSING.
            if (_allow_closed && _max_retries > 0)
            {
              if (GamingTableBusinessLogic.IsGamingTablesEnabled())
              {
                // Control of Chips
                using (DB_TRX _db_trx = new DB_TRX())
                {
                  _is_stock_control_ok = StockControl(_db_trx.SqlTransaction);
                }

                if (!_is_stock_control_ok || !CheckTips())
                {
                  _allow_closed = false;
                }
              }
            }

            //TODO: Generate strings with "Balance descuadrado" Detail (divisas, fichas, cheques...) to show in POPUP
            // FJC 02-SEP-2014: Fixed Bug WIG-1173: Change closing messages from CASH CLOSING and GAMBLING TABLE CLOSING.
            if (!_allow_closed)
            {
              if (_max_retries <= 0)
              {
                _allow_closed = true;
              }
              else
              {
                // FJC 02-SEP-2014: Fixed Bug WIG-1173: Change closing messages from CASH CLOSING and GAMBLING TABLE CLOSING.
                if (!IsGamingTable)
                {
                  if (m_num_of_retries < _max_retries)// MAX_NUMBER_OF_QUESTIONS)
                  {
                    DialogResult _dlg_rc;

                    _dlg_rc = frm_message.Show(Resource.String("STR_CAGE_CLOSE_SESSION_DOESNT_MATCH_USER_MSG_CONFIRMATION").Replace("\\r\\n", "\r\n"),
                                     Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_TITLE"),
                                     MessageBoxButtons.YesNo,
                                     MessageBoxIcon.Warning, this); //El monto introducido no coincide con el esperado. �Desea Cerrar la caja de todos modos? 

                    m_session_stats.MsgUnbalanceAccept = _dlg_rc == System.Windows.Forms.DialogResult.OK;

                    if (_dlg_rc != DialogResult.OK)
                    {
                      ++m_num_of_retries;

                      return;
                    }
                  }
                }
              }
            }

            // SMN & LEM & RCI 16-OCT-2014: Always include MXN in m_session_stats.collected_diff (default value = 0).
            _national_iso_type = new CurrencyIsoType();
            _national_iso_type.IsoCode = m_national_currency.CurrencyCode;
            _national_iso_type.Type = m_national_currency.Type;
            if (m_session_stats.collected_diff != null && !m_session_stats.collected_diff.ContainsKey(_national_iso_type))
            {
              m_session_stats.collected_diff.Add(_national_iso_type, 0);
            }

            //LTC 06-SEP-2016
            if (Misc.IsBankCardCloseCashEnabled() && Withdrawal.Reconciliated == true)
            {
              Misc.PrintBankCardSection = true;
            }

            break;

          case VoucherTypes.FillOut:
            break;

          //ESE 27-SEP-2016
          case VoucherTypes.StatusCash:
            if (Misc.IsBankCardCloseCashEnabled())
            {
              Withdrawal.GetPinpanMovementsCutOff(CashierSessionInfo.CashierSessionId);
              Misc.PrintBankCardSection_StatusCash = true;
            }
            break;
        }
      }

      // DLL 14-JAN-2014: Check stock
      // FJC 02-SEP-2014: Fixed Bug WIG-1173: Change closing messages from CASH CLOSING and GAMBLING TABLE CLOSING.
      if (m_voucher_type == VoucherTypes.FillOut)
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _is_stock_control_ok = StockControl(_db_trx.SqlTransaction);
        }

        if (GamingTableBusinessLogic.IsGamingTablesEnabled() && !_is_stock_control_ok)
        {
          frm_message.Show(Resource.String("STR_CHIPS_CONTROL_STOCK_ERROR"), // El monto introducido no coincide con el stock de fichas
                           Resource.String("STR_APP_GEN_MSG_ERROR"),
                           MessageBoxButtons.OK,
                           MessageBoxIcon.Error, this);

          return;
        }

        if (!CheckTips())
        {
          frm_message.Show(Resource.String("STR_CHIPS_TIPS_CHECK_ERROR"), // El monto de propinas no puede superar el total
                             Resource.String("STR_APP_GEN_MSG_ERROR"),
                             MessageBoxButtons.OK,
                             MessageBoxIcon.Error, this);
          return;
        }

        // ATB 11-JAN-2017
        // If the cards might be conciliated
        if (Misc.IsBankCardWithdrawalEnabled())
        {
          // Running inside every currency, the target is set the cards's reconciliation by the requested amount
          foreach (KeyValuePair<CurrencyIsoType, decimal> _currency in m_currencies_amount)
          {
            //LTC 05-AGO-2016
            // ATB 11-JAN-2017            
            if (_currency.Key.Type == CurrencyExchangeType.CARD)
            {
              if (_currency.Value == Withdrawal.Count)
              {
                Withdrawal.Reconciliated = true;
              }
              else
              {
                if (_currency.Value > Withdrawal.Count)
                {
                  frm_message.Show(Resource.String("STR_WITHDRAWAL_CHECK_ERROR_MAJOR_AMOUNT"), // El monto introducido no debe ser mayor que el contabilizado
                                 Resource.String("STR_APP_GEN_MSG_ERROR"),
                                 MessageBoxButtons.OK,
                                 MessageBoxIcon.Error, this);

                  Withdrawal.Reconciliated = false;
                  return;
                }

                if (_currency.Value < Withdrawal.Count)
                {
                  frm_message.Show(Resource.String("STR_WITHDRAWAL_CHECK_ERROR_MINOR_AMOUNT"), // El monto introducido menor que el contabilizado se debe conciliar
                                 Resource.String("STR_APP_GEN_MSG_ERROR"),
                                 MessageBoxButtons.OK,
                                 MessageBoxIcon.Error, this);

                  Withdrawal.Reconciliated = false;
                }
              }
            }
          }
        }
      }

      if (m_voucher_type == VoucherTypes.CardAdd)
      {
        Int64 _account_id = 0;
        frm_search_players _form_search_players = new frm_search_players();

        //Bug 22031: Compra de fichas con tarjeta bancaria puede realizarse aunque el cliente no se encuentre en la sala - Mallorca
        if (Misc.IsAccRequiredByCurrency(uc_payment_method1.CurrencyExchange, m_input_amount))
        {
          if (m_card_data.IsVirtualCard)
          {
            _account_id = _form_search_players.Show(this.ParentForm);
            if (_account_id <= 0)
            {
              return;
            }

            CardData.DB_CardGetAllData(_account_id, m_card_data, false);
          }

          if (!IsCustomerRegisteredInReception(m_card_data))
          {
            return;
          }
        }

        // SGB 30-JUL-2014 Permission to add create ticket an delete to uc_ticket_create
        if (WSI.Common.TITO.Utils.IsTitoMode() && !m_is_chips_operation && m_input_amount > 0)
        {
          _list_permissions.Add(ProfilePermissions.CashierFormFuncionality.TITO_CreateRETicket);
        }
        // HBB 18-DEC-2014 Permission to add credit in cashless
        if (!WSI.Common.TITO.Utils.IsTitoMode() && m_input_amount > 0)
        {
          _list_permissions.Add(ProfilePermissions.CashierFormFuncionality.CardAddCreditRE);
        }

        // JML 13-NOV-2013: No question about the draw, and no more checks, if the input amount is greater than remaining sales limit
        if (m_input_amount != null && m_input_amount > 0)
        {
          if (GeneralParam.GetBoolean("Cashier", "SalesLimit.Enabled"))
          {
            Currency _remaining_sales_limit;
            String _text;
            if (!WSI.Common.Cashier.GetRemainingSalesLimit(CashierSessionInfo.UserId, CashierSessionInfo.CashierSessionId, WSI.Common.Cashier.SalesLimitType.Cashier, out _remaining_sales_limit))
            {
              Log.Error("AllowCurrentCashierSale. Error reading Cashier Sesion Limit Sales. SessionId: " + CashierSessionInfo.CashierSessionId.ToString());

              return;
            }

            if (m_input_amount > _remaining_sales_limit)
            {
              _text = Resource.String("STR_FRM_CASHIER_LIMIT_SALES_REACHED") + _remaining_sales_limit;
              _text = _text.Replace("\\r\\n", "\r\n");
              frm_message.Show(_text, Resource.String("STR_APP_GEN_MSG_WARNING"), MessageBoxButtons.OK, MessageBoxIcon.Warning, ParentForm);

              return;
            }
          }
        }

        if (m_selected_promo_id > 0)
        {
          Currency _cashier_current_balance;
          Currency _promo_amount;

          // Checks that currently selected promotion (if any) can still be applied
          if (!ValidatePromotion(m_voucher_amount, m_promo_num_tokens, true, out m_promo_reward_amount, out m_promo_won_lock))
          {
            m_cancel = true;

            return;
          }

          if (m_promo_reward_amount > 0)
          {
            if (m_selected_promo_data.credit_type == ACCOUNT_PROMO_CREDIT_TYPE.NR1 || m_selected_promo_data.credit_type == ACCOUNT_PROMO_CREDIT_TYPE.NR2)
            {
              if (WSI.Common.TITO.Utils.IsTitoMode() && !m_is_chips_operation)
              {
                _list_permissions.Add(ProfilePermissions.CashierFormFuncionality.TITO_CreatePromoNRTicket);
              }
              else
              {
                _list_permissions.Add(ProfilePermissions.CashierFormFuncionality.CardAddPromoNRCredit);
              }
            }
            else if (m_selected_promo_data.credit_type == ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE && WSI.Common.TITO.Utils.IsTitoMode() && !m_is_chips_operation)
            {
              _list_permissions.Add(ProfilePermissions.CashierFormFuncionality.TITO_CreatePromoRETicket);
            }
            else if (m_selected_promo_data.credit_type == ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE && !WSI.Common.TITO.Utils.IsTitoMode())
            {
              _list_permissions.Add(ProfilePermissions.CashierFormFuncionality.CardAddPromoRE);
            }
          }

          switch (m_selected_promo_data.special_permission)
          {
            case Promotion.PROMOTION_SPECIAL_PERMISSION.PROMOTION_SPECIAL_PERMISSION_NOT_SET:
              break;

            case Promotion.PROMOTION_SPECIAL_PERMISSION.PROMOTION_SPECIAL_PERMISSION_A:
              _list_permissions.Add(ProfilePermissions.CashierFormFuncionality.PromoSpecialPermissionA);
              break;

            case Promotion.PROMOTION_SPECIAL_PERMISSION.PROMOTION_SPECIAL_PERMISSION_B:
              _list_permissions.Add(ProfilePermissions.CashierFormFuncionality.PromoSpecialPermissionB);
              break;

            default:
              frm_message.Show(Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_MSG_007"),
                               Resource.String("STR_APP_GEN_MSG_ERROR"),
                               MessageBoxButtons.OK,
                               MessageBoxIcon.Error, this);
              return;
          }

          _cashier_current_balance = 0;
          try
          {
            using (DB_TRX _db_trx = new DB_TRX())
            {
              if (m_selected_promo_data.credit_type == ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE)
              {
                String _iso_code;
                if (WSI.Common.TITO.Utils.IsTitoMode())
                {
                  _iso_code = m_input_amount_exchange.CurrencyIsoCode;
                }
                else
                {
                  _iso_code = CurrencyExchange.GetNationalCurrency();
                }
                _cashier_current_balance = WSI.Common.Cashier.UpdateSessionBalance(_db_trx.SqlTransaction, CashierSessionInfo.CashierSessionId, 0,
                                                                                       _iso_code, CurrencyExchangeType.CASINO_CHIP_RE);
              }
            }
          }
          catch (Exception ex)
          {
            Log.Exception(ex);

            return;
          }

          if (m_is_chips_operation)
          {
            if (Utils.IsTitoMode())
            {
              _promo_amount = m_selected_promo_data.credit_type == ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE ? m_promo_reward_amount : 0;
              if (_cashier_current_balance < m_input_amount + _promo_amount && !FeatureChips.IsChipsSaleByTicket)
              {
                frm_message.Show(Resource.String("STR_FRM_AMOUNT_INPUT_MSG_CHIPS_EXCEED_BANK"), Resource.String("STR_APP_GEN_MSG_ERROR"),
                               MessageBoxButtons.OK, MessageBoxIcon.Error, this);

                return;
              }
            }
            else
            {
              //SGB 22-OCT-2014: Check if integrated chip sale with promotion is over the chips cashier balance
              if (m_selected_promo_data.credit_type == ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE &&
                  _cashier_current_balance < m_total_redeemable + m_chips_promo_re && !FeatureChips.IsChipsSaleByTicket)
              {
                frm_message.Show(Resource.String("STR_FRM_AMOUNT_INPUT_MSG_CHIPS_EXCEED_BANK"),
                                 Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this);
                m_cancel = true;
                return;
              }
            }
          }
        }

        if (m_is_chips_sale_with_recharge && !WSI.Common.TITO.Utils.IsTitoMode())
        {
          m_chips_amounts.RoundingSaleChips = new RoundingSaleChips(m_total_redeemable + m_chips_promo_re, true);

          if (m_chips_amounts.RoundingSaleChips.CalculateResult != RoundingSaleChips.GT_CALCULATE_ROUNDED_AMOUNT_RESULT.OK)
          {
            if (m_chips_amounts.RoundingSaleChips.CalculateResult == RoundingSaleChips.GT_CALCULATE_ROUNDED_AMOUNT_RESULT.AMOUNT_LESS_THAN_MINIMUM_DENOMINATION)
            {
              // show message when enter amount less than minimun denomination
              frm_message.Show(Resource.String("STR_ROUNDING_SALE_CHIPS_AMOUNT_LESS_THAN_MINIMUM_DENOMINATION"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this);
            }
            return;
          }
          if (!GeneralParam.GetBoolean("GamingTables", "Cashier.Mode"))
          {
            m_chips_amounts.ChipsAmount = m_chips_amounts.RoundingSaleChips.RoundedAmount;
            m_chips_amounts.FillChipsAmount = m_chips_amounts.RoundingSaleChips.RoundedAmount;
          }
        }

        // RBG 02-APR-2013 Assing default values to calculate max allowed to show in popup 
        _type_limit_exceded = 0;

        // RCI 08-NOV-2010: Check MaxAllowedCashIn.
        if (m_is_chips_operation)
        {
          // Only for TITO gets the max allowed exchange chips
          if (WSI.Common.TITO.Utils.IsTitoMode())
          {
            _max_allowed_int = GamingTableBusinessLogic.GetGamingTablesMaxAllowedSales(m_input_amount_exchange.CurrencyIsoCode);
          }
          else
          {
            _max_allowed_int = GamingTableBusinessLogic.GetGamingTablesMaxAllowedSales(CurrencyExchange.GetNationalCurrency());
          }
        }
        else
        {
          Int32.TryParse(Misc.ReadGeneralParams("Cashier", "MaxAllowedCashIn"), out _max_allowed_int);
        }

        _max_allowed = _max_allowed_int;

        if (!WSI.Common.TITO.Utils.IsTitoMode() && _max_allowed > 0 && m_input_amount > _max_allowed ||
             WSI.Common.TITO.Utils.IsTitoMode() && _max_allowed > 0 && m_input_amount_exchange > _max_allowed)
        {
          _min_limit_exceded = _max_allowed;

          _type_limit_exceded = m_is_chips_operation ? ProfilePermissions.CashierFormFuncionality.ChipsSales_MaxAllowed
                                                     : ProfilePermissions.CashierFormFuncionality.CardAddCreditMaxAllowed;

          _list_permissions.Add(_type_limit_exceded);
        }

        // RCI 01-JUL-2011: Check MaxAllowedDailyCashIn.
        Int32.TryParse(Misc.ReadGeneralParams("Cashier", "MaxAllowedDailyCashIn"), out _max_allowed_int);
        _max_allowed = _max_allowed_int;

        if (_max_allowed > 0)
        {
          Currency _daily_cash_in;

          // Calculate all the cash-in of the day.
          _daily_cash_in = Accounts.SumTodayCashIn(m_card_data.AccountId) + (Currency)m_input_amount;
          if (_daily_cash_in > _max_allowed)
          {
            if (_min_limit_exceded == -1)
            {
              _min_limit_exceded = _max_allowed;
            }
            else
            {
              _min_limit_exceded = Math.Min(_max_allowed, _min_limit_exceded);
            }

            if (_min_limit_exceded == _max_allowed)
            {
              _type_limit_exceded = ProfilePermissions.CashierFormFuncionality.CardAddCreditMaxAllowedDaily;
            }

            _list_permissions.Add(ProfilePermissions.CashierFormFuncionality.CardAddCreditMaxAllowedDaily);
          }
        }

        // Show popup for max allowed exceded.
        if (_min_limit_exceded != -1)
        {
          switch (_type_limit_exceded)
          {
            case ProfilePermissions.CashierFormFuncionality.CardAddCreditMaxAllowed:
            default:
              _message = Resource.String("STR_CARD_ADD_CREDIT_MAX_EXCEDED", m_input_amount.ToString(), _min_limit_exceded.ToString());
              break;

            case ProfilePermissions.CashierFormFuncionality.CardAddCreditMaxAllowedDaily:
              _message = Resource.String("STR_CARD_ADD_CREDIT_MAX_DAILY_EXCEDED", m_input_amount.ToString());
              break;

            case ProfilePermissions.CashierFormFuncionality.ChipsSales_MaxAllowed:
              if (WSI.Common.TITO.Utils.IsTitoMode() && (m_input_amount_exchange.CurrencyIsoCode != null && m_input_amount_exchange.CurrencyIsoCode != CurrencyExchange.GetNationalCurrency()))
              {
                _message = Resource.String("STR_CHIPS_SALES_MAX_EXCEDED", Currency.Format(m_input_amount_exchange, m_input_amount_exchange.CurrencyIsoCode), Currency.Format(_min_limit_exceded, m_input_amount_exchange.CurrencyIsoCode));
              }
              else
              {
                _message = Resource.String("STR_CHIPS_SALES_MAX_EXCEDED", m_input_amount.ToString(), _min_limit_exceded.ToString());
              }
              break;
          }

          _message = _message.Replace("\\r\\n", "\r\n");
          if (frm_message.Show(_message, Resource.String("STR_APP_GEN_MSG_WARNING"),
              MessageBoxButtons.YesNo, Images.CashierImage.Question, Cashier.MainForm) != DialogResult.OK)
          {
            return;
          }
        }

        // LJM 29-JAN-2014: Check AntiMoneyLaundering only when CURRENCY recharge.
        // JAB 24-FEB-2014: Fixed Bug #WIG-667
        if (m_voucher_currency_exchange == null || m_voucher_currency_exchange.Type == CurrencyExchangeType.CURRENCY)
        {
          if (m_dCheckAntiMoneyLaundering != null)
          {
            _aml_level = ENUM_ANTI_MONEY_LAUNDERING_LEVEL.None;
            _threshold_crossed = false;

            if (!m_dCheckAntiMoneyLaundering(m_card_data.AccountId, ENUM_CREDITS_DIRECTION.Recharge, (Currency)m_input_amount,
                                             out _aml_level, out _threshold_crossed))
            {
              m_cancel = true;
              Misc.WriteLog("[FORM CLOSE] frm_amount_input (ok - not checkantimoneylaundering)", Log.Type.Message);
              this.Close();

              return;
            }

            if (AML_RequestPermissionToExceedReportLimit(ENUM_CREDITS_DIRECTION.Recharge, _aml_level, _threshold_crossed))
            {
              if (_aml_level == ENUM_ANTI_MONEY_LAUNDERING_LEVEL.MaxAllowed)
              {
                _list_permissions.Add(ProfilePermissions.CashierFormFuncionality.AML_ExceedMaximumLimitRecharge);
              }
              else
              {
                _list_permissions.Add(ProfilePermissions.CashierFormFuncionality.AML_ExceedReportLimitRecharge);
              }
            }
          }
        }

        // RCI 01-JUL-2011: Check if current user has all the permissions in the list.
        if (_list_permissions.Count > 0)
        {
          if (!ProfilePermissions.CheckPermissionList(_list_permissions,
                                                      ProfilePermissions.TypeOperation.RequestPasswd,
                                                      this,
                                                      0,
                                                      out _error_str))
          {
            return;
          }
        }

        if (m_voucher_type == VoucherTypes.CardAdd
           && WSI.Common.TITO.Utils.IsTitoMode()
           && PaymentThresholdAuthorization.GetInputThreshold() > 0
           && PaymentThresholdAuthorization.GetInputThreshold() <= Format.ParseCurrency(lbl_total_to_pay.Text))
        {
          if (!ProfilePermissions.CheckPermissionList(ProfilePermissions.CashierFormFuncionality.TITO_RechargeThreshold,
                                ProfilePermissions.TypeOperation.RequestPasswd,
                                this,
                                0,
                                out _error_str))
          {
            return;
          }
        }

        // RMS 08-AUG-2014 : Check if must enter data for exchange with bank cards or checks
        if (m_exchange_result != null)
        {
          if (m_exchange_result.InType == CurrencyExchangeType.CARD || m_exchange_result.InType == CurrencyExchangeType.CHECK)
          {

            // EOR 13-JUL-2016: Recharge with Bank Card
            if (m_exchange_result.InType == CurrencyExchangeType.CARD)
            {
              _recharge_limit_amount = GeneralParam.GetDecimal("Cashier.PaymentMethod", "BankCard.Recharge.Limit.Amount", 0);
              _permissions_list = new List<ProfilePermissions.CashierFormFuncionality>();
              _permissions_list.Add(ProfilePermissions.CashierFormFuncionality.Credit_card_recharge);

              if (_recharge_limit_amount > 0 & m_input_amount > _recharge_limit_amount)
              {
                if (!ProfilePermissions.CheckPermissionList(_permissions_list,
                                                            ProfilePermissions.TypeOperation.RequestPasswd,
                                                            this,
                                                            Cashier.AuthorizedByUserId,
                                                            out _error_str))
                {
                  return;
                }
              }
            }

            Boolean _edit_bank_transaction_data;

            switch (m_exchange_result.InType)
            {
              case CurrencyExchangeType.CARD:
                _edit_bank_transaction_data = GeneralParam.GetBoolean("Cashier.PaymentMethod", "BankCard.EnterTransactionDetails", false);
                break;

              case CurrencyExchangeType.CHECK:
                _edit_bank_transaction_data = GeneralParam.GetBoolean("Cashier.PaymentMethod", "Check.EnterTransactionDetails", false);
                break;

              default:
                _edit_bank_transaction_data = false;

                break;
            }

            if (WSI.Common.TITO.Utils.IsTitoMode() && PaymentThresholdAuthorization.GetInputThreshold() > 0 && !_edit_bank_transaction_data && (m_exchange_result.InType == CurrencyExchangeType.CARD || m_exchange_result.InType == CurrencyExchangeType.CHECK))
            {
              _edit_bank_transaction_data = (m_exchange_result.GrossAmount >= PaymentThresholdAuthorization.GetInputThreshold());
            }

            if (_edit_bank_transaction_data)
            {
              DialogResult _rc;

              _frm_shader = new frm_yesno();
              _frm_shader.Opacity = 0.6;
              _frm_shader.Show(Parent);

              _frm_bank_transaction = new frm_bank_transaction_data(uc_payment_method1.CurrencyExchange.Type);

              m_exchange_result.BankTransactionData.TransactionType = TransactionType.RECHARGE;

              // TODO: Edit the transaction data
              _rc = _frm_bank_transaction.Show(ref m_exchange_result);

              _frm_shader.Dispose();

              if (_rc != DialogResult.OK)
              {

                return;
              }
            }
          }
        }
        //LTC 22-SEP-2016
        if ((!m_is_chips_operation) || (m_is_chips_operation && m_voucher_type == VoucherTypes.CardAdd && m_is_chips_sale_with_recharge == true))
        {
          if ((m_is_chips_operation && m_voucher_type == VoucherTypes.CardAdd && m_is_chips_sale_with_recharge == true))
          {
            _cashDeskId = WSI.Common.CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION.DRAW_01;
          }

          //DMR 01-AUG-2013: Adding Cash Draws    
          if (m_input_amount != null &&
              m_input_amount > 0 &&
              CashDeskDrawBusinessLogic.IsEnabledCashDeskDraws(Convert.ToInt16(_cashDeskId)))
          {
            CashDeskDraw _cash_desk_draw;
            String ErrorMesagge;

            _cash_desk_draw = new CashDeskDraw(Convert.ToInt16(_cashDeskId));

            if (CashDeskDrawBusinessLogic.ValidateCashDeskDrawConfiguration(_cash_desk_draw, out ErrorMesagge))
            {
              if (CashDeskDrawBusinessLogic.AskForParticipation(Convert.ToInt16(_cashDeskId)))
              {
                _dlg_participate_cash_desk_draw = frm_message.Show(Resource.String("STR_FRM_CASH_DESK_DRAW_PARTICIPATION").Replace("\\n", "\n"), Resource.String("STR_FRM_CASH_DESK_DRAW_PARTICIPATION_TITLE").Replace("\\r\\n", "\r\n"), MessageBoxButtons.YesNo, Cashier.MainForm);
                m_participate_in_cashdesk_draw = (_dlg_participate_cash_desk_draw == DialogResult.OK) ? ParticipateInCashDeskDraw.Yes : ParticipateInCashDeskDraw.No;
              }
            }
            else
            {
              _dlg_participate_cash_desk_draw = frm_message.Show(Resource.String("STR_FRM_CASH_DESK_DRAW_CONFIGURATION_ERROR").Replace("\\n", "\n"), Resource.String("STR_FRM_CASH_DESK_DRAW_PARTICIPATION_TITLE").Replace("\\r\\n", "\r\n"), MessageBoxButtons.OK, Cashier.MainForm);
              m_participate_in_cashdesk_draw = ParticipateInCashDeskDraw.No;
            }
          }
        }
        else
        {
          m_participate_in_cashdesk_draw = ParticipateInCashDeskDraw.No;
        }
      }

      if (m_is_chips_operation && m_voucher_type == VoucherTypes.CardAdd)
      {
        if (m_is_chips_sale_at_table_register)
        {
          // In chip sale register, don't need to show chips form to distribute.
          m_chips_amounts.ChipsAmount = m_total_redeemable + m_chips_promo_re;
        }
        else
        {
          if (!ShowChipsForm())
          {
            m_cancel = true;
            Misc.WriteLog("[FORM CLOSE] frm_amount_input (ok - not showchipsform)", Log.Type.Message);
            this.Close();

            return;
          }
        }
      }

      //Insert missing currencies
      if (m_voucher_type == VoucherTypes.OpenCash)
      {
        _iso_type.IsoCode = m_national_currency.CurrencyCode;
        _iso_type.Type = m_national_currency.Type;
        if (!m_currencies_amount.ContainsKey(_iso_type))
        {
          m_currencies_amount[_iso_type] = 0;
        }
      }

      if (m_voucher_type == VoucherTypes.CardRedeem || m_voucher_type == VoucherTypes.TicketRedeem)
      {
        Currency _cashier_current_balance;

        // RCI 25-JAN-2013: Check cashier balance
        _cashier_current_balance = 0;

        try
        {
          using (DB_TRX _db_trx = new DB_TRX())
          {
            _cashier_current_balance = CashierBusinessLogic.UpdateSessionBalance(_db_trx.SqlTransaction, CashierSessionInfo.CashierSessionId, 0);
          }
        }
        catch (Exception ex)
        {
          Log.Exception(ex);
          m_cancel = true;

          return;
        }

        if (m_total_to_pay > _cashier_current_balance)
        {
          frm_message.Show(Resource.String("STR_FRM_AMOUNT_INPUT_MSG_AMOUNT_EXCEED_BANK"),
                           Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this);
          m_cancel = true;

          return;
        }

        if (WSI.Common.TITO.Utils.IsTitoMode() && m_handpay != null)
        {
          // Getting the card data to assign to the handpay
          if (m_card_data.AccountId > 0)
          {
            CardData _card_data;
            _card_data = new CardData();
            try
            {
              using (DB_TRX _db_trx = new DB_TRX())
              {
                // Get card data info
                if (!CardData.DB_CardGetAllData(m_card_data.AccountId, _card_data, _db_trx.SqlTransaction))
                {
                  return;
                }
              }
            }
            catch (Exception _ex)
            {
              Log.Exception(_ex);
            }
          }
        }
        else
        {
        if (m_promo_lost.Balance.TotalBalance > 0 || m_promo_lost.Points > 0)
        {
            if (!CheckPromoLost(MultiPromos.AccountBalance.Zero))
          {
            return;
          }
        }
        }

        // RCI 23-NOV-2011: Request PIN if configured.
        if (!CashierBusinessLogic.RequestPin(this, Accounts.PinRequestOperationType.WITHDRAWAL, m_card_data, m_total_to_pay))
        {
          m_cancel = true;

          return;
        }

        // DLL & RCI 28-OCT-2013: Only check permissions here for CreditRedeem when PARTIAL.
        //                        TOTAL Redeem permissions are checked in uc_card - btn_substract_all_Click().
        if (m_cash_mode == CASH_MODE.PARTIAL_REDEEM || m_voucher_type == VoucherTypes.TicketRedeem)
        {
          if (m_dCheckPermissionsToRedeem != null)
          {
            if (!m_dCheckPermissionsToRedeem(m_card_data.AccountId, m_total_to_pay, m_cash_redeem_prize_amount, _list_permissions))
            {
              return;
            }
          }

          //Check if current user has all the permissions in the list.
          if (_list_permissions.Count > 0)
          {
            if (!ProfilePermissions.CheckPermissionList(_list_permissions,
                                                        ProfilePermissions.TypeOperation.RequestPasswd,
                                                        this,
                                                        0,
                                                        out _error_str))
            {
              return;
            }
          }
        }
      }

      m_cancel = false;
      DialogResult = DialogResult.OK;

      // Clean To Pay label
      lbl_total_to_pay.Text = "";
      lbl_currency_to_pay.Text = "";

      Misc.WriteLog("[FORM CLOSE] frm_amount_input (ok)", Log.Type.Message);
      this.Close();

    } // btn_ok_Click

    //------------------------------------------------------------------------------
    // PURPOSE: Checks if the customer has been registered in reception
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    private Boolean IsCustomerRegisteredInReception(CardData CardData)
    {
      // Checks if the customer doesn't visit the casino in this working date
      if (!GeneralParam.GetBoolean("MultiCurrencyExchange", "ReceptionRequired"))
      {
        return true;
      }

      if (!Entrance.CustomerHasVisitedToday(CardData.AccountId))
      {
        frm_message.Show(Resource.String("STR_RECEPTION_CUSTOMER_NOT_REGISTERED"), Resource.String("STR_APP_GEN_MSG_WARNING"),
                                           MessageBoxButtons.OK, MessageBoxIcon.Error, this);
        return false;
      }

      return true;
    }

    private Boolean ShowChipsForm()
    {
      frm_chips _f_chips;
      frm_yesno _shader;
      FeatureChips.ChipsOperation _chips_amount;
      Decimal _input_amount;
      Cage.ShowDenominationsMode _show_denominations_mode;
      String _chips_operation_iso_code;
      CurrencyIsoType _currency_iso_type;
      RoundingSaleChips _rounding_sale_chips;

      _shader = new frm_yesno();
      _shader.Opacity = 0.6;

      _f_chips = new frm_chips();

      try
      {
        _show_denominations_mode = (Cage.ShowDenominationsMode)GeneralParam.GetInt32("Cage", "ShowDenominations");
        _shader.Show(this);

        _chips_operation_iso_code = CurrencyExchange.GetNationalCurrency();

        if (WSI.Common.TITO.Utils.IsTitoMode() && uc_payment_method1.CurrencyExchange != null)
        {
          _chips_operation_iso_code = uc_payment_method1.CurrencyExchange.CurrencyCode;
        }

        _currency_iso_type = new CurrencyIsoType();

        if (uc_payment_method1.CurrencyExchange == null)
        {
          _currency_iso_type.IsoCode = m_national_currency.CurrencyCode;
          _currency_iso_type.Type = m_national_currency.Type;
        }
        else
        {
          _currency_iso_type.IsoCode = uc_payment_method1.CurrencyExchange.CurrencyCode;
          _currency_iso_type.Type = uc_payment_method1.CurrencyExchange.Type;
        }

        _input_amount = m_input_amount.HasValue ? m_input_amount.Value : 0;
        if (WSI.Common.TITO.Utils.IsTitoMode())
        {
          // For check take the amount with comissions applied
          if (m_voucher_type != VoucherTypes.ChipsPurchaseAmountInput && _currency_iso_type.Type != CurrencyExchangeType.CHECK)
          {
            _input_amount = m_input_amount_exchange;
          }
          _input_amount += m_chips_promo_re;
        }

        if (m_is_chips_sale_with_recharge && !WSI.Common.TITO.Utils.IsTitoMode())
        {
          _input_amount = m_total_redeemable;
          _input_amount += m_chips_promo_re;
        }

        if (m_voucher_type == VoucherTypes.ChipsPurchaseAmountInput)
        {
          _chips_amount = new FeatureChips.ChipsOperation(CashierSessionInfo.CashierSessionId, _input_amount,
                                                            CASHIER_MOVEMENT.CHIPS_PURCHASE);
          _chips_amount.ChipsAmount = _input_amount;
        }
        else
        {
          _chips_amount = new FeatureChips.ChipsOperation(CashierSessionInfo.CashierSessionId, 0, _input_amount, m_chips_promo_re, _chips_operation_iso_code,
                                                            CASHIER_MOVEMENT.CHIPS_SALE);

          if ((_show_denominations_mode == Cage.ShowDenominationsMode.HideAllDenominations && m_voucher_type != VoucherTypes.WinLoss)
             || (m_voucher_type == VoucherTypes.WinLoss && m_win_loss_mode == GamingTableBusinessLogic.GT_WIN_LOSS_MODE.BY_TOTALS))
          {
            _chips_amount.ChipsAmount = _input_amount;
          }
        }

        _chips_amount.SelectedGamingTable = m_chips_amounts.SelectedGamingTable;

        if (GamingTableBusinessLogic.IsEnableRoundingSaleChips())
        {
          if (m_is_chips_sale_with_recharge && !WSI.Common.TITO.Utils.IsTitoMode())
          {
            _chips_amount.RoundingSaleChips = new RoundingSaleChips(_chips_amount.ChipsAmount, true);

            if (_chips_amount.RoundingSaleChips.CalculateResult != RoundingSaleChips.GT_CALCULATE_ROUNDED_AMOUNT_RESULT.OK)
            {
              if (_chips_amount.RoundingSaleChips.CalculateResult == RoundingSaleChips.GT_CALCULATE_ROUNDED_AMOUNT_RESULT.AMOUNT_LESS_THAN_MINIMUM_DENOMINATION)
              {
                // show message when enter amount less than minimun denomination
                Log.Error(Resource.String("STR_ROUNDING_SALE_CHIPS_AMOUNT_LESS_THAN_MINIMUM_DENOMINATION"));
                Log.Error(Resource.String("STR_APP_GEN_MSG_ERROR"));
              }
              return false;
            }
            if (!GeneralParam.GetBoolean("GamingTables", "Cashier.Mode"))
            {
              _chips_amount.ChipsAmount = _chips_amount.RoundingSaleChips.RoundedAmount;
              _chips_amount.FillChipsAmount = _chips_amount.RoundingSaleChips.RoundedAmount;
            }

            _rounding_sale_chips = new RoundingSaleChips(m_card_data.AccountBalance.Account_ChipsSalesRemainingAmount + m_chips_amounts.RoundingSaleChips.RemainingAmount, true);
            _chips_amount.DevolutionRoundingSaleChips = _rounding_sale_chips.RoundedAmount;
            _chips_amount.RoundingSaleChips.RoundedAmount += _chips_amount.DevolutionRoundingSaleChips;
            _chips_amount.FillChipsAmount += _chips_amount.DevolutionRoundingSaleChips;
            _chips_amount.ChipsAmount += _chips_amount.DevolutionRoundingSaleChips;
          }
          else if (!m_is_chips_sale_with_recharge && m_is_chips_operation && !WSI.Common.TITO.Utils.IsTitoMode())
          {
            _rounding_sale_chips = new RoundingSaleChips(m_card_data.AccountBalance.TotalBalance + m_card_data.AccountBalance.Account_ChipsSalesRemainingAmount, true);
            if (_rounding_sale_chips.InputAmountMinusTaxes > _rounding_sale_chips.RoundedAmount
              && m_card_data.AccountBalance.TotalBalance < _rounding_sale_chips.RoundedAmount)
            {
              _chips_amount.DevolutionRoundingSaleChips = m_card_data.AccountBalance.Account_ChipsSalesRemainingAmount - _rounding_sale_chips.RemainingAmount;
            }
          }
        }

        if ((!GeneralParam.GetBoolean("GamingTables", "Cashier.Mode") &&
             _show_denominations_mode != Cage.ShowDenominationsMode.HideAllDenominations && m_voucher_type != VoucherTypes.WinLoss)
           || (m_voucher_type == VoucherTypes.WinLoss && m_win_loss_mode != GamingTableBusinessLogic.GT_WIN_LOSS_MODE.BY_TOTALS))
        {
          if (!_f_chips.Show(m_card_data, WSI.Common.TITO.Utils.IsTitoMode(), _shader, ref _chips_amount, m_is_chips_sale_with_recharge))
          {
            return false;
          }
        }

        m_chips_amounts = _chips_amount;

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {
        _f_chips.Dispose();
        _shader.Hide();
        _shader.Dispose();
      }

      return false;
    } // ShowChipsSaleForm

    //------------------------------------------------------------------------------
    // PURPOSE : Indicates if movement is in progress status
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean: True if the pending movements are in progress status.
    //
    //   NOTES :
    //
    private Boolean IsMovementInProgressYet()
    {
      return true;
    } // IsMovementYetInProgress

    private void btn_check_redeem_Click(object sender, EventArgs e)
    {
      if (m_promo_lost.Balance.TotalBalance > 0 || m_promo_lost.Points > 0)
      {
        if (!CheckPromoLost(MultiPromos.AccountBalance.Zero))
        {
          return;
        }
      }

      // RCI 23-NOV-2011: Request PIN if configured.
      if (!CashierBusinessLogic.RequestPin(this, Accounts.PinRequestOperationType.WITHDRAWAL, m_card_data, m_total_to_pay))
      {
        m_cancel = true;

        return;
      }

      m_cancel = false;
      m_out_check_redeem = true;

      // Clean To Pay label
      lbl_total_to_pay.Text = "";
      lbl_currency_to_pay.Text = "";

      DialogResult = DialogResult.OK;

      Misc.WriteLog("[FORM CLOSE] frm_amount_input (check_redeem)", Log.Type.Message);
      this.Close();

    } // btn_check_redeem_Click

    private Boolean CheckPromoLost(MultiPromos.AccountBalance AccountBalanceTemp)
    {
      String _message;
      String _credit_cancel;
      String _title;
      DialogResult _rc;

      _credit_cancel = "";

      if (m_promo_lost.Balance.TotalNotRedeemable > 0)
      {
        _credit_cancel += "\r\n" + Resource.String("STR_UC_CARD_USER_MSG_NO_REDEEM", (Currency)m_promo_lost.Balance.TotalNotRedeemable);
      }

      if (m_promo_lost.Balance.PromoRedeemable > 0)
      {
        _credit_cancel += "\r\n" + Resource.String("STR_UC_CARD_USER_MSG_REDEEM", (Currency)m_promo_lost.Balance.PromoRedeemable);
      }

      if (m_promo_lost.Points > 0)
      {
        _credit_cancel += "\r\n" + Resource.String("STR_UC_CARD_USER_MSG_POINT", (Points)m_promo_lost.Points);
      }

      if (AccountBalanceTemp.TotalNotRedeemable > 0)
      {
        _credit_cancel += "\r\n" + Resource.String("STR_UC_CARD_USER_MSG_NO_REDEEM", (Currency)AccountBalanceTemp.TotalNotRedeemable);
      }

      if (AccountBalanceTemp.PromoRedeemable > 0)
      {
        _credit_cancel += "\r\n" + Resource.String("STR_UC_CARD_USER_MSG_REDEEM", (Currency)AccountBalanceTemp.PromoRedeemable);
      }

      _message = Resource.String("STR_UC_CARD_USER_MSG_ACTION_REDEEM", _credit_cancel);
      _message = _message.Replace("\\r\\n", "\r\n");

      if (m_cash_mode == CASH_MODE.PARTIAL_REDEEM)
      {
        _title = Resource.String("STR_UC_CARD_BTN_CREDIT_REDEEM_PARTIAL");
      }
      else
      {
        _title = Resource.String("STR_UC_CARD_BTN_CREDIT_REDEEM_TOTAL");
      }

      // Show confirmation message for losing non redeemable credit when CardRedeem
      _rc = frm_message.Show(_message, _title, MessageBoxButtons.YesNo, MessageBoxIcon.Warning, this);
      if (_rc != DialogResult.OK)
      {
        m_cancel = true;

        return false;
      }

      return true;
    } // CheckPromoLost

    //------------------------------------------------------------------------------
    // PURPOSE : Has any money.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean: true if has any total more than zero.
    //
    //   NOTES :
    //
    private Boolean HasAnyTotalMoreThanZero()
    {
      Boolean _result;

      _result = false;
      foreach (KeyValuePair<CageDenominationsItems.CageDenominationKey, CageDenominationsItems.CageDenominationsItem> _cur_currency in m_cage_amounts)
      {
        if (_cur_currency.Value.TotalAmount > 0)
        {
          _result = true;
        }
      }

      return _result;
    } // HasAnyTotalMoreThanZero

    //------------------------------------------------------------------------------
    // PURPOSE : Has any currency not allowed fill out.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean: false if has any total more than zero.
    //
    //   NOTES :
    //
    private Boolean CurrencyAllowedFillOutStatus()
    {
      if (m_voucher_type != VoucherTypes.StatusCash)
      {
        foreach (KeyValuePair<CageDenominationsItems.CageDenominationKey, CageDenominationsItems.CageDenominationsItem> _cur_currency in m_cage_amounts)
        {
          if (!_cur_currency.Value.AllowedFillOut)
          {
            return false;
          }
        }
      }

      return true;
    } // CurrencyAllowedFillOutStatus

    //------------------------------------------------------------------------------
    // PURPOSE : Has any currency Tickets tito.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean: true if has any tickets.
    //
    //   NOTES :
    //
    private Boolean HasTicketsTito()
    {
      if (m_cage_amounts != null)
      {
        foreach (KeyValuePair<CageDenominationsItems.CageDenominationKey, CageDenominationsItems.CageDenominationsItem> _cur_currency in m_cage_amounts)
        {
          if (_cur_currency.Value.HasTicketsTito)
          {
            return true;
          }
        }
      }

      return false;
    } // CurrencyAllowedFillOutStatus

    //------------------------------------------------------------------------------
    // PURPOSE : Handle the click on the intro button, which confirms the amount on the
    //           numeric pad.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void btn_intro_Click(object sender, EventArgs e)
    {
      Decimal _input_amount = 0;
      Boolean _promo_validated = true;
      Int32 _max_allowed_int;
      Currency _max_allowed;
      CurrencyIsoType _iso_type;
      Decimal _tmp_decimal;
      Boolean _previous_status;
      Currency _cashier_current_balance;
      Currency _card_deposit;
      Boolean _apply_comissions_exchange;

      _iso_type = new CurrencyIsoType();

      _previous_status = btn_ok.Enabled;
      btn_ok.Enabled = false;
      btn_check_redeem.Enabled = false;
      btn_apply_tax.Enabled = false;
      btn_apply_tax.Visible = false;
      _apply_comissions_exchange = true;

      if (m_handpay == null)
      {
        m_is_apply_tax = !Tax.ApplyTaxCollectionTicketPayment();
      }
      m_voucher_currency_exchange = uc_payment_method1.CurrencyExchange;
      _max_allowed_int = GeneralParam.GetInt32("Cashier", "MaxAllowedDeposit", 0);
      //Int32.TryParse(ReadGeneralParams("Cashier", "MaxAllowedDeposit"), out _max_allowed_int);

      //ESE 27-SEP-2016
      if (m_voucher_type == VoucherTypes.StatusCash && Misc.IsBankCardCloseCashEnabled())
      {
        Withdrawal.GetPinpanMovementsCutOff(CashierSessionInfo.CashierSessionId);
        Misc.PrintBankCardSection_StatusCash = true;
      }

      if (m_cage_enabled && m_voucher_type != VoucherTypes.StatusCash)
      {
        if (dgv_common.SelectedRows.Count == 0)
        {
          frm_message.Show(Resource.String("STR_DENOMINATION_UNSELECTED"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this);

          return;
        }
        if (m_voucher_type != VoucherTypes.OpenCash && String.IsNullOrEmpty(txt_amount.Text))
        {
          if (!m_from_clear)
          {
            set_dgv_common_next_row();
          }

          if (m_voucher_type == VoucherTypes.AmountRequest && get_cage_total_value > _max_allowed_int)
          {
            btn_ok.Enabled = _previous_status;

            return;
          }

          if ((get_cage_total_value > 0 || HasAnyTotalMoreThanZero() || HasTicketsTito()) && CurrencyAllowedFillOutStatus())
          {
            btn_ok.Enabled = true;
          }
          else
          {
            if (!m_from_clear)
            {
              btn_ok.Enabled = _previous_status;
            }
          }
          if (get_cage_total_value > _max_allowed_int && _max_allowed_int != 0)
          {
            btn_ok.Enabled = false;
          }
          if (!m_from_clear)
          {
            return;
          }
        }
        Decimal.TryParse(txt_amount.Text, out _input_amount);

        switch (m_voucher_type)
        {
          case VoucherTypes.OpenCash:
          case VoucherTypes.FillIn:
            // comprobar que no sea una apertura de caja
            if ((m_dt_cage_pending_movements != null && m_dt_cage_pending_movements.Rows.Count > 0 || m_cage_automode || m_closing_stock != null) && m_voucher_type != VoucherTypes.OpenCash)
            {
              if (!m_from_clear)
              {
                UpdateDenomination(_input_amount);
                set_dgv_common_next_row();
              }
            }
            break;

          case VoucherTypes.MBCardCashDeposit: // JMV
          case VoucherTypes.FillOut:
          case VoucherTypes.CloseCash:
          case VoucherTypes.AmountRequest:
          case VoucherTypes.DropBoxCount:
          case VoucherTypes.WinLoss:
            if (!m_from_clear)
            {
              UpdateDenomination(_input_amount);
              set_dgv_common_next_row();
            }
            break;
        }

        UpdateTotalCageAmount();

        _input_amount = get_cage_total_value;

      }
      else
      {
        Decimal.TryParse(txt_amount.Text, out _input_amount);
      }

      m_input_amount_exchange = (Currency)_input_amount;

      if (uc_payment_method1.CurrencyExchange != null)
      {
        m_input_amount_exchange.CurrencyIsoCode = uc_payment_method1.CurrencyExchange.CurrencyCode;
      }
      else
      {
        m_input_amount_exchange.CurrencyIsoCode = CurrencyExchange.GetNationalCurrency();
      }


      if (m_voucher_type != VoucherTypes.CardReplacement)
      {
        //DLL 25-JUN-2013: Apply currency exchange
        m_exchange_result = null;
      }

      if (m_voucher_type == VoucherTypes.CardAdd &&
        m_with_currency_exchange && (uc_payment_method1.CurrencyExchange.Type != CurrencyExchangeType.CURRENCY || uc_payment_method1.CurrencyExchange.CurrencyCode != m_national_currency.CurrencyCode))
      {
        if (_input_amount <= 0)
        {
          frm_message.Show(Resource.String("STR_FRM_AMOUNT_INPUT_MSG_ZERO_AMOUNT"), Resource.String("STR_APP_GEN_MSG_ERROR"),
                           MessageBoxButtons.OK, MessageBoxIcon.Error, this);

          return;
        }

        // DHA 30-JUN-2015
        if (!m_is_chips_sale_at_table_register)
        {
          _card_deposit = Accounts.CardDepositToPay(m_card_data);
        }
        else
        {
          _card_deposit = 0;
        }

        // Get exchange value and set iso code
        m_input_amount_exchange = (Currency)_input_amount;
        m_input_amount_exchange.CurrencyIsoCode = uc_payment_method1.CurrencyExchange.CurrencyCode;

        // No apply exchange comissions to chips sales
        if (WSI.Common.TITO.Utils.IsTitoMode() && m_voucher_type == VoucherTypes.CardAdd && m_chips_amounts != null &&
          !(uc_payment_method1.CurrencyExchange.Type == CurrencyExchangeType.CARD || uc_payment_method1.CurrencyExchange.Type == CurrencyExchangeType.CHECK))
        {
          _apply_comissions_exchange = false;
        }

        if (!uc_payment_method1.CurrencyExchange.ApplyExchange(_input_amount, out m_exchange_result, false, _card_deposit, _apply_comissions_exchange))
        {
          frm_message.Show(Resource.String("STR_FRM_AMOUNT_INPUT_MSG_CURRENCY_EXCHANGE"), Resource.String("STR_APP_GEN_MSG_WARNING"),
                           MessageBoxButtons.OK, MessageBoxIcon.Warning, this);

          return;
        }

        _input_amount = m_exchange_result.NetAmount;
      }

      // RCI 18-NOV-2010: For update promo input values area.
      m_input_amount = (Currency)_input_amount;

      m_voucher_amount = 0;
      m_out_amount = 0;

      if (m_voucher_type == VoucherTypes.CardAdd)
      {
        if (m_selected_promo_id > 0)
        {
          // Checks that currently selected promotion can still be applied
          _promo_validated = ValidatePromotion((Currency)_input_amount, m_promo_num_tokens, true, out m_promo_reward_amount, out m_promo_won_lock);
        }

        // SGB 09-OCT-2014 Fixed Bug WIG-1476: Check if input amount is bigger than cashier current balance (in chips).
        if (m_is_chips_operation && Utils.IsTitoMode() &&
            (GeneralParam.GetInt32("Cage", "ShowDenominations") == (Int32)Cage.ShowDenominationsMode.HideAllDenominations && m_voucher_type != VoucherTypes.WinLoss)
              || (m_voucher_type == VoucherTypes.WinLoss && m_win_loss_mode == GamingTableBusinessLogic.GT_WIN_LOSS_MODE.BY_TOTALS))
        {
          _cashier_current_balance = 0;
          try
          {
            using (DB_TRX _db_trx = new DB_TRX())
            {
              if (m_selected_promo_data.credit_type == ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE)
              {
                String _iso_code;
                if (WSI.Common.TITO.Utils.IsTitoMode())
                {
                  _iso_code = m_input_amount_exchange.CurrencyIsoCode;
                }
                else
                {
                  _iso_code = CurrencyExchange.GetNationalCurrency();
                }
                _cashier_current_balance = WSI.Common.Cashier.UpdateSessionBalance(_db_trx.SqlTransaction, CashierSessionInfo.CashierSessionId, 0,
                                                                                       _iso_code, CurrencyExchangeType.CASINO_CHIP_RE);
              }
            }
          }
          catch (Exception ex)
          {
            Log.Exception(ex);

            return;
          }

          if (m_input_amount > _cashier_current_balance && !FeatureChips.IsChipsSaleByTicket)
          {
            frm_message.Show(Resource.String("STR_FRM_AMOUNT_INPUT_MSG_CHIPS_EXCEED_BANK"), Resource.String("STR_APP_GEN_MSG_ERROR"),
                             MessageBoxButtons.OK, MessageBoxIcon.Error, this);

            is_voucher_loaded = false;

            return;
          }
        }
      }

      if (m_voucher_type == VoucherTypes.FillIn || m_voucher_type == VoucherTypes.AmountRequest)
      {
        // RCI 10-NOV-2010: Check MaxAllowedDeposit.

        _max_allowed = _max_allowed_int;

        if (_max_allowed > 0 && _input_amount > _max_allowed)
        {
          web_browser.Navigate("");
          is_voucher_loaded = false;
          lbl_total_to_pay.Text = "";
          lbl_currency_to_pay.Text = "";
          txt_amount.Text = "";

          if (!m_show_msg)
          {
            frm_message.Show(Resource.String("STR_FRM_AMOUNT_INPUT_MSG_MAX_ALLOWED", _max_allowed), Resource.String("STR_APP_GEN_MSG_WARNING"), MessageBoxButtons.OK, MessageBoxIcon.Warning, this);
            m_show_msg = true;
          }
          return;
        }
      }

      if (m_voucher_type == VoucherTypes.CloseCash || m_voucher_type == VoucherTypes.OpenCash || m_voucher_type == VoucherTypes.DropBoxCount
          || m_voucher_type == VoucherTypes.FillIn || m_voucher_type == VoucherTypes.FillOut
          || m_voucher_type == VoucherTypes.CardRedeem || m_voucher_type == VoucherTypes.MBCardChangeLimit || m_voucher_type == VoucherTypes.MBCardCashDeposit)
      {
        if (!m_cage_enabled)
        {
          if (txt_amount.Text == "")
          {
            _input_amount = -1;
          }
        }


        if (m_voucher_type == VoucherTypes.OpenCash || m_voucher_type == VoucherTypes.FillIn || m_voucher_type == VoucherTypes.FillOut)
        {
          _iso_type.IsoCode = m_national_currency.CurrencyCode;
          _iso_type.Type = m_national_currency.Type;
          if (uc_payment_method1.CurrencyExchange != null)
          {
            _iso_type.IsoCode = uc_payment_method1.CurrencyExchange.CurrencyCode;
            _iso_type.Type = uc_payment_method1.CurrencyExchange.Type;
          }
          m_currencies_amount[_iso_type] = _input_amount;

        }
      }

      if (m_voucher_type == VoucherTypes.WinLoss)
      {
        _iso_type.IsoCode = m_national_currency.CurrencyCode;
        _iso_type.Type = m_national_currency.Type;
        if (uc_payment_method1.CurrencyExchange != null)
        {
          _iso_type.IsoCode = uc_payment_method1.CurrencyExchange.CurrencyCode;
          _iso_type.Type = uc_payment_method1.CurrencyExchange.Type;
        }
        m_currencies_amount[_iso_type] = _input_amount;

      }

      // SGB 09-10-2014 Fixed Bug WIG-1476: Check if input amount is bigger than cashier current balance.
      if (m_voucher_type == VoucherTypes.ChipsSaleAmountInput || m_voucher_type == VoucherTypes.ChipsPurchaseAmountInput)
      {
        RoundingSaleChips _rounding_sales_chips;

        if (m_voucher_type == VoucherTypes.ChipsSaleAmountInput)
        {
          _rounding_sales_chips = new RoundingSaleChips(_input_amount, true);

          if (_rounding_sales_chips.CalculateResult == RoundingSaleChips.GT_CALCULATE_ROUNDED_AMOUNT_RESULT.AMOUNT_LESS_THAN_MINIMUM_DENOMINATION)
          {
            // show message when enter amount less than minimun denomination
            frm_message.Show(Resource.String("STR_ROUNDING_SALE_CHIPS_AMOUNT_LESS_THAN_MINIMUM_DENOMINATION"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this);

            return;
          }

          if (m_input_amount != _rounding_sales_chips.RoundedAmount)
          {
            if (DialogResult.OK != frm_message.Show(Resource.String("STR_ROUNDING_SALE_CHIPS_AMOUNT_ROUNDED_TO_MINIMUM_DENOMINATION", (Currency)_rounding_sales_chips.RoundedAmount), Resource.String("STR_APP_GEN_MSG_WARNING"), MessageBoxButtons.OKCancel, MessageBoxIcon.Error, this))
            {
              return;
            }

            m_input_amount = _rounding_sales_chips.RoundedAmount;
          }
        }

        //String _iso_code;
        //CurrencyExchangeType? _ex_type;
        //String _error_msg;

        // TODO: REVISAR DHA... no tiene mucho sentido
        //if (Chips.IsIntegratedChipAndCreditOperations(m_card_data.IsVirtualCard) ||
        //    m_voucher_type == VoucherTypes.ChipsSaleAmountInput ||
        //    (Utils.IsTitoMode() && m_voucher_type == VoucherTypes.ChipsPurchaseAmountInput))
        //{
        //  if (m_voucher_type == VoucherTypes.ChipsSaleAmountInput)
        //  {
        //    _iso_code = Cage.CHIPS_ISO_CODE;
        //    _ex_type = CurrencyExchangeType.CASINOCHIP;
        //    _error_msg = Resource.String("STR_FRM_AMOUNT_INPUT_MSG_CHIPS_EXCEED_BANK");
        //  }
        //  else
        //  {
        //    _iso_code = "";
        //    _ex_type = null;
        //    _error_msg = Resource.String("STR_FRM_AMOUNT_INPUT_MSG_AMOUNT_EXCEED_BANK");
        //  }

        //  _cashier_current_balance = 0;
        //  try
        //  {
        //    using (DB_TRX _db_trx = new DB_TRX())
        //    {
        //      _cashier_current_balance = WSI.Common.Cashier.UpdateSessionBalance(_db_trx.SqlTransaction, CashierSessionInfo.CashierSessionId, 0, _iso_code, _ex_type);
        //    }
        //  }
        //  catch (Exception ex)
        //  {
        //    Log.Exception(ex);

        //    return;
        //  }

        //  if (!GeneralParam.GetBoolean("GamingTables", "Cashier.Mode") && m_input_amount > _cashier_current_balance)
        //  {
        //    frm_message.Show(_error_msg, Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this);

        //    is_voucher_loaded = false;

        //    return;
        //  }
        //}

        if (m_input_amount <= 0)
        {
          frm_message.Show(Resource.String("STR_FRM_AMOUNT_INPUT_MSG_ZERO_AMOUNT"),
            Resource.String("STR_APP_GEN_MSG_ERROR"),
            MessageBoxButtons.OK,
            MessageBoxIcon.Error,
            ParentForm);

          return;
        }

        if (!ShowChipsForm())
        {
          m_cancel = true;
        }
        else
        {
          DialogResult = DialogResult.OK;
        }

        Misc.WriteLog("[FORM CLOSE] frm_amount_input (intro)", Log.Type.Message);
        this.Close();

        return;
      }

      if (_promo_validated)
      {
        // Generate voucher in preview mode
        PreviewVoucher((Currency)_input_amount, m_is_apply_tax);

        if (is_voucher_loaded)
        {
          if (m_cage_enabled)
          {
            btn_ok.Enabled = CurrencyAllowedFillOutStatus();
          }
          else
          {
            btn_ok.Enabled = true;
            btn_ok.Focus();
          }
        }
        else
        {
          m_input_amount = null;
        }
      }
      else
      {
        m_input_amount = null;
        // RCI 19-NOV-2010: Don't reset m_promo_num_tokens.
        //m_promo_num_tokens = 0;

        web_browser.Navigate("");
        is_voucher_loaded = false;
        lbl_total_to_pay.Text = "";
        lbl_currency_to_pay.Text = "";
      }

      UpdatePromoInputValues();

      // If did not allowing money entries then enter button is disabled
      if (m_allowed_entries)
      {
        if (!m_cage_enabled)
        {
          txt_amount.Text = "";
        }
        else
        {
          if (m_voucher_type != VoucherTypes.StatusCash)
          {
            Decimal.TryParse(dgv_common.SelectedRows[0].Cells[GRID_COLUMN_CAGE_AMOUNTS_DENOMINATION].Value.ToString(), out _tmp_decimal);
            switch ((Int32)_tmp_decimal)
            {
              case Cage.COINS_CODE:
              case Cage.BANK_CARD_CODE:
              case Cage.CHECK_CODE:
                txt_amount.Text = dgv_common.SelectedRows[0].Cells[GRID_COLUMN_CAGE_AMOUNTS_TOTAL].Value.ToString().Trim();

                break;

              case Cage.TICKETS_CODE:
                txt_amount.Text = dgv_common.SelectedRows[0].Cells[GRID_COLUMN_CAGE_AMOUNTS_UN].Value.ToString().Trim();

                break;

              default:
                if (m_voucher_type == VoucherTypes.WinLoss && m_win_loss_mode == GamingTableBusinessLogic.GT_WIN_LOSS_MODE.BY_TOTALS)
                {
                  txt_amount.Text = dgv_common.SelectedRows[0].Cells[GRID_COLUMN_CAGE_AMOUNTS_TOTAL].Value.ToString().Trim();
                }
                else
                {
                  txt_amount.Text = dgv_common.SelectedRows[0].Cells[GRID_COLUMN_CAGE_AMOUNTS_UN].Value.ToString().Trim();
                }
                break;
            }
          }
        }
      }
      else
      {
        btn_intro.Enabled = m_allowed_entries;
      }

      if (m_voucher_type == VoucherTypes.WinLoss
        && m_win_loss_mode == GamingTableBusinessLogic.GT_WIN_LOSS_MODE.BY_TOTALS
        && m_win_loss_amount_entry_screen_is_open)
      {

        btn_ok_Click(null, null);
      }

    } // btn_intro_Click

    //DLL 25-JUN-2013:
    //------------------------------------------------------------------------------
    // PURPOSE : indicate when closing PreviewVoucher
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //        - True: Close Preview
    //        - False: No close Preview
    //   NOTES :
    private Boolean ExitPreview()
    {
      CurrencyIsoType _iso_type = new CurrencyIsoType();
      if (m_currencies_amount.Count == 0)
      {
        web_browser.Navigate("");
        return true;
      }

      _iso_type.IsoCode = m_national_currency.CurrencyCode;
      _iso_type.Type = m_national_currency.Type;
      if (uc_payment_method1.CurrencyExchange != null)
      {
        _iso_type.IsoCode = uc_payment_method1.CurrencyExchange.CurrencyCode;
        _iso_type.Type = uc_payment_method1.CurrencyExchange.Type;
      }

      m_currencies_amount.Remove(_iso_type);

      if (m_currencies_amount.Count == 0)
      {
        web_browser.Navigate("");
        return true;
      }
      return false;

    } // ExitPreview

    //------------------------------------------------------------------------------
    // PURPOSE : Generate a preview version of the voucher to be generated.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    private void PreviewVoucher(Currency VoucherAmount, Boolean IsApplyTaxes)
    {
      SqlConnection sql_conn;
      SqlTransaction sql_trx;
      String _voucher_file_name;
      Currency _cashier_current_balance;
      Currency _card_deposit;
      ArrayList _voucher_list;
      Decimal _initial_balance;
      Boolean _has_error;
      //Int32 _index;
      String _msg;
      Boolean _has_tickets_tito;
      CurrencyExchange _tmp_currency_exchange;
      Dictionary<CurrencyIsoType, Decimal> _currencies_amount_order_to_config;
      //02-AGO-2016 ESE
      ArrayList _aux_voucher_list;
      Boolean _hide_voucher_recharge;
      CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION _cashDeskId;
      JunketsBusinessLogic _junket;

      _hide_voucher_recharge = Misc.IsVoucherModeTV();

      _has_error = false;
      _voucher_file_name = "";

      lbl_total_to_pay.Text = "";
      lbl_currency_to_pay.Text = "";

      _cashDeskId = CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION.DRAW_00; //LTC 22-SEP-2016

      _tmp_currency_exchange = (uc_payment_method1.CurrencyExchange == null) ? m_national_currency : uc_payment_method1.CurrencyExchange;
      _has_tickets_tito = HasTicketsTito();

      _currencies_amount_order_to_config = CurrencyExchange.OrderCurrenciesFromCurrencyConfiguration(m_currencies_amount);

      _junket = null;

      if (!(m_voucher_type == VoucherTypes.OpenCash
            || m_voucher_type == VoucherTypes.CloseCash
            || m_voucher_type == VoucherTypes.DropBoxCount
            || m_voucher_type == VoucherTypes.StatusCash
            || m_voucher_type == VoucherTypes.CardReplacement
            || (m_voucher_type == VoucherTypes.CardRedeem && m_cash_mode == CASH_MODE.TOTAL_REDEEM)
            || m_voucher_type == VoucherTypes.TicketRedeem
            || m_voucher_type == VoucherTypes.WinLoss
            ))
      {
        m_voucher_amount = VoucherAmount;

        if ((m_voucher_amount <= 0 && !_has_tickets_tito && m_voucher_type != VoucherTypes.MBCardSetLimit) && !(m_voucher_type == VoucherTypes.CardAdd && m_promo_reward_amount > 0)
          // Allow open cash with first fill in with amount 0
          && !(m_voucher_amount == 0 && m_cashier_session_info != null && m_cashier_session_info.CashierSessionId == 0 && m_voucher_type == VoucherTypes.FillIn))
        {
          if (!m_cage_enabled)
          {
            _msg = (m_voucher_amount == 0) ? Resource.String("STR_FRM_AMOUNT_INPUT_MSG_ZERO_AMOUNT") : Resource.String("STR_FRM_AMOUNT_INPUT_MSG_CASH_DESK_CLOSE_COLLECTED_AMOUNT");
            frm_message.Show(_msg, Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this);
          }
          is_voucher_loaded = false;

          _has_error = true;
          // Avoiding the unnecesary refresh in case of error
          if (ExitPreview() || _has_error)
          {
            return;
          }
        }
      }

      if (m_voucher_type == VoucherTypes.CloseCash || m_voucher_type == VoucherTypes.OpenCash || m_voucher_type == VoucherTypes.DropBoxCount)
      {
        if (VoucherAmount < 0)
        {
          frm_message.Show(Resource.String("STR_FRM_AMOUNT_INPUT_MSG_CASH_DESK_CLOSE_COLLECTED_AMOUNT"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this);
          is_voucher_loaded = !ExitPreview();

          return;
        }

        if (VoucherAmount != 0)
        {
          m_voucher_amount = VoucherAmount;
        }
      }

      sql_conn = null;
      sql_trx = null;

      try
      {
        // Get Sql Connection 
        sql_conn = WSI.Common.WGDB.Connection();
        sql_trx = sql_conn.BeginTransaction();

        // Show voucher
        switch (m_voucher_type)
        {
          case VoucherTypes.CardAdd:
            {
              Currency _cash_in_split1;
              Currency _cash_in_split2;
              Currency _redeemable;
              Currency _prize_coupon;
              Currency _nr2;
              Decimal _dec_nr2;
              Decimal _nr2_pct;
              TYPE_SPLITS _splits;
              MultiPromos.AccountBalance _to_add;
              Decimal _promo_re;
              Decimal _promo_nr;
              Decimal _promo_point;
              Currency _cash_in_tax_split1;
              Currency _cash_in_tax_split2;
              // DHA 01-MAR-2016: Winpot - Provisioning Tax
              Currency _tax_provisions;
              Currency _tax_custody; // LTC 14-ABR-2016

              if (!m_is_chips_sale_at_table_register)
              {
                // RCI 24-SEP-2010
                _card_deposit = Accounts.CardDepositToPay(m_card_data);
              }
              else
              {
                _card_deposit = 0;
              }

              if (!Split.ReadSplitParameters(out _splits))
              {
                return;
              }

              // Calculate cash in splits
              if (!m_is_chips_sale_with_recharge)
              {
                _cash_in_split1 = Math.Round(m_voucher_amount * _splits.company_a.cashin_pct / 100, 2, MidpointRounding.AwayFromZero);
                _cash_in_split2 = m_voucher_amount - _cash_in_split1;
              }
              else
              {
                _cash_in_split1 = Math.Round(m_voucher_amount * _splits.company_a.chips_sale_pct / 100, 2, MidpointRounding.AwayFromZero);
                _cash_in_split2 = m_voucher_amount - _cash_in_split1;
              }

              _redeemable = _cash_in_split1 + _cash_in_split2;

              // RCI 31-DEC-2013: CashIn Tax
              _cash_in_tax_split1 = 0;
              _cash_in_tax_split2 = 0;

              if (!m_is_chips_sale_with_recharge)
              {
                if (GeneralParam.GetBoolean("Cashier", "CashInTax.Enabled"))
                {
                  _cash_in_tax_split1 = Accounts.CalculateCashInTax(_cash_in_split1);

                  _cash_in_split1 -= _cash_in_tax_split1;
                  _redeemable -= _cash_in_tax_split1;

                  if (GeneralParam.GetBoolean("Cashier", "CashInTax.AlsoApplyToSplitB"))
                  {
                    _cash_in_tax_split2 = Accounts.CalculateCashInTax(_cash_in_split2);

                    _cash_in_split2 -= _cash_in_tax_split2;
                  }
                }
              }
              else
              {
                if (GeneralParam.GetBoolean("GamingTables", "CashInTax.Enabled"))
                {
                  _cash_in_tax_split1 = Accounts.CalculateCashInTaxGamingTables(_cash_in_split1);

                  _cash_in_split1 -= _cash_in_tax_split1;
                  _redeemable -= _cash_in_tax_split1;

                  if (GeneralParam.GetBoolean("GamingTables", "CashInTax.AlsoApplyToSplitB"))
                  {
                    _cash_in_tax_split2 = Accounts.CalculateCashInTaxGamingTables(_cash_in_split2);

                    _cash_in_split2 -= _cash_in_tax_split2;
                  }
                }
              }

              // LTC 14-ABR-2016
              _tax_custody = 0;

              switch (Misc.ApplyCustodyConfigurationCalculateRatio(m_is_chips_sale_with_recharge))
                {
                    case Misc.TaxCustodyMode.TaxOnSplit1:// CodereMode
                      _tax_custody = Accounts.CalculateTaxCustody(_cash_in_split1, m_is_chips_sale_with_recharge);

                      _cash_in_split1 -= _tax_custody;
                      _redeemable -= _tax_custody;
                      break;

                    case Misc.TaxCustodyMode.TaxOnDeposit:
                      Decimal _cash_in_spit1_temp;
                      _cash_in_spit1_temp = _cash_in_split1;
                      if (m_is_chips_sale_with_recharge)
                      {
                        _cash_in_split1 = Math.Round((_cash_in_split1 / (1 + Misc.GamingTablesTaxCustodyPct() / 100)), 2);
                      }
                      else
                      {
                        _cash_in_split1 = Math.Round((_cash_in_split1 / (1 + Misc.TaxCustodyPct() / 100)), 2);
                      }
                      _tax_custody = Accounts.CalculateTaxCustody(_cash_in_split1, m_is_chips_sale_with_recharge);
                      _cash_in_split1 = _cash_in_spit1_temp - _tax_custody; // ensure we have the whole initial cashin_split1

                      _redeemable -= _tax_custody;
                      break;

                case Misc.TaxCustodyMode.None:
                default:

                  break;
                  }

              // DHA 01-MAR-2016: Winpot - Provisioning Tax
              _tax_provisions = 0;
              if (Misc.IsTaxProvisionsEnabled())
              {
                _tax_provisions = Accounts.CalculateTaxProvisions(_cash_in_split1);

                _cash_in_split1 -= _tax_provisions;
                _redeemable -= _tax_provisions;
              }

              _prize_coupon = 0;

              // RRB 03-OCT-2012 Always create the "PrizeCoupon" promotion when A/B exists.

              if (_cash_in_split2 > 0)
              {
                _redeemable = _cash_in_split1;
                // DHA 21-AUG-2013: If CashDeskDraw is Enabled prize cupon is 0
                // DRV & RCI 30-JUL-2014: Integrated operations never uses CashDeskDraw, so _prize_coupon must be always set.
                if (m_is_chips_sale_with_recharge || (!CashDeskDrawBusinessLogic.IsEnabledCashDeskDraws() && !Voucher.IsTerminalDraw()))
                {
                  _prize_coupon = _cash_in_split2;
                }
              }

              _nr2 = 0;
              if (!m_is_chips_operation && !Voucher.IsTerminalDraw())
              {
                if (!WSI.Common.Cashier.GetNR2(m_card_data.AccountId, m_voucher_amount, out _dec_nr2, out _nr2_pct))
                {
                  return;
                }
                _nr2 = _dec_nr2;
              }

              _promo_re = 0;
              _promo_nr = 0;
              _promo_point = 0;
              m_chips_promo_re = 0;

              if (m_selected_promo_data != null) // first time
              {
                switch (m_selected_promo_data.credit_type)
                {
                  case ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE:
                    _promo_re = m_promo_reward_amount;
                    m_chips_promo_re = _promo_re;
                    break;
                  case ACCOUNT_PROMO_CREDIT_TYPE.POINT:
                    _promo_point = m_promo_reward_amount;
                    break;
                  case ACCOUNT_PROMO_CREDIT_TYPE.NR1:
                  case ACCOUNT_PROMO_CREDIT_TYPE.NR2:
                    _promo_nr = m_promo_reward_amount;
                    break;
                  case ACCOUNT_PROMO_CREDIT_TYPE.UNKNOWN:
                  default:
                    break;
                }
              }

              // DRV 15-JUL-2014: Needed for IntegratedChipOperation.
              m_total_redeemable = _redeemable + _cash_in_split2;

              _to_add = new MultiPromos.AccountBalance(_redeemable, _prize_coupon + _promo_re, _promo_nr + _nr2);
              //_promo_point

              _voucher_list = null;

              if (m_with_currency_exchange)
              {

                _voucher_list = VoucherBuilder.CurrencyExchange(m_card_data.VoucherAccountInfo(), m_card_data.AccountId, _splits, m_exchange_result,
                                                                     uc_payment_method1.CurrencyExchange.Type,
                                                                     CurrencyExchange.GetNationalCurrency(), 0, PrintMode.Preview, sql_trx);

                if (uc_payment_method1.CurrencyExchange.Type == CurrencyExchangeType.CREDITLINE)
                {
                  //Temporarily change spent to include new amount for Voucher preview
                  m_creditline_spent = m_card_data.CreditLineData.SpentAmount;
                  m_card_data.CreditLineData.SpentAmount = m_creditline_spent + m_exchange_result.InAmount;


                  //operation id 0 - Preview do not save in DB
                  VoucherBuilder.GetVoucherCreditLineGetMarker(_voucher_list, m_card_data.VoucherAccountInfo(), m_exchange_result.InAmount + m_exchange_result.CardPrice, m_card_data.CreditLineData, _splits, 0, PrintMode.Preview, sql_trx);

                  //Reset Spent to original value after preview
                  m_card_data.CreditLineData.SpentAmount = m_creditline_spent;
                }

                //02-AGO-2016 ESE
                if (_hide_voucher_recharge)
                {
                  _aux_voucher_list = _voucher_list;
                  _voucher_list = new ArrayList();
                }

                _to_add.PromoNotRedeemable += m_exchange_result.NR2Amount;
              }

              Currency _total_base_tax_split2;
              Currency _total_tax_split2;

              Accounts.CalculateSplit2Tax(_card_deposit, _cash_in_split2, out _total_base_tax_split2, out _total_tax_split2);

              VoucherCashIn.InputDetails _details = new VoucherCashIn.InputDetails();

              _details.InitialBalance = new MultiPromos.PromoBalance(m_card_data.AccountBalance, 0);
              _details.FinalBalance = new MultiPromos.PromoBalance(m_card_data.AccountBalance + _to_add, _promo_point);
              _details.CashIn1 = _cash_in_split1;
              _details.CashIn2 = _cash_in_split2;
              _details.CardPrice = _card_deposit;
              _details.CashInTaxSplit1 = _cash_in_tax_split1;
              _details.CashInTaxSplit2 = _cash_in_tax_split2;
              _details.CashInTaxCustody = _tax_custody; // LTC 14-ABR-2016
              _details.TotalBaseTaxSplit2 = _total_base_tax_split2;
              _details.TotalTaxSplit2 = _total_tax_split2;
              _details.IsIntegratedChipsSale = m_is_chips_operation;

              if (m_exchange_result != null)
              {
                _details.PaymentType = m_exchange_result.InType;
                _details.ExchangeCommission = m_exchange_result.Comission;
                _details.WasForeingCurrency = m_exchange_result.WasForeingCurrency;
                // ATB 06-JUN-2017
                _details.CardNumber = m_exchange_result.BankTransactionData.DocumentNumber;
                _details.CardHolder = m_exchange_result.BankTransactionData.HolderName;
                _details.IsPayingWithCreditCard = (_details.PaymentType == CurrencyExchangeType.CARD) ? true : false;

                //EOR 07-SEP-2016
                if (_hide_voucher_recharge)
                {
                  _details.CardAmount = m_exchange_result.CardPrice;
                  _details.CardCommission = m_exchange_result.Comission;
                }
              }

              ArrayList _cash_in_vouchers;
              //LTC 22-SEP-2016
              if (m_is_chips_operation && m_voucher_type == VoucherTypes.CardAdd && m_is_chips_sale_with_recharge == true)
              {
                _cashDeskId = CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION.DRAW_01;
              }

              if (m_junket_info != null && m_junket_info.BusinessLogic != null)
              {
                _junket = m_junket_info.BusinessLogic;
              }

              _cash_in_vouchers = VoucherBuilder.CashIn(m_card_data.VoucherAccountInfo(),
                                                        m_card_data.AccountId,
                                                        _splits,
                                                        _details,
                                                        null,
                                                        0,
                                                        PrintMode.Preview,
                                                        _cashDeskId,
                                                        sql_trx,
                                                        _junket,
                                                        m_selected_promo_data.PromoGameId);

              if (_voucher_list == null)
              {
                _voucher_list = _cash_in_vouchers;
              }
              else
              {
                foreach (Voucher _voucher in _cash_in_vouchers)
                {
                  _voucher_list.Add(_voucher);
                }
              }

              String _total_amount = "";
              if (m_with_currency_exchange)
              {
                if (m_exchange_result.WasForeingCurrency)
                {
                  _total_amount = Currency.Format(m_exchange_result.InAmount, m_exchange_result.InCurrencyCode);
                  if (m_exchange_result.InType == CurrencyExchangeType.CURRENCY)
                  {
                    lbl_currency_to_pay.Text = lbl_currency.Text;
                  }
                }
                else
                {
                  _total_amount = ((Currency)m_exchange_result.InAmount).ToString();

                  switch (m_exchange_result.InType)
                  {
                    case CurrencyExchangeType.CARD:
                      lbl_currency_to_pay.Text = Resource.String("STR_FRM_AMOUNT_INPUT_CREDIT_CARD_PAYMENT");
                      break;
                    case CurrencyExchangeType.CHECK:
                      lbl_currency_to_pay.Text = Resource.String("STR_FRM_AMOUNT_INPUT_CHECK_PAYMENT");
                      break;
                  }
                }

              }
              else
              {
                m_total_to_pay = m_voucher_amount + _card_deposit;
                _total_amount = m_total_to_pay.ToString();
              }

              // LTC 14-ABR-2016
              if (Misc.IsTaxCustodyEnabled() &&
                  _tax_custody > 0 &&
                  Misc.GetTaxCustodyVoucher() == Misc.TaxCustodyVoucher.CustodyVoucher) //EOR 01-08-2016
              {
                VoucherTaxCustody _tax_custody_voucher;

                _tax_custody_voucher = new VoucherTaxCustody(m_card_data.VoucherAccountInfo(), _splits, _tax_custody, PrintMode.Preview, sql_trx);

                if (_voucher_list == null)
                {
                  _voucher_list = new ArrayList();
                  _voucher_list.Add(_tax_custody_voucher);
                }
                else
                {
                  _voucher_list.Add(_tax_custody_voucher);
                }
              }

              if (Misc.IsTaxProvisionsEnabled() && _tax_provisions > 0)
              {
                VoucherTaxProvisions _tax_provisions_voucher;

                _tax_provisions_voucher = new VoucherTaxProvisions(m_card_data.VoucherAccountInfo(), _splits, _tax_provisions, PrintMode.Preview, sql_trx);

                if (_voucher_list == null)
                {
                  _voucher_list = new ArrayList();
                  _voucher_list.Add(_tax_provisions_voucher);
                }
                else
                {
                  _voucher_list.Add(_tax_provisions_voucher);
                }
              }

              // Add Flyer Voucher for Preview to the Voucher List
              if (m_junket_info != null && m_junket_info.BusinessLogic != null)
              {
                if (m_junket_info.BusinessLogic.PrintVoucher)
                {
                  VoucherJunket _junket_voucher;

                  _junket_voucher = m_junket_info.CreateVoucher(0, m_card_data, PrintMode.Preview, sql_trx);

                  if (_voucher_list == null)
                  {

                    _voucher_list = new ArrayList();
                    _voucher_list.Add(_junket_voucher);

                  }
                  else
                  {
                    _voucher_list.Add(_junket_voucher);
                  }
                }
              }

              _voucher_file_name = VoucherPrint.GenerateFileForPreview(_voucher_list);
              lbl_total_to_pay.Text = _total_amount.ToString();
            }
            break;

          case VoucherTypes.CardRedeem:
          case VoucherTypes.TicketRedeem:
            {
              _voucher_file_name = PreviewVoucherTicketORCardRedeem(VoucherAmount, sql_trx, IsApplyTaxes);
            }
            break;

          case VoucherTypes.FillIn:
            {
              VoucherCashDeskOpen _voucher;
              VoucherCashDeskOpenBody _voucher_body;

              _voucher = new VoucherCashDeskOpen(CASHIER_MOVEMENT.FILLER_IN, PrintMode.Preview, sql_trx);

              foreach (KeyValuePair<CurrencyIsoType, Decimal> _amount in _currencies_amount_order_to_config)
              {
                _initial_balance = 0;
                if (CashierSessionInfo.CashierSessionId != 0)
                {
                  if (uc_payment_method1.CurrencyExchange == null || m_national_currency.CurrencyCode == _amount.Key.IsoCode && _amount.Key.Type == CurrencyExchangeType.CURRENCY)
                  {
                    _initial_balance = CashierBusinessLogic.UpdateSessionBalance(sql_trx, CashierSessionInfo.CashierSessionId, 0);
                  }
                  else
                  {
                    _initial_balance = CashierBusinessLogic.UpdateSessionBalance(sql_trx, CashierSessionInfo.CashierSessionId, 0, _amount.Key.IsoCode, _amount.Key.Type);
                  }
                }

                _voucher_body = new VoucherCashDeskOpenBody(CASHIER_MOVEMENT.FILLER_IN, _amount.Key, _initial_balance, _amount.Value);
                _voucher.SetParameterValue("@VOUCHER_CASH_DESK_OPEN_BODY", _voucher_body.VoucherHTML);
              }
              _voucher.AddString("VOUCHER_CASH_DESK_OPEN_BODY", "");

              _voucher_file_name = _voucher.GetFileName();
            }
            break;

          case VoucherTypes.FillOut:
            {
              //LTC 05-AGO-2016
              Voucher voucher;
              Voucher _voucher_body;

              // ATB 27-OCT-2016
              _voucher_list = new ArrayList();
              voucher = new Voucher();

              if (uc_payment_method1.CurrencyExchange != null)
              {
                Withdrawal.Type = uc_payment_method1.CurrencyExchange.Type;
              }
              _cashier_current_balance = 0;
              try
              {
                if (uc_payment_method1.CurrencyExchange == null || m_national_currency.CurrencyCode == uc_payment_method1.CurrencyExchange.CurrencyCode && uc_payment_method1.CurrencyExchange.Type == CurrencyExchangeType.CURRENCY)
                {
                  _cashier_current_balance = m_aux_amount;
                }
                else
                {
                  _cashier_current_balance = CashierBusinessLogic.UpdateSessionBalance(sql_trx, CashierSessionInfo.CashierSessionId, 0, uc_payment_method1.CurrencyExchange.CurrencyCode, uc_payment_method1.CurrencyExchange.Type);
                }
              }
              catch (Exception ex)
              {
                Log.Exception(ex);
              }

              CageDenominationsItems.CageDenominationsItem _tmp_item;
              String _tmp_iso_code;

              switch (_tmp_currency_exchange.Type)
              {
                case CurrencyExchangeType.CURRENCY:
                case CurrencyExchangeType.CASINOCHIP:
                case CurrencyExchangeType.CASINO_CHIP_RE:
                case CurrencyExchangeType.CASINO_CHIP_NRE:
                case CurrencyExchangeType.CASINO_CHIP_COLOR:
                default:
                  _tmp_iso_code = _tmp_currency_exchange.CurrencyCode;
                  break;

                case CurrencyExchangeType.CARD:
                  _tmp_iso_code = _tmp_currency_exchange.CurrencyCode + Cage.BANK_CARD_CODE;
                  break;

                case CurrencyExchangeType.CHECK:
                  _tmp_iso_code = _tmp_currency_exchange.CurrencyCode + Cage.CHECK_CODE;
                  break;
              }

              if (m_voucher_amount > _cashier_current_balance && !_has_error && !IsGamingTable)
              {
                String _msg_error;

                if (FeatureChips.IsChipsType(_tmp_currency_exchange.Type))
                {
                  _msg_error = Resource.String("STR_FRM_AMOUNT_INPUT_MSG_CHIPS_EXCEED_BANK");
                }
                else
                {
                  _msg_error = Resource.String("STR_FRM_AMOUNT_INPUT_MSG_AMOUNT_EXCEED_BANK");

                }
                frm_message.Show(_msg_error, Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this);

                is_voucher_loaded = false;
                _has_error = true;

                if (m_cage_enabled)
                {
                  if (m_cage_amounts.ContainsKey(_tmp_iso_code, _tmp_currency_exchange.Type))
                  {
                    _tmp_item = new CageDenominationsItems.CageDenominationsItem();
                    _tmp_item.ItemAmounts = m_cage_amounts[_tmp_iso_code, _tmp_currency_exchange.Type].ItemAmounts;
                    _tmp_item.TotalAmount = GetCurrentBalance(_tmp_iso_code, _tmp_currency_exchange.Type);
                    _tmp_item.TotalTips = m_cage_amounts[_tmp_iso_code, _tmp_currency_exchange.Type].TotalTips;
                    _tmp_item.AllowedFillOut = is_voucher_loaded;
                    _tmp_item.HasTicketsTito = m_cage_amounts[_tmp_iso_code, _tmp_currency_exchange.Type].HasTicketsTito;
                    m_cage_amounts[_tmp_iso_code, _tmp_currency_exchange.Type] = _tmp_item;
                  }
                }
                if (ExitPreview())
                {
                  return;
                }
              }
              else if (m_cage_enabled)
              {
                if (m_cage_amounts.ContainsKey(_tmp_iso_code, _tmp_currency_exchange.Type))
                {
                  _tmp_item = new CageDenominationsItems.CageDenominationsItem();
                  _tmp_item.ItemAmounts = m_cage_amounts[_tmp_iso_code, _tmp_currency_exchange.Type].ItemAmounts;
                  _tmp_item.TotalAmount = GetCurrentBalance(_tmp_iso_code, _tmp_currency_exchange.Type);
                  _tmp_item.TotalTips = m_cage_amounts[_tmp_iso_code, _tmp_currency_exchange.Type].TotalTips;
                  _tmp_item.AllowedFillOut = true;
                  _tmp_item.HasTicketsTito = m_cage_amounts[_tmp_iso_code, _tmp_currency_exchange.Type].HasTicketsTito;
                  m_cage_amounts[_tmp_iso_code, _tmp_currency_exchange.Type] = _tmp_item;
                }
              }

              // ATB 02-NOV-2016
              //LTC 05-AGO-2016
              if (m_voucher_type == VoucherTypes.FillOut)
              {
                // Main voucher's creation
                voucher = new VoucherCashDeskWithdraw(CASHIER_MOVEMENT.FILLER_OUT, PrintMode.Preview, sql_trx, CashierVoucherType.CashWithdrawal);//Get pinpan records

                // Has the card's vouchers been requested before?
                if (!String.IsNullOrEmpty(m_voucher_card.VoucherHTML) && uc_payment_method1.CurrencyExchange.Type != CurrencyExchangeType.CARD)
                {
                  _voucher_body = m_voucher_card;
                  voucher.SetParameterValue("@VOUCHER_CASH_DESK_WITHDRAW_BODY", _voucher_body.VoucherHTML);
                }
                // If the user has requested the card's voucher/s
                if (Misc.IsBankCardWithdrawalEnabled() && uc_payment_method1.CurrencyExchange != null && uc_payment_method1.CurrencyExchange.Type == CurrencyExchangeType.CARD)
                {
                  Withdrawal.GetPinpanMovements(CashierSessionInfo.CashierSessionId);
                  Withdrawal.Type = CurrencyExchangeType.CARD;

                  _voucher_body = new VoucherCashDeskWithdrawCardBody(Convert.ToInt32(m_input_amount.Value), Withdrawal.Count, Withdrawal.AmountTotal);
                  // Saving the body in case of refresh
                  m_voucher_card = _voucher_body;
                  voucher.SetParameterValue("@VOUCHER_CASH_DESK_WITHDRAW_BODY", _voucher_body.VoucherHTML);
                }

                // If the user pays with any other payment method, it may contains many currencies so we run into them
                foreach (KeyValuePair<CurrencyIsoType, Decimal> _amount in _currencies_amount_order_to_config)
                {
                  if (m_national_currency.CurrencyCode == _amount.Key.IsoCode && _amount.Key.Type == CurrencyExchangeType.CURRENCY)
                  {
                    _cashier_current_balance = m_aux_amount;
                  }
                  else
                  {
                    _cashier_current_balance = CashierBusinessLogic.UpdateSessionBalance(sql_trx, CashierSessionInfo.CashierSessionId, 0, _amount.Key.IsoCode, _amount.Key.Type);
                  }

                  if (!(_amount.Key.Type == CurrencyExchangeType.CARD && Misc.IsBankCardCloseCashEnabled()))
                  {
                    // Adding the current petition's body to the main voucher
                    _voucher_body = new VoucherCashDeskWithdrawBody(_amount.Key, _cashier_current_balance, _amount.Value);
                    voucher.SetParameterValue("@VOUCHER_CASH_DESK_WITHDRAW_BODY", _voucher_body.VoucherHTML);
                  }

                  //}
                }

              }
              voucher.AddString("VOUCHER_CASH_DESK_WITHDRAW_BODY", "");
              _voucher_file_name = voucher.GetFileName();
            }
            break;

          case VoucherTypes.CloseCash:
            {
              Currency _input_amount;
              String _national_currency;
              CurrencyIsoType _iso_type;

              _national_currency = CurrencyExchange.GetNationalCurrency();
              _input_amount = m_input_amount.HasValue ? m_input_amount.Value : 0;
              _iso_type = new CurrencyIsoType();

              if (m_session_stats.collected_diff == null)
              {
                WSI.Common.Cashier.ReadCashierSessionData(sql_trx, CashierSessionInfo.CashierSessionId, ref m_session_stats);
                // SMN & LEM & RCI 16-OCT-2014: Remove collected_diff initialization because it cause error when final_balance = 0.
              }

              if (uc_payment_method1.CurrencyExchange == null || uc_payment_method1.CurrencyExchange.CurrencyCode == m_national_currency.CurrencyCode && uc_payment_method1.CurrencyExchange.Type == CurrencyExchangeType.CURRENCY)
              {
                _iso_type.IsoCode = m_national_currency.CurrencyCode;
                _iso_type.Type = m_national_currency.Type;
                m_session_stats.collected_diff[_iso_type] = (Decimal)VoucherAmount - m_session_stats.final_balance;
              }
              else
              {
                _iso_type.IsoCode = uc_payment_method1.CurrencyExchange.CurrencyCode;
                _iso_type.Type = uc_payment_method1.CurrencyExchange.Type;

                // DRV 02-DEC-2014: It's a foreign currency: We need to assign it to the Currency to round it properly.
                VoucherAmount.CurrencyIsoCode = uc_payment_method1.CurrencyExchange.CurrencyCode;

                m_session_stats.collected_diff[_iso_type] = (Decimal)VoucherAmount;

                if (!Misc.IsBankCardCloseCashEnabled() && m_session_stats.currencies_balance.ContainsKey(_iso_type))
                {
                  m_session_stats.collected_diff[_iso_type] -= m_session_stats.currencies_balance[_iso_type];
                }
              }

              m_session_stats.collected[_iso_type] = (Decimal)VoucherAmount;
              m_currencies_amount[_iso_type] = (Decimal)VoucherAmount;

              //m_currencies_amount = m_session_stats.collected;

              // ATB 16-NOV-2016
              // To see the Total Vouchers info
              if (m_voucher_type == VoucherTypes.CloseCash && Misc.IsBankCardCloseCashEnabled())
              {
                Withdrawal.GetPinpanMovementsCutOff(CashierSessionInfo.CashierSessionId);
                Misc.PrintBankCardSection_StatusCash = true;
              }

              // - Build voucher specific data (amounts)
              _voucher_list = VoucherBuilder.CashDeskClose(m_session_stats,
                                                           0,
                                                           PrintMode.Preview,
                                                           sql_trx,
                                                           Cage.IsGamingTable(CashierSessionInfo.CashierSessionId));

              _voucher_file_name = VoucherPrint.GenerateFileForPreview(_voucher_list);
            }
            break;

          case VoucherTypes.OpenCash:
            {
              VoucherCashDeskOpen voucher;
              VoucherCashDeskOpenBody _voucher_body;

              //SGB 08-OCT-2014: Modify call to VoucherCashDeskOpen for new parameter CashierVoucherType.CashOpening
              voucher = new VoucherCashDeskOpen(CASHIER_MOVEMENT.OPEN_SESSION, CashierVoucherType.CashOpening, PrintMode.Preview, sql_trx);

              foreach (KeyValuePair<CurrencyIsoType, Decimal> _amount in _currencies_amount_order_to_config)
              {
                _voucher_body = new VoucherCashDeskOpenBody(CASHIER_MOVEMENT.OPEN_SESSION, _amount.Key, 0, _amount.Value);
                voucher.SetParameterValue("@VOUCHER_CASH_DESK_OPEN_BODY", _voucher_body.VoucherHTML);
              }
              voucher.AddString("VOUCHER_CASH_DESK_OPEN_BODY", "");

              _voucher_file_name = voucher.GetFileName();
            }
            break;

          case VoucherTypes.MBCardSetLimit:
          case VoucherTypes.MBCardChangeLimit:
            {
              VoucherMBCardChange voucher;

              voucher = new VoucherMBCardChange(PrintMode.Preview, m_mb_card_data, sql_trx);

              voucher.AddCurrency("VOUCHER_MBCARD_CHANGE_INITIAL", m_mb_card_data.SalesLimit);
              voucher.AddCurrency("VOUCHER_MBCARD_CHANGE_AMOUNT", m_voucher_amount);

              if (m_voucher_type == VoucherTypes.MBCardSetLimit)
              {
                voucher.AddCurrency("VOUCHER_MBCARD_CHANGE_FINAL", m_voucher_amount);
              }
              else
              {
                voucher.AddCurrency("VOUCHER_MBCARD_CHANGE_FINAL", m_mb_card_data.SalesLimit + m_voucher_amount);
              }

              _voucher_file_name = voucher.GetFileName();
            }
            break;

          case VoucherTypes.MBCardCashDeposit:
            {
              VoucherMBCardDepositCash voucher;
              Currency _limit_mb_after_deposit;
              Currency _mb_deposit_excess;
              Currency _mb_deposit_without_excess;
              Boolean _gp_pending_cash_partial;

              _gp_pending_cash_partial = MobileBank.GP_GetRegisterShortfallOnDeposit();

              voucher = new VoucherMBCardDepositCash(PrintMode.Preview, CashierVoucherType.MBDeposit, m_mb_card_data, sql_trx);
              voucher.AddString("STR_VOUCHER_MBCARD_CASH_DEPOSIT_TITLE", Resource.String("STR_VOUCHER_MBCARD_CASH_DEPOSIT_TITLE"));
              voucher.AddString("STR_VOUCHER_MBCARD_CASH_DEPOSIT_AMOUNT", ((String)Resource.String("STR_VOUCHER_MBCARD_CASH_DEPOSIT_AMOUNT") + ":"));
              voucher.AddString("STR_VOUCHER_MBCARD_CASH_DEPOSIT_CASH", ((String)Resource.String("STR_VOUCHER_MBCARD_CASH_DEPOSIT_CASH") + ":"));

              if (_gp_pending_cash_partial)
              {
                voucher.AddString("STR_VOUCHER_MBCARD_CASH_DEPOSIT_PENDING", ((String)Resource.String("STR_UC_CARD_PENDING_DEPOSITS") + ":"));
              }
              else
              {
                voucher.AddString("STR_VOUCHER_MBCARD_CASH_DEPOSIT_PENDING", ((String)Resource.String("STR_VOUCHER_MBCARD_CASH_DEPOSIT_PENDING") + ":"));
              }

              voucher.AddString("STR_VOUCHER_MBCARD_CASH_DEPOSIT_EXCESS", Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_DIFF_EXTRA") + ":");
              voucher.AddCurrency("VOUCHER_MBCARD_CASH_DEPOSIT_INITIAL", m_mb_card_data.SalesLimit);
              voucher.AddCurrency("VOUCHER_MBCARD_CASH_DEPOSIT_AMOUNT", m_voucher_amount);

              _mb_deposit_excess = m_mb_card_data.PendingCash - m_voucher_amount; // Pending - Deposit

              if (_mb_deposit_excess < 0)
              {
                voucher.AddCurrency("VOUCHER_MBCARD_CASH_DEPOSIT_PENDING", 0);
                voucher.AddCurrency("VOUCHER_MBCARD_CASH_DEPOSIT_EXCESS", Math.Abs(_mb_deposit_excess));
                _mb_deposit_without_excess = m_mb_card_data.PendingCash;
              }
              else
              {
                voucher.AddCurrency("VOUCHER_MBCARD_CASH_DEPOSIT_PENDING", m_mb_card_data.PendingCash - m_voucher_amount);
                voucher.AddCurrency("VOUCHER_MBCARD_CASH_DEPOSIT_EXCESS", 0);
                _mb_deposit_without_excess = m_voucher_amount;
              }

              if (!CashierBusinessLogic.GetSalesLimitAutoIncrement(CashierSessionInfo.UserId, CashierSessionInfo.CashierSessionId, _mb_deposit_without_excess, out _limit_mb_after_deposit))
              {
                Log.Error("PreviewVoucher. Reading Available Sales After Deposit. UserId: " + CashierSessionInfo.UserId + " Cashier Session: " + CashierSessionInfo.CashierSessionId);
              }

              voucher.AddCurrency("VOUCHER_MBCARD_CASH_DEPOSIT_CASH", m_voucher_amount);
              voucher.AddCurrency("VOUCHER_MBCARD_CASH_DEPOSIT_FINAL", (m_mb_card_data.SalesLimit + Math.Abs(_limit_mb_after_deposit)));

              _voucher_file_name = voucher.GetFileName();
            }
            break;

          case VoucherTypes.StatusCash:
            {
              WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS session_stats;
              session_stats = new WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS();
              WSI.Common.Cashier.ReadCashierSessionData(sql_trx, CashierSessionInfo.CashierSessionId, ref session_stats);

              if (Misc.IsGamingHallMode())
              {
                VoucherGamingHallCashDeskStatus voucher;

                voucher = new VoucherGamingHallCashDeskStatus(PrintMode.Preview, sql_trx, session_stats);

                _voucher_file_name = voucher.GetFileName();
              }
              else
              {
                VoucherCashDeskStatus voucher;

                voucher = new VoucherCashDeskStatus(PrintMode.Preview, sql_trx);

                // - Build voucher specific data (amounts)
                voucher.SetupCashStatusVoucher(session_stats);

                _voucher_file_name = voucher.GetFileName();
              }
            }
            break;

          case VoucherTypes.CardReplacement:
            {
              TYPE_CARD_REPLACEMENT _params;

              _params = new TYPE_CARD_REPLACEMENT();
              _params.account_id = m_card_data.AccountId;
              _params.card_replacement_price = m_aux_amount;
              _params.old_track_data = m_card_data.TrackData;
              // CardReplacement voucher uses LoggedInTerminalName string to pass the track data related to the new card read
              _params.new_track_data = m_card_data.LoggedInTerminalName;
              _params.AccountInfo = m_card_data.VoucherAccountInfo(_params.new_track_data);

              if (GeneralParam.GetBoolean("Cashier.PlayerCard", "AmountToCompanyB", false))
              {
                Accounts.CalculateSplit2Tax(_params.card_replacement_price, out _params.TotalBaseTaxSplit2, out  _params.TotalTaxSplit2);
              }
              else
              {
                _params.TotalTaxSplit2 = -1;
              }

              // DHA 14-JUL-2015 set comissions on card replacement
              _params.ExchangeResult = m_exchange_result;

              // _params.CashierMovement = CASHIER_MOVEMENT.CARD_REPLACEMENT;
              Split.ReadSplitParameters(out _params.Splits);

              // RAB 24-MAR-2016: Bug 10745: Add CardPaid input parameter to function.
              _voucher_list = VoucherBuilder.CardPlayerReplacement(0, _params, OperationCode.CARD_REPLACEMENT, PrintMode.Preview, sql_trx, m_card_data.CardPaid);

              _voucher_file_name = VoucherPrint.GenerateFileForPreview(_voucher_list);
            }
            break;

          case VoucherTypes.CreditLineGetMarker:
            break;

          //DLL////////////////////////////////////////////////////
          //////case VoucherTypes.ChipsSaleTITO:
          //////  {
          //////    VoucherChipsPurchaseSale voucher;
          //////    TYPE_SPLITS _splits;

          //////    if (!Split.ReadSplitParameters(out _splits))
          //////    {
          //////      return;
          //////    }

          //////    voucher = new VoucherChipsPurchaseSale(m_card_data.VoucherAccountInfo(), _splits, m_chips_amounts, PrintMode.Preview, sql_trx);

          //////    _voucher_file_name = voucher.GetFileName();
          //////  }
          //////  break;
          //DLL////////////////////////////////////////////////////

          default:
            break;
        }
      }

      catch (Exception ex)
      {
        sql_trx.Rollback();
        Log.Exception(ex);
      }

      finally
      {
        if (sql_trx != null)
        {
          if (sql_trx.Connection != null)
          {
            sql_trx.Rollback();
          }
          sql_trx.Dispose();
          sql_trx = null;
        }

        // Close connection
        if (sql_conn != null)
        {
          sql_conn.Close();
          sql_conn = null;
        }
      }

      if ((m_voucher_type == VoucherTypes.CloseCash || m_voucher_type == VoucherTypes.OpenCash
          || m_voucher_type == VoucherTypes.FillIn || m_voucher_type == VoucherTypes.FillOut) && uc_payment_method1.CurrencyExchange != null && !_has_error)
      {
        if (!m_cage_enabled)
        {
          //select next currency
          uc_payment_method1.SelectNextCurrencyAllowed();

          SetCurrencyExchange();
        }
      }

      m_out_amount = m_voucher_amount;
      web_browser.Navigate(_voucher_file_name);
      if (VoucherTypes.CardRedeem == m_voucher_type)
      {
      is_voucher_loaded = !String.IsNullOrEmpty(_voucher_file_name);
      }
      else
      {
        is_voucher_loaded = true;
      }
    } // PreviewVoucher

    #endregion Buttons

    #region Public Methods

    protected override void OnClosing(CancelEventArgs e)
    {
      if (_prev_size.Count > 0)
      {
        gb_common.Size = _prev_size[0];
        dgv_common.Size = _prev_size[1];
      }

      if (_prev_loc.Count > 0)
      {
        btn_promos_change_view.Location = _prev_loc[0];
        gb_promotion_info.Location = _prev_loc[1];
      }

      if (_prev_loc.Count == 3) // closing from cancel promotions view
      {
        pnl_buttons.Controls.Add(btn_cancel);
        btn_cancel.Location = _prev_loc[2];
        btn_promos_change_view.Enabled = true;
        btn_promos_change_view.Visible = true;
        this.btn_cancel_by_cashier.Visible = false;

        this.btn_cancel_by_cashier.Enabled = false;

        btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");
        _prev_size.Add(btn_cancel.Size);
        btn_cancel.Size = _prev_size[2];
      }

      // ATB 02-NOV-2016
      // Clearing the vouchers
      m_voucher_card = new Voucher();
      base.OnClosing(e);
    }

    //------------------------------------------------------------------------------
    // PURPOSE:  Show the Dialog for acceptation of Ticket Total Redeem
    // 
    //  PARAMS:
    //      - INPUT:
    //          - ParamsTicketOperation
    //
    //      - OUTPUT:
    //          - Check_redeem
    //          - ENUM_TITO_OPERATIONS_RESULT
    //
    // RETURNS: 
    //      - Result of Operation (True -> Ok, False -> Something went wrong)
    // 
    public ENUM_TITO_OPERATIONS_RESULT ShowForCashout(ref ParamsTicketOperation TicketParams, out PaymentThresholdAuthorization PaymentThresholdAuthorization)
    {
      Boolean _result;
      Boolean _check_redeem;
      Currency _cash_amount = TicketParams.in_cash_amount;

      m_tito_max_devolution = TicketParams.max_devolution;
      m_validation_numbers = TicketParams.validation_numbers;
      m_show_apply_tax_button = true;

      _result = this.Show(Resource.String("STR_UC_CARD_BTN_CREDIT_REDEEM_TOTAL"),
                          out _cash_amount,
                          out _check_redeem,
                          VoucherTypes.TicketRedeem,
                          TicketParams.in_account,
                          _cash_amount,
                          CASH_MODE.TOTAL_REDEEM,
                          TicketParams.in_window_parent);
      if (_result)
      {
        TicketParams.is_apply_tax = m_is_apply_tax;
        TicketParams.AccountOperationReasonId = m_account_operation_reason_id;
        TicketParams.AccountOperationComment = m_account_operation_comment;
        PaymentThresholdAuthorization = m_payment_threshold_authorization;
        if (this.m_total_to_pay <= 0)
        {
          return ENUM_TITO_OPERATIONS_RESULT.AUX_OPERATION_ERROR;
        }

        TicketParams.check_redeem = _check_redeem;

        return ENUM_TITO_OPERATIONS_RESULT.OK;
      }
      else
      {
        PaymentThresholdAuthorization = m_payment_threshold_authorization;

        return ENUM_TITO_OPERATIONS_RESULT.AUX_OPERATION_CANCELED;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE:  Show the Dialog for input amount for Ticket creation
    // 
    //  PARAMS:
    //      - INPUT:
    //            - TicketParams
    //            - Title
    //            - VoucherType
    //            - CardData
    //            - OperationMode
    //            - WindowParent (Form)
    //
    //      - OUTPUT:
    //          - CashAmount (Currency)
    //          - MoneyExchangeResult
    //          - VoucherOutData
    //          - PromotionId
    //          - NotRedeemableAmount (Currency)
    //          - NotRedeemableWonLock (Currency)
    //          - SpentUsed (Currency)
    //
    // RETURNS: 
    //      - Result of Operation (True -> Ok, False -> Something went wrong)
    // 
    public Boolean ShowForCashin(out TicketCreationParams OutParams, CardData Info, Form WindowParent)
    {
      Boolean _result;
      Boolean _check_redeem;
      Currency _cash_amount;

      OutParams = new TicketCreationParams();

      promo_filter.VirtualMode = Info.IsVirtualCard;

      _result = this.Show(Resource.String("STR_FRM_AMOUNT_INPUT_TITO_TICKET_CREATION_TITLE"), //Params.Title
                          out _cash_amount,
                          out _check_redeem,
                          VoucherTypes.CardAdd,
                          Info,
                          0/*Redeem amount*/,
                          CASH_MODE.TOTAL_REDEEM,
                          WindowParent);
      if (_result)
      {
        OutParams.OutCashAmount = _cash_amount;
        OutParams.OutExchangeResult = this.m_exchange_result;
        OutParams.OutPromotionId = this.m_selected_promo_id;
        OutParams.OutNotRedeemableAmount = this.m_promo_reward_amount;
        OutParams.OutNotRedeemableWonLock = this.m_promo_won_lock;
        OutParams.OutSpentUsed = this.m_spent_used;
        OutParams.OutParticipateInCashdeskDraw = this.m_participate_in_cashdesk_draw;
        OutParams.JunketInfo = this.m_junket_info;
        OutParams.PaymentThresholdAuthorization = this.m_payment_threshold_authorization;
      }

      return _result;
    }

    /// <summary>
    /// Init and Show the form
    /// </summary>
    /// <param name="CaptionText"></param>
    /// <param name="Amount"></param>
    /// <param name="Parent"></param>
    /// <returns></returns>
    /// 
    public Boolean ShowForHandPay(String CaptionText,
                out Currency Amount,
                out Boolean CheckRedeem,
                out PaymentThresholdAuthorization PaymentThresholdAuthorization,
                VoucherTypes VoucherType,
                CardData Card,
                Currency AuxAmount,
                HandPay Handpay,
                CASH_MODE SubstractType,
                Form Parent,
                Boolean IsApplyTax)
    {
      WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS _session_stats;
      SortedDictionary<CurrencyIsoType, Decimal> _amount_dic;
      Boolean _result;

      m_handpay = Handpay;
      m_is_apply_tax = IsApplyTax;
      _result = Show(CaptionText, out _amount_dic, out CheckRedeem, VoucherType, Card, AuxAmount, SubstractType, Parent, out _session_stats);
      Amount = 0;

      foreach (CurrencyIsoType _str_aux in _amount_dic.Keys)
      {
        Amount = _amount_dic[_str_aux];
        break;
      }
      PaymentThresholdAuthorization = m_payment_threshold_authorization;
      return _result;
    }

    public Boolean Show(String CaptionText,
                    out Currency Amount,
                    VoucherTypes VoucherType,
                    CardData Card,
                    Currency AuxAmount,
                    CASH_MODE SubstractType,
                    Form Parent,
                    bool IsChipsSaleWithRecharge = false)
    {
      Boolean _check_redeem;

      return Show(CaptionText, out Amount, out _check_redeem, VoucherType, Card, AuxAmount, SubstractType, Parent, IsChipsSaleWithRecharge);
    }

    public Boolean Show(String CaptionText,
                    out Currency Amount,
                    VoucherTypes VoucherType,
                    CardData Card,
                    Currency AuxAmount,
                    CASH_MODE SubstractType,
                    Form Parent,
                    CurrencyExchangeResult ExchangeResult)
    {
      Boolean _check_redeem;

      m_exchange_result = ExchangeResult;

      return Show(CaptionText, out Amount, out _check_redeem, VoucherType, Card, AuxAmount, SubstractType, Parent);
    }

    public Boolean Show(String CaptionText,
                    out Currency Amount,
                    out Boolean CheckRedeem,
                    VoucherTypes VoucherType,
                    CardData Card,
                    Currency AuxAmount,
                    CASH_MODE SubstractType,
                    Form Parent,
                    bool IsChipsSaleWithRecharge = false)
    {
      WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS _session_stats;
      SortedDictionary<CurrencyIsoType, Decimal> _amount_dic;
      Boolean _result;

      _result = Show(CaptionText, out _amount_dic, out CheckRedeem, VoucherType, Card, AuxAmount, SubstractType, Parent, out _session_stats, IsChipsSaleWithRecharge);
      Amount = 0;
      foreach (CurrencyIsoType _str_aux in _amount_dic.Keys)
      {
        Amount = _amount_dic[_str_aux];
        break;
      }
      return _result;
    }

    public Boolean Show(String CaptionText,
                        out SortedDictionary<CurrencyIsoType, Decimal> Amount,
                        out Boolean CheckRedeem,
                        VoucherTypes VoucherType,
                        CardData Card,
                        Currency AuxAmount,
                        CASH_MODE SubstractType,
                        Form Parent,
                        out WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS SessionStats,
                        Boolean IsPendingRequest,
                        Int64 RequestMovementId,
                        Boolean HasPendingMovements,
                        CashierSessionInfo CashierSessionInfo,
                        ClosingStocks ClosingStocks)
    {
      Boolean _result;
      this.m_is_pending_request = IsPendingRequest;
      this.m_request_movement_id = RequestMovementId;
      this.m_has_pending_movements = HasPendingMovements;
      this.CashierSessionInfo = CashierSessionInfo;

      this.m_show_rows_without_amount = true;

      // DHA 09-JUN-2016: validating closing cash
      this.m_closing_stock = ClosingStocks;

      _result = Show(CaptionText, out Amount, out CheckRedeem, VoucherType, Card, AuxAmount, SubstractType, Parent, out SessionStats);

      // Save the values introduced by user to recalculate the closing amount
      if (ClosingStocks != null && ClosingStocks.Type == Common.ClosingStocks.ClosingStockType.FIXED && ClosingStocks.SleepsOnTable)
      {
        if (VoucherType == VoucherTypes.FillOut)
        {
          ClosingStocks.CurrenciesStocksCreditsUser = this.m_cage_amounts;
        }
        else if (VoucherType == VoucherTypes.FillIn)
        {
          ClosingStocks.CurrenciesStocksFillsUser = this.m_cage_amounts;
        }
      }

      this.m_closing_stock = null;

      return _result;
    }

    public Boolean Show(String CaptionText,
                        out SortedDictionary<CurrencyIsoType, Decimal> Amount,
                        out Boolean CheckRedeem,
                        VoucherTypes VoucherType,
                        CardData Card,
                        Currency AuxAmount,
                        CASH_MODE SubstractType,
                        Form Parent,
                        out WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS SessionStats,
                        bool IsChipsSaleWithRecharge = false)
    {
      CurrencyIsoType _iso_type;
      Boolean _allow_bank_card_types;
      Cage.ShowDenominationsMode _show_denominations_mode;

      PaymentThresholdAuthorization _payment_threshold_authorization;
      Boolean _process_continue;
      PaymentAndRechargeThresholdData _payment_and_recharge_threshold_data;
      String _error_str;

      _iso_type = new CurrencyIsoType();
      m_junket_info = null;
      _error_str = String.Empty;

      _show_denominations_mode = (Cage.ShowDenominationsMode)GeneralParam.GetInt32("Cage", "ShowDenominations", 1);
      if (VoucherType == VoucherTypes.WinLoss && m_win_loss_mode == GamingTableBusinessLogic.GT_WIN_LOSS_MODE.BY_TOTALS)
      {
        _show_denominations_mode = Cage.ShowDenominationsMode.HideAllDenominations;
      }

      uc_payment_method1.HeaderText = Resource.String("STR_FRM_AMOUNT_CURRENCY_GROUP_BOX");
      m_is_chips_sale_with_recharge = IsChipsSaleWithRecharge;

      // DHA 02-MAR-2015: Load all currencies defined on the system
      List<CurrencyExchangeType> _types = new List<CurrencyExchangeType>();

      btn_show_hide_rows.Visible = false;
      if (!IsGamingTable)
      {
        _types.Add(CurrencyExchangeType.CURRENCY);
      }
      if (GamingTableBusinessLogic.IsGamingTablesEnabled())
      {
        _types.Add(CurrencyExchangeType.CASINOCHIP);
      }
      if (IsGamingTable)
      {
        _types.Add(CurrencyExchangeType.CURRENCY);
      }

      switch (VoucherType)
      {
        case VoucherTypes.CardAdd:

          if (JunketsBusinessLogic.IsJunketsEnabled())
          {
            btn_enter_flyer.Visible = true;
            lbl_entered_tokens.Visible = false;
            btn_enter_flyer.Text = Resource.String("STR_APP_JUNKET_FLYER_BTN");
          }
          else
          {
            btn_enter_flyer.Visible = false;
            lbl_entered_tokens.Visible = true;
          }

          _allow_bank_card_types = GeneralParam.GetBoolean("Cashier.PaymentMethod", "BankCard.DifferentiateType", false);

          // ETP 20/06/2016 --> Habilitado solo moneda nacional para MultiCurrencyExchange
          CurrencyExchange.GetAllowedCurrencies(true, null, false, _allow_bank_card_types, out m_national_currency, out m_currencies);

          //DRV 15-JUL-2014: With Chips sale register, the payment method just could be with national currency in cash
          if (m_is_chips_sale_at_table_register)
          {
            m_currencies = new List<CurrencyExchange>();
            m_currencies.Add(m_national_currency);
          }
          uc_payment_method1.HeaderText = Resource.String("STR_FRM_AMOUNT_PAYMENT_METHOD_GROUP_BOX");
          break;

        case VoucherTypes.CloseCash:
        case VoucherTypes.FillOut:
        case VoucherTypes.DropBoxCount:
          // DHA 02-MAR-2015: Only when gaming table is not integrated to cashier session
          if (IsGamingTable && Cashier.TerminalId != CashierSessionInfo.TerminalId)
          {
            // When chips sleeps on table, don't show check and bank card
            if (this.m_closing_stock == null || !this.m_closing_stock.SleepsOnTable)
            {
              if (VoucherType == VoucherTypes.CloseCash || VoucherType == VoucherTypes.DropBoxCount)
              {
                _types.Add(CurrencyExchangeType.CARD);
              }

              _types.Add(CurrencyExchangeType.CHECK);
            }

            CurrencyExchange.GetAllowedCurrencies(true, _types, false, out m_national_currency, out m_currencies);
          }
          else
          {
            CurrencyExchange.GetAllowedCurrenciesForCashierSession(CashierSessionInfo.CashierSessionId, VoucherType == VoucherTypes.CloseCash, out m_national_currency, out m_currencies);
          }
          m_session_stats = new WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS();
          break;

        case VoucherTypes.ChipsSaleAmountInput:
          btn_enter_flyer.Visible = false;
          CurrencyExchange.GetAllowedCurrencies(true, _types, false, out m_national_currency, out m_currencies);
          break;

        case VoucherTypes.OpenCash:
        case VoucherTypes.FillIn:
        case VoucherTypes.AmountRequest:
        default:
          CurrencyExchange.GetAllowedCurrencies(true, _types, false, out m_national_currency, out m_currencies);
          break;
      }

      FeatureChips.ReloadChipsAndSets(_show_denominations_mode);

      m_voucher_type = VoucherType;
      uc_payment_method1.VoucherType = m_voucher_type;
      m_card_data = Card;

      //DPC 21-MAR-2017: If haven't credit line hide the payment type credit line
      if (m_card_data != null)
      {
        uc_payment_method1.showPaymentCreditLine = m_card_data.CreditLineData.Exists;
      }

      if (m_closing_stock == null && VoucherType == VoucherTypes.FillOut)
      {
        ClosingStocks cs = new ClosingStocks(m_cashier_session_info.TerminalId);
        if (cs != null && cs.Type == ClosingStocks.ClosingStockType.FIXED && cs.SleepsOnTable && !Misc.IsBankCardWithdrawalEnabled())
        {
          List<CurrencyExchange> m_currencies_copy = null;
          m_currencies_copy = new List<CurrencyExchange>(m_currencies);

          foreach (CurrencyExchange _currency_exchange in m_currencies)
          {
            if (_currency_exchange.Type == CurrencyExchangeType.CARD || _currency_exchange.Type == CurrencyExchangeType.CHECK)
            {
              m_currencies_copy.Remove(_currency_exchange);
            }
          }
          m_currencies = m_currencies_copy;
        }
      }

      if (m_closing_stock != null && VoucherType == VoucherTypes.CloseCash &&
        (m_closing_stock.Type == ClosingStocks.ClosingStockType.ROLLING || m_closing_stock.Type == ClosingStocks.ClosingStockType.FIXED && m_closing_stock.SleepsOnTable && !Misc.IsBankCardWithdrawalEnabled()))
      {
        List<CurrencyExchange> m_currencies_copy = null;
        m_currencies_copy = new List<CurrencyExchange>(m_currencies);

        foreach (CurrencyExchange _currency_exchange in m_currencies)
        {
          if (_currency_exchange.Type == CurrencyExchangeType.CARD || _currency_exchange.Type == CurrencyExchangeType.CHECK)
          {
            if (m_closing_stock.Type == ClosingStocks.ClosingStockType.ROLLING)
            {
              m_currencies_copy.Remove(_currency_exchange);
            }
          }
        }

        m_currencies = m_currencies_copy;
      }

      if (VoucherType == VoucherTypes.CardAdd && m_chips_amounts != null)
      {
        // This case is when the user introduces the amount to do a chips sales
        uc_payment_method1.InitControl(m_currencies, m_chips_amounts.SelectedGamingTable.TableId, FeatureChips.ChipsOperation.Operation.SALE_AMOUNT);
      }
      else
      {
        uc_payment_method1.InitControl(m_currencies, GamingTableId, ConvertToChipsOperationType(VoucherType));
      }

      m_currencies_amount = new SortedDictionary<CurrencyIsoType, Decimal>(new CurrencyIsoType.SortComparer());
      lbl_cage_currency_total.Visible = false;
      if (((VoucherType == VoucherTypes.CardAdd || VoucherType == VoucherTypes.OpenCash || VoucherType == VoucherTypes.FillIn)
            && CurrencyExchange.AreCurrenciesEnabled(VoucherType != VoucherTypes.CardAdd)) ||
          ((VoucherType == VoucherTypes.CloseCash || VoucherType == VoucherTypes.FillOut || VoucherType == VoucherTypes.DropBoxCount) && uc_payment_method1.GetCurrenciesCount() > 1) ||
          (VoucherType == VoucherTypes.AmountRequest))// m_currencies.Count > 1))
      {
        if (VoucherType == VoucherTypes.CardAdd)
        {
          // If the currencies not contain national currency
          if ((uc_payment_method1.GetCurrenciesCount() > 0) && (uc_payment_method1.GetCurrenciesCount() > 1 || !uc_payment_method1.CurrenciesList.Contains(m_national_currency)))
          {
            // Set allways the first currency
            uc_payment_method1.CurrencyExchange = uc_payment_method1.CurrenciesList[0];
            ShowCurrencyExchange();
            SetCurrencyExchange();
          }
          else
          {
            HideAllCurrencyExchange();
            ResetRebuildForm();
          }
        }
        else
        {
          uc_payment_method1.CurrencyExchange = m_currencies[0];
          ShowCurrencyExchange();
          SetCurrencyExchange();
        }
      }
      else
      {
        HideAllCurrencyExchange();
      }

      gb_total.Visible = (VoucherType == VoucherTypes.CardAdd || VoucherType == VoucherTypes.CardRedeem);

      //DLL////////////////////////////////////////////////////
      btn_ok.Visible = true;
      Amount = new SortedDictionary<CurrencyIsoType, Decimal>(new CurrencyIsoType.SortComparer());
      CheckRedeem = false;
      m_out_check_redeem = false;
      SessionStats = new WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS();

      FormTitle = CaptionText;
      txt_amount.Text = "";
      lbl_total_to_pay.Text = "";
      m_cancel = false;

      web_browser.Navigate("");

      m_voucher_amount = 0;
      m_voucher_type = VoucherType;
      uc_payment_method1.VoucherType = m_voucher_type;
      m_aux_amount = AuxAmount;
      m_cash_mode = SubstractType;
      btn_ok.Enabled = false;

      // Only Card Add operations use the active common grid
      // It must be restored upon exit
      if (VoucherType != VoucherTypes.CardAdd && VoucherType != VoucherTypes.CancelPromotions && VoucherType != VoucherTypes.CloseCash && VoucherType != VoucherTypes.DropBoxCount
        && VoucherType != VoucherTypes.FillIn && VoucherType != VoucherTypes.FillOut && VoucherType != VoucherTypes.OpenCash && VoucherType != VoucherTypes.AmountRequest)
      {
        HideCommonGrid();
      }
      else
      {
        RestoreCommonGrid();

        switch (VoucherType)
        {
          case VoucherTypes.CancelPromotions:
            ShowCancelablePromos();
            break;

          case VoucherTypes.OpenCash:
          case VoucherTypes.CloseCash:
          case VoucherTypes.FillIn:
          case VoucherTypes.FillOut:
          case VoucherTypes.AmountRequest:
          case VoucherTypes.DropBoxCount:
            m_cage_enabled = Cage.IsCageEnabled();
            m_cage_automode = Cage.IsCageAutoMode();
            m_tito_enabled = Common.TITO.Utils.IsTitoMode();
            m_tito_tickets_collection = GeneralParam.GetBoolean("TITO", "Cashier.TicketsCollection", false);
            m_cage_allow_denomination_in_balance = GeneralParam.GetBoolean("Cage", "AllowDenominationImbalance", false);

            if (VoucherType == VoucherTypes.DropBoxCount)
            {
              m_cage_allow_denomination_in_balance = true;
            }

            // TODO: remove when creating the general parameter
            //m_cage_enabled = false;
            if (m_cage_enabled)
            {
              // ATB 03-NOV-2016
              // This button must be visible only if the cage is enabled
              btn_show_hide_rows.Visible = true;
              RebuildForm();
              CageFunctionality(VoucherType);
            }
            else
            {
              ResetRebuildForm();
              //Always return national currency. We need almost 2 currencies
              if (m_currencies != null && m_currencies.Count > 1)
              {
                ShowCurrencyExchange();
                SetCurrencyExchange();
              }
              else
              {
                HideAllCurrencyExchange();
              }
              HideCommonGrid();
            }

            break;
          default:

            // JAB 26-NOV-2013: Fixed Bug #WIG-429.
            ResetRebuildForm();

            PopulatePromotionsGrid();
            break;
        }
      }

      if (m_voucher_type == VoucherTypes.StatusCash || m_voucher_type == VoucherTypes.CancelPromotions)
      {
        ResetRebuildForm();
      }

      //  - Money buttons 
      // when cage mode is enabled and there aren't pending movements number buttons are disabled.
      if (m_allowed_entries || !m_cage_enabled)
      {
        InitMoneyButtons();
      }

      promo_filter.Visible = (m_voucher_type == VoucherTypes.CardAdd || m_voucher_type == VoucherTypes.CancelPromotions);
      promo_filter.FilterChange += new uc_promo_filter.FilterChangeEvent(promo_filter_FilterChange);

      // RCI 23-JAN-2013: Payment Order
      //btn_ok.Location = new System.Drawing.Point(165, 0);
      btn_check_redeem.Enabled = false;
      btn_check_redeem.Visible = false;
      m_payment_order_enabled = PaymentOrder.IsEnabled();
      m_payment_order_min_amount = PaymentOrder.MinAmount();

      if (m_voucher_type == VoucherTypes.CardRedeem || m_voucher_type == VoucherTypes.TicketRedeem)
      {
        if (m_payment_order_enabled)
        {
          //btn_ok.Location = new System.Drawing.Point(147, 3);
          btn_check_redeem.Location = new Point(gp_digits_box.Width - btn_check_redeem.Width, pnl_buttons.Location.Y);
          btn_check_redeem.Enabled = false;
          btn_check_redeem.Visible = true;
        }
      }

      if (m_voucher_type == VoucherTypes.ChipsSaleAmountInput || m_voucher_type == VoucherTypes.ChipsPurchaseAmountInput)
      {
        gp_digits_box.Visible = true;
        pnl_buttons.Location = new System.Drawing.Point(uc_payment_method1.Location.X, gb_tokens.Location.Y);
        btn_ok.Visible = false;
        btn_num_dot.Enabled = (m_voucher_type == VoucherTypes.ChipsSaleAmountInput);
        this.Width = 400;
        this.Height = 560;
      }

      gp_digits_box.Enabled = true;
      if (m_voucher_type == VoucherTypes.StatusCash
       || (m_voucher_type == VoucherTypes.CardRedeem && m_cash_mode == CASH_MODE.TOTAL_REDEEM)
       || m_voucher_type == VoucherTypes.CardReplacement
       || m_voucher_type == VoucherTypes.TicketRedeem)
      {
        gp_digits_box.Enabled = false;
        // NMR: se fuerza a que se actualize el Pre-print del Ticket
        if (m_voucher_type == VoucherTypes.TicketRedeem)
        {
          txt_amount.Text = AuxAmount.ToStringWithoutSymbols();
        }
        btn_intro_Click(null, null);
      }

      //web_browser.Enabled = true;

      ShowHideBtnApplyTax();

      if (m_cancel)
      {
        RestoreCommonGrid();

        return false;
      }

      DialogResult _dlg_rc = this.ShowDialog(Parent);

      if (_dlg_rc != DialogResult.OK || m_cancel)
      {
        RestoreCommonGrid();

        return false;
      }

      if (m_voucher_type == VoucherTypes.CardRedeem || m_voucher_type == VoucherTypes.TicketRedeem)
      {
        if (PaymentThresholdAuthorization.GetOutputThreshold() > 0 && PaymentThresholdAuthorization.GetOutputThreshold() <= m_aux_amount)
        {
          if (!ProfilePermissions.CheckPermissionList(ProfilePermissions.CashierFormFuncionality.TITO_PaymentThreshold,
                                                      ProfilePermissions.TypeOperation.RequestPasswd,
                                                      this,
                                                      0,
                                                      out _error_str))
          {
            RestoreCommonGrid();
            return false;
          }
        }
      }
      else if (m_voucher_type == VoucherTypes.CardAdd)
      {
        Currency _converted_amount;
        _payment_and_recharge_threshold_data = new PaymentAndRechargeThresholdData();
        if (m_exchange_result != null)
        {
          using (DB_TRX _db_trx = new DB_TRX())
          {
            _converted_amount = CurrencyExchange.GetExchange(m_exchange_result.InAmount, m_exchange_result.InCurrencyCode, CurrencyExchange.GetNationalCurrency(), _db_trx.SqlTransaction);
          }

          _payment_and_recharge_threshold_data.TransactionType = m_exchange_result.InType;
          _payment_and_recharge_threshold_data.TransactionSubType = m_exchange_result.BankTransactionData.Type;
        }
        else
        {
          _converted_amount = m_out_amount;
        }
        _payment_threshold_authorization = new PaymentThresholdAuthorization(_converted_amount, m_card_data, _payment_and_recharge_threshold_data, PaymentThresholdAuthorization.PaymentThresholdAuthorizationOperation.Input, this, out _process_continue);
        if (!_process_continue)
        {
          return false;
        }

        m_payment_threshold_authorization = _payment_threshold_authorization;
      }

      if (m_voucher_type == VoucherTypes.OpenCash
        || m_voucher_type == VoucherTypes.FillIn
        || m_voucher_type == VoucherTypes.FillOut
        || m_voucher_type == VoucherTypes.AmountRequest
        || m_voucher_type == VoucherTypes.WinLoss
        || (IsGamingTable && (m_voucher_type == VoucherTypes.CloseCash || m_voucher_type == VoucherTypes.DropBoxCount)))
      {
        AutoClickIntroButton();
        Amount = m_currencies_amount;
      }
      else
      {
        _iso_type.IsoCode = m_national_currency.CurrencyCode;
        _iso_type.Type = m_national_currency.Type;

        if (uc_payment_method1.CurrencyExchange != null)
        {
          _iso_type.IsoCode = uc_payment_method1.CurrencyExchange.CurrencyCode;
          _iso_type.Type = uc_payment_method1.CurrencyExchange.Type;
        }

        Amount.Add(_iso_type, m_out_amount);
      }

      SessionStats = m_session_stats;
      CheckRedeem = m_out_check_redeem;

      if (Parent != null)
      {
        Parent.Focus();
      }

      RestoreCommonGrid();

      return true;
    } // Show

    //------------------------------------------------------------------------------
    // PURPOSE : Show method only for mobile banks on Deposit (used from uc_mobile_bank.cs)
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public Boolean ShowMobileBanks(String CaptionText,
                                   out SortedDictionary<CurrencyIsoType, Decimal> Amount,
                                   out Decimal TotalAmount,
                                   out Boolean CheckRedeem,
                                   MBCardData Card,
                                   Currency AuxAmount,
                                   CASH_MODE SubstractType,
                                   Form Parent,
                                   out WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS SessionStats)
    {
      CurrencyIsoType _iso_type;
      Boolean _show_denominations;
      Int32 _margin_between_controls;

      _margin_between_controls = 11;

      try
      {
        _iso_type = new CurrencyIsoType();

        uc_payment_method1.HeaderText = Resource.String("STR_FRM_AMOUNT_CURRENCY_GROUP_BOX");

        List<CurrencyExchangeType> _types = new List<CurrencyExchangeType>();

        _types.Add(CurrencyExchangeType.CURRENCY);
        if (GamingTableBusinessLogic.IsGamingTablesEnabled())
        {
          _types.Add(CurrencyExchangeType.CASINOCHIP);
        }
        CurrencyExchange.GetAllowedCurrencies(true, _types, false, out m_national_currency, out m_currencies);
        //CurrencyExchange.GetAllowedCurrenciesForCashierSession(Cashier.SessionId, false, out m_national_currency, out m_currencies);

        m_currencies_amount = new SortedDictionary<CurrencyIsoType, Decimal>(new CurrencyIsoType.SortComparer());
        lbl_cage_currency_total.Visible = false;

        uc_payment_method1.CurrencyExchange = m_national_currency;
        //ShowCurrencyExchange();      
        //HideAllCurrencyExchange();
        // JMV 
        //SetCurrencyExchange();

        Boolean _allowed_money_entries;

        // To set grid data source property.
        _allowed_money_entries = true;
        if (m_cage_enabled)
        {
          // Set form mode (enabled/disabled) with returned value.
          _allowed_money_entries = SetDenominationDataSource(uc_payment_method1.CurrencyExchange);
        }

        AllowedCurrency(_allowed_money_entries);

        InitMoneyButtons();

        // END JMV 

        //DLL////////////////////////////////////////////////////
        btn_ok.Visible = true;
        Amount = new SortedDictionary<CurrencyIsoType, Decimal>(new CurrencyIsoType.SortComparer());
        CheckRedeem = false;
        m_out_check_redeem = false;
        SessionStats = new WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS();

        FormTitle = CaptionText;
        txt_amount.Text = "";
        lbl_total_to_pay.Text = "";
        m_cancel = false;

        web_browser.Navigate("");

        m_voucher_amount = 0;
        m_voucher_type = VoucherTypes.MBCardCashDeposit;
        uc_payment_method1.VoucherType = m_voucher_type;
        m_mb_card_data = Card;
        m_aux_amount = 0;
        m_cash_mode = CASH_MODE.TOTAL_REDEEM;
        btn_ok.Enabled = false;

        // Use the active common grid
        // It must be restored upon exit

        RestoreCommonGrid();

        m_cage_enabled = Cage.IsCageEnabled();
        m_cage_automode = Cage.IsCageAutoMode();
        m_tito_enabled = Common.TITO.Utils.IsTitoMode();
        m_tito_tickets_collection = GeneralParam.GetBoolean("TITO", "Cashier.TicketsCollection", false);
        m_cage_allow_denomination_in_balance = GeneralParam.GetBoolean("Cage", "AllowDenominationImbalance", false);
        _show_denominations = GeneralParam.GetBoolean("MobileBank", "Deposit.ShowDenominations", false);

        m_cage_enabled = m_cage_enabled && _show_denominations;

        m_show_voucher = !m_cage_enabled;

        // When cage enabled and show denominations general param is true, we show denomination grid
        if (m_cage_enabled)
        {
          btn_show_hide_rows.Visible = m_cage_enabled;   //18-Apr-17 RGR
          RebuildForm();
          CageFunctionalityMB();
        }
        else
        {
          ResetRebuildForm();
          HideAllCurrencyExchange();
          HideCommonGrid();

          gb_total.Visible = false;
          pnl_buttons.Width = web_browser.Width;
          pnl_buttons.Location = new Point(web_browser.Location.X, web_browser.Location.Y + web_browser.Height + _margin_between_controls);
          btn_ok.Location = new Point(pnl_buttons.Width - btn_ok.Width, btn_cancel.Location.Y);
        }

        //  - Money buttons 
        // when cage mode is enabled and there aren't pending movements number buttons are disabled.
        if (m_allowed_entries || !m_cage_enabled)
        {
          InitMoneyButtons();
        }

        //SetButtonsColor(System.Drawing.Color.LightGoldenrodYellow);

        promo_filter.Visible = (m_voucher_type == VoucherTypes.CardAdd || m_voucher_type == VoucherTypes.CancelPromotions);
        promo_filter.FilterChange += new uc_promo_filter.FilterChangeEvent(promo_filter_FilterChange);

        // RCI 23-JAN-2013: Payment Order
        btn_check_redeem.Enabled = false;
        btn_check_redeem.Visible = false;
        m_payment_order_enabled = false;
        m_payment_order_min_amount = 0;

        gp_digits_box.Enabled = true;

        TotalAmount = 0;

        if (m_cancel)
        {
          RestoreCommonGrid();

          return false;
        }
        DialogResult _dlg_rc = this.ShowDialog(Parent);

        if (_dlg_rc != DialogResult.OK || m_cancel)
        {
          RestoreCommonGrid();

          return false;
        }

        _iso_type.IsoCode = m_national_currency.CurrencyCode;
        _iso_type.Type = m_national_currency.Type;

        if (uc_payment_method1.CurrencyExchange != null)
        {
          _iso_type.IsoCode = uc_payment_method1.CurrencyExchange.CurrencyCode;
          _iso_type.Type = uc_payment_method1.CurrencyExchange.Type;
        }

        Amount.Add(_iso_type, m_out_amount);

        SessionStats = m_session_stats;
        CheckRedeem = m_out_check_redeem;

        if (Parent != null)
        {
          Parent.Focus();
        }

        RestoreCommonGrid();

        if (Amount != null)
        {
          foreach (KeyValuePair<CurrencyIsoType, decimal> kvp in Amount)
          {
            TotalAmount += kvp.Value;
          }
        }
      }
      finally
      {
        m_cage_enabled = Cage.IsCageEnabled();
      }

      return true;

    } // ShowMobileBanks


    private void ShowCancelablePromos()
    {
      Int32 _margin_between_controls;

      _margin_between_controls = 5;
      //this.promo_filter.FilterChange += new uc_promo_filter.FilterChangeEvent(promo_filter_FilterChange);

      //working with promotions in grid
      m_grid_type = GridType.Promotions;

      Int16 _row_to_show;

      _row_to_show = 11;
      this.SuspendLayout();

      _prev_loc.Clear();
      _prev_loc.Add(btn_promos_change_view.Location);
      _prev_loc.Add(gb_promotion_info.Location);

      _prev_size.Clear();
      _prev_size.Add(gb_common.Size);  //{Width=264, Height=538}
      _prev_size.Add(dgv_common.Size); //{Width=244, Height=238}

      dgv_common.Width = 690;
      dgv_common.Height = (dgv_common.RowTemplateHeight * _row_to_show) + dgv_common.ColumnHeadersHeight + 1;

      gp_digits_box.Visible = false;
      gb_tokens.Visible = false;
      web_browser.Visible = false;
      gb_promotion_info.Visible = false;
      gb_total.Visible = false;

      gb_common.Width += web_browser.Left - gb_common.Width - gb_common.Left + web_browser.Width;
      gb_common.Height += pnl_buttons.Top - gb_common.Height - gb_common.Top + pnl_buttons.Height;
      promo_filter.Location = new Point(dgv_common.Location.Y, dgv_common.Height);

      btn_cancel_by_cashier.Visible = true;

      gb_common.Controls.Add(btn_cancel);

      btn_promos_change_view.Visible = false;
      btn_promos_change_view.Enabled = false;

      btn_cancel.Visible = true;

      _prev_loc.Add(btn_cancel.Location);
      _prev_size.Add(btn_cancel.Size);

      btn_cancel.Size = btn_cancel_by_cashier.Size;
      btn_cancel.Left = gb_common.Width - btn_promos_change_view.Width;
      btn_cancel.Top = gb_common.Height - btn_cancel.Height - 2 * _margin_between_controls;

      btn_cancel_by_cashier.Visible = true;
      btn_cancel_by_cashier.Enabled = true;
      btn_cancel_by_cashier.Left = gb_common.Width - btn_cancel_by_cashier.Width;
      btn_cancel_by_cashier.Top = btn_cancel.Top - btn_cancel_by_cashier.Height - _margin_between_controls;

      Int32 _dgv_real_width;
      if (_row_to_show < dgv_common.Rows.Count)
      {
        _dgv_real_width = dgv_common.Width + 15; // + SCROLL
      }
      else
      {
        _dgv_real_width = dgv_common.Width;
      }

      PopulateAccountPromotions();

      dgv_common.Columns[2].Width = (Int32)(0.50 * _dgv_real_width);
      dgv_common.Columns[5].Width = (Int32)(0.25 * _dgv_real_width);
      dgv_common.Columns[0].Width = 35;

      FormatDataGridCancelablePromos();
    }

    private void PopulateAccountPromotions()
    {
      DataTable _cancelable_promotions;

      if (WSI.Common.AccountPromotion.GetCancelablePromotions(m_card_data.AccountId, out _cancelable_promotions))
      {
        _cancelable_promotions.Columns[8].ColumnName = "SORT";

        int _credit_type;
        String _str_credit_type;
        String _str_reward;
        String _str_credit;
        Currency _reward;

        _str_credit_type = "";
        _str_reward = "";

        foreach (DataRow _account_promo in _cancelable_promotions.Rows)
        {
          _credit_type = (int)_account_promo["ACP_CREDIT_TYPE"];
          _reward = (Decimal)_account_promo[7];

          switch ((ACCOUNT_PROMO_CREDIT_TYPE)_credit_type)
          {
            case ACCOUNT_PROMO_CREDIT_TYPE.NR1:
            case ACCOUNT_PROMO_CREDIT_TYPE.NR2:
              _str_credit_type = "NR";
              _str_credit = ((Currency)_reward).ToString();
              _account_promo["SORT"] = "1";
              break;

            case ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE:
              _account_promo["SORT"] = "3";
              _str_credit_type = "RE";
              _str_credit = ((Currency)_reward).ToString();
              break;

            case ACCOUNT_PROMO_CREDIT_TYPE.POINT:
              _account_promo["SORT"] = "4";
              _str_credit_type = "PT";
              _str_credit = ((Points)_reward.SqlMoney).ToString();
              break;

            case ACCOUNT_PROMO_CREDIT_TYPE.UNKNOWN:
            default:
              continue;
          }

          if (_account_promo["ACP_INI_WONLOCK"] != DBNull.Value)
          {
            _account_promo["SORT"] = "2";
          }

          _str_reward = String.Format("{0} {1}", _str_credit, _str_credit_type);
          _account_promo[6] = _str_reward;
        }

        _cancelable_promotions.DefaultView.Sort = "SORT,ACP_PROMO_NAME";
        dgv_common.DataSource = _cancelable_promotions;
        SelectCurrentPromoGrid();
      }

      gb_common.PanelBackColor = CashierStyle.Colors.m_colors_dic[CashierStyle.Colors.ENUM_COLORS.HEX_E3E7E9];

      m_promos_expanded_view = true;
      btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CLOSE");
      btn_cancel_by_cashier.Text = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_032");
      this.ResumeLayout();
    }

    void promo_filter_FilterChange(object sender)
    {
      DataTable _dt;
      _dt = (DataTable)dgv_common.DataSource;
      _dt.DefaultView.RowFilter = ((uc_promo_filter)sender).m_filter;
      SelectCurrentPromoGrid();
      setImages();
    }

    /// <summary>
    /// Init and Show the form
    /// </summary>
    /// <param name="CaptionText"></param>
    /// <param name="Amount"></param>
    /// <param name="Parent"></param>
    /// <returns></returns>
    public Boolean ShowMBPublic(String CaptionText, out Currency Amount, VoucherTypes VoucherType, MBCardData Card, Form Parent)
    {
      Int32 _margin_between_controls;
      Int32 _margin_header;

      _margin_header = 55;
      _margin_between_controls = 11;

      Amount = 0;
      FormTitle = CaptionText;
      txt_amount.Text = "";
      m_cancel = false;

      // EOR 15-MAY-2017
      btn_show_hide_rows.Visible = false;
      m_cage_enabled = false;

      //Currency icon and label only visible Card Add
      HideAllCurrencyExchange();

      // Promotions grid is only visible for Card Cash In movements
      // It must be restored upon exit
      HideCommonGrid();

      web_browser.Navigate("");
      lbl_total_to_pay.Text = "";

      //  - Money buttons 
      InitMoneyButtons();

      m_voucher_amount = 0;
      m_voucher_type = VoucherType;
      uc_payment_method1.VoucherType = m_voucher_type;
      m_mb_card_data = Card;
      m_aux_amount = 0;
      m_cash_mode = CASH_MODE.TOTAL_REDEEM;
      btn_ok.Enabled = false;

      // JMV
      web_browser.Visible = true;
      btn_clear.Visible = false;
      m_cage_enabled = false;
      gb_total.Visible = false;

      //SetButtonsColor(System.Drawing.Color.LightGoldenrodYellow);

      gp_digits_box.Enabled = true;
      //web_browser.Enabled = true;

      // RCI 23-JAN-2013: Payment Order
      pnl_buttons.Width = web_browser.Width;
      pnl_buttons.Location = new Point(web_browser.Location.X, web_browser.Location.Y + web_browser.Height + _margin_between_controls);
      btn_ok.Location = new Point(pnl_buttons.Width - btn_ok.Width, btn_cancel.Location.Y);
      btn_check_redeem.Enabled = false;
      btn_check_redeem.Visible = false;

      // Change Height
      this.Height = _margin_header + pnl_buttons.Location.Y + pnl_buttons.Height + _margin_between_controls;

      if (m_cancel)
      {
        RestoreCommonGrid();

        return false;
      }

      DialogResult _dlg_rc = this.ShowDialog(Parent);

      if (_dlg_rc != DialogResult.OK || m_cancel)
      {
        RestoreCommonGrid();

        return false;
      }

      Amount = m_out_amount;

      if (Parent != null)
      {
        Parent.Focus();
      }

      RestoreCommonGrid();

      return true;
    } // ShowMBPublic


    //------------------------------------------------------------------------------
    // PURPOSE : Show method only for Win/Loss screen 
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public Boolean ShowWinLossPublic(String CaptionText,
                VoucherTypes VoucherType,
                Form Parent,
                List<Int64> GTWinLossIds,
                out CageDenominationsItems.CageDenominationsDictionary CageAmounts,
                out GamingTableBusinessLogic.GT_WIN_LOSS_MODE WinLossMode)
    {
      Cage.ShowDenominationsMode _show_denominations_mode;
      SortedDictionary<CurrencyIsoType, Decimal> _total_amounts;

      m_win_loss_mode = GamingTableBusinessLogic.GetGTWinLossMode();
      WinLossMode = m_win_loss_mode;
      m_gt_win_loss_ids = GTWinLossIds;
      m_win_loss_amount_entry_screen_is_open = false;

      _show_denominations_mode = (Cage.ShowDenominationsMode)GeneralParam.GetInt32("Cage", "ShowDenominations", 1);
      if (VoucherType == VoucherTypes.WinLoss && m_win_loss_mode == GamingTableBusinessLogic.GT_WIN_LOSS_MODE.BY_TOTALS)
      {
        _show_denominations_mode = Cage.ShowDenominationsMode.HideAllDenominations;
      }

      m_junket_info = null;

      uc_payment_method1.HeaderText = Resource.String("STR_FRM_AMOUNT_CURRENCY_GROUP_BOX");

      // DHA 02-MAR-2015: Load all currencies defined on the system
      List<CurrencyExchangeType> _types = new List<CurrencyExchangeType>();

      btn_show_hide_rows.Visible = false;

      _types.Add(CurrencyExchangeType.CURRENCY);
      if (GamingTableBusinessLogic.IsGamingTablesEnabled())
      {
        _types.Add(CurrencyExchangeType.CASINOCHIP);
      }

      CurrencyExchange.GetAllowedCurrencies(true, _types, false, out m_national_currency, out m_currencies);
      FeatureChips.ReloadChipsAndSets(_show_denominations_mode);

      m_voucher_type = VoucherType;
      uc_payment_method1.VoucherType = m_voucher_type;
      uc_payment_method1.InitControl(m_currencies, GamingTableId, ConvertToChipsOperationType(VoucherType));
      m_voucher_amount = 0;
      btn_ok.Enabled = false;

      m_currencies_amount = new SortedDictionary<CurrencyIsoType, Decimal>(new CurrencyIsoType.SortComparer());
      lbl_cage_currency_total.Visible = false;

      uc_payment_method1.CurrencyExchange = m_national_currency;
      ShowCurrencyExchange();
      SetCurrencyExchange();

      gb_total.Visible = false;

      btn_ok.Visible = true;
      _total_amounts = new SortedDictionary<CurrencyIsoType, Decimal>(new CurrencyIsoType.SortComparer());
      CageAmounts = new CageDenominationsItems.CageDenominationsDictionary();

      FormTitle = CaptionText;
      txt_amount.Text = "";
      lbl_total_to_pay.Text = "";
      m_cancel = false;

      RestoreCommonGrid();

      m_cage_enabled = Cage.IsCageEnabled();
      m_cage_automode = Cage.IsCageAutoMode();
      m_tito_enabled = Common.TITO.Utils.IsTitoMode();
      m_tito_tickets_collection = GeneralParam.GetBoolean("TITO", "Cashier.TicketsCollection", false);
      m_cage_allow_denomination_in_balance = GeneralParam.GetBoolean("Cage", "AllowDenominationImbalance", false);

      if (m_cage_enabled)
      {
        // ATB 03-NOV-2016
        // This button must be visible only if the cage is enabled
        btn_show_hide_rows.Visible = true;
        RebuildForm();
        CageFunctionality(VoucherType);
      }
      else
      {
        ResetRebuildForm();
        //Always return national currency. We need almost 2 currencies
        if (m_currencies != null && m_currencies.Count > 1)
        {
          ShowCurrencyExchange();
          SetCurrencyExchange();
        }
        else
        {
          HideAllCurrencyExchange();
        }

        HideCommonGrid();
      }

      //  - Money buttons 
      // when cage mode is enabled and there aren't pending movements number buttons are disabled.
      if (m_allowed_entries || !m_cage_enabled)
      {
        InitMoneyButtons();
      }

      promo_filter.Visible = false;
      promo_filter.FilterChange += new uc_promo_filter.FilterChangeEvent(promo_filter_FilterChange);

      btn_check_redeem.Enabled = false;
      btn_check_redeem.Visible = false;
      m_payment_order_enabled = PaymentOrder.IsEnabled();
      m_payment_order_min_amount = PaymentOrder.MinAmount();

      //HideAllDenominations;
      HideAllCurrencyExchange();

      if (m_win_loss_mode == GamingTableBusinessLogic.GT_WIN_LOSS_MODE.BY_TOTALS)
      {
        Int32 _des_x;
        _des_x = gp_digits_box.Location.X - 10;
        gb_common.Visible = false;
        btn_clear.Visible = false;
        btn_show_hide_rows.Visible = false;
        uc_payment_method1.Location = new Point(uc_payment_method1.Location.X - _des_x, uc_payment_method1.Location.Y);
        gp_digits_box.Location = new Point(gp_digits_box.Location.X - _des_x, gp_digits_box.Location.Y);
        pnl_buttons.Location = new Point(gp_digits_box.Location.X, gp_digits_box.Location.Y + gp_digits_box.Height + 5);
        this.Width = 400;
        this.Height = pnl_buttons.Location.Y + pnl_buttons.Height + 70;
        btn_ok.Visible = false;
      }

      UpdateTotalCageAmount();
      gp_digits_box.Enabled = true;

      //ShowHideBtnApplyTax();
      //btn_apply_tax.Visible = false;
      if (m_cancel)
      {
        RestoreCommonGrid();

        return false;
      }

      m_win_loss_amount_entry_screen_is_open = true;
      DialogResult _dlg_rc = this.ShowDialog(Parent);

      if (_dlg_rc != DialogResult.OK || m_cancel)
      {
        RestoreCommonGrid();

        return false;
      }

      _total_amounts = m_currencies_amount;
      CageAmounts = m_cage_amounts;

      if (Parent != null)
      {
        Parent.Focus();
      }

      RestoreCommonGrid();

      return true;
    } // ShowWinLossPublic

    #endregion

    #region Private Methods

    /// <summary>
    /// Initialize control resources (NLS strings, images, etc)
    /// </summary>
    public void InitializeControlResources()
    {

      // - NLS Strings:
      //   - Title: External set

      //   - Labels
      gp_digits_box.Text = Resource.String("STR_FRM_AMOUNT_INPUT_DIGITS_GROUP_BOX");
      uc_payment_method1.HeaderText = Resource.String("STR_FRM_AMOUNT_CURRENCY_GROUP_BOX");


      //   - Buttons
      btn_ok.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_OK");
      btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");
      btn_check_redeem.Text = Resource.String("STR_ACCOUNT_PAYMENT_ORDER_CHECK_REPAYMENT");

      //btn_intro.Text = Resource.String("STR_FRM_AMOUNT_INPUT_BTN_ENTER");

      grp_cash_type.Enabled = false;
      grp_cash_type.Visible = false;
      gb_tokens.Visible = true;
      btn_enter_tokens.Text = Resource.String("STR_FRM_AMOUNT_INPUT_004");

      lbl_promo_reward_text.Text = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_001");

      btn_promos_change_view.Text = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_019");
      promo_filter.Visible = false;

      gb_total.HeaderText = Resource.String("STR_FRM_INPUT_TOTAL_TO_PAY");

      gb_tokens.HeaderText = Resource.String("STR_FRM_AMOUNT_INPUT_004");
      gb_promotion_info.HeaderText = Resource.String("STR_FRM_AMOUNT_INPUT_007");

      ResetPromoRewardArea();
      ShowPromosContractedView();

      //  - Money buttons 
      InitMoneyButtons();

      // - Images:

      // - Customize Events:

      // Initialize members
      m_total_currency_amount = 0;

      // ATB 02-NOV-2016
      m_voucher_card = new Voucher();

      m_barcode = "";

    } // InitializeControlResources

    /// <summary>
    /// Check and Add digit to amount
    /// </summary>
    /// <param name="NumberStr"></param>
    private void AddNumber(String NumberStr)
    {
      String aux_str;
      String tentative_number;
      Currency input_amount;
      int dec_symbol_pos;
      Decimal _actual_amount;

      if (Decimal.TryParse(txt_amount.Text, out _actual_amount))
      {
        if (_actual_amount == 0 && txt_amount.Text != "0" + m_decimal_str)
        {
          txt_amount.Text = "";
        }
      }

      // Prevent exceeding maximum number of decimals (2)
      dec_symbol_pos = txt_amount.Text.IndexOf(m_decimal_str);
      if (dec_symbol_pos > -1)
      {
        aux_str = txt_amount.Text.Substring(dec_symbol_pos);
        if (aux_str.Length > 2)
        {
          return;
        }
      }

      // Prevent exceeding maximum amount length
      if (txt_amount.Text.Length >= Cashier.MAX_ALLOWED_AMOUNT_LENGTH)
      {
        return;
      }

      tentative_number = txt_amount.Text + NumberStr;

      try
      {
        input_amount = Decimal.Parse(tentative_number);
        if (input_amount > Cashier.MAX_ALLOWED_AMOUNT)
        {
          return;
        }
      }
      catch
      {
        input_amount = 0;

        return;
      }

      // Remove unneeded leading zeros
      // Unless we are entering a decimal value, there must not be any leading 0
      if (dec_symbol_pos == -1)
      {
        txt_amount.Text = txt_amount.Text.TrimStart('0') + NumberStr;
      }
      else
      {
        // Accept new symbol/digit 
        txt_amount.Text = txt_amount.Text + NumberStr;
      }
      btn_intro.Focus();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Show the Input amount form for the card add credit operation.
    //
    //  PARAMS :
    //      - INPUT :
    //        - Title
    //        - voucherTypes
    //        - card_data
    //        - p_6: not used
    //        - subtractCash_Type: not used 
    //        - form_yes_no
    //
    //      - OUTPUT :
    //        - Promotion: deprecated
    //        - CashRedeemable: deprecated
    //        - amount_to_add: final amount to be paid by player
    //        - OperationData: complete data about card add operation
    //
    // RETURNS :
    //        - True: operation sucessful
    //        - False: operation cancelled
    //
    //   NOTES :
    //

    internal bool Show(string Title,
                       VoucherTypes voucherTypes,
                       CardData card_data,
                       int p_6,
                       CASH_MODE subtractCash_Type,
                       frm_yesno form_yes_no,
                       out Object OperationData,
                       bool IsChipsSaleWithRecharge = false)
    {
      bool return_value;
      Currency _amount_to_add;
      CashierOperations.TYPE_CARD_CASH_IN operation_data;

      // Initialize web browser      
      is_voucher_loaded = false;

      OperationData = null;
      m_dt_remaining_spent_per_provider = null;
      m_dt_remaining_spent_per_terminal = null;

      return_value = this.Show(Title, out _amount_to_add, voucherTypes, card_data, p_6, subtractCash_Type, form_yes_no, IsChipsSaleWithRecharge);
      // New method to return results
      operation_data = new CashierOperations.TYPE_CARD_CASH_IN();
      operation_data.amount_to_pay = _amount_to_add;
      operation_data.num_tokens = this.m_promo_num_tokens;
      operation_data.promotion_id = this.m_selected_promo_id;
      operation_data.reward = this.m_promo_reward_amount;
      operation_data.won_lock = this.m_promo_won_lock;
      operation_data.spent_used = this.m_spent_used;
      operation_data.junketinfo = this.JunketInfo != null ? this.JunketInfo.BusinessLogic : null;

      OperationData = operation_data;

      return return_value;
    } // Show


    // DLL 20-JUN-2013 New Show for CurrencyExchange
    //------------------------------------------------------------------------------
    // PURPOSE : Show the Input amount form for the card add credit operation.
    //
    //  PARAMS :
    //      - INPUT :
    //        - Title
    //        - voucherTypes
    //        - Card
    //        - FormYesNo
    //        - Exchange
    //        - Exchange
    //        - OperationData
    //
    //      - OUTPUT :
    //        - ExchangeResult
    //        - OperationData: complete data about card add operation
    //
    // RETURNS :
    //        - True: operation sucessful
    //        - False: operation cancelled
    //
    //   NOTES :
    //

    internal Boolean Show(string Title,
                       VoucherTypes VoucherType,
                       CardData Card,
                       frm_yesno FormYesNo,
                       out ParticipateInCashDeskDraw ParticipateInCashDeskDraw,
                       out CurrencyExchangeResult ExchangeResult,
                       out Object OperationData)
    {
      Boolean return_value;

      try
      {
        m_participate_in_cashdesk_draw = ParticipateInCashDeskDraw.Default;
        return_value = this.Show(Title, VoucherType, Card, 0, CASH_MODE.TOTAL_REDEEM, FormYesNo, out OperationData);
        ParticipateInCashDeskDraw = m_participate_in_cashdesk_draw;

        ExchangeResult = m_exchange_result;
      }
      finally
      {
        HideAllCurrencyExchange();
      }

      return return_value;
    } // Show

    internal Boolean Show(string Title,
                   VoucherTypes VoucherType,
                   CardData Card,
                   frm_yesno FormYesNo,
                   out Decimal Amount)
    {
      ParticipateInCashDeskDraw _participate_in_cash_desk_draw;
      CurrencyExchangeResult _exchange_result;
      Object _operation_data;
      FeatureChips.ChipsOperation _chips_amount;
      Boolean _rc;
      PaymentThresholdAuthorization _payment_threshold_authorization;

      _chips_amount = new FeatureChips.ChipsOperation(CashierSessionInfo.CashierSessionId, CASHIER_MOVEMENT.CHIPS_SALE);

      _rc = this.Show(Title, VoucherType, Card, FormYesNo, out _participate_in_cash_desk_draw, out _exchange_result, out _operation_data, out _payment_threshold_authorization, ref _chips_amount);

      Amount = _chips_amount.ChipsAmount;

      return _rc;
    }

    internal Boolean Show(string Title,
                   VoucherTypes VoucherType,
                   CardData Card,
                   frm_yesno FormYesNo,
                   Boolean IsChipsSaleAtTableRegister,
                   out ParticipateInCashDeskDraw ParticipateInCashDeskDraw,
                   out CurrencyExchangeResult ExchangeResult,
                   out Object OperationData,
                   out PaymentThresholdAuthorization PaymentThresholdAuthorization,
                   ref FeatureChips.ChipsOperation ChipsAmounts)
    {
      m_is_chips_sale_at_table_register = IsChipsSaleAtTableRegister;

      return Show(Title, VoucherType, Card, FormYesNo, out ParticipateInCashDeskDraw, out ExchangeResult, out OperationData, out PaymentThresholdAuthorization, ref ChipsAmounts, VoucherType == VoucherTypes.ChipsSaleWithRecharge);
    }


    //DLL////////////////////////////////////////////////////
    internal Boolean Show(string Title,
                       VoucherTypes VoucherType,
                       CardData Card,
                       frm_yesno FormYesNo,
                       out ParticipateInCashDeskDraw ParticipateInCashDeskDraw,
                       out CurrencyExchangeResult ExchangeResult,
                       out Object OperationData,
                       out PaymentThresholdAuthorization PaymentThresholdAuthorization,
                       ref FeatureChips.ChipsOperation ChipsAmounts,
                       bool IsChipsSaleWithRecharge = false)
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_amount_input", Log.Type.Message);
      Boolean return_value;

      m_chips_amounts = ChipsAmounts;

      try
      {
        m_is_chips_operation = (VoucherType == VoucherTypes.ChipsSaleWithRecharge || VoucherType == VoucherTypes.ChipsSaleAmountInput ||
                                VoucherType == VoucherTypes.ChipsPurchaseAmountInput);
        if (VoucherType == VoucherTypes.ChipsSaleWithRecharge)
        {
          VoucherType = VoucherTypes.CardAdd;
        }
        else if (VoucherType == VoucherTypes.GenericAmountInput)
        {
          VoucherType = VoucherTypes.ChipsSaleAmountInput;
        }

        m_participate_in_cashdesk_draw = ParticipateInCashDeskDraw.Default;
        return_value = this.Show(Title, VoucherType, Card, 0, CASH_MODE.TOTAL_REDEEM, FormYesNo, out OperationData, IsChipsSaleWithRecharge);
        ParticipateInCashDeskDraw = m_participate_in_cashdesk_draw;

        ExchangeResult = m_exchange_result;
        ChipsAmounts = m_chips_amounts;
        PaymentThresholdAuthorization = this.m_payment_threshold_authorization;
      }
      finally
      {
        HideAllCurrencyExchange();
        m_is_chips_operation = false;
        m_chips_promo_re = 0;
        m_chips_amounts = null;
        m_is_chips_sale_at_table_register = false;
      }

      return return_value;
    } // Show
    //DLL////////////////////////////////////////////////////

    internal bool ShowMB(string Title, out Currency amount_to_add, VoucherTypes voucherTypes, MBCardData card_data, frm_yesno form_yes_no)
    {
      bool return_value;

      // Initialize web browser      
      is_voucher_loaded = false;
      // Init control to accept redeem cash type
      this.grp_cash_type.Visible = false;

      return_value = this.ShowMBPublic(Title, out amount_to_add, voucherTypes, card_data, form_yes_no);

      this.grp_cash_type.Visible = false;

      return return_value;
    } // ShowMB


    private void frm_amount_input_Shown(object sender, EventArgs e)
    {

      m_show_currency_exchange = GeneralParam.GetBoolean("Cashier.Recharge", "ShowCurrencyExchange", false);

      switch (m_voucher_type)
      {
        case VoucherTypes.CardAdd:
          m_previously_calculated = false;
          ShowPromosContractedView();
          break;

        case VoucherTypes.CardRedeem:
          gp_digits_box.Visible = true;
          gb_tokens.Visible = false;
          web_browser.Visible = true;
          break;

      } // end switch

      // ATB 03-NOV-2016
      // When the parameter ShowCurrencyExchange is active, the currency window must be opened
      // For win/loss only National Currency
      if (m_show_currency_exchange && m_voucher_type != VoucherTypes.WinLoss)
      {
        uc_payment_method1.ShowCurrenciesForm(false);
      }

      this.dgv_common.Focus();

      AutoClickIntroButton();

    }

    private void frm_amount_input_KeyPress(object sender, KeyPressEventArgs e)
    {
      String aux_str;
      char c;

      if (m_cage_enabled && m_voucher_type == VoucherTypes.OpenCash)
      {
        return;
      }

      c = e.KeyChar;

      if (c >= '0' && c <= '9')
      {
        aux_str = "" + c;
        AddNumber(aux_str);

        e.Handled = true;
      }

      if (c == '\r')
      {
        btn_intro_Click(null, null);
        e.Handled = true;
      }

      if (c == '\b')
      {
        btn_back_Click(null, null);

        e.Handled = true;
      }

      if (c == '.')
      {
        if (btn_num_dot.Enabled)
        {
          btn_num_dot_Click(null, null);

          e.Handled = true;
        }
      }

      m_show_msg = false;
      e.Handled = false;
    }

    private void splitContainer1_KeyPress(object sender, KeyPressEventArgs e)
    {
      frm_amount_input_KeyPress(sender, e);
    }

    private void frm_amount_input_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
    {
      char c;

      c = Convert.ToChar(e.KeyCode);
      if (c == '\r')
      {
        e.IsInputKey = true;
      }
    }

    //private Boolean CheckNoRedeemablePermissions(out String ErrorStr)
    //{
    //  string sql_str;
    //  SqlCommand sql_command;
    //  SqlConnection sql_conn;
    //  SqlTransaction sql_trx;
    //  int profile_id;

    //  ErrorStr = null;
    //  sql_conn = null;
    //  sql_trx = null;

    //  try
    //  {
    //    // Get Sql Connection 
    //    sql_conn = WSI.Common.WGDB.Connection();
    //    sql_trx = sql_conn.BeginTransaction();

    //    sql_str = "SELECT GU_PROFILE_ID " +
    //               " FROM GUI_USERS " +
    //              " WHERE GU_USER_ID = @p1 ";

    //    sql_command = new SqlCommand(sql_str);
    //    sql_command.Connection = sql_trx.Connection;
    //    sql_command.Transaction = sql_trx;

    //    sql_command.Parameters.Add("@p1", SqlDbType.Int, 4, "GU_USER_ID").Value = Cashier.UserId;

    //    profile_id = 0;
    //    profile_id = (Int32)sql_command.ExecuteScalar();

    //    sql_str = "SELECT GPF_READ_PERM " +
    //               " FROM GUI_PROFILE_FORMS " +
    //              " WHERE GPF_PROFILE_ID = @p1 " +
    //                " AND GPF_FORM_ID = @p2 " +
    //                " AND GPF_GUI_ID = @p3 ";

    //    sql_command = new SqlCommand(sql_str);
    //    sql_command.Connection = sql_trx.Connection;
    //    sql_command.Transaction = sql_trx;

    //    sql_command.Parameters.Add("@p1", SqlDbType.Int, 4, "GPF_PROFILE_ID").Value = profile_id;
    //    sql_command.Parameters.Add("@p2", SqlDbType.Int, 4, "GPF_FORM_ID").Value = 1;
    //    sql_command.Parameters.Add("@p3", SqlDbType.Int, 4, "GPF_GUI_ID").Value = 15;

    //    if (sql_command.ExecuteScalar() == null)
    //    {
    //      ErrorStr = Resource.String("STR_FRM_USER_LOGIN_ERROR_LOGIN_PERMS");

    //      return false;
    //    }
    //    else
    //    {
    //      Boolean read_perm;

    //      read_perm = (Boolean)sql_command.ExecuteScalar();
    //      if (!read_perm)
    //      {
    //        ErrorStr = Resource.String("STR_FRM_USER_LOGIN_ERROR_LOGIN_PERMS");

    //        return false;
    //      }
    //    }
    //  }
    //  catch (Exception ex)
    //  {
    //    sql_trx.Rollback();
    //    Log.Exception(ex);
    //  }
    //  finally
    //  {
    //    if (sql_trx != null)
    //    {
    //      if (sql_trx.Connection != null)
    //      {
    //        sql_trx.Rollback();
    //      }
    //      sql_trx.Dispose();
    //      sql_trx = null;
    //    }

    //    // Close connection
    //    if (sql_conn != null)
    //    {
    //      sql_conn.Close();
    //      sql_conn = null;
    //    }
    //  }

    //  return true;
    //}

    //------------------------------------------------------------------------------
    // PURPOSE : Hide common grid and related elements when operation is not 
    //           Card Add Credit.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //    - Entire form is reduced and some elements are moved as a consequence

    private void HideCommonGrid()
    {
      Int32 _margin_header;
      Int32 _margin_between_controls;

      _margin_header = 55;
      _margin_between_controls = 11;

      dgv_common.Enabled = false;
      gb_common.Hide();

      gb_tokens.Enabled = false;
      gb_tokens.Visible = false;
      gb_promotion_info.Visible = false;

      m_grid_type = GridType.None;

      // Relocate other elements in the screen
      gp_digits_box.Location = new Point(_margin_between_controls, gp_digits_box.Location.Y);
      grp_cash_type.Location = new Point(_margin_between_controls, grp_cash_type.Location.Y);

      web_browser.Location = new Point(gp_digits_box.Location.X + gp_digits_box.Width + _margin_between_controls, gb_common.Location.Y + 2);

      gb_total.Location = new Point(web_browser.Location.X, web_browser.Location.Y + web_browser.Height + _margin_between_controls);

      if (m_voucher_type == VoucherTypes.CardAdd || m_voucher_type == VoucherTypes.CardRedeem)
      {
        pnl_buttons.Location = new Point(web_browser.Location.X, gb_total.Location.Y + gb_total.Height + _margin_between_controls);
      }
      else
      {
        pnl_buttons.Location = new Point(web_browser.Location.X, Math.Max(web_browser.Location.Y + web_browser.Height, gp_digits_box.Location.Y + gp_digits_box.Height) + _margin_between_controls);
      }
      uc_payment_method1.Location = new Point(_margin_between_controls, uc_payment_method1.Location.Y);

      // WIDTH USED FOR MOST VOUCHER TYPES
      this.Width = web_browser.Location.X + web_browser.Width + _margin_between_controls;
      this.Height = _margin_header + pnl_buttons.Location.Y + pnl_buttons.Height + _margin_between_controls;


    } // HideCommonGrid

    //------------------------------------------------------------------------------
    // PURPOSE : Restore promotion grid and related elements when operation is  
    //           Card Add Credit.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //    - Entire form is restored and some elements are moved as a consequence

    private void RestoreCommonGrid()
    {
      Int32 _margin_header;
      Int32 _margin_between_controls;

      _margin_header = 55;
      _margin_between_controls = 11;

      btn_promos_change_view.Visible = false;
      btn_cancel_request.Visible = false;
      lbl_promo_input_values.Visible = false;
      gb_tokens.Enabled = false;
      gb_tokens.Visible = false;
      dgv_common.Enabled = true;
      gb_common.Show();

      // Relocate other elements in the screen
      gp_digits_box.Location = new Point(gb_common.Location.X + gb_common.Width + _margin_between_controls, gp_digits_box.Location.Y);
      grp_cash_type.Location = new Point(gb_common.Location.X + gb_common.Width + _margin_between_controls, grp_cash_type.Location.Y);
      web_browser.Location = new Point(gp_digits_box.Location.X + gp_digits_box.Width + _margin_between_controls, gb_common.Location.Y + 2);
      pnl_buttons.Location = new Point(web_browser.Location.X, gb_total.Location.Y + gb_total.Height + _margin_between_controls);
      uc_payment_method1.Location = new Point(gb_common.Location.X + gb_common.Width + _margin_between_controls, uc_payment_method1.Location.Y);
      gb_total.Location = new Point(web_browser.Location.X, web_browser.Location.Y + web_browser.Height + _margin_between_controls);

      // ORIGINAL FORM WIDTH VALUES
      this.Width = web_browser.Location.X + web_browser.Width + _margin_between_controls;
      this.Height = _margin_header + pnl_buttons.Location.Y + pnl_buttons.Height + _margin_between_controls;

    } // RestoreCommonGrid

    //------------------------------------------------------------------------------
    // PURPOSE : Retrieve promotion items that are appliable to current account and
    //           show them in the common data grid.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //    

    private void PopulatePromotionsGrid()
    {
      SqlConnection _sql_conn;
      SqlTransaction _sql_trx;
      DataTable _dt;
      Promotion.PROMOTION_STATUS _promo_status;

      _sql_conn = null;
      _sql_trx = null;

      //working with promotions in grid
      m_grid_type = GridType.Promotions;

      m_input_amount = null;

      m_selected_promo_index = 0;
      m_selected_promo_id = 0;
      m_promo_reward_amount = 0;
      m_promo_won_lock = 0;
      m_promo_num_tokens = 0;
      m_spent_used = 0;
      m_promo_warning_shown = false;

      gb_tokens.Enabled = false;

      btn_promos_change_view.Visible = true;
      lbl_promo_input_values.Visible = true;
      gb_tokens.Enabled = true;
      gb_tokens.Visible = true;

      // Clear msg areas
      ResetPromoRewardArea();

      try
      {
        _sql_conn = WGDB.Connection();
        _sql_trx = _sql_conn.BeginTransaction();

        // ACC 18-JAN-2012: Calculate spent per provider
        if (m_dt_remaining_spent_per_provider == null)
        {
          m_dt_remaining_spent_per_provider = Promotion.AccountRemainingDataPerProvider(m_card_data.AccountId, _sql_trx);
        }

        Promotion.CalculateDataByProvider(m_dt_remaining_spent_per_provider, null, out m_input_spent, out m_input_played);

        UpdatePromoInputValues();

        //DLL////////////////////////////////////////////////////
        if (m_is_chips_operation && m_voucher_type == VoucherTypes.CardAdd)
        {
          List<ACCOUNT_PROMO_CREDIT_TYPE> _list = new List<ACCOUNT_PROMO_CREDIT_TYPE>();
          if (m_is_chips_sale_at_table_register)
          {
            _list.Add(ACCOUNT_PROMO_CREDIT_TYPE.UNKNOWN);
          }
          else
          {
            _list.Add(ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE);
            _list.Add(ACCOUNT_PROMO_CREDIT_TYPE.POINT);
          }
          _dt = Promotion.GetApplicablePromotionList(m_card_data.AccountId,
                                                     Resource.String("STR_FRM_AMOUNT_INPUT_002"),
                                                     _list,
                                                     out _promo_status,
                                                     _sql_trx);
        }
        else
        {
          //DLL////////////////////////////////////////////////////
          _dt = Promotion.GetApplicablePromotionList(m_card_data.AccountId,
                                                     Resource.String("STR_FRM_AMOUNT_INPUT_002"),
                                                     out _promo_status,
                                                     _sql_trx)
;
        }
        //
        // Merge additional data from promotion table
        //
        _dt.PrimaryKey = new DataColumn[] { _dt.Columns[1] };

        StringBuilder _sb;

        _sb = new StringBuilder();

        //GRID_COLUMN_CREDIT_TYPE = 3;
        //GRID_COLUMN_MIN_RECHARGE = 4;
        //GRID_COLUMN_MIN_RECHARGE_TEXT = 5;
        //GRID_COLUMN_REWARD = 6;
        //GRID_COLUMN_SORT = 7;

        _sb.AppendLine(" SELECT   PM_PROMOTION_ID ");
        _sb.AppendLine("        , PM_CREDIT_TYPE ");
        _sb.AppendLine("        , CASE ");
        _sb.AppendLine("            WHEN PM_MIN_CASH_IN > 0 THEN PM_MIN_CASH_IN ");
        _sb.AppendLine("            ELSE PM_CASH_IN ");
        _sb.AppendLine("          END         AS MIN_CASH_IN ");
        _sb.AppendLine("        , ''          AS CASH_IN_TEXT ");
        _sb.AppendLine("        , ''          AS REWARD");
        _sb.AppendLine("        , PM_WON_LOCK AS EXCLUDE_WON_LOCK");
        _sb.AppendLine("        , 0           AS SORT ");
        _sb.AppendLine("   FROM   PROMOTIONS ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _sql_conn, _sql_trx))
        {
          DataRow _dr_find;
          List<DataColumn> _ls_col_added;
          Decimal _pm_cash_in;

          _ls_col_added = new List<DataColumn>();

          using (DataTable _dt_to_merge = new DataTable())
          {
            _dt_to_merge.Load(_cmd.ExecuteReader());
            foreach (DataColumn _col in _dt_to_merge.Columns)
            {
              if (!_col.ColumnName.StartsWith("EXCLUDE_"))
              {
                if (!_dt.Columns.Contains(_col.ColumnName))
                {
                  _dt.Columns.Add(_col.ColumnName, _col.DataType);
                  _ls_col_added.Add(_col);
                }
              }
            }
            foreach (DataRow _dr_to_merge in _dt_to_merge.Rows)
            {
              _dr_find = _dt.Rows.Find(_dr_to_merge["PM_PROMOTION_ID"]);
              if (_dr_find != null)
              {
                foreach (DataColumn _col in _ls_col_added)
                {
                  switch (_col.ColumnName)
                  {
                    case "CASH_IN_TEXT":
                      if (_dr_to_merge.IsNull("EXCLUDE_WON_LOCK"))
                      {
                        _pm_cash_in = (Decimal)_dr_find["MIN_CASH_IN"];
                        if (_pm_cash_in > 0)
                        {
                          _dr_find[_col.ColumnName] = ((Currency)_pm_cash_in).ToString();
                        }
                      }
                      else
                      {
                        _dr_find[_col.ColumnName] = M_STR_WONLOCK;
                      }
                      break;
                    case "SORT":
                      // 1 NR, 2 CA(WONLOCK), 3 RE, 4 PT
                      if (!_dr_to_merge.IsNull("EXCLUDE_WON_LOCK"))
                      {
                        _dr_find[_col.ColumnName] = 2;
                      }
                      else
                      {
                        _dr_find[_col.ColumnName] = _dr_to_merge["PM_CREDIT_TYPE"];
                      }

                      break;
                    default:
                      _dr_find[_col.ColumnName] = _dr_to_merge[_col.ColumnName];
                      break;
                  }
                }
              }
            }

            //
            // Default values of "Sin promoci�n"
            //

            _dr_find = _dt.Rows.Find(0);
            if (_dr_find != null)
            {
              _dr_find["PM_CREDIT_TYPE"] = -1;
              _dr_find["SORT"] = -1;
            }
          }

        }

        // Fill grid with remaining rows
        _dt.DefaultView.Sort = "SORT,PM_NAME";
        dgv_common.DataSource = _dt;

        // Format common grid
        GuiStyleSheet();

        _sql_trx.Commit();

      } // try
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return;
      }

      finally
      {
        // Close connection
        if (_sql_trx != null)
        {
          if (_sql_trx.Connection != null)
          {
            _sql_trx.Rollback();
          }
          _sql_trx.Dispose();
          _sql_trx = null;
        }

        if (_sql_conn != null)
        {
          _sql_conn.Close();
          _sql_conn = null;
        }
      } // finally

    } // PopulatePromotionsGrid

    //------------------------------------------------------------------------------
    // PURPOSE : Format common data grid.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //    

    private void GuiStyleSheet()
    {
      // Columns
      dgv_common.Columns[GRID_COLUMN_PROMO_CREDIT_TYPE_IMAGE].HeaderCell.Value = "";
      dgv_common.Columns[GRID_COLUMN_PROMO_CREDIT_TYPE_IMAGE].Width = 40;
      dgv_common.Columns[GRID_COLUMN_PROMO_CREDIT_TYPE_IMAGE].Visible = true;
      dgv_common.Columns[GRID_COLUMN_PROMO_CREDIT_TYPE_IMAGE].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
      dgv_common.Columns[GRID_COLUMN_PROMO_CREDIT_TYPE_IMAGE].SortMode = DataGridViewColumnSortMode.NotSortable;
      dgv_common.Columns[GRID_COLUMN_PROMO_CREDIT_TYPE_IMAGE].Name = "IMAGE";
      dgv_common.Columns[GRID_COLUMN_PROMO_CREDIT_TYPE_IMAGE].DataPropertyName = "IMAGE";
      dgv_common.Columns[GRID_COLUMN_PROMO_CREDIT_TYPE_IMAGE].ValueType = typeof(DataGridViewImageColumn);

      dgv_common.Columns[GRID_COLUMN_PROMO_ID].Width = 0;
      dgv_common.Columns[GRID_COLUMN_PROMO_ID].Visible = false;
      dgv_common.Columns[GRID_COLUMN_PROMO_ID].SortMode = DataGridViewColumnSortMode.NotSortable;

      dgv_common.Columns[GRID_COLUMN_PROMO_NAME].HeaderCell.Value = Resource.String("STR_FRM_AMOUNT_INPUT_003");
      dgv_common.Columns[GRID_COLUMN_PROMO_NAME].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
      dgv_common.Columns[GRID_COLUMN_PROMO_NAME].Visible = true;
      dgv_common.Columns[GRID_COLUMN_PROMO_NAME].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
      dgv_common.Columns[GRID_COLUMN_PROMO_NAME].SortMode = DataGridViewColumnSortMode.NotSortable;

      dgv_common.Columns[GRID_COLUMN_PROMO_WON_LOCK].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
      dgv_common.Columns[GRID_COLUMN_PROMO_WON_LOCK].Width = 0;
      dgv_common.Columns[GRID_COLUMN_PROMO_WON_LOCK].Visible = false;
      dgv_common.Columns[GRID_COLUMN_PROMO_WON_LOCK].SortMode = DataGridViewColumnSortMode.NotSortable;

      dgv_common.Columns[GRID_COLUMN_PROMO_CREDIT_TYPE].Width = 0;
      dgv_common.Columns[GRID_COLUMN_PROMO_CREDIT_TYPE].Visible = false;
      dgv_common.Columns[GRID_COLUMN_PROMO_CREDIT_TYPE].SortMode = DataGridViewColumnSortMode.NotSortable;

      dgv_common.Columns[GRID_COLUMN_PROMO_MIN_RECHARGE].Width = 0;
      dgv_common.Columns[GRID_COLUMN_PROMO_MIN_RECHARGE].Visible = false;
      dgv_common.Columns[GRID_COLUMN_PROMO_MIN_RECHARGE].SortMode = DataGridViewColumnSortMode.NotSortable;

      dgv_common.Columns[GRID_COLUMN_PROMO_MIN_RECHARGE_TEXT].HeaderCell.Value = Resource.String("STR_FRM_AMOUNT_INPUT_005");
      dgv_common.Columns[GRID_COLUMN_PROMO_MIN_RECHARGE_TEXT].Width = 0;
      dgv_common.Columns[GRID_COLUMN_PROMO_MIN_RECHARGE_TEXT].Visible = false;
      dgv_common.Columns[GRID_COLUMN_PROMO_MIN_RECHARGE_TEXT].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
      dgv_common.Columns[GRID_COLUMN_PROMO_MIN_RECHARGE_TEXT].SortMode = DataGridViewColumnSortMode.NotSortable;

      dgv_common.Columns[GRID_COLUMN_PROMO_REWARD].HeaderCell.Value = Resource.String("STR_FRM_AMOUNT_INPUT_006");
      dgv_common.Columns[GRID_COLUMN_PROMO_REWARD].Width = 0;
      dgv_common.Columns[GRID_COLUMN_PROMO_REWARD].Visible = false;
      dgv_common.Columns[GRID_COLUMN_PROMO_REWARD].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
      dgv_common.Columns[GRID_COLUMN_PROMO_REWARD].SortMode = DataGridViewColumnSortMode.NotSortable;

      dgv_common.Columns[GRID_COLUMN_PROMO_SORT].Width = 0;
      dgv_common.Columns[GRID_COLUMN_PROMO_SORT].Visible = false;
      dgv_common.Columns[GRID_COLUMN_PROMO_SORT].SortMode = DataGridViewColumnSortMode.NotSortable;

      dgv_common.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
      dgv_common.MultiSelect = false;

    } // GuiStyleSheet

    private void FormatDataGridCancelablePromos()
    {
      dgv_common.Columns[1].HeaderText = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_027");
      dgv_common.Columns[2].HeaderText = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_028");
      dgv_common.Columns[6].HeaderText = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_029");
      dgv_common.Columns[5].HeaderText = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_030");
      dgv_common.Columns[0].HeaderText = "";
      dgv_common.Columns[5].DisplayIndex = 1;
      dgv_common.Columns[0].DisplayIndex = 0;

      dgv_common.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

      dgv_common.Columns[0].Visible = true;
      dgv_common.Columns[8].Visible = false;
      dgv_common.Columns[3].Visible = false;
      dgv_common.Columns[4].Visible = false;
      dgv_common.Columns[6].Visible = true;
      dgv_common.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
      //dgv_common.Columns[5].DefaultCellStyle.Format = "dd/MM HH:MM:ss";
      dgv_common.Columns[7].Visible = false;
      dgv_common.Columns[9].Visible = false;
      dgv_common.Columns[10].Visible = false;
      dgv_common.Columns[11].Visible = false;
      dgv_common.Columns[12].Visible = false;
      dgv_common.Columns[13].Visible = false;
      dgv_common.Columns[14].Visible = false;
      dgv_common.Columns[15].Visible = false;
      dgv_common.Columns[16].Visible = false;
      dgv_common.Columns[17].Visible = false;
      dgv_common.Columns[18].Visible = false;
      dgv_common.Columns[1].Visible = false;
      dgv_common.Columns[6].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
      dgv_common.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
      dgv_common.MultiSelect = false;

    }

    //------------------------------------------------------------------------------
    // PURPOSE : Handle click event in the common grid. This event is used to 
    //           select the promotion to apply to current card add credit operation.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //    
    private void dgv_common_CellClick(object sender, DataGridViewCellEventArgs e)
    {
      DataGridViewRow _view_row;
      DataRow _row;
      Boolean _validated = true;
      Currency _current_amount;

      // Ignore clicks on non-data row areas; headers, for example
      if (e.RowIndex >= 0)
      {
        switch (m_grid_type)
        {
          case GridType.Promotions:
            if (m_selected_promo_index != e.RowIndex || m_contracting)
            {
              _view_row = dgv_common.Rows[e.RowIndex];
              _row = ((DataRowView)_view_row.DataBoundItem).Row;

              m_selected_promo_index = e.RowIndex;
              m_selected_promo_id = (Int64)_row[GRID_COLUMN_PROMO_ID];
              m_selected_promo_name = (String)_row[GRID_COLUMN_PROMO_NAME];
              m_promo_warning_shown = false;
              m_available_limit = 0;

              // RCI & DDM 05-SEP-2012:
              //           Don't clear num_tokens when contracting the view.
              //           Only clear num_tokens when changing the selected promotion.
              if (!m_contracting)
              {
                m_promo_num_tokens = 0;
              }
              // Clear msg areas
              ResetPromoRewardArea();

              if (m_selected_promo_id > 0 && m_voucher_type != VoucherTypes.CancelPromotions)
              {
                // Checks that currently selected promotion (if any) can still be applied
                _current_amount = 0;
                if (is_voucher_loaded)
                {
                  _validated = ValidatePromotion(m_voucher_amount, m_promo_num_tokens, true, out m_promo_reward_amount, out m_promo_won_lock);
                }
                else
                {
                  // Do not check amounts, as none is present
                  _validated = ValidatePromotion(0, 0, false, out m_promo_reward_amount, out m_promo_won_lock);
                }
              }
              else
              {
                // No promotion selected: Reset promotion data.
                m_selected_promo_data = new Promotion.TYPE_PROMOTION_DATA();

                m_selected_promo_index = 0;
                m_selected_promo_id = 0;
                m_promo_reward_amount = 0;
                m_promo_won_lock = 0;
                m_promo_num_tokens = 0;
                m_spent_used = 0;
                m_promo_warning_shown = false;

                m_available_limit = 0;

                // ACC 18-JAN-2012: Calculate spent per provider
                if (m_dt_remaining_spent_per_provider != null)
                {
                  Promotion.CalculateDataByProvider(m_dt_remaining_spent_per_provider, null, out m_input_spent, out m_input_played);
                }

                UpdatePromoInputValues();
              }

              // Toggle input tokens area according to promotion specifics
              gb_tokens.Enabled = (m_selected_promo_data.num_tokens > 0 && m_selected_promo_id > 0);
            }

            if (m_promos_expanded_view)
            {
              return;
            }

            // Refresh voucher in screen if already in preview mode
            if (is_voucher_loaded)
            {
              btn_ok.Enabled = false;

              if (_validated)
              {
                // Generate voucher again
                PreviewVoucher(m_voucher_amount, true);
              }
              else
              {

                m_input_amount = null;
                m_promo_num_tokens = 0;

                UpdatePromoInputValues();

                // Unload not valid voucher
                web_browser.Navigate("");
                lbl_total_to_pay.Text = "";
                is_voucher_loaded = false;
              }

              // PreviewVoucher may void is_voucher_loaded if validations fail
              if (is_voucher_loaded)
              {
                btn_ok.Enabled = true;
                btn_ok.Focus();
              }
            }

            if (m_promos_expanded_view)
            {
              DataTable _dt;
              DataRow _dr;

              _dt = (DataTable)dgv_common.DataSource;
              if (e.RowIndex < _dt.Rows.Count)
              {
                _dr = _dt.Rows.Find(m_selected_promo_data.id);

                if (_dr != null)
                {
                  if (m_selected_promo_data.won_lock_enabled)
                  {
                    _dr["MIN_CASH_IN"] = 0;
                    _dr["CASH_IN_TEXT"] = M_STR_WONLOCK;
                  }
                  else
                  {
                    Currency _min_cash_in;

                    if (m_selected_promo_data.min_cash_in > 0)
                    {
                      _min_cash_in = m_selected_promo_data.min_cash_in;
                    }
                    else
                    {
                      _min_cash_in = m_selected_promo_data.cash_in;
                    }
                    _dr["MIN_CASH_IN"] = (Decimal)_min_cash_in;
                    _dr["CASH_IN_TEXT"] = (_min_cash_in == 0) ? "" : _min_cash_in.ToString();
                  }
                }
              }
            }
            break;
          case GridType.None:
            break;
        }//switch
      }//if

      this.txt_amount.Focus();
    } // dgv_common_CellClick

    //------------------------------------------------------------------------------
    // PURPOSE : Validate that the currently selected promotion can still be applied 
    //           to the active player account.
    //
    //  PARAMS :
    //      - INPUT :
    //          - InputAmount: amount to add to the account
    //          - ValidateAmount: flag to signal whether amount must be checked 
    //                            against promotion conditions or not
    //
    //      - OUTPUT :
    //          - Reward: amount of non-redeemable credits awarded when applying the 
    //                    selected promotion
    //
    // RETURNS :
    //
    //   NOTES :
    //    

    private Boolean ValidatePromotion(Currency InputAmount,
                                      Int32 NumTokens,
                                      Boolean ValidateAmount,
                                      out Currency Reward,
                                      out Currency WonLock)
    {
      SqlConnection _sql_conn;
      SqlTransaction _sql_trx;
      Promotion.PROMOTION_STATUS _promo_status;
      Currency _reward = 0;
      Currency _won_lock = 0;
      Currency _promo_account_limit = 0;
      Currency _cash_in;
      Int32 _num_tokens;
      String _msg = "";
      String _title = "";
      MessageBoxIcon _icon;
      Boolean _rc;
      DataTable _account_flags;
      DateTime _now;

      _rc = true;
      _sql_conn = null;
      _sql_trx = null;
      Reward = 0;
      WonLock = 0;

      try
      {
        _sql_conn = WGDB.Connection();
        _sql_trx = _sql_conn.BeginTransaction();

        _cash_in = InputAmount;
        _num_tokens = NumTokens;
        m_selected_promo_data = new Promotion.TYPE_PROMOTION_DATA();
        _rc = Promotion.ReadPromotionData(_sql_trx, m_selected_promo_id, m_selected_promo_data);
        if (!_rc)
        {
          // Log error
          Log.Error("ValidatePromotion. Promotion not found: " + m_selected_promo_id + ".");

          _promo_status = Promotion.PROMOTION_STATUS.PROMOTION_NOT_FOUND;
        }
        else
        {
          //if (ValidateAmount)
          //{
          //  // Check that a number of tokens has been provided if promotion requires it
          //  if (_num_tokens < 0 && m_selected_promo_data.num_tokens > 0)
          //  {
          //    frm_message.Show(Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_MSG_009"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, _icon = MessageBoxIcon.Error, this);
          //    UpdatePromoRewardArea(0, false);

          //    return false;
          //  }
          //}   


          if (!Promotion.GetActiveFlags(m_card_data.AccountId, _sql_trx, out  _account_flags))
          {

            return false;
          }

          // AMF 05-SEP-2013: Calculate spent per terminal
          if (m_dt_remaining_spent_per_terminal == null)
          {
            m_dt_remaining_spent_per_terminal = Promotion.AccountRemainingDataPerTerminal(m_card_data.AccountId, _sql_trx);
          }

          // SDS 22-10-2015: calculate played & spent per terminal in one function only
          Promotion.CaculateDataByTerminal(m_dt_remaining_spent_per_terminal,
                                             m_selected_promo_data.terminal_list,
                                             m_selected_promo_id,
                                             out m_input_spent,
                                             out m_input_played,
                                             _sql_trx);



          _now = WGDB.Now;
          _promo_status = Promotion.CheckPromotionApplicable(_sql_trx,
                                                             m_selected_promo_id,
                                                             m_card_data.AccountId,
                                                             ValidateAmount,
                                                             _account_flags,
                                                             ref _cash_in,
                                                             m_input_spent,
                                                             m_input_played,
                                                             ref _num_tokens,
                                                             ref m_selected_promo_data,
                                                             out _reward,
                                                             out _won_lock,
                                                             out m_spent_used,
                                                             out m_available_limit,
                                                             _now,
                                                             _now);
        }

        // LEM & JCA:
        //    _promo_status = Promotion.CheckPromotionApplicable  =>  Commented because use a new overloaded function
        //    Promotion.CheckAccountLimits(                       =>  Commented because m_available_limit is unique used variable and now calculate in Promotion.CheckPromotionApplicable
        //_promo_status = Promotion.CheckPromotionApplicable(_sql_trx,
        //                                                     m_selected_promo_id,
        //                                                     m_card_data.AccountId,
        //                                                     ValidateAmount,
        //                                                     _account_flags,
        //                                                     ref _cash_in,
        //                                                     m_input_spent,
        //                                                     ref _num_tokens,
        //                                                     ref m_selected_promo_data,
        //                                                     out _reward,
        //                                                     out _won_lock,
        //                                                     out m_spent_used,
        //                                                     WGDB.Now);
        //}
        //Promotion.CheckAccountLimits(m_card_data.AccountId, m_selected_promo_id, m_selected_promo_data.daily_limit, m_selected_promo_data.monthly_limit, _sql_trx, out m_available_limit);

        // Update promo msg area 
        if (m_available_limit > 0)
        {
          UpdatePromoRewardArea(_rc ? _reward : 0, true);
        }
        else
        {
          UpdatePromoRewardArea(_rc ? _reward : 0, false);
        }

        Reward = _rc ? _reward : 0;
        WonLock = _rc ? _won_lock : 0;

        UpdatePromoInputValues();

        // Parse return code and display appropriate messages
        _icon = MessageBoxIcon.None;
        switch (_promo_status)
        {
          case Promotion.PROMOTION_STATUS.PROMOTION_OK:
            if (ValidateAmount && _reward <= 0)
            {
              // Check that a promotion awards something
              switch (m_selected_promo_data.credit_type)
              {
                case ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE:
                  _msg = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_MSG_009_REDEEMABLE");
                  break;
                case ACCOUNT_PROMO_CREDIT_TYPE.POINT:
                  _msg = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_MSG_009_POINT");
                  break;
                case ACCOUNT_PROMO_CREDIT_TYPE.NR1:
                case ACCOUNT_PROMO_CREDIT_TYPE.NR2:
                case ACCOUNT_PROMO_CREDIT_TYPE.UNKNOWN:
                default:
                  _msg = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_MSG_009_NOT_REDEEMABLE");
                  break;
              }
              _title = Resource.String("STR_APP_GEN_MSG_ERROR");
              _icon = MessageBoxIcon.Error;
              _rc = false;
            }
            break;

          case Promotion.PROMOTION_STATUS.PROMOTION_REDUCED_REWARD:
            if (ValidateAmount && _reward <= 0)
            {
              // Check that a promotion awards something
              switch (m_selected_promo_data.credit_type)
              {
                case ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE:
                  _msg = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_MSG_009_REDEEMABLE");
                  break;
                case ACCOUNT_PROMO_CREDIT_TYPE.POINT:
                  _msg = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_MSG_009_POINT");
                  break;
                case ACCOUNT_PROMO_CREDIT_TYPE.NR1:
                case ACCOUNT_PROMO_CREDIT_TYPE.NR2:
                case ACCOUNT_PROMO_CREDIT_TYPE.UNKNOWN:
                default:
                  _msg = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_MSG_009_NOT_REDEEMABLE");
                  break;
              }
              _title = Resource.String("STR_APP_GEN_MSG_ERROR");
              _icon = MessageBoxIcon.Error;
              _rc = false;
            }
            else
            {
              // Avoid showing every warning more than once
              if (!m_promo_warning_shown)
              {
                switch (m_selected_promo_data.credit_type)
                {
                  case ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE:
                    _msg = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_MSG_010_REDEEMABLE", _reward);
                    break;
                  case ACCOUNT_PROMO_CREDIT_TYPE.POINT:
                    _msg = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_MSG_010_POINT", (Points)((Decimal)_reward));
                    break;
                  case ACCOUNT_PROMO_CREDIT_TYPE.NR1:
                  case ACCOUNT_PROMO_CREDIT_TYPE.NR2:
                  case ACCOUNT_PROMO_CREDIT_TYPE.UNKNOWN:
                  default:
                    _msg = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_MSG_010_NOT_REDEEMABLE", _reward);
                    break;
                }
                _title = Resource.String("STR_APP_GEN_MSG_WARNING");
                _icon = MessageBoxIcon.Warning;
                m_promo_warning_shown = true;
              }
            }
            break;

          //case Promotion.PROMOTION_STATUS.PROMOTION_OK_MODIFIED_AMOUNTS:
          //  _msg = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_MSG_010", _reward);
          //  _title = Resource.String("STR_APP_GEN_MSG_WARNING");
          //  _icon = MessageBoxIcon.Warning;
          //  break;

          case Promotion.PROMOTION_STATUS.PROMOTION_GENERIC_ERROR:
          case Promotion.PROMOTION_STATUS.PROMOTION_NOT_FOUND:
          case Promotion.PROMOTION_STATUS.PROMOTION_ACCT_NOT_FOUND:
            _msg = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_MSG_001");
            _title = Resource.String("STR_APP_GEN_MSG_ERROR");
            _icon = MessageBoxIcon.Error;
            _rc = false;
            break;

          case Promotion.PROMOTION_STATUS.PROMOTION_BELOW_MINIMUM:
            if (ValidateAmount && _reward <= 0)
            {
              // Check that a promotion awards something
              switch (m_selected_promo_data.credit_type)
              {
                case ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE:
                  _msg = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_MSG_009_REDEEMABLE");
                  break;
                case ACCOUNT_PROMO_CREDIT_TYPE.POINT:
                  _msg = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_MSG_009_POINT");
                  break;
                case ACCOUNT_PROMO_CREDIT_TYPE.NR1:
                case ACCOUNT_PROMO_CREDIT_TYPE.NR2:
                case ACCOUNT_PROMO_CREDIT_TYPE.UNKNOWN:
                default:
                  _msg = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_MSG_009_NOT_REDEEMABLE");
                  break;
              }
              _title = Resource.String("STR_APP_GEN_MSG_ERROR");
              _icon = MessageBoxIcon.Error;
              _rc = false;
            }
            else
            {
              // Avoid showing every warning more than once
              if (!m_promo_warning_shown)
              {
                _msg = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_MSG_002", _cash_in);
                _title = Resource.String("STR_APP_GEN_MSG_WARNING");
                _icon = MessageBoxIcon.Warning;
                m_promo_warning_shown = true;
              }
            }
            break;

          case Promotion.PROMOTION_STATUS.PROMOTION_DISABLED:
          case Promotion.PROMOTION_STATUS.PROMOTION_OUT_OF_DATE:
          case Promotion.PROMOTION_STATUS.PROMOTION_OUT_OF_WEEKDAY:
          case Promotion.PROMOTION_STATUS.PROMOTION_OUT_OF_TIME:
            _msg = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_MSG_003");
            _title = Resource.String("STR_APP_GEN_MSG_ERROR");
            _icon = MessageBoxIcon.Error;
            _rc = false;
            break;

          case Promotion.PROMOTION_STATUS.PROMOTION_ACCT_ANONYMOUS:
            _msg = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_MSG_004");
            _title = Resource.String("STR_APP_GEN_MSG_ERROR");
            _icon = MessageBoxIcon.Error;
            _rc = false;
            break;

          case Promotion.PROMOTION_STATUS.PROMOTION_ACCT_BLOCKED:
            _msg = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_MSG_005");
            _title = Resource.String("STR_APP_GEN_MSG_ERROR");
            _icon = MessageBoxIcon.Error;
            _rc = false;
            break;

          case Promotion.PROMOTION_STATUS.PROMOTION_ACCT_NO_REDEEMABLE:
            _msg = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_MSG_006");
            _title = Resource.String("STR_APP_GEN_MSG_ERROR");
            _icon = MessageBoxIcon.Error;
            _rc = false;
            break;

          case Promotion.PROMOTION_STATUS.PROMOTION_ACCT_HAS_PRIZE:
            _msg = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_MSG_011");
            _title = Resource.String("STR_APP_GEN_MSG_ERROR");
            _icon = MessageBoxIcon.Error;
            _rc = false;
            break;

          case Promotion.PROMOTION_STATUS.PROMOTION_GENDER_FILTER:
          case Promotion.PROMOTION_STATUS.PROMOTION_BIRTHDAY_FILTER:
          case Promotion.PROMOTION_STATUS.PROMOTION_LEVEL_FILTER:
          case Promotion.PROMOTION_STATUS.PROMOTION_FREQUENCY_FILTER:
            _msg = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_MSG_007");
            _title = Resource.String("STR_APP_GEN_MSG_ERROR");
            _icon = MessageBoxIcon.Error;
            _rc = false;
            break;

          case Promotion.PROMOTION_STATUS.PROMOTION_LIMIT_EXHAUSTED:
            _msg = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_MSG_008");
            _title = Resource.String("STR_APP_GEN_MSG_ERROR");
            _icon = MessageBoxIcon.Error;
            _rc = false;
            break;

          case Promotion.PROMOTION_STATUS.PROMOTION_BELOW_MINIMUM_SPENT:
            _msg = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_MSG_012");
            _title = Resource.String("STR_APP_GEN_MSG_ERROR");
            _icon = MessageBoxIcon.Error;
            _rc = false;
            break;

          case Promotion.PROMOTION_STATUS.PROMOTION_BELOW_SPENT:
            _msg = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_MSG_013");
            _title = Resource.String("STR_APP_GEN_MSG_ERROR");
            _icon = MessageBoxIcon.Error;
            _rc = false;
            break;
        }

        // Display warning/error message 
        if (_msg != "" && !m_promos_expanded_view)
        {
          frm_message.Show(_msg, _title, MessageBoxButtons.OK, _icon, this);
        }
      }

      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      finally
      {
        // Close connection
        if (_sql_conn != null)
        {
          _sql_conn.Close();
          _sql_conn = null;
        }
      }

      return _rc;
    } // ValidatePromotion

    //------------------------------------------------------------------------------
    // PURPOSE : Restore promotion informational area to its default status.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //    - Non-populated fields are hidden

    private void ResetPromoRewardArea()
    {
      lbl_promo_parameters.Visible = false;
      lbl_promo_reward_text.Visible = false;
      lbl_promo_reward_value.Visible = false;

      lbl_entered_tokens.Text = Resource.String("STR_FRM_AMOUNT_INPUT_001", "---");
    } // ResetPromoRewardArea

    //------------------------------------------------------------------------------
    // PURPOSE : Update promotion parameters area.
    //
    //  PARAMS :
    //      - INPUT :
    //          - AmountToShow: amount to display as reward
    //          - ShowLimit: flag to indicate whther the available limit for account/promotion
    //                       must be shown or not
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //    - Non-populated fields are hidden

    private void UpdatePromoRewardArea(Currency AmountToShow, Boolean ShowLimit)
    {
      String _aux_label;

      Object min_cash_in_reward;
      Object cash_in_reward;
      Object min_spent_reward;
      Object token_reward;
      Object spent_reward;
      Object available_limit;

      _aux_label = "";
      lbl_promo_parameters.Text = "";
      lbl_promo_parameters.Visible = false;

      switch (m_selected_promo_data.credit_type)
      {
        case ACCOUNT_PROMO_CREDIT_TYPE.POINT:
          String _points;

          _points = " " + Resource.String("STR_UC_CARD_POINTS_BALANCE_SUFFIX");

          min_cash_in_reward = ((Points)m_selected_promo_data.min_cash_in_reward.SqlMoney).ToString() + _points;
          cash_in_reward = ((Points)m_selected_promo_data.cash_in_reward.SqlMoney).ToString() + _points;
          min_spent_reward = ((Points)m_selected_promo_data.min_spent_reward.SqlMoney).ToString() + _points;
          token_reward = ((Points)m_selected_promo_data.token_reward.SqlMoney).ToString() + _points;
          spent_reward = ((Points)m_selected_promo_data.spent_reward.SqlMoney).ToString() + _points;
          available_limit = ((Points)m_available_limit.SqlMoney).ToString() + _points;
          break;
        case ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE:
        case ACCOUNT_PROMO_CREDIT_TYPE.NR1:
        case ACCOUNT_PROMO_CREDIT_TYPE.NR2:
        case ACCOUNT_PROMO_CREDIT_TYPE.UNKNOWN:
        default:
          min_cash_in_reward = m_selected_promo_data.min_cash_in_reward;
          cash_in_reward = m_selected_promo_data.cash_in_reward;
          min_spent_reward = m_selected_promo_data.min_spent_reward;
          token_reward = m_selected_promo_data.token_reward;
          spent_reward = m_selected_promo_data.spent_reward;
          available_limit = m_available_limit;
          break;
      }

      if (m_selected_promo_data.min_cash_in > 0
        || m_selected_promo_data.min_cash_in_reward > 0)
      {
        _aux_label += "    " +
                      Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_004", m_selected_promo_data.min_cash_in) + "\r\n";
      }
      if (m_selected_promo_data.min_cash_in_reward > 0)
      {
        _aux_label += "    " +
                      Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_005", min_cash_in_reward) + "\r\n";
      }
      if (m_selected_promo_data.cash_in_reward > 0)
      {
        _aux_label += "    " +
                      Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_006", cash_in_reward, m_selected_promo_data.cash_in) + "\r\n";
      }

      if (_aux_label.Length > 0)
      {
        lbl_promo_parameters.Text += Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_014") + ":\r\n" + _aux_label;
        _aux_label = "";
      }

      if (m_selected_promo_data.min_spent_reward > 0)
      {
        _aux_label += "    " +
                      Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_012", m_selected_promo_data.min_spent) + "\r\n";
        _aux_label += "    " +
                      Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_013", min_spent_reward) + "\r\n";
      }
      if (m_selected_promo_data.spent_reward > 0)
      {
        _aux_label += "    " +
                      Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_006", spent_reward, m_selected_promo_data.spent) + "\r\n";
      }

      if (_aux_label.Length > 0)
      {
        lbl_promo_parameters.Text += Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_015") + ":\r\n" + _aux_label;
        _aux_label = "";
      }

      if (m_selected_promo_data.token_reward > 0)
      {
        lbl_promo_parameters.Text += Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_016") + ": " +
                                     m_selected_promo_data.token_name + "\r\n";

        if (m_selected_promo_data.num_tokens > 1)
        {

          _aux_label = m_selected_promo_data.num_tokens + " ";
        }
        lbl_promo_parameters.Text += "    " +
                                     Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_011",
                                                     token_reward,
                                                     _aux_label,
                                                     m_selected_promo_data.token_name) + "\r\n";
      }

      if (ShowLimit)
      {
        lbl_promo_parameters.Text += "\r\n" + Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_008", available_limit) + "\r\n";
      }

      if (m_selected_promo_data.terminal_list.ListType != TerminalList.TerminalListType.All)
      {
        lbl_promo_parameters.Text += "\r\n" + Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_017") + "\r\n";
      }

      if (lbl_promo_parameters.Text.Length > 0)
      {
        lbl_promo_parameters.Visible = true;
        gb_promotion_info.Visible = true;
      }

      lbl_promo_reward_text.Visible = true;
      switch (m_selected_promo_data.credit_type)
      {
        case ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE:
          lbl_promo_reward_value.Text = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_002_REDEEMABLE", AmountToShow);
          break;
        case ACCOUNT_PROMO_CREDIT_TYPE.POINT:
          lbl_promo_reward_value.Text = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_002_POINTS", (Points)AmountToShow.SqlMoney);
          break;
        case ACCOUNT_PROMO_CREDIT_TYPE.NR1:
        case ACCOUNT_PROMO_CREDIT_TYPE.NR2:
        // Must show something in UNKNOWN and default values.
        case ACCOUNT_PROMO_CREDIT_TYPE.UNKNOWN:
        default:
          lbl_promo_reward_value.Text = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_002_NOT_REDEEMABLE", AmountToShow);
          break;
      }
      lbl_promo_reward_value.Visible = true;

      setImages();

    } // UpdatePromoRewardArea

    //------------------------------------------------------------------------------
    // PURPOSE : Update promotion input values area.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private void UpdatePromoInputValues()
    {
      String _num_tokens;
      String _num_tokens_with_units;

      lbl_promo_input_values.Text = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_014") + ": ";
      if (m_input_amount.HasValue)
      {
        lbl_promo_input_values.Text += m_input_amount + "\r\n";
      }
      else
      {
        lbl_promo_input_values.Text += "---\r\n";
      }

      lbl_promo_input_values.Text += Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_015") + ": " + m_input_spent + "     "; // +" /  " + Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_033") + " :" + m_input_played + "\r\n";

      if (m_selected_promo_data == null
        || m_selected_promo_data.token_name == null
        || m_selected_promo_data.token_name == "")
      {
        _num_tokens = "---";
        _num_tokens_with_units = "---";
      }
      else
      {
        if (m_promo_num_tokens > 0)
        {
          _num_tokens = m_promo_num_tokens.ToString();
          _num_tokens_with_units = _num_tokens + " " + m_selected_promo_data.token_name;
        }
        else
        {
          _num_tokens = "---";
          _num_tokens_with_units = "0 " + m_selected_promo_data.token_name;
        }
      }

      lbl_promo_input_values.Text += Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_016") + ": " + _num_tokens_with_units + "\r\n";
      lbl_entered_tokens.Text = Resource.String("STR_FRM_AMOUNT_INPUT_001", _num_tokens);

    } // UpdatePromoInputValues

    //------------------------------------------------------------------------------
    // PURPOSE : Handle click event on Enter Num Tokens button.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //    - Non-populated fields are hidden

    private void btn_enter_tokens_Click(object sender, EventArgs e)
    {
      frm_input_tokens _input_tokens = new frm_input_tokens();
      frm_yesno _shader = new frm_yesno();
      Int32 _num_tokens;
      Boolean _validated;

      _shader.Opacity = 0.6f;
      _shader.Show();

      // Form requesting to enter number of tokens 
      _input_tokens.Show(m_selected_promo_data.token_name, m_selected_promo_data.num_tokens, out _num_tokens, this.ParentForm);
      // If no input is provided then _num_tokens returns -1
      if (_num_tokens >= 0)
      {
        m_promo_num_tokens = _num_tokens;

        // Update all calculation areas
        if (is_voucher_loaded)
        {
          _validated = ValidatePromotion(m_voucher_amount, m_promo_num_tokens, true, out m_promo_reward_amount, out m_promo_won_lock);
          if (_validated)
          {
            // Generate voucher in preview mode
            PreviewVoucher(m_voucher_amount, true);

            if (is_voucher_loaded)
            {
              // is_voucher loaded signals a valid voucher is displayed
              btn_ok.Enabled = true;
              btn_ok.Focus();
            }
          }
          else
          {
            m_input_amount = 0;
            // RCI 19-NOV-2010: Don't reset m_promo_num_tokens.
            //m_promo_num_tokens = 0;

            web_browser.Navigate("");
            is_voucher_loaded = false;
            lbl_total_to_pay.Text = "";
          }
        }
        else
        {
          // Do not check amounts, as none is present
          _validated = ValidatePromotion(0, 0, false, out m_promo_reward_amount, out m_promo_won_lock);
        }
      }

      _shader.Hide();
      _shader.Dispose();

      _input_tokens.Hide();
      _input_tokens.Dispose();
    } // btn_enter_tokens_Click

    //------------------------------------------------------------------------------
    // PURPOSE : Set image of them with different color.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //    
    private void setImages()
    {
      DataGridViewImageCell _cell;

      switch (m_grid_type)
      {
        case GridType.Promotions:

          foreach (DataGridViewRow item in dgv_common.Rows)
          {
            _cell = (DataGridViewImageCell)item.Cells[GRID_COLUMN_PROMO_CREDIT_TYPE_IMAGE];

            switch ((WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE)item.Cells[GRID_COLUMN_PROMO_CREDIT_TYPE].Value)
            {

              case ACCOUNT_PROMO_CREDIT_TYPE.NR1:
              case ACCOUNT_PROMO_CREDIT_TYPE.NR2:
                if (item.Selected)
                {
                  _cell.Value = CashierStyle.Image.ImageToByteArray(Resources.ResourceImages.promoNRSelected);
                }
                else
                {
                  _cell.Value = CashierStyle.Image.ImageToByteArray(Resources.ResourceImages.promoNR);
                }

                if (m_promos_expanded_view)
                {
                  if (item.Cells[GRID_COLUMN_PROMO_MIN_RECHARGE_TEXT].Value != DBNull.Value &&
                      (String)item.Cells[GRID_COLUMN_PROMO_MIN_RECHARGE_TEXT].Value == M_STR_WONLOCK)
                  {
                    item.Cells[GRID_COLUMN_PROMO_MIN_RECHARGE_TEXT].Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                  }
                }

                break;
              case ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE:
                if (item.Selected)
                {
                  _cell.Value = CashierStyle.Image.ImageToByteArray(Resources.ResourceImages.promoReedemableSelected);
                }
                else
                {
                  _cell.Value = CashierStyle.Image.ImageToByteArray(Resources.ResourceImages.promoReedemable);
                }

                break;
              case ACCOUNT_PROMO_CREDIT_TYPE.POINT:
                if (item.Selected)
                {
                  _cell.Value = CashierStyle.Image.ImageToByteArray(Resources.ResourceImages.promoPointsSelected);
                }
                else
                {
                  _cell.Value = CashierStyle.Image.ImageToByteArray(Resources.ResourceImages.promoPoints);
                }

                break;
              default:
                _cell.Value = CashierStyle.Image.ImageToByteArray((Image)new Bitmap(1, 1));

                break;
            }

            if (m_voucher_type == VoucherTypes.CancelPromotions)
            {
              if (item.Cells[GRID_COLUMN_PROMO_WON_LOCK].Value != DBNull.Value && (Decimal)item.Cells[GRID_COLUMN_PROMO_WON_LOCK].Value > 0)
              {
                if (item.Selected)
                {
                  _cell.Value = CashierStyle.Image.ImageToByteArray(Resources.ResourceImages.promoLockSelected);
                }
                else
                {
                  _cell.Value = CashierStyle.Image.ImageToByteArray(Resources.ResourceImages.promoLock);
                }
              }
            }
            else
            {
              if ((Int32)item.Cells[GRID_COLUMN_PROMO_WON_LOCK].Value > 0)
              {
                if (item.Selected)
                {
                  _cell.Value = CashierStyle.Image.ImageToByteArray(Resources.ResourceImages.promoLockSelected);
                }
                else
                {
                  _cell.Value = CashierStyle.Image.ImageToByteArray(Resources.ResourceImages.promoLock);
                }
              }
            }

          }
          break;
        case GridType.None:

          break;
      }

    }// setImages

    // PURPOSE: View all promos with additional info and It accept the amount input.
    //
    //  PARAMS:
    //     - INPUT:
    //           - sender:
    //           - e:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - none

    private void btn_promos_change_view_Click(object sender, EventArgs e)
    {
      Boolean _promo_validated;

      _promo_validated = false;
      btn_promos_change_view.Enabled = false;
      if (JunketsBusinessLogic.IsJunketsEnabled())
      {
        btn_enter_flyer.Visible = m_promos_expanded_view;
      }

      try
      {
        if (m_promos_expanded_view)
        {
          ShowPromosContractedView();

          if (!m_input_amount.HasValue)
          {
            return;
          }

          if (m_selected_promo_id > 0)
          {
            // Checks that currently selected promotion can still be applied            
            _promo_validated = ValidatePromotion(m_input_amount.Value, m_promo_num_tokens, true, out m_promo_reward_amount, out m_promo_won_lock);

            if (_promo_validated)
            {
              // Generate voucher in preview mode
              PreviewVoucher(m_input_amount.Value, true);

              if (is_voucher_loaded)
              {
                // is_voucher loaded signals a valid voucher is displayed
                btn_ok.Enabled = true;
                btn_ok.Focus();
              }
            }

            if (!_promo_validated || !is_voucher_loaded)
            {
              btn_ok.Enabled = false;
              m_input_amount = null;
              web_browser.Navigate("");
              is_voucher_loaded = false;
              lbl_total_to_pay.Text = "";

              UpdatePromoInputValues();
            }
          }
        }
        else
        {
          ShowPromosExpandedView();
        }
      }
      finally
      {
        btn_promos_change_view.Enabled = true;
      }
    }

    // PURPOSE: Show all promos mode
    //
    //  PARAMS:
    //     - INPUT:
    //           - none
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - none

    private void ShowPromosExpandedView()
    {
      Int16 _row_to_show;

      _row_to_show = 11;

      CalculateRewardGrid();

      this.SuspendLayout();

      _prev_loc.Clear();
      _prev_loc.Add(btn_promos_change_view.Location);
      _prev_loc.Add(gb_promotion_info.Location);

      _prev_size.Clear();
      _prev_size.Add(gb_common.Size);
      _prev_size.Add(dgv_common.Size);

      gp_digits_box.Visible = false;
      uc_payment_method1.Visible = false;
      gb_tokens.Visible = false;
      web_browser.Visible = false;
      gb_total.Visible = false;

      gb_common.Width = 1010;
      gb_common.Height = 640;

      gb_common.PanelBackColor = CashierStyle.Colors.m_colors_dic[CashierStyle.Colors.ENUM_COLORS.HEX_E3E7E9];

      btn_promos_change_view.Left = gb_common.Width + gb_common.Left - btn_promos_change_view.Width - 15;
      btn_promos_change_view.Top = gb_common.Height + gb_common.Top - btn_promos_change_view.Height - 10;

      dgv_common.Width = 710;
      dgv_common.Height = (dgv_common.RowTemplate.Height * _row_to_show) + dgv_common.ColumnHeadersHeight;

      gb_promotion_info.Location = new Point(dgv_common.Width + 25, gb_common.Location.Y);

      promo_filter.Location = new Point(dgv_common.Location.X, dgv_common.Height);

      lbl_currency_to_pay.Visible = false;

      Int32 _dgv_real_width;
      if (_row_to_show < dgv_common.Rows.Count)
      {
        _dgv_real_width = dgv_common.Width + 15; // + SCROLL
      }
      else
      {
        _dgv_real_width = dgv_common.Width;
      }
      GuiStyleSheet();
      dgv_common.Columns[GRID_COLUMN_PROMO_NAME].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
      dgv_common.Columns[GRID_COLUMN_PROMO_NAME].Width = (Int32)(0.53 * _dgv_real_width);
      dgv_common.Columns[GRID_COLUMN_PROMO_MIN_RECHARGE_TEXT].Width = (Int32)(0.2 * _dgv_real_width);
      dgv_common.Columns[GRID_COLUMN_PROMO_MIN_RECHARGE_TEXT].Visible = true;
      dgv_common.Columns[GRID_COLUMN_PROMO_REWARD].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
      dgv_common.Columns[GRID_COLUMN_PROMO_REWARD].Visible = true;

      btn_promos_change_view.Text = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_020");
      m_promos_expanded_view = true;

      setImages();

      this.ResumeLayout();
    }

    // PURPOSE: 
    //
    //  PARAMS:
    //     - INPUT:
    //           - none
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - none

    private void ShowPromosContractedView()
    {
      this.SuspendLayout();

      gp_digits_box.Visible = true;
      gb_tokens.Visible = true;
      web_browser.Visible = true;
      gb_promotion_info.Visible = true;
      gb_total.Visible = true;
      //btn_enter_flyer.Visible = true;

      //pnl_buttons.Location = new Point(gb_total.Location.X, gb_total.Location.Y + gb_total.Height + _margin_between_controls);

      uc_payment_method1.Visible = false;
      //lbl_currency_to_pay.Visible = true;

      gb_common.PanelBackColor = CashierStyle.Colors.m_colors_dic[CashierStyle.Colors.ENUM_COLORS.HEX_F4F6F9];

      if (m_currencies != null && m_currencies.Count > 1)
      {
        if (m_voucher_type == VoucherTypes.CardAdd && (uc_payment_method1.GetCurrenciesCount() > 1 || !uc_payment_method1.CurrenciesList.Contains(m_national_currency)))
        {
          uc_payment_method1.Visible = true;
        }
      }

      try
      {
        if (dgv_common.Columns.Count > 0)
        {
          GuiStyleSheet();
          dgv_common.Columns[GRID_COLUMN_PROMO_NAME].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
          dgv_common.Columns[GRID_COLUMN_PROMO_MIN_RECHARGE_TEXT].Width = 0;
          dgv_common.Columns[GRID_COLUMN_PROMO_MIN_RECHARGE_TEXT].Visible = false;
          dgv_common.Columns[GRID_COLUMN_PROMO_REWARD].Width = 0;
          dgv_common.Columns[GRID_COLUMN_PROMO_REWARD].Visible = false;
          setImages();
        }

        if (_prev_size.Count > 0)
        {
          gb_common.Size = _prev_size[0];
          dgv_common.Size = _prev_size[1];
        }
        if (_prev_loc.Count > 0)
        {
          btn_promos_change_view.Location = _prev_loc[0];
          gb_promotion_info.Location = _prev_loc[1];

          //lbl_promo_parameters.Location = _prev_loc[1];
          //lbl_promo_input_values.Location = _prev_loc[2];
          //lbl_promo_reward_text.Location = _prev_loc[3];
          //lbl_promo_reward_value.Location = _prev_loc[4];
        }
      }
      catch
      { }

      SelectCurrentPromoGrid();

      btn_promos_change_view.Text = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_019");
      m_promos_expanded_view = false;

      promo_filter.Location = new Point(dgv_common.Location.X, dgv_common.Height);

      this.ResumeLayout();
    }

    private void CalculateRewardGrid()
    {
      DataTable _dt;
      _dt = (DataTable)dgv_common.DataSource;

      ComputeReward(_dt);
    }

    // PURPOSE: Calculate the current reward for each promotion.
    //
    //  PARAMS:
    //     - INPUT:
    //           - Dt:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - none

    private void ComputeReward(DataTable Dt)
    {
      String _str_reward;
      Int64 _current_promo_id;
      Promotion.TYPE_PROMOTION_DATA _promo_data;
      Currency _input_spent;
      Currency _input_played;
      Boolean _rc;
      Promotion.PROMOTION_STATUS _promo_status;
      ACCOUNT_PROMO_CREDIT_TYPE _credit_type;
      Currency _reward = 0;
      Currency _won_lock = 0;
      Currency _spent_used;
      SqlTransaction _sql_trx;
      String _str_credit_type;
      String _str_credit;
      Currency _cash_in;
      Int32 _num_tokens;
      DataTable _account_flags;

      try
      {
        if (m_previously_calculated)
        {
          _cash_in = m_input_amount.HasValue ? m_input_amount.Value : 0;
          if (_cash_in == m_cash_in_calculated)
          {
            return;
          }
        }

        using (DB_TRX _db_trx = new DB_TRX())
        {
          _sql_trx = _db_trx.SqlTransaction;

          if (!Promotion.GetActiveFlags(m_card_data.AccountId, _sql_trx, out _account_flags))
          {
            return;
          }

          foreach (DataRow _dr in Dt.Rows)
          {
            _cash_in = m_input_amount.HasValue ? m_input_amount.Value : 0;
            // RCI 05-SEP-2012: Don't use Tokens to fill the grid values.
            //_num_tokens = m_promo_num_tokens;
            _num_tokens = 0;

            _current_promo_id = (Int64)_dr["PM_PROMOTION_ID"];
            _credit_type = (ACCOUNT_PROMO_CREDIT_TYPE)_dr["PM_CREDIT_TYPE"];
            _str_reward = "";

            _promo_data = new Promotion.TYPE_PROMOTION_DATA();
            _rc = Promotion.ReadPromotionData(_sql_trx, _current_promo_id, _promo_data);
            if (_rc)
            {
              // AMF 05-SEP-2013: Calculate spent per terminal
              if (m_dt_remaining_spent_per_terminal == null)
              {
                m_dt_remaining_spent_per_terminal = Promotion.AccountRemainingDataPerTerminal(m_card_data.AccountId, _sql_trx);
              }

              // SDS 22-10-2015: calculate played & spent per terminal in one function only
              Promotion.CaculateDataByTerminal(m_dt_remaining_spent_per_terminal,
                                                 _promo_data.terminal_list,
                                                 _current_promo_id,
                                                 out _input_spent,
                                                 out _input_played,
                                                 _sql_trx);


              _promo_status = Promotion.CheckPromotionApplicable(_sql_trx,
                                                                 _current_promo_id,
                                                                 m_card_data.AccountId,
                                                                 true,
                                                                 _account_flags,
                                                                 ref _cash_in,
                                                                 _input_spent,
                                                                 _input_played,
                                                                 ref _num_tokens,
                                                                 ref _promo_data,
                                                                 out _reward,
                                                                  out _won_lock,
                                                                 out _spent_used,
                                                                 WGDB.Now);

              if (_reward > 0)
              {
                //_won_lock;

                switch (_credit_type)
                {
                  case ACCOUNT_PROMO_CREDIT_TYPE.NR1:
                  case ACCOUNT_PROMO_CREDIT_TYPE.NR2:
                    _str_credit_type = "NR";
                    _str_credit = ((Currency)_reward).ToString();
                    break;
                  case ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE:
                    _str_credit_type = "RE";
                    _str_credit = ((Currency)_reward).ToString();
                    break;
                  case ACCOUNT_PROMO_CREDIT_TYPE.POINT:
                    _str_credit_type = "PT";
                    _str_credit = ((Points)_reward.SqlMoney).ToString();
                    break;
                  case ACCOUNT_PROMO_CREDIT_TYPE.UNKNOWN:
                  default:
                    continue;
                }

                _str_reward = String.Format("{0} {1}", _str_credit, _str_credit_type);
              }

            }

            _dr["REWARD"] = _str_reward;

          }

        }

      }
      catch
      { }
      finally
      {
        m_previously_calculated = true;
        m_cash_in_calculated = m_input_amount.HasValue ? m_input_amount.Value : 0;
      }
    }


    private void SelectCurrentPromoGrid()
    {
      DataGridViewRow _dgv_to_select;

      _dgv_to_select = null;

      foreach (DataGridViewRow _dgv_row in dgv_common.Rows)
      {
        if (_dgv_row.Cells[GRID_COLUMN_PROMO_ID].Value.Equals(m_selected_promo_id))
        {
          _dgv_to_select = _dgv_row;
          break;
        }
      }

      if (_dgv_to_select == null && dgv_common.Rows.Count > 0)
      {
        _dgv_to_select = dgv_common.Rows[0];
      }

      if (_dgv_to_select != null)
      {
        _dgv_to_select.Selected = true;
        if (!_dgv_to_select.Displayed)
        {
          dgv_common.FirstDisplayedScrollingRowIndex = _dgv_to_select.Index;
        }

        try
        {
          m_contracting = true;
          dgv_common_CellClick(dgv_common, new DataGridViewCellEventArgs(0, _dgv_to_select.Index));
        }
        catch
        { }
        finally
        {
          m_contracting = false;
        }
      }
      if (m_voucher_type == VoucherTypes.CancelPromotions)
      {
        btn_cancel_by_cashier.Enabled = (this.dgv_common.Rows.Count > 0);

      }
    }

    private void FilterCreditChecked(CheckBox CheckBoxFilter)
    {

      CheckBoxFilter.Font = m_font_filter_credit_checked;
      CheckBoxFilter.TextAlign = ContentAlignment.MiddleCenter;

    }

    private void FilterCreditUnChecked(CheckBox CheckBoxFilter)
    {

      CheckBoxFilter.Font = m_font_filter_credit_unchecked;
      CheckBoxFilter.TextAlign = ContentAlignment.BottomRight;
    }


    // RXM 21 SEP 2012

    // PURPOSE: Cancel selected promotion and print voucher
    //
    //  PARAMS:
    //     - INPUT:
    //           - none
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - none
    private void btn_cancel_by_cashier_Click(object sender, EventArgs e)
    {
      Promotion.PROMOTION_TYPE _promo_type;
      DataTable _account_promotions;
      DataRow _promotion_dr;
      ArrayList _voucher_promo_list;
      String _message;
      List<ProfilePermissions.CashierFormFuncionality> _permissions_list;
      Boolean _ok;
      DateTime _activation;
      Int32 _cancelable_interval; // minutes

      if (m_voucher_type != VoucherTypes.CancelPromotions)
      {
        return;
      }

      if (this.dgv_common.SelectedRows.Count != 1)
      {
        return;
      }

      _permissions_list = new List<ProfilePermissions.CashierFormFuncionality>();
      _permissions_list.Add(ProfilePermissions.CashierFormFuncionality.PromotionCancel);

      _account_promotions = AccountPromotion.CreateAccountPromotionTable();

      _promotion_dr = _account_promotions.NewRow();
      _promotion_dr[AccountPromotion.SQL_COLUMN_ACP_UNIQUE_ID] = dgv_common.SelectedRows[0].Cells[1].Value;
      _promotion_dr[AccountPromotion.SQL_COLUMN_ACP_ACCOUNT_ID] = m_card_data.AccountId;
      _promotion_dr[AccountPromotion.SQL_COLUMN_ACP_STATUS] = dgv_common.SelectedRows[0].Cells[10].Value;
      _promotion_dr[AccountPromotion.SQL_COLUMN_ACP_CREDIT_TYPE] = dgv_common.SelectedRows[0].Cells[4].Value;
      _promotion_dr[AccountPromotion.SQL_COLUMN_ACP_PROMO_NAME] = dgv_common.SelectedRows[0].Cells[2].Value;
      _promotion_dr[AccountPromotion.SQL_COLUMN_ACP_PROMO_ID] = dgv_common.SelectedRows[0].Cells[11].Value;
      _promotion_dr[AccountPromotion.SQL_COLUMN_ACP_PROMO_TYPE] = dgv_common.SelectedRows[0].Cells[12].Value;
      _promotion_dr[AccountPromotion.SQL_COLUMN_ACP_PRIZE_TYPE] = dgv_common.SelectedRows[0].Cells[14].Value;
      _promotion_dr[AccountPromotion.SQL_COLUMN_ACP_PRIZE_GROSS] = dgv_common.SelectedRows[0].Cells[15].Value;
      _promotion_dr[AccountPromotion.SQL_COLUMN_ACP_PRIZE_TAX1] = dgv_common.SelectedRows[0].Cells[16].Value;
      _promotion_dr[AccountPromotion.SQL_COLUMN_ACP_PRIZE_TAX2] = dgv_common.SelectedRows[0].Cells[17].Value;
      _promotion_dr[AccountPromotion.SQL_COLUMN_ACP_PRIZE_TAX3] = dgv_common.SelectedRows[0].Cells[18].Value;

      _account_promotions.Rows.Add(_promotion_dr);

      _promo_type = (Promotion.PROMOTION_TYPE)_promotion_dr[AccountPromotion.SQL_COLUMN_ACP_PROMO_TYPE];

      if (_promo_type == Promotion.PROMOTION_TYPE.PER_POINTS_REDEEMABLE ||
          _promo_type == Promotion.PROMOTION_TYPE.PER_POINTS_NOT_REDEEMABLE)
      {
        // To cancel Promotions PerPoints (Gifts), it is needed also the GiftCancel permission.
        _permissions_list.Add(ProfilePermissions.CashierFormFuncionality.GiftCancel);
      }

      // Check if current user is an authorized user
      if (!ProfilePermissions.CheckPermissionList(_permissions_list,
                                                  ProfilePermissions.TypeOperation.RequestPasswd,
                                                  this,
                                                  0,
                                                  out _message))
      {
        return;
      }

      _message = Resource.String("STR_PROMOTION_CLEAR_NOTYPE", dgv_common.SelectedRows[0].Cells[6].Value, "1");
      _message = _message.Replace("\\r\\n", "\r\n");
      if (frm_message.Show(_message, Resource.String("STR_PROMOTION"),
          MessageBoxButtons.YesNo, Images.CashierImage.Question, Cashier.MainForm) != DialogResult.OK)
      {
        return;
      }

      _cancelable_interval = GeneralParam.GetInt32("ManualPromotions", "TimeToCancel", 0);
      _activation = (DateTime)dgv_common.SelectedRows[0].Cells[13].Value;

      // Check if has not exceeded the available time to cancel the promotion
      if (WGDB.Now > _activation.AddMinutes(_cancelable_interval))
      {
        _message = Resource.String("STR_PROMOTION_ERROR_MSG_CANT_CANCEL");
        frm_message.Show(_message, Resource.String("STR_PROMOTION"),
                           MessageBoxButtons.OK, Images.CashierImage.Error, Cashier.MainForm);

        PopulateAccountPromotions();

        return;
      }

      using (DB_TRX _db_trx = new DB_TRX())
      {
        _ok = AccountPromotion.Trx_CancelPromotion(_account_promotions, m_card_data, CashierSessionInfo,
                                                   out _voucher_promo_list, out _message, _db_trx.SqlTransaction);
        if (_ok)
        {
          _db_trx.Commit();
        }
      }

      if (_ok)
      {
        VoucherPrint.Print(_voucher_promo_list);
        frm_message.Show(Resource.String("STR_PROMOTION_CANCELED"), Resource.String("STR_PROMOTION"),
                             MessageBoxButtons.OK, Images.CashierImage.Information, Cashier.MainForm);

        PopulateAccountPromotions();
        SetFilter();
      }
      else
      {
        frm_message.Show(_message, Resource.String("STR_PROMOTION"),
                                   MessageBoxButtons.OK, Images.CashierImage.Error, Cashier.MainForm);
      }
    }

    private void SetFilter()
    {
      DataTable _dt;
      _dt = (DataTable)dgv_common.DataSource;
      _dt.DefaultView.RowFilter = promo_filter.m_filter;

    }

    // DLL 20-JUN-2013
    // PURPOSE: Hide all controls of Currency Exchange
    //  PARAMS:
    //     - INPUT:
    //           - TotalHide
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - none
    private void HideAllCurrencyExchange()
    {
      HideNationalCurrency();

      lbl_currency.Visible = false;
      if (m_cage_enabled)
      {
        uc_payment_method1.Visible = false;
        gp_digits_box.Location = new Point(gp_digits_box.Location.X, 11);

      }
      else
      {
        HidePaymentMethod();
      }
      if (m_voucher_type != VoucherTypes.WinLoss)
      {
        uc_payment_method1.CurrencyExchange = null;
      }
    }
    // PURPOSE: Hide controls for national currency
    //  PARAMS:
    //     - INPUT:
    //           - none
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - none
    private void HideNationalCurrency()
    {
      txt_amount.TextChanged -= new System.EventHandler(this.txt_amount_TextChanged);

      lbl_currency.Text = "";
      lbl_currency_to_pay.Text = "";
      m_with_currency_exchange = false;
    }

    // PURPOSE: Show controls of Currency Exchange
    //  PARAMS:
    //     - INPUT:
    //           - none
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - none
    private void ShowCurrencyExchange()
    {
      Boolean _is_cage_enabled;
      _is_cage_enabled = Cage.IsCageEnabled();

      m_with_currency_exchange = true;
      lbl_currency.Text = "";
      lbl_currency_to_pay.Text = "";
      lbl_cage_currency_total.Text = "";
      lbl_currency.Visible = !_is_cage_enabled;
      lbl_cage_currency_total.Visible = (m_voucher_type != VoucherTypes.CardAdd && _is_cage_enabled);

      ShowPaymentMethod();
    }

    // PURPOSE: Show payment method group box
    //  PARAMS:
    //     - INPUT:
    //           - none
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - none
    private void ShowPaymentMethod()
    {
      uc_payment_method1.Visible = true;
      gp_digits_box.Location = new System.Drawing.Point(302, 137);
      gp_digits_box.Size = new System.Drawing.Size(380, 378);

      //lbl_currency_to_pay.Visible = true;

      gb_tokens.Location = new Point(gp_digits_box.Location.X, gp_digits_box.Location.Y + gp_digits_box.Height + 13);

      // Move number buttons
      //btn_money_4.Location = new System.Drawing.Point(262, 87);
      //btn_money_4.Size = new System.Drawing.Size(72, 56);
      //btn_money_3.Location = new System.Drawing.Point(184, 87);
      //btn_money_3.Size = new System.Drawing.Size(72, 56);
      //btn_money_2.Location = new System.Drawing.Point(105, 87);
      //btn_money_2.Size = new System.Drawing.Size(72, 56);
      //btn_money_1.Location = new System.Drawing.Point(27, 87);
      //btn_money_1.Size = new System.Drawing.Size(72, 56);
      //btn_back.Location = new System.Drawing.Point(214, 149);
      //btn_back.Size = new System.Drawing.Size(120, 56);
      //btn_num_1.Location = new System.Drawing.Point(27, 149);
      //btn_num_1.Size = new System.Drawing.Size(56, 56);
      //btn_num_2.Location = new System.Drawing.Point(89, 149);
      //btn_num_2.Size = new System.Drawing.Size(56, 56);
      //btn_num_3.Location = new System.Drawing.Point(152, 149);
      //btn_num_3.Size = new System.Drawing.Size(56, 56);
      //btn_num_4.Location = new System.Drawing.Point(27, 211);
      //btn_num_4.Size = new System.Drawing.Size(56, 56);
      //btn_num_5.Location = new System.Drawing.Point(90, 211);
      //btn_num_5.Size = new System.Drawing.Size(56, 56);
      //btn_num_6.Location = new System.Drawing.Point(152, 211);
      //btn_num_6.Size = new System.Drawing.Size(56, 56);
      //btn_num_7.Location = new System.Drawing.Point(27, 274);
      //btn_num_7.Size = new System.Drawing.Size(56, 56);
      //btn_num_8.Location = new System.Drawing.Point(90, 274);
      //btn_num_8.Size = new System.Drawing.Size(56, 56);
      //btn_num_9.Location = new System.Drawing.Point(152, 274);
      //btn_num_9.Size = new System.Drawing.Size(56, 56);
      //btn_num_10.Location = new System.Drawing.Point(27, 336);
      //btn_num_10.Size = new System.Drawing.Size(120, 56);
      //btn_num_dot.Location = new System.Drawing.Point(152, 336);
      //btn_num_dot.Size = new System.Drawing.Size(56, 56);
      //btn_intro.Location = new System.Drawing.Point(214, 274);
      //btn_intro.Size = new System.Drawing.Size(120, 118);
    }

    // PURPOSE: Hide payment method group box
    //  PARAMS:
    //     - INPUT:
    //           - none
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - none
    private void HidePaymentMethod()
    {
      uc_payment_method1.Visible = false;
      gp_digits_box.Location = new System.Drawing.Point(302, 11);
      gp_digits_box.Size = new System.Drawing.Size(380, 378);

      //lbl_currency_to_pay.Visible = false;

      gb_tokens.Location = new Point(gp_digits_box.Location.X, gp_digits_box.Location.Y + gp_digits_box.Height + 13);
      //btn_money_4.Location = new System.Drawing.Point(264, 101);
      //btn_money_4.Size = new System.Drawing.Size(80, 64);
      //btn_money_3.Location = new System.Drawing.Point(178, 101);
      //btn_money_3.Size = new System.Drawing.Size(80, 64);
      //btn_money_2.Location = new System.Drawing.Point(92, 101);
      //btn_money_2.Size = new System.Drawing.Size(80, 64);
      //btn_money_1.Location = new System.Drawing.Point(6, 101);
      //btn_money_1.Size = new System.Drawing.Size(80, 64);
      //btn_back.Location = new System.Drawing.Point(216, 171);
      //btn_back.Size = new System.Drawing.Size(128, 64);
      //btn_num_1.Location = new System.Drawing.Point(6, 171);
      //btn_num_1.Size = new System.Drawing.Size(64, 64);
      //btn_num_2.Location = new System.Drawing.Point(76, 171);
      //btn_num_2.Size = new System.Drawing.Size(64, 64);
      //btn_num_3.Location = new System.Drawing.Point(146, 171);
      //btn_num_3.Size = new System.Drawing.Size(64, 64);
      //btn_num_4.Location = new System.Drawing.Point(6, 241);
      //btn_num_4.Size = new System.Drawing.Size(64, 64);
      //btn_num_5.Location = new System.Drawing.Point(76, 241);
      //btn_num_5.Size = new System.Drawing.Size(64, 64);
      //btn_num_6.Location = new System.Drawing.Point(146, 241);
      //btn_num_6.Size = new System.Drawing.Size(64, 64);
      //btn_num_7.Location = new System.Drawing.Point(6, 311);
      //btn_num_7.Size = new System.Drawing.Size(64, 64);
      //btn_num_8.Location = new System.Drawing.Point(76, 311);
      //btn_num_8.Size = new System.Drawing.Size(64, 64);
      //btn_num_9.Location = new System.Drawing.Point(146, 311);
      //btn_num_9.Size = new System.Drawing.Size(64, 64);
      //btn_num_10.Location = new System.Drawing.Point(6, 381);
      //btn_num_10.Size = new System.Drawing.Size(134, 64);
      //btn_num_dot.Location = new System.Drawing.Point(147, 381);
      //btn_num_dot.Size = new System.Drawing.Size(64, 64);
      //btn_intro.Location = new System.Drawing.Point(216, 311);
      //btn_intro.Size = new System.Drawing.Size(128, 134);
    }

    private void txt_amount_TextChanged(object sender, EventArgs e)
    {
      CurrencyExchangeResult _exchange_result;
      Decimal _input_amount;
      String _amount;

      if (Cage.IsCageEnabled() && (m_voucher_type == VoucherTypes.CloseCash
                                    || m_voucher_type == VoucherTypes.FillIn
                                    || m_voucher_type == VoucherTypes.FillOut)
                               && dgv_common.CurrentRow != null)
      {
        _amount = dgv_common.CurrentRow.Cells[GRID_COLUMN_CAGE_AMOUNTS_TOTAL].Value.ToString();
      }
      else
      {
        _amount = txt_amount.Text;
      }

      Decimal.TryParse(_amount, out _input_amount);

      if (_input_amount > 0 && uc_payment_method1.CurrencyExchange.Type == CurrencyExchangeType.CURRENCY)
      {
        uc_payment_method1.CurrencyExchange.ApplyExchange(_input_amount, out _exchange_result);
        lbl_currency.Text = Resource.String("STR_FRM_AMOUNT_INPUT_EQUALS") + " " + Currency.Format(_exchange_result.GrossAmount, m_national_currency.CurrencyCode);
      }
      else
      {
        lbl_currency.Text = "";
      }
    }



    private void uc_payment_method1_CurrencyChanged(CurrencyExchange CurrencyExchange)
    {
      bool _isCard;

      _isCard = false;
      // Select currency only when card is paid
      if ((m_voucher_type == VoucherTypes.CardAdd
        || m_voucher_type == VoucherTypes.CloseCash
        || m_voucher_type == VoucherTypes.DropBoxCount
        || m_voucher_type == VoucherTypes.OpenCash
        || m_voucher_type == VoucherTypes.FillIn
        || m_voucher_type == VoucherTypes.FillOut
        || m_voucher_type == VoucherTypes.AmountRequest
        || m_voucher_type == VoucherTypes.WinLoss)
       && (m_currencies.Count > 0))
      {
        if (m_voucher_type == VoucherTypes.WinLoss)
        {
          btn_intro_Click(null, null);
        }

        txt_amount.Text = "";
        txt_amount.Focus();

        SetCurrencyExchange();
        //if (Format.GetFormattedDecimals(uc_payment_method1.CurrencyExchange.CurrencyCode) != 0)
        //{
        //  this.btn_num_dot.Enabled = true;
        //}
        //else
        //{
        //  this.btn_num_dot.Enabled = false;
        //}

        // JAB 24-FEB-2014: Fixed Bug #WIG-667
        if (m_voucher_type == VoucherTypes.CardAdd)
        {
          CardAdd_ResetVoucher();
        }

        if (m_voucher_type == VoucherTypes.FillIn
            || m_voucher_type == VoucherTypes.FillOut
            || m_voucher_type == VoucherTypes.AmountRequest
            || m_voucher_type == VoucherTypes.CloseCash
            || m_voucher_type == VoucherTypes.DropBoxCount)
        {
          if (m_national_currency.CurrencyCode != uc_payment_method1.CurrencyExchange.CurrencyCode && m_national_currency.Type != CurrencyExchangeType.CURRENCY)
          {
            m_is_exchange = true;

            if (Cage.IsCageEnabled() == true)
            {
              UpdateTotalCageAmount();
            }
          }
          else
          {
            m_is_exchange = false;
            lbl_cage_currency_total.Text = "";
          }
        }

        m_show_rows_without_amount = true;
        HideShowRowsWithoutAmount(m_show_rows_without_amount);

        // Credit line can't apply promotions
        if (dgv_common.Rows.Count > 0 && CurrencyExchange.Type == CurrencyExchangeType.CREDITLINE)
        {
          dgv_common.Rows[0].Selected = true;
          dgv_common_CellClick(dgv_common, new DataGridViewCellEventArgs(0, 0));
        }
        btn_promos_change_view.Enabled = (CurrencyExchange.Type != CurrencyExchangeType.CREDITLINE);
        dgv_common.Enabled = (CurrencyExchange.Type != CurrencyExchangeType.CREDITLINE);

        if (CurrencyExchange.Type == CurrencyExchangeType.CARD || CurrencyExchange.Type == CurrencyExchangeType.CARD2GO)
        {
          _isCard = true;
        }

        HideShowColumnByCurrencyExchangeCard(_isCard);
      }
    } // btn_selected_currency_Click

    //------------------------------------------------------------------------------
    // PURPOSE : Reset Voucher.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boobean: 
    //
    //   NOTES :
    //
    private void CardAdd_ResetVoucher()
    {
      ExitPreview();

      m_input_amount = 0;
      lbl_total_to_pay.Text = "";
      btn_ok.Enabled = false;
      is_voucher_loaded = false;
    }

    private void ColumnsVisibilityAndWidth(CurrencyExchangeType CurrencyExchange)
    {
      Cage.ShowDenominationsMode _show_denominations_mode;

      _show_denominations_mode = (Cage.ShowDenominationsMode)GeneralParam.GetInt32("Cage", "ShowDenominations", 1);

      switch (CurrencyExchange)
      {
        case CurrencyExchangeType.CURRENCY:
          dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_CHIP_DRAW].Visible = false;
          dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_CHIP_NAME].Visible = false;
          dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_IMAGE].Visible = false;
          dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_DENOMINATION_SHOWED].Visible = true;
          dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_TOTAL].Visible = true;

          dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_DENOMINATION_SHOWED].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
          dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_UN].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
          dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_TOTAL].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;

          if (_show_denominations_mode != Cage.ShowDenominationsMode.ShowAllDenominations)
          {
            dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_TOTAL].Width = WIDTH_COLUMN_CAGE_AMOUNTS_CHIP_NAME + WIDTH_COLUMN_CAGE_AMOUNTS_DRAWING + WIDTH_COLUMN_CAGE_AMOUNTS_TOTAL;
          }
          else
          {
            dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_TOTAL].Width = WIDTH_COLUMN_CAGE_AMOUNTS_TOTAL;
          }

          dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_DENOMINATION_SHOWED].Width = WIDTH_COLUMN_CAGE_AMOUNTS_DENOMINATION_SHOWED;
          dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_UN].Width = WIDTH_COLUMN_CAGE_AMOUNTS_UN;

          break;
        case CurrencyExchangeType.CASINO_CHIP_RE:
        case CurrencyExchangeType.CASINO_CHIP_NRE:
          dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_CHIP_DRAW].Visible = true;
          dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_CHIP_NAME].Visible = true;
          dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_IMAGE].Visible = true;
          dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_DENOMINATION_SHOWED].Visible = true;
          dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_TOTAL].Visible = true;

          dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_DENOMINATION_SHOWED].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
          dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_UN].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
          dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_CHIP_NAME].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
          dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_CHIP_DRAW].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
          dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_TOTAL].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;

          dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_DENOMINATION_SHOWED].Width = WIDTH_COLUMN_CAGE_AMOUNTS_DENOMINATION_SHOWED;
          dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_UN].Width = WIDTH_COLUMN_CAGE_AMOUNTS_UN;
          dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_CHIP_NAME].Width = WIDTH_COLUMN_CAGE_AMOUNTS_CHIP_NAME;
          dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_CHIP_DRAW].Width = WIDTH_COLUMN_CAGE_AMOUNTS_DRAWING;
          dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_TOTAL].Width = WIDTH_COLUMN_CAGE_AMOUNTS_TOTAL;

          break;
        case CurrencyExchangeType.CASINO_CHIP_COLOR:
          dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_CHIP_DRAW].Visible = true;
          dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_CHIP_NAME].Visible = true;
          dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_IMAGE].Visible = true;
          dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_DENOMINATION_SHOWED].Visible = false;

          if ((_show_denominations_mode != Cage.ShowDenominationsMode.HideAllDenominations && m_voucher_type != VoucherTypes.WinLoss)
              || (m_voucher_type == VoucherTypes.WinLoss && m_win_loss_mode != GamingTableBusinessLogic.GT_WIN_LOSS_MODE.BY_TOTALS))
          {
            dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_TOTAL].Visible = false;
          }
          else
          {
            dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_TOTAL].Visible = true;
            dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_TOTAL].Width = WIDTH_COLUMN_CAGE_AMOUNTS_TOTAL;
          }

          dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_CHIP_DRAW].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
          dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_CHIP_NAME].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
          dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_UN].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;

          dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_UN].Width = WIDTH_COLUMN_CAGE_AMOUNTS_UN;
          break;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Set selected currency exchange
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    private void SetCurrencyExchange()
    {
      Boolean _allowed_money_entries;

      // RMS 08-AUG-2014
      m_selected_bank_card_type = CurrencyExchangeSubType.NONE;

      // RAB 13-APR-2016: PBI 10855: Visibility drawing columns according to chips.
      if (dgv_common.Rows.Count > 0 && dgv_common.Columns.Count > 10)
      {
        ColumnsVisibilityAndWidth(uc_payment_method1.CurrencyExchange.Type);
      }

      if (uc_payment_method1.CurrencyExchange.CurrencyCode == m_national_currency.CurrencyCode && uc_payment_method1.CurrencyExchange.Type == CurrencyExchangeType.CURRENCY)
      {
        HideNationalCurrency();
      }
      else
      {
        txt_amount.TextChanged -= new System.EventHandler(this.txt_amount_TextChanged);
        txt_amount.TextChanged += new System.EventHandler(this.txt_amount_TextChanged);
        m_with_currency_exchange = true;
      }

      m_selected_bank_card_type = uc_payment_method1.SelectedBankCardType;

      // To set grid data source property.
      _allowed_money_entries = true;
      if (m_cage_enabled)
      {
        // Set form mode (enabled/disabled) with returned value.
        //AllowedMoneyEntries(SetDenominationDataSource(m_currency_exchange.Type));
        _allowed_money_entries = SetDenominationDataSource(uc_payment_method1.CurrencyExchange);
      }

      AllowedCurrency(_allowed_money_entries);

      if (_allowed_money_entries && ((m_voucher_type == VoucherTypes.AmountRequest && m_request_movement_id == -1) ||
                                     (m_voucher_type != VoucherTypes.AmountRequest)))
      {
        InitMoneyButtons();
      }
    } // SetCurrencyExchange

    //------------------------------------------------------------------------------
    // PURPOSE : Initialize Amount Buttons money.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boobean: True when data source was successfully assign.
    //
    //   NOTES :
    //
    private Boolean SetDenominationDataSource(CurrencyExchange CurrencyExchange)
    {
      DataTable _current_data_source;
      CurrencyExchange _current_currency_exchange;
      String _curret_isocode;

      _current_data_source = new DataTable();
      _current_currency_exchange = (uc_payment_method1.CurrencyExchange == null) ? m_national_currency : uc_payment_method1.CurrencyExchange;

      _curret_isocode = _current_currency_exchange.CurrencyCode;
      switch (_current_currency_exchange.Type)
      {
        case CurrencyExchangeType.CHECK:
          _curret_isocode += Cage.CHECK_CODE.ToString();
          break;
        case CurrencyExchangeType.CARD:
          _curret_isocode += Cage.BANK_CARD_CODE.ToString();
          break;
      }

      if (m_cage_amounts.ContainsKey(_curret_isocode, _current_currency_exchange.Type))
      {
        _current_data_source = m_cage_amounts[_curret_isocode, _current_currency_exchange.Type].ItemAmounts;

      }
      else
      {
        return false;
      }

      dgv_common.DataSource = _current_data_source;

      UpdateTotalCageAmount();
      if (_current_data_source.Rows.Count == 0)
      {
        return false;
      }
      return true;
    } // SetDenominationDataSource

    public static Boolean AML_RequestPermissionToExceedReportLimit(ENUM_CREDITS_DIRECTION CreditsDirection, ENUM_ANTI_MONEY_LAUNDERING_LEVEL AmlLevel, Boolean ThresholdCrossed)
    {
      String _gp_prefix;
      ENUM_AML_REQUEST_PERMISSION_TO_EXCEED_LIMIT _request_permission;

      switch (CreditsDirection)
      {
        case ENUM_CREDITS_DIRECTION.Recharge:
          _gp_prefix = "Recharge";
          break;

        case ENUM_CREDITS_DIRECTION.Redeem:
          _gp_prefix = "Prize";
          break;

        default:
          return false;
      }

      switch (AmlLevel)
      {
        case ENUM_ANTI_MONEY_LAUNDERING_LEVEL.MaxAllowed:

          return true;

        case ENUM_ANTI_MONEY_LAUNDERING_LEVEL.Report:
          _request_permission = (ENUM_AML_REQUEST_PERMISSION_TO_EXCEED_LIMIT)GeneralParam.GetInt32("AntiMoneyLaundering", _gp_prefix + ".Report.RequestPermissionToExceedLimit");

          switch (_request_permission)
          {
            case ENUM_AML_REQUEST_PERMISSION_TO_EXCEED_LIMIT.OnlyWhenCross:
              return ThresholdCrossed;

            case ENUM_AML_REQUEST_PERMISSION_TO_EXCEED_LIMIT.Always:
              return true;

            case ENUM_AML_REQUEST_PERMISSION_TO_EXCEED_LIMIT.Never:
            default:
              return false;
          }

        default:
          return false;
      }
    } // AML_RequestPermissionToExceedReportLimit

    //------------------------------------------------------------------------------
    // PURPOSE : Fill data grid.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void CageFunctionality(VoucherTypes VoucherType)
    {
      Boolean _allowed_money_entries;
      CurrencyExchange _tmp_currency_exchange;
      DataSet _ds_gt_win_loss;

      _allowed_money_entries = true;
      _tmp_currency_exchange = (uc_payment_method1.CurrencyExchange == null) ? m_national_currency : uc_payment_method1.CurrencyExchange;
      _ds_gt_win_loss = new DataSet();

      FillDenominationGrid();
      if (m_currencies != null && m_currencies.Count > 1)
      {
        SetCurrencyExchange();
      }
      else
      {
        switch (_tmp_currency_exchange.Type)
        {
          case CurrencyExchangeType.CURRENCY:

            if (m_cage_amounts.ContainsKey(_tmp_currency_exchange.CurrencyCode, _tmp_currency_exchange.Type))
            {
              dgv_common.DataSource = m_cage_amounts[_tmp_currency_exchange.CurrencyCode, _tmp_currency_exchange.Type].ItemAmounts;
              UpdateTotalCageAmount();
            }
            else
            {
              _allowed_money_entries = false;
            }
            break;

          case CurrencyExchangeType.CARD:
          case CurrencyExchangeType.CHECK:
          default:

            if (m_cage_amounts.ContainsKey(_tmp_currency_exchange.Description, _tmp_currency_exchange.Type))
            {
              dgv_common.DataSource = m_cage_amounts[_tmp_currency_exchange.Description, _tmp_currency_exchange.Type].ItemAmounts;
              UpdateTotalCageAmount();
            }
            else
            {
              _allowed_money_entries = false;
            }
            break;
        }
        HideAllCurrencyExchange();
      }

      GuiStyleSheetCage();

      switch (VoucherType)
      {
        case VoucherTypes.OpenCash:
          // Always open with zero amount.
          m_dt_cage_pending_movements = new DataTable();
          _allowed_money_entries = false;

          break;

        case VoucherTypes.FillIn:
        case VoucherTypes.AmountRequest:

          if (_allowed_money_entries)
          {
            if (m_closing_stock != null)
            {
              _allowed_money_entries = true;
            }
            else if (m_is_pending_request)
            {
              _allowed_money_entries = m_is_pending_request;
              if (m_request_movement_id != -1)
              {
                GetPendingRequest(m_request_movement_id);
                _allowed_money_entries = false;
              }
            }
            else
            {
              _allowed_money_entries = GetPendingMovements();
            }
          }

          break;

        case VoucherTypes.CloseCash:
        case VoucherTypes.FillOut:

          m_num_of_retries = 0;
          m_dt_cage_pending_movements = new DataTable();

          break;

        case VoucherTypes.WinLoss:
          _ds_gt_win_loss = GetWinLossEntries();

          break;
        default:
          break;
      }

      if (
          (_allowed_money_entries && GeneralParam.GetBoolean("Cage", "CashDeposit.SuggestDenominationsAmounts", false) && VoucherType == VoucherTypes.FillIn) ||
          (VoucherType == VoucherTypes.AmountRequest && m_request_movement_id != -1 ||
          m_closing_stock != null && m_closing_stock.CurrenciesStocks != null && m_closing_stock.CurrenciesStocks.Rows.Count > 0 && m_closing_stock.SleepsOnTable && (VoucherType == VoucherTypes.FillOut || VoucherType == VoucherTypes.FillIn || VoucherType == VoucherTypes.AmountRequest)) ||
          (VoucherType == VoucherTypes.WinLoss && _ds_gt_win_loss.Tables[0].Rows.Count > 0)
         )
      {
        Int32 _index;
        Int32 _index_top;
        Int32 _index_dgv;

        // DHA 09-JUN-2016: validating closing cash fill grid
        if (m_closing_stock != null
          && m_closing_stock.CurrenciesStocks != null
          && (m_closing_stock.SleepsOnTable || (m_closing_stock.Type == ClosingStocks.ClosingStockType.FIXED && m_cage_automode))
          && m_closing_stock.CurrenciesStocks.Rows.Count > 0)
        {
          FillClosingStocksDenominationGridQuick(VoucherType);
        }
        else if (VoucherType == VoucherTypes.WinLoss)
        {
          FillWinLossGrid(_ds_gt_win_loss);
        }
        else
        {
          FillDenominationGridQuick();
        }

        _index_top = uc_payment_method1.GetCurrenciesCount();

        int totalrows = 0;

        for (_index = 0; _index <= _index_top; ++_index)
        {
          if (dgv_common.RowCount > 0)
          {
            dgv_common.Rows[0].Selected = true;
          }

          totalrows = dgv_common.RowCount - 1;

          for (_index_dgv = 0; _index_dgv <= totalrows; ++_index_dgv)
          {
            btn_intro_Click(null, null);
          }
          if (_index <= _index_top - 1)
          {
            uc_payment_method1.SelectNextCurrencyAllowed();
          }
        }
      }
      m_show_msg = false;
      AllowedMoneyEntries(_allowed_money_entries);

      // Select the first currency/chips with value
      Boolean _value_found;
      _value_found = false;

      for (Int32 _idx_cur = 0; _idx_cur <= uc_payment_method1.GetCurrenciesCount(); _idx_cur++)
      {
        if (dgv_common.RowCount > 0)
        {
          dgv_common.Rows[0].Selected = true;
        }
        foreach (DataGridViewRow _grid_row in dgv_common.Rows)
        {
          _grid_row.Selected = true;
          if (_grid_row.Cells[GRID_COLUMN_CAGE_AMOUNTS_UN].Value != DBNull.Value && (Decimal)_grid_row.Cells[GRID_COLUMN_CAGE_AMOUNTS_UN].Value > 0)
          {
            _value_found = true;
            uc_payment_method1_CurrencyChanged(uc_payment_method1.CurrencyExchange);

            break;
          }
        }
        if (_value_found)
        {
          break;
        }
        else if (_idx_cur <= uc_payment_method1.GetCurrenciesCount() - 1)
        {
          uc_payment_method1.SelectNextCurrencyAllowed();
        }
      }


    } // CageFunctionality

    //------------------------------------------------------------------------------
    // PURPOSE : Fill data grid -> only for mobile banks
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void CageFunctionalityMB()
    {
      Boolean _allowed_money_entries;
      CurrencyExchange _tmp_currency_exchange;

      _allowed_money_entries = true;
      _tmp_currency_exchange = (uc_payment_method1.CurrencyExchange == null) ? m_national_currency : uc_payment_method1.CurrencyExchange;

      FillDenominationGrid();
      if (m_currencies != null && m_currencies.Count > 1)
      {
        SetCurrencyExchange();
      }
      else
      {
        switch (_tmp_currency_exchange.Type)
        {
          case CurrencyExchangeType.CURRENCY:
            if (m_cage_amounts.ContainsKey(_tmp_currency_exchange.CurrencyCode, _tmp_currency_exchange.Type))
            {
              dgv_common.DataSource = m_cage_amounts[_tmp_currency_exchange.CurrencyCode, _tmp_currency_exchange.Type].ItemAmounts;
              UpdateTotalCageAmount();
            }
            else
            {
              _allowed_money_entries = false;
            }
            break;

          case CurrencyExchangeType.CARD:
          case CurrencyExchangeType.CHECK:
          default:
            if (m_cage_amounts.ContainsKey(_tmp_currency_exchange.Description, _tmp_currency_exchange.Type))
            {
              dgv_common.DataSource = m_cage_amounts[_tmp_currency_exchange.Description, _tmp_currency_exchange.Type].ItemAmounts;
              UpdateTotalCageAmount();
            }
            else
            {
              _allowed_money_entries = false;
            }
            break;
        }
        HideAllCurrencyExchange();
      }

      GuiStyleSheetCage();


      // TODO: es necesario esto�?
      /*if (
          (_allowed_money_entries && GeneralParam.GetBoolean("Cage", "CashDeposit.SuggestDenominationsAmounts", false) && VoucherType == VoucherTypes.FillIn) ||
          (VoucherType == VoucherTypes.AmountRequest && m_request_movement_id != -1)
         )
      {
        Int32 _index;
        Int32 _index_top;
        Int32 _index_dgv;

        FillDenominationGridQuick();

        _index_top = GetCurrenciesCount();

        for (_index = 0; _index <= _index_top; ++_index)
        {
          dgv_common.Rows[0].Selected = true;
          for (_index_dgv = 0; _index_dgv <= dgv_common.RowCount - 1; ++_index_dgv)
          {
            btn_intro_Click(null, null);
          }
          if (_index <= _index_top - 1)
          {
            ChangeCurrency();
          }
        }
      }*/
      m_show_msg = false;
      AllowedMoneyEntries(_allowed_money_entries);

    } // CageFunctionality

    //------------------------------------------------------------------------------
    // PURPOSE : Fill data grid.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void FillDenominationGrid()
    {
      DataTable _dt;
      StringBuilder _sb;
      String _currency_exchange;
      CageDenominationsItems.CageDenominationsItem _item;
      Decimal _tmp_decimal;
      Cage.ShowDenominationsMode _show_denominations_mode;
      Boolean _hide_denominations;

      // Working with Denomination in grid
      m_grid_type = GridType.CageDenominations;
      m_input_amount = null;
      m_spent_used = 0;

      _show_denominations_mode = (Cage.ShowDenominationsMode)GeneralParam.GetInt32("Cage", "ShowDenominations", 1);

      try
      {
        m_cage_amounts = new CageDenominationsItems.CageDenominationsDictionary();

        foreach (CurrencyExchange _currency in m_currencies)
        {
          _dt = Cage.CageAmountsInitTable();

          _currency_exchange = _currency.CurrencyCode;

          if (_currency.Type == CurrencyExchangeType.CURRENCY)
          {
            _sb = new StringBuilder();

            _sb.AppendLine("    SELECT   0 AS COLOR                                                          ");
            _sb.AppendLine("           , CGC_ISO_CODE AS ISO_CODE                                            ");
            _sb.AppendLine("           , CGC_DENOMINATION AS DENOMINATION                                    ");
            _sb.AppendLine("           , CGC_ALLOWED AS ALLOWED                                              ");
            _sb.AppendLine("           , CASE WHEN CGC_DENOMINATION < 0 THEN 'Z'                             ");
            _sb.AppendLine("                  ELSE CASE WHEN CGC_CAGE_CURRENCY_TYPE = 0 THEN @pBillText      ");
            _sb.AppendLine("           				     ELSE @pCointText END                                      ");
            _sb.AppendLine("           	 END AS CURRENCY_TYPE                                                ");
            _sb.AppendLine("           , CGC_CAGE_CURRENCY_TYPE AS CAGE_CURRENCY_TYPE_ENUM                   ");
            _sb.AppendLine("           , 0 AS CHIP_DRAW                                                      ");
            _sb.AppendLine("           , '' AS CHIP_NAME                                                     ");
            _sb.AppendLine("      FROM   CAGE_CURRENCIES                                                     ");
            _sb.AppendLine("     WHERE   CGC_ISO_CODE = @pIsoCode                                            ");

            if (m_voucher_type == VoucherTypes.WinLoss && m_win_loss_mode == GamingTableBusinessLogic.GT_WIN_LOSS_MODE.BY_TOTALS)
            {
              // HideAllDenominations
              _sb.AppendLine("       AND    CGC_DENOMINATION = -100            ");
            }
            else
            {
              switch (_show_denominations_mode)
              {
                case Cage.ShowDenominationsMode.HideAllDenominations:
                  // HideAllDenominations
                  _sb.AppendLine("       AND    CGC_DENOMINATION = -100            ");
                  break;

                case Cage.ShowDenominationsMode.ShowAllDenominations:
                  // ShowAllDenominations
                  _sb.AppendLine("       AND   (CGC_DENOMINATION = -100");
                  _sb.AppendLine("        OR    CGC_DENOMINATION = -200");
                  _sb.AppendLine("        OR    CGC_DENOMINATION >= 0)  ");
                  break;

                case Cage.ShowDenominationsMode.ShowOnlyChipsDenominations:
                  // ShowOnlyChipsDenominations
                  //DLL11111
                  _sb.AppendLine("        OR    AND CGC_DENOMINATION = -100    ");
                  break;
              }
            }
            _sb.AppendLine("  ORDER BY   CGC_ISO_CODE ");
            _sb.AppendLine("           , CURRENCY_TYPE");
            _sb.AppendLine("           , CGC_DENOMINATION DESC  ");

            using (DB_TRX _db_trx = new DB_TRX())
            {
              using (SqlCommand _sql_command = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
              {
                _sql_command.Parameters.Add("@pIsoCode", SqlDbType.NVarChar).Value = _currency_exchange;
                _sql_command.Parameters.Add("@pBillText", SqlDbType.NVarChar).Value = Resource.String("STR_VOUCHER_CARD_HANDPAY_SESSION_CURRENCY_TYPE_BILL");
                _sql_command.Parameters.Add("@pCointText", SqlDbType.NVarChar).Value = Resource.String("STR_VOUCHER_CARD_HANDPAY_SESSION_CURRENCY_TYPE_COIN");

                using (SqlDataAdapter _da = new SqlDataAdapter(_sql_command))
                {
                  _da.Fill(_dt);
                }
              }// SqlCommand
            }// DB_TRX
          }
          else if (FeatureChips.IsChipsType(_currency.Type))
          {
            _hide_denominations = (_show_denominations_mode == Cage.ShowDenominationsMode.HideAllDenominations && m_voucher_type != VoucherTypes.WinLoss)
                                  || (m_voucher_type == VoucherTypes.WinLoss && m_win_loss_mode == GamingTableBusinessLogic.GT_WIN_LOSS_MODE.BY_TOTALS);
            _dt = FeatureChips.ChipsSets.GetChipsByTypeToCageAmountTable(_currency.Type, _currency.CurrencyCode, GamingTableId, _hide_denominations);
          }

          else
          {
            Decimal _currency_type; // currency type

            _currency_type = -1 * (Decimal)_currency.Type;
            _currency_exchange += _currency_type;

            Cage.CageAmountsPrepareRow(ref _dt, (Object)DBNull.Value,
                                                (Object)DBNull.Value,
                                                (Object)_currency_exchange,
                                                (Object)_currency_type,
                                                (Object)true,
                                                (Object)Cage.GetDenomination(_currency_type, _currency_exchange),
                                                (Object)_currency_type,
              // Added because previos param sometimes is "B" or "C"
                                                (Object)_currency_type,
                                                (Object)DBNull.Value,
                                                (Object)DBNull.Value,
                                                (Object)DBNull.Value,
                                                (Object)DBNull.Value,
                                                (Object)DBNull.Value,
                                                (Object)DBNull.Value);
          }

          //_dt.Columns.Add("CAA_IMAGE", typeof(Image)); 

          String _current_amount;
          String _current_iso_code;

          for (Int32 _count_row = 0; _count_row <= _dt.Rows.Count - 1; ++_count_row)
          {
            _dt.Rows[_count_row]["IMAGE"] = WSI.Cashier.Resources.ResourceImages.chips;  // Resource.Image("chip");

            _current_iso_code = _dt.Rows[_count_row][SQL_COLUMN_CAGE_AMOUNTS_CAA_ISO_CODE].ToString();
            _current_amount = _dt.Rows[_count_row][SQL_COLUMN_CAGE_AMOUNTS_CAA_DENOMINATION].ToString();

            Decimal.TryParse(_current_amount, out _tmp_decimal);

            switch (Convert.ToInt32(_tmp_decimal))
            {
              case Cage.COINS_CODE:
                if (FeatureChips.IsCageCurrencyTypeChips((CageCurrencyType)_dt.Rows[_count_row]["CAGE_CURRENCY_TYPE_ENUM"]))
                {
                  if (m_voucher_type == VoucherTypes.CloseCash ||
                      m_voucher_type == VoucherTypes.FillOut)
                  {
                    _dt.Rows[_count_row][SQL_COLUMN_CAGE_AMOUNTS_CAA_DENOMINATION_SHOWED] = "*" + Resource.String("STR_CAGE_TIPS");

                  }
                  else
                  {
                    _dt.Rows[_count_row].Delete();
                  }
                }
                else
                {
                  _dt.Rows[_count_row][SQL_COLUMN_CAGE_AMOUNTS_CAA_DENOMINATION_SHOWED] = Resource.String("STR_CAGE_COINS");          // "Monedas"
                  _dt.Rows[_count_row][SQL_COLUMN_CAGE_AMOUNTS_CAA_TYPE] = ""; // Without currency type
                }

                break;
              case Cage.BANK_CARD_CODE:
                if (m_closing_stock == null || m_closing_stock.Type != ClosingStocks.ClosingStockType.ROLLING || m_closing_stock.CurrenciesStocksCredits != null)
                {
                  _dt.Rows[_count_row][SQL_COLUMN_CAGE_AMOUNTS_CAA_DENOMINATION_SHOWED] = Resource.String("STR_CAGE_BANKCARD", "1");  // "Tarjeta bancaria"
                  _dt.Rows[_count_row][SQL_COLUMN_CAGE_AMOUNTS_CAA_TYPE] = ""; // Without currency type
                }
                else
                {
                  _dt.Rows[_count_row].Delete();
                  _dt.AcceptChanges();
                }

                break;
              case Cage.CHECK_CODE:
                if (m_closing_stock == null || m_closing_stock.Type != ClosingStocks.ClosingStockType.ROLLING || m_closing_stock.CurrenciesStocksCredits != null)
                {
                  _dt.Rows[_count_row][SQL_COLUMN_CAGE_AMOUNTS_CAA_DENOMINATION_SHOWED] = Resource.String("STR_CAGE_CHECK", "1");     // "Cheque n"
                  _dt.Rows[_count_row][SQL_COLUMN_CAGE_AMOUNTS_CAA_TYPE] = ""; // Without currency type
                }
                else
                {
                  _dt.Rows[_count_row].Delete();
                  _dt.AcceptChanges();
                }
                break;
              case Cage.TICKETS_CODE:
                if (AddTicketsTITORegister(_current_iso_code))
                {
                  _dt.Rows[_count_row][SQL_COLUMN_CAGE_AMOUNTS_CAA_DENOMINATION_SHOWED] = Resource.String("STR_CAGE_TICKETS");         // "Ticket"
                  _dt.Rows[_count_row][SQL_COLUMN_CAGE_AMOUNTS_CAA_TYPE] = ""; // Without currency type
                }
                else
                {
                  _dt.Rows[_count_row].Delete();
                  _dt.AcceptChanges();
                }

                break;
              default:
                _dt.Rows[_count_row][SQL_COLUMN_CAGE_AMOUNTS_CAA_DENOMINATION_SHOWED] = Currency.Format(_tmp_decimal, "");

                break;
            }

          }

          _dt.AcceptChanges();

          _item = new CageDenominationsItems.CageDenominationsItem();
          _item.ItemAmounts = _dt;
          _item.TotalUnits = 0;
          _item.TotalAmount = 0;
          _item.TotalTips = 0;
          _item.AllowedFillOut = true;
          _item.HasTicketsTito = false;
          m_cage_amounts.Add(new CageDenominationsItems.CageDenominationKey(_currency_exchange, _currency.Type), _item);

        }


      }// try 
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    }                                    // FillDenominationGrid

    //------------------------------------------------------------------------------
    // PURPOSE : Fill data grid with movement data (FillIn).
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void FillDenominationGridQuick()
    {
      String _iso_code;
      String _denomination;
      String _currency_type;
      Int32 _caa_un;
      Decimal _caa_total;
      CageDenominationsItems.CageDenominationsItem _tmp_item;
      CageCurrencyType _cage_currency_type_enum;
      Int64 _chip_id;

      _iso_code = "";
      _denomination = "";
      _currency_type = "";
      _caa_un = 0;
      _cage_currency_type_enum = CageCurrencyType.Bill;
      _chip_id = 0;

      foreach (DataRow _dr_pending in m_dt_cage_pending_movements.Rows)
      {
        _iso_code = _dr_pending[SQL_COLUMN_CMD_ISO_CODE].ToString();
        _cage_currency_type_enum = String.IsNullOrEmpty(_dr_pending["CMD_CAGE_CURRENCY_TYPE"].ToString()) ? CageCurrencyType.Bill : (CageCurrencyType)_dr_pending["CMD_CAGE_CURRENCY_TYPE"];

        if (_cage_currency_type_enum == CageCurrencyType.Others)
        {
          _cage_currency_type_enum = CageCurrencyType.Bill;
        }

        if (m_cage_amounts.ContainsKey(_iso_code, _cage_currency_type_enum))
        {
          if ((Int32)_dr_pending[SQL_COLUMN_CMD_QUANTITY] < 0)
          {
            _denomination = _dr_pending[SQL_COLUMN_CMD_QUANTITY].ToString();
          }
          else
          {
            _denomination = _dr_pending[SQL_COLUMN_CMD_DENOMINATION].ToString();
          }

          DataRow[] _rows;

          _rows = null;

          if (FeatureChips.IsCageCurrencyTypeChips(_cage_currency_type_enum))
          {
            _chip_id = (Int64)_dr_pending["CMD_CHIP_ID"];
            _rows = m_cage_amounts[_iso_code, _cage_currency_type_enum].ItemAmounts.Select(String.Format("CHIP_ID = {0}", _chip_id));
          }
          else
          {
            if (_dr_pending["CMD_CAGE_CURRENCY_TYPE"].ToString() == "0")
            {
              _currency_type = Resource.String("STR_VOUCHER_CARD_HANDPAY_SESSION_CURRENCY_TYPE_BILL");
            }
            else if (_dr_pending["CMD_CAGE_CURRENCY_TYPE"].ToString() == "1")
            {
              _currency_type = Resource.String("STR_VOUCHER_CARD_HANDPAY_SESSION_CURRENCY_TYPE_COIN");
            }
            else
            {
              _currency_type = "";
            }

            _rows = m_cage_amounts[_iso_code, _cage_currency_type_enum].ItemAmounts.Select(String.Format("DENOMINATION = {0} AND CURRENCY_TYPE = '{1}'", Format.LocalNumberToDBNumber(_denomination), _currency_type));
          }

          if (_rows != null && _rows.Length > 0)
          {
            Int32.TryParse(_dr_pending[SQL_COLUMN_CMD_QUANTITY].ToString(), out _caa_un);
            Decimal.TryParse(_dr_pending[SQL_COLUMN_CMD_DENOMINATION].ToString(), out _caa_total);
            if (_caa_un > 0)
            {
              if (_cage_currency_type_enum == CageCurrencyType.ChipsColor)
              {
                _caa_total = _caa_un;
              }
              else
              {
                _caa_total = _caa_total * _caa_un;
              }
            }
            else
            {
              _caa_un = 1;
            }

            _rows[0]["UN"] = _caa_un;
            _rows[0]["TOTAL"] = _caa_total;

            _tmp_item = new CageDenominationsItems.CageDenominationsItem();
            _tmp_item.ItemAmounts = m_cage_amounts[_iso_code, _cage_currency_type_enum].ItemAmounts;
            _tmp_item.TotalAmount += _caa_total;
            _tmp_item.TotalUnits += _caa_un;
            _tmp_item.AllowedFillOut = m_cage_amounts[_iso_code, _cage_currency_type_enum].AllowedFillOut;
            _tmp_item.HasTicketsTito = m_cage_amounts[_iso_code, _cage_currency_type_enum].HasTicketsTito;

            m_cage_amounts[_iso_code, _cage_currency_type_enum] = _tmp_item;
          } // if
        }
      }
    } // FillDenominationGridQuick


    //------------------------------------------------------------------------------
    // PURPOSE : Fill data grid with win loss previus entry data
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void FillWinLossGrid(DataSet DataSetWinLoss)
    {
      String _iso_code;
      String _currency_type;
      Int32 _caa_un;
      Decimal _caa_total;
      CageDenominationsItems.CageDenominationsItem _tmp_item;
      CageCurrencyType _cage_currency_type_enum;
      Int64 _chip_id;

      _iso_code = "";
      _currency_type = "";
      _caa_un = 0;
      _cage_currency_type_enum = CageCurrencyType.Bill;
      _chip_id = 0;

      switch (m_win_loss_mode)
      {
        case GamingTableBusinessLogic.GT_WIN_LOSS_MODE.NONE:
        default:
          break;

        case GamingTableBusinessLogic.GT_WIN_LOSS_MODE.BY_DENOMINATION:
          foreach (DataRow _dr in DataSetWinLoss.Tables[0].Rows)
          {
            _iso_code = _dr["gtwl_iso_code"].ToString();
            _currency_type = _dr["gtwl_cage_currency_type"].ToString();
            _caa_un = 0;
            _caa_total = 0;
            _tmp_item = new CageDenominationsItems.CageDenominationsItem();
            _cage_currency_type_enum = (CageCurrencyType)_dr["gtwl_cage_currency_type"];
            _chip_id = 0;

            if (m_cage_amounts.ContainsKey(_iso_code, _cage_currency_type_enum))
            {
              foreach (DataRow _dr_chip in DataSetWinLoss.Tables[1].Rows)
              {
                DataRow[] _rows;
                _rows = null;

                _chip_id = (Int64)_dr_chip["gtwld_chip_id"];
                _rows = m_cage_amounts[_iso_code, _cage_currency_type_enum].ItemAmounts.Select(String.Format("CHIP_ID = {0}", _chip_id));

                if (_rows != null && _rows.Length > 0)
                {
                  Int32.TryParse(_dr_chip["gtwld_quantity"].ToString(), out _caa_un);
                  Decimal.TryParse(_dr_chip["gtwld_win_loss_detail_amount"].ToString(), out _caa_total);
                  if (_caa_un > 0)
                  {
                    if (_cage_currency_type_enum == CageCurrencyType.ChipsColor)
                    {
                      _caa_total = _caa_un;
                    }
                    else
                    {
                      _caa_total = _caa_total * _caa_un;
                    }
                  }
                  else
                  {
                    _caa_un = 1;
                  }

                  _rows[0]["UN"] = _caa_un;
                  _rows[0]["TOTAL"] = _caa_total;

                  _tmp_item.ItemAmounts = m_cage_amounts[_iso_code, _cage_currency_type_enum].ItemAmounts;
                  _tmp_item.TotalAmount += _caa_total;
                  _tmp_item.TotalUnits += _caa_un;
                  _tmp_item.AllowedFillOut = m_cage_amounts[_iso_code, _cage_currency_type_enum].AllowedFillOut;
                  _tmp_item.HasTicketsTito = m_cage_amounts[_iso_code, _cage_currency_type_enum].HasTicketsTito;

                  m_cage_amounts[_iso_code, _cage_currency_type_enum] = _tmp_item;

                } // if rows
              } // foreach
            }
          }
          break;

        case GamingTableBusinessLogic.GT_WIN_LOSS_MODE.BY_TOTALS:
          foreach (DataRow _dr in DataSetWinLoss.Tables[0].Rows)
          {
            _iso_code = _dr["gtwl_iso_code"].ToString();
            _currency_type = _dr["gtwl_cage_currency_type"].ToString();
            _caa_un = 0;
            _caa_total = 0;
            _tmp_item = new CageDenominationsItems.CageDenominationsItem();
            _cage_currency_type_enum = (CageCurrencyType)_dr["gtwl_cage_currency_type"];
            _chip_id = 0;

            if (m_cage_amounts.ContainsKey(_iso_code, _cage_currency_type_enum))
            {
              DataRow[] _rows;
              _rows = null;

              _rows = m_cage_amounts[_iso_code, _cage_currency_type_enum].ItemAmounts.Select();

              if (_rows != null && _rows.Length > 0)
              {
                _caa_un = 1;
                Decimal.TryParse(_dr["gtwl_win_loss_amount"].ToString(), out _caa_total);
                if (_dr["gtwl_win_loss_amount"].ToString() != String.Empty)
                {
                  _rows[0]["UN"] = _caa_total / Convert.ToInt32(_rows[0]["DENOMINATION"]);
                  _rows[0]["TOTAL"] = _caa_total;
                }
                else
                {
                  _rows[0]["UN"] = DBNull.Value;
                  _rows[0]["TOTAL"] = DBNull.Value;
                }

                _tmp_item.ItemAmounts = m_cage_amounts[_iso_code, _cage_currency_type_enum].ItemAmounts;
                _tmp_item.TotalAmount += _caa_total;
                _tmp_item.TotalUnits += _caa_un;
                _tmp_item.AllowedFillOut = m_cage_amounts[_iso_code, _cage_currency_type_enum].AllowedFillOut;
                _tmp_item.HasTicketsTito = m_cage_amounts[_iso_code, _cage_currency_type_enum].HasTicketsTito;

                m_cage_amounts[_iso_code, _cage_currency_type_enum] = _tmp_item;
              } // if rows
            }
          }

          break;
      }
    } // FillWinLossGrid

    //------------------------------------------------------------------------------
    // PURPOSE : Fill data grid with Closing Cash.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void FillClosingStocksDenominationGridQuick(VoucherTypes VoucherType)
    {
      String _iso_code;
      String _denomination;
      String _currency_type;
      Int32 _caa_un;
      Decimal _caa_total;
      CageDenominationsItems.CageDenominationsItem _tmp_item;
      CageCurrencyType _cage_currency_type_enum;
      Int64 _chip_id;
      DataTable _currencies_stocks;
      Boolean _load_template;

      _load_template = GeneralParam.GetBoolean("GamingTables", "ClosingStock.SuggestDeposit", true);

      _iso_code = "";
      _denomination = "";
      _currency_type = "";
      _caa_un = 0;
      _cage_currency_type_enum = CageCurrencyType.Bill;
      _chip_id = 0;
      _currencies_stocks = m_closing_stock.CurrenciesStocks;

      if (m_closing_stock.IsClosing && (VoucherType == VoucherTypes.FillIn && Cage.IsCageAutoMode() || VoucherType == VoucherTypes.AmountRequest && !Cage.IsCageAutoMode()))
      {
        _currencies_stocks = m_closing_stock.CurrenciesStocksFills;
      }
      else if (m_closing_stock.IsClosing && VoucherType == VoucherTypes.FillOut)
      {
        _currencies_stocks = m_closing_stock.CurrenciesStocksCredits;
      }
      else if (VoucherType != VoucherTypes.FillIn)
      {
        return;
      }
      else if (!_load_template)
      {
        return;
      }

      foreach (DataRow _dr_currencies_stock in _currencies_stocks.Rows)
      {
        _iso_code = _dr_currencies_stock["ISO_CODE"].ToString();
        _cage_currency_type_enum = String.IsNullOrEmpty(_dr_currencies_stock["CAGE_CURRENCY_TYPE"].ToString()) ? CageCurrencyType.Bill : (CageCurrencyType)_dr_currencies_stock["CAGE_CURRENCY_TYPE"];

        if (m_cage_amounts.ContainsKey(_iso_code, _cage_currency_type_enum) ||
          (_cage_currency_type_enum == CageCurrencyType.BankCard || _cage_currency_type_enum == CageCurrencyType.Check && m_cage_amounts.ContainsKey(_iso_code, CageCurrencyType.Others)))
        {
          if (_dr_currencies_stock["QUANTITY"] == DBNull.Value || (Int32)_dr_currencies_stock["QUANTITY"] == 0)
          {
            continue;
          }

          if ((Int32)_dr_currencies_stock["QUANTITY"] < 0)
          {
            _denomination = _dr_currencies_stock["QUANTITY"].ToString();
          }
          else
          {
            _denomination = _dr_currencies_stock["DENOMINATION"].ToString();
          }

          DataRow[] _rows;

          _rows = null;

          if (FeatureChips.IsCageCurrencyTypeChips(_cage_currency_type_enum))
          {
            _chip_id = (Int64)_dr_currencies_stock["CHIP_ID"];
            _rows = m_cage_amounts[_iso_code, _cage_currency_type_enum].ItemAmounts.Select(String.Format("CHIP_ID = {0}", _chip_id));
          }
          else
          {
            if (_dr_currencies_stock["CAGE_CURRENCY_TYPE"].ToString() == "0")
            {
              _currency_type = Resource.String("STR_VOUCHER_CARD_HANDPAY_SESSION_CURRENCY_TYPE_BILL");
            }
            else if (_dr_currencies_stock["CAGE_CURRENCY_TYPE"].ToString() == "1")
            {
              _currency_type = Resource.String("STR_VOUCHER_CARD_HANDPAY_SESSION_CURRENCY_TYPE_COIN");
            }
            else
            {
              _currency_type = "";
            }

            if (_cage_currency_type_enum == CageCurrencyType.BankCard || _cage_currency_type_enum == CageCurrencyType.Check)
            {
              _rows = null;
              if (m_cage_amounts.ContainsKey(new CageDenominationsItems.CageDenominationKey(_iso_code, CageCurrencyType.Others)))
              {
                _rows = m_cage_amounts[_iso_code, CageCurrencyType.Others].ItemAmounts.Select(String.Format("DENOMINATION = {0} AND CAGE_CURRENCY_TYPE_ENUM = '{1}'", Format.LocalNumberToDBNumber(_denomination.ToString()), (Int32)_cage_currency_type_enum));
              }

              _cage_currency_type_enum = CageCurrencyType.Others;
            }
            else
            {
              _rows = m_cage_amounts[_iso_code, _cage_currency_type_enum].ItemAmounts.Select(String.Format("DENOMINATION = {0} AND CAGE_CURRENCY_TYPE_ENUM = '{1}'", Format.LocalNumberToDBNumber(_denomination.ToString()), (Int32)_cage_currency_type_enum));
            }
          }

          if (_rows != null && _rows.Length > 0)
          {
            Int32.TryParse(_dr_currencies_stock["QUANTITY"].ToString(), out _caa_un);
            Decimal.TryParse(_dr_currencies_stock["DENOMINATION"].ToString(), out _caa_total);
            if (_caa_un > 0)
            {
              if (_cage_currency_type_enum == CageCurrencyType.ChipsColor)
              {
                _caa_total = _caa_un;
              }
              else if (_caa_total < 0)
              {
                Decimal.TryParse(_dr_currencies_stock["TOTAL"].ToString(), out _caa_total);
              }
              else
              {
                _caa_total = _caa_total * _caa_un;
              }
            }
            else
            {
              _caa_un = 1;
            }

            _rows[0]["UN"] = _caa_un;
            _rows[0]["TOTAL"] = _caa_total;

            _tmp_item = new CageDenominationsItems.CageDenominationsItem();
            _tmp_item.ItemAmounts = m_cage_amounts[_iso_code, _cage_currency_type_enum].ItemAmounts;
            _tmp_item.TotalAmount += _caa_total;
            _tmp_item.TotalUnits += _caa_un;
            _tmp_item.AllowedFillOut = m_cage_amounts[_iso_code, _cage_currency_type_enum].AllowedFillOut;
            _tmp_item.HasTicketsTito = m_cage_amounts[_iso_code, _cage_currency_type_enum].HasTicketsTito;

            m_cage_amounts[_iso_code, _cage_currency_type_enum] = _tmp_item;
          } // if
        }
      }
    } // FillClosingStocksDenominationGridQuick

    //------------------------------------------------------------------------------
    // PURPOSE : Determine if the tickets TITO register must to appeare.
    //
    //  PARAMS :
    //      - INPUT :
    ///           String: CurrentISOCode
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True:   must add ticket tito register
    //
    //   NOTES :
    //  
    private Boolean AddTicketsTITORegister(String CurrentISOCode)
    {
      String _national_currency;
      Boolean _add_tickets_TITO_register;

      _add_tickets_TITO_register = false;
      _national_currency = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode");

      if (!m_tito_enabled || (m_closing_stock != null && m_closing_stock.SleepsOnTable))
      {
        return false;
      }

      if (CurrentISOCode == _national_currency && m_tito_tickets_collection &&
         (m_voucher_type == VoucherTypes.CloseCash || m_voucher_type == VoucherTypes.FillOut) &&
         (m_closing_stock.Type == ClosingStocks.ClosingStockType.FIXED))
      {
        _add_tickets_TITO_register = true;
      }

      // Virtual tables remove tickets TITO register
      if (IsGamingTable && Cashier.TerminalId != CashierSessionInfo.TerminalId && !GeneralParam.GetBoolean("Cage", "GamingTables.TicketsCollection.Enabled", false))
      {
        _add_tickets_TITO_register = false;
      }

      return _add_tickets_TITO_register;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Format common data grid.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //    
    private void GuiStyleSheetCage()
    {
      Cage.ShowDenominationsMode _show_denominations_mode;
      Boolean _show_denomination_showed;
      Boolean _show_amounts_un;
      Int32 _width_cage_amounts_total;
      CurrencyExchange _tmp_currency_exchange;

      _width_cage_amounts_total = WIDTH_COLUMN_CAGE_AMOUNTS_TOTAL;
      _show_denomination_showed = true;
      _show_amounts_un = true;

      _show_denominations_mode = (Cage.ShowDenominationsMode)GeneralParam.GetInt32("Cage", "ShowDenominations", 1);
      _tmp_currency_exchange = (uc_payment_method1.CurrencyExchange == null) ? m_national_currency : uc_payment_method1.CurrencyExchange;

      if (m_voucher_type == VoucherTypes.WinLoss && m_win_loss_mode == GamingTableBusinessLogic.GT_WIN_LOSS_MODE.BY_TOTALS)
      {
        _show_denominations_mode = Cage.ShowDenominationsMode.HideAllDenominations;
      }

      switch (_show_denominations_mode)
      {
        case Cage.ShowDenominationsMode.HideAllDenominations:
        case Cage.ShowDenominationsMode.ShowOnlyChipsDenominations:
          _width_cage_amounts_total = WIDTH_COLUMN_CAGE_AMOUNTS_TOTAL + WIDTH_COLUMN_CAGE_AMOUNTS_DENOMINATION_SHOWED + WIDTH_COLUMN_CAGE_AMOUNTS_UN;
          _show_amounts_un = false;
          _show_denomination_showed = false;

          break;

        case Cage.ShowDenominationsMode.ShowAllDenominations:
          _width_cage_amounts_total = WIDTH_COLUMN_CAGE_AMOUNTS_TOTAL;
          _show_amounts_un = true;
          _show_denomination_showed = true;

          break;

      }

      // Columns
      dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_CHIP_DRAW].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
      dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_CHIP_DRAW].Width = WIDTH_COLUMN_CAGE_AMOUNTS_DRAWING;
      dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_CHIP_DRAW].HeaderCell.Value = Resource.String("STR_UC_CARD_BTN_PRIZE_DRAW"); // Dibujo      
      dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_CHIP_DRAW].Visible = _tmp_currency_exchange.Type == CurrencyExchangeType.CASINOCHIP;
      dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_CHIP_DRAW].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
      dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_CHIP_DRAW].SortMode = DataGridViewColumnSortMode.NotSortable;

      dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_CHIP_NAME].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
      dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_CHIP_NAME].Width = WIDTH_COLUMN_CAGE_AMOUNTS_CHIP_NAME;
      dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_CHIP_NAME].HeaderCell.Value = Resource.String("STR_VOUCHER_CASH_CHIPS_NAME"); // Nombre      
      dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_CHIP_NAME].Visible = _tmp_currency_exchange.Type == CurrencyExchangeType.CASINOCHIP;
      dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_CHIP_NAME].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
      dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_CHIP_NAME].SortMode = DataGridViewColumnSortMode.NotSortable;

      dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_ISO_CODE].HeaderCell.Value = "";
      dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_ISO_CODE].Width = WIDTH_COLUMN_CAGE_AMOUNTS_ISO_CODE;
      dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_ISO_CODE].Visible = false;

      dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_DENOMINATION].HeaderCell.Value = "";
      dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_DENOMINATION].Width = WIDTH_COLUMN_CAGE_AMOUNTS_DENOMINATION;
      dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_DENOMINATION].Visible = false;

      dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_DENOMINATION_SHOWED].HeaderCell.Value = Resource.String("STR_VOUCHER_CARD_HANDPAY_SESSION_DENOMINATION");
      dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_DENOMINATION_SHOWED].Visible = _show_denomination_showed;
      dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_DENOMINATION_SHOWED].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
      dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_DENOMINATION_SHOWED].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
      dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_DENOMINATION_SHOWED].SortMode = DataGridViewColumnSortMode.NotSortable;
      dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_DENOMINATION_SHOWED].Width = WIDTH_COLUMN_CAGE_AMOUNTS_DENOMINATION_SHOWED;

      dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_CURRENCY_TYPE].HeaderCell.Value = Resource.String("STR_VOUCHER_CARD_HANDPAY_SESSION_CURRENCY_HEAD"); // "T";
      dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_CURRENCY_TYPE].Width = WIDTH_COLUMN_CAGE_AMOUNTS_DENOMINATION_CURRENCY_TYPE;
      dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_CURRENCY_TYPE].Visible = true;
      dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_CURRENCY_TYPE].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
      dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_CURRENCY_TYPE].SortMode = DataGridViewColumnSortMode.NotSortable;


      dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_ALLOWED].HeaderCell.Value = "";
      dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_ALLOWED].Width = WIDTH_COLUMN_CAGE_AMOUNTS_ALLOWED;
      dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_ALLOWED].Visible = false;

      dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_UN].HeaderCell.Value = Resource.String("STR_FRM_TITO_PROMO_005");
      dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_UN].Width = WIDTH_COLUMN_CAGE_AMOUNTS_UN;
      dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_UN].Visible = _show_amounts_un;
      dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_UN].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
      dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_UN].SortMode = DataGridViewColumnSortMode.NotSortable;

      dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_TOTAL].HeaderCell.Value = Resource.String("STR_CAGE_TEXT_COLUMN_TOTAL"); // "Total";
      dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_TOTAL].Width = _width_cage_amounts_total;
      dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_TOTAL].Visible = true;
      dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_TOTAL].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
      dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_TOTAL].SortMode = DataGridViewColumnSortMode.NotSortable;

      dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_COLOR].HeaderCell.Value = ""; // "";
      dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_COLOR].Width = WIDTH_COLUMN_CAGE_AMOUNTS_COLOR;
      dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_COLOR].Visible = false;
      dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_COLOR].SortMode = DataGridViewColumnSortMode.NotSortable;

      dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_IMAGE].HeaderCell.Value = ""; // "";
      dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_IMAGE].Width = WIDTH_COLUMN_CAGE_AMOUNTS_NOT_SHOWED;
      dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_IMAGE].Visible = false;
      dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_IMAGE].SortMode = DataGridViewColumnSortMode.NotSortable;

      dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_CURRENCY_TYPE_ENUM].HeaderCell.Value = ""; // "";
      dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_CURRENCY_TYPE_ENUM].Width = WIDTH_COLUMN_CAGE_AMOUNTS_NOT_SHOWED;
      dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_CURRENCY_TYPE_ENUM].Visible = false;
      dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_CURRENCY_TYPE_ENUM].SortMode = DataGridViewColumnSortMode.NotSortable;

      dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_CHIP_ID].HeaderCell.Value = ""; // "";
      dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_CHIP_ID].Width = WIDTH_COLUMN_CAGE_AMOUNTS_NOT_SHOWED;
      dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_CHIP_ID].Visible = false;
      dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_CHIP_ID].SortMode = DataGridViewColumnSortMode.NotSortable;

      dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_CHIP_SET_ID].HeaderCell.Value = ""; // "";
      dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_CHIP_SET_ID].Width = WIDTH_COLUMN_CAGE_AMOUNTS_NOT_SHOWED;
      dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_CHIP_SET_ID].Visible = false;
      dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_CHIP_SET_ID].SortMode = DataGridViewColumnSortMode.NotSortable;

      dgv_common.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
      dgv_common.MultiSelect = false;
    }// GuiStyleSheetCage

    //------------------------------------------------------------------------------
    // PURPOSE : Obtain pending request.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True:   There are movements.
    //      - False:  There aren't movements.
    //
    //   NOTES :
    //    
    private Boolean GetPendingRequest(Int64 RequestMovementId)
    {
      StringBuilder _sb;

      m_current_pending_movement = 0;

      _sb = new StringBuilder();
      _sb.AppendLine("     SELECT   cgm_movement_id                                                 ");
      _sb.AppendLine("            , cgm_user_cashier_id                                             ");
      _sb.AppendLine("            , cmd_cage_movement_detail_id                                     ");
      _sb.AppendLine("            , cmd_movement_id                                                 ");
      _sb.AppendLine("            , cmd_quantity                                                    ");
      _sb.AppendLine("            , cmd_denomination                                                ");
      _sb.AppendLine("            , cmd_iso_code                                                    ");
      _sb.AppendLine("            , cmd_cage_currency_type                                          ");
      _sb.AppendLine("            , cmd_chip_id                                                     ");
      _sb.AppendLine("       FROM   cage_movements                                                  ");
      _sb.AppendLine(" INNER JOIN   cage_movement_details ON cgm_movement_id = cmd_movement_id      ");
      _sb.AppendLine("      WHERE   cgm_movement_id =   @pRequestMovementId                         ");

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_command = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_command.Parameters.Add("@pRequestMovementId", SqlDbType.Int).Value = RequestMovementId;

            m_dt_cage_pending_movements = new DataTable();
            m_dt_cage_pending_movements.Columns.Add("cgm_movement_id", Type.GetType("System.Int64"));
            m_dt_cage_pending_movements.Columns.Add("cgm_user_cashier_id", Type.GetType("System.Int32"));

            m_dt_cage_pending_movements.Columns.Add("CMD_CAGE_MOVEMENT_DETAIL_ID", Type.GetType("System.Int64"));
            m_dt_cage_pending_movements.Columns.Add("CMD_MOVEMENT_ID", Type.GetType("System.Int64"));
            m_dt_cage_pending_movements.Columns.Add("CMD_QUANTITY", Type.GetType("System.Int32"));
            m_dt_cage_pending_movements.Columns.Add("CMD_DENOMINATION", Type.GetType("System.Decimal"));
            m_dt_cage_pending_movements.Columns.Add("CMD_ISO_CODE", Type.GetType("System.String"));
            m_dt_cage_pending_movements.Columns.Add("CMD_CAGE_CURRENCY_TYPE", Type.GetType("System.Int32"));
            m_dt_cage_pending_movements.Columns.Add("CMD_CHIP_ID", Type.GetType("System.Int64"));

            m_dt_cage_pending_movements.PrimaryKey = new DataColumn[] { m_dt_cage_pending_movements.Columns["CMD_CAGE_MOVEMENT_DETAIL_ID"] };

            using (SqlDataAdapter _da = new SqlDataAdapter(_sql_command))
            {
              _da.Fill(m_dt_cage_pending_movements);
            }
          }
        }

        m_cage_movement_status = Cage.CageStatus.Sent;

        return (m_dt_cage_pending_movements.Rows.Count > 0) ? true : false;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return false;
      }

      //return true;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Obtain pending movements.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True:   There are movements.
    //      - False:  There aren't movements.
    //
    //   NOTES :
    //    
    private Boolean GetPendingMovements()
    {
      StringBuilder _sb;
      Boolean _is_gaming_day_enabled;

      _is_gaming_day_enabled = Misc.IsGamingDayEnabled() && Cashier.GamingDay != DateTime.MinValue;

      m_current_pending_movement = 0;

      _sb = new StringBuilder();
      _sb.AppendLine("   DECLARE   @pMovementId as bigint                                        ");
      _sb.AppendLine("    SELECT   TOP 1 @pMovementId = cpm_movement_id                          ");
      _sb.AppendLine("      FROM   cage_pending_movements                                        ");
      _sb.AppendLine("INNER JOIN   cage_movements ON cgm_movement_id = cpm_movement_id           ");

      if (_is_gaming_day_enabled)
      {
        _sb.AppendLine("INNER JOIN   CAGE_SESSIONS ON CGM_CAGE_SESSION_ID = CGS_CAGE_SESSION_ID  ");
        _sb.AppendLine("       AND   CAGE_SESSIONS.CGS_WORKING_DAY = @pGamingDay                 ");
      }

      _sb.AppendLine("     WHERE   cpm_user_id = @pUserId                                        ");
      _sb.AppendLine("       AND   cpm_type   = @pTypeUser                                       ");


      _sb.AppendLine("   IF @pMovementId IS NULL                                                 ");
      _sb.AppendLine("   BEGIN                                                                   ");
      _sb.AppendLine("   	    SELECT TOP 1 @pMovementId = cpm_movement_id                        ");
      _sb.AppendLine("   	      FROM cage_pending_movements                                      ");
      _sb.AppendLine("   	INNER JOIN cage_movements ON cgm_movement_id = cpm_movement_id         ");

      if (_is_gaming_day_enabled)
      {
        _sb.AppendLine("INNER JOIN   CAGE_SESSIONS ON CGM_CAGE_SESSION_ID = CGS_CAGE_SESSION_ID  ");
        _sb.AppendLine("       AND   CAGE_SESSIONS.CGS_WORKING_DAY = @pGamingDay                 ");
      }

      _sb.AppendLine("   	     WHERE cpm_user_id = @pCashierId                                   ");
      _sb.AppendLine("   		     AND cpm_type = @pTypeCashier                                    ");
      _sb.AppendLine("   		     AND cgm_status = @pStatusInitial                                ");
      _sb.AppendLine("   END   ");
      _sb.AppendLine("--SELECT @pMovementId");

      _sb.AppendLine("   IF @pMovementId IS NULL                                                 ");
      _sb.AppendLine("   BEGIN                                                                   ");
      _sb.AppendLine("   	    SELECT TOP 1 @pMovementId = cpm_movement_id                        ");
      _sb.AppendLine("   	      FROM cage_pending_movements                                      ");
      _sb.AppendLine("   	INNER JOIN cage_movements ON cgm_movement_id = cpm_movement_id         ");

      if (_is_gaming_day_enabled)
      {
        _sb.AppendLine("INNER JOIN   CAGE_SESSIONS ON CGM_CAGE_SESSION_ID = CGS_CAGE_SESSION_ID  ");
        _sb.AppendLine("       AND   CAGE_SESSIONS.CGS_WORKING_DAY = @pGamingDay                 ");
      }

      _sb.AppendLine("   	     WHERE cpm_user_id = @pCashierId                                   ");
      _sb.AppendLine("   		     AND cpm_type = @pTypeGamingTable                                ");
      _sb.AppendLine("   		     AND cgm_status = @pStatusInitial                                ");
      _sb.AppendLine("   END   ");
      _sb.AppendLine("--SELECT @pMovementId");

      //if (m_voucher_type == VoucherTypes.FillIn)
      //{
      //  _sb.AppendLine("  UPDATE   cage_movements                                                ");
      //  _sb.AppendLine("     SET   cgm_status = @pStatus                                         ");
      //  _sb.AppendLine("   WHERE   cgm_movement_id = @pMovementId                                ");
      //}

      _sb.AppendLine("    SELECT   *                                                             ");
      _sb.AppendLine("      FROM   cage_pending_movements                                        ");
      _sb.AppendLine("INNER JOIN   cage_movement_details ON cmd_movement_id =  cpm_movement_id   ");

      // TODO: DHA Revisar: hace falta ISO Code???
      //_sb.AppendLine("INNER JOIN   currencies on cur_iso_code = cmd_iso_code                     ");

      //_sb.AppendLine("     WHERE   cpm_user_id = @pUserId                                        ");
      //_sb.AppendLine("       AND   cpm_movement_id = @pMovementId                                ");
      _sb.AppendLine("     WHERE   cpm_movement_id = @pMovementId                                ");
      _sb.AppendLine("  ORDER BY   cmd_iso_code DESC, cmd_quantity DESC                          ");

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_command = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            //            _sql_command.Parameters.Add("@pStatus", SqlDbType.Int).Value = Cage.CageStatus.InProgress;
            _sql_command.Parameters.Add("@pUserId", SqlDbType.Int).Value = CashierSessionInfo.UserId;
            _sql_command.Parameters.Add("@pStatusInitial", SqlDbType.Int).Value = Cage.CageStatus.Sent;
            _sql_command.Parameters.Add("@pCashierId", SqlDbType.Int).Value = CashierSessionInfo.TerminalId;
            _sql_command.Parameters.Add("@pTypeUser", SqlDbType.Int).Value = Cage.PendingMovementType.ToCashierUser;
            _sql_command.Parameters.Add("@pTypeCashier", SqlDbType.Int).Value = Cage.PendingMovementType.ToCashierTerminal;
            _sql_command.Parameters.Add("@pTypeGamingTable", SqlDbType.Int).Value = Cage.PendingMovementType.ToGamingTable;

            if (_is_gaming_day_enabled)
            {
              _sql_command.Parameters.Add("@pGamingDay", SqlDbType.DateTime).Value = Cashier.GamingDay;
            }

            m_dt_cage_pending_movements = new DataTable();
            m_dt_cage_pending_movements.Columns.Add("CPM_MOVEMENT_ID", Type.GetType("System.Int64"));
            m_dt_cage_pending_movements.Columns.Add("CPM_USER_ID", Type.GetType("System.Int32"));

            m_dt_cage_pending_movements.Columns.Add("CMD_CAGE_MOVEMENT_DETAIL_ID", Type.GetType("System.Int64"));
            m_dt_cage_pending_movements.Columns.Add("CMD_MOVEMENT_ID", Type.GetType("System.Int64"));
            m_dt_cage_pending_movements.Columns.Add("CMD_QUANTITY", Type.GetType("System.Int32"));
            m_dt_cage_pending_movements.Columns.Add("CMD_DENOMINATION", Type.GetType("System.Decimal"));
            m_dt_cage_pending_movements.Columns.Add("CMD_ISO_CODE", Type.GetType("System.String"));

            m_dt_cage_pending_movements.Columns.Add("CUR_ISO_CODE", Type.GetType("System.String"));
            m_dt_cage_pending_movements.Columns.Add("CUR_ALLOWED", Type.GetType("System.Boolean"));
            m_dt_cage_pending_movements.Columns.Add("CUR_NAME", Type.GetType("System.String"));
            m_dt_cage_pending_movements.Columns.Add("CUR_ALIAS1", Type.GetType("System.String"));
            m_dt_cage_pending_movements.Columns.Add("CUR_ALIAS2", Type.GetType("System.String"));

            m_dt_cage_pending_movements.Columns.Add("CMD_CAGE_CURRENCY_TYPE", typeof(CurrencyExchangeType));

            m_dt_cage_pending_movements.Columns.Add("CMD_CHIP_ID", Type.GetType("System.Int64"));
            m_dt_cage_pending_movements.Columns.Add("CPM_TYPE", Type.GetType("System.Int32"));

            m_dt_cage_pending_movements.PrimaryKey = new DataColumn[] { m_dt_cage_pending_movements.Columns["CPM_MOVEMENT_ID"] 
                                                                       , m_dt_cage_pending_movements.Columns["CMD_CAGE_MOVEMENT_DETAIL_ID"] };

            using (SqlDataAdapter _da = new SqlDataAdapter(_sql_command))
            {
              _da.Fill(m_dt_cage_pending_movements);
            }
          }// SqlCommand

          //_db_trx.Commit(); //JCA No es necesario pues no se hace ninguna operaci�n excepto Select

          m_cage_movement_status = Cage.CageStatus.Sent;
        }//  DB_TRX

        return (m_dt_cage_pending_movements.Rows.Count > 0) ? true : ((m_cage_automode) ? true : false);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

        return false;
    }                                  // GetPendingMovements

    //------------------------------------------------------------------------------
    // PURPOSE : Obtain Win Loss Entries.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Dataset:   with one or two tables with data read.
    //                or Empty 
    //
    //   NOTES :
    //    
    private DataSet GetWinLossEntries()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine("   SELECT   gtwl_id                           ");
      _sb.AppendLine("          , gtwl_gaming_table_session_id      ");
      _sb.AppendLine("          , gtwl_datetime_hour                ");
      _sb.AppendLine("          , gtwl_iso_code                     ");
      _sb.AppendLine("          , gtwl_cage_currency_type           ");
      _sb.AppendLine("          , gtwl_win_loss_amount              ");
      _sb.AppendLine("          , gtwl_user_id                      ");
      _sb.AppendLine("          , gtwl_last_update                  ");
      _sb.AppendLine("     FROM   gaming_tables_win_loss            ");
      _sb.AppendFormat("    WHERE   gtwl_id IN ({0})", NumberListToString(m_gt_win_loss_ids));
      _sb.AppendLine(" ");

      _sb.AppendLine("   SELECT   gtwld_id                          ");
      _sb.AppendLine("          , gtwld_chip_id                     ");
      _sb.AppendLine("          , gtwld_denomination                ");
      _sb.AppendLine("          , gtwld_quantity                    ");
      _sb.AppendLine("          , gtwld_win_loss_detail_amount      ");
      _sb.AppendLine("     FROM   gaming_tables_win_loss_detail     ");
      _sb.AppendFormat("    WHERE   gtwld_id IN ({0})", NumberListToString(m_gt_win_loss_ids));

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_command = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            using (DataSet _ds = new DataSet())
            {
              using (SqlDataAdapter _da = new SqlDataAdapter(_sql_command))
              {
                _da.Fill(_ds);

                return _ds;
              }
            }  // DataSet
          }  // SqlCommand
        } //  DB_TRX
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return new DataSet();
      }
    }     // GetWinLossEntries

    //------------------------------------------------------------------------------
    // PURPOSE: List of numbers to String
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - String of list of Numbers
    //
    public static String NumberListToString(List<Int64> Numbers)
    {
      String _str_in;

      _str_in = "";
      foreach (Int64 _number in Numbers)
      {
        _str_in += _number.ToString() + ", ";
      }
      if (_str_in.Length > 1)
      {
        _str_in = _str_in.Substring(0, _str_in.Length - 2);
      }

      return _str_in;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Cancel pending movements.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //    
    //private void CancelPendingMovements()
    //{
    //  StringBuilder _sb;

    //  _sb = new StringBuilder();
    //  _sb.AppendLine("    UPDATE   cage_movements                   ");
    //  _sb.AppendLine("       SET   cgm_status = @pStatus            ");
    //  _sb.AppendLine("     WHERE   cgm_movement_id = @pMovementId   ");
    //  _sb.AppendLine("       AND   cgm_status = @pPreviousStatus    ");

    //  try
    //  {
    //    using (DB_TRX _db_trx = new DB_TRX())
    //    {
    //      using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
    //      {
    //        _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = Cage.CageStatus.Sent;
    //        _cmd.Parameters.Add("@pPreviousStatus", SqlDbType.Int).Value = Cage.CageStatus.InProgress;
    //        _cmd.Parameters.Add("@pMovementId", SqlDbType.BigInt).Value = m_current_pending_movement;

    //        if (_cmd.ExecuteNonQuery() != 1)
    //        {
    //          Log.Error("Cashier: Movement status could not be updated. The status was changed during process. SessionId: " + Cashier.SessionId.ToString() + " TerminalId" + Cashier.TerminalId);

    //          return;
    //        }
    //      }// SqlCommand

    //      _db_trx.Commit();

    //    }// DB_TRX
    //  }
    //  catch (Exception _ex)
    //  {
    //    Log.Exception(_ex);
    //  }
    //}                                  // CancelPendingMovements

    //------------------------------------------------------------------------------
    // PURPOSE : Rebuild form.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //    
    private void RebuildForm()
    {
      Int32 _margin_between_controls;

      _margin_between_controls = 11;

      // Hide unused controls
      gp_digits_box.Text = Resource.String("STR_CAGE_INTRODUCE") + " " + Resource.String("STR_CAGE_TEXT_COLUMN_UNITS");
      web_browser.Visible = m_show_voucher;
      gb_tokens.Visible = false;
      btn_check_redeem.Visible = false;
      btn_promos_change_view.Visible = false;
      promo_filter.Visible = false;
      gb_promotion_info.Visible = false;
      gb_total.Visible = false;
      lbl_currency_to_pay.Visible = false;

      txt_amount.MaxLength = 7;

      if (m_show_voucher)
      {
        this.Width = 980;
        web_browser.Height = 478;
      }
      else
      {
        this.Width = 1024;
      }
      this.Height = 655;

      // Resize and reposition component
      gb_common.Width = 610; // 327;
      gb_common.Height = 505;
      gb_common.Location = new Point(11, 11);

      dgv_common.Style = WSI.Cashier.Controls.uc_DataGridView.DataGridViewStyle.PROMOTIONS;

      dgv_common.Width = gb_common.Width;
      dgv_common.Height = dgv_common.ColumnHeaderHeight + (dgv_common.RowTemplateHeight * 9) + 1;

      uc_payment_method1.Location = new Point(gb_common.Location.X + gb_common.Size.Width + _margin_between_controls, _margin_between_controls);

      if (uc_payment_method1.CurrencyExchange == null)
      {
        gp_digits_box.Location = new Point(gb_common.Location.X + gb_common.Size.Width + _margin_between_controls, _margin_between_controls);
        pnl_buttons.Location = new Point(gp_digits_box.Location.X, gb_common.Location.Y + gb_common.Height + _margin_between_controls);
      }
      else
      {
        gp_digits_box.Location = new Point(gb_common.Location.X + gb_common.Size.Width + 13, uc_payment_method1.Location.Y + uc_payment_method1.Height + 13);
        pnl_buttons.Location = new Point(gp_digits_box.Location.X, gp_digits_box.Location.Y + gp_digits_box.Height + _margin_between_controls);
      }

      pnl_buttons.Width = gp_digits_box.Width;
      btn_ok.Location = new Point(pnl_buttons.Width - btn_ok.Width, btn_ok.Location.Y);

      // Label Total:
      lbl_cage_total_text.Text = Resource.String("STR_VOUCHER_TOTAL_AMOUNT") + ":"; // Total:
      lbl_cage_total_text.Visible = true;
      lbl_cage_total_text.Location = new Point(10, dgv_common.Height + 10);
      lbl_cage_total_text.TextAlign = System.Drawing.ContentAlignment.MiddleRight;

      lbl_cage_total_value.Text = "";
      lbl_cage_total_value.Visible = true;
      lbl_cage_total_value.Size = new System.Drawing.Size(gb_common.Width - (lbl_cage_total_text.Width + 10), lbl_cage_total_value.Height);
      lbl_cage_total_value.Location = new System.Drawing.Point(lbl_cage_total_text.Width + 5, dgv_common.Height + 10);//(lbl_cage_total_text.Location.X + lbl_cage_total_text.Size.Width, 407);
      lbl_cage_total_value.TextAlign = System.Drawing.ContentAlignment.MiddleRight;

      lbl_cage_currency_total.Location = new System.Drawing.Point(10, lbl_cage_total_value.Location.Y + lbl_cage_total_value.Height + 10);

      // Button clear
      btn_clear.Enabled = true;
      btn_clear.Visible = true;
      btn_clear.Text = Resource.String("STR_CAGE_BTN_CLEAR");
      btn_clear.Location = new Point(gb_common.Location.X, pnl_buttons.Location.Y);

      // Button hide / show row without amount            
      btn_show_hide_rows.Text = Resource.String("STR_HIDE_ROWS_WITHOUT_AMOUNT");
      btn_show_hide_rows.Location = new Point(btn_clear.Location.X + btn_clear.Width + 10, pnl_buttons.Location.Y);

      // Button cancel request
      btn_cancel_request.Text = Resource.String("STR_UC_BANK_CANCEL_REQUEST");
      btn_cancel_request.Location = new Point(btn_clear.Location.X + dgv_common.Width - btn_cancel_request.Width, pnl_buttons.Location.Y);

    }  // RebuildForm

    //------------------------------------------------------------------------------
    // PURPOSE : Rebuild form. -> For mobile banks
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //    
    private void RebuildFormMB()
    {
      // Hide unused controls
      gp_digits_box.Text = Resource.String("STR_CAGE_INTRODUCE") + " " + Resource.String("STR_CAGE_TEXT_COLUMN_UNITS");
      web_browser.Visible = m_show_voucher;
      gb_tokens.Visible = false;
      btn_check_redeem.Visible = false;
      btn_promos_change_view.Visible = false;
      promo_filter.Visible = false;
      gb_promotion_info.Visible = false;
      //lbl_promo_parameters.Visible = false;
      //lbl_promo_input_values.Visible = false;
      //lbl_promo_reward_text.Visible = false;
      gb_total.Visible = false;


      txt_amount.MaxLength = 7;

      // Resize and reposition component
      gb_common.Width = 335; // 327;
      gb_common.Height = 478;
      gb_common.Location = new Point(8, 3);

      dgv_common.Width = 315;
      dgv_common.Height = 480; // 384;

      //form size
      this.Width = 740;
      this.Height = 615;

      gp_digits_box.Width = 380;
      gp_digits_box.Location = new Point(gb_common.Location.X + gb_common.Size.Width + 8, 4);

      // Button clear
      btn_clear.Enabled = true;
      btn_clear.Visible = true;
      btn_clear.Text = Resource.String("STR_CAGE_BTN_CLEAR");
      btn_clear.Location = new System.Drawing.Point(btn_clear.Location.X - 5, gb_common.Location.Y + gb_common.Height + 10);

      uc_payment_method1.Location = new Point(349, 4);



      // Label Total:
      lbl_cage_total_text.Text = Resource.String("STR_VOUCHER_TOTAL_AMOUNT") + ":"; // Total:
      lbl_cage_total_text.Visible = true;
      lbl_cage_total_text.Location = new Point(10, dgv_common.Height + 20);
      lbl_cage_total_text.MinimumSize = new Size(10, 56);
      lbl_cage_total_text.AutoSize = true;
      CashierStyle.Fonts.ChangeFontControl(lbl_cage_total_text, 26f, true);
      lbl_cage_total_text.TextAlign = System.Drawing.ContentAlignment.MiddleRight;

      lbl_cage_total_value.Text = "";
      lbl_cage_total_value.Visible = true;
      lbl_cage_total_value.Size = new System.Drawing.Size(230, 56);
      lbl_cage_total_value.Location = new System.Drawing.Point(lbl_cage_total_text.Width + 10, dgv_common.Height + 20);
      CashierStyle.Fonts.ChangeFontControl(lbl_cage_total_value, 26f, true);
      lbl_cage_total_value.TextAlign = System.Drawing.ContentAlignment.MiddleRight;

      lbl_cage_currency_total.Location = new System.Drawing.Point(10, lbl_cage_total_text.Location.Y + lbl_cage_total_text.Height + 20);

      // Button cancel request
      btn_cancel_request.Text = Resource.String("STR_UC_BANK_CANCEL_REQUEST");
      btn_cancel_request.Location = new System.Drawing.Point(btn_clear.Location.X + btn_clear.Size.Width + 10, btn_clear.Location.Y);



    }  // RebuildFormMB




    //------------------------------------------------------------------------------
    // PURPOSE : Reset modified properties in cage mode.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //    
    private void ResetRebuildForm()
    {
      Int32 _margin_between_controls;
      Int32 _margin_header;

      _margin_between_controls = 11;
      _margin_header = 55;

      gp_digits_box.Text = Resource.String("STR_FRM_AMOUNT_INPUT_DIGITS_GROUP_BOX");
      //this.Height = 721;
      //pnl_buttons.Location = new Point(web_browser.Location.X, this.Height - pnl_buttons.Height);
      lbl_cage_total_text.Visible = false;
      lbl_cage_total_value.Visible = false;

      btn_clear.Enabled = false;
      btn_clear.Visible = false;
      uc_payment_method1.Enabled = true;
      txt_amount.Enabled = true;

      if (m_voucher_type == VoucherTypes.StatusCash)
      {
        web_browser.Visible = true;
        gp_digits_box.Location = new Point(gp_digits_box.Location.X, web_browser.Location.Y);
        pnl_buttons.Location = new Point(web_browser.Location.X, web_browser.Location.Y + web_browser.Height + _margin_between_controls);
        pnl_buttons.Width = web_browser.Width;
        btn_ok.Location = new Point(pnl_buttons.Width - btn_ok.Width, 0);
        btn_num_dot.Enabled = false;

        this.Height = pnl_buttons.Location.Y + pnl_buttons.Height + _margin_between_controls + _margin_header;
      }

    }                                        // ResetRebuildForm

    //------------------------------------------------------------------------------
    // PURPOSE : Update denominations Grid.
    //
    //  PARAMS :
    //      - INPUT :
    //            - Decimal: NumberDecimal, bills quantity
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void UpdateDenomination(Decimal NumberDecimal)
    {
      Decimal _denomination;
      Currency _aux;
      if (dgv_common.SelectedRows.Count == 0)
      {
        frm_message.Show(Resource.String("STR_DENOMINATION_UNSELECTED"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this);

        return;
      }

      // Divisa
      _denomination = (Decimal)dgv_common.SelectedRows[0].Cells[GRID_COLUMN_CAGE_AMOUNTS_DENOMINATION].Value;

      switch ((Int32)_denomination)
      {
        case Cage.COINS_CODE:
        case Cage.CHECK_CODE:
        case Cage.BANK_CARD_CODE:
          dgv_common.SelectedRows[0].Cells[GRID_COLUMN_CAGE_AMOUNTS_UN].Value = (NumberDecimal > 0) ? 1 : 0;
          _aux = (Currency)NumberDecimal;
          _aux.CurrencyIsoCode = dgv_common.SelectedRows[0].Cells[GRID_COLUMN_CAGE_AMOUNTS_ISO_CODE].Value.ToString();

          //LTC 05-AGO-2016
          if ((m_voucher_type == VoucherTypes.FillOut && Misc.IsBankCardWithdrawalEnabled() && uc_payment_method1.CurrencyExchange.Type == CurrencyExchangeType.CARD) ||
              (m_voucher_type == VoucherTypes.CloseCash && Misc.IsBankCardCloseCashEnabled() && uc_payment_method1.CurrencyExchange.Type == CurrencyExchangeType.CARD))
          {
            dgv_common.SelectedRows[0].Cells[GRID_COLUMN_CAGE_AMOUNTS_UN].Value = Convert.ToInt32(_aux);
            dgv_common.SelectedRows[0].Cells[GRID_COLUMN_CAGE_AMOUNTS_TOTAL].Value = Convert.ToInt32(_aux);
          }
          else
          {
            dgv_common.SelectedRows[0].Cells[GRID_COLUMN_CAGE_AMOUNTS_TOTAL].Value = _aux.ToString(false);
          }

          break;

        case Cage.TICKETS_CODE:
          dgv_common.SelectedRows[0].Cells[GRID_COLUMN_CAGE_AMOUNTS_UN].Value = NumberDecimal;

          break;

        default:
          dgv_common.SelectedRows[0].Cells[GRID_COLUMN_CAGE_AMOUNTS_UN].Value = NumberDecimal;

          if ((CageCurrencyType)dgv_common.SelectedRows[0].Cells[GRID_COLUMN_CAGE_AMOUNTS_CURRENCY_TYPE_ENUM].Value == CageCurrencyType.ChipsColor)
          {
            dgv_common.SelectedRows[0].Cells[GRID_COLUMN_CAGE_AMOUNTS_TOTAL].Value = NumberDecimal;
          }
          else
          {
            dgv_common.SelectedRows[0].Cells[GRID_COLUMN_CAGE_AMOUNTS_TOTAL].Value = Currency.Format(NumberDecimal * _denomination, "");
          }

          break;
      }
    }                 // UpdateDenomination

    //------------------------------------------------------------------------------
    // PURPOSE : Update denominations Grid.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void UpdateAllDenominations()
    {
      DataTable _dt;
      String _tmp_iso_code;
      CurrencyExchange _tmp_currency_exchange;
      CageDenominationsItems.CageDenominationsItem _tmp_item;

      _dt = Cage.CageAmountsInitTable();

      for (Int32 _index = 0; _index <= dgv_common.Rows.Count - 1; ++_index)
      {
        Cage.CageAmountsPrepareRow(ref _dt, (Object)dgv_common.Rows[_index].Cells[GRID_COLUMN_CAGE_AMOUNTS_IMAGE].Value,
                                            (Object)dgv_common.Rows[_index].Cells[GRID_COLUMN_CAGE_AMOUNTS_COLOR].Value,
                                            (Object)dgv_common.Rows[_index].Cells[GRID_COLUMN_CAGE_AMOUNTS_ISO_CODE].Value,
                                            (Object)dgv_common.Rows[_index].Cells[GRID_COLUMN_CAGE_AMOUNTS_DENOMINATION].Value,
                                            (Object)dgv_common.Rows[_index].Cells[GRID_COLUMN_CAGE_AMOUNTS_ALLOWED].Value,
                                            (Object)dgv_common.Rows[_index].Cells[GRID_COLUMN_CAGE_AMOUNTS_DENOMINATION_SHOWED].Value,
                                            (Object)dgv_common.Rows[_index].Cells[GRID_COLUMN_CAGE_AMOUNTS_CURRENCY_TYPE].Value,
                                            (Object)dgv_common.Rows[_index].Cells[GRID_COLUMN_CAGE_AMOUNTS_CURRENCY_TYPE_ENUM].Value,
                                            (Object)dgv_common.Rows[_index].Cells[GRID_COLUMN_CAGE_AMOUNTS_CHIP_NAME].Value,
                                            (Object)dgv_common.Rows[_index].Cells[GRID_COLUMN_CAGE_AMOUNTS_CHIP_DRAW].Value,
                                            (Object)DBNull.Value,
                                            (Object)DBNull.Value,
                                            (Object)dgv_common.Rows[_index].Cells[GRID_COLUMN_CAGE_AMOUNTS_CHIP_ID].Value,
                                            (Object)dgv_common.Rows[_index].Cells[GRID_COLUMN_CAGE_AMOUNTS_CHIP_SET_ID].Value);
      }

      _tmp_currency_exchange = (uc_payment_method1.CurrencyExchange == null) ? m_national_currency : uc_payment_method1.CurrencyExchange;

      switch (_tmp_currency_exchange.Type)
      {
        case CurrencyExchangeType.CURRENCY:
        default:
          _tmp_iso_code = _tmp_currency_exchange.CurrencyCode;
          break;

        case CurrencyExchangeType.CARD:
          _tmp_iso_code = _tmp_currency_exchange.CurrencyCode + Cage.BANK_CARD_CODE;
          break;

        case CurrencyExchangeType.CHECK:
          _tmp_iso_code = _tmp_currency_exchange.CurrencyCode + Cage.CHECK_CODE;
          break;

      }

      _tmp_item = new CageDenominationsItems.CageDenominationsItem();
      _tmp_item.ItemAmounts = _dt;

      _tmp_item.TotalAmount = GetCurrentBalance(_tmp_iso_code, _tmp_currency_exchange.Type);
      _tmp_item.TotalTips = m_cage_amounts[_tmp_iso_code, _tmp_currency_exchange.Type].TotalTips;
      _tmp_item.AllowedFillOut = true;
      _tmp_item.HasTicketsTito = false;
      m_cage_amounts[_tmp_iso_code, _tmp_currency_exchange.Type] = _tmp_item;
      dgv_common.DataSource = m_cage_amounts[_tmp_iso_code, _tmp_currency_exchange.Type].ItemAmounts;
      UpdateTotalCageAmount();

    }                                  // UpdateAllDenominations

    //------------------------------------------------------------------------------
    // PURPOSE : Update  amount total cage to each currency.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void UpdateTotalCageAmount()
    {
      Decimal _tmp_total;
      Int32 _tmp_units;
      Decimal _tmp_tips;
      Decimal _tmp_current;
      Int32 _tmp_current_units;
      Decimal _tmp_denomination;
      String _tmp_iso_code;
      CageDenominationsItems.CageDenominationsItem _tmp_item;
      CurrencyExchange _tmp_currency_exchange;
      Boolean _has_tickets_tito;

      Cage.ShowDenominationsMode _show_denominations_mode;
      Boolean _show_amounts_image;
      Boolean _show_denomination_showed;
      Boolean _show_currency_type_showed;
      Boolean _show_amounts_un;
      Int32 _width_amounts_image;
      Int32 _width_amounts_un;
      //Int32 _width_denomination_showed;
      Int32 _width_currency_type;
      Int32 _width_cage_amounts_total;

      _show_denominations_mode = (Cage.ShowDenominationsMode)GeneralParam.GetInt32("Cage", "ShowDenominations", 1);
      _width_amounts_image = WIDTH_COLUMN_CAGE_AMOUNTS_COLOR;
      //_width_denomination_showed = WIDTH_COLUMN_CAGE_AMOUNTS_DENOMINATION_SHOWED;
      _width_currency_type = WIDTH_COLUMN_CAGE_AMOUNTS_DENOMINATION_CURRENCY_TYPE;
      _width_amounts_un = WIDTH_COLUMN_CAGE_AMOUNTS_UN;
      _width_cage_amounts_total = WIDTH_COLUMN_CAGE_AMOUNTS_TOTAL;
      _show_amounts_image = true;
      _show_denomination_showed = true;
      _show_currency_type_showed = true;
      _show_amounts_un = true;

      _tmp_currency_exchange = (uc_payment_method1.CurrencyExchange == null) ? m_national_currency : uc_payment_method1.CurrencyExchange;
      _has_tickets_tito = false;
      _tmp_iso_code = _tmp_currency_exchange.CurrencyCode;

      switch (_tmp_currency_exchange.Type)
      {
        case CurrencyExchangeType.CURRENCY:
        default:
          _tmp_iso_code = _tmp_currency_exchange.CurrencyCode;

          break;

        case CurrencyExchangeType.CHECK:
          _tmp_iso_code = _tmp_currency_exchange.CurrencyCode + Cage.CHECK_CODE;

          break;

        case CurrencyExchangeType.CARD:
          _tmp_iso_code = _tmp_currency_exchange.CurrencyCode + Cage.BANK_CARD_CODE;
          break;
      }

      _tmp_total = 0;
      _tmp_units = 0;
      _tmp_tips = 0;

      if (m_voucher_type == VoucherTypes.WinLoss && m_win_loss_mode == GamingTableBusinessLogic.GT_WIN_LOSS_MODE.BY_TOTALS)
      {
        _show_denominations_mode = Cage.ShowDenominationsMode.HideAllDenominations;
      }

      if (dgv_common.RowCount > 0)
      {
        if (FeatureChips.IsChipsType((CurrencyExchangeType)dgv_common.Rows[0].Cells[GRID_COLUMN_CAGE_AMOUNTS_CURRENCY_TYPE_ENUM].Value))
        {
          switch (_show_denominations_mode)
          {
            case Cage.ShowDenominationsMode.HideAllDenominations:
              _width_amounts_image = WIDTH_COLUMN_CAGE_AMOUNTS_IMAGE;
              //_width_denomination_showed = WIDTH_COLUMN_CAGE_AMOUNTS_NOT_SHOWED;
              _width_amounts_un = WIDTH_COLUMN_CAGE_AMOUNTS_NOT_SHOWED;
              _width_cage_amounts_total = WIDTH_COLUMN_CAGE_AMOUNTS_TOTAL + WIDTH_COLUMN_CAGE_AMOUNTS_DENOMINATION_SHOWED + WIDTH_COLUMN_CAGE_AMOUNTS_UN - WIDTH_COLUMN_CAGE_AMOUNTS_IMAGE;

              _show_amounts_image = true;
              _show_denomination_showed = false;
              _show_amounts_un = false;

              break;

            case Cage.ShowDenominationsMode.ShowAllDenominations:
            case Cage.ShowDenominationsMode.ShowOnlyChipsDenominations:
              _width_amounts_image = WIDTH_COLUMN_CAGE_AMOUNTS_IMAGE;
              //_width_denomination_showed = WIDTH_COLUMN_CAGE_AMOUNTS_DENOMINATION_SHOWED - WIDTH_COLUMN_CAGE_AMOUNTS_IMAGE + WIDTH_COLUMN_CAGE_AMOUNTS_DENOMINATION_CURRENCY_TYPE;
              _width_currency_type = WIDTH_COLUMN_CAGE_AMOUNTS_NOT_SHOWED;
              _width_amounts_un = WIDTH_COLUMN_CAGE_AMOUNTS_UN;
              _width_cage_amounts_total = WIDTH_COLUMN_CAGE_AMOUNTS_TOTAL;

              _show_amounts_image = true;
              _show_denomination_showed = true;
              _show_amounts_un = true;
              _show_currency_type_showed = false;
              break;
          }

        }
        else
        {
          switch (_show_denominations_mode)
          {
            case Cage.ShowDenominationsMode.HideAllDenominations:
            case Cage.ShowDenominationsMode.ShowOnlyChipsDenominations:
              _width_amounts_image = WIDTH_COLUMN_CAGE_AMOUNTS_NOT_SHOWED;
              //_width_denomination_showed = WIDTH_COLUMN_CAGE_AMOUNTS_NOT_SHOWED;
              _width_amounts_un = WIDTH_COLUMN_CAGE_AMOUNTS_NOT_SHOWED;
              _width_cage_amounts_total = WIDTH_COLUMN_CAGE_AMOUNTS_CHIP_NAME + WIDTH_COLUMN_CAGE_AMOUNTS_DRAWING + WIDTH_COLUMN_CAGE_AMOUNTS_TOTAL + WIDTH_COLUMN_CAGE_AMOUNTS_DENOMINATION_SHOWED + WIDTH_COLUMN_CAGE_AMOUNTS_UN;

              _show_amounts_image = false;
              _show_denomination_showed = false;
              _show_amounts_un = false;

              break;

            case Cage.ShowDenominationsMode.ShowAllDenominations:
              _width_amounts_image = WIDTH_COLUMN_CAGE_AMOUNTS_NOT_SHOWED;
              //_width_denomination_showed = WIDTH_COLUMN_CAGE_AMOUNTS_DENOMINATION_SHOWED;
              _width_amounts_un = WIDTH_COLUMN_CAGE_AMOUNTS_UN;
              _width_cage_amounts_total = WIDTH_COLUMN_CAGE_AMOUNTS_TOTAL;

              _show_amounts_image = false;
              _show_amounts_un = true;
              _show_denomination_showed = true;

              break;
          }
          _show_amounts_image = false;

        }

        dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_COLOR].Width = WIDTH_COLUMN_CAGE_AMOUNTS_NOT_SHOWED;
        dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_COLOR].Visible = false;

        dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_IMAGE].Width = _width_amounts_image;
        dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_IMAGE].Visible = _show_amounts_image;
        dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_IMAGE].DefaultCellStyle.NullValue = null;

        if (_tmp_currency_exchange.Type != CurrencyExchangeType.CASINO_CHIP_COLOR)
        {
          dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_DENOMINATION_SHOWED].Visible = _show_denomination_showed;
        }
        else
        {
          dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_DENOMINATION_SHOWED].Visible = false;
        }

        dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_CURRENCY_TYPE].Width = _width_currency_type;
        dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_CURRENCY_TYPE].Visible = _show_currency_type_showed;

        dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_UN].Width = _width_amounts_un;
        dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_UN].Visible = _show_amounts_un;

        dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_TOTAL].Width = _width_cage_amounts_total;

      }

      for (Int16 _count = 0; _count <= dgv_common.RowCount - 1; ++_count)
      {
        Decimal.TryParse(dgv_common.Rows[_count].Cells[GRID_COLUMN_CAGE_AMOUNTS_DENOMINATION].Value.ToString(), out _tmp_denomination);
        if (_tmp_denomination != Cage.TICKETS_CODE)
        {
          Decimal.TryParse(dgv_common.Rows[_count].Cells[GRID_COLUMN_CAGE_AMOUNTS_TOTAL].Value.ToString(), out _tmp_current);
          Int32.TryParse(dgv_common.Rows[_count].Cells[GRID_COLUMN_CAGE_AMOUNTS_UN].Value.ToString(), out _tmp_current_units);

          if (_tmp_denomination == Cage.COINS_CODE && FeatureChips.IsChipsType((CurrencyExchangeType)dgv_common.Rows[0].Cells[GRID_COLUMN_CAGE_AMOUNTS_CURRENCY_TYPE_ENUM].Value))
          {
            _tmp_tips += _tmp_current;
          }
          else
          {
            _tmp_total += _tmp_current;
            _tmp_units += _tmp_current_units;
          }
        }
        else
        {
          Decimal.TryParse(dgv_common.Rows[_count].Cells[GRID_COLUMN_CAGE_AMOUNTS_UN].Value.ToString(), out _tmp_current);
          _has_tickets_tito = (_tmp_current > 0);
        }

        if (_tmp_denomination != Cage.CHECK_CODE && _tmp_denomination != Cage.BANK_CARD_CODE)
        {
          if (FeatureChips.IsChipsType((CurrencyExchangeType)dgv_common.Rows[_count].Cells[GRID_COLUMN_CAGE_AMOUNTS_CURRENCY_TYPE_ENUM].Value))
          {
            SetChipsColor(_count);
          }
        }
      }

      _tmp_item = new CageDenominationsItems.CageDenominationsItem();
      _tmp_item.ItemAmounts = m_cage_amounts[_tmp_iso_code, _tmp_currency_exchange.Type].ItemAmounts;
      _tmp_item.TotalAmount = _tmp_total;
      _tmp_item.TotalUnits = _tmp_units;
      _tmp_item.TotalTips = _tmp_tips;
      _tmp_item.AllowedFillOut = m_cage_amounts[_tmp_iso_code, _tmp_currency_exchange.Type].AllowedFillOut;
      _tmp_item.HasTicketsTito = _has_tickets_tito;
      m_cage_amounts[_tmp_iso_code, _tmp_currency_exchange.Type] = _tmp_item;

      if (_tmp_currency_exchange.Type == CurrencyExchangeType.CASINO_CHIP_COLOR)
      {
        set_cage_total_value = _tmp_units;
      }
      else
      {
        set_cage_total_value = _tmp_total;
      }

    }                                   // UpdateTotalCageAmount

    //------------------------------------------------------------------------------
    // PURPOSE : Set cell color.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void SetChipsColor(Int16 RowIndex)
    {
      Decimal _denomination;

      _denomination = (Decimal)dgv_common.Rows[RowIndex].Cells[GRID_COLUMN_CAGE_AMOUNTS_DENOMINATION].Value;

      if ((Int32)_denomination != Cage.COINS_CODE)
      {
        Int32 _current_color;
        Int32.TryParse(dgv_common.Rows[RowIndex].Cells[GRID_COLUMN_CAGE_AMOUNTS_COLOR].Value.ToString(), out _current_color);
        dgv_common.Rows[RowIndex].Cells[GRID_COLUMN_CAGE_AMOUNTS_COLOR].Style.BackColor = System.Drawing.Color.FromArgb(_current_color);
        dgv_common.Rows[RowIndex].Cells[GRID_COLUMN_CAGE_AMOUNTS_COLOR].Style.ForeColor = System.Drawing.Color.FromArgb(_current_color);
        dgv_common.Rows[RowIndex].Cells[GRID_COLUMN_CAGE_AMOUNTS_COLOR].Style.SelectionBackColor = System.Drawing.Color.FromArgb(_current_color);
        dgv_common.Rows[RowIndex].Cells[GRID_COLUMN_CAGE_AMOUNTS_COLOR].Style.SelectionForeColor = System.Drawing.Color.FromArgb(_current_color);

        dgv_common.Rows[RowIndex].Cells[GRID_COLUMN_CAGE_AMOUNTS_IMAGE].Style.BackColor = System.Drawing.Color.FromArgb(_current_color);
        dgv_common.Rows[RowIndex].Cells[GRID_COLUMN_CAGE_AMOUNTS_IMAGE].Style.SelectionBackColor = System.Drawing.Color.FromArgb(_current_color);

        //dgv_common.Rows[RowIndex].Cells[GRID_COLUMN_CAGE_AMOUNTS_COLOR].Value = Resource.Image("chip");
      }
      else
      {
        dgv_common.Rows[RowIndex].Cells[GRID_COLUMN_CAGE_AMOUNTS_COLOR].Style.BackColor = Color.White;
        dgv_common.Rows[RowIndex].Cells[GRID_COLUMN_CAGE_AMOUNTS_COLOR].Style.ForeColor = Color.White;
        dgv_common.Rows[RowIndex].Cells[GRID_COLUMN_CAGE_AMOUNTS_COLOR].Style.SelectionBackColor = Color.LightBlue;
        dgv_common.Rows[RowIndex].Cells[GRID_COLUMN_CAGE_AMOUNTS_COLOR].Style.SelectionForeColor = Color.LightBlue;

        dgv_common.Rows[RowIndex].Cells[GRID_COLUMN_CAGE_AMOUNTS_IMAGE].Value = null;

        dgv_common.Rows[RowIndex].DefaultCellStyle.BackColor = Color.LightBlue;
      }
    } // SetChipsColor

    //------------------------------------------------------------------------------
    // PURPOSE : Allowed money entries.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void AllowedMoneyEntries(Boolean AllowedEntries)
    {
      // All buttons less enter button.
      txt_amount.Enabled = AllowedEntries;
      btn_money_1.Enabled = AllowedEntries;
      btn_money_2.Enabled = AllowedEntries;
      btn_money_3.Enabled = AllowedEntries;
      btn_money_4.Enabled = AllowedEntries;
      btn_num_1.Enabled = AllowedEntries;
      btn_num_2.Enabled = AllowedEntries;
      btn_num_3.Enabled = AllowedEntries;
      btn_num_4.Enabled = AllowedEntries;
      btn_num_5.Enabled = AllowedEntries;
      btn_num_6.Enabled = AllowedEntries;
      btn_num_7.Enabled = AllowedEntries;
      btn_num_8.Enabled = AllowedEntries;
      btn_num_9.Enabled = AllowedEntries;
      btn_num_10.Enabled = AllowedEntries;
      btn_num_dot.Enabled = AllowedEntries;
      btn_back.Enabled = AllowedEntries;
      uc_payment_method1.Enabled = AllowedEntries;
      gb_tokens.Enabled = AllowedEntries;
      gb_common.Enabled = AllowedEntries;
      promo_filter.Enabled = AllowedEntries;
      btn_check_redeem.Enabled = AllowedEntries;
      btn_clear.Enabled = AllowedEntries;

      if (m_voucher_type == VoucherTypes.FillIn)
      {
        btn_intro.Enabled = AllowedEntries;
      }
      if (m_voucher_type == VoucherTypes.AmountRequest && m_request_movement_id != -1)
      {
        btn_ok.Enabled = false;
        btn_intro.Enabled = false;
        uc_payment_method1.Enabled = true;
        btn_cancel_request.Enabled = true;
        btn_cancel_request.Visible = true;
        gb_common.Enabled = true;
      }
      else
      {
        btn_cancel_request.Enabled = false;
        btn_cancel_request.Visible = false;
      }
      m_allowed_entries = AllowedEntries;

      if (AllowedEntries && m_dt_cage_pending_movements != null && m_dt_cage_pending_movements.Rows.Count > 0)
      {
        txt_amount.Text = "";
        m_current_pending_movement = Convert.ToInt64(m_dt_cage_pending_movements.Rows[0][0]);
      }
      else // Lock controls to open cashier with zero.
      {
        if (m_voucher_type == VoucherTypes.OpenCash)
        {
          btn_intro.Enabled = true;
          txt_amount.Text = "0";
        }
        m_current_pending_movement = 0;
      }

    }               // AllowedMoneyEntries

    //------------------------------------------------------------------------------
    // PURPOSE : Allow controls to insert values of specific currency.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void AllowedCurrency(Boolean AllowedEntries)
    {
      if (m_voucher_type != VoucherTypes.AmountRequest || m_request_movement_id == -1)
      {
        gp_digits_box.Enabled = AllowedEntries;
        gb_common.Enabled = AllowedEntries;
        btn_clear.Enabled = AllowedEntries;
      }
      else
      {
        gp_digits_box.Enabled = false;
        gb_common.Enabled = false;
        btn_clear.Enabled = false;
        gb_common.Enabled = true;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Compare cage movements and set "CageStatus".
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private Boolean AllowedEntriesStatus()
    {
      // Cage.CageStatus.OK                             = 2 --> All OK
      // Cage.CageStatus.OkDenominationImbalance        = 3 --> Total OK Denomination KO
      // Cage.CageStatus.CanceledDueToImbalanceOrOther  = 6 --> All KO

      // m_dt_cage_pendint_movements --> datatable with pending movements
      // m_cage_amounts              --> dictionary with user values
      Boolean _match_found;
      Decimal _total_DB;
      Boolean _msg_showed;

      String _grid_isocode;
      Decimal _grid_denomination;
      Decimal _grid_units;
      Decimal _grid_total;
      CageCurrencyType _grid_cage_cur_type;
      Int64 _grid_chip_id;

      String _pending_isocode;
      Decimal _pending_denomination;
      Decimal _pending_units;
      CageCurrencyType _pending_cage_cur_type;
      Int64 _pending_chip_id;

      int _aux_currency;

      _match_found = false;
      _total_DB = 0;
      _msg_showed = false;
      _grid_isocode = "";

      m_cage_movement_status = Cage.CageStatus.Sent;
      foreach (KeyValuePair<CageDenominationsItems.CageDenominationKey, CageDenominationsItems.CageDenominationsItem> _result in m_cage_amounts)
      {
        foreach (DataRow _grid_row in _result.Value.ItemAmounts.Rows)
        {
          _grid_isocode = _grid_row[SQL_COLUMN_CAGE_AMOUNTS_CAA_ISO_CODE].ToString();
          _grid_cage_cur_type = (CageCurrencyType)_grid_row[SQL_COLUMN_CAGE_AMOUNTS_CAA_CAGE_CURRENCY_TYPE];

          Decimal.TryParse(_grid_row[SQL_COLUMN_CAGE_AMOUNTS_CAA_DENOMINATION].ToString(), out _grid_denomination);
          Decimal.TryParse(_grid_row[SQL_COLUMN_CAGE_AMOUNTS_CAA_UN].ToString(), out _grid_units);
          Decimal.TryParse(_grid_row[SQL_COLUMN_CAGE_AMOUNTS_CAA_TOTAL].ToString(), out _grid_total);

          foreach (DataRow _pending_row in m_dt_cage_pending_movements.Rows)
          {
            _pending_isocode = _pending_row[SQL_COLUMN_CMD_ISO_CODE].ToString();
            _pending_cage_cur_type = String.IsNullOrEmpty(_pending_row["CMD_CAGE_CURRENCY_TYPE"].ToString()) ? CageCurrencyType.Bill : (CageCurrencyType)_pending_row["CMD_CAGE_CURRENCY_TYPE"];

            Decimal.TryParse(_pending_row[SQL_COLUMN_CMD_DENOMINATION].ToString(), out _pending_denomination);
            Decimal.TryParse(_pending_row[SQL_COLUMN_CMD_QUANTITY].ToString(), out _pending_units);

            if (_grid_isocode == _pending_isocode)
            {
              if (_grid_cage_cur_type == _pending_cage_cur_type && FeatureChips.IsCageCurrencyTypeChips(_grid_cage_cur_type))
              {
                Int64.TryParse(_grid_row["CHIP_ID"].ToString(), out _grid_chip_id);
                Int64.TryParse(_pending_row["CMD_CHIP_ID"].ToString(), out _pending_chip_id);

                if (_grid_cage_cur_type == CageCurrencyType.ChipsColor)
                {
                  _total_DB += _pending_units;
                }
                else
                {
                  _total_DB += (_pending_units * _pending_denomination);
                }

                if (_grid_chip_id == _pending_chip_id && _grid_units.Equals(_pending_units))
                {
                  _match_found = true;
                }
              }
              else if (!FeatureChips.IsCageCurrencyTypeChips(_grid_cage_cur_type) && !FeatureChips.IsCageCurrencyTypeChips(_pending_cage_cur_type))
              {
                // Units, to Cage.COINS_CODE is Coins
                Decimal _p_units;
                _p_units = (_pending_units == Cage.COINS_CODE) ? 1 : _pending_units;

                // Denomination, to coins is amount.
                // Units * denomination; In coins only denomination.
                _total_DB += (_p_units * _pending_denomination);

                if (_grid_denomination == Cage.COINS_CODE)
                {
                  if (_grid_total == _pending_denomination &&
                      _grid_denomination == _pending_units)
                  {
                    _match_found = true;
                  }
                }
                else
                {

                  if ((string)_grid_row[SQL_COLUMN_CAGE_AMOUNTS_CAA_TYPE] == Resource.String("STR_VOUCHER_CARD_HANDPAY_SESSION_CURRENCY_TYPE_BILL"))
                    _aux_currency = (int)CageCurrencyType.Bill;
                  else
                    _aux_currency = (int)CageCurrencyType.Coin;


                  if (_grid_denomination == _pending_denomination &&
                    Decimal.Parse(_grid_row[SQL_COLUMN_CAGE_AMOUNTS_CAA_UN] == DBNull.Value ? "0" : _grid_row[SQL_COLUMN_CAGE_AMOUNTS_CAA_UN].ToString()).Equals(Decimal.Parse(_pending_row[SQL_COLUMN_CMD_QUANTITY].ToString())) &&
                        (
                        (!(_pending_row["CMD_CAGE_CURRENCY_TYPE"] is System.DBNull) && _aux_currency == Convert.ToInt32(_pending_row["CMD_CAGE_CURRENCY_TYPE"]))
                        || _result.Key.ISOCode == Cage.CHIPS_ISO_CODE
                        )
                     ) //if
                  {
                    _match_found = true;


                  } //if
                }//if
              }//if
            }//if
          }//foreach

          if (_total_DB != GetCurrentBalance(_result.Key.ISOCode, _result.Key.Type))
          {
            m_cage_movement_status = Cage.CageStatus.ErrorTotalImbalance;
            return false;
          }//if

          if (!_match_found)
          {
            //Decimal.TryParse(_grid_denomination, out _tmp_decimal);
            if (_grid_denomination == Cage.COINS_CODE && _grid_total > 0)
            {
              m_cage_movement_status = Cage.CageStatus.OkDenominationImbalance;
            }
            else if (_grid_units > 0)
            {
              m_cage_movement_status = Cage.CageStatus.OkDenominationImbalance;
            }
          }//if

          _total_DB = 0;
          _match_found = false;
        }//foreach

        if (m_cage_movement_status == Cage.CageStatus.OkDenominationImbalance)
        {
          if (!m_cage_allow_denomination_in_balance)
          {
            return false;
          }
          else
          {
            // TODO: Yes/NO; �Desea validar los importes introducidos?
            if (!_msg_showed)
            {
              DialogResult _dlg_rc;

              _dlg_rc = frm_message.Show(Resource.String("STR_CAGE_VALIDATE_DENOMINATIONS_MOVEMENT").Replace("\\r\\n", "\r\n"),
                                         Resource.String("STR_CAGE_MOVEMENTS"),
                                         MessageBoxButtons.YesNo,
                                         MessageBoxIcon.Warning, this);

              if (_dlg_rc != DialogResult.OK)
              {
                // NO  ==>  NO HACER NADA, COMO HASTA AHORA
                m_cage_movement_status = Cage.CageStatus.UserCancelToConfirmAmounts;
                return false;
              }
              else
              {
                _msg_showed = true;
              }
            }
          }
        }
        else
        {
          m_cage_movement_status = Cage.CageStatus.OK;
        }
      }

      return true;
    }                                 // AllowedEntriesStatus

    //------------------------------------------------------------------------------
    // PURPOSE : When selected row is different to "Coins", the bottom dot is disabled.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void dgv_common_SelectionChanged(object sender, EventArgs e)
    {
      Decimal _tmp_decimal;

      if (m_cage_enabled && m_voucher_type != VoucherTypes.OpenCash)
      {

        if (dgv_common.SelectedRows.Count > 0)
        {
          Decimal.TryParse(dgv_common.SelectedRows[0].Cells[GRID_COLUMN_CAGE_AMOUNTS_DENOMINATION].Value.ToString(), out _tmp_decimal);

          switch ((Int32)_tmp_decimal)
          {
            case Cage.COINS_CODE:
            case Cage.BANK_CARD_CODE:
            case Cage.CHECK_CODE:
              txt_amount.Text = dgv_common.SelectedRows[0].Cells[GRID_COLUMN_CAGE_AMOUNTS_TOTAL].Value.ToString().Trim();
              if (uc_payment_method1.CurrencyExchange != null)
              {
                btn_num_dot.Enabled = Format.GetFormattedDecimals(uc_payment_method1.CurrencyExchange.CurrencyCode) != 0;// true;
              }
              else
              {
                btn_num_dot.Enabled = Format.GetFormattedDecimals("") != 0;
              }
              break;

            case Cage.TICKETS_CODE:
              txt_amount.Text = dgv_common.SelectedRows[0].Cells[GRID_COLUMN_CAGE_AMOUNTS_UN].Value.ToString().Trim();
              btn_num_dot.Enabled = false;
              break;

            default:
              txt_amount.Text = dgv_common.SelectedRows[0].Cells[GRID_COLUMN_CAGE_AMOUNTS_UN].Value.ToString().Trim();
              if (m_voucher_type != VoucherTypes.WinLoss)
              {
                btn_num_dot.Enabled = false;
              }
              else
              {
                if (m_win_loss_mode == GamingTableBusinessLogic.GT_WIN_LOSS_MODE.BY_TOTALS)
                {
                  txt_amount.Text = dgv_common.SelectedRows[0].Cells[GRID_COLUMN_CAGE_AMOUNTS_TOTAL].Value.ToString().Trim();
                  if (uc_payment_method1.CurrencyExchange != null)
                  {
                    btn_num_dot.Enabled = Format.GetFormattedDecimals(uc_payment_method1.CurrencyExchange.CurrencyCode) != 0;// true;
                  }
                }
                else
                {
                  btn_num_dot.Enabled = false;

                }
              }

              break;
          }
        }
      }

      if (dgv_common.SelectedRows.Count > 0)
      {
        setImages();
      }

    }   // dgv_common_SelectionChanged

    //------------------------------------------------------------------------------
    // PURPOSE : After clicking on enter button, row selected is changed.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void set_dgv_common_next_row()
    {
      Int32 _index;
      Int32 _index_visible_force;
      String _currency_isocode;
      Decimal _currency_denomination;
      String _currency_denomination_showed;
      DataTable _dt_currency;
      DataRow _dr_currency;
      Boolean _add_row;

      _index = dgv_common.SelectedRows[0].Index;
      _currency_isocode = dgv_common.Rows[_index].Cells[GRID_COLUMN_CAGE_AMOUNTS_ISO_CODE].Value.ToString();
      _currency_denomination = (Decimal)dgv_common.Rows[_index].Cells[GRID_COLUMN_CAGE_AMOUNTS_DENOMINATION].Value;
      _currency_denomination_showed = "";

      dgv_common.Rows[_index].Selected = false;
      if (_index == dgv_common.Rows.Count - 1)
      {
        switch ((Int32)_currency_denomination)
        {
          case Cage.CHECK_CODE:
            _currency_denomination_showed = Resource.String("STR_CAGE_CHECK", dgv_common.Rows.Count + 1);

            if (m_voucher_type == VoucherTypes.FillOut && m_closing_stock != null && m_closing_stock.CurrenciesStocksCredits != null)
            {
              _add_row = false;
            }
            else
            {
              _add_row = true;
            }
            break;

          case Cage.BANK_CARD_CODE:
            _currency_denomination_showed = Resource.String("STR_CAGE_BANKCARD", dgv_common.Rows.Count + 1);

            //LTC 05-AGO-2016
            if ((m_voucher_type == VoucherTypes.FillOut && Misc.IsBankCardWithdrawalEnabled() && uc_payment_method1.CurrencyExchange.Type == CurrencyExchangeType.CARD) ||
                (m_voucher_type == VoucherTypes.CloseCash && Misc.IsBankCardCloseCashEnabled() && uc_payment_method1.CurrencyExchange.Type == CurrencyExchangeType.CARD))
            {
              _add_row = false;
            }
            else
            {
              _add_row = true;
            }

            break;

          default:
            _add_row = false;
            break;
        }

        if (_add_row)
        {
          _dt_currency = new DataTable();
          _dt_currency = (DataTable)dgv_common.DataSource;
          _dr_currency = _dt_currency.NewRow();
          _dr_currency[GRID_COLUMN_CAGE_AMOUNTS_ISO_CODE] = _currency_isocode;
          _dr_currency[GRID_COLUMN_CAGE_AMOUNTS_DENOMINATION] = _currency_denomination;
          _dr_currency[GRID_COLUMN_CAGE_AMOUNTS_CURRENCY_TYPE_ENUM] = _currency_denomination;
          _dr_currency[GRID_COLUMN_CAGE_AMOUNTS_ALLOWED] = true;
          _dr_currency[GRID_COLUMN_CAGE_AMOUNTS_DENOMINATION_SHOWED] = _currency_denomination_showed;

          _dt_currency.Rows.Add(_dr_currency);
        }
        else
        {
          dgv_common.Rows[0].Selected = true;
          dgv_common.FirstDisplayedScrollingRowIndex = 0;
          return;
        }
      }

      _index_visible_force = CalculateIndexForceVisible(++_index);

      dgv_common.Rows[_index].Selected = true;

      if (!(_index_visible_force == _index))
      {
        dgv_common.FirstDisplayedScrollingRowIndex = _index_visible_force;
      }

    }                                 // set_dgv_common_next_row

    private Int32 CalculateIndexForceVisible(Int32 CurrentIndex)
    {
      Int32 _index;
      Int32 _max_index_visible;

      _index = CurrentIndex;
      _max_index_visible = 8;

      if (CurrentIndex > _max_index_visible)
      {
        _index = CurrentIndex - _max_index_visible;
      }

      return _index;
    }           // CalculateIndexVisibleForce

    //------------------------------------------------------------------------------
    // PURPOSE : Clear the currency's selected grid.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void btn_clear_Click(object sender, EventArgs e)
    {
      m_from_clear = true;
      UpdateAllDenominations();
      btn_intro_Click(sender, e);
      txt_amount.Focus();
      m_from_clear = false;
      lbl_currency.Text = String.Empty;

      if (!m_show_rows_without_amount)
      {
        m_show_rows_without_amount = true;
        HideShowRowsWithoutAmount(m_show_rows_without_amount);
      }
    }               // btn_clear_Click 

    //------------------------------------------------------------------------------
    // PURPOSE : Return the total for the Iso Code given.
    //
    //  PARAMS :
    //      - INPUT :
    //            - String: IsoCode
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //         Decimal: total.
    //
    //   NOTES :
    //
    private Decimal GetCurrentBalance(String IsoCode, CageCurrencyType Type)
    {
      if (Type == CageCurrencyType.ChipsColor)
      {
        return (m_cage_amounts.ContainsKey(IsoCode, Type)) ? m_cage_amounts[IsoCode, Type].TotalUnits : 0;
      }

      return (m_cage_amounts.ContainsKey(IsoCode, Type)) ? m_cage_amounts[IsoCode, Type].TotalAmount : 0;
    }

    private Decimal GetCurrentBalance(String IsoCode, CurrencyExchangeType Type)
    {
      return GetCurrentBalance(IsoCode, FeatureChips.ConvertToCageCurrencyType(Type));
    }                      // GetCurrentBalance

    //------------------------------------------------------------------------------
    // PURPOSE : Return the total for the Iso Code given.
    //
    //  PARAMS :
    //      - INPUT :
    //            - String: IsoCode
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //         Boolean: total.
    //
    //   NOTES :
    //
    private Boolean GetCurrentAllowedFillOut(String IsoCode, CageCurrencyType Type)
    {
      return (m_cage_amounts.ContainsKey(IsoCode, Type)) ? m_cage_amounts[IsoCode, Type].AllowedFillOut : false;
    }               // GetCurrentAllowedFillOut

    //------------------------------------------------------------------------------
    // PURPOSE : Return if introduced amount chips and stock chips is ok
    //
    //  PARAMS :
    //      - INPUT :
    //            - SqlTransaction Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //         Boolean: ok or not.
    //
    //   NOTES :
    //
    private Boolean StockControl(SqlTransaction Trx)
    {
      Int64 _chip_id;
      Int32 _units;
      Boolean _chips_stock_control;
      Boolean _exists_concepts_movements;
      CageDenominationsItems.CageDenominationsItem _cage_item;
      object _total_units;
      Cage.ShowDenominationsMode _show_denominations_mode;

      _chips_stock_control = FeatureChips.IsChipsStockControlEnabled;
      _show_denominations_mode = (Cage.ShowDenominationsMode)GeneralParam.GetInt32("Cage", "ShowDenominations", 1);
      if (m_voucher_type == VoucherTypes.WinLoss && m_win_loss_mode == GamingTableBusinessLogic.GT_WIN_LOSS_MODE.BY_TOTALS)
      {
        _show_denominations_mode = Cage.ShowDenominationsMode.HideAllDenominations;
      }

      if (IsGamingTable ||
          !_chips_stock_control ||
          (_chips_stock_control && _show_denominations_mode == Cage.ShowDenominationsMode.HideAllDenominations))
      {
        return true;
      }

      foreach (CurrencyExchange _currency in m_currencies)
      {
        // Validate only chips stock
        if (!FeatureChips.IsChipsType(_currency.Type))
        {
          continue;
        }

        // Validate the key is in the cage_amount (values introduced on close/fill out by user)
        if (!m_cage_amounts.ContainsKey(_currency.CurrencyCode, _currency.Type))
        {
          continue;
        }

        // Read stock
        if (!FeatureChips.Chips.ReadStock(CashierSessionInfo.CashierSessionId, Trx))
        {

          return false;
        }

        _cage_item = m_cage_amounts.GetValue(_currency.CurrencyCode, FeatureChips.ConvertToCageCurrencyType(_currency.Type));


        // if the total is 0, avoid to validate the stock
        _total_units = _cage_item.ItemAmounts.Compute("SUM(UN)", "");
        if (_total_units == DBNull.Value || Decimal.Parse(_total_units.ToString()) == 0)
        {
          continue;
        }

        // Validate if exist concepts for the chip type/iso code
        CageMeters.ExistConceptsMovements(CashierSessionInfo.CashierSessionId, _currency.CurrencyCode, FeatureChips.ConvertToCageCurrencyType(_currency.Type), out _exists_concepts_movements, Trx);
        if (_exists_concepts_movements)
        {
          continue;
        }

        foreach (DataRow _dr in _cage_item.ItemAmounts.Rows)
        {
          if (String.IsNullOrEmpty(_dr["CHIP_ID"].ToString()))
          {
            continue;
          }

          _chip_id = (Int64)_dr["CHIP_ID"];
          Int32.TryParse(_dr["UN"].ToString(), out _units);

          if (!FeatureChips.Chips.ContainsKey(_chip_id) && _units > 0)
          {
            return false;
          }

          if (FeatureChips.Chips.ContainsKey(_chip_id) &&
            FeatureChips.Chips.GetChip(_chip_id).Stock < _units)
          {

            return false;
          }

          if (m_voucher_type == VoucherTypes.CloseCash &&
                  FeatureChips.Chips.ContainsKey(_chip_id) &&
                  FeatureChips.Chips.GetChip(_chip_id).Stock != _units)
          {

            return false;
          }
        }
      }

      return true;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Return if tips is not greather than amount chips
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //         Boolean: (True = tips  is not greather than amount chips)
    //
    //   NOTES :
    //
    private Boolean CheckTips()
    {
      foreach (KeyValuePair<CageDenominationsItems.CageDenominationKey, CageDenominationsItems.CageDenominationsItem> _cage_amount in m_cage_amounts)
      {

        if (((Cage.ShowDenominationsMode)GeneralParam.GetInt32("Cage", "ShowDenominations", 1) == Cage.ShowDenominationsMode.HideAllDenominations && m_voucher_type != VoucherTypes.WinLoss)
          || (m_voucher_type == VoucherTypes.WinLoss && m_win_loss_mode == GamingTableBusinessLogic.GT_WIN_LOSS_MODE.BY_TOTALS))
        {
          return true;
        }

        if (_cage_amount.Value.TotalAmount < _cage_amount.Value.TotalTips)
        {
          return false;
        }

      }

      return true;

    }


    public Boolean HasPendingRequests(Cage.CageOperationType OperationType, out Int64 MovementId)
    {
      StringBuilder _sb;
      Object _unique_id;

      MovementId = -1;

      _sb = new StringBuilder();
      _sb.AppendLine("    SELECT    TOP 1 CGM_MOVEMENT_ID               ");
      _sb.AppendLine("      FROM    CAGE_MOVEMENTS                      ");
      _sb.AppendLine("     WHERE    cgm_terminal_cashier_id = @pUserId  ");
      _sb.AppendLine("       AND    cgm_user_cashier_id = @pCashierId   ");
      _sb.AppendLine("       AND    cgm_status =  @pStatus              ");
      _sb.AppendLine("       AND    cgm_type =   @pOperationType        ");

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = CashierSessionInfo.TerminalId;  // No es necesario si va por solo por usuario.
            _cmd.Parameters.Add("@pCashierId", SqlDbType.Int).Value = CashierSessionInfo.UserId;
            _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = Cage.CageStatus.Sent;
            _cmd.Parameters.Add("@pOperationType", SqlDbType.Int).Value = OperationType;

            _unique_id = _cmd.ExecuteScalar();

          }// SqlCommand

        }// DB_TRX

        if (_unique_id == null)
        {
          return false;
        }

        if (_unique_id == DBNull.Value)
        {
          return false;
        }

        MovementId = (Int64)_unique_id;

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return false;
      }

      return true;

    }

    private void btn_cancel_request_Click(object sender, EventArgs e)
    {
      Int64 _movement_id;
      HasPendingRequests(Cage.CageOperationType.RequestOperation, out _movement_id);
      if (_movement_id == m_request_movement_id)
      {
        CancelPendingRequest(m_request_movement_id);
        m_cancel = true;

      }
      else
      {
        frm_message.Show(Resource.String("STR_UC_BANK_HAS_NOT_AMOUNT_REQUEST").Replace("\\r\\n", "\r\n"),
                         Resource.String("STR_UC_BANK_BTN_AMOUNT_REQUEST"),
                         MessageBoxButtons.OK,
                         MessageBoxIcon.Information,
                         this.ParentForm);
      }

      lbl_total_to_pay.Text = "";
      lbl_currency_to_pay.Text = "";

      Misc.WriteLog("[FORM CLOSE] frm_amount_input (cancel_request)", Log.Type.Message);
      this.Close();


    } // btn_cancel_request_Click

    private void CancelPendingRequest(Int64 RequestMovementId)
    {
      StringBuilder _sb;
      Boolean _result;

      _result = false;
      _sb = new StringBuilder();
      _sb.AppendLine("    UPDATE    CAGE_MOVEMENTS                      ");
      _sb.AppendLine("       SET    cgm_status = @pStatus               ");
      _sb.AppendLine("     WHERE    cgm_movement_id = @pMovementId      ");

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pMovementId", SqlDbType.BigInt).Value = RequestMovementId;  // No es necesario si va por solo por usuario.
            _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = Cage.CageStatus.Canceled;

            _cmd.ExecuteScalar();

            _result = true;

          }

          _db_trx.Commit();
        }
      }
      catch (Exception _ex)
      {
        frm_message.Show(Resource.String("STR_UC_BANK_CANCEL_REQUEST_NO_OK").Replace("\\r\\n", "\r\n"),
                         Resource.String("STR_UC_BANK_BTN_AMOUNT_REQUEST"),
                         MessageBoxButtons.OK,
                         MessageBoxIcon.Information,
                         this.ParentForm);

        Log.Exception(_ex);
        _result = false;
      }
      finally
      {
        if (_result)
        {
          frm_message.Show(Resource.String("STR_UC_BANK_CANCEL_REQUEST_OK").Replace("\\r\\n", "\r\n"),
                             Resource.String("STR_UC_BANK_BTN_AMOUNT_REQUEST"),
                             MessageBoxButtons.OK,
                             MessageBoxIcon.Information,
                             this.ParentForm);
        }
      }

    }

    private void web_browser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
    {

    } // CancelPendingRequest


    private void ShowHideBtnApplyTax()
    {
      if (Tax.EnableTitoTaxWaiver(true) && m_show_apply_tax_button)
      {
        btn_apply_tax.Enabled = Tax.ApplyTaxButtonEnabled(m_total_to_pay);
        btn_apply_tax.Visible = true;

        btn_apply_tax.Location = new Point(15, 516);
        if (Tax.ApplyTaxCollectionTicketPayment())
        {
          btn_apply_tax.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_APPLY_TAX");
        }
        else
        {
          btn_apply_tax.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_NO_APPLY_TAX");
        }
      }
      else
      {
        btn_apply_tax.Visible = false;
      }

    }


    private void btn_apply_tax_Click(object sender, EventArgs e)
    {
      string _error_str;

      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.HandpayTaxCollector,
                                                ProfilePermissions.TypeOperation.RequestPasswd, this, out _error_str))
      {
        return;
      }

      m_form_catalog = new frm_catalog(Tax.ApplyTaxCollectionTicketPayment());
      m_form_catalog.Show(this, out m_is_apply_tax_result, out m_account_operation_reason_id, out m_account_operation_comment);

      if (m_is_apply_tax_result)
      {
        RecalculateAmountTax();

        btn_apply_tax.Enabled = false;
      }
    }

    private void RecalculateAmountTax()
    {
      Decimal _amount_recalculate;
      Decimal _tax_1;
      Decimal _tax_2;
      Decimal _tax_3;

      _tax_1 = Math.Round(m_total_to_pay * m_pct_tax_1, 2, MidpointRounding.AwayFromZero);
      _tax_2 = Math.Round(m_total_to_pay * m_pct_tax_2, 2, MidpointRounding.AwayFromZero);
      _tax_3 = Math.Round(m_total_to_pay * m_pct_tax_3, 2, MidpointRounding.AwayFromZero);

      if (Tax.ApplyTaxCollectionTicketPayment())
      {
        _amount_recalculate = m_total_to_pay - (_tax_1 + _tax_2 + _tax_3);
        m_is_apply_tax = true;
      }
      else
      {
        _amount_recalculate = (Currency)m_input_amount;
        m_is_apply_tax = false;
      }

      PreviewVoucher((Currency)_amount_recalculate, m_is_apply_tax);
    }

    private string PreviewVoucherTicketORCardRedeem(Currency VoucherAmount, SqlTransaction sql_trx, Boolean IsApplyTaxes)
    {
      TYPE_CASH_REDEEM _cash_redeem_ab;
      TYPE_CASH_REDEEM _cash_company_a;
      TYPE_CASH_REDEEM _cash_company_b;
      TYPE_SPLITS _splits;
      MultiPromos.PromoBalance _ini_balance;
      MultiPromos.PromoBalance _fin_balance;
      MultiPromos.AccountBalance _acc_ini_balance;
      MultiPromos.AccountBalance _acc_fin_balance;
      Terminal.TerminalInfo _terminal_info;
      HANDPAY_TYPE _handpay_type;
      VoucherCardHandpay _handpay_voucher;
      Currency _played_amount;
      String _denom_str;
      String _denom_value;
      MultiPromos.PromoBalance _ticket_amount;
      Currency _max_devolution;
      Currency _cashier_current_balance;
      ArrayList _voucher_list;
      String _voucher_file_name;
      CashAmount _cash;

      _cash = new CashAmount();

      _acc_ini_balance = MultiPromos.AccountBalance.Zero;

      #region Split
      if (!Split.ReadSplitParameters(out _splits))
      {
        return String.Empty;
      }
      #endregion

      #region VoucherType

      if (m_voucher_type == VoucherTypes.TicketRedeem)
      {
        if (!Utils.AccountBalanceIn(m_card_data.AccountId, VoucherAmount, m_tito_max_devolution, out _acc_ini_balance, out _acc_fin_balance, sql_trx))
        {
          Log.Error("TITO.TicketRedeem: AccountBalanceIn. Error adding credit to card.");

          return String.Empty;
        }

        if (!CardData.DB_CardGetAllData(m_card_data.AccountId, m_card_data, sql_trx))
        {
          Log.Error("TITO.TicketRedeem: DB_CardGetAllData. Error reading card data.");

          return String.Empty;
        }
      }
      #endregion

      #region CalculateCashAmount
      _ini_balance = new MultiPromos.PromoBalance(m_card_data.AccountBalance, m_card_data.PlayerTracking.TruncatedPoints);

      if (m_handpay != null)
      {
        Handpays.HPAmountsCalculated _calculated;
        if (!Handpays.CalculateCashAmountHandpay(m_handpay.Status,
                                                 m_handpay.Amount,
                                                 m_handpay.TaxBaseAmount,
                                                 m_handpay.Type,
                                                 m_card_data,
                                                 IsApplyTaxes,
                                                 Tax.ApplyTaxCollectionByHandPay(m_handpay.Type, m_handpay.Level),
                                                 out _calculated))
        {
          String _str_warning;

          _cash.CalculateCashAmount_GetErrorMessage(_ini_balance.Balance.TotalRedeemable, m_voucher_amount, m_card_data, out _str_warning);

          frm_message.Show(_str_warning, Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this);

          is_voucher_loaded = false;

          return String.Empty;
        }

        _cash_redeem_ab = _calculated.CashRedeem;
        _cash_company_a = _calculated.CashRedeemA;
        _cash_company_b = _calculated.CashRedeemB;
      }
      else
      {
        if (!_cash.CalculateCashAmountTicketPayment(m_cash_mode, _splits,
                            _ini_balance,
                            m_voucher_amount,
                            false,
                            IsApplyTaxes,
                            Tax.ApplyTaxCollectionTicketPayment(),
                            true,
                            m_card_data,
                            out _fin_balance,
                            out _cash_redeem_ab,
                            out _cash_company_a,
                            out _cash_company_b))
        {
          String _str_warning;

          _cash.CalculateCashAmount_GetErrorMessage(_ini_balance.Balance.TotalRedeemable, m_voucher_amount, m_card_data, out _str_warning);

          frm_message.Show(_str_warning, Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this);

          is_voucher_loaded = false;

          return string.Empty;
        }
      }
      #endregion

      #region UpdateSessionBalance

      // DLL & RCI 28-OCT-2013
      m_cash_redeem_prize_amount = _cash_redeem_ab.prize;

      m_promo_lost = new MultiPromos.PromoBalance(_cash_redeem_ab.lost_balance.Balance, _cash_redeem_ab.lost_balance.Points);
      // The Redeemable Promotions to be lost excludes the Prize Coupon.
      m_promo_lost.Balance.PromoRedeemable -= _cash_company_b.cancellation; // PRIZE COUPON

      _cashier_current_balance = 0;

      try
      {
        _cashier_current_balance = CashierBusinessLogic.UpdateSessionBalance(sql_trx, CashierSessionInfo.CashierSessionId, 0);
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
      #endregion

      //  JCM 17-JAN-2012
      // If    ( order payment disabled  AND   current redeem exceds cashier balance )
      //    OR ( order payment enabled   AND   current redeem exceds cashier balance AND   current redeem < min_payment_order  )

      // APB (13-MAR-2009): amount to compare with cashier session balance must include (subtract) taxes
      //                    since they are not to be paid from the cashier in cash

      #region AmountToCompare
      if (!m_payment_order_enabled && _cash_redeem_ab.TotalPaid > _cashier_current_balance)
      {
        frm_message.Show(Resource.String("STR_FRM_AMOUNT_INPUT_MSG_AMOUNT_EXCEED_BANK"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this);

        if (m_cash_mode == CASH_MODE.TOTAL_REDEEM)
        {
          m_cancel = true;
        }
        is_voucher_loaded = false;

        return string.Empty;
      }
      #endregion

      // RCI 23-JAN-2013: Payment Order
      btn_check_redeem.Enabled = _cash_redeem_ab.TotalPaid >= m_payment_order_min_amount;
      m_jackpot_handpay_validation_numbers = new List<VoucherValidationNumber>();
      if (m_validation_numbers != null)
      {
        for (int i = m_validation_numbers.Count - 1; i >= 0; i--)
        {
          if (m_validation_numbers[i].TicketType == TITO_TICKET_TYPE.JACKPOT || m_validation_numbers[i].TicketType == TITO_TICKET_TYPE.HANDPAY)
          {
            m_jackpot_handpay_validation_numbers.Add(m_validation_numbers[i]);
          }
        }
      }

      #region VoucherCashOut
      if (Tax.EnableTitoTaxWaiver(true) && m_handpay != null)
      {
        _voucher_list = VoucherBuilder.CashOut(m_card_data.VoucherAccountInfo()
                                          , m_card_data.AccountId
                                          , m_handpay.Amount
                                          , _splits
                                          , _cash_company_a
                                          , _cash_company_b
                                          , 0
                                          , PrintMode.Preview
                                          , m_validation_numbers
                                          , sql_trx
                                          , false
                                          , IsApplyTaxes
                                          , false);

      }
      else
      {
        _voucher_list = VoucherBuilder.CashOut(m_card_data.VoucherAccountInfo()
                                          , m_card_data.AccountId
                                          , m_card_data.CurrentBalance
                                          , _splits
                                          , _cash_company_a
                                          , _cash_company_b
                                          , 0
                                          , PrintMode.Preview
                                          , m_validation_numbers
                                          , sql_trx
                                          , false
                                          , IsApplyTaxes
                                          , true);
      }
      #endregion

      if (IsApplyTaxes)
      {
        m_total_to_pay = _cash_redeem_ab.TotalPaid;
      }
      else
      {
        m_total_to_pay = _cash_redeem_ab.prize;
      }

      m_pct_tax_1 = _cash_redeem_ab.tax1;
      m_pct_tax_2 = _cash_redeem_ab.tax2;
      m_pct_tax_3 = _cash_redeem_ab.tax3;

      if (m_jackpot_handpay_validation_numbers.Count > 0)
      {
        // Save card data max devolution
        _max_devolution = m_card_data.MaxDevolution;

        foreach (VoucherValidationNumber _ticket in m_jackpot_handpay_validation_numbers)
        {
          #region HandPayType
          if (_ticket.TicketType == TITO_TICKET_TYPE.HANDPAY)
          {
            _handpay_type = HANDPAY_TYPE.CANCELLED_CREDITS;
          }
          else
          {
            _handpay_type = HANDPAY_TYPE.JACKPOT;
          }
          #endregion

          #region GetTerminalInfo
          Terminal.Trx_GetTerminalInfo(_ticket.CreatedTerminalName, out _terminal_info, sql_trx);
          WSI.Common.TITO.HandPay.GetHandpaySessionPlayedAmount(_ticket.CreatedAccountId
                                                                , _ticket.CreatedPlaySessionId
                                                                , out _played_amount
                                                                , out _denom_str
                                                                , out _denom_value
                                                                , sql_trx);

          #endregion

          _ticket_amount = new MultiPromos.PromoBalance(new MultiPromos.AccountBalance(_ticket.Amount, 0, 0), 0);

          #region CardDataMaxDevolution
          // Change card data max devolution to calculate cash amounts for the handpay
          m_card_data.MaxDevolution = Utils.MaxDevolutionAmount(_ticket.Amount, _ticket.TicketType == TITO_TICKET_TYPE.JACKPOT);

          _cash = new CashAmount();
          if (!_cash.CalculateCashAmount(m_cash_mode, _splits,
                                           _ticket_amount,
                                           _ticket.Amount,
                                           false,
                                           m_is_apply_tax,
                                           Tax.ApplyTaxCollectionTicketPayment(),
                                           m_card_data,
                                           out _fin_balance,
                                           out _cash_redeem_ab,
                                           out _cash_company_a,
                                           out _cash_company_b))
          {
            String _str_warning;

            _cash.CalculateCashAmount_GetErrorMessage(_ini_balance.Balance.TotalRedeemable, m_voucher_amount, m_card_data, out _str_warning);

            frm_message.Show(_str_warning, Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this);

            is_voucher_loaded = false;

            return string.Empty;
          }
          #endregion

          #region VoucherHandPay
          VoucherCardHandpay.VoucherCardHandpayInputParams _input_params;
          _input_params = new VoucherCardHandpay.VoucherCardHandpayInputParams();

          _input_params.VoucherAccountInfo = m_card_data.VoucherAccountInfo();
          _input_params.TerminalName = _ticket.CreatedTerminalName;
          _input_params.TerminalProvider = _terminal_info.ProviderName;
          _input_params.Type = _handpay_type;
          _input_params.TypeName = Handpays.HandpayTypeString(_handpay_type, 0);
          _input_params.CardBalance = 0;
          _input_params.Amount = _cash_redeem_ab.TotalPaid;
          _input_params.SessionPlayedAmount = _played_amount;
          _input_params.DenominacionStr = _denom_str;
          _input_params.DenominacionValue = _denom_value;
          _input_params.Mode = PrintMode.Preview;
          _input_params.ValidationNumber = _ticket;
          _input_params.InitialAmount = _cash_redeem_ab.TotalRedeemed;

          _input_params.TaxAmount = 0;
          _input_params.TaxBaseAmount = 0;
          _input_params.TaxPct = 0;
          _input_params.Deductions = 0;

          _handpay_voucher = new VoucherCardHandpay(_input_params, OperationCode.NOT_SET, sql_trx);
          _voucher_list.Add(_handpay_voucher);
          #endregion
        }

        // Restore card data max devolution
        m_card_data.MaxDevolution = _max_devolution;
      }

      if (m_handpay != null)
      {
        #region CalculateCashAmountHandPay
        Handpays.HPAmountsCalculated _calculated;
        if (!Handpays.CalculateCashAmountHandpay(m_handpay.Status,
                                                 m_handpay.Amount,
                                                 m_handpay.TaxBaseAmount,
                                                 m_handpay.Type,
                                                 m_card_data,
                                                 IsApplyTaxes,
                                                 Tax.ApplyTaxCollectionByHandPay(m_handpay.Type, m_handpay.Level),
                                                 out _calculated))
        {
          String _str_warning;

          _cash = new CashAmount();
          _cash.CalculateCashAmount_GetErrorMessage(_ini_balance.Balance.TotalRedeemable, m_voucher_amount, m_card_data, out _str_warning);

          frm_message.Show(_str_warning, Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this);

          is_voucher_loaded = false;

          return string.Empty;
        }
        #endregion

        #region VoucherParamInputandpays
        VoucherCardHandpay.VoucherCardHandpayInputParams _input_params;

        _input_params = FillVoucherHandPayInputParams(ref _cash_redeem_ab);

        _handpay_voucher = new VoucherCardHandpay(_input_params, OperationCode.NOT_SET, sql_trx);

        _voucher_list.Add(_handpay_voucher);
        m_total_to_pay = _cash_redeem_ab.TotalPaid;
        #endregion
      }

      _voucher_file_name = VoucherPrint.GenerateFileForPreview(_voucher_list);

      m_pct_tax_1 = _cash_redeem_ab.tax1_pct;
      m_pct_tax_2 = _cash_redeem_ab.tax2_pct;
      m_pct_tax_3 = _cash_redeem_ab.tax3_pct;

      lbl_total_to_pay.Text = m_total_to_pay.ToString();

      return _voucher_file_name;
    } // PreviewVoucher

    private FeatureChips.ChipsOperation.Operation ConvertToChipsOperationType(VoucherTypes VoucherType)
    {
      FeatureChips.ChipsOperation.Operation _chips_operation_type;

      _chips_operation_type = FeatureChips.ChipsOperation.Operation.NONE;

      switch (VoucherType)
      {
        case VoucherTypes.FillIn:
          _chips_operation_type = FeatureChips.ChipsOperation.Operation.FILL_IN;
          break;
        case VoucherTypes.FillOut:
          _chips_operation_type = FeatureChips.ChipsOperation.Operation.FILL_OUT;
          break;
        case VoucherTypes.OpenCash:
          _chips_operation_type = FeatureChips.ChipsOperation.Operation.OPEN_CASH;
          break;
        case VoucherTypes.CloseCash:
          _chips_operation_type = FeatureChips.ChipsOperation.Operation.CLOSE_CASH;
          break;
        case VoucherTypes.DropBoxCount:
          _chips_operation_type = FeatureChips.ChipsOperation.Operation.DROPBOX_COUNT;
          break;
        case VoucherTypes.ChipsSaleAmountInput:
          _chips_operation_type = FeatureChips.ChipsOperation.Operation.SALE;
          break;
        case VoucherTypes.ChipsSaleWithRecharge:
          _chips_operation_type = FeatureChips.ChipsOperation.Operation.SALE;
          break;
        case VoucherTypes.AmountRequest:
          _chips_operation_type = FeatureChips.ChipsOperation.Operation.CAGE_REQUEST;
          break;
        case VoucherTypes.ChipsPurchaseAmountInput:
          _chips_operation_type = FeatureChips.ChipsOperation.Operation.PURCHASE;
          break;
        case VoucherTypes.ChipsStock:
          _chips_operation_type = FeatureChips.ChipsOperation.Operation.SHOW_STOCK;
          break;
        case VoucherTypes.WinLoss:
          _chips_operation_type = FeatureChips.ChipsOperation.Operation.WIN_LOSS;
          break;
        default:
          break;
      }

      return _chips_operation_type;
    }


    private VoucherCardHandpay.VoucherCardHandpayInputParams FillVoucherHandPayInputParams(ref TYPE_CASH_REDEEM _cash_redeem_ab)
    {
      VoucherCardHandpay.VoucherCardHandpayInputParams _input_params;
      _input_params = new VoucherCardHandpay.VoucherCardHandpayInputParams();

      _input_params.VoucherAccountInfo = m_card_data.VoucherAccountInfo();
      _input_params.TerminalName = m_handpay.TerminalName;
      _input_params.TerminalProvider = m_handpay.TerminalProvider;
      _input_params.Type = m_handpay.Type;
      _input_params.TypeName = m_handpay.TypeName;
      _input_params.CardBalance = 0;
      _input_params.Amount = _cash_redeem_ab.TotalPaid;
      _input_params.SessionPlayedAmount = m_handpay.SessionPlayedAmount;
      _input_params.DenominacionStr = m_handpay.DenominationStr;
      _input_params.DenominacionValue = m_handpay.DenominationValue;
      _input_params.Mode = PrintMode.Preview;
      _input_params.ValidationNumber = null;
      _input_params.InitialAmount = _cash_redeem_ab.TotalRedeemed;

      _input_params.TaxAmount = m_handpay.TaxAmount;
      _input_params.TaxBaseAmount = m_handpay.TaxBaseAmount;
      _input_params.TaxPct = m_handpay.TaxPct;

      if (Tax.EnableTitoTaxWaiver(true) && m_is_apply_tax)
      {
        _input_params.Deductions = _input_params.TaxBaseAmount * _input_params.TaxPct;
      }
      else
      {
        _input_params.Deductions = _input_params.TaxAmount;
      }

      return _input_params;
    }

    private void dgv_common_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
    {
      if (m_grid_type == GridType.CageDenominations)
      {
        for (Int16 _count = 0; _count <= dgv_common.RowCount - 1; ++_count)
        {
          //LTC 05-AGO-2016
          if (dgv_common.Rows[_count].Cells[GRID_COLUMN_CAGE_AMOUNTS_CURRENCY_TYPE_ENUM].Value != DBNull.Value)
          {
            if (FeatureChips.IsChipsType((CurrencyExchangeType)dgv_common.Rows[_count].Cells[GRID_COLUMN_CAGE_AMOUNTS_CURRENCY_TYPE_ENUM].Value))
            {
              SetChipsColor(_count);
            }
          }
        }
      }
    }

    /// <summary>
    /// Hide or show rows without input parameter
    /// </summary>
    /// <param name="ShowRows">True show rows, false hide rows</param>
    private void HideShowRowsWithoutAmount(Boolean ShowRows = true)
    {
      if (ShowRows)
      {
        btn_show_hide_rows.Text = Resource.String("STR_HIDE_ROWS_WITHOUT_AMOUNT");
        foreach (DataGridViewRow _row in dgv_common.Rows)
        {
          if (!_row.Visible)
          {
            _row.Visible = true;
          }
        }
      }
      else
      {
        btn_show_hide_rows.Text = Resource.String("STR_SHOW_ROWS_WITHOUT_AMOUNT");
        foreach (DataGridViewRow _row in dgv_common.Rows)
        {
          if (String.IsNullOrEmpty(_row.Cells[GRID_COLUMN_CAGE_AMOUNTS_UN].Value.ToString()) || Int32.Parse(_row.Cells[GRID_COLUMN_CAGE_AMOUNTS_UN].Value.ToString()) == 0)
          {
            dgv_common.CurrentCell = null;
            _row.Visible = false;
          }
        }
      }
    }

    /// <summary>
    /// Hide column total if only currency exchange type is card
    /// </summary>
    /// <param name="type">is card</param>
    private void HideShowColumnByCurrencyExchangeCard(bool isCard)
    {
      if (!m_tito_enabled
                 && dgv_common.Columns.Count >= GRID_COLUMN_CAGE_AMOUNTS_TOTAL
                 && GeneralParam.GetBoolean("Cage", "ShowDenominations", true)
                 && (m_voucher_type == VoucherTypes.FillOut || m_voucher_type == VoucherTypes.CloseCash))
      {
        if (isCard)
        {
          if (Misc.IsVoucherModeTV() && Misc.IsPinPadEnabled())
          {
            dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_TOTAL].Visible = false;
          }
          else
          {
            dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_TOTAL].Visible = true;
          }
        }
        else
        {
          dgv_common.Columns[GRID_COLUMN_CAGE_AMOUNTS_TOTAL].Visible = true;
        }
      }
    }

    private void btn_show_hide_rows_Click(object sender, EventArgs e)
    {
      m_show_rows_without_amount = !m_show_rows_without_amount;
      HideShowRowsWithoutAmount(m_show_rows_without_amount);
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Force click to ensure that amount is really setted to avoid Bug #27.196
    //  PARAMS :   
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //          - Simply makes a call to 'btn_intro_Click' event when that conditions are given:
    //            - ShowDenominations=0.
    //            - m_currencies_amount is setted to 0 yet.
    //
    private void AutoClickIntroButton()
    {
      // OMC [BUG #27.196] Faltaba trasladar el valor del monto a depositar, se reaprovecha el evento del 'btn_intro_Click' 
      // que ya lo hace para hacer que en el momento de aceptar se haga el click en este de forma impl�cita.
      //Se valida que el txt_amount.txt
      if (!string.IsNullOrEmpty(txt_amount.Text) && m_currencies_amount.Count == 0 && (Cage.ShowDenominationsMode)GeneralParam.GetInt32("Cage", "ShowDenominations") == 0)
      {
        btn_intro_Click(null, null);
      }
    } //AutoClickIntroButton

    private void btn_enter_flyer_Click(object sender, EventArgs e)
    {
      JunketsShared _junket_info = null;
      Currency _amount = 0.00m;

      frm_input_flyer _input_flyer = new frm_input_flyer();
      frm_yesno _shader = new frm_yesno();

      _shader.Opacity = 0.6f;
      _shader.Show();

      // Form requesting to enter flyer code
      _input_flyer.Show(m_card_data.AccountId, this.ParentForm, out _junket_info);

      m_junket_info = _junket_info;

      if (_junket_info != null && _junket_info.BusinessLogic.PromotionID != null)
      {
        m_selected_promo_id = (long)_junket_info.BusinessLogic.PromotionID;
        SelectCurrentPromoGrid();
        dgv_common.Enabled = false;
        m_junket_info = _junket_info;
      }

      if (_junket_info != null)
      {
        if (m_voucher_amount != 0 || m_selected_promo_id > 0)
        {
          PreviewVoucher(m_voucher_amount, true);
        }
      }

      _shader.Hide();
      _shader.Dispose();

      _input_flyer.Hide();
      _input_flyer.Dispose();
    }

    #region Events

    /// <summary>
    /// Event for flyers recived.
    /// </summary>
    /// <param name="e"></param>
    /// <param name="data"></param>
    private void FlyerReceivedEvent(KbdMsgEvent e, Object data)
    {

      if (e == KbdMsgEvent.EventFlyer)
      {
        this.m_barcode = (String)data;
        CheckFlyerCode();
      }

    } // FlyerReceivedEvent
    #endregion

    private void CheckFlyerCode()
    {
      JunketsShared _junket_info = new JunketsShared();

      _junket_info.BusinessLogic = new Common.Junkets.JunketsBusinessLogic();
      _junket_info.BusinessLogic.AccountId = m_card_data.AccountId;

      _junket_info.BusinessLogic.FlyerCode = this.m_barcode;


      if (_junket_info.ValidateFlyerCode(_junket_info.BusinessLogic.AccountId, this.m_barcode, this))
      {
        if (_junket_info.BusinessLogic.PromotionID != null)
        {
          m_selected_promo_id = (long)_junket_info.BusinessLogic.PromotionID;
          SelectCurrentPromoGrid();
          dgv_common.Enabled = false;
        }

        this.m_junket_info = _junket_info;
      }

      if (_junket_info != null)
      {
        if (m_voucher_amount > 0 || m_selected_promo_id > 0)
        {
          PreviewVoucher(m_voucher_amount, true);
        }
      }
    }
  }

    #endregion

}