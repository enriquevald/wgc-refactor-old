//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : frm_payment_order.cs
// 
//   DESCRIPTION : 
// 
// REVISION HISTORY :
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 30-JAN-2012 TJG    
// 06-AUG-2012 DDM     Modifications: - Deleted all tabs except the main.
//                                    - Use the class Withhold instead CardData.
//                                    - The SaveWitholding function, only Save 
//                                      values in the class Withhold
// 05-AUG-2012 DDM     Fixed bug. Not should check the last name 2 
//
// 31-JAN-2013 LEM     Change surname 2 to optional (allow empty string)
// 05-FEB-2013 LEM     Changed type of some controls for data validating (PhoneNumberTextBox and CharactertextBox)
// 11-FEB-2013 ICS     Added a format validation for Name field
// 19-FEB-2013 ICS     Added a format validation for All fields
// 09-AGO-2013 LEM     Auto TAB for date fields.
// 13-AUG-2013 DRV     Removed disabling OSK if it's in window mode. 
// 20-AUG-2013 FBA     WIG-104: Fixed bug with auto-tab on date fields
// 04-AUG-2014 MPO     WIG-1146: Error on the label of payment order
// 22-AUG-2014 JBC     WIG-1201: Second combo from documents always appear empty.
// 02-OCT-2014 OPC     WIG-1366: Modified the width of the document combo.
// 09-OCT-2017 JML     PBI 30022:WIGOS-4052 Payment threshold registration - Recharge with Check payment
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WSI.Common;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using WSI.Cashier.Controls;

namespace WSI.Cashier
{
  public partial class frm_payment_order : frm_base
  {

    #region Constants

    const Int32 TAB_PAYMENT = 0;
    const Int32 TAB_BANK = 1;
    const Int32 TAB_ACCOUNT = 2;

    const Int32 NUM_TABS = 3;

    #endregion

    #region Attributes

    Currency m_cashable;
    PaymentOrder m_pay_order;
    WithholdData m_account_data;
    DataTable m_banks_table;
    CardData m_card_data;
    Decimal m_min_amount;
    uc_round_textbox m_editable_textbox;
    uc_round_textbox m_calculated_textbox;
    Currency m_cashier_current_balance;
    Boolean m_allow_player_edit;
    Boolean m_generate_document_enabled;
    Currency m_total_cashable;

    private static String m_decimal_str = System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencyDecimalSeparator;

    #endregion

    #region Constructor

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="Parent"></param>
    /// <param name="Card"></param>
    public frm_payment_order(WithholdData WithHoldingData, CardData CardData, Currency Cashable, Boolean HasWitholding)
    {
      this.m_cashable = Cashable;
      this.m_account_data = WithHoldingData;
      this.m_card_data = CardData;
      this.m_allow_player_edit = (HasWitholding == false);

      this.m_generate_document_enabled = PaymentOrder.IsGenerateDocumentEnabled();
      

      InitializeComponent();

      InitializeControlResources();

      Init();
    }

    public DialogResult Show(Currency Cashable, out PaymentOrder PayOrder)
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_payment_order", Log.Type.Message);
      DialogResult _rc;

      m_total_cashable = Cashable;

      _rc = this.ShowDialog();

      PayOrder = this.m_pay_order;

      return _rc;
    }

    #endregion

    #region Private Methods

    private void LoadDocTypeCombos()
    {
      DataTable _dt_doc_types1;
      DataTable _dt_doc_types2;

      IdentificationTypes.GetIdentificationTypesPaymentOrders(out _dt_doc_types1, out _dt_doc_types2);

      if (_dt_doc_types1.Rows.Count > 0)
      {
        cb_type_doc1.DataSource = _dt_doc_types1;
        cb_type_doc1.ValueMember = "IDT_ID";
        cb_type_doc1.DisplayMember = "IDT_NAME";

        // WIG-1146: It is necessary load -1 for don't show the text "System.Data.DataRowView"
        cb_type_doc1.SelectedIndex = -1;
        cb_type_doc1.SelectedIndex = 0;
      }

      if (_dt_doc_types2.Rows.Count > 0)
      {
        cb_type_doc2.DataSource = _dt_doc_types2;
        cb_type_doc2.ValueMember = "IDT_ID";
        cb_type_doc2.DisplayMember = "IDT_NAME";

        // WIG-1146: It is necessary load -1 for don't show the text "System.Data.DataRowView"
        cb_type_doc2.SelectedIndex = -1;
        cb_type_doc2.SelectedIndex = 0;
        while (cb_type_doc1.Text == cb_type_doc2.Text && cb_type_doc2.SelectedIndex < (cb_type_doc2.Items.Count - 1))
        {
          cb_type_doc2.SelectedIndex = cb_type_doc2.SelectedIndex + 1;
        }
      }
      else
      {
        cb_type_doc2.Visible = false;
        ef_holder_id_02.Visible = false;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Initialize the controls
    // 
    //  PARAMS :
    //      - INPUT :
    //         
    //      - OUTPUT :
    //
    // RETURNS :
    // 
    private void InitializeControlResources()
    {
      String _str_min;
      Decimal _min;

      // Title
      this.FormTitle = Resource.String("STR_ACCOUNT_PAYMENT_ORDER_PAYMENT_ORDER_CREATION");
      
      //    - Main

      // Read Document Type from GP.
      LoadDocTypeCombos();

      if (m_generate_document_enabled)
      {
        // Authorization Title, Name
        LoadAuthorizationData();
      }

      //   - Buttons
      btn_ok.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_OK");
      btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");

      //lbl_bank_acount.Text = Resource.String("STR_FRM_PAY_ORDER_BANK_ACCOUNT");

      lbl_Bank_Check.Text = Resource.String("STR_FRM_PAY_ORDER_BANK_CHECK");
      lbl_cash.Text = Resource.String("STR_FRM_PAY_ORDER_CASH");
      lbl_total.Text = Resource.String("STR_FRM_PAY_ORDER_TOTAL");

      // this.Text = Resource.String("STR_ACCOUNT_PAYMENT_ORDER_PAYMENT_ORDER_CREATION");

      //Tab1
      Main.Text = Resource.String("STR_ACCOUNT_PAYMENT_ORDER_CLIENT");
      if (GeneralParam.GetBoolean("Account.VisibleField", "Name2", true))
      {
        lbl_name_01.Text = Resource.String("STR_ACCOUNT_PAYMENT_ORDER_FIRST_SURNAME");
      }
      else
      {
        lbl_name_01.Text = Resource.String("STR_FRM_PLAYER_EDIT_LAST_NAME");
      }
      lbl_name_02.Text = Resource.String("STR_ACCOUNT_PAYMENT_ORDER_SECOND_SURNAME");
      lbl_name_03.Text = Resource.String("STR_ACCOUNT_PAYMENT_ORDER_NAME");
      lbl_name_04.Text = Resource.String("STR_FRM_PLAYER_EDIT_NAME4");

      //Tab2
      tap_values.Text = Resource.String("STR_ACCOUNT_PAYMENT_ORDER_CONFIGURATION");
      lbl_bank_acount.Text = Resource.String("STR_ACCOUNT_PAYMENT_ORDER_ACCOUNT");
      uc_aply_date.DateText = Resource.String("STR_ACCOUNT_PAYMENT_ORDER_DATE");
      lb_authorization_title.Text = Resource.String("STR_ACCOUNT_PAYMENT_ORDER_AUTHORIZATION_POSITION", " 1");
      lb_authorization1.Text = Resource.String("STR_ACCOUNT_PAYMENT_ORDER_AUTHORIZATION", " 1");
      lb_authorization_name.Text = Resource.String("STR_ACCOUNT_PAYMENT_ORDER_AUTHORIZATION_PERSON", " 2");
      lb_authorization2.Text = Resource.String("STR_ACCOUNT_PAYMENT_ORDER_AUTHORIZATION", " 2");
      lbl_bank_name.Text = String.Empty;
      lbl_account_number.Text = String.Empty;

      //Tab3
      tab_payment.Text = Resource.String("STR_ACCOUNT_PAYMENT_ORDER_PAYMENT_METHOD");
      lbl_Bank_Check.Text = Resource.String("STR_ACCOUNT_PAYMENT_ORDER_CHECK_PAYMENT");
      lbl_cash.Text = Resource.String("STR_ACCOUNT_PAYMENT_ORDER_CASH_PAYMENT");
      lbl_total.Text = Resource.String("STR_ACCOUNT_PAYMENT_ORDER_TOTAL_PAYMENT");
      ef_bank_check.ReadOnly = false;

      _min = Format.ParseCurrency(WSI.Common.Misc.ReadGeneralParams("PaymentOrder", "MinAmount"));
      m_min_amount = _min;
      //if (Decimal.TryParse(_str_min, out _min))
      //{
      //  m_min_amount = _min;
      //}

      lb_doc.Text = Resource.String("STR_FRM_PLAYER_EDIT_DOCUMENT");
      lb_doc_type.Text = Resource.String("STR_FRM_PLAYER_EDIT_TYPE_DOCUMENT");

      lbl_check_type.Text = Resource.String("STR_BANK_TRANSACTION_CHECK_TYPE");//"Check Type";
      cb_check_types.Items.Add("");  // None
      cb_check_types.Items.Add(Resource.String("STR_BANK_TRANSACTION_CHECK_TYPE_BEARER_CHECK"));
      cb_check_types.Items.Add(Resource.String("STR_BANK_TRANSACTION_CHECK_TYPE_NOMINAL_CHECK"));

      cb_check_types.SelectedIndex = 0;
      cb_check_types.Enabled = true;

    }// InitializeControlResources

    private void LoadAuthorizationData()
    {
      if (GeneralParam.GetBoolean("PaymentOrder", "Authorization1.Required"))
      {
        ef_authorization1_title.Text = GeneralParam.GetString("PaymentOrder", "Authorization1.DefaultTitle");
        ef_authorization1_name.Text = GeneralParam.GetString("PaymentOrder", "Authorization1.DefaultName");
      }
      else
      {
        ef_authorization1_title.Visible = false;
        ef_authorization1_name.Visible = false;
        lb_authorization1.Visible = false;
      }

      if (GeneralParam.GetBoolean("PaymentOrder", "Authorization2.Required"))
      {
        ef_authorization2_title.Text = GeneralParam.GetString("PaymentOrder", "Authorization2.DefaultTitle");
        ef_authorization2_name.Text = GeneralParam.GetString("PaymentOrder", "Authorization2.DefaultName");
      }
      else
      {
        ef_authorization2_title.Visible = false;
        ef_authorization2_name.Visible = false;
        lb_authorization2.Visible = false;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Init the properties of the class.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    private void Init()
    {
      // Center screen
      this.Location = new Point(1024 / 2 - this.Width / 2, 0);

      // Show keyboard if automatic OSK it's enabled
      WSIKeyboard.Toggle();

      // Enable the edition controls
      EnableControls();

      // Load existing card content
      LoadAccountData();

      uc_aply_date.Init();
      uc_aply_date.DateValue = WGDB.Now;

      m_cashier_current_balance = 0;

      try
      {
        using (DB_TRX _trx = new DB_TRX())
        {
          if (m_generate_document_enabled)
          {
            PaymentOrder.BanksAccounts(true, out m_banks_table, _trx.SqlTransaction);
          }
          m_cashier_current_balance = CashierBusinessLogic.UpdateSessionBalance(_trx.SqlTransaction, Cashier.SessionId, 0);
        }

        if (m_generate_document_enabled)
        {
          foreach (DataRow _dr in m_banks_table.Rows)
          {
            cb_bank_account.Items.Add(_dr[2].ToString());
          }
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        Misc.WriteLog("[FORM CLOSE] frm_payment_order (init -> exception)", Log.Type.Message);
        this.Close();
        //Shows OSK if Automatic OSK it's enabled
        WSIKeyboard.Toggle();

        return;
      }

      ef_bank_check.Enabled = true;
      lb_total_cashable.Enabled = true;
      ef_cash.Enabled = true;

      m_editable_textbox = ef_bank_check;
      m_calculated_textbox = ef_cash;

      m_editable_textbox.BackColor = System.Drawing.Color.LightGoldenrodYellow;
      m_calculated_textbox.Style = uc_round_textbox.RoundTextBoxStyle.BASIC;

      ef_bank_check.Text = m_cashable.ToString();
      lb_total_cashable.Text = m_cashable.ToString();
      ef_cash.Text = Currency.Zero().ToString();
      
      // RCI 19-AUG-2010: Disable timer. Check data only on Ok button press.
      // Check if data is correct and activate timer
      //CheckData(true);
    } // Init

    //------------------------------------------------------------------------------
    // PURPOSE: Enables the controls.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //    
    private void EnableControls()
    {
      // Tabs
      //    - Main
      ef_holder_id_01.Enabled = true;
      ef_holder_id_02.Enabled = true;
      ef_name_01.Enabled = this.m_allow_player_edit;
      ef_name_02.Enabled = this.m_allow_player_edit;
      ef_name_03.Enabled = this.m_allow_player_edit;
      ef_name_04.Enabled = this.m_allow_player_edit;

      if (!this.m_allow_player_edit)
      {
        ef_name_01.BackColor = Color.Silver;
        ef_name_02.BackColor = Color.Silver;
        ef_name_03.BackColor = Color.Silver;
      }

      if (!m_generate_document_enabled)
      {
        
        this.tab_client.TabPages.RemoveAt(TAB_ACCOUNT);
        this.tab_client.TabPages.RemoveAt(TAB_BANK);

      }

    } // EnableControls

    private void VisualizeData()
    {
      VisibleData _visible_data;

      _visible_data = new VisibleData();

      _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.NAME1, new Control[] { lbl_name_01, ef_name_01 });
      _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.NAME2, new Control[] { lbl_name_02, ef_name_02 });
      _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.NAME3, new Control[] { lbl_name_03, ef_name_03 });
      _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.NAME4, new Control[] { lbl_name_04, ef_name_04 });

      _visible_data.HideControls();

    }

    //------------------------------------------------------------------------------
    // PURPOSE: Loads the account data.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //   
    private void LoadAccountData()
    {
      // Tabs
      //    - Main

      ef_name_01.Text = m_account_data.HolderName1;      // Family Name 01
      ef_name_02.Text = m_account_data.HolderName2;      // Family Name 02
      ef_name_03.Text = m_account_data.HolderName3;      // First Name
      ef_name_04.Text = m_account_data.HolderName4;      // Middle Name


      if (cb_type_doc1.Items.Count > 0)
      {
        ef_holder_id_01.Text = m_account_data.HolderId;   // Holder Document id 1
        cb_type_doc1.SelectedIndex = (int)m_account_data.HolderDocumentId1;  // Holder Id type
      }
    } // LoadAccountData

    //------------------------------------------------------------------------------
    // PURPOSE: Gets the RFC.
    // 
    //  PARAMS:
    //      - INPUT: 
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //        If everything goes ok returns the RFC
    //        if fails returns ""
    private String ExpectedRFC(String Paterno, String Materno, String Nombre, DateTime Nacimiento)
    {
      String _expected_rfc;

      _expected_rfc = "";

      try
      {
        _expected_rfc = WSI.Common.RFC.RFC13(Paterno, Materno, Nombre, Nacimiento);
      }
      catch
      {
        _expected_rfc = "";
      }

      return _expected_rfc;
    } //ExpectedRFC

    //------------------------------------------------------------------------------
    // PURPOSE: Save the payment order.
    // 
    //  PARAMS:
    //      - INPUT: 
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - TRUE: if everything goes ok
    //      - FALSE: if fails 
    private Boolean SaveOrderPayment()
    {
      try
      {
        m_pay_order = new PaymentOrder();

        // Save values in the class Withhold
        m_pay_order.AccountId = m_account_data.AccountId;


        m_pay_order.HolderId1 = ef_holder_id_01.Text;                       // RFC
        m_pay_order.HolderId2 = ef_holder_id_02.Text;                       // CURP
        m_pay_order.HolderName1 = ef_name_01.Text.Trim();                   // Family Name 01
        m_pay_order.HolderName2 = ef_name_02.Text.Trim();                   // Family Name 02
        m_pay_order.HolderName3 = ef_name_03.Text.Trim();                   // First Name
        m_pay_order.HolderName4 = ef_name_04.Text.Trim();                   // Middle Name

        m_pay_order.HolderId1Type = cb_type_doc1.SelectedIndex < 0 ? "" : cb_type_doc1.SelectedValue.ToString();
        m_pay_order.HolderId2Type = cb_type_doc2.SelectedIndex < 0 ? "" : cb_type_doc2.SelectedValue.ToString();

        if (m_generate_document_enabled)
        {
          m_pay_order.BankId = ((Int32)m_banks_table.Rows[cb_bank_account.SelectedIndex].ItemArray[0]);
          m_pay_order.BankName = ((String)m_banks_table.Rows[cb_bank_account.SelectedIndex].ItemArray[1]);
          m_pay_order.EffectiveDays = ((String)m_banks_table.Rows[cb_bank_account.SelectedIndex].ItemArray[3]);
          m_pay_order.BankCustomerNumber = ((String)m_banks_table.Rows[cb_bank_account.SelectedIndex].ItemArray[4]);
          m_pay_order.BankAccountNumber = ((String)m_banks_table.Rows[cb_bank_account.SelectedIndex].ItemArray[5]);
          m_pay_order.PaymentType = ((String)m_banks_table.Rows[cb_bank_account.SelectedIndex].ItemArray[6]);

          m_pay_order.AuthorizedName1 = ef_authorization1_name.Text;
          m_pay_order.AuthorizedName2 = ef_authorization2_name.Text;
          m_pay_order.AuthorizedTitle1 = ef_authorization1_title.Text;
          m_pay_order.AuthorizedTitle2 = ef_authorization2_title.Text;
        }
        else
        {
		  //TODO: A�adir coment
          m_pay_order.BankId = -1;
        }

        m_pay_order.CheckType = CurrencyExchangeSubType.CHECK;

        switch (cb_check_types.SelectedIndex)
        {
          case 1:
            m_pay_order.CheckType = CurrencyExchangeSubType.BEARER_CHECK;
            break;
          case 2:
            m_pay_order.CheckType = CurrencyExchangeSubType.NOMINAL_CHECK;
            break;
        }

        m_pay_order.CashPayment = Format.ParseCurrency(ef_cash.Text);

        m_pay_order.CheckPayment = Format.ParseCurrency(ef_bank_check.Text);

        m_pay_order.TotalPayment = m_cashable;

        m_pay_order.DocumentId = 0;

        m_pay_order.OrderDate = WGDB.Now;
        m_pay_order.ApplicationDate = uc_aply_date.DateValue;

        return true;
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      return false;
    } //SaveOrderPayment

    //------------------------------------------------------------------------------
    // PURPOSE : Check the entered data in a specific tab
    // 
    //  PARAMS :
    //      - INPUT :
    //          - TabOrder: Index of the tab that must be validated.
    //          - FocusIt : Whether the control containing a wrong value must be focused or not
    //
    //      - OUTPUT :
    //
    // RETURNS:
    //    True if all the information in text controls is correct.
    //    False if not.
    // 
    //   NOTES:
    private Boolean CheckDataFromTab(Int32 TabOrder, Boolean FocusIt)
    {
      String _aux_string;
      Currency _check;
      Currency _cash;
      Currency _min_check_amount;
      String _field;
      DateTime _from;
      DateTime _to;
      Currency _payment_threshold;

      lbl_msg_blink.Visible = false;
      lbl_msg_blink.Text = "";

      // RCI 19-AUG-2010: Disable timer. Check data only on Ok button press.
      //timer1.Enabled = true;

      try
      {
        switch (TabOrder)
        {
          case TAB_PAYMENT:

            _payment_threshold = PaymentThresholdAuthorization.GetOutputThreshold();
            if (WSI.Common.TITO.Utils.IsTitoMode() && _payment_threshold > 0 && _payment_threshold <= m_total_cashable && cb_check_types.SelectedIndex <= 0)
            {
              lbl_msg_blink.Text = Resource.String("STR_BANK_TRANSACTION_CHECK_TYPE_ERROR");

              if (FocusIt)
              {
                tab_client.SelectedIndex = TabOrder;
                cb_check_types.Focus();
              }

              return false;
            }

            _check = Format.ParseCurrency(this.ef_bank_check.Text);
            _cash = Format.ParseCurrency(this.ef_cash.Text);

            // Validate that sum of two values is equal than the cashable amount
            if (_cash + _check != m_cashable)
            {
              lbl_msg_blink.Text = Resource.String("STR_FRM_AMOUNT_EXCEEDS_TOTAL");

              if (FocusIt)
              {
                tab_client.SelectedIndex = TabOrder;
                ef_bank_check.Focus();
              }

              return false;
            }

            // Validate that check amount is bigger than m�nimum value
            _min_check_amount = Math.Max(m_min_amount, 0.01m);

            if (_check < _min_check_amount)
            {
              lbl_msg_blink.Text = Resource.String("STR_FRM_MINIUM_VALUE_MUST_BE", lbl_Bank_Check.Text, _min_check_amount.ToString());

              if (FocusIt)
              {
                tab_client.SelectedIndex = TabOrder;
                ef_bank_check.Focus();
              }

              return false;
            }

            // Validate that cash amount is not negative
            if (_cash < 0)
            {
              lbl_msg_blink.Text = Resource.String("STR_FRM_VALUE_GRATHER_THAN", lbl_cash.Text, 0);

              if (FocusIt)
              {
                tab_client.SelectedIndex = TabOrder;
                ef_cash.Focus();
              }

              return false;
            }

            // Validate that cash pay is bigger than available cash in the caisher
            if (_cash > m_cashier_current_balance)
            {
              lbl_msg_blink.Text = Resource.String("STR_FRM_AMOUNT_INPUT_MSG_AMOUNT_EXCEED_BANK");

              if (FocusIt)
              {
                tab_client.SelectedIndex = TabOrder;
                ef_cash.Focus();
              }

              return false;
            }
            break;

          //VALIDATE BANKS TAB
          case TAB_BANK:

            if (uc_aply_date.IsEmpty)
            {
              lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC", uc_aply_date.DateText);

              if (FocusIt)
              {
                tab_client.SelectedIndex = TabOrder;
                uc_aply_date.Focus();
              }

              return false;
            }

            if (!uc_aply_date.ValidateFormat())
            {
              lbl_msg_blink.Text = Resource.String("STR_FRM_VALUE_MUST_BE_CORRECT_VALUE", uc_aply_date.DateText);

              if (FocusIt)
              {
                tab_client.SelectedIndex = TabOrder;
                uc_aply_date.Focus();
              }

              return false;
            }

            if (!PaymentOrder.IsValidApplicationDate(uc_aply_date.DateValue, out _from, out _to))
            {
              lbl_msg_blink.Text = Resource.String("STR_FRM_MUST_BE_BETWEEN_ERROR", uc_aply_date.DateText, _from.ToShortDateString(), _to.ToShortDateString());
              if (FocusIt)
              {
                tab_client.SelectedIndex = TabOrder;
                uc_aply_date.Focus();
              }

              return false;
            }

            // validate bank account
            if (cb_bank_account.SelectedIndex == -1)
            {

              lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_SELECT", lbl_bank_acount.Text);

              if (FocusIt)
              {
                tab_client.SelectedIndex = TabOrder;
                cb_bank_account.Focus();
              }

              return false;
            }

            //    - Authorization 1
            if (WSI.Common.Misc.ReadGeneralParams("PaymentOrder", "Authorization1.Required") == "1")//ef_authorization1_title.Visible
            {
              _aux_string = ef_authorization1_title.Text.Trim().ToUpperInvariant();
              ef_authorization1_title.Text = _aux_string;

              if (_aux_string == "")
              {
                _field = lb_authorization1.Text + " - " + lb_authorization_title.Text;
                lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC", _field);

                if (FocusIt)
                {
                  tab_client.SelectedIndex = TabOrder;
                  ef_authorization1_title.Focus();
                }

                return false;
              }
              else if (!ef_authorization1_title.ValidateFormat()) // Format Validation
              {
                lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", lb_authorization_title.Text);

                if (FocusIt)
                {
                  tab_client.SelectedIndex = TabOrder;
                  ef_authorization1_title.Focus();
                }

                return false;
              }

              _aux_string = ef_authorization1_name.Text.Trim().ToUpperInvariant();
              ef_authorization1_name.Text = _aux_string;

              if (_aux_string == "")
              {
                _field = lb_authorization1.Text + " - " + lb_authorization_name.Text;
                lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC", _field);

                if (FocusIt)
                {
                  tab_client.SelectedIndex = TabOrder;
                  ef_authorization1_name.Focus();
                }

                return false;
              }
              else if (!ef_authorization1_name.ValidateFormat()) // Format Validation
              {
                lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", lb_authorization_name.Text);

                if (FocusIt)
                {
                  tab_client.SelectedIndex = TabOrder;
                  ef_authorization1_name.Focus();
                }

                return false;
              }
            }

            //    - Authorization 2
            if (WSI.Common.Misc.ReadGeneralParams("PaymentOrder", "Authorization2.Required") == "1")//ef_authorization2_title.Visible
            {
              _aux_string = ef_authorization2_title.Text.Trim().ToUpperInvariant();
              ef_authorization2_title.Text = _aux_string;

              if (_aux_string == "")
              {
                _field = lb_authorization2.Text + " - " + lb_authorization_title.Text;
                lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC", _field);

                if (FocusIt)
                {
                  tab_client.SelectedIndex = TabOrder;
                  ef_authorization2_title.Focus();
                }

                return false;
              }
              else if (!ef_authorization2_title.ValidateFormat()) // Format Validation
              {
                lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", lb_authorization_title.Text);

                if (FocusIt)
                {
                  tab_client.SelectedIndex = TabOrder;
                  ef_authorization2_title.Focus();
                }

                return false;
              }

              _aux_string = ef_authorization2_name.Text.Trim().ToUpperInvariant();
              ef_authorization2_name.Text = _aux_string;

              if (_aux_string == "")
              {
                _field = lb_authorization2.Text + " - " + lb_authorization_name.Text;
                lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC", _field);

                if (FocusIt)
                {
                  tab_client.SelectedIndex = TabOrder;
                  ef_authorization2_name.Focus();
                }

                return false;
              }
              else if (!ef_authorization2_name.ValidateFormat()) // Format Validation
              {
                lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", lb_authorization_name.Text);

                if (FocusIt)
                {
                  tab_client.SelectedIndex = TabOrder;
                  ef_authorization2_name.Focus();
                }

                return false;
              }
            }
            break;

          // Validate User Fields
          case TAB_ACCOUNT:

            if (cb_type_doc1.Items.Count > 0)
            {
              _aux_string = ef_holder_id_01.Text.Trim().ToUpperInvariant();
              ef_holder_id_01.Text = _aux_string;

              if (_aux_string == "")
              {
                lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC", cb_type_doc1.Text);

                if (FocusIt)
                {
                  tab_client.SelectedIndex = TabOrder;
                  ef_holder_id_01.Focus();
                }

                return false;
              }
              else if (!ef_holder_id_01.ValidateFormat()) // Format Validation
              {
                lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", lb_doc.Text);

                if (FocusIt)
                {
                  tab_client.SelectedIndex = TabOrder;
                  ef_holder_id_01.Focus();
                }

                return false;
              }
            }
            //    - DOC2
            if (cb_type_doc2.Items.Count > 0)
            {
              _aux_string = ef_holder_id_02.Text.Trim().ToUpperInvariant();
              ef_holder_id_02.Text = _aux_string;

              if (cb_type_doc1.Text == cb_type_doc2.Text)
              {
                lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_SAME_TYPE_DOC");

                if (FocusIt)
                {
                  tab_client.SelectedIndex = TabOrder;
                  cb_type_doc2.Focus();
                }

                return false;
              }

              if (_aux_string == "")
              {
                lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC", cb_type_doc2.Text);

                if (FocusIt)
                {
                  tab_client.SelectedIndex = TabOrder;
                  ef_holder_id_02.Focus();
                }

                return false;
              }
              else if (!ef_holder_id_02.ValidateFormat()) // Format Validation
              {
                lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", lb_doc.Text);

                if (FocusIt)
                {
                  tab_client.SelectedIndex = TabOrder;
                  ef_holder_id_02.Focus();
                }

                return false;
              }


            }

            //    - Family name (Father)
            _aux_string = ef_name_01.Text.Trim().ToUpperInvariant();
            ef_name_01.Text = _aux_string;

            if (_aux_string == "")
            {
              lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC", lbl_name_01.Text);

              if (FocusIt)
              {
                tab_client.SelectedIndex = TabOrder;
                ef_name_01.Focus();
              }

              return false;
            }
            else if (!ef_name_01.ValidateFormat()) // Format Validation
            {
              lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", lbl_name_01.Text);

              if (FocusIt)
              {
                tab_client.SelectedIndex = TabOrder;
                ef_name_01.Focus();
              }

              return false;
            }

            //    - Family name (Mother) (optional)
            if (!String.IsNullOrEmpty(ef_name_02.Text) && !ef_name_02.ValidateFormat()) // Format Validation
            {
              lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", lbl_name_02.Text);

              if (FocusIt)
              {
                tab_client.SelectedIndex = TabOrder;
                ef_name_02.Focus();
              }

              return false;
            }

            //    - First name 
            _aux_string = ef_name_03.Text.Trim().ToUpperInvariant();
            ef_name_03.Text = _aux_string;

            if (_aux_string == "")
            {
              lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC", lbl_name_03.Text);

              if (FocusIt)
              {
                tab_client.SelectedIndex = TabOrder;
                ef_name_03.Focus();
              }

              return false;
            }
            else if (!ef_name_03.ValidateFormat()) // Format Validation
            {
              lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", lbl_name_03.Text);

              if (FocusIt)
              {
                tab_client.SelectedIndex = TabOrder;
                ef_name_03.Focus();
              }

              return false;
            }
            break;
        }

        return true;
      }
      finally
      {
        if (lbl_msg_blink.Text != "")
        {
          lbl_msg_blink.Visible = true;
        }
      }
    } // CheckDataFromTab

    //------------------------------------------------------------------------------
    // PURPOSE : Check the entered data
    // 
    //  PARAMS :
    //      - INPUT :
    //          - FocusIt : Whether the control containing a wrong value must be focused or not
    //
    //      - OUTPUT :
    //
    // RETURNS:
    //    True if all the information in text controls is correct.
    //    False if not.
    private Boolean CheckData(Boolean FocusIt)
    {
      Int32 _tab_to_check;
      try
      {

        if (!m_generate_document_enabled)
        {
          // Only validate tab payment
          if (!CheckDataFromTab(TAB_PAYMENT, FocusIt))
          {
            return false;
          }
        }
        else
        {
          // Validate all tabs
          for (int i = tab_client.SelectedIndex; i < (tab_client.SelectedIndex + NUM_TABS); i++)
          {
            _tab_to_check = (i % NUM_TABS);
            if (!CheckDataFromTab(_tab_to_check, FocusIt))
            {
              return false;
            }
          }
        }
        return true;
      }
      finally
      {
        if (lbl_msg_blink.Text != "")
        {
          lbl_msg_blink.Visible = true;
        }
      }
    } //CheckData

    //------------------------------------------------------------------------------
    // PURPOSE :  Check and Add digit to amount
    // 
    //  PARAMS :
    //      - INPUT :
    //          - NumberStr : a number in string format
    //
    //      - OUTPUT :
    //
    // RETURNS:
    // 
    //   NOTES:
    private void AddNumber(String NumberStr)
    {
      String _aux_str;
      String _tentative_number;
      Currency _input_amount;
      Int32 _dec_symbol_pos;

      // Prevent exceeding maximum number of decimals (2)
      _dec_symbol_pos = this.m_editable_textbox.Text.IndexOf(m_decimal_str);
      if (_dec_symbol_pos > -1)
      {
        _aux_str = this.m_editable_textbox.Text.Substring(_dec_symbol_pos);
        if (_aux_str.Length > 2)
        {
          return;
        }
      }

      // Prevent exceeding maximum amount length
      if (this.m_editable_textbox.Text.Length >= Cashier.MAX_ALLOWED_AMOUNT_LENGTH)
      {
        return;
      }

      _tentative_number = this.m_editable_textbox.Text + NumberStr;

      try
      {
        _input_amount = Format.ParseCurrency(_tentative_number);
        if (_input_amount > Cashier.MAX_ALLOWED_AMOUNT)
        {
          return;
        }
      }
      catch
      {
        _input_amount = 0;

        return;
      }

      // Remove unneeded leading zeros
      // Unless we are entering a decimal value, there must not be any leading 0
      if (_dec_symbol_pos == -1)
      {
        this.m_editable_textbox.Text = this.m_editable_textbox.Text.TrimStart('0') + NumberStr;
      }
      else
      {
        // Accept new symbol/digit 
        this.m_editable_textbox.Text = this.m_editable_textbox.Text + NumberStr;
      }
    } // AddNumber

    #endregion

    #region "Handlers"

    private void txt_date_TextChanged(object sender, EventArgs e)
    {
      TextBox _tag;

      _tag = (TextBox)sender;

      if (_tag.Text.Length < _tag.MaxLength)
      {
        return;
      }

      Control nextInTabOrder = GetNextControl((Control)sender, true);
      nextInTabOrder.Focus();
    }

    private void btn_ok_Click(object sender, EventArgs e)
    {
      // Check out whether the data is right
      if (CheckData(true))
      {
        // Save Witholding entered data to the related DB account
        if (SaveOrderPayment())
        {
          // hide keyboard and close
          WSIKeyboard.Hide();

          this.DialogResult = DialogResult.OK;

          this.Dispose();
        }
      }
    }

    private void btn_cancel_Click(object sender, EventArgs e)
    {
      // hide keyboard and close
      WSIKeyboard.Hide();

      this.DialogResult = DialogResult.Cancel;

      this.Dispose();
    }

    private void btn_keyboard_Click(object sender, EventArgs e)
    {
      // show keyboard (if hidden)
      WSIKeyboard.ForceToggle();
    }

    private void timer1_Tick(object sender, EventArgs e)
    {
      // Check if data is correct
      CheckData(false);
    }

    private void btn_num_1_Click(object sender, EventArgs e)
    {
      AddNumber("1");
    }

    private void btn_num_2_Click(object sender, EventArgs e)
    {
      AddNumber("2");
    }

    private void btn_num_3_Click(object sender, EventArgs e)
    {
      AddNumber("3");
    }

    private void btn_num_4_Click(object sender, EventArgs e)
    {
      AddNumber("4");
    }

    private void btn_num_5_Click(object sender, EventArgs e)
    {
      AddNumber("5");
    }

    private void btn_num_6_Click(object sender, EventArgs e)
    {
      AddNumber("6");
    }

    private void btn_num_7_Click(object sender, EventArgs e)
    {
      AddNumber("7");
    }

    private void btn_num_8_Click(object sender, EventArgs e)
    {
      AddNumber("8");
    }

    private void btn_num_9_Click(object sender, EventArgs e)
    {
      AddNumber("9");
    }

    private void btn_num_10_Click(object sender, EventArgs e)
    {
      AddNumber("0");
    }

    private void btn_num_dot_Click(object sender, EventArgs e)
    {
      if (this.m_editable_textbox.Text.IndexOf(m_decimal_str) == -1)
      {
        this.m_editable_textbox.Text = this.m_editable_textbox.Text + m_decimal_str;
      }
    }

    private void btn_back_Click(object sender, EventArgs e)
    {
      if (this.m_editable_textbox.Text.Length > 0)
      {
        this.m_editable_textbox.Text = this.m_editable_textbox.Text.Substring(0, this.m_editable_textbox.Text.Length - 1);
        if (WSI.Common.GeneralParam.GetString("WigosGUI", "CurrencySymbol") == this.m_editable_textbox.Text || String.IsNullOrEmpty(this.m_editable_textbox.Text))
        {
          this.m_editable_textbox.Text = "0";
        }
      }
    }

    private void ef_TextChanged(object sender, EventArgs e)
    {
      Currency _calculated;
      Currency _edited;
      Currency _total;

      if (String.IsNullOrEmpty(m_editable_textbox.Text))
      {
        return;
      }

      _total = m_cashable;

      _edited = Format.ParseCurrency(this.m_editable_textbox.Text);

      _calculated = _total - _edited;

      if (_calculated < 0)
      {
        this.m_calculated_textbox.Text = "---";

        return;
      }

      this.m_calculated_textbox.Text = _calculated.ToString();
    }

    private void ef_KeyPress(object sender, KeyPressEventArgs e)
    {
      String _aux_str;
      Char _c;

      _c = e.KeyChar;

      if (_c >= '0' && _c <= '9')
      {
        _aux_str = "" + _c;
        AddNumber(_aux_str);

        e.Handled = true;
      }

      if (_c == '\r')
      {
        btn_ok_Click(null, null);

        e.Handled = true;
      }

      if (_c == '\b')
      {
        btn_back_Click(null, null);

        e.Handled = true;
      }

      if (_c == '.')
      {
        btn_num_dot_Click(null, null);

        e.Handled = true;
      }

      this.m_editable_textbox.SelectionStart = this.m_editable_textbox.Text.Length;
      this.m_editable_textbox.SelectionLength = 0;

      e.Handled = true;
    }

    private void ef_bank_check_KeyPress(object sender, KeyPressEventArgs e)
    {
      ef_KeyPress(sender, e);
    }

    private void ef_cash_KeyPress(object sender, KeyPressEventArgs e)
    {
      ef_KeyPress(sender, e);
    }

    private void ef_bank_check_TextChanged(object sender, EventArgs e)
    {
      ef_TextChanged(sender, e);
    }

    private void ef_cash_TextChanged(object sender, EventArgs e)
    {
      ef_TextChanged(sender, e);
    }

    private void ef_Enter(object sender, EventArgs e)
    {
      Currency _value;

      m_editable_textbox.BackColor = System.Drawing.Color.LightGoldenrodYellow;
      m_calculated_textbox.Style = uc_round_textbox.RoundTextBoxStyle.BASIC;

      if (m_editable_textbox.Text == "---")
      {
        m_editable_textbox.Text = String.Empty;
      }
      else
      {
        _value = Format.ParseCurrency(this.m_editable_textbox.Text);
        m_editable_textbox.Text = _value.ToStringWithoutSymbols();
      }
      _value = Format.ParseCurrency(this.m_calculated_textbox.Text);
      m_calculated_textbox.Text = _value.ToString();
    }

    private void ef_bank_check_Enter(object sender, EventArgs e)
    {
      m_editable_textbox = ef_bank_check;
      m_calculated_textbox = ef_cash;

      ef_Enter(sender, e);
    }

    private void ef_cash_Enter(object sender, EventArgs e)
    {
      m_editable_textbox = ef_cash;
      m_calculated_textbox = ef_bank_check;

      ef_Enter(sender, e);
    }

    private void cb_bank_account_SelectedIndexChanged(object sender, EventArgs e)
    {
      lbl_bank_name.Text = Resource.String("STR_ACCOUNT_PAYMENT_BANK") + ": " + ((String)m_banks_table.Rows[cb_bank_account.SelectedIndex].ItemArray[1]);
      lbl_account_number.Text = Resource.String("STR_ACCOUNT_PAYMENT_ORDER_ACCOUNT") + ": " + ((String)m_banks_table.Rows[cb_bank_account.SelectedIndex].ItemArray[5]);
    }

    private void frm_payment_order_Load(object sender, EventArgs e)
    {
      VisualizeData();

      ef_holder_id_01.Focus();
    }

    #endregion
  }

}