//------------------------------------------------------------------------------
// Copyright � 2007-2009 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_container.cs
// 
//   DESCRIPTION: Implements the uc_card_reader user control for card track data input (uc_card_reader)
//
//        AUTHOR: AJQ
// 
// CREATION DATE: 29-AUG-2007
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 29-AUG-2007 AJQ     First release.
// 18-APR-2012 MPO     Generate activity when read the card
// 03-MAY-2012 JCM     Fixed Bug #265: Focus after user session expires
// 26-SEP-2014 DRV     Added KeyboardMessageEvent.
// 24-FEB-2016 DDS     Fixed Bug 9679: Allow read and hide 'Invalid number card' text from outside of the class
// 14-MAR-2016 FOS     Fixed Bug 9357: New design corrections
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using WSI.Common;

namespace WSI.Cashier
{

  public partial class uc_card_reader : UserControl
  {
    public delegate void TrackNumberReadEventHandler(String TrackNumber, ref Boolean IsValid);
    public delegate void TrackNumberReadingHandler();

    public event TrackNumberReadEventHandler OnTrackNumberReadEvent;
    public event TrackNumberReadingHandler OnTrackNumberReadingEvent;
    private bool m_error_message_below = false;
    private Int32 m_track_number_length = CardTrackData.MinimumSize();

    #region Class Atributes

    private Int32 tick_last_change = 0;
    private Boolean all_data_is_entered = false;
    private Boolean ignore_additional_tracks = false;
    private Boolean is_kbd_msg_filter_event = false;
    private String received_track_data;

    #endregion

    #region Constructor

    public uc_card_reader()
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[UC LOAD] uc_card_reader", Log.Type.Message);

      InitializeComponent();

      InitializeControlResources();
      if (!this.DesignMode)
      {
        tmr_clear.Enabled = true;
        ClearTrackNumber();
        txt_track_number.MaxLength = CardTrackData.MaximumSize();
        
      }
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Initialize control resources. (NLS string, images, etc.)
    /// </summary>
    public void InitializeControlResources()
    {
      // - NLS Strings:
      //   - Title

      //   - Labels
      lbl_swipe_card.Text = Resource.String("STR_UC_CARD_READER_SWIPE_CARD");
      lbl_invalid_card.Text = Resource.String("STR_UC_CARD_READER_INVALID_CARD");

    } // InitializeControlResources

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void pictureBox1_Click(object sender, EventArgs e)
    {
      ClearTrackNumber();
      txt_track_number.Focus();
    } // pictureBox1_Click

    /// <summary>
    /// Clear input for track number actions.
    /// </summary>
    public void ClearTrackNumber()
    {
      txt_track_number.Text = "";
      tick_last_change = 0;

      lbl_invalid_card.Visible = false;
      all_data_is_entered = false;
      ignore_additional_tracks = false;

    } // ClearTrackNumber

    /// <summary>
    /// Show message on bottom control
    /// </summary>
    public void ShowMessageBottom()
    {
      lbl_invalid_card.Location = new System.Drawing.Point(txt_track_number.Location.X, txt_track_number.Location.Y + 45);
    } // ShowMessageBottom

    /// <summary>
    /// Checks is the input is completely numeric.
    /// </summary>
    /// <param name="TrackNumber"></param>
    /// <returns></returns>
    private Boolean PartialTrackNumberIsValid(String TrackNumber)
    {
      
      return CardTrackData.IsPartialTrackDataValid(TrackNumber);
    } // PartialTrackNumberIsValid

    /// <summary>
    /// Track number checking.
    /// </summary>
    /// <param name="TrackNumber"></param>
    /// <returns></returns>
    private Boolean TrackNumberIsValid(String TrackNumber)
    {

        if (PartialTrackNumberIsValid(TrackNumber))
        {
          String _internal_track;
          int _card_type;

          return WSI.Common.CardNumber.TrackDataToInternal(TrackNumber, out _internal_track, out _card_type);
          
      }

      return false;
    } // TrackNumberIsValid

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void txt_track_number_TextChanged(object sender, EventArgs e)
    {
      String track_number;
      //if card readed meanwhile disconnected message, do nothing
      
      if (WSI.Common.WGDB.ConnectionState != ConnectionState.Open
          && WSI.Common.WGDB.ConnectionState != ConnectionState.Executing
          && WSI.Common.WGDB.ConnectionState != ConnectionState.Fetching)
      {
        ClearTrackNumber();
        return;
      }

      // Variables initialization
      tick_last_change = 0;
      if (is_kbd_msg_filter_event)
      {
        track_number = received_track_data;
      }
      else
      {
        track_number = txt_track_number.Text;
      }

      all_data_is_entered = (track_number.Length >= m_track_number_length);

      if (track_number.Length > 0)
      {
        
        lbl_invalid_card.Visible = !PartialTrackNumberIsValid(track_number);
        if (OnTrackNumberReadingEvent != null)
        {
          OnTrackNumberReadingEvent();
        }
      }
    } // txt_track_number_TextChanged

    /// <summary>
    /// Key pressed management.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void txt_track_number_KeyPress(object sender, KeyPressEventArgs e)
    {
      String track_number;
      Boolean is_valid;
      tick_last_change = 0;
      if (is_kbd_msg_filter_event)
      {
        track_number = received_track_data;
        is_kbd_msg_filter_event = false;
      }
      else
      {
        track_number = txt_track_number.Text;
      }
      switch (e.KeyChar)
      {
        case '%':
          // Ignore track start/end markers
          ignore_additional_tracks = false;
          e.KeyChar = '\0';
          break;

        case ';':
          // Ignore track start/end markers
          ignore_additional_tracks = true;
          e.KeyChar = '\0';
          break;

        case '?':
          // Ignore track start/end markers
          e.KeyChar = '\0';
          break;

        case '\r':
          if (!ignore_additional_tracks)
          {
            if (all_data_is_entered)
            {
              if (track_number.Length >= m_track_number_length)
              {
                is_valid = TrackNumberIsValid(track_number);

                if (is_valid)
                {
                  tmr_clear.Enabled = false;
                  if (OnTrackNumberReadEvent != null)
                  {
                    try
                    {
                      WSI.Common.Users.SetLastAction("ReadCard", "");
                      OnTrackNumberReadEvent(track_number, ref is_valid);
                    }
                    catch
                    {
                      is_valid = false;
                      ClearTrackNumber();
                    }
                  }
                  tmr_clear.Enabled = true;
                }
                
                if (!is_valid)
                {
                  txt_track_number.SelectAll();

                  ClearTrackNumber(); 
                  lbl_invalid_card.Visible = true;

                }
              }
            }
          }
          else
          {
            // Allow processing additional keystrokes
            ignore_additional_tracks = false;
          }

          e.Handled = true;
          break;

        default:
          if (ignore_additional_tracks)
          {
            // All keystrokes must be ignored until a valid sequence starts
            e.KeyChar = '\0';
          }
          break;
      } // switch
    } // txt_track_number_KeyPress

    /// <summary>
    /// Timer to clear card input, if key is not pressed in five seconds.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void tmr_clear_Tick(object sender, EventArgs e)
    {
      tick_last_change += 1;
      if (tick_last_change > 20) // 5 seconds
      {
        ClearTrackNumber();
        tick_last_change = 0;
      }
    } // tmr_clear_Tick

    private void AccountReceivedEvent(KbdMsgEvent e, Object data)
    {
      Barcode _barcode;
      KeyPressEventArgs _event_args;

      if (e == KbdMsgEvent.EventBarcode)
      {
        _barcode = (Barcode)data;
        if (CardTrackData.AdmitAnyData())
        {
          if (!String.IsNullOrEmpty(_barcode.ExternalCode))
          {
            received_track_data = _barcode.ExternalCode;
          }
          else
          { 
            received_track_data = _barcode.CardTrackData.ExternalTrackData;
          }
        }
        else
        { 
        received_track_data = _barcode.ExternalCode;
        }

        is_kbd_msg_filter_event = true;
        _event_args = new KeyPressEventArgs('\r');
        txt_track_number_TextChanged(this, new EventArgs());
        txt_track_number_KeyPress(this, _event_args);
      }
    }

    private void uc_card_reader_VisibleChanged(object sender, EventArgs e)
    {
      if (this.Visible)
      {
        KeyboardMessageFilter.RegisterHandler(this, BarcodeType.Account, AccountReceivedEvent);
      }
    }

    #endregion

    #region Public Properties

    public string InvalidCardText
    {
        get { return this.lbl_invalid_card.Text; }
    }

    public Boolean InvalidCardTextVisible
    {
        get { return this.lbl_invalid_card.Visible; }
        set { this.lbl_invalid_card.Visible = value; }
    }

    public bool ErrorMessageBelow {
      get { return m_error_message_below; }
      set
      {
        m_error_message_below = value;
        if (ErrorMessageBelow)
        {
          lbl_invalid_card.Location = new Point(227, 60);
        }
        else
        {
          lbl_invalid_card.Location = new Point(469, 19);
         
        }
      }
    }

    #endregion
  }
}
