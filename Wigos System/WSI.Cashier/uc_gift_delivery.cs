//------------------------------------------------------------------------------
// Copyright � 2010 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : uc_gift_delivery.cs
// 
//   DESCRIPTION : Implements the uc_gift_delivery user control
//                 Class: uc_gift_delivery (Card Operations)
//
// REVISION HISTORY
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 30-SEP-2010 TJG    First release.
// 29-OCT-2010 TJG    New gift: Points per draw numbers
// 29-MAR-2012 MPO    User session: Added a event for each button for set the last activity.
// 02-MAY-2012 JCM    Fixed Bug #267: Fixed focus in card reader.
// 29-MAY-2012 SSC    Moved LoyaltyProgram, Gift and GiftInstance classes to WSI.Common
// 11-OCT-2013 RMS    Fixed Bug WIG-272: Update Cashier Session Status label
// 31-JAN-2013 QMP    Fixed Bug WIG-563: Enabled buttons when cash session is closed
// 14-JUL-2014 AMF    Regionalization
// 09-NOV-2015 ECP    Backlog Item 5789 New Cashier Design
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Drawing.Printing;
using WSI.Common;

namespace WSI.Cashier
{
  public partial class uc_gift_delivery : UserControl
  {
#region Constants
#endregion

#region Class Atributes

    private static frm_yesno form_yes_no;

    Barcode             m_voucher_number;

    GiftInstanceCashier  m_gift_instance;
    Int32                m_tick_count_tracknumber_read = 0;
    Boolean              m_first_time = true;
    Boolean              m_gift_expired;
    Boolean              m_gift_canceled;

    System.Drawing.Color m_window_fore_color = System.Drawing.Color.Black;
    System.Drawing.Color m_window_back_color = System.Drawing.Color.PowderBlue;

    System.Drawing.Color m_fore_color_delivered = System.Drawing.Color.Black;
    System.Drawing.Color m_back_color_delivered = System.Drawing.Color.Khaki;

    System.Drawing.Color m_fore_color_expired = System.Drawing.Color.WhiteSmoke;
    System.Drawing.Color m_back_color_expired = System.Drawing.Color.IndianRed;

#endregion

#region Constructor

    /// <summary>
    /// Constructor
    /// </summary>
    public uc_gift_delivery()
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[UC LOAD] uc_gift_delivery", Log.Type.Message);

      InitializeComponent();

      form_yes_no = new frm_yesno();
      form_yes_no.Opacity = 0.6;

      EventLastAction.AddEventLastAction(this.Controls);

    }

#endregion

#region Private Methods

    /// <summary>
    /// Initialize control resources. (NLS string, images, etc.)
    /// </summary>
    public void InitializeControlResources()
    {
      // NLS Strings

      gb_account.HeaderText = Resource.String("STR_UC_GIFT_DELIVERY_014");       // "Cuenta"
      lbl_account_client_prefix.Text = Resource.String("STR_UC_GIFT_DELIVERY_013") + ":";       // "Nombre"
      lbl_account_number_prefix.Text = Resource.String("STR_UC_GIFT_DELIVERY_014") + ":";       // "Cuenta"
      lbl_card_number_prefix.Text = Resource.String("STR_UC_GIFT_DELIVERY_015") + ":";       // "Tarjeta"
      
      gb_gift.HeaderText = Resource.String("STR_UC_GIFT_DELIVERY_003");  // "Regalo"
      lbl_gift_instance_number_prefix.Text = Resource.String("STR_UC_GIFT_DELIVERY_004") + ":";  // "N�mero"
      lbl_date_request_prefix.Text = Resource.String("STR_UC_GIFT_DELIVERY_006") + ":";        // "Solicitado"
      lbl_date_delivery_prefix.Text = Resource.String("STR_UC_GIFT_DELIVERY_011") + ":";        // "Entregado"
      lbl_gift_value_prefix.Text = Resource.String("STR_UC_GIFT_DELIVERY_016") + ":";        // "Puntos"
      
      btn_ok.Text     = Resource.String("STR_UC_GIFT_DELIVERY_005");

      this.BackColor = WSI.Cashier.CashierStyle.Colors.m_colors_dic[CashierStyle.Colors.ENUM_COLORS.HEX_E3E7E9];
      this.uc_gift_reader.InitializeControlResources();
    }

    #endregion

    /// <summary>
    /// Initialize controls
    /// </summary>
    public void InitControls()
    {
      ClearScreen ();
      EnableDisableButtons (false);

      if ( m_first_time )
      {
        uc_gift_reader.OnVoucherTrackNumberReadEvent += new uc_gift_reader.VoucherNumberReadEventHandler(uc_gift_reader_OnVoucherTrackNumberReadEvent);
        uc_gift_reader.OnVoucherTrackNumberReadingEvent += new uc_gift_reader.VoucherNumberReadingHandler(uc_gift_reader_OnVoucherTrackNumberReadingEvent);

        btn_ok.Click += new EventHandler (btn_button_clicked);
        btn_ok.Top = 446;

        InitializeControlResources();

        m_first_time = false;
      }
    }

    private void LockControls(bool p)
    {
      throw new Exception("The method or operation is not implemented.");
    }

    #region Buttons

    //------------------------------------------------------------------------------
    // PURPOSE : Check whether current gift instance has expired (gift voucher 
    //           can be exchanged) or not.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - true : gift instance has expired and therefore the gift voucher can NOT be exchanged.
    //      - false : gift instance has NOT expired and therefore the gift voucher can be exchanged.
    //
    //   NOTES :

    private Boolean GiftInstanceExpired()
    {
      // Delivered gifts's expiration is not checked out 
      if ( m_gift_instance.Delivered
        || m_gift_instance.RelatedGift.Type == GIFT_TYPE.NOT_REDEEMABLE_CREDIT 
        || m_gift_instance.RelatedGift.Type == GIFT_TYPE.REDEEMABLE_CREDIT 
        || m_gift_instance.RelatedGift.Type == GIFT_TYPE.DRAW_NUMBERS )
      {
        // Delivered means NOT expired
        return false;
      }

      if ( m_gift_instance.ExpirationDateTime > WGDB.Now )
      {
        // Gift instance is NOT expired
        return false;
      }

      // Gift instance is expired
      return true;

    } // GiftInstanceExpired

    /// <summary>
    /// Check entered gift instance voucher
    /// </summary>
    /// <param name="ExternalCode"></param>
    private Boolean LoadGiftByExternalCode (String ExternalCode)
    {
      // Checkings
      //    - The number entered corresponds to a gift voucher 
      //    - The number entered corresponds to a gift voucher issued in the site
      //    - Gift must exist
      //    - Gift can not be expired

      ClearScreen ();

      if (!Barcode.TryParse(ExternalCode, out m_voucher_number))
      {
        return false;
      }
      if (m_voucher_number.Type != BarcodeType.Gift)
      {
        return false;
      }


      //    - The number entered corresponds to a gift voucher issued in the site
      if ( Program.site_id != m_voucher_number.SiteId )
      {
        // "El regalo no corresponde a esta sala."
        frm_message.Show (Resource.String ("STR_UC_GIFT_DELIVERY_007"),
                          Resource.String ("STR_APP_GEN_MSG_WARNING"),
                          MessageBoxButtons.OK,
                          MessageBoxIcon.Warning,
                          this.ParentForm);

        uc_gift_reader.Focus ();

        return false;
      }

      //    - Gift must exist
      m_gift_instance = new GiftInstanceCashier(ExternalCode);
      m_gift_instance.IsCashier = true;

      if ( ! m_gift_instance.VoucherNumberOk || ! m_gift_instance.GiftOk )
      {
        m_gift_instance.Message(GiftInstanceCashier.GIFT_INSTANCE_MSG.GIFT_NOT_FOUND, ParentForm);

        uc_gift_reader.Focus ();

        return false;
      }

      //    - Gift can not be expired
      m_gift_expired = GiftInstanceExpired ();

      if ( m_gift_expired )
      {
        m_gift_instance.Message(GiftInstanceCashier.GIFT_INSTANCE_MSG.GIFT_EXPIRED, ParentForm);

        uc_gift_reader.Focus ();

        return true;
      }

      //    - Gift can not be cancelled  
      m_gift_canceled = (m_gift_instance.Status == GIFT_REQUEST_STATUS.CANCELED);
      if (m_gift_canceled)
      {
        m_gift_instance.Message(GiftInstance.GIFT_INSTANCE_MSG.STATUS_ERROR, ParentForm);

        uc_gift_reader.Focus();

        return true;
      }

      return true;  
    } // LoadGiftByTrackNumber

    //------------------------------------------------------------------------------
    // PURPOSE : Generate a preview version of the voucher to be generated.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    private void PreviewVoucher (Boolean ShowIt)
    {
      SqlTransaction  _sql_trx;
      SqlConnection   _sql_conn;
      String          _voucher_file_name;
      ArrayList       _voucher_list;
     
      _sql_conn = null;
      _sql_trx  = null;

      _voucher_file_name = "";
      web_browser.Navigate (_voucher_file_name);
      
      if ( ! ShowIt )
      {
        return;
      }

      try
      {
        // Get Sql Connection 
        _sql_conn = WSI.Common.WGDB.Connection ();
        _sql_trx = _sql_conn.BeginTransaction ();

        // Gift voucher 
        //    - OperationId, GiftInstanceId and SiteId are not required when Previewing the voucher
        _voucher_list = VoucherBuilder.GiftDelivery (0,                                                   // OperationId
                                                     m_gift_instance.RelatedCard.VoucherAccountInfo (),   // Account Info 
                                                     m_voucher_number.ExternalCode,                       // Gift Number
                                                     m_gift_instance.RelatedGift.GiftName,                // Gift Name
                                                     PrintMode.Preview,
                                                    _sql_trx);

        _voucher_file_name = VoucherPrint.GenerateFileForPreview (_voucher_list);
      }
      catch ( Exception _ex )
      {
        _sql_trx.Rollback ();
        Log.Exception (_ex);
      }

      finally
      {
        if ( _sql_trx != null )
        {
          if ( _sql_trx.Connection != null )
          {
            _sql_trx.Rollback ();
          }

          _sql_trx.Dispose ();
          _sql_trx = null;
        }

        // Close connection
        if ( _sql_conn != null )
        {
          _sql_conn.Close ();
          _sql_conn = null;
        }
      }

      web_browser.Navigate (_voucher_file_name);
    } // PreviewVoucher

    //------------------------------------------------------------------------------
    // PURPOSE : Refresh the displayed voucher
    //
    //  PARAMS :
    //      - INPUT : 
    //          - ShowVoucher : Whether the voucher must be shown or not7
    //
    //      - OUTPUT :
    //
    // RETURNS :
    // 
    //   NOTES :

    private void RefreshData (Boolean ShowVoucher)
    {
      Boolean     _reset = false;
      Boolean     _show_voucher;
      String      _aux_text;
      
      if ( m_voucher_number == null || m_gift_instance == null )
      {
        _reset = true;
      }
      else if ( ! m_gift_instance.VoucherNumberOk || ! m_gift_instance.GiftOk )
      {
        _reset = true;
      }

      if ( _reset )      
      {
        lbl_gift_instance_number.Text = "";
        lbl_date_request.Text = "";
        lbl_date_delivery.Text = "";
        lbl_date_delivery.ForeColor = m_window_fore_color;
        lbl_date_delivery.BackColor = m_window_back_color;
        lbl_account_client.Text = "";
        lbl_account_number.Text = "";
        lbl_card_number.Text = "";
        
        lbl_gift_name.Text  = "-----";
        lbl_gift_value.Text = "-----";

        PreviewVoucher (false);

        return;
      }

      lbl_gift_instance_number.Text = m_voucher_number.ExternalCode;
      lbl_date_request.Text = Format.CustomFormatDateTime(m_gift_instance.RequestDateTime, true);

      lbl_account_client.Text = m_gift_instance.RelatedCard.PlayerTracking.HolderName;
      lbl_account_number.Text = m_gift_instance.RelatedCard.AccountId.ToString();
      lbl_card_number.Text = m_gift_instance.RelatedCard.VisibleTrackdata();
      
      _show_voucher = ShowVoucher;

      _aux_text = Resource.String ("STR_UC_GIFT_DELIVERY_011");                   // "Entregado"

      if ( m_gift_instance.Delivered ) 
      {
        lbl_date_delivery.Text = Format.CustomFormatDateTime(m_gift_instance.DeliveryDateTime, true);
        lbl_date_delivery.ForeColor = m_fore_color_delivered;
        lbl_date_delivery.BackColor = m_back_color_delivered;
      }
      else
      {
        if ( m_gift_expired )
        {
          _aux_text = Resource.String ("STR_UC_GIFT_DELIVERY_019");               // "Expirado"

          lbl_date_delivery.Text = Format.CustomFormatDateTime(m_gift_instance.ExpirationDateTime, true);
          lbl_date_delivery.ForeColor = m_fore_color_expired;
          lbl_date_delivery.BackColor = m_back_color_expired;
          
          _show_voucher = false;
        }
        else if (m_gift_canceled)
        {
          _aux_text = "";

          lbl_date_delivery.Text = Resource.String("STR_UC_GIFT_DELIVERY_023");   // "Cancelado"
          lbl_date_delivery.ForeColor = m_fore_color_expired;
          lbl_date_delivery.BackColor = m_back_color_expired;

          _show_voucher = false;          
        }
        else
        {
          lbl_date_delivery.Text = Resource.String("STR_UC_GIFT_DELIVERY_012");  // "NO"
          lbl_date_delivery.ForeColor = m_window_fore_color;
          lbl_date_delivery.BackColor = m_window_back_color;
        }
      }

      if (String.IsNullOrEmpty(_aux_text))
      {
        lbl_date_delivery_prefix.Text = ""; // for canceled gifts
      }
      else
      {
        lbl_date_delivery_prefix.Text = _aux_text + ":";    // "Entregado" / "Expirado" / "Cancelado"
      }      

      lbl_gift_name.Text  = m_gift_instance.RelatedGift.GiftName;
      lbl_gift_value.Text = m_gift_instance.SpentPoints.ToString (Gift.FORMAT_POINTS) 
                          + " "
                          + Resource.String ("STR_UC_GIFT_DELIVERY_010");      // "puntos"

      // Check whether the voucher must be shown
      PreviewVoucher (_show_voucher);

      EnableDisableButtons (true);

      if (CashierBusinessLogic.ReadCashierStatus(Cashier.SessionId) != CASHIER_STATUS.OPEN
           || m_gift_instance.RelatedGift.Type == GIFT_TYPE.NOT_REDEEMABLE_CREDIT
           || m_gift_instance.RelatedGift.Type == GIFT_TYPE.REDEEMABLE_CREDIT
           || m_gift_instance.RelatedGift.Type == GIFT_TYPE.DRAW_NUMBERS
           || m_gift_instance.Delivered
           || m_gift_expired
           || m_gift_canceled)
      {
        btn_ok.Enabled = false;
      }
    } // RefreshData

#endregion Buttons

#region Methods

#endregion

    public void ClearScreen()
    {
      m_voucher_number = null;
      m_gift_instance  = null;

      RefreshData (false);
    }

    public void InitFocus()
    {
      ClearScreen ();
      uc_gift_reader.Focus();
    }

    public void OnlyInitFocus()
    {
      uc_gift_reader.Focus();
    }

    /// <summary>
    /// Event fired when a voucher number is entered.
    /// </summary>
    /// <param name="TrackNumber">Number read</param>
    /// <param name="IsValid">Voucher read is valid</param>
    private void uc_gift_reader_OnVoucherTrackNumberReadEvent (String      TrackNumber, 
                                                               ref Boolean IsValid)
    {
      // Load and load track number
      IsValid = LoadGiftByExternalCode (TrackNumber);

      if ( ! IsValid )
      {
        EnableDisableButtons (IsValid);
      }
      
      // Show the voucher only if the number is valid
      RefreshData (IsValid);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Change the status of the cashier buttons based on the current cashier 
    //          status (open / not open), the account status (locked / in use) and the 
    //          incoming parameter.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - EnableButtons
    //              - TRUE: try to enable buttons, actually enabled buttons depend on 
    //                      cashier and account status.
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:

    private void EnableDisableButtons (Boolean EnableButtons)
    {
      try
      {
        m_tick_count_tracknumber_read = 0;
        timer1.Enabled = EnableButtons;

        ToggleApparenceCtrl(this, EnableButtons);

        btn_ok.Enabled = EnableButtons;

        try
        {
          frm_container _parent_form = ParentForm as frm_container;
          _parent_form.ChangeLabelStatus();
        }
        catch
        { }
      }
      finally
      {
        // JCO 17-JUL-2014: Moved OnlyInitFocus() to other position because data input deletes the first selected character.
      }
    } // EnableDisableButtons

    private void ToggleApparenceCtrl(Control Ctrl, Boolean Enabled)
    {
      if ( Ctrl.Equals(uc_gift_reader) || Ctrl is Button )
      {
        return;
      }

      if (!Ctrl.Equals(this))
      {
        Ctrl.Enabled = Enabled;
      }

      foreach (Control ctrl in Ctrl.Controls)
      {
        ToggleApparenceCtrl(ctrl, Enabled);
      }

    }

    private void timer1_Tick(object sender, EventArgs e)
    {
      m_tick_count_tracknumber_read += 1;

      //lbl_card_in_use.Visible = (m_is_logged_in || m_is_locked) && ((m_tick_count_tracknumber_read % 2) == 0);

      if ( m_tick_count_tracknumber_read >= CashierBusinessLogic.DURATION_BUTTONS_ENABLED_AFTER_OPERATION )
      {
        EnableDisableButtons (false);

        //JCM 02-MAY-2012 Ensure focus is in the card reader after enable/disable buttons
        OnlyInitFocus();

        return;
      }
    }

    private void uc_gift_reader_OnVoucherTrackNumberReadingEvent ()
    {
      EnableDisableButtons (false);
    }

    private void RefreshAccount()
    {
      //account_watcher.ForceRefresh();
    }

    private void btn_button_clicked (object sender, EventArgs e)
    {
      if ( sender.Equals (btn_ok) )
      {
        btn_ok_Click (sender, e);
      }

      ClearScreen ();
      EnableDisableButtons (false);

      if ( ! uc_gift_reader.Focused )
      {
        uc_gift_reader.Focus ();
      }
    }

    private void btn_ok_Click (object sender, EventArgs e)
    {
      if ( ! m_gift_instance.Delivery () )
      {
        m_gift_instance.Message (GiftInstance.GIFT_INSTANCE_MSG.DELIVERY_ERROR, ParentForm);

        this.Focus();
      }
    }
    
  } // class uc_gift_delivery 
}
