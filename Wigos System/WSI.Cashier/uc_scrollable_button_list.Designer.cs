namespace WSI.Cashier
{
  partial class uc_scrollable_button_list
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(uc_scrollable_button_list));
      this.pnl_buttons = new WSI.Cashier.Controls.uc_round_panel();
      this.btn_right = new WSI.Cashier.Controls.uc_round_button();
      this.btn_left = new WSI.Cashier.Controls.uc_round_button();
      this.SuspendLayout();
      // 
      // pnl_buttons
      // 
      this.pnl_buttons.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.pnl_buttons.BackColor = System.Drawing.Color.Transparent;
      this.pnl_buttons.CornerRadius = 1;
      this.pnl_buttons.HeaderBackColor = System.Drawing.Color.Empty;
      this.pnl_buttons.HeaderForeColor = System.Drawing.Color.Empty;
      this.pnl_buttons.HeaderHeight = 0;
      this.pnl_buttons.HeaderSubText = null;
      this.pnl_buttons.HeaderText = null;
      this.pnl_buttons.Location = new System.Drawing.Point(30, 0);
      this.pnl_buttons.Margin = new System.Windows.Forms.Padding(2);
      this.pnl_buttons.Name = "pnl_buttons";
      this.pnl_buttons.PanelBackColor = System.Drawing.Color.Empty;
      this.pnl_buttons.Size = new System.Drawing.Size(462, 46);
      this.pnl_buttons.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NONE;
      this.pnl_buttons.TabIndex = 8;
      // 
      // btn_right
      // 
      this.btn_right.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_right.BackColor = System.Drawing.Color.Transparent;
      this.btn_right.CornerRadius = 0;
      this.btn_right.FlatAppearance.BorderSize = 0;
      this.btn_right.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_right.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_right.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_right.Image = ((System.Drawing.Image)(resources.GetObject("btn_right.Image")));
      this.btn_right.Location = new System.Drawing.Point(481, -3);
      this.btn_right.Margin = new System.Windows.Forms.Padding(0);
      this.btn_right.Name = "btn_right";
      this.btn_right.Size = new System.Drawing.Size(50, 50);
      this.btn_right.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.NONE;
      this.btn_right.TabIndex = 37;
      this.btn_right.UseVisualStyleBackColor = false;
      // 
      // btn_left
      // 
      this.btn_left.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
      this.btn_left.BackColor = System.Drawing.Color.Transparent;
      this.btn_left.CornerRadius = 0;
      this.btn_left.FlatAppearance.BorderSize = 0;
      this.btn_left.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_left.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_left.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_left.Image = ((System.Drawing.Image)(resources.GetObject("btn_left.Image")));
      this.btn_left.Location = new System.Drawing.Point(-13, -2);
      this.btn_left.Margin = new System.Windows.Forms.Padding(0);
      this.btn_left.Name = "btn_left";
      this.btn_left.Size = new System.Drawing.Size(50, 50);
      this.btn_left.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.NONE;
      this.btn_left.TabIndex = 36;
      this.btn_left.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.btn_left.UseVisualStyleBackColor = false;
      // 
      // uc_scrollable_button_list
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.Color.Transparent;
      this.Controls.Add(this.pnl_buttons);
      this.Controls.Add(this.btn_right);
      this.Controls.Add(this.btn_left);
      this.Margin = new System.Windows.Forms.Padding(2);
      this.Name = "uc_scrollable_button_list";
      this.Size = new System.Drawing.Size(522, 46);
      this.SizeChanged += new System.EventHandler(this.uc_scrollable_button_list_SizeChanged);
      this.ResumeLayout(false);

    }

    #endregion

    private WSI.Cashier.Controls.uc_round_panel pnl_buttons;
    private WSI.Cashier.Controls.uc_round_button btn_left;
    private WSI.Cashier.Controls.uc_round_button btn_right;
  }
}
