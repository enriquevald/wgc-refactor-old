﻿//------------------------------------------------------------------------------
// Copyright © 2007-2014 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_gt_card_reader.cs
// 
//   DESCRIPTION: Implements the uc_popup_helper user control
//                Class: uc_popup_helper
//
//        AUTHOR: David Hernández
// 
// CREATION DATE: 28-MAR-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 28-MAR-2017 DHA     First release.

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace WSI.Cashier
{
  public class uc_popup_helper : IDisposable
  {

    public delegate void CloseHandler();
    public event CloseHandler OnClose;

    private readonly Control m_control;
    private readonly ToolStripDropDown m_tsdd;
    private readonly Panel m_host_panel;

    public uc_popup_helper(Control Control)
    {
      m_host_panel = new Panel();
      m_host_panel.Padding = Padding.Empty;
      m_host_panel.Margin = Padding.Empty;
      m_host_panel.TabStop = false;
      m_host_panel.BorderStyle = BorderStyle.None;
      m_host_panel.BackColor = Color.Transparent;

      m_tsdd = new ToolStripDropDown();
      m_tsdd.CausesValidation = false;
      m_tsdd.AutoClose = true;

      m_tsdd.Padding = Padding.Empty;
      m_tsdd.Margin = Padding.Empty;
      m_tsdd.Opacity = 1;

      m_control = Control;
      m_control.CausesValidation = false;
      m_control.Resize += MControlResize;

      m_host_panel.Controls.Add(m_control);

      m_tsdd.Padding = Padding.Empty;
      m_tsdd.Margin = Padding.Empty;

      m_tsdd.MinimumSize = m_tsdd.MaximumSize = m_tsdd.Size = Control.Size;

      m_control.Disposed += m_control_Disposed;
      m_tsdd.Closed += m_tsdd_Closed;
      
      m_tsdd.Items.Add(new ToolStripControlHost(m_host_panel));
    }

    void m_tsdd_Closed(object sender, ToolStripDropDownClosedEventArgs e)
    {
      if (OnClose != null)
      {
        OnClose();
      }
    }

    void m_control_Disposed(object sender, EventArgs e)
    {
      Close();
    }

    private void ResizeWindow()
    {
      m_tsdd.MinimumSize = m_tsdd.MaximumSize = m_tsdd.Size = m_control.Size;
      m_host_panel.MinimumSize = m_host_panel.MaximumSize = m_host_panel.Size = m_control.Size;
    }

    private void MControlResize(object sender, EventArgs e)
    {
      ResizeWindow();
    }

    /// <summary>
    /// Display the popup and keep the focus
    /// </summary>
    /// <param name="pParentControl"></param>
    public void Show(Point Location)
    {
      m_tsdd.Show(Location);
      m_control.Focus();
    }

    public void Close()
    {
      m_tsdd.Close();
    }

    public void Dispose()
    {
      m_control.Resize -= MControlResize;

      m_tsdd.Dispose();
      m_host_panel.Dispose();
    }
  }
}
