//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_TITO_handpays.cs
// 
//   DESCRIPTION: Implements the following classes:
//                1. frm_TITO_handpays: List of candidate tickets to apply handpay
//
// CREATION DATE: 12-NOV-2013
//
// REVISION HISTORY: 
// 
// Date        Author     Description
// ----------- ------     ----------------------------------------------------------
// 09-SEP-2014 MPO        First release.
// 13-OCT-2014 MPO        WIG-1475: Progressive jackpots exception.
// 15-OCT-2014 JBC        Fixed Bug WIG-1474: Now textbox works like payment order.
// 23-OCT-2014 LEM        Fixed Bug WIG-1561: Wrong taxes apply if authorization required.
// 03-NOV-2014 MPO        WIG-1620: Show incorrect message when cancel payment
// 27-NOV-2014 MPO        WIG-1769: The payment permission always is needed. Incorrect use of Cashier.HandPays LimitToAllowPayment
// 02-DEC-2014 MPO        Fixed bugs WIG-1793 / WIG-1788: need check status after update
// 23-DEC-2014 MPO        Fixed bugs WIG-1874
// 16-JAN-2016 JMM & MPO  Heed on partial pay field comming from the 1B SAS poll
// 16-FEB-2015 MPO        WIG-2065: Progressive level not reported correctly
// 03-MAR-2015 YNM        WIG-2118: Handpaid: deleting "base of retention", the amount is erased
// 10-MAR-2015 FOS        TASK-333: Televisa: Pagos Manuales de entrada Manual
// 10-APR-2015 DLL        WIG-2199: Incorrect enabled buttons when handpay is authorize
// 19-AUG-2015 YNM        Fixed Bug 3826: Parameter Cashier.HandPays-MaxAllowedManualEntry wasn't limiting a handpay by manual entry
// 11-NOV-2015 SGB        Backlog Item WIG-5880: Change designer
// 11-NOV-2015 FOS        Product Backlog Item: 3709 Payment tickets & handpays
// 23-NOV-2015 FOS        Bug 6771:Payment tickets & handpays
// 25-NOV-2015 FOS        Product Backlog Item: 3709 Payment tickets & handpays, applied new design
// 01-DEC-2015 FOS        Fixed Bug 7205: Protect taxWaiverMode and rename function ApplyTaxCollection to EnableTitoTaxWaiver
// 02-DEC-2015 FOS        Fixed Bug 7208: Incorrect Tax Value on Handpays
// 18-DEC-2015 FOS        Fixed Bug 6769: It doesn't delete taxes when TITO tickets are paid
// 19-JAN-2016 FOS        Backlog Item 7969: Apply Taxes in handpay authorization
// 29-JAN-2016 RAB        Bug 8577:Cajero - Tipo de pago Manual de Entrada Manual - DisableTypeSelection=1 no se cumple
// 22-FEB-2016 LTC        Product Backlog Item: 7440 Disputes and Handpays division - New Button functionality
// 24-FEB-2016 EOR        Product Backlog Item 7402:SAS20: Add Comments HandPays
// 11-MAR-2016 JPJ        Fixed Bug 8301: Taxwaiver button isn't enabled when terminal handpay 
// 21-MAR-2016 ETP        Fixed Bug 10449: Only "other" Option appears in combo.
// 23-MAR-2016 JML        Fixed Bug 10877:Dual currency - Play sessions: "Jackpot" is reported in national currency for terminals configured in exchange currency
// 29-MAR-2016 RAB        Product Backlog Item 10789:Floor Dual Currency: manual input manual payments
// 11-APR-2016 FOS        Fixed Bug 11485: handPays generated to machine, it doesn't apply the General Param.
// 14-APR-2016 FOS        Fixed Bug 11777: when cancel a handpay payment the funcionality doesn't work correctly
// 21-APR-2016 FOS        Fixed Bug 11698: Jackpot by level
// 11-JUL-2016 RAB        Bug 10719: TITO Mode - Floor Dual Currency - Payment with manual input: does the change to currency
// 20-JUL-2016 ETP        Fixed Bug 15749: Ref. 561D: Base has to be updated with amount value if it has not been modificated
// 19-SEP-2016 FGB        PBI 15985: Exped System: Cashier: Check if the payment/sale can be done
// 10-GEN-2017 XGJ        Bug 16372: Problemas con Jackpots
// 16-JAN-2017 ETP        Fixed Bug 21609: Base tax is not automatically updated.
// 06-FEB-2017 ETP        Fixed Bug 24225: Base tax applied in cashless
// 08-MAR-2017 FGB        PBI 25268: Third TAX - Payment Voucher
// 22-MAR-2017 FGB        PBI 25951: Third TAX - Reports
// 06-JUL-2017 DHA        WIGOS-3419 - Manual HP's with no taxes are having taxes applied when cashing them
// 19-JUL-2017 DHA        Bug 28857:WIGOS-3776 Manual handpay button can generate several handpays before action exiting
// 25-JUL-2017 DHA        Bug 28934:WIGOS-3454 [Ticket #6260] tax withholding
// 08-AUG-2017 ATB        Bug 29252: The voucher of a paid "Handpay" is displaying "An�nimo" in the field "Cliente" when the user already introduced their account.
// 09-AUG-2017 ATB        Bug 29252: The voucher of a paid "Handpay" is displaying "An�nimo" in the field "Cliente" when the user already introduced their account (WIGOS-4288, WIGOS-4289)
// 09-AUG-2017 ATB        Bug 29252: The voucher of a paid "Handpay" is displaying "An�nimo" in the field "Cliente" when the user already introduced their account (WIGOS-4352)
// 23-NOV-2017 RAB        Bug 30896:WIGOS-6573 [Ticket #10452] Falla al pagar pagos manuales de entrada manual
// 01-DEC-2017 RAB        Bug 30896:WIGOS-6573 [Ticket #10452] Falla al pagar pagos manuales de entrada manual
// 21-FEB-2018 RGR        Bug 31632:WIGOS-6248 Cashier: Manual handpay has registered an error on cashier log when it is not it 
// 23-FEB-2018 DHA        Bug 31720:WIGOS-8188 [Ticket #12441] Pagos manuales varian en funci�n de las pantallas
// 24-APR-2018 JML        Fixed Bug 32424:WIGOS-7573 [Ticket #11669] Handpay Discrepancy
// 04-JUL-2018 FOS        Bug 33488:WIGOS-13374 Extra Handpay confirmation screen at the end of Handpay payment
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WSI.Common;
using WSI.Common.TITO;
using WSI.Cashier.TITO;
using WSI.Cashier.Controls;

namespace WSI.Cashier
{
  public partial class frm_handpay_action : frm_base
  {
    public enum ENTRY_TYPE
    {
      DEFAULT = 0,
      MANUAL = 1,
    }

    private const Int32 TYPE_JACKPOT_PROGRESSIVE_40 = 9999;
    private const Int32 TYPE_JACKPOT_PROGRESSIVE = 8888;
    private const Int32 JACKPOT_MAX_PRIZE_LEVEL = 64;
    private const Int32 BOTTOM_MARGIN = 100;
    private const Int32 CALC_SCROLL_RIGHT = 380;

    private ENTRY_TYPE m_entry_type = ENTRY_TYPE.DEFAULT;
    private frm_yesno form_yes_no;
    private Form m_parent;
    private HandPay m_handpay;
    private Int32 m_initial_status = 0;
    private Boolean m_initializing = true;
    private frm_generic_amount_input m_amount_input = null;
    private Int64 m_current_progressive_id = 0;
    private Control m_current_amount_control;
    private static String m_decimal_str = System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencyDecimalSeparator;
    private Boolean m_can_edit = false;
    private HANDPAY_STATUS m_taxes = 0;
    private Boolean m_is_tito = WSI.Common.TITO.Utils.IsTitoMode();

    private frm_catalog m_form_catalog;
    private Boolean m_is_apply_tax;
    private Boolean m_is_apply_tax_result;
    private Boolean m_is_apply_tax_initial;
    private Int64 m_account_operation_reason_id;
    private String m_account_operation_comment;
    private Int64 m_account_id;
    private Decimal m_previous_amount;
    private Int32 m_initial_x_form_location;
    private Int32 m_initial_y_form_location;
    private Int32 m_x_form_location_amount_input;
    private Boolean m_is_new_handpay;
    private Boolean m_enabled_floor_dual_currency;
    private String m_iso_code_terminal_selected;

    // LTC 22-FEB-2016
    private Boolean m_is_Dispute;
    private String m_comment_handpay;
    private Decimal m_base_text_value;

      // DLM  12-JUN-2018 
    private frm_handpays parentHandPay; 
    private frm_TITO_handpays parentHandTITO;
    private uc_ticket_view parentTicketView;
    private uc_card parentCard;

    public frm_handpay_action(
        ENTRY_TYPE EntryType,
        Int64 HandpayId,
        CardData Card,
        frm_handpays parentHandPay = null,
        frm_TITO_handpays parentHandTITO = null,
        uc_ticket_view parentTicketView = null,
        uc_card parentCard = null,
        Boolean IsDispute = false //LTC 22-FEB-2016
        
      )
    {
      // LTC 22-FEB-2016
      m_is_Dispute = IsDispute;
      m_base_text_value = 0;

      if (HandpayId != 0)
      {
        m_is_new_handpay = false;
        CardData _virtual_card;
        Handpays _handpays_db;

        _virtual_card = new CardData();
        if (!BusinessLogic.LoadVirtualAccount(_virtual_card))
        {
          Log.Error(String.Empty);
        }

        Handpays.GetHandpayFromHandpayId(HandpayId, out _handpays_db);

        m_handpay = new HandPay(_virtual_card);
        m_handpay.TerminalId = _handpays_db.Hp_Terminal_Id;
        m_handpay.TerminalName = _handpays_db.Hp_Te_Name;
        m_handpay.TerminalProvider = _handpays_db.Hp_Te_Provider_Id;
        m_handpay.Amount = _handpays_db.Hp_Amount;
        m_handpay.Type = (HANDPAY_TYPE)_handpays_db.Hp_Type;
        m_handpay.DateTime = _handpays_db.Hp_Datetime;
        m_handpay.SiteJackpotName = _handpays_db.Hp_Site_Jackpot_Name;
        m_handpay.SiteJackpotAwardedOnTerminalId = _handpays_db.Hp_Site_Jackpot_Awarded_On_Terminal_Id;
        m_handpay.Level = _handpays_db.Hp_Level;
        m_handpay.ProgressiveId = _handpays_db.Hp_Progressive_Id;
        m_handpay.HandpayId = _handpays_db.Hp_Id;
        m_handpay.Status = _handpays_db.Hp_Status;
        m_handpay.TaxAmount = _handpays_db.Hp_Tax_Amount;
        m_handpay.TaxBaseAmount = _handpays_db.Hp_Tax_Base_Amount;
        m_handpay.TaxPct = _handpays_db.Hp_Tax_Pct;
        m_handpay.PartialPay = _handpays_db.Hp_Partial_Pay;
        m_handpay.Amt0 = _handpays_db.Hp_Amt_0;
        m_handpay.Cur0 = _handpays_db.Hp_Cur_0;
        m_handpay.Amt1 = _handpays_db.Hp_Amt_1;
        m_handpay.Cur1 = _handpays_db.Hp_Cur_1;
        m_previous_amount = m_handpay.Amount;
        m_base_text_value = 0;

        LoadPrintPlayedAmountInHandpay();

        if (Tax.EnableTitoTaxWaiver(true))
        {
          Int64 _account_operation_reason_id;
          string _account_operation_reason_comment;

          bool _is_exist_reason = AccountOperations.GetReasonAndComment((Int64)_handpays_db.Hp_Operation_Id, out _account_operation_reason_id, out _account_operation_reason_comment);
          m_account_operation_reason_id = _account_operation_reason_id;
          m_account_operation_comment = _account_operation_reason_comment;
        }
      }
      else
      {
        if (Card == null)
        {
          m_handpay = new HandPay(new CardData());
        }
        else
        {
          m_handpay = new HandPay(Card);
        }

        m_is_new_handpay = true;
      }

      m_entry_type = EntryType;
      m_initial_status = m_handpay.Status;
      m_taxes = (HANDPAY_STATUS)m_handpay.Status & HANDPAY_STATUS.MASK_TAX;

      InitializeComponent();
      
      this.parentHandPay = parentHandPay;
      this.parentHandTITO = parentHandTITO;
      this.parentTicketView = parentTicketView;
      this.parentCard = parentCard;

      this.FormTitle = Resource.String("STR_HANDPAY_ACTION_16"); // HANDPAY

      if (EntryType == ENTRY_TYPE.MANUAL)
      {
        // LTC 22-FEB-2016
        if (IsDispute)
        {
          this.FormTitle = Resource.String("STR_HANDPAY_ACTION_29"); // 
        }
        else
        {
          this.FormTitle = Resource.String("STR_HANDPAY_ACTION_01"); // 
        }

        this.txt_amount.KeyPress += new KeyPressEventHandler(txt_amount_KeyPress);
        this.txt_amount.PreviewKeyDown += new PreviewKeyDownEventHandler(txt_amount_PreviewKeyDown);
        this.txt_amount.MouseClick += new MouseEventHandler(this.txt_amount_MouseClick);
        this.txt_amount.Enter += new EventHandler(this.txt_amount_Enter);
        this.txt_amount.Leave += new System.EventHandler(this.txt_amount_Leave);
        this.txt_amount.Click += txt_amount_Click;
      }

      if (Handpays.StatusActivated(m_initial_status, HANDPAY_STATUS.PENDING))
      {
        this.txt_base_amount.KeyPress += new KeyPressEventHandler(txt_amount_KeyPress);
        this.txt_base_amount.PreviewKeyDown += new PreviewKeyDownEventHandler(txt_amount_PreviewKeyDown);
        this.txt_base_amount.MouseClick += new MouseEventHandler(this.txt_amount_MouseClick);
        this.txt_base_amount.Enter += new EventHandler(this.txt_amount_Enter);
        this.txt_base_amount.Leave += new System.EventHandler(this.txt_amount_Leave);
        this.txt_base_amount.Click += txt_base_amount_Click;
      }

      // NLS
      this.Text = Resource.String("STR_HANDPAY_ACTION_01");                         // FORM
      this.gb_terminal.HeaderText = Resource.String("STR_HANDPAY_ACTION_02");             // TERMINAL
      this.gp_type.HeaderText = Resource.String("STR_HANDPAY_ACTION_03");                 // HANDPAY TYPE
      this.lbl_level.Text = Resource.String("STR_HANDPAY_ACTION_04");               // LEVEL
      this.lbl_progresive.Text = Resource.String("STR_HANDPAY_ACTION_19");          // PROGRESIVE
      this.lbl_amount.Text = Resource.String("STR_HANDPAY_ACTION_05");              // AMOUNT
      this.lbl_to_pay.Text = Resource.String("STR_HANDPAY_ACTION_07");              // TO PAY
      this.btn_athorization.Text = Resource.String("STR_HANDPAY_ACTION_08");        // AUTHORIZATION
      this.btn_payment.Text = Resource.String("STR_HANDPAY_ACTION_09");             // PAY
      this.btn_close.Text = Resource.String("STR_HANDPAY_ACTION_10");               // CANCEL
      this.lbl_tax_base.Text = Resource.String("STR_HANDPAY_ACTION_27");            // WITHHOLDING BASE
      this.lbl_created_amount.Text = Resource.String("STR_FRM_VOUCHERS_REPRINT_100");   // MACHINE AMOUNT

      if (Handpays.StatusActivated(m_initial_status, HANDPAY_STATUS.VOIDED))
      {
        this.btn_voided.Text = Resource.String("STR_HANDPAY_ACTION_22");
      }
      else
      {
        this.btn_voided.Text = Resource.String("STR_HANDPAY_ACTION_23");
      }

      if (Handpays.StatusActivated(m_initial_status, HANDPAY_STATUS.AUTHORIZED))
      {
        this.btn_athorization.Text = Resource.String("STR_HANDPAY_ACTION_24");
      }
      else
      {
        this.btn_athorization.Text = Resource.String("STR_HANDPAY_ACTION_08");
      }

      InitControls();
      LoadHpTypesCombo();
      form_yes_no = new frm_yesno();
      form_yes_no.Opacity = 0.6;
    }
    void txt_created_amount_Click(object sender, EventArgs e)
    {
      ShowPadNumber((Control)sender);
    }

    void txt_base_amount_Click(object sender, EventArgs e)
    {
      ShowPadNumber((Control)sender);
    }

    void txt_amount_Click(object sender, EventArgs e)
    {
      ShowPadNumber((Control)sender);
    }

    private void LoadPrintPlayedAmountInHandpay()
    {
      Boolean _print_played_amount;
      Currency _played_amount;
      String _denomination_str;
      String _denomination_value;
      Int64 _virtual_terminal_account_id;

      _print_played_amount = GeneralParam.GetBoolean("Cashier.Voucher", "Handpay.PrintPlayedAmount");
      if (_print_played_amount)
      {
        _virtual_terminal_account_id = Accounts.GetOrCreateVirtualAccount(m_handpay.TerminalId);

        if (!Handpays.GetHandpaySessionPlayedAmount(_virtual_terminal_account_id
                                                  , m_handpay.Type == HANDPAY_TYPE.SITE_JACKPOT ? m_handpay.SiteJackpotAwardedOnTerminalId : m_handpay.TerminalId
                                                  , m_handpay.DateTime
                                                  , out _played_amount
                                                  , out _denomination_str
                                                  , out _denomination_value))
        {
          _played_amount = -1;
        }

        m_handpay.SessionPlayedAmount = _played_amount;
        m_handpay.DenominationStr = _denomination_str;
        m_handpay.DenominationValue = _denomination_value;
      }
      else
      {
        m_handpay.SessionPlayedAmount = -1;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Init and Show the form.
    //
    //  PARAMS :
    //      - INPUT :
    //          - ParentForm
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public void Show(Form Parent)
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_handpay_action", Log.Type.Message);
      m_parent = Parent;

      try
      {
        form_yes_no.Show(Parent);
        this.ShowDialog(form_yes_no);
        form_yes_no.Hide();

        Misc.WriteLog("[FORM CLOSE] frm_handpay_action", Log.Type.Message);
        this.Close();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        form_yes_no.Hide();
      }
    }

    private void btn_payment_Click(object sender, EventArgs e)
    {
      TITO_HandPay _tito_handpay;
      String _error_str;
      Control _ctrl;
      Boolean _success;
      List<ProfilePermissions.CashierFormFuncionality> _permission_list = new List<ProfilePermissions.CashierFormFuncionality>();

      btn_payment.Enabled = false;
      btn_athorization.Enabled = false;
      btn_voided.Enabled = false;
      if (!Handpays.IsPayable(m_initial_status, m_handpay.Amount))
      {
        frm_message.Show(Resource.String("STR_HANDPAY_PAYMENT_NOT_ALLOWED").Replace("\\r\\n", "\r\n"),
                         Resource.String("STR_FRM_HANDPAYS_021"),
                         MessageBoxButtons.OK,
                         MessageBoxIcon.Information,
                         this.ParentForm);

        btn_payment.Enabled = true;
        btn_athorization.Enabled = true;
        btn_voided.Enabled = true;
        return;
      }

      //DRV 21-NOV-2014: If handpay has an associated ticket, must pay the ticket, not the handpay
      if (m_is_tito && Common.TITO.HandPay.HasHandpayTicket(m_handpay.HandpayId))
      {
        frm_message.Show(Resource.String("STR_UC_CARD_USER_MSG_GENERIC_ERROR"),
                         Resource.String("STR_APP_GEN_MSG_ERROR"),
                         MessageBoxButtons.OK,
                         MessageBoxIcon.Warning,
                         this.ParentForm);

        btn_payment.Enabled = true;
        btn_athorization.Enabled = true;
        btn_voided.Enabled = true;
        return;
      }

      if (m_is_tito && m_handpay.TaxBaseAmount > m_handpay.Amount)
      {
        frm_message.Show(Resource.String("STR_HANDPAY_TAX_BASE_ERROR_001").Replace("\\r\\n", "\r\n"),
                         Resource.String("STR_APP_GEN_MSG_ERROR"),
                         MessageBoxButtons.OK,
                         MessageBoxIcon.Warning,
                         this.ParentForm);
        txt_base_amount.Focus();

        btn_payment.Enabled = true;
        btn_athorization.Enabled = true;
        btn_voided.Enabled = true;
        return;
      }

      // When the account id is registered, it means the user wants to do the handpay with the card data
      if (this.m_entry_type == ENTRY_TYPE.MANUAL)
      {
        if (!CheckAndGetDataFromControls(out _error_str, out _ctrl))
        {
          frm_message.Show(_error_str, Resource.String("STR_APP_GEN_MSG_WARNING"), MessageBoxButtons.OK, MessageBoxIcon.Warning, this.ParentForm);
          _ctrl.Focus();

          btn_payment.Enabled = true;
          btn_athorization.Enabled = true;
          btn_voided.Enabled = true;
          return;
        }

        if (m_is_tito)
        {
          // In TITO the payment of the handpay does not have to consider the balance of the account
          m_handpay.Card.AccountBalance = MultiPromos.AccountBalance.Zero;
          m_handpay.Card.CurrentBalance = 0;
          m_handpay.Card.NumberOfPromosByCreditType.Clear();
        }

        _permission_list.Add(ProfilePermissions.CashierFormFuncionality.ManualHandPay);
      }
      else
      {
        // Getting the card data to assign to the handpay
        if (this.AccountId > 0)
        {
          CardData _card_data;
          _card_data = new CardData();
          try
          {
            using (DB_TRX _trx = new DB_TRX())
            {
              // Get card data info
              if (CardData.DB_CardGetAllData(this.AccountId, _card_data, _trx.SqlTransaction))
              {                
                _trx.Commit();
              }
            }
          }
          catch (Exception _ex)
          {
            Log.Exception(_ex);
          }
          if (m_is_tito)
          {
            // In TITO the payment of the handpay does not have to consider the balance of the account
            _card_data.AccountBalance = MultiPromos.AccountBalance.Zero;
            _card_data.CurrentBalance = 0;
            _card_data.NumberOfPromosByCreditType.Clear();
          }
          m_handpay.Card = _card_data;
        }
        _permission_list.Add(ProfilePermissions.CashierFormFuncionality.CardHandPay);
      }

      // Check if the slot attendant is authorized to pay if is tito mode.
      if (Handpays.StatusActivated(m_initial_status, HANDPAY_STATUS.AUTHORIZED))
      {
        _permission_list.Add(ProfilePermissions.CashierFormFuncionality.SlotAttendantPayAuthorization);
      }

      if (!ProfilePermissions.CheckPermissionList(_permission_list, ProfilePermissions.TypeOperation.RequestPasswd, this, 0, out _error_str))
      {

        btn_payment.Enabled = true;
        btn_athorization.Enabled = true;
        btn_voided.Enabled = true;
        return;
      }

      // Check whether the account is available or not
      if (!m_handpay.CheckPayment())
      {
        m_handpay.HandpayMsg(Handpays.HANDPAY_MSG.REGISTER_NOT_ALLOWED, this, m_is_apply_tax);

        btn_payment.Enabled = true;
        btn_athorization.Enabled = true;
        btn_voided.Enabled = true;
        return;
      }

      // LTC 22-FEB-2016
      using (frm_catalog _frm_comment = new frm_catalog(m_is_Dispute, true))
      {
        _frm_comment.Show(this, out m_comment_handpay);
      }

      //EOR 24-FEB-2016 Add Comments HandPays
      m_handpay.IsDispute = m_is_Dispute;
      m_handpay.AccountOperationCommentHandPays = m_comment_handpay;

      if (m_is_tito)
      {
        if (!btn_apply_tax.Visible && !m_is_apply_tax_initial)
        {
          m_is_apply_tax_initial = true;
        }

        if (m_is_new_handpay)
        {
          if (!m_handpay.HandpayMsg(Handpays.HANDPAY_MSG.REGISTER_CONF_TITO, this, (m_is_apply_tax_initial && m_handpay.TaxAmount > 0)))
          {
            btn_payment.Enabled = true;
            btn_athorization.Enabled = true;
            btn_voided.Enabled = true;
            return;
          }
        }
        else
        {
          if (!m_handpay.HandpayMsg(Handpays.HANDPAY_MSG.REGISTER_CONF_TITO, this, m_handpay.TaxAmount > 0))
          {
            btn_payment.Enabled = true;
            btn_athorization.Enabled = true;
            btn_voided.Enabled = true;
            return;
          }
        }
      }
      else
      {
        if (!m_handpay.HandpayMsg(Handpays.HANDPAY_MSG.REGISTER_CONF, this, true))
        {
          btn_payment.Enabled = true;
          btn_athorization.Enabled = true;
          btn_voided.Enabled = true;
          return;
        }
            /* dlm 12/06/2018 fix: refresh grid when exist new manual pay */
      }

      m_handpay.SetPaidStatus();
      if (m_is_tito)
      {
        Boolean _cancelled;

        _cancelled = false;
        _tito_handpay = new TITO_HandPay(this);
        if (this.AccountId > 0)
        {
          _tito_handpay.AccountId = this.AccountId;
        }

        m_handpay.AccountOperationReasonId = m_account_operation_reason_id;
        m_handpay.AccountOperationComment = m_account_operation_comment;

        if (m_is_new_handpay)
        {
          _success = _tito_handpay.PayHandpay(m_handpay, m_handpay.DateTime, m_is_apply_tax_initial, out  _cancelled);
        }
        else
        {
          _success = _tito_handpay.PayHandpay(m_handpay, m_handpay.DateTime, m_handpay.TaxAmount > 0, out  _cancelled);
        }

        if (_cancelled)
        {
          btn_payment.Enabled = true;
          btn_athorization.Enabled = true;
          btn_voided.Enabled = true;
          return;
        }
      }
      else
      {
        _success = m_handpay.Register(WGDB.Now);
      }

      if (!_success)
      {
        m_handpay.Status = m_initial_status;
        if (m_handpay.HandpayErrorMsg != Handpays.HANDPAY_MSG.REGISTER_ERROR_ALREADY_PAID && m_handpay.HandpayErrorMsg != Handpays.HANDPAY_MSG.REGISTER_ERROR_VOIDED)
        {
          m_handpay.HandpayErrorMsg = Handpays.HANDPAY_MSG.REGISTER_ERROR;
        }

        m_handpay.HandpayMsg(m_handpay.HandpayErrorMsg, this.ParentForm, m_is_apply_tax_initial);
      }
      else
      {
        /* DLM 12-JUN-2018 fix manual new entry */ 
       
        if (this.m_parent != null)
        {                  
          if (parentHandPay != null)
          {
              parentHandPay.refreshHandPays();
          }
          else if (parentHandTITO != null)
          {
              parentHandTITO.refreshHandPays();
          }
        }
      }

      btn_payment.Enabled = true;
      btn_athorization.Enabled = true;
      btn_voided.Enabled = true;
      btn_close_Click(null, null);
    }

    private void LoadHpTypesCombo()
    {
      DataTable _dt_hp_types;
      HANDPAY_TYPE _type = HANDPAY_TYPE.UNKNOWN;
      DataRow _dr;
      String _name;

      _dt_hp_types = new DataTable();
      _dt_hp_types.Columns.Add("ID", Type.GetType("System.Int32"));
      _dt_hp_types.Columns.Add("NAME", Type.GetType("System.String"));

      if (m_entry_type == ENTRY_TYPE.MANUAL)
      {
        //FOS 10-03-2015
        //RAB 29-JAN-2016        
        //LTC 22-FEB-2016
        if (!Handpays.IsDisableTypeSelection())
        {
          _dr = _dt_hp_types.NewRow();
          _dr[0] = HANDPAY_TYPE.MANUAL_CANCELLED_CREDITS;
          _dr[1] = Handpays.HandpayTypeString((HANDPAY_TYPE)_dr[0], 0);
          _dt_hp_types.Rows.Add(_dr);

          _dr = _dt_hp_types.NewRow();
          _dr[0] = HANDPAY_TYPE.MANUAL_JACKPOT;
          _dr[1] = Handpays.HandpayTypeString((HANDPAY_TYPE)_dr[0], 0);
          _dt_hp_types.Rows.Add(_dr);

          _dr = _dt_hp_types.NewRow();
          _dr[0] = TYPE_JACKPOT_PROGRESSIVE;
          _dr[1] = Handpays.HandpayTypeString((HANDPAY_TYPE)HANDPAY_TYPE.MANUAL_JACKPOT, 1);
          _dt_hp_types.Rows.Add(_dr);

          _dr = _dt_hp_types.NewRow();
          _dr[0] = TYPE_JACKPOT_PROGRESSIVE_40;
          _dr[1] = Handpays.HandpayTypeString((HANDPAY_TYPE)HANDPAY_TYPE.MANUAL_JACKPOT, JACKPOT_MAX_PRIZE_LEVEL);
          _dt_hp_types.Rows.Add(_dr);
        }

        // LTC 22-FEB-2016
        if (Handpays.IsDisableTypeSelection() || m_is_Dispute)
        {
          _dr = _dt_hp_types.NewRow();
          _dr[0] = HANDPAY_TYPE.MANUAL_OTHERS;
          _dr[1] = Handpays.HandpayTypeString((HANDPAY_TYPE)_dr[0], 0);
          _dt_hp_types.Rows.Add(_dr);
        }
      }
      else
      {
        foreach (Int32 _value in Enum.GetValues(_type.GetType()))
        {
          _name = Handpays.HandpayTypeString((HANDPAY_TYPE)_value, 0);

          if (!String.IsNullOrEmpty(_name))
          {
            _dr = _dt_hp_types.NewRow();
            _dr[0] = _value;
            _dr[1] = _name;
            _dt_hp_types.Rows.Add(_dr);
          }
        }

        _dr = _dt_hp_types.NewRow();
        _dr[0] = TYPE_JACKPOT_PROGRESSIVE;
        _dr[1] = Handpays.HandpayTypeString((HANDPAY_TYPE)HANDPAY_TYPE.MANUAL_JACKPOT, 1);
        _dt_hp_types.Rows.Add(_dr);

        _dr = _dt_hp_types.NewRow();
        _dr[0] = TYPE_JACKPOT_PROGRESSIVE_40;
        _dr[1] = Handpays.HandpayTypeString((HANDPAY_TYPE)HANDPAY_TYPE.MANUAL_JACKPOT, JACKPOT_MAX_PRIZE_LEVEL);
        _dt_hp_types.Rows.Add(_dr);
      }

      cb_hp_type.DataSource = _dt_hp_types;
      cb_hp_type.ValueMember = "ID";
      cb_hp_type.DisplayMember = "NAME";

      // LTC 22-FEB-2016
      if (m_is_Dispute)
      {
        cb_hp_type.SelectedValue = HANDPAY_TYPE.MANUAL_OTHERS;
      }

      if (cb_hp_type.Items.Count <= 0)
      {
        _dr = _dt_hp_types.NewRow();
        _dt_hp_types.Rows.Add(_dr);
        _dr[0] = 0;
        _dr[1] = "";
      }
    }

    private void btn_athorization_Click(object sender, EventArgs e)
    {
      HANDPAY_STATUS _status_tax;
      CashierMovementsTable _cashier_movements;
      VoucherHandpayAuthorized _voucher_generated;
      Boolean _success;
      String _error_str;
      Control _ctrl;
      Int64 HpIdUpdated;
      Int64 _operation_id;

      HpIdUpdated = 0;
      _operation_id = 0;
      btn_payment.Enabled = false;
      btn_voided.Enabled = false;

      try
      {
        // Disable button to avoid to authorized more than one HP by clicking faster
        this.btn_athorization.Enabled = false;

        if (this.m_entry_type == ENTRY_TYPE.MANUAL)
        {
          if (!CheckAndGetDataFromControls(out _error_str, out _ctrl))
          {
            frm_message.Show(_error_str, Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Warning, this.ParentForm);
            _ctrl.Focus();

            btn_payment.Enabled = true;
            btn_athorization.Enabled = true;
            btn_voided.Enabled = true;
            return;
          }
        }

        // Condition needed when deauthorize handpay
        if (!Handpays.StatusActivated(m_initial_status, HANDPAY_STATUS.AUTHORIZED))
        {
          if (!Handpays.IsAuthorizable(m_initial_status, m_handpay.Amount))
          {
            frm_message.Show(Resource.String("STR_UC_CARD_USER_MSG_GENERIC_ERROR"),
                             Resource.String("STR_APP_GEN_MSG_ERROR"),
                             MessageBoxButtons.OK,
                             MessageBoxIcon.Warning,
                             this.ParentForm);

            btn_payment.Enabled = true;
            btn_athorization.Enabled = true;
            btn_voided.Enabled = true;
            return;
          }
        }

        if (m_is_tito && m_handpay.TaxBaseAmount > m_handpay.Amount)
        {
          frm_message.Show(Resource.String("STR_HANDPAY_TAX_BASE_ERROR_001").Replace("\\r\\n", "\r\n"),
                           Resource.String("STR_APP_GEN_MSG_ERROR"),
                           MessageBoxButtons.OK,
                           MessageBoxIcon.Warning,
                           this.ParentForm);
          txt_base_amount.Focus();

          btn_payment.Enabled = true;
          btn_athorization.Enabled = true;
          btn_voided.Enabled = true;
          return;
        }

        if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.HandpayAuthorization,
                                                 ProfilePermissions.TypeOperation.RequestPasswd,
                                                 this,
                                                 out _error_str))
        {
          btn_payment.Enabled = true;
          btn_athorization.Enabled = true;
          btn_voided.Enabled = true;
          return;
        }

        // Check whether the account is available or not 
        if (!m_handpay.CheckPayment())
        {
          m_handpay.HandpayMsg(Handpays.HANDPAY_MSG.REGISTER_NOT_ALLOWED, this, m_is_apply_tax);

          btn_payment.Enabled = true;
          btn_athorization.Enabled = true;
          btn_voided.Enabled = true;
          return;
        }

        _success = false;

        using (DB_TRX _trx = new DB_TRX())
        {
          if (!Operations.DB_InsertOperation(OperationCode.HANDPAY_VALIDATION,
                                             m_handpay.Card.AccountId, Cashier.SessionId,
                                             0,
                                             m_handpay.Amount,
                                             0,
                                             0,
                                             0,
                                             0,
                                             0,
                                             false,
                                             m_account_operation_reason_id,
                                             m_account_operation_comment,
                                             string.Empty, //EOR 24-FEB-2016 ADD COMMENTS HANDPAY DEFAULT VALUE
                                             out _operation_id,
                                             _trx.SqlTransaction))
          {
            _success = false;
          }

          if (!Handpays.StatusActivated(m_initial_status, HANDPAY_STATUS.AUTHORIZED))
          {
            // DEAUTHORIZED TO AUTHORIZED
            m_handpay.SetAuthorizedStatus();

            _status_tax = (HANDPAY_STATUS)(m_handpay.Status & (Int32)HANDPAY_STATUS.MASK_TAX);
            _cashier_movements = new CashierMovementsTable(Cashier.CashierSessionInfo());

            Handpays.HandpayAuthorizedInputParams _input_params;
            _input_params = new Handpays.HandpayAuthorizedInputParams();
            _input_params.HpId = m_handpay.HandpayId;
            _input_params.HpType = m_handpay.Type;
            _input_params.MaskTaxes = _status_tax;
            _input_params.TerminalId = m_handpay.TerminalId;
            _input_params.TerminalName = m_handpay.TerminalName;
            _input_params.TerminalProvider = m_handpay.TerminalProvider;
            _input_params.Amount = m_handpay.Amt0;
            _input_params.ProgressiveId = m_handpay.ProgressiveId;
            _input_params.Level = m_handpay.Level;
            _input_params.TypeName = Handpays.HandpayTypeString(m_handpay.Type, m_handpay.Level);
            _input_params.OperationId = _operation_id;

            if (Tax.EnableTitoTaxWaiver(true))
            {
              _input_params.TaxBaseAmount = Format.ParseCurrency(txt_base_amount.Text);
            }
            else
            {
              _input_params.TaxBaseAmount = m_handpay.TaxBaseAmount;
            }

            _input_params.TaxAmount = m_handpay.TaxAmount;

            _input_params.TaxPct = m_handpay.TaxPct;

            _success = Handpays.ChangeToAuthorizedStatus(_input_params, _cashier_movements, m_handpay.Card, _input_params.TaxAmount > 0, m_is_apply_tax, out _voucher_generated, out HpIdUpdated, _trx.SqlTransaction);
          }
          else
          {
            // AUTHORIZED TO DEAUTHORIZED
            _cashier_movements = new CashierMovementsTable(Cashier.CashierSessionInfo());

            _success = Handpays.ResetAuthorizedStatus(m_handpay.HandpayId, m_handpay.Card, _cashier_movements, _trx.SqlTransaction);
            _voucher_generated = null;
          }

          if (_success)
          {
            _cashier_movements.Save(_trx.SqlTransaction);
            _trx.SqlTransaction.Commit();

            if (_voucher_generated != null)
            {
              VoucherPrint.Print(_voucher_generated);
            }

            m_handpay.HandpayId = HpIdUpdated;
          }
        }

        if (!_success)
        {
          m_handpay.Status = m_initial_status;

          frm_message.Show(Resource.String("STR_UC_CARD_USER_MSG_GENERIC_ERROR"),
                           Resource.String("STR_APP_GEN_MSG_ERROR"),
                           MessageBoxButtons.OK,
                           MessageBoxIcon.Warning,
                           this.ParentForm);
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {
        // Enable button
        this.btn_payment.Enabled = true;
        this.btn_athorization.Enabled = true;
        this.btn_voided.Enabled = true;
      }

      btn_close_Click(null, null);

    }

    private void btn_close_Click(object sender, EventArgs e)
    {
      Misc.WriteLog("[FORM CLOSE] frm_handpay_action (close)", Log.Type.Message);
      this.Close();
    }

    private Boolean SetHPToControls()
    {
      Boolean _is_progressive;
      Decimal _base_amount;

      _is_progressive = false;
      _base_amount = 0;

      uc_terminal_filter.SelectTerminal(m_handpay.TerminalId);
      cb_hp_type.SelectedValue = m_handpay.Type;

      if (m_handpay.Type == HANDPAY_TYPE.MANUAL_JACKPOT || m_handpay.Type == HANDPAY_TYPE.JACKPOT)
      {
        if (m_handpay.Level >= 1 && m_handpay.Level <= 32)
        {
          _is_progressive = true;
          cb_hp_type.SelectedValue = TYPE_JACKPOT_PROGRESSIVE;
        }
        else if (m_handpay.Level == JACKPOT_MAX_PRIZE_LEVEL)
        {
          cb_hp_type.SelectedValue = TYPE_JACKPOT_PROGRESSIVE_40;
        }
      }


      if (!Handpays.StatusActivated(m_handpay.Status, HANDPAY_STATUS.AUTHORIZED) && m_is_new_handpay)
      {
        // Default value for base amount

        _base_amount = m_handpay.Amount;
        if (_is_progressive)
        {
          _base_amount = Math.Max(m_handpay.Amount - m_handpay.PartialPay, 0);
        }

        txt_base_amount.Text = ((Currency)_base_amount).ToString();
        m_handpay.TaxBaseAmount = _base_amount;
      }
      else
      {
        txt_base_amount.Text = ((Currency)m_handpay.TaxBaseAmount).ToString();
        lbl_amount_to_pay.Text = ((Currency)(m_handpay.Amount - m_handpay.TaxAmount)).ToString();
      }

      // OPC 10-DIC-2014: Load the current tax 
      txt_amount.Text = ((Currency)m_handpay.Amount).ToString();

      lbl_tax.Text = ((Currency)(-1) * m_handpay.TaxAmount).ToString();

      RefreshTotals();
      return true;
    }

    /// <summary>Update amount according to created amount reported in national or foreign currency</summary>
    /// <returns>True if all goes well, false if there are errors</returns>
    private Boolean ConvertCreatedAmountToNationalAmount()
    {
      Decimal _amount_to_converted;
      Int32 _terminal_id_selected;

      try
      {
        // RAB 30-MAR-2016: PBI 10789
        if (m_enabled_floor_dual_currency)
        {
          using (DB_TRX _sql_trx = new DB_TRX())
          {
            _terminal_id_selected = m_handpay.HandpayId == 0 ? uc_terminal_filter.TerminalSelected() : m_handpay.TerminalId;
            if (String.IsNullOrEmpty(m_iso_code_terminal_selected))
            {
              if (!CurrencyExchange.GetIsoCodeFromTerminalID(_terminal_id_selected, out m_iso_code_terminal_selected))
              {
                return false;
              }
            }

            _amount_to_converted = Format.ParseCurrency(txt_created_amount.Text.Replace(m_iso_code_terminal_selected, ""));
            m_handpay.Amount = CurrencyExchange.GetExchange(_amount_to_converted, m_iso_code_terminal_selected, CurrencyExchange.GetNationalCurrency(), _sql_trx.SqlTransaction);
            txt_amount.Text = ((Currency)m_handpay.Amount).ToString();
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
    }

    private void RefreshTotals()
    {

      if (!Tax.EnableTitoTaxWaiver(true))
      {
        RefreshTotalsWithOutTaxApplication();
      }
      else
      {
        if (ConvertCreatedAmountToNationalAmount())
        {
          btn_tax.Visible = false;
          btn_apply_tax.Visible = true;

          if (m_handpay.Type == HANDPAY_TYPE.UNKNOWN)
          {
            m_handpay.Type = (HANDPAY_TYPE)cb_hp_type.SelectedValue;
          }

          if (m_is_new_handpay)
          {
            m_is_apply_tax = Tax.ApplyTaxCollectionByHandPay(m_handpay.Type, m_handpay.Level);

            if (btn_apply_tax.Enabled)
            {
              m_is_apply_tax_initial = !m_is_apply_tax;
              m_is_apply_tax = !m_is_apply_tax;
            }
          }
          else
          {
            m_is_apply_tax = !Tax.ApplyTaxCollectionByHandPay(m_handpay.Type, m_handpay.Level);
          }

          CalculateCashAmountHandPayByStatus(m_is_apply_tax);
        }

        ChangeLiteralButtonApplyTax();

        if ((m_handpay.Status == (Int32)HANDPAY_STATUS.PENDING) && (m_handpay.HandpayId != 0) || (!m_is_new_handpay))
        {
          this.btn_apply_tax.Enabled = true;
        }
      }
    }

    private void ChangeLiteralButtonApplyTax()
    {
      if (btn_apply_tax.Visible)
      {
        m_is_apply_tax = Tax.ApplyTaxCollectionByHandPay(m_handpay.Type, m_handpay.Level);

        if (m_is_apply_tax)
        {
          btn_apply_tax.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_APPLY_TAX");
        }
        else
        {
          btn_apply_tax.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_NO_APPLY_TAX");
        }
      }
    }

    private void RefreshTotalsWithOutTaxApplication()
    {
      Boolean _old_visible;

      if (m_is_tito)
      {
        _old_visible = btn_tax.Visible;
        btn_tax.Visible = Handpays.CanApplyNoTax(m_handpay.TaxBaseAmount, m_handpay.Type) && !Tax.EnableTitoTaxWaiver(true);

        if (btn_tax.Visible)
        {
          if (_old_visible != btn_tax.Visible || m_taxes == 0)
          {
            //Default value for base amount and amount
            ChangeStatusTaxButton(true);
          }
          else
          {
            ChangeStatusTaxButton(m_taxes == HANDPAY_STATUS.NO_TAX);
          }

          m_handpay.Status = Handpays.ChangeStatus(m_handpay.Status, m_taxes);
        }
        else
        {
          m_handpay.Status = (m_handpay.Status & ~(Int32)HANDPAY_STATUS.MASK_TAX) + 0;
        }

        Handpays.HPAmountsCalculated _calculated;

        if (!ConvertCreatedAmountToNationalAmount())
        {
          return;
        }

        if (!Handpays.CalculateCashAmountHandpay(m_handpay.Status,
                                                 m_handpay.Amount,
                                                 m_handpay.TaxBaseAmount,
                                                 m_handpay.Type,
                                                 m_handpay.Card,
                                                 m_is_apply_tax_initial,
                                                 Tax.ApplyTaxCollectionByHandPay(m_handpay.Type, m_handpay.Level),
                                                 out _calculated))
        {
          return;
        }

        lbl_tax.Text = ((Currency)(-1 * _calculated.TaxAmount)).ToString();

        if (Tax.EnableTitoTaxWaiver(true))
        {
          if (!Tax.ApplyTaxCollectionByHandPay(m_handpay.Type, m_handpay.Level))
          {
            lbl_amount_to_pay.Text = ((Currency)_calculated.CashRedeem.TotalPaid).ToString();
          }
          else
          {
            lbl_amount_to_pay.Text = ((Currency)(_calculated.CashRedeem.TotalPaid + _calculated.TaxAmount)).ToString();
          }
        }
        else
        {
          lbl_tax_percent.Text = Resource.String("STR_HANDPAY_ACTION_28") + Environment.NewLine + "(" + _calculated.TaxPct.ToString() + ")";
          lbl_amount_to_pay.Text = ((Currency)_calculated.CashRedeem.TotalPaid).ToString();
        }

        m_handpay.TaxPct = _calculated.TaxPct;
        m_handpay.TaxAmount = _calculated.TaxAmount;
      }
      else
      {
        lbl_amount_to_pay.Text = ((Currency)m_handpay.Amount).ToString();
      }
    }

    private void CalculateCashAmountHandPayByStatus(Boolean IsApplyTax)
    {
      Handpays.HPAmountsCalculated _calculated;

      if (btn_apply_tax.Enabled)
      {
        if (m_is_apply_tax)
        {
          m_previous_amount = 0;

          m_previous_amount = m_handpay.Amount;

          if (IsApplyTax)
          {
            m_handpay.Amount = Format.ParseCurrency(txt_base_amount.Text);
          }
          else
          {
            m_handpay.TaxBaseAmount = Format.ParseCurrency(txt_base_amount.Text);
          }
        }
        else
        {
          m_handpay.Amount = m_previous_amount;
        }
      }
      else
      {
        if (Tax.ApplyTaxCollectionByHandPay(m_handpay.Type, m_handpay.Level))
        {
          m_handpay.Amount = m_previous_amount;
          m_handpay.TaxBaseAmount = Format.ParseCurrency(txt_base_amount.Text);
        }
      }

      if (!Handpays.CalculateCashAmountHandpay(m_handpay.Status,
                                               m_handpay.Amount,
                                               m_handpay.TaxBaseAmount,
                                               m_handpay.Type,
                                               m_handpay.Card,
                                               IsApplyTax,
                                               Tax.ApplyTaxCollectionByHandPay(m_handpay.Type, m_handpay.Level),
                                               out _calculated))
      {
        return;
      }

      lbl_tax_percent.Text = Resource.String("STR_HANDPAY_ACTION_28") + Environment.NewLine + "(" + _calculated.TaxPct.ToString() + ")";
      lbl_tax.Text = ((Currency)(-1 * _calculated.TaxAmount)).ToString();
      lbl_amount_to_pay.Text = ((Currency)(m_previous_amount - _calculated.TaxAmount)).ToString();

      m_handpay.TaxPct = _calculated.TaxPct;
      m_handpay.TaxAmount = _calculated.TaxAmount;
      m_handpay.TaxBaseAmount = Format.ParseCurrency(txt_base_amount.Text);

      if (m_handpay.Amount != m_previous_amount)
      {
        m_handpay.Amount = m_previous_amount;
      }

      return;
    }

    private Boolean CheckAndGetDataFromControls(out String Error, out Control CtrlToFocus)
    {
      CardData _card_data;
      Currency _input_amount;
      Int32 _terminal_id;
      Int64 _used_account_id;
      Int32 _selected_type;
      Decimal _max_allowed_manual_entry;

      _card_data = new CardData();
      Error = "";
      CtrlToFocus = null;

      _terminal_id = uc_terminal_filter.TerminalSelected();

      if (_terminal_id <= 0)
      {
        Log.Error("ManualHandpay: CheckAndGetDataFromControls " + Resource.String("STR_FRM_HANDPAYS_MANUAL_008"));
        Error = Resource.String("STR_FRM_HANDPAYS_MANUAL_008");
        CtrlToFocus = uc_terminal_filter;

        return false;
      }

      if (m_is_tito)
      {
        try
        {
          using (DB_TRX _trx = new DB_TRX())
          {
            if (this.AccountId > 0)
            {
              _used_account_id = this.AccountId;
            }
            else
            {
              _used_account_id = Accounts.GetOrCreateVirtualAccount(uc_terminal_filter.TerminalSelected(), _trx.SqlTransaction);
            }

            if (_used_account_id == 0)
            {
              Error = Resource.String("STR_AC_IMPORT_RETRY_END");
              CtrlToFocus = btn_close;

              return false;
            }

            // Get card data info
            if (!CardData.DB_CardGetAllData(_used_account_id, _card_data, _trx.SqlTransaction))
            {
              Error = Resource.String("STR_AC_IMPORT_RETRY_END");
              CtrlToFocus = btn_close;

              return false;
            }

            _trx.Commit();
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
          Error = Resource.String("STR_AC_IMPORT_RETRY_END");
          CtrlToFocus = btn_close;

          return false;
        }
        m_handpay.Card = _card_data;
      }

      m_handpay.TerminalId = _terminal_id;
      m_handpay.TerminalName = uc_terminal_filter.TerminalSelectedName();
      m_handpay.TerminalProvider = uc_terminal_filter.ProviderSelected();

      // Load amount played data for handpay voucher (if needed)
      LoadPrintPlayedAmountInHandpay();

      _input_amount = 0;

      if (!String.IsNullOrEmpty(txt_amount.Text))
      {
        _input_amount = Format.ParseCurrency(txt_amount.Text);
      }

      if (_input_amount <= 0)
      {
        CtrlToFocus = txt_amount;
        Log.Warning("ManualHandpay: CheckAndGetDataFromControls. Wrong handpay amount (" + _input_amount + ").");
        Error = Resource.String("STR_FRM_HANDPAYS_MANUAL_006");

        return false;
      }

      m_handpay.Amount = (Decimal)_input_amount;

      Decimal _sys_amt;
      Decimal _egm_amt;
      String _egm_curr;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          WSI.Common.TITO.HandPay.GetFloorDualCurrency(m_handpay.Amount, m_handpay.TerminalId, out _sys_amt, out _egm_amt, out _egm_curr, _db_trx.SqlTransaction);
          m_handpay.Cur0 = _egm_curr;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      if (!Tax.EnableTitoTaxWaiver(true))
      {
        // Get the Handpay Manual settings from the General Parameters table
        _max_allowed_manual_entry = GeneralParam.GetInt32("Cashier.HandPays", "MaxAllowedManualEntry");

        // If limit is configured, entered amount must be less or equal to limit
        if (_max_allowed_manual_entry > 0 && _input_amount > _max_allowed_manual_entry)
        {
          Currency _max_allowed_manual_entry_curr;
          _max_allowed_manual_entry_curr = _max_allowed_manual_entry;

          CtrlToFocus = txt_amount;
          Log.Error("ManualHandpay: CheckAndGetDataFromControls. The amount exceeds the maximum allowed of " + _max_allowed_manual_entry_curr + ".");
          Error = Resource.String("STR_FRM_AMOUNT_INPUT_MSG_MAX_ALLOWED", _max_allowed_manual_entry_curr);

          return false;
        }
      }

      _input_amount = 0;
      if (!String.IsNullOrEmpty(txt_base_amount.Text))
      {
        _input_amount = Format.ParseCurrency(txt_base_amount.Text);
      }

      if (_input_amount < 0)
      {
        CtrlToFocus = txt_base_amount;
        Log.Error("ManualHandpay: CheckAndGetDataFromControls. Wrong handpay base amount (" + _input_amount + ").");
        Error = Resource.String("STR_FRM_HANDPAYS_MANUAL_006");

        return false;
      }

      _selected_type = (Int32)cb_hp_type.SelectedValue;
      m_handpay.Type = (HANDPAY_TYPE)_selected_type;

      if (_selected_type >= TYPE_JACKPOT_PROGRESSIVE)
      {
        Int32 _level;

        if (GeneralParam.GetBoolean("Progressives", "Enabled")
          && m_current_progressive_id == 0
          && _selected_type == TYPE_JACKPOT_PROGRESSIVE)
        {
          CtrlToFocus = uc_terminal_filter;
          Log.Error("ManualHandpay: CheckAndGetDataFromControls. Error in Progressive Id.");
          Error = Resource.String("STR_HANDPAY_ACTION_20");

          return false;
        }

        _level = 0;
        if (!GeneralParam.GetBoolean("Progressives", "Enabled")
          && m_current_progressive_id == 0)
        {
          _level = 1;
        }
        else if (_selected_type == TYPE_JACKPOT_PROGRESSIVE)
        {
          _level = ((Int32)cb_Level.SelectedIndex) + 1;
        }

        if (_selected_type == TYPE_JACKPOT_PROGRESSIVE_40)
        {
          _level = JACKPOT_MAX_PRIZE_LEVEL;
        }

        m_handpay.Type = HANDPAY_TYPE.MANUAL_JACKPOT;
        m_handpay.Level = _level;
        m_handpay.ProgressiveId = m_current_progressive_id;
      }

      m_handpay.DateTime = WGDB.Now;

      return true;
    }

    private void EnableControls(Boolean IsManualEntry)
    {
      uc_terminal_filter.Enabled = IsManualEntry;

      //FOS 10-03-2015
      //LTC 29-FEB-2016
      if (!m_is_Dispute || m_is_tito)
      {
        cb_hp_type.Enabled = IsManualEntry;
      }
      else
      {
        cb_hp_type.Enabled = false;
      }

      txt_amount.Enabled = IsManualEntry;
      txt_amount.TabStop = IsManualEntry;

      //LTC 22-FEB-2016
      if (Handpays.IsDisableTypeSelection() || m_is_Dispute)
      {
        this.cb_hp_type.Enabled = false;
    }
    }

    private void InitControls()
    {
      StringBuilder _where = new StringBuilder();
      Int32 _handpay_time_period = 0;
      Int32 _handpay_cancel_time_period = 0;
      Int32 _terminal_id_selected;
      String _amount_add_currency;
      Decimal _sum_taxes_percentage;

      m_enabled_floor_dual_currency = Misc.IsFloorDualCurrencyEnabled();
      _amount_add_currency = String.Empty;

      // Get the Handpays settings from the General Parameters table
      Handpays.GetParameters(out _handpay_time_period, out _handpay_cancel_time_period);
      if (m_is_tito)
      {
        // Load the list of available terminals
        _where.AppendLine("   WHERE TE_TERMINAL_TYPE = " + (Int32)TerminalTypes.SAS_HOST);
        _where.AppendLine(String.Format("      AND (  TE_RETIREMENT_DATE IS NULL OR DATEDIFF(MINUTE, TE_RETIREMENT_DATE, GETDATE()) <= {0} ) ", _handpay_time_period));
      }
      else
      {
        _where.AppendLine("   WHERE TE_TERMINAL_ID IN (");
        _where.AppendLine("SELECT   TE_TERMINAL_ID ");
        _where.AppendLine("  FROM   TERMINALS ");
        _where.AppendLine("       , ( ");
        _where.AppendLine("           SELECT   PS_TERMINAL_ID ");
        _where.AppendLine("                  , MAX(PS_STARTED) PS_STARTED  ");
        _where.AppendLine("             FROM   PLAY_SESSIONS WITH (INDEX (IX_PS_STARTED)) ");
        _where.AppendLine("            WHERE   PS_STARTED    >= DATEADD(MINUTE, -" + _handpay_time_period + ", GETDATE()) ");
        _where.AppendLine("              AND   PS_ACCOUNT_ID  = " + m_handpay.Card.AccountId);
        _where.AppendLine("              AND   (PS_INITIAL_BALANCE <> PS_FINAL_BALANCE OR PS_PLAYED_COUNT > 0)  ");
        _where.AppendLine("           GROUP BY PS_TERMINAL_ID ");
        _where.AppendLine("         ) RECENT_TERMINALS ");
        _where.AppendLine(" WHERE   TE_TERMINAL_TYPE <> " + (Int32)TerminalTypes.SITE);
        _where.AppendLine("   AND   TE_TERMINAL_TYPE <> " + (Int32)TerminalTypes.CASHDESK_DRAW);
        _where.AppendLine("   AND   TE_TERMINAL_TYPE <> " + (Int32)TerminalTypes.CASHDESK_DRAW_TABLE);
        _where.AppendLine("   AND   TE_TERMINAL_TYPE <> " + (Int32)TerminalTypes.TERMINAL_DRAW);
        _where.AppendLine("   AND   TE_TERMINAL_TYPE <> " + (Int32)TerminalTypes.GAMEGATEWAY);
        _where.AppendLine("   AND   TE_TERMINAL_TYPE <> " + (Int32)TerminalTypes.PARIPLAY);
        _where.AppendLine("   AND   TE_TERMINAL_ID    = RECENT_TERMINALS.PS_TERMINAL_ID ");
        _where.AppendLine(")");
      }

      uc_terminal_filter.InitControls("", _where.ToString(), true, true);
      _terminal_id_selected = m_handpay.HandpayId == 0 ? uc_terminal_filter.TerminalSelected() : m_handpay.TerminalId;
      m_base_text_value = 0;

      if (m_is_new_handpay)
      {
        txt_base_amount.Text = ((Currency)0).ToString();
        txt_amount.Text = ((Currency)0).ToString();
      }

      if (!m_is_tito)
      {
        lbl_tax.Visible = false;
      }

      _sum_taxes_percentage = (GeneralParam.GetDecimal("Cashier", "Tax.OnPrize.1.Pct") + GeneralParam.GetDecimal("Cashier", "Tax.OnPrize.2.Pct") + GeneralParam.GetDecimal("Cashier", "Tax.OnPrize.3.Pct")) / 100;

      lbl_tax.Text = ((Currency)0).ToString();
      lbl_amount_to_pay.Text = ((Currency)0).ToString();
      lbl_tax_percent.Text = Resource.String("STR_HANDPAY_ACTION_28")
                                + Environment.NewLine + "(" + String.Format("{0:P2}", _sum_taxes_percentage) + ")";
      m_is_apply_tax_initial = !Tax.EnableTitoTaxWaiver(true);

      // RAB 29-MAR-2016: PBI 10789
      if (!CurrencyExchange.GetIsoCodeFromTerminalID(_terminal_id_selected, out m_iso_code_terminal_selected))
      {
        return;
      }

      if (!CurrencyExchange.AddCurrencyToAmount(m_handpay.Amt0, m_iso_code_terminal_selected, out _amount_add_currency))
      {
        return;
      }

      txt_created_amount.Text = _amount_add_currency;
      LocationControls();
    }

    // PURPOSE: Location of controls depending on whether the FloorDualCurrency is enabled
    //
    // PARAMS:
    //     - INPUT:
    //
    //     - OUTPUT:
    //
    // RETURNS:
    //     - None
    //
    private void LocationControls()
    {
      if (m_enabled_floor_dual_currency)
      {
        gb_terminal.Location = new Point(13, 5);
        gp_type.Location = new Point(13, 184);
        pnl_created_amount.Location = new Point(9, 280);
        pnl_level.Location = new Point(9, 338);
        pnl_body_handpay.Location = new Point(9, 386);
        btn_voided.Location = new Point(11, 592);
        btn_athorization.Location = new Point(119, 592);
        btn_close.Location = new Point(247, 592);
        btn_payment.Location = new Point(355, 592);
        pnl_amount.Enabled = !m_enabled_floor_dual_currency;

        //Add events to txt_created_amount
        this.txt_created_amount.KeyPress += new KeyPressEventHandler(txt_amount_KeyPress);
        this.txt_created_amount.PreviewKeyDown += new PreviewKeyDownEventHandler(txt_amount_PreviewKeyDown);
        this.txt_created_amount.MouseClick += new MouseEventHandler(this.txt_amount_MouseClick);
        this.txt_created_amount.Enter += new EventHandler(this.txt_amount_Enter);
        this.txt_created_amount.Leave += new System.EventHandler(this.txt_amount_Leave);
        this.txt_created_amount.Click += txt_created_amount_Click;
      }
      else
      {
        gb_terminal.Location = new Point(13, 14);
        gp_type.Location = new Point(13, 206);
        pnl_created_amount.Location = new Point(9, 280);
        pnl_level.Location = new Point(9, 311);
        pnl_body_handpay.Location = new Point(9, 368);
        btn_voided.Location = new Point(11, 581);
        btn_athorization.Location = new Point(119, 581);
        btn_close.Location = new Point(247, 581);
        btn_payment.Location = new Point(355, 581);
        pnl_amount.Enabled = !m_enabled_floor_dual_currency;

        //Add events to txt_amount
        this.txt_amount.KeyPress += new KeyPressEventHandler(txt_amount_KeyPress);
        this.txt_amount.PreviewKeyDown += new PreviewKeyDownEventHandler(txt_amount_PreviewKeyDown);
        this.txt_amount.MouseClick += new MouseEventHandler(this.txt_amount_MouseClick);
        this.txt_amount.Enter += new EventHandler(this.txt_amount_Enter);
        this.txt_amount.Leave += new System.EventHandler(this.txt_amount_Leave);
        this.txt_amount.Click += txt_amount_Click;
      }

      // RAB 30-MAR-2016: Created amount panel will be visible if the FloorDualCurrency is enabled
      pnl_created_amount.Visible = m_enabled_floor_dual_currency;
    } // LocationControls

    private void EnableDisableButtonsByStatus()
    {
      btn_voided.Enabled = false;
      btn_athorization.Enabled = false;
      btn_payment.Enabled = false;
      txt_base_amount.Enabled = true;

      if (Handpays.StatusActivated(m_initial_status, HANDPAY_STATUS.PENDING)
         && !Handpays.StatusActivated(m_initial_status, HANDPAY_STATUS.AUTHORIZED))
      {
        if (m_entry_type == ENTRY_TYPE.DEFAULT)
        {
          btn_voided.Enabled = true;
        }

        btn_athorization.Enabled = true;
        btn_payment.Enabled = true;
      }

      if (Handpays.StatusActivated(m_initial_status, HANDPAY_STATUS.AUTHORIZED)
           && !Handpays.StatusActivated(m_initial_status, HANDPAY_STATUS.PAID))
      {
        btn_voided.Enabled = true;
        btn_payment.Enabled = true;
        btn_athorization.Enabled = true;

        btn_tax.Enabled = false;
        txt_base_amount.Enabled = false;
      }

      if (Handpays.StatusActivated(m_initial_status, HANDPAY_STATUS.VOIDED))
      {
        btn_tax.Enabled = false;
        btn_athorization.Enabled = !Handpays.StatusActivated(m_initial_status, HANDPAY_STATUS.AUTHORIZED);
        btn_payment.Enabled = false;
        btn_athorization.Enabled = false;
        btn_voided.Enabled = true;
      }

      if (Tax.EnableTitoTaxWaiver(true))
      {

        btn_tax.Visible = false;
        btn_apply_tax.Visible = true;

        if (m_is_new_handpay)
        {
          RefreshTotals();
        }
        else
        {
          btn_apply_tax.Enabled = false;
        }

        ChangeLiteralButtonApplyTax();
      }

      // RAB 30-MAR-2016: Created amouont control will have the same state of enabled amount.
      if (m_enabled_floor_dual_currency)
      {
        pnl_created_amount.Enabled = m_handpay.TerminalId == 0 ? true : txt_amount.Enabled;
      }

      //XGJ 09-GEN-2017
      this.txt_base_amount.Enabled = btn_payment.Enabled;
    }

    private void frm_handpay_action_Load(object sender, EventArgs e)
    {

      Point _main_form_location;
      Size _main_form_size;

      EnableControls(m_entry_type == ENTRY_TYPE.MANUAL);
      EnableDisableButtonsByStatus();

      if (m_entry_type == ENTRY_TYPE.DEFAULT)
      {
        SetHPToControls();
      }

      if (!m_is_tito)
      {
        btn_athorization.Visible = false;
        btn_tax.Visible = false;
        btn_voided.Visible = false;
        ResizeContent();
      }

      UpdateProgressiveControls();

      this.StartPosition = FormStartPosition.Manual;
      this.CustomLocation = true;

      _main_form_size = WindowManager.GetMainFormAreaSize();
      m_initial_x_form_location = 0;
      m_initial_y_form_location = 0;

      m_amount_input = new frm_generic_amount_input();
      _main_form_location = WindowManager.GetMainFormAreaLocation();
      m_initial_x_form_location = 161;
      this.Location = new Point(_main_form_location.X + m_initial_x_form_location, _main_form_location.Y + m_initial_y_form_location + 55);

      m_x_form_location_amount_input = this.Location.X + this.Width;
      m_initial_y_form_location = this.Location.Y + this.Height;

      m_initializing = false;
    }

    private void ResizeContent()
    {
      btn_close.Location = btn_payment.Location;
      btn_payment.Location = btn_athorization.Location;

      lbl_separator_1.Visible = false;
      lbl_tax_percent.Visible = false;
      lbl_tax.Visible = false;

      txt_base_amount.Visible = false;
      lbl_tax_base.Visible = false;

      lbl_amount_to_pay.Location = new Point(lbl_amount_to_pay.Location.X, lbl_amount_to_pay.Location.Y - BOTTOM_MARGIN);
      lbl_to_pay.Location = new Point(lbl_to_pay.Location.X, lbl_to_pay.Location.Y - BOTTOM_MARGIN);
      btn_close.Location = new Point(btn_close.Location.X, btn_close.Location.Y - BOTTOM_MARGIN);
      btn_payment.Location = new Point(btn_payment.Location.X, btn_payment.Location.Y - BOTTOM_MARGIN);
      this.Size = new Size(this.Size.Width, this.Size.Height - BOTTOM_MARGIN);

    } // ResizeContent

    private void uc_terminal_filter_TerminalOrProviderChanged()
    {
      String _amount_add_currency;
      _amount_add_currency = String.Empty;

      if (m_initializing)
      {
        return;
      }

      UpdateProgressiveControls();
      // RAB 30-MAR-2016: PBI 10789: Initialize controls to change terminal of groupbox.
      if (!CurrencyExchange.GetIsoCodeFromTerminalID(uc_terminal_filter.TerminalSelected(), out m_iso_code_terminal_selected))
      {
        return;
      }

      if (!CurrencyExchange.AddCurrencyToAmount(0, m_iso_code_terminal_selected, out _amount_add_currency))
      {
        return;
      }

      txt_created_amount.Text = _amount_add_currency;
      CleanControls();
    }

    private void CleanControls()
    {
      m_base_text_value = 0;
      txt_base_amount.Text = ((Currency)0).ToString();
      txt_amount.Text = ((Currency)0).ToString();
      lbl_tax.Text = ((Currency)0).ToString();
      lbl_amount_to_pay.Text = ((Currency)0).ToString();
      m_handpay.TaxAmount = 0;
      m_handpay.TaxBaseAmount = 0;
      m_handpay.Amount = 0;
    }

    private void cb_hp_type_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (m_initializing)
      {
        return;
      }

      UpdateProgressiveControls();

      m_handpay.Level = 0;

      // For update taxes
      m_handpay.Type = (HANDPAY_TYPE)cb_hp_type.SelectedValue;
      if ((Int32)cb_hp_type.SelectedValue >= TYPE_JACKPOT_PROGRESSIVE)
      {
        m_handpay.Type = HANDPAY_TYPE.MANUAL_JACKPOT;

        if ((Int32)cb_hp_type.SelectedValue == TYPE_JACKPOT_PROGRESSIVE)
        {
          m_handpay.Level = ((Int32)cb_Level.SelectedIndex) + 1;
        }

        if ((Int32)cb_hp_type.SelectedValue == TYPE_JACKPOT_PROGRESSIVE_40)
        {
          m_handpay.Level = JACKPOT_MAX_PRIZE_LEVEL;
        }
      }

      if (btn_apply_tax.Enabled == false)
      {
        m_base_text_value = 0;
        txt_base_amount.Text = ((Currency)0).ToString();
        txt_amount.Text = ((Currency)0).ToString();
        lbl_tax.Text = ((Currency)0).ToString();
        lbl_amount_to_pay.Text = ((Currency)0).ToString();
        btn_apply_tax.Enabled = true;
        m_handpay = new HandPay(new CardData());
        m_is_apply_tax = Tax.ApplyTaxCollectionByHandPay(m_handpay.Type, m_handpay.Level);
        m_previous_amount = 0;
      }

      RefreshTotals();
    }

    private void UpdateProgressiveControls()
    {
      Int32 _terminal_id;
      String _progressive_name;
      Int32 _num_levels;

      m_current_progressive_id = 0;

      lbl_progresive.Text = Resource.String("STR_HANDPAY_ACTION_19") + ": ---";
      cb_Level.Items.Clear();
      lbl_level.Text = Resource.String("STR_HANDPAY_ACTION_04");

      lbl_progresive.Enabled = false;
      cb_Level.Enabled = false;
      lbl_level.Enabled = false;

      _num_levels = 0;

      if ((Int32)cb_hp_type.SelectedValue != TYPE_JACKPOT_PROGRESSIVE)
      {
        return;
      }

      _terminal_id = uc_terminal_filter.TerminalSelected();
      if (_terminal_id < 1)
      {
        return;
      }

      using (DB_TRX _db_trx = new DB_TRX())
      {
        Progressives.GetProgressiveNameFromTerminal(_terminal_id, out m_current_progressive_id, out _progressive_name, out _num_levels, _db_trx.SqlTransaction);
      }

      if (String.IsNullOrEmpty(_progressive_name))
      {
        return;
      }

      lbl_progresive.Enabled = (m_entry_type == ENTRY_TYPE.MANUAL);
      cb_Level.Enabled = (m_entry_type == ENTRY_TYPE.MANUAL);
      lbl_level.Enabled = (m_entry_type == ENTRY_TYPE.MANUAL);

      lbl_progresive.Text = Resource.String("STR_HANDPAY_ACTION_19") + ": " + _progressive_name;
      lbl_level.Text = Resource.String("STR_HANDPAY_ACTION_04");
      if (m_entry_type == ENTRY_TYPE.MANUAL)
      {
        if (_num_levels > 0)
        {
          for (int _level = 1; _level <= _num_levels; _level++)
          {
            cb_Level.Items.Add(_level);
          }

          cb_Level.SelectedIndex = 0;
        }
      }
      else
      {
        cb_Level.Items.Add(m_handpay.Level);
        cb_Level.SelectedIndex = 0;
      }
    }

    void m_amount_input_OnKeyPress(String Amount)
    {
      Decimal _amount;

      if (String.IsNullOrEmpty(Amount))
      {
        _amount = 0;
      }
      else if (!Decimal.TryParse(Amount, out _amount))
      {
        return;
      }

      SetAmountToControl(m_current_amount_control, _amount, false);

      RefreshTotals();
    }

    private Boolean SetAmountToControl(Control TxtControl, Decimal Amount, Boolean CurrencySymbol)
    {
      if (TxtControl.Equals(txt_base_amount))
      {
        m_handpay.TaxBaseAmount = Amount;
        TxtControl.Text = ((Currency)m_handpay.TaxBaseAmount).ToString(CurrencySymbol);
      }
      else
      {
        m_previous_amount = Amount;
        m_handpay.Amount = Amount;
        TxtControl.Text = ((Currency)m_handpay.Amount).ToString(CurrencySymbol);

        m_handpay.Cur0 = CurrencyExchange.GetNationalCurrency();
        m_handpay.Amt0 = m_handpay.Amount;
      }

      return true;
    }

    private void frm_imput_Leave(object sender, EventArgs e)
    {
      if (m_amount_input != null)
      {
        m_amount_input.Hide();
        m_amount_input.Dispose();
        m_amount_input = null;
      }
    }

    private void txt_amount_KeyPress(object sender, KeyPressEventArgs e)
    {
      WSI.Cashier.Controls.uc_round_textbox txt_aux;
      String aux_str;
      char c;

      c = e.KeyChar;

      if (sender == txt_amount)
      {
        txt_aux = txt_amount;
      }
      else
      {
        txt_aux = txt_base_amount;
      }

      if (c >= '0' && c <= '9')
      {
        aux_str = "" + c;
        AddNumber(aux_str);
        e.Handled = true;
      }

      if (c == '\b')
      {
        if (txt_aux.Text.Length > 0)
        {
          txt_aux.Text = txt_aux.Text.Substring(0, txt_aux.Text.Length - 1);
        }
      }

      if (c == '.')
      {
        if (txt_aux.Text.IndexOf(m_decimal_str) == -1)
        {
          txt_aux.Text = txt_aux.Text + m_decimal_str;
        }
      }

      e.Handled = true;

      if (e.Handled && c != '.')
      {
        Decimal _amount;

        if (!Decimal.TryParse(txt_aux.Text, out _amount))
        {
          return;
        }

        SetAmountToControl((Control)sender, _amount, false);

        RefreshTotals();
      }
    }

    private void txt_amount_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
    {
      char c;

      c = Convert.ToChar(e.KeyCode);
      if (c == '\r')
      {
        e.IsInputKey = true;
      }
    }

    /// <summary>
    /// Check and Add digit to amount
    /// </summary>
    /// <param name="NumberStr"></param>
    private void AddNumber(String NumberStr)
    {
      String aux_str;
      String tentative_number;
      Currency input_amount;
      int dec_symbol_pos;

      // Prevent exceeding maximum number of decimals (2)
      dec_symbol_pos = txt_amount.Text.IndexOf(m_decimal_str);
      if (dec_symbol_pos > -1)
      {
        aux_str = txt_amount.Text.Substring(dec_symbol_pos);

        if (aux_str.Length > 2)
        {
          return;
        }
      }

      // Prevent exceeding maximum amount length
      if (txt_amount.Text.Length >= Cashier.MAX_ALLOWED_AMOUNT_LENGTH)
      {
        return;
      }

      tentative_number = txt_amount.Text + NumberStr;

      try
      {
        input_amount = Decimal.Parse(tentative_number);
        if (input_amount > Cashier.MAX_ALLOWED_AMOUNT)
        {
          return;
        }
      }
      catch
      {
        input_amount = 0;

        return;
      }

      // Remove unneeded leading zeros
      // Unless we are entering a decimal value, there must not be any leading 0
      if (dec_symbol_pos == -1)
      {
        txt_amount.Text = txt_amount.Text.TrimStart('0') + NumberStr;
      }
      else
      {
        // Accept new symbol/digit 
        txt_amount.Text = txt_amount.Text + NumberStr;
      }
    }


    private void frm_handpay_action_Activated(object sender, EventArgs e)
    {
      HidePadNumber();
    }

    private void txt_amount_MouseClick(object sender, MouseEventArgs e)
    {
      ShowPadNumber((Control)sender);
    }

    private void txt_amount_Enter(object sender, EventArgs e)
    {
      //ShowPadNumber((Control)sender);
    }

    private void ShowPadNumber(Control AmountCtrl)
    {
      Decimal _amount;
      Boolean _result;

      m_current_amount_control = AmountCtrl;

      if (m_amount_input == null)
      {
        //this.Location = new Point(m_x_form_location_amount_input, m_initial_y_form_location);
        m_amount_input = new frm_generic_amount_input();
        m_amount_input.OnKeyPressed += new frm_generic_amount_input.KeyPressed(m_amount_input_OnKeyPress);
        m_amount_input.StartPosition = FormStartPosition.Manual;
        m_amount_input.CustomLocation = true;
      }

      if (AmountCtrl.Equals(txt_base_amount))
      {
        _amount = Format.ParseCurrency(txt_base_amount.Text);
      }
      else
      {
        // RAB 30-MAR-2016: PBI 10789
        if (m_enabled_floor_dual_currency)
        {
          _amount = Format.ParseCurrency(txt_created_amount.Text.Replace(m_iso_code_terminal_selected, ""));
        }
        else
        {
          _amount = Format.ParseCurrency(txt_amount.Text);
        }
      }

      this.txt_amount.TextChanged -= new System.EventHandler(this.txt_amount_TextChanged);
      _result = m_amount_input.Show(GENERIC_AMOUNT_INPUT_TYPE.AMOUNT, string.Empty, ref _amount);
      this.txt_amount.TextChanged += new System.EventHandler(this.txt_amount_TextChanged);

      m_amount_input.Location = new Point(m_x_form_location_amount_input, m_initial_y_form_location - m_amount_input.Height);

      if (_result)
      {
        RefreshTotals();
        m_amount_input.BringToFront();
        m_amount_input.Focus();
        m_can_edit = false;
      }
    }

    private void HidePadNumber()
    {
      if (!m_can_edit)
      {
        m_can_edit = true;

        if (m_amount_input != null)
        {
          m_amount_input.Hide();
          m_amount_input.Dispose();
          m_amount_input = null;
        }
      }
    }

    private void btn_voided_Click(object sender, EventArgs e)
    {
      CashierMovementsTable _cashier_movements;
      Boolean _success;
      String _error_str;
      Boolean _is_voided;

      if (Handpays.StatusActivated(m_initial_status, HANDPAY_STATUS.PENDING))
      {
        if (!Handpays.IsVoidable(m_initial_status))
        {
          frm_message.Show(Resource.String("STR_UC_CARD_USER_MSG_GENERIC_ERROR"),
                           Resource.String("STR_APP_GEN_MSG_ERROR"),
                           MessageBoxButtons.OK,
                           MessageBoxIcon.Warning,
                           this.ParentForm);

          return;
        }
      }

      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.HandpayVoid,
                                               ProfilePermissions.TypeOperation.RequestPasswd,
                                               this,
                                               out _error_str))
      {
        return;
      }

      _success = false;

      try
      {
        using (DB_TRX _trx = new DB_TRX())
        {
          _cashier_movements = new CashierMovementsTable(Cashier.CashierSessionInfo());

          _is_voided = Handpays.StatusActivated(m_initial_status, HANDPAY_STATUS.VOIDED) ? true : false;

          _success = Handpays.ChangeToVoidedOrPendingStatus(m_handpay.HandpayId, _is_voided, m_handpay.Card, _cashier_movements, _trx.SqlTransaction);

          if (_success)
          {
            _cashier_movements.Save(_trx.SqlTransaction);

            _trx.Commit();
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      if (!_success)
      {
        // Set the initial status to the current status
        m_handpay.Status = m_initial_status;

        frm_message.Show(Resource.String("STR_UC_CARD_USER_MSG_GENERIC_ERROR"),
                         Resource.String("STR_APP_GEN_MSG_ERROR"),
                         MessageBoxButtons.OK,
                         MessageBoxIcon.Warning,
                         this.ParentForm);
      }

      btn_close_Click(null, null);
    }

    private void cb_Level_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (m_entry_type != ENTRY_TYPE.MANUAL)
      {
        return;
      }

      if (cb_Level.Items.Count > 0)
      {
        m_handpay.Level = ((int)cb_Level.SelectedIndex) + 1;
      }
    }

    public Int64 LastIdCreated()
    {
      return m_handpay.HandpayId;
    }

    private void txt_amount_Leave(object sender, EventArgs e)
    {
      Currency _amount;
      String _amount_add_currency;
      Control _sender;

      _sender = (Control)sender;
      _amount = Format.ParseCurrency(_sender.Text);
      _amount_add_currency = String.Empty;

      // RAB 30-MAR-2016: PBI 10789
      if (m_enabled_floor_dual_currency && _sender == txt_created_amount)
      {
        if (!CurrencyExchange.AddCurrencyToAmount(_amount, m_iso_code_terminal_selected, out _amount_add_currency))
        {
          return;
        }

        _sender.Text = _amount_add_currency;
      }
      else
      {
        _sender.Text = _amount.ToString(true);
      }
    }

    private void ChangeStatusTaxButton(Boolean NoTax)
    {
      if (NoTax)
      {
        btn_tax.Text = Resource.String("STR_HANDPAY_ACTION_25");
        m_taxes = HANDPAY_STATUS.NO_TAX;
      }
      else
      {
        btn_tax.Text = Resource.String("STR_HANDPAY_ACTION_26");
        m_taxes = HANDPAY_STATUS.TAXED;
      }
    }

    private void btn_tax_MouseClick(object sender, MouseEventArgs e)
    {
      String _error_str;

      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.HandpayApplyNoTax,
                                               ProfilePermissions.TypeOperation.RequestPasswd,
                                               this, out _error_str))
      {
        return;
      }

      if (m_taxes == HANDPAY_STATUS.NO_TAX)
      {
        ChangeStatusTaxButton(false);
      }
      else
      {
        ChangeStatusTaxButton(true);
      }

      RefreshTotals();
    }

    private void btn_tax_VisibleChanged(object sender, EventArgs e)
    {
    }

    private void btn_apply_tax_Click(object sender, EventArgs e)
    {
      string _error_str;
      _error_str = string.Empty;

      if (!ProfilePermissions.CheckPermissionList(ProfilePermissions.CashierFormFuncionality.HandpayTaxCollector,
                                                ProfilePermissions.TypeOperation.RequestPasswd, this, 0, out _error_str))
      {
        return;
      }

      m_form_catalog = new frm_catalog(m_is_apply_tax);
      m_form_catalog.Show(this, out m_is_apply_tax_result, out m_account_operation_reason_id, out m_account_operation_comment);

      if (m_is_apply_tax_result)
      {
        if (!m_is_apply_tax)
        {
          m_is_apply_tax = true;
          m_is_apply_tax_initial = !m_is_apply_tax;
          RefreshTotalsWithOutTaxApplication();
          m_handpay.TaxBaseAmount = 0;
        }
        else
        {
          CalculateCashAmountHandPayByStatus(m_is_apply_tax);
          m_is_apply_tax_initial = m_is_apply_tax;
        }

        btn_apply_tax.Enabled = false;
      }
    }

    private void AmountValueWithBtnTaxEnabledPressed()
    {
      if (btn_apply_tax.Enabled == false && Tax.ApplyTaxCollectionByHandPay(m_handpay.Type, m_handpay.Level))
      {
        m_previous_amount = Format.ParseCurrency(txt_amount.Text);
        CalculateCashAmountHandPayByStatus(m_is_apply_tax);
      }
      else
      {
        RefreshTotalsWithOutTaxApplication();
      }
    }

    private void txt_amount_TextChanged(object sender, EventArgs e)
    {
      if (Tax.EnableTitoTaxWaiver(true) && m_is_new_handpay && m_handpay.TaxAmount > 0)
      {
        AmountValueWithBtnTaxEnabledPressed();
      }

      if (m_base_text_value == Format.ParseCurrency(txt_base_amount.Text))
      {
        m_base_text_value = Format.ParseCurrency(txt_amount.Text);
        txt_base_amount.Text = txt_amount.Text;

        //txt_base_amount.Text = (m_handpay.TaxBaseAmount != 0) ? m_handpay.TaxBaseAmount.ToString() : txt_amount.Text;
        m_handpay.TaxBaseAmount = Format.ParseCurrency(txt_base_amount.Text);
      }
      else
      {
        m_base_text_value = -1; // Etp base text value has been modificated
      }
    }

    public Int64 AccountId
    {
      set
      {
        this.m_account_id = value;
      }
      get
      {
        return this.m_account_id;
      }
    }
  }
}

