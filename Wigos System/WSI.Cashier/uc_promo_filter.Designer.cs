namespace WSI.Cashier
{
  partial class uc_promo_filter
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(uc_promo_filter));
      this.chk_re = new System.Windows.Forms.CheckBox();
      this.chk_nr = new System.Windows.Forms.CheckBox();
      this.chk_pt = new System.Windows.Forms.CheckBox();
      this.chk_ca = new System.Windows.Forms.CheckBox();
      this.pnl_info_promos = new System.Windows.Forms.Panel();
      this.lbl_sort_by = new WSI.Cashier.Controls.uc_label();
      this.lbl_points = new WSI.Cashier.Controls.uc_label();
      this.lbl_redeemable = new WSI.Cashier.Controls.uc_label();
      this.lbl_lock = new WSI.Cashier.Controls.uc_label();
      this.lbl_non_redeemable = new WSI.Cashier.Controls.uc_label();
      this.pnl_info_promos.SuspendLayout();
      this.SuspendLayout();
      // 
      // chk_re
      // 
      this.chk_re.Appearance = System.Windows.Forms.Appearance.Button;
      this.chk_re.BackColor = System.Drawing.Color.Transparent;
      this.chk_re.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
      this.chk_re.FlatAppearance.BorderColor = System.Drawing.Color.Black;
      this.chk_re.FlatAppearance.BorderSize = 0;
      this.chk_re.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
      this.chk_re.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
      this.chk_re.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
      this.chk_re.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.chk_re.Image = ((System.Drawing.Image)(resources.GetObject("chk_re.Image")));
      this.chk_re.Location = new System.Drawing.Point(159, 15);
      this.chk_re.Name = "chk_re";
      this.chk_re.Size = new System.Drawing.Size(28, 28);
      this.chk_re.TabIndex = 3;
      this.chk_re.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.chk_re.UseVisualStyleBackColor = false;
      // 
      // chk_nr
      // 
      this.chk_nr.Appearance = System.Windows.Forms.Appearance.Button;
      this.chk_nr.BackColor = System.Drawing.Color.Transparent;
      this.chk_nr.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
      this.chk_nr.FlatAppearance.BorderColor = System.Drawing.Color.Black;
      this.chk_nr.FlatAppearance.BorderSize = 0;
      this.chk_nr.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
      this.chk_nr.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
      this.chk_nr.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
      this.chk_nr.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.chk_nr.Image = ((System.Drawing.Image)(resources.GetObject("chk_nr.Image")));
      this.chk_nr.Location = new System.Drawing.Point(29, 15);
      this.chk_nr.Margin = new System.Windows.Forms.Padding(0);
      this.chk_nr.Name = "chk_nr";
      this.chk_nr.Size = new System.Drawing.Size(28, 28);
      this.chk_nr.TabIndex = 1;
      this.chk_nr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.chk_nr.UseVisualStyleBackColor = false;
      // 
      // chk_pt
      // 
      this.chk_pt.Appearance = System.Windows.Forms.Appearance.Button;
      this.chk_pt.BackColor = System.Drawing.Color.Transparent;
      this.chk_pt.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
      this.chk_pt.FlatAppearance.BorderColor = System.Drawing.Color.Black;
      this.chk_pt.FlatAppearance.BorderSize = 0;
      this.chk_pt.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
      this.chk_pt.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
      this.chk_pt.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
      this.chk_pt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.chk_pt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.chk_pt.Image = ((System.Drawing.Image)(resources.GetObject("chk_pt.Image")));
      this.chk_pt.Location = new System.Drawing.Point(224, 15);
      this.chk_pt.Name = "chk_pt";
      this.chk_pt.Size = new System.Drawing.Size(28, 28);
      this.chk_pt.TabIndex = 4;
      this.chk_pt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.chk_pt.UseVisualStyleBackColor = false;
      // 
      // chk_ca
      // 
      this.chk_ca.Appearance = System.Windows.Forms.Appearance.Button;
      this.chk_ca.BackColor = System.Drawing.Color.Transparent;
      this.chk_ca.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
      this.chk_ca.FlatAppearance.BorderColor = System.Drawing.Color.Black;
      this.chk_ca.FlatAppearance.BorderSize = 0;
      this.chk_ca.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
      this.chk_ca.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
      this.chk_ca.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
      this.chk_ca.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.chk_ca.Image = ((System.Drawing.Image)(resources.GetObject("chk_ca.Image")));
      this.chk_ca.Location = new System.Drawing.Point(94, 15);
      this.chk_ca.Name = "chk_ca";
      this.chk_ca.Size = new System.Drawing.Size(28, 28);
      this.chk_ca.TabIndex = 2;
      this.chk_ca.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.chk_ca.UseVisualStyleBackColor = false;
      // 
      // pnl_info_promos
      // 
      this.pnl_info_promos.Controls.Add(this.lbl_sort_by);
      this.pnl_info_promos.Controls.Add(this.lbl_points);
      this.pnl_info_promos.Controls.Add(this.lbl_redeemable);
      this.pnl_info_promos.Controls.Add(this.lbl_lock);
      this.pnl_info_promos.Controls.Add(this.lbl_non_redeemable);
      this.pnl_info_promos.Controls.Add(this.chk_re);
      this.pnl_info_promos.Controls.Add(this.chk_nr);
      this.pnl_info_promos.Controls.Add(this.chk_pt);
      this.pnl_info_promos.Controls.Add(this.chk_ca);
      this.pnl_info_promos.Location = new System.Drawing.Point(0, 0);
      this.pnl_info_promos.Margin = new System.Windows.Forms.Padding(0);
      this.pnl_info_promos.Name = "pnl_info_promos";
      this.pnl_info_promos.Size = new System.Drawing.Size(280, 70);
      this.pnl_info_promos.TabIndex = 30;
      // 
      // lbl_sort_by
      // 
      this.lbl_sort_by.BackColor = System.Drawing.Color.Transparent;
      this.lbl_sort_by.Font = new System.Drawing.Font("Open Sans Semibold", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_sort_by.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_sort_by.Location = new System.Drawing.Point(0, -2);
      this.lbl_sort_by.Name = "lbl_sort_by";
      this.lbl_sort_by.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
      this.lbl_sort_by.Size = new System.Drawing.Size(124, 18);
      this.lbl_sort_by.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_10PX_BLACK;
      this.lbl_sort_by.TabIndex = 0;
      this.lbl_sort_by.Text = "xFilterBy";
      this.lbl_sort_by.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_points
      // 
      this.lbl_points.BackColor = System.Drawing.Color.Transparent;
      this.lbl_points.Font = new System.Drawing.Font("Open Sans Semibold", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_points.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_points.Location = new System.Drawing.Point(208, 43);
      this.lbl_points.Name = "lbl_points";
      this.lbl_points.Size = new System.Drawing.Size(60, 25);
      this.lbl_points.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_10PX_BLACK;
      this.lbl_points.TabIndex = 8;
      this.lbl_points.Text = "xPoints";
      this.lbl_points.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lbl_redeemable
      // 
      this.lbl_redeemable.BackColor = System.Drawing.Color.Transparent;
      this.lbl_redeemable.Font = new System.Drawing.Font("Open Sans Semibold", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_redeemable.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_redeemable.Location = new System.Drawing.Point(134, 43);
      this.lbl_redeemable.Name = "lbl_redeemable";
      this.lbl_redeemable.Size = new System.Drawing.Size(79, 25);
      this.lbl_redeemable.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_10PX_BLACK;
      this.lbl_redeemable.TabIndex = 7;
      this.lbl_redeemable.Text = "xRedeemable";
      this.lbl_redeemable.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lbl_lock
      // 
      this.lbl_lock.BackColor = System.Drawing.Color.Transparent;
      this.lbl_lock.Font = new System.Drawing.Font("Open Sans Semibold", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_lock.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_lock.Location = new System.Drawing.Point(76, 43);
      this.lbl_lock.Name = "lbl_lock";
      this.lbl_lock.Size = new System.Drawing.Size(64, 25);
      this.lbl_lock.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_10PX_BLACK;
      this.lbl_lock.TabIndex = 6;
      this.lbl_lock.Text = "xLock";
      this.lbl_lock.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lbl_non_redeemable
      // 
      this.lbl_non_redeemable.BackColor = System.Drawing.Color.Transparent;
      this.lbl_non_redeemable.Font = new System.Drawing.Font("Open Sans Semibold", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_non_redeemable.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_non_redeemable.Location = new System.Drawing.Point(0, 43);
      this.lbl_non_redeemable.Name = "lbl_non_redeemable";
      this.lbl_non_redeemable.Size = new System.Drawing.Size(87, 25);
      this.lbl_non_redeemable.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_10PX_BLACK;
      this.lbl_non_redeemable.TabIndex = 5;
      this.lbl_non_redeemable.Text = "xNonRedeemable";
      this.lbl_non_redeemable.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // uc_promo_filter
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
      this.BackColor = System.Drawing.Color.Transparent;
      this.Controls.Add(this.pnl_info_promos);
      this.Margin = new System.Windows.Forms.Padding(0);
      this.MaximumSize = new System.Drawing.Size(280, 85);
      this.Name = "uc_promo_filter";
      this.Size = new System.Drawing.Size(280, 85);
      this.Load += new System.EventHandler(this.uc_promo_filter_Load);
      this.pnl_info_promos.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.CheckBox chk_re;
    private System.Windows.Forms.CheckBox chk_nr;
    private System.Windows.Forms.CheckBox chk_pt;
    private System.Windows.Forms.CheckBox chk_ca;
    private Controls.uc_label lbl_non_redeemable;
    private Controls.uc_label lbl_points;
    private Controls.uc_label lbl_redeemable;
    private Controls.uc_label lbl_lock;
    private System.Windows.Forms.Panel pnl_info_promos;
    private Controls.uc_label lbl_sort_by;
  }
}
