﻿namespace WSI.Cashier
{
  partial class frm_add_account_blacklist
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.lbl_blacklist_reason = new WSI.Cashier.Controls.uc_label();
      this.cb_blackList_reasons = new WSI.Cashier.Controls.uc_round_auto_combobox();
      this.lbl_blacklist_reason_description = new WSI.Cashier.Controls.uc_label();
      this.txt_blacklist_reason_description = new WSI.Cashier.Controls.uc_round_textbox();
      this.btn_ok = new WSI.Cashier.Controls.uc_round_button();
      this.btn_cancel = new WSI.Cashier.Controls.uc_round_button();
      this.lbl_msg_blink = new WSI.Cashier.Controls.uc_label();
      this.btn_keyboard = new WSI.Cashier.Controls.uc_round_button();
      this.pnl_data.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.Controls.Add(this.btn_keyboard);
      this.pnl_data.Controls.Add(this.lbl_msg_blink);
      this.pnl_data.Controls.Add(this.btn_ok);
      this.pnl_data.Controls.Add(this.btn_cancel);
      this.pnl_data.Controls.Add(this.txt_blacklist_reason_description);
      this.pnl_data.Controls.Add(this.lbl_blacklist_reason_description);
      this.pnl_data.Controls.Add(this.lbl_blacklist_reason);
      this.pnl_data.Controls.Add(this.cb_blackList_reasons);
      this.pnl_data.Size = new System.Drawing.Size(596, 326);
      // 
      // lbl_blacklist_reason
      // 
      this.lbl_blacklist_reason.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_blacklist_reason.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_blacklist_reason.Location = new System.Drawing.Point(12, 12);
      this.lbl_blacklist_reason.Name = "lbl_blacklist_reason";
      this.lbl_blacklist_reason.Size = new System.Drawing.Size(319, 23);
      this.lbl_blacklist_reason.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_blacklist_reason.TabIndex = 1020;
      this.lbl_blacklist_reason.Text = "xBlackListReason";
      // 
      // cb_blackList_reasons
      // 
      this.cb_blackList_reasons.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_blackList_reasons.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.cb_blackList_reasons.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.cb_blackList_reasons.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.cb_blackList_reasons.CornerRadius = 5;
      this.cb_blackList_reasons.DataSource = null;
      this.cb_blackList_reasons.DisplayMember = "";
      this.cb_blackList_reasons.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
      this.cb_blackList_reasons.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cb_blackList_reasons.DropDownWidth = 314;
      this.cb_blackList_reasons.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.cb_blackList_reasons.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_blackList_reasons.FormattingEnabled = true;
      this.cb_blackList_reasons.FormatValidator = null;
      this.cb_blackList_reasons.IsDroppedDown = false;
      this.cb_blackList_reasons.ItemHeight = 40;
      this.cb_blackList_reasons.Location = new System.Drawing.Point(12, 35);
      this.cb_blackList_reasons.MaxDropDownItems = 8;
      this.cb_blackList_reasons.Name = "cb_blackList_reasons";
      this.cb_blackList_reasons.OnFocusOpenListBox = true;
      this.cb_blackList_reasons.SelectedIndex = -1;
      this.cb_blackList_reasons.SelectedItem = null;
      this.cb_blackList_reasons.SelectedValue = null;
      this.cb_blackList_reasons.SelectionLength = 0;
      this.cb_blackList_reasons.SelectionStart = 0;
      this.cb_blackList_reasons.Size = new System.Drawing.Size(314, 40);
      this.cb_blackList_reasons.Sorted = false;
      this.cb_blackList_reasons.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.BASIC;
      this.cb_blackList_reasons.TabIndex = 1021;
      this.cb_blackList_reasons.TabStop = false;
      this.cb_blackList_reasons.ValueMember = "";
      // 
      // lbl_blacklist_reason_description
      // 
      this.lbl_blacklist_reason_description.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_blacklist_reason_description.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_blacklist_reason_description.Location = new System.Drawing.Point(12, 87);
      this.lbl_blacklist_reason_description.Name = "lbl_blacklist_reason_description";
      this.lbl_blacklist_reason_description.Size = new System.Drawing.Size(319, 23);
      this.lbl_blacklist_reason_description.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_blacklist_reason_description.TabIndex = 1022;
      this.lbl_blacklist_reason_description.Text = "xBlackListReasonDescription";
      // 
      // txt_blacklist_reason_description
      // 
      this.txt_blacklist_reason_description.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_blacklist_reason_description.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_blacklist_reason_description.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_blacklist_reason_description.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_blacklist_reason_description.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_blacklist_reason_description.CornerRadius = 5;
      this.txt_blacklist_reason_description.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_blacklist_reason_description.Location = new System.Drawing.Point(12, 113);
      this.txt_blacklist_reason_description.MaxLength = 100;
      this.txt_blacklist_reason_description.Multiline = true;
      this.txt_blacklist_reason_description.Name = "txt_blacklist_reason_description";
      this.txt_blacklist_reason_description.PasswordChar = '\0';
      this.txt_blacklist_reason_description.ReadOnly = false;
      this.txt_blacklist_reason_description.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
      this.txt_blacklist_reason_description.SelectedText = "";
      this.txt_blacklist_reason_description.SelectionLength = 0;
      this.txt_blacklist_reason_description.SelectionStart = 0;
      this.txt_blacklist_reason_description.Size = new System.Drawing.Size(572, 124);
      this.txt_blacklist_reason_description.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.MULTILINE;
      this.txt_blacklist_reason_description.TabIndex = 1023;
      this.txt_blacklist_reason_description.TabStop = false;
      this.txt_blacklist_reason_description.Tag = "";
      this.txt_blacklist_reason_description.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_blacklist_reason_description.UseSystemPasswordChar = false;
      this.txt_blacklist_reason_description.WaterMark = null;
      this.txt_blacklist_reason_description.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // btn_ok
      // 
      this.btn_ok.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_ok.FlatAppearance.BorderSize = 0;
      this.btn_ok.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_ok.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_ok.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_ok.Image = null;
      this.btn_ok.IsSelected = false;
      this.btn_ok.Location = new System.Drawing.Point(418, 268);
      this.btn_ok.Name = "btn_ok";
      this.btn_ok.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_ok.Size = new System.Drawing.Size(166, 48);
      this.btn_ok.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_ok.TabIndex = 1025;
      this.btn_ok.Text = "XOK";
      this.btn_ok.UseVisualStyleBackColor = false;
      this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
      // 
      // btn_cancel
      // 
      this.btn_cancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btn_cancel.FlatAppearance.BorderSize = 0;
      this.btn_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_cancel.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_cancel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_cancel.Image = null;
      this.btn_cancel.IsSelected = false;
      this.btn_cancel.Location = new System.Drawing.Point(246, 268);
      this.btn_cancel.Name = "btn_cancel";
      this.btn_cancel.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_cancel.Size = new System.Drawing.Size(166, 48);
      this.btn_cancel.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_cancel.TabIndex = 1024;
      this.btn_cancel.Text = "XCANCEL";
      this.btn_cancel.UseVisualStyleBackColor = false;
      // 
      // lbl_msg_blink
      // 
      this.lbl_msg_blink.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_msg_blink.BackColor = System.Drawing.Color.Transparent;
      this.lbl_msg_blink.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_msg_blink.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.lbl_msg_blink.Location = new System.Drawing.Point(12, 242);
      this.lbl_msg_blink.Name = "lbl_msg_blink";
      this.lbl_msg_blink.Padding = new System.Windows.Forms.Padding(3);
      this.lbl_msg_blink.Size = new System.Drawing.Size(572, 25);
      this.lbl_msg_blink.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_RED;
      this.lbl_msg_blink.TabIndex = 1026;
      this.lbl_msg_blink.Text = "xError Message";
      this.lbl_msg_blink.Visible = false;
      // 
      // btn_keyboard
      // 
      this.btn_keyboard.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
      this.btn_keyboard.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(47)))), ((int)(((byte)(50)))));
      this.btn_keyboard.FlatAppearance.BorderSize = 0;
      this.btn_keyboard.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_keyboard.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_keyboard.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_keyboard.Image = global::WSI.Cashier.Properties.Resources.keyboard;
      this.btn_keyboard.IsSelected = false;
      this.btn_keyboard.Location = new System.Drawing.Point(16, 268);
      this.btn_keyboard.Name = "btn_keyboard";
      this.btn_keyboard.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_keyboard.Size = new System.Drawing.Size(75, 45);
      this.btn_keyboard.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.TERTIARY;
      this.btn_keyboard.TabIndex = 1027;
      this.btn_keyboard.TabStop = false;
      this.btn_keyboard.UseVisualStyleBackColor = false;
      this.btn_keyboard.Click += new System.EventHandler(this.btn_keyboard_Click);
      // 
      // frm_add_account_blacklist
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(596, 381);
      this.Name = "frm_add_account_blacklist";
      this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
      this.Text = "frm_add_account_blacklist";
      this.pnl_data.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private Controls.uc_label lbl_blacklist_reason;
    private Controls.uc_round_auto_combobox cb_blackList_reasons;
    private Controls.uc_label lbl_blacklist_reason_description;
    private Controls.uc_round_textbox txt_blacklist_reason_description;
    private Controls.uc_round_button btn_ok;
    private Controls.uc_round_button btn_cancel;
    private Controls.uc_label lbl_msg_blink;
    private Controls.uc_round_button btn_keyboard;
  }
}