﻿namespace WSI.Cashier
{
  partial class frm_gaming_tables_win_loss
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
      this.btn_win_loss_cancel = new WSI.Cashier.Controls.uc_round_button();
      this.btn_win_loss_edit = new WSI.Cashier.Controls.uc_round_button();
      this.pnl_win_loss_last_report_hour = new WSI.Cashier.Controls.uc_round_panel();
      this.lbl_not_reported_hour_error = new WSI.Cashier.Controls.uc_label();
      this.lbl_win_loss_last_reported_hour_amount = new WSI.Cashier.Controls.uc_label();
      this.lbl_win_loss_last_reported_hour_date = new WSI.Cashier.Controls.uc_label();
      this.lbl_win_loss_last_reported_hour_time_slot = new WSI.Cashier.Controls.uc_label();
      this.dgv_win_loss = new WSI.Cashier.Controls.uc_DataGridView();
      this.lbl_win_loss_error = new WSI.Cashier.Controls.uc_label();
      this.pnl_win_loss_error = new WSI.Common.uc_transparent_panel();
      this.pnl_win_loss_buttons = new WSI.Common.uc_transparent_panel();
      this.pnl_data.SuspendLayout();
      this.pnl_win_loss_last_report_hour.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dgv_win_loss)).BeginInit();
      this.pnl_win_loss_error.SuspendLayout();
      this.pnl_win_loss_buttons.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.Controls.Add(this.pnl_win_loss_buttons);
      this.pnl_data.Controls.Add(this.pnl_win_loss_error);
      this.pnl_data.Controls.Add(this.pnl_win_loss_last_report_hour);
      this.pnl_data.Controls.Add(this.dgv_win_loss);
      this.pnl_data.Size = new System.Drawing.Size(449, 377);
      // 
      // btn_win_loss_cancel
      // 
      this.btn_win_loss_cancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_win_loss_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btn_win_loss_cancel.FlatAppearance.BorderSize = 0;
      this.btn_win_loss_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_win_loss_cancel.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_win_loss_cancel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_win_loss_cancel.Image = null;
      this.btn_win_loss_cancel.IsSelected = false;
      this.btn_win_loss_cancel.Location = new System.Drawing.Point(0, 0);
      this.btn_win_loss_cancel.Name = "btn_win_loss_cancel";
      this.btn_win_loss_cancel.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_win_loss_cancel.Size = new System.Drawing.Size(155, 60);
      this.btn_win_loss_cancel.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_win_loss_cancel.TabIndex = 3;
      this.btn_win_loss_cancel.Text = "XCANCEL";
      this.btn_win_loss_cancel.UseVisualStyleBackColor = false;
      // 
      // btn_win_loss_edit
      // 
      this.btn_win_loss_edit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_win_loss_edit.FlatAppearance.BorderSize = 0;
      this.btn_win_loss_edit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_win_loss_edit.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_win_loss_edit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_win_loss_edit.Image = null;
      this.btn_win_loss_edit.IsSelected = false;
      this.btn_win_loss_edit.Location = new System.Drawing.Point(256, 0);
      this.btn_win_loss_edit.Name = "btn_win_loss_edit";
      this.btn_win_loss_edit.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_win_loss_edit.Size = new System.Drawing.Size(155, 60);
      this.btn_win_loss_edit.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_win_loss_edit.TabIndex = 4;
      this.btn_win_loss_edit.Text = "XEDIT";
      this.btn_win_loss_edit.UseVisualStyleBackColor = false;
      this.btn_win_loss_edit.Click += new System.EventHandler(this.btn_win_loss_edit_Click);
      // 
      // pnl_win_loss_last_report_hour
      // 
      this.pnl_win_loss_last_report_hour.BackColor = System.Drawing.Color.Transparent;
      this.pnl_win_loss_last_report_hour.BorderColor = System.Drawing.Color.Transparent;
      this.pnl_win_loss_last_report_hour.Controls.Add(this.lbl_not_reported_hour_error);
      this.pnl_win_loss_last_report_hour.Controls.Add(this.lbl_win_loss_last_reported_hour_amount);
      this.pnl_win_loss_last_report_hour.Controls.Add(this.lbl_win_loss_last_reported_hour_date);
      this.pnl_win_loss_last_report_hour.Controls.Add(this.lbl_win_loss_last_reported_hour_time_slot);
      this.pnl_win_loss_last_report_hour.CornerRadius = 10;
      this.pnl_win_loss_last_report_hour.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.pnl_win_loss_last_report_hour.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.pnl_win_loss_last_report_hour.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.pnl_win_loss_last_report_hour.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.pnl_win_loss_last_report_hour.HeaderHeight = 35;
      this.pnl_win_loss_last_report_hour.HeaderSubText = null;
      this.pnl_win_loss_last_report_hour.HeaderText = null;
      this.pnl_win_loss_last_report_hour.Location = new System.Drawing.Point(18, 21);
      this.pnl_win_loss_last_report_hour.Name = "pnl_win_loss_last_report_hour";
      this.pnl_win_loss_last_report_hour.PanelBackColor = System.Drawing.Color.White;
      this.pnl_win_loss_last_report_hour.Size = new System.Drawing.Size(411, 80);
      this.pnl_win_loss_last_report_hour.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL_WHITE;
      this.pnl_win_loss_last_report_hour.TabIndex = 5;
      // 
      // lbl_not_reported_hour_error
      // 
      this.lbl_not_reported_hour_error.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_not_reported_hour_error.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_not_reported_hour_error.Location = new System.Drawing.Point(6, 46);
      this.lbl_not_reported_hour_error.Name = "lbl_not_reported_hour_error";
      this.lbl_not_reported_hour_error.Size = new System.Drawing.Size(402, 22);
      this.lbl_not_reported_hour_error.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLUE;
      this.lbl_not_reported_hour_error.TabIndex = 3;
      this.lbl_not_reported_hour_error.Text = "xNotReportedHour";
      this.lbl_not_reported_hour_error.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_win_loss_last_reported_hour_amount
      // 
      this.lbl_win_loss_last_reported_hour_amount.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_win_loss_last_reported_hour_amount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_win_loss_last_reported_hour_amount.Location = new System.Drawing.Point(259, 46);
      this.lbl_win_loss_last_reported_hour_amount.Name = "lbl_win_loss_last_reported_hour_amount";
      this.lbl_win_loss_last_reported_hour_amount.Size = new System.Drawing.Size(150, 22);
      this.lbl_win_loss_last_reported_hour_amount.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_win_loss_last_reported_hour_amount.TabIndex = 1;
      this.lbl_win_loss_last_reported_hour_amount.Text = "xAmount";
      this.lbl_win_loss_last_reported_hour_amount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_win_loss_last_reported_hour_date
      // 
      this.lbl_win_loss_last_reported_hour_date.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_win_loss_last_reported_hour_date.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_win_loss_last_reported_hour_date.Location = new System.Drawing.Point(116, 46);
      this.lbl_win_loss_last_reported_hour_date.Name = "lbl_win_loss_last_reported_hour_date";
      this.lbl_win_loss_last_reported_hour_date.Size = new System.Drawing.Size(100, 22);
      this.lbl_win_loss_last_reported_hour_date.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLUE;
      this.lbl_win_loss_last_reported_hour_date.TabIndex = 2;
      this.lbl_win_loss_last_reported_hour_date.Text = "xDate";
      this.lbl_win_loss_last_reported_hour_date.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_win_loss_last_reported_hour_time_slot
      // 
      this.lbl_win_loss_last_reported_hour_time_slot.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_win_loss_last_reported_hour_time_slot.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_win_loss_last_reported_hour_time_slot.Location = new System.Drawing.Point(5, 46);
      this.lbl_win_loss_last_reported_hour_time_slot.Name = "lbl_win_loss_last_reported_hour_time_slot";
      this.lbl_win_loss_last_reported_hour_time_slot.Size = new System.Drawing.Size(105, 22);
      this.lbl_win_loss_last_reported_hour_time_slot.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLUE;
      this.lbl_win_loss_last_reported_hour_time_slot.TabIndex = 0;
      this.lbl_win_loss_last_reported_hour_time_slot.Text = "xTimeSlot";
      this.lbl_win_loss_last_reported_hour_time_slot.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // dgv_win_loss
      // 
      this.dgv_win_loss.AllowUserToAddRows = false;
      this.dgv_win_loss.AllowUserToDeleteRows = false;
      this.dgv_win_loss.AllowUserToResizeColumns = false;
      this.dgv_win_loss.AllowUserToResizeRows = false;
      this.dgv_win_loss.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.dgv_win_loss.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.dgv_win_loss.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.dgv_win_loss.ColumnHeaderHeight = 35;
      this.dgv_win_loss.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
      dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      dataGridViewCellStyle1.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
      dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
      dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
      this.dgv_win_loss.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
      this.dgv_win_loss.ColumnHeadersHeight = 35;
      this.dgv_win_loss.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
      this.dgv_win_loss.CornerRadius = 10;
      this.dgv_win_loss.DefaultCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.dgv_win_loss.DefaultCellForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.dgv_win_loss.DefaultCellSelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.dgv_win_loss.DefaultCellSelectionForeColor = System.Drawing.Color.White;
      dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      dataGridViewCellStyle2.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.White;
      dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
      this.dgv_win_loss.DefaultCellStyle = dataGridViewCellStyle2;
      this.dgv_win_loss.EnableHeadersVisualStyles = false;
      this.dgv_win_loss.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.dgv_win_loss.GridColor = System.Drawing.Color.LightGray;
      this.dgv_win_loss.HeaderBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
      this.dgv_win_loss.HeaderDefaultCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.dgv_win_loss.HeaderDefaultCellForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.dgv_win_loss.HeaderImages = null;
      this.dgv_win_loss.Location = new System.Drawing.Point(18, 113);
      this.dgv_win_loss.MultiSelect = false;
      this.dgv_win_loss.Name = "dgv_win_loss";
      this.dgv_win_loss.ReadOnly = true;
      this.dgv_win_loss.RowHeadersVisible = false;
      this.dgv_win_loss.RowHeadersWidth = 40;
      this.dgv_win_loss.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
      this.dgv_win_loss.RowTemplate.Height = 40;
      this.dgv_win_loss.RowTemplateHeight = 35;
      this.dgv_win_loss.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
      this.dgv_win_loss.Size = new System.Drawing.Size(411, 117);
      this.dgv_win_loss.Style = WSI.Cashier.Controls.uc_DataGridView.DataGridViewStyle.WITH_ALL_ROUND_CORNERS;
      this.dgv_win_loss.TabIndex = 2;
      this.dgv_win_loss.SelectionChanged += new System.EventHandler(this.dgv_win_loss_SelectionChanged);
      // 
      // lbl_win_loss_error
      // 
      this.lbl_win_loss_error.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_win_loss_error.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.lbl_win_loss_error.Location = new System.Drawing.Point(5, 5);
      this.lbl_win_loss_error.Name = "lbl_win_loss_error";
      this.lbl_win_loss_error.Size = new System.Drawing.Size(402, 44);
      this.lbl_win_loss_error.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_RED;
      this.lbl_win_loss_error.TabIndex = 6;
      this.lbl_win_loss_error.Text = "xError";
      // 
      // pnl_win_loss_error
      // 
      this.pnl_win_loss_error.Controls.Add(this.lbl_win_loss_error);
      this.pnl_win_loss_error.Image = null;
      this.pnl_win_loss_error.Location = new System.Drawing.Point(18, 234);
      this.pnl_win_loss_error.Name = "pnl_win_loss_error";
      this.pnl_win_loss_error.Opacity = 0;
      this.pnl_win_loss_error.Size = new System.Drawing.Size(411, 59);
      this.pnl_win_loss_error.TabIndex = 7;
      // 
      // pnl_win_loss_buttons
      // 
      this.pnl_win_loss_buttons.Controls.Add(this.btn_win_loss_cancel);
      this.pnl_win_loss_buttons.Controls.Add(this.btn_win_loss_edit);
      this.pnl_win_loss_buttons.Image = null;
      this.pnl_win_loss_buttons.Location = new System.Drawing.Point(18, 294);
      this.pnl_win_loss_buttons.Name = "pnl_win_loss_buttons";
      this.pnl_win_loss_buttons.Opacity = 0;
      this.pnl_win_loss_buttons.Size = new System.Drawing.Size(411, 59);
      this.pnl_win_loss_buttons.TabIndex = 8;
      // 
      // frm_gaming_tables_win_loss
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(449, 432);
      this.Name = "frm_gaming_tables_win_loss";
      this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
      this.Style = WSI.Cashier.Controls.frm_base.BaseFormStyle.PRINCIPAL;
      this.Text = "frm_win_loss_chips";
      this.pnl_data.ResumeLayout(false);
      this.pnl_win_loss_last_report_hour.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.dgv_win_loss)).EndInit();
      this.pnl_win_loss_error.ResumeLayout(false);
      this.pnl_win_loss_buttons.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private Controls.uc_round_panel pnl_win_loss_last_report_hour;
    private Controls.uc_round_button btn_win_loss_edit;
    private Controls.uc_round_button btn_win_loss_cancel;
    private Controls.uc_label lbl_win_loss_last_reported_hour_time_slot;
    private Controls.uc_label lbl_win_loss_last_reported_hour_amount;
    private Controls.uc_DataGridView dgv_win_loss;
    private Controls.uc_label lbl_win_loss_error;
    private Common.uc_transparent_panel pnl_win_loss_error;
    private Common.uc_transparent_panel pnl_win_loss_buttons;
    private Controls.uc_label lbl_win_loss_last_reported_hour_date;
    private Controls.uc_label lbl_not_reported_hour_error;
  }
}