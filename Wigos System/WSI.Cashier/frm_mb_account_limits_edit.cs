//------------------------------------------------------------------------------
// Copyright � 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_mb_account_limits_edit.cs
// 
//   DESCRIPTION: Implements the following classes:
//                1. frm_mb_account_limits_edit
//
//        AUTHOR: JML
// 
// CREATION DATE: 12-NOV-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 12-NOV-2014 JML    First release.
// 14-MAR-2016 FOS     Fixed Bug 9357: New design corrections
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WSI.Common;
using WSI.Cashier.Controls;

namespace WSI.Cashier
{
  public partial class frm_mb_account_limits_edit : frm_base
  {
    #region Attributes

    string m_account_id;
    MBCardData m_card_data = new MBCardData();

    Currency m_initial_total_limit;
    Currency m_initial_recharge_limit;
    Int32 m_initial_number_of_recharges_limit;

    private System.Globalization.NumberFormatInfo m_nfi;

    Auditor m_auditor;

    #endregion // Attributes

    #region Constants

    const int AUDIT_NAME_CASHIER_CONFIGURATION = 26; // Configuration
    const int AUDIT_TITLE_ID = 6500 + 4;  // Modification of 
    const int AUDIT_FIELD_ID = 6500 + 2;

    #endregion // Constants

    #region Constructor

    public frm_mb_account_limits_edit()
    {
      InitializeComponent();

      InitializeControlResources();
    } // frm_mb_account_limits_edit

    #endregion // Constructor

    #region Private Methods

    /// <summary>
    /// Initialize NLS strings
    /// </summary>
    private void InitializeControlResources()
    {
      // - NLS Strings:
      //   - Title
      this.FormTitle = Resource.String("STR_FRM_MB_ACCOUNT_USER_LIMIT");

      //   - Labels
      chk_total_limit.Text = Resource.String("STR_FRM_MB_ACCOUNT_USER_LIMIT_BY_SESSION");
      chk_recharge_limit.Text = Resource.String("STR_FRM_MB_ACCOUNT_USER_LIMIT_BY_RECHARGE");
      chk_number_of_recharges.Text = Resource.String("STR_FRM_MB_ACCOUNT_USER_LIMIT_BY_NUMBER_OF_RECHARGES");

      //   - Buttons
      btn_ok.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_OK");
      btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");

      // - Images:

    } // InitializeControlResources

    private void Init()
    {
      this.Location = new Point(1024 / 2 - this.Width / 2, 768 / 2 - this.Height / 2 - 70);

      m_card_data = new MBCardData();

      // Get Card data
      if (!CashierBusinessLogic.DB_MBCardGetAllData(Int64.Parse(this.m_account_id), m_card_data))
      {
        Log.Error("LoadCardByTrackNumber. DB_MBCardGetAllData: Error reading card.");

        return;
      }

      m_nfi = System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat;

      if (m_card_data.TotalLimit > 0)
      {
        txt_total_limit.Enabled = true;
        chk_total_limit.Checked = true;
        txt_total_limit.Text = m_card_data.TotalLimit.ToString();
      }
      else
      {
        txt_total_limit.Text = "";
        txt_total_limit.Enabled = false;
        chk_total_limit.Checked = false;
      }
      if (m_card_data.RechargeLimit > 0)
      {
        txt_recharge_limit.Enabled = true;
        chk_recharge_limit.Checked = true;
        txt_recharge_limit.Text = m_card_data.RechargeLimit.ToString();
      }
      else
      {
        txt_recharge_limit.Text = "";
        txt_recharge_limit.Enabled = false;
        chk_recharge_limit.Checked = false;
      }
      if (m_card_data.NumberOfRechargesLimit > 0)
      {
        txt_number_of_recharges.Enabled = true;
        chk_number_of_recharges.Checked = true;
        txt_number_of_recharges.Text = m_card_data.NumberOfRechargesLimit.ToString();
      }
      else
      {
        txt_number_of_recharges.Text = "";
        txt_number_of_recharges.Enabled = false;
        chk_number_of_recharges.Checked = false;
      }

      m_initial_total_limit = m_card_data.TotalLimit;
      m_initial_recharge_limit = m_card_data.RechargeLimit;
      m_initial_number_of_recharges_limit = m_card_data.NumberOfRechargesLimit;
            
      //Shows OSK if Automatic OSK it's enabled
      WSIKeyboard.Toggle();

    } // Init

    protected override void OnShown(EventArgs e)
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_mb_account_limits_edit", Log.Type.Message);

      base.OnShown(e);

      if (chk_total_limit.Checked)
      {
        chk_total_limit_CheckedChanged(null, null);
      }
      else if (chk_recharge_limit.Checked)
      {
        chk_recharge_limit_CheckedChanged(null, null);
      }
      else if (chk_number_of_recharges.Checked)
      {
        chk_number_of_recharges_CheckedChanged(null, null);
      }
    }

    #endregion // Private Methods

    #region Public Methods

    public void GetUserData(string Id)
    {
      m_account_id = Id;
 
      Init();
    } // GetUserData(Id)

    #endregion // Public Methods

    #region events

    private void btn_ok_Click(object sender, EventArgs e)
    {
      Decimal _new_total_limit;
      Decimal _new_recharge_limit;
      Int32 _new_number_of_recharges_limit;

      String _str_new_total_limit;
      String _str_m_initial_total_limit;
      String _str_new_recharge_limit;
      String _str_m_initial_recharge_limit;
      String _str_new_number_of_recharges_limit;
      String _str_m_initial_number_of_recharges_limit;

      string[] _message_params;

      this.timer1.Enabled = true;

      _message_params = new string[] { "", "" };

      Decimal.TryParse(txt_total_limit.Text.Replace(m_nfi.CurrencySymbol, ""), out _new_total_limit);
      Decimal.TryParse(txt_recharge_limit.Text.Replace(m_nfi.CurrencySymbol, ""), out _new_recharge_limit);
      Int32.TryParse(txt_number_of_recharges.Text.Replace(m_nfi.CurrencyGroupSeparator, ""), out _new_number_of_recharges_limit);

      // MB limits should be greater than cero if check box eneble is checked
      if (chk_total_limit.Checked)
      {
        if (_new_total_limit == 0)
        {
          this.txt_total_limit.Focus();
          this.lbl_msg_blink.Text = Resource.String("STR_FRM_MB_LIMIT_ERROR", Resource.String("STR_FRM_MB_ACCOUNT_USER_LIMIT_BY_SESSION"));
          this.lbl_msg_blink.Visible = true;

          return;
        }
      }
      else
      {
        _new_total_limit = 0;
      }

      if (chk_recharge_limit.Checked)
      {
        if (_new_recharge_limit == 0)
        {
          this.txt_recharge_limit.Focus();
          this.lbl_msg_blink.Text = Resource.String("STR_FRM_MB_LIMIT_ERROR", Resource.String("STR_FRM_MB_ACCOUNT_USER_LIMIT_BY_RECHARGE"));
          this.lbl_msg_blink.Visible = true;

          return;
        }
      }
      else
      {
        _new_recharge_limit = 0;
      }

      if (chk_number_of_recharges.Checked)
      {
        if (_new_number_of_recharges_limit == 0)
        {
          this.txt_number_of_recharges.Focus();
          this.lbl_msg_blink.Text = Resource.String("STR_FRM_MB_LIMIT_ERROR", Resource.String("STR_FRM_MB_ACCOUNT_USER_LIMIT_BY_NUMBER_OF_RECHARGES"));
          this.lbl_msg_blink.Visible = true;

          return;
        }
      }
      else
      {
        _new_number_of_recharges_limit = 0;
      }

      // MB "total limit" should be equal or greater than "recharge limit" if check boxes are checked
      if (chk_total_limit.Checked && chk_recharge_limit.Checked)
      {
        if (_new_total_limit < _new_recharge_limit)
        {
          this.txt_total_limit.Focus();

          _message_params[0] = Resource.String("STR_FRM_MB_ACCOUNT_USER_LIMIT_BY_RECHARGE");
          _message_params[1] = Resource.String("STR_FRM_MB_ACCOUNT_USER_LIMIT_BY_SESSION");

          this.lbl_msg_blink.Text = Resource.String("STR_FRM_MB_LIMITS_ERROR", _message_params);
          this.lbl_msg_blink.Visible = true;

          return;
        }
      }

      // Limits ok. Save & audit changes.
      if (m_initial_total_limit != _new_total_limit
        || m_initial_recharge_limit != _new_recharge_limit
        || m_initial_number_of_recharges_limit != _new_number_of_recharges_limit)
      {
        CashierBusinessLogic.DB_UpdateMBCardLimits(this.m_account_id, _new_total_limit, _new_recharge_limit, _new_number_of_recharges_limit);

        _str_new_total_limit = (_new_total_limit == 0 ? "---" : _new_total_limit.ToString());
        _str_m_initial_total_limit = (m_initial_total_limit == 0 ? "---" : m_initial_total_limit.ToString());
        _str_new_recharge_limit = (_new_recharge_limit == 0 ? "---" : _new_recharge_limit.ToString());
        _str_m_initial_recharge_limit = (m_initial_recharge_limit == 0 ? "---" : m_initial_recharge_limit.ToString());
        _str_new_number_of_recharges_limit = (_new_number_of_recharges_limit == 0 ? "---" : _new_number_of_recharges_limit.ToString());
        _str_m_initial_number_of_recharges_limit = (m_initial_number_of_recharges_limit == 0 ? "---" : m_initial_number_of_recharges_limit.ToString());


        m_auditor = new Auditor(AUDIT_NAME_CASHIER_CONFIGURATION);
        m_auditor.SetName(AUDIT_TITLE_ID, Resource.String("STR_FORMAT_GENERAL_NAME_MB"), this.m_account_id + " - " + m_card_data.HolderName, "", "", "");
        // detail
        if (m_initial_total_limit != _new_total_limit)
        {
          m_auditor.SetField(AUDIT_FIELD_ID, Resource.String("STR_FRM_MB_ACCOUNT_USER_LIMIT_BY_SESSION"), _str_new_total_limit, _str_m_initial_total_limit, "", "");
        }
        if (m_initial_recharge_limit != _new_recharge_limit)
        {
          m_auditor.SetField(AUDIT_FIELD_ID, Resource.String("STR_FRM_MB_ACCOUNT_USER_LIMIT_BY_RECHARGE"), _str_new_recharge_limit, _str_m_initial_recharge_limit, "", "");
        }
        if (m_initial_number_of_recharges_limit != _new_number_of_recharges_limit)
        {
          m_auditor.SetField(AUDIT_FIELD_ID, Resource.String("STR_FRM_MB_ACCOUNT_USER_LIMIT_BY_NUMBER_OF_RECHARGES"), _str_new_number_of_recharges_limit, _str_m_initial_number_of_recharges_limit, "", "");
        }

        // save
        m_auditor.Notify(ENUM_GUI.CASHIER, Cashier.UserId, Cashier.UserName, Cashier.TerminalName);

      }

      WSIKeyboard.Hide();
      this.Dispose();

    } // btn_ok_Click

    private void btn_cancel_Click(object sender, EventArgs e)
    {
      WSIKeyboard.Hide();
      this.Dispose();
    }  // btn_cancel_Click

    private void btn_keyboard_Click(object sender, EventArgs e)
    {
      //Shows OSK
      WSIKeyboard.ForceToggle();
    }  // btn_keyboard_Click

    private void txt_total_limit_Validating(object sender, CancelEventArgs e)
    {
      Decimal _temp_value;
      Decimal.TryParse(txt_total_limit.Text.Replace(m_nfi.CurrencySymbol, ""), out _temp_value);
      txt_total_limit.Text = ((Currency)_temp_value).ToString();
    } // txt_total_limit_Validating

    private void txt_recharge_limit_Validating(object sender, CancelEventArgs e)
    {
      Decimal _temp_value;
      Decimal.TryParse(txt_recharge_limit.Text.Replace(m_nfi.CurrencySymbol, ""), out _temp_value);
      txt_recharge_limit.Text = ((Currency)_temp_value).ToString();
    } // txt_recharge_limit_Validating

    private void txt_number_of_recharges_Validating(object sender, CancelEventArgs e)
    {
      Int32 _temp_value;
      Int32.TryParse(txt_number_of_recharges.Text.Replace(m_nfi.CurrencySymbol, ""), out _temp_value);
      txt_number_of_recharges.Text = _temp_value.ToString();
    } // txt_number_of_recharges_Validating

    private void txt_number_KeyPress(object sender, KeyPressEventArgs e)
    {
      String _temp_number;

      _temp_number = "";

      if (sender.Equals(txt_total_limit))
      {
        if (txt_total_limit.SelectedText.Length > 0)
        {
          txt_total_limit.Text = txt_total_limit.Text.Replace(txt_total_limit.SelectedText, "");
        }
        _temp_number = txt_total_limit.Text;
      }
      if (sender.Equals(txt_recharge_limit)) 
      {
        if (txt_recharge_limit.SelectedText.Length > 0)
        {
          txt_recharge_limit.Text = txt_recharge_limit.Text.Replace(txt_recharge_limit.SelectedText, "");
        }
        _temp_number = txt_recharge_limit.Text;
      }
      if (sender.Equals(txt_number_of_recharges))
      {
        if (txt_number_of_recharges.SelectedText.Length > 0)
        {
          txt_number_of_recharges.Text = txt_number_of_recharges.Text.Replace(txt_number_of_recharges.SelectedText, "");
        }
        _temp_number = txt_number_of_recharges.Text;
      }

      switch (e.KeyChar)
      {
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
        case '.':

          if ((_temp_number.IndexOf(m_nfi.CurrencyDecimalSeparator) > 0 && _temp_number.IndexOf(m_nfi.CurrencyDecimalSeparator) < _temp_number.Length - 2) // Decimals length
            || (_temp_number.Length > 8 && _temp_number.IndexOf(m_nfi.CurrencyDecimalSeparator) < 0 && e.KeyChar.ToString() != m_nfi.CurrencyDecimalSeparator)
            || (_temp_number.Length > 11 || (sender.Equals(txt_number_of_recharges) && _temp_number.Length > 8)) ) // max length
          {
            e.KeyChar = '\0';
            break;
          }

          _temp_number += e.KeyChar;

          if (sender.Equals(txt_total_limit))
          {
            txt_total_limit.Text = _temp_number;
          }
          if (sender.Equals(txt_recharge_limit))
          {
            txt_recharge_limit.Text = _temp_number;
          }
          if (sender.Equals(txt_number_of_recharges))
          {
            if (e.KeyChar != '.')
            {
              txt_number_of_recharges.Text = _temp_number;
            }
            else
            {
              e.KeyChar = '\0';
            }
          }
          e.Handled = true;
          break;

        case '\b':
          // Backspace is used
          if (_temp_number.Length > 0)
          {
            _temp_number = _temp_number.Substring(0, _temp_number.Length - 1);
          }
          if (sender.Equals(txt_total_limit))
          {
            txt_total_limit.Text = _temp_number;
          }
          if (sender.Equals(txt_recharge_limit))
          {
            txt_recharge_limit.Text = _temp_number;
          }
          if (sender.Equals(txt_number_of_recharges))
          {
            txt_number_of_recharges.Text = _temp_number;
          }
          e.Handled = true;

         break;

        //case '\r':
        //  btn_ok_click(null, null);
        //  break;

        default:
          e.KeyChar = '\0';
          break;
      } // switch
    } // txt_number_KeyPress

    private void chk_total_limit_CheckedChanged(object sender, EventArgs e)
    {
      if (chk_total_limit.Checked)
      {
        txt_total_limit.Enabled = true;
        txt_total_limit.Text = m_initial_total_limit.ToString();
        txt_total_limit.Focus();
      }
      else
      {
        txt_total_limit.Text = "";
        txt_total_limit.Enabled = false;
      }
    } // chk_total_limit_CheckedChanged

    private void chk_recharge_limit_CheckedChanged(object sender, EventArgs e)
    {
      if (chk_recharge_limit.Checked)
      {
        txt_recharge_limit.Enabled = true;
        txt_recharge_limit.Text = m_initial_recharge_limit.ToString();
        txt_recharge_limit.Focus();
      }
      else
      {
        txt_recharge_limit.Text = "";
        txt_recharge_limit.Enabled = false;
      }
    } // chk_recharge_limit_CheckedChanged

    private void chk_number_of_recharges_CheckedChanged(object sender, EventArgs e)
    {
      if (chk_number_of_recharges.Checked)
      {
        txt_number_of_recharges.Enabled = true;
        txt_number_of_recharges.Text = m_initial_number_of_recharges_limit.ToString();
        txt_number_of_recharges.Focus();
      }
      else
      {
        txt_number_of_recharges.Text = "";
        txt_number_of_recharges.Enabled = false;
      }
    } // chk_number_of_recharges_CheckedChanged

    private void timer1_Tick(object sender, EventArgs e)
    {
      if (lbl_msg_blink.Visible == true)
      {
        lbl_msg_blink.Visible = false;
        timer1.Enabled = false;
      }
    } // timer1_Tick

    private void txt_number_Click(object sender, EventArgs e)
    {

      if (sender.Equals(txt_total_limit))
      {
        txt_total_limit.Text = ((Currency)Format.ParseCurrency(txt_total_limit.Text)).ToStringWithoutSymbols();
      }
      if (sender.Equals(txt_recharge_limit))
      {
        txt_recharge_limit.Text = ((Currency)Format.ParseCurrency(txt_recharge_limit.Text)).ToStringWithoutSymbols();
      }
      if (sender.Equals(txt_number_of_recharges))
      {
        txt_number_of_recharges.Text = Format.CompactNumber(decimal.Parse(txt_number_of_recharges.Text));
      }
 
    } 

    #endregion //events

   }
}