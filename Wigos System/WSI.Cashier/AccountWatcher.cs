//------------------------------------------------------------------------------
// Copyright � 2007-2009 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Account_Watcher.cs
// 
//   DESCRIPTION: Implements the Account_Watcher class, to keep on-screen data
//                completely updated
//
//        AUTHOR: 
// 
// CREATION DATE: 08-JUN-2009 
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 08-JUN-2009 APB    Header.
//                    Extended to support Mobile_Bank accounts as well
// 25-MAY-2012 SSC    Moved CardData, PlaySession and PlayerTrackingData classes to WSI.Common
// 24-JUL-2012 RCI    AccountWatcher: Also, take in account the modifications in table play_sessions.
// 10-JAN-2014 JPJ    Now it's possible to clear the information stored in the thread.
// 23-JUL-2014 RRR & RCI  Fixed Bug WIG-1121: Error in account refresh.
// 24-FEB-2017 FAV    Bug 25131: The Cashier memory is growing
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using WSI.Common;
using System.Threading;

namespace WSI.Cashier
{
  class AccountWatcher
  {
    public const Int16 ACCOUNT_TYPE_PLAYER = 0;
    public const Int16 ACCOUNT_TYPE_MOBILE_BANK = 1;

    public const Int32 ACCOUNT_READ_TIMEOUT = 10 * 1000;  // 10 secs

    public delegate void AccountChangedHandler ();
    public event AccountChangedHandler AccountChangedEvent;
    private Int64 m_account_id;
    private Int16 m_account_type;
    private Thread m_thread;
    private Boolean m_stopped;
    private CardData m_card = new CardData ();
    private MBCardData m_mb_card = new MBCardData();
    private Int64 m_ts_card;
    private ManualResetEvent m_ev_read = new ManualResetEvent(true);

    private AutoResetEvent m_ev_account_changed = new AutoResetEvent(false);

    private String m_track_number = "";

    public String TrackNumber
    {
      get 
      { 
        return m_track_number; 
      }
      set 
      { 
        m_track_number = value; 
      }
    }

    public CardData Card
    {
      get
      {
        while (true)
        {
          lock (this)
          {
            if (m_account_type != ACCOUNT_TYPE_PLAYER)
            {
              break;
            }
            if (m_ev_read.WaitOne(0))
            {
              return m_card;
            }
          }

          if (!m_ev_read.WaitOne(ACCOUNT_READ_TIMEOUT))
          {
            break;
          }
        }

        return new CardData();
      }
    }

    public MBCardData MBCard
    {
      get
      {
        while (true)
        {
          lock (this)
          {
            if (m_account_type != ACCOUNT_TYPE_MOBILE_BANK)
            {
              break;
            }
            if (m_ev_read.WaitOne(0))
            {
              return m_mb_card;
            }
          }

          if (!m_ev_read.WaitOne(ACCOUNT_READ_TIMEOUT))
          {
            break;
          }
        }

        return new MBCardData();
      }
    }

    public Int64 AccountId
    {
      get { return m_account_id; }
    }

    public Int16 AccountType
    {
      get { return m_account_type; }
    }

    public void Start ()
    {
      if ( m_thread == null )
      {
        m_thread = new Thread (this.AccountWatcherThread);
        m_thread.Name = "AccountWatcherThread";
        m_thread.Start ();
        m_stopped = false;
      }
    }

    public void Stop ()
    {
      m_stopped = true;
      m_thread.Join (5000);
      m_thread = null;
    }

    public void SetAccount(Object Account)
    {
      String _track_data;
      Boolean _is_multisite_member;

      _track_data = String.Empty;
      _is_multisite_member = GeneralParam.GetBoolean("Site", "MultiSiteMember", false);

      if (Account == null)
      {
        return;
      }

      lock ( this )
      {
        if (Account.GetType().ToString() == "WSI.Common.CardData")
        {
          m_account_type = AccountWatcher.ACCOUNT_TYPE_PLAYER;
          m_card = (CardData)Account;
          m_account_id = m_card.AccountId;
          m_ts_card = m_card.Timestamp;
          _track_data = m_card.TrackData;
          
        }
        else if (Account.GetType().ToString() == "WSI.Common.MBCardData")
        {
          m_account_type = AccountWatcher.ACCOUNT_TYPE_MOBILE_BANK;
          m_mb_card = (MBCardData)Account;
          m_account_id = m_mb_card.CardId;
          m_ts_card = m_mb_card.Timestamp;
        }
        else
        {
          Log.Error("Unexpected account type: " + Account.GetType().ToString());

          return;
        }

        m_ev_read.Set();
      }
      if (AccountChangedEvent != null)
      {
        AccountChangedEvent();
      }

      if (_is_multisite_member)
      {
        // Trigger to sincronize account 
        if (Account.GetType().ToString() == "WSI.Common.CardData"
            && !String.IsNullOrEmpty(_track_data))
        {

          // Get internal track data from external trackdata
          if (WSI.Common.CardNumber.TrackDataToInternal(_track_data, out _track_data))
          {
            if (!CommonMultiSite.ForceSynchronization_Accounts(_track_data))
            {
              Log.Warning("Can't trigger to sincronize account....");
            }
          }
      }
      

        
      }

    }

    public void RefreshAccount(Object Account)
    {
      if (Account == null)
      {
        return;
      }

      lock (this)
      {
        if (Account.GetType().ToString() == "WSI.Common.CardData")
        {
          if (m_account_type == AccountWatcher.ACCOUNT_TYPE_PLAYER)
          {
            CardData _card;

            _card = (CardData)Account;

            if (m_account_id == _card.AccountId
             && m_ts_card == _card.Timestamp)
            {
              m_ev_read.Reset();
              m_ev_account_changed.Set();
            }
          }
        }
        else if (Account.GetType().ToString() == "WSI.Cashier.MBCardData")
        {
          if (m_account_type == AccountWatcher.ACCOUNT_TYPE_MOBILE_BANK)
          {
            MBCardData _mb_card;

            _mb_card = (MBCardData)Account;

            if (m_account_id == _mb_card.CardId
             && m_ts_card == _mb_card.Timestamp)
            {
              m_ev_read.Reset();
              m_ev_account_changed.Set();
            }
          }
        }
        else
        {
          Log.Error("Unexpected account type: " + Account.GetType().ToString());

          return;
        }
      }
    }

    public void ForceRefresh ()
    {
      m_ev_read.Reset();
      m_ev_account_changed.Set();
    }

    public void ClearAccount()
    {
      m_account_id = 0;
      m_account_type=0;
      m_account_id=0;
      m_ts_card=0;
 
    }

    public Boolean CheckAccountChanged(SqlConnection SqlConn,
                                       Int16 AccountType,
                                       Int64 AccountId,
                                       Int64 OldTs,
                                       out Int64 NewTs)
    {
      String _str_sql;
      Object _obj;

      NewTs = -1;
      _str_sql = "";

      try
      {
        switch (AccountType)
        {
          case ACCOUNT_TYPE_MOBILE_BANK:
            _str_sql += "SELECT  CAST (MB_TIMESTAMP AS BIGINT) ";
            _str_sql += "  FROM  MOBILE_BANKS ";
            _str_sql += " WHERE  MB_ACCOUNT_ID = @pAccountId ";
            break;

          case ACCOUNT_TYPE_PLAYER:
          default:
            _str_sql += "SELECT  CASE WHEN CAST (PS_TIMESTAMP AS BIGINT) > CAST (AC_TIMESTAMP AS BIGINT) THEN CAST (PS_TIMESTAMP AS BIGINT) ELSE CAST (AC_TIMESTAMP AS BIGINT) END ";
            _str_sql += "  FROM  ACCOUNTS ";
            _str_sql += "  LEFT  JOIN PLAY_SESSIONS ON PS_PLAY_SESSION_ID = AC_CURRENT_PLAY_SESSION_ID ";
            _str_sql += " WHERE  AC_ACCOUNT_ID = @pAccountId ";
            break;
        }

        using (SqlCommand _sql_cmd = new SqlCommand(_str_sql, SqlConn))
        {
          _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;

          _obj = _sql_cmd.ExecuteScalar();
          if (_obj != null)
          {
            NewTs = (Int64)_obj;
          }
        }
      }
      catch { }

      return (OldTs != NewTs);
    } // CheckAccountChanged

    private void AccountWatcherThread ()
    {
      SqlConnection _sql_conn;
      CardData _local_card = new CardData ();
      MBCardData _mb_local_card = new MBCardData();
      Int16 _account_type; 
      Int64 _account_id;
      Int64 _old_ts;
      Int64 _new_ts;
      Boolean _changed;
      Boolean _reassign_card_ts;

      _sql_conn = null;

      try
      {
        while ( !m_stopped )
        {
          try
          {
            m_ev_account_changed.WaitOne (1000, false);

            if ( m_account_id == 0 )
            {
              lock (this)
              {
                m_ev_read.Set();
              }
              continue;
            }
            if ( _sql_conn == null )
            {
              _sql_conn = WGDB.Connection ();
              if ( _sql_conn == null )
              {
                continue;
              }
            }
            if ( _sql_conn.State != ConnectionState.Open )
            {
              _sql_conn.Close ();
              _sql_conn = null;

              continue;
            }
            
            lock (this)
            {
              _account_type = m_account_type;
              _account_id = m_account_id;
              _old_ts = m_ts_card;
            }

            _reassign_card_ts = false;

            if (!CheckAccountChanged(_sql_conn, _account_type, _account_id, _old_ts, out _new_ts))
            {
              lock (this)
              {
                // RRR & RCI 23-JUL-2014: Re-check the account has not changed.
                if (_account_type == m_account_type && _account_id == m_account_id)
                {
                  m_ev_read.Set();
                  continue;
                }
              }
            }

            lock(this)
            {
              if (_account_type != m_account_type || _account_id != m_account_id)
              {
                // If changed, read the new account.
                _account_type = m_account_type;
                _account_id = m_account_id;
                _reassign_card_ts = true;
              }
            }

            switch (_account_type)
            {
              case ACCOUNT_TYPE_MOBILE_BANK:
                _mb_local_card = new MBCardData();
                CashierBusinessLogic.DB_MBCardGetAllData(_account_id, _mb_local_card);
                break;

              case ACCOUNT_TYPE_PLAYER:
              default:
                _local_card = new CardData();
                CardData.DB_CardGetAllData(_account_id, _local_card, false);
                break;
            } // switch

            _changed = false;
            lock (this)
            {
              if (_account_type == m_account_type
               && _account_id == m_account_id)
              {
                _changed = true;
                m_ev_read.Set();

                if (_account_type == ACCOUNT_TYPE_MOBILE_BANK)
                {
                  m_mb_card = _mb_local_card;
                }
                else
                {
                  m_card = _local_card;
                  if (_reassign_card_ts)
                  {
                    _new_ts = m_card.Timestamp;
                  }
                }
                m_ts_card = _new_ts;
              }
            } // lock

            if (_changed)
            {
              if (AccountChangedEvent != null)
              {
                AccountChangedEvent();
              }
            }

          } // try

          catch  (Exception ex)
          {
            Log.Exception (ex);
          }
        } // while ( !m_stopped )
      } // try

      finally
      {
        if ( _sql_conn != null )
        {
          try { _sql_conn.Close(); }
          catch { }
          _sql_conn = null;
        }
      }
    } // AccountWatcherThread

  } // Class AccountWatcher

}
