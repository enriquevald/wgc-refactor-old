namespace WSI.Cashier
{
  partial class frm_mb_account_user_edit
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose (bool disposing)
    {
      if ( disposing && ( components != null ) )
      {
        components.Dispose ();
      }
      base.Dispose (disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent ()
    {
      this.components = new System.ComponentModel.Container();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_mb_account_user_edit));
      this.btn_keyboard = new WSI.Cashier.Controls.uc_round_button();
      this.groupBox1 = new WSI.Cashier.Controls.uc_round_panel();
      this.lbl_mode = new WSI.Cashier.Controls.uc_label();
      this.lbl_account = new WSI.Cashier.Controls.uc_label();
      this.lbl_mode_value = new WSI.Cashier.Controls.uc_label();
      this.lbl_account_id_value = new WSI.Cashier.Controls.uc_label();
      this.lbl_name = new WSI.Cashier.Controls.uc_label();
      this.txt_name = new WSI.Cashier.CharacterTextBox();
      this.pictureBox1 = new System.Windows.Forms.PictureBox();
      this.btn_mode_to_anonymous = new WSI.Cashier.Controls.uc_round_button();
      this.btn_mode_to_personal = new WSI.Cashier.Controls.uc_round_button();
      this.btn_cancel = new WSI.Cashier.Controls.uc_round_button();
      this.btn_ok = new WSI.Cashier.Controls.uc_round_button();
      this.timer1 = new System.Windows.Forms.Timer(this.components);
      this.lbl_msg_blink = new WSI.Cashier.Controls.uc_label();
      this.pnl_data.SuspendLayout();
      this.groupBox1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.Controls.Add(this.lbl_msg_blink);
      this.pnl_data.Controls.Add(this.btn_keyboard);
      this.pnl_data.Controls.Add(this.groupBox1);
      this.pnl_data.Controls.Add(this.btn_cancel);
      this.pnl_data.Controls.Add(this.btn_ok);
      this.pnl_data.Size = new System.Drawing.Size(640, 241);
      // 
      // btn_keyboard
      // 
      this.btn_keyboard.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.btn_keyboard.FlatAppearance.BorderSize = 0;
      this.btn_keyboard.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_keyboard.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_keyboard.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.btn_keyboard.Image = ((System.Drawing.Image)(resources.GetObject("btn_keyboard.Image")));
      this.btn_keyboard.Location = new System.Drawing.Point(20, 164);
      this.btn_keyboard.Margin = new System.Windows.Forms.Padding(0);
      this.btn_keyboard.Name = "btn_keyboard";
      this.btn_keyboard.Size = new System.Drawing.Size(83, 60);
      this.btn_keyboard.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.TERTIARY;
      this.btn_keyboard.TabIndex = 2;
      this.btn_keyboard.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.btn_keyboard.UseVisualStyleBackColor = false;
      this.btn_keyboard.Click += new System.EventHandler(this.btn_keyboard_Click);
      // 
      // groupBox1
      // 
      this.groupBox1.BackColor = System.Drawing.Color.Transparent;
      this.groupBox1.Controls.Add(this.lbl_mode);
      this.groupBox1.Controls.Add(this.lbl_account);
      this.groupBox1.Controls.Add(this.lbl_mode_value);
      this.groupBox1.Controls.Add(this.lbl_account_id_value);
      this.groupBox1.Controls.Add(this.lbl_name);
      this.groupBox1.Controls.Add(this.txt_name);
      this.groupBox1.Controls.Add(this.pictureBox1);
      this.groupBox1.Controls.Add(this.btn_mode_to_anonymous);
      this.groupBox1.Controls.Add(this.btn_mode_to_personal);
      this.groupBox1.CornerRadius = 10;
      this.groupBox1.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.groupBox1.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.groupBox1.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.groupBox1.HeaderHeight = 0;
      this.groupBox1.HeaderSubText = null;
      this.groupBox1.HeaderText = null;
      this.groupBox1.Location = new System.Drawing.Point(4, 4);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.groupBox1.Size = new System.Drawing.Size(626, 129);
      this.groupBox1.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.WITHOUT_HEAD;
      this.groupBox1.TabIndex = 0;
      // 
      // lbl_mode
      // 
      this.lbl_mode.AutoSize = true;
      this.lbl_mode.BackColor = System.Drawing.Color.Transparent;
      this.lbl_mode.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_mode.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.lbl_mode.Location = new System.Drawing.Point(97, 45);
      this.lbl_mode.Name = "lbl_mode";
      this.lbl_mode.Size = new System.Drawing.Size(57, 18);
      this.lbl_mode.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_mode.TabIndex = 2;
      this.lbl_mode.Text = "xMode";
      // 
      // lbl_account
      // 
      this.lbl_account.AutoSize = true;
      this.lbl_account.BackColor = System.Drawing.Color.Transparent;
      this.lbl_account.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_account.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.lbl_account.Location = new System.Drawing.Point(97, 15);
      this.lbl_account.Name = "lbl_account";
      this.lbl_account.Size = new System.Drawing.Size(75, 18);
      this.lbl_account.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_account.TabIndex = 0;
      this.lbl_account.Text = "xAccount";
      // 
      // lbl_mode_value
      // 
      this.lbl_mode_value.AutoSize = true;
      this.lbl_mode_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_mode_value.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_mode_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.lbl_mode_value.Location = new System.Drawing.Point(201, 45);
      this.lbl_mode_value.Name = "lbl_mode_value";
      this.lbl_mode_value.Size = new System.Drawing.Size(98, 18);
      this.lbl_mode_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_mode_value.TabIndex = 3;
      this.lbl_mode_value.Text = "XXXXXXXXX";
      // 
      // lbl_account_id_value
      // 
      this.lbl_account_id_value.AutoSize = true;
      this.lbl_account_id_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_account_id_value.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_account_id_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.lbl_account_id_value.Location = new System.Drawing.Point(201, 15);
      this.lbl_account_id_value.Name = "lbl_account_id_value";
      this.lbl_account_id_value.Size = new System.Drawing.Size(143, 18);
      this.lbl_account_id_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_account_id_value.TabIndex = 1;
      this.lbl_account_id_value.Text = "888888888888888";
      // 
      // lbl_name
      // 
      this.lbl_name.AutoSize = true;
      this.lbl_name.BackColor = System.Drawing.Color.Transparent;
      this.lbl_name.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_name.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.lbl_name.Location = new System.Drawing.Point(13, 92);
      this.lbl_name.Name = "lbl_name";
      this.lbl_name.Size = new System.Drawing.Size(62, 18);
      this.lbl_name.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_name.TabIndex = 7;
      this.lbl_name.Text = "XNAME";
      this.lbl_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // txt_name
      // 
      this.txt_name.AllowSpace = false;
      this.txt_name.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_name.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_name.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.txt_name.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.txt_name.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.txt_name.CornerRadius = 5;
      this.txt_name.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_name.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.txt_name.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.BaseMode;
      this.txt_name.Location = new System.Drawing.Point(100, 80);
      this.txt_name.MaxLength = 50;
      this.txt_name.Multiline = false;
      this.txt_name.Name = "txt_name";
      this.txt_name.PasswordChar = '\0';
      this.txt_name.ReadOnly = false;
      this.txt_name.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_name.SelectedText = "";
      this.txt_name.SelectionLength = 0;
      this.txt_name.SelectionStart = 0;
      this.txt_name.Size = new System.Drawing.Size(519, 40);
      this.txt_name.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_name.TabIndex = 6;
      this.txt_name.TabStop = false;
      this.txt_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_name.UseSystemPasswordChar = false;
      this.txt_name.WaterMark = null;
      // 
      // pictureBox1
      // 
      this.pictureBox1.Image = global::WSI.Cashier.Properties.Resources.anonymous_user;
      this.pictureBox1.Location = new System.Drawing.Point(0, 3);
      this.pictureBox1.Name = "pictureBox1";
      this.pictureBox1.Size = new System.Drawing.Size(85, 65);
      this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
      this.pictureBox1.TabIndex = 0;
      this.pictureBox1.TabStop = false;
      // 
      // btn_mode_to_anonymous
      // 
      this.btn_mode_to_anonymous.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.btn_mode_to_anonymous.FlatAppearance.BorderSize = 0;
      this.btn_mode_to_anonymous.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_mode_to_anonymous.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_mode_to_anonymous.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.btn_mode_to_anonymous.Image = null;
      this.btn_mode_to_anonymous.Location = new System.Drawing.Point(464, 15);
      this.btn_mode_to_anonymous.Name = "btn_mode_to_anonymous";
      this.btn_mode_to_anonymous.Size = new System.Drawing.Size(155, 50);
      this.btn_mode_to_anonymous.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.TERTIARY;
      this.btn_mode_to_anonymous.TabIndex = 4;
      this.btn_mode_to_anonymous.Text = "XCHANGE TO ANONYMOUS MODE";
      this.btn_mode_to_anonymous.UseVisualStyleBackColor = false;
      this.btn_mode_to_anonymous.Click += new System.EventHandler(this.btn_anonym_Click);
      // 
      // btn_mode_to_personal
      // 
      this.btn_mode_to_personal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.btn_mode_to_personal.FlatAppearance.BorderSize = 0;
      this.btn_mode_to_personal.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_mode_to_personal.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_mode_to_personal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.btn_mode_to_personal.Image = null;
      this.btn_mode_to_personal.Location = new System.Drawing.Point(464, 15);
      this.btn_mode_to_personal.Name = "btn_mode_to_personal";
      this.btn_mode_to_personal.Size = new System.Drawing.Size(155, 50);
      this.btn_mode_to_personal.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.TERTIARY;
      this.btn_mode_to_personal.TabIndex = 5;
      this.btn_mode_to_personal.Text = "XCHANGE TO PERSONAL MODE";
      this.btn_mode_to_personal.UseVisualStyleBackColor = false;
      this.btn_mode_to_personal.Click += new System.EventHandler(this.btn_personal_Click);
      // 
      // btn_cancel
      // 
      this.btn_cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_cancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.btn_cancel.FlatAppearance.BorderSize = 0;
      this.btn_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_cancel.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_cancel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.btn_cancel.Image = null;
      this.btn_cancel.Location = new System.Drawing.Point(299, 164);
      this.btn_cancel.Name = "btn_cancel";
      this.btn_cancel.Size = new System.Drawing.Size(155, 60);
      this.btn_cancel.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_cancel.TabIndex = 3;
      this.btn_cancel.Text = "XCANCEL";
      this.btn_cancel.UseVisualStyleBackColor = false;
      this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
      // 
      // btn_ok
      // 
      this.btn_ok.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_ok.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.btn_ok.FlatAppearance.BorderSize = 0;
      this.btn_ok.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_ok.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_ok.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.btn_ok.Image = null;
      this.btn_ok.Location = new System.Drawing.Point(468, 164);
      this.btn_ok.Name = "btn_ok";
      this.btn_ok.Size = new System.Drawing.Size(155, 60);
      this.btn_ok.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_ok.TabIndex = 4;
      this.btn_ok.Text = "XOK";
      this.btn_ok.UseVisualStyleBackColor = false;
      this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
      // 
      // timer1
      // 
      this.timer1.Interval = 4000;
      this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
      // 
      // lbl_msg_blink
      // 
      this.lbl_msg_blink.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_msg_blink.BackColor = System.Drawing.Color.Transparent;
      this.lbl_msg_blink.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_msg_blink.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.lbl_msg_blink.Location = new System.Drawing.Point(7, 139);
      this.lbl_msg_blink.Name = "lbl_msg_blink";
      this.lbl_msg_blink.Size = new System.Drawing.Size(623, 23);
      this.lbl_msg_blink.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_RED;
      this.lbl_msg_blink.TabIndex = 1;
      this.lbl_msg_blink.Text = "xPERSONAL NAME CANNOT BE BLANK";
      this.lbl_msg_blink.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.lbl_msg_blink.Visible = false;
      // 
      // frm_mb_account_user_edit
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(640, 296);
      this.ControlBox = false;
      this.HeaderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "frm_mb_account_user_edit";
      this.ShowIcon = false;
      this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
      this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
      this.Style = WSI.Cashier.Controls.frm_base.BaseFormStyle.PRINCIPAL;
      this.Text = "frm_mb_account_user_edit";
      this.Shown += new System.EventHandler(this.frm_mb_account_user_edit_Shown);
      this.pnl_data.ResumeLayout(false);
      this.groupBox1.ResumeLayout(false);
      this.groupBox1.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private CharacterTextBox txt_name;
    private WSI.Cashier.Controls.uc_label lbl_name;
    private System.Windows.Forms.PictureBox pictureBox1;
    private WSI.Cashier.Controls.uc_round_button btn_cancel;
    private WSI.Cashier.Controls.uc_round_button btn_ok;
    private WSI.Cashier.Controls.uc_round_panel groupBox1;
    private WSI.Cashier.Controls.uc_round_button btn_mode_to_anonymous;
    private WSI.Cashier.Controls.uc_round_button btn_mode_to_personal;
    private WSI.Cashier.Controls.uc_round_button  btn_keyboard;
    private WSI.Cashier.Controls.uc_label lbl_account;
    internal System.Windows.Forms.Timer timer1;
    private WSI.Cashier.Controls.uc_label lbl_account_id_value;
    private WSI.Cashier.Controls.uc_label lbl_mode;
    private WSI.Cashier.Controls.uc_label lbl_mode_value;
    private Controls.uc_label lbl_msg_blink;
  }
}