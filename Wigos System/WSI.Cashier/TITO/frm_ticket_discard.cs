//------------------------------------------------------------------------------
// Copyright © 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_ticket_discard.cs
// 
//   DESCRIPTION: Implements the following classes:
//                1. frm_ticket_discard
//
//        AUTHOR: David Hernández
// 
// CREATION DATE: 24-JAN-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 24-JAN-2014 DHA     First release.
//-------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WSI.Common;
using System.Data.SqlClient;
using WSI.Common.TITO;
using System.Collections;

namespace WSI.Cashier.TITO
{
  public partial class frm_ticket_discard : Controls.frm_base
  {
    private Int64 m_operation_id;
    private DataTable m_tickets_table;
    private Boolean m_last_ticket_mode;
    private static frm_yesno form_yes_no;
    private Boolean m_all_valids;

    #region Constructor
    public frm_ticket_discard()
    {
      InitializeComponent();

      form_yes_no = new frm_yesno();
      form_yes_no.Opacity = 0.6;
    }
    #endregion Constructor

    //------------------------------------------------------------------------------
    // PURPOSE : Dispose yes_no form.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    new public void Dispose()
    {
      form_yes_no.Dispose();
      form_yes_no = null;

      base.Dispose();
    } // Dispose

    #region Constants

    private const Int32 GRID_COLUMN_TICKET_ID = 0;
    private const Int32 GRID_COLUMN_TICKET_AUTO_NUM = 1;
    private const Int32 GRID_COLUMN_TICKET_MACHINE_NUMBER = 2;
    private const Int32 GRID_COLUMN_TICKET_NUMBER = 3;
    private const Int32 GRID_COLUMN_TICKET_NUMBER_STRING = 4;
    private const Int32 GRID_COLUMN_TICKET_TO_DISCARD = 5;
    private const Int32 GRID_COLUMN_TICKET_TO_DISCARD_STRING = 6;

    private const Int32 WIDTH_COLUMN_TICKET_ID = 0;
    private const Int32 WIDTH_COLUMN_TICKET_AUTO_NUM = 70;
    private const Int32 WIDTH_COLUMN_TICKET_MACHINE_NUMBER = 70;
    private const Int32 WIDTH_COLUMN_TICKET_NUMBER = 0;
    private const Int32 WIDTH_COLUMN_TICKET_NUMBER_STRING = 212;
    private const Int32 WIDTH_COLUMN_TICKET_TO_DISCARD = 0;
    private const Int32 WIDTH_COLUMN_TICKET_TO_DISCARD_STRING = 100;

    #endregion

    //------------------------------------------------------------------------------
    // PURPOSE : Init and Show the form.
    //
    //  PARAMS :
    //      - INPUT :
    //          - OperationId
    //          - ParentForm
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public void Show(Int64 OperationId, Boolean AllValids, Form Parent)
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_ticket_discard", Log.Type.Message);

      try
      {
        m_last_ticket_mode = GeneralParam.GetBoolean("TITO", "MassivePromotionsValidation.LastTicket", true);

        m_operation_id = OperationId;
        m_all_valids = AllValids;

        uc_ticket_reader.OnValidationNumberReadEvent += new uc_ticket_reader.ValidationNumberReadEventHandler(ProcessTicketNumber);
        uc_ticket_reader.ShowMessageBottom();

        InitializeControlResources();

        PopulateDataGrid();
        InitDataGrid();
        InitFocus();

        form_yes_no.Show(Parent);
        this.ShowDialog(form_yes_no);
        form_yes_no.Hide();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } // Show

    //------------------------------------------------------------------------------
    // PURPOSE : Init the controls resources.
    //
    //  PARAMS :
    //      - INPUT :
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void InitializeControlResources()
    {
      uc_ticket_reader.InitializeControlResources();

      this.FormTitle = m_all_valids ? Resource.String("STR_FRM_TITO_TICKETS_DISCARD_TITLE_ALL_VALIDS") : Resource.String("STR_FRM_TITO_TICKETS_DISCARD_TITLE");
      gb_tickets.HeaderText = Resource.String("STR_FRM_TITO_TICKETS_DISCARD_GROUP");
      btn_discard_all.Text = Resource.String("STR_FRM_TITO_TICKETS_DISCARD_DISCARD_ALL");
      btn_validate_all.Text = Resource.String("STR_FRM_TITO_TICKETS_DISCARD_VALIDATE_ALL");
      btn_accept.Text = Resource.String("STR_FRM_TITO_TICKETS_DISCARD_ACCEPT");
      btn_close.Text = Resource.String("STR_FRM_CONTAINER_USER_MSG_EXIT_TITLE");
      lbl_scan_ticket_mode.Text = m_last_ticket_mode ? Resource.String("STR_FRM_TITO_TICKETS_CANCEL_SCAN_TICKETS_MODE_LAST_TICKET") : Resource.String("STR_FRM_TITO_TICKETS_CANCEL_SCAN_TICKETS_MODE_SINGLE_TICKET");
    }

    public void InitFocus()
    {
      uc_ticket_reader.Select();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Populate DataGrid
    //
    //  PARAMS :
    //      - INPUT :
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void PopulateDataGrid()
    {
      StringBuilder _sb;
      bool _allow_pay_tickets_pending_print;

      _allow_pay_tickets_pending_print = GeneralParam.GetBoolean("TITO", "PendingPrint.AllowToRedeem", true);
      _sb = new StringBuilder();

      CreateTicketListTable();

      try
      {
        _sb.AppendLine("     SELECT   TI_TICKET_ID                                            ");
        _sb.AppendLine("            , ROW_NUMBER() OVER (ORDER BY TI_TICKET_ID) AS TI_AUTONUM ");
        _sb.AppendLine("	          , TI_MACHINE_NUMBER                                       ");
        _sb.AppendLine("	          , TI_VALIDATION_NUMBER                                    ");
        _sb.AppendLine("       FROM   ACCOUNT_OPERATIONS                                      ");
        _sb.AppendLine(" INNER JOIN   TICKETS                                                 ");
        _sb.AppendLine("         ON   AO_OPERATION_ID = TI_TRANSACTION_ID                     ");
        _sb.AppendLine("      WHERE   AO_OPERATION_ID = @pOperationId                         ");

        if (_allow_pay_tickets_pending_print)
        {
          _sb.AppendLine("        AND   (TI_STATUS = @pStatusValid                              ");
          _sb.AppendLine("         OR   TI_STATUS = @pStatusPendingPrint )                      ");
        }
        else
        {
          _sb.AppendLine("        AND   TI_STATUS = @pStatusValid                              ");
        }

        _sb.AppendLine("        AND   TI_CREATED_TERMINAL_TYPE = @pTerminalType               ");
        _sb.AppendLine("   ORDER BY   TI_TICKET_ID DESC                                       ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt).Value = m_operation_id;
            _cmd.Parameters.Add("@pStatusValid", SqlDbType.Int).Value = TITO_TICKET_STATUS.VALID;
            if(_allow_pay_tickets_pending_print) _cmd.Parameters.Add("@pStatusPendingPrint", SqlDbType.Int).Value = TITO_TICKET_STATUS.PENDING_PRINT;
            _cmd.Parameters.Add("@pTerminalType", SqlDbType.Int).Value = TITO_TERMINAL_TYPE.CASHIER;

            m_tickets_table.Load(_db_trx.ExecuteReader(_cmd));

          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return;
      }

      ProcessDataTable(m_tickets_table);

      dgv_tickets_list.DataSource = m_tickets_table;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Process data table fields
    //
    //  PARAMS :
    //      - INPUT :
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void ProcessDataTable(DataTable Tickets)
    {
      TITO_VALIDATION_TYPE _validation_type;
      Int64 _ticket_number;

      foreach (DataColumn _col in Tickets.Columns)
      {
        _col.ReadOnly = false;
      }

      foreach (DataRow _row in Tickets.Rows)
      {
        if (_row[GRID_COLUMN_TICKET_NUMBER] != DBNull.Value)
        {
          _ticket_number = ValidationNumberManager.UnFormatValidationNumber(_row[GRID_COLUMN_TICKET_NUMBER].ToString(), out _validation_type);
          _row[GRID_COLUMN_TICKET_NUMBER_STRING] = ValidationNumberManager.FormatValidationNumber(_ticket_number, (Int32)_validation_type, true);

          _row[GRID_COLUMN_TICKET_TO_DISCARD] = !m_all_valids;
          _row[GRID_COLUMN_TICKET_TO_DISCARD_STRING] = m_all_valids ? String.Empty : Resource.String("STR_FRM_TITO_TICKETS_DISCARD_ACTION_DISCARD");
        }
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Init DataGrid columns
    //
    //  PARAMS :
    //      - INPUT :
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void InitDataGrid()
    {

      //Ticket Id
      dgv_tickets_list.Columns[GRID_COLUMN_TICKET_ID].Width = WIDTH_COLUMN_TICKET_ID;
      dgv_tickets_list.Columns[GRID_COLUMN_TICKET_ID].Visible = false;
      dgv_tickets_list.Columns[GRID_COLUMN_TICKET_ID].Resizable = DataGridViewTriState.False;

      //AutoNumeric
      dgv_tickets_list.Columns[GRID_COLUMN_TICKET_AUTO_NUM].Width = WIDTH_COLUMN_TICKET_AUTO_NUM;
      dgv_tickets_list.Columns[GRID_COLUMN_TICKET_AUTO_NUM].Visible = true;
      dgv_tickets_list.Columns[GRID_COLUMN_TICKET_AUTO_NUM].HeaderText = Resource.String("STR_FRM_TITO_TICKETS_DISCARD_GRID_NUM_ROW");
      dgv_tickets_list.Columns[GRID_COLUMN_TICKET_AUTO_NUM].Resizable = DataGridViewTriState.False;
      dgv_tickets_list.Columns[GRID_COLUMN_TICKET_AUTO_NUM].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
      dgv_tickets_list.Columns[GRID_COLUMN_TICKET_AUTO_NUM].SortMode = DataGridViewColumnSortMode.NotSortable;

      //Machine Number
      dgv_tickets_list.Columns[GRID_COLUMN_TICKET_MACHINE_NUMBER].Width = WIDTH_COLUMN_TICKET_MACHINE_NUMBER;
      dgv_tickets_list.Columns[GRID_COLUMN_TICKET_MACHINE_NUMBER].Visible = true;
      dgv_tickets_list.Columns[GRID_COLUMN_TICKET_MACHINE_NUMBER].HeaderText = Resource.String("STR_FRM_TITO_TICKETS_DISCARD_GRID_SEQ");
      dgv_tickets_list.Columns[GRID_COLUMN_TICKET_MACHINE_NUMBER].Resizable = DataGridViewTriState.False;
      dgv_tickets_list.Columns[GRID_COLUMN_TICKET_MACHINE_NUMBER].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
      dgv_tickets_list.Columns[GRID_COLUMN_TICKET_MACHINE_NUMBER].SortMode = DataGridViewColumnSortMode.NotSortable;

      //Ticket Number
      dgv_tickets_list.Columns[GRID_COLUMN_TICKET_NUMBER].Width = WIDTH_COLUMN_TICKET_NUMBER;
      dgv_tickets_list.Columns[GRID_COLUMN_TICKET_NUMBER].Visible = false;

      //Ticket Number String
      dgv_tickets_list.Columns[GRID_COLUMN_TICKET_NUMBER_STRING].Width = WIDTH_COLUMN_TICKET_NUMBER_STRING;
      dgv_tickets_list.Columns[GRID_COLUMN_TICKET_NUMBER_STRING].Visible = true;
      dgv_tickets_list.Columns[GRID_COLUMN_TICKET_NUMBER_STRING].HeaderText = Resource.String("STR_FRM_TITO_TICKETS_DISCARD_GRID_TICKET_NUMBER");
      dgv_tickets_list.Columns[GRID_COLUMN_TICKET_NUMBER_STRING].Resizable = DataGridViewTriState.False;
      dgv_tickets_list.Columns[GRID_COLUMN_TICKET_NUMBER_STRING].SortMode = DataGridViewColumnSortMode.NotSortable;

      //Ticket Cancel
      dgv_tickets_list.Columns[GRID_COLUMN_TICKET_TO_DISCARD].Width = WIDTH_COLUMN_TICKET_TO_DISCARD;
      dgv_tickets_list.Columns[GRID_COLUMN_TICKET_TO_DISCARD].Visible = false;
      dgv_tickets_list.Columns[GRID_COLUMN_TICKET_TO_DISCARD].Resizable = DataGridViewTriState.False;

      //Ticket Cancel String
      dgv_tickets_list.Columns[GRID_COLUMN_TICKET_TO_DISCARD_STRING].Width = WIDTH_COLUMN_TICKET_TO_DISCARD_STRING;
      dgv_tickets_list.Columns[GRID_COLUMN_TICKET_TO_DISCARD_STRING].Visible = true;
      dgv_tickets_list.Columns[GRID_COLUMN_TICKET_TO_DISCARD_STRING].HeaderText = Resource.String("STR_FRM_TITO_TICKETS_DISCARD_GRID_ACTION");
      dgv_tickets_list.Columns[GRID_COLUMN_TICKET_TO_DISCARD_STRING].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
      dgv_tickets_list.Columns[GRID_COLUMN_TICKET_TO_DISCARD_STRING].Resizable = DataGridViewTriState.False;
      dgv_tickets_list.Columns[GRID_COLUMN_TICKET_TO_DISCARD_STRING].SortMode = DataGridViewColumnSortMode.NotSortable;

    }

    //------------------------------------------------------------------------------
    // PURPOSE : Create ticket datatable
    //
    //  PARAMS :
    //      - INPUT :
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void CreateTicketListTable()
    {
      DataColumn _column;

      m_tickets_table = new DataTable();

      //Ticket Id
      _column = m_tickets_table.Columns.Add("TI_TICKET_ID");
      _column.DataType = typeof(Int64);
      _column.Unique = true;

      //AutoNum
      _column = m_tickets_table.Columns.Add("TI_AUTONUM");
      _column.DataType = typeof(Int32);

      //Machine Number
      _column = m_tickets_table.Columns.Add("TI_MACHINE_NUMBER");
      _column.DataType = typeof(Int32);

      //Ticket Number
      _column = m_tickets_table.Columns.Add("TI_VALIDATION_NUMBER");
      _column.DataType = typeof(Int64);

      //Ticket Number (String)
      _column = m_tickets_table.Columns.Add("TI_TICKET_NUMBER_STRING");
      _column.DataType = typeof(String);

      //To Cancel
      _column = m_tickets_table.Columns.Add("TI_TO_DISCARD");
      _column.DataType = typeof(Boolean);

      //To Cancel String
      _column = m_tickets_table.Columns.Add("TI_TO_DISCARD_STRING");
      _column.DataType = typeof(String);

      m_tickets_table.PrimaryKey = new DataColumn[] { m_tickets_table.Columns["TICKET_ID"] };
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Process new ticket inserted.
    //
    //  PARAMS :
    //      - INPUT :
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void ProcessTicketNumber(String ValidationNumber, ref Boolean IsValid, out String ErrorStr)
    {
      DataRow[] _tickets_to_cancel;
      TITO_VALIDATION_TYPE _validation_type;
      Int64 _validation_number;
      Boolean _discard_tickets;

      _validation_number = 0;
      ErrorStr = Resource.String("STR_UC_TICKET_NOT_FOUND");
      IsValid = false;
      _discard_tickets = false;

      _validation_number = ValidationNumberManager.UnFormatValidationNumber(ValidationNumber, out _validation_type);
      _tickets_to_cancel = m_tickets_table.Select("TI_VALIDATION_NUMBER = " + _validation_number);

      // Scan every ticket mode
      if (!m_last_ticket_mode && _tickets_to_cancel.Length > 0)
      {
        _tickets_to_cancel[0][GRID_COLUMN_TICKET_TO_DISCARD] = false;
        _tickets_to_cancel[0][GRID_COLUMN_TICKET_TO_DISCARD_STRING] = String.Empty;
        ErrorStr = "";
        IsValid = true;
      }
      // Last ticket mode
      else if (m_last_ticket_mode && _tickets_to_cancel.Length > 0)
      {
        ErrorStr = "";
        IsValid = true;

        for (Int32 _idx_row = m_tickets_table.Rows.Count - 1; _idx_row >= 0; _idx_row--)
        {
          if (!_discard_tickets)
          {
            m_tickets_table.Rows[_idx_row][GRID_COLUMN_TICKET_TO_DISCARD] = false;
            m_tickets_table.Rows[_idx_row][GRID_COLUMN_TICKET_TO_DISCARD_STRING] = String.Empty;
          }
          else
          {
            m_tickets_table.Rows[_idx_row][GRID_COLUMN_TICKET_TO_DISCARD] = true;
            m_tickets_table.Rows[_idx_row][GRID_COLUMN_TICKET_TO_DISCARD_STRING] = Resource.String("STR_FRM_TITO_TICKETS_DISCARD_ACTION_DISCARD");
          }

          if ((Int64)m_tickets_table.Rows[_idx_row][GRID_COLUMN_TICKET_NUMBER] == _validation_number)
          {
            _discard_tickets = true;
          }
        }
      }
    }

    private void btn_discard_all_Click(object sender, EventArgs e)
    {
      foreach (DataRow _row in m_tickets_table.Rows)
      {
        _row[GRID_COLUMN_TICKET_TO_DISCARD] = true;
        _row[GRID_COLUMN_TICKET_TO_DISCARD_STRING] = Resource.String("STR_FRM_TITO_TICKETS_DISCARD_ACTION_DISCARD");
      }
    }

    private void btn_validate_all_Click(object sender, EventArgs e)
    {
      foreach (DataRow _row in m_tickets_table.Rows)
      {
        _row[GRID_COLUMN_TICKET_TO_DISCARD] = false;
        _row[GRID_COLUMN_TICKET_TO_DISCARD_STRING] = String.Empty;
      }
    }

    private void btn_accept_Click(object sender, EventArgs e)
    {
      CardData _card_data;
      ArrayList _voucher_list;
      List<Ticket> _tickets_undo;
      ENUM_TITO_OPERATIONS_RESULT _result;
      String _msg_text;
      Int64 _ticket_no_discardable;
      TITO_VALIDATION_TYPE _validation_type;

      _tickets_undo = new List<Ticket>();
      _msg_text = String.Empty;
      _result = ENUM_TITO_OPERATIONS_RESULT.OK;
      _ticket_no_discardable = 0;

      foreach (DataRow _row in m_tickets_table.Rows)
      {
        if ((Boolean)_row[GRID_COLUMN_TICKET_TO_DISCARD])
        {
          _tickets_undo.Add(Ticket.LoadTicket((Int64)_row[GRID_COLUMN_TICKET_ID]));
        }
      }

      if (_tickets_undo.Count > 0)
      {
        _card_data = new CardData();
        if (CardData.DB_CardGetAllData(_tickets_undo[0].CreatedAccountID, _card_data))
        {
          if (!OperationUndo.UndoTITOTicket(OperationCode.PROMOTION, _card_data, m_operation_id, Cashier.CashierSessionInfo(), _tickets_undo, out _voucher_list, out _ticket_no_discardable))
          {
            _result = ENUM_TITO_OPERATIONS_RESULT.AUX_OPERATION_ERROR;
          }
          else
          {
            VoucherPrint.Print(_voucher_list);
          }
        }
        else
        {
          _result = ENUM_TITO_OPERATIONS_RESULT.AUX_OPERATION_ERROR;
        }

        if (_result == ENUM_TITO_OPERATIONS_RESULT.OK)
        {
          _msg_text = Resource.String("STR_FRM_TITO_TICKETS_DISCARD_TICKETS_DISCARDED", _tickets_undo.Count);
          frm_message.Show(_msg_text, Resource.String("STR_APP_GEN_MSG_WARNING"), MessageBoxButtons.OK, MessageBoxIcon.Information, this.ParentForm);
        }
        else
        {
          if (_ticket_no_discardable > 0)
          {
            _ticket_no_discardable = ValidationNumberManager.UnFormatValidationNumber(_ticket_no_discardable.ToString(), out _validation_type);

            _msg_text = Resource.String("STR_TICKET_ERROR_STATUS_PLAYED", ValidationNumberManager.FormatValidationNumber(_ticket_no_discardable, (Int32)_validation_type, true)).Replace("\\r\\n\\r\\n", "\r\n\r\n");
            frm_message.Show(_msg_text, Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this.ParentForm);

            return;
          }
          else
          {
            _msg_text = Resource.String("STR_FRM_TITO_TICKETS_DISCARD_TICKETS_DISCARDED_ERROR");
            frm_message.Show(_msg_text, Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this.ParentForm);
          }
        }
      }

      form_yes_no.Hide();

      this.DialogResult = DialogResult.OK;

      Misc.WriteLog("[FORM CLOSE] frm_ticket_discard (accept)", Log.Type.Message);
      this.Close();
    }

    private void dgv_tickets_list_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
    {
      if (e.ColumnIndex == GRID_COLUMN_TICKET_TO_DISCARD_STRING)
      {
        if ((Boolean)dgv_tickets_list.Rows[e.RowIndex].Cells[GRID_COLUMN_TICKET_TO_DISCARD].Value)
        {
          dgv_tickets_list.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LightSalmon;
        }
        else
        {
          dgv_tickets_list.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.White;
        }

        e.FormattingApplied = true;
      }

    }

    private void dgv_tickets_list_SelectionChanged(object sender, EventArgs e)
    {
      dgv_tickets_list.ClearSelection();
    }

    private void btn_close_Click(object sender, EventArgs e)
    {
      form_yes_no.Hide();

      this.DialogResult = DialogResult.None;

      Misc.WriteLog("[FORM CLOSE] frm_ticket_discard (close)", Log.Type.Message);
      this.Close();
    }

  }
}