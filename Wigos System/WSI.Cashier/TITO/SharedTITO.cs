//------------------------------------------------------------------------------
// Copyright � 2007-2013 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: SharedTITO.cs
// 
//   DESCRIPTION: Declare all functions that will be shared 
//
//        AUTHOR: Humberto Braojos
//
// REVISION HISTORY:
// 
// Date        Author Description
// 09-HBB-2013 HBB    First release.


using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Windows.Forms;
using WSI.Common;
using WSI.Common.TITO;

namespace WSI.Cashier.TITO 
{
  public class SharedTITO
  {

    //------------------------------------------------------------------------------
    // PURPOSE : Search account associated to TerminalId (Cashier)
    //
    //  PARAMS :
    //      - INPUT:
    //        - TerminalId: Cashier Terminal ID
    //        - WithCreation: Indicates if account is created when it doesn't exists
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Account ID (Int64) if found; 0 if not found
    //
    // NOTES:
    //
    public static Int64 GetTerminalAccount(Int64 TerminalId, Boolean WithCreation)
    {
      Int64 _result;
      Int64 _new_account_id;
      Int64 _site_id;
      GU_USER_TYPE _user_type;
       
      _result = 0;
      _user_type = GU_USER_TYPE.SYS_TITO;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _result = GetTerminalAccount(TerminalId, _user_type, _db_trx.SqlTransaction);
          if ((_result == 0) && WithCreation)
          {
            _new_account_id = 0;
            _site_id = GeneralParam.GetInt32("Site", "Identifier");
            if (CreateAnonymousAccountForCashier(_site_id, _user_type, TerminalId, out _new_account_id, _db_trx.SqlTransaction))
            {
              _db_trx.Commit();
              _result = _new_account_id;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return _result;
    } // GetTerminalAccount


    static public Int64 GetTerminalAccount(Int64 TerminalId, GU_USER_TYPE UserType, SqlTransaction SqlTrx)
    {
      Int64 _result;
      StringBuilder _sb;
      SqlCommand _sql_cmd;
      SqlDataReader _reader;

      _result = 0;
      _reader = null;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT   AC_ACCOUNT_ID");
        _sb.AppendLine("   FROM   ACCOUNTS");
        _sb.AppendLine("  WHERE   AC_HOLDER_NICKNAME='" + TerminalId.ToString() + "'");
        _sb.AppendLine("    AND   AC_USER_TYPE=" + (Int32)UserType);

        _sql_cmd = new SqlCommand(_sb.ToString());
        _sql_cmd.Connection = SqlTrx.Connection;
        _sql_cmd.Transaction = SqlTrx;

        _reader = _sql_cmd.ExecuteReader();

        if ((_reader != null) && _reader.Read())
        {
          _result = Convert.ToInt64(_reader[0]);
        }
      }
      finally
      {
        if (_reader != null)
        {
          _reader.Close();
        }
      }

      return _result;
    } // GetTerminalAccount

    //------------------------------------------------------------------------------
    // PURPOSE : Asociates internal account with Cashier ID. This matching method
    //           is used temporary
    //
    //  PARAMS :
    //      - INPUT:
    //        - UserType: type for asociated account (SYS_TITO)
    //        - TerminalId: Cashier Terminal ID
    //        - AccountId: Account ID
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True if all went OK; False otherwise
    //
    // NOTES:
    //
    private static Boolean MatchAccountWithCashier(GU_USER_TYPE UserType, Int64 TerminalId, Int64 AccountId, SqlTransaction SqlTrx)
    {
      Boolean _result;
      StringBuilder _sb;
      SqlCommand _sql_cmd;

      _result = false;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("UPDATE   ACCOUNTS ");
        _sb.AppendLine("   SET   AC_USER_TYPE = @pUserType ");
        _sb.AppendLine("       , AC_HOLDER_NICKNAME = @pTerminalId ");
        _sb.AppendLine(" WHERE   AC_ACCOUNT_ID = @pAccountId");

        _sql_cmd = new SqlCommand(_sb.ToString());
        _sql_cmd.Connection = SqlTrx.Connection;
        _sql_cmd.Transaction = SqlTrx;

        _sql_cmd.Parameters.Add("@pUserType", SqlDbType.Int).Value = (Int32)UserType;
        _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.NVarChar).Value = TerminalId.ToString();
        _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;

        _result = _sql_cmd.ExecuteNonQuery() > 0;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        _result = false;
      }

      return _result;
    } // MatchAccountWithCashier

    //------------------------------------------------------------------------------
    // PURPOSE : Creates new internal(anonymous) account and associates it with Cashier.
    //           Before creation verifies if cashier has matched account.
    //
    //  PARAMS :
    //      - INPUT:
    //        - SiteId: Site ID for new account (trackdata)
    //        - UserType: User type for new account (trackdata)
    //        - TerminalId: Cashier Terminal ID
    //        - SqlTrx: common transaction
    //
    //      - OUTPUT :
    //        - AccountId: ID of created account
    //
    // RETURNS :
    //      - True if account was created; False otherwise
    //
    // NOTES:
    //
    public static Boolean CreateAnonymousAccountForCashier(Int64 SiteId, GU_USER_TYPE UserType, Int64 TerminalId, out Int64 AccountId, SqlTransaction SqlTrx)
    {
      Boolean _result = false;
      String _external_track_data;

      AccountId = 0;

      

      _external_track_data = CreateCardNumber(SiteId, SqlTrx);
      if (!String.IsNullOrEmpty(_external_track_data))
      {
        _result = CashierBusinessLogic.DB_CreateCard(_external_track_data, out AccountId, SqlTrx);
        if (_result)
        {
          _result = MatchAccountWithCashier(UserType, TerminalId, AccountId, SqlTrx);
        }
      }

      return _result;
    } // CreateAnonymousAccount

    //------------------------------------------------------------------------------
    // PURPOSE : Create a new Card Number for internal account, when app is running
    //           in TITO mode, cashier is open, and it doen's exists account for ticket
    //           operations
    //
    //  PARAMS :
    //      - INPUT:
    //        - TerminalId: Cashier Terminal ID
    //        - UserType: Account user type
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Account ID (Int64) if found; 0 if not found
    //
    // NOTES:
    //
    private static String CreateCardNumber(Int64 SiteId, SqlTransaction SqlTrx)
    {
      String _result;
      Int32 _card_type;
      Int64 _last_card_sequence;
      String _external_track_data;
      String _internal_track_data;

      _result = "";
      _card_type = 0;
      _last_card_sequence = 0;

      _last_card_sequence = GetLastCardSequence(SiteId, SqlTrx);
      if (_last_card_sequence == 0)
      {
        Log.Error("CardDataNG.CreateCardNumber: unable to create Internal Card Number");
        return _result;
      }

      _last_card_sequence++;

      // Card Num generation
      _internal_track_data = String.Concat(String.Format("{0:D4}", SiteId), String.Format("{0:D9}", _last_card_sequence));

      //
      // calling auxiliar algorithm
      if (CardNumber.TrackDataToExternal(out _external_track_data, _internal_track_data, _card_type/*CARD_TYPE_PLAYER*/))
      {
        //
        // number is returned only if las used sequence is saved
        if (UpdateLastCardSequence(SiteId, _last_card_sequence, SqlTrx))
        {
          _result = _external_track_data;
        }
        else
        {
          Log.Error("CardDataNG.CreateCardNumber: unable to update Internal Card Sequence");
        }
      }

      return _result;
    } // CreateCardNumber

    //------------------------------------------------------------------------------
    // PURPOSE : Return the last sequence used during CARD_GENERATION
    //
    //  PARAMS :
    //      - INPUT:
    //        - SiteId: Site ID 
    //        - SqlTransaction: common transaction for operation
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Sequence ID (Int64) if found; 0 if not found
    //
    // NOTES:
    //
    public static Int64 GetLastCardSequence(Int64 SiteId, SqlTransaction SqlTrx)
    {
      Int64 _result;
      StringBuilder _sb;
      SqlCommand _sql_cmd;
      SqlDataReader _reader;

      _result = 0;
      _reader = null;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT   CG_LAST_SEQUENCE");
        _sb.AppendLine("   FROM   CARD_GENERATION");
        _sb.AppendLine("  WHERE   CG_SITE_ID = " + SiteId);

        _sql_cmd = new SqlCommand(_sb.ToString());
        _sql_cmd.Connection = SqlTrx.Connection;
        _sql_cmd.Transaction = SqlTrx;

        _reader = _sql_cmd.ExecuteReader();

        if ((_reader != null) && _reader.Read())
        {
          _result = Convert.ToInt64(_reader[0]);
        }
      }
      finally
      {
        if (_reader != null)
        {
          _reader.Close();
        }
      }

      return _result;
    } // GetLastCardSequence

    //------------------------------------------------------------------------------
    // PURPOSE : Update last sequence used in CARD_GENERATION
    //
    //  PARAMS :
    //      - INPUT:
    //        - Sequence: Last Used ID
    //        - SqlTransaction: common transaction for operation
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True if all went OK; False otherwise
    //
    // NOTES:
    //
    public static Boolean UpdateLastCardSequence(Int64 SiteId, Int64 Sequence, SqlTransaction SqlTrx)
    {
      Boolean _result;
      StringBuilder _sb;
      SqlCommand _sql_cmd;

      _result = false;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("UPDATE   CARD_GENERATION ");
        _sb.AppendLine("   SET   CG_LAST_SEQUENCE = " + Sequence);
        _sb.AppendLine(" WHERE   CG_SITE_ID = " + SiteId);

        _sql_cmd = new SqlCommand(_sb.ToString());
        _sql_cmd.Connection = SqlTrx.Connection;
        _sql_cmd.Transaction = SqlTrx;

        _result = _sql_cmd.ExecuteNonQuery() == 1;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        _result = false;
      }

      return _result;
    } // UpdateLastCardSequence

    


  }
}
