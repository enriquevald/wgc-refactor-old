namespace WSI.Cashier.TITO
{
  partial class uc_ticket_reader
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.tmr_clear = new System.Windows.Forms.Timer(this.components);
      this.pictureBox1 = new System.Windows.Forms.PictureBox();
      this.lbl_swipe_ticket = new WSI.Cashier.Controls.uc_label();
      this.lbl_invalid_ticket = new WSI.Cashier.Controls.uc_label();
      this.txt_validation_number = new WSI.Cashier.Controls.uc_round_textbox();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
      this.SuspendLayout();
      // 
      // tmr_clear
      // 
      this.tmr_clear.Interval = 250;
      this.tmr_clear.Tick += new System.EventHandler(this.tmr_clear_Tick);
      // 
      // pictureBox1
      // 
      this.pictureBox1.Image = global::WSI.Cashier.Properties.Resources.ticketsTito_header;
      this.pictureBox1.Location = new System.Drawing.Point(5, 5);
      this.pictureBox1.Name = "pictureBox1";
      this.pictureBox1.Size = new System.Drawing.Size(50, 50);
      this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
      this.pictureBox1.TabIndex = 1;
      this.pictureBox1.TabStop = false;
      this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
      // 
      // lbl_swipe_ticket
      // 
      this.lbl_swipe_ticket.BackColor = System.Drawing.Color.Transparent;
      this.lbl_swipe_ticket.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_swipe_ticket.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_swipe_ticket.Location = new System.Drawing.Point(61, 20);
      this.lbl_swipe_ticket.Name = "lbl_swipe_ticket";
      this.lbl_swipe_ticket.Size = new System.Drawing.Size(169, 20);
      this.lbl_swipe_ticket.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_swipe_ticket.TabIndex = 0;
      this.lbl_swipe_ticket.Text = "xValidation Number";
      // 
      // lbl_invalid_ticket
      // 
      this.lbl_invalid_ticket.BackColor = System.Drawing.Color.Transparent;
      this.lbl_invalid_ticket.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_invalid_ticket.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.lbl_invalid_ticket.Location = new System.Drawing.Point(473, 0);
      this.lbl_invalid_ticket.Name = "lbl_invalid_ticket";
      this.lbl_invalid_ticket.Size = new System.Drawing.Size(222, 60);
      this.lbl_invalid_ticket.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_RED;
      this.lbl_invalid_ticket.TabIndex = 2;
      this.lbl_invalid_ticket.Text = "N�mero de ticket no v�lido ...";
      this.lbl_invalid_ticket.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // txt_validation_number
      // 
      this.txt_validation_number.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_validation_number.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_validation_number.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_validation_number.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_validation_number.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_validation_number.CornerRadius = 5;
      this.txt_validation_number.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_validation_number.Location = new System.Drawing.Point(227, 10);
      this.txt_validation_number.MaxLength = 32767;
      this.txt_validation_number.Multiline = false;
      this.txt_validation_number.Name = "txt_validation_number";
      this.txt_validation_number.PasswordChar = '\0';
      this.txt_validation_number.ReadOnly = false;
      this.txt_validation_number.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_validation_number.SelectedText = "";
      this.txt_validation_number.SelectionLength = 0;
      this.txt_validation_number.SelectionStart = 0;
      this.txt_validation_number.Size = new System.Drawing.Size(226, 40);
      this.txt_validation_number.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_validation_number.TabIndex = 1;
      this.txt_validation_number.TabStop = false;
      this.txt_validation_number.Text = "12345678901234567890";
      this.txt_validation_number.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.txt_validation_number.UseSystemPasswordChar = false;
      this.txt_validation_number.WaterMark = null;
      this.txt_validation_number.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      this.txt_validation_number.TextChanged += new System.EventHandler(this.txt_track_number_TextChanged);
      this.txt_validation_number.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_track_number_KeyPress);
      // 
      // uc_ticket_reader
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.lbl_invalid_ticket);
      this.Controls.Add(this.pictureBox1);
      this.Controls.Add(this.txt_validation_number);
      this.Controls.Add(this.lbl_swipe_ticket);
      this.MaximumSize = new System.Drawing.Size(800, 100);
      this.MinimumSize = new System.Drawing.Size(750, 60);
      this.Name = "uc_ticket_reader";
      this.Size = new System.Drawing.Size(750, 60);
      this.VisibleChanged += new System.EventHandler(this.uc_ticket_reader_VisibleChanged);
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private WSI.Cashier.Controls.uc_round_textbox txt_validation_number;
    private WSI.Cashier.Controls.uc_label lbl_invalid_ticket;
    private WSI.Cashier.Controls.uc_label lbl_swipe_ticket;
    private System.Windows.Forms.PictureBox pictureBox1;
    private System.Windows.Forms.Timer tmr_clear;
  }
}
