//------------------------------------------------------------------------------
// Copyright © 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_ticket_print_progress.cs
// 
//   DESCRIPTION: Implements the following classes:
//                1. frm_ticket_print_progress
//
//        AUTHOR: David Hernández
// 
// CREATION DATE: 21-JAN-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 21-JAN-2014 DHA     First release.
// 26-FEB-2014 XIT     Deleted SmartParams uses
// 10-NOV 2015 FAV     Product Backlog Item 6019: New design
// 21-DIC-2016 XGJ     Bug: 21863: No se pueden vincular promociones a la cuenta
// 09-JAN-2017 FAV     Bug: 21863: No se pueden vincular promociones a la cuenta
//-------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using WSI.Common;
using WSI.Cashier.TITO;
using WSI.Common.TITO;
using System.Data.SqlClient;
using WSI.Cashier.Controls;

namespace WSI.Cashier.TITO
{
  public partial class frm_ticket_print_progress : frm_base
  {
    #region Attributes

    private TicketCreationParams m_params;
    private String m_promo_name;
    private frm_container m_parent;
    private static frm_yesno form_yes_no;
    private ParamsTicketOperation m_operation_params;
    private ENUM_TITO_OPERATIONS_RESULT m_operation_result;
    private Int32 m_tickets_generated;
    private Int32 m_quantity_of_promos;
    private delegate void SetValueProgresBarCallBack(Int32 Value);
    private delegate void SetValueLabelCallBack(String Value);

    #endregion Attributes

    #region Constructor

    public frm_ticket_print_progress()
    {
      InitializeComponent();

      form_yes_no = new frm_yesno();
      form_yes_no.Opacity = 0.6;
    }

    #endregion Constructor

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Init and Show the form.
    //
    //  PARAMS :
    //      - INPUT :
    //          - ParentForm
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public void Show(ParamsTicketOperation OperationParams, TicketCreationParams Params, String PromoName, frm_container Parent)
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_ticket_print_progress", Log.Type.Message);

      try
      {
        m_parent = Parent;
        m_operation_params = OperationParams;
        m_params = Params;
        m_promo_name = PromoName;
        m_quantity_of_promos =Params.QuantityOfPromotions;
        InitControls();

        bw_print_tickets.RunWorkerAsync();

        form_yes_no.Show(Parent);
        this.ShowDialog(form_yes_no);
        form_yes_no.Hide();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } // Show

    //------------------------------------------------------------------------------
    // PURPOSE : Assing points to account.
    //
    //  PARAMS :
    //      - INPUT :
    //          - ParentForm
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public void AssingPointsToAccount(ParamsTicketOperation OperationParams, TicketCreationParams Params, String PromoName, frm_container Parent)
    {
      try
      {
        m_parent = Parent;
        m_operation_params = OperationParams;
        m_params = Params;
        m_promo_name = PromoName;
        m_quantity_of_promos = Params.QuantityOfPromotions;
        InitControls();

        bw_print_tickets.RunWorkerAsync();

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } // AssingPointsToAccount

    //------------------------------------------------------------------------------
    // PURPOSE : Dispose yes_no form.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    new public void Dispose()
    {
      form_yes_no.Dispose();
      form_yes_no = null;

      base.Dispose();
    } // Dispose

    #endregion Public Methods

    #region Private Methods

    private void InitControls()
    {
      btn_cancel_print.Text = Resource.String("STR_FRM_TITO_PRINT_TICKET_STOP");
      this.FormTitle = Resource.String("STR_FRM_TITO_PRINT_TICKET");
      lbl_status_bar_msg.Text = String.Empty;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Initialize the printing process
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    private void bw_print_tickets_DoWork(object sender, DoWorkEventArgs e)
    {
        BackgroundWorker worker = sender as BackgroundWorker;

        m_tickets_generated = 0;

        e.Result = BusinessLogic.CreatePromotionsTicketThread(ref m_operation_params, m_params, TitoPrinter.TITO_PRINTER_PRINT_MODE.REAL_TIME, bw_print_tickets, e);
      }

    //------------------------------------------------------------------------------
    // PURPOSE: Update the status of the printing process
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    private void bw_print_tickets_ProgressChanged(object sender, ProgressChangedEventArgs e)
    {
        m_tickets_generated++;

        SetValueProgresBar(e.ProgressPercentage);

        SetValueLabel(Resource.String("STR_FRM_TITO_PROMO_TICKETS_PRINTED_STATUS", new object[] { m_tickets_generated, m_quantity_of_promos }));
      }

    private void SetValueProgresBar(Int32 Value)
    {
      if (this.pb_print_tickets.InvokeRequired)
      {
        SetValueProgresBarCallBack d = new SetValueProgresBarCallBack(SetValueProgresBar);
        this.Invoke(d, new object[] { Value });
      }
      else
      {
        pb_print_tickets.Value = Value;
      }
    }

    private void SetValueLabel(String Value)
    {
      if (this.pb_print_tickets.InvokeRequired)
      {
        SetValueLabelCallBack d = new SetValueLabelCallBack(SetValueLabel);
        this.Invoke(d, new object[] { Value });
      }
      else
      {
        lbl_status_bar_msg.Text = Value;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Printing process is finalized or canceled
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    private void bw_print_tickets_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
    {
      try
      {
        PrinterStatus _status;
        String _status_text;

        if (e.Error != null)
        {
          Log.Error("Error: PrintTicketThread: " + e.Error);
          m_operation_result = ENUM_TITO_OPERATIONS_RESULT.PRINTER_PRINT_FAIL;
        }
        if (e.Cancelled)
        {
          // Canceled by user
          m_operation_result = ENUM_TITO_OPERATIONS_RESULT.PRINTER_PRINT_STOPED;
        }
        else if (e.Result != null)
        {
          m_operation_result = (ENUM_TITO_OPERATIONS_RESULT)e.Result;
        }

        String _msg_text;
        MessageBoxIcon _msg_type;
        String _msg_caption;

        _msg_text = String.Empty;
        _msg_type = MessageBoxIcon.Information;
        _msg_caption = String.Empty;

        switch (m_operation_result)
        {
          case ENUM_TITO_OPERATIONS_RESULT.PRINTER_PRINT_FAIL:
          case ENUM_TITO_OPERATIONS_RESULT.PRINTER_IS_NOT_READY:
            _msg_type = MessageBoxIcon.Error;
            _msg_caption = Resource.String("STR_APP_GEN_MSG_ERROR");
            _msg_text += Resource.String("STR_FRM_TITO_PROMO_ERROR_PRINTER_FAILS");
            TitoPrinter.GetStatus(out _status, out _status_text);
            _msg_text += "\r\n    - " + TitoPrinter.GetMsgError(_status);
            Log.Error(String.Format("bw_print_tickets_RunWorkerCompleted. Result:[{0}]", m_operation_result.ToString()));
            break;
          case ENUM_TITO_OPERATIONS_RESULT.OK:
            _msg_type = MessageBoxIcon.Information;
            _msg_caption = m_params.PromoName;
            _msg_text = Resource.String("STR_FRM_TITO_PROMO_004", m_params.PromoRewardValue, m_params.PromoName);
            break;

          case ENUM_TITO_OPERATIONS_RESULT.PRINTER_PRINT_STOPED:
            _msg_type = MessageBoxIcon.Warning;
            _msg_caption = Resource.String("STR_APP_GEN_MSG_WARNING");
            _msg_text += Resource.String("STR_FRM_TITO_PROMO_CANCELED_BY_USER");
            break;

          case ENUM_TITO_OPERATIONS_RESULT.AUX_OPERATION_CANCELED:
            _msg_type = MessageBoxIcon.Error;
            _msg_caption = Resource.String("STR_APP_GEN_MSG_ERROR");
            _msg_text += Resource.String("STR_FRM_TITO_PROMO_ERROR_OPERATION");
            break;
          default:
            _msg_type = MessageBoxIcon.Error;
            _msg_caption = Resource.String("STR_APP_GEN_MSG_ERROR");
            _msg_text += Resource.String("STR_FRM_TITO_PROMO_ERROR_OPERATION");
            break;
        }

        frm_message.Show(_msg_text, _msg_caption, MessageBoxButtons.OK, _msg_type, m_parent);

        //if (m_operation_result != ENUM_TITO_OPERATIONS_RESULT.PRINTER_PRINT_STOPED)
        {
          CancelTicketNotPrinted();
        }

        this.DialogResult = DialogResult.OK;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Cancel the printing process
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    private void btn_cancel_print_Click(object sender, EventArgs e)
    {
      btn_cancel_print.Enabled = false;
      bw_print_tickets.CancelAsync();
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Open form to discard the tickets
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:

    private void CancelTicketNotPrinted()
    {
      List<Int64> _ticket_list;
      if (Ticket.DB_GetTicketsFromOperation((Int64)m_operation_params.out_operation_id, out _ticket_list))
      {
        if (_ticket_list.Count > 0)
        {
          frm_ticket_discard _ticket_discard = new frm_ticket_discard();
          _ticket_discard.Show((Int64)m_operation_params.out_operation_id, true, m_parent);

          _ticket_discard.Dispose();

          if (form_yes_no != null)
          {
            form_yes_no.Hide();
          }
        }
      }
    }

    #endregion Private Methods
  }
}