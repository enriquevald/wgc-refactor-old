namespace WSI.Cashier
{
  partial class uc_tito_card
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose (bool disposing)
    {
      if ( disposing && ( components != null ) )
      {
        components.Dispose ();
      }
      base.Dispose (disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent ()
    {
      this.components = new System.ComponentModel.Container();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(uc_tito_card));
      this.timer1 = new System.Windows.Forms.Timer(this.components);
      this.lbl_separator_1 = new System.Windows.Forms.Label();
      this.pb_uc_icon = new System.Windows.Forms.PictureBox();
      this.btn_multiple_buckets = new WSI.Cashier.Controls.uc_round_button();
      this.btn_player_browse = new WSI.Cashier.Controls.uc_round_button();
      this.btn_win_loss_statement_request = new WSI.Cashier.Controls.uc_round_button();
      this.btn_vouchers_space = new WSI.Cashier.Controls.uc_round_button();
      this.btn_draw = new WSI.Cashier.Controls.uc_round_button();
      this.gbAccount = new WSI.Cashier.Controls.uc_round_panel();
      this.pb_blocked = new System.Windows.Forms.PictureBox();
      this.lbl_vip_account = new WSI.Cashier.Controls.uc_label();
      this.pb_user = new System.Windows.Forms.PictureBox();
      this.lbl_account_id_value = new WSI.Cashier.Controls.uc_label();
      this.lbl_birth = new WSI.Cashier.Controls.uc_label();
      this.lbl_holder_name = new WSI.Cashier.Controls.uc_label();
      this.lbl_block_reason = new WSI.Cashier.Controls.uc_label();
      this.lbl_account_id_name = new WSI.Cashier.Controls.uc_label();
      this.lbl_birth_title = new WSI.Cashier.Controls.uc_label();
      this.lbl_blocked = new WSI.Cashier.Controls.uc_label();
      this.lbl_track_number = new WSI.Cashier.Controls.uc_label();
      this.lbl_id_title = new WSI.Cashier.Controls.uc_label();
      this.lbl_trackdata = new WSI.Cashier.Controls.uc_label();
      this.lbl_id = new WSI.Cashier.Controls.uc_label();
      this.lbl_happy_birthday = new WSI.Cashier.Controls.uc_label();
      this.lbl_name = new WSI.Cashier.Controls.uc_label();
      this.gb_card_operations = new WSI.Cashier.Controls.uc_round_panel();
      this.btn_recycle_card = new WSI.Cashier.Controls.uc_round_button();
      this.btn_print_card = new WSI.Cashier.Controls.uc_round_button();
      this.btn_new_card = new WSI.Cashier.Controls.uc_round_button();
      this.gb_gifts = new WSI.Cashier.Controls.uc_round_panel();
      this.lbl_points_balance = new WSI.Cashier.Controls.uc_label();
      this.lbl_level = new WSI.Cashier.Controls.uc_label();
      this.lbl_level_title = new WSI.Cashier.Controls.uc_label();
      this.btn_gifts = new WSI.Cashier.Controls.uc_round_button();
      this.lbl_points_balance_suffix = new WSI.Cashier.Controls.uc_label();
      this.lbl_points_balance_title = new WSI.Cashier.Controls.uc_label();
      this.btn_lasts_movements = new WSI.Cashier.Controls.uc_round_button();
      this.btn_card_block = new WSI.Cashier.Controls.uc_round_button();
      this.btn_account_edit = new WSI.Cashier.Controls.uc_round_button();
      this.lbl_cards_title = new WSI.Cashier.Controls.uc_label();
      this.uc_card_reader1 = new WSI.Cashier.uc_card_reader();
      this.btn_pay_ticket = new WSI.Cashier.Controls.uc_round_button();
      ((System.ComponentModel.ISupportInitialize)(this.pb_uc_icon)).BeginInit();
      this.gbAccount.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pb_blocked)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.pb_user)).BeginInit();
      this.gb_card_operations.SuspendLayout();
      this.gb_gifts.SuspendLayout();
      this.SuspendLayout();
      // 
      // timer1
      // 
      this.timer1.Interval = 500;
      this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
      // 
      // lbl_separator_1
      // 
      this.lbl_separator_1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_separator_1.BackColor = System.Drawing.Color.Black;
      this.lbl_separator_1.Location = new System.Drawing.Point(-3, 60);
      this.lbl_separator_1.Name = "lbl_separator_1";
      this.lbl_separator_1.Size = new System.Drawing.Size(885, 1);
      this.lbl_separator_1.TabIndex = 2;
      // 
      // pb_uc_icon
      // 
      this.pb_uc_icon.Image = global::WSI.Cashier.Properties.Resources.accounts_header;
      this.pb_uc_icon.Location = new System.Drawing.Point(10, 7);
      this.pb_uc_icon.Name = "pb_uc_icon";
      this.pb_uc_icon.Size = new System.Drawing.Size(50, 50);
      this.pb_uc_icon.TabIndex = 124;
      this.pb_uc_icon.TabStop = false;
      // 
      // btn_multiple_buckets
      // 
      this.btn_multiple_buckets.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_multiple_buckets.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_multiple_buckets.FlatAppearance.BorderSize = 0;
      this.btn_multiple_buckets.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_multiple_buckets.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_multiple_buckets.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_multiple_buckets.Image = null;
      this.btn_multiple_buckets.IsSelected = false;
      this.btn_multiple_buckets.Location = new System.Drawing.Point(670, 462);
      this.btn_multiple_buckets.Name = "btn_multiple_buckets";
      this.btn_multiple_buckets.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_multiple_buckets.Size = new System.Drawing.Size(132, 50);
      this.btn_multiple_buckets.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_multiple_buckets.TabIndex = 125;
      this.btn_multiple_buckets.Text = "XMULTIPLEBUCKETS";
      this.btn_multiple_buckets.UseVisualStyleBackColor = false;
      this.btn_multiple_buckets.Click += new System.EventHandler(this.btn_multiple_buckets_Click);
      // 
      // btn_player_browse
      // 
      this.btn_player_browse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_player_browse.BackColor = System.Drawing.Color.Transparent;
      this.btn_player_browse.CornerRadius = 0;
      this.btn_player_browse.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(114)))), ((int)(((byte)(121)))));
      this.btn_player_browse.FlatAppearance.BorderSize = 0;
      this.btn_player_browse.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_player_browse.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_player_browse.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_player_browse.Image = global::WSI.Cashier.Properties.Resources.btnSearch;
      this.btn_player_browse.IsSelected = false;
      this.btn_player_browse.Location = new System.Drawing.Point(815, 3);
      this.btn_player_browse.Margin = new System.Windows.Forms.Padding(0);
      this.btn_player_browse.Name = "btn_player_browse";
      this.btn_player_browse.SelectedColor = System.Drawing.Color.Empty;
      this.btn_player_browse.Size = new System.Drawing.Size(55, 55);
      this.btn_player_browse.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.NONE;
      this.btn_player_browse.TabIndex = 1;
      this.btn_player_browse.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.btn_player_browse.UseVisualStyleBackColor = false;
      // 
      // btn_win_loss_statement_request
      // 
      this.btn_win_loss_statement_request.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_win_loss_statement_request.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_win_loss_statement_request.FlatAppearance.BorderSize = 0;
      this.btn_win_loss_statement_request.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_win_loss_statement_request.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_win_loss_statement_request.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_win_loss_statement_request.Image = null;
      this.btn_win_loss_statement_request.IsSelected = false;
      this.btn_win_loss_statement_request.Location = new System.Drawing.Point(670, 334);
      this.btn_win_loss_statement_request.Name = "btn_win_loss_statement_request";
      this.btn_win_loss_statement_request.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_win_loss_statement_request.Size = new System.Drawing.Size(132, 50);
      this.btn_win_loss_statement_request.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_win_loss_statement_request.TabIndex = 10;
      this.btn_win_loss_statement_request.Text = "XWINLOSSSTATEMENT";
      this.btn_win_loss_statement_request.UseVisualStyleBackColor = false;
      // 
      // btn_vouchers_space
      // 
      this.btn_vouchers_space.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_vouchers_space.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_vouchers_space.FlatAppearance.BorderSize = 0;
      this.btn_vouchers_space.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_vouchers_space.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_vouchers_space.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_vouchers_space.Image = null;
      this.btn_vouchers_space.IsSelected = false;
      this.btn_vouchers_space.Location = new System.Drawing.Point(670, 398);
      this.btn_vouchers_space.Name = "btn_vouchers_space";
      this.btn_vouchers_space.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_vouchers_space.Size = new System.Drawing.Size(132, 50);
      this.btn_vouchers_space.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_vouchers_space.TabIndex = 11;
      this.btn_vouchers_space.Text = "XVOUCHERSPACE";
      this.btn_vouchers_space.UseVisualStyleBackColor = false;
      this.btn_vouchers_space.Visible = false;
      // 
      // btn_draw
      // 
      this.btn_draw.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_draw.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_draw.FlatAppearance.BorderSize = 0;
      this.btn_draw.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_draw.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_draw.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_draw.Image = null;
      this.btn_draw.IsSelected = false;
      this.btn_draw.Location = new System.Drawing.Point(670, 206);
      this.btn_draw.Name = "btn_draw";
      this.btn_draw.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_draw.Size = new System.Drawing.Size(132, 50);
      this.btn_draw.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_draw.TabIndex = 8;
      this.btn_draw.Text = "XDRAW";
      this.btn_draw.UseVisualStyleBackColor = false;
      // 
      // gbAccount
      // 
      this.gbAccount.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.gbAccount.BackColor = System.Drawing.Color.Transparent;
      this.gbAccount.BorderColor = System.Drawing.Color.Empty;
      this.gbAccount.Controls.Add(this.pb_blocked);
      this.gbAccount.Controls.Add(this.lbl_vip_account);
      this.gbAccount.Controls.Add(this.pb_user);
      this.gbAccount.Controls.Add(this.lbl_account_id_value);
      this.gbAccount.Controls.Add(this.lbl_birth);
      this.gbAccount.Controls.Add(this.lbl_holder_name);
      this.gbAccount.Controls.Add(this.lbl_block_reason);
      this.gbAccount.Controls.Add(this.lbl_account_id_name);
      this.gbAccount.Controls.Add(this.lbl_birth_title);
      this.gbAccount.Controls.Add(this.lbl_blocked);
      this.gbAccount.Controls.Add(this.lbl_track_number);
      this.gbAccount.Controls.Add(this.lbl_id_title);
      this.gbAccount.Controls.Add(this.lbl_trackdata);
      this.gbAccount.Controls.Add(this.lbl_id);
      this.gbAccount.Controls.Add(this.lbl_happy_birthday);
      this.gbAccount.Controls.Add(this.lbl_name);
      this.gbAccount.CornerRadius = 10;
      this.gbAccount.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gbAccount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gbAccount.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gbAccount.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gbAccount.HeaderHeight = 35;
      this.gbAccount.HeaderSubText = null;
      this.gbAccount.HeaderText = null;
      this.gbAccount.Location = new System.Drawing.Point(50, 78);
      this.gbAccount.Name = "gbAccount";
      this.gbAccount.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gbAccount.Size = new System.Drawing.Size(600, 310);
      this.gbAccount.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.gbAccount.TabIndex = 3;
      // 
      // pb_blocked
      // 
      this.pb_blocked.Enabled = false;
      this.pb_blocked.Location = new System.Drawing.Point(384, 47);
      this.pb_blocked.Name = "pb_blocked";
      this.pb_blocked.Size = new System.Drawing.Size(48, 46);
      this.pb_blocked.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
      this.pb_blocked.TabIndex = 75;
      this.pb_blocked.TabStop = false;
      // 
      // lbl_vip_account
      // 
      this.lbl_vip_account.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_vip_account.BackColor = System.Drawing.Color.Transparent;
      this.lbl_vip_account.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_vip_account.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.lbl_vip_account.Location = new System.Drawing.Point(77, 80);
      this.lbl_vip_account.Name = "lbl_vip_account";
      this.lbl_vip_account.Size = new System.Drawing.Size(205, 23);
      this.lbl_vip_account.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_RED;
      this.lbl_vip_account.TabIndex = 2;
      this.lbl_vip_account.Text = "xVip Account";
      this.lbl_vip_account.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // pb_user
      // 
      this.pb_user.Image = ((System.Drawing.Image)(resources.GetObject("pb_user.Image")));
      this.pb_user.InitialImage = ((System.Drawing.Image)(resources.GetObject("pb_user.InitialImage")));
      this.pb_user.Location = new System.Drawing.Point(17, 47);
      this.pb_user.Name = "pb_user";
      this.pb_user.Size = new System.Drawing.Size(48, 46);
      this.pb_user.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
      this.pb_user.TabIndex = 72;
      this.pb_user.TabStop = false;
      this.pb_user.Click += new System.EventHandler(this.pb_user_Click);
      // 
      // lbl_account_id_value
      // 
      this.lbl_account_id_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_account_id_value.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_account_id_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_account_id_value.Location = new System.Drawing.Point(161, 59);
      this.lbl_account_id_value.Name = "lbl_account_id_value";
      this.lbl_account_id_value.Size = new System.Drawing.Size(95, 21);
      this.lbl_account_id_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_account_id_value.TabIndex = 1;
      this.lbl_account_id_value.Text = "012345";
      this.lbl_account_id_value.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_birth
      // 
      this.lbl_birth.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_birth.BackColor = System.Drawing.Color.Transparent;
      this.lbl_birth.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_birth.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_birth.Location = new System.Drawing.Point(154, 245);
      this.lbl_birth.Name = "lbl_birth";
      this.lbl_birth.Size = new System.Drawing.Size(128, 23);
      this.lbl_birth.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_birth.TabIndex = 10;
      this.lbl_birth.Text = "99 / 99 / 9999";
      this.lbl_birth.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lbl_holder_name
      // 
      this.lbl_holder_name.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_holder_name.BackColor = System.Drawing.Color.Transparent;
      this.lbl_holder_name.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_holder_name.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_holder_name.Location = new System.Drawing.Point(18, 172);
      this.lbl_holder_name.Name = "lbl_holder_name";
      this.lbl_holder_name.Size = new System.Drawing.Size(561, 23);
      this.lbl_holder_name.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_holder_name.TabIndex = 6;
      this.lbl_holder_name.Text = "TTTTTTTTTTTTT";
      this.lbl_holder_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.lbl_holder_name.UseMnemonic = false;
      // 
      // lbl_block_reason
      // 
      this.lbl_block_reason.BackColor = System.Drawing.Color.Transparent;
      this.lbl_block_reason.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_block_reason.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_block_reason.Location = new System.Drawing.Point(437, 71);
      this.lbl_block_reason.Name = "lbl_block_reason";
      this.lbl_block_reason.Size = new System.Drawing.Size(160, 21);
      this.lbl_block_reason.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_block_reason.TabIndex = 13;
      this.lbl_block_reason.Text = "xDesde Cajero";
      this.lbl_block_reason.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_account_id_name
      // 
      this.lbl_account_id_name.BackColor = System.Drawing.Color.Transparent;
      this.lbl_account_id_name.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_account_id_name.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_account_id_name.Location = new System.Drawing.Point(66, 59);
      this.lbl_account_id_name.Name = "lbl_account_id_name";
      this.lbl_account_id_name.Size = new System.Drawing.Size(87, 21);
      this.lbl_account_id_name.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_account_id_name.TabIndex = 0;
      this.lbl_account_id_name.Text = "xAccount";
      this.lbl_account_id_name.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_birth_title
      // 
      this.lbl_birth_title.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_birth_title.BackColor = System.Drawing.Color.Transparent;
      this.lbl_birth_title.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_birth_title.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_birth_title.Location = new System.Drawing.Point(18, 245);
      this.lbl_birth_title.Name = "lbl_birth_title";
      this.lbl_birth_title.Size = new System.Drawing.Size(109, 23);
      this.lbl_birth_title.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_birth_title.TabIndex = 9;
      this.lbl_birth_title.Text = "Nacimiento:";
      this.lbl_birth_title.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_blocked
      // 
      this.lbl_blocked.BackColor = System.Drawing.Color.Transparent;
      this.lbl_blocked.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_blocked.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_blocked.Location = new System.Drawing.Point(437, 51);
      this.lbl_blocked.Name = "lbl_blocked";
      this.lbl_blocked.Size = new System.Drawing.Size(160, 21);
      this.lbl_blocked.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_blocked.TabIndex = 12;
      this.lbl_blocked.Text = "xBloqueado";
      this.lbl_blocked.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_track_number
      // 
      this.lbl_track_number.BackColor = System.Drawing.Color.Transparent;
      this.lbl_track_number.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_track_number.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_track_number.Location = new System.Drawing.Point(87, 110);
      this.lbl_track_number.Name = "lbl_track_number";
      this.lbl_track_number.Size = new System.Drawing.Size(239, 20);
      this.lbl_track_number.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_track_number.TabIndex = 4;
      this.lbl_track_number.Text = "88888888888888888812";
      this.lbl_track_number.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_id_title
      // 
      this.lbl_id_title.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_id_title.BackColor = System.Drawing.Color.Transparent;
      this.lbl_id_title.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_id_title.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_id_title.Location = new System.Drawing.Point(18, 195);
      this.lbl_id_title.Name = "lbl_id_title";
      this.lbl_id_title.Size = new System.Drawing.Size(376, 23);
      this.lbl_id_title.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_id_title.TabIndex = 7;
      this.lbl_id_title.Text = "xCarnet de padre:";
      this.lbl_id_title.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_trackdata
      // 
      this.lbl_trackdata.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_trackdata.BackColor = System.Drawing.Color.Transparent;
      this.lbl_trackdata.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_trackdata.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_trackdata.Location = new System.Drawing.Point(18, 109);
      this.lbl_trackdata.Name = "lbl_trackdata";
      this.lbl_trackdata.Size = new System.Drawing.Size(247, 23);
      this.lbl_trackdata.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_trackdata.TabIndex = 3;
      this.lbl_trackdata.Text = "Tarjeta:";
      this.lbl_trackdata.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_id
      // 
      this.lbl_id.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_id.BackColor = System.Drawing.Color.Transparent;
      this.lbl_id.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_id.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_id.Location = new System.Drawing.Point(18, 220);
      this.lbl_id.Name = "lbl_id";
      this.lbl_id.Size = new System.Drawing.Size(561, 23);
      this.lbl_id.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_id.TabIndex = 8;
      this.lbl_id.Text = "AAAAAAAAAAAAAAAAAAAB";
      this.lbl_id.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_happy_birthday
      // 
      this.lbl_happy_birthday.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_happy_birthday.BackColor = System.Drawing.Color.Transparent;
      this.lbl_happy_birthday.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_happy_birthday.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.lbl_happy_birthday.Location = new System.Drawing.Point(18, 270);
      this.lbl_happy_birthday.Name = "lbl_happy_birthday";
      this.lbl_happy_birthday.Size = new System.Drawing.Size(561, 23);
      this.lbl_happy_birthday.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_RED;
      this.lbl_happy_birthday.TabIndex = 11;
      this.lbl_happy_birthday.Text = "XHAPPY BIRTHDAY!";
      this.lbl_happy_birthday.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.lbl_happy_birthday.Visible = false;
      // 
      // lbl_name
      // 
      this.lbl_name.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_name.BackColor = System.Drawing.Color.Transparent;
      this.lbl_name.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_name.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_name.Location = new System.Drawing.Point(18, 149);
      this.lbl_name.Name = "lbl_name";
      this.lbl_name.Size = new System.Drawing.Size(291, 23);
      this.lbl_name.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_name.TabIndex = 5;
      this.lbl_name.Text = "xName:";
      this.lbl_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // gb_card_operations
      // 
      this.gb_card_operations.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.gb_card_operations.BackColor = System.Drawing.Color.Transparent;
      this.gb_card_operations.BorderColor = System.Drawing.Color.Empty;
      this.gb_card_operations.Controls.Add(this.btn_recycle_card);
      this.gb_card_operations.Controls.Add(this.btn_print_card);
      this.gb_card_operations.Controls.Add(this.btn_new_card);
      this.gb_card_operations.CornerRadius = 10;
      this.gb_card_operations.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_card_operations.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gb_card_operations.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_card_operations.HeaderHeight = 35;
      this.gb_card_operations.HeaderSubText = null;
      this.gb_card_operations.HeaderText = null;
      this.gb_card_operations.Location = new System.Drawing.Point(50, 398);
      this.gb_card_operations.Name = "gb_card_operations";
      this.gb_card_operations.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_card_operations.Size = new System.Drawing.Size(600, 115);
      this.gb_card_operations.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.gb_card_operations.TabIndex = 4;
      this.gb_card_operations.Text = "xCardOperations";
      // 
      // btn_recycle_card
      // 
      this.btn_recycle_card.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.btn_recycle_card.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_recycle_card.FlatAppearance.BorderSize = 0;
      this.btn_recycle_card.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_recycle_card.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_recycle_card.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_recycle_card.Image = null;
      this.btn_recycle_card.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
      this.btn_recycle_card.IsSelected = false;
      this.btn_recycle_card.Location = new System.Drawing.Point(408, 49);
      this.btn_recycle_card.Name = "btn_recycle_card";
      this.btn_recycle_card.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
      this.btn_recycle_card.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_recycle_card.Size = new System.Drawing.Size(132, 50);
      this.btn_recycle_card.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_recycle_card.TabIndex = 2;
      this.btn_recycle_card.Text = "XRECYCLE CARD";
      this.btn_recycle_card.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.btn_recycle_card.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
      this.btn_recycle_card.UseVisualStyleBackColor = false;
      // 
      // btn_print_card
      // 
      this.btn_print_card.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.btn_print_card.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_print_card.FlatAppearance.BorderSize = 0;
      this.btn_print_card.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_print_card.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_print_card.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_print_card.Image = null;
      this.btn_print_card.IsSelected = false;
      this.btn_print_card.Location = new System.Drawing.Point(60, 49);
      this.btn_print_card.Name = "btn_print_card";
      this.btn_print_card.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_print_card.Size = new System.Drawing.Size(132, 50);
      this.btn_print_card.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_print_card.TabIndex = 0;
      this.btn_print_card.Text = "XPRINT CARD";
      this.btn_print_card.UseVisualStyleBackColor = false;
      // 
      // btn_new_card
      // 
      this.btn_new_card.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.btn_new_card.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_new_card.FlatAppearance.BorderSize = 0;
      this.btn_new_card.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_new_card.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_new_card.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_new_card.Image = null;
      this.btn_new_card.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
      this.btn_new_card.IsSelected = false;
      this.btn_new_card.Location = new System.Drawing.Point(234, 49);
      this.btn_new_card.Name = "btn_new_card";
      this.btn_new_card.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
      this.btn_new_card.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_new_card.Size = new System.Drawing.Size(132, 50);
      this.btn_new_card.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_new_card.TabIndex = 1;
      this.btn_new_card.Text = "XNEW CARD";
      this.btn_new_card.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.btn_new_card.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
      this.btn_new_card.UseVisualStyleBackColor = false;
      // 
      // gb_gifts
      // 
      this.gb_gifts.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.gb_gifts.BackColor = System.Drawing.Color.Transparent;
      this.gb_gifts.BorderColor = System.Drawing.Color.Empty;
      this.gb_gifts.Controls.Add(this.lbl_points_balance);
      this.gb_gifts.Controls.Add(this.lbl_level);
      this.gb_gifts.Controls.Add(this.lbl_level_title);
      this.gb_gifts.Controls.Add(this.btn_gifts);
      this.gb_gifts.Controls.Add(this.lbl_points_balance_suffix);
      this.gb_gifts.Controls.Add(this.lbl_points_balance_title);
      this.gb_gifts.CornerRadius = 10;
      this.gb_gifts.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_gifts.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gb_gifts.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_gifts.HeaderHeight = 35;
      this.gb_gifts.HeaderSubText = null;
      this.gb_gifts.HeaderText = null;
      this.gb_gifts.Location = new System.Drawing.Point(50, 524);
      this.gb_gifts.Name = "gb_gifts";
      this.gb_gifts.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_gifts.Size = new System.Drawing.Size(600, 109);
      this.gb_gifts.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.gb_gifts.TabIndex = 5;
      this.gb_gifts.Text = "xLoyalty Program";
      // 
      // lbl_points_balance
      // 
      this.lbl_points_balance.BackColor = System.Drawing.Color.Transparent;
      this.lbl_points_balance.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_points_balance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_points_balance.Location = new System.Drawing.Point(147, 68);
      this.lbl_points_balance.Name = "lbl_points_balance";
      this.lbl_points_balance.Size = new System.Drawing.Size(103, 20);
      this.lbl_points_balance.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_points_balance.TabIndex = 2;
      this.lbl_points_balance.Text = "0";
      this.lbl_points_balance.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_level
      // 
      this.lbl_level.BackColor = System.Drawing.Color.Transparent;
      this.lbl_level.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_level.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_level.Location = new System.Drawing.Point(21, 68);
      this.lbl_level.Name = "lbl_level";
      this.lbl_level.Size = new System.Drawing.Size(120, 20);
      this.lbl_level.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_level.TabIndex = 1;
      this.lbl_level.Text = "xSilver";
      this.lbl_level.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_level_title
      // 
      this.lbl_level_title.BackColor = System.Drawing.Color.Transparent;
      this.lbl_level_title.Font = new System.Drawing.Font("Open Sans Semibold", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_level_title.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_level_title.Location = new System.Drawing.Point(21, 48);
      this.lbl_level_title.Name = "lbl_level_title";
      this.lbl_level_title.Size = new System.Drawing.Size(76, 20);
      this.lbl_level_title.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_10PX_BLACK;
      this.lbl_level_title.TabIndex = 0;
      this.lbl_level_title.Text = "xLevel:";
      this.lbl_level_title.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // btn_gifts
      // 
      this.btn_gifts.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_gifts.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_gifts.FlatAppearance.BorderSize = 0;
      this.btn_gifts.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_gifts.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_gifts.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_gifts.Image = null;
      this.btn_gifts.IsSelected = false;
      this.btn_gifts.Location = new System.Drawing.Point(436, 48);
      this.btn_gifts.Name = "btn_gifts";
      this.btn_gifts.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_gifts.Size = new System.Drawing.Size(132, 50);
      this.btn_gifts.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_gifts.TabIndex = 5;
      this.btn_gifts.Text = "XGIFTS";
      this.btn_gifts.UseVisualStyleBackColor = false;
      // 
      // lbl_points_balance_suffix
      // 
      this.lbl_points_balance_suffix.BackColor = System.Drawing.Color.Transparent;
      this.lbl_points_balance_suffix.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_points_balance_suffix.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_points_balance_suffix.Location = new System.Drawing.Point(249, 68);
      this.lbl_points_balance_suffix.Name = "lbl_points_balance_suffix";
      this.lbl_points_balance_suffix.Size = new System.Drawing.Size(69, 20);
      this.lbl_points_balance_suffix.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_points_balance_suffix.TabIndex = 4;
      this.lbl_points_balance_suffix.Text = "xpuntos";
      this.lbl_points_balance_suffix.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_points_balance_title
      // 
      this.lbl_points_balance_title.BackColor = System.Drawing.Color.Transparent;
      this.lbl_points_balance_title.Font = new System.Drawing.Font("Open Sans Semibold", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_points_balance_title.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_points_balance_title.Location = new System.Drawing.Point(206, 48);
      this.lbl_points_balance_title.Name = "lbl_points_balance_title";
      this.lbl_points_balance_title.Size = new System.Drawing.Size(76, 20);
      this.lbl_points_balance_title.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_10PX_BLACK;
      this.lbl_points_balance_title.TabIndex = 3;
      this.lbl_points_balance_title.Text = "xBalance:";
      this.lbl_points_balance_title.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // btn_lasts_movements
      // 
      this.btn_lasts_movements.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_lasts_movements.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_lasts_movements.FlatAppearance.BorderSize = 0;
      this.btn_lasts_movements.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_lasts_movements.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_lasts_movements.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_lasts_movements.Image = null;
      this.btn_lasts_movements.IsSelected = false;
      this.btn_lasts_movements.Location = new System.Drawing.Point(670, 270);
      this.btn_lasts_movements.Name = "btn_lasts_movements";
      this.btn_lasts_movements.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_lasts_movements.Size = new System.Drawing.Size(132, 50);
      this.btn_lasts_movements.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_lasts_movements.TabIndex = 9;
      this.btn_lasts_movements.Text = "XLASTS MOVEMENTS";
      this.btn_lasts_movements.UseVisualStyleBackColor = false;
      // 
      // btn_card_block
      // 
      this.btn_card_block.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_card_block.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_card_block.FlatAppearance.BorderSize = 0;
      this.btn_card_block.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_card_block.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_card_block.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_card_block.Image = null;
      this.btn_card_block.IsSelected = false;
      this.btn_card_block.Location = new System.Drawing.Point(670, 142);
      this.btn_card_block.Name = "btn_card_block";
      this.btn_card_block.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_card_block.Size = new System.Drawing.Size(132, 50);
      this.btn_card_block.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_card_block.TabIndex = 7;
      this.btn_card_block.Text = "XUNBLOCK";
      this.btn_card_block.UseVisualStyleBackColor = false;
      // 
      // btn_account_edit
      // 
      this.btn_account_edit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_account_edit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_account_edit.FlatAppearance.BorderSize = 0;
      this.btn_account_edit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_account_edit.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_account_edit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_account_edit.Image = null;
      this.btn_account_edit.IsSelected = false;
      this.btn_account_edit.Location = new System.Drawing.Point(670, 78);
      this.btn_account_edit.Name = "btn_account_edit";
      this.btn_account_edit.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_account_edit.Size = new System.Drawing.Size(132, 50);
      this.btn_account_edit.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_account_edit.TabIndex = 6;
      this.btn_account_edit.Text = "XEDIT";
      this.btn_account_edit.UseVisualStyleBackColor = false;
      // 
      // lbl_cards_title
      // 
      this.lbl_cards_title.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_cards_title.AutoSize = true;
      this.lbl_cards_title.BackColor = System.Drawing.Color.Transparent;
      this.lbl_cards_title.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_cards_title.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_cards_title.Location = new System.Drawing.Point(807, 347);
      this.lbl_cards_title.Name = "lbl_cards_title";
      this.lbl_cards_title.Size = new System.Drawing.Size(71, 22);
      this.lbl_cards_title.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_cards_title.TabIndex = 12;
      this.lbl_cards_title.Text = "xCARDS";
      this.lbl_cards_title.Visible = false;
      // 
      // uc_card_reader1
      // 
      this.uc_card_reader1.ErrorMessageBelow = false;
      this.uc_card_reader1.InvalidCardTextVisible = false;
      this.uc_card_reader1.Location = new System.Drawing.Point(78, 0);
      this.uc_card_reader1.MaximumSize = new System.Drawing.Size(800, 90);
      this.uc_card_reader1.MinimumSize = new System.Drawing.Size(800, 60);
      this.uc_card_reader1.Name = "uc_card_reader1";
      this.uc_card_reader1.Size = new System.Drawing.Size(800, 60);
      this.uc_card_reader1.TabIndex = 0;
      // 
      // btn_pay_ticket
      // 
      this.btn_pay_ticket.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_pay_ticket.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_pay_ticket.FlatAppearance.BorderSize = 0;
      this.btn_pay_ticket.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_pay_ticket.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_pay_ticket.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_pay_ticket.Image = null;
      this.btn_pay_ticket.IsSelected = false;
      this.btn_pay_ticket.Location = new System.Drawing.Point(670, 524);
      this.btn_pay_ticket.Name = "btn_pay_ticket";
      this.btn_pay_ticket.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_pay_ticket.Size = new System.Drawing.Size(132, 50);
      this.btn_pay_ticket.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_pay_ticket.TabIndex = 126;
      this.btn_pay_ticket.Text = "XPAYTICKET";
      this.btn_pay_ticket.UseVisualStyleBackColor = false;
      this.btn_pay_ticket.Click += new System.EventHandler(this.btn_pay_ticket_Click);
      // 
      // uc_tito_card
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.btn_pay_ticket);
      this.Controls.Add(this.btn_multiple_buckets);
      this.Controls.Add(this.pb_uc_icon);
      this.Controls.Add(this.btn_player_browse);
      this.Controls.Add(this.btn_win_loss_statement_request);
      this.Controls.Add(this.btn_vouchers_space);
      this.Controls.Add(this.btn_draw);
      this.Controls.Add(this.gbAccount);
      this.Controls.Add(this.gb_card_operations);
      this.Controls.Add(this.gb_gifts);
      this.Controls.Add(this.btn_lasts_movements);
      this.Controls.Add(this.btn_card_block);
      this.Controls.Add(this.btn_account_edit);
      this.Controls.Add(this.lbl_cards_title);
      this.Controls.Add(this.lbl_separator_1);
      this.Controls.Add(this.uc_card_reader1);
      this.Name = "uc_tito_card";
      this.Size = new System.Drawing.Size(884, 656);
      ((System.ComponentModel.ISupportInitialize)(this.pb_uc_icon)).EndInit();
      this.gbAccount.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.pb_blocked)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.pb_user)).EndInit();
      this.gb_card_operations.ResumeLayout(false);
      this.gb_gifts.ResumeLayout(false);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private WSI.Cashier.Controls.uc_round_button btn_lasts_movements;
    private WSI.Cashier.Controls.uc_round_button btn_new_card;
    private WSI.Cashier.Controls.uc_label lbl_track_number;
    private WSI.Cashier.Controls.uc_round_button btn_account_edit;
    private WSI.Cashier.Controls.uc_label lbl_holder_name;
    private WSI.Cashier.Controls.uc_round_button btn_card_block;
    private WSI.Cashier.Controls.uc_label lbl_account_id_value;
    private WSI.Cashier.Controls.uc_label lbl_account_id_name;
    private WSI.Cashier.Controls.uc_label lbl_cards_title;
    private WSI.Cashier.Controls.uc_round_button btn_print_card;
    private WSI.Cashier.Controls.uc_label lbl_id_title;
    private WSI.Cashier.Controls.uc_label lbl_id;
    private WSI.Cashier.Controls.uc_label lbl_birth_title;
    private WSI.Cashier.Controls.uc_label lbl_birth;
    private WSI.Cashier.Controls.uc_label lbl_happy_birthday;
    private WSI.Cashier.Controls.uc_label lbl_name;
    private WSI.Cashier.Controls.uc_label lbl_trackdata;
    private WSI.Cashier.Controls.uc_label lbl_blocked;
    private WSI.Cashier.Controls.uc_round_panel gb_gifts;
    private WSI.Cashier.Controls.uc_round_button btn_gifts;
    private WSI.Cashier.Controls.uc_label lbl_points_balance_suffix;
    private WSI.Cashier.Controls.uc_label lbl_points_balance_title;
    private WSI.Cashier.Controls.uc_label lbl_points_balance;
    private WSI.Cashier.Controls.uc_label lbl_level;
    private WSI.Cashier.Controls.uc_label lbl_level_title;
    private WSI.Cashier.Controls.uc_label lbl_block_reason;
    private WSI.Cashier.Controls.uc_round_button btn_recycle_card;
    private WSI.Cashier.Controls.uc_round_panel gb_card_operations;
    private WSI.Cashier.Controls.uc_round_panel gbAccount;
    private WSI.Cashier.Controls.uc_round_button btn_draw;
    private WSI.Cashier.Controls.uc_label lbl_vip_account;
    private WSI.Cashier.Controls.uc_round_button btn_vouchers_space;
    private WSI.Cashier.Controls.uc_round_button btn_win_loss_statement_request;
    private WSI.Cashier.Controls.uc_round_button btn_player_browse;

    private uc_card_reader uc_card_reader1;
    private System.Windows.Forms.Timer timer1;
    private System.Windows.Forms.PictureBox pb_user;
    private System.Windows.Forms.PictureBox pb_blocked;
    private System.Windows.Forms.Label lbl_separator_1;
    private System.Windows.Forms.PictureBox pb_uc_icon;
    private Controls.uc_round_button btn_multiple_buckets;
    private Controls.uc_round_button btn_pay_ticket;

  }
}
