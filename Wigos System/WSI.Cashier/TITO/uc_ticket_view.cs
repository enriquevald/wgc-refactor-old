//------------------------------------------------------------------------------
// Copyright � 2007-2013 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_ticket_view.cs
// 
//   DESCRIPTION: Implements the uc_ticket_view user control
//                Class: uc_ticket_view (Ticket Operations)
//
//        AUTHOR: Nelson Madrigal
//
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 05-AUG-2013 NMR    First release.
// 26-AUG-2013 NMR    Changing button 'but_search_account' position; its belongs to Card Reader control
// 27-AUG-2013 NMR    Grouping use of TITO parameters
// 28-AUG-2013 NMR    Controlling Allowed Ticket Type for Redemption
// 19-SEP-2013 NMR    Removed control about ticket pay amount and other
// 20-SEP-2013 NMR    Removed control for TrackData processment
// 10-OCT-2013 NMR    Added new TITO permissions with tickets
// 23-OCT-2013 LEM    Allow redeem more than one ticket at the same time    
//                    Show ticket list to be redeemed, allow clear that list
// 10-DEC-2013 ACM    Hide "CREATED_ACCOUNT" column from main grid.
// 12-DEC-2013 RMS    Check jackpot and handpay tickets has valid handpay before put in pay list
// 30-DEC-2013 JRM    Fixed bug WIGOSTITO-918 pertaining the wrong focus after a delete action
// 30-DEC-2013 JRM    Fixed bug WIGOSTITO-931 Manual pay without authorization was disabled. Needs to ask for login.�
// 31-OCT-2014 SGB    Fixed bug WIG-1606: Message modification from ticket status
// 22-JAN-2015 MPO    Fixed bug WIG-1947: Commented in function GetAccountOperation
// 01-SEP-2015 DHA    Added number of tickets to pay label
// 14-SEP-2015 DHA    Product Backlog Item 4435: last ticket inserted always visible
// 10-NOV 2015 FAV    Product Backlog Item 5803: New design
// 23-FEB-2016 RAB    Product Backlog Item 9887:TITO: Pagar tickets en estado PENDING_CANCEL (Cajero)
// 24-FEB-2016 RAB    Bug 9926:T�ckets TITO pendientes de cancelaci�n: no se pueden pagar una vez validados
// 29-FEB-2016 LTC    Product Backlog Item: 7440 Disputes and Handpays division - New Button functionality  
// 19-APR-2016 ETP    Fixed Bug 12159: PendingCancell.AllowToReedem allows to pays Non-Redeemeble Tickets.
// 07-MAR-2016 LTC    Product Backlog Item 10227:TITA: TITO ticket exchange for gambling chips
// 29-MAR-2016 RGR    Product Backlog Item 10981:Sprint Review 21 Winions - Mex
// 08-ABR-2016 LTC    Bug 11628:TITA - Chips Swap - List items are cleaned on Cancel operation
// 09-JUN-2016 ESE    Bug 12670:Ref. 17777 - Ticket TITO payment, grace period
// 01-JUL-2016 LTC    Bug 14071:TITA - TITO Tickets redeem for chips - Wrong message
// 06-OCT-2016 JRC    PBI 18259:TITO: Nuevo estado "Pending Print" en tickets - Mostrar nuevo estado en pantallas (GUI y Cashier)
// 20-OCT-2016 XGJ    PBI 18259:TITO: Nuevo estado "Pending Print" en tickets - Mostrar nuevo estado en pantallas (GUI y Cashier)
// 31-JAN-2017 ATB    Bug 21719:CountR is applying taxes when it never owns
// 17-MAR-2017 ETP    WIGOS-194: Credit line: Add logic to avoid withdraw with debt.
// 04-APR-2017 RAB    PBI 26537: MES10 Ticket validation - Prevent the use of dealer copy tickets (Cashier)
// 04-MAY-2017 RAB    PBI 27173: MES10 Ticket validation - Ticket not found messages
// 08-AUG-2017 ATB    Bug 29252: The voucher of a paid "Handpay" is displaying "An�nimo" in the field "Cliente" when the user already introduced their account.
// 07-SEP-2017 DHA    Bug 29624:WIGOS-4652 The button "Disputa" is enabled when the cash session is closed
// 06-OCT-2017 RAB    Bug 30161:WIGOS-5663 Cashier: Disputa handpay operation with custom user account has been changed by anonymous user
// 10-OCT-2017 JML    PBI 30023:WIGOS-4068 Payment threshold registration - Transaction information
// 30-OCT-2017 RAB    Bug 30446:WIGOS-6052 Threshold: Screen order is incorrect in HP and dispute (paying with PO) exceeding threshold and without permissions
// 17-NOV-2017 DPC    PBI 30845:[WIGOS-4375]: Associate TITO handpays to the account
//------------ ------ ----------------------------------------------------------

using System;
using System.Data;
using System.ComponentModel;
using System.Windows.Forms;
using WSI.Common;
using WSI.Common.TITO;
using System.Collections.Generic;
using WSI.Cashier.TITO;
using System.Drawing;
using System.Globalization;

namespace WSI.Cashier
{
  public partial class uc_ticket_view : UserControl
  {
    const Int32 DURATION_BUTTONS_ENABLED_AFTER_OPERATION = 120;


    Boolean m_first_time = true;
    frm_yesno m_frm_shadow_gui;
    frm_container m_parent;
    Currency m_tickets_total_amount;
    DataTable m_tickets_table;
    CardData m_card_data;
    Int32 m_tick_count_tracknumber_read;

    #region Constructor

    public uc_ticket_view()
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[UC LOAD] uc_ticket_view", Log.Type.Message);

      InitializeComponent();

      m_frm_shadow_gui = new frm_yesno();
      m_frm_shadow_gui.Opacity = 0.6;

      CreateTicketListTable();

      EventLastAction.AddEventLastAction(this.Controls);
    }

    public void InitFocus()
    {
      uc_ticket_reader.Focus();
    }

    private void CreateTicketListTable()
    {
      DataColumn _column;

      m_tickets_table = new DataTable();

      //Ticket Id
      _column = m_tickets_table.Columns.Add("TICKET_ID");
      _column.DataType = typeof(Int64);
      _column.Unique = true;

      //Ticket Number
      _column = m_tickets_table.Columns.Add("TICKET_NUMBER");
      _column.DataType = typeof(Int64);

      //Ticket Number (String)
      _column = m_tickets_table.Columns.Add("TICKET_NUMBER_STRING");
      _column.DataType = typeof(String);

      //Ticket Date
      _column = m_tickets_table.Columns.Add("TICKET_DATETIME");
      _column.DataType = typeof(DateTime);

      //Ticket Terminal
      _column = m_tickets_table.Columns.Add("TICKET_TERMINAL");
      _column.DataType = typeof(String);

      //Ticket created Amount with currency
      _column = m_tickets_table.Columns.Add("TICKET_AMT0");
      _column.DataType = typeof(String);

      //Ticket Amount
      _column = m_tickets_table.Columns.Add("TICKET_AMOUNT");
      _column.DataType = typeof(Currency);

      //Is Virtual Account
      _column = m_tickets_table.Columns.Add("CREATED_ACCOUNT");//0 if virtual account
      _column.DataType = typeof(Int64);

      //Ticket Validation Type
      _column = m_tickets_table.Columns.Add("TICKET_VALIDATION_TYPE");//0 if virtual account
      _column.DataType = typeof(TITO_VALIDATION_TYPE);

      //Ticket Type
      _column = m_tickets_table.Columns.Add("TICKET_TYPE");
      _column.DataType = typeof(TITO_TICKET_TYPE);

      //Ticket Number (String)
      _column = m_tickets_table.Columns.Add("TICKET_ACCOUNT");
      _column.DataType = typeof(String);

      //Ticket Terminal Type (Int32)
      _column = m_tickets_table.Columns.Add("TICKET_TERMINAL_TYPE");
      _column.DataType = typeof(TITO_TERMINAL_TYPE);

      m_tickets_table.PrimaryKey = new DataColumn[] { m_tickets_table.Columns["TICKET_ID"] };
    }

    public void RefreshCardData(Boolean Clear)
    {
      String _visible_track_number;

      if (Clear)
      {
        BusinessLogic.LoadVirtualAccount(m_card_data);

      }
      else
      {
        m_card_data = m_parent.uc_tito_card1.GetAccountWacherCard();
      };

      lbl_account_id_value.Text = m_card_data.AccountId.ToString();
      lbl_account_id_value.Visible = (m_card_data.AccountId != 0);

      if (m_card_data.IsRecycled)
      {
        _visible_track_number = CardData.RECYCLED_TRACK_DATA;
      }
      else
      {
        _visible_track_number = m_card_data.VisibleTrackdata();
        _visible_track_number = m_card_data.TrackData;
      }
      // lbl_section_1.Text = track_number;
      lbl_track_number.Text = _visible_track_number;

      if (String.IsNullOrEmpty(m_card_data.PlayerTracking.HolderName))
      {
        lbl_holder_name.Text = Resource.String("STR_UC_CARD_CARD_HOLDER_ANONYMOUS");
        lbl_account_id_value.Text = Resource.String("STR_UC_CARD_CARD_HOLDER_ANONYMOUS");
      }
      else
      {
        lbl_holder_name.Text = m_card_data.PlayerTracking.HolderName;
      }

      m_tick_count_tracknumber_read = 0;

    }

    #endregion

    #region Constants

    private const Int32 GRID_COLUMN_TICKET_ID = 0;
    private const Int32 GRID_COLUMN_TICKET_NUMBER = 1;
    private const Int32 GRID_COLUMN_TICKET_NUMBER_STRING = 2;
    private const Int32 GRID_COLUMN_TICKET_DATE = 3;
    private const Int32 GRID_COLUMN_TICKET_TERMINAL = 4;
    private const Int32 GRID_COLUMN_TICKET_AMT0 = 5;
    private const Int32 GRID_COLUMN_TICKET_AMOUNT = 6;
    private const Int32 GRID_COLUMN_CREATED_ACCOUNT = 7;
    private const Int32 GRID_COLUMN_TICKET_VALIDATION_TYPE = 8;
    private const Int32 GRID_COLUMN_TICKET_TYPE = 9;
    private const Int32 GRID_COLUMN_TICKET_ACCOUNT = 10;
    private const Int32 GRID_COLUMN_TICKET_TERMINAL_TYPE = 11;

    private const Int32 WIDTH_COLUMN_TICKET_ID = 0;
    private const Int32 WIDTH_COLUMN_TICKET_NUMBER = 0;
    private const Int32 WIDTH_COLUMN_TICKET_NUMBER_STRING = 180;
    private const Int32 WIDTH_COLUMN_TICKET_AMT0 = 135;
    private const Int32 WIDTH_COLUMN_TICKET_AMOUNT = 135;
    private const Int32 WIDTH_COLUMN_TICKET_DATE = 90;
    private const Int32 WIDTH_COLUMN_TICKET_TERMINAL = 190;
    private const Int32 WIDTH_COLUMN_TICKET_VALIDATION_TYPE = 0;
    private const Int32 WIDTH_COLUMN_TICKET_TYPE = 0;
    private const Int32 WIDTH_COLUMN_TICKET_TERMINAL_TYPE = 0;

    #endregion

    #region Private Methods
    /// <summary>
    /// Initialize controls
    /// </summary>
    public void InitControls(frm_container Parent)
    {
      m_parent = Parent;

      ClearForm();

      if (m_first_time)
      {
        uc_ticket_reader.OnValidationNumberReadEvent += new uc_ticket_reader.ValidationNumberReadEventHandler(ProcessTicketNumber);

        m_first_time = false;

        InitializeControlResources();
      }
      m_tick_count_tracknumber_read = 0;
      RefreshCardData(false);

    }

    /// <summary>
    /// Initialize control resources. (NLS string, images, etc.)
    /// OJO: some resources are used more than once
    /// </summary>
    void InitializeControlResources()
    {
      //lbl_cards_title.Text = Resource.String("STR_UC_TICKETS_TITLE");
      lbl_tickets_total.Text = Resource.String("STR_VOUCHER_CARD_ADD_TOTAL_AMOUNT") + ":";
      lbl_tickets_num.Text = Resource.String("STR_UC_TICKET_TICKETS_TO_PAY") + ":";
      btn_ticket_redeem_total.Text = Resource.String("STR_UC_TICKETS_BTN_TICKETS_REEDEM");
      btn_handpays.Text = Resource.String("STR_UC_CARD_BTN_HAND_PAY");
      btn_clean_list.Text = Resource.String("STR_UC_TICKETS_BTN_TICKETS_CLEAN_LIST");
      btn_delete_ticket.Text = Resource.String("STR_UC_TICKETS_BTN_TICKETS_DELETE");
      gb_tickets_list.HeaderText = Resource.String("STR_FRM_VOUCHERS_REPRINT_070");
      lbl_handpays_warning.Text = Resource.String("STR_UC_TICKETS_HANDPAYS_WARNING");
      lbl_account_id_name.Text = Resource.String("STR_FORMAT_GENERAL_NAME_ACCOUNT") + ":";
      lbl_trackdata.Text = Resource.String("STR_FORMAT_GENERAL_NAME_CARD") + ":";
      lbl_name.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_NAME") + ":";
      gbAccount.HeaderText = Resource.String("STR_FORMAT_GENERAL_NAME_ACCOUNT");
      // LTC 29-FEB-2016
      btn_disputes.Text = Resource.String("STR_FRM_TITO_HANDPAYS_OTHERS");

      // LTC 07-MAR-2016
      btn_chips_swap.Text = Resource.String("STR_UC_CHIPS_SWAP");
      if (!GeneralParam.GetBoolean("TITA", "Enabled"))
      {
        btn_chips_swap.Visible = false;
      }

      uc_ticket_reader.InitializeControlResources();
      timer1.Enabled = true;

    }

    private void InitTicketsDataGrid()
    {
      String _date_format;
      String _time_format;

      _date_format = CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern.Replace("/yyyy", "").Replace("yyyy/", "");
      _time_format = CultureInfo.InvariantCulture.DateTimeFormat.LongTimePattern.Replace(":ss", "");

      //Ticket Id
      dgv_tickets_list.Columns[GRID_COLUMN_TICKET_ID].Width = WIDTH_COLUMN_TICKET_ID;
      dgv_tickets_list.Columns[GRID_COLUMN_TICKET_ID].Visible = false;
      dgv_tickets_list.Columns[GRID_COLUMN_TICKET_ID].Resizable = DataGridViewTriState.False;

      //Ticket Number
      dgv_tickets_list.Columns[GRID_COLUMN_TICKET_NUMBER].Width = WIDTH_COLUMN_TICKET_NUMBER;
      dgv_tickets_list.Columns[GRID_COLUMN_TICKET_NUMBER].Visible = false;

      //Ticket Number String
      dgv_tickets_list.Columns[GRID_COLUMN_TICKET_NUMBER_STRING].Width = WIDTH_COLUMN_TICKET_NUMBER_STRING;
      dgv_tickets_list.Columns[GRID_COLUMN_TICKET_NUMBER_STRING].Visible = true;
      dgv_tickets_list.Columns[GRID_COLUMN_TICKET_NUMBER_STRING].HeaderText = Resource.String("STR_FRM_VOUCHERS_REPRINT_056");
      dgv_tickets_list.Columns[GRID_COLUMN_TICKET_NUMBER_STRING].Resizable = DataGridViewTriState.False;

      //Ticket Date
      dgv_tickets_list.Columns[GRID_COLUMN_TICKET_DATE].HeaderText = Resource.String("STR_FRM_VOUCHERS_REPRINT_092");
      dgv_tickets_list.Columns[GRID_COLUMN_TICKET_DATE].DefaultCellStyle.Format = _date_format + " " + _time_format;
      dgv_tickets_list.Columns[GRID_COLUMN_TICKET_DATE].Width = WIDTH_COLUMN_TICKET_DATE;
      dgv_tickets_list.Columns[GRID_COLUMN_TICKET_DATE].Resizable = DataGridViewTriState.False;

      //Ticket Terminal
      dgv_tickets_list.Columns[GRID_COLUMN_TICKET_TERMINAL].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
      dgv_tickets_list.Columns[GRID_COLUMN_TICKET_TERMINAL].HeaderText = Resource.String("STR_VOUCHER_TERMINAL_ID");
      dgv_tickets_list.Columns[GRID_COLUMN_TICKET_TERMINAL].Width = WIDTH_COLUMN_TICKET_TERMINAL;

      if (Misc.IsFloorDualCurrencyEnabled())
      {
        //Ticket created Amount
        dgv_tickets_list.Columns[GRID_COLUMN_TICKET_AMT0].Width = WIDTH_COLUMN_TICKET_AMT0;
        dgv_tickets_list.Columns[GRID_COLUMN_TICKET_AMT0].Visible = true;
        dgv_tickets_list.Columns[GRID_COLUMN_TICKET_AMT0].HeaderText = Resource.String("STR_FRM_VOUCHERS_REPRINT_095");
        dgv_tickets_list.Columns[GRID_COLUMN_TICKET_AMT0].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
        dgv_tickets_list.Columns[GRID_COLUMN_TICKET_AMT0].DefaultCellStyle.Format = "C" + Math.Abs(Format.GetFormattedDecimals(CurrencyExchange.GetNationalCurrency()));
        dgv_tickets_list.Columns[GRID_COLUMN_TICKET_AMT0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        dgv_tickets_list.Columns[GRID_COLUMN_TICKET_AMT0].Resizable = DataGridViewTriState.False;
      }
      else
      {
        dgv_tickets_list.Columns[GRID_COLUMN_TICKET_AMT0].Visible = false;
      }

      //Ticket Amount
      dgv_tickets_list.Columns[GRID_COLUMN_TICKET_AMOUNT].Width = WIDTH_COLUMN_TICKET_AMOUNT;
      dgv_tickets_list.Columns[GRID_COLUMN_TICKET_AMOUNT].Visible = true;
      dgv_tickets_list.Columns[GRID_COLUMN_TICKET_AMOUNT].HeaderText = Resource.String("STR_FRM_VOUCHERS_REPRINT_057");
      dgv_tickets_list.Columns[GRID_COLUMN_TICKET_AMOUNT].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
      dgv_tickets_list.Columns[GRID_COLUMN_TICKET_AMOUNT].DefaultCellStyle.Format = "c";
      dgv_tickets_list.Columns[GRID_COLUMN_TICKET_AMOUNT].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
      dgv_tickets_list.Columns[GRID_COLUMN_TICKET_AMOUNT].Resizable = DataGridViewTriState.False;

      //Validation Type
      dgv_tickets_list.Columns[GRID_COLUMN_TICKET_VALIDATION_TYPE].Width = WIDTH_COLUMN_TICKET_VALIDATION_TYPE;
      dgv_tickets_list.Columns[GRID_COLUMN_TICKET_VALIDATION_TYPE].Visible = false;

      //Ticket Type
      dgv_tickets_list.Columns[GRID_COLUMN_TICKET_TYPE].Width = WIDTH_COLUMN_TICKET_TYPE;
      dgv_tickets_list.Columns[GRID_COLUMN_TICKET_TYPE].Visible = false;

      //Created Account
      dgv_tickets_list.Columns[GRID_COLUMN_CREATED_ACCOUNT].Visible = false;

      // Ticket account
      dgv_tickets_list.Columns[GRID_COLUMN_TICKET_ACCOUNT].Visible = false;

      // Ticket Terminal Type
      dgv_tickets_list.Columns[GRID_COLUMN_TICKET_TERMINAL_TYPE].Visible = false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Restore focus in th form
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    private void RestoreParentFocus()
    {
      this.ParentForm.Focus();
      this.ParentForm.BringToFront();
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Set status of ALL buttons in control
    // 
    //  PARAMS:
    //      - INPUT:
    //        - Ticket: reference to a ticket instance
    //
    //      - OUTPUT:
    //
    private void SetButtonStatus()
    {
      Boolean _is_cashier_open;

      _is_cashier_open = BusinessLogic.IsTITOCashierOpen;

      btn_ticket_redeem_total.Enabled = _is_cashier_open && m_tickets_table.Rows.Count > 0;
      btn_clean_list.Enabled = m_tickets_table.Rows.Count > 0;
      btn_delete_ticket.Enabled = m_tickets_table.Rows.Count > 0 && dgv_tickets_list.SelectedRows.Count > 0;
      btn_handpays.Enabled = _is_cashier_open;
      btn_disputes.Enabled = _is_cashier_open;
      uc_ticket_reader.Enabled = _is_cashier_open;
      lbl_tickets_num_value.Text = m_tickets_table.Rows.Count.ToString();
      // LTC 07-MAR-2016
      btn_chips_swap.Enabled = _is_cashier_open && m_tickets_table.Rows.Count > 0;
      uc_ticket_reader.Focus();
      m_tick_count_tracknumber_read = 0;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Enable status of ALL buttons in control
    // 
    //  PARAMS:
    //      - INPUT:
    //        - Ticket: reference to a ticket instance
    //
    //      - OUTPUT:
    //
    private void EnableButtonStatus(Boolean Enable)
    {
      btn_ticket_redeem_total.Enabled = Enable;
      btn_clean_list.Enabled = Enable;
      btn_delete_ticket.Enabled = Enable;
      btn_handpays.Enabled = Enable;
      uc_ticket_reader.Enabled = Enable;
      // LTC 07-MAR-2016
      btn_chips_swap.Enabled = Enabled;
      uc_ticket_reader.Focus();
    }

    //------------------------------------------------------------------------------
    // PURPOSE: clear all controls in the form
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    private void ClearForm()
    {
      ClearTicketControls();
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Refresh texts in all controls in the form
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    private void RefreshForm(Ticket Ticket)
    {
      RefreshTicketControls(Ticket);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Clear controls related to ticket
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    private void ClearTicketControls()
    {
      try
      {
        // Disable buttons
        EnableButtonStatus(false);

        RefreshTicketControls(null);
        uc_ticket_reader.ClearTrackNumber();
        uc_ticket_reader.Focus();
      }
      catch { }
      finally
      {
        // Enable/disable buttons
        SetButtonStatus();
      }

    }

    //------------------------------------------------------------------------------
    // PURPOSE: Refresh content of controls related to ticket
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    private void RefreshTicketControls(Ticket Ticket)
    {
      DataRow _new_row;
      CardData _card;

      _card = new CardData();

      if (Ticket != null)
      {
        if (Ticket.Amount <= 0)
        {
          Log.Error("Ticket Redeem: Ticket amount not valid. Ticket ID: " + Ticket.TicketID);

          return;
        }

        if (m_tickets_table.Rows.Contains(Ticket.TicketID))
        {
          return;
        }

        _new_row = m_tickets_table.NewRow();
        _new_row[GRID_COLUMN_TICKET_ID] = Ticket.TicketID;
        _new_row[GRID_COLUMN_TICKET_NUMBER] = Ticket.ValidationNumber;
        _new_row[GRID_COLUMN_TICKET_NUMBER_STRING] = ValidationNumberManager.FormatValidationNumber(Ticket.ValidationNumber, (Int32)TITO_VALIDATION_TYPE.SYSTEM, false);
        _new_row[GRID_COLUMN_TICKET_DATE] = Ticket.CreatedDateTime;
        // DHA: added terminal column
        _new_row[GRID_COLUMN_TICKET_TERMINAL] = Ticket.CreatedTerminalName;
        _new_row[GRID_COLUMN_TICKET_ACCOUNT] = "";
        _new_row[GRID_COLUMN_CREATED_ACCOUNT] = 0;
        _new_row[GRID_COLUMN_TICKET_AMT0] = Currency.Format((Currency)Ticket.Amt_0, Ticket.Cur_0);
        _new_row[GRID_COLUMN_TICKET_AMOUNT] = (Currency)Ticket.Amount;
        _new_row[GRID_COLUMN_TICKET_VALIDATION_TYPE] = (TITO_VALIDATION_TYPE)Ticket.ValidationType;
        _new_row[GRID_COLUMN_TICKET_TYPE] = Ticket.TicketType;
        _new_row[GRID_COLUMN_TICKET_TERMINAL_TYPE] = (TITO_TERMINAL_TYPE)Ticket.CreatedTerminalType;

        if (CardData.DB_CardGetAllData(Ticket.CreatedAccountID, _card))
        {
          _new_row[GRID_COLUMN_TICKET_ACCOUNT] = _card.PlayerTracking.HolderName;
          _new_row[GRID_COLUMN_CREATED_ACCOUNT] = _card.IsVirtualCard ? 0 : _card.AccountId;
        }

        m_tickets_table.Rows.Add(_new_row);
        m_tickets_total_amount += Ticket.Amount;

      }
      else
      {
        m_tickets_table.Rows.Clear();
        m_tickets_total_amount = 0;
      }

      UpdateTicketsGrid();

      lbl_tickets_amount.Text = m_tickets_total_amount.ToString();

      SetButtonStatus();

      // Last ticket inserted always be visible
      if (Ticket != null && dgv_tickets_list.RowCount > 0)
      {
        dgv_tickets_list.Rows[dgv_tickets_list.RowCount - 1].Selected = true;
        dgv_tickets_list.FirstDisplayedScrollingRowIndex = dgv_tickets_list.RowCount - 1;
      }
    }

    private String GetInvalidStatusMsg(TITO_TICKET_STATUS Status)
    {
      String _msg;

      _msg = "";

      //SGB 31-OCT-2014: Create ticket status of pending_cancel
      switch (Status)
      {
        case TITO_TICKET_STATUS.DISCARDED:
          _msg = Resource.String("STR_UC_TITO_TICKET_STATUS_DISCARDED");
          break;
        case TITO_TICKET_STATUS.CANCELED:
          _msg = Resource.String("STR_UC_TITO_TICKET_STATUS_CANCELED");
          break;
        case TITO_TICKET_STATUS.PENDING_CANCEL:
          // RAB 23-FEB-2016
          Boolean _allow_to_redeem_gp;
          _allow_to_redeem_gp = GeneralParam.GetBoolean("TITO", "PendingCancel.AllowToRedeem", false);

          if (!_allow_to_redeem_gp)
          {
            _msg = Resource.String("STR_UC_TITO_TICKET_NOT_ALLOWED_REDEEM");
          }
          break;
        case TITO_TICKET_STATUS.EXPIRED:
          _msg = Resource.String("STR_UC_TITO_TICKET_STATUS_EXPIRED");
          break;
        case TITO_TICKET_STATUS.REDEEMED:
        case TITO_TICKET_STATUS.PAID_AFTER_EXPIRATION:
          _msg = Resource.String("STR_UC_TITO_TICKET_STATUS_REDEEMED");
          break;
        case TITO_TICKET_STATUS.SWAP_FOR_CHIPS:
          //LTC 01-JUL-2016
          _msg = Resource.String("STR_UC_TITO_TICKET_STATUS_SWAPPED").Replace("\\n", "\n");
          break;
        case TITO_TICKET_STATUS.PENDING_PRINT:
          //JRC 06-OCT-2016
          _msg = Resource.String("STR_UC_TITO_TICKET_STATUS_PENDING_PRINT").Replace("\\n", "\n");
          break;
        default:
          break;
      }

      return _msg;
      //frm_message.Show(_msg, Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this.ParentForm);
    }

    private void UpdateTicketsGrid()
    {
      dgv_tickets_list.DataSource = null;
      dgv_tickets_list.DataSource = m_tickets_table;
      InitTicketsDataGrid();
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Process the entered text, when it is a ticket Validation Number
    // 
    //  PARAMS:
    //      - INPUT:
    //        - String: ValidationNumber
    //
    //      - OUTPUT:
    //
    private void ProcessTicketNumber(String ValidationNumber, ref Boolean IsValid, out String ErrorStr)
    {
      Ticket _ticket;
      Int64 _validation_number;
      TITO_VALIDATION_TYPE _validation_type;
      DateTime _expiration_date_limit;
      DialogResult _dialog_grace_period;
      String _error_str;
      Boolean _allow_pay_ticket;
      bool _allow_pay_tickets_pending_print;

      _allow_pay_tickets_pending_print = GeneralParam.GetBoolean("TITO", "PendingPrint.AllowToRedeem", true);
      IsValid = false;
      ErrorStr = "";
      _validation_number = 0;
      _error_str = string.Empty;
      _allow_pay_ticket = true;

      try
      {
        // Disable buttons
        EnableButtonStatus(false);

        if (String.IsNullOrEmpty(ValidationNumber))
        {
          IsValid = true;

          return;
        }



        _validation_number = ValidationNumberManager.UnFormatValidationNumber(ValidationNumber, out _validation_type);

        if (_validation_number > 0)
        {
          // First validate that ticket is not yet in the grid
          if (m_tickets_table.Select(String.Format("TICKET_NUMBER = {0}", _validation_number)).Length > 0)
          {
            //frm_message.Show(Resource.String("STR_UC_TITO_TICKET_LISTED"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this.ParentForm);
            ErrorStr = Resource.String("STR_UC_TITO_TICKET_LISTED");

            return;
          }

          using (DB_TRX _db_trx = new DB_TRX())
          {
            _ticket = BusinessLogic.SelectTicketByCondition(String.Format(" TI_VALIDATION_NUMBER = {0} ", _validation_number), String.Format(" TI_STATUS = {0} ", (Int32)TITO_TICKET_STATUS.VALID));
            if (_ticket.TicketType == TITO_TICKET_TYPE.CHIPS_DEALER_COPY)
            {
              ErrorStr = Resource.String("STR_UC_DEALER_COPY_DEALER_NOT_FOUND");
              ClearTicketControls();
              return;
            }

            if (_allow_pay_tickets_pending_print)
            {
              _ticket = BusinessLogic.SelectTicketByCondition(String.Format(" TI_VALIDATION_NUMBER = {0} ", _validation_number),
                                                                String.Format(" TI_STATUS IN ({0}, {1}, {2}) ", (Int32)TITO_TICKET_STATUS.VALID, (Int32)TITO_TICKET_STATUS.PENDING_CANCEL, (Int32)TITO_TICKET_STATUS.PENDING_PRINT));

              if (_ticket.Status == TITO_TICKET_STATUS.PENDING_PRINT)
                Log.Message("WSI.Cashier.uc_ticket_view ProcessTicketNumber: Ticket " + _ticket.ValidationNumber + " has status PENDING PRINTING");

            }
            else
            {
              _ticket = BusinessLogic.SelectTicketByCondition(String.Format(" TI_VALIDATION_NUMBER = {0} ", _validation_number),
                                                            String.Format(" TI_STATUS IN ({0}, {1}) ", (Int32)TITO_TICKET_STATUS.VALID, (Int32)TITO_TICKET_STATUS.PENDING_CANCEL));

              // XGJ 20-OCT-2016
              if (_ticket.Status == TITO_TICKET_STATUS.PENDING_PRINT)
              {
                ErrorStr = Resource.String("STR_TICKETS_PENDING_PRINTING");
                return;
              }
              //Log.Message("WSI.Cashier.uc_ticket_view ProcessTicketNumber: Ticket " + _ticket.ValidationNumber + " has status PENDING PRINTING");
            }

            if (_ticket.ValidationNumber > 0)
            {
              BusinessLogic.CheckGracePeriod(_ticket.ExpirationDateTime, out _expiration_date_limit);

              // LTC 01-JUL-2016
              // RAB 23-FEB-2016
              if (_ticket.Status == TITO_TICKET_STATUS.REDEEMED || _ticket.Status == TITO_TICKET_STATUS.PAID_AFTER_EXPIRATION
                  || _ticket.Status == TITO_TICKET_STATUS.DISCARDED || _ticket.Status == TITO_TICKET_STATUS.CANCELED || _ticket.Status == TITO_TICKET_STATUS.SWAP_FOR_CHIPS)
              {
                ErrorStr = GetInvalidStatusMsg(_ticket.Status);
                return;
              }

              if (_ticket.ExpirationDateTime < WGDB.Now && _ticket.IsRedeemable)
              {
                _allow_pay_ticket = false;
                if (_expiration_date_limit != DateTime.MinValue && WGDB.Now < _expiration_date_limit)
                {
                  _dialog_grace_period = frm_message.Show(Resource.String("STR_TITO_TICKET_GRACE_PERIOD").Replace("\\r\\n", "\r\n"), "", MessageBoxButtons.YesNo, this.ParentForm);

                  if (_dialog_grace_period == DialogResult.OK)
                  {
                    if (ProfilePermissions.CheckPermissionList(ProfilePermissions.CashierFormFuncionality.TITO_AllowTicketPaymentAfterExpirationDate,
                                             ProfilePermissions.TypeOperation.RequestPasswd,
                                             this.ParentForm,
                                             Cashier.AuthorizedByUserId,
                                             out _error_str))
                    {
                      _allow_pay_ticket = true;
                      // MPO Commented PBI 9887
                      //_ticket.Status = TITO_TICKET_STATUS.VALID;
                    }
                  } //(_dialog_grace_period == DialogResult.Yes)
                } //(_expiration_date_limit != DateTime.MinValue && _ticket.ExpirationDateTime < _expiration_date_limit)
              } //(_ticket.ExpirationDateTime < WGDB.Now)

              if (!_allow_pay_ticket)
              {
                ErrorStr = GetInvalidStatusMsg(TITO_TICKET_STATUS.EXPIRED);

                return;
              }

              if (!_ticket.IsRedeemable)
              {
                ErrorStr = Resource.String("STR_UC_TITO_TICKET_NOT_ALLOWED_REDEEM");
                return;
              }

              // If ticket is Jackpot or Handpay, we must check the vinculated handpay entry to verify if the ticket is valid
              if (_allow_pay_tickets_pending_print)
              {
                if ((_ticket.Status == TITO_TICKET_STATUS.VALID || _ticket.Status == TITO_TICKET_STATUS.PENDING_PRINT) && (_ticket.TicketType == TITO_TICKET_TYPE.HANDPAY || _ticket.TicketType == TITO_TICKET_TYPE.JACKPOT))
                {
                  switch (WSI.Common.TITO.HandPay.GetTicketHandpayStatus(_ticket, _db_trx.SqlTransaction))
                  {
                    case WSI.Common.TITO.HandPay.TICKET_HANDPAY_STATUS.EXPIRED:
                      _ticket.Status = TITO_TICKET_STATUS.EXPIRED;
                      break;
                    case WSI.Common.TITO.HandPay.TICKET_HANDPAY_STATUS.REDEEMED:
                      _ticket.Status = TITO_TICKET_STATUS.REDEEMED;
                      break;
                    case WSI.Common.TITO.HandPay.TICKET_HANDPAY_STATUS.INCORRECT_AMOUNTS:
                    case WSI.Common.TITO.HandPay.TICKET_HANDPAY_STATUS.NOT_FOUND:
                      _ticket.Status = TITO_TICKET_STATUS.DISCARDED;
                      break;
                    case WSI.Common.TITO.HandPay.TICKET_HANDPAY_STATUS.VALID:
                    default:
                      break;
                  }
                }
              }
              else
              {
                if (_ticket.Status == TITO_TICKET_STATUS.VALID && (_ticket.TicketType == TITO_TICKET_TYPE.HANDPAY || _ticket.TicketType == TITO_TICKET_TYPE.JACKPOT))
                {
                  switch (WSI.Common.TITO.HandPay.GetTicketHandpayStatus(_ticket, _db_trx.SqlTransaction))
                  {
                    case WSI.Common.TITO.HandPay.TICKET_HANDPAY_STATUS.EXPIRED:
                      _ticket.Status = TITO_TICKET_STATUS.EXPIRED;
                      break;
                    case WSI.Common.TITO.HandPay.TICKET_HANDPAY_STATUS.REDEEMED:
                      _ticket.Status = TITO_TICKET_STATUS.REDEEMED;
                      break;
                    case WSI.Common.TITO.HandPay.TICKET_HANDPAY_STATUS.INCORRECT_AMOUNTS:
                    case WSI.Common.TITO.HandPay.TICKET_HANDPAY_STATUS.NOT_FOUND:
                      _ticket.Status = TITO_TICKET_STATUS.DISCARDED;
                      break;
                    case WSI.Common.TITO.HandPay.TICKET_HANDPAY_STATUS.VALID:
                    default:
                      break;
                  }
                }
              }


              RefreshTicketControls(_ticket);
              IsValid = true;
            }

          }

          RestoreParentFocus();
          InitFocus();
        }
      }
      catch { }
      finally
      {
        // Enable/disable buttons
        SetButtonStatus();

      }
    }

    private List<Int64> GetListTicketNumbers()
    {
      List<Int64> _list;

      _list = new List<Int64>();

      foreach (DataRow _row in m_tickets_table.Rows)
      {
        _list.Add((Int64)_row[GRID_COLUMN_TICKET_ID]);
      }

      return _list;
    }

    private List<Ticket> GetTicketsList()
    {
      List<Ticket> _list;
      Ticket _ticket;

      _list = new List<Ticket>();

      foreach (DataRow _row in m_tickets_table.Rows)
      {
        _ticket = new Ticket();
        _ticket.TicketID = (Int64)_row[GRID_COLUMN_TICKET_ID];
        _ticket.TicketType = (TITO_TICKET_TYPE)_row[GRID_COLUMN_TICKET_TYPE];
        _ticket.Amount = (Currency)_row[GRID_COLUMN_TICKET_AMOUNT];
        _ticket.CreatedDateTime = (DateTime)_row[GRID_COLUMN_TICKET_DATE];
        _ticket.ValidationNumber = (Int64)_row[GRID_COLUMN_TICKET_NUMBER];
        _ticket.ValidationType = (TITO_VALIDATION_TYPE)_row[GRID_COLUMN_TICKET_VALIDATION_TYPE];
        _ticket.CreatedTerminalType = (Int32)_row[GRID_COLUMN_TICKET_TERMINAL_TYPE];
        _list.Add(_ticket);
      }

      return _list;
    }

    private Boolean GetAccountOperation(CardData Card)
    {
      //Int64 _account_id;

      // MPO 22-JAN-2015 WIG-1947: If the personal account have a promotion, will cancel the promotion. 
      //                           In TITO the account should keep his promotions. 
      //// Check number of tickets in row
      ////if (m_tickets_table.Rows.Count == 1)
      ////{
      ////  // 1 ticket, search for accountid
      ////  _account_id = (Int64)m_tickets_table.Rows[0][GRID_COLUMN_CREATED_ACCOUNT];
      ////  if (_account_id != 0)
      ////  {
      ////    // Account must be valid and not bloqued
      ////    if (CardData.DB_CardGetAllData(_account_id, Card) && !Card.Blocked)
      ////    {
      ////      return true;
      ////    }
      ////  }
      ////}

      // More than one ticket or ticket in row has not an account id: Use virtual account
      if (Card.IsVirtualCard || Card.AccountId == 0)
      {
        if (!BusinessLogic.LoadVirtualAccount(Card))
        {
          return false;
        }
      }

      return true;
    }

    #endregion

    #region Events

    private void btn_ticket_redeem_total_Click(object sender, EventArgs e)
    {
      ParamsTicketOperation _params_ticket_redeem;
      CardData _card;
      ENUM_TITO_OPERATIONS_RESULT _result;
      String _error_str;
      Ticket _ticket;
      VoucherValidationNumber _voucher_ticket_data;

      try
      {
        // Disable buttons
        EnableButtonStatus(false);
        timer1.Enabled = false;

        if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.TITO_RedeemTicket,
                                                ProfilePermissions.TypeOperation.RequestPasswd,
                                                this.ParentForm,
                                                out _error_str))
        {
          return;
        }

        _card = m_card_data;

        if (!GetAccountOperation(_card))
        {
          Log.Error("btn_ticket_redeem_total_Click: Previous control. There are no account to redeem");

          return;
        }

        m_frm_shadow_gui.Show(this.ParentForm);

        // input params for payment algorithm
        _params_ticket_redeem = new ParamsTicketOperation();
        _params_ticket_redeem.in_account = _card;
        _params_ticket_redeem.in_cash_amount = m_tickets_total_amount;
        _params_ticket_redeem.in_window_parent = this.ParentForm;
        _params_ticket_redeem.session_info = Cashier.CashierSessionInfo();
        _params_ticket_redeem.max_devolution = Utils.MaxDevolutionAmount(GetTicketsList());
        _params_ticket_redeem.validation_numbers = new List<VoucherValidationNumber>();

        foreach (DataGridViewRow _row in dgv_tickets_list.Rows)
        {
          _ticket = Ticket.LoadTicket((Int64)_row.Cells[GRID_COLUMN_TICKET_ID].Value);
          if (_ticket != null)
          {
            _voucher_ticket_data = new VoucherValidationNumber();
            _voucher_ticket_data.ValidationNumber = _ticket.ValidationNumber;
            _voucher_ticket_data.Amount = _ticket.Amount;
            _voucher_ticket_data.ValidationType = _ticket.ValidationType;
            _voucher_ticket_data.TicketType = _ticket.TicketType;
            _voucher_ticket_data.CreatedTerminalName = _ticket.CreatedTerminalName;
            _voucher_ticket_data.CreatedPlaySessionId = _ticket.CreatedPlaySessionId;
            _voucher_ticket_data.CreatedAccountId = _ticket.CreatedAccountID;
            _voucher_ticket_data.TicketId = _ticket.TicketID;
            _voucher_ticket_data.CreatedTerminalType = _ticket.CreatedTerminalType;
            _params_ticket_redeem.validation_numbers.Add(_voucher_ticket_data);
          }
        }

        PaymentThresholdAuthorization _payment_threshold_authorization;
        PaymentAndRechargeThresholdData _payment_and_recharge_threshold_data;
        Boolean _process_continue;

        _process_continue = false;
        _result = BusinessLogic.PreviewVoucherTicketRedeem(ref _params_ticket_redeem, out _payment_threshold_authorization);

        if (_result == ENUM_TITO_OPERATIONS_RESULT.OK)
        {
          if ((_result = CashierBusinessLogic.WithholdAndPaymentOrderDialog(OperationCode.CASH_OUT, ref _params_ticket_redeem,
                                                                            CashierBusinessLogic.EnterWitholdingData,
                                                                            CashierBusinessLogic.EnterOrderPaymentData, null))
              == ENUM_TITO_OPERATIONS_RESULT.OK)
          {

            _payment_and_recharge_threshold_data = new PaymentAndRechargeThresholdData();
            _payment_threshold_authorization = new PaymentThresholdAuthorization(_params_ticket_redeem.in_cash_amount, m_card_data, _payment_and_recharge_threshold_data, PaymentThresholdAuthorization.PaymentThresholdAuthorizationOperation.Output, this.ParentForm, out _process_continue);

            if (!_process_continue)
            {
              _result = ENUM_TITO_OPERATIONS_RESULT.AUX_OPERATION_CANCELED;
            }
            else
            {
              _result = BusinessLogic.TicketRedeemCashOut(GetListTicketNumbers(), _params_ticket_redeem, OperationCode.CASH_OUT, _payment_threshold_authorization);
            }
          }
        }

        switch (_result)
        {
          case ENUM_TITO_OPERATIONS_RESULT.OK:
            ClearForm();
            break;
          case ENUM_TITO_OPERATIONS_RESULT.AUX_OPERATION_CANCELED:
            //do nothing
            break;
          case ENUM_TITO_OPERATIONS_RESULT.TICKET_NOT_VALID:
            break;
          default:
            Log.Warning(string.Format("The redeem of the ticket has returned [{0}]", _result.ToString()));
            frm_message.Show(Resource.String("STR_TITO_TICKET_ERROR_PAYING_TICKET"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this.ParentForm);
            break;
        }

        this.m_frm_shadow_gui.Hide();
        this.RestoreParentFocus();
      }
      catch { }
      finally
      {
        // Enable/disable buttons
        SetButtonStatus();
        timer1.Enabled = true;
      }

    } // btn_ticket_redeem_total_Click

    private void btn_clean_list_Click(object sender, EventArgs e)
    {
      ClearTicketControls();
    }

    private void btn_delete_ticket_Click(object sender, EventArgs e)
    {
      Int32 _row_index;

      try
      {
        // Disable buttons
        EnableButtonStatus(false);

        if (dgv_tickets_list.SelectedRows.Count > 0)
        {
          _row_index = dgv_tickets_list.SelectedRows[0].Index;
          m_tickets_total_amount -= (Currency)m_tickets_table.Rows[_row_index]["TICKET_AMOUNT"];
          lbl_tickets_amount.Text = m_tickets_total_amount.ToString();
          dgv_tickets_list.Rows.RemoveAt(_row_index);
        }
      }
      catch { }
      finally
      {
        // Enable/disable buttons
        SetButtonStatus();
      }
    }

    private void dgv_tickets_list_SelectionChanged(object sender, EventArgs e)
    {
      btn_delete_ticket.Enabled = m_tickets_table.Rows.Count > 0 && dgv_tickets_list.SelectedRows.Count > 0;
    }

    private void buttons_EnabledChanged(object sender, EventArgs e)
    {
      System.Windows.Forms.Button a;

      a = (System.Windows.Forms.Button)sender;

      if (a.Enabled)
      {
        a.BackColor = System.Drawing.Color.PaleTurquoise;
      }
      else
      {
        a.BackColor = System.Drawing.Color.LightGray;
      }
    }

    #endregion

    private void btn_handpays_Click(object sender, EventArgs e)
    {
      String _error_str;

      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.TITO_HandPay,
                                               ProfilePermissions.TypeOperation.RequestPasswd,
                                               this.ParentForm,
                                               out _error_str))
      {
        return;
      }

      frm_TITO_handpays _TITO_handpays;

      _TITO_handpays = new frm_TITO_handpays();

      if (m_card_data.AccountId > 0)
      {
        _TITO_handpays.AccountId = m_card_data.AccountId;
        _TITO_handpays.IsVirtual = m_card_data.IsVirtualCard;
      }
      _TITO_handpays.Show(this.ParentForm);
      _TITO_handpays.Dispose();
      _TITO_handpays = null;
      this.RestoreParentFocus();
    }
    /// LTC 29-FEB-2016
    /// <summary>
    /// Disputes Button Click Event - Set payments by disputes
    /// </summary>
    /// <param name="sender">Button Object</param>
    /// <param name="e">Arguments</param>
    private void btn_disputes_Click(object sender, EventArgs e)
    {
      String _error_str;

      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.TITO_HandPay,
                                               ProfilePermissions.TypeOperation.RequestPasswd,
                                               this.ParentForm,
                                               out _error_str))
      {
        return;
      }

      //DLM 12-JUN-2018 using (frm_handpay_action _frm = new frm_handpay_action(frm_handpay_action.ENTRY_TYPE.MANUAL, 0, null, true))
      using (frm_handpay_action _frm = new frm_handpay_action(frm_handpay_action.ENTRY_TYPE.MANUAL, 0, null, null, null, this, null, true))
      {
        if (m_card_data.AccountId > 0)
        {
          _frm.AccountId = m_card_data.AccountId;
        }

        _frm.Show(this.ParentForm);
      }
      this.RestoreParentFocus();
    }
    // LTC 07-MAR-2016
    /// <summary>
    /// TITO Chips Swap Click Event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btn_chips_swap_Click(object sender, EventArgs e)
    {
      //Definitions
      uc_ticket_create _uc_ticket_create = new uc_ticket_create();
      Ticket _ticket = new Ticket();
      ParamsTicketOperation _d_tickets;
      CardData _card_data = new CardData();
      String _error_str;
      VoucherValidationNumber _voucher_ticket_data;

      //Check Permissions
      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.TITO_ChipsSwap,
                                               ProfilePermissions.TypeOperation.RequestPasswd,
                                               this.ParentForm,
                                               out _error_str))
      {
        return;
      }

      //Get CardData
      if (!GetAccountOperation(_card_data))
      {
        Log.Error("btn_ticket_swap_Click: There is no virtual account ");

        //Clean Controls
        ClearTicketControls();
        return;
      }


      _d_tickets = new ParamsTicketOperation();

      _d_tickets.in_account = _card_data;
      _d_tickets.in_cash_amount = m_tickets_total_amount;
      _d_tickets.in_window_parent = this.ParentForm;
      _d_tickets.session_info = Cashier.CashierSessionInfo();
      _d_tickets.max_devolution = Utils.MaxDevolutionAmount(GetTicketsList());
      _d_tickets.validation_numbers = new List<VoucherValidationNumber>();

      foreach (DataGridViewRow _row in dgv_tickets_list.Rows)
      {
        _ticket = Ticket.LoadTicket((Int64)_row.Cells[GRID_COLUMN_TICKET_ID].Value);
        if (_ticket != null)
        {
          _voucher_ticket_data = new VoucherValidationNumber();
          _voucher_ticket_data.ValidationNumber = _ticket.ValidationNumber;
          _voucher_ticket_data.Amount = _ticket.Amount;
          _voucher_ticket_data.ValidationType = _ticket.ValidationType;
          _voucher_ticket_data.TicketType = _ticket.TicketType;
          _voucher_ticket_data.CreatedTerminalName = _ticket.CreatedTerminalName;
          _voucher_ticket_data.CreatedPlaySessionId = _ticket.CreatedPlaySessionId;
          _voucher_ticket_data.CreatedAccountId = _ticket.CreatedAccountID;
          _voucher_ticket_data.TicketId = _ticket.TicketID;
          _d_tickets.validation_numbers.Add(_voucher_ticket_data);
        }
      }

      // LTC 08-ABR-2016
      //Call ticket create
      if (_uc_ticket_create.btn_chips_swap_with_ticket_tito_Click(_d_tickets, m_tickets_total_amount, _card_data))
      {
        //Clean Controls
        ClearTicketControls();
      }
    }// uc_ticket_view

    private void timer1_Tick(object sender, EventArgs e)
    {
      m_tick_count_tracknumber_read += 1;

      if (m_tick_count_tracknumber_read >= DURATION_BUTTONS_ENABLED_AFTER_OPERATION)
      {
        // 10-JAN-2014 JPJ     Account information in the AccountWatcher thread is cleared.
        RefreshCardData(true);
        return;
      }
    } // timer1_Tick
  }
}
