namespace WSI.Cashier
{
  partial class uc_ticket_view
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(uc_ticket_view));
      this.lbl_separator_1 = new System.Windows.Forms.Label();
      this.pb_uc_icon = new System.Windows.Forms.PictureBox();
      this.lbl_tickets_num_value = new WSI.Cashier.Controls.uc_label();
      this.lbl_tickets_num = new WSI.Cashier.Controls.uc_label();
      this.lbl_handpays_warning = new WSI.Cashier.Controls.uc_label();
      this.btn_handpays = new WSI.Cashier.Controls.uc_round_button();
      this.btn_delete_ticket = new WSI.Cashier.Controls.uc_round_button();
      this.btn_clean_list = new WSI.Cashier.Controls.uc_round_button();
      this.gb_tickets_list = new WSI.Cashier.Controls.uc_round_panel();
      this.dgv_tickets_list = new WSI.Cashier.Controls.uc_DataGridView();
      this.lbl_tickets_amount = new WSI.Cashier.Controls.uc_label();
      this.lbl_tickets_total = new WSI.Cashier.Controls.uc_label();
      this.uc_ticket_reader = new WSI.Cashier.TITO.uc_ticket_reader();
      this.btn_ticket_redeem_total = new WSI.Cashier.Controls.uc_round_button();
      this.btn_chips_swap = new WSI.Cashier.Controls.uc_round_button();
      this.btn_disputes = new WSI.Cashier.Controls.uc_round_button();
      this.gbAccount = new WSI.Cashier.Controls.uc_round_panel();
      this.pb_user = new System.Windows.Forms.PictureBox();
      this.lbl_account_id_value = new WSI.Cashier.Controls.uc_label();
      this.lbl_holder_name = new WSI.Cashier.Controls.uc_label();
      this.lbl_account_id_name = new WSI.Cashier.Controls.uc_label();
      this.lbl_track_number = new WSI.Cashier.Controls.uc_label();
      this.lbl_trackdata = new WSI.Cashier.Controls.uc_label();
      this.lbl_name = new WSI.Cashier.Controls.uc_label();
      this.timer1 = new System.Windows.Forms.Timer(this.components);
      ((System.ComponentModel.ISupportInitialize)(this.pb_uc_icon)).BeginInit();
      this.gb_tickets_list.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dgv_tickets_list)).BeginInit();
      this.gbAccount.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pb_user)).BeginInit();
      this.SuspendLayout();
      // 
      // lbl_separator_1
      // 
      this.lbl_separator_1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_separator_1.BackColor = System.Drawing.Color.Black;
      this.lbl_separator_1.Location = new System.Drawing.Point(-3, 60);
      this.lbl_separator_1.Name = "lbl_separator_1";
      this.lbl_separator_1.Size = new System.Drawing.Size(885, 1);
      this.lbl_separator_1.TabIndex = 1;
      // 
      // pb_uc_icon
      // 
      this.pb_uc_icon.Image = global::WSI.Cashier.Properties.Resources.pagoTickets;
      this.pb_uc_icon.Location = new System.Drawing.Point(10, 7);
      this.pb_uc_icon.Name = "pb_uc_icon";
      this.pb_uc_icon.Size = new System.Drawing.Size(50, 50);
      this.pb_uc_icon.TabIndex = 124;
      this.pb_uc_icon.TabStop = false;
      // 
      // lbl_tickets_num_value
      // 
      this.lbl_tickets_num_value.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_tickets_num_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_tickets_num_value.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_tickets_num_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_tickets_num_value.Location = new System.Drawing.Point(547, 553);
      this.lbl_tickets_num_value.Name = "lbl_tickets_num_value";
      this.lbl_tickets_num_value.Size = new System.Drawing.Size(160, 30);
      this.lbl_tickets_num_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_tickets_num_value.TabIndex = 7;
      this.lbl_tickets_num_value.Text = "123456789";
      this.lbl_tickets_num_value.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_tickets_num
      // 
      this.lbl_tickets_num.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_tickets_num.AutoSize = true;
      this.lbl_tickets_num.BackColor = System.Drawing.Color.Transparent;
      this.lbl_tickets_num.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_tickets_num.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_tickets_num.Location = new System.Drawing.Point(414, 559);
      this.lbl_tickets_num.Name = "lbl_tickets_num";
      this.lbl_tickets_num.Size = new System.Drawing.Size(106, 19);
      this.lbl_tickets_num.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_tickets_num.TabIndex = 6;
      this.lbl_tickets_num.Text = "xNo.OfTickets";
      this.lbl_tickets_num.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_handpays_warning
      // 
      this.lbl_handpays_warning.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_handpays_warning.BackColor = System.Drawing.Color.Transparent;
      this.lbl_handpays_warning.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_handpays_warning.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_handpays_warning.Location = new System.Drawing.Point(12, 623);
      this.lbl_handpays_warning.Name = "lbl_handpays_warning";
      this.lbl_handpays_warning.Size = new System.Drawing.Size(844, 23);
      this.lbl_handpays_warning.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_handpays_warning.TabIndex = 11;
      this.lbl_handpays_warning.Text = "xHandPaysWithoutTicketsMustBePaidAsHandPays";
      this.lbl_handpays_warning.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.lbl_handpays_warning.Visible = false;
      // 
      // btn_handpays
      // 
      this.btn_handpays.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_handpays.FlatAppearance.BorderSize = 0;
      this.btn_handpays.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_handpays.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_handpays.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_handpays.Image = null;
      this.btn_handpays.IsSelected = false;
      this.btn_handpays.Location = new System.Drawing.Point(13, 553);
      this.btn_handpays.Name = "btn_handpays";
      this.btn_handpays.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_handpays.Size = new System.Drawing.Size(155, 60);
      this.btn_handpays.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_handpays.TabIndex = 5;
      this.btn_handpays.Text = "XHANDPAYS";
      this.btn_handpays.UseVisualStyleBackColor = false;
      this.btn_handpays.Click += new System.EventHandler(this.btn_handpays_Click);
      // 
      // btn_delete_ticket
      // 
      this.btn_delete_ticket.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_delete_ticket.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(47)))), ((int)(((byte)(50)))));
      this.btn_delete_ticket.FlatAppearance.BorderSize = 0;
      this.btn_delete_ticket.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_delete_ticket.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_delete_ticket.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_delete_ticket.Image = null;
      this.btn_delete_ticket.IsSelected = false;
      this.btn_delete_ticket.Location = new System.Drawing.Point(721, 204);
      this.btn_delete_ticket.Name = "btn_delete_ticket";
      this.btn_delete_ticket.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_delete_ticket.Size = new System.Drawing.Size(155, 60);
      this.btn_delete_ticket.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.TERTIARY;
      this.btn_delete_ticket.TabIndex = 3;
      this.btn_delete_ticket.Text = "XDELETETICKET";
      this.btn_delete_ticket.UseVisualStyleBackColor = false;
      this.btn_delete_ticket.Click += new System.EventHandler(this.btn_delete_ticket_Click);
      // 
      // btn_clean_list
      // 
      this.btn_clean_list.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_clean_list.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(47)))), ((int)(((byte)(50)))));
      this.btn_clean_list.FlatAppearance.BorderSize = 0;
      this.btn_clean_list.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_clean_list.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_clean_list.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_clean_list.Image = null;
      this.btn_clean_list.IsSelected = false;
      this.btn_clean_list.Location = new System.Drawing.Point(721, 272);
      this.btn_clean_list.Name = "btn_clean_list";
      this.btn_clean_list.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_clean_list.Size = new System.Drawing.Size(155, 60);
      this.btn_clean_list.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.TERTIARY;
      this.btn_clean_list.TabIndex = 4;
      this.btn_clean_list.Text = "XCLEANLIST";
      this.btn_clean_list.UseVisualStyleBackColor = false;
      this.btn_clean_list.Click += new System.EventHandler(this.btn_clean_list_Click);
      // 
      // gb_tickets_list
      // 
      this.gb_tickets_list.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.gb_tickets_list.BackColor = System.Drawing.Color.Transparent;
      this.gb_tickets_list.BorderColor = System.Drawing.Color.Empty;
      this.gb_tickets_list.Controls.Add(this.dgv_tickets_list);
      this.gb_tickets_list.CornerRadius = 10;
      this.gb_tickets_list.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_tickets_list.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gb_tickets_list.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_tickets_list.HeaderHeight = 35;
      this.gb_tickets_list.HeaderSubText = null;
      this.gb_tickets_list.HeaderText = null;
      this.gb_tickets_list.Location = new System.Drawing.Point(15, 204);
      this.gb_tickets_list.Name = "gb_tickets_list";
      this.gb_tickets_list.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_tickets_list.Size = new System.Drawing.Size(697, 334);
      this.gb_tickets_list.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.gb_tickets_list.TabIndex = 2;
      this.gb_tickets_list.Text = "xTicketsList";
      // 
      // dgv_tickets_list
      // 
      this.dgv_tickets_list.AllowUserToAddRows = false;
      this.dgv_tickets_list.AllowUserToDeleteRows = false;
      this.dgv_tickets_list.AllowUserToResizeColumns = false;
      this.dgv_tickets_list.AllowUserToResizeRows = false;
      this.dgv_tickets_list.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.dgv_tickets_list.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.dgv_tickets_list.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.dgv_tickets_list.ColumnHeaderHeight = 35;
      this.dgv_tickets_list.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
      dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      dataGridViewCellStyle1.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
      dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
      dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
      this.dgv_tickets_list.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
      this.dgv_tickets_list.ColumnHeadersHeight = 35;
      this.dgv_tickets_list.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
      this.dgv_tickets_list.CornerRadius = 10;
      this.dgv_tickets_list.DefaultCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.dgv_tickets_list.DefaultCellForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.dgv_tickets_list.DefaultCellSelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.dgv_tickets_list.DefaultCellSelectionForeColor = System.Drawing.Color.White;
      dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      dataGridViewCellStyle2.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.White;
      dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
      this.dgv_tickets_list.DefaultCellStyle = dataGridViewCellStyle2;
      this.dgv_tickets_list.EnableHeadersVisualStyles = false;
      this.dgv_tickets_list.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.dgv_tickets_list.GridColor = System.Drawing.Color.LightGray;
      this.dgv_tickets_list.HeaderBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
      this.dgv_tickets_list.HeaderDefaultCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.dgv_tickets_list.HeaderDefaultCellForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.dgv_tickets_list.HeaderImages = null;
      this.dgv_tickets_list.Location = new System.Drawing.Point(-1, 35);
      this.dgv_tickets_list.MultiSelect = false;
      this.dgv_tickets_list.Name = "dgv_tickets_list";
      this.dgv_tickets_list.ReadOnly = true;
      this.dgv_tickets_list.RowHeadersVisible = false;
      this.dgv_tickets_list.RowHeadersWidth = 20;
      this.dgv_tickets_list.RowTemplate.Height = 35;
      this.dgv_tickets_list.RowTemplateHeight = 35;
      this.dgv_tickets_list.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
      this.dgv_tickets_list.Size = new System.Drawing.Size(699, 300);
      this.dgv_tickets_list.Style = WSI.Cashier.Controls.uc_DataGridView.DataGridViewStyle.WITH_BOTTOM_ROUND_BORDERS;
      this.dgv_tickets_list.TabIndex = 0;
      this.dgv_tickets_list.SelectionChanged += new System.EventHandler(this.dgv_tickets_list_SelectionChanged);
      // 
      // lbl_tickets_amount
      // 
      this.lbl_tickets_amount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_tickets_amount.BackColor = System.Drawing.Color.Transparent;
      this.lbl_tickets_amount.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_tickets_amount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_tickets_amount.Location = new System.Drawing.Point(547, 583);
      this.lbl_tickets_amount.Name = "lbl_tickets_amount";
      this.lbl_tickets_amount.Size = new System.Drawing.Size(160, 30);
      this.lbl_tickets_amount.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_tickets_amount.TabIndex = 9;
      this.lbl_tickets_amount.Text = "1234567.89 �";
      this.lbl_tickets_amount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_tickets_total
      // 
      this.lbl_tickets_total.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_tickets_total.AutoSize = true;
      this.lbl_tickets_total.BackColor = System.Drawing.Color.Transparent;
      this.lbl_tickets_total.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_tickets_total.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_tickets_total.Location = new System.Drawing.Point(414, 589);
      this.lbl_tickets_total.Name = "lbl_tickets_total";
      this.lbl_tickets_total.Size = new System.Drawing.Size(109, 19);
      this.lbl_tickets_total.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_tickets_total.TabIndex = 8;
      this.lbl_tickets_total.Text = "xTotalAmount";
      this.lbl_tickets_total.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // uc_ticket_reader
      // 
      this.uc_ticket_reader.Location = new System.Drawing.Point(140, 0);
      this.uc_ticket_reader.MaximumSize = new System.Drawing.Size(800, 100);
      this.uc_ticket_reader.MinimumSize = new System.Drawing.Size(800, 60);
      this.uc_ticket_reader.Name = "uc_ticket_reader";
      this.uc_ticket_reader.Size = new System.Drawing.Size(800, 60);
      this.uc_ticket_reader.TabIndex = 0;
      // 
      // btn_ticket_redeem_total
      // 
      this.btn_ticket_redeem_total.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_ticket_redeem_total.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_ticket_redeem_total.FlatAppearance.BorderSize = 0;
      this.btn_ticket_redeem_total.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_ticket_redeem_total.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_ticket_redeem_total.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_ticket_redeem_total.Image = null;
      this.btn_ticket_redeem_total.IsSelected = false;
      this.btn_ticket_redeem_total.Location = new System.Drawing.Point(721, 553);
      this.btn_ticket_redeem_total.Name = "btn_ticket_redeem_total";
      this.btn_ticket_redeem_total.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_ticket_redeem_total.Size = new System.Drawing.Size(155, 60);
      this.btn_ticket_redeem_total.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_ticket_redeem_total.TabIndex = 10;
      this.btn_ticket_redeem_total.Text = "XTOTAL REDEEM";
      this.btn_ticket_redeem_total.UseVisualStyleBackColor = false;
      this.btn_ticket_redeem_total.Click += new System.EventHandler(this.btn_ticket_redeem_total_Click);
      // 
      // btn_chips_swap
      // 
      this.btn_chips_swap.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_chips_swap.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(47)))), ((int)(((byte)(50)))));
      this.btn_chips_swap.FlatAppearance.BorderSize = 0;
      this.btn_chips_swap.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_chips_swap.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_chips_swap.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_chips_swap.Image = null;
      this.btn_chips_swap.IsSelected = false;
      this.btn_chips_swap.Location = new System.Drawing.Point(721, 341);
      this.btn_chips_swap.Name = "btn_chips_swap";
      this.btn_chips_swap.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_chips_swap.Size = new System.Drawing.Size(155, 60);
      this.btn_chips_swap.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.TERTIARY;
      this.btn_chips_swap.TabIndex = 125;
      this.btn_chips_swap.Text = "XCHIPSEXCHANGE";
      this.btn_chips_swap.UseVisualStyleBackColor = false;
      this.btn_chips_swap.Click += new System.EventHandler(this.btn_chips_swap_Click);
      // 
      // btn_disputes
      // 
      this.btn_disputes.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_disputes.FlatAppearance.BorderSize = 0;
      this.btn_disputes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_disputes.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_disputes.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_disputes.Image = null;
      this.btn_disputes.IsSelected = false;
      this.btn_disputes.Location = new System.Drawing.Point(174, 553);
      this.btn_disputes.Name = "btn_disputes";
      this.btn_disputes.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_disputes.Size = new System.Drawing.Size(155, 60);
      this.btn_disputes.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_disputes.TabIndex = 125;
      this.btn_disputes.Text = "XDISPUTES";
      this.btn_disputes.UseVisualStyleBackColor = false;
      this.btn_disputes.Click += new System.EventHandler(this.btn_disputes_Click);
      // 
      // gbAccount
      // 
      this.gbAccount.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.gbAccount.BackColor = System.Drawing.Color.Transparent;
      this.gbAccount.BorderColor = System.Drawing.Color.Empty;
      this.gbAccount.Controls.Add(this.pb_user);
      this.gbAccount.Controls.Add(this.lbl_account_id_value);
      this.gbAccount.Controls.Add(this.lbl_holder_name);
      this.gbAccount.Controls.Add(this.lbl_account_id_name);
      this.gbAccount.Controls.Add(this.lbl_track_number);
      this.gbAccount.Controls.Add(this.lbl_trackdata);
      this.gbAccount.Controls.Add(this.lbl_name);
      this.gbAccount.CornerRadius = 10;
      this.gbAccount.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gbAccount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gbAccount.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gbAccount.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gbAccount.HeaderHeight = 35;
      this.gbAccount.HeaderSubText = null;
      this.gbAccount.HeaderText = null;
      this.gbAccount.Location = new System.Drawing.Point(18, 75);
      this.gbAccount.Name = "gbAccount";
      this.gbAccount.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gbAccount.Size = new System.Drawing.Size(858, 113);
      this.gbAccount.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.gbAccount.TabIndex = 126;
      // 
      // pb_user
      // 
      this.pb_user.Image = ((System.Drawing.Image)(resources.GetObject("pb_user.Image")));
      this.pb_user.InitialImage = ((System.Drawing.Image)(resources.GetObject("pb_user.InitialImage")));
      this.pb_user.Location = new System.Drawing.Point(17, 47);
      this.pb_user.Name = "pb_user";
      this.pb_user.Size = new System.Drawing.Size(48, 46);
      this.pb_user.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
      this.pb_user.TabIndex = 72;
      this.pb_user.TabStop = false;
      // 
      // lbl_account_id_value
      // 
      this.lbl_account_id_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_account_id_value.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_account_id_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_account_id_value.Location = new System.Drawing.Point(161, 44);
      this.lbl_account_id_value.Name = "lbl_account_id_value";
      this.lbl_account_id_value.Size = new System.Drawing.Size(95, 21);
      this.lbl_account_id_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_account_id_value.TabIndex = 1;
      this.lbl_account_id_value.Text = "012345";
      this.lbl_account_id_value.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_holder_name
      // 
      this.lbl_holder_name.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_holder_name.BackColor = System.Drawing.Color.Transparent;
      this.lbl_holder_name.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_holder_name.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_holder_name.Location = new System.Drawing.Point(333, 65);
      this.lbl_holder_name.Name = "lbl_holder_name";
      this.lbl_holder_name.Size = new System.Drawing.Size(522, 23);
      this.lbl_holder_name.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_holder_name.TabIndex = 6;
      this.lbl_holder_name.Text = "TTTTTTTTTTTTT";
      this.lbl_holder_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.lbl_holder_name.UseMnemonic = false;
      // 
      // lbl_account_id_name
      // 
      this.lbl_account_id_name.BackColor = System.Drawing.Color.Transparent;
      this.lbl_account_id_name.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_account_id_name.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_account_id_name.Location = new System.Drawing.Point(66, 44);
      this.lbl_account_id_name.Name = "lbl_account_id_name";
      this.lbl_account_id_name.Size = new System.Drawing.Size(87, 21);
      this.lbl_account_id_name.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_account_id_name.TabIndex = 0;
      this.lbl_account_id_name.Text = "xAccount";
      this.lbl_account_id_name.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_track_number
      // 
      this.lbl_track_number.BackColor = System.Drawing.Color.Transparent;
      this.lbl_track_number.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_track_number.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_track_number.Location = new System.Drawing.Point(140, 71);
      this.lbl_track_number.Name = "lbl_track_number";
      this.lbl_track_number.Size = new System.Drawing.Size(239, 20);
      this.lbl_track_number.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_track_number.TabIndex = 4;
      this.lbl_track_number.Text = "88888888888888888812";
      this.lbl_track_number.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_trackdata
      // 
      this.lbl_trackdata.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_trackdata.BackColor = System.Drawing.Color.Transparent;
      this.lbl_trackdata.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_trackdata.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_trackdata.Location = new System.Drawing.Point(71, 70);
      this.lbl_trackdata.Name = "lbl_trackdata";
      this.lbl_trackdata.Size = new System.Drawing.Size(505, 23);
      this.lbl_trackdata.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_trackdata.TabIndex = 3;
      this.lbl_trackdata.Text = "Tarjeta:";
      this.lbl_trackdata.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_name
      // 
      this.lbl_name.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_name.BackColor = System.Drawing.Color.Transparent;
      this.lbl_name.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_name.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_name.Location = new System.Drawing.Point(333, 42);
      this.lbl_name.Name = "lbl_name";
      this.lbl_name.Size = new System.Drawing.Size(522, 23);
      this.lbl_name.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_name.TabIndex = 5;
      this.lbl_name.Text = "xName:";
      this.lbl_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // timer1
      // 
      this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
      // 
      // uc_ticket_view
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.gbAccount);
      this.Controls.Add(this.btn_disputes);
      this.Controls.Add(this.btn_chips_swap);
      this.Controls.Add(this.pb_uc_icon);
      this.Controls.Add(this.lbl_separator_1);
      this.Controls.Add(this.lbl_tickets_num_value);
      this.Controls.Add(this.lbl_tickets_num);
      this.Controls.Add(this.lbl_handpays_warning);
      this.Controls.Add(this.btn_handpays);
      this.Controls.Add(this.btn_delete_ticket);
      this.Controls.Add(this.btn_clean_list);
      this.Controls.Add(this.gb_tickets_list);
      this.Controls.Add(this.lbl_tickets_amount);
      this.Controls.Add(this.lbl_tickets_total);
      this.Controls.Add(this.uc_ticket_reader);
      this.Controls.Add(this.btn_ticket_redeem_total);
      this.Name = "uc_ticket_view";
      this.Size = new System.Drawing.Size(884, 656);
      ((System.ComponentModel.ISupportInitialize)(this.pb_uc_icon)).EndInit();
      this.gb_tickets_list.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.dgv_tickets_list)).EndInit();
      this.gbAccount.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.pb_user)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private WSI.Cashier.TITO.uc_ticket_reader uc_ticket_reader;
    private WSI.Cashier.Controls.uc_round_button btn_ticket_redeem_total;
    private WSI.Cashier.Controls.uc_label lbl_tickets_total;
    private WSI.Cashier.Controls.uc_label lbl_tickets_amount;
    private WSI.Cashier.Controls.uc_round_button btn_clean_list;
    private WSI.Cashier.Controls.uc_round_panel gb_tickets_list;
    private WSI.Cashier.Controls.uc_round_button btn_delete_ticket;
    private WSI.Cashier.Controls.uc_round_button btn_handpays;
    private WSI.Cashier.Controls.uc_label lbl_handpays_warning;
    private WSI.Cashier.Controls.uc_label lbl_tickets_num;
    private WSI.Cashier.Controls.uc_label lbl_tickets_num_value;
    private WSI.Cashier.Controls.uc_DataGridView dgv_tickets_list;
    private System.Windows.Forms.Label lbl_separator_1;
    private System.Windows.Forms.PictureBox pb_uc_icon;
    private Controls.uc_round_button btn_disputes;
    private Controls.uc_round_button btn_chips_swap;
    private Controls.uc_round_panel gbAccount;
    private System.Windows.Forms.PictureBox pb_user;
    private Controls.uc_label lbl_account_id_value;
    private Controls.uc_label lbl_holder_name;
    private Controls.uc_label lbl_account_id_name;
    private Controls.uc_label lbl_track_number;
    private Controls.uc_label lbl_trackdata;
    private Controls.uc_label lbl_name;
    private System.Windows.Forms.Timer timer1;
  }
}
