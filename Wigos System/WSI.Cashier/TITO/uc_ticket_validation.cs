//------------------------------------------------------------------------------
// Copyright © 2007-2013 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_tickets.cs
// 
//   DESCRIPTION: Implements the uc_tickets user control
//
//        AUTHOR: Luis Mesa
//
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 26-NOV-2013 LEM    First release.
// 13-DEC-2013 ICS    Added ticket type label.
// 18-DEC-2013 ICS    Added a timer to reset labels.
// 31-DEC-2013 JRM    Removed all references to method UserHasPermission and changed it for ProfilePermissions.CheckPermissions
// 14-JAN-2014 LEM    Fixed Bug WIGOSTITO-965: Time-out is too short
// 28-MAR-2014 ICS    Fixed Bug WIGOSTITO-1177: Incongruence of data in the GUI for the expiration of tickets
// 11-JUL-2014 AMF    Regionalization
// 10-NOV-2015 ETP    Product Backlog Item 5802 - New cashier design - uc_ticket_validation
// 12-NOV-2015 ETP    Product Backlog Item 5802 - New cashier design - uc_ticket_validation lbl separator color.
// 22-FEB-2016 RAB    Product Backlog Item 9871:TITO: Validación de tickets PENDING_CANCEL
// 26-OCT-2016 XGJ    Product Backlog Item 19580:TITO: Pantalla de Log-Seguimiento de Tickets (Cambios Review)
// 18-NOV-2016 LTC    Bug 17133: TITO Ticket validation is allowed with out the corresponding GP activated
// 04-APR-2017 RAB    PBI 26537: MES10 Ticket validation - Prevent the use of dealer copy tickets (Cashier)
// 04-MAY-2017 RAB    PBI 27173: MES10 Ticket validation - Ticket not found messages
//------------ ------ ----------------------------------------------------------

using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.ComponentModel;
using System.Windows.Forms;
using WSI.Common;
using WSI.Common.TITO;
using System.Threading;
using System.Collections.Generic;
using WSI.Cashier.TITO;
using System.Drawing;
using System.Collections;
using WSI.Cashier.Controls;

namespace WSI.Cashier
{
  public partial class uc_ticket_validation : UserControl
  {
    Boolean m_first_time = true;
    frm_yesno m_frm_shadow_gui;
    frm_container m_parent;
    Ticket m_current_ticket;
    CardData m_virtual_account;

    #region Constructor

    public uc_ticket_validation()
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[UC LOAD] uc_ticket_validation", Log.Type.Message);

      InitializeComponent();

      m_frm_shadow_gui = new frm_yesno();
      m_frm_shadow_gui.Opacity = 0.6;

      EventLastAction.AddEventLastAction(this.Controls);
      this.lbl_separator.BackColor = WSI.Cashier.CashierStyle.Colors.m_colors_dic[CashierStyle.Colors.ENUM_COLORS.HEX_959595];
    }

    public void InitFocus()
    {
      uc_ticket_reader.Focus();
    }

    #endregion

    #region Constants

    private const String EMPTY = "---";

    #endregion

    #region Private Methods
    /// <summary>
    /// Initialize controls
    /// </summary>
    public void InitControls(frm_container Parent)
    {
      m_parent = Parent;
      m_current_ticket = null;

      if (m_virtual_account == null || m_virtual_account.AccountId == 0)
      {
        m_virtual_account = new CardData();
        BusinessLogic.LoadVirtualAccount(m_virtual_account);
      }

      if (m_first_time)
      {
        uc_ticket_reader.OnValidationNumberReadEvent += new uc_ticket_reader.ValidationNumberReadEventHandler(ProcessTicketNumber);

        m_first_time = false;

        InitializeControlResources();
      }

      InitForm();
      
      timer1.Interval = CashierBusinessLogic.DURATION_BUTTONS_ENABLED_AFTER_OPERATION * 500;
    }

    /// <summary>
    /// Initialize control resources. (NLS string, images, etc.)
    /// OJO: some resources are used more than once
    /// </summary>
    void InitializeControlResources()
    {
      lbl_title.Text = Resource.String("STR_UC_TICKET_VALIDATION_TITLE");
      btn_validate_ticket.Text = Resource.String("STR_UC_TICKETS_03");

      gb_ticket_info.HeaderText = Resource.String("STR_TITO_TICKET");
      lbl_ticket_number.Text = Resource.String("STR_UC_TICKETS_04") + ":";
      lbl_ticket_status.Text = Resource.String("STR_UC_TICKETS_05") + ":";
      lbl_ticket_amount.Text = Resource.String("STR_UC_TICKETS_06") + ":";

      gb_ticket_creation.HeaderText = Resource.String("STR_UC_TICKETS_07");
      lbl_c_terminal.Text = Resource.String("STR_UC_TICKETS_08") + ":";
      lbl_c_date.Text = Resource.String("STR_UC_TICKETS_09") + ":";
      lbl_c_account.Text = Resource.String("STR_UC_TICKETS_10") + ":";

      gb_ticket_lastaction.HeaderText = Resource.String("STR_UC_TICKETS_05");
      lbl_l_terminal.Text = Resource.String("STR_UC_TICKETS_08") + ":";
      lbl_l_date.Text = Resource.String("STR_UC_TICKETS_09") + ":";

      lbl_ticket_type.Text = Resource.String("STR_UC_TICKETS_TYPE") + ":";

      uc_ticket_reader.InitializeControlResources();
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Restore focus in th form
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    private void RestoreParentFocus()
    {
      this.ParentForm.Focus();
      this.ParentForm.BringToFront();
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Set status of ALL buttons in control
    // 
    //  PARAMS:
    //      - INPUT:
    //        - Ticket: reference to a ticket instance
    //
    //      - OUTPUT:
    //
    private void SetButtonStatus()
    {
      String _msg;

      btn_validate_ticket.Enabled = BusinessLogic.IsTITOCashierOpen &&
                                    Ticket.IsAllowToAvoid &&  // LTC 18-NOV-2016
                                    AllowCurrentTicketValidation(out _msg);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: clear all controls in the form
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    private void InitForm()
    {
      ClearTicketControls();
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Clear controls related to ticket
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    private void ClearTicketControls()
    {
      RefreshTicketControls(null);
      uc_ticket_reader.ClearTrackNumber();
      uc_ticket_reader.Focus();
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Refresh content of controls related to ticket
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    private void RefreshTicketControls(Ticket Ticket)
    {
      CardData _card_create;
      TITO_TICKET_STATUS _ticket_status;
      bool _allow_pay_tickets_pending_print;

      _allow_pay_tickets_pending_print = GeneralParam.GetBoolean("TITO", "PendingPrint.AllowToRedeem", true);

      timer1.Enabled = false;
      m_current_ticket = Ticket;

      if (Ticket != null)
      {
        pnl_ticket.Visible = true;
        pnl_creation.Visible = true;
        gb_ticket_info.Enabled = true;
        gb_ticket_creation.Enabled = true;
        _ticket_status = Ticket.Status;
        Header(Ticket);

        lbl_ticket_number_value.Text = ValidationNumberManager.FormatValidationNumber(Ticket.ValidationNumber, (Int32)Ticket.ValidationType, true);
        
        if (_allow_pay_tickets_pending_print) 
        { 
          if ((Ticket.Status == TITO_TICKET_STATUS.VALID || Ticket.Status == TITO_TICKET_STATUS.PENDING_PRINT) && WGDB.Now >= Ticket.ExpirationDateTime)
          {
            _ticket_status = TITO_TICKET_STATUS.EXPIRED;
          }
        }
        else
        {
        if (Ticket.Status == TITO_TICKET_STATUS.VALID && WGDB.Now >= Ticket.ExpirationDateTime)
        {
          _ticket_status = TITO_TICKET_STATUS.EXPIRED;
        }
        }

        lbl_ticket_status_value.Text = BusinessLogic.StatusName(_ticket_status);
        lbl_ticket_amount_value.Text = ((Currency)Ticket.Amount).ToString(true);
        lbl_c_terminal_value.Text = Ticket.CreatedTerminalName;
        lbl_c_date_value.Text = Format.CustomFormatDateTime(Ticket.CreatedDateTime, true);
        lbl_ticket_type_value.Text = BusinessLogic.TypeName(Ticket.TicketType);

        lbl_c_account_value.Text = "";
        _card_create = new CardData();
        if (CardData.DB_CardGetAllData(Ticket.CreatedAccountID, _card_create))
        {
          if (String.IsNullOrEmpty(_card_create.PlayerTracking.HolderName))
          {
            lbl_c_account_value.Text = EMPTY;
          }
          else
          {
            lbl_c_account_value.Text = _card_create.PlayerTracking.HolderName;
          }
        }

        if (Ticket.Status != TITO_TICKET_STATUS.VALID && Ticket.Status != TITO_TICKET_STATUS.PENDING_PRINT)
        {
          gb_ticket_lastaction.Visible = true;
          pnl_last.Visible = true;
          gb_ticket_lastaction.Enabled = true;

          gb_ticket_lastaction.HeaderText = lbl_ticket_status_value.Text;
          lbl_l_terminal_value.Text = Ticket.LastActionTerminalName;

          // ICS 13-DEC-2013: Show expiration date time when it's expired
          if (Ticket.Status == TITO_TICKET_STATUS.EXPIRED)
          {
            lbl_l_date_value.Text = Format.CustomFormatDateTime(Ticket.ExpirationDateTime, true);
          }
          else
          {
            lbl_l_date_value.Text = Format.CustomFormatDateTime(Ticket.LastActionDateTime, true);
          }
        }
        else
        {
          gb_ticket_lastaction.Visible = false;
          pnl_last.Visible = false;
          gb_ticket_lastaction.HeaderText = Resource.String("STR_UC_TICKETS_05");
        }
      }
      else
      {
        gb_ticket_lastaction.Visible = false;
        pnl_creation.Visible = false;
        pnl_last.Visible = false;
        pnl_ticket.Visible = false;
        gb_ticket_info.Enabled = false;
        gb_ticket_creation.Enabled = false;
        gb_ticket_lastaction.Enabled = false;
      }

      SetButtonStatus();

      // XGJ 26-OCT-2016
      //timer1.Enabled = true;
    }

    private void Header(Ticket ticket)
    {
      if (ticket.Status == TITO_TICKET_STATUS.PENDING_PRINT)
        gb_ticket_creation.HeaderText = Resource.String("STR_UC_TITO_TICKET_STATUS_PENDING_PRINT");
      else
        gb_ticket_creation.HeaderText = Resource.String("STR_UC_TICKETS_07");
    }

    private void ShowInvalidStatusMsg(Ticket Ticket)
    {
      String _msg;

      _msg = "";

      switch (Ticket.Status)
      {
        case TITO_TICKET_STATUS.CANCELED:
          _msg = Resource.String("STR_UC_TITO_TICKET_STATUS_CANCELED");
          break;
        case TITO_TICKET_STATUS.EXPIRED:
          _msg = Resource.String("STR_UC_TITO_TICKET_STATUS_EXPIRED");
          break;
        case TITO_TICKET_STATUS.REDEEMED:
          _msg = Resource.String("STR_UC_TITO_TICKET_STATUS_REDEEMED");
          break;
        default:
          break;
      }

      frm_message.Show(_msg, Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this.ParentForm);
    }

    private Boolean AllowCurrentTicketValidation(out String ErrorMsg)
    {
      // RAB 22-FEB-2016
      Int32 _allow_validation_time;
      DateTime _date_time_validation;

      _allow_validation_time = GeneralParam.GetInt32("TITO", "Tickets.MaxMinutesAllowedToValidate", 2880);
      ErrorMsg = "";

      if (m_current_ticket == null)
      {
        return false;
      }

      if (m_current_ticket.Status != TITO_TICKET_STATUS.PENDING_CANCEL)
      {
        ErrorMsg = Resource.String("STR_UC_TICKET_VALIDATION_STATE_ERROR");

        return false;
      }

      // RAB 22-FEB-2016
      _date_time_validation = m_current_ticket.LastActionDateTime;

      if (_date_time_validation == DateTime.MinValue)
      {
        _date_time_validation = m_current_ticket.CreatedDateTime;
      }

      if (_date_time_validation.AddMinutes(_allow_validation_time) < WGDB.Now)
      {
        ErrorMsg = Resource.String("STR_UC_TICKET_VALIDATION_TIME_ERROR");

        return false;
      }

      return true;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Process the entered text, when it is a ticket Validation Number
    // 
    //  PARAMS:
    //      - INPUT:
    //        - String: ValidationNumber
    //
    //      - OUTPUT:
    //
    private void ProcessTicketNumber(String ValidationNumber, ref Boolean IsValid, out String ErrorStr)
    {
      Ticket _ticket;
      Int64 _validation_number;
      TITO_VALIDATION_TYPE _validation_type;
      String _condition;
      String _selection_condition;

      IsValid = false;
      ErrorStr = "";
      _validation_number = 0;

      if (String.IsNullOrEmpty(ValidationNumber))
      {
        IsValid = true;

        return;
      }

      _validation_number = ValidationNumberManager.UnFormatValidationNumber(ValidationNumber, out _validation_type);
      _condition = " TI_VALIDATION_NUMBER = '" + _validation_number + "' ";

      _selection_condition = " TI_STATUS <> " + (Int32)Common.TITO_TICKET_STATUS.DISCARDED;
      _selection_condition += " OR TI_TYPE_ID = " + (Int32)Common.TITO_TICKET_TYPE.OFFLINE;

      if (_validation_number > 0)
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _ticket = BusinessLogic.SelectTicketByCondition(_condition, _selection_condition);
        }
        if (_ticket.ValidationNumber > 0)
        {
          if (_ticket.TicketType == TITO_TICKET_TYPE.CHIPS_DEALER_COPY)
          {
            ErrorStr = Resource.String("STR_UC_DEALER_COPY_DEALER_NOT_FOUND");
            ClearTicketControls();
            return;
          }

          RefreshTicketControls(null);
          RefreshTicketControls(_ticket);
          IsValid = true;
        }
        else
        {
          ClearTicketControls();
        }

        RestoreParentFocus();
        InitFocus();
      }
    }

    #endregion

    #region Events

    #endregion

    private void btn_validate_ticket_Click(object sender, EventArgs e)
    {
      Boolean _result_ok;
      String _msg;
      CashierSessionInfo CashierSessionInfo;
      ArrayList _voucher_list;

      _voucher_list = new ArrayList();

      _result_ok = true;

      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.TITO_ValidateTicket,
                                   ProfilePermissions.TypeOperation.RequestPasswd,
                                   this.ParentForm,
                                   out _msg))
      {
        return;
      }

      if (!AllowCurrentTicketValidation(out _msg))
      {
        if (!String.IsNullOrEmpty(_msg))
        {
          m_frm_shadow_gui.Show(this.ParentForm);
          frm_message.Show(_msg, Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, m_frm_shadow_gui);
          this.m_frm_shadow_gui.Hide();
          this.RestoreParentFocus();
        }

        return;
      }

      _result_ok = false;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (m_virtual_account == null || m_virtual_account.AccountId == 0)
          {
            m_virtual_account = new CardData();
            if (!BusinessLogic.LoadVirtualAccount(m_virtual_account))
            {
              Log.Error("Tickets: Undo Ticket. Error loading virtual account. Terminal: " + Cashier.TerminalName);

              return;
            }
          }

          CashierSessionInfo = Cashier.CashierSessionInfo();

          if (!Ticket.Cashier_ValidateCanceledTicket(m_current_ticket, (Int32)Cashier.TerminalId, m_virtual_account.AccountId, CashierSessionInfo, out _voucher_list, _db_trx.SqlTransaction))
          {
            Log.Error("Tickets: Validate Ticket. Error in ticket validation. TicketId: " + m_current_ticket.TicketID);

            return;
          }

          _db_trx.Commit();
          _result_ok = true;
        }

        if (_voucher_list.Count > 0)
        {
          VoucherPrint.Print(_voucher_list);
        }

        ClearTicketControls();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {
        if (!_result_ok)
        {
          m_frm_shadow_gui.Show(this.ParentForm);
          frm_message.Show(Resource.String("STR_UC_TICKETS_11"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, m_frm_shadow_gui);
          this.m_frm_shadow_gui.Hide();
          this.RestoreParentFocus();
        }
      }
    }

    private void timer1_Tick(object sender, EventArgs e)
    {
      ClearTicketControls();
      timer1.Enabled = false;
    }

  }
}
