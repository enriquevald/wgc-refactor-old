namespace WSI.Cashier
{
  partial class frm_transfer_to_terminal
  {

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.btn_cancel = new WSI.Cashier.Controls.uc_round_button();
      this.txt_amount = new WSI.Cashier.Controls.uc_round_textbox();
      this.lbl_amount_to_pay = new WSI.Cashier.Controls.uc_label();
      this.lbl_to_pay = new WSI.Cashier.Controls.uc_label();
      this.uc_terminal_filter = new WSI.Cashier.uc_terminal_filter();
      this.btn_transfer = new WSI.Cashier.Controls.uc_round_button();
      this.lbl_amount = new WSI.Cashier.Controls.uc_label();
      this.gb_terminal = new WSI.Cashier.Controls.uc_round_panel();
      this.lbl_separator_1 = new System.Windows.Forms.Label();
      this.pnl_data.SuspendLayout();
      this.gb_terminal.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.Controls.Add(this.lbl_amount);
      this.pnl_data.Controls.Add(this.lbl_to_pay);
      this.pnl_data.Controls.Add(this.gb_terminal);
      this.pnl_data.Controls.Add(this.lbl_separator_1);
      this.pnl_data.Controls.Add(this.btn_cancel);
      this.pnl_data.Controls.Add(this.txt_amount);
      this.pnl_data.Controls.Add(this.lbl_amount_to_pay);
      this.pnl_data.Controls.Add(this.btn_transfer);
      this.pnl_data.Size = new System.Drawing.Size(433, 605);
      this.pnl_data.TabIndex = 0;
      // 
      // btn_cancel
      // 
      this.btn_cancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.btn_cancel.FlatAppearance.BorderSize = 0;
      this.btn_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_cancel.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_cancel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.btn_cancel.Image = null;
      this.btn_cancel.Location = new System.Drawing.Point(264, 532);
      this.btn_cancel.Name = "btn_cancel";
      this.btn_cancel.Size = new System.Drawing.Size(155, 60);
      this.btn_cancel.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_cancel.TabIndex = 7;
      this.btn_cancel.Text = "XCANCEL";
      this.btn_cancel.UseVisualStyleBackColor = false;
      this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
      // 
      // txt_amount
      // 
      this.txt_amount.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_amount.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_amount.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.txt_amount.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.txt_amount.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_amount.CornerRadius = 5;
      this.txt_amount.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_amount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.txt_amount.Location = new System.Drawing.Point(136, 417);
      this.txt_amount.MaxLength = 18;
      this.txt_amount.Multiline = false;
      this.txt_amount.Name = "txt_amount";
      this.txt_amount.PasswordChar = '\0';
      this.txt_amount.ReadOnly = false;
      this.txt_amount.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_amount.SelectedText = "";
      this.txt_amount.SelectionLength = 0;
      this.txt_amount.SelectionStart = 0;
      this.txt_amount.Size = new System.Drawing.Size(287, 40);
      this.txt_amount.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_amount.TabIndex = 2;
      this.txt_amount.TabStop = false;
      this.txt_amount.Text = "$99,000,000.00";
      this.txt_amount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      this.txt_amount.UseSystemPasswordChar = false;
      this.txt_amount.WaterMark = null;
      this.txt_amount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_amount_KeyPress);
      this.txt_amount.Enter += new System.EventHandler(this.txt_amount_Enter);
      this.txt_amount.Leave += new System.EventHandler(this.txt_amount_Leave);
      this.txt_amount.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txt_amount_MouseClick);
      this.txt_amount.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.txt_amount_PreviewKeyDown);
      // 
      // lbl_amount_to_pay
      // 
      this.lbl_amount_to_pay.BackColor = System.Drawing.Color.Transparent;
      this.lbl_amount_to_pay.Font = new System.Drawing.Font("Montserrat", 32F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_amount_to_pay.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.lbl_amount_to_pay.Location = new System.Drawing.Point(136, 475);
      this.lbl_amount_to_pay.Name = "lbl_amount_to_pay";
      this.lbl_amount_to_pay.Size = new System.Drawing.Size(279, 37);
      this.lbl_amount_to_pay.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_32PX_BLACK_BOLD;
      this.lbl_amount_to_pay.TabIndex = 5;
      this.lbl_amount_to_pay.Text = "$1,074,567.00";
      this.lbl_amount_to_pay.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_to_pay
      // 
      this.lbl_to_pay.AutoSize = true;
      this.lbl_to_pay.BackColor = System.Drawing.Color.Transparent;
      this.lbl_to_pay.Font = new System.Drawing.Font("Montserrat", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_to_pay.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.lbl_to_pay.Location = new System.Drawing.Point(15, 482);
      this.lbl_to_pay.Name = "lbl_to_pay";
      this.lbl_to_pay.Size = new System.Drawing.Size(56, 19);
      this.lbl_to_pay.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_to_pay.TabIndex = 4;
      this.lbl_to_pay.Text = "xTotal";
      // 
      // uc_terminal_filter
      // 
      this.uc_terminal_filter.BackColor = System.Drawing.Color.Transparent;
      this.uc_terminal_filter.Cursor = System.Windows.Forms.Cursors.Default;
      this.uc_terminal_filter.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(2)));
      this.uc_terminal_filter.Location = new System.Drawing.Point(0, 38);
      this.uc_terminal_filter.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
      this.uc_terminal_filter.Name = "uc_terminal_filter";
      this.uc_terminal_filter.Size = new System.Drawing.Size(414, 362);
      this.uc_terminal_filter.TabIndex = 0;
      // 
      // btn_transfer
      // 
      this.btn_transfer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.btn_transfer.FlatAppearance.BorderSize = 0;
      this.btn_transfer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_transfer.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_transfer.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.btn_transfer.Image = null;
      this.btn_transfer.Location = new System.Drawing.Point(97, 532);
      this.btn_transfer.Name = "btn_transfer";
      this.btn_transfer.Size = new System.Drawing.Size(155, 60);
      this.btn_transfer.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_transfer.TabIndex = 6;
      this.btn_transfer.Text = "XTRANSFER";
      this.btn_transfer.UseVisualStyleBackColor = false;
      this.btn_transfer.Click += new System.EventHandler(this.btn_transfer_Click);
      // 
      // lbl_amount
      // 
      this.lbl_amount.AutoSize = true;
      this.lbl_amount.BackColor = System.Drawing.Color.Transparent;
      this.lbl_amount.Font = new System.Drawing.Font("Montserrat", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_amount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.lbl_amount.Location = new System.Drawing.Point(15, 429);
      this.lbl_amount.Name = "lbl_amount";
      this.lbl_amount.Size = new System.Drawing.Size(83, 19);
      this.lbl_amount.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_amount.TabIndex = 1;
      this.lbl_amount.Text = "xAmount";
      // 
      // gb_terminal
      // 
      this.gb_terminal.BackColor = System.Drawing.Color.Transparent;
      this.gb_terminal.Controls.Add(this.uc_terminal_filter);
      this.gb_terminal.CornerRadius = 10;
      this.gb_terminal.Cursor = System.Windows.Forms.Cursors.Default;
      this.gb_terminal.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_terminal.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.gb_terminal.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.gb_terminal.HeaderHeight = 35;
      this.gb_terminal.HeaderSubText = null;
      this.gb_terminal.HeaderText = null;
      this.gb_terminal.Location = new System.Drawing.Point(9, 10);
      this.gb_terminal.Margin = new System.Windows.Forms.Padding(0);
      this.gb_terminal.Name = "gb_terminal";
      this.gb_terminal.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.gb_terminal.Size = new System.Drawing.Size(414, 402);
      this.gb_terminal.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.gb_terminal.TabIndex = 0;
      this.gb_terminal.Text = "Terminal";
      // 
      // lbl_separator_1
      // 
      this.lbl_separator_1.BackColor = System.Drawing.Color.Black;
      this.lbl_separator_1.Location = new System.Drawing.Point(9, 468);
      this.lbl_separator_1.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
      this.lbl_separator_1.Name = "lbl_separator_1";
      this.lbl_separator_1.Size = new System.Drawing.Size(411, 1);
      this.lbl_separator_1.TabIndex = 3;
      // 
      // frm_transfer_to_terminal
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.AutoSize = true;
      this.ClientSize = new System.Drawing.Size(433, 660);
      this.HeaderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.Name = "frm_transfer_to_terminal";
      this.ShowCloseButton = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Style = WSI.Cashier.Controls.frm_base.BaseFormStyle.PRINCIPAL;
      this.Text = "Hand Pays";
      this.Activated += new System.EventHandler(this.frm_handpay_action_Activated);
      this.pnl_data.ResumeLayout(false);
      this.pnl_data.PerformLayout();
      this.gb_terminal.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private WSI.Cashier.Controls.uc_round_panel gb_terminal;
    private WSI.Cashier.Controls.uc_label lbl_amount_to_pay;
    private WSI.Cashier.Controls.uc_label lbl_to_pay;
    private WSI.Cashier.Controls.uc_round_button btn_transfer;
    private WSI.Cashier.Controls.uc_round_textbox txt_amount;
    private WSI.Cashier.Controls.uc_label lbl_amount;
    private WSI.Cashier.Controls.uc_round_button btn_cancel;
    private uc_terminal_filter uc_terminal_filter;
    private System.Windows.Forms.Label lbl_separator_1;

    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

     #region Windows Form Designer generated code


    #endregion
  }
}