namespace WSI.Cashier.TITO
{
  partial class frm_transfer_to_terminal_status
  {

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.icon_box = new System.Windows.Forms.PictureBox();
      this.lbl_status = new WSI.Cashier.Controls.uc_label();
      this.bar_status = new System.Windows.Forms.ProgressBar();
      this.btn_cancel = new WSI.Cashier.Controls.uc_round_button();
      this.btn_retry = new WSI.Cashier.Controls.uc_round_button();
      this.pnl_data.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.icon_box)).BeginInit();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.Controls.Add(this.icon_box);
      this.pnl_data.Controls.Add(this.lbl_status);
      this.pnl_data.Controls.Add(this.bar_status);
      this.pnl_data.Controls.Add(this.btn_cancel);
      this.pnl_data.Controls.Add(this.btn_retry);
      this.pnl_data.Size = new System.Drawing.Size(486, 184);
      // 
      // icon_box
      // 
      this.icon_box.Location = new System.Drawing.Point(15, 13);
      this.icon_box.Name = "icon_box";
      this.icon_box.Size = new System.Drawing.Size(48, 48);
      this.icon_box.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
      this.icon_box.TabIndex = 115;
      this.icon_box.TabStop = false;
      // 
      // lbl_status
      // 
      this.lbl_status.BackColor = System.Drawing.Color.Transparent;
      this.lbl_status.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_status.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.lbl_status.Location = new System.Drawing.Point(74, 15);
      this.lbl_status.Name = "lbl_status";
      this.lbl_status.Size = new System.Drawing.Size(395, 46);
      this.lbl_status.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_status.TabIndex = 114;
      this.lbl_status.Text = "xTransfer Status";
      this.lbl_status.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // bar_status
      // 
      this.bar_status.Location = new System.Drawing.Point(15, 71);
      this.bar_status.Name = "bar_status";
      this.bar_status.Size = new System.Drawing.Size(456, 18);
      this.bar_status.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
      this.bar_status.TabIndex = 113;
      this.bar_status.Value = 50;
      // 
      // btn_cancel
      // 
      this.btn_cancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.btn_cancel.FlatAppearance.BorderSize = 0;
      this.btn_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_cancel.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_cancel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.btn_cancel.Image = null;
      this.btn_cancel.Location = new System.Drawing.Point(316, 107);
      this.btn_cancel.Name = "btn_cancel";
      this.btn_cancel.Size = new System.Drawing.Size(155, 60);
      this.btn_cancel.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_cancel.TabIndex = 112;
      this.btn_cancel.Text = "XCANCEL";
      this.btn_cancel.UseVisualStyleBackColor = false;
      this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
      // 
      // btn_retry
      // 
      this.btn_retry.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.btn_retry.FlatAppearance.BorderSize = 0;
      this.btn_retry.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_retry.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_retry.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.btn_retry.Image = null;
      this.btn_retry.Location = new System.Drawing.Point(15, 107);
      this.btn_retry.Name = "btn_retry";
      this.btn_retry.Size = new System.Drawing.Size(155, 60);
      this.btn_retry.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_retry.TabIndex = 11;
      this.btn_retry.Text = "XRETRY";
      this.btn_retry.UseVisualStyleBackColor = false;
      this.btn_retry.Click += new System.EventHandler(this.btn_retry_Click);
      // 
      // frm_transfer_to_terminal_status
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.AutoSize = true;
      this.ClientSize = new System.Drawing.Size(486, 239);
      this.HeaderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.Name = "frm_transfer_to_terminal_status";
      this.ShowCloseButton = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Style = WSI.Cashier.Controls.frm_base.BaseFormStyle.PRINCIPAL;
      this.Text = "Hand Pays";
      this.Shown += new System.EventHandler(this.frm_transfer_to_terminal_status_Shown);
      this.pnl_data.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.icon_box)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    //private System.Windows.Forms.SplitContainer splitContainer1;
    private WSI.Cashier.Controls.uc_round_button btn_retry;


    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;
    private WSI.Cashier.Controls.uc_round_button btn_cancel;
    private System.Windows.Forms.ProgressBar bar_status;
    private WSI.Cashier.Controls.uc_label lbl_status;
    public System.Windows.Forms.PictureBox icon_box;


    #region Windows Form Designer generated code


    #endregion
  }
}