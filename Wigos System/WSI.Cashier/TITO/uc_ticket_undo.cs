//------------------------------------------------------------------------------
// Copyright � 2007-2013 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_ticket_undo.cs
// 
//   DESCRIPTION: Implements the uc_tickets user control
//
//        AUTHOR: Luis Mesa
//
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 26-NOV-2013 LEM    First release.
// 31-DEC-2013 JRM    Removed all references to method UserHasPermission and changed it for ProfilePermissions.CheckPermissions
// 31-JAN-2013 JPJ    Added new STATUS and type column.
// 07-MAR-2014 DHA    Fixed Bug WIGOSTITO-1129: Hide Operations that have no valid tickets
// 16-APR-2014 ICS, MPO & RCI    Fixed Bug WIG-830: Not enough available storage exception.
// 10-DEC-2014 DRV    Decimal currency formatting: DUT 201411 - Tito Chile - Currency formatting without decimals
// 11-NOV-2015 FOS    Product Backlog Item: 3709 Payment tickets & handpays
//------------------------------------------------------------------------------

using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.ComponentModel;
using System.Windows.Forms;
using WSI.Common;
using WSI.Common.TITO;
using System.Threading;
using System.Collections.Generic;
using WSI.Cashier.TITO;
using System.Drawing;
using System.Collections;
using System.Globalization;

namespace WSI.Cashier
{
  public partial class uc_ticket_undo : UserControl
  {
    Boolean m_first_time = true;
    frm_yesno m_frm_shadow_gui;
    frm_container m_parent;
    DataTable m_tickets_detail;

    #region Constructor

    public uc_ticket_undo()
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[UC LOAD] uc_ticket_undo", Log.Type.Message);

      InitializeComponent();

      m_frm_shadow_gui = new frm_yesno();
      m_frm_shadow_gui.Opacity = 0.6;

      EventLastAction.AddEventLastAction(this.Controls);
    }

    public void InitFocus()
    {
      //do nothing
    }

    #endregion

    #region Constants

    private const Int32 GRID_COLUMN_OPERATION_ID = 0;
    private const Int32 GRID_COLUMN_OPERATION_CODE = 1;
    private const Int32 GRID_COLUMN_OPERATION_DESC = 2;
    private const Int32 GRID_COLUMN_TICKET_CUANTITY = 3;
    private const Int32 GRID_COLUMN_OPERATION_DATE = 4;
    private const Int32 GRID_COLUMN_OPERATION_AMOUNT = 5;
    private const Int32 GRID_COLUMN_OPERATION_ACCOUNT = 6;

    private const Int32 WIDTH_COLUMN_OPERATION_ID = 0;
    private const Int32 WIDTH_COLUMN_OPERATION_CODE = 0;
    private const Int32 WIDTH_COLUMN_OPERATION_DESC = 150;
    private const Int32 WIDTH_COLUMN_TICKET_CUANTITY = 0;
    private const Int32 WIDTH_COLUMN_OPERATION_DATE = 150;
    private const Int32 WIDTH_COLUMN_OPERATION_AMOUNT = 150;
    private const Int32 WIDTH_COLUMN_OPERATION_ACCOUNT = 0;

    #endregion

    #region Private Methods
    /// <summary>
    /// Initialize controls
    /// </summary>
    public void InitControls(frm_container Parent)
    {
      m_parent = Parent;

      InitForm();

      if (m_first_time)
      {
        m_first_time = false;

        btn_undo_ticket.Click += new EventHandler(btn_button_clicked);
        btn_details.Click += new EventHandler(btn_button_clicked);

        InitializeControlResources();
      }
    }

    /// <summary>
    /// Initialize control resources. (NLS string, images, etc.)
    /// OJO: some resources are used more than once
    /// </summary>
    void InitializeControlResources()
    {
      lbl_title.Text = Resource.String("STR_UC_UNDO_TICKET_TITLE");
      btn_undo_ticket.Text = Resource.String("STR_UC_TICKETS_02");
      btn_details.Text = Resource.String("STR_UC_GIFT_DELIVERY_002");
      gb_tickets_list.HeaderText = Resource.String("STR_UC_TICKETS_12");
      btn_discard_tickets.Text = Resource.String("STR_FRM_TITO_PROMO_DISCARD_TICKETS");

      lbl_separator_1.BackColor = CashierStyle.Colors.m_colors_dic[CashierStyle.Colors.ENUM_COLORS.HEX_959595];

      this.BackColor = CashierStyle.Colors.m_colors_dic[CashierStyle.Colors.ENUM_COLORS.HEX_E3E7E9];
    }

    private void InitDataGrid()
    {
      //OPERATION_ID
      dgv_last_operations.Columns[GRID_COLUMN_OPERATION_ID].Width = WIDTH_COLUMN_OPERATION_ID;
      dgv_last_operations.Columns[GRID_COLUMN_OPERATION_ID].Visible = false;

      //OPERATION_CODE
      dgv_last_operations.Columns[GRID_COLUMN_OPERATION_CODE].Width = WIDTH_COLUMN_OPERATION_CODE;
      dgv_last_operations.Columns[GRID_COLUMN_OPERATION_CODE].Visible = false;

      //OPERATION_TYPE
      dgv_last_operations.Columns[GRID_COLUMN_OPERATION_DESC].Width = WIDTH_COLUMN_OPERATION_DESC;
      dgv_last_operations.Columns[GRID_COLUMN_OPERATION_DESC].Visible = true;
      dgv_last_operations.Columns[GRID_COLUMN_OPERATION_DESC].HeaderText = Resource.String("STR_FRM_VOUCHERS_REPRINT_014");
      dgv_last_operations.Columns[GRID_COLUMN_OPERATION_DESC].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

      //TICKET_CUANTITY
      dgv_last_operations.Columns[GRID_COLUMN_TICKET_CUANTITY].Width = WIDTH_COLUMN_TICKET_CUANTITY;
      dgv_last_operations.Columns[GRID_COLUMN_TICKET_CUANTITY].Visible = false;

      //OPERATION_DATE
      dgv_last_operations.Columns[GRID_COLUMN_OPERATION_DATE].HeaderText = Resource.String("STR_FRM_VOUCHERS_REPRINT_013");
      dgv_last_operations.Columns[GRID_COLUMN_OPERATION_DATE].DefaultCellStyle.Format = CultureInfo.InvariantCulture.DateTimeFormat.LongTimePattern;
      dgv_last_operations.Columns[GRID_COLUMN_OPERATION_DATE].Width = WIDTH_COLUMN_OPERATION_DATE;

      //OPERATION_AMOUNT
      dgv_last_operations.Columns[GRID_COLUMN_OPERATION_AMOUNT].HeaderText = Resource.String("STR_FRM_VOUCHERS_REPRINT_015");
      dgv_last_operations.Columns[GRID_COLUMN_OPERATION_AMOUNT].Width = WIDTH_COLUMN_OPERATION_AMOUNT;
      dgv_last_operations.Columns[GRID_COLUMN_OPERATION_AMOUNT].DefaultCellStyle.Format = "C" + Math.Abs(Format.GetFormattedDecimals(CurrencyExchange.GetNationalCurrency()));
      dgv_last_operations.Columns[GRID_COLUMN_OPERATION_AMOUNT].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

      //OPERATION_ACCOUNT
      dgv_last_operations.Columns[GRID_COLUMN_OPERATION_ACCOUNT].Width = WIDTH_COLUMN_OPERATION_ACCOUNT;
      dgv_last_operations.Columns[GRID_COLUMN_OPERATION_ACCOUNT].Visible = false;

      dgv_last_operations.ColumnHeadersHeight = 30;
      dgv_last_operations.RowTemplate.Height = 30;
      dgv_last_operations.RowTemplate.DividerHeight = 0;
    }

    private void PopulateDataGrid()
    {
      DataTable _dt_operations;
      StringBuilder _sb;
      Int32 _last_minutes;

      _last_minutes = GeneralParam.GetInt32("Cashier.Undo", "Allowed.Minutes");
      _dt_operations = new DataTable("OPERATIONS");
      _sb = new StringBuilder();

      try
      {
        _sb.AppendLine("     SELECT   TOP 100                                   ");
        _sb.AppendLine("              AO_OPERATION_ID                           ");
        _sb.AppendLine("	          , AO_CODE                                   ");
        _sb.AppendLine("            , COUNT(TI_TICKET_ID) TI_CTY                ");
        _sb.AppendLine("	          , AO_DATETIME                               ");
        _sb.AppendLine("	          , AO_AMOUNT                                 ");
        _sb.AppendLine("	          , AO_NON_REDEEMABLE                         ");
        _sb.AppendLine("	          , AO_ACCOUNT_ID                             ");
        _sb.AppendLine("       FROM   ACCOUNT_OPERATIONS                        ");
        _sb.AppendLine(" INNER JOIN   TICKETS                                   ");
        _sb.AppendLine("         ON   AO_OPERATION_ID = TI_TRANSACTION_ID       ");
        _sb.AppendLine("      WHERE   AO_UNDO_STATUS IS NULL                    ");
        _sb.AppendLine("        AND   TI_CREATED_TERMINAL_TYPE = @pTerminalType ");
        _sb.AppendLine("        AND   AO_CODE IN (@pCashIn, @pPromo)            ");
        _sb.AppendLine("        AND   AO_CASHIER_SESSION_ID = @pSessionId       ");
        _sb.AppendLine("        AND   AO_DATETIME           > @pStartDate       ");
        _sb.AppendLine("        AND   AO_AMOUNT + AO_NON_REDEEMABLE > 0         ");
        _sb.AppendLine("   GROUP BY   AO_OPERATION_ID, AO_CODE, AO_AMOUNT       ");
        _sb.AppendLine("            , AO_NON_REDEEMABLE, AO_DATETIME            ");
        _sb.AppendLine("            , AO_ACCOUNT_ID                             ");
        _sb.AppendLine("   ORDER BY   AO_OPERATION_ID DESC                      ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pStartDate", SqlDbType.DateTime).Value = WGDB.Now.AddMinutes(-_last_minutes);
            _cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = Cashier.SessionId;
            _cmd.Parameters.Add("@pCashIn", SqlDbType.Int).Value = Common.OperationCode.CASH_IN;
            _cmd.Parameters.Add("@pPromo", SqlDbType.Int).Value = Common.OperationCode.PROMOTION;
            _cmd.Parameters.Add("@pTerminalType", SqlDbType.Int).Value = TITO_TERMINAL_TYPE.CASHIER;

            _dt_operations.Load(_db_trx.ExecuteReader(_cmd));

          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return;
      }

      ProcessDataTable(_dt_operations);

      dgv_last_operations.DataSource = _dt_operations;
    }

    private void ProcessDataTable(DataTable Operations)
    {
      DataColumn _desc_col;
      String _description;

      if (Operations.Columns.Count == 0)
      {
        return;
      }

      _desc_col = Operations.Columns.Add("OPERATION_DESC", typeof(String));
      _desc_col.SetOrdinal(GRID_COLUMN_OPERATION_DESC);

      foreach (DataRow _row in Operations.Rows)
      {
        switch ((Common.OperationCode)_row["AO_CODE"])
        {
          case OperationCode.CASH_IN:
          case OperationCode.PROMOTION:
            _description = Resource.String("STR_FRM_VOUCHERS_REPRINT_028");//Promotion 
            if ((Decimal)_row["AO_AMOUNT"] > 0)
            {
              _description = Resource.String("STR_FRM_VOUCHERS_REPRINT_026");//Recharge
              _row["AO_CODE"] = OperationCode.CASH_IN;
            }
            _description += " (" + _row["TI_CTY"] + " tickets)";
            break;
          default:
            _description = "---";
            break;
        }

        _row["AO_AMOUNT"] = (Decimal)_row["AO_AMOUNT"] + (Decimal)_row["AO_NON_REDEEMABLE"];
        _row["OPERATION_DESC"] = _description;
      }

      Operations.Columns.Remove("AO_NON_REDEEMABLE");
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Restore focus in th form
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    private void RestoreParentFocus()
    {
      this.ParentForm.Focus();
      this.ParentForm.BringToFront();
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Set status of ALL buttons in control
    // 
    //  PARAMS:
    //      - INPUT:
    //        - Ticket: reference to a ticket instance
    //
    //      - OUTPUT:
    //
    private void SetButtonStatus()
    {
      Boolean _is_tito_cashier_open;

      _is_tito_cashier_open = BusinessLogic.IsTITOCashierOpen;

      btn_undo_ticket.Enabled = _is_tito_cashier_open && dgv_last_operations.SelectedRows.Count > 0;
      btn_details.Enabled = dgv_last_operations.SelectedRows.Count > 0;
      btn_discard_tickets.Enabled = _is_tito_cashier_open && dgv_last_operations.SelectedRows.Count > 0 && OperationCode.PROMOTION == (OperationCode)dgv_last_operations.SelectedRows[0].Cells[GRID_COLUMN_OPERATION_CODE].Value;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: clear all controls in the form
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    private void InitForm()
    {
      UpdateGrid();
      SetButtonStatus();
    }

    private void UpdateGrid()
    {
      PopulateDataGrid();
      InitDataGrid();
    }

    private void LoadOperationVouchers(Int64 OperationId)
    {
      web_browser.Navigate("");

      if (OperationId > 0)
      {
        if (!VoucherManager.LoadVouchers(VoucherSource.FROM_OPERATION,
                                         OperationId,
                                         true,
                                         web_browser))
        {
          frm_message.Show(Resource.String("STR_FRM_VOUCHERS_REPRINT_017"),
                           Resource.String("STR_APP_GEN_MSG_ERROR"),
                           MessageBoxButtons.OK,
                           MessageBoxIcon.Error,
                           this.ParentForm);
        }
      }
    }

    private void ShowOperationTickets(Int64 OperationId)
    {
      StringBuilder _sb;
      DataTable _tickets_table;
      frm_tickets_reprint _frm_ticket_sel;


      _frm_ticket_sel = new frm_tickets_reprint();
      _tickets_table = new DataTable();
      try
      {
        m_tickets_detail = _frm_ticket_sel.CreateSelectTicketTable();

        _sb = new StringBuilder();
        _sb.AppendLine("   SELECT   TI_TICKET_ID          AS TI_TICKET_ID");
        _sb.AppendLine("          , TI_VALIDATION_NUMBER  AS TI_VALIDATION_NUMBER");
        _sb.AppendLine("          , TI_AMOUNT             AS TI_AMOUNT");
        _sb.AppendLine("          , TI_CREATED_DATETIME   AS TI_CREATED_DATETIME");
        _sb.AppendLine("          , ISNULL(CASE WHEN TI_CREATED_TERMINAL_TYPE = 0 THEN CT.CT_NAME ELSE TE.TE_NAME END, '') AS TE_NAME");
        _sb.AppendLine("          , TI_TYPE_ID            AS TI_TYPE_ID");
        _sb.AppendLine("          , TI_STATUS             AS TI_STATUS");
        _sb.AppendLine("     FROM   TICKETS TI");
        _sb.AppendLine("LEFT JOIN   TERMINALS TE ON (TE.TE_TERMINAL_ID = TI_CREATED_TERMINAL_ID)");
        _sb.AppendLine("LEFT JOIN   CASHIER_TERMINALS CT ON (CT.CT_CASHIER_ID = TI_CREATED_TERMINAL_ID)");
        _sb.AppendLine("LEFT JOIN   TERMINAL_TYPES TT ON (TT.TT_TYPE = TI_CREATED_TERMINAL_TYPE)");
        _sb.AppendLine("LEFT JOIN   TERMINALS TE2 ON (TE2.TE_TERMINAL_ID = TI_LAST_ACTION_TERMINAL_ID)");
        _sb.AppendLine("LEFT JOIN   CASHIER_TERMINALS CT2 ON (CT2.CT_CASHIER_ID = TI_LAST_ACTION_TERMINAL_ID)");
        _sb.AppendLine("LEFT JOIN   TERMINAL_TYPES TT2 ON (TT2.TT_TYPE = TI_LAST_ACTION_TERMINAL_TYPE)");
        _sb.AppendLine("    WHERE   TI_TRANSACTION_ID = @pOperationId ");
        _sb.AppendLine("      AND   TI_CREATED_TERMINAL_TYPE = @pTerminalType ");
        _sb.AppendLine(" ORDER BY   TI_CREATED_DATETIME DESC ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt).Value = OperationId;
            _cmd.Parameters.Add("@pTerminalType", SqlDbType.Int).Value = TITO_TERMINAL_TYPE.CASHIER;

            using (SqlDataAdapter _da = new SqlDataAdapter(_cmd))
            {
              if (_da.Fill(m_tickets_detail) == 0)
              {
                return;
              }
            }
          }
        }

        _frm_ticket_sel.ShowTicketList(m_tickets_detail);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {
        // ICS 14-APR-2014 Free the object
        _frm_ticket_sel.Dispose();
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Open form to discard the tickets
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:

    private void CancelMassivePromotionsTickets(Int64 OperationId)
    {
      List<Int64> _ticket_list;

      if (Ticket.DB_GetTicketsFromOperation(OperationId, out _ticket_list))
      {
        if (_ticket_list.Count > 0)
        {
          frm_ticket_discard _ticket_discard = new frm_ticket_discard();
          _ticket_discard.Show(OperationId, false, m_parent);
          // JPJ 31-JAN-2014 We refresh the grid if user has accepted the window
          if (_ticket_discard.DialogResult == DialogResult.OK)
          {
            InitForm();
          }

          _ticket_discard.Dispose();
        }
      }
    }

    #endregion

    #region Events

    private void dgv_last_operations_SelectionChanged(object sender, EventArgs e)
    {
      Int64 _operation_id;

      _operation_id = 0;

      if (dgv_last_operations.SelectedRows.Count > 0)
      {
        _operation_id = (Int64)dgv_last_operations.SelectedRows[0].Cells[GRID_COLUMN_OPERATION_ID].Value;
      }

      LoadOperationVouchers(_operation_id);
      SetButtonStatus();
    }

    private void btn_button_clicked(object sender, EventArgs e)
    {
      // LEM 07-JAN-2014: Accept only mouse events.
      if (!(e is MouseEventArgs))
      {
        return;
      }

      Cashier.ResetAuthorizedByUser();

      try
      {
        if (sender.Equals(btn_undo_ticket))
        {
          btn_undo_ticket_Click(sender, e);
        }
        if (sender.Equals(btn_details))
        {
          btn_details_Click(sender, e);
        }
      }
      finally
      {
        Cashier.ResetAuthorizedByUser();
      }
    }

    private void btn_undo_ticket_Click(object sender, EventArgs e)
    {
      Int64 _account_id;
      Int64 _operation_id;
      ENUM_TITO_OPERATIONS_RESULT _result;
      CardData _card_data;
      ParamsTicketOperation _operation_params;
      String _str_error;
      OperationUndo.UndoError _error;
      AccountOperations.Operation _account_operation;
      CurrencyIsoType _iso_type;
      Decimal _iso_amount;
      List<Int64> _ticket_list;
      String _str_message;
      ArrayList _voucher_list;

      _error = OperationUndo.UndoError.None;
      _result = ENUM_TITO_OPERATIONS_RESULT.AUX_OPERATION_CANCELED;

      if (dgv_last_operations.SelectedRows.Count == 0)
      {
        return;
      }

      // Check if current user is an authorized user
      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.UndoLastOperation,
                                               ProfilePermissions.TypeOperation.RequestPasswd, m_parent, out _str_error))
      {
        return;
      }

      _result = ENUM_TITO_OPERATIONS_RESULT.AUX_OPERATION_ERROR;

      try
      {
        _account_id = (Int64)dgv_last_operations.SelectedRows[0].Cells[GRID_COLUMN_OPERATION_ACCOUNT].Value;
        _operation_id = (Int64)dgv_last_operations.SelectedRows[0].Cells[GRID_COLUMN_OPERATION_ID].Value;

        _card_data = new CardData();
        if (!CardData.DB_CardGetAllData(_account_id, _card_data))
        {
          return;
        }

        _operation_params = new ParamsTicketOperation();
        _operation_params.in_account = _card_data;
        _operation_params.in_window_parent = m_parent;
        _operation_params.session_info = Cashier.CashierSessionInfo();
        _operation_params.out_operation_id = _operation_id;

        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (!OperationUndo.IsTITOReversible(_card_data, _operation_id, Cashier.CashierSessionInfo().CashierSessionId, OperationCode.CASH_IN,
                                              out _account_operation, out _iso_type, out _iso_amount, out _error, out _ticket_list, _db_trx.SqlTransaction))
          {
            return;
          }
        }

        // Show information message
        _str_message = Resource.String("STR_UNDO_RECHARGE_TICKETS", _ticket_list.Count);
        _str_message = _str_message.Replace("\\r\\n", "\r\n");
        if (DialogResult.OK != frm_message.Show(_str_message,
                                   Resource.String("STR_UNDO_OPERATION_TICKET"),
                                   MessageBoxButtons.YesNo, Images.CashierImage.Question, m_frm_shadow_gui))
        {
          _result = ENUM_TITO_OPERATIONS_RESULT.AUX_OPERATION_CANCELED;

          return;
        }

        OperationUndo.InputUndoOperation _params = new OperationUndo.InputUndoOperation();
        _params.CodeOperation = OperationCode.CASH_IN;
        _params.CardData = _card_data;
        _params.OperationIdForUndo = _operation_id;
        _params.CashierSessionInfo = Cashier.CashierSessionInfo();

        if (!OperationUndo.UndoOperation(_params, out _voucher_list))
        {
          return;
        }

        _result = ENUM_TITO_OPERATIONS_RESULT.OK;

        VoucherPrint.Print(_voucher_list);
        InitForm();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {
        m_frm_shadow_gui.Show(this.ParentForm);

        switch (_result)
        {
          case ENUM_TITO_OPERATIONS_RESULT.OK:
            UpdateGrid();
            break;
          case ENUM_TITO_OPERATIONS_RESULT.AUX_OPERATION_CANCELED:
            //do nothing
            break;
          case ENUM_TITO_OPERATIONS_RESULT.TICKET_NOT_VALID:
            break;
          default:
            frm_message.Show(OperationUndo.GetErrorMessage(_error), Resource.String("STR_APP_GEN_MSG_ERROR"),
                             MessageBoxButtons.OK, MessageBoxIcon.Error, m_frm_shadow_gui);
            break;
        }

        this.m_frm_shadow_gui.Hide();
        this.RestoreParentFocus();
      }
    }

    private void btn_details_Click(object sender, EventArgs e)
    {
      if (dgv_last_operations.SelectedRows.Count == 0)
      {
        return;
      }

      ShowOperationTickets((Int64)dgv_last_operations.SelectedRows[0].Cells[GRID_COLUMN_OPERATION_ID].Value);

      this.RestoreParentFocus();
    }

    #endregion

    private void btn_discard_tickets_Click(object sender, EventArgs e)
    {
      String _msg;

      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.TITO_DiscardTicket,
                                   ProfilePermissions.TypeOperation.RequestPasswd,
                                   this.ParentForm,
                                   out _msg))
      {
        return;
      }

      if (OperationCode.PROMOTION == (OperationCode)dgv_last_operations.SelectedRows[0].Cells[GRID_COLUMN_OPERATION_CODE].Value)
      {
        CancelMassivePromotionsTickets((Int64)dgv_last_operations.SelectedRows[0].Cells[GRID_COLUMN_OPERATION_ID].Value);
      }
    }

  }
}
