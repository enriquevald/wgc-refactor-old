//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_TITO_handpays.cs
// 
//   DESCRIPTION: Implements the following classes:
//                1. frm_TITO_handpays: List of candidate tickets to apply handpay
//
// CREATION DATE: 12-NOV-2013
//
// REVISION HISTORY: 
// 
// Date        Author     Description
// ----------- ------     ----------------------------------------------------------
// 09-SEP-2014 MPO        First release.
// 13-OCT-2014 MPO        WIG-1475: Progressive jackpots exception.
// 15-OCT-2014 JBC        Fixed Bug WIG-1474: Now textbox works like payment order.
// 23-OCT-2014 LEM        Fixed Bug WIG-1561: Wrong taxes apply if authorization required.
// 03-NOV-2014 MPO        WIG-1620: Show incorrect message when cancel payment
// 27-NOV-2014 MPO        WIG-1769: The payment permission always is needed. Incorrect use of Cashier.HandPays LimitToAllowPayment
// 02-DEC-2014 MPO        Fixed bugs WIG-1793 / WIG-1788: need check status after update
// 23-DEC-2014 MPO        Fixed bugs WIG-1874
// 16-JAN-2016 JMM & MPO  Heed on partial pay field comming from the 1B SAS poll
// 16-FEB-2015 MPO        WIG-2065: Progressive level not reported correctly
// 03-MAR-2015 YNM        WIG-2118: Handpaid: deleting "base of retention", the amount is erased
// 10-MAR-2015 FOS        TASK-333: Televisa:Pagos Manuales de entrada Manual
// 10-APR-2015 DLL        WIG-2199: Incorrect enabled buttons when handpay is authorize
// 10-NOV 2015 FAV        Product Backlog Item 6015: New design
// 11-NOV-2015 SGB        Backlog Item WIG-5880: Change designer
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WSI.Common;
using WSI.Common.TITO;
using WSI.Cashier.TITO;
using WSI.Cashier.Controls;

namespace WSI.Cashier
{
  public partial class frm_transfer_to_terminal : frm_base
  {
    public enum ENTRY_TYPE
    {
      DEFAULT = 0,
      MANUAL = 1,
    }

    private const Int32 TYPE_JACKPOT_PROGRESSIVE_40 = 9999;
    private const Int32 TYPE_JACKPOT_PROGRESSIVE = 8888;
    private const Int32 BOTTOM_MARGIN = 100;

    private ENTRY_TYPE m_entry_type = ENTRY_TYPE.DEFAULT;
    private frm_yesno form_yes_no;
    private Form m_parent;
    private frm_generic_amount_input m_amount_input = null;
    private Control m_current_amount_control;
    private static String m_decimal_str = System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencyDecimalSeparator;
    private Boolean m_can_edit = false;
    private Boolean m_is_tito = WSI.Common.TITO.Utils.IsTitoMode();

    public frm_transfer_to_terminal()
    {

      Point _main_form_location;
      Size _main_form_size;
      Int32 _form_location_X;
      Int32 _form_location_Y;

      InitializeComponent();

      this.FormTitle = Resource.String("STR_INPUT_TARGET_CARD_BUTTON");

      // NLS
      this.Text = Resource.String("STR_HANDPAY_ACTION_01");
      this.gb_terminal.HeaderText = Resource.String("STR_HANDPAY_ACTION_02");
      this.lbl_amount.Text = Resource.String("STR_FRM_HANDPAYS_006");
      this.lbl_to_pay.Text = Resource.String("STR_TOTAL_TO_TRANSFER");
      this.btn_transfer.Text = Resource.String("STR_INPUT_TARGET_CARD_BUTTON");
      this.btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");

      InitControls();
      form_yes_no = new frm_yesno();
      form_yes_no.Opacity = 0.6;

      this.StartPosition = FormStartPosition.Manual;
      this.CustomLocation = true;

      _main_form_size = WindowManager.GetMainFormAreaSize();
      _form_location_X = 0;
      _form_location_Y = 0;

      m_amount_input = new frm_generic_amount_input();
      _main_form_location = WindowManager.GetMainFormAreaLocation();
      _form_location_X += (_main_form_size.Width - (m_amount_input.Width + this.Width)) / 2;
      _form_location_Y += _main_form_location.Y + (_main_form_size.Height - this.Height) / 2;
      this.Location = new Point(_form_location_X, _form_location_Y);
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Init and Show the form.
    //
    //  PARAMS :
    //      - INPUT :
    //          - ParentForm
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public void Show(Form Parent)
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_transfer_to_terminal", Log.Type.Message);

      m_parent = Parent;

      try
      {
        form_yes_no.Show(Parent);
        this.ShowDialog(form_yes_no);
        form_yes_no.Hide();

        Misc.WriteLog("[FORM CLOSE] frm_transfer_to_terminal (show)", Log.Type.Message);
        this.Close();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        form_yes_no.Hide();
      }
    }

    private void btn_transfer_Click(object sender, EventArgs e)
    {
      Currency _amount;
      Int32 _terminal_id_selected;
      String _terminal_name_selected;

      try
      {
        _amount = Format.ParseCurrency(txt_amount.Text);
        _terminal_id_selected = uc_terminal_filter.TerminalSelected();
        _terminal_name_selected = uc_terminal_filter.TerminalSelectedName();

        if (_amount <= 0)
        {
          frm_message.Show(Resource.String("STR_FRM_HANDPAYS_MANUAL_006"), Resource.String("STR_FRM_VOUCHERS_REPRINT_081"), MessageBoxButtons.OK, MessageBoxIcon.Information, this.ParentForm);

          return;
        }

        Currency _sales_limit;

        if (GeneralParam.GetBoolean("Cashier", "SalesLimit.Enabled"))
        {
          _sales_limit = Math.Max(0, GeneralParam.GetCurrency("Cashier", "SalesLimit.DefaultValue", 0));
          _sales_limit = Math.Max(_sales_limit, GeneralParam.GetCurrency("Cashier", "SalesLimit.MaxValue", 0));

          if (_sales_limit > 0 && _amount > _sales_limit)
          {
            frm_message.Show(Resource.String("STR_FRM_CASHIER_LIMIT_SALES_REACHED").Replace("\\r\\n", "\r\n") + _sales_limit, Resource.String("STR_FRM_VOUCHERS_REPRINT_081"), MessageBoxButtons.OK, MessageBoxIcon.Information, this.ParentForm);

            return;
          }
        }

        using (frm_transfer_to_terminal_status _frm = new frm_transfer_to_terminal_status())
        {
          _frm.Show(this, _terminal_id_selected, _terminal_name_selected, _amount);
        }

        this.btn_cancel_Click(null, null);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {
      }

    }

    private void RefreshTotals()
    {
      lbl_amount_to_pay.Text = ((Currency)Format.ParseCurrency(txt_amount.Text)).ToString();
    }

    private void InitControls()
    {
      StringBuilder _where = new StringBuilder();
      Int32 _handpay_time_period = 0;
      Int32 _handpay_cancel_time_period = 0;

      // Get the Handpays settings from the General Parameters table
      Handpays.GetParameters(out _handpay_time_period, out _handpay_cancel_time_period);
      if (m_is_tito)
      {
        // Load the list of available terminals
        _where.AppendLine("   WHERE TE_TERMINAL_TYPE = " + (Int32)TerminalTypes.SAS_HOST);
        _where.AppendLine(String.Format("      AND (  TE_RETIREMENT_DATE IS NULL OR DATEDIFF(MINUTE, TE_RETIREMENT_DATE, GETDATE()) <= {0} ) ", _handpay_time_period));
      }
      else
      {
        // Load the list of available terminals
        _where.AppendLine("   WHERE TE_TERMINAL_TYPE = " + (Int32)TerminalTypes.SAS_HOST);
        _where.AppendLine(String.Format("      AND (  TE_RETIREMENT_DATE IS NULL OR DATEDIFF(MINUTE, TE_RETIREMENT_DATE, GETDATE()) <= {0} ) ", _handpay_time_period));
      }

      uc_terminal_filter.InitControls("", _where.ToString(), true, true);

      txt_amount.Text = ((Currency)0).ToString();

      if (!m_is_tito)
      {
      }
      lbl_amount_to_pay.Text = ((Currency)0).ToString();

    }

    void m_amount_input_OnKeyPress(String Amount)
    {
      Decimal _amount;

      if (String.IsNullOrEmpty(Amount))
      {
        _amount = 0;
      }
      else if (!Decimal.TryParse(Amount, out _amount))
      {
        return;
      }

      SetAmountToControl(m_current_amount_control, _amount, false);

      RefreshTotals();
    }

    private Boolean SetAmountToControl(Control TxtControl, Decimal Amount, Boolean CurrencySymbol)
    {
      TxtControl.Text = ((Currency)Amount).ToString(CurrencySymbol);

      return true;
    }

    private void frm_imput_Leave(object sender, EventArgs e)
    {
      if (m_amount_input != null)
      {
        m_amount_input.Hide();
        m_amount_input.Dispose();
        m_amount_input = null;
      }
    }

    private void txt_amount_KeyPress(object sender, KeyPressEventArgs e)
    {
      WSI.Cashier.Controls.uc_round_textbox txt_aux;
      String aux_str;
      char c;

      c = e.KeyChar;

      if (sender == txt_amount)
      {
        txt_aux = txt_amount;
      }
      else
      {
        return;
      }

      if (c >= '0' && c <= '9')
      {
        aux_str = "" + c;
        AddNumber(aux_str);
        e.Handled = true;
      }

      if (c == '\b')
      {
      }

      if (c == '.')
      {
        if (txt_aux.Text.IndexOf(m_decimal_str) == -1)
        {
          txt_aux.Text = txt_aux.Text + m_decimal_str;
        }
      }

      e.Handled = true;

      if (e.Handled && c != '.')
      {
        Decimal _amount;

        if (!Decimal.TryParse(txt_aux.Text, out _amount))
        {
          return;
        }

        SetAmountToControl((Control)sender, _amount, true);

        RefreshTotals();
      }
    }

    private void txt_amount_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
    {
      char c;

      c = Convert.ToChar(e.KeyCode);
      if (c == '\r')
      {
        e.IsInputKey = true;
      }
    }

    /// <summary>
    /// Check and Add digit to amount
    /// </summary>
    /// <param name="NumberStr"></param>
    private void AddNumber(String NumberStr)
    {
      String aux_str;
      String tentative_number;
      Currency input_amount;
      int dec_symbol_pos;

      // Prevent exceeding maximum number of decimals (2)
      dec_symbol_pos = txt_amount.Text.IndexOf(m_decimal_str);
      if (dec_symbol_pos > -1)
      {
        aux_str = txt_amount.Text.Substring(dec_symbol_pos);
        if (aux_str.Length > 2)
        {
          return;
        }
      }

      // Prevent exceeding maximum amount length
      if (txt_amount.Text.Length >= Cashier.MAX_ALLOWED_AMOUNT_LENGTH)
      {
        return;
      }

      tentative_number = txt_amount.Text + NumberStr;

      try
      {
        input_amount = Decimal.Parse(tentative_number);
        if (input_amount > Cashier.MAX_ALLOWED_AMOUNT)
        {
          return;
        }
      }
      catch
      {
        input_amount = 0;

        return;
      }

      // Remove unneeded leading zeros
      // Unless we are entering a decimal value, there must not be any leading 0
      if (dec_symbol_pos == -1)
      {
        txt_amount.Text = txt_amount.Text.TrimStart('0') + NumberStr;
      }
      else
      {
        // Accept new symbol/digit 
        txt_amount.Text = txt_amount.Text + NumberStr;
      }
    }


    private void frm_handpay_action_Activated(object sender, EventArgs e)
    {
      HidePadNumber();
    }

    private void txt_amount_MouseClick(object sender, MouseEventArgs e)
    {
      ShowPadNumber((Control)sender);
    }

    private void txt_amount_Enter(object sender, EventArgs e)
    {
      ShowPadNumber((Control)sender);
    }

    private void ShowPadNumber(Control AmountCtrl)
    {
      Decimal _amount;
      Boolean _result;

      m_current_amount_control = AmountCtrl;
      _amount = Format.ParseCurrency(m_current_amount_control.Text);

      if (m_amount_input == null)
      {
        m_amount_input = new frm_generic_amount_input();
        m_amount_input.OnKeyPressed += new frm_generic_amount_input.KeyPressed(m_amount_input_OnKeyPress);
        m_amount_input.StartPosition = FormStartPosition.Manual;
        m_amount_input.CustomLocation = true;

        m_amount_input.Location = new Point(this.Location.X + this.Width,
                                            this.Location.Y + (this.Height - m_amount_input.Height + (80))); // 80 = Button cancel of m_amount_input hider

      }

      _result = m_amount_input.Show(GENERIC_AMOUNT_INPUT_TYPE.AMOUNT, string.Empty, ref _amount);

      if (_result)
      {
        RefreshTotals();
        m_amount_input.BringToFront();
        m_amount_input.Focus();
        m_can_edit = false;
      }
    }

    private void HidePadNumber()
    {
      if (!m_can_edit)
      {
        m_can_edit = true;

        if (m_amount_input != null)
        {
          m_amount_input.Hide();
          m_amount_input.Dispose();
          m_amount_input = null;
        }
      }
    }

    private void cb_Level_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (m_entry_type != ENTRY_TYPE.MANUAL)
      {
        return;
      }

    }

    private void txt_amount_Leave(object sender, EventArgs e)
    {
      Currency _amount;
      Control _sender;

      _sender = (Control)sender;

      _amount = Format.ParseCurrency(_sender.Text);
      _sender.Text = _amount.ToString(true);
    }

    private void btn_cancel_Click(object sender, EventArgs e)
    {
      form_yes_no.Hide();

      Misc.WriteLog("[FORM CLOSE] frm_transfer_to_terminal (cancel)", Log.Type.Message);
      this.Close();
    }

  }
}

