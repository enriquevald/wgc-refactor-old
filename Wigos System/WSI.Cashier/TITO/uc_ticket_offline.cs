//------------------------------------------------------------------------------
// Copyright � 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_ticket_offline.cs
// 
//   DESCRIPTION: Implements the following classes:
//                1. uc_ticket_offline
//
//        AUTHOR: Jaume Barn�s
// 
// CREATION DATE: 02-DEC-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 02-DEC-2013 JBC     First release.
// 15-JAN-2014 JPJ     Setting automatically the terminal, generating alarms, retired terminals aren't showen, cashier tickets aren't payable
// 28-FEB-2014 DHA     WIGOSTITO-1101: tickets offline expiration date are set to the default ticket expiration days
// 07-MAR-2014 QMP & DDM & DHA Fixed bug WIGOSTITO-1131: Created Cash/Account movements have the same Operation ID
// 02-APR-2014 ICS     Fixed Bug WIGOSTITO-1186: Print offline ticket: there is no error if ticket printer is not ready
// 24-APR-2014 HBB     Fixed Bug: WIGOSTITO-1213 : Create a voucher for redeemed offline ticket 
// 25-JUN-2014 DRV     Fixed Bug: WIGOSTITO-1242 : Incorrect label paying ticket without a selected terminal.
// 11-NOV-2014 LEM     Fixed Bug WIG-1674: Don't restore cashier session information after voucher generation.
// 20-NOV-2014 SGB     Fixed Bug WIG-1708: Incorrect behavior when entering incorrect display ticket.
// 02-FEB-2015 MPO     WIG-1969: System crash when printing "constancias"
// 23-NOV-2015 FOS     Bug 6771:Payment tickets & handpays
// 05-JAN-2016 JMV     Bug 8022:Cajero - Pago de tickets offline
// 02-FEB-2016 JML     Product Backlog Item 8352:Floor Dual Currency: Ticket Offline
// 24-MAR-2016 DHA     Fixed Bug 10987: on dual currency mode insert operation amount in national currency
// 20-JUL-2016 ETP     Fixed Bug 15637: Print exception log when handpays fail.
// 18-OCT-2016 FAV     PBI 20407: TITO Ticket: Calculated fields
// 31-JAN-2017 ATB     Bug 21719:CountR is applying taxes when it never owns
// 05-APR-2017 RAB     PBI 26537: MES10 Ticket validation - Prevent the use of dealer copy tickets (Cashier)
// 04-MAY-2017 RAB     PBI 27173: MES10 Ticket validation - Ticket not found messages
// 10-OCT-2017 JML     PBI 30023:WIGOS-4068 Payment threshold registration - Transaction information
//-------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using WSI.Common;
using WSI.Cashier.TITO;
using WSI.Common.TITO;
using System.Data.SqlClient;
using System.Collections;

namespace WSI.Cashier.TITO
{
  public partial class uc_ticket_offline : UserControl
  {
    #region Enums

    public enum WCP_TerminalType
    {
      IntermediateServer = 0,
      GamingTerminal = 1
    }

    #endregion

    #region Members

    private frm_yesno m_frm_shadow_gui;
    private Boolean m_first_time = true;
    private Int32 m_machine_seq;
    private Ticket m_ticket_read;
    private Ticket m_ticket_to_pay;
    private Int64 m_machine_seq_to_pay;
    private static String m_decimal_str = System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencyDecimalSeparator;
    private Ticket.ENUM_PAYMENT_TYPE m_payment_type;
    private TITO_TICKET_TYPE m_tito_ticket_type;

    #endregion

    #region Constants

    private const Int32 MAX_HOURS_IS_OFFLINE = 24;

    #endregion

    #region Constructor

    //Constructor
    public uc_ticket_offline()
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[UC LOAD] uc_ticket_offline", Log.Type.Message);

      InitializeComponent();

      m_frm_shadow_gui = new frm_yesno();
      m_frm_shadow_gui.Opacity = 0.6;

      EventLastAction.AddEventLastAction(this.Controls);

      gp_digits_box.HeaderBackColor = WSI.Cashier.CashierStyle.Colors.m_colors_dic[CashierStyle.Colors.ENUM_COLORS.HEX_373B3F];
      gp_digits_box.PanelBackColor = WSI.Cashier.CashierStyle.Colors.m_colors_dic[CashierStyle.Colors.ENUM_COLORS.HEX_F4F6F9];
    }

    //Establece el foco en el control lector de ticket.
    public void InitFocus()
    {
      this.uc_ticket_reader.Focus();
    }

    #endregion

    #region Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE:  Enable or Disable uc terminal and buttons.
    // 
    //  PARAMS:
    //      - INPUT:
    //            - Value (Boolean)
    //
    private void EnableButtons(Boolean Value, TITO_VALIDATION_TYPE ValidationType)
    {
      this.uc_terminal_filter.Enabled = ValidationType != TITO_VALIDATION_TYPE.SECURITY_ENHANCED && Value ? true : false;

      if (CashierBusinessLogic.ReadCashierStatus(Cashier.SessionId) != CASHIER_STATUS.OPEN)
      {
        this.btn_amount_add.Enabled = false;
        this.btn_gen.Enabled = false;
      }
      else
      {
        //SGB 10-DIC-14 It is checked if not terminal, with set m_ticket_read.CreatedTerminalType in previos condition
        if (ValidationType == TITO_VALIDATION_TYPE.SECURITY_ENHANCED && m_ticket_read.CreatedTerminalType == (Int32)TITO_TERMINAL_TYPE.CASHIER)
        {
          this.btn_amount_add.Enabled = false;
          this.btn_gen.Enabled = false;
          this.gp_digits_box.Enabled = false;
          this.gb_date.Enabled = false;
          return;
        }
        else
        {
          this.btn_amount_add.Enabled = Value;
          this.btn_gen.Enabled = Value;
        }

      }
      this.gp_digits_box.Enabled = Value;
      this.gb_date.Enabled = ValidationType == TITO_VALIDATION_TYPE.STANDARD && Value ? true : false;
      this.btn_intro.Visible = false;

    } // EnableButtons

    //------------------------------------------------------------------------------
    // PURPOSE:  Validates input ticket.
    // 
    //  PARAMS:
    //      - INPUT:
    //            - ValidationNumber (String)
    //      - OUTPUT:
    //            - IsValid (String)
    //
    private void ProcessTicketNumber(String ValidationNumber, ref Boolean IsValid, out String ErrorStr)
    {
      Int64 _validation_number;
      TITO_VALIDATION_TYPE _validation_type;
      Int32 _created_terminal_id;
      Boolean _terminal_selected;

      m_ticket_read = new Ticket();

      IsValid = false;
      ErrorStr = "";
      _validation_number = 0;
      _terminal_selected = false;
      _created_terminal_id = 0;

      if (String.IsNullOrEmpty(ValidationNumber))
      {
        IsValid = false;
        ErrorStr = Resource.String("STR_UC_TICKET_INVALID");

        return;
      }

      _validation_number = ValidationNumberManager.UnFormatValidationNumber(ValidationNumber, out _validation_type);

      if (!IsTicketOffline(_validation_number))
      {
        IsValid = false;
        ErrorStr = Resource.String("STR_UC_TICKET_INVALID");

        //To determine the type of error get the ticket to find out the id type
        m_ticket_read = BusinessLogic.SelectTicketByCondition(String.Format(" TI_VALIDATION_NUMBER = {0} ", _validation_number), String.Format(" TI_STATUS = {0} ", (Int32)TITO_TICKET_STATUS.VALID));
        if (m_ticket_read.TicketType == TITO_TICKET_TYPE.CHIPS_DEALER_COPY)
        {
          ErrorStr = Resource.String("STR_UC_DEALER_COPY_DEALER_NOT_FOUND");
        }
      }
      else if (_validation_number > 0)
      {
        m_ticket_read.ValidationNumber = _validation_number;
        m_ticket_read.ValidationType = _validation_type;
        m_ticket_read.CreatedTerminalType = (Int32)TITO_TERMINAL_TYPE.TERMINAL;

        if (m_ticket_read.ValidationType == TITO_VALIDATION_TYPE.SECURITY_ENHANCED)
        {
          ValidationNumberManager.Decrypt(_validation_number, out _created_terminal_id, out m_machine_seq);

          m_ticket_read.CreatedTerminalId = _created_terminal_id;
          m_ticket_read.CreatedTerminalType = IsTerminal(_created_terminal_id) ? (Int32)TITO_TERMINAL_TYPE.TERMINAL : m_ticket_read.CreatedTerminalType = (Int32)TITO_TERMINAL_TYPE.CASHIER; ;
          _terminal_selected = uc_terminal_filter.SelectTerminal(_created_terminal_id);
          if (m_ticket_read.CreatedTerminalType == (Int32)TITO_TERMINAL_TYPE.CASHIER)
          {
            uc_ticket_reader.TicketMessage(Resource.String("STR_UC_TICKET_OFFLINE_NOT_FROM_TERMINAL"));
          }

          IsValid = true;
        }
        else
        {
          IsValid = true;
        }
        this.lbl_ticket_num.Text = ValidationNumberManager.FormatValidationNumber(_validation_number, (Int32)_validation_type, true);

        if (_created_terminal_id == 0)
        {
          _created_terminal_id = uc_terminal_filter.TerminalSelected();
        }

        ShowTerminalCurrency(_created_terminal_id);

        RestoreParentFocus();
        InitFocus();
      }
      EnableButtons(IsValid, _validation_type);

    } //ProcessTicketNumber

    private void ShowTerminalCurrency(int TerminalId)
    {
      Terminal.TerminalInfo _terminal_info;
      Currency _input_amount;
      String _national_currency;
      Boolean _floor_dual_currency_enabled;
      Currency _input_tmp;

      _national_currency = CurrencyExchange.GetNationalCurrency();
      _floor_dual_currency_enabled = GeneralParam.GetBoolean("FloorDualCurrency", "Enabled", false);
      _input_tmp = 0;

      if (!_floor_dual_currency_enabled)
      {
        this.lbl_ticket_curr_text.Visible = false;
        this.lbl_ticket_curr.Visible = false;
        this.lbl_national_amount.Visible = false;
        return;
      }

      using (DB_TRX _db_trx = new DB_TRX())
      {
        if (Terminal.Trx_GetTerminalInfo(TerminalId, out _terminal_info, _db_trx.SqlTransaction))
        {
          this.lbl_ticket_curr_text.Text = Resource.String("STR_UC_TICKET_OFFLINE_TERMINAL") + " " + _terminal_info.CurrencyCode;
          this.lbl_ticket_curr.Text = _terminal_info.CurrencyCode;
        }
        if (this.txt_amount.Text.Length > 0)
        {
          _input_amount = Decimal.Parse(this.txt_amount.Text);
        }
        else
        {
          _input_amount = 0;
        }

        if (lbl_ticket_curr.Text.Length > 0 && _national_currency != this.lbl_ticket_curr.Text)
        {
          _input_tmp = CurrencyExchange.GetExchange(_input_amount, this.lbl_ticket_curr.Text, _national_currency, _db_trx.SqlTransaction);
        }
      }

      this.lbl_national_amount.Text = Resource.String("STR_VOUCHER_CONVERTED_TO") + " " + Currency.Format(_input_tmp, _national_currency);

      this.lbl_ticket_curr_text.Visible = _floor_dual_currency_enabled && this.lbl_ticket_curr.Text != String.Empty;
      this.lbl_ticket_curr.Visible = this.lbl_ticket_curr_text.Visible;
      this.lbl_national_amount.Visible = this.lbl_ticket_curr_text.Visible && (_national_currency != this.lbl_ticket_curr.Text);

    } //ShowTerminalCurrency

    //------------------------------------------------------------------------------
    // PURPOSE:  Variable reset
    // 
    //  PARAMS:
    //
    private void ClearForm()
    {
      this.uc_ticket_reader.ClearTrackNumber();
      ResetValues();
      EnableButtons(false, TITO_VALIDATION_TYPE.STANDARD);
      this.uc_ticket_reader.Focus();
    } // ClearForm

    //------------------------------------------------------------------------------
    // PURPOSE:  Initialize Control Resources
    // 
    //  PARAMS:
    //
    private void InitializeControlResources()
    {
      this.lbl_cards_title.Text = Resource.String("STR_UC_TICKET_OFFLINE_TITLE");
      this.gb_date.HeaderText = Resource.String("STR_FRM_LAST_MOVEMENTS_DATAGRID_CARD_DATE");
      this.gb_date.Refresh();
      this.gp_digits_box.Text = Resource.String("STR_FRM_AMOUNT_INPUT_DIGITS_GROUP_BOX");
      this.gb_terminals.HeaderText = Resource.String("STR_UC_TICKET_OFFLINE_GROUP_BOX_TERMINALS");
      this.gb_terminals.Refresh();
      this.lbl_hour.Text = Resource.String("STR_FRM_CONTAINER_EVENTS_DATAGRID_HEADER_DATE");
      this.btn_amount_add.Text = Resource.String("STR_UC_TICKET_OFFLINE_BUTTON_PAY");
      this.btn_gen.Text = Resource.String("STR_UC_TICKET_OFFLINE_BUTTON_CREATE");
      this.lbl_ticked_readed.Text = (Resource.String("STR_UC_TICKETS_04") + ":");

      uc_ticket_reader.InitializeControlResources();

      ResetValues();
    } // InitializeControlResources

    //------------------------------------------------------------------------------
    // PURPOSE:  Restore all values
    // 
    //  PARAMS:
    //
    private void ResetValues()
    {
      DateTime _date;

      _date = new DateTime();
      _date = WGDB.Now;

      this.txt_amount.Text = "0";
      this.lbl_ticket_num.Text = String.Empty;
      this.lbl_ticket_curr.Text = String.Empty;
      this.txt_hour.Text = _date.Hour.ToString("00");
      this.txt_minute.Text = _date.Minute.ToString("00");
      this.txt_second.Text = _date.Second.ToString("00");
      this.lbl_national_amount.Text = String.Empty;

      m_payment_type = Ticket.ENUM_PAYMENT_TYPE.Ticket;
      m_machine_seq_to_pay = 0;
      m_machine_seq = 0;

      ShowTerminalCurrency(-1);

      EnableButtons(false, TITO_VALIDATION_TYPE.STANDARD);
    } // ResetValues

    //------------------------------------------------------------------------------
    // PURPOSE: Parent form restore focus and bring to front
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    private void RestoreParentFocus()
    {
      this.ParentForm.Focus();
      this.ParentForm.BringToFront();
    } // RestoreParentFocus

    //------------------------------------------------------------------------------
    // PURPOSE:  Return Date Time from date controls
    // 
    //  PARAMS:
    //      - OUTPUT: Date (DateTime)
    //
    private Boolean getDate(ref Ticket TicketToPay)
    {
      DateTime _now;
      DateTime _ticket_datetime;

      _now = WGDB.Now;

      try
      {
        _ticket_datetime = new DateTime(_now.Year, _now.Month, _now.Day,
                            Convert.ToInt32(txt_hour.Text), Convert.ToInt32(txt_minute.Text), Convert.ToInt32(txt_second.Text));

        if (_ticket_datetime > _now)
        {
          _ticket_datetime = _ticket_datetime.AddDays(-1);
        }
        TicketToPay.CreatedDateTime = _ticket_datetime;

        return true;
      }
      catch (Exception)
      {
        TicketToPay.CreatedDateTime = _now;
        return false;
      }
    } // getDate

    //------------------------------------------------------------------------------
    // PURPOSE:  Return last sequence number from machine id.
    // 
    //  PARAMS:
    //      - INPUT:
    //            - CashierTerminalId (Int64)
    //            - SqlTrx (SqlTransaction)
    //      - OUTPUT:
    //            - SequenceId (Int64)
    //
    private Boolean DB_LoadParamsForValidationNumber(Int64 CashierTerminalId, out Int64 SequenceId, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      SequenceId = 0;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT   ISNULL(TE_SEQUENCE_ID,0)");
        _sb.AppendLine("   FROM   TERMINALS");
        _sb.AppendLine("  WHERE   TE_TERMINAL_ID = @pCashierTerminalId");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pCashierTerminalId", SqlDbType.Int).Value = CashierTerminalId;

          using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {
              return false;
            }

            SequenceId = _reader.GetInt64(0);

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
    } // DB_LoadTicketType

    private void ProcessRedeemTicket()
    {
      String _error_str;
      _error_str = "";
      List<ProfilePermissions.CashierFormFuncionality> _list_permissions;

      _list_permissions = new List<ProfilePermissions.CashierFormFuncionality>();

      _list_permissions.Add(ProfilePermissions.CashierFormFuncionality.TITO_RedeemTicketOffline);

      if (m_tito_ticket_type == TITO_TICKET_TYPE.PROMO_NONREDEEM)
      {
        _list_permissions.Add(ProfilePermissions.CashierFormFuncionality.TITO_CreatePromoNRTicket);
      }
      else
      {
        _list_permissions.Add(ProfilePermissions.CashierFormFuncionality.TITO_CreateRETicket);
      }

      if (!ProfilePermissions.CheckPermissionList(_list_permissions,
                                                  ProfilePermissions.TypeOperation.RequestPasswd,
                                                  this.ParentForm,
                                                  0,
                                                  out _error_str))
      {
        return;
      }

      if (CheckData())
      {
        RedeemTicket();
        ClearForm();
      }

    } // ProcessRedeemTicket

    //------------------------------------------------------------------------------
    // PURPOSE:  Creates a ticket from controls info, insert in db, generate voucher and insert cashier operation movements.
    // 
    //  PARAMS:
    //      - INPUT:
    //            - Trx (SqlTransaction)
    //
    private void RedeemTicket()
    {
      ParamsTicketOperation _operations_params;
      CardData _virtual_card;
      ENUM_TITO_OPERATIONS_RESULT _result;
      Decimal _amount;
      Ticket _new_ticket;
      Currency _redeemable_amount_to_add;
      Currency _not_redeemable_amount;
      Int64 _operation_id;
      VoucherValidationNumber _voucher_ticket_info;
      ArrayList _voucher_list;
      Int64 _money_collection_id;
      String _terminal_name;
      PrinterStatus _printer_status;
      String _printer_status_text;
      CashierSessionInfo _current_session_info;
      CashierSessionInfo _terminal_session_info;
      VoucherTitoTicketOperation _voucher_temp;

      m_ticket_to_pay = new Ticket();
      _virtual_card = new CardData();
      _new_ticket = null;
      _redeemable_amount_to_add = 0;
      _not_redeemable_amount = 0;
      _voucher_list = new ArrayList();
      _money_collection_id = 0;
      _terminal_name = String.Empty;

      _result = ENUM_TITO_OPERATIONS_RESULT.NONE;

      // Check that the amount is valid
      if (!Decimal.TryParse(txt_amount.Text, out _amount))
      {
        return;
      }

      if (!gb_date.Enabled)
      {
        m_ticket_to_pay.CreatedDateTime = WSI.Common.WGDB.Now;
      }
      else if (!getDate(ref m_ticket_to_pay))
      {
        return;
      }

      m_ticket_to_pay.CreatedTerminalId = uc_terminal_filter.TerminalSelected();

      // DHA 13-FEB-2014 Get the terminal name for generate the voucher when is redeemable
      using (DB_TRX _db_trx = new DB_TRX())
      {
        m_ticket_to_pay.Amt_0 = _amount;
        m_ticket_to_pay.Cur_0 = CurrencyExchange.GetNationalCurrency();
        m_ticket_to_pay.Amount = _amount;
        m_ticket_to_pay.Amt_1 = _amount;
        m_ticket_to_pay.Cur_1 = CurrencyExchange.GetNationalCurrency();

        if (Misc.IsFloorDualCurrencyEnabled())
        {
          m_ticket_to_pay.Amt_0 = _amount;
          m_ticket_to_pay.Cur_0 = lbl_ticket_curr.Text;
          m_ticket_to_pay.Amount = CurrencyExchange.GetExchange(_amount, lbl_ticket_curr.Text, CurrencyExchange.GetNationalCurrency(), _db_trx.SqlTransaction);
          m_ticket_to_pay.Amt_1 = m_ticket_to_pay.Amount;
          m_ticket_to_pay.Cur_1 = CurrencyExchange.GetNationalCurrency();
        }

        if (!Misc.GetTerminalName(uc_terminal_filter.TerminalSelected(), out _terminal_name, _db_trx.SqlTransaction))
        {
          Log.Error(String.Format("GetTerminalName Terminal={0}, TerminalName={1}", uc_terminal_filter.TerminalSelected(), _terminal_name));
          return;
        }
      }

      m_ticket_to_pay.CreatedTerminalName = _terminal_name;

      try
      {
        // Returns the ID of the virtual account by the name of the terminal
        if (!BusinessLogic.LoadVirtualAccount(_virtual_card))
        {
          Log.Error("RedeemTicket: DB_CardGetAllData. Error loading Account data.");

          return;
        }

        m_ticket_to_pay.ValidationNumber = m_ticket_read.ValidationNumber;
        m_ticket_to_pay.Status = TITO_TICKET_STATUS.VALID;
        m_ticket_to_pay.ValidationType = m_ticket_read.ValidationType;
        m_ticket_to_pay.TicketType = TITO_TICKET_TYPE.OFFLINE;
        m_ticket_to_pay.CreatedTerminalType = m_ticket_read.CreatedTerminalType;
        m_ticket_to_pay.CreatedAccountID = _virtual_card.AccountId;
        m_ticket_to_pay.ExpirationDateTime = m_ticket_to_pay.CreatedDateTime.AddDays(Utils.DefaultTicketExpiration(m_tito_ticket_type));

        _voucher_ticket_info = new VoucherValidationNumber();
        _voucher_ticket_info.ValidationNumber = m_ticket_to_pay.ValidationNumber;
        _voucher_ticket_info.Amount = m_ticket_to_pay.Amount;
        _voucher_ticket_info.ValidationType = m_ticket_to_pay.ValidationType;
        _voucher_ticket_info.TicketType = m_tito_ticket_type;
        _voucher_ticket_info.CreatedTerminalName = m_ticket_to_pay.CreatedTerminalName;
        _voucher_ticket_info.CreatedTerminalType = m_ticket_to_pay.CreatedTerminalType;

        _operations_params = new ParamsTicketOperation();
        _operations_params.in_account = _virtual_card;
        _operations_params.in_cash_amt_0 = m_ticket_to_pay.Amt_0;
        _operations_params.in_cash_cur_0 = m_ticket_to_pay.Cur_0;
        _operations_params.in_cash_amount = m_ticket_to_pay.Amount;
        _operations_params.in_cash_amt_1 = m_ticket_to_pay.Amt_1;
        _operations_params.in_cash_cur_1 = m_ticket_to_pay.Cur_1;
        _operations_params.in_window_parent = this.ParentForm;
        _operations_params.session_info = Cashier.CashierSessionInfo();
        _operations_params.ticket_type = TITO_TICKET_TYPE.OFFLINE;
        _operations_params.max_devolution = Utils.MaxDevolutionAmount(m_ticket_to_pay.Amount);
        _operations_params.validation_numbers = new List<VoucherValidationNumber>();
        _operations_params.validation_numbers.Add(_voucher_ticket_info);

        if (m_payment_type == Ticket.ENUM_PAYMENT_TYPE.Redeem)
        {
          PaymentThresholdAuthorization _payment_threshold_authorization;
          m_frm_shadow_gui.Show(Parent);
          _result = BusinessLogic.PreviewVoucherTicketRedeem(ref _operations_params, out _payment_threshold_authorization);
          m_frm_shadow_gui.Hide();

          if (_result != ENUM_TITO_OPERATIONS_RESULT.OK)
          {

            return;
          }

          _result = CashierBusinessLogic.WithholdAndPaymentOrderDialog(OperationCode.TITO_OFFLINE, ref _operations_params,
                                                                       CashierBusinessLogic.EnterWitholdingData,
                                                                       CashierBusinessLogic.EnterOrderPaymentData, null);

          if (_result != ENUM_TITO_OPERATIONS_RESULT.OK)
          {
            return;
          }
        }
        else // ENUM_PAYMENT_TYPE.Ticket
        {
          if (!TitoPrinter.IsReady())
          {
            TitoPrinter.GetStatus(out _printer_status, out _printer_status_text);
            Log.Error("RedeemTicket: Ticket printer error. Printer Status: " + _printer_status_text);
            return;
          }
        }

        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (!TicketAlarmValidation(_db_trx.SqlTransaction))
          {

            return;
          }

          if (m_tito_ticket_type == TITO_TICKET_TYPE.PROMO_NONREDEEM)
          {
            _not_redeemable_amount = _amount;
            _operations_params.ticket_type = TITO_TICKET_TYPE.PROMO_NONREDEEM;
          }
          else if (m_tito_ticket_type == TITO_TICKET_TYPE.CASHABLE)
          {
            // DHA 24-MAR-2016
            if (Misc.IsFloorDualCurrencyEnabled())
            {
              _redeemable_amount_to_add = m_ticket_to_pay.Amount;
            }
            else
            {
              _redeemable_amount_to_add = _amount;
            }

            _operations_params.ticket_type = TITO_TICKET_TYPE.CASHABLE;
          }

          if (!Operations.DB_InsertOperation(OperationCode.TITO_OFFLINE, _operations_params.in_account.AccountId, Cashier.SessionId, 0,
                                             _redeemable_amount_to_add, 0, _not_redeemable_amount, 0, 0,
                                             0,                      // Operation Data
                                             0, string.Empty,
                                             out _operation_id, _db_trx.SqlTransaction))
          {
            return;
          }

          _operations_params.out_operation_id = _operation_id;
          m_ticket_to_pay.TransactionId = _operation_id;

          CashierMovementsTable _cashier_movement;
          AccountMovementsTable _account_movement;
          _cashier_movement = new CashierMovementsTable(_operations_params.session_info);
          _account_movement = new AccountMovementsTable(_operations_params.session_info);

          if (!m_ticket_to_pay.DB_CreateTicket(_db_trx.SqlTransaction, _virtual_card.TrackData, false, m_payment_type, _amount, _cashier_movement, _account_movement))
          {
            Log.Error("ticket_offline: DB_CreateTicket. Error adding Ticket data.");

            return;
          }

          if (!_account_movement.Save(_db_trx.SqlTransaction))
          {
            return;
          }

          if (!_cashier_movement.Save(_db_trx.SqlTransaction))
          {
            return;
          }

          if (m_payment_type == Ticket.ENUM_PAYMENT_TYPE.Redeem)
          {
            List<Int64> _list;
            _list = new List<Int64>();
            _list.Add(m_ticket_to_pay.TicketID);

            _result = BusinessLogic.TicketRedeemCashOut(_list, ref _operations_params, OperationCode.TITO_OFFLINE, _db_trx.SqlTransaction);

            if (_result != ENUM_TITO_OPERATIONS_RESULT.OK)
            {
              Log.Warning(string.Format("RedeemTicket has returned status [{0}]",_result.ToString()));

              return;
            }

            _current_session_info = CommonCashierInformation.CashierSessionInfo();

            try
            {
              _terminal_session_info = Common.Cashier.GetSystemCashierSessionInfo(GU_USER_TYPE.SYS_TITO, _db_trx.SqlTransaction, m_ticket_to_pay.CreatedTerminalName);
              _operations_params.terminal_id = m_ticket_to_pay.CreatedTerminalId;
              _terminal_session_info.TerminalId = m_ticket_to_pay.CreatedTerminalId;
              _terminal_session_info.AuthorizedByUserName = _operations_params.session_info.AuthorizedByUserName;
              _terminal_session_info.AuthorizedByUserId = _operations_params.session_info.AuthorizedByUserId;
              _operations_params.session_info = _terminal_session_info;
              _voucher_temp = new VoucherTitoTicketOperation(_operations_params.in_account.VoucherAccountInfo(), OperationCode.TITO_OFFLINE, false, _operations_params.validation_numbers, PrintMode.Print, _db_trx.SqlTransaction);
              _voucher_temp.Save(_operations_params.out_operation_id, _db_trx.SqlTransaction);
              _voucher_list.Add(_voucher_temp);
            }
            finally
            {
              CommonCashierInformation.SetCashierInformation(_current_session_info.CashierSessionId, _current_session_info.UserId, _current_session_info.UserName, _current_session_info.TerminalId, _current_session_info.TerminalName);
            }
          }
          else
          {
            //Change status from old ticket and create new ticket
            _result = BusinessLogic.TicketReprint(m_ticket_to_pay.TicketID, _operations_params, true, this.ParentForm, out _new_ticket, out _voucher_list, _db_trx.SqlTransaction);

            if (_result != ENUM_TITO_OPERATIONS_RESULT.OK)
            {
              Log.Error("CreateTicket: TicketReprint. Error adding operations ticket data.");

              return;
            }

            //Out collection Id
            if (!MoneyCollection.GetCashierMoneyCollectionId(Cashier.SessionId, out _money_collection_id, _db_trx.SqlTransaction))
            {
              Log.Error("ReedemTicketList: Error getting collection id. Session Id:" + Cashier.SessionId);
              return;
            }

            //Set Money Collection ID
            if (!Ticket.DB_ForceOfflineTicketMoneyCollection(m_ticket_to_pay.TicketID, (Int64)_money_collection_id, _db_trx.SqlTransaction))
            {

              return;
            }

          }//m_payment_type

          _db_trx.Commit();

        } //DB_TRX _db_trx

        if (CashierBusinessLogic.GLB_OperationIdForPrintDocs > 0)
        {
          // Select the printer and print the Witholding documents
          PrintingDocuments.PrintDialogDocuments(CashierBusinessLogic.GLB_OperationIdForPrintDocs);
        }

        if (_new_ticket != null)
        {
          if (!TitoPrinter.PrintTicket(_new_ticket.ValidationNumber, _new_ticket.MachineTicketNumber, _new_ticket.TicketType, _new_ticket.Amount, _new_ticket.CreatedDateTime, _new_ticket.ExpirationDateTime, _operations_params.session_info))
          {
            Log.Error("TicketReprint: PrintTicket. Error on printing ticket.");

            _result = ENUM_TITO_OPERATIONS_RESULT.PRINTER_PRINT_FAIL;

            using (DB_TRX _trx = new DB_TRX())
            {
              if (BusinessLogic.UndoTicketReissue(_virtual_card, m_ticket_to_pay.TicketID, _operation_id, OperationCode.TITO_OFFLINE, _trx.SqlTransaction))
              {
                _result = ENUM_TITO_OPERATIONS_RESULT.PRINTER_IS_NOT_READY;

                _trx.Commit();
              }
            }

            return;
          }
        }

        if (_voucher_list.Count > 0)
        {
          VoucherPrint.Print(_voucher_list);
        }
      }
      catch (Exception Ex)
      {
        Log.Exception(Ex);
        _result = ENUM_TITO_OPERATIONS_RESULT.AUX_OPERATION_ERROR;
        return;
      }
      finally
      {
        switch (_result)
        {
          case ENUM_TITO_OPERATIONS_RESULT.OK:
            if (m_payment_type == Ticket.ENUM_PAYMENT_TYPE.Ticket)
            {
              frm_message.Show(Resource.String("STR_UC_TITO_OFFLINE_TICKET_CREATED"), Resource.String("STR_APP_GEN_MSG_WARNING"), MessageBoxButtons.OK, MessageBoxIcon.Information, this.ParentForm);
            }
            break;
          case ENUM_TITO_OPERATIONS_RESULT.AUX_OPERATION_CANCELED:
            break;

          case ENUM_TITO_OPERATIONS_RESULT.PRINTER_PRINT_FAIL:
          case ENUM_TITO_OPERATIONS_RESULT.PRINTER_IS_NOT_READY:
            PrinterStatus _status;
            String _status_text;

            TitoPrinter.GetStatus(out _status, out _status_text);
            _status_text = TitoPrinter.GetMsgError(_status);

            if (_result == ENUM_TITO_OPERATIONS_RESULT.PRINTER_PRINT_FAIL)
            {
              _status_text += "\r\n" + Resource.String("STR_FRM_CONTAINER_EVENTS_DESC_PRINTER_CANCEL_OPERATION");
            }

            frm_message.Show(_status_text,
                             Resource.String("STR_APP_GEN_MSG_ERROR"),
                             MessageBoxButtons.OK,
                             MessageBoxIcon.Error,
                             this.ParentForm);
            break;

          case ENUM_TITO_OPERATIONS_RESULT.TICKET_NOT_VALID:
            frm_message.Show(Resource.String("STR_TITO_TICKET_ERROR_PAYING_TICKET"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this.ParentForm);
            break;

          default:
            Log.Warning(string.Format("The redeem of the ticket has returned [{0}]", _result.ToString()));
            frm_message.Show(Resource.String("STR_TITO_TICKET_ERROR_PAYING_TICKET"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this.ParentForm);
            break;
        }

      }
    } // RedeemTicket

    //------------------------------------------------------------------------------
    // PURPOSE:  Validates the input ticket and the ticket created by the control info. If something wrong, insert alarm.
    // 
    //  PARAMS:
    //      - INPUT:
    //            - Trx (SqlTransaction)
    //
    private Boolean TicketAlarmValidation(SqlTransaction Trx)
    {
      CashierSessionInfo _session_info;
      String _cashier_info;

      _cashier_info = String.Empty;
      _session_info = Cashier.CashierSessionInfo();

      DB_LoadParamsForValidationNumber(m_ticket_to_pay.CreatedTerminalId, out m_machine_seq_to_pay, Trx);

      _cashier_info = _session_info.TerminalName + "@" + _session_info.UserName;

      try
      {
        if (m_ticket_read.ValidationType == TITO_VALIDATION_TYPE.STANDARD)
        {
          Int64 _validation_number;

          ValidationNumberManager.EncryptStandard(m_ticket_to_pay.Amount, m_ticket_to_pay.CreatedDateTime, out _validation_number);

          m_ticket_read.ValidationNumber = _validation_number;

          // DHA 17-FEB-2014: Added alarm to Ticket TITO Standard if amount & time validation not match with validation number entered
          if (m_ticket_read.ValidationNumber != m_ticket_to_pay.ValidationNumber)
          {
            if (!GenerateAlarm("STR_UC_TICKET_OFFLINE_ALARM_05", _session_info, _cashier_info, Trx))
            {

              return false;
            }
          }
        } //m_ticket_readed.ValidationType == TITO_VALIDATION_TYPE.STANDARD
        else if (m_ticket_read.ValidationType == TITO_VALIDATION_TYPE.SECURITY_ENHANCED)
        {
          if (m_ticket_read.CreatedTerminalId != m_ticket_to_pay.CreatedTerminalId)
          {
            if (!GenerateAlarm("STR_UC_TICKET_OFFLINE_ALARM_02", _session_info, _cashier_info, Trx))
            {

              return false;
            }
          }

          //if (!((m_machine_seq_to_pay <= m_machine_seq) && (m_machine_seq_to_pay >= (m_machine_seq - 5))))
          //{
          //  if (!GenerateAlarm("STR_UC_TICKET_OFFLINE_ALARM_01", _session_info, _cashier_info, Trx))
          //  {

          //    return false;
          //  }
          //}
        }

        return true;
      }
      catch (Exception)
      {

        return false;
      }
    } // TicketAlarmValidation

    private Boolean GenerateAlarm(String AlarmNLS, CashierSessionInfo _session_info, String _cashier_info, SqlTransaction Trx)
    {
      StringBuilder _description;
      String _description_payment_type;

      if (m_payment_type == Ticket.ENUM_PAYMENT_TYPE.Redeem)
      {
        _description_payment_type = "STR_UC_TICKET_OFFLINE_ALARM_REDEEM_ORIGIN";      // Offline ticket - Redeemable: @p0
      }
      else
      {
        _description_payment_type = "STR_UC_TICKET_OFFLINE_ALARM_NON_REDEEM_ORIGIN"; // Offline ticket - Playable ticket: @p0
      }

      _description = new StringBuilder();
      _description.Append(Resource.String(_description_payment_type, Resource.String(AlarmNLS)));
      _description.Append(", " + Resource.String("STR_UC_TICKETS_04") + ": " + ValidationNumberManager.FormatValidationNumber(m_ticket_to_pay.ValidationNumber, (Int32)m_ticket_to_pay.ValidationType, true));
      _description.Append(", " + Resource.String("STR_UC_TICKETS_08") + ": " + uc_terminal_filter.ProviderSelected().ToString() + " " + uc_terminal_filter.TerminalSelectedName().ToString());
      _description.Append(", " + Resource.String("STR_UC_TICKETS_09") + ": " + m_ticket_to_pay.CreatedDateTime);
      _description.Append(", " + Resource.String("STR_UC_TICKETS_06") + ": " + ((Currency)m_ticket_to_pay.Amount).ToString(true));

      if (!Alarm.Register(AlarmSourceCode.User,
                          _session_info.TerminalId,
                          _cashier_info,
                          (UInt32)AlarmCode.User_TITO_TicketOffline,
                          _description.ToString(),
                          AlarmSeverity.Warning,
                          DateTime.MinValue,
                          Trx))
      {

        return false;
      }

      return true;
    } // GenerateAlarm

    //------------------------------------------------------------------------------
    // PURPOSE:  CheckData
    // 
    //
    private Boolean CheckData()
    {
      Int32 _wrong;

      if (txt_amount.Text == "0" || String.IsNullOrEmpty(txt_amount.Text))
      {
        frm_message.Show(Resource.String("STR_FRM_AMOUNT_INPUT_MSG_ZERO_AMOUNT"), Resource.String("STR_APP_GEN_MSG_ERROR"),
                         MessageBoxButtons.OK, MessageBoxIcon.Error, this.ParentForm);
        this.txt_amount.Focus();

        return false;
      }

      if (!CustomDate.ValidateTime(txt_hour.Text.Trim(), txt_minute.Text.Trim(), txt_second.Text.Trim(), out _wrong))
      {
        switch (_wrong)
        {
          case 1:
            txt_hour.Focus();
            txt_hour.Select();
            break;
          case 2:
            txt_minute.Focus();
            txt_minute.Select();
            break;
          case 3:
            txt_second.Focus();
            txt_second.Select();
            break;
          default:
            break;
        }
        frm_message.Show(Resource.String("STR_UC_TICKET_OFFLINE_DATE"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK,
                         MessageBoxIcon.Error, this.ParentForm);


        return false;
      }

      if (uc_terminal_filter.TerminalSelected() == 0)
      {
        frm_message.Show(Resource.String("STR_UC_TICKET_OFFLINE_GENERIC_SELECT", Resource.String("STR_VOUCHER_TERMINAL_ID").ToLower()),
                         Resource.String("STR_APP_GEN_MSG_ERROR"),
                         MessageBoxButtons.OK, MessageBoxIcon.Error, this.ParentForm);
        this.uc_terminal_filter.Focus();

        return false;
      }

      if (m_ticket_read.ValidationType == TITO_VALIDATION_TYPE.SYSTEM && m_ticket_read.CreatedTerminalType == (Int32)TITO_TERMINAL_TYPE.CASHIER)
      {
        frm_message.Show(Resource.String("STR_UC_TICKET_OFFLINE_NOT_FROM_TERMINAL"), Resource.String("STR_APP_GEN_MSG_ERROR"),
                         MessageBoxButtons.OK, MessageBoxIcon.Error, this.ParentForm);
        this.uc_terminal_filter.Focus();

        return false;
      }

      if (!IsTicketOffline(m_ticket_read.ValidationNumber))
      {
        frm_message.Show(Resource.String("STR_UC_TICKET_OFFLINE_IS_ONLINE"), Resource.String("STR_APP_GEN_MSG_ERROR"),
                         MessageBoxButtons.OK, MessageBoxIcon.Error, this.ParentForm);
        this.uc_ticket_reader.Focus();

        return false;
      }

      return true;
    } // CheckData

    //------------------------------------------------------------------------------
    // PURPOSE:  Return if it's a terminal or a cashier.
    // 
    //  PARAMS:
    //      - INPUT:
    //            - CashierTerminalId (Int32)
    //            - SqlTrx (SqlTransaction)
    //      - OUTPUT:
    //            - IsTerminal (Boolean)
    //
    private Boolean IsTerminal(Int32 CashierTerminalId)
    {
      StringBuilder _sb;
      Object _obj;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT   TE_TERMINAL_ID");
        _sb.AppendLine("   FROM   TERMINALS");
        _sb.AppendLine("  WHERE   TE_TERMINAL_ID = @pCashierTerminalId");
        _sb.AppendLine("    AND   TE_TYPE = @pType");
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pCashierTerminalId", SqlDbType.Int).Value = CashierTerminalId;
            _sql_cmd.Parameters.Add("@pType", SqlDbType.Int, 4, "TE_TYPE").Value = (Int32)WCP_TerminalType.GamingTerminal;
            _obj = _sql_cmd.ExecuteScalar();
          }
          if (_obj != null && _obj != DBNull.Value)
          {

            return true;
          }
          else
          {

            return false;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
    } // IsTerminal

    //------------------------------------------------------------------------------
    // PURPOSE:  Return if it's a validation numbert exists in the DB.
    // 
    //  PARAMS:
    //      - INPUT:
    //            - ValidationNumber (Int64)
    //            - SqlTrx (SqlTransaction)
    //      - OUTPUT:
    //            - IsTicketOffline (Boolean)
    //
    private Boolean IsTicketOffline(Int64 ValidationNumber)
    {
      StringBuilder _sb;
      Object _obj;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT   TI_TICKET_ID");
        _sb.AppendLine("   FROM   TICKETS");
        _sb.AppendLine("  WHERE   TI_VALIDATION_NUMBER = @pValidationNumber");
        _sb.AppendLine("    AND   TI_CREATED_TERMINAL_TYPE IN (@pTerminalType, @pCashierType)");
        _sb.AppendLine("    AND  ( TI_CREATED_DATETIME IS NULL OR DATEDIFF(HOUR, TI_CREATED_DATETIME, GETDATE()) < = @pMaxHoursIsOffline ) ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pValidationNumber", SqlDbType.BigInt).Value = ValidationNumber;
            _sql_cmd.Parameters.Add("@pTerminalType", SqlDbType.Int).Value = TITO_TERMINAL_TYPE.TERMINAL;
            _sql_cmd.Parameters.Add("@pCashierType", SqlDbType.Int).Value = TITO_TERMINAL_TYPE.CASHIER;
            _sql_cmd.Parameters.Add("@pMaxHoursIsOffline", SqlDbType.Int).Value = MAX_HOURS_IS_OFFLINE;
            _obj = _sql_cmd.ExecuteScalar();
          }
          if (_obj != null && _obj != DBNull.Value)
          {

            return false;
          }
          else
          {

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
    } // IsTicketOffline

    #region Buttons

    //
    //------------------------------------------------------------------------------
    // PURPOSE:  Add amount
    // 
    //  PARAMS:
    //      - INPUT:
    //            - NumberStr (String)
    //
    private void AddNumber(String NumberStr)
    {
      String aux_str;
      String tentative_number;
      Currency input_amount;
      Int32 dec_symbol_pos;

      // Prevent exceeding maximum number of decimals (2)
      dec_symbol_pos = txt_amount.Text.IndexOf(m_decimal_str);
      if (dec_symbol_pos > -1)
      {
        aux_str = txt_amount.Text.Substring(dec_symbol_pos);
        if (aux_str.Length > 2)
        {
          return;
        }
      }

      // Prevent exceeding maximum amount length
      if (txt_amount.Text.Length >= Cashier.MAX_ALLOWED_AMOUNT_LENGTH)
      {
        return;
      }

      tentative_number = txt_amount.Text + NumberStr;

      try
      {
        input_amount = Decimal.Parse(tentative_number);
        if (input_amount > Cashier.MAX_ALLOWED_AMOUNT)
        {
          return;
        }
      }
      catch
      {
        input_amount = 0;

        return;
      }

      // Remove unneeded leading zeros
      // Unless we are entering a decimal value, there must not be any leading 0
      if (dec_symbol_pos == -1)
      {
        txt_amount.Text = txt_amount.Text.TrimStart('0') + NumberStr;
      }
      else
      {
        // Accept new symbol/digit 
        txt_amount.Text = txt_amount.Text + NumberStr;
      }

      ShowTerminalCurrency(uc_terminal_filter.TerminalSelected());

      btn_amount_add.Focus();
    } // AddNumber

    private void btn_num_1_Click(object sender, EventArgs e)
    {
      AddNumber("1");
    } // btn_num_1_Click

    private void btn_num_2_Click(object sender, EventArgs e)
    {
      AddNumber("2");
    } // btn_num_2_Click

    private void btn_num_3_Click(object sender, EventArgs e)
    {
      AddNumber("3");
    } // btn_num_3_Click

    private void btn_num_4_Click(object sender, EventArgs e)
    {
      AddNumber("4");
    } // btn_num_4_Click

    private void btn_num_5_Click(object sender, EventArgs e)
    {
      AddNumber("5");
    } // btn_num_5_Click

    private void btn_num_6_Click(object sender, EventArgs e)
    {
      AddNumber("6");
    } // btn_num_6_Click

    private void btn_num_7_Click(object sender, EventArgs e)
    {
      AddNumber("7");
    } // btn_num_7_Click

    private void btn_num_8_Click(object sender, EventArgs e)
    {
      AddNumber("8");
    } // btn_num_8_Click

    private void btn_num_9_Click(object sender, EventArgs e)
    {
      AddNumber("9");
    } // btn_num_9_Click

    private void btn_num_10_Click(object sender, EventArgs e)
    {
      AddNumber("0");
    } // btn_num_10_Click

    private void btn_num_dot_Click(object sender, EventArgs e)
    {
      // Only one decimal separator is allowed
      if (txt_amount.Text.IndexOf(m_decimal_str) == -1)
      {
        txt_amount.Text = txt_amount.Text + m_decimal_str;
      }

      btn_intro.Focus();
    } // btn_num_dot_Click

    private void btn_back_Click(object sender, EventArgs e)
    {
      if (txt_amount.Text.Length > 0)
      {
        txt_amount.Text = txt_amount.Text.Substring(0, txt_amount.Text.Length - 1);
      }

      ShowTerminalCurrency(uc_terminal_filter.TerminalSelected());

      btn_amount_add.Focus();
    } // btn_back_Click

    #endregion

    #endregion

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE: Init controls and events
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    public void InitControls()
    {
      StringBuilder _where_condition;

      _where_condition = new StringBuilder();

      ClearForm();

      if (m_first_time)
      {
        this.uc_ticket_reader.OnValidationNumberReadEvent += new uc_ticket_reader.ValidationNumberReadEventHandler(ProcessTicketNumber);

        m_first_time = false;

        InitializeControlResources();
      }
      EnableButtons(false, TITO_VALIDATION_TYPE.STANDARD);

      _where_condition.AppendLine(" WHERE TE_TERMINAL_TYPE = " + (Int32)TerminalTypes.SAS_HOST);
      _where_condition.AppendLine(String.Format("      AND (  TE_RETIREMENT_DATE IS NULL OR DATEDIFF(HOUR, TE_RETIREMENT_DATE, GETDATE()) <= {0} ) ", MAX_HOURS_IS_OFFLINE));
      uc_terminal_filter.InitControls("", _where_condition.ToString(), true, true);

    } // InitControls

    #endregion

    #region Event Controls

    private void btn_credit_add_Click(object sender, EventArgs e)
    {
      m_payment_type = Ticket.ENUM_PAYMENT_TYPE.Redeem;
      m_tito_ticket_type = TITO_TICKET_TYPE.CASHABLE;

      ProcessRedeemTicket();
    } // btn_credit_add_Click

    private void btn_gen_Click(object sender, EventArgs e)
    {
      m_payment_type = Ticket.ENUM_PAYMENT_TYPE.Ticket;
      m_tito_ticket_type = TITO_TICKET_TYPE.PROMO_NONREDEEM;

      ProcessRedeemTicket();
    } // btn_gen_Click

    private void uc_terminal_offline_KeyPress(object sender, KeyPressEventArgs e)
    {
      String aux_str;
      char c;

      c = e.KeyChar;

      if (c >= '0' && c <= '9')
      {
        aux_str = "" + c;
        AddNumber(aux_str);

        e.Handled = true;
      }

      if (c == '\r')
      {
        btn_credit_add_Click(null, null);

        e.Handled = true;
      }

      if (c == '\b')
      {
        btn_back_Click(null, null);

        e.Handled = true;
      }

      if (c == '.')
      {
        btn_num_dot_Click(null, null);

        e.Handled = true;
      }

      e.Handled = false;
    }// uc_terminal_offline_KeyPress

    protected override void OnLayout(LayoutEventArgs e)
    {
      base.OnLayout(e);
      this.BackColor = WSI.Cashier.CashierStyle.Colors.m_colors_dic[CashierStyle.Colors.ENUM_COLORS.HEX_E3E7E9];
    }

    private void uc_terminal_filter_TerminalChanged()
    {
      ShowTerminalCurrency(uc_terminal_filter.TerminalSelected());
    }

    #endregion

  }
}
