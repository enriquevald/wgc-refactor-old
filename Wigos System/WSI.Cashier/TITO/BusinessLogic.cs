//------------------------------------------------------------------------------
// Copyright � 2007-2013 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: BusinessLogic.cs
// 
//   DESCRIPTION: Declare all functions and procedures related to TITO Business Logic
//
//        AUTHOR: Nelson Madrigal
//
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 05-AUG-2013 NMR    First release.
// 13-SEP-2013 NMR    Added procedure for TITO-ticket reprint
// 18-SEP-2013 NMR    Rearranged movements created in all In-Out operations
// 19-SEP-2013 NMR    New promotional ticket print
// 25-SEP-2013 NMR    Revision of Cancel ticket operations
// 01-OCT-2013 NMR    Merging/comparing with class CashierBusinessLogic
// 03-OCT-2013 NMR    During ticket-out must be TOTAL Redeem
// 10-OCT-2013 NMR    Added ticket reprint audit
// 28-OCT-2013 LEM    Modify routines to CashIn-CreateTicket and TicketRedeem-CashOut funcionalities 
//                    using common cashier and account movements and adding Creation and Redeemption movements.
//                    Added routines to simulate Creation and Redeemtion for PreviewVoucher actions for amount input form.
// 30-OCT-2013 LEM    Modify routine for TicketReprint to use new routines for Create and Cancel tickets. 
// 13-DEC-2013 ICS    Added function to get ticket type name string
// 10-JAN-2014 DHA    Fixed Bug WIGOSTITO-966: reprinting non redeemable ticket does not set right non redeemable balance
// 13-FEB-2014 DHA    Fixed Buf WIGOSTITO-1068 promotions from cashier now are cancelled when are applied
// 24-FEB-2014 DRV    Fixed Bugs WIGOSTITO-1089, WIGOSTITO-1088: Cancellation of tickets with corresponding offline
// 26-FEB-2014 XIT    Deleted SmartParams uses
// 07-MAR-2014 QMP & DDM & DHA Fixed bug WIGOSTITO-1131: Created Cash/Account movements have the same Operation ID
// 20-MAR-2014 ICS    Fixed Bug WIGOSTITO-1161: A recharge with a selected promotion using currency exchange gives an error.
// 25-MAR-2014 XIT & LEM & DRV Fixed Bug WIGOSTITO-1162: Currency exchange promotions doesn't update his status when ticket is generated 
// 16-APR-2014 ICS, MPO & RCI    Fixed Bug WIG-830: Not enough available storage exception.
// 25-APR-2014 DHA & JML Rename TITO Cashier/Account Movements
// 26-JUN-2014 LEM    Fixed Bug WIG-1052: CashDraw in TITO Mode: Always participates, even selecting confirmation option NO.
// 08-SEP-2014 JCO    Added call to ProgressiveJackpotPaid().
// 16-OCT-2014 DRV    Fixed Bug WIGOSTITO-1254: If cupon Cover it's enabled it's ticket is not generated.
// 28-OCT-2014 MPO    WIG-1582/WIG-1581 When Ticket is redeemed with payment order doesn't show it and "resultado de caja" it's wrong
// 11-NOV-2014 LEM    Fixed Bug WIG-1674: Ticket Reprint: Don't restore cashier session information after voucher generation.
// 02-FEB-2015 MPO    WIG-1969: System crash when printing "constancias"
// 30-JUN-2015 MPO    WIG-2492: Get and set account promo id for calculate cost (Refactor vars)
// 13-JUL-2015 DLL    WIG-2559: Status ticket is not showed
// 11-NOV-2015 FOS    Product Backlog Item: 3709 Payment tickets & handpays
// 23-NOV-2015 FOS    Bug 6771:Payment tickets & handpays
// 27-NOV-2015 SGB    Product Backlog Item 4713: Change functions & structs to Common.Cashier
// 03-FEB-2016 DHA    Bug 8932: on ticket TITO insert in BD field "TI_AMT0" and "TI_CUR0" are with wrong value
// 08-FEB-2016 RGR    Product Backlog Item 6462: Garcia River-08: Cash Promotions NR: Print Ticket TITO or assign account
// 23-FEB-2016 RAB    Product Backlog Item 9887:TITO: Pagar tickets en estado PENDING_CANCEL (Cajero)
// 19-APR-2016 ETP    Fixed Bug 12159: PendingCancell.AllowToReedem allows to pays Non-Redeemeble Tickets.
// 29-MAR-2016 RGR    Product Backlog Item 10981:Sprint Review 21 Winions - Mex
// 04-APR-2016 RGR    Product Backlog Item 11018:UNPLANNED - Temas varios Winions - Mex Sprint 22
// 05-APR-2016 RGR    Bug 11442: TITA - Ticket Payments the number of tickets is canceled by Redeemed Chips
// 05-APR-2016 RGR    Bug 11453: TITA - Ticket Validation - NOT the ticket status is updated Redeems for chips
// 13-JUN-2016 ETP    Bug 14463: Cajero: Pago de tickets da error cuando queremos pagar m�s de un ticket o creamos una orden pago.
// 19-SEP-2016 FGB    PBI 15985: Exped System: Cashier: Check if the payment/sale can be done
// 20-SEP-2016 FJC    Bug 17669:Anular retiro de caja desde b�veda/Cajero: no deshace el stock y conceptos de b�veda
// 29-SEP-2016 FJC    Fixed Bug (Reopened) 17669:Anular retiro de caja desde b�veda/Cajero: no deshace el stock y conceptos de b�veda
// 30-SEP-2016 ETP    Bug 18003: Cashier: los vouchers de recarga no reflejan los cambios solicitados por Televisa
// 11-OCT-2016 ETP    Bug 18845: NR promotions are assigned to account and tito printed.
// 21-OCT-2016 ETP    Bug 19314: PinPad Card Payment Refactorized
// 24-OCT-2016 ETP    Fixed Bug 19314 Pin Pad recharge refactoring
// 26-OCT-2016 ETP    Fixed Bug 19314 Pin Pad recharge Wigos Rollback + Pinpad + Wigos Commit
// 08-NOV-2016 ETP    Fixed Bug Bug 20035: Exception out of memory when pinpad is used.
// 18-OCT-2016 FAV    PBI 20407: TITO Ticket: Calculated fields
// 31-JAN-2017 ATB    Bug 21719:CountR is applying taxes when it never owns
// 13-FEB-2017 JML    Fixed Bug 24422: Cashier: Incorrect withdrawal and closing with bank card
// 20-FEB-2017 CCG    Bug 24817:Cajero: No se imprimen tickets TITO desde el bot�n "Crear tickets"
// 22-MAR-2017 DPC    PBI 25788:CreditLine - Get a Marker
// 24-MAR-2017 ETP    WIGOS-110: Creditlines - Paybacks
// 29-MAR-2017 DPC    WIGOS-685: CreditLine - Undo last operation "Get Marker"
// 21-APR-2017 FAV    Bug 26704: Error al asignar promociones a la cuenta
// 27-APR-2017 DHA    Bug 26735:WTA-350: wait till cut manual paper on printer
// 04-MAY-2017 DPC    WIGOS-1574: Junkets - Flyers - New field "Text in Promotional Voucher" - Change voucher
// 11-MAY-2017 RGR    Bug 26907:Ticket counter going negative in cashier session
// 21-MAY-2017 ETP    Fixed Bug WIGOS-2997: Added voucher data.
// 08-MAY-2017 MS     PBI 27045: Junket Vouchers
// 09-OCT-2017 JML    PBI 30022:WIGOS-4052 Payment threshold registration - Recharge with Check payment
// 10-OCT-2017 JML    PBI 30023:WIGOS-4068 Payment threshold registration - Transaction information
// 19-OCT-2017 JML    Fixed Bug 30321:WIGOS-5924 [Ticket #9450] F�rmula datos tarjeta bancaria (entregado-faltante) - Resumen de sesi�n de caja
// 24-OCT-2017 RAB    Bug 30381:WIGOS-6034 Threshold: Log error when creating ticket TITO without threshold for deposit
// 31-OCT-2017 RAB    Bug 30488:WIGOS-6058 Threshold: missing check data screen when exceeding input threshold
// 13-NOV-2017 DHA    Bug 30775:WIGOS-6110 [Ticket #9672] MTY I - Plaza Real - PinPad con error y autorizadas
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Windows.Forms;
using WSI.Common;
using WSI.Common.TITO;
using System.ComponentModel;
using WSI.Common.PinPad;
using WSI.Common.CreditLines;
using WSI.Cashier.JunketsCashier;
using WSI.Common.Junkets;

namespace WSI.Cashier.TITO
{
  public class TicketCreationParams
  {
    //Used in single and multiple tickets creation
    public Currency OutCashAmount;
    public Int64 OutPromotionId;
    public Currency OutNotRedeemableAmount;

    //Used in multiple tickets creation
    public ACCOUNT_PROMO_CREDIT_TYPE OutPromotionType;
    public Currency PromoRewardAmount;
    public Currency PromoWonLock;
    public Int32 QuantityOfPromotions;
    public Currency UnitaryReward;
    public String PromoRewardValue; //Used as information showed
    public String PromoName;        //Used as information showed

    //Used in single tickets creation
    public CurrencyExchangeResult OutExchangeResult;
    public Currency OutNotRedeemableWonLock;
    public Currency OutSpentUsed;

    public ParticipateInCashDeskDraw OutParticipateInCashdeskDraw;

    public JunketsShared JunketInfo;

    public PaymentThresholdAuthorization PaymentThresholdAuthorization;
  }

  class BusinessLogic
  {
    #region Properties

    //------------------------------------------------------------------------------
    // PURPOSE: Indicates if Cashier function mode is TITO
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    //  RETURNS:
    //      - True: If mode is TITO; False, otherwise
    //
    public static Boolean IsModeTITO
    {
      get { return WSI.Common.TITO.Utils.IsTitoMode() || Misc.IsWassMode(); }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Indicates if Cashier status is OPEN
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    //  RETURNS:
    //      - True: If cashier is open; False, otherwise
    //
    public static Boolean IsTITOCashierOpen
    {
      get
      {
        if (IsModeTITO || Misc.IsGamingHallMode())
        {
          return (CashierBusinessLogic.ReadCashierStatus(Cashier.SessionId) == CASHIER_STATUS.OPEN);
        }

        return false;
      }
    }

    #endregion

    //------------------------------------------------------------------------------
    // PURPOSE: Redeem a list of valid tickets and create global movements and operation voucher.
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    //  RETURNS:
    //      - True: All went OK; False: otherwise
    //
    public static ENUM_TITO_OPERATIONS_RESULT TicketRedeemCashOut(List<Int64> ListTicket, ParamsTicketOperation OperationParams, OperationCode OpCode, PaymentThresholdAuthorization PaymentThresholdAuthorization)
    {
      ENUM_TITO_OPERATIONS_RESULT _result;

      using (DB_TRX _db_trx = new DB_TRX())
      {
        _result = TicketRedeemCashOut(ListTicket, ref OperationParams, OpCode, _db_trx.SqlTransaction);
        if (_result == ENUM_TITO_OPERATIONS_RESULT.OK)
        {
          if (CashierMovementsGrouped.BySessionID(OperationParams.session_info.CashierSessionId, _db_trx.SqlTransaction))
          {

            if (PaymentThresholdAuthorization != null && PaymentThresholdAuthorization.OutputThreshold > 0 && 
              OperationParams.in_cash_amount >= PaymentThresholdAuthorization.OutputThreshold)
            {
              if (OperationParams.payment_order != null)
            {
              PaymentThresholdAuthorization.PaymentAndRechargeThresholdData.CashAmount = OperationParams.payment_order.CashPayment;
              if (OperationParams.payment_order.CheckPayment > 0)
              {
                PaymentThresholdAuthorization.PaymentAndRechargeThresholdData.TransactionType = CurrencyExchangeType.CHECK;
                PaymentThresholdAuthorization.PaymentAndRechargeThresholdData.TransactionSubType = OperationParams.payment_order.CheckType;
              }
              }
              PaymentThresholdAuthorization.SaveThresholdOperation(OperationParams.out_operation_id, OpCode, _db_trx.SqlTransaction);
            }

          _db_trx.Commit();

        }
          else
          {
            _result = ENUM_TITO_OPERATIONS_RESULT.GENERIC_ERROR;

      }
      }
      }

      if (CashierBusinessLogic.GLB_OperationIdForPrintDocs > 0)
      {
        // Select the printer and print the Witholding documents
        PrintingDocuments.PrintDialogDocuments(CashierBusinessLogic.GLB_OperationIdForPrintDocs);
      }

      return _result;
    }

    public static ENUM_TITO_OPERATIONS_RESULT TicketRedeemCashOut(List<Int64> ListTicket, ref ParamsTicketOperation OperationParams, OperationCode OpCode,
                                                                  SqlTransaction Trx)
    {
      ArrayList _handpay_vouchers;
      String _error_msg;
      ArrayList _voucher_list;

      _voucher_list = new ArrayList();

      if (!BusinessLogic.RedeemTicketList(ListTicket, OpCode, ref OperationParams, out _handpay_vouchers, Trx))
      {
        return ENUM_TITO_OPERATIONS_RESULT.AUX_OPERATION_ERROR;
      }

      if (!CashierBusinessLogic.DB_CardCreditRedeem(OperationParams.out_operation_id,
                                                    OperationParams.in_account.AccountId,
                                                    OperationParams.withhold,
                                                    OperationParams.payment_order,
                                                    OperationParams.in_cash_amount,
                                                    CASH_MODE.TOTAL_REDEEM,
                                                    OperationParams.validation_numbers,
                                                    OpCode,
                                                    Trx,
                                                    out _error_msg,
                                                    OperationParams.is_apply_tax,
                                                    out _voucher_list))
      {
        Log.Error("TicketRedeemCashOut: DB_CardCreditRedeem. Error subtracting all credit to card.");

        return ENUM_TITO_OPERATIONS_RESULT.AUX_OPERATION_ERROR;
      }
      if (_handpay_vouchers != null && _handpay_vouchers.Count > 0)
      {
        _voucher_list.AddRange(_handpay_vouchers);
      }

      VoucherPrint.Print(_voucher_list);

      return ENUM_TITO_OPERATIONS_RESULT.OK;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Show amount input form in specific mode to accept cash out info for redemption ticket
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    //  RETURNS:
    //      - True: All went OK; False: otherwise
    //
    public static ENUM_TITO_OPERATIONS_RESULT PreviewVoucherTicketRedeem(ref ParamsTicketOperation OperationParams, out PaymentThresholdAuthorization PaymentThresholdAuthorization)
    {
      frm_amount_input _frm_input_amount;
      ENUM_TITO_OPERATIONS_RESULT _result;

      _frm_input_amount = new frm_amount_input(((frm_container)OperationParams.in_window_parent).uc_ticket_create1.CheckPermissionsToRedeem, null);
      _result = _frm_input_amount.ShowForCashout(ref OperationParams, out PaymentThresholdAuthorization);
      _frm_input_amount.Dispose();

      return _result;
    }


    //------------------------------------------------------------------------------
    // PURPOSE: Show amount input form in specific mode to accept cash in info for creation ticket
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    //  RETURNS:
    //      - True: All went OK; False: otherwise
    //
    public static ENUM_TITO_OPERATIONS_RESULT PreviewVoucherCreateTicket(ParamsTicketOperation OperationParams, out TicketCreationParams Params)
    {
      frm_amount_input _frm_input_amount;
      Boolean _result;

      _frm_input_amount = new frm_amount_input(((frm_container)OperationParams.in_window_parent).uc_ticket_create1.CheckPermissionsToRedeem, null);
      _result = _frm_input_amount.ShowForCashin(out Params, OperationParams.in_account, OperationParams.in_window_parent);
      _frm_input_amount.Dispose();



      if (!_result)
      {
        return ENUM_TITO_OPERATIONS_RESULT.AUX_OPERATION_CANCELED;
      }

      return ENUM_TITO_OPERATIONS_RESULT.OK;
    }

    public static ENUM_TITO_OPERATIONS_RESULT AsignPromotion(ParamsTicketOperation OperationParams, TicketCreationParams Params, DataTable PromotionTable, TitoPrinter.TITO_PRINTER_PRINT_MODE TitoPrintMode, out Ticket TicketPromo, SqlTransaction SqlTrx)
    {
      return AsignPromotion(OperationParams, Params, PromotionTable, TitoPrintMode, out TicketPromo, SqlTrx, ENUM_OPERATION_MODE_PROMOTIONS_NONREDEM.CREATE_TICKET);
    }

    public static ENUM_TITO_OPERATIONS_RESULT AsignPromotion(ParamsTicketOperation OperationParams, TicketCreationParams Params, DataTable PromotionTable, TitoPrinter.TITO_PRINTER_PRINT_MODE TitoPrintMode, out Ticket TicketPromo, SqlTransaction SqlTrx, ENUM_OPERATION_MODE_PROMOTIONS_NONREDEM ModePromotions)
    {
      CardData _card;
      RechargeInputParameters _idata;
      RechargeOutputParameters _odata;
      Int64 _operation_id;
      Int64 _promotion_id;
      Currency _redeemable;
      Currency _not_redeemable;
      AccountPromotion _promo;
      DataTable _promos;
      AccountMovementsTable _account_movements;
      CashierMovementsTable _cashier_movements;
      String _str_message;

      TicketPromo = null;
      _card = OperationParams.in_account;
      _redeemable = Params.OutCashAmount;
      _not_redeemable = Params.OutNotRedeemableAmount;
      _promotion_id = Params.OutPromotionId; //CHECK if can be used OperationParams.out_promotion_id instead Params.OutPromotionId in order to delete it from PromotionalTicketsParams

      _operation_id = OperationParams.out_operation_id;

      _str_message = String.Empty;

      if (PromotionTable == null)
      {
        PromotionTable = AccountPromotion.CreateAccountPromotionTable();
      }

      if (!TitoPrinter.IsReady() && ModePromotions == ENUM_OPERATION_MODE_PROMOTIONS_NONREDEM.CREATE_TICKET)
      {
        return ENUM_TITO_OPERATIONS_RESULT.PRINTER_IS_NOT_READY;
      }

      _idata = new RechargeInputParameters();
      _idata.NoMoney = false;
      _idata.GenerateVouchers = false;
      _idata.AccountId = _card.AccountId;
      _idata.AccountLevel = _card.PlayerTracking.CardLevel;
      _idata.ExternalTrackData = _card.TrackData;
      _idata.VoucherAccountInfo = _card.VoucherAccountInfo();
      _idata.CardPaid = true;
      _idata.CardPrice = 0;

      _account_movements = new AccountMovementsTable(OperationParams.session_info);
      _cashier_movements = new CashierMovementsTable(OperationParams.session_info);

      _promo = new AccountPromotion();
      _promos = AccountPromotion.CreateAccountPromotionTable();

      if (!Promotion.ReadPromotionData(SqlTrx, _promotion_id, _promo))
      {
        return ENUM_TITO_OPERATIONS_RESULT.AUX_OPERATION_ERROR;
      }

      if (!AccountPromotion.AddPromotionToAccount(_operation_id, _card.AccountId, _card.PlayerTracking.CardLevel,
                                                   _redeemable, 0, true, _not_redeemable, _promo, _promos))
      {
        return ENUM_TITO_OPERATIONS_RESULT.AUX_OPERATION_ERROR;
      }

      // ICS 19-MAR-2014: Update promotion spent used
      if (Params.OutSpentUsed > 0)
      {
        if (!Promotion.UpdateSpentUsedInPromotion(Params.OutSpentUsed, _card.AccountId, _promo, SqlTrx, out _str_message))
        {
          Log.Error("AsignPromotion. " + _str_message);

          return ENUM_TITO_OPERATIONS_RESULT.AUX_OPERATION_ERROR;
        }
      } // if (Params.OutSpentUsed > 0)

      if (!Accounts.DB_Recharge(_operation_id, _idata, 0, _promos, _account_movements, _cashier_movements, SqlTrx, out _odata))
      {
        return ENUM_TITO_OPERATIONS_RESULT.AUX_OPERATION_ERROR;
      }

      // DHA 13-FEB-2014: it's necessary to cancel the promotion when the ticket is created
      OperationParams.out_account_promo_id = _odata.AccountPromotionId;

      if (_promos.Rows.Count > 0)
      {
        PromotionTable.Rows.Add(_promos.Rows[0].ItemArray);
      }

      _account_movements.Save(SqlTrx);
      _cashier_movements.Save(SqlTrx);

      if (OperationParams.ticket_type == TITO_TICKET_TYPE.PROMO_REDEEM || OperationParams.ticket_type == TITO_TICKET_TYPE.PROMO_NONREDEEM)
      {
        bool account = true;
        if (ModePromotions == ENUM_OPERATION_MODE_PROMOTIONS_NONREDEM.ONLY_ASSIGN_PROMOTION)
        {
          account = false;
        }

        if (OperationParams.out_cash_amount > 0 && OperationParams.in_cash_amount == 0)
        {
          OperationParams.in_cash_amt_0 = OperationParams.out_cash_amount;
          OperationParams.in_cash_cur_0 = CurrencyExchange.GetNationalCurrency();
        }

        if (!BusinessLogic.CreateTicket(OperationParams, account, out TicketPromo, SqlTrx))
        {
          return ENUM_TITO_OPERATIONS_RESULT.AUX_OPERATION_ERROR;
        }
      }
      return ENUM_TITO_OPERATIONS_RESULT.OK;
    }


    //------------------------------------------------------------------------------
    // PURPOSE: Create Ticket with introduced amount; perform all movements related to this operation.
    //
    //  PARAMS:
    //      - INPUT:
    //        - ParamsTicketOperation
    //
    //      - OUTPUT:
    //
    //  RETURNS:
    //      - True: All went OK; False: otherwise
    //
    public static ENUM_TITO_OPERATIONS_RESULT CashInCreateTicket(ParamsTicketOperation OperationParams, TicketCreationParams Params)
    {
      Currency _credit_allowed_sale = Currency.Zero();
      Boolean _credit_allowed_reached = false;
      ArrayList _voucher_list;
      Currency _cash_amount;
      Currency _promo_amount;

      OperationCode _operation_code;
      CurrencyExchangeResult _exchange_result;

      RechargeOutputParameters OutputParameter;
      CashierSessionInfo _cashier_session_info;

      ArrayList _voucher_list_tpv;

      // ETP: PinPad vars:
      ENUM_TITO_OPERATIONS_RESULT _return_error;
      Boolean _is_pinpad_recharge;
      frm_tpv _form_tpv;
      PinPadInput _pinpad_input;
      PinPadOutput _pinpad_output;

      OutParamsTicketOperation _out_params_ticket_operation;

      _pinpad_output = new PinPadOutput();

      _voucher_list_tpv = new ArrayList();

      _cash_amount = 0;
      _promo_amount = 0;
      _exchange_result = null;

      OutputParameter = new RechargeOutputParameters();
      _exchange_result = Params.OutExchangeResult;
      _promo_amount = Params.OutNotRedeemableAmount;
      _cash_amount = Params.OutCashAmount;

      if (!TitoPrinter.IsReady() && _cash_amount > 0)
      {
        return ENUM_TITO_OPERATIONS_RESULT.PRINTER_IS_NOT_READY;
      }

      _operation_code = _promo_amount > 0 ? OperationCode.PROMOTION : OperationCode.CASH_IN;
      _cashier_session_info = Cashier.CashierSessionInfo();
      
      _is_pinpad_recharge = Misc.IsPinPadEnabled() && Common.Cashier.ReadPinPadEnabled() && _exchange_result != null && _exchange_result.InType == CurrencyExchangeType.CARD;

      if (_is_pinpad_recharge)
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {

          if (!InternetConnection.GetServiceInternetConnectionStatus("WCP"))
          {
            frm_message.Show(Resource.String("STR_PINPAD_CANCELLED_OPERATION_ERROR_CONNEXION"), Resource.String("STR_APP_GEN_MSG_WARNING"), MessageBoxButtons.OK, MessageBoxIcon.Warning, OperationParams.in_window_parent);

            _return_error = ENUM_TITO_OPERATIONS_RESULT.TPV_NOT_PAID;

            return _return_error;
          }

          _return_error = BusinessLogic.Common_CashInCreateTicket(OperationParams, Params, _db_trx.SqlTransaction, out _out_params_ticket_operation);

          if (_return_error != ENUM_TITO_OPERATIONS_RESULT.OK)
          {
            if (_return_error == ENUM_TITO_OPERATIONS_RESULT.CREDIT_LINE_KO) return ENUM_TITO_OPERATIONS_RESULT.CREDIT_LINE_KO;

            _db_trx.Rollback();
            if (_credit_allowed_reached)
            {
              String _text;
              _text = Resource.String("STR_FRM_CASHIER_LIMIT_SALES_REACHED") + _credit_allowed_sale;
              _text = _text.Replace("\\r\\n", "\r\n");

              frm_message.Show(_text,
                   Resource.String("STR_APP_GEN_MSG_WARNING"),
                   MessageBoxButtons.OK,
                   MessageBoxIcon.Warning,
                   OperationParams.in_window_parent); //   Params.WindowParent
            }
            return _return_error;
          }
          _db_trx.Rollback();
        }

        _pinpad_input = new PinPadInput();
        _pinpad_input.Amount = _exchange_result;
        _pinpad_input.Card = OperationParams.in_account;
        _pinpad_input.TerminalName = _cashier_session_info.TerminalName;

        _form_tpv = new frm_tpv(_pinpad_input);

        if (!_form_tpv.Show(out _pinpad_output)) // 20/10/2016 ETP: Pin Pad payement
        {
          _form_tpv.Dispose();
          return ENUM_TITO_OPERATIONS_RESULT.TPV_NOT_PAID;
        }
        _form_tpv.Dispose();
        // 20/10/2016 ETP: Transaction is Pending of udpating wigos data.
      }

      _return_error = ENUM_TITO_OPERATIONS_RESULT.OK;

      using (DB_TRX _db_trx = new DB_TRX())
      {
        _return_error = BusinessLogic.Common_CashInCreateTicket(OperationParams, Params, _db_trx.SqlTransaction, out _out_params_ticket_operation);

        if (_return_error != ENUM_TITO_OPERATIONS_RESULT.OK)
        {
          if (_return_error == ENUM_TITO_OPERATIONS_RESULT.CREDIT_LINE_KO) return ENUM_TITO_OPERATIONS_RESULT.CREDIT_LINE_KO;

          _db_trx.Rollback();
          if (_credit_allowed_reached)
          {
            String _text;
            _text = Resource.String("STR_FRM_CASHIER_LIMIT_SALES_REACHED") + _credit_allowed_sale;
            _text = _text.Replace("\\r\\n", "\r\n");

            frm_message.Show(_text,
                 Resource.String("STR_APP_GEN_MSG_WARNING"),
                 MessageBoxButtons.OK,
                 MessageBoxIcon.Warning,
                 OperationParams.in_window_parent); //   Params.WindowParent
          }
        }

        if (_is_pinpad_recharge && _return_error == ENUM_TITO_OPERATIONS_RESULT.OK)
        {
          PinPadTransactions _transaction;

          _transaction = _pinpad_output.PinPadTransaction;

          _transaction.OperationId = _out_params_ticket_operation.operation_id;
          _transaction.StatusCode = PinPadTransactions.STATUS.APPROVED;  //  20/10/2016 ETP: PinPad is payed OK.

          if (!_transaction.DB_Insert(_db_trx.SqlTransaction) && _return_error == ENUM_TITO_OPERATIONS_RESULT.OK)
          {
            _return_error = ENUM_TITO_OPERATIONS_RESULT.TPV_NOT_PAID;
          }

          _transaction.UpdateCashierSessionHasPinPadOperation(_cashier_session_info.CashierSessionId, _db_trx.SqlTransaction);

          if (!PinPadTransactions.DeleteFromUndoPinPad(_transaction.ControlNumber, _db_trx.SqlTransaction) && _return_error == ENUM_TITO_OPERATIONS_RESULT.OK)
          {
            _return_error = ENUM_TITO_OPERATIONS_RESULT.TPV_NOT_PAID;
          }

          if (!_pinpad_output.PinPadTransaction.CreateVoucher(_exchange_result, _pinpad_output, _db_trx.SqlTransaction, out _voucher_list_tpv))
          {
            _return_error = ENUM_TITO_OPERATIONS_RESULT.TPV_NOT_PAID;
          }

          _out_params_ticket_operation.VoucherList.AddRange(_voucher_list_tpv);
        }
        if (Params.PaymentThresholdAuthorization != null && Params.PaymentThresholdAuthorization.PaymentAndRechargeThresholdData != null && Params.PaymentThresholdAuthorization.InputThreshold > 0
          && (Params.OutExchangeResult != null && Params.PaymentThresholdAuthorization.InputThreshold <= Params.OutExchangeResult.GrossAmount || Params.PaymentThresholdAuthorization.InputThreshold <= Params.OutCashAmount))
        {
          Params.PaymentThresholdAuthorization.SaveThresholdOperation(_out_params_ticket_operation.operation_id, _operation_code, _db_trx.SqlTransaction);
        }

        if (_return_error == ENUM_TITO_OPERATIONS_RESULT.OK)
        {
          _db_trx.Commit();
        }
      } // using _db_trx


      if (_return_error != ENUM_TITO_OPERATIONS_RESULT.OK)
      {
        // 20/10/2016 ETP: Undo PinPad Operations if and error has ocurred:
        if (_is_pinpad_recharge)
        {
          using (DB_TRX _db_trx = new DB_TRX())
          {
            _pinpad_output.PinPadTransaction.OperationId = _out_params_ticket_operation.operation_id;

            _pinpad_output.PinPadTransaction.StatusCode = PinPadTransactions.STATUS.ERROR;
            _pinpad_output.PinPadTransaction.DB_Insert(_db_trx.SqlTransaction);
            _pinpad_output.PinPadTransaction.UpdateCashierSessionHasPinPadOperation(_cashier_session_info.CashierSessionId, _db_trx.SqlTransaction);

            PinPadTransactions.UpdateDateUndoPinPad(_pinpad_output.PinPadTransaction.ControlNumber, _db_trx.SqlTransaction);
            _db_trx.Commit();
          }
        }

        return _return_error;
      }

      VoucherPrint.Print(_out_params_ticket_operation.VoucherList);

      foreach (Ticket _tkt in _out_params_ticket_operation.TicketList)
      {
        if (!TitoPrinter.PrintTicket(_tkt.ValidationNumber, _tkt.MachineTicketNumber, _tkt.TicketType, _tkt.Amount, _tkt.CreatedDateTime, _tkt.ExpirationDateTime, _cashier_session_info))
        {
          Log.Error("CashInCreateTicket: PrintTicket. Error on printing ticket.");

          using (DB_TRX _trx = new DB_TRX())
          {
            OperationUndo.InputUndoOperation _params = new OperationUndo.InputUndoOperation();
            _params.CodeOperation = OperationCode.CASH_IN;
            _params.CardData = OperationParams.in_account;
            _params.OperationIdForUndo = _out_params_ticket_operation.operation_id;
            _params.CashierSessionInfo = Cashier.CashierSessionInfo();

            if (OperationUndo.UndoOperation(_params, false, out _voucher_list, _trx.SqlTransaction))
            {
              _trx.Commit();

              return ENUM_TITO_OPERATIONS_RESULT.PRINTER_IS_NOT_READY;
            }
          }

          return ENUM_TITO_OPERATIONS_RESULT.PRINTER_PRINT_FAIL;
        }
      }

      return ENUM_TITO_OPERATIONS_RESULT.OK;
    }

    private static ENUM_TITO_OPERATIONS_RESULT Common_CashInCreateTicket(ParamsTicketOperation OperationParams, TicketCreationParams Params, SqlTransaction Trx, out OutParamsTicketOperation OutParamsTicketOperation)
    {
      Currency _credit_allowed_sale = Currency.Zero();
      Boolean _credit_allowed_reached = false;
      Result _cash_desk_draw_result;
      Promotion.TYPE_PROMOTION_DATA _promo;
      Ticket _ticket;

      Currency _cash_amount;
      Currency _promo_amount;

      Int64 _promo_id;
      OperationCode _operation_code;
      CurrencyExchangeResult _exchange_result;
      Int64 _account_operation_id;
      AccountPromotion _promotion_data;

      RechargeOutputParameters OutputParameter;
      CashierSessionInfo _cashier_session_info;

      Int64 _operation_id;
      ResultGetMarker _result_get_marker;

      JunketsBusinessLogic _junket;
      VoucherJunket _junket_voucher;

      _junket = null;

      OutParamsTicketOperation = new Common.OutParamsTicketOperation();
      OutParamsTicketOperation.CreaditAllowedReached = false;

      _cash_amount = 0;
      _promo_amount = 0;
      _promo_id = 0;
      _exchange_result = null;
      OutParamsTicketOperation.TicketList = new List<Ticket>();
      OutParamsTicketOperation.VoucherList = new ArrayList();

      OutputParameter = new RechargeOutputParameters();

      _exchange_result = Params.OutExchangeResult;
      _promo_amount = Params.OutNotRedeemableAmount;
      _cash_amount = Params.OutCashAmount;
      _promo_id = Params.OutPromotionId;

      _operation_code = _promo_amount > 0 ? OperationCode.PROMOTION : OperationCode.CASH_IN;
      _cashier_session_info = Cashier.CashierSessionInfo();

      _operation_id = 0;

      if (Params.OutExchangeResult != null && Params.OutExchangeResult.InType == CurrencyExchangeType.CREDITLINE)
      {
        #region " CREDIT LINE "

        _result_get_marker = WSI.Cashier.CreditLines.CreditLine.getMarker(OperationParams.in_account, Params.OutExchangeResult.InAmount + Params.OutExchangeResult.CardPrice, OperationParams.in_window_parent,
                                                                            _cashier_session_info, CreditLineMovements.CreditLineMovementType.GetMarker,
                                                                            Trx, out _operation_id);
        if (_result_get_marker != ResultGetMarker.OK)
        {
          return ENUM_TITO_OPERATIONS_RESULT.CREDIT_LINE_KO;
        }

        #endregion " CREDIT LINE "
      }

      if (Params.JunketInfo != null && Params.JunketInfo.BusinessLogic != null)
      {
        _junket = Params.JunketInfo.BusinessLogic;
      }

      if (!Accounts.DB_CardCreditAdd(OperationParams.in_account,
                                                 _cash_amount,
                                                 _promo_id,
                                                 _promo_amount,
                                                 Params.OutNotRedeemableWonLock,
                                                 Params.OutSpentUsed,
                                                 _exchange_result,
                                                 Params.OutParticipateInCashdeskDraw,
                                                 _operation_code,
                                                 _operation_id,
                                                 _cashier_session_info,
                                                 Trx,
                                                 _junket,
                                                 out OutputParameter))
      {
        if (_credit_allowed_reached)
        {
          OutParamsTicketOperation.CreaditAllowedReached = true;

          return ENUM_TITO_OPERATIONS_RESULT.AUX_OPERATION_CANCELED;
        }

        Log.Error("CashInCreateTicket: DB_CardCreditAdd. Error adding credit to card.");

        return ENUM_TITO_OPERATIONS_RESULT.AUX_OPERATION_ERROR;

      }
      else //Card Credit Successfully added
      {

        if (Params.JunketInfo != null)
        {
          // Add Flyer Voucher to Voucher List for Printing
        _junket_voucher = Params.JunketInfo.CreateVoucher(OutputParameter.OperationId, OperationParams.in_account, PrintMode.Print, Trx);

        OutputParameter.VoucherList.Add(_junket_voucher);
      }
      }

      _cash_desk_draw_result = OutputParameter.CashDeskDrawResult;
      OutParamsTicketOperation.operation_id = OutputParameter.OperationId;
      _account_operation_id = OutputParameter.AccountPromotionId;
      OutParamsTicketOperation.VoucherList = OutputParameter.VoucherList;


      //if (_junket != null)
      //{
      //  if (_junket.PrintVoucher)
      //  {
      //    VoucherJunket _junket_voucher;

      //    _junket_voucher = new VoucherJunket(OperationParams.in_account.VoucherAccountInfo(),
      //                                        _junket.TitleVoucher,
      //                                        _junket.TextVoucher,
      //                                        _junket.FlyerCode,
      //                                        _account_operation_id,
      //                                        PrintMode.Preview,
      //                                        null);

      //  }
      //}


      //creation of cashable ticket
      OperationParams.ticket_type = TITO_TICKET_TYPE.CASHABLE;
      OperationParams.out_cash_amount = _cash_amount;
      OperationParams.out_operation_id = OutParamsTicketOperation.operation_id;
      OperationParams.out_account_promo_id = _account_operation_id;

      // DHA 03-FEB-2016
      OperationParams.in_cash_amt_0 = _cash_amount;
      OperationParams.in_cash_cur_0 = CurrencyExchange.GetNationalCurrency();

      if (OperationParams.out_cash_amount > 0)
      {
        if (!BusinessLogic.CreateTicket(OperationParams, true, out _ticket, Trx))
        {
          Log.Error("CashInCreateTicket: CreateTicket. Error on create and update ticket data.");

          return ENUM_TITO_OPERATIONS_RESULT.AUX_OPERATION_ERROR;
        }
        OutParamsTicketOperation.TicketList.Add(_ticket);
      }

      // creation of promotional ticket
      OperationParams.out_cash_amount = _promo_amount;
      OperationParams.out_promotion_id = _promo_id;

      // DHA 03-FEB-2016
      OperationParams.in_cash_amt_0 = _promo_amount;

      _promo = new Promotion.TYPE_PROMOTION_DATA();
      Promotion.ReadPromotionData(Trx, OperationParams.out_promotion_id, _promo);

      if (OperationParams.out_cash_amount > 0 && _promo.credit_type != ACCOUNT_PROMO_CREDIT_TYPE.POINT)
      {
        if (_promo.credit_type == ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE)
        {
          OperationParams.ticket_type = TITO_TICKET_TYPE.PROMO_REDEEM;
        }
        else
        {
          OperationParams.ticket_type = TITO_TICKET_TYPE.PROMO_NONREDEEM;
        }
        if (!BusinessLogic.CreateTicket(OperationParams, true, out _ticket, Trx))
        {
          Log.Error("CashInCreateTicket: CreateTicket. Error on create and update ticket data.");

          return ENUM_TITO_OPERATIONS_RESULT.AUX_OPERATION_ERROR;
        }
        OutParamsTicketOperation.TicketList.Add(_ticket);
      }

      //create currency exchange promotional ticket
      if (_exchange_result != null && _exchange_result.ArePromotionsEnabled && _exchange_result.NR2Amount > 0)
      {
        Promotion.ReadPromotionData(Trx, Promotion.PROMOTION_TYPE.AUTOMATIC_PER_RECHARGE_WITH_CURRENCY_EXCHANGE, out _promotion_data);

        OperationParams.out_promotion_id = _promotion_data.id;
        OperationParams.out_cash_amount = _exchange_result.NR2Amount;
        OperationParams.ticket_type = TITO_TICKET_TYPE.PROMO_NONREDEEM;
        OperationParams.out_account_promo_id = _exchange_result.AccountPromotionId;

        if (!BusinessLogic.CreateTicket(OperationParams, true, out _ticket, Trx))
        {
          Log.Error("CashInCreateTicket: CreateTicket. Error on create and update ticket data.");

          return ENUM_TITO_OPERATIONS_RESULT.AUX_OPERATION_ERROR;
        }
        OutParamsTicketOperation.TicketList.Add(_ticket);
      }

      //create currency exchange promotional ticket
      if (OutputParameter.CoverCouponAmount > 0)
      {
        Promotion.ReadPromotionData(Trx, Promotion.PROMOTION_TYPE.AUTOMATIC_PER_RECHARGE_COVER_COUPON, out _promotion_data);

        OperationParams.out_promotion_id = _promotion_data.id;
        OperationParams.out_cash_amount = OutputParameter.CoverCouponAmount;
        OperationParams.ticket_type = TITO_TICKET_TYPE.PROMO_NONREDEEM;
        OperationParams.out_account_promo_id = OutputParameter.CoverCouponAccountPromotionId;

        if (!BusinessLogic.CreateTicket(OperationParams, true, out _ticket, Trx))
        {
          Log.Error("CashInCreateTicket: CreateTicket. Error on create and update ticket data.");

          return ENUM_TITO_OPERATIONS_RESULT.AUX_OPERATION_ERROR;
        }
        OutParamsTicketOperation.TicketList.Add(_ticket);
      }

      if (Params.JunketInfo != null)
      {
        if (!Params.JunketInfo.ProcessJunketToAccount(OutputParameter.OperationId, Trx))
        {
          return WSI.Common.ENUM_TITO_OPERATIONS_RESULT.JUNKETS_KO;
        }
      }

      return ENUM_TITO_OPERATIONS_RESULT.OK;
    }

    public static Boolean SwapTicketStatus(CardData Card, IDictionary<long, decimal> Tickets, long OperationId, SqlTransaction Trx)
    {
      bool _success;

      _success = true;
      foreach (var item in Tickets)
      {
        _success = UndoTicketReissue(Card, item.Key, OperationId, OperationCode.CHIPS_SWAP, Trx);
        if (!_success)
        {
          return false;
        }
      }

      return _success;
    }

    public static Boolean UndoTicketReissue(CardData Card, Int64 TicketId, Int64 OperationId, OperationCode OpCode, SqlTransaction Trx)
    {
      Ticket _ticket;
      ArrayList _voucher_list;

      OperationUndo.InputUndoOperation _params = new OperationUndo.InputUndoOperation();
      _params.CodeOperation = OpCode;
      _params.CardData = Card;
      _params.OperationIdForUndo = OperationId;
      _params.CashierSessionInfo = Cashier.CashierSessionInfo();

      if (!OperationUndo.UndoOperation(_params, false, out _voucher_list, Trx))
      {
        return false;
      }

      if (OpCode == OperationCode.TITO_OFFLINE)
      {
        return true;
      }

      _ticket = Ticket.LoadTicket(TicketId, Trx, false);
      if (_ticket.TicketID < 1)
      {
        return false;
      }

      if (_ticket.Status != TITO_TICKET_STATUS.DISCARDED)
      {
        return false;
      }

      _ticket.Status = TITO_TICKET_STATUS.VALID;
      _ticket.LastActionDateTime = WGDB.Now;

      if (!Ticket.DB_UpdateTicket(_ticket, Trx))
      {
        return false;
      }

      return true;
    }

    public static Boolean UndoGift(CardData Account, Int64 OperationId, OperationCode OpCode, SqlTransaction Trx)
    {
      GiftInstance _gin;
      StringBuilder _sb;
      Decimal _final_points;
      ArrayList _voucher_list;
      Object _obj;

      _gin = new GiftInstance();

      _sb = new StringBuilder();
      _sb.AppendLine(" DECLARE @pNumUnits AS INT             ");
      _sb.AppendLine(" DECLARE @pGiftId   AS BIGINT          ");
      _sb.AppendLine(" DECLARE @pPoints   AS MONEY           ");
      _sb.AppendLine(" SELECT   @pNumUnits = GIN_NUM_ITEMS   ");
      _sb.AppendLine("        , @pGiftId   = GIN_GIFT_ID     ");
      _sb.AppendLine("        , @pPoints   = GIN_SPENT_POINTS");
      _sb.AppendLine("   FROM   GIFT_INSTANCES               ");
      _sb.AppendLine("  WHERE   GIN_OPER_REQUEST_ID = @pOpId ");
      _sb.AppendLine(" UPDATE   GIFTS                        ");
      _sb.AppendLine("    SET   GI_DELIVERY_COUNTER = CASE WHEN GI_DELIVERY_COUNTER > @pNumUnits THEN GI_DELIVERY_COUNTER - @pNumUnits ELSE 0 END");
      _sb.AppendLine("  WHERE   GI_GIFT_ID = @pGiftId        ");
      _sb.AppendLine(" SELECT   @pPoints                     ");

      try
      {
        OperationUndo.InputUndoOperation _params = new OperationUndo.InputUndoOperation();
        _params.CodeOperation = OpCode;
        _params.CardData = Account;
        _params.OperationIdForUndo = OperationId;
        _params.CashierSessionInfo = Cashier.CashierSessionInfo();

        if (!OperationUndo.UndoOperation(_params, false, out _voucher_list, Trx))
        {
          return false;
        }

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pOpId", SqlDbType.BigInt).Value = OperationId;

          _obj = _cmd.ExecuteScalar();
          if (_obj == null || _obj == DBNull.Value)
          {
            return false;
          }
        }

        if (!MultiPromos.Trx_UpdateAccountPoints(Account.AccountId, (Decimal)_obj, out _final_points, Trx))
        {
          return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Load account data asociated with terminal
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    //  RETURNS:
    //      - True: All went OK; False: otherwise
    //
    public static Boolean LoadVirtualAccount(CardData VirtualCard)
    {
      Boolean _success;

      using (DB_TRX _db_trx = new DB_TRX())
      {
        _success = LoadVirtualAccount((Int32)Cashier.TerminalId, AccountType.ACCOUNT_VIRTUAL_CASHIER, VirtualCard, _db_trx.SqlTransaction);

        if (_success)
        {
          _db_trx.Commit();
        }
      }
      return _success;

    }

    public static Boolean LoadVirtualAccount(Int32 TerminalId, AccountType AccType, CardData VirtualCard, SqlTransaction Trx)
    {
      Int64 _account_id;

      _account_id = Accounts.GetOrCreateVirtualAccount(TerminalId, AccType, Trx);

      if (_account_id <= 0)
      {
        return false;
      }

      // Get Data by TrackNumber
      if (!CardData.DB_CardGetAllData(_account_id, VirtualCard, Trx))
      {
        return false;
      }

      return true;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Recreate ticket for reprinting. Operations are the followings:
    //            - cancel the actual ticket
    //            - create a new ticket
    //
    //  PARAMS:
    //      - INPUT:
    //        - Int64: TicketNumber
    //
    //      - OUTPUT:
    //
    //  RETURNS:
    //      - ENUM_TITO_OPERATIONS_RESULT
    //        - OK(1): All went OK
    //        - != 1: otherwise
    //
    public static ENUM_TITO_OPERATIONS_RESULT TicketReprint(Int64 TicketId, ParamsTicketOperation OperationParams, Form ParentForm, out ArrayList VoucherList)
    {
      ENUM_TITO_OPERATIONS_RESULT _result;
      Ticket _new_ticket;
      Currency _not_redeemable_amount;
      Currency _redeemable_amount_to_add;
      Int64 _operation_id;

      _not_redeemable_amount = 0;
      _redeemable_amount_to_add = 0;
      VoucherList = new ArrayList();

      using (DB_TRX _db_trx = new DB_TRX())
      {
        if (OperationParams.ticket_type == TITO_TICKET_TYPE.PROMO_NONREDEEM)
        {
          _not_redeemable_amount = OperationParams.out_cash_amount;
        }
        else if (OperationParams.ticket_type == TITO_TICKET_TYPE.CASHABLE)
        {
          _redeemable_amount_to_add = OperationParams.out_cash_amount;
        }

        if (!Operations.DB_InsertOperation(OperationCode.TITO_REISSUE, OperationParams.in_account.AccountId, Cashier.SessionId, 0,
                                           _redeemable_amount_to_add, 0, _not_redeemable_amount, 0, 0,
                                           0,                      // operation data
                                           0, string.Empty,
                                           out _operation_id, _db_trx.SqlTransaction))
        {
          return ENUM_TITO_OPERATIONS_RESULT.AUX_OPERATION_ERROR;
        }

        OperationParams.out_operation_id = _operation_id;

        _result = TicketReprint(TicketId, OperationParams, false, ParentForm, out _new_ticket, out VoucherList, _db_trx.SqlTransaction);

        if (_result == ENUM_TITO_OPERATIONS_RESULT.OK)
        {
          _db_trx.Commit();
        }
      }

      if (_new_ticket != null)
      {
        if (!TitoPrinter.PrintTicket(_new_ticket.ValidationNumber, _new_ticket.MachineTicketNumber, _new_ticket.TicketType, _new_ticket.Amount, _new_ticket.CreatedDateTime, _new_ticket.ExpirationDateTime, Cashier.CashierSessionInfo()))
        {
          Log.Error("TicketReprint: PrintTicket. Error on printing ticket.");

          _result = ENUM_TITO_OPERATIONS_RESULT.PRINTER_PRINT_FAIL;

          using (DB_TRX _trx = new DB_TRX())
          {
            if (UndoTicketReissue(OperationParams.in_account, TicketId, _operation_id, OperationCode.TITO_REISSUE, _trx.SqlTransaction))
            {
              _result = ENUM_TITO_OPERATIONS_RESULT.PRINTER_IS_NOT_READY;

              _trx.Commit();
            }
          }
        }
      }

      return _result;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Recreate ticket for reprinting. Operations are the followings:
    //            - cancel the actual ticket
    //            - create a new ticket
    //
    //  PARAMS:
    //      - INPUT:
    //        - Int64: TicketNumber
    //
    //      - OUTPUT:
    //
    //  RETURNS:
    //      - ENUM_TITO_OPERATIONS_RESULT
    //        - OK(1): All went OK
    //        - != 1: otherwise
    //
    public static ENUM_TITO_OPERATIONS_RESULT TicketReprint(Int64 TicketId, ParamsTicketOperation OperationParams, Boolean MustBeCollected, Form ParentForm, out Ticket NewTicket, out ArrayList VoucherList, SqlTransaction Trx)
    {
      return TicketReprint(TicketId, OperationParams, MustBeCollected, ParentForm, out NewTicket, out VoucherList, Trx, ENUM_OPERATION_MODE_PROMOTIONS_NONREDEM.CREATE_TICKET);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Recreate ticket for reprinting. Operations are the followings:
    //            - cancel the actual ticket
    //            - create a new ticket
    //
    //  PARAMS:
    //      - INPUT:
    //        - Int64: TicketNumber
    //
    //      - OUTPUT:
    //
    //  RETURNS:
    //      - ENUM_TITO_OPERATIONS_RESULT
    //        - OK(1): All went OK
    //        - != 1: otherwise
    //
    public static ENUM_TITO_OPERATIONS_RESULT TicketReprint(Int64 TicketId, ParamsTicketOperation OperationParams, Boolean MustBeCollected, Form ParentForm, out Ticket NewTicket, out ArrayList VoucherList, SqlTransaction Trx, ENUM_OPERATION_MODE_PROMOTIONS_NONREDEM ModePromotions)
    {
      Ticket _ticket_data;
      Decimal _balance;
      OperationCode _operation_code;
      CashierSessionInfo _current_session_info;
      CashierSessionInfo _terminal_session_info;
      bool _allow_pay_tickets_pending_print;

      _allow_pay_tickets_pending_print = GeneralParam.GetBoolean("TITO", "PendingPrint.AllowToRedeem", true);
      NewTicket = null;
      VoucherList = new ArrayList();
      _operation_code = OperationCode.TITO_REISSUE;

      _current_session_info = CommonCashierInformation.CashierSessionInfo();

      try
      {
        if (!TitoPrinter.IsReady())
        {
          return ENUM_TITO_OPERATIONS_RESULT.PRINTER_IS_NOT_READY;
        }

        _ticket_data = Ticket.LoadTicket(TicketId, Trx, false);

        if (_ticket_data.ValidationNumber <= 0)
        {
          return ENUM_TITO_OPERATIONS_RESULT.AUX_DATA_NOT_FOUND;
        }

        if (_allow_pay_tickets_pending_print)
        {
          if (_ticket_data.Status != TITO_TICKET_STATUS.VALID && _ticket_data.Status != TITO_TICKET_STATUS.PENDING_PRINT)
          {
            return ENUM_TITO_OPERATIONS_RESULT.TICKET_NOT_VALID;
          }
        }
        else
        {
          if (_ticket_data.Status != TITO_TICKET_STATUS.VALID)
          {
            return ENUM_TITO_OPERATIONS_RESULT.TICKET_NOT_VALID;
          }
        }


        if (!BusinessLogic.TicketReissue(OperationParams, _ticket_data, MustBeCollected, out _balance, Trx))
        {
          Log.Error("TicketReprint: CancelTicket. Error updating ticket data.");

          return ENUM_TITO_OPERATIONS_RESULT.AUX_OPERATION_ERROR;
        }

        OperationParams.out_cash_amount = _ticket_data.Amount;

        // Create voucher
        // Ticket Reissue cashier movement must be at terminal cashier session
        _terminal_session_info = Common.Cashier.GetSystemCashierSessionInfo(GU_USER_TYPE.SYS_TITO, Trx, _ticket_data.CreatedTerminalName);
        OperationParams.terminal_id = _ticket_data.CreatedTerminalId;
        _terminal_session_info.TerminalId = _ticket_data.CreatedTerminalId;
        _terminal_session_info.AuthorizedByUserName = OperationParams.session_info.AuthorizedByUserName;
        _terminal_session_info.AuthorizedByUserId = OperationParams.session_info.AuthorizedByUserId;
        OperationParams.session_info = _terminal_session_info;

        if (OperationParams.validation_numbers != null)
        {
          if (_ticket_data.TicketType == TITO_TICKET_TYPE.OFFLINE)
          {
            _operation_code = OperationCode.TITO_OFFLINE;
          }

          VoucherTitoTicketOperation _voucher_temp;
          _voucher_temp = new VoucherTitoTicketOperation(OperationParams.in_account.VoucherAccountInfo(), _operation_code, false, OperationParams.validation_numbers, PrintMode.Print, Trx);
          _voucher_temp.Save(OperationParams.out_operation_id, Trx);
          VoucherList.Add(_voucher_temp);
        }

        if (!BusinessLogic.CreateTicket(OperationParams, false, _balance, out NewTicket, Trx))
        {
          Log.Error("TicketReprint: CreateTicket. Error on create and update ticket data.");

          return ENUM_TITO_OPERATIONS_RESULT.AUX_OPERATION_ERROR;
        }

        if (_ticket_data.Status == TITO_TICKET_STATUS.PENDING_PRINT)
          Log.Message("BusinessLogic TicketReprint: ticket status is PENDING PRINTING");

        return ENUM_TITO_OPERATIONS_RESULT.OK;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return ENUM_TITO_OPERATIONS_RESULT.EXCEPTION;
      }
      finally
      {
        CommonCashierInformation.SetCashierInformation(_current_session_info.CashierSessionId, _current_session_info.UserId, _current_session_info.UserName, _current_session_info.TerminalId, _current_session_info.TerminalName);
      }
    }

    #region Public Methods for ticket manipulation in Cashier

    public static Ticket SelectTicketByCondition(String GeneralCondition, String SelectionCondition)
    {
      DataTable _tickets;
      DataTable _tickets_selection;
      Ticket _ticket;
      Boolean _ok;
      DataRow _row;
      DataRow[] _selection_rows;
      frm_tickets_reprint _frm_ticket_sel;
      Int32 _idx;

      _tickets = new DataTable();
      _ticket = new Ticket();
      _ok = true;
      _row = null;

      using (DB_TRX _db_trx = new DB_TRX())
      {
        if (!Ticket.LoadTicketsByCondition(GeneralCondition, ref _tickets, _db_trx.SqlTransaction) || _tickets.Rows.Count == 0)
        {
          _ok = false;
        }
      }

      if (_ok)
      {
        _row = _tickets.Rows[0];

        if (_tickets.Rows.Count > 1)
        {
          _ok = false;
          if (String.IsNullOrEmpty(SelectionCondition))
          {
            _selection_rows = _tickets.Select();
          }
          else
          {
            _selection_rows = _tickets.Select(SelectionCondition);
          }
          if (_selection_rows.Length > 0)
          {
            _frm_ticket_sel = new frm_tickets_reprint();

            try
            {
              _tickets_selection = _frm_ticket_sel.CreateSelectTicketTable();

              foreach (DataRow _old_row in _selection_rows)
              {
                DataRow _new_row = _tickets_selection.Rows.Add();
                _new_row[_frm_ticket_sel.GRID_COLUMN_TERMINAL_NAME] = _old_row["TERMINAL_NAME"];
                _new_row[_frm_ticket_sel.GRID_COLUMN_CREATION_DATE] = _old_row["TI_CREATED_DATETIME"];
                _new_row[_frm_ticket_sel.GRID_COLUMN_VALIDATION_NUMBER] = _old_row["TI_VALIDATION_NUMBER"];
                _new_row[_frm_ticket_sel.GRID_COLUMN_AMOUNT] = _old_row["TI_AMOUNT"];
                _new_row[_frm_ticket_sel.GRID_COLUMN_TICKET_ID] = _old_row["TI_TICKET_ID"];
              }

              _idx = _frm_ticket_sel.ShowForTicketSelect(_tickets_selection);
            }
            finally
            {
              // MPO 15-APR-2014
              _frm_ticket_sel.Dispose();
            }

            if (_idx >= 0)
            {
              _row = _selection_rows[_idx];
              _ok = true;
            }
          }
        }
      }

      if (_ok)
      {
        _ticket.TicketID = (Int64)_row["TI_TICKET_ID"];
        _ticket.ValidationNumber = (Int64)_row["TI_VALIDATION_NUMBER"];
        _ticket.ValidationType = (TITO_VALIDATION_TYPE)_row["TI_VALIDATION_TYPE"];
        _ticket.Amount = (Decimal)_row["TI_AMOUNT"];
        _ticket.Status = (TITO_TICKET_STATUS)_row["TI_STATUS"];
        _ticket.TicketType = (TITO_TICKET_TYPE)_row["TI_TYPE_ID"];
        _ticket.CreatedDateTime = (DateTime)_row["TI_CREATED_DATETIME"];
        _ticket.CreatedTerminalId = (Int32)_row["TI_CREATED_TERMINAL_ID"];
        _ticket.CreatedTerminalName = (String)_row["TERMINAL_NAME"];
        _ticket.CreatedTerminalType = (Int32)_row["TI_CREATED_TERMINAL_TYPE"];
        _ticket.TransactionId = (Int64)_row["TI_TRANSACTION_ID"];
        _ticket.CreatedPlaySessionId = (Int64)_row["TI_CREATED_PLAY_SESSION_ID"];
        if (_row["TI_EXPIRATION_DATETIME"] != null && _row["TI_EXPIRATION_DATETIME"] != DBNull.Value)
        {
          _ticket.ExpirationDateTime = (DateTime)_row["TI_EXPIRATION_DATETIME"];
        }
        _ticket.MoneyCollectionID = (Int64)_row["MONEY_COLLECTION"];
        _ticket.CreatedAccountID = (Int64)_row["CREATED_ACCOUNT_ID"];
        _ticket.LastActionTerminalID = (Int32)_row["LAST_TERMINAL_ID"];
        _ticket.LastActionTerminalName = (String)_row["LAST_TERMINAL_NAME"];
        if (_row["TI_LAST_ACTION_DATETIME"] != null && _row["TI_LAST_ACTION_DATETIME"] != DBNull.Value)
        {
          _ticket.LastActionDateTime = (DateTime)_row["TI_LAST_ACTION_DATETIME"];
        }
        _ticket.LastActionAccountID = (Int64)_row["LAST_ACCOUNT_ID"];
        _ticket.LastActionTerminalType = (Int32)_row["LAST_TERMINAL_TYPE"];
        _ticket.PromotionID = (Int64)_row["PROMOTION"];
        _ticket.CollectedMoneyCollection = (Int64)_row["COLLECTED"];
        _ticket.MachineTicketNumber = (Int32)_row["TI_MACHINE_NUMBER"];

        _ticket.Amt_0 = (Decimal)_row["TI_AMT0"];
        _ticket.Cur_0 = (String)_row["TI_CUR0"];
        if (_row["TI_AMT1"] != null && _row["TI_AMT1"] != DBNull.Value)
        {
          _ticket.Amt_1 = (Decimal)_row["TI_AMT1"];

        }
        if (_row["TI_CUR1"] != null && _row["TI_CUR1"] != DBNull.Value)
        {
          _ticket.Cur_1 = (String)_row["TI_CUR1"];
        }
      }

      return _ticket;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Comporse ValidationNumber for new Tickets, related to CASHIER-TERMINAL
    //
    //  PARAMS:
    //      - INPUT:
    //        - ParamsTicketOperation
    //
    //      - OUTPUT:
    //
    //  RETURNS:
    //        - Int64 or 0
    //
    private static Int64 ComposeValidationNumber(out Int64 Sequence, SqlTransaction SqlTrx)
    {
      return WSI.Common.Cashier.Common_ComposeValidationNumber(out Sequence, SqlTrx, (Int32)Cashier.TerminalId);
    } // ComposeValidationNumber

    //------------------------------------------------------------------------------
    // PURPOSE: Internal function to create ticket structure from ParamsTicketOperation
    //          Some properties are setted to its default values
    //          The Validation Number is created From Cashier-Terminal data
    // 
    //  PARAMS:
    //      - INPUT:
    //        - ParamsTicketOperation
    //
    //      - OUTPUT:
    //
    //  RETURNS:
    //        - Ticket: nw ticket structure
    //
    public static Ticket CreateTicketData(ParamsTicketOperation Params, SqlTransaction SqlTrx)
    {
      return WSI.Common.Cashier.Common_CreateTicketData(Params, SqlTrx);
    } // CreateTicketData

    public static Boolean SetBalanceOperationAndPrintVoucherPromoLost(ParamsTicketOperation OperationParams, DataTable PromosTable, MultiPromos.AccountBalance OperationBalance)
    {
      Int32 _int_value;
      TYPE_SPLITS _splits;
      ArrayList _voucher_list;

      _voucher_list = new ArrayList();

      if (!Split.ReadSplitParameters(out _splits))
      {
        return false;
      }

      _int_value = GeneralParam.GetInt32("Cashier.Voucher", "VoucherOnPromotionAwarded", 0);

      using (DB_TRX _db_trx = new DB_TRX())
      {
        if (!Operations.Trx_SetOperationBalanceTITO(OperationParams.out_operation_id, OperationBalance, _db_trx.SqlTransaction))
        {
          return false;
        }

        if (_int_value > 0)
        {
          if (!VoucherBuilder.PromoLost(OperationParams.in_account.VoucherAccountInfo(),
                                          OperationParams.in_account.AccountId,
                                          _splits,
                                          (_int_value == 1),
                                          PromosTable,
                                          true,
                                          OperationParams.out_operation_id,
                                          PrintMode.Print,
                                          OperationCode.PROMOTION,
                                          null,
                                          _db_trx.SqlTransaction,
                                          out _voucher_list))
          {
            return false;
          }

          foreach (Voucher _voucher in _voucher_list)
          {
            _voucher.m_voucher_params.PrintCopy = 1;
            _voucher.NumPendingCopies = _voucher.m_voucher_params.PrintCopy;
          }

        }

        _db_trx.Commit();
      }

      VoucherPrint.Print(_voucher_list);

      return true;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Print ticket method without BackgroundWorker
    // 
    //  PARAMS:
    //      - INPUT:
    //        - ParamsTicketOperation
    //        - PromotionalTicketsParams
    //
    //      - OUTPUT:
    //
    //  RETURNS:
    //
    public static ENUM_TITO_OPERATIONS_RESULT CreatePromotionsTicketThread(ref ParamsTicketOperation OperationParams, TicketCreationParams Params)
    {
      BackgroundWorker _worker;
      DoWorkEventArgs _e;

      _worker = new BackgroundWorker();
      _e = null;

      return CreatePromotionsTicketThread(ref OperationParams, Params, TitoPrinter.TITO_PRINTER_PRINT_MODE.ASYNCHRONOUS, _worker, _e, ENUM_OPERATION_MODE_PROMOTIONS_NONREDEM.CREATE_TICKET);
    }


    //------------------------------------------------------------------------------
    // PURPOSE: Print ticket method without BackgroundWorker
    // 
    //  PARAMS:
    //      - INPUT:
    //        - ParamsTicketOperation
    //        - PromotionalTicketsParams
    //        - OperationModePromotionsNonRedem
    //
    //      - OUTPUT:
    //
    //  RETURNS:
    //
    public static ENUM_TITO_OPERATIONS_RESULT CreatePromotionsTicketThread(ref ParamsTicketOperation OperationParams, TicketCreationParams Params, ENUM_OPERATION_MODE_PROMOTIONS_NONREDEM OperationModePromotionsNonRedem)
    {
      BackgroundWorker _worker;
      DoWorkEventArgs _e;

      _worker = new BackgroundWorker();
      _e = null;

      return CreatePromotionsTicketThread(ref OperationParams, Params, TitoPrinter.TITO_PRINTER_PRINT_MODE.ASYNCHRONOUS, _worker, _e, OperationModePromotionsNonRedem);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Print ticket method with BackgroundWorker
    // 
    //  PARAMS:
    //      - INPUT:
    //        - ParamsTicketOperation
    //        - PromotionalTicketsParams
    //        - TitoPrinter.TITO_PRINTER_PRINT_MODE
    //        - BackgroundWorker
    //        - DoWorkEventArgs
    //
    //      - OUTPUT:
    //
    //  RETURNS:
    //
    public static ENUM_TITO_OPERATIONS_RESULT CreatePromotionsTicketThread(ref ParamsTicketOperation OperationParams, TicketCreationParams Params, TitoPrinter.TITO_PRINTER_PRINT_MODE TitoPrintMode, BackgroundWorker Worker, DoWorkEventArgs e)
    {
      return CreatePromotionsTicketThread(ref OperationParams, Params, TitoPrintMode, Worker, e, ENUM_OPERATION_MODE_PROMOTIONS_NONREDEM.CREATE_TICKET);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Print ticket method with BackgroundWorker
    // 
    //  PARAMS:
    //      - INPUT:
    //        - ParamsTicketOperation
    //        - PromotionalTicketsParams
    //        - TitoPrinter.TITO_PRINTER_PRINT_MODE
    //        - BackgroundWorker
    //        - DoWorkEventArgs
    //        - OperationModePromotionsNonRedem
    //
    //      - OUTPUT:
    //
    //  RETURNS:
    //
    public static ENUM_TITO_OPERATIONS_RESULT CreatePromotionsTicketThread(ref ParamsTicketOperation OperationParams, TicketCreationParams Params, TitoPrinter.TITO_PRINTER_PRINT_MODE TitoPrintMode, BackgroundWorker Worker, DoWorkEventArgs e, ENUM_OPERATION_MODE_PROMOTIONS_NONREDEM OperationModePromotionsNonRedem)
    {
      Int32 _ticket_created;
      Int32 _ticket_printed;
      Int32 _quantity_of_promos;
      Currency _remaining_reward;
      Currency _unitary_reward;
      Currency _promo_reward_amount;
      Currency _promo_won_lock;
      Currency _promo_reward_amount_nr;
      Currency _promo_reward_amount_re;
      Ticket _ticket;
      Int64 _operation_id;
      List<Ticket> _ticket_list;
      ENUM_TITO_OPERATIONS_RESULT _result;
      DataTable _promos_table;
      ArrayList _voucher_promo_list;
      ACCOUNT_PROMO_CREDIT_TYPE _promotion_type;

      // Set necessary params
      _ticket_created = 0;
      _ticket_printed = 0;
      _promo_reward_amount_nr = 0;
      _promo_reward_amount_re = 0;

      _remaining_reward = Params.PromoRewardAmount;
      _quantity_of_promos = Params.QuantityOfPromotions;
      _promo_reward_amount = Params.PromoRewardAmount;
      _promo_won_lock = Params.PromoWonLock;
      _unitary_reward = Params.UnitaryReward;
      _promotion_type = Params.OutPromotionType;

      _promos_table = AccountPromotion.CreateAccountPromotionTable();
      _ticket_list = new List<Ticket>();
      _voucher_promo_list = new ArrayList();
      _result = ENUM_TITO_OPERATIONS_RESULT.OK;

      // Create operation
      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (!Operations.DB_InsertOperation(OperationCode.PROMOTION, OperationParams.in_account.AccountId, Cashier.SessionId, 0,
                                   0,
                                   OperationParams.out_promotion_id,
                                   0,
                                   _promo_won_lock,
                                   0,
                                   0,
                                   0, string.Empty,
                                   out _operation_id, _db_trx.SqlTransaction))
          {
            Log.Error("CreatePromotionsTicketThread::Create operation. ENUM_TITO_OPERATIONS_RESULT.AUX_OPERATION_ERROR");
            return ENUM_TITO_OPERATIONS_RESULT.AUX_OPERATION_ERROR;
          }

          _db_trx.Commit();
        }
      }
      catch (Exception _ex)
      {
        Log.Error("Error: DB_InsertOperation() " + _ex);
        return ENUM_TITO_OPERATIONS_RESULT.AUX_OPERATION_ERROR;
      }

      // Set OperationId
      OperationParams.out_operation_id = _operation_id;

      for (int _idx = 0; _idx < _quantity_of_promos; _idx++)
      {
        // Cancel thread by user
        if (Worker.CancellationPending)
        {
          e.Cancel = true;
          _result = ENUM_TITO_OPERATIONS_RESULT.PRINTER_PRINT_STOPED;
          Log.Error(String.Format("CreatePromotionsTicketThread::break1. Result: [{0}]", _result.ToString()));
          break;
        }

        OperationParams.out_cash_amount = _unitary_reward;
        Params.OutNotRedeemableAmount = _unitary_reward;

        if (_remaining_reward < _unitary_reward)
        {
          OperationParams.out_cash_amount = _remaining_reward;
          Params.OutNotRedeemableAmount = _remaining_reward;

        }

        _remaining_reward -= _unitary_reward;

        // Create ticket
        try
        {
          using (DB_TRX _db_trx = new DB_TRX())
          {
            _result = BusinessLogic.AsignPromotion(OperationParams, Params, _promos_table, TitoPrintMode, out _ticket, _db_trx.SqlTransaction, OperationModePromotionsNonRedem);

            if (_result != ENUM_TITO_OPERATIONS_RESULT.OK)
            {
              Log.Error(String.Format("CreatePromotionsTicketThread::break2. Result: [{0}]", _result.ToString()));
              break;
            }
            _db_trx.Commit();
          }
        }
        catch (Exception _ex)
        {
          Log.Error("Error: AsignPromotion() " + _ex);
          _result = ENUM_TITO_OPERATIONS_RESULT.AUX_OPERATION_ERROR;
          break;
        }

        _ticket_created++;
        if (Worker.WorkerReportsProgress)
        {
          Worker.ReportProgress(100 / _quantity_of_promos * _ticket_created);
        }

        if (OperationModePromotionsNonRedem == ENUM_OPERATION_MODE_PROMOTIONS_NONREDEM.ONLY_ASSIGN_PROMOTION)
        {
          _ticket.Status = TITO_TICKET_STATUS.REDEEMED;
          using (DB_TRX _db_trx = new DB_TRX())
          {
            _ticket.Amt_1 = _ticket.Amt_0;
            _ticket.Cur_1 = _ticket.Cur_0;
            _ticket.LastActionDateTime = WGDB.Now;
            _ticket.LastActionTerminalID = (Int32)Cashier.TerminalId;
            _ticket.LastActionTerminalType = (Int32)OperationParams.tito_terminal_types;
            _ticket.LastActionAccountID = OperationParams.in_account.AccountId;

            if (!Ticket.DB_UpdateTicket(_ticket, _db_trx.SqlTransaction))
            {
              _result = ENUM_TITO_OPERATIONS_RESULT.DATA_NOT_SAVED;
              Log.Error(String.Format("CreatePromotionsTicketThread::break3. Result: [{0}]", _result.ToString()));
              break;
            }
            _db_trx.Commit();
          }
          continue;
        }

        // Print ticket
        if (_ticket != null)
        {
          if (!TitoPrinter.PrintTicket(_ticket.ValidationNumber, _ticket.MachineTicketNumber, _ticket.TicketType, _ticket.Amount, _ticket.CreatedDateTime, _ticket.ExpirationDateTime, TitoPrinter.TITO_PRINTER_PRINT_MODE.REAL_TIME, Cashier.CashierSessionInfo()))
          {
            Log.Error("CreatePromotionsTicketThread: PrintTicket. Error on printing ticket.");
            _result = ENUM_TITO_OPERATIONS_RESULT.PRINTER_PRINT_FAIL;
            Log.Error(String.Format("CreatePromotionsTicketThread::break4. Result: [{0}], OperationModePromotionsNonRedem: [{1}]", _result.ToString(), OperationModePromotionsNonRedem.ToString()));
            break;
          }

          _ticket_printed++;
        }

      }


      // Calculate the operation balance
      MultiPromos.AccountBalance _operation_balance = new MultiPromos.AccountBalance();

      if (_ticket_created != _quantity_of_promos)
      {
        if (_promotion_type == ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE)
        {
          _promo_reward_amount_re = _ticket_created * _unitary_reward;
        }
        else
        {
          _promo_reward_amount_nr = _ticket_created * _unitary_reward;
        }
      }
      else
      {
        if (_promotion_type == ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE)
        {
          _promo_reward_amount_re = _promo_reward_amount;
        }
        else
        {
          _promo_reward_amount_nr = _promo_reward_amount;
        }
      }
      _operation_balance.PromoNotRedeemable = _promo_reward_amount_nr;
      _operation_balance.PromoRedeemable = _promo_reward_amount_re;

      // Update operation balance
      if (!SetBalanceOperationAndPrintVoucherPromoLost(OperationParams, _promos_table, _operation_balance))
      {
        Log.Error("CreatePromotionsTicketThread::Update operation balance. ENUM_TITO_OPERATIONS_RESULT.AUX_OPERATION_ERROR");
        return ENUM_TITO_OPERATIONS_RESULT.AUX_OPERATION_ERROR;
      }

      return _result;

    } // PrintTicket


    //------------------------------------------------------------------------------
    // PURPOSE: Set status of Ticket to Redeem and update some other properties
    // 
    //  PARAMS:
    //      - INPUT:
    //        - TicketId
    //
    //      - OUTPUT:
    //
    //  RETURNS:
    //      - TRUE: all is OK
    //      - FALSE: something went wrong
    //
    public static Boolean RedeemTicketList(List<Int64> ListTicket, OperationCode OperationCode, ref ParamsTicketOperation Params, out ArrayList HandpayVouchers, SqlTransaction SqlTrx)
    {
      Ticket _ticket_data;
      List<Ticket> _tickets;
      MultiPromos.AccountBalance _ini_balance;
      MultiPromos.AccountBalance _final_balance;
      CashierSessionInfo _session_info;
      CashierMovementsTable _cashier_movements;
      AccountMovementsTable _account_movements;
      Int64 _money_collection_id;
      Handpays.HandpayRegisterOrCancelParameters _handpay_param;
      Decimal _total_tickets_amount;
      VoucherValidationNumber _voucher_number;
      MoneyCollection _money_collection;
      CASHIER_MOVEMENT _cashier_mov_type;
      MovementType _account_mov_type;
      CardData _card_for_handpay;
      DateTime _expiration_limit_date;
      String _cm_details;
      bool _allow_pay_tickets_pending_print;

      _allow_pay_tickets_pending_print = GeneralParam.GetBoolean("TITO", "PendingPrint.AllowToRedeem", true);
      _session_info = Params.session_info;
      _money_collection_id = 0;
      _total_tickets_amount = 0;
      _card_for_handpay = null;
      _cm_details = "";

      HandpayVouchers = new ArrayList();
      _tickets = new List<Ticket>();

      if (!MoneyCollection.GetCashierMoneyCollectionId(Params.session_info.CashierSessionId, out _money_collection_id, SqlTrx))
      {
        Log.Error("ReedemTicketList: Error getting collection id. Session Id:" + Params.session_info.CashierSessionId);
        return false;
      }
      _account_movements = new AccountMovementsTable(_session_info);
      _cashier_movements = new CashierMovementsTable(_session_info);
      _money_collection = MoneyCollection.DB_Read(_money_collection_id, SqlTrx);


      // Load tickets and validate ticket is Valid
      foreach (Int64 _ticket_id in ListTicket)
      {
        _ticket_data = Ticket.LoadTicket(_ticket_id, SqlTrx, Params.is_apply_tax);

        // RAB 23-FEB-2016
        // ETP 19-APR-2016
        // XGJ 11-OCT-2016
        if (_allow_pay_tickets_pending_print)
        {
          if (_ticket_data.TicketID <= 0 || ((_ticket_data.Status != TITO_TICKET_STATUS.VALID && _ticket_data.Status != TITO_TICKET_STATUS.PENDING_PRINT) && _ticket_data.Status != TITO_TICKET_STATUS.PENDING_CANCEL && _ticket_data.Status != TITO_TICKET_STATUS.EXPIRED))
          {
            return false;
          }
        }
        else
        {
          if (_ticket_data.TicketID <= 0 || (_ticket_data.Status != TITO_TICKET_STATUS.VALID && _ticket_data.Status != TITO_TICKET_STATUS.PENDING_CANCEL && _ticket_data.Status != TITO_TICKET_STATUS.EXPIRED))
          {
            return false;
          }
        }

        if (_ticket_data.Status == TITO_TICKET_STATUS.EXPIRED)
        {
          CheckGracePeriod(_ticket_data.ExpirationDateTime, out _expiration_limit_date);

          // RAB 23-FEB-2016
          // ETP 19-APR-2016
          if (_allow_pay_tickets_pending_print)
          {
            if (_ticket_data.TicketID <= 0 || ((_ticket_data.Status != TITO_TICKET_STATUS.VALID && _ticket_data.Status != TITO_TICKET_STATUS.PENDING_PRINT) && _ticket_data.Status != TITO_TICKET_STATUS.PENDING_CANCEL && _ticket_data.Status != TITO_TICKET_STATUS.EXPIRED))
            {
              if (_ticket_data.Status == TITO_TICKET_STATUS.EXPIRED && (_expiration_limit_date == DateTime.MinValue || _ticket_data.ExpirationDateTime > _expiration_limit_date))
              {
                return false;
              }
            }
          }
          else
          {
            if (_ticket_data.TicketID <= 0 || (_ticket_data.Status != TITO_TICKET_STATUS.VALID && _ticket_data.Status != TITO_TICKET_STATUS.PENDING_CANCEL && _ticket_data.Status != TITO_TICKET_STATUS.EXPIRED))
            {
              if (_ticket_data.Status == TITO_TICKET_STATUS.EXPIRED && (_expiration_limit_date == DateTime.MinValue || _ticket_data.ExpirationDateTime > _expiration_limit_date))
              {
                return false;
              }
            }
          }

        }
        _tickets.Add(_ticket_data);
        _total_tickets_amount += _ticket_data.Amount;

        if (_ticket_data.Status == TITO_TICKET_STATUS.PENDING_PRINT)
        {
          Log.Message("BusinessLogic RedeemTicketList: Ticket " + _ticket_data.ValidationNumber + " has status PENDING PRINTING");
        }

      }

      if (Params.out_operation_id == 0)
      {
        if (!Operations.DB_InsertOperation(OperationCode.CASH_OUT,
                                           Params.in_account.AccountId,
                                           Cashier.SessionId,
                                           0,
                                           Params.out_promotion_id,
                                           _total_tickets_amount,
                                           0,
                                 0,                      // Operation Data
                                           Params.AccountOperationReasonId,
                                           Params.AccountOperationComment,
                                 out Params.out_operation_id, SqlTrx))
        {
          Log.Error("RedeemTicketList. Error DB_InsertOperation.");

          return false;
        }
      }

      foreach (Ticket _ticket in _tickets)
      {
        // If status is PENDING_CANCEL set _cm_details
        if (_ticket.Status == TITO_TICKET_STATUS.PENDING_CANCEL) _cm_details = Resource.String("STR_TICKET_PAID_IN_PENDING_CANCEL_STATUS");

        // Update ticket status
        _ticket.Status = _ticket.Status == TITO_TICKET_STATUS.EXPIRED ? TITO_TICKET_STATUS.PAID_AFTER_EXPIRATION : TITO_TICKET_STATUS.REDEEMED;
        if (OperationCode == Common.OperationCode.CHIPS_SWAP)
        {
          _ticket.Status = TITO_TICKET_STATUS.SWAP_FOR_CHIPS;
        }
        _ticket.LastActionDateTime = WGDB.Now;
        _ticket.LastActionTerminalID = _session_info.TerminalId;
        _ticket.LastActionTerminalType = (Int32)TITO_TERMINAL_TYPE.CASHIER;
        _ticket.LastActionAccountID = _ticket.CreatedAccountID;
        //_ticket.LastActionCashierSessionId = _session_info.CashierSessionId;
        _ticket.MoneyCollectionID = _money_collection_id;

        if (!Ticket.DB_UpdateTicket(_ticket, SqlTrx))
        {
          return false;
        }

        switch (_ticket.TicketType)
        {
          case TITO_TICKET_TYPE.HANDPAY:
          case TITO_TICKET_TYPE.JACKPOT:

            Int64 _play_session_id;
            Int16 _handpay_error_msg;

            // Check if ticket has a valid Handpay
            _handpay_param = new Handpays.HandpayRegisterOrCancelParameters();

            // DRV 19-03-14: Calculate amounts for expected money collection totalizator
            _money_collection.ExpectedRETicketAmount += _ticket.Amount;
            _money_collection.ExpectedRETicketCount += 1;

            WSI.Common.TITO.HandPay.TICKET_HANDPAY_STATUS _ticket_handpay_status = WSI.Common.TITO.HandPay.GetTicketHandpayStatus(_ticket, out _handpay_param, SqlTrx);
            if (_ticket_handpay_status != WSI.Common.TITO.HandPay.TICKET_HANDPAY_STATUS.VALID)
            {
              Log.Message(String.Format("The Ticket handpay status is not valid. Status [{0}], TicketId [{1}]", _ticket_handpay_status.ToString(), _ticket.TicketID));
              return false;
            }

            // Set the account 
            if (_card_for_handpay == null)
            {
              _card_for_handpay = new CardData();
              CardData.DB_CardGetAllData(Params.in_account.AccountId, _card_for_handpay, false);
              _card_for_handpay.AccountBalance = MultiPromos.AccountBalance.Zero;
            }
            _handpay_param.CardData = _card_for_handpay;

            _voucher_number = new VoucherValidationNumber();
            _voucher_number.Amount = _ticket.Amount;
            _voucher_number.CreatedTerminalName = _handpay_param.TerminalName;
            _voucher_number.TicketId = _ticket.TicketID;
            _voucher_number.TicketType = _ticket.TicketType;
            _voucher_number.ValidationNumber = _ticket.ValidationNumber;
            _voucher_number.ValidationType = _ticket.ValidationType;
            _voucher_number.CreatedTerminalType = _ticket.CreatedTerminalType;

            // Register the handpay
            if (!Handpays.RegisterOrCancel(_handpay_param, Cashier.CashierSessionInfo(), false, _voucher_number, OperationCode, ref Params.out_operation_id, out _play_session_id, out _handpay_error_msg, SqlTrx))
            {
              Log.Message("RedeemTicketList: RegisterOrCancel: Ticket [" + _ticket.TicketID + "] unable to register handpay.");
              return false;
            }

            // Update ticket play session
            if (!WSI.Common.TITO.HandPay.DB_SetTicketPlaySessionId(_ticket.TicketID, _play_session_id, SqlTrx))
            {
              Log.Message("RedeemTicketList: DB_SetTicketPlaySessionId: Ticket [" + _ticket.TicketID + "] unable to update play session.");
              return false;
            }

            // Add voucher to the list
            HandpayVouchers.Add(_handpay_param.Voucher);

            break;

          default:

            // DRV 19-03-14: Calculate amounts for expected money collection totalizator
            switch (_ticket.TicketType)
            {
              case TITO_TICKET_TYPE.PROMO_REDEEM:
                _money_collection.ExpectedPromoRETicketAmount += _ticket.Amount;
                _money_collection.ExpectedPromoRETicketCount += 1;
                break;
              case TITO_TICKET_TYPE.OFFLINE:
              case TITO_TICKET_TYPE.CASHABLE:
                _money_collection.ExpectedRETicketAmount += _ticket.Amount;
                _money_collection.ExpectedRETicketCount += 1;
                break;
            }

            // Normal pay process for the tickets 
            // Create the cash_out operation
            if (OperationCode == Common.OperationCode.CHIPS_SWAP)
            {
              continue;
            }
            if (!Utils.AccountBalanceIn(Params.in_account.AccountId, _ticket.Amount, out _ini_balance, out _final_balance, SqlTrx))
            {
              return false;
            }

            // DHA 25-APR-2014 modified/added TITO tickets movements
            if (_ticket.TicketType != TITO_TICKET_TYPE.OFFLINE)
            {
              if (_ticket.TicketType == TITO_TICKET_TYPE.CASHABLE)
              {
                if (_ticket.Status == TITO_TICKET_STATUS.PAID_AFTER_EXPIRATION)
                {
                  _account_mov_type = MovementType.TITO_TicketCashierExpiredPaidCashable;//new
                  _cashier_mov_type = CASHIER_MOVEMENT.TITO_TICKET_CASHIER_EXPIRED_PAID_CASHABLE;//new
                }
                else
                {
                  _account_mov_type = MovementType.TITO_TicketCashierPaidCashable;
                  _cashier_mov_type = CASHIER_MOVEMENT.TITO_TICKET_CASHIER_PAID_CASHABLE;
                }
              }
              else
              {
                if (_ticket.Status == TITO_TICKET_STATUS.PAID_AFTER_EXPIRATION)
                {
                  _account_mov_type = MovementType.TITO_TicketCashierExpiredPaidPromoRedeemable;
                  _cashier_mov_type = CASHIER_MOVEMENT.TITO_TICKET_CASHIER_EXPIRED_PAID_PROMO_REDEEMABLE;
                }
                else
                {
                  _account_mov_type = MovementType.TITO_TicketCashierPaidPromoRedeemable;
                  _cashier_mov_type = CASHIER_MOVEMENT.TITO_TICKET_CASHIER_PAID_PROMO_REDEEMABLE;
                }
              }
              _account_movements.Add(Params.out_operation_id, Params.in_account.AccountId, _account_mov_type,
                           _ini_balance.TotalBalance, 0, _ticket.Amount, _final_balance.TotalBalance);
              _cashier_movements.Add(Params.out_operation_id, _cashier_mov_type, _ticket.Amount, Params.in_account.AccountId, Params.in_account.TrackData, _cm_details);
            }

            break;
        }
        // DRV 19-03-14: Calculate amounts for expected money collection totalizator
        _money_collection.ExpectedTicketAmount += _ticket.Amount;
        _money_collection.ExpectedTicketCount += 1;

        // Insert Cashier/Account movements
      }

      //Updates Money collection totalizators
      if (!_money_collection.DB_UpdateExpectedValues(SqlTrx))
      {
        return false;
      }

      // We must set operation_id to min value because it will be updated later with the correct operation_id
      if (!_account_movements.Save(SqlTrx))
      {
        return false;
      }

      // We must set operation_id to min value because it will be updated later with the correct operation_id
      if (!_cashier_movements.Save(SqlTrx))
      {
        return false;
      }

      return true;
    }

    public static void CheckGracePeriod(DateTime ExpirationDate, out DateTime DateLimit)
    {
      Object _grace_period;
      DateTime _grace_period_limit;

      _grace_period = Misc.GetTITOPaymentGracePeriod();
      _grace_period_limit = DateTime.MinValue;

      if (_grace_period != null)
      {
        if (_grace_period is Int32)
        {
          _grace_period_limit = ExpirationDate.AddDays((Int32)_grace_period);
        }
        else if (_grace_period is DateTime)
        {
          _grace_period_limit = new DateTime(ExpirationDate.Year, ((DateTime)_grace_period).Month, ((DateTime)_grace_period).Day);
        }
      }

      if (ExpirationDate > _grace_period_limit)
      {
        DateLimit = _grace_period_limit.AddYears(1);
      }
      else
      {
        DateLimit = _grace_period_limit;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Execute the ticket cancelation method in common Transaction
    // 
    //  PARAMS:
    //      - INPUT:
    //        - ParamsTicketOperation
    //        - SqlTransaction
    //
    //      - OUTPUT:
    //
    //  RETURNS:
    //        - TRUE: all is OK
    //        - FALSE: something went wrong
    //
    public static Boolean TicketReissue(ParamsTicketOperation Params, Ticket Ticket, Boolean MustBeCollected, out Decimal FinalBalance, SqlTransaction SqlTrx)
    {
      MultiPromos.AccountBalance _ini_balance;
      CashierSessionInfo _session_info;
      CashierMovementsTable _cashier_movements;
      AccountMovementsTable _account_movements;
      MoneyCollection _money_collection;
      Int64 _money_collection_id;
      bool _allow_pay_tickets_pending_print;

      _allow_pay_tickets_pending_print = GeneralParam.GetBoolean("TITO", "PendingPrint.AllowToRedeem", true);
      FinalBalance = 0;

      if (_allow_pay_tickets_pending_print)
      {
        if (Ticket.ValidationNumber <= 0 || (Ticket.Status != TITO_TICKET_STATUS.VALID && Ticket.Status != TITO_TICKET_STATUS.PENDING_PRINT))
        {
          return false;
        }
      }
      else
      {
        if (Ticket.ValidationNumber <= 0 || Ticket.Status != TITO_TICKET_STATUS.VALID)
        {
          return false;
        }
      }


      if (MustBeCollected)
      {
        if (!MoneyCollection.GetCashierMoneyCollectionId(Params.session_info.CashierSessionId, out _money_collection_id, SqlTrx))
        {
          return false;
        }

        _money_collection = MoneyCollection.DB_Read(_money_collection_id, SqlTrx);

        if (Ticket.TicketType == TITO_TICKET_TYPE.OFFLINE)
        {
          Ticket.MoneyCollectionID = _money_collection_id;
          _money_collection.ExpectedPromoNRTicketAmount += Ticket.Amount;
          _money_collection.ExpectedPromoNRTicketCount += 1;
          _money_collection.ExpectedTicketAmount += Ticket.Amount;
          _money_collection.ExpectedTicketCount += 1;
        }
        if (!_money_collection.DB_UpdateExpectedValues(SqlTrx))
        {
          return false;
        }
      }

      Ticket.Status = TITO_TICKET_STATUS.DISCARDED;
      Ticket.LastActionAccountID = Params.in_account.AccountId;
      Ticket.LastActionDateTime = WGDB.Now;
      //Ticket.LastActionCashierSessionId = Params.session_info.CashierSessionId;
      Ticket.LastActionTerminalID = Params.session_info.TerminalId;
      Ticket.LastActionTerminalType = (Int16)TITO_TERMINAL_TYPE.CASHIER;

      if (!Ticket.DB_UpdateTicket(Ticket, SqlTrx))
      {
        return false;
      }

      if (!MultiPromos.Trx_UpdateAccountBalance(Params.in_account.AccountId, MultiPromos.AccountBalance.Zero, out _ini_balance, SqlTrx))
      {
        return false;
      }

      FinalBalance = _ini_balance.TotalBalance + Ticket.Amount;

      _session_info = Params.session_info;

      _account_movements = new AccountMovementsTable(_session_info);
      _cashier_movements = new CashierMovementsTable(_session_info);

      if (Ticket.TicketType == TITO_TICKET_TYPE.OFFLINE)
      {
        //_account_movements.Add(Params.out_operation_id, Params.in_account.AccountId, MovementType.TITO_TicketOffline,
        //                       _ini_balance.TotalBalance, 0, Ticket.Amount, FinalBalance,
        //                       ValidationNumberManager.FormatValidationNumber(Ticket.ValidationNumber, (Int32)Ticket.ValidationType, true));

        //_cashier_movements.Add(Params.out_operation_id, CASHIER_MOVEMENT.TITO_TICKET_OFFLINE, Ticket.Amount,
        //                       Params.in_account.AccountId, "", ValidationNumberManager.FormatValidationNumber(Ticket.ValidationNumber, (Int32)Ticket.ValidationType, true));
      }
      else
      {
        _account_movements.Add(Params.out_operation_id, Params.in_account.AccountId, MovementType.TITO_TicketReissue,
                               _ini_balance.TotalBalance, 0, Ticket.Amount, FinalBalance,
                               ValidationNumberManager.FormatValidationNumber(Ticket.ValidationNumber, (Int32)Ticket.ValidationType, true));

        _cashier_movements.Add(Params.out_operation_id, CASHIER_MOVEMENT.TITO_TICKET_REISSUE, Ticket.Amount,
                             Params.in_account.AccountId, "", ValidationNumberManager.FormatValidationNumber(Ticket.ValidationNumber, (Int32)Ticket.ValidationType, true));
      }
      if (!_account_movements.Save(SqlTrx))
      {
        return false;
      }

      if (!_cashier_movements.Save(SqlTrx))
      {
        return false;
      }

      if (Ticket.Status == TITO_TICKET_STATUS.PENDING_PRINT)
      {
        Log.Message("BusinessLogic RedeemTicketList: Ticket " + Ticket.ValidationNumber + " has status PENDING PRINTING");
      }

      return true;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Execute the ticket creation method in common Transaction
    // 
    //  PARAMS:
    //      - INPUT:
    //        - ParamsTicketOperation
    //        - UpdateAccount
    //        - SqlTransaction
    //
    //      - OUTPUT:
    //        - Ticket
    //
    //  RETURNS:
    //        - TRUE: all is OK
    //        - FALSE: something went wrong
    //
    public static Boolean CreateTicket(ParamsTicketOperation Params, Boolean UpdateAccount, out Ticket Ticket, SqlTransaction SqlTrx)
    {
      return CreateTicket(Params, UpdateAccount, 0, out Ticket, SqlTrx);
    } // CreateTicket

    public static Boolean CreateTicket(ParamsTicketOperation Params, Boolean UpdateAccount, Decimal InitialBalance, out Ticket Ticket, SqlTransaction SqlTrx)
    {
      Params.cashier_terminal_id = Cashier.TerminalId;
      //CreateTicket has been moved to WSI.Common.Cashier
      return WSI.Common.Cashier.Common_CreateTicket(Params, UpdateAccount, InitialBalance, out Ticket, SqlTrx);
    } // CreateTicket

    //------------------------------------------------------------------------------
    // PURPOSE: Return a readable string representing Ticket Status
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    //  RETURNS:
    //       - String: Ticket status string
    //
    public static string StatusName(TITO_TICKET_STATUS Status)
    {
      string _result;

      switch (Status)
      {
        case TITO_TICKET_STATUS.EXPIRED:
          _result = Resource.String("STR_UC_TICKET_EXPIRED");
          break;
        case TITO_TICKET_STATUS.REDEEMED:
          _result = Resource.String("STR_UC_TICKET_REDEEM");
          break;
        case TITO_TICKET_STATUS.CANCELED:
          _result = Resource.String("STR_UC_TICKETS_18");
          break;
        case TITO_TICKET_STATUS.VALID:
          _result = Resource.String("STR_UC_TICKET_VALID");
          break;
        case TITO_TICKET_STATUS.DISCARDED:
          _result = Resource.String("STR_UC_TICKETS_14");
          break;
        case TITO_TICKET_STATUS.PENDING_CANCEL:
          _result = Resource.String("STR_UC_TICKETS_15");
          break;
        case TITO_TICKET_STATUS.PAID_AFTER_EXPIRATION:
          _result = Resource.String("STR_UC_TICKETS_PAID_EXPIRED");
          break;
        case TITO_TICKET_STATUS.SWAP_FOR_CHIPS:
          _result = Resource.String("STR_SWAP_CHIPS_ONE");
          break;
        case TITO_TICKET_STATUS.PENDING_PRINT:
          _result = Resource.String("STR_UC_TICKET_PENDING_PRINT");
          break;
        default:
          _result = "";
          break;
      }

      return _result;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Return a readable string representing Ticket Type
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Ticket type
    //
    //      - OUTPUT:
    //
    //  RETURNS:
    //       - String: Ticket type string
    //
    public static String TypeName(TITO_TICKET_TYPE Type)
    {
      String _result;

      switch (Type)
      {
        case TITO_TICKET_TYPE.CASHABLE:
          _result = Resource.String("STR_TICKET_TYPE_CASHABLE");
          break;
        case TITO_TICKET_TYPE.HANDPAY:
          _result = Resource.String("STR_TICKET_TYPE_HANDPAY");
          break;
        case TITO_TICKET_TYPE.JACKPOT:
          _result = Resource.String("STR_TICKET_TYPE_JACKPOT");
          break;
        case TITO_TICKET_TYPE.OFFLINE:
          _result = Resource.String("STR_TICKET_TYPE_OFFLINE");
          break;
        case TITO_TICKET_TYPE.PROMO_NONREDEEM:
          _result = Resource.String("STR_TICKET_TYPE_PROMO_NONREDEEM");
          break;
        case TITO_TICKET_TYPE.PROMO_REDEEM:
          _result = Resource.String("STR_TICKET_TYPE_PROMO_REDEEM");
          break;
        default:
          _result = String.Empty;
          break;
      }

      return _result;
    }

    #endregion
  }
}