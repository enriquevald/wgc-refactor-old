namespace WSI.Cashier
{
  partial class uc_ticket_promo
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(uc_ticket_promo));
      this.pnl_title = new System.Windows.Forms.Panel();
      this.lbl_title = new WSI.Cashier.Controls.uc_label();
      this.pb_promo_icon = new System.Windows.Forms.PictureBox();
      this.label7 = new System.Windows.Forms.Label();
      this.lbl_separator_1 = new WSI.Cashier.Controls.uc_label();
      this.pnl_main = new System.Windows.Forms.Panel();
      this.pnl_common = new System.Windows.Forms.Panel();
      this.dgv_common = new WSI.Cashier.Controls.uc_DataGridView();
      this.btn_promos_change_view = new WSI.Cashier.Controls.uc_round_button();
      this.lbl_promo_reward_value = new WSI.Cashier.Controls.uc_label();
      this.lbl_promo_reward_text = new WSI.Cashier.Controls.uc_label();
      this.gb_digits_box = new WSI.Cashier.Controls.uc_round_panel();
      this.txt_amount = new WSI.Cashier.Controls.uc_round_textbox();
      this.btn_num_1 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_2 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_3 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_4 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_0 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_5 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_clear = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_6 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_9 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_7 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_8 = new WSI.Cashier.Controls.uc_round_button();
      this.pnl_bottom = new System.Windows.Forms.Panel();
      this.uc_round_button1 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_assign_to_account = new WSI.Cashier.Controls.uc_round_button();
      this.gb_filter_box = new WSI.Cashier.Controls.uc_round_panel();
      this.promo_filter = new WSI.Cashier.uc_promo_filter();
      this.btn_create_ticket = new WSI.Cashier.Controls.uc_round_button();
      this.btn_cancel = new WSI.Cashier.Controls.uc_round_button();
      this.pnl_title.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pb_promo_icon)).BeginInit();
      this.pnl_main.SuspendLayout();
      this.pnl_common.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dgv_common)).BeginInit();
      this.gb_digits_box.SuspendLayout();
      this.pnl_bottom.SuspendLayout();
      this.gb_filter_box.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_title
      // 
      this.pnl_title.BackColor = System.Drawing.Color.Transparent;
      this.pnl_title.Controls.Add(this.lbl_title);
      this.pnl_title.Controls.Add(this.pb_promo_icon);
      this.pnl_title.Controls.Add(this.label7);
      this.pnl_title.Controls.Add(this.lbl_separator_1);
      this.pnl_title.Dock = System.Windows.Forms.DockStyle.Top;
      this.pnl_title.Location = new System.Drawing.Point(0, 0);
      this.pnl_title.Name = "pnl_title";
      this.pnl_title.Size = new System.Drawing.Size(871, 71);
      this.pnl_title.TabIndex = 0;
      // 
      // lbl_title
      // 
      this.lbl_title.BackColor = System.Drawing.Color.Transparent;
      this.lbl_title.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_title.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_title.Location = new System.Drawing.Point(77, 20);
      this.lbl_title.Name = "lbl_title";
      this.lbl_title.Size = new System.Drawing.Size(781, 24);
      this.lbl_title.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_title.TabIndex = 90;
      this.lbl_title.Text = "XPROMOTION";
      // 
      // pb_promo_icon
      // 
      this.pb_promo_icon.BackColor = System.Drawing.Color.Transparent;
      this.pb_promo_icon.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
      this.pb_promo_icon.Image = global::WSI.Cashier.Properties.Resources.promos;
      this.pb_promo_icon.Location = new System.Drawing.Point(10, 7);
      this.pb_promo_icon.Name = "pb_promo_icon";
      this.pb_promo_icon.Size = new System.Drawing.Size(50, 50);
      this.pb_promo_icon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
      this.pb_promo_icon.TabIndex = 89;
      this.pb_promo_icon.TabStop = false;
      // 
      // label7
      // 
      this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.label7.BackColor = System.Drawing.Color.Black;
      this.label7.Location = new System.Drawing.Point(-3, 60);
      this.label7.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(870, 1);
      this.label7.TabIndex = 0;
      // 
      // lbl_separator_1
      // 
      this.lbl_separator_1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_separator_1.BackColor = System.Drawing.Color.Transparent;
      this.lbl_separator_1.Font = new System.Drawing.Font("Open Sans Semibold", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_separator_1.Location = new System.Drawing.Point(-5, 130);
      this.lbl_separator_1.Name = "lbl_separator_1";
      this.lbl_separator_1.Size = new System.Drawing.Size(967, 2);
      this.lbl_separator_1.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.lbl_separator_1.TabIndex = 87;
      // 
      // pnl_main
      // 
      this.pnl_main.BackColor = System.Drawing.Color.Transparent;
      this.pnl_main.Controls.Add(this.pnl_common);
      this.pnl_main.Controls.Add(this.gb_digits_box);
      this.pnl_main.Dock = System.Windows.Forms.DockStyle.Top;
      this.pnl_main.Location = new System.Drawing.Point(0, 71);
      this.pnl_main.Name = "pnl_main";
      this.pnl_main.Size = new System.Drawing.Size(871, 390);
      this.pnl_main.TabIndex = 1;
      // 
      // pnl_common
      // 
      this.pnl_common.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.pnl_common.BackColor = System.Drawing.Color.Transparent;
      this.pnl_common.Controls.Add(this.dgv_common);
      this.pnl_common.Controls.Add(this.btn_promos_change_view);
      this.pnl_common.Controls.Add(this.lbl_promo_reward_value);
      this.pnl_common.Controls.Add(this.lbl_promo_reward_text);
      this.pnl_common.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.pnl_common.ForeColor = System.Drawing.Color.Black;
      this.pnl_common.Location = new System.Drawing.Point(18, 20);
      this.pnl_common.Name = "pnl_common";
      this.pnl_common.Size = new System.Drawing.Size(587, 349);
      this.pnl_common.TabIndex = 0;
      // 
      // dgv_common
      // 
      this.dgv_common.AllowUserToAddRows = false;
      this.dgv_common.AllowUserToDeleteRows = false;
      this.dgv_common.AllowUserToResizeColumns = false;
      this.dgv_common.AllowUserToResizeRows = false;
      this.dgv_common.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.dgv_common.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.dgv_common.ColumnHeaderHeight = 35;
      this.dgv_common.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
      dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      dataGridViewCellStyle1.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
      dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
      dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
      this.dgv_common.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
      this.dgv_common.ColumnHeadersHeight = 35;
      this.dgv_common.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
      this.dgv_common.CornerRadius = 10;
      this.dgv_common.DefaultCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.dgv_common.DefaultCellForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.dgv_common.DefaultCellSelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.dgv_common.DefaultCellSelectionForeColor = System.Drawing.Color.White;
      dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      dataGridViewCellStyle2.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.White;
      dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
      this.dgv_common.DefaultCellStyle = dataGridViewCellStyle2;
      this.dgv_common.Dock = System.Windows.Forms.DockStyle.Top;
      this.dgv_common.EnableHeadersVisualStyles = false;
      this.dgv_common.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.dgv_common.GridColor = System.Drawing.Color.LightGray;
      this.dgv_common.HeaderBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
      this.dgv_common.HeaderDefaultCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.dgv_common.HeaderDefaultCellForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.dgv_common.HeaderImages = null;
      this.dgv_common.Location = new System.Drawing.Point(0, 0);
      this.dgv_common.Name = "dgv_common";
      this.dgv_common.ReadOnly = true;
      this.dgv_common.RowHeadersVisible = false;
      this.dgv_common.RowHeadersWidth = 20;
      this.dgv_common.RowTemplate.Height = 35;
      this.dgv_common.RowTemplateHeight = 35;
      this.dgv_common.Size = new System.Drawing.Size(587, 218);
      this.dgv_common.Style = WSI.Cashier.Controls.uc_DataGridView.DataGridViewStyle.WITH_ALL_ROUND_CORNERS;
      this.dgv_common.TabIndex = 0;
      this.dgv_common.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_common_CellClick);
      this.dgv_common.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.dgv_common_RowPrePaint);
      // 
      // btn_promos_change_view
      // 
      this.btn_promos_change_view.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(47)))), ((int)(((byte)(50)))));
      this.btn_promos_change_view.FlatAppearance.BorderSize = 0;
      this.btn_promos_change_view.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_promos_change_view.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_promos_change_view.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_promos_change_view.Image = null;
      this.btn_promos_change_view.IsSelected = false;
      this.btn_promos_change_view.Location = new System.Drawing.Point(11, 225);
      this.btn_promos_change_view.Margin = new System.Windows.Forms.Padding(0);
      this.btn_promos_change_view.Name = "btn_promos_change_view";
      this.btn_promos_change_view.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_promos_change_view.Size = new System.Drawing.Size(205, 50);
      this.btn_promos_change_view.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.TERTIARY;
      this.btn_promos_change_view.TabIndex = 1;
      this.btn_promos_change_view.Text = "XALLPROMOS";
      this.btn_promos_change_view.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.btn_promos_change_view.UseVisualStyleBackColor = false;
      this.btn_promos_change_view.Click += new System.EventHandler(this.btn_promos_change_view_Click);
      // 
      // lbl_promo_reward_value
      // 
      this.lbl_promo_reward_value.AutoSize = true;
      this.lbl_promo_reward_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_promo_reward_value.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_promo_reward_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_promo_reward_value.Location = new System.Drawing.Point(8, 307);
      this.lbl_promo_reward_value.Name = "lbl_promo_reward_value";
      this.lbl_promo_reward_value.Size = new System.Drawing.Size(151, 17);
      this.lbl_promo_reward_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLUE;
      this.lbl_promo_reward_value.TabIndex = 3;
      this.lbl_promo_reward_value.Text = "x$9.999 non-redeemable";
      this.lbl_promo_reward_value.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_promo_reward_text
      // 
      this.lbl_promo_reward_text.AutoSize = true;
      this.lbl_promo_reward_text.BackColor = System.Drawing.Color.Transparent;
      this.lbl_promo_reward_text.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_promo_reward_text.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_promo_reward_text.Location = new System.Drawing.Point(8, 291);
      this.lbl_promo_reward_text.Name = "lbl_promo_reward_text";
      this.lbl_promo_reward_text.Size = new System.Drawing.Size(81, 17);
      this.lbl_promo_reward_text.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_promo_reward_text.TabIndex = 2;
      this.lbl_promo_reward_text.Text = "xPlayer gets:";
      // 
      // gb_digits_box
      // 
      this.gb_digits_box.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.gb_digits_box.BackColor = System.Drawing.Color.Transparent;
      this.gb_digits_box.BorderColor = System.Drawing.Color.Empty;
      this.gb_digits_box.Controls.Add(this.txt_amount);
      this.gb_digits_box.Controls.Add(this.btn_num_1);
      this.gb_digits_box.Controls.Add(this.btn_num_2);
      this.gb_digits_box.Controls.Add(this.btn_num_3);
      this.gb_digits_box.Controls.Add(this.btn_num_4);
      this.gb_digits_box.Controls.Add(this.btn_num_0);
      this.gb_digits_box.Controls.Add(this.btn_num_5);
      this.gb_digits_box.Controls.Add(this.btn_num_clear);
      this.gb_digits_box.Controls.Add(this.btn_num_6);
      this.gb_digits_box.Controls.Add(this.btn_num_9);
      this.gb_digits_box.Controls.Add(this.btn_num_7);
      this.gb_digits_box.Controls.Add(this.btn_num_8);
      this.gb_digits_box.CornerRadius = 10;
      this.gb_digits_box.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_digits_box.ForeColor = System.Drawing.Color.Black;
      this.gb_digits_box.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.gb_digits_box.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_digits_box.HeaderHeight = 55;
      this.gb_digits_box.HeaderSubText = null;
      this.gb_digits_box.HeaderText = null;
      this.gb_digits_box.Location = new System.Drawing.Point(621, 20);
      this.gb_digits_box.Name = "gb_digits_box";
      this.gb_digits_box.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_digits_box.Size = new System.Drawing.Size(237, 349);
      this.gb_digits_box.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NONE;
      this.gb_digits_box.TabIndex = 1;
      this.gb_digits_box.Text = "xInput Amount";
      // 
      // txt_amount
      // 
      this.txt_amount.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_amount.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_amount.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.txt_amount.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.txt_amount.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_amount.CornerRadius = 0;
      this.txt_amount.Font = new System.Drawing.Font("Open Sans Semibold", 40F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_amount.Location = new System.Drawing.Point(0, 7);
      this.txt_amount.MaxLength = 18;
      this.txt_amount.Multiline = false;
      this.txt_amount.Name = "txt_amount";
      this.txt_amount.PasswordChar = '\0';
      this.txt_amount.ReadOnly = true;
      this.txt_amount.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_amount.SelectedText = "";
      this.txt_amount.SelectionLength = 0;
      this.txt_amount.SelectionStart = 0;
      this.txt_amount.Size = new System.Drawing.Size(237, 44);
      this.txt_amount.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.CALCULATOR;
      this.txt_amount.TabIndex = 0;
      this.txt_amount.TabStop = false;
      this.txt_amount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      this.txt_amount.UseSystemPasswordChar = false;
      this.txt_amount.WaterMark = null;
      this.txt_amount.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(153)))), ((int)(((byte)(49)))));
      this.txt_amount.TextChanged += new System.EventHandler(this.txt_amount_TextChanged);
      this.txt_amount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_amount_KeyPress);
      // 
      // btn_num_1
      // 
      this.btn_num_1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_1.CornerRadius = 0;
      this.btn_num_1.FlatAppearance.BorderSize = 0;
      this.btn_num_1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_1.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_1.Image = null;
      this.btn_num_1.IsSelected = false;
      this.btn_num_1.Location = new System.Drawing.Point(13, 67);
      this.btn_num_1.Name = "btn_num_1";
      this.btn_num_1.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_1.SelectedColor = System.Drawing.Color.Empty;
      this.btn_num_1.Size = new System.Drawing.Size(65, 60);
      this.btn_num_1.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_1.TabIndex = 1;
      this.btn_num_1.TabStop = false;
      this.btn_num_1.Text = "1";
      this.btn_num_1.UseVisualStyleBackColor = false;
      this.btn_num_1.Click += new System.EventHandler(this.btn_num_1_Click);
      this.btn_num_1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_amount_KeyPress);
      // 
      // btn_num_2
      // 
      this.btn_num_2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_2.CornerRadius = 0;
      this.btn_num_2.FlatAppearance.BorderSize = 0;
      this.btn_num_2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_2.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_2.Image = null;
      this.btn_num_2.IsSelected = false;
      this.btn_num_2.Location = new System.Drawing.Point(88, 67);
      this.btn_num_2.Name = "btn_num_2";
      this.btn_num_2.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_2.SelectedColor = System.Drawing.Color.Empty;
      this.btn_num_2.Size = new System.Drawing.Size(65, 60);
      this.btn_num_2.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_2.TabIndex = 2;
      this.btn_num_2.TabStop = false;
      this.btn_num_2.Text = "2";
      this.btn_num_2.UseVisualStyleBackColor = false;
      this.btn_num_2.Click += new System.EventHandler(this.btn_num_2_Click);
      this.btn_num_2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_amount_KeyPress);
      // 
      // btn_num_3
      // 
      this.btn_num_3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_3.CornerRadius = 0;
      this.btn_num_3.FlatAppearance.BorderSize = 0;
      this.btn_num_3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_3.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_3.Image = null;
      this.btn_num_3.IsSelected = false;
      this.btn_num_3.Location = new System.Drawing.Point(163, 67);
      this.btn_num_3.Name = "btn_num_3";
      this.btn_num_3.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_3.SelectedColor = System.Drawing.Color.Empty;
      this.btn_num_3.Size = new System.Drawing.Size(65, 60);
      this.btn_num_3.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_3.TabIndex = 3;
      this.btn_num_3.TabStop = false;
      this.btn_num_3.Text = "3";
      this.btn_num_3.UseVisualStyleBackColor = false;
      this.btn_num_3.Click += new System.EventHandler(this.btn_num_3_Click);
      this.btn_num_3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_amount_KeyPress);
      // 
      // btn_num_4
      // 
      this.btn_num_4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_4.CornerRadius = 0;
      this.btn_num_4.FlatAppearance.BorderSize = 0;
      this.btn_num_4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_4.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_4.Image = null;
      this.btn_num_4.IsSelected = false;
      this.btn_num_4.Location = new System.Drawing.Point(13, 138);
      this.btn_num_4.Name = "btn_num_4";
      this.btn_num_4.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_4.SelectedColor = System.Drawing.Color.Empty;
      this.btn_num_4.Size = new System.Drawing.Size(65, 60);
      this.btn_num_4.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_4.TabIndex = 4;
      this.btn_num_4.TabStop = false;
      this.btn_num_4.Text = "4";
      this.btn_num_4.UseVisualStyleBackColor = false;
      this.btn_num_4.Click += new System.EventHandler(this.btn_num_4_Click);
      this.btn_num_4.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_amount_KeyPress);
      // 
      // btn_num_0
      // 
      this.btn_num_0.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_0.CornerRadius = 0;
      this.btn_num_0.FlatAppearance.BorderSize = 0;
      this.btn_num_0.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_0.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_0.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_0.Image = null;
      this.btn_num_0.IsSelected = false;
      this.btn_num_0.Location = new System.Drawing.Point(13, 280);
      this.btn_num_0.Name = "btn_num_0";
      this.btn_num_0.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_0.SelectedColor = System.Drawing.Color.Empty;
      this.btn_num_0.Size = new System.Drawing.Size(65, 60);
      this.btn_num_0.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_0.TabIndex = 10;
      this.btn_num_0.TabStop = false;
      this.btn_num_0.Text = "0";
      this.btn_num_0.UseVisualStyleBackColor = false;
      this.btn_num_0.Click += new System.EventHandler(this.btn_num_0_Click);
      this.btn_num_0.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_amount_KeyPress);
      // 
      // btn_num_5
      // 
      this.btn_num_5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_5.CornerRadius = 0;
      this.btn_num_5.FlatAppearance.BorderSize = 0;
      this.btn_num_5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_5.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_5.Image = null;
      this.btn_num_5.IsSelected = false;
      this.btn_num_5.Location = new System.Drawing.Point(88, 138);
      this.btn_num_5.Name = "btn_num_5";
      this.btn_num_5.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_5.SelectedColor = System.Drawing.Color.Empty;
      this.btn_num_5.Size = new System.Drawing.Size(65, 60);
      this.btn_num_5.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_5.TabIndex = 5;
      this.btn_num_5.TabStop = false;
      this.btn_num_5.Text = "5";
      this.btn_num_5.UseVisualStyleBackColor = false;
      this.btn_num_5.Click += new System.EventHandler(this.btn_num_5_Click);
      this.btn_num_5.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_amount_KeyPress);
      // 
      // btn_num_clear
      // 
      this.btn_num_clear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_num_clear.CornerRadius = 0;
      this.btn_num_clear.FlatAppearance.BorderSize = 0;
      this.btn_num_clear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_clear.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_clear.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_clear.Image = ((System.Drawing.Image)(resources.GetObject("btn_num_clear.Image")));
      this.btn_num_clear.IsSelected = false;
      this.btn_num_clear.Location = new System.Drawing.Point(88, 280);
      this.btn_num_clear.Name = "btn_num_clear";
      this.btn_num_clear.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_clear.SelectedColor = System.Drawing.Color.Empty;
      this.btn_num_clear.Size = new System.Drawing.Size(140, 60);
      this.btn_num_clear.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_BACKSPACE;
      this.btn_num_clear.TabIndex = 11;
      this.btn_num_clear.TabStop = false;
      this.btn_num_clear.UseVisualStyleBackColor = false;
      this.btn_num_clear.Click += new System.EventHandler(this.btn_num_clear_Click);
      this.btn_num_clear.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_amount_KeyPress);
      // 
      // btn_num_6
      // 
      this.btn_num_6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_6.CornerRadius = 0;
      this.btn_num_6.FlatAppearance.BorderSize = 0;
      this.btn_num_6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_6.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_6.Image = null;
      this.btn_num_6.IsSelected = false;
      this.btn_num_6.Location = new System.Drawing.Point(163, 138);
      this.btn_num_6.Name = "btn_num_6";
      this.btn_num_6.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_6.SelectedColor = System.Drawing.Color.Empty;
      this.btn_num_6.Size = new System.Drawing.Size(65, 60);
      this.btn_num_6.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_6.TabIndex = 6;
      this.btn_num_6.TabStop = false;
      this.btn_num_6.Text = "6";
      this.btn_num_6.UseVisualStyleBackColor = false;
      this.btn_num_6.Click += new System.EventHandler(this.btn_num_6_Click);
      this.btn_num_6.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_amount_KeyPress);
      // 
      // btn_num_9
      // 
      this.btn_num_9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_9.CornerRadius = 0;
      this.btn_num_9.FlatAppearance.BorderSize = 0;
      this.btn_num_9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_9.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_9.Image = null;
      this.btn_num_9.IsSelected = false;
      this.btn_num_9.Location = new System.Drawing.Point(163, 209);
      this.btn_num_9.Name = "btn_num_9";
      this.btn_num_9.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_9.SelectedColor = System.Drawing.Color.Empty;
      this.btn_num_9.Size = new System.Drawing.Size(65, 60);
      this.btn_num_9.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_9.TabIndex = 9;
      this.btn_num_9.TabStop = false;
      this.btn_num_9.Text = "9";
      this.btn_num_9.UseVisualStyleBackColor = false;
      this.btn_num_9.Click += new System.EventHandler(this.btn_num_9_Click);
      this.btn_num_9.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_amount_KeyPress);
      // 
      // btn_num_7
      // 
      this.btn_num_7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_7.CornerRadius = 0;
      this.btn_num_7.FlatAppearance.BorderSize = 0;
      this.btn_num_7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_7.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_7.Image = null;
      this.btn_num_7.IsSelected = false;
      this.btn_num_7.Location = new System.Drawing.Point(13, 209);
      this.btn_num_7.Name = "btn_num_7";
      this.btn_num_7.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_7.SelectedColor = System.Drawing.Color.Empty;
      this.btn_num_7.Size = new System.Drawing.Size(65, 60);
      this.btn_num_7.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_7.TabIndex = 7;
      this.btn_num_7.TabStop = false;
      this.btn_num_7.Text = "7";
      this.btn_num_7.UseVisualStyleBackColor = false;
      this.btn_num_7.Click += new System.EventHandler(this.btn_num_7_Click);
      this.btn_num_7.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_amount_KeyPress);
      // 
      // btn_num_8
      // 
      this.btn_num_8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_8.CornerRadius = 0;
      this.btn_num_8.FlatAppearance.BorderSize = 0;
      this.btn_num_8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_8.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_8.Image = null;
      this.btn_num_8.IsSelected = false;
      this.btn_num_8.Location = new System.Drawing.Point(88, 209);
      this.btn_num_8.Name = "btn_num_8";
      this.btn_num_8.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_8.SelectedColor = System.Drawing.Color.Empty;
      this.btn_num_8.Size = new System.Drawing.Size(65, 60);
      this.btn_num_8.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_8.TabIndex = 8;
      this.btn_num_8.TabStop = false;
      this.btn_num_8.Text = "8";
      this.btn_num_8.UseVisualStyleBackColor = false;
      this.btn_num_8.Click += new System.EventHandler(this.btn_num_8_Click);
      this.btn_num_8.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_amount_KeyPress);
      // 
      // pnl_bottom
      // 
      this.pnl_bottom.BackColor = System.Drawing.Color.Transparent;
      this.pnl_bottom.Controls.Add(this.uc_round_button1);
      this.pnl_bottom.Controls.Add(this.btn_assign_to_account);
      this.pnl_bottom.Controls.Add(this.gb_filter_box);
      this.pnl_bottom.Controls.Add(this.btn_create_ticket);
      this.pnl_bottom.Controls.Add(this.btn_cancel);
      this.pnl_bottom.Dock = System.Windows.Forms.DockStyle.Top;
      this.pnl_bottom.Location = new System.Drawing.Point(0, 461);
      this.pnl_bottom.Name = "pnl_bottom";
      this.pnl_bottom.Size = new System.Drawing.Size(871, 130);
      this.pnl_bottom.TabIndex = 90;
      // 
      // uc_round_button1
      // 
      this.uc_round_button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.uc_round_button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.uc_round_button1.FlatAppearance.BorderSize = 0;
      this.uc_round_button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.uc_round_button1.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.uc_round_button1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.uc_round_button1.Image = null;
      this.uc_round_button1.IsSelected = false;
      this.uc_round_button1.Location = new System.Drawing.Point(703, 6);
      this.uc_round_button1.Name = "uc_round_button1";
      this.uc_round_button1.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.uc_round_button1.Size = new System.Drawing.Size(155, 47);
      this.uc_round_button1.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.uc_round_button1.TabIndex = 4;
      this.uc_round_button1.Text = "XASSIGNACCOUNT";
      this.uc_round_button1.UseVisualStyleBackColor = false;
      this.uc_round_button1.Click += new System.EventHandler(this.uc_round_button1_Click);
      // 
      // btn_assign_to_account
      // 
      this.btn_assign_to_account.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_assign_to_account.FlatAppearance.BorderSize = 0;
      this.btn_assign_to_account.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_assign_to_account.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_assign_to_account.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_assign_to_account.Image = null;
      this.btn_assign_to_account.IsSelected = false;
      this.btn_assign_to_account.Location = new System.Drawing.Point(703, 56);
      this.btn_assign_to_account.Name = "btn_assign_to_account";
      this.btn_assign_to_account.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_assign_to_account.Size = new System.Drawing.Size(155, 60);
      this.btn_assign_to_account.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_assign_to_account.TabIndex = 3;
      this.btn_assign_to_account.Text = "XASSIGNACCOUNT";
      this.btn_assign_to_account.UseVisualStyleBackColor = false;
      this.btn_assign_to_account.Click += new System.EventHandler(this.btn_assign_to_account_Click);
      // 
      // gb_filter_box
      // 
      this.gb_filter_box.BackColor = System.Drawing.Color.Transparent;
      this.gb_filter_box.BorderColor = System.Drawing.Color.Empty;
      this.gb_filter_box.Controls.Add(this.promo_filter);
      this.gb_filter_box.CornerRadius = 10;
      this.gb_filter_box.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_filter_box.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gb_filter_box.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_filter_box.HeaderHeight = 35;
      this.gb_filter_box.HeaderSubText = null;
      this.gb_filter_box.HeaderText = "XFILTERTEXT";
      this.gb_filter_box.Location = new System.Drawing.Point(18, 6);
      this.gb_filter_box.Name = "gb_filter_box";
      this.gb_filter_box.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_filter_box.Size = new System.Drawing.Size(315, 110);
      this.gb_filter_box.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.gb_filter_box.TabIndex = 0;
      // 
      // promo_filter
      // 
      this.promo_filter.BackColor = System.Drawing.Color.Transparent;
      this.promo_filter.Location = new System.Drawing.Point(14, 29);
      this.promo_filter.Margin = new System.Windows.Forms.Padding(0);
      this.promo_filter.MaximumSize = new System.Drawing.Size(280, 85);
      this.promo_filter.Name = "promo_filter";
      this.promo_filter.ShowHeadLabel = false;
      this.promo_filter.Size = new System.Drawing.Size(270, 75);
      this.promo_filter.TabIndex = 0;
      this.promo_filter.VirtualMode = false;
      this.promo_filter.FilterChange += new WSI.Cashier.uc_promo_filter.FilterChangeEvent(this.promo_filter_FilterChange);
      // 
      // btn_create_ticket
      // 
      this.btn_create_ticket.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_create_ticket.FlatAppearance.BorderSize = 0;
      this.btn_create_ticket.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_create_ticket.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_create_ticket.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_create_ticket.Image = null;
      this.btn_create_ticket.IsSelected = false;
      this.btn_create_ticket.Location = new System.Drawing.Point(530, 56);
      this.btn_create_ticket.Name = "btn_create_ticket";
      this.btn_create_ticket.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_create_ticket.Size = new System.Drawing.Size(155, 60);
      this.btn_create_ticket.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_create_ticket.TabIndex = 2;
      this.btn_create_ticket.Text = "XCREATETICKET";
      this.btn_create_ticket.UseVisualStyleBackColor = false;
      this.btn_create_ticket.Click += new System.EventHandler(this.btn_create_ticket_Click);
      this.btn_create_ticket.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_amount_KeyPress);
      // 
      // btn_cancel
      // 
      this.btn_cancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_cancel.FlatAppearance.BorderSize = 0;
      this.btn_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_cancel.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_cancel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_cancel.Image = null;
      this.btn_cancel.IsSelected = false;
      this.btn_cancel.Location = new System.Drawing.Point(357, 56);
      this.btn_cancel.Name = "btn_cancel";
      this.btn_cancel.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_cancel.Size = new System.Drawing.Size(155, 60);
      this.btn_cancel.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_cancel.TabIndex = 1;
      this.btn_cancel.Text = "XCANCEL";
      this.btn_cancel.UseVisualStyleBackColor = false;
      this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
      // 
      // uc_ticket_promo
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.Color.Transparent;
      this.Controls.Add(this.pnl_bottom);
      this.Controls.Add(this.pnl_main);
      this.Controls.Add(this.pnl_title);
      this.Name = "uc_ticket_promo";
      this.Size = new System.Drawing.Size(871, 639);
      this.pnl_title.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.pb_promo_icon)).EndInit();
      this.pnl_main.ResumeLayout(false);
      this.pnl_common.ResumeLayout(false);
      this.pnl_common.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dgv_common)).EndInit();
      this.gb_digits_box.ResumeLayout(false);
      this.pnl_bottom.ResumeLayout(false);
      this.gb_filter_box.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private WSI.Cashier.Controls.uc_label lbl_separator_1;
    private WSI.Cashier.Controls.uc_round_button btn_create_ticket;
    private System.Windows.Forms.Panel pnl_common;
    private WSI.Cashier.Controls.uc_label lbl_promo_reward_value;
    private WSI.Cashier.Controls.uc_label lbl_promo_reward_text;
    private WSI.Cashier.Controls.uc_DataGridView dgv_common;
    private WSI.Cashier.Controls.uc_round_button btn_promos_change_view;
    private uc_promo_filter promo_filter;
    private WSI.Cashier.Controls.uc_round_panel gb_digits_box;
    private WSI.Cashier.Controls.uc_round_textbox txt_amount;
    private WSI.Cashier.Controls.uc_round_button btn_num_1;
    private WSI.Cashier.Controls.uc_round_button btn_num_2;
    private WSI.Cashier.Controls.uc_round_button btn_num_3;
    private WSI.Cashier.Controls.uc_round_button btn_num_4;
    private WSI.Cashier.Controls.uc_round_button btn_num_0;
    private WSI.Cashier.Controls.uc_round_button btn_num_5;
    private WSI.Cashier.Controls.uc_round_button btn_num_clear;
    private WSI.Cashier.Controls.uc_round_button btn_num_6;
    private WSI.Cashier.Controls.uc_round_button btn_num_9;
    private WSI.Cashier.Controls.uc_round_button btn_num_7;
    private WSI.Cashier.Controls.uc_round_button btn_num_8;
    private System.Windows.Forms.Panel pnl_title;
    private System.Windows.Forms.Panel pnl_main;
    private System.Windows.Forms.Panel pnl_bottom;
    private WSI.Cashier.Controls.uc_round_button btn_cancel;
    private System.Windows.Forms.Label label7;
    private System.Windows.Forms.PictureBox pb_promo_icon;
    private Controls.uc_round_panel gb_filter_box;
    private Controls.uc_label lbl_title;
    private Controls.uc_round_button btn_assign_to_account;
    private Controls.uc_round_button uc_round_button1;

  }
}
