namespace WSI.Cashier.TITO
{
  partial class uc_ticket_offline
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(uc_ticket_offline));
      this.lbl_separator_1 = new System.Windows.Forms.Label();
      this.lbl_ticket_curr_text = new WSI.Cashier.Controls.uc_label();
      this.lbl_ticket_curr = new WSI.Cashier.Controls.uc_label();
      this.gb_terminals = new WSI.Cashier.Controls.uc_round_panel();
      this.uc_terminal_filter = new WSI.Cashier.uc_terminal_filter();
      this.btn_gen = new WSI.Cashier.Controls.uc_round_button();
      this.lbl_ticked_readed = new WSI.Cashier.Controls.uc_label();
      this.lbl_ticket_num = new WSI.Cashier.Controls.uc_label();
      this.gb_date = new WSI.Cashier.Controls.uc_round_panel();
      this.label4 = new WSI.Cashier.Controls.uc_label();
      this.txt_second = new WSI.Cashier.NumericTextBox();
      this.txt_hour = new WSI.Cashier.NumericTextBox();
      this.label5 = new WSI.Cashier.Controls.uc_label();
      this.lbl_hour = new WSI.Cashier.Controls.uc_label();
      this.txt_minute = new WSI.Cashier.NumericTextBox();
      this.gp_digits_box = new WSI.Cashier.Controls.uc_round_panel();
      this.txt_amount = new WSI.Cashier.Controls.uc_round_textbox();
      this.btn_num_1 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_back = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_2 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_intro = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_3 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_4 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_10 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_5 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_dot = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_6 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_9 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_7 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_8 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_amount_add = new WSI.Cashier.Controls.uc_round_button();
      this.lbl_cards_title = new WSI.Cashier.Controls.uc_label();
      this.uc_ticket_reader = new WSI.Cashier.TITO.uc_ticket_reader();
      this.lbl_national_amount = new WSI.Cashier.Controls.uc_label();
      this.gb_terminals.SuspendLayout();
      this.gb_date.SuspendLayout();
      this.gp_digits_box.SuspendLayout();
      this.SuspendLayout();
      // 
      // lbl_separator_1
      // 
      this.lbl_separator_1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_separator_1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(95)))), ((int)(((byte)(95)))));
      this.lbl_separator_1.Location = new System.Drawing.Point(-3, 60);
      this.lbl_separator_1.Name = "lbl_separator_1";
      this.lbl_separator_1.Size = new System.Drawing.Size(885, 1);
      this.lbl_separator_1.TabIndex = 2;
      // 
      // lbl_ticket_curr_text
      // 
      this.lbl_ticket_curr_text.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_ticket_curr_text.AutoSize = true;
      this.lbl_ticket_curr_text.BackColor = System.Drawing.Color.Transparent;
      this.lbl_ticket_curr_text.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_ticket_curr_text.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_ticket_curr_text.Location = new System.Drawing.Point(590, 96);
      this.lbl_ticket_curr_text.Name = "lbl_ticket_curr_text";
      this.lbl_ticket_curr_text.Size = new System.Drawing.Size(153, 22);
      this.lbl_ticket_curr_text.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_ticket_curr_text.TabIndex = 11;
      this.lbl_ticket_curr_text.Text = "lbl_ticket_curr_text";
      this.lbl_ticket_curr_text.TextAlign = System.Drawing.ContentAlignment.TopRight;
      // 
      // lbl_ticket_curr
      // 
      this.lbl_ticket_curr.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_ticket_curr.BackColor = System.Drawing.Color.Transparent;
      this.lbl_ticket_curr.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_ticket_curr.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_ticket_curr.Location = new System.Drawing.Point(790, 96);
      this.lbl_ticket_curr.Name = "lbl_ticket_curr";
      this.lbl_ticket_curr.Size = new System.Drawing.Size(73, 22);
      this.lbl_ticket_curr.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_ticket_curr.TabIndex = 10;
      this.lbl_ticket_curr.Text = "LBL_ISO";
      this.lbl_ticket_curr.Visible = false;
      // 
      // gb_terminals
      // 
      this.gb_terminals.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.gb_terminals.BackColor = System.Drawing.Color.Transparent;
      this.gb_terminals.BorderColor = System.Drawing.Color.Empty;
      this.gb_terminals.Controls.Add(this.uc_terminal_filter);
      this.gb_terminals.CornerRadius = 10;
      this.gb_terminals.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_terminals.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gb_terminals.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_terminals.HeaderHeight = 35;
      this.gb_terminals.HeaderSubText = null;
      this.gb_terminals.HeaderText = "XTERMINALS";
      this.gb_terminals.Location = new System.Drawing.Point(14, 96);
      this.gb_terminals.Name = "gb_terminals";
      this.gb_terminals.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_terminals.Size = new System.Drawing.Size(549, 387);
      this.gb_terminals.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.gb_terminals.TabIndex = 5;
      this.gb_terminals.Text = "xDate";
      // 
      // uc_terminal_filter
      // 
      this.uc_terminal_filter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.uc_terminal_filter.BackColor = System.Drawing.SystemColors.Control;
      this.uc_terminal_filter.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.uc_terminal_filter.Location = new System.Drawing.Point(-1, 35);
      this.uc_terminal_filter.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
      this.uc_terminal_filter.Name = "uc_terminal_filter";
      this.uc_terminal_filter.Size = new System.Drawing.Size(551, 351);
      this.uc_terminal_filter.TabIndex = 0;
      this.uc_terminal_filter.TerminalChanged += new WSI.Cashier.uc_terminal_filter.TerminalOrProviderChangedEventHandler(this.uc_terminal_filter_TerminalChanged);
      // 
      // btn_gen
      // 
      this.btn_gen.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_gen.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_gen.FlatAppearance.BorderSize = 0;
      this.btn_gen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_gen.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_gen.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_gen.Image = null;
      this.btn_gen.IsSelected = false;
      this.btn_gen.Location = new System.Drawing.Point(584, 489);
      this.btn_gen.Name = "btn_gen";
      this.btn_gen.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_gen.Size = new System.Drawing.Size(135, 48);
      this.btn_gen.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_gen.TabIndex = 8;
      this.btn_gen.Text = "XCREAR TICKET JUGABLE";
      this.btn_gen.UseVisualStyleBackColor = false;
      this.btn_gen.Click += new System.EventHandler(this.btn_gen_Click);
      // 
      // lbl_ticked_readed
      // 
      this.lbl_ticked_readed.BackColor = System.Drawing.Color.Transparent;
      this.lbl_ticked_readed.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_ticked_readed.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_ticked_readed.Location = new System.Drawing.Point(18, 68);
      this.lbl_ticked_readed.Name = "lbl_ticked_readed";
      this.lbl_ticked_readed.Size = new System.Drawing.Size(81, 26);
      this.lbl_ticked_readed.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLUE;
      this.lbl_ticked_readed.TabIndex = 3;
      this.lbl_ticked_readed.Text = "xTicket:";
      this.lbl_ticked_readed.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_ticket_num
      // 
      this.lbl_ticket_num.AutoSize = true;
      this.lbl_ticket_num.BackColor = System.Drawing.Color.Transparent;
      this.lbl_ticket_num.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_ticket_num.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_ticket_num.Location = new System.Drawing.Point(134, 71);
      this.lbl_ticket_num.Name = "lbl_ticket_num";
      this.lbl_ticket_num.Size = new System.Drawing.Size(102, 22);
      this.lbl_ticket_num.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_ticket_num.TabIndex = 4;
      this.lbl_ticket_num.Text = "xTicketNum";
      this.lbl_ticket_num.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // gb_date
      // 
      this.gb_date.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.gb_date.BackColor = System.Drawing.Color.Transparent;
      this.gb_date.BorderColor = System.Drawing.Color.Empty;
      this.gb_date.Controls.Add(this.label4);
      this.gb_date.Controls.Add(this.txt_second);
      this.gb_date.Controls.Add(this.txt_hour);
      this.gb_date.Controls.Add(this.label5);
      this.gb_date.Controls.Add(this.lbl_hour);
      this.gb_date.Controls.Add(this.txt_minute);
      this.gb_date.CornerRadius = 10;
      this.gb_date.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_date.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gb_date.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_date.HeaderHeight = 35;
      this.gb_date.HeaderSubText = null;
      this.gb_date.HeaderText = "XDATE";
      this.gb_date.Location = new System.Drawing.Point(14, 489);
      this.gb_date.Name = "gb_date";
      this.gb_date.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_date.Size = new System.Drawing.Size(549, 98);
      this.gb_date.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.gb_date.TabIndex = 6;
      this.gb_date.Text = "xDate";
      // 
      // label4
      // 
      this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.label4.BackColor = System.Drawing.Color.Transparent;
      this.label4.Font = new System.Drawing.Font("Open Sans Semibold", 32F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.label4.Location = new System.Drawing.Point(338, 44);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(21, 39);
      this.label4.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_32PX_BLACK;
      this.label4.TabIndex = 4;
      this.label4.Text = ":";
      this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // txt_second
      // 
      this.txt_second.AllowSpace = false;
      this.txt_second.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.txt_second.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_second.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_second.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_second.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_second.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_second.CornerRadius = 5;
      this.txt_second.FillWithCeros = true;
      this.txt_second.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_second.Location = new System.Drawing.Point(358, 48);
      this.txt_second.MaxLength = 2;
      this.txt_second.Multiline = false;
      this.txt_second.Name = "txt_second";
      this.txt_second.PasswordChar = '\0';
      this.txt_second.ReadOnly = false;
      this.txt_second.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_second.SelectedText = "";
      this.txt_second.SelectionLength = 0;
      this.txt_second.SelectionStart = 0;
      this.txt_second.Size = new System.Drawing.Size(49, 40);
      this.txt_second.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_second.TabIndex = 5;
      this.txt_second.TabStop = false;
      this.txt_second.Text = "19";
      this.txt_second.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.txt_second.UseSystemPasswordChar = false;
      this.txt_second.WaterMark = null;
      this.txt_second.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // txt_hour
      // 
      this.txt_hour.AllowSpace = false;
      this.txt_hour.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.txt_hour.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_hour.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_hour.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_hour.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_hour.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_hour.CornerRadius = 5;
      this.txt_hour.FillWithCeros = true;
      this.txt_hour.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_hour.Location = new System.Drawing.Point(218, 48);
      this.txt_hour.MaxLength = 2;
      this.txt_hour.Multiline = false;
      this.txt_hour.Name = "txt_hour";
      this.txt_hour.PasswordChar = '\0';
      this.txt_hour.ReadOnly = false;
      this.txt_hour.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_hour.SelectedText = "";
      this.txt_hour.SelectionLength = 0;
      this.txt_hour.SelectionStart = 0;
      this.txt_hour.Size = new System.Drawing.Size(49, 40);
      this.txt_hour.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_hour.TabIndex = 1;
      this.txt_hour.TabStop = false;
      this.txt_hour.Text = "06";
      this.txt_hour.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.txt_hour.UseSystemPasswordChar = false;
      this.txt_hour.WaterMark = null;
      this.txt_hour.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // label5
      // 
      this.label5.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.label5.BackColor = System.Drawing.Color.Transparent;
      this.label5.Font = new System.Drawing.Font("Open Sans Semibold", 32F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.label5.Location = new System.Drawing.Point(268, 44);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(21, 38);
      this.label5.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_32PX_BLACK;
      this.label5.TabIndex = 2;
      this.label5.Text = ":";
      this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lbl_hour
      // 
      this.lbl_hour.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.lbl_hour.BackColor = System.Drawing.Color.Transparent;
      this.lbl_hour.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_hour.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_hour.Location = new System.Drawing.Point(132, 55);
      this.lbl_hour.Name = "lbl_hour";
      this.lbl_hour.Size = new System.Drawing.Size(85, 26);
      this.lbl_hour.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLUE;
      this.lbl_hour.TabIndex = 0;
      this.lbl_hour.Text = "xHour";
      this.lbl_hour.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // txt_minute
      // 
      this.txt_minute.AllowSpace = false;
      this.txt_minute.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.txt_minute.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_minute.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_minute.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_minute.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_minute.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_minute.CornerRadius = 5;
      this.txt_minute.FillWithCeros = true;
      this.txt_minute.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_minute.Location = new System.Drawing.Point(288, 48);
      this.txt_minute.MaxLength = 2;
      this.txt_minute.Multiline = false;
      this.txt_minute.Name = "txt_minute";
      this.txt_minute.PasswordChar = '\0';
      this.txt_minute.ReadOnly = false;
      this.txt_minute.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_minute.SelectedText = "";
      this.txt_minute.SelectionLength = 0;
      this.txt_minute.SelectionStart = 0;
      this.txt_minute.Size = new System.Drawing.Size(49, 40);
      this.txt_minute.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_minute.TabIndex = 3;
      this.txt_minute.TabStop = false;
      this.txt_minute.Text = "11";
      this.txt_minute.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.txt_minute.UseSystemPasswordChar = false;
      this.txt_minute.WaterMark = null;
      this.txt_minute.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // gp_digits_box
      // 
      this.gp_digits_box.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.gp_digits_box.BackColor = System.Drawing.Color.Transparent;
      this.gp_digits_box.BorderColor = System.Drawing.Color.Empty;
      this.gp_digits_box.Controls.Add(this.txt_amount);
      this.gp_digits_box.Controls.Add(this.btn_num_1);
      this.gp_digits_box.Controls.Add(this.btn_back);
      this.gp_digits_box.Controls.Add(this.btn_num_2);
      this.gp_digits_box.Controls.Add(this.btn_intro);
      this.gp_digits_box.Controls.Add(this.btn_num_3);
      this.gp_digits_box.Controls.Add(this.btn_num_4);
      this.gp_digits_box.Controls.Add(this.btn_num_10);
      this.gp_digits_box.Controls.Add(this.btn_num_5);
      this.gp_digits_box.Controls.Add(this.btn_num_dot);
      this.gp_digits_box.Controls.Add(this.btn_num_6);
      this.gp_digits_box.Controls.Add(this.btn_num_9);
      this.gp_digits_box.Controls.Add(this.btn_num_7);
      this.gp_digits_box.Controls.Add(this.btn_num_8);
      this.gp_digits_box.CornerRadius = 10;
      this.gp_digits_box.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.gp_digits_box.ForeColor = System.Drawing.Color.Black;
      this.gp_digits_box.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.gp_digits_box.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gp_digits_box.HeaderHeight = 55;
      this.gp_digits_box.HeaderSubText = null;
      this.gp_digits_box.HeaderText = null;
      this.gp_digits_box.Location = new System.Drawing.Point(584, 144);
      this.gp_digits_box.Name = "gp_digits_box";
      this.gp_digits_box.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gp_digits_box.Size = new System.Drawing.Size(279, 291);
      this.gp_digits_box.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NONE;
      this.gp_digits_box.TabIndex = 7;
      this.gp_digits_box.Text = "xEnter Amount";
      // 
      // txt_amount
      // 
      this.txt_amount.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_amount.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_amount.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.txt_amount.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.txt_amount.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_amount.CornerRadius = 0;
      this.txt_amount.Font = new System.Drawing.Font("Open Sans Semibold", 40F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_amount.Location = new System.Drawing.Point(10, 4);
      this.txt_amount.MaxLength = 18;
      this.txt_amount.Multiline = false;
      this.txt_amount.Name = "txt_amount";
      this.txt_amount.PasswordChar = '\0';
      this.txt_amount.ReadOnly = true;
      this.txt_amount.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_amount.SelectedText = "";
      this.txt_amount.SelectionLength = 0;
      this.txt_amount.SelectionStart = 0;
      this.txt_amount.Size = new System.Drawing.Size(260, 50);
      this.txt_amount.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.CALCULATOR;
      this.txt_amount.TabIndex = 14;
      this.txt_amount.TabStop = false;
      this.txt_amount.Text = "123";
      this.txt_amount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      this.txt_amount.UseSystemPasswordChar = false;
      this.txt_amount.WaterMark = null;
      this.txt_amount.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(153)))), ((int)(((byte)(49)))));
      this.txt_amount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.uc_terminal_offline_KeyPress);
      // 
      // btn_num_1
      // 
      this.btn_num_1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_1.CornerRadius = 0;
      this.btn_num_1.FlatAppearance.BorderSize = 0;
      this.btn_num_1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_1.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_1.Image = null;
      this.btn_num_1.IsSelected = false;
      this.btn_num_1.Location = new System.Drawing.Point(6, 73);
      this.btn_num_1.Name = "btn_num_1";
      this.btn_num_1.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_1.SelectedColor = System.Drawing.Color.Empty;
      this.btn_num_1.Size = new System.Drawing.Size(48, 48);
      this.btn_num_1.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_1.TabIndex = 0;
      this.btn_num_1.TabStop = false;
      this.btn_num_1.Text = "1";
      this.btn_num_1.UseVisualStyleBackColor = false;
      this.btn_num_1.Click += new System.EventHandler(this.btn_num_1_Click);
      this.btn_num_1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.uc_terminal_offline_KeyPress);
      // 
      // btn_back
      // 
      this.btn_back.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_back.CornerRadius = 0;
      this.btn_back.FlatAppearance.BorderSize = 0;
      this.btn_back.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_back.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_back.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_back.Image = ((System.Drawing.Image)(resources.GetObject("btn_back.Image")));
      this.btn_back.IsSelected = false;
      this.btn_back.Location = new System.Drawing.Point(168, 73);
      this.btn_back.Name = "btn_back";
      this.btn_back.SelectedColor = System.Drawing.Color.Empty;
      this.btn_back.Size = new System.Drawing.Size(102, 48);
      this.btn_back.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_BACKSPACE;
      this.btn_back.TabIndex = 13;
      this.btn_back.TabStop = false;
      this.btn_back.UseVisualStyleBackColor = false;
      this.btn_back.Click += new System.EventHandler(this.btn_back_Click);
      this.btn_back.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.uc_terminal_offline_KeyPress);
      // 
      // btn_num_2
      // 
      this.btn_num_2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_2.CornerRadius = 0;
      this.btn_num_2.FlatAppearance.BorderSize = 0;
      this.btn_num_2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_2.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_2.Image = null;
      this.btn_num_2.IsSelected = false;
      this.btn_num_2.Location = new System.Drawing.Point(60, 73);
      this.btn_num_2.Name = "btn_num_2";
      this.btn_num_2.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_2.SelectedColor = System.Drawing.Color.Empty;
      this.btn_num_2.Size = new System.Drawing.Size(48, 48);
      this.btn_num_2.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_2.TabIndex = 1;
      this.btn_num_2.TabStop = false;
      this.btn_num_2.Text = "2";
      this.btn_num_2.UseVisualStyleBackColor = false;
      this.btn_num_2.Click += new System.EventHandler(this.btn_num_2_Click);
      this.btn_num_2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.uc_terminal_offline_KeyPress);
      // 
      // btn_intro
      // 
      this.btn_intro.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(153)))), ((int)(((byte)(49)))));
      this.btn_intro.CornerRadius = 0;
      this.btn_intro.FlatAppearance.BorderSize = 0;
      this.btn_intro.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_intro.Font = new System.Drawing.Font("Open Sans Semibold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_intro.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_intro.Image = ((System.Drawing.Image)(resources.GetObject("btn_intro.Image")));
      this.btn_intro.IsSelected = false;
      this.btn_intro.Location = new System.Drawing.Point(168, 181);
      this.btn_intro.Name = "btn_intro";
      this.btn_intro.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_intro.SelectedColor = System.Drawing.Color.Empty;
      this.btn_intro.Size = new System.Drawing.Size(102, 102);
      this.btn_intro.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_ENTER;
      this.btn_intro.TabIndex = 12;
      this.btn_intro.UseVisualStyleBackColor = false;
      this.btn_intro.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.uc_terminal_offline_KeyPress);
      // 
      // btn_num_3
      // 
      this.btn_num_3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_3.CornerRadius = 0;
      this.btn_num_3.FlatAppearance.BorderSize = 0;
      this.btn_num_3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_3.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_3.Image = null;
      this.btn_num_3.IsSelected = false;
      this.btn_num_3.Location = new System.Drawing.Point(114, 73);
      this.btn_num_3.Name = "btn_num_3";
      this.btn_num_3.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_3.SelectedColor = System.Drawing.Color.Empty;
      this.btn_num_3.Size = new System.Drawing.Size(48, 48);
      this.btn_num_3.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_3.TabIndex = 2;
      this.btn_num_3.TabStop = false;
      this.btn_num_3.Text = "3";
      this.btn_num_3.UseVisualStyleBackColor = false;
      this.btn_num_3.Click += new System.EventHandler(this.btn_num_3_Click);
      this.btn_num_3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.uc_terminal_offline_KeyPress);
      // 
      // btn_num_4
      // 
      this.btn_num_4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_4.CornerRadius = 0;
      this.btn_num_4.FlatAppearance.BorderSize = 0;
      this.btn_num_4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_4.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_4.Image = null;
      this.btn_num_4.IsSelected = false;
      this.btn_num_4.Location = new System.Drawing.Point(6, 127);
      this.btn_num_4.Name = "btn_num_4";
      this.btn_num_4.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_4.SelectedColor = System.Drawing.Color.Empty;
      this.btn_num_4.Size = new System.Drawing.Size(48, 48);
      this.btn_num_4.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_4.TabIndex = 3;
      this.btn_num_4.TabStop = false;
      this.btn_num_4.Text = "4";
      this.btn_num_4.UseVisualStyleBackColor = false;
      this.btn_num_4.Click += new System.EventHandler(this.btn_num_4_Click);
      this.btn_num_4.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.uc_terminal_offline_KeyPress);
      // 
      // btn_num_10
      // 
      this.btn_num_10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_10.CornerRadius = 0;
      this.btn_num_10.FlatAppearance.BorderSize = 0;
      this.btn_num_10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_10.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_10.Image = null;
      this.btn_num_10.IsSelected = false;
      this.btn_num_10.Location = new System.Drawing.Point(6, 235);
      this.btn_num_10.Name = "btn_num_10";
      this.btn_num_10.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_10.SelectedColor = System.Drawing.Color.Empty;
      this.btn_num_10.Size = new System.Drawing.Size(102, 48);
      this.btn_num_10.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_10.TabIndex = 10;
      this.btn_num_10.TabStop = false;
      this.btn_num_10.Text = "0";
      this.btn_num_10.UseVisualStyleBackColor = false;
      this.btn_num_10.Click += new System.EventHandler(this.btn_num_10_Click);
      this.btn_num_10.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.uc_terminal_offline_KeyPress);
      // 
      // btn_num_5
      // 
      this.btn_num_5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_5.CornerRadius = 0;
      this.btn_num_5.FlatAppearance.BorderSize = 0;
      this.btn_num_5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_5.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_5.Image = null;
      this.btn_num_5.IsSelected = false;
      this.btn_num_5.Location = new System.Drawing.Point(60, 127);
      this.btn_num_5.Name = "btn_num_5";
      this.btn_num_5.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_5.SelectedColor = System.Drawing.Color.Empty;
      this.btn_num_5.Size = new System.Drawing.Size(48, 48);
      this.btn_num_5.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_5.TabIndex = 4;
      this.btn_num_5.TabStop = false;
      this.btn_num_5.Text = "5";
      this.btn_num_5.UseVisualStyleBackColor = false;
      this.btn_num_5.Click += new System.EventHandler(this.btn_num_5_Click);
      this.btn_num_5.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.uc_terminal_offline_KeyPress);
      // 
      // btn_num_dot
      // 
      this.btn_num_dot.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_dot.CornerRadius = 0;
      this.btn_num_dot.FlatAppearance.BorderSize = 0;
      this.btn_num_dot.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_dot.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_dot.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_dot.Image = null;
      this.btn_num_dot.IsSelected = false;
      this.btn_num_dot.Location = new System.Drawing.Point(114, 235);
      this.btn_num_dot.Name = "btn_num_dot";
      this.btn_num_dot.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_dot.SelectedColor = System.Drawing.Color.Empty;
      this.btn_num_dot.Size = new System.Drawing.Size(48, 48);
      this.btn_num_dot.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_dot.TabIndex = 9;
      this.btn_num_dot.TabStop = false;
      this.btn_num_dot.Text = "�";
      this.btn_num_dot.UseVisualStyleBackColor = false;
      this.btn_num_dot.Click += new System.EventHandler(this.btn_num_dot_Click);
      this.btn_num_dot.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.uc_terminal_offline_KeyPress);
      // 
      // btn_num_6
      // 
      this.btn_num_6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_6.CornerRadius = 0;
      this.btn_num_6.FlatAppearance.BorderSize = 0;
      this.btn_num_6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_6.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_6.Image = null;
      this.btn_num_6.IsSelected = false;
      this.btn_num_6.Location = new System.Drawing.Point(114, 127);
      this.btn_num_6.Name = "btn_num_6";
      this.btn_num_6.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_6.SelectedColor = System.Drawing.Color.Empty;
      this.btn_num_6.Size = new System.Drawing.Size(48, 48);
      this.btn_num_6.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_6.TabIndex = 5;
      this.btn_num_6.TabStop = false;
      this.btn_num_6.Text = "6";
      this.btn_num_6.UseVisualStyleBackColor = false;
      this.btn_num_6.Click += new System.EventHandler(this.btn_num_6_Click);
      this.btn_num_6.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.uc_terminal_offline_KeyPress);
      // 
      // btn_num_9
      // 
      this.btn_num_9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_9.CornerRadius = 0;
      this.btn_num_9.FlatAppearance.BorderSize = 0;
      this.btn_num_9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_9.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_9.Image = null;
      this.btn_num_9.IsSelected = false;
      this.btn_num_9.Location = new System.Drawing.Point(114, 181);
      this.btn_num_9.Name = "btn_num_9";
      this.btn_num_9.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_9.SelectedColor = System.Drawing.Color.Empty;
      this.btn_num_9.Size = new System.Drawing.Size(48, 48);
      this.btn_num_9.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_9.TabIndex = 8;
      this.btn_num_9.TabStop = false;
      this.btn_num_9.Text = "9";
      this.btn_num_9.UseVisualStyleBackColor = false;
      this.btn_num_9.Click += new System.EventHandler(this.btn_num_9_Click);
      this.btn_num_9.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.uc_terminal_offline_KeyPress);
      // 
      // btn_num_7
      // 
      this.btn_num_7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_7.CornerRadius = 0;
      this.btn_num_7.FlatAppearance.BorderSize = 0;
      this.btn_num_7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_7.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_7.Image = null;
      this.btn_num_7.IsSelected = false;
      this.btn_num_7.Location = new System.Drawing.Point(6, 181);
      this.btn_num_7.Name = "btn_num_7";
      this.btn_num_7.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_7.SelectedColor = System.Drawing.Color.Empty;
      this.btn_num_7.Size = new System.Drawing.Size(48, 48);
      this.btn_num_7.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_7.TabIndex = 6;
      this.btn_num_7.TabStop = false;
      this.btn_num_7.Text = "7";
      this.btn_num_7.UseVisualStyleBackColor = false;
      this.btn_num_7.Click += new System.EventHandler(this.btn_num_7_Click);
      this.btn_num_7.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.uc_terminal_offline_KeyPress);
      // 
      // btn_num_8
      // 
      this.btn_num_8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_8.CornerRadius = 0;
      this.btn_num_8.FlatAppearance.BorderSize = 0;
      this.btn_num_8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_8.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_8.Image = null;
      this.btn_num_8.IsSelected = false;
      this.btn_num_8.Location = new System.Drawing.Point(60, 181);
      this.btn_num_8.Name = "btn_num_8";
      this.btn_num_8.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_8.SelectedColor = System.Drawing.Color.Empty;
      this.btn_num_8.Size = new System.Drawing.Size(48, 48);
      this.btn_num_8.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_8.TabIndex = 7;
      this.btn_num_8.TabStop = false;
      this.btn_num_8.Text = "8";
      this.btn_num_8.UseVisualStyleBackColor = false;
      this.btn_num_8.Click += new System.EventHandler(this.btn_num_8_Click);
      this.btn_num_8.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.uc_terminal_offline_KeyPress);
      // 
      // btn_amount_add
      // 
      this.btn_amount_add.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_amount_add.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_amount_add.FlatAppearance.BorderSize = 0;
      this.btn_amount_add.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_amount_add.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_amount_add.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_amount_add.Image = null;
      this.btn_amount_add.IsSelected = false;
      this.btn_amount_add.Location = new System.Drawing.Point(728, 489);
      this.btn_amount_add.Name = "btn_amount_add";
      this.btn_amount_add.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_amount_add.Size = new System.Drawing.Size(135, 48);
      this.btn_amount_add.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_amount_add.TabIndex = 9;
      this.btn_amount_add.Text = "XPAGAR TICKET REDIMIBLE";
      this.btn_amount_add.UseVisualStyleBackColor = false;
      this.btn_amount_add.Click += new System.EventHandler(this.btn_credit_add_Click);
      this.btn_amount_add.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.uc_terminal_offline_KeyPress);
      // 
      // lbl_cards_title
      // 
      this.lbl_cards_title.BackColor = System.Drawing.Color.Transparent;
      this.lbl_cards_title.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_cards_title.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_cards_title.Image = global::WSI.Cashier.Properties.Resources.ticketsTito_header;
      this.lbl_cards_title.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.lbl_cards_title.Location = new System.Drawing.Point(11, 20);
      this.lbl_cards_title.Name = "lbl_cards_title";
      this.lbl_cards_title.Size = new System.Drawing.Size(138, 33);
      this.lbl_cards_title.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_cards_title.TabIndex = 13;
      this.lbl_cards_title.Text = "xTickets Offline";
      // 
      // uc_ticket_reader
      // 
      this.uc_ticket_reader.Location = new System.Drawing.Point(185, 0);
      this.uc_ticket_reader.MaximumSize = new System.Drawing.Size(800, 100);
      this.uc_ticket_reader.MinimumSize = new System.Drawing.Size(800, 60);
      this.uc_ticket_reader.Name = "uc_ticket_reader";
      this.uc_ticket_reader.Size = new System.Drawing.Size(800, 60);
      this.uc_ticket_reader.TabIndex = 1;
      // 
      // lbl_national_amount
      // 
      this.lbl_national_amount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_national_amount.BackColor = System.Drawing.Color.Transparent;
      this.lbl_national_amount.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_national_amount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_national_amount.Location = new System.Drawing.Point(586, 449);
      this.lbl_national_amount.Name = "lbl_national_amount";
      this.lbl_national_amount.Size = new System.Drawing.Size(277, 23);
      this.lbl_national_amount.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_national_amount.TabIndex = 12;
      this.lbl_national_amount.Text = "lbl_national_amount";
      this.lbl_national_amount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // uc_ticket_offline
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.SystemColors.Control;
      this.Controls.Add(this.lbl_national_amount);
      this.Controls.Add(this.lbl_ticket_curr_text);
      this.Controls.Add(this.lbl_ticket_curr);
      this.Controls.Add(this.gb_terminals);
      this.Controls.Add(this.btn_gen);
      this.Controls.Add(this.lbl_ticked_readed);
      this.Controls.Add(this.lbl_ticket_num);
      this.Controls.Add(this.gb_date);
      this.Controls.Add(this.gp_digits_box);
      this.Controls.Add(this.btn_amount_add);
      this.Controls.Add(this.lbl_cards_title);
      this.Controls.Add(this.lbl_separator_1);
      this.Controls.Add(this.uc_ticket_reader);
      this.Name = "uc_ticket_offline";
      this.Size = new System.Drawing.Size(884, 604);
      this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.uc_terminal_offline_KeyPress);
      this.gb_terminals.ResumeLayout(false);
      this.gb_date.ResumeLayout(false);
      this.gp_digits_box.ResumeLayout(false);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private uc_terminal_filter uc_terminal_filter;
    private uc_ticket_reader uc_ticket_reader;
    private System.Windows.Forms.Label lbl_separator_1;
    private WSI.Cashier.Controls.uc_label lbl_cards_title;
    private WSI.Cashier.Controls.uc_round_button btn_amount_add;
    private WSI.Cashier.Controls.uc_round_panel gp_digits_box;
    private WSI.Cashier.Controls.uc_round_textbox txt_amount;
    private WSI.Cashier.Controls.uc_round_button btn_num_1;
    private WSI.Cashier.Controls.uc_round_button btn_back;
    private WSI.Cashier.Controls.uc_round_button btn_num_2;
    private WSI.Cashier.Controls.uc_round_button btn_intro;
    private WSI.Cashier.Controls.uc_round_button btn_num_3;
    private WSI.Cashier.Controls.uc_round_button btn_num_4;
    private WSI.Cashier.Controls.uc_round_button btn_num_5;
    private WSI.Cashier.Controls.uc_round_button btn_num_dot;
    private WSI.Cashier.Controls.uc_round_button btn_num_6;
    private WSI.Cashier.Controls.uc_round_button btn_num_9;
    private WSI.Cashier.Controls.uc_round_button btn_num_7;
    private WSI.Cashier.Controls.uc_round_button btn_num_8;
    private WSI.Cashier.Controls.uc_round_panel gb_date;
    private WSI.Cashier.Controls.uc_label label4;
    private NumericTextBox txt_second;
    private WSI.Cashier.Controls.uc_label label5;
    private NumericTextBox txt_minute;
    private WSI.Cashier.Controls.uc_label lbl_ticket_num;
    private WSI.Cashier.Controls.uc_label lbl_ticked_readed;
    private WSI.Cashier.Controls.uc_round_button btn_gen;
    private NumericTextBox txt_hour;
    private WSI.Cashier.Controls.uc_label lbl_hour;
    private Controls.uc_round_button btn_num_10;
    private Controls.uc_round_panel gb_terminals;
    private Controls.uc_label lbl_ticket_curr;
    private Controls.uc_label lbl_ticket_curr_text;
    private Controls.uc_label lbl_national_amount;
  }
}
