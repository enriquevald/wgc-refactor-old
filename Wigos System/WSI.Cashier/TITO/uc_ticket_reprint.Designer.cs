namespace WSI.Cashier.TITO
{
  partial class uc_ticket_reprint
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.lbl_ticket_modified_terminal_type = new System.Windows.Forms.Label();
      this.lbl_ticket_modified_terminal_type_value = new System.Windows.Forms.Label();
      this.lbl_ticket_modified_terminal_value = new System.Windows.Forms.Label();
      this.lbl_ticket_modified_terminal = new System.Windows.Forms.Label();
      this.lbl_ticket_modified_value = new System.Windows.Forms.Label();
      this.lbl_ticket_modified = new System.Windows.Forms.Label();
      this.lbl_ticket_created_terminal_type_value = new System.Windows.Forms.Label();
      this.lbl_ticket_created_terminal_type = new System.Windows.Forms.Label();
      this.lbl_ticket_terminal_creation_value = new System.Windows.Forms.Label();
      this.lbl_ticket_terminal_creation = new System.Windows.Forms.Label();
      this.lbl_ticket_created_value = new System.Windows.Forms.Label();
      this.lbl_ticket_created = new System.Windows.Forms.Label();
      this.lbl_separator_1 = new System.Windows.Forms.Label();
      this.lbl_ticket_status_value = new System.Windows.Forms.Label();
      this.btn_print_ticket = new System.Windows.Forms.Button();
      this.lbl_ticket_status = new System.Windows.Forms.Label();
      this.lbl_ticket_amount_value = new System.Windows.Forms.Label();
      this.lbl_ticket_amount = new System.Windows.Forms.Label();
      this.lbl_validation_number = new System.Windows.Forms.Label();
      this.lbl_ticket_number2 = new System.Windows.Forms.Label();
      this.timer1 = new System.Windows.Forms.Timer(this.components);
      this.btn_ticket_redeem_total = new System.Windows.Forms.Button();
      this.gb_ticket_props = new System.Windows.Forms.GroupBox();
      this.label1 = new System.Windows.Forms.Label();
      this.uc_ticket_reader1 = new WSI.Cashier.TITO.uc_ticket_reader();
      this.label2 = new System.Windows.Forms.Label();
      this.lbl_account_id_value = new System.Windows.Forms.Label();
      this.lbl_block_reason = new System.Windows.Forms.Label();
      this.lbl_blocked = new System.Windows.Forms.Label();
      this.lbl_trackdata = new System.Windows.Forms.Label();
      this.lbl_name = new System.Windows.Forms.Label();
      this.pb_blocked = new System.Windows.Forms.PictureBox();
      this.lbl_happy_birthday = new System.Windows.Forms.Label();
      this.lbl_id = new System.Windows.Forms.Label();
      this.lbl_id_title = new System.Windows.Forms.Label();
      this.lbl_track_number = new System.Windows.Forms.Label();
      this.lbl_birth_title = new System.Windows.Forms.Label();
      this.lbl_account_id_name = new System.Windows.Forms.Label();
      this.lbl_birth = new System.Windows.Forms.Label();
      this.lbl_holder_name = new System.Windows.Forms.Label();
      this.gb_ticket_props.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pb_blocked)).BeginInit();
      this.SuspendLayout();
      // 
      // lbl_ticket_modified_terminal_type
      // 
      this.lbl_ticket_modified_terminal_type.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.lbl_ticket_modified_terminal_type.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_ticket_modified_terminal_type.Location = new System.Drawing.Point(5, 240);
      this.lbl_ticket_modified_terminal_type.Name = "lbl_ticket_modified_terminal_type";
      this.lbl_ticket_modified_terminal_type.Size = new System.Drawing.Size(142, 23);
      this.lbl_ticket_modified_terminal_type.TabIndex = 125;
      this.lbl_ticket_modified_terminal_type.Text = "xMod Term type:";
      this.lbl_ticket_modified_terminal_type.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_ticket_modified_terminal_type_value
      // 
      this.lbl_ticket_modified_terminal_type_value.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.lbl_ticket_modified_terminal_type_value.AutoSize = true;
      this.lbl_ticket_modified_terminal_type_value.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_ticket_modified_terminal_type_value.Location = new System.Drawing.Point(149, 247);
      this.lbl_ticket_modified_terminal_type_value.Name = "lbl_ticket_modified_terminal_type_value";
      this.lbl_ticket_modified_terminal_type_value.Size = new System.Drawing.Size(15, 16);
      this.lbl_ticket_modified_terminal_type_value.TabIndex = 126;
      this.lbl_ticket_modified_terminal_type_value.Text = "0";
      this.lbl_ticket_modified_terminal_type_value.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_ticket_modified_terminal_value
      // 
      this.lbl_ticket_modified_terminal_value.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.lbl_ticket_modified_terminal_value.AutoSize = true;
      this.lbl_ticket_modified_terminal_value.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_ticket_modified_terminal_value.Location = new System.Drawing.Point(149, 224);
      this.lbl_ticket_modified_terminal_value.Name = "lbl_ticket_modified_terminal_value";
      this.lbl_ticket_modified_terminal_value.Size = new System.Drawing.Size(15, 16);
      this.lbl_ticket_modified_terminal_value.TabIndex = 124;
      this.lbl_ticket_modified_terminal_value.Text = "0";
      this.lbl_ticket_modified_terminal_value.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_ticket_modified_terminal
      // 
      this.lbl_ticket_modified_terminal.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.lbl_ticket_modified_terminal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_ticket_modified_terminal.Location = new System.Drawing.Point(15, 217);
      this.lbl_ticket_modified_terminal.Name = "lbl_ticket_modified_terminal";
      this.lbl_ticket_modified_terminal.Size = new System.Drawing.Size(132, 23);
      this.lbl_ticket_modified_terminal.TabIndex = 123;
      this.lbl_ticket_modified_terminal.Text = "xModified Term:";
      this.lbl_ticket_modified_terminal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_ticket_modified_value
      // 
      this.lbl_ticket_modified_value.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.lbl_ticket_modified_value.AutoSize = true;
      this.lbl_ticket_modified_value.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_ticket_modified_value.Location = new System.Drawing.Point(149, 201);
      this.lbl_ticket_modified_value.Name = "lbl_ticket_modified_value";
      this.lbl_ticket_modified_value.Size = new System.Drawing.Size(72, 16);
      this.lbl_ticket_modified_value.TabIndex = 122;
      this.lbl_ticket_modified_value.Text = "00/00/0000";
      this.lbl_ticket_modified_value.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_ticket_modified
      // 
      this.lbl_ticket_modified.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.lbl_ticket_modified.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_ticket_modified.Location = new System.Drawing.Point(8, 194);
      this.lbl_ticket_modified.Name = "lbl_ticket_modified";
      this.lbl_ticket_modified.Size = new System.Drawing.Size(139, 23);
      this.lbl_ticket_modified.TabIndex = 121;
      this.lbl_ticket_modified.Text = "Fecha modificación:";
      this.lbl_ticket_modified.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_ticket_created_terminal_type_value
      // 
      this.lbl_ticket_created_terminal_type_value.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.lbl_ticket_created_terminal_type_value.AutoSize = true;
      this.lbl_ticket_created_terminal_type_value.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_ticket_created_terminal_type_value.Location = new System.Drawing.Point(149, 169);
      this.lbl_ticket_created_terminal_type_value.Name = "lbl_ticket_created_terminal_type_value";
      this.lbl_ticket_created_terminal_type_value.Size = new System.Drawing.Size(15, 16);
      this.lbl_ticket_created_terminal_type_value.TabIndex = 120;
      this.lbl_ticket_created_terminal_type_value.Text = "0";
      this.lbl_ticket_created_terminal_type_value.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_ticket_created_terminal_type
      // 
      this.lbl_ticket_created_terminal_type.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_ticket_created_terminal_type.Location = new System.Drawing.Point(34, 166);
      this.lbl_ticket_created_terminal_type.Name = "lbl_ticket_created_terminal_type";
      this.lbl_ticket_created_terminal_type.Size = new System.Drawing.Size(114, 23);
      this.lbl_ticket_created_terminal_type.TabIndex = 119;
      this.lbl_ticket_created_terminal_type.Text = "xTerminal type:";
      this.lbl_ticket_created_terminal_type.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_ticket_terminal_creation_value
      // 
      this.lbl_ticket_terminal_creation_value.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.lbl_ticket_terminal_creation_value.AutoSize = true;
      this.lbl_ticket_terminal_creation_value.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_ticket_terminal_creation_value.Location = new System.Drawing.Point(149, 146);
      this.lbl_ticket_terminal_creation_value.Name = "lbl_ticket_terminal_creation_value";
      this.lbl_ticket_terminal_creation_value.Size = new System.Drawing.Size(15, 16);
      this.lbl_ticket_terminal_creation_value.TabIndex = 118;
      this.lbl_ticket_terminal_creation_value.Text = "0";
      this.lbl_ticket_terminal_creation_value.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_ticket_terminal_creation
      // 
      this.lbl_ticket_terminal_creation.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_ticket_terminal_creation.Location = new System.Drawing.Point(31, 143);
      this.lbl_ticket_terminal_creation.Name = "lbl_ticket_terminal_creation";
      this.lbl_ticket_terminal_creation.Size = new System.Drawing.Size(117, 23);
      this.lbl_ticket_terminal_creation.TabIndex = 117;
      this.lbl_ticket_terminal_creation.Text = "xCreated Term:";
      this.lbl_ticket_terminal_creation.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_ticket_created_value
      // 
      this.lbl_ticket_created_value.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.lbl_ticket_created_value.AutoSize = true;
      this.lbl_ticket_created_value.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_ticket_created_value.Location = new System.Drawing.Point(149, 123);
      this.lbl_ticket_created_value.Name = "lbl_ticket_created_value";
      this.lbl_ticket_created_value.Size = new System.Drawing.Size(72, 16);
      this.lbl_ticket_created_value.TabIndex = 116;
      this.lbl_ticket_created_value.Text = "00/00/0000";
      this.lbl_ticket_created_value.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_ticket_created
      // 
      this.lbl_ticket_created.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_ticket_created.Location = new System.Drawing.Point(34, 120);
      this.lbl_ticket_created.Name = "lbl_ticket_created";
      this.lbl_ticket_created.Size = new System.Drawing.Size(114, 23);
      this.lbl_ticket_created.TabIndex = 115;
      this.lbl_ticket_created.Text = "xCreated:";
      this.lbl_ticket_created.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_separator_1
      // 
      this.lbl_separator_1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_separator_1.BackColor = System.Drawing.Color.Black;
      this.lbl_separator_1.Location = new System.Drawing.Point(10, 117);
      this.lbl_separator_1.Name = "lbl_separator_1";
      this.lbl_separator_1.Size = new System.Drawing.Size(780, 2);
      this.lbl_separator_1.TabIndex = 115;
      // 
      // lbl_ticket_status_value
      // 
      this.lbl_ticket_status_value.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.lbl_ticket_status_value.AutoSize = true;
      this.lbl_ticket_status_value.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_ticket_status_value.Location = new System.Drawing.Point(149, 98);
      this.lbl_ticket_status_value.Name = "lbl_ticket_status_value";
      this.lbl_ticket_status_value.Size = new System.Drawing.Size(16, 16);
      this.lbl_ticket_status_value.TabIndex = 114;
      this.lbl_ticket_status_value.Text = "0";
      this.lbl_ticket_status_value.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // btn_print_ticket
      // 
      this.btn_print_ticket.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_print_ticket.BackColor = System.Drawing.Color.PaleTurquoise;
      this.btn_print_ticket.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btn_print_ticket.Location = new System.Drawing.Point(755, 442);
      this.btn_print_ticket.Name = "btn_print_ticket";
      this.btn_print_ticket.Size = new System.Drawing.Size(128, 48);
      this.btn_print_ticket.TabIndex = 114;
      this.btn_print_ticket.Text = "xPrint Ticket";
      this.btn_print_ticket.UseVisualStyleBackColor = false;
      // 
      // lbl_ticket_status
      // 
      this.lbl_ticket_status.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_ticket_status.Location = new System.Drawing.Point(73, 95);
      this.lbl_ticket_status.Name = "lbl_ticket_status";
      this.lbl_ticket_status.Size = new System.Drawing.Size(75, 23);
      this.lbl_ticket_status.TabIndex = 113;
      this.lbl_ticket_status.Text = "xStatus:";
      this.lbl_ticket_status.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_ticket_amount_value
      // 
      this.lbl_ticket_amount_value.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.lbl_ticket_amount_value.AutoSize = true;
      this.lbl_ticket_amount_value.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_ticket_amount_value.Location = new System.Drawing.Point(149, 70);
      this.lbl_ticket_amount_value.Name = "lbl_ticket_amount_value";
      this.lbl_ticket_amount_value.Size = new System.Drawing.Size(28, 16);
      this.lbl_ticket_amount_value.TabIndex = 112;
      this.lbl_ticket_amount_value.Text = "0.0";
      this.lbl_ticket_amount_value.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_ticket_amount
      // 
      this.lbl_ticket_amount.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_ticket_amount.Location = new System.Drawing.Point(73, 67);
      this.lbl_ticket_amount.Name = "lbl_ticket_amount";
      this.lbl_ticket_amount.Size = new System.Drawing.Size(75, 23);
      this.lbl_ticket_amount.TabIndex = 111;
      this.lbl_ticket_amount.Text = "xAmount:";
      this.lbl_ticket_amount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_validation_number
      // 
      this.lbl_validation_number.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.lbl_validation_number.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_validation_number.ForeColor = System.Drawing.Color.Black;
      this.lbl_validation_number.Location = new System.Drawing.Point(149, 46);
      this.lbl_validation_number.Name = "lbl_validation_number";
      this.lbl_validation_number.Size = new System.Drawing.Size(72, 21);
      this.lbl_validation_number.TabIndex = 94;
      this.lbl_validation_number.Text = "012345";
      this.lbl_validation_number.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_ticket_number2
      // 
      this.lbl_ticket_number2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_ticket_number2.Location = new System.Drawing.Point(74, 46);
      this.lbl_ticket_number2.Name = "lbl_ticket_number2";
      this.lbl_ticket_number2.Size = new System.Drawing.Size(74, 21);
      this.lbl_ticket_number2.TabIndex = 93;
      this.lbl_ticket_number2.Text = "xTicket";
      this.lbl_ticket_number2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // timer1
      // 
      this.timer1.Interval = 500;
      // 
      // btn_ticket_redeem_total
      // 
      this.btn_ticket_redeem_total.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_ticket_redeem_total.BackColor = System.Drawing.Color.PaleTurquoise;
      this.btn_ticket_redeem_total.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btn_ticket_redeem_total.Location = new System.Drawing.Point(755, 496);
      this.btn_ticket_redeem_total.Name = "btn_ticket_redeem_total";
      this.btn_ticket_redeem_total.Size = new System.Drawing.Size(128, 48);
      this.btn_ticket_redeem_total.TabIndex = 113;
      this.btn_ticket_redeem_total.Text = "xTotal Redeem";
      this.btn_ticket_redeem_total.UseVisualStyleBackColor = false;
      // 
      // gb_ticket_props
      // 
      this.gb_ticket_props.Controls.Add(this.lbl_account_id_value);
      this.gb_ticket_props.Controls.Add(this.lbl_block_reason);
      this.gb_ticket_props.Controls.Add(this.lbl_blocked);
      this.gb_ticket_props.Controls.Add(this.lbl_trackdata);
      this.gb_ticket_props.Controls.Add(this.lbl_name);
      this.gb_ticket_props.Controls.Add(this.pb_blocked);
      this.gb_ticket_props.Controls.Add(this.lbl_happy_birthday);
      this.gb_ticket_props.Controls.Add(this.lbl_id);
      this.gb_ticket_props.Controls.Add(this.lbl_id_title);
      this.gb_ticket_props.Controls.Add(this.lbl_track_number);
      this.gb_ticket_props.Controls.Add(this.lbl_birth_title);
      this.gb_ticket_props.Controls.Add(this.lbl_account_id_name);
      this.gb_ticket_props.Controls.Add(this.lbl_birth);
      this.gb_ticket_props.Controls.Add(this.lbl_holder_name);
      this.gb_ticket_props.Controls.Add(this.label2);
      this.gb_ticket_props.Controls.Add(this.label1);
      this.gb_ticket_props.Controls.Add(this.lbl_ticket_modified_terminal_type_value);
      this.gb_ticket_props.Controls.Add(this.lbl_ticket_modified_terminal_type);
      this.gb_ticket_props.Controls.Add(this.lbl_ticket_modified_terminal_value);
      this.gb_ticket_props.Controls.Add(this.lbl_ticket_modified_terminal);
      this.gb_ticket_props.Controls.Add(this.lbl_ticket_modified_value);
      this.gb_ticket_props.Controls.Add(this.lbl_ticket_modified);
      this.gb_ticket_props.Controls.Add(this.lbl_ticket_created_terminal_type_value);
      this.gb_ticket_props.Controls.Add(this.lbl_ticket_created_terminal_type);
      this.gb_ticket_props.Controls.Add(this.lbl_ticket_terminal_creation_value);
      this.gb_ticket_props.Controls.Add(this.lbl_ticket_terminal_creation);
      this.gb_ticket_props.Controls.Add(this.lbl_ticket_created_value);
      this.gb_ticket_props.Controls.Add(this.lbl_ticket_created);
      this.gb_ticket_props.Controls.Add(this.lbl_ticket_status_value);
      this.gb_ticket_props.Controls.Add(this.lbl_ticket_status);
      this.gb_ticket_props.Controls.Add(this.lbl_ticket_amount_value);
      this.gb_ticket_props.Controls.Add(this.lbl_ticket_amount);
      this.gb_ticket_props.Controls.Add(this.lbl_validation_number);
      this.gb_ticket_props.Controls.Add(this.lbl_ticket_number2);
      this.gb_ticket_props.Location = new System.Drawing.Point(15, 131);
      this.gb_ticket_props.Name = "gb_ticket_props";
      this.gb_ticket_props.Size = new System.Drawing.Size(708, 394);
      this.gb_ticket_props.TabIndex = 112;
      this.gb_ticket_props.TabStop = false;
      // 
      // label1
      // 
      this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label1.Location = new System.Drawing.Point(31, 16);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(179, 21);
      this.label1.TabIndex = 127;
      this.label1.Text = "xInfo de ticket";
      this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // uc_ticket_reader1
      // 
      this.uc_ticket_reader1.Location = new System.Drawing.Point(262, 7);
      this.uc_ticket_reader1.Name = "uc_ticket_reader1";
      this.uc_ticket_reader1.Size = new System.Drawing.Size(337, 108);
      this.uc_ticket_reader1.TabIndex = 116;
      // 
      // label2
      // 
      this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label2.Location = new System.Drawing.Point(368, 16);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(242, 21);
      this.label2.TabIndex = 128;
      this.label2.Text = "xInfo cuenta asociada";
      this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_account_id_value
      // 
      this.lbl_account_id_value.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_account_id_value.ForeColor = System.Drawing.Color.Black;
      this.lbl_account_id_value.Location = new System.Drawing.Point(433, 58);
      this.lbl_account_id_value.Name = "lbl_account_id_value";
      this.lbl_account_id_value.Size = new System.Drawing.Size(72, 21);
      this.lbl_account_id_value.TabIndex = 134;
      this.lbl_account_id_value.Text = "012345";
      this.lbl_account_id_value.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_block_reason
      // 
      this.lbl_block_reason.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
      this.lbl_block_reason.ForeColor = System.Drawing.Color.Black;
      this.lbl_block_reason.Location = new System.Drawing.Point(575, 69);
      this.lbl_block_reason.Name = "lbl_block_reason";
      this.lbl_block_reason.Size = new System.Drawing.Size(128, 21);
      this.lbl_block_reason.TabIndex = 143;
      this.lbl_block_reason.Text = "xDesde Cajero";
      this.lbl_block_reason.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.lbl_block_reason.Visible = false;
      // 
      // lbl_blocked
      // 
      this.lbl_blocked.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_blocked.ForeColor = System.Drawing.Color.Black;
      this.lbl_blocked.Location = new System.Drawing.Point(575, 48);
      this.lbl_blocked.Name = "lbl_blocked";
      this.lbl_blocked.Size = new System.Drawing.Size(128, 21);
      this.lbl_blocked.TabIndex = 142;
      this.lbl_blocked.Text = "xBloqueado";
      this.lbl_blocked.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.lbl_blocked.Visible = false;
      // 
      // lbl_trackdata
      // 
      this.lbl_trackdata.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_trackdata.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_trackdata.Location = new System.Drawing.Point(323, 78);
      this.lbl_trackdata.Name = "lbl_trackdata";
      this.lbl_trackdata.Size = new System.Drawing.Size(58, 23);
      this.lbl_trackdata.TabIndex = 141;
      this.lbl_trackdata.Text = "Tarjeta:";
      this.lbl_trackdata.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_name
      // 
      this.lbl_name.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_name.Location = new System.Drawing.Point(323, 129);
      this.lbl_name.Name = "lbl_name";
      this.lbl_name.Size = new System.Drawing.Size(102, 23);
      this.lbl_name.TabIndex = 140;
      this.lbl_name.Text = "xName:";
      this.lbl_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // pb_blocked
      // 
      this.pb_blocked.Enabled = false;
      this.pb_blocked.Location = new System.Drawing.Point(522, 46);
      this.pb_blocked.Name = "pb_blocked";
      this.pb_blocked.Size = new System.Drawing.Size(48, 46);
      this.pb_blocked.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
      this.pb_blocked.TabIndex = 132;
      this.pb_blocked.TabStop = false;
      this.pb_blocked.Visible = false;
      // 
      // lbl_happy_birthday
      // 
      this.lbl_happy_birthday.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_happy_birthday.BackColor = System.Drawing.Color.MediumTurquoise;
      this.lbl_happy_birthday.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_happy_birthday.ForeColor = System.Drawing.Color.Blue;
      this.lbl_happy_birthday.Location = new System.Drawing.Point(349, 295);
      this.lbl_happy_birthday.Name = "lbl_happy_birthday";
      this.lbl_happy_birthday.Size = new System.Drawing.Size(190, 23);
      this.lbl_happy_birthday.TabIndex = 139;
      this.lbl_happy_birthday.Text = "xHappy Birthday!";
      this.lbl_happy_birthday.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.lbl_happy_birthday.Visible = false;
      // 
      // lbl_id
      // 
      this.lbl_id.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_id.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_id.Location = new System.Drawing.Point(323, 209);
      this.lbl_id.Name = "lbl_id";
      this.lbl_id.Size = new System.Drawing.Size(190, 23);
      this.lbl_id.TabIndex = 137;
      this.lbl_id.Text = "AAAAAAAAAAAAAAAAAAAB";
      this.lbl_id.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_id_title
      // 
      this.lbl_id_title.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_id_title.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_id_title.Location = new System.Drawing.Point(323, 177);
      this.lbl_id_title.Name = "lbl_id_title";
      this.lbl_id_title.Size = new System.Drawing.Size(187, 23);
      this.lbl_id_title.TabIndex = 138;
      this.lbl_id_title.Text = "xCarnet de padre:";
      this.lbl_id_title.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_track_number
      // 
      this.lbl_track_number.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_track_number.Location = new System.Drawing.Point(323, 102);
      this.lbl_track_number.Name = "lbl_track_number";
      this.lbl_track_number.Size = new System.Drawing.Size(158, 20);
      this.lbl_track_number.TabIndex = 129;
      this.lbl_track_number.Text = "88888888888888888812";
      this.lbl_track_number.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_birth_title
      // 
      this.lbl_birth_title.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_birth_title.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_birth_title.Location = new System.Drawing.Point(323, 239);
      this.lbl_birth_title.Name = "lbl_birth_title";
      this.lbl_birth_title.Size = new System.Drawing.Size(91, 23);
      this.lbl_birth_title.TabIndex = 136;
      this.lbl_birth_title.Text = "Nacimiento:";
      this.lbl_birth_title.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_account_id_name
      // 
      this.lbl_account_id_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_account_id_name.Location = new System.Drawing.Point(323, 49);
      this.lbl_account_id_name.Name = "lbl_account_id_name";
      this.lbl_account_id_name.Size = new System.Drawing.Size(74, 21);
      this.lbl_account_id_name.TabIndex = 133;
      this.lbl_account_id_name.Text = "xAccount";
      this.lbl_account_id_name.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_birth
      // 
      this.lbl_birth.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_birth.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_birth.Location = new System.Drawing.Point(536, 239);
      this.lbl_birth.Name = "lbl_birth";
      this.lbl_birth.Size = new System.Drawing.Size(114, 23);
      this.lbl_birth.TabIndex = 135;
      this.lbl_birth.Text = "99 / 99 / 9999";
      this.lbl_birth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_holder_name
      // 
      this.lbl_holder_name.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_holder_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_holder_name.Location = new System.Drawing.Point(323, 154);
      this.lbl_holder_name.Name = "lbl_holder_name";
      this.lbl_holder_name.Size = new System.Drawing.Size(261, 23);
      this.lbl_holder_name.TabIndex = 131;
      this.lbl_holder_name.Text = "TTTTTTTTTTTTT";
      this.lbl_holder_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.lbl_holder_name.UseMnemonic = false;
      // 
      // uc_ticket_reprint
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.uc_ticket_reader1);
      this.Controls.Add(this.lbl_separator_1);
      this.Controls.Add(this.btn_print_ticket);
      this.Controls.Add(this.btn_ticket_redeem_total);
      this.Controls.Add(this.gb_ticket_props);
      this.Name = "uc_ticket_reprint";
      this.Size = new System.Drawing.Size(899, 684);
      this.gb_ticket_props.ResumeLayout(false);
      this.gb_ticket_props.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pb_blocked)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Label lbl_ticket_modified_terminal_type;
    private System.Windows.Forms.Label lbl_ticket_modified_terminal_type_value;
    private System.Windows.Forms.Label lbl_ticket_modified_terminal_value;
    private System.Windows.Forms.Label lbl_ticket_modified_terminal;
    private System.Windows.Forms.Label lbl_ticket_modified_value;
    private System.Windows.Forms.Label lbl_ticket_modified;
    private System.Windows.Forms.Label lbl_ticket_created_terminal_type_value;
    private System.Windows.Forms.Label lbl_ticket_created_terminal_type;
    private System.Windows.Forms.Label lbl_ticket_terminal_creation_value;
    private System.Windows.Forms.Label lbl_ticket_terminal_creation;
    private System.Windows.Forms.Label lbl_ticket_created_value;
    private System.Windows.Forms.Label lbl_ticket_created;
    private System.Windows.Forms.Label lbl_separator_1;
    private System.Windows.Forms.Label lbl_ticket_status_value;
    private System.Windows.Forms.Button btn_print_ticket;
    private System.Windows.Forms.Label lbl_ticket_status;
    private System.Windows.Forms.Label lbl_ticket_amount_value;
    private System.Windows.Forms.Label lbl_ticket_amount;
    private System.Windows.Forms.Label lbl_validation_number;
    private System.Windows.Forms.Label lbl_ticket_number2;
    private System.Windows.Forms.Timer timer1;
    private System.Windows.Forms.Button btn_ticket_redeem_total;
    private System.Windows.Forms.GroupBox gb_ticket_props;
    private System.Windows.Forms.Label label1;
    private uc_ticket_reader uc_ticket_reader1;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label lbl_account_id_value;
    private System.Windows.Forms.Label lbl_block_reason;
    private System.Windows.Forms.Label lbl_blocked;
    private System.Windows.Forms.Label lbl_trackdata;
    private System.Windows.Forms.Label lbl_name;
    private System.Windows.Forms.PictureBox pb_blocked;
    private System.Windows.Forms.Label lbl_happy_birthday;
    private System.Windows.Forms.Label lbl_id;
    private System.Windows.Forms.Label lbl_id_title;
    private System.Windows.Forms.Label lbl_track_number;
    private System.Windows.Forms.Label lbl_birth_title;
    private System.Windows.Forms.Label lbl_account_id_name;
    private System.Windows.Forms.Label lbl_birth;
    private System.Windows.Forms.Label lbl_holder_name;
  }
}
