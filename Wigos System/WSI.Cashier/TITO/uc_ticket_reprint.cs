//------------------------------------------------------------------------------
// Copyright � 2007-2013 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_ticket_reprint.cs
// 
//   DESCRIPTION: User control for reprinting tickets
//
//        AUTHOR: Joshwa Marcalle
//
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 25-AUG-2013 JRM    First release.
//------------ ------ ----------------------------------------------------------

using System;
using System.ComponentModel;
using System.Windows.Forms;
using WSI.Common;
using WSI.Cashier.TITO;
using System.Collections.Generic;

namespace WSI.Cashier.TITO
{
  public partial class uc_ticket_reprint : UserControl
  {
    static Boolean m_first_time = true;
    Boolean m_cashier_is_open;
    frm_yesno m_frm_shadow_gui;
    frm_container m_parent;
    TicketData m_ticket;
    CardData m_card;

    #region Constructor

    /// <summary>
    /// Constructor
    /// </summary>
    /// 
    public uc_ticket_reprint()
    {
      InitializeComponent();

      //m_frm_shadow_gui = new frm_yesno();
      //m_frm_shadow_gui.Opacity = 0.6;


      //EventLastAction.AddEventLastAction(this.Controls);
    }

    #region Private Methods
    /// <summary>
    /// Initialize controls
    /// </summary>
    public void InitControls(frm_container Parent)
    {
      m_parent = Parent;
      if (m_first_time)
      {
        //pb_ticket.Image = WSI.Cashier.Images.Get(Images.CashierImage.UserAnonymous);
        //pb_account.Image = WSI.Cashier.Images.Get(Images.CashierImage.UserAnonymous);
        //pb_blocked.Image = WSI.Cashier.Images.Get(Images.CashierImage.Locked);

        //uc_ticket_reader.OnValidationNumberReadEvent += new uc_ticket_reader.ValidationNumberReadEventHandler(uc_ticket_reader_OnValidationNumberReadEvent);
        //uc_card_reader.OnTrackNumberReadEvent += new uc_card_reader.TrackNumberReadEventHandler(uc_card_reader_OnTrackNumberReadEvent);

        m_first_time = false;
        InitializeControlResources();
      }

      m_cashier_is_open = CashierBusinessLogic.ReadCashierStatus(Cashier.SessionId) == CASHIER_STATUS.OPEN;
      clearForm();
      RestoreParentFocus();
      //uc_ticket_reader.Focus();
    }

    /// <summary>
    /// Initialize control resources. (NLS string, images, etc.)
    /// OJO: se reutilizan algunos recursos de otras pantallas
    /// </summary>
    void InitializeControlResources()
    {
      //      - Title
      //lbl_cards_title.Text = Resource.String("STR_UC_TICKETS");
      lbl_ticket_number2.Text = Resource.String("STR_UC_TICKET") + ":";
      lbl_ticket_amount.Text = Resource.String("STR_UC_TICKET_QUANTITY") + ":";
      lbl_ticket_status.Text = Resource.String("STR_FRM_VOUCHERS_REPRINT_009") + ":";
      lbl_ticket_created.Text = Resource.String("STR_UC_TICKET_CREATED_AT") + ":";
      lbl_ticket_terminal_creation.Text = Resource.String("STR_VOUCHER_TERMINAL_ID") + ":";
      lbl_ticket_created_terminal_type.Text = Resource.String("STR_UC_PLAYER_TRACK_TYPE") + ":";
      lbl_ticket_modified.Text = Resource.String("STR_UC_TICKET_MODIFIED_AT") + ":";
      lbl_ticket_modified_terminal.Text = Resource.String("STR_VOUCHER_TERMINAL_ID") + ":";
      lbl_ticket_modified_terminal_type.Text = Resource.String("STR_UC_PLAYER_TRACK_TYPE") + ":";

      //      - Account
      //lbl_account_id_name.Text = Resource.String("STR_FORMAT_GENERAL_NAME_ACCOUNT") + ":";
      //lbl_trackdata.Text = Resource.String("STR_FORMAT_GENERAL_NAME_CARD") + ":";
      //lbl_name.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_NAME") + ":";
      //lbl_id_title.Text = Resource.String("STR_FRM_PLAYER_EDIT_TYPE_DOCUMENT") + ":";
      //lbl_birth_title.Text = Resource.String("STR_UC_PLAYER_TRACK_BIRTH") + ":";
      //lbl_happy_birthday.Text = Resource.String("STR_UC_PLAYER_TRACK_BIRTHDAY_IN_NEXT_WEEK");

      //lbl_blocked.Text = Resource.String("STR_UC_CARD_BLOCK_STATUS_LOCKED");
      //lbl_block_reason.Text = "";

      btn_print_ticket.Text = Resource.String("STR_UC_TICKET_PRINT");
      btn_ticket_redeem_total.Text = Resource.String("STR_VOUCHER_VOUCHER_BUTTON");

      //uc_ticket_reader.InitializeControlResources();
      //uc_card_reader.InitializeControlResources();
    }

    // PURPOSE: Restore focus in th form
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    void RestoreParentFocus()
    {
      this.ParentForm.Focus();
      this.ParentForm.BringToFront();
    }

    // PURPOSE: Set status of ALL buttons in control
    // 
    //  PARAMS:
    //      - INPUT:
    //        - Ticket: reference to a ticket instance
    //
    //      - OUTPUT:
    //
    void setButtonStatus(TicketData Ticket)
    {
      if (Ticket != null)
      {
        btn_ticket_redeem_total.Enabled = Ticket.Status == TITO_STATES.VALID && m_cashier_is_open;
        btn_print_ticket.Enabled = Ticket.Status == TITO_STATES.VALID;
      }
      else
      {
        btn_ticket_redeem_total.Enabled = false;
        btn_print_ticket.Enabled = false;
      }
    }

    // PURPOSE: clear all controls in the form
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    void clearForm()
    {
      clearTicketControls();
      clearAccountControls();
    }

    // PURPOSE: Refresh texts in all controls in the form
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    void refreshForm()
    {
      refreshTicketControls(m_ticket);
      refreshAccountControls(m_card);
    }

    // PURPOSE: Clear controls related to ticket
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    void clearTicketControls()
    {
      m_ticket = null;
      refreshTicketControls(null);
    }

    // PURPOSE: Clear controls related to account
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    void clearAccountControls()
    {
      m_card = null;
      refreshAccountControls(null);
    }

    // PURPOSE: Refresh content of controls related to ticket
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    void refreshTicketControls(TicketData Ticket)
    {
      if (Ticket != null)
      {
        lbl_validation_number.Text = Ticket.ValidationNumber;
        lbl_ticket_amount_value.Text = Ticket.Amount.ToString();
        lbl_ticket_status_value.Text = TicketMgr.Instance.StatusAsString(Ticket.Status);
        lbl_ticket_created_value.Text = Ticket.CreatedAt.ToShortDateString();
        lbl_ticket_terminal_creation_value.Text = Ticket.CreatedTerminalName;
        lbl_ticket_created_terminal_type_value.Text = Ticket.CreatedTerminalTypeName;
        if (Ticket.LastActionAt.Ticks != 0)
        {
          lbl_ticket_modified_value.Text = Ticket.LastActionAt.ToShortDateString();
          lbl_ticket_modified_terminal_value.Text = Ticket.LastActionTerminalName;
          lbl_ticket_modified_terminal_type_value.Text = Ticket.LastActionTerminalTypeName;
        }
        else
        {
          lbl_ticket_modified_value.Text = "";
          lbl_ticket_modified_terminal_value.Text = "";
          lbl_ticket_modified_terminal_type_value.Text = "";
        }
      }
      else
      {
        lbl_validation_number.Text = "";
        lbl_ticket_amount_value.Text = "";
        lbl_ticket_status_value.Text = "";
        lbl_ticket_created_value.Text = "";
        lbl_ticket_terminal_creation_value.Text = "";
        lbl_ticket_created_terminal_type_value.Text = "";
        lbl_ticket_modified_value.Text = "";
        lbl_ticket_modified_terminal_value.Text = "";
        lbl_ticket_modified_terminal_type_value.Text = "";
      }

      setButtonStatus(Ticket);
    }

    // PURPOSE: Refresh content of controls related to Account
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    void refreshAccountControls(CardData Card)
    {
      DateTime _birth_date;
      DateTime _now_date;
      TimeSpan _tspam;

      if (Card != null)
      {
        //lbl_account_id_value.Text = Card.AccountId.ToString();
        //lbl_account_id_value.Visible = Card.AccountId != 0;

        //if (Card.IsRecycled)
        //  lbl_track_number.Text = CardData.RECYCLED_TRACK_DATA;
        //else
        //  lbl_track_number.Text = Card.VisibleTrackdata();

      //  lbl_id.Visible = false;
      //  lbl_birth.Visible = false;
      //  lbl_happy_birthday.Visible = false;
      //  if (Card.PlayerTracking.HolderName == null)
      //    lbl_holder_name.Text = "";
      //  else if (Card.PlayerTracking.HolderName == "")
      //    lbl_holder_name.Text = Resource.String("STR_UC_CARD_CARD_HOLDER_INTERNAL");
      //  else
      //  {
      //    lbl_holder_name.Text = Card.PlayerTracking.HolderName;

      //    lbl_id.Visible = true;
      //    lbl_id.Text = Card.PlayerTracking.HolderId;

      //    if (Card.PlayerTracking.HolderBirthDate != DateTime.MinValue)
      //    {
      //      lbl_birth.Text = Card.PlayerTracking.HolderBirthDate.ToShortDateString();
      //      lbl_birth.Visible = true;

      //      if (Card.PlayerTracking.HolderBirthDate.Month == 2
      //        && Card.PlayerTracking.HolderBirthDate.Day == 29
      //        && !DateTime.IsLeapYear(WGDB.Now.Year))
      //        _birth_date = new DateTime(WGDB.Now.Year, Card.PlayerTracking.HolderBirthDate.Month, Card.PlayerTracking.HolderBirthDate.AddDays(-1).Day);
      //      else
      //        _birth_date = new DateTime(WGDB.Now.Year, Card.PlayerTracking.HolderBirthDate.Month, Card.PlayerTracking.HolderBirthDate.Day);

      //      _now_date = new DateTime(WGDB.Now.Year, WGDB.Now.Month, WGDB.Now.Day);
      //      _tspam = _birth_date - _now_date;

      //      if (_tspam.TotalDays > 0 && _tspam.TotalDays <= 7)
      //      {
      //        lbl_happy_birthday.Text = Resource.String("STR_UC_PLAYER_TRACK_BIRTHDAY_IN_NEXT_WEEK");
      //        lbl_happy_birthday.Visible = true;
      //      }
      //      else if (_tspam.TotalDays == 0)
      //      {
      //        lbl_happy_birthday.Text = Resource.String("STR_UC_PLAYER_TRACK_HAPPY_BIRTHDAY");
      //        lbl_happy_birthday.Visible = true;
      //      }
      //    }
      //  }

      //  if (Card.Blocked)
      //  {
      //    lbl_blocked.Text = Resource.String("STR_UC_CARD_BLOCK_STATUS_LOCKED");
      //    lbl_block_reason.Text = Accounts.GetBlockReason(Card.BlockReason);
      //  }
      //  else
      //  {
      //    lbl_blocked.Text = Resource.String("STR_UC_CARD_BLOCK_STATUS_UNLOCKED");
      //    lbl_block_reason.Text = "";
      //  }
      //  lbl_blocked.Visible = Card.Blocked;
      //  lbl_block_reason.Visible = Card.Blocked;
      //  pb_blocked.Visible = Card.Blocked;
      //}
      //else
      //{
      //  lbl_account_id_value.Text = "";
      //  lbl_track_number.Text = "";
      //  lbl_holder_name.Text = "";
      //  lbl_id.Text = "";
      //  lbl_birth.Text = "";
      //  lbl_happy_birthday.Text = "";
      //  lbl_account_id_value.Text = "";
      //  lbl_blocked.Text = "";
      //  lbl_block_reason.Text = "";

      //  pb_blocked.Visible = false;
      }
    }

    // PURPOSE: Check if current cashier-user is an authorized user
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    Boolean UserHasPermission()
    {
      Boolean _result = false;
      String _error_str;
      List<ProfilePermissions.CashierFormFuncionality> _permissions_list;

      _permissions_list = new List<ProfilePermissions.CashierFormFuncionality>();
      _permissions_list.Add(ProfilePermissions.CashierFormFuncionality.CardAddRedeemCredit); // TODO: verificar si es otro Tipo y c�mo se a�aden
      _result = ProfilePermissions.CheckPermissionList(_permissions_list,
                                                       ProfilePermissions.TypeOperation.RequestPasswd,
                                                       this.ParentForm,
                                                       0,
                                                       out _error_str);
      return _result;
    }

    #endregion

    #region Events

    /// <summary>
    /// Event fired when a card is swiped.
    /// </summary>
    /// <param name="TrackNumber">Number read</param>
    /// <param name="IsValid">Track read is valid</param>
    void uc_card_reader_OnTrackNumberReadEvent(string TrackNumber, ref Boolean IsValid)
    {
      CardData _card_data;
      _card_data = new CardData();

      // Get Data by TrackNumber
      if (CardData.DB_CardGetAllData(TrackNumber, _card_data))
      {
        m_card = _card_data;
        refreshAccountControls(m_card);
      }
    }

    /// <summary>
    /// Event called after a card is swiped
    /// and called when a Player is searched
    /// </summary>
    /// <param name="TrackNumber">Number read</param>
    /// <param name="IsValid">Track read is valid</param>
    void uc_card_reader_OnTrackNumberReadEvent(Int64 AccountId, ref Boolean IsValid)
    {
      CardData _card_data;
      _card_data = new CardData();

      // Get Data by TrackNumber
      if (CardData.DB_CardGetAllData(AccountId, _card_data))
      {
        m_card = _card_data;
        refreshAccountControls(m_card);
      }
    }

    /// <summary>
    /// Event fired when a Ticket is swiped.
    /// </summary>
    /// <param name="ValidationNumber">Number read</param>
    /// <param name="IsValid">Track read is valid</param>
    void uc_ticket_reader_OnValidationNumberReadEvent(string ValidationNumber, ref Boolean IsValid)
    {
      Boolean _ticket_refreshed;
      Boolean _account_refreshed;
      TicketData _ticket;
      CardData _card_data;
      Cursor _old_cursor;

      _ticket_refreshed = false;
      _account_refreshed = false;
      _old_cursor = this.Cursor;

      try
      {
        // Load ticket data
        _ticket = TicketMgr.Instance.Load(ValidationNumber);
        if (_ticket != null)
        {
          m_ticket = _ticket;
          refreshTicketControls(m_ticket);
          _ticket_refreshed = true;
          IsValid = true;

          // load and show associated account
          if (m_ticket.CreatedAccountId > 0)
          {
            _card_data = new CardData();
            if (CardData.DB_CardGetAllData(m_ticket.CreatedAccountId, _card_data))
            {
              m_card = _card_data;
              refreshAccountControls(m_card);
              _account_refreshed = true;
            }
          }
        }
      }
      finally { }

      if (!_ticket_refreshed)
        clearTicketControls();

      if (!_account_refreshed)
        clearAccountControls();
    } // uc_ticket_reader_OnValidationNumberReadEvent

    private void btn_ticket_redeem_total_Click(object sender, EventArgs e)
    {
      int _max_allowed_int;
      ParamsTicketOperation _params_ticket_redeem;

      if (m_ticket == null || m_card == null || !this.UserHasPermission())
      {
        Log.Error("btn_ticket_redeem_total_Click: Previous control. There are no data for ticket redeem");
        return;
      }

      // NMR 14-UG-2013: Control of maximum amount permitted for ticket payment
      Int32.TryParse(Misc.ReadGeneralParams("TITO", "MaxAllowedATMReedem"), out _max_allowed_int);
      if (_max_allowed_int > 0 && m_ticket.Amount > _max_allowed_int)
      {
        frm_message.Show(Resource.String("STR_FRM_AMOUNT_INPUT_MSG_MAX_ALLOWED", _max_allowed_int),
                         Resource.String("STR_APP_GEN_MSG_WARNING"),
                         MessageBoxButtons.OK, MessageBoxIcon.Warning,
                         this.ParentForm);
        return;
      }

      //
      // the process
      //
      m_frm_shadow_gui.Show(this.ParentForm);

      // par�metros de entrada
      _params_ticket_redeem = new ParamsTicketOperation();
      _params_ticket_redeem.in_title = Resource.String("STR_UC_CARD_BTN_CREDIT_REDEEM_TOTAL");
      _params_ticket_redeem.in_voucher_type = VoucherTypes.TicketRedeem;
      _params_ticket_redeem.in_account = m_card;
      _params_ticket_redeem.in_cash_amount = m_ticket.Amount;
      _params_ticket_redeem.in_operation_mode = CASH_MODE.PARTIAL_REDEEM;
      _params_ticket_redeem.in_window_parent = m_frm_shadow_gui;

      if (BusinessLogic.Instance.CashierTicketIn(ref m_ticket, _params_ticket_redeem))
      {
        this.refreshTicketControls(m_ticket);
      }
      else
      {
        Log.Error("btn_ticket_redeem_total_Click: CashierTicketIn. Error paying ticket");
      }

      this.m_frm_shadow_gui.Hide();
      this.RestoreParentFocus();

    } // btn_ticket_redeem_total_Click

    private void btn_print_ticket_Click(object sender, EventArgs e)
    {
      //
    } // btn_print_ticket_Click

    private void but_search_account_Click(object sender, EventArgs e)
    {
      Int64 _selected_account_id;
      Boolean _is_track_valid = true;
      Cursor _previous_cursor;
      String _error_str;
      frm_search_players _frm_search_players;

      _selected_account_id = -1;

      // Check if current user is an authorized user
      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.AccountSearchByName,
                                               ProfilePermissions.TypeOperation.RequestPasswd,
                                               this.ParentForm,
                                               out _error_str))
      {
        return;
      }

      _previous_cursor = this.Cursor;

      try
      {
        this.Cursor = Cursors.WaitCursor;

        _frm_search_players = new frm_search_players();
        _frm_search_players.InitializeControlResources();
        _selected_account_id = _frm_search_players.Show(this.ParentForm);
        if (_selected_account_id > 0)
        {
          // Load card data
          uc_card_reader_OnTrackNumberReadEvent(_selected_account_id, ref _is_track_valid);
        }
      }
      finally
      {
        this.Cursor = _previous_cursor;
      }
    }

    private void lbl_cards_title_Click(object sender, EventArgs e)
    {

    }

 
    #endregion

    #endregion

  }
}
