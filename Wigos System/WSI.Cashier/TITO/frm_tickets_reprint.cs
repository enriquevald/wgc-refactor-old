//------------------------------------------------------------------------------
// Copyright � 2010 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_tickets_reprint.cs
// 
//   DESCRIPTION: Implements the following classes:
//                1. frm_tickets_reprint: List last 20 and current cashier sessions and,
//                   allows to print one or all operation tickets associated.
//
//        AUTHOR: JRM, cloned from frm_voucher_reprint
// 
// CREATION DATE: 17-SEP-2013
//
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 17-SEP-2013 JRM    First release.
// 20-SEP-2013 NMR    Deleted confirmation for Ticket Reprint
// 20-SEP-2013 JRM    Removed session table and functionallity, added date search filter for tickets
// 27-SEP-2013 NMR    Some aditional changes in design; changued search tickets condition
// 01-OCT-2013 NMR    Fixed error in dialog result; prevent printing when cashier is closed
// 03-OCT-2013 NMR    Deleted Ticket Movement
// 07-OCT-2013 HBB    Moved the function GetFilteredTicketsList from PrintMobileBankTickets
// 10-OCT-2013 NMR    Added new TITO permissions with tickets
// 24-OCT-2013 JBP    Redesign filter. 
// 11-NOV-2013 LEM    Show form in mode ticket list selection 
// 31-JAN-2013 JPJ    Added new STATUS column.
// 26-SEP-2014 DRV    Added KeyboardMessageEvent.
// 10-DEC-2014 DRV    Decimal currency formatting: DUT 201411 - Tito Chile - Currency formatting without decimals
// 17-DEC-2015 SMN    If filtered by Ticket Number, ignore the other filters
// 09-FEB-2016 FAV    PBI 9148: Void machine ticket
// 16-FEB-2016 FAV    Fixed Bug 9347, Added amount of the ticket voided (Information)
// 19-FEB-2016 FAV    Fixed Bug 9659, Added code to save the status of cashier movement looking source and type (tickets).
// 23-FEB-2016 RAB    Product Backlog Item 9888:TITO: Invalidar tickets en estado PENDING_CANCEL (Cajero)
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using WSI.Common;
using WSI.Common.TITO;
using WSI.Cashier.TITO;
using System.IO;
using System.Drawing.Text;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Globalization;
using System.Collections;

namespace WSI.Cashier
{
  public partial class frm_tickets_reprint : Controls.frm_base
  {
    #region Enums

    public enum FORM_MODE
    {
      REPRINT,
      SELECTION,
      LIST,
    }

    #endregion //Enums

    #region Constants

    // Number of columns
    public Int32 GRID_COLUMN_MAX_COLUMNS = 6; //Must be updated if change number of columns

    // Columns defined
    public Int32 GRID_COLUMN_TERMINAL_NAME = 0;
    public Int32 GRID_COLUMN_FLOOR_ID = 1;
    public Int32 GRID_COLUMN_CREATION_DATE = 2;
    public Int32 GRID_COLUMN_VALIDATION_NUMBER = 3;
    public Int32 GRID_COLUMN_TICKET_STATUS_NAME = 4;
    public Int32 GRID_COLUMN_AMOUNT = 5;
    public Int32 GRID_COLUMN_TICKET_ID = 6;
    public Int32 GRID_COLUMN_TICKET_TYPE = 7;
    public Int32 GRID_COLUMN_TICKET_STATUS = 8;
    public Int32 GRID_COLUMN_TICKET_CREATED_TERMINAL_TYPE = 9;


    // Column widths
    private const Int32 GRID_COLUMN_WIDTH_TERMINAL_NAME = 170;
    private const Int32 GRID_COLUMN_WIDTH_FLOOR_ID = 100;
    private const Int32 GRID_COLUMN_WIDTH_CREATION_DATE = 170;
    private const Int32 GRID_COLUMN_WIDTH_VALIDATION_NUMBER = 70;
    private const Int32 GRID_COLUMN_WIDTH_AMOUNT = 135;
    private const Int32 GRID_COLUMN_WIDTH_STATUS = 80;
    private const Int32 GRID_COLUMN_WIDTH_TICKET_ID = 0;
    private const Int32 GRID_COLUMN_WIDTH_TICKET_TYPE = 0;

    private const Int32 CREATED_IN_LAST_MINUTES = 60;
    private const Int32 DEFAULT_VOIDING_TICKETS_IN_LAST_DAYS = 15;
    #endregion // Constants

    #region Attributes

    private CASHIER_STATUS m_cashier_status;
    private frm_yesno form_yes_no;
    private Int32 m_selected_row_index;
    private FORM_MODE m_form_mode;

    #endregion // Attributes

    #region Constructor

    public frm_tickets_reprint()
    {
      InitializeComponent();
      InitializeControlResources();

      KeyboardMessageFilter.RegisterHandler(this, BarcodeType.FilterAnyTito, TicketReceivedEvent);

      form_yes_no = new frm_yesno();
      form_yes_no.Opacity = 0.6;
    }

    #endregion // Constructor

    #region Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Initialize control resources (NLS strings, images, etc).
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public void InitializeControlResources()
    {
      // labels 
      this.FormTitle = Resource.String("STR_FRM_VOUCHERS_REPRINT_054");        // Ticket Reprint
      lbl_floor_id.Text = Resource.String("STR_FRM_VOUCHERS_REPRINT_067");        // Name
      lbl_ticket_number.Text = Resource.String("STR_TICKET_NUMBER");                   // Ticket Number

      // Buttons
      btn_ticket_search.Text = Resource.String("STR_FRM_VOUCHERS_REPRINT_059");
      btn_close.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CLOSE");    // Close
      btn_print_selected.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_REPRINT");  // Manual Entry
      btn_cancell_selected.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_VOID_TICKET");

      // Textboxes
      gb_search.HeaderText = Resource.String("STR_FRM_VOUCHERS_REPRINT_053");

      // Show keyboard if Automatic OSK is enabled
      WSIKeyboard.Toggle();

    } // InitializeControlResources

    //------------------------------------------------------------------------------
    // PURPOSE : Initialize Operations Data Grid.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private void InitOperationsDataGrid()
    {
      String _date_format;
      Int32 _width_date_col;
      Boolean _list_mode;

      _date_format = System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern;
      _list_mode = (m_form_mode == FORM_MODE.SELECTION || m_form_mode == FORM_MODE.LIST);

      // Terminal name
      dgv_last_operations.Columns[GRID_COLUMN_TERMINAL_NAME].Width = GRID_COLUMN_WIDTH_TERMINAL_NAME;
      dgv_last_operations.Columns[GRID_COLUMN_TERMINAL_NAME].HeaderCell.Value = Resource.String("STR_TITO_MACHINE_TICKET_MACHINE_HEAD");

      // Last Operations DataGrid
      dgv_last_operations.ColumnHeadersHeight = 30;
      dgv_last_operations.RowTemplate.Height = 30;
      dgv_last_operations.RowTemplate.DividerHeight = 0;

      // Floor ID
      dgv_last_operations.Columns[GRID_COLUMN_FLOOR_ID].Width = GRID_COLUMN_WIDTH_FLOOR_ID;
      dgv_last_operations.Columns[GRID_COLUMN_FLOOR_ID].HeaderCell.Value = Resource.String("STR_FRM_VOUCHERS_REPRINT_067");
      dgv_last_operations.Columns[GRID_COLUMN_FLOOR_ID].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
      dgv_last_operations.Columns[GRID_COLUMN_FLOOR_ID].Visible = !_list_mode;

      // last ticket operation date
      dgv_last_operations.Columns[GRID_COLUMN_CREATION_DATE].HeaderCell.Value = Resource.String("STR_FRM_VOUCHERS_REPRINT_055");
      if (_list_mode)
      {
        _date_format += " " + CultureInfo.InvariantCulture.DateTimeFormat.LongTimePattern;
        _width_date_col = GRID_COLUMN_WIDTH_CREATION_DATE + GRID_COLUMN_WIDTH_FLOOR_ID;
      }
      else
      {
        _date_format = System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern + " " + CultureInfo.InvariantCulture.DateTimeFormat.LongTimePattern;
        _width_date_col = GRID_COLUMN_WIDTH_CREATION_DATE;
      }
      dgv_last_operations.Columns[GRID_COLUMN_CREATION_DATE].Width = _width_date_col;
      dgv_last_operations.Columns[GRID_COLUMN_CREATION_DATE].DefaultCellStyle.Format = _date_format;

      // ticket validation number
      dgv_last_operations.Columns[GRID_COLUMN_VALIDATION_NUMBER].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
      dgv_last_operations.Columns[GRID_COLUMN_VALIDATION_NUMBER].Width = GRID_COLUMN_WIDTH_VALIDATION_NUMBER;
      dgv_last_operations.Columns[GRID_COLUMN_VALIDATION_NUMBER].HeaderCell.Value = Resource.String("STR_FRM_VOUCHERS_REPRINT_056");

      // ticket status name
      dgv_last_operations.Columns[GRID_COLUMN_TICKET_STATUS_NAME].Width = GRID_COLUMN_WIDTH_STATUS;
      dgv_last_operations.Columns[GRID_COLUMN_TICKET_STATUS_NAME].HeaderCell.Value = Resource.String("STR_UC_TICKETS_05");
      dgv_last_operations.Columns[GRID_COLUMN_TICKET_STATUS_NAME].Visible = (m_form_mode == FORM_MODE.LIST) ? true : false;

      // ticket amount
      dgv_last_operations.Columns[GRID_COLUMN_AMOUNT].Width = GRID_COLUMN_WIDTH_AMOUNT;
      dgv_last_operations.Columns[GRID_COLUMN_AMOUNT].HeaderCell.Value = Resource.String("STR_FRM_VOUCHERS_REPRINT_057");
      dgv_last_operations.Columns[GRID_COLUMN_AMOUNT].DefaultCellStyle.Format = "C" + Math.Abs(Format.GetFormattedDecimals(CurrencyExchange.GetNationalCurrency())).ToString();
      dgv_last_operations.Columns[GRID_COLUMN_AMOUNT].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

      // ticket id     
      dgv_last_operations.Columns[GRID_COLUMN_TICKET_ID].Width = GRID_COLUMN_WIDTH_TICKET_ID;
      dgv_last_operations.Columns[GRID_COLUMN_TICKET_ID].HeaderCell.Value = "TI_TICKET_ID";
      dgv_last_operations.Columns[GRID_COLUMN_TICKET_ID].Visible = false;

      // ticket type     
      dgv_last_operations.Columns[GRID_COLUMN_TICKET_TYPE].Width = GRID_COLUMN_WIDTH_TICKET_TYPE;
      dgv_last_operations.Columns[GRID_COLUMN_TICKET_TYPE].HeaderCell.Value = "TI_TYPE_ID";
      dgv_last_operations.Columns[GRID_COLUMN_TICKET_TYPE].Visible = false;

      // ticket status 
      dgv_last_operations.Columns[GRID_COLUMN_TICKET_STATUS].Width = GRID_COLUMN_WIDTH_TICKET_TYPE;
      dgv_last_operations.Columns[GRID_COLUMN_TICKET_STATUS].HeaderCell.Value = "TI_STATUS";
      dgv_last_operations.Columns[GRID_COLUMN_TICKET_STATUS].Visible = false;

      // Source that create ticket (Terminal / Cashier)
      dgv_last_operations.Columns[GRID_COLUMN_TICKET_CREATED_TERMINAL_TYPE].Width = GRID_COLUMN_WIDTH_TICKET_TYPE;
      dgv_last_operations.Columns[GRID_COLUMN_TICKET_CREATED_TERMINAL_TYPE].HeaderCell.Value = "TI_CREATED_TERMINAL_TYPE";
      dgv_last_operations.Columns[GRID_COLUMN_TICKET_CREATED_TERMINAL_TYPE].Visible = false;

    } // InitOperationsDataGrid

    //------------------------------------------------------------------------------
    // PURPOSE : Load last operations from a CashierSessionId to the LastOperations DataGridView.
    //
    //  PARAMS :
    //      - INPUT :
    //          - CashierSessionId
    //          - SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private void LoadTicketsList(DataTable TicketsTable)
    {
      String[] _message_params = { "" };
      DataColumn _str_col;
      DataColumn _num_col;
      TITO_VALIDATION_TYPE _validation_type;
      Int64 _ticket_number;

      try
      {
        //Code to Format the Validation Number
        if (!TicketsTable.Columns.Contains("VALIDATION_NUMBER"))
        {
          _num_col = TicketsTable.Columns[GRID_COLUMN_VALIDATION_NUMBER];
          _str_col = TicketsTable.Columns.Add("VALIDATION_NUMBER", typeof(String));
          _str_col.SetOrdinal(GRID_COLUMN_VALIDATION_NUMBER);
          foreach (DataRow _row in TicketsTable.Rows)
          {
            _ticket_number = ValidationNumberManager.UnFormatValidationNumber(_row[_num_col.Ordinal].ToString(), out _validation_type);
            _row[_str_col.Ordinal] = ValidationNumberManager.FormatValidationNumber(_ticket_number, (Int32)_validation_type, true);
          }
          TicketsTable.Columns.Remove(_num_col);
        }

        //Code to Format the Status Number
        if (TicketsTable.Columns.Contains("TI_STATUS_NAME"))
        {
          _num_col = TicketsTable.Columns[GRID_COLUMN_TICKET_STATUS];
          _str_col = TicketsTable.Columns[GRID_COLUMN_TICKET_STATUS_NAME];
          foreach (DataRow _row in TicketsTable.Rows)
          {
            if (!String.IsNullOrEmpty(_row[_num_col.Ordinal].ToString()))
            {
              _row[_str_col.Ordinal] = BusinessLogic.StatusName(((TITO_TICKET_STATUS)_row[_num_col.Ordinal]));
            }
          }
        }

        dgv_last_operations.DataSource = TicketsTable;

        InitOperationsDataGrid();

        SetButtonStatus(TicketsTable.Rows.Count > 0);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } // LoadOperations


    //------------------------------------------------------------------------------
    // PURPOSE : Cancel a Machine Ticket
    //
    //  PARAMS :
    //      - INPUT :
    //          - Ticket
    //          - CardData
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private Boolean CancelMachineTicket(Ticket Ticket, CardData CardData, TITO_TERMINAL_TYPE CreateTerminalType, TITO_TICKET_TYPE Status)
    {
      CashierMovementsTable _cashier_mov_table;
      ParamsTicketOperation _operation_params;
      VoucherValidationNumber _voucher_ticket_info;
      Currency _not_redeemable_amount;
      Currency _redeemable_amount_to_add;
      Int64 _operation_id;
      CASHIER_MOVEMENT _cashier_movement = CASHIER_MOVEMENT.NOT_ASSIGNED; 

      _not_redeemable_amount = 0;
      _redeemable_amount_to_add = 0;

      _operation_params = new ParamsTicketOperation();
      _operation_params.in_account = CardData;
      _operation_params.in_window_parent = this;
      _operation_params.session_info = Cashier.CashierSessionInfo();
      _operation_params.out_cash_amount = Ticket.Amount;
      _operation_params.ticket_type = Ticket.TicketType;

      _voucher_ticket_info = new VoucherValidationNumber();
      _voucher_ticket_info.ValidationNumber = Ticket.ValidationNumber;
      _voucher_ticket_info.Amount = Ticket.Amount;
      _voucher_ticket_info.ValidationType = Ticket.ValidationType;
      _voucher_ticket_info.TicketType = Ticket.TicketType;

      _operation_params.validation_numbers = new List<VoucherValidationNumber>();
      _operation_params.validation_numbers.Add(_voucher_ticket_info);

      switch (CreateTerminalType)
      { 
        case TITO_TERMINAL_TYPE.CASHIER:
          if (Status == TITO_TICKET_TYPE.CASHABLE)
            _cashier_movement = CASHIER_MOVEMENT.TITO_TICKET_CASHIER_VOID_CASHABLE;
          else if (Status == TITO_TICKET_TYPE.PROMO_REDEEM)
            _cashier_movement = CASHIER_MOVEMENT.TITO_TICKET_CASHIER_VOID_PROMO_REDEEMABLE;
          else if (Status == TITO_TICKET_TYPE.PROMO_NONREDEEM)
            _cashier_movement = CASHIER_MOVEMENT.TITO_TICKET_CASHIER_VOID_PROMO_NOT_REDEEMABLE;
          break;
        case TITO_TERMINAL_TYPE.TERMINAL:
          if (Status == TITO_TICKET_TYPE.CASHABLE)
            _cashier_movement = CASHIER_MOVEMENT.TITO_TICKET_MACHINE_VOID_CASHABLE;
          else if (Status == TITO_TICKET_TYPE.PROMO_NONREDEEM)
            _cashier_movement = CASHIER_MOVEMENT.TITO_TICKET_MACHINE_VOID_PROMO_NOT_REDEEMABLE;
          break;
      }

      if (_cashier_movement == CASHIER_MOVEMENT.NOT_ASSIGNED)
      {
        Log.Error("It doesn't exist a valid cashier movement for cancel the ticket");
        return false;
      }

      using (DB_TRX _db_trx = new DB_TRX())
      {
        // 1. Cancel machine Ticket. Change status to DISCARDED
        if (!Ticket.Cashier_TicketDiscard(Ticket, Cashier.CashierSessionInfo().TerminalId, CardData.AccountId, _db_trx.SqlTransaction))
        {
          return false;
        }

        // 2. Create movement
        _cashier_mov_table = new CashierMovementsTable(CommonCashierInformation.CashierSessionInfo());
        _cashier_mov_table.Add(0, _cashier_movement, Ticket.Amount, CardData.AccountId, CardData.TrackData);
        if (!_cashier_mov_table.Save(_db_trx.SqlTransaction))
        {
          return false;
        }

        // 3. Create Operation
        if (_operation_params.ticket_type == TITO_TICKET_TYPE.PROMO_NONREDEEM)
        {
          _not_redeemable_amount = Ticket.Amount;
        }
        else if (_operation_params.ticket_type == TITO_TICKET_TYPE.CASHABLE)
        {
          _redeemable_amount_to_add = Ticket.Amount;
        }
        if (!Operations.DB_InsertOperation(OperationCode.TITO_VOID_MACHINE_TICKET, _operation_params.in_account.AccountId, Cashier.SessionId, 0,
                                  _redeemable_amount_to_add, 0, _not_redeemable_amount, 0, 0, 0, string.Empty, out _operation_id, _db_trx.SqlTransaction))
        {
          return false;
        }

        // 4. Create Voucher
        VoucherTitoTicketOperation _voucher_temp;
        _voucher_temp = new VoucherTitoTicketOperation(_operation_params.in_account.VoucherAccountInfo(), OperationCode.TITO_VOID_MACHINE_TICKET, 
                                                       false, _operation_params.validation_numbers, PrintMode.Print, _db_trx.SqlTransaction, Ticket);
        if (!_voucher_temp.Save(_operation_params.out_operation_id, _db_trx.SqlTransaction))
        {
          return false;
        }

        _db_trx.Commit();

        // 5. Print Voucher
        VoucherPrint.Print(_voucher_temp);
        return true;
      }
    }

    #endregion // Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Get DataTable with tickets
    //
    //  PARAMS :
    //      - INPUT :
    //          - String: SearchString
    //          - SqlTransaction: Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - DataTable: Ticket List
    //
    private DataTable GetFilteredTicketsList()
    {
      DataTable _dt_tickets;
      StringBuilder _sb;
      String _floor_id;
      String _ticket_number;
      String _provider_id;
      Int32 _terminal_id;
      Boolean _allow_to_void_gp; bool _allow_pay_tickets_pending_print;

      _dt_tickets = CreateSelectTicketTable();
      
      // RAB 23-FEB-2016      
      _allow_to_void_gp = GeneralParam.GetBoolean("TITO", "PendingCancel.AllowToVoid", false);
      _allow_pay_tickets_pending_print = GeneralParam.GetBoolean("TITO", "PendingPrint.AllowToRedeem", true);

      if (!txt_ticket_number.ValidateFormat())
      {
        return _dt_tickets;
      }
      _ticket_number = txt_ticket_number.Text.TrimStart('0');
      _floor_id = txt_floorID.Text.Trim();
      _terminal_id = uc_prov_terminals.TerminalSelected();
      _provider_id = uc_prov_terminals.ProviderSelected();
      _sb = new StringBuilder();

      try
      {
        _sb.AppendLine("    SELECT   TOP 100                                            ");
        _sb.AppendLine("             TE_NAME                                            ");
        _sb.AppendLine("           , TE_FLOOR_ID                                        ");
        _sb.AppendLine("           , TI_CREATED_DATETIME                                ");
        _sb.AppendLine("           , TI_VALIDATION_NUMBER                               ");
        _sb.AppendLine("           , TI_AMOUNT                                          ");
        _sb.AppendLine("           , TI_TICKET_ID                                       ");
        _sb.AppendLine("           , TI_TYPE_ID                                         ");
        _sb.AppendLine("           , TI_STATUS                                          ");
        _sb.AppendLine("           , TI_CREATED_TERMINAL_TYPE                           ");
        _sb.AppendLine("      FROM   TICKETS                                            ");
        _sb.AppendLine(" LEFT JOIN   TERMINALS                                          ");
        _sb.AppendLine("        ON   TE_TERMINAL_ID            = TI_CREATED_TERMINAL_ID ");
        _sb.AppendLine("     WHERE   TI_CREATED_DATETIME       > @pStartDate            ");

        if (_allow_pay_tickets_pending_print)
        {
          _sb.AppendLine("       AND  ((TI_STATUS                 = @pTicketValid         ");
          _sb.AppendLine("        OR  TI_STATUS                 = @pTicketPeningPrint)    ");
        }
        else
        {
          _sb.AppendLine("       AND  (TI_STATUS                 = @pTicketValid         ");
        }
        
        // RAB 23-FEB-2016
        if (_allow_to_void_gp && !String.IsNullOrEmpty(_ticket_number))
        {
          _sb.AppendLine("        OR   TI_STATUS               = @pTicketPendingCancel  ");
        }
        _sb.AppendLine("              )                                                 ");
        _sb.AppendLine("       AND   TI_TYPE_ID     IN (@pCashable,@pPromoRE,@pPromoNR) ");

        // 17-DEC-2015 SMN If filtered by Ticket Number, ignore the other filters.
        if (!String.IsNullOrEmpty(_ticket_number))
        {
          _sb.AppendLine("       AND  TI_VALIDATION_NUMBER  = @pValidationNumber ");
        }
        else
        {
        _sb.AppendLine("       AND   TI_CREATED_TERMINAL_TYPE  = @pTerminalType         ");

        if (_terminal_id > 0)
        {
          _sb.AppendLine("       AND   TE_TERMINAL_ID            = @pTerminalId           ");
        }
        if (!String.IsNullOrEmpty(_provider_id))
        {
          _sb.AppendLine("       AND   TE_PROVIDER_ID            = @pProviderId           ");
        }
        if (!String.IsNullOrEmpty(_floor_id))
        {
          _sb.AppendLine("       AND   TE_FLOOR_ID               LIKE '%'+@pFloorId+'%'   ");
        }

        _sb.AppendLine("  ORDER BY   TI_CREATED_DATETIME DESC                           ");
        }

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            if (_terminal_id > 0)
            {
              _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = _terminal_id;
            }
            if (!String.IsNullOrEmpty(_provider_id))
            {
              _cmd.Parameters.Add("@pProviderId", SqlDbType.NVarChar, 50).Value = _provider_id;
            }
            if (!String.IsNullOrEmpty(_floor_id))
            {
              _cmd.Parameters.Add("@pFloorId", SqlDbType.NVarChar, 20).Value = _floor_id;
            }
            if (!String.IsNullOrEmpty(_ticket_number))
            {
              _cmd.Parameters.Add("@pValidationNumber", SqlDbType.NVarChar, 20).Value = _ticket_number;
              _cmd.Parameters.Add("@pStartDate", SqlDbType.DateTime).Value = WGDB.Now.AddDays(-GeneralParam.GetInt32 ("TITO", "Tickets.MaxDaysAllowedToVoid", DEFAULT_VOIDING_TICKETS_IN_LAST_DAYS));
            }
            else
            {
              _cmd.Parameters.Add("@pStartDate", SqlDbType.DateTime).Value = WGDB.Now.AddMinutes(-CREATED_IN_LAST_MINUTES);
            }

            _cmd.Parameters.Add("@pTicketValid", SqlDbType.Int).Value = TITO_TICKET_STATUS.VALID;
            if(_allow_pay_tickets_pending_print) _cmd.Parameters.Add("@pTicketPeningPrint", SqlDbType.Int).Value = TITO_TICKET_STATUS.PENDING_PRINT;
            _cmd.Parameters.Add("@pTicketPendingCancel", SqlDbType.Int).Value = TITO_TICKET_STATUS.PENDING_CANCEL;
            _cmd.Parameters.Add("@pTerminalType", SqlDbType.Int).Value = TITO_TERMINAL_TYPE.TERMINAL;
            _cmd.Parameters.Add("@pCashable", SqlDbType.Int).Value = TITO_TICKET_TYPE.CASHABLE;
            _cmd.Parameters.Add("@pPromoRE", SqlDbType.Int).Value = TITO_TICKET_TYPE.PROMO_REDEEM;
            _cmd.Parameters.Add("@pPromoNR", SqlDbType.Int).Value = TITO_TICKET_TYPE.PROMO_NONREDEEM;

            _dt_tickets.Load(_db_trx.ExecuteReader(_cmd));

          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return _dt_tickets;
    } // GeTicketsFromCashierSessionId

    private void TicketReceivedEvent(KbdMsgEvent e, Object data)
    {
      Barcode _barcode;
      KeyPressEventArgs _event_args;

      if (e == KbdMsgEvent.EventBarcode)
      {
        _barcode = (Barcode)data;
        txt_ticket_number.Text = _barcode.ExternalCode;

        _event_args = new KeyPressEventArgs('\r');
        LoadTicketsList(GetFilteredTicketsList());
      }
    }

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Create Table Schema for ticket grid control
    //
    //  PARAMS :
    //      - INPUT :
    //      - OUTPUT :
    //
    // RETURNS : DataTable
    //
    //   NOTES :
    //
    public DataTable CreateSelectTicketTable()
    {
      DataTable _table = new DataTable();

      _table.Columns.Add("TE_NAME", Type.GetType("System.String"));
      _table.Columns.Add("TE_FLOOR_ID", Type.GetType("System.String"));
      _table.Columns.Add("TI_CREATED_DATETIME", Type.GetType("System.DateTime"));
      _table.Columns.Add("TI_VALIDATION_NUMBER", Type.GetType("System.Int64"));
      _table.Columns.Add("TI_STATUS_NAME", Type.GetType("System.String"));
      _table.Columns.Add("TI_AMOUNT", Type.GetType("System.Decimal"));
      _table.Columns.Add("TI_TICKET_ID", Type.GetType("System.Int64"));
      _table.Columns.Add("TI_TYPE_ID", Type.GetType("System.Int32"));
      _table.Columns.Add("TI_STATUS", Type.GetType("System.Int32"));
      _table.Columns.Add("TI_CREATED_TERMINAL_TYPE", Type.GetType("System.Int32"));

      _table.PrimaryKey = new DataColumn[] { _table.Columns["TICKET_ID"] };

      return _table;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Init and Show the form.
    //
    //  PARAMS :
    //      - INPUT :
    //          - ParentForm
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public void Show(Form Parent)
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_tickets_reprint", Log.Type.Message);

      StringBuilder _where;

      _where = new StringBuilder();

      _where.AppendLine(" WHERE   TE_TERMINAL_TYPE = " + (Int32)TerminalTypes.SAS_HOST);
      _where.AppendLine("   AND   (DATEADD(MINUTE, " + CREATED_IN_LAST_MINUTES + " , TE_RETIREMENT_REQUESTED) >= GETDATE()");
      _where.AppendLine("    OR   TE_RETIREMENT_REQUESTED IS NULL)");

      try
      {
        m_form_mode = FORM_MODE.REPRINT;

        uc_prov_terminals.InitControls("", _where.ToString(), false);

        LoadTicketsList(GetFilteredTicketsList());

        m_cashier_status = CashierBusinessLogic.ReadCashierStatus(Cashier.SessionId);

        form_yes_no.Show(Parent);
        this.ShowDialog(form_yes_no);
        form_yes_no.Hide();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } // Show

    public Int32 ShowForTicketSelect(DataTable TicketsTable)
    {
      gb_search.Visible = false;

      m_form_mode = FORM_MODE.SELECTION;
      m_selected_row_index = -1;

      btn_keyboard.Visible = true;

      KeyboardMessageFilter.UnregisterHandler(this, TicketReceivedEvent);

      if (TicketsTable.Rows.Count > 0)
      {
        m_selected_row_index = 0;
      }

      if (TicketsTable.Rows.Count > 1)
      {
        this.Height = this.Height - gb_search.Height;
        gb_last_operations.Location = new Point(gb_last_operations.Location.X, gb_last_operations.Location.Y - gb_search.Height);
        btn_close.Location = new Point(btn_close.Location.X, btn_close.Location.Y - gb_search.Height);
        btn_print_selected.Location = new Point(btn_print_selected.Location.X, btn_print_selected.Location.Y - gb_search.Height);
        btn_print_selected.Text = Resource.String("STR_FRM_VOUCHERS_REPRINT_076");
        this.FormTitle = Resource.String("STR_FRM_VOUCHERS_REPRINT_077");

        LoadTicketsList(TicketsTable);

        form_yes_no.Show(Parent);
        this.ShowDialog(form_yes_no);
        form_yes_no.Hide();
      }

      return m_selected_row_index;
    }

    public void ShowTicketList(DataTable TicketsTable)
    {
      gb_search.Visible = false;

      m_form_mode = FORM_MODE.LIST;
      m_selected_row_index = -1;

      btn_close.Location = btn_print_selected.Location;
      btn_keyboard.Visible = false;

      //this.Height = this.Height - gb_search.Height;
      gb_last_operations.Location = new Point(gb_last_operations.Location.X, gb_last_operations.Location.Y - gb_search.Height);
      gb_last_operations.Height = gb_last_operations.Height + gb_search.Height;
      //btn_close.Location = new Point(btn_close.Location.X, btn_close.Location.Y - gb_search.Height);
      btn_print_selected.Visible = false;
      this.FormTitle = Resource.String("STR_FRM_VOUCHERS_REPRINT_070");

      btn_cancell_selected.Visible = false;

      LoadTicketsList(TicketsTable);

      form_yes_no.Show(Parent);
      this.ShowDialog(form_yes_no);
      form_yes_no.Hide();

    }

    //------------------------------------------------------------------------------
    // PURPOSE : Dispose yes_no form.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    new public void Dispose()
    {
      form_yes_no.Dispose();
      form_yes_no = null;

      KeyboardMessageFilter.UnregisterHandler(this, TicketReceivedEvent);

      base.Dispose();
    } // Dispose

    #endregion // Public Methods

    #region Events

    #region Buttons

    //------------------------------------------------------------------------------
    // PURPOSE : Handle the click on the Close/Cancel button.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private void btn_close_Click(object sender, EventArgs e)
    {
      // Close Button

      Misc.WriteLog("[FORM CLOSE] frm_tickets_reprint (close)", Log.Type.Message);
      this.Close();

      return;

    } // btn_close_Click

    //------------------------------------------------------------------------------
    // PURPOSE: Set status of ALL buttons in control
    // 
    //  PARAMS:
    //      - INPUT:
    //        - Ticket: reference to a ticket instance
    //
    //      - OUTPUT:
    //
    private void SetButtonStatus(Boolean GridHasRows)
    {
      btn_print_selected.Enabled = (BusinessLogic.IsTITOCashierOpen || (m_form_mode == FORM_MODE.SELECTION)) &&
                                   GridHasRows;

      btn_cancell_selected.Enabled = (BusinessLogic.IsTITOCashierOpen || (m_form_mode == FORM_MODE.SELECTION)) &&
                                     GridHasRows;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Handle the click on the Print Selected button.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private void btn_print_selected_Click(object sender, EventArgs e)
    {
      ENUM_TITO_OPERATIONS_RESULT _recreate_ticket_result;
      ParamsTicketOperation _operation_params;
      CardData _virtual_card;
      ArrayList _voucher_list;
      VoucherValidationNumber _voucher_ticket_info;
      Ticket _reprint_ticket;
      List<ProfilePermissions.CashierFormFuncionality> _list_permissions;
      String _error_str;

      _list_permissions = new List<ProfilePermissions.CashierFormFuncionality>();

      // Check if current user is an authorized user
      _list_permissions.Add(ProfilePermissions.CashierFormFuncionality.TITO_ReprintTicket);

      _voucher_list = new ArrayList();

      if (dgv_last_operations.SelectedRows.Count == 0)
      {
        return;
      }

      if (m_form_mode == FORM_MODE.SELECTION)
      {
        m_selected_row_index = dgv_last_operations.SelectedRows[0].Index;
        btn_close_Click(null, null);

        return;
      }

      _recreate_ticket_result = ENUM_TITO_OPERATIONS_RESULT.OK;

      _virtual_card = new CardData();

      _reprint_ticket = Ticket.LoadTicket((Int64)dgv_last_operations.SelectedRows[0].Cells[GRID_COLUMN_TICKET_ID].Value);

      //comprobar permisos de los tickets
      switch (_reprint_ticket.TicketType) 
      {
        case TITO_TICKET_TYPE.PROMO_NONREDEEM:
          _list_permissions.Add(ProfilePermissions.CashierFormFuncionality.TITO_CreatePromoNRTicket);
          break;

        case TITO_TICKET_TYPE.CASHABLE:
          _list_permissions.Add(ProfilePermissions.CashierFormFuncionality.TITO_CreateRETicket);

          break;

        default:
          break;
      }
      if (_list_permissions.Count > 0)
      {
        if (!ProfilePermissions.CheckPermissionList(_list_permissions,
                                                    ProfilePermissions.TypeOperation.RequestPasswd,
                                                    this,
                                                    0,
                                                    out _error_str))
        {
          return;
        }
      }


      if (!BusinessLogic.LoadVirtualAccount(_virtual_card))
      {
        _recreate_ticket_result = ENUM_TITO_OPERATIONS_RESULT.AUX_DATA_NOT_FOUND;
      }

      _operation_params = new ParamsTicketOperation();
      _operation_params.in_account = _virtual_card;
      _operation_params.in_window_parent = this;
      _operation_params.session_info = Cashier.CashierSessionInfo();
      _operation_params.out_cash_amount = _reprint_ticket.Amount;
      _operation_params.ticket_type = _reprint_ticket.TicketType;
      _operation_params.in_cash_amt_0 = _reprint_ticket.Amount;
      _operation_params.in_cash_cur_0 = CurrencyExchange.GetNationalCurrency();

      _voucher_ticket_info = new VoucherValidationNumber();
      _voucher_ticket_info.ValidationNumber = _reprint_ticket.ValidationNumber;
      _voucher_ticket_info.Amount = _reprint_ticket.Amount;
      _voucher_ticket_info.ValidationType = _reprint_ticket.ValidationType;
      _voucher_ticket_info.TicketType = _reprint_ticket.TicketType;

      _operation_params.validation_numbers = new List<VoucherValidationNumber>();
      _operation_params.validation_numbers.Add(_voucher_ticket_info);

      if (_recreate_ticket_result == ENUM_TITO_OPERATIONS_RESULT.OK)
      {
        if (frm_message.Show(Resource.String("STR_TITO_TICKET_WARNING_REPRINT").Replace("\\r\\n", "\r\n"),
                             Resource.String("STR_APP_GEN_MSG_WARNING"),
                             MessageBoxButtons.YesNo,
                             MessageBoxIcon.Warning,
                             ParentForm) != DialogResult.OK)
        {
          _recreate_ticket_result = ENUM_TITO_OPERATIONS_RESULT.AUX_OPERATION_CANCELED;
        }
      }


      if (_recreate_ticket_result == ENUM_TITO_OPERATIONS_RESULT.OK && _reprint_ticket.CreatedDateTime < WGDB.Now.AddMinutes(-CREATED_IN_LAST_MINUTES))
      {
        _recreate_ticket_result = ENUM_TITO_OPERATIONS_RESULT.TICKET_NOT_NULLABLE;
      }

      if (_recreate_ticket_result == ENUM_TITO_OPERATIONS_RESULT.OK)
      {
        _recreate_ticket_result = BusinessLogic.TicketReprint(_reprint_ticket.TicketID, _operation_params, this.ParentForm, out _voucher_list);
      }

      if (_voucher_list.Count > 0)
      {
        VoucherPrint.Print(_voucher_list);
      }

      if (_recreate_ticket_result != ENUM_TITO_OPERATIONS_RESULT.OK)
      {
        if (_recreate_ticket_result != ENUM_TITO_OPERATIONS_RESULT.AUX_OPERATION_CANCELED)
        {
          switch (_recreate_ticket_result)
          {
            case ENUM_TITO_OPERATIONS_RESULT.TICKET_NOT_VALID:
              frm_message.Show(Resource.String("STR_UC_TITO_TICKET_NO_REPRINT"),
                     Resource.String("STR_APP_GEN_MSG_ERROR"),
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error,
                     ParentForm);
              break;
            case ENUM_TITO_OPERATIONS_RESULT.TICKET_NOT_NULLABLE:
              frm_message.Show(Resource.String("STR_UC_TITO_TICKET_TIME_LIMIT_TO_REPRINT", CREATED_IN_LAST_MINUTES),
                     Resource.String("STR_APP_GEN_MSG_ERROR"),
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error,
                     ParentForm);
              break;

            case ENUM_TITO_OPERATIONS_RESULT.PRINTER_PRINT_FAIL:
            case ENUM_TITO_OPERATIONS_RESULT.PRINTER_IS_NOT_READY:
              PrinterStatus _status;
              String _status_text;

              TitoPrinter.GetStatus(out _status, out _status_text);
              _status_text = TitoPrinter.GetMsgError(_status);

              if (_recreate_ticket_result == ENUM_TITO_OPERATIONS_RESULT.PRINTER_PRINT_FAIL)
              {
                _status_text += "\r\n" + Resource.String("STR_FRM_CONTAINER_EVENTS_DESC_PRINTER_CANCEL_OPERATION");
              }

              frm_message.Show(_status_text,
                               Resource.String("STR_APP_GEN_MSG_ERROR"),
                               MessageBoxButtons.OK,
                               MessageBoxIcon.Error,
                               this.ParentForm);
              break;
            default:
              frm_message.Show(Resource.String("STR_TITO_TICKET_ERROR_REPRINT"),
                 Resource.String("STR_APP_GEN_MSG_ERROR"),
                 MessageBoxButtons.OK,
                 MessageBoxIcon.Error,
                 this.ParentForm);
              break;
          }
        }
        this.Focus();
      }

    } // btn_print_selected_Click

    //------------------------------------------------------------------------------
    // PURPOSE : Handle the click on the Cancell Selected button.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private void btn_cancell_selected_Click(object sender, EventArgs e)
    {
      try
      {
        ENUM_TITO_OPERATIONS_RESULT _recreate_ticket_result = ENUM_TITO_OPERATIONS_RESULT.OK;
        CardData _virtual_card;
        Ticket _ticket_selected;
        String _error_str;
        Boolean _allow_to_void_gp;

        if (dgv_last_operations.SelectedRows.Count == 0)
        {
          return;
        }

        if (m_form_mode == FORM_MODE.SELECTION)
        {
          m_selected_row_index = dgv_last_operations.SelectedRows[0].Index;
          btn_close_Click(null, null);
          return;
        }

        if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.TITO_VoidMachineTicket,
                                           ProfilePermissions.TypeOperation.RequestPasswd,
                                           this,
                                           out _error_str))
        {
          return;
        }

        _virtual_card = new CardData();
        if (!BusinessLogic.LoadVirtualAccount(_virtual_card))
        {
          _recreate_ticket_result = ENUM_TITO_OPERATIONS_RESULT.AUX_DATA_NOT_FOUND;
        }

        if (_recreate_ticket_result == ENUM_TITO_OPERATIONS_RESULT.OK)
        {
          if (frm_message.Show(Resource.String("STR_TITO_MACHINE_TICKET_WARNING_VOID_TICKET", 
                               dgv_last_operations.SelectedRows[0].Cells[GRID_COLUMN_VALIDATION_NUMBER].Value,
                               dgv_last_operations.SelectedRows[0].Cells[GRID_COLUMN_TERMINAL_NAME].Value,
                               ((Currency)Convert.ToDecimal(dgv_last_operations.SelectedRows[0].Cells[GRID_COLUMN_AMOUNT].Value)).ToString(false)).Replace("\\r\\n", "\r\n"),
                               Resource.String("STR_APP_GEN_MSG_WARNING"),
                               MessageBoxButtons.YesNo, MessageBoxIcon.Warning, ParentForm) != DialogResult.OK)
          {
            _recreate_ticket_result = ENUM_TITO_OPERATIONS_RESULT.AUX_OPERATION_CANCELED;
          }
        }

        if (_recreate_ticket_result == ENUM_TITO_OPERATIONS_RESULT.OK)
        {
          // Load selected ticket in the grid
          _ticket_selected = Ticket.LoadTicket((Int64)dgv_last_operations.SelectedRows[0].Cells[GRID_COLUMN_TICKET_ID].Value);
          
          // RAB 23-FEB-2016
          _allow_to_void_gp = GeneralParam.GetBoolean("TITO", "PendingCancel.AllowToVoid", false);

          if ((_ticket_selected.Status == TITO_TICKET_STATUS.VALID || _ticket_selected.Status == TITO_TICKET_STATUS.PENDING_PRINT) || _allow_to_void_gp && _ticket_selected.Status == TITO_TICKET_STATUS.PENDING_CANCEL)
          {
            if (CancelMachineTicket(_ticket_selected, _virtual_card,
                (TITO_TERMINAL_TYPE)dgv_last_operations.SelectedRows[0].Cells[GRID_COLUMN_TICKET_CREATED_TERMINAL_TYPE].Value,
                (TITO_TICKET_TYPE)dgv_last_operations.SelectedRows[0].Cells[GRID_COLUMN_TICKET_TYPE].Value))
            {
              // Refresh grid
              LoadTicketsList(GetFilteredTicketsList());

              frm_message.Show(Resource.String("STR_TITO_MACHINE_TICKET_VOIDED_OK"), " ",
                  MessageBoxButtons.OK, MessageBoxIcon.Information, this);


              _recreate_ticket_result = ENUM_TITO_OPERATIONS_RESULT.OK;
            }
            else
            {
              _recreate_ticket_result = ENUM_TITO_OPERATIONS_RESULT.DATA_NOT_SAVED;
            }
          }
          else
          {
            _recreate_ticket_result = ENUM_TITO_OPERATIONS_RESULT.TICKET_NOT_VALID;
          }
        }

        if (_recreate_ticket_result != ENUM_TITO_OPERATIONS_RESULT.OK
            && _recreate_ticket_result != ENUM_TITO_OPERATIONS_RESULT.AUX_OPERATION_CANCELED)
        {
          frm_message.Show(Resource.String("STR_TITO_TICKET_ERROR_VOID_TICKET"),
                           Resource.String("STR_APP_GEN_MSG_ERROR"),
                           MessageBoxButtons.OK, MessageBoxIcon.Error, this.ParentForm);
        }

        this.Focus();
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

    } //btn_cancell_selected_Click

    #endregion // Buttons

    #region DataGridView

    #endregion // DataGridView
    //------------------------------------------------------------------------------
    // PURPOSE : Handle the reprint button click
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private void btn_ticket_search_Click(object sender, EventArgs e)
    {
      LoadTicketsList(GetFilteredTicketsList());
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Capture KeyPress event to invoke search when press Return
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private void txt_ticket_number_KeyPress(object sender, KeyPressEventArgs e)
    {
      if (e.KeyChar == '\r')
      {
        btn_ticket_search_Click(btn_ticket_search, e);
      }
      e.Handled = true;
    }

    private void btn_keyboard_Click(object sender, EventArgs e)
    {
      // show keyboard (if hidden)
      WSIKeyboard.ForceToggle();
    }

    #endregion // Events

  } // frm_vouchers_reprint

} // WSI.Cashier
