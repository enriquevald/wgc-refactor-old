namespace WSI.Cashier.TITO
{
  partial class frm_ticket_discard
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
      this.btn_close = new WSI.Cashier.Controls.uc_round_button();
      this.btn_validate_all = new WSI.Cashier.Controls.uc_round_button();
      this.lbl_scan_ticket_mode = new WSI.Cashier.Controls.uc_label();
      this.uc_ticket_reader = new WSI.Cashier.TITO.uc_ticket_reader();
      this.btn_accept = new WSI.Cashier.Controls.uc_round_button();
      this.btn_discard_all = new WSI.Cashier.Controls.uc_round_button();
      this.gb_tickets = new WSI.Cashier.Controls.uc_round_panel();
      this.dgv_tickets_list = new WSI.Cashier.Controls.uc_DataGridView();
      this.pnl_data.SuspendLayout();
      this.gb_tickets.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dgv_tickets_list)).BeginInit();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.Controls.Add(this.btn_close);
      this.pnl_data.Controls.Add(this.btn_validate_all);
      this.pnl_data.Controls.Add(this.lbl_scan_ticket_mode);
      this.pnl_data.Controls.Add(this.uc_ticket_reader);
      this.pnl_data.Controls.Add(this.btn_accept);
      this.pnl_data.Controls.Add(this.btn_discard_all);
      this.pnl_data.Controls.Add(this.gb_tickets);
      this.pnl_data.Size = new System.Drawing.Size(623, 543);
      this.pnl_data.TabIndex = 0;
      // 
      // btn_close
      // 
      this.btn_close.Anchor = System.Windows.Forms.AnchorStyles.Top;
      this.btn_close.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_close.FlatAppearance.BorderSize = 0;
      this.btn_close.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_close.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_close.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_close.Image = null;
      this.btn_close.IsSelected = false;
      this.btn_close.Location = new System.Drawing.Point(485, 481);
      this.btn_close.Name = "btn_close";
      this.btn_close.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_close.Size = new System.Drawing.Size(128, 48);
      this.btn_close.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_close.TabIndex = 6;
      this.btn_close.Text = "XCLOSE";
      this.btn_close.UseVisualStyleBackColor = false;
      this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
      // 
      // btn_validate_all
      // 
      this.btn_validate_all.Anchor = System.Windows.Forms.AnchorStyles.Top;
      this.btn_validate_all.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_validate_all.FlatAppearance.BorderSize = 0;
      this.btn_validate_all.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_validate_all.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_validate_all.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_validate_all.Image = null;
      this.btn_validate_all.IsSelected = false;
      this.btn_validate_all.Location = new System.Drawing.Point(485, 143);
      this.btn_validate_all.Name = "btn_validate_all";
      this.btn_validate_all.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_validate_all.Size = new System.Drawing.Size(128, 48);
      this.btn_validate_all.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_validate_all.TabIndex = 3;
      this.btn_validate_all.Text = "XVALIDATEALL";
      this.btn_validate_all.UseVisualStyleBackColor = false;
      this.btn_validate_all.Click += new System.EventHandler(this.btn_validate_all_Click);
      // 
      // lbl_scan_ticket_mode
      // 
      this.lbl_scan_ticket_mode.AutoSize = true;
      this.lbl_scan_ticket_mode.BackColor = System.Drawing.Color.Transparent;
      this.lbl_scan_ticket_mode.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_scan_ticket_mode.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_scan_ticket_mode.Location = new System.Drawing.Point(11, 114);
      this.lbl_scan_ticket_mode.Name = "lbl_scan_ticket_mode";
      this.lbl_scan_ticket_mode.Size = new System.Drawing.Size(110, 17);
      this.lbl_scan_ticket_mode.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_scan_ticket_mode.TabIndex = 1;
      this.lbl_scan_ticket_mode.Text = "xScanTicketMode";
      this.lbl_scan_ticket_mode.TextAlign = System.Drawing.ContentAlignment.TopRight;
      // 
      // uc_ticket_reader
      // 
      this.uc_ticket_reader.Location = new System.Drawing.Point(10, 20);
      this.uc_ticket_reader.MaximumSize = new System.Drawing.Size(800, 100);
      this.uc_ticket_reader.MinimumSize = new System.Drawing.Size(800, 60);
      this.uc_ticket_reader.Name = "uc_ticket_reader";
      this.uc_ticket_reader.Size = new System.Drawing.Size(800, 90);
      this.uc_ticket_reader.TabIndex = 0;
      // 
      // btn_accept
      // 
      this.btn_accept.Anchor = System.Windows.Forms.AnchorStyles.Top;
      this.btn_accept.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_accept.FlatAppearance.BorderSize = 0;
      this.btn_accept.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_accept.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_accept.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_accept.Image = null;
      this.btn_accept.IsSelected = false;
      this.btn_accept.Location = new System.Drawing.Point(485, 426);
      this.btn_accept.Name = "btn_accept";
      this.btn_accept.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_accept.Size = new System.Drawing.Size(128, 48);
      this.btn_accept.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_accept.TabIndex = 5;
      this.btn_accept.Text = "XACCEPT";
      this.btn_accept.UseVisualStyleBackColor = false;
      this.btn_accept.Click += new System.EventHandler(this.btn_accept_Click);
      // 
      // btn_discard_all
      // 
      this.btn_discard_all.Anchor = System.Windows.Forms.AnchorStyles.Top;
      this.btn_discard_all.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_discard_all.FlatAppearance.BorderSize = 0;
      this.btn_discard_all.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_discard_all.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_discard_all.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_discard_all.Image = null;
      this.btn_discard_all.IsSelected = false;
      this.btn_discard_all.Location = new System.Drawing.Point(485, 201);
      this.btn_discard_all.Name = "btn_discard_all";
      this.btn_discard_all.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_discard_all.Size = new System.Drawing.Size(128, 48);
      this.btn_discard_all.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_discard_all.TabIndex = 4;
      this.btn_discard_all.Text = "XDISCARDALL";
      this.btn_discard_all.UseVisualStyleBackColor = false;
      this.btn_discard_all.Click += new System.EventHandler(this.btn_discard_all_Click);
      // 
      // gb_tickets
      // 
      this.gb_tickets.BackColor = System.Drawing.Color.Transparent;
      this.gb_tickets.BorderColor = System.Drawing.Color.Empty;
      this.gb_tickets.Controls.Add(this.dgv_tickets_list);
      this.gb_tickets.CornerRadius = 10;
      this.gb_tickets.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_tickets.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gb_tickets.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_tickets.HeaderHeight = 35;
      this.gb_tickets.HeaderSubText = null;
      this.gb_tickets.HeaderText = "XTICKETS";
      this.gb_tickets.Location = new System.Drawing.Point(11, 143);
      this.gb_tickets.Name = "gb_tickets";
      this.gb_tickets.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_tickets.Size = new System.Drawing.Size(468, 386);
      this.gb_tickets.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.gb_tickets.TabIndex = 2;
      this.gb_tickets.Text = "xTickets";
      // 
      // dgv_tickets_list
      // 
      this.dgv_tickets_list.AllowUserToAddRows = false;
      this.dgv_tickets_list.AllowUserToDeleteRows = false;
      this.dgv_tickets_list.AllowUserToResizeColumns = false;
      this.dgv_tickets_list.AllowUserToResizeRows = false;
      this.dgv_tickets_list.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.dgv_tickets_list.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.dgv_tickets_list.ColumnHeaderHeight = 35;
      this.dgv_tickets_list.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
      dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      dataGridViewCellStyle1.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
      dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
      dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
      this.dgv_tickets_list.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
      this.dgv_tickets_list.ColumnHeadersHeight = 35;
      this.dgv_tickets_list.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
      this.dgv_tickets_list.CornerRadius = 10;
      this.dgv_tickets_list.DefaultCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.dgv_tickets_list.DefaultCellForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.dgv_tickets_list.DefaultCellSelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.dgv_tickets_list.DefaultCellSelectionForeColor = System.Drawing.Color.White;
      dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      dataGridViewCellStyle2.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.White;
      dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
      this.dgv_tickets_list.DefaultCellStyle = dataGridViewCellStyle2;
      this.dgv_tickets_list.EnableHeadersVisualStyles = false;
      this.dgv_tickets_list.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.dgv_tickets_list.GridColor = System.Drawing.Color.LightGray;
      this.dgv_tickets_list.HeaderBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
      this.dgv_tickets_list.HeaderDefaultCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.dgv_tickets_list.HeaderDefaultCellForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.dgv_tickets_list.Location = new System.Drawing.Point(0, 35);
      this.dgv_tickets_list.MultiSelect = false;
      this.dgv_tickets_list.Name = "dgv_tickets_list";
      this.dgv_tickets_list.ReadOnly = true;
      this.dgv_tickets_list.RowHeadersVisible = false;
      this.dgv_tickets_list.RowHeadersWidth = 20;
      this.dgv_tickets_list.RowTemplate.Height = 35;
      this.dgv_tickets_list.RowTemplateHeight = 35;
      this.dgv_tickets_list.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
      this.dgv_tickets_list.Size = new System.Drawing.Size(470, 350);
      this.dgv_tickets_list.Style = WSI.Cashier.Controls.uc_DataGridView.DataGridViewStyle.WITH_BOTTOM_ROUND_BORDERS;
      this.dgv_tickets_list.TabIndex = 0;
      this.dgv_tickets_list.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgv_tickets_list_CellFormatting);
      this.dgv_tickets_list.SelectionChanged += new System.EventHandler(this.dgv_tickets_list_SelectionChanged);
      // 
      // frm_ticket_discard
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(623, 598);
      this.Name = "frm_ticket_discard";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Style = WSI.Cashier.Controls.frm_base.BaseFormStyle.PRINCIPAL;
      this.Text = "TITO Cancel tickets";
      this.pnl_data.ResumeLayout(false);
      this.pnl_data.PerformLayout();
      this.gb_tickets.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.dgv_tickets_list)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private Controls.uc_round_button btn_accept;
    private Controls.uc_round_button btn_discard_all;
    private Controls.uc_round_panel gb_tickets;
    private Controls.uc_DataGridView dgv_tickets_list;
    private uc_ticket_reader uc_ticket_reader;
    private Controls.uc_label lbl_scan_ticket_mode;
    private Controls.uc_round_button btn_validate_all;
    private Controls.uc_round_button btn_close;

  }
}