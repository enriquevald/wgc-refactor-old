//------------------------------------------------------------------------------
// Copyright � 2007-2013 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_ticket_promo.cs
// 
//   DESCRIPTION: 
//
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 12-NOV-2013 LEM     First release.
// 12-DEC-2013 JPJ     Adding introducing numbers using keyboard.
// 16-DEC-2013 JPJ     Mantain filters, Virtual accounts shouldn't calculate spent per terminal, 
//                     spent was calculated only for the first account, block buttons when no selected promotion.
// 31-DEC-2013 JRM     Removed all references to method UserHasPermission and changed it for ProfilePermissions.CheckPermissions
// 09-JAN-2014 JPJ     Fixed Bug #WIGOSTITO-958: First time without interaction doesn't generate the promotion.
// 26-FEB-2014 XIT     Deleted SmartParams uses
// 05-FEB-2014 JFC     Resolved WIGOSTITO-1117 
// 07-FEB-2014 JRM     Fixed Bug No. WIGOSTITO-1120 
// 11-MAR-2014 JRM     Fixed Bug. WIGOSTITO-1133
// 12-MAR-2014 LEM     Fixed Bug WIGOSTITO-1137: Multiple errors.
// 24-MAR-2014 ICS     Fixed Bug WIGOSTITO-1141: Not appear promotions of exhausted redeemable
// 25-JUN-2014 DLL     Ficed Bug WIG-1041: When dgv_common don't have promotion selected disable buttons
// 30-JUL-2014 SGB     Fixed Buf WIGOSTITO-1247: Permission add to credit non redeemable and check old and new permissions in btn_assign_promo
// 22-OCT-2015 SDS     Add promotion by played - Backlog Item 3856
// 10-NOV-2015 ETP     Product Backlog Item 5808 - Cashier design - uc_ticket_promo.cs
// 11-NOV-2015 ETP     Product Backlog Item 5808 - Cashier design - New image column adapted, Added New header Icon.
// 08-FEB-2016 RGR     Product Backlog Item 6462: Garcia River-08: Cash Promotions NR: Print Ticket TITO or assign account
// 17-FEB-2016 RAB     Bug 9020:Promos - Selecci�n Incorrecta
// 22-FEB-2016 LTC     Task 9470 TITO Ticket Printing / Account Assignment
// 16-DIc-2016 XGJ     Bug 19792: Promoci�n que asigna puntos a la cuenta
// 21-DIC-2016 XGJ     Bug 21863: No se pueden vincular promociones a la cuenta
// 09-JAN-2017 FAV     Bug 21863: No se pueden vincular promociones a la cuenta
// 20-APR-2017 FAV     Bug 26704: Error al asignar promociones a la cuenta
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Drawing.Printing;
using WSI.Common;
using WSI.Cashier.TITO;
using WSI.Common.TITO;
using System.Collections;
using WSI.Cashier.Controls;
using System.IO;

namespace WSI.Cashier
{
  public partial class uc_ticket_promo : UserControl
  {
    private const Int32 GRID_COLUMN_PROMO_IMAGE = 0;
    private const Int32 GRID_COLUMN_PROMO_ID = 1;
    private const Int32 GRID_COLUMN_PROMO_NAME = 2;
    private const Int32 GRID_COLUMN_PROMO_WON_LOCK = 3;
    private const Int32 GRID_COLUMN_PROMO_CREDIT_TYPE = 4;
    private const Int32 GRID_COLUMN_PROMO_REWARD_TEXT = 5;
    private const Int32 GRID_COLUMN_PROMO_SORT = 6;

    #region Members

    private static frm_yesno form_yes_no;
    private CardData m_current_card;
    private frm_container m_parent;
    private Boolean m_promos_expanded_view;
    private List<Point> m_prev_loc = new List<Point>();
    private List<Size> m_prev_size = new List<Size>();
    private Int32 m_selected_promo_index;
    private Int64 m_selected_promo_id;
    private String m_selected_promo_name;
    private Promotion.TYPE_PROMOTION_DATA m_selected_promo_data;
    private Currency m_available_limit;
    private Boolean m_contracting = false;
    private Currency m_promo_reward_amount;
    private Currency m_promo_won_lock;
    private Boolean m_promo_warning_shown;
    private Currency m_spent_used;
    private Currency m_input_spent;
    private Currency m_input_played;
    private DataTable m_dt_remaining_spent_per_terminal;
    private Boolean m_virtual_mode;
    private Boolean m_form_embedded;
    private String m_filter;
    private Int32 m_max_promos;

    #endregion

    #region Constructor

    public uc_ticket_promo()
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[UC LOAD] uc_ticket_promo", Log.Type.Message);


      InitializeComponent();

      uc_round_button1.Visible = false;
#if DEBUG
      uc_round_button1.Visible = true;
#endif

      form_yes_no = new frm_yesno();
      form_yes_no.Opacity = 0.6;


      InicializeControlResources();

      SaveControlLocations();

      EventLastAction.AddEventLastAction(this.Controls);
    }

    #endregion

    #region Privates

    //------------------------------------------------------------------------------
    // PURPOSE: Iniciailize controls: NLS / Images ...
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    private void InicializeControlResources()
    {
      m_max_promos = GeneralParam.GetInt32("TITO", "MassivePromotions.MaxPromos", 400);
      gb_filter_box.HeaderText = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_034");
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Parent form restore focus and bring to front
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    private void RestoreParentFocus()
    {
      this.ParentForm.Focus();
      this.ParentForm.BringToFront();
    } // RestoreParentFocus

    //------------------------------------------------------------------------------
    // PURPOSE: Refresh card data controls whith empty info.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    private void ClearCardData()
    {
      BusinessLogic.LoadVirtualAccount(m_current_card);
      RefreshData(m_current_card);
    } // ClearCardData

    // PURPOSE: 
    //
    //  PARAMS:
    //     - INPUT:
    //           - none
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - none
    private void ShowPromosContractedView()
    {
      this.SuspendLayout();
      m_promos_expanded_view = false;

      try
      {
        if (dgv_common.Columns.Count > 0)
        {
          GuiStyleSheet();
        }

        gb_digits_box.Visible = m_virtual_mode;

        LoadControlLocations();
      }
      catch
      {
        m_promos_expanded_view = true;
      }

      SelectCurrentPromoGrid();

      btn_promos_change_view.Text = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_019");

      this.ResumeLayout();
    } // ShowPromosContractedView

    // PURPOSE: Show all promos mode
    //
    //  PARAMS:
    //     - INPUT:
    //           - none
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - none
    private void ShowPromosExpandedView()
    {
      Int32 _margin;
      Int32 _gb_width_add;
      Int32 _btn_top_add;

      this.SuspendLayout();
      m_promos_expanded_view = true;

      try
      {
        _margin = pnl_common.Width - dgv_common.Width;

        _gb_width_add = (this.Width - pnl_common.Left) - pnl_common.Right;

        if (m_virtual_mode)
        {
          _gb_width_add = _gb_width_add - gb_digits_box.Width - 18;
          gb_digits_box.Left += _gb_width_add;
          btn_create_ticket.Left += _gb_width_add;
        }

        pnl_common.Width += _gb_width_add;

        _btn_top_add = (pnl_common.Height - btn_promos_change_view.Height - _margin / 2) - btn_promos_change_view.Top;

        btn_promos_change_view.Left += _gb_width_add;
        btn_promos_change_view.Top += _btn_top_add;

        dgv_common.Width += _gb_width_add;
        dgv_common.Height += _btn_top_add;

        GuiStyleSheet();

        btn_promos_change_view.Text = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_020");
      }
      catch
      {
        m_promos_expanded_view = false;
      }

      gb_digits_box.Visible = m_virtual_mode;

      this.ResumeLayout();
    } // ShowPromosExpandedView

    private void SaveControlLocations()
    {
      m_prev_loc.Clear();
      m_prev_loc.Add(btn_promos_change_view.Location);
      m_prev_loc.Add(lbl_promo_reward_text.Location);
      m_prev_loc.Add(lbl_promo_reward_value.Location);
      m_prev_loc.Add(gb_digits_box.Location);
      m_prev_loc.Add(btn_create_ticket.Location);
      m_prev_loc.Add(btn_cancel.Location);
      m_prev_loc.Add(btn_assign_to_account.Location);

      m_prev_size.Clear();
      m_prev_size.Add(pnl_common.Size);
      m_prev_size.Add(dgv_common.Size);
    } // SaveControlLocations

    private void LoadControlLocations() // LoadControlLocations
    {
      if (m_prev_size.Count > 0)
      {
        pnl_common.Size = m_prev_size[0];
        dgv_common.Size = m_prev_size[1];
      }
      if (m_prev_loc.Count > 0)
      {
        btn_promos_change_view.Location = m_prev_loc[0];
        lbl_promo_reward_text.Location = m_prev_loc[1];
        lbl_promo_reward_value.Location = m_prev_loc[2];
        gb_digits_box.Location = m_prev_loc[3];
        btn_create_ticket.Location = m_prev_loc[4];
        btn_cancel.Location = m_prev_loc[5];
        btn_assign_to_account.Location = m_prev_loc[6];
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Handle PrePaint event data grid rows, to paint some of them with different color.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //    
    private void dgv_common_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
    {
      /*Color _back_color;

            _back_color = Color.Gray;

            switch ((WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE)dgv_common.Rows[e.RowIndex].Cells[GRID_COLUMN_PROMO_CREDIT_TYPE].Value)
            {
                case ACCOUNT_PROMO_CREDIT_TYPE.NR1:
                case ACCOUNT_PROMO_CREDIT_TYPE.NR2:
                    _back_color = Color.White;
                    break;
                case ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE:
                    _back_color = Color.LightSeaGreen;
                    break;
                case ACCOUNT_PROMO_CREDIT_TYPE.POINT:
                    _back_color = Color.LightSalmon;
                    break;
            }

            if ((Int32)dgv_common.Rows[e.RowIndex].Cells[GRID_COLUMN_PROMO_WON_LOCK].Value > 0)
            {
                _back_color = Color.PaleGoldenrod;
            }

            dgv_common.Rows[e.RowIndex].DefaultCellStyle.BackColor = _back_color;
      */
    } // dgv_common_RowPrePaint

    //------------------------------------------------------------------------------
    // PURPOSE : Restore promotion informational area to its default status.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //    - Non-populated fields are hidden
    private void ResetPromoRewardArea()
    {
      lbl_promo_reward_text.Visible = false;
      lbl_promo_reward_value.Visible = false;
    } // ResetPromoRewardArea

    //------------------------------------------------------------------------------
    // PURPOSE : Update promotion parameters area.
    //
    //  PARAMS :
    //      - INPUT :
    //          - AmountToShow: amount to display as reward
    //          - ShowLimit: flag to indicate whther the available limit for account/promotion
    //                       must be shown or not
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //    - Non-populated fields are hidden
    private void UpdatePromoRewardArea(Currency UnitaryAmount, Currency TotalAmount)
    {
      Currency _remainig_amount;
      Int32 _quantity;

      ResetPromoRewardArea();
      lbl_promo_reward_value.Text = "";

      if (UnitaryAmount == 0)
      {
        return;
      }

      _remainig_amount = TotalAmount % UnitaryAmount;
      _quantity = (Int32)(TotalAmount / UnitaryAmount);

      switch (m_selected_promo_data.credit_type)
      {
        case ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE:
          if (_quantity != 0)
          {
            lbl_promo_reward_value.Text = Resource.String("STR_FRM_TITO_PROMO_007", _quantity, Resource.String("STR_FRM_TITO_PROMO_011", UnitaryAmount));
          }
          if (_remainig_amount != 0)
          {
            if (!String.IsNullOrEmpty(lbl_promo_reward_value.Text))
            {
              lbl_promo_reward_value.Text += " " + Resource.String("STR_FRM_TITO_PROMO_012") + " ";
            }
            lbl_promo_reward_value.Text += Resource.String("STR_FRM_TITO_PROMO_007", 1, Resource.String("STR_FRM_TITO_PROMO_011", _remainig_amount));
          }

          break;
        case ACCOUNT_PROMO_CREDIT_TYPE.POINT:
          lbl_promo_reward_value.Text = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_003_POINTS", (Points)UnitaryAmount.SqlMoney);

          break;
        case ACCOUNT_PROMO_CREDIT_TYPE.NR1:
        case ACCOUNT_PROMO_CREDIT_TYPE.NR2:
        // Must show something in UNKNOWN and default values.
        case ACCOUNT_PROMO_CREDIT_TYPE.UNKNOWN:
        default:
          if (_quantity != 0)
          {
            lbl_promo_reward_value.Text = Resource.String("STR_FRM_TITO_PROMO_007", _quantity, Resource.String("STR_FRM_TITO_PROMO_010", UnitaryAmount));
          }
          if (_remainig_amount != 0)
          {
            if (!String.IsNullOrEmpty(lbl_promo_reward_value.Text))
            {
              lbl_promo_reward_value.Text += " " + Resource.String("STR_FRM_TITO_PROMO_012") + " ";
            }
            lbl_promo_reward_value.Text += Resource.String("STR_FRM_TITO_PROMO_007", 1, Resource.String("STR_FRM_TITO_PROMO_010", _remainig_amount));
          }

          break;
      }
      lbl_promo_reward_value.Visible = true;

    } // UpdatePromoRewardArea

    private void ComputeReward(DataRow Dr, SqlTransaction SqlTransac)
    {
      String _str_reward;
      Int64 _current_promo_id;
      Promotion.TYPE_PROMOTION_DATA _promo_data;
      Currency _input_spent;
      Currency _input_played;
      Boolean _rc;
      Promotion.PROMOTION_STATUS _promo_status;
      ACCOUNT_PROMO_CREDIT_TYPE _credit_type;
      Currency _reward = 0;
      Currency _won_lock = 0;
      Currency _spent_used;
      String _str_credit_type;
      String _str_credit;
      Currency _cash_in;
      Int32 _num_tokens;
      DataTable _account_flags;

      try
      {
        _account_flags = null;
        if (!m_virtual_mode)
        {
          if (!Promotion.GetActiveFlags(m_current_card.AccountId, SqlTransac, out _account_flags))
          {
            return;
          }
        }

        _cash_in = 0;
        // RCI 05-SEP-2012: Don't use Tokens to fill the grid values.
        //_num_tokens = m_promo_num_tokens;
        _num_tokens = 0;

        _current_promo_id = (Int64)Dr["PM_PROMOTION_ID"];
        _credit_type = (ACCOUNT_PROMO_CREDIT_TYPE)Dr["PM_CREDIT_TYPE"];
        _str_reward = "";

        _promo_data = new Promotion.TYPE_PROMOTION_DATA();
        _rc = Promotion.ReadPromotionData(SqlTransac, _current_promo_id, _promo_data);
        if (_rc)
        {
          _input_spent = 0;

          //SDS 20-10-2015
          _input_played = 0;

          if (!m_virtual_mode) // There is no need to calculate it when it's a virtual account.
          {

            // AMF 05-SEP-2013: Calculate spent per terminal
            // SDS 22-10-2015: calculate played & spent per terminal in one function only
            m_dt_remaining_spent_per_terminal = Promotion.AccountRemainingDataPerTerminal(m_current_card.AccountId, SqlTransac);
            Promotion.CaculateDataByTerminal(m_dt_remaining_spent_per_terminal,
                                               _promo_data.terminal_list,
                                               _current_promo_id,
                                               out _input_spent,
                                               out _input_played,
                                               SqlTransac);

          }

          _promo_status = Promotion.CheckPromotionApplicable(SqlTransac,
                                                             _current_promo_id,
                                                             m_current_card.AccountId,
                                                             true,
                                                             _account_flags,
                                                             ref _cash_in,
                                                             _input_spent,
                                                             _input_played,
                                                             ref _num_tokens,
                                                             ref _promo_data,
                                                             out _reward,
                                                             out _won_lock,
                                                             out _spent_used,
                                                             WGDB.Now);

          if (_reward > 0)
          {
            //_won_lock;

            switch (_credit_type)
            {
              case ACCOUNT_PROMO_CREDIT_TYPE.NR1:
              case ACCOUNT_PROMO_CREDIT_TYPE.NR2:
                _str_credit_type = "NR";
                _str_credit = ((Currency)_reward).ToString();

                break;
              case ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE:
                _str_credit_type = "RE";
                _str_credit = ((Currency)_reward).ToString();

                break;
              case ACCOUNT_PROMO_CREDIT_TYPE.POINT:
                _str_credit_type = "PT";
                _str_credit = ((Points)_reward.SqlMoney).ToString();

                break;
              case ACCOUNT_PROMO_CREDIT_TYPE.UNKNOWN:
              default:

                return;
            }

            _str_reward = String.Format("{0} {1}", _str_credit, _str_credit_type);
          }
        }

        Dr["REWARD"] = _str_reward;

      }
      catch
      { }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Retrieve promotion items that are appliable to current account and
    //           show them in the common data grid.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //    
    private void PopulatePromotionsGrid()
    {
      DataTable _dt;
      Promotion.PROMOTION_STATUS _promo_status;
      StringBuilder _sb;

      try
      {
        ResetPromoRewardArea();

        using (DB_TRX _db_trx = new DB_TRX())
        {
          _dt = Promotion.GetApplicablePromotionList(m_current_card.AccountId,
                                                     Resource.String("STR_FRM_AMOUNT_INPUT_002"),
                                                     out _promo_status,
                                                     _db_trx.SqlTransaction);

          //
          // Merge additional data from promotion table
          //
          _dt.PrimaryKey = new DataColumn[] { _dt.Columns["PM_PROMOTION_ID"] };

          _sb = new StringBuilder();
          _sb.AppendLine(" SELECT   PM_PROMOTION_ID                       ");
          _sb.AppendLine("        , PM_CREDIT_TYPE                        ");
          _sb.AppendLine("        , CASE WHEN PM_MIN_CASH_IN > PM_CASH_IN THEN PM_CASH_IN ELSE PM_MIN_CASH_IN END AS MIN_CASH_IN ");
          _sb.AppendLine("        , CASE WHEN PM_MIN_CASH_IN > PM_CASH_IN THEN PM_CASH_IN_REWARD ELSE PM_MIN_CASH_IN_REWARD END AS MIN_CASH_IN_REWARD ");
          _sb.AppendLine("        , ''  AS REWARD ");
          _sb.AppendLine("        , PM_WON_LOCK AS EXCLUDE_WON_LOCK       ");
          _sb.AppendLine("        , 0           AS SORT                   ");
          _sb.AppendLine("        , PM_TYPE                               ");
          _sb.AppendLine("        , CASE WHEN PM_SPENT_REWARD > PM_MIN_SPENT_REWARD THEN PM_SPENT_REWARD ELSE PM_MIN_SPENT_REWARD END AS SPENT_REWARD ");
          _sb.AppendLine("   FROM   PROMOTIONS                            ");

          // JPJ 16-DEC-2013 query is filtered with the applicable promotions
          if (_dt.Rows.Count > 0)
          {
            string[] _promo = new string[_dt.Rows.Count];
            for (Int32 _idx_promo = 0; _idx_promo < _dt.Rows.Count; _idx_promo++)
            {
              _promo[_idx_promo] = _dt.Rows[_idx_promo]["PM_PROMOTION_ID"].ToString();
            }
            _sb.AppendLine("  WHERE   PM_PROMOTION_ID IN ( " + string.Join(", ", _promo) + " )");
          }

          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            DataRow _dr_find;
            List<DataColumn> _ls_col_added;

            _ls_col_added = new List<DataColumn>();

            using (DataTable _dt_to_merge = new DataTable())
            {
              _dt_to_merge.Load(_cmd.ExecuteReader());
              foreach (DataColumn _col in _dt_to_merge.Columns)
              {
                if (!_col.ColumnName.StartsWith("EXCLUDE_"))
                {
                  if (!_dt.Columns.Contains(_col.ColumnName))
                  {
                    _dt.Columns.Add(_col.ColumnName, _col.DataType);
                    _ls_col_added.Add(_col);
                  }
                }
              }
              foreach (DataRow _dr_to_merge in _dt_to_merge.Rows)
              {
                _dr_find = _dt.Rows.Find(_dr_to_merge["PM_PROMOTION_ID"]);
                if (_dr_find != null)
                {
                  if ((Promotion.PROMOTION_TYPE)_dr_to_merge["PM_TYPE"] != Promotion.PROMOTION_TYPE.PREASSIGNED)
                  {
                    if (((Decimal)_dr_to_merge["MIN_CASH_IN_REWARD"] == 0 &&
                        (Decimal)_dr_to_merge["SPENT_REWARD"] == 0)
                      || (_dr_to_merge["MIN_CASH_IN"] != DBNull.Value &&
                        (Decimal)_dr_to_merge["MIN_CASH_IN"] > 0 &&
                        (Int64)_dr_to_merge["PM_PROMOTION_ID"] > 0))
                    {
                      _dt.Rows.Remove(_dr_find);

                      continue;
                    }
                  }

                  foreach (DataColumn _col in _ls_col_added)
                  {
                    switch (_col.ColumnName)
                    {
                      case "SORT":
                        // 1 NR, 2 CA(WONLOCK), 3 RE, 4 PT
                        if (!_dr_to_merge.IsNull("EXCLUDE_WON_LOCK"))
                        {
                          _dr_find[_col.ColumnName] = 2;
                        }
                        else
                        {
                          _dr_find[_col.ColumnName] = _dr_to_merge["PM_CREDIT_TYPE"];
                        }

                        break;
                      default:
                        _dr_find[_col.ColumnName] = _dr_to_merge[_col.ColumnName];
                        break;
                    }
                  }
                }

                ComputeReward(_dr_find, _db_trx.SqlTransaction);
              }
            }

            // ICS 13-DEC-2013: Remove "Sin Promoci�n" row because it has no sense here
            _dr_find = _dt.Rows.Find(0);
            if (_dr_find != null)
            {
              _dt.Rows.RemoveAt(0);
            }
          }

          // Fill grid with remaining rows
          _dt.DefaultView.Sort = "SORT,PM_NAME";

          _dt.Columns.Remove("MIN_CASH_IN");
          _dt.Columns.Remove("MIN_CASH_IN_REWARD");
          _dt.Columns.Remove("SPENT_REWARD");
          _dt.Columns.Remove("PM_TYPE");

          dgv_common.DataSource = _dt;

          if (_dt != null)
          {
            _dt.DefaultView.RowFilter = m_filter;
          }

          // Format common grid
          GuiStyleSheet();

          SelectCurrentPromoGrid();

          btn_create_ticket.Enabled = (dgv_common.RowCount > 0);
          gb_digits_box.Enabled = (dgv_common.RowCount > 0);

          setImages();

        } // try
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return;
      }

    } // PopulatePromotionsGrid


    private void setImages()
    {
      DataGridViewImageCell _cell;
      foreach (DataGridViewRow item in dgv_common.Rows)
      {
        _cell = (DataGridViewImageCell)item.Cells[GRID_COLUMN_PROMO_IMAGE];

        switch ((WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE)item.Cells[GRID_COLUMN_PROMO_CREDIT_TYPE].Value)
        {

          case ACCOUNT_PROMO_CREDIT_TYPE.NR1:
          case ACCOUNT_PROMO_CREDIT_TYPE.NR2:
            if (_cell.Selected)
            {
              _cell.Value = CashierStyle.Image.ImageToByteArray(Resources.ResourceImages.promoNRSelected);
            }
            else
            {
              _cell.Value = CashierStyle.Image.ImageToByteArray(Resources.ResourceImages.promoNR);
            }

            break;
          case ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE:

            if (_cell.Selected)
            {
              _cell.Value = CashierStyle.Image.ImageToByteArray(Resources.ResourceImages.promoReedemableSelected);
            }
            else
            {
              _cell.Value = CashierStyle.Image.ImageToByteArray(Resources.ResourceImages.promoReedemable);
            }

            break;
          case ACCOUNT_PROMO_CREDIT_TYPE.POINT:
            if (_cell.Selected)
            {
              _cell.Value = CashierStyle.Image.ImageToByteArray(Resources.ResourceImages.promoPointsSelected);
            }
            else
            {
              _cell.Value = CashierStyle.Image.ImageToByteArray(Resources.ResourceImages.promoPoints);
            }

            break;
          default:
            _cell.Value = CashierStyle.Image.ImageToByteArray((Image)new Bitmap(1, 1));

            break;
        }

        if ((Int32)item.Cells[GRID_COLUMN_PROMO_WON_LOCK].Value > 0)
        {
          if (_cell.Selected)
          {
            _cell.Value = CashierStyle.Image.ImageToByteArray(Resources.ResourceImages.promoLockSelected);
          }
          else
          {
            _cell.Value = CashierStyle.Image.ImageToByteArray(Resources.ResourceImages.promoLock);
          }

        }

      }
    }// setImages
    //------------------------------------------------------------------------------
    // PURPOSE : Format common data grid.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //    
    private void GuiStyleSheet()
    {
      if (dgv_common.ColumnCount == 0)
      {
        return;
      }
      dgv_common.Columns[GRID_COLUMN_PROMO_IMAGE].HeaderCell.Value = "";
      dgv_common.Columns[GRID_COLUMN_PROMO_IMAGE].Width = 50;
      dgv_common.Columns[GRID_COLUMN_PROMO_IMAGE].SortMode = DataGridViewColumnSortMode.NotSortable;
      dgv_common.Columns[GRID_COLUMN_PROMO_IMAGE].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

      dgv_common.Columns[GRID_COLUMN_PROMO_ID].Width = 0;
      dgv_common.Columns[GRID_COLUMN_PROMO_ID].Visible = false;
      dgv_common.Columns[GRID_COLUMN_PROMO_ID].SortMode = DataGridViewColumnSortMode.NotSortable;

      dgv_common.Columns[GRID_COLUMN_PROMO_NAME].HeaderCell.Value = Resource.String("STR_FRM_AMOUNT_INPUT_003");
      dgv_common.Columns[GRID_COLUMN_PROMO_NAME].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
      dgv_common.Columns[GRID_COLUMN_PROMO_NAME].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
      dgv_common.Columns[GRID_COLUMN_PROMO_NAME].SortMode = DataGridViewColumnSortMode.NotSortable;

      dgv_common.Columns[GRID_COLUMN_PROMO_WON_LOCK].Width = 0;
      dgv_common.Columns[GRID_COLUMN_PROMO_WON_LOCK].Visible = false;
      dgv_common.Columns[GRID_COLUMN_PROMO_WON_LOCK].SortMode = DataGridViewColumnSortMode.NotSortable;

      dgv_common.Columns[GRID_COLUMN_PROMO_CREDIT_TYPE].Width = 0;
      dgv_common.Columns[GRID_COLUMN_PROMO_CREDIT_TYPE].Visible = false;
      dgv_common.Columns[GRID_COLUMN_PROMO_CREDIT_TYPE].SortMode = DataGridViewColumnSortMode.NotSortable;

      dgv_common.Columns[GRID_COLUMN_PROMO_REWARD_TEXT].HeaderCell.Value = Resource.String("STR_FRM_AMOUNT_INPUT_006");
      dgv_common.Columns[GRID_COLUMN_PROMO_REWARD_TEXT].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
      dgv_common.Columns[GRID_COLUMN_PROMO_REWARD_TEXT].SortMode = DataGridViewColumnSortMode.NotSortable;

      if (m_promos_expanded_view)
      {
        dgv_common.Columns[GRID_COLUMN_PROMO_NAME].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
        dgv_common.Columns[GRID_COLUMN_PROMO_NAME].Width = (Int32)(0.6 * dgv_common.Width);
        dgv_common.Columns[GRID_COLUMN_PROMO_REWARD_TEXT].Visible = true;
        dgv_common.Columns[GRID_COLUMN_PROMO_REWARD_TEXT].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
      }
      else
      {
        dgv_common.Columns[GRID_COLUMN_PROMO_REWARD_TEXT].Width = 0;
        dgv_common.Columns[GRID_COLUMN_PROMO_REWARD_TEXT].Visible = false;
        dgv_common.Columns[GRID_COLUMN_PROMO_NAME].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
      }

      dgv_common.Columns[GRID_COLUMN_PROMO_SORT].Width = 0;
      dgv_common.Columns[GRID_COLUMN_PROMO_SORT].Visible = false;
      dgv_common.Columns[GRID_COLUMN_PROMO_SORT].SortMode = DataGridViewColumnSortMode.NotSortable;

      dgv_common.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
      dgv_common.MultiSelect = false;
    } // GuiStyleSheet

    //------------------------------------------------------------------------------
    // PURPOSE: Check for input card in order to notify user to create card.
    ///         Previously called LoadCardByTrackNumber
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    private Boolean LoadCardByAccountId(Int64 AccountId)
    {
      Boolean _error;

      _error = false;

      if (AccountId < 1)
      {
        //load virtual account
        m_current_card = new CardData();
        if (!BusinessLogic.LoadVirtualAccount(m_current_card))
        {
          Log.Error("LoadCardByAccountId. LoadVirtualAccount: Error reading card.");

          _error = true;
        }
      }
      else
      {
        //load player account
        if (m_current_card == null || m_current_card.AccountId != AccountId)
        {
          m_current_card = new CardData();
          if (!CardData.DB_CardGetAllData(AccountId, m_current_card))
          {
            BusinessLogic.LoadVirtualAccount(m_current_card);

            Log.Error("LoadCardByAccountId. DB_GetCardAllData: Error reading card.");

            _error = true;
          }
        }
      }

      RefreshData(m_current_card);

      return !_error;
    } // LoadCardByAccountId

    //------------------------------------------------------------------------------
    // PURPOSE: Set status of ALL buttons in control
    // 
    //  PARAMS:
    //      - INPUT:
    //        - Ticket: reference to a ticket instance
    //
    //      - OUTPUT:
    //
    private void SetButtonStatus()
    {
      btn_create_ticket.Enabled = false;
      btn_assign_to_account.Enabled = false;

      if (m_selected_promo_data != null)
      {
        Boolean _is_tito_cashier_open;
        _is_tito_cashier_open = BusinessLogic.IsTITOCashierOpen;

        btn_create_ticket.Enabled = true;
        btn_assign_to_account.Enabled = true;

        switch (m_selected_promo_data.credit_type)
        {
          case ACCOUNT_PROMO_CREDIT_TYPE.POINT:
            btn_create_ticket.Enabled = false;
            break;

          default:
            btn_create_ticket.Enabled = _is_tito_cashier_open && (dgv_common.RowCount > 0) && (dgv_common.SelectedRows.Count > 0);
            gb_digits_box.Enabled = _is_tito_cashier_open && (dgv_common.RowCount > 0) && (dgv_common.SelectedRows.Count > 0);
            break;
        }
      }
      // WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE.NR2, WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE.NR1   =   NO REDIMIBLE
      // WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE.POINT                                           =   PUNTOS
      // WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE    

    } // SetButtonStatus

    //------------------------------------------------------------------------------
    // PURPOSE: Refresh account data controls from CardData
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    private void RefreshData(CardData CardData)
    {
      CardData _card_data;

      _card_data = CardData;

      if (CardData == null)
      {
        BusinessLogic.LoadVirtualAccount(m_current_card);
      }
      else
      {
        m_current_card = CardData;
      }

      PopulatePromotionsGrid();

      SetButtonStatus();

    } // RefreshData

    #endregion

    #region Publics

    //------------------------------------------------------------------------------
    // PURPOSE: Initialize control resources. (NLS string, images, etc.).
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    public void InitializeControlResources()
    {
      lbl_title.Text = Resource.String("STR_FRM_TITO_PROMO_001");

      if (m_virtual_mode)
      {
        btn_create_ticket.Text = Resource.String("STR_FRM_TITO_PROMO_009");
        lbl_promo_reward_text.Text = Resource.String("STR_FRM_TITO_PROMO_006");
      }
      else
      {
        btn_create_ticket.Text = Resource.String("STR_FRM_TITO_PROMO_002");
        lbl_promo_reward_text.Text = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_001");
      }
      btn_assign_to_account.Text = Resource.String("STR_FRM_TITO_PROMO_013");
      gb_digits_box.Text = Resource.String("STR_FRM_TITO_PROMO_005");
      btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CLOSE");

    } // InitializeControlResources

    //------------------------------------------------------------------------------
    // PURPOSE: Init controls and events
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    public void InitControls(frm_container Parent, Int64 AccountId)
    {
      m_parent = Parent;
      VirtualAccountMode = false;
      LoadCardByAccountId(AccountId);
    } // InitControls
    public void InitControls(frm_container Parent)
    {
      m_parent = Parent;
      VirtualAccountMode = true;
      LoadCardByAccountId(0);
    } // InitControls

    private void RefreshControls()
    {
      m_form_embedded = !m_virtual_mode;
      m_selected_promo_id = 0;

      txt_amount.Text = "1";
      btn_promos_change_view.Visible = false;
      promo_filter.Width = 260;

      gb_digits_box.Visible = m_virtual_mode;

      pnl_title.Visible = !m_form_embedded;
      btn_cancel.Visible = m_form_embedded;

      // LTC 22-FEB-2016
      btn_assign_to_account.Visible = m_form_embedded;

      promo_filter.VirtualMode = m_virtual_mode;

      InitializeControlResources();
      LoadControlLocations();
      ShowPromosExpandedView();

      if (m_form_embedded)
      {
        btn_cancel.Location = new Point(btn_cancel.Location.X + 140, btn_cancel.Location.Y);
        btn_create_ticket.Location = new Point(btn_create_ticket.Location.X + 140, btn_create_ticket.Location.Y);
        btn_assign_to_account.Location = new Point(btn_assign_to_account.Location.X + 140, btn_assign_to_account.Location.Y);
        this.Height = pnl_main.Height + pnl_bottom.Height + (pnl_title.Visible ? pnl_title.Height : 0);
      }
      else
      {
        pb_promo_icon.Image = WSI.Cashier.Images.Get32x32(Resources.ResourceImages.promo_header);

        // LTC 22-FEB-2216
        btn_cancel.Location = new Point(btn_cancel.Location.X + 140, btn_cancel.Location.Y);
        btn_assign_to_account.Location = new Point(btn_assign_to_account.Location.X + 140, btn_assign_to_account.Location.Y);
        btn_create_ticket.Location = new Point(btn_create_ticket.Location.X + 163, btn_create_ticket.Location.Y);
        this.Height = pnl_main.Height + pnl_bottom.Height + (pnl_title.Visible ? pnl_title.Height : 0);
      }
    }

    public Boolean VirtualAccountMode
    {
      get
      {
        return m_virtual_mode;
      }
      set
      {
        m_virtual_mode = value;
        RefreshControls();
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Set initial focus
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    public void InitFocus()
    {
      this.dgv_common.Focus();
    } // InitFocus

    #endregion

    #region Buttons Events

    private void SelectCurrentPromoGrid()
    {
      DataGridViewRow _dgv_to_select;

      _dgv_to_select = null;

      foreach (DataGridViewRow _dgv_row in dgv_common.Rows)
      {
        if (_dgv_row.Cells[GRID_COLUMN_PROMO_ID].Value.Equals(m_selected_promo_id))
        {
          _dgv_to_select = _dgv_row;
          break;
        }
      }

      if (_dgv_to_select == null && dgv_common.Rows.Count > 0)
      {
        _dgv_to_select = dgv_common.Rows[0];
      }

      if (_dgv_to_select != null)
      {
        _dgv_to_select.Selected = true;
        if (!_dgv_to_select.Displayed)
        {
          dgv_common.FirstDisplayedScrollingRowIndex = _dgv_to_select.Index;
        }

        try
        {
          m_contracting = true;
          dgv_common_CellClick(dgv_common, new DataGridViewCellEventArgs(0, _dgv_to_select.Index));
        }
        catch
        { }
        finally
        {
          m_contracting = false;
        }
      }

      // Clean & reset if necessary 
      if (dgv_common.RowCount == 0)
      {
        txt_amount.Text = "1";
        UpdatePromoRewardArea(0, 0);
      }

      SetButtonStatus();

    } // SelectCurrentPromoGrid

    private Boolean CreateTicketAndAssignPromotion(Int32 Quantity, Currency UnitaryReward, ENUM_OPERATION_MODE_PROMOTIONS_NONREDEM OperationModePromotionsNonRedem)
    {
      CardData _card_data;
      ParamsTicketOperation _operation_params;
      TicketCreationParams _promo_params;
      String _error_str;
      List<ProfilePermissions.CashierFormFuncionality> _list_permissions;
      ENUM_TITO_OPERATIONS_RESULT _result;
      PrinterStatus _status;
      String _status_text;
      //Int32 _tickets_printed;

      _card_data = m_current_card;
      _list_permissions = new List<ProfilePermissions.CashierFormFuncionality>();
      _result = ENUM_TITO_OPERATIONS_RESULT.NONE;
      //_tickets_printed = 0;

      _operation_params = new ParamsTicketOperation();
      _operation_params.in_account = _card_data;
      _operation_params.in_window_parent = m_parent;
      _operation_params.session_info = Cashier.CashierSessionInfo();
      _operation_params.check_redeem = false;
      _operation_params.out_promotion_id = m_selected_promo_id;
      _operation_params.out_operation_id = 0;

      try
      {
        switch (m_selected_promo_data.special_permission)
        {
          case Promotion.PROMOTION_SPECIAL_PERMISSION.PROMOTION_SPECIAL_PERMISSION_NOT_SET:
            break;

          case Promotion.PROMOTION_SPECIAL_PERMISSION.PROMOTION_SPECIAL_PERMISSION_A:
            _list_permissions.Add(ProfilePermissions.CashierFormFuncionality.PromoSpecialPermissionA);
            break;

          case Promotion.PROMOTION_SPECIAL_PERMISSION.PROMOTION_SPECIAL_PERMISSION_B:
            _list_permissions.Add(ProfilePermissions.CashierFormFuncionality.PromoSpecialPermissionB);
            break;

          default:
            frm_message.Show(Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_MSG_007"),
                             Resource.String("STR_APP_GEN_MSG_ERROR"),
                             MessageBoxButtons.OK,
                             MessageBoxIcon.Error, m_parent);
            return false;
        }

        if (_list_permissions.Count > 0)
        {
          if (!ProfilePermissions.CheckPermissionList(_list_permissions,
                                                      ProfilePermissions.TypeOperation.RequestPasswd,
                                                      m_parent,
                                                      0,
                                                      out _error_str))
          {
            return false;
          }
        }

        switch (m_selected_promo_data.credit_type)
        {
          case ACCOUNT_PROMO_CREDIT_TYPE.NR1:
            _operation_params.ticket_type = TITO_TICKET_TYPE.PROMO_NONREDEEM;
            break;
          case ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE:
            _operation_params.ticket_type = TITO_TICKET_TYPE.PROMO_REDEEM;
            break;
          default:
            break;
        }


        _promo_params = new TicketCreationParams();
        _promo_params.OutCashAmount = Currency.Zero();
        _promo_params.OutPromotionId = m_selected_promo_id;
        _promo_params.OutPromotionType = m_selected_promo_data.credit_type;
        _promo_params.PromoRewardAmount = m_promo_reward_amount;
        _promo_params.PromoWonLock = m_promo_won_lock;
        _promo_params.QuantityOfPromotions = Quantity;
        _promo_params.UnitaryReward = UnitaryReward;
        _promo_params.PromoRewardValue = lbl_promo_reward_value.Text;
        _promo_params.PromoName = m_selected_promo_name;
        _promo_params.OutSpentUsed = m_spent_used;

        // If not virtual mode, tickets are printed in asynchronous mode without showing progress form
        if (!m_virtual_mode || OperationModePromotionsNonRedem == ENUM_OPERATION_MODE_PROMOTIONS_NONREDEM.ONLY_ASSIGN_PROMOTION)
        {
          _result = BusinessLogic.CreatePromotionsTicketThread(ref _operation_params, _promo_params, OperationModePromotionsNonRedem);
        }
        // Virtual mode, tickets are printed in real-time mode showing progress form
        else
        {
          frm_ticket_print_progress _ticket_print_progress = new frm_ticket_print_progress();
          _ticket_print_progress.Show(_operation_params, _promo_params, m_selected_promo_name, m_parent);
          _ticket_print_progress.Dispose();
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        _result = ENUM_TITO_OPERATIONS_RESULT.EXCEPTION;

        return false;
      }
      finally
      {
        if (!m_virtual_mode)
        {
          String _msg_text;
          MessageBoxIcon _msg_type;
          String _msg_caption;

          _msg_text = String.Empty;
          _msg_type = MessageBoxIcon.Information;
          _msg_caption = String.Empty;

          switch (_result)
          {
            case ENUM_TITO_OPERATIONS_RESULT.PRINTER_PRINT_FAIL:
            case ENUM_TITO_OPERATIONS_RESULT.PRINTER_IS_NOT_READY:
              _msg_type = MessageBoxIcon.Error;
              _msg_caption = Resource.String("STR_APP_GEN_MSG_ERROR");
              _msg_text += Resource.String("STR_FRM_TITO_PROMO_ERROR_PRINTER_FAILS");
              TitoPrinter.GetStatus(out _status, out _status_text);
              _msg_text += "\r\n    - " + TitoPrinter.GetMsgError(_status);
              Log.Error(String.Format("CreateTicketAndAssignPromotion. Result:[{0}]", _result.ToString()));
              break;
            case ENUM_TITO_OPERATIONS_RESULT.OK:
              _msg_type = MessageBoxIcon.Information;
              _msg_caption = this.lbl_title.Text;

              if (OperationModePromotionsNonRedem == ENUM_OPERATION_MODE_PROMOTIONS_NONREDEM.CREATE_TICKET)
              {
                _msg_text = Resource.String("STR_FRM_TITO_PROMO_004", new object[] { lbl_promo_reward_value.Text, m_selected_promo_name });
              }
              else if (OperationModePromotionsNonRedem == ENUM_OPERATION_MODE_PROMOTIONS_NONREDEM.ONLY_ASSIGN_PROMOTION)
              {
                _msg_text = Resource.String("STR_FRM_TITO_PROMO_014", new object[] { lbl_promo_reward_value.Text, m_selected_promo_name });
              }

              if (!m_virtual_mode)
              {
                _msg_text += "\r\n\r\n " + Resource.String("STR_FORMAT_GENERAL_NAME_ACCOUNT") + ": " + m_current_card.AccountId;
                _msg_text += "\r\n " + Resource.String("STR_FORMAT_GENERAL_NAME_CARD") + ": " + m_current_card.VisibleTrackdata();
                _msg_text += "\r\n " + Resource.String("STR_FRM_ACCOUNT_USER_EDIT_NAME") + ": " + m_current_card.PlayerTracking.HolderName;
              }
              break;

            case ENUM_TITO_OPERATIONS_RESULT.PRINTER_PRINT_STOPED:
              _msg_type = MessageBoxIcon.Warning;
              _msg_caption = Resource.String("STR_APP_GEN_MSG_WARNING");
              _msg_text += Resource.String("STR_FRM_TITO_PROMO_CANCELED_BY_USER");
              break;

            case ENUM_TITO_OPERATIONS_RESULT.AUX_OPERATION_CANCELED:
              _msg_type = MessageBoxIcon.Error;
              _msg_caption = Resource.String("STR_APP_GEN_MSG_ERROR");
              _msg_text += Resource.String("STR_FRM_TITO_PROMO_ERROR_OPERATION");
              break;
            case ENUM_TITO_OPERATIONS_RESULT.NONE:
              _msg_type = MessageBoxIcon.None;
              break;

            default:
              _msg_type = MessageBoxIcon.Error;
              _msg_caption = Resource.String("STR_APP_GEN_MSG_ERROR");
              _msg_text += Resource.String("STR_FRM_TITO_PROMO_ERROR_OPERATION");
              break;
          }

          if (_msg_type != MessageBoxIcon.None)
          {
            frm_message.Show(_msg_text, _msg_caption, MessageBoxButtons.OK, _msg_type, this.ParentForm);
          }
        }

        form_yes_no.Hide();
        this.RestoreParentFocus();
      }
    } // AssignPromotion

    //------------------------------------------------------------------------------
    // PURPOSE : Validate that the currently selected promotion can still be applied 
    //           to the active player account.
    //
    //  PARAMS :
    //      - INPUT :
    //          - InputAmount: amount to add to the account
    //          - ValidateAmount: flag to signal whether amount must be checked 
    //                            against promotion conditions or not
    //
    //      - OUTPUT :
    //          - Reward: amount of non-redeemable credits awarded when applying the 
    //                    selected promotion
    //
    // RETURNS :
    //
    //   NOTES :
    //    
    private Boolean ValidatePromotion(Currency InputAmount,
                                      Int32 NumTokens,
                                      Boolean ValidateAmount,
                                      Boolean ShowMessage,
                                      out Currency Reward,
                                      out Currency WonLock,
                                      out Currency UnitaryReward,
                                      ref Int32 QuantityForMinCashIn)
    {

      Promotion.PROMOTION_STATUS _promo_status;
      Currency _reward = 0;
      Currency _won_lock = 0;
      Currency _promo_account_limit = 0;
      Currency _cash_in;
      Int32 _num_tokens;
      String _msg = "";
      String _title = "";
      MessageBoxIcon _icon;
      Boolean _rc;
      DataTable _account_flags;
      DateTime _now;

      _rc = true;
      Reward = 0;
      WonLock = 0;
      UnitaryReward = 0;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _cash_in = InputAmount;
          _num_tokens = NumTokens;
          m_selected_promo_data = new Promotion.TYPE_PROMOTION_DATA();
          _rc = Promotion.ReadPromotionData(_db_trx.SqlTransaction, m_selected_promo_id, m_selected_promo_data);
          if (!_rc)
          {
            // Log error
            Log.Error("ValidatePromotion. Promotion not found: " + m_selected_promo_id + ".");

            _promo_status = Promotion.PROMOTION_STATUS.PROMOTION_NOT_FOUND;
          }
          else
          {
            UnitaryReward = m_selected_promo_data.min_cash_in_reward;

            _account_flags = null;

            if (!m_virtual_mode)
            {
              if (!Promotion.GetActiveFlags(m_current_card.AccountId, _db_trx.SqlTransaction, out  _account_flags))
              {

                return false;
              }

              // AMF 05-SEP-2013: Calculate spent per terminal
              m_dt_remaining_spent_per_terminal = Promotion.AccountRemainingDataPerTerminal(m_current_card.AccountId, _db_trx.SqlTransaction);

              Promotion.CaculateDataByTerminal(m_dt_remaining_spent_per_terminal,
                                                 m_selected_promo_data.terminal_list,
                                                 m_selected_promo_id,
                                                 out m_input_spent, out m_input_played,
                                                 _db_trx.SqlTransaction);


            }

            _now = WGDB.Now;
            _promo_status = Promotion.CheckPromotionApplicable(_db_trx.SqlTransaction,
                                                               m_selected_promo_id,
                                                               m_current_card.AccountId,
                                                               ValidateAmount,
                                                               _account_flags,
                                                               ref _cash_in,
                                                               m_input_spent,
                                                               m_input_played,
                                                               ref _num_tokens,
                                                               ref m_selected_promo_data,
                                                               out _reward,
                                                               out _won_lock,
                                                               out m_spent_used,
                                                               out m_available_limit,
                                                               _now,
                                                               _now,
                                                               ref QuantityForMinCashIn);
          }

          if (m_selected_promo_data.type == Promotion.PROMOTION_TYPE.PREASSIGNED ||
              m_selected_promo_data.min_spent_reward > 0 ||
              m_selected_promo_data.spent_reward > 0)
          {
            UnitaryReward = _reward;
          }

          // Update promo msg area 
          UpdatePromoRewardArea(UnitaryReward, _reward);

          Reward = _rc ? _reward : 0;
          WonLock = _rc ? _won_lock : 0;

          // Parse return code and display appropriate messages
          _icon = MessageBoxIcon.None;
          switch (_promo_status)
          {
            case Promotion.PROMOTION_STATUS.PROMOTION_OK:
              if (ValidateAmount && _reward <= 0)
              {
                // Check that a promotion awards something
                switch (m_selected_promo_data.credit_type)
                {
                  case ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE:
                    _msg = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_MSG_009_REDEEMABLE");
                    break;
                  case ACCOUNT_PROMO_CREDIT_TYPE.POINT:
                    _msg = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_MSG_009_POINT");
                    break;
                  case ACCOUNT_PROMO_CREDIT_TYPE.NR1:
                  case ACCOUNT_PROMO_CREDIT_TYPE.NR2:
                  case ACCOUNT_PROMO_CREDIT_TYPE.UNKNOWN:
                  default:
                    _msg = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_MSG_009_NOT_REDEEMABLE");
                    break;
                }
                _title = Resource.String("STR_APP_GEN_MSG_ERROR");
                _icon = MessageBoxIcon.Error;
                _rc = false;
              }
              break;

            case Promotion.PROMOTION_STATUS.PROMOTION_REDUCED_REWARD:
              if (ValidateAmount && _reward <= 0)
              {
                // Check that a promotion awards something
                switch (m_selected_promo_data.credit_type)
                {
                  case ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE:
                    _msg = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_MSG_009_REDEEMABLE");
                    break;
                  case ACCOUNT_PROMO_CREDIT_TYPE.POINT:
                    _msg = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_MSG_009_POINT");
                    break;
                  case ACCOUNT_PROMO_CREDIT_TYPE.NR1:
                  case ACCOUNT_PROMO_CREDIT_TYPE.NR2:
                  case ACCOUNT_PROMO_CREDIT_TYPE.UNKNOWN:
                  default:
                    _msg = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_MSG_009_NOT_REDEEMABLE");
                    break;
                }
                _title = Resource.String("STR_APP_GEN_MSG_ERROR");
                _icon = MessageBoxIcon.Error;
                _rc = false;
              }
              else
              {
                // Avoid showing every warning more than once
                if (!m_promo_warning_shown)
                {
                  switch (m_selected_promo_data.credit_type)
                  {
                    case ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE:
                      _msg = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_MSG_010_REDEEMABLE", _reward);
                      break;
                    case ACCOUNT_PROMO_CREDIT_TYPE.POINT:
                      _msg = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_MSG_010_POINT", (Points)((Decimal)_reward));
                      break;
                    case ACCOUNT_PROMO_CREDIT_TYPE.NR1:
                    case ACCOUNT_PROMO_CREDIT_TYPE.NR2:
                    case ACCOUNT_PROMO_CREDIT_TYPE.UNKNOWN:
                    default:
                      _msg = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_MSG_010_NOT_REDEEMABLE", _reward);
                      break;
                  }
                  _title = Resource.String("STR_APP_GEN_MSG_WARNING");
                  _icon = MessageBoxIcon.Warning;
                  m_promo_warning_shown = true;
                }
              }
              break;

            //case Promotion.PROMOTION_STATUS.PROMOTION_OK_MODIFIED_AMOUNTS:
            //  _msg = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_MSG_010", _reward);
            //  _title = Resource.String("STR_APP_GEN_MSG_WARNING");
            //  _icon = MessageBoxIcon.Warning;
            //  break;

            case Promotion.PROMOTION_STATUS.PROMOTION_GENERIC_ERROR:
            case Promotion.PROMOTION_STATUS.PROMOTION_NOT_FOUND:
            case Promotion.PROMOTION_STATUS.PROMOTION_ACCT_NOT_FOUND:
              _msg = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_MSG_001");
              _title = Resource.String("STR_APP_GEN_MSG_ERROR");
              _icon = MessageBoxIcon.Error;
              _rc = false;
              break;

            case Promotion.PROMOTION_STATUS.PROMOTION_BELOW_MINIMUM:
              if (ValidateAmount && _reward <= 0)
              {
                // Check that a promotion awards something
                switch (m_selected_promo_data.credit_type)
                {
                  case ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE:
                    _msg = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_MSG_009_REDEEMABLE");
                    break;
                  case ACCOUNT_PROMO_CREDIT_TYPE.POINT:
                    _msg = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_MSG_009_POINT");
                    break;
                  case ACCOUNT_PROMO_CREDIT_TYPE.NR1:
                  case ACCOUNT_PROMO_CREDIT_TYPE.NR2:
                  case ACCOUNT_PROMO_CREDIT_TYPE.UNKNOWN:
                  default:
                    _msg = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_MSG_009_NOT_REDEEMABLE");
                    break;
                }
                _title = Resource.String("STR_APP_GEN_MSG_ERROR");
                _icon = MessageBoxIcon.Error;
                _rc = false;
              }
              else
              {
                // Avoid showing every warning more than once
                if (!m_promo_warning_shown)
                {
                  _msg = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_MSG_002", _cash_in);
                  _title = Resource.String("STR_APP_GEN_MSG_WARNING");
                  _icon = MessageBoxIcon.Warning;
                  m_promo_warning_shown = true;
                }
              }
              break;

            case Promotion.PROMOTION_STATUS.PROMOTION_DISABLED:
            case Promotion.PROMOTION_STATUS.PROMOTION_OUT_OF_DATE:
            case Promotion.PROMOTION_STATUS.PROMOTION_OUT_OF_WEEKDAY:
            case Promotion.PROMOTION_STATUS.PROMOTION_OUT_OF_TIME:
              _msg = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_MSG_003");
              _title = Resource.String("STR_APP_GEN_MSG_ERROR");
              _icon = MessageBoxIcon.Error;
              _rc = false;
              break;

            case Promotion.PROMOTION_STATUS.PROMOTION_ACCT_ANONYMOUS:
              _msg = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_MSG_004");
              _title = Resource.String("STR_APP_GEN_MSG_ERROR");
              _icon = MessageBoxIcon.Error;
              _rc = false;
              break;

            case Promotion.PROMOTION_STATUS.PROMOTION_ACCT_BLOCKED:
              _msg = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_MSG_005");
              _title = Resource.String("STR_APP_GEN_MSG_ERROR");
              _icon = MessageBoxIcon.Error;
              _rc = false;
              break;

            case Promotion.PROMOTION_STATUS.PROMOTION_ACCT_NO_REDEEMABLE:
              _msg = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_MSG_006");
              _title = Resource.String("STR_APP_GEN_MSG_ERROR");
              _icon = MessageBoxIcon.Error;
              _rc = false;
              break;

            case Promotion.PROMOTION_STATUS.PROMOTION_ACCT_HAS_PRIZE:
              _msg = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_MSG_011");
              _title = Resource.String("STR_APP_GEN_MSG_ERROR");
              _icon = MessageBoxIcon.Error;
              _rc = false;
              break;

            case Promotion.PROMOTION_STATUS.PROMOTION_GENDER_FILTER:
            case Promotion.PROMOTION_STATUS.PROMOTION_BIRTHDAY_FILTER:
            case Promotion.PROMOTION_STATUS.PROMOTION_LEVEL_FILTER:
            case Promotion.PROMOTION_STATUS.PROMOTION_FREQUENCY_FILTER:
              _msg = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_MSG_007");
              _title = Resource.String("STR_APP_GEN_MSG_ERROR");
              _icon = MessageBoxIcon.Error;
              _rc = false;
              break;

            case Promotion.PROMOTION_STATUS.PROMOTION_LIMIT_EXHAUSTED:
              _msg = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_MSG_008");
              _title = Resource.String("STR_APP_GEN_MSG_ERROR");
              _icon = MessageBoxIcon.Error;
              _rc = false;
              break;

            case Promotion.PROMOTION_STATUS.PROMOTION_BELOW_MINIMUM_SPENT:
              _msg = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_MSG_012");
              _title = Resource.String("STR_APP_GEN_MSG_ERROR");
              _icon = MessageBoxIcon.Error;
              _rc = false;
              break;

            case Promotion.PROMOTION_STATUS.PROMOTION_BELOW_SPENT:
              _msg = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_MSG_013");
              _title = Resource.String("STR_APP_GEN_MSG_ERROR");
              _icon = MessageBoxIcon.Error;
              _rc = false;
              break;
          }

          // Display warning/error message 
          if (_msg != "" && ShowMessage)
          {
            frm_message.Show(_msg, _title, MessageBoxButtons.OK, _icon, this.ParentForm);
          }
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      return _rc;
    } // ValidatePromotion


    //------------------------------------------------------------------------------
    // PURPOSE : Handle click event in the common grid. This event is used to 
    //           select the promotion to apply to current card add credit operation.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //    
    private void dgv_common_CellClick(object sender, DataGridViewCellEventArgs e)
    {
      DataGridViewRow _view_row;
      DataRow _row;
      Boolean _validated = true;
      Int32 _quantity_for_min_cash_in;
      Currency _unitary_reward;

      _quantity_for_min_cash_in = 1;
      if (m_virtual_mode)
      {
        if (!Int32.TryParse(txt_amount.Text, out _quantity_for_min_cash_in))
        {
          return;
        }
      }

      // Ignore clicks on non-data row areas; headers, for example
      if (e.RowIndex >= 0)
      {

        if (m_selected_promo_index != e.RowIndex || m_contracting)
        {
          _view_row = dgv_common.Rows[e.RowIndex];
          _row = ((DataRowView)_view_row.DataBoundItem).Row;

          m_selected_promo_index = e.RowIndex;
          m_selected_promo_id = (Int64)_row[GRID_COLUMN_PROMO_ID];
          m_selected_promo_name = (String)_row[GRID_COLUMN_PROMO_NAME];
          m_promo_warning_shown = false;
          m_available_limit = 0;

          // Clear msg areas
          ResetPromoRewardArea();

          if (m_selected_promo_id > 0)
          {
            // Do not check amounts, as none is present
            _validated = ValidatePromotion(0, 0, true, false, out m_promo_reward_amount, out m_promo_won_lock, out _unitary_reward, ref _quantity_for_min_cash_in);
          }
          else
          {
            // No promotion selected: Reset promotion data.
            m_selected_promo_data = new Promotion.TYPE_PROMOTION_DATA();

            m_selected_promo_index = 0;
            m_selected_promo_id = 0;
            m_promo_reward_amount = 0;
            m_promo_won_lock = 0;
            m_spent_used = 0;
            m_promo_warning_shown = false;

            m_available_limit = 0;
          }
        }

        SetButtonStatus();

        setImages();
      }//if
    } // dgv_common_CellClick


    //------------------------------------------------------------------------------
    // PURPOSE : Handles the filter change event.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //    
    void promo_filter_FilterChange(object sender)
    {
      m_filter = ((uc_promo_filter)sender).m_filter;
      ApplyFilter();
    } // promo_filter_FilterChange

    //------------------------------------------------------------------------------
    // PURPOSE : Applies filter to the grid.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //    
    private void ApplyFilter()
    {
      DataTable _dt;

      _dt = (DataTable)dgv_common.DataSource;
      if (_dt != null)
      {
        _dt.DefaultView.RowFilter = m_filter;
        SelectCurrentPromoGrid();
      }
    } // ApplyFilter

    //------------------------------------------------------------------------------
    // PURPOSE : Sends the number associated with the button.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //    
    private void btn_num_1_Click(object sender, EventArgs e)
    {
      AddNumber("1");
    } // btn_num_1_Click

    private void btn_num_2_Click(object sender, EventArgs e)
    {
      AddNumber("2");
    } // btn_num_2_Click

    private void btn_num_3_Click(object sender, EventArgs e)
    {
      AddNumber("3");
    } // btn_num_3_Click

    private void btn_num_4_Click(object sender, EventArgs e)
    {
      AddNumber("4");
    } // btn_num_4_Click

    private void btn_num_5_Click(object sender, EventArgs e)
    {
      AddNumber("5");
    } // btn_num_5_Click

    private void btn_num_6_Click(object sender, EventArgs e)
    {
      AddNumber("6");
    } // btn_num_6_Click

    private void btn_num_7_Click(object sender, EventArgs e)
    {
      AddNumber("7");
    } // btn_num_7_Click

    private void btn_num_8_Click(object sender, EventArgs e)
    {
      AddNumber("8");
    } // btn_num_8_Click

    private void btn_num_9_Click(object sender, EventArgs e)
    {
      AddNumber("9");
    } // btn_num_9_Click

    private void btn_num_0_Click(object sender, EventArgs e)
    {
      AddNumber("0");
    } // btn_num_0_Click

    private void btn_promos_change_view_Click(object sender, EventArgs e)
    {
      if (m_promos_expanded_view)
      {
        ShowPromosContractedView();
      }
      else
      {
        ShowPromosExpandedView();
      }
    } // btn_promos_change_view_Click

    private void btn_create_ticket_Click(object sender, EventArgs e)
    {
      List<ProfilePermissions.CashierFormFuncionality> _list_permissions;
      _list_permissions = new List<ProfilePermissions.CashierFormFuncionality>();

      String _error_str;

      if (m_selected_promo_data.credit_type == ACCOUNT_PROMO_CREDIT_TYPE.NR1 || m_selected_promo_data.credit_type == ACCOUNT_PROMO_CREDIT_TYPE.NR2)
      {
        _list_permissions.Add(ProfilePermissions.CashierFormFuncionality.TITO_CreatePromoNRTicket);
      }
      else if (m_selected_promo_data.credit_type == ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE)
      {
        _list_permissions.Add(ProfilePermissions.CashierFormFuncionality.TITO_CreatePromoRETicket);
      }

      if (!ProfilePermissions.CheckPermissionList(_list_permissions,
                                                  ProfilePermissions.TypeOperation.RequestPasswd,
                                                  this.ParentForm,
                                                  0,
                                                  out _error_str))
      {
        return;
      }

      ValidatePromotionRow(ENUM_OPERATION_MODE_PROMOTIONS_NONREDEM.CREATE_TICKET);

    } // btn_assign_promo_Click

    private void btn_num_clear_Click(object sender, EventArgs e)
    {
      if (txt_amount.Text.Length > 1)
      {
        txt_amount.Text = txt_amount.Text.Substring(0, txt_amount.Text.Length - 1);
      }
      else
      {
        txt_amount.Text = "0";
      }
      SelectCurrentPromoGrid();
    } // btn_num_clear_Click

    #endregion

    #region Privates

    //------------------------------------------------------------------------------
    // PURPOSE : Add a number in the amount textbox.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //    
    private void AddNumber(string NumberStr)
    {
      String _tentative_number;
      Currency _input_amount;

      // Prevent exceeding maximum amount length
      if (txt_amount.Text.Length >= Cashier.MAX_ALLOWED_AMOUNT_LENGTH)
      {
        return;
      }

      _tentative_number = txt_amount.Text + NumberStr;

      try
      {
        _input_amount = Decimal.Parse(_tentative_number);
        if (_input_amount > Cashier.MAX_ALLOWED_AMOUNT)
        {
          return;
        }
      }
      catch
      {
        _input_amount = 0;

        return;
      }

      // Remove unneeded leading zeros
      // Unless we are entering a decimal value, there must not be any leading 0
      txt_amount.Text = txt_amount.Text.TrimStart('0') + NumberStr;

      SelectCurrentPromoGrid();
      btn_create_ticket.Focus();
    } // AddNumber

    //------------------------------------------------------------------------------
    // PURPOSE : Handles KeyPress event.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //    
    private void txt_amount_KeyPress(object sender, KeyPressEventArgs e)
    {
      String aux_str;
      char c;

      c = e.KeyChar;

      if (c >= '0' && c <= '9')
      {
        aux_str = "" + c;

        AddNumber(aux_str);

        e.Handled = true;
      }

      if (c == '\r')
      {
        btn_create_ticket_Click(null, null);

        e.Handled = true;
      }

      if (c == '\b')
      {
        btn_num_clear_Click(null, null);

        e.Handled = true;
      }

      e.Handled = false;
    } // txt_amount_KeyPress

    //------------------------------------------------------------------------------
    // PURPOSE : Validates promotion row
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //    
    private void ValidatePromotionRow(ENUM_OPERATION_MODE_PROMOTIONS_NONREDEM Operation)
    {
      Int32 _quantity_for_min_cash_in;
      Currency _unitary_reward;
      DataGridViewRow _view_row;
      DataRow _row;

      if (dgv_common.SelectedRows.Count < 1 || m_selected_promo_index < 0)
      {
        return;
      }

      // 09-JAN-2014 JPJ First time without interaction didn't generate the promotion.
      _view_row = dgv_common.SelectedRows[0];
      _row = ((DataRowView)_view_row.DataBoundItem).Row;

      m_selected_promo_id = (Int64)_row[GRID_COLUMN_PROMO_ID];
      m_selected_promo_name = (String)_row[GRID_COLUMN_PROMO_NAME];

      _quantity_for_min_cash_in = 1;

      if (m_virtual_mode)
      {
        _quantity_for_min_cash_in = 0;
        if (!Int32.TryParse(txt_amount.Text, out _quantity_for_min_cash_in) || _quantity_for_min_cash_in < 1)
        {
          return;
        }
      }

      if (!ValidatePromotion(0, 0, true, true, out m_promo_reward_amount, out m_promo_won_lock, out _unitary_reward, ref _quantity_for_min_cash_in))
      {
        return;
      }

      if (!CreateTicketAndAssignPromotion(_quantity_for_min_cash_in, _unitary_reward, Operation))
      {
        return;
      }

      // LTC 22-FEB-2016
      if (m_form_embedded)
      {
        RefreshData(m_current_card);
      }
      else
      {
        ClearCardData();
      }

    }// ValidatePromotionRow

    private void btn_cancel_Click(object sender, EventArgs e)
    {
      this.ParentForm.Close();
    }

    #endregion

    private void txt_amount_TextChanged(object sender, EventArgs e)
    {
      int _quantity_of_promo;
      if (Int32.TryParse(txt_amount.Text, out _quantity_of_promo))
      {
        if (_quantity_of_promo > m_max_promos)
        {
          txt_amount.Text = txt_amount.Text.Substring(0, txt_amount.Text.Length - 1);
        }
      }
    }

    private void btn_assign_to_account_Click(object sender, EventArgs e)
    {
      String _error_str;

      if (m_selected_promo_data.credit_type == ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE)
      {
        frm_message.Show(Resource.String("STR_FRM_TITO_PROMO_NON_ASSIGNABLE")
                        , Resource.String("STR_APP_GEN_MSG_ERROR")
                        , MessageBoxButtons.OK
                        , MessageBoxIcon.Error
                        , this.ParentForm);
        return;
      }


      List<ProfilePermissions.CashierFormFuncionality> _list_permissions;
      _list_permissions = new List<ProfilePermissions.CashierFormFuncionality>();

      // XGJ 16-DIC-2016
      if (m_selected_promo_data.credit_type != ACCOUNT_PROMO_CREDIT_TYPE.POINT)
      {
        _list_permissions.Add(ProfilePermissions.CashierFormFuncionality.TITO_AssignPromoNR);
      }

      if (!ProfilePermissions.CheckPermissionList(_list_permissions,
                                                  ProfilePermissions.TypeOperation.RequestPasswd,
                                                  this.ParentForm,
                                                  0,
                                                  out _error_str))
      {
        return;
      }

      ValidatePromotionRow(ENUM_OPERATION_MODE_PROMOTIONS_NONREDEM.ONLY_ASSIGN_PROMOTION);
    }

    private void uc_round_button1_Click(object sender, EventArgs e)
    {
      frm_amount_input _frm_input_amount;
      Boolean _result;
      TicketCreationParams Params;
      RechargeOutputParameters OutputParameter;
      CashierSessionInfo _cashier_session_info;

      _frm_input_amount = new frm_amount_input(((frm_container)m_parent).uc_ticket_create1.CheckPermissionsToRedeem, null);
      _result = _frm_input_amount.ShowForCashin(out Params, m_current_card, m_parent);
      _frm_input_amount.Dispose();
      _cashier_session_info = Cashier.CashierSessionInfo();

      if (!_result)
      {

      }

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (!Accounts.DB_CardCreditAdd(m_current_card,
                                                     Params.OutCashAmount,
                                                     Params.OutPromotionId,
                                                     Params.OutNotRedeemableAmount,
                                                     Params.OutNotRedeemableWonLock,
                                                     Params.OutSpentUsed,
                                                     Params.OutExchangeResult,
                                                     Params.OutParticipateInCashdeskDraw,
                                                     OperationCode.CASH_IN,
                                                     _cashier_session_info,
                                                     _db_trx.SqlTransaction,
                                                     out OutputParameter))
          {
            return;
          }

          _db_trx.Commit();
        }
      }
      catch (Exception _ex)
      {
        Log.Error("Error: DB_CardCreditAdd() " + _ex);

        return;
      }

      return;
    }
  }
}

