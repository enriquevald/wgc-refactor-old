//------------------------------------------------------------------------------
// Copyright � 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_ticket_terminal.cs
// 
//   DESCRIPTION: Implements the following classes:
//                1. uc_ticket_terminal
//
//        AUTHOR: Maximiliano Villarreal
// 
// CREATION DATE: 01-SEP-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 01-SEP-2015 GMV     First release.
// 19-NOV-2015 ETP     Bug 6821: No se muestra la fecha en la columna Hora
// 23-NOV-2015 ETP     PBI 6902: Some bugs corrected 
// 15-NOV-2016 JML     Bug 11867: Last TITO bills and tickets entered: those entered in foreign currency are converted into national currency
// 12-JAN-2017 FAV     Bug 22679: The Type column shows 'Bill' for coins.
//-------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using WSI.Common;
using WSI.Cashier.TITO;
using WSI.Common.TITO;
using System.Data.SqlClient;
using System.Collections;
using System.Globalization;

namespace WSI.Cashier.TITO
{
  public partial class uc_ticket_terminal : UserControl
  {
    #region Enums

    public enum WCP_TerminalType
    {
      IntermediateServer = 0,
      GamingTerminal = 1
    }

    #endregion

    #region Members

    private frm_yesno m_frm_shadow_gui;
    private Boolean m_first_time = true;
    private static String m_decimal_str = System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencyDecimalSeparator;


    #endregion

    #region Constants

    private const Int32 MAX_HOURS_IS_OFFLINE = 24;

    // Number of columns
    public Int32 GRID_COLUMN_MAX_COLUMNS = 5; //Must be updated if change number of columns

    // Columns defined
    public Int32 GRID_COLUMN_CREATION_DATE = 0;
    public Int32 GRID_COLUMN_TICKET_TYPE = 1;
    public Int32 GRID_COLUMN_CREATED_AMOUNT = 2;
    public Int32 GRID_COLUMN_AMOUNT = 3;
    public Int32 GRID_COLUMN_VALIDATION_NUMBER = 4;



    // Column widths
    private const Int32 GRID_COLUMN_WIDTH_CREATION_DATE = 95; //160;
    private const Int32 GRID_COLUMN_WIDTH_TICKET_TYPE = 200;
    private const Int32 GRID_COLUMN_WIDTH_CREATED_AMOUNT = 145;
    private const Int32 GRID_COLUMN_WIDTH_AMOUNT = 135;
    private const Int32 GRID_COLUMN_WIDTH_VALIDATION_NUMBER = 185;

    private const Int32 CREATED_IN_LAST_HOURS = 24;


    #endregion

    #region Constructor

    //Constructor
    public uc_ticket_terminal()
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[UC LOAD] uc_ticket_terminal", Log.Type.Message);

      InitializeComponent();

      m_frm_shadow_gui = new frm_yesno();
      m_frm_shadow_gui.Opacity = 0.6;

      EventLastAction.AddEventLastAction(this.Controls);
    }

    //Establece el foco en el control lector de ticket.
    public void InitFocus()
    {
    }

    #endregion

    #region Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE:  Enable or Disable uc terminal and buttons.
    // 
    //  PARAMS:
    //      - INPUT:
    //            - Value (Boolean)
    //
    private void EnableButtons(Boolean Value, TITO_VALIDATION_TYPE ValidationType)
    {

      if (CashierBusinessLogic.ReadCashierStatus(Cashier.SessionId) != CASHIER_STATUS.OPEN)
      {
        this.uc_prov_terminals.Enabled = false;
      }
      else
      {
        if (ValidationType == TITO_VALIDATION_TYPE.SECURITY_ENHANCED)
        {
          this.uc_prov_terminals.Enabled = false;
          return;
        }
        else
        {
          this.uc_prov_terminals.Enabled = Value;
        }

      }

    } // EnableButtons



    //------------------------------------------------------------------------------
    // PURPOSE:  Variable reset
    // 
    //  PARAMS:
    //
    private void ClearForm()
    {
      ResetValues();
      EnableButtons(true, TITO_VALIDATION_TYPE.STANDARD);
    } // ClearForm

    //------------------------------------------------------------------------------
    // PURPOSE:  Initialize Control Resources
    // 
    //  PARAMS:
    //
    private void InitializeControlResources()
    {
      this.lbl_cards_title.Text = Resource.String("STR_UC_TICKET_TERMINAL_TITLE");
      this.gb_ticket_source.HeaderText = Resource.String("STR_FRM_TITO_HANDPAYS_SOURCE");
      //  this.gb_tickets_list.Text = Resource.String("STR_UC_TICKET_TERMINAL_BUTTON_CALLER");
      gb_tickets_list.HeaderText = Resource.String("STR_FRM_TITO_TICKETS_DISCARD_GROUP");

      ResetValues();
    } // InitializeControlResources

    //------------------------------------------------------------------------------
    // PURPOSE:  Restore all values
    // 
    //  PARAMS:
    //
    private void ResetValues()
    {
      EnableButtons(true, TITO_VALIDATION_TYPE.STANDARD);
    } // ResetValues




    //------------------------------------------------------------------------------
    // PURPOSE : Load last operations from a CashierSessionId to the LastOperations DataGridView.
    //
    //  PARAMS :
    //      - INPUT :
    //          - CashierSessionId
    //          - SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private void LoadTicketsList(DataTable TicketsTable)
    {
      String[] _message_params = { "" };
      DataColumn _str_col;
      DataColumn _num_col;
      TITO_VALIDATION_TYPE _validation_type;
      Int64 _ticket_number;

      try
      {
        //PBI 6902 Indicate terminal name
        // gb_tickets_list.Text = uc_prov_terminals.TerminalSelectedName();

        lbl_Terminal.Text = Resource.String("STR_TERMINAL_NAME", uc_prov_terminals.TerminalSelectedName());
        lbl_provider.Text = Resource.String("STR_PROVIDER_NAME", uc_prov_terminals.TerminalSelectedProvider());

        //Code to Format the Validation Number
        if (TicketsTable.Columns.Contains("VALIDATION_NUMBER"))
        {
          _num_col = TicketsTable.Columns[GRID_COLUMN_VALIDATION_NUMBER];
          _str_col = TicketsTable.Columns["VALIDATION_NUMBER"];
          _str_col.SetOrdinal(GRID_COLUMN_VALIDATION_NUMBER);
          foreach (DataRow _row in TicketsTable.Rows)
          {
            if (_row[_num_col.Ordinal].ToString() != String.Empty)
            {
              _ticket_number = ValidationNumberManager.UnFormatValidationNumber(_row[_num_col.Ordinal].ToString(), out _validation_type);
              _row[_str_col.Ordinal] = ValidationNumberManager.FormatValidationNumber(_ticket_number, (Int32)_validation_type, true);
            }
          }
        }


        dgv_last_operations.DataSource = TicketsTable;

        InitOperationsDataGrid();

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } // LoadOperations

    //------------------------------------------------------------------------------
    // PURPOSE : Get DataTable with tickets
    //
    //  PARAMS :
    //      - INPUT :
    //          - String: SearchString
    //          - SqlTransaction: Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - DataTable: Ticket List
    //
    private DataTable GetFilteredTicketsList()
    {
      DataTable _dt_tickets;
      StringBuilder _sb;

      Int32 _terminal_id;

      _dt_tickets = CreateSelectTicketTable();

      _terminal_id = uc_prov_terminals.TerminalSelected();

      if (_terminal_id == 0) // No eligio filtro
      {
        return _dt_tickets;
      }


      string _cm_type_string_cashable = Resource.String("STR_TICKET_TYPE_CASHABLE");
      string _cm_type_string_promo_nonredeem = Resource.String("STR_TICKET_TYPE_PROMO_NONREDEEM");
      string _cm_type_string_promo_redeem = Resource.String("STR_TICKET_TYPE_PROMO_REDEEM");
      string _cm_type_string_bill = Resource.String("STR_UC_TICKET_TERMINAL_BILL_COIN"); // Bill/Coin

      _sb = new StringBuilder();

      try
      {
        _sb.AppendLine("     SELECT   TOP 20 [CM_DATE]                                                                         ");
        _sb.AppendLine("            , ISNULL([CM_DETAILS], '') [VALIDATION_NUMBER]                                             ");
        _sb.AppendLine("            , CASE                                                                                     ");
        _sb.AppendLine("                   WHEN [CM_TYPE] = @pCashIn THEN '" + _cm_type_string_bill + "'                       ");
        _sb.AppendLine("                   WHEN [CM_TYPE] = @pCashable THEN '" + _cm_type_string_cashable + "'                 ");
        _sb.AppendLine("                   WHEN [CM_TYPE] = @pPromoRE THEN '" + _cm_type_string_promo_redeem + "'              ");
        _sb.AppendLine("                   WHEN [CM_TYPE] = @pPromoNR THEN '" + _cm_type_string_promo_nonredeem + "'           ");
        _sb.AppendLine("                   ELSE '' END [CM_TYPE]                                                               ");
        _sb.AppendLine("            , CASE WHEN [CM_TYPE] = @pCashIn THEN [CM_ADD_AMOUNT] ELSE [CM_SUB_AMOUNT] END [CM_AMOUNT] ");
        _sb.AppendLine("            , CAST(TI_AMT0 AS NVARCHAR(50)) + ' ' + TI_CUR0 [CM_CREATED_AMOUNT] ");
        _sb.AppendLine("       FROM   [DBO].[CASHIER_MOVEMENTS]                                                                ");
        _sb.AppendLine(" INNER JOIN   [DBO].[CASHIER_TERMINALS] ON [CM_CASHIER_ID] = [CT_CASHIER_ID]                           ");
        _sb.AppendLine("  LEFT JOIN   [DBO].[TICKETS]           ON [CM_DETAILS] = [TI_VALIDATION_NUMBER]                       ");
        _sb.AppendLine("      WHERE   [CM_DATE] > @pStartDate                                                                  ");
        _sb.AppendLine("        AND   [CM_TYPE] IN (@pCashIn, @pCashable,@pPromoRE,@pPromoNR)                                  ");

        if (_terminal_id > 0)
        {
          _sb.AppendLine("    	AND [CT_TERMINAL_ID] = @pTerminalId                                                             ");
        }

        _sb.AppendLine("    ORDER BY [CM_DATE] DESC                                                                                 ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            if (_terminal_id > 0)
            {
              _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = _terminal_id;
            }

            _cmd.Parameters.Add("@pStartDate", SqlDbType.DateTime).Value = WGDB.Now.AddHours(-CREATED_IN_LAST_HOURS);

            _cmd.Parameters.Add("@pCashIn", SqlDbType.Int).Value = CASHIER_MOVEMENT.NA_CASH_IN_SPLIT1;
            _cmd.Parameters.Add("@pPromoNR", SqlDbType.Int).Value = CASHIER_MOVEMENT.TITO_TICKET_MACHINE_PLAYED_PROMO_NOT_REDEEMABLE;
            _cmd.Parameters.Add("@pPromoRE", SqlDbType.Int).Value = CASHIER_MOVEMENT.TITO_TICKET_MACHINE_PLAYED_PROMO_REDEEMABLE;
            _cmd.Parameters.Add("@pCashable", SqlDbType.Int).Value = CASHIER_MOVEMENT.TITO_TICKET_MACHINE_PLAYED_CASHABLE;

            _cmd.Parameters.Add("@pBill", SqlDbType.Int).Value = CageCurrencyType.Bill;
            _cmd.Parameters.Add("@pOthers", SqlDbType.Int).Value = CageCurrencyType.Others;

            _dt_tickets.Load(_db_trx.ExecuteReader(_cmd));

          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return _dt_tickets;
    } // GeTicketsFromCashierSessionId

    //------------------------------------------------------------------------------
    // PURPOSE : Initialize Operations Data Grid.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private void InitOperationsDataGrid()
    {
      // Last Operations DataGrid
      dgv_last_operations.ColumnHeadersHeight = 30;
      dgv_last_operations.RowTemplate.Height = 30;
      dgv_last_operations.RowTemplate.DividerHeight = 0;
      //dgv_last_operations.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);

      // last ticket operation date
      dgv_last_operations.Columns[GRID_COLUMN_CREATION_DATE].HeaderCell.Value = Resource.String("STR_UC_TICKET_TERMINAL_DATA_HORA");
      dgv_last_operations.Columns[GRID_COLUMN_CREATION_DATE].Width = GRID_COLUMN_WIDTH_CREATION_DATE;
      //ETP PBI 6901 Se modifica el formato de la columna hora.
      dgv_last_operations.Columns[GRID_COLUMN_CREATION_DATE].DefaultCellStyle.Format = Resource.String("STR_FORMAT_CurrentDateTime");

      // ticket type     
      dgv_last_operations.Columns[GRID_COLUMN_TICKET_TYPE].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
      dgv_last_operations.Columns[GRID_COLUMN_TICKET_TYPE].Width = GRID_COLUMN_WIDTH_TICKET_TYPE;
      dgv_last_operations.Columns[GRID_COLUMN_TICKET_TYPE].HeaderCell.Value = Resource.String("STR_UC_TICKET_TERMINAL_DATA_TIPO");

      // ticket created amount
      if (Misc.IsFloorDualCurrencyEnabled())
      {
        dgv_last_operations.Columns[GRID_COLUMN_CREATED_AMOUNT].Width = GRID_COLUMN_WIDTH_CREATED_AMOUNT;
        dgv_last_operations.Columns[GRID_COLUMN_CREATED_AMOUNT].HeaderCell.Value = Resource.String("STR_FRM_VOUCHERS_REPRINT_095");
      }
      else
      {
        dgv_last_operations.Columns[GRID_COLUMN_CREATED_AMOUNT].Visible = false;
      }
      dgv_last_operations.Columns[GRID_COLUMN_CREATED_AMOUNT].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
      dgv_last_operations.Columns[GRID_COLUMN_CREATED_AMOUNT].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

      // ticket amount
      dgv_last_operations.Columns[GRID_COLUMN_AMOUNT].Width = GRID_COLUMN_WIDTH_AMOUNT;
      dgv_last_operations.Columns[GRID_COLUMN_AMOUNT].HeaderCell.Value = Resource.String("STR_UC_TICKET_TERMINAL_DATA_MONTO");
      dgv_last_operations.Columns[GRID_COLUMN_AMOUNT].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
      dgv_last_operations.Columns[GRID_COLUMN_AMOUNT].DefaultCellStyle.Format = "C" + Math.Abs(Format.GetFormattedDecimals(CurrencyExchange.GetNationalCurrency())).ToString();
      dgv_last_operations.Columns[GRID_COLUMN_AMOUNT].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

      // ticket validation number
      dgv_last_operations.Columns[GRID_COLUMN_VALIDATION_NUMBER].Width = GRID_COLUMN_WIDTH_VALIDATION_NUMBER;
      dgv_last_operations.Columns[GRID_COLUMN_VALIDATION_NUMBER].HeaderCell.Value = Resource.String("STR_UC_TICKET_TERMINAL_DATA_NUMERO");


    } // InitOperationsDataGrid



    //------------------------------------------------------------------------------
    // PURPOSE : Create Table Schema for ticket grid control
    //
    //  PARAMS :
    //      - INPUT :
    //      - OUTPUT :
    //
    // RETURNS : DataTable
    //
    //   NOTES :
    //
    public DataTable CreateSelectTicketTable()
    {
      DataTable _table = new DataTable();

      //        _table.Columns.Add("cm_movement_id", Type.GetType("System.Int64"));

      _table.Columns.Add("cm_date", Type.GetType("System.DateTime"));
      //        _table.Columns.Add("cm_related_id", Type.GetType("System.Int64"));
      _table.Columns.Add("cm_type", Type.GetType("System.String"));
      _table.Columns.Add("cm_created_amount", Type.GetType("System.String"));
      _table.Columns.Add("cm_amount", Type.GetType("System.Decimal"));
      _table.Columns.Add("validation_number", Type.GetType("System.String"));

      //        _table.Columns.Add("ct_terminal_id", Type.GetType("System.Int32"));

      //        _table.PrimaryKey = new DataColumn[] { _table.Columns["cm_movement_id"] };

      return _table;
    }

    #endregion

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE: Init controls and events
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    public void InitControls()
    {
      StringBuilder _where_condition;

      _where_condition = new StringBuilder();

      ClearForm();

      if (m_first_time)
      {

        m_first_time = false;

        InitializeControlResources();
      }
      EnableButtons(true, TITO_VALIDATION_TYPE.STANDARD);

      _where_condition.AppendLine(" WHERE TE_TERMINAL_TYPE = " + (Int32)TerminalTypes.SAS_HOST);
      _where_condition.AppendLine(String.Format("      AND (  TE_RETIREMENT_DATE IS NULL OR DATEDIFF(HOUR, TE_RETIREMENT_DATE, GETDATE()) <= {0} ) ", MAX_HOURS_IS_OFFLINE));
      uc_prov_terminals.InitControls("", _where_condition.ToString(), true, true);
      LoadTicketsList(GetFilteredTicketsList());
    } // InitControls

    #endregion

    #region Event Controls

    private void uc_terminal_terminal_KeyPress(object sender, KeyPressEventArgs e)
    {
      char c;

      c = e.KeyChar;


      e.Handled = false;
    } // uc_terminal_offline_KeyPress

    private void btn_ticket_search_Click(object sender, EventArgs e)
    {
      LoadTicketsList(GetFilteredTicketsList());
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Reload screen by terminal selected
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    private void uc_terminal_filter_TerminalOrProviderChanged()
    {


      LoadTicketsList(GetFilteredTicketsList());
    }

    #endregion

  }
}
