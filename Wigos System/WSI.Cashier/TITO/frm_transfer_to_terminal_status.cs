//------------------------------------------------------------------------------
// Copyright � 2007-2013 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_transfer_to_terminal_status
// 
//   DESCRIPTION: Implements the transfer_to_terminal_status form
//
//        AUTHOR: Marcos Piedra
//
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 13-JUN-2015 MPO    First release.
// 10-NOV-2015 ETP    Product Backlog Item 5847 - New cashier design - frm_transfer_to_terminal_status.
// 29-SEP-2016 FJC    Fixed Bug (Reopened) 17669:Anular retiro de caja desde b�veda/Cajero: no deshace el stock y conceptos de b�veda
//------------ ------ ----------------------------------------------------------



using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WSI.Common;
using System.Threading;
using System.Collections;
using System.Diagnostics;
using WSI.Cashier.Controls;

namespace WSI.Cashier.TITO
{
  public partial class frm_transfer_to_terminal_status : frm_base
  {
    private frm_yesno form_yes_no;
    private Form m_parent;
    private Decimal m_amount_to_transfer = 0;
    private Int32 m_terminal_id = 0;
    private String m_terminal_name = "";
    private Thread m_thread;
    private Int64 m_operation_id = 0;
    private CardData m_card_data = null;
    private MultiPromos.AccountBalance m_initial_bal = MultiPromos.AccountBalance.Zero;
    private ArrayList m_voucher_list = null;

    private Boolean m_continue_while = true;
    private TerminalFundsTransfer.TRANSFER_STATUS m_status_changed = (TerminalFundsTransfer.TRANSFER_STATUS)(-1);

    delegate void CommonDelegate();
    delegate void TextDelegate(String Text);
    delegate void BooleanDelegate(Boolean Boolean);
    delegate void MessageBoxIconDelegate(MessageBoxIcon MsgBoxIcon);

    public frm_transfer_to_terminal_status()
    {
      InitializeComponent();

      form_yes_no = new frm_yesno();
      form_yes_no.Opacity = 0.6;

      lbl_status.Text = "";
      this.FormTitle = Resource.String("STR_TRANSFER_STATUS");
      btn_retry.Text = Resource.String("STR_RETRY");
      btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");
    }

    public void Show(Form Parent, Int32 TerminalId, String TerminalName, Decimal Amount)
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_transfer_to_terminal_status", Log.Type.Message);

      m_parent = Parent;
      m_amount_to_transfer = Amount;
      m_terminal_id = TerminalId;
      m_terminal_name = TerminalName;
      m_card_data = new CardData();

      // Get card data info
      using (DB_TRX _db_trx = new DB_TRX())
      {
        if (!CardData.DB_CardGetAllData(Accounts.GetOrCreateVirtualAccount(TerminalId, _db_trx.SqlTransaction), m_card_data, _db_trx.SqlTransaction))
        {
          Log.Error("Error on frm_transfer_to_terminal_status.Show: DB_CardGetAllData");

          return;
        }

        _db_trx.Commit();
      }

      m_initial_bal = m_card_data.AccountBalance;
      RefreshControlToStatus(TerminalFundsTransfer.TRANSFER_STATUS.READY);

      try
      {
        form_yes_no.Show(Parent);
        this.ShowDialog(form_yes_no);
        form_yes_no.Hide();

        Misc.WriteLog("[FORM CLOSE] frm_transfer_to_terminal_status (show)", Log.Type.Message);
        this.Close();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        form_yes_no.Hide();
      }
    }

    private void btn_cancel_Click(object sender, EventArgs e)
    {
      TerminalFundsTransfer.TRANSFER_STATUS _current_status;

      m_continue_while = false;

      while (m_thread.IsAlive)
      {
      }
      SetEnabledRetry(false);
      SetEnabledCancel(false);

      try
      {

        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (!TerminalFundsTransfer.DB_GetTransferStatus(m_terminal_id, out _current_status, _db_trx.SqlTransaction))
          {
          }
        }

        switch (_current_status)
        {
          case TerminalFundsTransfer.TRANSFER_STATUS.READY:
            break;
          case TerminalFundsTransfer.TRANSFER_STATUS.TRANSFERRING:
          case TerminalFundsTransfer.TRANSFER_STATUS.TRANSFERRED:

            break;
          case TerminalFundsTransfer.TRANSFER_STATUS.CANCELED_BY_USER:
          case TerminalFundsTransfer.TRANSFER_STATUS.CANCELED_BY_TERMINAL:
          case TerminalFundsTransfer.TRANSFER_STATUS.TRANSFER_PENDING:
          case TerminalFundsTransfer.TRANSFER_STATUS.NOTIFIED:

            OperationUndo.InputUndoOperation _input_undo_operation;
            ArrayList _voucher_list;

            using (DB_TRX _db_trx = new DB_TRX())
            {
              if (m_operation_id > 0)
              {
                _input_undo_operation = new OperationUndo.InputUndoOperation();
                _input_undo_operation.CardData = m_card_data;
                _input_undo_operation.CashierSessionInfo = CommonCashierInformation.CashierSessionInfo();
                _input_undo_operation.CodeOperation = OperationCode.CASH_IN;
                _input_undo_operation.GenerateVoucherForUndo = false;
                _input_undo_operation.OperationId = 0;
                _input_undo_operation.OperationIdForUndo = m_operation_id;
                _input_undo_operation.TransferToTerminalOperation = true;

                if (!OperationUndo.UndoOperation(_input_undo_operation, false,  out _voucher_list, _db_trx.SqlTransaction))
                {
                  Log.Error("Transfer to terminal: btn_cancel_Click: Error on UndoOperation");
                }
              }

              if (!TerminalFundsTransfer.DB_UpdateTerminalFundsTransferStatus(m_terminal_id, TerminalFundsTransfer.TRANSFER_STATUS.CANCELED_BY_USER, _db_trx.SqlTransaction))
              {
                Log.Error("Transfer to terminal: btn_cancel_Click: Error on DB_UpdateTerminalFundsTransferStatus");
              }

              if (!TerminalFundsTransfer.DB_UpdateTerminalFundsTransferStatus(m_terminal_id, TerminalFundsTransfer.TRANSFER_STATUS.READY, _db_trx.SqlTransaction))
              {
                Log.Error("Transfer to terminal: btn_cancel_Click: Error on DB_UpdateTerminalFundsTransferStatus");
              }

              _db_trx.Commit();
            }

            break;
          default:
            break;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      form_yes_no.Hide();

      Misc.WriteLog("[FORM CLOSE] frm_transfer_to_terminal_status (cancel)", Log.Type.Message);
      this.Close();
    }

    private void frm_transfer_to_terminal_status_Shown(object sender, EventArgs e)
    {
      m_thread = new Thread(GetAndRefreshStatus);
      m_thread.Name = "GetAndRefreshStatus";
      m_thread.Start();
    }

    private void GetAndRefreshStatus()
    {
      TerminalFundsTransfer.TRANSFER_STATUS _current_status;
      TimeSpan _pending_timespan;
      DateTime _time_before_pending;
      Int32 _retry_command;

      m_continue_while = true;
      _pending_timespan = new TimeSpan(0);
      _time_before_pending = WGDB.Now;
      _retry_command = 0;

      try
      {
        do
        {
          using (DB_TRX _db_trx = new DB_TRX())
          {
            if (!TerminalFundsTransfer.DB_GetTransferStatus(m_terminal_id, out _current_status, _db_trx.SqlTransaction))
            {
              m_continue_while = false;
            }
          }

          switch (_current_status)
          {
            case TerminalFundsTransfer.TRANSFER_STATUS.READY:
              if (!AddCreditAndChangeStatus(m_terminal_id, m_amount_to_transfer))
              {
                break;
              }
              RefreshControlToStatus(TerminalFundsTransfer.TRANSFER_STATUS.TRANSFER_PENDING);
              _time_before_pending = WGDB.Now;

              break;
            default:

              RefreshControlToStatus(_current_status);

              if (_current_status == TerminalFundsTransfer.TRANSFER_STATUS.TRANSFER_PENDING ||
                  _current_status == TerminalFundsTransfer.TRANSFER_STATUS.NOTIFIED)
              {
                _pending_timespan = WGDB.Now.Subtract(_time_before_pending);
                if (_pending_timespan.TotalSeconds > 5)
                {
                  Log.Warning("GetAndRefreshStatus: retry insert command for transfer pending");
                  using (DB_TRX _db_trx = new DB_TRX())
                  {
                    if (!WcpCommands.InsertWcpCommand(m_terminal_id, WCP_CommandCode.RequestTransfer, "", _db_trx.SqlTransaction))
                    {
                      break;
                    }

                    if (!_db_trx.Commit())
                    {
                      Log.Error("GetAndRefreshStatus: Error on InsertWcpCommand");
                      break;
                    }
                    _time_before_pending = WGDB.Now;
                    _retry_command++;
                  }
                  if (_retry_command > 5)
                  {
                    Log.Error("GetAndRefreshStatus: Canceled by user");
                    using (DB_TRX _db_trx = new DB_TRX())
                    {
                      if (!TerminalFundsTransfer.DB_UpdateTerminalFundsTransferStatus(m_terminal_id, TerminalFundsTransfer.TRANSFER_STATUS.CANCELED_BY_USER, _db_trx.SqlTransaction))
                      {
                        break;
                      }
                      if (!_db_trx.Commit())
                      {
                        Log.Error("GetAndRefreshStatus: Error on InsertWcpCommand");
                        break;
                      }
                    }
                    _retry_command = 0;
                  }
                }
              }

              if (_current_status == TerminalFundsTransfer.TRANSFER_STATUS.TRANSFERRED)
              {
                m_continue_while = false;
                using (DB_TRX _db_trx = new DB_TRX())
                {
                  if (!TerminalFundsTransfer.DB_UpdateTerminalFundsTransferStatus(m_terminal_id, TerminalFundsTransfer.TRANSFER_STATUS.READY, _db_trx.SqlTransaction))
                  {
                    break;
                  }
                  if (_db_trx.Commit())
                  {
                    Log.Error("GetAndRefreshStatus: Error on DB_UpdateTerminalFundsTransferStatus");
                  }
                }
                if (m_voucher_list != null)
                {
                  VoucherPrint.Print(m_voucher_list);
                }
              }

              break;
          }

          Thread.Sleep(500);

          lock (this)
          {
          }

        } while (m_continue_while);

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    }

    private void RefreshControlToStatus(TerminalFundsTransfer.TRANSFER_STATUS CurrentStatus)
    {
      if (m_status_changed == CurrentStatus)
      {
        return;
      }
      m_status_changed = CurrentStatus;

      switch (CurrentStatus)
      {
        case TerminalFundsTransfer.TRANSFER_STATUS.READY:

          SetEnabledRetry(false);
          SetEnabledCancel(true);
          SetImage(MessageBoxIcon.Information);
          SetLabelStatus(GetLblInfo(CurrentStatus));

          break;
        case TerminalFundsTransfer.TRANSFER_STATUS.TRANSFER_PENDING:
        case TerminalFundsTransfer.TRANSFER_STATUS.NOTIFIED:
        case TerminalFundsTransfer.TRANSFER_STATUS.TRANSFERRING:

          SetEnabledRetry(false);
          SetEnabledCancel(false);
          SetImage(MessageBoxIcon.Information);
          SetLabelStatus(GetLblInfo(CurrentStatus));
          SetVisibleBar(true);

          break;
        case TerminalFundsTransfer.TRANSFER_STATUS.TRANSFERRED:

          SetEnabledRetry(false);
          SetEnabledCancel(true);
          SetImage(MessageBoxIcon.Information);
          SetLabelStatus(GetLblInfo(CurrentStatus));
          SetTextBottonCancel(Resource.String("STR_FORMAT_GENERAL_BUTTONS_OK"));
          SetVisibleBar(false);

          break;
        case TerminalFundsTransfer.TRANSFER_STATUS.CANCELED_BY_USER:
        case TerminalFundsTransfer.TRANSFER_STATUS.CANCELED_BY_TERMINAL:

          SetEnabledRetry(true);
          SetEnabledCancel(true);
          SetImage(MessageBoxIcon.Exclamation);
          SetLabelStatus(GetLblInfo(CurrentStatus));
          SetTextBottonCancel(Resource.String("STR_FORMAT_GENERAL_BUTTONS_OK"));
          SetVisibleBar(false);

          break;
        default:
          break;
      }
    }

    private String GetLblInfo(TerminalFundsTransfer.TRANSFER_STATUS CurrentStatus)
    {
      String _lbl;
      String[] _message_params = { "", "", "", "", "", "", "" };

      switch (CurrentStatus)
      {
        case TerminalFundsTransfer.TRANSFER_STATUS.READY:
        case TerminalFundsTransfer.TRANSFER_STATUS.TRANSFER_PENDING:
        case TerminalFundsTransfer.TRANSFER_STATUS.NOTIFIED:
        case TerminalFundsTransfer.TRANSFER_STATUS.TRANSFERRING:

          _message_params[0] = ((Currency)m_amount_to_transfer).ToString();
          _message_params[1] = m_terminal_name;
          _lbl = Resource.String("STR_TRANSFERING", _message_params);
          if (m_initial_bal.TotalBalance > 0)
          {
            _lbl += "\n(Total: " + ((Currency)m_initial_bal.TotalBalance).ToString() + ")";
          }

          break;
        case TerminalFundsTransfer.TRANSFER_STATUS.TRANSFERRED:

          _message_params[0] = ((Currency)m_amount_to_transfer).ToString();
          _message_params[1] = m_terminal_name;
          _lbl = Resource.String("STR_TRANSFERRED", _message_params);
          if (m_initial_bal.TotalBalance > 0)
          {
            _lbl += "\n(Total: " + ((Currency)m_initial_bal.TotalBalance).ToString() + ")";
          }

          break;
        case TerminalFundsTransfer.TRANSFER_STATUS.CANCELED_BY_USER:
        case TerminalFundsTransfer.TRANSFER_STATUS.CANCELED_BY_TERMINAL:

          _lbl = Resource.String("STR_ERROR_WHEN_TRANSFER");

          break;
        default:
          _lbl = "";
          break;
      }

      return _lbl;

    }

    private Boolean AddCreditAndChangeStatus(Int32 TerminalId, Decimal Amount)
    {
      ParticipateInCashDeskDraw _participate_in_cash_draw;
      CurrencyExchangeResult _exchange_result;
      RechargeOutputParameters OutputParameter;
      CashierSessionInfo _cashier_session_info;

      try
      {
        _participate_in_cash_draw = new ParticipateInCashDeskDraw();
        _exchange_result = null;
        _cashier_session_info = Cashier.CashierSessionInfo();

        using (DB_TRX _trx = new DB_TRX())
        {
          if (!Accounts.DB_CardCreditAdd(m_card_data,
                                                     Amount,
                                                     0,
                                                     0,
                                                     0,
                                                     0,
                                                     _exchange_result,
                                                     _participate_in_cash_draw,
                                                     OperationCode.CASH_IN,
                                                     _cashier_session_info,
                                                     _trx.SqlTransaction,
                                                     out OutputParameter))
          {
            return false;
          }

          m_operation_id = OutputParameter.OperationId;

          if (!TerminalFundsTransfer.DB_UpdateTerminalFundsTransferStatus(TerminalId, TerminalFundsTransfer.TRANSFER_STATUS.TRANSFER_PENDING, _trx.SqlTransaction))
          {
            return false;
          }

          if (!WcpCommands.InsertWcpCommand(TerminalId, WCP_CommandCode.RequestTransfer, "", _trx.SqlTransaction))
          {
            return false;
          }

          m_voucher_list = OutputParameter.VoucherList;

          return _trx.Commit();
        }

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    private Boolean ChangeStatusToTransferPending(Int32 TerminalId, Decimal Amount)
    {
      try
      {
        using (DB_TRX _trx = new DB_TRX())
        {
          //if (m_card_data.AccountBalance.TotalBalance != Amount)
          //{
          //  Log.Error("ChangeStatusToTransferPending: AccountBalance: " + m_card_data.AccountBalance.ToString() + ", Amount" + Amount.ToString());

          //  return false;
          //}

          if (!TerminalFundsTransfer.DB_UpdateTerminalFundsTransferStatus(TerminalId, TerminalFundsTransfer.TRANSFER_STATUS.TRANSFER_PENDING, _trx.SqlTransaction))
          {
            return false;
          }

          if (!WcpCommands.InsertWcpCommand(TerminalId, WCP_CommandCode.RequestTransfer, "", _trx.SqlTransaction))
          {
            return false;
          }

          return _trx.Commit();
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    private void SetLabelStatus(String Text)
    {
      if (this.lbl_status.InvokeRequired)
      {
        TextDelegate _dlg = new TextDelegate(SetLabelStatus);
        this.Invoke(_dlg, new object[] { Text });
      }
      else
      {
        this.lbl_status.Text = Text;
      }
    }

    private void SetTextBottonCancel(String Text)
    {
      if (this.btn_cancel.InvokeRequired)
      {
        TextDelegate _dlg = new TextDelegate(SetTextBottonCancel);
        this.Invoke(_dlg, new object[] { Text });
      }
      else
      {
        this.btn_cancel.Text = Text;
      }
    }

    private void SetVisibleBar(Boolean Visible)
    {
      if (this.bar_status.InvokeRequired)
      {
        BooleanDelegate _dlg = new BooleanDelegate(SetVisibleBar);
        this.Invoke(_dlg, new object[] { Visible });
      }
      else
      {
        this.bar_status.Visible = Visible;
      }
    }

    private void SetEnabledRetry(Boolean Enabled)
    {
      if (this.btn_retry.InvokeRequired)
      {
        BooleanDelegate _dlg = new BooleanDelegate(SetEnabledRetry);
        this.Invoke(_dlg, new object[] { Enabled });
      }
      else
      {
        this.btn_retry.Enabled = Enabled;
        //Debug.Print("SetEnabledRetry");
      }
    }

    private void SetEnabledCancel(Boolean Enabled)
    {
      if (this.btn_cancel.InvokeRequired)
      {
        BooleanDelegate _dlg = new BooleanDelegate(SetEnabledCancel);
        this.Invoke(_dlg, new object[] { Enabled });
      }
      else
      {
        this.btn_cancel.Enabled = Enabled;
      }
    }

    private void SetImage(MessageBoxIcon IconType)
    {
      if (this.icon_box.InvokeRequired)
      {
        MessageBoxIconDelegate _dlg = new MessageBoxIconDelegate(SetImage);
        this.Invoke(_dlg, new object[] { IconType });
      }
      else
      {
        Images.CashierImage _image_id;

        switch (IconType)
        {
          case MessageBoxIcon.Error:
            _image_id = Images.CashierImage.Error;
            break;
          case MessageBoxIcon.Information:
            _image_id = Images.CashierImage.Information;
            break;
          case MessageBoxIcon.Question:
            _image_id = Images.CashierImage.Question;
            break;
          case MessageBoxIcon.Warning:
            _image_id = Images.CashierImage.Warning;
            break;
          default:
            _image_id = Images.CashierImage.Warning;
            break;
        }

        icon_box.Image = Images.Get32x32(_image_id);
      }
    }

    private void btn_retry_Click(object sender, EventArgs e)
    {
      lock (this)
      {
        this.btn_retry.Enabled = false;
        this.btn_cancel.Enabled = false;
        this.bar_status.Visible = true;

        ChangeStatusToTransferPending(m_terminal_id, m_amount_to_transfer);
        RefreshControlToStatus(TerminalFundsTransfer.TRANSFER_STATUS.TRANSFER_PENDING);
      }
    }

  }
}