namespace WSI.Cashier
{
  partial class uc_ticket_undo
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
      this.lbl_separator_1 = new System.Windows.Forms.Label();
      this.btn_discard_tickets = new WSI.Cashier.Controls.uc_round_button();
      this.btn_details = new WSI.Cashier.Controls.uc_round_button();
      this.gp_voucher_box = new WSI.Cashier.Controls.uc_round_panel();
      this.web_browser = new System.Windows.Forms.WebBrowser();
      this.btn_undo_ticket = new WSI.Cashier.Controls.uc_round_button();
      this.gb_tickets_list = new WSI.Cashier.Controls.uc_round_panel();
      this.dgv_last_operations = new WSI.Cashier.Controls.uc_DataGridView();
      this.lbl_title = new WSI.Cashier.Controls.uc_label();
      this.gp_voucher_box.SuspendLayout();
      this.gb_tickets_list.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dgv_last_operations)).BeginInit();
      this.SuspendLayout();
      // 
      // lbl_separator_1
      // 
      this.lbl_separator_1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_separator_1.BackColor = System.Drawing.Color.Black;
      this.lbl_separator_1.Location = new System.Drawing.Point(-3, 60);
      this.lbl_separator_1.Name = "lbl_separator_1";
      this.lbl_separator_1.Size = new System.Drawing.Size(885, 1);
      this.lbl_separator_1.TabIndex = 1;
      // 
      // btn_discard_tickets
      // 
      this.btn_discard_tickets.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_discard_tickets.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_discard_tickets.FlatAppearance.BorderSize = 0;
      this.btn_discard_tickets.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_discard_tickets.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_discard_tickets.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_discard_tickets.Image = null;
      this.btn_discard_tickets.IsSelected = false;
      this.btn_discard_tickets.Location = new System.Drawing.Point(538, 579);
      this.btn_discard_tickets.Name = "btn_discard_tickets";
      this.btn_discard_tickets.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_discard_tickets.Size = new System.Drawing.Size(155, 60);
      this.btn_discard_tickets.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_discard_tickets.TabIndex = 4;
      this.btn_discard_tickets.Text = "XDISCARDTICKETS";
      this.btn_discard_tickets.UseVisualStyleBackColor = false;
      this.btn_discard_tickets.Click += new System.EventHandler(this.btn_discard_tickets_Click);
      // 
      // btn_details
      // 
      this.btn_details.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_details.FlatAppearance.BorderSize = 0;
      this.btn_details.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_details.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_details.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_details.Image = null;
      this.btn_details.IsSelected = false;
      this.btn_details.Location = new System.Drawing.Point(18, 579);
      this.btn_details.Name = "btn_details";
      this.btn_details.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_details.Size = new System.Drawing.Size(155, 60);
      this.btn_details.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_details.TabIndex = 3;
      this.btn_details.Text = "XDETAILS";
      this.btn_details.UseVisualStyleBackColor = false;
      // 
      // gp_voucher_box
      // 
      this.gp_voucher_box.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.gp_voucher_box.BackColor = System.Drawing.Color.Transparent;
      this.gp_voucher_box.BorderColor = System.Drawing.Color.Empty;
      this.gp_voucher_box.Controls.Add(this.web_browser);
      this.gp_voucher_box.CornerRadius = 1;
      this.gp_voucher_box.HeaderBackColor = System.Drawing.Color.Empty;
      this.gp_voucher_box.HeaderForeColor = System.Drawing.Color.Empty;
      this.gp_voucher_box.HeaderHeight = 0;
      this.gp_voucher_box.HeaderSubText = null;
      this.gp_voucher_box.HeaderText = null;
      this.gp_voucher_box.Location = new System.Drawing.Point(538, 82);
      this.gp_voucher_box.Name = "gp_voucher_box";
      this.gp_voucher_box.PanelBackColor = System.Drawing.Color.Empty;
      this.gp_voucher_box.Size = new System.Drawing.Size(330, 480);
      this.gp_voucher_box.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NONE;
      this.gp_voucher_box.TabIndex = 99;
      // 
      // web_browser
      // 
      this.web_browser.AllowWebBrowserDrop = false;
      this.web_browser.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.web_browser.CausesValidation = false;
      this.web_browser.IsWebBrowserContextMenuEnabled = false;
      this.web_browser.Location = new System.Drawing.Point(1, 0);
      this.web_browser.MinimumSize = new System.Drawing.Size(20, 20);
      this.web_browser.Name = "web_browser";
      this.web_browser.ScriptErrorsSuppressed = true;
      this.web_browser.Size = new System.Drawing.Size(329, 480);
      this.web_browser.TabIndex = 0;
      this.web_browser.Url = new System.Uri("", System.UriKind.Relative);
      this.web_browser.WebBrowserShortcutsEnabled = false;
      // 
      // btn_undo_ticket
      // 
      this.btn_undo_ticket.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_undo_ticket.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_undo_ticket.FlatAppearance.BorderSize = 0;
      this.btn_undo_ticket.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_undo_ticket.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_undo_ticket.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_undo_ticket.Image = null;
      this.btn_undo_ticket.IsSelected = false;
      this.btn_undo_ticket.Location = new System.Drawing.Point(713, 579);
      this.btn_undo_ticket.Name = "btn_undo_ticket";
      this.btn_undo_ticket.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_undo_ticket.Size = new System.Drawing.Size(155, 60);
      this.btn_undo_ticket.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_undo_ticket.TabIndex = 5;
      this.btn_undo_ticket.Text = "XUNDOTICKET";
      this.btn_undo_ticket.UseVisualStyleBackColor = false;
      // 
      // gb_tickets_list
      // 
      this.gb_tickets_list.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.gb_tickets_list.BackColor = System.Drawing.Color.Transparent;
      this.gb_tickets_list.BorderColor = System.Drawing.Color.Empty;
      this.gb_tickets_list.Controls.Add(this.dgv_last_operations);
      this.gb_tickets_list.CornerRadius = 10;
      this.gb_tickets_list.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_tickets_list.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gb_tickets_list.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_tickets_list.HeaderHeight = 35;
      this.gb_tickets_list.HeaderSubText = null;
      this.gb_tickets_list.HeaderText = "XTICKETLIST";
      this.gb_tickets_list.Location = new System.Drawing.Point(18, 82);
      this.gb_tickets_list.Name = "gb_tickets_list";
      this.gb_tickets_list.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_tickets_list.Size = new System.Drawing.Size(502, 482);
      this.gb_tickets_list.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.gb_tickets_list.TabIndex = 2;
      this.gb_tickets_list.Text = "xTicketsList";
      // 
      // dgv_last_operations
      // 
      this.dgv_last_operations.AllowUserToAddRows = false;
      this.dgv_last_operations.AllowUserToDeleteRows = false;
      this.dgv_last_operations.AllowUserToResizeColumns = false;
      this.dgv_last_operations.AllowUserToResizeRows = false;
      this.dgv_last_operations.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.dgv_last_operations.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.dgv_last_operations.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.dgv_last_operations.ColumnHeaderHeight = 35;
      this.dgv_last_operations.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
      dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      dataGridViewCellStyle1.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
      dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
      dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
      this.dgv_last_operations.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
      this.dgv_last_operations.ColumnHeadersHeight = 35;
      this.dgv_last_operations.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
      this.dgv_last_operations.CornerRadius = 10;
      this.dgv_last_operations.DefaultCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.dgv_last_operations.DefaultCellForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.dgv_last_operations.DefaultCellSelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.dgv_last_operations.DefaultCellSelectionForeColor = System.Drawing.Color.White;
      dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      dataGridViewCellStyle2.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.White;
      dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
      this.dgv_last_operations.DefaultCellStyle = dataGridViewCellStyle2;
      this.dgv_last_operations.EnableHeadersVisualStyles = false;
      this.dgv_last_operations.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.dgv_last_operations.GridColor = System.Drawing.Color.LightGray;
      this.dgv_last_operations.HeaderBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
      this.dgv_last_operations.HeaderDefaultCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.dgv_last_operations.HeaderDefaultCellForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.dgv_last_operations.HeaderImages = null;
      this.dgv_last_operations.Location = new System.Drawing.Point(-1, 35);
      this.dgv_last_operations.MultiSelect = false;
      this.dgv_last_operations.Name = "dgv_last_operations";
      this.dgv_last_operations.ReadOnly = true;
      this.dgv_last_operations.RowHeadersVisible = false;
      this.dgv_last_operations.RowHeadersWidth = 20;
      this.dgv_last_operations.RowTemplate.Height = 35;
      this.dgv_last_operations.RowTemplateHeight = 35;
      this.dgv_last_operations.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
      this.dgv_last_operations.Size = new System.Drawing.Size(503, 447);
      this.dgv_last_operations.Style = WSI.Cashier.Controls.uc_DataGridView.DataGridViewStyle.WITH_BOTTOM_ROUND_BORDERS;
      this.dgv_last_operations.TabIndex = 0;
      this.dgv_last_operations.SelectionChanged += new System.EventHandler(this.dgv_last_operations_SelectionChanged);
      // 
      // lbl_title
      // 
      this.lbl_title.BackColor = System.Drawing.Color.Transparent;
      this.lbl_title.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_title.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_title.Image = global::WSI.Cashier.Properties.Resources.ticketsTito_header;
      this.lbl_title.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.lbl_title.Location = new System.Drawing.Point(11, 20);
      this.lbl_title.Name = "lbl_title";
      this.lbl_title.Size = new System.Drawing.Size(275, 33);
      this.lbl_title.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_title.TabIndex = 0;
      this.lbl_title.Text = "XTICKETS UNDO OPERATION";
      // 
      // uc_ticket_undo
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.SystemColors.Control;
      this.Controls.Add(this.btn_discard_tickets);
      this.Controls.Add(this.btn_details);
      this.Controls.Add(this.gp_voucher_box);
      this.Controls.Add(this.btn_undo_ticket);
      this.Controls.Add(this.gb_tickets_list);
      this.Controls.Add(this.lbl_title);
      this.Controls.Add(this.lbl_separator_1);
      this.Name = "uc_ticket_undo";
      this.Size = new System.Drawing.Size(884, 656);
      this.gp_voucher_box.ResumeLayout(false);
      this.gb_tickets_list.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.dgv_last_operations)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private Controls.uc_label lbl_title;
    private System.Windows.Forms.Label lbl_separator_1;
    private Controls.uc_round_button btn_undo_ticket;
    private Controls.uc_round_panel gb_tickets_list;
    private Controls.uc_DataGridView dgv_last_operations;
    private Controls.uc_round_panel gp_voucher_box;
    public System.Windows.Forms.WebBrowser web_browser;
    private Controls.uc_round_button btn_details;
    private Controls.uc_round_button btn_discard_tickets;
  }
}
