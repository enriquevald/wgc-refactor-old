namespace WSI.Cashier
{
  partial class frm_tickets_reprint
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_tickets_reprint));
      this.gb_search = new WSI.Cashier.Controls.uc_round_panel();
      this.txt_ticket_number = new WSI.Cashier.NumericTextBox();
      this.uc_prov_terminals = new WSI.Cashier.uc_terminal_filter();
      this.lbl_ticket_number = new WSI.Cashier.Controls.uc_label();
      this.txt_floorID = new WSI.Cashier.Controls.uc_round_textbox();
      this.lbl_floor_id = new WSI.Cashier.Controls.uc_label();
      this.btn_ticket_search = new WSI.Cashier.Controls.uc_round_button();
      this.btn_print_selected = new WSI.Cashier.Controls.uc_round_button();
      this.gb_last_operations = new WSI.Cashier.Controls.uc_round_panel();
      this.dgv_last_operations = new WSI.Cashier.Controls.uc_DataGridView();
      this.btn_close = new WSI.Cashier.Controls.uc_round_button();
      this.btn_keyboard = new WSI.Cashier.Controls.uc_round_button();
      this.btn_cancell_selected = new WSI.Cashier.Controls.uc_round_button();
      this.pnl_data.SuspendLayout();
      this.gb_search.SuspendLayout();
      this.gb_last_operations.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dgv_last_operations)).BeginInit();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.Controls.Add(this.btn_cancell_selected);
      this.pnl_data.Controls.Add(this.btn_keyboard);
      this.pnl_data.Controls.Add(this.gb_search);
      this.pnl_data.Controls.Add(this.btn_print_selected);
      this.pnl_data.Controls.Add(this.gb_last_operations);
      this.pnl_data.Controls.Add(this.btn_close);
      this.pnl_data.Size = new System.Drawing.Size(1024, 658);
      this.pnl_data.TabIndex = 0;
      // 
      // gb_search
      // 
      this.gb_search.BackColor = System.Drawing.Color.Transparent;
      this.gb_search.BorderColor = System.Drawing.Color.Empty;
      this.gb_search.Controls.Add(this.txt_ticket_number);
      this.gb_search.Controls.Add(this.uc_prov_terminals);
      this.gb_search.Controls.Add(this.lbl_ticket_number);
      this.gb_search.Controls.Add(this.txt_floorID);
      this.gb_search.Controls.Add(this.lbl_floor_id);
      this.gb_search.Controls.Add(this.btn_ticket_search);
      this.gb_search.CornerRadius = 10;
      this.gb_search.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_search.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gb_search.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_search.HeaderHeight = 35;
      this.gb_search.HeaderSubText = null;
      this.gb_search.HeaderText = "XSOURCE";
      this.gb_search.Location = new System.Drawing.Point(180, 6);
      this.gb_search.Name = "gb_search";
      this.gb_search.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_search.Size = new System.Drawing.Size(655, 362);
      this.gb_search.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.gb_search.TabIndex = 0;
      this.gb_search.Text = "xSource";
      // 
      // txt_ticket_number
      // 
      this.txt_ticket_number.AllowSpace = false;
      this.txt_ticket_number.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_ticket_number.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_ticket_number.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_ticket_number.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_ticket_number.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_ticket_number.CornerRadius = 5;
      this.txt_ticket_number.FillWithCeros = false;
      this.txt_ticket_number.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_ticket_number.Location = new System.Drawing.Point(127, 313);
      this.txt_ticket_number.MaxLength = 18;
      this.txt_ticket_number.Multiline = false;
      this.txt_ticket_number.Name = "txt_ticket_number";
      this.txt_ticket_number.PasswordChar = '\0';
      this.txt_ticket_number.ReadOnly = false;
      this.txt_ticket_number.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_ticket_number.SelectedText = "";
      this.txt_ticket_number.SelectionLength = 0;
      this.txt_ticket_number.SelectionStart = 0;
      this.txt_ticket_number.Size = new System.Drawing.Size(282, 40);
      this.txt_ticket_number.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_ticket_number.TabIndex = 4;
      this.txt_ticket_number.TabStop = false;
      this.txt_ticket_number.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      this.txt_ticket_number.UseSystemPasswordChar = false;
      this.txt_ticket_number.WaterMark = null;
      this.txt_ticket_number.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      this.txt_ticket_number.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_ticket_number_KeyPress);
      // 
      // uc_prov_terminals
      // 
      this.uc_prov_terminals.BackColor = System.Drawing.Color.Transparent;
      this.uc_prov_terminals.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(2)));
      this.uc_prov_terminals.Location = new System.Drawing.Point(-1, 35);
      this.uc_prov_terminals.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
      this.uc_prov_terminals.Name = "uc_prov_terminals";
      this.uc_prov_terminals.Size = new System.Drawing.Size(657, 210);
      this.uc_prov_terminals.TabIndex = 0;
      // 
      // lbl_ticket_number
      // 
      this.lbl_ticket_number.AutoSize = true;
      this.lbl_ticket_number.BackColor = System.Drawing.Color.Transparent;
      this.lbl_ticket_number.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_ticket_number.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_ticket_number.Location = new System.Drawing.Point(13, 326);
      this.lbl_ticket_number.Name = "lbl_ticket_number";
      this.lbl_ticket_number.Size = new System.Drawing.Size(114, 19);
      this.lbl_ticket_number.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_ticket_number.TabIndex = 3;
      this.lbl_ticket_number.Text = "xTicketNumber";
      this.lbl_ticket_number.TextAlign = System.Drawing.ContentAlignment.TopRight;
      // 
      // txt_floorID
      // 
      this.txt_floorID.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_floorID.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_floorID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_floorID.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_floorID.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_floorID.CornerRadius = 5;
      this.txt_floorID.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_floorID.Location = new System.Drawing.Point(127, 264);
      this.txt_floorID.MaxLength = 20;
      this.txt_floorID.Multiline = false;
      this.txt_floorID.Name = "txt_floorID";
      this.txt_floorID.PasswordChar = '\0';
      this.txt_floorID.ReadOnly = false;
      this.txt_floorID.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_floorID.SelectedText = "";
      this.txt_floorID.SelectionLength = 0;
      this.txt_floorID.SelectionStart = 0;
      this.txt_floorID.Size = new System.Drawing.Size(282, 40);
      this.txt_floorID.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_floorID.TabIndex = 2;
      this.txt_floorID.TabStop = false;
      this.txt_floorID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      this.txt_floorID.UseSystemPasswordChar = false;
      this.txt_floorID.WaterMark = null;
      this.txt_floorID.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // lbl_floor_id
      // 
      this.lbl_floor_id.AutoSize = true;
      this.lbl_floor_id.BackColor = System.Drawing.Color.Transparent;
      this.lbl_floor_id.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_floor_id.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_floor_id.Location = new System.Drawing.Point(13, 274);
      this.lbl_floor_id.Name = "lbl_floor_id";
      this.lbl_floor_id.Size = new System.Drawing.Size(65, 19);
      this.lbl_floor_id.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_floor_id.TabIndex = 1;
      this.lbl_floor_id.Text = "xFloorId";
      this.lbl_floor_id.TextAlign = System.Drawing.ContentAlignment.TopRight;
      // 
      // btn_ticket_search
      // 
      this.btn_ticket_search.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_ticket_search.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_ticket_search.FlatAppearance.BorderSize = 0;
      this.btn_ticket_search.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_ticket_search.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_ticket_search.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_ticket_search.Image = null;
      this.btn_ticket_search.IsSelected = false;
      this.btn_ticket_search.Location = new System.Drawing.Point(507, 306);
      this.btn_ticket_search.Name = "btn_ticket_search";
      this.btn_ticket_search.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_ticket_search.Size = new System.Drawing.Size(137, 47);
      this.btn_ticket_search.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_ticket_search.TabIndex = 5;
      this.btn_ticket_search.Text = "XBUSCAR";
      this.btn_ticket_search.UseVisualStyleBackColor = false;
      this.btn_ticket_search.Click += new System.EventHandler(this.btn_ticket_search_Click);
      // 
      // btn_print_selected
      // 
      this.btn_print_selected.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_print_selected.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_print_selected.FlatAppearance.BorderSize = 0;
      this.btn_print_selected.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_print_selected.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_print_selected.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_print_selected.Image = null;
      this.btn_print_selected.IsSelected = false;
      this.btn_print_selected.Location = new System.Drawing.Point(857, 588);
      this.btn_print_selected.Name = "btn_print_selected";
      this.btn_print_selected.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_print_selected.Size = new System.Drawing.Size(155, 60);
      this.btn_print_selected.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_print_selected.TabIndex = 2;
      this.btn_print_selected.Text = "XPRINTSELECTED";
      this.btn_print_selected.UseVisualStyleBackColor = false;
      this.btn_print_selected.Click += new System.EventHandler(this.btn_print_selected_Click);
      // 
      // gb_last_operations
      // 
      this.gb_last_operations.BackColor = System.Drawing.Color.Transparent;
      this.gb_last_operations.BorderColor = System.Drawing.Color.Empty;
      this.gb_last_operations.Controls.Add(this.dgv_last_operations);
      this.gb_last_operations.CornerRadius = 1;
      this.gb_last_operations.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
      this.gb_last_operations.HeaderBackColor = System.Drawing.Color.Empty;
      this.gb_last_operations.HeaderForeColor = System.Drawing.Color.Empty;
      this.gb_last_operations.HeaderHeight = 0;
      this.gb_last_operations.HeaderSubText = null;
      this.gb_last_operations.HeaderText = null;
      this.gb_last_operations.Location = new System.Drawing.Point(12, 374);
      this.gb_last_operations.Name = "gb_last_operations";
      this.gb_last_operations.PanelBackColor = System.Drawing.Color.Empty;
      this.gb_last_operations.Size = new System.Drawing.Size(1000, 208);
      this.gb_last_operations.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NONE;
      this.gb_last_operations.TabIndex = 95;
      this.gb_last_operations.Text = "xLastOperations";
      // 
      // dgv_last_operations
      // 
      this.dgv_last_operations.AllowUserToAddRows = false;
      this.dgv_last_operations.AllowUserToDeleteRows = false;
      this.dgv_last_operations.AllowUserToResizeColumns = false;
      this.dgv_last_operations.AllowUserToResizeRows = false;
      this.dgv_last_operations.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.dgv_last_operations.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.dgv_last_operations.ColumnHeaderHeight = 35;
      this.dgv_last_operations.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
      dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      dataGridViewCellStyle1.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
      dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
      dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
      this.dgv_last_operations.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
      this.dgv_last_operations.ColumnHeadersHeight = 35;
      this.dgv_last_operations.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
      this.dgv_last_operations.CornerRadius = 10;
      this.dgv_last_operations.DefaultCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.dgv_last_operations.DefaultCellForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.dgv_last_operations.DefaultCellSelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.dgv_last_operations.DefaultCellSelectionForeColor = System.Drawing.Color.White;
      dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      dataGridViewCellStyle2.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.White;
      dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
      this.dgv_last_operations.DefaultCellStyle = dataGridViewCellStyle2;
      this.dgv_last_operations.Dock = System.Windows.Forms.DockStyle.Fill;
      this.dgv_last_operations.EnableHeadersVisualStyles = false;
      this.dgv_last_operations.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.dgv_last_operations.GridColor = System.Drawing.Color.LightGray;
      this.dgv_last_operations.HeaderBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
      this.dgv_last_operations.HeaderDefaultCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.dgv_last_operations.HeaderDefaultCellForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.dgv_last_operations.Location = new System.Drawing.Point(0, 0);
      this.dgv_last_operations.MultiSelect = false;
      this.dgv_last_operations.Name = "dgv_last_operations";
      this.dgv_last_operations.ReadOnly = true;
      this.dgv_last_operations.RowHeadersVisible = false;
      this.dgv_last_operations.RowHeadersWidth = 20;
      this.dgv_last_operations.RowTemplate.Height = 35;
      this.dgv_last_operations.RowTemplateHeight = 35;
      this.dgv_last_operations.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
      this.dgv_last_operations.Size = new System.Drawing.Size(1000, 208);
      this.dgv_last_operations.Style = WSI.Cashier.Controls.uc_DataGridView.DataGridViewStyle.WITH_ALL_ROUND_CORNERS;
      this.dgv_last_operations.TabIndex = 0;
      // 
      // btn_close
      // 
      this.btn_close.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_close.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_close.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btn_close.FlatAppearance.BorderSize = 0;
      this.btn_close.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_close.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_close.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_close.Image = null;
      this.btn_close.IsSelected = false;
      this.btn_close.Location = new System.Drawing.Point(535, 588);
      this.btn_close.Name = "btn_close";
      this.btn_close.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_close.Size = new System.Drawing.Size(155, 60);
      this.btn_close.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_close.TabIndex = 1;
      this.btn_close.Text = "XCLOSE/CANCEL";
      this.btn_close.UseVisualStyleBackColor = false;
      this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
      // 
      // btn_keyboard
      // 
      this.btn_keyboard.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.btn_keyboard.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(47)))), ((int)(((byte)(50)))));
      this.btn_keyboard.FlatAppearance.BorderSize = 0;
      this.btn_keyboard.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_keyboard.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_keyboard.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_keyboard.Image = ((System.Drawing.Image)(resources.GetObject("btn_keyboard.Image")));
      this.btn_keyboard.IsSelected = false;
      this.btn_keyboard.Location = new System.Drawing.Point(12, 588);
      this.btn_keyboard.Name = "btn_keyboard";
      this.btn_keyboard.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_keyboard.Size = new System.Drawing.Size(70, 60);
      this.btn_keyboard.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.TERTIARY;
      this.btn_keyboard.TabIndex = 96;
      this.btn_keyboard.TabStop = false;
      this.btn_keyboard.UseVisualStyleBackColor = false;
      this.btn_keyboard.Click += new System.EventHandler(this.btn_keyboard_Click);
      // 
      // btn_cancell_selected
      // 
      this.btn_cancell_selected.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_cancell_selected.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_cancell_selected.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btn_cancell_selected.FlatAppearance.BorderSize = 0;
      this.btn_cancell_selected.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_cancell_selected.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_cancell_selected.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_cancell_selected.Image = null;
      this.btn_cancell_selected.IsSelected = false;
      this.btn_cancell_selected.Location = new System.Drawing.Point(696, 588);
      this.btn_cancell_selected.Name = "btn_cancell_selected";
      this.btn_cancell_selected.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_cancell_selected.Size = new System.Drawing.Size(155, 60);
      this.btn_cancell_selected.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_cancell_selected.TabIndex = 97;
      this.btn_cancell_selected.Text = "XCANCELLSELECTED";
      this.btn_cancell_selected.UseVisualStyleBackColor = false;
      this.btn_cancell_selected.Click += new System.EventHandler(this.btn_cancell_selected_Click);
      // 
      // frm_tickets_reprint
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.btn_close;
      this.ClientSize = new System.Drawing.Size(1024, 713);
      this.Name = "frm_tickets_reprint";
      this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
      this.Style = WSI.Cashier.Controls.frm_base.BaseFormStyle.PRINCIPAL;
      this.Text = "Vouchers Reprint";
      this.pnl_data.ResumeLayout(false);
      this.gb_search.ResumeLayout(false);
      this.gb_search.PerformLayout();
      this.gb_last_operations.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.dgv_last_operations)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private Controls.uc_round_button btn_print_selected;
    private Controls.uc_round_panel gb_last_operations;
    private Controls.uc_DataGridView dgv_last_operations;
    private Controls.uc_round_button btn_close;
    private Controls.uc_round_button btn_ticket_search;
    private Controls.uc_round_panel gb_search;
    private Controls.uc_label lbl_floor_id;
    private Controls.uc_round_textbox txt_floorID;
    private Controls.uc_label lbl_ticket_number;
    private uc_terminal_filter uc_prov_terminals;
    private NumericTextBox txt_ticket_number;
    private Controls.uc_round_button btn_keyboard;
    private Controls.uc_round_button btn_cancell_selected;

  }
}