namespace WSI.Cashier.TITO
{
    partial class uc_ticket_terminal
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
      this.lbl_separator_1 = new System.Windows.Forms.Label();
      this.gb_ticket_source = new WSI.Cashier.Controls.uc_round_panel();
      this.uc_prov_terminals = new WSI.Cashier.uc_terminal_filter();
      this.gb_tickets_list = new WSI.Cashier.Controls.uc_round_panel();
      this.dgv_last_operations = new WSI.Cashier.Controls.uc_DataGridView();
      this.lbl_Terminal = new WSI.Cashier.Controls.uc_label();
      this.lbl_provider = new WSI.Cashier.Controls.uc_label();
      this.lbl_cards_title = new WSI.Cashier.Controls.uc_label();
      this.gb_ticket_source.SuspendLayout();
      this.gb_tickets_list.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dgv_last_operations)).BeginInit();
      this.SuspendLayout();
      // 
      // lbl_separator_1
      // 
      this.lbl_separator_1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_separator_1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(95)))), ((int)(((byte)(95)))));
      this.lbl_separator_1.Location = new System.Drawing.Point(-3, 60);
      this.lbl_separator_1.Name = "lbl_separator_1";
      this.lbl_separator_1.Size = new System.Drawing.Size(885, 1);
      this.lbl_separator_1.TabIndex = 88;
      // 
      // gb_ticket_source
      // 
      this.gb_ticket_source.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.gb_ticket_source.BackColor = System.Drawing.Color.Transparent;
      this.gb_ticket_source.BorderColor = System.Drawing.Color.Empty;
      this.gb_ticket_source.Controls.Add(this.uc_prov_terminals);
      this.gb_ticket_source.CornerRadius = 10;
      this.gb_ticket_source.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_ticket_source.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gb_ticket_source.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_ticket_source.HeaderHeight = 35;
      this.gb_ticket_source.HeaderSubText = null;
      this.gb_ticket_source.HeaderText = "XTICKETLIST";
      this.gb_ticket_source.Location = new System.Drawing.Point(40, 84);
      this.gb_ticket_source.Name = "gb_ticket_source";
      this.gb_ticket_source.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_ticket_source.Size = new System.Drawing.Size(584, 239);
      this.gb_ticket_source.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.gb_ticket_source.TabIndex = 1025;
      this.gb_ticket_source.Text = "xTicketsList";
      // 
      // uc_prov_terminals
      // 
      this.uc_prov_terminals.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.uc_prov_terminals.BackColor = System.Drawing.Color.Transparent;
      this.uc_prov_terminals.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.uc_prov_terminals.Location = new System.Drawing.Point(-1, 35);
      this.uc_prov_terminals.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
      this.uc_prov_terminals.Name = "uc_prov_terminals";
      this.uc_prov_terminals.Size = new System.Drawing.Size(586, 205);
      this.uc_prov_terminals.TabIndex = 0;
      this.uc_prov_terminals.TerminalOrProviderChanged += new WSI.Cashier.uc_terminal_filter.TerminalOrProviderChangedEventHandler(this.uc_terminal_filter_TerminalOrProviderChanged);
      // 
      // gb_tickets_list
      // 
      this.gb_tickets_list.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.gb_tickets_list.BackColor = System.Drawing.Color.Transparent;
      this.gb_tickets_list.BorderColor = System.Drawing.Color.Empty;
      this.gb_tickets_list.Controls.Add(this.dgv_last_operations);
      this.gb_tickets_list.CornerRadius = 10;
      this.gb_tickets_list.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_tickets_list.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gb_tickets_list.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_tickets_list.HeaderHeight = 35;
      this.gb_tickets_list.HeaderSubText = null;
      this.gb_tickets_list.HeaderText = "XTICKETLIST";
      this.gb_tickets_list.Location = new System.Drawing.Point(40, 371);
      this.gb_tickets_list.Name = "gb_tickets_list";
      this.gb_tickets_list.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_tickets_list.Size = new System.Drawing.Size(804, 321);
      this.gb_tickets_list.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.gb_tickets_list.TabIndex = 1023;
      this.gb_tickets_list.Text = "xTicketsList";
      // 
      // dgv_last_operations
      // 
      this.dgv_last_operations.AllowUserToAddRows = false;
      this.dgv_last_operations.AllowUserToDeleteRows = false;
      this.dgv_last_operations.AllowUserToResizeColumns = false;
      this.dgv_last_operations.AllowUserToResizeRows = false;
      this.dgv_last_operations.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.dgv_last_operations.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.dgv_last_operations.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.dgv_last_operations.ColumnHeaderHeight = 35;
      this.dgv_last_operations.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
      dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      dataGridViewCellStyle3.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
      dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
      dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
      this.dgv_last_operations.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
      this.dgv_last_operations.ColumnHeadersHeight = 35;
      this.dgv_last_operations.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
      this.dgv_last_operations.CornerRadius = 10;
      this.dgv_last_operations.DefaultCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.dgv_last_operations.DefaultCellForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.dgv_last_operations.DefaultCellSelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.dgv_last_operations.DefaultCellSelectionForeColor = System.Drawing.Color.White;
      dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      dataGridViewCellStyle4.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      dataGridViewCellStyle4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.White;
      dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
      this.dgv_last_operations.DefaultCellStyle = dataGridViewCellStyle4;
      this.dgv_last_operations.EnableHeadersVisualStyles = false;
      this.dgv_last_operations.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.dgv_last_operations.GridColor = System.Drawing.Color.LightGray;
      this.dgv_last_operations.HeaderBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
      this.dgv_last_operations.HeaderDefaultCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.dgv_last_operations.HeaderDefaultCellForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.dgv_last_operations.HeaderImages = null;
      this.dgv_last_operations.Location = new System.Drawing.Point(-1, 35);
      this.dgv_last_operations.MultiSelect = false;
      this.dgv_last_operations.Name = "dgv_last_operations";
      this.dgv_last_operations.ReadOnly = true;
      this.dgv_last_operations.RowHeadersVisible = false;
      this.dgv_last_operations.RowHeadersWidth = 20;
      this.dgv_last_operations.RowTemplate.Height = 30;
      this.dgv_last_operations.RowTemplateHeight = 35;
      this.dgv_last_operations.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
      this.dgv_last_operations.Size = new System.Drawing.Size(805, 286);
      this.dgv_last_operations.Style = WSI.Cashier.Controls.uc_DataGridView.DataGridViewStyle.WITH_BOTTOM_ROUND_BORDERS;
      this.dgv_last_operations.TabIndex = 0;
      // 
      // lbl_Terminal
      // 
      this.lbl_Terminal.AutoSize = true;
      this.lbl_Terminal.BackColor = System.Drawing.Color.Transparent;
      this.lbl_Terminal.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_Terminal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_Terminal.Location = new System.Drawing.Point(344, 336);
      this.lbl_Terminal.Name = "lbl_Terminal";
      this.lbl_Terminal.Size = new System.Drawing.Size(78, 19);
      this.lbl_Terminal.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_Terminal.TabIndex = 1022;
      this.lbl_Terminal.Text = "xTerminal";
      // 
      // lbl_provider
      // 
      this.lbl_provider.AutoSize = true;
      this.lbl_provider.BackColor = System.Drawing.Color.Transparent;
      this.lbl_provider.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_provider.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_provider.Location = new System.Drawing.Point(42, 336);
      this.lbl_provider.Name = "lbl_provider";
      this.lbl_provider.Size = new System.Drawing.Size(76, 19);
      this.lbl_provider.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_provider.TabIndex = 1021;
      this.lbl_provider.Text = "xProvider";
      // 
      // lbl_cards_title
      // 
      this.lbl_cards_title.BackColor = System.Drawing.Color.Transparent;
      this.lbl_cards_title.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_cards_title.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_cards_title.Image = global::WSI.Cashier.Properties.Resources.ticketsTito_header;
      this.lbl_cards_title.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.lbl_cards_title.Location = new System.Drawing.Point(11, 20);
      this.lbl_cards_title.Name = "lbl_cards_title";
      this.lbl_cards_title.Size = new System.Drawing.Size(823, 33);
      this.lbl_cards_title.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_cards_title.TabIndex = 90;
      this.lbl_cards_title.Text = "xTickets por Terminal";
      // 
      // uc_ticket_terminal
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.SystemColors.Control;
      this.Controls.Add(this.gb_ticket_source);
      this.Controls.Add(this.lbl_separator_1);
      this.Controls.Add(this.gb_tickets_list);
      this.Controls.Add(this.lbl_Terminal);
      this.Controls.Add(this.lbl_provider);
      this.Controls.Add(this.lbl_cards_title);
      this.Name = "uc_ticket_terminal";
      this.Size = new System.Drawing.Size(884, 740);
      this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.uc_terminal_terminal_KeyPress);
      this.gb_ticket_source.ResumeLayout(false);
      this.gb_tickets_list.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.dgv_last_operations)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private uc_terminal_filter uc_prov_terminals;
    private WSI.Cashier.Controls.uc_label lbl_cards_title;
    private WSI.Cashier.Controls.uc_DataGridView dgv_last_operations;
    private WSI.Cashier.Controls.uc_label lbl_provider;
    private WSI.Cashier.Controls.uc_label lbl_Terminal;
    private Controls.uc_round_panel gb_tickets_list;
    private System.Windows.Forms.Label lbl_separator_1;
    private Controls.uc_round_panel gb_ticket_source;
  }
}
