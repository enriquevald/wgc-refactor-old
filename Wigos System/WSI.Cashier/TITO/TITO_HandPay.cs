//------------------------------------------------------------------------------
// Copyright © 2007-2014 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: TITO_HandPay.cs
// 
//   DESCRIPTION: Declare all functions and procedures related to handpay in Tito
//
//        AUTHOR: David Rigal
//
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 30-JAN-2014 DRV    First release.
// 03-NOV-2014 MPO    WIG-1620: Show incorrect message when cancel payment
// 19-DEC-2014 OPC    WIG-1871: Can't pay handpay with taxes with pay order.
// 02-FEB-2015 MPO    WIG-1969: System crash when printing "constancias"
// 07-APR-2015 MPO    WIG-2195: Handpay vouchers generated incorrectly
// 11-NOV-2015 FOS    Product Backlog Item: 3709 Payment tickets & handpays
// 23-NOV-2015 FOS    Bug 6771:Payment tickets & handpays
// 01-DEC-2015 FOS    Fixed Bug 7205: Protected taxWaiverMode and renamed function ApplyTaxCollection to EnableTitoTaxWaiver
// 02-DEC-2015 FOS    Fixed Bug 7208: Incorrect Tax Value on Handpays
// 01-MAR-2016 EOR    Product Backlog Item 7402:SAS20: Add Comments HandPays
// 04-APR-2016 RAB    Bug 11325: Cashier: Failed to pay a manual payment when you click 'No' in the pop-up of generation of document of record
// 19-SEP-2016 FGB    PBI 15985: Exped System: Cashier: Check if the payment/sale can be done
// 27-APR-2017 DHA    Bug 26735:WTA-350: wait till cut manual paper on printer
// 06-JUL-2017 DHA    WIGOS-3419 - Manual HP's with no taxes are having taxes applied when cashing them
// 09-AUG-2017 ATB    Bug 29252: The voucher of a paid "Handpay" is displaying "Anónimo" in the field "Cliente" when the user already introduced their account (WIGOS-4288, WIGOS-4289)
// 10-OCT-2017 JML    PBI 30023:WIGOS-4068 Payment threshold registration - Transaction information
// 30-OCT-2017 RAB    Bug 30446:WIGOS-6052 Threshold: Screen order is incorrect in HP and dispute (paying with PO) exceeding threshold and without permissions
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;
using WSI.Common;
using WSI.Common.TITO;
using System.Collections;

namespace WSI.Cashier
{
  public class TITO_HandPay
  {
    #region Attributes

    //private frm_yesno form_yes_no;
    private Form m_parent;
    private Int64 m_account_id;

    #endregion // Attributes

    #region Constructor

    public TITO_HandPay(Form Parent)
    {
      m_parent = Parent;
    }

    #endregion

    #region Enums

    #endregion

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Do the handpay for Tito 
    // 
    //  PARAMS :
    //      - INPUT :
    //          - Handpay : Handpay Data
    //          - Handpay Date : Handpay DateTime
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True : Handpay done
    //      - False : Problem during process
    // 
    public Boolean PayHandpay(HandPay Handpay, DateTime HandpayDate, Boolean IsApplyTaxes, out Boolean PaymentCancelled)
    {
      Currency _amount_to_subtract;
      Boolean _check_redeem;
      String _error_str;
      Boolean _witholding_required;
      Boolean[] _witholding_required_docs;
      List<ProfilePermissions.CashierFormFuncionality> _permissions_list;
      WithholdData _withhold;
      CardData _related_card;
      Currency _amount;
      PaymentThresholdAuthorization _payment_threshold_authorization;
      PaymentAndRechargeThresholdData _payment_and_recharge_threshold_data;
      Boolean _process_continue;

      Boolean _payment_order_enabled = false;
      Currency _payment_order_min_amount = 0;
      PaymentOrder _payment_order;

      Currency _cashier_current_balance;
      Boolean _is_apply_tax;

      ArrayList _voucher_list;

      _is_apply_tax = IsApplyTaxes;
      _related_card = Handpay.Card;
      _amount = Handpay.Amount;
      PaymentCancelled = false;
      _voucher_list = new ArrayList();
      _process_continue = false;
      _payment_and_recharge_threshold_data = new PaymentAndRechargeThresholdData();

      if (this.AccountId > 0)
      {
        _related_card.AccountId = this.AccountId;
      }

      if (_related_card.AccountId == 0)
      {
        return false;
      }


      _withhold = null;

      Handpays.HPAmountsCalculated _calculated;
      Handpays.CalculateCashAmountHandpay(Handpay.Status, Handpay.Amount, Handpay.TaxBaseAmount, Handpay.Type, _related_card, _is_apply_tax, false, out _calculated);

      if (!Witholding.CheckWitholding(_calculated.CashRedeem.prize, out _witholding_required, out _witholding_required_docs))
      {
        return false;
      }

      _payment_order_enabled = PaymentOrder.IsEnabled();
      _payment_order_min_amount = PaymentOrder.MinAmount();

      // Check out all the required permissions just once
      _permissions_list = new List<ProfilePermissions.CashierFormFuncionality>();

      if (_witholding_required)
      {
        _permissions_list.Add(ProfilePermissions.CashierFormFuncionality.WitholdDocument);

        // Check if current user is an authorized user
        if (!ProfilePermissions.CheckPermissionList(_permissions_list,
                                                    ProfilePermissions.TypeOperation.RequestPasswd,
                                                    m_parent,
                                                    0,
                                                    out _error_str))
        {
          return false;
        }

        // Get cashier current balance
        try
        {
          using (DB_TRX _db_trx = new DB_TRX())
          {
            _cashier_current_balance = CashierBusinessLogic.UpdateSessionBalance(_db_trx.SqlTransaction, Cashier.SessionId, 0);
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);

          return false;
        }

        // If    ( order payment disabled  AND   current redeem exceds cashier balance )
        //    OR ( order payment enabled   AND   current redeem exceds cashier balance AND   current redeem < min_payment_order  )
        if ((!_payment_order_enabled && _calculated.CashRedeem.TotalPaid > _cashier_current_balance)
            || (_payment_order_enabled && _calculated.CashRedeem.TotalPaid > _cashier_current_balance && _calculated.CashRedeem.TotalPaid < _payment_order_min_amount))
        {
          frm_message.Show(Resource.String("STR_FRM_AMOUNT_INPUT_MSG_AMOUNT_EXCEED_BANK"),
                           Resource.String("STR_APP_GEN_MSG_ERROR"),
                           MessageBoxButtons.OK,
                           MessageBoxIcon.Error,
                           m_parent);

          //this.RestoreParentFocus();

          return false;
        }

        // Enter required data to generate the Witholding documents
        if (!CashierBusinessLogic.EnterWitholdingData(m_parent, _related_card, out _withhold))
        {
          // RAB 04-APR-2016: Bug 11325
          PaymentCancelled = true;

          // Data entering process was cancelled => no further process
          return false;
        }

      } // if CheckWitholding

      using (frm_amount_input _frm_credit = new frm_amount_input())
      {
        using (frm_yesno _frm_yes_no = new frm_yesno())
        {
          _frm_yes_no.Opacity = 0.6;
          _frm_yes_no.Show(m_parent);

          // MPO 11-NOV-2014 Needed to show in cashout voucher (Total).
          _related_card.CurrentBalance += _amount;
          try
          {
            PaymentCancelled = !(_frm_credit.ShowForHandPay(Resource.String("STR_UC_CARD_BTN_CREDIT_REDEEM_TOTAL"),
                                                            out _amount_to_subtract,
                                                            out _check_redeem,
                                                            out _payment_threshold_authorization, 
                                                            VoucherTypes.CardRedeem,
                                                            _related_card,
                                                            _amount,
                                                            Handpay,
                                                            CASH_MODE.TOTAL_REDEEM,
                                                            _frm_yes_no,
                                                            _is_apply_tax));
          }
          finally
          {
            // MPO 11-NOV-2014 Needed to show in cashout voucher (Total). 
            // MPO 11-NOV-2014 Restore CurrentBalance because will be modified in RegisterOrCancel.
            //_related_card.CurrentBalance -= _amount;
            if (PaymentCancelled)
            {
              // MPO 07-APR-2015 WIG-2195: When handpay payment is cancelled should be "clean" the "current balance"
              _related_card.CurrentBalance -= _amount;
            }
          }
        }
      }

      if (PaymentCancelled)
      {
        return false;
      }

      _payment_order = new PaymentOrder();
      if (_check_redeem)
      {
        // Enter required data to generate the Witholding documents
        if (!CashierBusinessLogic.EnterOrderPaymentData(m_parent, _related_card, _calculated.CashRedeem.TotalPaid, _calculated.CashRedeem.prize, _withhold, out _payment_order))
        {
          PaymentCancelled = true;

          // Data entering process was cancelled => no further process
          return false;
        }
      }

      Handpays.HandpayRegisterOrCancelParameters _input;
      Int64 _operation_id;
      Int16 _handpay_error_msg;

      _operation_id = 0;

      // Redeem the credit
      using (DB_TRX _db_trx = new DB_TRX())
      {
        _input = new Handpays.HandpayRegisterOrCancelParameters();
        _input.CancelHandpay = false;

        _input.HandpayType = Handpay.Type;
        _input.HandpayDate = HandpayDate;
        _input.HandpayAmount = _amount;

        _input.CardData = _related_card;
        _input.TerminalId = Handpay.TerminalId;
        _input.TerminalProvider = Handpay.TerminalProvider;
        _input.TerminalName = Handpay.TerminalName;
        _input.SessionPlayedAmount = Handpay.SessionPlayedAmount;
        _input.DenominationStr = Handpay.DenominationStr;
        _input.DenominationValue = Handpay.DenominationValue;
        _input.ProgressiveId = Handpay.ProgressiveId;
        _input.HandpayId = Handpay.HandpayId;
        _input.Level = Handpay.Level;
        _input.Status = Handpay.Status;
        _input.TaxBaseAmount = Handpay.TaxBaseAmount;
        _input.TaxAmount = Handpay.TaxAmount;
        _input.TaxBasePct = Handpay.TaxPct;
        _input.AccountOperationReasonId = Handpay.AccountOperationReasonId;
        _input.AccountOperationComment = Handpay.AccountOperationComment;
        _input.IsApplyTaxes = _is_apply_tax;
        _input.HandpayAmt0 = Handpay.Amt0;
        _input.HandpayCur0 = Handpay.Cur0;
        _input.HandpayAmt1 = Handpay.Amt1;
        _input.HandpayCur1 = Handpay.Cur1;
        _input.AccountOperationCommentHandPay = Handpay.AccountOperationCommentHandPays; //EOR 01-MAR-2016 Add Comments HandPays

        if (!Handpays.RegisterOrCancel(_input, Cashier.CashierSessionInfo(), true, OperationCode.HANDPAY, ref _operation_id, out _handpay_error_msg, _db_trx.SqlTransaction))
        {
          try
          {
            Handpay.HandpayErrorMsg = (Handpays.HANDPAY_MSG)_handpay_error_msg;
          }
          catch
          {
            Handpay.HandpayErrorMsg = Handpays.HANDPAY_MSG.REGISTER_ERROR;
          }
          return false;
        }

        TYPE_SITE_JACKPOT_WINNER _jackpot_info;

        if (_input.HandpayType == HANDPAY_TYPE.SITE_JACKPOT)
        {
          _jackpot_info = new TYPE_SITE_JACKPOT_WINNER();

          _jackpot_info.awarded_date = _input.HandpayDate;
          _jackpot_info.awarded_on_terminal_id = _input.TerminalId;
          _jackpot_info.amount = _input.HandpayAmount;
          _jackpot_info.awarded_to_account_id = _input.CardData.AccountId;

          CardData.UpdateJackpotNotifiedStatus(_jackpot_info, SITE_JACKPOT_NOTIFICATION_STATUS.Notified, _db_trx.SqlTransaction);
        }

        if (!CashierBusinessLogic.DB_CardCreditRedeem(_operation_id, _related_card.AccountId,
                                                      _withhold, _payment_order,
                                                      _amount, CASH_MODE.TOTAL_REDEEM,
                                                      CreditRedeemSourceOperationType.PAY_HANDPAY, OperationCode.HANDPAY, _related_card,
                                                      _db_trx.SqlTransaction,
                                                      out _error_str,
                                                      _is_apply_tax,
                                                      out _voucher_list))
        {
          Log.Error("PayHandpay: DB_CardCreditRedeem. Error subtracting all credit to card.");

          return false;
        }

        _payment_threshold_authorization = new PaymentThresholdAuthorization(_calculated.CashRedeem.TotalRedeemed, _related_card, _payment_and_recharge_threshold_data, PaymentThresholdAuthorization.PaymentThresholdAuthorizationOperation.Output, m_parent, out _process_continue);
        if (_process_continue && _payment_threshold_authorization != null && _payment_threshold_authorization.OutputThreshold > 0 && _amount >= _payment_threshold_authorization.OutputThreshold)
        {
          if (_payment_order != null)
        {
          _payment_threshold_authorization.PaymentAndRechargeThresholdData.CashAmount = _payment_order.CashPayment;
          if (_payment_order.CheckPayment > 0)
          {
            _payment_threshold_authorization.PaymentAndRechargeThresholdData.TransactionType = CurrencyExchangeType.CHECK;
            _payment_threshold_authorization.PaymentAndRechargeThresholdData.TransactionSubType = _payment_order.CheckType;
          }
          }
          _payment_threshold_authorization.SaveThresholdOperation(_operation_id, OperationCode.HANDPAY, _db_trx.SqlTransaction);
        }                

        if(_process_continue)
        {
          _db_trx.Commit();
        }
        else
        {
          PaymentCancelled = true;
          return false;
        }        
      }

      if (CashierBusinessLogic.GLB_OperationIdForPrintDocs > 0)
      {
        // Select the printer and print the Witholding documents
        PrintingDocuments.PrintDialogDocuments(CashierBusinessLogic.GLB_OperationIdForPrintDocs);
      }

      if (_input.Voucher != null)
      {
        _voucher_list.Add(_input.Voucher);
      }

      VoucherPrint.Print(_voucher_list);

      return true;
    }

    public Int64 AccountId
    {
      set
      {
        this.m_account_id = value;
      }
      get
      {
        return this.m_account_id;
      }
    }
    #endregion
  }

}