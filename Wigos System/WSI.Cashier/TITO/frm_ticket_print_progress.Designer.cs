namespace WSI.Cashier.TITO
{
  partial class frm_ticket_print_progress
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.btn_cancel_print = new WSI.Cashier.Controls.uc_round_button();
      this.pb_print_tickets = new System.Windows.Forms.ProgressBar();
      this.bw_print_tickets = new System.ComponentModel.BackgroundWorker();
      this.lbl_status_bar_msg = new WSI.Cashier.Controls.uc_label();
      this.pnl_data.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.Controls.Add(this.lbl_status_bar_msg);
      this.pnl_data.Controls.Add(this.btn_cancel_print);
      this.pnl_data.Controls.Add(this.pb_print_tickets);
      this.pnl_data.Size = new System.Drawing.Size(509, 140);
      // 
      // btn_cancel_print
      // 
      this.btn_cancel_print.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.btn_cancel_print.FlatAppearance.BorderSize = 0;
      this.btn_cancel_print.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_cancel_print.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_cancel_print.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.btn_cancel_print.Image = null;
      this.btn_cancel_print.Location = new System.Drawing.Point(166, 60);
      this.btn_cancel_print.Name = "btn_cancel_print";
      this.btn_cancel_print.Size = new System.Drawing.Size(155, 60);
      this.btn_cancel_print.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_cancel_print.TabIndex = 4;
      this.btn_cancel_print.Text = "XCANCELPRINT";
      this.btn_cancel_print.UseVisualStyleBackColor = false;
      this.btn_cancel_print.Click += new System.EventHandler(this.btn_cancel_print_Click);
      // 
      // pb_print_tickets
      // 
      this.pb_print_tickets.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.pb_print_tickets.Location = new System.Drawing.Point(12, 19);
      this.pb_print_tickets.Name = "pb_print_tickets";
      this.pb_print_tickets.Size = new System.Drawing.Size(485, 23);
      this.pb_print_tickets.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
      this.pb_print_tickets.TabIndex = 0;
      // 
      // bw_print_tickets
      // 
      this.bw_print_tickets.WorkerReportsProgress = true;
      this.bw_print_tickets.WorkerSupportsCancellation = true;
      this.bw_print_tickets.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bw_print_tickets_DoWork);
      this.bw_print_tickets.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bw_print_tickets_ProgressChanged);
      this.bw_print_tickets.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bw_print_tickets_RunWorkerCompleted);
      // 
      // lbl_status_bar_msg
      // 
      this.lbl_status_bar_msg.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_status_bar_msg.BackColor = System.Drawing.Color.Transparent;
      this.lbl_status_bar_msg.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_status_bar_msg.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.lbl_status_bar_msg.Location = new System.Drawing.Point(349, 94);
      this.lbl_status_bar_msg.Name = "lbl_status_bar_msg";
      this.lbl_status_bar_msg.Size = new System.Drawing.Size(148, 26);
      this.lbl_status_bar_msg.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_status_bar_msg.TabIndex = 104;
      this.lbl_status_bar_msg.Text = "xStatusBarMsg";
      this.lbl_status_bar_msg.TextAlign = System.Drawing.ContentAlignment.TopRight;
      // 
      // frm_ticket_print_progress
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(509, 195);
      this.HeaderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.Name = "frm_ticket_print_progress";
      this.ShowCloseButton = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Style = WSI.Cashier.Controls.frm_base.BaseFormStyle.PRINCIPAL;
      this.Text = "TITO Print Tickets";
      this.pnl_data.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private WSI.Cashier.Controls.uc_round_button btn_cancel_print;
    private WSI.Cashier.Controls.uc_label lbl_status_bar_msg;
    private System.Windows.Forms.ProgressBar pb_print_tickets;
    private System.ComponentModel.BackgroundWorker bw_print_tickets;

  }
}