//------------------------------------------------------------------------------
// Copyright � 2007-2009 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_container.cs
// 
//   DESCRIPTION: Implements the uc_card_reader user control for card track data input (uc_card_reader)
//
//        AUTHOR: AJQ
// 
// CREATION DATE: 29-AUG-2007
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 29-AUG-2007 AJQ     First release.
// 18-APR-2012 MPO     Generate activity when read the card
// 03-MAY-2012 JCM     Fixed Bug #265: Focus after user session expires
// 20-SEP-2013 NMR     Setting new resources
// 13-JAN-2014 DHA     Fixed Bug #WIGOSTITO-972: When user is entering a ticket number, the invalid ticket message is hidden
// 26-SEP-2014 DRV     Added KeyboardMessageEvent.
// 10-NOV 2015 FAV     Product Backlog Item 5807: New design
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using WSI.Common;

namespace WSI.Cashier.TITO
{
  public partial class uc_ticket_reader : UserControl
  {
    public delegate void ValidationNumberReadEventHandler(String ValidationNumber, ref Boolean IsValid, out String ErrorStr);
    public delegate void ValidationNumberReadingHandler();

    public event ValidationNumberReadEventHandler OnValidationNumberReadEvent;
    public event ValidationNumberReadingHandler OnValidationNumberReadingEvent;

    private const Int32 VALIDATION_NUMBER_LENGTH = 20;

    private Boolean m_External_message;
    private Boolean is_kbd_msg_filter_event = false;
    private String received_track_data;

    #region Class Atributes

    private Int32 tick_last_change = 0;
    private Boolean all_data_is_entered = false;
    private Boolean ignore_additional_numbers = false;

    #endregion

    #region Constructor

    public uc_ticket_reader()
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[UC LOAD] uc_ticket_reader", Log.Type.Message);

      InitializeComponent();

      InitializeControlResources();

      if (!this.DesignMode)
      {
        tmr_clear.Enabled = true;
        ClearTrackNumber();
        txt_validation_number.MaxLength = VALIDATION_NUMBER_LENGTH;
      }
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Initialize control resources. (NLS string, images, etc.) 
    /// </summary>
    public void InitializeControlResources()
    {
      // - NLS Strings:
      //   - Title

      //   - Labels
      lbl_swipe_ticket.Text = Resource.String("STR_UC_TICKET_VALID_NUMBER");
      lbl_invalid_ticket.Text = Resource.String("STR_UC_TICKET_NOT_FOUND");
      m_External_message = false;

    } // InitializeControlResources

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void pictureBox1_Click(object sender, EventArgs e)
    {
      ClearTrackNumber();
      txt_validation_number.Focus();
    } // pictureBox1_Click

    /// <summary>
    /// Clear input for track number actions.
    /// </summary>
    public void ClearTrackNumber()
    {
      txt_validation_number.Text = "";
      tick_last_change = 0;

      lbl_invalid_ticket.Visible = false;
      all_data_is_entered = false;
      ignore_additional_numbers = false;

    } // ClearTrackNumber

    /// <summary>
    /// Checks is the input is completely numeric.
    /// </summary>
    /// <param name="TrackNumber"></param>
    /// <returns></returns>
    private Boolean PartialTrackNumberIsValid(String TrackNumber)
    {
      Byte[] chars;

      chars = null;

      try
      {
        chars = Encoding.ASCII.GetBytes(TrackNumber);
        foreach (byte b in chars)
        {
          if (b < '0')
          {
            return false;
          }
          if (b > '9')
          {
            return false;
          }
        }
      }
      finally
      {
        chars = null;
      }

      return true;
    } // PartialTrackNumberIsValid

    /// <summary>
    /// Track number checking.
    /// </summary>
    /// <param name="TrackNumber"></param>
    /// <returns></returns>
    private Boolean TrackNumberIsValid(String TrackNumber)
    {
      if (TrackNumber.Length == VALIDATION_NUMBER_LENGTH)
      {
        if (PartialTrackNumberIsValid(TrackNumber))
        {
          String _internal_track;
          int _card_type;

          if (WSI.Common.CardNumber.TrackDataToInternal(TrackNumber, out _internal_track, out _card_type))
          {
            if (_card_type == 0
                || _card_type == 1)
            {
              return true;
            }
          }
        }
      }
      return false;
    } // TrackNumberIsValid

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void txt_track_number_TextChanged(object sender, EventArgs e)
    {
      String track_number;
      //if card readed meanwhile disconnected message, do nothing
      if (WSI.Common.WGDB.ConnectionState != ConnectionState.Open
          && WSI.Common.WGDB.ConnectionState != ConnectionState.Executing
          && WSI.Common.WGDB.ConnectionState != ConnectionState.Fetching)
      {
        ClearTrackNumber();
        return;
      }

      // Variables initialization
      tick_last_change = 0;
      if (is_kbd_msg_filter_event)
      {
        track_number = received_track_data;
      }
      else
      {
        track_number = txt_validation_number.Text;
      }
      all_data_is_entered = (track_number.Length >= VALIDATION_NUMBER_LENGTH);

      if (track_number.Length > 0)
      {
        // DHA 13-JAN-2014: When user is entering a ticket number, the invalid ticket message is hidden
        lbl_invalid_ticket.Visible = false;
        if (OnValidationNumberReadingEvent != null)
        {
          OnValidationNumberReadingEvent();
        }
      }
    } // txt_track_number_TextChanged

    /// <summary>
    /// Key pressed management.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void txt_track_number_KeyPress(object sender, KeyPressEventArgs e)
    {
      String track_number;
      Boolean is_valid;
      String _error_str;

      tick_last_change = 0;
      _error_str = "";

      if (is_kbd_msg_filter_event)
      {
        track_number = received_track_data;
        is_kbd_msg_filter_event = false;
      }
      else
      {
        track_number = txt_validation_number.Text;
      }

      switch (e.KeyChar)
      {
        case '%':
          // Ignore track start/end markers
          ignore_additional_numbers = false;
          e.KeyChar = '\0';
          break;

        case ';':
          // Ignore track start/end markers
          ignore_additional_numbers = true;
          e.KeyChar = '\0';
          break;

        case '?':
          // Ignore track start/end markers
          e.KeyChar = '\0';
          break;

        case '\r':
          if (!ignore_additional_numbers)
          {
            //TODOif (all_data_is_entered)
            {
              if (track_number.Length <= VALIDATION_NUMBER_LENGTH)
              {
                is_valid = true;
                //TODO: is_valid = TrackNumberIsValid(track_number);
                if (is_valid)
                {
                  tmr_clear.Enabled = false;
                  if (OnValidationNumberReadEvent != null)
                  {
                    try
                    {
                      WSI.Common.Users.SetLastAction("ReadTicket", "");
                      OnValidationNumberReadEvent(track_number, ref is_valid, out _error_str);
                    }
                    catch
                    {
                      is_valid = false;
                    }
                  }
                  tmr_clear.Enabled = true;
                }

                if (!is_valid) 
                {
                  txt_validation_number.SelectAll();

                  lbl_invalid_ticket.Text = !String.IsNullOrEmpty(_error_str) ? _error_str : Resource.String("STR_UC_TICKET_NOT_FOUND");
                  lbl_invalid_ticket.Visible = true;

                }
                else
                {
                  if (!m_External_message)
                  {
                    ClearTrackNumber();
                  }
                  m_External_message = false;
                }
              }
            }
          }
          else
          {
            // Allow processing additional keystrokes
            ignore_additional_numbers = false;
          }

          e.Handled = true;
          break;

        default:
          if (ignore_additional_numbers)
          {
            // All keystrokes must be ignored until a valid sequence starts
            e.KeyChar = '\0';
          }
          break;
      } // switch
    } // txt_track_number_KeyPress

    public void TicketMessage(String ResourceString)
    {
      lbl_invalid_ticket.Text = ResourceString;
      lbl_invalid_ticket.Visible = true;
      m_External_message = true;
    }

    /// <summary>
    /// Timer to clear card input, if key is not pressed in five seconds.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void tmr_clear_Tick(object sender, EventArgs e)
    {
      tick_last_change += 1;
      if (tick_last_change > 20) // 5 seconds
      {
        ClearTrackNumber();
        tick_last_change = 0;
      }
    } // tmr_clear_Tick

    private void BarCodeReceivedEvent(KbdMsgEvent e, Object data)
    {
      Barcode _barcode;
      KeyPressEventArgs _event_args;

      if (e == KbdMsgEvent.EventBarcode)
      {
        _barcode = (Barcode)data;
        received_track_data = _barcode.ExternalCode;

        is_kbd_msg_filter_event = true;
        txt_track_number_TextChanged(this, new EventArgs());
        _event_args = new KeyPressEventArgs('\r');
        txt_track_number_KeyPress(this, _event_args);
      }
    }

    #endregion

    public void ShowMessageBottom()
    {
      lbl_invalid_ticket.Location = new System.Drawing.Point(txt_validation_number.Location.X, txt_validation_number.Location.Y + 45);
    } // ShowMessageBottom

    private void uc_ticket_reader_VisibleChanged(object sender, EventArgs e)
    {
      if (this.Visible)
      {
        KeyboardMessageFilter.RegisterHandler(this, BarcodeType.FilterAnyTito, BarCodeReceivedEvent);
      }
    }
  }
}
