//------------------------------------------------------------------------------
// Copyright � 2014 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_account_promos.cs
// 
//   DESCRIPTION: 
//
//        AUTHOR: LEM
// 
// CREATION DATE: 07-JAN-2014
//
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 07-JAN-2013 LEM    First release.
// 12-MAR-2014 LEM    Fixed Bug WIGOSTITO-1139: Disable lock promotions for TITO.
// 10-NOV-2015 ETP    Product Backlog Item 6021 - New cashier design - frm_account_promos.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using WSI.Common;
using WSI.Common.TITO;
using WSI.Cashier.TITO;
using System.IO;
using System.Drawing.Text;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Globalization;
using WSI.Cashier.Controls;

namespace WSI.Cashier
{
  public partial class frm_account_promos : frm_base
  {
    #region Attributes

    private frm_yesno form_yes_no;
    private Int64 m_account_id;
    private frm_container m_parent;


    #endregion // Attributes

    #region Constructor

    public frm_account_promos()
    {
      InitializeComponent();
      InitializeControlResources();

      form_yes_no = new frm_yesno();
      form_yes_no.Opacity = 0.6;
    }

    #endregion // Constructor

    #region Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Initialize control resources (NLS strings, images, etc).
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private void InitializeControlResources()
    {
      // labels 
     this.FormTitle = Resource.String("STR_FRM_ACCOUNT_PROMOS");

    } // InitializeControlResources

    #endregion // Private Methods

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Init and Show the form.
    //
    //  PARAMS :
    //      - INPUT :
    //          - ParentForm
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public void Show(Int64 AccountId, frm_container Parent)
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_account_promos", Log.Type.Message);

      try
      {
        m_account_id  = AccountId;
        m_parent = Parent;

        form_yes_no.Show(Parent);
        this.ShowDialog(form_yes_no);
        form_yes_no.Hide();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } // Show

    //------------------------------------------------------------------------------
    // PURPOSE : Dispose yes_no form.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    new public void Dispose()
    {
      form_yes_no.Dispose();
      form_yes_no = null;

      base.Dispose();
    } // Dispose

    #endregion // Public Methods

    #region Buttons

    //------------------------------------------------------------------------------
    // PURPOSE : Handle the click on the Close/Cancel button.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private void btn_close_Click(object sender, EventArgs e)
    {
      // Close Button

      Misc.WriteLog("[FORM CLOSE] frm_account_promos (close)", Log.Type.Message);
      this.Close();

      return;

    }// btn_close_Click

    private void frm_account_promos_Load(object sender, EventArgs e)
    {
      uc_ticket_promo1.InitControls(m_parent, m_account_id);
      uc_ticket_promo1.InitFocus();
    } //frm_account_promos_Load

    #endregion // Buttons

  } 
} 
