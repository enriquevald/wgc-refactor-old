//------------------------------------------------------------------------------
// Copyright © 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_TITO_handpays.cs
// 
//   DESCRIPTION: Implements the following classes:
//                1. frm_TITO_handpays: List of candidate tickets to apply handpay
//
// CREATION DATE: 12-NOV-2013
//
// REVISION HISTORY: 
// 
// Date        Author     Description
// ----------- ------     ----------------------------------------------------------
// 17-SEP-2013 ICS        First release.
// 05-DEC-2013 ICS & RMS  Using handpays table instead tickets table and refactor
// 06-MAR-2014 ICS        Fixed Bug #WIG-703: Error paying Jackpots or Handpays
// 02-APR-2014 ICS        Refactoring and code cleaning
// 16-APR-2014 ICS, MPO & RCI    Fixed Bug WIG-830: Not enough available storage exception.
// 16-APR-2014 ICS, MPO & RCI    Fixed Bug WIGOSTITO-1153: Not enough available storage exception.
// 11-JUL-2014 JMM        Fixed Bug #WIGOSTITO-1244: Error on pay handpay when there is no playsession and GP to print played amount is active
// 25-AUG-2014 MPO        Changed column name: Created -> Date, Payment -> Type
// 04-SEP-2014 JCO        Added level and progressive_id.
// 08-SEP-2014 JCO        Added hp_id.
// 08-SEP-2014 FJC        HANDPAY_TYPE: 'MANUAL_PROGRESSIVE' should not appear.
// 17-OCT-2014 MPO    Added undo handpay functions
// 02-DEC-2014 MPO        WIG-1796: Permissions for undo handpay
// 03-DEC-2014 MPO        WIG-1802: Error when read the handpay status 
// 10-JUN-2015 FAV        WIG-241: HandPay, show message when trying to cancel handpay created by another cash session.
// 11-JUN-2015 FAV        WIG-2366: HandPay, shows only handpays paid in the list.
// 19-OCT-2015 ETP        Product Backlog Item 4690 Gaming hall: avoid minimize cashier when closing frm.
// 22-FEB-2016 LTC        Product Backlog Item: 7440 Disputes and Handpays division - New Button functionality    
// 30-NOV-2016 ETP        Bug 21102 Old handpays are not payed after update
// 23-JUN-2017 XGJ        Bug 28349:[Ticket #6436] When canceling a handpay the cashier is blocked
// 08-AUG-2017 ATB        Bug 29252: The voucher of a paid "Handpay" is displaying "Anónimo" in the field "Cliente" when the user already introduced their account.
// 09-AUG-2017 ATB        Bug 29252: The voucher of a paid "Handpay" is displaying "Anónimo" in the field "Cliente" when the user already introduced their account (WIGOS-4288, WIGOS-4289)
// 19-SEP-2017 XGJ        Bug 28349:[Ticket #6436] When canceling a handpay the cashier is blocked
// 17-NOV-2017 DPC        PBI 30845:[WIGOS-4375]: Associate TITO handpays to the account
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using WSI.Common;
using WSI.Common.TITO;
using WSI.Cashier.TITO;
using System.Drawing.Text;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Threading;
using System.Collections;
using WSI.Cashier.Controls;

namespace WSI.Cashier
{
  public partial class frm_TITO_handpays : frm_base
  {
    #region Constants

    // Columns defined
    private const Int32 GRID_COLUMN_STATUS = 0;
    private const Int32 GRID_COLUMN_TERMINAL_PROVIDER = 1;
    private const Int32 GRID_COLUMN_TERMINAL_NAME = 2;
    private const Int32 GRID_COLUMN_FLOOR_ID = 3;
    private const Int32 GRID_COLUMN_DATE = 4;
    private const Int32 GRID_COLUMN_TICKET_TYPE = 5;
    private const Int32 GRID_COLUMN_HANDPAY_AMT_0 = 6;
    private const Int32 GRID_COLUMN_HANDPAY_AMT_0_FORMAT = 7;
    private const Int32 GRID_COLUMN_HANDPAY_CUR_0 = 8;
    private const Int32 GRID_COLUMN_AMOUNT = 9;
    private const Int32 GRID_COLUMN_HANDPAY_ID = 10;
    private const Int32 GRID_COLUMN_SESSION_ACCOUNT_ID = 11;

    // Column widths 
    private const Int32 GRID_COLUMN_WIDTH_STATUS = 100;
    private const Int32 GRID_COLUMN_WIDTH_TERMINAL_PROVIDER = 130;
    private const Int32 GRID_COLUMN_WIDTH_FLOOR_ID = 80;
    private const Int32 GRID_COLUMN_WIDTH_DATE = 50;
    private const Int32 GRID_COLUMN_WIDTH_TICKET_TYPE = 200;
    private const Int32 GRID_COLUMN_WIDTH_HANDPAY_AMT_0 = 140;
    private const Int32 GRID_COLUMN_WIDTH_AMOUNT = 140;

    private const Int32 REFRESH_INTERVAL = 5000;

    #endregion // Constants

    #region Attributes

    private frm_yesno form_yes_no;
    private Form m_parent;

    private Boolean m_monitor_activated = false;
    private System.Windows.Forms.Timer m_timer = new System.Windows.Forms.Timer();
    private DB_TRX _timer_db_trx = null;
    private Stopwatch _stop_watch = new Stopwatch();

    private String m_date_format;

    private Int32 m_countdown = 0;
    private Boolean m_force_selection = false;
    private Boolean m_monitor_pending = false;
    private Boolean m_monitor_paid = false;

    private Int64 m_account_id = 0;
    private Boolean m_is_virtual = false;

    // LTC 22-FEB-2016
    private Boolean m_is_Dispute;

    // XGJ 23-JUN-2017
    // XGJ 19-SEP-2017
    private bool m_handpay_flag = false;

    #endregion // Attributes

    #region Constructor

    public frm_TITO_handpays(Boolean IsDispute = false /*LTC 23-FEB-2016*/)
    {
      InitializeComponent();
      InitializeControlResources();
      EventLastAction.AddEventLastAction(this.Controls);
      form_yes_no = new frm_yesno();
      form_yes_no.Opacity = 0.6;

      // LTC 23-FEB-2016
      m_is_Dispute = IsDispute;
    }

    #endregion // Constructor

    #region Private Methods

    private void SetRowSelected(Int64 pHandPayId)
    {
      foreach (DataGridViewRow _row in dgv_handpays.Rows)
      {
        if (_row.Cells[GRID_COLUMN_HANDPAY_ID].Value.Equals(pHandPayId))
        {
          _row.Selected = true;
          if (!_row.Displayed)
          {
            dgv_handpays.FirstDisplayedScrollingRowIndex = _row.Index;
          }

          break;
        }
      }
    }

    private void SetFirstElementOfDataGridView()
    {
      Control[] _controls = uc_prov_terminals.Controls.Find("dgv_providers", true);
      if (_controls.Length > 0)
      {
        foreach (Control _control in _controls)
        {
          if (_control.GetType() == typeof(DataGridView))
          {
            DataGridView _dgv_test = (DataGridView)_control;
            if (_dgv_test.Rows.Count > 0)
            {
              _dgv_test.Rows[0].Selected = true;
              _dgv_test.FirstDisplayedScrollingRowIndex = 0;
            }
          }
        }
      }
    }

    private void DataGridContentLoad(DataTable _dt)
    {
      Int64 _hp_id_selected = 0;
      DataGridViewRow _view_row;
      DataRow _row;
      Boolean _do_seleted;

      _do_seleted = m_monitor_activated || m_force_selection;
      if (m_force_selection)
      {
        m_force_selection = false;
      }

      if (dgv_handpays.SelectedRows.Count > 0)
      {
        _view_row = dgv_handpays.SelectedRows[0];
        _row = ((DataRowView)_view_row.DataBoundItem).Row;
        _hp_id_selected = (Int64)_row[GRID_COLUMN_HANDPAY_ID];
      }

      LoadHandpaysList(_dt);

      if (m_monitor_activated && m_timer.Interval == REFRESH_INTERVAL)
      {
        lock (this)
        {
          m_countdown = Math.Max(0, m_countdown - REFRESH_INTERVAL);
        }
        _do_seleted = (m_countdown != 0);
      }

      if (_do_seleted && _hp_id_selected > 0)
      {
        SetRowSelected(_hp_id_selected);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Initialize Tickets Data Grid.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private void InitHandpaysDataGrid()
    {
      // Handpays DataGrid
      dgv_handpays.ColumnHeadersHeight = 30;
      dgv_handpays.RowTemplate.Height = 30;
      dgv_handpays.RowTemplate.DividerHeight = 0;

      // Status
      dgv_handpays.Columns[GRID_COLUMN_STATUS].Width = GRID_COLUMN_WIDTH_STATUS;
      dgv_handpays.Columns[GRID_COLUMN_STATUS].HeaderCell.Value = Resource.String("STR_FRM_HANDPAYS_043");

      // Provider
      dgv_handpays.Columns[GRID_COLUMN_TERMINAL_PROVIDER].Width = GRID_COLUMN_WIDTH_TERMINAL_PROVIDER;
      dgv_handpays.Columns[GRID_COLUMN_TERMINAL_PROVIDER].HeaderCell.Value = Resource.String("STR_FRM_HANDPAYS_004");

      // Terminal name
      dgv_handpays.Columns[GRID_COLUMN_TERMINAL_NAME].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
      dgv_handpays.Columns[GRID_COLUMN_TERMINAL_NAME].HeaderCell.Value = Resource.String("STR_FRM_VOUCHERS_REPRINT_069");

      // Floor ID
      dgv_handpays.Columns[GRID_COLUMN_FLOOR_ID].Width = GRID_COLUMN_WIDTH_FLOOR_ID;
      dgv_handpays.Columns[GRID_COLUMN_FLOOR_ID].HeaderCell.Value = Resource.String("STR_FRM_VOUCHERS_REPRINT_067");
      dgv_handpays.Columns[GRID_COLUMN_FLOOR_ID].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

      // Ticket Creation Date
      dgv_handpays.Columns[GRID_COLUMN_DATE].HeaderCell.Value = Resource.String("STR_FRM_HANDPAYS_049"); // Hour
      dgv_handpays.Columns[GRID_COLUMN_DATE].Width = GRID_COLUMN_WIDTH_DATE;
      dgv_handpays.Columns[GRID_COLUMN_DATE].DefaultCellStyle.Format = m_date_format.Replace(":ss", "");
      dgv_handpays.Columns[GRID_COLUMN_DATE].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

      // Ticket Type
      dgv_handpays.Columns[GRID_COLUMN_TICKET_TYPE].Width = GRID_COLUMN_WIDTH_TICKET_TYPE;
      dgv_handpays.Columns[GRID_COLUMN_TICKET_TYPE].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
      dgv_handpays.Columns[GRID_COLUMN_TICKET_TYPE].HeaderCell.Value = Resource.String("STR_FRM_HANDPAYS_028"); // Type

      // AMOUNT 0
      dgv_handpays.Columns[GRID_COLUMN_HANDPAY_AMT_0].Width = 0;
      dgv_handpays.Columns[GRID_COLUMN_HANDPAY_AMT_0].Visible = false;

      // AMOUNT 0 FORMAT
      if (Misc.IsFloorDualCurrencyEnabled())
      {
        dgv_handpays.Columns[GRID_COLUMN_HANDPAY_AMT_0_FORMAT].Width = GRID_COLUMN_WIDTH_HANDPAY_AMT_0;
        dgv_handpays.Columns[GRID_COLUMN_HANDPAY_AMT_0_FORMAT].HeaderCell.Value = Resource.String("STR_FRM_VOUCHERS_REPRINT_095");
        dgv_handpays.Columns[GRID_COLUMN_HANDPAY_AMT_0_FORMAT].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
        dgv_handpays.Columns[GRID_COLUMN_HANDPAY_AMT_0_FORMAT].DefaultCellStyle.Format = "C" + Math.Abs(Format.GetFormattedDecimals(CurrencyExchange.GetNationalCurrency()));
        dgv_handpays.Columns[GRID_COLUMN_HANDPAY_AMT_0_FORMAT].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
      }
      else
      {
        dgv_handpays.Columns[GRID_COLUMN_HANDPAY_AMT_0_FORMAT].Width = 0;
        dgv_handpays.Columns[GRID_COLUMN_HANDPAY_AMT_0_FORMAT].Visible = false;
      }

      // Ticket Amount (Last column)
      dgv_handpays.Columns[GRID_COLUMN_AMOUNT].Width = GRID_COLUMN_WIDTH_AMOUNT;
      dgv_handpays.Columns[GRID_COLUMN_AMOUNT].HeaderCell.Value = Resource.String("STR_FRM_VOUCHERS_REPRINT_057");
      dgv_handpays.Columns[GRID_COLUMN_AMOUNT].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
      dgv_handpays.Columns[GRID_COLUMN_AMOUNT].DefaultCellStyle.Format = "C" + Math.Abs(Format.GetFormattedDecimals(CurrencyExchange.GetNationalCurrency()));
      dgv_handpays.Columns[GRID_COLUMN_AMOUNT].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

      // CUR 0
      dgv_handpays.Columns[GRID_COLUMN_HANDPAY_CUR_0].Width = 0;
      dgv_handpays.Columns[GRID_COLUMN_HANDPAY_CUR_0].Visible = false;

      // Handpay Id
      dgv_handpays.Columns[GRID_COLUMN_HANDPAY_ID].Width = 0;
      dgv_handpays.Columns[GRID_COLUMN_HANDPAY_ID].Visible = false;

      dgv_handpays.Columns[GRID_COLUMN_SESSION_ACCOUNT_ID].Width = 0;
      dgv_handpays.Columns[GRID_COLUMN_SESSION_ACCOUNT_ID].Visible = false;

 
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Load filtered tickets
    //
    //  PARAMS :
    //      - INPUT :
    //          - TicketsTable
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private void LoadHandpaysList(DataTable HandpaysTable)
    {
      DataColumn _col;

      try
      {
        // Parse each row to set:
        //      - Ticket Type Name = Ticket type
        //      - Handpay type
        _col = HandpaysTable.Columns.Add("TYPE_NAME", typeof(String));

        _col = HandpaysTable.Columns.Add("STATUS", typeof(String));

        _col = HandpaysTable.Columns.Add("HP_AMT0_FORMAT", typeof(String));

        foreach (DataRow _row in HandpaysTable.Rows)
        {
          // Type string
          _row["TYPE_NAME"] = Handpays.HandpayTypeString((HANDPAY_TYPE)_row["HP_TYPE"], (Int32)_row["HP_LEVEL"]);

          // Status string
          _row["STATUS"] = Handpays.HandpayStatusString((Int32)_row["HP_STATUS"]);

          // Created aAmount
          _row["HP_AMT0_FORMAT"] = Currency.Format((Decimal)_row["HP_AMT0"], (String)_row["HP_CUR0"]);

        }

        HandpaysTable.Columns.Remove("HP_STATUS");
        HandpaysTable.Columns.Remove("HP_TYPE");
        HandpaysTable.Columns.Remove("HP_LEVEL");

        HandpaysTable.Columns["STATUS"].SetOrdinal(GRID_COLUMN_STATUS);
        HandpaysTable.Columns["HP_TE_PROVIDER_ID"].SetOrdinal(GRID_COLUMN_TERMINAL_PROVIDER);
        HandpaysTable.Columns["HP_TE_NAME"].SetOrdinal(GRID_COLUMN_TERMINAL_NAME);
        HandpaysTable.Columns["TE_FLOOR_ID"].SetOrdinal(GRID_COLUMN_FLOOR_ID);
        HandpaysTable.Columns["HP_DATETIME"].SetOrdinal(GRID_COLUMN_DATE);
        HandpaysTable.Columns["TYPE_NAME"].SetOrdinal(GRID_COLUMN_TICKET_TYPE);
        HandpaysTable.Columns["HP_AMT0"].SetOrdinal(GRID_COLUMN_HANDPAY_AMT_0);
        HandpaysTable.Columns["HP_AMT0_FORMAT"].SetOrdinal(GRID_COLUMN_HANDPAY_AMT_0_FORMAT);
        HandpaysTable.Columns["HP_CUR0"].SetOrdinal(GRID_COLUMN_HANDPAY_CUR_0);
        HandpaysTable.Columns["HP_AMOUNT"].SetOrdinal(GRID_COLUMN_AMOUNT);
        HandpaysTable.Columns["HP_ID"].SetOrdinal(GRID_COLUMN_HANDPAY_ID);

        dgv_handpays.DataSource = HandpaysTable;

        InitHandpaysDataGrid();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Get DataTable with tickets
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - DataTable: Ticket List
    //
    private DataTable GetFilteredHandpaysList()
    {
      DataTable _dt_handpays;

      _dt_handpays = new DataTable();

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = GetSqlCommand(false, _db_trx))
          {
            _dt_handpays.Load(_cmd.ExecuteReader());
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return _dt_handpays;
    }

    private void EnableDisableButtons()
    {
      String _error_str;
      Boolean _is_tito_cashier_open;

      _is_tito_cashier_open = BusinessLogic.IsTITOCashierOpen;
      btn_handpay_manual.Enabled = _is_tito_cashier_open;
      btn_hp_selected.Enabled = _is_tito_cashier_open;
      btn_undo_last_handpay.Enabled = _is_tito_cashier_open && m_monitor_paid && dgv_handpays.Rows.Count > 0;

      // Check if current user can redeem a ticket
      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.TITO_HandPay,
                                               ProfilePermissions.TypeOperation.OnlyCheck,
                                               this,
                                               out _error_str))
      {
        btn_hp_selected.Enabled = false;
        btn_handpay_manual.Enabled = false;
        btn_undo_last_handpay.Enabled = false;
      }

      // Check if there are any ticket in datagrid
      if (dgv_handpays.Rows.Count == 0)
      {
        btn_hp_selected.Enabled = false;
      }

    }

    #endregion // Private Methods

    #region Public Methods

          //------------------------------------------------------------------------------
    // PURPOSE : Init and Show the form.
    //
    //  PARAMS :
    //      - INPUT : ParentForm
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //

    public void refreshHandPays(){
        StringBuilder _where;
        Int32 _handpay_time_period;
        String _time_separator;

        _handpay_time_period = GeneralParam.GetInt32("Cashier.HandPays", "TimePeriod", 60, 5, 2500);

        _where = new StringBuilder();

        _where.AppendLine(" WHERE   TE_TERMINAL_TYPE = " + (Int32)TerminalTypes.SAS_HOST);
        _where.AppendLine("   AND   (DATEADD(MINUTE, " + _handpay_time_period + " , TE_RETIREMENT_REQUESTED) >= GETDATE()");
        _where.AppendLine("    OR   TE_RETIREMENT_REQUESTED IS NULL)");

        try
        {

            _time_separator = System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.TimeSeparator;
            m_date_format = " HH" + _time_separator + "mm" + _time_separator + "ss";

            //uc_prov_terminals.InitControls("", " WHERE TE_TERMINAL_TYPE = " + (Int32)TerminalTypes.SAS_HOST, false);
            uc_prov_terminals.InitControls("", _where.ToString(), false);
            LoadHandpaysList(GetFilteredHandpaysList());

            EnableDisableButtons();

            form_yes_no.Show(Parent);
            this.ShowDialog(form_yes_no);
            form_yes_no.Hide();

            Misc.WriteLog("[FORM CLOSE] frm_tito_handpays (show)", Log.Type.Message);
            this.Close();
        }
        catch (Exception _ex)
        {
            Log.Exception(_ex);
            form_yes_no.Hide();
        }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Init and Show the form.
    //
    //  PARAMS :
    //      - INPUT :
    //          - ParentForm
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public void Show(Form Parent)
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_TITO_handpays", Log.Type.Message);
      m_parent = Parent;
      this.refreshHandPays();
      /*StringBuilder _where;
      Int32 _handpay_time_period;
      String _time_separator;

      m_parent = Parent;

      _handpay_time_period = GeneralParam.GetInt32("Cashier.HandPays", "TimePeriod", 60, 5, 2500);

      _where = new StringBuilder();

      _where.AppendLine(" WHERE   TE_TERMINAL_TYPE = " + (Int32)TerminalTypes.SAS_HOST);
      _where.AppendLine("   AND   (DATEADD(MINUTE, " + _handpay_time_period + " , TE_RETIREMENT_REQUESTED) >= GETDATE()");
      _where.AppendLine("    OR   TE_RETIREMENT_REQUESTED IS NULL)");

      try
      {

        _time_separator = System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.TimeSeparator;
        m_date_format = " HH" + _time_separator + "mm" + _time_separator + "ss";

        //uc_prov_terminals.InitControls("", " WHERE TE_TERMINAL_TYPE = " + (Int32)TerminalTypes.SAS_HOST, false);
        uc_prov_terminals.InitControls("", _where.ToString(), false);
        LoadHandpaysList(GetFilteredHandpaysList());

        EnableDisableButtons();

        form_yes_no.Show(Parent);
        this.ShowDialog(form_yes_no);
        form_yes_no.Hide();

        Misc.WriteLog("[FORM CLOSE] frm_tito_handpays (show)", Log.Type.Message);
        this.Close();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        form_yes_no.Hide();
      }*/
    } // Show

    //------------------------------------------------------------------------------
    // PURPOSE : Dispose yes_no form.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    new public void Dispose()
    {
      form_yes_no.Dispose();
      form_yes_no = null;

      base.Dispose();
    } // Dispose

    //------------------------------------------------------------------------------
    // PURPOSE : Initialize control resources (NLS strings, images, etc).
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public void InitializeControlResources()
    {
      // Labels 
      this.FormTitle = Resource.String("STR_FRM_TITO_HANDPAYS_TITLE");
      lbl_last_update_pending.Text = "";
      lbl_last_update_paid.Text = "";

      // Groupboxes
      gb_search.HeaderText = Resource.String("STR_FRM_TITO_HANDPAYS_SOURCE");
      //gb_tickets.Text = Resource.String("STR_FRM_TITO_HANDPAYS_TICKETS");
      gb_tickets.HeaderText = Resource.String("STR_FRM_TITO_HANDPAYS_TITLE");

      // Buttons
      btn_ticket_search.Text = Resource.String("STR_FRM_TITO_HANDPAYS_SEARCH");
      btn_monitor_pending.Text = Resource.String("STR_FRM_TITO_HANDPAYS_PENDING_MONITOR");
      btn_monitor_paid.Text = Resource.String("STR_FRM_TITO_HANDPAYS_PAID_MONITOR");
      btn_close.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CLOSE");    // Close
      btn_hp_selected.Text = Resource.String("STR_FRM_HANDPAYS_041"); // Pay selected ticket
      btn_handpay_manual.Text = Resource.String("STR_FRM_TITO_HANDPAYS_MANUAL");
      btn_undo_last_handpay.Text = Resource.String("STR_HANDPAY_ACTION_13");

    } // InitializeControlResources

    /// <summary>
    /// AccountId will be a optional property to identify the handpay with a user
    /// </summary>
    public Int64 AccountId
    {
      set
      {
        this.m_account_id = value;
      }
      get
      {
        return this.m_account_id;
      }
    }

    public Boolean IsVirtual
    {
      set
      {
        this.m_is_virtual = value;
      }
      get
      {
        return this.m_is_virtual;
      }
    }
    #endregion // Public Methods

    #region Events

    #region Buttons

    //------------------------------------------------------------------------------
    // PURPOSE : Handle the click on the Close/Cancel button.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private void btn_close_Click(object sender, EventArgs e)
    {
      if (m_monitor_activated)
      {
        RunMonitor(false, false);
      }

      // Close Button

      Misc.WriteLog("[FORM CLOSE] frm_tito_handpays (close)", Log.Type.Message);
      this.Close();

      return;

    }// btn_close_Click

    //------------------------------------------------------------------------------
    // PURPOSE : Handle the click on the handpay manual button.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private void btn_handpay_manual_Click(object sender, EventArgs e)
    {
      Int64 _id_created;
      Boolean _current_monitor_activated;

      _id_created = 0;
      _current_monitor_activated = m_monitor_activated;

      if (_current_monitor_activated)
      {
        RunMonitor(false, false);
      }
      // LTC 23-FEB-2016
      // DLM 12-JUN-2018 using (frm_handpay_action _frm = new frm_handpay_action(frm_handpay_action.ENTRY_TYPE.MANUAL, 0, null, m_is_Dispute))
      using (frm_handpay_action _frm = new frm_handpay_action(frm_handpay_action.ENTRY_TYPE.MANUAL, 0, null, null, this, null, null, m_is_Dispute))
      {
        if (this.AccountId > 0)
        {
          _frm.AccountId = this.AccountId;
        }
        _frm.Show(this);
        _id_created = _frm.LastIdCreated();
      }

      if (_current_monitor_activated)
      {
        RunMonitor(true, false);
      }
      else
      {
        btn_ticket_search_Click(null, null);
      }

      if (_id_created > 0)
      {
        SetRowSelected(_id_created);
      }

    } // btn_handpay_manual_Click

    //------------------------------------------------------------------------------
    // PURPOSE : Handle the click on the search button.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //    
    private void btn_ticket_search_Click(object sender, EventArgs e)
    {
      //LoadHandpaysList(GetFilteredHandpaysList());

      DataGridContentLoad(GetFilteredHandpaysList());

      EnableDisableButtons();
    }

    private void btn_hp_selected_Click(object sender, EventArgs e)
    {
      DataGridViewRow _view_row;
      DataRow _row;
      CardData _virtual_card;
      Int64 _hp_id;

      if (this.dgv_handpays.SelectedRows.Count != 1)
      {
        frm_message.Show(Resource.String("STR_FRM_HANDPAYS_009"),
                         Resource.String("STR_APP_GEN_MSG_ERROR"),
                         MessageBoxButtons.OK,
                         MessageBoxIcon.Error,
                         m_parent);
        this.Focus();

        return;
      }

      _view_row = dgv_handpays.SelectedRows[0];
      _row = ((DataRowView)_view_row.DataBoundItem).Row;

      _virtual_card = new CardData();
      if (!BusinessLogic.LoadVirtualAccount(_virtual_card))
      {
        Log.Error("btn_redeem_selected_Click: Previous control. There are no account to redeem");

        return;
      }

      _hp_id = (Int64)_row[GRID_COLUMN_HANDPAY_ID];

      Boolean _current_monitor_activated;
      _current_monitor_activated = m_monitor_activated;

      if (_current_monitor_activated)
      {
        RunMonitor(false, false);
      }

      using (frm_handpay_action _frm = new frm_handpay_action(frm_handpay_action.ENTRY_TYPE.DEFAULT, _hp_id, null))
      {
        if (this.AccountId > 0)
        {
            _frm.AccountId = this.AccountId;
        }

        if ((Int64)_row[GRID_COLUMN_SESSION_ACCOUNT_ID] > 0)
        {
          _frm.AccountId = (Int64)_row[GRID_COLUMN_SESSION_ACCOUNT_ID];
        }

        _frm.Show(this);
      }

      dgv_handpays_Click(null, null);

      if (_current_monitor_activated)
      {
        RunMonitor(true, false);
      }
      else
      {
        m_force_selection = true;
        // Perfom search click event
        btn_ticket_search_Click(null, null);
      }

    }

    #endregion // Buttons

    private void btn_monitor_paid_Click(object sender, EventArgs e)
    {
      m_monitor_pending = false;
      RunMonitor(!m_monitor_paid, true);
      m_monitor_paid = !m_monitor_paid;

      btn_monitor_pending.IsSelected = m_monitor_pending;
      btn_monitor_paid.IsSelected = m_monitor_paid;
    }

    private void btn_monitor_Click(object sender, EventArgs e)
    {
      m_monitor_paid = false;
      RunMonitor(!m_monitor_pending, true);
      m_monitor_pending = !m_monitor_pending;
      btn_monitor_pending.IsSelected = m_monitor_pending;
      btn_monitor_paid.IsSelected = m_monitor_paid;
    }

    private void RunMonitor(Boolean Activate, Boolean RequestPermissions)
    {
      m_monitor_activated = Activate;

      _stop_watch.Reset();
      m_timer.Stop();
      m_timer.Tick -= null;
      m_timer.Dispose();
      m_timer = new System.Windows.Forms.Timer();


      //////////////////////
      //btn_undo_last_handpay.Enabled = m_monitor_paid;

      lbl_last_update_pending.Text = "";
      lbl_last_update_paid.Text = "";

      if (Activate && RequestPermissions)
      {
        // Check if current user is an authorized user
        //if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.HandpayMonitor,
        //                                         ProfilePermissions.TypeOperation.RequestPasswd,
        //                                         this,
        //                                         out _error_str))
        //{
        //  return;
        //}
      }

      //Enable/Disable controls for find handpays
      SetFirstElementOfDataGridView();
      uc_prov_terminals.Enabled = !Activate;
      btn_ticket_search.Enabled = !Activate;

      try
      {
        if (_timer_db_trx != null)
        {
          _timer_db_trx.Dispose();
        }

        if (Activate)
        {
          _timer_db_trx = new DB_TRX();
          _stop_watch.Start();
          m_timer.Tick += new System.EventHandler(RefreshHandpays);
          m_timer.Interval = 500;
          m_timer.Start();
        }

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    }

    private void RefreshHandpays(Object myObject, EventArgs myEventArgs)
    {
      DataTable _dt;

      // XGJ 23-JUN-2017
      if (!m_handpay_flag)
      {
        try
        {
          m_timer.Interval = REFRESH_INTERVAL;
          m_handpay_flag = true;

          if (_timer_db_trx != null && _stop_watch.Elapsed.Minutes > 15)
          {
            _stop_watch.Reset();
            _stop_watch.Start();

            _timer_db_trx.Dispose();
            _timer_db_trx = new DB_TRX();
          }

          _dt = new DataTable();

          using (SqlCommand _cmd = GetSqlCommand(true, _timer_db_trx))
          {
            using (SqlDataReader _reader = _cmd.ExecuteReader())
            {
              _dt.Load(_reader);
            }
          }

          DataGridContentLoad(_dt);

          if (m_monitor_pending)
          {
            lbl_last_update_paid.Text = "";
            lbl_last_update_pending.Text = Resource.String("STR_FRM_HANDPAYS_030", new object[] { WGDB.Now.ToString() }).Replace("\\r\\n", "\r\n");
          }
          else
          {
            lbl_last_update_pending.Text = "";
            lbl_last_update_paid.Text = Resource.String("STR_FRM_HANDPAYS_030", new object[] { WGDB.Now.ToString() }).Replace("\\r\\n", "\r\n");
          }

          EnableDisableButtons();
          m_handpay_flag = false;
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
          m_handpay_flag = false;
        }
      }
    }

    private SqlCommand GetSqlCommand(Boolean MonitorMode, DB_TRX _db_trx)
    {
      StringBuilder _sb;
      String _provider_id;
      Int32 _terminal_id;
      Int32 _time_period;
      Int32 _time_to_cancel;
      SqlCommand _cmd;

      _terminal_id = 0;
      _provider_id = String.Empty;

      if (!MonitorMode)
      {
        _terminal_id = uc_prov_terminals.TerminalSelected();
        _provider_id = uc_prov_terminals.ProviderSelected();
      }

      _sb = new StringBuilder();
      _time_period = GeneralParam.GetInt32("Cashier.HandPays", "TimePeriod", 60, 5, 2500);
      _time_to_cancel = GeneralParam.GetInt32("Cashier.HandPays", "TimeToCancel", 5);

      //_sb.AppendLine("     SELECT   TOP 100 ");
      //_sb.AppendLine("              HP_TE_PROVIDER_ID ");
      //_sb.AppendLine("            , HP_TE_NAME ");
      //_sb.AppendLine("            , TE_FLOOR_ID ");
      //_sb.AppendLine("            , HP_DATETIME ");
      //_sb.AppendLine("            , HP_TYPE AS TYPE_ID ");
      //_sb.AppendLine("            , HP_AMOUNT ");
      //_sb.AppendLine("            , TE_EXTERNAL_ID ");
      //_sb.AppendLine("            , HP_TICKET_ID ");
      //_sb.AppendLine("            , HP_TERMINAL_ID ");
      //_sb.AppendLine("            , HP_TYPE ");
      //_sb.AppendLine("            , HP_SITE_JACKPOT_NAME ");
      //_sb.AppendLine("            , HP_SITE_JACKPOT_AWARDED_ON_TERMINAL_ID ");
      //_sb.AppendLine("            , ISNULL (HP_LEVEL, 0) AS HP_LEVEL ");
      //_sb.AppendLine("            , ISNULL (HP_PROGRESSIVE_ID, 0) AS HP_PROGRESSIVE_ID ");
      //_sb.AppendLine("            , HP_ID ");
      //_sb.AppendLine("            , HP_STATUS ");
      //_sb.AppendLine("       FROM   HANDPAYS ");
      //_sb.AppendLine(" INNER JOIN   TERMINALS ");
      //_sb.AppendLine("         ON   TE_TERMINAL_ID = HP_TERMINAL_ID ");
      //_sb.AppendLine("      WHERE ( HP_STATUS & @pHpStatusMask = @pHpStatusPending ");
      //_sb.AppendLine("         OR   HP_STATUS & @pHpStatusMask = @pHpStatusVoided ) ");
      //_sb.AppendLine("        AND   HP_DATETIME                > @pCreatedDate ");
      //_sb.AppendLine("        AND   HP_TICKET_ID               IS NULL ");
      //_sb.AppendLine("        AND   HP_TYPE                   <> @pManualProgressiveHandPay ");

      _sb.AppendLine("     SELECT   TOP 100                                                                                          ");
      _sb.AppendLine("              HP_STATUS                                                                                        ");
      _sb.AppendLine("            , HP_TE_PROVIDER_ID                                                                                ");
      _sb.AppendLine("            , HP_TE_NAME                                                                                       ");
      _sb.AppendLine("            , TE_FLOOR_ID                                                                                      ");
      _sb.AppendLine("            , HP_DATETIME                                                                                      ");
      _sb.AppendLine("            , HP_TYPE                                                                                          ");
      _sb.AppendLine("            , dbo.ApplyExchange2(ISNULL(HP_AMT0, HP_AMOUNT), ISNULL(HP_CUR0, @pNatCur), @pNatCur) AS HP_AMOUNT ");
      _sb.AppendLine("            , ISNULL (HP_LEVEL, 0) AS HP_LEVEL                                                                 ");
      _sb.AppendLine("            , HP_ID                                                                                            ");
      _sb.AppendLine("            , ISNULL(HP_AMT0, HP_AMOUNT)   AS HP_AMT0                                                          ");
      _sb.AppendLine("            , ISNULL(HP_CUR0, @pNatCur)    AS HP_CUR0                                                          ");
      _sb.AppendLine("            , ISNULL(PS_ACCOUNT_ID, 0)                                                                         ");
      _sb.AppendLine("       FROM   HANDPAYS                                                                                         ");
      _sb.AppendLine(" INNER JOIN   TERMINALS                                                                                        ");
      _sb.AppendLine("         ON   TE_TERMINAL_ID = HP_TERMINAL_ID                                                                  ");
      _sb.AppendLine("  LEFT JOIN   PLAY_SESSIONS                                                                                    ");
      _sb.AppendLine("         ON   HP_PLAY_SESSION_ID = PS_PLAY_SESSION_ID                                                          ");

      if (!m_monitor_paid)
      {
        _sb.AppendLine("      WHERE ( HP_STATUS & @pHpStatusMask = @pHpStatusPending                                                 ");
        _sb.AppendLine("         OR   HP_STATUS & @pHpStatusMask = @pHpStatusVoided )                                                ");
        _sb.AppendLine("        AND   HP_DATETIME                > @pCreatedDate                                                     ");
      }
      else
      {
        //FAV 11-JUN-2015: JOIN to CASHIER_MOVEMENTS and conditions to show only hadpays paid.
        _sb.AppendLine(" INNER JOIN  CASHIER_MOVEMENTS                                                                               ");
        _sb.AppendLine("         ON  HANDPAYS.HP_ID = CASHIER_MOVEMENTS.CM_RELATED_ID                                                ");
        _sb.AppendLine("      WHERE  HP_STATUS & @pHpStatusMask = @pHpStatusPaid                                                     ");
        _sb.AppendLine("        AND  HP_STATUS_CHANGED > DATEADD(minute,- @pCancelTimePeriod,GETDATE())                              ");
        _sb.AppendLine("        AND  CASHIER_MOVEMENTS.CM_UNDO_STATUS IS NULL                                                        ");
        _sb.AppendLine("        AND  CASHIER_MOVEMENTS.CM_TYPE IN (@pHandpay, @pManualHandpay)                                       ");
      }
      _sb.AppendLine("         AND   HP_TICKET_ID               IS NULL                                                              ");
      _sb.AppendLine("         AND   HP_TYPE                    <> @pManualProgressiveHandPay                                        ");

      if (_terminal_id > 0)
      {
        _sb.AppendLine("       AND   TE_TERMINAL_ID = @pTerminalId                                                                   ");
      }

      if (!String.IsNullOrEmpty(_provider_id))
      {
        _sb.AppendLine("       AND   TE_PROVIDER_ID = @pProviderId                                                                   ");
      }

      if (m_account_id != 0 && !m_is_virtual)
      {
        _sb.AppendLine("        AND   PS_ACCOUNT_ID      = @AccountID                         ");
      }

      _sb.AppendLine("   ORDER BY   HP_DATETIME DESC ");

      _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction);

      if (_terminal_id > 0)
      {
        _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = _terminal_id;
      }
      if (!String.IsNullOrEmpty(_provider_id))
      {
        _cmd.Parameters.Add("@pProviderId", SqlDbType.NVarChar, 50).Value = _provider_id;
      }
      _cmd.Parameters.Add("@pCreatedDate", SqlDbType.DateTime).Value = WGDB.Now.AddMinutes(-_time_period);
      _cmd.Parameters.Add("@pHpStatusMask", SqlDbType.Int).Value = HANDPAY_STATUS.MASK_STATUS;
      _cmd.Parameters.Add("@pHpStatusPending", SqlDbType.Int).Value = HANDPAY_STATUS.PENDING;
      _cmd.Parameters.Add("@pHpStatusVoided", SqlDbType.Int).Value = HANDPAY_STATUS.VOIDED;
      _cmd.Parameters.Add("@pHpStatusPaid", SqlDbType.Int).Value = HANDPAY_STATUS.PAID;
      _cmd.Parameters.Add("@pManualProgressiveHandPay", SqlDbType.Int).Value = WSI.Common.HANDPAY_TYPE.SPECIAL_PROGRESSIVE;
      _cmd.Parameters.Add("@pCancelTimePeriod", SqlDbType.Int).Value = _time_to_cancel;
      _cmd.Parameters.Add("@pNatCur", SqlDbType.NVarChar).Value = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode", "");

      if (m_account_id != 0 && !m_is_virtual)
      {
        _cmd.Parameters.Add("@AccountID", SqlDbType.BigInt).Value = m_account_id;
      }

      //FAV 11-JUN-2015
      if (m_monitor_paid)
      {
        _cmd.Parameters.Add("@pHandpay", SqlDbType.Int).Value = CASHIER_MOVEMENT.HANDPAY;
        _cmd.Parameters.Add("@pManualHandpay", SqlDbType.Int).Value = CASHIER_MOVEMENT.MANUAL_HANDPAY;
      }

      return _cmd;
    }

    #endregion // Events

    private void btn_undo_last_handpay_Click(object sender, EventArgs e)
    {
      String _msg;
      StringBuilder _sb;
      String _text;
      Int64 _undo_operation_id;
      Boolean _monitor_activated;
      Handpays.HandpayRegisterOrCancelParameters _hp_params;
      DateTime _datetime_undo_operation;
      ProfilePermissions.CashierFormFuncionality _required_permission;
      String _error_str;
      Int64 _hand_pay_id;
      DataGridViewRow _view_row;

      _msg = "";
      _text = "";
      _undo_operation_id = 0;
      _monitor_activated = m_monitor_activated;
      _hp_params = null;
      _datetime_undo_operation = new DateTime();
      //dgv_handpays.

      try
      {
        if (_monitor_activated)
        {
          RunMonitor(false, false);
        }

        if (this.dgv_handpays.SelectedRows.Count != 1)
        {
          frm_message.Show(Resource.String("STR_FRM_HANDPAYS_009"),
                           Resource.String("STR_APP_GEN_MSG_ERROR"),
                           MessageBoxButtons.OK,
                           MessageBoxIcon.Error,
                           m_parent);
          this.Focus();

          return;
        }

        _view_row = dgv_handpays.SelectedRows[0];
        //_row = ((DataRowView)_view_row.DataBoundItem).Row;

        if (dgv_handpays.Rows.Count > 0 && Int64.TryParse(_view_row.Cells[GRID_COLUMN_HANDPAY_ID].Value.ToString(), out _hand_pay_id))
        {
          if (!Handpays.GetHandpayForUndo(Cashier.TerminalId, Cashier.CashierSessionInfo().CashierSessionId, _hand_pay_id, out _undo_operation_id, out _datetime_undo_operation, out _hp_params))
          {
            throw new Exception("Error on GetHandpayToUndo");
          }
        }
        else
        {
          ///pop up y return
          return;
        }

        if (_hp_params == null || _undo_operation_id == 0)
        {
          Int64 _cashier_session_id;
          string _cashier_session_name;
          if (Handpays.GetUserForManualHandpayMovement(_hand_pay_id, out _cashier_session_id, out _cashier_session_name, out _hp_params))
          {
            _sb = new StringBuilder();
            _sb.AppendLine(" - " + Resource.String("STR_SESSION_USER") + ": " + _cashier_session_name); // session name
            _sb.AppendLine(" - " + Resource.String("STR_FRM_VOUCHERS_REPRINT_057") + ": " + (Currency)_hp_params.HandpayAmount); //Amount
            _sb.AppendLine(" - " + Resource.String("STR_FRM_HANDPAYS_049") + ": " + _hp_params.HandpayDate.ToString()); //Hour
            _sb.AppendLine(" - " + Resource.String("STR_FRM_HANDPAYS_028") + ": " + Handpays.HandpayTypeString(_hp_params.HandpayType, _hp_params.Level)); //Type
            _sb.AppendLine(" - " + Resource.String("STR_FRM_VOUCHERS_REPRINT_069") + ": " + _hp_params.TerminalName); //Terminal
            _sb.AppendLine(" - " + Resource.String("STR_FRM_HANDPAYS_004") + ": " + _hp_params.TerminalProvider); //Provider

            _text = Resource.String("STR_FRM_HANDPAYS_048", _sb.ToString()).Replace("\\r\\n", "\r\n");
            frm_message.Show(_text, Resource.String("STR_FRM_HANDPAYS_022"), MessageBoxButtons.OK, MessageBoxIcon.Warning, this);

          }
          else
          {
            frm_message.Show(Resource.String("STR_FRM_HANDPAYS_033"), Resource.String("STR_APP_GEN_MSG_WARNING"), MessageBoxButtons.OK, MessageBoxIcon.Warning, this);
          }
          return;
        }

        // Check if current user is an authorized user
        _required_permission = ProfilePermissions.CashierFormFuncionality.CardHandPayCancel;
        switch (_hp_params.HandpayType)
        {
          case HANDPAY_TYPE.CANCELLED_CREDITS:
          case HANDPAY_TYPE.JACKPOT:
          case HANDPAY_TYPE.SITE_JACKPOT:
          case HANDPAY_TYPE.ORPHAN_CREDITS:
            _required_permission = ProfilePermissions.CashierFormFuncionality.CardHandPayCancel;
            break;

          case HANDPAY_TYPE.MANUAL:
          case HANDPAY_TYPE.MANUAL_CANCELLED_CREDITS:
          case HANDPAY_TYPE.MANUAL_JACKPOT:
          case HANDPAY_TYPE.MANUAL_OTHERS:
            _required_permission = ProfilePermissions.CashierFormFuncionality.ManualHandPayCancel;
            break;
        }

        if (!ProfilePermissions.CheckPermissions(_required_permission,
                                                 ProfilePermissions.TypeOperation.RequestPasswd,
                                                 this,
                                                 out _error_str))
        {
          return;
        }

        _sb = new StringBuilder();
        _sb.AppendLine(" - " + Resource.String("STR_FRM_VOUCHERS_REPRINT_057") + ": " + (Currency)_hp_params.HandpayAmount); //Amount
        _sb.AppendLine(" - " + Resource.String("STR_FRM_HANDPAYS_049") + ": " + _hp_params.HandpayDate.ToString()); //Hour
        _sb.AppendLine(" - " + Resource.String("STR_FRM_HANDPAYS_028") + ": " + Handpays.HandpayTypeString(_hp_params.HandpayType, _hp_params.Level)); //Type
        _sb.AppendLine(" - " + Resource.String("STR_FRM_VOUCHERS_REPRINT_069") + ": " + _hp_params.TerminalName); //Terminal
        _sb.AppendLine(" - " + Resource.String("STR_FRM_HANDPAYS_004") + ": " + _hp_params.TerminalProvider); //Provider

        _text = Resource.String("STR_FRM_HANDPAYS_032", _sb.ToString()).Replace("\\r\\n", "\r\n");

        if (frm_message.Show(_text, Resource.String("STR_FRM_HANDPAYS_022"), MessageBoxButtons.YesNo, MessageBoxIcon.Question, this) == DialogResult.OK)
        {
          using (DB_TRX _db_trx = new DB_TRX())
          {
            ArrayList _voucher;

            if (!Handpays.UndoHandpay(_hp_params,
                                      _undo_operation_id,
                                      _datetime_undo_operation,
                                      Cashier.CashierSessionInfo(),
                                      out _msg,
                                      out _voucher,
                                      _db_trx.SqlTransaction))
            {
              frm_message.Show(_msg, Resource.String("STR_APP_GEN_MSG_WARNING"), MessageBoxButtons.OK, MessageBoxIcon.Warning, this);

              return;
            }

            if (_db_trx.Commit())
            {
              if (_voucher != null)
              {
                VoucherPrint.Print(_voucher);
              }
            }
          }
        }

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        frm_message.Show(Resource.String("STR_FRM_HANDPAYS_040"), Resource.String("STR_APP_GEN_MSG_WARNING"), MessageBoxButtons.OK, MessageBoxIcon.Warning, this);
      }
      finally
      {
        if (_monitor_activated)
        {
          RunMonitor(true, false);
        }
        else
        {
          btn_ticket_search_Click(null, null);
        }
      }
    }

    private void dgv_handpays_Click(object sender, EventArgs e)
    {
      lock (this)
      {
        m_countdown = REFRESH_INTERVAL * 3;
      }
    }





  } // frm_TITO_handpays


} // WSI.Cashier
