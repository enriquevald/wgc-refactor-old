//------------------------------------------------------------------------------
// Copyright � 2007-2009 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_tito_card.cs
// 
//   DESCRIPTION: Implements the uc_tito_card user control to supply uc_card in TITO mode
//
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 08-NOV-2013 LEM     First release.
// 21-NOV-2013 AMF     New comment when close session.
// 10-JAN-2014 JPJ     Account information in the AccountWatcher thread is cleared.
// 31-JAN-2014 QMP     Fixed Bug WIG-563: Enabled buttons when cash session is closed
// 03-FEB-2014 DLL     Remove promotion button and moved to uc_ticket_create
// 06-MAR-2014 ICS     Deleted the checking of site jackpot winner. Not needed in TITO.
// 14-APR-2014 DLL     Added new functionality: VIP Account
// 16-APR-2014 ICS, MPO & RCI    Fixed Bug WIG-830: Not enough available storage exception.
// 05-MAY-2014 RMS     Fixed Bug WIG-842: Personalizaci�n de Cuentas: Al intentar personalizar una cuenta el formulario aparece en blanco. 
// 22-JUL-2014 JCO     Fixed Bub WIG-1109: Added functionality to show comments when chk_show_comments is checked.
// 30-SEP-2014 SGB     Fixed Bub WIG-1261: Click on the screen to cancel redeem ticket, and reappeared for the second time.
// 28-OCT-2014 SMN     Added new functionality: BLOCK_REASON field can have more than one block reason.
// 21-MAY-2015 RCI     Fixed Bug WIG-2375: Don't allow to unlock accounts if they are locked by External PLD.
// 28-APR-2015 FJC     Add Button for WinLossStatement Request
// 14-DEC-2015 FAV     Fixed Bug 7599: Errors with permissions for card replacement free
// 12-NOV-2015 FAV     Product Backlog Item 5885: New design
// 19-JAN-2016 RAB     Product Backlog Item 7941:Multiple buckets: Cajero: A�adir/Restar valores a los buckets
// 29-MAR-2016 EOR     FixedBug 10432: New Cashier Error Lock/Unlock Accounts
// 13-OCT-2016 FGB     Bug 18904: Buckets - Cashier: Error if there are no buckets enabled.
// 17-MAR-2017 ETP     WIGOS-194: Credit line: Add logic to avoid withdraw with debt.
// 22-MAY-2017 OMC     Bug 26156: Cashier - Last account movements, unidentified movement in card recharge.
// 10-JUL-2017 DHA     PBI 28610:WIGOS-3458 - Show Customer Image in a Pop up - Accounts
// 03-OCT-2017 DHA     PBI 30017:WIGOS-4056 Payment threshold registration - Player registration
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Drawing.Printing;
using WSI.Common;
using WSI.Cashier.TITO;
using WSI.Common.TITO;
using System.Collections;
using System.Threading;
using WSI.Cashier.MVC.Controller.PlayerEditDetails;
using WSI.Cashier.MVC.View.PlayerEditDetails;
using WSI.Common.CreditLines;

namespace WSI.Cashier
{
  public partial class uc_tito_card : UserControl
  {
    #region Members

    private static frm_yesno form_yes_no;
    Int64 m_current_card_id;
    frm_available_draws form_available_draws = new frm_available_draws();
    frm_last_movements form_last_plays = new frm_last_movements();
    frm_search_players form_search_players = new frm_search_players();
    String m_visible_track_number;
    String m_current_track_number;
    Int32 m_tick_count_tracknumber_read = CashierBusinessLogic.DURATION_BUTTONS_ENABLED_AFTER_OPERATION + 1;
    Boolean m_is_logged_in;
    Boolean m_can_log_off;
    AccountWatcher m_account_watcher;
    CardPrinter m_card_printer;
    Boolean m_first_time = true;
    private frm_container m_parent;
    Boolean m_is_new_card;
    Boolean m_is_vip_account;
    Boolean m_pending_check_comments;
    Boolean m_must_show_comments;
    
    private Boolean m_exist_photo = false;

    #endregion Members

    #region Constructor

    public uc_tito_card()
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[UC LOAD] uc_tito_card", Log.Type.Message);

      InitializeComponent();

      form_yes_no = new frm_yesno();
      form_yes_no.Opacity = 0.6;

      m_card_printer = new CardPrinter();
      m_pending_check_comments = false;

      EventLastAction.AddEventLastAction(this.Controls);
    }

    public IDScanner.IDScanner IDScanner { get; set; }

    #endregion Constructor

    #region Privates


    private bool GetMustShowComments(CardData pCard)
    {
      return !m_is_new_card &&
             pCard.PlayerTracking.ShowCommentsOnCashier &&
             m_pending_check_comments &&
             CashierBusinessLogic.ReadCashierStatus(Cashier.SessionId) == CASHIER_STATUS.OPEN;
    } // GetPermissions

    private void RestoreParentFocus()
    {
      this.ParentForm.Focus();
      this.ParentForm.BringToFront();
    }

    private void account_watcher_AccountChangedEvent()
    {
      Delegate _method;

      if (this.InvokeRequired)
      {
        try
        {
          _method = new AccountWatcher.AccountChangedHandler(account_watcher_AccountChangedEvent);

          if (_method != null)
          {
            this.BeginInvoke(_method);
          }
          else
          {
            Log.Error("Account changed: nobody taking care of the event, the method is null.");
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }

        return;
      }

      RefreshData();
    }

    private void EnableDisableButtons(Boolean EnableButtons)
    {
      Boolean _enabled;
      Boolean _can_recycle;
      Boolean _account_is_anonymous;
      CASHIER_STATUS _status;

      _can_recycle = true;
      _account_is_anonymous = false;
      _enabled = EnableButtons && !m_is_logged_in;

      timer1.Enabled = EnableButtons;

      _status = CashierBusinessLogic.ReadCashierStatus(Cashier.SessionId);

      m_parent.ChangeLabelStatus();

      ToggleApparenceCtrl(this, EnableButtons);

      btn_new_card.Enabled = _enabled && (m_account_watcher.Card.CardPaid || m_account_watcher.Card.IsRecycled);
      btn_account_edit.Enabled = EnableButtons;
      btn_card_block.Enabled = EnableButtons;
      btn_lasts_movements.Enabled = EnableButtons;
      btn_print_card.Enabled = EnableButtons && m_card_printer.PrinterExist();
      btn_gifts.Enabled = _enabled;
      btn_draw.Enabled = _enabled;
      btn_vouchers_space.Enabled = EnableButtons;
      btn_win_loss_statement_request.Enabled = EnableButtons;
      if ((GeneralParam.GetInt32("PlayerTracking.ExternalLoyaltyProgram", "Mode", 0) != 0))
        btn_multiple_buckets.Visible = false;
      btn_multiple_buckets.Enabled = EnableButtons;
      btn_pay_ticket.Enabled = EnableButtons;


      if (EnableButtons)
      {
        if (_status != CASHIER_STATUS.OPEN)
        {
          btn_gifts.Enabled = false;
          btn_new_card.Enabled = false;
          btn_account_edit.Enabled = false;
          btn_card_block.Enabled = false;
          btn_draw.Enabled = false;
          btn_vouchers_space.Enabled = false;
          btn_win_loss_statement_request.Enabled = false;

          _can_recycle = false;
        }

        if (m_account_watcher != null && m_account_watcher.Card != null)
        {
          if (m_account_watcher.Card.Blocked)
          {
            btn_gifts.Enabled = false;
            btn_vouchers_space.Enabled = false;
          }

          if (m_account_watcher.Card.PlayerTracking.CardLevel == 0)
          {
            _account_is_anonymous = true;
            btn_vouchers_space.Enabled = false;
          }
        }
      }

      HideRecycleButton(_enabled && _can_recycle);

      btn_vouchers_space.Visible = false;

      if (GeneralParam.GetInt32("PlayerTracking.ExternalLoyaltyProgram", "Mode") != 0)
      {
        btn_new_card.Enabled = _account_is_anonymous;
        btn_account_edit.Enabled = false;
        gb_gifts.Visible = false;

        btn_vouchers_space.Visible = true;
      }
      if (m_account_watcher != null &&
          Accounts.ExistsBlockReasonInMask(new List<AccountBlockReason>(new AccountBlockReason[] {
                                                    AccountBlockReason.EXTERNAL_SYSTEM_CANCELED /*, AccountBlockReason.EXTERNAL_AML */ }),
                                           Accounts.ConvertOldBlockReason((Int32)m_account_watcher.Card.BlockReason)))
      {
        btn_card_block.Enabled = false;
      }

      if (WSI.Common.GeneralParam.GetBoolean("WinLossStatement", "Enabled", false))
      {
        btn_win_loss_statement_request.Visible = true;
      }
      else
      {
        btn_win_loss_statement_request.Visible = false;
      }
    }

    private void HideRecycleButton(Boolean EnableButtons)
    {
      String _error_str;

      if (m_current_track_number == CardData.RECYCLED_TRACK_DATA)
      {
        btn_account_edit.Enabled = false;
        btn_card_block.Enabled = false;
        btn_recycle_card.Enabled = false;
        btn_gifts.Enabled = false;
        btn_print_card.Enabled = false;
      }

      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.RecyleCard,
                                               ProfilePermissions.TypeOperation.OnlyCheck, this.ParentForm, false, out _error_str))
      {
        btn_recycle_card.Visible = false;

        return;
      }

      btn_recycle_card.Visible = true;
      btn_recycle_card.Enabled = false;

      if (!EnableButtons)
      {
        return;
      }

      if (m_current_track_number == CardData.RECYCLED_TRACK_DATA)
      {
        return;
      }

      if (GeneralParam.GetBoolean("Site", "MultiSiteMember", false))
      {
        return;
      }

      btn_recycle_card.Enabled = true;

    }

    private void ToggleApparenceCtrl(Control Ctrl, Boolean Enabled)
    {
      if (Ctrl.Equals(uc_card_reader1)
          || Ctrl.Equals(lbl_cards_title)
          || Ctrl is Button)
      {
        return;
      }
      if (!Ctrl.Equals(this))
      {
        Ctrl.Enabled = Enabled;
      }

      foreach (Control ctrl in Ctrl.Controls)
      {
        ToggleApparenceCtrl(ctrl, Enabled);
      }

    }

    private void RefreshAccount()
    {
      m_account_watcher.ForceRefresh();
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Creates a new Card Data Account
    // 
    //  PARAMS:
    //      - INPUT:
    //          CardData
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    private void NewAccount(CardData CardData)
    {
      Boolean _card_paid;
      String _msgbox_text;
      CardData _aux_card = new CardData();
      String _error_str;

      // MBF 30-NOV-2009 - Check BEFORE asking if the user wants to create
      // Check provided trackdata is not already associated to a MB account
      if (CashierBusinessLogic.DB_IsMBCardDefined(CardData.TrackData))
      {
        // MsgBox: Card already assigned to a Mobile Bank account.
        // frm_message.Show(Resource.String("STR_UC_CARD_USER_MSG_CARD_LINKED_TO_MB_ACCT"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this.ParentForm);

        // uc_card_reader1.Focus();

        // We are going to change the form shown, expire the timer to disable buttons in case of return to this form.
        m_tick_count_tracknumber_read = CashierBusinessLogic.DURATION_BUTTONS_ENABLED_AFTER_OPERATION + 1;

        m_parent.SwitchToMobileBanck(CardData.TrackData);
        ClearCardData();
        uc_card_reader1.ClearTrackNumber();

        return;
      }

      // Card does not exist: request confirmation to create a new one.
      _aux_card.TrackData = CardData.TrackData;

      // RCI 18-OCT-2011: Can't create new accounts when cashier is not open.
      if (CashierBusinessLogic.ReadCashierStatus(Cashier.SessionId) != CASHIER_STATUS.OPEN)
      {
        _msgbox_text = Resource.String("STR_UC_CARD_USER_MSG_CARD_NOT_EXIST_AND_CASH_CLOSED", "\n\n" + _aux_card.VisibleTrackdata() + "\n\n");
        frm_message.Show(_msgbox_text, Resource.String("STR_UC_CARD_USER_MSG_CARD_NOT_EXIST_TITLE"), MessageBoxButtons.OK, MessageBoxIcon.Warning, this.ParentForm);

        uc_card_reader1.Focus();

        return;
      }

      //m_is_new_card = true;
      // JCM 13-APR-2012 Ask the new account type.
      switch (frm_message.ShowAccountSelect(_aux_card.VisibleTrackdata(), this.ParentForm))
      {
        case ACCOUNT_USER_TYPE.ANONYMOUS:

          // Check if current user is an authorized user
          if (!ProfilePermissions.CheckPermissionList(ProfilePermissions.CashierFormFuncionality.AccountCreateAnonymous, ProfilePermissions.TypeOperation.RequestPasswd, this.ParentForm, 0, out _error_str))
          {
            return;
          }

          using (DB_TRX _db_trx = new DB_TRX())
          {
            // Creates CardData into DB
            if (!CashierBusinessLogic.DB_CreateCard(CardData.TrackData, out CardData.AccountId, _db_trx.SqlTransaction))
            {
              Log.Error("LoadCardByTrackNumber. DB_CreateCard: Card already exists or error creating card.");
              // TODO: MsgBox: Card already exist or error creating card.
              //frm_message.Show(Resource.String("STR_FRM_AMOUNT_INPUT_MSG_AMOUNT_EXCEED_PAYABLE"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this);

              uc_card_reader1.Focus();
            }
            else
            {
              if (CashierBusinessLogic.DB_UpdateCardPayAfterCreated(CardData.AccountId, ACCOUNT_USER_TYPE.ANONYMOUS, out _card_paid, _db_trx.SqlTransaction))
              {
                CardData.CardPaid = _card_paid;
              }

              _db_trx.Commit();
              m_is_new_card = false;
            }
          }

          // Load data after create account.
          LoadCardByAccountId(CardData.AccountId);

          return;

        case ACCOUNT_USER_TYPE.PERSONAL:

          // Check if current user is an authorized user
          if (!ProfilePermissions.CheckPermissionList(ProfilePermissions.CashierFormFuncionality.AccountCreatePersonal, ProfilePermissions.TypeOperation.RequestPasswd, this.ParentForm, 0, out _error_str))
          {
            return;
          }

          this.ClearCardData();
          m_current_track_number = CardData.TrackData;
          btn_edit_Click(null, null);

          break;

        case ACCOUNT_USER_TYPE.NONE:  // cancel
        default:
          this.ClearCardData();
          this.InitControls(m_parent);
          break;
      }
    } // NewAccount

    /// <summary>
    /// Check for input card in order to notify user to create card.
    /// Previously called LoadCardByTrackNumber
    /// </summary>
    /// <param name="AccountId"></param>
    private Boolean LoadCardByAccountId(Int64 AccountId)
    {
      String _message;
      string[] _message_params = { "", "", "", "", "", "", "" };

      // Check for no input card
      if (AccountId < 0)
      {
        return false;
      }

      CardData card_data = new CardData();

      m_visible_track_number = "";

      // Get Card data
      if (!CardData.DB_CardGetAllData(AccountId, card_data, false))
      {
        ClearCardData();

        Log.Error("LoadCardByAccountId. DB_GetCardAllData: Error reading card.");

        return false;
      }

      if (!card_data.IsRecycled)
      {
        if (!CardData.CheckTrackdataSiteId(new ExternalTrackData (card_data.TrackData)))
        {
          ClearCardData();

          Log.Error("LoadCardByAccountId. Card: " + card_data.TrackData + " doesn�t belong to this site.");

          frm_message.Show(Resource.String("STR_CARD_SITE_ID_ERROR"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK,
                           MessageBoxIcon.Warning, this.ParentForm);

          uc_card_reader1.Focus();

          return false;

        }
      }

      if (card_data.AccountId != 0)
      {
        // Keep card read data.

        m_current_card_id = card_data.AccountId;
        m_current_track_number = card_data.TrackData;
        //m_is_new_card = false;
        m_account_watcher.SetAccount(card_data);

        m_is_vip_account = card_data.PlayerTracking.HolderIsVIP;

        // RCI 10-NOV-2010: Show pop-up if recent change of level occurrs.
        if (card_data.PlayerTracking.HolderLevelNotify != 0)
        {
          Int32 _old_level_int;
          String _old_level;
          String _new_level;

          _old_level_int = card_data.PlayerTracking.CardLevel - card_data.PlayerTracking.HolderLevelNotify;
          _old_level = LoyaltyProgram.LevelName(_old_level_int);
          _new_level = LoyaltyProgram.LevelName(card_data.PlayerTracking.CardLevel);

          _message_params[0] = _old_level;
          _message_params[1] = _new_level;
          if (card_data.PlayerTracking.HolderLevelNotify > 0)
          {
            _message = Resource.String("STR_UC_CARD_USER_MSG_HOLDER_LEVEL_RISEN", _message_params);
          }
          else
          {
            _message = Resource.String("STR_UC_CARD_USER_MSG_HOLDER_LEVEL_FALLEN", _message_params);
          }
          _message = _message.Replace("\\r\\n", "\r\n");

          frm_message.Show(_message,
                           Resource.String("STR_APP_GEN_MSG_WARNING"),
                           MessageBoxButtons.OK,
                           MessageBoxIcon.Information,
                           ParentForm);


          CashierBusinessLogic.DB_ResetHolderLevelNotify(card_data);
        }

        m_must_show_comments = GetMustShowComments(card_data);

        if (m_must_show_comments)
        {
          m_pending_check_comments = false;
          btn_edit_Click(null, null);
          uc_card_reader1.Focus();
        }
      }

      return true;
    } // LoadCardByAccountId

    /// <summary>
    /// 
    /// </summary>
    private void RefreshData()
    {
      RefreshData(m_account_watcher.Card);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="CardData"></param>
    private void RefreshData(CardData CardData)
    {
      CardData _card_data;
      DateTime _birth_date;
      DateTime _now_date;
      TimeSpan _tspam;
      Int32 _timeout_seconds;
      Boolean _enabled_buttons_after_operation;
      Dictionary<AccountBlockReason, String> _block_reasons;

      _enabled_buttons_after_operation = false;

      _card_data = CardData;
      if (_card_data == null)
      {
        _card_data = new CardData();
      }

      if (_card_data.IsRecycled)
      {
        m_visible_track_number = CardData.RECYCLED_TRACK_DATA;
      }
      else
      {
        m_visible_track_number = _card_data.VisibleTrackdata();
        m_current_track_number = _card_data.TrackData;
      }

      // lbl_section_1.Text = track_number;
      lbl_track_number.Text = m_visible_track_number;

      // Display card data
      m_current_card_id = _card_data.AccountId;

      m_is_vip_account = _card_data.PlayerTracking.HolderIsVIP;

      lbl_happy_birthday.Visible = false;

      // Refresh the account's level
      lbl_level.Text = LoyaltyProgram.LevelName(_card_data.PlayerTracking.CardLevel);
      // DDM 08-AUG-2012: If Is anonymous or not have account loaded, is set "Document Type:"
      lbl_id_title.Text = Resource.String("STR_FRM_PLAYER_EDIT_TYPE_DOCUMENT") + ":";

      pb_user.Image = Resources.ResourceImages.anonymous_user;
      lbl_id.Visible = false;
      lbl_birth.Visible = false;

      if (_card_data.PlayerTracking.HolderName == null)
      {
        lbl_holder_name.Text = "";
      }
      else if (_card_data.PlayerTracking.HolderName == "")
      {
        lbl_holder_name.Text = Resource.String("STR_UC_CARD_CARD_HOLDER_ANONYMOUS");
      }
      else
      {
        lbl_id.Visible = true;
        lbl_id_title.Visible = true;
        lbl_birth.Visible = true;
        lbl_birth_title.Visible = true;
        lbl_name.Visible = true;

        lbl_holder_name.Text = _card_data.PlayerTracking.HolderName;

        // RMS 20-AUG-2013 WIG-109
        lbl_id_title.Text = CardData.GetLabelDocument(m_account_watcher.Card.PlayerTracking.PreferredHolderIdType);
        lbl_id.Text = _card_data.PlayerTracking.PreferredHolderId;

        if (_card_data.PlayerTracking.HolderBirthDate != DateTime.MinValue)
        {
          lbl_birth.Text = _card_data.PlayerTracking.HolderBirthDate.ToShortDateString();

          // MBF 26-OCT-2010 - People born in 29 of Feb have birthdays too.
          if (_card_data.PlayerTracking.HolderBirthDate.Month == 2
            && _card_data.PlayerTracking.HolderBirthDate.Day == 29
            && !DateTime.IsLeapYear(WGDB.Now.Year))
          {
            _birth_date = new DateTime(WGDB.Now.Year, _card_data.PlayerTracking.HolderBirthDate.Month, _card_data.PlayerTracking.HolderBirthDate.AddDays(-1).Day);
          }
          else
          {
            _birth_date = new DateTime(WGDB.Now.Year, _card_data.PlayerTracking.HolderBirthDate.Month, _card_data.PlayerTracking.HolderBirthDate.Day);
          }

          // RCI 19-AUG-2010: Happy Birthday!
          _now_date = new DateTime(WGDB.Now.Year, WGDB.Now.Month, WGDB.Now.Day);
          _tspam = _birth_date - _now_date;

          if (_tspam.TotalDays > 0 && _tspam.TotalDays <= GeneralParam.GetInt32("Accounts", "Player.BirthdayWarningDays", 7))
          {
            lbl_happy_birthday.Text = Resource.String("STR_UC_PLAYER_TRACK_BIRTHDAY_IN_NEXT_WEEK");
            lbl_happy_birthday.Visible = true;
          }
          else if (_tspam.TotalDays == 0)
          {
            lbl_happy_birthday.Text = Resource.String("STR_UC_PLAYER_TRACK_HAPPY_BIRTHDAY");
            lbl_happy_birthday.Visible = true;
          }

          //JBC 30-01-2015 BirthDay alarm feature
          if (!Alarm.CheckBirthdayAlarm(_card_data.AccountId, _card_data.PlayerTracking.HolderName,
            CommonCashierInformation.CashierSessionInfo().TerminalName + " - " + CommonCashierInformation.CashierSessionInfo().TerminalId, TerminalTypes.UNKNOWN))
          {

            Log.Error("uc_ticket_create.RefreshData: Error checking Birthday Alarm.");
          }

        }
        else
        {
          lbl_birth.Visible = false;
          lbl_birth_title.Visible = false;
        }

        // RMS 20-AUG-2013 WIG-109
        if (String.IsNullOrEmpty(lbl_id.Text))
        {
          lbl_id.Visible = false;
          lbl_id_title.Visible = false;
        }

        // AJQ 28-MAY-2015, Account Photo        

        AccountPhotoFunctions.SetAccountPhoto(pb_user, _card_data.AccountId, (GENDER)_card_data.PlayerTracking.HolderGender, out m_exist_photo);
      }

      lbl_account_id_value.Text = m_current_card_id.ToString();
      lbl_account_id_value.Visible = (m_current_card_id != 0);

      lbl_vip_account.Text = Resource.String("STR_VIP_ACCOUNT");
      lbl_vip_account.Visible = m_is_vip_account;

      m_is_logged_in = _card_data.IsLoggedIn;
      m_can_log_off = m_is_logged_in;

      if (m_can_log_off)
      {
        try
        {
          if (!_card_data.CurrentPlaySession.BalanceMismatch)
          {
            _timeout_seconds = Int32.Parse(WSI.Common.Misc.ReadGeneralParams("Cashier", "MinTimeToCloseSession"));

            if (_card_data.CurrentPlaySession.SecondsSinceLastActivity < _timeout_seconds)
            {
              m_can_log_off = false;
            }
          }
        }
        catch (Exception ex)
        {
          Log.Exception(ex);
        }
      }

      // RCI 22-OCT-2010: Added block reason label.
      if (_card_data.Blocked)
      {
        btn_card_block.Text = Resource.String("STR_UC_CARD_BLOCK_ACTION_UNLOCK");
        lbl_blocked.Text = Resource.String("STR_UC_CARD_BLOCK_STATUS_LOCKED");

        _block_reasons = Accounts.GetBlockReason((Int32)_card_data.BlockReason);
        if (_block_reasons.Count > 1)
        {
          // Show "by several reasons"
          lbl_block_reason.Text = Resource.String("STR_UC_CARD_BLOCK_SEVERAL_REASONS");
        }
        else
        {
          // Show the only one block_reason
          foreach (KeyValuePair<AccountBlockReason, String> _key_value_pair in _block_reasons)
          {
            lbl_block_reason.Text = _key_value_pair.Value;
          }
        }

        pb_blocked.Image = WSI.Cashier.Images.Get(Images.CashierImage.Locked);
      }
      else
      {
        btn_card_block.Text = Resource.String("STR_UC_CARD_BLOCK_ACTION_LOCK");
        lbl_blocked.Text = Resource.String("STR_UC_CARD_BLOCK_STATUS_UNLOCKED");
        lbl_block_reason.Text = String.Empty;
        pb_blocked.Image = WSI.Cashier.Images.Get(Images.CashierImage.Unlocked);
      }

      // Hide the blocked label if not blocked
      lbl_blocked.Visible = _card_data.Blocked;
      lbl_block_reason.Visible = _card_data.Blocked;
      pb_blocked.Visible = _card_data.Blocked;

      // Points
      lbl_points_balance.Text = _card_data.PlayerTracking.TruncatedPoints.ToString();

      _enabled_buttons_after_operation = (WSI.Common.Misc.ReadGeneralParams("Cashier", "KeepEnabledButtonsAfterOperation") == "1");

      if (m_current_card_id > 0 &&
           (m_tick_count_tracknumber_read == 0 ||
            (_enabled_buttons_after_operation
             && m_tick_count_tracknumber_read < CashierBusinessLogic.DURATION_BUTTONS_ENABLED_AFTER_OPERATION)))
      {
        EnableDisableButtons(true);
      }
      else
      {
        // If must show comments, enabled buttons.
        if (m_must_show_comments)
        {
          m_tick_count_tracknumber_read = 0;
        }
        EnableDisableButtons(m_must_show_comments);
      }
    }

    #endregion Privates

    #region Publics

    public void InitializeControlResources()
    {
      //Title
      lbl_cards_title.Text = Resource.String("STR_UC_CARD_TITLE");

      //Labels
      lbl_account_id_name.Text = Resource.String("STR_FORMAT_GENERAL_NAME_ACCOUNT") + ":";
      lbl_id_title.Text = Resource.String("STR_FRM_PLAYER_EDIT_TYPE_DOCUMENT") + ":";
      lbl_birth_title.Text = Resource.String("STR_UC_PLAYER_TRACK_BIRTH") + ":";
      lbl_name.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_NAME") + ":";
      lbl_trackdata.Text = Resource.String("STR_FORMAT_GENERAL_NAME_CARD") + ":";
      lbl_points_balance_title.Text = Resource.String("STR_UC_CARD_POINTS_BALANCE_TITLE") + ":";
      lbl_points_balance_suffix.Text = Resource.String("STR_UC_CARD_POINTS_BALANCE_SUFFIX");
      lbl_level_title.Text = Resource.String("STR_UC_CARD_POINTS_LEVEL_TITLE") + ":";

      //Group Boxes
      gb_gifts.HeaderText = " " + Resource.String("STR_UC_CARD_GB_LOYALTY_PROGRAM") + " ";
      gb_card_operations.HeaderText = Resource.String("STR_UC_TITO_CARD_GB_CARD");
      gbAccount.HeaderText = Resource.String("STR_FORMAT_GENERAL_NAME_ACCOUNT");

      //Buttons
      btn_recycle_card.Image = Images.Get32x32(Images.CashierImage.Recycle);
      btn_new_card.Image = Images.Get32x32(Images.CashierImage.SwipeCard);
      btn_recycle_card.Text = Resource.String("STR_RECYCLE_CARD_TITLE");
      btn_print_card.Text = Resource.String("STR_UC_CARD_BTN_PRINT_CARD");
      btn_new_card.Text = Resource.String("STR_UC_CARD_BTN_NEW_CARD");
      btn_account_edit.Text = Resource.String("STR_UC_CARD_BTN_ACCOUNT_EDIT");
      btn_card_block.Text = Resource.String("STR_UC_CARD_BTN_CARD_BLOCK");
      btn_lasts_movements.Text = Resource.String("STR_UC_CARD_BTN_LAST_MOVEMENTS");
      btn_gifts.Text = Resource.String("STR_UC_CARD_BTN_GIFTS");
      btn_draw.Text = Resource.String("STR_UC_CARD_BTN_DRAW");
      btn_vouchers_space.Text = Resource.String("STR_VOUCHER_VOUCHER_BUTTON");
      btn_win_loss_statement_request.Text = Resource.String("STR_WIN_LOSS_STATEMENT_PLAYER_EDIT_REQUEST");

      //Others
      uc_card_reader1.InitializeControlResources();
      form_last_plays.InitializeControlResources();
      form_search_players.InitializeControlResources();

      btn_multiple_buckets.Text = Resource.String("STR_BUCKET_BUTTON");
      btn_pay_ticket.Text = Resource.String("STR_CREDIT_LINE_BTN_REDEEM_TICKET");

    }


    public void ConditionallyGenerateDrawTickets(OperationCode LastOpCode)
    {
      Boolean _generate;
      Int32 _flag;

      switch (LastOpCode)
      {
        case OperationCode.CASH_IN:
          if (!Int32.TryParse(WSI.Common.Misc.ReadGeneralParams("Cashier.DrawTicket", "AutomaticPrintAfterCashIn"), out _flag))
          {
            _flag = 0;
          }
          _generate = (_flag != 0);
          break;

        case OperationCode.CASH_OUT:
          if (!Int32.TryParse(WSI.Common.Misc.ReadGeneralParams("Cashier.DrawTicket", "AutomaticPrintAfterCashOut"), out _flag))
          {
            _flag = 0;
          }
          _generate = (_flag != 0);
          break;

        case OperationCode.DRAW_TICKET:
          _generate = true;
          break;

        default:
          _generate = false;
          break;
      }

      if (_generate)
      {
        GenerateDrawTickets(LastOpCode);
      }
    }

    private void GenerateDrawTickets(OperationCode LastOpCode)
    {
      Boolean _ok;
      DrawNumberList _awarded_numbers;
      DrawNumberList _real_awarded_numbers;
      Int64 _num_pending;
      String _message;
      DialogResult _yes_no;
      Boolean _show_available_draws;
      CardData _card;

      // RCI 17-JUN-2011: Possible General Param to indicate if show the available draws form.
      _show_available_draws = true;

      switch (LastOpCode)
      {
        case OperationCode.CASH_IN:
        case OperationCode.CASH_OUT:
        case OperationCode.DRAW_TICKET:
          break;

        default:
          Log.Warning("Unexpected Operation Code: " + LastOpCode + ".");

          return;
      }

      _card = new CardData();
      if (!CardData.DB_CardGetAllData(m_current_card_id, _card))
      {
        return;
      }
      // Check For Draw Numbers
      _ok = DrawsCashier.GenerateTickets(_card, true, null, out _awarded_numbers);
      if (!_ok)
      {
        // Show error message: Unable to check available draws.
        if (LastOpCode == OperationCode.DRAW_TICKET)
        {
          frm_message.Show(Resource.String("STR_DRAW_TICKETS_MSG_001"), Resource.String("STR_DRAW_TICKETS_TITLE"),
                           MessageBoxButtons.OK, MessageBoxIcon.Error, this.ParentForm);
        }

        return;
      }

      if (_awarded_numbers.Count == 0)
      {
        // Show info message: No tickets
        if (LastOpCode == OperationCode.DRAW_TICKET)
        {
          frm_message.Show(Resource.String("STR_VOUCHER_ERROR_MSG_DRAW"), Resource.String("STR_DRAW_TICKETS_TITLE"),
                           MessageBoxButtons.OK, MessageBoxIcon.Information, this.ParentForm);
        }

        return;
      }

      if (_show_available_draws && LastOpCode == OperationCode.DRAW_TICKET)
      {
        // Make a list of draws and show total and pending in the period.

        try
        {
          form_yes_no.Show(this.ParentForm);

          if (!form_available_draws.Show(ref _awarded_numbers, form_yes_no))
          {
            return;
          }
        }
        finally
        {
          form_yes_no.Hide();
          this.RestoreParentFocus();
        }
      }
      else
      {
        _num_pending = _awarded_numbers.NumPendingNumbers;

        if (_num_pending == 0)
        {
          // Show info message: No pending numbers to print.
          if (LastOpCode == OperationCode.DRAW_TICKET)
          {
            frm_message.Show(Resource.String("STR_DRAW_TICKETS_MSG_002"), Resource.String("STR_DRAW_TICKETS_TITLE"),
                               MessageBoxButtons.OK, MessageBoxIcon.Information, this.ParentForm);
          }
          return;
        }

        // Ask permission to print.
        _yes_no = DialogResult.OK;
        if (LastOpCode == OperationCode.DRAW_TICKET)
        {
          _yes_no = frm_message.Show(Resource.String("STR_DRAW_TICKETS_MSG_005"), Resource.String("STR_DRAW_TICKETS_TITLE"),
                                     MessageBoxButtons.YesNo, MessageBoxIcon.Information, this.ParentForm);
        }
        if (_yes_no != DialogResult.OK)
        {
          return;
        }
      }

      // RCI 05-JUL-2011: Used to refresh CardData current balance, for the DrawTicket movement.
      RefreshAccount();

      _card = new CardData();
      if (!CardData.DB_CardGetAllData(m_current_card_id, _card))
      {
        return;
      }

      // Create Tickets - For Draw Numbers
      _ok = DrawsCashier.GenerateTickets(_card, false, _awarded_numbers, out _real_awarded_numbers);
      if (!_ok)
      {
        if (_real_awarded_numbers.Count > 0)
        {
          // Error printing (Tickets have been saved)
          _message = Resource.String("STR_DRAW_TICKETS_MSG_006");
        }
        else
        {
          // Error generating numbers
          _message = Resource.String("STR_DRAW_TICKETS_MSG_007");
        }
        _message = _message.Replace("\\r\\n", "\r\n");
        frm_message.Show(_message, Resource.String("STR_DRAW_TICKETS_TITLE"),
                         MessageBoxButtons.OK, MessageBoxIcon.Error, this.ParentForm);

        return;
      }

      if (_real_awarded_numbers.NumPendingNumbers == 0)
      {
        // Show info message: No tickets
        if (LastOpCode == OperationCode.DRAW_TICKET)
        {
          frm_message.Show(Resource.String("STR_VOUCHER_ERROR_MSG_DRAW"), Resource.String("STR_DRAW_TICKETS_TITLE"),
                           MessageBoxButtons.OK, MessageBoxIcon.Information, this.ParentForm);
        }

        return;
      }

    } // GenerateDrawTickets


    public void InitControls(frm_container Parent)
    {
      m_parent = Parent;

      pb_user.Image = Resources.ResourceImages.iconoUsuario;
      pb_blocked.Image = WSI.Cashier.Images.Get(Images.CashierImage.Locked);

      ClearCardData();
      // RCI & AJQ 18-FEB-2011: Expire the timer: Disable buttons.
      m_tick_count_tracknumber_read = CashierBusinessLogic.DURATION_BUTTONS_ENABLED_AFTER_OPERATION + 1;

      EnableDisableButtons(false);

      if (m_first_time)
      {
        m_first_time = false;

        m_account_watcher = new AccountWatcher();
        m_account_watcher.Start();
        m_account_watcher.AccountChangedEvent += new AccountWatcher.AccountChangedHandler(account_watcher_AccountChangedEvent);

        uc_card_reader1.OnTrackNumberReadEvent += new uc_card_reader.TrackNumberReadEventHandler(uc_card_reader1_OnTrackNumberReadEvent);
        uc_card_reader1.OnTrackNumberReadingEvent += new uc_card_reader.TrackNumberReadingHandler(uc_card_reader1_OnTrackNumberReadingEvent);

        btn_recycle_card.Click += new EventHandler(btn_button_clicked);
        btn_new_card.Click += new EventHandler(btn_button_clicked);
        btn_card_block.Click += new EventHandler(btn_button_clicked);
        btn_account_edit.Click += new EventHandler(btn_button_clicked);
        btn_lasts_movements.Click += new EventHandler(btn_button_clicked);
        btn_print_card.Click += new EventHandler(btn_button_clicked);
        btn_gifts.Click += new EventHandler(btn_button_clicked);
        btn_player_browse.Click += new EventHandler(btn_button_clicked);
        btn_draw.Click += new EventHandler(btn_button_clicked);
        btn_vouchers_space.Click += new EventHandler(btn_button_clicked);
        btn_win_loss_statement_request.Click += new EventHandler(btn_button_clicked);

        InitializeControlResources();
      }
    }

    public void ClearCardData()
    {
      m_current_track_number = "";
      m_visible_track_number = "";

      RefreshData(new CardData());
    }

    public void InitFocus()
    {
      uc_card_reader1.Focus();
    }

    public CardData GetAccountWacherCard()
    {
      return m_account_watcher.Card;
    }

    #endregion Publics

    #region Events Buttons

    /// <summary>
    /// Show last card movements (plays)
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btn_last_plays_Click(object sender, EventArgs e)
    {
      String sql_str;

      if (m_current_card_id == 0)
      {
        return;
      }

      // Query to obtain last cards movements

      // MBF 08-FEB-2010 ROLLBACK Player Tracking

      //sql_str = "SELECT TOP 40 AM_TYPE, '  ' AS TYPE_NAME, AM_DATETIME, '  ' AS USER_NAME, TE_NAME, AM_TERMINAL_ID, AM_INITIAL_BALANCE, AM_SUB_AMOUNT, AM_ADD_AMOUNT, AM_FINAL_BALANCE, AM_ADD_POINTS " +
      //          "FROM ( SELECT TOP 40 AM_MOVEMENT_ID, AM_TYPE, AM_DATETIME, AM_TERMINAL_ID, AM_INITIAL_BALANCE, AM_ADD_AMOUNT, AM_SUB_AMOUNT, AM_FINAL_BALANCE, AM_ADD_POINTS " +
      //                  "FROM ACCOUNT_MOVEMENTS " +
      //                  "WHERE AM_TYPE NOT IN (14,15,16,17,19) AND AM_ACCOUNT_ID = " + m_current_card_id.ToString() + "  " +
      //                  "ORDER BY AM_DATETIME DESC, AM_MOVEMENT_ID DESC " +
      //               ") X LEFT OUTER JOIN TERMINALS AS T ON AM_TERMINAL_ID = T.TE_TERMINAL_ID ";

      // RCI 14-JUN-2010: Get TERMINAL or CASHIER Name and Id. Removed JOIN with TERMINALS Table: we have all 
      //                  data needed in ACCOUNT_MOVEMENTS Table.
      sql_str = "SELECT TOP 150 AM_TYPE " +
                "  , '  ' AS TYPE_NAME " +
                "  , AM_DATETIME " +
                "  , '  ' AS USER_NAME " +
                "  , CASE WHEN AM_TERMINAL_ID IS NOT NULL THEN AM_TERMINAL_NAME " +
                "         WHEN AM_CASHIER_ID IS NOT NULL THEN AM_CASHIER_NAME " +
                "         WHEN AM_TERMINAL_ID IS NULL AND AM_CASHIER_ID IS NULL THEN AM_CASHIER_NAME" +
                "          ELSE NULL " +
                "    END AM_TERM_NAME " +
                "  , CASE WHEN AM_TERMINAL_ID IS NOT NULL THEN AM_TERMINAL_ID " +
                "         WHEN AM_CASHIER_ID IS NOT NULL THEN AM_CASHIER_ID " +
                "         ELSE NULL " +
                "    END AM_TERM_ID " +
                "  , AM_INITIAL_BALANCE " +
                "  , AM_SUB_AMOUNT " +
                "  , AM_ADD_AMOUNT " +
                "  , AM_FINAL_BALANCE " +
                "  , ISNULL(AM_OPERATION_ID, 0) " +
                "  , '' AS COLOR_ROW " +
                "  , AM_DETAILS " +
                "  , AM_UNDO_STATUS " +
                "FROM ACCOUNT_MOVEMENTS " +
                "WHERE AM_ACCOUNT_ID = " + m_current_card_id.ToString() + "  " +
                "       AND AM_TYPE NOT IN ( " + (Int32)MovementType.HIDDEN_RechargeNotDoneWithCash + " ) " +
                "ORDER BY AM_DATETIME DESC, AM_MOVEMENT_ID DESC ";

      this.Cursor = Cursors.WaitCursor;
      form_last_plays.Show(sql_str, frm_last_movements.MovementShow.Card, m_account_watcher.Card.AccountId + " - " + m_account_watcher.Card.PlayerTracking.HolderName);
      this.Cursor = Cursors.Default;
    }

    /// <summary>
    /// Print Card
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btn_print_card_Click(object sender, EventArgs e)
    {
      // Check if current user is an authorized user
      String error_str;
      DialogResult msg_answer;

      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.PrintCard,
                                                 ProfilePermissions.TypeOperation.ShowMsg,
                                                 this.ParentForm,
                                                 out error_str))
      {
        return;
      }

      // Check the printer 
      if (!m_card_printer.PrinterExist())
      {
        frm_message.Show(Resource.String("STR_PRINT_CARD_PRINTER_IS_NOT_INSTALLED"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this.ParentForm);

        return;
      }

      // Ask for confirmation before printing 
      msg_answer = frm_message.Show(Resource.String("STR_PRINT_CARD_PRINTER_INPUT_INFO"),
                                    Resource.String("STR_PRINT_CARD_PRINTER_INPUT_INFO_MSG"),
                                    MessageBoxButtons.YesNo,
                                    MessageBoxIcon.Warning,
                                    this.ParentForm);

      if (msg_answer == DialogResult.Cancel)
      {
        return;
      }

      // Send document to print
      this.Cursor = Cursors.WaitCursor;

      m_card_printer.PrintNameInCard(lbl_holder_name.Text);

      this.Cursor = Cursors.Default;

    } // btn_print_card_Click

    private void btn_block_Click(object sender, EventArgs e)
    {
      CardData _card_data;
      frm_block_account _block_account;
      String _error_str;
      AccountBlockReason _block_reason_found;

      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.AccountLock, ProfilePermissions.TypeOperation.RequestPasswd, this.ParentForm, out _error_str))
      {
        return;
      }

      _card_data = m_account_watcher.Card;

      if (Accounts.ExistsBlockReasonInMask(new List<AccountBlockReason>(new AccountBlockReason[] {
                                                    AccountBlockReason.EXTERNAL_SYSTEM_CANCELED, AccountBlockReason.EXTERNAL_AML }),
                                           Accounts.ConvertOldBlockReason((Int32)_card_data.BlockReason), out _block_reason_found))
      {
        String _msg;

        _msg = "";
        switch (_block_reason_found)
        {
          case AccountBlockReason.EXTERNAL_SYSTEM_CANCELED:
            _msg = Resource.String("STR_CANCELLED_SPACE_ACCOUNTS");
            break;

          case AccountBlockReason.EXTERNAL_AML:
            _msg = Resource.String("STR_UNBLOCK_ERR_EXTERNAL_AML");
            break;
        }

        if (!String.IsNullOrEmpty(_msg))
        {
          frm_message.Show(_msg,
                           Resource.String("STR_APP_GEN_MSG_WARNING"),
                           MessageBoxButtons.OK,
                           MessageBoxIcon.Information,
                           ParentForm);
        }

        return;
      }
      _block_account = new frm_block_account(_card_data);

      try
      {
        if (!_block_account.Show(this.ParentForm))
        {
          ClearCardData();
          Log.Error("btn_block_Click: BlockUnblockAccount. Error block / unblock account. AccountId: " + _card_data.AccountId.ToString());

          return;
        }
        else
        {
          //EOR 29-MAR-2016 Update CardData with new state block
          if (_block_account.pCardData != null)
          {
            _card_data.Blocked = _block_account.pCardData.Blocked;
            _card_data.BlockDescription = _block_account.pCardData.BlockDescription;
            _card_data.BlockReason = _block_account.pCardData.BlockReason;
            RefreshData(_card_data);
          }
        }
      }
      finally
      {
        // RCI & ICS 16-APR-2014 Free the object
        _block_account.Dispose();
      }

      RefreshAccount();
    }

    private void btn_gifts_Click(object sender, EventArgs e)
    {
      CardData _card_data = new CardData();
      frm_account_points _account_points;

      if (m_current_card_id == 0)
      {
        return;
      }

      // Get Card data
      _card_data = m_account_watcher.Card;
      // Points Program checkings
      //      - Only available for non anonymous cards
      //      - Only if the account has points to exchange

      //      - Only available for non anonymous cards
      if (_card_data.PlayerTracking.HolderName == "")
      {
        string _message;
        string[] _message_params = { "", "", "", "", "", "" };

        _message_params[0] = _card_data.AccountId.ToString();

        _message = Resource.String("STR_FRM_ACCOUNT_POINTS_014", _message_params);
        _message = _message.Replace("\\r\\n", "\r\n");

        frm_message.Show(_message,
                          Resource.String("STR_APP_GEN_MSG_WARNING"),
                          MessageBoxButtons.OK,
                          MessageBoxIcon.Warning,
                          ParentForm);

        return;
      }

      _account_points = new frm_account_points(_card_data);
      _account_points.Show(this.ParentForm);
      _account_points.Dispose();

      return;
    } // btn_gifts_Click

    /// <summary>
    /// Log of Card Session: Apply when a card remained connected on a bet machine without being really connected.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btn_logoff_Click(object sender, EventArgs e)
    {
      DialogResult _dlg_result;
      Boolean _log_off_status;
      CardData _card_data;
      Decimal _current_balance;
      Decimal _calculated_balance;
      Decimal _reported_balance;
      Boolean _selected_by_user;
      Boolean _dummy_balance_changed;
      Decimal _new_balance;
      String _error_str;
      String _close_session_comment;

      // Check if current user is an authorized user
      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.AccountLogOff,
                                               ProfilePermissions.TypeOperation.RequestPasswd, this.ParentForm, out _error_str))
      {
        return;
      }

      _card_data = m_account_watcher.Card;

      _selected_by_user = _card_data.CurrentPlaySession.BalanceMismatch;
      _new_balance = 0;

      _current_balance = _card_data.CurrentPlaySession.FinalBalance;
      // RCI 22-JUN-2012: Have in account the CashIn while in session to calculate the final balance.
      _calculated_balance = Math.Max(0, _card_data.CurrentPlaySession.InitialBalance + _card_data.CurrentPlaySession.CashIn
                                        - _card_data.CurrentPlaySession.PlayedAmount + _card_data.CurrentPlaySession.WonAmount);

      CashierBusinessLogic.DB_GetReportedBalance(_card_data.CurrentPlaySession.PlaySessionId, out _reported_balance);

      _dlg_result = frm_balance_mismatch.Show(_current_balance, _calculated_balance, _reported_balance, this.ParentForm, _card_data.CurrentPlaySession.BalanceMismatch,
                                              out _dummy_balance_changed, out _new_balance, out _close_session_comment);

      if (_dlg_result == DialogResult.OK)
      {
        if (_new_balance == Math.Max(_calculated_balance, _reported_balance))
        {
          // Check permission for the highest balance
          if (!ProfilePermissions.CheckPermissionList(ProfilePermissions.CashierFormFuncionality.BalanceMismatchHighestBalance,
                                                      ProfilePermissions.TypeOperation.RequestPasswd,
                                                      this.ParentForm, 0, out _error_str))
          {
            return;
          }
        }
      }

      if (_dlg_result == DialogResult.OK)
      {
        String _user_and_cashier_name;

        if (String.IsNullOrEmpty(Cashier.AuthorizedByUserName))
        {
          _user_and_cashier_name = Cashier.TerminalName;
        }
        else
        {
          _user_and_cashier_name = Cashier.AuthorizedByUserName + "@" + Cashier.TerminalName;
        }

        // Card data may have changed, refresh it using account_watcher.
        _card_data = m_account_watcher.Card;
        _log_off_status = CardData.DB_PlaySessionCloseManually(m_current_card_id, _card_data.CurrentPlaySession.PlaySessionId, _card_data.CurrentPlaySession.FinalBalance,
                                                               _card_data.LoggedInTerminalId, _card_data.LoggedInTerminalName, _card_data.LoggedInTerminalType,
                                                               _user_and_cashier_name,
                                                               _selected_by_user, _new_balance, _close_session_comment);
      }

      // Refresh Card Data
      m_account_watcher.ForceRefresh();
    }

    private void btn_edit_Click(object sender, EventArgs e)
    {
      CardData _card_data;
      String error_str;
      DialogResult _dgr_ac_edit;

      if (m_is_new_card)
      {
        // AJQ 29-MAY-2013, Clear data on new Track read & Stop the AccountWatcher
        _card_data = new CardData();
        _card_data.TrackData = m_current_track_number;
        m_account_watcher.RefreshAccount(_card_data);
        CardData.DB_CardGetPersonalData(_card_data.AccountId, _card_data);
        ClearCardData();
      }
      else
      {
        ////0 byAccountId, 1 byAccountId+TrackData (Anonymous), 3 byAccountId+TrackData(Anonymous+Personal)
        if (m_account_watcher.Card.PlayerTracking.CardLevel == 0)
        {
          if (GeneralParam.GetInt32("AntiMoneyLaundering", "GroupByMode", 0) == 1)
          {
            frm_message.Show(Resource.String("STR_UC_CARD_AML_CANT_CUSTOMIZE_ACCOUNT"),
                              Resource.String("STR_MSG_ANTIMONEYLAUNDERING_WARNING"), MessageBoxButtons.OK, MessageBoxIcon.Warning, this.ParentForm);
            return;
          }
        }
        _card_data = m_account_watcher.Card;


        if (!m_must_show_comments)
        {
          // RMS 14-OCT-2013: not new card, check edition permission depending on account type anonymous or personal
          ProfilePermissions.CashierFormFuncionality _permission;

          _permission = (m_account_watcher.Card.PlayerTracking.CardLevel > 0) ? ProfilePermissions.CashierFormFuncionality.AccountEditPersonal :
                                                                              ProfilePermissions.CashierFormFuncionality.AccountEditAnonymous;

          if (!ProfilePermissions.CheckPermissionList(_permission, ProfilePermissions.TypeOperation.RequestPasswd, this.ParentForm, 0, out error_str))
          {
            return;
          }
        }

      }

      using (frm_yesno _shader = new frm_yesno())
      {
        using (PlayerEditDetailsCashierView _view = new PlayerEditDetailsCashierView())
        {
          using (PlayerEditDetailsCashierController _controller =
            new PlayerEditDetailsCashierController(_card_data, _view, IDScanner, PlayerEditDetailsCashierView.SCREEN_MODE.DEFAULT))
          {
            _controller.MustShowComments = m_must_show_comments;

            if (WindowManager.StartUpMode == WindowManager.StartUpModes.Explorer)
            {
              _view.Location = new Point(WindowManager.GetFormCenterLocation(_shader).X - (_view.Width / 2),
                _shader.Location.Y + 2);
            }

            _shader.Opacity = 0.6;
            _shader.Show();

            _dgr_ac_edit = _view.ShowDialog(_shader);
          }
        }
      }

      if (_dgr_ac_edit == DialogResult.OK)
      {
        // AJQ 29-MAY-2013, Reload the data of the "TrackData" being edited
        // Error: use the m_current_track_number that was refreshed on background)
        //        The background refresh has been disabled for tracks that are not associated to any account.
        CardData.DB_CardGetAllData(_card_data.TrackData, _card_data, false);
        m_is_new_card = _card_data.IsNew;
        m_pending_check_comments = false;
      }

      if (_card_data.AccountId != 0)
      {
        // AJQ 29-MAY-2013, Refresh the data on Screen when there is an account.
        //                  AccountId is 0 when the "Personalizacion" has been cancelled and the card was "new".
        //                  Avoids displaying the message "The card doesn't belong to the site".
        LoadCardByAccountId(_card_data.AccountId);
      }
    }

    private void btn_button_clicked(object sender, EventArgs e)
    {
      Cashier.ResetAuthorizedByUser();

      if (!uc_card_reader1.Focused)
      {
        uc_card_reader1.Focus();
      }

      try
      {
        if (sender.Equals(btn_new_card))
        {
          btn_new_card_Click(sender, e);
        }
        if (sender.Equals(btn_recycle_card))
        {
          btn_recycle_card_Click(sender, e);
        }
        if (sender.Equals(btn_card_block))
        {
          btn_block_Click(sender, e);
        }
        if (sender.Equals(btn_account_edit))
        {
          btn_edit_Click(sender, e);
        }
        if (sender.Equals(btn_lasts_movements))
        {
          btn_last_plays_Click(sender, e);
        }
        if (sender.Equals(btn_print_card))
        {
          btn_print_card_Click(sender, e);
        }
        if (sender.Equals(btn_gifts))
        {
          btn_gifts_Click(sender, e);
        }
        if (sender.Equals(btn_draw))
        {
          btn_draw_Click(sender, e);
        }

        if (sender.Equals(btn_win_loss_statement_request))
        {
          btn_win_loss_statement_request_Click(sender, e);
        }

        if (sender.Equals(btn_player_browse))
        {
          EnableDisableButtons(btn_player_browse_Click(sender, e));

          return;
        }

        if (sender.Equals(btn_vouchers_space))
        {
          vouchers_space_Click();
        }

        if (GeneralParam.GetBoolean("Cashier", "KeepEnabledButtonsAfterOperation", false) && !String.IsNullOrEmpty(m_visible_track_number))
        {
          m_tick_count_tracknumber_read = 0;
          EnableDisableButtons(true);
        }
        else
        {
          EnableDisableButtons(false);
        }
      }
      finally
      {
        Cashier.ResetAuthorizedByUser();

        if (!uc_card_reader1.Focused)
        {
          uc_card_reader1.Focus();
        }

        GC.Collect();
        ThreadPool.QueueUserWorkItem(delegate
        {
          GC.WaitForPendingFinalizers();
          GC.Collect();
        });

      }
    }

    private void btn_new_card_Click(object sender, EventArgs e)
    {
      String _error_str;
      CurrencyExchangeResult _exchange_result;

      Cashier.ResetAuthorizedByUser();

      try
      {
        if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.AccountAsociateCard,
                                                 ProfilePermissions.TypeOperation.RequestPasswd, this.ParentForm, out _error_str))
        {
          return;
        }

        frm_card_assign card_assign = new frm_card_assign();
        frm_yesno shader = new frm_yesno();

        shader.Opacity = 0.6f;
        shader.Show();

        card_assign.CardAssignParams(m_account_watcher.Card, 0, m_visible_track_number, out _exchange_result);
        card_assign.ShowDialog(shader);

        shader.Hide();
        shader.Dispose();

        card_assign.Hide();
        card_assign.Dispose();

        RefreshAccount();
      }
      finally
      {
        Cashier.ResetAuthorizedByUser();
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Evente fired when user press button for desasociate the actual card 
    //          with the account (Recycle card)    
    //          
    // 
    //  PARAMS:
    //      - INPUT:
    //      - OUTPUT:
    // RETURNS:
    //   NOTES:
    //
    private void btn_recycle_card_Click(object sender, EventArgs e)
    {
      String _error_str;
      CardData _card;
      String _message;

      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.RecyleCard,
                                               ProfilePermissions.TypeOperation.RequestPasswd, this.ParentForm, out _error_str))
      {
        return;
      }

      _message = Resource.String("STR_RECYCLE_CARD_ALERT");
      _message = _message.Replace("\\r\\n", "\r\n");

      if (frm_message.Show(_message, Resource.String("STR_RECYCLE_CARD_TITLE"),
                           MessageBoxButtons.YesNo, MessageBoxIcon.Warning, this.ParentForm) != DialogResult.OK)
      {
        return;
      }

      _card = m_account_watcher.Card;

      if (!CashierBusinessLogic.DB_CardRecycle(_card.TrackData, _card.AccountId))
      {
        frm_message.Show(Resource.String("STR_APP_GEN_ERROR_DATABASE_ERROR_WRITING"), Resource.String("STR_APP_GEN_MSG_ERROR"),
                         MessageBoxButtons.OK, MessageBoxIcon.Error, this.ParentForm);
        return;
      }

      RefreshAccount();

      m_current_track_number = CardData.RECYCLED_TRACK_DATA;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: High level handler for the MB Browse functionality (select from the 
    //          list of mobile banks associated to the current cashier session). 
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    private Boolean btn_player_browse_Click(object sender, EventArgs e)
    {
      Int64 _selected_account_id;
      Boolean _is_track_valid;
      Cursor _previous_cursor;
      String _error_str;

      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.AccountSearchByName,
                                               ProfilePermissions.TypeOperation.RequestPasswd,
                                               this.ParentForm,
                                               out _error_str))
      {
        return false;
      }

      _previous_cursor = this.Cursor;
      _selected_account_id = -1;
      _is_track_valid = true;

      try
      {
        this.Cursor = Cursors.WaitCursor;

        _selected_account_id = form_search_players.Show(this.ParentForm);

        if (_selected_account_id < 0)
        {
          return false;
        }

        uc_card_reader1_OnTrackNumberReadEvent(_selected_account_id, ref _is_track_valid);

        return _is_track_valid;
      }
      finally
      {
        this.Cursor = _previous_cursor;
      }
    }

    #endregion Events Buttons

    #region Event Controls

    private void timer1_Tick(object sender, EventArgs e)
    {
      m_tick_count_tracknumber_read += 1;

      if (m_tick_count_tracknumber_read >= CashierBusinessLogic.DURATION_BUTTONS_ENABLED_AFTER_OPERATION)
      {
        EnableDisableButtons(false);
        
        // 10-JAN-2014 JPJ     Account information in the AccountWatcher thread is cleared.
        if (m_account_watcher != null)
        {
          m_account_watcher.ClearAccount();
        }

        InitFocus();

        return;
      }
    }

    private void uc_card_reader1_OnTrackNumberReadingEvent()
    {
      EnableDisableButtons(false);
    }

    public void uc_card_reader1_OnTrackNumberReadEvent(String TrackNumber, ref Boolean IsValid)
    {
      CardData _card_data;

      _card_data = new CardData();

      CardData.DB_CardGetAllData(TrackNumber, _card_data, false);

      Log.Message(String.Format("Card reading - AccountId: {0}  Trackdata: {1}", _card_data.AccountId, TrackNumber));

      m_is_new_card = _card_data.IsNew;

      if (_card_data.IsNew)
      {
        _card_data.TrackData = TrackNumber;

        NewAccount(_card_data);
        m_tick_count_tracknumber_read = 0;
      }
      else
      {
        m_pending_check_comments = true;
        if (!_card_data.IsVirtualCard)
        {
          uc_card_reader1_OnTrackNumberReadEvent(_card_data.AccountId, ref IsValid);
        }
        else
        {
          IsValid = false;
        }
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Draw
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    private void btn_draw_Click(object sender, EventArgs e)
    {
      String _error_str;

      // Check if current user is an authorized user
      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.DrawTicketsPrint,
                                               ProfilePermissions.TypeOperation.RequestPasswd,
                                               this.ParentForm,
                                               out _error_str))
      {
        return;
      }

      ConditionallyGenerateDrawTickets(OperationCode.DRAW_TICKET);

    } // btn_draw_Click

    public void uc_card_reader1_OnTrackNumberReadEvent(Int64 AccountId, ref Boolean IsValid)
    {
      //Reset timer to disable buttons
      m_tick_count_tracknumber_read = 0;
      // Load and load track number
      m_pending_check_comments = true;
      IsValid = LoadCardByAccountId(AccountId);

      m_is_new_card = false;

      if (!IsValid)
      {
        EnableDisableButtons(IsValid);
      }
    }

    private void btn_win_loss_statement_request_Click(object sender, EventArgs e)
    {
      frm_win_loss_statement _frm_win_loss_statement;

      String error_str;
      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.WinLossStatementRequest, ProfilePermissions.TypeOperation.RequestPasswd, this.ParentForm, out error_str))
      {
        return;
      }

      _frm_win_loss_statement = new frm_win_loss_statement(m_account_watcher.Card);

      try
      {

        form_yes_no.Show(ParentForm);
        _frm_win_loss_statement.Show();
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
      finally
      {
        form_yes_no.Hide();
        _frm_win_loss_statement.Dispose();
        uc_card_reader1.Focus();
      }
    }

    #endregion Event Controls

    private void vouchers_space_Click()
    {
      CardData _card_data = new CardData();
      frm_voucher_space vs = new frm_voucher_space();

      _card_data = m_account_watcher.Card;

      try
      {
        form_yes_no.Show(ParentForm);
        vs.Show(_card_data, form_yes_no);
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
      finally
      {
        form_yes_no.Hide();
        vs.Dispose();
        uc_card_reader1.Focus();
      }
    }

    /// <summary>
    /// Returns if there are buckets enabled
    /// </summary>
    /// <returns></returns>
    private Boolean HasBucketsEnabled()
    {
      BucketsForm _buckets_frm;

      _buckets_frm = new BucketsForm();

      return (_buckets_frm.HasBucketsEnabled());
    }

    private void btn_multiple_buckets_Click(object sender, EventArgs e)
    {
      //RAB 19-JAN-2016
      String error_str;

      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.SetBuckets,
                                                 ProfilePermissions.TypeOperation.RequestPasswd,
                                                 this.ParentForm,
                                                 out error_str))
      {
        return;
      }

      //FGB 13-OCT-2016 Check if there are buckets enabled
      if (!HasBucketsEnabled())
      {
        frm_message.Show(Resource.String("STR_BUCKET_THERE_ARE_NO_BUCKET_ENABLED")
                       , Resource.String("STR_APP_GEN_MSG_WARNING")
                       , MessageBoxButtons.OK
                       , MessageBoxIcon.Error
                       , this.ParentForm);
        return;
      }

      CardData _card_data = new CardData();
      frm_add_amount_multiple_buckets vs = new frm_add_amount_multiple_buckets();

      _card_data = m_account_watcher.Card;

      try
      {
        form_yes_no.Show(ParentForm);
        vs.Show(_card_data, form_yes_no);
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
      finally
      {
        form_yes_no.Hide();
        vs.Dispose();
        uc_card_reader1.Focus();
      }
    } // multiple_buckets_Click

    ///
    private void btn_pay_ticket_Click(object sender, EventArgs e)
    {
      CardData _card_data;
      _card_data = m_account_watcher.Card;

      if (!CreditLine_CheckUserDebt(_card_data.AccountId))
      {
        return;
      }

      m_parent.ticket_redeem_Click();
    } // pay_ticket_click

    /// <summary>
    /// Check if user can withdraw with credit line debt.
    /// </summary>
    /// <param name="Debt"></param>
    /// <returns></returns>
    private Boolean CreditLine_CheckUserDebt(Int64 AccountId)
    {
      String _message;
      CreditLine _credit_line;
      Currency _debt;
      _credit_line = new CreditLine();

      //CreditLine Disabled.
      if (!GeneralParam.GetBoolean("CreditLine", "Enabled", false))
      {
        return true;
      }

      using (DB_TRX _db_trx = new DB_TRX())
      {
        _credit_line.DB_GetByAccountId(AccountId, _db_trx.SqlTransaction);
      }
      _debt = _credit_line.SpentAmount;

      //No Debt.
      if (_debt == 0)
      {
        return true;
      }

      _message = Resource.String("STR_CREDIT_LINE_WITH_DEBT_MESSAGE", _debt.ToString());
      _message = _message.Replace("\\r\\n", "\r\n");
      if (!(frm_message.Show(_message, Resource.String("STR_APP_GEN_MSG_WARNING"),
                       MessageBoxButtons.YesNo, MessageBoxIcon.Error, this.ParentForm) == DialogResult.OK))
      {
        return false;
      }

      return true;
    } // CreditLine_CheckUserDebt

    private void pb_user_Click(object sender, EventArgs e)
    {
      /*frm_player_photo _frm_player_photo;

      if (GeneralParam.GetBoolean("Account", "PlayerPicture.AllowZoom", false))
      {
        form_yes_no.Show(m_parent);

        _frm_player_photo = new frm_player_photo();
        _frm_player_photo.PlayerPhoto = pb_user.Image;
        _frm_player_photo.ShowDialog();

        form_yes_no.Hide();
      }*/
      if (GeneralParam.GetBoolean("Account", "PlayerPicture.AllowZoom", false))
      {
        if (m_exist_photo)
        {
          frm_account_photo frm = new frm_account_photo(lbl_holder_name.Text, this.pb_user.Image);
          frm.ShowDialog();
        }
      }
    } 
  }
}

