namespace WSI.Cashier
{
  partial class uc_ticket_create
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(uc_ticket_create));
      this.lbl_separator_1 = new System.Windows.Forms.Label();
      this.timer1 = new System.Windows.Forms.Timer(this.components);
      this.pb_uc_icon = new System.Windows.Forms.PictureBox();
      this.gb_cash_advance = new WSI.Cashier.Controls.uc_round_panel();
      this.btn_undo_cash_advance = new WSI.Cashier.Controls.uc_round_button();
      this.btn_cash_advance = new WSI.Cashier.Controls.uc_round_button();
      this.gb_tickets = new WSI.Cashier.Controls.uc_round_panel();
      this.btn_create_ticket = new WSI.Cashier.Controls.uc_round_button();
      this.btn_promotions = new WSI.Cashier.Controls.uc_round_button();
      this.gb_divisas = new WSI.Cashier.Controls.uc_round_panel();
      this.btn_undo_last_devolution_currency = new WSI.Cashier.Controls.uc_round_button();
      this.btn_undo_last_exchange_currency = new WSI.Cashier.Controls.uc_round_button();
      this.btn_change_currency = new WSI.Cashier.Controls.uc_round_button();
      this.btn_devolution_currency = new WSI.Cashier.Controls.uc_round_button();
      this.btn_player_browse = new WSI.Cashier.Controls.uc_round_button();
      this.btn_terminal_transfer = new WSI.Cashier.Controls.uc_round_button();
      this.gb_chips = new WSI.Cashier.Controls.uc_round_panel();
      this.btn_chips_sale_with_recharge = new WSI.Cashier.Controls.uc_round_button();
      this.btn_chips_purchase = new WSI.Cashier.Controls.uc_round_button();
      this.btn_undo_chips_sale_with_recharge = new WSI.Cashier.Controls.uc_round_button();
      this.btn_undo_chips_purchase = new WSI.Cashier.Controls.uc_round_button();
      this.gb_account = new WSI.Cashier.Controls.uc_round_panel();
      this.lbl_vip_account = new WSI.Cashier.Controls.uc_label();
      this.pb_user = new System.Windows.Forms.PictureBox();
      this.lbl_birth = new WSI.Cashier.Controls.uc_label();
      this.lbl_holder_name = new WSI.Cashier.Controls.uc_label();
      this.lbl_account_id_value = new WSI.Cashier.Controls.uc_label();
      this.lbl_account_id_name = new WSI.Cashier.Controls.uc_label();
      this.lbl_block_reason = new WSI.Cashier.Controls.uc_label();
      this.lbl_birth_title = new WSI.Cashier.Controls.uc_label();
      this.lbl_blocked = new WSI.Cashier.Controls.uc_label();
      this.lbl_track_number = new WSI.Cashier.Controls.uc_label();
      this.lbl_id_title = new WSI.Cashier.Controls.uc_label();
      this.lbl_trackdata = new WSI.Cashier.Controls.uc_label();
      this.lbl_id = new WSI.Cashier.Controls.uc_label();
      this.lbl_name = new WSI.Cashier.Controls.uc_label();
      this.lbl_happy_birthday = new WSI.Cashier.Controls.uc_label();
      this.pb_blocked = new System.Windows.Forms.PictureBox();
      this.uc_card_reader1 = new WSI.Cashier.uc_card_reader();
      ((System.ComponentModel.ISupportInitialize)(this.pb_uc_icon)).BeginInit();
      this.gb_cash_advance.SuspendLayout();
      this.gb_tickets.SuspendLayout();
      this.gb_divisas.SuspendLayout();
      this.gb_chips.SuspendLayout();
      this.gb_account.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pb_user)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.pb_blocked)).BeginInit();
      this.SuspendLayout();
      // 
      // lbl_separator_1
      // 
      this.lbl_separator_1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_separator_1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(95)))), ((int)(((byte)(95)))));
      this.lbl_separator_1.Location = new System.Drawing.Point(-3, 60);
      this.lbl_separator_1.Name = "lbl_separator_1";
      this.lbl_separator_1.Size = new System.Drawing.Size(885, 1);
      this.lbl_separator_1.TabIndex = 87;
      // 
      // timer1
      // 
      this.timer1.Interval = 500;
      // 
      // pb_uc_icon
      // 
      this.pb_uc_icon.Image = global::WSI.Cashier.Properties.Resources.otherOpt_header;
      this.pb_uc_icon.Location = new System.Drawing.Point(10, 7);
      this.pb_uc_icon.Name = "pb_uc_icon";
      this.pb_uc_icon.Size = new System.Drawing.Size(50, 50);
      this.pb_uc_icon.TabIndex = 123;
      this.pb_uc_icon.TabStop = false;
      // 
      // gb_cash_advance
      // 
      this.gb_cash_advance.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.gb_cash_advance.BackColor = System.Drawing.Color.Transparent;
      this.gb_cash_advance.BorderColor = System.Drawing.Color.Empty;
      this.gb_cash_advance.Controls.Add(this.btn_undo_cash_advance);
      this.gb_cash_advance.Controls.Add(this.btn_cash_advance);
      this.gb_cash_advance.CornerRadius = 10;
      this.gb_cash_advance.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_cash_advance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_cash_advance.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gb_cash_advance.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_cash_advance.HeaderHeight = 35;
      this.gb_cash_advance.HeaderSubText = null;
      this.gb_cash_advance.HeaderText = null;
      this.gb_cash_advance.Location = new System.Drawing.Point(512, 478);
      this.gb_cash_advance.Name = "gb_cash_advance";
      this.gb_cash_advance.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_cash_advance.Size = new System.Drawing.Size(351, 199);
      this.gb_cash_advance.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.gb_cash_advance.TabIndex = 5;
      this.gb_cash_advance.Visible = false;
      // 
      // btn_undo_cash_advance
      // 
      this.btn_undo_cash_advance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_undo_cash_advance.FlatAppearance.BorderSize = 0;
      this.btn_undo_cash_advance.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_undo_cash_advance.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_undo_cash_advance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_undo_cash_advance.Image = null;
      this.btn_undo_cash_advance.IsSelected = false;
      this.btn_undo_cash_advance.Location = new System.Drawing.Point(14, 50);
      this.btn_undo_cash_advance.Name = "btn_undo_cash_advance";
      this.btn_undo_cash_advance.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_undo_cash_advance.Size = new System.Drawing.Size(155, 60);
      this.btn_undo_cash_advance.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_undo_cash_advance.TabIndex = 0;
      this.btn_undo_cash_advance.Text = "XUNDO LAST CASH ADVANCE";
      this.btn_undo_cash_advance.UseVisualStyleBackColor = false;
      // 
      // btn_cash_advance
      // 
      this.btn_cash_advance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_cash_advance.FlatAppearance.BorderSize = 0;
      this.btn_cash_advance.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_cash_advance.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_cash_advance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_cash_advance.Image = null;
      this.btn_cash_advance.IsSelected = false;
      this.btn_cash_advance.Location = new System.Drawing.Point(182, 50);
      this.btn_cash_advance.Name = "btn_cash_advance";
      this.btn_cash_advance.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_cash_advance.Size = new System.Drawing.Size(155, 60);
      this.btn_cash_advance.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_cash_advance.TabIndex = 1;
      this.btn_cash_advance.Text = "XCASH ADVANCE";
      this.btn_cash_advance.UseVisualStyleBackColor = false;
      // 
      // gb_tickets
      // 
      this.gb_tickets.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.gb_tickets.BackColor = System.Drawing.Color.Transparent;
      this.gb_tickets.BorderColor = System.Drawing.Color.Empty;
      this.gb_tickets.Controls.Add(this.btn_create_ticket);
      this.gb_tickets.Controls.Add(this.btn_promotions);
      this.gb_tickets.CornerRadius = 10;
      this.gb_tickets.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_tickets.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_tickets.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gb_tickets.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_tickets.HeaderHeight = 35;
      this.gb_tickets.HeaderSubText = null;
      this.gb_tickets.HeaderText = null;
      this.gb_tickets.Location = new System.Drawing.Point(512, 319);
      this.gb_tickets.Name = "gb_tickets";
      this.gb_tickets.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_tickets.Size = new System.Drawing.Size(351, 130);
      this.gb_tickets.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.gb_tickets.TabIndex = 3;
      this.gb_tickets.Visible = false;
      // 
      // btn_create_ticket
      // 
      this.btn_create_ticket.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_create_ticket.FlatAppearance.BorderSize = 0;
      this.btn_create_ticket.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_create_ticket.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_create_ticket.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_create_ticket.Image = null;
      this.btn_create_ticket.IsSelected = false;
      this.btn_create_ticket.Location = new System.Drawing.Point(182, 50);
      this.btn_create_ticket.Name = "btn_create_ticket";
      this.btn_create_ticket.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_create_ticket.Size = new System.Drawing.Size(155, 60);
      this.btn_create_ticket.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_create_ticket.TabIndex = 1;
      this.btn_create_ticket.Text = "XCREATETICKET";
      this.btn_create_ticket.UseVisualStyleBackColor = false;
      // 
      // btn_promotions
      // 
      this.btn_promotions.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_promotions.FlatAppearance.BorderSize = 0;
      this.btn_promotions.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_promotions.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_promotions.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_promotions.Image = null;
      this.btn_promotions.IsSelected = false;
      this.btn_promotions.Location = new System.Drawing.Point(14, 50);
      this.btn_promotions.Name = "btn_promotions";
      this.btn_promotions.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_promotions.Size = new System.Drawing.Size(155, 60);
      this.btn_promotions.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_promotions.TabIndex = 0;
      this.btn_promotions.Text = "XPROMOTIONS";
      this.btn_promotions.UseVisualStyleBackColor = false;
      // 
      // gb_divisas
      // 
      this.gb_divisas.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.gb_divisas.BackColor = System.Drawing.Color.Transparent;
      this.gb_divisas.BorderColor = System.Drawing.Color.Empty;
      this.gb_divisas.Controls.Add(this.btn_undo_last_devolution_currency);
      this.gb_divisas.Controls.Add(this.btn_undo_last_exchange_currency);
      this.gb_divisas.Controls.Add(this.btn_change_currency);
      this.gb_divisas.Controls.Add(this.btn_devolution_currency);
      this.gb_divisas.CornerRadius = 10;
      this.gb_divisas.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_divisas.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_divisas.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gb_divisas.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_divisas.HeaderHeight = 35;
      this.gb_divisas.HeaderSubText = null;
      this.gb_divisas.HeaderText = null;
      this.gb_divisas.Location = new System.Drawing.Point(25, 478);
      this.gb_divisas.Name = "gb_divisas";
      this.gb_divisas.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_divisas.Size = new System.Drawing.Size(462, 199);
      this.gb_divisas.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.gb_divisas.TabIndex = 4;
      this.gb_divisas.Visible = false;
      // 
      // btn_undo_last_devolution_currency
      // 
      this.btn_undo_last_devolution_currency.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.btn_undo_last_devolution_currency.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_undo_last_devolution_currency.FlatAppearance.BorderSize = 0;
      this.btn_undo_last_devolution_currency.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_undo_last_devolution_currency.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_undo_last_devolution_currency.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_undo_last_devolution_currency.Image = null;
      this.btn_undo_last_devolution_currency.IsSelected = false;
      this.btn_undo_last_devolution_currency.Location = new System.Drawing.Point(235, 123);
      this.btn_undo_last_devolution_currency.Name = "btn_undo_last_devolution_currency";
      this.btn_undo_last_devolution_currency.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_undo_last_devolution_currency.Size = new System.Drawing.Size(155, 60);
      this.btn_undo_last_devolution_currency.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_undo_last_devolution_currency.TabIndex = 2;
      this.btn_undo_last_devolution_currency.Text = "XUNDODEVOLUTION";
      this.btn_undo_last_devolution_currency.UseVisualStyleBackColor = false;
      // 
      // btn_undo_last_exchange_currency
      // 
      this.btn_undo_last_exchange_currency.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.btn_undo_last_exchange_currency.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_undo_last_exchange_currency.FlatAppearance.BorderSize = 0;
      this.btn_undo_last_exchange_currency.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_undo_last_exchange_currency.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_undo_last_exchange_currency.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_undo_last_exchange_currency.Image = null;
      this.btn_undo_last_exchange_currency.IsSelected = false;
      this.btn_undo_last_exchange_currency.Location = new System.Drawing.Point(69, 123);
      this.btn_undo_last_exchange_currency.Name = "btn_undo_last_exchange_currency";
      this.btn_undo_last_exchange_currency.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_undo_last_exchange_currency.Size = new System.Drawing.Size(155, 60);
      this.btn_undo_last_exchange_currency.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_undo_last_exchange_currency.TabIndex = 0;
      this.btn_undo_last_exchange_currency.Text = "XUNDOCHANGE";
      this.btn_undo_last_exchange_currency.UseVisualStyleBackColor = false;
      // 
      // btn_change_currency
      // 
      this.btn_change_currency.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.btn_change_currency.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_change_currency.FlatAppearance.BorderSize = 0;
      this.btn_change_currency.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_change_currency.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_change_currency.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_change_currency.Image = null;
      this.btn_change_currency.IsSelected = false;
      this.btn_change_currency.Location = new System.Drawing.Point(69, 50);
      this.btn_change_currency.Name = "btn_change_currency";
      this.btn_change_currency.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_change_currency.Size = new System.Drawing.Size(155, 60);
      this.btn_change_currency.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_change_currency.TabIndex = 1;
      this.btn_change_currency.Text = "XCHANGECURRENCY";
      this.btn_change_currency.UseVisualStyleBackColor = false;
      // 
      // btn_devolution_currency
      // 
      this.btn_devolution_currency.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.btn_devolution_currency.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_devolution_currency.FlatAppearance.BorderSize = 0;
      this.btn_devolution_currency.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_devolution_currency.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_devolution_currency.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_devolution_currency.Image = null;
      this.btn_devolution_currency.IsSelected = false;
      this.btn_devolution_currency.Location = new System.Drawing.Point(235, 50);
      this.btn_devolution_currency.Name = "btn_devolution_currency";
      this.btn_devolution_currency.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_devolution_currency.Size = new System.Drawing.Size(155, 60);
      this.btn_devolution_currency.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_devolution_currency.TabIndex = 3;
      this.btn_devolution_currency.Text = "XDEVOLUTIONCURRENCY";
      this.btn_devolution_currency.UseVisualStyleBackColor = false;
      // 
      // btn_player_browse
      // 
      this.btn_player_browse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_player_browse.BackColor = System.Drawing.Color.Transparent;
      this.btn_player_browse.BackgroundImage = global::WSI.Cashier.Properties.Resources.btnSearch;
      this.btn_player_browse.CornerRadius = 0;
      this.btn_player_browse.FlatAppearance.BorderSize = 0;
      this.btn_player_browse.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_player_browse.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btn_player_browse.Image = null;
      this.btn_player_browse.IsSelected = false;
      this.btn_player_browse.Location = new System.Drawing.Point(815, 3);
      this.btn_player_browse.Name = "btn_player_browse";
      this.btn_player_browse.SelectedColor = System.Drawing.Color.Empty;
      this.btn_player_browse.Size = new System.Drawing.Size(55, 55);
      this.btn_player_browse.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.NONE;
      this.btn_player_browse.TabIndex = 1;
      this.btn_player_browse.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      this.btn_player_browse.UseVisualStyleBackColor = false;
      this.btn_player_browse.Visible = false;
      // 
      // btn_terminal_transfer
      // 
      this.btn_terminal_transfer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_terminal_transfer.FlatAppearance.BorderSize = 0;
      this.btn_terminal_transfer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_terminal_transfer.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_terminal_transfer.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_terminal_transfer.Image = null;
      this.btn_terminal_transfer.IsSelected = false;
      this.btn_terminal_transfer.Location = new System.Drawing.Point(31, 634);
      this.btn_terminal_transfer.Name = "btn_terminal_transfer";
      this.btn_terminal_transfer.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_terminal_transfer.Size = new System.Drawing.Size(155, 60);
      this.btn_terminal_transfer.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_terminal_transfer.TabIndex = 7;
      this.btn_terminal_transfer.Text = "TRANSFERIR A TERMINAL";
      this.btn_terminal_transfer.UseVisualStyleBackColor = false;
      this.btn_terminal_transfer.Click += new System.EventHandler(this.btn_terminal_transfer_Click);
      // 
      // gb_chips
      // 
      this.gb_chips.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.gb_chips.BackColor = System.Drawing.Color.Transparent;
      this.gb_chips.BorderColor = System.Drawing.Color.Empty;
      this.gb_chips.Controls.Add(this.btn_chips_sale_with_recharge);
      this.gb_chips.Controls.Add(this.btn_chips_purchase);
      this.gb_chips.Controls.Add(this.btn_undo_chips_sale_with_recharge);
      this.gb_chips.Controls.Add(this.btn_undo_chips_purchase);
      this.gb_chips.CornerRadius = 10;
      this.gb_chips.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_chips.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_chips.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gb_chips.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_chips.HeaderHeight = 35;
      this.gb_chips.HeaderSubText = null;
      this.gb_chips.HeaderText = null;
      this.gb_chips.Location = new System.Drawing.Point(512, 89);
      this.gb_chips.Name = "gb_chips";
      this.gb_chips.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_chips.Size = new System.Drawing.Size(351, 199);
      this.gb_chips.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.gb_chips.TabIndex = 2;
      this.gb_chips.Visible = false;
      // 
      // btn_chips_sale_with_recharge
      // 
      this.btn_chips_sale_with_recharge.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_chips_sale_with_recharge.FlatAppearance.BorderSize = 0;
      this.btn_chips_sale_with_recharge.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_chips_sale_with_recharge.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_chips_sale_with_recharge.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_chips_sale_with_recharge.Image = null;
      this.btn_chips_sale_with_recharge.IsSelected = false;
      this.btn_chips_sale_with_recharge.Location = new System.Drawing.Point(14, 50);
      this.btn_chips_sale_with_recharge.Name = "btn_chips_sale_with_recharge";
      this.btn_chips_sale_with_recharge.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_chips_sale_with_recharge.Size = new System.Drawing.Size(155, 60);
      this.btn_chips_sale_with_recharge.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_chips_sale_with_recharge.TabIndex = 0;
      this.btn_chips_sale_with_recharge.Text = "XCHIPS SALE WITH RECHARGE";
      this.btn_chips_sale_with_recharge.UseVisualStyleBackColor = false;
      // 
      // btn_chips_purchase
      // 
      this.btn_chips_purchase.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_chips_purchase.FlatAppearance.BorderSize = 0;
      this.btn_chips_purchase.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_chips_purchase.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_chips_purchase.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_chips_purchase.Image = null;
      this.btn_chips_purchase.IsSelected = false;
      this.btn_chips_purchase.Location = new System.Drawing.Point(182, 50);
      this.btn_chips_purchase.Name = "btn_chips_purchase";
      this.btn_chips_purchase.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_chips_purchase.Size = new System.Drawing.Size(155, 60);
      this.btn_chips_purchase.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_chips_purchase.TabIndex = 1;
      this.btn_chips_purchase.Text = "XCHIPS PURCHASE";
      this.btn_chips_purchase.UseVisualStyleBackColor = false;
      // 
      // btn_undo_chips_sale_with_recharge
      // 
      this.btn_undo_chips_sale_with_recharge.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_undo_chips_sale_with_recharge.FlatAppearance.BorderSize = 0;
      this.btn_undo_chips_sale_with_recharge.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_undo_chips_sale_with_recharge.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_undo_chips_sale_with_recharge.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_undo_chips_sale_with_recharge.Image = null;
      this.btn_undo_chips_sale_with_recharge.IsSelected = false;
      this.btn_undo_chips_sale_with_recharge.Location = new System.Drawing.Point(14, 123);
      this.btn_undo_chips_sale_with_recharge.Name = "btn_undo_chips_sale_with_recharge";
      this.btn_undo_chips_sale_with_recharge.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_undo_chips_sale_with_recharge.Size = new System.Drawing.Size(155, 60);
      this.btn_undo_chips_sale_with_recharge.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_undo_chips_sale_with_recharge.TabIndex = 2;
      this.btn_undo_chips_sale_with_recharge.Text = "XUNDO LAST CHIPS SALE WITH RECHARGE";
      this.btn_undo_chips_sale_with_recharge.UseVisualStyleBackColor = false;
      // 
      // btn_undo_chips_purchase
      // 
      this.btn_undo_chips_purchase.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_undo_chips_purchase.FlatAppearance.BorderSize = 0;
      this.btn_undo_chips_purchase.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_undo_chips_purchase.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_undo_chips_purchase.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_undo_chips_purchase.Image = null;
      this.btn_undo_chips_purchase.IsSelected = false;
      this.btn_undo_chips_purchase.Location = new System.Drawing.Point(182, 123);
      this.btn_undo_chips_purchase.Name = "btn_undo_chips_purchase";
      this.btn_undo_chips_purchase.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_undo_chips_purchase.Size = new System.Drawing.Size(155, 60);
      this.btn_undo_chips_purchase.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_undo_chips_purchase.TabIndex = 3;
      this.btn_undo_chips_purchase.Text = "XUNDO LAST CHIPS PURCHASE";
      this.btn_undo_chips_purchase.UseVisualStyleBackColor = false;
      // 
      // gb_account
      // 
      this.gb_account.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.gb_account.BackColor = System.Drawing.Color.Transparent;
      this.gb_account.BorderColor = System.Drawing.Color.Empty;
      this.gb_account.Controls.Add(this.lbl_vip_account);
      this.gb_account.Controls.Add(this.pb_user);
      this.gb_account.Controls.Add(this.lbl_birth);
      this.gb_account.Controls.Add(this.lbl_holder_name);
      this.gb_account.Controls.Add(this.lbl_account_id_value);
      this.gb_account.Controls.Add(this.lbl_account_id_name);
      this.gb_account.Controls.Add(this.lbl_block_reason);
      this.gb_account.Controls.Add(this.lbl_birth_title);
      this.gb_account.Controls.Add(this.lbl_blocked);
      this.gb_account.Controls.Add(this.lbl_track_number);
      this.gb_account.Controls.Add(this.lbl_id_title);
      this.gb_account.Controls.Add(this.lbl_trackdata);
      this.gb_account.Controls.Add(this.lbl_id);
      this.gb_account.Controls.Add(this.lbl_name);
      this.gb_account.Controls.Add(this.lbl_happy_birthday);
      this.gb_account.Controls.Add(this.pb_blocked);
      this.gb_account.CornerRadius = 10;
      this.gb_account.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_account.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gb_account.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_account.HeaderHeight = 35;
      this.gb_account.HeaderSubText = null;
      this.gb_account.HeaderText = null;
      this.gb_account.Location = new System.Drawing.Point(25, 89);
      this.gb_account.Name = "gb_account";
      this.gb_account.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_account.Size = new System.Drawing.Size(462, 360);
      this.gb_account.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.gb_account.TabIndex = 8;
      // 
      // lbl_vip_account
      // 
      this.lbl_vip_account.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_vip_account.BackColor = System.Drawing.Color.Transparent;
      this.lbl_vip_account.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_vip_account.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.lbl_vip_account.Location = new System.Drawing.Point(61, 85);
      this.lbl_vip_account.Name = "lbl_vip_account";
      this.lbl_vip_account.Size = new System.Drawing.Size(178, 23);
      this.lbl_vip_account.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_RED;
      this.lbl_vip_account.TabIndex = 2;
      this.lbl_vip_account.Text = "xVip Account";
      this.lbl_vip_account.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // pb_user
      // 
      this.pb_user.Image = global::WSI.Cashier.Properties.Resources.anonymous_user;
      this.pb_user.InitialImage = ((System.Drawing.Image)(resources.GetObject("pb_user.InitialImage")));
      this.pb_user.Location = new System.Drawing.Point(6, 48);
      this.pb_user.Name = "pb_user";
      this.pb_user.Size = new System.Drawing.Size(48, 46);
      this.pb_user.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
      this.pb_user.TabIndex = 72;
      this.pb_user.TabStop = false;
      this.pb_user.Click += new System.EventHandler(this.pb_user_Click);
      // 
      // lbl_birth
      // 
      this.lbl_birth.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_birth.BackColor = System.Drawing.Color.Transparent;
      this.lbl_birth.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_birth.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_birth.Location = new System.Drawing.Point(119, 247);
      this.lbl_birth.Name = "lbl_birth";
      this.lbl_birth.Size = new System.Drawing.Size(331, 23);
      this.lbl_birth.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_birth.TabIndex = 12;
      this.lbl_birth.Text = "99 / 99 / 9999";
      this.lbl_birth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_holder_name
      // 
      this.lbl_holder_name.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_holder_name.BackColor = System.Drawing.Color.Transparent;
      this.lbl_holder_name.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_holder_name.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_holder_name.Location = new System.Drawing.Point(7, 166);
      this.lbl_holder_name.Name = "lbl_holder_name";
      this.lbl_holder_name.Size = new System.Drawing.Size(443, 23);
      this.lbl_holder_name.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_holder_name.TabIndex = 8;
      this.lbl_holder_name.Text = "TTTTTTTTTTTTT";
      this.lbl_holder_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.lbl_holder_name.UseMnemonic = false;
      // 
      // lbl_account_id_value
      // 
      this.lbl_account_id_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_account_id_value.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_account_id_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_account_id_value.Location = new System.Drawing.Point(140, 60);
      this.lbl_account_id_value.Name = "lbl_account_id_value";
      this.lbl_account_id_value.Size = new System.Drawing.Size(113, 21);
      this.lbl_account_id_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_account_id_value.TabIndex = 1;
      this.lbl_account_id_value.Text = "012345";
      this.lbl_account_id_value.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_account_id_name
      // 
      this.lbl_account_id_name.BackColor = System.Drawing.Color.Transparent;
      this.lbl_account_id_name.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_account_id_name.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_account_id_name.Location = new System.Drawing.Point(65, 60);
      this.lbl_account_id_name.Name = "lbl_account_id_name";
      this.lbl_account_id_name.Size = new System.Drawing.Size(74, 21);
      this.lbl_account_id_name.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_account_id_name.TabIndex = 0;
      this.lbl_account_id_name.Text = "xAccount";
      this.lbl_account_id_name.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_block_reason
      // 
      this.lbl_block_reason.BackColor = System.Drawing.Color.Transparent;
      this.lbl_block_reason.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_block_reason.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_block_reason.Location = new System.Drawing.Point(309, 69);
      this.lbl_block_reason.Name = "lbl_block_reason";
      this.lbl_block_reason.Size = new System.Drawing.Size(128, 69);
      this.lbl_block_reason.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_block_reason.TabIndex = 4;
      this.lbl_block_reason.Text = "xDesde Cajero";
      // 
      // lbl_birth_title
      // 
      this.lbl_birth_title.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_birth_title.BackColor = System.Drawing.Color.Transparent;
      this.lbl_birth_title.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_birth_title.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_birth_title.Location = new System.Drawing.Point(7, 247);
      this.lbl_birth_title.Name = "lbl_birth_title";
      this.lbl_birth_title.Size = new System.Drawing.Size(106, 23);
      this.lbl_birth_title.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_birth_title.TabIndex = 11;
      this.lbl_birth_title.Text = "Nacimiento:";
      this.lbl_birth_title.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_blocked
      // 
      this.lbl_blocked.BackColor = System.Drawing.Color.Transparent;
      this.lbl_blocked.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_blocked.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_blocked.Location = new System.Drawing.Point(309, 48);
      this.lbl_blocked.Name = "lbl_blocked";
      this.lbl_blocked.Size = new System.Drawing.Size(128, 21);
      this.lbl_blocked.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_blocked.TabIndex = 3;
      this.lbl_blocked.Text = "xBloqueado";
      this.lbl_blocked.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_track_number
      // 
      this.lbl_track_number.BackColor = System.Drawing.Color.Transparent;
      this.lbl_track_number.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_track_number.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_track_number.Location = new System.Drawing.Point(73, 113);
      this.lbl_track_number.Name = "lbl_track_number";
      this.lbl_track_number.Size = new System.Drawing.Size(331, 20);
      this.lbl_track_number.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_track_number.TabIndex = 6;
      this.lbl_track_number.Text = "88888888888888888812";
      this.lbl_track_number.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_id_title
      // 
      this.lbl_id_title.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_id_title.BackColor = System.Drawing.Color.Transparent;
      this.lbl_id_title.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_id_title.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_id_title.Location = new System.Drawing.Point(7, 193);
      this.lbl_id_title.Name = "lbl_id_title";
      this.lbl_id_title.Size = new System.Drawing.Size(318, 23);
      this.lbl_id_title.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_id_title.TabIndex = 9;
      this.lbl_id_title.Text = "xCarnet de padre:";
      this.lbl_id_title.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_trackdata
      // 
      this.lbl_trackdata.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_trackdata.BackColor = System.Drawing.Color.Transparent;
      this.lbl_trackdata.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_trackdata.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_trackdata.Location = new System.Drawing.Point(7, 112);
      this.lbl_trackdata.Name = "lbl_trackdata";
      this.lbl_trackdata.Size = new System.Drawing.Size(60, 23);
      this.lbl_trackdata.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_trackdata.TabIndex = 5;
      this.lbl_trackdata.Text = "Tarjeta:";
      this.lbl_trackdata.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_id
      // 
      this.lbl_id.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_id.BackColor = System.Drawing.Color.Transparent;
      this.lbl_id.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_id.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_id.Location = new System.Drawing.Point(7, 220);
      this.lbl_id.Name = "lbl_id";
      this.lbl_id.Size = new System.Drawing.Size(443, 23);
      this.lbl_id.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_id.TabIndex = 10;
      this.lbl_id.Text = "AAAAAAAAAAAAAAAAAAAB";
      this.lbl_id.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_name
      // 
      this.lbl_name.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_name.BackColor = System.Drawing.Color.Transparent;
      this.lbl_name.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_name.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_name.Location = new System.Drawing.Point(7, 139);
      this.lbl_name.Name = "lbl_name";
      this.lbl_name.Size = new System.Drawing.Size(318, 23);
      this.lbl_name.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_name.TabIndex = 7;
      this.lbl_name.Text = "xName:";
      this.lbl_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_happy_birthday
      // 
      this.lbl_happy_birthday.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_happy_birthday.BackColor = System.Drawing.Color.Transparent;
      this.lbl_happy_birthday.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_happy_birthday.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_happy_birthday.Location = new System.Drawing.Point(8, 274);
      this.lbl_happy_birthday.Name = "lbl_happy_birthday";
      this.lbl_happy_birthday.Size = new System.Drawing.Size(318, 23);
      this.lbl_happy_birthday.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLUE;
      this.lbl_happy_birthday.TabIndex = 13;
      this.lbl_happy_birthday.Text = "xHappy Birthday!";
      this.lbl_happy_birthday.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.lbl_happy_birthday.Visible = false;
      // 
      // pb_blocked
      // 
      this.pb_blocked.Enabled = false;
      this.pb_blocked.Location = new System.Drawing.Point(256, 44);
      this.pb_blocked.Name = "pb_blocked";
      this.pb_blocked.Size = new System.Drawing.Size(48, 46);
      this.pb_blocked.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
      this.pb_blocked.TabIndex = 75;
      this.pb_blocked.TabStop = false;
      // 
      // uc_card_reader1
      // 
      this.uc_card_reader1.ErrorMessageBelow = false;
      this.uc_card_reader1.InvalidCardTextVisible = false;
      this.uc_card_reader1.Location = new System.Drawing.Point(119, 0);
      this.uc_card_reader1.MaximumSize = new System.Drawing.Size(800, 60);
      this.uc_card_reader1.MinimumSize = new System.Drawing.Size(760, 60);
      this.uc_card_reader1.Name = "uc_card_reader1";
      this.uc_card_reader1.Size = new System.Drawing.Size(760, 60);
      this.uc_card_reader1.TabIndex = 0;
      // 
      // uc_ticket_create
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.SystemColors.Control;
      this.Controls.Add(this.gb_cash_advance);
      this.Controls.Add(this.gb_tickets);
      this.Controls.Add(this.gb_divisas);
      this.Controls.Add(this.pb_uc_icon);
      this.Controls.Add(this.btn_player_browse);
      this.Controls.Add(this.btn_terminal_transfer);
      this.Controls.Add(this.gb_chips);
      this.Controls.Add(this.gb_account);
      this.Controls.Add(this.uc_card_reader1);
      this.Controls.Add(this.lbl_separator_1);
      this.Name = "uc_ticket_create";
      this.Size = new System.Drawing.Size(884, 696);
      ((System.ComponentModel.ISupportInitialize)(this.pb_uc_icon)).EndInit();
      this.gb_cash_advance.ResumeLayout(false);
      this.gb_tickets.ResumeLayout(false);
      this.gb_divisas.ResumeLayout(false);
      this.gb_chips.ResumeLayout(false);
      this.gb_account.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.pb_user)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.pb_blocked)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private WSI.Cashier.Controls.uc_label lbl_track_number;
    private System.Windows.Forms.PictureBox pb_user;
    private WSI.Cashier.Controls.uc_label lbl_holder_name;
    private System.Windows.Forms.PictureBox pb_blocked;
    private WSI.Cashier.Controls.uc_label lbl_account_id_value;
    private WSI.Cashier.Controls.uc_label lbl_account_id_name;
    private uc_card_reader uc_card_reader1;
    private System.Windows.Forms.Label lbl_separator_1;
    private WSI.Cashier.Controls.uc_label lbl_id_title;
    private WSI.Cashier.Controls.uc_label lbl_id;
    private WSI.Cashier.Controls.uc_label lbl_birth_title;
    private WSI.Cashier.Controls.uc_label lbl_birth;
    private WSI.Cashier.Controls.uc_label lbl_happy_birthday;
    private WSI.Cashier.Controls.uc_label lbl_name;
    private WSI.Cashier.Controls.uc_label lbl_trackdata;
    private WSI.Cashier.Controls.uc_label lbl_blocked;
    private WSI.Cashier.Controls.uc_label lbl_block_reason;
    private WSI.Cashier.Controls.uc_round_button btn_player_browse;
    private WSI.Cashier.Controls.uc_round_button btn_create_ticket;
    private WSI.Cashier.Controls.uc_round_panel gb_account;
    private System.Windows.Forms.Timer timer1;
    private WSI.Cashier.Controls.uc_round_button btn_chips_sale_with_recharge;
    private WSI.Cashier.Controls.uc_round_button btn_chips_purchase;
    private WSI.Cashier.Controls.uc_round_button btn_undo_chips_purchase;
    private WSI.Cashier.Controls.uc_round_button btn_undo_chips_sale_with_recharge;
    private WSI.Cashier.Controls.uc_round_panel gb_chips;
    private WSI.Cashier.Controls.uc_round_button btn_promotions;
    private WSI.Cashier.Controls.uc_label lbl_vip_account;
    private WSI.Cashier.Controls.uc_round_button btn_undo_cash_advance;
    private WSI.Cashier.Controls.uc_round_button btn_cash_advance;
    private WSI.Cashier.Controls.uc_round_button btn_terminal_transfer;
    private System.Windows.Forms.PictureBox pb_uc_icon;
    private Controls.uc_round_button btn_change_currency;
    private Controls.uc_round_button btn_devolution_currency;
    private Controls.uc_round_panel gb_divisas;
    private Controls.uc_round_button btn_undo_last_exchange_currency;
    private Controls.uc_round_button btn_undo_last_devolution_currency;
    private Controls.uc_round_panel gb_tickets;
    private Controls.uc_round_panel gb_cash_advance;

  }
}
