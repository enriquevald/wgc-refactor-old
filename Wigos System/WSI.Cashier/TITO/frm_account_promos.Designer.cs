namespace WSI.Cashier
{
  partial class frm_account_promos
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.uc_ticket_promo1 = new WSI.Cashier.uc_ticket_promo();
      this.pnl_data.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.Controls.Add(this.uc_ticket_promo1);
      this.pnl_data.Size = new System.Drawing.Size(1024, 658);
      // 
      // uc_ticket_promo1
      // 
      this.uc_ticket_promo1.BackColor = System.Drawing.SystemColors.Control;
      this.uc_ticket_promo1.Location = new System.Drawing.Point(3, 29);
      this.uc_ticket_promo1.Name = "uc_ticket_promo1";
      this.uc_ticket_promo1.Size = new System.Drawing.Size(1021, 520);
      this.uc_ticket_promo1.TabIndex = 0;
      this.uc_ticket_promo1.VirtualAccountMode = false;
      // 
      // frm_account_promos
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.ClientSize = new System.Drawing.Size(1024, 713);
      this.HeaderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.Name = "frm_account_promos";
      this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
      this.Style = WSI.Cashier.Controls.frm_base.BaseFormStyle.PRINCIPAL;
      this.Text = "Vouchers Reprint";
      this.Load += new System.EventHandler(this.frm_account_promos_Load);
      this.pnl_data.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private uc_ticket_promo uc_ticket_promo1;

  }
}