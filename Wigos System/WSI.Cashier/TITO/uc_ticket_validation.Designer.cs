namespace WSI.Cashier
{
  partial class uc_ticket_validation
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.timer1 = new System.Windows.Forms.Timer(this.components);
      this.lbl_separator = new System.Windows.Forms.Label();
      this.lbl_title = new WSI.Cashier.Controls.uc_label();
      this.gb_ticket_lastaction = new WSI.Cashier.Controls.uc_round_panel();
      this.lbl_l_date = new WSI.Cashier.Controls.uc_label();
      this.pnl_last = new WSI.Cashier.Controls.uc_round_panel();
      this.lbl_l_date_value = new WSI.Cashier.Controls.uc_label();
      this.lbl_l_terminal_value = new WSI.Cashier.Controls.uc_label();
      this.lbl_l_terminal = new WSI.Cashier.Controls.uc_label();
      this.gb_ticket_creation = new WSI.Cashier.Controls.uc_round_panel();
      this.lbl_c_account = new WSI.Cashier.Controls.uc_label();
      this.lbl_c_terminal = new WSI.Cashier.Controls.uc_label();
      this.lbl_c_date = new WSI.Cashier.Controls.uc_label();
      this.pnl_creation = new WSI.Cashier.Controls.uc_round_panel();
      this.lbl_c_account_value = new WSI.Cashier.Controls.uc_label();
      this.lbl_c_date_value = new WSI.Cashier.Controls.uc_label();
      this.lbl_c_terminal_value = new WSI.Cashier.Controls.uc_label();
      this.gb_ticket_info = new WSI.Cashier.Controls.uc_round_panel();
      this.lbl_ticket_type = new WSI.Cashier.Controls.uc_label();
      this.lbl_ticket_number = new WSI.Cashier.Controls.uc_label();
      this.pnl_ticket = new WSI.Cashier.Controls.uc_round_panel();
      this.lbl_ticket_type_value = new WSI.Cashier.Controls.uc_label();
      this.lbl_ticket_number_value = new WSI.Cashier.Controls.uc_label();
      this.lbl_ticket_status_value = new WSI.Cashier.Controls.uc_label();
      this.lbl_ticket_amount_value = new WSI.Cashier.Controls.uc_label();
      this.lbl_ticket_amount = new WSI.Cashier.Controls.uc_label();
      this.lbl_ticket_status = new WSI.Cashier.Controls.uc_label();
      this.btn_validate_ticket = new WSI.Cashier.Controls.uc_round_button();
      this.uc_ticket_reader = new WSI.Cashier.TITO.uc_ticket_reader();
      this.gb_ticket_lastaction.SuspendLayout();
      this.pnl_last.SuspendLayout();
      this.gb_ticket_creation.SuspendLayout();
      this.pnl_creation.SuspendLayout();
      this.gb_ticket_info.SuspendLayout();
      this.pnl_ticket.SuspendLayout();
      this.SuspendLayout();
      // 
      // timer1
      // 
      this.timer1.Interval = 20000;
      this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
      // 
      // lbl_separator
      // 
      this.lbl_separator.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_separator.BackColor = System.Drawing.Color.Black;
      this.lbl_separator.Location = new System.Drawing.Point(-3, 60);
      this.lbl_separator.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
      this.lbl_separator.Name = "lbl_separator";
      this.lbl_separator.Size = new System.Drawing.Size(894, 1);
      this.lbl_separator.TabIndex = 2;
      // 
      // lbl_title
      // 
      this.lbl_title.BackColor = System.Drawing.Color.Transparent;
      this.lbl_title.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_title.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_title.Image = global::WSI.Cashier.Properties.Resources.ticketsTito_header;
      this.lbl_title.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.lbl_title.Location = new System.Drawing.Point(11, 20);
      this.lbl_title.Name = "lbl_title";
      this.lbl_title.Size = new System.Drawing.Size(123, 33);
      this.lbl_title.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_title.TabIndex = 0;
      this.lbl_title.Text = "xTickets Validation";
      // 
      // gb_ticket_lastaction
      // 
      this.gb_ticket_lastaction.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.gb_ticket_lastaction.BackColor = System.Drawing.Color.Transparent;
      this.gb_ticket_lastaction.BorderColor = System.Drawing.Color.Empty;
      this.gb_ticket_lastaction.Controls.Add(this.lbl_l_date);
      this.gb_ticket_lastaction.Controls.Add(this.pnl_last);
      this.gb_ticket_lastaction.Controls.Add(this.lbl_l_terminal);
      this.gb_ticket_lastaction.CornerRadius = 10;
      this.gb_ticket_lastaction.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_ticket_lastaction.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gb_ticket_lastaction.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_ticket_lastaction.HeaderHeight = 35;
      this.gb_ticket_lastaction.HeaderSubText = null;
      this.gb_ticket_lastaction.HeaderText = "XPLAYED";
      this.gb_ticket_lastaction.Location = new System.Drawing.Point(169, 444);
      this.gb_ticket_lastaction.Name = "gb_ticket_lastaction";
      this.gb_ticket_lastaction.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_ticket_lastaction.Size = new System.Drawing.Size(454, 110);
      this.gb_ticket_lastaction.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.gb_ticket_lastaction.TabIndex = 5;
      this.gb_ticket_lastaction.Text = "xPlayed";
      // 
      // lbl_l_date
      // 
      this.lbl_l_date.BackColor = System.Drawing.Color.Transparent;
      this.lbl_l_date.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_l_date.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_l_date.Location = new System.Drawing.Point(4, 75);
      this.lbl_l_date.Name = "lbl_l_date";
      this.lbl_l_date.Size = new System.Drawing.Size(170, 25);
      this.lbl_l_date.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_l_date.TabIndex = 1;
      this.lbl_l_date.Text = "xDate:";
      this.lbl_l_date.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // pnl_last
      // 
      this.pnl_last.BackColor = System.Drawing.Color.Transparent;
      this.pnl_last.BorderColor = System.Drawing.Color.Empty;
      this.pnl_last.Controls.Add(this.lbl_l_date_value);
      this.pnl_last.Controls.Add(this.lbl_l_terminal_value);
      this.pnl_last.CornerRadius = 10;
      this.pnl_last.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.pnl_last.HeaderBackColor = System.Drawing.Color.Empty;
      this.pnl_last.HeaderForeColor = System.Drawing.Color.Empty;
      this.pnl_last.HeaderHeight = 0;
      this.pnl_last.HeaderSubText = null;
      this.pnl_last.HeaderText = null;
      this.pnl_last.Location = new System.Drawing.Point(179, 36);
      this.pnl_last.Name = "pnl_last";
      this.pnl_last.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.pnl_last.Size = new System.Drawing.Size(272, 69);
      this.pnl_last.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.WITHOUT_HEAD;
      this.pnl_last.TabIndex = 120;
      // 
      // lbl_l_date_value
      // 
      this.lbl_l_date_value.AutoSize = true;
      this.lbl_l_date_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_l_date_value.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_l_date_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_l_date_value.Location = new System.Drawing.Point(3, 43);
      this.lbl_l_date_value.Name = "lbl_l_date_value";
      this.lbl_l_date_value.Size = new System.Drawing.Size(47, 17);
      this.lbl_l_date_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_l_date_value.TabIndex = 1;
      this.lbl_l_date_value.Text = "xValue";
      this.lbl_l_date_value.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_l_terminal_value
      // 
      this.lbl_l_terminal_value.AutoSize = true;
      this.lbl_l_terminal_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_l_terminal_value.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_l_terminal_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_l_terminal_value.Location = new System.Drawing.Point(3, 15);
      this.lbl_l_terminal_value.Name = "lbl_l_terminal_value";
      this.lbl_l_terminal_value.Size = new System.Drawing.Size(47, 17);
      this.lbl_l_terminal_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_l_terminal_value.TabIndex = 0;
      this.lbl_l_terminal_value.Text = "xValue";
      this.lbl_l_terminal_value.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_l_terminal
      // 
      this.lbl_l_terminal.BackColor = System.Drawing.Color.Transparent;
      this.lbl_l_terminal.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_l_terminal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_l_terminal.Location = new System.Drawing.Point(4, 47);
      this.lbl_l_terminal.Name = "lbl_l_terminal";
      this.lbl_l_terminal.Size = new System.Drawing.Size(170, 25);
      this.lbl_l_terminal.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_l_terminal.TabIndex = 0;
      this.lbl_l_terminal.Text = "xTerminal:";
      this.lbl_l_terminal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // gb_ticket_creation
      // 
      this.gb_ticket_creation.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.gb_ticket_creation.BackColor = System.Drawing.Color.Transparent;
      this.gb_ticket_creation.BorderColor = System.Drawing.Color.Empty;
      this.gb_ticket_creation.Controls.Add(this.lbl_c_account);
      this.gb_ticket_creation.Controls.Add(this.lbl_c_terminal);
      this.gb_ticket_creation.Controls.Add(this.lbl_c_date);
      this.gb_ticket_creation.Controls.Add(this.pnl_creation);
      this.gb_ticket_creation.CornerRadius = 10;
      this.gb_ticket_creation.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_ticket_creation.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gb_ticket_creation.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_ticket_creation.HeaderHeight = 35;
      this.gb_ticket_creation.HeaderSubText = null;
      this.gb_ticket_creation.HeaderText = "XCREATED";
      this.gb_ticket_creation.Location = new System.Drawing.Point(169, 291);
      this.gb_ticket_creation.Name = "gb_ticket_creation";
      this.gb_ticket_creation.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_ticket_creation.Size = new System.Drawing.Size(454, 135);
      this.gb_ticket_creation.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.gb_ticket_creation.TabIndex = 4;
      this.gb_ticket_creation.Text = "xCreated";
      // 
      // lbl_c_account
      // 
      this.lbl_c_account.BackColor = System.Drawing.Color.Transparent;
      this.lbl_c_account.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_c_account.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_c_account.Location = new System.Drawing.Point(4, 103);
      this.lbl_c_account.Name = "lbl_c_account";
      this.lbl_c_account.Size = new System.Drawing.Size(170, 25);
      this.lbl_c_account.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_c_account.TabIndex = 2;
      this.lbl_c_account.Text = "xAccount:";
      this.lbl_c_account.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_c_terminal
      // 
      this.lbl_c_terminal.BackColor = System.Drawing.Color.Transparent;
      this.lbl_c_terminal.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_c_terminal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_c_terminal.Location = new System.Drawing.Point(4, 47);
      this.lbl_c_terminal.Name = "lbl_c_terminal";
      this.lbl_c_terminal.Size = new System.Drawing.Size(170, 25);
      this.lbl_c_terminal.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_c_terminal.TabIndex = 0;
      this.lbl_c_terminal.Text = "xTerminal:";
      this.lbl_c_terminal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_c_date
      // 
      this.lbl_c_date.BackColor = System.Drawing.Color.Transparent;
      this.lbl_c_date.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_c_date.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_c_date.Location = new System.Drawing.Point(4, 75);
      this.lbl_c_date.Name = "lbl_c_date";
      this.lbl_c_date.Size = new System.Drawing.Size(170, 25);
      this.lbl_c_date.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_c_date.TabIndex = 1;
      this.lbl_c_date.Text = "xDate:";
      this.lbl_c_date.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // pnl_creation
      // 
      this.pnl_creation.BackColor = System.Drawing.Color.Transparent;
      this.pnl_creation.BorderColor = System.Drawing.Color.Empty;
      this.pnl_creation.Controls.Add(this.lbl_c_account_value);
      this.pnl_creation.Controls.Add(this.lbl_c_date_value);
      this.pnl_creation.Controls.Add(this.lbl_c_terminal_value);
      this.pnl_creation.CornerRadius = 10;
      this.pnl_creation.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.pnl_creation.HeaderBackColor = System.Drawing.Color.Empty;
      this.pnl_creation.HeaderForeColor = System.Drawing.Color.Empty;
      this.pnl_creation.HeaderHeight = 0;
      this.pnl_creation.HeaderSubText = null;
      this.pnl_creation.HeaderText = null;
      this.pnl_creation.Location = new System.Drawing.Point(179, 36);
      this.pnl_creation.Name = "pnl_creation";
      this.pnl_creation.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.pnl_creation.Size = new System.Drawing.Size(272, 94);
      this.pnl_creation.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.WITHOUT_HEAD;
      this.pnl_creation.TabIndex = 120;
      // 
      // lbl_c_account_value
      // 
      this.lbl_c_account_value.AutoSize = true;
      this.lbl_c_account_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_c_account_value.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_c_account_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_c_account_value.Location = new System.Drawing.Point(3, 71);
      this.lbl_c_account_value.Name = "lbl_c_account_value";
      this.lbl_c_account_value.Size = new System.Drawing.Size(47, 17);
      this.lbl_c_account_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_c_account_value.TabIndex = 2;
      this.lbl_c_account_value.Text = "xValue";
      this.lbl_c_account_value.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_c_date_value
      // 
      this.lbl_c_date_value.AutoSize = true;
      this.lbl_c_date_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_c_date_value.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_c_date_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_c_date_value.Location = new System.Drawing.Point(2, 44);
      this.lbl_c_date_value.Name = "lbl_c_date_value";
      this.lbl_c_date_value.Size = new System.Drawing.Size(53, 17);
      this.lbl_c_date_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_c_date_value.TabIndex = 1;
      this.lbl_c_date_value.Text = "XVALUE";
      this.lbl_c_date_value.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_c_terminal_value
      // 
      this.lbl_c_terminal_value.AutoSize = true;
      this.lbl_c_terminal_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_c_terminal_value.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_c_terminal_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_c_terminal_value.Location = new System.Drawing.Point(2, 16);
      this.lbl_c_terminal_value.Name = "lbl_c_terminal_value";
      this.lbl_c_terminal_value.Size = new System.Drawing.Size(47, 17);
      this.lbl_c_terminal_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_c_terminal_value.TabIndex = 0;
      this.lbl_c_terminal_value.Text = "xValue";
      this.lbl_c_terminal_value.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // gb_ticket_info
      // 
      this.gb_ticket_info.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.gb_ticket_info.BackColor = System.Drawing.Color.Transparent;
      this.gb_ticket_info.BorderColor = System.Drawing.Color.Empty;
      this.gb_ticket_info.Controls.Add(this.lbl_ticket_type);
      this.gb_ticket_info.Controls.Add(this.lbl_ticket_number);
      this.gb_ticket_info.Controls.Add(this.pnl_ticket);
      this.gb_ticket_info.Controls.Add(this.lbl_ticket_amount);
      this.gb_ticket_info.Controls.Add(this.lbl_ticket_status);
      this.gb_ticket_info.CornerRadius = 10;
      this.gb_ticket_info.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_ticket_info.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gb_ticket_info.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_ticket_info.HeaderHeight = 35;
      this.gb_ticket_info.HeaderSubText = null;
      this.gb_ticket_info.HeaderText = "XTICKET INFO";
      this.gb_ticket_info.Location = new System.Drawing.Point(169, 108);
      this.gb_ticket_info.Name = "gb_ticket_info";
      this.gb_ticket_info.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_ticket_info.Size = new System.Drawing.Size(454, 165);
      this.gb_ticket_info.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.gb_ticket_info.TabIndex = 3;
      this.gb_ticket_info.Text = "xTicket Info";
      // 
      // lbl_ticket_type
      // 
      this.lbl_ticket_type.BackColor = System.Drawing.Color.Transparent;
      this.lbl_ticket_type.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_ticket_type.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_ticket_type.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
      this.lbl_ticket_type.Location = new System.Drawing.Point(4, 131);
      this.lbl_ticket_type.Name = "lbl_ticket_type";
      this.lbl_ticket_type.Size = new System.Drawing.Size(170, 25);
      this.lbl_ticket_type.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_ticket_type.TabIndex = 3;
      this.lbl_ticket_type.Text = "xType:";
      this.lbl_ticket_type.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_ticket_number
      // 
      this.lbl_ticket_number.BackColor = System.Drawing.Color.Transparent;
      this.lbl_ticket_number.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_ticket_number.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_ticket_number.Location = new System.Drawing.Point(4, 47);
      this.lbl_ticket_number.Name = "lbl_ticket_number";
      this.lbl_ticket_number.Size = new System.Drawing.Size(170, 25);
      this.lbl_ticket_number.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_ticket_number.TabIndex = 0;
      this.lbl_ticket_number.Text = "xNumber:";
      this.lbl_ticket_number.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // pnl_ticket
      // 
      this.pnl_ticket.BackColor = System.Drawing.Color.Transparent;
      this.pnl_ticket.BorderColor = System.Drawing.Color.Empty;
      this.pnl_ticket.Controls.Add(this.lbl_ticket_type_value);
      this.pnl_ticket.Controls.Add(this.lbl_ticket_number_value);
      this.pnl_ticket.Controls.Add(this.lbl_ticket_status_value);
      this.pnl_ticket.Controls.Add(this.lbl_ticket_amount_value);
      this.pnl_ticket.CornerRadius = 10;
      this.pnl_ticket.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.pnl_ticket.HeaderBackColor = System.Drawing.Color.Empty;
      this.pnl_ticket.HeaderForeColor = System.Drawing.Color.Empty;
      this.pnl_ticket.HeaderHeight = 0;
      this.pnl_ticket.HeaderSubText = null;
      this.pnl_ticket.HeaderText = null;
      this.pnl_ticket.Location = new System.Drawing.Point(179, 39);
      this.pnl_ticket.Name = "pnl_ticket";
      this.pnl_ticket.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.pnl_ticket.Size = new System.Drawing.Size(272, 120);
      this.pnl_ticket.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.WITHOUT_HEAD;
      this.pnl_ticket.TabIndex = 121;
      // 
      // lbl_ticket_type_value
      // 
      this.lbl_ticket_type_value.AutoSize = true;
      this.lbl_ticket_type_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_ticket_type_value.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_ticket_type_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_ticket_type_value.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
      this.lbl_ticket_type_value.Location = new System.Drawing.Point(3, 96);
      this.lbl_ticket_type_value.Name = "lbl_ticket_type_value";
      this.lbl_ticket_type_value.Size = new System.Drawing.Size(47, 17);
      this.lbl_ticket_type_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_ticket_type_value.TabIndex = 3;
      this.lbl_ticket_type_value.Text = "xValue";
      this.lbl_ticket_type_value.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_ticket_number_value
      // 
      this.lbl_ticket_number_value.AutoSize = true;
      this.lbl_ticket_number_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_ticket_number_value.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_ticket_number_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_ticket_number_value.Location = new System.Drawing.Point(3, 12);
      this.lbl_ticket_number_value.Name = "lbl_ticket_number_value";
      this.lbl_ticket_number_value.Size = new System.Drawing.Size(47, 17);
      this.lbl_ticket_number_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_ticket_number_value.TabIndex = 0;
      this.lbl_ticket_number_value.Text = "xValue";
      this.lbl_ticket_number_value.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lbl_ticket_status_value
      // 
      this.lbl_ticket_status_value.AutoSize = true;
      this.lbl_ticket_status_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_ticket_status_value.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_ticket_status_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_ticket_status_value.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
      this.lbl_ticket_status_value.Location = new System.Drawing.Point(3, 40);
      this.lbl_ticket_status_value.Name = "lbl_ticket_status_value";
      this.lbl_ticket_status_value.Size = new System.Drawing.Size(47, 17);
      this.lbl_ticket_status_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_ticket_status_value.TabIndex = 1;
      this.lbl_ticket_status_value.Text = "xValue";
      this.lbl_ticket_status_value.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_ticket_amount_value
      // 
      this.lbl_ticket_amount_value.AutoSize = true;
      this.lbl_ticket_amount_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_ticket_amount_value.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_ticket_amount_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_ticket_amount_value.Location = new System.Drawing.Point(3, 68);
      this.lbl_ticket_amount_value.Name = "lbl_ticket_amount_value";
      this.lbl_ticket_amount_value.Size = new System.Drawing.Size(47, 17);
      this.lbl_ticket_amount_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_ticket_amount_value.TabIndex = 2;
      this.lbl_ticket_amount_value.Text = "xValue";
      this.lbl_ticket_amount_value.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lbl_ticket_amount
      // 
      this.lbl_ticket_amount.BackColor = System.Drawing.Color.Transparent;
      this.lbl_ticket_amount.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_ticket_amount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_ticket_amount.Location = new System.Drawing.Point(4, 103);
      this.lbl_ticket_amount.Name = "lbl_ticket_amount";
      this.lbl_ticket_amount.Size = new System.Drawing.Size(170, 25);
      this.lbl_ticket_amount.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_ticket_amount.TabIndex = 2;
      this.lbl_ticket_amount.Text = "xAmount:";
      this.lbl_ticket_amount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_ticket_status
      // 
      this.lbl_ticket_status.BackColor = System.Drawing.Color.Transparent;
      this.lbl_ticket_status.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_ticket_status.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_ticket_status.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
      this.lbl_ticket_status.Location = new System.Drawing.Point(4, 75);
      this.lbl_ticket_status.Name = "lbl_ticket_status";
      this.lbl_ticket_status.Size = new System.Drawing.Size(170, 25);
      this.lbl_ticket_status.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_ticket_status.TabIndex = 1;
      this.lbl_ticket_status.Text = "xStatus:";
      this.lbl_ticket_status.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // btn_validate_ticket
      // 
      this.btn_validate_ticket.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_validate_ticket.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_validate_ticket.FlatAppearance.BorderSize = 0;
      this.btn_validate_ticket.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_validate_ticket.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_validate_ticket.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_validate_ticket.Image = null;
      this.btn_validate_ticket.IsSelected = false;
      this.btn_validate_ticket.Location = new System.Drawing.Point(702, 366);
      this.btn_validate_ticket.Name = "btn_validate_ticket";
      this.btn_validate_ticket.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_validate_ticket.Size = new System.Drawing.Size(155, 60);
      this.btn_validate_ticket.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_validate_ticket.TabIndex = 6;
      this.btn_validate_ticket.Text = "XVALIDATETICKET";
      this.btn_validate_ticket.UseVisualStyleBackColor = false;
      this.btn_validate_ticket.Click += new System.EventHandler(this.btn_validate_ticket_Click);
      // 
      // uc_ticket_reader
      // 
      this.uc_ticket_reader.Location = new System.Drawing.Point(140, 0);
      this.uc_ticket_reader.MaximumSize = new System.Drawing.Size(800, 100);
      this.uc_ticket_reader.MinimumSize = new System.Drawing.Size(800, 60);
      this.uc_ticket_reader.Name = "uc_ticket_reader";
      this.uc_ticket_reader.Size = new System.Drawing.Size(800, 60);
      this.uc_ticket_reader.TabIndex = 1;
      // 
      // uc_ticket_validation
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.Color.Transparent;
      this.Controls.Add(this.lbl_title);
      this.Controls.Add(this.lbl_separator);
      this.Controls.Add(this.gb_ticket_lastaction);
      this.Controls.Add(this.gb_ticket_creation);
      this.Controls.Add(this.gb_ticket_info);
      this.Controls.Add(this.btn_validate_ticket);
      this.Controls.Add(this.uc_ticket_reader);
      this.Name = "uc_ticket_validation";
      this.Size = new System.Drawing.Size(897, 714);
      this.gb_ticket_lastaction.ResumeLayout(false);
      this.pnl_last.ResumeLayout(false);
      this.pnl_last.PerformLayout();
      this.gb_ticket_creation.ResumeLayout(false);
      this.pnl_creation.ResumeLayout(false);
      this.pnl_creation.PerformLayout();
      this.gb_ticket_info.ResumeLayout(false);
      this.pnl_ticket.ResumeLayout(false);
      this.pnl_ticket.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private WSI.Cashier.TITO.uc_ticket_reader uc_ticket_reader;
    private WSI.Cashier.Controls.uc_round_button btn_validate_ticket;
    private WSI.Cashier.Controls.uc_label lbl_ticket_number;
    private WSI.Cashier.Controls.uc_label lbl_ticket_amount;
    private WSI.Cashier.Controls.uc_label lbl_ticket_status;
    private WSI.Cashier.Controls.uc_label lbl_c_terminal;
    private WSI.Cashier.Controls.uc_label lbl_c_date;
    private WSI.Cashier.Controls.uc_label lbl_c_account;
    private WSI.Cashier.Controls.uc_label lbl_ticket_number_value;
    private WSI.Cashier.Controls.uc_label lbl_ticket_status_value;
    private WSI.Cashier.Controls.uc_label lbl_ticket_amount_value;
    private WSI.Cashier.Controls.uc_label lbl_c_terminal_value;
    private WSI.Cashier.Controls.uc_label lbl_c_date_value;
    private WSI.Cashier.Controls.uc_label lbl_c_account_value;
    private WSI.Cashier.Controls.uc_label lbl_l_date_value;
    private WSI.Cashier.Controls.uc_label lbl_l_terminal_value;
    private WSI.Cashier.Controls.uc_label lbl_l_date;
    private WSI.Cashier.Controls.uc_label lbl_l_terminal;
    private WSI.Cashier.Controls.uc_round_panel pnl_creation;
    private WSI.Cashier.Controls.uc_round_panel pnl_ticket;
    private WSI.Cashier.Controls.uc_round_panel gb_ticket_info;
    private WSI.Cashier.Controls.uc_round_panel gb_ticket_creation;
    private WSI.Cashier.Controls.uc_round_panel gb_ticket_lastaction;
    private WSI.Cashier.Controls.uc_round_panel pnl_last;
    private WSI.Cashier.Controls.uc_label lbl_ticket_type;
    private WSI.Cashier.Controls.uc_label lbl_ticket_type_value;
    private System.Windows.Forms.Timer timer1;
    private System.Windows.Forms.Label lbl_separator;
    private WSI.Cashier.Controls.uc_label lbl_title;
  }
}
