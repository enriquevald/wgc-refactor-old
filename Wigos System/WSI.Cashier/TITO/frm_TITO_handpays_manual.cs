//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : frm_TITO_handpay_manual.cs
// 
//   DESCRIPTION : Implements the following classes:
//                 1. frm_TITO_handpay_manual: For TITO manual handpays input.
//
// REVISION HISTORY:
// 
// Date        Author     Description
// ----------- ------     ----------------------------------------------------------
// 15-NOV-2013 ICS        First release.
// 09-DEC-2013 ICS & RMS  Use PayHandpay method from TITO_HandPays Class
// 10-JAN-2014 LEM        Fixed Bug WIGOSTITO-967: Show retired terminals
// 11-NOV-2015 FOS        Product Backlog Item: 3709 Payment tickets & handpays
// 23-NOV-2015 FOS        Bug 6771:Payment tickets & handpays
// 18-SEP-2017 DPC        WIGOS-5268: Cashier & GUI - Icons - Implement
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WSI.Common;
using System.Data.SqlClient;
using System.Globalization;
using WSI.Cashier.TITO;

namespace WSI.Cashier
{
  public partial class frm_TITO_handpays_manual : Controls.frm_base_icon
  {
    #region Constants

    #endregion

    #region Attributes

    private frm_yesno form_yes_no;

    private static String m_decimal_str = System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencyDecimalSeparator;
    private static Boolean m_cancel = true;

    private Form m_parent;
    private static HandPay m_handpay;

    #endregion

    #region Constructor

    /// <summary>
    /// Constructor method.
    /// 1. Initialize controls.
    /// 2. Initialize control�s resources. (NLS, Images, etc)
    /// </summary>
    public frm_TITO_handpays_manual(CardData RelatedCard)
    {
      InitializeComponent();

      InitializeControlResources();

      m_handpay = new HandPay(RelatedCard);

      form_yes_no = new frm_yesno();
      form_yes_no.Opacity = 0.6;

      InitHandpaysManual();
    }

    #endregion

    #region Buttons

    private void btn_num_1_Click(object sender, EventArgs e)
    {
      AddNumber("1");
    }

    private void btn_num_2_Click(object sender, EventArgs e)
    {
      AddNumber("2");
    }

    private void btn_num_3_Click(object sender, EventArgs e)
    {
      AddNumber("3");
    }

    private void btn_num_4_Click(object sender, EventArgs e)
    {
      AddNumber("4");
    }

    private void btn_num_5_Click(object sender, EventArgs e)
    {
      AddNumber("5");
    }

    private void btn_num_6_Click(object sender, EventArgs e)
    {
      AddNumber("6");
    }

    private void btn_num_7_Click(object sender, EventArgs e)
    {
      AddNumber("7");
    }

    private void btn_num_8_Click(object sender, EventArgs e)
    {
      AddNumber("8");
    }

    private void btn_num_9_Click(object sender, EventArgs e)
    {
      AddNumber("9");
    }

    private void btn_num_10_Click(object sender, EventArgs e)
    {
      AddNumber("0");
    }

    private void btn_num_dot_Click(object sender, EventArgs e)
    {
      // Only one decimal separator is allowed
      if (txt_amount.Text.IndexOf(m_decimal_str) == -1)
      {
        txt_amount.Text = txt_amount.Text + m_decimal_str;
      }

      btn_intro.Focus();
    }

    private void btn_cancel_Click(object sender, EventArgs e)
    {
      m_cancel = true;

      Misc.WriteLog("[FORM CLOSE] frm_tito_handpays_manual (cancel)", Log.Type.Message);
      this.Close();
    }

    private void btn_back_Click(object sender, EventArgs e)
    {
      if (txt_amount.Text.Length > 0)
      {
        txt_amount.Text = txt_amount.Text.Substring(0, txt_amount.Text.Length - 1);
      }
      btn_intro.Focus();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Handle the click on the ok button, which confirms all shown data
    //           and closes the screen.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    private void btn_ok_Click(object sender, EventArgs e)
    {
      TITO_HandPay _tito_handpay;
      Boolean _dummy;

      this.btn_ok.Enabled = false;
      _tito_handpay = new TITO_HandPay(this);

      if (!_tito_handpay.PayHandpay(m_handpay, WGDB.Now, true, out _dummy))
      {
        return;
      }

      Misc.WriteLog("[FORM CLOSE] frm_tito_handpays_manual (ok)", Log.Type.Message);
      this.Close();

    } // btn_ok_Click

    //------------------------------------------------------------------------------
    // PURPOSE : Handle the click on the intro button, which confirms the amount on the
    //           numeric pad.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //

    private void btn_intro_Click(object sender, EventArgs e)
    {
      Decimal _input_amount;
      Decimal _max_allowed_manual_entry;
      Int32 _terminal_id;
      CardData _card_data;
      Int64 _used_account_id;

      btn_ok.Enabled = false;

      // Check before proceeding
      //    - Terminal selection
      //    - Valid amount entered


      //    - Terminal selection
      if (uc_terminal_filter.TerminalsCount() < 1)
      {
        Log.Error("btn_intro_Click. ManualHandpay." + Resource.String("STR_FRM_HANDPAYS_MANUAL_007"));

        frm_message.Show(Resource.String("STR_FRM_HANDPAYS_MANUAL_007"),
                          Resource.String("STR_APP_GEN_MSG_ERROR"),
                          MessageBoxButtons.OK,
                          MessageBoxIcon.Warning,
                          this.ParentForm);
        uc_terminal_filter.Focus();

        return;
      }

      _terminal_id = uc_terminal_filter.TerminalSelected();

      if (_terminal_id < 0)
      {
        Log.Error("btn_intro_Click. ManualHandpay." + Resource.String("STR_FRM_HANDPAYS_MANUAL_008"));

        frm_message.Show(Resource.String("STR_FRM_HANDPAYS_MANUAL_008"),
                          Resource.String("STR_APP_GEN_MSG_ERROR"),
                          MessageBoxButtons.OK,
                          MessageBoxIcon.Warning,
                          this.ParentForm);
        uc_terminal_filter.Focus();

        return;
      }

      //    - Valid amount entered
      _input_amount = 0;

      if (txt_amount.Text != "")
      {
        Decimal.TryParse(txt_amount.Text, out _input_amount);
      }

      if (_input_amount <= 0)
      {
        Log.Error("btn_intro_Click. Wrong handpay amount (" + _input_amount + ").");

        frm_message.Show(Resource.String("STR_FRM_HANDPAYS_MANUAL_006"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Warning, this.ParentForm);
        txt_amount.Focus();

        return;
      }

      // Get the Handpay Manual settings from the General Parameters table
      _max_allowed_manual_entry = GeneralParam.GetInt32("Cashier.HandPays", "MaxAllowedManualEntry");

      // If limit is configured, entered amount must be less or equal to limit
      if (_max_allowed_manual_entry > 0 && _input_amount > _max_allowed_manual_entry)
      {
        Currency _max_allowed_manual_entry_curr;

        _max_allowed_manual_entry_curr = _max_allowed_manual_entry;
        frm_message.Show(Resource.String("STR_FRM_AMOUNT_INPUT_MSG_MAX_ALLOWED", _max_allowed_manual_entry_curr),
                         Resource.String("STR_APP_GEN_MSG_WARNING"),
                         MessageBoxButtons.OK, MessageBoxIcon.Warning, this.ParentForm);
        txt_amount.Focus();

        return;
      }

      _card_data = new CardData();

      try
      {
        using (DB_TRX _trx = new DB_TRX())
        {
          _used_account_id = Accounts.GetOrCreateVirtualAccount(uc_terminal_filter.TerminalSelected(), _trx.SqlTransaction);

          if (_used_account_id == 0)
          {
            return;
          }

          // Get card data info
          if (!CardData.DB_CardGetAllData(_used_account_id, _card_data, _trx.SqlTransaction))
          {
            return;
          }

          _trx.Commit();
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return;
      }

      m_handpay = new HandPay(_card_data);
      m_handpay.TerminalId = _terminal_id;
      m_handpay.TerminalName = uc_terminal_filter.TerminalSelectedName();
      m_handpay.TerminalProvider = uc_terminal_filter.ProviderSelected();
      m_handpay.Amount = _input_amount;
      m_handpay.Type = HANDPAY_TYPE.MANUAL;
      m_handpay.DateTime = WGDB.Now;

      // Generate voucher in preview mode
      PreviewVoucher();

      txt_amount.Text = "";
      btn_ok.Enabled = true;
      btn_ok.Focus();
    } // btn_intro_Click

    //------------------------------------------------------------------------------
    // PURPOSE : Generate a preview version of the voucher to be generated.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    private void PreviewVoucher()
    {
      SqlTransaction _sql_trx;
      SqlConnection _sql_conn;
      VoucherCardHandpay _voucher;
      String _voucher_file_name;
      MultiPromos.PromoBalance _ticket_amount;
      MultiPromos.PromoBalance _final_balance;
      TYPE_SPLITS _splits;
      TYPE_CASH_REDEEM _cash_redeem_ab;
      TYPE_CASH_REDEEM _cash_company_a;
      TYPE_CASH_REDEEM _cash_company_b;
      CashAmount _cash;

      _cash = new CashAmount();
      _sql_conn = null;
      _sql_trx = null;

      _voucher_file_name = "";
      web_browser.Navigate(_voucher_file_name);

      try
      {
        // Get Sql Connection 
        _sql_conn = WSI.Common.WGDB.Connection();
        _sql_trx = _sql_conn.BeginTransaction();

        // Check for enough balance to subtract and make maths
        if (!Split.ReadSplitParameters(out _splits))
        {
          return;
        }

        _ticket_amount = new MultiPromos.PromoBalance(new MultiPromos.AccountBalance(m_handpay.Amount, 0, 0), 0);
        m_handpay.Card.MaxDevolution = WSI.Common.TITO.Utils.MaxDevolutionAmount(m_handpay.Amount);
        if (!_cash.CalculateCashAmount(CASH_MODE.TOTAL_REDEEM, _splits,
                                                 _ticket_amount,
                                                 m_handpay.Amount,
                                                 false,
                                                 false, 
                                                 false, 
                                                 m_handpay.Card,
                                                 out _final_balance,
                                                 out _cash_redeem_ab,
                                                 out _cash_company_a,
                                                 out _cash_company_b))
        {

          return;
        }
        // Handpay voucher 
        VoucherCardHandpay.VoucherCardHandpayInputParams _input_params;

        _input_params = new VoucherCardHandpay.VoucherCardHandpayInputParams();

        _input_params.VoucherAccountInfo = m_handpay.Card.VoucherAccountInfo();
        _input_params.TerminalName = m_handpay.TerminalName;
        _input_params.TerminalProvider = m_handpay.TerminalProvider;
        _input_params.Type = m_handpay.Type;
        _input_params.TypeName = m_handpay.TypeName;
        _input_params.CardBalance = m_handpay.Card.CurrentBalance;
        _input_params.Amount = _cash_redeem_ab.TotalPaid;
        _input_params.SessionPlayedAmount = -1;
        _input_params.DenominacionStr = m_handpay.DenominationStr;
        _input_params.DenominacionValue = m_handpay.DenominationValue;
        _input_params.Mode = PrintMode.Preview;
        _input_params.ValidationNumber = null;
        _input_params.InitialAmount = _cash_redeem_ab.TotalRedeemed;

        _input_params.TaxAmount = m_handpay.TaxAmount;
        _input_params.TaxBaseAmount = m_handpay.TaxBaseAmount;
        _input_params.TaxPct = m_handpay.TaxPct;

        _voucher = new VoucherCardHandpay(_input_params, OperationCode.NOT_SET, _sql_trx);

        _voucher_file_name = _voucher.GetFileName();
      }
      catch (Exception _ex)
      {
        _sql_trx.Rollback();
        Log.Exception(_ex);
      }

      finally
      {
        if (_sql_trx != null)
        {
          if (_sql_trx.Connection != null)
          {
            _sql_trx.Rollback();
          }

          _sql_trx.Dispose();
          _sql_trx = null;
        }

        // Close connection
        if (_sql_conn != null)
        {
          _sql_conn.Close();
          _sql_conn = null;
        }
      }

      web_browser.Navigate(_voucher_file_name);
    } // PreviewVoucher

    #endregion Buttons

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Init and Show the form.
    //
    //  PARAMS :
    //      - INPUT :
    //          - ParentForm
    //
    //      - OUTPUT :
    //
    // RETURNS : The result of the opperation
    //
    //   NOTES :
    //
    public Boolean Show(Form ParentForm)
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_TITO_handpays_manual", Log.Type.Message);

      txt_amount.Text = "";

      m_parent = ParentForm;

      btn_ok.Enabled = false;
      
      gp_digits_box.Enabled = true;
      gp_voucher_box.Enabled = false;

      form_yes_no.Show();
      this.ShowDialog(form_yes_no);
      form_yes_no.Hide();

      if (m_cancel)
      {
        return false;
      }

      return true;
    } // Show

    //------------------------------------------------------------------------------
    // PURPOSE : Dispose yes_no form.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    new public void Dispose()
    {
      form_yes_no.Dispose();
      form_yes_no = null;

      base.Dispose();
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Initialize control resources (NLS strings, images, etc)
    /// </summary>
    public void InitializeControlResources()
    {
      // NLS Strings
      //    - Labels
      lbl_amount_input_title.Text = Resource.String("STR_FRM_HANDPAYS_MANUAL_001");
      gp_term_game.Text = Resource.String("STR_FRM_HANDPAYS_MANUAL_002");
      gp_digits_box.Text = Resource.String("STR_FRM_HANDPAYS_MANUAL_003");

      //    - Buttons
      btn_ok.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_OK");
      btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");
      btn_intro.Text = Resource.String("STR_FRM_AMOUNT_INPUT_BTN_ENTER");

    } // InitializeControlResources

    /// <summary>
    /// Initialize screen logic
    /// </summary>
    private void InitHandpaysManual()
    {
      StringBuilder _where = new StringBuilder();
      Int32 _handpay_time_period = 0;
      Int32 _handpay_cancel_time_period = 0;

      // Get the Handpays settings from the General Parameters table
      Handpays.GetParameters(out _handpay_time_period, out _handpay_cancel_time_period);

      //_where.AppendLine("           , ( ");
      //_where.AppendLine("             SELECT   PS_TERMINAL_ID ");
      //_where.AppendLine("               FROM   PLAY_SESSIONS WITH (INDEX (IX_PS_STARTED)) ");
      //_where.AppendLine("              WHERE   PS_STARTED    >= DATEADD(MINUTE, " + _handpay_time_period + ", GETDATE()) ");
      //_where.AppendLine("                AND   (PS_INITIAL_BALANCE <> PS_FINAL_BALANCE OR PS_PLAYED_COUNT > 0)  ");
      //_where.AppendLine("           GROUP BY PS_TERMINAL_ID  ");
      //_where.AppendLine("           ) RECENT_TERMINALS ");
      _where.AppendLine("   WHERE TE_TERMINAL_TYPE = " + (Int32)TerminalTypes.SAS_HOST);
      _where.AppendLine(String.Format("      AND (  TE_RETIREMENT_DATE IS NULL OR DATEDIFF(MINUTE, TE_RETIREMENT_DATE, GETDATE()) <= {0} ) ", _handpay_time_period));

      //_where.AppendLine("     AND TE_TERMINAL_ID   = RECENT_TERMINALS.PS_TERMINAL_ID ");

      // Center screen
      this.Location = new Point(1024 / 2 - this.Width / 2, 0);

      // Load the list of available terminals
      uc_terminal_filter.InitControls("", _where.ToString(), true, true);
    }

    /// <summary>
    /// Check and Add digit to amount
    /// </summary>
    /// <param name="NumberStr"></param>
    private void AddNumber(String NumberStr)
    {
      String aux_str;
      String tentative_number;
      Currency input_amount;
      int dec_symbol_pos;


      // Prevent exceeding maximum number of decimals (2)
      dec_symbol_pos = txt_amount.Text.IndexOf(m_decimal_str);
      if (dec_symbol_pos > -1)
      {
        aux_str = txt_amount.Text.Substring(dec_symbol_pos);
        if (aux_str.Length > 2)
        {
          return;
        }
      }

      // Prevent exceeding maximum amount length
      if (txt_amount.Text.Length >= Cashier.MAX_ALLOWED_AMOUNT_LENGTH)
      {
        return;
      }

      tentative_number = txt_amount.Text + NumberStr;

      try
      {
        input_amount = Decimal.Parse(tentative_number);
        if (input_amount > Cashier.MAX_ALLOWED_AMOUNT)
        {
          return;
        }
      }
      catch
      {
        input_amount = 0;

        return;
      }

      // Remove unneeded leading zeros
      // Unless we are entering a decimal value, there must not be any leading 0
      if (dec_symbol_pos == -1)
      {
        txt_amount.Text = txt_amount.Text.TrimStart('0') + NumberStr;
      }
      else
      {
        // Accept new symbol/digit 
        txt_amount.Text = txt_amount.Text + NumberStr;
      }

      btn_intro.Focus();
    }

    private void frm_handpays_manual_Shown(object sender, EventArgs e)
    {
      this.txt_amount.Focus();
    }

    private void frm_handpays_manual_KeyPress(object sender, KeyPressEventArgs e)
    {
      String aux_str;
      char c;

      c = e.KeyChar;

      if (c >= '0' && c <= '9')
      {
        aux_str = "" + c;
        AddNumber(aux_str);

        e.Handled = true;
      }

      if (c == '\r')
      {
        btn_intro_Click(null, null);

        e.Handled = true;
      }

      if (c == '\b')
      {
        btn_back_Click(null, null);

        e.Handled = true;
      }

      if (c == '.')
      {
        btn_num_dot_Click(null, null);

        e.Handled = true;
      }

      e.Handled = false;
    }

    private void splitContainer1_KeyPress(object sender, KeyPressEventArgs e)
    {
      frm_handpays_manual_KeyPress(sender, e);
    }

    private void frm_handpays_manual_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
    {
      char c;

      c = Convert.ToChar(e.KeyCode);
      if (c == '\r')
      {
        e.IsInputKey = true;
      }
    }
   #endregion
  }

 
}