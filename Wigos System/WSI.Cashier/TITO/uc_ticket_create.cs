//------------------------------------------------------------------------------
// Copyright � 2007-2013 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_ticket_create.cs
// 
//   DESCRIPTION: 
//
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 08-NOV-2013 LEM     First release. 
// 31-DEC-2013 JRM     Removed all references to method UserHasPermission and changed it for ProfilePermissions.CheckPermissions
// 07-JAN-2014 RCI     Added buttons for casino chips: Sale and Purchase
// 07-JAN-2014 LEM & RCI     For all buttons, accept only mouse events, not key events
// 10-JAN-2014 JPJ     Account information in the AccountWatcher thread is cleared.
// 15-JAN-2014 JBP     Refund in TITO Ticket.
// 16-JAN-2014 ACM     Added Undo Chips Sale/Purchase
// 17-JAN-2014 DLL     Added Change Chips
// 24-JAN-2014 DLL     Fixed Bug #WIG-541: Incorrect message when cancel permission
// 03-FEB-2014 DLL     Added promotion button and reorganize chips buttons
// 14-FEB-2014 LJM     Fixed Bug WIG-632: cashier can sell chips when amount > balance
// 26-FEB-2014 XIT     Deleted SmartParams uses
// 06-MAR-2014 ICS     Deleted the checking of site jackpot winner. Not needed in TITO.
// 14-APR-2014 DLL     Added new functionality: VIP Account
// 22-APR-2014 DLL     Added new functionality for print specific voucher for the Dealer
// 05-MAY-2014 RMS     Fixed Bug WIG-842: Personalizaci�n de Cuentas: Al intentar personalizar una cuenta el formulario aparece en blanco. 
// 22-JUL-2014 JCO     Fixed Bub WIG-1109: Added functionality to show comments when chk_show_comments is checked.
// 30-JUL-2014 SGB     Fixed Bug WIGOSTITO-1247: Permission delete create ticket and add to frm_anount_input in btn_ok
// 18-SEP-2014 DLL     Added buttons Cash Advance
// 26-SEP-2014 JBC     Fixed Bug WIG-1315: Cash advance buttons disabled when cashdesk is closed
// 17-OCT-2014 JBC     Fixed Bug WIG-1518: Cash advance undo operation problems.
// 28-OCT-2014 SMN     Added new functionality: BLOCK_REASON field can have more than one block reason.
// 04-MAR-2015 RCI     Fixed Bug WIG-2134: After show comments, buttons must be enabled.
// 11-NOV-2015 FOS     Product Backlog Item: 3709 Payment tickets & handpays
// 24-FEB-2016 FAV     Fixed Bug 9019: Resize buttons because the text in "btn_undo_cash_advance" button is large
// 07-MAR-2016 LTC     Product Backlog Item 10227:TITA: TITO ticket exchange for gambling chips
// 29-MAR-2016 RGR     Product Backlog Item 10981:Sprint Review 21 Winions - Mex
// 08-ABR-2016 LTC     Bug 11628:TITA - Chips Swap - List items are cleaned on Cancel operation
// 13-APR-2016 DHA     Product Backlog Item 9950: added chips operation (sales and purchases)
// 02-MAY-2016 ETP     Product Backlog Item 12652: Added and buttons for test funcionality.
// 03-MAY-2016 FAV     Product Backlog Item 12653:Cajero: Currency exchange for multiple denominations (Mallorca)
// 11-MAY-2016 FAV     Product Backlog Item 12893: Card/Check refund 
// 19-MAY-2016 ETP     Bug 13384: Multicurrency - Error si los permisos estan deshabilitados.
// 23-MAY-2016 FAV     PBI 13491: Multicurrency, added functionality for Cashless
// 13-APR-2016 DHA     Product Backlog Item 9950: added chips operation (sales and purchases)
// 25-MAY-2016 ETP     Fixed buf 13688:Empty groupbox when card and cheke are disabled.
// 31-MAY-2016 ETP    Fixed Bug 13994 - Excepci�n no controlada al no tener configurada ninguna divisa.
// 01-JUN-2016 FAV    Fixed Bug 14036: The "Undo Cash Advance" Form doesn't show the confirmation popup
// 31-MAY-2016 ETP     Fixed Bug 13994 - Excepci�n no controlada al no tener configurada ninguna divisa.
// 01-JUN-2016 ETP     Fixed Bug 14039 - Cambio de divisa: no se lleva a t�rmino la operaci�n al no aplicar las comisiones con el permiso deshabilitado
// 01-JUL-2016 LTC     Bug 10939: Cashier New Design  - NLS Sale/Purchase button in engles
// 05-JUL-2016 ETP     Product Backlog Item 15195:Cajero: permitir Multidivisa sin cuenta asociada.
// 06-JUL-2016 DHA     Product Backlog Item 15064: multicurrency chips sale/purchase
// 12-SEP-2016 JML     Fixed Bug 17571 - Gaming tables: Wrong message "The Cash Desk does not have enough stock."
// 19-SEP-2016 FGB     PBI 15985: Exped System: Cashier: Check if the payment/sale can be done
// 23-SEP-2016 LTC     Product Backlog Item 17461: Gamingtables 46 - Cash desk draw for gamblign tables: Chips purchase
// 27-SEP-2016 FOS     Fixed Bug 17799:  Error hide chips buttons when playertracking param is disabled 
// 16-JAN-2017 ETP     Fixed Bug 22747: Multivisa: needed valid visid.
// 23-JAN-2017 JML     Fixed Bug 21868: Allows to perform Buy-In exceeding the number configured in the external PLD.
// 06-JUN-2017 DHA     PBI 27760:WIGOS-257: MES21 - Gaming table chair assignment with card selection
// 10-JUL-2017 DHA     PBI 28610:WIGOS-3458 - Show Customer Image in a Pop up - Accounts
// 03-OCT-2017 DHA     PBI 30017:WIGOS-4056 Payment threshold registration - Player registration
// 05-OCT-2017 RAB     PBI 30018:WIGOS-4036 Payment threshold authorization - Payment authorisation
// 09-OCT-2017 JML     PBI 30022:WIGOS-4052 Payment threshold registration - Recharge with Check payment
// 10-OCT-2017 JML     PBI 30023:WIGOS-4068 Payment threshold registration - Transaction information
// 03-NOV-2017 RGR     Bug 30558:WIGOS-6158 Exception when the user proceed with a "Devolucion de divisa"
// 04-MAY-2018 AGS     Bug 32552:WIGOS-8179 [Ticket #12450] Reporte de Compra Venta de Fichas Duplica ventas en Caja V03.06.0035
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Drawing.Printing;
using WSI.Common;
using WSI.Cashier.TITO;
using WSI.Common.TITO;
using System.Collections;
using WSI.Cashier.MVC.Controller.PlayerEditDetails;
using WSI.Cashier.MVC.View.PlayerEditDetails;
using WSI.Common.Entrances;

namespace WSI.Cashier
{
  public partial class uc_ticket_create : UserControl
  {
    #region Members

    private static frm_yesno form_yes_no;
    Int64 m_current_card_id;
    frm_amount_input form_credit;
    frm_search_players form_search_players = new frm_search_players();
    String visible_track_number;
    String m_current_track_number;
    Boolean is_logged_in;
    Boolean can_log_off;
    Boolean is_locked;
    Boolean m_swipe_card;
    AccountWatcher account_watcher;
    CardPrinter card_printer;
    Boolean m_first_time = true;
    private frm_container m_parent;
    Boolean m_is_new_card;
    Int32 m_tick_count_tracknumber_read = CashierBusinessLogic.DURATION_BUTTONS_ENABLED_AFTER_OPERATION + 1;
    Boolean m_is_vip_account;
    Boolean m_pending_check_comments;
    Boolean m_must_show_comments;
    private Boolean m_exist_photo = false;

    #endregion

    #region Constructor

    public uc_ticket_create()
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[UC LOAD] uc_ticket_create", Log.Type.Message);

      InitializeComponent();

      form_yes_no = new frm_yesno();
      form_yes_no.Opacity = 0.6;

      m_swipe_card = false;
      m_pending_check_comments = false;

      card_printer = new CardPrinter();

      EventLastAction.AddEventLastAction(this.Controls);

      btn_terminal_transfer.Text = Resource.String("STR_TRANSFER_TO_TERMINAL");
      btn_terminal_transfer.Visible = GeneralParam.GetBoolean("SasHost", "TransferFromCashier", false);

    }
    #endregion

    #region Privates

    //------------------------------------------------------------------------------
    // PURPOSE: Parent form restore focus and bring to front
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    private void RestoreParentFocus()
    {
      this.ParentForm.Focus();
      this.ParentForm.BringToFront();
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Refresh card data controls whith empty info.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    private void ClearCardData()
    {
      m_current_track_number = "";
      visible_track_number = "";
      // 10-JAN-2014 JPJ     Account information in the AccountWatcher thread is cleared.
      if (account_watcher != null)
      {
        account_watcher.ClearAccount();
      }
      RefreshData(new CardData());
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Refresh account watcher
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    private void RefreshAccount()
    {
      account_watcher.ForceRefresh();
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Creates a new Card Data Account
    // 
    //  PARAMS:
    //      - INPUT:
    //          CardData
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    private void NewAccount(CardData CardData)
    {
      Boolean _card_paid;
      String _msgbox_text;
      CardData _aux_card = new CardData();
      String _error_str;

      // MBF 30-NOV-2009 - Check BEFORE asking if the user wants to create
      // Check provided trackdata is not already associated to a MB account
      if (CashierBusinessLogic.DB_IsMBCardDefined(CardData.TrackData))
      {
        // MsgBox: Card already assigned to a Mobile Bank account.
        // frm_message.Show(Resource.String("STR_UC_CARD_USER_MSG_CARD_LINKED_TO_MB_ACCT"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this.ParentForm);

        // uc_card_reader1.Focus();

        m_parent.SwitchToMobileBanck(CardData.TrackData);
        ClearCardData();
        uc_card_reader1.ClearTrackNumber();

        return;
      }

      // Card does not exist: request confirmation to create a new one.
      _aux_card.TrackData = CardData.TrackData;

      // RCI 18-OCT-2011: Can't create new accounts when cashier is not open.
      if (CashierBusinessLogic.ReadCashierStatus(Cashier.SessionId) != CASHIER_STATUS.OPEN)
      {
        _msgbox_text = Resource.String("STR_UC_CARD_USER_MSG_CARD_NOT_EXIST_AND_CASH_CLOSED", "\n\n" + _aux_card.VisibleTrackdata() + "\n\n");
        frm_message.Show(_msgbox_text, Resource.String("STR_UC_CARD_USER_MSG_CARD_NOT_EXIST_TITLE"), MessageBoxButtons.OK, MessageBoxIcon.Warning, this.ParentForm);

        uc_card_reader1.Focus();

        return;
      }

      //m_is_new_card = true;
      // JCM 13-APR-2012 Ask the new account type.
      switch (frm_message.ShowAccountSelect(_aux_card.VisibleTrackdata(), this.ParentForm))
      {
        case ACCOUNT_USER_TYPE.ANONYMOUS:

          // Check if current user is an authorized user
          if (!ProfilePermissions.CheckPermissionList(ProfilePermissions.CashierFormFuncionality.AccountCreateAnonymous, ProfilePermissions.TypeOperation.RequestPasswd, this.ParentForm, 0, out _error_str))
          {
            return;
          }

          using (DB_TRX _db_trx = new DB_TRX())
          {
            // Creates CardData into DB
            if (!CashierBusinessLogic.DB_CreateCard(CardData.TrackData, out CardData.AccountId, _db_trx.SqlTransaction))
            {
              Log.Error("LoadCardByTrackNumber. DB_CreateCard: Card already exists or error creating card.");
              // TODO: MsgBox: Card already exist or error creating card.
              //frm_message.Show(Resource.String("STR_FRM_AMOUNT_INPUT_MSG_AMOUNT_EXCEED_PAYABLE"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this);

              uc_card_reader1.Focus();
            }
            else
            {
              if (CashierBusinessLogic.DB_UpdateCardPayAfterCreated(CardData.AccountId, ACCOUNT_USER_TYPE.ANONYMOUS, out _card_paid, _db_trx.SqlTransaction))
              {
                CardData.CardPaid = _card_paid;
              }

              _db_trx.Commit();
              m_is_new_card = false;
            }
          }

          // Load data after create account.
          LoadCardByAccountId(CardData.AccountId);

          return;

        case ACCOUNT_USER_TYPE.PERSONAL:

          // Check if current user is an authorized user
          if (!ProfilePermissions.CheckPermissionList(ProfilePermissions.CashierFormFuncionality.AccountCreatePersonal, ProfilePermissions.TypeOperation.RequestPasswd, this.ParentForm, 0, out _error_str))
          {
            return;
          }

          this.ClearCardData();
          m_current_track_number = CardData.TrackData;
          btn_edit_Click(null, null);

          break;

        case ACCOUNT_USER_TYPE.NONE:  // cancel
        default:
          this.ClearCardData();
          this.InitControls(m_parent);
          break;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Check for input card in order to notify user to create card.
    ///         Previously called LoadCardByTrackNumber
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    private Boolean LoadCardByAccountId(Int64 AccountId)
    {
      String _message;
      string[] _message_params = { "", "", "", "", "", "", "" };

      // Check for no input card
      if (AccountId < 0)
      {
        return false;
      }

      CardData card_data = new CardData();

      visible_track_number = "";

      // Get Card data
      if (!CardData.DB_CardGetAllData(AccountId, card_data, false))
      {
        ClearCardData();

        Log.Error("LoadCardByAccountId. DB_GetCardAllData: Error reading card.");

        return false;
      }

      if (!card_data.IsRecycled)
      {
        if (!CardData.CheckTrackdataSiteId( new ExternalTrackData (card_data.TrackData)))
        {
          ClearCardData();

          Log.Error("LoadCardByAccountId. Card: " + card_data.TrackData + " doesn�t belong to this site.");

          frm_message.Show(Resource.String("STR_CARD_SITE_ID_ERROR"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK,
                           MessageBoxIcon.Warning, this.ParentForm);

          uc_card_reader1.Focus();

          return false;

        }
      }

      if (card_data.AccountId != 0)
      {
        // Keep card read data.

        m_current_card_id = card_data.AccountId;
        m_current_track_number = card_data.TrackData;
        //m_is_new_card = false;
        account_watcher.SetAccount(card_data);

        m_is_vip_account = card_data.PlayerTracking.HolderIsVIP;

        // RCI 10-NOV-2010: Show pop-up if recent change of level occurrs.
        if (card_data.PlayerTracking.HolderLevelNotify != 0)
        {
          Int32 _old_level_int;
          String _old_level;
          String _new_level;

          _old_level_int = card_data.PlayerTracking.CardLevel - card_data.PlayerTracking.HolderLevelNotify;
          _old_level = LoyaltyProgram.LevelName(_old_level_int);
          _new_level = LoyaltyProgram.LevelName(card_data.PlayerTracking.CardLevel);

          _message_params[0] = _old_level;
          _message_params[1] = _new_level;
          if (card_data.PlayerTracking.HolderLevelNotify > 0)
          {
            _message = Resource.String("STR_UC_CARD_USER_MSG_HOLDER_LEVEL_RISEN", _message_params);
          }
          else
          {
            _message = Resource.String("STR_UC_CARD_USER_MSG_HOLDER_LEVEL_FALLEN", _message_params);
          }
          _message = _message.Replace("\\r\\n", "\r\n");

          frm_message.Show(_message,
                           Resource.String("STR_APP_GEN_MSG_WARNING"),
                           MessageBoxButtons.OK,
                           MessageBoxIcon.Information,
                           ParentForm);

          CashierBusinessLogic.DB_ResetHolderLevelNotify(card_data);
        }

        // JCO 22-JUL-2014: WIG-1109. Show comments to the user when a card is inserted.
        m_must_show_comments = !m_is_new_card
                               && card_data.PlayerTracking.ShowCommentsOnCashier
                               && m_pending_check_comments
                               && CashierBusinessLogic.ReadCashierStatus(Cashier.SessionId) == CASHIER_STATUS.OPEN;


        if (m_must_show_comments)
        {
          m_pending_check_comments = false;
          btn_edit_Click(null, null);
          uc_card_reader1.Focus();
        }

      }

      return true;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get Card by Account ID
    // 
    //  PARAMS:
    //      - INPUT: Card ID
    //               CardData
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //  True if Card_ID is valid.
    //   NOTES:
    private Boolean GetCardData(Int64 CardId, out CardData CardData)
    {
      Int64 _account_id = 0;

      CardData = account_watcher.Card;

      if (CardId == 0)
      {
        if (GeneralParam.GetBoolean("MultiCurrencyExchange", "AccountRequired", false))
        {
          _account_id = form_search_players.Show(this.ParentForm);

          if (_account_id <= 0)
          {
            return false;
          }

          CardData.DB_CardGetAllData(_account_id, CardData, false);
          RefreshData(CardData);

        }
        else
        {
          return CardData.DB_VirtualCardGetAllData(Cashier.TerminalName, AccountType.ACCOUNT_VIRTUAL_CASHIER, CardData);
        }
      }

      return true;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Set status of ALL buttons in control
    // 
    //  PARAMS:
    //      - INPUT:
    //        - Ticket: reference to a ticket instance
    //
    //      - OUTPUT:
    //
    private void SetButtonStatus(Boolean CardEnabled)
    {
      Boolean _is_tito_cashier_open;

      _is_tito_cashier_open = BusinessLogic.IsTITOCashierOpen;

      btn_create_ticket.Enabled = _is_tito_cashier_open && CardEnabled;
      btn_chips_purchase.Enabled = _is_tito_cashier_open && CardEnabled;
      btn_chips_sale_with_recharge.Enabled = _is_tito_cashier_open && CardEnabled;
      btn_undo_chips_purchase.Enabled = _is_tito_cashier_open && CardEnabled;
      btn_undo_chips_sale_with_recharge.Enabled = _is_tito_cashier_open && CardEnabled;
      btn_promotions.Enabled = _is_tito_cashier_open && m_swipe_card && CardEnabled;
      btn_undo_cash_advance.Enabled = _is_tito_cashier_open && CardEnabled;
      btn_cash_advance.Enabled = _is_tito_cashier_open && CardEnabled;
      btn_terminal_transfer.Enabled = _is_tito_cashier_open;
      btn_change_currency.Enabled = _is_tito_cashier_open && CardEnabled && CurrencyExchange.IsValidCurrencyExchange();
      btn_devolution_currency.Enabled = _is_tito_cashier_open && CardEnabled && CurrencyExchange.IsValidCurrencyExchange();
      btn_undo_last_exchange_currency.Enabled = _is_tito_cashier_open && CardEnabled;
      btn_undo_last_devolution_currency.Enabled = _is_tito_cashier_open && CardEnabled;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Refresh account data controls watcher card info
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    private void RefreshData()
    {
      RefreshData(account_watcher.Card);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Refresh account data controls from CardData
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    private void RefreshData(CardData CardData)
    {
      CardData _card_data;
      DateTime _birth_date;
      DateTime _now_date;
      TimeSpan _tspam;
      Int32 _timeout_seconds;
      Dictionary<AccountBlockReason, String> _block_reasons;

      _card_data = CardData;
      if (_card_data == null)
      {
        _card_data = new CardData();
      }

      if (_card_data.IsRecycled)
      {
        visible_track_number = CardData.RECYCLED_TRACK_DATA;
      }
      else
      {
        visible_track_number = _card_data.VisibleTrackdata();
        m_current_track_number = _card_data.TrackData;
      }

      lbl_track_number.Text = visible_track_number;

      m_current_card_id = _card_data.AccountId;

      m_is_vip_account = _card_data.PlayerTracking.HolderIsVIP;

      lbl_happy_birthday.Visible = false;

      lbl_id_title.Text = Resource.String("STR_FRM_PLAYER_EDIT_TYPE_DOCUMENT") + ":";

      // 11-NOV-2015 AVZ TODO Imagen a cambiar por la de anonimo.
      pb_user.Image = Resources.ResourceImages.anonymous_user;
      lbl_id.Visible = false;
      lbl_birth.Visible = false;

      if (_card_data.PlayerTracking.HolderName == null)
      {
        lbl_holder_name.Text = "";
      }
      else if (_card_data.PlayerTracking.HolderName == "")
      {
        lbl_holder_name.Text = Resource.String("STR_UC_CARD_CARD_HOLDER_ANONYMOUS");
      }
      else
      {
        lbl_id.Visible = true;
        lbl_id_title.Visible = true;
        lbl_birth.Visible = true;
        lbl_birth_title.Visible = true;
        lbl_name.Visible = true;

        lbl_holder_name.Text = _card_data.PlayerTracking.HolderName;

        lbl_id_title.Text = CardData.GetLabelDocument(account_watcher.Card.PlayerTracking.PreferredHolderIdType);
        lbl_id.Text = _card_data.PlayerTracking.PreferredHolderId;

        if (_card_data.PlayerTracking.HolderBirthDate != DateTime.MinValue)
        {
          lbl_birth.Text = _card_data.PlayerTracking.HolderBirthDate.ToShortDateString();

          if (_card_data.PlayerTracking.HolderBirthDate.Month == 2
            && _card_data.PlayerTracking.HolderBirthDate.Day == 29
            && !DateTime.IsLeapYear(WGDB.Now.Year))
          {
            _birth_date = new DateTime(WGDB.Now.Year, _card_data.PlayerTracking.HolderBirthDate.Month, _card_data.PlayerTracking.HolderBirthDate.AddDays(-1).Day);
          }
          else
          {
            _birth_date = new DateTime(WGDB.Now.Year, _card_data.PlayerTracking.HolderBirthDate.Month, _card_data.PlayerTracking.HolderBirthDate.Day);
          }

          _now_date = new DateTime(WGDB.Now.Year, WGDB.Now.Month, WGDB.Now.Day);
          _tspam = _birth_date - _now_date;

          if (_tspam.TotalDays > 0 && _tspam.TotalDays <= GeneralParam.GetInt32("Accounts", "Player.BirthdayWarningDays", 7))
          {
            lbl_happy_birthday.Text = Resource.String("STR_UC_PLAYER_TRACK_BIRTHDAY_IN_NEXT_WEEK");
            lbl_happy_birthday.Visible = true;
          }
          else if (_tspam.TotalDays == 0)
          {
            lbl_happy_birthday.Text = Resource.String("STR_UC_PLAYER_TRACK_HAPPY_BIRTHDAY");
            lbl_happy_birthday.Visible = true;
          }

          //JBC 30-01-2015 BirthDay alarm feature
          if (!Alarm.CheckBirthdayAlarm(_card_data.AccountId, _card_data.PlayerTracking.HolderName,
            CommonCashierInformation.CashierSessionInfo().TerminalName + " - " + CommonCashierInformation.CashierSessionInfo().TerminalId, TerminalTypes.UNKNOWN))
          {

            Log.Error("uc_ticket_create.RefreshData: Error checking Birthday Alarm.");
          }

        }
        else
        {
          lbl_birth.Visible = false;
          lbl_birth_title.Visible = false;
        }

        if (String.IsNullOrEmpty(lbl_id.Text))
        {
          lbl_id.Visible = false;
          lbl_id_title.Visible = false;
        }

        // AJQ 28-MAY-2015, Account Photo        
        AccountPhotoFunctions.SetAccountPhoto(pb_user, _card_data.AccountId, (GENDER)_card_data.PlayerTracking.HolderGender, out  m_exist_photo);
      }

      lbl_account_id_value.Text = m_current_card_id.ToString();
      lbl_account_id_value.Visible = (m_current_card_id != 0);

      lbl_vip_account.Text = Resource.String("STR_VIP_ACCOUNT");
      lbl_vip_account.Visible = m_is_vip_account;

      is_logged_in = _card_data.IsLoggedIn;
      is_locked = _card_data.IsLocked;
      can_log_off = is_logged_in;

      if (can_log_off)
      {
        try
        {
          if (!_card_data.CurrentPlaySession.BalanceMismatch)
          {
            _timeout_seconds = Int32.Parse(WSI.Common.Misc.ReadGeneralParams("Cashier", "MinTimeToCloseSession"));

            if (_card_data.CurrentPlaySession.SecondsSinceLastActivity < _timeout_seconds)
            {
              can_log_off = false;
            }
          }
        }
        catch (Exception ex)
        {
          Log.Exception(ex);
        }
      }

      // RCI 22-OCT-2010: Added block reason label.
      if (_card_data.Blocked)
      {
        lbl_blocked.Text = Resource.String("STR_UC_CARD_BLOCK_STATUS_LOCKED");

        _block_reasons = Accounts.GetBlockReason((Int32)_card_data.BlockReason);
        if (_block_reasons.Count > 1)
        {
          // Show "by several reasons"
          lbl_block_reason.Text = Resource.String("STR_UC_CARD_BLOCK_SEVERAL_REASONS");
        }
        else
        {
          // Show the only one block_reason
          foreach (KeyValuePair<AccountBlockReason, String> _key_value_pair in _block_reasons)
          {
            lbl_block_reason.Text = _key_value_pair.Value;
          }
        }

        pb_blocked.Image = Resources.ResourceImages.locked;
      }
      else
      {
        lbl_blocked.Text = Resource.String("STR_UC_CARD_BLOCK_STATUS_UNLOCKED");
        lbl_block_reason.Text = String.Empty;
        pb_blocked.Image = Resources.ResourceImages.unlocked;
      }

      // Hide the blocked label if not blocked
      lbl_blocked.Visible = _card_data.Blocked;
      lbl_block_reason.Visible = _card_data.Blocked;
      pb_blocked.Visible = _card_data.Blocked;

      // If must show comments, enabled buttons.
      if (m_must_show_comments)
      {
        m_tick_count_tracknumber_read = 0;
        m_swipe_card = true;
      }

      SetButtonStatus(!_card_data.Blocked);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Undo the last currency exchange
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    private void UndoLastCurrencyExchange()
    {
      String _str_error;
      String _str_message;
      String _str_generic_error = String.Empty;
      DialogResult _dlg_rc = DialogResult.None;
      CashierSessionInfo _cashier_session_info;
      AccountOperations.Operation _account_operation;
      CurrencyIsoType _iso_type;
      Decimal _iso_amount;
      OperationUndo.UndoError _undo_error;
      ArrayList _voucher_list;
      CardData _card_data = null;

      _str_generic_error = Resource.String("STR_CURRENCY_EXCHANGE_GENERIC_ERROR_EXCHANGE");

      try
      {
        if (!Misc.IsMultiCurrencyExchangeEnabled())
        {
          return;
        }

        if (!GetCardData(m_current_card_id, out _card_data))
        {
          return;
        }

        form_yes_no.Show(this.ParentForm);

        if (_card_data.AccountId > 0)
        {
          if (!IsCustomerRegisteredInReception(_card_data))
          {
            return;
          }

          _cashier_session_info = Cashier.CashierSessionInfo();

          // 1. Get last reversible operation
          if (!OperationUndo.GetLastReversibleOperation(OperationCode.CURRENCY_EXCHANGE_CHANGE, _card_data, _cashier_session_info.CashierSessionId, out _account_operation,
                                                out _iso_type, out _iso_amount, out _undo_error))
          {
            _str_error = OperationUndo.GetErrorMessage(_undo_error);

            frm_message.Show(_str_error, Resource.String("STR_APP_GEN_MSG_ERROR"),
                             MessageBoxButtons.OK, MessageBoxIcon.Error, this.ParentForm);

            return;
          }

          // 2. Ask confirmation to user
          Currency _currency = new Currency();
          _currency = _iso_amount;
          _currency.CurrencyIsoCode = _iso_type.IsoCode;

          _str_message = Resource.String("STR_UNDO_LAST_CURRENCY_EXCHANGE", Currency.Format(_iso_amount, _iso_type.IsoCode));
          _str_message = _str_message.Replace("\\r\\n", "\r\n");
          _dlg_rc = frm_message.Show(_str_message,
                                     Resource.String("STR_CURRENCY_EXCHANGE_UNDO_LAST_EXCHANGE"),
                                     MessageBoxButtons.YesNo, Images.CashierImage.Question, Cashier.MainForm);


          if (_dlg_rc != DialogResult.OK)
          {
            return;
          }

          // 3. Undo operation
          OperationUndo.InputUndoOperation _params = new OperationUndo.InputUndoOperation();
          _params.CodeOperation = OperationCode.CURRENCY_EXCHANGE_CHANGE;
          _params.CardData = _card_data;
          _params.OperationIdForUndo = _account_operation.OperationId;
          _params.CashierSessionInfo = _cashier_session_info;

          if (!OperationUndo.UndoOperation(_params, out _voucher_list))
          {
            frm_message.Show(_str_generic_error, Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this.ParentForm);

            return;
          }

          // Print vouchers
          VoucherPrint.Print(_voucher_list);

        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        frm_message.Show(_str_generic_error, Resource.String("STR_APP_GEN_MSG_ERROR"),
                MessageBoxButtons.OK, Images.CashierImage.Error, this.ParentForm);
      }
      finally
      {
        form_yes_no.Hide();
        this.RestoreParentFocus();
      }

    }

    //------------------------------------------------------------------------------
    // PURPOSE: Undo the last currency devolution
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    private void UndoLastDevolution()
    {
      String _str_error;
      String _str_message;
      String _str_generic_error = String.Empty;
      DialogResult _dlg_rc = DialogResult.None;
      CashierSessionInfo _cashier_session_info;
      AccountOperations.Operation _account_operation;
      CurrencyIsoType _iso_type;
      Decimal _iso_amount;
      OperationUndo.UndoError _undo_error;
      ArrayList _voucher_list;
      CardData _card_data = null;

      _str_generic_error = Resource.String("STR_CURRENCY_EXCHANGE_GENERIC_ERROR_DEVOLUTION");

      try
      {
        if (!Misc.IsMultiCurrencyExchangeEnabled())
        {
          return;
        }

        if (!GetCardData(m_current_card_id, out _card_data))
        {
          return;
        }

        form_yes_no.Show(this.ParentForm);

        if (_card_data.AccountId > 0)
        {
          if (!IsCustomerRegisteredInReception(_card_data))
          {
            return;
          }

          _cashier_session_info = Cashier.CashierSessionInfo();

          // 1. Get last reversible operation
          if (!OperationUndo.GetLastReversibleOperation(OperationCode.CURRENCY_EXCHANGE_DEVOLUTION, _card_data, _cashier_session_info.CashierSessionId, out _account_operation,
                                                out _iso_type, out _iso_amount, out _undo_error))
          {
            _str_error = OperationUndo.GetErrorMessage(_undo_error);

            frm_message.Show(_str_error, Resource.String("STR_APP_GEN_MSG_ERROR"),
                             MessageBoxButtons.OK, MessageBoxIcon.Error, this.ParentForm);

            return;
          }

          // 2. Ask confirmation to user
          _str_message = Resource.String("STR_UNDO_LAST_CURRENCY_DEVOLUTION", Currency.Format(_iso_amount, _iso_type.IsoCode));
          _str_message = _str_message.Replace("\\r\\n", "\r\n");
          _dlg_rc = frm_message.Show(_str_message,
                                     Resource.String("STR_CURRENCY_EXCHANGE_UNDO_LAST_DEVOLUTION"),
                                     MessageBoxButtons.YesNo, Images.CashierImage.Question, Cashier.MainForm);
          if (_dlg_rc != DialogResult.OK)
          {
            return;
          }

          // 3. Undo operation
          OperationUndo.InputUndoOperation _params = new OperationUndo.InputUndoOperation();
          _params.CodeOperation = OperationCode.CURRENCY_EXCHANGE_DEVOLUTION;
          _params.CardData = _card_data;
          _params.OperationIdForUndo = _account_operation.OperationId;
          _params.CashierSessionInfo = _cashier_session_info;

          if (!OperationUndo.UndoOperation(_params, out _voucher_list))
          {
            frm_message.Show(_str_generic_error, Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this.ParentForm);

            return;
          }

          // Print vouchers
          VoucherPrint.Print(_voucher_list);

        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        frm_message.Show(_str_generic_error, Resource.String("STR_APP_GEN_MSG_ERROR"),
                MessageBoxButtons.OK, Images.CashierImage.Error, this.ParentForm);
      }
      finally
      {
        form_yes_no.Hide();
        this.RestoreParentFocus();
      }

    }

    //------------------------------------------------------------------------------
    // PURPOSE: Undo last cash advance (card or check) selected in the list
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    private void UndoCardCash_CashAdvanceList()
    {
      CardData _card_data;
      List<CashierMovementsInfo> _cashier_movements_info_list;
      String _str_error;


      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.CashAdvandce_Return,
                                               ProfilePermissions.TypeOperation.RequestPasswd, m_parent, out _str_error))
      {
        return;
      }

      try
      {
        if (!GetCardData(m_current_card_id, out _card_data))
        {
          return;
        }

        if (!IsCustomerRegisteredInReception(_card_data))
        {
          return;
        }

        CashAdvance _cash_advance = new CashAdvance();
        _cash_advance.GetCashAdvanceListForUndo(_card_data, out _cashier_movements_info_list);

        frm_cash_advance_undo _frm = new frm_cash_advance_undo();
        _frm.Show(_card_data, _cashier_movements_info_list, m_parent);

      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Undo las cash advance
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    private void UndoLastCashAdvance()
    {
      AccountOperations.Operation _account_operation;
      CashierSessionInfo _cashier_session_info;
      String _str_message;
      String _str_error;
      String _str_national_currency;
      String _caption;
      String _str_amount;
      DialogResult _dlg_rc;
      CardData _card_data;
      CurrencyIsoType _iso_type;
      Decimal _iso_amount;
      OperationUndo.UndoError _undo_error;
      ArrayList _voucher_list;

      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.UndoLastCashAdvance,
                                                ProfilePermissions.TypeOperation.RequestPasswd, this.ParentForm, out _str_error))
      {
        return;
      }

      _cashier_session_info = Cashier.CashierSessionInfo();
      _card_data = account_watcher.Card;


      if (m_current_card_id == 0)
      {
        _card_data = new CardData();
      }

      if (!OperationUndo.GetLastReversibleOperation(OperationCode.CASH_ADVANCE, _card_data, _cashier_session_info.CashierSessionId, out _account_operation,
                                                    out _iso_type, out _iso_amount, out _undo_error))
      {
        _str_error = OperationUndo.GetErrorMessage(_undo_error);

        frm_message.Show(_str_error, Resource.String("STR_APP_GEN_MSG_ERROR"),
                         MessageBoxButtons.OK, MessageBoxIcon.Error, this.ParentForm);

        return;
      }

      _str_message = "";


      _str_national_currency = CurrencyExchange.GetNationalCurrency();

      // Check if it's national currency
      if (_iso_type.IsoCode == _str_national_currency || String.IsNullOrEmpty(_iso_type.IsoCode))
      {
        _str_amount = ((Currency)_iso_amount).ToString();
      }
      else
      {
        _str_amount = Currency.Format(_iso_amount, _iso_type.IsoCode);
      }

      // Check if type is not Currency
      switch (_iso_type.Type)
      {
        case CurrencyExchangeType.CARD:
          _str_amount += " (" + Resource.String("STR_FRM_AMOUNT_INPUT_CREDIT_CARD_PAYMENT") + ")";
          break;

        case CurrencyExchangeType.CHECK:
          _str_amount += " (" + Resource.String("STR_FRM_AMOUNT_INPUT_CHECK_PAYMENT") + ")";
          break;

        default:
          break;
      }

      _caption = Resource.String("STR_UNDO_LAST_OPERATION_CASH_ADVANCE");
      _str_message = Resource.String("STR_UNDO_CASH_ADVANCE", _str_amount);

      // Show information message
      _str_message = _str_message.Replace("\\r\\n", "\r\n");
      _dlg_rc = frm_message.Show(_str_message,
                                 _caption,
                                 MessageBoxButtons.YesNo, Images.CashierImage.Question, Cashier.MainForm);

      if (_dlg_rc != DialogResult.OK)
      {
        return;
      }

      account_watcher.RefreshAccount(_card_data);
      _card_data = account_watcher.Card;

      if (m_current_card_id == 0) // Virtual Account
      {
        _card_data = new CardData();
        if (!CardData.DB_CardGetAllData(_account_operation.AccountId, _card_data))
        {
          Log.Error("uc_ticket_create: DB_GetCardAllData. Error reading card.");

          return;
        }
      }

      OperationUndo.InputUndoOperation _params = new OperationUndo.InputUndoOperation();
      _params.CodeOperation = OperationCode.CASH_ADVANCE;
      _params.CardData = _card_data;
      _params.OperationIdForUndo = _account_operation.OperationId;
      _params.CashierSessionInfo = _cashier_session_info;

      if (!OperationUndo.UndoOperation(_params, out _voucher_list))
      {
        frm_message.Show(Resource.String("STR_UNDO_CASH_ADVANCE_ERROR"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this.ParentForm);

        return;
      }

      // Print vouchers
      VoucherPrint.Print(_voucher_list);

    }

    //------------------------------------------------------------------------------
    // PURPOSE: Checks if the customer has been registered in reception
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    private Boolean IsCustomerRegisteredInReception(CardData CardData)
    {
      // Checks if the customer doesn't visit the casino in this working date
      if (!GeneralParam.GetBoolean("MultiCurrencyExchange", "ReceptionRequired", false))
      {
        return true;
      }

      if (!Entrance.CustomerHasVisitedToday(CardData.AccountId))
      {
        frm_message.Show(Resource.String("STR_RECEPTION_CUSTOMER_NOT_REGISTERED"), Resource.String("STR_APP_GEN_MSG_WARNING"),
                                           MessageBoxButtons.OK, MessageBoxIcon.Error, m_parent);
        return false;
      }

      return true;
    }

    #endregion

    #region Publics

    //------------------------------------------------------------------------------
    // PURPOSE: Check permissions to redeem ticket including AML
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    public Boolean CheckPermissionsToRedeem(Int64 AccountId, Currency TotalToPay, Currency PrizeAmount,
                                            List<ProfilePermissions.CashierFormFuncionality> PermissionList)
    {
      Int32 _max_allowed_int;
      Currency _max_allowed;
      Currency _min_limit_exceded;
      String _message;

      _min_limit_exceded = -1;

      // ACM 14-MAY-2013:  Check MaxAllowedCashOut.
      _max_allowed_int = GeneralParam.GetInt32("Cashier", "MaxAllowedCashOut");
      _max_allowed = _max_allowed_int;

      if (_max_allowed > 0 && TotalToPay > _max_allowed)
      {
        _min_limit_exceded = _max_allowed;
        PermissionList.Add(ProfilePermissions.CashierFormFuncionality.CardSubtractCreditMaxAllowed);
      }

      // Show popup for max allowed exceded.
      if (_min_limit_exceded != -1)
      {
        _message = Resource.String("STR_CARD_SUBTRACT_CREDIT_MAX_EXCEDED", TotalToPay.ToString(), _min_limit_exceded.ToString());
        _message = _message.Replace("\\r\\n", "\r\n");

        if (frm_message.Show(_message, Resource.String("STR_APP_GEN_MSG_WARNING"),
            MessageBoxButtons.YesNo, Images.CashierImage.Question, Cashier.MainForm) != DialogResult.OK)
        {
          return false;
        }
      }

      return true;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Initialize control resources. (NLS string, images, etc.).
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    public void InitializeControlResources()
    {/*
      lbl_ticket_create_title.Text = Resource.String("STR_UC_CREATE_TICKET_TITLE");
      if (Chips.IsGamingTablesEnabled() || CurrencyExchange.IsCashAdvanceEnabled())
      {
        lbl_ticket_create_title.Text = Resource.String("STR_UC_CREATE_TICKET_TITLE_OTHER");
      }*/

      lbl_account_id_name.Text = Resource.String("STR_FORMAT_GENERAL_NAME_ACCOUNT") + ":";

      lbl_id_title.Text = Resource.String("STR_FRM_PLAYER_EDIT_TYPE_DOCUMENT") + ":";
      lbl_birth_title.Text = Resource.String("STR_UC_PLAYER_TRACK_BIRTH") + ":";
      lbl_name.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_NAME") + ":";
      lbl_trackdata.Text = Resource.String("STR_FORMAT_GENERAL_NAME_CARD") + ":";

      gb_chips.HeaderText = Resource.String("STR_UC_TICKET_CREATE_GB_CHIPS");
      gb_account.HeaderText = Resource.String("STR_FORMAT_GENERAL_NAME_ACCOUNT");
      gb_tickets.HeaderText = Resource.String("STR_FRM_TITO_TICKETS_DISCARD_GROUP");
      gb_account.Refresh();
      btn_create_ticket.Text = Resource.String("STR_UC_TITO_TICKET_CREATE_BTN_CREATE");
      btn_chips_purchase.Text = Resource.String("STR_UC_TITO_TICKET_CREATE_BTN_CHIPS_PURCHASE");
      btn_chips_sale_with_recharge.Text = Resource.String("STR_UC_TITO_TICKET_CREATE_BTN_CHIPS_SALE_WITH_RECHARGE");
      btn_undo_chips_purchase.Text = Resource.String("STR_UC_TICKET_CREATE_UNDO_LAST_OPERATION_SALE_CHIPS_1") + Environment.NewLine + Resource.String("STR_UC_TICKET_CREATE_UNDO_LAST_OPERATION_PURCHASE_CHIPS"); // LTC 01-JUL-2016
      btn_undo_chips_sale_with_recharge.Text = Resource.String("STR_UC_TICKET_CREATE_UNDO_LAST_OPERATION_SALE_CHIPS_1") + Environment.NewLine + Resource.String("STR_UC_TICKET_CREATE_UNDO_LAST_OPERATION_SALE_CHIPS_2");
      btn_promotions.Text = Resource.String("STR_FRM_TITO_PROMO_001");

      gb_cash_advance.HeaderText = Resource.String("STR_TITLE_CARD_CHECK");
      btn_cash_advance.Text = Resource.String("STR_VOUCHER_CASH_ADVANCE");

      gb_divisas.HeaderText = Resource.String("STR_CURRENCY");
      btn_change_currency.Text = Resource.String("STR_VOUCHER_MULTI_EXCHANGE");
      btn_devolution_currency.Text = Resource.String("STR_VOUCHER_MULTI_EXCHANGE_DEVOLUTION");
      btn_undo_last_exchange_currency.Text = Resource.String("STR_CURRENCY_EXCHANGE_UNDO_LAST_EXCHANGE");
      btn_undo_last_devolution_currency.Text = Resource.String("STR_CURRENCY_EXCHANGE_UNDO_LAST_DEVOLUTION");


      if (Misc.IsMultiCurrencyExchangeEnabled())
        btn_undo_cash_advance.Text = Resource.String("STR_REFUND_CARD_CHECK");

      else
        btn_undo_cash_advance.Text = Resource.String("STR_UNDO_LAST_OPERATION_CASH_ADVANCE");

      uc_card_reader1.InitializeControlResources();
      form_credit.InitializeControlResources();
      form_search_players.InitializeControlResources();
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Init controls and events
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    public void InitControls(frm_container Parent)
    {
      m_parent = Parent;

      form_credit = new frm_amount_input(this.CheckPermissionsToRedeem, null);

      // 11-NOV-2015 AVZ TODO Imagen a cambiar por la de anonimo.
      pb_user.Image = Resources.ResourceImages.anonymous_user;
      pb_blocked.Image = Resources.ResourceImages.locked;

      ClearCardData();

      if (m_first_time)
      {
        m_first_time = false;

        account_watcher = new AccountWatcher();
        account_watcher.Start();
        account_watcher.AccountChangedEvent += new AccountWatcher.AccountChangedHandler(account_watcher_AccountChangedEvent);

        uc_card_reader1.OnTrackNumberReadEvent += new uc_card_reader.TrackNumberReadEventHandler(uc_card_reader1_OnTrackNumberReadEvent);

        timer1.Enabled = true;
        timer1.Tick += new System.EventHandler(timer1_Tick);

        btn_player_browse.Click += new EventHandler(btn_button_clicked);
        btn_create_ticket.Click += new EventHandler(btn_button_clicked);
        btn_chips_purchase.Click += new EventHandler(btn_button_clicked);
        btn_chips_sale_with_recharge.Click += new EventHandler(btn_button_clicked);
        btn_undo_chips_purchase.Click += new EventHandler(btn_button_clicked);
        btn_undo_chips_sale_with_recharge.Click += new EventHandler(btn_button_clicked);
        btn_promotions.Click += new EventHandler(btn_button_clicked);
        btn_cash_advance.Click += new EventHandler(btn_button_clicked);
        btn_undo_cash_advance.Click += new EventHandler(btn_button_clicked);
        btn_change_currency.Click += new EventHandler(btn_button_clicked);
        btn_devolution_currency.Click += new EventHandler(btn_button_clicked);
        btn_undo_last_exchange_currency.Click += new EventHandler(btn_button_clicked);
        btn_undo_last_devolution_currency.Click += new EventHandler(btn_button_clicked);

        InitializeControlResources();
      }

      if (GamingTableBusinessLogic.IsGamingTablesEnabled())
      {
        this.gb_chips.Visible = true;
        if (!GeneralParam.GetBoolean("GamingTables", "Cashier.ChipsSaleEnabled"))
        {
          this.btn_chips_sale_with_recharge.Visible = false;
          this.btn_undo_chips_sale_with_recharge.Visible = false;
          this.btn_chips_purchase.Location = this.btn_chips_sale_with_recharge.Location;
          this.btn_undo_chips_purchase.Location = this.btn_undo_chips_sale_with_recharge.Location;
          this.gb_chips.Size = new Size(182, this.gb_chips.Size.Height);
        }
      }

      this.gb_divisas.Visible = Misc.IsMultiCurrencyExchangeEnabled();
      this.gb_tickets.Visible = true;
      this.gb_cash_advance.Visible = CurrencyExchange.IsCashAdvanceEnabled();
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Set initial focus
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    public void InitFocus()
    {
      uc_card_reader1.Focus();
    }

    #endregion

    #region Controls Events

    void account_watcher_AccountChangedEvent()
    {
      if (this.InvokeRequired)
      {
        Delegate _method;

        _method = null;

        try
        {
          _method = new AccountWatcher.AccountChangedHandler(account_watcher_AccountChangedEvent);
          if (_method != null)
          {
            this.BeginInvoke(_method);
          }
          else
          {
            Log.Error("Account changed: nobody taking care of the event, the method is null.");
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }
        return;
      }

      RefreshData();
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Event fired when a card is swiped.
    // 
    //  PARAMS:
    //      - INPUT: TrackNumber: number typed or swiped 
    //
    //      - OUTPUT: IsValid: is valid external trackdata number
    //
    // RETURNS:
    // 
    //   NOTES:
    public void uc_card_reader1_OnTrackNumberReadEvent(string TrackNumber, ref Boolean IsValid)
    {
      CardData _card_data;
      _card_data = new CardData();

      if (!CardData.DB_CardGetAllData(TrackNumber, _card_data, false))
      {
        IsValid = false;
      }

      //If not exitst, then create account, else Load the Account
      m_is_new_card = _card_data.IsNew;
      if (_card_data.IsNew)
      {
        _card_data.TrackData = TrackNumber;
        NewAccount(_card_data);
        m_tick_count_tracknumber_read = 0;
      }
      else
      {
        m_pending_check_comments = true;
        if (!_card_data.IsVirtualCard)
        {
          uc_card_reader1_OnTrackNumberReadEvent(_card_data.AccountId, ref IsValid);
        }
        else
        {
          IsValid = false;
        }
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Event called after a card is swiped
    //          and called when a Player is searched
    // 
    //  PARAMS:
    //      - INPUT: TrackNumber: number typed or swiped 
    //
    //      - OUTPUT: IsValid: is valid external trackdata number
    //
    // RETURNS:
    // 
    //   NOTES:
    public void uc_card_reader1_OnTrackNumberReadEvent(Int64 AccountId, ref Boolean IsValid)
    {

      m_swipe_card = true;

      IsValid = LoadCardByAccountId(AccountId);

      m_tick_count_tracknumber_read = 0;
      m_is_new_card = false;
    }

    #endregion

    #region Buttons Events

    private void btn_edit_Click(object sender, EventArgs e)
    {
      CardData _card_data;
      String error_str;
      DialogResult _dgr_ac_edit;

      if (m_is_new_card)
      {
        // AJQ 29-MAY-2013, Clear data on new Track read & Stop the AccountWatcher
        _card_data = new CardData();
        _card_data.TrackData = m_current_track_number;
        account_watcher.RefreshAccount(_card_data);
        ClearCardData();
      }
      else
      {
        ////0 byAccountId, 1 byAccountId+TrackData (Anonymous), 3 byAccountId+TrackData(Anonymous+Personal)
        if (account_watcher.Card.PlayerTracking.CardLevel == 0)
        {
          if (GeneralParam.GetInt32("AntiMoneyLaundering", "GroupByMode", 0) == 1)
          {
            frm_message.Show(Resource.String("STR_UC_CARD_AML_CANT_CUSTOMIZE_ACCOUNT"),
                              Resource.String("STR_MSG_ANTIMONEYLAUNDERING_WARNING"), MessageBoxButtons.OK, MessageBoxIcon.Warning, this.ParentForm);
            return;
          }
        }
        _card_data = account_watcher.Card;

        if (!m_must_show_comments)
        {
          // RMS 14-OCT-2013: not new card, check edition permission depending on account type anonymous or personal
          ProfilePermissions.CashierFormFuncionality _permission;

          _permission = (account_watcher.Card.PlayerTracking.CardLevel > 0) ? ProfilePermissions.CashierFormFuncionality.AccountEditPersonal :
                                                                              ProfilePermissions.CashierFormFuncionality.AccountEditAnonymous;

          if (!ProfilePermissions.CheckPermissionList(_permission, ProfilePermissions.TypeOperation.RequestPasswd, this.ParentForm, 0, out error_str))
          {
            return;
          }
        }
      }

      using (frm_yesno _shader = new frm_yesno())
      {
        using (PlayerEditDetailsCashierView _view = new PlayerEditDetailsCashierView())
        {
          using (PlayerEditDetailsCashierController _controller =
            new PlayerEditDetailsCashierController(_card_data, _view, IDScanner, PlayerEditDetailsCashierView.SCREEN_MODE.DEFAULT))
          {

            _controller.MustShowComments = m_must_show_comments;

            if (WindowManager.StartUpMode == WindowManager.StartUpModes.Explorer)
            {
              _view.Location = new Point(WindowManager.GetFormCenterLocation(_shader).X - (_view.Width / 2),
                _shader.Location.Y + 2);
            }
            _shader.Opacity = 0.6;
            _shader.Show();
            _dgr_ac_edit = _view.ShowDialog(_shader);
          }
        }
      }


      if (_dgr_ac_edit == DialogResult.OK)
      {
        // AJQ 29-MAY-2013, Reload the data of the "TrackData" being edited
        // Error: use the m_current_track_number that was refreshed on background)
        //        The background refresh has been disabled for tracks that are not associated to any account.
        CardData.DB_CardGetAllData(_card_data.TrackData, _card_data, false);
        m_is_new_card = _card_data.IsNew;
        m_pending_check_comments = false;
      }

      if (_card_data.AccountId != 0)
      {
        // AJQ 29-MAY-2013, Refresh the data on Screen when there is an account.
        //                  AccountId is 0 when the "Personalizacion" has been cancelled and the card was "new".
        //                  Avoids displaying the message "The card doesn't belong to the site".
        LoadCardByAccountId(_card_data.AccountId);
      }
    }

    private void btn_button_clicked(object sender, EventArgs e)
    {
      // LEM 07-JAN-2014: Accept only mouse events.
      if (!(e is MouseEventArgs))
      {
        return;
      }

      Cashier.ResetAuthorizedByUser();

      if (!uc_card_reader1.Focused)
      {
        uc_card_reader1.Focus();
      }

      try
      {
        if (sender.Equals(btn_create_ticket))
        {
          btn_create_ticket_Click(sender, e);
        }
        if (sender.Equals(btn_chips_purchase))
        {
          btn_chips_purchase_Click(sender, e);
        }
        if (sender.Equals(btn_chips_sale_with_recharge))
        {
          btn_chips_sale_with_recharge_Click(sender, e);
        }
        if (sender.Equals(btn_undo_chips_purchase))
        {
          btn_undo_chips_purchase_Click(sender, e);
        }
        if (sender.Equals(btn_undo_chips_sale_with_recharge))
        {
          btn_undo_chips_sale_with_recharge_Click(sender, e);
        }
        if (sender.Equals(btn_promotions))
        {
          btn_promotions_Click(sender, e);
        }
        if (sender.Equals(btn_cash_advance))
        {
          btn_cash_advance_Click(sender, e);
        }
        if (sender.Equals(btn_undo_cash_advance))
        {
          btn_undo_cash_advance_Click(sender, e);
        }
        if (sender.Equals(btn_player_browse))
        {
          btn_player_browse_Click(sender, e);

          return;
        }

        if (sender.Equals(btn_change_currency))
        {
          btn_change_currency_Click(sender, e);

          return;
        }

        if (sender.Equals(btn_devolution_currency))
        {
          btn_devolution_currency_Click(sender, e);

          return;
        }

        if (sender.Equals(btn_undo_last_exchange_currency))
        {
          btn_undo_last_exchange_currency_Click(sender, e);

          return;
        }

        if (sender.Equals(btn_undo_last_devolution_currency))
        {
          btn_undo_last_devolution_currency_Click(sender, e);

          return;
        }
      }
      finally
      {
        Cashier.ResetAuthorizedByUser();

        if (!uc_card_reader1.Focused)
        {
          uc_card_reader1.Focus();
        }
      }
    }

    //private void btn_chips_stock_Click(object sender, EventArgs e)
    //{
    //  CardData _card_data = null;
    //  frm_chips _frm_chips;
    //  Boolean _check_redeem;
    //  ChipsAmount _dummy_chips_amount;
    //  String _error_str;

    //  // Check if current user is an authorized user
    //  if (!ProfilePermissions.CheckPermissionList(ProfilePermissions.CashierFormFuncionality.ChipsStockControl,
    //                                              ProfilePermissions.TypeOperation.RequestPasswd, this.ParentForm, 0, out _error_str))
    //  {
    //    return;
    //  }

    //  _dummy_chips_amount = new ChipsAmount(Cashier.SessionId, CASHIER_MOVEMENT.NOT_ASSIGNED);

    //  _frm_chips = new frm_chips();

    //  form_yes_no.Show(this.ParentForm);

    //  _frm_chips.Show(_card_data, true, null, FormType.Stock, ref _dummy_chips_amount, out _check_redeem);

    //  form_yes_no.Hide();
    //}

    private void btn_create_ticket_Click(object sender, EventArgs e)
    {
      CardData _card_data = null;
      ParamsTicketOperation _operation_params;
      TicketCreationParams _params;
      ENUM_TITO_OPERATIONS_RESULT _result;

      _result = ENUM_TITO_OPERATIONS_RESULT.NONE;

      try
      {
        _card_data = account_watcher.Card;

        if (m_current_card_id == 0)
        {
          if (!CardData.DB_VirtualCardGetAllData(Cashier.TerminalName, AccountType.ACCOUNT_VIRTUAL_CASHIER, _card_data))
          {
            return;
          }
        }

        form_yes_no.Show(this.ParentForm);

        _operation_params = new ParamsTicketOperation();
        _operation_params.in_account = _card_data;
        _operation_params.in_window_parent = this.ParentForm;
        _operation_params.session_info = Cashier.CashierSessionInfo();

        _result = TITO.BusinessLogic.PreviewVoucherCreateTicket(_operation_params, out _params);
        if (_result != ENUM_TITO_OPERATIONS_RESULT.OK)
        {
          return;
        }

        _result = TITO.BusinessLogic.CashInCreateTicket(_operation_params, _params);

        if (_result == ENUM_TITO_OPERATIONS_RESULT.OK ||
            _result == ENUM_TITO_OPERATIONS_RESULT.AUX_OPERATION_CANCELED)
        {
          RefreshAccount();
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return;
      }
      finally
      {
        switch (_result)
        {
          case ENUM_TITO_OPERATIONS_RESULT.PRINTER_PRINT_FAIL:
          case ENUM_TITO_OPERATIONS_RESULT.PRINTER_IS_NOT_READY:
            PrinterStatus _status;
            String _status_text;

            TitoPrinter.GetStatus(out _status, out _status_text);
            _status_text = TitoPrinter.GetMsgError(_status);

            if (_result == ENUM_TITO_OPERATIONS_RESULT.PRINTER_PRINT_FAIL)
            {
              _status_text += "\r\n" + Resource.String("STR_FRM_CONTAINER_EVENTS_DESC_PRINTER_CANCEL_OPERATION");
            }

            frm_message.Show(_status_text,
                             Resource.String("STR_APP_GEN_MSG_ERROR"),
                             MessageBoxButtons.OK,
                             MessageBoxIcon.Error,
                             this.ParentForm);
            break;
          case ENUM_TITO_OPERATIONS_RESULT.TPV_NOT_PAID:
            frm_message.Show(Resource.String("STR_GENERIC_ERROR_TPV_PAY"),
                             Resource.String("STR_TITO_TICKET_ERROR_CREATING_TICKET"),
                             MessageBoxButtons.OK,
                             MessageBoxIcon.Error,
                             this.ParentForm);
            break;
          case ENUM_TITO_OPERATIONS_RESULT.JUNKETS_KO:
             frm_message.Show(Resource.String("STR_JUNKET_ERROR"),
                             Resource.String("STR_TITO_TICKET_ERROR_CREATING_TICKET"),
                             MessageBoxButtons.OK,
                             MessageBoxIcon.Error,
                             this.ParentForm);
            break;
          case ENUM_TITO_OPERATIONS_RESULT.OK:
          case ENUM_TITO_OPERATIONS_RESULT.AUX_OPERATION_CANCELED:
          case ENUM_TITO_OPERATIONS_RESULT.CREDIT_LINE_KO:
            break;
          default:
            frm_message.Show(Resource.String("STR_TITO_TICKET_ERROR_CREATING_TICKET"),
                             Resource.String("STR_APP_GEN_MSG_ERROR"),
                             MessageBoxButtons.OK,
                             MessageBoxIcon.Error,
                             this.ParentForm);
            break;
        }

        form_yes_no.Hide();
        this.RestoreParentFocus();
      }
    }

    private void btn_chips_purchase_Click(object sender, EventArgs e)
    {
      CardData _card_data;
      FeatureChips.ChipsOperation _chip_operation;
      ParamsTicketOperation _operations_params;
      String _error_str;

      // Check if current user is an authorized user
      if (!ProfilePermissions.CheckPermissionList(ProfilePermissions.CashierFormFuncionality.ChipsPurchase,
                                                  ProfilePermissions.TypeOperation.RequestPasswd,
                                                  this.ParentForm,
                                                  0,
                                                  out _error_str))
      {
        return;
      }

      _card_data = account_watcher.Card;

      if (m_current_card_id == 0)
      {
        if (!CardData.DB_VirtualCardGetAllData(Cashier.TerminalName, AccountType.ACCOUNT_VIRTUAL_CASHIER, _card_data))
        {
          return;
        }
      }

      try
      {
        form_yes_no.Show(this.ParentForm);

        PaymentThresholdAuthorization _payment_threshold_authorization;

        if (!CashierChips.ChipsPurchase_GetAmount(_card_data, this.CheckPermissionsToRedeem, form_yes_no, out _chip_operation, out _operations_params, out _payment_threshold_authorization))
        {
          return;
        }

        account_watcher.RefreshAccount(_card_data);
        _card_data = account_watcher.Card;

        if (!CashierChips.ChipsPurchase_Save(_card_data,
                                             _chip_operation,
                                             _operations_params,
                                             _payment_threshold_authorization, 
                                             out _error_str))
        {
          if (String.IsNullOrEmpty(_error_str))
          {
            _error_str = Resource.String("STR_UC_CARD_USER_MSG_GENERIC_ERROR");
          }

          frm_message.Show(_error_str
                          , Resource.String("STR_APP_GEN_MSG_ERROR")
                          , MessageBoxButtons.OK
                          , MessageBoxIcon.Error
                          , ParentForm);

          return;
        }
      }
      finally
      {
        form_yes_no.Hide();
        this.RestoreParentFocus();
      }

      RefreshAccount();
    }

    private void btn_chips_sale_with_recharge_Click(object sender, EventArgs e)
    {
      List<ProfilePermissions.CashierFormFuncionality> _list_permissions;
      CardData _card_data;
      CurrencyExchangeResult _exchange_result;
      CashierOperations.TYPE_CARD_CASH_IN _cash_operation;
      FeatureChips.ChipsOperation _chips_amount;
      String _error_str;
      MessageBoxIcon _message_icon;
      String _message_caption;
      CashierSessionInfo _cashier_session_info;
      Boolean _chips_sale_mode;
      // LTC 23-SEP-2016
      RechargeOutputParameters _outputParameter;
      ParticipateInCashDeskDraw _participate_in_cash_draw;
      Ticket _ticket_copy_dealer;

      _chips_sale_mode = GeneralParam.GetBoolean("GamingTables", "Cashier.Mode");
      _ticket_copy_dealer = null;

      // Check if current user is an authorized user
      _list_permissions = new List<ProfilePermissions.CashierFormFuncionality>();
      _list_permissions.Add(ProfilePermissions.CashierFormFuncionality.CardAddCredit);
      _list_permissions.Add(ProfilePermissions.CashierFormFuncionality.ChipsSale);

      if (!ProfilePermissions.CheckPermissionList(_list_permissions, ProfilePermissions.TypeOperation.RequestPasswd, this.ParentForm, 0, out _error_str))
      {
        return;
      }

      // TODO: REVISAR DHA, en principio no tiene sentido la validaci�n
      //if (!Chips.HasChipsBalance(Cashier.SessionId, _chips_sale_mode))
      //{
      //  frm_message.Show(Resource.String("STR_FRM_AMOUNT_INPUT_MSG_CHIPS_AMOUNT_EXCEED_BANK").Replace("\\r\\n", "\r\n"),
      //          Resource.String("STR_APP_GEN_MSG_WARNING"),
      //          MessageBoxButtons.OK,
      //          MessageBoxIcon.Warning,
      //          ParentForm);

      //  return;
      //}

      if (GamingTablesSessions.GamingTableInfo.IsGamingTable)
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _cashier_session_info = Cashier.CashierSessionInfo();
          GamingTablesSessions _gt_session;

          if (GamingTablesSessions.GetOrOpenSession(out _gt_session, _cashier_session_info.CashierSessionId, _db_trx.SqlTransaction))
          {
            if (_gt_session.InitialChipsAmount + _gt_session.FillChipsAmount + _gt_session.OwnPurchaseAmount + _gt_session.ExternalPurchaseAmount <= 0 && !_chips_sale_mode)
            {
              frm_message.Show(Resource.String("STR_FRM_CHIPS_ERROR_STOCK"),
                               Resource.String("STR_APP_GEN_MSG_WARNING"),
                               MessageBoxButtons.OK,
                               MessageBoxIcon.Warning,
                               ParentForm);

              return;
            }
          }
        }
      }

      _card_data = account_watcher.Card;

      if (m_current_card_id == 0)
      {
        if (!CardData.DB_VirtualCardGetAllData(Cashier.TerminalName, AccountType.ACCOUNT_VIRTUAL_CASHIER, _card_data))
        {
          frm_message.Show(Resource.String("STR_UC_CARD_USER_MSG_GENERIC_ERROR"),
                           Resource.String("STR_APP_GEN_MSG_ERROR"),
                           MessageBoxButtons.OK,
                           MessageBoxIcon.Error,
                           ParentForm);

          return;
        }
      }

      try
      {
        form_yes_no.Show(this.ParentForm);
        // LTC 23-SEP-2016

        PaymentThresholdAuthorization _payment_threshold_authorization; 
        if (!CashierChips.ChipsSale_GetAmount(_card_data, form_credit, VoucherTypes.ChipsSaleWithRecharge, null, form_yes_no, 
                                              out _chips_amount, out _cash_operation, out _exchange_result, out _participate_in_cash_draw, out _payment_threshold_authorization))
        {
          return;
        }

        account_watcher.RefreshAccount(_card_data);
        _card_data = account_watcher.Card;

        // JBP 15-JAN-2014: RequestForRefundIfExist
        if (!CashierChips.RequestForRefund(_chips_amount, _cash_operation, this.ParentForm, _exchange_result))
        {
          return;
        }

        if (!CashierChips.ChipsSaleWithRecharge_Save(_card_data, form_yes_no, _chips_amount, _cash_operation, _exchange_result, _payment_threshold_authorization, out _error_str, out _outputParameter, out _ticket_copy_dealer))
        {
          _message_icon = MessageBoxIcon.Warning;
          _message_caption = Resource.String("STR_APP_GEN_MSG_WARNING");

          // Generic error
          if (_error_str == String.Empty)
          {
            _error_str = Resource.String("STR_UC_CARD_USER_MSG_GENERIC_ERROR");
            _message_caption = Resource.String("STR_APP_GEN_MSG_ERROR");
            _message_icon = MessageBoxIcon.Error;
          }

          frm_message.Show(_error_str,
                           _message_caption,
                           MessageBoxButtons.OK,
                           _message_icon,
                           ParentForm);

          return;
        }
      }
      finally
      {
        form_yes_no.Hide();
        this.RestoreParentFocus();
      }

      RefreshAccount();
    }

    /// LTC 07-MAR-2016
    /// <summary>
    ///  Create Ticket for Chips Swap
    /// </summary>
    /// <param name="Tickets"></param>
    /// <param name="TicketsTotalAmount"></param>
    /// <param name="CardData"></param>
    public bool btn_chips_swap_with_ticket_tito_Click(ParamsTicketOperation Tickets, decimal TicketsTotalAmount, CardData CardData) // LTC 08-ABR-2016
    {
      List<ProfilePermissions.CashierFormFuncionality> _list_permissions;
      CurrencyExchangeResult _exchange_result;
      CashierOperations.TYPE_CARD_CASH_IN _cash_operation;
      FeatureChips.ChipsOperation _chips_operation;
      String _error_str;
      MessageBoxIcon _message_icon;
      String _message_caption;
      CashierSessionInfo _cashier_session_info;
      Boolean _chips_sale_mode;
      frm_chips _f_chips;
      frm_yesno _shader;
      LinkedGamingTable _selected_gaming_table;
      StringBuilder _message = new StringBuilder();  // LTC 08-ABR-2016
      Decimal _total_amount_tickets = 0;
      Decimal _total_amount_chips = 0;
      Ticket _ticket_copy_dealer;

      _chips_sale_mode = GeneralParam.GetBoolean("GamingTables", "Cashier.Mode");
      _f_chips = new frm_chips();
      _selected_gaming_table = new LinkedGamingTable();
      _shader = new frm_yesno();
      _ticket_copy_dealer = null;

      // Check if current user is an authorized user
      _list_permissions = new List<ProfilePermissions.CashierFormFuncionality>();
      _list_permissions.Add(ProfilePermissions.CashierFormFuncionality.CardAddCredit);
      _list_permissions.Add(ProfilePermissions.CashierFormFuncionality.TITO_ChipsSwap);

      //if (!GamingTableBusinessLogic.HasChipsBalance(Cashier.SessionId, _chips_sale_mode))
      //{
      //  frm_message.Show(Resource.String("STR_FRM_AMOUNT_INPUT_MSG_CHIPS_AMOUNT_EXCEED_BANK").Replace("\\r\\n", "\r\n"),
      //          Resource.String("STR_APP_GEN_MSG_WARNING"),
      //          MessageBoxButtons.OK,
      //          MessageBoxIcon.Warning,
      //          ParentForm);

      //  return false;  // LTC 08-ABR-2016
      //}

      if (GamingTablesSessions.GamingTableInfo.IsGamingTable)
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _cashier_session_info = Cashier.CashierSessionInfo();
          GamingTablesSessions _gt_session;

          if (GamingTablesSessions.GetOrOpenSession(out _gt_session, _cashier_session_info.CashierSessionId, _db_trx.SqlTransaction))
          {
            if (_gt_session.InitialChipsAmount + _gt_session.FillChipsAmount + _gt_session.OwnPurchaseAmount + _gt_session.ExternalPurchaseAmount <= 0 && !_chips_sale_mode)
            {
              frm_message.Show(Resource.String("STR_FRM_CHIPS_ERROR_STOCK"),
                               Resource.String("STR_APP_GEN_MSG_WARNING"),
                               MessageBoxButtons.OK,
                               MessageBoxIcon.Warning,
                               ParentForm);

              return false;  // LTC 08-ABR-2016
            }
          }
        }
      }

      try
      {
        form_yes_no.Show(this.ParentForm);
        if (!CashierChips.ChipsSwap_GetAmount(CardData, new frm_amount_input(), VoucherTypes.ChipsSwap, form_yes_no, TicketsTotalAmount,
                                              out _chips_operation, out _cash_operation, out _exchange_result))
        {
          return false;  // LTC 08-ABR-2016
        }
        form_yes_no.Hide();

        ///// Ask for User confirmmation

        _message_icon = MessageBoxIcon.Question;
        _message_caption = Resource.String("STR_PRINT_CARD_PRINTER_INPUT_INFO_MSG");

        // Total Tickets
        foreach (var item in Tickets.validation_numbers)
        {
          _total_amount_tickets += item.Amount;
        }

        //Total Chips
        _total_amount_chips = (decimal)_chips_operation.ChipsAmount;

        // LTC 08-ABR-2016
        _message.AppendLine(Resource.String("STR_UC_TITO_TICKET_CREATE_SWAP_TOT") + ": " + _total_amount_tickets.ToString("C2")); //Ticket TITO to Change
        _message.AppendLine("        " + Resource.String("STR_TICKET_TITO_SWAPS_CHIPS") + ": " + _total_amount_chips.ToString("C2")); //Chips Swap 
        _message.AppendLine("        " + Resource.String("STR_UC_TITO_TICKET_CREATE_SWAP_RESI") + ": " + (_total_amount_tickets - _total_amount_chips).ToString("C2")); //New Ticket TITO

        if (frm_message.Show(_message.ToString(),
                             _message_caption,
                             MessageBoxButtons.OKCancel,
                             _message_icon,
                             ParentForm) == DialogResult.Cancel)
        {
          return false;  // LTC 08-ABR-2016
        }

        /////

        // Save Operation
        if (!CashierChips.ChipsSwapWithTicketTito_Save(CardData, form_yes_no, _chips_operation, _cash_operation, _exchange_result, Tickets, out _error_str, out _ticket_copy_dealer))
        {
          _message_icon = MessageBoxIcon.Warning;
          _message_caption = Resource.String("STR_APP_GEN_MSG_WARNING");

          // Generic error
          if (_error_str == String.Empty)
          {
            _error_str = Resource.String("STR_UC_CARD_USER_MSG_GENERIC_ERROR");
            _message_caption = Resource.String("STR_APP_GEN_MSG_ERROR");
            _message_icon = MessageBoxIcon.Error;
          }

          frm_message.Show(_error_str,
                           _message_caption,
                           MessageBoxButtons.OK,
                           _message_icon,
                           ParentForm);

          return false;  // LTC 08-ABR-2016
        }

        return true;  // LTC 08-ABR-2016
      }
      finally
      {
        form_yes_no.Hide();
      }
    }

    private Boolean btn_player_browse_Click(object sender, EventArgs e)
    {
      Int64 _selected_account_id;
      Boolean _is_track_valid = true;
      Cursor _previous_cursor;
      String _error_str;

      _selected_account_id = -1;

      // Check if current user is an authorized user
      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.AccountSearchByName,
                                               ProfilePermissions.TypeOperation.RequestPasswd,
                                               this.ParentForm,
                                               out _error_str))
      {
        return false;
      }

      _previous_cursor = this.Cursor;

      try
      {
        this.Cursor = Cursors.WaitCursor;

        _selected_account_id = form_search_players.Show(this.ParentForm);

        if (_selected_account_id < 0)
        {
          return false;
        }

        // Load card data
        uc_card_reader1_OnTrackNumberReadEvent(_selected_account_id, ref _is_track_valid);

        return _is_track_valid;
      }
      finally
      {
        this.Cursor = _previous_cursor;
      }
    }

    private void btn_change_currency_Click(object sender, EventArgs e)
    {
      CardData _card_data = null;
      frm_currency_exchange frm_exchange = null;
      CurrencyExchangeResult _exchange_result;
      ExchangeMultiDenomination _exchange;
      VoucherMultiCurrencyExchange _voucher;
      String _str_error;

      if (!Misc.IsMultiCurrencyExchangeEnabled())
      {
        return;
      }

      if (ExchangeMultiDenomination.GetEnabledCurrencies() <= 0)
      {
        frm_message.Show(Resource.String("STR_NOT_ENABLED_CURRENCIES"), Resource.String("STR_APP_GEN_MSG_WARNING"),
                                              MessageBoxButtons.OK, MessageBoxIcon.Error, m_parent);
        return;
      }

      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.MultiCurrency_Exchange,
                                        ProfilePermissions.TypeOperation.RequestPasswd, m_parent, out _str_error))
      {
        return;
      }

      try
      {
        if (!GetCardData(m_current_card_id, out _card_data))
        {
          return;
        }

        form_yes_no.Show(m_parent);

        if (_card_data.AccountId > 0)
        {
          if (!IsCustomerRegisteredInReception(_card_data))
          {
            return;
          }

          frm_exchange = new frm_currency_exchange(_card_data, m_parent);
          if (frm_exchange.ShowDialog() == DialogResult.OK)
          {
            _exchange_result = frm_exchange.GetExchangeResult;

            _exchange = new ExchangeMultiDenomination();

            // Validate cash in the cashier
            if (_exchange_result.NetAmount > _exchange.GetCashCashierCurrentBalance(_exchange_result.OutCurrencyCode))
            {
              frm_message.Show(Resource.String("STR_FRM_AMOUNT_INPUT_MSG_AMOUNT_EXCEED_BANK"), Resource.String("STR_APP_GEN_MSG_WARNING"),
                                               MessageBoxButtons.OK, MessageBoxIcon.Error, m_parent);
              return;
            }

            if (_exchange.ChangeForeignCurrencyToNationalCurrency(Cashier.CashierSessionInfo(), _card_data, _exchange_result, out _voucher))
            {
              VoucherPrint.Print(_voucher);
            }
            else
            {
              frm_message.Show(Resource.String("STR_CURRENCY_EXCHANGE_GENERIC_ERROR_EXCHANGE"), Resource.String("STR_APP_GEN_MSG_ERROR"),
                                MessageBoxButtons.OK, Images.CashierImage.Error, m_parent);
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        frm_message.Show(Resource.String("STR_CURRENCY_EXCHANGE_GENERIC_ERROR_EXCHANGE"), Resource.String("STR_APP_GEN_MSG_ERROR"),
                MessageBoxButtons.OK, Images.CashierImage.Error, m_parent);
      }
      finally
      {
        form_yes_no.Hide();
        this.RestoreParentFocus();
      }
    }

    private void btn_devolution_currency_Click(object sender, EventArgs e)
    {
      CardData _card_data = null;
      frm_currency_refund frm_refund = null;
      CurrencyExchangeResult _exchange_result = null;
      VoucherMultiCurrencyExchange _voucher;
      ExchangeMultiDenomination _exchange;
      String _str_error;

      if (!Misc.IsMultiCurrencyExchangeEnabled())
      {
        return;
      }

      if (ExchangeMultiDenomination.GetEnabledCurrencies() <= 0)
      {
        frm_message.Show(Resource.String("STR_NOT_ENABLED_CURRENCIES"), Resource.String("STR_APP_GEN_MSG_WARNING"),
                                              MessageBoxButtons.OK, MessageBoxIcon.Error, m_parent);
        return;
      }

      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.MultiCurrency_Return,
                                   ProfilePermissions.TypeOperation.RequestPasswd, m_parent, out _str_error))
      {
        return;
      }

      try
      {
        if (!GetCardData(m_current_card_id, out _card_data))
        {
          return;
        }

        form_yes_no.Show(m_parent);

        if (_card_data.AccountId > 0)
        {
          if (!IsCustomerRegisteredInReception(_card_data))
          {
            return;
          }

          frm_refund = new frm_currency_refund(_card_data);
          if (frm_refund.ShowDialog() == DialogResult.OK)
          {
            _exchange_result = frm_refund.GetExchangeResult;

            _exchange = new ExchangeMultiDenomination();

            // Validate cash in the cashier
            if (_exchange_result.NetAmount > _exchange.GetCashCashierCurrentBalance(_exchange_result.OutCurrencyCode))
            {
              frm_message.Show(Resource.String("STR_FRM_AMOUNT_INPUT_MSG_AMOUNT_EXCEED_BANK"), Resource.String("STR_APP_GEN_MSG_WARNING"),
                                               MessageBoxButtons.OK, MessageBoxIcon.Error, m_parent);
              return;
            }

            // Validate min value
            Currency _min_check_amount;
            Decimal min_amount = 0; // From a parameter generaral
            _min_check_amount = Math.Max(min_amount, 0.01m);
            if (_exchange_result.InAmount < _min_check_amount)
            {
              frm_message.Show(Resource.String("STR_FRM_AMOUNT_INPUT_MSG_ZERO_AMOUNT"), Resource.String("STR_APP_GEN_MSG_WARNING"),
                                               MessageBoxButtons.OK, MessageBoxIcon.Error, m_parent);
              return;
            }

            // Devolution
            if (_exchange.ChangeNationalCurrencyToForeignCurrency(Cashier.CashierSessionInfo(), _card_data, _exchange_result, out _voucher))
            {
              VoucherPrint.Print(_voucher);
            }
            else
            {
              frm_message.Show(Resource.String("STR_CURRENCY_EXCHANGE_GENERIC_ERROR_DEVOLUTION"), Resource.String("STR_APP_GEN_MSG_ERROR"),
                 MessageBoxButtons.OK, Images.CashierImage.Error, m_parent);
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        frm_message.Show(Resource.String("STR_CURRENCY_EXCHANGE_GENERIC_ERROR_DEVOLUTION"), Resource.String("STR_APP_GEN_MSG_ERROR"),
           MessageBoxButtons.OK, Images.CashierImage.Error, m_parent);
      }
      finally
      {
        form_yes_no.Hide();
        this.RestoreParentFocus();
      }
    }

    private void btn_undo_last_exchange_currency_Click(object sender, EventArgs e)
    {
      String _str_error;

      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.UndoLastCurrencyExchange,
                                         ProfilePermissions.TypeOperation.RequestPasswd, m_parent, out _str_error))
      {
        return;
      }

      UndoLastCurrencyExchange();
    }

    private void btn_undo_last_devolution_currency_Click(object sender, EventArgs e)
    {
      String _str_error;

      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.UndoLastCurrencyDevolution,
                                         ProfilePermissions.TypeOperation.RequestPasswd, m_parent, out _str_error))
      {
        return;
      }

      UndoLastDevolution();
    }

    private void btn_undo_chips_purchase_Click(object sender, EventArgs e)
    {
      AccountOperations.Operation _account_operation;
      CashierSessionInfo _cashier_session_info;
      String _str_message;
      String _str_error;
      DialogResult _dlg_rc;
      CardData _card_data;
      CurrencyIsoType _iso_type;
      Decimal _iso_amount;
      OperationUndo.UndoError _undo_error;
      ArrayList _voucher_list;

      // Check if current user is an authorized user
      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.UndoLastChipsPurchase,
                                               ProfilePermissions.TypeOperation.RequestPasswd, this.ParentForm, out _str_error))
      {
        return;
      }

      _card_data = account_watcher.Card;
      _cashier_session_info = Cashier.CashierSessionInfo();

      if (m_current_card_id == 0) // Virtual Account
      {
        _card_data = new CardData();
      }

      if (!OperationUndo.GetLastReversibleOperation(OperationCode.CHIPS_PURCHASE, _card_data, _cashier_session_info.CashierSessionId, out _account_operation,
                                                    out _iso_type, out _iso_amount, out _undo_error))
      {
        _str_error = OperationUndo.GetErrorMessage(_undo_error);

        frm_message.Show(_str_error, Resource.String("STR_APP_GEN_MSG_ERROR"),
                         MessageBoxButtons.OK, MessageBoxIcon.Error, this.ParentForm);

        return;
      }

      // Show information message
      // If it is a Gambling Table, show a warning to check the chips stock before to continue
      if (GamingTablesSessions.GamingTableInfo.IsGamingTable)
      {
        _str_message = Resource.String("STR_UNDO_PURCHASE_CHIPS_WARNING", Currency.Format(_iso_amount, _iso_type.IsoCode));
      }
      else
      {
        _str_message = Resource.String("STR_UNDO_PURCHASE_CHIPS", Currency.Format(_iso_amount, _iso_type.IsoCode));
      }
      _str_message = _str_message.Replace("\\r\\n", "\r\n");
      _dlg_rc = frm_message.Show(_str_message,
                                 Resource.String("STR_UNDO_LAST_OPERATION_PURCHASE_CHIPS"),
                                 MessageBoxButtons.YesNo, Images.CashierImage.Question, Cashier.MainForm);

      if (_dlg_rc != DialogResult.OK)
      {
        return;
      }

      account_watcher.RefreshAccount(_card_data);
      _card_data = account_watcher.Card;

      if (_card_data.AccountId == 0) // Virtual Account
      {
        _card_data = new CardData();
        if (!CardData.DB_CardGetAllData(_account_operation.AccountId, _card_data))
        {
          Log.Error("uc_ticket_create: DB_GetCardAllData. Error reading card.");

          return;
        }
      }

      OperationUndo.InputUndoOperation _params = new OperationUndo.InputUndoOperation();
      _params.CodeOperation = OperationCode.CHIPS_PURCHASE;
      _params.CardData = _card_data;
      _params.OperationIdForUndo = _account_operation.OperationId;
      _params.CashierSessionInfo = _cashier_session_info;

      if (!OperationUndo.UndoOperation(_params, out _voucher_list))
      {
        frm_message.Show(Resource.String("STR_UNDO_PURCHASE_CHIPS_ERROR"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this.ParentForm);

        return;
      }

      // Print vouchers
      VoucherPrint.Print(_voucher_list);
    }

    private void btn_undo_chips_sale_with_recharge_Click(object sender, EventArgs e)
    {
      AccountOperations.Operation _account_operation;
      CashierSessionInfo _cashier_session_info;
      String _str_message;
      String _str_error;
      DialogResult _dlg_rc;
      CardData _card_data;
      CurrencyIsoType _iso_type;
      Decimal _iso_amount;
      OperationUndo.UndoError _undo_error;
      ArrayList _voucher_list;
      String _str_national_currency;
      String _str_amount;

      // Check if current user is an authorized user
      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.UndoLastChipsSale,
                                               ProfilePermissions.TypeOperation.RequestPasswd, this.ParentForm, out _str_error))
      {
        return;
      }

      _card_data = account_watcher.Card;
      _cashier_session_info = Cashier.CashierSessionInfo();

      if (m_current_card_id == 0) // Virtual Account
      {
        _card_data = new CardData();
      }

      if (!OperationUndo.GetLastReversibleOperation(OperationCode.CHIPS_SALE_WITH_RECHARGE, _card_data, _cashier_session_info.CashierSessionId, out _account_operation,
                                                    out _iso_type, out _iso_amount, out _undo_error))
      {
        _str_error = OperationUndo.GetErrorMessage(_undo_error);

        frm_message.Show(_str_error, Resource.String("STR_APP_GEN_MSG_ERROR"),
                         MessageBoxButtons.OK, MessageBoxIcon.Error, this.ParentForm);

        return;
      }

      _str_national_currency = CurrencyExchange.GetNationalCurrency();

      // Check if it's national currency
      if (_iso_type.IsoCode == _str_national_currency || String.IsNullOrEmpty(_iso_type.IsoCode))
      {
        _str_amount = ((Currency)_iso_amount).ToString();
      }
      else
      {
        _str_amount = Currency.Format(_iso_amount, _iso_type.IsoCode);
      }

      // Check if type is not Currency
      switch (_iso_type.Type)
      {
        case CurrencyExchangeType.CARD:
          _str_amount += " (" + Resource.String("STR_FRM_AMOUNT_INPUT_CREDIT_CARD_PAYMENT") + ")";
          break;

        case CurrencyExchangeType.CHECK:
          _str_amount += " (" + Resource.String("STR_FRM_AMOUNT_INPUT_CHECK_PAYMENT") + ")";
          break;

        default:
          break;
      }

      // Show information message
      _str_message = Resource.String("STR_UNDO_SALE_CHIPS", _str_amount);
      _str_message = _str_message.Replace("\\r\\n", "\r\n");
      _dlg_rc = frm_message.Show(_str_message,
                                 Resource.String("STR_UNDO_LAST_OPERATION_SALE_CHIPS"),
                                 MessageBoxButtons.YesNo, Images.CashierImage.Question, Cashier.MainForm);

      if (_dlg_rc != DialogResult.OK)
      {
        return;
      }

      account_watcher.RefreshAccount(_card_data);
      _card_data = account_watcher.Card;

      if (m_current_card_id == 0) // Virtual Account
      {
        _card_data = new CardData();
        if (!CardData.DB_CardGetAllData(_account_operation.AccountId, _card_data))
        {
          Log.Error("uc_ticket_create: DB_GetCardAllData. Error reading card.");

          return;
        }
      }

      OperationUndo.InputUndoOperation _params = new OperationUndo.InputUndoOperation();
      _params.CodeOperation = OperationCode.CHIPS_SALE_WITH_RECHARGE;
      _params.CardData = _card_data;
      _params.OperationIdForUndo = _account_operation.OperationId;
      _params.CashierSessionInfo = _cashier_session_info;

      if (!OperationUndo.UndoOperation(_params, out _voucher_list))
      {
        frm_message.Show(Resource.String("STR_UNDO_SALE_CHIPS_ERROR"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this.ParentForm);

        return;
      }

      // Print vouchers
      VoucherPrint.Print(_voucher_list);
    }

    private void btn_promotions_Click(object sender, EventArgs e)
    {
      frm_account_promos _promos;

      if (m_current_card_id == 0)
      {
        return;
      }

      _promos = new frm_account_promos();
      _promos.Show(m_current_card_id, m_parent);
      _promos.Dispose();

      this.RestoreParentFocus();
    }

    private void btn_cash_advance_Click(object sender, EventArgs e)
    {
      CardData _card_data;
      String _error_str;
      CurrencyExchangeResult _exchange_result;
      RechargeInputParameters _input_params;
      Currency _session_balance;
      List<ProfilePermissions.CashierFormFuncionality> _permissions_list;
      PaymentThresholdAuthorization _payment_threshold_authorization;

      _permissions_list = new List<ProfilePermissions.CashierFormFuncionality>();
      _permissions_list.Add(ProfilePermissions.CashierFormFuncionality.CashAdvance);

      _exchange_result = new CurrencyExchangeResult();

      // Check if current user is an authorized user
      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.CashAdvance,
                                                  ProfilePermissions.TypeOperation.RequestPasswd, this.ParentForm, out _error_str))
      {
        return;
      }

      if (Misc.IsMultiCurrencyExchangeEnabled())
      {
        if (!GetCardData(m_current_card_id, out _card_data))
        {
          return;
        }
      }
      else
      {
        _card_data = account_watcher.Card;
      }

      if (m_current_card_id == 0)
      {
        if (!CardData.DB_VirtualCardGetAllData(Cashier.TerminalName, AccountType.ACCOUNT_VIRTUAL_CASHIER, _card_data))
        {
          return;
        }
      }

      try
      {
        form_yes_no.Show(this.ParentForm);

        // Checks if the customer doesn't visit the casino in this working date
        if (Misc.IsMultiCurrencyExchangeEnabled() && !IsCustomerRegisteredInReception(_card_data))
        {
          return;
        }

        using (DB_TRX _db_trx = new DB_TRX())
        {
          _session_balance = CashierBusinessLogic.UpdateSessionBalance(_db_trx.SqlTransaction, Cashier.SessionId, 0);
        }
        if (_session_balance <= 0)
        {
          frm_message.Show(Resource.String("STR_FRM_AMOUNT_INPUT_MSG_AMOUNT_EXCEED_BANK"),
                           Resource.String("STR_APP_GEN_MSG_WARNING"),
                           MessageBoxButtons.OK,
                           MessageBoxIcon.Error,
                           ParentForm);
          return;
        }

        frm_payment_method _frm_payment = new frm_payment_method(this.ParentForm);
        if (!_frm_payment.Show(out _exchange_result, out _payment_threshold_authorization, _card_data))
        {
          return;
        }

        _input_params = new RechargeInputParameters();
        _input_params.AccountId = _card_data.AccountId;
        _input_params.AccountLevel = _card_data.PlayerTracking.CardLevel;
        _input_params.CardPaid = _card_data.CardPaid;
        _input_params.CardPrice = Accounts.CardDepositToPay(_card_data);
        _input_params.ExternalTrackData = _card_data.TrackData;
        _input_params.VoucherAccountInfo = _card_data.VoucherAccountInfo();
        _input_params.IsChipsOperation = false;
        _input_params.ChipSaleRegister = false;

        //JBC 01/10/2014: We need to save Holder Name into bank transaction database.
        if (_exchange_result != null && _exchange_result.BankTransactionData != null)
        {
          _exchange_result.BankTransactionData.AccountHolderName = _card_data.PlayerTracking.HolderName;
        }

        if (!CashierBusinessLogic.DB_CashAdvance(_input_params, _exchange_result, _payment_threshold_authorization, out _error_str))
        {
          frm_message.Show(_error_str,
                           Resource.String("STR_APP_GEN_MSG_WARNING"),
                           MessageBoxButtons.OK,
                           MessageBoxIcon.Error,
                           ParentForm);
        }
      }
      finally
      {
        form_yes_no.Hide();
        this.RestoreParentFocus();
      }

      RefreshAccount();
    }

    private void btn_undo_cash_advance_Click(object sender, EventArgs e)
    {
      if (Misc.IsMultiCurrencyExchangeEnabled())
      {
        UndoCardCash_CashAdvanceList();
      }
      else
      {
        UndoLastCashAdvance();
      }
    }

    #endregion

    private void timer1_Tick(object sender, EventArgs e)
    {
        m_tick_count_tracknumber_read += 1;

      if (m_tick_count_tracknumber_read >= CashierBusinessLogic.DURATION_BUTTONS_ENABLED_AFTER_OPERATION)
      {
        m_tick_count_tracknumber_read = 0;
        m_swipe_card = false;
        ClearCardData();
        InitFocus();

        return;
      }
    }

    private void btn_terminal_transfer_Click(object sender, EventArgs e)
    {
      using (frm_transfer_to_terminal _frm = new frm_transfer_to_terminal())
      {
        _frm.Show(this.ParentForm);
      }

      this.RestoreParentFocus();
    }

    protected override void OnLayout(LayoutEventArgs e)
    {
      base.OnLayout(e);
      this.BackColor = WSI.Cashier.CashierStyle.Colors.m_colors_dic[CashierStyle.Colors.ENUM_COLORS.HEX_E3E7E9];
    }

    public IDScanner.IDScanner IDScanner { get; set; }

    private void pb_user_Click(object sender, EventArgs e)
    {
      /*frm_player_photo _frm_player_photo;

      if (GeneralParam.GetBoolean("Account", "PlayerPicture.AllowZoom", false))
      {
        form_yes_no.Show(m_parent);

        _frm_player_photo = new frm_player_photo();
        _frm_player_photo.PlayerPhoto = pb_user.Image;
        _frm_player_photo.ShowDialog();

        form_yes_no.Hide();
      }*/
      if (GeneralParam.GetBoolean("Account", "PlayerPicture.AllowZoom", false))
      {
        if (m_exist_photo)
        {

          frm_account_photo frm = new frm_account_photo(this.lbl_holder_name.Text, this.pb_user.Image);
          frm.ShowDialog();
        } else 
        {


          CardData _card_data;
          /*_card_data = new CardData();

          if (!CardData.DB_CardGetAllData(TrackNumber, _card_data, false))
          {
            IsValid = false;
          }

          //If not exitst, then create account, else Load the Account
          m_is_new_card = _card_data.IsNew;*/

          String error_str;
          DialogResult _dgr_ac_edit;

          if (m_is_new_card)
          {

            _card_data = new CardData();
            _card_data.TrackData = m_current_track_number;
            account_watcher.RefreshAccount(_card_data);
            ClearCardData();
          }
          else
          {
            ////0 byAccountId, 1 byAccountId+TrackData (Anonymous), 3 byAccountId+TrackData(Anonymous+Personal)
            if (account_watcher.Card.PlayerTracking.CardLevel == 0)
            {
              if (GeneralParam.GetInt32("AntiMoneyLaundering", "GroupByMode", 0) == 1)
              {
                frm_message.Show(Resource.String("STR_UC_CARD_AML_CANT_CUSTOMIZE_ACCOUNT"),
                                 Resource.String("STR_MSG_ANTIMONEYLAUNDERING_WARNING"),
                                 MessageBoxButtons.OK,
                                 MessageBoxIcon.Warning,
                                 this.ParentForm);
                return;
              }
            }
            _card_data = account_watcher.Card;
            CardData.DB_CardGetPersonalData(_card_data.AccountId, _card_data);

            if (!m_must_show_comments)
            {
              // RMS 14-OCT-2013: not new card, check edition permission depending on account type anonymous or personal
              ProfilePermissions.CashierFormFuncionality _permission;

              _permission = (account_watcher.Card.PlayerTracking.CardLevel > 0) ? ProfilePermissions.CashierFormFuncionality.AccountEditPersonal :
                                                                                  ProfilePermissions.CashierFormFuncionality.AccountEditAnonymous;

              if (!ProfilePermissions.CheckPermissionList(_permission, ProfilePermissions.TypeOperation.RequestPasswd, this.ParentForm, 0, out error_str))
              {
                return;
              }
            }
          }
          using (frm_yesno _shader = new frm_yesno())
          {
            using (PlayerEditDetailsCashierView _view = new PlayerEditDetailsCashierView())
            {
              using (PlayerEditDetailsCashierController _controller =
                new PlayerEditDetailsCashierController(_card_data, _view, IDScanner, PlayerEditDetailsCashierView.SCREEN_MODE.DEFAULT))
              {
                _controller.TakePhoto();

                if (_controller.Dialog_Cancel)
                {
                  return;
                }

                if (WindowManager.StartUpMode == WindowManager.StartUpModes.Explorer)
                {
                  _view.Location = new Point(WindowManager.GetFormCenterLocation(_shader).X - (_view.Width / 2),
                    _shader.Location.Y + 2);
                }
                _shader.Opacity = 0.6;
                _shader.Show();
                _dgr_ac_edit = _view.ShowDialog(_shader);
              }
            }
          }

          if (_dgr_ac_edit == DialogResult.OK)
          {
            // AJQ 29-MAY-2013, Reload the data of the "TrackData" being edited
            // Error: use the m_current_track_number that was refreshed on background)
            //        The background refresh has been disabled for tracks that are not associated to any account.
            CardData.DB_CardGetAllData(_card_data.TrackData, _card_data, false);

            // RCI 28-AUG-2014: A new personal card is created, reset the timer.
            /*if (m_is_new_card)
            {
              tick_count_tracknumber_read = 0;
            }*/

            m_is_new_card = _card_data.IsNew;
            m_pending_check_comments = false;
          }

          if (_card_data.AccountId != 0)
          {
            // AJQ 29-MAY-2013, Refresh the data on Screen when there is an account.
            //                  AccountId is 0 when the "Personalizacion" has been cancelled and the card was "new".
            //                  Avoids displaying the message "The card doesn't belong to the site".
            LoadCardByAccountId(_card_data.AccountId);
          }



        }
      }
    }

  }
}