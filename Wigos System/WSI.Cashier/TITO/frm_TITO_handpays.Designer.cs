namespace WSI.Cashier
{
  partial class frm_TITO_handpays
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.btn_undo_last_handpay = new WSI.Cashier.Controls.uc_round_button();
      this.btn_handpay_manual = new WSI.Cashier.Controls.uc_round_button();
      this.gb_search = new WSI.Cashier.Controls.uc_round_panel();
      this.lbl_last_update_paid = new WSI.Cashier.Controls.uc_label();
      this.btn_monitor_paid = new WSI.Cashier.Controls.uc_round_button();
      this.lbl_last_update_pending = new WSI.Cashier.Controls.uc_label();
      this.btn_monitor_pending = new WSI.Cashier.Controls.uc_round_button();
      this.uc_prov_terminals = new WSI.Cashier.uc_terminal_filter();
      this.lbl_terminal = new WSI.Cashier.Controls.uc_label();
      this.btn_ticket_search = new WSI.Cashier.Controls.uc_round_button();
      this.gb_tickets = new WSI.Cashier.Controls.uc_round_panel();
      this.dgv_handpays = new WSI.Cashier.Controls.uc_DataGridView();
      this.btn_hp_selected = new WSI.Cashier.Controls.uc_round_button();
      this.btn_close = new WSI.Cashier.Controls.uc_round_button();
      this.pnl_data.SuspendLayout();
      this.gb_search.SuspendLayout();
      this.gb_tickets.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dgv_handpays)).BeginInit();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.Controls.Add(this.btn_undo_last_handpay);
      this.pnl_data.Controls.Add(this.btn_handpay_manual);
      this.pnl_data.Controls.Add(this.gb_search);
      this.pnl_data.Controls.Add(this.gb_tickets);
      this.pnl_data.Controls.Add(this.btn_hp_selected);
      this.pnl_data.Controls.Add(this.btn_close);
      this.pnl_data.Size = new System.Drawing.Size(1024, 658);
      this.pnl_data.TabIndex = 0;
      // 
      // btn_undo_last_handpay
      // 
      this.btn_undo_last_handpay.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_undo_last_handpay.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(47)))), ((int)(((byte)(50)))));
      this.btn_undo_last_handpay.FlatAppearance.BorderSize = 0;
      this.btn_undo_last_handpay.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_undo_last_handpay.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_undo_last_handpay.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_undo_last_handpay.Image = null;
      this.btn_undo_last_handpay.IsSelected = false;
      this.btn_undo_last_handpay.Location = new System.Drawing.Point(17, 584);
      this.btn_undo_last_handpay.Name = "btn_undo_last_handpay";
      this.btn_undo_last_handpay.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_undo_last_handpay.Size = new System.Drawing.Size(155, 60);
      this.btn_undo_last_handpay.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.TERTIARY;
      this.btn_undo_last_handpay.TabIndex = 2;
      this.btn_undo_last_handpay.Text = "XUNDOLASTHP";
      this.btn_undo_last_handpay.UseVisualStyleBackColor = false;
      this.btn_undo_last_handpay.Click += new System.EventHandler(this.btn_undo_last_handpay_Click);
      // 
      // btn_handpay_manual
      // 
      this.btn_handpay_manual.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_handpay_manual.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_handpay_manual.FlatAppearance.BorderSize = 0;
      this.btn_handpay_manual.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_handpay_manual.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_handpay_manual.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_handpay_manual.Image = null;
      this.btn_handpay_manual.IsSelected = false;
      this.btn_handpay_manual.Location = new System.Drawing.Point(341, 584);
      this.btn_handpay_manual.Name = "btn_handpay_manual";
      this.btn_handpay_manual.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_handpay_manual.Size = new System.Drawing.Size(155, 60);
      this.btn_handpay_manual.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_handpay_manual.TabIndex = 3;
      this.btn_handpay_manual.Text = "XHANDPAYMANUAL";
      this.btn_handpay_manual.UseVisualStyleBackColor = false;
      this.btn_handpay_manual.Click += new System.EventHandler(this.btn_handpay_manual_Click);
      // 
      // gb_search
      // 
      this.gb_search.BackColor = System.Drawing.Color.Transparent;
      this.gb_search.BorderColor = System.Drawing.Color.Empty;
      this.gb_search.Controls.Add(this.lbl_last_update_paid);
      this.gb_search.Controls.Add(this.btn_monitor_paid);
      this.gb_search.Controls.Add(this.lbl_last_update_pending);
      this.gb_search.Controls.Add(this.btn_monitor_pending);
      this.gb_search.Controls.Add(this.uc_prov_terminals);
      this.gb_search.Controls.Add(this.lbl_terminal);
      this.gb_search.Controls.Add(this.btn_ticket_search);
      this.gb_search.CornerRadius = 10;
      this.gb_search.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_search.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gb_search.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_search.HeaderHeight = 35;
      this.gb_search.HeaderSubText = null;
      this.gb_search.HeaderText = null;
      this.gb_search.Location = new System.Drawing.Point(17, 9);
      this.gb_search.Name = "gb_search";
      this.gb_search.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_search.Size = new System.Drawing.Size(988, 282);
      this.gb_search.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.gb_search.TabIndex = 0;
      this.gb_search.Text = "xSource";
      // 
      // lbl_last_update_paid
      // 
      this.lbl_last_update_paid.AutoSize = true;
      this.lbl_last_update_paid.BackColor = System.Drawing.Color.Transparent;
      this.lbl_last_update_paid.Font = new System.Drawing.Font("Montserrat", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_last_update_paid.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_last_update_paid.Location = new System.Drawing.Point(793, 189);
      this.lbl_last_update_paid.Name = "lbl_last_update_paid";
      this.lbl_last_update_paid.Size = new System.Drawing.Size(86, 15);
      this.lbl_last_update_paid.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_last_update_paid.TabIndex = 4;
      this.lbl_last_update_paid.Text = "xLast Update";
      // 
      // btn_monitor_paid
      // 
      this.btn_monitor_paid.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_monitor_paid.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_monitor_paid.FlatAppearance.BorderSize = 0;
      this.btn_monitor_paid.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_monitor_paid.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_monitor_paid.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_monitor_paid.Image = null;
      this.btn_monitor_paid.IsSelected = false;
      this.btn_monitor_paid.Location = new System.Drawing.Point(771, 136);
      this.btn_monitor_paid.Name = "btn_monitor_paid";
      this.btn_monitor_paid.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_monitor_paid.Size = new System.Drawing.Size(205, 50);
      this.btn_monitor_paid.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_monitor_paid.TabIndex = 3;
      this.btn_monitor_paid.Text = "XPAID MONITOR";
      this.btn_monitor_paid.UseVisualStyleBackColor = false;
      this.btn_monitor_paid.Click += new System.EventHandler(this.btn_monitor_paid_Click);
      // 
      // lbl_last_update_pending
      // 
      this.lbl_last_update_pending.AutoSize = true;
      this.lbl_last_update_pending.BackColor = System.Drawing.Color.Transparent;
      this.lbl_last_update_pending.Font = new System.Drawing.Font("Montserrat", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_last_update_pending.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_last_update_pending.Location = new System.Drawing.Point(792, 99);
      this.lbl_last_update_pending.Name = "lbl_last_update_pending";
      this.lbl_last_update_pending.Size = new System.Drawing.Size(86, 15);
      this.lbl_last_update_pending.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_last_update_pending.TabIndex = 2;
      this.lbl_last_update_pending.Text = "xLast Update";
      // 
      // btn_monitor_pending
      // 
      this.btn_monitor_pending.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_monitor_pending.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_monitor_pending.FlatAppearance.BorderSize = 0;
      this.btn_monitor_pending.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_monitor_pending.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_monitor_pending.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_monitor_pending.Image = null;
      this.btn_monitor_pending.IsSelected = false;
      this.btn_monitor_pending.Location = new System.Drawing.Point(771, 46);
      this.btn_monitor_pending.Name = "btn_monitor_pending";
      this.btn_monitor_pending.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_monitor_pending.Size = new System.Drawing.Size(205, 50);
      this.btn_monitor_pending.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_monitor_pending.TabIndex = 1;
      this.btn_monitor_pending.Text = "XPENDING MONITOR";
      this.btn_monitor_pending.UseVisualStyleBackColor = false;
      this.btn_monitor_pending.Click += new System.EventHandler(this.btn_monitor_Click);
      // 
      // uc_prov_terminals
      // 
      this.uc_prov_terminals.BackColor = System.Drawing.Color.Transparent;
      this.uc_prov_terminals.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(2)));
      this.uc_prov_terminals.Location = new System.Drawing.Point(-1, 35);
      this.uc_prov_terminals.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
      this.uc_prov_terminals.Name = "uc_prov_terminals";
      this.uc_prov_terminals.Size = new System.Drawing.Size(760, 247);
      this.uc_prov_terminals.TabIndex = 0;
      // 
      // lbl_terminal
      // 
      this.lbl_terminal.AutoSize = true;
      this.lbl_terminal.BackColor = System.Drawing.Color.Transparent;
      this.lbl_terminal.Font = new System.Drawing.Font("Montserrat", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_terminal.Location = new System.Drawing.Point(3, 37);
      this.lbl_terminal.Name = "lbl_terminal";
      this.lbl_terminal.Size = new System.Drawing.Size(48, 11);
      this.lbl_terminal.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.lbl_terminal.TabIndex = 103;
      this.lbl_terminal.Text = "xTerminal";
      this.lbl_terminal.TextAlign = System.Drawing.ContentAlignment.TopRight;
      // 
      // btn_ticket_search
      // 
      this.btn_ticket_search.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_ticket_search.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_ticket_search.FlatAppearance.BorderSize = 0;
      this.btn_ticket_search.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_ticket_search.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_ticket_search.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_ticket_search.Image = null;
      this.btn_ticket_search.IsSelected = false;
      this.btn_ticket_search.Location = new System.Drawing.Point(771, 226);
      this.btn_ticket_search.Name = "btn_ticket_search";
      this.btn_ticket_search.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_ticket_search.Size = new System.Drawing.Size(205, 50);
      this.btn_ticket_search.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_ticket_search.TabIndex = 5;
      this.btn_ticket_search.Text = "XSEARCH";
      this.btn_ticket_search.UseVisualStyleBackColor = false;
      this.btn_ticket_search.Click += new System.EventHandler(this.btn_ticket_search_Click);
      // 
      // gb_tickets
      // 
      this.gb_tickets.BackColor = System.Drawing.Color.Transparent;
      this.gb_tickets.BorderColor = System.Drawing.Color.Empty;
      this.gb_tickets.Controls.Add(this.dgv_handpays);
      this.gb_tickets.CornerRadius = 10;
      this.gb_tickets.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_tickets.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gb_tickets.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_tickets.HeaderHeight = 35;
      this.gb_tickets.HeaderSubText = null;
      this.gb_tickets.HeaderText = null;
      this.gb_tickets.Location = new System.Drawing.Point(17, 299);
      this.gb_tickets.Name = "gb_tickets";
      this.gb_tickets.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_tickets.Size = new System.Drawing.Size(988, 277);
      this.gb_tickets.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.gb_tickets.TabIndex = 1;
      this.gb_tickets.Text = "xTickets";
      // 
      // dgv_handpays
      // 
      this.dgv_handpays.AllowUserToAddRows = false;
      this.dgv_handpays.AllowUserToDeleteRows = false;
      this.dgv_handpays.AllowUserToResizeColumns = false;
      this.dgv_handpays.AllowUserToResizeRows = false;
      this.dgv_handpays.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.dgv_handpays.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.dgv_handpays.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.dgv_handpays.ColumnHeaderHeight = 35;
      this.dgv_handpays.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
      this.dgv_handpays.ColumnHeadersHeight = 35;
      this.dgv_handpays.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
      this.dgv_handpays.CornerRadius = 10;
      this.dgv_handpays.DefaultCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.dgv_handpays.DefaultCellForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.dgv_handpays.EnableHeadersVisualStyles = false;
      this.dgv_handpays.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.dgv_handpays.GridColor = System.Drawing.Color.LightGray;
      this.dgv_handpays.HeaderBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
      this.dgv_handpays.HeaderDefaultCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.dgv_handpays.HeaderDefaultCellForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.dgv_handpays.Location = new System.Drawing.Point(0, 35);
      this.dgv_handpays.MultiSelect = false;
      this.dgv_handpays.Name = "dgv_handpays";
      this.dgv_handpays.ReadOnly = true;
      this.dgv_handpays.RowHeadersVisible = false;
      this.dgv_handpays.RowHeadersWidth = 20;
      this.dgv_handpays.RowTemplate.Height = 35;
      this.dgv_handpays.RowTemplateHeight = 35;
      this.dgv_handpays.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
      this.dgv_handpays.Size = new System.Drawing.Size(988, 242);
      this.dgv_handpays.Style = WSI.Cashier.Controls.uc_DataGridView.DataGridViewStyle.WITH_BOTTOM_ROUND_BORDERS;
      this.dgv_handpays.TabIndex = 0;
      this.dgv_handpays.Click += new System.EventHandler(this.dgv_handpays_Click);
      // 
      // btn_hp_selected
      // 
      this.btn_hp_selected.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_hp_selected.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_hp_selected.FlatAppearance.BorderSize = 0;
      this.btn_hp_selected.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_hp_selected.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_hp_selected.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_hp_selected.Image = null;
      this.btn_hp_selected.IsSelected = false;
      this.btn_hp_selected.Location = new System.Drawing.Point(524, 584);
      this.btn_hp_selected.Name = "btn_hp_selected";
      this.btn_hp_selected.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_hp_selected.Size = new System.Drawing.Size(155, 60);
      this.btn_hp_selected.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_hp_selected.TabIndex = 4;
      this.btn_hp_selected.Text = "XHPSELECTED";
      this.btn_hp_selected.UseVisualStyleBackColor = false;
      this.btn_hp_selected.Click += new System.EventHandler(this.btn_hp_selected_Click);
      // 
      // btn_close
      // 
      this.btn_close.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_close.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_close.FlatAppearance.BorderSize = 0;
      this.btn_close.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_close.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_close.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_close.Image = null;
      this.btn_close.IsSelected = false;
      this.btn_close.Location = new System.Drawing.Point(850, 584);
      this.btn_close.Name = "btn_close";
      this.btn_close.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_close.Size = new System.Drawing.Size(155, 60);
      this.btn_close.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_close.TabIndex = 5;
      this.btn_close.Text = "XCLOSE/CANCEL";
      this.btn_close.UseVisualStyleBackColor = false;
      this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
      // 
      // frm_TITO_handpays
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(1024, 713);
      this.Name = "frm_TITO_handpays";
      this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
      this.Style = WSI.Cashier.Controls.frm_base.BaseFormStyle.PRINCIPAL;
      this.Text = "TITO Handpays";
      this.pnl_data.ResumeLayout(false);
      this.gb_search.ResumeLayout(false);
      this.gb_search.PerformLayout();
      this.gb_tickets.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.dgv_handpays)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion



    private WSI.Cashier.Controls.uc_round_button btn_hp_selected;
    private WSI.Cashier.Controls.uc_round_button btn_close;
    private WSI.Cashier.Controls.uc_round_button btn_handpay_manual;
    private WSI.Cashier.Controls.uc_round_panel gb_search;
    private uc_terminal_filter uc_prov_terminals;    
    private WSI.Cashier.Controls.uc_label lbl_terminal;
    private WSI.Cashier.Controls.uc_round_button btn_ticket_search;
    private WSI.Cashier.Controls.uc_round_panel gb_tickets;
    private WSI.Cashier.Controls.uc_DataGridView dgv_handpays;
    private WSI.Cashier.Controls.uc_round_button btn_monitor_pending;
    private WSI.Cashier.Controls.uc_label lbl_last_update_pending;
    private WSI.Cashier.Controls.uc_round_button btn_undo_last_handpay;
    private WSI.Cashier.Controls.uc_label lbl_last_update_paid;
    private WSI.Cashier.Controls.uc_round_button btn_monitor_paid;

  }
}