namespace WSI.Cashier
{
  partial class frm_browser
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.splitContainer1 = new System.Windows.Forms.SplitContainer();
      this.lbl_browser_title = new System.Windows.Forms.Label();
      this.btn_close = new System.Windows.Forms.Button();
      this.btn_print = new System.Windows.Forms.Button();
      this.web_browser = new System.Windows.Forms.WebBrowser();
      this.splitContainer1.Panel1.SuspendLayout();
      this.splitContainer1.Panel2.SuspendLayout();
      this.splitContainer1.SuspendLayout();
      this.SuspendLayout();
      // 
      // splitContainer1
      // 
      this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
      this.splitContainer1.IsSplitterFixed = true;
      this.splitContainer1.Location = new System.Drawing.Point(0, 0);
      this.splitContainer1.Name = "splitContainer1";
      this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
      // 
      // splitContainer1.Panel1
      // 
      this.splitContainer1.Panel1.BackColor = System.Drawing.Color.CornflowerBlue;
      this.splitContainer1.Panel1.Controls.Add(this.lbl_browser_title);
      // 
      // splitContainer1.Panel2
      // 
      this.splitContainer1.Panel2.Controls.Add(this.btn_close);
      this.splitContainer1.Panel2.Controls.Add(this.btn_print);
      this.splitContainer1.Panel2.Controls.Add(this.web_browser);
      this.splitContainer1.Size = new System.Drawing.Size(344, 620);
      this.splitContainer1.SplitterDistance = 25;
      this.splitContainer1.TabIndex = 12;
      // 
      // lbl_browser_title
      // 
      this.lbl_browser_title.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_browser_title.ForeColor = System.Drawing.Color.White;
      this.lbl_browser_title.Location = new System.Drawing.Point(3, 5);
      this.lbl_browser_title.Name = "lbl_browser_title";
      this.lbl_browser_title.Size = new System.Drawing.Size(338, 16);
      this.lbl_browser_title.TabIndex = 3;
      this.lbl_browser_title.Text = "xNone";
      // 
      // btn_close
      // 
      this.btn_close.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.btn_close.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btn_close.Location = new System.Drawing.Point(183, 529);
      this.btn_close.Name = "btn_close";
      this.btn_close.Size = new System.Drawing.Size(112, 48);
      this.btn_close.TabIndex = 14;
      this.btn_close.Text = "xClose";
      this.btn_close.UseVisualStyleBackColor = false;
      this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
      // 
      // btn_print
      // 
      this.btn_print.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.btn_print.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btn_print.Location = new System.Drawing.Point(37, 529);
      this.btn_print.Name = "btn_print";
      this.btn_print.Size = new System.Drawing.Size(112, 48);
      this.btn_print.TabIndex = 13;
      this.btn_print.Text = "xPrint";
      this.btn_print.UseVisualStyleBackColor = false;
      this.btn_print.Click += new System.EventHandler(this.btn_reprint_Click);
      // 
      // web_browser
      // 
      this.web_browser.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                  | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.web_browser.Location = new System.Drawing.Point(12, 3);
      this.web_browser.MinimumSize = new System.Drawing.Size(20, 20);
      this.web_browser.Name = "web_browser";
      this.web_browser.Size = new System.Drawing.Size(320, 520);
      this.web_browser.TabIndex = 12;
      // 
      // frm_browser
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(344, 620);
      this.Controls.Add(this.splitContainer1);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
      this.Name = "frm_browser";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Voucher";
      this.splitContainer1.Panel1.ResumeLayout(false);
      this.splitContainer1.Panel2.ResumeLayout(false);
      this.splitContainer1.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.SplitContainer splitContainer1;
    private System.Windows.Forms.Button btn_close;
    private System.Windows.Forms.Button btn_print;
    public System.Windows.Forms.WebBrowser web_browser;
    private System.Windows.Forms.Label lbl_browser_title;

  }
}