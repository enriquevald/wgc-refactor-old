namespace WSI.Cashier
{
  partial class frm_amount_input
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_amount_input));
      this.btn_cancel_request = new WSI.Cashier.Controls.uc_round_button();
      this.btn_clear = new WSI.Cashier.Controls.uc_round_button();
      this.gp_digits_box = new WSI.Cashier.Controls.uc_round_panel();
      this.lbl_currency = new WSI.Cashier.Controls.uc_label();
      this.btn_money_4 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_money_3 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_money_2 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_money_1 = new WSI.Cashier.Controls.uc_round_button();
      this.txt_amount = new WSI.Cashier.Controls.uc_round_textbox();
      this.btn_num_1 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_back = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_2 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_intro = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_3 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_4 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_10 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_5 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_dot = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_6 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_9 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_7 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_8 = new WSI.Cashier.Controls.uc_round_button();
      this.promo_filter = new WSI.Cashier.uc_promo_filter();
      this.gb_common = new WSI.Cashier.Controls.uc_round_panel();
      this.btn_promos_change_view = new WSI.Cashier.Controls.uc_round_button();
      this.lbl_cage_currency_total = new WSI.Cashier.Controls.uc_label();
      this.dgv_common = new WSI.Cashier.Controls.uc_DataGridView();
      this.lbl_cage_total_value = new WSI.Cashier.Controls.uc_label();
      this.btn_cancel_by_cashier = new WSI.Cashier.Controls.uc_round_button();
      this.lbl_cage_total_text = new WSI.Cashier.Controls.uc_label();
      this.btn_show_hide_rows = new WSI.Cashier.Controls.uc_round_button();
      this.lbl_promo_parameters = new WSI.Cashier.Controls.uc_label();
      this.lbl_promo_input_values = new WSI.Cashier.Controls.uc_label();
      this.lbl_promo_reward_value = new WSI.Cashier.Controls.uc_label();
      this.lbl_promo_reward_text = new WSI.Cashier.Controls.uc_label();
      this.pnl_buttons = new System.Windows.Forms.Panel();
      this.btn_cancel = new WSI.Cashier.Controls.uc_round_button();
      this.btn_ok = new WSI.Cashier.Controls.uc_round_button();
      this.btn_check_redeem = new WSI.Cashier.Controls.uc_round_button();
      this.lbl_currency_to_pay = new WSI.Cashier.Controls.uc_label();
      this.lbl_total_to_pay = new WSI.Cashier.Controls.uc_label();
      this.gb_tokens = new WSI.Cashier.Controls.uc_round_panel();
      this.btn_enter_tokens = new WSI.Cashier.Controls.uc_round_button();
      this.lbl_entered_tokens = new WSI.Cashier.Controls.uc_label();
      this.btn_enter_flyer = new WSI.Cashier.Controls.uc_round_button();
      this.grp_cash_type = new System.Windows.Forms.GroupBox();
      this.lbl_cash_type = new WSI.Cashier.Controls.uc_label();
      this.pb_current_type = new System.Windows.Forms.PictureBox();
      this.web_browser = new System.Windows.Forms.WebBrowser();
      this.btn_apply_tax = new WSI.Cashier.Controls.uc_round_button();
      this.gb_total = new WSI.Cashier.Controls.uc_round_panel();
      this.gb_promotion_info = new WSI.Cashier.Controls.uc_round_panel();
      this.uc_payment_method1 = new WSI.Cashier.uc_payment_method();
      this.pnl_data.SuspendLayout();
      this.gp_digits_box.SuspendLayout();
      this.gb_common.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dgv_common)).BeginInit();
      this.pnl_buttons.SuspendLayout();
      this.gb_tokens.SuspendLayout();
      this.grp_cash_type.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pb_current_type)).BeginInit();
      this.gb_total.SuspendLayout();
      this.gb_promotion_info.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.Controls.Add(this.btn_show_hide_rows);
      this.pnl_data.Controls.Add(this.gb_promotion_info);
      this.pnl_data.Controls.Add(this.gb_common);
      this.pnl_data.Controls.Add(this.btn_check_redeem);
      this.pnl_data.Controls.Add(this.pnl_buttons);
      this.pnl_data.Controls.Add(this.btn_cancel_request);
      this.pnl_data.Controls.Add(this.btn_clear);
      this.pnl_data.Controls.Add(this.lbl_currency_to_pay);
      this.pnl_data.Controls.Add(this.gb_total);
      this.pnl_data.Controls.Add(this.web_browser);
      this.pnl_data.Controls.Add(this.gp_digits_box);
      this.pnl_data.Controls.Add(this.gb_tokens);
      this.pnl_data.Controls.Add(this.grp_cash_type);
      this.pnl_data.Controls.Add(this.uc_payment_method1);
      this.pnl_data.Size = new System.Drawing.Size(1024, 658);
      this.pnl_data.TabIndex = 0;
      // 
      // btn_cancel_request
      // 
      this.btn_cancel_request.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.btn_cancel_request.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(47)))), ((int)(((byte)(50)))));
      this.btn_cancel_request.Enabled = false;
      this.btn_cancel_request.FlatAppearance.BorderSize = 0;
      this.btn_cancel_request.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_cancel_request.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_cancel_request.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_cancel_request.Image = null;
      this.btn_cancel_request.IsSelected = false;
      this.btn_cancel_request.Location = new System.Drawing.Point(134, 542);
      this.btn_cancel_request.Name = "btn_cancel_request";
      this.btn_cancel_request.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_cancel_request.Size = new System.Drawing.Size(155, 60);
      this.btn_cancel_request.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.TERTIARY;
      this.btn_cancel_request.TabIndex = 2;
      this.btn_cancel_request.Text = "XCANCELREQUEST";
      this.btn_cancel_request.UseVisualStyleBackColor = false;
      this.btn_cancel_request.Visible = false;
      this.btn_cancel_request.Click += new System.EventHandler(this.btn_cancel_request_Click);
      // 
      // btn_clear
      // 
      this.btn_clear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.btn_clear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_clear.FlatAppearance.BorderSize = 0;
      this.btn_clear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_clear.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_clear.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_clear.Image = null;
      this.btn_clear.IsSelected = false;
      this.btn_clear.Location = new System.Drawing.Point(14, 542);
      this.btn_clear.Name = "btn_clear";
      this.btn_clear.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_clear.Size = new System.Drawing.Size(155, 60);
      this.btn_clear.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_clear.TabIndex = 1;
      this.btn_clear.Text = "XCLEAR";
      this.btn_clear.UseVisualStyleBackColor = false;
      this.btn_clear.Visible = false;
      this.btn_clear.Click += new System.EventHandler(this.btn_clear_Click);
      // 
      // gp_digits_box
      // 
      this.gp_digits_box.BackColor = System.Drawing.Color.Transparent;
      this.gp_digits_box.BorderColor = System.Drawing.Color.Empty;
      this.gp_digits_box.Controls.Add(this.lbl_currency);
      this.gp_digits_box.Controls.Add(this.btn_money_4);
      this.gp_digits_box.Controls.Add(this.btn_money_3);
      this.gp_digits_box.Controls.Add(this.btn_money_2);
      this.gp_digits_box.Controls.Add(this.btn_money_1);
      this.gp_digits_box.Controls.Add(this.txt_amount);
      this.gp_digits_box.Controls.Add(this.btn_num_1);
      this.gp_digits_box.Controls.Add(this.btn_back);
      this.gp_digits_box.Controls.Add(this.btn_num_2);
      this.gp_digits_box.Controls.Add(this.btn_intro);
      this.gp_digits_box.Controls.Add(this.btn_num_3);
      this.gp_digits_box.Controls.Add(this.btn_num_4);
      this.gp_digits_box.Controls.Add(this.btn_num_10);
      this.gp_digits_box.Controls.Add(this.btn_num_5);
      this.gp_digits_box.Controls.Add(this.btn_num_dot);
      this.gp_digits_box.Controls.Add(this.btn_num_6);
      this.gp_digits_box.Controls.Add(this.btn_num_9);
      this.gp_digits_box.Controls.Add(this.btn_num_7);
      this.gp_digits_box.Controls.Add(this.btn_num_8);
      this.gp_digits_box.CornerRadius = 10;
      this.gp_digits_box.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gp_digits_box.ForeColor = System.Drawing.Color.Black;
      this.gp_digits_box.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.gp_digits_box.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gp_digits_box.HeaderHeight = 55;
      this.gp_digits_box.HeaderSubText = null;
      this.gp_digits_box.HeaderText = null;
      this.gp_digits_box.Location = new System.Drawing.Point(302, 137);
      this.gp_digits_box.Name = "gp_digits_box";
      this.gp_digits_box.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gp_digits_box.Size = new System.Drawing.Size(380, 378);
      this.gp_digits_box.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NONE;
      this.gp_digits_box.TabIndex = 4;
      this.gp_digits_box.Text = "xInput Amount";
      // 
      // lbl_currency
      // 
      this.lbl_currency.BackColor = System.Drawing.Color.Transparent;
      this.lbl_currency.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_currency.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_currency.Location = new System.Drawing.Point(1, 53);
      this.lbl_currency.Name = "lbl_currency";
      this.lbl_currency.Size = new System.Drawing.Size(231, 18);
      this.lbl_currency.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_currency.TabIndex = 24;
      this.lbl_currency.Text = "xMsgCurrency";
      this.lbl_currency.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // btn_money_4
      // 
      this.btn_money_4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_money_4.CornerRadius = 0;
      this.btn_money_4.FlatAppearance.BorderSize = 0;
      this.btn_money_4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_money_4.Font = new System.Drawing.Font("Open Sans Semibold", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_money_4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_money_4.Image = null;
      this.btn_money_4.IsSelected = false;
      this.btn_money_4.Location = new System.Drawing.Point(286, 85);
      this.btn_money_4.Name = "btn_money_4";
      this.btn_money_4.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_money_4.SelectedColor = System.Drawing.Color.Empty;
      this.btn_money_4.Size = new System.Drawing.Size(90, 60);
      this.btn_money_4.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_MONEY;
      this.btn_money_4.TabIndex = 19;
      this.btn_money_4.TabStop = false;
      this.btn_money_4.Text = "2000 $";
      this.btn_money_4.UseVisualStyleBackColor = false;
      this.btn_money_4.Click += new System.EventHandler(this.btn_money_4_Click);
      this.btn_money_4.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_amount_input_KeyPress);
      this.btn_money_4.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_amount_input_PreviewKeyDown);
      // 
      // btn_money_3
      // 
      this.btn_money_3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_money_3.CornerRadius = 0;
      this.btn_money_3.FlatAppearance.BorderSize = 0;
      this.btn_money_3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_money_3.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_money_3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_money_3.Image = null;
      this.btn_money_3.IsSelected = false;
      this.btn_money_3.Location = new System.Drawing.Point(192, 85);
      this.btn_money_3.Name = "btn_money_3";
      this.btn_money_3.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_money_3.SelectedColor = System.Drawing.Color.Empty;
      this.btn_money_3.Size = new System.Drawing.Size(90, 60);
      this.btn_money_3.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_MONEY;
      this.btn_money_3.TabIndex = 18;
      this.btn_money_3.TabStop = false;
      this.btn_money_3.Text = "100";
      this.btn_money_3.UseVisualStyleBackColor = false;
      this.btn_money_3.Click += new System.EventHandler(this.btn_money_3_Click);
      this.btn_money_3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_amount_input_KeyPress);
      this.btn_money_3.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_amount_input_PreviewKeyDown);
      // 
      // btn_money_2
      // 
      this.btn_money_2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_money_2.CornerRadius = 0;
      this.btn_money_2.FlatAppearance.BorderSize = 0;
      this.btn_money_2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_money_2.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_money_2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_money_2.Image = null;
      this.btn_money_2.IsSelected = false;
      this.btn_money_2.Location = new System.Drawing.Point(98, 85);
      this.btn_money_2.Name = "btn_money_2";
      this.btn_money_2.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_money_2.SelectedColor = System.Drawing.Color.Empty;
      this.btn_money_2.Size = new System.Drawing.Size(90, 60);
      this.btn_money_2.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_MONEY;
      this.btn_money_2.TabIndex = 17;
      this.btn_money_2.TabStop = false;
      this.btn_money_2.Text = "50";
      this.btn_money_2.UseVisualStyleBackColor = false;
      this.btn_money_2.Click += new System.EventHandler(this.btn_money_2_Click);
      this.btn_money_2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_amount_input_KeyPress);
      this.btn_money_2.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_amount_input_PreviewKeyDown);
      // 
      // btn_money_1
      // 
      this.btn_money_1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_money_1.CornerRadius = 0;
      this.btn_money_1.FlatAppearance.BorderSize = 0;
      this.btn_money_1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_money_1.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_money_1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_money_1.Image = null;
      this.btn_money_1.IsSelected = false;
      this.btn_money_1.Location = new System.Drawing.Point(4, 85);
      this.btn_money_1.Name = "btn_money_1";
      this.btn_money_1.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_money_1.SelectedColor = System.Drawing.Color.Empty;
      this.btn_money_1.Size = new System.Drawing.Size(90, 60);
      this.btn_money_1.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_MONEY;
      this.btn_money_1.TabIndex = 16;
      this.btn_money_1.TabStop = false;
      this.btn_money_1.Text = "10";
      this.btn_money_1.UseVisualStyleBackColor = false;
      this.btn_money_1.Click += new System.EventHandler(this.btn_money_1_Click);
      this.btn_money_1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_amount_input_KeyPress);
      this.btn_money_1.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_amount_input_PreviewKeyDown);
      // 
      // txt_amount
      // 
      this.txt_amount.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_amount.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_amount.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.txt_amount.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.txt_amount.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_amount.CornerRadius = 0;
      this.txt_amount.Font = new System.Drawing.Font("Open Sans Semibold", 40F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_amount.Location = new System.Drawing.Point(3, 5);
      this.txt_amount.MaxLength = 18;
      this.txt_amount.Multiline = false;
      this.txt_amount.Name = "txt_amount";
      this.txt_amount.PasswordChar = '\0';
      this.txt_amount.ReadOnly = true;
      this.txt_amount.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_amount.SelectedText = "";
      this.txt_amount.SelectionLength = 0;
      this.txt_amount.SelectionStart = 0;
      this.txt_amount.Size = new System.Drawing.Size(374, 45);
      this.txt_amount.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.CALCULATOR;
      this.txt_amount.TabIndex = 14;
      this.txt_amount.TabStop = false;
      this.txt_amount.Text = "123";
      this.txt_amount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      this.txt_amount.UseSystemPasswordChar = false;
      this.txt_amount.WaterMark = null;
      this.txt_amount.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(153)))), ((int)(((byte)(49)))));
      this.txt_amount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_amount_input_KeyPress);
      this.txt_amount.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_amount_input_PreviewKeyDown);
      // 
      // btn_num_1
      // 
      this.btn_num_1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_1.CornerRadius = 0;
      this.btn_num_1.FlatAppearance.BorderSize = 0;
      this.btn_num_1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_1.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_1.Image = null;
      this.btn_num_1.IsSelected = false;
      this.btn_num_1.Location = new System.Drawing.Point(4, 150);
      this.btn_num_1.Name = "btn_num_1";
      this.btn_num_1.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_1.SelectedColor = System.Drawing.Color.Empty;
      this.btn_num_1.Size = new System.Drawing.Size(70, 50);
      this.btn_num_1.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_1.TabIndex = 0;
      this.btn_num_1.TabStop = false;
      this.btn_num_1.Text = "1";
      this.btn_num_1.UseVisualStyleBackColor = false;
      this.btn_num_1.Click += new System.EventHandler(this.btn_num_1_Click);
      this.btn_num_1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_amount_input_KeyPress);
      this.btn_num_1.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_amount_input_PreviewKeyDown);
      // 
      // btn_back
      // 
      this.btn_back.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_back.CornerRadius = 0;
      this.btn_back.FlatAppearance.BorderSize = 0;
      this.btn_back.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_back.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_back.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_back.Image = ((System.Drawing.Image)(resources.GetObject("btn_back.Image")));
      this.btn_back.IsSelected = false;
      this.btn_back.Location = new System.Drawing.Point(235, 150);
      this.btn_back.Name = "btn_back";
      this.btn_back.SelectedColor = System.Drawing.Color.Empty;
      this.btn_back.Size = new System.Drawing.Size(141, 50);
      this.btn_back.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_BACKSPACE;
      this.btn_back.TabIndex = 13;
      this.btn_back.TabStop = false;
      this.btn_back.UseVisualStyleBackColor = false;
      this.btn_back.Click += new System.EventHandler(this.btn_back_Click);
      this.btn_back.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_amount_input_KeyPress);
      this.btn_back.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_amount_input_PreviewKeyDown);
      // 
      // btn_num_2
      // 
      this.btn_num_2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_2.CornerRadius = 0;
      this.btn_num_2.FlatAppearance.BorderSize = 0;
      this.btn_num_2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_2.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_2.Image = null;
      this.btn_num_2.IsSelected = false;
      this.btn_num_2.Location = new System.Drawing.Point(81, 150);
      this.btn_num_2.Name = "btn_num_2";
      this.btn_num_2.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_2.SelectedColor = System.Drawing.Color.Empty;
      this.btn_num_2.Size = new System.Drawing.Size(70, 50);
      this.btn_num_2.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_2.TabIndex = 1;
      this.btn_num_2.TabStop = false;
      this.btn_num_2.Text = "2";
      this.btn_num_2.UseVisualStyleBackColor = false;
      this.btn_num_2.Click += new System.EventHandler(this.btn_num_2_Click);
      this.btn_num_2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_amount_input_KeyPress);
      this.btn_num_2.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_amount_input_PreviewKeyDown);
      // 
      // btn_intro
      // 
      this.btn_intro.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(153)))), ((int)(((byte)(49)))));
      this.btn_intro.CornerRadius = 0;
      this.btn_intro.FlatAppearance.BorderSize = 0;
      this.btn_intro.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_intro.Font = new System.Drawing.Font("Open Sans Semibold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_intro.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_intro.Image = ((System.Drawing.Image)(resources.GetObject("btn_intro.Image")));
      this.btn_intro.IsSelected = false;
      this.btn_intro.Location = new System.Drawing.Point(235, 205);
      this.btn_intro.Name = "btn_intro";
      this.btn_intro.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_intro.SelectedColor = System.Drawing.Color.Empty;
      this.btn_intro.Size = new System.Drawing.Size(141, 160);
      this.btn_intro.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_ENTER;
      this.btn_intro.TabIndex = 12;
      this.btn_intro.UseVisualStyleBackColor = false;
      this.btn_intro.Click += new System.EventHandler(this.btn_intro_Click);
      this.btn_intro.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_amount_input_KeyPress);
      this.btn_intro.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_amount_input_PreviewKeyDown);
      // 
      // btn_num_3
      // 
      this.btn_num_3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_3.CornerRadius = 0;
      this.btn_num_3.FlatAppearance.BorderSize = 0;
      this.btn_num_3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_3.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_3.Image = null;
      this.btn_num_3.IsSelected = false;
      this.btn_num_3.Location = new System.Drawing.Point(158, 150);
      this.btn_num_3.Name = "btn_num_3";
      this.btn_num_3.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_3.SelectedColor = System.Drawing.Color.Empty;
      this.btn_num_3.Size = new System.Drawing.Size(70, 50);
      this.btn_num_3.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_3.TabIndex = 2;
      this.btn_num_3.TabStop = false;
      this.btn_num_3.Text = "3";
      this.btn_num_3.UseVisualStyleBackColor = false;
      this.btn_num_3.Click += new System.EventHandler(this.btn_num_3_Click);
      this.btn_num_3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_amount_input_KeyPress);
      this.btn_num_3.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_amount_input_PreviewKeyDown);
      // 
      // btn_num_4
      // 
      this.btn_num_4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_4.CornerRadius = 0;
      this.btn_num_4.FlatAppearance.BorderSize = 0;
      this.btn_num_4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_4.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_4.Image = null;
      this.btn_num_4.IsSelected = false;
      this.btn_num_4.Location = new System.Drawing.Point(4, 205);
      this.btn_num_4.Name = "btn_num_4";
      this.btn_num_4.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_4.SelectedColor = System.Drawing.Color.Empty;
      this.btn_num_4.Size = new System.Drawing.Size(70, 50);
      this.btn_num_4.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_4.TabIndex = 3;
      this.btn_num_4.TabStop = false;
      this.btn_num_4.Text = "4";
      this.btn_num_4.UseVisualStyleBackColor = false;
      this.btn_num_4.Click += new System.EventHandler(this.btn_num_4_Click);
      this.btn_num_4.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_amount_input_KeyPress);
      this.btn_num_4.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_amount_input_PreviewKeyDown);
      // 
      // btn_num_10
      // 
      this.btn_num_10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_10.CornerRadius = 0;
      this.btn_num_10.FlatAppearance.BorderSize = 0;
      this.btn_num_10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_10.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_10.Image = null;
      this.btn_num_10.IsSelected = false;
      this.btn_num_10.Location = new System.Drawing.Point(4, 315);
      this.btn_num_10.Name = "btn_num_10";
      this.btn_num_10.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_10.SelectedColor = System.Drawing.Color.Empty;
      this.btn_num_10.Size = new System.Drawing.Size(147, 50);
      this.btn_num_10.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_10.TabIndex = 10;
      this.btn_num_10.TabStop = false;
      this.btn_num_10.Text = "0";
      this.btn_num_10.UseVisualStyleBackColor = false;
      this.btn_num_10.Click += new System.EventHandler(this.btn_num_10_Click);
      this.btn_num_10.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_amount_input_KeyPress);
      this.btn_num_10.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_amount_input_PreviewKeyDown);
      // 
      // btn_num_5
      // 
      this.btn_num_5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_5.CornerRadius = 0;
      this.btn_num_5.FlatAppearance.BorderSize = 0;
      this.btn_num_5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_5.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_5.Image = null;
      this.btn_num_5.IsSelected = false;
      this.btn_num_5.Location = new System.Drawing.Point(81, 205);
      this.btn_num_5.Name = "btn_num_5";
      this.btn_num_5.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_5.SelectedColor = System.Drawing.Color.Empty;
      this.btn_num_5.Size = new System.Drawing.Size(70, 50);
      this.btn_num_5.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_5.TabIndex = 4;
      this.btn_num_5.TabStop = false;
      this.btn_num_5.Text = "5";
      this.btn_num_5.UseVisualStyleBackColor = false;
      this.btn_num_5.Click += new System.EventHandler(this.btn_num_5_Click);
      this.btn_num_5.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_amount_input_KeyPress);
      this.btn_num_5.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_amount_input_PreviewKeyDown);
      // 
      // btn_num_dot
      // 
      this.btn_num_dot.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_dot.CornerRadius = 0;
      this.btn_num_dot.FlatAppearance.BorderSize = 0;
      this.btn_num_dot.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_dot.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_dot.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_dot.Image = null;
      this.btn_num_dot.IsSelected = false;
      this.btn_num_dot.Location = new System.Drawing.Point(158, 315);
      this.btn_num_dot.Name = "btn_num_dot";
      this.btn_num_dot.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_dot.SelectedColor = System.Drawing.Color.Empty;
      this.btn_num_dot.Size = new System.Drawing.Size(70, 50);
      this.btn_num_dot.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_dot.TabIndex = 9;
      this.btn_num_dot.TabStop = false;
      this.btn_num_dot.Text = "�";
      this.btn_num_dot.UseVisualStyleBackColor = false;
      this.btn_num_dot.Click += new System.EventHandler(this.btn_num_dot_Click);
      this.btn_num_dot.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_amount_input_KeyPress);
      this.btn_num_dot.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_amount_input_PreviewKeyDown);
      // 
      // btn_num_6
      // 
      this.btn_num_6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_6.CornerRadius = 0;
      this.btn_num_6.FlatAppearance.BorderSize = 0;
      this.btn_num_6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_6.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_6.Image = null;
      this.btn_num_6.IsSelected = false;
      this.btn_num_6.Location = new System.Drawing.Point(158, 205);
      this.btn_num_6.Name = "btn_num_6";
      this.btn_num_6.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_6.SelectedColor = System.Drawing.Color.Empty;
      this.btn_num_6.Size = new System.Drawing.Size(70, 50);
      this.btn_num_6.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_6.TabIndex = 5;
      this.btn_num_6.TabStop = false;
      this.btn_num_6.Text = "6";
      this.btn_num_6.UseVisualStyleBackColor = false;
      this.btn_num_6.Click += new System.EventHandler(this.btn_num_6_Click);
      this.btn_num_6.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_amount_input_KeyPress);
      this.btn_num_6.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_amount_input_PreviewKeyDown);
      // 
      // btn_num_9
      // 
      this.btn_num_9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_9.CornerRadius = 0;
      this.btn_num_9.FlatAppearance.BorderSize = 0;
      this.btn_num_9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_9.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_9.Image = null;
      this.btn_num_9.IsSelected = false;
      this.btn_num_9.Location = new System.Drawing.Point(158, 260);
      this.btn_num_9.Name = "btn_num_9";
      this.btn_num_9.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_9.SelectedColor = System.Drawing.Color.Empty;
      this.btn_num_9.Size = new System.Drawing.Size(70, 50);
      this.btn_num_9.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_9.TabIndex = 8;
      this.btn_num_9.TabStop = false;
      this.btn_num_9.Text = "9";
      this.btn_num_9.UseVisualStyleBackColor = false;
      this.btn_num_9.Click += new System.EventHandler(this.btn_num_9_Click);
      this.btn_num_9.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_amount_input_KeyPress);
      this.btn_num_9.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_amount_input_PreviewKeyDown);
      // 
      // btn_num_7
      // 
      this.btn_num_7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_7.CornerRadius = 0;
      this.btn_num_7.FlatAppearance.BorderSize = 0;
      this.btn_num_7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_7.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_7.Image = null;
      this.btn_num_7.IsSelected = false;
      this.btn_num_7.Location = new System.Drawing.Point(4, 260);
      this.btn_num_7.Name = "btn_num_7";
      this.btn_num_7.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_7.SelectedColor = System.Drawing.Color.Empty;
      this.btn_num_7.Size = new System.Drawing.Size(70, 50);
      this.btn_num_7.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_7.TabIndex = 6;
      this.btn_num_7.TabStop = false;
      this.btn_num_7.Text = "7";
      this.btn_num_7.UseVisualStyleBackColor = false;
      this.btn_num_7.Click += new System.EventHandler(this.btn_num_7_Click);
      this.btn_num_7.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_amount_input_KeyPress);
      this.btn_num_7.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_amount_input_PreviewKeyDown);
      // 
      // btn_num_8
      // 
      this.btn_num_8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_8.CornerRadius = 0;
      this.btn_num_8.FlatAppearance.BorderSize = 0;
      this.btn_num_8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_8.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_8.Image = null;
      this.btn_num_8.IsSelected = false;
      this.btn_num_8.Location = new System.Drawing.Point(81, 260);
      this.btn_num_8.Name = "btn_num_8";
      this.btn_num_8.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_8.SelectedColor = System.Drawing.Color.Empty;
      this.btn_num_8.Size = new System.Drawing.Size(70, 50);
      this.btn_num_8.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_8.TabIndex = 7;
      this.btn_num_8.TabStop = false;
      this.btn_num_8.Text = "8";
      this.btn_num_8.UseVisualStyleBackColor = false;
      this.btn_num_8.Click += new System.EventHandler(this.btn_num_8_Click);
      this.btn_num_8.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_amount_input_KeyPress);
      this.btn_num_8.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_amount_input_PreviewKeyDown);
      // 
      // promo_filter
      // 
      this.promo_filter.BackColor = System.Drawing.Color.Transparent;
      this.promo_filter.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.promo_filter.Location = new System.Drawing.Point(12, 542);
      this.promo_filter.Margin = new System.Windows.Forms.Padding(0);
      this.promo_filter.MaximumSize = new System.Drawing.Size(420, 137);
      this.promo_filter.Name = "promo_filter";
      this.promo_filter.ShowHeadLabel = true;
      this.promo_filter.Size = new System.Drawing.Size(260, 70);
      this.promo_filter.TabIndex = 3;
      this.promo_filter.VirtualMode = false;
      // 
      // gb_common
      // 
      this.gb_common.BackColor = System.Drawing.Color.Transparent;
      this.gb_common.BorderColor = System.Drawing.Color.Empty;
      this.gb_common.Controls.Add(this.promo_filter);
      this.gb_common.Controls.Add(this.btn_promos_change_view);
      this.gb_common.Controls.Add(this.lbl_cage_currency_total);
      this.gb_common.Controls.Add(this.dgv_common);
      this.gb_common.Controls.Add(this.lbl_cage_total_value);
      this.gb_common.Controls.Add(this.btn_cancel_by_cashier);
      this.gb_common.Controls.Add(this.lbl_cage_total_text);
      this.gb_common.CornerRadius = 10;
      this.gb_common.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_common.ForeColor = System.Drawing.Color.Black;
      this.gb_common.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gb_common.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_common.HeaderHeight = 0;
      this.gb_common.HeaderSubText = null;
      this.gb_common.HeaderText = null;
      this.gb_common.Location = new System.Drawing.Point(11, 11);
      this.gb_common.Name = "gb_common";
      this.gb_common.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_common.Size = new System.Drawing.Size(280, 400);
      this.gb_common.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NONE;
      this.gb_common.TabIndex = 0;
      this.gb_common.Visible = false;
      // 
      // btn_promos_change_view
      // 
      this.btn_promos_change_view.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_promos_change_view.FlatAppearance.BorderSize = 0;
      this.btn_promos_change_view.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_promos_change_view.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_promos_change_view.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_promos_change_view.Image = null;
      this.btn_promos_change_view.IsSelected = false;
      this.btn_promos_change_view.Location = new System.Drawing.Point(75, 336);
      this.btn_promos_change_view.Name = "btn_promos_change_view";
      this.btn_promos_change_view.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_promos_change_view.Size = new System.Drawing.Size(130, 50);
      this.btn_promos_change_view.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_promos_change_view.TabIndex = 1;
      this.btn_promos_change_view.Text = "XALLPROMOS";
      this.btn_promos_change_view.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
      this.btn_promos_change_view.UseVisualStyleBackColor = false;
      this.btn_promos_change_view.Click += new System.EventHandler(this.btn_promos_change_view_Click);
      // 
      // lbl_cage_currency_total
      // 
      this.lbl_cage_currency_total.BackColor = System.Drawing.Color.Transparent;
      this.lbl_cage_currency_total.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_cage_currency_total.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_cage_currency_total.Location = new System.Drawing.Point(11, 380);
      this.lbl_cage_currency_total.Name = "lbl_cage_currency_total";
      this.lbl_cage_currency_total.Size = new System.Drawing.Size(231, 18);
      this.lbl_cage_currency_total.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_cage_currency_total.TabIndex = 6;
      this.lbl_cage_currency_total.Text = "xMsgCurrency";
      this.lbl_cage_currency_total.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.lbl_cage_currency_total.Visible = false;
      // 
      // dgv_common
      // 
      this.dgv_common.AllowUserToAddRows = false;
      this.dgv_common.AllowUserToDeleteRows = false;
      this.dgv_common.AllowUserToResizeColumns = false;
      this.dgv_common.AllowUserToResizeRows = false;
      this.dgv_common.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.dgv_common.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.dgv_common.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
      this.dgv_common.ColumnHeaderHeight = 35;
      this.dgv_common.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
      dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      dataGridViewCellStyle1.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
      dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
      dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
      this.dgv_common.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
      this.dgv_common.ColumnHeadersHeight = 35;
      this.dgv_common.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
      this.dgv_common.CornerRadius = 10;
      this.dgv_common.DefaultCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.dgv_common.DefaultCellForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.dgv_common.DefaultCellSelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.dgv_common.DefaultCellSelectionForeColor = System.Drawing.Color.White;
      dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      dataGridViewCellStyle2.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.White;
      dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
      this.dgv_common.DefaultCellStyle = dataGridViewCellStyle2;
      this.dgv_common.EnableHeadersVisualStyles = false;
      this.dgv_common.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.dgv_common.GridColor = System.Drawing.Color.White;
      this.dgv_common.HeaderBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
      this.dgv_common.HeaderDefaultCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.dgv_common.HeaderDefaultCellForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.dgv_common.HeaderImages = null;
      this.dgv_common.Location = new System.Drawing.Point(0, 0);
      this.dgv_common.Name = "dgv_common";
      this.dgv_common.ReadOnly = true;
      this.dgv_common.RowHeadersVisible = false;
      this.dgv_common.RowHeadersWidth = 35;
      this.dgv_common.RowTemplate.Height = 45;
      this.dgv_common.RowTemplateHeight = 45;
      this.dgv_common.Size = new System.Drawing.Size(280, 261);
      this.dgv_common.Style = WSI.Cashier.Controls.uc_DataGridView.DataGridViewStyle.PROMOTIONS;
      this.dgv_common.TabIndex = 0;
      this.dgv_common.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_common_CellClick);
      this.dgv_common.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgv_common_CellFormatting);
      this.dgv_common.SelectionChanged += new System.EventHandler(this.dgv_common_SelectionChanged);
      // 
      // lbl_cage_total_value
      // 
      this.lbl_cage_total_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_cage_total_value.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_cage_total_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_cage_total_value.Location = new System.Drawing.Point(172, 321);
      this.lbl_cage_total_value.Name = "lbl_cage_total_value";
      this.lbl_cage_total_value.Size = new System.Drawing.Size(78, 20);
      this.lbl_cage_total_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_cage_total_value.TabIndex = 4;
      this.lbl_cage_total_value.Text = "V02";
      this.lbl_cage_total_value.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // btn_cancel_by_cashier
      // 
      this.btn_cancel_by_cashier.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_cancel_by_cashier.FlatAppearance.BorderSize = 0;
      this.btn_cancel_by_cashier.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_cancel_by_cashier.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_cancel_by_cashier.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_cancel_by_cashier.Image = null;
      this.btn_cancel_by_cashier.IsSelected = false;
      this.btn_cancel_by_cashier.Location = new System.Drawing.Point(75, 336);
      this.btn_cancel_by_cashier.Name = "btn_cancel_by_cashier";
      this.btn_cancel_by_cashier.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_cancel_by_cashier.Size = new System.Drawing.Size(130, 50);
      this.btn_cancel_by_cashier.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_cancel_by_cashier.TabIndex = 2;
      this.btn_cancel_by_cashier.Text = "XCANCEL BY CASHIER";
      this.btn_cancel_by_cashier.UseVisualStyleBackColor = false;
      this.btn_cancel_by_cashier.Visible = false;
      this.btn_cancel_by_cashier.Click += new System.EventHandler(this.btn_cancel_by_cashier_Click);
      // 
      // lbl_cage_total_text
      // 
      this.lbl_cage_total_text.BackColor = System.Drawing.Color.Transparent;
      this.lbl_cage_total_text.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_cage_total_text.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_cage_total_text.Location = new System.Drawing.Point(3, 321);
      this.lbl_cage_total_text.Name = "lbl_cage_total_text";
      this.lbl_cage_total_text.Size = new System.Drawing.Size(103, 20);
      this.lbl_cage_total_text.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_cage_total_text.TabIndex = 0;
      this.lbl_cage_total_text.Text = "V01";
      this.lbl_cage_total_text.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // btn_show_hide_rows
      // 
      this.btn_show_hide_rows.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.btn_show_hide_rows.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_show_hide_rows.FlatAppearance.BorderColor = System.Drawing.SystemColors.Control;
      this.btn_show_hide_rows.FlatAppearance.BorderSize = 0;
      this.btn_show_hide_rows.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_show_hide_rows.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_show_hide_rows.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_show_hide_rows.Image = null;
      this.btn_show_hide_rows.IsSelected = false;
      this.btn_show_hide_rows.Location = new System.Drawing.Point(150, 573);
      this.btn_show_hide_rows.Name = "btn_show_hide_rows";
      this.btn_show_hide_rows.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_show_hide_rows.Size = new System.Drawing.Size(155, 60);
      this.btn_show_hide_rows.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_show_hide_rows.TabIndex = 2;
      this.btn_show_hide_rows.Text = "OCULTAR FILAS SIN CANTIDAD";
      this.btn_show_hide_rows.UseVisualStyleBackColor = false;
      this.btn_show_hide_rows.Visible = false;
      this.btn_show_hide_rows.Click += new System.EventHandler(this.btn_show_hide_rows_Click);
      // 
      // lbl_promo_parameters
      // 
      this.lbl_promo_parameters.BackColor = System.Drawing.Color.Transparent;
      this.lbl_promo_parameters.Dock = System.Windows.Forms.DockStyle.Top;
      this.lbl_promo_parameters.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_promo_parameters.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_promo_parameters.Location = new System.Drawing.Point(0, 40);
      this.lbl_promo_parameters.Name = "lbl_promo_parameters";
      this.lbl_promo_parameters.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
      this.lbl_promo_parameters.Size = new System.Drawing.Size(280, 105);
      this.lbl_promo_parameters.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLUE;
      this.lbl_promo_parameters.TabIndex = 0;
      this.lbl_promo_parameters.Text = resources.GetString("lbl_promo_parameters.Text");
      // 
      // lbl_promo_input_values
      // 
      this.lbl_promo_input_values.BackColor = System.Drawing.Color.Transparent;
      this.lbl_promo_input_values.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_promo_input_values.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_promo_input_values.Location = new System.Drawing.Point(0, 145);
      this.lbl_promo_input_values.Name = "lbl_promo_input_values";
      this.lbl_promo_input_values.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
      this.lbl_promo_input_values.Size = new System.Drawing.Size(280, 45);
      this.lbl_promo_input_values.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLUE;
      this.lbl_promo_input_values.TabIndex = 1;
      this.lbl_promo_input_values.Text = "xCashIn: 100$\r\nxSpent: 300$\r\nxTokens: 999\r\n";
      // 
      // lbl_promo_reward_value
      // 
      this.lbl_promo_reward_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_promo_reward_value.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.lbl_promo_reward_value.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_promo_reward_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_promo_reward_value.Location = new System.Drawing.Point(0, 210);
      this.lbl_promo_reward_value.Name = "lbl_promo_reward_value";
      this.lbl_promo_reward_value.Padding = new System.Windows.Forms.Padding(0, 0, 8, 0);
      this.lbl_promo_reward_value.Size = new System.Drawing.Size(280, 25);
      this.lbl_promo_reward_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_promo_reward_value.TabIndex = 3;
      this.lbl_promo_reward_value.Text = "x$9.999 non-redeemable";
      this.lbl_promo_reward_value.TextAlign = System.Drawing.ContentAlignment.TopRight;
      // 
      // lbl_promo_reward_text
      // 
      this.lbl_promo_reward_text.BackColor = System.Drawing.Color.Transparent;
      this.lbl_promo_reward_text.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.lbl_promo_reward_text.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_promo_reward_text.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_promo_reward_text.Location = new System.Drawing.Point(0, 192);
      this.lbl_promo_reward_text.Name = "lbl_promo_reward_text";
      this.lbl_promo_reward_text.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
      this.lbl_promo_reward_text.Size = new System.Drawing.Size(280, 18);
      this.lbl_promo_reward_text.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLUE;
      this.lbl_promo_reward_text.TabIndex = 2;
      this.lbl_promo_reward_text.Text = "xPlayer gets:";
      // 
      // pnl_buttons
      // 
      this.pnl_buttons.BackColor = System.Drawing.Color.Transparent;
      this.pnl_buttons.Controls.Add(this.btn_cancel);
      this.pnl_buttons.Controls.Add(this.btn_ok);
      this.pnl_buttons.Location = new System.Drawing.Point(693, 598);
      this.pnl_buttons.Name = "pnl_buttons";
      this.pnl_buttons.Size = new System.Drawing.Size(320, 60);
      this.pnl_buttons.TabIndex = 28;
      // 
      // btn_cancel
      // 
      this.btn_cancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btn_cancel.FlatAppearance.BorderSize = 0;
      this.btn_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_cancel.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_cancel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_cancel.Image = null;
      this.btn_cancel.IsSelected = false;
      this.btn_cancel.Location = new System.Drawing.Point(0, 0);
      this.btn_cancel.Name = "btn_cancel";
      this.btn_cancel.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_cancel.Size = new System.Drawing.Size(155, 60);
      this.btn_cancel.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_cancel.TabIndex = 0;
      this.btn_cancel.Text = "CANCEL";
      this.btn_cancel.UseVisualStyleBackColor = false;
      this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
      this.btn_cancel.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_amount_input_KeyPress);
      // 
      // btn_ok
      // 
      this.btn_ok.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_ok.FlatAppearance.BorderSize = 0;
      this.btn_ok.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_ok.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_ok.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_ok.Image = null;
      this.btn_ok.IsSelected = false;
      this.btn_ok.Location = new System.Drawing.Point(165, 0);
      this.btn_ok.Name = "btn_ok";
      this.btn_ok.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_ok.Size = new System.Drawing.Size(155, 60);
      this.btn_ok.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_ok.TabIndex = 1;
      this.btn_ok.Text = "OK";
      this.btn_ok.UseVisualStyleBackColor = false;
      this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
      this.btn_ok.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_amount_input_KeyPress);
      // 
      // btn_check_redeem
      // 
      this.btn_check_redeem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(47)))), ((int)(((byte)(50)))));
      this.btn_check_redeem.FlatAppearance.BorderSize = 0;
      this.btn_check_redeem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_check_redeem.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_check_redeem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_check_redeem.Image = null;
      this.btn_check_redeem.IsSelected = false;
      this.btn_check_redeem.Location = new System.Drawing.Point(527, 597);
      this.btn_check_redeem.Name = "btn_check_redeem";
      this.btn_check_redeem.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_check_redeem.Size = new System.Drawing.Size(155, 60);
      this.btn_check_redeem.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.TERTIARY;
      this.btn_check_redeem.TabIndex = 8;
      this.btn_check_redeem.Text = "XCHECK REDEEM";
      this.btn_check_redeem.UseVisualStyleBackColor = false;
      this.btn_check_redeem.Visible = false;
      this.btn_check_redeem.Click += new System.EventHandler(this.btn_check_redeem_Click);
      // 
      // lbl_currency_to_pay
      // 
      this.lbl_currency_to_pay.BackColor = System.Drawing.Color.Transparent;
      this.lbl_currency_to_pay.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_currency_to_pay.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_currency_to_pay.Location = new System.Drawing.Point(695, 580);
      this.lbl_currency_to_pay.Name = "lbl_currency_to_pay";
      this.lbl_currency_to_pay.Size = new System.Drawing.Size(231, 15);
      this.lbl_currency_to_pay.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_currency_to_pay.TabIndex = 45;
      this.lbl_currency_to_pay.Text = "xMsgCurrency";
      this.lbl_currency_to_pay.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.lbl_currency_to_pay.Visible = false;
      // 
      // lbl_total_to_pay
      // 
      this.lbl_total_to_pay.BackColor = System.Drawing.Color.Transparent;
      this.lbl_total_to_pay.Font = new System.Drawing.Font("Open Sans Semibold", 32F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_total_to_pay.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.lbl_total_to_pay.Location = new System.Drawing.Point(3, 50);
      this.lbl_total_to_pay.Name = "lbl_total_to_pay";
      this.lbl_total_to_pay.Size = new System.Drawing.Size(314, 46);
      this.lbl_total_to_pay.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_32PX_WHITE;
      this.lbl_total_to_pay.TabIndex = 26;
      this.lbl_total_to_pay.Text = "$XXX.XX";
      this.lbl_total_to_pay.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // gb_tokens
      // 
      this.gb_tokens.BackColor = System.Drawing.Color.Transparent;
      this.gb_tokens.BorderColor = System.Drawing.Color.Empty;
      this.gb_tokens.Controls.Add(this.btn_enter_tokens);
      this.gb_tokens.Controls.Add(this.lbl_entered_tokens);
      this.gb_tokens.CornerRadius = 10;
      this.gb_tokens.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_tokens.ForeColor = System.Drawing.Color.Black;
      this.gb_tokens.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gb_tokens.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_tokens.HeaderHeight = 35;
      this.gb_tokens.HeaderSubText = null;
      this.gb_tokens.HeaderText = "XTOKENS";
      this.gb_tokens.Location = new System.Drawing.Point(302, 528);
      this.gb_tokens.Name = "gb_tokens";
      this.gb_tokens.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_tokens.Size = new System.Drawing.Size(380, 122);
      this.gb_tokens.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.gb_tokens.TabIndex = 5;
      this.gb_tokens.Visible = false;
      // 
      // btn_enter_tokens
      // 
      this.btn_enter_tokens.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.btn_enter_tokens.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_enter_tokens.FlatAppearance.BorderSize = 0;
      this.btn_enter_tokens.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_enter_tokens.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_enter_tokens.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_enter_tokens.Image = null;
      this.btn_enter_tokens.IsSelected = false;
      this.btn_enter_tokens.Location = new System.Drawing.Point(235, 56);
      this.btn_enter_tokens.Name = "btn_enter_tokens";
      this.btn_enter_tokens.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_enter_tokens.Size = new System.Drawing.Size(128, 48);
      this.btn_enter_tokens.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_enter_tokens.TabIndex = 1;
      this.btn_enter_tokens.Text = "XTOKENS";
      this.btn_enter_tokens.UseVisualStyleBackColor = false;
      this.btn_enter_tokens.Click += new System.EventHandler(this.btn_enter_tokens_Click);
      // 
      // lbl_entered_tokens
      // 
      this.lbl_entered_tokens.BackColor = System.Drawing.Color.Transparent;
      this.lbl_entered_tokens.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_entered_tokens.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_entered_tokens.Location = new System.Drawing.Point(16, 71);
      this.lbl_entered_tokens.Name = "lbl_entered_tokens";
      this.lbl_entered_tokens.Size = new System.Drawing.Size(150, 18);
      this.lbl_entered_tokens.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_entered_tokens.TabIndex = 0;
      this.lbl_entered_tokens.Text = "xNum. Tokens: XXX";
      // 
      // btn_enter_flyer
      // 
      this.btn_enter_flyer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.btn_enter_flyer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_enter_flyer.FlatAppearance.BorderSize = 0;
      this.btn_enter_flyer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_enter_flyer.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_enter_flyer.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_enter_flyer.Image = null;
      this.btn_enter_flyer.IsSelected = false;
      this.btn_enter_flyer.Location = new System.Drawing.Point(327, 641);
      this.btn_enter_flyer.Name = "btn_enter_flyer";
      this.btn_enter_flyer.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_enter_flyer.Size = new System.Drawing.Size(128, 48);
      this.btn_enter_flyer.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_enter_flyer.TabIndex = 2;
      this.btn_enter_flyer.Text = "XFLYER";
      this.btn_enter_flyer.UseVisualStyleBackColor = false;
      this.btn_enter_flyer.Visible = false;
      this.btn_enter_flyer.Click += new System.EventHandler(this.btn_enter_flyer_Click);
      // 
      // grp_cash_type
      // 
      this.grp_cash_type.Controls.Add(this.lbl_cash_type);
      this.grp_cash_type.Controls.Add(this.pb_current_type);
      this.grp_cash_type.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
      this.grp_cash_type.ForeColor = System.Drawing.Color.Black;
      this.grp_cash_type.Location = new System.Drawing.Point(280, 471);
      this.grp_cash_type.Name = "grp_cash_type";
      this.grp_cash_type.Size = new System.Drawing.Size(352, 70);
      this.grp_cash_type.TabIndex = 20;
      this.grp_cash_type.TabStop = false;
      this.grp_cash_type.Visible = false;
      // 
      // lbl_cash_type
      // 
      this.lbl_cash_type.BackColor = System.Drawing.Color.Transparent;
      this.lbl_cash_type.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_cash_type.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_cash_type.Location = new System.Drawing.Point(60, 48);
      this.lbl_cash_type.Name = "lbl_cash_type";
      this.lbl_cash_type.Size = new System.Drawing.Size(150, 18);
      this.lbl_cash_type.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_cash_type.TabIndex = 22;
      this.lbl_cash_type.Text = "xXXXX_XXXXXXXX";
      // 
      // pb_current_type
      // 
      this.pb_current_type.Location = new System.Drawing.Point(6, 20);
      this.pb_current_type.Name = "pb_current_type";
      this.pb_current_type.Size = new System.Drawing.Size(48, 46);
      this.pb_current_type.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
      this.pb_current_type.TabIndex = 24;
      this.pb_current_type.TabStop = false;
      // 
      // web_browser
      // 
      this.web_browser.Location = new System.Drawing.Point(693, 11);
      this.web_browser.MinimumSize = new System.Drawing.Size(20, 20);
      this.web_browser.Name = "web_browser";
      this.web_browser.Size = new System.Drawing.Size(320, 440);
      this.web_browser.TabIndex = 6;
      this.web_browser.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.web_browser_DocumentCompleted);
      // 
      // btn_apply_tax
      // 
      this.btn_apply_tax.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.btn_apply_tax.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_apply_tax.FlatAppearance.BorderSize = 0;
      this.btn_apply_tax.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_apply_tax.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_apply_tax.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_apply_tax.Image = null;
      this.btn_apply_tax.IsSelected = false;
      this.btn_apply_tax.Location = new System.Drawing.Point(286, 571);
      this.btn_apply_tax.Name = "btn_apply_tax";
      this.btn_apply_tax.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_apply_tax.Size = new System.Drawing.Size(155, 60);
      this.btn_apply_tax.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_apply_tax.TabIndex = 1;
      this.btn_apply_tax.Text = "XAPPLYTAX_AMOUNT";
      this.btn_apply_tax.UseVisualStyleBackColor = false;
      this.btn_apply_tax.Visible = false;
      this.btn_apply_tax.Click += new System.EventHandler(this.btn_apply_tax_Click);
      // 
      // gb_total
      // 
      this.gb_total.BackColor = System.Drawing.Color.Transparent;
      this.gb_total.BorderColor = System.Drawing.Color.Empty;
      this.gb_total.Controls.Add(this.lbl_total_to_pay);
      this.gb_total.CornerRadius = 10;
      this.gb_total.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_total.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.gb_total.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_total.HeaderHeight = 35;
      this.gb_total.HeaderSubText = null;
      this.gb_total.HeaderText = "XTOTAL";
      this.gb_total.Location = new System.Drawing.Point(693, 462);
      this.gb_total.Name = "gb_total";
      this.gb_total.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.gb_total.Size = new System.Drawing.Size(320, 115);
      this.gb_total.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.TOTAL;
      this.gb_total.TabIndex = 7;
      // 
      // gb_promotion_info
      // 
      this.gb_promotion_info.BackColor = System.Drawing.Color.Transparent;
      this.gb_promotion_info.BorderColor = System.Drawing.Color.Empty;
      this.gb_promotion_info.Controls.Add(this.lbl_promo_parameters);
      this.gb_promotion_info.Controls.Add(this.lbl_promo_reward_text);
      this.gb_promotion_info.Controls.Add(this.lbl_promo_reward_value);
      this.gb_promotion_info.Controls.Add(this.lbl_promo_input_values);
      this.gb_promotion_info.CornerRadius = 10;
      this.gb_promotion_info.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_promotion_info.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gb_promotion_info.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_promotion_info.HeaderHeight = 35;
      this.gb_promotion_info.HeaderSubText = null;
      this.gb_promotion_info.HeaderText = "XINFOPROMOTION";
      this.gb_promotion_info.Location = new System.Drawing.Point(11, 415);
      this.gb_promotion_info.Name = "gb_promotion_info";
      this.gb_promotion_info.Padding = new System.Windows.Forms.Padding(0, 40, 0, 0);
      this.gb_promotion_info.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_promotion_info.Size = new System.Drawing.Size(280, 235);
      this.gb_promotion_info.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.gb_promotion_info.TabIndex = 0;
      // 
      // uc_payment_method1
      // 
      this.uc_payment_method1.BtnSelectedCurrencyEnabled = true;
      this.uc_payment_method1.Location = new System.Drawing.Point(302, 11);
      this.uc_payment_method1.Name = "uc_payment_method1";
      this.uc_payment_method1.showPaymentCreditLine = false;
      this.uc_payment_method1.Size = new System.Drawing.Size(385, 116);
      this.uc_payment_method1.TabIndex = 46;
      this.uc_payment_method1.CurrencyChanged += new WSI.Cashier.uc_payment_method.CurrencyChangedHandler(this.uc_payment_method1_CurrencyChanged);
      // 
      // frm_amount_input
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.CancelButton = this.btn_cancel;
      this.ClientSize = new System.Drawing.Size(1024, 713);
      this.Controls.Add(this.btn_apply_tax);
      this.Controls.Add(this.btn_enter_flyer);
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "frm_amount_input";
      this.ShowInTaskbar = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
      this.Style = WSI.Cashier.Controls.frm_base.BaseFormStyle.PRINCIPAL;
      this.Text = "frm_add_credit";
      this.Shown += new System.EventHandler(this.frm_amount_input_Shown);
      this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_amount_input_KeyPress);
      this.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_amount_input_PreviewKeyDown);
      this.Controls.SetChildIndex(this.pnl_data, 0);
      this.Controls.SetChildIndex(this.btn_enter_flyer, 0);
      this.Controls.SetChildIndex(this.btn_apply_tax, 0);
      this.pnl_data.ResumeLayout(false);
      this.gp_digits_box.ResumeLayout(false);
      this.gb_common.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.dgv_common)).EndInit();
      this.pnl_buttons.ResumeLayout(false);
      this.gb_tokens.ResumeLayout(false);
      this.grp_cash_type.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.pb_current_type)).EndInit();
      this.gb_total.ResumeLayout(false);
      this.gb_promotion_info.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private WSI.Cashier.Controls.uc_round_textbox txt_amount;
    private WSI.Cashier.Controls.uc_round_panel gp_digits_box;
    public System.Windows.Forms.WebBrowser web_browser;
    private System.Windows.Forms.GroupBox grp_cash_type;
    private WSI.Cashier.Controls.uc_label lbl_cash_type;
    private System.Windows.Forms.PictureBox pb_current_type;
    private WSI.Cashier.Controls.uc_DataGridView dgv_common;
    private WSI.Cashier.Controls.uc_round_panel gb_common;
    private WSI.Cashier.Controls.uc_label lbl_promo_reward_value;
    private WSI.Cashier.Controls.uc_label lbl_promo_input_values;
    private WSI.Cashier.Controls.uc_label lbl_promo_parameters;
    private WSI.Cashier.Controls.uc_round_panel gb_tokens;
    private WSI.Cashier.Controls.uc_label lbl_entered_tokens;
    private WSI.Cashier.Controls.uc_label lbl_total_to_pay;
    private System.Windows.Forms.Panel pnl_buttons;
    private WSI.Cashier.Controls.uc_label lbl_promo_reward_text;
    private uc_promo_filter promo_filter;
    private WSI.Cashier.Controls.uc_label lbl_currency;
    private WSI.Cashier.Controls.uc_label lbl_currency_to_pay;
    private WSI.Cashier.Controls.uc_label lbl_cage_total_value;
    private WSI.Cashier.Controls.uc_label lbl_cage_total_text;
    private WSI.Cashier.Controls.uc_label lbl_cage_currency_total;
    private Controls.uc_round_button btn_num_1;
    private Controls.uc_round_button btn_num_10;
    private Controls.uc_round_button btn_num_dot;
    private Controls.uc_round_button btn_num_9;
    private Controls.uc_round_button btn_num_8;
    private Controls.uc_round_button btn_num_7;
    private Controls.uc_round_button btn_num_6;
    private Controls.uc_round_button btn_num_5;
    private Controls.uc_round_button btn_num_4;
    private Controls.uc_round_button btn_num_3;
    private Controls.uc_round_button btn_num_2;
    private Controls.uc_round_button btn_back;
    private Controls.uc_round_button btn_intro;
    private Controls.uc_round_button btn_cancel;
    private Controls.uc_round_button btn_ok;
    private Controls.uc_round_button btn_money_4;
    private Controls.uc_round_button btn_money_3;
    private Controls.uc_round_button btn_money_2;
    private Controls.uc_round_button btn_money_1;
    private Controls.uc_round_button btn_enter_tokens;
    private Controls.uc_round_button btn_promos_change_view;
    private Controls.uc_round_button btn_cancel_by_cashier;
    private Controls.uc_round_button btn_check_redeem;
    private Controls.uc_round_button btn_clear;
    private Controls.uc_round_button btn_cancel_request;
    private Controls.uc_round_panel gb_total;
    private Controls.uc_round_panel gb_promotion_info;
    private Controls.uc_round_button btn_apply_tax;
    private uc_payment_method uc_payment_method1;
    private Controls.uc_round_button btn_show_hide_rows;
    private Controls.uc_round_button btn_enter_flyer;
  }
}