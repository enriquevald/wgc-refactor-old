//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : frm_withholding.cs
// 
//   DESCRIPTION : 
// 
// REVISION HISTORY :
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 30-JAN-2012 TJG    
// 06-AUG-2012 DDM     Modifications: - Deleted all tabs except the main.
//                                    - Use the class Withhold instead CardData.
//                                    - The SaveWitholding function, only Save 
//                                      values in the class Withhold
// 05-AUG-2012 DDM     Fixed bug. Not should check the last name 2 
// 05-FEB-2013 LEM     Changed type of some controls for data validating (PhoneNumberTextBox and CharactertextBox)
// 11-FEB-2013 ICS     Added a format validation for Name field
// 19-FEB-2013 ICS     Added a format validation for All fields
// 09-AUG-2013 LEM     Auto TAB for date fields
// 13-AUG-2013 DRV     Added support to show automatic OSK if it's enabled or open it if the keyboard button is clicked     
// 20-AUG-2013 FBA     WIG-104: Fixed bug with auto-tab on date fields
// 09-DEC-2013 QMP     Check RFCs against regular expression and allow generic RFCs
// 12-FEB-2014 FBA     Added account holder id type control via CardData class IdentificationTypes
// 20-JUN-2014 LEM     Fixed Bug WIGOSTITO-1241: Wrong RFC validation
// 03-NOV-2014 DHA     Fixed Bug WIG-1616: if RFC is generic don't update account RFC & CURP. Validate if RFC generic matches with country.
// 04-NOV-2014 DHA     Fixed Bug WIG-1639: if RFC is generic and is anonymous account, not validate country
// 12-NOV-2015 ECP     Backlog Item 5900 New Cashier Design
// 14-JUL-2016 LTC     Bug 15598:Witholding - Anonymous Account Shows NLS for RFC field
// 01-MAR-2017 FGB     Bug 24126: Withholding generation: The Excel data is incorrect
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using WSI.Common;
using System.Data.SqlClient;
using System.Data.SqlTypes;

namespace WSI.Cashier
{
  public partial class frm_withholding : Controls.frm_base
  {
    #region Constants

    const Int32 TAB_MAIN = 1;
    const Int32 TAB_CONTACT = 2;
    const Int32 TAB_ADDRESS = 3;
    const Int32 TAB_COMMENTS = 4;

    #endregion

    #region Attributes

    public struct TabItem
    {
      public String name;
      public Boolean enabled;
    }

    WithholdData m_withhold;

    #endregion

    #region Constructor

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="Parent"></param>
    /// <param name="Card"></param>
    public frm_withholding(WithholdData Withhold)
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_withholding", Log.Type.Message);

      this.m_withhold = Withhold;

      InitializeComponent();

      InitializeControlResources();

      Init();
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Initialize NLS strings
    /// </summary>
    private void InitializeControlResources()
    {
      // Title
      this.FormTitle = Resource.String("STR_FRM_WITHOLDING_001");

      //    - Principal Group
      gb_principal.HeaderText = Resource.String("STR_FRM_WITHOLDING_015");

      lbl_holder_id_01.Text = IdentificationTypes.DocIdTypeString(ACCOUNT_HOLDER_ID_TYPE.RFC);  // "RFC"
      lbl_holder_id_02.Text = IdentificationTypes.DocIdTypeString(ACCOUNT_HOLDER_ID_TYPE.CURP);    // "CURP"
      lbl_name_01.Text = Resource.String("STR_FRM_WITHOLDING_002");         // "Surname 01"
      lbl_name_02.Text = Resource.String("STR_FRM_WITHOLDING_003");         // "Surname 02"
      lbl_name_03.Text = Resource.String("STR_FRM_WITHOLDING_004");         // "Name"
      uc_birth_date.DateText = Resource.String("STR_FRM_PLAYER_EDIT_BIRTH_DATE");// "Birth"
      lbl_gender.Text = Resource.String("STR_UC_PLAYER_TRACK_GENDER");
      lbl_msg_blink.Text = "";

      //   - Buttons
      btn_ok.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_OK");
      btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");

      // LTC 14-JUL-2016
      lbl_expected_rfc.Text = "";

    } // InitializeControlResources

    /// <summary>
    /// Initialize screen logic
    /// </summary>
    private void Init()
    {
      // Center screen
      this.Location = new Point(1024 / 2 - this.Width / 2, 0);

      // Show keyboard if automatic OSK it's enabled
      WSIKeyboard.Toggle();


      LoadComboNLS("STR_FRM_PLAYER_TRACKING_CB_GENDER_MALE", this.cb_gender);    // "Hombre"
      LoadComboNLS("STR_FRM_PLAYER_TRACKING_CB_GENDER_FEMALE", this.cb_gender);  // "Mujer"

      // Enable the edition controls
      EnableControls();

      uc_birth_date.Init();
      uc_birth_date.OnDateChanged += new System.EventHandler(this.DataChanged);


      // Load existing card content from DB
      LoadAccountData();

      // RCI 19-AUG-2010: Disable timer. Check data only on Ok button press.
      // Check if data is correct and activate timer
      //CheckData(true);
    }

    /// <summary>
    /// Enable the edition controls 
    /// </summary>
    private void EnableControls()
    {
      //    - Principal Group
      ef_holder_id_01.Enabled = true;
      ef_holder_id_02.Enabled = true;
      ef_name_01.Enabled = true;
      ef_name_02.Enabled = true;
      ef_name_03.Enabled = true;
      uc_birth_date.Enabled = true;
    }



    /// <summary>
    /// Load account data content from DB
    /// </summary>
    private void LoadAccountData()
    {
      //  Principal Group
      uc_birth_date.DateValue = m_withhold.HolderBirthDate;

      ef_holder_id_01.Text = m_withhold.HolderId1;   // RFC
      ef_holder_id_02.Text = m_withhold.HolderId2;   // CURP

      ef_name_01.Text = m_withhold.HolderName1;      // Family Name 01
      ef_name_02.Text = m_withhold.HolderName2;      // Family Name 02
      ef_name_03.Text = m_withhold.HolderName3;      // First Name

      //if (m_withhold.HolderLevel != 0 )
      //{
      //  // Is not anonymous
      //  switch (m_withhold.HolderIdType)
      //  {
      //    case ACCOUNT_HOLDER_ID_TYPE.RFC:
      //        ef_holder_id_01.Text = m_withhold.HolderId;
      //      break;
      //    case ACCOUNT_HOLDER_ID_TYPE.CURP:
      //        ef_holder_id_02.Text = m_withhold.HolderId;
      //      break;
      //    case ACCOUNT_HOLDER_ID_TYPE.UNKNOWN:
      //    case ACCOUNT_HOLDER_ID_TYPE.ELECTOR_CODE:
      //    case ACCOUNT_HOLDER_ID_TYPE.PASSPORT:
      //    case ACCOUNT_HOLDER_ID_TYPE.MAX_TYPES:
      //    default:
      //      break;
      //  }
      //}

      if (m_withhold.HolderGender > 0)
      {
        cb_gender.SelectedIndex = m_withhold.HolderGender - 1;
      }
    }

    /// <summary>
    /// Save screen content to the card
    /// </summary>
    private Boolean SaveWitholding()
    {

      try
      {
        // Save values in the class Withhold
        m_withhold.HolderId1 = ef_holder_id_01.Text;                 // RFC
        m_withhold.HolderId2 = ef_holder_id_02.Text;                 // CURP
        m_withhold.HolderName1 = ef_name_01.Text.Trim();             // Family Name 01
        m_withhold.HolderName2 = ef_name_02.Text.Trim();             // Family Name 02
        m_withhold.HolderName3 = ef_name_03.Text.Trim();             // First Name
        m_withhold.HolderFullName = m_withhold.WithholdingHolderName;// Full Name
        m_withhold.HolderBirthDate = uc_birth_date.DateValue;        // Birth date
        m_withhold.HolderGender = (cb_gender.SelectedIndex == 0) ? 1 : 2; // 1-Man  2-Woman

        if (m_withhold.HolderLevel != 0)
        {
          // If is not anonymous, 
          switch (m_withhold.HolderIdType)
          {
            case ACCOUNT_HOLDER_ID_TYPE.RFC:
              m_withhold.HolderId = ef_holder_id_01.Text;
              break;
            case ACCOUNT_HOLDER_ID_TYPE.CURP:
              m_withhold.HolderId = ef_holder_id_02.Text;
              break;

            default:
              break;
          }
        }

        return true;
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
      finally
      {
      }

      return false;
    }

    /// <summary>
    /// Load combo box items from the NLS
    /// </summary>
    /// <param name="Key"></param>
    /// <param name="Combo"></param>
    private void LoadComboNLS(string Key, Controls.uc_round_combobox Combo)
    {
      String _str_aux;
      _str_aux = Resource.String(Key);
      Combo.Items.Add(_str_aux);
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Check the entered data 
    // 
    //  PARAMS :
    //      - INPUT :
    //          - FocusIt : Whether the control containing a wrong value must be focused or not
    //
    //      - OUTPUT :
    //
    // RETURNS:
    //    True if all the information in text controls is correct.
    //    False if not.
    // 
    //   NOTES:

    private Boolean CheckData(Boolean FocusIt)
    {
      Int32 _legal_age;
      String _aux_string;
      ACCOUNT_HOLDER_ID_TYPE _which_one_is_wrong;

      lbl_msg_blink.Visible = false;
      lbl_msg_blink.Text = "";

      // RCI 19-AUG-2010: Disable timer. Check data only on Ok button press.
      //timer1.Enabled = true;

      try
      {
        //    - RFC
        _aux_string = ef_holder_id_01.Text.Trim().ToUpperInvariant();
        ef_holder_id_01.Text = _aux_string;

        if (_aux_string == "")
        {
          lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC", lbl_holder_id_01.Text);

          if (FocusIt)
          {
            ef_holder_id_01.Focus();
          }

          return false;
        }

        if (!ef_holder_id_01.ValidateFormat()) // Format Validation
        {
          lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", lbl_holder_id_01.Text);

          if (FocusIt)
          {
            ef_holder_id_01.Focus();
          }

          return false;
        }

        //    - CURP (optional)
        _aux_string = ef_holder_id_02.Text.Trim().ToUpperInvariant();
        ef_holder_id_02.Text = _aux_string;

        if (!String.IsNullOrEmpty(_aux_string) && !ef_holder_id_02.ValidateFormat()) // Format Validation
        {
          lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", lbl_holder_id_02.Text);

          if (FocusIt)
          {
            ef_holder_id_02.Focus();
          }

          return false;
        }

        //    - Family name (Father)
        _aux_string = ef_name_01.Text.Trim().ToUpperInvariant();
        ef_name_01.Text = _aux_string;

        if (_aux_string == "")
        {
          lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC", lbl_name_01.Text);

          if (FocusIt)
          {
            ef_name_01.Focus();
          }

          return false;
        }
        else if (!ef_name_01.ValidateFormat()) // Format Validation
        {
          lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", lbl_name_01.Text);

          if (FocusIt)
          {
            ef_name_01.Focus();
          }

          return false;
        }

        //    - Family name (Mother) (optional)
        _aux_string = ef_name_02.Text.Trim().ToUpperInvariant();
        ef_name_02.Text = _aux_string;

        if (!String.IsNullOrEmpty(_aux_string) && !ef_name_02.ValidateFormat()) // Format Validation
        {
          lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", lbl_name_02.Text);

          if (FocusIt)
          {
            ef_name_02.Focus();
          }

          return false;
        }

        //    - First name 
        _aux_string = ef_name_03.Text.Trim().ToUpperInvariant();
        ef_name_03.Text = _aux_string;

        if (_aux_string == "")
        {
          lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC", lbl_name_03.Text);

          if (FocusIt)
          {
            ef_name_03.Focus();
          }

          return false;
        }
        else if (!ef_name_03.ValidateFormat()) // Format Validation
        {
          lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", lbl_name_03.Text);

          if (FocusIt)
          {
            ef_name_03.Focus();
          }

          return false;
        }

        //// Checks whether there already exists another account (different than AccountID) with same name as Name
        //if ( CashierBusinessLogic.DB_CheckExistHolderName ((UInt64) m_card.CardId, lbl_full_name.Text) )
        //{
        //  lbl_msg_blink.Text = Resource.String ("STR_HOLDER_NAME_ALREADY_EXISTS", ef_holder_id_01.Text);
        //
        //  return false;
        //}
        if (uc_birth_date.IsEmpty)
        {
          lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC", uc_birth_date.DateText);

          if (FocusIt)
          {
            uc_birth_date.Focus();
          }

          return false;
        }

        if (!uc_birth_date.ValidateFormat())
        {
          lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_BIRTHDATE_ERROR");

          if (FocusIt)
          {
            uc_birth_date.Focus();
          }

          return false;
        }

        // Check whether the player is an adult
        if (!CardData.IsValidBirthdate(uc_birth_date.DateValue, out _legal_age))
        {
          lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_NOT_ADULT", _legal_age);

          if (FocusIt)
          {
            uc_birth_date.Focus();
          }

          return false;
        }

        //    - Gender
        if (cb_gender.SelectedIndex < 0)
        {
          lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC", lbl_gender.Text);

          if (FocusIt)
          {
            cb_gender.Focus();
          }

          return false;
        }

        //    - Check RFC and CURP consistency
        String _expected_rfc;
        String _expected_curp;
        GENDER _gender;

        switch (cb_gender.SelectedIndex)
        {
          case 0:
            _gender = GENDER.MALE;
            break;

          case 1:
            _gender = GENDER.FEMALE;
            break;

          default:
            _gender = GENDER.UNKNOWN;
            break;
        }

        if (!ValidateFormat.CheckHolderIds(this.ef_name_01.Text, this.ef_name_02.Text, this.ef_name_03.Text,
                                            this.ef_holder_id_01.Text, this.ef_holder_id_02.Text,
                                            this.uc_birth_date.DateValue,
                                            _gender,
                                            true,
                                            out _which_one_is_wrong, out _expected_rfc))
        {
          DialogResult _answer;
          Boolean _stop_on_error;
          Boolean _continue;

          // Wrong RFC / CURP
          _stop_on_error = GeneralParam.GetBoolean("Witholding.Document", "OnWrongId.Stop", false);
          _continue = false;

          if (String.IsNullOrEmpty(_expected_rfc))
          {
            _expected_rfc = "---";
          }

          if (String.IsNullOrEmpty(ef_holder_id_02.Text.Trim()))
          {
            _expected_curp = "";
          }
          else
          {
            _expected_curp = _expected_rfc.Substring(0, Math.Min(10, _expected_rfc.Length)) + "---";
          }

          if (_stop_on_error)
          {
            String _message;

            _message = Resource.String("STR_FRM_WITHOLDING_WRONG_RFC_ERROR", _expected_rfc, _expected_curp);
            _message = _message.Replace("\\r\\n", "\r\n");
            _answer = frm_message.Show(_message,
                                        Resource.String("STR_APP_GEN_MSG_ERROR"),
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Error,
                                        this.ParentForm);
          }
          else
          {
            String _message;

            _message = Resource.String("STR_FRM_WITHOLDING_WRONG_RFC_CONTINUE", _expected_rfc, _expected_curp);
            _message = _message.Replace("\\r\\n", "\r\n");
            _answer = frm_message.Show(_message,
                                        Resource.String("STR_APP_GEN_MSG_WARNING"),
                                        MessageBoxButtons.YesNo,
                                        MessageBoxIcon.Warning,
                                        this.ParentForm);

            if (_answer != DialogResult.OK)
            {
              // User doesn't want to fix the values
              _continue = true;
            }
          }

          if (!_continue)
          {

            switch (_which_one_is_wrong)
            {
              default:
              case ACCOUNT_HOLDER_ID_TYPE.RFC:
                lbl_msg_blink.Text = Resource.String("STR_FRM_WITHOLDING_009", lbl_holder_id_01.Text);

                if (FocusIt)
                {
                  ef_holder_id_01.Focus();
                }
                break;

              case ACCOUNT_HOLDER_ID_TYPE.CURP:
                lbl_msg_blink.Text = Resource.String("STR_FRM_WITHOLDING_009", lbl_holder_id_02.Text);

                if (FocusIt)
                {
                  ef_holder_id_02.Focus();
                }
                break;
            } // switch

            return false;
          }
        }

        // QMP 09-DEC-2013: Check RFC against regex
        if (Common.ValidateFormat.WrongRFC(ef_holder_id_01.Text))
        {
          lbl_msg_blink.Text = Resource.String("STR_FRM_WITHOLDING_009", lbl_holder_id_01.Text);

          if (FocusIt)
          {
            ef_holder_id_01.Focus();
          }

          return false;
        }

        return true;
      }
      finally
      {
        if (lbl_msg_blink.Text != "")
        {
          lbl_msg_blink.Visible = true;
        }
      }
    } //CheckData

    //------------------------------------------------------------------------------
    // PURPOSE: Check if ID is already exists in DB
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //    True Id Exists
    //    False Id does not exists
    // 
    //   NOTES:

    private bool CheckUniqueIds(String Id)
    {
      SqlConnection _sql_conn = null;
      SqlTransaction _sql_trx = null;
      SqlCommand _sql_cmd;
      String _sql_query;
      Int32 _count_id;

      _sql_trx = null;
      _sql_conn = null;

      try
      {
        // Get Sql Connection 
        _sql_conn = WGDB.Connection();
        _sql_trx = _sql_conn.BeginTransaction();

        _sql_query = "SELECT COUNT(*) FROM ACCOUNTS WHERE AC_HOLDER_ID = @p1 AND AC_ACCOUNT_ID <> @p2 ";

        _sql_cmd = new SqlCommand(_sql_query);
        _sql_cmd.Connection = _sql_trx.Connection;
        _sql_cmd.Transaction = _sql_trx;
        _sql_cmd.Parameters.Add("@p1", SqlDbType.NVarChar, 20, "AC_HOLDER_ID").Value = Id;
        _sql_cmd.Parameters.Add("@p2", SqlDbType.BigInt, 8, "AC_ACCOUNT_ID").Value = m_withhold.AccountId;

        _count_id = (Int32)_sql_cmd.ExecuteScalar();

        if (_count_id > 0)
        {
          return true;
        }

      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
      finally
      {
        if (_sql_trx != null)
        {
          _sql_trx.Dispose();
          _sql_trx = null;
        }

        // Close connection
        if (_sql_conn != null)
        {
          _sql_conn.Close();
          _sql_conn = null;
        }
      }

      return false;

    } // CheckUniqueIds

    private void txt_date_TextChanged(object sender, EventArgs e)
    {
      TextBox _tag;

      _tag = (TextBox)sender;

      if (_tag.Text.Length < _tag.MaxLength)
      {
        return;
      }

      Control nextInTabOrder = GetNextControl((Control)sender, true);
      nextInTabOrder.Focus();
    }

    #endregion

    #region "Handlers"

    private void frm_withholding_Shown(object sender, EventArgs e)
    {
      this.ef_holder_id_01.Focus();

      return;
    } // frm_withholding_Shown

    private void btn_ok_Click(object sender, EventArgs e)
    {
      // Check out whether the data is right
      if (CheckData(true))
      {
        // Save Witholding entered data to the related DB account
        if (SaveWitholding())
        {
          // hide keyboard and close
          WSIKeyboard.Hide();

          this.DialogResult = DialogResult.OK;

          this.Dispose();
        }
      }
    }

    private void btn_cancel_Click(object sender, EventArgs e)
    {
      // hide keyboard and close
      WSIKeyboard.Hide();

      this.DialogResult = DialogResult.Cancel;

      this.Dispose();
    }

    private void btn_keyboard_Click(object sender, EventArgs e)
    {
      // show keyboard (if hidden)
      WSIKeyboard.ForceToggle();
    }

    private void timer1_Tick(object sender, EventArgs e)
    {
      // Check if data is correct
      CheckData(false);
    }


    #endregion

    private void DataChanged(object sender, EventArgs e)
    {
      try
      {
        String _expected;
        String _rfc;

        _rfc = ef_holder_id_01.Text.Trim();
        _expected = "";

        try
        {
          if (uc_birth_date.ValidateFormat())
          {
            _expected = ValidateFormat.ExpectedRFC(ef_name_01.Text, ef_name_02.Text, ef_name_03.Text, uc_birth_date.DateValue);
          }
        }
        catch
        {
          _expected = "";
        }

        // QMP 09-DEC-2013: Allow generic RFCs
        if (_rfc == _expected || ValidateFormat.IsGenericRFC(_rfc))
        {
          lbl_expected_rfc.Visible = false;
        }
        else
        {
          if (!String.IsNullOrEmpty(_expected))
          {
            lbl_expected_rfc.Text = _expected;
            lbl_expected_rfc.Visible = true;
          }
          else
          {
            lbl_expected_rfc.Visible = false;
          }
        }

        //Auto TAB
        txt_date_TextChanged(sender, e);
      }
      catch
      {
      }
    }

  }
}