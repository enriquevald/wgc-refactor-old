namespace WSI.Cashier
{
  partial class frm_voucher_space
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.lbl_waiting = new WSI.Cashier.Controls.uc_label();
      this.txt_voucher_name = new WSI.Cashier.Controls.uc_round_textbox();
      this.btn_accept = new WSI.Cashier.Controls.uc_round_button();
      this.btn_cancel = new WSI.Cashier.Controls.uc_round_button();
      this.timer1 = new System.Windows.Forms.Timer(this.components);
      this.pnl_data.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.Controls.Add(this.lbl_waiting);
      this.pnl_data.Controls.Add(this.txt_voucher_name);
      this.pnl_data.Controls.Add(this.btn_accept);
      this.pnl_data.Controls.Add(this.btn_cancel);
      this.pnl_data.Size = new System.Drawing.Size(390, 209);
      this.pnl_data.TabIndex = 0;
      // 
      // lbl_waiting
      // 
      this.lbl_waiting.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_waiting.BackColor = System.Drawing.Color.Transparent;
      this.lbl_waiting.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_waiting.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.lbl_waiting.Location = new System.Drawing.Point(0, 93);
      this.lbl_waiting.Name = "lbl_waiting";
      this.lbl_waiting.Size = new System.Drawing.Size(390, 23);
      this.lbl_waiting.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_RED;
      this.lbl_waiting.TabIndex = 1;
      this.lbl_waiting.Text = "xMESSAGE LINE";
      this.lbl_waiting.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.lbl_waiting.Visible = false;
      // 
      // txt_voucher_name
      // 
      this.txt_voucher_name.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_voucher_name.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_voucher_name.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.txt_voucher_name.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.txt_voucher_name.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_voucher_name.CornerRadius = 5;
      this.txt_voucher_name.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_voucher_name.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.txt_voucher_name.Location = new System.Drawing.Point(42, 35);
      this.txt_voucher_name.MaxLength = 25;
      this.txt_voucher_name.Multiline = false;
      this.txt_voucher_name.Name = "txt_voucher_name";
      this.txt_voucher_name.PasswordChar = '\0';
      this.txt_voucher_name.ReadOnly = false;
      this.txt_voucher_name.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_voucher_name.SelectedText = "";
      this.txt_voucher_name.SelectionLength = 0;
      this.txt_voucher_name.SelectionStart = 0;
      this.txt_voucher_name.Size = new System.Drawing.Size(307, 40);
      this.txt_voucher_name.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_voucher_name.TabIndex = 0;
      this.txt_voucher_name.TabStop = false;
      this.txt_voucher_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_voucher_name.UseSystemPasswordChar = false;
      this.txt_voucher_name.WaterMark = null;
      // 
      // btn_accept
      // 
      this.btn_accept.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.btn_accept.FlatAppearance.BorderSize = 0;
      this.btn_accept.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_accept.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_accept.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.btn_accept.Image = null;
      this.btn_accept.Location = new System.Drawing.Point(200, 140);
      this.btn_accept.Name = "btn_accept";
      this.btn_accept.Size = new System.Drawing.Size(155, 60);
      this.btn_accept.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_accept.TabIndex = 2;
      this.btn_accept.Text = "XACCEPT";
      this.btn_accept.UseVisualStyleBackColor = false;
      this.btn_accept.Click += new System.EventHandler(this.btn_accept_Click);
      // 
      // btn_cancel
      // 
      this.btn_cancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.btn_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btn_cancel.FlatAppearance.BorderSize = 0;
      this.btn_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_cancel.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_cancel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.btn_cancel.Image = null;
      this.btn_cancel.Location = new System.Drawing.Point(35, 140);
      this.btn_cancel.Name = "btn_cancel";
      this.btn_cancel.Size = new System.Drawing.Size(155, 60);
      this.btn_cancel.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_cancel.TabIndex = 3;
      this.btn_cancel.Text = "XCANCEL";
      this.btn_cancel.UseVisualStyleBackColor = false;
      this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
      // 
      // timer1
      // 
      this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
      // 
      // frm_voucher_space
      // 
      this.AcceptButton = this.btn_accept;
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.btn_cancel;
      this.ClientSize = new System.Drawing.Size(390, 264);
      this.ControlBox = false;
      this.HeaderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.Name = "frm_voucher_space";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Style = WSI.Cashier.Controls.frm_base.BaseFormStyle.PRINCIPAL;
      this.Text = "Card Assign";
      this.Shown += new System.EventHandler(this.frm_voucher_space_Shown);
      this.pnl_data.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private WSI.Cashier.Controls.uc_round_button btn_cancel;
    private WSI.Cashier.Controls.uc_round_button btn_accept;
    private WSI.Cashier.Controls.uc_round_textbox txt_voucher_name;
    private System.Windows.Forms.Timer timer1;
    private WSI.Cashier.Controls.uc_label lbl_waiting;
  }
}