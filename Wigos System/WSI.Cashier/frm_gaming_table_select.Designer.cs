namespace WSI.Cashier
{
  partial class frm_gaming_table_select
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.splitContainer1 = new System.Windows.Forms.SplitContainer();
      this.lbl_gaming_table_sel_title = new WSI.Cashier.Controls.uc_label();
      this.bt_ok = new WSI.Cashier.Controls.uc_round_button();
      this.uc_gaming_table_filter1 = new WSI.Cashier.uc_gaming_table_filter();
      this.btn_close = new WSI.Cashier.Controls.uc_round_button();
      this.pnl_data.SuspendLayout();
      this.splitContainer1.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.Controls.Add(this.uc_gaming_table_filter1);
      this.pnl_data.Controls.Add(this.bt_ok);
      this.pnl_data.Controls.Add(this.btn_close);
      this.pnl_data.Size = new System.Drawing.Size(707, 314);
      this.pnl_data.Paint += new System.Windows.Forms.PaintEventHandler(this.pnl_data_Paint);
      // 
      // splitContainer1
      // 
      this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
      this.splitContainer1.IsSplitterFixed = true;
      this.splitContainer1.Location = new System.Drawing.Point(0, 55);
      this.splitContainer1.Name = "splitContainer1";
      this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
      // 
      // splitContainer1.Panel1
      // 
      this.splitContainer1.Panel1.BackColor = System.Drawing.Color.CornflowerBlue;
      // 
      // splitContainer1.Panel2
      // 
      this.splitContainer1.Size = new System.Drawing.Size(707, 314);
      this.splitContainer1.SplitterDistance = 25;
      this.splitContainer1.TabIndex = 1;
      // 
      // lbl_gaming_table_sel_title
      // 
      this.lbl_gaming_table_sel_title.BackColor = System.Drawing.Color.Transparent;
      this.lbl_gaming_table_sel_title.Font = new System.Drawing.Font("Montserrat", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_gaming_table_sel_title.ForeColor = System.Drawing.Color.White;
      this.lbl_gaming_table_sel_title.Location = new System.Drawing.Point(3, 5);
      this.lbl_gaming_table_sel_title.Name = "lbl_gaming_table_sel_title";
      this.lbl_gaming_table_sel_title.Size = new System.Drawing.Size(512, 16);
      this.lbl_gaming_table_sel_title.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.lbl_gaming_table_sel_title.TabIndex = 2;
      this.lbl_gaming_table_sel_title.Text = "xGamingTableSelect";
      // 
      // bt_ok
      // 
      this.bt_ok.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.bt_ok.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.bt_ok.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.bt_ok.FlatAppearance.BorderSize = 0;
      this.bt_ok.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.bt_ok.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.bt_ok.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.bt_ok.Image = null;
      this.bt_ok.Location = new System.Drawing.Point(538, 239);
      this.bt_ok.Name = "bt_ok";
      this.bt_ok.Size = new System.Drawing.Size(155, 60);
      this.bt_ok.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.bt_ok.TabIndex = 0;
      this.bt_ok.Text = "XOK";
      this.bt_ok.UseVisualStyleBackColor = false;
      this.bt_ok.Click += new System.EventHandler(this.bt_ok_Click);
      // 
      // uc_gaming_table_filter1
      // 
      this.uc_gaming_table_filter1.BackColor = System.Drawing.Color.Gainsboro;
      this.uc_gaming_table_filter1.Location = new System.Drawing.Point(10, 12);
      this.uc_gaming_table_filter1.Margin = new System.Windows.Forms.Padding(2);
      this.uc_gaming_table_filter1.Name = "uc_gaming_table_filter1";
      this.uc_gaming_table_filter1.Size = new System.Drawing.Size(685, 213);
      this.uc_gaming_table_filter1.TabIndex = 0;
      this.uc_gaming_table_filter1.GamingTableSelected += new WSI.Cashier.uc_gaming_table_filter.GamingTableSelectedEventHandler(this.uc_gaming_table_filter1_GamingTableSelected_1);
      // 
      // btn_close
      // 
      this.btn_close.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_close.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_close.FlatAppearance.BorderSize = 0;
      this.btn_close.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_close.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_close.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_close.Image = null;
      this.btn_close.Location = new System.Drawing.Point(369, 239);
      this.btn_close.Name = "btn_close";
      this.btn_close.Size = new System.Drawing.Size(155, 60);
      this.btn_close.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_close.TabIndex = 1;
      this.btn_close.Text = "XCLOSE/CANCEL";
      this.btn_close.UseVisualStyleBackColor = false;
      this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
      // 
      // frm_gaming_table_select
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(707, 369);
      this.Controls.Add(this.splitContainer1);
      this.Name = "frm_gaming_table_select";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Style = WSI.Cashier.Controls.frm_base.BaseFormStyle.PRINCIPAL;
      this.Text = "Gambling Table Select";
      this.Shown += new System.EventHandler(this.frm_gaming_table_select_Shown);
      this.Controls.SetChildIndex(this.splitContainer1, 0);
      this.Controls.SetChildIndex(this.pnl_data, 0);
      this.pnl_data.ResumeLayout(false);
      this.splitContainer1.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.SplitContainer splitContainer1;
    private WSI.Cashier.Controls.uc_label lbl_gaming_table_sel_title;
    private WSI.Cashier.Controls.uc_round_button btn_close;
    private WSI.Cashier.Controls.uc_round_button bt_ok;
    private uc_gaming_table_filter uc_gaming_table_filter1;

  }
}