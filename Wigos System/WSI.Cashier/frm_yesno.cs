//------------------------------------------------------------------------------
// Copyright � 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_yesno.cs
// 
//   DESCRIPTION: Implements the following classes:
//                1. frm_yesno
//
//        AUTHOR: AJQ
// 
// CREATION DATE: 01-AUG-2007
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 01-AUG-2007 AJQ     First release.
// 02-MAY-2012 JCM     Fixed Bug #265: Fixed Visual Position of disconnected data message over other forms
// 18-SEP-2017 DPC     WIGOS-5268: Cashier & GUI - Icons - Implement
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace WSI.Cashier
{
  public partial class frm_yesno : Controls.frm_base_icon
  {
    #region Attributes

    #endregion

    #region Constructor

    /// <summary>
    /// Constructor method.
    /// 1. Initialize controls.
    /// 2. Initialize control�s resources. (NLS, Images, etc)
    /// </summary>
    public frm_yesno()
    {
      InitializeComponent();
      this.lbl_conn_error.VisibleChanged += new EventHandler(OnTop);
    }

    #endregion

    #region Event Handlers

    private void frm_yesno_VisibleChanged(object sender, EventArgs e)
    {
      // Adjust to frm_container
      foreach (Form _form in Application.OpenForms)
      {
        if (_form.Name == "frm_container")
        {
          this.Size = WindowManager.GetClientAreaSize(_form);
          this.Location = WindowManager.GetClientAreaLocation(_form);
          break;
        }
      }
    }

    #endregion

    #region Private Methods

    private void OnTop(Object sender, EventArgs e)
    {
      //Repaint On top ever time data disconected message appear, ensuring
      //   appears over other forms 
      this.TopMost = this.lbl_conn_error.Visible;
    }

    #endregion

    #region Public Methods

    #endregion
  }
}