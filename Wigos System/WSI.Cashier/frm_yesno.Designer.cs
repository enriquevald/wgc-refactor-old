namespace WSI.Cashier
{
  partial class frm_yesno
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose (bool disposing)
    {
      if ( disposing && ( components != null ) )
      {
        components.Dispose ();
      }
      base.Dispose (disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent ()
    {
      this.pb_error = new System.Windows.Forms.PictureBox();
      this.lbl_conn_error = new WSI.Cashier.Controls.uc_label();
      ((System.ComponentModel.ISupportInitialize)(this.pb_error)).BeginInit();
      this.SuspendLayout();
      // 
      // pb_error
      // 
      this.pb_error.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                  | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.pb_error.Location = new System.Drawing.Point(367, 275);
      this.pb_error.Name = "pb_error";
      this.pb_error.Size = new System.Drawing.Size(192, 153);
      this.pb_error.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
      this.pb_error.TabIndex = 0;
      this.pb_error.TabStop = false;
      this.pb_error.Visible = false;
      // 
      // lbl_conn_error
      // 
      this.lbl_conn_error.AutoSize = true;
      this.lbl_conn_error.Font = new System.Drawing.Font("Microsoft Sans Serif", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_conn_error.ForeColor = System.Drawing.Color.White;
      this.lbl_conn_error.Location = new System.Drawing.Point(37, 80);
      this.lbl_conn_error.Name = "lbl_conn_error";
      this.lbl_conn_error.Size = new System.Drawing.Size(867, 108);
      this.lbl_conn_error.TabIndex = 1;
      this.lbl_conn_error.Text = "DESCONECTADO";
      this.lbl_conn_error.Visible = false;
      // 
      // frm_yesno
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.Color.DimGray;
      this.ClientSize = new System.Drawing.Size(1024, 768);
      this.Controls.Add(this.lbl_conn_error);
      this.Controls.Add(this.pb_error);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
      this.Name = "frm_yesno";
      this.Opacity = 0.8;
      this.ShowIcon = false;
      this.ShowInTaskbar = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
      this.Text = "frm_yesno";
      this.VisibleChanged += new System.EventHandler(this.frm_yesno_VisibleChanged);
      ((System.ComponentModel.ISupportInitialize)(this.pb_error)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    public System.Windows.Forms.PictureBox pb_error;
    public WSI.Cashier.Controls.uc_label lbl_conn_error;

  }
}