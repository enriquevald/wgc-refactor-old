//------------------------------------------------------------------------------
// Copyright � 2007-2010 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_container.cs
// 
//   DESCRIPTION: Implements the following classes:
//                1. frm_container (Main window form)
//
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 01-AUG-2007 AJQ        First release.
// 30-SEP-2010 TJG        Gift Delivery.
// 10-JAN-2012 ACC        Disable Events Watcher_.
// 23-MAR-2012 RCI & AJQ  Add license checker to the Cashier. Only check valid date.
// 28-MAR-2012 MPO        User session: Added the event for each button for set the last activity.
// 02-APR-2012 RCI & MPO  Added event handler OnAutoLogout.
// 13-APR-2012 MPO        Added a commented line for OnAutoLogout. It may be necessary.
// 02-MAY-2012 JCM        Fixed Bug #265: Fixed focus in card reader.
// 25-MAY-2012 SSC        Moved CardData, PlaySession and PlayerTrackingData classes to WSI.Common
// 18-APR-2013 ICS        Changed the machine name by its alias.
// 24-JUL-2013 LEM        Added STATUS field to site jackpot message.
// 25-JUL-2013 DLL        Hide gb_events when show uc_bank.
// 13-AUG-2013 DRV        Added support to show automatic OSK if it's enabled or open it if the keyboard button is clicked.
// 13-SEP-2013 QMP        Added Work Shifts.
// 18-SEP-2013 JRM        Added method to open ticket reprint form, claen up of old ticket re-print functionallity.
// 11-OCT-2013 RMS        Fixed Bug WIG-272: Update Cashier Session Status label.
// 29-OCT-2013 DRV        Added Create Ticket Button (TITO Mode).
// 13-NOV-2013 LEM        Added uc_ticket_create, uc_ticket_promo, uc_tito_card (TITO Mode).
// 30-NOV-2013 JPJ        Added new Print and Ticket Menu (TITO Mode).
// 05-DEC-2013 AMF        Reorder Buttons.
// 07-NOV-2013 CCG & JAB  Fixed Bug #WIGOSTITO-818: Exception occurred when user restart session.
// 11-DEC-2013 ICS        Added support for 'Work Shifts' in TITO controls
// 13-JAN-2014 JPJ        Refactored TITO floating menu and calls.
// 24-APR-2014 RMS        Fixed Bug WIGOSTITO-1208: Cashier doesn't update session status when cash session is closed from GUI 
// 17-JUL-2014 DHA        Added table mode.
// 14-AUG-2014 DCS        Table mode in max resolution
// 18-SEP-2014 LMRS       Added button "others"
// 18-SEP-2014 DLL        Cash advance change button texto to others
// 11-MAR-2015 YNM        Fixed Bug WIG-2136: Cage Concepts: Update Concepts changes on Cashier login.
// 26-JUN-2015 FAV        Fixed Bug about the location of "Options" button (frm_container.Designer.cs)
// 16-OCT-2015 ETP        Product Backlog Item-4690 adding new gaming hall mode and refactoring frm_container.
// 23-NOV-2015 ETP        PBI 6902: Set invisible events for uc_ticket_terminal1 
// 25-NOV-2015 FOS        Product Backlog Item 5852:New design: Optimize frm_container
// 12-DEC-2015 YNM        PBI 4695: Gaminghall Configurations
// 15-DIC-2015 YNM        PBI 7503: Visits/Reception Trinidad y Tobago: search screen     
// 17-DIC-2015 YNM        Fixed Bug 7816: Errors with reception module buttom
// 14-JAN-2015 ETP        Change selectable property for some buttons
// 28-JAN-2016 YNM        Fixed Bug 8731: btn "Reception" does not appear when is not enable the funcionality
// 09-FEB-2015 JBC        PBI 7147: SmartFloor license. Now the system allow to check license features.
// 22-FEB-2016 YNM        Fixed Bug 9686: btn "Reception" does not appear in cashier menu when system mode is GamingHall
// 11-MAR-2016 FAV        Fixed Bug 10063: Added permission for gaming hall
// 24-MAR-2016 ETP        Fixed Bug 10980: Bank refresh error when Handpay or fill
// 04-APR-2016 DHA        Fixed Bug 9791 & 11318: error calling refresh thread data
// 19-AUG-2016 RGR        Bug 15421: Cashier: Button Cancel last operation of Concepts cage enabled on closing
// 07-SEP-2016 DHA        Product Backlog Item 15912: resize for differents windows size
// 08-SEP-2016 FOS        PBI 16567: Tables: Split PlayerTracking to GamingTables (Phase 5)
// 31-OCT-2016 JML        Fixed Bug 19865: Gamming tables stock control - not displayed in the cashier
// 31-OCT-2016 JML        Fixed Bug 19866: Chips shange control - not displayed in the cashier
// 11-NOV-2016 ESE        PBI 19932: Add permisions to Gaming table menu button
// 23-NOV-2016 FGB&FOS    PBI 19199: Reopen cashier sessions and gambling tables sessions
// 07-DEC-2016 JBP        Bug 21323:Cajero - No debe permitir entrar en el formulario de cambio de pin en modo card login si el usuario no dispone de tarjeta de empleado.
// 12-JAN-2017 ETP        Fixed Bug 22634: Controls are not enabled if session has been disabled by Expiration.
// 31-JAN-2017 CCG        Fixed Bug 23924:Cajero: No es posible cerrar caja cuando se tiene una recaudaci�n pendiente de BM y el cajero esta fuera de turno.
// 16-FEB-2017 ATB        Add logging system to the shutdowns to control it
// 17-MAR-2017 ETP        WIGOS-194: Credit line: Add logic to avoid withdraw with debt.
// 17-MAY-2017 DHA        Bug 27507: Changes on cashier update/ expired license design
// 06-JUN-2017 DHA        PBI 27760:WIGOS-257: MES21 - Gaming table chair assignment with card selection
// 04-JUL-2017 DHA        Bug 28535:WIGOS-3186 - [Ticket #6351] Lock of table controls when exceeding the working day (general parameter 'GamingDay'), in not integrated cashiers.
// 20-JUL-2017 LTC        Bug 28887:WIGOS-3638 Error when trying to change user's password in Cashier
// 07-JUL-2017 ATB        Bug 29230:WIGOS-4267 The user is able to use the tab "Otras operac." when the cashier is out of the gaming day
// 07-SEP-2017 DHA        Bug 29625:WIGOS-4653 Wrong highlight tab when the user press the button "Pago de tickets" from the tab "Cuentas"
// 18-SEP-2017 DPC        WIGOS-5268: Cashier & GUI - Icons - Implement
// 02-MAR-2018 EOR        Bug 31785: WIGOS-8536 The cashier is not displaying the login box when a cashier user try to access to the "Reception" tab without have the proper permit 
// 14-JUN-2018 AGS        Bug 33045: WIGOS-12920 Cashier - To open session doesn't refresh cashier
// 03-JUL-2018 MS         Bug 33458: WIGOS-13349 Cash > redeem tickets is not starting as anonymous
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Globalization;
using WSI.Common;
using WSI.Common.TITO;
using System.Data.SqlClient;
using System.Diagnostics;
using Microsoft.Win32;
using WSI.Cashier.MVC.Controller.TestMVC;
using WSI.Cashier.MVC.Model.TestMVC;
using WSI.Cashier.MVC.View.TestMVC;
using WSI.Cashier.TITO;
using System.Threading;
using WSI.Cashier.Controls;

namespace WSI.Cashier
{
  public partial class frm_container : frm_base_icon
  {
    #region " Constants "

    private const string BUTTON_OPTIONS_NAME = "BTN_OPTIONS";

    #endregion

    #region Class Fields

    Boolean first_open_gift_delivery = true;

    //DCS 19-NOV-2015: Convert the timer on an asynchronous thread
    private SystemInfo m_system_info;
    private IDScanner.IDScanner m_id_scanner;

    static frm_yesno shader_disconnected = new frm_yesno();

    const int MIN_RESOLUTION_WIDTH = 1024;
    const int MIN_RESOLUTION_HEIGTH = 768;

    const int MIN_RESOLUTION_SHADER_WIDTH = 867;
    const int MIN_RESOLUTION_SHADER_HEIGHT = 108;

    #endregion Class Fields

    #region Constructor

    public frm_container()
    {
      Boolean _reception_enabled;
      String _error_str;
      RegistryKey _cashier;
      RegistryKey _common;

      InitializeComponent();
      ChangeLabelStatus();
      Application.DoEvents();

      if (GeneralParam.GetBoolean("IDCardScanner", "Enabled"))
      {
        m_id_scanner = new IDScanner.IDScanner();
        m_id_scanner.RefreshAllConnectors();
        m_id_scanner.InitializeAll();
      }
      else
      {
        m_id_scanner = null;
      }

      uc_card1.IDScanner = m_id_scanner;
      uc_tito_card1.IDScanner = m_id_scanner;
      uc_ticket_create1.IDScanner = m_id_scanner;
      uc_options1.IDScanner = m_id_scanner;

      CreateMenu();

      _reception_enabled = GeneralParam.GetBoolean("Reception", "Enabled", false);
      switch (Misc.SystemMode())
      {
        case SYSTEM_MODE.MICO2:
        case SYSTEM_MODE.CASHLESS:
          SetConfigCashlessUC();
          break;
        case SYSTEM_MODE.TITO:
        case SYSTEM_MODE.WASS:
          //ContractMenu();
          SetConfigTitoUC();
          break;
        case SYSTEM_MODE.GAMINGHALL:
          //ContractMenu();
          SetConfigGamingHallUC();
          break;
        default:
          Log.Error("frm_container() SYSTEM_MODE not definet: " + Misc.SystemMode());
          break;
      }

      uc_bank1.SetParent(this);
      ShowOnly("none");
      splitContainer2.SplitterDistance = 55;
      //splitContainer2.Panel1.BackColor = WSI.Cashier.CashierStyle.Colors.m_colors_dic[CashierStyle.Colors.ENUM_COLORS.HEX_6B83A1];

      FtpVersions.OnNewVersionAvailable += new FtpVersions.NewVersionAvailableDelegate(FtpVersions_OnNewVersionAvailable);
      FtpVersions.OnNewVersionDetected += new FtpVersions.NewVersionAvailableDelegate(FtpVersions_OnNewVersionDetected);

      // Initialize thread to check for new software versions
      FtpVersions.Init(ENUM_TSV_PACKET.TSV_PACKET_WIN_CASHIER, VersionControl.ClientId, VersionControl.BuildId);

      // RCI 23-MAR-2012: Check license only by valid date.
      Misc.StartLicenseChecker(OnLicenseStatusChanged, true, new List<string>());

      // RCI & MPO 02-APR-2012
      Users.AutoLogout += new Users.AutoLogoutEventHandler(OnAutoLogout);

      // MPO 27-MAR-2012
      EventLastAction.AddEventLastAction(this.splitContainer1.Panel1.Controls);

      if (WindowManager.StartUpMode == WindowManager.StartUpModes.CashDesk)
      {
        Cashier.SetDesktopWallpaper(Cashier.WallpaperAction.Running);
      }

      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_container", Log.Type.Message);
      this.Show();

      // Show login form
      frm_user_login login_screen = new frm_user_login(frm_user_login.LOGIN_MODE_REGULAR);
      login_screen.Show(this);
      login_screen.Dispose();

      // Set initial focus
      InitFocusInVisibleDisplay();

      // DHA 11-JUL-2014: When is table mode, load by default player tracking
      if (WindowManager.IsTableMode && GamingTableBusinessLogic.IsEnabledGTPlayerTracking())
      {
        btn_player_tracking_Click(null, null);
      }

      if (WindowManager.IsReceptionMode && _reception_enabled)
      {
        _cashier = null;
        _common = null;

        _cashier = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\LK_SYSTEM\CASHIER", true);

        if (_cashier == null)
        {
          _cashier = Registry.CurrentUser.CreateSubKey(@"Software\LK_SYSTEM\CASHIER");
          _common = _cashier.CreateSubKey("Common");
        }
        else
        {
          _common = _cashier.OpenSubKey("Common", true);
          if (_common == null)
          {
            _common = _cashier.CreateSubKey("Common");
          }
        }

        //SET DEFAULT UC VISIBLE
        if (!uc_bank1.Visible)
        {
          this.btn_bank_Click(null, null);
        }

        if (ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.CustomersReceptionSearch,
                                  ProfilePermissions.TypeOperation.OnlyCheck,
                                  this,
                                  out _error_str))
        {
          StartReceptionMode();
          _common.SetValue("EnableReceptionBtn", "true", RegistryValueKind.String);
        }
      }

      RefreshDataThread m_thread = new RefreshDataThread();
      m_thread.Init(SystemInfo);
      m_thread.GamingDayStatusChanged += m_thread_GamingDayStatusChanged;
      m_thread.WorkShiftExpiredStatusChanged += m_thread_WorkShiftExpiredStatusChanged;
      m_thread.EnableReceptionButtonChanged += m_thread_EnableReceptionButtonChanged;

      WGDB.OnStatusChanged += WGDB_OnStatusChanged;

    }

    #endregion Constructor

    #region Private Methods

    private void ContractMenu()
    {
      foreach (Control _ctrl in splitContainer1.Panel1.Controls)
      {
        if (_ctrl.Name.Contains("label"))
        {
          _ctrl.Height = 0;
        }
      }
    }

    private void ShowOnly(String DisplayName)
    {
      uc_bank1.Visible = (DisplayName == "bank");
      uc_mobile_bank1.Visible = (DisplayName == "mobile_bank");
      uc_options1.Visible = (DisplayName == "options");
      uc_gift_delivery1.Visible = (DisplayName == "gift_delivery");
      uc_gt_player_tracking1.Visible = (DisplayName == "player_tracking");
      uc_concept_operations1.Visible = (DisplayName == "uc_concept_operations");

      uc_ticket_terminal1.Visible = (DisplayName == "ticket_terminal");

      switch (Misc.SystemMode())
      {
        case SYSTEM_MODE.CASHLESS:
        case SYSTEM_MODE.MICO2:
          uc_card1.Visible = (DisplayName == "card");
          break;
        case SYSTEM_MODE.TITO:
        case SYSTEM_MODE.WASS:
          uc_tito_card1.Visible = (DisplayName == "card");
          uc_ticket_create1.Visible = (DisplayName == "ticket_create");
          uc_ticket_view.Visible = (DisplayName == "ticket_view");
          uc_ticket_validation1.Visible = (DisplayName == "tickets");
          uc_ticket_offline1.Visible = (DisplayName == "ticket_offline");
          uc_ticket_undo1.Visible = (DisplayName == "undo_ticket");
          uc_ticket_promo1.Visible = (DisplayName == "ticket_promo");
          break;
        case SYSTEM_MODE.GAMINGHALL:
          this.uc_gaming_hall1.Visible = (DisplayName == "gaming_hall_collect");
          this.uc_gaming_hall_set_pcd_meters1.Visible = (DisplayName == "gaming_hall_set_pcd_meters");
          break;
      }

      // Read Cashier Status (Open/Close)
      ChangeLabelStatus();

      EnableDisableMobileBankButtonStatusChanged();

      return;
    } // ShowOnly

    private void InitFocusInVisibleDisplay()
    {
      ChangeLabelStatus();

      if (uc_ticket_create1.Visible)
      {
        uc_ticket_create1.Focus();
        uc_ticket_create1.InitFocus();

        return;
      }

      if (uc_ticket_validation1.Visible)
      {
        uc_ticket_validation1.Focus();
        uc_ticket_validation1.InitFocus();

        return;
      }

      if (uc_ticket_offline1.Visible)
      {
        uc_ticket_offline1.Focus();
        uc_ticket_offline1.InitFocus();

        return;
      }

      if (uc_tito_card1.Visible)
      {
        uc_tito_card1.Focus();
        uc_tito_card1.InitFocus();

        return;
      }

      if (uc_ticket_promo1.Visible)
      {
        uc_ticket_promo1.Focus();
        uc_ticket_promo1.InitFocus();

        return;
      }

      if (uc_ticket_view.Visible)
      {
        uc_ticket_view.Focus();
        uc_ticket_view.InitFocus();

        return;
      }

      if (uc_ticket_undo1.Visible)
      {
        uc_ticket_undo1.Focus();
        uc_ticket_undo1.InitFocus();

        return;
      }

      if (uc_card1.Visible)
      {
        uc_card1.Focus();
        uc_card1.InitFocus();

        return;
      }

      if (uc_mobile_bank1.Visible)
      {
        uc_mobile_bank1.Focus();
        uc_mobile_bank1.InitFocus();

        return;
      }

      if (uc_bank1.Visible)
      {
        uc_bank1.Focus();
        uc_bank1.RefreshStatus();

        return;
      }

      if (uc_options1.Visible)
      {
        uc_options1.Focus();

        return;
      }

      if (uc_gift_delivery1.Visible)
      {
        uc_gift_delivery1.Focus();
        uc_gift_delivery1.OnlyInitFocus();

        return;
      }

      if (uc_gt_player_tracking1.Visible)
      {
        uc_gt_player_tracking1.Focus();

        return;
      }

      if (uc_ticket_terminal1.Visible)
      {
        uc_ticket_terminal1.Focus();
        uc_ticket_terminal1.InitFocus();

        return;
      }
      if (uc_gaming_hall1.Visible)
      {
        uc_gaming_hall1.Focus();

      }

    }

    /// <summary>
    /// configure uc in cashless mode
    /// </summary>
    private void SetConfigCashlessUC()
    {
      uc_card1.Visible = true;
      uc_bank1.Visible = true;
      uc_options1.Visible = true;

      uc_tito_card1.Visible = false;
      uc_ticket_create1.Visible = false;
      uc_ticket_promo1.Visible = false;

      uc_ticket_validation1.Visible = false;
      uc_ticket_view.Visible = false;
      uc_ticket_offline1.Visible = false;

      uc_ticket_undo1.Visible = false;
      InitDocStyleUC();
      uc_card1.InitControls(this);
      uc_options1.InitControls(this);
    }

    /// <summary>
    /// configure uc in tito mode
    /// </summary>
    private void SetConfigTitoUC()
    {
      uc_tito_card1.Visible = true;
      uc_ticket_create1.Visible = true;
      uc_ticket_promo1.Visible = true;

      uc_ticket_validation1.Visible = true;
      uc_ticket_view.Visible = true;
      uc_ticket_offline1.Visible = true;

      uc_ticket_undo1.Visible = true;
      uc_bank1.Visible = true;
      uc_options1.Visible = true;

      uc_card1.Visible = false;
      InitDocStyleUC();
      uc_tito_card1.InitControls(this);
      uc_options1.InitControls(this);
    }

    /// <summary>
    /// configure uc in Gaming Hall mode
    /// </summary>
    private void SetConfigGamingHallUC()
    {
      uc_bank1.Visible = true;
      uc_options1.Visible = true;

      uc_tito_card1.Visible = false;
      uc_ticket_create1.Visible = false;
      uc_ticket_promo1.Visible = false;

      uc_ticket_validation1.Visible = false;
      uc_ticket_view.Visible = false;
      uc_ticket_offline1.Visible = false;

      uc_ticket_undo1.Visible = false;
      uc_card1.Visible = false;
      InitDocStyleUC();
      uc_options1.InitControls(this);
    }

    /// <summary>
    /// Inicialize docstyle for all modes.
    /// </summary>
    private void InitDocStyleUC()
    {
      uc_tito_card1.Dock = DockStyle.Fill;
      uc_ticket_create1.Dock = DockStyle.Fill;
      uc_ticket_promo1.Dock = DockStyle.Fill;
      uc_ticket_view.Dock = DockStyle.Fill;
      uc_ticket_validation1.Dock = DockStyle.Fill;
      uc_ticket_offline1.Dock = DockStyle.Fill;
      uc_ticket_undo1.Dock = DockStyle.Fill;
      uc_ticket_terminal1.Dock = DockStyle.Fill;

      uc_card1.Dock = DockStyle.Fill;
      uc_bank1.Dock = DockStyle.Fill;
      uc_mobile_bank1.Dock = DockStyle.Fill;
      uc_options1.Dock = DockStyle.Fill;
      uc_gift_delivery1.Dock = DockStyle.Fill;

      // 03-OCT-2014 SMN
      uc_concept_operations1.Dock = DockStyle.Fill;

      uc_gaming_hall1.Dock = DockStyle.Fill;
      uc_gaming_hall_set_pcd_meters1.Dock = DockStyle.Fill;

      uc_gt_player_tracking1.Dock = DockStyle.Fill;
    }

    private void UcConceptOperationsCenter()
    {
      int _left;

      // Resize uc_concept_operations in splitContainer2
      _left = (splitContainer2.Size.Width - uc_concept_operations1.Size.Width) / 2;
      uc_concept_operations1.Left = _left;
    }

    #endregion

    #region Public Methods

    public SystemInfo SystemInfo
    {
      get
      {
        if (m_system_info == null)
        {
          m_system_info = new SystemInfo();
        }
        return m_system_info;
      }
      set
      {
        m_system_info = value;
      }
    }

    public void ChangeLabelStatus()
    {
      if (!uc_top_bar.GetInfo())
      {
        uc_top_bar.SystemInfo = SystemInfo;
        uc_top_bar.GetInfo();
      }
    }

    public void OnAutoLogout(String Title, String Message)
    {

      if (this.InvokeRequired)
      {
        this.Invoke(new Users.AutoLogoutEventHandler(OnAutoLogout), new object[] { Title, Message });
      }
      else
      {
        Form _form;

        _form = System.Windows.Forms.Form.ActiveForm;
        //Get last opened form
        if (Application.OpenForms.Count > 0)
        {
          _form = Application.OpenForms[Application.OpenForms.Count - 1];
        }
        if (frm_message.ShowTimerWithCancel(Message, Title, "", 60, "s", MessageBoxIcon.Warning, _form) != DialogResult.Cancel)
        {
          // ATB 16-FEB-2017
          Misc.WriteLog("[RESTART] REASON: Expiration due inactivity", Log.Type.Message);
          Cashier.Shutdown(false, true, Users.EXIT_CODE.EXPIRED);
        }
        else
        {
          WSI.Common.Users.SetLastAction("Continue", "");
          if (_form != null)
          {
            _form.Focus();
          }
          //Ensure focus is in the card reader after cancelling the session timeout countdown.
          InitFocusInVisibleDisplay();
        }

      }

    }

    //------------------------------------------------------------------------------
    // PURPOSE : Calculate size form form with some configuration modes
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public void UpdateSize()
    {
      Int32 _width;
      Int32 _heigth;
      Int32 _max_allowed_width;
      Int32 _max_allowed_heigth;
      Int32 _win_task_bar_height;
      Int32 _win_task_bar_width;

      _width = MIN_RESOLUTION_WIDTH;
      _heigth = MIN_RESOLUTION_HEIGTH;

      //Get windows task bar size
      _win_task_bar_height = Screen.PrimaryScreen.Bounds.Height - Screen.PrimaryScreen.WorkingArea.Height;
      _win_task_bar_width = Screen.PrimaryScreen.Bounds.Width - Screen.PrimaryScreen.WorkingArea.Width;

      // Get Primary Screen resolution
      if (WindowManager.IsTableMode || WindowManager.StartUpMode == WindowManager.StartUpModes.CashDesk)
      {
        _width = Screen.PrimaryScreen.Bounds.Width;
        _heigth = Screen.PrimaryScreen.Bounds.Height;
      }
      else
      {
        Size _size = WindowManager.CashierWindowSize;

        _width = _size.Width;
        _heigth = _size.Height;
      }

      // Check allowed Max screen resolution
      _max_allowed_width = _width;
      _max_allowed_heigth = _heigth;

      if (!WindowManager.IsTableMode)
      {
        _max_allowed_width = GeneralParam.GetInt32("Cashier", "MaxScreenResolutionWidth", _width);
        _max_allowed_heigth = GeneralParam.GetInt32("Cashier", "MaxScreenResolutionHeigth", _heigth);
      }

      if ((_heigth > _max_allowed_heigth) || (_width > _max_allowed_width))
      {
        _width = _max_allowed_width;
        _heigth = _max_allowed_heigth;
      }

      this.SuspendLayout();
      this.MaximumSize = new Size(0, 0);
      this.WindowState = FormWindowState.Normal;
      this.MaximizeBox = false;
      this.MinimizeBox = true;
      this.Size = new Size(_width, _heigth);

      if (WindowManager.IsWindowMode)
      {
        this.FormBorderStyle = FormBorderStyle.FixedSingle;

        if (WindowManager.IsTableMode)
        {
          _width = _width - (2 * WindowManager.GetMarginWidth(this)) - _win_task_bar_width;
          _heigth = _heigth - (2 * WindowManager.GetMarginWidth(this)) - _win_task_bar_height;
        }
        else
        {
          _width += 2 * WindowManager.GetMarginWidth(this);
          _heigth += WindowManager.GetTittleBarHeight(this) + WindowManager.GetMarginWidth(this);
        }

        this.Location = new Point(WindowManager.GetMarginWidth(this), WindowManager.GetMarginWidth(this));
      }
      else
      {
        this.FormBorderStyle = FormBorderStyle.None;
        this.Location = new Point(0, 0);
      }

      // Adjust Min resolution (even if window form is higher to screen resolution)
      if ((_width < MIN_RESOLUTION_WIDTH) || (_heigth < MIN_RESOLUTION_HEIGTH))
      {
        _width = MIN_RESOLUTION_WIDTH;
        _heigth = MIN_RESOLUTION_HEIGTH;
      }

      this.Size = new Size(_width, _heigth);

      uc_menu.Visible = !WindowManager.IsTableMode;
      UcConceptOperationsCenter();

      this.ResumeLayout();
    }


    /// <summary>
    /// Acces to ticket redeem form.
    /// </summary>
    public void ticket_redeem_Click()
    {
      uc_menu.ForceButtonClick("BTN_REDEEM_TICKET");

      btn_ticket_redeem_Click(null, null);
    }

    #endregion

    #region Menu Handling

    #region Menu Items Handling

    //------------------------------------------------------------------------------
    // PURPOSE: Print last voucher.
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    private void LastVoucherReprint()
    {
      frm_vouchers_reprint _vouchers_reprint;
      String error_str;

      try
      {
        // Check if current user is an authorized user
        if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.VoucherHistory,
                                                 ProfilePermissions.TypeOperation.RequestPasswd,
                                                 this,
                                                 out error_str))
        {
          return;
        }

        _vouchers_reprint = new frm_vouchers_reprint();
        _vouchers_reprint.Show(this);
        _vouchers_reprint.Dispose();
      }
      finally
      {
        InitFocusInVisibleDisplay();
      }
    } // LastVoucherReprint

    //------------------------------------------------------------------------------
    // PURPOSE : automatic print button click
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void AutomaticPrint()
    {
      String error_str;

      // Check if current user is an authorized user
      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.AutomaticPrintMobileBank,
                                               ProfilePermissions.TypeOperation.RequestPasswd,
                                               this,
                                               out error_str))
      {
        return;
      }

      frm_automatic_print.Show(Cashier.MainForm);

      UserLogOff(false);

    } // AutomaticPrint

    //------------------------------------------------------------------------------
    // PURPOSE: Ticket reprint
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    private void TicketReprint(object sender, EventArgs e)
    {
      frm_tickets_reprint _tickets_reprint;
      String error_str;

      try
      {
        // Check if current user is an authorized user
        if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.VoucherHistory,
                                                 ProfilePermissions.TypeOperation.RequestPasswd,
                                                 this,
                                                 out error_str))
        {
          return;
        }

        _tickets_reprint = new frm_tickets_reprint();

        _tickets_reprint.Show(this);
        _tickets_reprint.Dispose();
      }
      finally
      {
        InitFocusInVisibleDisplay();
      }
    } // TicketReprint

    //------------------------------------------------------------------------------
    // PURPOSE: Button click to show tickets
    //
    //  PARAMS:
    //      - INPUT:
    //          - Sender
    //          - EventArgs
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    private void ShowTickets(object sender, EventArgs e)
    {
      ShowOnly("tickets");

      uc_ticket_validation1.InitControls(this);
      uc_ticket_validation1.Focus();
      uc_ticket_validation1.InitFocus();
    } // ShowTickets

    //------------------------------------------------------------------------------
    // PURPOSE: Button click to show tickets
    //
    //  PARAMS:
    //      - INPUT:
    //          - Sender
    //          - EventArgs
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    private void UndoTicketOperation(object sender, EventArgs e)
    {
      ShowOnly("undo_ticket");

      uc_ticket_undo1.InitControls(this);
      uc_ticket_undo1.Focus();
      uc_ticket_undo1.InitFocus();
    } // ShowTickets

    #endregion

    #endregion

    #region Button Handling

    public void btn_cards_Click(object sender, EventArgs e)
    {
      ShowOnly("card");

      if (BusinessLogic.IsModeTITO && !Misc.IsMico2Mode())
      {
        uc_tito_card1.InitControls(this);
        uc_tito_card1.Focus();
        uc_tito_card1.InitFocus();
        uc_tito_card1.ClearCardData();
      }
      else
      {
        uc_card1.InitControls(this);
        uc_card1.Focus();
        uc_card1.InitFocus();
      }
    } // btn_cards_Click

    public void btn_mobile_banks_Click(object sender, EventArgs e)
    {
      String _error_str;

      // Check if current user is an authorized user
      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.MobileBank,
                                                  ProfilePermissions.TypeOperation.RequestPasswd,
                                                  this,
                                                  out _error_str))
      {
        return;
      }

      ShowOnly("mobile_bank");

      uc_mobile_bank1.InitControls(this);
      uc_mobile_bank1.Focus();
      uc_mobile_bank1.InitFocus();
    } // btn_mobile_banks_Click

    public void SwitchToMobileBanck(String Trackdata)
    {
      Boolean _is_valid;

      btn_mobile_banks_Click(null, null);

      uc_menu.ForceButtonClick("BTN_MOBILE_BANK");

      _is_valid = true;

      uc_mobile_bank1.uc_card_reader1_OnTrackNumberReadEvent(Trackdata, ref _is_valid);
    }

    public void SwitchToGamingTables(Int32 GamingTableId, CardData CardData, Ticket TicketCopyDealer)
    {
      if (FeatureChips.IsGamingTablesEnabled)
      {
        btn_player_tracking_Click(null, null);

        uc_menu.ForceButtonClick("BTN_PLAYER_TRACKING");

        uc_gt_player_tracking1.AutomaticCopyDealervalidation(GamingTableId, CardData, TicketCopyDealer);
      }
    } // SwitchToGamingTables

    public void SwitchToCard(String Trackdata)
    {
      Boolean _is_valid;

      btn_cards_Click(null, null);

      uc_menu.ForceButtonClick("BTN_CARDS");

      _is_valid = true;

      if (BusinessLogic.IsModeTITO)
      {
        uc_tito_card1.uc_card_reader1_OnTrackNumberReadEvent(Trackdata, ref _is_valid);
      }
      else
      {
        uc_card1.uc_card_reader1_OnTrackNumberReadEvent(Trackdata, ref _is_valid);
      }
    }

    public void btn_gifts_Click(object sender, EventArgs e)
    {
      String _error_str;

      // Check if current user is an authorized user
      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.GiftDelivery,
                                                  ProfilePermissions.TypeOperation.RequestPasswd,
                                                  this,
                                                  out _error_str))
      {
        InitFocusInVisibleDisplay();

        return;
      }

      // Hide all other displays but this one
      ShowOnly("gift_delivery");

      if (first_open_gift_delivery)
      {
        uc_gift_delivery1.InitControls();
        first_open_gift_delivery = false;
      }

      //uc_gift_delivery1.InitControls();
      uc_gift_delivery1.Focus();
      uc_gift_delivery1.InitFocus();

      return;
    } // btn_gifts_Click

    public void btn_bank_Click(object sender, EventArgs e)
    {
      String _error_str;

      // Check if current user is an authorized user
      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.AccessCash,
                                                  ProfilePermissions.TypeOperation.RequestPasswd,
                                                  this,
                                                  out _error_str))
      {
        InitFocusInVisibleDisplay();

        return;
      }

      ShowOnly("bank");

      uc_bank1.RefreshStatus();
    } // btn_bank_Click

    public void btn_ticket_offline_Click(object sender, EventArgs e)
    {
      ShowOnly("ticket_offline");

      uc_ticket_offline1.InitControls();
      uc_ticket_offline1.Focus();
      uc_ticket_offline1.InitFocus();
    } // btn_bank_Click

    public void btn_player_tracking_Click()
    {
      ShowOnly("player_tracking");

      uc_gt_player_tracking1.InitControls(this);
      uc_gt_player_tracking1.Focus();
    } // btn_player_tracking_Click

    private void btn_shutdown_Click()
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[PROMPTED SHUTDOWN] REASON: User action (from container form", Log.Type.Message);
      Cashier.Shutdown(true);

      InitFocusInVisibleDisplay();
    }

    private void btn_change_password_Click()
    {
      frm_pwd_change _change_pwd;
      _change_pwd = new frm_pwd_change(Cashier.UserId, Cashier.UserName, UserLogin.PasswordChange.PWD_CHANGE_REQUESTED); // LTC 2017-JUL-20

      _change_pwd.Show(this);
      _change_pwd.Dispose();
    }

    private void btn_change_pin_Click()
    {
      UserCardStatus _card_status;

      using (frm_yesno _shader = new frm_yesno())
      {
        using (frm_change_pin _change_pin = new frm_change_pin(_shader))
        {
          _shader.Opacity = 0.6f;
          _shader.Show();

          if (!_change_pin.GetUserCardData(Cashier.UserId, out _card_status))
          {
            frm_message.Show(Resource.String("STR_APP_USER_NO_CARD_ASIGNED_MSG_ERROR"),
                             Resource.String("STR_APP_GEN_MSG_ERROR"),
                             MessageBoxButtons.OK,
                             MessageBoxIcon.Error,
                             this.ParentForm);

            return;
          }

          if (_card_status != UserCardStatus.NoPin)
          {
            // Card login (Request PIN)
            if (!CheckPin(Cashier.UserId))
            {
              return;
            }
          }

          _change_pin.ShowDialog(_shader);
        }
      }

      WSIKeyboard.Hide();
    }

    private void btn_log_off_Click()
    {
      // Log off current user
      UserLogOff(true);
      uc_concept_operations1.Init();
    }

    private void btn_options_Click()
    {
      ShowOnly("options");

      uc_options1.InitControls(this);
      uc_options1.Focus();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : User Log Off function
    //
    //  PARAMS :
    //      - INPUT :
    //          - Question
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void UserLogOff(Boolean Question)
    {
      DialogResult _question_dialog;

      // Log off current user

      // Make the Login screen static and log off from the SQL
      // probably a confirmation message before logging off will be needed
      _question_dialog = DialogResult.OK;
      if (Question)
      {
        _question_dialog = frm_message.Show(Resource.String("STR_FRM_CONTAINER_USER_MSG_LOG_OFF"), Resource.String("STR_FRM_CONTAINER_USER_MSG_LOG_OFF_TITLE"), MessageBoxButtons.OKCancel, Images.CashierImage.LogOff, this);
      }

      if (_question_dialog == DialogResult.OK)
      {
        Cashier.SetUserLoggedOff();
        frm_user_login _login_screen = new frm_user_login(frm_user_login.LOGIN_MODE_REGULAR);

        ChangeLabelStatus();

        ProfilePermissions.ClearBufferPermissions();

        _login_screen.Show(this);
        _login_screen.Dispose();

        // Clear Card Data
        // 21/10/2015 ETP login first page is handpay for gaming hall mode.
        switch (Misc.SystemMode())
        {
          case SYSTEM_MODE.CASHLESS:
          case SYSTEM_MODE.MICO2:
            uc_card1.ClearCardData();
            break;
          case SYSTEM_MODE.TITO:
          case SYSTEM_MODE.WASS:
            uc_tito_card1.ClearCardData();
            break;
          case SYSTEM_MODE.GAMINGHALL:
            break;
          default:
            Log.Error("frm_container.UserLogOff SYSTEM_MODE not definet: " + Misc.SystemMode());
            break;
        }
      }

      InitFocusInVisibleDisplay();
    } // UserLogOff

    private void btn_log_off_Click(object sender, EventArgs e)
    {
      btn_log_off_Click();
    }

    private void btn_shutdown_Click(object sender, EventArgs e)
    {
      btn_shutdown_Click();
    }

    private void btn_change_password_Click(object sender, EventArgs e)
    {
      btn_change_password_Click();
    }

    private void btn_change_pin_Click(object sender, EventArgs e)
    {
      btn_change_pin_Click();
    }

    private void btn_keyboard_Click(object sender, EventArgs e)
    {
      //Shows OSK
      WSIKeyboard.ForceToggle();

      InitFocusInVisibleDisplay();
    }

    private void btn_terminal_Click(object sender, EventArgs e)
    {
      String _error_str;

      // Check if current user is an authorized user
      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.GameSessions,
                                                  ProfilePermissions.TypeOperation.ShowMsg,
                                                  this,
                                                  out _error_str))
      {
        return;
      }

      frm_sessions _form;
      _form = new frm_sessions();
      _form.Show(this);
      _form.Close();
      _form.Dispose();
      _form = null;

      InitFocusInVisibleDisplay();
    }

    private void btn_last_voucher_Click(object sender, EventArgs e)
    {
      LastVoucherReprint();
    } // btn_last_voucher_Click

    private void btn_menu_table_mode_Click(object sender, EventArgs e)
    {
      CreateSubMenuGamingTables(sender, e);
      //CreateMenu(MENU_GROUPS.MENU_TABLE_MODE, this.pnl_menu_table_mode.Top);
    }

    private void btn_jackpot_Click(object sender, EventArgs e)
    {
      TYPE_SITE_JACKPOT_WINNER _winner_info;
      String _message;
      String[] _message_params = { "", "", "", "", "", "", "" };

      if (CardData.CheckWinner(out _winner_info))
      {
        _message_params[0] = _winner_info.awarded_to_account_id.ToString();

        if (!String.IsNullOrEmpty(_winner_info.awarded_to_account_name))
        {
          _message_params[0] += "-" + _winner_info.awarded_to_account_name;
        }

        _message_params[1] = _winner_info.awarded_on_provider_name + " " + _winner_info.awarded_on_terminal_name;
        _message_params[2] = _winner_info.jackpot_name;
        _message_params[3] = _winner_info.amount.ToString("c");

        if (_winner_info.paid)
        {
          _message_params[4] = Resource.String("STR_SITE_JACKPOT_WINNER_MSG_STATE_PAID");
        }
        else
        {
          _message_params[4] = Resource.String("STR_SITE_JACKPOT_WINNER_MSG_STATE_NOT_PAID");
        }

        _message = Resource.String("STR_SITE_JACKPOT_WINNER_MSG_001", _message_params);
        _message = _message.Replace("\\r\\n", "\r\n");

        frm_message.Show(_message,
                         Resource.String("STR_FRM_HANDPAYS_026"),
                         MessageBoxButtons.OK, MessageBoxIcon.Information, Cashier.MainForm);
      }
      else
      {
        frm_message.Show(Resource.String("STR_SITE_JACKPOT_WINNER_MSG_002"),
                         Resource.String("STR_FRM_HANDPAYS_026"),
                         MessageBoxButtons.OK, MessageBoxIcon.Warning, Cashier.MainForm);
      }

      InitFocusInVisibleDisplay();
    }

    private void btn_options_Click(object sender, EventArgs e)
    {
      btn_options_Click();
    }

    private void buttons_EnabledChanged(object sender, EventArgs e)
    {
      System.Windows.Forms.Button _button;

      _button = (System.Windows.Forms.Button)sender;

      if (_button.Enabled)
      {
        _button.BackColor = System.Drawing.Color.LightSkyBlue;
      }
      else
      {
        _button.BackColor = System.Drawing.Color.LightGray;
      }
    }

    // PURPOSE: Event for Ticket (cash-in) creation
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    public void btn_create_ticket_Click(object sender, EventArgs e)
    {
      ShowOnly("ticket_create");

      uc_ticket_create1.InitControls(this);
      uc_ticket_create1.Focus();
      uc_ticket_create1.InitFocus();
    } // btn_create_ticket_Click

    // PURPOSE: Event for Ticket (cash-in) creation
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    private void btn_ticket_promo_Click(object sender, EventArgs e)
    {
      ShowOnly("ticket_promo");

      uc_ticket_promo1.InitControls(this);
      uc_ticket_promo1.Focus();
      uc_ticket_promo1.InitFocus();
    } // btn_create_ticket_Click

    public void ShowTicketPromoAccount(Int64 AccountId)
    {
      ShowOnly("ticket_promo");

      uc_ticket_promo1.InitControls(this, AccountId);
      uc_ticket_promo1.Focus();
      uc_ticket_promo1.InitFocus();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : automatic print button click
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void btn_automatic_print_Click(object sender, EventArgs e)
    {
      AutomaticPrint();
    } // btn_automatic_print_Click

    //------------------------------------------------------------------------------
    // PURPOSE: Button click for redeeming tickets
    //
    //  PARAMS:
    //      - INPUT:
    //          - Sender
    //          - EventArgs
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    private void btn_ticket_redeem_Click(object sender, EventArgs e)
    {
      ShowOnly("ticket_view");

      uc_ticket_view.InitControls(this);
      if (sender != null)
      {
        uc_ticket_view.RefreshCardData(true);
      }
      uc_ticket_view.Focus();
      uc_ticket_view.InitFocus();
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Button click for redeeming tickets
    //
    //  PARAMS:
    //      - INPUT:
    //          - Sender
    //          - EventArgs
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    private void btn_player_tracking_Click(object sender, EventArgs e)
    {
      String _out_msg;
      _out_msg = String.Empty;

      if (ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.Gaming_Tables_Btn, ProfilePermissions.TypeOperation.RequestPasswd, this, out _out_msg))
      {
        btn_player_tracking_Click();
      }
      else
      {
        this.Focus();
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Button click for collecting terminals
    //
    //  PARAMS:
    //      - INPUT:
    //          - Sender
    //          - EventArgs
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    public void btn_collect_Click(object sender, EventArgs e)
    {
      String _error_str;

      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.GAMINGHALL_Collect,
                                                 ProfilePermissions.TypeOperation.RequestPasswd,
                                                 this,
                                                 out _error_str))
      {
        InitFocusInVisibleDisplay();

        return;
      }

      ShowOnly("gaming_hall_collect");
      uc_gaming_hall1.RefreshStatus();
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Button click for fill terminals
    //
    //  PARAMS:
    //      - INPUT:
    //          - Sender
    //          - EventArgs
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    private void btn_fill_terminal_Click(object sender, EventArgs e)
    {
      frm_gaming_hall_refill _gaming_hall_refill;
      String _error_str;

      try
      {
        if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.GAMINGHALL_Refill,
                                                ProfilePermissions.TypeOperation.RequestPasswd,
                                                this,
                                                out _error_str))
        {
          InitFocusInVisibleDisplay();

          return;
        }

        _gaming_hall_refill = new frm_gaming_hall_refill();
        _gaming_hall_refill.Show(this);
        _gaming_hall_refill.Dispose();
      }
      finally
      {
        InitFocusInVisibleDisplay();
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Button click for set PCD meters
    //
    //  PARAMS:
    //      - INPUT:
    //          - Sender
    //          - EventArgs
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    public void btn_set_pcd_meters_Click(object sender, EventArgs e)
    {
      String _error_str;

      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.GAMINGHALL_WriteMeters,
                                                 ProfilePermissions.TypeOperation.RequestPasswd,
                                                 this,
                                                 out _error_str))
      {
        InitFocusInVisibleDisplay();

        return;
      }

      ShowOnly("gaming_hall_set_pcd_meters");
      uc_gaming_hall1.RefreshStatus();
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Button click for handpays.
    //
    //  PARAMS:
    //      - INPUT:
    //          - Sender
    //          - EventArgs
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    private void btn_handpay_Click(object sender, EventArgs e)
    {
      frm_TITO_handpays _handpay;
      String _error_str;

      try
      {
        // Check if current user is an authorized user
        if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.GAMINGHALL_HandPay,
                                                 ProfilePermissions.TypeOperation.RequestPasswd,
                                                 this,
                                                 out _error_str))
        {
          InitFocusInVisibleDisplay();

          return;
        }

        _handpay = new frm_TITO_handpays();
        _handpay.Show(this);
        _handpay.Dispose();
      }
      finally
      {
        InitFocusInVisibleDisplay();
      }
    }

    private void btn_player_others_Click(object sender, EventArgs e)
    {
      String _error_str;

      // Check if current user is an authorized user
      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.OthersAccess,
                                               ProfilePermissions.TypeOperation.RequestPasswd,
                                               this,
                                               out _error_str))
      {
        return;
      }

      ShowOnly("uc_concept_operations");

      if (uc_concept_operations1 != null)
      {
        uc_concept_operations1.SetButtonEnableUndo();
      }
    }

    private void btn_ticket_terminal_Click(object sender, EventArgs e)
    {
      String _error_str;

      // Check if current user is an authorized user
      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.TITO_ListLastBillAndTickets,
                                               ProfilePermissions.TypeOperation.RequestPasswd,
                                               this,
                                               out _error_str))
      {
        return;
      }

      ShowOnly("ticket_terminal");

      uc_ticket_terminal1.InitControls();

      InitFocusInVisibleDisplay();
    }

    #endregion Button Handling

    #region Ftp Event Handling

    //------------------------------------------------------------------------------
    // PURPOSE : Ftp Event Handling: Ask to user if restart application for upgrade software.
    //                               It can have a countdown
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    void FtpVersions_OnNewVersionAvailable(FtpVersions.NewVersionEvent VersionEvent)
    {
      if (Development.Enabled)
      {
        return;
      }

      if (this.InvokeRequired)
      {
        this.Invoke(new FtpVersions.NewVersionAvailableDelegate(FtpVersions_OnNewVersionAvailable), VersionEvent);

        return;
      }

      if (WindowManager.StartUpMode == WindowManager.StartUpModes.CashDesk)
      {
        Cashier.SetDesktopWallpaper(Cashier.WallpaperAction.Updating);
      }

      System.Threading.Thread.Sleep(5000);

      Cashier.RestartApplicationForUpdate();
    }

    private void DisableControl(Control Item)
    {
      try
      {
        Item.Enabled = false;

        foreach (Control _ctrl in Item.Controls)
        {
          DisableControl(_ctrl);
        }
      }
      catch
      {

      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Ftp Event Handling: Ask to user if restart application for upgrade software.
    //                               It can have a countdown
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    void FtpVersions_OnNewVersionDetected(FtpVersions.NewVersionEvent VersionEvent)
    {
      DialogResult _dialog_result;

      if (Development.Enabled)
      {
        return;
      }

      if (this.InvokeRequired)
      {
        // Use of BeginInvoke is mandatory
        this.BeginInvoke(new FtpVersions.NewVersionAvailableDelegate(FtpVersions_OnNewVersionDetected), VersionEvent);

        return;
      }

      foreach (Form _frm in Application.OpenForms)
      {
        DisableControl(_frm);
      }

      while (true)
      {
        String _msg;

        _msg = Resource.String("STR_APP_GEN_MSG_DOWNLOADING_NEW_VERSION");
        _msg = _msg.Replace("\\r\\n", "\r\n");

        _dialog_result = frm_message.Show(_msg,
                                          Resource.String("STR_APP_GEN_MSG_NEW_VERSION_CAPTION"),
                                          "",
                                          60,
                                          Resource.String("STR_APP_GEN_MSG_NEW_VERSION_COUNTDOWN_UNIT_TIME"),
                                          MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Information,
                                          this, true, false);
      }
    }

    #endregion Ftp Event Handling

    #region License Event Handling

    //------------------------------------------------------------------------------
    // PURPOSE : It is called when the license has expired.
    //           Show a pop-up informing the license has expired and restart the cashier.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private void OnLicenseStatusChanged(LicenseStatus LicenseStatus)
    {
      switch (LicenseStatus)
      {
        case LicenseStatus.VALID:
          return;

        case LicenseStatus.VALID_EXPIRES_SOON:
          return;

        case LicenseStatus.NOT_AUTHORIZED:
        case LicenseStatus.INVALID:
        case LicenseStatus.EXPIRED:
        case LicenseStatus.NOT_CHECKED:
        default:
          break;
      }

      if (this.InvokeRequired)
      {
        this.Invoke(new Misc.LicenseStatusChangedDelegate(OnLicenseStatusChanged), new Object[] { LicenseStatus });

        return;
      }

      foreach (Form _frm in Application.OpenForms)
      {
        DisableControl(_frm);
      }
      // License NOT VALID
      frm_message.Show(Resource.String("STR_FRM_CONTAINER_USER_MSG_LICENSE_EXPIRED"), Resource.String("STR_FRM_CONTAINER_USER_MSG_SHUTDOWN_TITLE"),
                         MessageBoxButtons.OK, Images.CashierImage.Shutdown, this, true);
      // ATB 16-FEB-2017
      Misc.WriteLog("[RESTART] REASON: Invalid license", Log.Type.Message);
      Cashier.Shutdown(false, true);

    }// OnLicenseStatusChanged

    #endregion

    #region Other Events

    private void frm_container_Load(object sender, EventArgs e)
    {
      //if (WindowManager.StartUpMode == WindowManager.StartUpModes.Explorer)
      {
        // Resize Forms
        UpdateSize();

        // DHA 11-JUL-2014: Added table mode to hidde menu
        if (WindowManager.IsTableMode && GamingTableBusinessLogic.IsEnabledGTPlayerTracking())
        {
          splitContainer1.Panel1Collapsed = true;
          pnl_menu_table_mode.Visible = true;
        }
      }
    }

    private void frm_container_FormClosing(object sender, FormClosingEventArgs e)
    {
      // When calling Application.Restart(), the code arrives here.
      // If the system is already closing, do nothing here. Continue the close.

      if (!Cashier.AlreadyClosing)
      {
        // ATB 16-FEB-2017
        Misc.WriteLog("[PROMPTED SHUTDOWN] REASON: Prompted", Log.Type.Message);
        Cashier.Shutdown(true, false);
        // If the code arrives here, the Shutdown is canceled.
        e.Cancel = true;

        InitFocusInVisibleDisplay();
      }
    }

    private void splitContainer2_Resize(object sender, System.EventArgs e)
    {
      UcConceptOperationsCenter();
    }

    void m_thread_EnableReceptionButtonChanged()
    {
      Delegate _event;

      if (this.InvokeRequired)
      {
        _event = new RefreshDataThread.EnableReceptionButtonChangedEventHandler(m_thread_EnableReceptionButtonChanged);
        this.Invoke(_event, new object[] { });
      }
      else
      {
        this.EnableCommonMenuButtons(true);
      }
    }

    /// <summary>
    /// Enable/Disable MB Button
    /// </summary>
    void EnableDisableMobileBankButtonStatusChanged()
    {
      // Fixed Bug 23924:Cajero: No es posible cerrar caja cuando se tiene una recaudaci�n pendiente de BM y el cajero esta fuera de turno
      // Check that are no pending amounts from mobile banks          
      if (CashierBusinessLogic.CalculatePendingAmount(Cashier.SessionId) > 0)
      {
        this.uc_menu.SetEnabledButton("BTN_MOBILE_BANK", !WindowManager.IsReceptionMode);
      }
    } // EnableDisableMBButtonStatusChanged

    void m_thread_WorkShiftExpiredStatusChanged(SystemInfo.StatusInfo Status, Int32 MaxDuration)
    {
      Delegate _event;
      String _message;

      if (this.InvokeRequired)
      {
        _event = new RefreshDataThread.WorkShiftExpiredStatusChangedEventHandler(m_thread_WorkShiftExpiredStatusChanged);
        this.Invoke(_event, new object[] { Status, MaxDuration });
      }
      else
      {
        // Only apply changes if status has changed
        if (Status == SystemInfo.StatusInfo.NOT_EXPIRED)
        {
          SystemInfo.WorkShiftExpired = SystemInfo.StatusInfo.NOT_EXPIRED;

          ChangeLabelStatus();

          this.uc_bank1.DisableButton(CASHIER_STATUS.OPEN);
          this.uc_bank1.RefreshStatus();
          this.uc_mobile_bank1.EnableDisableButtons(CASHIER_STATUS.OPEN);

          // Enable buttons
          EnableMenuButtons(true);
        }

        // Shift has expired

        // Only apply changes if status has changed
        if (Status == SystemInfo.StatusInfo.EXPIRED)
        {
          SystemInfo.WorkShiftExpired = SystemInfo.StatusInfo.EXPIRED;

          ChangeLabelStatus();

          this.uc_bank1.DisableButton(CASHIER_STATUS.OPEN);
          this.uc_mobile_bank1.EnableDisableButtons(CASHIER_STATUS.OPEN);

          // Disable buttons
          EnableMenuButtons(false);

          // Show notification warning
          if (MaxDuration == 1)
          {
            _message = Resource.String("STR_WORK_SHIFT_EXPIRED_NOTIFICATION_HOUR");
          }
          else
          {
            _message = Resource.String("STR_WORK_SHIFT_EXPIRED_NOTIFICATION_HOURS");
          }

          frm_message.Show(Resource.String("STR_WORK_SHIFT_EXPIRED_NOTIFICATION", MaxDuration.ToString() + " " + _message).Replace("\\r\\n", "\r\n"),
                           Resource.String("STR_APP_GEN_MSG_WARNING"),
                           MessageBoxButtons.OK, MessageBoxIcon.Warning, this.ParentForm);

          this.Focus();
          this.BringToFront();

          btn_bank_Click(null, null);
        }
      }
    }

    /// <summary>
    /// Enable/Disable menu buttons
    /// </summary>
    /// <param name="Enabled"></param>
    public void EnableMenuButtons(Boolean Enabled)
    {
      this.uc_options1.Enabled = Enabled;
      this.uc_card1.Enabled = Enabled;
      this.uc_tito_card1.Enabled = Enabled;
      this.uc_gaming_hall1.Enabled = Enabled;

      this.uc_menu.SetEnabledButton("BTN_RECEPTION", Enabled);
      this.uc_menu.SetEnabledButton("BTN_CARDS", Enabled);

      this.EnableCommonMenuButtons(Enabled);
    }

    /// <summary>
    /// Enable/Disable common menu buttons
    /// </summary>
    /// <param name="Enabled"></param>
    private void EnableCommonMenuButtons(Boolean Enabled)
    {
      if (Enabled)
      {
        Enabled = !WindowManager.IsReceptionMode;
      }

      this.uc_menu.SetEnabledButton("BTN_MOBILE_BANK", Enabled);
      this.uc_menu.SetEnabledButton("BTN_GIFT", Enabled);
      this.uc_menu.SetEnabledButton("BTN_LAST_VOUCHER", Enabled);
      this.uc_menu.SetEnabledButton("BTN_JACKPOT", Enabled);
      this.uc_menu.SetEnabledButton("BTN_PRINT", Enabled);

      this.uc_menu.SetEnabledButton("BTN_CONCEPTS", Enabled);
      this.uc_menu.SetEnabledButton("BTN_JACKPOT", Enabled);

      this.uc_menu.SetEnabledButton("BTN_CREATE_TICKET", Enabled);
      this.uc_menu.SetEnabledButton("BTN_PROMO", Enabled);
      this.uc_menu.SetEnabledButton("BTN_REDEEM_TICKET", Enabled);
      this.uc_menu.SetEnabledButton("BTN_TICKETS", Enabled);

      this.uc_menu.SetEnabledButton("BTN_HANDPAY", Enabled);
      this.uc_menu.SetEnabledButton("BTN_SET_PCD_METERS", Enabled);
      this.uc_menu.SetEnabledButton("BTN_COLLECT", Enabled);
      this.uc_menu.SetEnabledButton("BTN_FILL", Enabled);
    }

    void WGDB_OnStatusChanged()
    {
      Delegate _event;

      if (this.InvokeRequired)
      {
        _event = new WGDB.StatusChangedHandler(WGDB_OnStatusChanged);
        this.Invoke(_event);
      }
      else
      {
        if (shader_disconnected.Visible != true && WGDB.ConnectionState != ConnectionState.Open)
        {
          shader_disconnected.lbl_conn_error.Text = Resource.String("STR_DATABASE_CONN_STATUS_DISCONNECTED");
          shader_disconnected.lbl_conn_error.Visible = true;
          shader_disconnected.lbl_conn_error.Location = new Point(1024 / 2 - MIN_RESOLUTION_SHADER_WIDTH / 2, 768 / 2 - MIN_RESOLUTION_SHADER_HEIGHT / 2);
          shader_disconnected.pb_error.Visible = true;
          shader_disconnected.Show(this);
        }
        else if (WGDB.ConnectionState == ConnectionState.Open)
        {
          shader_disconnected.Hide();
          //Return focus after a disconnection
          InitFocusInVisibleDisplay();
        }
      }
    }

    void m_thread_GamingDayStatusChanged(SystemInfo.StatusInfo Status)
    {
      Delegate _event;
      String _message;

      if (this.InvokeRequired)
      {
        _event = new RefreshDataThread.GamingDayStatusChangedEventHandler(m_thread_GamingDayStatusChanged);
        this.Invoke(_event, new object[] { Status });
      }
      else
      {
        if (Status == SystemInfo.StatusInfo.EXPIRED)
        {
          SystemInfo.GamingDayExpired = SystemInfo.StatusInfo.EXPIRED;

          // Disable buttons
          this.uc_card1.Enabled = false;
          this.uc_bank1.DisableButton(CASHIER_STATUS.OPEN);
          this.uc_mobile_bank1.EnableDisableButtons(CASHIER_STATUS.OPEN);
          this.uc_gift_delivery1.Enabled = false;
          this.uc_options1.Enabled = false;
          this.uc_ticket_view.Enabled = false;
          this.uc_tito_card1.Enabled = false;
          this.uc_ticket_promo1.Enabled = false;
          this.uc_ticket_create1.Enabled = false;
          this.uc_ticket_offline1.Enabled = false;
          this.uc_ticket_undo1.Enabled = false;
          this.uc_ticket_validation1.Enabled = false;
          this.uc_gt_player_tracking1.Enabled = false;

          // Show notification warning
          _message = Resource.String("STR_GAMING_DAY_OFF");

          frm_message.Show(_message.Replace("\\r\\n", "\r\n"),
                           Resource.String("STR_APP_GEN_MSG_WARNING"),
                           MessageBoxButtons.OK, MessageBoxIcon.Warning, this.ParentForm);

          this.Focus();
          this.BringToFront();
        }
        // Only apply changes if status has changed
        else if (Status == SystemInfo.StatusInfo.NOT_EXPIRED)
        {
          SystemInfo.GamingDayExpired = SystemInfo.StatusInfo.NOT_EXPIRED;

          // Enable buttons
          this.uc_card1.Enabled = true;
          this.uc_bank1.DisableButton(CASHIER_STATUS.OPEN);
          this.uc_mobile_bank1.EnableDisableButtons(CASHIER_STATUS.OPEN);
          this.uc_gift_delivery1.Enabled = true;
          this.uc_options1.Enabled = true;
          this.uc_ticket_view.Enabled = true;
          this.uc_tito_card1.Enabled = true;
          this.uc_ticket_promo1.Enabled = true;
          this.uc_ticket_create1.Enabled = true;
          this.uc_ticket_offline1.Enabled = true;
          this.uc_ticket_undo1.Enabled = true;
          this.uc_ticket_validation1.Enabled = true;
          this.uc_gt_player_tracking1.Enabled = true;
        }

        ChangeLabelStatus();
      }
    }

    #endregion Other Events

    #region Menu

    private void CreateSubMenu(uc_round_button SelectedButton, CreateMenu.Menu SubMenu, Point ShowLocation)
    {
      Point _menu_location;
      ToolStripDropDownDirection _dropdown_direction;
      // DCS & DDM: Revisar para refactoring.
      // Se deber�a extraer toda la l�gica de esta funcion excepto la creaci�n de los botones y a�adir en una clase.
      ToolStripControlHost _controlHost;
      ToolStripDropDown _popupControl;
      WSI.Cashier.Controls.uc_menu _uc_sub_menu;

      _uc_sub_menu = new Controls.uc_menu();
      _uc_sub_menu.AutoCollapsed = true;
      _uc_sub_menu.TimeToCollapse = GeneralParam.GetInt32("Cashier", "Menu.Timeout.Seconds", 3) * 1000;
      _uc_sub_menu.Style = WSI.Cashier.Controls.uc_menu.MenuStyle.SUB_MENU;
      _uc_sub_menu.FillMenu(SubMenu.GetButtons());

      _controlHost = new ToolStripControlHost(_uc_sub_menu);
      _controlHost.Padding = Padding.Empty;
      _controlHost.Margin = Padding.Empty;

      _popupControl = new ToolStripDropDown();
      _popupControl.AutoSize = false;
      _popupControl.Size = new Size(_controlHost.Size.Width, _controlHost.Size.Height);
      _popupControl.Padding = new Padding(0);
      _popupControl.Margin = new Padding(0);
      _popupControl.DropShadowEnabled = true;
      _popupControl.Items.Add(_controlHost);

      _menu_location = new Point(ShowLocation.X, ShowLocation.Y - 1);
      _dropdown_direction = ToolStripDropDownDirection.BelowRight;

      if (SubMenu.MenuType == WSI.Cashier.CreateMenu.MenuType.GAMING_TABLES_MENU)
      {
        _menu_location = new Point(ShowLocation.X, ShowLocation.Y - _popupControl.Height - 2);
      }
      else if (SelectedButton.Name == BUTTON_OPTIONS_NAME)
      {
        _menu_location = new Point(ShowLocation.X, ShowLocation.Y + SelectedButton.Height + 1);
        _dropdown_direction = ToolStripDropDownDirection.AboveRight;
      }

      _popupControl.Show(SelectedButton.Parent, _menu_location, _dropdown_direction);

      _popupControl.Refresh();
    } // CreateSubMenu

    private void CreateMenu()
    {
      CreateMenu.Menu _menu = new CreateMenu.Menu(WSI.Cashier.CreateMenu.MenuType.MAIN_MENU);

      //CASHLESS MODE
      _menu.AddButton("BTN_CARDS", Resource.String("STR_FRM_CONTAINER_BTN_CARDS"), Resources.ResourceImages.menu_accounts, btn_cards_Click, SYSTEM_MODE.CASHLESS);
      _menu.AddButton("BTN_BANK", Resource.String("STR_FRM_CONTAINER_BTN_BANK"), Resources.ResourceImages.menu_cash, btn_bank_Click, SYSTEM_MODE.CASHLESS);
      _menu.AddButton("BTN_MOBILE_BANK", Resource.String("STR_FRM_CONTAINER_MB_TITLE"), Resources.ResourceImages.menu_mobileBanks, btn_mobile_banks_Click, SYSTEM_MODE.CASHLESS);
      _menu.AddButton("BTN_LAST_VOUCHER", Resource.String("STR_FRM_CONTAINER_BTN_LAST_VOUCHER"), Resources.ResourceImages.menu_cashDeskVoucher, btn_last_voucher_Click, SYSTEM_MODE.CASHLESS, false);
      _menu.AddButton("BTN_PRINT", Resource.String("STR_FRM_CONTAINER_BTN_AUTOMATIC_PRINT"), Resources.ResourceImages.menu_automaticPrinting, btn_automatic_print_Click, SYSTEM_MODE.CASHLESS, false);
      _menu.AddButton("BTN_GIFT", Resource.String("STR_FRM_CONTAINER_BTN_GIFTS"), Resources.ResourceImages.menu_gifts, btn_gifts_Click, SYSTEM_MODE.CASHLESS);
      _menu.AddButton("BTN_JACKPOT", Resource.String("STR_FRM_CONTAINER_BTN_JACKPOT"), Resources.ResourceImages.menu_jackpot, btn_jackpot_Click, SYSTEM_MODE.CASHLESS, false);
      _menu.AddButton("BTN_CONCEPTS", Resource.String("STR_CASH_CAGE_CONCEPTS"), Resources.ResourceImages.menu_items, btn_player_others_Click, SYSTEM_MODE.CASHLESS);

      if (GamingTableBusinessLogic.GamingTablesMode() == GamingTableBusinessLogic.GT_MODE.GUI_AND_CASHIER)
      {
        _menu.AddButton("BTN_PLAYER_TRACKING", Resource.String("STR_FRM_CONTAINER_BTN_PLAYER_TRACKING"), Resources.ResourceImages.menu_gamblingTables, btn_player_tracking_Click, SYSTEM_MODE.CASHLESS);
      }

      _menu.AddButton("BTN_RECEPTION", Resource.String("STR_FRM_CONTAINER_BTN_RECEPTION"), Resources.ResourceImages.bell, btn_reception_Click, SYSTEM_MODE.CASHLESS, false);
      _menu.AddButton(BUTTON_OPTIONS_NAME, Resource.String("STR_OPTIONS_OPTIONS"), Resources.ResourceImages.options, CreateSubMenuOptions, SYSTEM_MODE.CASHLESS, false);

      //MICO2 MODE 
      _menu.AddButton("BTN_CARDS", Resource.String("STR_FRM_CONTAINER_BTN_CARDS"), Resources.ResourceImages.menu_accounts, btn_cards_Click, SYSTEM_MODE.MICO2);
      _menu.AddButton("BTN_BANK", Resource.String("STR_FRM_CONTAINER_BTN_BANK"), Resources.ResourceImages.menu_cash, btn_bank_Click, SYSTEM_MODE.MICO2);
      _menu.AddButton("BTN_MOBILE_BANK", Resource.String("STR_FRM_CONTAINER_MB_TITLE"), Resources.ResourceImages.menu_mobileBanks, btn_mobile_banks_Click, SYSTEM_MODE.MICO2);
      _menu.AddButton("BTN_LAST_VOUCHER", Resource.String("STR_FRM_CONTAINER_BTN_LAST_VOUCHER"), Resources.ResourceImages.menu_cashDeskVoucher, btn_last_voucher_Click, SYSTEM_MODE.MICO2, false);
      _menu.AddButton("BTN_PRINT", Resource.String("STR_FRM_CONTAINER_BTN_AUTOMATIC_PRINT"), Resources.ResourceImages.menu_automaticPrinting, btn_automatic_print_Click, SYSTEM_MODE.MICO2, false);
      _menu.AddButton("BTN_GIFT", Resource.String("STR_FRM_CONTAINER_BTN_GIFTS"), Resources.ResourceImages.menu_gifts, btn_gifts_Click, SYSTEM_MODE.MICO2);
      _menu.AddButton("BTN_JACKPOT", Resource.String("STR_FRM_CONTAINER_BTN_JACKPOT"), Resources.ResourceImages.menu_jackpot, btn_jackpot_Click, SYSTEM_MODE.MICO2, false);
      _menu.AddButton("BTN_CONCEPTS", Resource.String("STR_CASH_CAGE_CONCEPTS"), Resources.ResourceImages.menu_items, btn_player_others_Click, SYSTEM_MODE.MICO2);

      if (GamingTableBusinessLogic.GamingTablesMode() == GamingTableBusinessLogic.GT_MODE.GUI_AND_CASHIER)
      {
        _menu.AddButton("BTN_PLAYER_TRACKING", Resource.String("STR_FRM_CONTAINER_BTN_PLAYER_TRACKING"), Resources.ResourceImages.menu_gamblingTables, btn_player_tracking_Click, SYSTEM_MODE.MICO2);
      }

      _menu.AddButton("BTN_RECEPTION", Resource.String("STR_FRM_CONTAINER_BTN_RECEPTION"), Resources.ResourceImages.bell, btn_reception_Click, SYSTEM_MODE.MICO2, false);
      _menu.AddButton("BTN_OPTIONS", Resource.String("STR_OPTIONS_OPTIONS"), Resources.ResourceImages.options, CreateSubMenuOptions, SYSTEM_MODE.MICO2, false);

      //// DCS 03-11-2015: PENDIENTE --------------------------

      //GAMING HALL MODE
      _menu.AddButton("BTN_HANDPAY", Resource.String("STR_FRM_CONTAINER_BTN_HANDPAY"), Resources.ResourceImages.manualPay, btn_handpay_Click, SYSTEM_MODE.GAMINGHALL, false);
      _menu.AddButton("BTN_BANK", Resource.String("STR_FRM_CONTAINER_BTN_BANK"), Resources.ResourceImages.menu_cash, btn_bank_Click, SYSTEM_MODE.GAMINGHALL);
      _menu.AddButton("BTN_CONCEPTS", Resource.String("STR_CASH_CAGE_CONCEPTS"), Resources.ResourceImages.menu_items, btn_player_others_Click, SYSTEM_MODE.GAMINGHALL);
      _menu.AddButton("BTN_LAST_VOUCHER", Resource.String("STR_FRM_CONTAINER_BTN_LAST_VOUCHER"), Resources.ResourceImages.menu_cashDeskVoucher, btn_last_voucher_Click, SYSTEM_MODE.GAMINGHALL, false);
      _menu.AddButton("BTN_COLLECT", Resource.String("STR_FRM_CONTAINER_BTN_COLLECT"), Resources.ResourceImages.extract_white, btn_collect_Click, SYSTEM_MODE.GAMINGHALL);
      _menu.AddButton("BTN_FILL", Resource.String("STR_FRM_CONTAINER_BTN_FILL"), Resources.ResourceImages.deposit_white, btn_fill_terminal_Click, SYSTEM_MODE.GAMINGHALL, false);
      _menu.AddButton("BTN_SET_PCD_METERS", Resource.String("STR_FRM_CONTAINER_BTN_SET_PCD_METERS"), Resources.ResourceImages.settingCounter_white, btn_set_pcd_meters_Click, SYSTEM_MODE.GAMINGHALL);
      _menu.AddButton("BTN_RECEPTION", Resource.String("STR_FRM_CONTAINER_BTN_RECEPTION"), Resources.ResourceImages.bell, btn_reception_Click, SYSTEM_MODE.GAMINGHALL, false);
      _menu.AddButton("BTN_OPTIONS", Resource.String("STR_OPTIONS_OPTIONS"), Resources.ResourceImages.options, CreateSubMenuOptions, SYSTEM_MODE.GAMINGHALL, false);

      //_menu.AddButton("BTN_HANDPAY", Resource.String("STR_FRM_CONTAINER_BTN_HANDPAY"), Images.CashierImage.Handpay, btn_handpay_Click, SYSTEM_MODE.GAMINGHALL);
      //_menu.AddButton("BTN_FILL", Resource.String("STR_FRM_CONTAINER_BTN_FILL"), Images.CashierImage.Fill, btn_fill_terminal_Click, SYSTEM_MODE.GAMINGHALL);
      //_menu.AddButton("BTN_READ_METERS", Resource.String("STR_FRM_CONTAINER_BTN_READ_METERS"), Images.CashierImage.Read, btn_read_terminal_meters_Click, SYSTEM_MODE.GAMINGHALL);

      //TITO
      if (GamingTableBusinessLogic.IsGamingTablesEnabled() || CurrencyExchange.IsCashAdvanceEnabled())
      {
        _menu.AddButton("BTN_CREATE_TICKET", Resource.String("STR_FRM_CONTAINER_BTN_CREATE_TICKET_OTHER"), Resources.ResourceImages.otherOperations, btn_create_ticket_Click, SYSTEM_MODE.TITO);
      }
      else
      {
        _menu.AddButton("BTN_CREATE_TICKET", Resource.String("STR_FRM_CONTAINER_BTN_CREATE_TICKET"), Resources.ResourceImages.otherOperations, btn_create_ticket_Click, SYSTEM_MODE.TITO);
      }

      _menu.AddButton("BTN_REDEEM_TICKET", Resource.String("STR_FRM_CONTAINER_BTN_REDEEM_TICKET"), Resources.ResourceImages.payTickets, btn_ticket_redeem_Click, SYSTEM_MODE.TITO);
      _menu.AddButton("BTN_PROMO", Resource.String("STR_FRM_TITO_PROMO_000"), Resources.ResourceImages.promos, btn_ticket_promo_Click, SYSTEM_MODE.TITO);
      _menu.AddButton("BTN_TICKETS", Resource.String("STR_UC_TICKETS_01"), Resources.ResourceImages.ticketsTito, CreateSubMenuTITO, SYSTEM_MODE.TITO, false);
      _menu.AddButton("BTN_CARDS", Resource.String("STR_FRM_CONTAINER_BTN_CARDS"), Resources.ResourceImages.menu_accounts, btn_cards_Click, SYSTEM_MODE.TITO);
      _menu.AddButton("BTN_BANK", Resource.String("STR_FRM_CONTAINER_BTN_BANK"), Resources.ResourceImages.menu_cash, btn_bank_Click, SYSTEM_MODE.TITO);
      _menu.AddButton("BTN_LAST_VOUCHER", Resource.String("STR_FRM_CONTAINER_BTN_LAST_VOUCHER"), Resources.ResourceImages.menu_cashDeskVoucher, btn_last_voucher_Click, SYSTEM_MODE.TITO, false);
      _menu.AddButton("BTN_GIFT", Resource.String("STR_FRM_CONTAINER_BTN_GIFTS"), Resources.ResourceImages.menu_gifts, btn_gifts_Click, SYSTEM_MODE.TITO);
      _menu.AddButton("BTN_JACKPOT", Resource.String("STR_FRM_CONTAINER_BTN_JACKPOT"), Resources.ResourceImages.menu_jackpot, btn_jackpot_Click, SYSTEM_MODE.TITO, false);
      _menu.AddButton("BTN_CONCEPTS", Resource.String("STR_CASH_CAGE_CONCEPTS"), Resources.ResourceImages.menu_items, btn_player_others_Click, SYSTEM_MODE.TITO);

      if (GamingTableBusinessLogic.GamingTablesMode() == GamingTableBusinessLogic.GT_MODE.GUI_AND_CASHIER)
      {
        _menu.AddButton("BTN_PLAYER_TRACKING", Resource.String("STR_FRM_CONTAINER_BTN_PLAYER_TRACKING"), Resources.ResourceImages.menu_gamblingTables, btn_player_tracking_Click, SYSTEM_MODE.TITO);
      }

      _menu.AddButton("BTN_RECEPTION", Resource.String("STR_FRM_CONTAINER_BTN_RECEPTION"), Resources.ResourceImages.bell, btn_reception_Click, SYSTEM_MODE.TITO, false);
      _menu.AddButton("BTN_OPTIONS", Resource.String("STR_OPTIONS_OPTIONS"), Resources.ResourceImages.options, CreateSubMenuOptions, SYSTEM_MODE.TITO, false);

      uc_menu.Style = WSI.Cashier.Controls.uc_menu.MenuStyle.MAIN_MENU;
      uc_menu.FillMenu(_menu.GetButtons());
    }

    private void CreateSubMenuOptions(object sender, EventArgs e)
    {
      uc_round_button _button_selected;

      _button_selected = (WSI.Cashier.Controls.uc_round_button)sender;

      CreateMenu.Menu _sub_menu = new CreateMenu.Menu(WSI.Cashier.CreateMenu.MenuType.SUB_MENU);
      _sub_menu.AddButton("BTN_CHANGE_PASSWORD", Resource.String("STR_FRM_CONTAINER_CHANGE_PASSWORD"), Resources.ResourceImages.menu_options, btn_change_password_Click, _button_selected);
      _sub_menu.AddButton("BTN_CHANGE_PIN", Resource.String("STR_FRM_CONTAINER_CHANGE_PIN"), Resources.ResourceImages.menu_options, btn_change_pin_Click, _button_selected);
      _sub_menu.AddButton("BTN_CONFIGURATION", Resource.String("STR_FRM_CONTAINER_CONFIGURATION"), Resources.ResourceImages.menu_options, btn_options_Click, _button_selected);
      _sub_menu.AddButton("BTN_LOG_OFF", Resource.String("STR_FRM_CONTAINER_BTN_LOG_OFF"), Resources.ResourceImages.menu_closeSession, btn_log_off_Click, _button_selected);
      _sub_menu.AddButton("BTN_SHUTDOWN", Resource.String("STR_FRM_CONTAINER_BTN_SHUTDOWN"), Resources.ResourceImages.menu_logOut, btn_shutdown_Click, _button_selected);

      CreateSubMenu(_button_selected, _sub_menu, new Point(_button_selected.Location.X + _button_selected.Width, _button_selected.Location.Y));
    }

    private void CreateSubMenuTITO(object sender, EventArgs e)
    {
      WSI.Cashier.Controls.uc_round_button _button_selected;

      _button_selected = (WSI.Cashier.Controls.uc_round_button)sender;

      CreateMenu.Menu _sub_menu = new CreateMenu.Menu(WSI.Cashier.CreateMenu.MenuType.SUB_MENU);
      _sub_menu.AddButton("BTN_UNDO_TICKET", Resource.String("STR_UC_TICKET_UNDO_NAME"), null, UndoTicketOperation, _button_selected);
      _sub_menu.AddButton("BTN_TICKET", Resource.String("STR_UC_TICKET_VALIDATION_NAME"), null, ShowTickets, _button_selected);
      _sub_menu.AddButton("BTN_TICKET_REPRINT", Resource.String("STR_FRM_CONTAINER_BTN_LAST_TICKETS"), null, TicketReprint, _button_selected, false);
      _sub_menu.AddButton("BTN_TICKET_OFFLINE", Resource.String("STR_UC_TICKET_OFFLINE"), null, btn_ticket_offline_Click, _button_selected);
      _sub_menu.AddButton("BTN_TICKET_TERMINAL", Resource.String("STR_UC_TICKET_TERMINAL_BUTTON_CALLER"), null, btn_ticket_terminal_Click, _button_selected);

      CreateSubMenu(_button_selected, _sub_menu, new Point(_button_selected.Location.X + _button_selected.Width, _button_selected.Location.Y));
    }

    private void CreateSubMenuGamingTables(object sender, EventArgs e)
    {
      WSI.Cashier.Controls.uc_round_button _button_selected;

      _button_selected = (WSI.Cashier.Controls.uc_round_button)sender;

      CreateMenu.Menu _sub_menu = new CreateMenu.Menu(WSI.Cashier.CreateMenu.MenuType.GAMING_TABLES_MENU);

      if (GamingTableBusinessLogic.IsEnabledGTPlayerTracking())
      {
        _sub_menu.AddButton("BTN_PLAYER_TRACKING", Resource.String("STR_FRM_CONTAINER_BTN_PLAYER_TRACKING"), Resources.ResourceImages.menu_gamblingTables, btn_player_tracking_Click);
      }
      _sub_menu.AddButton("BTN_CHANGE_PASSWORD", Resource.String("STR_FRM_CONTAINER_CHANGE_PASSWORD"), Resources.ResourceImages.menu_options, btn_change_password_Click);
      _sub_menu.AddButton("BTN_CHANGE_PIN", Resource.String("STR_FRM_CONTAINER_CHANGE_PIN"), Resources.ResourceImages.menu_options, btn_change_pin_Click);
      _sub_menu.AddButton("BTN_CONFIGURATION", Resource.String("STR_FRM_CONTAINER_CONFIGURATION"), Resources.ResourceImages.menu_options, btn_options_Click);
      _sub_menu.AddButton("BTN_LOG_OFF", Resource.String("STR_FRM_CONTAINER_BTN_LOG_OFF"), Resources.ResourceImages.menu_closeSession, btn_log_off_Click);
      _sub_menu.AddButton("BTN_SHUTDOWN", Resource.String("STR_FRM_CONTAINER_BTN_SHUTDOWN"), Resources.ResourceImages.menu_logOut, btn_shutdown_Click);

      CreateSubMenu(_button_selected, _sub_menu, new Point(0, 0));
    }

    // REVISAR CON JCA y DDM
    private void StartReceptionMode()
    {
      using (frm_yesno _frm_shader = new frm_yesno())
      {
        _frm_shader.Opacity = 0.6;
        _frm_shader.Show();

        using (FrmSearchCustomersReception _form_reception = new FrmSearchCustomersReception(m_id_scanner, this.uc_bank1))
        {
          if (WindowManager.StartUpMode == WindowManager.StartUpModes.Explorer)
          {
            _form_reception.Location = new Point(
              WindowManager.GetFormCenterLocation(_frm_shader).X - (_form_reception.Width / 2), _frm_shader.Location.Y + 2);
            _form_reception.Width = this.Width;
            _form_reception.Height = this.Height - 42;
          }

          _form_reception.IDScanner = m_id_scanner;

          try
          {
            _form_reception.ShowDialog(_frm_shader);
            //_form_reception.ShowDialog();
          }
          finally
          {
          }
        }
      }
    }

    private void btn_ticket_terminal_Click()
    {

      String error_str;

      // Check if current user is an authorized user
      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.TITO_ListLastBillAndTickets,
                                               ProfilePermissions.TypeOperation.RequestPasswd,
                                               this,
                                               out error_str))
      {
        return;
      }



      ShowOnly("ticket_terminal");

      uc_ticket_terminal1.InitControls();

      InitFocusInVisibleDisplay();

    }

    private void btn_reception_Click(object sender, EventArgs e)
    {
      String _error_str;

      if (!ProfilePermissions.CheckPermissionList(ProfilePermissions.CashierFormFuncionality.CustomersReceptionSearch,
                                                  ProfilePermissions.TypeOperation.RequestPasswd,
                                 this,
                                                  Cashier.AuthorizedByUserId,
                                 out _error_str))
      {
        return;
      }

      StartReceptionMode();
    }

    #endregion Menu

    public class RefreshDataThread
    {
      private SystemInfo m_system_info;

      public SystemInfo SystemInfo
      {
        get { return m_system_info; }
        set { m_system_info = value; }
      }

      private Boolean m_reception_button_enabled;

      public delegate void GamingDayStatusChangedEventHandler(SystemInfo.StatusInfo Status);
      public event GamingDayStatusChangedEventHandler GamingDayStatusChanged;

      public delegate void WorkShiftExpiredStatusChangedEventHandler(SystemInfo.StatusInfo Status, Int32 MaxDuration);
      public event WorkShiftExpiredStatusChangedEventHandler WorkShiftExpiredStatusChanged;

      public delegate void EnableReceptionButtonChangedEventHandler();
      public event EnableReceptionButtonChangedEventHandler EnableReceptionButtonChanged;

      public void Init(SystemInfo SystemInfo)
      {
        Thread _thread;

        this.SystemInfo = SystemInfo;
        this.m_reception_button_enabled = false;

        _thread = new Thread(RefreshThread_frm_container);
        _thread.Name = "RefreshThread_frm_container";
        _thread.Start();
      } // Init

      private void RefreshThread_frm_container()
      {
        Int32 _wait_hint;

        _wait_hint = 0;

        while (true)
        {
          try
          {
            Thread.Sleep(_wait_hint);

            CheckGamingDayExpiration();
            CheckWorkShiftExpiration();
            CheckReceptionEnable();

            _wait_hint = 2000;
          }
          catch (Exception _ex)
          {
            Log.Exception(_ex);
            _wait_hint = 10000;
          }
        } // while(true)
      }

      //------------------------------------------------------------------------------
      // PURPOSE : Check if gaming day has expired and take appropriate action
      //
      //  PARAMS :
      //      - INPUT :
      //
      //      - OUTPUT :
      //
      // RETURNS :
      //
      // JBC#41
      private void CheckGamingDayExpiration()
      {
        DateTime _closing_time;
        DateTime _gaming_day;
        DateTime _opening_time;
        DateTime _next_opening;
        SystemInfo.StatusInfo _status;

        _status = m_system_info.GamingDayExpired;

        if (Misc.IsGamingDayEnabled() && Cashier.GamingDay != DateTime.MinValue)
        {
          _gaming_day = Cashier.GamingDay;
          _opening_time = Misc.TodayOpening();

          // Check if there's a logged user
          if (Cashier.UserId == 0)
          {
            return;
          }

          // Check if there's an open cashier session
          if (Cashier.SessionId == 0)
          {
            return;
          }

          // As an additional safety measure,
          // check if there's a valid opening date stored
          if (SystemInfo.CashSessionOpeningDate == DateTime.MinValue)
          {
            return;
          }

          //Coger jornada de la session
          _closing_time = new DateTime(_gaming_day.Year, _gaming_day.Month, _gaming_day.Day, _opening_time.Hour, _opening_time.Minute, 0);

          //a�adirle 1 d�a
          _next_opening = _closing_time.AddDays(1);

          //si la fecha de hoy es mayor o igual a la hora de cierre, mostrar pop-up y deshabilitar todo
          if (WGDB.Now >= _next_opening)
          {
            if (SystemInfo.GamingDayExpired != SystemInfo.StatusInfo.EXPIRED)
            {
              _status = SystemInfo.StatusInfo.EXPIRED;
            }
          }
          else
          {
            // Only apply changes if status has changed
            if (SystemInfo.GamingDayExpired != SystemInfo.StatusInfo.NOT_EXPIRED)
            {
              _status = SystemInfo.StatusInfo.NOT_EXPIRED;
            }
          } //if (DateTime.Now >= _closing_time)

          if (_status != m_system_info.GamingDayExpired)
          {
            m_system_info.GamingDayExpired = _status;

            if (GamingDayStatusChanged != null)
            {
              GamingDayStatusChanged(m_system_info.GamingDayExpired);
            }
          }

        }
      } //CheckGamingDayExpiration

      private void CheckWorkShiftExpiration()
      {
        TimeSpan _elapsed_time;
        Int32 _max_duration;
        Int64 _session_id;
        SystemInfo.StatusInfo _status;
        bool forceRefresh = false;

        _status = m_system_info.WorkShiftExpired;

        // Check if there's a logged user
        if (Cashier.UserId == 0)
        {
          return;
        }

        // Check if there is any open cashier session
        if (Cashier.SessionId == 0 && Cashier.TerminalId != 0 && Cashier.UserId != 0)
        {
          DB_TRX _db_trx = new DB_TRX();
          _session_id = CashierBusinessLogic.GetSessionId(_db_trx.SqlTransaction, CashierBusinessLogic.ReadBankMode(_db_trx.SqlTransaction), Cashier.TerminalId, Cashier.UserId);
          if (_session_id != 0)
          {
            Cashier.SetUserSession(_session_id);
            forceRefresh = true;
          }
        }

        // Check if there's an open cashier session
        if (Cashier.SessionId == 0 || CashierBusinessLogic.ReadCashierStatusWithoutUpdateCashierSession(Cashier.SessionId) != CASHIER_STATUS.OPEN)
        {
          return;
        }

        // As an additional safety measure,
        // check if there's a valid opening date stored
        if (SystemInfo.CashSessionOpeningDate == DateTime.MinValue)
        {
          return;
        }

        // Get maximum duration
        _max_duration = GeneralParam.GetInt32("Cashier", "WorkShiftDurationHours");

        // Calculate elapsed time
        _elapsed_time = WGDB.Now - SystemInfo.CashSessionOpeningDate;

        // Use for testing when you want shift hours time out
        //_elapsed_time = WGDB.Now.AddHours(1) - SystemInfo.CashSessionOpeningDate;


        // Shift has not expired or is not limited
        if (_max_duration == 0 || _elapsed_time.TotalHours < _max_duration)
        {
          // Shift has not expired

          // Only apply changes if status has changed
          if (_status != SystemInfo.StatusInfo.NOT_EXPIRED)
          {
            _status = SystemInfo.StatusInfo.NOT_EXPIRED;
          }

        }
        else
        {
          // Shift has expired

          // Only apply changes if status has changed
          if (_status != SystemInfo.StatusInfo.EXPIRED)
          {
            _status = SystemInfo.StatusInfo.EXPIRED;
          }

        }

        //Enable or Disable controls if status has changed.
        if (_status != m_system_info.WorkShiftExpired || forceRefresh)
        {
          m_system_info.WorkShiftExpired = _status;

          if (WorkShiftExpiredStatusChanged != null)
          {
            WorkShiftExpiredStatusChanged(m_system_info.WorkShiftExpired, _max_duration);
          }
        }

      } // CheckWorkShiftExpiration

      private void CheckReceptionEnable()
      {
        if (WindowManager.IsReceptionMode != m_reception_button_enabled)
        {
          m_reception_button_enabled = WindowManager.IsReceptionMode;

          if (EnableReceptionButtonChanged != null)
          {
            EnableReceptionButtonChanged();
          }
        }
      }
    }

    /// <summary>
    /// Pin login
    /// </summary>
    /// <param name="UserId"></param>
    /// <returns></returns>
    private Boolean CheckPin(Int32 UserId)
    {
      DialogResult _dr;

      _dr = new frm_pin_imput().Show(UserId);

      switch (_dr)
      {
        case DialogResult.No:
          {
            this.UserLogOff(false);

            return false;
          }
        case DialogResult.Cancel:
          {
            return false;
          }
        default:
          // Nothing TODO
          break;
      }

      return true;

    } // PinLogin

  }
}
