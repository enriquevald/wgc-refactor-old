﻿namespace WSI.Cashier
{
  partial class frm_currency_refund
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.uc_given_amount = new WSI.Cashier.Controls.uc_round_textbox();
      this.lbl_refund_iso_foreign = new WSI.Cashier.Controls.uc_label();
      this.lbl_given_iso = new WSI.Cashier.Controls.uc_label();
      this.lbl_max_refund_amount = new WSI.Cashier.Controls.uc_label();
      this.lbl_amount_to_give_foreign = new WSI.Cashier.Controls.uc_label();
      this.lbl_max_refund = new WSI.Cashier.Controls.uc_label();
      this.lbl_given_amount = new WSI.Cashier.Controls.uc_label();
      this.btn_ok = new WSI.Cashier.Controls.uc_round_button();
      this.btn_cancel = new WSI.Cashier.Controls.uc_round_button();
      this.lbl_amount_to_give_national = new WSI.Cashier.Controls.uc_label();
      this.lbl_refund_iso_national = new WSI.Cashier.Controls.uc_label();
      this.uc_amount_to_give_foreign = new WSI.Cashier.Controls.uc_round_textbox();
      this.lbl_refund_national = new WSI.Cashier.Controls.uc_label();
      this.lbl_exchange = new WSI.Cashier.Controls.uc_label();
      this.lbl_exchange_amount = new WSI.Cashier.Controls.uc_label();
      this.lbl_exchange_rate_amount = new WSI.Cashier.Controls.uc_label();
      this.lbl_exchange_rate = new WSI.Cashier.Controls.uc_label();
      this.uc_payment_method1 = new WSI.Cashier.uc_payment_method();
      this.pnl_data.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.Controls.Add(this.uc_payment_method1);
      this.pnl_data.Controls.Add(this.lbl_exchange_rate_amount);
      this.pnl_data.Controls.Add(this.lbl_exchange_amount);
      this.pnl_data.Controls.Add(this.lbl_exchange_rate);
      this.pnl_data.Controls.Add(this.lbl_exchange);
      this.pnl_data.Controls.Add(this.lbl_refund_national);
      this.pnl_data.Controls.Add(this.uc_amount_to_give_foreign);
      this.pnl_data.Controls.Add(this.lbl_refund_iso_national);
      this.pnl_data.Controls.Add(this.lbl_amount_to_give_national);
      this.pnl_data.Controls.Add(this.uc_given_amount);
      this.pnl_data.Controls.Add(this.lbl_refund_iso_foreign);
      this.pnl_data.Controls.Add(this.lbl_given_iso);
      this.pnl_data.Controls.Add(this.lbl_max_refund_amount);
      this.pnl_data.Controls.Add(this.lbl_amount_to_give_foreign);
      this.pnl_data.Controls.Add(this.lbl_max_refund);
      this.pnl_data.Controls.Add(this.lbl_given_amount);
      this.pnl_data.Controls.Add(this.btn_ok);
      this.pnl_data.Controls.Add(this.btn_cancel);
      this.pnl_data.Size = new System.Drawing.Size(502, 435);
      // 
      // uc_given_amount
      // 
      this.uc_given_amount.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.uc_given_amount.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.uc_given_amount.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.uc_given_amount.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.uc_given_amount.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.uc_given_amount.CornerRadius = 5;
      this.uc_given_amount.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.uc_given_amount.Location = new System.Drawing.Point(224, 196);
      this.uc_given_amount.MaxLength = 12;
      this.uc_given_amount.Multiline = false;
      this.uc_given_amount.Name = "uc_given_amount";
      this.uc_given_amount.PasswordChar = '\0';
      this.uc_given_amount.ReadOnly = false;
      this.uc_given_amount.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.uc_given_amount.SelectedText = "";
      this.uc_given_amount.SelectionLength = 0;
      this.uc_given_amount.SelectionStart = 0;
      this.uc_given_amount.Size = new System.Drawing.Size(207, 50);
      this.uc_given_amount.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC_CUSTOM;
      this.uc_given_amount.TabIndex = 38;
      this.uc_given_amount.TabStop = false;
      this.uc_given_amount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      this.uc_given_amount.UseSystemPasswordChar = false;
      this.uc_given_amount.WaterMark = null;
      this.uc_given_amount.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      this.uc_given_amount.TextChanged += new System.EventHandler(this.uc_given_amount_TextChanged);
      this.uc_given_amount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.uc_given_amount_KeyPress);
      this.uc_given_amount.Enter += new System.EventHandler(this.uc_given_amount_Enter);
      this.uc_given_amount.Leave += new System.EventHandler(this.uc_given_amount_Leave);
      this.uc_given_amount.Click += new System.EventHandler(this.uc_given_amount_Click);
      this.uc_given_amount.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.uc_given_amount_PreviewKeyDown);
      // 
      // lbl_refund_iso_foreign
      // 
      this.lbl_refund_iso_foreign.BackColor = System.Drawing.Color.Transparent;
      this.lbl_refund_iso_foreign.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_refund_iso_foreign.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_refund_iso_foreign.Location = new System.Drawing.Point(440, 269);
      this.lbl_refund_iso_foreign.Name = "lbl_refund_iso_foreign";
      this.lbl_refund_iso_foreign.Size = new System.Drawing.Size(46, 25);
      this.lbl_refund_iso_foreign.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_refund_iso_foreign.TabIndex = 37;
      this.lbl_refund_iso_foreign.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_given_iso
      // 
      this.lbl_given_iso.BackColor = System.Drawing.Color.Transparent;
      this.lbl_given_iso.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_given_iso.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_given_iso.Location = new System.Drawing.Point(440, 211);
      this.lbl_given_iso.Name = "lbl_given_iso";
      this.lbl_given_iso.Size = new System.Drawing.Size(43, 25);
      this.lbl_given_iso.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_given_iso.TabIndex = 36;
      this.lbl_given_iso.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_max_refund_amount
      // 
      this.lbl_max_refund_amount.BackColor = System.Drawing.Color.Transparent;
      this.lbl_max_refund_amount.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_max_refund_amount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_max_refund_amount.Location = new System.Drawing.Point(314, 135);
      this.lbl_max_refund_amount.Name = "lbl_max_refund_amount";
      this.lbl_max_refund_amount.Size = new System.Drawing.Size(157, 25);
      this.lbl_max_refund_amount.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_max_refund_amount.TabIndex = 34;
      this.lbl_max_refund_amount.Text = "xxxx";
      this.lbl_max_refund_amount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_amount_to_give_foreign
      // 
      this.lbl_amount_to_give_foreign.BackColor = System.Drawing.Color.Transparent;
      this.lbl_amount_to_give_foreign.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_amount_to_give_foreign.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_amount_to_give_foreign.Location = new System.Drawing.Point(36, 266);
      this.lbl_amount_to_give_foreign.Name = "lbl_amount_to_give_foreign";
      this.lbl_amount_to_give_foreign.Size = new System.Drawing.Size(201, 25);
      this.lbl_amount_to_give_foreign.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_amount_to_give_foreign.TabIndex = 31;
      this.lbl_amount_to_give_foreign.Text = "xRefund in foreign currency";
      this.lbl_amount_to_give_foreign.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_max_refund
      // 
      this.lbl_max_refund.BackColor = System.Drawing.Color.Transparent;
      this.lbl_max_refund.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_max_refund.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_max_refund.Location = new System.Drawing.Point(53, 135);
      this.lbl_max_refund.Name = "lbl_max_refund";
      this.lbl_max_refund.Size = new System.Drawing.Size(267, 25);
      this.lbl_max_refund.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_max_refund.TabIndex = 29;
      this.lbl_max_refund.Text = "xMax. amount in foreign currency:";
      this.lbl_max_refund.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_given_amount
      // 
      this.lbl_given_amount.BackColor = System.Drawing.Color.Transparent;
      this.lbl_given_amount.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_given_amount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_given_amount.Location = new System.Drawing.Point(36, 209);
      this.lbl_given_amount.Name = "lbl_given_amount";
      this.lbl_given_amount.Size = new System.Drawing.Size(191, 25);
      this.lbl_given_amount.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_given_amount.TabIndex = 28;
      this.lbl_given_amount.Text = "xGiven amount";
      this.lbl_given_amount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // btn_ok
      // 
      this.btn_ok.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_ok.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_ok.FlatAppearance.BorderSize = 0;
      this.btn_ok.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_ok.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_ok.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_ok.Image = null;
      this.btn_ok.IsSelected = false;
      this.btn_ok.Location = new System.Drawing.Point(334, 362);
      this.btn_ok.Name = "btn_ok";
      this.btn_ok.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_ok.Size = new System.Drawing.Size(155, 60);
      this.btn_ok.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_ok.TabIndex = 27;
      this.btn_ok.Text = "XOK";
      this.btn_ok.UseVisualStyleBackColor = false;
      this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
      // 
      // btn_cancel
      // 
      this.btn_cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_cancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btn_cancel.FlatAppearance.BorderSize = 0;
      this.btn_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_cancel.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_cancel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_cancel.Image = null;
      this.btn_cancel.IsSelected = false;
      this.btn_cancel.Location = new System.Drawing.Point(166, 362);
      this.btn_cancel.Name = "btn_cancel";
      this.btn_cancel.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_cancel.Size = new System.Drawing.Size(155, 60);
      this.btn_cancel.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_cancel.TabIndex = 26;
      this.btn_cancel.Text = "XCANCEL";
      this.btn_cancel.UseVisualStyleBackColor = false;
      this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
      // 
      // lbl_amount_to_give_national
      // 
      this.lbl_amount_to_give_national.BackColor = System.Drawing.Color.Transparent;
      this.lbl_amount_to_give_national.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_amount_to_give_national.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_amount_to_give_national.Location = new System.Drawing.Point(36, 322);
      this.lbl_amount_to_give_national.Name = "lbl_amount_to_give_national";
      this.lbl_amount_to_give_national.Size = new System.Drawing.Size(187, 25);
      this.lbl_amount_to_give_national.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_amount_to_give_national.TabIndex = 39;
      this.lbl_amount_to_give_national.Text = "xRefund in national currency";
      this.lbl_amount_to_give_national.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_refund_iso_national
      // 
      this.lbl_refund_iso_national.BackColor = System.Drawing.Color.Transparent;
      this.lbl_refund_iso_national.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_refund_iso_national.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_refund_iso_national.Location = new System.Drawing.Point(440, 324);
      this.lbl_refund_iso_national.Name = "lbl_refund_iso_national";
      this.lbl_refund_iso_national.Size = new System.Drawing.Size(46, 25);
      this.lbl_refund_iso_national.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_refund_iso_national.TabIndex = 41;
      this.lbl_refund_iso_national.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // uc_amount_to_give_foreign
      // 
      this.uc_amount_to_give_foreign.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.uc_amount_to_give_foreign.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.uc_amount_to_give_foreign.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.uc_amount_to_give_foreign.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.uc_amount_to_give_foreign.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.uc_amount_to_give_foreign.CornerRadius = 5;
      this.uc_amount_to_give_foreign.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.uc_amount_to_give_foreign.Location = new System.Drawing.Point(224, 253);
      this.uc_amount_to_give_foreign.MaxLength = 12;
      this.uc_amount_to_give_foreign.Multiline = false;
      this.uc_amount_to_give_foreign.Name = "uc_amount_to_give_foreign";
      this.uc_amount_to_give_foreign.PasswordChar = '\0';
      this.uc_amount_to_give_foreign.ReadOnly = false;
      this.uc_amount_to_give_foreign.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.uc_amount_to_give_foreign.SelectedText = "";
      this.uc_amount_to_give_foreign.SelectionLength = 0;
      this.uc_amount_to_give_foreign.SelectionStart = 0;
      this.uc_amount_to_give_foreign.Size = new System.Drawing.Size(206, 50);
      this.uc_amount_to_give_foreign.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC_CUSTOM;
      this.uc_amount_to_give_foreign.TabIndex = 42;
      this.uc_amount_to_give_foreign.TabStop = false;
      this.uc_amount_to_give_foreign.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      this.uc_amount_to_give_foreign.UseSystemPasswordChar = false;
      this.uc_amount_to_give_foreign.WaterMark = null;
      this.uc_amount_to_give_foreign.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      this.uc_amount_to_give_foreign.TextChanged += new System.EventHandler(this.uc_amount_to_give_foreign_TextChanged);
      this.uc_amount_to_give_foreign.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.uc_amount_to_give_foreign_KeyPress);
      this.uc_amount_to_give_foreign.Enter += new System.EventHandler(this.uc_amount_to_give_foreign_Enter);
      this.uc_amount_to_give_foreign.Leave += new System.EventHandler(this.uc_amount_to_give_foreign_Leave);
      this.uc_amount_to_give_foreign.Click += new System.EventHandler(this.uc_amount_to_give_foreign_Click);
      this.uc_amount_to_give_foreign.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.uc_amount_to_give_foreign_PreviewKeyDown);
      // 
      // lbl_refund_national
      // 
      this.lbl_refund_national.BackColor = System.Drawing.Color.Transparent;
      this.lbl_refund_national.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_refund_national.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_refund_national.Location = new System.Drawing.Point(218, 317);
      this.lbl_refund_national.Name = "lbl_refund_national";
      this.lbl_refund_national.Size = new System.Drawing.Size(215, 28);
      this.lbl_refund_national.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_24PX_BLACK;
      this.lbl_refund_national.TabIndex = 43;
      this.lbl_refund_national.Text = "xRefund in national currency";
      this.lbl_refund_national.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      this.lbl_refund_national.TextChanged += new System.EventHandler(this.lbl_refund_national_TextChanged);
      // 
      // lbl_exchange
      // 
      this.lbl_exchange.BackColor = System.Drawing.Color.Transparent;
      this.lbl_exchange.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_exchange.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_exchange.Location = new System.Drawing.Point(53, 163);
      this.lbl_exchange.Name = "lbl_exchange";
      this.lbl_exchange.Size = new System.Drawing.Size(267, 25);
      this.lbl_exchange.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_exchange.TabIndex = 44;
      this.lbl_exchange.Text = "xExchange amount:";
      this.lbl_exchange.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_exchange_amount
      // 
      this.lbl_exchange_amount.BackColor = System.Drawing.Color.Transparent;
      this.lbl_exchange_amount.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_exchange_amount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_exchange_amount.Location = new System.Drawing.Point(314, 163);
      this.lbl_exchange_amount.Name = "lbl_exchange_amount";
      this.lbl_exchange_amount.Size = new System.Drawing.Size(157, 25);
      this.lbl_exchange_amount.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_exchange_amount.TabIndex = 45;
      this.lbl_exchange_amount.Text = "xxxx";
      this.lbl_exchange_amount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_exchange_rate_amount
      // 
      this.lbl_exchange_rate_amount.BackColor = System.Drawing.Color.Transparent;
      this.lbl_exchange_rate_amount.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_exchange_rate_amount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_exchange_rate_amount.Location = new System.Drawing.Point(220, 150);
      this.lbl_exchange_rate_amount.Name = "lbl_exchange_rate_amount";
      this.lbl_exchange_rate_amount.Size = new System.Drawing.Size(241, 25);
      this.lbl_exchange_rate_amount.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_exchange_rate_amount.TabIndex = 30;
      this.lbl_exchange_rate_amount.Text = "xxxx";
      this.lbl_exchange_rate_amount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_exchange_rate
      // 
      this.lbl_exchange_rate.BackColor = System.Drawing.Color.Transparent;
      this.lbl_exchange_rate.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_exchange_rate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_exchange_rate.Location = new System.Drawing.Point(40, 150);
      this.lbl_exchange_rate.Name = "lbl_exchange_rate";
      this.lbl_exchange_rate.Size = new System.Drawing.Size(174, 25);
      this.lbl_exchange_rate.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_exchange_rate.TabIndex = 29;
      this.lbl_exchange_rate.Text = "xExchangeRate:";
      this.lbl_exchange_rate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // uc_payment_method1
      // 
      this.uc_payment_method1.BtnSelectedCurrencyEnabled = true;
      this.uc_payment_method1.Location = new System.Drawing.Point(55, 21);
      this.uc_payment_method1.Name = "uc_payment_method1";
      this.uc_payment_method1.Size = new System.Drawing.Size(385, 116);
      this.uc_payment_method1.TabIndex = 48;
      this.uc_payment_method1.CurrencyChanged += new WSI.Cashier.uc_payment_method.CurrencyChangedHandler(this.uc_payment_method1_CurrencyChanged);
      // 
      // frm_currency_refund
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(502, 490);
      this.FormSubTitle = "xCurrency refund";
      this.FormTitle = "xCurrency refund";
      this.MinimumSize = new System.Drawing.Size(502, 490);
      this.Name = "frm_currency_refund";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "frm_currency_refund";
      this.Activated += new System.EventHandler(this.frm_currency_refund_Activated);
      this.Load += new System.EventHandler(this.frm_currency_refund_Load);
      this.pnl_data.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private Controls.uc_round_textbox uc_given_amount;
    private Controls.uc_label lbl_refund_iso_foreign;
    private Controls.uc_label lbl_given_iso;
    private Controls.uc_label lbl_max_refund_amount;
    private Controls.uc_label lbl_amount_to_give_foreign;
    private Controls.uc_label lbl_max_refund;
    private Controls.uc_label lbl_given_amount;
    private Controls.uc_round_button btn_ok;
    private Controls.uc_round_button btn_cancel;
    private Controls.uc_label lbl_amount_to_give_national;
    private Controls.uc_label lbl_refund_iso_national;
    private Controls.uc_round_textbox uc_amount_to_give_foreign;
    private Controls.uc_label lbl_refund_national;
    private Controls.uc_label lbl_exchange_amount;
    private Controls.uc_label lbl_exchange;
    private Controls.uc_label lbl_exchange_rate_amount;
    private Controls.uc_label lbl_exchange_rate;
    private uc_payment_method uc_payment_method1;

  }
}