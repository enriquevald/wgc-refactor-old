//------------------------------------------------------------------------------
// Copyright � 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_payment_method.cs
// 
//   DESCRIPTION: Implements the following classes:
//                1. frm_payment_method
//
//        AUTHOR: David Lasdiez
// 
// CREATION DATE: 19-SEP-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 19-SEP-2014 DLL     First release.
// 26-SEP-2014 JBC     Fixed Bug WIG-1314: Number pad problems solved.
// 08-OCT-2014 JBC     Fixed Bug WIG-1436: CashAdvance mode in ApplyExchange
// 08-OCT-2014 JBC     Fixed Bug WIG-1437: CashAdvance mode in ApplyExchange
// 15-OCT-2014 JBC     Fixed Bug WIG-1474: Now textbox works like payment order.
// 29-OCT-2014 JBC     Fixed Bug WIG-1594: Location
// 10-NOV-2015 FOS     Apply new design in form
// 11-NOV-2015 SGB     Backlog Item WIG-5880: Change designer
// 15-MAR-2016 FOS     Fixed Bug 10649:Button selected Style doesn't change.
// 19-MAY-2016 ETP     Bug 13384: Multicurrency - Error si los permisos estan deshabilitados.
// 26-MAY-2016 FAV     Fixed Bug 13575: Style for Check button
// 02-JUN-2016 ETP     Fixed Bug 13995: The waive button not works as especified.
// 14-JUL-2016 EOR     Product Backlog Item 14984: TPV Televisa: Limit of recharge/cash advance in card bank
// 05-OCT-2017 RAB     PBI 30018:WIGOS-4036 Payment threshold authorization - Payment authorisation
// 09-OCT-2017 JML     PBI 30022:WIGOS-4052 Payment threshold registration - Recharge with Check payment
// 10-OCT-2017 JML     PBI 30023:WIGOS-4068 Payment threshold registration - Transaction information
// 30-OCT-2017 RAB     Bug 30446:WIGOS-6052 Threshold: Screen order is incorrect in HP and dispute (paying with PO) exceeding threshold and without permissions
// 16-NOV-2017 RAB     Bug 30816:WIGOS-6647 Threshold: the check data screen is missing when the amount exceed to boundary value with cash advance by check operation
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WSI.Common;
using WSI.Cashier.Controls;

namespace WSI.Cashier
{
  public partial class frm_payment_method : frm_base
  {
    enum CalculationType
    {
      None = 0,
      Normal = 1,
      Inverse = 2,
    }

    #region Members

    private static String m_decimal_str = System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencyDecimalSeparator;
    private frm_generic_amount_input m_amount_input;
    private CurrencyExchange m_national_currency;
    private CurrencyExchange m_selected_currency;
    private Dictionary<String, CurrencyExchange> m_currencies;
    private CurrencyExchangeResult m_exchange_result;
    private CalculationType m_calculation;
    private Boolean Modify = false;
    private Boolean m_first_time = true;
    private Form m_ParentForm;
    private Boolean m_waive_commission;

    #endregion

    #region Constructor

    /// <summary>
    /// Constructor method.
    /// 1. Initialize controls.
    /// 2. Initialize control�s resources. (NLS, Images, etc)
    /// </summary>
    public frm_payment_method(Form Parent)
    {
      InitializeComponent();
      InitializeControlResources();
      SetCurrenciesImage();

      EventLastAction.AddEventLastAction(this.Controls);
      m_waive_commission = false;
      this.m_ParentForm = Parent;
    }

    #endregion

    #region Public

    //------------------------------------------------------------------------------
    // PURPOSE : Initialize control resources (NLS strings, images, etc).
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public void InitializeControlResources()
    {
      this.FormTitle = Resource.String("STR_VOUCHER_CASH_ADVANCE");

      // Buttons
      btn_ok.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_OK");
      btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");
      chk_without_comissions.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_NOT_APPLY_COMISSION");


      lbl_total_to_paid.Text = Resource.String("STR_VOUCHER_CASH_ADVANCE_TOTAL_TO_PAY");
      lbl_total_to_advance.Text = Resource.String("STR_VOUCHER_CASH_ADVANCE_AMOUNT");
      lbl_comission.Text = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COMMISSION");

      m_calculation = CalculationType.None;

      m_amount_input = new frm_generic_amount_input();

      this.btn_ok.Enabled = false;

      m_amount_input.OnKeyPressed += new frm_generic_amount_input.KeyPressed(m_amount_input_OnKeyPress);


    }

    void m_amount_input_OnKeyPress(String Amount)
    {
      switch (m_calculation)
      {
        case CalculationType.Normal:
          txt_amount_to_advance.Text = Amount;
          break;

        case CalculationType.Inverse:
          txt_amount_paid.Text = Amount;
          break;

        default:
          break;
      }
    } // InitializeControlResources

    public Boolean Show(out CurrencyExchangeResult ExchangeResult, out PaymentThresholdAuthorization PaymentThresholdAuthorization, CardData CardData)
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_payment_method", Log.Type.Message);

      Point _main_form_location;
      Size _main_form_size;
      Int32 _form_location_X;
      Int32 _form_location_Y;
      Boolean _process_continue;
      PaymentAndRechargeThresholdData _payment_and_recharge_threshold_data;

      PaymentThresholdAuthorization = null;

      ExchangeResult = new CurrencyExchangeResult();
      txt_amount_paid.Text = Currency.Zero().ToString();
      txt_amount_to_advance.Text = Currency.Zero().ToString();
      txt_tax.Text = Currency.Zero().ToString();

      txt_amount_to_advance.BackColor = Color.LightGoldenrodYellow;

      this.StartPosition = FormStartPosition.Manual;
      this.CustomLocation = true;

      _main_form_size = WindowManager.GetMainFormAreaSize();
      _form_location_X = 0;
      _form_location_Y = 0;

      _main_form_location = WindowManager.GetMainFormAreaLocation();
      _form_location_X += 161; //96 + 65 (center location) + (Extra location X)
      _form_location_Y += _main_form_location.Y + (_main_form_size.Height - this.Width) / 2;
      this.Location = new Point(_form_location_X, _form_location_Y);

      DialogResult _dlg_rc = this.ShowDialog(Parent);

      if (_dlg_rc != DialogResult.OK)
      {
        return false;
      }
      
      _payment_and_recharge_threshold_data = new PaymentAndRechargeThresholdData();
      if (m_exchange_result != null)
      {
        _payment_and_recharge_threshold_data.TransactionType = m_exchange_result.InType;
        _payment_and_recharge_threshold_data.TransactionSubType = m_exchange_result.BankTransactionData.Type;
      }

      PaymentThresholdAuthorization = new PaymentThresholdAuthorization(m_exchange_result.GrossAmount, CardData, _payment_and_recharge_threshold_data, PaymentThresholdAuthorization.PaymentThresholdAuthorizationOperation.Output, this, out _process_continue);
      if (!_process_continue)
      {
        return false;
      }

      ExchangeResult = m_exchange_result;

      return true;
    }

    #endregion

    #region private

    //------------------------------------------------------------------------------
    // PURPOSE : Set flag corresponding with Iso Code
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    private void SetCurrenciesImage()
    {
      CurrencyExchangeProperties _properties;
      List<CurrencyExchangeType> _currency_types;
      List<CurrencyExchange> _currencies_exchange;
      Int32 _index;
      String _name;
      Boolean _distinct_bank_card;
      Boolean[] _list_of_visibles = { false, false, false };

      _index = -1;
      _currency_types = new List<CurrencyExchangeType>();
      _currency_types.Add(CurrencyExchangeType.CARD);
      _currency_types.Add(CurrencyExchangeType.CHECK);

      _distinct_bank_card = GeneralParam.GetBoolean("Cashier.PaymentMethod", "BankCard.DifferentiateType", false);

      CurrencyExchange.GetAllowedCurrencies(true, _currency_types, false, _distinct_bank_card, out m_national_currency, out _currencies_exchange);

      btn_currency.Image = null;
      btn_currency_1.Image = null;
      btn_currency_2.Image = null;
      btn_currency.Style = uc_round_button.RoundButonStyle.PRINCIPAL;
      btn_currency_1.Style = uc_round_button.RoundButonStyle.SECONDARY;
      btn_currency_2.Style = uc_round_button.RoundButonStyle.SECONDARY;

      btn_currency.IsSelected = true;

      pnl_currency.Visible = false;
      pnl_currency_1.Visible = false;
      pnl_currency_2.Visible = false;

      m_currencies = new Dictionary<String, CurrencyExchange>();
      foreach (CurrencyExchange _currency in _currencies_exchange)
      {
        if (_index > 2)
        {
          break;
        }

        _index++;
        _properties = CurrencyExchangeProperties.GetProperties(_currency.CurrencyCode);

        if (_properties == null)
        {
          continue;
        }

        _name = _currency.CurrencyCode + "-" + _currency.Type + "-" + _currency.SubType;
        m_currencies.Add(_name, _currency);

        switch (_index)
        {
          case 0:
            btn_currency.Tag = _name;
            btn_currency.Text = _currency.Description;
            btn_currency.Image = _properties.GetIcon(_currency.Type, 50, 50);
            pnl_currency.Visible = true;
            m_selected_currency = _currency;
            _list_of_visibles[_index] = true;
            break;

          case 1:
            btn_currency_1.Tag = _name;
            btn_currency_1.Text = _currency.Description;
            btn_currency_1.Image = _properties.GetIcon(_currency.Type, 50, 50, true);
            pnl_currency_1.Visible = true;
            _list_of_visibles[_index] = true;
            break;

          case 2:
            btn_currency_2.Tag = _name;
            btn_currency_2.Text = _currency.Description;
            btn_currency_2.Image = _properties.GetIcon(_currency.Type, 50, 50, true);
            pnl_currency_2.Visible = true;
            _list_of_visibles[_index] = true;
            break;
        }
      }

      if (!_list_of_visibles[2])
      {
        this.Height -= (Int32)tableLayoutPanel1.RowStyles[1].Height;
      }

      tableLayoutPanel1.ColumnStyles[0].Width = !_list_of_visibles[0] ? 0 : tableLayoutPanel1.ColumnStyles[0].Width;
      tableLayoutPanel1.ColumnStyles[1].Width = !_list_of_visibles[1] ? 0 : tableLayoutPanel1.ColumnStyles[1].Width;
      tableLayoutPanel1.RowStyles[1].Height = !_list_of_visibles[2] ? 0 : tableLayoutPanel1.RowStyles[1].Height;
    }

    /// <summary>
    /// Check and Add digit to amount
    /// </summary>
    /// <param name="NumberStr"></param>
    private void AddNumber(String NumberStr)
    {
      String aux_str;
      String tentative_number;
      Currency input_amount;
      String _aux_amount;
      int dec_symbol_pos;

      switch (m_calculation)
      {
        case CalculationType.Normal:
          _aux_amount = txt_amount_to_advance.Text;
          break;

        case CalculationType.Inverse:
          _aux_amount = txt_amount_paid.Text;
          break;

        default:
          return;
      }

      // Prevent exceeding maximum number of decimals (2)
      dec_symbol_pos = _aux_amount.IndexOf(m_decimal_str);
      if (dec_symbol_pos > -1)
      {
        aux_str = _aux_amount.Substring(dec_symbol_pos);
        if (aux_str.Length > 2)
        {
          return;
        }
      }

      // Prevent exceeding maximum amount length
      if (_aux_amount.Length >= Cashier.MAX_ALLOWED_AMOUNT_LENGTH)
      {
        return;
      }

      tentative_number = _aux_amount + NumberStr;

      try
      {
        input_amount = Decimal.Parse(tentative_number);
        if (input_amount > Cashier.MAX_ALLOWED_AMOUNT)
        {
          return;
        }
      }
      catch
      {
        input_amount = 0;

        return;
      }

      // Remove unneeded leading zeros
      // Unless we are entering a decimal value, there must not be any leading 0
      if (dec_symbol_pos == -1)
      {
        _aux_amount = _aux_amount.TrimStart('0') + NumberStr;
      }
      else
      {
        // Accept new symbol/digit 
        _aux_amount = _aux_amount + NumberStr;
      }

      switch (m_calculation)
      {
        case CalculationType.Normal:
          txt_amount_to_advance.Text = _aux_amount;
          break;

        case CalculationType.Inverse:
          txt_amount_paid.Text = _aux_amount;
          break;
      }
    }

    /// <summary>
    /// Calculate exchange result
    /// </summary>
    /// <returns>CurrencyExchangeResult</returns>
    private CurrencyExchangeResult CalculateExchange()
    {
      Decimal _amount;
      CurrencyExchangeResult _exchange_result;
      Decimal _total_to_pay;
      Decimal _total_to_advance;

      _total_to_pay = 0;
      _total_to_advance = 0;
      _amount = 0;
      _exchange_result = new CurrencyExchangeResult();

      
      switch (m_calculation)
      {
        case CalculationType.Normal:
          _amount = Format.ParseCurrency(this.txt_amount_to_advance.Text);
          break;

        case CalculationType.Inverse:
          _amount = Format.ParseCurrency(this.txt_amount_paid.Text);
          break;
      }

      if (_amount == 0 || m_selected_currency == null)
      {
        this.txt_tax.Text = Currency.Zero().ToString();
        this.btn_ok.Enabled = false;

        return null;
      }
      else
      {
        this.btn_ok.Enabled = true;
      }

      txt_amount_paid.TextChanged -= new System.EventHandler(this.txt_amount_TextChanged);
      txt_amount_to_advance.TextChanged -= new System.EventHandler(this.txt_amount_to_advance_TextChanged);

      try
      {
        switch (m_calculation)
        {
          case CalculationType.Normal:
            m_selected_currency.ApplyExchange(_amount, out _exchange_result, true);
            if (m_waive_commission)
            {
              _exchange_result.GrossAmount = _exchange_result.NetAmount;
              _exchange_result.InAmount = _exchange_result.NetAmount;
              _exchange_result.Comission = 0;
            }
            break;

          case CalculationType.Inverse:
            m_selected_currency.InverseApplyExchangeForCashAdvance(_amount, out _exchange_result);
            if (m_waive_commission)
            {
             _exchange_result.NetAmountSplit1 =  _exchange_result.GrossAmount; 
              _exchange_result.Comission = 0;
            }
            break;
        }

        switch (m_calculation)
        {
          case CalculationType.Normal:
            txt_amount_paid.Text = ((Currency)_exchange_result.GrossAmount).ToString();
            break;

          case CalculationType.Inverse:
            if (_exchange_result.NetAmount > 0)
            {
              txt_amount_to_advance.Text = ((Currency)_exchange_result.NetAmount).ToString();
            }
            else
            {
              txt_amount_to_advance.Text = ((Currency)0).ToString();
            }
            break;
        }

        this.txt_tax.Text = ((Currency)_exchange_result.Comission).ToString();

        _total_to_pay = Format.ParseCurrency(this.txt_amount_paid.Text);
        _total_to_advance = Format.ParseCurrency(this.txt_amount_to_advance.Text);

        if (_total_to_pay == 0 || _total_to_advance == 0)
        {
          this.btn_ok.Enabled = false;
        }
        _exchange_result.SubType = m_selected_currency.SubType;
      }
      finally
      {
        txt_amount_paid.TextChanged += new System.EventHandler(this.txt_amount_TextChanged);
        txt_amount_to_advance.TextChanged += new System.EventHandler(this.txt_amount_to_advance_TextChanged);
      }

      return _exchange_result;
    }

    private void ShowOrCreate()
    {
      Decimal _amount;
      Boolean result;
      Currency _amount_old;
      Int32 _form_location_X;

      if (this.m_amount_input == null || this.m_amount_input.IsDisposed)
      {
        this.m_amount_input = new frm_generic_amount_input();
        this.m_amount_input.OnKeyPressed += new frm_generic_amount_input.KeyPressed(m_amount_input_OnKeyPress);
      }

      this.m_amount_input.StartPosition = FormStartPosition.Manual;
      this.m_amount_input.CustomLocation = true;

      _form_location_X = 607;//542 + 65 (with frm:payment_method + center location) + (Extra location X)
      m_amount_input.Location = new Point(_form_location_X, this.Location.Y);
      this.m_amount_input.BringToFront();

      switch (m_calculation)
      {
        case CalculationType.Normal:
          _amount = Format.ParseCurrency(txt_amount_to_advance.Text);
          break;

        case CalculationType.Inverse:
          _amount = Format.ParseCurrency(txt_amount_paid.Text);
          break;

        default:
          _amount = Format.ParseCurrency(txt_amount_paid.Text);
          break;
      }
      _amount_old = _amount;

      result = this.m_amount_input.Show(GENERIC_AMOUNT_INPUT_TYPE.AMOUNT, this.FormTitle, ref _amount);//ferran

      // JBC 08-OCT-2014 If it's same amount, don't set value on textbox (setting value on textbox throws keypress event and recalculate Exchange).
      if (_amount_old != _amount)
      {
        if (result)
        {
          switch (m_calculation)
          {
            case CalculationType.Normal:
              this.txt_amount_paid.Text = ((Currency)_amount).ToStringWithoutSymbols();
              break;

            case CalculationType.Inverse:
              this.txt_amount_to_advance.Text = ((Currency)_amount).ToStringWithoutSymbols();
              break;

            default:
              this.txt_amount_paid.Text = ((Currency)_amount).ToStringWithoutSymbols();
              break;
          }
        }
        this.m_amount_input.BringToFront();
        this.Modify = false;
      }
    }

    #endregion

    #region Events

    private void btn_currency_Click(object sender, EventArgs e)
    {
      uc_round_button _btn;


      _btn = ((uc_round_button)sender);

      _btn.Style = uc_round_button.RoundButonStyle.SECONDARY;

      m_selected_currency = m_currencies[(String)_btn.Tag];

      UpdateButtonStyle();

      btn_currency.Refresh();
      btn_currency_1.Refresh();
      btn_currency_2.Refresh();

      CalculateExchange();

      switch (m_calculation)
      {
        case CalculationType.Normal:
          this.txt_amount_to_advance_Click(this.txt_amount_to_advance, new MouseEventArgs(System.Windows.Forms.MouseButtons.Left, 1, 1, 1, 1));
          break;

        case CalculationType.Inverse:
          this.txt_amount_paid_Click(this.txt_amount_paid, new MouseEventArgs(System.Windows.Forms.MouseButtons.Left, 1, 1, 1, 1));
          break;
      }

    }

    private void UpdateButtonStyle()
    {

      if (m_currencies.Count > 2)
      {
        RedrawVariousCurrenciesButtonsStyle();
      }
      else
      {
        // Set default currecy types to buttons
        RedrawOneCurrencyButtonStyle();
      }

    }

    private void RedrawVariousCurrenciesButtonsStyle()
    {
      // Set default currecy types to buttons
      btn_currency.Image = CurrencyExchangeProperties.GetProperties(m_currencies[(String)btn_currency.Tag].CurrencyCode).GetIcon(m_currencies[(String)btn_currency.Tag].Type, 50, 50, true);
      btn_currency_1.Image = CurrencyExchangeProperties.GetProperties(m_currencies[(String)btn_currency_1.Tag].CurrencyCode).GetIcon(m_currencies[(String)btn_currency_1.Tag].Type, 50, 50, true);
      btn_currency_2.Image = CurrencyExchangeProperties.GetProperties(m_currencies[(String)btn_currency_2.Tag].CurrencyCode).GetIcon(m_currencies[(String)btn_currency_2.Tag].Type, 50, 50, true);

      btn_currency.IsSelected = false;
      btn_currency_1.IsSelected = false;
      btn_currency_2.IsSelected = false;

      switch (m_selected_currency.SubType)
      {
        case CurrencyExchangeSubType.NONE:
        case CurrencyExchangeSubType.CHECK:
          RedrawCheckStyle();

          break;

        case CurrencyExchangeSubType.BANK_CARD:
          break;
        case CurrencyExchangeSubType.CREDIT_CARD:
          RedrawCreditCardStyle();

          break;
        case CurrencyExchangeSubType.DEBIT_CARD:
          RedrawDebitCardStyle();

          break;
        default:

          break;
      }
    }

    private void RedrawOneCurrencyButtonStyle()
    {
      btn_currency.Image = CurrencyExchangeProperties.GetProperties(m_currencies[(String)btn_currency.Tag].CurrencyCode).GetIcon(m_currencies[(String)btn_currency.Tag].Type, 50, 50, true);
      btn_currency.IsSelected = false;
      btn_currency.Style = uc_round_button.RoundButonStyle.PRINCIPAL;
      if (m_currencies.Count > 1)
      {
        btn_currency_1.Image = CurrencyExchangeProperties.GetProperties(m_currencies[(String)btn_currency_1.Tag].CurrencyCode).GetIcon(m_currencies[(String)btn_currency_1.Tag].Type, 50, 50, true);
        btn_currency_1.IsSelected = false;
        switch (m_selected_currency.Type)
        {
          case CurrencyExchangeType.CARD:

            if (m_selected_currency.SubType != CurrencyExchangeSubType.NONE)
            {
              if (m_selected_currency.SubType == CurrencyExchangeSubType.CREDIT_CARD)
              {
                RedrawCreditCardStyle();
              }
              else
              {
                RedrawDebitCardStyle();
              }
            }
            else
            {
              RedrawNoneCardStyle();

            }
            break;
          case CurrencyExchangeType.CHECK:
            RedrawCheckStyle();

            break;
          case CurrencyExchangeType.CURRENCY:
          case CurrencyExchangeType.CASINOCHIP:
          case CurrencyExchangeType.FREE:
          default:
            break;
        }
      }
    }

    private void RedrawNoneCardStyle()
    {
      btn_currency.Style = uc_round_button.RoundButonStyle.PRINCIPAL;
      btn_currency_1.Style = uc_round_button.RoundButonStyle.SECONDARY;
      btn_currency_2.Style = uc_round_button.RoundButonStyle.SECONDARY;

      btn_currency.Image = CurrencyExchangeProperties.GetProperties(m_currencies[(String)btn_currency.Tag].CurrencyCode).GetIcon(m_currencies[(String)btn_currency.Tag].Type, 50, 50);
      btn_currency.IsSelected = true;
    }

    private void RedrawDebitCardStyle()
    {
      btn_currency.Style = uc_round_button.RoundButonStyle.SECONDARY;
      btn_currency_1.Style = uc_round_button.RoundButonStyle.PRINCIPAL;
      btn_currency_2.Style = uc_round_button.RoundButonStyle.SECONDARY;

      btn_currency_1.Image = CurrencyExchangeProperties.GetProperties(m_currencies[(String)btn_currency_1.Tag].CurrencyCode).GetIcon(m_currencies[(String)btn_currency_1.Tag].Type, 50, 50);
      btn_currency_1.IsSelected = true;
    }

    private void RedrawCreditCardStyle()
    {
      btn_currency.Style = uc_round_button.RoundButonStyle.PRINCIPAL;
      btn_currency_1.Style = uc_round_button.RoundButonStyle.SECONDARY;
      btn_currency_2.Style = uc_round_button.RoundButonStyle.SECONDARY;

      btn_currency.Image = CurrencyExchangeProperties.GetProperties(m_currencies[(String)btn_currency.Tag].CurrencyCode).GetIcon(m_currencies[(String)btn_currency.Tag].Type, 50, 50);
      btn_currency.IsSelected = true;
    }

    private void RedrawCheckStyle()
    {
      btn_currency.Style = uc_round_button.RoundButonStyle.SECONDARY;
      btn_currency_1.Style = uc_round_button.RoundButonStyle.SECONDARY;
      btn_currency_2.Style = uc_round_button.RoundButonStyle.PRINCIPAL;

      // CHECK button is the second button
      if (btn_currency_1.Tag != null && btn_currency_1.Tag.ToString().Contains(CurrencyExchangeType.CHECK.ToString()))
      {
        btn_currency_1.Image = CurrencyExchangeProperties.GetProperties(m_currencies[(String)btn_currency_1.Tag].CurrencyCode).GetIcon(m_currencies[(String)btn_currency_1.Tag].Type, 50, 50);
        btn_currency_1.IsSelected = true;
      }

      // CHECK button is the third button
      if (btn_currency_2.Tag != null && btn_currency_2.Tag.ToString().Contains(CurrencyExchangeType.CHECK.ToString()))
      {
        btn_currency_2.Image = CurrencyExchangeProperties.GetProperties(m_currencies[(String)btn_currency_2.Tag].CurrencyCode).GetIcon(m_currencies[(String)btn_currency_2.Tag].Type, 50, 50);
        btn_currency_2.IsSelected = true;
      }
    }




    
    private void frm_payment_method_KeyPress(object sender, KeyPressEventArgs e)
    {
      String aux_str;
      uc_round_textbox _txt_aux;

      switch (m_calculation)
      {
        case CalculationType.Normal:
          _txt_aux = txt_amount_to_advance;

          break;

        case CalculationType.Inverse:
          _txt_aux = txt_amount_paid;
          break;

        default:
          return;
      }


      char c;

      c = e.KeyChar;

      if (c >= '0' && c <= '9')
      {
        aux_str = "" + c;
        AddNumber(aux_str);
        e.Handled = true;
      }

      if (c == '\b')
      {
        if (_txt_aux.Text.Length > 0)
        {
          _txt_aux.Text = _txt_aux.Text.Substring(0, _txt_aux.Text.Length - 1);
        }
      }

      if (c == '.')
      {
        if (_txt_aux.Text.IndexOf(m_decimal_str) == -1)
        {
          _txt_aux.Text = _txt_aux.Text + m_decimal_str;
        }
      }

      e.Handled = true;
    }

    private void frm_payment_method_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
    {
      char c;

      c = Convert.ToChar(e.KeyCode);
      if (c == '\r')
      {
        e.IsInputKey = true;
      }
    }

    private void btn_ok_Click(object sender, EventArgs e)
    {
      frm_bank_transaction_data _frm_bank_transaction;
      Boolean _save_bank_transaction;
      Currency _session_balance;
      
      String _error_str;

      // EOR 14-JUL-2016  
      Decimal _cash_advance_limit_amount; 
      List<ProfilePermissions.CashierFormFuncionality> _permissions_list;
     


      m_exchange_result = CalculateExchange();

      _session_balance = 0;


      try
      {
        if (m_waive_commission)
        {
          if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.CashAdvandce_AllowNoComission, ProfilePermissions.TypeOperation.RequestPasswd, this.m_ParentForm, false, out _error_str))
          {
            return;
          }
        }
        
        if (PaymentThresholdAuthorization.GetOutputThreshold() > 0 && PaymentThresholdAuthorization.GetOutputThreshold() <= m_exchange_result.GrossAmount)
        {
          if (!ProfilePermissions.CheckPermissionList(ProfilePermissions.CashierFormFuncionality.TITO_PaymentThreshold,
                                              ProfilePermissions.TypeOperation.RequestPasswd,
                                              this.m_ParentForm,
                                              0,
                                              out _error_str))
          {
            return;
          }
        }

        using (DB_TRX _db_trx = new DB_TRX())
        {
          _session_balance = CashierBusinessLogic.UpdateSessionBalance(_db_trx.SqlTransaction, Cashier.SessionId, 0);
        }
        if (_session_balance < m_exchange_result.NetAmount)
        {
          frm_message.Show(Resource.String("STR_FRM_AMOUNT_INPUT_MSG_AMOUNT_EXCEED_BANK"),
                           Resource.String("STR_APP_GEN_MSG_WARNING"),
                           MessageBoxButtons.OK,
                           MessageBoxIcon.Error,
                           ParentForm);

          return;
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);

        return;
      }

      // EOR 14-JUL-2016: Cash Advance with Bank Card
      if (m_exchange_result.InType == CurrencyExchangeType.CARD)
      {
        _cash_advance_limit_amount = GeneralParam.GetDecimal("Cashier.PaymentMethod", "BankCard.CashAdvance.Limit.Amount", 0);
        _permissions_list = new List<ProfilePermissions.CashierFormFuncionality>();
        _permissions_list.Add(ProfilePermissions.CashierFormFuncionality.Credit_card_cash_advance);

        if (_cash_advance_limit_amount > 0 & m_exchange_result.NetAmount > _cash_advance_limit_amount)
        {
          if (!ProfilePermissions.CheckPermissionList(_permissions_list,
                                                      ProfilePermissions.TypeOperation.RequestPasswd,
                                                      this,
                                                      Cashier.AuthorizedByUserId,
                                                      out _error_str))
          {
            return;
          }
        }
      }

      switch (m_exchange_result.InType)
      {
        case CurrencyExchangeType.CARD:
          _save_bank_transaction = GeneralParam.GetBoolean("Cashier.PaymentMethod", "BankCard.EnterTransactionDetails");
          break;
        case CurrencyExchangeType.CHECK:
          _save_bank_transaction = GeneralParam.GetBoolean("Cashier.PaymentMethod", "Check.EnterTransactionDetails");
          break;
        default:
          _save_bank_transaction = false;
          break;
      }

      if (WSI.Common.TITO.Utils.IsTitoMode() && PaymentThresholdAuthorization.GetOutputThreshold() > 0 && !_save_bank_transaction && (m_exchange_result.InType == CurrencyExchangeType.CARD || m_exchange_result.InType == CurrencyExchangeType.CHECK))
      {
        _save_bank_transaction = (m_exchange_result.GrossAmount >= PaymentThresholdAuthorization.GetOutputThreshold());
      }

      if (_save_bank_transaction)
      {
        _frm_bank_transaction = new frm_bank_transaction_data(m_exchange_result.InType);
        m_exchange_result.BankTransactionData.TransactionType = TransactionType.CASH_ADVANCE;

        DialogResult _rc = _frm_bank_transaction.Show(ref m_exchange_result);
        _frm_bank_transaction.Dispose();

        if (_rc != DialogResult.OK)
        {
          return;
        }
      }

      DialogResult = DialogResult.OK;

      Misc.WriteLog("[FORM CLOSE] frm_payment_method (ok)", Log.Type.Message);
      this.Close();

    }

    private void txt_amount_paid_Enter(object sender, EventArgs e)
    {
      Currency _value;

      m_calculation = CalculationType.Inverse;

      txt_amount_paid.TextChanged -= new System.EventHandler(this.txt_amount_TextChanged);
      txt_amount_to_advance.TextChanged -= new System.EventHandler(this.txt_amount_to_advance_TextChanged);

      _value = Format.ParseCurrency(this.txt_amount_paid.Text);
      txt_amount_paid.Text = _value.ToStringWithoutSymbols();

      _value = Format.ParseCurrency(this.txt_amount_to_advance.Text);
      txt_amount_to_advance.Text = _value.ToString();

      txt_amount_paid.TextChanged += new System.EventHandler(this.txt_amount_TextChanged);
      txt_amount_to_advance.TextChanged += new System.EventHandler(this.txt_amount_to_advance_TextChanged);
    }

    private void txt_amount_to_advance_Enter(object sender, EventArgs e)
    {
      Currency _value;

      m_calculation = CalculationType.Normal;

      txt_amount_paid.TextChanged -= new System.EventHandler(this.txt_amount_TextChanged);
      txt_amount_to_advance.TextChanged -= new System.EventHandler(this.txt_amount_to_advance_TextChanged);

      _value = Format.ParseCurrency(this.txt_amount_to_advance.Text);
      txt_amount_to_advance.Text = _value.ToStringWithoutSymbols();

      _value = Format.ParseCurrency(this.txt_amount_paid.Text);
      txt_amount_paid.Text = _value.ToString();

      txt_amount_paid.TextChanged += new System.EventHandler(this.txt_amount_TextChanged);
      txt_amount_to_advance.TextChanged += new System.EventHandler(this.txt_amount_to_advance_TextChanged);

    }

    private void txt_amount_to_advance_TextChanged(object sender, EventArgs e)
    {
      CalculateExchange();
    }

    private void txt_amount_TextChanged(object sender, EventArgs e)
    {
      CalculateExchange();
    }

    private void txt_amount_to_advance_Click(object sender, EventArgs e)
    {
      this.txt_amount_to_advance.BackColor = Color.LightGoldenrodYellow;
      this.txt_amount_paid.Style = uc_round_textbox.RoundTextBoxStyle.BASIC_CUSTOM;

      ShowOrCreate();

      txt_amount_to_advance.TextChanged -= new System.EventHandler(this.txt_amount_to_advance_TextChanged);
      txt_amount_to_advance.Text = ((Currency)Format.ParseCurrency(txt_amount_to_advance.Text)).ToStringWithoutSymbols();
      txt_amount_to_advance.TextChanged += new System.EventHandler(this.txt_amount_to_advance_TextChanged);
    }

    private void txt_amount_paid_Click(object sender, EventArgs e)
    {
      this.txt_amount_to_advance.Style = uc_round_textbox.RoundTextBoxStyle.BASIC_CUSTOM;
      this.txt_amount_paid.BackColor = Color.LightGoldenrodYellow;

      ShowOrCreate();

      txt_amount_paid.TextChanged -= new System.EventHandler(this.txt_amount_TextChanged);
      txt_amount_paid.Text = ((Currency)Format.ParseCurrency(txt_amount_paid.Text)).ToStringWithoutSymbols();
      txt_amount_paid.TextChanged += new System.EventHandler(this.txt_amount_TextChanged);
    }

    private void frm_payment_method_Activated(object sender, EventArgs e)
    {
      if (m_first_time)
      {
        this.Location = new Point(this.Location.X - 11, this.Location.Y);
        txt_amount_to_advance.Focus();
        m_first_time = false;
      }


      if (!Modify)
      {
        Modify = false;

        if (m_amount_input != null)
        {
          m_amount_input.Hide();
          m_amount_input.Dispose();
          m_amount_input = null;
        }

      }

    }

    private void chk_without_comissions_Click(object sender, EventArgs e)
    {
      m_waive_commission = !m_waive_commission;
      if (m_waive_commission)
      {
        chk_without_comissions.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_APPLY_COMISSION");
      }
      else
      {
        chk_without_comissions.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_NOT_APPLY_COMISSION");
      }
      CalculateExchange();

    }

    #endregion  

  }
}