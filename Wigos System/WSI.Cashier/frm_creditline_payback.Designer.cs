﻿namespace WSI.Cashier
{
  partial class frm_creditline_payback
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.uc_payment_method1 = new WSI.Cashier.uc_payment_method();
      this.lbl_payback = new WSI.Cashier.Controls.uc_label();
      this.btn_ok = new WSI.Cashier.Controls.uc_round_button();
      this.btn_cancel = new WSI.Cashier.Controls.uc_round_button();
      this.lbl_credit_spent_value = new WSI.Cashier.Controls.uc_label();
      this.lbl_credit_spent = new WSI.Cashier.Controls.uc_label();
      this.uc_payback_amount = new WSI.Cashier.Controls.uc_round_textbox_padnumber();
      this.gb_exchange_info = new WSI.Cashier.Controls.uc_round_panel();
      this.lbl_total = new WSI.Cashier.Controls.uc_label();
      this.lbl_total_amount = new WSI.Cashier.Controls.uc_label();
      this.lbl_comission_value = new WSI.Cashier.Controls.uc_label();
      this.lbl_payback_value = new WSI.Cashier.Controls.uc_label();
      this.lbl_pending = new WSI.Cashier.Controls.uc_label();
      this.lbl_payback_amount = new WSI.Cashier.Controls.uc_label();
      this.lbl_comission = new WSI.Cashier.Controls.uc_label();
      this.lbl_pending_amount = new WSI.Cashier.Controls.uc_label();
      this.pnl_data.SuspendLayout();
      this.gb_exchange_info.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.Controls.Add(this.gb_exchange_info);
      this.pnl_data.Controls.Add(this.uc_payback_amount);
      this.pnl_data.Controls.Add(this.lbl_payback);
      this.pnl_data.Controls.Add(this.btn_ok);
      this.pnl_data.Controls.Add(this.btn_cancel);
      this.pnl_data.Controls.Add(this.uc_payment_method1);
      this.pnl_data.Size = new System.Drawing.Size(583, 563);
      // 
      // uc_payment_method1
      // 
      this.uc_payment_method1.BtnSelectedCurrencyEnabled = true;
      this.uc_payment_method1.Location = new System.Drawing.Point(99, 18);
      this.uc_payment_method1.Name = "uc_payment_method1";
      this.uc_payment_method1.showPaymentCreditLine = false;
      this.uc_payment_method1.Size = new System.Drawing.Size(385, 116);
      this.uc_payment_method1.TabIndex = 48;
      this.uc_payment_method1.CurrencyChanged += new WSI.Cashier.uc_payment_method.CurrencyChangedHandler(this.uc_payment_method1_CurrencyChanged);
      // 
      // lbl_payback
      // 
      this.lbl_payback.BackColor = System.Drawing.Color.Transparent;
      this.lbl_payback.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_payback.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_payback.Location = new System.Drawing.Point(14, 158);
      this.lbl_payback.Name = "lbl_payback";
      this.lbl_payback.Size = new System.Drawing.Size(173, 32);
      this.lbl_payback.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.lbl_payback.TabIndex = 51;
      this.lbl_payback.Text = "xPayback";
      this.lbl_payback.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // btn_ok
      // 
      this.btn_ok.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_ok.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_ok.FlatAppearance.BorderSize = 0;
      this.btn_ok.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_ok.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_ok.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_ok.Image = null;
      this.btn_ok.IsSelected = false;
      this.btn_ok.Location = new System.Drawing.Point(398, 474);
      this.btn_ok.Name = "btn_ok";
      this.btn_ok.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_ok.Size = new System.Drawing.Size(155, 60);
      this.btn_ok.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_ok.TabIndex = 50;
      this.btn_ok.Text = "XOK";
      this.btn_ok.UseVisualStyleBackColor = false;
      this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
      // 
      // btn_cancel
      // 
      this.btn_cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_cancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btn_cancel.FlatAppearance.BorderSize = 0;
      this.btn_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_cancel.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_cancel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_cancel.Image = null;
      this.btn_cancel.IsSelected = false;
      this.btn_cancel.Location = new System.Drawing.Point(231, 474);
      this.btn_cancel.Name = "btn_cancel";
      this.btn_cancel.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_cancel.Size = new System.Drawing.Size(155, 60);
      this.btn_cancel.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_cancel.TabIndex = 49;
      this.btn_cancel.Text = "XCANCEL";
      this.btn_cancel.UseVisualStyleBackColor = false;
      this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
      // 
      // lbl_credit_spent_value
      // 
      this.lbl_credit_spent_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_credit_spent_value.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_credit_spent_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_credit_spent_value.Location = new System.Drawing.Point(293, 43);
      this.lbl_credit_spent_value.Name = "lbl_credit_spent_value";
      this.lbl_credit_spent_value.Size = new System.Drawing.Size(148, 23);
      this.lbl_credit_spent_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_credit_spent_value.TabIndex = 54;
      this.lbl_credit_spent_value.Text = "xSpentValue";
      this.lbl_credit_spent_value.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_credit_spent
      // 
      this.lbl_credit_spent.BackColor = System.Drawing.Color.Transparent;
      this.lbl_credit_spent.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_credit_spent.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_credit_spent.Location = new System.Drawing.Point(6, 41);
      this.lbl_credit_spent.Name = "lbl_credit_spent";
      this.lbl_credit_spent.Size = new System.Drawing.Size(255, 25);
      this.lbl_credit_spent.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.lbl_credit_spent.TabIndex = 53;
      this.lbl_credit_spent.Text = "xCreditSpent";
      this.lbl_credit_spent.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // uc_payback_amount
      // 
      this.uc_payback_amount.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.uc_payback_amount.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.uc_payback_amount.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.uc_payback_amount.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.uc_payback_amount.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.uc_payback_amount.CornerRadius = 5;
      this.uc_payback_amount.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.uc_payback_amount.Location = new System.Drawing.Point(189, 153);
      this.uc_payback_amount.MaxLength = 10;
      this.uc_payback_amount.Multiline = false;
      this.uc_payback_amount.Name = "uc_payback_amount";
      this.uc_payback_amount.PadNumberAmountInputType = WSI.Common.GENERIC_AMOUNT_INPUT_TYPE.CURRENCY;
      this.uc_payback_amount.PadNumberHorizontalPosition = WSI.Cashier.Controls.PadNumberHorizontalPosition.RIGHT;
      this.uc_payback_amount.PadNumberLocation = null;
      this.uc_payback_amount.PadNumberStartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.uc_payback_amount.PadNumberVerticalPosition = WSI.Cashier.Controls.PadNumberVerticalPosition.BOTTOM;
      this.uc_payback_amount.PasswordChar = '\0';
      this.uc_payback_amount.ReadOnly = false;
      this.uc_payback_amount.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.uc_payback_amount.SelectedText = "";
      this.uc_payback_amount.SelectionLength = 0;
      this.uc_payback_amount.SelectionStart = 0;
      this.uc_payback_amount.ShowPadNumberOnEnter = true;
      this.uc_payback_amount.Size = new System.Drawing.Size(204, 40);
      this.uc_payback_amount.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.NONE;
      this.uc_payback_amount.TabIndex = 55;
      this.uc_payback_amount.TabStop = false;
      this.uc_payback_amount.Text = "0";
      this.uc_payback_amount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      this.uc_payback_amount.UseSystemPasswordChar = false;
      this.uc_payback_amount.WaterMark = null;
      this.uc_payback_amount.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      this.uc_payback_amount.TextChanged += new System.EventHandler(this.uc_payback_amount_TextChanged);
      this.uc_payback_amount.Enter += new System.EventHandler(this.uc_payback_amount_Enter);
      this.uc_payback_amount.Leave += new System.EventHandler(this.uc_payback_amount_Leave);
      // 
      // gb_exchange_info
      // 
      this.gb_exchange_info.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.gb_exchange_info.BackColor = System.Drawing.Color.Transparent;
      this.gb_exchange_info.BorderColor = System.Drawing.Color.Empty;
      this.gb_exchange_info.Controls.Add(this.lbl_total);
      this.gb_exchange_info.Controls.Add(this.lbl_total_amount);
      this.gb_exchange_info.Controls.Add(this.lbl_comission_value);
      this.gb_exchange_info.Controls.Add(this.lbl_payback_value);
      this.gb_exchange_info.Controls.Add(this.lbl_credit_spent_value);
      this.gb_exchange_info.Controls.Add(this.lbl_pending);
      this.gb_exchange_info.Controls.Add(this.lbl_payback_amount);
      this.gb_exchange_info.Controls.Add(this.lbl_comission);
      this.gb_exchange_info.Controls.Add(this.lbl_credit_spent);
      this.gb_exchange_info.Controls.Add(this.lbl_pending_amount);
      this.gb_exchange_info.CornerRadius = 10;
      this.gb_exchange_info.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_exchange_info.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gb_exchange_info.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_exchange_info.HeaderHeight = 35;
      this.gb_exchange_info.HeaderSubText = null;
      this.gb_exchange_info.HeaderText = "XCURRENCYDATA";
      this.gb_exchange_info.Location = new System.Drawing.Point(30, 216);
      this.gb_exchange_info.Name = "gb_exchange_info";
      this.gb_exchange_info.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_exchange_info.Size = new System.Drawing.Size(523, 233);
      this.gb_exchange_info.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NONE;
      this.gb_exchange_info.TabIndex = 56;
      this.gb_exchange_info.Text = "xLast Game Session";
      // 
      // lbl_total
      // 
      this.lbl_total.BackColor = System.Drawing.Color.Transparent;
      this.lbl_total.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_total.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_total.Location = new System.Drawing.Point(6, 186);
      this.lbl_total.Name = "lbl_total";
      this.lbl_total.Size = new System.Drawing.Size(255, 25);
      this.lbl_total.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLUE;
      this.lbl_total.TabIndex = 55;
      this.lbl_total.Text = "xGrosAmount";
      this.lbl_total.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_total_amount
      // 
      this.lbl_total_amount.BackColor = System.Drawing.Color.Transparent;
      this.lbl_total_amount.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_total_amount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_total_amount.Location = new System.Drawing.Point(273, 176);
      this.lbl_total_amount.Name = "lbl_total_amount";
      this.lbl_total_amount.Size = new System.Drawing.Size(170, 35);
      this.lbl_total_amount.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_24PX_BLACK;
      this.lbl_total_amount.TabIndex = 56;
      this.lbl_total_amount.Text = "xNetAmountValue";
      this.lbl_total_amount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_comission_value
      // 
      this.lbl_comission_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_comission_value.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_comission_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_comission_value.Location = new System.Drawing.Point(293, 106);
      this.lbl_comission_value.Name = "lbl_comission_value";
      this.lbl_comission_value.Size = new System.Drawing.Size(148, 23);
      this.lbl_comission_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_comission_value.TabIndex = 9;
      this.lbl_comission_value.Text = "xComissionValue";
      this.lbl_comission_value.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_payback_value
      // 
      this.lbl_payback_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_payback_value.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_payback_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_payback_value.Location = new System.Drawing.Point(293, 74);
      this.lbl_payback_value.Name = "lbl_payback_value";
      this.lbl_payback_value.Size = new System.Drawing.Size(148, 23);
      this.lbl_payback_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_payback_value.TabIndex = 8;
      this.lbl_payback_value.Text = "xPaybackValue";
      this.lbl_payback_value.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_pending
      // 
      this.lbl_pending.BackColor = System.Drawing.Color.Transparent;
      this.lbl_pending.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_pending.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_pending.Location = new System.Drawing.Point(6, 139);
      this.lbl_pending.Name = "lbl_pending";
      this.lbl_pending.Size = new System.Drawing.Size(255, 25);
      this.lbl_pending.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.lbl_pending.TabIndex = 0;
      this.lbl_pending.Text = "xPendingAmount";
      this.lbl_pending.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_payback_amount
      // 
      this.lbl_payback_amount.BackColor = System.Drawing.Color.Transparent;
      this.lbl_payback_amount.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_payback_amount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_payback_amount.Location = new System.Drawing.Point(6, 75);
      this.lbl_payback_amount.Name = "lbl_payback_amount";
      this.lbl_payback_amount.Size = new System.Drawing.Size(255, 25);
      this.lbl_payback_amount.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.lbl_payback_amount.TabIndex = 2;
      this.lbl_payback_amount.Text = "xPaybackAmount";
      this.lbl_payback_amount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_comission
      // 
      this.lbl_comission.BackColor = System.Drawing.Color.Transparent;
      this.lbl_comission.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_comission.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_comission.Location = new System.Drawing.Point(6, 107);
      this.lbl_comission.Name = "lbl_comission";
      this.lbl_comission.Size = new System.Drawing.Size(255, 25);
      this.lbl_comission.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.lbl_comission.TabIndex = 3;
      this.lbl_comission.Text = "xComission";
      this.lbl_comission.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_pending_amount
      // 
      this.lbl_pending_amount.BackColor = System.Drawing.Color.Transparent;
      this.lbl_pending_amount.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_pending_amount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_pending_amount.Location = new System.Drawing.Point(293, 138);
      this.lbl_pending_amount.Name = "lbl_pending_amount";
      this.lbl_pending_amount.Size = new System.Drawing.Size(148, 23);
      this.lbl_pending_amount.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_pending_amount.TabIndex = 1;
      this.lbl_pending_amount.Text = "xPendingAmount";
      this.lbl_pending_amount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // frm_creditline_payback
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(583, 618);
      this.Name = "frm_creditline_payback";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "frm_creditline_payback";
      this.pnl_data.ResumeLayout(false);
      this.gb_exchange_info.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private uc_payment_method uc_payment_method1;
    private Controls.uc_label lbl_payback;
    private Controls.uc_round_button btn_ok;
    private Controls.uc_round_button btn_cancel;
    private Controls.uc_label lbl_credit_spent_value;
    private Controls.uc_label lbl_credit_spent;
    private Controls.uc_round_textbox_padnumber uc_payback_amount;
    private Controls.uc_round_panel gb_exchange_info;
    private Controls.uc_label lbl_pending;
    private Controls.uc_label lbl_pending_amount;
    private Controls.uc_label lbl_payback_amount;
    private Controls.uc_label lbl_comission;
    private Controls.uc_label lbl_payback_value;
    private Controls.uc_label lbl_comission_value;
    private Controls.uc_label lbl_total;
    private Controls.uc_label lbl_total_amount;
  }
}