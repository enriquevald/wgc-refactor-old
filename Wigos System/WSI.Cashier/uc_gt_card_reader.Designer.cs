﻿namespace WSI.Cashier
{
  partial class uc_gt_card_reader
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(uc_gt_card_reader));
      this.tmr_clear = new System.Windows.Forms.Timer(this.components);
      this.lbl_swipe_card = new WSI.Cashier.Controls.uc_label();
      this.lbl_invalid_card = new WSI.Cashier.Controls.uc_label();
      this.pictureBox1 = new System.Windows.Forms.PictureBox();
      this.txt_track_number = new WSI.Cashier.Controls.uc_round_textbox();
      this.btn_player_unknown = new WSI.Cashier.Controls.uc_round_button();
      this.btn_player_browse = new WSI.Cashier.Controls.uc_round_button();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
      this.SuspendLayout();
      // 
      // tmr_clear
      // 
      this.tmr_clear.Interval = 250;
      this.tmr_clear.Tick += new System.EventHandler(this.tmr_clear_Tick);
      // 
      // lbl_swipe_card
      // 
      this.lbl_swipe_card.BackColor = System.Drawing.Color.Transparent;
      this.lbl_swipe_card.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_swipe_card.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_swipe_card.Location = new System.Drawing.Point(77, 22);
      this.lbl_swipe_card.Name = "lbl_swipe_card";
      this.lbl_swipe_card.Size = new System.Drawing.Size(200, 23);
      this.lbl_swipe_card.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLUE;
      this.lbl_swipe_card.TabIndex = 0;
      this.lbl_swipe_card.Text = "xInput Card";
      // 
      // lbl_invalid_card
      // 
      this.lbl_invalid_card.BackColor = System.Drawing.Color.Transparent;
      this.lbl_invalid_card.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_invalid_card.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.lbl_invalid_card.Location = new System.Drawing.Point(5, 138);
      this.lbl_invalid_card.Name = "lbl_invalid_card";
      this.lbl_invalid_card.Size = new System.Drawing.Size(274, 40);
      this.lbl_invalid_card.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_RED;
      this.lbl_invalid_card.TabIndex = 4;
      this.lbl_invalid_card.Text = "Número de tarjeta no válido ...";
      this.lbl_invalid_card.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.lbl_invalid_card.Click += new System.EventHandler(this.lbl_invalid_card_Click);
      // 
      // pictureBox1
      // 
      this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
      this.pictureBox1.Location = new System.Drawing.Point(5, 0);
      this.pictureBox1.Name = "pictureBox1";
      this.pictureBox1.Size = new System.Drawing.Size(65, 62);
      this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
      this.pictureBox1.TabIndex = 105;
      this.pictureBox1.TabStop = false;
      this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
      // 
      // txt_track_number
      // 
      this.txt_track_number.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_track_number.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_track_number.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_track_number.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_track_number.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_track_number.CornerRadius = 5;
      this.txt_track_number.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_track_number.Location = new System.Drawing.Point(5, 63);
      this.txt_track_number.MaxLength = 32767;
      this.txt_track_number.Multiline = false;
      this.txt_track_number.Name = "txt_track_number";
      this.txt_track_number.PasswordChar = '●';
      this.txt_track_number.ReadOnly = false;
      this.txt_track_number.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_track_number.SelectedText = "";
      this.txt_track_number.SelectionLength = 0;
      this.txt_track_number.SelectionStart = 0;
      this.txt_track_number.Size = new System.Drawing.Size(213, 36);
      this.txt_track_number.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC_CUSTOM;
      this.txt_track_number.TabIndex = 1;
      this.txt_track_number.TabStop = false;
      this.txt_track_number.Text = "12345678901234567890";
      this.txt_track_number.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.txt_track_number.UseSystemPasswordChar = false;
      this.txt_track_number.WaterMark = "";
      this.txt_track_number.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      this.txt_track_number.TextChanged += new System.EventHandler(this.txt_track_number_TextChanged);
      this.txt_track_number.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_track_number_KeyPress);
      // 
      // btn_player_unknown
      // 
      this.btn_player_unknown.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_player_unknown.FlatAppearance.BorderSize = 0;
      this.btn_player_unknown.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_player_unknown.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_player_unknown.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_player_unknown.Image = null;
      this.btn_player_unknown.IsSelected = false;
      this.btn_player_unknown.Location = new System.Drawing.Point(41, 107);
      this.btn_player_unknown.Name = "btn_player_unknown";
      this.btn_player_unknown.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_player_unknown.Size = new System.Drawing.Size(141, 29);
      this.btn_player_unknown.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_player_unknown.TabIndex = 3;
      this.btn_player_unknown.Text = "XDESCONOCIDO";
      this.btn_player_unknown.UseVisualStyleBackColor = false;
      // 
      // btn_player_browse
      // 
      this.btn_player_browse.BackColor = System.Drawing.Color.Transparent;
      this.btn_player_browse.CornerRadius = 0;
      this.btn_player_browse.FlatAppearance.BorderSize = 0;
      this.btn_player_browse.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_player_browse.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_player_browse.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_player_browse.Image = global::WSI.Cashier.Properties.Resources.btnSearch;
      this.btn_player_browse.IsSelected = false;
      this.btn_player_browse.Location = new System.Drawing.Point(218, 55);
      this.btn_player_browse.Name = "btn_player_browse";
      this.btn_player_browse.SelectedColor = System.Drawing.Color.Empty;
      this.btn_player_browse.Size = new System.Drawing.Size(60, 53);
      this.btn_player_browse.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.NONE;
      this.btn_player_browse.TabIndex = 2;
      this.btn_player_browse.UseVisualStyleBackColor = false;
      // 
      // uc_gt_card_reader
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
      this.BackColor = System.Drawing.Color.Transparent;
      this.Controls.Add(this.btn_player_unknown);
      this.Controls.Add(this.btn_player_browse);
      this.Controls.Add(this.pictureBox1);
      this.Controls.Add(this.txt_track_number);
      this.Controls.Add(this.lbl_invalid_card);
      this.Controls.Add(this.lbl_swipe_card);
      this.Name = "uc_gt_card_reader";
      this.Size = new System.Drawing.Size(279, 195);
      this.VisibleChanged += new System.EventHandler(this.uc_gt_card_reader_VisibleChanged);
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Timer tmr_clear;
    private WSI.Cashier.Controls.uc_label lbl_swipe_card;
    private WSI.Cashier.Controls.uc_label lbl_invalid_card;
    private System.Windows.Forms.PictureBox pictureBox1;
    private WSI.Cashier.Controls.uc_round_textbox txt_track_number;
    private WSI.Cashier.Controls.uc_round_button btn_player_browse;
    private WSI.Cashier.Controls.uc_round_button btn_player_unknown;
  }
}
