namespace WSI.Cashier
{
  partial class frm_container
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      if (m_id_scanner != null)
      {
        m_id_scanner.DetatchFromAllAvailable(null);
        m_id_scanner.Dispose();
        m_id_scanner = null;
      }

      base.Dispose(disposing);
    }

    
    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_container));
      this.imageList1 = new System.Windows.Forms.ImageList(this.components);
      this.splitContainer1 = new System.Windows.Forms.SplitContainer();
      this.splitContainer2 = new System.Windows.Forms.SplitContainer();
      this.uc_ticket_offline1 = new WSI.Cashier.TITO.uc_ticket_offline();
      this.btn_reception = new System.Windows.Forms.Button();
      this.uc_concept_operations1 = new WSI.Cashier.uc_concept_operations();
      this.pnl_menu_table_mode = new WSI.Cashier.Controls.uc_round_panel();
      this.btn_menu_table_mode = new WSI.Cashier.Controls.uc_round_button();
      this.uc_gt_player_tracking1 = new WSI.Cashier.uc_gt_player_tracking();
      this.uc_card1 = new WSI.Cashier.uc_card();
      this.uc_ticket_undo1 = new WSI.Cashier.uc_ticket_undo();
      this.uc_ticket_validation1 = new WSI.Cashier.uc_ticket_validation();
      this.uc_ticket_promo1 = new WSI.Cashier.uc_ticket_promo();
      this.uc_ticket_create1 = new WSI.Cashier.uc_ticket_create();
      this.uc_tito_card1 = new WSI.Cashier.uc_tito_card();
      this.uc_bank1 = new WSI.Cashier.uc_bank();
      this.uc_gift_delivery1 = new WSI.Cashier.uc_gift_delivery();
      this.uc_mobile_bank1 = new WSI.Cashier.uc_mobile_bank();
      this.uc_options1 = new WSI.Cashier.uc_options();
      this.uc_ticket_view = new WSI.Cashier.uc_ticket_view();
      this.uc_gaming_hall1 = new WSI.Cashier.uc_gaming_hall();
      this.uc_gaming_hall_set_pcd_meters1 = new WSI.Cashier.uc_gaming_hall_set_pcd_meters();
      this.uc_ticket_terminal1 = new WSI.Cashier.TITO.uc_ticket_terminal();
      this.uc_menu = new WSI.Cashier.Controls.uc_menu();
      this.tmr_alias = new System.Windows.Forms.Timer(this.components);
      this.uc_top_bar = new WSI.Cashier.Controls.uc_top_bar();
      this.splitContainer1.Panel2.SuspendLayout();
      this.splitContainer1.SuspendLayout();
      this.splitContainer2.Panel2.SuspendLayout();
      this.splitContainer2.SuspendLayout();
      this.pnl_menu_table_mode.SuspendLayout();
      this.SuspendLayout();
      // 
      // imageList1
      // 
      this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
      this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
      this.imageList1.Images.SetKeyName(0, "CREDT04F.ICO");
      this.imageList1.Images.SetKeyName(1, "DOLLR01M.ICO");
      this.imageList1.Images.SetKeyName(2, "EXIT00B.ICO");
      this.imageList1.Images.SetKeyName(3, "POINT02.ICO");
      this.imageList1.Images.SetKeyName(4, "POINT04.ICO");
      this.imageList1.Images.SetKeyName(5, "SECUR08.ICO");
      this.imageList1.Images.SetKeyName(6, "TRFFC03.ICO");
      this.imageList1.Images.SetKeyName(7, "DATAENTR.ICO");
      this.imageList1.Images.SetKeyName(8, "END.ICO");
      this.imageList1.Images.SetKeyName(9, "TRFFC03.ICO");
      this.imageList1.Images.SetKeyName(10, "entrada_tickets.png");
      this.imageList1.Images.SetKeyName(11, "anulaci�n.png");
      this.imageList1.Images.SetKeyName(12, "creaci�n.png");
      this.imageList1.Images.SetKeyName(13, "pago.png");
      this.imageList1.Images.SetKeyName(14, "promociones.png");
      this.imageList1.Images.SetKeyName(15, "validaci�n.png");
      this.imageList1.Images.SetKeyName(16, "Go1.png");
      this.imageList1.Images.SetKeyName(17, "forward-icon.png");
      this.imageList1.Images.SetKeyName(18, "GT_player_traking.png");
      // 
      // splitContainer1
      // 
      this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
      this.splitContainer1.IsSplitterFixed = true;
      this.splitContainer1.Location = new System.Drawing.Point(0, 0);
      this.splitContainer1.Name = "splitContainer1";
      // 
      // splitContainer1.Panel1
      // 
      this.splitContainer1.Panel1.BackColor = System.Drawing.Color.CornflowerBlue;
      this.splitContainer1.Panel1.Padding = new System.Windows.Forms.Padding(8);
      this.splitContainer1.Panel1MinSize = 48;
      // 
      // splitContainer1.Panel2
      // 
      this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
      this.splitContainer1.Size = new System.Drawing.Size(1024, 768);
      this.splitContainer1.SplitterDistance = 128;
      this.splitContainer1.TabIndex = 1;
      // 
      // splitContainer2
      // 
      this.splitContainer2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splitContainer2.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
      this.splitContainer2.IsSplitterFixed = true;
      this.splitContainer2.Location = new System.Drawing.Point(0, 0);
      this.splitContainer2.Margin = new System.Windows.Forms.Padding(0);
      this.splitContainer2.Name = "splitContainer2";
      this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
      // 
      // splitContainer2.Panel1
      // 
      this.splitContainer2.Panel1.BackColor = System.Drawing.Color.CornflowerBlue;
      this.splitContainer2.Panel1.Padding = new System.Windows.Forms.Padding(0, 4, 0, 0);
      this.splitContainer2.Panel1MinSize = 32;
      // 
      // splitContainer2.Panel2
      // 
      this.splitContainer2.Panel2.Controls.Add(this.uc_ticket_offline1);
      this.splitContainer2.Panel2.Controls.Add(this.btn_reception);
      this.splitContainer2.Panel2.Controls.Add(this.uc_concept_operations1);
      this.splitContainer2.Panel2.Controls.Add(this.pnl_menu_table_mode);
      this.splitContainer2.Panel2.Controls.Add(this.uc_gt_player_tracking1);
      this.splitContainer2.Panel2.Controls.Add(this.uc_card1);
      this.splitContainer2.Panel2.Controls.Add(this.uc_ticket_undo1);
      this.splitContainer2.Panel2.Controls.Add(this.uc_ticket_validation1);
      this.splitContainer2.Panel2.Controls.Add(this.uc_ticket_promo1);
      this.splitContainer2.Panel2.Controls.Add(this.uc_ticket_create1);
      this.splitContainer2.Panel2.Controls.Add(this.uc_tito_card1);
      this.splitContainer2.Panel2.Controls.Add(this.uc_bank1);
      this.splitContainer2.Panel2.Controls.Add(this.uc_gift_delivery1);
      this.splitContainer2.Panel2.Controls.Add(this.uc_mobile_bank1);
      this.splitContainer2.Panel2.Controls.Add(this.uc_options1);
      this.splitContainer2.Panel2.Controls.Add(this.uc_ticket_view);
      this.splitContainer2.Panel2.Controls.Add(this.uc_gaming_hall1);
      this.splitContainer2.Panel2.Controls.Add(this.uc_gaming_hall_set_pcd_meters1);
      this.splitContainer2.Panel2.Controls.Add(this.uc_ticket_terminal1);
      this.splitContainer2.Panel2MinSize = 0;
      this.splitContainer2.Size = new System.Drawing.Size(892, 768);
      this.splitContainer2.SplitterDistance = 55;
      this.splitContainer2.SplitterWidth = 1;
      this.splitContainer2.TabIndex = 1;
      // 
      // uc_ticket_offline1
      // 
      this.uc_ticket_offline1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.uc_ticket_offline1.Location = new System.Drawing.Point(0, 0);
      this.uc_ticket_offline1.Name = "uc_ticket_offline1";
      this.uc_ticket_offline1.Size = new System.Drawing.Size(768, 606);
      this.uc_ticket_offline1.TabIndex = 25;
      this.uc_ticket_offline1.Visible = false;
      // 
      // btn_reception
      // 
      this.btn_reception.BackColor = System.Drawing.Color.LightSkyBlue;
      this.btn_reception.Dock = System.Windows.Forms.DockStyle.Top;
      this.btn_reception.FlatAppearance.BorderSize = 0;
      this.btn_reception.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btn_reception.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
      this.btn_reception.Location = new System.Drawing.Point(0, 0);
      this.btn_reception.Margin = new System.Windows.Forms.Padding(0);
      this.btn_reception.Name = "btn_reception";
      this.btn_reception.Padding = new System.Windows.Forms.Padding(0, 0, 4, 0);
      this.btn_reception.Size = new System.Drawing.Size(892, 48);
      this.btn_reception.TabIndex = 71;
      this.btn_reception.Text = "xReception";
      this.btn_reception.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.btn_reception.UseVisualStyleBackColor = false;
      this.btn_reception.Visible = false;
      this.btn_reception.Click += new System.EventHandler(this.btn_reception_Click);
      // 
      // uc_concept_operations1
      // 
      this.uc_concept_operations1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.uc_concept_operations1.Location = new System.Drawing.Point(3, -2);
      this.uc_concept_operations1.Name = "uc_concept_operations1";
      this.uc_concept_operations1.Size = new System.Drawing.Size(883, 596);
      this.uc_concept_operations1.TabIndex = 31;
      this.uc_concept_operations1.Visible = false;
      // 
      // pnl_menu_table_mode
      // 
      this.pnl_menu_table_mode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.pnl_menu_table_mode.BackColor = System.Drawing.Color.Transparent;
      this.pnl_menu_table_mode.BorderColor = System.Drawing.Color.Empty;
      this.pnl_menu_table_mode.Controls.Add(this.btn_menu_table_mode);
      this.pnl_menu_table_mode.CornerRadius = 20;
      this.pnl_menu_table_mode.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.pnl_menu_table_mode.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.pnl_menu_table_mode.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.pnl_menu_table_mode.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.pnl_menu_table_mode.HeaderHeight = 0;
      this.pnl_menu_table_mode.HeaderSubText = null;
      this.pnl_menu_table_mode.HeaderText = null;
      this.pnl_menu_table_mode.Location = new System.Drawing.Point(10, 659);
      this.pnl_menu_table_mode.Name = "pnl_menu_table_mode";
      this.pnl_menu_table_mode.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.pnl_menu_table_mode.Size = new System.Drawing.Size(54, 50);
      this.pnl_menu_table_mode.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.GAMING_TABLE;
      this.pnl_menu_table_mode.TabIndex = 30;
      this.pnl_menu_table_mode.Visible = false;
      // 
      // btn_menu_table_mode
      // 
      this.btn_menu_table_mode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.btn_menu_table_mode.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(90)))), ((int)(((byte)(102)))));
      this.btn_menu_table_mode.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_menu_table_mode.FlatAppearance.BorderSize = 0;
      this.btn_menu_table_mode.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_menu_table_mode.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_menu_table_mode.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_menu_table_mode.Image = ((System.Drawing.Image)(resources.GetObject("btn_menu_table_mode.Image")));
      this.btn_menu_table_mode.IsSelected = false;
      this.btn_menu_table_mode.Location = new System.Drawing.Point(3, 6);
      this.btn_menu_table_mode.Margin = new System.Windows.Forms.Padding(0);
      this.btn_menu_table_mode.Name = "btn_menu_table_mode";
      this.btn_menu_table_mode.SelectedColor = System.Drawing.Color.Empty;
      this.btn_menu_table_mode.Size = new System.Drawing.Size(49, 40);
      this.btn_menu_table_mode.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.GAMING_TABLE;
      this.btn_menu_table_mode.TabIndex = 73;
      this.btn_menu_table_mode.UseVisualStyleBackColor = false;
      this.btn_menu_table_mode.Click += new System.EventHandler(this.btn_menu_table_mode_Click);
      // 
      // uc_gt_player_tracking1
      // 
      this.uc_gt_player_tracking1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.uc_gt_player_tracking1.Location = new System.Drawing.Point(235, 123);
      this.uc_gt_player_tracking1.Margin = new System.Windows.Forms.Padding(0);
      this.uc_gt_player_tracking1.Name = "uc_gt_player_tracking1";
      this.uc_gt_player_tracking1.Size = new System.Drawing.Size(949, 735);
      this.uc_gt_player_tracking1.TabIndex = 27;
      // 
      // uc_card1
      // 
      this.uc_card1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.uc_card1.IDScanner = null;
      this.uc_card1.LineSeparatorColor = System.Drawing.Color.Empty;
      this.uc_card1.Location = new System.Drawing.Point(0, 0);
      this.uc_card1.Name = "uc_card1";
      this.uc_card1.Size = new System.Drawing.Size(886, 200);
      this.uc_card1.TabIndex = 4;
      this.uc_card1.Visible = false;
      // 
      // uc_ticket_undo1
      // 
      this.uc_ticket_undo1.BackColor = System.Drawing.SystemColors.Control;
      this.uc_ticket_undo1.Location = new System.Drawing.Point(0, 0);
      this.uc_ticket_undo1.Name = "uc_ticket_undo1";
      this.uc_ticket_undo1.Size = new System.Drawing.Size(884, 656);
      this.uc_ticket_undo1.TabIndex = 26;
      // 
      // uc_ticket_validation1
      // 
      this.uc_ticket_validation1.BackColor = System.Drawing.Color.Transparent;
      this.uc_ticket_validation1.Location = new System.Drawing.Point(0, 0);
      this.uc_ticket_validation1.Name = "uc_ticket_validation1";
      this.uc_ticket_validation1.Size = new System.Drawing.Size(884, 656);
      this.uc_ticket_validation1.TabIndex = 24;
      // 
      // uc_ticket_promo1
      // 
      this.uc_ticket_promo1.BackColor = System.Drawing.Color.Transparent;
      this.uc_ticket_promo1.Location = new System.Drawing.Point(0, 0);
      this.uc_ticket_promo1.Name = "uc_ticket_promo1";
      this.uc_ticket_promo1.Size = new System.Drawing.Size(884, 591);
      this.uc_ticket_promo1.TabIndex = 23;
      this.uc_ticket_promo1.VirtualAccountMode = true;
      // 
      // uc_ticket_create1
      // 
      this.uc_ticket_create1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.uc_ticket_create1.IDScanner = null;
      this.uc_ticket_create1.Location = new System.Drawing.Point(0, 0);
      this.uc_ticket_create1.Name = "uc_ticket_create1";
      this.uc_ticket_create1.Size = new System.Drawing.Size(884, 656);
      this.uc_ticket_create1.TabIndex = 22;
      // 
      // uc_tito_card1
      // 
      this.uc_tito_card1.IDScanner = null;
      this.uc_tito_card1.Location = new System.Drawing.Point(0, 0);
      this.uc_tito_card1.Name = "uc_tito_card1";
      this.uc_tito_card1.Size = new System.Drawing.Size(884, 656);
      this.uc_tito_card1.TabIndex = 21;
      // 
      // uc_bank1
      // 
      this.uc_bank1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.uc_bank1.Location = new System.Drawing.Point(0, 0);
      this.uc_bank1.Name = "uc_bank1";
      this.uc_bank1.Size = new System.Drawing.Size(889, 462);
      this.uc_bank1.TabIndex = 3;
      this.uc_bank1.Visible = false;
      // 
      // uc_gift_delivery1
      // 
      this.uc_gift_delivery1.BackColor = System.Drawing.SystemColors.Control;
      this.uc_gift_delivery1.Location = new System.Drawing.Point(0, 0);
      this.uc_gift_delivery1.Name = "uc_gift_delivery1";
      this.uc_gift_delivery1.Size = new System.Drawing.Size(913, 260);
      this.uc_gift_delivery1.TabIndex = 6;
      // 
      // uc_mobile_bank1
      // 
      this.uc_mobile_bank1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.uc_mobile_bank1.Location = new System.Drawing.Point(0, 0);
      this.uc_mobile_bank1.Name = "uc_mobile_bank1";
      this.uc_mobile_bank1.Size = new System.Drawing.Size(884, 153);
      this.uc_mobile_bank1.TabIndex = 5;
      // 
      // uc_options1
      // 
      this.uc_options1.BackColor = System.Drawing.Color.Gainsboro;
      this.uc_options1.IDScanner = null;
      this.uc_options1.Location = new System.Drawing.Point(0, 0);
      this.uc_options1.Name = "uc_options1";
      this.uc_options1.Size = new System.Drawing.Size(883, 252);
      this.uc_options1.TabIndex = 5;
      // 
      // uc_ticket_view
      // 
      this.uc_ticket_view.BackColor = System.Drawing.Color.Transparent;
      this.uc_ticket_view.Location = new System.Drawing.Point(0, 0);
      this.uc_ticket_view.Name = "uc_ticket_view";
      this.uc_ticket_view.Size = new System.Drawing.Size(883, 605);
      this.uc_ticket_view.TabIndex = 20;
      this.uc_ticket_view.Visible = false;
      // 
      // uc_gaming_hall1
      // 
      this.uc_gaming_hall1.BackColor = System.Drawing.Color.Transparent;
      this.uc_gaming_hall1.Location = new System.Drawing.Point(0, 0);
      this.uc_gaming_hall1.Name = "uc_gaming_hall1";
      this.uc_gaming_hall1.Size = new System.Drawing.Size(889, 750);
      this.uc_gaming_hall1.TabIndex = 3;
      this.uc_gaming_hall1.Visible = false;
      // 
      // uc_gaming_hall_set_pcd_meters1
      // 
      this.uc_gaming_hall_set_pcd_meters1.BackColor = System.Drawing.Color.Transparent;
      this.uc_gaming_hall_set_pcd_meters1.Location = new System.Drawing.Point(0, 0);
      this.uc_gaming_hall_set_pcd_meters1.Name = "uc_gaming_hall_set_pcd_meters1";
      this.uc_gaming_hall_set_pcd_meters1.Size = new System.Drawing.Size(889, 750);
      this.uc_gaming_hall_set_pcd_meters1.TabIndex = 3;
      this.uc_gaming_hall_set_pcd_meters1.Visible = false;
      // 
      // uc_ticket_terminal1
      // 
      this.uc_ticket_terminal1.BackColor = System.Drawing.Color.Transparent;
      this.uc_ticket_terminal1.Location = new System.Drawing.Point(0, 0);
      this.uc_ticket_terminal1.Name = "uc_ticket_terminal1";
      this.uc_ticket_terminal1.Size = new System.Drawing.Size(884, 780);
      this.uc_ticket_terminal1.TabIndex = 32;
      this.uc_ticket_terminal1.Visible = false;
      // 
      // uc_menu
      // 
      this.uc_menu.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
      this.uc_menu.AutoAdjustHeight = false;
      this.uc_menu.AutoCollapsed = false;
      this.uc_menu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.uc_menu.Location = new System.Drawing.Point(-1, 55);
      this.uc_menu.Margin = new System.Windows.Forms.Padding(0);
      this.uc_menu.MenuButtonHoverStyle = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.NONE;
      this.uc_menu.MenuButtonStyle = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.NONE;
      this.uc_menu.Name = "uc_menu";
      this.uc_menu.ShowLogo = false;
      this.uc_menu.Size = new System.Drawing.Size(130, 713);
      this.uc_menu.Style = WSI.Cashier.Controls.uc_menu.MenuStyle.NONE;
      this.uc_menu.TabIndex = 34;
      this.uc_menu.TimeToCollapse = 0;
      // 
      // tmr_alias
      // 
      this.tmr_alias.Enabled = true;
      this.tmr_alias.Interval = 10000;
      // 
      // uc_top_bar
      // 
      this.uc_top_bar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.uc_top_bar.ButtonKeyboardStyle = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.NONE;
      this.uc_top_bar.CashierStatus = WSI.Cashier.SystemInfo.StatusInfo.NONE;
      this.uc_top_bar.DatabaseStatus = WSI.Cashier.SystemInfo.StatusInfo.NONE;
      this.uc_top_bar.Dock = System.Windows.Forms.DockStyle.Top;
      this.uc_top_bar.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.uc_top_bar.Location = new System.Drawing.Point(0, 0);
      this.uc_top_bar.Margin = new System.Windows.Forms.Padding(0);
      this.uc_top_bar.MultisiteStatus = WSI.Cashier.SystemInfo.StatusInfo.NONE;
      this.uc_top_bar.Name = "uc_top_bar";
      this.uc_top_bar.ShowRol = false;
      this.uc_top_bar.Size = new System.Drawing.Size(1024, 55);
      this.uc_top_bar.SystemInfo = null;
      this.uc_top_bar.TabIndex = 21;
      this.uc_top_bar.TerminalName = null;
      this.uc_top_bar.UserImage = null;
      this.uc_top_bar.UserName = null;
      this.uc_top_bar.UserRol = null;
      // 
      // frm_container
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(1024, 768);
      this.Controls.Add(this.uc_top_bar);
      this.Controls.Add(this.uc_menu);
      this.Controls.Add(this.splitContainer1);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
      this.MaximumSize = new System.Drawing.Size(1024, 768);
      this.MinimizeBox = false;
      this.Name = "frm_container";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "Cashier";
      this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
      this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_container_FormClosing);
      this.Load += new System.EventHandler(this.frm_container_Load);
      this.splitContainer1.Panel2.ResumeLayout(false);
      this.splitContainer1.ResumeLayout(false);
      this.splitContainer2.Panel2.ResumeLayout(false);
      this.splitContainer2.ResumeLayout(false);
      this.pnl_menu_table_mode.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.SplitContainer splitContainer1;
    private System.Windows.Forms.SplitContainer splitContainer2;
    public System.Windows.Forms.ImageList imageList1;
    public uc_ticket_create uc_ticket_create1;
    public uc_tito_card uc_tito_card1;
    public uc_card uc_card1;
    public uc_bank uc_bank1;
    public uc_gaming_hall uc_gaming_hall1;
    public uc_gaming_hall_set_pcd_meters uc_gaming_hall_set_pcd_meters1;
    public uc_mobile_bank uc_mobile_bank1;
    public uc_options uc_options1;
    public uc_gift_delivery uc_gift_delivery1;
    private System.Windows.Forms.Timer tmr_alias;
    public uc_ticket_view uc_ticket_view;
    private uc_ticket_promo uc_ticket_promo1;
    private uc_ticket_validation uc_ticket_validation1;
    private WSI.Cashier.TITO.uc_ticket_offline uc_ticket_offline1;
    private uc_ticket_undo uc_ticket_undo1;
    public uc_gt_player_tracking uc_gt_player_tracking1;
    private WSI.Cashier.Controls.uc_round_panel pnl_menu_table_mode;
    private Controls.uc_round_button btn_menu_table_mode;
    private uc_concept_operations uc_concept_operations1;
    private TITO.uc_ticket_terminal uc_ticket_terminal1;
    // ETP 19/10/2015 added new buttons for GamingHall systems.
    private Controls.uc_top_bar uc_top_bar;
    private Controls.uc_menu uc_menu;
    private System.Windows.Forms.Button btn_reception;
  }
}