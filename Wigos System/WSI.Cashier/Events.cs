using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using WSI.Common;
using System.Threading;
using WSI.Cashier.Controls;

namespace WSI.Cashier
{
  public static class EventHistory
  {

  #region Constants

    public const int DGV_COLUMN_EVENT_ID = 0;
    public const int DGV_COLUMN_DATETIME = 1;
    public const int DGV_COLUMN_TERMINAL_ID = 2;
    public const int DGV_COLUMN_TERMINAL_NAME = 3;
    public const int DGV_COLUMN_EVENT_TYPE = 4;
    public const int DGV_COLUMN_DESCRIPTION = 5;

    //Columns 6-10: Visible = false
    public const int DGV_COLUMN_DEVICE_CODE = 6;
    public const int DGV_COLUMN_DEVICE_STATUS = 7;
    public const int DGV_COLUMN_DEVICE_PRIORITY = 8;
    public const int DGV_COLUMN_OPERATION_CODE = 9;
    public const int DGV_COLUMN_OPERATION_DATA = 10;

  #endregion Constants

  #region Members

    static Int64 m_last_timestamp = 0;

  #endregion Members

  #region Public Events Functions

    /// <summary>
    /// Refresh events in DataGridView
    /// </summary>
    /// <param name="DataGridView"></param>
    public static DataTable RefreshData()
    {
      SqlConnection sql_conn;
      DataSet ds;
      SqlDataAdapter da;
      String sql_str;

      sql_conn = null;

      try
      {
        sql_str = " SELECT TOP 31 EH_EVENT_ID, " +
                                " EH_DATETIME, " +
                                " EH_TERMINAL_ID, " +
                                " TE_NAME, " +
                                " EH_EVENT_TYPE, " +
                                " ISNULL(EH_EVENT_DATA, '') EH_EVENT_DATA, " +
                                " ISNULL(EH_DEVICE_CODE, 0) EH_DEVICE_CODE, " +
                                " ISNULL(EH_DEVICE_STATUS, 0) EH_DEVICE_STATUS, " +
                                " ISNULL(EH_DEVICE_PRIORITY, 0) EH_DEVICE_PRIORITY, " +
                                " ISNULL(EH_OPERATION_CODE, 0) EH_OPERATION_CODE, " +
                                " ISNULL(EH_OPERATION_DATA, 0) EH_OPERATION_DATA " +
                "   FROM EVENT_HISTORY, TERMINALS " +
                "   WHERE EH_ACK_DATE IS NULL " +
                "     AND EH_TERMINAL_ID = TE_TERMINAL_ID " +
                "   ORDER BY EH_EVENT_ID DESC ";

        sql_conn = WGDB.Connection();
        ds = new DataSet();

        da = new SqlDataAdapter(sql_str, sql_conn);
        da.Fill(ds);

        if ( ds.Tables[0].Rows.Count > 0 )
        {
          foreach (DataRow row in ds.Tables[0].Rows)
          {
            row["EH_EVENT_DATA"] = WSI.Common.Cashier.EvOpGetDescription((WSI.Common.Cashier.EnumEventType)row["EH_EVENT_TYPE"],
                                                                         (WSI.Common.Cashier.EnumDeviceCode)row["EH_DEVICE_CODE"],
                                                                         (WSI.Common.Cashier.EnumDeviceStatus)row["EH_DEVICE_STATUS"],
                                                                         (WSI.Common.Cashier.EnumEventSeverity)row["EH_DEVICE_PRIORITY"],
                                                                         (WSI.Common.Cashier.EnumOperationCode)row["EH_OPERATION_CODE"],
                                                                         (Decimal)row["EH_OPERATION_DATA"],
                                                                         0,
                                                                         0);
          }
        }

        return ds.Tables[0];
      }
      catch (Exception ex)
      {
        Log.Exception(ex);

        return null;
      }
      finally
      {
        // Close connection
        if (sql_conn != null)
        {
          sql_conn.Close();
          sql_conn = null;
        }
      }
    }

    /// <summary>
    /// Check if insert or update events.
    /// </summary>
    /// <returns></returns>
    public static Boolean CheckChanged()
    {
      SqlTransaction sql_trx;
      SqlConnection sql_conn;
      SqlCommand sql_command;
      String sql_str;
      Int64 current_timestamp;

      sql_conn = null;
      sql_trx = null;

      try
      {
        sql_str = "SELECT CAST(ISNULL(MAX(EH_TIMESTAMP), 0) AS BIGINT) ";
        sql_str += " FROM EVENT_HISTORY ";

        sql_conn = WGDB.Connection();
        sql_trx = sql_conn.BeginTransaction();

        sql_command = new SqlCommand(sql_str);
        sql_command.Connection = sql_trx.Connection;
        sql_command.Transaction = sql_trx;

        current_timestamp = (Int64)sql_command.ExecuteScalar();
        sql_trx.Commit();

        if (current_timestamp > m_last_timestamp)
        {
          m_last_timestamp = current_timestamp;

          return true;
        }

        return false;
      }
      catch (Exception ex)
      {
        Log.Exception(ex);

        return true;
      }
      finally
      {
        if (sql_trx != null)
        {
          if (sql_trx.Connection != null)
          {
            sql_trx.Rollback();
          }
          sql_trx.Dispose();
          sql_trx = null;
        }

        // Close connection
        if (sql_conn != null)
        {
          sql_conn.Close();
          sql_conn = null;
        }
      }
    }

    public static void DataGridViewStyle(DataGridView DataGridView)
    {
      String date_format;
      String time_separator;
      Int32 width_last_colum;

      // DataGrid Style
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();

      dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
      dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
      dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
      dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
      dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
      DataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;


      dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
      dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
      dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
      dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
      dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
      DataGridView.DefaultCellStyle = dataGridViewCellStyle2;


      dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
      dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
      dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
      dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
      dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
      DataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;

      date_format = System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern;
      time_separator = System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.TimeSeparator;

      // Columns Style
      DataGridView.Columns[DGV_COLUMN_EVENT_ID].Width = 0;
      DataGridView.Columns[DGV_COLUMN_EVENT_ID].Visible = false;
      DataGridView.Columns[DGV_COLUMN_DATETIME].Width = 120;
      DataGridView.Columns[DGV_COLUMN_DATETIME].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
      DataGridView.Columns[DGV_COLUMN_DATETIME].DefaultCellStyle.Format = date_format + " HH" + time_separator + "mm" + time_separator + "ss";
      DataGridView.Columns[DGV_COLUMN_TERMINAL_ID].Width = 0;
      DataGridView.Columns[DGV_COLUMN_TERMINAL_ID].Visible = false;
      DataGridView.Columns[DGV_COLUMN_TERMINAL_NAME].Width = 180;
      DataGridView.Columns[DGV_COLUMN_TERMINAL_NAME].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
      DataGridView.Columns[DGV_COLUMN_EVENT_TYPE].Width = 0;
      DataGridView.Columns[DGV_COLUMN_EVENT_TYPE].Visible = false;
      DataGridView.Columns[DGV_COLUMN_DESCRIPTION].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

      DataGridView.Columns[EventHistory.DGV_COLUMN_DATETIME].HeaderCell.Value = Resource.String("STR_FRM_CONTAINER_EVENTS_DATAGRID_HEADER_DATE");
      DataGridView.Columns[EventHistory.DGV_COLUMN_TERMINAL_NAME].HeaderCell.Value = Resource.String("STR_FRM_CONTAINER_EVENTS_DATAGRID_HEADER_TERMINAL");
      DataGridView.Columns[EventHistory.DGV_COLUMN_DESCRIPTION].HeaderCell.Value = Resource.String("STR_FRM_CONTAINER_EVENTS_DATAGRID_HEADER_DESCRIPTION");

      DataGridView.Columns[DGV_COLUMN_DEVICE_CODE].Visible = false;
      DataGridView.Columns[DGV_COLUMN_DEVICE_STATUS].Visible = false;
      DataGridView.Columns[DGV_COLUMN_DEVICE_PRIORITY].Visible = false;
      DataGridView.Columns[DGV_COLUMN_OPERATION_CODE].Visible = false;
      DataGridView.Columns[DGV_COLUMN_OPERATION_DATA].Visible = false;
    
      width_last_colum = DataGridView.Width;
      width_last_colum -= DataGridView.Columns[DGV_COLUMN_DATETIME].Width;
      width_last_colum -= DataGridView.Columns[DGV_COLUMN_TERMINAL_NAME].Width;
      if (DataGridView.Size.Height < 200)
      {
        width_last_colum -= System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
      }
      width_last_colum -= 1;
      DataGridView.Columns[DGV_COLUMN_DESCRIPTION].Width = width_last_colum;
    }

  #endregion Public Events Functions

  }

  class EventsWatcher
  {
    #region Members

    public delegate void EventsChangedHandler();
    public event EventsChangedHandler EventsChangedEvent;
    private Thread thread;
    bool stopped;
    private DataTable events_data = null;
    private AutoResetEvent ev_events_changed = new AutoResetEvent(false);

    #endregion Members

    #region Public Functions

    public DataTable EventsData
    {
      get
      {
        lock (this)
        {
          return events_data;
        }
      }
    }

    public void Start()
    {
      if (thread == null)
      {
        stopped = false;
        thread = new Thread(this.EventsWatcherThread);
        thread.Name = "EventsWatcherThread";
        thread.Start();
      }
    }

    public void Stop()
    {
      stopped = true;
      thread.Join(5000);
      thread = null;
    }

    public void ForceRefresh()
    {
      ev_events_changed.Set();
    }

    #endregion Public Functions

    #region Private Functions (Thread)

    private void EventsWatcherThread()
    {
      Boolean refresh_data;

      while (!stopped)
      {
        try
        {
          refresh_data = false;
          if (ev_events_changed.WaitOne(2000, false))
          {
            refresh_data = true;
          }

          if (EventHistory.CheckChanged())
          {
            refresh_data = true;
          }

          if (refresh_data)
          {
            events_data = EventHistory.RefreshData();

            if (EventsChangedEvent != null)
            {
              EventsChangedEvent();
            }
          }

        }
        catch (Exception ex)
        {
          Log.Exception(ex);
        }
      }
    }

    #endregion Private Functions (Thread)
  }


  public static class EventLastAction
  {
      
    #region Public Functions

    // PURPOSE: Add a event for every control
    //
    //  PARAMS:
    //     - INPUT:
    //           - ControlCol: control collection for add the event
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - none

    public static void AddEventLastAction(Control.ControlCollection ControlCol)
    {
      foreach (Control _ctrl in ControlCol)
      {
        if (_ctrl is Button)
        {
          _ctrl.Click += new EventHandler(ctrl_Click);
        }
        else if (_ctrl is uc_concept_operations.ButtonConcept)
        {
          _ctrl.Click += new EventHandler(ctrl_Click);
        }
        else if (_ctrl is RadioButton)
        {
          _ctrl.Click += new EventHandler(ctrl_Click);
        }
        else if (_ctrl is DataGridView)
        {
          _ctrl.Click += new EventHandler(ctrl_Click);
        }
        else if (_ctrl is TextBox)
        {
          _ctrl.Click += new EventHandler(ctrl_Click);
        }
        else if (_ctrl is WSI.Common.GamingTablePanel)
        {
          _ctrl.Click += new EventHandler(ctrl_Click);
        }
        else if (_ctrl.Controls.Count > 0)
        {
          AddEventLastAction(_ctrl.Controls);
        }
        // Added new controls
        else if (_ctrl is WSI.Cashier.Controls.uc_checkBox)
        {
          _ctrl.Click += new EventHandler(ctrl_Click);
        }
        else if (_ctrl is WSI.Cashier.Controls.uc_DataGridView)
        {
          _ctrl.Click += new EventHandler(ctrl_Click);
        }
        else if (_ctrl is WSI.Cashier.Controls.uc_maskedTextBox)
        {
          _ctrl.Click += new EventHandler(ctrl_Click);
        }
        else if (_ctrl is WSI.Cashier.Controls.uc_radioButton)
        {
          _ctrl.Click += new EventHandler(ctrl_Click);
        }
        else if (_ctrl is WSI.Cashier.Controls.uc_round_auto_combobox)
        {
          _ctrl.Click += new EventHandler(ctrl_Click);
        }
        else if (_ctrl is WSI.Cashier.Controls.uc_round_button)
        {
          _ctrl.Click += new EventHandler(ctrl_Click);
        }
        else if (_ctrl is WSI.Cashier.Controls.uc_round_combobox)
        {
          _ctrl.Click += new EventHandler(ctrl_Click);
        }
        else if (_ctrl is WSI.Cashier.Controls.uc_round_list_box)
        {
          _ctrl.Click += new EventHandler(ctrl_Click);
        }
        else if (_ctrl is WSI.Cashier.Controls.uc_round_tab_control)
        {
          _ctrl.Click += new EventHandler(ctrl_Click);
        }
        else if (_ctrl is WSI.Cashier.Controls.uc_round_textbox)
        {
          _ctrl.Click += new EventHandler(ctrl_Click);
        }
      }
    }
    //SJA 27-JAN 2016 >> https://msdn.microsoft.com/en-us/library/ms366768(v=vs.90).aspx#Anchor_0
    /// <summary>
    /// unsubscribe events for faster GC cleanup
    /// </summary>
    /// <param name="ControlCol"></param>
    public static void RemoveEventLastAction(Control.ControlCollection ControlCol)
    {
      foreach (Control _ctrl in ControlCol)
      {
        if (_ctrl is Button || _ctrl is uc_concept_operations.ButtonConcept ||
            _ctrl is RadioButton || _ctrl is DataGridView || _ctrl is TextBox ||
            _ctrl is GamingTablePanel || _ctrl is uc_checkBox ||
            _ctrl is uc_DataGridView || _ctrl is uc_maskedTextBox ||
            _ctrl is uc_radioButton || _ctrl is uc_round_auto_combobox ||
            _ctrl is uc_round_button || _ctrl is uc_round_combobox ||
            _ctrl is uc_round_list_box || _ctrl is uc_round_tab_control ||
            _ctrl is uc_round_textbox)
        {
          _ctrl.Click -= new EventHandler(ctrl_Click);
        }
        else if (_ctrl.Controls.Count > 0)
        {
          RemoveEventLastAction(_ctrl.Controls);
        }
      }
    }

    #endregion

    #region Private Functions

    private static void ctrl_Click(object sender, EventArgs e)
    {
      if (sender is Button)
      {
        WSI.Common.Users.SetLastAction("Open", ((Button)sender).Text);
      }
      else if (sender is uc_concept_operations.ButtonConcept)
      {
        WSI.Common.Users.SetLastAction("Open", ((uc_concept_operations.ButtonConcept)sender).Text);
      }
      else if (sender is RadioButton)
      {
        WSI.Common.Users.SetLastAction("Open", ((RadioButton)sender).Text);
      }
      else if (sender is DataGridView)
      {
        WSI.Common.Users.SetLastAction("Open", ((DataGridView)sender).Name);
      }
      else if (sender is TextBox)
      {
        WSI.Common.Users.SetLastAction("Open", ((TextBox)sender).Name);
      }
      else if (sender is GamingTablePanel)
      {
        WSI.Common.Users.SetLastAction("Open", ((GamingTablePanel)sender).Name);
      }
    }

    #endregion    

  }

}
