using WSI.Cashier.Controls;
namespace WSI.Cashier
{
  partial class frm_player_enter_pin
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.tmr_status_label = new System.Windows.Forms.Timer(this.components);
      this.btn_ok = new WSI.Cashier.Controls.uc_round_button();
      this.lbl_status = new WSI.Cashier.Controls.uc_label();
      this.lbl_amount_value = new WSI.Cashier.Controls.uc_label();
      this.lbl_amount = new WSI.Cashier.Controls.uc_label();
      this.lbl_text_to_read = new WSI.Cashier.Controls.uc_label();
      this.txt_pin_value = new WSI.Cashier.Controls.uc_round_textbox();
      this.lbl_enter_pin = new WSI.Cashier.Controls.uc_label();
      this.btn_cancel = new WSI.Cashier.Controls.uc_round_button();
      this.pnl_data.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.Controls.Add(this.btn_ok);
      this.pnl_data.Controls.Add(this.lbl_status);
      this.pnl_data.Controls.Add(this.lbl_amount_value);
      this.pnl_data.Controls.Add(this.lbl_amount);
      this.pnl_data.Controls.Add(this.lbl_text_to_read);
      this.pnl_data.Controls.Add(this.txt_pin_value);
      this.pnl_data.Controls.Add(this.lbl_enter_pin);
      this.pnl_data.Controls.Add(this.btn_cancel);
      this.pnl_data.Size = new System.Drawing.Size(390, 255);
      // 
      // tmr_status_label
      // 
      this.tmr_status_label.Interval = 500;
      this.tmr_status_label.Tick += new System.EventHandler(this.tmr_status_label_Tick);
      // 
      // btn_ok
      // 
      this.btn_ok.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_ok.FlatAppearance.BorderSize = 0;
      this.btn_ok.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_ok.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_ok.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_ok.Image = null;
      this.btn_ok.Location = new System.Drawing.Point(231, 196);
      this.btn_ok.Name = "btn_ok";
      this.btn_ok.Size = new System.Drawing.Size(124, 46);
      this.btn_ok.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_ok.TabIndex = 113;
      this.btn_ok.Text = "XOK";
      this.btn_ok.UseVisualStyleBackColor = false;
      this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
      // 
      // lbl_status
      // 
      this.lbl_status.BackColor = System.Drawing.Color.Transparent;
      this.lbl_status.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_status.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.lbl_status.Location = new System.Drawing.Point(31, 163);
      this.lbl_status.Name = "lbl_status";
      this.lbl_status.Size = new System.Drawing.Size(324, 23);
      this.lbl_status.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_RED;
      this.lbl_status.TabIndex = 118;
      this.lbl_status.Text = "xInvalid PIN / Card blocked";
      this.lbl_status.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.lbl_status.Visible = false;
      // 
      // lbl_amount_value
      // 
      this.lbl_amount_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_amount_value.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_amount_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_amount_value.Location = new System.Drawing.Point(212, 61);
      this.lbl_amount_value.Name = "lbl_amount_value";
      this.lbl_amount_value.Size = new System.Drawing.Size(149, 16);
      this.lbl_amount_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_amount_value.TabIndex = 117;
      this.lbl_amount_value.Text = "$999.99";
      this.lbl_amount_value.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_amount
      // 
      this.lbl_amount.BackColor = System.Drawing.Color.Transparent;
      this.lbl_amount.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_amount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_amount.Location = new System.Drawing.Point(53, 61);
      this.lbl_amount.Name = "lbl_amount";
      this.lbl_amount.Size = new System.Drawing.Size(153, 16);
      this.lbl_amount.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_amount.TabIndex = 116;
      this.lbl_amount.Text = "xAmount:";
      this.lbl_amount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_text_to_read
      // 
      this.lbl_text_to_read.BackColor = System.Drawing.Color.Transparent;
      this.lbl_text_to_read.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_text_to_read.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_text_to_read.Location = new System.Drawing.Point(25, 3);
      this.lbl_text_to_read.Name = "lbl_text_to_read";
      this.lbl_text_to_read.Size = new System.Drawing.Size(330, 50);
      this.lbl_text_to_read.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_text_to_read.TabIndex = 115;
      this.lbl_text_to_read.Text = "xAsk the client to enter his/her PIN to authorize the withdrawal.";
      this.lbl_text_to_read.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // txt_pin_value
      // 
      this.txt_pin_value.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_pin_value.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_pin_value.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_pin_value.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_pin_value.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_pin_value.CornerRadius = 5;
      this.txt_pin_value.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_pin_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.txt_pin_value.Location = new System.Drawing.Point(127, 127);
      this.txt_pin_value.MaxLength = 3;
      this.txt_pin_value.Multiline = false;
      this.txt_pin_value.Name = "txt_pin_value";
      this.txt_pin_value.PasswordChar = '*';
      this.txt_pin_value.ReadOnly = false;
      this.txt_pin_value.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_pin_value.SelectedText = "";
      this.txt_pin_value.SelectionLength = 0;
      this.txt_pin_value.SelectionStart = 0;
      this.txt_pin_value.Size = new System.Drawing.Size(127, 33);
      this.txt_pin_value.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC_CUSTOM;
      this.txt_pin_value.TabIndex = 111;
      this.txt_pin_value.TabStop = false;
      this.txt_pin_value.Text = "xxxx";
      this.txt_pin_value.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.txt_pin_value.UseSystemPasswordChar = false;
      this.txt_pin_value.WaterMark = null;
      this.txt_pin_value.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_pin_number_KeyPress);
      // 
      // lbl_enter_pin
      // 
      this.lbl_enter_pin.BackColor = System.Drawing.Color.Transparent;
      this.lbl_enter_pin.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_enter_pin.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_enter_pin.Location = new System.Drawing.Point(61, 95);
      this.lbl_enter_pin.Name = "lbl_enter_pin";
      this.lbl_enter_pin.Size = new System.Drawing.Size(258, 25);
      this.lbl_enter_pin.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_enter_pin.TabIndex = 114;
      this.lbl_enter_pin.Text = "xEnter the player PIN code";
      this.lbl_enter_pin.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // btn_cancel
      // 
      this.btn_cancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btn_cancel.FlatAppearance.BorderSize = 0;
      this.btn_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_cancel.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_cancel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_cancel.Image = null;
      this.btn_cancel.Location = new System.Drawing.Point(34, 196);
      this.btn_cancel.Name = "btn_cancel";
      this.btn_cancel.Size = new System.Drawing.Size(124, 46);
      this.btn_cancel.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_cancel.TabIndex = 112;
      this.btn_cancel.Text = "XCANCEL";
      this.btn_cancel.UseVisualStyleBackColor = false;
      this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
      // 
      // frm_player_enter_pin
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(390, 310);
      this.ControlBox = false;
      this.Name = "frm_player_enter_pin";
      this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
      this.Style = WSI.Cashier.Controls.frm_base.BaseFormStyle.PRINCIPAL;
      this.Text = "xIntroduce Pin";
      this.Shown += new System.EventHandler(this.frm_player_enter_pin_Shown);
      this.pnl_data.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Timer tmr_status_label;
    private uc_round_button btn_ok;
    private uc_label lbl_status;
    private uc_label lbl_amount_value;
    private uc_label lbl_amount;
    private uc_label lbl_text_to_read;
    private uc_round_textbox txt_pin_value;
    private uc_label lbl_enter_pin;
    private uc_round_button btn_cancel;
  }
}