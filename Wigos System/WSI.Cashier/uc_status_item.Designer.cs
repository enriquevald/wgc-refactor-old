namespace WSI.Cashier
{
  partial class uc_status_item
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose (bool disposing)
    {
      if ( disposing && ( components != null ) )
      {
        components.Dispose ();
      }
      base.Dispose (disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent ()
    {
      this.lbl_status = new System.Windows.Forms.Label();
      this.pic_status = new System.Windows.Forms.PictureBox();
      ((System.ComponentModel.ISupportInitialize)(this.pic_status)).BeginInit();
      this.SuspendLayout();
      // 
      // lbl_status
      // 
      this.lbl_status.Anchor = System.Windows.Forms.AnchorStyles.Right;
      this.lbl_status.BackColor = System.Drawing.Color.Transparent;
      this.lbl_status.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_status.ForeColor = System.Drawing.Color.White;
      this.lbl_status.Location = new System.Drawing.Point(2, 4);
      this.lbl_status.Margin = new System.Windows.Forms.Padding(0);
      this.lbl_status.Name = "lbl_status";
      this.lbl_status.Size = new System.Drawing.Size(96, 28);
      this.lbl_status.TabIndex = 10;
      this.lbl_status.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // pic_status
      // 
      this.pic_status.Dock = System.Windows.Forms.DockStyle.Left;
      this.pic_status.Image = global::WSI.Cashier.Properties.Resources.shader;
      this.pic_status.Location = new System.Drawing.Point(2, 4);
      this.pic_status.Margin = new System.Windows.Forms.Padding(0);
      this.pic_status.Name = "pic_status";
      this.pic_status.Size = new System.Drawing.Size(24, 28);
      this.pic_status.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
      this.pic_status.TabIndex = 9;
      this.pic_status.TabStop = false;
      this.pic_status.Click += new System.EventHandler(this.pic_status_Click);
      // 
      // uc_status_item
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.Color.Transparent;
      this.Controls.Add(this.pic_status);
      this.Controls.Add(this.lbl_status);
      this.Margin = new System.Windows.Forms.Padding(0);
      this.Name = "uc_status_item";
      this.Padding = new System.Windows.Forms.Padding(2, 4, 2, 0);
      this.Size = new System.Drawing.Size(100, 32);
      ((System.ComponentModel.ISupportInitialize)(this.pic_status)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.PictureBox pic_status;
    public System.Windows.Forms.Label lbl_status;


  }
}
