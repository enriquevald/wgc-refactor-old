﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_reserved.cs
// 
//   DESCRIPTION: Reserved amount
//
//        AUTHOR: Alberto Marcos
// 
// CREATION DATE: 06-NOV-2015
//  
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 06-NOV-2015 AMF     First release.
// 07-DIC-2015 FJC     Fixed BUG: 7360 (Added Cashier Movement when we reserve credit)
// 03-FEB-2016 FJC     Product Backlog Item 8930:Cajero: Pasar la pantalla de reserva de crédito de BonoPlay al formato nuevo
// 04-APR-2016 FJC     BUG 7585: No se insertan Movimientos de cuenta al reservar créditos desde el LCD
// 22-JUN-2017 FJC     WIGOS-3142 WS2S - Set / Retrieve Reserved Credit on the Cashier
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using WSI.Common;
using WSI.Cashier.Controls;

namespace WSI.Cashier
{
  public partial class frm_reserved : frm_base
  {
    #region Members

    private Decimal m_amount = 0;

    #endregion Members

    #region Attributes

    private frm_yesno form_yes_no;
    private static CardData m_card;
    private static String m_decimal_str = System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencyDecimalSeparator;

    #endregion

    #region Constructor

    //------------------------------------------------------------------------------
    // PURPOSE : Initialize form
    //
    //  PARAMS :
    //      - INPUT :
    //          - CardData
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    public frm_reserved(CardData Card)
    {
      InitializeComponent();

      m_card = Card;

      InitializeControlResources();

      Init();
    } // frm_reserved

    #endregion Constructor

    #region Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Initialize screen logic
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES : 
    private void Init()
    {
      // Center screen
      this.Location = new Point(1024 / 2 - this.Width / 2, 0);

      // Show keyboard if Automatic OSK it's enabled
      WSIKeyboard.Toggle();

      // Set focus
      this.btn_ok.Select();

      form_yes_no = new frm_yesno();
      form_yes_no.Opacity = 0.6;
    } // Init

    //------------------------------------------------------------------------------
    // PURPOSE : Initialize control resources (NLS strings, images, etc).
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    private void InitializeControlResources()
    {
      //String _provider_gp_id; // Don't use for anything

      chk_mode_reserved.Text = Resource.String("GAMING_TABLE_PROPERTY_ENABLED");
      chk_mode_reserved.Checked = m_card.AccountBalance.ModeReserved;

      chk_mode_reserved_CheckedChanged(null, null);

      btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");

      //_provider_gp_id = GeneralParam.GetString("GameGateway", "Provider", "001"); // Don't use for anything

      if (Misc.IsGameGatewayEnabled())
      {
        lbl_reserved_title.Text = Resource.String("STR_RESERVED_TITLE", GeneralParam.GetString("GameGateway", "Name", "BonoPlay"));
        gb_reserved.HeaderText = Resource.String("STR_RESERVED_REDEEM_GP", GeneralParam.GetString("GameGateway", "Name", "BonoPlay"));
      }
      else
      {
        if (Misc.IsWS2SFBMEnabled())
        {
          lbl_reserved_title.Text = Resource.String("STR_RESERVED_TITLE", GeneralParam.GetString("WS2S", "TextReserved", String.Empty));
          gb_reserved.HeaderText = Resource.String("STR_RESERVED_REDEEM_GP", GeneralParam.GetString("WS2S", "TextReserved", String.Empty));
        }
      }

      gb_total_redeem.HeaderText = Resource.String("STR_RESERVED_REDEEM_BALANCE");
      lbl_total_redeem.Text = ((Currency)m_card.AccountBalance.TotalRedeemable).ToString();
      
      m_amount = m_card.AccountBalance.Reserved;
      lbl_reserved.Text = ((Currency)m_card.AccountBalance.Reserved).ToString();

    } // InitializeControlResources

    #endregion Private Methods

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Init and Show the form.
    //
    //  PARAMS :
    //      - INPUT :
    //          - ParentForm
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public Boolean Show(Form ParentForm)
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_reserved", Log.Type.Message);

      form_yes_no.Show();

      if (WindowManager.StartUpMode == WindowManager.StartUpModes.Explorer)
      {
        this.Location = new Point(WindowManager.GetFormCenterLocation(form_yes_no).X - (this.Width / 2), form_yes_no.Location.Y + 50);
      }
      this.ShowDialog(form_yes_no);

      form_yes_no.Hide();

      return (this.DialogResult != DialogResult.Abort);
    } // Show

    //------------------------------------------------------------------------------
    // PURPOSE : Dispose yes_no form.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    new public void Dispose()
    {
      form_yes_no.Dispose();
      form_yes_no = null;

      base.Dispose();
    } // Dispose

    #endregion Public Methods

    #region ControlsHandling

    //------------------------------------------------------------------------------
    // PURPOSE : Button 1 click
    //
    //  PARAMS :
    //      - INPUT :
    //          - Sender
    //          - EventArgs
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    //private void btn_num_1_Click(object sender, EventArgs e)
    //{
    //  AddNumber(((System.Windows.Forms.ButtonBase)(sender)).Text);
    //} // btn_num_1_Click

    //------------------------------------------------------------------------------
    // PURPOSE : Button 2 click
    //
    //  PARAMS :
    //      - INPUT :
    //          - Sender
    //          - EventArgs
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    //private void btn_num_2_Click(object sender, EventArgs e)
    //{
    //  AddNumber(((System.Windows.Forms.ButtonBase)(sender)).Text);
    //} // btn_num_2_Click

    //------------------------------------------------------------------------------
    // PURPOSE : Button 3 click
    //
    //  PARAMS :
    //      - INPUT :
    //          - Sender
    //          - EventArgs
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    //private void btn_num_3_Click(object sender, EventArgs e)
    //{
    //  AddNumber(((System.Windows.Forms.ButtonBase)(sender)).Text);
    //} // btn_num_3_Click

    //------------------------------------------------------------------------------
    // PURPOSE : Button 4 click
    //
    //  PARAMS :
    //      - INPUT :
    //          - Sender
    //          - EventArgs
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    //private void btn_num_4_Click(object sender, EventArgs e)
    //{
    //  AddNumber(((System.Windows.Forms.ButtonBase)(sender)).Text);
    //} // btn_num_4_Click

    //------------------------------------------------------------------------------
    // PURPOSE : Button 5 click
    //
    //  PARAMS :
    //      - INPUT :
    //          - Sender
    //          - EventArgs
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    //private void btn_num_5_Click(object sender, EventArgs e)
    //{
    //  AddNumber(((System.Windows.Forms.ButtonBase)(sender)).Text);
    //} // btn_num_5_Click

    //------------------------------------------------------------------------------
    // PURPOSE : Button 6 click
    //
    //  PARAMS :
    //      - INPUT :
    //          - Sender
    //          - EventArgs
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    //private void btn_num_6_Click(object sender, EventArgs e)
    //{
    //  AddNumber(((System.Windows.Forms.ButtonBase)(sender)).Text);
    //} // btn_num_6_Click

    //------------------------------------------------------------------------------
    // PURPOSE : Button 7 click
    //
    //  PARAMS :
    //      - INPUT :
    //          - Sender
    //          - EventArgs
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    //private void btn_num_7_Click(object sender, EventArgs e)
    //{
    //  AddNumber(((System.Windows.Forms.ButtonBase)(sender)).Text);
    //} // btn_num_7_Click

    //------------------------------------------------------------------------------
    // PURPOSE : Button 8 click
    //
    //  PARAMS :
    //      - INPUT :
    //          - Sender
    //          - EventArgs
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    //private void btn_num_8_Click(object sender, EventArgs e)
    //{
    //  AddNumber(((System.Windows.Forms.ButtonBase)(sender)).Text);
    //} // btn_num_8_Click

    //------------------------------------------------------------------------------
    // PURPOSE : Button 9 click
    //
    //  PARAMS :
    //      - INPUT :
    //          - Sender
    //          - EventArgs
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    //private void btn_num_9_Click(object sender, EventArgs e)
    //{
    //  AddNumber(((System.Windows.Forms.ButtonBase)(sender)).Text);
    //} // btn_num_9_Click

    //------------------------------------------------------------------------------
    // PURPOSE : Button 0 click
    //
    //  PARAMS :
    //      - INPUT :
    //          - Sender
    //          - EventArgs
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    //private void btn_num_10_Click(object sender, EventArgs e)
    //{
    //  AddNumber(((System.Windows.Forms.ButtonBase)(sender)).Text);
    //} // btn_num_10_Click

    //------------------------------------------------------------------------------
    // PURPOSE : Button money 1 click
    //
    //  PARAMS :
    //      - INPUT :
    //          - Sender
    //          - EventArgs
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    //private void btn_money_1_Click(object sender, EventArgs e)
    //{
    //  Decimal _money_value;
    //  String _money_text;

    //  //try
    //  //{
    //  //  _money_value = Decimal.Parse(btn_money_1_value);
    //  //  if (txt_amount.Text.Length > 0)
    //  //  {
    //  //    _money_value = _money_value + Decimal.Parse(txt_amount.Text);
    //  //  }
    //  //}
    //  //catch
    //  //{
    //  //  _money_value = 0;
    //  //}

    //  //if (_money_value <= Cashier.MAX_ALLOWED_AMOUNT)
    //  //{
    //  //  _money_text = _money_value.ToString();

    //  //  txt_amount.Text = "";
    //  //  AddNumber(_money_text);
    //  //}
    //} // btn_money_1_Click

    //------------------------------------------------------------------------------
    // PURPOSE : Button money 2 click
    //
    //  PARAMS :
    //      - INPUT :
    //          - Sender
    //          - EventArgs
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    //private void btn_money_2_Click(object sender, EventArgs e)
    //{
    //  Decimal _money_value;
    //  String _money_text;

    //  //try
    //  //{
    //  //  _money_value = Decimal.Parse(btn_money_2_value);
    //  //  if (txt_amount.Text.Length > 0)
    //  //  {
    //  //    _money_value = _money_value + Decimal.Parse(txt_amount.Text);
    //  //  }
    //  //}
    //  //catch
    //  //{
    //  //  _money_value = 0;
    //  //}

    //  //if (_money_value <= Cashier.MAX_ALLOWED_AMOUNT)
    //  //{
    //  //  _money_text = _money_value.ToString();

    //  //  txt_amount.Text = "";
    //  //  AddNumber(_money_text);
    //  //}
    //} // btn_money_2_Click

    //------------------------------------------------------------------------------
    // PURPOSE : Button money 3 click
    //
    //  PARAMS :
    //      - INPUT :
    //          - Sender
    //          - EventArgs
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    //private void btn_money_3_Click(object sender, EventArgs e)
    //{
    //  Decimal _money_value;
    //  String _money_text;

    //  //try
    //  //{
    //  //  _money_value = Decimal.Parse(btn_money_3_value);
    //  //  if (txt_amount.Text.Length > 0)
    //  //  {
    //  //    _money_value = _money_value + Decimal.Parse(txt_amount.Text);
    //  //  }
    //  //}
    //  //catch
    //  //{
    //  //  _money_value = 0;
    //  //}

    //  //if (_money_value <= Cashier.MAX_ALLOWED_AMOUNT)
    //  //{
    //  //  _money_text = _money_value.ToString();

    //  //  txt_amount.Text = "";
    //  //  AddNumber(_money_text);
    //  //}
    //} // btn_money_3_Click

    //------------------------------------------------------------------------------
    // PURPOSE : Button money 4 click
    //
    //  PARAMS :
    //      - INPUT :
    //          - Sender
    //          - EventArgs
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    //private void btn_money_4_Click(object sender, EventArgs e)
    //{
    //  Decimal _money_value;
    //  String _money_text;

    //  //try
    //  //{
    //  //  _money_value = Decimal.Parse(btn_money_4_value);
    //  //  if (txt_amount.Text.Length > 0)
    //  //  {
    //  //    _money_value = _money_value + Decimal.Parse(txt_amount.Text);
    //  //  }
    //  //}
    //  //catch
    //  //{
    //  //  _money_value = 0;
    //  //}

    //  //if (_money_value <= Cashier.MAX_ALLOWED_AMOUNT)
    //  //{
    //  //  _money_text = _money_value.ToString();

    //  //  txt_amount.Text = "";
    //  //  AddNumber(_money_text);
    //  //}
    //} // btn_money_4_Click

    //------------------------------------------------------------------------------
    // PURPOSE : Button num dot click
    //
    //  PARAMS :
    //      - INPUT :
    //          - Sender
    //          - EventArgs
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    //private void btn_num_dot_Click(object sender, EventArgs e)
    //{
    //  //if (txt_amount.Text.IndexOf(m_decimal_str) == -1)
    //  //{
    //  //  txt_amount.Text = txt_amount.Text + m_decimal_str;
    //  //}
    //  //btn_intro.Focus();
    //} // btn_num_dot_Click

    //------------------------------------------------------------------------------
    // PURPOSE : Handle the click on the intro button, which confirms the amount on the
    //           numeric pad.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Sender
    //          - EventArgs
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    //private void btn_intro_Click(object sender, EventArgs e)
    //{
    //  Decimal _input_amount;

    //  //Decimal.TryParse(txt_amount.Text, out _input_amount);

    //  //if (_input_amount < 0)
    //  //{
    //  //  frm_message.Show(Resource.String("STR_FRM_AMOUNT_INPUT_MSG_CASH_DESK_CLOSE_COLLECTED_AMOUNT"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this);

    //  //  return;
    //  //}

    //  //if (_input_amount > m_card.AccountBalance.TotalRedeemable)
    //  //{
    //  //  frm_message.Show(Resource.String("STR_RESERVED_NOT_ENOUGH_BALANCE"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this);

    //  //  return;
    //  //}

    //  //m_amount = _input_amount;
    //  //lbl_reserved.Text = ((Currency)_input_amount).ToString();
    //  //txt_amount.Text = String.Empty;
    //} // btn_intro_Click

    //------------------------------------------------------------------------------
    // PURPOSE : Button back click
    //
    //  PARAMS :
    //      - INPUT :
    //          - Sender
    //          - EventArgs
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    //private void btn_back_Click(object sender, EventArgs e)
    //{
    //  ////if (txt_amount.Text.Length > 0)
    //  ////{
    //  ////  txt_amount.Text = txt_amount.Text.Substring(0, txt_amount.Text.Length - 1);
    //  ////}
    //  ////btn_intro.Focus();
    //} // btn_back_Click

    //------------------------------------------------------------------------------
    // PURPOSE : Button accept click
    //
    //  PARAMS :
    //      - INPUT :
    //          - Sender
    //          - EventArgs
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    //private void btn_accept_Click(object sender, EventArgs e)
    private void btn_accept_Click(object sender, EventArgs e)
    {
      MultiPromos.REQUEST_TYPE_RESPONSE _status;
      Decimal _initial_reserved_credit;
      AccountMovementsTable _account_movements;

      _status = MultiPromos.REQUEST_TYPE_RESPONSE.REQUEST_TYPE_RESPONSE_ERROR;

      try
      {
        if (!chk_mode_reserved.Checked)
        {
          m_amount = 0;
        }

        using (DB_TRX _trx = new DB_TRX())
        {
          //1. Process for reserve credit
          if (MultiPromos.Trx_GameGateway_Reserve_Credit(m_card.AccountId,
                                                          m_amount,
                                                          MultiPromos.REQUEST_TYPE.MODIFY_RESERVED_CREDIT,
                                                          chk_mode_reserved.Checked,
                                                          out _status,
                                                          out _initial_reserved_credit,
                                                          _trx.SqlTransaction)
            && _status == MultiPromos.REQUEST_TYPE_RESPONSE.REQUEST_TYPE_RESPONSE_OK)
          {

            //2. Process for insert account movement

            _account_movements = new AccountMovementsTable(Cashier.CashierSessionInfo());
            if (!WSI.Common.GameGateway.InsertAccountMovementReserveCredit(Cashier.CashierSessionInfo(),
                                                                           _account_movements,
                                                                           m_card.AccountId,
                                                                           m_amount,
                                                                           _initial_reserved_credit,
                                                                           _trx.SqlTransaction))
            {
              frm_message.Show(Resource.String("STR_RESERVED_ERROR"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this);

              return;
            }
          }

          switch (_status)
          {
            case MultiPromos.REQUEST_TYPE_RESPONSE.REQUEST_TYPE_RESPONSE_OK:

              _trx.Commit();

              // Hide keyboard and close
              WSIKeyboard.Hide();

              Misc.WriteLog("[FORM CLOSE] frm_reserved (accept - case request_type_response_ok)", Log.Type.Message);
              this.Close();

              break;

            case MultiPromos.REQUEST_TYPE_RESPONSE.REQUEST_TYPE_RESPONSE_ERROR:

              frm_message.Show(Resource.String("STR_RESERVED_ERROR"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this);

              break;

            case MultiPromos.REQUEST_TYPE_RESPONSE.REQUEST_TYPE_RESPONSE_NOT_ENOUGH_CREDIT:

              frm_message.Show(Resource.String("STR_RESERVED_NOT_ENOUGH_BALANCE"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this);

              break;
          }
        }
      }

      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } // btn_accept_Click

    //------------------------------------------------------------------------------
    // PURPOSE : Button cancel click
    //
    //  PARAMS :
    //      - INPUT :
    //          - Sender
    //          - EventArgs
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void btn_cancel_Click(object sender, EventArgs e)
    {
      // hide keyboard and close
      WSIKeyboard.Hide();

      Misc.WriteLog("[FORM CLOSE] frm_reserved (cancel)", Log.Type.Message);
      this.Close();
    } // btn_cancel_Click

    //------------------------------------------------------------------------------
    // PURPOSE : Check mode reserved changed
    //
    //  PARAMS :
    //      - INPUT :
    //          - Sender
    //          - EventArgs
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void chk_mode_reserved_CheckedChanged(object sender, EventArgs e)
    {
      gb_total_redeem.Enabled = chk_mode_reserved.Checked;
      gb_reserved.Enabled = chk_mode_reserved.Checked;
      this.uc_input_amount.Enabled = chk_mode_reserved.Checked;

      //TODO Revisar en uc_inpunt_amount.cs / uc_round_panel.cs
      this.uc_input_amount.gp_digits_box.HeaderHeight = 55;
      this.uc_input_amount.gp_digits_box.HeaderBackColor = WSI.Cashier.CashierStyle.Colors.m_colors_dic[CashierStyle.Colors.ENUM_COLORS.HEX_373B3F];
    } // chk_mode_reserved_CheckedChanged

    //------------------------------------------------------------------------------
    // PURPOSE : Event when click ENTER button
    //
    //  PARAMS :
    //      - INPUT :
    //          - Sender
    //          - EventArgs
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void uc_input_amount_OnAmountSelected(decimal Amount)
    {
      Decimal _input_amount;

      Decimal.TryParse(this.uc_input_amount.txt_amount.Text, out _input_amount);

      if (_input_amount < 0)
      {
        frm_message.Show(Resource.String("STR_FRM_AMOUNT_INPUT_MSG_CASH_DESK_CLOSE_COLLECTED_AMOUNT"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this);

        return;
      }

      if (_input_amount > m_card.AccountBalance.TotalRedeemable)
      {
        frm_message.Show(Resource.String("STR_RESERVED_NOT_ENOUGH_BALANCE"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this);

        return;
      }

      m_amount = _input_amount;
      lbl_reserved.Text = ((Currency)_input_amount).ToString();
      this.uc_input_amount.txt_amount.Text = String.Empty;
    } // uc_input_amount_OnAmountSelected

    #endregion ControlsHandling

  }
}
