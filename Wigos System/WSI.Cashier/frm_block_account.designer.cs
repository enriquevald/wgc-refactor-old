namespace WSI.Cashier
{
  partial class frm_block_account
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }
    
    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_block_account));
      this.btn_keyboard = new WSI.Cashier.Controls.uc_round_button();
      this.btn_cancel = new WSI.Cashier.Controls.uc_round_button();
      this.btn_accept = new WSI.Cashier.Controls.uc_round_button();
      this.gb_block_unblock = new WSI.Cashier.Controls.uc_round_panel();
      this.uc_blacklist_checkbox = new WSI.Cashier.Controls.uc_checkBox();
      this.txt_block_description = new WSI.Cashier.Controls.uc_round_textbox();
      this.lbl_block_unblock_description = new WSI.Cashier.Controls.uc_label();
      this.lbl_unblock_description = new WSI.Cashier.Controls.uc_label();
      this.txt_reason = new WSI.Cashier.Controls.uc_round_textbox();
      this.pnl_data.SuspendLayout();
      this.gb_block_unblock.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.Controls.Add(this.btn_cancel);
      this.pnl_data.Controls.Add(this.btn_accept);
      this.pnl_data.Controls.Add(this.gb_block_unblock);
      this.pnl_data.Controls.Add(this.btn_keyboard);
      this.pnl_data.Size = new System.Drawing.Size(623, 496);
      // 
      // btn_keyboard
      // 
      this.btn_keyboard.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(47)))), ((int)(((byte)(50)))));
      this.btn_keyboard.FlatAppearance.BorderSize = 0;
      this.btn_keyboard.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_keyboard.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_keyboard.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_keyboard.Image = ((System.Drawing.Image)(resources.GetObject("btn_keyboard.Image")));
      this.btn_keyboard.IsSelected = false;
      this.btn_keyboard.Location = new System.Drawing.Point(14, 424);
      this.btn_keyboard.Name = "btn_keyboard";
      this.btn_keyboard.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_keyboard.Size = new System.Drawing.Size(80, 60);
      this.btn_keyboard.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.TERTIARY;
      this.btn_keyboard.TabIndex = 0;
      this.btn_keyboard.TabStop = false;
      this.btn_keyboard.UseVisualStyleBackColor = false;
      this.btn_keyboard.Click += new System.EventHandler(this.btn_keyboard_Click);
      // 
      // btn_cancel
      // 
      this.btn_cancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_cancel.FlatAppearance.BorderSize = 0;
      this.btn_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_cancel.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_cancel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_cancel.Image = null;
      this.btn_cancel.IsSelected = false;
      this.btn_cancel.Location = new System.Drawing.Point(279, 425);
      this.btn_cancel.Name = "btn_cancel";
      this.btn_cancel.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_cancel.Size = new System.Drawing.Size(155, 60);
      this.btn_cancel.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_cancel.TabIndex = 2;
      this.btn_cancel.Text = "XCANCEL";
      this.btn_cancel.UseVisualStyleBackColor = false;
      this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
      // 
      // btn_accept
      // 
      this.btn_accept.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_accept.FlatAppearance.BorderSize = 0;
      this.btn_accept.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_accept.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_accept.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_accept.Image = null;
      this.btn_accept.IsSelected = false;
      this.btn_accept.Location = new System.Drawing.Point(448, 425);
      this.btn_accept.Name = "btn_accept";
      this.btn_accept.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_accept.Size = new System.Drawing.Size(155, 60);
      this.btn_accept.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_accept.TabIndex = 1;
      this.btn_accept.Text = "XACCEPT";
      this.btn_accept.UseVisualStyleBackColor = false;
      this.btn_accept.Click += new System.EventHandler(this.btn_accept_Click);
      // 
      // gb_block_unblock
      // 
      this.gb_block_unblock.BackColor = System.Drawing.Color.WhiteSmoke;
      this.gb_block_unblock.BorderColor = System.Drawing.Color.Empty;
      this.gb_block_unblock.Controls.Add(this.uc_blacklist_checkbox);
      this.gb_block_unblock.Controls.Add(this.txt_block_description);
      this.gb_block_unblock.Controls.Add(this.lbl_block_unblock_description);
      this.gb_block_unblock.Controls.Add(this.lbl_unblock_description);
      this.gb_block_unblock.Controls.Add(this.txt_reason);
      this.gb_block_unblock.CornerRadius = 10;
      this.gb_block_unblock.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_block_unblock.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gb_block_unblock.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_block_unblock.HeaderHeight = 35;
      this.gb_block_unblock.HeaderSubText = null;
      this.gb_block_unblock.HeaderText = null;
      this.gb_block_unblock.Location = new System.Drawing.Point(17, 17);
      this.gb_block_unblock.Name = "gb_block_unblock";
      this.gb_block_unblock.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_block_unblock.Size = new System.Drawing.Size(586, 399);
      this.gb_block_unblock.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.gb_block_unblock.TabIndex = 0;
      // 
      // uc_blacklist_checkbox
      // 
      this.uc_blacklist_checkbox.AutoSize = true;
      this.uc_blacklist_checkbox.BackColor = System.Drawing.Color.Transparent;
      this.uc_blacklist_checkbox.Checked = true;
      this.uc_blacklist_checkbox.CheckState = System.Windows.Forms.CheckState.Checked;
      this.uc_blacklist_checkbox.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.uc_blacklist_checkbox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.uc_blacklist_checkbox.Location = new System.Drawing.Point(15, 353);
      this.uc_blacklist_checkbox.MinimumSize = new System.Drawing.Size(197, 30);
      this.uc_blacklist_checkbox.Name = "uc_blacklist_checkbox";
      this.uc_blacklist_checkbox.Padding = new System.Windows.Forms.Padding(5);
      this.uc_blacklist_checkbox.Size = new System.Drawing.Size(197, 33);
      this.uc_blacklist_checkbox.TabIndex = 4;
      this.uc_blacklist_checkbox.Text = "uc_blacklist_checkbox";
      this.uc_blacklist_checkbox.UseVisualStyleBackColor = false;
      // 
      // txt_block_description
      // 
      this.txt_block_description.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txt_block_description.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_block_description.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_block_description.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_block_description.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_block_description.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_block_description.CornerRadius = 5;
      this.txt_block_description.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_block_description.Location = new System.Drawing.Point(15, 64);
      this.txt_block_description.MaxLength = 256;
      this.txt_block_description.Multiline = true;
      this.txt_block_description.Name = "txt_block_description";
      this.txt_block_description.PasswordChar = '\0';
      this.txt_block_description.ReadOnly = true;
      this.txt_block_description.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
      this.txt_block_description.SelectedText = "";
      this.txt_block_description.SelectionLength = 0;
      this.txt_block_description.SelectionStart = 0;
      this.txt_block_description.Size = new System.Drawing.Size(557, 125);
      this.txt_block_description.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.MULTILINE;
      this.txt_block_description.TabIndex = 1;
      this.txt_block_description.TabStop = false;
      this.txt_block_description.Text = "xBlockDescription";
      this.txt_block_description.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_block_description.UseSystemPasswordChar = false;
      this.txt_block_description.WaterMark = null;
      this.txt_block_description.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // lbl_block_unblock_description
      // 
      this.lbl_block_unblock_description.AutoSize = true;
      this.lbl_block_unblock_description.BackColor = System.Drawing.Color.Transparent;
      this.lbl_block_unblock_description.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_block_unblock_description.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_block_unblock_description.Location = new System.Drawing.Point(10, 198);
      this.lbl_block_unblock_description.Name = "lbl_block_unblock_description";
      this.lbl_block_unblock_description.Size = new System.Drawing.Size(242, 19);
      this.lbl_block_unblock_description.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_block_unblock_description.TabIndex = 2;
      this.lbl_block_unblock_description.Text = "xDescriptionBlockUnblockReason";
      // 
      // lbl_unblock_description
      // 
      this.lbl_unblock_description.AutoSize = true;
      this.lbl_unblock_description.BackColor = System.Drawing.Color.Transparent;
      this.lbl_unblock_description.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_unblock_description.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_unblock_description.Location = new System.Drawing.Point(10, 42);
      this.lbl_unblock_description.Name = "lbl_unblock_description";
      this.lbl_unblock_description.Size = new System.Drawing.Size(237, 19);
      this.lbl_unblock_description.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_unblock_description.TabIndex = 0;
      this.lbl_unblock_description.Text = "xDescriptionBlockUnblockAction";
      // 
      // txt_reason
      // 
      this.txt_reason.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txt_reason.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_reason.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_reason.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_reason.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_reason.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_reason.CornerRadius = 5;
      this.txt_reason.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_reason.Location = new System.Drawing.Point(15, 220);
      this.txt_reason.MaxLength = 255;
      this.txt_reason.Multiline = true;
      this.txt_reason.Name = "txt_reason";
      this.txt_reason.PasswordChar = '\0';
      this.txt_reason.ReadOnly = false;
      this.txt_reason.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
      this.txt_reason.SelectedText = "";
      this.txt_reason.SelectionLength = 0;
      this.txt_reason.SelectionStart = 0;
      this.txt_reason.Size = new System.Drawing.Size(557, 125);
      this.txt_reason.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.MULTILINE;
      this.txt_reason.TabIndex = 3;
      this.txt_reason.TabStop = false;
      this.txt_reason.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_reason.UseSystemPasswordChar = false;
      this.txt_reason.WaterMark = null;
      this.txt_reason.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // frm_block_account
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(623, 551);
      this.Name = "frm_block_account";
      this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
      this.Style = WSI.Cashier.Controls.frm_base.BaseFormStyle.PRINCIPAL;
      this.Text = "Block / Unblock Account";
      this.pnl_data.ResumeLayout(false);
      this.gb_block_unblock.ResumeLayout(false);
      this.gb_block_unblock.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion


    private WSI.Cashier.Controls.uc_round_panel gb_block_unblock;
    private WSI.Cashier.Controls.uc_label lbl_block_unblock_description;
    private WSI.Cashier.Controls.uc_label lbl_unblock_description;
    private WSI.Cashier.Controls.uc_round_textbox txt_reason;
    private WSI.Cashier.Controls.uc_round_button btn_keyboard;
    private WSI.Cashier.Controls.uc_round_button btn_cancel;
    private WSI.Cashier.Controls.uc_round_button btn_accept;
    private WSI.Cashier.Controls.uc_round_textbox txt_block_description;
    private Controls.uc_checkBox uc_blacklist_checkbox;
  }
}