﻿//------------------------------------------------------------------------------
// Copyright © 2007-2008 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_gaming_hall.cs
//  
//   DESCRIPTION: Implements the class uc_gaming_hall (Collect, fill and read terminal meters.)
// 
//        AUTHOR Enric Tomas
// 
// CREATION DATE: 26-OCT-2015
// 
// REVISION HISTORY:
// 
// Date        Author     Description
//------------------------------------------------------------------------------
// 26-OCT-2015 ETP        Inital Draft.
//------------------------------------------------------------------------------

#region Usings

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WSI.Common;
using WSI.Cashier.Controls;

#endregion // Usings

namespace WSI.Cashier
{
  public partial class frm_waiting_message : frm_base, IDisposable
  {
    
    #region Atributes

    private int m_Elapsed_time;
    private int m_Time;
    private frm_yesno form_yes_no;
    private bool m_close_by_time;

    #endregion //Atributes

    #region Constructors

    public frm_waiting_message()
    {
      InitializeComponent();

      form_yes_no = new frm_yesno();
      form_yes_no.Opacity = 0.6;
    }

    #endregion //Constructors

    #region Properties

    public Boolean CloseByTime
    {
      get
      {
        return m_close_by_time;
      }
      set
      {
        m_close_by_time = value;
      }
    }

    #endregion

    #region public methods

    //------------------------------------------------------------------------------
    // PURPOSE : show frm and sets a new displayed time
    //
    //  PARAMS :
    //      - INPUT : Parent, Time
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public void Show(Form Parent, String Tittle, String Text, int Time)
    {
      m_Time = Time;
      this.FormTitle = Tittle;
      lbl_message.Text = Text;
      try
      {
        // ATB 16-FEB-2017
        Misc.WriteLog("[FORM LOAD] frm_waiting_message", Log.Type.Message);

        form_yes_no.Show(Parent);
        this.tm_close.Enabled = true;
        this.tm_close.Start();
        this.ShowDialog(form_yes_no);
        form_yes_no.Hide();
        Parent.Focus();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        form_yes_no.Hide();
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Close current window
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public void CloseAll()
    {
      tm_close.Stop();

      m_Elapsed_time = m_Time;

      Misc.WriteLog("[FORM CLOSE] frm_waiting_message (closeall)", Log.Type.Message);
      this.Close();
      Dispose();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Dispose
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    void IDisposable.Dispose()
    {
      form_yes_no.Dispose();
      form_yes_no = null;

      base.Dispose();
    } // Dispose

    #endregion //public methods

    #region event handling

    //------------------------------------------------------------------------------
    // PURPOSE : close terminal if change stacker is finished
    //
    //  PARAMS :
    //      - INPUT : sender, e
    //
    //      - OUTPUT :
    //
    // RETURNS :
    private void tm_close_Tick(object sender, EventArgs e)
    {
      m_Elapsed_time += tm_close.Interval;
      if (m_Elapsed_time > m_Time && m_close_by_time)
      {
        CloseAll();
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : avoid closing terminal if ALT+F4 is pressed.
    //
    //  PARAMS :
    //      - INPUT : sender, e
    //
    //      - OUTPUT :
    //
    // RETURNS :
    private void Form1_FormClosing(object sender, FormClosingEventArgs e)
    {
      if (m_Elapsed_time < m_Time)
      {
        e.Cancel = true;
      }
    }

    #endregion //event handling

  }
}
