//------------------------------------------------------------------------------
// Copyright � 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_account_user_edit.cs
// 
//   DESCRIPTION: Implements the following classes:
//                1. frm_account_user_edit
//
//        AUTHOR: AJQ
// 
// CREATION DATE: 03-SEP-2007
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 03-SEP-2007 AJQ     First release.
// 16-JUN-2010 MBF     DEPRECATED, use frm_player_edit
//------------------------------------------------------------------------------
//using System;
//using System.Collections.Generic;
//using System.ComponentModel;
//using System.Data;
//using System.Drawing;
//using System.Text;
//using System.Windows.Forms;

//namespace WSI.Cashier
//{  
//  public partial class frm_account_user_edit:Form
//  {
//    #region Attributes

//    string account_id;
//    string initial_user_name;
//    //uc_card parent_form;

//    #endregion

//    #region Constructor

//    public frm_account_user_edit (/*uc_card Parent*/)
//    {
//      InitializeComponent ();

//      InitializeControlResources();

//      //parent_form = Parent;
//      pictureBox1.Image = WSI.Cashier.Images.Get(Images.CashierImage.UserMale);
//    }

//    #endregion

//    #region Private Methods

//    /// <summary>
//    /// Initialize NLS strings
//    /// </summary>
//    private void InitializeControlResources()
//    {
//      // - NLS Strings:
//      //   - Title
//      lbl_account_edit_title.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_TITLE");

//      //   - Labels
//      lbl_account.Text = Resource.String("STR_FORMAT_GENERAL_NAME_ACCOUNT");
//      lbl_mode.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MODE");
//      lbl_name.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_NAME");
//      lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK");

//      //   - Buttons
//      btn_ok.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_OK");
//      btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");
//      btn_mode_to_anonymous.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_BTN_CARD_MODE_TO_ANONYM");
//      btn_mode_to_personal.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_BTN_CARD_MODE_TO_PERSONAL");

//      // - Images:

//    } // InitializeControlResources

//    private void Init()
//    {
//      this.Location = new Point(1024 / 2 - this.Width / 2 , 768 / 2 - this.Height / 2 - 70);

//      this.lbl_account_id_value.Text = this.account_id;

//      if (initial_user_name == "")
//      {
//        // Mode: Anonymous
//        btn_mode_to_anonymous.Enabled = false;
//        btn_mode_to_personal.Enabled = true;

//        txt_name.Enabled = false;
//        txt_name.Text = "";
//        initial_user_name = "";

//        txt_name.Visible = false;
//        lbl_name.Visible = false;

//        btn_mode_to_anonymous.Visible = false;
//        btn_mode_to_personal.Visible = true;

//        pictureBox1.Image = WSI.Cashier.Images.Get(Images.CashierImage.UserAnonymous);

//        lbl_mode_value.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_CARD_MODE_ANONYMOUS");
//      }
//      else
//      {
//        // Mode: Personal
//        btn_mode_to_anonymous.Enabled = true;
//        btn_mode_to_personal.Enabled = false;

//        txt_name.Enabled = true;
//        txt_name.Text = this.initial_user_name;

//        txt_name.Visible = true;
//        lbl_name.Visible = true;

//        // TJG 01-MAR-2010
//        // Don't allow Personal Cards be converted to Anonymous Cards
//        btn_mode_to_anonymous.Visible = false;
//        btn_mode_to_personal.Visible = false;

//        pictureBox1.Image = WSI.Cashier.Images.Get(Images.CashierImage.UserMale);

//        lbl_mode_value.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_CARD_MODE_PERSONAL");
//      }

//      WSIKeyboard.Toggle(); 
//    }

//    private void btn_ok_Click(object sender, EventArgs e)
//    {
//      this.timer1.Enabled = true;
//      String holder_name;
//      int idx;

//      if (this.txt_name.Text.Trim() == "" && btn_mode_to_anonymous.Visible == true)
//      {
//        this.txt_name.Focus();
//        this.lbl_msg_blink.Visible = true;
//      }
//      else
//      {
//        holder_name = this.txt_name.Text;
        
//        for ( idx = 0; idx < 50; idx++)
//        {
//          holder_name = holder_name.Replace("  ", " ");
//          holder_name = holder_name.Replace("..", ".");
//        }
//        holder_name = holder_name.Trim ();
        
//        if (holder_name != this.initial_user_name)
//        {
//          if (CashierBusinessLogic.DB_CheckExistHolderName(ulong.Parse(this.account_id), holder_name))
//          {
//            frm_message.Show(Resource.String("STR_HOLDER_NAME_ALREADY_EXISTS"),
//                             "", 
//                             MessageBoxButtons.OK, Images.CashierImage.Information, Cashier.MainForm);
//            return;
//          }
//          CashierBusinessLogic.DB_UpdateCardholder(this.account_id, holder_name);
//        }

//        WSIKeyboard.Hide();
//        this.Dispose();
//      }
//    }

//    private void btn_cancel_Click(object sender, EventArgs e)
//    {
//      WSIKeyboard.Hide();
//      this.Dispose();
//    }

//    /// <summary>
//    /// Actions for Mode: "To Anonymous"
//    /// </summary>
//    /// <param name="sender"></param>
//    /// <param name="e"></param>
//    private void btn_anonym_Click(object sender, EventArgs e)
//    {
//      txt_name.Text = "";
//      txt_name.Enabled = false;
//      txt_name.Visible = false;

//      btn_mode_to_personal.Enabled = true;
//      btn_mode_to_anonymous.Enabled = false;

//      lbl_name.Visible = false;

//      btn_mode_to_anonymous.Visible = false;
//      btn_mode_to_personal.Visible = true;

//      pictureBox1.Image = WSI.Cashier.Images.Get(Images.CashierImage.UserAnonymous);
//      lbl_mode_value.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_CARD_MODE_ANONYMOUS");
//    }

//    /// <summary>
//    /// Actions for Mode: "To Personal"
//    /// </summary>
//    /// <param name="sender"></param>
//    /// <param name="e"></param>
//    private void btn_personal_Click(object sender, EventArgs e)
//    {
//      btn_mode_to_personal.Enabled = false;
//      btn_mode_to_anonymous.Enabled = true;

//      txt_name.Enabled = true;
//      txt_name.Visible = true;
//      txt_name.Text = this.initial_user_name;
//      lbl_name.Visible = true;

//      btn_mode_to_anonymous.Visible = true;
//      btn_mode_to_personal.Visible = false;

//      pictureBox1.Image = WSI.Cashier.Images.Get(Images.CashierImage.UserMale);

//      lbl_mode_value.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_CARD_MODE_PERSONAL");

//      txt_name.Focus();
//    }

//    private void btn_keyboard_Click(object sender, EventArgs e)
//    {
//      WSIKeyboard.Toggle();            
//    }

//    private void frm_account_user_Shown(object sender, EventArgs e)
//    {
//      this.txt_name.Focus();
//    } // frm_account_user_Shown

//    private void timer1_Tick(object sender, EventArgs e)
//    { 
//      if (lbl_msg_blink.Visible == true)
//      {
//        lbl_msg_blink.Visible = false;
//        timer1.Enabled = false;        
//      }
//    }

//    #endregion

//    #region Public Methods

//    public void GetUserData(string Id, string Name)
//    {
//      account_id = Id;
//      initial_user_name = Name;

//      Init();
//    }

//    public void GetUserData(string Id)
//    {
//      account_id = Id;
//      initial_user_name = "";

//      Init();
//    }


//    #endregion
//  }
//}