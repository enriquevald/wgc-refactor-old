﻿using WSI.Cashier.Controls;

namespace WSI.Cashier
{
    partial class frmCapturePhoto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
      this.previewSuface = new System.Windows.Forms.PictureBox();
      this.btnCancel = new WSI.Cashier.Controls.uc_round_button();
      this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
      this.btn_take_photo = new WSI.Cashier.Controls.uc_round_button();
      this.pnl_data.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.previewSuface)).BeginInit();
      this.tableLayoutPanel1.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.Controls.Add(this.tableLayoutPanel1);
      this.pnl_data.Controls.Add(this.previewSuface);
      this.pnl_data.Size = new System.Drawing.Size(460, 327);
      // 
      // previewSuface
      // 
      this.previewSuface.Dock = System.Windows.Forms.DockStyle.Fill;
      this.previewSuface.Location = new System.Drawing.Point(0, 0);
      this.previewSuface.Name = "previewSuface";
      this.previewSuface.Size = new System.Drawing.Size(460, 327);
      this.previewSuface.TabIndex = 2;
      this.previewSuface.TabStop = false;
      this.previewSuface.MouseDown += new System.Windows.Forms.MouseEventHandler(this.previewSuface_MouseDown);
      // 
      // btnCancel
      // 
      this.btnCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btnCancel.Dock = System.Windows.Forms.DockStyle.Fill;
      this.btnCancel.FlatAppearance.BorderSize = 0;
      this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
      this.btnCancel.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btnCancel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btnCancel.Image = null;
      this.btnCancel.IsSelected = false;
      this.btnCancel.Location = new System.Drawing.Point(230, 0);
      this.btnCancel.Margin = new System.Windows.Forms.Padding(0);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btnCancel.Size = new System.Drawing.Size(230, 55);
      this.btnCancel.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btnCancel.TabIndex = 3;
      this.btnCancel.Text = "STRING \'STR_FORMAT_GENERAL_BUTTONS_CANCEL\' NOT FOUND.";
      this.btnCancel.UseVisualStyleBackColor = false;
      this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
      // 
      // tableLayoutPanel1
      // 
      this.tableLayoutPanel1.ColumnCount = 2;
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.tableLayoutPanel1.Controls.Add(this.btn_take_photo, 0, 0);
      this.tableLayoutPanel1.Controls.Add(this.btnCancel, 1, 0);
      this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 272);
      this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
      this.tableLayoutPanel1.Name = "tableLayoutPanel1";
      this.tableLayoutPanel1.RowCount = 1;
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.tableLayoutPanel1.Size = new System.Drawing.Size(460, 55);
      this.tableLayoutPanel1.TabIndex = 4;
      // 
      // btn_take_photo
      // 
      this.btn_take_photo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_take_photo.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btn_take_photo.Dock = System.Windows.Forms.DockStyle.Fill;
      this.btn_take_photo.FlatAppearance.BorderSize = 0;
      this.btn_take_photo.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
      this.btn_take_photo.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_take_photo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_take_photo.Image = null;
      this.btn_take_photo.IsSelected = false;
      this.btn_take_photo.Location = new System.Drawing.Point(0, 0);
      this.btn_take_photo.Margin = new System.Windows.Forms.Padding(0);
      this.btn_take_photo.Name = "btn_take_photo";
      this.btn_take_photo.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_take_photo.Size = new System.Drawing.Size(230, 55);
      this.btn_take_photo.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_take_photo.TabIndex = 4;
      this.btn_take_photo.Text = "STRING \'STR_FORMAT_GENERAL_BUTTONS_CANCEL\' NOT FOUND.";
      this.btn_take_photo.UseVisualStyleBackColor = false;
      this.btn_take_photo.Click += new System.EventHandler(this.uc_round_button1_Click);
      // 
      // frmCapturePhoto
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(460, 382);
      this.ControlBox = false;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "frmCapturePhoto";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "CapturePhoto";
      this.Deactivate += new System.EventHandler(this.frmCapturePhoto_Deactivate);
      this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmCapturePhoto_FormClosing);
      this.pnl_data.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.previewSuface)).EndInit();
      this.tableLayoutPanel1.ResumeLayout(false);
      this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox previewSuface;
        private uc_round_button btnCancel;
        private uc_round_button btn_take_photo;
        public System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    }
}