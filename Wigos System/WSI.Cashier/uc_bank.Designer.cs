namespace WSI.Cashier
{
  partial class uc_bank
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose (bool disposing)
    {
      if ( disposing && ( components != null ) )
      {
        components.Dispose ();
      }
      base.Dispose (disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent ()
    {
      this.pb_chashier_status = new System.Windows.Forms.PictureBox();
      this.label7 = new System.Windows.Forms.Label();
      this.web_browser = new System.Windows.Forms.WebBrowser();
      this.tab_transfer_test = new System.Windows.Forms.TabControl();
      this.tab_page_transfer_test_1 = new System.Windows.Forms.TabPage();
      this.lbl_transfer_title_1 = new System.Windows.Forms.Label();
      this.txt_target_session_id = new System.Windows.Forms.TextBox();
      this.btn_transfer_send_another = new WSI.Cashier.Controls.uc_round_button();
      this.tab_page_transfer_test_2 = new System.Windows.Forms.TabPage();
      this.lbl_transfer_title_2 = new System.Windows.Forms.Label();
      this.txt_source_session_id = new System.Windows.Forms.TextBox();
      this.btn_transfer_send_to_this = new WSI.Cashier.Controls.uc_round_button();
      this.tab_page_transfer_test_3 = new System.Windows.Forms.TabPage();
      this.lbl_transfer_title_3 = new System.Windows.Forms.Label();
      this.txt_session_transfer_id = new System.Windows.Forms.TextBox();
      this.btn_transfer_receive = new WSI.Cashier.Controls.uc_round_button();
      this.panel_test_transfer = new System.Windows.Forms.Panel();
      this.lbl_rpt_test_transfer = new System.Windows.Forms.Label();
      this.panel8 = new System.Windows.Forms.Panel();
      this.panel2 = new System.Windows.Forms.Panel();
      this.panel3 = new System.Windows.Forms.Panel();
      this.panel4 = new System.Windows.Forms.Panel();
      this.panel5 = new System.Windows.Forms.Panel();
      this.panel6 = new System.Windows.Forms.Panel();
      this.panel7 = new System.Windows.Forms.Panel();
      this.panel1 = new System.Windows.Forms.Panel();
      this.btn_totalizing_strip = new WSI.Cashier.Controls.uc_round_button();
      this.panel11 = new System.Windows.Forms.Panel();
      this.btn_undo_withdrawal = new WSI.Cashier.Controls.uc_round_button();
      this.panel9 = new System.Windows.Forms.Panel();
      this.btn_chips_stock = new WSI.Cashier.Controls.uc_round_button();
      this.btn_change_chips = new WSI.Cashier.Controls.uc_round_button();
      this.btn_status = new WSI.Cashier.Controls.uc_round_button();
      this.btn_history = new WSI.Cashier.Controls.uc_round_button();
      this.btn_withdraw = new WSI.Cashier.Controls.uc_round_button();
      this.btn_deposit = new WSI.Cashier.Controls.uc_round_button();
      this.btn_open = new WSI.Cashier.Controls.uc_round_button();
      this.btn_close = new WSI.Cashier.Controls.uc_round_button();
      this.btn_test_transfer = new WSI.Cashier.Controls.uc_round_button();
      this.btn_show_balance_info = new WSI.Cashier.Controls.uc_round_button();
      this.lbl_cash_desk_name = new WSI.Cashier.Controls.uc_label();
      this.lbl_cash_desk_status = new WSI.Cashier.Controls.uc_label();
      ((System.ComponentModel.ISupportInitialize)(this.pb_chashier_status)).BeginInit();
      this.tab_transfer_test.SuspendLayout();
      this.tab_page_transfer_test_1.SuspendLayout();
      this.tab_page_transfer_test_2.SuspendLayout();
      this.tab_page_transfer_test_3.SuspendLayout();
      this.panel_test_transfer.SuspendLayout();
      this.panel1.SuspendLayout();
      this.SuspendLayout();
      // 
      // pb_chashier_status
      // 
      this.pb_chashier_status.Location = new System.Drawing.Point(10, 7);
      this.pb_chashier_status.Name = "pb_chashier_status";
      this.pb_chashier_status.Size = new System.Drawing.Size(50, 50);
      this.pb_chashier_status.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
      this.pb_chashier_status.TabIndex = 58;
      this.pb_chashier_status.TabStop = false;
      // 
      // label7
      // 
      this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.label7.BackColor = System.Drawing.Color.Black;
      this.label7.Location = new System.Drawing.Point(-3, 60);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(885, 1);
      this.label7.TabIndex = 3;
      // 
      // web_browser
      // 
      this.web_browser.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.web_browser.Location = new System.Drawing.Point(10, 62);
      this.web_browser.MinimumSize = new System.Drawing.Size(20, 20);
      this.web_browser.Name = "web_browser";
      this.web_browser.ScrollBarsEnabled = false;
      this.web_browser.Size = new System.Drawing.Size(733, 655);
      this.web_browser.TabIndex = 4;
      // 
      // tab_transfer_test
      // 
      this.tab_transfer_test.Controls.Add(this.tab_page_transfer_test_1);
      this.tab_transfer_test.Controls.Add(this.tab_page_transfer_test_2);
      this.tab_transfer_test.Controls.Add(this.tab_page_transfer_test_3);
      this.tab_transfer_test.Location = new System.Drawing.Point(2, 2);
      this.tab_transfer_test.Name = "tab_transfer_test";
      this.tab_transfer_test.SelectedIndex = 0;
      this.tab_transfer_test.Size = new System.Drawing.Size(345, 85);
      this.tab_transfer_test.TabIndex = 189;
      // 
      // tab_page_transfer_test_1
      // 
      this.tab_page_transfer_test_1.Controls.Add(this.lbl_transfer_title_1);
      this.tab_page_transfer_test_1.Controls.Add(this.txt_target_session_id);
      this.tab_page_transfer_test_1.Controls.Add(this.btn_transfer_send_another);
      this.tab_page_transfer_test_1.Location = new System.Drawing.Point(4, 22);
      this.tab_page_transfer_test_1.Name = "tab_page_transfer_test_1";
      this.tab_page_transfer_test_1.Padding = new System.Windows.Forms.Padding(3);
      this.tab_page_transfer_test_1.Size = new System.Drawing.Size(337, 59);
      this.tab_page_transfer_test_1.TabIndex = 0;
      this.tab_page_transfer_test_1.Text = "Enviar a otro cajero";
      // 
      // lbl_transfer_title_1
      // 
      this.lbl_transfer_title_1.AutoSize = true;
      this.lbl_transfer_title_1.Location = new System.Drawing.Point(12, 10);
      this.lbl_transfer_title_1.Name = "lbl_transfer_title_1";
      this.lbl_transfer_title_1.Size = new System.Drawing.Size(130, 13);
      this.lbl_transfer_title_1.TabIndex = 0;
      this.lbl_transfer_title_1.Text = "Target cashier session ID:";
      // 
      // txt_target_session_id
      // 
      this.txt_target_session_id.Location = new System.Drawing.Point(15, 26);
      this.txt_target_session_id.Name = "txt_target_session_id";
      this.txt_target_session_id.Size = new System.Drawing.Size(138, 20);
      this.txt_target_session_id.TabIndex = 1;
      this.txt_target_session_id.Text = "6490";
      // 
      // btn_transfer_send_another
      // 
      this.btn_transfer_send_another.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_transfer_send_another.FlatAppearance.BorderSize = 0;
      this.btn_transfer_send_another.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_transfer_send_another.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_transfer_send_another.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_transfer_send_another.Image = null;
      this.btn_transfer_send_another.IsSelected = false;
      this.btn_transfer_send_another.Location = new System.Drawing.Point(231, 17);
      this.btn_transfer_send_another.Name = "btn_transfer_send_another";
      this.btn_transfer_send_another.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_transfer_send_another.Size = new System.Drawing.Size(96, 33);
      this.btn_transfer_send_another.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_transfer_send_another.TabIndex = 2;
      this.btn_transfer_send_another.Text = "ENVIAR";
      this.btn_transfer_send_another.UseVisualStyleBackColor = false;
      // 
      // tab_page_transfer_test_2
      // 
      this.tab_page_transfer_test_2.Controls.Add(this.lbl_transfer_title_2);
      this.tab_page_transfer_test_2.Controls.Add(this.txt_source_session_id);
      this.tab_page_transfer_test_2.Controls.Add(this.btn_transfer_send_to_this);
      this.tab_page_transfer_test_2.Location = new System.Drawing.Point(4, 22);
      this.tab_page_transfer_test_2.Name = "tab_page_transfer_test_2";
      this.tab_page_transfer_test_2.Padding = new System.Windows.Forms.Padding(3);
      this.tab_page_transfer_test_2.Size = new System.Drawing.Size(337, 59);
      this.tab_page_transfer_test_2.TabIndex = 1;
      this.tab_page_transfer_test_2.Text = "Enviar a este cajero";
      // 
      // lbl_transfer_title_2
      // 
      this.lbl_transfer_title_2.AutoSize = true;
      this.lbl_transfer_title_2.Location = new System.Drawing.Point(12, 10);
      this.lbl_transfer_title_2.Name = "lbl_transfer_title_2";
      this.lbl_transfer_title_2.Size = new System.Drawing.Size(130, 13);
      this.lbl_transfer_title_2.TabIndex = 189;
      this.lbl_transfer_title_2.Text = "Target cashier session ID:";
      // 
      // txt_source_session_id
      // 
      this.txt_source_session_id.Location = new System.Drawing.Point(15, 26);
      this.txt_source_session_id.Name = "txt_source_session_id";
      this.txt_source_session_id.Size = new System.Drawing.Size(138, 20);
      this.txt_source_session_id.TabIndex = 188;
      this.txt_source_session_id.Text = "6490";
      // 
      // btn_transfer_send_to_this
      // 
      this.btn_transfer_send_to_this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_transfer_send_to_this.FlatAppearance.BorderSize = 0;
      this.btn_transfer_send_to_this.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_transfer_send_to_this.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_transfer_send_to_this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_transfer_send_to_this.Image = null;
      this.btn_transfer_send_to_this.IsSelected = false;
      this.btn_transfer_send_to_this.Location = new System.Drawing.Point(231, 17);
      this.btn_transfer_send_to_this.Name = "btn_transfer_send_to_this";
      this.btn_transfer_send_to_this.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_transfer_send_to_this.Size = new System.Drawing.Size(96, 33);
      this.btn_transfer_send_to_this.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_transfer_send_to_this.TabIndex = 186;
      this.btn_transfer_send_to_this.Text = "ENVIAR";
      this.btn_transfer_send_to_this.UseVisualStyleBackColor = false;
      // 
      // tab_page_transfer_test_3
      // 
      this.tab_page_transfer_test_3.Controls.Add(this.lbl_transfer_title_3);
      this.tab_page_transfer_test_3.Controls.Add(this.txt_session_transfer_id);
      this.tab_page_transfer_test_3.Controls.Add(this.btn_transfer_receive);
      this.tab_page_transfer_test_3.Location = new System.Drawing.Point(4, 22);
      this.tab_page_transfer_test_3.Name = "tab_page_transfer_test_3";
      this.tab_page_transfer_test_3.Size = new System.Drawing.Size(337, 59);
      this.tab_page_transfer_test_3.TabIndex = 2;
      this.tab_page_transfer_test_3.Text = "Recepcion a este cajero";
      // 
      // lbl_transfer_title_3
      // 
      this.lbl_transfer_title_3.AutoSize = true;
      this.lbl_transfer_title_3.Location = new System.Drawing.Point(8, 12);
      this.lbl_transfer_title_3.Name = "lbl_transfer_title_3";
      this.lbl_transfer_title_3.Size = new System.Drawing.Size(119, 13);
      this.lbl_transfer_title_3.TabIndex = 191;
      this.lbl_transfer_title_3.Text = "Session transfer info ID:";
      // 
      // txt_session_transfer_id
      // 
      this.txt_session_transfer_id.Location = new System.Drawing.Point(11, 28);
      this.txt_session_transfer_id.Name = "txt_session_transfer_id";
      this.txt_session_transfer_id.Size = new System.Drawing.Size(138, 20);
      this.txt_session_transfer_id.TabIndex = 190;
      // 
      // btn_transfer_receive
      // 
      this.btn_transfer_receive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_transfer_receive.FlatAppearance.BorderSize = 0;
      this.btn_transfer_receive.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_transfer_receive.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_transfer_receive.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_transfer_receive.Image = null;
      this.btn_transfer_receive.IsSelected = false;
      this.btn_transfer_receive.Location = new System.Drawing.Point(231, 18);
      this.btn_transfer_receive.Name = "btn_transfer_receive";
      this.btn_transfer_receive.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_transfer_receive.Size = new System.Drawing.Size(96, 32);
      this.btn_transfer_receive.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_transfer_receive.TabIndex = 186;
      this.btn_transfer_receive.Text = "RECIBIR";
      this.btn_transfer_receive.UseVisualStyleBackColor = false;
      // 
      // panel_test_transfer
      // 
      this.panel_test_transfer.Controls.Add(this.lbl_rpt_test_transfer);
      this.panel_test_transfer.Controls.Add(this.tab_transfer_test);
      this.panel_test_transfer.Location = new System.Drawing.Point(28, 562);
      this.panel_test_transfer.Name = "panel_test_transfer";
      this.panel_test_transfer.Size = new System.Drawing.Size(350, 120);
      this.panel_test_transfer.TabIndex = 6;
      this.panel_test_transfer.Visible = false;
      // 
      // lbl_rpt_test_transfer
      // 
      this.lbl_rpt_test_transfer.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_rpt_test_transfer.Location = new System.Drawing.Point(6, 72);
      this.lbl_rpt_test_transfer.Name = "lbl_rpt_test_transfer";
      this.lbl_rpt_test_transfer.Size = new System.Drawing.Size(338, 26);
      this.lbl_rpt_test_transfer.TabIndex = 0;
      // 
      // panel8
      // 
      this.panel8.Dock = System.Windows.Forms.DockStyle.Top;
      this.panel8.Location = new System.Drawing.Point(0, 60);
      this.panel8.Name = "panel8";
      this.panel8.Size = new System.Drawing.Size(122, 5);
      this.panel8.TabIndex = 1;
      // 
      // panel2
      // 
      this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
      this.panel2.Location = new System.Drawing.Point(0, 123);
      this.panel2.Name = "panel2";
      this.panel2.Size = new System.Drawing.Size(122, 5);
      this.panel2.TabIndex = 3;
      // 
      // panel3
      // 
      this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
      this.panel3.Location = new System.Drawing.Point(0, 186);
      this.panel3.Name = "panel3";
      this.panel3.Size = new System.Drawing.Size(122, 5);
      this.panel3.TabIndex = 5;
      // 
      // panel4
      // 
      this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
      this.panel4.Location = new System.Drawing.Point(0, 249);
      this.panel4.Name = "panel4";
      this.panel4.Size = new System.Drawing.Size(122, 5);
      this.panel4.TabIndex = 7;
      // 
      // panel5
      // 
      this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
      this.panel5.Location = new System.Drawing.Point(0, 312);
      this.panel5.Name = "panel5";
      this.panel5.Size = new System.Drawing.Size(122, 5);
      this.panel5.TabIndex = 9;
      // 
      // panel6
      // 
      this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
      this.panel6.Location = new System.Drawing.Point(0, 375);
      this.panel6.Name = "panel6";
      this.panel6.Size = new System.Drawing.Size(122, 5);
      this.panel6.TabIndex = 11;
      // 
      // panel7
      // 
      this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
      this.panel7.Location = new System.Drawing.Point(0, 438);
      this.panel7.Name = "panel7";
      this.panel7.Size = new System.Drawing.Size(122, 5);
      this.panel7.TabIndex = 13;
      // 
      // panel1
      // 
      this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.panel1.AutoScroll = true;
      this.panel1.Controls.Add(this.btn_totalizing_strip);
      this.panel1.Controls.Add(this.panel11);
      this.panel1.Controls.Add(this.btn_undo_withdrawal);
      this.panel1.Controls.Add(this.panel9);
      this.panel1.Controls.Add(this.btn_chips_stock);
      this.panel1.Controls.Add(this.panel7);
      this.panel1.Controls.Add(this.btn_change_chips);
      this.panel1.Controls.Add(this.panel6);
      this.panel1.Controls.Add(this.btn_status);
      this.panel1.Controls.Add(this.panel5);
      this.panel1.Controls.Add(this.btn_history);
      this.panel1.Controls.Add(this.panel4);
      this.panel1.Controls.Add(this.btn_withdraw);
      this.panel1.Controls.Add(this.panel3);
      this.panel1.Controls.Add(this.btn_deposit);
      this.panel1.Controls.Add(this.panel2);
      this.panel1.Controls.Add(this.btn_open);
      this.panel1.Controls.Add(this.panel8);
      this.panel1.Controls.Add(this.btn_close);
      this.panel1.Location = new System.Drawing.Point(754, 73);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(122, 630);
      this.panel1.TabIndex = 61;
      // 
      // btn_totalizing_strip
      // 
      this.btn_totalizing_strip.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_totalizing_strip.Dock = System.Windows.Forms.DockStyle.Top;
      this.btn_totalizing_strip.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(114)))), ((int)(((byte)(121)))));
      this.btn_totalizing_strip.FlatAppearance.BorderSize = 0;
      this.btn_totalizing_strip.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_totalizing_strip.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_totalizing_strip.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_totalizing_strip.Image = null;
      this.btn_totalizing_strip.IsSelected = false;
      this.btn_totalizing_strip.Location = new System.Drawing.Point(0, 569);
      this.btn_totalizing_strip.Margin = new System.Windows.Forms.Padding(0);
      this.btn_totalizing_strip.Name = "btn_totalizing_strip";
      this.btn_totalizing_strip.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_totalizing_strip.Size = new System.Drawing.Size(122, 58);
      this.btn_totalizing_strip.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_totalizing_strip.TabIndex = 18;
      this.btn_totalizing_strip.Text = "XTOTALIZING STRIP";
      this.btn_totalizing_strip.UseVisualStyleBackColor = false;
      this.btn_totalizing_strip.Click += new System.EventHandler(this.btn_totalizing_strip_Click);
      // 
      // panel11
      // 
      this.panel11.Dock = System.Windows.Forms.DockStyle.Top;
      this.panel11.Location = new System.Drawing.Point(0, 564);
      this.panel11.Name = "panel11";
      this.panel11.Size = new System.Drawing.Size(122, 5);
      this.panel11.TabIndex = 17;
      // 
      // btn_undo_withdrawal
      // 
      this.btn_undo_withdrawal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_undo_withdrawal.Dock = System.Windows.Forms.DockStyle.Top;
      this.btn_undo_withdrawal.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(114)))), ((int)(((byte)(121)))));
      this.btn_undo_withdrawal.FlatAppearance.BorderSize = 0;
      this.btn_undo_withdrawal.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_undo_withdrawal.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_undo_withdrawal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_undo_withdrawal.Image = null;
      this.btn_undo_withdrawal.IsSelected = false;
      this.btn_undo_withdrawal.Location = new System.Drawing.Point(0, 506);
      this.btn_undo_withdrawal.Margin = new System.Windows.Forms.Padding(0);
      this.btn_undo_withdrawal.Name = "btn_undo_withdrawal";
      this.btn_undo_withdrawal.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_undo_withdrawal.Size = new System.Drawing.Size(122, 58);
      this.btn_undo_withdrawal.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_undo_withdrawal.TabIndex = 15;
      this.btn_undo_withdrawal.Text = "XUNDO RETIREMENT";
      this.btn_undo_withdrawal.UseVisualStyleBackColor = false;
      this.btn_undo_withdrawal.Click += new System.EventHandler(this.btn_undo_filler_out_Click);
      // 
      // panel9
      // 
      this.panel9.Dock = System.Windows.Forms.DockStyle.Top;
      this.panel9.Location = new System.Drawing.Point(0, 501);
      this.panel9.Name = "panel9";
      this.panel9.Size = new System.Drawing.Size(122, 5);
      this.panel9.TabIndex = 15;
      // 
      // btn_chips_stock
      // 
      this.btn_chips_stock.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_chips_stock.Dock = System.Windows.Forms.DockStyle.Top;
      this.btn_chips_stock.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(114)))), ((int)(((byte)(121)))));
      this.btn_chips_stock.FlatAppearance.BorderSize = 0;
      this.btn_chips_stock.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_chips_stock.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_chips_stock.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_chips_stock.Image = null;
      this.btn_chips_stock.IsSelected = false;
      this.btn_chips_stock.Location = new System.Drawing.Point(0, 443);
      this.btn_chips_stock.Margin = new System.Windows.Forms.Padding(0);
      this.btn_chips_stock.Name = "btn_chips_stock";
      this.btn_chips_stock.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_chips_stock.Size = new System.Drawing.Size(122, 58);
      this.btn_chips_stock.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_chips_stock.TabIndex = 14;
      this.btn_chips_stock.Text = "XSTOCK";
      this.btn_chips_stock.UseVisualStyleBackColor = false;
      this.btn_chips_stock.Click += new System.EventHandler(this.btn_chips_stock_Click);
      // 
      // btn_change_chips
      // 
      this.btn_change_chips.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_change_chips.Dock = System.Windows.Forms.DockStyle.Top;
      this.btn_change_chips.FlatAppearance.BorderSize = 0;
      this.btn_change_chips.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_change_chips.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_change_chips.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_change_chips.Image = null;
      this.btn_change_chips.IsSelected = false;
      this.btn_change_chips.Location = new System.Drawing.Point(0, 380);
      this.btn_change_chips.Name = "btn_change_chips";
      this.btn_change_chips.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_change_chips.Size = new System.Drawing.Size(122, 58);
      this.btn_change_chips.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_change_chips.TabIndex = 12;
      this.btn_change_chips.Text = "XCHANGECHIPS";
      this.btn_change_chips.UseVisualStyleBackColor = false;
      this.btn_change_chips.Click += new System.EventHandler(this.btn_change_chips_Click);
      // 
      // btn_status
      // 
      this.btn_status.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_status.Dock = System.Windows.Forms.DockStyle.Top;
      this.btn_status.FlatAppearance.BorderSize = 0;
      this.btn_status.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_status.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_status.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_status.Image = null;
      this.btn_status.IsSelected = false;
      this.btn_status.Location = new System.Drawing.Point(0, 317);
      this.btn_status.Name = "btn_status";
      this.btn_status.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_status.Size = new System.Drawing.Size(122, 58);
      this.btn_status.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_status.TabIndex = 10;
      this.btn_status.Text = "XCORTEDECAJA";
      this.btn_status.UseVisualStyleBackColor = false;
      this.btn_status.Click += new System.EventHandler(this.btn_status_Click);
      // 
      // btn_history
      // 
      this.btn_history.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_history.Dock = System.Windows.Forms.DockStyle.Top;
      this.btn_history.FlatAppearance.BorderSize = 0;
      this.btn_history.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_history.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_history.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_history.Image = null;
      this.btn_history.IsSelected = false;
      this.btn_history.Location = new System.Drawing.Point(0, 254);
      this.btn_history.Name = "btn_history";
      this.btn_history.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_history.Size = new System.Drawing.Size(122, 58);
      this.btn_history.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_history.TabIndex = 8;
      this.btn_history.Text = "XCASH FLOW MOVEMENTS";
      this.btn_history.UseVisualStyleBackColor = false;
      this.btn_history.Click += new System.EventHandler(this.btn_history_Click);
      // 
      // btn_withdraw
      // 
      this.btn_withdraw.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_withdraw.Dock = System.Windows.Forms.DockStyle.Top;
      this.btn_withdraw.FlatAppearance.BorderSize = 0;
      this.btn_withdraw.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_withdraw.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_withdraw.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_withdraw.Image = null;
      this.btn_withdraw.IsSelected = false;
      this.btn_withdraw.Location = new System.Drawing.Point(0, 191);
      this.btn_withdraw.Name = "btn_withdraw";
      this.btn_withdraw.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_withdraw.Size = new System.Drawing.Size(122, 58);
      this.btn_withdraw.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_withdraw.TabIndex = 6;
      this.btn_withdraw.Text = "XWITHDRAW";
      this.btn_withdraw.UseVisualStyleBackColor = false;
      this.btn_withdraw.Click += new System.EventHandler(this.btn_filler_out_Click);
      // 
      // btn_deposit
      // 
      this.btn_deposit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_deposit.Dock = System.Windows.Forms.DockStyle.Top;
      this.btn_deposit.FlatAppearance.BorderSize = 0;
      this.btn_deposit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_deposit.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_deposit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_deposit.Image = null;
      this.btn_deposit.IsSelected = false;
      this.btn_deposit.Location = new System.Drawing.Point(0, 128);
      this.btn_deposit.Name = "btn_deposit";
      this.btn_deposit.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_deposit.Size = new System.Drawing.Size(122, 58);
      this.btn_deposit.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_deposit.TabIndex = 4;
      this.btn_deposit.Text = "XDEPOSIT";
      this.btn_deposit.UseVisualStyleBackColor = false;
      this.btn_deposit.Click += new System.EventHandler(this.btn_filler_in_Click);
      // 
      // btn_open
      // 
      this.btn_open.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_open.Dock = System.Windows.Forms.DockStyle.Top;
      this.btn_open.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(114)))), ((int)(((byte)(121)))));
      this.btn_open.FlatAppearance.BorderSize = 0;
      this.btn_open.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_open.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_open.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_open.Image = null;
      this.btn_open.IsSelected = false;
      this.btn_open.Location = new System.Drawing.Point(0, 65);
      this.btn_open.Margin = new System.Windows.Forms.Padding(0);
      this.btn_open.Name = "btn_open";
      this.btn_open.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_open.Size = new System.Drawing.Size(122, 58);
      this.btn_open.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_open.TabIndex = 2;
      this.btn_open.Text = "XOPEN";
      this.btn_open.UseVisualStyleBackColor = false;
      this.btn_open.Click += new System.EventHandler(this.btn_open_Click);
      // 
      // btn_close
      // 
      this.btn_close.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_close.Dock = System.Windows.Forms.DockStyle.Top;
      this.btn_close.FlatAppearance.BorderSize = 0;
      this.btn_close.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_close.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_close.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_close.Image = null;
      this.btn_close.IsSelected = false;
      this.btn_close.Location = new System.Drawing.Point(0, 0);
      this.btn_close.Name = "btn_close";
      this.btn_close.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_close.Size = new System.Drawing.Size(122, 60);
      this.btn_close.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_close.TabIndex = 0;
      this.btn_close.Text = "XCLOSE";
      this.btn_close.UseVisualStyleBackColor = false;
      this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
      // 
      // btn_test_transfer
      // 
      this.btn_test_transfer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_test_transfer.FlatAppearance.BorderSize = 0;
      this.btn_test_transfer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_test_transfer.Font = new System.Drawing.Font("Open Sans Semibold", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_test_transfer.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_test_transfer.Image = null;
      this.btn_test_transfer.IsSelected = false;
      this.btn_test_transfer.Location = new System.Drawing.Point(532, 10);
      this.btn_test_transfer.Name = "btn_test_transfer";
      this.btn_test_transfer.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_test_transfer.Size = new System.Drawing.Size(101, 33);
      this.btn_test_transfer.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_test_transfer.TabIndex = 1;
      this.btn_test_transfer.Text = "TEST TRANSFER";
      this.btn_test_transfer.UseVisualStyleBackColor = false;
      this.btn_test_transfer.Visible = false;
      // 
      // btn_show_balance_info
      // 
      this.btn_show_balance_info.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_show_balance_info.FlatAppearance.BorderSize = 0;
      this.btn_show_balance_info.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_show_balance_info.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_show_balance_info.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_show_balance_info.Image = null;
      this.btn_show_balance_info.IsSelected = false;
      this.btn_show_balance_info.Location = new System.Drawing.Point(23, 124);
      this.btn_show_balance_info.Name = "btn_show_balance_info";
      this.btn_show_balance_info.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_show_balance_info.Size = new System.Drawing.Size(122, 68);
      this.btn_show_balance_info.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_show_balance_info.TabIndex = 5;
      this.btn_show_balance_info.Text = "XSHOWBALANCE";
      this.btn_show_balance_info.UseVisualStyleBackColor = false;
      this.btn_show_balance_info.Click += new System.EventHandler(this.btn_show_balance_info_Click);
      // 
      // lbl_cash_desk_name
      // 
      this.lbl_cash_desk_name.BackColor = System.Drawing.Color.Transparent;
      this.lbl_cash_desk_name.Font = new System.Drawing.Font("Open Sans", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_cash_desk_name.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_cash_desk_name.Location = new System.Drawing.Point(698, 12);
      this.lbl_cash_desk_name.Name = "lbl_cash_desk_name";
      this.lbl_cash_desk_name.Size = new System.Drawing.Size(129, 35);
      this.lbl_cash_desk_name.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK_BOLD;
      this.lbl_cash_desk_name.TabIndex = 2;
      this.lbl_cash_desk_name.Text = "xCash Desk Namex";
      // 
      // lbl_cash_desk_status
      // 
      this.lbl_cash_desk_status.BackColor = System.Drawing.Color.Transparent;
      this.lbl_cash_desk_status.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_cash_desk_status.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.lbl_cash_desk_status.Location = new System.Drawing.Point(67, 12);
      this.lbl_cash_desk_status.Name = "lbl_cash_desk_status";
      this.lbl_cash_desk_status.Size = new System.Drawing.Size(70, 40);
      this.lbl_cash_desk_status.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_RED;
      this.lbl_cash_desk_status.TabIndex = 0;
      this.lbl_cash_desk_status.Text = "XCASH DESK STATUSX";
      // 
      // uc_bank
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.panel_test_transfer);
      this.Controls.Add(this.btn_test_transfer);
      this.Controls.Add(this.btn_show_balance_info);
      this.Controls.Add(this.web_browser);
      this.Controls.Add(this.panel1);
      this.Controls.Add(this.lbl_cash_desk_name);
      this.Controls.Add(this.pb_chashier_status);
      this.Controls.Add(this.lbl_cash_desk_status);
      this.Controls.Add(this.label7);
      this.Name = "uc_bank";
      this.Size = new System.Drawing.Size(884, 717);
      ((System.ComponentModel.ISupportInitialize)(this.pb_chashier_status)).EndInit();
      this.tab_transfer_test.ResumeLayout(false);
      this.tab_page_transfer_test_1.ResumeLayout(false);
      this.tab_page_transfer_test_1.PerformLayout();
      this.tab_page_transfer_test_2.ResumeLayout(false);
      this.tab_page_transfer_test_2.PerformLayout();
      this.tab_page_transfer_test_3.ResumeLayout(false);
      this.tab_page_transfer_test_3.PerformLayout();
      this.panel_test_transfer.ResumeLayout(false);
      this.panel1.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private Controls.uc_label lbl_cash_desk_name;
    public System.Windows.Forms.PictureBox pb_chashier_status;
    private Controls.uc_label lbl_cash_desk_status;
    private System.Windows.Forms.Label label7;
    public System.Windows.Forms.WebBrowser web_browser;
    private Controls.uc_round_button btn_show_balance_info;
    private System.Windows.Forms.TabControl tab_transfer_test;
    private System.Windows.Forms.TabPage tab_page_transfer_test_1;
    private System.Windows.Forms.Label lbl_transfer_title_1;
    private System.Windows.Forms.TextBox txt_target_session_id;
    private Controls.uc_round_button btn_transfer_send_another;
    private System.Windows.Forms.TabPage tab_page_transfer_test_2;
    private System.Windows.Forms.Label lbl_transfer_title_2;
    private System.Windows.Forms.TextBox txt_source_session_id;
    private Controls.uc_round_button btn_transfer_send_to_this;
    private System.Windows.Forms.TabPage tab_page_transfer_test_3;
    private System.Windows.Forms.Label lbl_transfer_title_3;
    private System.Windows.Forms.TextBox txt_session_transfer_id;
    private Controls.uc_round_button btn_transfer_receive;
    private Controls.uc_round_button btn_test_transfer;
    private System.Windows.Forms.Panel panel_test_transfer;
    private System.Windows.Forms.Label lbl_rpt_test_transfer;
    private Controls.uc_round_button btn_close;
    private System.Windows.Forms.Panel panel8;
    private Controls.uc_round_button btn_open;
    private System.Windows.Forms.Panel panel2;
    private Controls.uc_round_button btn_deposit;
    private System.Windows.Forms.Panel panel3;
    private Controls.uc_round_button btn_withdraw;
    private System.Windows.Forms.Panel panel4;
    private Controls.uc_round_button btn_history;
    private System.Windows.Forms.Panel panel5;
    private Controls.uc_round_button btn_status;
    private System.Windows.Forms.Panel panel6;
    private Controls.uc_round_button btn_change_chips;
    private System.Windows.Forms.Panel panel7;    
    private System.Windows.Forms.Panel panel9;
    private Controls.uc_round_button btn_chips_stock;
    private Controls.uc_round_button btn_undo_withdrawal;
    private System.Windows.Forms.Panel panel1;    
    private Controls.uc_round_button btn_totalizing_strip;
    private System.Windows.Forms.Panel panel11;


  }
}
