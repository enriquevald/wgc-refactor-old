﻿//------------------------------------------------------------------------------
// Copyright © 2015-2020 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_currency_selector.cs
// 
//   DESCRIPTION: Implements the uc_currency_selector user control
//                Class: uc_currency_selector
//
//        AUTHOR: Jordi Masachs
// 
// CREATION DATE: 04-MAY-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 04-MAY-2016 JMV     First release.
// 24-MAY-2016 ETP     PBI 12652: Forced to show currency selection on shown event.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using WSI.Common;

namespace WSI.Cashier
{

  public partial class uc_currency_selector : UserControl
  {

    #region Atributes
    private CurrencyExchange m_national_currency = null;
    private List<CurrencyExchange> m_currencies = null;
    private CurrencyExchange m_currency_exchange = null;
    private bool m_show_national_currency = false;
    #endregion

    #region Properties

    public bool ShowNationalCurrency
    {
      get { return m_show_national_currency; }
      set { m_show_national_currency = value; }
    }

    public CurrencyExchange GetSelectedCurrency
    {
      get { return m_currency_exchange; }
    }

    public CurrencyExchange GetNationalCurrency
    {
      get { return m_national_currency; }
    }
    #endregion

   

    #region Public Functions 

    public void OpenCurrencySelector()
    {

      Int32 _index;
      Int32 _count_currencies = 0;
      VoucherTypes _voucher_type;

      CurrencyExchange _currency_returned;

      _voucher_type = VoucherTypes.AmountRequest;

      _count_currencies = m_currencies.Count;

      if (_count_currencies > 3)
      {
        frm_browse_currencies frm = new frm_browse_currencies();
        frm.Show(m_currencies, out _currency_returned, _voucher_type);
        if (_currency_returned != null)
        {
          m_currency_exchange = _currency_returned;
        }
      }
      else
      {
        _index = m_currencies.IndexOf(m_currency_exchange);
        _index = (_index + 1) % m_currencies.Count;

        m_currency_exchange = m_currencies[_index];
      }
      SetCurrency();

    }

    public uc_currency_selector()
    {
      InitializeComponent();      
    }

    public void SetSelectedCurrency(string iso_code)
    {
      bool _exists;

      _exists = false;
      if (m_currencies != null)
      {
        foreach (CurrencyExchange cur in m_currencies)
        {
          if (cur.CurrencyCode == iso_code)
          {
            _exists = true;
            m_currency_exchange = cur;
            break;
          }
        }
      }
      if (_exists)
      {
        SetCurrency();
      }
    }
    
    #endregion

    #region Public Events

    public event EventHandler CurrencyChanged;

    #endregion

    #region private functions

    private void Init()
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[UC LOAD] uc_currency_selector", Log.Type.Message);

      List<CurrencyExchangeType> _allowed_types;
      List<CurrencyExchange> _currencies_aux;
      bool _first_currency = true;     

      _allowed_types = new List<CurrencyExchangeType>();
      _allowed_types.Add(CurrencyExchangeType.CURRENCY);

      uc_round_panel.HeaderText = Resource.String("STR_VOUCHER_CAGESESSIONREPORT_CURRENCY");

      CurrencyExchange.GetAllowedCurrencies(true, _allowed_types, false, false, out m_national_currency, out _currencies_aux);

      if (!m_show_national_currency)
      {
        m_currencies = new List<CurrencyExchange>();
        if (_currencies_aux != null)
        {
          foreach (CurrencyExchange cur in _currencies_aux)
          {
            if (cur != m_national_currency)
            {
              m_currencies.Add(cur);
              if (_first_currency)
              {
                m_currency_exchange = cur;
                _first_currency = false;
              }
            }
          }
        }
      }
      else
      {
        m_currencies = _currencies_aux;
        m_currency_exchange = m_national_currency;
      }            
      SetCurrency();

      btn_selected_currency.Text = Resource.String("STR_FRM_AMOUNT_CHANGE_BUTTON");
    }

    private void SetCurrency()
    {
      CurrencyChangedArgs _args;
      if (m_currency_exchange != null)
      {
        lbl_currency_iso_code.Text = m_currency_exchange.CurrencyCode;
        lbl_paymet_method.Text = m_currency_exchange.Description;
        SetCurrencyImage();
        _args = new CurrencyChangedArgs(m_currency_exchange);
        if (this.CurrencyChanged != null)
        {
          this.CurrencyChanged(new object(), _args);
        }
        if (m_currencies.Count < 2)
        {
          btn_selected_currency.Enabled = false;
        }

      }
    }

    private void SetCurrencyImage()
    {
      CurrencyExchangeProperties _properties;

      _properties = CurrencyExchangeProperties.GetProperties(m_currency_exchange.CurrencyCode);
      if (_properties != null)
      {
        pb_currency.Image = _properties.GetIcon(m_currency_exchange.Type, pb_currency.Size.Width, pb_currency.Size.Height, false);
      }
      else
      {
        pb_currency.Image = null;
      }
    }

    #endregion

    #region private events

    private void btn_selected_currency_Click(object sender, EventArgs e)
    {
      OpenCurrencySelector();
  
    }

  

    //------------------------------------------------------------------------------
    // PURPOSE : Put the flag corresponding with Iso Code
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
  

    private void uc_currency_selector_Load(object sender, EventArgs e)
    {
      Init();
    }

    #endregion
  }

  public class CurrencyChangedArgs : EventArgs
  {
    private CurrencyExchange m_currency;

    public CurrencyChangedArgs(CurrencyExchange currency)
    {
      this.m_currency = currency;
    }

    public CurrencyExchange NewCurrency
    {
      get
      {
        return m_currency;
      }
    }
  }
}
