﻿namespace WSI.Cashier
{
  partial class frm_user_login
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_user_login));
      this.pnl_permission_list = new System.Windows.Forms.Panel();
      this.txt_permission_list = new WSI.Cashier.Controls.uc_round_textbox();
      this.btn_login_cancel = new WSI.Cashier.Controls.uc_round_button();
      this.btn_shutdown = new WSI.Cashier.Controls.uc_round_button();
      this.btn_keyboard = new WSI.Cashier.Controls.uc_round_button();
      this.btn_user_login = new WSI.Cashier.Controls.uc_round_button();
      this.pb_login_icon = new System.Windows.Forms.PictureBox();
      this.txt_user_password = new WSI.Cashier.Controls.uc_round_textbox();
      this.txt_user_name = new WSI.Cashier.Controls.uc_round_textbox();
      this.lbl_user_password = new WSI.Cashier.Controls.uc_label();
      this.lbl_user_name = new WSI.Cashier.Controls.uc_label();
      this.txt_user_pin = new WSI.Cashier.Controls.uc_round_textbox();
      this.lbl_user_pin = new WSI.Cashier.Controls.uc_label();
      this.lbl_user_pin_confirm = new WSI.Cashier.Controls.uc_label();
      this.txt_user_pin_confirm = new WSI.Cashier.Controls.uc_round_textbox();
      this.lbl_user_username = new WSI.Cashier.Controls.uc_label();
      this.pnl_data.SuspendLayout();
      this.pnl_permission_list.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pb_login_icon)).BeginInit();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.Controls.Add(this.txt_user_pin_confirm);
      this.pnl_data.Controls.Add(this.txt_user_pin);
      this.pnl_data.Controls.Add(this.lbl_user_pin_confirm);
      this.pnl_data.Controls.Add(this.lbl_user_pin);
      this.pnl_data.Controls.Add(this.pnl_permission_list);
      this.pnl_data.Controls.Add(this.btn_login_cancel);
      this.pnl_data.Controls.Add(this.btn_shutdown);
      this.pnl_data.Controls.Add(this.btn_keyboard);
      this.pnl_data.Controls.Add(this.btn_user_login);
      this.pnl_data.Controls.Add(this.pb_login_icon);
      this.pnl_data.Controls.Add(this.txt_user_password);
      this.pnl_data.Controls.Add(this.txt_user_name);
      this.pnl_data.Controls.Add(this.lbl_user_password);
      this.pnl_data.Controls.Add(this.lbl_user_name);
      this.pnl_data.Controls.Add(this.lbl_user_username);
      this.pnl_data.Size = new System.Drawing.Size(573, 372);
      this.pnl_data.TabIndex = 0;
      // 
      // pnl_permission_list
      // 
      this.pnl_permission_list.Controls.Add(this.txt_permission_list);
      this.pnl_permission_list.Location = new System.Drawing.Point(97, 12);
      this.pnl_permission_list.Name = "pnl_permission_list";
      this.pnl_permission_list.Size = new System.Drawing.Size(463, 76);
      this.pnl_permission_list.TabIndex = 9;
      // 
      // txt_permission_list
      // 
      this.txt_permission_list.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_permission_list.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_permission_list.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_permission_list.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_permission_list.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_permission_list.CornerRadius = 5;
      this.txt_permission_list.Dock = System.Windows.Forms.DockStyle.Fill;
      this.txt_permission_list.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_permission_list.Location = new System.Drawing.Point(0, 0);
      this.txt_permission_list.MaxLength = 32767;
      this.txt_permission_list.Multiline = true;
      this.txt_permission_list.Name = "txt_permission_list";
      this.txt_permission_list.PasswordChar = '\0';
      this.txt_permission_list.ReadOnly = true;
      this.txt_permission_list.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
      this.txt_permission_list.SelectedText = "";
      this.txt_permission_list.SelectionLength = 0;
      this.txt_permission_list.SelectionStart = 0;
      this.txt_permission_list.Size = new System.Drawing.Size(463, 76);
      this.txt_permission_list.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.MULTILINE;
      this.txt_permission_list.TabIndex = 0;
      this.txt_permission_list.TabStop = false;
      this.txt_permission_list.Text = "\r\n   xIt is required the following permissions:";
      this.txt_permission_list.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_permission_list.UseSystemPasswordChar = false;
      this.txt_permission_list.WaterMark = null;
      this.txt_permission_list.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // btn_login_cancel
      // 
      this.btn_login_cancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_login_cancel.FlatAppearance.BorderSize = 0;
      this.btn_login_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_login_cancel.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_login_cancel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_login_cancel.Image = null;
      this.btn_login_cancel.IsSelected = false;
      this.btn_login_cancel.Location = new System.Drawing.Point(242, 300);
      this.btn_login_cancel.Name = "btn_login_cancel";
      this.btn_login_cancel.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_login_cancel.Size = new System.Drawing.Size(155, 60);
      this.btn_login_cancel.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_login_cancel.TabIndex = 5;
      this.btn_login_cancel.Text = "XCANCEL";
      this.btn_login_cancel.UseVisualStyleBackColor = false;
      this.btn_login_cancel.Click += new System.EventHandler(this.btn_login_cancel_Click);
      // 
      // btn_shutdown
      // 
      this.btn_shutdown.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(47)))), ((int)(((byte)(50)))));
      this.btn_shutdown.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
      this.btn_shutdown.FlatAppearance.BorderSize = 0;
      this.btn_shutdown.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_shutdown.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_shutdown.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_shutdown.Image = ((System.Drawing.Image)(resources.GetObject("btn_shutdown.Image")));
      this.btn_shutdown.IsSelected = false;
      this.btn_shutdown.Location = new System.Drawing.Point(12, 300);
      this.btn_shutdown.Name = "btn_shutdown";
      this.btn_shutdown.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_shutdown.Size = new System.Drawing.Size(70, 60);
      this.btn_shutdown.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.TERTIARY;
      this.btn_shutdown.TabIndex = 7;
      this.btn_shutdown.UseVisualStyleBackColor = false;
      this.btn_shutdown.Click += new System.EventHandler(this.btn_shutdown_Click);
      // 
      // btn_keyboard
      // 
      this.btn_keyboard.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(47)))), ((int)(((byte)(50)))));
      this.btn_keyboard.FlatAppearance.BorderSize = 0;
      this.btn_keyboard.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_keyboard.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_keyboard.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_keyboard.Image = ((System.Drawing.Image)(resources.GetObject("btn_keyboard.Image")));
      this.btn_keyboard.IsSelected = false;
      this.btn_keyboard.Location = new System.Drawing.Point(90, 300);
      this.btn_keyboard.Name = "btn_keyboard";
      this.btn_keyboard.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_keyboard.Size = new System.Drawing.Size(70, 60);
      this.btn_keyboard.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.TERTIARY;
      this.btn_keyboard.TabIndex = 6;
      this.btn_keyboard.UseVisualStyleBackColor = false;
      this.btn_keyboard.Click += new System.EventHandler(this.btn_keys_Click);
      // 
      // btn_user_login
      // 
      this.btn_user_login.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_user_login.FlatAppearance.BorderSize = 0;
      this.btn_user_login.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_user_login.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_user_login.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_user_login.Image = null;
      this.btn_user_login.IsSelected = false;
      this.btn_user_login.Location = new System.Drawing.Point(406, 300);
      this.btn_user_login.Name = "btn_user_login";
      this.btn_user_login.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_user_login.Size = new System.Drawing.Size(155, 60);
      this.btn_user_login.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_user_login.TabIndex = 4;
      this.btn_user_login.Text = "XLOGIN";
      this.btn_user_login.UseVisualStyleBackColor = false;
      this.btn_user_login.Click += new System.EventHandler(this.btn_ok_Click);
      // 
      // pb_login_icon
      // 
      this.pb_login_icon.Image = global::WSI.Cashier.Properties.Resources.iconoUsuario;
      this.pb_login_icon.Location = new System.Drawing.Point(13, 12);
      this.pb_login_icon.Name = "pb_login_icon";
      this.pb_login_icon.Size = new System.Drawing.Size(82, 76);
      this.pb_login_icon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
      this.pb_login_icon.TabIndex = 4;
      this.pb_login_icon.TabStop = false;
      // 
      // txt_user_password
      // 
      this.txt_user_password.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_user_password.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_user_password.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_user_password.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_user_password.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_user_password.CornerRadius = 5;
      this.txt_user_password.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_user_password.Location = new System.Drawing.Point(295, 151);
      this.txt_user_password.MaxLength = 15;
      this.txt_user_password.Multiline = false;
      this.txt_user_password.Name = "txt_user_password";
      this.txt_user_password.PasswordChar = '*';
      this.txt_user_password.ReadOnly = false;
      this.txt_user_password.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_user_password.SelectedText = "";
      this.txt_user_password.SelectionLength = 0;
      this.txt_user_password.SelectionStart = 0;
      this.txt_user_password.Size = new System.Drawing.Size(264, 40);
      this.txt_user_password.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_user_password.TabIndex = 1;
      this.txt_user_password.TabStop = false;
      this.txt_user_password.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_user_password.UseSystemPasswordChar = false;
      this.txt_user_password.WaterMark = "";
      this.txt_user_password.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // txt_user_name
      // 
      this.txt_user_name.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_user_name.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_user_name.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_user_name.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_user_name.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.txt_user_name.CornerRadius = 5;
      this.txt_user_name.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_user_name.Location = new System.Drawing.Point(295, 103);
      this.txt_user_name.MaxLength = 15;
      this.txt_user_name.Multiline = false;
      this.txt_user_name.Name = "txt_user_name";
      this.txt_user_name.PasswordChar = '\0';
      this.txt_user_name.ReadOnly = false;
      this.txt_user_name.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_user_name.SelectedText = "";
      this.txt_user_name.SelectionLength = 0;
      this.txt_user_name.SelectionStart = 0;
      this.txt_user_name.Size = new System.Drawing.Size(264, 40);
      this.txt_user_name.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_user_name.TabIndex = 0;
      this.txt_user_name.TabStop = false;
      this.txt_user_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_user_name.UseSystemPasswordChar = false;
      this.txt_user_name.WaterMark = "";
      this.txt_user_name.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // lbl_user_password
      // 
      this.lbl_user_password.BackColor = System.Drawing.Color.Transparent;
      this.lbl_user_password.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_user_password.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_user_password.Location = new System.Drawing.Point(99, 159);
      this.lbl_user_password.Name = "lbl_user_password";
      this.lbl_user_password.Size = new System.Drawing.Size(180, 25);
      this.lbl_user_password.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_user_password.TabIndex = 2;
      this.lbl_user_password.Text = "xPASSWORD";
      this.lbl_user_password.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_user_name
      // 
      this.lbl_user_name.BackColor = System.Drawing.Color.Transparent;
      this.lbl_user_name.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_user_name.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_user_name.Location = new System.Drawing.Point(99, 111);
      this.lbl_user_name.Name = "lbl_user_name";
      this.lbl_user_name.Size = new System.Drawing.Size(180, 25);
      this.lbl_user_name.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_user_name.TabIndex = 0;
      this.lbl_user_name.Text = "xUSER";
      this.lbl_user_name.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // txt_user_pin
      // 
      this.txt_user_pin.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_user_pin.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_user_pin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_user_pin.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_user_pin.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_user_pin.CornerRadius = 5;
      this.txt_user_pin.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_user_pin.Location = new System.Drawing.Point(295, 199);
      this.txt_user_pin.MaxLength = 4;
      this.txt_user_pin.Multiline = false;
      this.txt_user_pin.Name = "txt_user_pin";
      this.txt_user_pin.PasswordChar = '*';
      this.txt_user_pin.ReadOnly = false;
      this.txt_user_pin.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_user_pin.SelectedText = "";
      this.txt_user_pin.SelectionLength = 0;
      this.txt_user_pin.SelectionStart = 0;
      this.txt_user_pin.Size = new System.Drawing.Size(264, 40);
      this.txt_user_pin.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_user_pin.TabIndex = 2;
      this.txt_user_pin.TabStop = false;
      this.txt_user_pin.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_user_pin.UseSystemPasswordChar = false;
      this.txt_user_pin.WaterMark = "";
      this.txt_user_pin.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // lbl_user_pin
      // 
      this.lbl_user_pin.BackColor = System.Drawing.Color.Transparent;
      this.lbl_user_pin.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_user_pin.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_user_pin.Location = new System.Drawing.Point(99, 205);
      this.lbl_user_pin.Name = "lbl_user_pin";
      this.lbl_user_pin.Size = new System.Drawing.Size(180, 25);
      this.lbl_user_pin.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_user_pin.TabIndex = 11;
      this.lbl_user_pin.Text = "xPIN";
      this.lbl_user_pin.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_user_pin_confirm
      // 
      this.lbl_user_pin_confirm.BackColor = System.Drawing.Color.Transparent;
      this.lbl_user_pin_confirm.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_user_pin_confirm.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_user_pin_confirm.Location = new System.Drawing.Point(99, 253);
      this.lbl_user_pin_confirm.Name = "lbl_user_pin_confirm";
      this.lbl_user_pin_confirm.Size = new System.Drawing.Size(180, 25);
      this.lbl_user_pin_confirm.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_user_pin_confirm.TabIndex = 11;
      this.lbl_user_pin_confirm.Text = "xCONFIRM";
      this.lbl_user_pin_confirm.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // txt_user_pin_confirm
      // 
      this.txt_user_pin_confirm.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_user_pin_confirm.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_user_pin_confirm.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_user_pin_confirm.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_user_pin_confirm.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_user_pin_confirm.CornerRadius = 5;
      this.txt_user_pin_confirm.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_user_pin_confirm.Location = new System.Drawing.Point(295, 247);
      this.txt_user_pin_confirm.MaxLength = 4;
      this.txt_user_pin_confirm.Multiline = false;
      this.txt_user_pin_confirm.Name = "txt_user_pin_confirm";
      this.txt_user_pin_confirm.PasswordChar = '*';
      this.txt_user_pin_confirm.ReadOnly = false;
      this.txt_user_pin_confirm.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_user_pin_confirm.SelectedText = "";
      this.txt_user_pin_confirm.SelectionLength = 0;
      this.txt_user_pin_confirm.SelectionStart = 0;
      this.txt_user_pin_confirm.Size = new System.Drawing.Size(264, 40);
      this.txt_user_pin_confirm.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_user_pin_confirm.TabIndex = 3;
      this.txt_user_pin_confirm.TabStop = false;
      this.txt_user_pin_confirm.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_user_pin_confirm.UseSystemPasswordChar = false;
      this.txt_user_pin_confirm.WaterMark = "";
      this.txt_user_pin_confirm.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // lbl_user_username
      // 
      this.lbl_user_username.BackColor = System.Drawing.Color.Transparent;
      this.lbl_user_username.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_user_username.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_user_username.Location = new System.Drawing.Point(291, 111);
      this.lbl_user_username.Name = "lbl_user_username";
      this.lbl_user_username.Size = new System.Drawing.Size(268, 25);
      this.lbl_user_username.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_user_username.TabIndex = 12;
      this.lbl_user_username.Text = "xUSERNAME";
      this.lbl_user_username.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // frm_user_login
      // 
      this.AcceptButton = this.btn_user_login;
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
      this.ClientSize = new System.Drawing.Size(573, 427);
      this.CustomLocation = true;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "frm_user_login";
      this.ShowCloseButton = false;
      this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
      this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
      this.Style = WSI.Cashier.Controls.frm_base.BaseFormStyle.PRINCIPAL;
      this.Text = "User Login";
      this.TopMost = true;
      this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_user_login_FormClosing);
      this.Shown += new System.EventHandler(this.frm_user_login_Shown);
      this.pnl_data.ResumeLayout(false);
      this.pnl_permission_list.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.pb_login_icon)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion


    private WSI.Cashier.Controls.uc_round_textbox txt_user_name;
    private WSI.Cashier.Controls.uc_label lbl_user_password;
    private WSI.Cashier.Controls.uc_label lbl_user_name;
    private WSI.Cashier.Controls.uc_round_textbox txt_user_password;
    private System.Windows.Forms.PictureBox pb_login_icon;
    private WSI.Cashier.Controls.uc_round_button btn_user_login;
    private WSI.Cashier.Controls.uc_round_button btn_keyboard;
    private WSI.Cashier.Controls.uc_round_button btn_shutdown;
    private WSI.Cashier.Controls.uc_round_button btn_login_cancel;
    private System.Windows.Forms.Panel pnl_permission_list;
    private WSI.Cashier.Controls.uc_round_textbox txt_permission_list;
    private Controls.uc_round_textbox txt_user_pin_confirm;
    private Controls.uc_round_textbox txt_user_pin;
    private Controls.uc_label lbl_user_pin_confirm;
    private Controls.uc_label lbl_user_pin;
    private Controls.uc_label lbl_user_username;


  }
}