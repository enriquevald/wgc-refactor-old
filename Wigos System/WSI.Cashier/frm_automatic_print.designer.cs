namespace WSI.Cashier
{
  partial class frm_automatic_print
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.panel1 = new System.Windows.Forms.Panel();
      this.lbl_text_closing = new WSI.Cashier.Controls.uc_label();
      this.btn_cancel = new WSI.Cashier.Controls.uc_round_button();
      this.panel2 = new System.Windows.Forms.Panel();
      this.lbl_text = new WSI.Cashier.Controls.uc_label();
      this.tm_close = new System.Windows.Forms.Timer(this.components);
      this.pnl_data.SuspendLayout();
      this.panel1.SuspendLayout();
      this.panel2.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.Controls.Add(this.panel2);
      this.pnl_data.Controls.Add(this.panel1);
      this.pnl_data.Size = new System.Drawing.Size(572, 283);
      // 
      // panel1
      // 
      this.panel1.Controls.Add(this.lbl_text_closing);
      this.panel1.Controls.Add(this.btn_cancel);
      this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.panel1.Location = new System.Drawing.Point(0, 199);
      this.panel1.Name = "panel1";
      this.panel1.Padding = new System.Windows.Forms.Padding(8);
      this.panel1.Size = new System.Drawing.Size(572, 84);
      this.panel1.TabIndex = 10;
      // 
      // lbl_text_closing
      // 
      this.lbl_text_closing.BackColor = System.Drawing.Color.Transparent;
      this.lbl_text_closing.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_text_closing.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_text_closing.Location = new System.Drawing.Point(30, 24);
      this.lbl_text_closing.Name = "lbl_text_closing";
      this.lbl_text_closing.Size = new System.Drawing.Size(359, 45);
      this.lbl_text_closing.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_text_closing.TabIndex = 5;
      this.lbl_text_closing.Text = "Closing ...";
      // 
      // btn_cancel
      // 
      this.btn_cancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_cancel.FlatAppearance.BorderSize = 0;
      this.btn_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_cancel.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_cancel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_cancel.Location = new System.Drawing.Point(406, 12);
      this.btn_cancel.Name = "btn_cancel";
      this.btn_cancel.Size = new System.Drawing.Size(155, 60);
      this.btn_cancel.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_cancel.TabIndex = 9;
      this.btn_cancel.Text = "XCANCEL";
      this.btn_cancel.UseVisualStyleBackColor = false;
      this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
      // 
      // panel2
      // 
      this.panel2.AutoSize = true;
      this.panel2.Controls.Add(this.lbl_text);
      this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panel2.Location = new System.Drawing.Point(0, 0);
      this.panel2.Name = "panel2";
      this.panel2.Size = new System.Drawing.Size(572, 199);
      this.panel2.TabIndex = 11;
      // 
      // lbl_text
      // 
      this.lbl_text.BackColor = System.Drawing.Color.Transparent;
      this.lbl_text.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_text.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_text.Location = new System.Drawing.Point(30, 35);
      this.lbl_text.Name = "lbl_text";
      this.lbl_text.Size = new System.Drawing.Size(505, 148);
      this.lbl_text.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_text.TabIndex = 3;
      this.lbl_text.Text = "Message";
      // 
      // tm_close
      // 
      this.tm_close.Interval = 500;
      this.tm_close.Tick += new System.EventHandler(this.tm_close_Tick);
      // 
      // frm_automatic_print
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.AutoSize = true;
      this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
      this.ClientSize = new System.Drawing.Size(572, 338);
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "frm_automatic_print";
      this.ShowCloseButton = true;
      this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Style = WSI.Cashier.Controls.frm_base.BaseFormStyle.PRINCIPAL;
      this.Text = "Message";
      this.pnl_data.ResumeLayout(false);
      this.pnl_data.PerformLayout();
      this.panel1.ResumeLayout(false);
      this.panel2.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel panel1;
    private WSI.Cashier.Controls.uc_round_button btn_cancel;
    private System.Windows.Forms.Panel panel2;
    private WSI.Cashier.Controls.uc_label lbl_text;
    private System.Windows.Forms.Timer tm_close;
    private WSI.Cashier.Controls.uc_label lbl_text_closing;


  }
}