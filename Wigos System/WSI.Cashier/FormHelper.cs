using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Forms;

namespace WSI.Cashier
{

  public static class Utilities
  {
    public static T DeepClone<T>(T obj) 
    {
      using (var ms = new MemoryStream())
      {
        var formatter = new BinaryFormatter();
        formatter.Serialize(ms, obj);
        ms.Position = 0;

        return (T) formatter.Deserialize(ms);
      }
    }
  }

  public static class FormHelper
  {
    public static Control FindControlByTag(string Tagname, Control Control)
    {

      Control _retval = null;
      foreach (Control _ctrl in Control.Controls)
      {
        if (_ctrl.Tag != null && _ctrl.Tag.Equals(Tagname))
        {
          _retval = _ctrl;
          break;
        }
        _retval = FindControlByTag(Tagname, _ctrl);
        if (_retval != null)
        {
          break;
        }
      }
      return _retval;
    }
    public static IEnumerable<Control> FindControlsByTag(string Tagname, Control Control)
    {

      List<Control> _retval = new  List<Control>();
      foreach (Control _ctrl in Control.Controls)
      {
        if (_ctrl.Tag != null && _ctrl.Tag.Equals(Tagname))
        {
          _retval.Add(_ctrl);
        }
        _retval.AddRange(FindControlsByTag(Tagname, _ctrl));
      }
      return _retval;
    }

  }
}