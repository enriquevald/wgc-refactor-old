namespace WSI.Cashier
{
  partial class frm_message
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.panel2 = new System.Windows.Forms.Panel();
      this.btn_custom = new WSI.Cashier.Controls.uc_round_button();
      this.btn_ok = new WSI.Cashier.Controls.uc_round_button();
      this.btn_cancel = new WSI.Cashier.Controls.uc_round_button();
      this.panel1 = new System.Windows.Forms.Panel();
      this.lbl_text = new WSI.Cashier.Controls.uc_label();
      this.lbl_text_countdown_time_unit = new WSI.Cashier.Controls.uc_label();
      this.lbl_time_countdown = new WSI.Cashier.Controls.uc_label();
      this.lbl_text_countdown = new WSI.Cashier.Controls.uc_label();
      this.icon_box = new System.Windows.Forms.PictureBox();
      this.tm_close = new System.Windows.Forms.Timer(this.components);
      this.panel0 = new System.Windows.Forms.Panel();
      this.lbl_message_title = new WSI.Cashier.Controls.uc_label();
      this.panel2.SuspendLayout();
      this.panel1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.icon_box)).BeginInit();
      this.panel0.SuspendLayout();
      this.SuspendLayout();
      // 
      // panel2
      // 
      this.panel2.BackColor = System.Drawing.Color.Transparent;
      this.panel2.Controls.Add(this.btn_custom);
      this.panel2.Controls.Add(this.btn_ok);
      this.panel2.Controls.Add(this.btn_cancel);
      this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.panel2.Location = new System.Drawing.Point(0, 166);
      this.panel2.Name = "panel2";
      this.panel2.Padding = new System.Windows.Forms.Padding(50, 8, 50, 8);
      this.panel2.Size = new System.Drawing.Size(512, 180);
      this.panel2.TabIndex = 1;
      // 
      // btn_custom
      // 
      this.btn_custom.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_custom.FlatAppearance.BorderSize = 0;
      this.btn_custom.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_custom.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_custom.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_custom.Image = null;
      this.btn_custom.Location = new System.Drawing.Point(50, 4);
      this.btn_custom.Name = "btn_custom";
      this.btn_custom.Size = new System.Drawing.Size(408, 48);
      this.btn_custom.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_custom.TabIndex = 0;
      this.btn_custom.Text = "XCUSTOM";
      this.btn_custom.UseVisualStyleBackColor = false;
      this.btn_custom.Visible = false;
      this.btn_custom.Click += new System.EventHandler(this.btn_custom_Click);
      // 
      // btn_ok
      // 
      this.btn_ok.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_ok.FlatAppearance.BorderSize = 0;
      this.btn_ok.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_ok.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_ok.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_ok.Image = null;
      this.btn_ok.Location = new System.Drawing.Point(50, 62);
      this.btn_ok.Name = "btn_ok";
      this.btn_ok.Size = new System.Drawing.Size(408, 48);
      this.btn_ok.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_ok.TabIndex = 1;
      this.btn_ok.Text = "XOK";
      this.btn_ok.UseVisualStyleBackColor = false;
      this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
      // 
      // btn_cancel
      // 
      this.btn_cancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(47)))), ((int)(((byte)(50)))));
      this.btn_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btn_cancel.FlatAppearance.BorderSize = 0;
      this.btn_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_cancel.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_cancel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_cancel.Image = null;
      this.btn_cancel.Location = new System.Drawing.Point(50, 120);
      this.btn_cancel.Name = "btn_cancel";
      this.btn_cancel.Size = new System.Drawing.Size(408, 48);
      this.btn_cancel.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.TERTIARY;
      this.btn_cancel.TabIndex = 2;
      this.btn_cancel.Text = "XCANCEL";
      this.btn_cancel.UseVisualStyleBackColor = false;
      this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
      // 
      // panel1
      // 
      this.panel1.BackColor = System.Drawing.Color.Transparent;
      this.panel1.Controls.Add(this.lbl_text);
      this.panel1.Controls.Add(this.lbl_text_countdown_time_unit);
      this.panel1.Controls.Add(this.lbl_time_countdown);
      this.panel1.Controls.Add(this.lbl_text_countdown);
      this.panel1.Location = new System.Drawing.Point(0, 51);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(512, 116);
      this.panel1.TabIndex = 0;
      // 
      // lbl_text
      // 
      this.lbl_text.AutoSize = true;
      this.lbl_text.BackColor = System.Drawing.Color.Transparent;
      this.lbl_text.Font = new System.Drawing.Font("Montserrat", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_text.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_text.Location = new System.Drawing.Point(5, 30);
      this.lbl_text.Name = "lbl_text";
      this.lbl_text.Size = new System.Drawing.Size(120, 19);
      this.lbl_text.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_text.TabIndex = 3;
      this.lbl_text.Text = "User Message";
      this.lbl_text.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.lbl_text.UseMnemonic = false;
      // 
      // lbl_text_countdown_time_unit
      // 
      this.lbl_text_countdown_time_unit.AutoSize = true;
      this.lbl_text_countdown_time_unit.BackColor = System.Drawing.Color.Transparent;
      this.lbl_text_countdown_time_unit.Font = new System.Drawing.Font("Montserrat", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_text_countdown_time_unit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_text_countdown_time_unit.Location = new System.Drawing.Point(248, 53);
      this.lbl_text_countdown_time_unit.Name = "lbl_text_countdown_time_unit";
      this.lbl_text_countdown_time_unit.Size = new System.Drawing.Size(84, 19);
      this.lbl_text_countdown_time_unit.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_text_countdown_time_unit.TabIndex = 7;
      this.lbl_text_countdown_time_unit.Text = "Time unit";
      // 
      // lbl_time_countdown
      // 
      this.lbl_time_countdown.AutoSize = true;
      this.lbl_time_countdown.BackColor = System.Drawing.Color.Transparent;
      this.lbl_time_countdown.Font = new System.Drawing.Font("Montserrat", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_time_countdown.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_time_countdown.Location = new System.Drawing.Point(79, 53);
      this.lbl_time_countdown.Name = "lbl_time_countdown";
      this.lbl_time_countdown.Size = new System.Drawing.Size(145, 19);
      this.lbl_time_countdown.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_time_countdown.TabIndex = 6;
      this.lbl_time_countdown.Text = "Countdown time";
      this.lbl_time_countdown.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // lbl_text_countdown
      // 
      this.lbl_text_countdown.AutoSize = true;
      this.lbl_text_countdown.BackColor = System.Drawing.Color.Transparent;
      this.lbl_text_countdown.Font = new System.Drawing.Font("Montserrat", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_text_countdown.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_text_countdown.Location = new System.Drawing.Point(12, 77);
      this.lbl_text_countdown.Name = "lbl_text_countdown";
      this.lbl_text_countdown.Padding = new System.Windows.Forms.Padding(5);
      this.lbl_text_countdown.Size = new System.Drawing.Size(197, 29);
      this.lbl_text_countdown.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_text_countdown.TabIndex = 5;
      this.lbl_text_countdown.Text = "Countdown messsage";
      // 
      // icon_box
      // 
      this.icon_box.Location = new System.Drawing.Point(83, 26);
      this.icon_box.Name = "icon_box";
      this.icon_box.Size = new System.Drawing.Size(48, 48);
      this.icon_box.TabIndex = 4;
      this.icon_box.TabStop = false;
      this.icon_box.Visible = false;
      // 
      // tm_close
      // 
      this.tm_close.Interval = 1000;
      this.tm_close.Tick += new System.EventHandler(this.tm_close_Tick);
      // 
      // panel0
      // 
      this.panel0.Controls.Add(this.lbl_message_title);
      this.panel0.Dock = System.Windows.Forms.DockStyle.Top;
      this.panel0.Location = new System.Drawing.Point(0, 0);
      this.panel0.Name = "panel0";
      this.panel0.Size = new System.Drawing.Size(512, 51);
      this.panel0.TabIndex = 12;
      // 
      // lbl_message_title
      // 
      this.lbl_message_title.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.lbl_message_title.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_message_title.Font = new System.Drawing.Font("Montserrat", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_message_title.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(167)))), ((int)(((byte)(221)))), ((int)(((byte)(241)))));
      this.lbl_message_title.Location = new System.Drawing.Point(0, 0);
      this.lbl_message_title.Name = "lbl_message_title";
      this.lbl_message_title.Padding = new System.Windows.Forms.Padding(0, 6, 0, 6);
      this.lbl_message_title.Size = new System.Drawing.Size(512, 51);
      this.lbl_message_title.Style = WSI.Cashier.Controls.uc_label.LabelStyle.POPUP_HEADER;
      this.lbl_message_title.TabIndex = 9;
      this.lbl_message_title.Text = "xPOPUP_HEADER";
      this.lbl_message_title.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // frm_message
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
      this.ClientSize = new System.Drawing.Size(512, 346);
      this.Controls.Add(this.icon_box);
      this.Controls.Add(this.panel0);
      this.Controls.Add(this.panel1);
      this.Controls.Add(this.panel2);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "frm_message";
      this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Message";
      this.TopMost = true;
      this.panel2.ResumeLayout(false);
      this.panel1.ResumeLayout(false);
      this.panel1.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.icon_box)).EndInit();
      this.panel0.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private WSI.Cashier.Controls.uc_label lbl_message_title;
    private System.Windows.Forms.Panel panel2;
    private WSI.Cashier.Controls.uc_round_button btn_cancel;
    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.PictureBox icon_box;
    private WSI.Cashier.Controls.uc_label lbl_text;
    private WSI.Cashier.Controls.uc_round_button btn_ok;
    private System.Windows.Forms.Timer tm_close;
    private WSI.Cashier.Controls.uc_label lbl_text_countdown;
    private WSI.Cashier.Controls.uc_label lbl_time_countdown;
    private WSI.Cashier.Controls.uc_label lbl_text_countdown_time_unit;
    private WSI.Cashier.Controls.uc_round_button btn_custom;
    private System.Windows.Forms.Panel panel0;


  }
}