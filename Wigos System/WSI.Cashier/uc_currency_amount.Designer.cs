namespace WSI.Cashier
{
  partial class uc_currency_amount
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.txt_amount = new System.Windows.Forms.TextBox();
      this.pnl_currency_amount = new System.Windows.Forms.Panel();
      this.chk_CurrAct = new System.Windows.Forms.CheckBox();
      this.lbl_equal = new System.Windows.Forms.Label();
      this.pnl_currency_amount.SuspendLayout();
      this.SuspendLayout();
      // 
      // txt_amount
      // 
      this.txt_amount.BackColor = System.Drawing.Color.LightGoldenrodYellow;
      this.txt_amount.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.txt_amount.Location = new System.Drawing.Point(3, 3);
      this.txt_amount.MaxLength = 18;
      this.txt_amount.Name = "txt_amount";
      this.txt_amount.ReadOnly = true;
      this.txt_amount.Size = new System.Drawing.Size(235, 44);
      this.txt_amount.TabIndex = 16;
      this.txt_amount.TabStop = false;
      this.txt_amount.Text = "123";
      this.txt_amount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      // 
      // pnl_currency_amount
      // 
      this.pnl_currency_amount.Controls.Add(this.lbl_equal);
      this.pnl_currency_amount.Controls.Add(this.chk_CurrAct);
      this.pnl_currency_amount.Controls.Add(this.txt_amount);
      this.pnl_currency_amount.Location = new System.Drawing.Point(0, 1);
      this.pnl_currency_amount.Margin = new System.Windows.Forms.Padding(0);
      this.pnl_currency_amount.Name = "pnl_currency_amount";
      this.pnl_currency_amount.Size = new System.Drawing.Size(589, 71);
      this.pnl_currency_amount.TabIndex = 22;
      // 
      // chk_CurrAct
      // 
      this.chk_CurrAct.Appearance = System.Windows.Forms.Appearance.Button;
      this.chk_CurrAct.BackColor = System.Drawing.Color.White;
      this.chk_CurrAct.FlatAppearance.BorderColor = System.Drawing.Color.Black;
      this.chk_CurrAct.FlatAppearance.CheckedBackColor = System.Drawing.Color.White;
      this.chk_CurrAct.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
      this.chk_CurrAct.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
      this.chk_CurrAct.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.chk_CurrAct.Location = new System.Drawing.Point(244, 7);
      this.chk_CurrAct.Name = "chk_CurrAct";
      this.chk_CurrAct.Size = new System.Drawing.Size(49, 35);
      this.chk_CurrAct.TabIndex = 36;
      this.chk_CurrAct.Text = "xCURR";
      this.chk_CurrAct.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
      this.chk_CurrAct.UseVisualStyleBackColor = false;
      // 
      // lbl_equal
      // 
      this.lbl_equal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
      this.lbl_equal.Location = new System.Drawing.Point(3, 50);
      this.lbl_equal.Name = "lbl_equal";
      this.lbl_equal.Size = new System.Drawing.Size(235, 18);
      this.lbl_equal.TabIndex = 37;
      this.lbl_equal.Text = "xEqual:";
      this.lbl_equal.TextAlign = System.Drawing.ContentAlignment.TopRight;
      // 
      // uc_currency_amount
      // 
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
      this.BackColor = System.Drawing.Color.Transparent;
      this.Controls.Add(this.pnl_currency_amount);
      this.Margin = new System.Windows.Forms.Padding(0);
      this.Name = "uc_currency_amount";
      this.Size = new System.Drawing.Size(302, 77);
      this.Load += new System.EventHandler(this.uc_currency_amount_Load);
      this.pnl_currency_amount.ResumeLayout(false);
      this.pnl_currency_amount.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.TextBox txt_amount;
    private System.Windows.Forms.Panel pnl_currency_amount;
    private System.Windows.Forms.CheckBox chk_CurrAct;
    private System.Windows.Forms.Label lbl_equal;



  }
}
