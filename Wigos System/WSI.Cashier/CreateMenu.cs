﻿//------------------------------------------------------------------------------
// Copyright © 2015-2020 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: CreateMenu.cs
// 
//   DESCRIPTION: Class to manage the uc_menu
//
//        AUTHOR: Didac Campanals
// 
// CREATION DATE: 28-OCT-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 28-OCT-2015 DCS     First release.
// 28-JAN-2015 YNM     Fixed Bug 8731:BTN_RECEPTION is visible when Reception Modudle is disabled
// 08-SEP-2016 FOS     PBI 16567: Tables: Split PlayerTracking to GamingTables (Phase 5)
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WSI.Common;

namespace WSI.Cashier
{
  public class CreateMenu
  {

    public enum MenuType
    {
      NONE = 0,
      MAIN_MENU = 1,
      SUB_MENU = 2,
      GAMING_TABLES_MENU =3,
    }
    
    public class MenuItem
    {
      #region Members

      private String m_name;                                        // Internal name of the element.
      private String m_text;                                        // Text to show up in the menu element.
      private System.Drawing.Bitmap m_image;                        // Image to show in the menu element.
      private CreateMenu.Menu.MyEventHandler m_ButtonAction;        // Action to be execute when button is clicked.
      private Object m_mode;                                        // System mode for the button -> Only used in Main Menu
      private Boolean m_is_selectable;
      private Boolean m_is_enabled;
      private WSI.Cashier.Controls.uc_round_button m_sub_menu_button_parent;

      #endregion // Members

      #region Properties

      public String Name
      {
        get
        {
          return m_name;
        }
        set
        {
          m_name = value;
        }
      }

      public String Text
      {
        get
        {
          return m_text;
        }
        set
        {
          m_text = value;
        }
      }

      public System.Drawing.Bitmap Image
      {
        get
        {
          return m_image;
        }
        set
        {
          m_image = value;
        }
      }

      public CreateMenu.Menu.MyEventHandler Action
      {
        get
        {
          return m_ButtonAction;
        }
        set
        {
          m_ButtonAction = value;
        }
      }

      public Object  Mode
      {
        get
        {
          return m_mode;
        }
        set
        {
          m_mode = value;
        }
      }

      public Boolean IsSelectable
      {
        get
        {
          return m_is_selectable;
        }
        set
        {
          m_is_selectable = value;
        }
      }

      public Boolean IsEnabled
      {
        get
        {
          return m_is_enabled;
        }
        set
        {
          m_is_enabled = value;
        }
      }

      public WSI.Cashier.Controls.uc_round_button SubMenuButtonParent
      {
        get
        {
          return m_sub_menu_button_parent;
        }
        set
        {
          m_sub_menu_button_parent = value;
        }
      }


      #endregion // Properties

    }
    
    public class Menu
    {
      public delegate void MyEventHandler(object sender, EventArgs e);
      private Dictionary<String, MenuItem> m_button_list;
      private MenuType m_type;

      public MenuType MenuType
      {
        get
        {
          return m_type;
        }
        set
        {
          m_type = value;
        }
      }
            
      public Dictionary<String, MenuItem> ButtonList
      {
        get { return m_button_list; }
        set { m_button_list = value; }
      }

      public void AddButton(String Name, String Text, System.Drawing.Bitmap Image, MyEventHandler Action, Boolean Enabled)
      {
        AddButton(Name, Text, Image, Action, null, true, Enabled, null);
      }

      public void AddButton(String Name, String Text, System.Drawing.Bitmap Image, MyEventHandler Action)
      {
        AddButton(Name, Text, Image, Action, null, true, true, null);
      }

      public void AddButton(String Name, String Text, System.Drawing.Bitmap Image, MyEventHandler Action, WSI.Cashier.Controls.uc_round_button SubMenuButtonParent)
      {
        AddButton(Name, Text, Image, Action, null, true, true, SubMenuButtonParent);
      }

      public void AddButton(String Name, String Text, System.Drawing.Bitmap Image, MyEventHandler Action, Object Mode)
      {
        AddButton(Name, Text, Image, Action, Mode, true, true, null);
      }

      public void AddButton(String Name, String Text, System.Drawing.Bitmap Image, MyEventHandler Action, Object Mode, Boolean IsSelectable)
      {
        AddButton(Name, Text, Image, Action, Mode, IsSelectable, true, null);
      }

      public void AddButton(String Name, String Text, System.Drawing.Bitmap Image, MyEventHandler Action, Object Mode, Boolean IsSelectable, Boolean Enabled, WSI.Cashier.Controls.uc_round_button SubMenuButtonParent)
      {
        MenuItem _item;

        _item = new MenuItem();

        _item.Name = Name;
        _item.Text = Text.Replace("\\r\\n", "\r\n");
        _item.Image = Image;
        _item.Action = Action;
        _item.IsEnabled = Enabled;
        if (Mode == null)
        {
          _item.Mode = MenuType.SUB_MENU;
        }
          else
        {
          _item.Mode = Mode;
        }
        _item.IsSelectable = IsSelectable;
        
        if (m_button_list == null)
        {
          m_button_list = new Dictionary<String, MenuItem>();
        }
        _item.SubMenuButtonParent = SubMenuButtonParent;

        m_button_list.Add(Name + _item.Mode.ToString(), _item);
      }

      public List<MenuItem> GetButtons()
      {
        List<MenuItem> _return_list;
        Boolean _allowed_button;
                        
        _return_list = new List<MenuItem>();
        
        foreach (KeyValuePair<String, MenuItem> _item in m_button_list)
        {
          switch (m_type)
          {
            case CreateMenu.MenuType.MAIN_MENU:
              _allowed_button = IsMainButtonAllowedByConfiguration(_item.Value);
              break;
            case CreateMenu.MenuType.SUB_MENU:
              _allowed_button = true;
              break;
            case CreateMenu.MenuType.GAMING_TABLES_MENU:
              _allowed_button = true;
              break;
            case CreateMenu.MenuType.NONE:
            default:
              _allowed_button = false;
              break;
          }

          if (_allowed_button)
          {
            _return_list.Add(_item.Value);
          }
        }

        return _return_list;
      }

      // The button by default is allowed, but if exists any special condition which not is true, returns false
      private Boolean IsMainButtonAllowedByConfiguration(MenuItem Item)
      {
        Boolean _btn_allowed;

        if ((SYSTEM_MODE)Item.Mode != Misc.SystemMode())
        {
          return false;
        }
        
        switch (Item.Name)
        {
          case "BTN_GIFTS":
            _btn_allowed = GeneralParam.GetInt32("PlayerTracking.ExternalLoyaltyProgram", "Mode") == 0;
            break;
          case "BTN_CONCEPTS":
            _btn_allowed = Cage.IsCageEnabled();
            break;
          case "BTN_PLAYER_TRACKING":
            _btn_allowed = GamingTableBusinessLogic.IsFeaturesGamingTablesEnabled();
            break;
          case "BTN_RECEPTION":
            _btn_allowed = GeneralParam.GetBoolean("Reception", "Enabled", false);
            break;
          default:
            _btn_allowed = true;
            break;
        }

        return _btn_allowed;
      }
      
      public Menu(MenuType MenuType)
      {
        m_type = MenuType;
      }

    }
  }
}
