﻿//------------------------------------------------------------------------------
// Copyright © 2012 Win Systems International.
//------------------------------------------------------------------------------
// 
//   FORM NAME: frm_prize_payout.cs
// 
//   DESCRIPTION: Form to charge Prize Payout & Prize Payout and Recharge
// 
//        AUTHOR: Ariel Vazquez Zerpa / Gustavo Ali
// 
// CREATION DATE: 23-APR-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 13-JUN-2016 GA SDS Bug 13981:CardPrice
// 19-SEP-2016 FGB    PBI 15985: Exped System: Cashier: Check if the payment/sale can be done
// 22-SEP-2016 LTC    Product Backlog Item 17461: Gamingtables 46 - Cash desk draw for gamblign tables: Chips purchase
// 06-OCT-2016 FAV    Fixed Bug 18440: Exception in log for a Refund (award)
// 15-MAR-2017 FAV    PBI 25262: Exped - Payment authorisation
// 24-MAR-2017 ATB    PBI 25262: Exped - Payment authorisation
// 04-MAY-2017 DPC    WIGOS-1574: Junkets - Flyers - New field "Text in Promotional Voucher" - Change voucher
// 03-JUL-2017 EOR    Bug 28393: WIGOS-3185 Cash net is negative after withdraw partial redeem in cashier
// ----------- ------ ----------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WSI.Common;
using WSI.Common.ExternalPaymentAndSaleValidation;
using WSI.Common.Utilities.Extensions;

namespace WSI.Cashier
{
  public partial class frm_prize_payout : Controls.frm_base
  {
    private CardData m_card_data;
    private Form m_parent_form;
    private frm_amount_input.CheckPermissionsToRedeemDelegate m_check_permissions_to_redeem;
    private Currency m_current_amount;
    private Boolean m_is_voucher_loaded;

    public frm_prize_payout(CardData RelatedCard, frm_amount_input.CheckPermissionsToRedeemDelegate CheckPermissionsToRedeem)
    {
      InitializeComponent();
      InitializeControlResources();

      uc_input_amount.SetDotButtonEnabled(true);
      m_card_data = RelatedCard;
      m_check_permissions_to_redeem = CheckPermissionsToRedeem;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Init and Show the form.
    //
    //  PARAMS :
    //      - INPUT : ParentForm
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public DialogResult Show(Form ParentForm)
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_prize_payout", Log.Type.Message);

      m_parent_form = ParentForm;
      DisableButtons();

      this.ShowDialog(ParentForm);
      return this.DialogResult;
    }

    private void InitializeControlResources()
    {
      this.FormTitle = Resource.String("STR_FRM_PRIZE_PAYOUT");
      this.btn_pay.Text = Resource.String("STR_FRM_PRIZE_PAYOUT_PAY");
      this.gb_total.HeaderText = Resource.String("STR_FRM_PRIZE_PAYOUT_PAY_AMOUNT_TEXT");
      this.btn_pay_and_recharge.Text = Resource.String("STR_FRM_PRIZE_PAYOUT_PAY_AND_RECHARGE");
      this.btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");
    }

    private void btn_pay_Click(object sender, EventArgs e)
    {
      MakePayment(Promotion.PROMOTION_TYPE.AUTOMATIC_PRIZE_PAYOUT);
    }

    private void btn_pay_and_recharge_Click(object sender, EventArgs e)
    {
      MakePayment(Promotion.PROMOTION_TYPE.AUTOMATIC_PRIZE_PAYOUT_AND_RECHARGE);
    }

    private void MakePayment(Promotion.PROMOTION_TYPE PromotionType)
    {
      AccountPromotion _promotion;
      OperationCode _operation_code;
      RechargeOutputParameters _output_params;
      ParamsTicketOperation _operations_params;
      CashierSessionInfo _cashier_session_info;

      if (!Validate(PromotionType))
      {
        return;
      }

      _operations_params = new ParamsTicketOperation();
      _operations_params.in_account = m_card_data;
      _operations_params.in_window_parent = m_parent_form;

      if (WSI.Common.Misc.IsCashlessMode() || WSI.Common.Misc.IsMico2Mode())
      {
        _operations_params.is_apply_tax = true;
      }

      //Witholding documents
      if (CashierBusinessLogic.WithholdAndPaymentOrderDialog(OperationCode.CASH_OUT,
                                                             ref _operations_params,
                                                             CashierBusinessLogic.EnterWitholdingData,
                                                             null,
                                                             m_check_permissions_to_redeem,
                                                             false,
                                                             m_current_amount) != ENUM_TITO_OPERATIONS_RESULT.OK)
      {
        return;
      }

      using (DB_TRX _db_trx = new DB_TRX())
      {
        if (!Promotion.ReadPromotionData(_db_trx.SqlTransaction, PromotionType, out _promotion))
        {
          return;
        }

        _cashier_session_info = Cashier.CashierSessionInfo();
        _operation_code = GetOperationCode(_promotion, _db_trx.SqlTransaction);

        // If exped is active, we need to request authorization to refund
        if (PromotionType == Promotion.PROMOTION_TYPE.AUTOMATIC_PRIZE_PAYOUT && ExternalPaymentAndSaleValidationCommon.ExpedMode == EnumExternalPaymentAndSaleValidationMode.MODE_EXPED)
        {
          if (!ExternalWS.ExpedConnect(m_card_data.AccountId, m_current_amount, _operation_code))
          {

            if (ExternalPaymentAndSaleValidationCommon.ExpedMode != EnumExternalPaymentAndSaleValidationMode.MODE_EXPED
                || Misc.ExternalWS.OutputParams.ValidationResult != EnumExternalPaymentAndSaleValidationResult.RESULT_AUTHORIZED)
            {
              frm_message.Show(ExternalPaymentAndSaleValidationCommon.GetErrorMessageFromExternalProvider(m_card_data.AccountId, m_card_data.PlayerTracking.HolderName, m_current_amount),
                             Resource.String("STR_APP_GEN_MSG_ERROR"),
                             MessageBoxButtons.OK,
                             MessageBoxIcon.Error,
                             m_parent_form);
            }
            return;
          }
          if (Misc.ExternalWS.OutputParams.ValidationMessage == "cancel")
          {
            return;
          }
        }

        if (_operation_code == OperationCode.PRIZE_PAYOUT_AND_RECHARGE)
        {
          Misc.ExternalWS.InputParams = null;
          Misc.ExternalWS.OutputParams = null;
        }

        if (!Accounts.DB_CardCreditAdd(m_card_data, 0, _promotion.id, m_current_amount, 0, 0, null, ParticipateInCashDeskDraw.Default, _operation_code,
                                      _operations_params.withhold, _cashier_session_info, _db_trx.SqlTransaction, out _output_params, Misc.ExternalWS.InputParams, Misc.ExternalWS.OutputParams))
        {
          string _error_message;
          if (ExternalPaymentAndSaleValidationCommon.ExpedMode == EnumExternalPaymentAndSaleValidationMode.MODE_EXPED &&
            String.IsNullOrEmpty(_output_params.ErrorMessage))
          {
            _error_message = ExternalPaymentAndSaleValidationCommon.GetErrorMessageFromExternalProvider(m_card_data.AccountId, m_card_data.PlayerTracking.HolderName, m_current_amount);
          }
          else
          {
            _error_message = _output_params.ErrorMessage;
          }
          // If there's no message in any way (weird case)
          if (ExternalPaymentAndSaleValidationCommon.ExpedMode == EnumExternalPaymentAndSaleValidationMode.MODE_EXPED &&
            String.IsNullOrEmpty(_error_message))
          {
            _error_message = _output_params.ErrorCode.ToString();
          }
          frm_message.Show(_error_message,
                           Resource.String("STR_APP_GEN_MSG_ERROR"),
                           MessageBoxButtons.OK,
                           MessageBoxIcon.Error,
                           m_parent_form);
          return;
        }

        _db_trx.Commit();

        if (_operations_params.withhold != null)
        {
          if (Witholding.GenerateWitholdingDocument()) //Witholding
          {
            PrintingDocuments.PrintDialogDocuments(_output_params.OperationId);
          }
        }
      }

      VoucherPrint.Print(_output_params.VoucherList);
      this.DialogResult = DialogResult.OK;
    }
    private bool Validate(Promotion.PROMOTION_TYPE PromotionType)
    {
      if (m_current_amount <= 0)
      {
        frm_message.Show(Resource.String("STR_FRM_PRIZE_PAYOUT_INVALID_AMOUNT"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this);
        return false;
      }

      CardData.DB_CardGetAllData(m_card_data.AccountId, m_card_data);
      if (m_card_data.IsLoggedIn)
      {
        frm_message.Show(Resource.String("STR_UC_CARD_IN_USE_ERROR"),
                         Resource.String("STR_APP_GEN_MSG_WARNING"),
                         MessageBoxButtons.OK,
                         MessageBoxIcon.Warning,
                         m_parent_form);

        return false;
      }

      // EOR 28-JUN-2017
      if (PromotionType == Promotion.PROMOTION_TYPE.AUTOMATIC_PRIZE_PAYOUT | PromotionType == Promotion.PROMOTION_TYPE.AUTOMATIC_PRIZE_PAYOUT_AND_RECHARGE)
      {
        Currency _cashier_current_balance;
        try
        {
          using (DB_TRX _db_trx = new DB_TRX())
          {
            _cashier_current_balance = CashierBusinessLogic.UpdateSessionBalance(_db_trx.SqlTransaction, Cashier.SessionId, 0);
          }
        }
        catch (Exception ex)
        {
          Log.Exception(ex);
          return false;
        }

        if (m_current_amount > _cashier_current_balance)
        {
          frm_message.Show(Resource.String("STR_FRM_AMOUNT_INPUT_MSG_AMOUNT_EXCEED_BANK"),
                           Resource.String("STR_APP_GEN_MSG_ERROR"),
                           MessageBoxButtons.OK,
                           MessageBoxIcon.Error,
                           this);
          return false;
        }
      }

      return true;
    }

    private OperationCode GetOperationCode(AccountPromotion Promotion, SqlTransaction SqlTrx)
    {
      return (Promotion.type == Common.Promotion.PROMOTION_TYPE.AUTOMATIC_PRIZE_PAYOUT ? OperationCode.PRIZE_PAYOUT : OperationCode.PRIZE_PAYOUT_AND_RECHARGE);
    }

    private void EnableButtons()
    {
      //uc_input_amount.SetTextNoVisible();
      this.btn_pay.Enabled = true;
      this.btn_pay_and_recharge.Enabled = true;

      this.lbl_total_to_pay.Visible = true;
      uc_input_amount.ResetAmount();
    }

    private void DisableButtons()
    {
      //uc_input_amount.SetTextVisible();
      this.btn_pay.Enabled = false;
      this.btn_pay_and_recharge.Enabled = false;

      this.lbl_total_to_pay.Visible = false;
      web_browser.Navigate("about:blank");
      m_is_voucher_loaded = false;
    }

    private void uc_input_amount_OnAmountSelected(decimal Amount)
    {
      if (Amount > 0)
      {
        AccountPromotion _promo;

        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (!Promotion.ReadPromotionData(_db_trx.SqlTransaction, Promotion.PROMOTION_TYPE.AUTOMATIC_PRIZE_PAYOUT, out _promo))
          {
            return;
          }

          m_current_amount = Amount;
          OperationCode _op = GetOperationCode(_promo, _db_trx.SqlTransaction);
          DevolutionPrizeParts _parts = new DevolutionPrizeParts(false, _op);
          _parts.Compute(m_current_amount, 0, true, false);

          // GA SDS 13-06-2016 Bug 13981:CardPrice
          Currency _total = _parts.TotalPayable - Accounts.CardDepositToPay(m_card_data);
          this.lbl_total_to_pay.Text = _total.ToString();
          // Generate voucher in preview mode
          PreviewVoucher((Currency)m_current_amount, true, _db_trx);

          if (m_is_voucher_loaded)
          {
            EnableButtons();
          }
        }
      }
    }

    private void uc_input_amount_OnModifyValue(string Amount)
    {
      if (m_is_voucher_loaded)
      {
        DisableButtons();
      }
    }

    private ArrayList GetCardVoucher(CardData CardData, DB_TRX DbTrx)
    {
      ArrayList _list = new ArrayList();
      TYPE_SPLITS _splits;
      if (!Split.ReadSplitParameters(out _splits))
      {
        return _list;
      }

      VoucherCashIn.InputDetails _details = new VoucherCashIn.InputDetails();
      _details.InitialBalance = new MultiPromos.PromoBalance();
      _details.FinalBalance = new MultiPromos.PromoBalance();
      //_details.CashIn1 = (Currency)0;
      //_details.CashIn2 = (Currency)0;
      //_details.CashInTaxSplit1 = (Currency)0;
      //_details.CashInTaxSplit2 = (Currency)0;
      //_details.CashInTaxCustody = (Currency)0;
      //_details.TotalBaseTaxSplit2 = (Currency)0;
      //_details.TotalTaxSplit2 = (Currency)0;
      //_details.IsIntegratedChipsSale = false;
      _details.CardPrice = Accounts.CardDepositToPay(CardData);

      _list = VoucherBuilder.CashIn(CardData.VoucherAccountInfo(),
                                                CardData.AccountId,
                                                _splits,
                                                _details,
                                                null,
                                                0,
                                                PrintMode.Preview,
                                                CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION.DRAW_00, //LTC 22-SEP-2016
                                    DbTrx.SqlTransaction,
                                    null);

      return _list;
    }

    private void PreviewVoucher(Currency VoucherAmount, Boolean IsApplyTaxes, DB_TRX DbTrx)
    {
      String _voucher_file_name;
      ArrayList _voucher_list;
      TYPE_SPLITS _splits;
      MultiPromos.PromoBalance _ini_balance;
      MultiPromos.PromoBalance _fin_promo_balance;
      TYPE_CASH_REDEEM _cash_redeem_ab;
      TYPE_CASH_REDEEM _cash_company_a;
      TYPE_CASH_REDEEM _cash_company_b;
      CashAmount _cash;
      List<VoucherValidationNumber> m_validation_numbers;

      _voucher_file_name = "";

      try
      {
        _cash = new CashAmount();

        _voucher_list = new ArrayList();

        // Re-read account data

        //SDS 18-05-2016 Bug 13001
        if (!CardData.DB_CardGetAllData(m_card_data.AccountId, m_card_data, DbTrx.SqlTransaction))
        {
          //return false;
        }

        m_card_data.Cancellable.Operations.Clear();
        m_card_data.Cancellable.TotalBalance = MultiPromos.AccountBalance.Zero;
        m_card_data.Cancellable.TotalOperationAmount = 0;
        m_card_data.Cancellable.TotalCashInTaxAmount = 0;

        _ini_balance = new MultiPromos.PromoBalance();
        _ini_balance.Balance = m_card_data.AccountBalance;
        _ini_balance.Balance.Redeemable += m_current_amount;

        if (!m_card_data.CardPaid)
        {
          _voucher_list.AddRange(GetCardVoucher(m_card_data, DbTrx));
        }

        if (!Split.ReadSplitParameters(out _splits))
        {
          return;
        }

        if (!_cash.CalculateCashAmountWithRedeemableTaxes(CASH_MODE.PARTIAL_REDEEM,
                                              _splits,
                                              _ini_balance,
                                              VoucherAmount, // Only used in CASH_MODE.PARTIAL_REDEEM
                                              false,
                                              false,
                                              m_card_data,
                                              OperationCode.PRIZE_PAYOUT_AND_RECHARGE,
                                              out _fin_promo_balance,
                                              out _cash_redeem_ab,
                                              out _cash_company_a,
                                              out _cash_company_b))
        {
          String _str_warning;

          _cash.CalculateCashAmount_GetErrorMessage(_ini_balance.Balance.TotalRedeemable, VoucherAmount, m_card_data, out _str_warning);

          frm_message.Show(_str_warning, Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this);

          m_is_voucher_loaded = false;

          return;
        }

        string[] acountInfo = m_card_data.VoucherAccountInfo();
        Int64 acountid = m_card_data.AccountId;
        Currency balance = m_card_data.CurrentBalance;
        m_validation_numbers = new List<VoucherValidationNumber>();

        _voucher_list.AddRange(VoucherBuilder.CashOut(acountInfo,
                                          acountid,
                                          balance,
                                           _splits,
                                           _cash_company_a,
                                           _cash_company_b,
                                           0,
                                           PrintMode.Preview,
                                           m_validation_numbers,
                                           DbTrx.SqlTransaction,
                                           false,
                                           IsApplyTaxes,
                                           false,
                                           OperationCode.PRIZE_PAYOUT_AND_RECHARGE));

        _voucher_file_name = VoucherPrint.GenerateFileForPreview(_voucher_list);
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
      finally
      {

      }

      web_browser.Navigate(_voucher_file_name);
      m_is_voucher_loaded = true;
    } // PreviewVoucher

    public enum VoucherTypes
    {
      CardAdd = 0,
      CardRedeem = 1,
      FillIn = 2,
      FillOut = 3,
      OpenCash = 4,
      CloseCash = 5,
      MBCardChangeLimit = 6,
      MBCardCashDeposit = 7,
      StatusCash = 8,
      CardReplacement = 9,
      MBCardSetLimit = 10,
      CancelPromotions = 11,
      TicketRedeem = 12,
      ChipsSaleAmountInput = 13,
      ChipsSaleWithRecharge = 14,
      GenericAmountInput = 15,
      AmountRequest = 16,
      ChipsPurchaseAmountInput = 17
    }
  }
}
