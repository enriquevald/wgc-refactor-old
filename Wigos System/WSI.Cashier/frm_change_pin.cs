//------------------------------------------------------------------------------
// Copyright � 2009 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_change_pin.cs
// 
//   DESCRIPTION: Implements the following classes:
//                1. frm_change_pin
//
//        AUTHOR: APB
// 
// CREATION DATE: 13-JUL-2009
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 13-JUL-2007 APB    First release.
// 13-AUG-2013 DRV    Removed disabling OSK if it's in window mode. 
// 07-DEC-2016 JBP    Bug 21323:Cajero - No debe permitir entrar en el formulario de cambio de pin en modo card login si el usuario no dispone de tarjeta de empleado.
// 27-NOV-2017 JBM    WIGOS-3383: MobiBank: Cashier management - Change pin
//------------------------------------------------------------------------------
using System;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WSI.Cashier.Controls;
using WSI.Common;

namespace WSI.Cashier
{
  public partial class frm_change_pin : frm_base
  {
    #region Attributes
    private Form m_parent_form;

    private ChangePinMode m_mode;

    private UserCard m_user_card;
    private Int64 m_account_id;
    private String m_track_data;
    private String m_holder_name;
    private String m_old_pin;
    #endregion

    #region Enums
    public enum ChangePinMode
    {
      MobileBank = 0,
      UserCard = 1
    }
    #endregion 

    #region Constructor

    public frm_change_pin(Form ParentForm)
    {
      m_parent_form = ParentForm;

      InitializeComponent();

      InitializeControlResources();
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Initialize control resources (NLS strings, images, etc)
    /// </summary>
    private void InitializeControlResources()
    {
      // - NLS Strings:
      //   - Title:
      this.FormTitle = Resource.String("STR_FRM_CHANGE_PIN_TITLE");
      this.Text = this.FormTitle;

      //   - Labels
      lbl_enter_pin.Text = Resource.String("STR_FRM_CHANGE_PIN_MSG");
      lbl_confirm_pin.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_CONFIRM_PIN");
      lbl_account.Text = Resource.String("STR_FORMAT_GENERAL_NAME_MB");
      lbl_card.Text = Resource.String("STR_FORMAT_GENERAL_NAME_CARD");
      lbl_holder.Text = Resource.String("STR_FRM_CHANGE_PIN_HOLDER");

      //   - Buttons
      btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");
      btn_ok.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_OK");

      //   - Other
      gp_card_box.HeaderText = Resource.String("STR_FRM_CARD_ASSIGN_003");

      txt_pin.MaxLength = Accounts.PIN_MAX_SAVE_LENGTH;
      txt_confirm_pin.MaxLength = Accounts.PIN_MAX_SAVE_LENGTH;

      btn_ok.Enabled = false;

      // Mobibank device mode
      if (MobileBankConfig.IsMobileBankLinkedToADevice)
      {
        gp_card_box.Visible = false;
        gp_card_box.Enabled = false;

        this.btn_ok.Location = new System.Drawing.Point(356, 120);
        this.btn_cancel.Location = new System.Drawing.Point(188, 120);

        this.Size = new System.Drawing.Size(526, 250);

        CenterFormInScreen();
      }
    } // InitializeControlResources

    private void Init()
    {
      CenterFormInScreen();

      //Shows OSK if Automatic OSK it's enabled
      WSIKeyboard.Toggle();
    } // Init

    private void CenterFormInScreen()
    {
      //Center in shader
      WindowManager.CenterInParentForm(m_parent_form, this);
    }

    private void btn_cancel_Click(object sender, EventArgs e)
    {
      WSIKeyboard.Hide(); 
      this.Dispose();
    } // btn_cancel_Click

    private void btn_ok_click(object sender, EventArgs e)
    {
      if (!CheckIfCanUpdatePin())
        {
        return;
        }

      if (this.m_mode == ChangePinMode.MobileBank)
        {
          // Update MobileBank pin
        CashierBusinessLogic.DB_UpdateMBCardPin(this.m_account_id, this.txt_pin.Text);
          }
          else
          {
        // Update UserCard pin
            this.m_user_card.UpdatePin(this.txt_pin.Text);
          }

          WSIKeyboard.Hide();
          this.Dispose();
    } // btn_ok_click

    /// <summary>
    /// Check if pim can be updated
    /// </summary>
    /// <returns></returns>
    private Boolean CheckIfCanUpdatePin()
    {
      if (this.txt_pin.Text.Length != Accounts.PIN_MIN_LENGTH)
      {
        frm_message.Show(Resource.String("STR_FRM_CHANGE_PIN_LENGTH_ERROR",
                         Accounts.PIN_MIN_LENGTH.ToString()),
                         Resource.String("STR_APP_GEN_MSG_ERROR"),
                         MessageBoxButtons.OK,
                         MessageBoxIcon.Warning,
                         this.ParentForm);
        this.txt_pin.Focus();

        return false;
        }

      if (this.txt_pin.Text != this.txt_confirm_pin.Text)
      {
        frm_message.Show(Resource.String("STR_FRM_ACCOUNT_USER_EDIT_CONFIRMATION_PIN_DIFFERS"),
                         Resource.String("STR_APP_GEN_MSG_ERROR"),
                         MessageBoxButtons.OK,
                         MessageBoxIcon.Warning,
                         this.ParentForm);
        return false;
      }

      return true;
    }

    private void frm_change_pin_Shown(object sender, EventArgs e)
    {
      this.txt_pin.Focus();
    } // frm_change_pin_Shown

    /// <summary>
    /// Checks if the input is completely numeric.
    /// </summary>
    /// <param name="TrackNumber"></param>
    /// <returns></returns>
    private Boolean PartialPinNumberIsValid(String PinNumber)
    {
      Byte[] chars;

      chars = null;

      try
      {
        chars = Encoding.ASCII.GetBytes(PinNumber);
        foreach (byte b in chars)
        {
          if (b < '0')
          {
            return false;
          }
          if (b > '9')
          {
            return false;
          }
        }
      }
      finally
      {
        chars = null;
      }

      return true;
    } // PartialPinNumberIsValid

    #endregion

    #region Public Methods

    /// <summary>
    /// Get mobile bank data
    /// </summary>
    /// <param name="MBAccountId"></param>
    /// <param name="TrackData"></param>
    /// <param name="HolderName"></param>
    /// <param name="OldPin"></param>
    public Boolean GetMBCardData(Int64 MBAccountId, String TrackData, String HolderName, String OldPin)
    {
      this.Init();

      lbl_account.Text = Resource.String("STR_FORMAT_GENERAL_NAME_MB");

      this.m_mode = ChangePinMode.MobileBank;

      this.m_account_id = MBAccountId;
      this.m_track_data = TrackData;
      this.m_holder_name = HolderName;
      this.m_old_pin = OldPin;

      ShowData();
     
     return true;
    }

    /// <summary>
    /// Get user card data
    /// </summary>
    /// <param name="Id"></param>
    /// <param name="CardStatus"></param>
    /// <returns></returns>
    public Boolean GetUserCardData(Int32 Id, out UserCardStatus CardStatus)
    {      
      this.Init();

      lbl_account.Text = Resource.String("STR_SAFE_KEEPING_HOLDER_ID");

      this.m_mode = ChangePinMode.UserCard;
      this.m_user_card = new UserCard(Id, WCP_CardTypes.CARD_TYPE_EMPLOYEE);

      CardStatus = this.m_user_card.Status;

      if (this.m_user_card.Status != UserCardStatus.Ok)
      {
        return false;
      }

      this.m_account_id = Id;
      this.m_track_data = this.m_user_card.TrackData;
      this.m_holder_name = this.m_user_card.HolderName;
      this.m_old_pin = this.m_user_card.Pin;

      ShowData();
      
      return true;
    } // GetUserCardData

    private void ShowData()
    {
      this.txt_pin.Text = String.Empty;
      this.txt_confirm_pin.Text = String.Empty;
      this.lbl_account_value.Text = m_account_id.ToString();
      this.lbl_trackdata.Text = this.m_track_data;
      this.lbl_holder_value.Text = this.m_holder_name;

      if (MobileBankConfig.IsMobileBankLinkedToADevice)
      {
        this.FormTitle += " (" + lbl_holder_value.Text + ") ";
      }
    }

    private void txt_pin_TextChanged(object sender, EventArgs e)
    {
      EnableTextFieldAndButtonOk();
    }

    private void txt_confirm_pin_TextChanged(object sender, EventArgs e)
    {
      EnableTextFieldAndButtonOk();
    }

    private void EnableTextFieldAndButtonOk()
    {
      txt_confirm_pin.Enabled = (txt_pin.Text.Length >= Accounts.PIN_MIN_LENGTH);

      btn_ok.Enabled = ((txt_pin.Text.Length >= Accounts.PIN_MIN_LENGTH)
                              && (txt_pin.Text == txt_confirm_pin.Text));
    }

    #endregion

  } 
}