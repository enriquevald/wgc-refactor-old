﻿//------------------------------------------------------------------------------
// Copyright © 2015-2020 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: CashierStyle.cs
// 
//   DESCRIPTION: Implements the CashierStyle user control
//                Class: CashierStyle
//
//        AUTHOR: David Hernández
//                José Martínez
// 
// CREATION DATE: 20-OCT-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 20-OCT-2015 DHA     First release.
// 20-OCT-2015 JML     First release.
// 09-NOV-2015 FOS     Product Backlog Item 5850:New button type
// 11-NOV-2015 ETP     Product Backlog Item 5808: New class image for data grids.
// 22-JAN-2015 FOS     Bug 8587: Change Font "Montserrat" to "Open Sans"
// 14-MAR-2016 FOS     Fixed Bug 9357: New design corrections
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Text;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using WSI.Cashier.Controls;
using System.Globalization;
using System.Reflection;

namespace WSI.Cashier
{
  public static class CashierStyle
  {
    public static class ControlStyle
    {
      // ROUND BUTTON
      public static void InitStyle(uc_round_button Control)
      {
        float _font_size;
        Boolean _bold;

        _font_size = Control.Font.Size;
        _bold = false;
        Control.FlatAppearance.BorderSize = 0;

        // Set properties
        switch (Control.Style)
        {
          case uc_round_button.RoundButonStyle.NONE:
            return;

          case uc_round_button.RoundButonStyle.PRINCIPAL:
            Control.BackColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_6B83A1];
            Control.ForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_F4F6F9];
            Control.SelectedColor = Color.LightSteelBlue;
            Control.CornerRadius = 10;
            Control.Text = Control.Text.ToUpper();
            _font_size = 13f;
            break;

          case uc_round_button.RoundButonStyle.SECONDARY:
            Control.BackColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_3F4853];
            Control.ForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_F4F6F9];
            Control.SelectedColor = Color.LightSteelBlue;
            Control.CornerRadius = 10;
            Control.Text = Control.Text.ToUpper();
            _font_size = 13f;
            break;

          case uc_round_button.RoundButonStyle.TERTIARY:
            Control.BackColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_2D2F32];
            Control.ForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_F4F6F9];
            Control.SelectedColor = Color.LightSteelBlue;
            Control.CornerRadius = 10;
            Control.Text = Control.Text.ToUpper();
            _font_size = 13f;
            break;

          case uc_round_button.RoundButonStyle.CALC_MONEY:
            Control.BackColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_6B83A1];
            Control.ForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_F4F6F9];
            Control.CornerRadius = 0;
            _font_size = 24f;
            break;

          case uc_round_button.RoundButonStyle.CALC_NUMBER:
            Control.BackColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_7D7D7D];
            Control.ForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_F4F6F9];
            Control.CornerRadius = 0;
            _font_size = 24f;
            break;

          case uc_round_button.RoundButonStyle.CALC_BACKSPACE:
            Control.BackColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_3F4853];
            Control.ForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_F4F6F9];
            Control.CornerRadius = 0;
            Control.Image = Resources.Resource.calc_backspace;
            Control.ImageAlign = ContentAlignment.MiddleCenter;
            break;

          case uc_round_button.RoundButonStyle.CALC_ENTER:
            Control.BackColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_DE9931];
            Control.ForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_F4F6F9];
            Control.CornerRadius = 0;
            Control.Image = Resources.Resource.calc_enter;
            Control.ImageAlign = ContentAlignment.MiddleCenter;
            break;

          case uc_round_button.RoundButonStyle.MENU:
            Control.BackColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_373B3F];
            Control.ForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_F4F6F9];
            Control.CornerRadius = 0;
            Control.ImageAlign = ContentAlignment.MiddleRight;
            Control.TextAlign = ContentAlignment.MiddleLeft;
            Control.Text = Control.Text.ToUpper();
            Control.Margin = new Padding(0, 0, 0, 0);
            Control.Height = 48;
            Control.Width = 130;
            _font_size = 12f;
            _bold = true;
            break;

          case uc_round_button.RoundButonStyle.MENU_SELECTED:
            Control.BackColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_4C565F];
            Control.ForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_F4F6F9];
            Control.CornerRadius = 0;
            Control.ImageAlign = ContentAlignment.MiddleRight;
            Control.TextAlign = ContentAlignment.MiddleLeft;
            Control.Text = Control.Text.ToUpper();
            Control.Margin = new Padding(0, 0, 0, 0);
            Control.Height = 48;
            Control.Width = 130;
            _font_size = 12f;
            _bold = true;
            break;

          case uc_round_button.RoundButonStyle.SUB_MENU:
            Control.BackColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_373B3F];
            Control.ForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_F4F6F9];
            Control.CornerRadius = 0;
            Control.ImageAlign = ContentAlignment.MiddleRight;
            Control.TextAlign = ContentAlignment.MiddleLeft;
            Control.Text = Control.Text.ToUpper();
            Control.Margin = new Padding(0, 0, 0, 0);
            Control.Height = 48;
            Control.Width = 130;
            _font_size = 12f;
            _bold = true;
            break;

          case uc_round_button.RoundButonStyle.SUB_MENU_SELECTED:
            Control.BackColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_4C565F];
            Control.ForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_F4F6F9];
            Control.CornerRadius = 0;
            Control.ImageAlign = ContentAlignment.MiddleRight;
            Control.TextAlign = ContentAlignment.MiddleLeft;
            Control.Text = Control.Text.ToUpper();
            Control.Margin = new Padding(0, 0, 0, 0);
            Control.Height = 48;
            Control.Width = 130;
            _font_size = 12f;
            _bold = true;
            break;

          case uc_round_button.RoundButonStyle.TOP_BAR:
            Control.BackColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_5E7189];
            Control.ForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_FFFFFF];
            Control.CornerRadius = 0;
            Control.ImageAlign = ContentAlignment.MiddleCenter;
            Control.TextAlign = ContentAlignment.BottomCenter;
            Control.Text = Control.Text.ToUpper();
            Control.TextImageRelation = TextImageRelation.Overlay;
            Control.Margin = new Padding(0, 0, 0, 30);
            _font_size = 9f;
            _bold = true;
            break;

          case uc_round_button.RoundButonStyle.PAYMENT_METHOD:
            Control.BackColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_6B83A1];
            Control.ForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_F4F6F9];
            Control.SelectedColor = Color.LightSteelBlue;
            Control.CornerRadius = 10;
            Control.Text = Control.Text.ToUpper();
            _font_size = 9f;
            break;

          case uc_round_button.RoundButonStyle.PAYMENT_METHOD_SELECTED:
            Control.BackColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_3F4853];
            Control.ForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_F4F6F9];
            Control.CornerRadius = 10;
            Control.Text = Control.Text.ToUpper();
            _font_size = 9f;
            break;

          case uc_round_button.RoundButonStyle.GAMING_TABLE:
            Control.BackColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_505A66];
            Control.ForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_F4F6F9];
            Control.CornerRadius = 10;
            break;

          default:
            break;
        }

        // Set disabled style
        if (!Control.Enabled)
        {
          Control.BackColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_C8C8C8];
          Control.ForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_989898];
        }

        CashierStyle.Fonts.ChangeFontControl(Control, _font_size, _bold);
      }

      // ROUND TEXT BOX
      public static void InitStyle(uc_round_textbox Control)
      {
        float _font_size;
        Color _disabled_backcolor;
        Color _disabled_forecolor;
        Color _disabled_bordercolor;
        Color _disabled_watermarkcolor;

        _font_size = Control.Font.Size;
        _disabled_backcolor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_ECECEC];
        _disabled_forecolor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_ACACAC];
        _disabled_bordercolor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_ACACAC];
        _disabled_watermarkcolor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_ACACAC];

        // Set properties
        switch (Control.Style)
        {
          case uc_round_textbox.RoundTextBoxStyle.NONE:
            return;

          case uc_round_textbox.RoundTextBoxStyle.BASIC:
          case uc_round_textbox.RoundTextBoxStyle.MULTILINE:

            if (Control.Style == uc_round_textbox.RoundTextBoxStyle.MULTILINE)
            {
              Control.Multiline = true;
            }
            else
            {
              Control.Multiline = false;
              Control.Size = new Size(((uc_round_textbox)Control).Size.Width, 40);
            }

            Control.BackColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_E1E7E9];
            Control.ForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_373B3F];
            Control.BorderColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_898989];
            Control.WaterMarkColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_ACACAC];

            Control.CornerRadius = 5;

            _font_size = 14f;
            break;

          case uc_round_textbox.RoundTextBoxStyle.BASIC_CUSTOM:

            Control.BackColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_E1E7E9];
            Control.ForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_373B3F];
            Control.BorderColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_898989];
            Control.WaterMarkColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_ACACAC];

            Control.CornerRadius = 5;

            break;

          case uc_round_textbox.RoundTextBoxStyle.CALCULATOR:
            Control.BackColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_373B3F];
            Control.ForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_DE9931];
            Control.BorderColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_373B3F];
            Control.WaterMarkColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_DE9931];

            _disabled_backcolor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_373B3F];
            _disabled_forecolor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_5D5D5D];
            _disabled_bordercolor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_373B3F];
            _disabled_watermarkcolor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_5D5D5D];

            Control.CornerRadius = 0;

            _font_size = 40f;

            break;

          default:
            break;
        }

        CashierStyle.Fonts.ChangeFontControl(Control, _font_size);

        if (!Control.Enabled && !Control.ReadOnly)
        {
          Control.BackColor = _disabled_backcolor;
          Control.ForeColor = _disabled_forecolor;
          Control.BorderColor = _disabled_bordercolor;
          Control.WaterMarkColor = _disabled_watermarkcolor;
        }

      }

      // MASKED TEXT BOX
      public static void InitStyle(uc_maskedTextBox Control)
      {
        float _font_size;
        Color _disabled_backcolor;
        Color _disabled_forecolor;
        Color _disabled_bordercolor;

        _font_size = Control.Font.Size;
        _disabled_backcolor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_ECECEC];
        _disabled_forecolor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_ACACAC];
        _disabled_bordercolor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_ACACAC];

        // Set properties
        switch (Control.Style)
        {
          case uc_maskedTextBox.RoundMaskedTextBoxStyle.NONE:
            return;

          case uc_maskedTextBox.RoundMaskedTextBoxStyle.BASIC:

            Control.Size = new Size(Control.Size.Width, 40);

            Control.BackColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_E1E7E9];
            Control.ForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_373B3F];
            Control.BorderColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_898989];

            Control.CornerRadius = 5;

            _font_size = 14f;
            break;

          default:
            break;
        }

        CashierStyle.Fonts.ChangeFontControl(Control, _font_size);

        if (!Control.Enabled)
        {
          Control.BackColor = _disabled_backcolor;
          Control.ForeColor = _disabled_forecolor;
          Control.BorderColor = _disabled_bordercolor;
        }

      }

      // LABEL
      public static void InitStyle(uc_label Control)
      {
        float _font_size;
        Padding _margin;
        Boolean _bold;

        _font_size = Control.Font.Size;
        _bold = false;

        // Set properties
        switch (Control.Style)
        {
          case uc_label.LabelStyle.NONE:
            _font_size = Control.Font.Size;
            break;
          // Tittle form
          case uc_label.LabelStyle.USER_NAME:
            Control.ForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_FFFFFF];
            _font_size = 16f;
            _bold = true;
            break;

          case uc_label.LabelStyle.ROL:
            Control.ForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_82D2E3];
            _font_size = 14f;
            break;

          case uc_label.LabelStyle.STATUS_CONNECTED:
            ((uc_label)Control).ForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_09DC09];
            _font_size = 14f;
            _bold = true;
            break;

          case uc_label.LabelStyle.STATUS_DISCONNECTED:
            ((uc_label)Control).ForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_CC0000];
            _font_size = 14f;
            _bold = true;
            break;

          case uc_label.LabelStyle.STATUS_WARNING:
            ((uc_label)Control).ForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_FF8C00];
            _font_size = 14f;
            _bold = true;
            break;

          case uc_label.LabelStyle.DATE:
            ((uc_label)Control).ForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_FFFFFF];
            ((uc_label)Control).Text = ((uc_label)Control).Text.ToUpper();
            _bold = true;
            _font_size = 12f;
            break;

          case uc_label.LabelStyle.TIME:
            Control.ForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_FFFFFF];
            _font_size = 14f;
            _bold = true;
            break;

          case uc_label.LabelStyle.TERMINAL:
            ((uc_label)Control).ForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_FFFFFF];
            ((uc_label)Control).TextAlign = ContentAlignment.TopRight;
            ((uc_label)Control).Text = ((uc_label)Control).Text.ToUpper();
            _font_size = 14f;
            _bold = true;
            break;

          //........................................................................................................................
          case uc_label.LabelStyle.BOX_TITLE:
            Control.ForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_F4F6F9];
            Control.Text = Control.Text.ToUpper();
            _font_size = 14f;
            break;

          case uc_label.LabelStyle.BOX_TITLE_WITH_DATA_UP:
            Control.ForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_F4F6F9];
            Control.Text = Control.Text.ToUpper();
            _font_size = 14f;
            break;

          case uc_label.LabelStyle.BOX_TITLE_WITH_DATA_DOWN:
            Control.ForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_F4F6F9];
            Control.Text = Control.Text.ToUpper();
            _font_size = 36f;
            break;

          case uc_label.LabelStyle.TABLE_HEADER:
            Control.ForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_F4F6F9];
            _font_size = 14f;
            break;

          case uc_label.LabelStyle.POPUP_HEADER:
            Control.BackColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_3F4853];
            Control.ForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_A7DDF1];
            _margin = Control.Padding;
            _margin.Top = 6;
            _font_size = 18f;
            break;

          case uc_label.LabelStyle.POPUP_FULL_SCREEN_LEFT:
            Control.ForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_A7DDF1];
            _font_size = 18f;
            break;

          case uc_label.LabelStyle.POPUP_FULL_SCREEN_PLAYER:
            Control.ForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_F4F6F9];
            _font_size = 18f;
            break;


          // .......................................................................................................................
          case uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK:
            Control.ForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_333333];
            _font_size = 16f;

            break;
          case uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLUE:
            Control.ForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_3D8DC1];
            _font_size = 16f;

            break;
          case uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE:
            Control.ForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_3D8DC1];
            _font_size = 14f;

            break;
          case uc_label.LabelStyle.TEXT_INFO_8PX_BLACK_BOLD:
            Control.ForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_333333];
            _bold = true;
            _font_size = 8f;

            break;
          case uc_label.LabelStyle.TEXT_INFO_10PX_BLACK:
            Control.ForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_333333];
            _font_size = 10f;

            break;
          case uc_label.LabelStyle.TEXT_INFO_12PX_BLACK:
            Control.ForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_333333];
            _font_size = 12f;

            break;
          case uc_label.LabelStyle.TEXT_INFO_12PX_BLACK_BOLD:
            Control.ForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_333333];
            _bold = true;
            _font_size = 12f;

            break;
          case uc_label.LabelStyle.TEXT_INFO_12PX_BLUE:
            Control.ForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_3D8DC1];
            _font_size = 12f;

            break;
          case uc_label.LabelStyle.TEXT_INFO_14PX_BLACK:
            Control.ForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_333333];
            _font_size = 14f;

            break;
          case uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD:
            Control.ForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_333333];
            _bold = true;
            _font_size = 14f;

            break;
          case uc_label.LabelStyle.TEXT_INFO_14PX_RED:
            Control.ForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_CC0000];
            _bold = true;
            _font_size = 14f;

            break;
          case uc_label.LabelStyle.TEXT_INFO_14PX_GREEN:
            Control.ForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_09DC09];
            _bold = true;
            _font_size = 14f;

            break;

          case uc_label.LabelStyle.TEXT_INFO_14PX_BLUE:
            Control.ForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_3D8DC1];
            _bold = true;
            _font_size = 14f;

            break;

          case uc_label.LabelStyle.TEXT_INFO_14PX_WHITE:
            Control.ForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_F4F6F9];
            _font_size = 14f;

            break;

          case uc_label.LabelStyle.TEXT_INFO_14PX_WHITE_BOLD:
            Control.ForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_F4F6F9];
            _bold = true;
            _font_size = 14f;

            break;

          case uc_label.LabelStyle.TEXT_INFO_32PX_WHITE:
            Control.ForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_F4F6F9];
            _font_size = 32f;

            break;
          case uc_label.LabelStyle.TEXT_INFO_32PX_BLACK:
            Control.ForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_333333];
            _font_size = 32f;

            break;
          case uc_label.LabelStyle.TEXT_INFO_32PX_BLACK_BOLD:
            Control.ForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_333333];
            _bold = true;
            _font_size = 32f;

            break;

          case uc_label.LabelStyle.TITLE_PRINCIPAL_24PX_BLACK:
            Control.ForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_333333];
            _font_size = 24f;

            break;
          default:
            Control.ForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_333333];
            _font_size = 14f;
            break;
        }

        if (!Control.Enabled)
        {
          Control.ForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_ACACAC];
        }

        CashierStyle.Fonts.ChangeFontControl(Control, _font_size, _bold);
      }

      // RADIO BUTTON
      public static void InitStyle(uc_radioButton Control)
      {
        float _font_size;

        _font_size = Control.Font.Size;

        // Set properties
        Control.ForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_333333];
        _font_size = 14f;

        if (!Control.Enabled)
        {
          Control.ForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_ACACAC];
        }

        CashierStyle.Fonts.ChangeFontControl(Control, _font_size);
      }

      // CHECK BOX
      public static void InitStyle(uc_checkBox Control)
      {
        float _font_size;

        _font_size = Control.Font.Size;

        // Set properties
        Control.ForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_333333];
        _font_size = 14f;

        if (!Control.Enabled)
        {
          Control.ForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_ACACAC];
        }

        CashierStyle.Fonts.ChangeFontControl(Control, _font_size, Control.Font.Bold);
      }

      // PANEL
      public static void InitStyle(uc_round_panel Control)
      {
        float _font_size;

        _font_size = Control.Font.Size;

        // Set properties
        switch (Control.Style)
        {
          case uc_round_panel.RoundPanelStyle.NONE:
            return;

          case uc_round_panel.RoundPanelStyle.NORMAL:
            Control.HeaderHeight = 35;
            Control.CornerRadius = 10;
            Control.HeaderBackColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_51585F];
            Control.HeaderForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_F4F6F9];
            Control.PanelBackColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_F4F6F9];

            break;

          case uc_round_panel.RoundPanelStyle.NORMAL_WHITE:
            Control.HeaderHeight = 35;
            Control.CornerRadius = 10;
            Control.HeaderBackColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_51585F];
            Control.HeaderForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_F4F6F9];
            Control.PanelBackColor = Color.White;

            break;


          case uc_round_panel.RoundPanelStyle.DOUBLE:
            Control.HeaderHeight = 80;
            Control.CornerRadius = 10;
            Control.HeaderBackColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_51585F];
            Control.HeaderForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_F4F6F9];
            Control.PanelBackColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_F4F6F9];

            _font_size = 14f;

            break;
          case uc_round_panel.RoundPanelStyle.TOTAL:
            Control.HeaderHeight = 35;
            Control.CornerRadius = 10;
            Control.HeaderBackColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_6B83A1];
            Control.HeaderForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_F4F6F9];
            Control.PanelBackColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_373B3F];

            _font_size = 14f;

            break;
          case uc_round_panel.RoundPanelStyle.WITHOUT_HEAD:
            Control.HeaderHeight = 0;
            Control.CornerRadius = 10;
            Control.PanelBackColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_F4F6F9];
            _font_size = 14f;

            break;

          case uc_round_panel.RoundPanelStyle.ONLY_BORDER:
            Control.HeaderHeight = 0;
            Control.CornerRadius = 0;
            Control.BorderColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_51585F];
            Control.PanelBackColor = Color.Transparent;

            _font_size = 14f;
            break;

          case uc_round_panel.RoundPanelStyle.GAMING_TABLE:
            Control.HeaderHeight = 0;
            Control.CornerRadius = 20;
            Control.BackColor = Color.Transparent;
            Control.ForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_F4F6F9];
            Control.BorderStyle = BorderStyle.None;
            Control.PanelBackColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_3F4853];

            _font_size = 14f;
            break;

          case uc_round_panel.RoundPanelStyle.MIN_HEAD:
            Control.HeaderHeight = 18;
            Control.CornerRadius = 10;
            Control.HeaderBackColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_51585F];
            Control.HeaderForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_F4F6F9];
            Control.PanelBackColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_F4F6F9];
            _font_size = 12f;

            break;
          default:
            Control.HeaderBackColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_51585F];
            Control.HeaderForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_F4F6F9];
            Control.PanelBackColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_F4F6F9];
            _font_size = 14f;
            break;
        }

        CashierStyle.Fonts.ChangeFontControl(Control, _font_size);
      }

      // LIST CONTROL
      public static void InitStyle(uc_round_list_box Control)
      {
        float _font_size;

        _font_size = Control.Font.Size;

        // Set properties
        switch (Control.Style)
        {
          case uc_round_list_box.RoundListBoxStyle.NONE:
            return;

          case uc_round_list_box.RoundListBoxStyle.BASIC:
            Control.BackColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_E1E7E9];
            Control.ForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_3B4954];
            Control.BorderColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_898989];
            Control.CornerRadius = 0;
            Control.ItemHeight = 40;

            _font_size = 14f;
            break;

          case uc_round_list_box.RoundListBoxStyle.COMBOBOX_LIST:
            Control.BackColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_E1E7E9];
            Control.ForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_3B4954];
            Control.BorderColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_898989];
            Control.CornerRadius = 5;

            _font_size = 14f;
            break;

          case uc_round_list_box.RoundListBoxStyle.COMBOBOX_LIST_CUSTOM:
            Control.BackColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_E1E7E9];
            Control.ForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_3B4954];
            Control.BorderColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_898989];
            Control.CornerRadius = 5;

            _font_size = Control.Font.Size;
            break;

          default:
            break;
        }

        CashierStyle.Fonts.ChangeFontControl(Control, _font_size);

        if (!Control.Enabled)
        {
          Control.BackColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_ECECEC];
          Control.ForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_ACACAC];
          Control.BorderColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_ACACAC];
        }

      }

      // COMBO BOX
      public static void InitStyle(uc_round_combobox Control)
      {
        float _font_size;

        _font_size = Control.Font.Size;

        // Set properties
        switch (Control.Style)
        {
          case uc_round_combobox.RoundComboBoxStyle.NONE:
            return;

          case uc_round_combobox.RoundComboBoxStyle.BASIC:
            Control.BackColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_E1E7E9];
            Control.ForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_3B4954];
            Control.BorderColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_898989];
            Control.ArrowColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_3A4954];
            Control.CornerRadius = 5;

            _font_size = 14f;
            break;

          case uc_round_combobox.RoundComboBoxStyle.CUSTOM:
            Control.BackColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_E1E7E9];
            Control.ForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_3B4954];
            Control.BorderColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_898989];
            Control.ArrowColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_3A4954];
            Control.CornerRadius = 5;

            _font_size = Control.Font.Size;
            break;

          default:
            break;
        }

        CashierStyle.Fonts.ChangeFontControl(Control, _font_size);

        if (!Control.Enabled)
        {
          Control.BackColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_ECECEC];
          Control.ForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_ACACAC];
          Control.BorderColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_ACACAC];
          Control.ArrowColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_D7D7D7];
        }

      }

      // DATA GRID
      public static void InitStyle(uc_DataGridView Control)
      {

        float _font_size;

        _font_size = Control.Font.Size;

        Control.CornerRadius = 10;
        Control.DefaultCellSelectionBackColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_373B3F];
        Control.DefaultCellSelectionForeColor = Color.White;
        Control.DefaultCellBackColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_E1E7E9];
        Control.DefaultCellForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_333333];

        Control.EnableHeadersVisualStyles = false;
        Control.HeaderBorderStyle = DataGridViewHeaderBorderStyle.None;
        Control.HeaderDefaultCellBackColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_6B83A1];
        Control.HeaderDefaultCellForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_F4F6F9];
        Control.BorderColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_7D7D7D];

        // Set properties
        switch (Control.Style)
        {
          case uc_DataGridView.DataGridViewStyle.NONE:
            return;

          case uc_DataGridView.DataGridViewStyle.WITH_ALL_ROUND_CORNERS:
            Control.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            Control.ColumnHeadersHeight = 35;
            Control.RowTemplateHeight = 35;

            break;
          case uc_DataGridView.DataGridViewStyle.WITH_TOP_ROUND_BORDERS:
            Control.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            Control.ColumnHeadersHeight = 35;
            Control.RowTemplateHeight = 35;

            break;
          case uc_DataGridView.DataGridViewStyle.WITH_BOTTOM_ROUND_BORDERS:
            Control.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            Control.ColumnHeadersHeight = 35;
            Control.RowTemplateHeight = 35;

            break;
          case uc_DataGridView.DataGridViewStyle.WITHOUT_ROUND_BORDERS:
            Control.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            Control.ColumnHeadersHeight = 35;
            Control.RowTemplateHeight = 35;

            break;
          case uc_DataGridView.DataGridViewStyle.PROMOTIONS:
            Control.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            Control.ColumnHeadersHeight = 35;
            Control.RowTemplateHeight = 45;

            break;
          default:

            _font_size = 14f;
            break;
        }

        CashierStyle.Fonts.ChangeFontControl(Control, _font_size);

        Control.ColumnHeadersDefaultCellStyle.Font = Control.Font;
        Control.RowHeadersDefaultCellStyle.Font = Control.Font;

        foreach (DataGridViewColumn c in Control.Columns)
        {
          c.DefaultCellStyle.Font = Control.Font;
        }

        if (!Control.Enabled)
        {
          Control.DefaultCellSelectionBackColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_373B3F];
          Control.DefaultCellSelectionForeColor = Color.White;
          Control.DefaultCellBackColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_E1E7E9];
          Control.DefaultCellForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_ACACAC];

          Control.HeaderDefaultCellBackColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_ACACAC];
          Control.HeaderDefaultCellForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_858585];
        }

      }

      // TAB CONTROL
      public static void InitStyle(uc_round_tab_control Control)
      {
        float _font_size;

        _font_size = 14;

        // Set properties
        Control.ItemSize = new Size(Control.Height, 40);
        Control.DisplayStyleProvider.TextColor = CashierStyle.Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_F4F6F9];
        Control.DisplayStyleProvider.TextColorSelected = CashierStyle.Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_F4F6F9];
        Control.DisplayStyleProvider.TabColorSelected = CashierStyle.Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_3F4853];
        Control.DisplayStyleProvider.TabColorHeader = CashierStyle.Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_6B83A1];
        Control.DisplayStyleProvider.TabColor = CashierStyle.Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_F4F6F9];
        Control.DisplayStyleProvider.Radius = 10;
        Control.DisplayStyleProvider.FocusTrack = true;

        foreach (TabPage _page in Control.TabPages)
        {
          _page.BackColor = CashierStyle.Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_F4F6F9];
        }

        CashierStyle.Fonts.ChangeFontControl(Control, _font_size);

      }

      // MESSAGE
      public static void InitStyle(frm_message Form, frm_message.MessageStyle Type)
      {
        float _font_size;

        _font_size = Form.Font.Size;

        Form.TransparentBackColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_7E7E7E]; //Color.FromArgb(178, Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_7E7E7E]);
        Form.LabelHeight = 51;
        Form.FormBackColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_F4F6F9];
        Form.LabelBackColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_3F4853];
        Form.ForeColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_333333];
        Form.FormWidth = 512;
        Form.ImageSize = new Size(32, 32);

        // Set properties
        switch (Type)
        {
          case frm_message.MessageStyle.NONE:
            return;

          case frm_message.MessageStyle.TWO_BUTTON:
            _font_size = 14f;
            break;

          case frm_message.MessageStyle.THREE_BUTTON:
            _font_size = 14f;
            break;

          default:
            break;
        }

        CashierStyle.Fonts.ChangeFontControl(Form, _font_size);

      }

      // FORM
      public static void InitStyle(frm_base Form)
      {
        switch (Form.Style)
        {
          case frm_base.BaseFormStyle.NONE:
            break;
          case frm_base.BaseFormStyle.PRINCIPAL:
            Form.HeaderColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_3F4853];
            Form.BackColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_E3E7E9];

            break;
          default:
            break;
        }
      }

      // MENU BAR
      public static void InitStyle(uc_menu Control, uc_menu.MenuStyle Type)
      {
        float _font_size;

        _font_size = Control.Font.Size;

        // Set properties
        switch (Type)
        {
          case uc_menu.MenuStyle.NONE:
            return;

          case uc_menu.MenuStyle.MAIN_MENU:
            Control.BackColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_373B3F];
            Control.ShowLogo = true;
            Control.AutoAdjustHeight = false;
            Control.Size = new Size(130, 721);
            Control.AutoCollapsed = false;

            _font_size = 16f;
            break;

          case uc_menu.MenuStyle.SUB_MENU:
            Control.BackColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_373B3F];
            Control.ShowLogo = false;
            Control.AutoAdjustHeight = true;
            Control.Size = new Size(130, 50);
            Control.AutoCollapsed = false;

            _font_size = 16f;
            break;

          default:
            break;
        }

        CashierStyle.Fonts.ChangeFontControl(Control, _font_size, true);
      }

      // TOP BAR
      public static void InitStyle(uc_top_bar Control)
      {
        float _font_size;

        _font_size = Control.Font.Size;

        // Set properties
        ((uc_top_bar)Control).BackColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_6B83A1];
        //((uc_top_bar)Control).Size = new Size(1024, 55);
        ((uc_top_bar)Control).Height = 55;

        _font_size = 14f;

        CashierStyle.Fonts.ChangeFontControl(Control, _font_size, true);
      }

      // UC CARD 
      public static void InitStyle(uc_card Control, uc_card.CardStyle Type)
      {
        Control.LineSeparatorColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_959595];
        // Set properties
        switch (Type)
        {
          case uc_card.CardStyle.DEFAULT:
          default:
            ((uc_card)Control).BackColor = Colors.m_colors_dic[Colors.ENUM_COLORS.HEX_E3E7E9];

            break;
        }
      }

    }

    public static class Colors
    {
      public enum ENUM_COLORS
      {

        HEX_009C2F = 1,
        HEX_00BBF9 = 2,
        HEX_373B3F = 3,
        HEX_3F4853 = 4,
        HEX_51585F = 5,
        HEX_618BBF = 6,
        HEX_6B83A1 = 7,
        HEX_7D7D7D = 8,
        HEX_989898 = 9,
        HEX_A7DDF1 = 10,
        HEX_C8C8C8 = 11,
        HEX_D0D3D8 = 12,
        HEX_DE9931 = 13,
        HEX_E3E7E9 = 14,
        HEX_EC5D00 = 15,
        HEX_F4F6F9 = 16,
        HEX_FFCB00 = 17,
        HEX_FFFFFF = 18,
        HEX_82D2E3 = 19,
        HEX_333333 = 20,
        HEX_3D8DC1 = 21,
        HEX_898989 = 22,
        HEX_E1E7E9 = 23,
        HEX_3B4954 = 24,
        HEX_CED3D8 = 25,
        HEX_7E7E7E = 26,
        HEX_2D2F32 = 27,
        HEX_4C565F = 28,
        HEX_6B7279 = 29,
        HEX_CC0000 = 30,
        HEX_09DC09 = 31,
        HEX_5E7189 = 32,
        HEX_535B65 = 33,
        HEX_959595 = 34,
        HEX_78A3C2 = 35,
        HEX_ACACAC = 36,
        HEX_ECECEC = 37,
        HEX_5D5D5D = 38,
        HEX_858585 = 39,
        HEX_BCBCBC = 40,
        HEX_505A66 = 41,
        HEX_D7D7D7 = 42,
        HEX_3A4954 = 43,
        HEX_FFBC00 = 44,
        HEX_00A651 = 45,
        HEX_FF8C00 = 46,
      }

      public static Dictionary<ENUM_COLORS, Color> m_colors_dic = InitControls();

      public static Dictionary<ENUM_COLORS, Color> InitControls()
      {
        if (m_colors_dic == null)
        {
          m_colors_dic = new Dictionary<ENUM_COLORS, Color>();

          m_colors_dic.Add(ENUM_COLORS.HEX_009C2F, Color.FromArgb(0x00, 0x9C, 0x2F));
          m_colors_dic.Add(ENUM_COLORS.HEX_00BBF9, Color.FromArgb(0x00, 0xBB, 0xF9));
          m_colors_dic.Add(ENUM_COLORS.HEX_373B3F, Color.FromArgb(0x37, 0x3B, 0x3F));
          m_colors_dic.Add(ENUM_COLORS.HEX_3F4853, Color.FromArgb(0x3F, 0x48, 0x53));
          m_colors_dic.Add(ENUM_COLORS.HEX_51585F, Color.FromArgb(0x51, 0x58, 0x5F));
          m_colors_dic.Add(ENUM_COLORS.HEX_618BBF, Color.FromArgb(0x61, 0x8B, 0xBF));
          m_colors_dic.Add(ENUM_COLORS.HEX_6B83A1, Color.FromArgb(0x6B, 0x83, 0xA1));
          m_colors_dic.Add(ENUM_COLORS.HEX_7D7D7D, Color.FromArgb(0x7D, 0x7D, 0x7D));
          m_colors_dic.Add(ENUM_COLORS.HEX_989898, Color.FromArgb(0x98, 0x98, 0x98));
          m_colors_dic.Add(ENUM_COLORS.HEX_A7DDF1, Color.FromArgb(0xA7, 0xDD, 0xF1));
          m_colors_dic.Add(ENUM_COLORS.HEX_C8C8C8, Color.FromArgb(0xC8, 0xC8, 0xC8));
          m_colors_dic.Add(ENUM_COLORS.HEX_D0D3D8, Color.FromArgb(0xD0, 0xD3, 0xD8));
          m_colors_dic.Add(ENUM_COLORS.HEX_DE9931, Color.FromArgb(0xDE, 0x99, 0x31));
          m_colors_dic.Add(ENUM_COLORS.HEX_E3E7E9, Color.FromArgb(0xE3, 0xE7, 0xE9));
          m_colors_dic.Add(ENUM_COLORS.HEX_EC5D00, Color.FromArgb(0xEC, 0x5D, 0x00));
          m_colors_dic.Add(ENUM_COLORS.HEX_F4F6F9, Color.FromArgb(0xF4, 0xF6, 0xF9));
          m_colors_dic.Add(ENUM_COLORS.HEX_FFCB00, Color.FromArgb(0xFF, 0xCB, 0x00));

          m_colors_dic.Add(ENUM_COLORS.HEX_FFFFFF, Color.FromArgb(0xFF, 0xFF, 0xFF));
          m_colors_dic.Add(ENUM_COLORS.HEX_82D2E3, Color.FromArgb(0x82, 0xD2, 0xE3));
          m_colors_dic.Add(ENUM_COLORS.HEX_333333, Color.FromArgb(0x33, 0x33, 0x33));
          m_colors_dic.Add(ENUM_COLORS.HEX_3D8DC1, Color.FromArgb(0x3D, 0x8D, 0xC1));

          // TEXTBOX
          m_colors_dic.Add(ENUM_COLORS.HEX_898989, Color.FromArgb(0x89, 0x89, 0x89));
          m_colors_dic.Add(ENUM_COLORS.HEX_E1E7E9, Color.FromArgb(0xE1, 0xE7, 0xE9));
          m_colors_dic.Add(ENUM_COLORS.HEX_3B4954, Color.FromArgb(0x3B, 0x49, 0x54));

          m_colors_dic.Add(ENUM_COLORS.HEX_CED3D8, Color.FromArgb(0xCE, 0xD3, 0xD8));
          m_colors_dic.Add(ENUM_COLORS.HEX_7E7E7E, Color.FromArgb(0x7E, 0x7E, 0x7E));
          m_colors_dic.Add(ENUM_COLORS.HEX_4C565F, Color.FromArgb(0x4C, 0x56, 0x5F));
          m_colors_dic.Add(ENUM_COLORS.HEX_6B7279, Color.FromArgb(0x6B, 0x72, 0x79));

          m_colors_dic.Add(ENUM_COLORS.HEX_2D2F32, Color.FromArgb(0x2D, 0x2F, 0x32));

          m_colors_dic.Add(ENUM_COLORS.HEX_CC0000, Color.FromArgb(0xCC, 0x00, 0x00));
          m_colors_dic.Add(ENUM_COLORS.HEX_09DC09, Color.FromArgb(0x09, 0xDC, 0x09));
          m_colors_dic.Add(ENUM_COLORS.HEX_5E7189, Color.FromArgb(0x5E, 0x71, 0x89));
          m_colors_dic.Add(ENUM_COLORS.HEX_535B65, Color.FromArgb(0x53, 0x5B, 0x65));
          m_colors_dic.Add(ENUM_COLORS.HEX_78A3C2, Color.FromArgb(0x78, 0xA3, 0xC2));


          m_colors_dic.Add(ENUM_COLORS.HEX_959595, Color.FromArgb(0x95, 0x95, 0x95));

          m_colors_dic.Add(ENUM_COLORS.HEX_ACACAC, Color.FromArgb(0xac, 0xac, 0xac));
          m_colors_dic.Add(ENUM_COLORS.HEX_ECECEC, Color.FromArgb(0xec, 0xec, 0xec));
          m_colors_dic.Add(ENUM_COLORS.HEX_5D5D5D, Color.FromArgb(0x5d, 0x5d, 0x5d));
          m_colors_dic.Add(ENUM_COLORS.HEX_858585, Color.FromArgb(0x85, 0x85, 0x85));
          m_colors_dic.Add(ENUM_COLORS.HEX_BCBCBC, Color.FromArgb(0xbc, 0xbc, 0xbc));
          m_colors_dic.Add(ENUM_COLORS.HEX_505A66, Color.FromArgb(0x50, 0x5a, 0x66));
          m_colors_dic.Add(ENUM_COLORS.HEX_D7D7D7, Color.FromArgb(0xd7, 0xd7, 0xd7));
          m_colors_dic.Add(ENUM_COLORS.HEX_3A4954, Color.FromArgb(0x3a, 0x49, 0x54));

          m_colors_dic.Add(ENUM_COLORS.HEX_FFBC00, Color.FromArgb(0xff, 0xbc, 0x00));
          m_colors_dic.Add(ENUM_COLORS.HEX_00A651, Color.FromArgb(0x00, 0xa6, 0x51));
          m_colors_dic.Add(ENUM_COLORS.HEX_FF8C00, Color.FromArgb(0xff, 0x8C, 0x00));
          
        }

        return m_colors_dic;
      }

      public static String HexConverter(System.Drawing.Color c)
      {
        return "#" + c.R.ToString("X2") + c.G.ToString("X2") + c.B.ToString("X2");
      }
    }

    public static class Fonts
    {
      [DllImport("gdi32.dll")]
      private static extern IntPtr AddFontMemResourceEx(IntPtr pbFont, uint cbFont, IntPtr pdv, [In] ref uint pcFonts);

      private static FontFamily _ff_system_regular = LoadFont(Resources.Resource.OpenSans_Semibold  );
      private static FontFamily _ff_system_bold = LoadFont(Resources.Resource.OpenSans_Bold);
       
      public static void ChangeFontControl(Control Control, float Size)
      {
        ChangeFontControl(Control, Size, false);
      }

      public static Font ChangeFontSize(float Size, Boolean IsBold)
      {
        if (IsBold)
        {
          return new System.Drawing.Font(_ff_system_bold, Size, FontStyle.Bold, GraphicsUnit.Pixel);
        }
        else
        {
          return new System.Drawing.Font(_ff_system_regular, Size, FontStyle.Regular, GraphicsUnit.Pixel);
        }
      }

      private static FontFamily LoadFont(byte[] Font)
      {
        FontFamily _font_family;
        int dataLength = Font.Length;

        IntPtr ptrData = Marshal.AllocCoTaskMem(dataLength);
        Marshal.Copy(Font, 0, ptrData, dataLength);

        uint cFonts = 0;
        AddFontMemResourceEx(ptrData, (uint)Font.Length, IntPtr.Zero, ref cFonts);

        PrivateFontCollection pfc = new PrivateFontCollection();
        pfc.AddMemoryFont(ptrData, dataLength);

        Marshal.FreeCoTaskMem(ptrData);

        return _font_family = pfc.Families[0];
      }

      public static void ChangeFontControl(Control Control, float Size, Boolean Bold)
      {
        if (Bold)
        {
          Control.Font = new Font(_ff_system_bold, Size, FontStyle.Bold, GraphicsUnit.Pixel);
        }
        else
        {
          Control.Font = new Font(_ff_system_regular, Size, FontStyle.Regular, GraphicsUnit.Pixel);
        }
      }
    }

    public static class Image
    {
      public static byte[] ImageToByteArray(System.Drawing.Image imageIn)
      {
        MemoryStream ms = new MemoryStream();
        imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
        return ms.ToArray();
      } // ImageToByteArray

    }

  }
}
