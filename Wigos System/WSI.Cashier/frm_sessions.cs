//------------------------------------------------------------------------------
// Copyright � 2007-2008 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_sessions.cs
// 
//   DESCRIPTION: Implements the active sessions form.
//
//        AUTHOR: David C.
// 
// CREATION DATE: 22-AUG-2007
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 22-AUG-2007 DC     First release.
// 18-SEP-2017 DPC    WIGOS-5268: Cashier & GUI - Icons - Implement
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using WSI.Common;

namespace WSI.Cashier
{
  public partial class frm_sessions : Controls.frm_base_icon
  {
    static readonly int DEFAULT_INACTIVITY = 4000;

    #region Attributes

    private frm_yesno form_yes_no;
    SqlConnection conn;
    DataSet ds;
    DataTable dt_sessions = new DataTable();
    DataTable dt_plays = new DataTable();
    DataTable dt_terminals = new DataTable();
    string sql_terminals;
    string filtered = " WHERE ps_status = 0 ";
    bool no_data = false;

    private const int COL_TERMINAL_ID = 0;
    private const int COL_TERMINAL_NAME = 1;
    private const int COL_WS_STARTED = 2;
    private const int COL_LAYOUT_INACTIVITY = 3;
    private const int COL_INACTIVITY = 4;
    private const int COL_PS_STARTED = 5;
    private const int COL_TRACK_NUMBER = 6;
    private const int COL_PLAYED_COUNT = 7;
    private const int COL_MAX_PLAY = 8;
    private const int COL_PLAYS_INACTIVITY = 9;
    private const int COL_LAYOUT_PLAYS_INACT = 10;

    #endregion

    #region Constructor

    /// <summary>
    /// Constructor method.
    /// 1. Initialize controls.
    /// 2. Initialize control�s resources. (NLS, Images, etc)
    /// </summary>
    public frm_sessions()
    {
      InitializeComponent();

      InitializeControlResources();

      form_yes_no = new frm_yesno();
      form_yes_no.Opacity = 0.6;
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Initialize control resources (NLS strings, images, etc)
    /// </summary>
    private void InitializeControlResources()
    {
      // - NLS Strings:
      //   - Title: External set
      lbl_sessions_title.Text = Resource.String("STR_FRM_SESSIONS_TITLE");

      //   - Labels

      //   - Buttons
      btn_close.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CLOSE");

      // - Images:

    } // InitializeControlResources

    private void frm_sessions_Load(object sender, EventArgs e)
    {
      dg_sessions.SuspendLayout();

      try
      {
        LoadDataGrid();
      }
      catch (Exception _ex)
      {

        Misc.WriteLog("[FORM CLOSE] frm_sessions_load (exception)", Log.Type.Message);
        this.Close();
        Log.Exception(_ex);
        frm_message.Show(Resource.String("STR_GENERIC_ERROR_GETTING_DATA"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this.ParentForm);

        return;
      }

      dg_sessions.Columns[4].Visible = false;
      dg_sessions.Visible = true;
      Intervals();
      dg_sessions.ResumeLayout();

      dg_sessions.Columns[COL_TERMINAL_NAME].Width = 150;
      dg_sessions.Columns[COL_WS_STARTED].Width = 120;
      dg_sessions.Columns[COL_LAYOUT_INACTIVITY].Width = 100;
      dg_sessions.Columns[COL_LAYOUT_INACTIVITY].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
      dg_sessions.Columns[COL_PS_STARTED].Width = 120;
      dg_sessions.Columns[COL_TRACK_NUMBER].Width = 135;
      dg_sessions.Columns[COL_PLAYED_COUNT].Width = 80;
      dg_sessions.Columns[COL_PLAYED_COUNT].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
      dg_sessions.Columns[COL_MAX_PLAY].Width = 120;
      dg_sessions.Columns[COL_LAYOUT_PLAYS_INACT].Width = 110;
      dg_sessions.Columns[COL_LAYOUT_PLAYS_INACT].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
      dg_sessions.Columns[COL_TERMINAL_ID].Visible = false;
      dg_sessions.Columns[COL_PLAYS_INACTIVITY].Visible = false;
      dg_sessions.Sort(dg_sessions.Columns[1], ListSortDirection.Ascending);

      dg_sessions.Width = 958;

      dg_sessions.Location = new Point(this.Width / 2 - dg_sessions.Width / 2, this.Height / 2 - dg_sessions.Height / 2 - 10);

      this.timer1.Interval = 2000;
      timer1.Start();
    }

    /// <summary>
    /// Fill datagrid with active sessions.
    /// </summary>
    private void LoadDataGrid()
    {
      SqlDataAdapter da;
      DataColumn[] dc;
      DataColumn pk;
      conn = WGDB.Connection();
      ds = new DataSet();
      da = new SqlDataAdapter();
      dc = new DataColumn[1];
      pk = new DataColumn();

      // Table Play_Sessions
      string sessions = "";
      dt_sessions.TableName = "Playsessions";
      dt_sessions.Columns.Add("terminal_id");
      dt_sessions.Columns.Add("max_ps_id");
      dt_sessions.Columns.Add("ps_started");
      dt_sessions.Columns.Add("ac_track");
      dt_sessions.Columns.Add("played_count");
      sessions = Load_DataTableSessions();
      if (sessions == "")
      {
        no_data = true;
      }
      ds.Tables.Add(dt_sessions);

      // Table Plays
      string terminals = "";
      dt_plays.TableName = "Plays";
      dt_plays.Columns.Add("terminal_id");
      dt_plays.Columns.Add("CardInserted");
      dt_plays.Columns.Add("LastPlayDate");

      if (!no_data)
      {
        terminals = Load_DataTablePlays(sessions);
      }

      if (terminals == "")
      {
        no_data = true;
      }
      ds.Tables.Add(dt_plays);

      // Table terminals
      dt_terminals.TableName = "Terminals";
      dt_terminals.Columns.Add("terminal_id");
      dt_terminals.Columns.Add("Terminal");
      dt_terminals.Columns.Add("LastMessage");
      dt_terminals.Columns.Add("inact");
      if (!no_data)
      {
        Load_DataTableTerminals(terminals);
      }

      // Add datagrid columns
      dt_terminals.Columns.Add("Inactivity");
      dt_terminals.Columns.Add("ps_started");
      dt_terminals.Columns.Add("ac_track");
      dt_terminals.Columns.Add("played_count");
      dt_terminals.Columns.Add("max_play");
      dt_terminals.Columns.Add("inact_plays");
      dt_terminals.Columns.Add("PlaysInactivity");
      dt_terminals.Columns["Inactivity"].SetOrdinal(3);

      ds.Tables.Add(dt_terminals);

      dc[0] = dt_terminals.Columns["terminal_id"];
      ds.Tables["Terminals"].PrimaryKey = dc;

      dg_sessions.DataSource = ds.Tables["Terminals"];

      // Headers
      dg_sessions.Columns[COL_TERMINAL_NAME].HeaderText = Resource.String("STR_FRM_SESSIONS_DATAGRID_TERMINAL_NAME");
      dg_sessions.Columns[COL_WS_STARTED].HeaderText = Resource.String("STR_FRM_SESSIONS_DATAGRID_LAST_MSG");
      dg_sessions.Columns[COL_LAYOUT_INACTIVITY].HeaderText = Resource.String("STR_FRM_SESSIONS_DATAGRID_INACTIVITY");
      dg_sessions.Columns[COL_PS_STARTED].HeaderText = Resource.String("STR_FRM_SESSIONS_DATAGRID_CARD_INSERTED");
      dg_sessions.Columns[COL_TRACK_NUMBER].HeaderText = Resource.String("STR_FORMAT_GENERAL_NAME_CARD");
      dg_sessions.Columns[COL_PLAYED_COUNT].HeaderText = Resource.String("STR_FRM_SESSIONS_DATAGRID_PLAYS");
      dg_sessions.Columns[COL_MAX_PLAY].HeaderText = Resource.String("STR_FRM_SESSIONS_DATAGRID_LAST_PLAY");
      dg_sessions.Columns[COL_LAYOUT_PLAYS_INACT].HeaderText = Resource.String("STR_FRM_SESSIONS_DATAGRID_NOT_PLAYING");

      if (no_data) 
      {
        // No opened sessions retrieved

        // Add a blank row to datagrid
        DataRow row;
        row = dt_terminals.NewRow();
        row["terminal_id"] = "";
        row["Terminal"] = "";
        row["LastMessage"] = "";
        row["inact"] = "";
        dt_terminals.Rows.Add(row);

        return;
      }

      // Fill datagrid with obtained data
      DataRow[] dr_sessions;
      DataRow[] dr_plays;
      System.DateTime dtime;
      TimeSpan tspan;
      for (int l = 0; l < dg_sessions.Rows.Count; l++)
      {
        string exp = "terminal_id = '" + dg_sessions.Rows[l].Cells["terminal_id"].Value.ToString() + "'";
        dr_sessions = dt_sessions.Select(exp);
        dg_sessions.Rows[l].Cells[COL_PS_STARTED].Value = dr_sessions[0]["ps_started"];
        dr_plays = dt_plays.Select(exp);

        if (dr_sessions[0]["ac_track"].ToString() == "")
        {
          dg_sessions.Rows[l].Cells[COL_PS_STARTED].Value = "";
          dg_sessions.Rows[l].Cells[COL_TRACK_NUMBER].Value = "";
          dg_sessions.Rows[l].Cells[COL_PLAYED_COUNT].Value = Resource.String("STR_FRM_SESSIONS_DATAGRID_NOT_PLAYING");
          dg_sessions.Rows[l].Cells[COL_LAYOUT_PLAYS_INACT].Value = DEFAULT_INACTIVITY;
          if (dr_plays.Length == 0)
          {
            dg_sessions.Rows[l].Cells[COL_PLAYED_COUNT].Value = Resource.String("STR_FRM_SESSIONS_DATAGRID_NOT_PLAYING");
          }
        }
        else
        {
          dg_sessions.Rows[l].Cells[COL_TRACK_NUMBER].Value = dr_sessions[0]["ac_track"];
          if (dr_plays.Length != 0)
          {
            dg_sessions.Rows[l].Cells[COL_PLAYED_COUNT].Value = dr_sessions[0]["played_count"];
            if (dr_plays[0]["LastPlayDate"].ToString() == "")
            {
              dg_sessions.Rows[l].Cells[COL_MAX_PLAY].Value = "";
              dtime = Convert.ToDateTime(dr_plays[0]["ps_started"]);
            }
            else
            {
              dg_sessions.Rows[l].Cells[COL_MAX_PLAY].Value = dr_plays[0]["LastPlayDate"];
              dtime = Convert.ToDateTime(dr_plays[0]["LastPlayDate"]);
            }
            tspan = WGDB.Now.Subtract(dtime);
            dg_sessions.Rows[l].Cells[COL_PLAYS_INACTIVITY].Value = Convert.ToInt32(tspan.TotalSeconds);

          }
          else
          {
            dg_sessions.Rows[l].Cells[COL_PLAYED_COUNT].Value = Resource.String("STR_FRM_SESSIONS_DATAGRID_NOT_PLAYING");
            dtime = Convert.ToDateTime(dr_sessions[0]["ps_started"]);
            tspan = WGDB.Now.Subtract(dtime);
            dg_sessions.Rows[l].Cells[COL_PLAYS_INACTIVITY].Value = Convert.ToInt32(tspan.TotalSeconds);
          }
        }
      }
    }

    /// <summary>
    /// Obtain play sessions table
    /// </summary>
    /// <returns></returns>
    private string Load_DataTableSessions()
    {
      string sql_terminalsxsession = "SELECT ps_terminal_id, MAX(ps_play_session_id) AS session_id, MAX(ps_started) as ps_started " +
                                     " FROM play_sessions " + filtered + " GROUP BY ps_terminal_id ORDER BY ps_terminal_id";
      SqlCommand command;
      SqlDataReader reader;
      DataRow row;
      string sessions = "";
      int r = 0;

      command = new SqlCommand(sql_terminalsxsession, conn);
      reader = command.ExecuteReader();

      while (reader.Read())
      {
        row = dt_sessions.NewRow();
        row["terminal_id"] = reader.GetInt32(0);
        row["max_ps_id"] = reader.GetInt64(1);
        row["ps_started"] = reader.GetDateTime(2);
        sessions = sessions + reader.GetInt64(1).ToString() + ", ";
        dt_sessions.Rows.Add(row);
        r++;
      }
      reader.Close();

      if (r != 0)
      {
        sessions = sessions.Remove(sessions.Length - 2);
      }
      else
      {
        // No sessions retrieved
        return "";
      }

      return sessions;
    }

    /// <summary>
    /// Load data table plays for the opened sessions ids.
    /// </summary>
    /// <param name="sessions">Session ids.</param>
    /// <returns></returns>
    private string Load_DataTablePlays(string sessions)
    {
      string terminals = "";

      string sql_plays = "SELECT ISNULL(MAX(pl_datetime),''), pl_terminal_id, pl_play_session_id FROM plays WHERE pl_play_session_id IN " +
                         " (" + sessions + ") GROUP BY pl_terminal_id, pl_play_session_id ORDER BY pl_terminal_id";
      SqlCommand command;
      SqlDataReader reader;
      DataRow row;
      int r = 0;

      command = new SqlCommand(sql_plays, conn);
      reader = command.ExecuteReader();

      while (reader.Read())
      {
        row = dt_plays.NewRow();
        row["LastPlayDate"] = reader.GetDateTime(0);
        row["terminal_id"] = reader.GetInt32(1);
        row["CardInserted"] = reader.GetInt64(2);
        dt_plays.Rows.Add(row);
        r++;
      }
      reader.Close();

      string cards = "SELECT ISNULL(ac_track_data,0), ps_played_count, ps_terminal_id FROM PLAY_SESSIONS LEFT OUTER JOIN ACCOUNTS ON PS_ACCOUNT_ID = ac_account_id WHERE ps_play_session_id in(" +
                     sessions + ") ORDER BY ps_terminal_id";
      command = new SqlCommand(cards, conn);
      reader = command.ExecuteReader();
      r = 0;
      while (reader.Read())
      {
        String track_number;
        int card_type = 0;
        Boolean rc;

        rc = CardNumber.TrackDataToExternal(out track_number, (String)reader.GetString(0), card_type);

        dt_sessions.Rows[r]["ac_track"] = track_number;
        dt_sessions.Rows[r]["played_count"] = reader.GetInt32(1);
        terminals = terminals + reader.GetInt32(2) + ", ";
        r++;
      }
      reader.Close();

      if (r == 0)
      {
        no_data = true;
        return "";
      }

      terminals = terminals.Remove(terminals.Length - 2);

      return terminals;
    }

    /// <summary>
    /// Obtain terminal data list with sessions from the terminal ids.
    /// </summary>
    /// <param name="terminals"></param>
    private void Load_DataTableTerminals(string terminals)
    {
      sql_terminals = "SELECT  t.te_terminal_id, t.te_name, MAX(s.ws_last_rcvd_msg) AS LastConnection, " +
                      " ISNULL(DATEDIFF(ss, MAX(s.ws_last_rcvd_msg), GETDATE()),0) AS inact FROM terminals t, wcp_sessions s WHERE t.te_terminal_id = s.ws_terminal_id " +
                      " and t.te_terminal_id in (" + terminals + ") GROUP BY t.te_terminal_id, t.te_name ORDER BY t.te_terminal_id";

      SqlCommand command;
      SqlDataReader reader;
      DataRow row;
      int r = 0;

      command = new SqlCommand(sql_terminals, conn);
      reader = command.ExecuteReader();

      while (reader.Read())
      {
        row = dt_terminals.NewRow();
        row["terminal_id"] = reader.GetInt32(0);
        row["Terminal"] = reader.GetString(1);
        row["LastMessage"] = reader.GetDateTime(2);
        row["inact"] = reader.GetInt32(3);
        dt_terminals.Rows.Add(row);
        r++;
      }
      reader.Close();
    }

    /// <summary>
    /// Timer to refresh the sessions grid.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void timer1_Tick(object sender, EventArgs e)
    {
      dg_sessions.SuspendLayout();
      RefreshDataGrid();
      ds.AcceptChanges();
      Intervals();
      dg_sessions.ResumeLayout(true);
    }

    /// <summary>
    /// Function to update obtained data grid data.
    /// </summary>
    private void RefreshDataGrid()
    {
      string exp = "";
      conn = WGDB.Connection();
      SqlDataAdapter da;
      DataSet dsBridge; 
      DataRow[] dr_sessions;
      DataRow[] dr_plays;
      DataRow[] dr_terminals;
      DataRow[] dr_cards;
      DataRow dr;
      string sql = "";

      try
      {

        sql = GetSqlTerminals(filtered);

        if (sql != "")
        {
          da = new SqlDataAdapter(sql, conn);
          dsBridge = new DataSet();
          da.Fill(dsBridge);

          if (ds.Tables[2].Rows.Count != dsBridge.Tables[2].Rows.Count)
          {
            for (int m = 0; m < dsBridge.Tables[2].Rows.Count; m++)
            {
              exp = "terminal_id = '" + dsBridge.Tables[2].Rows[m][0] + "'";
              dr_terminals = ds.Tables["Terminals"].Select(exp);
              if (dr_terminals.Length == 0)
              {
                dr = ds.Tables[2].NewRow();
                dr["terminal_id"] = dsBridge.Tables[2].Rows[m][0];
                dr["Terminal"] = dsBridge.Tables[2].Rows[m][1];
                dr["LastMessage"] = dsBridge.Tables[2].Rows[m][2];
                ds.Tables[2].Rows.Add(dr);
              }
            }
            if (no_data)
            {
              ds.Tables[2].Rows[0].Delete(); //Delete blank row if there are opened sessions
              ds.AcceptChanges();
            }
          }

          foreach (DataRow row in dsBridge.Tables[3].Rows)
          {
            String track_number;
            int card_type = 0;
            Boolean rc;

            rc = CardNumber.TrackDataToExternal(out track_number, (String)row[0].ToString(), card_type);
            row[0] = track_number;
          }

          DateTime dtime;
          TimeSpan tspan;

          if (ds.Tables["Terminals"].Rows.Count == 1 && ds.Tables["Terminals"].Rows[0][0].ToString() == "") return;

          int j = 0;
          while (j < ds.Tables["Terminals"].Rows.Count)
          {
            exp = "te_terminal_id = " + ds.Tables[2].Rows[j][0].ToString();
            dr_terminals = dsBridge.Tables[2].Select(exp);
            if (dr_terminals.Length != 0)
            {
              ds.Tables[2].Rows[j]["LastMessage"] = dr_terminals[0][2];

              if (ds.Tables[2].Rows[j]["inact"].ToString() != dr_terminals[0]["inact"].ToString())
              {
                ds.Tables[2].Rows[j]["inact"] = dr_terminals[0]["inact"];
              }
              exp = exp.Replace("te_", "ps_");
              dr_sessions = dsBridge.Tables[0].Select(exp);
              dr_cards = dsBridge.Tables[3].Select(exp);
              if (ds.Tables[2].Rows[j]["ps_started"].ToString() != dr_sessions[0]["ps_started"].ToString())
              {
                ds.Tables[2].Rows[j]["ps_started"] = dr_sessions[0]["ps_started"];
              }
              exp = exp.Replace("ps_", "pl_");
              dr_plays = dsBridge.Tables[1].Select(exp);
              if (ds.Tables[2].Rows[j]["ac_track"].ToString() != dr_cards[0][0].ToString())
              {
                ds.Tables[2].Rows[j]["ac_track"] = dr_cards[0][0];
              }
              if (ds.Tables[2].Rows[j]["ac_track"].ToString() != "")
              {
                if (ds.Tables[2].Rows[j]["played_count"].ToString() != dr_cards[0][2].ToString())
                {
                  ds.Tables[2].Rows[j]["played_count"] = dr_cards[0][2];
                }
                if (dr_plays.Length != 0 && dr_plays[0]["LastPlayDate"] != null)
                {
                  if (ds.Tables[2].Rows[j]["max_play"] != dr_plays[0]["LastPlayDate"])
                  {
                    ds.Tables[2].Rows[j]["max_play"] = dr_plays[0]["LastPlayDate"];
                  }
                  dtime = Convert.ToDateTime(dr_plays[0]["LastPlayDate"]);
                  tspan = WGDB.Now.Subtract(dtime);
                  if (ds.Tables[2].Rows[j]["inact_plays"].ToString() != tspan.TotalSeconds.ToString())
                  {
                    ds.Tables[2].Rows[j]["inact_plays"] = Convert.ToInt32(tspan.TotalSeconds);
                  }
                }
                else //No plays for this session
                {
                  ds.Tables[2].Rows[j]["played_count"] = Resource.String("STR_FRM_SESSIONS_DATAGRID_NOT_PLAYING");
                  dtime = Convert.ToDateTime(dr_sessions[0]["ps_started"]);
                  tspan = WGDB.Now.Subtract(dtime);
                  ds.Tables[2].Rows[j]["inact_plays"] = Convert.ToInt32(tspan.TotalSeconds);
                }
              }
              else //No card number for this session
              {
                ds.Tables[2].Rows[j]["ps_started"] = "";
                ds.Tables[2].Rows[j]["ac_track"] = "";
                ds.Tables[2].Rows[j]["played_count"] = "";
                ds.Tables[2].Rows[j]["inact_plays"] = "";
              }
            }
            else //Play session not started for this terminal
            {
              ds.Tables[2].Rows[j].Delete();
            }
            j++;
          }
          ds.AcceptChanges();
          dsBridge.Dispose();
          no_data = false;
        }
        else
        {
          // No active sessions retrieved.
          if (dg_sessions.Rows.Count > 1)
          {
            //Add a blank row to the datagrid
            ds.Tables["Terminals"].Clear();
            dr = dt_terminals.NewRow();
            dr["terminal_id"] = "";
            dr["Terminal"] = "";
            dr["LastMessage"] = "";
            dr["inact"] = "";
            dt_terminals.Rows.Add(dr);
          }
          ds.AcceptChanges();
          no_data = true;
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }        
    }

    /// <summary>
    /// Obtaing gaming sessions sql.
    /// </summary>
    /// <param name="whereclause"></param>
    /// <returns></returns>
    private string GetSqlTerminals(string whereclause)
    {
      string sql_exp;
      string sql_terminalsxsession = "SELECT MAX(ps_play_session_id) AS session_id, MAX(ps_started) as ps_started, ps_terminal_id " +
                                     " FROM play_sessions " + whereclause +
                                     " GROUP BY ps_terminal_id ORDER BY ps_terminal_id";
      sql_exp = sql_terminalsxsession;

      SqlCommand command = new SqlCommand(sql_terminalsxsession, conn);
      SqlDataReader reader = command.ExecuteReader();
      string sessions = "";

      while (reader.Read())
      {
        sessions = sessions + reader.GetInt64(0).ToString() + ", ";
      }
      reader.Close();

      if (sessions == "")
      {
        // No sessions retrieved
        return "";
      }

      // Remove last coma + space
      sessions = sessions.Remove(sessions.Length - 2);

      string terminals = "";
      string sql_plays = "SELECT MAX(pl_datetime) as LastPlayDate, pl_terminal_id, pl_play_session_id FROM plays WHERE pl_play_session_id IN " +
                         " (" + sessions + ") GROUP BY pl_terminal_id, pl_play_session_id ORDER BY pl_terminal_id";
      sql_exp = sql_exp + ";" + sql_plays;

      string ps_data = "SELECT ISNULL(ac_track_data,0), ps_terminal_id, ps_played_count FROM PLAY_SESSIONS LEFT OUTER JOIN ACCOUNTS ON PS_ACCOUNT_ID = ac_account_id WHERE ps_play_session_id in(" +
                     sessions + ") ORDER BY ps_terminal_id";

      command = new SqlCommand(ps_data, conn);
      reader = command.ExecuteReader();
      while (reader.Read())
      {
        terminals = terminals + reader.GetInt32(1) + ", ";
      }

      reader.Close();
      terminals = terminals.Remove(terminals.Length - 2);

      sql_terminals = "SELECT  t.te_terminal_id, t.te_name, MAX(s.ws_last_rcvd_msg) AS LastConnection, " +
        " DATEDIFF(ss, MAX(s.ws_last_rcvd_msg), GETDATE()) AS inact FROM terminals t, wcp_sessions s WHERE t.te_terminal_id = s.ws_terminal_id " +
        " and t.te_terminal_id in (" + terminals + ") GROUP BY t.te_terminal_id, t.te_name ORDER BY t.te_terminal_id";
      sql_exp = sql_exp + ";" + sql_terminals;

      sql_exp = sql_exp + ";" + ps_data + ";";

      return sql_exp;
    }

    /// <summary>
    /// Set inactivity cell colors on sessions.
    /// </summary>
    private void Intervals()
    {
      int inact = 0;
      string plays_inact = "";
      for (int row = 0; row < dg_sessions.Rows.Count; row++)
      {
        // Sessions
        if (!no_data)
        {
          inact = Convert.ToInt32(dg_sessions.Rows[row].Cells[COL_INACTIVITY].Value);
          if (inact < 300)
          {
            dg_sessions.Rows[row].Cells[COL_LAYOUT_INACTIVITY].Value = inact.ToString() + " " + Resource.String("STR_FORMAT_GENERAL_SECS");
          }
          else if (inact >= 300 && inact < 600)
          {
            dg_sessions.Rows[row].Cells[COL_LAYOUT_INACTIVITY].Value = "> 5 " + Resource.String("STR_FORMAT_GENERAL_MINS");
          }
          else if (inact >= 600 && inact < 3600)
          {
            dg_sessions.Rows[row].Cells[COL_LAYOUT_INACTIVITY].Value = "> 10 " + Resource.String("STR_FORMAT_GENERAL_MINS");
          }
          else
          {
            dg_sessions.Rows[row].Cells[COL_LAYOUT_INACTIVITY].Value = "> 1 " + Resource.String("STR_FORMAT_GENERAL_HOUR");
          }
          dg_sessions.Rows[row].Cells[COL_LAYOUT_INACTIVITY].Style.BackColor = CashierBusinessLogic.GetInactivityColor(inact);
        }

        // Plays
        plays_inact = dg_sessions.Rows[row].Cells["inact_plays"].Value.ToString();
        if (plays_inact != "")
        {
          inact = Convert.ToInt32(plays_inact);
          if (inact < 300)
          {
            dg_sessions.Rows[row].Cells[COL_LAYOUT_PLAYS_INACT].Value = inact.ToString() + " " + Resource.String("STR_FORMAT_GENERAL_SECS");
          }
          else if (inact >= 300 && inact < 600)
          {
            dg_sessions.Rows[row].Cells[COL_LAYOUT_PLAYS_INACT].Value = "> 5 " + Resource.String("STR_FORMAT_GENERAL_MINS");
          }
          else if (inact >= 600 && inact < 3600)
          {
            dg_sessions.Rows[row].Cells[COL_LAYOUT_PLAYS_INACT].Value = "> 10 " + Resource.String("STR_FORMAT_GENERAL_MINS");
          }
          else
          {
            dg_sessions.Rows[row].Cells[COL_LAYOUT_PLAYS_INACT].Value = "> 1 " + Resource.String("STR_FORMAT_GENERAL_HOUR");
          }
          dg_sessions.Rows[row].Cells[COL_LAYOUT_PLAYS_INACT].Style.BackColor = CashierBusinessLogic.GetInactivityColor(inact);
        }
      }
    }

    /// <summary>
    /// Close button actions.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btn_exit_Click(object sender, EventArgs e)
    {
      timer1.Stop();
      timer1.Dispose();
      Close();
    }

    /// <summary>
    /// Sort grid.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void dataGridView1_Sorted(object sender, EventArgs e)
    {
      Intervals();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void rb_session_st_CheckedChanged(object sender, EventArgs e)
    {
      if (!rb_session_st.Checked)
      {
        filtered = "";
      }
      else
      {
        filtered = " WHERE ps_status = 0 ";
      }

      timer1.Stop();
      dg_sessions.SuspendLayout();
      RefreshDataGrid();
      ds.AcceptChanges();
      Intervals();
      dg_sessions.ResumeLayout();
      dg_sessions.DataSource = ds.Tables["Terminals"];
      timer1.Start();

    }

    #endregion // Private Methods

    #region Public Methods

    public void Show(Form Parent)
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_sessions", Log.Type.Message);

      Width = 955;
      dg_sessions.Width = 772;
      Width += System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
      dg_sessions.Width += System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
      Location = new Point(Parent.Width / 2 - Width / 2, Parent.Height / 2 - 200);

      //btn_close
      btn_close.Location = new Point(Width / 2 - (btn_close.Width / 2), btn_close.Location.Y);
      
      form_yes_no.Show(Parent);
      ShowDialog(form_yes_no);
      
      form_yes_no.Hide();
      
      Parent.Focus();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Dispose yes_no form.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    new public void Dispose()
    {
      form_yes_no.Dispose();
      form_yes_no = null;

      base.Dispose();
    }

    #endregion // Public Methods
  }
}