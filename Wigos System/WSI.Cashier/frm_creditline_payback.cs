﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_creditline_payback.cs
// 
//   DESCRIPTION: Implements the following classes:
//                1. frm_creditline_payback
//
//        AUTHOR: Enric Tomas
// 
// CREATION DATE: 23-MAR-2017
// 
// REVISION HISTORY:
// 
// Date          Author    Description
// -----------   ------    -----------------------------------------------------
// 23-MAR-2017   ETP       First release.
// 10-AUG-2017   MS        Bug 29304:[WIGOS-4377] Creditline: repayment with checks (commission defined)
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WSI.Cashier.Controls;
using WSI.Common;
using WSI.Common.CreditLines;

namespace WSI.Cashier
{
  public partial class frm_creditline_payback : frm_base
  {

    private List<CurrencyExchange> m_currencies = null;
    private CurrencyExchange m_national_currency = null;
    private CardData m_card_data;
    private CurrencyExchangeResult m_exchange_result;
    private Form m_ParentForm;

    #region Constructor
    public frm_creditline_payback(Form Parent)
    {
      InitializeComponent();
      InitializeControlResources();
      m_ParentForm = Parent;
    }
    #endregion // Constructor

    #region public methods

    /// <summary>
    /// Show payback form.
    /// </summary>
    /// <param name="_account_id"></param>
    /// <param name="CreditLineData"></param>
    /// <returns></returns>
    public Boolean PaybackShow(Int64 _account_id, out CreditLine CreditLineData)
    {
      CardData _card_data;
      _card_data = new CardData();
      Int64 _operation_id;
      MultiPromos.AccountBalance _charge_credit_line;
      ArrayList VoucherList;
      TYPE_SPLITS _splits;
      VoucherList = new ArrayList();

      CardData.DB_CardGetAllData(_account_id, _card_data);
      CreditLineData = null;

      m_card_data = _card_data;

      UpdateControlAmounts();
      _charge_credit_line = MultiPromos.AccountBalance.Zero;
      CreditLineData = m_card_data.CreditLineData;

      if (ShowDialog() == DialogResult.OK)
      {
        if (!Split.ReadSplitParameters(out _splits))
        {
          return false;
        }

        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (!CreditLine.DoMovementsCreditLine(m_card_data, m_exchange_result.NetAmount, Cashier.CashierSessionInfo(), CreditLineMovements.CreditLineMovementType.Payback, m_exchange_result, _db_trx.SqlTransaction, out _operation_id))
          {
            return false;
          }

          if (m_exchange_result.InType != CurrencyExchangeType.CURRENCY || m_exchange_result.InCurrencyCode != CurrencyExchange.GetNationalCurrency())
          {
            VoucherList = VoucherBuilder.CurrencyExchange(m_card_data.VoucherAccountInfo(), m_card_data.AccountId, _splits, m_exchange_result, m_exchange_result.InType
               , CurrencyExchange.GetNationalCurrency(), _operation_id, OperationCode.CREDIT_LINE, PrintMode.Print, _db_trx.SqlTransaction);
          }

          VoucherBuilder.GetVoucherCreditLinePayback(VoucherList, m_card_data.VoucherAccountInfo(), m_exchange_result.NetAmount, CreditLineData
                       , _splits, _operation_id, PrintMode.Print, _db_trx.SqlTransaction);


          _db_trx.Commit();
        }

        VoucherPrint.Print(VoucherList);
        _charge_credit_line.Redeemable -= m_exchange_result.NetAmount;
      }


      return true;
    }// PaybackShow

    #endregion // Public Methods

    #region private methods

    /// <summary>
    /// Init Resources
    /// </summary>
    private void InitializeControlResources()
    {
      List<CurrencyExchangeType> _types = new List<CurrencyExchangeType>();

      FormTitle = Resource.String("STR_FRM_ACTION_CREDITLINE_PAYBACK");

      //  Labels
      lbl_payback.Text = Resource.String("STR_UC_BANK_MOVEMENTS_SUMMARY_AMOUNTS");
      lbl_credit_spent.Text = Resource.String("STR_FRM_CREDIT_LINE_SPENT");
      lbl_payback_amount.Text = String.Format("{0}:", Resource.String("STR_VOUCHER_CREDIT_LINE_PAYBACK"));
      lbl_comission.Text = String.Format("{0}:", Resource.String("STR_FRM_CARD_ASSIGN_007"));
      lbl_pending.Text = String.Format("{0}:", Resource.String("STR_TOTAL_CREDIT_LINE"));
      lbl_total.Text = String.Format("{0}:", Resource.String("STR_FRM_CARD_ASSIGN_008"));

      // Grupbox
      uc_payment_method1.HeaderText = Resource.String("STR_FRM_AMOUNT_PAYMENT_METHOD_GROUP_BOX");
      gb_exchange_info.HeaderText = Resource.String("STR_FRM_RECEPTION_TICKET_ENTRY_DETAILS");

      //  Buttons
      btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");
      btn_ok.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_OK");



      _types.Add(CurrencyExchangeType.CURRENCY);
      _types.Add(CurrencyExchangeType.CARD);
      _types.Add(CurrencyExchangeType.CHECK);

      CurrencyExchange.GetAllowedCurrencies(true, _types, false, true, false, false, out m_national_currency, out m_currencies);

      // Remove national currency

      uc_payment_method1.VoucherType = VoucherTypes.CardAdd; // m_voucher_type
      uc_payment_method1.InitControl(m_currencies, null, FeatureChips.ChipsOperation.Operation.NONE);

      uc_payment_method1.CurrencyExchange = m_national_currency;
    } // InitializeControlResources

    /// <summary>
    /// Update control amounts
    /// </summary>
    private void UpdateControlAmounts()
    {
      Currency _amount;
      if (m_card_data != null)
      {
        lbl_credit_spent_value.Text = m_card_data.CreditLineData.SpentAmount.ToString();
        _amount = Format.ParseCurrency(this.uc_payback_amount.Text);
        uc_payment_method1.CurrencyExchange.ApplyExchange(_amount, out m_exchange_result);

        m_exchange_result.SubType = uc_payment_method1.CurrencyExchange.SubType;

        if (m_exchange_result.InType == CurrencyExchangeType.CHECK && _amount > 0 && m_exchange_result.NetAmount == 0 && m_exchange_result.Comission > 0)
        {
          lbl_payback_value.ForeColor = Color.Red;
        }
        else
        {
          lbl_payback_value.ForeColor = Color.Black;
        }

        _amount = m_exchange_result.NetAmount;
        lbl_payback_value.Text = _amount.ToString();

        _amount = m_card_data.CreditLineData.SpentAmount - m_exchange_result.NetAmount;
        lbl_pending_amount.Text = _amount.ToString();
        if (_amount < 0)
        {
          lbl_pending_amount.ForeColor = Color.Red;
        }
        else
        {
          lbl_pending_amount.ForeColor = Color.Black;
        }

        _amount = m_exchange_result.Comission;
        lbl_comission_value.Text = _amount.ToString();


        if (m_exchange_result.InCurrencyCode == CurrencyExchange.GetNationalCurrency())
        {
          _amount = m_exchange_result.GrossAmount;
          lbl_total_amount.Text = _amount.ToString();
        }
        else
        {
          _amount = m_exchange_result.InAmount;
          _amount.CurrencyIsoCode = m_exchange_result.InCurrencyCode;
          lbl_total_amount.Text = String.Format("{0} {1}", _amount.ToStringWithoutSymbols(), m_exchange_result.InCurrencyCode);
        }
      }
    } // UpdateControlAmounts

    private Boolean CheckPaybackOperation()
    {
      String _error_str;

      if ((m_exchange_result.InAmount <= m_exchange_result.Comission) && m_exchange_result.InType == CurrencyExchangeType.CHECK  && m_exchange_result.Comission > 0)
      {
        frm_message.Show(Resource.String("STR_FRM_AMOUNT_INPUT_MSG_COMMISSION_NOT_COVERED"), Resource.String("STR_APP_GEN_MSG_WARNING"), MessageBoxButtons.OK, Images.CashierImage.Warning, m_ParentForm);
        return false;
      }

      if (m_exchange_result.NetAmount == 0)
      {
        frm_message.Show(Resource.String("STR_FRM_AMOUNT_INPUT_MSG_ZERO_AMOUNT"), Resource.String("STR_APP_GEN_MSG_WARNING"), MessageBoxButtons.OK, Images.CashierImage.Warning, m_ParentForm);
        return false;
      }

      if (m_card_data.CreditLineData.SpentAmount - m_exchange_result.NetAmount < 0)
      {
        frm_message.Show(Resource.String("STR_CREDITLINE_WARNING_PAYBACK"), Resource.String("STR_APP_GEN_MSG_WARNING"), MessageBoxButtons.OK, Images.CashierImage.Warning, m_ParentForm);
        return false;
      }

      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.CreditLine_Payback, ProfilePermissions.TypeOperation.RequestPasswd, m_ParentForm, false, out _error_str))
      {
        return false;
      }
      return true;
    }

    #endregion // Private Methods

    #region Events

    private void uc_payment_method1_CurrencyChanged(CurrencyExchange CurrencyExchange)
    {
      UpdateControlAmounts();
    } // uc_payment_method1_CurrencyChanged


    private void uc_payback_amount_TextChanged(object sender, EventArgs e)
    {
      UpdateControlAmounts();
    } // uc_payback_amount_TextChanged

    private void uc_payback_amount_Enter(object sender, EventArgs e)
    {
      Currency _value;

      uc_payback_amount.TextChanged -= new System.EventHandler(this.uc_payback_amount_TextChanged);
      uc_payback_amount.TextChanged -= new System.EventHandler(this.uc_payback_amount_TextChanged);

      _value = Format.ParseCurrency(this.uc_payback_amount.Text);
      uc_payback_amount.Text = _value.ToStringWithoutSymbols();

      uc_payback_amount.TextChanged += new System.EventHandler(this.uc_payback_amount_TextChanged);
      uc_payback_amount.TextChanged += new System.EventHandler(this.uc_payback_amount_TextChanged);
    } // uc_payback_amount_Enter

    private void uc_payback_amount_Leave(object sender, EventArgs e)
    {
      Currency _value;
      if (m_exchange_result != null)
      {
        String _format = "n" + m_exchange_result.Decimals;

        uc_payback_amount.TextChanged -= new System.EventHandler(this.uc_payback_amount_TextChanged);
        uc_payback_amount.TextChanged -= new System.EventHandler(this.uc_payback_amount_TextChanged);

        _value = Format.ParseCurrency(this.uc_payback_amount.Text);
        uc_payback_amount.Text = _value.ToString(_format);

        uc_payback_amount.TextChanged += new System.EventHandler(this.uc_payback_amount_TextChanged);
        uc_payback_amount.TextChanged += new System.EventHandler(this.uc_payback_amount_TextChanged);


        this.uc_payback_amount.Style = uc_round_textbox.RoundTextBoxStyle.BASIC_CUSTOM;
        this.uc_payback_amount.Refresh();
      }
    } // uc_payback_amount_Leave

    private void btn_cancel_Click(object sender, EventArgs e)
    {
      this.DialogResult = System.Windows.Forms.DialogResult.Cancel;

      Misc.WriteLog("[FORM CLOSE] frm_creditline_payback (cancel)", Log.Type.Message);
      this.Close();
    } // btn_cancel_Click

    private void btn_ok_Click(object sender, EventArgs e)
    {
      if (!CheckPaybackOperation())
      {
        return;
      }

      this.DialogResult = System.Windows.Forms.DialogResult.OK;
    } // btn_ok_Click

    #endregion // Events
  }
}
