using System.Windows.Forms;
namespace WSI.Cashier
{
  partial class uc_input_amount_resizable
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(uc_input_amount_resizable));
      this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
      this.pb_close = new System.Windows.Forms.PictureBox();
      this.lbl_head = new WSI.Cashier.Controls.uc_label();
      this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
      this.btn_num_1 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_2 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_dot = new WSI.Cashier.Controls.uc_round_button();
      this.btn_intro = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_3 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_back = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_4 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_0 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_5 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_9 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_6 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_8 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_7 = new WSI.Cashier.Controls.uc_round_button();
      this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
      this.btn_money_1 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_money_2 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_money_3 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_money_4 = new WSI.Cashier.Controls.uc_round_button();
      this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
      this.txt_amount = new WSI.Cashier.Controls.uc_round_textbox();
      this.tableLayoutPanel4.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pb_close)).BeginInit();
      this.tableLayoutPanel1.SuspendLayout();
      this.tableLayoutPanel2.SuspendLayout();
      this.tableLayoutPanel3.SuspendLayout();
      this.SuspendLayout();
      // 
      // tableLayoutPanel4
      // 
      this.tableLayoutPanel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.tableLayoutPanel4.ColumnCount = 2;
      this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
      this.tableLayoutPanel4.Controls.Add(this.pb_close, 0, 0);
      this.tableLayoutPanel4.Controls.Add(this.lbl_head, 0, 0);
      this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 0);
      this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(0);
      this.tableLayoutPanel4.Name = "tableLayoutPanel4";
      this.tableLayoutPanel4.RowCount = 4;
      this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
      this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
      this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 17F));
      this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 68F));
      this.tableLayoutPanel4.Size = new System.Drawing.Size(385, 30);
      this.tableLayoutPanel4.TabIndex = 25;
      // 
      // pb_close
      // 
      this.pb_close.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.pb_close.Location = new System.Drawing.Point(350, 0);
      this.pb_close.Margin = new System.Windows.Forms.Padding(0);
      this.pb_close.MinimumSize = new System.Drawing.Size(35, 35);
      this.pb_close.Name = "pb_close";
      this.pb_close.Padding = new System.Windows.Forms.Padding(1, 1, 1, 5);
      this.pb_close.Size = new System.Drawing.Size(35, 35);
      this.pb_close.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
      this.pb_close.TabIndex = 0;
      this.pb_close.TabStop = false;
      this.pb_close.Click += new System.EventHandler(this.pb_close_Click);
      // 
      // lbl_head
      // 
      this.lbl_head.AutoSize = true;
      this.lbl_head.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.lbl_head.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_head.Font = new System.Drawing.Font("Open Sans Semibold", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_head.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(167)))), ((int)(((byte)(221)))), ((int)(((byte)(241)))));
      this.lbl_head.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
      this.lbl_head.Location = new System.Drawing.Point(0, 0);
      this.lbl_head.Margin = new System.Windows.Forms.Padding(0);
      this.lbl_head.Name = "lbl_head";
      this.lbl_head.Size = new System.Drawing.Size(350, 30);
      this.lbl_head.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.lbl_head.TabIndex = 23;
      this.lbl_head.Text = "XTECLADO NUM�RICO";
      this.lbl_head.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // tableLayoutPanel1
      // 
      this.tableLayoutPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.tableLayoutPanel1.ColumnCount = 5;
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
      this.tableLayoutPanel1.Controls.Add(this.btn_num_1, 0, 0);
      this.tableLayoutPanel1.Controls.Add(this.btn_num_2, 1, 0);
      this.tableLayoutPanel1.Controls.Add(this.btn_num_dot, 2, 3);
      this.tableLayoutPanel1.Controls.Add(this.btn_intro, 3, 1);
      this.tableLayoutPanel1.Controls.Add(this.btn_num_3, 2, 0);
      this.tableLayoutPanel1.Controls.Add(this.btn_back, 3, 0);
      this.tableLayoutPanel1.Controls.Add(this.btn_num_4, 0, 1);
      this.tableLayoutPanel1.Controls.Add(this.btn_num_0, 0, 3);
      this.tableLayoutPanel1.Controls.Add(this.btn_num_5, 1, 1);
      this.tableLayoutPanel1.Controls.Add(this.btn_num_9, 2, 2);
      this.tableLayoutPanel1.Controls.Add(this.btn_num_6, 2, 1);
      this.tableLayoutPanel1.Controls.Add(this.btn_num_8, 1, 2);
      this.tableLayoutPanel1.Controls.Add(this.btn_num_7, 0, 2);
      this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 141);
      this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
      this.tableLayoutPanel1.Name = "tableLayoutPanel1";
      this.tableLayoutPanel1.RowCount = 4;
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
      this.tableLayoutPanel1.Size = new System.Drawing.Size(379, 241);
      this.tableLayoutPanel1.TabIndex = 2;
      // 
      // btn_num_1
      // 
      this.btn_num_1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_1.CornerRadius = 0;
      this.btn_num_1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.btn_num_1.FlatAppearance.BorderSize = 0;
      this.btn_num_1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_1.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_1.Image = null;
      this.btn_num_1.IsSelected = false;
      this.btn_num_1.Location = new System.Drawing.Point(3, 3);
      this.btn_num_1.Name = "btn_num_1";
      this.btn_num_1.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_1.SelectedColor = System.Drawing.Color.Empty;
      this.btn_num_1.Size = new System.Drawing.Size(69, 54);
      this.btn_num_1.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_1.TabIndex = 0;
      this.btn_num_1.TabStop = false;
      this.btn_num_1.Text = "1";
      this.btn_num_1.UseVisualStyleBackColor = false;
      // 
      // btn_num_2
      // 
      this.btn_num_2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_2.CornerRadius = 0;
      this.btn_num_2.Dock = System.Windows.Forms.DockStyle.Fill;
      this.btn_num_2.FlatAppearance.BorderSize = 0;
      this.btn_num_2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_2.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_2.Image = null;
      this.btn_num_2.IsSelected = false;
      this.btn_num_2.Location = new System.Drawing.Point(78, 3);
      this.btn_num_2.Name = "btn_num_2";
      this.btn_num_2.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_2.SelectedColor = System.Drawing.Color.Empty;
      this.btn_num_2.Size = new System.Drawing.Size(69, 54);
      this.btn_num_2.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_2.TabIndex = 1;
      this.btn_num_2.TabStop = false;
      this.btn_num_2.Text = "2";
      this.btn_num_2.UseVisualStyleBackColor = false;
      // 
      // btn_num_dot
      // 
      this.btn_num_dot.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_dot.CornerRadius = 0;
      this.btn_num_dot.Dock = System.Windows.Forms.DockStyle.Fill;
      this.btn_num_dot.FlatAppearance.BorderSize = 0;
      this.btn_num_dot.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_dot.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_dot.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_dot.Image = null;
      this.btn_num_dot.IsSelected = false;
      this.btn_num_dot.Location = new System.Drawing.Point(153, 183);
      this.btn_num_dot.Name = "btn_num_dot";
      this.btn_num_dot.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_dot.SelectedColor = System.Drawing.Color.Empty;
      this.btn_num_dot.Size = new System.Drawing.Size(69, 55);
      this.btn_num_dot.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_dot.TabIndex = 9;
      this.btn_num_dot.TabStop = false;
      this.btn_num_dot.Text = "�";
      this.btn_num_dot.UseVisualStyleBackColor = false;
      // 
      // btn_intro
      // 
      this.btn_intro.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(153)))), ((int)(((byte)(49)))));
      this.tableLayoutPanel1.SetColumnSpan(this.btn_intro, 2);
      this.btn_intro.CornerRadius = 0;
      this.btn_intro.Dock = System.Windows.Forms.DockStyle.Fill;
      this.btn_intro.FlatAppearance.BorderSize = 0;
      this.btn_intro.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_intro.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_intro.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_intro.Image = ((System.Drawing.Image)(resources.GetObject("btn_intro.Image")));
      this.btn_intro.IsSelected = false;
      this.btn_intro.Location = new System.Drawing.Point(228, 63);
      this.btn_intro.Name = "btn_intro";
      this.btn_intro.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.tableLayoutPanel1.SetRowSpan(this.btn_intro, 3);
      this.btn_intro.SelectedColor = System.Drawing.Color.Empty;
      this.btn_intro.Size = new System.Drawing.Size(148, 175);
      this.btn_intro.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_ENTER;
      this.btn_intro.TabIndex = 12;
      this.btn_intro.TabStop = false;
      this.btn_intro.UseVisualStyleBackColor = false;
      // 
      // btn_num_3
      // 
      this.btn_num_3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_3.CornerRadius = 0;
      this.btn_num_3.Dock = System.Windows.Forms.DockStyle.Fill;
      this.btn_num_3.FlatAppearance.BorderSize = 0;
      this.btn_num_3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_3.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_3.Image = null;
      this.btn_num_3.IsSelected = false;
      this.btn_num_3.Location = new System.Drawing.Point(153, 3);
      this.btn_num_3.Name = "btn_num_3";
      this.btn_num_3.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_3.SelectedColor = System.Drawing.Color.Empty;
      this.btn_num_3.Size = new System.Drawing.Size(69, 54);
      this.btn_num_3.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_3.TabIndex = 2;
      this.btn_num_3.TabStop = false;
      this.btn_num_3.Text = "3";
      this.btn_num_3.UseVisualStyleBackColor = false;
      // 
      // btn_back
      // 
      this.btn_back.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.tableLayoutPanel1.SetColumnSpan(this.btn_back, 2);
      this.btn_back.CornerRadius = 0;
      this.btn_back.Dock = System.Windows.Forms.DockStyle.Fill;
      this.btn_back.FlatAppearance.BorderSize = 0;
      this.btn_back.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_back.Font = new System.Drawing.Font("Open Sans Semibold", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_back.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_back.Image = ((System.Drawing.Image)(resources.GetObject("btn_back.Image")));
      this.btn_back.IsSelected = false;
      this.btn_back.Location = new System.Drawing.Point(228, 3);
      this.btn_back.Name = "btn_back";
      this.btn_back.SelectedColor = System.Drawing.Color.Empty;
      this.btn_back.Size = new System.Drawing.Size(148, 54);
      this.btn_back.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_BACKSPACE;
      this.btn_back.TabIndex = 11;
      this.btn_back.TabStop = false;
      this.btn_back.UseVisualStyleBackColor = false;
      // 
      // btn_num_4
      // 
      this.btn_num_4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_4.CornerRadius = 0;
      this.btn_num_4.Dock = System.Windows.Forms.DockStyle.Fill;
      this.btn_num_4.FlatAppearance.BorderSize = 0;
      this.btn_num_4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_4.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_4.Image = null;
      this.btn_num_4.IsSelected = false;
      this.btn_num_4.Location = new System.Drawing.Point(3, 63);
      this.btn_num_4.Name = "btn_num_4";
      this.btn_num_4.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_4.SelectedColor = System.Drawing.Color.Empty;
      this.btn_num_4.Size = new System.Drawing.Size(69, 54);
      this.btn_num_4.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_4.TabIndex = 3;
      this.btn_num_4.TabStop = false;
      this.btn_num_4.Text = "4";
      this.btn_num_4.UseVisualStyleBackColor = false;
      // 
      // btn_num_0
      // 
      this.btn_num_0.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.tableLayoutPanel1.SetColumnSpan(this.btn_num_0, 2);
      this.btn_num_0.CornerRadius = 0;
      this.btn_num_0.Dock = System.Windows.Forms.DockStyle.Fill;
      this.btn_num_0.FlatAppearance.BorderSize = 0;
      this.btn_num_0.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_0.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_0.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_0.Image = null;
      this.btn_num_0.IsSelected = false;
      this.btn_num_0.Location = new System.Drawing.Point(3, 183);
      this.btn_num_0.Name = "btn_num_0";
      this.btn_num_0.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_0.SelectedColor = System.Drawing.Color.Empty;
      this.btn_num_0.Size = new System.Drawing.Size(144, 55);
      this.btn_num_0.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_0.TabIndex = 10;
      this.btn_num_0.TabStop = false;
      this.btn_num_0.Text = "0";
      this.btn_num_0.UseVisualStyleBackColor = false;
      // 
      // btn_num_5
      // 
      this.btn_num_5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_5.CornerRadius = 0;
      this.btn_num_5.Dock = System.Windows.Forms.DockStyle.Fill;
      this.btn_num_5.FlatAppearance.BorderSize = 0;
      this.btn_num_5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_5.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_5.Image = null;
      this.btn_num_5.IsSelected = false;
      this.btn_num_5.Location = new System.Drawing.Point(78, 63);
      this.btn_num_5.Name = "btn_num_5";
      this.btn_num_5.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_5.SelectedColor = System.Drawing.Color.Empty;
      this.btn_num_5.Size = new System.Drawing.Size(69, 54);
      this.btn_num_5.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_5.TabIndex = 4;
      this.btn_num_5.TabStop = false;
      this.btn_num_5.Text = "5";
      this.btn_num_5.UseVisualStyleBackColor = false;
      // 
      // btn_num_9
      // 
      this.btn_num_9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_9.CornerRadius = 0;
      this.btn_num_9.Dock = System.Windows.Forms.DockStyle.Fill;
      this.btn_num_9.FlatAppearance.BorderSize = 0;
      this.btn_num_9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_9.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_9.Image = null;
      this.btn_num_9.IsSelected = false;
      this.btn_num_9.Location = new System.Drawing.Point(153, 123);
      this.btn_num_9.Name = "btn_num_9";
      this.btn_num_9.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_9.SelectedColor = System.Drawing.Color.Empty;
      this.btn_num_9.Size = new System.Drawing.Size(69, 54);
      this.btn_num_9.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_9.TabIndex = 8;
      this.btn_num_9.TabStop = false;
      this.btn_num_9.Text = "9";
      this.btn_num_9.UseVisualStyleBackColor = false;
      // 
      // btn_num_6
      // 
      this.btn_num_6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_6.CornerRadius = 0;
      this.btn_num_6.Dock = System.Windows.Forms.DockStyle.Fill;
      this.btn_num_6.FlatAppearance.BorderSize = 0;
      this.btn_num_6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_6.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_6.Image = null;
      this.btn_num_6.IsSelected = false;
      this.btn_num_6.Location = new System.Drawing.Point(153, 63);
      this.btn_num_6.Name = "btn_num_6";
      this.btn_num_6.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_6.SelectedColor = System.Drawing.Color.Empty;
      this.btn_num_6.Size = new System.Drawing.Size(69, 54);
      this.btn_num_6.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_6.TabIndex = 5;
      this.btn_num_6.TabStop = false;
      this.btn_num_6.Text = "6";
      this.btn_num_6.UseVisualStyleBackColor = false;
      // 
      // btn_num_8
      // 
      this.btn_num_8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_8.CornerRadius = 0;
      this.btn_num_8.Dock = System.Windows.Forms.DockStyle.Fill;
      this.btn_num_8.FlatAppearance.BorderSize = 0;
      this.btn_num_8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_8.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_8.Image = null;
      this.btn_num_8.IsSelected = false;
      this.btn_num_8.Location = new System.Drawing.Point(78, 123);
      this.btn_num_8.Name = "btn_num_8";
      this.btn_num_8.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_8.SelectedColor = System.Drawing.Color.Empty;
      this.btn_num_8.Size = new System.Drawing.Size(69, 54);
      this.btn_num_8.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_8.TabIndex = 7;
      this.btn_num_8.TabStop = false;
      this.btn_num_8.Text = "8";
      this.btn_num_8.UseVisualStyleBackColor = false;
      // 
      // btn_num_7
      // 
      this.btn_num_7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_7.CornerRadius = 0;
      this.btn_num_7.Dock = System.Windows.Forms.DockStyle.Fill;
      this.btn_num_7.FlatAppearance.BorderSize = 0;
      this.btn_num_7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_7.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_7.Image = null;
      this.btn_num_7.IsSelected = false;
      this.btn_num_7.Location = new System.Drawing.Point(3, 123);
      this.btn_num_7.Name = "btn_num_7";
      this.btn_num_7.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_7.SelectedColor = System.Drawing.Color.Empty;
      this.btn_num_7.Size = new System.Drawing.Size(69, 54);
      this.btn_num_7.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_7.TabIndex = 6;
      this.btn_num_7.TabStop = false;
      this.btn_num_7.Text = "7";
      this.btn_num_7.UseVisualStyleBackColor = false;
      // 
      // tableLayoutPanel2
      // 
      this.tableLayoutPanel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.tableLayoutPanel2.ColumnCount = 4;
      this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
      this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
      this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
      this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
      this.tableLayoutPanel2.Controls.Add(this.btn_money_1, 0, 0);
      this.tableLayoutPanel2.Controls.Add(this.btn_money_2, 1, 0);
      this.tableLayoutPanel2.Controls.Add(this.btn_money_3, 2, 0);
      this.tableLayoutPanel2.Controls.Add(this.btn_money_4, 3, 0);
      this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 82);
      this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
      this.tableLayoutPanel2.Name = "tableLayoutPanel2";
      this.tableLayoutPanel2.RowCount = 1;
      this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.tableLayoutPanel2.Size = new System.Drawing.Size(379, 59);
      this.tableLayoutPanel2.TabIndex = 1;
      // 
      // btn_money_1
      // 
      this.btn_money_1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_money_1.CornerRadius = 0;
      this.btn_money_1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.btn_money_1.FlatAppearance.BorderSize = 0;
      this.btn_money_1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_money_1.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_money_1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_money_1.Image = null;
      this.btn_money_1.IsSelected = false;
      this.btn_money_1.Location = new System.Drawing.Point(3, 3);
      this.btn_money_1.Name = "btn_money_1";
      this.btn_money_1.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_money_1.SelectedColor = System.Drawing.Color.Empty;
      this.btn_money_1.Size = new System.Drawing.Size(88, 53);
      this.btn_money_1.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_MONEY;
      this.btn_money_1.TabIndex = 0;
      this.btn_money_1.TabStop = false;
      this.btn_money_1.Text = "+10";
      this.btn_money_1.UseVisualStyleBackColor = false;
      // 
      // btn_money_2
      // 
      this.btn_money_2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_money_2.CornerRadius = 0;
      this.btn_money_2.Dock = System.Windows.Forms.DockStyle.Fill;
      this.btn_money_2.FlatAppearance.BorderSize = 0;
      this.btn_money_2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_money_2.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_money_2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_money_2.Image = null;
      this.btn_money_2.IsSelected = false;
      this.btn_money_2.Location = new System.Drawing.Point(97, 3);
      this.btn_money_2.Name = "btn_money_2";
      this.btn_money_2.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_money_2.SelectedColor = System.Drawing.Color.Empty;
      this.btn_money_2.Size = new System.Drawing.Size(88, 53);
      this.btn_money_2.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_MONEY;
      this.btn_money_2.TabIndex = 1;
      this.btn_money_2.TabStop = false;
      this.btn_money_2.Text = "+50";
      this.btn_money_2.UseVisualStyleBackColor = false;
      // 
      // btn_money_3
      // 
      this.btn_money_3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_money_3.CornerRadius = 0;
      this.btn_money_3.Dock = System.Windows.Forms.DockStyle.Fill;
      this.btn_money_3.FlatAppearance.BorderSize = 0;
      this.btn_money_3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_money_3.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_money_3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_money_3.Image = null;
      this.btn_money_3.IsSelected = false;
      this.btn_money_3.Location = new System.Drawing.Point(191, 3);
      this.btn_money_3.Name = "btn_money_3";
      this.btn_money_3.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_money_3.SelectedColor = System.Drawing.Color.Empty;
      this.btn_money_3.Size = new System.Drawing.Size(88, 53);
      this.btn_money_3.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_MONEY;
      this.btn_money_3.TabIndex = 2;
      this.btn_money_3.TabStop = false;
      this.btn_money_3.Text = "+100";
      this.btn_money_3.UseVisualStyleBackColor = false;
      // 
      // btn_money_4
      // 
      this.btn_money_4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_money_4.CornerRadius = 0;
      this.btn_money_4.Dock = System.Windows.Forms.DockStyle.Fill;
      this.btn_money_4.FlatAppearance.BorderSize = 0;
      this.btn_money_4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_money_4.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_money_4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_money_4.Image = null;
      this.btn_money_4.IsSelected = false;
      this.btn_money_4.Location = new System.Drawing.Point(285, 3);
      this.btn_money_4.Name = "btn_money_4";
      this.btn_money_4.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_money_4.SelectedColor = System.Drawing.Color.Empty;
      this.btn_money_4.Size = new System.Drawing.Size(91, 53);
      this.btn_money_4.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_MONEY;
      this.btn_money_4.TabIndex = 3;
      this.btn_money_4.TabStop = false;
      this.btn_money_4.Text = "+200";
      this.btn_money_4.UseVisualStyleBackColor = false;
      // 
      // tableLayoutPanel3
      // 
      this.tableLayoutPanel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.tableLayoutPanel3.ColumnCount = 1;
      this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel4, 0, 0);
      this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel1, 0, 3);
      this.tableLayoutPanel3.Controls.Add(this.txt_amount, 0, 1);
      this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel2, 0, 2);
      this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 0);
      this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(0);
      this.tableLayoutPanel3.Name = "tableLayoutPanel3";
      this.tableLayoutPanel3.RowCount = 4;
      this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
      this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
      this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 17F));
      this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 68F));
      this.tableLayoutPanel3.Size = new System.Drawing.Size(385, 382);
      this.tableLayoutPanel3.TabIndex = 18;
      // 
      // txt_amount
      // 
      this.txt_amount.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_amount.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_amount.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.txt_amount.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.txt_amount.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_amount.CornerRadius = 0;
      this.txt_amount.Dock = System.Windows.Forms.DockStyle.Fill;
      this.txt_amount.Font = new System.Drawing.Font("Open Sans Semibold", 40F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_amount.Location = new System.Drawing.Point(3, 33);
      this.txt_amount.MaxLength = 9;
      this.txt_amount.Multiline = false;
      this.txt_amount.Name = "txt_amount";
      this.txt_amount.PasswordChar = '\0';
      this.txt_amount.ReadOnly = false;
      this.txt_amount.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_amount.SelectedText = "";
      this.txt_amount.SelectionLength = 0;
      this.txt_amount.SelectionStart = 0;
      this.txt_amount.Size = new System.Drawing.Size(379, 46);
      this.txt_amount.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.CALCULATOR;
      this.txt_amount.TabIndex = 0;
      this.txt_amount.TabStop = false;
      this.txt_amount.Text = "123";
      this.txt_amount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      this.txt_amount.UseSystemPasswordChar = false;
      this.txt_amount.WaterMark = null;
      this.txt_amount.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(153)))), ((int)(((byte)(49)))));
      this.txt_amount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.uc_input_amount_resizable_KeyPress);
      // 
      // uc_input_amount_resizable
      // 
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
      this.BackColor = System.Drawing.Color.Transparent;
      this.Controls.Add(this.tableLayoutPanel3);
      this.Margin = new System.Windows.Forms.Padding(0);
      this.Name = "uc_input_amount_resizable";
      this.Size = new System.Drawing.Size(385, 382);
      this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.uc_input_amount_resizable_MouseMove);
      this.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.uc_input_amount_resizable_PreviewKeyDown);
      this.tableLayoutPanel4.ResumeLayout(false);
      this.tableLayoutPanel4.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pb_close)).EndInit();
      this.tableLayoutPanel1.ResumeLayout(false);
      this.tableLayoutPanel2.ResumeLayout(false);
      this.tableLayoutPanel3.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
    private WSI.Cashier.Controls.uc_round_button btn_money_1;
    private WSI.Cashier.Controls.uc_round_button btn_money_2;
    private WSI.Cashier.Controls.uc_round_button btn_money_3;
    private WSI.Cashier.Controls.uc_round_button btn_money_4;
    private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    private WSI.Cashier.Controls.uc_round_button btn_num_1;
    private WSI.Cashier.Controls.uc_round_button btn_num_2;
    private WSI.Cashier.Controls.uc_round_button btn_num_dot;
    private WSI.Cashier.Controls.uc_round_button btn_intro;
    private WSI.Cashier.Controls.uc_round_button btn_num_3;
    private WSI.Cashier.Controls.uc_round_button btn_back;
    private WSI.Cashier.Controls.uc_round_button btn_num_4;
    private WSI.Cashier.Controls.uc_round_button btn_num_5;
    private WSI.Cashier.Controls.uc_round_button btn_num_9;
    private WSI.Cashier.Controls.uc_round_button btn_num_6;
    private WSI.Cashier.Controls.uc_round_button btn_num_8;
    private WSI.Cashier.Controls.uc_round_button btn_num_7;
    public WSI.Cashier.Controls.uc_round_textbox txt_amount;
    private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
    private WSI.Cashier.Controls.uc_round_button btn_num_0;
    private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
    private PictureBox pb_close;
    private Controls.uc_label lbl_head;
  }
}
