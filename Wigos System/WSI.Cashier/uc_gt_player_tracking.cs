//------------------------------------------------------------------------------
// Copyright � 2007-2014 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_player_tracking.cs
// 
//   DESCRIPTION: Implements the uc_gt_player_tracking user control
//                Class: uc_gt_player_tracking
//
//        AUTHOR: Jos� Mart�nez
// 
// CREATION DATE: 05-JUN-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 05-JUN-2014 JML     First release.
// 15-JUL-2014 DHA     Added legend
// 25-JUL-2014 DHA     Fixed Bug #WIG-1119: set SetLastAction for player tracking gaming tables
// 26-SEP-2014 DHA     Fixed Bug #WIG-1269: corrected gaming table minimum and maximum values when there are no tables on a bank
// 01-OCT-2014 DHA     Fixed Bug #WIG-1350: configure default view when there are no gaming tables createds
// 07-OCT-2014 DHA     Fixed Bug #WIG-1432: using more than one devices generate an error on visualization 
// 28-OCT-2014 DHA&DCS Fixed Bug #WIG-1585: Refresh table design when open table
// 10-DEC-2014 DRV     Decimal currency formatting: DUT 201411 - Tito Chile - Currency formatting without decimals
// 14-MAY-2015 DCS     Fixed Bug #WIG-2350: Incorrect display in report mode
// 06-JUN-2015 YNM     Fixed Bug #WIG-2377: Gambling Table report update
// 26-JUN-2015 FAV     Fixed Bug about "null refence" when the control is showed in the frm_container form.
// 23/OCT-2015 ETP     Fixed Bug-5529 while the gaming table not exist or has change its area.
// 12-NOV-2015 FOS     Backlog Item 5796: Apply new design in form
// 01-DEC-2015 DHA     Fixed Bug-7105: when there are no gaming tables enabled, the menu shows the option to open GT
// 16-JUN-2016 RAB     Product Backlog Item 9853: GamingTables (Phase 3): Fixed Bank (Phase 1)
// 11-JUL-2016 JML     Product Backlog Item 15065: Gaming tables (Fase 4): Player tracking. Multicurrency
// 20-JUL-2016 DLL     Fixed Bug 15757:Gaming tables error permission
// 10-AUG-2016 RAB     Product Backlog Item 15172: GamingTables (Phase 4): Cashier - Adapt report screen in Gaming Tables
// 29-AUG-2016 DHA     Fixed Bug 17113: error showing first fill in gaming tables (Fixed and chips sleeps on table)
// 29-AUG-2016 FOS     Fixed Bug 16903: error handling interacting with gaming day
// 08-SEP-2016 FOS     PBI 16567: Tables: Split PlayerTracking to GamingTables (Phase 5)
// 29-SEP-2016 FOS     Fixed Bug 17813: PlayerSeat IsoCode selected
// 04-OCT-2016 DHA     Fixed Bug 18569: error when changes seat selection, creates a new table session
// 16-NOV-2016 LTC     Bug 19569: Tables: After closing gaming table players hold on the previous state (there is no creen refresh)
// 30-NOV-2016 FOS&FGB PBI 19197: Reopen cashier sessions and gambling tables sessions
// 02-ENE-2017 DPC     Bug 21848:Mesas de Juego sin Isla y sin asientos: label incorrecta
// 11-JAN-2017 RAB     Bug 21956:CASHIER: Application Exception When Table Opened
// 27-JAN-2017 CCG     Fixed Bug 23612:Cajero - Error en la pantalla de mesas de juego
// 31-JAN-2017 DPC     Bug 22357:Reabrir Caja: Al intentar reabrir mesa aparece un pop-up con error de sesi�n inv�lida. La NLS sobre la sesi�n de caja no se reconoce.
// 31-JAN-2017 DHA     Bug 23600: Gaming table permissions request authorized user
// 07-FEB-2017 DHA     Bug 24176: error on closing cashier session
// 07-FEB-2017 ATB     Cashier: Unify messages when user has no permission for any action
// 03-APR-2017 RAB&DHA PBI 26401: MES10 Ticket validation - Ticket status change to cancelled
// 20-APR-2017 RAB     PBI 26809: Drop gaming table information - Actual drop
// 26-APR-2017 RAB     PBI 26811: Drop gaming table information - Drop hourly report
// 27-APR-2017 RAB     Bug 27111: WIGOS-1617 Hourly Drop report is disabled on gaming table session in cashier
// 03-MAY-2017 DHA     PBI 27171:MES10 Ticket validation - Ticket validation at player tracking screen
// 11-MAY-2017 RAB     Bug 27397: WIGOS-1986 Drop sum error on gaming table session in TITO Multicurrency environment
// 17-MAY-2017 RAB     PBI 26811: Drop gaming table information - Drop hourly report (Add hourly drop icon)
// 08-MAY-2017 RAB     PBI 27177: Win / loss - Access to introduction screen
// 17-MAY-2017 RAB     PBI 26811: Drop gaming table information - Drop hourly report (Add win/loss icon)
// 24-MAY-2017 RAB     PBI 27485: WIGOS-983 Win / loss - Introduce the WinLoss - SELECTION SCREEN
// 06-JUN-2017 DHA     PBI 27760:WIGOS-257: MES21 - Gaming table chair assignment with card selection
// 27-JUL-2017 RAB     Bug 29021:WIGOS-4027 String "Leyenda" is not translated into English for "Gambling Tables" information from Cashdesk menu
// 01-AUG-2017 RAB     Bug 29058:WIGOS-4093 Tables - The validation of dealer copy ticket doens't allow to seat the player when the report is shown
// 02-AUG-2017 MS      Bug 29151:Wigos-4135: Gambling tables report with integrated cashier is not been refreshing with actions from cashier
// 07-AUG-2017 RAB     Bug 29201:WIGOS-4245 The cashier is not applying properly the conversion between currency exchanges once the user introduce a new currency in a openened gambling table session
// 05-SEP-2017 DHA     PBI 28630:WIGOS-3324 MES22 - Gaming table chair assignment with linked gaming tables and card selection
// 07-SEP-2017 DHA     Bug 29623:WIGOS-4190 Tables - sell chips on integrated cashier doesn't show the message "Ticket validated MXN" the first time after selection the seat
// 08-SEP-2017 DHA     Bug 29665:WIGOS-4986 Gaming tables - Sales chips with national currency is possible validate it on gaming table without national currency operative.
// 22-SEP-2017 RAB     Bug 29823: WIGOS-4984 [Ticket #8492] Gambling Table out of working day - Exception v03.006.0023
// 14-FEB-2018 RGR     Bug 31533: WIGOS-5293 Gambling tables: user are enabled on player tracking when the gaming table has been closed 
// 22-FEB-2018 AGS     Bug 31566:WIGOS-8129 [Ticket #12342] Resumen de Caja Homologaci�n de Nombres y Montos Secci�n Fichas V03.06.0035
// 06-MAR-2018 EOR     Bug 31807: WIGOS-8336 Wrong behaviour in Gambling table after swiping a player card
// 20-JUN-2018 DLM     Bug 33209: WIGOS-13058 Enviar feedback Gambling tables: The gambling table type and name is not informed in "movimientos de juego en mesa" report when the ticket is validated in cashier in only one movement.
// 21-JUN-2018 AGS     Bug 33237:WIGOS-13127 Gambling tables: Gambling table with integrated cashier terminal name (TABLE) is lost when a different gambling table is closed.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using WSI.Common;
using System.Drawing.Imaging;
using System.Reflection;
using System.Collections;
using WSI.Common.ReopenSessions;


namespace WSI.Cashier
{
  public partial class uc_gt_player_tracking : UserControl
  {
    #region Enums

    private enum MENU_GT_GROUPS
    {
      MENU_TABLE_CASH = 0
    }

    public enum GT_VIEW_MODE
    {
      UNDEFINED = 0,
      NORMAL = 1,
      REPORT = 2,
    }

    #endregion // Enums

    #region Constants

    // Report Grid Constants
    private const Int32 GRID_COLUMN_DATE = 0;
    private const Int32 GRID_COLUMN_ID_MOVE = 1;
    private const Int32 GRID_COLUMN_TYPE_ID = 2;
    private const Int32 GRID_COLUMN_TYPE_NAME = 3;
    private const Int32 GRID_COLUMN_CALCULATED_AMOUNT = 4;
    private const Int32 GRID_COLUMN_CALCULATED_AMOUNT_FORMATTED = 5;
    private const Int32 GRID_COLUMN_ISO_CODE = 6;
    private const Int32 GRID_COLUMN_CURRENCY_TYPE = 7;
    private const Int32 GRID_COLUMN_CURRENCY_TYPE_DESCRIPTION = 8;
    private const Int32 GRID_COLUMN_IS_OPENER_MOVEMENT = 9;

    private const int TIME_TO_COLLAPSE = 3000;

    #endregion Constants // Constants

    #region Class Atributes

    private frm_container m_parent;
    private static frm_yesno form_yes_no;

    private Boolean m_has_execute_permission;

    private GT_VIEW_MODE m_view_mode = GT_VIEW_MODE.UNDEFINED;

    GamingTable m_gaming_table;
    Seat m_selected_seat;
    Int64 m_selected_seat_key;

    Seat m_initial_seat_for_swap;
    Int32 m_bank_id = -1;
    private Int64 m_thread_sequence;

    PlayerTracking_InterfaceRefresh m_thread;

    uc_gt_player_in_session.TextBoxSelected m_text_box_selected;

    private delegate void OnClickMenuItem();
    private uc_bank m_uc_bank;

    private String m_table_selected_iso_code;

    String m_selected_currency;

    private Boolean m_seat_player_from_chips_sell;
    private CardData m_card_data_from_chips_sell;
    private Ticket m_ticket_from_chips_sell;

    #endregion // Class Atributes

    #region Constructor

    public uc_gt_player_tracking()
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[UC LOAD] uc_gt_player_tracking", Log.Type.Message);

      InitializeComponent();
      InitControls();
      InitializeResources();

      m_uc_bank = new uc_bank();
      m_uc_bank.Visible = false;

      form_yes_no = new frm_yesno();
      form_yes_no.Opacity = 0.6;

      m_gaming_table = null;
      m_selected_seat = new Seat();

      m_seat_player_from_chips_sell = false;
      m_card_data_from_chips_sell = null;
      m_ticket_from_chips_sell = null;

      if (GamingTableBusinessLogic.GamingTablesMode() == GamingTableBusinessLogic.GT_MODE.GUI_AND_CASHIER)
      {
        m_thread = new PlayerTracking_InterfaceRefresh();
        m_thread.Init();
        m_thread.GamingTablesValuesChangedEvent += new PlayerTracking_InterfaceRefresh.GamingTablesValuesChanged(PlayerTracking_InterfaceRefresh_GamingTablesValuesChangedEvent);
      }
      transparentPanel1.Parent = this;

      // Set DoubleBuffer to avoid flashing on bottom panel
      typeof(Control).InvokeMember("DoubleBuffered", BindingFlags.SetProperty | BindingFlags.Instance | BindingFlags.NonPublic, null, pnl_bottom, new object[] { true });

      // Init events
      btn_table_iso_code.Click += new EventHandler(btn_table_iso_code_Click);

      if (!GamingTableBusinessLogic.IsGamingTablesEnabledAndPlayerTracking())
      {
        InitControlsGamingTablesParameters();
      }

      if (GamingTableBusinessLogic.IsEnableCashDeskDealerTicketsValidation())
      {
        KeyboardMessageFilter.RegisterHandler(this, BarcodeType.FilterAnyTito, TicketReceivedEvent);
        uc_gt_dealer_copy_ticket_reader1.OnValidationNumberReadEvent += uc_gt_dealer_copy_ticket_reader1_OnValidationNumberReadEvent;
      }
    }

    private void InitControlsGamingTablesParameters()
    {
      btn_play_pause.Visible = false;
      lbl_speed.Visible = false;
      cb_table_speed.Visible = false;
      btn_legend.Visible = false;
      lbl_bet.Visible = false;
      lbl_max_bet_name.Visible = false;
      lbl_max_bet_value.Visible = false;
      lbl_min_bet_name.Visible = false;
      lbl_min_bet_value.Visible = false;
      btn_table_iso_code.Visible = false;
    }

    private void TicketReceivedEvent(KbdMsgEvent e, Object data)
    {
      Barcode _barcode;

      _barcode = null;

      if (m_seat_player_from_chips_sell)
      {
        return;
      }

      if (e == KbdMsgEvent.EventBarcode)
      {
        if (m_gaming_table.GamingTableSession != null && m_gaming_table.GamingTableSession.CashierSessionId > 0 &&
              m_selected_seat_key > 0 && m_gaming_table.Seats[m_selected_seat_key].PlaySession != null)
        {
          if (!m_gaming_table.GamingTablePaused & (TITO.BusinessLogic.IsModeTITO || (m_gaming_table.Seats[m_selected_seat_key].PlaySession != null && CurrencyExchange.GetNationalCurrency() == m_gaming_table.Seats[m_selected_seat_key].PlaySession.PlayerIsoCode)))
          {
            _barcode = (Barcode)data;

            uc_gt_dealer_copy_ticket_reader1.AutoTicketReadMode(_barcode);
          }
        }
      }
    }

    private void InitControlsGamingTablesParametersWithPlayerTracking()
    {
      btn_play_pause.Visible = true;
      lbl_speed.Visible = true;
      cb_table_speed.Visible = true;
      btn_legend.Visible = true;
      lbl_bet.Visible = true;
      lbl_max_bet_name.Visible = true;
      lbl_max_bet_value.Visible = true;
      lbl_min_bet_name.Visible = true;
      lbl_min_bet_value.Visible = true;
      btn_table_iso_code.Visible = true;
    }

    void PlayerTracking_InterfaceRefresh_GamingTablesValuesChangedEvent(Int64 ThreadTimeStamp, Dictionary<Int32, GamingTable> GamingTablesOut)
    {
      Dictionary<Int32, ScrollableButtonItem> _scrollable_buttons;

      if (this.InvokeRequired)
      {
        Delegate _method;

        _method = null;

        try
        {
          _method = new PlayerTracking_InterfaceRefresh.GamingTablesValuesChanged(PlayerTracking_InterfaceRefresh_GamingTablesValuesChangedEvent);

          if (_method != null)
          {
            this.BeginInvoke(_method, new object[] { ThreadTimeStamp, GamingTablesOut });
          }
          else
          {
            Log.Error("Gamingtable changed: nobody taking care of the event, the method is null.");
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }

        return;
      }

      if (ThreadTimeStamp <= m_thread_sequence)
      {
        return;
      }

      m_thread_sequence = Math.Max(m_thread_sequence, ThreadTimeStamp + 1);

      if (m_gaming_table == null || (GamingTablesOut != null && !GamingTablesOut.ContainsKey(m_gaming_table.GamingTableId)))
      {
        // If selected gambling table has been changed the bank
        if (GamingTablesOut.Count > 0)
        {
          // Load first gambling table from selected bank
          foreach (KeyValuePair<Int32, GamingTable> _table in GamingTablesOut)
          {
            m_gaming_table = _table.Value;
            LoadDesignTable();

            break;
          }
        }
        else
        {
          m_gaming_table = null;
          gamingTablePanel1.Clean();
        }
      }
      else if (m_gaming_table != null)
      {
        m_gaming_table = GamingTablesOut[m_gaming_table.GamingTableId];

        if (m_selected_seat_key != 0)
        {
          if ((m_gaming_table.Seats[m_selected_seat_key].PlayerType == GTPlayerType.Unknown) && (m_view_mode != GT_VIEW_MODE.REPORT))
          {
            uc_gt_card_reader1.Visible = true;
          }
          else
          {
            uc_gt_card_reader1.Visible = false;
          }

          uc_gt_player_in_session1.Seat = m_gaming_table.Seats[m_selected_seat_key];
        }
      }

      if (m_gaming_table != null) // LTC 16-NOV-2016
      {
        gamingTablePanel1.UpdateGamingTableAndSeatInfo(m_gaming_table);
      }

      // Refresh buttons
      _scrollable_buttons = CreateScrollButtonsList(GamingTablesOut);
      uc_scrollable_button_list1.RefreshButtons(_scrollable_buttons);
      uc_scrollable_button_list1.Visible = (_scrollable_buttons.Count > 0);

      ShowHidePanelTransparent();
      SetScreenGamingTableData();

      if ((m_gaming_table == null) || ((m_gaming_table.GamingTableSession == null) && (m_view_mode == GT_VIEW_MODE.REPORT)))
      {
        m_view_mode = GT_VIEW_MODE.NORMAL;
        SetViewMode(false);
      }
    }

    public void SetScreenGamingTableData()
    {
      String _min_bet;
      String _max_bet;
      String _table_iso_code;

      _min_bet = "---";
      _max_bet = "---";
      _table_iso_code = String.Empty;

      if (m_gaming_table != null)
      {
        //Refresh other gaming table fields
        InitTableIsoCodeButtom();

        if (m_table_selected_iso_code != null && m_gaming_table.TableAceptedIsoCode != null && m_gaming_table.TableAceptedIsoCode.Count > 0)
        {
          _table_iso_code = m_table_selected_iso_code;
          m_gaming_table.BetMin = m_gaming_table.TableAceptedIsoCode[m_table_selected_iso_code].MinBet;
          m_gaming_table.BetMax = m_gaming_table.TableAceptedIsoCode[m_table_selected_iso_code].MaxBet;

          if (m_gaming_table.BetMax > 0)
          {
            _max_bet = m_gaming_table.BetMax.ToString(false);
          }

          if (m_gaming_table.BetMin > 0)
          {
            _min_bet = m_gaming_table.BetMin.ToString(false);
          }
        }

        uc_gt_player_in_session1.TableMinBet = m_gaming_table.BetMin;
        uc_gt_player_in_session1.TableMaxBet = m_gaming_table.BetMax;
        uc_gt_player_in_session1.TableIdlePlays = m_gaming_table.TableIdlePlays;

        this.cb_table_speed.SelectedIndexChanged -= new EventHandler(this.cb_speed_SelectedIndexChanged);

        if (m_gaming_table.SelectedSpeed != GTPlayerTrackingSpeed.Unknown)
        {
          cb_table_speed.SelectedValue = (int)m_gaming_table.SelectedSpeed;
        }
        else
        {
          cb_table_speed.SelectedValue = (int)GTPlayerTrackingSpeed.Medium;
        }

        this.cb_table_speed.SelectedIndexChanged += new EventHandler(this.cb_speed_SelectedIndexChanged);

        if (m_gaming_table.GamingTablePaused)
        {
          btn_play_pause.Image = Resources.ResourceImages.play1;
        }
        else
        {
          btn_play_pause.Image = Resources.ResourceImages.pause1;
        }
      }
      else
      {
        btn_play_pause.Enabled = false;
        cb_table_speed.Enabled = false;
      }

      if (m_table_selected_iso_code != null)
      {
        btn_table_iso_code.Text = _table_iso_code;
      }
      else
      {
        btn_table_iso_code.Text = m_selected_currency;
      }

      lbl_min_bet_value.Text = _min_bet;
      lbl_max_bet_value.Text = _max_bet;

      //SetBottomPanelColor();
      SetEnabledControls();

      if (lbl_drop_national_currency.Visible && m_gaming_table.GamingTableSession != null)
      {
        lbl_drop_national_currency.Text = Resource.String("STR_GAMING_TABLE_REPORT_DROP") + " (" + CurrencyExchange.GetNationalCurrency().ToUpper() + "): ";
        lbl_drop_national_currency_amount.Text = m_gaming_table.GamingTableSession.ValidatedDealerCopyAmount.ToString(false);
      }
      else
      {
        lbl_drop_national_currency.Text = String.Empty;
        lbl_drop_national_currency_amount.Text = String.Empty;
      }
    }

    /// <summary>
    /// Change position of the controls according to the permission of drop in gaming table
    /// </summary>
    /// <param name="DropVisible">Permission of drop in gaming table and gaming session ID different a null</param>
    private void SetControlsPositions(Boolean DropVisible)
    {
      if (DropVisible)
      {
        cb_table_speed.DropDownWidth = 100;
        cb_table_speed.Width = 100;
        lbl_drop_national_currency.Location = new Point(cb_table_speed.Location.X + cb_table_speed.Width + 10, lbl_drop_national_currency.Location.Y);
        lbl_drop_national_currency_amount.Location = new Point(lbl_drop_national_currency.Location.X + lbl_drop_national_currency.Width + 5, lbl_drop_national_currency_amount.Location.Y);
      }
      else
      {
        cb_table_speed.DropDownWidth = 160;
        cb_table_speed.Width = 160;
      }

      lbl_drop_national_currency.Visible = DropVisible;
      lbl_drop_national_currency_amount.Visible = DropVisible;

    }

    //------------------------------------------------------------------------------
    // PURPOSE : Add event handler to scan and configure buttons
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    private void InitControls()
    {
      EventLastAction.AddEventLastAction(this.Controls);

      btn_legend.Click += new System.EventHandler(btn_legend_Click);
      uc_gt_card_reader1.AccountReadWithTrackNumber += new uc_gt_card_reader.TrackNumberReadEventHandler(uc_gt_card_reader1_AccountReadWithTrackNumber);
      uc_gt_player_in_session1.TextBoxEnterEvent += new uc_gt_player_in_session.TexBoxEnter(uc_gt_player_in_session1_TextBoxEnterEvent);
      uc_gt_player_in_session1.ButtonClickedInPlayerInSession += new uc_gt_player_in_session.ButtonClick(uc_gt_player_in_session1_ButtonClicked);
      uc_gt_player_in_session1.ValueChangedInSession += new uc_gt_player_in_session.ValueChanged(uc_gt_player_in_session1_ValueChangedInSession);
      uc_gt_player_in_session1.ValuePlayerIsoCodeChangedInSession += new uc_gt_player_in_session.ValuePlayerIsoCodeChanged(uc_gt_player_in_session1_ValuePlayerIsoCodeChanged);
      gamingTablePanel1.SelectedSeat += new GamingTablePanel.SelectedSeatEventHandler(gamingTablePanel1_SelectedSeat);
      uc_input_amount_resizable1.OnAmountSelected += new uc_input_amount_resizable.AmountSelectedEventHandler(uc_input_amount_resizable1_OnAmountSelected);
      uc_input_amount_resizable1.AcceptDecimals = Format.GetFormattedDecimals(CurrencyExchange.GetNationalCurrency()) > 0;
      cb_banks.SelectedIndexChanged += new EventHandler(cb_bank_SelectedIndexChanged);

    }

    void uc_gt_player_in_session1_ValueChangedInSession(Seat Seat)
    {
      if (m_gaming_table != null && Seat != null && m_selected_seat != null)
      {
        m_gaming_table.Seats[m_selected_seat.SeatId] = Seat;
        m_selected_seat = Seat;

        m_thread_sequence = m_thread.ForceRefresh();

        gamingTablePanel1.UpdateSeatInfo(Seat);
      }
    }

    private void InitializeResources()
    {
      lbl_legend_end_session.Text = Resource.String("PLAYER_TRACKING_LEGEND_END_SESSION");
      lbl_legend_resume_session.Text = Resource.String("PLAYER_TRACKING_LEGEND_RESUME_SESSION");
      lbl_legend_pause_session.Text = Resource.String("PLAYER_TRACKING_LEGEND_PAUSE_SESSION");
      lbl_legend_swap_position.Text = Resource.String("PLAYER_TRACKING_LEGEND_SWAP_POSITION");
      lbl_legend_free_seat.Text = Resource.String("PLAYER_TRACKING_LEGEND_FREE_SEAT");
      lbl_legend_taken_seat.Text = Resource.String("PLAYER_TRACKING_LEGEND_TAKEN_SEAT");
      lbl_legend_away_player.Text = Resource.String("PLAYER_TRACKING_LEGEND_AWAY_PLAYER");
      lbl_legend_disabled_seat.Text = Resource.String("PLAYER_TRACKING_LEGEND_DISABLED_SEAT");

      btn_play_pause.Image = Resources.ResourceImages.play1;
      btn_play_pause.BackColor = Color.FromArgb(80, 90, 102);
      btn_menu_table_mode.Image = Resources.ResourceImages.menuTable;
      btn_menu_table_mode.BackColor = Color.FromArgb(80, 90, 102);
      btn_legend.Image = Resources.ResourceImages.info_letter;
      btn_legend.BackColor = Color.FromArgb(80, 90, 102);

      pb_legend_end_session.Image = Resources.ResourceImages.endSession;
      pb_legend_resume_session.Image = Resources.ResourceImages.play;
      pb_legend_pause_session.Image = Resources.ResourceImages.pause;
      pb_legend_swap_position.Image = Resources.ResourceImages.swapPosition;

      // Draw seats image on legend menu
      GamingSeatProperties _seat = new GamingSeatProperties();
      _seat.SeatPosition = 1;

      _seat.SeatStatus = SEAT_STATUS_PLAYER_TRACKING.FREE;
      pb_legend_free_seat.Image = UtilsDesign.GetDrawSeatImage(_seat);

      _seat.SeatStatus = SEAT_STATUS_PLAYER_TRACKING.OCUPATED;
      pb_legend_taken_seat.Image = UtilsDesign.GetDrawSeatImage(_seat);

      _seat.SeatStatus = SEAT_STATUS_PLAYER_TRACKING.WALK;
      pb_legend_away_player.Image = UtilsDesign.GetDrawSeatImage(_seat);

      _seat.SeatStatus = SEAT_STATUS_PLAYER_TRACKING.DISABLED;
      pb_legend_disabled_seat.Image = UtilsDesign.GetDrawSeatImage(_seat);

      m_selected_currency = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode");

      lbl_max_bet_name.Text = Resource.String("STR_CASHIER_GAMING_TABLE_MAX_BET") + ": ";
      lbl_bet.Text = Resource.String("STR_CASHIER_GAMING_TABLE_TAB_BET");
      btn_table_iso_code.Text = m_selected_currency;
      lbl_min_bet_name.Text = Resource.String("STR_CASHIER_GAMING_TABLE_MIN_BET") + ": ";
      lbl_speed.Text = Resource.String("STR_CASHIER_GAMING_TABLE_SPEED");

    }

    private void uc_input_amount_resizable1_OnAmountSelected(decimal Amount)
    {
      switch (m_text_box_selected)
      {
        case uc_gt_player_in_session.TextBoxSelected.Played_Current:
          uc_gt_player_in_session1.PlayedCurrent = Amount;

          break;

        case uc_gt_player_in_session.TextBoxSelected.Points_Won:
          uc_gt_player_in_session1.PointsWon = Amount;

          break;
        case uc_gt_player_in_session.TextBoxSelected.Chips_In:
          uc_gt_player_in_session1.ChipsIn = Amount;

          break;
        case uc_gt_player_in_session.TextBoxSelected.Chips_Out:
          uc_gt_player_in_session1.ChipsOut = Amount;

          break;

        case uc_gt_player_in_session.TextBoxSelected.Chips_Sell:
          uc_gt_player_in_session1.Sale_Chips(null, Amount);

          break;
      }

      uc_input_amount_resizable1.Hide_KeyBoard();
      uc_gt_dealer_copy_ticket_reader1.Hide_DealerCopyValidation();
    }

    #endregion // Constructor

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE: Initialize controls. 
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    public void InitControls(frm_container Parent)
    {
      String _error_str;

      m_parent = Parent;
      m_uc_bank.SetParent(m_parent);
      this.ParentForm.Controls.Add(m_uc_bank);

      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.PlayerTrackingExecute,
                                         ProfilePermissions.TypeOperation.OnlyCheck,
                                         m_parent,
                                         out _error_str))
      {
        m_has_execute_permission = false;
      }
      else
      {
        m_has_execute_permission = true;
      }

      if (!GamingTableBusinessLogic.IsGamingTablesEnabledAndPlayerTracking())
      {
        m_view_mode = GT_VIEW_MODE.REPORT;
        SetViewMode(false);
      }

      LoadBanksFilter();

      //uc_gt_card_reader1.Visible = false;
      uc_gt_player_in_session1.Enabled = m_has_execute_permission;
      uc_gt_player_in_session1.SetControls();
      uc_input_amount_resizable1.Visible = false;
      uc_gt_dealer_copy_ticket_reader1.Visible = false;

      InitSpeedCombo();

      InitTableIsoCodeButtom();

      InitializeControlResources();

      ShowHidePanelTransparent();

      if (m_view_mode == GT_VIEW_MODE.UNDEFINED)
      {
        // Hide report mode controls
        gamingTablePanel1.ReadOnly = false;
        pnl_report.Visible = false;
        dgv_gt_movememnts.Visible = false;
      }

      UpdateReportInformation();

    } // InitControls

    //------------------------------------------------------------------------------
    // PURPOSE: Initialize control resources. (NLS string, images, etc.).
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    public void InitializeControlResources()
    {
      // - NLS Strings:
      //lbl_table.Text = Resource.String("STR_GT_TABLE");

    } // InitializeControlResources

    public void AutomaticCopyDealervalidation(Int32 GamingTableId, CardData CardData, Ticket TicketCopyDealer)
    {
      Int32 _seats_free_count;
      Boolean _player_is_seat_at_table;
      Boolean _is_valid;

      _seats_free_count = 0;
      _player_is_seat_at_table = false;
      _is_valid = true;

      // Show gaming table
      m_bank_id = -1;
      LoadBanksFilter();

      if (m_thread != null)
      {
        m_thread.BankId = m_bank_id;
      }

      m_view_mode = GT_VIEW_MODE.NORMAL;
      SetViewMode(false);

      uc_scrollable_button_list1.SetSelectedButton(GamingTableId);
      uc_scrollable_gambling_tables_ButtonClicked(GamingTableId);

      if (m_gaming_table.GamingTablePaused)
      {
        return;
      }

      if (m_gaming_table != null && m_gaming_table.GamingTableId == GamingTableId && m_gaming_table.GamingTableSession != null)
      {
        foreach (KeyValuePair<Int64, Seat> _seat_item in m_gaming_table.Seats)
        {
          if (_seat_item.Value.AccountId == CardData.AccountId)
          {
            // Player is at table
            _player_is_seat_at_table = true;

            gamingTablePanel1.SetSelectedSeatAtGamingTable(_seat_item.Key);

            m_selected_seat_key = _seat_item.Key;
            m_selected_seat = _seat_item.Value;

            break;
          }

          if (_seat_item.Value.PlaySession == null || _seat_item.Value.PlaySession.AccountId == 0)
          {
            // there are free seats?
            _seats_free_count++;
          }
        }
      }

      if (_player_is_seat_at_table)
      {
        uc_gt_player_in_session1.Seat = m_selected_seat;

        uc_gt_dealer_copy_ticket_reader1_OnValidationNumberReadEvent(ref _is_valid, TicketCopyDealer);

      }
      else if (_seats_free_count > 0)
      {
        gamingTablePanel1.CleanSeatSelection();
        uc_scrollable_button_list1.Enabled = false;
        cb_banks.Enabled = false;
        uc_gt_player_in_session1.EnabledControls(false);
        uc_gt_card_reader1.Enabled = false;
        uc_gt_card_reader1.SendToBack();
        pnl_bottom.Enabled = false;

        uc_gt_player_in_session1.Seat = null;

        m_seat_player_from_chips_sell = true;

        m_card_data_from_chips_sell = CardData;
        m_ticket_from_chips_sell = TicketCopyDealer;

        uc_gt_player_in_session1.ShowHideSeatUserLabel(true);
      }
      else
      {
        String _message;

        _message = Resource.String("STR_PLAYER_TRACKING_MSG_THERE_ARE_NOT_AVAILABLE_SEATS");

        gamingTablePanel1.CleanSeatSelection();
        uc_gt_player_in_session1.Seat = null;
        uc_gt_card_reader1.Enabled = false;

        if (m_gaming_table.GamingTableSession == null)
        {
          _message = Resource.String("STR_PLAYER_TRACKING_MSG_GAMING_TABLE_IS_CLOSED");
        }
        frm_message.Show(_message, Resource.String("STR_APP_GEN_MSG_WARNINGS"), MessageBoxButtons.OK, MessageBoxIcon.Warning, this.ParentForm);
      }
    }

    #endregion // Public Methods

    #region Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE: initialize combo box
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    private void InitSpeedCombo()
    {
      cb_table_speed.SelectedIndexChanged -= new EventHandler(cb_speed_SelectedIndexChanged);

      DataTable _dt;
      cb_table_speed.DataSource = null;
      cb_table_speed.Items.Clear();

      // Speed combo
      _dt = new DataTable();

      _dt.Columns.Add("Key", Type.GetType("System.Int32")).AllowDBNull = false;
      _dt.Columns.Add("Value", Type.GetType("System.String")).AllowDBNull = false;

      _dt.Rows.Add(GTPlayerTrackingSpeed.Slow, Resource.String("STR_FRM_PLAYER_TRACKING_CB_SLOW"));
      _dt.Rows.Add(GTPlayerTrackingSpeed.Medium, Resource.String("STR_FRM_PLAYER_TRACKING_CB_MEDIUM"));
      _dt.Rows.Add(GTPlayerTrackingSpeed.Fast, Resource.String("STR_FRM_PLAYER_TRACKING_CB_FAST"));

      cb_table_speed.DataSource = _dt;
      cb_table_speed.ValueMember = "Key";
      cb_table_speed.DisplayMember = "Value";

      if (m_gaming_table != null && m_gaming_table.SelectedSpeed != GTPlayerTrackingSpeed.Unknown)
      {
        cb_table_speed.SelectedValue = (int)m_gaming_table.SelectedSpeed;
      }
      else
      {
        cb_table_speed.SelectedValue = (int)GTPlayerTrackingSpeed.Medium;
      }

      cb_table_speed.SelectedIndexChanged += new EventHandler(cb_speed_SelectedIndexChanged);

    }  // InitSpeedCombo

    //------------------------------------------------------------------------------
    // PURPOSE: initialize combo box
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    private void InitTableIsoCodeButtom()
    {
      String _first_iso_code;
      Boolean _found;

      _first_iso_code = String.Empty;
      _found = false;

      if (m_gaming_table != null && m_gaming_table.TableAceptedIsoCode != null)
      {
        if (m_gaming_table.TableAceptedIsoCode.Count > 0)
        {
          foreach (String _key in m_gaming_table.TableAceptedIsoCode.Keys)
          {
            if (_first_iso_code == String.Empty)
            {
              _first_iso_code = _key;
            }

            if (m_table_selected_iso_code == _key)
            {
              _found = true;

              break;
            }
          }

          if (!_found)
          {
            m_table_selected_iso_code = _first_iso_code;
          }
        }
      }

    }  // InitTableIsoCodeButtom

    //------------------------------------------------------------------------------
    // PURPOSE: Read card for register a new player. 
    // 
    //  PARAMS:
    //      - INPUT:
    //              
    //      - OUTPUT:
    //
    // RETURNS: 
    // 
    //   NOTES:
    //  
    private void RegisterPlayer()
    {
      if (m_selected_seat == null)
      {
        uc_gt_player_in_session1.InitControls(m_parent, m_selected_seat, m_gaming_table.BetMax, m_gaming_table.BetMin, m_gaming_table.TableIdlePlays);
        uc_input_amount_resizable1.Visible = false;
        uc_gt_dealer_copy_ticket_reader1.Visible = false;
        uc_gt_card_reader1.Visible = false;
      }
      else if (m_selected_seat.PlayerType != GTPlayerType.Unknown)
      {
        m_selected_seat.PlaySession.RegisterMode = m_gaming_table.Seats[m_selected_seat_key].PlaySession.RegisterMode;
        ShowPlayer();
      }
      else
      {
        uc_gt_player_in_session1.InitControls(m_parent, m_selected_seat, m_gaming_table.BetMax, m_gaming_table.BetMin, m_gaming_table.TableIdlePlays);
        uc_gt_player_in_session1.SetControls();
        uc_input_amount_resizable1.Visible = false;
        uc_gt_dealer_copy_ticket_reader1.Visible = false;

        if (m_selected_seat_key > 0)
        {
          uc_gt_card_reader1.Visible = true;
          uc_gt_card_reader1.BringToFront();
          //uc_gt_card_reader1.Enabled = m_has_execute_permission;
          uc_gt_card_reader1.InitControls(m_parent, m_selected_seat);
          uc_gt_card_reader1.Focus();
          uc_gt_card_reader1.InitFocus();
        }
      }
    } // RegisterPlayer

    //------------------------------------------------------------------------------
    // PURPOSE: Show player data of the selected seat, and it can updated. 
    // 
    //  PARAMS:
    //      - INPUT: 
    //               
    //      - OUTPUT:
    //
    // RETURNS: 
    // 
    //   NOTES:
    //  
    private void ShowPlayer()
    {
      if (m_selected_seat_key != 0)
      {
        m_gaming_table.Seats[m_selected_seat_key].GamingTableId = m_gaming_table.GamingTableId;
        uc_gt_card_reader1.Visible = false;

        if (GamingTableBusinessLogic.IsEnableCashDeskDealerTicketsValidation())
        {
          uc_gt_dealer_copy_ticket_reader1.Hide_DealerCopyValidation();
          uc_gt_dealer_copy_ticket_reader1.Visible = m_has_execute_permission;
          uc_gt_dealer_copy_ticket_reader1.BringToFront();
        }
        else
        {
          uc_input_amount_resizable1.Hide_KeyBoard();
          uc_input_amount_resizable1.Visible = m_has_execute_permission;
          uc_input_amount_resizable1.BringToFront();
        }

        uc_gt_dealer_copy_ticket_reader1.Hide_DealerCopyValidation();




        if (m_gaming_table.TableAceptedIsoCode.ContainsKey(m_selected_seat.PlaySession.PlayerIsoCode))
        {
          m_table_selected_iso_code = m_selected_seat.PlaySession.PlayerIsoCode;
        }

        if (m_gaming_table.TableAceptedIsoCode.Count > 0 && m_gaming_table.TableAceptedIsoCode.ContainsKey(m_table_selected_iso_code))
        {
          m_selected_seat.PlaySession.PlayerIsoCode = m_table_selected_iso_code;
          m_gaming_table.BetMin = m_gaming_table.TableAceptedIsoCode[m_table_selected_iso_code].MinBet;
          m_gaming_table.BetMax = m_gaming_table.TableAceptedIsoCode[m_table_selected_iso_code].MaxBet;
        }
        else
        {
          m_gaming_table.BetMin = 0;
          m_gaming_table.BetMax = 0;
        }

        SetScreenGamingTableData();

        uc_gt_player_in_session1.InitControls(m_parent, m_selected_seat, m_gaming_table.BetMax, m_gaming_table.BetMin, m_gaming_table.TableIdlePlays);

        uc_gt_player_in_session1.Focus();
        uc_gt_player_in_session1.InitFocus();
      }
      uc_gt_player_in_session1.SetControls();
    } // ShowPlayer

    //------------------------------------------------------------------------------
    // PURPOSE: Check that new player is not playing in another seat/table
    // 
    //  PARAMS:
    //      - INPUT: Seat
    //               
    //      - OUTPUT:
    //
    // RETURNS: 
    // 
    //   NOTES:
    //  
    private Boolean CheckNewPlayer(Seat Seat)
    {
      StringBuilder _sb;
      Boolean _return = true;

      Int32 _table_id_before;
      Int64 _seat_id_before;
      Boolean _was_he_sitting_in_another_chair;
      Seat _seat_before;
      GamingTable _gaming_table_before;
      Int64 _gaming_table_session_id_before;

      GamingTableBusinessLogic.GT_RESPONSE_CODE _error_code;

      _was_he_sitting_in_another_chair = false;
      _table_id_before = 0;
      _seat_id_before = 0;
      _gaming_table_session_id_before = 0;

      foreach (Seat _element in m_gaming_table.Seats.Values)
      {
        if (_element.AccountId == Seat.AccountId && _element.SeatId != Seat.SeatId)
        {
          _return = false;

          frm_message.Show(Resource.String("STR_PLAYER_TRACKING_MSG_PLAYER_IS_ALREADY_PLAYING_IN_THIS_TABLE", _element.SeatPosition), Resource.String("STR_PLAYER_TRACKING_PLAYER") + " " + _element.HolderName,
                 MessageBoxButtons.OK, MessageBoxIcon.Warning, this.ParentForm);

          break;
        }
      }

      if (_return)
      {
        _sb = new StringBuilder();

        _sb.AppendLine("SELECT   GTPS_GAMING_TABLE_ID ");
        _sb.AppendLine("       , GTPS_SEAT_ID ");
        _sb.AppendLine("       , GTPS_GAMING_TABLE_SESSION_ID ");
        _sb.AppendLine("  FROM   GT_PLAY_SESSIONS ");
        _sb.AppendLine(" WHERE   GTPS_ACCOUNT_ID = @pAccountId ");
        _sb.AppendLine("   AND   GTPS_GAMING_TABLE_ID <> @pGamingTableId ");
        _sb.AppendLine("   AND   GTPS_STATUS = @pStatus ");

        try
        {
          using (DB_TRX _db_trx = new DB_TRX())
          {
            using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
            {
              _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = Seat.AccountId;
              _sql_cmd.Parameters.Add("@pGamingTableId", SqlDbType.Int).Value = Seat.GamingTableId;
              _sql_cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = GTPlaySessionStatus.Opened;

              using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
              {
                if (_reader.Read())
                {
                  _was_he_sitting_in_another_chair = true;
                  _table_id_before = _reader.GetInt32(0);
                  _seat_id_before = _reader.GetInt64(1);
                  _gaming_table_session_id_before = _reader.GetInt64(2);

                  _return = false;
                }
              } // reader
            } // SqlCommand
          } // DB_TRX
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
          _return = false;
        }
      }

      if (!_return)
      {
        if (_was_he_sitting_in_another_chair)
        {
          if (!GamingTableBusinessLogic.ReadSeat(_table_id_before, _seat_id_before, _gaming_table_session_id_before, out _seat_before))
          {
            return false;
          }

          _error_code = GamingTableBusinessLogic.UpdateSeatWalk(_seat_before, Cashier.UserId, Cashier.TerminalId);
          GamingTableBusinessLogic.ReadTable(_seat_before.GamingTableId, out _gaming_table_before);

          frm_message.Show(Resource.String("STR_PLAYER_TRACKING_MSG_PLAYER_IS_ALREADY_PLAYING", _gaming_table_before.GamingTableName, _seat_before.SeatPosition), Resource.String("STR_PLAYER_TRACKING_PLAYER") + " " + Seat.HolderName,
                           MessageBoxButtons.OK, MessageBoxIcon.Warning, this.ParentForm);

          _return = true;
        }
      }

      return _return;
    }  // CheckNewPlayer

    //------------------------------------------------------------------------------
    // PURPOSE: Add new player in gaming table and update values
    // 
    //  PARAMS:
    //      - INPUT: 
    //                - Seat seat_new_player
    //      - OUTPUT:
    //
    // RETURNS: 
    // 
    //   NOTES:
    //  
    private void AddNewPlayer(Seat seat_new_player)
    {
      GamingTableBusinessLogic.GT_RESPONSE_CODE _error_code;

      _error_code = GamingTableBusinessLogic.StartGTPlaySession(seat_new_player, Cashier.UserId, Cashier.TerminalId);

      m_gaming_table.Seats[seat_new_player.SeatId] = seat_new_player;
      seat_new_player.PlaySession.CurrentTableSpeed = m_gaming_table.CurrentSpeed;

      uc_gt_player_in_session1.Seat = seat_new_player;

      _error_code = GamingTableBusinessLogic.UpdateGTPlaySession(seat_new_player, Cashier.UserId, Cashier.TerminalId);

      m_thread_sequence = m_thread.ForceRefresh();

      gamingTablePanel1.UpdateSeatInfo(seat_new_player);

    } // AddNewPlayer

    //------------------------------------------------------------------------------
    // PURPOSE: Fill Banks filter 
    // 
    //  PARAMS:
    //      - INPUT: 
    //               
    //      - OUTPUT:
    //
    // RETURNS: 
    // 
    //   NOTES:
    //  
    private void LoadBanksFilter()
    {
      DataTable _banks;
      Int32 _bank_id;

      cb_banks.SelectedIndexChanged -= new EventHandler(cb_bank_SelectedIndexChanged);

      GamingTableBusinessLogic.ReadBanksWithTablesAssociated((Int32)WSI.Cashier.Cashier.TerminalId, out _banks, out _bank_id);

      if (_banks.Rows.Count == 0)
      {
        lbl_max_bet_value.Text = "---";
        lbl_min_bet_value.Text = "---";
        cb_table_speed.Enabled = false;
        btn_play_pause.Enabled = false;
        uc_scrollable_button_list1.Visible = false;

        return;
      }

      uc_scrollable_button_list1.Visible = true;

      cb_banks.DataSource = _banks;
      cb_banks.ValueMember = "BK_BANK_ID";
      cb_banks.DisplayMember = "BK_NAME";
      cb_banks.SelectedIndexChanged += new EventHandler(cb_bank_SelectedIndexChanged);

      if (m_bank_id == -1)
      {
        //the first time..
        if ((Int32)cb_banks.SelectedValue == _bank_id)
        {
          cb_bank_SelectedIndexChanged(cb_banks, new EventArgs());
        }
        else
        {
          cb_banks.SelectedValue = _bank_id;
        }
      }
      else if (!ComboBoxBanksContains(m_bank_id))
      {
        _bank_id = int.Parse(((DataRowView)cb_banks.Items[0]).Row.ItemArray[0].ToString());
        cb_banks.SelectedValue = _bank_id;
        cb_bank_SelectedIndexChanged(cb_banks, new EventArgs());
      }
      else
      {
        cb_banks.SelectedValue = m_bank_id;
      }

    } //LoadBanksFilter

    private Boolean ComboBoxBanksContains(Int32 BankId)
    {
      try
      {
        foreach (DataRowView _item in cb_banks.Items)
        {
          if ((Int32)_item.Row.ItemArray[0] == BankId)
          {
            return true;
          }
        }
      }
      catch (Exception)
      {
      }

      return false;
    }

    private void LoadDesignTable()
    {
      String _error_str;
      if (m_gaming_table != null)
      {
        GamingTableBusinessLogic.ReadTable(m_gaming_table.GamingTableId, out m_gaming_table);

        if (!gamingTablePanel1.LoadGamingTable(m_gaming_table.GamingTableId, true))
        {
          gamingTablePanel1.Invalidate();
        }

        if (gamingTablePanel1.SelectedObject == null)
        {
          uc_gt_player_in_session1.InitCmbCurrency(m_gaming_table.GamingTableId);
          uc_gt_player_in_session1.InitControls(m_parent, null, m_gaming_table.BetMax, m_gaming_table.BetMin, m_gaming_table.TableIdlePlays);
        }

        uc_gt_player_in_session1.SetControls();
        uc_input_amount_resizable1.Visible = false;
        uc_gt_dealer_copy_ticket_reader1.Visible = false;
        uc_gt_card_reader1.Visible = false;

        gamingTablePanel1.UpdateGamingTableAndSeatInfo(m_gaming_table);
        m_initial_seat_for_swap = null;
      }
      else
      {
        gamingTablePanel1.LoadGamingTable("");
      }

      if (m_view_mode == GT_VIEW_MODE.REPORT)
      {
        if (m_gaming_table.GamingTableSession == null)
        {
          m_view_mode = GT_VIEW_MODE.NORMAL;

          SetViewMode(false);
        }
        else
        {
          UpdateReportInformation();
        }
      }

      SetScreenGamingTableData();

      ShowHidePanelTransparent();

      if (ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.GamingTablesHourlyDropReport,
                                                                        ProfilePermissions.TypeOperation.OnlyCheck,
                                                                        m_parent,
                                                                        out _error_str)
          && m_gaming_table.GamingTableSession != null)
      {
        SetControlsPositions(true);
      }
      else
      {
        SetControlsPositions(false);
      }
    } // LoadDesignTable

    //------------------------------------------------------------------------------
    // PURPOSE: Set Seat With CardData. 
    // 
    //  PARAMS:
    //      - INPUT: CardData: Number read
    //
    //      - OUTPUT:
    //               Seat: Seat with player information
    // RETURNS: 
    // 
    //   NOTES:
    //  
    private void SetSeatWithCardData(CardData CardData, out Seat Seat)
    {

      Seat = m_selected_seat;
      Seat.PlaySession = new TablePlaySession();
      Seat.PlaySession.CurrentTableSpeed = m_gaming_table.CurrentSpeed;

      // DLM Bug 13058. begin
      Seat.PlaySession.GamingTableId = m_gaming_table.GamingTableId;
      Seat.PlaySession.SeatId = Seat.SeatId;
      // DLM Bug 13058. end

      if (CardData.AccountId == 0)
      {
        Seat.PlayerType = GTPlayerType.AnonymousWithoutCard;
      }
      else if (CardData.PlayerTracking.CardLevel == 0)
      {
        Seat.PlayerType = GTPlayerType.AnonymousWithCard;
        Seat.AccountId = CardData.AccountId;
      }
      else
      {
        Seat.PlayerType = GTPlayerType.Customized;
        Seat.AccountId = CardData.AccountId;
      }

      Seat.HolderGender = CardData.PlayerTracking.HolderGender;
      Seat.HolderLevel = CardData.PlayerTracking.CardLevel;
      Seat.Points = CardData.PlayerTracking.TruncatedPoints;

      switch (Seat.PlayerType)
      {
        case GTPlayerType.Unknown:
          Seat.HolderName = "";
          Seat.FirstName = "";

          break;
        case GTPlayerType.AnonymousWithoutCard:
          Seat.HolderName = Resource.String("STR_CASHIER_GAMING_TABLE_PLAYER_UNKNOWN_BUTTON");
          Seat.FirstName = Seat.HolderName;

          break;
        case GTPlayerType.AnonymousWithCard:
          Seat.HolderName = Resource.String("STR_UC_CARD_CARD_HOLDER_ANONYMOUS");
          Seat.FirstName = Seat.HolderName;

          break;
        case GTPlayerType.Customized:
          Seat.HolderName = CardData.PlayerTracking.HolderName;
          Seat.FirstName = CardData.PlayerTracking.HolderName3;

          break;
        default:
          Seat.HolderName = "";
          Seat.FirstName = "";

          break;
      }

      if (CardData.IsRecycled)
      {
        Seat.TrackData = CardData.RECYCLED_TRACK_DATA;
      }
      else
      {
        Seat.TrackData = CardData.VisibleTrackdata();
      }

      if (m_gaming_table.TableAceptedIsoCode.Count > 1)
      {
        Seat.PlaySession.Started_walk = WGDB.Now;
      }

    }  // SetSeatWithCardData

    //------------------------------------------------------------------------------
    // PURPOSE: Enable - Diable controls dependig table states
    // 
    //  PARAMS:
    //      - INPUT: CardData: Number read
    //
    //      - OUTPUT:
    //               Seat: Seat with player information
    // RETURNS: 
    // 
    //   NOTES:
    //  
    private void SetEnabledControls()
    {
      //Update gaming table status in uc_gt_player_in_session for Enable - Disable controls
      if (m_gaming_table != null)
      {
        uc_gt_player_in_session1.GamingTablePaused = m_gaming_table.GamingTablePaused;

        // If table is paused or closed disabled table panel
        if (m_gaming_table.GamingTablePaused || (m_gaming_table.GamingTableSession == null))
        {
          gamingTablePanel1.Enabled = false;
        }
        else
        {
          gamingTablePanel1.Enabled = true;
        }

        if (!m_gaming_table.GamingTablePaused && (m_gaming_table.GamingTableSession != null))
        {
          if (!m_seat_player_from_chips_sell)
          {
            uc_gt_card_reader1.Enabled = m_has_execute_permission;
            cb_table_speed.Enabled = m_has_execute_permission;
          }
        }
        else
        {
          uc_gt_card_reader1.Enabled = false;
          cb_table_speed.Enabled = false;
        }

        if (m_gaming_table.GamingTableSession != null)
        {
          btn_play_pause.Enabled = m_has_execute_permission;
        }
        else
        {
          btn_play_pause.Enabled = false;
        }
      }
      else
      {
        uc_gt_card_reader1.Enabled = false;
        cb_table_speed.Enabled = false;
        btn_play_pause.Enabled = false;
      }
    } // SetEnabledControls

    private void CreateSubMenu(WSI.Cashier.Controls.uc_round_button SelectedButton, CreateMenu.Menu SubMenu, Point ShowLocation)
    {
      // DCS & DDM: Revisar para refactoring.
      // Se deber�a extraer toda la l�gica de esta funcion excepto la creaci�n de los botones y a�adir en una clase.

      ToolStripControlHost _controlHost;
      ToolStripDropDown _popupControl;
      WSI.Cashier.Controls.uc_menu _uc_sub_menu;

      _uc_sub_menu = new Controls.uc_menu();
      _uc_sub_menu.AutoCollapsed = true;
      _uc_sub_menu.TimeToCollapse = GeneralParam.GetInt32("Cashier", "Menu.Timeout.Seconds", 3) * 1000;
      _uc_sub_menu.Style = WSI.Cashier.Controls.uc_menu.MenuStyle.SUB_MENU;
      _uc_sub_menu.FillMenu(SubMenu.GetButtons());

      _controlHost = new ToolStripControlHost(_uc_sub_menu);
      _controlHost.Padding = Padding.Empty;
      _controlHost.Margin = Padding.Empty;

      _popupControl = new ToolStripDropDown();
      _popupControl.AutoSize = false;
      _popupControl.Size = new Size(_controlHost.Size.Width, _controlHost.Size.Height);
      _popupControl.Padding = new Padding(0);
      _popupControl.Margin = new Padding(0);
      _popupControl.DropShadowEnabled = true;
      _popupControl.Items.Add(_controlHost);

      if (SubMenu.MenuType == WSI.Cashier.CreateMenu.MenuType.GAMING_TABLES_MENU)
      {
        _popupControl.Show(SelectedButton, ShowLocation, ToolStripDropDownDirection.AboveRight);
      }
      else
      {
        _popupControl.Show(SelectedButton, ShowLocation, ToolStripDropDownDirection.BelowRight);
      }

      _popupControl.Refresh();
    }

    private void CreateSubMenuTable(object sender, EventArgs e)
    {
      WSI.Cashier.Controls.uc_round_button _button_selected;
      CreateMenu.Menu _sub_menu;
      String _button_last_movements_text;
      Bitmap _button_last_movements_image;
      Boolean _is_button_enabled;
      String _name_button;
      String _text_button;
      Bitmap _image_button;

      _button_selected = (WSI.Cashier.Controls.uc_round_button)sender;
      _sub_menu = new CreateMenu.Menu(WSI.Cashier.CreateMenu.MenuType.GAMING_TABLES_MENU);

      if (m_view_mode == GT_VIEW_MODE.REPORT)
      {
        _button_last_movements_text = "STR_UC_BANK_BTN_GAMING_TABLE_TABLE";
        _button_last_movements_image = Resources.ResourceImages.arrowRight;
      }
      else
      {
        _button_last_movements_text = "STR_UC_BANK_BTN_GAMING_TABLE_LAST_MOVEMENTS";
        _button_last_movements_image = Resources.ResourceImages.report;
      }

      _is_button_enabled = (!m_gaming_table.IsIntegrated) && (m_gaming_table.GamingTableSession == null);

      _sub_menu.AddButton("BTN_GAMING_TABLE_REOPEN", Resource.String("STR_UC_BANK_BTN_GAMING_TABLE_REOPEN"), Resources.ResourceImages.open, btn_gaming_table_reopen_Click, _is_button_enabled);

      _name_button = "BTN_GAMING_TABLE_OPEN";
      _text_button = Resource.String("STR_UC_BANK_BTN_GAMING_TABLE_OPEN");
      _image_button = Resources.ResourceImages.open;

      if (!Cage.IsCageAutoMode() && !m_gaming_table.IsIntegrated && m_gaming_table.GamingTableSession != null)
      {
        _name_button = "BTN_GAMING_TABLE_REQUEST";
        _text_button = Resource.String("STR_UC_GT_PLAYER_TRACKING_REQUEST");
        _image_button = Resources.ResourceImages.request;
        _is_button_enabled = true;
      }

      _sub_menu.AddButton(_name_button, _text_button, _image_button, btn_gaming_table_open_Click, _is_button_enabled);

      _is_button_enabled = (!m_gaming_table.IsIntegrated) && (m_gaming_table.GamingTableSession != null);
      _sub_menu.AddButton("BTN_GAMING_TABLE_CLOSE", Resource.String("STR_UC_BANK_BTN_GAMING_TABLE_CLOSE"), Resources.ResourceImages.close, btn_gaming_table_close_cash_Click, _is_button_enabled);

      _is_button_enabled = (!m_gaming_table.IsIntegrated) && (m_gaming_table.GamingTableSession != null);
      _sub_menu.AddButton("BTN_GAMING_TABLE_FILLER_IN", Resource.String("STR_UC_BANK_BTN_GAMING_TABLE_DEPOSIT"), Resources.ResourceImages.deposit, btn_gaming_table_filler_in_Click, _is_button_enabled);

      _is_button_enabled = (!m_gaming_table.IsIntegrated) && (m_gaming_table.GamingTableSession != null);
      _sub_menu.AddButton("BTN_GAMING_TABLE_FILLER_OUT", Resource.String("STR_UC_BANK_BTN_GAMING_TABLE_WITHDRAW"), Resources.ResourceImages.withdrawal, btn_gaming_table_filler_out_Click, _is_button_enabled);

      if (GamingTableBusinessLogic.IsGamingTablesEnabledAndPlayerTracking())
      {
        _is_button_enabled = (m_gaming_table.GamingTableSession != null);
        _sub_menu.AddButton("BTN_GAMING_TABLE_HISTORY", Resource.String(_button_last_movements_text), _button_last_movements_image, btn_gaming_table_history_Click, _is_button_enabled);
      }

      _is_button_enabled = (m_gaming_table.GamingTableSession != null);
      _sub_menu.AddButton("BTN_GAMING_TABLE_DROP_HOURLY", Resource.String("STR_UC_BANK_BTN_GAMING_TABLE_DROP_HOURLY"), Resources.ResourceImages.hourly_drop, btn_gaming_table_drop_hourly_in_Click, _is_button_enabled);


      if (GamingTableBusinessLogic.IsEnabledGTWinLoss())
      {
        _is_button_enabled = (m_gaming_table.GamingTableSession != null);
        _sub_menu.AddButton("BTN_GAMING_TABLE_WINLOSS", Resource.String("STR_UC_BANK_BTN_GAMING_TABLE_WINLOSS"), Resources.ResourceImages.winloss, btn_gaming_table_winloss_in_Click, _is_button_enabled);

      }

      CreateSubMenu(_button_selected, _sub_menu, new Point(_button_selected.Location.X - 5, _button_selected.Location.Y - 5));
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Menu : Open/close cash for tables
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //              
    // RETURNS: 
    // 
    //   NOTES:
    //  
    private void btn_menu_table_mode_Click(object sender, EventArgs e)
    {
      if (m_gaming_table != null)
      {
        CreateSubMenuTable(sender, e);

        if (GamingTableBusinessLogic.IsEnabledGTPlayerTracking())
        {
          InitControlsGamingTablesParametersWithPlayerTracking();
        }
        else
        {
          InitControlsGamingTablesParameters();
        }
      }
      else
      {
        frm_message.Show(Resource.String("STR_UNAVAILABLE_GAMING_TABLE"), Resource.String("STR_PLAYER_TRACKING"),
                         MessageBoxButtons.OK, MessageBoxIcon.Warning, this.ParentForm);
        Log.Warning("Unavailable gaming table");
      }
    }

    private CashierSessionInfo CreateCashierSessionInfoGamingTable()
    {
      Int32 _user_id;
      String _user_name;
      Int64 _cashier_session_id;
      String _gaming_table_name;

      _cashier_session_id = 0;
      _gaming_table_name = String.Empty;

      CashierSessionInfo _cashier_session_info = Cashier.CashierSessionInfo().Copy();

      using (DB_TRX _db_trx = new DB_TRX())
      {
        WSI.Common.Cashier.GetSystemUser(GU_USER_TYPE.SYS_GAMING_TABLE, _db_trx.SqlTransaction, out _user_id, out _user_name);
        GamingTablesSessions.GetGamingTableName(m_gaming_table.GamingTableId, out  _gaming_table_name, _db_trx.SqlTransaction);
      }

      if (m_gaming_table.GamingTableSession != null)
      {
        _cashier_session_id = m_gaming_table.GamingTableSession.CashierSessionId;
      }

      _cashier_session_info.CashierSessionId = _cashier_session_id;
      _cashier_session_info.TerminalId = m_gaming_table.CashierId.Value;
      _cashier_session_info.TerminalName = _gaming_table_name;
      _cashier_session_info.UserType = GU_USER_TYPE.SYS_GAMING_TABLE;
      _cashier_session_info.UserId = _user_id;
      _cashier_session_info.UserName = _user_name;

      return _cashier_session_info;
    } // CreateCashierSessionInfoGamingTable

    private void ConfigureUcBank(Boolean Restart)
    {
      GamingTablesSessions.GamingTableInfo.GamingTableId = 0;
      GamingTablesSessions.GamingTableInfo.HasDropbox = false;
      GamingTablesSessions.GamingTableInfo.IsEnabled = false;
      GamingTablesSessions.GamingTableInfo.IsGamingTable = false;
      CashierSessionInfo _cashier_session_info;
      m_uc_bank.GamingTableId = null;
      m_uc_bank.CashierSessionInfo = null;

      WSI.Common.CommonCashierInformation.SetCashierInformation(Cashier.SessionId, Cashier.UserId, Cashier.UserName, (Int32)Cashier.TerminalId, Cashier.TerminalName);

      if (!Restart)
      {
        _cashier_session_info = CreateCashierSessionInfoGamingTable();
        m_uc_bank.CashierSessionInfo = _cashier_session_info;
        m_uc_bank.GamingTableId = m_gaming_table.GamingTableId;
        WSI.Common.CommonCashierInformation.SetCashierInformation(_cashier_session_info.CashierSessionId, _cashier_session_info.UserId, _cashier_session_info.UserName, (Int32)_cashier_session_info.TerminalId, _cashier_session_info.TerminalName);
      }
      else
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          GamingTablesSessions.GetGamingTableInfo((Int32)Cashier.TerminalId, _db_trx.SqlTransaction); 
        }
      }
    } // ConfigureUcBank

    //------------------------------------------------------------------------------
    // PURPOSE : Visual effect transition
    //           - For effect translation change fin_location_x/y and put the same location in other parameters
    //           - For effect grow/decrease  change fin_width/height and put the same size other parameters
    //           - For both effect combine both parameters
    //           - If generalParam GamingTable.PlayerTracking - Cashier.TransitionEffects is not enabled change values without effect transition
    //  PARAMS :
    //      - INPUT :
    //        - obj: Control to resize or move
    //        - fin_width: Final width
    //        - fin_height: Final height
    //        - fin_location_x: Final X position into the parent control
    //        - fin_location_y: Final Y position into the parent control
    //      - OUTPUT :
    //
    // RETURNS :
    //
    // NOTES : The control should be autoadjust
    //
    private void TransitionEffect(Control Obj, Decimal FinWidth, Decimal FinHeight, Decimal FinLocationX, Decimal FinLocationY)
    {
      bool _break_width;
      bool _break_height;
      bool _break_location_x;
      bool _break_location_y;
      int _total_steps;
      int _timer_sleep;
      int _start;

      // Width
      Decimal _ini_width;
      Decimal _current_width;
      Decimal _grow_factor_width;

      // Height
      Decimal _ini_height;
      Decimal _current_height;
      Decimal _grow_factor_height;

      // Location X
      Decimal _ini_location_x;
      Decimal _current_location_x;
      Decimal _grow_factor_location_x;

      // Location Y
      Decimal _ini_location_y;
      Decimal _current_location_y;
      Decimal _grow_factor_location_y;

      // If not enabled Effects do nothing
      if (GamingTableBusinessLogic.IsEnabledTransitionEffects())
      {
        _timer_sleep = 5;
        _total_steps = 17;
        _start = Environment.TickCount;

        _break_width = true;
        _break_height = true;
        _break_location_x = true;
        _break_location_y = true;

        _ini_height = Obj.Height;
        _ini_width = Obj.Width;
        _ini_location_x = Obj.Location.X;
        _ini_location_y = Obj.Location.Y;

        _current_height = _ini_height;
        _current_width = _ini_width;
        _current_location_y = _ini_location_y;
        _current_location_x = _ini_location_x;

        _grow_factor_width = Math.Abs((_ini_width - FinWidth) / _total_steps);
        _grow_factor_height = Math.Abs((_ini_height - FinHeight) / _total_steps);
        _grow_factor_location_y = Math.Abs((_ini_location_y - FinLocationY) / _total_steps);
        _grow_factor_location_x = Math.Abs((_ini_location_x - FinLocationX) / _total_steps);

        if (m_view_mode == GT_VIEW_MODE.REPORT)
        {
          do
          {
            // Width
            if (_current_width - _grow_factor_width >= FinWidth && _break_width)
            {
              _current_width -= _grow_factor_width;
            }
            else
            {
              _break_width = false;
            }

            //Height
            if (_current_height - _grow_factor_height >= FinHeight && _break_height)
            {
              _current_height -= _grow_factor_height;
            }
            else
            {
              _break_height = false;
            }

            //Location x 
            if (_current_location_x - _grow_factor_location_x >= FinLocationX && _break_location_x)
            {
              _current_location_x -= _grow_factor_location_x;
            }
            else
            {
              _break_location_x = false;
            }

            //Location y
            if (_current_location_y - _grow_factor_location_y >= FinLocationY && _break_location_y)
            {
              _current_location_y -= _grow_factor_location_y;
            }
            else
            {
              _break_location_y = false;
            }

            Obj.SuspendLayout();

            Obj.Width = decimal.ToInt32(Decimal.Round(_current_width));
            Obj.Height = decimal.ToInt32(Decimal.Round(_current_height));
            Obj.Location = new Point(decimal.ToInt32(Decimal.Round(_current_location_x)), decimal.ToInt32(Decimal.Round(_current_location_y)));
            Obj.ResumeLayout();

            if (Environment.TickCount - _start <= _timer_sleep)
            {
              System.Threading.Thread.Sleep(_timer_sleep);
              _start = Environment.TickCount;
            }

            Obj.Refresh();

          } while (_break_width & _break_height & _break_location_x & _break_location_y);
        }
        else
        {
          do
          {
            // Width
            if (_current_width + _grow_factor_width <= FinWidth && _break_width)
            {
              _current_width += _grow_factor_width;
            }
            else
            {
              _break_width = false;
            }

            //Height
            if (_current_height + _grow_factor_height <= FinHeight && _break_height)
            {
              _current_height += _grow_factor_height;
            }
            else
            {
              _break_height = false;
            }

            //Location x 
            if (_current_location_x + _grow_factor_location_x <= FinLocationX && _break_location_x)
            {
              _current_location_x += _grow_factor_location_x;
            }
            else
            {
              _break_location_x = false;
            }

            //Location y 
            if (_current_location_y + _grow_factor_location_y <= FinLocationY && _break_location_y)
            {
              _current_location_y += _grow_factor_location_y;
            }
            else
            {
              _break_location_y = false;
            }

            Obj.SuspendLayout();

            Obj.Width = decimal.ToInt32(Decimal.Round(_current_width));
            Obj.Height = decimal.ToInt32(Decimal.Round(_current_height));
            Obj.Location = new Point(decimal.ToInt32(Decimal.Round(_current_location_x)), decimal.ToInt32(Decimal.Round(_current_location_y)));
            Obj.ResumeLayout();

            if (Environment.TickCount - _start <= _timer_sleep)
            {
              System.Threading.Thread.Sleep(_timer_sleep);
            }

            Obj.Refresh();

          } while (_break_width & _break_height & _break_location_y & _break_location_x);
        }
      }

      Obj.Width = decimal.ToInt32(Decimal.Round(FinWidth));
      Obj.Height = decimal.ToInt32(Decimal.Round(FinHeight));
      Obj.Location = new Point(decimal.ToInt32(Decimal.Round(FinLocationX)), decimal.ToInt32(Decimal.Round(FinLocationY)));
      Obj.Refresh();
    } // TransitionEffect

    //------------------------------------------------------------------------------
    // PURPOSE : Change view mode between report and Normal, update visibility objects and call to transittion efect
    //
    //  PARAMS :
    //      - INPUT :
    //        - Sender: sender of the event
    //        - e: event arguments.
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private void SetViewMode(Boolean DoEffectTransition)
    {
      if (!GamingTableBusinessLogic.IsGamingTablesEnabledAndPlayerTracking())
      {
        m_view_mode = GT_VIEW_MODE.REPORT;
        UpdateReportInformation();
      }

      if (GamingTableBusinessLogic.IsEnabledGTPlayerTracking())
      {
        InitControlsGamingTablesParametersWithPlayerTracking();
      }
      else
      {
        InitControlsGamingTablesParameters();
      }

      if (m_view_mode == GT_VIEW_MODE.REPORT)
      {
        InitLegendMovementsControl();

        uc_gt_player_in_session1.Visible = false;
        uc_gt_card_reader1.Visible = false;

        if (DoEffectTransition)
        {
          if (transparentPanel1.Visible)
          {
            transparentPanel1.Visible = false;
          }

          TransitionEffect(gamingTablePanel1, pnl_container.Width / 2 + 30, (pnl_container.Height - uc_gt_player_in_session1.Height) / 2 + 60, gamingTablePanel1.Location.X, 0);
        }
        else
        {
          gamingTablePanel1.Width = pnl_container.Width / 2 + 30;
          gamingTablePanel1.Height = (pnl_container.Height - uc_gt_player_in_session1.Height) / 2 + 60;
          gamingTablePanel1.Location = new Point(0, 0);
          gamingTablePanel1.Refresh();
        }

        transparentPanel1.Width = gamingTablePanel1.Width;
        transparentPanel1.Height = gamingTablePanel1.Height;
        transparentPanel1.Refresh();

        gamingTablePanel1.ReadOnly = true;

        pnl_report.Location = new Point(gamingTablePanel1.Location.X + 40, gamingTablePanel1.Location.Y + gamingTablePanel1.Height + 145);
        pnl_report.Width = gamingTablePanel1.Width - (pnl_report.Location.X * 2 - gamingTablePanel1.Location.X) + 48;
        pnl_report.Visible = true;
        dgv_gt_movememnts.Visible = true;

        if (m_gaming_table == null)
        {
          lbl_name_and_data_open_session.Text = string.Empty;
        }

        lbl_name_and_data_open_session.Visible = true;
        lbl_name_and_data_open_session.Font = new Font("Open Sans", (float)15, FontStyle.Bold);
        lbl_name_and_data_open_session.Width = pnl_report.Width - 15;
        lbl_name_and_data_open_session.Location = new Point(pnl_report.Location.X, (pnl_report.Location.Y - pnl_report.Height) + 100);
      }
      else
      {
        gamingTablePanel1.ReadOnly = false;
        rp_legend_movements.Visible = false;
        lbl_name_and_data_open_session.Visible = false;
        pnl_report.Visible = false;
        dgv_gt_movememnts.Visible = false;

        if (DoEffectTransition)
        {
          if (transparentPanel1.Visible)
          {
            transparentPanel1.Visible = false;
          }

          TransitionEffect(gamingTablePanel1, pnl_container.Width, pnl_container.Height - uc_gt_player_in_session1.Height - 3, gamingTablePanel1.Location.X, uc_gt_player_in_session1.Height);
        }
        else
        {
          gamingTablePanel1.Width = pnl_container.Width;
          gamingTablePanel1.Height = pnl_container.Height - uc_gt_player_in_session1.Height - 3;
          gamingTablePanel1.Location = new Point(20, uc_gt_player_in_session1.Height + 3);
          gamingTablePanel1.Refresh();
        }

        transparentPanel1.Width = pnl_container.Width;
        transparentPanel1.Height = pnl_container.Height;
        transparentPanel1.Refresh();

        uc_gt_player_in_session1.Visible = true;
      }


      ShowHidePanelTransparent();

      if (m_selected_seat_key != 0 && m_gaming_table.Seats[m_selected_seat_key].PlayerType == GTPlayerType.Unknown && m_view_mode != GT_VIEW_MODE.REPORT)
      {
        uc_gt_card_reader1.Visible = true;
      }
      else
      {
        uc_gt_card_reader1.Visible = false;
      }
    } // SetViewMode

    /// <summary>
    /// Initializes  legend of totals movements control.
    /// </summary>
    private void InitLegendMovementsControl()
    {
      rp_legend_movements.Visible = true;
      tlp_legend_movements.Size = new Size(dgv_gt_movememnts.Size.Width, 85);
      rp_legend_movements.Size = new Size(dgv_gt_movememnts.Size.Width, 85);
      rp_legend_movements.Location = new Point(dgv_gt_movememnts.Location.X - 10, dgv_gt_movememnts.Height + 10);
      pb_legend_chip_re.Image = CurrencyExchangeProperties.GetProperties("").GetIcon(CurrencyExchangeType.CASINO_CHIP_RE, 30, 30);
      pb_legend_chip_nre.Image = CurrencyExchangeProperties.GetProperties("").GetIcon(CurrencyExchangeType.CASINO_CHIP_NRE, 30, 30);
      pb_legend_chip_color.Image = CurrencyExchangeProperties.GetProperties("").GetIcon(CurrencyExchangeType.CASINO_CHIP_COLOR, 30, 30);
      pb_legend_amount.Image = new Bitmap(Resources.ResourceImages.money, new Size(30, 30));
      lbl_legend_chip_re.Text = FeatureChips.GetChipTypeDescription(CurrencyExchangeType.CASINO_CHIP_RE, "");
      lbl_legend_chip_nre.Text = FeatureChips.GetChipTypeDescription(CurrencyExchangeType.CASINO_CHIP_NRE, "");
      lbl_legend_chip_color.Text = FeatureChips.GetChipTypeDescription(CurrencyExchangeType.CASINO_CHIP_COLOR, "");
      lbl_legend_amount.Text = Resource.String("STR_REFUND_CASH");
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Change style row grid showed in report mode
    //
    //  PARAMS :
    //      - INPUT :
    //              - Idx Row
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private void SetupRowGrid()
    {
      //Color _color;
      int _idx_row;
      //_color = Color.White;
      CASHIER_MOVEMENT _movement_description_id;

      _movement_description_id = 0;

      for (_idx_row = 0; _idx_row < dgv_gt_movememnts.Rows.Count; _idx_row++)
      {
        _movement_description_id = (CASHIER_MOVEMENT)dgv_gt_movememnts.Rows[_idx_row].Cells[GRID_COLUMN_TYPE_ID].Value;

        //Movement Description
        switch (_movement_description_id)
        {

          case CASHIER_MOVEMENT.OPEN_SESSION:
            dgv_gt_movememnts.Rows[_idx_row].Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_UC_BANK_BTN_GAMING_TABLE_OPEN");

            break;
          case CASHIER_MOVEMENT.CAGE_CLOSE_SESSION:
            dgv_gt_movememnts.Rows[_idx_row].Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_MOV_CAGE_CLOSE_SESSION");

            break;
          case CASHIER_MOVEMENT.CAGE_FILLER_IN:
            if ((Int32)dgv_gt_movememnts.Rows[_idx_row].Cells[GRID_COLUMN_IS_OPENER_MOVEMENT].Value == 1)
            {
              dgv_gt_movememnts.Rows[_idx_row].Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_UC_BANK_MOVEMENTS_SUMMARY_CHIPS_OPENER");
            }
            else
            {
              dgv_gt_movememnts.Rows[_idx_row].Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_UC_BANK_BTN_GAMING_TABLE_DEPOSIT");
            }

            break;
          case CASHIER_MOVEMENT.CAGE_FILLER_OUT:
            dgv_gt_movememnts.Rows[_idx_row].Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_UC_BANK_MOVEMENTS_SUMMARY_CHIPS_CASH_OUT");

            break;
          case CASHIER_MOVEMENT.CHIPS_SALE:
            dgv_gt_movememnts.Rows[_idx_row].Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_MOVEMENT_TYPE_CHIPS_SALES");

            break;
          case CASHIER_MOVEMENT.CHIPS_PURCHASE:
            dgv_gt_movememnts.Rows[_idx_row].Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_MOVEMENT_TYPE_CHIPS_PURCHASE");

            break;
          case CASHIER_MOVEMENT.CHIPS_SALE_DEVOLUTION_FOR_TITO:
            dgv_gt_movememnts.Rows[_idx_row].Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_MOVEMENT_TYPE_CHIPS_SALES_DEVOLUTION_FOR_TITO");

            break;
          case CASHIER_MOVEMENT.CHIPS_SALE_WITH_CASH_IN:
            dgv_gt_movememnts.Rows[_idx_row].Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_CHIPS_SALE_WITH_CASH_IN");

            break;
          case CASHIER_MOVEMENT.CHIPS_SALE_REGISTER_TOTAL:
            dgv_gt_movememnts.Rows[_idx_row].Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_TABLE_SALE_REGISTER");

            break;
          case CASHIER_MOVEMENT.FILLER_IN_CLOSING_STOCK:
            dgv_gt_movememnts.Rows[_idx_row].Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_MOVEMENT_TYPE_FILLER_IN_CLOSING_STOCK");

            break;
          case CASHIER_MOVEMENT.FILLER_OUT_CLOSING_STOCK:
            dgv_gt_movememnts.Rows[_idx_row].Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_MOVEMENT_TYPE_FILLER_OUT_CLOSING_STOCK");

            break;
          case CASHIER_MOVEMENT.REOPEN_CASHIER:
            dgv_gt_movememnts.Rows[_idx_row].Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_UC_BANK_MOVEMENTS_SUMMARY_SESSION_REOPEN");

            break;
          default:
            dgv_gt_movememnts.Rows[_idx_row].Cells[GRID_COLUMN_TYPE_NAME].Value = String.Empty;

            break;

        }

        //Literal Chips
        if (dgv_gt_movememnts.Rows[_idx_row].Cells[GRID_COLUMN_ISO_CODE].Value.ToString() == Cage.CHIPS_ISO_CODE.ToString())
        {
          dgv_gt_movememnts.Rows[_idx_row].Cells[GRID_COLUMN_ISO_CODE].Value = Resource.String("STR_CURRENCY_TYPE_0_ISO_X01");
          dgv_gt_movememnts.Rows[_idx_row].Cells[GRID_COLUMN_CALCULATED_AMOUNT].Value = Convert.ToInt32(dgv_gt_movememnts.Rows[_idx_row].Cells[GRID_COLUMN_CALCULATED_AMOUNT].Value);
        }

        //Currency type description
        switch ((int)dgv_gt_movememnts.Rows[_idx_row].Cells[GRID_COLUMN_CURRENCY_TYPE].Value)
        {
          case (int)CageCurrencyType.ChipsRedimible:
            dgv_gt_movememnts.Rows[_idx_row].Cells[GRID_COLUMN_CALCULATED_AMOUNT_FORMATTED].Value = Currency.Format((decimal)dgv_gt_movememnts.Rows[_idx_row].Cells[GRID_COLUMN_CALCULATED_AMOUNT].Value,
                                                                                                                    (string)dgv_gt_movememnts.Rows[_idx_row].Cells[GRID_COLUMN_ISO_CODE].Value);
            dgv_gt_movememnts.Rows[_idx_row].Cells[GRID_COLUMN_CURRENCY_TYPE_DESCRIPTION].Value = CurrencyExchangeProperties.GetProperties("").GetIcon(CurrencyExchangeType.CASINO_CHIP_RE, 30, 30);

            break;
          case (int)CageCurrencyType.ChipsNoRedimible:
            dgv_gt_movememnts.Rows[_idx_row].Cells[GRID_COLUMN_CALCULATED_AMOUNT_FORMATTED].Value = Currency.Format((decimal)dgv_gt_movememnts.Rows[_idx_row].Cells[GRID_COLUMN_CALCULATED_AMOUNT].Value,
                                                                                                                    (string)dgv_gt_movememnts.Rows[_idx_row].Cells[GRID_COLUMN_ISO_CODE].Value);
            dgv_gt_movememnts.Rows[_idx_row].Cells[GRID_COLUMN_CURRENCY_TYPE_DESCRIPTION].Value = CurrencyExchangeProperties.GetProperties("").GetIcon(CurrencyExchangeType.CASINO_CHIP_NRE, 30, 30);

            break;
          case (int)CageCurrencyType.ChipsColor:
            dgv_gt_movememnts.Rows[_idx_row].Cells[GRID_COLUMN_CALCULATED_AMOUNT_FORMATTED].Value = Convert.ToInt32(dgv_gt_movememnts.Rows[_idx_row].Cells[GRID_COLUMN_CALCULATED_AMOUNT].Value);
            dgv_gt_movememnts.Rows[_idx_row].Cells[GRID_COLUMN_CURRENCY_TYPE_DESCRIPTION].Value = CurrencyExchangeProperties.GetProperties("").GetIcon(CurrencyExchangeType.CASINO_CHIP_COLOR, 30, 30);

            break;
          default:
            dgv_gt_movememnts.Rows[_idx_row].Cells[GRID_COLUMN_CALCULATED_AMOUNT_FORMATTED].Value = Currency.Format((decimal)dgv_gt_movememnts.Rows[_idx_row].Cells[GRID_COLUMN_CALCULATED_AMOUNT].Value,
                                                                                                                    (string)dgv_gt_movememnts.Rows[_idx_row].Cells[GRID_COLUMN_ISO_CODE].Value);
            dgv_gt_movememnts.Rows[_idx_row].Cells[GRID_COLUMN_CURRENCY_TYPE_DESCRIPTION].Value = new Bitmap(Resources.ResourceImages.money, new Size(30, 30));

            break;
        }

        dgv_gt_movememnts.Rows[_idx_row].Cells[GRID_COLUMN_CURRENCY_TYPE_DESCRIPTION].Style.SelectionBackColor = Color.Transparent;
      }
    } // SetupRowGrid

    //------------------------------------------------------------------------------
    // PURPOSE : Add column into data grid view
    //
    //  PARAMS :
    //      - INPUT :
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private void AddColumns()
    {
      DataGridViewTextBoxColumn _date_column;
      DataGridViewTextBoxColumn _id_move_column;
      DataGridViewTextBoxColumn _id_name_column;
      DataGridViewTextBoxColumn _name_move_column;
      DataGridViewTextBoxColumn _amount_column;
      DataGridViewTextBoxColumn _amount_formatted_column;
      DataGridViewTextBoxColumn _iso_code_column;
      DataGridViewTextBoxColumn _currency_type_column;
      DataGridViewImageColumn _currency_type_description_column;
      DataGridViewTextBoxColumn _is_opener_movement;

      // Grid Style
      dgv_gt_movememnts.BackgroundColor = Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      dgv_gt_movememnts.AllowUserToOrderColumns = false;
      dgv_gt_movememnts.Font = new System.Drawing.Font("Open Sans Semibold", 15, FontStyle.Bold, GraphicsUnit.World, Convert.ToByte(1), false);

      // Column Date - 0
      _date_column = new DataGridViewTextBoxColumn();
      _date_column.HeaderText = Resource.String("STR_FRM_LAST_MOVEMENTS_DATAGRID_CARD_DATE");
      _date_column.DataPropertyName = "DATE_MOV";
      _date_column.ReadOnly = true;
      _date_column.DefaultCellStyle.Format = Format.TimeCustomFormatString(true);
      _date_column.Width = 73;
      _date_column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
      _date_column.SortMode = DataGridViewColumnSortMode.NotSortable;

      // Column Id - 1
      _id_move_column = new DataGridViewTextBoxColumn();
      _id_move_column.HeaderText = "ID_MOV";
      _id_move_column.DataPropertyName = "ID_MOV";
      _id_move_column.Width = 0;
      _id_move_column.Visible = false;

      // Column Type ID - 2
      _id_name_column = new DataGridViewTextBoxColumn();
      _id_name_column.HeaderText = "TYPE_MOV";
      _id_name_column.DataPropertyName = "TYPE_MOV";
      _id_name_column.Width = 0;
      _id_name_column.Visible = false;

      //Column Type Name - 3
      _name_move_column = new DataGridViewTextBoxColumn();
      _name_move_column.HeaderText = Resource.String("STR_FRM_LAST_MOVEMENTS_DATAGRID_CARD_TYPE");
      _name_move_column.ReadOnly = true;
      _name_move_column.Width = 67;
      _name_move_column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
      _name_move_column.SortMode = DataGridViewColumnSortMode.NotSortable;

      // Column Calculated Amount - 4
      _amount_column = new DataGridViewTextBoxColumn();
      _amount_column.HeaderText = "CALCULATED_AMOUNT";
      _amount_column.DataPropertyName = "CALCULATED_AMOUNT";
      _amount_column.Width = 0;
      _amount_column.Visible = false;

      // Column Calculated Amount Formatted - 5
      _amount_formatted_column = new DataGridViewTextBoxColumn();
      _amount_formatted_column.HeaderText = Resource.String("STR_UC_BANK_MOVEMENTS_SUMMARY_AMOUNTS");
      _amount_formatted_column.ReadOnly = true;
      _amount_formatted_column.Width = 178;
      _amount_formatted_column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
      _amount_formatted_column.SortMode = DataGridViewColumnSortMode.NotSortable;

      //Column Iso Code - 6
      _iso_code_column = new DataGridViewTextBoxColumn();
      _iso_code_column.HeaderText = Resource.String("STR_VOUCHER_CAGESESSIONREPORT_CURRENCY");
      _iso_code_column.DataPropertyName = "CURRENCY_ISO_CODE";
      _iso_code_column.ReadOnly = true;
      _iso_code_column.Width = 0;
      _iso_code_column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
      _iso_code_column.SortMode = DataGridViewColumnSortMode.NotSortable;
      _iso_code_column.Visible = false;

      //Column currency type - 7
      _currency_type_column = new DataGridViewTextBoxColumn();
      _currency_type_column.HeaderText = "CURRENCY_TYPE";
      _currency_type_column.DataPropertyName = "CURRENCY_TYPE";
      _currency_type_column.Visible = false;
      _currency_type_column.Width = 0;

      //Column currency type - 8
      _currency_type_description_column = new DataGridViewImageColumn();
      _currency_type_description_column.HeaderText = " ";
      _currency_type_description_column.ReadOnly = true;
      _currency_type_description_column.Width = 44;
      _currency_type_description_column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
      _currency_type_description_column.SortMode = DataGridViewColumnSortMode.NotSortable;
      _currency_type_description_column.DefaultCellStyle.Padding = new Padding(5);

      // Column Id - 9
      _is_opener_movement = new DataGridViewTextBoxColumn();
      _is_opener_movement.HeaderText = "OPENER_MOV";
      _is_opener_movement.DataPropertyName = "OPENER_MOV";
      _is_opener_movement.Visible = false;
      _is_opener_movement.Width = 0;

      dgv_gt_movememnts.Columns.Add(_date_column);
      dgv_gt_movememnts.Columns.Add(_id_move_column);
      dgv_gt_movememnts.Columns.Add(_id_name_column);
      dgv_gt_movememnts.Columns.Add(_name_move_column);
      dgv_gt_movememnts.Columns.Add(_amount_column);
      dgv_gt_movememnts.Columns.Add(_amount_formatted_column);
      dgv_gt_movememnts.Columns.Add(_iso_code_column);
      dgv_gt_movememnts.Columns.Add(_currency_type_column);
      dgv_gt_movememnts.Columns.Add(_currency_type_description_column);
      dgv_gt_movememnts.Columns.Add(_is_opener_movement);
    } // AddColumns

    //------------------------------------------------------------------------------
    // PURPOSE : Initialize report variables and show all information
    //
    //  PARAMS :
    //      - INPUT :
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private void UpdateReportInformation()
    {
      VoucherGamingTableSessionOverview _preview;
      ArrayList _voucher_list;
      VoucherGamingTableSessionOverview.ParametersGamingTableSessionOverview _parameters;

      if (m_gaming_table != null)
      {
        lbl_name_and_data_open_session.Text = m_gaming_table.GamingTableName;
      }

      if (GamingTableBusinessLogic.RefreshGamingTableCashierSessionInfo(m_gaming_table) == GamingTableBusinessLogic.GT_RESPONSE_CODE.OK)
      {
        dgv_gt_movememnts.AutoGenerateColumns = false;

        if (dgv_gt_movememnts.Columns.Count == 0)
        {
          AddColumns();
        }

        dgv_gt_movememnts.DataSource = m_gaming_table.CashierSessionMovements;
        dgv_gt_movememnts.ClearSelection();

        //StyleGrid();
        SetupRowGrid();

        // Do the voucher
        _parameters = new VoucherGamingTableSessionOverview.ParametersGamingTableSessionOverview();
        _voucher_list = new ArrayList();
        _parameters.BackColor = ControlPaint.Light(Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(231)))), ((int)(((byte)(233))))), 1);
        _preview = new VoucherGamingTableSessionOverview(m_gaming_table, PrintMode.Preview, _parameters);
        _voucher_list.Add(_preview);

        lbl_name_and_data_open_session.Text = m_gaming_table.GamingTableName + Environment.NewLine + m_gaming_table.CashierSessionOpening;

        // Add voucher to web browser
        webBrowserGtSessionInfo.Navigate(VoucherPrint.GenerateFileForPreview(_voucher_list));

        pnl_report.HeaderText = m_gaming_table.GamingTableName;
        pnl_report.HeaderSubText = m_gaming_table.CashierSessionOpening.ToString("dd/MMM/yyyy HH:mm");
      }
      else
      {
        dgv_gt_movememnts.ClearSelection();

        if (dgv_gt_movememnts.Columns.Count == 0)
        {
          AddColumns();
        }

        dgv_gt_movememnts.DataSource = null;

        webBrowserGtSessionInfo.Navigate("");
        webBrowserGtSessionInfo.Show();
      }
    } // UpdateReportInformation

    #endregion // Private Methods

    #region Events

    //------------------------------------------------------------------------------
    // PURPOSE: Event called when a Player is searched. 
    // 
    //  PARAMS:
    //      - INPUT: TrackNumber: Number read
    //               IsValid: Track read is valid
    //      - OUTPUT:
    //
    // RETURNS: 
    // 
    //   NOTES:
    //  
    private void uc_gt_card_reader1_AccountReadWithTrackNumber(CardData CardData, Boolean IsValid)
    {
      Seat Seat;
      String _selected_iso_code;

      Seat = new Seat();
      if (m_selected_seat.PlaySession == null)
      {
        _selected_iso_code = CurrencyExchange.GetNationalCurrency();
      }
      else
      {
        _selected_iso_code = m_selected_seat.PlaySession.PlayerIsoCode;
      }


      if (!GamingTableBusinessLogic.ReadSeat(m_gaming_table.GamingTableId, m_selected_seat_key, m_gaming_table.GamingTableSession.GamingTableSessionId, out m_selected_seat))
      {
        return;
      }

      if (CardData != null && IsValid)
      {
        SetSeatWithCardData(CardData, out Seat);

        if (_selected_iso_code != String.Empty)
        {
          Seat.PlaySession.PlayerIsoCode = _selected_iso_code;
        }

        //JBC 30-01-2015 BirthDay alarm feature
        if (!Alarm.CheckBirthdayAlarm(CardData.AccountId, CardData.PlayerTracking.HolderName,
            CommonCashierInformation.CashierSessionInfo().TerminalName + " - " + CommonCashierInformation.CashierSessionInfo().TerminalId, TerminalTypes.UNKNOWN))
        {
          Log.Error("uc_ticket_create.RefreshData: Error checking Birthday Alarm.");
        }
      }

      if (IsValid && Seat.PlayerType != GTPlayerType.Unknown)
      {
        // check if player is allready playing in other table/seat
        if (!CheckNewPlayer(Seat))
        {
          return;
        }

        m_selected_seat = Seat;

        if (!m_gaming_table.Seats.ContainsKey(m_selected_seat_key))
        {
          m_gaming_table.Seats.Add(m_selected_seat_key, m_selected_seat);
          m_selected_seat.SeatId = m_selected_seat_key;
        }

        if (m_selected_seat.PlaySession.PlaySessionId == 0)
        {
          AddNewPlayer(m_selected_seat);
        }
      }
      else
      {
        m_selected_seat.PlayerType = GTPlayerType.Unknown;
      }

      if (IsValid)
      {
        ShowPlayer();
      }
    }  // uc_gt_card_reader1_AccountReadWithTrackNumber

    private void gamingTablePanel1_SelectedSeat(Int64? SeatId, ref Boolean Cancel)
    {
      //Int64 _initial_seat_id;
      GamingTableBusinessLogic.GT_RESPONSE_CODE _status_changes;
      Seat _temp_seat;
      CardData card_data;
      Int64 _initial_seat_id;

      if (m_seat_player_from_chips_sell)
      {
        if (!SeatId.HasValue || m_ticket_from_chips_sell == null || m_gaming_table.Seats[SeatId.Value].PlaySession.AccountId > 0 || m_card_data_from_chips_sell == null || m_card_data_from_chips_sell.AccountId == 0)
        {
          Cancel = true;
        }
        else
        {

          Boolean _is_valid;

          _is_valid = true;
          m_selected_seat_key = SeatId.Value;

          uc_gt_player_in_session1.EnabledControls(true);
          uc_scrollable_button_list1.Enabled = true;
          cb_banks.Enabled = true;
          uc_gt_card_reader1.Enabled = true;
          pnl_bottom.Enabled = true;

          uc_gt_card_reader1_AccountReadWithTrackNumber(m_card_data_from_chips_sell, true);

          gamingTablePanel1.UpdateSeatInfo(m_selected_seat);
          ShowPlayer();

          m_thread_sequence = m_thread.ForceRefresh();

          m_seat_player_from_chips_sell = false;
          m_card_data_from_chips_sell = null;

          uc_gt_player_in_session1.ShowHideSeatUserLabel(false);
          uc_gt_dealer_copy_ticket_reader1_OnValidationNumberReadEvent(ref _is_valid, m_ticket_from_chips_sell);
        }

        return;
      }

      if (SeatId == m_selected_seat_key)
      {
        return;
      }

      if (m_selected_seat != null && m_selected_seat.HasPendingChanges)
      {
        uc_gt_player_in_session1.SaveChanges();
      }

      if (!SeatId.HasValue)
      {
        uc_gt_card_reader1.Visible = false;
        uc_gt_player_in_session1.Seat = null;
        uc_gt_player_in_session1.SetControls();
        uc_input_amount_resizable1.Visible = false;
        uc_gt_dealer_copy_ticket_reader1.Visible = false;

        m_selected_seat = null;
        m_selected_seat_key = 0;

        return;
      }

      m_selected_seat_key = (Int64)SeatId;

      if (m_initial_seat_for_swap == null)
      {
        if (!GamingTableBusinessLogic.ReadSeat(m_gaming_table.GamingTableId, SeatId.Value, m_gaming_table.GamingTableSession.GamingTableSessionId, out m_selected_seat))
        {
          m_selected_seat = null;
        }

        RegisterPlayer();
      }
      else
      {
        // Save target seat in temp seat 
        if (!GamingTableBusinessLogic.ReadSeat(m_gaming_table.GamingTableId, m_selected_seat_key, m_gaming_table.GamingTableSession.GamingTableSessionId, out m_selected_seat))
        {
          m_selected_seat_key = m_initial_seat_for_swap.SeatId;
          m_selected_seat = null;
          Cancel = true;

          return;
        }

        _temp_seat = new Seat();
        _temp_seat = m_selected_seat;
        _initial_seat_id = m_initial_seat_for_swap.SeatId;

        // Swap with restart sessions
        if (GamingTableBusinessLogic.IsEnabledRestartInSwapSeats())
        {
          // Close session source
          _status_changes = GamingTableBusinessLogic.EndGTPlaySession(m_initial_seat_for_swap, Cashier.UserId, Cashier.TerminalId, Cashier.TerminalName);

          if (_status_changes == GamingTableBusinessLogic.GT_RESPONSE_CODE.OK)
          {
            // Close session target if exist
            if (m_selected_seat.PlayerType != GTPlayerType.Unknown)
            {
              _status_changes = GamingTableBusinessLogic.EndGTPlaySession(m_selected_seat, Cashier.UserId, Cashier.TerminalId, Cashier.TerminalName);
            }

            if (_status_changes == GamingTableBusinessLogic.GT_RESPONSE_CODE.OK)
            {
              GamingTableBusinessLogic.ReadTable(m_gaming_table.GamingTableId, out m_gaming_table);

              // Update seat target with source session
              card_data = new CardData();

              if (m_initial_seat_for_swap.PlayerType != GTPlayerType.AnonymousWithoutCard)
              {
                if (!CardData.DB_CardGetAllData(m_initial_seat_for_swap.AccountId, card_data, false))
                {
                  m_selected_seat_key = m_initial_seat_for_swap.SeatId;
                  m_selected_seat = null;
                  Cancel = true;

                  return;
                }
              }

              uc_gt_card_reader1_AccountReadWithTrackNumber(card_data, true);

              if (m_initial_seat_for_swap.PlaySession.Started_walk.HasValue)
              {
                GamingTableBusinessLogic.UpdateSeatWalk(m_selected_seat, Cashier.UserId, Cashier.TerminalId);
              }

              // Update seat source with target session
              if (_temp_seat.PlayerType != GTPlayerType.Unknown)
              {
                m_selected_seat_key = m_initial_seat_for_swap.SeatId;

                card_data = new CardData();

                if (_temp_seat.PlayerType != GTPlayerType.AnonymousWithoutCard)
                {
                  if (!CardData.DB_CardGetAllData(_temp_seat.AccountId, card_data, false))
                  {
                    m_selected_seat_key = m_initial_seat_for_swap.SeatId;
                    m_selected_seat = null;
                    Cancel = true;

                    return;
                  }
                }

                uc_gt_card_reader1_AccountReadWithTrackNumber(card_data, true);

                if (_temp_seat.PlaySession.Started_walk.HasValue)
                {
                  GamingTableBusinessLogic.UpdateSeatWalk(m_selected_seat, Cashier.UserId, Cashier.TerminalId);
                }
              }

              m_selected_seat_key = _temp_seat.SeatId;
            }
          }
        }
        else
        {
          // Swap without restart
          _status_changes = GamingTableBusinessLogic.SwapSeats(m_initial_seat_for_swap, m_selected_seat, Cashier.UserId, Cashier.TerminalId);
        }

        // If is all ok, update visible information
        if (_status_changes == GamingTableBusinessLogic.GT_RESPONSE_CODE.OK)
        {
          GamingTableBusinessLogic.ReadTable(m_gaming_table.GamingTableId, out m_gaming_table);

          //GamingTableBusinessLogic.UpdateGTPlaySession(m_initial_seat_for_swap, Cashier.UserId, Cashier.TerminalId);
          m_gaming_table.Seats.TryGetValue(_initial_seat_id, out m_initial_seat_for_swap);
          gamingTablePanel1.UpdateSeatInfo(m_initial_seat_for_swap);

          //GamingTableBusinessLogic.UpdateGTPlaySession(m_selected_seat, Cashier.UserId, Cashier.TerminalId);
          m_gaming_table.Seats.TryGetValue(m_selected_seat_key, out m_selected_seat);
          gamingTablePanel1.UpdateSeatInfo(m_selected_seat);
        }

        ShowPlayer();

        m_thread_sequence = m_thread.ForceRefresh();
        m_initial_seat_for_swap = null;
        uc_gt_player_in_session1.ShowHideSwapLabel(false);
      }
    } // gamingTablePanel1_SelectedSeat

    private void uc_gt_player_in_session1_ValuePlayerIsoCodeChanged(Seat Seat, String OldIsoCode, String NewIsoCode)
    {
      GamingTableBusinessLogic.GT_RESPONSE_CODE _status_changes;
      CardData _card_data;

      if (OldIsoCode == NewIsoCode)
      {
        return;
      }

      if (Seat.PlaySession.PlaysCount > 0 || Seat.PlaySession.CurrentBet > 0 || Seat.PlaySession.TotalSellChips > 0 || Seat.PlaySession.ChipsIn > 0)
      {
        // Close session source
        _status_changes = GamingTableBusinessLogic.EndGTPlaySession(Seat, Cashier.UserId, Cashier.TerminalId, Cashier.TerminalName);

        Seat.PlaySession.PlayerIsoCode = NewIsoCode;

        if (_status_changes == GamingTableBusinessLogic.GT_RESPONSE_CODE.OK)
        {
          GamingTableBusinessLogic.ReadTable(m_gaming_table.GamingTableId, out m_gaming_table);
          m_gaming_table.Seats[m_selected_seat_key].PlaySession.PlayerIsoCode = NewIsoCode;

          // Open new session
          _card_data = new CardData();

          if (!CardData.DB_CardGetAllData(Seat.AccountId, _card_data, false))
          {
            return;
          }

          uc_gt_card_reader1_AccountReadWithTrackNumber(_card_data, true);
        }
      }

      // Set new currency
      m_gaming_table.Seats[m_selected_seat_key].PlaySession.PlayerIsoCode = NewIsoCode;

      m_selected_seat_key = Seat.SeatId;
      m_selected_seat = m_gaming_table.Seats[m_selected_seat_key];
      m_table_selected_iso_code = NewIsoCode;

      // Save new currency
      GamingTableBusinessLogic.UpdateGTPlaySession(m_selected_seat, Cashier.UserId, Cashier.TerminalId);

      // referesh min & max bet values by currency
      if (NewIsoCode != btn_table_iso_code.Text)
      {
        btn_table_iso_code.Text = NewIsoCode;
        lbl_min_bet_value.Text = m_gaming_table.TableAceptedIsoCode[NewIsoCode].MinBet.ToString(false);
        lbl_max_bet_value.Text = m_gaming_table.TableAceptedIsoCode[NewIsoCode].MaxBet.ToString(false);
      }

      m_thread_sequence = m_thread.ForceRefresh();

    }  // uc_gt_player_in_session1_ValuePlayerIsoCodeChanged

    private void uc_input_amount_resizable1_MouseMove(object sender, MouseEventArgs e)
    {
      if (e.Button == MouseButtons.Left)
      {
        uc_input_amount_resizable us = sender as uc_input_amount_resizable;

        if (us != null)
        {
          us.Left += e.X;
          us.Top += e.Y;
        }
      }

    }  // uc_input_amount_resizable1_MouseMove

    private void uc_gt_player_in_session1_TextBoxEnterEvent(Decimal Amount, uc_gt_player_in_session.TextBoxSelected TextBoxSelected, Boolean GoBack)
    {
      //if (GoBack)
      //{
      //  return;
      //}

      if (TextBoxSelected == uc_gt_player_in_session.TextBoxSelected.Unknown)
      {
        uc_gt_dealer_copy_ticket_reader1.Hide_DealerCopyValidation();
        uc_input_amount_resizable1.Hide_KeyBoard();
      }
      else
      {
        if (GamingTableBusinessLogic.IsEnableCashDeskDealerTicketsValidation() && TextBoxSelected == uc_gt_player_in_session.TextBoxSelected.Btn_Buy_In)
        {
          m_text_box_selected = TextBoxSelected;
          uc_gt_dealer_copy_ticket_reader1.BringToFront();
          uc_gt_dealer_copy_ticket_reader1.Show_DealerCopyValidation();
        }

        else
        {
          m_text_box_selected = TextBoxSelected;
          uc_input_amount_resizable1.BringToFront();
          uc_input_amount_resizable1.txt_amount.Text = Amount.ToString();
          uc_input_amount_resizable1.Show_KeyBoard();
        }
      }

      // In case Button cliked swap and without selected a  seat to swap and change any value for the same player discard swap
      m_initial_seat_for_swap = null;
      uc_gt_player_in_session1.ShowHideSwapLabel(false);

    }  //  uc_gt_player_in_session1_TextBoxEnterEvent

    private void uc_gt_player_in_session1_ButtonClicked(Seat Seat, uc_gt_player_in_session.ButtonClicked ButtonClicked)
    {
      Dictionary<Int32, GamingTable> _gt_tables;
      Dictionary<Int32, ScrollableButtonItem> _scrollable_buttons;

      if (ButtonClicked == uc_gt_player_in_session.ButtonClicked.Close)
      {
        m_selected_seat = Seat;
        RegisterPlayer();
      }

      if (ButtonClicked == uc_gt_player_in_session.ButtonClicked.PlayPause)
      {
        m_selected_seat = Seat;
        CheckNewPlayer(Seat);
      }

      if (ButtonClicked == uc_gt_player_in_session.ButtonClicked.Swap)
      {
        m_initial_seat_for_swap = Seat;
        uc_gt_player_in_session1.ShowHideSwapLabel(true);
      }
      else
      {
        m_initial_seat_for_swap = null;
        uc_gt_player_in_session1.ShowHideSwapLabel(false);
      }

      m_gaming_table.Seats[m_selected_seat.SeatId] = m_selected_seat;

      _gt_tables = m_thread.GamingTablesInterface;

      if (_gt_tables != null)
      {
        _gt_tables[m_gaming_table.GamingTableId] = m_gaming_table;
        m_thread.GamingTablesInterface = _gt_tables;

        _scrollable_buttons = CreateScrollButtonsList(_gt_tables);
        uc_scrollable_button_list1.RefreshButtons(_scrollable_buttons);
        uc_scrollable_button_list1.Visible = _scrollable_buttons.Count > 0;
      }

      m_thread_sequence = m_thread.ForceRefresh();

      gamingTablePanel1.UpdateSeatInfo(m_selected_seat);
    } // uc_gt_player_in_session1_ButtonClicked

    private void btn_legend_Click(object sender, EventArgs e)
    {
      pnl_information.Visible = !pnl_information.Visible;
      pnl_information.BringToFront();
      pnl_information.HeaderText = Resource.String("PLAYER_TRACKING_LEGEND_LABEL");
    } // btn_legend_Click

    private void cb_bank_SelectedIndexChanged(object sender, EventArgs e)
    {
      Dictionary<Int32, ScrollableButtonItem> _scrollable_buttons;
      //DataTable _tables;
      Boolean _get_default_table;
      Boolean _table_exist;
      Dictionary<Int32, GamingTable> _tables;
      Int32 _table_selected;
      Int32 _bank_id_selected;
      GamingTable _gaming_table_selected_default;

      _bank_id_selected = int.Parse(((System.Data.DataRowView)(cb_banks.SelectedItem)).Row.ItemArray[0].ToString());

      _get_default_table = true;
      _table_exist = false;
      _table_selected = 0;
      _gaming_table_selected_default = null;

      if (cb_banks.Items.Count == 0)
      {
        lbl_max_bet_value.Text = "---";
        lbl_min_bet_value.Text = "---";
        cb_table_speed.Enabled = false;
        btn_play_pause.Enabled = false;

        return;
      }

      if (m_thread != null && m_bank_id != _bank_id_selected)
      {
        m_thread.BankId = _bank_id_selected;
      }

      // load uc_scrollable_button_list1      
      m_bank_id = _bank_id_selected;

      GamingTableBusinessLogic.ReadTables(m_bank_id, out _tables);

      if (_tables.Count > 0)
      {
        foreach (KeyValuePair<Int32, GamingTable> _table in _tables)
        {
          if (_get_default_table)
          {
            _get_default_table = false;
            _table_selected = _table.Key;

            if (m_gaming_table == null)
            {
              _gaming_table_selected_default = _table.Value;
            }
          }

          // Validate if table exist in current bank
          if (m_gaming_table != null && _table.Key == m_gaming_table.GamingTableId)
          {
            _table_exist = true;
          }

          if (m_gaming_table != null && _table.Value.CashierId == (Int32)WSI.Cashier.Cashier.TerminalId)
          {
            _table_selected = _table.Key;
          }
        }

        _scrollable_buttons = CreateScrollButtonsList(_tables);

        uc_scrollable_button_list1.MinButtonWidth = 96;
        uc_scrollable_button_list1.MaxButtons = _scrollable_buttons.Count;

        if (_table_exist)
        {
          uc_scrollable_button_list1.RefreshButtons(_scrollable_buttons);
          uc_scrollable_button_list1.SetSelectedButton(m_gaming_table.GamingTableId);
          uc_scrollable_button_list1.Visible = _scrollable_buttons.Count > 0;

          if (m_gaming_table == null)
          {
            m_gaming_table = _gaming_table_selected_default;
            LoadDesignTable();
          }
        }
        else
        {
          uc_scrollable_button_list1.LoadButtons(_scrollable_buttons);
          uc_scrollable_button_list1.SetSelectedButton(_table_selected);
          uc_scrollable_button_list1.Visible = _scrollable_buttons.Count > 0;

          m_selected_seat_key = 0;

          if (m_selected_seat != null)
          {
            m_selected_seat.PlayerType = GTPlayerType.Unknown;
          }

          m_gaming_table = new GamingTable();
          m_gaming_table.GamingTableId = _table_selected;
          LoadDesignTable();
        }
      }

      ShowHidePanelTransparent();
    }  // cb_table_SelectedIndexChanged

    public void ShowHidePanelTransparent()
    {
      Bitmap FormScreenShot;

      FormScreenShot = null;

      if (m_gaming_table == null || m_gaming_table.GamingTableSession == null || m_gaming_table.GamingTablePaused)
      {
        transparentPanel1.Visible = true;
        transparentPanel1.BringToFront();
        FormScreenShot = new Bitmap(pnl_container.Width, pnl_container.Height);
        pnl_container.DrawToBitmap(FormScreenShot, new Rectangle(new Point(0, 0), pnl_container.Size));

        transparentPanel1.BackgroundImage = FormScreenShot;

        if (m_gaming_table != null && m_gaming_table.GamingTableId != 0 && m_gaming_table.GamingTableSession == null)
        {
          transparentPanel1.Image = Resources.ResourceImages.lock_100px_red;
        }
        else if (m_gaming_table != null && m_gaming_table.GamingTableId != 0 && m_gaming_table.GamingTablePaused)
        {
          if (!GamingTableBusinessLogic.IsGamingTablesEnabledAndPlayerTracking())
          {
            transparentPanel1.Visible = false;
          }
          else
          {
            transparentPanel1.Image = Resources.ResourceImages.pause_100px_orange;
          }
        }
        else
        {
          transparentPanel1.Image = null;
        }
      }
      else
      {
        // WIG-1585
        if (transparentPanel1.Visible)
        {
          transparentPanel1.Visible = false;
          LoadDesignTable();
        }
      }

      if (pnl_information.Visible)
      {
        pnl_information.BringToFront();
      }

      if (pnl_table_options != null && pnl_table_options.Visible)
      {
        pnl_table_options.BringToFront();
      }
    }

    private Dictionary<Int32, ScrollableButtonItem> CreateScrollButtonsList(DataTable Table)
    {
      Dictionary<Int32, ScrollableButtonItem> _scrollable_buttons = new Dictionary<int, ScrollableButtonItem>();

      foreach (DataRow _row in Table.Rows)
      {
        ScrollableButtonItem _button = new ScrollableButtonItem();

        _button.ButtonId = Int32.Parse(_row["GT_GAMING_TABLE_ID"].ToString());
        _button.ButtonLabel = _row["GT_NAME"].ToString();
        _button.ButtonColor = Color.Transparent;

        if (Int32.Parse(_row["GT_SESSION_STATUS"].ToString()) == (Int32)CASHIER_SESSION_STATUS.CLOSED)
        {
          //_button.ButtonColor = Color.Red;
          _button.ButtonImage = Resources.ResourceImages.lock_35px_red;
        }
        else if (_row["GT_PLAYER_TRACKING_PAUSE"] != DBNull.Value && (Boolean)_row["GT_PLAYER_TRACKING_PAUSE"])
        {
          //_button.ButtonColor = Color.Orange;
          _button.ButtonImage = Resources.ResourceImages.pause_35px_orange;
        }
        else
        {
          //_button.ButtonColor = Color.Transparent;
          _button.ButtonImage = null;
        }

        _scrollable_buttons.Add(_button.ButtonId, _button);
      }

      return _scrollable_buttons;
    }

    private Boolean HasWarningsGamingTable(GamingTable Table)
    {
      Boolean _has_warnings;

      _has_warnings = false;

      if (Table.Seats != null && Table.Seats.Count > 0)
      {
        foreach (KeyValuePair<Int64, Seat> _seat in Table.Seats)
        {
          // Validate idle Seat
          if (_seat.Value.PlaySession.PlaySessionId != 0 && _seat.Value.PlaySession.PlaysCount - _seat.Value.PlaySession.LastPlaysCount > Table.TableIdlePlays)
          {
            _has_warnings = true;

            break;
          }

          // Validate table Min/Max of current bet
          if (_seat.Value.PlayerType != GTPlayerType.Unknown &&
            ((_seat.Value.PlaySession.CurrentBet < Table.BetMin) || (Table.BetMax != 0 && _seat.Value.PlaySession.CurrentBet > Table.BetMax)))
          {
            _has_warnings = true;

            break;
          }
        }
      }

      return _has_warnings;
    }

    private Dictionary<Int32, ScrollableButtonItem> CreateScrollButtonsList(Dictionary<Int32, GamingTable> Tables)
    {
      Dictionary<Int32, ScrollableButtonItem> _scrollable_buttons = new Dictionary<int, ScrollableButtonItem>();

      foreach (KeyValuePair<Int32, GamingTable> _table in Tables)
      {
        ScrollableButtonItem _button = new ScrollableButtonItem();

        _button.ButtonId = _table.Key;
        _button.ButtonLabel = _table.Value.GamingTableName;
        _button.ButtonColor = Color.Transparent;

        if (_table.Value.GamingTableSession == null)
        {
          //_button.ButtonColor = Color.Red;
          _button.ButtonImage = Resources.ResourceImages.lock_35px_red;
        }
        else if (_table.Value.GamingTablePaused)
        {
          //_button.ButtonColor = Color.Orange;
          _button.ButtonImage = Resources.ResourceImages.pause_35px_orange;
        }
        else if (HasWarningsGamingTable(_table.Value))
        {
          if (!GamingTableBusinessLogic.IsGamingTablesEnabledAndPlayerTracking())
          {
            _button.ButtonImage = null;
          }
          else
          {
            _button.ButtonImage = Resources.ResourceImages.warning_35px_yellow;
          }
        }
        else
        {
          //_button.ButtonColor = Color.Transparent;
          _button.ButtonImage = null;
        }

        _scrollable_buttons.Add(_button.ButtonId, _button);
      }

      return _scrollable_buttons;
    }

    private void uc_scrollable_gambling_tables_ButtonClicked(int Id)
    {
      if (m_gaming_table.GamingTableId != Id)
      {
        m_gaming_table.GamingTableId = Id;
        m_selected_seat_key = 0;
        LoadDesignTable();

        lbl_name_and_data_open_session.Text = m_gaming_table.GamingTableName;
      }
      else if (m_gaming_table.GamingTableId == Id && HasWarningsGamingTable(m_gaming_table))
      {
        // Reset idle seats
        GamingTableBusinessLogic.ResetGamingTableIdle(m_gaming_table);

        if (m_thread != null)
        {
          m_thread.ForceRefresh();
        }
      }

      if (GamingTableBusinessLogic.IsEnabledGTPlayerTracking())
      {
        InitControlsGamingTablesParametersWithPlayerTracking();
      }
      else
      {
        InitControlsGamingTablesParameters();
        SetViewMode(false);
      }

      UpdateReportInformation();
    } // uc_scrollable_button_list1_ButtonClicked

    private void SetBottomPanelColor()
    {
      if (m_gaming_table != null && m_gaming_table.GamingTableSession == null)
      {
        pnl_bottom.BackColor = Color.Red;
      }
      else if (m_gaming_table != null && m_gaming_table.GamingTablePaused)
      {
        pnl_bottom.BackColor = Color.Orange;
      }
      else
      {
        pnl_bottom.BackColor = Color.LightSkyBlue;
      }

      pnl_bottom.Refresh();
    } // SetBottomPanelColor

    private void cb_speed_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (cb_table_speed.Items.Count > 0)
      {
        if ((int)cb_table_speed.SelectedValue != (int)m_gaming_table.SelectedSpeed || m_gaming_table.SelectedSpeed == GTPlayerTrackingSpeed.Unknown)
        {
          m_gaming_table.SelectedSpeed = (GTPlayerTrackingSpeed)cb_table_speed.SelectedValue;
          GamingTableBusinessLogic.UpdateTable(ref m_gaming_table, Cashier.UserId, Cashier.TerminalId);
        }
      }
    }  // cb_speed_SelectedIndexChanged

    private void btn_table_iso_code_Click(object sender, EventArgs e)
    {
      String _first_iso_code;
      Boolean _next;

      _first_iso_code = String.Empty;
      _next = false;

      if (m_gaming_table != null && m_gaming_table.TableAceptedIsoCode != null)
      {
        foreach (String _key in m_gaming_table.TableAceptedIsoCode.Keys)
        {
          if (_first_iso_code == String.Empty)
          {
            _first_iso_code = _key;
          }

          if (_next)
          {
            m_table_selected_iso_code = _key;
            _next = false;

            break;
          }

          if (m_table_selected_iso_code == _key)
          {
            _next = true;
          }
        }

        if (_next)
        {
          m_table_selected_iso_code = _first_iso_code;
        }
      }

      SetScreenGamingTableData();
    }  // cb_speed_SelectedIndexChanged

    private void btn_play_pause_Click(object sender, EventArgs e)
    {
      Color _color;
      Image _image;

      _color = Color.Transparent;
      _image = null;

      if (!m_gaming_table.GamingTablePaused)
      {
        btn_play_pause.Image = Resources.ResourceImages.play1;
        btn_play_pause.BackColor = Color.FromArgb(80, 90, 102);
        m_gaming_table.GamingTablePaused = true;
        GamingTableBusinessLogic.UpdateTable(ref m_gaming_table, Cashier.UserId, Cashier.TerminalId);
        _image = Resources.ResourceImages.pause_100px_orange;
      }
      else
      {
        btn_play_pause.Image = Resources.ResourceImages.pause1;
        btn_play_pause.BackColor = Color.FromArgb(80, 90, 102);
        m_gaming_table.GamingTablePaused = false;
        GamingTableBusinessLogic.UpdateTable(ref m_gaming_table, Cashier.UserId, Cashier.TerminalId);
      }

      m_thread_sequence = m_thread.ForceRefresh();

      uc_scrollable_button_list1.RefreshButton(new ScrollableButtonItem(m_gaming_table.GamingTableId, m_gaming_table.GamingTableName, _color, _image));
      //SetBottomPanelColor();
      SetEnabledControls();
      ShowHidePanelTransparent();
    } // btn_play_pause_Click

    private void uc_gt_player_tracking_SizeChanged(object sender, EventArgs e)
    {
      Point m_point;
      System.Drawing.Size m_size;
      Int16 _extract_width;
      Int32 _container_width;
      Int32 _container_height;
      Int16 _margin;

      _extract_width = 65;
      _margin = 9;
      _container_width = (Int32)this.Width;
      _container_height = (Int32)this.Height;

      if (m_view_mode == GT_VIEW_MODE.REPORT)
      {
        m_view_mode = GT_VIEW_MODE.NORMAL;
        SetViewMode(false);
        m_view_mode = GT_VIEW_MODE.REPORT;
      }

      if (WindowManager.IsTableMode)
      {
        // Set Size
        m_size = pnl_bottom.Size;

        m_size.Width = (_container_width - 2 * _margin) - _extract_width;

        pnl_bottom.Size = m_size;

        // Set location
        m_point = pnl_bottom.Location;

        m_point.X = _margin + _extract_width;
        m_point.Y = _container_height - pnl_bottom.Height - 3;

        pnl_bottom.Location = m_point;
      }
      else
      {
        // Set Size
        m_size = pnl_bottom.Size;

        m_size.Width = (_container_width - 2 * _margin);

        pnl_bottom.Size = m_size;

        // Set location
        m_point = pnl_bottom.Location;

        m_point.X = _margin;
        m_point.Y = _container_height - pnl_bottom.Height - 3;

        pnl_bottom.Location = m_point;
      }

      if (m_view_mode == GT_VIEW_MODE.REPORT)
      {
        SetViewMode(false);

        if (WindowManager.IsTableMode)
        {
          dgv_gt_movememnts.Height = dgv_gt_movememnts.Height - 12;
        }
        else
        {
          dgv_gt_movememnts.Height = dgv_gt_movememnts.Height + 12;
          rp_legend_movements.Location = new Point(rp_legend_movements.Location.X, rp_legend_movements.Location.Y + 12);
        }
      }

    } // uc_gt_player_tracking_SizeChanged

    private void btn_gaming_table_open_Click(object sender, EventArgs e)
    {
      ConfigureUcBank(false);
      m_uc_bank.btn_open_Click();

      if (m_thread != null)
      {
        m_thread.ForceRefresh();
      }

      ConfigureUcBank(true);
      LoadDesignTable();
      UpdateScrollButtonsStatus();
    } // btn_gaming_table_open_Click

    //FGB 03-NOV-2016 
    /// <summary>
    /// Reopen closed session
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btn_gaming_table_reopen_Click(object sender, EventArgs e)
    {
      ConfigureUcBank(false);

      ReopenGamingTable();
      //m_uc_bank.btn_open_Click();

      if (m_thread != null)
      {
        m_thread.ForceRefresh();
      }

      ConfigureUcBank(true);
      LoadDesignTable();
      UpdateScrollButtonsStatus();
    } // btn_gaming_table_open_Click


    /// <summary>
    /// Has user permission to reopen cashier session
    /// </summary>
    /// <returns></returns>
    private Boolean HasUserPermissionToReopenGamingTableSession()
    {
      String _error_message;

      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.GamingTableReOpen, ProfilePermissions.TypeOperation.RequestPasswd, this.ParentForm, out _error_message))
      {
        return false;
      }

      m_uc_bank.CashierSessionInfo.AuthorizedByUserId = Cashier.AuthorizedByUserId;
      m_uc_bank.CashierSessionInfo.AuthorizedByUserName = Cashier.AuthorizedByUserName;

      return true;
    }

    private void ReopenGamingTable()
    {
      ReopenGamingTableSession _reopen_gaming_table;
      String _error_message;
      ArrayList _voucher_list;

      _voucher_list = null;

      try
      {
        _reopen_gaming_table = new ReopenGamingTableSession();

        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (_reopen_gaming_table.GetGamingTableSessionId(m_gaming_table.GamingTableId, _db_trx.SqlTransaction) == 0)
          {
            _error_message = Resource.String("STR_REOPEN_SESSION_ERROR_SESSION_CAN_NOT_BE_REOPENED", _reopen_gaming_table.GetReopenTypeDescription());

            frm_message.Show(_error_message,
                             Resource.String("STR_REOPEN_SESSION"),
                             MessageBoxButtons.OK,
                             Images.CashierImage.Error,
                             this.ParentForm);
            return;
          }
        }

        if (!HasUserPermissionToReopenGamingTableSession())
        {
          _error_message = Resource.String("STR_REOPEN_SESSION_ERROR_USER_DOES_NOT_HAVE_PERMISSION", _reopen_gaming_table.GetReopenTypeDescription());

          frm_message.Show(_error_message,
                           Resource.String("STR_UC_BANK_BTN_CASH_DESK_REOPEN"),
                           MessageBoxButtons.OK,
                           Images.CashierImage.Error,
                           this.ParentForm);
          return;
        }

        using (DB_TRX _db_trx = new DB_TRX())
        {

          if (!_reopen_gaming_table.Undo(m_uc_bank.CashierSessionInfo, m_gaming_table, _db_trx.SqlTransaction, out _error_message, out _voucher_list))
          {
            frm_message.Show(_error_message,
                             Resource.String("STR_UC_BANK_BTN_CASH_DESK_REOPEN"),
                             MessageBoxButtons.OK,
                             Images.CashierImage.Error,
                             this.ParentForm);
            return;
          }

          _db_trx.Commit();
        }

        if (_voucher_list != null)
        {
          VoucherPrint.Print(_voucher_list);
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    }

    private void btn_gaming_table_drop_hourly_in_Click(object sender, EventArgs e)
    {
      String _error_str;
      _error_str = String.Empty;

      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.GamingTablesHourlyDropReport,
                                                               ProfilePermissions.TypeOperation.RequestPasswd,
                                                               m_parent,
                                                               out _error_str)
                          && (m_gaming_table.GamingTableSession != null))
      {
        return;
      }

      frm_drop_hourly.Show(this.ParentForm, m_gaming_table.GamingTableSession.GamingTableSessionId);
    } // btn_gaming_table_drop_hourly_in_Click

    private void btn_gaming_table_winloss_in_Click(object sender, EventArgs e)
    {
      String _error_str;
      _error_str = String.Empty;

      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.GamingTablesWinLoss,
                                                               ProfilePermissions.TypeOperation.RequestPasswd,
                                                               m_parent,
                                                               out _error_str)
                          && (m_gaming_table.GamingTableSession != null))
      {
        return;
      }

      frm_gaming_tables_win_loss.Show(this.ParentForm, m_gaming_table.GamingTableSession.GamingTableSessionId);
    } // btn_gaming_table_winloss_in_Click

    private void btn_gaming_table_filler_in_Click(object sender, EventArgs e)
    {
      ConfigureUcBank(false);
      m_uc_bank.btn_filler_in_Click();
      ConfigureUcBank(true);

      UpdateReportInformation();
    } // btn_gaming_table_filler_in_Click

    private void btn_gaming_table_filler_out_Click(object sender, EventArgs e)
    {
      ConfigureUcBank(false);
      m_uc_bank.btn_filler_out_Click();
      ConfigureUcBank(true);

      UpdateReportInformation();
    } // btn_gaming_table_filler_out_Click

    private void btn_gaming_table_history_Click(object sender, EventArgs e)
    {
      String _error_str;

      uc_input_amount_resizable1.Visible = false;
      uc_gt_dealer_copy_ticket_reader1.Visible = false;

      if (m_view_mode == GT_VIEW_MODE.NORMAL || m_view_mode == GT_VIEW_MODE.UNDEFINED)
      {
        if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.GamingTableReport,
                                                                            ProfilePermissions.TypeOperation.RequestPasswd,
                                                                            m_parent,
                                                                            out _error_str))
        {
          return;
        }

        m_view_mode = GT_VIEW_MODE.REPORT;

        UpdateReportInformation();
      }
      else
      {
        m_view_mode = GT_VIEW_MODE.NORMAL;
      }

      SetViewMode(true);
    } // btn_gaming_table_history_Click

    private void btn_gaming_table_close_cash_Click(object sender, EventArgs e)
    {
      ConfigureUcBank(false);

      m_uc_bank.SeatToClose = this.GetSessionsInGamingTable();

      m_uc_bank.CloseSession();

      ConfigureUcBank(true);

      if (m_thread != null)
      {
        m_thread.ForceRefresh();
      }

      LoadDesignTable();
      UpdateScrollButtonsStatus();
      UpdateReportInformation();
    } // btn_close_cash_Click

    private IList<Seat> GetSessionsInGamingTable()
    {
      IList<Seat> _seats;
      Seat _seat;

      _seats = new List<Seat>();
      foreach (var _item in m_gaming_table.Seats)
      {
        _seat = _item.Value;
        if (_seat.PlayerType != GTPlayerType.Unknown)
        {
          _seats.Add(_seat);
        }
      }

      return _seats;
    }

    private void UpdateScrollButtonsStatus()
    {
      Dictionary<Int32, GamingTable> _gaming_tables_out;
      Dictionary<Int32, ScrollableButtonItem> _scrollable_buttons;
      int _bank_id_selected;

      _bank_id_selected = int.Parse(((System.Data.DataRowView)(cb_banks.SelectedItem)).Row.ItemArray[0].ToString());
      GamingTableBusinessLogic.ReadTables(_bank_id_selected, out _gaming_tables_out);

      // Refresh buttons
      _scrollable_buttons = CreateScrollButtonsList(_gaming_tables_out);
      uc_scrollable_button_list1.RefreshButtons(_scrollable_buttons);
      uc_scrollable_button_list1.Visible = _scrollable_buttons.Count > 0;
    }

    private void uc_gt_dealer_copy_ticket_reader1_OnValidationNumberReadEvent(ref bool IsValid, Ticket Ticket)
    {
      // Add necesary validations
      if (IsValid && Ticket != null)
      {
        if (m_selected_seat.PlaySession == null || m_selected_seat.PlaySession.PlayerIsoCode != Ticket.Cur_0)
        {
          return;
        }

        if (!CheckPermissionsValidateAccountTicket(Ticket))
        {
          uc_gt_dealer_copy_ticket_reader1.Hide_DealerCopyValidation();
          return;
        }

        IsValid = uc_gt_player_in_session1.Sale_Chips(Ticket);
      }

    }

    /// <summary>
    /// Check account on validating ticket at gaming table
    /// </summary>
    /// <param name="Ticket"></param>
    /// <returns></returns>
    private Boolean CheckPermissionsValidateAccountTicket(Ticket Ticket)
    {
      String _error_str;
      DialogResult _dlg_r;

      if (GamingTableBusinessLogic.CheckAccountValidationTicketValidation() &&
          Ticket.CreatedAccountID != m_selected_seat.AccountId)
      {
        _dlg_r = frm_message.Show(Resource.String("STR_PLAYER_TRACKING_CHECK_ACCOUNT_VALIDATED_TICKET"), Resource.String("STR_APP_GEN_MSG_WARNINGS"), MessageBoxButtons.OKCancel, MessageBoxIcon.Warning, this.ParentForm);

        if (_dlg_r == DialogResult.Cancel)
        {
          return false;
        }

        if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.GamingTablesCheckAccountValidationTicket,
                                        ProfilePermissions.TypeOperation.RequestPasswd,
                                        m_parent,
                                        out _error_str))
        {
          return false;
        }
      }

      return true;
    }

    #endregion // events

    #region Overrides

    protected override void OnVisibleChanged(EventArgs e)
    {
      if (m_thread != null)
      {
        m_thread.PauseThread = !this.Visible;
      }

      uc_scrollable_button_list1.Enabled = true;
      cb_banks.Enabled = true;
      uc_gt_player_in_session1.EnabledControls(true);
      uc_gt_card_reader1.Enabled = true;
      pnl_bottom.Enabled = true;

      m_seat_player_from_chips_sell = false;
      m_card_data_from_chips_sell = null;
      m_ticket_from_chips_sell = null;
      uc_gt_player_in_session1.ShowHideSeatUserLabel(false);

    }

    private void transparentPanel1_ButtonClick(object sender)
    {
      if (m_gaming_table != null && m_gaming_table.GamingTablePaused)
      {
        btn_play_pause_Click(null, null);
      }
      else if (!m_gaming_table.IsIntegrated && m_gaming_table.GamingTableSession == null)
      {
        btn_gaming_table_open_Click(null, null);
      }
    }

    #endregion Overrides


  } // uc_gt_player_tracking
}
