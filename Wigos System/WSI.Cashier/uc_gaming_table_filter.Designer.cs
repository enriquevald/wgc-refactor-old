namespace WSI.Cashier
{
  partial class uc_gaming_table_filter
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
      this.splitContainer1 = new System.Windows.Forms.SplitContainer();
      this.dgv_gaming_table_types = new WSI.Cashier.Controls.uc_DataGridView();
      this.dgv_gaming_tables = new WSI.Cashier.Controls.uc_DataGridView();
      this.xGT_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.xGT_GAMING_TABLE_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.xGT_TYPE_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.xGT_CASHIER_SESSION = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.xGTT_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.xGTT_GAMING_TABLE_TYPE_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.splitContainer1.Panel1.SuspendLayout();
      this.splitContainer1.Panel2.SuspendLayout();
      this.splitContainer1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dgv_gaming_table_types)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.dgv_gaming_tables)).BeginInit();
      this.SuspendLayout();
      // 
      // splitContainer1
      // 
      this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splitContainer1.Location = new System.Drawing.Point(0, 0);
      this.splitContainer1.Margin = new System.Windows.Forms.Padding(0);
      this.splitContainer1.Name = "splitContainer1";
      // 
      // splitContainer1.Panel1
      // 
      this.splitContainer1.Panel1.Controls.Add(this.dgv_gaming_table_types);
      // 
      // splitContainer1.Panel2
      // 
      this.splitContainer1.Panel2.Controls.Add(this.dgv_gaming_tables);
      this.splitContainer1.Size = new System.Drawing.Size(508, 246);
      this.splitContainer1.SplitterDistance = 254;
      this.splitContainer1.TabIndex = 0;
      // 
      // dgv_gaming_table_types
      // 
      this.dgv_gaming_table_types.AllowUserToAddRows = false;
      this.dgv_gaming_table_types.AllowUserToDeleteRows = false;
      this.dgv_gaming_table_types.AllowUserToResizeColumns = false;
      this.dgv_gaming_table_types.AllowUserToResizeRows = false;
      this.dgv_gaming_table_types.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.dgv_gaming_table_types.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.dgv_gaming_table_types.ColumnHeaderHeight = 35;
      this.dgv_gaming_table_types.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
      dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      dataGridViewCellStyle1.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
      dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
      dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
      this.dgv_gaming_table_types.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
      this.dgv_gaming_table_types.ColumnHeadersHeight = 35;
      this.dgv_gaming_table_types.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
      this.dgv_gaming_table_types.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.xGTT_NAME,
            this.xGTT_GAMING_TABLE_TYPE_ID});
      this.dgv_gaming_table_types.CornerRadius = 10;
      this.dgv_gaming_table_types.DefaultCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.dgv_gaming_table_types.DefaultCellForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.dgv_gaming_table_types.DefaultCellSelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.dgv_gaming_table_types.DefaultCellSelectionForeColor = System.Drawing.Color.White;
      dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      dataGridViewCellStyle4.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      dataGridViewCellStyle4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.White;
      dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
      this.dgv_gaming_table_types.DefaultCellStyle = dataGridViewCellStyle4;
      this.dgv_gaming_table_types.Dock = System.Windows.Forms.DockStyle.Fill;
      this.dgv_gaming_table_types.EnableHeadersVisualStyles = false;
      this.dgv_gaming_table_types.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.dgv_gaming_table_types.GridColor = System.Drawing.Color.LightGray;
      this.dgv_gaming_table_types.HeaderBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
      this.dgv_gaming_table_types.HeaderDefaultCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.dgv_gaming_table_types.HeaderDefaultCellForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.dgv_gaming_table_types.Location = new System.Drawing.Point(0, 0);
      this.dgv_gaming_table_types.Margin = new System.Windows.Forms.Padding(0);
      this.dgv_gaming_table_types.MultiSelect = false;
      this.dgv_gaming_table_types.Name = "dgv_gaming_table_types";
      this.dgv_gaming_table_types.ReadOnly = true;
      this.dgv_gaming_table_types.RowHeadersVisible = false;
      this.dgv_gaming_table_types.RowTemplate.Height = 35;
      this.dgv_gaming_table_types.RowTemplateHeight = 35;
      this.dgv_gaming_table_types.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
      this.dgv_gaming_table_types.ShowEditingIcon = false;
      this.dgv_gaming_table_types.Size = new System.Drawing.Size(254, 246);
      this.dgv_gaming_table_types.Style = WSI.Cashier.Controls.uc_DataGridView.DataGridViewStyle.WITH_ALL_ROUND_CORNERS;
      this.dgv_gaming_table_types.TabIndex = 100;
      this.dgv_gaming_table_types.SelectionChanged += new System.EventHandler(this.dgv_gaming_table_types_SelectionChanged);
      // 
      // dgv_gaming_tables
      // 
      this.dgv_gaming_tables.AllowUserToAddRows = false;
      this.dgv_gaming_tables.AllowUserToDeleteRows = false;
      this.dgv_gaming_tables.AllowUserToOrderColumns = true;
      this.dgv_gaming_tables.AllowUserToResizeColumns = false;
      this.dgv_gaming_tables.AllowUserToResizeRows = false;
      this.dgv_gaming_tables.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.dgv_gaming_tables.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.dgv_gaming_tables.ColumnHeaderHeight = 35;
      this.dgv_gaming_tables.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
      dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      dataGridViewCellStyle5.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      dataGridViewCellStyle5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
      dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
      dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
      this.dgv_gaming_tables.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
      this.dgv_gaming_tables.ColumnHeadersHeight = 35;
      this.dgv_gaming_tables.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
      this.dgv_gaming_tables.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.xGT_NAME,
            this.xGT_GAMING_TABLE_ID,
            this.xGT_TYPE_ID,
            this.xGT_CASHIER_SESSION});
      this.dgv_gaming_tables.CornerRadius = 10;
      this.dgv_gaming_tables.DefaultCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.dgv_gaming_tables.DefaultCellForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.dgv_gaming_tables.DefaultCellSelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.dgv_gaming_tables.DefaultCellSelectionForeColor = System.Drawing.Color.White;
      dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      dataGridViewCellStyle10.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      dataGridViewCellStyle10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      dataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      dataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.White;
      dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
      this.dgv_gaming_tables.DefaultCellStyle = dataGridViewCellStyle10;
      this.dgv_gaming_tables.Dock = System.Windows.Forms.DockStyle.Fill;
      this.dgv_gaming_tables.EnableHeadersVisualStyles = false;
      this.dgv_gaming_tables.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.dgv_gaming_tables.GridColor = System.Drawing.Color.LightGray;
      this.dgv_gaming_tables.HeaderBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
      this.dgv_gaming_tables.HeaderDefaultCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.dgv_gaming_tables.HeaderDefaultCellForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.dgv_gaming_tables.Location = new System.Drawing.Point(0, 0);
      this.dgv_gaming_tables.Margin = new System.Windows.Forms.Padding(0);
      this.dgv_gaming_tables.MultiSelect = false;
      this.dgv_gaming_tables.Name = "dgv_gaming_tables";
      this.dgv_gaming_tables.ReadOnly = true;
      this.dgv_gaming_tables.RowHeadersVisible = false;
      this.dgv_gaming_tables.RowTemplate.Height = 35;
      this.dgv_gaming_tables.RowTemplateHeight = 35;
      this.dgv_gaming_tables.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
      this.dgv_gaming_tables.ShowEditingIcon = false;
      this.dgv_gaming_tables.Size = new System.Drawing.Size(250, 246);
      this.dgv_gaming_tables.Style = WSI.Cashier.Controls.uc_DataGridView.DataGridViewStyle.WITH_ALL_ROUND_CORNERS;
      this.dgv_gaming_tables.TabIndex = 101;
      this.dgv_gaming_tables.SelectionChanged += new System.EventHandler(this.dgv_gaming_tables_SelectionChanged);
      // 
      // xGT_NAME
      // 
      this.xGT_NAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
      dataGridViewCellStyle6.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.xGT_NAME.DefaultCellStyle = dataGridViewCellStyle6;
      this.xGT_NAME.HeaderText = "xGT_NAME";
      this.xGT_NAME.Name = "xGT_NAME";
      this.xGT_NAME.ReadOnly = true;
      this.xGT_NAME.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
      // 
      // xGT_GAMING_TABLE_ID
      // 
      dataGridViewCellStyle7.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.xGT_GAMING_TABLE_ID.DefaultCellStyle = dataGridViewCellStyle7;
      this.xGT_GAMING_TABLE_ID.HeaderText = "xGT_GAMING_TABLE_ID";
      this.xGT_GAMING_TABLE_ID.Name = "xGT_GAMING_TABLE_ID";
      this.xGT_GAMING_TABLE_ID.ReadOnly = true;
      this.xGT_GAMING_TABLE_ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
      this.xGT_GAMING_TABLE_ID.Visible = false;
      // 
      // xGT_TYPE_ID
      // 
      dataGridViewCellStyle8.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.xGT_TYPE_ID.DefaultCellStyle = dataGridViewCellStyle8;
      this.xGT_TYPE_ID.HeaderText = "xGT_TYPE_ID";
      this.xGT_TYPE_ID.Name = "xGT_TYPE_ID";
      this.xGT_TYPE_ID.ReadOnly = true;
      this.xGT_TYPE_ID.Visible = false;
      // 
      // xGT_CASHIER_SESSION
      // 
      dataGridViewCellStyle9.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.xGT_CASHIER_SESSION.DefaultCellStyle = dataGridViewCellStyle9;
      this.xGT_CASHIER_SESSION.HeaderText = "xGT_CASHIER_SESSION";
      this.xGT_CASHIER_SESSION.Name = "xGT_CASHIER_SESSION";
      this.xGT_CASHIER_SESSION.ReadOnly = true;
      this.xGT_CASHIER_SESSION.Visible = false;
      // 
      // xGTT_NAME
      // 
      this.xGTT_NAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
      dataGridViewCellStyle2.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.xGTT_NAME.DefaultCellStyle = dataGridViewCellStyle2;
      this.xGTT_NAME.HeaderText = "xGTT_NAME";
      this.xGTT_NAME.Name = "xGTT_NAME";
      this.xGTT_NAME.ReadOnly = true;
      this.xGTT_NAME.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
      // 
      // xGTT_GAMING_TABLE_TYPE_ID
      // 
      this.xGTT_GAMING_TABLE_TYPE_ID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
      dataGridViewCellStyle3.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.xGTT_GAMING_TABLE_TYPE_ID.DefaultCellStyle = dataGridViewCellStyle3;
      this.xGTT_GAMING_TABLE_TYPE_ID.HeaderText = "xGTT_GAMING_TABLE_TYPE_ID";
      this.xGTT_GAMING_TABLE_TYPE_ID.Name = "xGTT_GAMING_TABLE_TYPE_ID";
      this.xGTT_GAMING_TABLE_TYPE_ID.ReadOnly = true;
      this.xGTT_GAMING_TABLE_TYPE_ID.Resizable = System.Windows.Forms.DataGridViewTriState.False;
      this.xGTT_GAMING_TABLE_TYPE_ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
      this.xGTT_GAMING_TABLE_TYPE_ID.Visible = false;
      // 
      // uc_gaming_table_filter
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.Color.Gainsboro;
      this.Controls.Add(this.splitContainer1);
      this.Margin = new System.Windows.Forms.Padding(0);
      this.Name = "uc_gaming_table_filter";
      this.Size = new System.Drawing.Size(508, 246);
      this.splitContainer1.Panel1.ResumeLayout(false);
      this.splitContainer1.Panel2.ResumeLayout(false);
      this.splitContainer1.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.dgv_gaming_table_types)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.dgv_gaming_tables)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.SplitContainer splitContainer1;
    private WSI.Cashier.Controls.uc_DataGridView dgv_gaming_table_types;
    private WSI.Cashier.Controls.uc_DataGridView dgv_gaming_tables;
    private System.Windows.Forms.DataGridViewTextBoxColumn xGT_NAME;
    private System.Windows.Forms.DataGridViewTextBoxColumn xGT_GAMING_TABLE_ID;
    private System.Windows.Forms.DataGridViewTextBoxColumn xGT_TYPE_ID;
    private System.Windows.Forms.DataGridViewTextBoxColumn xGT_CASHIER_SESSION;
    private System.Windows.Forms.DataGridViewTextBoxColumn xGTT_NAME;
    private System.Windows.Forms.DataGridViewTextBoxColumn xGTT_GAMING_TABLE_TYPE_ID;
  }
}
