namespace WSI.Cashier
{
  partial class uc_mobile_bank
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose (bool disposing)
    {
      if ( disposing && ( components != null ) )
      {
        components.Dispose ();
      }
      base.Dispose (disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent ()
    {
      this.components = new System.ComponentModel.Container();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(uc_mobile_bank));
      this.timer1 = new System.Windows.Forms.Timer(this.components);
      this.gb_last_activity = new System.Windows.Forms.GroupBox();
      this.pb_terminal = new System.Windows.Forms.PictureBox();
      this.lbl_last_activity_value = new WSI.Cashier.Controls.uc_label();
      this.lbl_terminal_name = new WSI.Cashier.Controls.uc_label();
      this.lbl_separator_1 = new WSI.Cashier.Controls.uc_label();
      this.btn_undo_operation = new WSI.Cashier.Controls.uc_round_button();
      this.gb_limits = new WSI.Cashier.Controls.uc_round_panel();
      this.btn_limits_edit = new WSI.Cashier.Controls.uc_round_button();
      this.lbl_limit_by_number_of_recharges = new WSI.Cashier.Controls.uc_label();
      this.lbl_limit_by_recharge = new WSI.Cashier.Controls.uc_label();
      this.lbl_limit_by_session = new WSI.Cashier.Controls.uc_label();
      this.lbl_limit_by_number_of_recharges_amount = new WSI.Cashier.Controls.uc_label();
      this.lbl_limit_by_recharge_amount = new WSI.Cashier.Controls.uc_label();
      this.lbl_limit_by_session_amount = new WSI.Cashier.Controls.uc_label();
      this.pb_limit = new System.Windows.Forms.PictureBox();
      this.btn_print_card = new WSI.Cashier.Controls.uc_round_button();
      this.gb_pin = new WSI.Cashier.Controls.uc_round_panel();
      this.pb_pin = new System.Windows.Forms.PictureBox();
      this.lbl_pin = new WSI.Cashier.Controls.uc_label();
      this.btn_change_pin = new WSI.Cashier.Controls.uc_round_button();
      this.btn_last_movements = new WSI.Cashier.Controls.uc_round_button();
      this.btn_close_sesion = new WSI.Cashier.Controls.uc_round_button();
      this.btn_set_limit = new WSI.Cashier.Controls.uc_round_button();
      this.btn_cash_deposit = new WSI.Cashier.Controls.uc_round_button();
      this.gb_card = new WSI.Cashier.Controls.uc_round_panel();
      this.btn_new_card = new WSI.Cashier.Controls.uc_round_button();
      this.pb_card = new System.Windows.Forms.PictureBox();
      this.lbl_track_number = new WSI.Cashier.Controls.uc_label();
      this.gb_user_edit = new WSI.Cashier.Controls.uc_round_panel();
      this.pb_user = new System.Windows.Forms.PictureBox();
      this.btn_account_edit = new WSI.Cashier.Controls.uc_round_button();
      this.lbl_holder_name = new WSI.Cashier.Controls.uc_label();
      this.gb_lock_unlock = new WSI.Cashier.Controls.uc_round_panel();
      this.pb_blocked = new System.Windows.Forms.PictureBox();
      this.lbl_blocked = new WSI.Cashier.Controls.uc_label();
      this.btn_card_block = new WSI.Cashier.Controls.uc_round_button();
      this.lbl_account_id_value = new WSI.Cashier.Controls.uc_label();
      this.lbl_account_id_name = new WSI.Cashier.Controls.uc_label();
      this.uc_card_reader1 = new WSI.Cashier.uc_card_reader();
      this.uc_mb_card_balance1 = new WSI.Cashier.uc_mb_card_balance();
      this.btn_change_limit = new WSI.Cashier.Controls.uc_round_button();
      this.btn_mb_browse = new WSI.Cashier.Controls.uc_round_button();
      this.label1 = new System.Windows.Forms.Label();
      this.gb_last_activity.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pb_terminal)).BeginInit();
      this.gb_limits.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pb_limit)).BeginInit();
      this.gb_pin.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pb_pin)).BeginInit();
      this.gb_card.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pb_card)).BeginInit();
      this.gb_user_edit.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pb_user)).BeginInit();
      this.gb_lock_unlock.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pb_blocked)).BeginInit();
      this.SuspendLayout();
      // 
      // timer1
      // 
      this.timer1.Interval = 500;
      this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
      // 
      // gb_last_activity
      // 
      this.gb_last_activity.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.gb_last_activity.Controls.Add(this.pb_terminal);
      this.gb_last_activity.Controls.Add(this.lbl_last_activity_value);
      this.gb_last_activity.Controls.Add(this.lbl_terminal_name);
      this.gb_last_activity.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
      this.gb_last_activity.Location = new System.Drawing.Point(5, 600);
      this.gb_last_activity.Name = "gb_last_activity";
      this.gb_last_activity.Size = new System.Drawing.Size(413, 76);
      this.gb_last_activity.TabIndex = 15;
      this.gb_last_activity.TabStop = false;
      this.gb_last_activity.Text = "xLast Activity";
      this.gb_last_activity.Visible = false;
      // 
      // pb_terminal
      // 
      this.pb_terminal.Location = new System.Drawing.Point(5, 19);
      this.pb_terminal.Name = "pb_terminal";
      this.pb_terminal.Size = new System.Drawing.Size(48, 46);
      this.pb_terminal.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
      this.pb_terminal.TabIndex = 77;
      this.pb_terminal.TabStop = false;
      // 
      // lbl_last_activity_value
      // 
      this.lbl_last_activity_value.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_last_activity_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_last_activity_value.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_last_activity_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_last_activity_value.Location = new System.Drawing.Point(273, 27);
      this.lbl_last_activity_value.Name = "lbl_last_activity_value";
      this.lbl_last_activity_value.Size = new System.Drawing.Size(130, 23);
      this.lbl_last_activity_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_last_activity_value.TabIndex = 1;
      this.lbl_last_activity_value.Text = "XDATETIME";
      this.lbl_last_activity_value.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lbl_terminal_name
      // 
      this.lbl_terminal_name.BackColor = System.Drawing.Color.Transparent;
      this.lbl_terminal_name.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_terminal_name.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_terminal_name.Location = new System.Drawing.Point(61, 27);
      this.lbl_terminal_name.Name = "lbl_terminal_name";
      this.lbl_terminal_name.Size = new System.Drawing.Size(184, 23);
      this.lbl_terminal_name.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_terminal_name.TabIndex = 0;
      this.lbl_terminal_name.Text = "XTERMINALNAME";
      this.lbl_terminal_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_separator_1
      // 
      this.lbl_separator_1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_separator_1.BackColor = System.Drawing.Color.Transparent;
      this.lbl_separator_1.Font = new System.Drawing.Font("Open Sans Semibold", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_separator_1.Location = new System.Drawing.Point(-1, 60);
      this.lbl_separator_1.Name = "lbl_separator_1";
      this.lbl_separator_1.Size = new System.Drawing.Size(980, 1);
      this.lbl_separator_1.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.lbl_separator_1.TabIndex = 4;
      // 
      // btn_undo_operation
      // 
      this.btn_undo_operation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_undo_operation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_undo_operation.FlatAppearance.BorderSize = 0;
      this.btn_undo_operation.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_undo_operation.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_undo_operation.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_undo_operation.Image = null;
      this.btn_undo_operation.IsSelected = false;
      this.btn_undo_operation.Location = new System.Drawing.Point(453, 511);
      this.btn_undo_operation.Name = "btn_undo_operation";
      this.btn_undo_operation.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_undo_operation.Size = new System.Drawing.Size(130, 50);
      this.btn_undo_operation.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_undo_operation.TabIndex = 12;
      this.btn_undo_operation.Text = "XUNDO OPERATION";
      this.btn_undo_operation.UseVisualStyleBackColor = false;
      // 
      // gb_limits
      // 
      this.gb_limits.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.gb_limits.BackColor = System.Drawing.Color.Transparent;
      this.gb_limits.BorderColor = System.Drawing.Color.Empty;
      this.gb_limits.Controls.Add(this.btn_limits_edit);
      this.gb_limits.Controls.Add(this.lbl_limit_by_number_of_recharges);
      this.gb_limits.Controls.Add(this.lbl_limit_by_recharge);
      this.gb_limits.Controls.Add(this.lbl_limit_by_session);
      this.gb_limits.Controls.Add(this.lbl_limit_by_number_of_recharges_amount);
      this.gb_limits.Controls.Add(this.lbl_limit_by_recharge_amount);
      this.gb_limits.Controls.Add(this.lbl_limit_by_session_amount);
      this.gb_limits.Controls.Add(this.pb_limit);
      this.gb_limits.CornerRadius = 10;
      this.gb_limits.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_limits.HeaderBackColor = System.Drawing.Color.Empty;
      this.gb_limits.HeaderForeColor = System.Drawing.Color.Empty;
      this.gb_limits.HeaderHeight = 0;
      this.gb_limits.HeaderSubText = null;
      this.gb_limits.HeaderText = null;
      this.gb_limits.Location = new System.Drawing.Point(7, 416);
      this.gb_limits.Name = "gb_limits";
      this.gb_limits.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_limits.Size = new System.Drawing.Size(412, 157);
      this.gb_limits.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.WITHOUT_HEAD;
      this.gb_limits.TabIndex = 6;
      // 
      // btn_limits_edit
      // 
      this.btn_limits_edit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_limits_edit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_limits_edit.FlatAppearance.BorderSize = 0;
      this.btn_limits_edit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_limits_edit.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_limits_edit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_limits_edit.Image = null;
      this.btn_limits_edit.IsSelected = false;
      this.btn_limits_edit.Location = new System.Drawing.Point(270, 94);
      this.btn_limits_edit.Name = "btn_limits_edit";
      this.btn_limits_edit.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_limits_edit.Size = new System.Drawing.Size(130, 50);
      this.btn_limits_edit.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_limits_edit.TabIndex = 6;
      this.btn_limits_edit.Text = "XLIMITS EDIT";
      this.btn_limits_edit.UseVisualStyleBackColor = false;
      // 
      // lbl_limit_by_number_of_recharges
      // 
      this.lbl_limit_by_number_of_recharges.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_limit_by_number_of_recharges.BackColor = System.Drawing.Color.Transparent;
      this.lbl_limit_by_number_of_recharges.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_limit_by_number_of_recharges.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_limit_by_number_of_recharges.Location = new System.Drawing.Point(58, 63);
      this.lbl_limit_by_number_of_recharges.Name = "lbl_limit_by_number_of_recharges";
      this.lbl_limit_by_number_of_recharges.Size = new System.Drawing.Size(221, 23);
      this.lbl_limit_by_number_of_recharges.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_limit_by_number_of_recharges.TabIndex = 4;
      this.lbl_limit_by_number_of_recharges.Text = "TTTTTTTTTTTTT";
      this.lbl_limit_by_number_of_recharges.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.lbl_limit_by_number_of_recharges.UseMnemonic = false;
      // 
      // lbl_limit_by_recharge
      // 
      this.lbl_limit_by_recharge.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_limit_by_recharge.BackColor = System.Drawing.Color.Transparent;
      this.lbl_limit_by_recharge.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_limit_by_recharge.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_limit_by_recharge.Location = new System.Drawing.Point(58, 37);
      this.lbl_limit_by_recharge.Name = "lbl_limit_by_recharge";
      this.lbl_limit_by_recharge.Size = new System.Drawing.Size(221, 23);
      this.lbl_limit_by_recharge.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_limit_by_recharge.TabIndex = 2;
      this.lbl_limit_by_recharge.Text = "TTTTTTTTTTTTT";
      this.lbl_limit_by_recharge.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.lbl_limit_by_recharge.UseMnemonic = false;
      // 
      // lbl_limit_by_session
      // 
      this.lbl_limit_by_session.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_limit_by_session.BackColor = System.Drawing.Color.Transparent;
      this.lbl_limit_by_session.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_limit_by_session.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_limit_by_session.Location = new System.Drawing.Point(58, 11);
      this.lbl_limit_by_session.Name = "lbl_limit_by_session";
      this.lbl_limit_by_session.Size = new System.Drawing.Size(221, 23);
      this.lbl_limit_by_session.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_limit_by_session.TabIndex = 0;
      this.lbl_limit_by_session.Text = "TTTTTTTTTTTTT";
      this.lbl_limit_by_session.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.lbl_limit_by_session.UseMnemonic = false;
      // 
      // lbl_limit_by_number_of_recharges_amount
      // 
      this.lbl_limit_by_number_of_recharges_amount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_limit_by_number_of_recharges_amount.BackColor = System.Drawing.Color.Transparent;
      this.lbl_limit_by_number_of_recharges_amount.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_limit_by_number_of_recharges_amount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_limit_by_number_of_recharges_amount.Location = new System.Drawing.Point(240, 63);
      this.lbl_limit_by_number_of_recharges_amount.Name = "lbl_limit_by_number_of_recharges_amount";
      this.lbl_limit_by_number_of_recharges_amount.Size = new System.Drawing.Size(160, 23);
      this.lbl_limit_by_number_of_recharges_amount.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_limit_by_number_of_recharges_amount.TabIndex = 5;
      this.lbl_limit_by_number_of_recharges_amount.Text = "999,999,999.99";
      this.lbl_limit_by_number_of_recharges_amount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      this.lbl_limit_by_number_of_recharges_amount.UseMnemonic = false;
      // 
      // lbl_limit_by_recharge_amount
      // 
      this.lbl_limit_by_recharge_amount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_limit_by_recharge_amount.BackColor = System.Drawing.Color.Transparent;
      this.lbl_limit_by_recharge_amount.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_limit_by_recharge_amount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_limit_by_recharge_amount.Location = new System.Drawing.Point(236, 37);
      this.lbl_limit_by_recharge_amount.Name = "lbl_limit_by_recharge_amount";
      this.lbl_limit_by_recharge_amount.Size = new System.Drawing.Size(164, 23);
      this.lbl_limit_by_recharge_amount.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_limit_by_recharge_amount.TabIndex = 3;
      this.lbl_limit_by_recharge_amount.Text = "$999,999,999.99";
      this.lbl_limit_by_recharge_amount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      this.lbl_limit_by_recharge_amount.UseMnemonic = false;
      // 
      // lbl_limit_by_session_amount
      // 
      this.lbl_limit_by_session_amount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_limit_by_session_amount.BackColor = System.Drawing.Color.Transparent;
      this.lbl_limit_by_session_amount.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_limit_by_session_amount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_limit_by_session_amount.Location = new System.Drawing.Point(232, 11);
      this.lbl_limit_by_session_amount.Name = "lbl_limit_by_session_amount";
      this.lbl_limit_by_session_amount.Size = new System.Drawing.Size(170, 23);
      this.lbl_limit_by_session_amount.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_limit_by_session_amount.TabIndex = 1;
      this.lbl_limit_by_session_amount.Text = "$999,999,999.99";
      this.lbl_limit_by_session_amount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      this.lbl_limit_by_session_amount.UseMnemonic = false;
      // 
      // pb_limit
      // 
      this.pb_limit.InitialImage = ((System.Drawing.Image)(resources.GetObject("pb_limit.InitialImage")));
      this.pb_limit.Location = new System.Drawing.Point(5, 15);
      this.pb_limit.Name = "pb_limit";
      this.pb_limit.Size = new System.Drawing.Size(48, 46);
      this.pb_limit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
      this.pb_limit.TabIndex = 72;
      this.pb_limit.TabStop = false;
      // 
      // btn_print_card
      // 
      this.btn_print_card.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_print_card.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_print_card.FlatAppearance.BorderSize = 0;
      this.btn_print_card.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_print_card.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_print_card.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_print_card.Image = null;
      this.btn_print_card.IsSelected = false;
      this.btn_print_card.Location = new System.Drawing.Point(596, 511);
      this.btn_print_card.Name = "btn_print_card";
      this.btn_print_card.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_print_card.Size = new System.Drawing.Size(130, 50);
      this.btn_print_card.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_print_card.TabIndex = 13;
      this.btn_print_card.Text = "XPRINT CARD";
      this.btn_print_card.UseVisualStyleBackColor = false;
      // 
      // gb_pin
      // 
      this.gb_pin.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.gb_pin.BackColor = System.Drawing.Color.Transparent;
      this.gb_pin.BorderColor = System.Drawing.Color.Empty;
      this.gb_pin.Controls.Add(this.pb_pin);
      this.gb_pin.Controls.Add(this.lbl_pin);
      this.gb_pin.Controls.Add(this.btn_change_pin);
      this.gb_pin.CornerRadius = 10;
      this.gb_pin.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_pin.HeaderBackColor = System.Drawing.Color.Empty;
      this.gb_pin.HeaderForeColor = System.Drawing.Color.Empty;
      this.gb_pin.HeaderHeight = 0;
      this.gb_pin.HeaderSubText = null;
      this.gb_pin.HeaderText = null;
      this.gb_pin.Location = new System.Drawing.Point(6, 336);
      this.gb_pin.Name = "gb_pin";
      this.gb_pin.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_pin.Size = new System.Drawing.Size(413, 73);
      this.gb_pin.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.WITHOUT_HEAD;
      this.gb_pin.TabIndex = 5;
      this.gb_pin.Text = "xPin";
      // 
      // pb_pin
      // 
      this.pb_pin.Enabled = false;
      this.pb_pin.Image = global::WSI.Cashier.Properties.Resources.pin;
      this.pb_pin.InitialImage = global::WSI.Cashier.Properties.Resources.pin;
      this.pb_pin.Location = new System.Drawing.Point(5, 14);
      this.pb_pin.Name = "pb_pin";
      this.pb_pin.Size = new System.Drawing.Size(48, 46);
      this.pb_pin.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
      this.pb_pin.TabIndex = 75;
      this.pb_pin.TabStop = false;
      // 
      // lbl_pin
      // 
      this.lbl_pin.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_pin.BackColor = System.Drawing.Color.Transparent;
      this.lbl_pin.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_pin.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_pin.Location = new System.Drawing.Point(58, 25);
      this.lbl_pin.Name = "lbl_pin";
      this.lbl_pin.Size = new System.Drawing.Size(204, 23);
      this.lbl_pin.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_pin.TabIndex = 0;
      this.lbl_pin.Text = "TTTTTTTTTTTTT";
      this.lbl_pin.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // btn_change_pin
      // 
      this.btn_change_pin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_change_pin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_change_pin.FlatAppearance.BorderSize = 0;
      this.btn_change_pin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_change_pin.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_change_pin.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_change_pin.Image = null;
      this.btn_change_pin.IsSelected = false;
      this.btn_change_pin.Location = new System.Drawing.Point(273, 11);
      this.btn_change_pin.Name = "btn_change_pin";
      this.btn_change_pin.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_change_pin.Size = new System.Drawing.Size(130, 50);
      this.btn_change_pin.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_change_pin.TabIndex = 1;
      this.btn_change_pin.Text = "XCHANGE PIN";
      this.btn_change_pin.UseVisualStyleBackColor = false;
      // 
      // btn_last_movements
      // 
      this.btn_last_movements.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_last_movements.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_last_movements.FlatAppearance.BorderSize = 0;
      this.btn_last_movements.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_last_movements.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_last_movements.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_last_movements.Image = null;
      this.btn_last_movements.IsSelected = false;
      this.btn_last_movements.Location = new System.Drawing.Point(738, 511);
      this.btn_last_movements.Name = "btn_last_movements";
      this.btn_last_movements.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_last_movements.Size = new System.Drawing.Size(130, 50);
      this.btn_last_movements.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_last_movements.TabIndex = 14;
      this.btn_last_movements.Text = "XLAST MOVEMENTS";
      this.btn_last_movements.UseVisualStyleBackColor = false;
      // 
      // btn_close_sesion
      // 
      this.btn_close_sesion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_close_sesion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_close_sesion.FlatAppearance.BorderSize = 0;
      this.btn_close_sesion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_close_sesion.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_close_sesion.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_close_sesion.Image = null;
      this.btn_close_sesion.IsSelected = false;
      this.btn_close_sesion.Location = new System.Drawing.Point(738, 450);
      this.btn_close_sesion.Name = "btn_close_sesion";
      this.btn_close_sesion.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_close_sesion.Size = new System.Drawing.Size(130, 50);
      this.btn_close_sesion.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_close_sesion.TabIndex = 11;
      this.btn_close_sesion.Text = "XCLOSE SESSION";
      this.btn_close_sesion.UseVisualStyleBackColor = false;
      // 
      // btn_set_limit
      // 
      this.btn_set_limit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_set_limit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_set_limit.FlatAppearance.BorderSize = 0;
      this.btn_set_limit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_set_limit.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_set_limit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_set_limit.Image = null;
      this.btn_set_limit.IsSelected = false;
      this.btn_set_limit.Location = new System.Drawing.Point(743, 191);
      this.btn_set_limit.Name = "btn_set_limit";
      this.btn_set_limit.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_set_limit.Size = new System.Drawing.Size(130, 50);
      this.btn_set_limit.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_set_limit.TabIndex = 9;
      this.btn_set_limit.Text = "XSET LIMIT";
      this.btn_set_limit.UseVisualStyleBackColor = false;
      // 
      // btn_cash_deposit
      // 
      this.btn_cash_deposit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_cash_deposit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_cash_deposit.FlatAppearance.BorderSize = 0;
      this.btn_cash_deposit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_cash_deposit.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_cash_deposit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_cash_deposit.Image = null;
      this.btn_cash_deposit.IsSelected = false;
      this.btn_cash_deposit.Location = new System.Drawing.Point(743, 252);
      this.btn_cash_deposit.Name = "btn_cash_deposit";
      this.btn_cash_deposit.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_cash_deposit.Size = new System.Drawing.Size(130, 50);
      this.btn_cash_deposit.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_cash_deposit.TabIndex = 10;
      this.btn_cash_deposit.Text = "XCASH DEPOSIT";
      this.btn_cash_deposit.UseVisualStyleBackColor = false;
      // 
      // gb_card
      // 
      this.gb_card.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.gb_card.BackColor = System.Drawing.Color.Transparent;
      this.gb_card.BorderColor = System.Drawing.Color.Empty;
      this.gb_card.Controls.Add(this.btn_new_card);
      this.gb_card.Controls.Add(this.pb_card);
      this.gb_card.Controls.Add(this.lbl_track_number);
      this.gb_card.CornerRadius = 10;
      this.gb_card.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_card.HeaderBackColor = System.Drawing.Color.Empty;
      this.gb_card.HeaderForeColor = System.Drawing.Color.Empty;
      this.gb_card.HeaderHeight = 0;
      this.gb_card.HeaderSubText = null;
      this.gb_card.HeaderText = null;
      this.gb_card.Location = new System.Drawing.Point(6, 98);
      this.gb_card.Name = "gb_card";
      this.gb_card.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_card.Size = new System.Drawing.Size(413, 73);
      this.gb_card.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.WITHOUT_HEAD;
      this.gb_card.TabIndex = 2;
      // 
      // btn_new_card
      // 
      this.btn_new_card.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_new_card.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_new_card.FlatAppearance.BorderSize = 0;
      this.btn_new_card.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_new_card.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_new_card.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_new_card.Image = null;
      this.btn_new_card.IsSelected = false;
      this.btn_new_card.Location = new System.Drawing.Point(273, 11);
      this.btn_new_card.Name = "btn_new_card";
      this.btn_new_card.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_new_card.Size = new System.Drawing.Size(130, 50);
      this.btn_new_card.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_new_card.TabIndex = 1;
      this.btn_new_card.Text = "XNEW CARD";
      this.btn_new_card.UseVisualStyleBackColor = false;
      this.btn_new_card.Click += new System.EventHandler(this.btn_new_card_Click);
      // 
      // pb_card
      // 
      this.pb_card.Enabled = false;
      this.pb_card.Image = global::WSI.Cashier.Properties.Resources.mobileBank;
      this.pb_card.InitialImage = global::WSI.Cashier.Properties.Resources.mobileBank;
      this.pb_card.Location = new System.Drawing.Point(5, 14);
      this.pb_card.Name = "pb_card";
      this.pb_card.Size = new System.Drawing.Size(48, 46);
      this.pb_card.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
      this.pb_card.TabIndex = 60;
      this.pb_card.TabStop = false;
      // 
      // lbl_track_number
      // 
      this.lbl_track_number.BackColor = System.Drawing.Color.Transparent;
      this.lbl_track_number.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_track_number.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_track_number.Location = new System.Drawing.Point(57, 28);
      this.lbl_track_number.Name = "lbl_track_number";
      this.lbl_track_number.Size = new System.Drawing.Size(212, 20);
      this.lbl_track_number.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_track_number.TabIndex = 0;
      this.lbl_track_number.Text = "88888888888888888812";
      this.lbl_track_number.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // gb_user_edit
      // 
      this.gb_user_edit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.gb_user_edit.BackColor = System.Drawing.Color.Transparent;
      this.gb_user_edit.BorderColor = System.Drawing.Color.Empty;
      this.gb_user_edit.Controls.Add(this.pb_user);
      this.gb_user_edit.Controls.Add(this.btn_account_edit);
      this.gb_user_edit.Controls.Add(this.lbl_holder_name);
      this.gb_user_edit.CornerRadius = 10;
      this.gb_user_edit.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_user_edit.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.gb_user_edit.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_user_edit.HeaderHeight = 0;
      this.gb_user_edit.HeaderSubText = null;
      this.gb_user_edit.HeaderText = null;
      this.gb_user_edit.Location = new System.Drawing.Point(6, 177);
      this.gb_user_edit.Name = "gb_user_edit";
      this.gb_user_edit.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_user_edit.Size = new System.Drawing.Size(413, 73);
      this.gb_user_edit.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.WITHOUT_HEAD;
      this.gb_user_edit.TabIndex = 3;
      // 
      // pb_user
      // 
      this.pb_user.Image = global::WSI.Cashier.Properties.Resources.anonymous_user;
      this.pb_user.InitialImage = global::WSI.Cashier.Properties.Resources.iconoUsuario;
      this.pb_user.Location = new System.Drawing.Point(5, 14);
      this.pb_user.Name = "pb_user";
      this.pb_user.Size = new System.Drawing.Size(48, 46);
      this.pb_user.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
      this.pb_user.TabIndex = 72;
      this.pb_user.TabStop = false;
      // 
      // btn_account_edit
      // 
      this.btn_account_edit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_account_edit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_account_edit.FlatAppearance.BorderSize = 0;
      this.btn_account_edit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_account_edit.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_account_edit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_account_edit.Image = null;
      this.btn_account_edit.IsSelected = false;
      this.btn_account_edit.Location = new System.Drawing.Point(273, 11);
      this.btn_account_edit.Name = "btn_account_edit";
      this.btn_account_edit.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_account_edit.Size = new System.Drawing.Size(130, 50);
      this.btn_account_edit.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_account_edit.TabIndex = 1;
      this.btn_account_edit.Text = "XEDIT";
      this.btn_account_edit.UseVisualStyleBackColor = false;
      // 
      // lbl_holder_name
      // 
      this.lbl_holder_name.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_holder_name.BackColor = System.Drawing.Color.Transparent;
      this.lbl_holder_name.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_holder_name.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_holder_name.Location = new System.Drawing.Point(59, 27);
      this.lbl_holder_name.Name = "lbl_holder_name";
      this.lbl_holder_name.Size = new System.Drawing.Size(221, 23);
      this.lbl_holder_name.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_holder_name.TabIndex = 0;
      this.lbl_holder_name.Text = "TTTTTTTTTTTTT";
      this.lbl_holder_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.lbl_holder_name.UseMnemonic = false;
      // 
      // gb_lock_unlock
      // 
      this.gb_lock_unlock.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.gb_lock_unlock.BackColor = System.Drawing.Color.Transparent;
      this.gb_lock_unlock.BorderColor = System.Drawing.Color.Empty;
      this.gb_lock_unlock.Controls.Add(this.pb_blocked);
      this.gb_lock_unlock.Controls.Add(this.lbl_blocked);
      this.gb_lock_unlock.Controls.Add(this.btn_card_block);
      this.gb_lock_unlock.CornerRadius = 10;
      this.gb_lock_unlock.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_lock_unlock.HeaderBackColor = System.Drawing.Color.Empty;
      this.gb_lock_unlock.HeaderForeColor = System.Drawing.Color.Empty;
      this.gb_lock_unlock.HeaderHeight = 0;
      this.gb_lock_unlock.HeaderSubText = null;
      this.gb_lock_unlock.HeaderText = null;
      this.gb_lock_unlock.Location = new System.Drawing.Point(6, 256);
      this.gb_lock_unlock.Name = "gb_lock_unlock";
      this.gb_lock_unlock.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_lock_unlock.Size = new System.Drawing.Size(413, 73);
      this.gb_lock_unlock.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.WITHOUT_HEAD;
      this.gb_lock_unlock.TabIndex = 4;
      // 
      // pb_blocked
      // 
      this.pb_blocked.Enabled = false;
      this.pb_blocked.ErrorImage = global::WSI.Cashier.Properties.Resources.notLocked;
      this.pb_blocked.Image = global::WSI.Cashier.Properties.Resources.notLocked;
      this.pb_blocked.InitialImage = global::WSI.Cashier.Properties.Resources.notLocked;
      this.pb_blocked.Location = new System.Drawing.Point(5, 14);
      this.pb_blocked.Name = "pb_blocked";
      this.pb_blocked.Size = new System.Drawing.Size(48, 46);
      this.pb_blocked.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
      this.pb_blocked.TabIndex = 75;
      this.pb_blocked.TabStop = false;
      // 
      // lbl_blocked
      // 
      this.lbl_blocked.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_blocked.BackColor = System.Drawing.Color.Transparent;
      this.lbl_blocked.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_blocked.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_blocked.Location = new System.Drawing.Point(58, 25);
      this.lbl_blocked.Name = "lbl_blocked";
      this.lbl_blocked.Size = new System.Drawing.Size(204, 23);
      this.lbl_blocked.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_blocked.TabIndex = 0;
      this.lbl_blocked.Text = "TTTTTTTTTTTTT";
      this.lbl_blocked.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // btn_card_block
      // 
      this.btn_card_block.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_card_block.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_card_block.FlatAppearance.BorderSize = 0;
      this.btn_card_block.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_card_block.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_card_block.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_card_block.Image = null;
      this.btn_card_block.IsSelected = false;
      this.btn_card_block.Location = new System.Drawing.Point(273, 11);
      this.btn_card_block.Name = "btn_card_block";
      this.btn_card_block.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_card_block.Size = new System.Drawing.Size(130, 50);
      this.btn_card_block.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_card_block.TabIndex = 1;
      this.btn_card_block.Text = "XUNBLOCK";
      this.btn_card_block.UseVisualStyleBackColor = false;
      // 
      // lbl_account_id_value
      // 
      this.lbl_account_id_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_account_id_value.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_account_id_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_account_id_value.Location = new System.Drawing.Point(177, 63);
      this.lbl_account_id_value.Name = "lbl_account_id_value";
      this.lbl_account_id_value.Size = new System.Drawing.Size(255, 30);
      this.lbl_account_id_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_account_id_value.TabIndex = 5;
      this.lbl_account_id_value.Text = "0";
      this.lbl_account_id_value.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_account_id_name
      // 
      this.lbl_account_id_name.BackColor = System.Drawing.Color.Transparent;
      this.lbl_account_id_name.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_account_id_name.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_account_id_name.Location = new System.Drawing.Point(8, 63);
      this.lbl_account_id_name.Name = "lbl_account_id_name";
      this.lbl_account_id_name.Size = new System.Drawing.Size(163, 30);
      this.lbl_account_id_name.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_account_id_name.TabIndex = 3;
      this.lbl_account_id_name.Text = "xAccount";
      this.lbl_account_id_name.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // uc_card_reader1
      // 
      this.uc_card_reader1.ErrorMessageBelow = false;
      this.uc_card_reader1.InvalidCardTextVisible = false;
      this.uc_card_reader1.Location = new System.Drawing.Point(4, 0);
      this.uc_card_reader1.MaximumSize = new System.Drawing.Size(800, 60);
      this.uc_card_reader1.MinimumSize = new System.Drawing.Size(800, 60);
      this.uc_card_reader1.Name = "uc_card_reader1";
      this.uc_card_reader1.Size = new System.Drawing.Size(800, 60);
      this.uc_card_reader1.TabIndex = 0;
      // 
      // uc_mb_card_balance1
      // 
      this.uc_mb_card_balance1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.uc_mb_card_balance1.CashIn = "$ 0,00";
      this.uc_mb_card_balance1.CashOut = "$ 0,00";
      this.uc_mb_card_balance1.CurrentBalance = "$ 0,00";
      this.uc_mb_card_balance1.Location = new System.Drawing.Point(426, 98);
      this.uc_mb_card_balance1.Name = "uc_mb_card_balance1";
      this.uc_mb_card_balance1.Size = new System.Drawing.Size(313, 350);
      this.uc_mb_card_balance1.TabIndex = 7;
      // 
      // btn_change_limit
      // 
      this.btn_change_limit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_change_limit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_change_limit.FlatAppearance.BorderSize = 0;
      this.btn_change_limit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_change_limit.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_change_limit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_change_limit.Image = null;
      this.btn_change_limit.IsSelected = false;
      this.btn_change_limit.Location = new System.Drawing.Point(743, 130);
      this.btn_change_limit.Name = "btn_change_limit";
      this.btn_change_limit.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_change_limit.Size = new System.Drawing.Size(130, 50);
      this.btn_change_limit.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_change_limit.TabIndex = 8;
      this.btn_change_limit.Text = "XCHANGE LIMIT";
      this.btn_change_limit.UseVisualStyleBackColor = false;
      // 
      // btn_mb_browse
      // 
      this.btn_mb_browse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_mb_browse.BackColor = System.Drawing.Color.Transparent;
      this.btn_mb_browse.CornerRadius = 0;
      this.btn_mb_browse.FlatAppearance.BorderSize = 0;
      this.btn_mb_browse.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_mb_browse.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_mb_browse.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_mb_browse.Image = global::WSI.Cashier.Properties.Resources.btnSearch;
      this.btn_mb_browse.IsSelected = false;
      this.btn_mb_browse.Location = new System.Drawing.Point(815, 3);
      this.btn_mb_browse.Margin = new System.Windows.Forms.Padding(0);
      this.btn_mb_browse.Name = "btn_mb_browse";
      this.btn_mb_browse.SelectedColor = System.Drawing.Color.Empty;
      this.btn_mb_browse.Size = new System.Drawing.Size(55, 55);
      this.btn_mb_browse.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.NONE;
      this.btn_mb_browse.TabIndex = 1;
      this.btn_mb_browse.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.btn_mb_browse.UseVisualStyleBackColor = false;
      // 
      // label1
      // 
      this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.label1.BackColor = System.Drawing.Color.Black;
      this.label1.Location = new System.Drawing.Point(-3, 60);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(885, 1);
      this.label1.TabIndex = 88;
      // 
      // uc_mobile_bank
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.Controls.Add(this.label1);
      this.Controls.Add(this.btn_change_limit);
      this.Controls.Add(this.btn_close_sesion);
      this.Controls.Add(this.uc_mb_card_balance1);
      this.Controls.Add(this.btn_set_limit);
      this.Controls.Add(this.lbl_separator_1);
      this.Controls.Add(this.btn_undo_operation);
      this.Controls.Add(this.btn_cash_deposit);
      this.Controls.Add(this.gb_limits);
      this.Controls.Add(this.btn_print_card);
      this.Controls.Add(this.btn_mb_browse);
      this.Controls.Add(this.gb_pin);
      this.Controls.Add(this.btn_last_movements);
      this.Controls.Add(this.gb_card);
      this.Controls.Add(this.gb_user_edit);
      this.Controls.Add(this.gb_lock_unlock);
      this.Controls.Add(this.gb_last_activity);
      this.Controls.Add(this.lbl_account_id_value);
      this.Controls.Add(this.lbl_account_id_name);
      this.Controls.Add(this.uc_card_reader1);
      this.Name = "uc_mobile_bank";
      this.Size = new System.Drawing.Size(884, 717);
      this.gb_last_activity.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.pb_terminal)).EndInit();
      this.gb_limits.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.pb_limit)).EndInit();
      this.gb_pin.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.pb_pin)).EndInit();
      this.gb_card.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.pb_card)).EndInit();
      this.gb_user_edit.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.pb_user)).EndInit();
      this.gb_lock_unlock.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.pb_blocked)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Timer timer1;
    private Controls.uc_round_button btn_last_movements;
    private Controls.uc_round_button btn_cash_deposit;
    private Controls.uc_round_panel gb_card;
    private Controls.uc_round_button btn_new_card;
    private System.Windows.Forms.PictureBox pb_card;
    private Controls.uc_label lbl_track_number;
    private Controls.uc_round_panel gb_user_edit;
    private System.Windows.Forms.PictureBox pb_user;
    private Controls.uc_round_button btn_account_edit;
    private Controls.uc_label lbl_holder_name;
    private Controls.uc_round_panel gb_lock_unlock;
    private System.Windows.Forms.PictureBox pb_blocked;
    private Controls.uc_label lbl_blocked;
    private Controls.uc_round_button btn_card_block;
    private System.Windows.Forms.GroupBox gb_last_activity;
    private System.Windows.Forms.PictureBox pb_terminal;
    private Controls.uc_label lbl_last_activity_value;
    private Controls.uc_label lbl_terminal_name;
    private Controls.uc_label lbl_account_id_value;
    private Controls.uc_label lbl_account_id_name;
    private uc_card_reader uc_card_reader1;
    private Controls.uc_round_panel gb_pin;
    private System.Windows.Forms.PictureBox pb_pin;
    private Controls.uc_label lbl_pin;
    private Controls.uc_round_button btn_change_pin;
    private Controls.uc_round_button btn_mb_browse;
    private Controls.uc_round_button btn_print_card;
    private Controls.uc_round_button btn_set_limit;
    private Controls.uc_round_button btn_close_sesion;
    private Controls.uc_round_panel gb_limits;
    private System.Windows.Forms.PictureBox pb_limit;
    private Controls.uc_round_button btn_limits_edit;
    private Controls.uc_label lbl_limit_by_session;
    private Controls.uc_label lbl_limit_by_number_of_recharges;
    private Controls.uc_label lbl_limit_by_recharge;
    private Controls.uc_label lbl_limit_by_number_of_recharges_amount;
    private Controls.uc_label lbl_limit_by_recharge_amount;
    private Controls.uc_label lbl_limit_by_session_amount;
    private Controls.uc_round_button btn_undo_operation;
    private uc_mb_card_balance uc_mb_card_balance1;
    private Controls.uc_label lbl_separator_1;
    private Controls.uc_round_button btn_change_limit;
    private System.Windows.Forms.Label label1;


  }
}
