//------------------------------------------------------------------------------
// Copyright � 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_sub_amount_confirm.cs
// 
//   DESCRIPTION: Implements the following classes:
//                1. frm_sessions
//
//        AUTHOR: RRT
// 
// CREATION DATE: 14-AUG-2007
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 14-AUG-2007 RRT     First release.
// 18-SEP-2017 DPC    WIGOS-5268: Cashier & GUI - Icons - Implement
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WSI.Common;

namespace WSI.Cashier
{
  public partial class frm_sub_amount_confirm : Controls.frm_base_icon
  {
    #region Attributes

    #endregion

    #region Constructor

    /// <summary>
    /// Constructor method.
    /// 1. Initialize controls.
    /// 2. Initialize control�s resources. (NLS, Images, etc)
    /// </summary>

    public frm_sub_amount_confirm()
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_sub_amount_confirm", Log.Type.Message);

      InitializeComponent();

      InitializeControlResources();
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Initialize control resources (NLS strings, images, etc)
    /// </summary>
    private void InitializeControlResources()
    {
      // - NLS Strings:
      //   - Title:

      //   - Labels

      //   - Buttons
      btn_ok.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_OK");
      btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");

      // - Images:

    } // InitializeControlResources

    #endregion

    #region Public Methods

    #endregion
  }
}