namespace WSI.Cashier
{
  partial class frm_bank_transaction_data
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_bank_transaction_data));
      this.label3 = new WSI.Cashier.Controls.uc_label();
      this.label4 = new WSI.Cashier.Controls.uc_label();
      this.textBox1 = new WSI.Cashier.Controls.uc_round_textbox();
      this.comboBox1 = new WSI.Cashier.Controls.uc_round_combobox();
      this.label5 = new WSI.Cashier.Controls.uc_label();
      this.label6 = new WSI.Cashier.Controls.uc_label();
      this.label7 = new WSI.Cashier.Controls.uc_label();
      this.label8 = new WSI.Cashier.Controls.uc_label();
      this.label9 = new WSI.Cashier.Controls.uc_label();
      this.textBox2 = new WSI.Cashier.Controls.uc_round_textbox();
      this.tab_bank_transaction_data = new WSI.Cashier.Controls.uc_round_tab_control();
      this.tabPage1 = new System.Windows.Forms.TabPage();
      this.tlp_card = new System.Windows.Forms.TableLayoutPanel();
      this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
      this.lbl_exp = new WSI.Cashier.Controls.uc_label();
      this.ef_mm = new WSI.Cashier.Controls.uc_maskedTextBox();
      this.chk_manual_entry = new WSI.Cashier.Controls.uc_checkBox();
      this.lbl_card_number = new WSI.Cashier.Controls.uc_label();
      this.lbl_card_holder_name = new WSI.Cashier.Controls.uc_label();
      this.ef_holder_name = new WSI.Cashier.CharacterTextBox();
      this.lbl_expiration_date = new WSI.Cashier.Controls.uc_label();
      this.ef_card_number = new WSI.Cashier.NumericTextBox();
      this.tlp_check = new System.Windows.Forms.TableLayoutPanel();
      this.ef_account_number = new WSI.Cashier.CharacterTextBox();
      this.ef_check_number = new WSI.Cashier.NumericTextBox();
      this.ef_check_payee = new WSI.Cashier.CharacterTextBox();
      this.lbl_check_routing_number = new WSI.Cashier.Controls.uc_label();
      this.lbl_check_number = new WSI.Cashier.Controls.uc_label();
      this.lbl_check_account_number = new WSI.Cashier.Controls.uc_label();
      this.lbl_check_payee = new WSI.Cashier.Controls.uc_label();
      this.lbl_check_date = new WSI.Cashier.Controls.uc_label();
      this.ef_routing_number = new WSI.Cashier.CharacterTextBox();
      this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
      this.uc_check_date = new WSI.Cashier.uc_datetime();
      this.cb_check_types = new WSI.Cashier.Controls.uc_round_combobox();
      this.lbl_check_type = new WSI.Cashier.Controls.uc_label();
      this.tlp_all = new System.Windows.Forms.TableLayoutPanel();
      this.lbl_bank_name = new WSI.Cashier.Controls.uc_label();
      this.lbl_nationality = new WSI.Cashier.Controls.uc_label();
      this.cb_nationality = new WSI.Cashier.Controls.uc_round_auto_combobox();
      this.ef_bank_name = new WSI.Cashier.CharacterTextBox();
      this.lbl_card_type = new WSI.Cashier.Controls.uc_label();
      this.cb_card_types = new WSI.Cashier.Controls.uc_round_combobox();
      this.pictureBox1 = new System.Windows.Forms.PictureBox();
      this.ef_total_amount = new WSI.Cashier.NumericTextBox();
      this.lbl_swip_card = new WSI.Cashier.Controls.uc_label();
      this.lbl_total_amount = new WSI.Cashier.Controls.uc_label();
      this.tabPage2 = new System.Windows.Forms.TabPage();
      this.txt_comments = new WSI.Cashier.Controls.uc_round_textbox();
      this.btn_keyboard = new WSI.Cashier.Controls.uc_round_button();
      this.btn_cancel = new WSI.Cashier.Controls.uc_round_button();
      this.btn_ok = new WSI.Cashier.Controls.uc_round_button();
      this.lbl_msg_blink = new WSI.Cashier.Controls.uc_label();
      this.panel1 = new System.Windows.Forms.Panel();
      this.panel2 = new System.Windows.Forms.Panel();
      this.pnl_data.SuspendLayout();
      this.tab_bank_transaction_data.SuspendLayout();
      this.tabPage1.SuspendLayout();
      this.tlp_card.SuspendLayout();
      this.tableLayoutPanel1.SuspendLayout();
      this.tlp_check.SuspendLayout();
      this.tableLayoutPanel2.SuspendLayout();
      this.tlp_all.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
      this.tabPage2.SuspendLayout();
      this.panel1.SuspendLayout();
      this.panel2.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.Controls.Add(this.panel2);
      this.pnl_data.Controls.Add(this.panel1);
      this.pnl_data.Size = new System.Drawing.Size(1024, 658);
      this.pnl_data.TabIndex = 0;
      // 
      // label3
      // 
      this.label3.BackColor = System.Drawing.Color.Transparent;
      this.label3.Font = new System.Drawing.Font("Open Sans Semibold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.label3.Location = new System.Drawing.Point(365, 130);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(19, 38);
      this.label3.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.label3.TabIndex = 35;
      this.label3.Text = "/";
      this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // label4
      // 
      this.label4.BackColor = System.Drawing.Color.Transparent;
      this.label4.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.label4.Location = new System.Drawing.Point(6, 77);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(144, 38);
      this.label4.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.label4.TabIndex = 34;
      this.label4.Text = "xC�digo Elector";
      this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // textBox1
      // 
      this.textBox1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.textBox1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.textBox1.BackColor = System.Drawing.Color.White;
      this.textBox1.BorderColor = System.Drawing.Color.Empty;
      this.textBox1.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.textBox1.CornerRadius = 0;
      this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.textBox1.Location = new System.Drawing.Point(156, 76);
      this.textBox1.MaxLength = 20;
      this.textBox1.Multiline = false;
      this.textBox1.Name = "textBox1";
      this.textBox1.PasswordChar = '\0';
      this.textBox1.ReadOnly = false;
      this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.textBox1.SelectedText = "";
      this.textBox1.SelectionLength = 0;
      this.textBox1.SelectionStart = 0;
      this.textBox1.Size = new System.Drawing.Size(510, 38);
      this.textBox1.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.NONE;
      this.textBox1.TabIndex = 2;
      this.textBox1.TabStop = false;
      this.textBox1.Text = "AAAAAAAAAAAAAAAAAAAAAAAAAB";
      this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.textBox1.UseSystemPasswordChar = false;
      this.textBox1.WaterMark = null;
      this.textBox1.WaterMarkColor = System.Drawing.SystemColors.WindowText;
      // 
      // comboBox1
      // 
      this.comboBox1.ArrowColor = System.Drawing.Color.Empty;
      this.comboBox1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.comboBox1.BackColor = System.Drawing.Color.White;
      this.comboBox1.BorderColor = System.Drawing.Color.White;
      this.comboBox1.CornerRadius = 5;
      this.comboBox1.DataSource = null;
      this.comboBox1.DisplayMember = "";
      this.comboBox1.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
      this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.comboBox1.DropDownWidth = 174;
      this.comboBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.comboBox1.FormattingEnabled = true;
      this.comboBox1.IsDroppedDown = false;
      this.comboBox1.ItemHeight = 21;
      this.comboBox1.Location = new System.Drawing.Point(156, 238);
      this.comboBox1.MaxDropDownItems = 8;
      this.comboBox1.Name = "comboBox1";
      this.comboBox1.OnFocusOpenListBox = true;
      this.comboBox1.SelectedIndex = -1;
      this.comboBox1.SelectedItem = null;
      this.comboBox1.SelectedValue = null;
      this.comboBox1.SelectionLength = 0;
      this.comboBox1.SelectionStart = 0;
      this.comboBox1.Size = new System.Drawing.Size(174, 50);
      this.comboBox1.Sorted = false;
      this.comboBox1.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.NONE;
      this.comboBox1.TabIndex = 7;
      this.comboBox1.TabStop = false;
      this.comboBox1.ValueMember = "";
      // 
      // label5
      // 
      this.label5.BackColor = System.Drawing.Color.Transparent;
      this.label5.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.label5.Location = new System.Drawing.Point(6, 238);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(144, 38);
      this.label5.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.label5.TabIndex = 26;
      this.label5.Text = "xEstado Civil";
      this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // label6
      // 
      this.label6.BackColor = System.Drawing.Color.Transparent;
      this.label6.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.label6.Location = new System.Drawing.Point(6, 184);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(144, 38);
      this.label6.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.label6.TabIndex = 24;
      this.label6.Text = "xGenero";
      this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // label7
      // 
      this.label7.BackColor = System.Drawing.Color.Transparent;
      this.label7.Font = new System.Drawing.Font("Open Sans Semibold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.label7.Location = new System.Drawing.Point(248, 130);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(19, 38);
      this.label7.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.label7.TabIndex = 31;
      this.label7.Text = "/";
      this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // label8
      // 
      this.label8.BackColor = System.Drawing.Color.Transparent;
      this.label8.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.label8.Location = new System.Drawing.Point(6, 131);
      this.label8.Name = "label8";
      this.label8.Size = new System.Drawing.Size(144, 38);
      this.label8.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.label8.TabIndex = 25;
      this.label8.Text = "xNacimiento";
      this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // label9
      // 
      this.label9.BackColor = System.Drawing.Color.Transparent;
      this.label9.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.label9.Location = new System.Drawing.Point(6, 23);
      this.label9.Name = "label9";
      this.label9.Size = new System.Drawing.Size(144, 38);
      this.label9.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.label9.TabIndex = 1;
      this.label9.Text = "xName";
      this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // textBox2
      // 
      this.textBox2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.textBox2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.textBox2.BackColor = System.Drawing.Color.White;
      this.textBox2.BorderColor = System.Drawing.Color.Empty;
      this.textBox2.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.textBox2.CornerRadius = 0;
      this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.textBox2.Location = new System.Drawing.Point(156, 22);
      this.textBox2.MaxLength = 50;
      this.textBox2.Multiline = false;
      this.textBox2.Name = "textBox2";
      this.textBox2.PasswordChar = '\0';
      this.textBox2.ReadOnly = false;
      this.textBox2.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.textBox2.SelectedText = "";
      this.textBox2.SelectionLength = 0;
      this.textBox2.SelectionStart = 0;
      this.textBox2.Size = new System.Drawing.Size(510, 38);
      this.textBox2.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.NONE;
      this.textBox2.TabIndex = 1;
      this.textBox2.TabStop = false;
      this.textBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.textBox2.UseSystemPasswordChar = false;
      this.textBox2.WaterMark = null;
      this.textBox2.WaterMarkColor = System.Drawing.SystemColors.WindowText;
      // 
      // tab_bank_transaction_data
      // 
      this.tab_bank_transaction_data.Controls.Add(this.tabPage1);
      this.tab_bank_transaction_data.Controls.Add(this.tabPage2);
      this.tab_bank_transaction_data.DisplayStyle = WSI.Cashier.Controls.TabStyle.Rounded;
      // 
      // 
      // 
      this.tab_bank_transaction_data.DisplayStyleProvider.BorderColor = System.Drawing.SystemColors.ControlDark;
      this.tab_bank_transaction_data.DisplayStyleProvider.BorderColorHot = System.Drawing.SystemColors.ControlDark;
      this.tab_bank_transaction_data.DisplayStyleProvider.BorderColorSelected = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(157)))), ((int)(((byte)(185)))));
      this.tab_bank_transaction_data.DisplayStyleProvider.FocusTrack = true;
      this.tab_bank_transaction_data.DisplayStyleProvider.HotTrack = true;
      this.tab_bank_transaction_data.DisplayStyleProvider.Opacity = 1F;
      this.tab_bank_transaction_data.DisplayStyleProvider.Overlap = 0;
      this.tab_bank_transaction_data.DisplayStyleProvider.Padding = new System.Drawing.Point(6, 3);
      this.tab_bank_transaction_data.DisplayStyleProvider.Radius = 10;
      this.tab_bank_transaction_data.DisplayStyleProvider.TabColor = System.Drawing.SystemColors.ControlDark;
      this.tab_bank_transaction_data.DisplayStyleProvider.TabColorDisabled = System.Drawing.SystemColors.ControlDark;
      this.tab_bank_transaction_data.DisplayStyleProvider.TabColorHeader = System.Drawing.SystemColors.ControlDark;
      this.tab_bank_transaction_data.DisplayStyleProvider.TabColorSelected = System.Drawing.SystemColors.ControlDark;
      this.tab_bank_transaction_data.DisplayStyleProvider.TextColor = System.Drawing.SystemColors.ControlText;
      this.tab_bank_transaction_data.DisplayStyleProvider.TextColorDisabled = System.Drawing.SystemColors.ControlDark;
      this.tab_bank_transaction_data.DisplayStyleProvider.TextColorSelected = System.Drawing.SystemColors.ControlText;
      this.tab_bank_transaction_data.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tab_bank_transaction_data.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.tab_bank_transaction_data.HotTrack = true;
      this.tab_bank_transaction_data.ItemSize = new System.Drawing.Size(100, 40);
      this.tab_bank_transaction_data.Location = new System.Drawing.Point(10, 10);
      this.tab_bank_transaction_data.Name = "tab_bank_transaction_data";
      this.tab_bank_transaction_data.SelectedIndex = 0;
      this.tab_bank_transaction_data.Size = new System.Drawing.Size(1004, 561);
      this.tab_bank_transaction_data.TabIndex = 0;
      this.tab_bank_transaction_data.TabStop = false;
      // 
      // tabPage1
      // 
      this.tabPage1.Controls.Add(this.tlp_card);
      this.tabPage1.Controls.Add(this.tlp_check);
      this.tabPage1.Controls.Add(this.tlp_all);
      this.tabPage1.Controls.Add(this.lbl_card_type);
      this.tabPage1.Controls.Add(this.cb_card_types);
      this.tabPage1.Controls.Add(this.pictureBox1);
      this.tabPage1.Controls.Add(this.ef_total_amount);
      this.tabPage1.Controls.Add(this.lbl_swip_card);
      this.tabPage1.Controls.Add(this.lbl_total_amount);
      this.tabPage1.Location = new System.Drawing.Point(4, 45);
      this.tabPage1.Name = "tabPage1";
      this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
      this.tabPage1.Size = new System.Drawing.Size(996, 512);
      this.tabPage1.TabIndex = 0;
      this.tabPage1.Text = "CardorCheck";
      this.tabPage1.UseVisualStyleBackColor = true;
      // 
      // tlp_card
      // 
      this.tlp_card.ColumnCount = 2;
      this.tlp_card.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.16575F));
      this.tlp_card.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 79.83425F));
      this.tlp_card.Controls.Add(this.tableLayoutPanel1, 1, 2);
      this.tlp_card.Controls.Add(this.lbl_card_number, 0, 0);
      this.tlp_card.Controls.Add(this.lbl_card_holder_name, 0, 1);
      this.tlp_card.Controls.Add(this.ef_holder_name, 1, 1);
      this.tlp_card.Controls.Add(this.lbl_expiration_date, 0, 2);
      this.tlp_card.Controls.Add(this.ef_card_number, 1, 0);
      this.tlp_card.Location = new System.Drawing.Point(6, 92);
      this.tlp_card.Name = "tlp_card";
      this.tlp_card.RowCount = 3;
      this.tlp_card.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.tlp_card.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.tlp_card.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 49F));
      this.tlp_card.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
      this.tlp_card.Size = new System.Drawing.Size(724, 140);
      this.tlp_card.TabIndex = 5;
      // 
      // tableLayoutPanel1
      // 
      this.tableLayoutPanel1.ColumnCount = 4;
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 67F));
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 43.80404F));
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 56.19596F));
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 191F));
      this.tableLayoutPanel1.Controls.Add(this.lbl_exp, 0, 0);
      this.tableLayoutPanel1.Controls.Add(this.ef_mm, 0, 0);
      this.tableLayoutPanel1.Controls.Add(this.chk_manual_entry, 2, 0);
      this.tableLayoutPanel1.Location = new System.Drawing.Point(147, 93);
      this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(1, 3, 3, 3);
      this.tableLayoutPanel1.Name = "tableLayoutPanel1";
      this.tableLayoutPanel1.RowCount = 1;
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.tableLayoutPanel1.Size = new System.Drawing.Size(568, 44);
      this.tableLayoutPanel1.TabIndex = 5;
      // 
      // lbl_exp
      // 
      this.lbl_exp.BackColor = System.Drawing.Color.Transparent;
      this.lbl_exp.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_exp.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_exp.Location = new System.Drawing.Point(70, 1);
      this.lbl_exp.Margin = new System.Windows.Forms.Padding(3, 1, 3, 0);
      this.lbl_exp.Name = "lbl_exp";
      this.lbl_exp.Size = new System.Drawing.Size(79, 42);
      this.lbl_exp.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_exp.TabIndex = 1;
      this.lbl_exp.Text = "(mm/yy)";
      this.lbl_exp.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // ef_mm
      // 
      this.ef_mm.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.ef_mm.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.ef_mm.CornerRadius = 5;
      this.ef_mm.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.ef_mm.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.ef_mm.Location = new System.Drawing.Point(3, 3);
      this.ef_mm.Mask = "00/00";
      this.ef_mm.MaxLength = 32767;
      this.ef_mm.Name = "ef_mm";
      this.ef_mm.ReadOnly = false;
      this.ef_mm.SelectedText = "";
      this.ef_mm.SelectionLength = 0;
      this.ef_mm.SelectionStart = 0;
      this.ef_mm.Size = new System.Drawing.Size(59, 40);
      this.ef_mm.Style = WSI.Cashier.Controls.uc_maskedTextBox.RoundMaskedTextBoxStyle.BASIC;
      this.ef_mm.TabIndex = 0;
      this.ef_mm.TabStop = false;
      this.ef_mm.Text = "  /";
      this.ef_mm.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.ef_mm.TextForeColor = System.Drawing.Color.Empty;
      this.ef_mm.Click += new System.EventHandler(this.ef_mm_Click);
      // 
      // chk_manual_entry
      // 
      this.chk_manual_entry.BackColor = System.Drawing.Color.Transparent;
      this.chk_manual_entry.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
      this.chk_manual_entry.Dock = System.Windows.Forms.DockStyle.Fill;
      this.chk_manual_entry.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.chk_manual_entry.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.chk_manual_entry.Location = new System.Drawing.Point(205, 1);
      this.chk_manual_entry.Margin = new System.Windows.Forms.Padding(3, 1, 3, 0);
      this.chk_manual_entry.MinimumSize = new System.Drawing.Size(186, 30);
      this.chk_manual_entry.Name = "chk_manual_entry";
      this.chk_manual_entry.Padding = new System.Windows.Forms.Padding(5);
      this.chk_manual_entry.Size = new System.Drawing.Size(186, 43);
      this.chk_manual_entry.TabIndex = 2;
      this.chk_manual_entry.Text = "xManualEntry";
      this.chk_manual_entry.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.chk_manual_entry.UseVisualStyleBackColor = true;
      this.chk_manual_entry.CheckedChanged += new System.EventHandler(this.chk_manual_entry_CheckedChanged);
      // 
      // lbl_card_number
      // 
      this.lbl_card_number.BackColor = System.Drawing.Color.Transparent;
      this.lbl_card_number.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_card_number.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_card_number.Location = new System.Drawing.Point(3, 1);
      this.lbl_card_number.Margin = new System.Windows.Forms.Padding(3, 1, 3, 0);
      this.lbl_card_number.Name = "lbl_card_number";
      this.lbl_card_number.Size = new System.Drawing.Size(140, 42);
      this.lbl_card_number.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_card_number.TabIndex = 0;
      this.lbl_card_number.Text = "xCardNumber";
      this.lbl_card_number.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_card_holder_name
      // 
      this.lbl_card_holder_name.BackColor = System.Drawing.Color.Transparent;
      this.lbl_card_holder_name.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_card_holder_name.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_card_holder_name.Location = new System.Drawing.Point(3, 46);
      this.lbl_card_holder_name.Margin = new System.Windows.Forms.Padding(3, 1, 3, 0);
      this.lbl_card_holder_name.Name = "lbl_card_holder_name";
      this.lbl_card_holder_name.Size = new System.Drawing.Size(140, 42);
      this.lbl_card_holder_name.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_card_holder_name.TabIndex = 2;
      this.lbl_card_holder_name.Text = "xHolderName";
      this.lbl_card_holder_name.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // ef_holder_name
      // 
      this.ef_holder_name.AllowSpace = true;
      this.ef_holder_name.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.ef_holder_name.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.ef_holder_name.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.ef_holder_name.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.ef_holder_name.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.ef_holder_name.CornerRadius = 5;
      this.ef_holder_name.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.ef_holder_name.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.ExtendedMode;
      this.ef_holder_name.Location = new System.Drawing.Point(149, 48);
      this.ef_holder_name.MaxLength = 200;
      this.ef_holder_name.Multiline = false;
      this.ef_holder_name.Name = "ef_holder_name";
      this.ef_holder_name.PasswordChar = '\0';
      this.ef_holder_name.ReadOnly = false;
      this.ef_holder_name.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.ef_holder_name.SelectedText = "";
      this.ef_holder_name.SelectionLength = 0;
      this.ef_holder_name.SelectionStart = 0;
      this.ef_holder_name.Size = new System.Drawing.Size(566, 40);
      this.ef_holder_name.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.ef_holder_name.TabIndex = 3;
      this.ef_holder_name.TabStop = false;
      this.ef_holder_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.ef_holder_name.UseSystemPasswordChar = false;
      this.ef_holder_name.WaterMark = null;
      this.ef_holder_name.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // lbl_expiration_date
      // 
      this.lbl_expiration_date.BackColor = System.Drawing.Color.Transparent;
      this.lbl_expiration_date.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_expiration_date.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_expiration_date.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_expiration_date.Location = new System.Drawing.Point(3, 91);
      this.lbl_expiration_date.Margin = new System.Windows.Forms.Padding(3, 1, 3, 0);
      this.lbl_expiration_date.Name = "lbl_expiration_date";
      this.lbl_expiration_date.Size = new System.Drawing.Size(140, 49);
      this.lbl_expiration_date.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_expiration_date.TabIndex = 4;
      this.lbl_expiration_date.Text = "xExpirationDate";
      this.lbl_expiration_date.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // ef_card_number
      // 
      this.ef_card_number.AllowSpace = false;
      this.ef_card_number.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.ef_card_number.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.ef_card_number.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.ef_card_number.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.ef_card_number.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.ef_card_number.CornerRadius = 5;
      this.ef_card_number.FillWithCeros = false;
      this.ef_card_number.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.ef_card_number.Location = new System.Drawing.Point(149, 3);
      this.ef_card_number.MaxLength = 50;
      this.ef_card_number.Multiline = false;
      this.ef_card_number.Name = "ef_card_number";
      this.ef_card_number.PasswordChar = '\0';
      this.ef_card_number.ReadOnly = false;
      this.ef_card_number.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.ef_card_number.SelectedText = "";
      this.ef_card_number.SelectionLength = 0;
      this.ef_card_number.SelectionStart = 0;
      this.ef_card_number.Size = new System.Drawing.Size(566, 40);
      this.ef_card_number.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.ef_card_number.TabIndex = 1;
      this.ef_card_number.TabStop = false;
      this.ef_card_number.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.ef_card_number.UseSystemPasswordChar = false;
      this.ef_card_number.WaterMark = null;
      this.ef_card_number.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // tlp_check
      // 
      this.tlp_check.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.tlp_check.ColumnCount = 2;
      this.tlp_check.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.19231F));
      this.tlp_check.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 79.80769F));
      this.tlp_check.Controls.Add(this.ef_account_number, 1, 3);
      this.tlp_check.Controls.Add(this.ef_check_number, 1, 0);
      this.tlp_check.Controls.Add(this.ef_check_payee, 1, 2);
      this.tlp_check.Controls.Add(this.lbl_check_routing_number, 0, 4);
      this.tlp_check.Controls.Add(this.lbl_check_number, 0, 0);
      this.tlp_check.Controls.Add(this.lbl_check_account_number, 0, 3);
      this.tlp_check.Controls.Add(this.lbl_check_payee, 0, 2);
      this.tlp_check.Controls.Add(this.lbl_check_date, 0, 1);
      this.tlp_check.Controls.Add(this.ef_routing_number, 1, 4);
      this.tlp_check.Controls.Add(this.tableLayoutPanel2, 1, 1);
      this.tlp_check.Location = new System.Drawing.Point(6, 158);
      this.tlp_check.Name = "tlp_check";
      this.tlp_check.RowCount = 5;
      this.tlp_check.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 48.95833F));
      this.tlp_check.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 51.04167F));
      this.tlp_check.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 49F));
      this.tlp_check.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 49F));
      this.tlp_check.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 47F));
      this.tlp_check.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
      this.tlp_check.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
      this.tlp_check.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
      this.tlp_check.Size = new System.Drawing.Size(724, 244);
      this.tlp_check.TabIndex = 6;
      // 
      // ef_account_number
      // 
      this.ef_account_number.AllowSpace = true;
      this.ef_account_number.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.ef_account_number.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.ef_account_number.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.ef_account_number.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.ef_account_number.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.ef_account_number.CornerRadius = 5;
      this.ef_account_number.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.ef_account_number.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.ExtendedMode;
      this.ef_account_number.Location = new System.Drawing.Point(149, 150);
      this.ef_account_number.MaxLength = 50;
      this.ef_account_number.Multiline = false;
      this.ef_account_number.Name = "ef_account_number";
      this.ef_account_number.PasswordChar = '\0';
      this.ef_account_number.ReadOnly = false;
      this.ef_account_number.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.ef_account_number.SelectedText = "";
      this.ef_account_number.SelectionLength = 0;
      this.ef_account_number.SelectionStart = 0;
      this.ef_account_number.Size = new System.Drawing.Size(566, 40);
      this.ef_account_number.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.ef_account_number.TabIndex = 7;
      this.ef_account_number.TabStop = false;
      this.ef_account_number.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.ef_account_number.UseSystemPasswordChar = false;
      this.ef_account_number.WaterMark = null;
      this.ef_account_number.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // ef_check_number
      // 
      this.ef_check_number.AllowSpace = false;
      this.ef_check_number.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.ef_check_number.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.ef_check_number.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.ef_check_number.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.ef_check_number.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.ef_check_number.CornerRadius = 5;
      this.ef_check_number.FillWithCeros = false;
      this.ef_check_number.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.ef_check_number.Location = new System.Drawing.Point(149, 3);
      this.ef_check_number.MaxLength = 50;
      this.ef_check_number.Multiline = false;
      this.ef_check_number.Name = "ef_check_number";
      this.ef_check_number.PasswordChar = '\0';
      this.ef_check_number.ReadOnly = false;
      this.ef_check_number.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.ef_check_number.SelectedText = "";
      this.ef_check_number.SelectionLength = 0;
      this.ef_check_number.SelectionStart = 0;
      this.ef_check_number.Size = new System.Drawing.Size(566, 40);
      this.ef_check_number.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.ef_check_number.TabIndex = 1;
      this.ef_check_number.TabStop = false;
      this.ef_check_number.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.ef_check_number.UseSystemPasswordChar = false;
      this.ef_check_number.WaterMark = null;
      this.ef_check_number.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // ef_check_payee
      // 
      this.ef_check_payee.AllowSpace = true;
      this.ef_check_payee.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.ef_check_payee.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.ef_check_payee.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.ef_check_payee.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.ef_check_payee.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.ef_check_payee.CornerRadius = 5;
      this.ef_check_payee.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.ef_check_payee.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.ExtendedMode;
      this.ef_check_payee.Location = new System.Drawing.Point(149, 101);
      this.ef_check_payee.MaxLength = 200;
      this.ef_check_payee.Multiline = false;
      this.ef_check_payee.Name = "ef_check_payee";
      this.ef_check_payee.PasswordChar = '\0';
      this.ef_check_payee.ReadOnly = false;
      this.ef_check_payee.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.ef_check_payee.SelectedText = "";
      this.ef_check_payee.SelectionLength = 0;
      this.ef_check_payee.SelectionStart = 0;
      this.ef_check_payee.Size = new System.Drawing.Size(566, 40);
      this.ef_check_payee.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.ef_check_payee.TabIndex = 5;
      this.ef_check_payee.TabStop = false;
      this.ef_check_payee.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.ef_check_payee.UseSystemPasswordChar = false;
      this.ef_check_payee.WaterMark = null;
      this.ef_check_payee.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // lbl_check_routing_number
      // 
      this.lbl_check_routing_number.BackColor = System.Drawing.Color.Transparent;
      this.lbl_check_routing_number.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_check_routing_number.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_check_routing_number.Location = new System.Drawing.Point(3, 197);
      this.lbl_check_routing_number.Margin = new System.Windows.Forms.Padding(3, 1, 3, 0);
      this.lbl_check_routing_number.Name = "lbl_check_routing_number";
      this.lbl_check_routing_number.Size = new System.Drawing.Size(140, 42);
      this.lbl_check_routing_number.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_check_routing_number.TabIndex = 8;
      this.lbl_check_routing_number.Text = "xRoutingNumber";
      this.lbl_check_routing_number.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_check_number
      // 
      this.lbl_check_number.BackColor = System.Drawing.Color.Transparent;
      this.lbl_check_number.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_check_number.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_check_number.Location = new System.Drawing.Point(3, 0);
      this.lbl_check_number.Name = "lbl_check_number";
      this.lbl_check_number.Size = new System.Drawing.Size(140, 32);
      this.lbl_check_number.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_check_number.TabIndex = 0;
      this.lbl_check_number.Text = "xCheckNumber";
      this.lbl_check_number.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_check_account_number
      // 
      this.lbl_check_account_number.BackColor = System.Drawing.Color.Transparent;
      this.lbl_check_account_number.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_check_account_number.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_check_account_number.Location = new System.Drawing.Point(3, 148);
      this.lbl_check_account_number.Margin = new System.Windows.Forms.Padding(3, 1, 3, 0);
      this.lbl_check_account_number.Name = "lbl_check_account_number";
      this.lbl_check_account_number.Size = new System.Drawing.Size(140, 42);
      this.lbl_check_account_number.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_check_account_number.TabIndex = 6;
      this.lbl_check_account_number.Text = "xAccountNumber";
      this.lbl_check_account_number.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_check_payee
      // 
      this.lbl_check_payee.BackColor = System.Drawing.Color.Transparent;
      this.lbl_check_payee.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_check_payee.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_check_payee.Location = new System.Drawing.Point(3, 99);
      this.lbl_check_payee.Margin = new System.Windows.Forms.Padding(3, 1, 3, 0);
      this.lbl_check_payee.Name = "lbl_check_payee";
      this.lbl_check_payee.Size = new System.Drawing.Size(140, 42);
      this.lbl_check_payee.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_check_payee.TabIndex = 4;
      this.lbl_check_payee.Text = "xPayee";
      this.lbl_check_payee.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_check_date
      // 
      this.lbl_check_date.BackColor = System.Drawing.Color.Transparent;
      this.lbl_check_date.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_check_date.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_check_date.Location = new System.Drawing.Point(3, 49);
      this.lbl_check_date.Margin = new System.Windows.Forms.Padding(3, 1, 3, 0);
      this.lbl_check_date.Name = "lbl_check_date";
      this.lbl_check_date.Size = new System.Drawing.Size(140, 42);
      this.lbl_check_date.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_check_date.TabIndex = 2;
      this.lbl_check_date.Text = "xDate";
      this.lbl_check_date.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // ef_routing_number
      // 
      this.ef_routing_number.AllowSpace = true;
      this.ef_routing_number.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.ef_routing_number.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.ef_routing_number.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.ef_routing_number.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.ef_routing_number.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.ef_routing_number.CornerRadius = 5;
      this.ef_routing_number.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.ef_routing_number.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.ExtendedMode;
      this.ef_routing_number.Location = new System.Drawing.Point(149, 199);
      this.ef_routing_number.MaxLength = 50;
      this.ef_routing_number.Multiline = false;
      this.ef_routing_number.Name = "ef_routing_number";
      this.ef_routing_number.PasswordChar = '\0';
      this.ef_routing_number.ReadOnly = false;
      this.ef_routing_number.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.ef_routing_number.SelectedText = "";
      this.ef_routing_number.SelectionLength = 0;
      this.ef_routing_number.SelectionStart = 0;
      this.ef_routing_number.Size = new System.Drawing.Size(566, 40);
      this.ef_routing_number.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.ef_routing_number.TabIndex = 9;
      this.ef_routing_number.TabStop = false;
      this.ef_routing_number.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.ef_routing_number.UseSystemPasswordChar = false;
      this.ef_routing_number.WaterMark = null;
      this.ef_routing_number.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // tableLayoutPanel2
      // 
      this.tableLayoutPanel2.ColumnCount = 3;
      this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 67.48466F));
      this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 32.51534F));
      this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 261F));
      this.tableLayoutPanel2.Controls.Add(this.uc_check_date, 0, 0);
      this.tableLayoutPanel2.Controls.Add(this.cb_check_types, 2, 0);
      this.tableLayoutPanel2.Controls.Add(this.lbl_check_type, 1, 0);
      this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tableLayoutPanel2.Location = new System.Drawing.Point(146, 48);
      this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
      this.tableLayoutPanel2.Name = "tableLayoutPanel2";
      this.tableLayoutPanel2.RowCount = 1;
      this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.tableLayoutPanel2.Size = new System.Drawing.Size(578, 50);
      this.tableLayoutPanel2.TabIndex = 3;
      // 
      // uc_check_date
      // 
      this.uc_check_date.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
      this.uc_check_date.DateText = "";
      this.uc_check_date.DateTextFont = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.uc_check_date.DateTextWidth = 0;
      this.uc_check_date.DateValue = new System.DateTime(((long)(0)));
      this.uc_check_date.FormatFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
      this.uc_check_date.FormatWidth = 19;
      this.uc_check_date.Invalid = false;
      this.uc_check_date.Location = new System.Drawing.Point(3, 3);
      this.uc_check_date.Name = "uc_check_date";
      this.uc_check_date.Size = new System.Drawing.Size(207, 41);
      this.uc_check_date.TabIndex = 0;
      this.uc_check_date.ValuesFont = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold);
      // 
      // cb_check_types
      // 
      this.cb_check_types.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_check_types.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.cb_check_types.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.cb_check_types.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.cb_check_types.CornerRadius = 5;
      this.cb_check_types.DataSource = null;
      this.cb_check_types.DisplayMember = "";
      this.cb_check_types.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
      this.cb_check_types.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cb_check_types.DropDownWidth = 244;
      this.cb_check_types.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.cb_check_types.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_check_types.FormattingEnabled = true;
      this.cb_check_types.IsDroppedDown = false;
      this.cb_check_types.ItemHeight = 40;
      this.cb_check_types.Location = new System.Drawing.Point(319, 3);
      this.cb_check_types.MaxDropDownItems = 8;
      this.cb_check_types.Name = "cb_check_types";
      this.cb_check_types.OnFocusOpenListBox = true;
      this.cb_check_types.SelectedIndex = -1;
      this.cb_check_types.SelectedItem = null;
      this.cb_check_types.SelectedValue = null;
      this.cb_check_types.SelectionLength = 0;
      this.cb_check_types.SelectionStart = 0;
      this.cb_check_types.Size = new System.Drawing.Size(244, 40);
      this.cb_check_types.Sorted = false;
      this.cb_check_types.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.BASIC;
      this.cb_check_types.TabIndex = 2;
      this.cb_check_types.TabStop = false;
      this.cb_check_types.ValueMember = "";
      // 
      // lbl_check_type
      // 
      this.lbl_check_type.BackColor = System.Drawing.Color.Transparent;
      this.lbl_check_type.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_check_type.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_check_type.Location = new System.Drawing.Point(216, 1);
      this.lbl_check_type.Margin = new System.Windows.Forms.Padding(3, 1, 3, 0);
      this.lbl_check_type.Name = "lbl_check_type";
      this.lbl_check_type.Size = new System.Drawing.Size(92, 42);
      this.lbl_check_type.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_check_type.TabIndex = 1;
      this.lbl_check_type.Text = "xCheckType";
      this.lbl_check_type.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // tlp_all
      // 
      this.tlp_all.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.tlp_all.ColumnCount = 2;
      this.tlp_all.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.44199F));
      this.tlp_all.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 79.55801F));
      this.tlp_all.Controls.Add(this.lbl_bank_name, 0, 0);
      this.tlp_all.Controls.Add(this.lbl_nationality, 0, 1);
      this.tlp_all.Controls.Add(this.cb_nationality, 1, 1);
      this.tlp_all.Controls.Add(this.ef_bank_name, 1, 0);
      this.tlp_all.Location = new System.Drawing.Point(6, 407);
      this.tlp_all.Name = "tlp_all";
      this.tlp_all.RowCount = 2;
      this.tlp_all.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 52F));
      this.tlp_all.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 48F));
      this.tlp_all.Size = new System.Drawing.Size(724, 98);
      this.tlp_all.TabIndex = 7;
      // 
      // lbl_bank_name
      // 
      this.lbl_bank_name.BackColor = System.Drawing.Color.Transparent;
      this.lbl_bank_name.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_bank_name.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_bank_name.Location = new System.Drawing.Point(3, 1);
      this.lbl_bank_name.Margin = new System.Windows.Forms.Padding(3, 1, 3, 0);
      this.lbl_bank_name.Name = "lbl_bank_name";
      this.lbl_bank_name.Size = new System.Drawing.Size(140, 42);
      this.lbl_bank_name.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_bank_name.TabIndex = 0;
      this.lbl_bank_name.Text = "xBankName";
      this.lbl_bank_name.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_nationality
      // 
      this.lbl_nationality.BackColor = System.Drawing.Color.Transparent;
      this.lbl_nationality.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_nationality.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_nationality.Location = new System.Drawing.Point(3, 51);
      this.lbl_nationality.Margin = new System.Windows.Forms.Padding(3, 1, 3, 0);
      this.lbl_nationality.Name = "lbl_nationality";
      this.lbl_nationality.Size = new System.Drawing.Size(140, 42);
      this.lbl_nationality.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_nationality.TabIndex = 2;
      this.lbl_nationality.Text = "xNationality";
      this.lbl_nationality.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // cb_nationality
      // 
      this.cb_nationality.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_nationality.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.cb_nationality.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.cb_nationality.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.cb_nationality.CornerRadius = 5;
      this.cb_nationality.DataSource = null;
      this.cb_nationality.DisplayMember = "";
      this.cb_nationality.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
      this.cb_nationality.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
      this.cb_nationality.DropDownWidth = 269;
      this.cb_nationality.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.cb_nationality.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_nationality.FormattingEnabled = true;
      this.cb_nationality.FormatValidator = null;
      this.cb_nationality.IsDroppedDown = false;
      this.cb_nationality.ItemHeight = 40;
      this.cb_nationality.Location = new System.Drawing.Point(151, 53);
      this.cb_nationality.MaxDropDownItems = 4;
      this.cb_nationality.Name = "cb_nationality";
      this.cb_nationality.OnFocusOpenListBox = true;
      this.cb_nationality.SelectedIndex = -1;
      this.cb_nationality.SelectedItem = null;
      this.cb_nationality.SelectedValue = null;
      this.cb_nationality.SelectionLength = 0;
      this.cb_nationality.SelectionStart = 0;
      this.cb_nationality.Size = new System.Drawing.Size(269, 40);
      this.cb_nationality.Sorted = false;
      this.cb_nationality.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.BASIC;
      this.cb_nationality.TabIndex = 3;
      this.cb_nationality.TabStop = false;
      this.cb_nationality.ValueMember = "";
      // 
      // ef_bank_name
      // 
      this.ef_bank_name.AllowSpace = true;
      this.ef_bank_name.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.ef_bank_name.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.ef_bank_name.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.ef_bank_name.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.ef_bank_name.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.ef_bank_name.CornerRadius = 5;
      this.ef_bank_name.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.ef_bank_name.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.ExtendedMode;
      this.ef_bank_name.Location = new System.Drawing.Point(151, 3);
      this.ef_bank_name.MaxLength = 50;
      this.ef_bank_name.Multiline = false;
      this.ef_bank_name.Name = "ef_bank_name";
      this.ef_bank_name.PasswordChar = '\0';
      this.ef_bank_name.ReadOnly = false;
      this.ef_bank_name.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.ef_bank_name.SelectedText = "";
      this.ef_bank_name.SelectionLength = 0;
      this.ef_bank_name.SelectionStart = 0;
      this.ef_bank_name.Size = new System.Drawing.Size(564, 40);
      this.ef_bank_name.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.ef_bank_name.TabIndex = 1;
      this.ef_bank_name.TabStop = false;
      this.ef_bank_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.ef_bank_name.UseSystemPasswordChar = false;
      this.ef_bank_name.WaterMark = null;
      this.ef_bank_name.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // lbl_card_type
      // 
      this.lbl_card_type.BackColor = System.Drawing.Color.Transparent;
      this.lbl_card_type.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_card_type.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_card_type.Location = new System.Drawing.Point(326, 48);
      this.lbl_card_type.Margin = new System.Windows.Forms.Padding(3, 1, 3, 0);
      this.lbl_card_type.Name = "lbl_card_type";
      this.lbl_card_type.Size = new System.Drawing.Size(104, 42);
      this.lbl_card_type.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_card_type.TabIndex = 3;
      this.lbl_card_type.Text = "xCardType";
      this.lbl_card_type.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // cb_card_types
      // 
      this.cb_card_types.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_card_types.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.cb_card_types.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.cb_card_types.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.cb_card_types.CornerRadius = 5;
      this.cb_card_types.DataSource = null;
      this.cb_card_types.DisplayMember = "";
      this.cb_card_types.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
      this.cb_card_types.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cb_card_types.DropDownWidth = 110;
      this.cb_card_types.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.cb_card_types.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_card_types.FormattingEnabled = true;
      this.cb_card_types.IsDroppedDown = false;
      this.cb_card_types.ItemHeight = 40;
      this.cb_card_types.Location = new System.Drawing.Point(436, 49);
      this.cb_card_types.MaxDropDownItems = 8;
      this.cb_card_types.Name = "cb_card_types";
      this.cb_card_types.OnFocusOpenListBox = true;
      this.cb_card_types.SelectedIndex = -1;
      this.cb_card_types.SelectedItem = null;
      this.cb_card_types.SelectedValue = null;
      this.cb_card_types.SelectionLength = 0;
      this.cb_card_types.SelectionStart = 0;
      this.cb_card_types.Size = new System.Drawing.Size(110, 40);
      this.cb_card_types.Sorted = false;
      this.cb_card_types.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.BASIC;
      this.cb_card_types.TabIndex = 4;
      this.cb_card_types.TabStop = false;
      this.cb_card_types.ValueMember = "";
      // 
      // pictureBox1
      // 
      this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
      this.pictureBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.pictureBox1.Location = new System.Drawing.Point(12, 5);
      this.pictureBox1.Name = "pictureBox1";
      this.pictureBox1.Size = new System.Drawing.Size(38, 35);
      this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
      this.pictureBox1.TabIndex = 48;
      this.pictureBox1.TabStop = false;
      // 
      // ef_total_amount
      // 
      this.ef_total_amount.AllowSpace = false;
      this.ef_total_amount.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.ef_total_amount.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.ef_total_amount.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.ef_total_amount.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.ef_total_amount.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.ef_total_amount.CornerRadius = 5;
      this.ef_total_amount.FillWithCeros = false;
      this.ef_total_amount.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.ef_total_amount.Location = new System.Drawing.Point(155, 49);
      this.ef_total_amount.MaxLength = 32767;
      this.ef_total_amount.Multiline = false;
      this.ef_total_amount.Name = "ef_total_amount";
      this.ef_total_amount.PasswordChar = '\0';
      this.ef_total_amount.ReadOnly = false;
      this.ef_total_amount.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.ef_total_amount.SelectedText = "";
      this.ef_total_amount.SelectionLength = 0;
      this.ef_total_amount.SelectionStart = 0;
      this.ef_total_amount.Size = new System.Drawing.Size(137, 40);
      this.ef_total_amount.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.ef_total_amount.TabIndex = 2;
      this.ef_total_amount.TabStop = false;
      this.ef_total_amount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      this.ef_total_amount.UseSystemPasswordChar = false;
      this.ef_total_amount.WaterMark = null;
      this.ef_total_amount.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // lbl_swip_card
      // 
      this.lbl_swip_card.BackColor = System.Drawing.Color.Transparent;
      this.lbl_swip_card.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_swip_card.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_swip_card.Location = new System.Drawing.Point(154, 8);
      this.lbl_swip_card.Name = "lbl_swip_card";
      this.lbl_swip_card.Size = new System.Drawing.Size(565, 32);
      this.lbl_swip_card.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_swip_card.TabIndex = 0;
      this.lbl_swip_card.Text = "xSwipcard";
      this.lbl_swip_card.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_total_amount
      // 
      this.lbl_total_amount.BackColor = System.Drawing.Color.Transparent;
      this.lbl_total_amount.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_total_amount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_total_amount.Location = new System.Drawing.Point(9, 48);
      this.lbl_total_amount.Margin = new System.Windows.Forms.Padding(3, 1, 3, 0);
      this.lbl_total_amount.Name = "lbl_total_amount";
      this.lbl_total_amount.Size = new System.Drawing.Size(140, 42);
      this.lbl_total_amount.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_total_amount.TabIndex = 1;
      this.lbl_total_amount.Text = "xTotalAmount";
      this.lbl_total_amount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // tabPage2
      // 
      this.tabPage2.Controls.Add(this.txt_comments);
      this.tabPage2.Location = new System.Drawing.Point(4, 45);
      this.tabPage2.Name = "tabPage2";
      this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
      this.tabPage2.Size = new System.Drawing.Size(72, 0);
      this.tabPage2.TabIndex = 1;
      this.tabPage2.Text = "Comments";
      this.tabPage2.UseVisualStyleBackColor = true;
      // 
      // txt_comments
      // 
      this.txt_comments.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_comments.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_comments.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_comments.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_comments.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_comments.CornerRadius = 5;
      this.txt_comments.Dock = System.Windows.Forms.DockStyle.Fill;
      this.txt_comments.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_comments.Location = new System.Drawing.Point(3, 3);
      this.txt_comments.MaxLength = 256;
      this.txt_comments.Multiline = true;
      this.txt_comments.Name = "txt_comments";
      this.txt_comments.PasswordChar = '\0';
      this.txt_comments.ReadOnly = false;
      this.txt_comments.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
      this.txt_comments.SelectedText = "";
      this.txt_comments.SelectionLength = 0;
      this.txt_comments.SelectionStart = 0;
      this.txt_comments.Size = new System.Drawing.Size(66, 0);
      this.txt_comments.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.MULTILINE;
      this.txt_comments.TabIndex = 2;
      this.txt_comments.TabStop = false;
      this.txt_comments.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_comments.UseSystemPasswordChar = false;
      this.txt_comments.WaterMark = null;
      this.txt_comments.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // btn_keyboard
      // 
      this.btn_keyboard.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(47)))), ((int)(((byte)(50)))));
      this.btn_keyboard.FlatAppearance.BorderSize = 0;
      this.btn_keyboard.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_keyboard.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_keyboard.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_keyboard.Image = ((System.Drawing.Image)(resources.GetObject("btn_keyboard.Image")));
      this.btn_keyboard.IsSelected = false;
      this.btn_keyboard.Location = new System.Drawing.Point(14, 15);
      this.btn_keyboard.Name = "btn_keyboard";
      this.btn_keyboard.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_keyboard.Size = new System.Drawing.Size(70, 60);
      this.btn_keyboard.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.TERTIARY;
      this.btn_keyboard.TabIndex = 0;
      this.btn_keyboard.TabStop = false;
      this.btn_keyboard.UseVisualStyleBackColor = false;
      this.btn_keyboard.Click += new System.EventHandler(this.btn_keyboard_Click);
      // 
      // btn_cancel
      // 
      this.btn_cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_cancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_cancel.FlatAppearance.BorderSize = 0;
      this.btn_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_cancel.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_cancel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_cancel.Image = null;
      this.btn_cancel.IsSelected = false;
      this.btn_cancel.Location = new System.Drawing.Point(686, 12);
      this.btn_cancel.Name = "btn_cancel";
      this.btn_cancel.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_cancel.Size = new System.Drawing.Size(155, 60);
      this.btn_cancel.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_cancel.TabIndex = 2;
      this.btn_cancel.Text = "CANCEL";
      this.btn_cancel.UseVisualStyleBackColor = false;
      this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
      // 
      // btn_ok
      // 
      this.btn_ok.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_ok.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_ok.FlatAppearance.BorderSize = 0;
      this.btn_ok.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_ok.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_ok.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_ok.Image = null;
      this.btn_ok.IsSelected = false;
      this.btn_ok.Location = new System.Drawing.Point(855, 12);
      this.btn_ok.Name = "btn_ok";
      this.btn_ok.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_ok.Size = new System.Drawing.Size(155, 60);
      this.btn_ok.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_ok.TabIndex = 3;
      this.btn_ok.Text = "OK";
      this.btn_ok.UseVisualStyleBackColor = false;
      this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
      // 
      // lbl_msg_blink
      // 
      this.lbl_msg_blink.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.lbl_msg_blink.BackColor = System.Drawing.Color.Transparent;
      this.lbl_msg_blink.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_msg_blink.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.lbl_msg_blink.Location = new System.Drawing.Point(88, 15);
      this.lbl_msg_blink.Name = "lbl_msg_blink";
      this.lbl_msg_blink.Size = new System.Drawing.Size(589, 60);
      this.lbl_msg_blink.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_RED;
      this.lbl_msg_blink.TabIndex = 1;
      this.lbl_msg_blink.Text = "xMESSAGE LINE";
      this.lbl_msg_blink.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.lbl_msg_blink.UseMnemonic = false;
      this.lbl_msg_blink.Visible = false;
      // 
      // panel1
      // 
      this.panel1.Controls.Add(this.btn_keyboard);
      this.panel1.Controls.Add(this.btn_ok);
      this.panel1.Controls.Add(this.lbl_msg_blink);
      this.panel1.Controls.Add(this.btn_cancel);
      this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.panel1.Location = new System.Drawing.Point(0, 571);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(1024, 87);
      this.panel1.TabIndex = 1;
      // 
      // panel2
      // 
      this.panel2.Controls.Add(this.tab_bank_transaction_data);
      this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panel2.Location = new System.Drawing.Point(0, 0);
      this.panel2.Name = "panel2";
      this.panel2.Padding = new System.Windows.Forms.Padding(10, 10, 10, 0);
      this.panel2.Size = new System.Drawing.Size(1024, 571);
      this.panel2.TabIndex = 0;
      // 
      // frm_bank_transaction_data
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.ClientSize = new System.Drawing.Size(1024, 713);
      this.ControlBox = false;
      this.KeyPreview = true;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "frm_bank_transaction_data";
      this.ShowIcon = false;
      this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
      this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
      this.Style = WSI.Cashier.Controls.frm_base.BaseFormStyle.PRINCIPAL;
      this.Text = "frm_payment_order";
      this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frm_bank_transaction_data_FormClosed);
      this.Shown += new System.EventHandler(this.frm_bank_transaction_data_Shown);
      this.pnl_data.ResumeLayout(false);
      this.tab_bank_transaction_data.ResumeLayout(false);
      this.tabPage1.ResumeLayout(false);
      this.tlp_card.ResumeLayout(false);
      this.tableLayoutPanel1.ResumeLayout(false);
      this.tlp_check.ResumeLayout(false);
      this.tableLayoutPanel2.ResumeLayout(false);
      this.tlp_all.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
      this.tabPage2.ResumeLayout(false);
      this.panel1.ResumeLayout(false);
      this.panel2.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion


    private WSI.Cashier.Controls.uc_round_button btn_cancel;
    private WSI.Cashier.Controls.uc_round_button btn_ok;
    private WSI.Cashier.Controls.uc_round_button btn_keyboard;
    private WSI.Cashier.Controls.uc_label lbl_msg_blink;
    private WSI.Cashier.Controls.uc_label label3;
    private WSI.Cashier.Controls.uc_label label4;
    private WSI.Cashier.Controls.uc_round_textbox textBox1;
    private WSI.Cashier.Controls.uc_round_combobox comboBox1;
    private WSI.Cashier.Controls.uc_label label5;
    private WSI.Cashier.Controls.uc_label label6;
    private WSI.Cashier.Controls.uc_label label7;
    private WSI.Cashier.Controls.uc_label label8;
    private WSI.Cashier.Controls.uc_label label9;
    private WSI.Cashier.Controls.uc_round_textbox textBox2;
    private System.Windows.Forms.TableLayoutPanel tlp_check;
    private WSI.Cashier.Controls.uc_label lbl_check_number;
    private WSI.Cashier.Controls.uc_label lbl_card_number;
    private CharacterTextBox ef_holder_name;
    private WSI.Cashier.Controls.uc_label lbl_expiration_date;
    private WSI.Cashier.Controls.uc_label lbl_card_holder_name;
    private NumericTextBox ef_card_number;
    private CharacterTextBox ef_bank_name;
    private WSI.Cashier.Controls.uc_label lbl_check_account_number;
    private WSI.Cashier.Controls.uc_label lbl_check_payee;
    private WSI.Cashier.Controls.uc_label lbl_check_date;
    private CharacterTextBox ef_routing_number;
    private WSI.Cashier.Controls.uc_round_auto_combobox cb_nationality;
    private NumericTextBox ef_total_amount;
    private WSI.Cashier.Controls.uc_label lbl_total_amount;
    private WSI.Cashier.Controls.uc_label lbl_swip_card;
    private CharacterTextBox ef_check_payee;
    private WSI.Cashier.Controls.uc_label lbl_check_routing_number;
    private WSI.Cashier.Controls.uc_label lbl_nationality;
    private WSI.Cashier.Controls.uc_label lbl_bank_name;
    private WSI.Cashier.Controls.uc_checkBox chk_manual_entry;
    private uc_datetime uc_check_date;
    private System.Windows.Forms.TableLayoutPanel tlp_all;
    private System.Windows.Forms.TableLayoutPanel tlp_card;
    private WSI.Cashier.Controls.uc_round_tab_control tab_bank_transaction_data;
    private System.Windows.Forms.TabPage tabPage1;
    private System.Windows.Forms.TabPage tabPage2;
    private WSI.Cashier.Controls.uc_round_textbox txt_comments;
    private WSI.Cashier.Controls.uc_maskedTextBox ef_mm;
    private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    private WSI.Cashier.Controls.uc_label lbl_card_type;
    private WSI.Cashier.Controls.uc_round_combobox cb_card_types;
    private WSI.Cashier.Controls.uc_label lbl_exp;
    private NumericTextBox ef_check_number;
    private CharacterTextBox ef_account_number;
    private System.Windows.Forms.PictureBox pictureBox1;
    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.Panel panel2;
    private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
    private Controls.uc_round_combobox cb_check_types;
    private Controls.uc_label lbl_check_type;
  }
}