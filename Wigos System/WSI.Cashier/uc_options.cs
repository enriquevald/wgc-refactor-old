//------------------------------------------------------------------------------
// Copyright � 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : uc_options.cs
// 
//   DESCRIPTION : Cashier Options
// 
// REVISION HISTORY :
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 26-SEP-2007 XXX    Initial version.
// 13-AUG-2013 DRV    Automatic OSK support.
// 17-JUL-2014 DHA    Added table mode.
// 14-AUG-2014 DCS    Add screen resolution and new OS
// 26-AUG-2014 DHA    Add aditional permission for gambling table mode
// 26-JUN-2015 FAV    Fixed Bug WIG-2487: Hide Events group for Table mode.
// 03-JUL-2015 YNM    Added functionality to disable Scanner options 
// 15-DIC-2015 YNM    PBI 7503: Visits/Reception Trinidad y Tobago: search screen     
// 24-DIC-2015 JCA    Bug 7963:Recepci�n: En cajero f�sico sale cortado el men�
// 15-JUN-2016 ETP    Product Backlog Item 14340: Instalaci�n del sofware para PinPad
// 05-SEP-2016 FAV    Fixed Bug 17128: Error enable PinPad
// 07-SEP-2016 DHA    Product Backlog Item 15912: resize for differents windows size
// 13-SEP-2016 ETP    Bug 17128: Pinpad: Merge corrected.
// 13-FEB-2017 JML    Fixed Bug 24422: Cashier: Incorrect withdrawal and closing with bank card
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using WSI.Common;
using System.Management;
using Microsoft.Win32;
using WSI.PinPad;
using WSI.Common.PinPad;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Diagnostics;
using WSI.PinPad.Banorte.Adapter;

namespace WSI.Cashier
{
  public partial class uc_options : UserControl
  {
    #region Fields

    private frm_container parent;
    private uc_database_cfg db_config;
    private Boolean initialized_static_properties = false;

    #endregion

    public uc_options()
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[UC LOAD] uc_options", Log.Type.Message);

      InitializeComponent();

      db_config = new uc_database_cfg();

      pnl_db_config.Controls.Add(db_config);
      db_config.Location = new Point(0, 36);
      db_config.Anchor = AnchorStyles.Left | AnchorStyles.Top | AnchorStyles.Right;

      EventLastAction.AddEventLastAction(this.Controls);

      if (WindowManager.StartUpMode == WindowManager.StartUpModes.Explorer)
      {
        foreach (String _item in DisplaySettings.GetScreenResolutions())
        {
          cb_screen_resolutions.Items.Add(_item);
        }

        cb_screen_resolutions.SelectedItem = String.Format("{0}x{1}", WindowManager.CashierWindowSize.Width, WindowManager.CashierWindowSize.Height);
      }
      else
      {
        cb_screen_resolutions.Visible = false;
        lbl_screen_resolution.Visible = false;
      }
    }

    public void InitControls(frm_container Parent)
    {
      Boolean _scan_visible;
      Boolean _pinpad_visible;

      InitializeControlResources();

      db_config.InitControls();

      if (initialized_static_properties)
      {
        return;
      }

      initialized_static_properties = true;

      pb_osk_logo.Image = Resources.ResourceImages.black_keyboard;
      pb_logo.Image = WSI.Cashier.Images.Get(Images.CashierImage.Logo);
      pb_options.Image = Resources.ResourceImages.option_header;
      pb_sys_info.Image = Resources.ResourceImages.black_info;
      pb_calibrate.Image = Resources.ResourceImages.calibrate;
      this.BackColor = WSI.Cashier.CashierStyle.Colors.m_colors_dic[CashierStyle.Colors.ENUM_COLORS.HEX_E3E7E9];
      panel1.BackColor = WSI.Cashier.CashierStyle.Colors.m_colors_dic[CashierStyle.Colors.ENUM_COLORS.HEX_E3E7E9];
      panel1.HeaderText = Resource.String("STR_VERSION");

      parent = Parent;

      GetVersionInfo();

      btn_osk_disable.Enabled = !Cashier.HideOsk;
      btn_osk_enable.Enabled = Cashier.HideOsk;

      _scan_visible = GeneralParam.GetBoolean("IDCardScanner", "Enabled");
      groupBox1.Visible = _scan_visible;

      _pinpad_visible = Misc.IsPinPadEnabled();
      gb_pinpad_panel.Visible = _pinpad_visible;

      if (_scan_visible)
      {
        bool _enabled = IDScanner != null && IDScanner.Enabled();
        btn_scan_disable.Enabled = _enabled; //AccountPhotoFunctions.Enabled;
        btn_scan_enable.Enabled = !_enabled; //!AccountPhotoFunctions.Enabled;
      }

      if (_pinpad_visible)
      {
        if (Cashier.PinPadEnabled)
        {
          btn_pinpad_enable.Enabled = false;
          btn_pinpad_disable.Enabled = true;
          btn_pinpad_config.Enabled = true;
        }
        else
        {
          btn_pinpad_enable.Enabled = true;
          btn_pinpad_disable.Enabled = false;
          btn_pinpad_config.Enabled = false;
        }
      }

    }

    private void GetVersionInfo()
    {
      lbl_mem.Text = PhysicalMemory();
      lbl_os_version.Text = SystemVersion();
      lbl_architecture.Text = Architecture();
      lbl_resolution.Text = Resolution();
    }

    private string Architecture()
    {
      return SystemQuery("Win32_ComputerSystem", "Manufacturer") +
             SystemQuery("Win32_ComputerSystem", "Model") + "\n" +
             SystemQuery("Win32_ComputerSystem", "SystemType") + "\n" +
             SystemQuery("Win32_Processor", "CurrentClockSpeed") + " Mhz.";

    }

    private string PhysicalMemory()
    {
      return Int32.Parse(SystemQuery("Win32_OperatingSystem", "TotalVisibleMemorySize")) / 1024 + " MB";
    }

    private string SystemVersion()
    {
      String os_name = SystemQuery("Win32_OperatingSystem", "Version");

      os_name = os_name.Remove(3);

      switch (os_name)
      {
        case "10.":
          os_name = "Windows 10";
          break;
        case "6.3":
          os_name = "Windows 8.1";
          break;
        case "6.2":
          os_name = "Windows 8";
          break;
        case "6.1":
          os_name = "Windows 7";
          break;
        case "6.0":
          os_name = "Windows Vista";
          break;
        case "5.2":
          os_name = "Windows 2003 server";
          break;
        case "5.1":
          os_name = "Windows XP";
          break;
        case "5.0":
          os_name = "Windows 2000";
          break;
        case "4.0":
          os_name = "Windows NT 4.0";
          break;
        default:
          os_name = "Windows9x or lower";
          break;
      }

      return os_name + " " + SystemQuery("Win32_OperatingSystem", "Version");
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Calculate window program resolution 
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Window Resolution (Widt x Heigth)
    //
    //COMMENTS:
    // In window mode windows add 3px in every side between form and window border.
    // The function GetMargin calculate that difference and add in calculated resolution to show the real resolution.
    //
    private string Resolution()
    {
      return (WindowManager.CashierWindowSize.Width + " x " + WindowManager.CashierWindowSize.Height);
    }


    private string SystemQuery(String Dll, String Item)
    {
      string value = "";

      SelectQuery query = new SelectQuery(Dll);

      using (ManagementObjectSearcher searcher = new ManagementObjectSearcher(query))
      {
        foreach (ManagementObject mo in searcher.Get())
        {
          try
          {
            value = Convert.ToString(mo[Item]);
          }
          finally
          {
            mo.Dispose();
          }
        }
      }

      return value;
    }

    private void btn_osk_enable_Click(object sender, EventArgs e)
    {
      Cashier.HideOsk = false;
      btn_osk_enable.Enabled = false;
      btn_osk_disable.Enabled = true;
    }

    private void btn_osk_disable_Click(object sender, EventArgs e)
    {
      Cashier.HideOsk = true;
      btn_osk_enable.Enabled = true;
      btn_osk_disable.Enabled = false;
    }

    private void btn_scan_enable_Click(object sender, EventArgs e)
    {
      //AccountPhotoFunctions.Enable();
      IDScanner.Enable();
      btn_scan_enable.Enabled = false;
      btn_scan_disable.Enabled = true;
    }

    private void btn_scan_disable_Click(object sender, EventArgs e)
    {
      IDScanner.Dissable();
      btn_scan_enable.Enabled = true;
      btn_scan_disable.Enabled = false;
    }

    private void btn_pinpad_enable_Click(object sender, EventArgs e)
    {
      try
      {
        PinPadCashierTerminals _pinpad_cashier_terminals;
        PinPadCommon _pinpad_common = null;
        IPinPad _pinpad = null;
        PinPadCashierTerminal _pinpad_terminal;
        String _error_str;

        if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.PinPad_enable,
                                   ProfilePermissions.TypeOperation.RequestPasswd,
                                   this.ParentForm,
                                   out _error_str))
        {
          return;
        }

        if (!Misc.IsFramework4Installed())
        {
          frm_message.Show(Resource.String("STR_NOT_FRAMEWORK_INSTALLED"), Resource.String("STR_VOUCHER_ERROR_TITLE"), MessageBoxButtons.OK, this.ParentForm);
          return;
        }

        _pinpad_terminal = new PinPadCashierTerminal();
        _pinpad_terminal.Port = PinPadCashierTerminal.PINPAD_PORT.COM9;
        _pinpad_terminal.Type = PinPadType.BANORTE;
        _pinpad_terminal.Enabled = true;

        switch (_pinpad_terminal.Type)
        {
          case PinPadType.BANORTE:

            Misc.RegisterComDll("WSI.PinPad.Banorte.Adapter.dll", "WSI.PinPad.Banorte.Adapter.PinPadBanorteAdapter", true);

            _pinpad = new PinPadBanorte();

            break;
          //case PinPadType.DUMMY:
          //   Created for test.
          //  _pinpad = new PinPadDummy();
          //  break;
          default:
            Log.Error("btn_pinpad_enable_Click PinPadType Not defined: " + _pinpad_terminal.Type);
            break;
        }

        _pinpad_common = new PinPadCommon(_pinpad, _pinpad_terminal);

        if (!_pinpad_common.InstallDriver())
        {
          Log.Warning("The PinPad driver has not been installed. Check your system!");
          return;
        }

        _pinpad_cashier_terminals = new PinPadCashierTerminals();
        _pinpad_cashier_terminals.CashierId = (Int32)Cashier.TerminalId;

        if (!_pinpad_cashier_terminals.DB_CheckSomeEnabledByCashierId())
        {
          btn_pinpad_config_Click(sender, e);
          _pinpad_cashier_terminals.DB_GetDataTableByCashierId();

          if (!_pinpad_cashier_terminals.DB_CheckSomeEnabledByCashierId())
          {
            return;
          }
        }

        btn_pinpad_enable.Enabled = false;
        btn_pinpad_disable.Enabled = true;
        btn_pinpad_config.Enabled = true;

        Cashier.PinPadEnabled = true;

        _pinpad_common.Initialize();
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
    }



    private void btn_pinpad_disable_Click(object sender, EventArgs e)
    {
      // TODO PINPAD
      //if (!Misc.IsFramework4Installed())
      //{
      //  frm_message.Show(Resource.String("STR_NOT_FRAMEWORK_INSTALLED"), Resource.String("STR_VOUCHER_ERROR_TITLE"), MessageBoxButtons.OK, this.ParentForm);
      //  return;
      //}

      btn_pinpad_disable.Enabled = false;
      btn_pinpad_config.Enabled = false;
      btn_pinpad_enable.Enabled = true;

      Cashier.PinPadEnabled = false;
    }

    private void btn_pinpad_config_Click(object sender, EventArgs e)
    {
      frm_pinpad_config pinpad_config = new frm_pinpad_config();
      frm_yesno shader = new frm_yesno();
      PinPadCashierTerminals _pinpad_cashier_terminals;

      shader.Opacity = 0.6f;
      shader.Show();

      pinpad_config.ShowDialog(shader);

      shader.Hide();
      shader.Dispose();

      pinpad_config.Hide();
      pinpad_config.Dispose();

      _pinpad_cashier_terminals = new PinPadCashierTerminals();
      _pinpad_cashier_terminals.CashierId = (Int32)Cashier.TerminalId;

      if (!_pinpad_cashier_terminals.DB_CheckSomeEnabledByCashierId())
      {
        btn_pinpad_enable.Enabled = true;
        btn_pinpad_disable.Enabled = false;
        btn_pinpad_config.Enabled = false;
      }
    }

    private void RestoreParentFocus()
    {
      this.ParentForm.Focus();
      this.ParentForm.BringToFront();
    }

    private void ReloadResources()
    {
      parent.uc_bank1.InitializeControlResources();
      parent.uc_card1.InitializeControlResources();
      parent.uc_mobile_bank1.InitializeControlResources();
      parent.uc_gift_delivery1.InitializeControlResources();

      this.InitializeControlResources();
    }

    public void InitializeControlResources()
    {
      //lbl_options_title.Text.ToUpper();
      //lbl_options_title.Text = Resource.String("STR_OPTIONS_OPTIONS");
      lbl_cashier_version.Text = Resource.String("STR_OPTIONS_CASHIER_VER");
      lbl_os_version_text.Text = Resource.String("STR_OPTIONS_OS_VER");
      lbl_mem_text.Text = Resource.String("STR_OPTIONS_TOTAL_MEM");
      lbl_cpu_info.Text = Resource.String("STR_OPTIONS_CPU_INFO");
      lbl_version_info_txt.Text = "WIGOS CASHIER VERSION " + VersionControl.VersionStr;
      gb_lang_selector.HeaderText = Resource.String("STR_OPTIONS_OSK_TITLE");
      lbl_resolution_text.Text = Resource.String("STR_OPTIONS_RESOLUTION");
      groupBox1.HeaderText = Resource.String("STR_OPTIONS_SCAN_TITLE");
      groupBox2.HeaderText = Resource.String("STR_OPTIONS_SYSTEM_INFORMATION");
      btn_calibrate.Text = Resource.String("STR_OPTIONS_BTN_CALIBRATE");
      gb_calibrate_touch.HeaderText = Resource.String("STR_OPTIONS_GB_CALIBRATE");
      pnl_db_config.HeaderText = Resource.String("STR_DB_CONFIG");
      gb_pinpad_panel.HeaderText = "TPV";

      lbl_screen_resolution.Text = Resource.String("STR_OPTIONS_LBL_WINDOW_SIZE");
      btn_window_mode.Text = Resource.String("STR_OPTIONS_BTN_WINDOW_MODE");
      btn_osk_enable.Text = Resource.String("STR_OPTIONS_BTN_OSK_ENABLE");
      btn_osk_disable.Text = Resource.String("STR_OPTIONS_BTN_OSK_DISABLE");
      btn_table_mode.Text = Resource.String("STR_OPTIONS_BTN_TABLE_MODE");
      btn_scan_enable.Text = Resource.String("STR_OPTIONS_BTN_OSK_ENABLE");
      btn_scan_disable.Text = Resource.String("STR_OPTIONS_BTN_OSK_DISABLE");

      btn_pinpad_enable.Text = Resource.String("STR_OPTIONS_BTN_OSK_ENABLE");
      btn_pinpad_disable.Text = Resource.String("STR_OPTIONS_BTN_OSK_DISABLE");
      btn_pinpad_config.Text = Resource.String("STR_PCD_METERS_CONFIGURATION");

      btn_reception_mode.Text = WindowManager.IsReceptionMode ? Resource.String("STR_OPTIONS_BTN_RECEPTION_MODE_ENABLE") : Resource.String("STR_OPTIONS_BTN_RECEPTION_MODE_DISABLE");
      // REVISAR CON JCA y DDM
      //btn_reception.Text = Resource.String("STR_FRM_CONTAINER_BTN_RECEPTION");
      uc_title.Text = Resource.String("STR_OPTIONS_OPTIONS");


      //WindowManager.StartUpMode = WindowManager.StartUpModes.CashDesk;
      if (WindowManager.StartUpMode == WindowManager.StartUpModes.CashDesk)
      {
        btn_window_mode.Visible = false;

        btn_table_mode.Visible = GamingTableBusinessLogic.IsEnabledGTPlayerTracking();
      }
      else
      {
        btn_window_mode.Visible = true;

        if (!GamingTableBusinessLogic.IsEnabledGTPlayerTracking() && !GeneralParam.GetBoolean("Reception", "Enabled", false))
        {

          btn_table_mode.Visible = false;
          btn_reception_mode.Visible = GeneralParam.GetBoolean("Reception", "Enabled", false);
        }
        else
        {
          if (GamingTableBusinessLogic.IsEnabledGTPlayerTracking() && GeneralParam.GetBoolean("Reception", "Enabled", false))
          {
            btn_table_mode.Visible = true;

            btn_reception_mode.Visible = true;

          }
          else
          {
            if (GamingTableBusinessLogic.IsEnabledGTPlayerTracking())
            {
              btn_table_mode.Visible = true;
              btn_reception_mode.Visible = false;
            }
            else if (GeneralParam.GetBoolean("Reception", "Enabled", false))
            {
              btn_reception_mode.Visible = true;
              btn_table_mode.Visible = false;
            }

          }

        }
      }// if (WindowManager.StartUpMode == WindowManager.StartUpModes.CashDesk) else

      db_config.InitControlResources();
    }

    protected override void OnLoad(EventArgs e)
    {
      base.OnLoad(e);

      SetControlsLocation();
    }

    private void SetControlsLocation()
    {
      List<Point> _buttons_position;
      List<Control> _buttons_visibles;

      Int32 _height;


      _buttons_position = new List<Point>();
      _buttons_visibles = new List<Control>();

      // Set buttons positions
      if (btn_calibrate.Visible)
      {
        _buttons_visibles.Add(btn_calibrate);
      }

      if (btn_window_mode.Visible)
      {
        _buttons_visibles.Add(btn_window_mode);
      }

      if (btn_table_mode.Visible)
      {
        _buttons_visibles.Add(btn_table_mode);
      }

      if (btn_reception_mode.Visible)
      {
        _buttons_visibles.Add(btn_reception_mode);
      }

      // Set positions one line / two lines
      if (_buttons_visibles.Count <= 2)
      {
        _buttons_position.Add(new Point(btn_osk_disable.Left, btn_osk_disable.Top));
        _buttons_position.Add(new Point(btn_osk_enable.Left, btn_osk_enable.Top));
      }
      else
      {
        _buttons_position.Add(new Point(btn_osk_disable.Left, 44));
        _buttons_position.Add(new Point(btn_osk_enable.Left, 44));
        _buttons_position.Add(new Point(btn_osk_disable.Left, 98));
        _buttons_position.Add(new Point(btn_osk_enable.Left, 98));
      }

      for (Int32 _idx = 0; _idx < _buttons_visibles.Count; _idx++)
      {
        _buttons_visibles[_idx].Location = _buttons_position[_idx];
      }

      if (_buttons_visibles.Count <= 2 && !cb_screen_resolutions.Visible)
      {
        gb_calibrate_touch.Height = gb_lang_selector.Height;
      }
      else
      {
        gb_calibrate_touch.Height = 154;
      }

      groupBox1.Location = new Point(gb_calibrate_touch.Location.X, gb_calibrate_touch.Location.Y + gb_calibrate_touch.Height + 6);
      _height = groupBox1.Location.Y + groupBox1.Height - groupBox2.Location.Y;

      if (!groupBox1.Visible)
      {
        gb_pinpad_panel.Size = groupBox1.Size;
        gb_pinpad_panel.Location = groupBox1.Location;
      }
      else
      {
        if (gb_pinpad_panel.Visible)
        {
          gb_pinpad_panel.Location = new Point(gb_pinpad_panel.Location.X, groupBox1.Location.Y);
          _height -= (gb_pinpad_panel.Height + 6);
        }
      }

      groupBox2.Height = _height;

    }

    private void btn_calibrate_Click(object sender, EventArgs e)
    {
      // Check if current user is an authorized user
      String error_str;
      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.OptionsCalibrate, ProfilePermissions.TypeOperation.ShowMsg, this.ParentForm, out error_str))
      {
        return;
      }

      try
      {
        // create the ProcessStartInfo using "cmd" as the program to be run,
        // and "/c " as the parameters.
        // Incidentally, /c tells cmd that we want it to execute the command that follows,
        // and then exit.
        System.Diagnostics.ProcessStartInfo procStartInfo =
            new System.Diagnostics.ProcessStartInfo("cmd", "/c CashDesk_TouchCalib.bat");

        // The following commands are needed to redirect the standard output.
        // This means that it will be redirected to the Process.StandardOutput StreamReader.
        procStartInfo.RedirectStandardOutput = true;
        procStartInfo.UseShellExecute = false;
        // Do not create the black window.
        procStartInfo.CreateNoWindow = true;
        // Now we create a process, assign its ProcessStartInfo and start it
        System.Diagnostics.Process proc = new System.Diagnostics.Process();
        proc.StartInfo = procStartInfo;
        proc.Start();
        // Get the output into a string
        //string result = proc.StandardOutput.ReadToEnd();
      }
      catch (Exception ex)
      {
        // Log the exception
        Log.Exception(ex);
      }

    } // btn_calibrate_Click

    private void btn_window_mode_Click(object sender, EventArgs e)
    {
      String _current_value;
      RegistryKey _cashier;
      RegistryKey _common;

      _cashier = null;
      _common = null;

      try
      {
        _cashier = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\LK_SYSTEM\CASHIER", true);

        if (_cashier == null)
        {
          _cashier = Registry.CurrentUser.CreateSubKey(@"Software\LK_SYSTEM\CASHIER");
          _common = _cashier.CreateSubKey("Common");

          _current_value = "false";
        }
        else
        {
          _common = _cashier.OpenSubKey("Common", true);

          if (_common == null)
          {
            _common = _cashier.CreateSubKey("Common");

            _current_value = "false";
          }
          else
          {
            if (_common.GetValue("WindowMode") != null)
            {
              _current_value = _common.GetValue("WindowMode").ToString();
            }
            else
            {
              _current_value = "false";
            }
          }
        }

        if (_current_value == "true")
        {
          _current_value = "false";

          WindowManager.IsWindowMode = false;

        }
        else
        {
          _current_value = "true";

          WindowManager.IsWindowMode = true;

        }
        // Resize Forms
        parent.UpdateSize();

        _common.SetValue("WindowMode", _current_value, RegistryValueKind.String);

      }
      finally
      {

        if (_common != null) _common.Close();
        if (_cashier != null) _cashier.Close();

      }
    }

    private void btn_table_mode_Click(object sender, EventArgs e)
    {
      String _current_value;
      RegistryKey _cashier;
      RegistryKey _common;
      String _error_str;

      _cashier = null;
      _common = null;

      if (ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.CashierGamblingTableMode,
                                         ProfilePermissions.TypeOperation.RequestPasswd,
                                         this.ParentForm,
                                         out _error_str))
      {
        try
        {
          _cashier = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\LK_SYSTEM\CASHIER", true);

          if (_cashier == null)
          {
            _cashier = Registry.CurrentUser.CreateSubKey(@"Software\LK_SYSTEM\CASHIER");
            _common = _cashier.CreateSubKey("Common");

            _current_value = "false";
          }
          else
          {
            _common = _cashier.OpenSubKey("Common", true);

            if (_common == null)
            {
              _common = _cashier.CreateSubKey("Common");

              _current_value = "false";
            }
            else
            {
              if (_common.GetValue("TableMode") != null)
              {
                _current_value = _common.GetValue("TableMode").ToString();
              }
              else
              {
                _current_value = "false";
              }
            }
          }

          if (_current_value == "true")
          {
            _current_value = "false";

          }
          else
          {
            _current_value = "true";
          }

          WindowManager.IsTableMode = Boolean.Parse(_current_value);
          _common.SetValue("TableMode", _current_value, RegistryValueKind.String);

          // Resize Forms
          parent.UpdateSize();

          Control[] _control = parent.Controls.Find("pnl_menu_table_mode", true);

          if (_control.Length > 0)
          {
            _control[0].Visible = Boolean.Parse(_current_value);
            _control[0].BringToFront();
          }

          _control = parent.Controls.Find("splitContainer1", true);

          if (_control.Length > 0)
          {
            ((SplitContainer)_control[0]).Panel1Collapsed = Boolean.Parse(_current_value);
          }

          //_control = parent.Controls.Find("gb_events", true);

          //if (_control.Length > 0)
          //{
          //  _control[0].Visible = !Boolean.Parse(_current_value);
          //}

        }
        finally
        {

          if (_common != null) _common.Close();
          if (_cashier != null) _cashier.Close();

        }
      }
    }

    private void uc_options_SizeChanged(object sender, EventArgs e)
    {
      lbl_resolution.Text = Resolution();
    }

    private void btn_reception_mode_Click(object sender, EventArgs e)
    {
      String _current_value;
      RegistryKey _cashier;
      RegistryKey _common;
      Boolean _reception_enabled;
      String _error_str;

      _cashier = null;
      _common = null;

      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.CustomersReceptionSearch,
                                 ProfilePermissions.TypeOperation.RequestPasswd,
                                 this.ParentForm,
                                 out _error_str))
      {
        return;
      }
      _reception_enabled = GeneralParam.GetBoolean("Reception", "Enabled", false);

      if (_reception_enabled)
      {
        try
        {
          _cashier = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\LK_SYSTEM\CASHIER", true);

          if (_cashier == null)
          {
            _cashier = Registry.CurrentUser.CreateSubKey(@"Software\LK_SYSTEM\CASHIER");
            _common = _cashier.CreateSubKey("Common");

            _current_value = "false";
          }
          else
          {
            _common = _cashier.OpenSubKey("Common", true);

            if (_common == null)
            {
              _common = _cashier.CreateSubKey("Common");

              _current_value = "false";
            }
            else
            {
              if (_common.GetValue("ReceptionMode") != null)
              {
                _current_value = _common.GetValue("ReceptionMode").ToString();
              }
              else
              {
                _current_value = "false";
              }
            }
          }

          if (_current_value == "true")
          {
            _current_value = "false";

          }
          else
          {
            _current_value = "true";
          }

          WindowManager.IsReceptionMode = Boolean.Parse(_current_value);
          _common.SetValue("ReceptionMode", _current_value, RegistryValueKind.String);
          _common.SetValue("EnableReceptionBtn", "true", RegistryValueKind.String);

          btn_reception_mode.Text = WindowManager.IsReceptionMode ? Resource.String("STR_OPTIONS_BTN_RECEPTION_MODE_ENABLE") : Resource.String("STR_OPTIONS_BTN_RECEPTION_MODE_DISABLE");
          if (WindowManager.IsReceptionMode) StartReceptionMode();
        }
        finally
        {

          if (_common != null) _common.Close();
          if (_cashier != null) _cashier.Close();

        }
      }
    }

    private void StartReceptionMode()
    {
      frm_yesno shader = new frm_yesno();
      shader.Opacity = 0.6;
      shader.Show();

      FrmSearchCustomersReception _form_reception = new FrmSearchCustomersReception(null, parent.uc_bank1);
      if (WindowManager.StartUpMode == WindowManager.StartUpModes.Explorer)
      {
        _form_reception.Location = new Point(WindowManager.GetFormCenterLocation(shader).X - (_form_reception.Width / 2), shader.Location.Y + 2);
      }

      try
      {
        _form_reception.ShowDialog(shader);
      }
      finally
      {
        _form_reception.Hide();
        _form_reception.Dispose();
        shader.Hide();
        shader.Dispose();
      }

    }

    public IDScanner.IDScanner IDScanner { get; set; }

    private void cb_resolutions_SelectedIndexChanged(object sender, EventArgs e)
    {
      String[] _size;
      Int32 _width;
      Int32 _height;
      String _current_value;
      RegistryKey _cashier;
      RegistryKey _common;

      _cashier = null;
      _common = null;
      _width = 0;
      _height = 0;

      if (cb_screen_resolutions.SelectedItem != null)
      {
        try
        {
          _current_value = cb_screen_resolutions.SelectedItem.ToString();

          _size = cb_screen_resolutions.SelectedItem.ToString().Split('x');
          _width = Int32.Parse(_size[0]);
          _height = Int32.Parse(_size[1]);

          _cashier = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\LK_SYSTEM\CASHIER", true);

          if (_cashier == null)
          {
            _cashier = Registry.CurrentUser.CreateSubKey(@"Software\LK_SYSTEM\CASHIER");
            _common = _cashier.CreateSubKey("Common");
          }
          else
          {
            _common = _cashier.OpenSubKey("Common", true);

            if (_common == null)
            {
              _common = _cashier.CreateSubKey("Common");
            }
          }

          _common.SetValue("CashierWindow", cb_screen_resolutions.SelectedItem.ToString(), RegistryValueKind.String);

          WindowManager.CashierWindowSize = new Size(_width, _height);

          // Resize Forms
          if(parent!=null)
          parent.UpdateSize();

        }
        finally
        {

          if (_common != null) _common.Close();
          if (_cashier != null) _cashier.Close();

        }
      }
    }
  }
}
