namespace WSI.Cashier
{
  partial class uc_gt_player_in_session
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(uc_gt_player_in_session));
      this.timerClear = new System.Windows.Forms.Timer(this.components);
      this.timerRefresh = new System.Windows.Forms.Timer(this.components);
      this.gb_played_time = new WSI.Cashier.Controls.uc_round_panel();
      this.lbl_play_date_value = new WSI.Cashier.Controls.uc_label();
      this.lbl_play_date_name = new WSI.Cashier.Controls.uc_label();
      this.lbl_play_time_name = new WSI.Cashier.Controls.uc_label();
      this.lbl_play_time_value = new WSI.Cashier.Controls.uc_label();
      this.gb_account = new WSI.Cashier.Controls.uc_round_panel();
      this.pb_seat_img = new System.Windows.Forms.PictureBox();
      this.lbl_holder_name = new WSI.Cashier.Controls.uc_label();
      this.lbl_points_balance = new WSI.Cashier.Controls.uc_label();
      this.lbl_track_number = new WSI.Cashier.Controls.uc_label();
      this.lbl_account_id_name = new WSI.Cashier.Controls.uc_label();
      this.lbl_trackdata = new WSI.Cashier.Controls.uc_label();
      this.lbl_account_id_value = new WSI.Cashier.Controls.uc_label();
      this.lbl_level = new WSI.Cashier.Controls.uc_label();
      this.lbl_plays_value = new WSI.Cashier.Controls.uc_label();
      this.lbl_speed = new WSI.Cashier.Controls.uc_label();
      this.lbl_plays_name = new WSI.Cashier.Controls.uc_label();
      this.lbl_points_balance_title = new WSI.Cashier.Controls.uc_label();
      this.lbl_skill = new WSI.Cashier.Controls.uc_label();
      this.lbl_level_title = new WSI.Cashier.Controls.uc_label();
      this.cb_speed = new WSI.Cashier.Controls.uc_round_combobox();
      this.pb_user = new System.Windows.Forms.PictureBox();
      this.cb_skill = new WSI.Cashier.Controls.uc_round_combobox();
      this.lbl_action_description = new WSI.Cashier.Controls.uc_label();
      this.gb_away_time = new WSI.Cashier.Controls.uc_round_panel();
      this.lbl_pause_time_value_total = new WSI.Cashier.Controls.uc_label();
      this.lbl_pause_date_value = new WSI.Cashier.Controls.uc_label();
      this.lbl_pause_time_value_actual = new WSI.Cashier.Controls.uc_label();
      this.lbl_pause_date_name = new WSI.Cashier.Controls.uc_label();
      this.lbl_pause_time_name_actual = new WSI.Cashier.Controls.uc_label();
      this.lbl_pause_time_name_total = new WSI.Cashier.Controls.uc_label();
      this.gb_chips = new WSI.Cashier.Controls.uc_round_panel();
      this.btn_sell_chips = new WSI.Cashier.Controls.uc_round_button();
      this.tb_netwin = new WSI.Cashier.Controls.uc_label();
      this.lbl_netwin = new WSI.Cashier.Controls.uc_label();
      this.lbl_total_chips_sell = new WSI.Cashier.Controls.uc_label();
      this.lbl_chips_sale_accumulate = new WSI.Cashier.Controls.uc_label();
      this.lbl_chips_in = new WSI.Cashier.Controls.uc_label();
      this.tb_chips_in = new WSI.Cashier.Controls.uc_round_textbox();
      this.lbl_chips_out = new WSI.Cashier.Controls.uc_label();
      this.tb_chips_out = new WSI.Cashier.Controls.uc_round_textbox();
      this.gb_points = new WSI.Cashier.Controls.uc_round_panel();
      this.lbl_points_won = new WSI.Cashier.Controls.uc_label();
      this.tb_points_won = new WSI.Cashier.Controls.uc_round_textbox();
      this.lbl_points_won_calculate = new WSI.Cashier.Controls.uc_label();
      this.lbl_calculate_points_won = new WSI.Cashier.Controls.uc_label();
      this.gb_bet = new WSI.Cashier.Controls.uc_round_panel();
      this.tb_max_played = new WSI.Cashier.Controls.uc_label();
      this.tb_min_played = new WSI.Cashier.Controls.uc_label();
      this.lbl_min_played = new WSI.Cashier.Controls.uc_label();
      this.cb_player_iso_code = new WSI.Cashier.Controls.uc_round_combobox();
      this.tb_total_bet = new WSI.Cashier.Controls.uc_label();
      this.lbl_average_bet = new WSI.Cashier.Controls.uc_label();
      this.lbl_average_bet_value = new WSI.Cashier.Controls.uc_label();
      this.lbl_current_bet = new WSI.Cashier.Controls.uc_label();
      this.lbl_max_played = new WSI.Cashier.Controls.uc_label();
      this.tb_current_bet = new WSI.Cashier.Controls.uc_round_textbox();
      this.lbl_total_bet = new WSI.Cashier.Controls.uc_label();
      this.btn_swap_seat = new WSI.Cashier.Controls.uc_round_button();
      this.btn_play_pause = new WSI.Cashier.Controls.uc_round_button();
      this.btn_close = new WSI.Cashier.Controls.uc_round_button();
      this.gb_played_time.SuspendLayout();
      this.gb_account.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pb_seat_img)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.pb_user)).BeginInit();
      this.gb_away_time.SuspendLayout();
      this.gb_chips.SuspendLayout();
      this.gb_points.SuspendLayout();
      this.gb_bet.SuspendLayout();
      this.SuspendLayout();
      // 
      // timerClear
      // 
      this.timerClear.Interval = 500;
      this.timerClear.Tick += new System.EventHandler(this.timerClear_Tick);
      // 
      // timerRefresh
      // 
      this.timerRefresh.Interval = 3000;
      this.timerRefresh.Tick += new System.EventHandler(this.timerRefresh_Tick);
      // 
      // gb_played_time
      // 
      this.gb_played_time.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.gb_played_time.BackColor = System.Drawing.Color.Transparent;
      this.gb_played_time.BorderColor = System.Drawing.Color.Empty;
      this.gb_played_time.Controls.Add(this.lbl_play_date_value);
      this.gb_played_time.Controls.Add(this.lbl_play_date_name);
      this.gb_played_time.Controls.Add(this.lbl_play_time_name);
      this.gb_played_time.Controls.Add(this.lbl_play_time_value);
      this.gb_played_time.CornerRadius = 10;
      this.gb_played_time.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_played_time.HeaderBackColor = System.Drawing.Color.Empty;
      this.gb_played_time.HeaderForeColor = System.Drawing.Color.Empty;
      this.gb_played_time.HeaderHeight = 0;
      this.gb_played_time.HeaderSubText = null;
      this.gb_played_time.HeaderText = null;
      this.gb_played_time.Location = new System.Drawing.Point(325, 114);
      this.gb_played_time.Name = "gb_played_time";
      this.gb_played_time.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_played_time.Size = new System.Drawing.Size(322, 20);
      this.gb_played_time.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.WITHOUT_HEAD;
      this.gb_played_time.TabIndex = 162;
      // 
      // lbl_play_date_value
      // 
      this.lbl_play_date_value.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_play_date_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_play_date_value.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_play_date_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_play_date_value.Location = new System.Drawing.Point(52, 2);
      this.lbl_play_date_value.Name = "lbl_play_date_value";
      this.lbl_play_date_value.Size = new System.Drawing.Size(69, 17);
      this.lbl_play_date_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_play_date_value.TabIndex = 18;
      this.lbl_play_date_value.Text = "hh:mm";
      this.lbl_play_date_value.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_play_date_name
      // 
      this.lbl_play_date_name.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_play_date_name.BackColor = System.Drawing.Color.Transparent;
      this.lbl_play_date_name.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_play_date_name.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_play_date_name.Location = new System.Drawing.Point(2, 2);
      this.lbl_play_date_name.Name = "lbl_play_date_name";
      this.lbl_play_date_name.Size = new System.Drawing.Size(52, 17);
      this.lbl_play_date_name.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLUE;
      this.lbl_play_date_name.TabIndex = 17;
      this.lbl_play_date_name.Text = "xPlay:";
      this.lbl_play_date_name.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_play_time_name
      // 
      this.lbl_play_time_name.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_play_time_name.BackColor = System.Drawing.Color.Transparent;
      this.lbl_play_time_name.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_play_time_name.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_play_time_name.Location = new System.Drawing.Point(162, 2);
      this.lbl_play_time_name.Name = "lbl_play_time_name";
      this.lbl_play_time_name.Size = new System.Drawing.Size(95, 17);
      this.lbl_play_time_name.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLUE;
      this.lbl_play_time_name.TabIndex = 19;
      this.lbl_play_time_name.Text = "xPlay time:";
      this.lbl_play_time_name.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_play_time_value
      // 
      this.lbl_play_time_value.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_play_time_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_play_time_value.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_play_time_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_play_time_value.Location = new System.Drawing.Point(256, 2);
      this.lbl_play_time_value.Name = "lbl_play_time_value";
      this.lbl_play_time_value.Size = new System.Drawing.Size(62, 17);
      this.lbl_play_time_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_play_time_value.TabIndex = 20;
      this.lbl_play_time_value.Text = "0d 00:00";
      this.lbl_play_time_value.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // gb_account
      // 
      this.gb_account.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.gb_account.BackColor = System.Drawing.Color.Transparent;
      this.gb_account.BorderColor = System.Drawing.Color.Empty;
      this.gb_account.Controls.Add(this.pb_seat_img);
      this.gb_account.Controls.Add(this.lbl_holder_name);
      this.gb_account.Controls.Add(this.lbl_points_balance);
      this.gb_account.Controls.Add(this.lbl_track_number);
      this.gb_account.Controls.Add(this.lbl_account_id_name);
      this.gb_account.Controls.Add(this.lbl_trackdata);
      this.gb_account.Controls.Add(this.lbl_account_id_value);
      this.gb_account.Controls.Add(this.lbl_level);
      this.gb_account.Controls.Add(this.lbl_plays_value);
      this.gb_account.Controls.Add(this.lbl_speed);
      this.gb_account.Controls.Add(this.lbl_plays_name);
      this.gb_account.Controls.Add(this.lbl_points_balance_title);
      this.gb_account.Controls.Add(this.lbl_skill);
      this.gb_account.Controls.Add(this.lbl_level_title);
      this.gb_account.Controls.Add(this.cb_speed);
      this.gb_account.Controls.Add(this.pb_user);
      this.gb_account.Controls.Add(this.cb_skill);
      this.gb_account.CornerRadius = 10;
      this.gb_account.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_account.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gb_account.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_account.HeaderHeight = 18;
      this.gb_account.HeaderSubText = null;
      this.gb_account.HeaderText = null;
      this.gb_account.Location = new System.Drawing.Point(0, 6);
      this.gb_account.Name = "gb_account";
      this.gb_account.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_account.Size = new System.Drawing.Size(319, 201);
      this.gb_account.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.MIN_HEAD;
      this.gb_account.TabIndex = 161;
      // 
      // pb_seat_img
      // 
      this.pb_seat_img.InitialImage = ((System.Drawing.Image)(resources.GetObject("pb_seat_img.InitialImage")));
      this.pb_seat_img.Location = new System.Drawing.Point(1, 24);
      this.pb_seat_img.Name = "pb_seat_img";
      this.pb_seat_img.Size = new System.Drawing.Size(48, 46);
      this.pb_seat_img.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
      this.pb_seat_img.TabIndex = 161;
      this.pb_seat_img.TabStop = false;
      // 
      // lbl_holder_name
      // 
      this.lbl_holder_name.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_holder_name.BackColor = System.Drawing.Color.Transparent;
      this.lbl_holder_name.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_holder_name.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_holder_name.Location = new System.Drawing.Point(54, 19);
      this.lbl_holder_name.Name = "lbl_holder_name";
      this.lbl_holder_name.Size = new System.Drawing.Size(259, 20);
      this.lbl_holder_name.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_holder_name.TabIndex = 0;
      this.lbl_holder_name.Text = "TTTTTTTTTTTTT";
      this.lbl_holder_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.lbl_holder_name.UseMnemonic = false;
      // 
      // lbl_points_balance
      // 
      this.lbl_points_balance.AutoSize = true;
      this.lbl_points_balance.BackColor = System.Drawing.Color.Transparent;
      this.lbl_points_balance.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_points_balance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_points_balance.Location = new System.Drawing.Point(121, 103);
      this.lbl_points_balance.Name = "lbl_points_balance";
      this.lbl_points_balance.Size = new System.Drawing.Size(54, 17);
      this.lbl_points_balance.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_points_balance.TabIndex = 8;
      this.lbl_points_balance.Text = "0 points";
      this.lbl_points_balance.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_track_number
      // 
      this.lbl_track_number.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_track_number.BackColor = System.Drawing.Color.Transparent;
      this.lbl_track_number.Font = new System.Drawing.Font("Open Sans Semibold", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_track_number.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_track_number.Location = new System.Drawing.Point(121, 63);
      this.lbl_track_number.Name = "lbl_track_number";
      this.lbl_track_number.Size = new System.Drawing.Size(197, 17);
      this.lbl_track_number.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_10PX_BLACK;
      this.lbl_track_number.TabIndex = 4;
      this.lbl_track_number.Text = "88888888888888888889";
      this.lbl_track_number.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_account_id_name
      // 
      this.lbl_account_id_name.BackColor = System.Drawing.Color.Transparent;
      this.lbl_account_id_name.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_account_id_name.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_account_id_name.Location = new System.Drawing.Point(51, 42);
      this.lbl_account_id_name.Name = "lbl_account_id_name";
      this.lbl_account_id_name.Size = new System.Drawing.Size(70, 17);
      this.lbl_account_id_name.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLUE;
      this.lbl_account_id_name.TabIndex = 1;
      this.lbl_account_id_name.Text = "xAccount:";
      this.lbl_account_id_name.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_trackdata
      // 
      this.lbl_trackdata.BackColor = System.Drawing.Color.Transparent;
      this.lbl_trackdata.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_trackdata.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_trackdata.Location = new System.Drawing.Point(51, 63);
      this.lbl_trackdata.Name = "lbl_trackdata";
      this.lbl_trackdata.Size = new System.Drawing.Size(70, 17);
      this.lbl_trackdata.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLUE;
      this.lbl_trackdata.TabIndex = 3;
      this.lbl_trackdata.Text = "xTarjeta:";
      this.lbl_trackdata.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_account_id_value
      // 
      this.lbl_account_id_value.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_account_id_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_account_id_value.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_account_id_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_account_id_value.Location = new System.Drawing.Point(121, 42);
      this.lbl_account_id_value.Name = "lbl_account_id_value";
      this.lbl_account_id_value.Size = new System.Drawing.Size(179, 17);
      this.lbl_account_id_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_account_id_value.TabIndex = 2;
      this.lbl_account_id_value.Text = "012345";
      this.lbl_account_id_value.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_level
      // 
      this.lbl_level.AutoSize = true;
      this.lbl_level.BackColor = System.Drawing.Color.Transparent;
      this.lbl_level.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_level.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_level.Location = new System.Drawing.Point(121, 84);
      this.lbl_level.Name = "lbl_level";
      this.lbl_level.Size = new System.Drawing.Size(46, 17);
      this.lbl_level.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_level.TabIndex = 6;
      this.lbl_level.Text = "xSilver";
      this.lbl_level.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_plays_value
      // 
      this.lbl_plays_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_plays_value.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_plays_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_plays_value.Location = new System.Drawing.Point(121, 181);
      this.lbl_plays_value.Name = "lbl_plays_value";
      this.lbl_plays_value.Size = new System.Drawing.Size(106, 17);
      this.lbl_plays_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_plays_value.TabIndex = 14;
      this.lbl_plays_value.Text = "0";
      this.lbl_plays_value.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_speed
      // 
      this.lbl_speed.BackColor = System.Drawing.Color.Transparent;
      this.lbl_speed.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_speed.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_speed.Location = new System.Drawing.Point(51, 127);
      this.lbl_speed.Name = "lbl_speed";
      this.lbl_speed.Size = new System.Drawing.Size(70, 17);
      this.lbl_speed.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLUE;
      this.lbl_speed.TabIndex = 9;
      this.lbl_speed.Text = "xSpeed:";
      this.lbl_speed.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_plays_name
      // 
      this.lbl_plays_name.BackColor = System.Drawing.Color.Transparent;
      this.lbl_plays_name.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_plays_name.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_plays_name.Location = new System.Drawing.Point(51, 181);
      this.lbl_plays_name.Name = "lbl_plays_name";
      this.lbl_plays_name.Size = new System.Drawing.Size(70, 17);
      this.lbl_plays_name.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLUE;
      this.lbl_plays_name.TabIndex = 13;
      this.lbl_plays_name.Text = "xPlays:";
      this.lbl_plays_name.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_points_balance_title
      // 
      this.lbl_points_balance_title.BackColor = System.Drawing.Color.Transparent;
      this.lbl_points_balance_title.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_points_balance_title.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_points_balance_title.Location = new System.Drawing.Point(51, 103);
      this.lbl_points_balance_title.Name = "lbl_points_balance_title";
      this.lbl_points_balance_title.Size = new System.Drawing.Size(70, 17);
      this.lbl_points_balance_title.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLUE;
      this.lbl_points_balance_title.TabIndex = 7;
      this.lbl_points_balance_title.Text = "xBalance:";
      this.lbl_points_balance_title.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_skill
      // 
      this.lbl_skill.BackColor = System.Drawing.Color.Transparent;
      this.lbl_skill.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_skill.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_skill.Location = new System.Drawing.Point(51, 156);
      this.lbl_skill.Name = "lbl_skill";
      this.lbl_skill.Size = new System.Drawing.Size(70, 17);
      this.lbl_skill.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLUE;
      this.lbl_skill.TabIndex = 11;
      this.lbl_skill.Text = "xSkill:";
      this.lbl_skill.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_level_title
      // 
      this.lbl_level_title.BackColor = System.Drawing.Color.Transparent;
      this.lbl_level_title.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_level_title.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_level_title.Location = new System.Drawing.Point(51, 84);
      this.lbl_level_title.Name = "lbl_level_title";
      this.lbl_level_title.Size = new System.Drawing.Size(70, 17);
      this.lbl_level_title.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLUE;
      this.lbl_level_title.TabIndex = 5;
      this.lbl_level_title.Text = "xLevel:";
      this.lbl_level_title.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // cb_speed
      // 
      this.cb_speed.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_speed.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.cb_speed.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.cb_speed.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.cb_speed.CornerRadius = 5;
      this.cb_speed.DataSource = null;
      this.cb_speed.DisplayMember = "";
      this.cb_speed.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
      this.cb_speed.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cb_speed.DropDownWidth = 104;
      this.cb_speed.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.cb_speed.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_speed.FormattingEnabled = true;
      this.cb_speed.IsDroppedDown = false;
      this.cb_speed.ItemHeight = 30;
      this.cb_speed.Location = new System.Drawing.Point(123, 123);
      this.cb_speed.MaxDropDownItems = 8;
      this.cb_speed.Name = "cb_speed";
      this.cb_speed.OnFocusOpenListBox = true;
      this.cb_speed.SelectedIndex = -1;
      this.cb_speed.SelectedItem = null;
      this.cb_speed.SelectedValue = null;
      this.cb_speed.SelectionLength = 0;
      this.cb_speed.SelectionStart = 0;
      this.cb_speed.Size = new System.Drawing.Size(104, 24);
      this.cb_speed.Sorted = false;
      this.cb_speed.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.CUSTOM;
      this.cb_speed.TabIndex = 10;
      this.cb_speed.TabStop = false;
      this.cb_speed.ValueMember = "";
      this.cb_speed.SelectedIndexChanged += new System.EventHandler(this.cb_SelectedIndexChanged);
      // 
      // pb_user
      // 
      this.pb_user.Image = global::WSI.Cashier.Properties.Resources.anonymous_user;
      this.pb_user.InitialImage = ((System.Drawing.Image)(resources.GetObject("pb_user.InitialImage")));
      this.pb_user.Location = new System.Drawing.Point(1, 79);
      this.pb_user.Name = "pb_user";
      this.pb_user.Size = new System.Drawing.Size(48, 46);
      this.pb_user.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
      this.pb_user.TabIndex = 160;
      this.pb_user.TabStop = false;
      this.pb_user.Click += new System.EventHandler(this.pb_user_Click);
      // 
      // cb_skill
      // 
      this.cb_skill.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_skill.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.cb_skill.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.cb_skill.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.cb_skill.CornerRadius = 5;
      this.cb_skill.DataSource = null;
      this.cb_skill.DisplayMember = "";
      this.cb_skill.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
      this.cb_skill.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cb_skill.DropDownWidth = 104;
      this.cb_skill.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.cb_skill.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_skill.FormattingEnabled = true;
      this.cb_skill.IsDroppedDown = false;
      this.cb_skill.ItemHeight = 30;
      this.cb_skill.Location = new System.Drawing.Point(123, 152);
      this.cb_skill.MaxDropDownItems = 8;
      this.cb_skill.Name = "cb_skill";
      this.cb_skill.OnFocusOpenListBox = true;
      this.cb_skill.SelectedIndex = -1;
      this.cb_skill.SelectedItem = null;
      this.cb_skill.SelectedValue = null;
      this.cb_skill.SelectionLength = 0;
      this.cb_skill.SelectionStart = 0;
      this.cb_skill.Size = new System.Drawing.Size(104, 24);
      this.cb_skill.Sorted = false;
      this.cb_skill.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.CUSTOM;
      this.cb_skill.TabIndex = 12;
      this.cb_skill.TabStop = false;
      this.cb_skill.ValueMember = "";
      this.cb_skill.SelectedIndexChanged += new System.EventHandler(this.cb_SelectedIndexChanged);
      // 
      // lbl_action_description
      // 
      this.lbl_action_description.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_action_description.BackColor = System.Drawing.Color.Transparent;
      this.lbl_action_description.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_action_description.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.lbl_action_description.Location = new System.Drawing.Point(324, 177);
      this.lbl_action_description.Name = "lbl_action_description";
      this.lbl_action_description.RightToLeft = System.Windows.Forms.RightToLeft.No;
      this.lbl_action_description.Size = new System.Drawing.Size(322, 34);
      this.lbl_action_description.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_RED;
      this.lbl_action_description.TabIndex = 21;
      this.lbl_action_description.Text = "xAction description";
      this.lbl_action_description.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.lbl_action_description.Visible = false;
      // 
      // gb_away_time
      // 
      this.gb_away_time.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.gb_away_time.BackColor = System.Drawing.Color.Transparent;
      this.gb_away_time.BorderColor = System.Drawing.Color.Empty;
      this.gb_away_time.Controls.Add(this.lbl_pause_time_value_total);
      this.gb_away_time.Controls.Add(this.lbl_pause_date_value);
      this.gb_away_time.Controls.Add(this.lbl_pause_time_value_actual);
      this.gb_away_time.Controls.Add(this.lbl_pause_date_name);
      this.gb_away_time.Controls.Add(this.lbl_pause_time_name_actual);
      this.gb_away_time.Controls.Add(this.lbl_pause_time_name_total);
      this.gb_away_time.CornerRadius = 10;
      this.gb_away_time.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_away_time.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gb_away_time.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_away_time.HeaderHeight = 18;
      this.gb_away_time.HeaderSubText = null;
      this.gb_away_time.HeaderText = "XTIEMPO AUSENTE";
      this.gb_away_time.Location = new System.Drawing.Point(325, 136);
      this.gb_away_time.Name = "gb_away_time";
      this.gb_away_time.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_away_time.Size = new System.Drawing.Size(322, 40);
      this.gb_away_time.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.MIN_HEAD;
      this.gb_away_time.TabIndex = 16;
      this.gb_away_time.Text = "Tiempo Ausente";
      // 
      // lbl_pause_time_value_total
      // 
      this.lbl_pause_time_value_total.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_pause_time_value_total.AutoSize = true;
      this.lbl_pause_time_value_total.BackColor = System.Drawing.Color.Transparent;
      this.lbl_pause_time_value_total.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_pause_time_value_total.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_pause_time_value_total.Location = new System.Drawing.Point(257, 21);
      this.lbl_pause_time_value_total.Name = "lbl_pause_time_value_total";
      this.lbl_pause_time_value_total.Size = new System.Drawing.Size(56, 17);
      this.lbl_pause_time_value_total.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_pause_time_value_total.TabIndex = 5;
      this.lbl_pause_time_value_total.Text = "0d 00:00";
      this.lbl_pause_time_value_total.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_pause_date_value
      // 
      this.lbl_pause_date_value.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_pause_date_value.AutoSize = true;
      this.lbl_pause_date_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_pause_date_value.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_pause_date_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_pause_date_value.Location = new System.Drawing.Point(52, 21);
      this.lbl_pause_date_value.Margin = new System.Windows.Forms.Padding(0);
      this.lbl_pause_date_value.Name = "lbl_pause_date_value";
      this.lbl_pause_date_value.Size = new System.Drawing.Size(49, 17);
      this.lbl_pause_date_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_pause_date_value.TabIndex = 1;
      this.lbl_pause_date_value.Text = "hh:mm";
      this.lbl_pause_date_value.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_pause_time_value_actual
      // 
      this.lbl_pause_time_value_actual.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_pause_time_value_actual.AutoSize = true;
      this.lbl_pause_time_value_actual.BackColor = System.Drawing.Color.Transparent;
      this.lbl_pause_time_value_actual.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_pause_time_value_actual.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_pause_time_value_actual.Location = new System.Drawing.Point(156, 21);
      this.lbl_pause_time_value_actual.Name = "lbl_pause_time_value_actual";
      this.lbl_pause_time_value_actual.Size = new System.Drawing.Size(56, 17);
      this.lbl_pause_time_value_actual.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_pause_time_value_actual.TabIndex = 3;
      this.lbl_pause_time_value_actual.Text = "0d 00:00";
      this.lbl_pause_time_value_actual.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_pause_date_name
      // 
      this.lbl_pause_date_name.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_pause_date_name.BackColor = System.Drawing.Color.Transparent;
      this.lbl_pause_date_name.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_pause_date_name.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_pause_date_name.Location = new System.Drawing.Point(6, 21);
      this.lbl_pause_date_name.Name = "lbl_pause_date_name";
      this.lbl_pause_date_name.Size = new System.Drawing.Size(49, 17);
      this.lbl_pause_date_name.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLUE;
      this.lbl_pause_date_name.TabIndex = 0;
      this.lbl_pause_date_name.Text = "Desde:";
      this.lbl_pause_date_name.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_pause_time_name_actual
      // 
      this.lbl_pause_time_name_actual.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_pause_time_name_actual.BackColor = System.Drawing.Color.Transparent;
      this.lbl_pause_time_name_actual.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_pause_time_name_actual.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_pause_time_name_actual.Location = new System.Drawing.Point(99, 21);
      this.lbl_pause_time_name_actual.Margin = new System.Windows.Forms.Padding(0);
      this.lbl_pause_time_name_actual.Name = "lbl_pause_time_name_actual";
      this.lbl_pause_time_name_actual.Size = new System.Drawing.Size(57, 17);
      this.lbl_pause_time_name_actual.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLUE;
      this.lbl_pause_time_name_actual.TabIndex = 2;
      this.lbl_pause_time_name_actual.Text = "Actual:";
      this.lbl_pause_time_name_actual.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_pause_time_name_total
      // 
      this.lbl_pause_time_name_total.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_pause_time_name_total.BackColor = System.Drawing.Color.Transparent;
      this.lbl_pause_time_name_total.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_pause_time_name_total.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_pause_time_name_total.Location = new System.Drawing.Point(218, 21);
      this.lbl_pause_time_name_total.Margin = new System.Windows.Forms.Padding(0);
      this.lbl_pause_time_name_total.Name = "lbl_pause_time_name_total";
      this.lbl_pause_time_name_total.Size = new System.Drawing.Size(40, 17);
      this.lbl_pause_time_name_total.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLUE;
      this.lbl_pause_time_name_total.TabIndex = 4;
      this.lbl_pause_time_name_total.Text = "Total:";
      this.lbl_pause_time_name_total.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // gb_chips
      // 
      this.gb_chips.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.gb_chips.BackColor = System.Drawing.Color.Transparent;
      this.gb_chips.BorderColor = System.Drawing.Color.Empty;
      this.gb_chips.Controls.Add(this.btn_sell_chips);
      this.gb_chips.Controls.Add(this.tb_netwin);
      this.gb_chips.Controls.Add(this.lbl_netwin);
      this.gb_chips.Controls.Add(this.lbl_total_chips_sell);
      this.gb_chips.Controls.Add(this.lbl_chips_sale_accumulate);
      this.gb_chips.Controls.Add(this.lbl_chips_in);
      this.gb_chips.Controls.Add(this.tb_chips_in);
      this.gb_chips.Controls.Add(this.lbl_chips_out);
      this.gb_chips.Controls.Add(this.tb_chips_out);
      this.gb_chips.CornerRadius = 10;
      this.gb_chips.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_chips.HeaderBackColor = System.Drawing.Color.Empty;
      this.gb_chips.HeaderForeColor = System.Drawing.Color.Empty;
      this.gb_chips.HeaderHeight = 0;
      this.gb_chips.HeaderSubText = null;
      this.gb_chips.HeaderText = null;
      this.gb_chips.Location = new System.Drawing.Point(653, 81);
      this.gb_chips.Name = "gb_chips";
      this.gb_chips.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_chips.Size = new System.Drawing.Size(231, 126);
      this.gb_chips.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.WITHOUT_HEAD;
      this.gb_chips.TabIndex = 23;
      // 
      // btn_sell_chips
      // 
      this.btn_sell_chips.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_sell_chips.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_sell_chips.FlatAppearance.BorderSize = 0;
      this.btn_sell_chips.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_sell_chips.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_sell_chips.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_sell_chips.Image = null;
      this.btn_sell_chips.IsSelected = false;
      this.btn_sell_chips.Location = new System.Drawing.Point(6, 7);
      this.btn_sell_chips.Name = "btn_sell_chips";
      this.btn_sell_chips.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_sell_chips.Size = new System.Drawing.Size(85, 40);
      this.btn_sell_chips.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_sell_chips.TabIndex = 0;
      this.btn_sell_chips.Text = "XSELL";
      this.btn_sell_chips.UseVisualStyleBackColor = false;
      // 
      // tb_netwin
      // 
      this.tb_netwin.BackColor = System.Drawing.Color.Transparent;
      this.tb_netwin.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.tb_netwin.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.tb_netwin.Location = new System.Drawing.Point(94, 106);
      this.tb_netwin.Name = "tb_netwin";
      this.tb_netwin.Size = new System.Drawing.Size(129, 17);
      this.tb_netwin.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.tb_netwin.TabIndex = 8;
      this.tb_netwin.Text = "0";
      this.tb_netwin.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_netwin
      // 
      this.lbl_netwin.BackColor = System.Drawing.Color.Transparent;
      this.lbl_netwin.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_netwin.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_netwin.Location = new System.Drawing.Point(6, 106);
      this.lbl_netwin.Name = "lbl_netwin";
      this.lbl_netwin.Size = new System.Drawing.Size(84, 17);
      this.lbl_netwin.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLUE;
      this.lbl_netwin.TabIndex = 7;
      this.lbl_netwin.Text = "xNetwin";
      this.lbl_netwin.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_total_chips_sell
      // 
      this.lbl_total_chips_sell.BackColor = System.Drawing.Color.Transparent;
      this.lbl_total_chips_sell.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_total_chips_sell.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_total_chips_sell.Location = new System.Drawing.Point(96, 20);
      this.lbl_total_chips_sell.Name = "lbl_total_chips_sell";
      this.lbl_total_chips_sell.Size = new System.Drawing.Size(55, 17);
      this.lbl_total_chips_sell.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLUE;
      this.lbl_total_chips_sell.TabIndex = 1;
      this.lbl_total_chips_sell.Text = "xTotal";
      this.lbl_total_chips_sell.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_chips_sale_accumulate
      // 
      this.lbl_chips_sale_accumulate.BackColor = System.Drawing.Color.Transparent;
      this.lbl_chips_sale_accumulate.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_chips_sale_accumulate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_chips_sale_accumulate.Location = new System.Drawing.Point(133, 20);
      this.lbl_chips_sale_accumulate.Name = "lbl_chips_sale_accumulate";
      this.lbl_chips_sale_accumulate.Size = new System.Drawing.Size(90, 17);
      this.lbl_chips_sale_accumulate.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_chips_sale_accumulate.TabIndex = 2;
      this.lbl_chips_sale_accumulate.Text = "0";
      this.lbl_chips_sale_accumulate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_chips_in
      // 
      this.lbl_chips_in.BackColor = System.Drawing.Color.Transparent;
      this.lbl_chips_in.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_chips_in.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_chips_in.Location = new System.Drawing.Point(6, 55);
      this.lbl_chips_in.Name = "lbl_chips_in";
      this.lbl_chips_in.Size = new System.Drawing.Size(84, 17);
      this.lbl_chips_in.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLUE;
      this.lbl_chips_in.TabIndex = 3;
      this.lbl_chips_in.Text = "xIn";
      this.lbl_chips_in.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // tb_chips_in
      // 
      this.tb_chips_in.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.tb_chips_in.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.tb_chips_in.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.tb_chips_in.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.tb_chips_in.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.tb_chips_in.CornerRadius = 5;
      this.tb_chips_in.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.tb_chips_in.Location = new System.Drawing.Point(96, 51);
      this.tb_chips_in.MaxLength = 32767;
      this.tb_chips_in.Multiline = false;
      this.tb_chips_in.Name = "tb_chips_in";
      this.tb_chips_in.PasswordChar = '\0';
      this.tb_chips_in.ReadOnly = true;
      this.tb_chips_in.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.tb_chips_in.SelectedText = "";
      this.tb_chips_in.SelectionLength = 0;
      this.tb_chips_in.SelectionStart = 0;
      this.tb_chips_in.Size = new System.Drawing.Size(129, 24);
      this.tb_chips_in.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC_CUSTOM;
      this.tb_chips_in.TabIndex = 4;
      this.tb_chips_in.TabStop = false;
      this.tb_chips_in.Text = "0";
      this.tb_chips_in.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      this.tb_chips_in.UseSystemPasswordChar = false;
      this.tb_chips_in.WaterMark = null;
      this.tb_chips_in.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // lbl_chips_out
      // 
      this.lbl_chips_out.BackColor = System.Drawing.Color.Transparent;
      this.lbl_chips_out.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_chips_out.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_chips_out.Location = new System.Drawing.Point(6, 82);
      this.lbl_chips_out.Name = "lbl_chips_out";
      this.lbl_chips_out.Size = new System.Drawing.Size(84, 17);
      this.lbl_chips_out.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLUE;
      this.lbl_chips_out.TabIndex = 5;
      this.lbl_chips_out.Text = "xOut";
      this.lbl_chips_out.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // tb_chips_out
      // 
      this.tb_chips_out.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.tb_chips_out.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.tb_chips_out.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.tb_chips_out.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.tb_chips_out.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.tb_chips_out.CornerRadius = 5;
      this.tb_chips_out.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.tb_chips_out.Location = new System.Drawing.Point(96, 78);
      this.tb_chips_out.MaxLength = 32767;
      this.tb_chips_out.Multiline = false;
      this.tb_chips_out.Name = "tb_chips_out";
      this.tb_chips_out.PasswordChar = '\0';
      this.tb_chips_out.ReadOnly = true;
      this.tb_chips_out.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.tb_chips_out.SelectedText = "";
      this.tb_chips_out.SelectionLength = 0;
      this.tb_chips_out.SelectionStart = 0;
      this.tb_chips_out.Size = new System.Drawing.Size(129, 24);
      this.tb_chips_out.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC_CUSTOM;
      this.tb_chips_out.TabIndex = 6;
      this.tb_chips_out.TabStop = false;
      this.tb_chips_out.Text = "0";
      this.tb_chips_out.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      this.tb_chips_out.UseSystemPasswordChar = false;
      this.tb_chips_out.WaterMark = null;
      this.tb_chips_out.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // gb_points
      // 
      this.gb_points.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.gb_points.BackColor = System.Drawing.Color.Transparent;
      this.gb_points.BorderColor = System.Drawing.Color.Empty;
      this.gb_points.Controls.Add(this.lbl_points_won);
      this.gb_points.Controls.Add(this.tb_points_won);
      this.gb_points.Controls.Add(this.lbl_points_won_calculate);
      this.gb_points.Controls.Add(this.lbl_calculate_points_won);
      this.gb_points.CornerRadius = 10;
      this.gb_points.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_points.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gb_points.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_points.HeaderHeight = 18;
      this.gb_points.HeaderSubText = null;
      this.gb_points.HeaderText = "XPOINTS";
      this.gb_points.Location = new System.Drawing.Point(653, 6);
      this.gb_points.Name = "gb_points";
      this.gb_points.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_points.Size = new System.Drawing.Size(231, 69);
      this.gb_points.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.MIN_HEAD;
      this.gb_points.TabIndex = 22;
      this.gb_points.Text = "xPoints";
      // 
      // lbl_points_won
      // 
      this.lbl_points_won.BackColor = System.Drawing.Color.Transparent;
      this.lbl_points_won.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_points_won.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_points_won.Location = new System.Drawing.Point(6, 29);
      this.lbl_points_won.Name = "lbl_points_won";
      this.lbl_points_won.Size = new System.Drawing.Size(84, 17);
      this.lbl_points_won.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLUE;
      this.lbl_points_won.TabIndex = 0;
      this.lbl_points_won.Text = "xPoints";
      this.lbl_points_won.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // tb_points_won
      // 
      this.tb_points_won.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.tb_points_won.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.tb_points_won.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.tb_points_won.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.tb_points_won.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.tb_points_won.CornerRadius = 5;
      this.tb_points_won.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.tb_points_won.Location = new System.Drawing.Point(97, 25);
      this.tb_points_won.MaxLength = 32767;
      this.tb_points_won.Multiline = false;
      this.tb_points_won.Name = "tb_points_won";
      this.tb_points_won.PasswordChar = '\0';
      this.tb_points_won.ReadOnly = true;
      this.tb_points_won.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.tb_points_won.SelectedText = "";
      this.tb_points_won.SelectionLength = 0;
      this.tb_points_won.SelectionStart = 0;
      this.tb_points_won.Size = new System.Drawing.Size(129, 24);
      this.tb_points_won.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC_CUSTOM;
      this.tb_points_won.TabIndex = 1;
      this.tb_points_won.TabStop = false;
      this.tb_points_won.Text = "0";
      this.tb_points_won.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      this.tb_points_won.UseSystemPasswordChar = false;
      this.tb_points_won.WaterMark = null;
      this.tb_points_won.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      this.tb_points_won.Validating += new System.ComponentModel.CancelEventHandler(this.tb_TextChanged);
      // 
      // lbl_points_won_calculate
      // 
      this.lbl_points_won_calculate.BackColor = System.Drawing.Color.Transparent;
      this.lbl_points_won_calculate.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_points_won_calculate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_points_won_calculate.Location = new System.Drawing.Point(6, 50);
      this.lbl_points_won_calculate.Name = "lbl_points_won_calculate";
      this.lbl_points_won_calculate.Size = new System.Drawing.Size(84, 17);
      this.lbl_points_won_calculate.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLUE;
      this.lbl_points_won_calculate.TabIndex = 2;
      this.lbl_points_won_calculate.Text = "xCalculated";
      this.lbl_points_won_calculate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_calculate_points_won
      // 
      this.lbl_calculate_points_won.BackColor = System.Drawing.Color.Transparent;
      this.lbl_calculate_points_won.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_calculate_points_won.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_calculate_points_won.Location = new System.Drawing.Point(94, 50);
      this.lbl_calculate_points_won.Name = "lbl_calculate_points_won";
      this.lbl_calculate_points_won.Size = new System.Drawing.Size(129, 17);
      this.lbl_calculate_points_won.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_calculate_points_won.TabIndex = 3;
      this.lbl_calculate_points_won.Text = "0";
      this.lbl_calculate_points_won.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // gb_bet
      // 
      this.gb_bet.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.gb_bet.BackColor = System.Drawing.Color.Transparent;
      this.gb_bet.BorderColor = System.Drawing.Color.Empty;
      this.gb_bet.Controls.Add(this.tb_max_played);
      this.gb_bet.Controls.Add(this.tb_min_played);
      this.gb_bet.Controls.Add(this.lbl_min_played);
      this.gb_bet.Controls.Add(this.cb_player_iso_code);
      this.gb_bet.Controls.Add(this.tb_total_bet);
      this.gb_bet.Controls.Add(this.lbl_average_bet);
      this.gb_bet.Controls.Add(this.lbl_average_bet_value);
      this.gb_bet.Controls.Add(this.lbl_current_bet);
      this.gb_bet.Controls.Add(this.lbl_max_played);
      this.gb_bet.Controls.Add(this.tb_current_bet);
      this.gb_bet.Controls.Add(this.lbl_total_bet);
      this.gb_bet.CornerRadius = 10;
      this.gb_bet.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_bet.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gb_bet.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_bet.HeaderHeight = 18;
      this.gb_bet.HeaderSubText = null;
      this.gb_bet.HeaderText = "XBET";
      this.gb_bet.Location = new System.Drawing.Point(324, 6);
      this.gb_bet.Name = "gb_bet";
      this.gb_bet.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_bet.Size = new System.Drawing.Size(323, 106);
      this.gb_bet.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.MIN_HEAD;
      this.gb_bet.TabIndex = 15;
      this.gb_bet.Text = "xBet";
      // 
      // tb_max_played
      // 
      this.tb_max_played.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.tb_max_played.BackColor = System.Drawing.Color.Transparent;
      this.tb_max_played.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.tb_max_played.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.tb_max_played.Location = new System.Drawing.Point(195, 50);
      this.tb_max_played.Name = "tb_max_played";
      this.tb_max_played.Size = new System.Drawing.Size(120, 17);
      this.tb_max_played.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.tb_max_played.TabIndex = 7;
      this.tb_max_played.Text = "0";
      this.tb_max_played.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // tb_min_played
      // 
      this.tb_min_played.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.tb_min_played.BackColor = System.Drawing.Color.Transparent;
      this.tb_min_played.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.tb_min_played.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.tb_min_played.Location = new System.Drawing.Point(36, 50);
      this.tb_min_played.Name = "tb_min_played";
      this.tb_min_played.Size = new System.Drawing.Size(120, 17);
      this.tb_min_played.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.tb_min_played.TabIndex = 3;
      this.tb_min_played.Text = "0";
      this.tb_min_played.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_min_played
      // 
      this.lbl_min_played.BackColor = System.Drawing.Color.Transparent;
      this.lbl_min_played.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_min_played.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_min_played.Location = new System.Drawing.Point(11, 50);
      this.lbl_min_played.Name = "lbl_min_played";
      this.lbl_min_played.Size = new System.Drawing.Size(74, 17);
      this.lbl_min_played.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLUE;
      this.lbl_min_played.TabIndex = 2;
      this.lbl_min_played.Text = "xMinimum";
      this.lbl_min_played.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // cb_player_iso_code
      // 
      this.cb_player_iso_code.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_player_iso_code.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.cb_player_iso_code.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.cb_player_iso_code.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.cb_player_iso_code.CornerRadius = 5;
      this.cb_player_iso_code.DataSource = null;
      this.cb_player_iso_code.DisplayMember = "";
      this.cb_player_iso_code.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
      this.cb_player_iso_code.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cb_player_iso_code.DropDownWidth = 70;
      this.cb_player_iso_code.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.cb_player_iso_code.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_player_iso_code.FormattingEnabled = true;
      this.cb_player_iso_code.IsDroppedDown = false;
      this.cb_player_iso_code.ItemHeight = 30;
      this.cb_player_iso_code.Location = new System.Drawing.Point(11, 25);
      this.cb_player_iso_code.MaxDropDownItems = 8;
      this.cb_player_iso_code.Name = "cb_player_iso_code";
      this.cb_player_iso_code.OnFocusOpenListBox = true;
      this.cb_player_iso_code.SelectedIndex = -1;
      this.cb_player_iso_code.SelectedItem = null;
      this.cb_player_iso_code.SelectedValue = null;
      this.cb_player_iso_code.SelectionLength = 0;
      this.cb_player_iso_code.SelectionStart = 0;
      this.cb_player_iso_code.Size = new System.Drawing.Size(70, 24);
      this.cb_player_iso_code.Sorted = false;
      this.cb_player_iso_code.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.CUSTOM;
      this.cb_player_iso_code.TabIndex = 11;
      this.cb_player_iso_code.TabStop = false;
      this.cb_player_iso_code.ValueMember = "";
      // 
      // tb_total_bet
      // 
      this.tb_total_bet.BackColor = System.Drawing.Color.Transparent;
      this.tb_total_bet.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.tb_total_bet.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.tb_total_bet.Location = new System.Drawing.Point(178, 68);
      this.tb_total_bet.Name = "tb_total_bet";
      this.tb_total_bet.Size = new System.Drawing.Size(137, 17);
      this.tb_total_bet.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.tb_total_bet.TabIndex = 8;
      this.tb_total_bet.Text = "0";
      this.tb_total_bet.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_average_bet
      // 
      this.lbl_average_bet.BackColor = System.Drawing.Color.Transparent;
      this.lbl_average_bet.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_average_bet.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_average_bet.Location = new System.Drawing.Point(10, 87);
      this.lbl_average_bet.Name = "lbl_average_bet";
      this.lbl_average_bet.Size = new System.Drawing.Size(144, 17);
      this.lbl_average_bet.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLUE;
      this.lbl_average_bet.TabIndex = 5;
      this.lbl_average_bet.Text = "xAverage";
      this.lbl_average_bet.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_average_bet_value
      // 
      this.lbl_average_bet_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_average_bet_value.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_average_bet_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_average_bet_value.Location = new System.Drawing.Point(178, 87);
      this.lbl_average_bet_value.Name = "lbl_average_bet_value";
      this.lbl_average_bet_value.Size = new System.Drawing.Size(137, 17);
      this.lbl_average_bet_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_average_bet_value.TabIndex = 9;
      this.lbl_average_bet_value.Text = "0";
      this.lbl_average_bet_value.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_current_bet
      // 
      this.lbl_current_bet.BackColor = System.Drawing.Color.Transparent;
      this.lbl_current_bet.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_current_bet.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_current_bet.Location = new System.Drawing.Point(90, 29);
      this.lbl_current_bet.Name = "lbl_current_bet";
      this.lbl_current_bet.Size = new System.Drawing.Size(65, 17);
      this.lbl_current_bet.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLUE;
      this.lbl_current_bet.TabIndex = 0;
      this.lbl_current_bet.Text = "xCurrent";
      this.lbl_current_bet.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_max_played
      // 
      this.lbl_max_played.BackColor = System.Drawing.Color.Transparent;
      this.lbl_max_played.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_max_played.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_max_played.Location = new System.Drawing.Point(165, 50);
      this.lbl_max_played.Name = "lbl_max_played";
      this.lbl_max_played.Size = new System.Drawing.Size(82, 17);
      this.lbl_max_played.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLUE;
      this.lbl_max_played.TabIndex = 6;
      this.lbl_max_played.Text = "xMaximum";
      this.lbl_max_played.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // tb_current_bet
      // 
      this.tb_current_bet.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.tb_current_bet.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.tb_current_bet.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.tb_current_bet.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.tb_current_bet.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.tb_current_bet.CornerRadius = 5;
      this.tb_current_bet.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.tb_current_bet.Location = new System.Drawing.Point(166, 25);
      this.tb_current_bet.MaxLength = 32767;
      this.tb_current_bet.Multiline = false;
      this.tb_current_bet.Name = "tb_current_bet";
      this.tb_current_bet.PasswordChar = '\0';
      this.tb_current_bet.ReadOnly = true;
      this.tb_current_bet.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.tb_current_bet.SelectedText = "";
      this.tb_current_bet.SelectionLength = 0;
      this.tb_current_bet.SelectionStart = 0;
      this.tb_current_bet.Size = new System.Drawing.Size(152, 24);
      this.tb_current_bet.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC_CUSTOM;
      this.tb_current_bet.TabIndex = 1;
      this.tb_current_bet.TabStop = false;
      this.tb_current_bet.Text = "0";
      this.tb_current_bet.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      this.tb_current_bet.UseSystemPasswordChar = false;
      this.tb_current_bet.WaterMark = null;
      this.tb_current_bet.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      this.tb_current_bet.Validating += new System.ComponentModel.CancelEventHandler(this.tb_TextChanged);
      // 
      // lbl_total_bet
      // 
      this.lbl_total_bet.BackColor = System.Drawing.Color.Transparent;
      this.lbl_total_bet.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_total_bet.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_total_bet.Location = new System.Drawing.Point(10, 68);
      this.lbl_total_bet.Name = "lbl_total_bet";
      this.lbl_total_bet.Size = new System.Drawing.Size(144, 17);
      this.lbl_total_bet.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLUE;
      this.lbl_total_bet.TabIndex = 4;
      this.lbl_total_bet.Text = "xTotal";
      this.lbl_total_bet.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // btn_swap_seat
      // 
      this.btn_swap_seat.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_swap_seat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_swap_seat.FlatAppearance.BorderSize = 0;
      this.btn_swap_seat.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_swap_seat.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_swap_seat.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_swap_seat.Image = null;
      this.btn_swap_seat.IsSelected = false;
      this.btn_swap_seat.Location = new System.Drawing.Point(890, 152);
      this.btn_swap_seat.Name = "btn_swap_seat";
      this.btn_swap_seat.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_swap_seat.Size = new System.Drawing.Size(55, 55);
      this.btn_swap_seat.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_swap_seat.TabIndex = 26;
      this.btn_swap_seat.Text = "XSWAP";
      this.btn_swap_seat.UseVisualStyleBackColor = false;
      // 
      // btn_play_pause
      // 
      this.btn_play_pause.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_play_pause.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_play_pause.FlatAppearance.BorderSize = 0;
      this.btn_play_pause.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_play_pause.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_play_pause.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_play_pause.Image = null;
      this.btn_play_pause.IsSelected = false;
      this.btn_play_pause.Location = new System.Drawing.Point(890, 79);
      this.btn_play_pause.Name = "btn_play_pause";
      this.btn_play_pause.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_play_pause.Size = new System.Drawing.Size(55, 55);
      this.btn_play_pause.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_play_pause.TabIndex = 25;
      this.btn_play_pause.Text = "XPLAY";
      this.btn_play_pause.UseVisualStyleBackColor = false;
      // 
      // btn_close
      // 
      this.btn_close.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_close.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_close.FlatAppearance.BorderSize = 0;
      this.btn_close.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_close.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_close.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_close.Image = null;
      this.btn_close.IsSelected = false;
      this.btn_close.Location = new System.Drawing.Point(890, 6);
      this.btn_close.Name = "btn_close";
      this.btn_close.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_close.Size = new System.Drawing.Size(55, 55);
      this.btn_close.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_close.TabIndex = 24;
      this.btn_close.Text = "XCLOSE";
      this.btn_close.UseVisualStyleBackColor = false;
      // 
      // uc_gt_player_in_session
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.Controls.Add(this.gb_played_time);
      this.Controls.Add(this.gb_account);
      this.Controls.Add(this.lbl_action_description);
      this.Controls.Add(this.gb_away_time);
      this.Controls.Add(this.gb_chips);
      this.Controls.Add(this.gb_points);
      this.Controls.Add(this.gb_bet);
      this.Controls.Add(this.btn_swap_seat);
      this.Controls.Add(this.btn_play_pause);
      this.Controls.Add(this.btn_close);
      this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.Name = "uc_gt_player_in_session";
      this.Size = new System.Drawing.Size(952, 217);
      this.gb_played_time.ResumeLayout(false);
      this.gb_account.ResumeLayout(false);
      this.gb_account.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pb_seat_img)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.pb_user)).EndInit();
      this.gb_away_time.ResumeLayout(false);
      this.gb_away_time.PerformLayout();
      this.gb_chips.ResumeLayout(false);
      this.gb_points.ResumeLayout(false);
      this.gb_bet.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private WSI.Cashier.Controls.uc_label lbl_points_balance;
    private WSI.Cashier.Controls.uc_label lbl_track_number;
    private WSI.Cashier.Controls.uc_label lbl_trackdata;
    private WSI.Cashier.Controls.uc_label lbl_level;
    private WSI.Cashier.Controls.uc_label lbl_points_balance_title;
    private WSI.Cashier.Controls.uc_label lbl_level_title;
    private System.Windows.Forms.PictureBox pb_user;
    private WSI.Cashier.Controls.uc_label lbl_holder_name;
    private WSI.Cashier.Controls.uc_label lbl_account_id_value;
    private WSI.Cashier.Controls.uc_label lbl_account_id_name;
    private WSI.Cashier.Controls.uc_round_button btn_close;
    private WSI.Cashier.Controls.uc_round_button btn_play_pause;
    private WSI.Cashier.Controls.uc_round_button btn_swap_seat;
    private WSI.Cashier.Controls.uc_label lbl_current_bet;
    private WSI.Cashier.Controls.uc_label lbl_netwin;
    private WSI.Cashier.Controls.uc_label lbl_points_won;
    private WSI.Cashier.Controls.uc_label lbl_total_bet;
    private WSI.Cashier.Controls.uc_round_textbox tb_current_bet;
    private WSI.Cashier.Controls.uc_round_textbox tb_points_won;
    private WSI.Cashier.Controls.uc_label lbl_min_played;
    private WSI.Cashier.Controls.uc_label lbl_max_played;
    private WSI.Cashier.Controls.uc_label lbl_speed;
    private WSI.Cashier.Controls.uc_label lbl_skill;
    private WSI.Cashier.Controls.uc_round_combobox cb_speed;
    private WSI.Cashier.Controls.uc_round_combobox cb_skill;
    private WSI.Cashier.Controls.uc_label lbl_points_won_calculate;
    private WSI.Cashier.Controls.uc_label lbl_calculate_points_won;
    private WSI.Cashier.Controls.uc_label lbl_play_date_name;
    private WSI.Cashier.Controls.uc_label lbl_pause_date_name;
    private WSI.Cashier.Controls.uc_label lbl_pause_date_value;
    private WSI.Cashier.Controls.uc_label lbl_action_description;
    private System.Windows.Forms.Timer timerClear;
    private WSI.Cashier.Controls.uc_round_button btn_sell_chips;
    private System.Windows.Forms.Timer timerRefresh;
    private WSI.Cashier.Controls.uc_label lbl_play_time_value;
    private WSI.Cashier.Controls.uc_label lbl_pause_time_value_total;
    private WSI.Cashier.Controls.uc_label lbl_plays_value;
    private WSI.Cashier.Controls.uc_label lbl_plays_name;
    private WSI.Cashier.Controls.uc_label tb_min_played;
    private WSI.Cashier.Controls.uc_label tb_max_played;
    private WSI.Cashier.Controls.uc_round_panel gb_bet;
    private WSI.Cashier.Controls.uc_label lbl_average_bet;
    private WSI.Cashier.Controls.uc_label lbl_average_bet_value;
    private WSI.Cashier.Controls.uc_round_panel gb_points;
    private WSI.Cashier.Controls.uc_label lbl_pause_time_name_total;
    private WSI.Cashier.Controls.uc_label lbl_play_time_name;
    private WSI.Cashier.Controls.uc_round_panel gb_chips;
    private WSI.Cashier.Controls.uc_label lbl_chips_sale_accumulate;
    private WSI.Cashier.Controls.uc_label lbl_chips_in;
    private WSI.Cashier.Controls.uc_round_textbox tb_chips_in;
    private WSI.Cashier.Controls.uc_label lbl_chips_out;
    private WSI.Cashier.Controls.uc_round_textbox tb_chips_out;
    private WSI.Cashier.Controls.uc_label lbl_total_chips_sell;
    private WSI.Cashier.Controls.uc_label tb_total_bet;
    private WSI.Cashier.Controls.uc_label tb_netwin;
    private WSI.Cashier.Controls.uc_label lbl_pause_time_value_actual;
    private WSI.Cashier.Controls.uc_label lbl_pause_time_name_actual;
    private WSI.Cashier.Controls.uc_round_panel gb_away_time;
    private WSI.Cashier.Controls.uc_label lbl_play_date_value;
    private Controls.uc_round_panel gb_account;
    private System.Windows.Forms.PictureBox pb_seat_img;
    private Controls.uc_round_panel gb_played_time;
    private Controls.uc_round_combobox cb_player_iso_code;

  }
}
