//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_scan_preview.cs
// 
//   DESCRIPTION: Implements document scan dialog. 
//
//        AUTHOR: Javier Molina
// 
// CREATION DATE: 13-JUN-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 13-JUN-2013 JMM    First release.
// 29-JUL-2013 ACM    Modified: only scanning image and return the path of the scanned file.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Windows.Forms;
using WSI.Common;
using System.Threading;
using System.Drawing;
using System.IO;
using WSI.Cashier.Controls;

namespace WSI.Cashier
{
  public partial class frm_scan_preview : frm_base
  {
    delegate void SetEnableButtonsCallback(bool Enabled);

    #region Constants


    #endregion

    #region Members

    private frm_yesno form_yes_no;
    private Int32 m_count_down_value;
    private DateTime m_count_down_started;

    private String m_error_message;
    private String m_out_file_name;

    #endregion

    #region Properties

    public String OutFileName
    {
      get { return m_out_file_name; }
    }

    public String ErrorMessage
    {
      get { return m_error_message; }
    }

    #endregion

    #region Constructor

    /// <summary>
    /// Constructor method.
    /// 1. Initialize controls.
    /// 2. Initialize control�s resources. (NLS, Images, etc)
    /// </summary>
    public frm_scan_preview()
    {
      InitializeComponent();

      m_count_down_value = GeneralParam.GetInt32("Cashier.Scan", "AutoStartAfterSeconds", 30);

      InitControls();

      InitializeControlResources();

      Init();
    }

    #endregion

    #region Private Methods

    private void InitControls()
    {
      EventLastAction.AddEventLastAction(this.Controls);

      this.btn_scan.Click += new EventHandler(this.btn_scan_Click);
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Init resources
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void InitializeControlResources()
    {
      //NLS Strings    
      this.FormTitle = Resource.String("STR_FRM_SCAN_TITLE");
      this.btn_cancel.Text = Resource.String("STR_FRM_SCAN_CANCEL");
      this.btn_scan.Text = Resource.String("STR_FRM_SCAN_SCAN");
      this.lbl_text.Text = Resource.String("STR_FRM_SCAN_MESSAGE_3");

    } // InitializeControlResources

    //------------------------------------------------------------------------------
    // PURPOSE : Init 
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void Init()
    {
      m_count_down_started = WGDB.Now;
 
      m_error_message = String.Empty;

      tm_scan_Tick(this, null);
    } // Init  

    private void SetEnableButtons(bool Enabled)
    {
      if (this.btn_cancel.InvokeRequired || this.btn_scan.InvokeRequired)
      {
        SetEnableButtonsCallback _buttons_callback = new SetEnableButtonsCallback(SetEnableButtons);
        this.Invoke(_buttons_callback, new object[] { Enabled });
      }
      else
      {
        this.tm_scan.Enabled = Enabled;
        this.btn_cancel.Enabled = Enabled;
        this.btn_scan.Enabled = Enabled;

        if (Enabled)
        {
          m_count_down_started = WGDB.Now;
          this.tm_scan_Tick(this, null);
        }
      }
    }


    private void ScanID()
    {
      try
      {
        SetEnableButtons(false);

        String _in_filename;

        _in_filename = "scan_temp";

        this.Cursor = Cursors.WaitCursor;
        this.lbl_count_down.Text = Resource.String("STR_FRM_SCAN_SCANNING");

        if (ScanImage.Start(_in_filename, out m_out_file_name))
        {
          this.DialogResult = DialogResult.OK;
          return;
        }

        m_error_message = Resource.String("STR_FRM_SCAN_ERROR_GENERIC");

        this.DialogResult = DialogResult.Cancel;
      }

      catch (Exception _ex)
      {
        Log.Exception(_ex);

        m_error_message = Resource.String("STR_FRM_SCAN_ERROR_GENERIC");

        this.DialogResult = DialogResult.Cancel;
      }

      finally
      {
        this.Cursor = Cursors.Default;
      }
    } // ScanID


    #endregion

    #region Events

    private void btn_scan_Click(object sender, EventArgs e)
    {
      ScanID();
    }

    private void tm_scan_Tick(object sender, EventArgs e)
    {
      TimeSpan _time_span;
      String _seconds_left;

      _time_span = WGDB.Now - m_count_down_started;

      if (_time_span.TotalSeconds > m_count_down_value)
      {
        ScanID();
      }
      else
      {
        _seconds_left = String.Format("{0}", Math.Max(0, Math.Min(m_count_down_value, Math.Truncate(m_count_down_value - _time_span.TotalSeconds))));
        this.lbl_count_down.Text = Resource.String("STR_FRM_SCAN_COUNT_DOWN_MESSAGE", _seconds_left);
      }
    }

    #endregion
    
    #region Public Methods

    public new Boolean Show()
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_scan_preview", Log.Type.Message);

      form_yes_no = new frm_yesno();
      form_yes_no.Opacity = 0.6;
      form_yes_no.Show();

      // Center screen
      if (WindowManager.StartUpMode == WindowManager.StartUpModes.Explorer)
      {
        this.Location = new Point(WindowManager.GetFormCenterLocation(form_yes_no).X - (this.Width / 2)
                                  , WindowManager.GetFormCenterLocation(form_yes_no).Y - (form_yes_no.Height / 2));
      }
      else
      {
        this.Location = new Point(1024 / 2 - this.Width / 2, 0);
      }

      this.ShowDialog(form_yes_no);

      form_yes_no.Hide();

      return (this.DialogResult == DialogResult.OK);
    } // Show

    //------------------------------------------------------------------------------
    // PURPOSE : Dispose yes_no form.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    new public void Dispose()
    {
      form_yes_no.Dispose();
      form_yes_no = null;

      base.Dispose();
    }

    #endregion
  }
}
