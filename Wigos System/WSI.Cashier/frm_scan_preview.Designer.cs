namespace WSI.Cashier
{
  partial class frm_scan_preview
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.btn_scan = new WSI.Cashier.Controls.uc_round_button();
      this.btn_cancel = new WSI.Cashier.Controls.uc_round_button();
      this.lbl_count_down = new WSI.Cashier.Controls.uc_label();
      this.lbl_text = new WSI.Cashier.Controls.uc_label();
      this.tm_scan = new System.Windows.Forms.Timer(this.components);
      this.pnl_data.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.Controls.Add(this.btn_scan);
      this.pnl_data.Controls.Add(this.btn_cancel);
      this.pnl_data.Controls.Add(this.lbl_count_down);
      this.pnl_data.Controls.Add(this.lbl_text);
      this.pnl_data.Size = new System.Drawing.Size(377, 263);
      this.pnl_data.TabIndex = 0;
      // 
      // btn_scan
      // 
      this.btn_scan.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_scan.FlatAppearance.BorderSize = 0;
      this.btn_scan.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_scan.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_scan.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_scan.Image = null;
      this.btn_scan.IsSelected = false;
      this.btn_scan.Location = new System.Drawing.Point(201, 203);
      this.btn_scan.Name = "btn_scan";
      this.btn_scan.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_scan.Size = new System.Drawing.Size(166, 48);
      this.btn_scan.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_scan.TabIndex = 2;
      this.btn_scan.Text = "XOK";
      this.btn_scan.UseVisualStyleBackColor = false;
      // 
      // btn_cancel
      // 
      this.btn_cancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btn_cancel.FlatAppearance.BorderSize = 0;
      this.btn_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_cancel.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_cancel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_cancel.Image = null;
      this.btn_cancel.IsSelected = false;
      this.btn_cancel.Location = new System.Drawing.Point(12, 203);
      this.btn_cancel.Name = "btn_cancel";
      this.btn_cancel.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_cancel.Size = new System.Drawing.Size(166, 48);
      this.btn_cancel.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_cancel.TabIndex = 3;
      this.btn_cancel.Text = "XCANCEL";
      this.btn_cancel.UseVisualStyleBackColor = false;
      // 
      // lbl_count_down
      // 
      this.lbl_count_down.BackColor = System.Drawing.Color.Transparent;
      this.lbl_count_down.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_count_down.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_count_down.Location = new System.Drawing.Point(12, 157);
      this.lbl_count_down.Name = "lbl_count_down";
      this.lbl_count_down.Size = new System.Drawing.Size(355, 43);
      this.lbl_count_down.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_count_down.TabIndex = 1;
      this.lbl_count_down.Text = "xCountDown";
      this.lbl_count_down.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // lbl_text
      // 
      this.lbl_text.BackColor = System.Drawing.Color.Transparent;
      this.lbl_text.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_text.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_text.Location = new System.Drawing.Point(12, 14);
      this.lbl_text.Name = "lbl_text";
      this.lbl_text.Size = new System.Drawing.Size(355, 137);
      this.lbl_text.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_text.TabIndex = 0;
      this.lbl_text.Text = "xMessage";
      // 
      // tm_scan
      // 
      this.tm_scan.Enabled = true;
      this.tm_scan.Interval = 500;
      this.tm_scan.Tick += new System.EventHandler(this.tm_scan_Tick);
      // 
      // frm_scan_preview
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.AutoSize = true;
      this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
      this.ClientSize = new System.Drawing.Size(377, 318);
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "frm_scan_preview";
      this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
      this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
      this.Style = WSI.Cashier.Controls.frm_base.BaseFormStyle.PRINCIPAL;
      this.Text = "xScanDocument";
      this.pnl_data.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion
    

    private WSI.Cashier.Controls.uc_round_button btn_cancel;

    private WSI.Cashier.Controls.uc_label lbl_text;
    private System.Windows.Forms.Timer tm_scan;
    private WSI.Cashier.Controls.uc_round_button btn_scan;
    private WSI.Cashier.Controls.uc_label lbl_count_down;


  }
}