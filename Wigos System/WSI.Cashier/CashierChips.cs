//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: CashierChips.cs
// 
//   DESCRIPTION: Common cashier operations on Chips.
//
//        AUTHOR: Ra�l Cervera
// 
// CREATION DATE: 23-DEC-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 23-DIC-2013 RCI    First release.
// 15-JAN-2014 JBP    Refund in TITO Ticket.
// 29-JAN-2014 RCI & ACM    Purchase amount must be considered as prize (not devolution).
// 14-FEB-2014 RMS & DLL    Fixed bug WIG-627:buy chips without gaming table selector
// 14-Feb-2014 DHA    WIGOSTITO-1038: Show message printer error, when printer is not ready
// 20-MAR-2014 DLL    WIGOSTITO-1159: Don't undo tickets associated to chips sale & purchase
// 16-APR-2014 ICS, MPO & RCI    Fixed Bug WIG-830: Not enough available storage exception.
// 22-APR-2014 DLL    Added new functionality for print specific voucher for the Dealer
// 16-JUL-2014 DRV    Added new Chip Sale Register functionality
// 12-AUG-2014 RCI    Fixed Bug WIG-1156: Deposit value when user sale chips only takes in account the last one.
// 21-AUG-2014 DRV    Fixed Bug WIG-1193: No one should have to select a table for a chip sale register
// 27-AUG-2014 DRV    Fixed Bug WIG-1211: Chips Sale/Purchase doesn't work with decimals
// 16-SEP-2014 DHA & JML & RCI    Fixed Bug WIG-1242: Can't undo last operation chips purchase when is virtual account
// 21-OCT-2014 SGB    Fixed Bug WIG-1476: Buy & Sale chips in frm_chips
// 02-FEB-2015 MPO    WIG-1969: System crash when printing "constancias"
// 02-APR-2015 DHA    Avoid to send Chips sale register on table operations to MultiSite
// 23-APR-2015 YNM    Fixed Bug WIG-2240: Payment Order + Chips Purchase take cashier taxes 
// 07-MAY-2015 YNM    Fixed Bug TFS-286: Cashier ChipsPurchase, taxes should be deducted when payment is with Ticket TITO
// 25-JUN-2015 DHA    Added manual conciliation chips sales
// 11-NOV-2015 FOS    Product Backlog Item: 3709 Payment tickets & handpays
// 23-NOV-2015 FOS    Bug 6771:Payment tickets & handpays
// 24-FEB-2016 EOR    Product Backlog Item 7402:SAS20: Add Comments HandPays
// 07-MAR-2016 LTC    Product Backlog Item 10227:TITA: TITO ticket exchange for gambling chips
// 17-MAR-2016 ESE    Product Backlog Item 10229:TITA: Create swap chips with ticket TITO
// 29-MAR-2016 RGR    Product Backlog Item 10981:Sprint Review 21 Winions - Mex
// 30-MAR-2016 RGR    Product Backlog Item 10981:Sprint Review 21 Winions - Mex - TITA: code review and merge
// 04-APR-2016 RGR    Product Backlog Item 11018:UNPLANNED - Temas varios Winions - Mex Sprint 22
// 13-APR-2016 DHA    Product Backlog Item 9950: added chips operation (sales and purchases)
// 30-MAY-2016 LTC    Bug 13968:Cashier - TITA: Not select distribution swap chips
// 02-JUN-2016 LTC    Bug 13792:Cashless, Swap chips: can not be validated residual tickets
// 06-JUL-2016 DHA    Product Backlog Item 15064: multicurrency chips sale/purchase
// 26-JUL-2016 RGR    Product Backlog Item 15318: Table-21: Sales of chips A and B now
// 09-AUG-2016 FGB    Product Backlog Item 15064: multicurrency chips sale/purchase
// 19-SEP-2016 FGB    PBI 15985: Exped System: Cashier: Check if the payment/sale can be done
// 22-SEP-2016 LTC    Product Backlog Item 17461: Gamingtables 46 - Cash desk draw for gamblign tables: Chips purchase
// 22-SEP-2016 LTC    Product Backlog Item 17461: Gamingtables 46 - Cash desk draw for gamblign tables: Chips purchase
// 30-SEP-2016 ETP    Bug 18003: Cashier: los vouchers de recarga no reflejan los cambios solicitados por Televisa
// 13-OCT-2016 LTC    Bug 19012: Craw on Gaming tables: won amount is not given as gaming chips
// 14-OCT-2016 DHA    Bug 19012: Craw on Gaming tables: won amount is not given as gaming chips
// 21-OCT-2016 ETP    Bug 19314: PinPad Card Payment Refactorized
// 24-OCT-2016 ETP    Fixed Bug 19314 Pin Pad recharge refactoring
// 26-OCT-2016 ETP    Fixed Bug 19314 Pin Pad recharge Wigos Rollback + Pinpad + Wigos Commit
// 08-NOV-2016 ETP    Fixed Bug Bug 20035: Exception out of memory when pinpad is used.
// 18-OCT-2016 FAV    PBI 20407: TITO Ticket: Calculated fields
// 15-NOV-2016 ETP    Fixed Bug Bug 20363: Can't recharge wihtout participate in cashdesk draw 
// 03-MAR-2017 JML    Fixed Bug 25394:Tax Custody erroneous. Types 0 y 2
// 15-MAR-2017 FAV    PBI 25262: Exped - Payment authorisation
// 22-MAR-2017 DPC    PBI 25788:CreditLine - Get a Marker
// 24-MAR-2017 ETP    WIGOS-110: Creditlines - Paybacks
// 29-MAR-2017 DPC    WIGOS-685: CreditLine - Undo last operation "Get Marker"
// 27-APR-2017 DHA    Bug 26735:WTA-350: wait till cut manual paper on printer
// 04-MAY-2017 DPC    WIGOS-1574: Junkets - Flyers - New field "Text in Promotional Voucher" - Change voucher
// 21-MAY-2017 ETP    Fixed Bug WIGOS-2997: Added voucher data.
// 08-MAY-2017 MS     PBI 27045: Junket Vouchers
// 23-MAY-2017 DHA    PBI 27487: WIGOS-83 MES21 - Sales selection
// 30-MAY-2017 JML    PBI 27486: WIGOS-1227 Win / loss - Introduce the WinLoss - EDITION SCREEN - ACCUMULATED
// 06-JUN-2017 DHA    PBI 27760:WIGOS-257: MES21 - Gaming table chair assignment with card selection
// 19-JUN-2017 JML    PBI 27999:WIGOS-2699 - Rounding - Accumulate the remaining amount
// 26-JUL-2017 DPC    WIGOS-3990: Creditline: two messages appear when selling chips using expired credit line
// 09-OCT-2017 JML    PBI 30022:WIGOS-4052 Payment threshold registration - Recharge with Check payment
// 10-OCT-2017 JML    PBI 30023:WIGOS-4068 Payment threshold registration - Transaction information
// 11-OCT-2017 DHA    Bug 30224:WIGOS-5696 [Ticket #9466] error en canje de tickets por fichas con centavos (TITA)
// 19-OCT-2017 JML    Fixed Bug 30321:WIGOS-5924 [Ticket #9450] F�rmula datos tarjeta bancaria (entregado-faltante) - Resumen de sesi�n de caja
// 24-OCT-2017 RAB    Bug 30384:WIGOS-5996 Cashier - Threshold recharge - Cashless: threshold error is registered when is done a sale chips with credit card or check in cashless
// 30-OCT-2017 RAB    Bug 30446:WIGOS-6052 Threshold: Screen order is incorrect in HP and dispute (paying with PO) exceeding threshold and without permissions
// 02-NOV-2017 JML    Bug 30524:WIGOS-6283 Gambling tables alarms: "AmountPurchaseInOneOperation" alarm is not triggered when there are tax custody and chip sa...
// 03-NOV-2017 RAB    Bug 30553:WIGOS-6352 Threshold - recharge: An "EXCEPTION: Referencia a objeto no establecida como instancia de un objeto" is registered when the sale chips by credit card or check is below to boundary value configuration.
// 06-NOV-2017 RAB    Bug 30580:WIGOS-6271 Gambling tables alarms: "AmountPurchaseInOneOperation" alarm is not triggered when there are check/credit card taxes applied to sales chips operation
// 06-NOV-2017 DHA    Bug 30224:WIGOS-5696 [Ticket #9466] error en canje de tickets por fichas con centavos (TITA)
// 13-NOV-2017 DHA    Bug 30775:WIGOS-6110 [Ticket #9672] MTY I - Plaza Real - PinPad con error y autorizadas
// 15-NOV-2017 RAB    Bug 30797:WIGOS-6578 Gambling tables alarms: "AmountPurchaseInOneOperation" alarm is not triggered when the amount is over or equal to boundary value.
// 20-NOV-2017 RAB    Bug 30856:WIGOS-6692 Threshold - output: Buy chips threshold transaction is saved with foreign currency when the currency exchange amount of local currency is below to boundary value
// 20-NOV-2017 RAB    Bug 30858:WIGOS-6680 Threshold - input: The transaction is not saved in BD when the sale chips is done by cash with local currency
// 04-MAY-2018 AGS    Bug 32552:WIGOS-8179 [Ticket #12450] Reporte de Compra Venta de Fichas Duplica ventas en Caja V03.06.0035
// 12-APR-2018 JGC    Bug 32311:[WIGOS-9805]: [Ticket #13564] Reporte de "Resumen de caja"
//------------------------------------------------------------------------------
//
using System;
using System.Collections.Generic;
using System.Text;
using WSI.Common;
using WSI.Common.TITO;
using System.Windows.Forms;
using WSI.Cashier.TITO;
using System.Collections;
using WSI.Common.PinPad;
using System.Data.SqlClient;
using WSI.Common.ExternalPaymentAndSaleValidation;
using WSI.Common.CreditLines;
using WSI.Cashier.JunketsCashier;

namespace WSI.Cashier
{
  public class CashierChips
  {
    public static PaymentThresholdAuthorization m_payment_threshold_authorization;

    #region Public Methods

    public static Boolean ChipsPurchase_GetAmount(CardData Card,
                                                  frm_amount_input.CheckPermissionsToRedeemDelegate dCheckPermissionsToRedeem,
                                                  Form Parent, out FeatureChips.ChipsOperation ChipsAmount, out ParamsTicketOperation OperationParams, 
                                                  out PaymentThresholdAuthorization PaymentThresholdAuthorization)
    {
      return ChipsPurchase_GetAmount(Card, dCheckPermissionsToRedeem, null, Parent, out ChipsAmount, out OperationParams, out PaymentThresholdAuthorization);
    }

    public static Boolean ChipsPurchase_GetAmount(CardData Card,
                                                  frm_amount_input.CheckPermissionsToRedeemDelegate dCheckPermissionsToRedeem, Int32? GamingTableId,
                                                  Form Parent, out FeatureChips.ChipsOperation ChipsAmount, out ParamsTicketOperation OperationParams, 
                                                  out PaymentThresholdAuthorization PaymentThresholdAuthorization)
    {
      frm_chips _f_chips;
      Boolean _check_redeem;
      Boolean _is_tito_mode;
      frm_gaming_table_select _frm_sel_gamin_table;
      LinkedGamingTable _selected_gaming_table;
      Boolean _integrated_chip_operation;
      OperationCode _operation_code;

      _check_redeem = false;
      _is_tito_mode = WSI.Common.TITO.Utils.IsTitoMode();
      _integrated_chip_operation = FeatureChips.IsIntegratedChipAndCreditOperations(Card.IsVirtualCard);

      ChipsAmount = new FeatureChips.ChipsOperation(Cashier.SessionId, CASHIER_MOVEMENT.CHIPS_PURCHASE);
      OperationParams = new ParamsTicketOperation();
      PaymentThresholdAuthorization = null;

      if (GeneralParam.GetBoolean("GamingTables", "Purchases.AskForSourceTable", false) && !GamingTableId.HasValue)
      {
        _frm_sel_gamin_table = new frm_gaming_table_select();

        try
        {
          if (!_frm_sel_gamin_table.Show(Resource.String("STR_FRM_GAMING_TABLE_SELECT_TITLE"), out _selected_gaming_table))
          {
            return false;
          }
        }
        finally
        {
          // ICS 14-APR-2014 Free the object
          _frm_sel_gamin_table.Dispose();
        }
      }
      else
      {
        // DHA 22-JUL-2014
        if (GamingTableId.HasValue)
        {
          _selected_gaming_table = new LinkedGamingTable();
          _selected_gaming_table.CashierSession = Cashier.CashierSessionInfo().CashierSessionId;
          _selected_gaming_table.TableId = GamingTableId.Value;
        }
        else if (GamingTablesSessions.GamingTableInfo.IsGamingTable)
        {
          _selected_gaming_table = new LinkedGamingTable();
          _selected_gaming_table.CashierSession = Cashier.CashierSessionInfo().CashierSessionId;
          _selected_gaming_table.TableId = GamingTablesSessions.GamingTableInfo.GamingTableId;
        }
        else
        {
          _selected_gaming_table = LinkedGamingTable.UnknownSession();
        }
      }

      ChipsAmount.SelectedGamingTable = _selected_gaming_table;

      Cage.ShowDenominationsMode _show_denominations_mode;
      _show_denominations_mode = (Cage.ShowDenominationsMode)GeneralParam.GetInt32("Cage", "ShowDenominations", 1);

      _f_chips = new frm_chips();

      try
      {
        if (!_f_chips.Show(Card, _is_tito_mode, _integrated_chip_operation, Parent, ref ChipsAmount, out _check_redeem))
        {
          return false;
        }
      }
      finally
      {
        // ICS 14-APR-2014 Free the object
        _f_chips.Dispose();
      }

      if (ChipsAmount == null)
      {
        return false;
      }

      if ((_is_tito_mode) || _integrated_chip_operation)
      {
        Card.AccountBalance = new MultiPromos.AccountBalance(ChipsAmount.AmountNationalCurrency, 0, 0);
        if (_integrated_chip_operation)
        {
          // DRV & RCI 22-AUG-2014: Update the balance in memory.
          Card.CurrentBalance += ChipsAmount.AmountNationalCurrency;
          OperationParams.out_cash_amount = Card.AccountBalance.TotalRedeemable;
        }

        OperationParams.in_account = Card;
        OperationParams.in_window_parent = Parent;
        OperationParams.check_redeem = _check_redeem;
        _operation_code = _integrated_chip_operation ? OperationCode.CHIPS_PURCHASE_WITH_CASH_OUT : OperationCode.CHIPS_PURCHASE;

        OperationParams.is_apply_tax = ChipsAmount.IsApplyTaxes;

        if (CashierBusinessLogic.WithholdAndPaymentOrderDialog(_operation_code,
                                                               ref OperationParams,
                                                               CashierBusinessLogic.EnterWitholdingData,
                                                               CashierBusinessLogic.EnterOrderPaymentData,
                                                               dCheckPermissionsToRedeem,
                                                               _integrated_chip_operation || _is_tito_mode) != ENUM_TITO_OPERATIONS_RESULT.OK)
        {
          return false;
        }

        PaymentThresholdAuthorization _payment_threshold_authorization;
        PaymentAndRechargeThresholdData _payment_and_recharge_threshold_data;
        Boolean _process_continue;

        _payment_and_recharge_threshold_data = new PaymentAndRechargeThresholdData();
        _payment_threshold_authorization = new PaymentThresholdAuthorization(ChipsAmount.AmountNationalCurrency, Card, _payment_and_recharge_threshold_data, PaymentThresholdAuthorization.PaymentThresholdAuthorizationOperation.Output, Parent, out _process_continue);
        PaymentThresholdAuthorization = _payment_threshold_authorization;

        if (!_process_continue)
        {
          return false;
        }
      } // if _is_tito_mode

      return true;
    } // ChipsPurchase_GetAmount

    public static Boolean ChipsPurchase_Save(CardData Card,
                                             FeatureChips.ChipsOperation ChipsAmount,
                                             ParamsTicketOperation OperationParams,
                                             PaymentThresholdAuthorization PaymentThresholdAuthorization, 
                                             out String ErrorMessage)
    {
      return ChipsPurchase_Save(Card, ChipsAmount, OperationParams, null, null, PaymentThresholdAuthorization, out ErrorMessage);
    }

    public static Boolean ChipsPurchase_Save(CardData Card,
                                             FeatureChips.ChipsOperation ChipsAmount,
                                             ParamsTicketOperation OperationParams,
                                             ExternalValidationInputParameters ExpedInputParams,
                                             ExternalValidationOutputParameters ExpedOutputParams,
                                             PaymentThresholdAuthorization PaymentThresholdAuthorization,
                                             out String ErrorMessage)
    {
      Int64 _operation_id;
      ArrayList _chips_voucher_list;
      Boolean _is_tito_mode;
      GamingTablesSessions _gt_session_selected;
      GamingTablesSessions _gt_session_own;
      GTS_UPDATE_TYPE _gts_update_type;
      ParamsTicketOperation _params_ticket_operation;
      Ticket _ticket;
      CashierSessionInfo _cashier_session_info;
      Boolean _integrated_chip_operation;
      OperationCode _operation_code;
      TicketCreationParams _params;
      RechargeOutputParameters OutputParameter;
      ArrayList _voucher_list;

      Boolean _customer_gets_paid_without_a_purchase;
      String _alarm_description;
      Boolean _has_customer_chips_purchases;
      Int32 _minutes_from_last_chips_sale;
      Int32 _payment_done_by_player_under_minutes;
      Currency _paid_amount_limit;

      _is_tito_mode = WSI.Common.TITO.Utils.IsTitoMode();
      _integrated_chip_operation = FeatureChips.IsIntegratedChipAndCreditOperations(Card.IsVirtualCard);

      _cashier_session_info = Cashier.CashierSessionInfo();
      _ticket = null;
      ErrorMessage = String.Empty;
      _voucher_list = new ArrayList();

      _operation_code = _integrated_chip_operation ? OperationCode.CHIPS_PURCHASE_WITH_CASH_OUT : OperationCode.CHIPS_PURCHASE;

      // DHA & JML & RCI 16-SEP-2014
      if (Card.IsVirtualCard && !_is_tito_mode)
      {
        _operation_code = OperationCode.CHIPS_PURCHASE_WITH_CASH_OUT;
      }

      //JGC 03/05/2018: Trace Net Amount WIGOS-9805
      Misc.WriteLog(string.Format("CashierChips > NetAmount = {0}", ChipsAmount.ChipsNetAmount), Log.Type.Message);

      using (DB_TRX _db_trx = new DB_TRX())
      {
        if (!Operations.DB_InsertOperation(_operation_code,
                                           Card.AccountId,
                                           Cashier.SessionId,
                                           0,
                                           0,
                                           ChipsAmount.ChipsNetAmount,
                                           0,
                                           0,                      // Operation Data
                                           ChipsAmount.AccountOperationReasonId,
                                           ChipsAmount.AccountOperationComment,
                                           out _operation_id,
                                           out ErrorMessage,
                                           _db_trx.SqlTransaction,
                                           ExpedInputParams,
                                           ExpedOutputParams))
        {
          return false;
        }

        // Update Gaming Table Sessions
        if (!GamingTablesSessions.GetOrOpenSession(out _gt_session_selected
                                                  , ChipsAmount.SelectedGamingTable.TableId
                                                  , ChipsAmount.SelectedGamingTable.CashierSession
                                                  , _db_trx.SqlTransaction))
        {
          return false;
        }

        ChipsAmount.SelectedGamingTable.GamingTableSession = _gt_session_selected.GamingTableSessionId;

        if (!FeatureChips.Purchase(ChipsAmount, _operation_id, Card, _is_tito_mode, _cashier_session_info,
                                   out _chips_voucher_list, _db_trx.SqlTransaction))
        {
          return false;
        }

        if (_is_tito_mode && ChipsAmount.PrintTicket)
        {
          if (!TitoPrinter.IsReady())
          {
            // Do Rollback here, a frm_message is going to be shown!!!!
            _db_trx.Rollback();

            Log.Error("ChipPurchase_Save: PrintTicket. Printer is not ready.");

            PrinterStatus _status;
            String _status_text;
            String _msg_text;

            _msg_text = Resource.String("STR_FRM_CHIPS_TITO_PRINTER_FAILS");
            TitoPrinter.GetStatus(out _status, out _status_text);
            _msg_text += "\r\n    - " + TitoPrinter.GetMsgError(_status);

            frm_message.Show(_msg_text,
                             Resource.String("STR_APP_GEN_MSG_ERROR"),
                             MessageBoxButtons.OK,
                             MessageBoxIcon.Error,
                             Cashier.MainForm);

            return false;
          }

          //Generate Chips Purchase Movements - CashOut
          if (!CashierBusinessLogic.DB_CardCreditRedeem(_operation_id,
                                                          Card.AccountId,
                                                          OperationParams.withhold,
                                                          OperationParams.payment_order,
                                                          0,
                                                          CASH_MODE.TOTAL_REDEEM,
                                                          CreditRedeemSourceOperationType.CHIPS_PURCHASE_WITH_CASHOUT,
                                                          Card,
                                                          _db_trx.SqlTransaction,
                                                          out ErrorMessage,
                                                          ChipsAmount.IsApplyTaxes,
                                                          out _voucher_list))
          {
            Log.Error("ChipPurchase_Save: DB_CardCreditRedeem. Error subtracting credit to card.");

            return false;
          }

          _chips_voucher_list.AddRange(_voucher_list);

          //Generate Ticket Sell Movements - CashIn
          _params_ticket_operation = new ParamsTicketOperation();
          _params_ticket_operation.out_cash_amount = OperationParams.cash_redeem_total_paid;
          _params_ticket_operation.ticket_type = TITO_TICKET_TYPE.CASHABLE;
          _params_ticket_operation.in_account = Card;
          _params_ticket_operation.out_promotion_id = 0;
          _params_ticket_operation.session_info = _cashier_session_info;
          _params_ticket_operation.out_operation_id = _operation_id;
          // DLL 20-MAR-2014: Terminal_id has to be set when the terminal is a gaming terminal. When it's a cashier terminal, don't have to set it.
          _params_ticket_operation.terminal_id = 0;

          _params = new TicketCreationParams();
          _params.OutCashAmount = OperationParams.cash_redeem_total_paid;

          OutputParameter = new RechargeOutputParameters();

          if (!Accounts.DB_CardCreditAdd(OperationParams.in_account,
                                         OperationParams.cash_redeem_total_paid,
                                         _params.OutPromotionId,
                                         _params.PromoRewardAmount,
                                         _params.OutNotRedeemableWonLock,
                                         _params.OutSpentUsed,
                                         _params.OutExchangeResult,
                                         _params.OutParticipateInCashdeskDraw,
                                         OperationCode.CASH_IN,
                                         _operation_id,
                                         _cashier_session_info,
                                         _db_trx.SqlTransaction,
                                         out OutputParameter))
          {
            Log.Error("ChipPurchase_Save: DB_CardCreditAdd. Error adding credit to card.");

            return false;
          }
          _chips_voucher_list.Add(OutputParameter.VoucherList[0]);

          //Generate Ticket TITO
          if (!BusinessLogic.CreateTicket(_params_ticket_operation, true, out _ticket, _db_trx.SqlTransaction))
          {
            Log.Error("ChipsPurchase: BusinessLogic.CreateTicket. Error creating ticket.");

            //////todo
            ////frm_message.Show(Resource.String("STR_FRM_AMOUNT_INPUT_MSG_AMOUNT_EXCEED_BANK")
            ////  , Resource.String("STR_APP_GEN_MSG_ERROR")
            ////  , MessageBoxButtons.OK
            ////  , MessageBoxIcon.Error,
            ////  , );

            return false;
          }
        }
        else if (_is_tito_mode || _integrated_chip_operation)
        {
          // RCI 12-AUG-2014: When Integrated chip operation, the cash out operation is inserted before MaxDevolution is calculated.
          //                  This produces that MaxDevolution is 0. It is necessary to use the MaxDevolution calculated previous to insert the cash out operation.
          if (!CashierBusinessLogic.DB_CardCreditRedeem(_operation_id,
                                                        Card.AccountId,
                                                        OperationParams.withhold,
                                                        OperationParams.payment_order,
                                                        0,
                                                        CASH_MODE.TOTAL_REDEEM,
                                                        CreditRedeemSourceOperationType.CHIPS_PURCHASE_WITH_CASHOUT,
                                                        Card,
                                                        _db_trx.SqlTransaction,
                                                        out ErrorMessage,
                                                        ChipsAmount.IsApplyTaxes,
                                                        out _voucher_list))
          {
            Log.Error("ChipsPurchase: DB_CardCreditRedeem. Error subtracting credit to card.");

            return false;
          }

          _chips_voucher_list.AddRange(_voucher_list);
        }

        if (ChipsAmount.SelectedGamingTable.CashierSession == _cashier_session_info.CashierSessionId)
        {
          _gts_update_type = GTS_UPDATE_TYPE.ChipsOwnPurchase;
        }
        else
        {
          _gts_update_type = GTS_UPDATE_TYPE.ChipsExternalPurchase;
        }

        if (!_gt_session_selected.UpdateSession(_gts_update_type, ChipsAmount.AmountNationalCurrency, new CurrencyIsoType(ChipsAmount.IsoCode, ChipsAmount.CurrencyExchangeType), _db_trx.SqlTransaction))
        {
          return false;
        }

        // Update visits
        if (!_gt_session_selected.UpdateSession(GTS_UPDATE_TYPE.Visits, 1, _db_trx.SqlTransaction))
        {
          return false;
        }

        if (GamingTablesSessions.GamingTableInfo.IsGamingTable)
        {
          if (_gts_update_type == GTS_UPDATE_TYPE.ChipsExternalPurchase)
          {
            if (!GamingTablesSessions.GetOrOpenSession(out _gt_session_own
                                                      , GamingTablesSessions.GamingTableInfo.GamingTableId
                                                      , _cashier_session_info.CashierSessionId
                                                      , _db_trx.SqlTransaction))
            {
              return false;
            }

            if (!_gt_session_own.UpdateSession(GTS_UPDATE_TYPE.ChipsTotalPurchase, ChipsAmount.AmountNationalCurrency, new CurrencyIsoType(ChipsAmount.IsoCode, ChipsAmount.CurrencyExchangeType), _db_trx.SqlTransaction))
            {
              return false;
            }

            if (!_gt_session_own.UpdateSessionLastSessionId(GTS_UPDATE_TYPE.LastExternalPurchase, _gt_session_selected.CashierSessionId, _db_trx.SqlTransaction))
            {
              return false;
            }
          } // ChipsExternalPurchase
          else
          {
            if (!_gt_session_selected.UpdateSessionLastSessionId(GTS_UPDATE_TYPE.LastExternalPurchase, null, _db_trx.SqlTransaction))
            {
              return false;
            }
          }
        } // IsGamingTable

        // Alarm: Alarm for Customer gets paid without a Drop (purchase).
        // Alarm: Alarm for Purchase time limit.
        _customer_gets_paid_without_a_purchase = GamingTableAlarms.GetCustomerGetsPaidWithoutADrop();
        _payment_done_by_player_under_minutes = GamingTableAlarms.GetPaymentDoneByPlayerUnderMinutes();

        if (_customer_gets_paid_without_a_purchase || _payment_done_by_player_under_minutes > 0)
        {
          // Commun function for two alarms
          _has_customer_chips_purchases = GamingTableAlarms.HasCustomerChipsPurchases(Card.AccountId, out _minutes_from_last_chips_sale, _db_trx.SqlTransaction);

          if (_customer_gets_paid_without_a_purchase && !_has_customer_chips_purchases)
          {
            // Insert Alarm
            _alarm_description = Resource.String("STR_ALARM_GAMBLING_TABLES_CUSTOMER_GETS_PAID_WITHOUT_A_PURCHASE", Card.AccountId);
            Alarm.Register(AlarmSourceCode.Cashier,
                           _cashier_session_info.UserId,
                           _cashier_session_info.UserName + "@" + _cashier_session_info.TerminalName,
                           (Int32)AlarmCode.User_GamingTables_CustomerGetsPaidWithoutAPurchase,
                           _alarm_description,
                           AlarmSeverity.Warning,
                           WGDB.Now, _db_trx.SqlTransaction);
          }
          if (_payment_done_by_player_under_minutes > 0 && _payment_done_by_player_under_minutes > _minutes_from_last_chips_sale && !Card.IsVirtualCard)
          {
            // Insert Alarm
            _alarm_description = Resource.String("STR_ALARM_GAMBLING_TABLES_PAYMENT_DONE_BY_PLAYER_UNDER_MINUTES", Card.AccountId, _payment_done_by_player_under_minutes, _minutes_from_last_chips_sale);
            Alarm.Register(AlarmSourceCode.Cashier,
                          _cashier_session_info.UserId,
                          _cashier_session_info.UserName + "@" + _cashier_session_info.TerminalName,
                          (Int32)AlarmCode.User_GamingTables_PaymentDoneByPlayer_UnderMinutes,
                          _alarm_description,
                          AlarmSeverity.Warning,
                          WGDB.Now, _db_trx.SqlTransaction);
          }
        }
        // End Alarm: Customer gets paid without a Drop (purchase).
        // End Alarm: Alarm for Purchase time limit.

        // Alarm: Alarm for Amount purchased in one operation
        _paid_amount_limit = GamingTableAlarms.GetAmountPaidInOneOperation();

        if (_paid_amount_limit > 0 && _paid_amount_limit <= ChipsAmount.AmountNationalCurrency)
        {
          // Insert Alarm
          _alarm_description = Resource.String("STR_ALARM_GAMBLING_TABLES_AMOUNT_PAID_IN_ONE_OPERATION", ChipsAmount.AmountNationalCurrency);
          Alarm.Register(AlarmSourceCode.Cashier,
                        _cashier_session_info.UserId,
                        _cashier_session_info.UserName + "@" + _cashier_session_info.TerminalName,
                        (Int32)AlarmCode.User_GamingTables_AmountPaidInOneOperation,
                        _alarm_description,
                        AlarmSeverity.Warning,
                        WGDB.Now, _db_trx.SqlTransaction);
        }
        // End Alarm: Alarm for Amount purchased in one operation

        if (PaymentThresholdAuthorization != null && PaymentThresholdAuthorization.OutputThreshold > 0 && ChipsAmount.AmountNationalCurrency >= PaymentThresholdAuthorization.OutputThreshold)
        {
          if (OperationParams.payment_order != null)
          {
            PaymentThresholdAuthorization.PaymentAndRechargeThresholdData.CashAmount = OperationParams.payment_order.CashPayment;
            if (OperationParams.payment_order.CheckPayment > 0)
            {
              PaymentThresholdAuthorization.PaymentAndRechargeThresholdData.TransactionType = CurrencyExchangeType.CHECK;
              PaymentThresholdAuthorization.PaymentAndRechargeThresholdData.TransactionSubType = OperationParams.payment_order.CheckType;
            }
          }
          PaymentThresholdAuthorization.SaveThresholdOperation(_operation_id, _operation_code, _db_trx.SqlTransaction);
        }



        _db_trx.Commit();

      } // DB_TRX _db_trx

      if (CashierBusinessLogic.GLB_OperationIdForPrintDocs > 0)
      {
        // Select the printer and print the Witholding documents
        PrintingDocuments.PrintDialogDocuments(CashierBusinessLogic.GLB_OperationIdForPrintDocs);
      }

      if (_ticket != null)
      {
        if (!TitoPrinter.PrintTicket(_ticket.ValidationNumber, _ticket.MachineTicketNumber, _ticket.TicketType, _ticket.Amount, _ticket.CreatedDateTime, _ticket.ExpirationDateTime, _cashier_session_info))
        {
          Log.Error("ChipPurchase_Save: PrintTicket. Error on printing ticket.");

          PrinterStatus _status;
          String _status_text;

          TitoPrinter.GetStatus(out _status, out _status_text);
          _status_text = TitoPrinter.GetMsgError(_status);

          frm_message.Show(_status_text + "\r\n" + Resource.String("STR_FRM_CONTAINER_EVENTS_DESC_PRINTER_CANCEL_OPERATION"),
                           Resource.String("STR_APP_GEN_MSG_ERROR"),
                           MessageBoxButtons.OK,
                           MessageBoxIcon.Error,
                           Cashier.MainForm);
          // 'return true': Sale Chip operation is correctly, but printing fail. Transaction is commited. 
          // The user must cancel last operation.
        }
      }

      VoucherPrint.Print(_chips_voucher_list);

      return true;
    } // ChipsPurchase_Save

    public static Boolean ChipsSaleWithRecharge_GetAmount(CardData Card, frm_amount_input FormCredit, Int32? GamingTableId, Form Parent,
                                                      out FeatureChips.ChipsOperation ChipsAmount, out CashierOperations.TYPE_CARD_CASH_IN CashOperation,
                                                      out CurrencyExchangeResult ExchangeResult)
    {
      ParticipateInCashDeskDraw _participate_in_cash_draw;
      PaymentThresholdAuthorization _payment_threshold_authorization;
      return Internal_ChipsSale_GetAmount(Card, FormCredit, VoucherTypes.ChipsSaleWithRecharge, GamingTableId, Parent, false, out ChipsAmount, out CashOperation, out ExchangeResult, out _participate_in_cash_draw, out _payment_threshold_authorization);
    }

    public static Boolean ChipsSaleRegister_GetAmount(CardData Card, frm_amount_input FormCredit, VoucherTypes Voucher, Form Parent,
                                                  out FeatureChips.ChipsOperation ChipsAmount, out CashierOperations.TYPE_CARD_CASH_IN CashOperation,
                                                  out CurrencyExchangeResult ExchangeResult)
    {
      ParticipateInCashDeskDraw _participate_in_cash_draw;
      PaymentThresholdAuthorization _payment_threshold_authorization;
      return Internal_ChipsSale_GetAmount(Card, FormCredit, Voucher, null, Parent, true, out ChipsAmount, out CashOperation, out ExchangeResult, out _participate_in_cash_draw, out _payment_threshold_authorization);
    }

    public static Boolean ChipsSaleRegister_GetAmount(CardData Card, frm_amount_input FormCredit, VoucherTypes Voucher, Int32? GamingTableId, Form Parent,
                                                  out FeatureChips.ChipsOperation ChipsAmount, out CashierOperations.TYPE_CARD_CASH_IN CashOperation,
                                                  out CurrencyExchangeResult ExchangeResult)
    {
      ParticipateInCashDeskDraw _participate_in_cash_draw;
      PaymentThresholdAuthorization _payment_threshold_authorization;
      return Internal_ChipsSale_GetAmount(Card, FormCredit, Voucher, GamingTableId, Parent, true, out ChipsAmount, out CashOperation, out ExchangeResult, out _participate_in_cash_draw, out _payment_threshold_authorization);
    }

    public static Boolean ChipsSale_GetAmount(CardData Card, frm_amount_input FormCredit, VoucherTypes Voucher, Int32? GamingTableId, Form Parent,
                                              out FeatureChips.ChipsOperation ChipsAmount, out CashierOperations.TYPE_CARD_CASH_IN CashOperation,
                                              out CurrencyExchangeResult ExchangeResult, out ParticipateInCashDeskDraw _participate_in_cash_draw, out PaymentThresholdAuthorization PaymentThresholdAuthorization)
    {
      return Internal_ChipsSale_GetAmount(Card, FormCredit, Voucher, GamingTableId, Parent, false, out ChipsAmount, out CashOperation, out ExchangeResult, out _participate_in_cash_draw, out PaymentThresholdAuthorization);
    }

    public static Boolean ChipsSale_GetAmount(CardData Card, frm_amount_input FormCredit, VoucherTypes Voucher, Int32? GamingTableId, Form Parent,
                                              out FeatureChips.ChipsOperation ChipsAmount, out CashierOperations.TYPE_CARD_CASH_IN CashOperation,
                                              out CurrencyExchangeResult ExchangeResult)
    {
      ParticipateInCashDeskDraw _participate_in_cash_draw;
      PaymentThresholdAuthorization _payment_threshold_authorization;
      return Internal_ChipsSale_GetAmount(Card, FormCredit, Voucher, GamingTableId, Parent, false, out ChipsAmount, out CashOperation, out ExchangeResult, out _participate_in_cash_draw, out _payment_threshold_authorization);
    }

    public static Boolean ChipsSale_Save(CardData Card, Form Parent,
                                         FeatureChips.ChipsOperation ChipsAmount, ChipSaleType SaleType, out String ErrorStr, out Ticket TicketCopyDealer)
    {
      RechargeOutputParameters OutputParameter;
      return Internal_ChipsSale_Save(Card, OperationCode.CHIPS_SALE, Parent, ChipsAmount, new CashierOperations.TYPE_CARD_CASH_IN(), null, SaleType, null, out ErrorStr, out OutputParameter, ParticipateInCashDeskDraw.No, out TicketCopyDealer);
    }

    public static Boolean ChipsSaleWithRecharge_Save(CardData Card, Form Parent,
                                                     FeatureChips.ChipsOperation ChipsAmount, CashierOperations.TYPE_CARD_CASH_IN CashOperation,
                                                     CurrencyExchangeResult ExchangeResult, ChipSaleType SaleType, out String ErrorStr,
                                                      out RechargeOutputParameters OutputParameter, ParticipateInCashDeskDraw _participate_in_cash_draw, out Ticket TicketCopyDealer)
    {
      return Internal_ChipsSale_Save(Card, OperationCode.CHIPS_SALE_WITH_RECHARGE, Parent, ChipsAmount, CashOperation, ExchangeResult, SaleType, null, out ErrorStr, out OutputParameter, _participate_in_cash_draw, out TicketCopyDealer);
    }

    /// LTC 07-MAR-2016
    /// <summary>
    /// Chips Swap Get Amount
    /// </summary>
    /// <param name="Card"></param>
    /// <param name="FormCredit"></param>
    /// <param name="Voucher"></param>
    /// <param name="Parent"></param>
    /// <param name="tickets_total_amount"></param>
    /// <param name="ChipsOperation"></param>
    /// <param name="CashOperation"></param>
    /// <param name="ExchangeResult"></param>
    /// <returns></returns>
    public static Boolean ChipsSwap_GetAmount(CardData Card, frm_amount_input FormCredit, VoucherTypes Voucher, Form Parent, Decimal tickets_total_amount,
                                              out FeatureChips.ChipsOperation ChipsOperation, out CashierOperations.TYPE_CARD_CASH_IN CashOperation,
                                              out CurrencyExchangeResult ExchangeResult)
    {
      return Internal_ChipsSwap_GetAmount(Card, FormCredit, Voucher, null, Parent, false, tickets_total_amount, out ChipsOperation, out CashOperation, out ExchangeResult);
    }

    public static Boolean ChipsSaleWithRecharge_Save(CardData Card, Form Parent,
                                                     FeatureChips.ChipsOperation ChipsAmount, CashierOperations.TYPE_CARD_CASH_IN CashOperation,
                                                     CurrencyExchangeResult ExchangeResult, PaymentThresholdAuthorization PaymentThresholdAuthorization, out String ErrorStr, out RechargeOutputParameters OutputParameter, out Ticket TicketCopyDealer)
    {
      return Internal_ChipsSale_Save(Card, OperationCode.CHIPS_SALE_WITH_RECHARGE, Parent, ChipsAmount, CashOperation, ExchangeResult, ChipSaleType.CashDeskSale, PaymentThresholdAuthorization, out ErrorStr, out OutputParameter, ParticipateInCashDeskDraw.No, out TicketCopyDealer);
    }

    public static Boolean ChipsSwapWithTicketTito_Save(CardData Card, Form Parent,
                                                     FeatureChips.ChipsOperation ChipsOperation, CashierOperations.TYPE_CARD_CASH_IN CashOperation,
                                                     CurrencyExchangeResult ExchangeResult, ParamsTicketOperation Tickets, out String ErrorStr, out Ticket TicketCopyDealer)
    {
      RechargeOutputParameters OutputParameter;
      return Internal_ChipsSale_Save(Card, OperationCode.CHIPS_SWAP, Parent, ChipsOperation, CashOperation, ExchangeResult, null, ChipSaleType.CashDeskSale, Tickets, out ErrorStr, out OutputParameter, ParticipateInCashDeskDraw.No, out TicketCopyDealer);
    }

    #endregion // Public Methods

    #region Private Methods

    private static Boolean Internal_ChipsSale_GetAmount(CardData Card, frm_amount_input FormCredit, VoucherTypes VoucherType, Int32? GamingTableId, Form Parent, Boolean IsChipsSaleRegister,
                                                    out FeatureChips.ChipsOperation ChipsAmount, out CashierOperations.TYPE_CARD_CASH_IN CashOperation,
                                                    out CurrencyExchangeResult ExchangeResult, out ParticipateInCashDeskDraw _participate_in_cash_draw, 
                                                    out PaymentThresholdAuthorization PaymentThresholdAuthorization)
    {
      Object _operation_data;
      String _title;
      frm_gaming_table_select _frm_sel_gaming_table;
      LinkedGamingTable _selected_gaming_table;

      ChipsAmount = null;
      CashOperation = new CashierOperations.TYPE_CARD_CASH_IN();
      ExchangeResult = null;
      _selected_gaming_table = new LinkedGamingTable();
      _participate_in_cash_draw = new ParticipateInCashDeskDraw();
      PaymentThresholdAuthorization = null;

      if (!IsChipsSaleRegister)
      {
        _frm_sel_gaming_table = new frm_gaming_table_select();

        try
        {
          // 25-JUN-2015 added for manual conciliation chips sales
          if (GamingTableId.HasValue)
          {
            _selected_gaming_table.CashierSession = Cashier.CashierSessionInfo().CashierSessionId;
            _selected_gaming_table.TableId = GamingTableId.Value;
          }
          else if (!_frm_sel_gaming_table.Show(Resource.String("STR_FRM_GAMING_TABLE_SELECT_TITLE"), out _selected_gaming_table))
          {
            return false;
          }
        }
        finally
        {
          // ICS 14-APR-2014 Free the object
          _frm_sel_gaming_table.Dispose();
        }
      }
      else
      {
        VoucherType = VoucherTypes.ChipsSaleWithRecharge;
      }

      switch (VoucherType)
      {
        case VoucherTypes.ChipsSaleAmountInput:
          _title = Resource.String("STR_VOUCHER_CASH_CHIPS_SALE_TITLE");
          break;

        case VoucherTypes.ChipsSaleWithRecharge:
          if (IsChipsSaleRegister)
          {
            _title = Resource.String("STR_FRM_GAMING_TABLE_SELECT_BTN_REGISTER");
          }
          else
          {
            _title = Resource.String("STR_VOUCHER_CASH_CHIPS_SALE_RECHARGE_TITLE");
          }
          break;

        default:
          return false;
      }

      ChipsAmount = new FeatureChips.ChipsOperation(Cashier.SessionId, CASHIER_MOVEMENT.CHIPS_SALE);

      ChipsAmount.SelectedGamingTable = _selected_gaming_table;

      if (!FormCredit.Show(_title, VoucherType, Card, (frm_yesno)Parent, IsChipsSaleRegister,
                            out _participate_in_cash_draw, out ExchangeResult, out _operation_data, out PaymentThresholdAuthorization, ref ChipsAmount))
      {
        return false;
      }

      m_payment_threshold_authorization = PaymentThresholdAuthorization;
      
      if (ChipsAmount == null)
      {
        return false;
      }

      ChipsAmount.IsChipsSaleRegister = IsChipsSaleRegister;

      if (VoucherType == VoucherTypes.ChipsSaleWithRecharge)
      {
        CashOperation = (CashierOperations.TYPE_CARD_CASH_IN)_operation_data;

        if (CashOperation.amount_to_pay + CashOperation.reward == 0)
        {
          return false;
        }
      }

      return true;
    } // Internal_ChipsSale_GetAmount

    /// LTC 07-MAR-2016
    /// <summary>
    ///  Get Amount of Chips to Swap
    /// </summary>
    /// <param name="Card"></param>
    /// <param name="FormCredit"></param>
    /// <param name="VoucherType"></param>
    /// <param name="GamingTableId"></param>
    /// <param name="Parent"></param>
    /// <param name="IsChipsSaleRegister"></param>
    /// <param name="TicketsTotalAmount"></param>
    /// <param name="ChipsOperation"></param>
    /// <param name="CashOperation"></param>
    /// <param name="ExchangeResult"></param>
    /// <returns></returns>
    private static Boolean Internal_ChipsSwap_GetAmount(CardData Card, frm_amount_input FormCredit, VoucherTypes VoucherType, Int32? GamingTableId, Form Parent, Boolean IsChipsSaleRegister, decimal TicketsTotalAmount,
                                                    out FeatureChips.ChipsOperation ChipsOperation, out CashierOperations.TYPE_CARD_CASH_IN CashOperation,
                                                    out CurrencyExchangeResult ExchangeResult)
    {
      frm_gaming_table_select _frm_sel_gaming_table;
      LinkedGamingTable _selected_gaming_table;
      frm_chips _f_chips;

      ChipsOperation = null;
      CashOperation = new CashierOperations.TYPE_CARD_CASH_IN();
      ExchangeResult = null;
      _selected_gaming_table = new LinkedGamingTable();
      _f_chips = new frm_chips();
      _frm_sel_gaming_table = new frm_gaming_table_select();

      // LTC 30-MAY-2016
      _selected_gaming_table.CashierSession = 0;
      _selected_gaming_table.TableId = 0;

      try
      {
        // LTC 30-MAY-2016
        if (!_frm_sel_gaming_table.Show(Resource.String("STR_FRM_GAMING_TABLE_SELECT_TITLE"), out _selected_gaming_table))
        {
          return false;
        }
      }
      finally
      {
        _frm_sel_gaming_table.Dispose();
      }

      ChipsOperation = new FeatureChips.ChipsOperation(Cashier.CashierSessionInfo().CashierSessionId, TicketsTotalAmount,
                                                       CASHIER_MOVEMENT.TITO_TICKET_CASHIER_SWAP_FOR_CHIPS);

      ChipsOperation.SelectedGamingTable = _selected_gaming_table;

      // LTC 07-MAR-2016 
      if (!_f_chips.Show(Card, WSI.Common.TITO.Utils.IsTitoMode(), Parent, ref ChipsOperation, false))
      {
        return false;
      }

      if (ChipsOperation == null)
      {
        return false;
      }

      ChipsOperation.IsChipsSaleRegister = IsChipsSaleRegister;
      ChipsOperation.PrintTicket = ChipsOperation.ChipsDifference > 0 ? true : false;
      CashOperation.amount_to_pay = ChipsOperation.AmountNationalCurrency;

      // Add total operation amount to add amount to account including tito ticket
      if (ChipsOperation.PrintTicket)
      {
        CashOperation.amount_to_pay += ChipsOperation.ChipsDifference;
      }

      return true;
    }

    public static Boolean RequestForRefund(FeatureChips.ChipsOperation ChipsAmount, CashierOperations.TYPE_CARD_CASH_IN CashOperation, Form Parent, CurrencyExchangeResult ExchangeResult)
    {
      DialogResult _devol_type;
      PrinterStatus _status;
      String _status_text;
      Boolean _amount_exceed;
      Currency _cashier_current_balance;
      Boolean _pay_refund_in_cash;
      Boolean _cash_enabled;

      try
      {
        // JBP 15-JAN-2014 - Request for refund type: Cash or Ticket
        ChipsAmount.PrintTicket = false;
        _amount_exceed = false;
        _cash_enabled = true;

        // Only check if cashier has cash when client pay in foreing currency
        if (ExchangeResult != null)
        {
          using (DB_TRX _db_trx = new DB_TRX())
          {
            _cashier_current_balance = CashierBusinessLogic.UpdateSessionBalance(_db_trx.SqlTransaction, Cashier.SessionId, 0);
            _amount_exceed = (_cashier_current_balance < ChipsAmount.ChipsDifference);
          }

          //If currency exchange type is card, refounds with tickts TITO only. 
          _cash_enabled = (ExchangeResult.InType != CurrencyExchangeType.CARD);
        }

        if (ChipsAmount.AmountNationalCurrencyDifference > 0)
        {
          if (CashOperation.reward <= 0)
          {
            do
            {
              _devol_type = frm_message.ShowRefundSelect(Parent, ChipsAmount.AmountNationalCurrencyDifference, _cash_enabled);
              _pay_refund_in_cash = _devol_type == DialogResult.No;

              if (_amount_exceed && _pay_refund_in_cash)
              {
                frm_message.Show(Resource.String("STR_FRM_AMOUNT_INPUT_MSG_AMOUNT_EXCEED_BANK"),
                                 Resource.String("STR_APP_GEN_MSG_WARNING"),
                                 MessageBoxButtons.OK,
                                 MessageBoxIcon.Warning,
                                 Parent);
              }
            } while (_amount_exceed && _pay_refund_in_cash);

            // cancel operation
            if (_devol_type == DialogResult.Cancel)
            {
              frm_message.Show(Resource.String("STR_REFUND_CANCEL_OPERATION"),
                               Resource.String("STR_APP_GEN_MSG_WARNING"),
                               MessageBoxButtons.OK,
                               MessageBoxIcon.Warning,
                               Parent);

              return false;
            }

            ChipsAmount.PrintTicket = (_devol_type == DialogResult.OK);
          }
          else
          {
            ChipsAmount.PrintTicket = true;
          }

          // Check for TITO printer
          if (ChipsAmount.PrintTicket)
          {
            if (!TitoPrinter.IsReady())
            {
              TitoPrinter.GetStatus(out _status, out _status_text);
              _status_text = TitoPrinter.GetMsgError(_status);

              frm_message.Show(_status_text,
                               Resource.String("STR_APP_GEN_MSG_ERROR"),
                               MessageBoxButtons.OK,
                               MessageBoxIcon.Error,
                               Parent);

              return false;
            }
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // RequestForRefund

    private static bool Internal_ChipsSale_Save(CardData Card, OperationCode operationCode, Form Parent, FeatureChips.ChipsOperation ChipsOperation, CashierOperations.TYPE_CARD_CASH_IN CashOperation,
      CurrencyExchangeResult ExchangeResult, ChipSaleType SaleType, PaymentThresholdAuthorization PaymentThresholdAuthorization, out string ErrorStr, out RechargeOutputParameters OutputParameter, ParticipateInCashDeskDraw _participate_in_cash_draw, out Ticket TicketCopyDealer)
    {
      ParamsTicketOperation _tickets;

      _tickets = new ParamsTicketOperation();

      return Internal_ChipsSale_Save(Card, operationCode, Parent, ChipsOperation, CashOperation, ExchangeResult, PaymentThresholdAuthorization, SaleType, _tickets, out ErrorStr, out OutputParameter, _participate_in_cash_draw, out TicketCopyDealer);
    }

    private static Boolean Internal_ChipsSale_Save(CardData Card, OperationCode OpCode, Form Parent,
                                                   FeatureChips.ChipsOperation ChipsAmount, CashierOperations.TYPE_CARD_CASH_IN CashOperation,
                                                   CurrencyExchangeResult ExchangeResult, PaymentThresholdAuthorization PaymentThresholdAuthorization, ChipSaleType SaleType, ParamsTicketOperation Tickets,
                                                   out String ErrorStr, out RechargeOutputParameters OutputParameter, ParticipateInCashDeskDraw _participate_in_cash_draw, out Ticket TicketCopyDealer)
    {
      ArrayList _voucher_list;
      ArrayList _voucher_list_tpv;

      CashierSessionInfo _cashier_session_info;
      Boolean _is_tito_mode;
      Ticket _ticket;
      Boolean _is_chips_sale_register;

      // ETP: PinPad vars:
      Boolean _return_error;
      Boolean _is_pinpad_recharge;
      frm_tpv _form_tpv;
      PinPadInput _pinpad_input;
      PinPadOutput _pinpad_output;
      Int64 _operation_id;
      ResultGetMarker _result_get_marker;
      String _error_str;

      _operation_id = 0;
      _error_str = String.Empty;

      _pinpad_output = new PinPadOutput();

      _is_chips_sale_register = (SaleType == ChipSaleType.RegisterTableSale);

      _is_tito_mode = Utils.IsTitoMode();
      _cashier_session_info = Cashier.CashierSessionInfo();
      OutputParameter = new RechargeOutputParameters();



      _ticket = null;

      ErrorStr = String.Empty;
      _voucher_list = new ArrayList();
      _voucher_list_tpv = new ArrayList();

      _is_pinpad_recharge = Misc.IsPinPadEnabled() && Common.Cashier.ReadPinPadEnabled() && ExchangeResult != null && ExchangeResult.InType == CurrencyExchangeType.CARD;

      TicketCopyDealer = null;

      if (_is_pinpad_recharge)
      {
        if (!InternetConnection.GetServiceInternetConnectionStatus("WCP"))
        {
          ErrorStr = Resource.String("STR_PINPAD_CANCELLED_OPERATION_ERROR_CONNEXION");
          return false;
        }

        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (!BD_Internal_ChipsSale_Save(Card, OpCode, ChipsAmount, CashOperation,
                                                     ExchangeResult, PaymentThresholdAuthorization, SaleType, Tickets, _operation_id,
                                                     out  ErrorStr, out  OutputParameter, _participate_in_cash_draw, _db_trx.SqlTransaction, out _voucher_list, out TicketCopyDealer)) // Check if operation can be done.
          {
            _db_trx.Rollback();
            return false;
          }
          _db_trx.Rollback();
        }
        _pinpad_input = new PinPadInput();
        _pinpad_input.Amount = ExchangeResult;
        _pinpad_input.Card = Card;
        _pinpad_input.TerminalName = _cashier_session_info.TerminalName;

        _form_tpv = new frm_tpv(_pinpad_input);
        if (!_form_tpv.Show(out _pinpad_output)) // 20/10/2016 ETP: Pin Pad payment
        {
          if (_form_tpv.OperationCanceled)
          {
            ErrorStr = Resource.String("STR_PINPAD_CANCELED_BY_USER");
          }
          else
          {
            ErrorStr = _pinpad_output.PinPadTransaction.ErrorMessage;
          }
          _form_tpv.Dispose();
          return false;
        }
        _form_tpv.Dispose();
        // 20/10/2016 ETP: Transaction is Pending of udpating wigos data.
      }

      _return_error = false;

      using (DB_TRX _db_trx = new DB_TRX())
      {

        #region " CREDIT LINE "

        //Currency exchange CREDIT LINE
        if (ExchangeResult != null && ExchangeResult.InType == CurrencyExchangeType.CREDITLINE)
        {
          _result_get_marker = WSI.Cashier.CreditLines.CreditLine.getMarker(Card, CashOperation.amount_to_pay, Parent,
                                                                            _cashier_session_info, CreditLineMovements.CreditLineMovementType.GetMarkerChips,
                                                                            _db_trx.SqlTransaction, out _operation_id);
          if (_result_get_marker != ResultGetMarker.OK)
          {
            OutputParameter.ErrorCode = RECHARGE_ERROR_CODE.ERROR_RECHARGE_WITH_CREDIT_LINE;
            return false;
          }
        }

        #endregion " CREDIT LINE "

        if (BD_Internal_ChipsSale_Save(Card, OpCode, ChipsAmount, CashOperation,
                                                   ExchangeResult, m_payment_threshold_authorization, SaleType, Tickets, _operation_id,
                                                   out  ErrorStr, out  OutputParameter, _participate_in_cash_draw, _db_trx.SqlTransaction, out _voucher_list, out TicketCopyDealer))
        {
          if (_is_pinpad_recharge)
          {
            PinPadTransactions _transaction;

            _transaction = _pinpad_output.PinPadTransaction;

            _transaction.OperationId = OutputParameter.OperationId;
            _transaction.StatusCode = PinPadTransactions.STATUS.APPROVED;  //  20/10/2016 ETP: PinPad is payed OK.

            if (!_transaction.DB_Insert(_db_trx.SqlTransaction))
            {
              _return_error = true;
            }

            _transaction.UpdateCashierSessionHasPinPadOperation(_cashier_session_info.CashierSessionId, _db_trx.SqlTransaction);

            if (!PinPadTransactions.DeleteFromUndoPinPad(_transaction.ControlNumber, _db_trx.SqlTransaction))
            {
              _return_error = true;
            }

            if (!_pinpad_output.PinPadTransaction.CreateVoucher(ExchangeResult, _pinpad_output, _db_trx.SqlTransaction, out _voucher_list_tpv))
            {
              _return_error = true;
            }

            _voucher_list.AddRange(_voucher_list_tpv);
          }
        }
        else
        {
          _return_error = true;
        }

        if (!_return_error)
        {
          _db_trx.Commit();
        }

      } // DB_TRX _db_trx


      if (_return_error)
      {
        // 20/10/2016 ETP: Undo PinPad Operations if and error has ocurred:
        if (_is_pinpad_recharge)
        {
          using (DB_TRX _db_trx = new DB_TRX())
          {
            _pinpad_output.PinPadTransaction.OperationId = OutputParameter.OperationId;
            _pinpad_output.PinPadTransaction.StatusCode = PinPadTransactions.STATUS.ERROR;
            _pinpad_output.PinPadTransaction.DB_Insert(_db_trx.SqlTransaction);
            _pinpad_output.PinPadTransaction.UpdateCashierSessionHasPinPadOperation(_cashier_session_info.CashierSessionId, _db_trx.SqlTransaction);
            PinPadTransactions.UpdateDateUndoPinPad(_pinpad_output.PinPadTransaction.ControlNumber, _db_trx.SqlTransaction);
            _db_trx.Commit();
          }
        }

        return false;
      }

      if (!(SaleType == ChipSaleType.RegisterTableSale))
      {
        // JBP 15-JAN-2014: Print TITO Ticket
        if (OutputParameter != null && OutputParameter.TicketCreated != null)
        {
          if (!TitoPrinter.PrintTicket(OutputParameter.TicketCreated.ValidationNumber, OutputParameter.TicketCreated.MachineTicketNumber, OutputParameter.TicketCreated.TicketType, OutputParameter.TicketCreated.Amount, OutputParameter.TicketCreated.CreatedDateTime, OutputParameter.TicketCreated.ExpirationDateTime, _cashier_session_info))
          {
            Log.Error("Internal_ChipSale_Save: PrintTicket. Error on printing ticket.");

            PrinterStatus _status;
            String _status_text;

            TitoPrinter.GetStatus(out _status, out _status_text);
            _status_text = TitoPrinter.GetMsgError(_status);

            frm_message.Show(_status_text + "\r\n" + Resource.String("STR_FRM_CONTAINER_EVENTS_DESC_PRINTER_CANCEL_OPERATION"),
                             Resource.String("STR_APP_GEN_MSG_ERROR"),
                             MessageBoxButtons.OK,
                             MessageBoxIcon.Error,
                             Parent);
            // 'return true': Sale Chip operation is correctly, but printing fail and Transaction is commited. 
            // The user must cancel last operation.
          }
        }
      } // if (!_is_chips_sale_register)


      VoucherPrint.Print(_voucher_list);

      return true;
    }// ChipsSale_Save


    private static Boolean BD_Internal_ChipsSale_Save(CardData Card, OperationCode OpCode,
                                                  FeatureChips.ChipsOperation ChipsAmount, CashierOperations.TYPE_CARD_CASH_IN CashOperation,
                                                  CurrencyExchangeResult ExchangeResult, PaymentThresholdAuthorization PaymentThresholdAuthorization, ChipSaleType SaleType, ParamsTicketOperation Tickets, Int64 OperationId,
                                                  out String ErrorStr, out RechargeOutputParameters OutputParameter, ParticipateInCashDeskDraw _participate_in_cash_draw,
                                                  SqlTransaction Trx, out ArrayList VoucherList, out Ticket TicketCopyDealer)
    {
      Boolean _credit_allowed_reached;
      Currency _credit_allowed_sale;
      Int64 _operation_id;
      ArrayList _chips_voucher_list;
      GamingTablesSessions _gt_session;
      GamingTablesSessions _gt_session_own;
      GTS_UPDATE_TYPE _gts_update_type;
      CashierSessionInfo _cashier_session_info;
      Boolean _is_tito_mode;
      ParamsTicketOperation _params_ticket_operation;
      Ticket _ticket;
      Boolean _is_chips_sale_register;
      String _holder_name;
      IDictionary<long, decimal> _tickets_swap;
      ArrayList _handpayVouchers;
      List<long> _listTicket;
      Currency _chip_prize;
      JunketsShared _junket_shared;

      String _alarm_description;
      Currency _purchase_amount_limit;

      _junket_shared = new JunketsShared();

      _is_chips_sale_register = (SaleType == ChipSaleType.RegisterTableSale);

      _is_tito_mode = Utils.IsTitoMode();
      _cashier_session_info = Cashier.CashierSessionInfo();
      OutputParameter = new RechargeOutputParameters();

      _operation_id = OperationId;

      _ticket = null;
      _gt_session = null;

      ErrorStr = String.Empty;
      VoucherList = new ArrayList();

      _chip_prize = 0;
      
      // Copy Dealer Ticket
      TicketCopyDealer = null;

      if (CashOperation.junketinfo != null)
      {

        _junket_shared = new JunketsShared()
        {
          BusinessLogic = CashOperation.junketinfo
        };

        if (!_junket_shared.ProcessJunketToAccount(OutputParameter.OperationId, Trx))
        {
          ErrorStr = Resource.String("STR_JUNKET_ERROR");

          return false;
        }
      }

      switch (OpCode)
      {
        case OperationCode.CHIPS_SALE_WITH_RECHARGE:
        case OperationCode.CHIPS_SWAP:
          //Insert data into ACCOUNT_OPERATIONS table
          if (!Accounts.DB_CardCreditAdd(Card,
                                         CashOperation.amount_to_pay,
                                         CashOperation.promotion_id,
                                         CashOperation.reward,
                                         CashOperation.won_lock,
                                         CashOperation.spent_used,
                                         ExchangeResult,
                                         _participate_in_cash_draw,
                                         OpCode,
                                         _is_chips_sale_register,
                                         OperationId,
                                         null,
                                         _cashier_session_info,
                                         Trx,
                                         CashOperation.junketinfo,
                                         out OutputParameter))
          {
            Log.Error(string.Format("btn_SaleChips_WithRecharge_Click: DB_AddCreditToCard. Error adding credit to card. AccountId: {0}. Amount: {1}",
                                    Card.AccountId, CashOperation.amount_to_pay));

            _credit_allowed_reached = OutputParameter.SalesLimitReached;
            _credit_allowed_sale = OutputParameter.RemainingSalesLimit;

            // JML 12-NOV-2013 This error is move because now the function return false, when the credit allowed is reached
            if (_credit_allowed_reached)
            {
              // JBP 17-JAN-2014: Show error out of transaction.
              ErrorStr = Resource.String("STR_FRM_CASHIER_LIMIT_SALES_REACHED") + _credit_allowed_sale;
              ErrorStr = ErrorStr.Replace("\\r\\n", "\r\n");

              return false;
            }

            // JML 02-DIC-2014 Added errors to msgbox for failed recharges
            if (Card.PlayerTracking.CardLevel != 0)
            {
              _holder_name = Card.PlayerTracking.HolderName;
            }
            else
            {
              _holder_name = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_ACCOUNT_MODE_ANONYMOUS");
            }



            switch (OutputParameter.ErrorCode)
            {
              case RECHARGE_ERROR_CODE.ERROR_ACCOUNT_GLOBAL_MAX_ALLOWED_CASH_IN_COUNT_EXCEEDED:
                ErrorStr = Resource.String("STR_ACCOUNT_GLOBAL_MAX_ALLOWED_CASH_IN_COUNT_EXCEEDED_ERROR", Card.AccountId, _holder_name, CashOperation.amount_to_pay.ToString());

                break;
              case RECHARGE_ERROR_CODE.ERROR_ACCOUNT_GLOBAL_MAX_ALLOWED_CASH_IN_AMOUNT_EXCEEDED:
                ErrorStr = Resource.String("STR_ACCOUNT_GLOBAL_MAX_ALLOWED_CASH_IN_AMOUNT_EXCEEDED_ERROR", Card.AccountId, _holder_name, CashOperation.amount_to_pay.ToString());

                break;
              case RECHARGE_ERROR_CODE.ERROR_ACCOUNT_DAILY_MAX_ALLOWED_CASH_IN_COUNT_EXCEEDED:
                ErrorStr = Resource.String("STR_ACCOUNT_DAILY_MAX_ALLOWED_CASH_IN_COUNT_EXCEEDED_ERROR", Card.AccountId, _holder_name, CashOperation.amount_to_pay.ToString());

                break;
              case RECHARGE_ERROR_CODE.ERROR_ACCOUNT_DAILY_MAX_ALLOWED_CASH_IN_AMOUNT_EXCEEDED:
                ErrorStr = Resource.String("STR_ACCOUNT_DAILY_MAX_ALLOWED_CASH_IN_AMOUNT_EXCEEDED_ERROR", Card.AccountId, _holder_name, CashOperation.amount_to_pay.ToString());

                break;
              case RECHARGE_ERROR_CODE.ERROR_RECHARGE_WITH_CASH_CLOSED:
                ErrorStr += Resource.String("STR_UC_CARD_RECHARGE_ERROR_CASH_CLOSED", Card.AccountId, _holder_name, CashOperation.amount_to_pay.ToString());

                break;
              default:
                if (!String.IsNullOrEmpty(OutputParameter.ErrorMessage))
                {
                  ErrorStr = OutputParameter.ErrorMessage + "\\r\\n" + "\\r\\n";
                }

                ErrorStr += Resource.String("STR_UC_CARD_RECHARGE_ERROR", Card.AccountId, _holder_name, CashOperation.amount_to_pay.ToString());

                break;
            }

            ErrorStr = ErrorStr.Replace("\\r\\n", "\r\n");

            return false;
          }


          if (OpCode != OperationCode.CHIPS_SWAP)
          {
            VoucherList = OutputParameter.VoucherList;
          }

          _operation_id = OutputParameter.OperationId;

          if (OpCode == OperationCode.CHIPS_SALE_WITH_RECHARGE && PaymentThresholdAuthorization != null && PaymentThresholdAuthorization.PaymentAndRechargeThresholdData != null && PaymentThresholdAuthorization.InputThreshold > 0
            && ChipsAmount != null && ChipsAmount.AmountNationalCurrency >= PaymentThresholdAuthorization.InputThreshold)
          {
            PaymentThresholdAuthorization.SaveThresholdOperation(_operation_id, OpCode, Trx);
          }

          break;

        case OperationCode.CHIPS_SALE:
          VoucherList = new ArrayList();

          if (!Operations.DB_InsertOperation(OpCode,
                                             Card.AccountId,
                                             Cashier.SessionId,
                                             0,
                                             ChipsAmount.ChipsAmount,
                                             0,
                                             0,
                                             0,
                                             0,
                                             0,                      // Operation Data
                                             ChipsAmount.IsChipsSaleRegister,
                                             ChipsAmount.AccountOperationReasonId,
                                             ChipsAmount.AccountOperationComment,
                                             String.Empty, //EOR 24-FEB-2016 ADD COMMENTS HANDPAY DEFAULT VALUE
                                             out _operation_id,
                                             out ErrorStr,
                                             Trx))
          {
            return false;
          }

          break;

        default:
          return false;
      }


      if (ChipsAmount != null && ChipsAmount.AmountNationalCurrency > 0 && (OpCode == OperationCode.CHIPS_SALE || OpCode == OperationCode.CHIPS_SALE_WITH_RECHARGE))
      {
        // Alarm: Alarm for Amount purchased in one operation
        _purchase_amount_limit = GamingTableAlarms.GetAmountPurchaseInOneOperation();

        if (_purchase_amount_limit > 0 && _purchase_amount_limit <= ChipsAmount.AmountNationalCurrency)
        {
          // Insert Alarm
          _alarm_description = Resource.String("STR_ALARM_GAMBLING_TABLES_AMOUNT_PURCHASE_IN_ONE_OPERATION", ChipsAmount.AmountNationalCurrency);
          Alarm.Register(AlarmSourceCode.Cashier,
                        _cashier_session_info.UserId,
                        _cashier_session_info.UserName + "@" + _cashier_session_info.TerminalName,
                        (Int32)AlarmCode.User_GamingTables_AmountPurchaseInOneOperation,
                        _alarm_description,
                        AlarmSeverity.Warning,
                        WGDB.Now, Trx);
        }
        // End Alarm: Alarm for Amount purchased in one operation
      }



      // Update Gaming Table Sessions
      // 25-JUN-2015 added for manual conciliation chips sales
      if (!_is_chips_sale_register && GamingTablesSessions.GamingTableInfo.GamingTableId > 0)// && !GamingTableBusinessLogic.IsEnableManualConciliation())
      {
        if (!GamingTablesSessions.GetOrOpenSession(out _gt_session
                                                  , ChipsAmount.SelectedGamingTable.TableId
                                                  , ChipsAmount.SelectedGamingTable.CashierSession
                                                  , Trx))
        {
          return false;
        }

        ChipsAmount.SelectedGamingTable.GamingTableSession = _gt_session.GamingTableSessionId;
      }

      _tickets_swap = GetDictionarySwap(Tickets);

      // LTC 13-OCT-2016
      if (OutputParameter.CashDeskDrawResult != null)
      {
        ChipsAmount.CashDeskDrawResult = OutputParameter.CashDeskDrawResult;
        _chip_prize = OutputParameter.CashDeskDrawResult.m_prize_category.m_fixed;
      }

      if (GamingTableBusinessLogic.IsEnableRoundingSaleChips() && !WSI.Common.TITO.Utils.IsTitoMode() && OutputParameter.CashDeskDrawResult != null)
      {
        RoundingSaleChips _temp_rounding_sales_chips;

        _temp_rounding_sales_chips = new RoundingSaleChips(_chip_prize, true);

        if (_temp_rounding_sales_chips.CalculateResult != RoundingSaleChips.GT_CALCULATE_ROUNDED_AMOUNT_RESULT.UNDEFINED
           && _temp_rounding_sales_chips.CalculateResult != RoundingSaleChips.GT_CALCULATE_ROUNDED_AMOUNT_RESULT.ROUND_NOT_APPLIED)
        {
          ChipsAmount.RoundingSaleChips.RemainingAmount += _temp_rounding_sales_chips.RemainingAmount;
        }

        _temp_rounding_sales_chips = new RoundingSaleChips(ChipsAmount.RoundingSaleChips.RemainingAmount + Card.AccountBalance.Account_ChipsSalesRemainingAmount, true);
        if (ChipsAmount.RoundingSaleChips.RemainingAmount < _temp_rounding_sales_chips.MinimiumDenomination)
        {
          if (_temp_rounding_sales_chips.CalculateResult == RoundingSaleChips.GT_CALCULATE_ROUNDED_AMOUNT_RESULT.OK)
          {
            ChipsAmount.FillChipsAmount += _temp_rounding_sales_chips.RoundedAmount - ChipsAmount.DevolutionRoundingSaleChips;
            ChipsAmount.ChipsAmount += _temp_rounding_sales_chips.RoundedAmount - ChipsAmount.DevolutionRoundingSaleChips;
            ChipsAmount.DevolutionRoundingSaleChips = _temp_rounding_sales_chips.RoundedAmount;
          }
        }
      }

        if (GeneralParam.GetBoolean("GamingTables", "Cashier.Mode"))
        {
        if (OutputParameter.CashDeskDrawResult != null)
        {
          ChipsAmount.FillChipsAmount += _chip_prize;
          ChipsAmount.ChipsAmount += _chip_prize;
        }

        if (OpCode == OperationCode.CHIPS_SALE_WITH_RECHARGE && !WSI.Common.TITO.Utils.IsTitoMode())
        {
          ChipsAmount.RoundingSaleChips = new RoundingSaleChips(ChipsAmount.ChipsAmount, true);

          if (ChipsAmount.RoundingSaleChips.CalculateResult != RoundingSaleChips.GT_CALCULATE_ROUNDED_AMOUNT_RESULT.OK)
          {
            if (ChipsAmount.RoundingSaleChips.CalculateResult == RoundingSaleChips.GT_CALCULATE_ROUNDED_AMOUNT_RESULT.AMOUNT_LESS_THAN_MINIMUM_DENOMINATION)
            {
              // show message when enter amount less than minimun denomination
              Log.Error(Resource.String("STR_ROUNDING_SALE_CHIPS_AMOUNT_LESS_THAN_MINIMUM_DENOMINATION")); //, MessageBoxButtons.OK, MessageBoxIcon.Error, this);
              Log.Error(Resource.String("STR_APP_GEN_MSG_ERROR")); //, MessageBoxButtons.OK, MessageBoxIcon.Error, this);
            }
            return false;
          }
          ChipsAmount.ChipsAmount = ChipsAmount.RoundingSaleChips.RoundedAmount;
          ChipsAmount.FillChipsAmount = ChipsAmount.RoundingSaleChips.RoundedAmount;
        }
      }

      if (!FeatureChips.Sale(ChipsAmount, _operation_id, Card, _is_tito_mode,
                            _cashier_session_info, out _chips_voucher_list,
                            _tickets_swap, Trx, OpCode, _chip_prize, CashOperation.amount_to_pay, out TicketCopyDealer, out ErrorStr))
      {
        return false;
      }

      // Add Any Flyer Voucher Scanned with the Chip Sale to the Chips Voucher List
      if (_junket_shared.BusinessLogic != null)
      {
        if (CashOperation.junketinfo.PrintVoucher)
        {

          VoucherJunket _junket_voucher;

          _junket_voucher = _junket_shared.CreateVoucher(_operation_id, Card, PrintMode.Print, Trx);

          _chips_voucher_list.Add(_junket_voucher);
        }

      }

      if (!_is_chips_sale_register)
      {
        // JBP 15-JAN-2014: Create TITO Ticket
        if (ChipsAmount.PrintTicket)
        {
          // Print TicketOut
          _params_ticket_operation = new ParamsTicketOperation();
          _params_ticket_operation.out_cash_amount = ChipsAmount.AmountNationalCurrencyDifference;
          _params_ticket_operation.in_cash_amt_0 = ChipsAmount.AmountNationalCurrencyDifference;     //LTC 02-JUN-2016
          _params_ticket_operation.ticket_type = TITO_TICKET_TYPE.CASHABLE;
          _params_ticket_operation.in_account = Card;
          _params_ticket_operation.out_promotion_id = 0;
          _params_ticket_operation.session_info = _cashier_session_info;
          _params_ticket_operation.out_operation_id = _operation_id;
          _params_ticket_operation.is_swap = (OpCode == OperationCode.CHIPS_SWAP) ? true : false;
          // DLL 20-MAR-2014: Terminal_id has to be set when the terminal is a gaming terminal. When it's a cashier terminal, don't have to set it.
          _params_ticket_operation.terminal_id = 0;

          if (!BusinessLogic.CreateTicket(_params_ticket_operation, true, out _ticket, Trx))
          {
            Log.Error("ChipsPurchase: BusinessLogic.CreateTicket. Error creating ticket.");

            return false;
          }

          OutputParameter.TicketCreated = _ticket;
        }

        if (OpCode != OperationCode.CHIPS_SWAP)
        {
          // Create currency exchange promotional ticket
          if (_is_tito_mode && ExchangeResult != null && ExchangeResult.ArePromotionsEnabled && ExchangeResult.NR2Amount > 0)
          {
            // Print TicketOut
            _params_ticket_operation = new ParamsTicketOperation();
            _params_ticket_operation.out_promotion_id = 0;
            _params_ticket_operation.out_cash_amount = ExchangeResult.NR2Amount;
            _params_ticket_operation.ticket_type = TITO_TICKET_TYPE.PROMO_NONREDEEM;
            _params_ticket_operation.in_account = Card;
            _params_ticket_operation.session_info = _cashier_session_info;
            _params_ticket_operation.out_operation_id = _operation_id;
            // DLL 20-MAR-2014: Terminal_id has to be set when the terminal is a gaming terminal. When it's a cashier terminal, don't have to set it.
            _params_ticket_operation.terminal_id = 0;

            if (!BusinessLogic.CreateTicket(_params_ticket_operation, true, out _ticket, Trx))
            {
              Log.Error("ChipsPurchase: BusinessLogic.CreateTicket. Error creating promotional ticket.");

              return false;
            }
          }
        }

        if (GamingTablesSessions.GamingTableInfo.IsGamingTable)
        {
          if (ChipsAmount.SelectedGamingTable.CashierSession == _cashier_session_info.CashierSessionId)
          {
            _gts_update_type = GTS_UPDATE_TYPE.ChipsOwnSales;
          }
          else
          {
            _gts_update_type = GTS_UPDATE_TYPE.ChipsExternalSales;
          }

          if (!_gt_session.UpdateSession(_gts_update_type, ChipsAmount.ChipsAmount, new CurrencyIsoType(ChipsAmount.IsoCode, ChipsAmount.CurrencyExchangeType), Trx))
          {
            return false;
          }

          if (!_gt_session.UpdateSession(GTS_UPDATE_TYPE.Visits, 1, Trx))
          {
            return false;
          }
            if (_gts_update_type == GTS_UPDATE_TYPE.ChipsExternalSales)
            {
              if (!GamingTablesSessions.GetOrOpenSession(out _gt_session_own
                                                        , GamingTablesSessions.GamingTableInfo.GamingTableId
                                                        , _cashier_session_info.CashierSessionId
                                                        , Trx))
              {
                return false;
              }

              if (!_gt_session_own.UpdateSession(GTS_UPDATE_TYPE.ChipsTotalSales, ChipsAmount.ChipsAmount, new CurrencyIsoType(ChipsAmount.IsoCode, ChipsAmount.CurrencyExchangeType), Trx))
              {
                return false;
              }

              if (!_gt_session_own.UpdateSessionLastSessionId(GTS_UPDATE_TYPE.LastExternalSale, _gt_session.CashierSessionId, Trx))
              {
                return false;
              }

            } // ChipsExternalSales
            else
            {
              if (!_gt_session.UpdateSessionLastSessionId(GTS_UPDATE_TYPE.LastExternalSale, null, Trx))
              {
                return false;
              }
            }
          } // IsGamingTable

        if (OpCode == OperationCode.CHIPS_SWAP)
        {
          _listTicket = new List<long>();

          foreach (var _item in Tickets.validation_numbers)
          {
            _listTicket.Add(_item.TicketId);
          }

          Tickets.out_operation_id = _operation_id;
          if (!BusinessLogic.RedeemTicketList(_listTicket, OpCode, ref  Tickets, out _handpayVouchers, Trx))
          {
            return false;
          }
        }
      } // if (!_is_chips_sale_register)


      foreach (Voucher _voucher in _chips_voucher_list)
      {
        VoucherList.Add(_voucher);
      }

      return true;
    }// ChipsSale_Save

    private static IDictionary<long, decimal> GetDictionarySwap(ParamsTicketOperation ParamsTicket)
    {
      IDictionary<long, decimal> _dic = new Dictionary<long, decimal>();

      if (ParamsTicket.validation_numbers != null)
      {
        foreach (var _item in ParamsTicket.validation_numbers)
        {
          _dic.Add(_item.TicketId, _item.Amount);
        }
      }

      return _dic;
    }

    #endregion // Private Methods

  } // CashierChips

}
