﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_add_account_blacklist.cs
// 
//   DESCRIPTION: Implements add account to local blacklist functionality. 
//
//        AUTHOR: Ariel Vazquez Zerpa
// 
// CREATION DATE: 11-FEB-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 11-FEB-2015 AVZ    Product Backlog Item 9046:Visitas / Recepción: BlackLists
// 27-MAY-2016 FJC    Bug 13766:Recepción: Teclado táctil no disponible cuando queremos escribir una descripción en la ficha "Lista de prohibidos" del cliente.
// 14-JUL-2016 PDM    PBI 15448:Visitas / Recepción: MEJORAS - Cajero & GUI - Nuevo motivo de bloqueo
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WSI.Cashier.Controls;
using WSI.Common;
using WSI.Common.Entrances;
using WSI.Common.Entrances.Enums;
using WSI.Common.Utilities;

namespace WSI.Cashier
{
  public partial class frm_add_account_blacklist : frm_base
  {
    #region Properties

    public DateTime InclusionDate
    {
      get
      {
        return m_inclusion_date;
      }
    }

    public ENUM_BLACKLIST_REASON Reason
    {
      get
      {
        return m_reason;
      }
    }

    public string ReasonDescription
    {
      get
      {
        return m_reason_description;
      }
    }

    #endregion

    #region Members

    private frm_yesno form_yes_no;
    private long m_account_id;
    private DateTime m_inclusion_date;
    private ENUM_BLACKLIST_REASON m_reason;
    private string m_reason_description;

    #endregion

    public frm_add_account_blacklist()
    {
      InitializeComponent();
      InitializeControlResources();
    }

    /// <summary>
    /// Initialize resources (NLS, etc)
    /// </summary>
    private void InitializeControlResources()
    {
      // NLS Strings    
      this.FormTitle = Resource.String("STR_FRM_ADD_ACCOUNT_BLACKLIST_TITLE");
      this.btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");
      this.btn_ok.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_OK");
      this.lbl_blacklist_reason.Text = Resource.String("STR_FRM_ADD_ACCOUNT_BLACKLIST_REASON");
      this.lbl_blacklist_reason_description.Text = Resource.String("STR_FRM_ADD_ACCOUNT_BLACKLIST_REASON_DESCRIPTION");
    }

    private void btn_ok_Click(object sender, EventArgs e)
    {

      lbl_msg_blink.Visible = false;
      CardData _card;
      AccountBlockUnblock _account_block_unblock;

      _card = new CardData();


      try
      {
        if (Validate())
        {

          if (!CardData.DB_CardGetAllData(m_account_id, _card))
          {
            return;
          }

          m_reason = (ENUM_BLACKLIST_REASON)cb_blackList_reasons.SelectedValue;
          m_reason_description = txt_blacklist_reason_description.Text.Trim();
          m_inclusion_date = Misc.TodayOpening();


          _account_block_unblock = new AccountBlockUnblock(_card, AccountBlockUnblockSource.RECEPTION_BLACKLIST, m_reason_description, true);

          try
          {
            using (DB_TRX _db_trx = new DB_TRX())
            {

              CardData _card_data;

              if (_account_block_unblock.ProceedToBlockUnblockAccount(Cashier.CashierSessionInfo(), true, _db_trx.SqlTransaction, out _card_data))
              {
                Customers.AddToBlacklist(_card_data, Cashier.CashierSessionInfo(),  Misc.TodayOpening(), m_reason, m_reason_description, _db_trx.SqlTransaction);
                _account_block_unblock.AddCashierMovementsBlacklist(Cashier.CashierSessionInfo(), CASHIER_MOVEMENT.ACCOUNT_ADD_BLACKLIST, _db_trx.SqlTransaction);
                _db_trx.Commit();
              }
            }

            Customers.RefreshBlackListCache(WSI.Common.Entrances.Enums.ENUM_BLACKLIST_SOURCE.LOCAL);
          }
          catch (Exception _ex)
          {
            Log.Exception(_ex);
          }

          this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }
      }
      catch (Exception)
      {
        frm_message.Show(Resource.String("STR_APP_GEN_ERROR_DATABASE_ERROR_WRITING"),
          Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, Images.CashierImage.Error, this);
      }
    }


    /// <summary>
    /// Validate controls data
    /// </summary>
    /// <returns></returns>
    private new bool Validate()
    {
      if (this.cb_blackList_reasons.SelectedValue == null)
      {
        lbl_msg_blink.Text = Resource.String("STR_FRM_ADD_ACCOUNT_BLACKLIST_ERROR_REASON");
        lbl_msg_blink.Visible = true;
        return false;
      }

      return true;
    }

    public DialogResult Show(long AccountId)
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_add_account_blacklist", Log.Type.Message);
      m_account_id = AccountId;
      using (form_yes_no = new frm_yesno())
      {
        form_yes_no.Opacity = 0.6;
        form_yes_no.Show();

        // Center screen
        if (WindowManager.StartUpMode == WindowManager.StartUpModes.Explorer)
        {
          this.Location = new Point(WindowManager.GetFormCenterLocation(form_yes_no).X - (this.Width / 2)
                                    , 0);
        }
        else
        {
          this.Location = new Point(1024 / 2 - this.Width / 2, 0);
        }

        WSIKeyboard.Show();

        LoadBlackListReasons();

        this.ShowDialog(form_yes_no);

        form_yes_no.Hide();

        return this.DialogResult;
      }
    }

    private void LoadBlackListReasons()
    {

      var _list = new List<KeyValuePair<Enum, string>>();
      _list.Add(new KeyValuePair<Enum, string>(ENUM_BLACKLIST_REASON.ALCOHOLIC, Resource.String("STR_ENUM_BLACKLIST_REASON_ALCOHOLIC")));
      _list.Add(new KeyValuePair<Enum, string>(ENUM_BLACKLIST_REASON.COMPULSIVE_GAMBLER, Resource.String("STR_ENUM_BLACKLIST_REASON_COMPULSIVE_GAMBLER")));
      _list.Add(new KeyValuePair<Enum, string>(ENUM_BLACKLIST_REASON.IN_DEBT, Resource.String("STR_ENUM_BLACKLIST_REASON_IN_DEBT")));
      _list.Add(new KeyValuePair<Enum, string>(ENUM_BLACKLIST_REASON.BLOCK, Resource.String("STR_ENUM_BLACKLIST_REASON_BLOCK")));
      _list.Add(new KeyValuePair<Enum, string>(ENUM_BLACKLIST_REASON.OTHER, Resource.String("STR_ENUM_BLACKLIST_REASON_OTHER")));

      this.cb_blackList_reasons.DataSource = _list;
      this.cb_blackList_reasons.DisplayMember = "Value";
      this.cb_blackList_reasons.ValueMember = "Key";
    }

    private void btn_keyboard_Click(object sender, EventArgs e)
    {
      WSIKeyboard.ForceToggle();
    }
  }
}
