namespace WSI.Cashier
{
  partial class uc_input_amount
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(uc_input_amount));
      this.gp_digits_box = new WSI.Cashier.Controls.uc_round_panel();
      this.btn_num_dot = new WSI.Cashier.Controls.uc_round_button();
      this.btn_money_4 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_money_3 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_money_2 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_money_1 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_1 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_back = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_2 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_intro = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_3 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_4 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_10 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_5 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_6 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_9 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_7 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_8 = new WSI.Cashier.Controls.uc_round_button();
      this.txt_amount = new WSI.Cashier.Controls.uc_round_textbox();
      this.gp_digits_box.SuspendLayout();
      this.SuspendLayout();
      // 
      // gp_digits_box
      // 
      this.gp_digits_box.BackColor = System.Drawing.Color.Transparent;
      this.gp_digits_box.Controls.Add(this.btn_num_dot);
      this.gp_digits_box.Controls.Add(this.btn_money_4);
      this.gp_digits_box.Controls.Add(this.btn_money_3);
      this.gp_digits_box.Controls.Add(this.btn_money_2);
      this.gp_digits_box.Controls.Add(this.btn_money_1);
      this.gp_digits_box.Controls.Add(this.btn_num_1);
      this.gp_digits_box.Controls.Add(this.btn_back);
      this.gp_digits_box.Controls.Add(this.btn_num_2);
      this.gp_digits_box.Controls.Add(this.btn_intro);
      this.gp_digits_box.Controls.Add(this.btn_num_3);
      this.gp_digits_box.Controls.Add(this.btn_num_4);
      this.gp_digits_box.Controls.Add(this.btn_num_10);
      this.gp_digits_box.Controls.Add(this.btn_num_5);
      this.gp_digits_box.Controls.Add(this.btn_num_6);
      this.gp_digits_box.Controls.Add(this.btn_num_9);
      this.gp_digits_box.Controls.Add(this.btn_num_7);
      this.gp_digits_box.Controls.Add(this.btn_num_8);
      this.gp_digits_box.Controls.Add(this.txt_amount);
      this.gp_digits_box.CornerRadius = 10;
      this.gp_digits_box.Dock = System.Windows.Forms.DockStyle.Fill;
      this.gp_digits_box.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gp_digits_box.ForeColor = System.Drawing.Color.Black;
      this.gp_digits_box.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gp_digits_box.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gp_digits_box.HeaderHeight = 35;
      this.gp_digits_box.HeaderSubText = null;
      this.gp_digits_box.HeaderText = null;
      this.gp_digits_box.Location = new System.Drawing.Point(0, 0);
      this.gp_digits_box.Name = "gp_digits_box";
      this.gp_digits_box.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gp_digits_box.Size = new System.Drawing.Size(380, 380);
      this.gp_digits_box.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.gp_digits_box.TabIndex = 16;
      // 
      // btn_num_dot
      // 
      this.btn_num_dot.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_dot.CornerRadius = 0;
      this.btn_num_dot.Enabled = false;
      this.btn_num_dot.FlatAppearance.BorderSize = 0;
      this.btn_num_dot.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_dot.Font = new System.Drawing.Font("Montserrat", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_dot.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_dot.Location = new System.Drawing.Point(158, 315);
      this.btn_num_dot.Name = "btn_num_dot";
      this.btn_num_dot.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
      this.btn_num_dot.Size = new System.Drawing.Size(70, 50);
      this.btn_num_dot.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_dot.TabIndex = 37;
      this.btn_num_dot.TabStop = false;
      this.btn_num_dot.Text = "�";
      this.btn_num_dot.UseVisualStyleBackColor = false;
      this.btn_num_dot.Click += new System.EventHandler(this.btn_num_dot_Click);
      // 
      // btn_money_4
      // 
      this.btn_money_4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_money_4.CornerRadius = 0;
      this.btn_money_4.FlatAppearance.BorderSize = 0;
      this.btn_money_4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_money_4.Font = new System.Drawing.Font("Montserrat", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_money_4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_money_4.Location = new System.Drawing.Point(286, 85);
      this.btn_money_4.Name = "btn_money_4";
      this.btn_money_4.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_money_4.Size = new System.Drawing.Size(90, 60);
      this.btn_money_4.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_MONEY;
      this.btn_money_4.TabIndex = 36;
      this.btn_money_4.TabStop = false;
      this.btn_money_4.Text = "2000";
      this.btn_money_4.UseVisualStyleBackColor = false;
      this.btn_money_4.Click += new System.EventHandler(this.btn_custom_Click);
      // 
      // btn_money_3
      // 
      this.btn_money_3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_money_3.CornerRadius = 0;
      this.btn_money_3.FlatAppearance.BorderSize = 0;
      this.btn_money_3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_money_3.Font = new System.Drawing.Font("Montserrat", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_money_3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_money_3.Location = new System.Drawing.Point(192, 85);
      this.btn_money_3.Name = "btn_money_3";
      this.btn_money_3.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_money_3.Size = new System.Drawing.Size(90, 60);
      this.btn_money_3.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_MONEY;
      this.btn_money_3.TabIndex = 35;
      this.btn_money_3.TabStop = false;
      this.btn_money_3.Text = "100";
      this.btn_money_3.UseVisualStyleBackColor = false;
      this.btn_money_3.Click += new System.EventHandler(this.btn_custom_Click);
      // 
      // btn_money_2
      // 
      this.btn_money_2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_money_2.CornerRadius = 0;
      this.btn_money_2.FlatAppearance.BorderSize = 0;
      this.btn_money_2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_money_2.Font = new System.Drawing.Font("Montserrat", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_money_2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_money_2.Location = new System.Drawing.Point(98, 85);
      this.btn_money_2.Name = "btn_money_2";
      this.btn_money_2.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_money_2.Size = new System.Drawing.Size(90, 60);
      this.btn_money_2.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_MONEY;
      this.btn_money_2.TabIndex = 34;
      this.btn_money_2.TabStop = false;
      this.btn_money_2.Text = "50";
      this.btn_money_2.UseVisualStyleBackColor = false;
      this.btn_money_2.Click += new System.EventHandler(this.btn_custom_Click);
      // 
      // btn_money_1
      // 
      this.btn_money_1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_money_1.CornerRadius = 0;
      this.btn_money_1.FlatAppearance.BorderSize = 0;
      this.btn_money_1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_money_1.Font = new System.Drawing.Font("Montserrat", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_money_1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_money_1.Location = new System.Drawing.Point(4, 85);
      this.btn_money_1.Name = "btn_money_1";
      this.btn_money_1.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_money_1.Size = new System.Drawing.Size(90, 60);
      this.btn_money_1.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_MONEY;
      this.btn_money_1.TabIndex = 33;
      this.btn_money_1.TabStop = false;
      this.btn_money_1.Text = "10";
      this.btn_money_1.UseVisualStyleBackColor = false;
      this.btn_money_1.Click += new System.EventHandler(this.btn_custom_Click);
      // 
      // btn_num_1
      // 
      this.btn_num_1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_1.CornerRadius = 0;
      this.btn_num_1.FlatAppearance.BorderSize = 0;
      this.btn_num_1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_1.Font = new System.Drawing.Font("Montserrat", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_1.Location = new System.Drawing.Point(4, 150);
      this.btn_num_1.Name = "btn_num_1";
      this.btn_num_1.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
      this.btn_num_1.Size = new System.Drawing.Size(70, 50);
      this.btn_num_1.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_1.TabIndex = 21;
      this.btn_num_1.TabStop = false;
      this.btn_num_1.Text = "1";
      this.btn_num_1.UseVisualStyleBackColor = false;
      this.btn_num_1.Click += new System.EventHandler(this.btn_num_Click);
      // 
      // btn_back
      // 
      this.btn_back.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_back.CornerRadius = 0;
      this.btn_back.FlatAppearance.BorderSize = 0;
      this.btn_back.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_back.Font = new System.Drawing.Font("Montserrat", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_back.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_back.Image = ((System.Drawing.Image)(resources.GetObject("btn_back.Image")));
      this.btn_back.Location = new System.Drawing.Point(235, 150);
      this.btn_back.Name = "btn_back";
      this.btn_back.Size = new System.Drawing.Size(141, 50);
      this.btn_back.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_BACKSPACE;
      this.btn_back.TabIndex = 32;
      this.btn_back.TabStop = false;
      this.btn_back.UseVisualStyleBackColor = false;
      this.btn_back.Click += new System.EventHandler(this.btn_back_Click);
      // 
      // btn_num_2
      // 
      this.btn_num_2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_2.CornerRadius = 0;
      this.btn_num_2.FlatAppearance.BorderSize = 0;
      this.btn_num_2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_2.Font = new System.Drawing.Font("Montserrat", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_2.Location = new System.Drawing.Point(81, 150);
      this.btn_num_2.Name = "btn_num_2";
      this.btn_num_2.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
      this.btn_num_2.Size = new System.Drawing.Size(70, 50);
      this.btn_num_2.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_2.TabIndex = 22;
      this.btn_num_2.TabStop = false;
      this.btn_num_2.Text = "2";
      this.btn_num_2.UseVisualStyleBackColor = false;
      this.btn_num_2.Click += new System.EventHandler(this.btn_num_Click);
      // 
      // btn_intro
      // 
      this.btn_intro.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(153)))), ((int)(((byte)(49)))));
      this.btn_intro.CornerRadius = 0;
      this.btn_intro.FlatAppearance.BorderSize = 0;
      this.btn_intro.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_intro.Font = new System.Drawing.Font("Montserrat", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_intro.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_intro.Image = ((System.Drawing.Image)(resources.GetObject("btn_intro.Image")));
      this.btn_intro.Location = new System.Drawing.Point(235, 205);
      this.btn_intro.Name = "btn_intro";
      this.btn_intro.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_intro.Size = new System.Drawing.Size(141, 160);
      this.btn_intro.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_ENTER;
      this.btn_intro.TabIndex = 31;
      this.btn_intro.TabStop = false;
      this.btn_intro.UseVisualStyleBackColor = false;
      this.btn_intro.Click += new System.EventHandler(this.btn_intro_Click);
      // 
      // btn_num_3
      // 
      this.btn_num_3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_3.CornerRadius = 0;
      this.btn_num_3.FlatAppearance.BorderSize = 0;
      this.btn_num_3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_3.Font = new System.Drawing.Font("Montserrat", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_3.Location = new System.Drawing.Point(158, 150);
      this.btn_num_3.Name = "btn_num_3";
      this.btn_num_3.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
      this.btn_num_3.Size = new System.Drawing.Size(70, 50);
      this.btn_num_3.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_3.TabIndex = 23;
      this.btn_num_3.TabStop = false;
      this.btn_num_3.Text = "3";
      this.btn_num_3.UseVisualStyleBackColor = false;
      this.btn_num_3.Click += new System.EventHandler(this.btn_num_Click);
      // 
      // btn_num_4
      // 
      this.btn_num_4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_4.CornerRadius = 0;
      this.btn_num_4.FlatAppearance.BorderSize = 0;
      this.btn_num_4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_4.Font = new System.Drawing.Font("Montserrat", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_4.Location = new System.Drawing.Point(4, 205);
      this.btn_num_4.Name = "btn_num_4";
      this.btn_num_4.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
      this.btn_num_4.Size = new System.Drawing.Size(70, 50);
      this.btn_num_4.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_4.TabIndex = 24;
      this.btn_num_4.TabStop = false;
      this.btn_num_4.Text = "4";
      this.btn_num_4.UseVisualStyleBackColor = false;
      this.btn_num_4.Click += new System.EventHandler(this.btn_num_Click);
      // 
      // btn_num_10
      // 
      this.btn_num_10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_10.CornerRadius = 0;
      this.btn_num_10.FlatAppearance.BorderSize = 0;
      this.btn_num_10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_10.Font = new System.Drawing.Font("Montserrat", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_10.Location = new System.Drawing.Point(4, 315);
      this.btn_num_10.Name = "btn_num_10";
      this.btn_num_10.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
      this.btn_num_10.Size = new System.Drawing.Size(145, 50);
      this.btn_num_10.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_10.TabIndex = 30;
      this.btn_num_10.TabStop = false;
      this.btn_num_10.Text = "0";
      this.btn_num_10.UseVisualStyleBackColor = false;
      this.btn_num_10.Click += new System.EventHandler(this.btn_num_Click);
      // 
      // btn_num_5
      // 
      this.btn_num_5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_5.CornerRadius = 0;
      this.btn_num_5.FlatAppearance.BorderSize = 0;
      this.btn_num_5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_5.Font = new System.Drawing.Font("Montserrat", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_5.Location = new System.Drawing.Point(81, 205);
      this.btn_num_5.Name = "btn_num_5";
      this.btn_num_5.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
      this.btn_num_5.Size = new System.Drawing.Size(70, 50);
      this.btn_num_5.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_5.TabIndex = 25;
      this.btn_num_5.TabStop = false;
      this.btn_num_5.Text = "5";
      this.btn_num_5.UseVisualStyleBackColor = false;
      this.btn_num_5.Click += new System.EventHandler(this.btn_num_Click);
      // 
      // btn_num_6
      // 
      this.btn_num_6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_6.CornerRadius = 0;
      this.btn_num_6.FlatAppearance.BorderSize = 0;
      this.btn_num_6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_6.Font = new System.Drawing.Font("Montserrat", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_6.Location = new System.Drawing.Point(158, 205);
      this.btn_num_6.Name = "btn_num_6";
      this.btn_num_6.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
      this.btn_num_6.Size = new System.Drawing.Size(70, 50);
      this.btn_num_6.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_6.TabIndex = 26;
      this.btn_num_6.TabStop = false;
      this.btn_num_6.Text = "6";
      this.btn_num_6.UseVisualStyleBackColor = false;
      this.btn_num_6.Click += new System.EventHandler(this.btn_num_Click);
      // 
      // btn_num_9
      // 
      this.btn_num_9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_9.CornerRadius = 0;
      this.btn_num_9.FlatAppearance.BorderSize = 0;
      this.btn_num_9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_9.Font = new System.Drawing.Font("Montserrat", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_9.Location = new System.Drawing.Point(158, 260);
      this.btn_num_9.Name = "btn_num_9";
      this.btn_num_9.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
      this.btn_num_9.Size = new System.Drawing.Size(70, 50);
      this.btn_num_9.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_9.TabIndex = 29;
      this.btn_num_9.TabStop = false;
      this.btn_num_9.Text = "9";
      this.btn_num_9.UseVisualStyleBackColor = false;
      this.btn_num_9.Click += new System.EventHandler(this.btn_num_Click);
      // 
      // btn_num_7
      // 
      this.btn_num_7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_7.CornerRadius = 0;
      this.btn_num_7.FlatAppearance.BorderSize = 0;
      this.btn_num_7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_7.Font = new System.Drawing.Font("Montserrat", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_7.Location = new System.Drawing.Point(4, 260);
      this.btn_num_7.Name = "btn_num_7";
      this.btn_num_7.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
      this.btn_num_7.Size = new System.Drawing.Size(70, 50);
      this.btn_num_7.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_7.TabIndex = 27;
      this.btn_num_7.TabStop = false;
      this.btn_num_7.Text = "7";
      this.btn_num_7.UseVisualStyleBackColor = false;
      this.btn_num_7.Click += new System.EventHandler(this.btn_num_Click);
      // 
      // btn_num_8
      // 
      this.btn_num_8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_8.CornerRadius = 0;
      this.btn_num_8.FlatAppearance.BorderSize = 0;
      this.btn_num_8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_8.Font = new System.Drawing.Font("Montserrat", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_8.Location = new System.Drawing.Point(81, 260);
      this.btn_num_8.Name = "btn_num_8";
      this.btn_num_8.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
      this.btn_num_8.Size = new System.Drawing.Size(70, 50);
      this.btn_num_8.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_8.TabIndex = 28;
      this.btn_num_8.TabStop = false;
      this.btn_num_8.Text = "8";
      this.btn_num_8.UseVisualStyleBackColor = false;
      this.btn_num_8.Click += new System.EventHandler(this.btn_num_Click);
      // 
      // txt_amount
      // 
      this.txt_amount.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_amount.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_amount.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.txt_amount.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.txt_amount.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_amount.CornerRadius = 0;
      this.txt_amount.Font = new System.Drawing.Font("Montserrat", 40F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_amount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(153)))), ((int)(((byte)(49)))));
      this.txt_amount.Location = new System.Drawing.Point(3, 5);
      this.txt_amount.MaxLength = 9;
      this.txt_amount.Multiline = false;
      this.txt_amount.Name = "txt_amount";
      this.txt_amount.PasswordChar = '\0';
      this.txt_amount.ReadOnly = false;
      this.txt_amount.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_amount.SelectedText = "";
      this.txt_amount.SelectionLength = 0;
      this.txt_amount.SelectionStart = 0;
      this.txt_amount.Size = new System.Drawing.Size(374, 44);
      this.txt_amount.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.NONE;
      this.txt_amount.TabIndex = 0;
      this.txt_amount.Text = "123";
      this.txt_amount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      this.txt_amount.UseSystemPasswordChar = false;
      this.txt_amount.TextChanged += new System.EventHandler(this.txt_amount_TextChanged);
      this.txt_amount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.uc_input_amount_KeyPress);
      // 
      // uc_input_amount
      // 
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
      this.BackColor = System.Drawing.Color.Transparent;
      this.Controls.Add(this.gp_digits_box);
      this.Name = "uc_input_amount";
      this.Size = new System.Drawing.Size(380, 380);
      this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.uc_input_amount_KeyPress);
      this.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.uc_input_amount_PreviewKeyDown);
      this.gp_digits_box.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    public WSI.Cashier.Controls.uc_round_panel gp_digits_box;
    public WSI.Cashier.Controls.uc_round_textbox txt_amount;
    private Controls.uc_round_button btn_num_dot;
    private Controls.uc_round_button btn_money_4;
    private Controls.uc_round_button btn_money_3;
    private Controls.uc_round_button btn_money_2;
    private Controls.uc_round_button btn_money_1;
    private Controls.uc_round_button btn_num_1;
    private Controls.uc_round_button btn_back;
    private Controls.uc_round_button btn_num_2;
    private Controls.uc_round_button btn_intro;
    private Controls.uc_round_button btn_num_3;
    private Controls.uc_round_button btn_num_4;
    private Controls.uc_round_button btn_num_10;
    private Controls.uc_round_button btn_num_5;
    private Controls.uc_round_button btn_num_6;
    private Controls.uc_round_button btn_num_9;
    private Controls.uc_round_button btn_num_7;
    private Controls.uc_round_button btn_num_8;
  }
}
