namespace WSI.Cashier
{
  partial class uc_set_terminal_meter
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.lbl_description = new System.Windows.Forms.Label();
      this.cb_action = new WSI.Cashier.Controls.uc_round_combobox();
      this.txt_value = new WSI.Cashier.Controls.uc_round_textbox_padnumber();
      this.SuspendLayout();
      // 
      // lbl_description
      // 
      this.lbl_description.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_description.Location = new System.Drawing.Point(3, 10);
      this.lbl_description.Name = "lbl_description";
      this.lbl_description.Size = new System.Drawing.Size(225, 23);
      this.lbl_description.TabIndex = 0;
      this.lbl_description.Text = "xDescription";
      this.lbl_description.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // cb_action
      // 
      this.cb_action.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_action.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.cb_action.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.cb_action.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.cb_action.CornerRadius = 5;
      this.cb_action.DataSource = null;
      this.cb_action.DisplayMember = "";
      this.cb_action.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
      this.cb_action.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cb_action.DropDownWidth = 228;
      this.cb_action.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.cb_action.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_action.FormattingEnabled = true;
      this.cb_action.IsDroppedDown = false;
      this.cb_action.ItemHeight = 40;
      this.cb_action.Location = new System.Drawing.Point(235, 3);
      this.cb_action.MaxDropDownItems = 8;
      this.cb_action.Name = "cb_action";
      this.cb_action.SelectedIndex = -1;
      this.cb_action.SelectedItem = null;
      this.cb_action.SelectedValue = null;
      this.cb_action.SelectionLength = 0;
      this.cb_action.SelectionStart = 0;
      this.cb_action.Size = new System.Drawing.Size(228, 40);
      this.cb_action.Sorted = false;
      this.cb_action.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.BASIC;
      this.cb_action.TabIndex = 0;
      this.cb_action.TabStop = false;
      this.cb_action.ValueMember = "";
      this.cb_action.SelectedIndexChanged += new System.EventHandler(this.cb_action_SelectedIndexChanged);
      // 
      // txt_value
      // 
      this.txt_value.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_value.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_value.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_value.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_value.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_value.CornerRadius = 5;
      this.txt_value.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_value.Location = new System.Drawing.Point(489, 3);
      this.txt_value.MaxLength = 10;
      this.txt_value.Multiline = false;
      this.txt_value.Name = "txt_value";
      this.txt_value.PadNumberLocation = null;
      this.txt_value.PadNumberStartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.txt_value.PasswordChar = '\0';
      this.txt_value.ReadOnly = false;
      this.txt_value.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_value.SelectedText = "";
      this.txt_value.SelectionLength = 0;
      this.txt_value.SelectionStart = 0;
      this.txt_value.ShowPadNumberOnEnter = true;
      this.txt_value.Size = new System.Drawing.Size(327, 40);
      this.txt_value.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_value.TabIndex = 1;
      this.txt_value.TabStop = false;
      this.txt_value.Text = "1234567890";
      this.txt_value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      this.txt_value.UseSystemPasswordChar = false;
      this.txt_value.WaterMark = null;
      this.txt_value.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      this.txt_value.TextChanged += new System.EventHandler(this.txt_value_TextChanged);
      this.txt_value.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_value_KeyPress);
      // 
      // uc_set_terminal_meter
      // 
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
      this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.Controls.Add(this.lbl_description);
      this.Controls.Add(this.cb_action);
      this.Controls.Add(this.txt_value);
      this.Name = "uc_set_terminal_meter";
      this.Size = new System.Drawing.Size(825, 46);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Label lbl_description;
    private WSI.Cashier.Controls.uc_round_combobox cb_action;
    private WSI.Cashier.Controls.uc_round_textbox_padnumber txt_value;
  }
}
