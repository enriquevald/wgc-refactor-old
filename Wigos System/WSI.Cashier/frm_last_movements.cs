//------------------------------------------------------------------------------
// Copyright © 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_last_movements.cs
// 
//   DESCRIPTION: Implements the following classes:
//                1. frm_last_movements
//
//        AUTHOR: ACC
// 
// CREATION DATE: 07-AUG-2007
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 07-AUG-2007 ACC     First release.
// 22-NOV-2011 JMM     Added new movements (PIN Change & PIN Generation)
// 17-FEB-2012 RCI     Added new movements (CashIn CoverCoupon & Not Redeemable 2 Expiration)
// 20-FEB-2012 RCI     Added new movement (MBMovementType.CreditCoverCoupon)
// 29-FEB-2012 JMM     Added new cashier movements (CASHIER_MOVEMENT.MB_CASH_IN_NOT_REDEEMABLE, 
//                     CASHIER_MOVEMENT.MB_CASH_IN_COVER_COUPON & CASHIER_MOVEMENT.MB_PRIZE_COUPON).
// 24-APR-2012 JCM     Added new movement (User Card Recycled)
// 05-JUL-2012 DDM     Modified the row color when is MovementShow.Card. Now grouped by operation
// 25-JUL-2012 DDM     Added news movements (CANCEL_PROMOTION_POINT & PROMOTION_POINT)
// 26-SEP-2012 MPO     Added news movement CancelGiftInstance
// 27-SEP-2012 DDM     Added new movement REQUEST_TO_VOUCHER_REPRINT
// 06-FEB-2013 JMM     Added new movement (PointsStatusChanged).
// 16-APR-2013 ICS     Changed the cashier name column by its alias.
// 06-JUN-2013 NMR     Fixed Bug #814: Exception when accessing to Last Player Movements
// 02-JUL-2013 DLL     Modify datagridview to show currency exchange cashier movements
// 12-AUG-2013 FBA     Added new movement (DepositCancelation) and sets default value for movements.
// 26-AUG-2013 FBA     Added New Movement "Recharge for points" (85)
// 04-SEP-2013 NMR     Addes New Movement "Ticket Reprint" (90)
// 10-SEP-2013 SMN     Addes New Movement "Participation in a Draw" (200)
// 10-SEP-2013 DHA     Addes New Movement "Cash Desk Draw PlaySession" (201)
// 10-SEP-2013 DLL     Addes New Movement "Check Reharge" (92)
// 18-SEP-2013 NMR     Added new string resources for TITO movements types
// 18-SEP-2013 JML     No view balance for Cash draw participation
// 25-SEP-2013 NMR     Added new TITO movements types and new string resources for these movements
// 26-SEP-2013 LEM     Set blank row for movements that not modified balance
// 18-OCT-2013 ICS     Added undo operation suport
// 19-NOV-2013 FBA     Added cage movements
// 03-DEC-2013 JCA     Show "total" and "detail" in movements
// 02-JAN-2014 DLL     Added New Movement CashInTax
// 20-JAN-2014 RMS     Added New Movements TransferCreditIn, TransferCreditOut
// 05-FEB-2014 DHA     Added New Movements TITO_TicketRedeemJackpot, TITO_TicketRedeemHandpay
// 06-FEB-2014 RCI     Fixed Bug WIG-499: When Cashier is Closed, cashier movements show 3 columns that must be hidden.
// 20-FEB-2014 JPJ     Fixed Bug WIG-659: Detail of the cashier movement when it's a ticket denominaion.
// 25-APR-2014 DHA & JML Rename/Added TITO Cashier/Account Movements
// 04-JUL-2014 AMF     Personal Card Replacement
// 08-AUG-2014 RMS     Added supoort for Specific Bank Card Types
// 28-AUG-2014 LEM & DLL Fixed Bug WIG-1218: Cage stock mismatch with tips
// 16-SEP-2014 AMF     Added new movement Concepts Input - Output
// 18-SEP-2014 DLL     Added new movement Cash Advance
// 22-SEP-2014 SMN     Added "(Undone)" to Concept movements
// 08-OCT-2014 DHA     Added new movement, advice if a recharge is going to refund without plays
// 14-OCT-2014 OPC     Fixed Bug WIG-1446: fix the problem when try to show an account without rows.
// 24-NOV-2014 MPO & OPC  LOGRAND CON02: Added IVA calculated (when needed) and input params class for cashin.
// 09-DIC-2014 OPC     Fixed Bug WIG-1816: base tax negative when undo last operation.
// 10-DEC-2014 DRV     Decimal currency formatting: DUT 201411 - Tito Chile - Currency formatting without decimals
// 15-DEC-2014 JBC, DLL, JPJ    New Company B commission movements
// 21-JAN-2015 JPJ     New mobile bank deposit movements (cancellation)
// 30-OCT-2015 GMV     Fixed Bug 5821: WinLoss Statement Request incorrect label for movement type.
// 09-NOV-2015 FOS     Apply new design in form
// 20-NOV-2015 FAV     PBI 6048: Report to Promobox movements about print promotions not redeemable
// 31-NOV-2015 FOS     Fixed Bug 7074: Last movement , incorrect Title.
// 03-DIC-2015 FAV     PBI 6048: Report to Promobox movements about print promotions redeemable
// 12-JAN-2016 AMF     PBI 8260: Changes NLS
// 20-JAN-2016 RAB     Product Backlog Item 7941:Multiple buckets: Cajero: Añadir/Restar valores a los buckets
// 03-FEB-2016 LA      Task 8875: TES35: Cashier: Add authorization login to last movements button.
// 09-FEB-2016 FAV     PBI 9148: Void machine ticket
// 16-FEB-2016 LA      PBI 7496: New Movements (RE)
// 02-MAR-2016 DHA    Product Backlog Item 10085:Winpot - Added Tax Provisions movements
// 17-MAR-2016 EOR    Product Backlog Item 10235:TITA: Reporte de tickets
// 19-APR-2016 RGR    Product Backlog Item 11297: Codere - Tax Custody: Reports and cash movements and account
// 25-APR-2016 EOR    Product Backlog Item 11296: Codere - Custody Tax: Implement Devolution Tax Custody
// 27-APR-2016 RGR    Product Backlog Item 11298: Codere - Tax Custody: Modify reports GUI Income and Return
// 04-MAR-2016 AMF     PBI 8267: Rollback
// 02-MAR-2016 DHA    Product Backlog Item 10085:Winpot - Added Tax Provisions movements
// 08-MAR-2016 ETP     PBI 10391: Points witohut currency symbol in account movements.
// 17-MAR-2016 EOR    Product Backlog Item 10235:TITA: Reporte de tickets
// 22-MAR-2106 YNM    Bug 10759: Reception error paying with currency
// 24-MAR-2016 RAB    Bug 10745: Reception: with the GP requiereNewCard to "0" when assigning a card client is counted as "replacement"
// 08-APR-2016 DHA    Fixed Bug 11595: Floor Dual Currency - added movement STR_FLOOR_DUAL_CURRENCY_TICKET_CREATE_EXCHANGE
// 28-APR-2016 FGB     PBI 9758: Tables (Phase 1): Review table reports
// 07-JUN-2016 JBP    PBI 13253:C2GO - Implementación métodos WS Wigos
// 09-JUN-2016 DHA    Product Backlog Item 13402 & 9853: Rolling & Fixed closing stock
// 23-JUN-2016 RAB    PBI 14486 :GamingTables (Phase 1): Review compatibility (last cashier movements)
// 28-JUN-2016 RAB    PBI 14524: GamingTables (Phase 4): Management from deposit and withdrawals - Multicurrency in chips.
// 30-JUN-2016 ETP    Fixed Bug 9360: Movimientos de caja: no se informa de la cantidad al anular un tícket TITO emitido por un terminal de juego
// 06-JUL-2016 DHA    Product Backlog Item 15064: multicurrency chips sale/purchase
// 02-AUG-2016 RAB    Product Backlog Item 15614: TPV Televisa: Undo last operation on bank card
// 09-AUG-2016 FGB    Product Backlog Item 15614: TPV Televisa: Undo last operation on bank card
// 19-SEP-2016 EOR    PBI 17468: Televisa - Service Charge: Cashier Movements
// 27-SEP-2016 RAB    Bug 17875: TPV Televisa: Shortages of balance to cancel a credit card operation. Problems with the price of the player card
// 29-SEP-2016 RGR    PBI 17447: Televisa - Draw Cash (Cash prize) - Recharge Operation
// 04-OCT-2016 RGR    PBI 17447: Televisa - Draw Cash (Cash prize) - Recharge Operation
// 05-OCT-2016 RGR    PBI 17964: Televisa - Draw Cash (Cash prize) - Configuration
// 20-OCT-2016 PDM    Fixed Bug 19399:Cajero: No se muestran los cambios solicitados por Televisa en los "Movimientos de caja"
// 20-OCT-2016 FAV    Fixed Bug 19400: Movements cashier Televisa
// 21-OCT-2016 FAV    Fixed Bug 19399: Cajero: Removed changes of cashier movements for Televisa
// 10-NOV-2016 ATB    Bug 20147:Televisa: Requested changes from Televisa for "Cashier movements"
// 30-NOV-2016 FOS    PBI 19197: Reopen cashier sessions and gambling tables sessions
// 06-MAR-2017 RAB    PBI 24378: Reopen sessions and gaming tables: Undo reopening and correction bugs
// 08-FEB-2017 FAV    PBI 25268: Third TAX - Payment Voucher
// 13-MAR-2017 FGB    PBI 25735: Third TAX - Movement reports
// 21-MAR-2017 ETP    PBI 25788: Credit Line - Get a Marker
// 30-MAR-2017 FGB    PBI 25735: Third TAX - Movement reports
// 19-JUN-2017 RAB    PBI 28000: WIGOS-2732 - Rounding - Movements
// 06-JUL-2017 RAB    PBI 28422: WIGOS-2702 Rounding - Consume the remaining amount
// 25-JUN-2018 FOS    Bug 33297:WIGOS-13059 Gambling tables: it is allowed to reopen a gambling table with integrated cashier (fixed banking) when the collection movements have been finished.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using WSI.Common;
using WSI.Cashier.Controls;
using WSI.Cashier.WebApi.Client;
using WSI.Cashier.WebApi.Client.Helpers;
using WSI.Cashier.Client.Interfaces.Messages;

namespace WSI.Cashier
{
  public partial class frm_last_movements : frm_base
  {
    private const Int32 GRID_MAX_COLUMNS = 17;

    private const Int32 GRID_COLUMN_TYPE_ID = 0;
    private const Int32 GRID_COLUMN_TYPE_NAME = 1;
    private const Int32 GRID_COLUMN_DATE = 2;
    private const Int32 GRID_COLUMN_USER_NAME = 3;
    private const Int32 GRID_COLUMN_TERMINAL = 4;
    private const Int32 GRID_COLUMN_TERM_ID = 5;
    private const Int32 GRID_COLUMN_INITIAL_AMOUNT = 6;
    private const Int32 GRID_COLUMN_SUB_AMOUNT = 7;
    private const Int32 GRID_COLUMN_ADD_AMOUNT = 8;
    private const Int32 GRID_COLUMN_FINAL_AMOUNT = 9;
    private const Int32 GRID_COLUMN_OPERATION_ID = 10;
    private const Int32 GRID_COLUMN_ROW_COLOR = 11;
    private const Int32 GRID_COLUMN_DETAILS = 12;

    // for mobile bank
    private const Int32 GRID_COLUMN_STATUS_UNDO = 10;

    // for Cashier
    private const Int32 GRID_COLUMN_ADD_AMOUNT_DECIMAL = 13;
    private const Int32 GRID_COLUMN_SUB_AMOUNT_DECIMAL = 14;
    private const Int32 GRID_COLUMN_CURRENCY_ISO_CODE = 15;
    private const Int32 GRID_COLUMN_STATUS = 16;
    private const Int32 GRID_COLUMN_DENOMINATION = 17;
    private const Int32 GRID_COLUMN_AUX_AMOUNT = 18;
    private const Int32 GRID_COLUMN_CAGE_CURRENCY_TYPE = 19;

    // for Card
    private const Int32 GRID_COLUMN_CARD_STATUS = 13;

    // for Player Tracking
    private const Int32 GRID_COLUMN_PT_SECOND_ACCOUNT = 6;
    private const Int32 GRID_COLUMN_PT_INITIAL_AMOUNT = 7;
    private const Int32 GRID_COLUMN_PT_SUB_AMOUNT = 8;
    private const Int32 GRID_COLUMN_PT_ADD_AMOUNT = 9;
    private const Int32 GRID_COLUMN_PT_FINAL_AMOUNT = 10;

    private const Decimal TICKET_TITO_DENOMINATION = -200;

    public enum MovementShow
    {
      Card = 1,
      Cashier = 2,
      Mobile_Bank = 3,
      Player_Tracking = 4
    }

    #region Attributes

    private frm_yesno form_yes_no;
    private MovementShow m_mov_show;

    private String m_split_a_name_str;
    private String m_split_b_name_str;

    private String m_prize_taxes_1_name;
    private String m_prize_taxes_2_name;
    private String m_prize_taxes_3_name;

    private String m_split2_tax_name;
    private string _national_currency = CurrencyExchange.GetNationalCurrency();

    #endregion

    #region Constructor

    /// <summary>
    /// Constructor method.
    /// 1. Initialize controls.
    /// 2. Initialize control´s resources. (NLS, Images, etc)
    /// </summary>
    public frm_last_movements()
    {
      InitializeComponent();

      InitializeControlResources();

      form_yes_no = new frm_yesno();
      form_yes_no.Opacity = 0.6;
    }

    #endregion

    #region Members

    #endregion

    #region Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Set the visibility and with of the columns.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void SetColumnsVisibility()
    {
      switch (m_mov_show)
      {
        case MovementShow.Card:
          dataGridView1.Columns[GRID_COLUMN_USER_NAME].Width = 0;
          dataGridView1.Columns[GRID_COLUMN_USER_NAME].Visible = false;
          dataGridView1.Columns[GRID_COLUMN_TERMINAL].Width = 224;
          dataGridView1.Columns[GRID_COLUMN_TERMINAL].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
          dataGridView1.Columns[GRID_COLUMN_CARD_STATUS].Width = 0;
          dataGridView1.Columns[GRID_COLUMN_CARD_STATUS].Visible = false;
          dataGridView1.Columns[GRID_COLUMN_TYPE_ID].Width = 0;
          dataGridView1.Columns[GRID_COLUMN_TYPE_ID].Visible = false;
          dataGridView1.Columns[GRID_COLUMN_DETAILS].Width = 0;
          dataGridView1.Columns[GRID_COLUMN_DETAILS].Visible = false;
          dataGridView1.Columns[GRID_COLUMN_OPERATION_ID].Width = 0;
          dataGridView1.Columns[GRID_COLUMN_OPERATION_ID].Visible = false;
          dataGridView1.Columns[GRID_COLUMN_ROW_COLOR].Width = 0;
          dataGridView1.Columns[GRID_COLUMN_ROW_COLOR].Visible = false;
          break;

        case MovementShow.Cashier:
          dataGridView1.Columns[GRID_COLUMN_USER_NAME].Width = 153;
          dataGridView1.Columns[GRID_COLUMN_USER_NAME].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
          dataGridView1.Columns[GRID_COLUMN_TERMINAL].Width = 0;
          dataGridView1.Columns[GRID_COLUMN_TERMINAL].Visible = false;
          dataGridView1.Columns[GRID_COLUMN_TYPE_NAME].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
          dataGridView1.Columns[GRID_COLUMN_TYPE_NAME].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
          dataGridView1.Columns[GRID_COLUMN_INITIAL_AMOUNT].Width = 0;
          dataGridView1.Columns[GRID_COLUMN_INITIAL_AMOUNT].Visible = false;
          dataGridView1.Columns[GRID_COLUMN_CURRENCY_ISO_CODE].Width = 0;
          dataGridView1.Columns[GRID_COLUMN_CURRENCY_ISO_CODE].Visible = false;
          dataGridView1.Columns[GRID_COLUMN_ADD_AMOUNT_DECIMAL].Width = 0;
          dataGridView1.Columns[GRID_COLUMN_ADD_AMOUNT_DECIMAL].Visible = false;
          dataGridView1.Columns[GRID_COLUMN_SUB_AMOUNT_DECIMAL].Width = 0;
          dataGridView1.Columns[GRID_COLUMN_SUB_AMOUNT_DECIMAL].Visible = false;

          dataGridView1.Columns[GRID_COLUMN_STATUS].Width = 0;
          dataGridView1.Columns[GRID_COLUMN_STATUS].Visible = false;
          dataGridView1.Columns[GRID_COLUMN_DENOMINATION].Width = 0;
          dataGridView1.Columns[GRID_COLUMN_DENOMINATION].Visible = false;
          dataGridView1.Columns[GRID_COLUMN_TYPE_ID].Width = 0;
          dataGridView1.Columns[GRID_COLUMN_TYPE_ID].Visible = false;
          dataGridView1.Columns[GRID_COLUMN_OPERATION_ID].Width = 0;
          dataGridView1.Columns[GRID_COLUMN_OPERATION_ID].Visible = false;
          dataGridView1.Columns[GRID_COLUMN_ROW_COLOR].Width = 0;
          dataGridView1.Columns[GRID_COLUMN_ROW_COLOR].Visible = false;
          dataGridView1.Columns[GRID_COLUMN_DETAILS].Width = 0;
          dataGridView1.Columns[GRID_COLUMN_DETAILS].Visible = false;
          dataGridView1.Columns[GRID_COLUMN_AUX_AMOUNT].Width = 0;
          dataGridView1.Columns[GRID_COLUMN_AUX_AMOUNT].Visible = false;
          dataGridView1.Columns[GRID_COLUMN_CAGE_CURRENCY_TYPE].Width = 0;
          dataGridView1.Columns[GRID_COLUMN_CAGE_CURRENCY_TYPE].Visible = false;
          break;

        case MovementShow.Mobile_Bank:
          dataGridView1.Columns[GRID_COLUMN_USER_NAME].Width = 135;
          dataGridView1.Columns[GRID_COLUMN_USER_NAME].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
          dataGridView1.Columns[GRID_COLUMN_TERMINAL].Width = 110;
          dataGridView1.Columns[GRID_COLUMN_TERMINAL].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
          dataGridView1.Columns[GRID_COLUMN_TYPE_NAME].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
          dataGridView1.Columns[GRID_COLUMN_TYPE_ID].Width = 0;
          dataGridView1.Columns[GRID_COLUMN_TYPE_ID].Visible = false;
          dataGridView1.Columns[GRID_COLUMN_STATUS_UNDO].Width = 0;
          dataGridView1.Columns[GRID_COLUMN_STATUS_UNDO].Visible = false;
          break;

        case MovementShow.Player_Tracking:
          dataGridView1.Columns[GRID_COLUMN_USER_NAME].Width = 0;
          dataGridView1.Columns[GRID_COLUMN_USER_NAME].Visible = false;
          dataGridView1.Columns[GRID_COLUMN_TERMINAL].Width = 150;
          dataGridView1.Columns[GRID_COLUMN_TERMINAL].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
          dataGridView1.Columns[GRID_COLUMN_PT_SECOND_ACCOUNT].Width = 110;
          dataGridView1.Columns[GRID_COLUMN_PT_SECOND_ACCOUNT].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
          dataGridView1.Columns[GRID_COLUMN_PT_SECOND_ACCOUNT].DefaultCellStyle.Format = "#,##0";
          dataGridView1.Columns[GRID_COLUMN_PT_INITIAL_AMOUNT].Width = 80;
          dataGridView1.Columns[GRID_COLUMN_PT_INITIAL_AMOUNT].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
          dataGridView1.Columns[GRID_COLUMN_PT_INITIAL_AMOUNT].DefaultCellStyle.Format = "#,##0";
          dataGridView1.Columns[GRID_COLUMN_PT_SUB_AMOUNT].Width = 80;
          dataGridView1.Columns[GRID_COLUMN_PT_SUB_AMOUNT].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
          dataGridView1.Columns[GRID_COLUMN_PT_SUB_AMOUNT].DefaultCellStyle.Format = "#,##0";
          dataGridView1.Columns[GRID_COLUMN_PT_ADD_AMOUNT].Width = 80;
          dataGridView1.Columns[GRID_COLUMN_PT_ADD_AMOUNT].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
          dataGridView1.Columns[GRID_COLUMN_PT_ADD_AMOUNT].DefaultCellStyle.Format = "#,##0";
          dataGridView1.Columns[GRID_COLUMN_PT_FINAL_AMOUNT].Width = 80;
          dataGridView1.Columns[GRID_COLUMN_PT_FINAL_AMOUNT].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
          dataGridView1.Columns[GRID_COLUMN_PT_FINAL_AMOUNT].DefaultCellStyle.Format = "#,##0";
          dataGridView1.Columns[GRID_COLUMN_TYPE_ID].Width = 0;
          dataGridView1.Columns[GRID_COLUMN_TYPE_ID].Visible = false;
          break;
      }

      //Set header alignment
      foreach (DataGridViewColumn _col in dataGridView1.Columns)
      {
        if (_col.Visible)
        {
          _col.HeaderCell.Style.Alignment = _col.DefaultCellStyle.Alignment;
        }
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Initialize control resources (NLS strings, images, etc).
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //

    public void InitializeControlResources()
    {
      // - NLS Strings:
      //   - Title:
      lbl_last_movements_title.Text = Resource.String("STR_FRM_LAST_MOVEMENTS_TITLE");
      this.FormTitle = Resource.String("STR_FRM_LAST_MOVEMENTS_TITLE");

      //   - Labels
      m_split_a_name_str = "SPLIT A NAME";
      m_split_b_name_str = "SPLIT B NAME";

      m_prize_taxes_1_name = "PRIZE TAXES 1";
      m_prize_taxes_2_name = "PRIZE TAXES 2";
      m_prize_taxes_3_name = "PRIZE TAXES 3";

      //   - Buttons
      btn_close.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CLOSE");

      // - Images:

      // - General Params Members:
      m_split2_tax_name = GeneralParam.GetString("Cashier", "Split.B.Tax.Name");

    } // InitializeControlResources

    //------------------------------------------------------------------------------
    // PURPOSE : 
    //
    //  PARAMS :
    //      - INPUT : 
    //               - IdxRow
    //
    //      - OUTPUT :
    //               - Row       
    //               - PrevOpId  
    //               - PrevOpDt  
    //               - PrevColor 
    //
    // RETURNS :
    //
    //   NOTES :
    //

    /// <summary>
    /// Setting the row color according to operation
    /// </summary>
    /// <param name="Row">Grid current row</param>
    /// <param name="PrevOpId">Previous operation id</param>
    /// <param name="PrevOpDt">Previous operation date</param>
    /// <param name="PrevOpColor">Previous operation color</param>
    private void SetColorRowAccordingToOperation(DataGridViewRow Row, ref Int64 PrevOpId, ref DateTime PrevOpDt, ref Color PrevOpColor)
    {
      Int64 _op_id;
      DateTime _op_dt;
      TimeSpan _dates_difference;
      Boolean _is_same_operation;

      _op_id = (Int64)Row.Cells[GRID_COLUMN_OPERATION_ID].Value;
      _op_dt = (DateTime)Row.Cells[GRID_COLUMN_DATE].Value;

      _is_same_operation = false;
      if (_op_id == PrevOpId)
      {
        _is_same_operation = true;

        if (PrevOpId == 0)
        {
          _dates_difference = (PrevOpDt - _op_dt);

          if (Math.Abs(_dates_difference.TotalMilliseconds) > 500)
          {
            _is_same_operation = false;
          }
        }
      }

      if (!_is_same_operation)
      {
        PrevOpId = _op_id;
        PrevOpDt = _op_dt;

        if (PrevOpColor == Color.White)
        {
          PrevOpColor = Color.LightSteelBlue;
        }
        else
        {
          PrevOpColor = Color.White;
        }
      }

      Row.Cells[GRID_COLUMN_ROW_COLOR].Value = PrevOpColor;
    }

    private String GetChipTypeDescription(CurrencyExchangeType CurrencyExchangeType, String IsoCode)
    {
      String _str_type;

      _str_type = String.Empty;
      if ((CurrencyExchangeType == WSI.Common.CurrencyExchangeType.CASINO_CHIP_RE)
            || (CurrencyExchangeType == WSI.Common.CurrencyExchangeType.CASINO_CHIP_NRE)
            || (CurrencyExchangeType == WSI.Common.CurrencyExchangeType.CASINO_CHIP_COLOR))
      {
        _str_type = FeatureChips.GetChipTypeDescription(CurrencyExchangeType, IsoCode);
      }

      return _str_type;
    }

    /// <summary>
    /// Returns the text of the Chips of the movement
    /// </summary>
    /// <param name="CurrencyExchangeType"></param>
    /// <param name="IsoCode"></param>
    /// <param name="AppendIsoCode"Append or not the Iso Code></param>
    /// <returns></returns>
    private String GetChipsMovementText(CurrencyExchangeType CurrencyExchangeType, String IsoCode, Boolean AppendIsoCode)
    {
      String _str_type;

      //Retrocompatibility with 'X01'
      if (IsoCode == Cage.CHIPS_ISO_CODE)
      {
        CurrencyExchangeType = CurrencyExchangeType.CASINO_CHIP_RE;
      }

      if (!AppendIsoCode)
      {
        IsoCode = String.Empty; //Return text without the currency code
      }

      _str_type = GetChipTypeDescription(CurrencyExchangeType, IsoCode); //Get the chips type description

      if (String.IsNullOrEmpty(_str_type))
      {
        _str_type = Resource.String("STR_REFUND_CASH"); //Cash text

        if (AppendIsoCode && !String.IsNullOrEmpty(IsoCode))
        {
          _str_type = String.Format("{0} ({1})", _str_type, IsoCode);
        }
      }

      return _str_type;
    }

    private String GetMovementTypeDescription(String Description, CurrencyExchangeType CurrencyExchangeType, String IsoCode, Boolean AppendIsoCode)
    {
      String _str_type;
      String _str_mov;

      _str_type = GetChipsMovementText(CurrencyExchangeType, IsoCode, AppendIsoCode);
      _str_mov = String.Format(Description, _str_type);

      return _str_mov;
    }

    #endregion

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Init and Show the form.
    //
    //  PARAMS :
    //      - INPUT :
    //          - SqlQuery
    //          - MovShow: type of information to show (Card / Cashier / Mobile Bank)
    //          - Name: identifier to be appended to screen title
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public void Show(String SqlQuery, MovementShow MovShow, String Name)
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_last_movements", Log.Type.Message);

      DataSet _ds;
      Int32 _movement_type;
      String _date_format;
      String _time_separator;
      String[] _level_name = new String[5];
      Int64 _prev_op_id;
      DateTime _prev_op_dt;
      Color _prev_row_color;
      String _promo_name;
      String _str_value;
      String[] _split;
      String _add_amount_str;
      String _tmp_iso_code;
      String _currency_iso_code;
      String _cash_in_tax_name;
      String _tax_provisions;
      Boolean _cash_in_tax_also_apply_to_split_b;
      Decimal _decimal_tmp;
      Decimal _aux_amount;
      Int32 _BucketMovementType;
      String _terminal_refilled = string.Empty;
      String _terminal_collected = string.Empty;
      BucketsForm _buckets_forms;
      CurrencyExchangeType _currency_exchange_type;

      _currency_iso_code = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode", "nothing");
      _cash_in_tax_also_apply_to_split_b = GeneralParam.GetBoolean("Cashier", "CashInTax.Enabled") & GeneralParam.GetBoolean("Cashier", "CashInTax.AlsoApplyToSplitB");

      _level_name[0] = "---";
      _level_name[1] = Misc.ReadGeneralParams("PlayerTracking", "Level01.Name");
      _level_name[2] = Misc.ReadGeneralParams("PlayerTracking", "Level02.Name");
      _level_name[3] = Misc.ReadGeneralParams("PlayerTracking", "Level03.Name");
      _level_name[4] = Misc.ReadGeneralParams("PlayerTracking", "Level04.Name");

      m_split_a_name_str = Misc.ReadGeneralParams("Cashier", "Split.A.Name");
      m_split_b_name_str = Misc.ReadGeneralParams("Cashier", "Split.B.Name");

      m_prize_taxes_1_name = Misc.ReadGeneralParams("Cashier", "Tax.OnPrize.1.Name");
      m_prize_taxes_2_name = Misc.ReadGeneralParams("Cashier", "Tax.OnPrize.2.Name");
      m_prize_taxes_3_name = Misc.ReadGeneralParams("Cashier", "Tax.OnPrize.3.Name");

      _cash_in_tax_name = GeneralParam.GetString("Cashier", "CashInTax.Name", Resource.String("STR_MOVEMENT_TYPE_CASH_IN_TAX"));

      // DHA 02-MAR-2016
      _tax_provisions = GeneralParam.GetString("Cashier.TaxProvisions", "Voucher.Title", Resource.String("STR_VOUCHER_TAX_PROVISIONS_TITLE"));

      // ORIGINAL FORM WIDTH VALUES
      this.Size = new Size(1024, 708);
      dataGridView1.Width = this.Width - 10;

      m_mov_show = MovShow;

      switch (m_mov_show)
      {
        case MovementShow.Card:
          // Check if current user is an authorized user
          // 03-FEB-2016 LA      Task 8875: TES35: Cashier: Add authorization login to last movements button.
          String error_str;
          if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.CardLastMovements, ProfilePermissions.TypeOperation.RequestPasswd, this, out error_str))
          {
            return;
          }
          lbl_last_movements_title.Text = Resource.String("STR_FRM_LAST_MOVEMENTS_CARD") + " " + Name;
          break;

        case MovementShow.Cashier:
          lbl_last_movements_title.Text = Resource.String("STR_FRM_LAST_MOVEMENTS_CASH_DESK") + " " + Name;
          break;

        case MovementShow.Mobile_Bank:
          lbl_last_movements_title.Text = Resource.String("STR_FRM_LAST_MOVEMENTS_MOBILE_BANK") + " " + Name;
          break;

        case MovementShow.Player_Tracking:
          lbl_last_movements_title.Text = Resource.String("STR_FRM_LAST_MOVEMENTS_POINTS") + " " + Name;
          break;

      } // switch

      FormTitle = lbl_last_movements_title.Text;

      _ds = new DataSet();

      try
      {
        using (SqlConnection _conn = WGDB.Connection())
        {
          using (SqlDataAdapter _da = new SqlDataAdapter(SqlQuery, _conn))
          {
            _da.Fill(_ds);
          }
        }

        //TODO: Refactor it 
        MovementServices service = new MovementServices();
        var movements = service.GetMovements();
        var movTable = new List<MovementTable>();
        int i = 0;
        foreach(var item in movements)
        {
          if(i < 20)
          {
            movTable.Add(new MovementTable()
            {
              CM_TYPE = item.Type,
              TYPE_NAME = string.Empty,
              CM_DATE = item.MovementDate,
              CM_USER_NAME = "USRREF",
              CT_NAME = "DAVID LUNA",
              CM_CASHIER_ID = item.CashierId,
              CM_INITIAL_BALANCE = 100,
              CM_SUB_AMOUNT_STR = "100",
              CM_ADD_AMOUNT_STR = "100",
              CM_FINAL_BALANCE = 100,
              COLOR_ROW = string.Empty,
              CM_DETAILS = string.Empty,
              CM_ADD_AMOUNT = 0,
              CM_SUB_AMOUNT = 0,
              CM_CURRENCY_ISO_CODE = string.Empty,
              CM_UNDO_STATUS = 0,
              CM_CURRENCY_DENOMINATION = 0,
              CM_AUX_AMOUNT = 0,
              CM_CAGE_CURRENCY_TYPE = 0

            });
          }
          
          i++;
        }
        var datetable = DataTableHelpers.ToDataTable<MovementTable>(movTable);
        dataGridView1.DataSource = datetable;
        //dataGridView1.DataSource = _ds.Tables[0];
      }
      catch (Exception _ex)
      {
        String _msg;
        _msg = _ex.Message;

        return;
      }

      _date_format = System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern;
      _time_separator = System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.TimeSeparator;

      // Columns      
      dataGridView1.Columns[GRID_COLUMN_TYPE_ID].Width = 0;
      dataGridView1.Columns[GRID_COLUMN_TYPE_ID].Visible = false;

      dataGridView1.Columns[GRID_COLUMN_TYPE_NAME].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
      dataGridView1.Columns[GRID_COLUMN_TYPE_NAME].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
      dataGridView1.Columns[GRID_COLUMN_DATE].Width = 150;
      dataGridView1.Columns[GRID_COLUMN_DATE].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
      dataGridView1.Columns[GRID_COLUMN_DATE].DefaultCellStyle.Format = _date_format + " HH" + _time_separator + "mm" + _time_separator + "ss";

      switch (m_mov_show)
      {
        case MovementShow.Card:
          SetColumnsVisibility();
          break;

        case MovementShow.Cashier:
          SetColumnsVisibility();
          break;

        case MovementShow.Mobile_Bank:
          SetColumnsVisibility();
          break;

        case MovementShow.Player_Tracking:
          SetColumnsVisibility();
          break;

      } // switch

      dataGridView1.Columns[GRID_COLUMN_TERM_ID].Width = 0;
      dataGridView1.Columns[GRID_COLUMN_TERM_ID].Visible = false;

      if (m_mov_show != MovementShow.Player_Tracking)
      {
        dataGridView1.Columns[GRID_COLUMN_INITIAL_AMOUNT].Width = 95;
        dataGridView1.Columns[GRID_COLUMN_INITIAL_AMOUNT].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        dataGridView1.Columns[GRID_COLUMN_INITIAL_AMOUNT].DefaultCellStyle.Format = "C" + Math.Abs(Format.GetFormattedDecimals(CurrencyExchange.GetNationalCurrency()));
        dataGridView1.Columns[GRID_COLUMN_SUB_AMOUNT].Width = 105;
        dataGridView1.Columns[GRID_COLUMN_SUB_AMOUNT].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
        dataGridView1.Columns[GRID_COLUMN_SUB_AMOUNT].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        dataGridView1.Columns[GRID_COLUMN_SUB_AMOUNT].DefaultCellStyle.Format = "C" + Math.Abs(Format.GetFormattedDecimals(CurrencyExchange.GetNationalCurrency()));
        dataGridView1.Columns[GRID_COLUMN_ADD_AMOUNT].Width = 85;
        dataGridView1.Columns[GRID_COLUMN_ADD_AMOUNT].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        dataGridView1.Columns[GRID_COLUMN_ADD_AMOUNT].DefaultCellStyle.Format = "C" + Math.Abs(Format.GetFormattedDecimals(CurrencyExchange.GetNationalCurrency()));
        dataGridView1.Columns[GRID_COLUMN_FINAL_AMOUNT].Width = 110;
        dataGridView1.Columns[GRID_COLUMN_FINAL_AMOUNT].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        dataGridView1.Columns[GRID_COLUMN_FINAL_AMOUNT].DefaultCellStyle.Format = "C" + Math.Abs(Format.GetFormattedDecimals(CurrencyExchange.GetNationalCurrency()));
      }

      if (m_mov_show == MovementShow.Mobile_Bank)
      {
        dataGridView1.Columns[GRID_COLUMN_SUB_AMOUNT].Width = 110;
      }

      // Datagrid Headers
      CreateDataGridHeaders();

      _prev_op_id = -1;
      _prev_op_dt = DateTime.MinValue;
      _prev_row_color = Color.White;

      foreach (DataGridViewRow _row in dataGridView1.Rows)
      {
        _movement_type = (Int32)_row.Cells[GRID_COLUMN_TYPE_ID].Value;
        //movement_type = (Int32)row.Cells[GRID_COLUMN_TYPE_ID].Value;

        if (m_mov_show == MovementShow.Player_Tracking)
        {
          if ((Decimal)_row.Cells[GRID_COLUMN_PT_SUB_AMOUNT].Value == 0)
          {
            _row.Cells[GRID_COLUMN_PT_SUB_AMOUNT].Value = DBNull.Value;
          }

          if (((Decimal)_row.Cells[GRID_COLUMN_PT_ADD_AMOUNT].Value) == 0)
          {
            _row.Cells[GRID_COLUMN_PT_ADD_AMOUNT].Value = DBNull.Value;
          }
        }
        else
        {
          if (m_mov_show == MovementShow.Cashier)
          {
            if ((Decimal)_row.Cells[GRID_COLUMN_ADD_AMOUNT_DECIMAL].Value == 0)
            {
              _row.Cells[GRID_COLUMN_ADD_AMOUNT].Value = "";
            }
            else
            {
              if (_row.Cells[GRID_COLUMN_CURRENCY_ISO_CODE].Value.ToString() == Cage.CHIPS_COLOR)
              {
                _row.Cells[GRID_COLUMN_ADD_AMOUNT].Value = Convert.ToInt32(_row.Cells[GRID_COLUMN_ADD_AMOUNT_DECIMAL].Value).ToString();
              }
              else
              {
                _row.Cells[GRID_COLUMN_ADD_AMOUNT].Value = ((Currency)Convert.ToDecimal(_row.Cells[GRID_COLUMN_ADD_AMOUNT_DECIMAL].Value)).ToString(false);
              }
            }

            if ((Decimal)_row.Cells[GRID_COLUMN_SUB_AMOUNT_DECIMAL].Value == 0)
            {
              _row.Cells[GRID_COLUMN_SUB_AMOUNT].Value = "";
            }
            else
            {
              if (_row.Cells[GRID_COLUMN_CURRENCY_ISO_CODE].Value.ToString() == Cage.CHIPS_COLOR)
              {
                _row.Cells[GRID_COLUMN_SUB_AMOUNT].Value = Convert.ToInt32(_row.Cells[GRID_COLUMN_SUB_AMOUNT_DECIMAL].Value).ToString();
              }
              else
              {
                _row.Cells[GRID_COLUMN_SUB_AMOUNT].Value = ((Currency)Convert.ToDecimal(_row.Cells[GRID_COLUMN_SUB_AMOUNT_DECIMAL].Value)).ToString(false);
              }
            }
          }
          else
          {
            // DHA 02-MAR-2016
            if ((Decimal)_row.Cells[GRID_COLUMN_ADD_AMOUNT].Value == 0 ||
                (m_mov_show == MovementShow.Card && ((MovementType)_movement_type == MovementType.CashInTax || (MovementType)_movement_type == MovementType.TaxProvisions || (MovementType)_movement_type == MovementType.TaxCustody)))
            {
              _row.Cells[GRID_COLUMN_ADD_AMOUNT].Value = DBNull.Value;
            }

            if ((Decimal)_row.Cells[GRID_COLUMN_SUB_AMOUNT].Value == 0)
            {
              _row.Cells[GRID_COLUMN_SUB_AMOUNT].Value = DBNull.Value;
            }

            if ((MovementType)_movement_type == MovementType.TaxReturnCustody)
            {
              _row.Cells[GRID_COLUMN_SUB_AMOUNT].Value = DBNull.Value;
            }

            // FBA 12-AUG-2013 sets ADD_AMOUNT column to blank if movement type is Deposit Cancelation
            // LEM 26-SEP-2013: Set blank row for movements that not modified balance
            switch ((MovementType)_movement_type)
            {
              case MovementType.Cancellation:
                _row.Cells[GRID_COLUMN_ADD_AMOUNT].Value = DBNull.Value;
                break;

              case MovementType.DrawTicketPrint:
                _row.Cells[GRID_COLUMN_INITIAL_AMOUNT].Value = DBNull.Value;
                _row.Cells[GRID_COLUMN_SUB_AMOUNT].Value = DBNull.Value;
                _row.Cells[GRID_COLUMN_ADD_AMOUNT].Value = DBNull.Value;
                _row.Cells[GRID_COLUMN_FINAL_AMOUNT].Value = DBNull.Value;
                break;

              case MovementType.CashdeskDrawParticipation:
                decimal _sub;
                _sub = 0;

                if (_row.Cells[GRID_COLUMN_SUB_AMOUNT].Value != DBNull.Value)
                {
                  decimal.TryParse(_row.Cells[GRID_COLUMN_SUB_AMOUNT].Value.ToString(), out _sub);
                }

                if (_sub == 0)
                {
                  _row.Cells[GRID_COLUMN_INITIAL_AMOUNT].Value = DBNull.Value;
                  _row.Cells[GRID_COLUMN_SUB_AMOUNT].Value = DBNull.Value;
                  _row.Cells[GRID_COLUMN_ADD_AMOUNT].Value = DBNull.Value;
                  _row.Cells[GRID_COLUMN_FINAL_AMOUNT].Value = DBNull.Value;
                }
                break;

              case MovementType.CashInTax:
              // DHA 02-MAR-2016
              case MovementType.TaxProvisions:
                _row.Cells[GRID_COLUMN_INITIAL_AMOUNT].Value = DBNull.Value;
                _row.Cells[GRID_COLUMN_FINAL_AMOUNT].Value = DBNull.Value;
                break;

              default:
                break;

            }
          }
        }

        //set Color According To Operation
        if (m_mov_show == MovementShow.Card)
        {
          SetColorRowAccordingToOperation(_row, ref _prev_op_id, ref _prev_op_dt, ref _prev_row_color);

          // ICS 16-APR-2013: Check if it's a Cashier Terminal (user@terminal) and use its alias instead
          _str_value = _row.Cells[GRID_COLUMN_TERMINAL].Value.ToString();
          _split = _str_value.Split('@');
          if (_split.Length == 2)
          {
            _row.Cells[GRID_COLUMN_TERMINAL].Value = _split[0] + "@" + Computer.Alias(_split[1]);
          }

          _buckets_forms = new BucketsForm();
          String _bucket_name = "";

          switch ((MovementType)_movement_type)
          {
            case MovementType.Play:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_001");
              break;

            case MovementType.CashIn:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = m_split_a_name_str;
              break;

            case MovementType.CashOut:
              if (Misc.IsVoucherModeTV())
              {
                _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_RAFFLE_PRICE_PAYMENT");
              }
              else
              {
                _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_003");
              }
              break;

            // DHA 25-APR-2014 modified/added TITO tickets movements
            case MovementType.TITO_TicketCashierPrintedCashable:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_UC_TITO_TICKET_CASHIER_PRINTED_CASHABLE");
              break;

            case MovementType.TITO_TicketCashierPrintedPromoRedeemable:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_UC_TITO_TICKET_CASHIER_PRINTED_PROMO_REDEEMABLE");
              break;

            case MovementType.TITO_TicketCashierPrintedPromoNotRedeemable:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_UC_TITO_TICKET_CASHIER_PRINTED_PROMO_NOT_REDEEMABLE");
              break;

            case MovementType.TITO_TicketPromoBoxPrintedPromoNotRedeemable:
              // FAV 20-NOV-2015
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_UC_TITO_TICKET_PROMOBOX_PRINTED_PROMO_NOT_REDEEMABLE");
              break;

            case MovementType.TITO_TicketPromoBoxPrintedPromoRedeemable:
              // FAV 03-DIC-2015
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_UC_TITO_TICKET_PROMOBOX_PRINTED_PROMO_REDEEMABLE");
              break;

            case MovementType.TITO_TicketMachinePrintedCashable:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_UC_TITO_TICKET_MACHINE_PRINTED_CASHABLE");
              break;

            case MovementType.TITO_TicketCountRPrintedCashable:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_UC_TITO_TICKET_COUNTR_PRINTED_CASHABLE");
              break;

            case MovementType.TITO_TicketMachinePrintedPromoNotRedeemable:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_UC_TITO_TICKET_MACHINE_PRINTED_PROMO_NOT_REDEEMABLE");
              break;

            case MovementType.TITO_TicketCashierPaidCashable:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_UC_TITO_TICKET_CASHIER_PAID_CASHABLE");
              break;

            case MovementType.TITO_TicketCashierPaidPromoRedeemable:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_UC_TITO_TICKET_CASHIER_PAID_PROMO_REDEEMABLE");
              break;

            case MovementType.TITO_TicketCashierPaidHandpay:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_UC_TITO_TICKET_CASHIER_PAID_HANDPAY");
              break;

            case MovementType.TITO_TicketCashierPaidJackpot:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_UC_TITO_TICKET_CASHIER_PAID_JACKPOT");
              break;

            case MovementType.TITO_TicketMachinePlayedCashable:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_UC_TITO_TICKET_MACHINE_PLAYED_CASHABLE");
              break;

            case MovementType.TITO_TicketMachinePlayedPromoRedeemable:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_UC_TITO_TICKET_MACHINE_PLAYED_PROMO_REDEEMABLE");
              break;

            case MovementType.TITO_ticketMachinePlayedPromoNotRedeemable:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_UC_TITO_TICKET_MACHINE_PLAYED_PROMO_NOT_REDEEMABLE");
              break;

            case MovementType.TITO_TicketReissue:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_UC_TITO_TICKET_REISSUE");
              break;

            case MovementType.TITO_AccountToTerminalCredit:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_MOV_TITO_ACCOUNT_TERMINAL");
              break;

            case MovementType.TITO_TerminalToAccountCredit:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_MOV_TITO_TERMINAL_ACCOUNT");
              break;

            case MovementType.TITO_TicketMachinePrintedHandpay:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_UC_TITO_TICKET_MACHINE_PRINTED_HANDPAY");
              break;

            case MovementType.TITO_TicketMachinePrintedJackpot:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_UC_TITO_TICKET_MACHINE_PRINTED_JACKPOT");
              break;

            case MovementType.TITO_TicketCashierExpiredPaidCashable:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_UC_TITO_TICKET_CASHIER_PAID_CASHABLE");
              break;

            case MovementType.TITO_TicketCashierExpiredPaidPromoRedeemable:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_UC_TITO_EXPIRED_TICKET_CASHIER_PAID_PROMO_REDEEMABLE");
              break;

            case MovementType.TITO_TicketCashierExpiredPaidJackpot:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_UC_TITO_EXPIRED_TICKET_CASHIER_PAID_JACKPOT");
              break;

            case MovementType.TITO_TicketCashierExpiredPaidHandpay:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_UC_TITO_EXPIRED_TICKET_CASHIER_PAID_HANDPAY");
              break;

            case MovementType.Devolution:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_004");
              break;

            case MovementType.TaxOnPrize1:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = m_prize_taxes_1_name;
              break;

            case MovementType.TaxOnPrize2:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = m_prize_taxes_2_name;
              break;

            case MovementType.TaxOnPrize3:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = m_prize_taxes_3_name;
              break;

            // LA 26-FEB-2016
            case MovementType.PromotionRedeemableFederalTax:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = String.Format("{0} - {1}", Resource.String("STR_RE_PROMO_NAME_MOVEMENT"), m_prize_taxes_1_name);
              break;

            case MovementType.PromotionRedeemableStateTax:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = String.Format("{0} - {1}", Resource.String("STR_RE_PROMO_NAME_MOVEMENT"), m_prize_taxes_2_name);
              break;

            case MovementType.PromotionRedeemableCouncilTax:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = String.Format("{0} - {1}", Resource.String("STR_RE_PROMO_NAME_MOVEMENT"), m_prize_taxes_3_name);
              break;

            case MovementType.StartCardSession:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_007");
              break;

            case MovementType.CancelStartSession:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_066");
              break;

            case MovementType.EndCardSession:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_008");
              break;

            case MovementType.PromotionNotRedeemable:
              _promo_name = _row.Cells[GRID_COLUMN_DETAILS].Value == System.DBNull.Value ? "" : _row.Cells[GRID_COLUMN_DETAILS].Value.ToString();

              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_009");
              if (!String.IsNullOrEmpty(_promo_name))
              {
                _row.Cells[GRID_COLUMN_TYPE_NAME].Value += " (" + _promo_name + ")";
              }
              break;

            case MovementType.CashInCoverCoupon:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_062");
              break;

            case MovementType.DepositIn:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_010");
              break;

            case MovementType.DepositOut:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_011");
              break;

            case MovementType.CardReplacement:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_036");
              break;

            case MovementType.CancelNotRedeemable:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_012");
              break;

            case MovementType.PromoCredits:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_015");
              break;

            case MovementType.PromoToRedeemable:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_016");
              break;

            case MovementType.PromoExpired:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_017");
              break;

            case MovementType.PromoCancel:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_018");
              break;

            case MovementType.PromoStartSession:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_019");
              break;

            case MovementType.PromoEndSession:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_020");
              break;

            case MovementType.CreditsExpired:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_021");
              break;

            case MovementType.PrizeExpired:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_056");
              break;

            case MovementType.ReservedExpired:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_MOVEMENT_TYPE_RESERVED_EXPIRED");
              break;

            case MovementType.CreditsNotRedeemableExpired:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_022");
              break;

            case MovementType.CreditsNotRedeemable2Expired:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_063");
              break;

            case MovementType.DrawTicketPrint:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_037");
              break;

            case MovementType.PointsToDrawTicketPrint:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_050");
              break;

            case MovementType.Handpay:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_038");
              break;

            case MovementType.HandpayCancellation:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_039");
              break;

            case MovementType.ManualEndSession:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_040");
              break;

            case MovementType.PromoManualEndSession:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_041");
              break;

            case MovementType.ManualHandpay:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_042");
              break;

            case MovementType.ManualHandpayCancellation:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_043");
              break;

            case MovementType.PointsAwarded:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_044");
              break;

            case MovementType.PointsToGiftRequest:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_045");
              break;

            case MovementType.PointsToNotRedeemable:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_046");
              break;

            case MovementType.PointsToRedeemable:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_055");
              break;

            case MovementType.PointsGiftDelivery:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_032");
              break;

            case MovementType.PointsGiftServices:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_051");
              break;

            case MovementType.PointsExpired:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_034");
              break;

            case MovementType.CancelGiftInstance:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_075");
              break;

            // RRB 29-AUG-2012 Added ManualHolderLevelChanged
            case MovementType.HolderLevelChanged:
            case MovementType.ManualHolderLevelChanged:
              {
                String _new_level;
                String _old_level;
                Decimal _value;

                _value = 0;
                if (_row.Cells[GRID_COLUMN_ADD_AMOUNT].Value != DBNull.Value)
                {
                  _value = (Decimal)_row.Cells[GRID_COLUMN_ADD_AMOUNT].Value;
                }
                _new_level = _level_name[((Int32)_value) % 5];

                _value = 0;
                if (_row.Cells[GRID_COLUMN_SUB_AMOUNT].Value != DBNull.Value)
                {
                  _value = (Decimal)_row.Cells[GRID_COLUMN_SUB_AMOUNT].Value;
                }

                _old_level = _level_name[((Int32)_value) % 5];

                _row.Cells[GRID_COLUMN_TYPE_NAME].Value = (_movement_type == (int)MovementType.HolderLevelChanged ? Resource.String("STR_FRM_LAST_MOVEMENTS_052", _new_level, _old_level) : Resource.String("STR_FRM_LAST_MOVEMENTS_073", _new_level, _old_level));

                _row.Cells[GRID_COLUMN_ADD_AMOUNT].Value = DBNull.Value;
                _row.Cells[GRID_COLUMN_SUB_AMOUNT].Value = DBNull.Value;
              }
              break;

            case MovementType.PrizeCoupon:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_053");
              break;

            case MovementType.DEPRECATED_RedeemableSpentUsedInPromotions:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_054");
              break;

            // MBF 11-OCT-2011
            case MovementType.CardCreated:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_057");
              break;

            case MovementType.AccountPersonalization:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_058");
              break;

            case MovementType.AccountPINChange:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_059");
              break;

            case MovementType.AccountPINRandom:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_060");
              break;

            // JCA 25-JAN-2012
            case MovementType.ManuallyAddedPointsOnlyForRedeem:
              _BucketMovementType = Convert.ToInt32(MovementType.MULTIPLE_BUCKETS_Manual_Add) + 2;
              if (_buckets_forms.GetBucketName((MovementType)_BucketMovementType, out _bucket_name))
              {
                _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_BUCKET_ADD_MOVEMENT", _bucket_name);
              }
              else
              {
                _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DEFAULT_MOVEMENT", ((int)_movement_type).ToString());
              }
              break;

            // JCM 24-APR-2012
            case MovementType.CardRecycled:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_067");
              break;

            // DDM 05-JUL-2012
            case MovementType.PromotionRedeemable:
              if (Misc.IsVoucherModeTV())
              {
                _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_CUSTOMER_CREDIT_OTHERS");
              }
              else
              {
                _promo_name = _row.Cells[GRID_COLUMN_DETAILS].Value == System.DBNull.Value ? "" : _row.Cells[GRID_COLUMN_DETAILS].Value.ToString();

                _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_068");
                if (!String.IsNullOrEmpty(_promo_name))
                {
                  _row.Cells[GRID_COLUMN_TYPE_NAME].Value += " (" + _promo_name + ")";
                }
              }
              break;

            case MovementType.CancelPromotionRedeemable:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_069");
              break;

            // DDM 31-JUL-2012
            case MovementType.PromotionPoint:
              _promo_name = _row.Cells[GRID_COLUMN_DETAILS].Value == System.DBNull.Value ? "" : _row.Cells[GRID_COLUMN_DETAILS].Value.ToString();

              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_070");
              if (!String.IsNullOrEmpty(_promo_name))
              {
                _row.Cells[GRID_COLUMN_TYPE_NAME].Value += " (" + _promo_name + ")";
              }
              break;

            case MovementType.CancelPromotionPoint:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_071");
              break;

            case MovementType.TaxReturningOnPrizeCoupon:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_TAX_RETURNING_ON_PRIZE_COUPON");
              break;

            case MovementType.DecimalRounding:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DECIMAL_ROUNDING");
              break;

            case MovementType.ServiceCharge:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_SERVICE_CHARGE");
              break;

            // JMM 06-FEB-2013
            case MovementType.PointsStatusChanged:
              ACCOUNT_POINTS_STATUS _new_points_status;
              ACCOUNT_POINTS_STATUS _old_points_status;
              String _new_pt_status;
              String _old_pt_status;

              _new_points_status = 0;
              _old_points_status = 0;

              _new_pt_status = "";
              _old_pt_status = "";

              if (_row.Cells[GRID_COLUMN_ADD_AMOUNT].Value != DBNull.Value)
              {
                _new_points_status = (ACCOUNT_POINTS_STATUS)Convert.ToInt32(_row.Cells[GRID_COLUMN_ADD_AMOUNT].Value);
              }

              if (_row.Cells[GRID_COLUMN_SUB_AMOUNT].Value != DBNull.Value)
              {
                _old_points_status = (ACCOUNT_POINTS_STATUS)Convert.ToInt32(_row.Cells[GRID_COLUMN_SUB_AMOUNT].Value);
              }

              _new_pt_status = GetPointsStatusDescription(_new_points_status);
              _old_pt_status = GetPointsStatusDescription(_old_points_status);

              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_POINTS_STATUS_MOVEMENT", _new_pt_status, _old_pt_status);
              _row.Cells[GRID_COLUMN_SUB_AMOUNT].Value = DBNull.Value;
              _row.Cells[GRID_COLUMN_ADD_AMOUNT].Value = DBNull.Value;
              break;

            case MovementType.ManuallyAddedPointsForLevel:                     //= 68, Add points from GUI
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_MOVEMENT_TYPE_MANUALLY_ADDED_POINTS_FOR_LEVEL");
              break;

            case MovementType.ImportedAccount:                                 //= 70, Imported account
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_MOVEMENT_TYPE_IMPORTED_ACCOUNT");
              break;

            case MovementType.ImportedPointsOnlyForRedeem:                     //= 71, Imported points (not accounted for the level)
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_MOVEMENT_TYPE_IMPORTED_POINTS_ONLY_FOR_REDEEM");
              break;

            case MovementType.ImportedPointsForLevel:                          //= 72, Imported points
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_MOVEMENT_TYPE_IMPORTED_POINTS_FOR_LEVEL");
              break;

            case MovementType.ImportedPointsHistory:                           //= 73, History points imported
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_MOVEMENT_TYPE_IMPORTED_POINTS_HISTORY");
              break;

            case MovementType.AccountBlocked:                                  //= 75, Block account movement
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_MOVEMENT_TYPE_BLOCKED");
              break;

            case MovementType.AccountUnblocked:                                //= 76, Unblock account movement
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_MOVEMENT_TYPE_UNBLOCKED");
              break;

            case MovementType.AccountInBlacklist:                              //= 240, Account add in blacklist
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_MOVEMENT_TYPE_ADD_TO_BLACKLIST");
              break;

            case MovementType.AccountNotInBlacklist:                           //= 241,Account remove to blacklist
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_MOVEMENT_TYPE_REMOVE_FROM_BLACKLIST");
              break;

            // FBA 12-AUG-2013 new movement ID 77. Deposit Cancelation
            case MovementType.Cancellation:                                    //= 77, Deposit Cancelation
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DEPOSIT_CANCELATION");
              break;

            case MovementType.MultiSiteCurrentLocalPoints:                     //= 101, Multisite - Current points in site // similar ManuallyAddedPointsOnlyForRedeem
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_MOVEMENT_TYPE_MULTI_SITE_CURRENT_LOCAL_POINTS");
              break;

            case MovementType.CashdeskDrawParticipation:                       //= 106, Participation in a Draw
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_091");
              break;

            case MovementType.CashdeskDrawPlaySession:                         //= 201, Cash Desk Draw PlaySession
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_MOVEMENT_TYPE_CASH_DESK_DRAW_PLAY_SESSION");
              break;

            case MovementType.ChipsPurchaseTotal:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_MOVEMENT_TYPE_ACCOUNT_CHIPS_PURCHASE_TOTAL");
              break;

            case MovementType.ChipsSaleTotal:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_MOVEMENT_TYPE_ACCOUNT_CHIPS_SALES_TOTAL");
              break;

            case MovementType.ChipsSaleRemainingAmount:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_MOVEMENT_TYPE_CHIP_PURCHASE_REMAINING_AMOUNT");
              break;

            case MovementType.ChipsSaleConsumedRemainingAmount:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_MOVEMENT_TYPE_CHIP_PURCHASE_CONSUMED_REMAINING_AMOUNT");
              break;

            case MovementType.ChipsSaleDevolutionForTito:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_MOVEMENT_TYPE_ACCOUNT_CHIPS_SALES_DEVOLUTION_FOR_TITO");
              break;
            // LJM

            // DLL 02-JAN-2014
            case MovementType.CashInTax:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = GeneralParam.GetString("Cashier", "CashInTax.Name", Resource.String("STR_MOVEMENT_TYPE_CASH_IN_TAX"));
              break;

            // DHA 02-MAR-2016
            case MovementType.TaxProvisions:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = GeneralParam.GetString("Cashier.TaxProvisions", "Voucher.Title", Resource.String("STR_VOUCHER_TAX_PROVISIONS_TITLE"));
              break;

            // RMS 14-JAN-2013
            case MovementType.TransferCreditOut:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_094");
              break;

            case MovementType.TransferCreditIn:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_095");
              break;

            // RCI & DRV 08-JUL-2014
            //case MovementType.ChipsSaleWithCashIn:
            //  row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_CHIPS_SALES_WITH_CACH_IN");
            //  break;
            //case MovementType.ChipsPurchaseWithCashOut:
            //  row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_CHIPS_PURCHASE_WITH_CASH_OUT");
            //  break;

            case MovementType.CardReplacementForFree:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_FREE_CARD");
              break;

            case MovementType.CardReplacementInPoints:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_BY_POINTS");
              break;

            case MovementType.CashAdvance:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_VOUCHER_CASH_ADVANCE");
              break;

            case MovementType.ExternalSystemPointsSubstract:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_108");
              break;

            //FOS 01-JUL-2015 
            case MovementType.CardReplacementCheck:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_110");
              break;

            case MovementType.CardReplacementCardCredit:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_112");
              break;

            case MovementType.CardReplacementCardDebit:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_113");
              break;

            case MovementType.CardReplacementCardGeneric:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_114");
              break;

            case MovementType.CardDepositInCheck:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_115");
              break;

            case MovementType.CardDepositInCurrencyExchange:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_116");
              break;

            case MovementType.CardDepositInCardCredit:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_117");
              break;

            case MovementType.CardDepositInCardDebit:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_118");
              break;

            case MovementType.CardDepositInCardGeneric:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_119");
              break;

            case MovementType.WinLossStatementRequest:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_WIN_LOSS_STATEMENT_MOVEMENT");
              break;

            case MovementType.GameGatewayReserveCredit:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_GAME_GATEWAY_RESERVE_CREDIT_MOVEMENT");
              break;

            case MovementType.GameGatewayBet:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_GAME_GATEWAY_BET_MOVEMENT", GeneralParam.GetString("GameGateway", "Name", "BonoPlay"));
              break;

            case MovementType.GameGatewayPrize:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_GAME_GATEWAY_PRIZE_MOVEMENT", GeneralParam.GetString("GameGateway", "Name", "BonoPlay"));
              break;

            case MovementType.GameGatewayBetRollback:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_GAME_GATEWAY_ROLLBACK_MOVEMENT", GeneralParam.GetString("GameGateway", "Name", "BonoPlay"));
              break;

            // JBP 13-09-2016: PariPlay movements
            case MovementType.PariPlayBet:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_GAME_GATEWAY_BET_MOVEMENT", GeneralParam.GetString("PariPlay", "Name", "PariPlay"));
              break;

            case MovementType.PariPlayPrize:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_GAME_GATEWAY_PRIZE_MOVEMENT", GeneralParam.GetString("PariPlay", "Name", "PariPlay"));
              break;

            case MovementType.PariPlayBetRollback:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_GAME_GATEWAY_ROLLBACK_MOVEMENT", GeneralParam.GetString("PariPlay", "Name", "PariPlay"));
              break;

            // RAB 24-MAR-2016: Bug 10745
            case MovementType.CardAssociate:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("BTN_CARD_FUNC_ASSIGN");
              break;

            case MovementType.CurrencyExchangeCashierIn:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_CURRENCY_IN");
              break;

            case MovementType.CurrencyExchangeCashierOut:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_CURRENCY_OUT");
              break;

            case MovementType.TaxCustody:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Misc.TaxCustodyName();
              break;

            case MovementType.TaxReturnCustody:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Misc.TaxCustodyRefundName();
              break;

            // XGJ 07-AGU-2016
            case MovementType.Admission:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_MOV_RECEPTION_ENTRY");
              break;

            case MovementType.CreditLineCreate:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_MOV_CREDITLINE_CREATION");
              break;

            case MovementType.CreditLineUpdate:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_MOV_CREDITLINE_UPDATE");
              break;

            case MovementType.CreditLineApproved:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_MOV_CREDITLINE_APPROVED");
              break;

            case MovementType.CreditLineNotApproved:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_MOV_CREDITLINE_NOT_APPROVED");
              break;

            case MovementType.CreditLineSuspended:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_MOV_CREDITLINE_SUSPENDED");
              break;

            case MovementType.CreditLineExpired:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_MOV_CREDITLINE_EXPIRED");
              break;

            case MovementType.CreditLineGetMarker:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_MOV_CREDITLINE_GETMARKER");
              break;

            case MovementType.CreditLinePayback:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_MOV_CREDITLINE_PAYBACK");
              break;

            case MovementType.CreditLineGetMarkerCardReplacement:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_MOV_CREDITLINE_GETMARKER_CARD_REPLACEMENT");
              break;

            default:
              // RAB 20-JAN-2016
              if ((MovementType)_movement_type > MovementType.MULTIPLE_BUCKETS_EndSession && (MovementType)_movement_type <= MovementType.MULTIPLE_BUCKETS_Manual_Set_Last) // (1100 - 1599)
              {
                // There are no ranks, this switch is implemented to cover multiple buckets ranges.
                // First, we divide _movements by the range and then check its range starting with 11
                Int32 _movement_base = Convert.ToInt32((MovementType)_movement_type) / 100;
                _movement_base = _movement_base * 100;

                if (_buckets_forms.GetBucketName((MovementType)_movement_type, out _bucket_name))
                {
                  switch ((MovementType)_movement_base)
                  {
                    case MovementType.MULTIPLE_BUCKETS_EndSession: //MULTIPLE_BUCKETS_EndSession Range (1100 to 1199)
                      _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_BUCKET_EARNED_MOVEMENT", _bucket_name);
                      break;
                    case MovementType.MULTIPLE_BUCKETS_Expired:    //MULTIPLE_BUCKETS_Expired Range (1200 to 1299)
                      _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_BUCKET_EXPIRED_MOVEMENT", _bucket_name);
                      break;
                    case MovementType.MULTIPLE_BUCKETS_Manual_Add: //MULTIPLE_BUCKETS_Manual_Add Range (1300 to 1399)
                      _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_BUCKET_ADD_MOVEMENT", _bucket_name);
                      break;
                    case MovementType.MULTIPLE_BUCKETS_Manual_Sub: //MULTIPLE_BUCKETS_MANUAL_Sub Range (1400 to 1499)     
                      _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_BUCKET_SUB_MOVEMENT", _bucket_name);
                      break;
                    case MovementType.MULTIPLE_BUCKETS_Manual_Set: //MULTIPLE_BUCKETS_Manual_Set Range (1500 to 1599)
                      _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_BUCKET_SET_MOVEMENT", _bucket_name);
                      break;
                  }

                  break;
                }
              }

              // FBA 12-AUG-2013 - Movement not defined -> Movement (XX)
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DEFAULT_MOVEMENT", ((int)_movement_type).ToString());
              break;
          }

          if (_row.Cells[GRID_COLUMN_CARD_STATUS].Value != DBNull.Value
            && (UndoStatus)_row.Cells[GRID_COLUMN_CARD_STATUS].Value == UndoStatus.UndoOperation)
          {
            _str_value = "";
            _str_value = "(" + Resource.String("STR_FRM_LAST_UNDO") + ") " + _row.Cells[GRID_COLUMN_TYPE_NAME].Value;
            _row.Cells[GRID_COLUMN_TYPE_NAME].Value = _str_value;
          }
        }
        else if (m_mov_show == MovementShow.Cashier)
        {
          // ICS 16-APR-2013: Change the cashier name by its alias
          _row.Cells[GRID_COLUMN_TERMINAL].Value = Computer.Alias(_row.Cells[GRID_COLUMN_TERMINAL].Value.ToString());

          switch ((CASHIER_MOVEMENT)_movement_type)
          {
            case CASHIER_MOVEMENT.REOPEN_CASHIER:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_REOPEN_SESSION");
              break;

            case CASHIER_MOVEMENT.OPEN_SESSION:
              if (_row.Cells[GRID_COLUMN_CURRENCY_ISO_CODE].Value.ToString() == "")
              {
                _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_023");
                if (_row.Cells[GRID_COLUMN_ADD_AMOUNT].Value.ToString() == "")
                {
                  _row.Cells[GRID_COLUMN_ADD_AMOUNT].Value = 0;
                }

                _row.Cells[GRID_COLUMN_ADD_AMOUNT].Value = ((Currency)Convert.ToDecimal(_row.Cells[GRID_COLUMN_ADD_AMOUNT].Value)).ToString(false);
              }
              else
              {
                _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_OPEN_CASH_CURRENCY")
                                                                                 + " (" + _row.Cells[GRID_COLUMN_CURRENCY_ISO_CODE].Value.ToString() + ")";
                if (_row.Cells[GRID_COLUMN_CURRENCY_ISO_CODE].Value.ToString() != Cage.CHIPS_COLOR)
                {
                  _row.Cells[GRID_COLUMN_ADD_AMOUNT].Value = Currency.Format((Decimal)_row.Cells[GRID_COLUMN_ADD_AMOUNT_DECIMAL].Value,
                                                                                                     _row.Cells[GRID_COLUMN_CURRENCY_ISO_CODE].Value.ToString());
                }
              }
              break;

            case CASHIER_MOVEMENT.CLOSE_SESSION:
              String _current_iso_code;
              CageCurrencyType _current_cage_currency_type;

              _current_iso_code = _row.Cells[GRID_COLUMN_CURRENCY_ISO_CODE].Value.ToString();
              _current_cage_currency_type = (CageCurrencyType)_row.Cells[GRID_COLUMN_CAGE_CURRENCY_TYPE].Value;

              if (String.IsNullOrEmpty(_current_iso_code) && (_current_cage_currency_type == CageCurrencyType.Bill || _current_cage_currency_type == CageCurrencyType.Coin))
              {
                GetTypeNameColumn(Resource.String("STR_FRM_LAST_MOVEMENTS_024") + " - " + Resource.String("STR_VOUCHER_TOTAL_AMOUNT"), _row);
              }
              else
              {
                GetTypeNameColumn(Resource.String("STR_MOV_CAGE_CLOSE_SESSION") + " - " + Resource.String("STR_VOUCHER_TOTAL_AMOUNT"), _row);
              }

              if (_current_cage_currency_type == CageCurrencyType.ChipsColor)
              {
                _row.Cells[GRID_COLUMN_SUB_AMOUNT].Value = Convert.ToInt32(_row.Cells[GRID_COLUMN_FINAL_AMOUNT].Value);
              }
              else
              {
                _row.Cells[GRID_COLUMN_SUB_AMOUNT].Value = ((Currency)Convert.ToDecimal(_row.Cells[GRID_COLUMN_FINAL_AMOUNT].Value)).ToString(false);
              }
              break;

            case CASHIER_MOVEMENT.CASH_IN:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Misc.IsVoucherModeTV() ? Resource.String("STR_FRM_LAST_MOVEMENTS_REQUEST") : Resource.String("STR_FRM_LAST_MOVEMENTS_026");
              break;

            case CASHIER_MOVEMENT.CASH_OUT:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_027");
              break;

            // DHA 25-APR-2014 modified/added TITO tickets movements
            case CASHIER_MOVEMENT.TITO_TICKET_CASHIER_PRINTED_CASHABLE:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_UC_TITO_TICKET_CASHIER_PRINTED_CASHABLE");
              break;

            case CASHIER_MOVEMENT.TITO_TICKET_CASHIER_PRINTED_PROMO_REDEEMABLE:
              // JPJ 29-JAN-2014
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_UC_TITO_TICKET_CASHIER_PRINTED_PROMO_REDEEMABLE");
              break;

            case CASHIER_MOVEMENT.TITO_TICKET_CASHIER_PRINTED_PROMO_NOT_REDEEMABLE:
              // JPJ 29-JAN-2014
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_UC_TITO_TICKET_CASHIER_PRINTED_PROMO_NOT_REDEEMABLE");
              break;

            case CASHIER_MOVEMENT.TITO_TICKET_PROMOBOX_PRINTED_PROMO_REDEEMABLE:
              // FAV 03-DIC-2015
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_UC_TITO_TICKET_PROMOBOX_PRINTED_PROMO_REDEEMABLE");
              break;

            case CASHIER_MOVEMENT.TITO_TICKET_PROMOBOX_PRINTED_PROMO_NOT_REDEEMABLE:
              // FAV 20-NOV-2015
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_UC_TITO_TICKET_PROMOBOX_PRINTED_PROMO_NOT_REDEEMABLE");
              break;

            case CASHIER_MOVEMENT.TITO_TICKET_MACHINE_PRINTED_CASHABLE:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_UC_TITO_TICKET_MACHINE_PRINTED_CASHABLE");
              break;

            case CASHIER_MOVEMENT.TITO_TICKET_COUNTR_PRINTED_CASHABLE:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_UC_TITO_TICKET_COUNTR_PRINTED_CASHABLE");
              break;

            case CASHIER_MOVEMENT.TITO_TICKET_MACHINE_PRINTED_PROMO_NOT_REDEEMABLE:
              // JPJ 29-JAN-2014
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_UC_TITO_TICKET_CASHIER_PAID_PROMO_REDEEMABLE");
              break;

            case CASHIER_MOVEMENT.TITO_TICKET_CASHIER_PAID_CASHABLE:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_UC_TITO_TICKET_CASHIER_PAID_CASHABLE");
              break;

            case CASHIER_MOVEMENT.TITO_TICKET_CASHIER_PAID_PROMO_REDEEMABLE:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_UC_TITO_TICKET_CASHIER_PAID_PROMO_REDEEMABLE");
              break;

            case CASHIER_MOVEMENT.TITO_TICKET_CASHIER_PAID_HANDPAY:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_UC_TITO_TICKET_CASHIER_PAID_HANDPAY");
              break;

            case CASHIER_MOVEMENT.TITO_TICKET_CASHIER_PAID_JACKPOT:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_UC_TITO_TICKET_CASHIER_PAID_JACKPOT");
              break;

            case CASHIER_MOVEMENT.TITO_TICKET_MACHINE_PLAYED_CASHABLE:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_UC_TITO_TICKET_MACHINE_PLAYED_CASHABLE");
              break;

            case CASHIER_MOVEMENT.TITO_TICKET_MACHINE_PLAYED_PROMO_REDEEMABLE:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_UC_TITO_TICKET_MACHINE_PLAYED_PROMO_REDEEMABLE");
              break;

            case CASHIER_MOVEMENT.TITO_TICKET_MACHINE_PLAYED_PROMO_NOT_REDEEMABLE:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_UC_TITO_TICKET_MACHINE_PLAYED_PROMO_NOT_REDEEMABLE");
              break;

            case CASHIER_MOVEMENT.TITO_TICKET_REISSUE:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_UC_TITO_TICKET_REISSUE");
              break;

            case CASHIER_MOVEMENT.TITO_TICKET_CASHIER_EXPIRED_PAID_CASHABLE:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_UC_TITO_TICKET_EXPIRED_CASHIER_PAID_CASHABLE");
              break;

            case CASHIER_MOVEMENT.TITO_TICKET_CASHIER_EXPIRED_PAID_PROMO_REDEEMABLE:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_UC_TITO_EXPIRED_TICKET_CASHIER_PAID_PROMO_REDEEMABLE");
              break;

            case CASHIER_MOVEMENT.TITO_TICKET_CASHIER_EXPIRED_PAID_JACKPOT:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_UC_TITO_EXPIRED_TICKET_CASHIER_PAID_JACKPOT");
              break;

            case CASHIER_MOVEMENT.TITO_TICKET_CASHIER_EXPIRED_PAID_HANDPAY:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_UC_TITO_EXPIRED_TICKET_CASHIER_PAID_HANDPAY");
              break;

            // DHA 08-APR-2016: Floor Dual Currency
            case CASHIER_MOVEMENT.TITO_TICKETS_CREATE_EXCHANGE_DUAL_CURRENCY:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FLOOR_DUAL_CURRENCY_TICKET_CREATE_EXCHANGE");
              _add_amount_str = Currency.Format((Decimal)_row.Cells[GRID_COLUMN_INITIAL_AMOUNT].Value,
                                                _row.Cells[GRID_COLUMN_CURRENCY_ISO_CODE].Value.ToString());
              _add_amount_str += " = ";
              _add_amount_str += Currency.Format((Decimal)_row.Cells[GRID_COLUMN_ADD_AMOUNT_DECIMAL].Value, CurrencyExchange.GetNationalCurrency());

              _row.Cells[GRID_COLUMN_ADD_AMOUNT].Value = _add_amount_str;
              break;

            case CASHIER_MOVEMENT.TAX_ON_PRIZE1:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = m_prize_taxes_1_name;
              _row.Cells[GRID_COLUMN_INITIAL_AMOUNT].Value = DBNull.Value;
              _row.Cells[GRID_COLUMN_FINAL_AMOUNT].Value = DBNull.Value;
              break;

            case CASHIER_MOVEMENT.TAX_ON_PRIZE2:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = m_prize_taxes_2_name;
              _row.Cells[GRID_COLUMN_INITIAL_AMOUNT].Value = DBNull.Value;
              _row.Cells[GRID_COLUMN_FINAL_AMOUNT].Value = DBNull.Value;
              break;

            case CASHIER_MOVEMENT.TAX_ON_PRIZE3:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = m_prize_taxes_3_name;
              _row.Cells[GRID_COLUMN_INITIAL_AMOUNT].Value = DBNull.Value;
              _row.Cells[GRID_COLUMN_FINAL_AMOUNT].Value = DBNull.Value;
              break;

            case CASHIER_MOVEMENT.TAX_RETURNING_ON_PRIZE_COUPON:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_TAX_RETURNING_ON_PRIZE_COUPON");
              _row.Cells[GRID_COLUMN_INITIAL_AMOUNT].Value = DBNull.Value;
              _row.Cells[GRID_COLUMN_FINAL_AMOUNT].Value = DBNull.Value;
              break;

            case CASHIER_MOVEMENT.DECIMAL_ROUNDING:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DECIMAL_ROUNDING");
              _row.Cells[GRID_COLUMN_INITIAL_AMOUNT].Value = DBNull.Value;
              _row.Cells[GRID_COLUMN_FINAL_AMOUNT].Value = DBNull.Value;
              break;

            case CASHIER_MOVEMENT.SERVICE_CHARGE:
            case CASHIER_MOVEMENT.SERVICE_CHARGE_ON_COMPANY_A:  //EOR 19-SEP-2016
            case CASHIER_MOVEMENT.SERVICE_CHARGE_GAMING_TABLE:
            case CASHIER_MOVEMENT.SERVICE_CHARGE_GAMING_TABLE_ON_COMPANY_A:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_SERVICE_CHARGE");
              _row.Cells[GRID_COLUMN_INITIAL_AMOUNT].Value = DBNull.Value;
              _row.Cells[GRID_COLUMN_FINAL_AMOUNT].Value = DBNull.Value;

              if (_row.Cells[GRID_COLUMN_AUX_AMOUNT].Value != DBNull.Value)
              {
                _aux_amount = (Decimal)_row.Cells[GRID_COLUMN_AUX_AMOUNT].Value;
                if (_aux_amount != 0)
                {
                  _row.Cells[GRID_COLUMN_SUB_AMOUNT].Value = m_split2_tax_name + ": " +
                                                                                    Currency.Format(_aux_amount, _row.Cells[GRID_COLUMN_CURRENCY_ISO_CODE].Value.ToString());
                }
              }

              break;

            case CASHIER_MOVEMENT.PRIZES:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Misc.IsVoucherModeTV() ? Resource.String("STR_PRIZES_GROSS") : Resource.String("STR_UC_BANK_PRIZES");
              _row.Cells[GRID_COLUMN_INITIAL_AMOUNT].Value = DBNull.Value;
              _row.Cells[GRID_COLUMN_FINAL_AMOUNT].Value = DBNull.Value;
              break;

            case CASHIER_MOVEMENT.PRIZE_DRAW_GROSS_OTHER:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_PRIZES_GROSS_OTHERS");
              _row.Cells[GRID_COLUMN_INITIAL_AMOUNT].Value = DBNull.Value;
              _row.Cells[GRID_COLUMN_FINAL_AMOUNT].Value = DBNull.Value;
              break;

            case CASHIER_MOVEMENT.FILLER_IN:
              GetTypeNameColumn(Resource.String("STR_VOUCHER_CASH_DESK_FILL_IN_TITLE"), _row, true, out _str_value, out _tmp_iso_code, out _currency_exchange_type);

              if (_tmp_iso_code != "")
              {
                if (_currency_exchange_type != CurrencyExchangeType.CASINO_CHIP_COLOR)
                {
                  _row.Cells[GRID_COLUMN_ADD_AMOUNT].Value = Currency.Format((Decimal)_row.Cells[GRID_COLUMN_ADD_AMOUNT_DECIMAL].Value,
                                                                                                      _tmp_iso_code);
                }
              }

              if (Cage.IsCageEnabled())
              {
                _str_value += " - " + Resource.String("STR_VOUCHER_TOTAL_AMOUNT");
              }

              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = _str_value;
              break;

            case CASHIER_MOVEMENT.FILLER_IN_CLOSING_STOCK:
              GetTypeNameColumn(Resource.String("STR_VOUCHER_CASH_DESK_FILL_IN_CLOSING_STOCKS_TITLE"), _row, true, out _str_value, out _tmp_iso_code, out _currency_exchange_type);

              if (_tmp_iso_code != "")
              {
                if (_currency_exchange_type != CurrencyExchangeType.CASINO_CHIP_COLOR)
                {
                  _row.Cells[GRID_COLUMN_ADD_AMOUNT].Value = Currency.Format((Decimal)_row.Cells[GRID_COLUMN_ADD_AMOUNT_DECIMAL].Value,
                                                                                                  _tmp_iso_code);
                }
              }

              if (Cage.IsCageEnabled())
              {
                _str_value += " - " + Resource.String("STR_VOUCHER_TOTAL_AMOUNT");
              }

              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = _str_value;
              break;

            //FBA 19-NOV-2013
            case CASHIER_MOVEMENT.CAGE_FILLER_IN:
              GetTypeNameColumn(Resource.String("STR_VOUCHER_CASH_DESK_FILL_IN_TITLE"), _row, true, out _str_value, out _tmp_iso_code, out _currency_exchange_type);

              if (_tmp_iso_code != "" && _tmp_iso_code != _currency_iso_code)
              {
                if (_currency_exchange_type != CurrencyExchangeType.CASINO_CHIP_COLOR)
                {
                  _row.Cells[GRID_COLUMN_ADD_AMOUNT].Value = Currency.Format((Decimal)_row.Cells[GRID_COLUMN_ADD_AMOUNT_DECIMAL].Value,
                                                                                                    _row.Cells[GRID_COLUMN_CURRENCY_ISO_CODE].Value.ToString());
                }
              }
              _str_value += " - " + Resource.String("STR_UC_GIFT_DELIVERY_002");
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = _str_value;
              break;

            case CASHIER_MOVEMENT.FILLER_OUT:
              GetTypeNameColumn(Resource.String("STR_VOUCHER_CASH_DESK_FILL_OUT_TITLE"), _row, true, out _str_value, out _tmp_iso_code, out _currency_exchange_type);

              if (_tmp_iso_code != "")
              {
                if (_currency_exchange_type != CurrencyExchangeType.CASINO_CHIP_COLOR)
                {
                  _row.Cells[GRID_COLUMN_SUB_AMOUNT].Value = Currency.Format((Decimal)_row.Cells[GRID_COLUMN_SUB_AMOUNT_DECIMAL].Value,
                                                                                                  _tmp_iso_code);
                }
              }

              if (Cage.IsCageEnabled())
              {
                _str_value += " - " + Resource.String("STR_VOUCHER_TOTAL_AMOUNT");
              }

              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = _str_value;

              break;

            case CASHIER_MOVEMENT.FILLER_OUT_CLOSING_STOCK:
              GetTypeNameColumn(Resource.String("STR_VOUCHER_CASH_DESK_FILL_OUT_CLOSING_STOCK_TITLE"), _row);
              if (_row.Cells[GRID_COLUMN_CURRENCY_ISO_CODE].Value.ToString() != "")
              {
                if (_row.Cells[GRID_COLUMN_CURRENCY_ISO_CODE].Value.ToString() != Cage.CHIPS_COLOR)
                {
                  _row.Cells[GRID_COLUMN_SUB_AMOUNT].Value = Currency.Format((Decimal)_row.Cells[GRID_COLUMN_SUB_AMOUNT_DECIMAL].Value,
                                                                                                    _row.Cells[GRID_COLUMN_CURRENCY_ISO_CODE].Value.ToString());
                }
              }

              if (Cage.IsCageEnabled())
              {
                _row.Cells[GRID_COLUMN_TYPE_NAME].Value += " - " + Resource.String("STR_VOUCHER_TOTAL_AMOUNT");
              }
              break;

            //FBA 19-NOV-2013
            case CASHIER_MOVEMENT.CAGE_FILLER_OUT:
              GetTypeNameColumn(Resource.String("STR_VOUCHER_CASH_DESK_FILL_OUT_TITLE"), _row, true, out _str_value, out _tmp_iso_code, out _currency_exchange_type);

              if (_tmp_iso_code != "" && _tmp_iso_code != _currency_iso_code)
              {
                if (_currency_exchange_type != CurrencyExchangeType.CASINO_CHIP_COLOR)
                {
                  _row.Cells[GRID_COLUMN_SUB_AMOUNT].Value = Currency.Format((Decimal)_row.Cells[GRID_COLUMN_SUB_AMOUNT_DECIMAL].Value,
                                                                                                    _row.Cells[GRID_COLUMN_CURRENCY_ISO_CODE].Value.ToString());
                }
              }

              // 20-FEB-2014 JPJ Fixed Bug WIG-659: Detail of the cashier movement when it's a ticket denominaion.
              if (_row.Cells[GRID_COLUMN_DENOMINATION].Value.ToString() != "")
              {
                //DLL 26-AUG-2014: Special message for tips
                if ((Decimal)_row.Cells[GRID_COLUMN_DENOMINATION].Value == Cage.COINS_CODE && _tmp_iso_code == Cage.CHIPS_ISO_CODE)
                {
                  _str_value += " - " + Resource.String("STR_CAGE_TIPS");
                }
                else
                {
                  _str_value += " - " + Resource.String("STR_UC_GIFT_DELIVERY_002");
                }

                if ((Decimal)_row.Cells[GRID_COLUMN_DENOMINATION].Value == TICKET_TITO_DENOMINATION)
                {
                  _str_value += " - (" + Resource.String("STR_UC_TICKETS") + ")";
                }
              }
              else
              {
                _str_value += " - " + Resource.String("STR_UC_GIFT_DELIVERY_002");
              }

              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = _str_value;
              break;

            //DLL 18-SEP-2013 
            case CASHIER_MOVEMENT.FILLER_OUT_CHECK:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_VOUCHER_CASH_DESK_FILL_OUT_CHECK_TITLE");
              if (_row.Cells[GRID_COLUMN_CURRENCY_ISO_CODE].Value.ToString() != ""
                 && _row.Cells[GRID_COLUMN_CURRENCY_ISO_CODE].Value.ToString() != CurrencyExchange.GetNationalCurrency())
              {
                if (_row.Cells[GRID_COLUMN_CURRENCY_ISO_CODE].Value.ToString() != Cage.CHIPS_COLOR)
                {
                  _row.Cells[GRID_COLUMN_SUB_AMOUNT].Value = Currency.Format((Decimal)_row.Cells[GRID_COLUMN_SUB_AMOUNT_DECIMAL].Value,
                                                                                                    _row.Cells[GRID_COLUMN_CURRENCY_ISO_CODE].Value.ToString());
                }
              }
              else
              {
                _row.Cells[GRID_COLUMN_SUB_AMOUNT].Value = ((Currency)Convert.ToDecimal(_row.Cells[GRID_COLUMN_SUB_AMOUNT_DECIMAL].Value)).ToString(false);
              }

              break;

            case CASHIER_MOVEMENT.PROMOTION_NOT_REDEEMABLE:
              _promo_name = _row.Cells[GRID_COLUMN_DETAILS].Value == System.DBNull.Value ? "" : _row.Cells[GRID_COLUMN_DETAILS].Value.ToString();

              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_009");
              if (!String.IsNullOrEmpty(_promo_name))
              {
                _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_009") + " (" + _promo_name + ")";
              }
              _row.Cells[GRID_COLUMN_INITIAL_AMOUNT].Value = DBNull.Value;
              _row.Cells[GRID_COLUMN_FINAL_AMOUNT].Value = DBNull.Value;
              break;

            case CASHIER_MOVEMENT.CASH_IN_COVER_COUPON:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_062");
              break;

            case CASHIER_MOVEMENT.CARD_DEPOSIT_IN:
            case CASHIER_MOVEMENT.CARD_REPLACEMENT:
            case CASHIER_MOVEMENT.CARD_REPLACEMENT_FOR_FREE:
            case CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CHECK:
            case CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CURRENCY_EXCHANGE:
            case CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CARD_CREDIT:
            case CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CARD_DEBIT:
            case CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CARD_GENERIC:
            case CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CARD_UNDO:
            case CASHIER_MOVEMENT.CARD_REPLACEMENT_CHECK:
            case CASHIER_MOVEMENT.CARD_REPLACEMENT_CARD_CREDIT:
            case CASHIER_MOVEMENT.CARD_REPLACEMENT_CARD_DEBIT:
            case CASHIER_MOVEMENT.CARD_REPLACEMENT_CARD_GENERIC:
            case CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN:
            case CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT:
            case CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CHECK:
            case CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CURRENCY_EXCHANGE:
            case CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CARD_CREDIT:
            case CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CARD_DEBIT:
            case CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CARD_GENERIC:
            case CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CHECK:
            case CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CARD_CREDIT:
            case CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CARD_DEBIT:
            case CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CARD_GENERIC:
            case CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CARD_UNDO:
            case CASHIER_MOVEMENT.CARD_ASSOCIATE:

              switch ((CASHIER_MOVEMENT)_movement_type)
              {
                case CASHIER_MOVEMENT.CARD_REPLACEMENT_FOR_FREE:
                  _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_FREE_CARD");
                  break;
                case CASHIER_MOVEMENT.CARD_DEPOSIT_IN:
                case CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN:
                  _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_010");
                  break;
                case CASHIER_MOVEMENT.CARD_REPLACEMENT:
                case CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT:
                  _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_036");
                  break;
                case CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CHECK:
                case CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CHECK:
                  _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_115");
                  break;
                case CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CURRENCY_EXCHANGE:
                case CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CURRENCY_EXCHANGE:
                  _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_116");
                  break;
                case CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CARD_CREDIT:
                case CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CARD_CREDIT:
                  _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_117");
                  break;
                case CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CARD_DEBIT:
                case CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CARD_DEBIT:
                  _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_118");
                  break;
                case CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CARD_GENERIC:
                case CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CARD_GENERIC:
                case CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CARD_UNDO:
                case CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CARD_UNDO:
                  _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_119");
                  break;
                case CASHIER_MOVEMENT.CARD_REPLACEMENT_CHECK:
                case CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CHECK:
                  _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_110");
                  break;
                case CASHIER_MOVEMENT.CARD_REPLACEMENT_CARD_CREDIT:
                case CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CARD_CREDIT:
                  _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_112");
                  break;
                case CASHIER_MOVEMENT.CARD_REPLACEMENT_CARD_DEBIT:
                case CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CARD_DEBIT:
                  _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_113");
                  break;
                case CASHIER_MOVEMENT.CARD_REPLACEMENT_CARD_GENERIC:
                case CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CARD_GENERIC:
                  _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_114");
                  break;
                case CASHIER_MOVEMENT.CARD_ASSOCIATE:
                  _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("BTN_CARD_FUNC_ASSIGN");
                  break;
              }

              if (_row.Cells[GRID_COLUMN_AUX_AMOUNT].Value != DBNull.Value)
              {
                _aux_amount = (Decimal)_row.Cells[GRID_COLUMN_AUX_AMOUNT].Value;
                if (_aux_amount != 0)
                {
                  _row.Cells[GRID_COLUMN_SUB_AMOUNT].Value = m_split2_tax_name + ": " +
                                                                                    Currency.Format(_aux_amount, _row.Cells[GRID_COLUMN_CURRENCY_ISO_CODE].Value.ToString());
                }
              }
              break;

            case CASHIER_MOVEMENT.CARD_DEPOSIT_OUT:
            case CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_OUT:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_011");
              break;

            case CASHIER_MOVEMENT.CANCEL_NOT_REDEEMABLE:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_012");
              _row.Cells[GRID_COLUMN_INITIAL_AMOUNT].Value = DBNull.Value;
              _row.Cells[GRID_COLUMN_FINAL_AMOUNT].Value = DBNull.Value;
              break;

            case CASHIER_MOVEMENT.MB_MANUAL_CHANGE_LIMIT:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_028");
              _row.Cells[GRID_COLUMN_INITIAL_AMOUNT].Value = DBNull.Value;
              _row.Cells[GRID_COLUMN_FINAL_AMOUNT].Value = DBNull.Value;
              break;

            case CASHIER_MOVEMENT.MB_CASH_IN:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_029");
              break;

            case CASHIER_MOVEMENT.PROMO_CREDITS:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_015");
              break;

            case CASHIER_MOVEMENT.PROMO_TO_REDEEMABLE:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_016");
              break;

            case CASHIER_MOVEMENT.PROMO_EXPIRED:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_017");
              break;

            case CASHIER_MOVEMENT.PROMO_CANCEL:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_018");
              break;

            case CASHIER_MOVEMENT.PROMO_START_SESSION:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_019");
              break;

            case CASHIER_MOVEMENT.PROMO_END_SESSION:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_020");
              break;

            case CASHIER_MOVEMENT.HANDPAY:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_038");
              break;

            case CASHIER_MOVEMENT.HANDPAY_CANCELLATION:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_039");
              break;

            case CASHIER_MOVEMENT.MANUAL_HANDPAY:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_042");
              break;

            case CASHIER_MOVEMENT.MANUAL_HANDPAY_CANCELLATION:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_043");
              break;

            case CASHIER_MOVEMENT.CASH_IN_SPLIT1:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = m_split_a_name_str;
              _row.Cells[GRID_COLUMN_INITIAL_AMOUNT].Value = DBNull.Value;
              _row.Cells[GRID_COLUMN_FINAL_AMOUNT].Value = DBNull.Value;
              break;

            case CASHIER_MOVEMENT.CASH_IN_SPLIT2:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = m_split_b_name_str;

              if (_row.Cells[GRID_COLUMN_AUX_AMOUNT].Value != DBNull.Value)
              {
                _aux_amount = (Decimal)_row.Cells[GRID_COLUMN_AUX_AMOUNT].Value;
                if (_aux_amount != 0)
                {
                  _row.Cells[GRID_COLUMN_SUB_AMOUNT].Value = m_split2_tax_name + ": " +
                                                                                    Currency.Format(_aux_amount, _row.Cells[GRID_COLUMN_CURRENCY_ISO_CODE].Value.ToString());
                }
              }
              break;

            case CASHIER_MOVEMENT.DEV_SPLIT1:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_047") + " - " + m_split_a_name_str;
              _row.Cells[GRID_COLUMN_INITIAL_AMOUNT].Value = DBNull.Value;
              _row.Cells[GRID_COLUMN_FINAL_AMOUNT].Value = DBNull.Value;
              break;

            case CASHIER_MOVEMENT.DEV_SPLIT2:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_047") + " - " + m_split_b_name_str;
              _row.Cells[GRID_COLUMN_INITIAL_AMOUNT].Value = DBNull.Value;
              _row.Cells[GRID_COLUMN_FINAL_AMOUNT].Value = DBNull.Value;
              break;

            case CASHIER_MOVEMENT.CANCEL_SPLIT1:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_048") + " - " + m_split_a_name_str;
              _row.Cells[GRID_COLUMN_INITIAL_AMOUNT].Value = DBNull.Value;
              _row.Cells[GRID_COLUMN_FINAL_AMOUNT].Value = DBNull.Value;
              break;

            case CASHIER_MOVEMENT.CANCEL_SPLIT2:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_048") + " - " + m_split_b_name_str;
              _row.Cells[GRID_COLUMN_INITIAL_AMOUNT].Value = DBNull.Value;
              _row.Cells[GRID_COLUMN_FINAL_AMOUNT].Value = DBNull.Value;
              break;

            //SSC 08-MAR-2012: Recharge Note Acceptor
            case CASHIER_MOVEMENT.NA_CASH_IN_SPLIT1:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_065") + " - " + m_split_a_name_str;
              _row.Cells[GRID_COLUMN_INITIAL_AMOUNT].Value = DBNull.Value;
              _row.Cells[GRID_COLUMN_FINAL_AMOUNT].Value = DBNull.Value;
              break;

            case CASHIER_MOVEMENT.MB_CASH_IN_SPLIT1:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_049") + " - " + m_split_a_name_str;
              _row.Cells[GRID_COLUMN_INITIAL_AMOUNT].Value = DBNull.Value;
              _row.Cells[GRID_COLUMN_FINAL_AMOUNT].Value = DBNull.Value;
              break;

            //SSC 08-MAR-2012: Recharge Note Acceptor
            case CASHIER_MOVEMENT.NA_CASH_IN_SPLIT2:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_065") + " - " + m_split_b_name_str;

              if (_row.Cells[GRID_COLUMN_AUX_AMOUNT].Value != DBNull.Value)
              {
                _aux_amount = (Decimal)_row.Cells[GRID_COLUMN_AUX_AMOUNT].Value;
                if (_aux_amount != 0)
                {
                  _row.Cells[GRID_COLUMN_SUB_AMOUNT].Value = m_split2_tax_name + ": " +
                                                                                    Currency.Format(_aux_amount, _row.Cells[GRID_COLUMN_CURRENCY_ISO_CODE].Value.ToString());
                }
              }
              break;

            case CASHIER_MOVEMENT.MB_CASH_IN_SPLIT2:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_049") + " - " + m_split_b_name_str;

              if (_row.Cells[GRID_COLUMN_AUX_AMOUNT].Value != DBNull.Value)
              {
                _aux_amount = (Decimal)_row.Cells[GRID_COLUMN_AUX_AMOUNT].Value;
                if (_aux_amount != 0)
                {
                  _row.Cells[GRID_COLUMN_SUB_AMOUNT].Value = m_split2_tax_name + ": " +
                                                                                    Currency.Format(_aux_amount, _row.Cells[GRID_COLUMN_CURRENCY_ISO_CODE].Value.ToString());
                }
              }
              break;

            case CASHIER_MOVEMENT.DRAW_TICKET_PRINT:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_037");
              break;

            case CASHIER_MOVEMENT.POINTS_TO_DRAW_TICKET_PRINT:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_050");
              break;

            case CASHIER_MOVEMENT.PRIZE_COUPON:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_053");
              _row.Cells[GRID_COLUMN_INITIAL_AMOUNT].Value = DBNull.Value;
              _row.Cells[GRID_COLUMN_FINAL_AMOUNT].Value = DBNull.Value;
              break;

            case CASHIER_MOVEMENT.CARD_CREATION:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_057");
              _row.Cells[GRID_COLUMN_INITIAL_AMOUNT].Value = DBNull.Value;
              _row.Cells[GRID_COLUMN_FINAL_AMOUNT].Value = DBNull.Value;
              break;

            case CASHIER_MOVEMENT.CARD_PERSONALIZATION:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_058");
              _row.Cells[GRID_COLUMN_INITIAL_AMOUNT].Value = DBNull.Value;
              _row.Cells[GRID_COLUMN_FINAL_AMOUNT].Value = DBNull.Value;
              break;

            case CASHIER_MOVEMENT.ACCOUNT_PIN_CHANGE:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_059");
              _row.Cells[GRID_COLUMN_INITIAL_AMOUNT].Value = DBNull.Value;
              _row.Cells[GRID_COLUMN_FINAL_AMOUNT].Value = DBNull.Value;
              break;

            case CASHIER_MOVEMENT.ACCOUNT_PIN_RANDOM:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_060");
              _row.Cells[GRID_COLUMN_INITIAL_AMOUNT].Value = DBNull.Value;
              _row.Cells[GRID_COLUMN_FINAL_AMOUNT].Value = DBNull.Value;
              break;

            //SSC 08-MAR-2012: Recharge Note Acceptor
            case CASHIER_MOVEMENT.NA_CASH_IN_NOT_REDEEMABLE:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_065") + " - " + Resource.String("STR_FRM_LAST_MOVEMENTS_009");
              break;

            case CASHIER_MOVEMENT.MB_CASH_IN_NOT_REDEEMABLE:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_049") + " - " + Resource.String("STR_FRM_LAST_MOVEMENTS_009");
              break;

            //SSC 08-MAR-2012: Recharge Note Acceptor
            case CASHIER_MOVEMENT.NA_CASH_IN_COVER_COUPON:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_065") + " - " + Resource.String("STR_FRM_LAST_MOVEMENTS_062");
              break;

            case CASHIER_MOVEMENT.MB_CASH_IN_COVER_COUPON:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_049") + " - " + Resource.String("STR_FRM_LAST_MOVEMENTS_062");
              break;

            //SSC 08-MAR-2012: Recharge Note Acceptor
            case CASHIER_MOVEMENT.NA_PRIZE_COUPON:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_065") + " - " + Resource.String("STR_FRM_LAST_MOVEMENTS_053");
              _row.Cells[GRID_COLUMN_INITIAL_AMOUNT].Value = DBNull.Value;
              _row.Cells[GRID_COLUMN_FINAL_AMOUNT].Value = DBNull.Value;
              break;

            case CASHIER_MOVEMENT.MB_PRIZE_COUPON:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_049") + " - " + Resource.String("STR_FRM_LAST_MOVEMENTS_053");
              _row.Cells[GRID_COLUMN_INITIAL_AMOUNT].Value = DBNull.Value;
              _row.Cells[GRID_COLUMN_FINAL_AMOUNT].Value = DBNull.Value;
              break;

            // JCM 24-APR-2012
            case CASHIER_MOVEMENT.CARD_RECYCLED:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_067");
              _row.Cells[GRID_COLUMN_INITIAL_AMOUNT].Value = DBNull.Value;
              _row.Cells[GRID_COLUMN_FINAL_AMOUNT].Value = DBNull.Value;
              break;

            // SSC 12-JUN-2012: GiftRequest
            case CASHIER_MOVEMENT.POINTS_TO_NOT_REDEEMABLE:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_046");
              _row.Cells[GRID_COLUMN_INITIAL_AMOUNT].Value = DBNull.Value;
              _row.Cells[GRID_COLUMN_FINAL_AMOUNT].Value = DBNull.Value;
              break;

            case CASHIER_MOVEMENT.POINTS_TO_REDEEMABLE:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_055");
              _row.Cells[GRID_COLUMN_INITIAL_AMOUNT].Value = DBNull.Value;
              _row.Cells[GRID_COLUMN_FINAL_AMOUNT].Value = DBNull.Value;
              break;

            // DDM 05-JUL-2012
            case CASHIER_MOVEMENT.PROMOTION_REDEEMABLE:
              _promo_name = _row.Cells[GRID_COLUMN_DETAILS].Value == System.DBNull.Value ? "" : _row.Cells[GRID_COLUMN_DETAILS].Value.ToString();

              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_068");
              if (!String.IsNullOrEmpty(_promo_name))
              {
                // ATB 10-NOV-2016
                // In case of Televisa, and the promotion is "Raffle Result", it only shows the string
                if (Misc.IsVoucherModeTV() && _promo_name.ToLower() == Resource.String("STR_PROMOTION_RAFFLE_RESULT").ToLower())
                {
                  _row.Cells[GRID_COLUMN_TYPE_NAME].Value = _promo_name;
                }
                else
                {
                  _row.Cells[GRID_COLUMN_TYPE_NAME].Value += " (" + _promo_name + ")";
                }
              }

              _row.Cells[GRID_COLUMN_INITIAL_AMOUNT].Value = DBNull.Value;
              _row.Cells[GRID_COLUMN_FINAL_AMOUNT].Value = DBNull.Value;
              break;

            // RCI & JCM 09-JUL-2012
            case CASHIER_MOVEMENT.CANCEL_PROMOTION_REDEEMABLE:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_069");
              _row.Cells[GRID_COLUMN_INITIAL_AMOUNT].Value = DBNull.Value;
              _row.Cells[GRID_COLUMN_FINAL_AMOUNT].Value = DBNull.Value;
              break;

            // DDM 31-JUL-2012
            case CASHIER_MOVEMENT.PROMOTION_POINT:
              _promo_name = _row.Cells[GRID_COLUMN_DETAILS].Value == System.DBNull.Value ? "" : _row.Cells[GRID_COLUMN_DETAILS].Value.ToString();

              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_070");
              if (!String.IsNullOrEmpty(_promo_name))
              {
                _row.Cells[GRID_COLUMN_TYPE_NAME].Value += " (" + _promo_name + ")";
              }
              _row.Cells[GRID_COLUMN_INITIAL_AMOUNT].Value = DBNull.Value;
              _row.Cells[GRID_COLUMN_FINAL_AMOUNT].Value = DBNull.Value;
              break;

            case CASHIER_MOVEMENT.CANCEL_PROMOTION_POINT:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_071");
              _row.Cells[GRID_COLUMN_INITIAL_AMOUNT].Value = DBNull.Value;
              _row.Cells[GRID_COLUMN_FINAL_AMOUNT].Value = DBNull.Value;
              break;

            case CASHIER_MOVEMENT.MB_CASH_LOST:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_074");
              _row.Cells[GRID_COLUMN_INITIAL_AMOUNT].Value = DBNull.Value;
              _row.Cells[GRID_COLUMN_ADD_AMOUNT].Value = DBNull.Value;
              _row.Cells[GRID_COLUMN_FINAL_AMOUNT].Value = DBNull.Value;
              break;

            case CASHIER_MOVEMENT.REQUEST_TO_VOUCHER_REPRINT:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_076");
              _row.Cells[GRID_COLUMN_INITIAL_AMOUNT].Value = DBNull.Value;
              _row.Cells[GRID_COLUMN_FINAL_AMOUNT].Value = DBNull.Value;
              break;

            case CASHIER_MOVEMENT.REQUEST_TO_TICKET_REPRINT:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_090");
              _row.Cells[GRID_COLUMN_INITIAL_AMOUNT].Value = DBNull.Value;
              _row.Cells[GRID_COLUMN_FINAL_AMOUNT].Value = DBNull.Value;
              break;

            case CASHIER_MOVEMENT.CHECK_PAYMENT:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_077");
              break;

            case CASHIER_MOVEMENT.ACCOUNT_BLOCKED:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_MOVEMENT_TYPE_BLOCKED");
              break;

            case CASHIER_MOVEMENT.ACCOUNT_UNBLOCKED:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_MOVEMENT_TYPE_UNBLOCKED");
              break;

            case CASHIER_MOVEMENT.ACCOUNT_ADD_BLACKLIST:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_MOVEMENT_TYPE_ADD_TO_BLACKLIST");
              break;

            case CASHIER_MOVEMENT.ACCOUNT_REMOVE_BLACKLIST:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_MOVEMENT_TYPE_REMOVE_FROM_BLACKLIST");
              break;

            // JBP 06-JUN-2016
            case CASHIER_MOVEMENT.C2GO_LOCK_ACCOUNT_INTERNAL:
            case CASHIER_MOVEMENT.C2GO_LOCK_ACCOUNT_EXTERNAL:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_MOVEMENT_TYPE_BLOCKED_C2GO");
              break;
            //case CASHIER_MOVEMENT.ACCOUNT_UNBLOCKED_C2GO:
            //  row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_MOVEMENT_TYPE_UNBLOCKED_C2GO");
            //  break;

            // DLL 01-JUL-2013
            case CASHIER_MOVEMENT.CURRENCY_EXCHANGE_SPLIT1:
            case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_EXCHANGE_SPLIT1:
              _add_amount_str = Currency.Format((Decimal)_row.Cells[GRID_COLUMN_INITIAL_AMOUNT].Value,
                                                _row.Cells[GRID_COLUMN_CURRENCY_ISO_CODE].Value.ToString());
              _add_amount_str += " = ";
              _add_amount_str += Currency.Format((Decimal)_row.Cells[GRID_COLUMN_ADD_AMOUNT_DECIMAL].Value, CurrencyExchange.GetNationalCurrency());
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_CURRENCY_EXCHANGE") + " " + _row.Cells[GRID_COLUMN_CURRENCY_ISO_CODE].Value.ToString();
              _row.Cells[GRID_COLUMN_ADD_AMOUNT].Value = _add_amount_str;
              _row.Cells[GRID_COLUMN_SUB_AMOUNT].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_COMMISSION") + ": " + Currency.Format((Decimal)_row.Cells[GRID_COLUMN_SUB_AMOUNT_DECIMAL].Value, CurrencyExchange.GetNationalCurrency());
              _row.Cells[GRID_COLUMN_FINAL_AMOUNT].Value = DBNull.Value;
              break;

            // RMS 08-AUG-2014
            case CASHIER_MOVEMENT.CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT1:
            case CASHIER_MOVEMENT.CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT1:
            case CASHIER_MOVEMENT.CURRENCY_CARD_EXCHANGE_SPLIT1:
            case CASHIER_MOVEMENT.CURRENCY_CARD_EXCHANGE_SPLIT1_UNDO:  // FGB 09-AUG-2016           
            case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT1:
            case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT1:
            case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CARD_EXCHANGE_SPLIT1:
            case CASHIER_MOVEMENT.CREDIT_LINE_PAYBACK_CARD_EXCHANGE:
            case CASHIER_MOVEMENT.CREDIT_LINE_PAYBACK_CREDIT_CARD_EXCHANGE:
            case CASHIER_MOVEMENT.CREDIT_LINE_PAYBACK_DEBIT_CARD_EXCHANGE:
              if (_row.Cells[GRID_COLUMN_CURRENCY_ISO_CODE].Value.ToString() != CurrencyExchange.GetNationalCurrency())
              {
                _add_amount_str = Currency.Format((Decimal)_row.Cells[GRID_COLUMN_INITIAL_AMOUNT].Value,
                                                  _row.Cells[GRID_COLUMN_CURRENCY_ISO_CODE].Value.ToString());
                _add_amount_str += " = ";
                _add_amount_str += Currency.Format((Decimal)_row.Cells[GRID_COLUMN_ADD_AMOUNT_DECIMAL].Value, CurrencyExchange.GetNationalCurrency());

                _row.Cells[GRID_COLUMN_SUB_AMOUNT].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_COMMISSION") + ": " + Currency.Format((Decimal)_row.Cells[GRID_COLUMN_SUB_AMOUNT_DECIMAL].Value, CurrencyExchange.GetNationalCurrency());
              }
              else
              {
                _add_amount_str = ((Currency)Convert.ToDecimal(_row.Cells[GRID_COLUMN_ADD_AMOUNT_DECIMAL].Value)).ToString(false);
                _row.Cells[GRID_COLUMN_SUB_AMOUNT].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_COMMISSION") + ": " + ((Currency)Convert.ToDecimal(_row.Cells[GRID_COLUMN_SUB_AMOUNT_DECIMAL].Value)).ToString(false);
              }

              // RMS 08-AUG-2014
              switch ((CASHIER_MOVEMENT)_movement_type)
              {
                case CASHIER_MOVEMENT.CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT1:
                case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT1:
                  _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_CURRENCY_CREDIT_CARD_EXCHANGE");
                  break;
                case CASHIER_MOVEMENT.CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT1:
                case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT1:
                  _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_CURRENCY_DEBIT_CARD_EXCHANGE");
                  break;
                case CASHIER_MOVEMENT.CREDIT_LINE_PAYBACK_CARD_EXCHANGE:
                  _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_PAYBACK_CARD_EXCHANGE");
                  break;
                case CASHIER_MOVEMENT.CREDIT_LINE_PAYBACK_CREDIT_CARD_EXCHANGE:
                  _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_PAYBACK_CREDIT_CARD_EXCHANGE");
                  break;
                case CASHIER_MOVEMENT.CREDIT_LINE_PAYBACK_DEBIT_CARD_EXCHANGE:
                  _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_PAYBACK_DEBIT_CARD_EXCHANGE");
                  break;
                default:
                  _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_CURRENCY_CARD_EXCHANGE");
                  break;
              }

              _row.Cells[GRID_COLUMN_ADD_AMOUNT].Value = _add_amount_str;
              _row.Cells[GRID_COLUMN_FINAL_AMOUNT].Value = DBNull.Value;
              break;

            // FBA 26-AUG-2013 New Movement "Recharge for points" (85)
            case CASHIER_MOVEMENT.POINTS_TO_REDEEMABLE_AS_CASHIN_SPLIT1:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_VOUCHERS_REPRINT_050") + " - " + m_split_a_name_str;
              break;

            case CASHIER_MOVEMENT.POINTS_TO_REDEEMABLE_AS_CASHIN_SPLIT2:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_VOUCHERS_REPRINT_050") + " - " + m_split_b_name_str;
              _row.Cells[GRID_COLUMN_INITIAL_AMOUNT].Value = DBNull.Value;
              _row.Cells[GRID_COLUMN_FINAL_AMOUNT].Value = DBNull.Value;
              break;

            case CASHIER_MOVEMENT.PARTICIPATION_IN_CASHDESK_DRAW:
              decimal _participation_add = 0;
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_091");
              _row.Cells[GRID_COLUMN_INITIAL_AMOUNT].Value = DBNull.Value;
              _row.Cells[GRID_COLUMN_FINAL_AMOUNT].Value = DBNull.Value;

              if (_row.Cells[GRID_COLUMN_ADD_AMOUNT].Value != DBNull.Value)
              {
                if (decimal.TryParse(_row.Cells[GRID_COLUMN_ADD_AMOUNT].Value.ToString(), out _participation_add))
                {
                  if (_participation_add == 0)
                  {
                    _row.Cells[GRID_COLUMN_ADD_AMOUNT].Value = DBNull.Value;
                  }
                }
              }
              break;

            case CASHIER_MOVEMENT.CURRENCY_CHECK_SPLIT1:
            case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CHECK_SPLIT1:
              if (_row.Cells[GRID_COLUMN_CURRENCY_ISO_CODE].Value.ToString() != ""
                 && _row.Cells[GRID_COLUMN_CURRENCY_ISO_CODE].Value.ToString() != CurrencyExchange.GetNationalCurrency())
              {
                _add_amount_str = Currency.Format((Decimal)_row.Cells[GRID_COLUMN_INITIAL_AMOUNT].Value,
                                                  _row.Cells[GRID_COLUMN_CURRENCY_ISO_CODE].Value.ToString());
                _add_amount_str += " = ";
                _add_amount_str += Currency.Format((Decimal)_row.Cells[GRID_COLUMN_ADD_AMOUNT_DECIMAL].Value, CurrencyExchange.GetNationalCurrency());

                _row.Cells[GRID_COLUMN_SUB_AMOUNT].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_COMMISSION") + ": " + Currency.Format((Decimal)_row.Cells[GRID_COLUMN_SUB_AMOUNT_DECIMAL].Value, CurrencyExchange.GetNationalCurrency());
              }
              else
              {
                _add_amount_str = ((Currency)Convert.ToDecimal(_row.Cells[GRID_COLUMN_ADD_AMOUNT_DECIMAL].Value)).ToString(false);
                _row.Cells[GRID_COLUMN_SUB_AMOUNT].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_COMMISSION") + ": " + ((Currency)Convert.ToDecimal(_row.Cells[GRID_COLUMN_SUB_AMOUNT_DECIMAL].Value)).ToString(false);
              }

              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_CURRENCY_CHECK");
              _row.Cells[GRID_COLUMN_ADD_AMOUNT].Value = _add_amount_str;
              _row.Cells[GRID_COLUMN_FINAL_AMOUNT].Value = DBNull.Value;
              break;

            case CASHIER_MOVEMENT.CREDIT_LINE_PAYBACK_CHECK_EXCHANGE:
              _add_amount_str = ((Currency)Convert.ToDecimal(_row.Cells[GRID_COLUMN_ADD_AMOUNT_DECIMAL].Value)).ToString(false);
              _row.Cells[GRID_COLUMN_SUB_AMOUNT].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_COMMISSION") + ": " + ((Currency)Convert.ToDecimal(_row.Cells[GRID_COLUMN_SUB_AMOUNT_DECIMAL].Value)).ToString(false);

              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_PAYBACK_CHECK_EXCHANGE");
              _row.Cells[GRID_COLUMN_ADD_AMOUNT].Value = _add_amount_str;
              _row.Cells[GRID_COLUMN_FINAL_AMOUNT].Value = DBNull.Value;

              break;

            case CASHIER_MOVEMENT.PRIZE_IN_KIND1_GROSS:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = String.Format("{0}", Misc.ReadGeneralParams("CashDesk.Draw", "AccountingMode.KindOfNameMovement"));
              break;

            case CASHIER_MOVEMENT.PRIZE_IN_KIND1_TAX1:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = String.Format("{0} - {1}", Misc.ReadGeneralParams("CashDesk.Draw", "AccountingMode.KindOfNameMovement"), m_prize_taxes_1_name);
              break;

            case CASHIER_MOVEMENT.PRIZE_IN_KIND1_TAX2:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = String.Format("{0} - {1}", Misc.ReadGeneralParams("CashDesk.Draw", "AccountingMode.KindOfNameMovement"), m_prize_taxes_2_name);
              break;

            case CASHIER_MOVEMENT.PRIZE_IN_KIND1_TAX3:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = String.Format("{0} - {1}", Misc.ReadGeneralParams("CashDesk.Draw", "AccountingMode.KindOfNameMovement"), m_prize_taxes_3_name);
              break;

            case CASHIER_MOVEMENT.PRIZE_IN_KIND2_GROSS:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = String.Format("{0}", Misc.ReadGeneralParams("CashDesk.Draw", "AccountingMode.PromoNameMovement"));
              break;

            case CASHIER_MOVEMENT.PRIZE_IN_KIND2_TAX1:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = String.Format("{0} - {1}", Misc.ReadGeneralParams("CashDesk.Draw", "AccountingMode.PromoNameMovement"), m_prize_taxes_1_name);
              break;

            case CASHIER_MOVEMENT.PRIZE_IN_KIND2_TAX2:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = String.Format("{0} - {1}", Misc.ReadGeneralParams("CashDesk.Draw", "AccountingMode.PromoNameMovement"), m_prize_taxes_2_name);
              break;

            case CASHIER_MOVEMENT.PRIZE_IN_KIND2_TAX3:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = String.Format("{0} - {1}", Misc.ReadGeneralParams("CashDesk.Draw", "AccountingMode.PromoNameMovement"), m_prize_taxes_3_name);
              break;

            //LA 16-FEB-2016 
            case CASHIER_MOVEMENT.PRIZE_RE_IN_KIND1_GROSS:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = String.Format("{0}", Misc.ReadGeneralParams("CashDesk.Draw", "AccountingMode.RE.KindOfNameMovement"));
              break;

            case CASHIER_MOVEMENT.PRIZE_RE_IN_KIND1_TAX1:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = String.Format("{0} - {1}", Misc.ReadGeneralParams("CashDesk.Draw", "AccountingMode.RE.KindOfNameMovement"), m_prize_taxes_1_name);
              break;

            case CASHIER_MOVEMENT.PRIZE_RE_IN_KIND1_TAX2:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = String.Format("{0} - {1}", Misc.ReadGeneralParams("CashDesk.Draw", "AccountingMode.RE.KindOfNameMovement"), m_prize_taxes_2_name);
              break;

            case CASHIER_MOVEMENT.PRIZE_RE_IN_KIND1_TAX3:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = String.Format("{0} - {1}", Misc.ReadGeneralParams("CashDesk.Draw", "AccountingMode.RE.KindOfNameMovement"), m_prize_taxes_3_name);
              break;

            case CASHIER_MOVEMENT.PRIZE_RE_IN_KIND2_GROSS:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = String.Format("{0}", Misc.ReadGeneralParams("CashDesk.Draw", "AccountingMode.RE.PromoNameMovement"));
              break;

            case CASHIER_MOVEMENT.PRIZE_RE_IN_KIND2_TAX1:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = String.Format("{0} - {1}", Misc.ReadGeneralParams("CashDesk.Draw", "AccountingMode.RE.PromoNameMovement"), m_prize_taxes_1_name);
              break;

            case CASHIER_MOVEMENT.PRIZE_RE_IN_KIND2_TAX2:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = String.Format("{0} - {1}", Misc.ReadGeneralParams("CashDesk.Draw", "AccountingMode.RE.PromoNameMovement"), m_prize_taxes_2_name);
              break;

            case CASHIER_MOVEMENT.PRIZE_RE_IN_KIND2_TAX3:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = String.Format("{0} - {1}", Misc.ReadGeneralParams("CashDesk.Draw", "AccountingMode.RE.PromoNameMovement"), m_prize_taxes_3_name);
              break;

            //LA 26-FEB-2016 
            case CASHIER_MOVEMENT.PROMOTION_REDEEMABLE_FEDERAL_TAX:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = String.Format("{0} - {1}", Resource.String("STR_RE_PROMO_NAME_MOVEMENT"), m_prize_taxes_1_name);
              break;

            case CASHIER_MOVEMENT.PROMOTION_REDEEMABLE_STATE_TAX:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = String.Format("{0} - {1}", Resource.String("STR_RE_PROMO_NAME_MOVEMENT"), m_prize_taxes_2_name);
              break;

            case CASHIER_MOVEMENT.PROMOTION_REDEEMABLE_COUNCIL_TAX:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = String.Format("{0} - {1}", Resource.String("STR_RE_PROMO_NAME_MOVEMENT"), m_prize_taxes_3_name);
              break;

            case CASHIER_MOVEMENT.TITO_TICKET_CREATE_CASHABLE_OFFLINE:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_UC_TITO_TICKET_CASHABLE_REDEEMED_OFFLINE");
              _row.Cells[GRID_COLUMN_ADD_AMOUNT].Value = ((Currency)Convert.ToDecimal(_row.Cells[GRID_COLUMN_ADD_AMOUNT_DECIMAL].Value)).ToString(false);
              _row.Cells[GRID_COLUMN_SUB_AMOUNT].Value = DBNull.Value;
              break;

            case CASHIER_MOVEMENT.TITO_TICKET_CREATE_PLAYABLE_OFFLINE:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_UC_TITO_TICKET_PLAYABLE_REDEEMED_OFFLINE");
              _row.Cells[GRID_COLUMN_ADD_AMOUNT].Value = ((Currency)Convert.ToDecimal(_row.Cells[GRID_COLUMN_ADD_AMOUNT_DECIMAL].Value)).ToString(false);
              _row.Cells[GRID_COLUMN_SUB_AMOUNT].Value = DBNull.Value;
              break;

            case CASHIER_MOVEMENT.TITO_VALIDATE_TICKET:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_MOVEMENT_TYPE_VALIDATE_TICKET");
              break;

            case CASHIER_MOVEMENT.CHIPS_PURCHASE:
              _currency_exchange_type = (CurrencyExchangeType)Convert.ToInt32(_row.Cells[GRID_COLUMN_CAGE_CURRENCY_TYPE].Value);
              _tmp_iso_code = _row.Cells[GRID_COLUMN_CURRENCY_ISO_CODE].Value.ToString();

              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = GetMovementTypeDescription(Resource.String("STR_MOVEMENT_TYPE_CHIPS_PURCHASE"),
                                                                                                          _currency_exchange_type,
                                                                                                          _tmp_iso_code,
                                                                                                          true);

              _row.Cells[GRID_COLUMN_ADD_AMOUNT].Value = Currency.Format((Decimal)_row.Cells[GRID_COLUMN_ADD_AMOUNT_DECIMAL].Value,
                                                  _row.Cells[GRID_COLUMN_CURRENCY_ISO_CODE].Value.ToString());

              break;

            case CASHIER_MOVEMENT.CHIPS_PURCHASE_TOTAL:
              _currency_exchange_type = (CurrencyExchangeType)Convert.ToInt32(_row.Cells[GRID_COLUMN_CAGE_CURRENCY_TYPE].Value);
              _tmp_iso_code = _row.Cells[GRID_COLUMN_CURRENCY_ISO_CODE].Value.ToString();

              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = GetMovementTypeDescription(Resource.String("STR_MOVEMENT_TYPE_CHIPS_PURCHASE_TOTAL"),
                                                                                                         _currency_exchange_type,
                                                                                                         _tmp_iso_code,
                                                                                                         true);

              _add_amount_str = Currency.Format((Decimal)_row.Cells[GRID_COLUMN_ADD_AMOUNT_DECIMAL].Value, CurrencyExchange.GetNationalCurrency());

              _row.Cells[GRID_COLUMN_ADD_AMOUNT].Value = _add_amount_str;
              break;

            case CASHIER_MOVEMENT.CHIPS_PURCHASE_TOTAL_EXCHANGE:
              _currency_exchange_type = (CurrencyExchangeType)Convert.ToInt32(_row.Cells[GRID_COLUMN_CAGE_CURRENCY_TYPE].Value);
              _tmp_iso_code = _row.Cells[GRID_COLUMN_CURRENCY_ISO_CODE].Value.ToString();

              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = GetMovementTypeDescription(Resource.String("STR_MOVEMENT_TYPE_CHIPS_PURCHASE_TOTAL"),
                                                                                                         _currency_exchange_type,
                                                                                                         _tmp_iso_code,
                                                                                                         true);

              _add_amount_str = Currency.Format((Decimal)_row.Cells[GRID_COLUMN_INITIAL_AMOUNT].Value,
                                                _row.Cells[GRID_COLUMN_CURRENCY_ISO_CODE].Value.ToString());
              _add_amount_str += " = ";
              _add_amount_str += Currency.Format((Decimal)_row.Cells[GRID_COLUMN_ADD_AMOUNT_DECIMAL].Value, CurrencyExchange.GetNationalCurrency());

              _row.Cells[GRID_COLUMN_ADD_AMOUNT].Value = _add_amount_str;
              break;

            case CASHIER_MOVEMENT.CHIPS_SALE:
              _currency_exchange_type = (CurrencyExchangeType)Convert.ToInt32(_row.Cells[GRID_COLUMN_CAGE_CURRENCY_TYPE].Value);
              _tmp_iso_code = _row.Cells[GRID_COLUMN_CURRENCY_ISO_CODE].Value.ToString();

              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = GetMovementTypeDescription(Resource.String("STR_MOVEMENT_TYPE_CHIPS_SALES"),
                                                                                                          _currency_exchange_type,
                                                                                                          _tmp_iso_code,
                                                                                                          true);

              _row.Cells[GRID_COLUMN_SUB_AMOUNT].Value = Currency.Format((Decimal)_row.Cells[GRID_COLUMN_SUB_AMOUNT_DECIMAL].Value,
                                                  _row.Cells[GRID_COLUMN_CURRENCY_ISO_CODE].Value.ToString());

              break;

            case CASHIER_MOVEMENT.CHIPS_SALE_TOTAL:
              _currency_exchange_type = (CurrencyExchangeType)Convert.ToInt32(_row.Cells[GRID_COLUMN_CAGE_CURRENCY_TYPE].Value);
              _tmp_iso_code = _row.Cells[GRID_COLUMN_CURRENCY_ISO_CODE].Value.ToString();

              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = GetMovementTypeDescription(Resource.String("STR_MOVEMENT_TYPE_CHIPS_SALES_TOTAL"),
                                                                                                          _currency_exchange_type,
                                                                                                          _tmp_iso_code,
                                                                                                          true);

              _add_amount_str = Currency.Format((Decimal)_row.Cells[GRID_COLUMN_SUB_AMOUNT_DECIMAL].Value, CurrencyExchange.GetNationalCurrency());

              _row.Cells[GRID_COLUMN_SUB_AMOUNT].Value = _add_amount_str;
              break;

            case CASHIER_MOVEMENT.CHIPS_SALE_REMAINING_AMOUNT:
              _currency_exchange_type = (CurrencyExchangeType)Convert.ToInt32(_row.Cells[GRID_COLUMN_CAGE_CURRENCY_TYPE].Value);
              _tmp_iso_code = _row.Cells[GRID_COLUMN_CURRENCY_ISO_CODE].Value.ToString();

              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = GetMovementTypeDescription(Resource.String("STR_MOVEMENT_TYPE_CHIP_SALE_REMAINING_AMOUNT"),
                                                                                                          _currency_exchange_type,
                                                                                                          _tmp_iso_code,
                                                                                                          false);

              _add_amount_str = Currency.Format((Decimal)_row.Cells[GRID_COLUMN_SUB_AMOUNT_DECIMAL].Value, CurrencyExchange.GetNationalCurrency());

              _row.Cells[GRID_COLUMN_SUB_AMOUNT].Value = _add_amount_str;
              break;

            case CASHIER_MOVEMENT.CHIPS_SALE_CONSUMED_REMAINING_AMOUNT:
              _currency_exchange_type = (CurrencyExchangeType)Convert.ToInt32(_row.Cells[GRID_COLUMN_CAGE_CURRENCY_TYPE].Value);
              _tmp_iso_code = _row.Cells[GRID_COLUMN_CURRENCY_ISO_CODE].Value.ToString();

              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = GetMovementTypeDescription(Resource.String("STR_MOVEMENT_TYPE_CHIP_SALE_CONSUMED_REMAINING_AMOUNT"),
                                                                                                          _currency_exchange_type,
                                                                                                          _tmp_iso_code,
                                                                                                          false);

              _add_amount_str = Currency.Format(Convert.ToDecimal(_row.Cells[GRID_COLUMN_ADD_AMOUNT].Value), CurrencyExchange.GetNationalCurrency());

              _row.Cells[GRID_COLUMN_ADD_AMOUNT].Value = _add_amount_str;
              break;


            case CASHIER_MOVEMENT.CHIPS_SALE_TOTAL_EXCHANGE:
              _currency_exchange_type = (CurrencyExchangeType)Convert.ToInt32(_row.Cells[GRID_COLUMN_CAGE_CURRENCY_TYPE].Value);
              _tmp_iso_code = _row.Cells[GRID_COLUMN_CURRENCY_ISO_CODE].Value.ToString();

              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = GetMovementTypeDescription(Resource.String("STR_MOVEMENT_TYPE_CHIPS_SALES_TOTAL"),
                                                                                                          _currency_exchange_type,
                                                                                                          _tmp_iso_code,
                                                                                                          true);

              _add_amount_str = Currency.Format((Decimal)_row.Cells[GRID_COLUMN_INITIAL_AMOUNT].Value,
                                                _row.Cells[GRID_COLUMN_CURRENCY_ISO_CODE].Value.ToString());
              _add_amount_str += " = ";
              _add_amount_str += Currency.Format((Decimal)_row.Cells[GRID_COLUMN_SUB_AMOUNT_DECIMAL].Value, CurrencyExchange.GetNationalCurrency());

              _row.Cells[GRID_COLUMN_SUB_AMOUNT].Value = _add_amount_str;
              break;

            case CASHIER_MOVEMENT.CHIPS_SALE_DEVOLUTION_FOR_TITO:
              _currency_exchange_type = (CurrencyExchangeType)Convert.ToInt32(_row.Cells[GRID_COLUMN_CAGE_CURRENCY_TYPE].Value);
              _tmp_iso_code = _row.Cells[GRID_COLUMN_CURRENCY_ISO_CODE].Value.ToString();

              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = GetMovementTypeDescription(Resource.String("STR_MOVEMENT_TYPE_CHIPS_SALES_DEVOLUTION_FOR_TITO"),
                                                                                                          _currency_exchange_type,
                                                                                                          _tmp_iso_code,
                                                                                                          false);
              break;

            // DLL 02-JAN-2014
            case CASHIER_MOVEMENT.CASH_IN_TAX_SPLIT1:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = _cash_in_tax_name + ((_cash_in_tax_also_apply_to_split_b) ? " - " + m_split_a_name_str : "");
              break;
            // DRV 25-JUL-2014  
            case CASHIER_MOVEMENT.CASH_IN_TAX_SPLIT2:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = _cash_in_tax_name + " - " + m_split_b_name_str;
              break;

            // DHA 02-MAR-2016
            case CASHIER_MOVEMENT.TAX_PROVISIONS:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = _tax_provisions;
              break;

            // RMS 14-JAN-2014
            case CASHIER_MOVEMENT.TRANSFER_CREDIT_OUT:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_092");
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value += " (" + _row.Cells[GRID_COLUMN_DETAILS].Value + ")";
              break;

            case CASHIER_MOVEMENT.TRANSFER_CREDIT_IN:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_093");
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value += " (" + _row.Cells[GRID_COLUMN_DETAILS].Value + ")";
              break;

            // DLL 02-JAN-2014
            case CASHIER_MOVEMENT.CHIPS_CHANGE_IN:
              GetTypeNameColumn(Resource.String("STR_FRM_CHIPS_TITLE_CHANGE_CHIPS_IN"), _row, true, out _str_value, out _tmp_iso_code, out _currency_exchange_type);

              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = _str_value;
              _row.Cells[GRID_COLUMN_ADD_AMOUNT].Value = ((Currency)Convert.ToDecimal(_row.Cells[GRID_COLUMN_ADD_AMOUNT_DECIMAL].Value)).ToString(false);
              _row.Cells[GRID_COLUMN_SUB_AMOUNT].Value = DBNull.Value;
              break;

            case CASHIER_MOVEMENT.CHIPS_CHANGE_OUT:
              GetTypeNameColumn(Resource.String("STR_FRM_CHIPS_TITLE_CHANGE_CHIPS_OUT"), _row, true, out _str_value, out _tmp_iso_code, out _currency_exchange_type);

              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = _str_value;
              _row.Cells[GRID_COLUMN_ADD_AMOUNT].Value = DBNull.Value;
              _row.Cells[GRID_COLUMN_SUB_AMOUNT].Value = ((Currency)Convert.ToDecimal(_row.Cells[GRID_COLUMN_SUB_AMOUNT_DECIMAL].Value)).ToString(false);
              break;

            // RRR 29-JAN-2014: Gambling Collected Movement
            case CASHIER_MOVEMENT.GAMING_TABLE_GAME_NETWIN:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_GAMING_TABLE_GAME_COLLECTED_MOVEMENT", _movement_type);
              break;

            case CASHIER_MOVEMENT.GAMING_TABLE_GAME_NETWIN_CLOSE:
              GetTypeNameColumn(Resource.String("STR_VOUCHER_OVERVIEW_GAME_PROFIT"), _row);
              break;

            // HBB 29-MAY-2014
            case CASHIER_MOVEMENT.CHIPS_SALE_WITH_CASH_IN:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_CHIPS_SALE_WITH_CASH_IN");
              _row.Cells[GRID_COLUMN_ADD_AMOUNT].Value = ((Currency)Convert.ToDecimal(_row.Cells[GRID_COLUMN_ADD_AMOUNT_DECIMAL].Value)).ToString(false);
              _row.Cells[GRID_COLUMN_SUB_AMOUNT].Value = ((Currency)Convert.ToDecimal(_row.Cells[GRID_COLUMN_SUB_AMOUNT_DECIMAL].Value)).ToString(false);
              break;

            case CASHIER_MOVEMENT.CHIPS_PURCHASE_WITH_CASH_OUT:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_CHIPS_PURCHASE_WITH_CASH_OUT");
              _row.Cells[GRID_COLUMN_ADD_AMOUNT].Value = ((Currency)Convert.ToDecimal(_row.Cells[GRID_COLUMN_ADD_AMOUNT_DECIMAL].Value)).ToString(false);
              _row.Cells[GRID_COLUMN_SUB_AMOUNT].Value = ((Currency)Convert.ToDecimal(_row.Cells[GRID_COLUMN_SUB_AMOUNT_DECIMAL].Value)).ToString(false);
              break;

            // DHA 29-MAY-2014: Point to redeemable as cashin prize
            case CASHIER_MOVEMENT.POINTS_TO_REDEEMABLE_AS_CASHIN_PRIZE:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_CASHIER_MOV_POINTS_TO_REDEEMABLE_AS_CASHIN_PRIZE");
              _row.Cells[GRID_COLUMN_ADD_AMOUNT].Value = DBNull.Value;
              _row.Cells[GRID_COLUMN_SUB_AMOUNT].Value = ((Currency)Convert.ToDecimal(_row.Cells[GRID_COLUMN_SUB_AMOUNT_DECIMAL].Value)).ToString(false);
              break;

            case CASHIER_MOVEMENT.CHIPS_SALE_REGISTER_TOTAL:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_TABLE_SALE_REGISTER");
              break;

            case CASHIER_MOVEMENT.CASH_ADVANCE_GENERIC_BANK_CARD:
            // RAB 02-AUG-2016
            case CASHIER_MOVEMENT.CASH_ADVANCE_GENERIC_BANK_CARD_UNDO:
            case CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_GENERIC_BANK_CARD:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_096");
              _row.Cells[GRID_COLUMN_SUB_AMOUNT].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_COMMISSION") + ": " + Currency.Format((Decimal)_row.Cells[GRID_COLUMN_SUB_AMOUNT_DECIMAL].Value, CurrencyExchange.GetNationalCurrency());
              break;

            case CASHIER_MOVEMENT.CASH_ADVANCE_CREDIT_BANK_CARD:
            // RAB 04-AUG-2016
            case CASHIER_MOVEMENT.CASH_ADVANCE_CREDIT_BANK_CARD_UNDO:
            case CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_CREDIT_BANK_CARD:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_097");
              _row.Cells[GRID_COLUMN_SUB_AMOUNT].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_COMMISSION") + ": " + Currency.Format((Decimal)_row.Cells[GRID_COLUMN_SUB_AMOUNT_DECIMAL].Value, CurrencyExchange.GetNationalCurrency());
              break;

            case CASHIER_MOVEMENT.CASH_ADVANCE_DEBIT_BANK_CARD:
            // RAB 04-AUG-2016
            case CASHIER_MOVEMENT.CASH_ADVANCE_DEBIT_BANK_CARD_UNDO:
            case CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_DEBIT_BANK_CARD:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_098");
              _row.Cells[GRID_COLUMN_SUB_AMOUNT].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_COMMISSION") + ": " + Currency.Format((Decimal)_row.Cells[GRID_COLUMN_SUB_AMOUNT_DECIMAL].Value, CurrencyExchange.GetNationalCurrency());
              break;

            case CASHIER_MOVEMENT.CASH_ADVANCE_CHECK:
            case CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_CHECK:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_099");
              _row.Cells[GRID_COLUMN_SUB_AMOUNT].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_COMMISSION") + ": " + Currency.Format((Decimal)_row.Cells[GRID_COLUMN_SUB_AMOUNT_DECIMAL].Value, CurrencyExchange.GetNationalCurrency());
              break;

            case CASHIER_MOVEMENT.CASH_ADVANCE_CASH:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_100");
              break;

            case CASHIER_MOVEMENT.CASHIER_BY_CONCEPT_IN:
              _decimal_tmp = 0;
              GetTypeNameColumn(String.Empty, _row, true, out _str_value, out _tmp_iso_code, out _currency_exchange_type);
              if (Decimal.TryParse((String)_row.Cells[GRID_COLUMN_ADD_AMOUNT].Value, out _decimal_tmp) && _decimal_tmp < 0)
              {
                _row.Cells[GRID_COLUMN_TYPE_NAME].Value = "(" + Resource.String("STR_FRM_LAST_UNDO") + ") " + Resource.String("STR_VOUCHER_CONCEPTS_IN") + _str_value;
              }
              else
              {
                _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_VOUCHER_CONCEPTS_IN") + _str_value;
              }
              break;

            case CASHIER_MOVEMENT.CASHIER_BY_CONCEPT_OUT:
              _decimal_tmp = 0;
              GetTypeNameColumn(String.Empty, _row, true, out _str_value, out _tmp_iso_code, out _currency_exchange_type);
              if (Decimal.TryParse((String)_row.Cells[GRID_COLUMN_SUB_AMOUNT].Value, out _decimal_tmp) && _decimal_tmp < 0)
              {
                _row.Cells[GRID_COLUMN_TYPE_NAME].Value = "(" + Resource.String("STR_FRM_LAST_UNDO") + ") " + Resource.String("STR_VOUCHER_CONCEPTS_OUT") + _str_value;
              }
              else
              {
                _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_VOUCHER_CONCEPTS_OUT") + _str_value;
              }
              break;

            case CASHIER_MOVEMENT.HANDPAY_AUTHORIZED:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_101");
              break;

            case CASHIER_MOVEMENT.HANDPAY_UNAUTHORIZED:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_102");
              break;

            case CASHIER_MOVEMENT.HANDPAY_VOIDED:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_103");
              break;

            case CASHIER_MOVEMENT.HANDPAY_VOIDED_UNDONE:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_104");
              break;

            // DHA 08-OCT-2014: New movement (157) advice if a recharge is going to refund without plays
            case CASHIER_MOVEMENT.NOT_PLAYED_RECHARGE_REFUNDED:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_MOVEMENT_TYPE_NOT_PLAYED_RECHARGE_REFUNDED");
              break;

            // JML 03-AUG-2015 DEPRECATED 
            case CASHIER_MOVEMENT.CASH_CLOSING_SHORT_DEPRECATED:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_MOVEMENT_TYPE_CASH_CLOSING_SHORT_DEPRECATED");
              break;

            // JML 03-AUG-2015 DEPRECATED 
            case CASHIER_MOVEMENT.CASH_CLOSING_OVER_DEPRECATED:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_MOVEMENT_TYPE_CASH_CLOSING_OVER_DEPRECATED");
              break;

            // DHA 10-OCT-2014: added new movement (158) if there is cash short on closing cashier session
            case CASHIER_MOVEMENT.CASH_CLOSING_SHORT:
              GetTypeNameColumn(Resource.String("STR_MOVEMENT_TYPE_CASH_CLOSING_SHORT"), _row);
              break;

            // DHA 10-OCT-2014: added new movement (159) if there is cash over on closing cashier session
            case CASHIER_MOVEMENT.CASH_CLOSING_OVER:
              GetTypeNameColumn(Resource.String("STR_MOVEMENT_TYPE_CASH_CLOSING_OVER"), _row);
              break;

            // OPC 3-NOV-2014: Added new movement (514)
            case CASHIER_MOVEMENT.MB_EXCESS:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_107");
              break;

            case CASHIER_MOVEMENT.HANDPAY_WITHHOLDING:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_109");
              break;

            // FAV 06-NOV-2015: Added cashier movements to Gaming Hall
            case CASHIER_MOVEMENT.CASHIER_REFILL_HOPPER_TERMINAL_CAGE:
            case CASHIER_MOVEMENT.CASHIER_REFILL_HOPPER_TERMINAL:
              if (WSI.Common.Cashier.GetCashierNameFromCashierMovements((Int64)_row.Cells[GRID_COLUMN_OPERATION_ID].Value, CASHIER_MOVEMENT.TERMINAL_REFILL_HOPPER, out _terminal_refilled))
              {
                _terminal_refilled = " - " + _terminal_refilled;
              }

              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_MOVEMENT_TYPE_HOPPER_FILLER_IN_REQUEST") + _terminal_refilled;
              break;

            case CASHIER_MOVEMENT.CASHIER_COLLECT_DROPBOX_TERMINAL_CAGE:
            case CASHIER_MOVEMENT.CASHIER_COLLECT_DROPBOX_TERMINAL:
              if (WSI.Common.Cashier.GetCashierNameFromCashierMovements((Int64)_row.Cells[GRID_COLUMN_OPERATION_ID].Value, CASHIER_MOVEMENT.CASHIER_COLLECT_DROPBOX_TERMINAL, out _terminal_collected))
              {
                _terminal_collected = " - " + _terminal_collected;
              }

              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_MOVEMENT_TYPE_TERMINAL_COLLECT") + _terminal_collected;
              break;

            case CASHIER_MOVEMENT.CASHIER_CHANGE_STACKER_TERMINAL:
              if (WSI.Common.Cashier.GetCashierNameFromCashierMovements((Int64)_row.Cells[GRID_COLUMN_OPERATION_ID].Value, CASHIER_MOVEMENT.CASHIER_COLLECT_DROPBOX_TERMINAL, out _terminal_collected))
              {
                _terminal_collected = " - " + _terminal_collected;
              }

              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_MOVEMENT_TYPE_TERMINAL_CHANGE_STACKER_REQUEST") + _terminal_collected;
              break;

            case CASHIER_MOVEMENT.WIN_LOSS_STATEMENT_REQUEST:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_WIN_LOSS_STATEMENT_MOVEMENT");
              break;

            case CASHIER_MOVEMENT.CUSTOMER_ENTRANCE:
              string _this_currency = ((_row.DataBoundItem as DataRowView).Row["CM_CURRENCY_ISO_CODE"]).ToString();
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value =
                Resource.String("STR_FRM_MOV_RECEPTION_MONEY") + (_national_currency == _this_currency ? "" : " (" + _this_currency + ")");
              break;

            case CASHIER_MOVEMENT.CUSTOMER_ENTRANCE_CHECK:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_MOV_RECEPTION_CHECK");
              break;

            case CASHIER_MOVEMENT.CUSTOMER_ENTRANCE_CREDIT_CARD:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_MOV_RECEPTION_CARD_CREDIT");
              break;

            case CASHIER_MOVEMENT.CUSTOMER_ENTRANCE_DEBIT_CARD:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_MOV_RECEPTION_CARD_DEBIT");
              break;

            case CASHIER_MOVEMENT.CUSTOMER_ENTRANCE_CARD:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_MOV_RECEPTION_CARD");
              break;

            case CASHIER_MOVEMENT.TITO_TICKET_CASHIER_VOID_CASHABLE:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_TITO_TICKET_CASHIER_VOID_CASHABLE");
              _row.Cells[GRID_COLUMN_ADD_AMOUNT].Value = "-" + _row.Cells[GRID_COLUMN_ADD_AMOUNT].Value;
              break;

            case CASHIER_MOVEMENT.TITO_TICKET_CASHIER_VOID_PROMO_REDEEMABLE:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_TITO_TICKET_CASHIER_VOID_PROMO_REDEEMABLE");
              _row.Cells[GRID_COLUMN_ADD_AMOUNT].Value = "-" + _row.Cells[GRID_COLUMN_ADD_AMOUNT].Value;
              break;

            case CASHIER_MOVEMENT.TITO_TICKET_CASHIER_VOID_PROMO_NOT_REDEEMABLE:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_TITO_TICKET_CASHIER_VOID_PROMO_NOT_REDEEMABLE");
              _row.Cells[GRID_COLUMN_ADD_AMOUNT].Value = "-" + _row.Cells[GRID_COLUMN_ADD_AMOUNT].Value;
              break;

            case CASHIER_MOVEMENT.TITO_TICKET_MACHINE_VOID_CASHABLE:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_TITO_TICKET_MACHINE_VOID_CASHABLE");
              _row.Cells[GRID_COLUMN_ADD_AMOUNT].Value = "-" + _row.Cells[GRID_COLUMN_ADD_AMOUNT].Value;
              break;

            case CASHIER_MOVEMENT.TITO_TICKET_MACHINE_VOID_PROMO_NOT_REDEEMABLE:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_TITO_TICKET_MACHINE_VOID_PROMO_NOT_REDEEMABLE");
              _row.Cells[GRID_COLUMN_ADD_AMOUNT].Value = "-" + _row.Cells[GRID_COLUMN_ADD_AMOUNT].Value;
              break;

            case CASHIER_MOVEMENT.CURRENCY_EXCHANGE_TO_NATIONAL_CURRENCY:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_CURRENCY_EXCHANGE");
              if (_row.Cells[GRID_COLUMN_CURRENCY_ISO_CODE].Value.ToString() != Cage.CHIPS_COLOR)
              {
                _row.Cells[GRID_COLUMN_ADD_AMOUNT].Value = Currency.Format((Decimal)_row.Cells[GRID_COLUMN_INITIAL_AMOUNT].Value, (String)_row.Cells[GRID_COLUMN_CURRENCY_ISO_CODE].Value) + " = " + Currency.Format((Decimal)_row.Cells[GRID_COLUMN_ADD_AMOUNT_DECIMAL].Value, CurrencyExchange.GetNationalCurrency());
              }

              _row.Cells[GRID_COLUMN_SUB_AMOUNT].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_COMMISSION") + ": " + Currency.Format((Decimal)_row.Cells[GRID_COLUMN_SUB_AMOUNT_DECIMAL].Value, CurrencyExchange.GetNationalCurrency());

              break;

            case CASHIER_MOVEMENT.CURRENCY_EXCHANGE_AMOUNT_OUT:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_CURRENCY_EXCHANGE_COLLECTED");
              _row.Cells[GRID_COLUMN_ADD_AMOUNT].Value = _row.Cells[GRID_COLUMN_SUB_AMOUNT].Value;
              _row.Cells[GRID_COLUMN_SUB_AMOUNT].Value = "";
              break;

            case CASHIER_MOVEMENT.CURRENCY_EXCHANGE_TO_FOREIGN_CURRENCY:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_CURRENCY_DEVOLUTION");
              if (_row.Cells[GRID_COLUMN_CURRENCY_ISO_CODE].Value.ToString() != Cage.CHIPS_COLOR)
              {
                _row.Cells[GRID_COLUMN_ADD_AMOUNT].Value = Currency.Format((Decimal)_row.Cells[GRID_COLUMN_ADD_AMOUNT_DECIMAL].Value, CurrencyExchange.GetNationalCurrency()) + " = " + Currency.Format((Decimal)_row.Cells[GRID_COLUMN_INITIAL_AMOUNT].Value, (String)_row.Cells[GRID_COLUMN_CURRENCY_ISO_CODE].Value);
              }

              _row.Cells[GRID_COLUMN_SUB_AMOUNT].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_COMMISSION") + ": " + Currency.Format((Decimal)_row.Cells[GRID_COLUMN_SUB_AMOUNT_DECIMAL].Value, CurrencyExchange.GetNationalCurrency());
              break;

            case CASHIER_MOVEMENT.CURRENCY_EXCHANGE_AMOUNT_IN:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_CURRENCY_DEVOLUTION_COLLECTED");
              break;

            case CASHIER_MOVEMENT.CURRENCY_EXCHANGE_AMOUNT_IN_CHANGE:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_CURRENCY_DEVOLUTION_CHANGE");
              break;

            case CASHIER_MOVEMENT.TAX_CUSTODY:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Misc.TaxCustodyName();
              break;

            case CASHIER_MOVEMENT.TAX_RETURNING_CUSTODY:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Misc.TaxCustodyRefundName();
              break;

            //EOR 17-MAR-2016
            case CASHIER_MOVEMENT.TITO_TICKET_CASHIER_SWAP_FOR_CHIPS:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_TICKET_TITO_SWAPS_CHIPS");
              break;

            case CASHIER_MOVEMENT.CLOSE_SESSION_BANK_CARD:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_MOV_CLOSE_SESSION_BANK_CARD");
              _row.Cells[GRID_COLUMN_SUB_AMOUNT].Value = ((Currency)Convert.ToDecimal(_row.Cells[GRID_COLUMN_FINAL_AMOUNT].Value)).ToString(false);
              break;

            case CASHIER_MOVEMENT.CLOSE_SESSION_CHECK:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_MOV_CLOSE_SESSION_CHECK");
              _row.Cells[GRID_COLUMN_SUB_AMOUNT].Value = ((Currency)Convert.ToDecimal(_row.Cells[GRID_COLUMN_FINAL_AMOUNT].Value)).ToString(false);
              break;

            case CASHIER_MOVEMENT.CAGE_CLOSE_SESSION:
              GetTypeNameColumn(Resource.String("STR_MOV_CAGE_CLOSE_SESSION") + " - " + Resource.String("STR_UC_GIFT_DELIVERY_002"), _row);

              if ((CageCurrencyType)_row.Cells[GRID_COLUMN_CAGE_CURRENCY_TYPE].Value == CageCurrencyType.ChipsColor)
              {
                _row.Cells[GRID_COLUMN_SUB_AMOUNT].Value = Convert.ToInt32(_row.Cells[GRID_COLUMN_FINAL_AMOUNT].Value);
              }
              else
              {
                _row.Cells[GRID_COLUMN_SUB_AMOUNT].Value = ((Currency)Convert.ToDecimal(_row.Cells[GRID_COLUMN_FINAL_AMOUNT].Value)).ToString(false);
              }

              break;

            case CASHIER_MOVEMENT.CREDIT_LINE_MARKER:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_MOV_CREDITLINE_GETMARKER");

              break;

            case CASHIER_MOVEMENT.CREDIT_LINE_PAYBACK:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_MOV_CREDITLINE_PAYBACK");
              break;

            case CASHIER_MOVEMENT.CREDIT_LINE_MARKER_CARD_REPLACEMENT:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_MOV_CREDITLINE_GETMARKER_CARD_REPLACEMENT");
              break;

            default:
              if ((CASHIER_MOVEMENT)_movement_type > CASHIER_MOVEMENT.MULTIPLE_BUCKETS_END_SESSION && (CASHIER_MOVEMENT)_movement_type <= CASHIER_MOVEMENT.MULTIPLE_BUCKETS_MANUAL_SET_LAST) // (1100 - 1599)
              {
                _buckets_forms = new BucketsForm();
                // There are no ranks, this switch is implemented to cover multiple buckets ranges.
                // First, we divide movement_type by the range and then check its range starting with 11
                String _bucket_name = "";
                Int32 _movement_base = _movement_type / 100;
                _movement_base = _movement_base * 100;

                if (_buckets_forms.GetBucketName((CASHIER_MOVEMENT)_movement_type, out _bucket_name))
                {
                  switch ((CASHIER_MOVEMENT)_movement_base)
                  {
                    case CASHIER_MOVEMENT.MULTIPLE_BUCKETS_END_SESSION: //MULTIPLE_BUCKETS_EndSession Range (1100 to 1199)
                      _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_BUCKET_EARNED_MOVEMENT", _bucket_name);
                      break;
                    case CASHIER_MOVEMENT.MULTIPLE_BUCKETS_EXPIRED:     //MULTIPLE_BUCKETS_Expired Range (1200 to 1299)
                      _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_BUCKET_EXPIRED_MOVEMENT", _bucket_name);
                      break;
                    case CASHIER_MOVEMENT.MULTIPLE_BUCKETS_MANUAL_ADD:  //MULTIPLE_BUCKETS_Manual_Add Range (1300 to 1399)
                      _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_BUCKET_ADD_MOVEMENT", _bucket_name);
                      break;
                    case CASHIER_MOVEMENT.MULTIPLE_BUCKETS_MANUAL_SUB:  //MULTIPLE_BUCKETS_MANUAL_Sub Range (1400 to 1499)     
                      _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_BUCKET_SUB_MOVEMENT", _bucket_name);
                      break;
                    case CASHIER_MOVEMENT.MULTIPLE_BUCKETS_MANUAL_SET:  //MULTIPLE_BUCKETS_Manual_Set Range (1500 to 1599)
                      _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_BUCKET_SET_MOVEMENT", _bucket_name);
                      break;
                  }

                  break;
                }
              }

              // FBA 12-AUG-2013 Movement not defined -> Movement (XX)
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DEFAULT_MOVEMENT", _movement_type);

              break;
          }

          if (_row.Cells[GRID_COLUMN_STATUS].Value != DBNull.Value
              && (UndoStatus)_row.Cells[GRID_COLUMN_STATUS].Value == UndoStatus.UndoOperation
              && (CASHIER_MOVEMENT)_movement_type != CASHIER_MOVEMENT.MANUAL_HANDPAY_CANCELLATION
              && (CASHIER_MOVEMENT)_movement_type != CASHIER_MOVEMENT.HANDPAY_CANCELLATION)
          {
            _str_value = "";
            _str_value = "(" + Resource.String("STR_FRM_LAST_UNDO") + ") " + _row.Cells[GRID_COLUMN_TYPE_NAME].Value;
            _row.Cells[GRID_COLUMN_TYPE_NAME].Value = _str_value;
          }
        }
        else if (m_mov_show == MovementShow.Mobile_Bank)
        {
          // ICS 16-APR-2013: Change the cashier name by its alias
          _row.Cells[GRID_COLUMN_TERMINAL].Value = Computer.Alias(_row.Cells[GRID_COLUMN_TERMINAL].Value.ToString());

          switch ((MBMovementType)_movement_type)
          {
            case MBMovementType.AutomaticChangeLimit:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_072");
              break;

            case MBMovementType.ManualChangeLimit:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_028");
              break;

            case MBMovementType.TransferCredit:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_030");
              break;

            case MBMovementType.CreditNonRedeemable:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_033");
              break;

            case MBMovementType.CreditCoverCoupon:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_064");
              break;

            case MBMovementType.DepositCash:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_029");
              break;

            case MBMovementType.CloseSession:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_031");
              break;

            case MBMovementType.CloseSessionWithCashLost:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_074");
              _row.Cells[GRID_COLUMN_INITIAL_AMOUNT].Value = DBNull.Value;
              _row.Cells[GRID_COLUMN_FINAL_AMOUNT].Value = DBNull.Value;
              break;

            // OPC 3-NOV-2014
            case MBMovementType.CashExcess:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_105");
              break;

            // JBC 14-NOV-2014
            case MBMovementType.CashShortFall:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_UC_CARD_PENDING_DEPOSITS");
              break;

            // OPC 3-NOV-2014
            case MBMovementType.CloseSessionWithExcess:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_106");
              _row.Cells[GRID_COLUMN_INITIAL_AMOUNT].Value = DBNull.Value;
              _row.Cells[GRID_COLUMN_FINAL_AMOUNT].Value = DBNull.Value;
              break;

            default:
              // FBA 12-AUG-2013 Movement not defined -> Movement (XX)
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DEFAULT_MOVEMENT", ((int)_movement_type).ToString());
              break;

          }

          if (_row.Cells[GRID_COLUMN_STATUS_UNDO].Value != DBNull.Value
              && (UndoStatus)_row.Cells[GRID_COLUMN_STATUS_UNDO].Value == UndoStatus.UndoOperation)
          {
            _str_value = "";
            _str_value = "(" + Resource.String("STR_FRM_LAST_UNDO") + ") " + _row.Cells[GRID_COLUMN_TYPE_NAME].Value;
            _row.Cells[GRID_COLUMN_TYPE_NAME].Value = _str_value;
          }
        }
        else if (m_mov_show == MovementShow.Player_Tracking)
        {
          dataGridView1.Columns[GRID_COLUMN_PT_SECOND_ACCOUNT].DefaultCellStyle.Format = "#,##0";
          switch ((MovementType)_movement_type)
          {
            case MovementType.Play:
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_001");
              _row.Cells[GRID_COLUMN_PT_SECOND_ACCOUNT].Value = DBNull.Value;
              break;

            default:
              // FBA 12-AUG-2013 Movement not defined -> Movement (XX)
              _row.Cells[GRID_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DEFAULT_MOVEMENT", ((int)_movement_type).ToString());
              break;
          }
        }
      }// for

      form_yes_no.Show();
      this.ShowDialog();
      form_yes_no.Hide();
    } // Show


    /// <summary>
    /// Function that set the title on grid in function that teh movement is showed on screen
    /// </summary>
    private void CreateDataGridHeaders()
    {
      switch (m_mov_show)
      {
        case MovementShow.Card:
          dataGridView1.Columns[GRID_COLUMN_TYPE_ID].HeaderCell.Value = "TypeId"; // Not Visible
          dataGridView1.Columns[GRID_COLUMN_TYPE_NAME].HeaderCell.Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DATAGRID_CARD_TYPE");
          dataGridView1.Columns[GRID_COLUMN_DATE].HeaderCell.Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DATAGRID_CARD_DATE");
          dataGridView1.Columns[GRID_COLUMN_USER_NAME].HeaderCell.Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DATAGRID_CARD_USER");
          dataGridView1.Columns[GRID_COLUMN_TERMINAL].HeaderCell.Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DATAGRID_CARD_TERMINAL");
          dataGridView1.Columns[GRID_COLUMN_TERM_ID].HeaderCell.Value = "TermId"; // Not Visible
          dataGridView1.Columns[GRID_COLUMN_INITIAL_AMOUNT].HeaderCell.Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DATAGRID_CARD_BAL_INITIAL");
          dataGridView1.Columns[GRID_COLUMN_SUB_AMOUNT].HeaderCell.Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DATAGRID_CARD_BAL_SUBS");
          dataGridView1.Columns[GRID_COLUMN_ADD_AMOUNT].HeaderCell.Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DATAGRID_CARD_BAL_ADDED");
          dataGridView1.Columns[GRID_COLUMN_FINAL_AMOUNT].HeaderCell.Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DATAGRID_CARD_BAL_FINAL");
          break;

        case MovementShow.Cashier:
          dataGridView1.Columns[GRID_COLUMN_TYPE_ID].HeaderCell.Value = "TypeId"; // Not Visible
          dataGridView1.Columns[GRID_COLUMN_TYPE_NAME].HeaderCell.Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DATAGRID_CASH_DESK_TYPE");
          dataGridView1.Columns[GRID_COLUMN_DATE].HeaderCell.Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DATAGRID_CASH_DESK_DATE");
          dataGridView1.Columns[GRID_COLUMN_USER_NAME].HeaderCell.Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DATAGRID_CASH_DESK_USER");
          dataGridView1.Columns[GRID_COLUMN_TERMINAL].HeaderCell.Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DATAGRID_CASH_DESK_CASHIER");
          dataGridView1.Columns[GRID_COLUMN_TERM_ID].HeaderCell.Value = "TermId"; // Not Visible
          dataGridView1.Columns[GRID_COLUMN_INITIAL_AMOUNT].HeaderCell.Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DATAGRID_CASH_DESK_BAL_INITIAL");
          dataGridView1.Columns[GRID_COLUMN_SUB_AMOUNT].HeaderCell.Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DATAGRID_CASH_DESK_BAL_SUBS");
          dataGridView1.Columns[GRID_COLUMN_ADD_AMOUNT].HeaderCell.Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DATAGRID_CASH_DESK_BAL_ADDED");
          dataGridView1.Columns[GRID_COLUMN_FINAL_AMOUNT].HeaderCell.Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DATAGRID_CASH_DESK_BAL_FINAL");
          dataGridView1.Columns[GRID_COLUMN_TERMINAL].Width = 150;
          dataGridView1.Columns[GRID_COLUMN_SUB_AMOUNT].Width = 160;
          dataGridView1.Columns[GRID_COLUMN_ADD_AMOUNT].Width = 198;
          dataGridView1.Columns[GRID_COLUMN_FINAL_AMOUNT].Visible = false;
          break;

        case MovementShow.Mobile_Bank:
          dataGridView1.Columns[GRID_COLUMN_TYPE_ID].HeaderCell.Value = "TypeId"; // Not Visible
          dataGridView1.Columns[GRID_COLUMN_TYPE_NAME].HeaderCell.Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DATAGRID_CASH_DESK_TYPE");
          dataGridView1.Columns[GRID_COLUMN_DATE].HeaderCell.Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DATAGRID_CASH_DESK_DATE");
          dataGridView1.Columns[GRID_COLUMN_USER_NAME].HeaderCell.Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DATAGRID_CASH_DESK_USER");
          dataGridView1.Columns[GRID_COLUMN_TERMINAL].HeaderCell.Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DATAGRID_CASH_DESK_CASHIER");
          dataGridView1.Columns[GRID_COLUMN_TERM_ID].HeaderCell.Value = "TermId"; // Not Visible
          dataGridView1.Columns[GRID_COLUMN_INITIAL_AMOUNT].HeaderCell.Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DATAGRID_CASH_DESK_BAL_INITIAL");
          dataGridView1.Columns[GRID_COLUMN_SUB_AMOUNT].HeaderCell.Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DATAGRID_CASH_DESK_BAL_SUBS");
          dataGridView1.Columns[GRID_COLUMN_ADD_AMOUNT].HeaderCell.Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DATAGRID_CASH_DESK_BAL_ADDED");
          dataGridView1.Columns[GRID_COLUMN_FINAL_AMOUNT].HeaderCell.Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DATAGRID_CASH_DESK_BAL_FINAL");
          break;

        case MovementShow.Player_Tracking:
          dataGridView1.Columns[GRID_COLUMN_TYPE_ID].HeaderCell.Value = "TypeId"; // Not Visible
          dataGridView1.Columns[GRID_COLUMN_TYPE_NAME].HeaderCell.Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DATAGRID_CARD_TYPE");
          dataGridView1.Columns[GRID_COLUMN_DATE].HeaderCell.Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DATAGRID_CARD_DATE");
          dataGridView1.Columns[GRID_COLUMN_USER_NAME].HeaderCell.Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DATAGRID_CARD_USER");
          dataGridView1.Columns[GRID_COLUMN_TERMINAL].HeaderCell.Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DATAGRID_CARD_TERMINAL");
          dataGridView1.Columns[GRID_COLUMN_TERM_ID].HeaderCell.Value = "TermId"; // Not Visible
          dataGridView1.Columns[GRID_COLUMN_PT_INITIAL_AMOUNT].HeaderCell.Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DATAGRID_CARD_BAL_INITIAL");
          dataGridView1.Columns[GRID_COLUMN_PT_SUB_AMOUNT].HeaderCell.Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DATAGRID_CARD_BAL_SUBS");
          dataGridView1.Columns[GRID_COLUMN_PT_ADD_AMOUNT].HeaderCell.Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DATAGRID_CARD_BAL_ADDED");
          dataGridView1.Columns[GRID_COLUMN_PT_FINAL_AMOUNT].HeaderCell.Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DATAGRID_CARD_BAL_FINAL");
          dataGridView1.Columns[GRID_COLUMN_PT_SECOND_ACCOUNT].HeaderCell.Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DATAGRID_CARD_TARGET");
          break;

      }
    } // CreateDataGridHeaders

    /// <summary>
    /// Returns the Account points status description
    /// </summary>
    /// <param name="AccountPointsStatus"></param>
    /// <returns></returns>
    private String GetPointsStatusDescription(ACCOUNT_POINTS_STATUS AccountPointsStatus)
    {
      String _points_status_description;
      _points_status_description = String.Empty;

      switch (AccountPointsStatus)
      {
        case WSI.Common.ACCOUNT_POINTS_STATUS.REDEEM_ALLOWED:
          _points_status_description = Resource.String("STR_FRM_LAST_MOVEMENTS_ALLOWED");
          break;

        case WSI.Common.ACCOUNT_POINTS_STATUS.REDEEM_NOT_ALLOWED:
          _points_status_description = Resource.String("STR_FRM_LAST_MOVEMENTS_NOT_ALLOWED");
          break;
      }

      return _points_status_description;
    }

    /// <summary>Get type name movement</summary>
    /// <param name="StringResource">First part of type name movement</param>
    /// <param name="Row">Grid current row</param>
    private void GetTypeNameColumn(String StringResource, DataGridViewRow Row)
    {
      //Define current iso code, current cage currency type and current denomination from dratagridview rows.
      String _current_iso_code;
      CageCurrencyType _current_cage_currency_type;
      Decimal _denomination;

      _current_iso_code = Row.Cells[GRID_COLUMN_CURRENCY_ISO_CODE].Value.ToString();
      _current_cage_currency_type = (CageCurrencyType)Row.Cells[GRID_COLUMN_CAGE_CURRENCY_TYPE].Value;
      _denomination = Convert.ToDecimal(Row.Cells[GRID_COLUMN_DENOMINATION].Value);

      if (String.IsNullOrEmpty(_current_iso_code))
      {
        _current_iso_code = CurrencyExchange.GetNationalCurrency();
      }

      if (FeatureChips.IsChipsType(_current_cage_currency_type))
      {
        Row.Cells[GRID_COLUMN_TYPE_NAME].Value = StringResource + " - " + FeatureChips.GetChipTypeDescription((CurrencyExchangeType)_current_cage_currency_type, _current_iso_code);
      }
      else if (_denomination == Cage.BANK_CARD_CODE)
      {
        Row.Cells[GRID_COLUMN_TYPE_NAME].Value = StringResource + " - " + Resource.String("STR_CURRENCY_TYPE_1");
      }
      else if (_denomination == Cage.CHECK_CODE)
      {
        Row.Cells[GRID_COLUMN_TYPE_NAME].Value = StringResource + " - " + Resource.String("STR_CURRENCY_TYPE_2");
      }
      else
      {
        Row.Cells[GRID_COLUMN_TYPE_NAME].Value = StringResource + " - " + Resource.String("STR_REFUND_CASH") + " (" + _current_iso_code + ")";
      }
    }

    /// <summary>Get type name movement</summary>
    /// <param name="StringResource">First part of type name movement</param>
    /// <param name="Row">Grid current row</param>
    /// <param name="AppendIsoCode">It indicates if it is put iso code or not</param>
    /// <param name="TypeName">Name to be introduced in the column</param>
    /// <param name="TmpIsoCode">Temporal iso code</param>
    /// <param name="CurrencyExchangeType">Currency exchange type</param>
    private void GetTypeNameColumn(String StringResource, DataGridViewRow Row, Boolean AppendIsoCode, out String TypeName, out String TmpIsoCode, out CurrencyExchangeType CurrencyExchangeType)
    {
      String _str_subtype;

      TypeName = StringResource;
      CurrencyExchangeType = (CurrencyExchangeType)Convert.ToInt32(Row.Cells[GRID_COLUMN_CAGE_CURRENCY_TYPE].Value);
      TmpIsoCode = Row.Cells[GRID_COLUMN_CURRENCY_ISO_CODE].Value.ToString();
      _str_subtype = GetChipsMovementText(CurrencyExchangeType, TmpIsoCode, AppendIsoCode);

      if (!String.IsNullOrEmpty(_str_subtype))
      {
        TypeName += " - " + _str_subtype;
      }
    } // GetTypeNameColumn

    //------------------------------------------------------------------------------
    // PURPOSE : Dispose yes_no form.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    new public void Dispose()
    {
      form_yes_no.Dispose();
      form_yes_no = null;

      base.Dispose();
    }

    #endregion

    #region DataGridEvents

    //------------------------------------------------------------------------------
    // PURPOSE : Paint a datagrid row before actually filling it.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void dataGridView1_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
    {
      if (m_mov_show == MovementShow.Card)
      {
        // set the color
        dataGridView1.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.FromName((String)dataGridView1.Rows[e.RowIndex].Cells[GRID_COLUMN_ROW_COLOR].Value);

        //Set points units for buckets movement types. 
        if ((MovementType)dataGridView1.Rows[e.RowIndex].Cells[GRID_COLUMN_TYPE_ID].Value > MovementType.MULTIPLE_BUCKETS_EndSession && (MovementType)dataGridView1.Rows[e.RowIndex].Cells[GRID_COLUMN_TYPE_ID].Value < MovementType.MULTIPLE_BUCKETS_Manual_Set_Last)
        {
          BucketsForm _bucketForm;
          _bucketForm = new BucketsForm();
          Buckets.BucketType _bucketType;

          _bucketForm.GetBucketType((MovementType)dataGridView1.Rows[e.RowIndex].Cells[GRID_COLUMN_TYPE_ID].Value, out _bucketType);

          if (Buckets.GetBucketUnits(_bucketType) == Buckets.BucketUnits.Points)
          {
            dataGridView1.Rows[e.RowIndex].Cells[GRID_COLUMN_INITIAL_AMOUNT].Style.Format = "N0";
            dataGridView1.Rows[e.RowIndex].Cells[GRID_COLUMN_SUB_AMOUNT].Style.Format = "N0";
            dataGridView1.Rows[e.RowIndex].Cells[GRID_COLUMN_ADD_AMOUNT].Style.Format = "N0";
            dataGridView1.Rows[e.RowIndex].Cells[GRID_COLUMN_FINAL_AMOUNT].Style.Format = "N0";
          }
        }
      }
      else if (m_mov_show == MovementShow.Cashier)
      {
        switch ((CASHIER_MOVEMENT)dataGridView1.Rows[e.RowIndex].Cells[GRID_COLUMN_TYPE_ID].Value)
        {
          case CASHIER_MOVEMENT.REOPEN_CASHIER:
          case CASHIER_MOVEMENT.OPEN_SESSION:
          case CASHIER_MOVEMENT.CLOSE_SESSION:
          case CASHIER_MOVEMENT.CHECK_PAYMENT:
            dataGridView1.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.NavajoWhite;
            break;

          case CASHIER_MOVEMENT.MB_CASH_LOST:
          case CASHIER_MOVEMENT.MB_EXCESS:
          case CASHIER_MOVEMENT.CASH_IN:
          case CASHIER_MOVEMENT.CASH_OUT:
          case CASHIER_MOVEMENT.MB_CASH_IN:
          case CASHIER_MOVEMENT.CHIPS_SALE_REGISTER_TOTAL:
            if (dataGridView1.Rows[e.RowIndex].Cells[GRID_COLUMN_STATUS].Value != DBNull.Value
                && (UndoStatus)dataGridView1.Rows[e.RowIndex].Cells[GRID_COLUMN_STATUS].Value == UndoStatus.UndoOperation)
            {
              dataGridView1.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.Gray;
              dataGridView1.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.White;
              break;
            }

            dataGridView1.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LightSalmon;
            break;

          case CASHIER_MOVEMENT.FILLER_OUT:
          case CASHIER_MOVEMENT.FILLER_OUT_CHECK:
            dataGridView1.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.Coral;
            break;

          case CASHIER_MOVEMENT.CURRENCY_CARD_EXCHANGE_SPLIT1:
          // FGB 09-AUG-2016 
          case CASHIER_MOVEMENT.CURRENCY_CARD_EXCHANGE_SPLIT1_UNDO:
          // RMS 08-AUG-2014
          case CASHIER_MOVEMENT.CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT1:
          case CASHIER_MOVEMENT.CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT1:
          case CASHIER_MOVEMENT.CURRENCY_EXCHANGE_SPLIT1:
          case CASHIER_MOVEMENT.CURRENCY_CHECK_SPLIT1:
          case CASHIER_MOVEMENT.CASH_ADVANCE_CHECK:
          case CASHIER_MOVEMENT.CASH_ADVANCE_CREDIT_BANK_CARD:
          case CASHIER_MOVEMENT.CASH_ADVANCE_DEBIT_BANK_CARD:
          case CASHIER_MOVEMENT.CASH_ADVANCE_GENERIC_BANK_CARD:
          case CASHIER_MOVEMENT.CASH_ADVANCE_CURRENCY_EXCHANGE:
          // DLL 15-DEC-2014
          case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CARD_EXCHANGE_SPLIT1:
          case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT1:
          case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT1:
          // DLL 17-DEC-2014          
          case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_EXCHANGE_SPLIT1:
          case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CHECK_SPLIT1:
          // DLL 18-DEC-2014
          case CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_CURRENCY_EXCHANGE:
          case CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_GENERIC_BANK_CARD:
          case CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_CREDIT_BANK_CARD:
          case CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_DEBIT_BANK_CARD:
          case CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_CHECK:
          case CASHIER_MOVEMENT.CREDIT_LINE_PAYBACK_CARD_EXCHANGE:
          case CASHIER_MOVEMENT.CREDIT_LINE_PAYBACK_CREDIT_CARD_EXCHANGE:
          case CASHIER_MOVEMENT.CREDIT_LINE_PAYBACK_DEBIT_CARD_EXCHANGE:
          case CASHIER_MOVEMENT.CREDIT_LINE_PAYBACK_CHECK_EXCHANGE:
            if (dataGridView1.Rows[e.RowIndex].Cells[GRID_COLUMN_STATUS].Value != DBNull.Value
              && (UndoStatus)dataGridView1.Rows[e.RowIndex].Cells[GRID_COLUMN_STATUS].Value == UndoStatus.UndoOperation)
            {
              dataGridView1.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.Gray;
              dataGridView1.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.White;
              break;
            }

            dataGridView1.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.Salmon;
            break;

          case CASHIER_MOVEMENT.CHIPS_PURCHASE_TOTAL:
          case CASHIER_MOVEMENT.CHIPS_SALE_TOTAL:
          case CASHIER_MOVEMENT.CHIPS_PURCHASE_TOTAL_EXCHANGE:
          case CASHIER_MOVEMENT.CHIPS_SALE_TOTAL_EXCHANGE:
            dataGridView1.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.Salmon;
            break;

          default:
            dataGridView1.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.White;
            break;
        }
      }
      else if (m_mov_show == MovementShow.Mobile_Bank)
      {
        switch ((MBMovementType)dataGridView1.Rows[e.RowIndex].Cells[GRID_COLUMN_TYPE_ID].Value)
        {
          case MBMovementType.TransferCredit:
            dataGridView1.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.Coral;
            break;

          case MBMovementType.CreditNonRedeemable:
          case MBMovementType.CreditCoverCoupon:
            dataGridView1.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LightSteelBlue;
            break;

          case MBMovementType.CloseSessionWithCashLost:
          case MBMovementType.DepositCash:
          case MBMovementType.CloseSessionWithExcess:
            dataGridView1.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.NavajoWhite;
            break;

          default:
            break;
        }
      }

      SetColumnsVisibility();
    } // dataGridView1_RowPrePaint

    #endregion DataGridEvents

    #region Buttons

    //------------------------------------------------------------------------------
    // PURPOSE : Handle the click on the Exit button.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //

    private void btn_exit_Click(object sender, EventArgs e)
    {
      Misc.WriteLog("[FORM CLOSE] frm_last_movements (exit)", Log.Type.Message);
      this.Close();
    } // btn_exit_Click

    private void frm_last_movements_Shown(object sender, EventArgs e)
    {
      this.dataGridView1.Focus();
    } // frm_last_movements_Shown

    #endregion Buttons

  }
}