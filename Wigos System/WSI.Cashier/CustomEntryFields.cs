//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : CustomEntryFields.cs
// 
//   DESCRIPTION : File that contains all custom entry fields classes:
//                  - NumericTextBox
//                  - PhoneNumberTextBox
//                  - CharacterTextBox
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 05-FEB-2013 LEM    First release
// 05-FEB-2013 LEM    Imported class NumericTextBox from frm_player_edit
// 11-FEB-2013 ICS    Added new functions to validate format of the entry data
// 01-JUL-2013 ANG    Added ICustomFormatValidable to atach existing filters to new ucontrols
// 30-NOV-2013 RMS    Added ValidateTime and ValidateDateTime functions
// -----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using WSI.Common;
using System.Windows.Forms;

namespace WSI.Cashier
{
  public class NumericTextBox : WSI.Cashier.Controls.uc_round_textbox, ICustomFormatValidable
  {
    Regex m_reg_exp;
    Boolean m_allow_space;
    Boolean m_fill_ceros;

    // PURPOSE: Initialize class members
    //
    //  PARAMS:
    //     - INPUT:
    //           - none
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - none
    public NumericTextBox()
    {
      m_allow_space = false;
      m_fill_ceros = false;
      ChangeRegex();
    }

    // PURPOSE: Update the regular expression according to space chatacter is allowed
    //
    //  PARAMS:
    //     - INPUT:
    //           - none
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - void
    private void ChangeRegex()
    {
      if (m_allow_space)
      {
        m_reg_exp = new Regex("^[\\d\\s]*$");
      }
      else
      {
        m_reg_exp = new Regex("^[\\d]*$");
      }
    }

    // PURPOSE: Check OnKeyPress event if valid character
    //
    //  PARAMS:
    //     - INPUT:
    //           - KeyPressEventArgs e
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - void
    protected override void OnKeyPress(object sender, KeyPressEventArgs e)
    {
      base.OnKeyPress(sender, e);

      if (e.KeyChar == '\b' ||
         (e.KeyChar == ' ' && AllowSpace) ||
         Char.IsDigit(e.KeyChar))
      {
        e.Handled = false;
      }
      else
      {
        e.Handled = true;
      }
    }

    protected override void OnLostFocus(EventArgs e)
    {
      base.OnLostFocus(e);

      SetText(this.Text);
    }

    public override string Text
    {
      get
      {
        return base.Text;
      }
      set
      {
        SetText(value);
      }
    }

    private void SetText(String Value)
    {
      if (m_fill_ceros && Value.Trim() != "")
      {
        base.Text = Value.Trim().PadLeft(this.MaxLength, '0');
      }
      else
      {
        base.Text = Value;
      }
    }

    // PURPOSE: Property to modify m_allow_space member
    //
    //  PARAMS:
    //     - INPUT:
    //           - new value
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - Boolean: m_allow_space value
    public Boolean AllowSpace
    {
      set
      {
        this.m_allow_space = value;
        ChangeRegex();
      }

      get
      {
        return this.m_allow_space;
      }
    }

    public Boolean FillWithCeros
    {
      get { return m_fill_ceros; }
      set { m_fill_ceros = value; }
    }

    // PURPOSE: Return the integer value
    //
    //  PARAMS:
    //     - INPUT:
    //           - none
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - Int32: parsed integer value
    public Int32 IntValue
    {
      get
      {
        //17-02-2016 : SJA - Stop Exception when in paranoia exception mode
        Int32 _intval;
        return Int32.TryParse(this.Text, out _intval) ? _intval : default(Int32);
      }
    }

    // PURPOSE: Return the Decimal value
    //
    //  PARAMS:
    //     - INPUT:
    //           - none
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - Int32: parsed Decimal value
    public Decimal DecimalValue
    {
      get
      {
        return System.Decimal.Parse(this.Text);
      }
    }

    // PURPOSE: Check the format of entered value
    //
    //  PARAMS:
    //     - INPUT:
    //
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - TRUE: Format is correct.
    //     - FALSE: Format is incorrect.
    public Boolean ValidateFormat()
    {
      return Common.ValidateFormat.RawNumber(this.Text);
    }
  }

  public class CharacterTextBox : WSI.Cashier.Controls.uc_round_textbox, ICustomFormatValidable
  {
    public new enum ENUM_FORMAT_MODE
    {
      BaseMode,
      ExtendedMode
    }

    Boolean m_allow_space;
    Regex m_reg_exp;
    ENUM_FORMAT_MODE m_mode;

    public ENUM_FORMAT_MODE FormatMode
    {
      get { return m_mode; }
      set { m_mode = value; }
    }

    // PURPOSE: Initialize class members
    //
    //  PARAMS:
    //     - INPUT:
    //           - none
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - none
    public CharacterTextBox()
    {
      m_allow_space = false;
      m_mode = ENUM_FORMAT_MODE.BaseMode;
      ChangeRegex();
    }

    // PURPOSE: Update the regular expression according to space chatacter is allowed
    //
    //  PARAMS:
    //     - INPUT:
    //           - none
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - void
    private void ChangeRegex()
    {
      if (m_allow_space)
      {
        m_reg_exp = new Regex("^[^\\d]*$");
      }
      else
      {
        m_reg_exp = new Regex("^[^\\d\\s]*$");
      }
    }

    // PURPOSE: Check OnKeyPress event if valid character
    //
    //  PARAMS:
    //     - INPUT:
    //           - KeyPressEventArgs e
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - void
    protected override void OnKeyPress(object sender, KeyPressEventArgs e)
    {
      base.OnKeyPress(sender, e);

      switch (m_mode)
      {
        case ENUM_FORMAT_MODE.BaseMode:
          e.Handled = !Common.ValidateFormat.CheckCharacter(e.KeyChar, false);
          break;
        case ENUM_FORMAT_MODE.ExtendedMode:
          e.Handled = !Common.ValidateFormat.CheckCharacter(e.KeyChar, true);
          break;
        default:
          e.Handled = true;
          break;
      }
    }

    // PURPOSE: Property to modify m_allow_space member
    //
    //  PARAMS:
    //     - INPUT:
    //           - new value
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - Boolean: m_allow_space value
    public Boolean AllowSpace
    {
      set
      {
        this.m_allow_space = value;
        ChangeRegex();
      }
      get
      {
        return this.m_allow_space;
      }
    }

    // PURPOSE: Check the format of entered value
    //
    //  PARAMS:
    //     - INPUT:
    //
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - TRUE: Format is correct.
    //     - FALSE: Format is incorrect.
    //
    public Boolean ValidateFormat()
    {
      //Check mode
      switch (m_mode)
      {
        case ENUM_FORMAT_MODE.BaseMode:
          return Common.ValidateFormat.TextWithoutNumbers(this.Text);

        case ENUM_FORMAT_MODE.ExtendedMode:
          return Common.ValidateFormat.TextWithNumbers(this.Text);

        default:
          return false;
      }
    }
  }

  public class PhoneNumberTextBox : WSI.Cashier.Controls.uc_round_textbox, ICustomFormatValidable
  {
    Boolean m_allow_space;
    Regex m_reg_exp;

    // PURPOSE: Initialize class members
    //
    //  PARAMS:
    //     - INPUT:
    //           - none
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - none
    public PhoneNumberTextBox()
    {
      m_allow_space = false;
      ChangeRegex();
    }

    // PURPOSE: Update the regular expression according to space chatacter is allowed
    //
    //  PARAMS:
    //     - INPUT:
    //           - none
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - void
    private void ChangeRegex()
    {
      if (m_allow_space)
      {
        m_reg_exp = new Regex("^[\\+\\d\\s\\(\\)-]*$");
      }
      else
      {
        m_reg_exp = new Regex("^[\\+\\d\\(\\)-]*$");
      }
    }

    // PURPOSE: Check OnKeyPress event if valid character
    //
    //  PARAMS:
    //     - INPUT:
    //           - KeyPressEventArgs e
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - void
    protected override void OnKeyPress(object sender, KeyPressEventArgs e)
    {
      base.OnKeyPress(sender, e);

      e.Handled = !Common.ValidateFormat.CheckPhoneNumber(e.KeyChar);
    }

    // PURPOSE: Property to modify m_allow_space member
    //
    //  PARAMS:
    //     - INPUT:
    //           - new value
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - Boolean: m_allow_space value
    public Boolean AllowSpace
    {
      set
      {
        this.m_allow_space = value;
        ChangeRegex();
      }

      get
      {
        return this.m_allow_space;
      }
    }

    // PURPOSE: Check the format of entered value
    //
    //  PARAMS:
    //     - INPUT:
    //           - Boolean IsFilterSearch
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - TRUE: Format is correct.
    //     - FALSE: Format is incorrect.
    public Boolean ValidateFormat(Boolean IsFilterSearch)
    {
      return Common.ValidateFormat.PhoneNumber(this.Text, IsFilterSearch);
    }

    // PURPOSE: Check the format of entered value
    //
    //  PARAMS:
    //     - INPUT:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - TRUE: Format is correct.
    //     - FALSE: Format is incorrect.
    public Boolean ValidateFormat()
    {
      return ValidateFormat(false);
    }
  }

  public class EmailTextBox : WSI.Cashier.Controls.uc_round_textbox, ICustomFormatValidable
  {
    Regex m_reg_exp;

    // PURPOSE: Initialize class members
    //
    //  PARAMS:
    //     - INPUT:
    //           - none
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - none
    public EmailTextBox()
    {
      ChangeRegex();
    }

    // PURPOSE: Update the regular expression according to space chatacter is allowed
    //
    //  PARAMS:
    //     - INPUT:
    //           - none
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - void
    private void ChangeRegex()
    {
        m_reg_exp = new Regex(@"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
    }

    // PURPOSE: Check OnKeyPress event if valid character
    //
    //  PARAMS:
    //     - INPUT:
    //           - KeyPressEventArgs e
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - void
    protected override void OnKeyPress(object sender, KeyPressEventArgs e)
    {
      base.OnKeyPress(sender, e);

      e.Handled = !Common.ValidateFormat.CheckEmail(e.KeyChar);
    }

    // PURPOSE: Check the format of entered value
    //
    //  PARAMS:
    //     - INPUT:
    //           - Boolean IsFilterSearch
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - TRUE: Format is correct.
    //     - FALSE: Format is incorrect.
    public Boolean ValidateFormat(Boolean IsFilterSearch)
    {
      return Common.ValidateFormat.PhoneNumber(this.Text, IsFilterSearch);
    }

    // PURPOSE: Check the format of entered value
    //
    //  PARAMS:
    //     - INPUT:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - TRUE: Format is correct.
    //     - FALSE: Format is incorrect.
    public Boolean ValidateFormat()
    {
      return ValidateFormat(false);
    }
  }


  public class CustomDate
  {
    // PURPOSE : Validates Time format
    //
    //  PARAMS:
    //     - INPUT:
    //              - Hour
    //              - Minutes
    //              - Seconds
    //
    //     - OUTPUT:
    //
    // RETURNS:
    //         - TRUE:   If the format of all fields is correct
    //         - FALSE:  If there is a field that has incorrect format
    //
    public static Boolean ValidateTime(String Hour, String Minutes, String Seconds, out Int32 IndexWrong)
    {
      Int32 _hour = 0;
      Int32 _minutes = 0;
      Int32 _seconds = 0;

      IndexWrong = 0;

      // Check Hour format (00:00 .. 23:59)
      if (!Int32.TryParse(Hour, out _hour)
        || _hour < 0
        || _hour > 23)
      {
        IndexWrong = 1;

        return false;
      }

      // Check Minutes format (00 .. 59)
      if (!Int32.TryParse(Minutes, out _minutes)
       || _minutes < 0
       || _minutes > 59)
      {
        IndexWrong = 2;

        return false;
      }

      // Check Seconds format (00 .. 59)
      if (!Int32.TryParse(Seconds, out _seconds)
       || _seconds < 0
       || _seconds > 59)
      {
        IndexWrong = 3;

        return false;
      }

      return true;
    } // ValidateTime
  }
}
