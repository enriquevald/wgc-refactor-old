//------------------------------------------------------------------------------
// Copyright � 2010 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_browse_players.cs
// 
//   DESCRIPTION: Implements the following classes:
//                1. frm_browse_players
//
//        AUTHOR: APB
// 
// CREATION DATE: 27-OCT-2010
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 27-OCT-2010 APB     First release.
// 20-APR-2012 JCM     Changed the form returned value: Encrypted TrackData by Account Id
// 03-MAY-2012 JCM     Fixed Bug #265: Fixed focus to uc_Card. Modified focus to grid, by modified frm_yes_no (disconnected)
// 26-MAR-2013 RRB     Now use SQLCommand to do the search for accounts
// 15-DIC-2015 YNM,ADI PBI 7503: Visits/Reception Trinidad y Tobago: search screen     
// 25-NOV-2015 FOS     Fixed Bug 6940: Select dataGrid Item with double click
// 28-JAN-2016 YNM     Fixed Bug 8737 - Reception: unhandled exception selecting an acocunt
// 01-FEB-2016 RAB     Bug 8836:Excepci�n no controlada al buscar cuentas
//------------------------------------------------------------------------------
using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using WSI.Common;
using WSI.Cashier.Controls;

namespace WSI.Cashier
{
  public partial class frm_browse_players : frm_base
  {
    public const Int32 GRID_MAX_ROWS = 15;

    private const Int32 GRID_MAX_COLUMNS = 5;

    private const Int32 GRID_COLUMN_ACCT_ID = 0;
    private const Int32 GRID_COLUMN_ACCT_TRACKDATA = 1;
    private const Int32 GRID_COLUMN_ACCT_HOLDER_NAME = 2;
    private const Int32 GRID_COLUMN_ACCT_HOLDER_ID = 3;
    private const Int32 GRID_COLUMN_ACCT_HOLDER_DATE_OF_BIRTH = 4;

    #region Attributes

    private frm_yesno form_yes_no = new frm_yesno();
    private Int64 m_selected_account_id;
    private Boolean m_data_overflow;
    private CustomerReception m_customer_reception;

    #endregion

    #region Constructor

    //------------------------------------------------------------------------------
    // PURPOSE: Form constructor
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    public frm_browse_players()
    {
      InitializeComponent();

      InitializeControlResources();
      
      form_yes_no.Opacity = 0.6;
    }

    #endregion

    #region Private Methods

    private void SelectAccountId()
    {
      String _holder_name;
      string[] _split;


      _holder_name = this.dataGridView1.CurrentRow.Cells[GRID_COLUMN_ACCT_HOLDER_NAME].Value.ToString();
      _split = _holder_name.Split(new Char[] { ' ' });

      if (this.dataGridView1.RowCount > 0)
      {
        
          m_selected_account_id = (Int64)this.dataGridView1.CurrentRow.Cells[GRID_COLUMN_ACCT_ID].Value;

          if (m_selected_account_id != 0 && m_customer_reception != null)
          {
            //Load Customer Reception 
            m_customer_reception.AccountId = m_selected_account_id;
            m_customer_reception.CardCodBar = this.dataGridView1.CurrentRow.Cells[GRID_COLUMN_ACCT_TRACKDATA].Value.ToString();
            m_customer_reception.Document = this.dataGridView1.CurrentRow.Cells[GRID_COLUMN_ACCT_HOLDER_ID].Value.ToString();

          }        
        else if ((String)this.dataGridView1.CurrentRow.Cells[GRID_COLUMN_ACCT_TRACKDATA].Value == "")
        {
          m_selected_account_id = -1;
        }
      }
    }



    //------------------------------------------------------------------------------
    // PURPOSE :Set inital params in DataGrid.
    //
    //  PARAMS :
    //      - INPUT :
    //          - 
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void SetFormatDataGrid()
    {

      Int32 _idx_row;
      String _internal_card_id;
      string _encrypted_trackdata;
      int _card_type;
      bool _rc;
      String _date_format;

      dataGridView1.MultiSelect = false;
      dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;

      _date_format = System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern;

      // Set row height for bigger font sizes
      dataGridView1.ColumnHeadersHeight = 30;
      dataGridView1.RowTemplate.Height = 30;
      dataGridView1.RowTemplate.DividerHeight = 0;

      // Columns
      
      dataGridView1.Columns[GRID_COLUMN_ACCT_ID].Width = 90;
      dataGridView1.Columns[GRID_COLUMN_ACCT_ID].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

      dataGridView1.Columns[GRID_COLUMN_ACCT_TRACKDATA].Width = 0;
      dataGridView1.Columns[GRID_COLUMN_ACCT_TRACKDATA].Visible = false;

      dataGridView1.Columns[GRID_COLUMN_ACCT_HOLDER_NAME].Width = 310;
      dataGridView1.Columns[GRID_COLUMN_ACCT_HOLDER_NAME].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

      dataGridView1.Columns[GRID_COLUMN_ACCT_HOLDER_ID].Width = 240;
      dataGridView1.Columns[GRID_COLUMN_ACCT_HOLDER_ID].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

      dataGridView1.Columns[GRID_COLUMN_ACCT_HOLDER_DATE_OF_BIRTH].Width = 130;
      dataGridView1.Columns[GRID_COLUMN_ACCT_HOLDER_DATE_OF_BIRTH].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
      dataGridView1.Columns[GRID_COLUMN_ACCT_HOLDER_DATE_OF_BIRTH].DefaultCellStyle.Format = _date_format;

      // Datagrid Headers
      dataGridView1.Columns[GRID_COLUMN_ACCT_ID].HeaderCell.Value = Resource.String("STR_FRM_PLAYER_SEARCH_RESULTS_002");
      dataGridView1.Columns[GRID_COLUMN_ACCT_TRACKDATA].HeaderCell.Value = "";    // Not visible
      dataGridView1.Columns[GRID_COLUMN_ACCT_HOLDER_NAME].HeaderCell.Value = Resource.String("STR_FRM_PLAYER_SEARCH_RESULTS_003");
      dataGridView1.Columns[GRID_COLUMN_ACCT_HOLDER_ID].HeaderCell.Value = Resource.String("STR_FRM_PLAYER_SEARCH_RESULTS_004");
      dataGridView1.Columns[GRID_COLUMN_ACCT_HOLDER_DATE_OF_BIRTH].HeaderCell.Value = Resource.String("STR_FRM_PLAYER_SEARCH_RESULTS_005");

      for (_idx_row = 0; _idx_row < dataGridView1.Rows.Count; _idx_row++)
      {
        if ((String)dataGridView1.Rows[_idx_row].Cells[GRID_COLUMN_ACCT_TRACKDATA].Value != "")
        {
          _internal_card_id = (String)dataGridView1.Rows[_idx_row].Cells[GRID_COLUMN_ACCT_TRACKDATA].Value;
          _card_type = 0;
          _rc = CardNumber.TrackDataToExternal(out _encrypted_trackdata, _internal_card_id, _card_type);
          if (_rc)
          {
            dataGridView1.Rows[_idx_row].Cells[GRID_COLUMN_ACCT_TRACKDATA].Value = _encrypted_trackdata;
          }          
        }
        //RAB 01-FEB-2016
        else
        {
          dataGridView1.Rows.RemoveAt(_idx_row);
        }
      }

      // Avoid selection from an empty grid
      if (dataGridView1.RowCount <= 0)
      {
        btn_select.Enabled = false;
      }
      else
      {
        btn_select.Enabled = true;
      }

      form_yes_no.Show();

      // Set focus to the DataGridView just after the form_yes_no.Show().
      dataGridView1.Focus();
      this.ShowDialog();

      form_yes_no.Hide();
    }

    #endregion

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Initialize control resources (NLS strings, images, etc).
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //

    public void InitializeControlResources()
    {
      // - NLS Strings:
      //   - Title:
      this.FormTitle = Resource.String("STR_FRM_PLAYER_SEARCH_RESULTS_001");

      //   - Labels

      //   - Buttons
      btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");
      btn_select.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_SELECT");
      // - Images:

    } // InitializeControlResources

    //------------------------------------------------------------------------------
    // PURPOSE : Init and Show the form.
    //
    //  PARAMS :
    //      - INPUT :
    //          - DataSet
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //

    public CustomerReception Show(DataSet DsAccounts)
    {     
      m_customer_reception = new CustomerReception();
      m_data_overflow = false;

      try
      {
        dataGridView1.DataSource = DsAccounts.Tables["Accounts"];  
      }
      catch
      {
        return null;
      }

      this.SetFormatDataGrid();  

      return m_customer_reception;
    } // Show

    public Int64 Show(DataTable DtAccounts)
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_browse_players(DsAccounts)", Log.Type.Message);
      m_customer_reception = new CustomerReception();
      m_data_overflow = false;

      try
      {
        dataGridView1.DataSource = DtAccounts;
      }
      catch
      {
        return -1;
      }

      this.SetFormatDataGrid();

       return 0;
    } // Show

    public Int64 Show(SqlCommand SqlCommand)
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_browse_players(SqlCommand)", Log.Type.Message);
      DataSet _ds;

      m_data_overflow = false;
      _ds = new DataSet();

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          SqlCommand.Connection = _db_trx.SqlTransaction.Connection;
          SqlCommand.Transaction = _db_trx.SqlTransaction;
          using (SqlDataAdapter _da = new SqlDataAdapter(SqlCommand))
          {
            _da.Fill(_ds);
            if (_ds.Tables[0].Rows.Count > GRID_MAX_ROWS)
            {
              m_data_overflow = true;
              _ds.Tables[0].Rows[GRID_MAX_ROWS].Delete();
            }
            dataGridView1.DataSource = _ds.Tables[0];        
          }
        }
      }
      catch
      {
        return -1;
      }

      this.SetFormatDataGrid();

      return m_selected_account_id;
    } // Show

    //------------------------------------------------------------------------------
    // PURPOSE : Dispose yes_no form.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //

    new public void Dispose()
    {
      form_yes_no.Dispose();
      form_yes_no = null;

      base.Dispose();
    } // Dispose

    #endregion Public methods

    #region DataGridEvents

    //------------------------------------------------------------------------------
    // PURPOSE : Handle the double click on a datagrid row.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //      This method is used to allow record selection by double-clicking on the grid
    //

    private void dataGridView1_CellMouseDoubleClick(object sender, MouseEventArgs e)
    {
      if (this.dataGridView1.CurrentRow != null)
      {
        SelectAccountId();

        Misc.WriteLog("[FORM CLOSE] frm_browse_players (cellmousedoubleclick)", Log.Type.Message);
        this.Close();
      }
    } // dataGridView1_CellMouseDoubleClick

    #endregion DataGridEvents

    #region Buttons

    //------------------------------------------------------------------------------
    // PURPOSE : Handle the click on the Cancel button (exit form).
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //

    private void btn_cancel_Click(object sender, EventArgs e)
    {
      m_selected_account_id = -1;
      m_customer_reception = null;

      Misc.WriteLog("[FORM CLOSE] frm_browse_players (cancel)", Log.Type.Message);
      this.Close();
    } // btn_cancel_Click

    //------------------------------------------------------------------------------
    // PURPOSE : Handle the click on the Select button (exit form and pass the
    //           selected record identifier to the caller).
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //

    private void btn_select_Click(object sender, EventArgs e)
    {
      SelectAccountId();

      Misc.WriteLog("[FORM CLOSE] frm_browse_players (select)", Log.Type.Message);
      this.Close();
    } // btn_select_Click

    //------------------------------------------------------------------------------
    // PURPOSE: Display message if too many records were found at loading time.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //

    private void frm_browse_players_Shown(object sender, EventArgs e)
    {
      // Show message is too many results were found
      if (m_data_overflow)
      {
        // Show pop-up msg
        frm_message.Show(Resource.String("STR_FRM_PLAYER_SEARCH_RESULTS_006", GRID_MAX_ROWS),
                         Resource.String("STR_APP_GEN_MSG_WARNING"),
                         MessageBoxButtons.OK,
                         MessageBoxIcon.Warning,
                         this);
      }
    }

   // frm_browse_players_Shown

    public override void pb_exit_Click(object sender, EventArgs e)
    {
      btn_cancel_Click(sender, e);
    }

    #endregion Buttons

  }
}