using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using WSI.Common;

namespace WSI.Cashier
{
  public partial class uc_status_item:UserControl
  {
    private int show_text = 1;
    private int left = 1;
    private Boolean icon_visible = true;
    
    public Boolean IconVisible
    {
      get { return icon_visible; }

      set
      {
        icon_visible = value;

        this.pic_status.Visible = value;
       
        if (value == false)
        {
          this.pic_status.Width = 0;
        }
        else
        {
          this.pic_status.Width = 24;
        }

        this.UpdateSize();
      }
    }

    public override String Text
    {
      get 
      { 
        return lbl_status.Text; 
      }
      set 
      {
        lbl_status.Text = value;
        UpdateSize ();
      }
    }

    public Image Icon
    {
      set { pic_status.Image = value; }
    }

    public uc_status_item ()
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[UC LOAD] uc_status_item", Log.Type.Message);

      InitializeComponent ();
      UpdateSize ();
    }

    private void UpdateSize ()
    {
      System.Drawing.Graphics G;
      SizeF size_f;
      int n;

      G = Graphics.FromHwnd (this.lbl_status.Handle);
      size_f = G.MeasureString (lbl_status.Text + "OOO", lbl_status.Font);
      n = (int)(size_f.Width);
      n = n + (10 - n % 10) % 10;
      this.lbl_status.Width = n;
      this.Width = this.pic_status.Width + show_text * n;

      if (!icon_visible)
      {
        this.lbl_status.TextAlign = ContentAlignment.MiddleCenter;
        this.Width -= this.pic_status.Width;
      }
      else
      {
        this.lbl_status.TextAlign = ContentAlignment.MiddleLeft;
      }

      this.pic_status.Dock = DockStyle.None;
      this.lbl_status.Dock = DockStyle.None;

      if ( left != 0 )
      {
        this.pic_status.Location = new Point (0, this.ClientRectangle.Height / 2 - this.pic_status.Height / 2);

        if (icon_visible)
        {
          this.lbl_status.Location = new Point(this.pic_status.Width, this.ClientRectangle.Height / 2 - this.lbl_status.Height / 2);
        }
        else
        {
          this.lbl_status.Location = new Point(0, this.ClientRectangle.Height / 2 - this.lbl_status.Height / 2);
        }
      }
      else
      {
        this.lbl_status.Location = new Point (0, this.ClientRectangle.Height / 2 - this.lbl_status.Height / 2);
        this.pic_status.Location = new Point (this.ClientRectangle.Width - this.pic_status.Width, this.ClientRectangle.Height / 2 - this.pic_status.Height / 2);
      }
      this.pic_status.BringToFront ();
    }

    private void pic_status_Click (object sender, EventArgs e)
    {
      // left = ( left + 1 ) % 2;
      UpdateSize ();
    }

    
  }
}
