using WSI.Cashier.Controls;
namespace WSI.Cashier
{
  partial class frm_account_points
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }
    
    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.gp_gifts_cancellation = new WSI.Cashier.Controls.uc_round_panel();
      this.dgv_available_instances = new WSI.Cashier.Controls.uc_DataGridView();
      this.btn_cancel = new WSI.Cashier.Controls.uc_round_button();
      this.gb_gifts = new WSI.Cashier.Controls.uc_round_panel();
      this.lbl_msg_blink = new WSI.Cashier.Controls.uc_label();
      this.dgv_next_gifts = new WSI.Cashier.Controls.uc_DataGridView();
      this.lbl_points_balance = new WSI.Cashier.Controls.uc_label();
      this.lbl_points_balance_title = new WSI.Cashier.Controls.uc_label();
      this.btn_gift_request = new WSI.Cashier.Controls.uc_round_button();
      this.btn_print = new WSI.Cashier.Controls.uc_round_button();
      this.dgv_available_gifts = new WSI.Cashier.Controls.uc_DataGridView();
      this.btn_close = new WSI.Cashier.Controls.uc_round_button();
      this.pnl_data.SuspendLayout();
      this.gp_gifts_cancellation.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dgv_available_instances)).BeginInit();
      this.gb_gifts.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dgv_next_gifts)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.dgv_available_gifts)).BeginInit();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.Controls.Add(this.gp_gifts_cancellation);
      this.pnl_data.Controls.Add(this.gb_gifts);
      this.pnl_data.Controls.Add(this.btn_close);
      this.pnl_data.Size = new System.Drawing.Size(869, 624);
      this.pnl_data.TabIndex = 0;
      // 
      // gp_gifts_cancellation
      // 
      this.gp_gifts_cancellation.BackColor = System.Drawing.Color.Transparent;
      this.gp_gifts_cancellation.Controls.Add(this.dgv_available_instances);
      this.gp_gifts_cancellation.Controls.Add(this.btn_cancel);
      this.gp_gifts_cancellation.CornerRadius = 10;
      this.gp_gifts_cancellation.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gp_gifts_cancellation.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gp_gifts_cancellation.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gp_gifts_cancellation.HeaderHeight = 35;
      this.gp_gifts_cancellation.HeaderSubText = null;
      this.gp_gifts_cancellation.HeaderText = null;
      this.gp_gifts_cancellation.Location = new System.Drawing.Point(12, 384);
      this.gp_gifts_cancellation.Name = "gp_gifts_cancellation";
      this.gp_gifts_cancellation.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gp_gifts_cancellation.Size = new System.Drawing.Size(843, 154);
      this.gp_gifts_cancellation.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.gp_gifts_cancellation.TabIndex = 1;
      this.gp_gifts_cancellation.Text = "xGifts Cancellation";
      // 
      // dgv_available_instances
      // 
      this.dgv_available_instances.AllowUserToAddRows = false;
      this.dgv_available_instances.AllowUserToDeleteRows = false;
      this.dgv_available_instances.AllowUserToResizeColumns = false;
      this.dgv_available_instances.AllowUserToResizeRows = false;
      this.dgv_available_instances.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.dgv_available_instances.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
      this.dgv_available_instances.ColumnHeaderHeight = 35;
      this.dgv_available_instances.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
      this.dgv_available_instances.ColumnHeadersHeight = 35;
      this.dgv_available_instances.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
      this.dgv_available_instances.CornerRadius = 10;
      this.dgv_available_instances.DefaultCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.dgv_available_instances.DefaultCellForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.dgv_available_instances.EnableHeadersVisualStyles = false;
      this.dgv_available_instances.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.dgv_available_instances.GridColor = System.Drawing.Color.White;
      this.dgv_available_instances.HeaderBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
      this.dgv_available_instances.HeaderDefaultCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.dgv_available_instances.HeaderDefaultCellForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.dgv_available_instances.Location = new System.Drawing.Point(0, 35);
      this.dgv_available_instances.MultiSelect = false;
      this.dgv_available_instances.Name = "dgv_available_instances";
      this.dgv_available_instances.ReadOnly = true;
      this.dgv_available_instances.RowHeadersVisible = false;
      this.dgv_available_instances.RowHeadersWidth = 20;
      this.dgv_available_instances.RowTemplate.Height = 35;
      this.dgv_available_instances.RowTemplateHeight = 35;
      this.dgv_available_instances.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
      this.dgv_available_instances.Size = new System.Drawing.Size(513, 120);
      this.dgv_available_instances.Style = WSI.Cashier.Controls.uc_DataGridView.DataGridViewStyle.WITH_BOTTOM_ROUND_BORDERS;
      this.dgv_available_instances.TabIndex = 0;
      // 
      // btn_cancel
      // 
      this.btn_cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_cancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_cancel.FlatAppearance.BorderSize = 0;
      this.btn_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_cancel.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_cancel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_cancel.Image = null;
      this.btn_cancel.Location = new System.Drawing.Point(535, 73);
      this.btn_cancel.Name = "btn_cancel";
      this.btn_cancel.Size = new System.Drawing.Size(132, 60);
      this.btn_cancel.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_cancel.TabIndex = 1;
      this.btn_cancel.Text = "XCANCEL";
      this.btn_cancel.UseVisualStyleBackColor = false;
      // 
      // gb_gifts
      // 
      this.gb_gifts.BackColor = System.Drawing.Color.Transparent;
      this.gb_gifts.Controls.Add(this.lbl_msg_blink);
      this.gb_gifts.Controls.Add(this.dgv_next_gifts);
      this.gb_gifts.Controls.Add(this.lbl_points_balance);
      this.gb_gifts.Controls.Add(this.lbl_points_balance_title);
      this.gb_gifts.Controls.Add(this.btn_gift_request);
      this.gb_gifts.Controls.Add(this.btn_print);
      this.gb_gifts.Controls.Add(this.dgv_available_gifts);
      this.gb_gifts.CornerRadius = 10;
      this.gb_gifts.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_gifts.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gb_gifts.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_gifts.HeaderHeight = 35;
      this.gb_gifts.HeaderSubText = null;
      this.gb_gifts.HeaderText = null;
      this.gb_gifts.Location = new System.Drawing.Point(12, 12);
      this.gb_gifts.Name = "gb_gifts";
      this.gb_gifts.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_gifts.Size = new System.Drawing.Size(843, 360);
      this.gb_gifts.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.gb_gifts.TabIndex = 0;
      this.gb_gifts.Text = "xGifts";
      // 
      // lbl_msg_blink
      // 
      this.lbl_msg_blink.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_msg_blink.BackColor = System.Drawing.Color.Transparent;
      this.lbl_msg_blink.Font = new System.Drawing.Font("Montserrat", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_msg_blink.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_msg_blink.Location = new System.Drawing.Point(551, 75);
      this.lbl_msg_blink.Name = "lbl_msg_blink";
      this.lbl_msg_blink.Size = new System.Drawing.Size(279, 23);
      this.lbl_msg_blink.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLUE;
      this.lbl_msg_blink.TabIndex = 97;
      this.lbl_msg_blink.Text = "xMESSAGE LINE";
      this.lbl_msg_blink.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.lbl_msg_blink.Visible = false;
      // 
      // dgv_next_gifts
      // 
      this.dgv_next_gifts.AllowUserToAddRows = false;
      this.dgv_next_gifts.AllowUserToDeleteRows = false;
      this.dgv_next_gifts.AllowUserToResizeColumns = false;
      this.dgv_next_gifts.AllowUserToResizeRows = false;
      this.dgv_next_gifts.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.dgv_next_gifts.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
      this.dgv_next_gifts.ColumnHeaderHeight = 35;
      this.dgv_next_gifts.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
      this.dgv_next_gifts.ColumnHeadersHeight = 35;
      this.dgv_next_gifts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
      this.dgv_next_gifts.CornerRadius = 10;
      this.dgv_next_gifts.DefaultCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.dgv_next_gifts.DefaultCellForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.dgv_next_gifts.Enabled = false;
      this.dgv_next_gifts.EnableHeadersVisualStyles = false;
      this.dgv_next_gifts.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.dgv_next_gifts.GridColor = System.Drawing.Color.White;
      this.dgv_next_gifts.HeaderBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
      this.dgv_next_gifts.HeaderDefaultCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.dgv_next_gifts.HeaderDefaultCellForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.dgv_next_gifts.Location = new System.Drawing.Point(0, 241);
      this.dgv_next_gifts.Name = "dgv_next_gifts";
      this.dgv_next_gifts.ReadOnly = true;
      this.dgv_next_gifts.RowHeadersVisible = false;
      this.dgv_next_gifts.RowHeadersWidth = 20;
      this.dgv_next_gifts.RowTemplate.Height = 35;
      this.dgv_next_gifts.RowTemplateHeight = 35;
      this.dgv_next_gifts.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
      this.dgv_next_gifts.Size = new System.Drawing.Size(513, 120);
      this.dgv_next_gifts.Style = WSI.Cashier.Controls.uc_DataGridView.DataGridViewStyle.WITH_ALL_ROUND_CORNERS;
      this.dgv_next_gifts.TabIndex = 1;
      // 
      // lbl_points_balance
      // 
      this.lbl_points_balance.BackColor = System.Drawing.Color.Transparent;
      this.lbl_points_balance.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_points_balance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_points_balance.Location = new System.Drawing.Point(700, 58);
      this.lbl_points_balance.Name = "lbl_points_balance";
      this.lbl_points_balance.Size = new System.Drawing.Size(130, 18);
      this.lbl_points_balance.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_points_balance.TabIndex = 95;
      this.lbl_points_balance.Text = "x123456";
      this.lbl_points_balance.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_points_balance_title
      // 
      this.lbl_points_balance_title.BackColor = System.Drawing.Color.Transparent;
      this.lbl_points_balance_title.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_points_balance_title.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_points_balance_title.Location = new System.Drawing.Point(542, 58);
      this.lbl_points_balance_title.Name = "lbl_points_balance_title";
      this.lbl_points_balance_title.Size = new System.Drawing.Size(152, 16);
      this.lbl_points_balance_title.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_points_balance_title.TabIndex = 94;
      this.lbl_points_balance_title.Text = "xAvailable Points:";
      this.lbl_points_balance_title.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // btn_gift_request
      // 
      this.btn_gift_request.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_gift_request.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_gift_request.FlatAppearance.BorderSize = 0;
      this.btn_gift_request.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_gift_request.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_gift_request.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_gift_request.Image = null;
      this.btn_gift_request.Location = new System.Drawing.Point(535, 139);
      this.btn_gift_request.Name = "btn_gift_request";
      this.btn_gift_request.Size = new System.Drawing.Size(132, 60);
      this.btn_gift_request.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_gift_request.TabIndex = 2;
      this.btn_gift_request.Text = "XGIFT REQUEST";
      this.btn_gift_request.UseVisualStyleBackColor = false;
      // 
      // btn_print
      // 
      this.btn_print.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_print.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_print.FlatAppearance.BorderSize = 0;
      this.btn_print.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_print.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_print.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_print.Image = null;
      this.btn_print.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
      this.btn_print.Location = new System.Drawing.Point(689, 139);
      this.btn_print.Margin = new System.Windows.Forms.Padding(0);
      this.btn_print.Name = "btn_print";
      this.btn_print.Size = new System.Drawing.Size(132, 60);
      this.btn_print.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_print.TabIndex = 3;
      this.btn_print.Text = "XPRINT";
      this.btn_print.UseVisualStyleBackColor = false;
      // 
      // dgv_available_gifts
      // 
      this.dgv_available_gifts.AllowUserToAddRows = false;
      this.dgv_available_gifts.AllowUserToDeleteRows = false;
      this.dgv_available_gifts.AllowUserToResizeColumns = false;
      this.dgv_available_gifts.AllowUserToResizeRows = false;
      this.dgv_available_gifts.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.dgv_available_gifts.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
      this.dgv_available_gifts.ColumnHeaderHeight = 35;
      this.dgv_available_gifts.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
      this.dgv_available_gifts.ColumnHeadersHeight = 35;
      this.dgv_available_gifts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
      this.dgv_available_gifts.CornerRadius = 10;
      this.dgv_available_gifts.DefaultCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.dgv_available_gifts.DefaultCellForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.dgv_available_gifts.EnableHeadersVisualStyles = false;
      this.dgv_available_gifts.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.dgv_available_gifts.GridColor = System.Drawing.Color.White;
      this.dgv_available_gifts.HeaderBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
      this.dgv_available_gifts.HeaderDefaultCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.dgv_available_gifts.HeaderDefaultCellForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.dgv_available_gifts.Location = new System.Drawing.Point(0, 35);
      this.dgv_available_gifts.MultiSelect = false;
      this.dgv_available_gifts.Name = "dgv_available_gifts";
      this.dgv_available_gifts.ReadOnly = true;
      this.dgv_available_gifts.RowHeadersVisible = false;
      this.dgv_available_gifts.RowHeadersWidth = 20;
      this.dgv_available_gifts.RowTemplate.Height = 35;
      this.dgv_available_gifts.RowTemplateHeight = 35;
      this.dgv_available_gifts.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
      this.dgv_available_gifts.Size = new System.Drawing.Size(513, 180);
      this.dgv_available_gifts.Style = WSI.Cashier.Controls.uc_DataGridView.DataGridViewStyle.WITH_BOTTOM_ROUND_BORDERS;
      this.dgv_available_gifts.TabIndex = 0;
      // 
      // btn_close
      // 
      this.btn_close.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_close.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_close.FlatAppearance.BorderSize = 0;
      this.btn_close.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_close.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_close.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_close.Image = null;
      this.btn_close.Location = new System.Drawing.Point(723, 550);
      this.btn_close.Name = "btn_close";
      this.btn_close.Size = new System.Drawing.Size(132, 60);
      this.btn_close.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_close.TabIndex = 2;
      this.btn_close.Text = "XCLOSE";
      this.btn_close.UseVisualStyleBackColor = false;
      // 
      // frm_account_points
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(869, 679);
      this.Name = "frm_account_points";
      this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
      this.Style = WSI.Cashier.Controls.frm_base.BaseFormStyle.PRINCIPAL;
      this.Text = "Account Points";
      this.Shown += new System.EventHandler(this.frm_account_points_Shown);
      this.pnl_data.ResumeLayout(false);
      this.gp_gifts_cancellation.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.dgv_available_instances)).EndInit();
      this.gb_gifts.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.dgv_next_gifts)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.dgv_available_gifts)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private uc_round_panel gp_gifts_cancellation;
    private uc_DataGridView dgv_available_instances;
    private uc_round_button btn_cancel;
    private uc_round_panel gb_gifts;
    private uc_label lbl_msg_blink;
    private uc_DataGridView dgv_next_gifts;
    private uc_label lbl_points_balance;
    private uc_label lbl_points_balance_title;
    private uc_round_button btn_gift_request;
    private uc_round_button btn_print;
    private uc_DataGridView dgv_available_gifts;
    private uc_round_button btn_close;

  }
}