﻿//------------------------------------------------------------------------------
// Copyright © 2007-2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: EntryManager.cs
// 
//   DESCRIPTION: Implementation of entry manager class. Context of the Reception mode strategy
//                
//                
//                
//                
//                
//                
// 
//        AUTHOR: Ariel Vazquez Zerpa.
// 
// CREATION DATE: 01-FEB-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 01-FEB-2016 AVZ    First release. Product Backlog Item 8673:Visitas / Recepción: Configuración de Modo de Recepción
// 10-OCT-2017 DPC    [WIGOS-5525]: Reception - Register customer output - Cashier button - Exit logic
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using WSI.Cashier.MVC.Model.PlayerEditDetails;
using WSI.Common;
using WSI.Common.Entrances.Entities;
using WSI.Common.Entrances.Enums;

namespace WSI.Cashier.Strategy.ReceptionMode
{
  public class EntryManager
  {
    private ReceptionModeStrategy m_strategy;
    public String FlyerCode { get; set; }

    /// <summary>
    /// Set reception mode strategy
    /// </summary>
    /// <param name="ReceptionMode"></param>
    public void SetStrategy(ENUM_RECEPTION_MODE ReceptionMode)
    {
      ReceptionModeStrategyFactory _strategy_factory = new ReceptionModeStrategyFactory();
      m_strategy = _strategy_factory.CreateStrategy(ReceptionMode);
    }

    /// <summary>
    /// Perform entry with the selected strategy
    /// </summary>
    /// <param name="Model"></param>
    /// <param name="AccountsReception"></param>
    /// <param name="CardData"></param>
    /// <returns></returns>
    public bool? PerformEntry(PlayerEditDetailsReceptionModel Model, AccountsReception AccountsReception, CardData CardData)
    {
      return this.m_strategy.Entry(Model, AccountsReception, CardData);
    }

    /// <summary>
    /// Perform exit with the selected strategy
    /// </summary>
    /// <param name="Model"></param>
    /// <param name="AccountsReception"></param>
    /// <param name="VisitsUpdated"></param>
    /// <returns></returns>
    public bool? PerformExit(PlayerEditDetailsReceptionModel Model, AccountsReception AccountsReception, out Int32 VisitsUpdated)
    {
      return this.m_strategy.Exit(Model, AccountsReception, out VisitsUpdated);
    } // PerformExit
  }
}
