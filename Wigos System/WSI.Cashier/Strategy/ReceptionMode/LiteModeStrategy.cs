﻿//------------------------------------------------------------------------------
// Copyright © 2007-2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: LiteModeStrategy.cs
// 
//   DESCRIPTION: Implementation of lite mode strategy class
//                
//                
//                
//                
//                
//                
// 
//        AUTHOR: Ariel Vazquez Zerpa.
// 
// CREATION DATE: 01-FEB-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 01-FEB-2016 AVZ    First release. Product Backlog Item 8673:Visitas / Recepción: Configuración de Modo de Recepción
// 10-OCT-2017 DPC    [WIGOS-5525]: Reception - Register customer output - Cashier button - Exit logic
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using WSI.Cashier.MVC.Model.PlayerEditDetails;
using WSI.Common;
using WSI.Common.Entrances;
using WSI.Common.Entrances.Entities;

namespace WSI.Cashier.Strategy.ReceptionMode
{
  public class LiteModeStrategy : ReceptionModeStrategy
  {
    public override bool? Entry(PlayerEditDetailsReceptionModel Model, AccountsReception AccountsReception, CardData CardData)
    {
      TicketData _ticket_data = base.GetEmptyTicketData();

      AccountsReception.AceptedAgreement = false; 
      return base.RegisterEntry(AccountsReception, _ticket_data, CardData, false, false);
    }

    public override bool? Exit(PlayerEditDetailsReceptionModel Model, AccountsReception AccountsReception, out Int32 VisitsUpdated)
    {
      TicketData _ticket_data = base.GetEmptyTicketData();

      AccountsReception.AceptedAgreement = false;
      return base.RegisterExit(AccountsReception, out VisitsUpdated);
    }
  }
}
