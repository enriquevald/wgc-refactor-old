﻿//------------------------------------------------------------------------------
// Copyright © 2007-2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: ReceptionModeStrategy.cs
// 
//   DESCRIPTION: Implementation of reception mode abstract class
//                
//                
//                
//                
//                
//                
// 
//        AUTHOR: Ariel Vazquez Zerpa.
// 
// CREATION DATE: 01-FEB-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 01-FEB-2016 AVZ    First release. Product Backlog Item 8673:Visitas / Recepción: Configuración de Modo de Recepción
// 08-FEB-2016 SJA    Changed type of Entry to bool? to be able to have tristate, null is no action performed where as
//                      true is entry on and fals eis entry error
// 22-MAR-2106 YNM    Bug 10759: Reception error paying with currency
// 04-ABR-2016 YNM    Bug 11282: Reception error paying with currency
// 10-MAY-2016 JBP    Product Backlog Item 12927:Visitas / Recepción: Caducidad de las entradas variable
// 30-SEP-2016 ETP    Bug 18003: Cashier: los vouchers de recarga no reflejan los cambios solicitados por Televisa
// 24-OCT-2016 ETP    Fixed Bug 19314 Pin Pad recharge refactoring
// 26-OCT-2016 ETP    Fixed Bug 19314 Pin Pad recharge Wigos Rollback + Pinpad + Wigos Commit
// 08-NOV-2016 ETP    Fixed Bug Bug 20035: Exception out of memory when pinpad is used.
// 09-MAY-2017 DPC    WIGOS-1219: Junkets - Voucher
// 21-MAY-2017 ETP    Fixed Bug WIGOS-2997: Added voucher data.
// 10-OCT-2017 DPC    [WIGOS-5525]: Reception - Register customer output - Cashier button - Exit logic
// 19-OCT-2017 JML    Fixed Bug 30321:WIGOS-5924 [Ticket #9450] Fórmula datos tarjeta bancaria (entregado-faltante) - Resumen de sesión de caja
// 13-NOV-2017 DHA    Bug 30775:WIGOS-6110 [Ticket #9672] MTY I - Plaza Real - PinPad con error y autorizadas
//------------------------------------------------------------------------------
using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using WSI.Cashier.JunketsCashier;
using WSI.Cashier.MVC.Model.PlayerEditDetails;
using WSI.Common;
using WSI.Common.Entrances;
using WSI.Common.Entrances.Entities;
using WSI.Common.Entrances.Entities.Persistence;
using WSI.Common.Junkets;
using WSI.Common.PinPad;

namespace WSI.Cashier.Strategy.ReceptionMode
{
  public abstract class ReceptionModeStrategy
  {

    /// <summary>
    /// Register customer entry to the casino
    /// </summary>
    /// <param name="AccountsReception"></param>
    /// <param name="CardData"></param>
    /// <returns></returns>
    public abstract bool? Entry(PlayerEditDetailsReceptionModel Model, AccountsReception AccountsReception, CardData CardData);
    //SJA - Changed to bool? to allow detection of cancel

    public abstract bool? Exit(PlayerEditDetailsReceptionModel Model, AccountsReception AccountsReception, out Int32 VisitsUpdated);

    /// <summary>
    /// Operations to save and generate the entrance
    /// </summary>
    /// <param name="_accounts_reception"></param>
    /// <param name="_ticket_data"></param>
    /// <returns></returns>
    protected Boolean RegisterEntry(AccountsReception AccountsReception, TicketData TicketData, CardData CardData, bool GenerateCashierMovement, bool GenerateVoucher)
    {
      Int64 _operation_id = 0;
      CashierMovementsTable _cashier_movements_table;
      CashierSessionInfo _csi;
      ArrayList _voucher_list;
      ArrayList _voucher_list_tpv;
      Boolean _return;

      // ETP: PinPad vars:
      Boolean _return_error;
      Boolean _is_pinpad_recharge;
      frm_tpv _form_tpv;
      PinPadInput _pinpad_input;
      PinPadOutput _pinpad_output;

      _pinpad_output = new PinPadOutput();

      _voucher_list_tpv = new ArrayList();
      _return = true;

      try
      {
        _csi = Cashier.CashierSessionInfo();
        _cashier_movements_table = new CashierMovementsTable(_csi);

        _is_pinpad_recharge = Misc.IsPinPadEnabled() && Common.Cashier.ReadPinPadEnabled() && TicketData.ExchangeResult != null && TicketData.ExchangeResult.InType == CurrencyExchangeType.CARD && TicketData.ExchangeResult.InAmount != 0;

        if (_is_pinpad_recharge)
        {
          if (!InternetConnection.GetServiceInternetConnectionStatus("WCP"))
          {
            return false;
          }

          using (DB_TRX _trx = new DB_TRX())
          {
            if (!Common_RegisterEntry(AccountsReception, TicketData, CardData, GenerateCashierMovement, GenerateVoucher, _trx.SqlTransaction, out _voucher_list)) // ETP 27/10/2016 Check if operation can be done.
            {
              _trx.Rollback();
              return false;
            }
            _trx.Rollback();
          }
          _pinpad_input = new PinPadInput();
          _pinpad_input.Amount = TicketData.ExchangeResult;
          _pinpad_input.Card = CardData;
          _pinpad_input.TerminalName = _csi.TerminalName;

          _form_tpv = new frm_tpv(_pinpad_input);
          if (!_form_tpv.Show(out _pinpad_output)) // 20/10/2016 ETP: Pin Pad payement
          {
            _form_tpv.Dispose();
            return false;
          }
          // 20/10/2016 ETP: Transaction is Pending of udpating wigos data.
          _form_tpv.Dispose();
        }

        _return_error = false;


        using (DB_TRX _trx = new DB_TRX())
        {

          if (Common_RegisterEntry(AccountsReception, TicketData, CardData, GenerateCashierMovement, GenerateVoucher, _trx.SqlTransaction, out _voucher_list))
          {
            if (_is_pinpad_recharge && !_return_error)
            {
              PinPadTransactions _transaction;

              _transaction = _pinpad_output.PinPadTransaction;

              _transaction.OperationId = _operation_id;
              _transaction.StatusCode = PinPadTransactions.STATUS.APPROVED;  //  20/10/2016 ETP: PinPad is payed OK.

              if (!_transaction.DB_Insert(_trx.SqlTransaction) && !_return_error)
              {
                _return_error = true;
              }

              _transaction.UpdateCashierSessionHasPinPadOperation(_csi.CashierSessionId, _trx.SqlTransaction);

              if (!PinPadTransactions.DeleteFromUndoPinPad(_transaction.ControlNumber, _trx.SqlTransaction) && !_return_error)
              {
                _return_error = true;
              }

              if (!_pinpad_output.PinPadTransaction.CreateVoucher(TicketData.ExchangeResult, _pinpad_output, _trx.SqlTransaction, out _voucher_list_tpv))
              {
                _return_error = true;
              }
            }
          }
          else
          {
            _return_error = true;
          }          
         

          if (!_return_error)
          {
            _trx.Commit();
          }

        }
        if (_return_error)
        {
          // 20/10/2016 ETP: Undo PinPad Operations if and error has ocurred:
          if (_is_pinpad_recharge)
          {
            using (DB_TRX _db_trx = new DB_TRX())
            {
              _pinpad_output.PinPadTransaction.OperationId = _operation_id;
              _pinpad_output.PinPadTransaction.StatusCode = PinPadTransactions.STATUS.ERROR;
              _pinpad_output.PinPadTransaction.DB_Insert(_db_trx.SqlTransaction);

              _pinpad_output.PinPadTransaction.UpdateCashierSessionHasPinPadOperation(_csi.CashierSessionId, _db_trx.SqlTransaction);

              PinPadTransactions.UpdateDateUndoPinPad(_pinpad_output.PinPadTransaction.ControlNumber, _db_trx.SqlTransaction);
              _db_trx.Commit();
            }
          }

          return false;
        }

        if (GenerateVoucher)
        {
          _voucher_list.AddRange(_voucher_list_tpv);
          VoucherPrint.Print(_voucher_list);
        }

        return _return;
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        return false;
      }
    }

    /// <summary>
    /// Register exit
    /// </summary>
    /// <param name="AccountsReception"></param>
    /// <param name="VisitsUpdated"></param>
    /// <returns></returns>
    protected Boolean RegisterExit(AccountsReception AccountsReception, out Int32 VisitsUpdated)
    {
      VisitsUpdated = 0;

      try
      {
        using (DB_TRX _trx = new DB_TRX())
        {
          if (Common_RegisterExit(AccountsReception, _trx.SqlTransaction, out VisitsUpdated))
          {
            _trx.Commit();
          }
        }

        return true;
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
    
      return false;
    } // RegisterExit

    protected TicketData GetEmptyTicketData()
    {
      return new TicketData
      {
        Agreement = false,
        Coupon = string.Empty,
        EntryId = 0,
        ExpireDate = DateTime.Now,
        PricePaid = 0,
        PriceReal = 0,
        Sequence = 0,
        VoucherId = 0
      };
    }

    private Boolean Common_RegisterEntry(AccountsReception AccountsReception, TicketData TicketData, CardData CardData, bool GenerateCashierMovement, bool GenerateVoucher, SqlTransaction Trx, out ArrayList VoucherList)
    {
      Int64 _operation_id = 0;
      CashierMovementsTable _cashier_movements_table;
      CashierSessionInfo _csi;
      CustomerEntrance _entrance;
      Currency[] _amounts;
      CASHIER_MOVEMENT _cm;
      Currency _aux_amount;
      AccountMovementsTable _account_movements_table;
      MovementType _movement_type;
      CardData _card;
      Boolean _return;
      JunketsShared _junket_business;

      _return = true;
      VoucherList = new ArrayList();
      _junket_business = new JunketsShared();

      try
      {
        _csi = Cashier.CashierSessionInfo();
        _cashier_movements_table = new CashierMovementsTable(_csi);


        _cm = CASHIER_MOVEMENT.CUSTOMER_ENTRANCE;

        using (DB_TRX _trx = new DB_TRX())
        {
          if (GenerateCashierMovement)
          {
            //Generate OperationId
            if (!Operations.DB_InsertOperation(OperationCode.CUSTOMER_ENTRANCE, AccountsReception.AccountId, _csi.CashierSessionId, 0,
                                                0, TicketData.PricePaid, 0, 0, 0, string.Empty, out _operation_id, Trx))
            {
              return false;
            }

            if (TicketData.ExchangeResult.InCurrencyCode == CurrencyExchange.GetNationalCurrency())
            {
              _aux_amount = 0;
              switch (TicketData.ExchangeResult.InType)
              {
                case CurrencyExchangeType.CARD:

                  if (TicketData.ExchangeResult.SubType == CurrencyExchangeSubType.NONE)
                    _cm = CASHIER_MOVEMENT.CUSTOMER_ENTRANCE_CARD;
                  else if (TicketData.ExchangeResult.SubType == CurrencyExchangeSubType.DEBIT_CARD)
                    _cm = CASHIER_MOVEMENT.CUSTOMER_ENTRANCE_DEBIT_CARD;
                  else if (TicketData.ExchangeResult.SubType == CurrencyExchangeSubType.CREDIT_CARD)
                    _cm = CASHIER_MOVEMENT.CUSTOMER_ENTRANCE_CREDIT_CARD;

                  break;
                case CurrencyExchangeType.CHECK:
                  _cm = CASHIER_MOVEMENT.CUSTOMER_ENTRANCE_CHECK;
                  break;

              }
            }
            else
            {
              _aux_amount = TicketData.ExchangeResult.InAmount;
            }


            //Generate Cashier Movements
            _cashier_movements_table = new CashierMovementsTable(_csi);
            _cashier_movements_table.Add(_operation_id, _cm, TicketData.PricePaid, AccountsReception.AccountId, String.Empty, String.Empty
                    , TicketData.ExchangeResult.InCurrencyCode, 0, 0, 0, _aux_amount, TicketData.ExchangeResult.InType, null, null);

            if (!_cashier_movements_table.Save(Trx))
            {
              return false;
            }
          }

          _junket_business.BusinessLogic = TicketData.junketInfo;

          if (_junket_business.BusinessLogic == null && AccountsReception.JunketsBusinessLogic != null)
          {
            _junket_business.BusinessLogic = AccountsReception.JunketsBusinessLogic;
            _junket_business.BusinessLogic.AccountId = AccountsReception.AccountId;
          }

          if (GenerateVoucher)
          {
            _amounts = new Currency[3];
            _amounts[0] = TicketData.PriceReal;// Base price
            _amounts[1] = TicketData.PricePaid;// Paid price
            _amounts[2] = Math.Abs(TicketData.PriceReal - TicketData.PricePaid);// Difference

            //Generate & Save Cashier Voucher for Entrance and LOPD
            VoucherList = VoucherBuilder.VoucherReception(CardData.VoucherAccountInfo(true)
                                                            , AccountsReception.AccountId
                                                            , CashierVoucherType.CustomerEntrance
                                                            , _amounts
                                                            , TicketData.ExchangeResult
                                                            , TicketData.Coupon
                                                            , TicketData.ValidGamingDays
                                                            , TicketData.Agreement
                                                            , PrintMode.Print
                                                            , _junket_business.BusinessLogic
                                                            , _operation_id
                                                            , Trx);

          }

          AccountsReception.AceptedAgreement = TicketData.Agreement;

          if (!Entrance.RecordEntry(AccountsReception, TicketData, out _entrance, Trx))
              {
                return false;
              }

          if (_junket_business.BusinessLogic != null)
              {
            if (!_junket_business.ProcessJunketToAccount(_operation_id, Trx))
                {
                  return false;
                }
          }

          // XGJ 07-SET-2016
          // Create account movement
          _movement_type = MovementType.Admission;
          _card = new CardData();

          CardData.DB_CardGetAllData(AccountsReception.AccountId, _card);
          _account_movements_table = new AccountMovementsTable(_csi);
          _account_movements_table.Add(_operation_id, AccountsReception.AccountId, _movement_type, _card.CurrentBalance, TicketData.PricePaid, 0, _card.CurrentBalance);

          if (!_account_movements_table.Save(Trx))
          {
            return false;
          }
        }
        return _return;
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        return false;
      }
    }

    /// <summary>
    /// Register exit
    /// </summary>
    /// <param name="AccountsReception"></param>
    /// <param name="Trx"></param>
    /// <param name="VisitsUpdated"></param>
    /// <returns></returns>
    private Boolean Common_RegisterExit(AccountsReception AccountsReception, SqlTransaction Trx, out Int32 VisitsUpdated)
    {
      VisitsUpdated = 0;

      try
      {
        if (!Entrance.RecordExit(AccountsReception, Trx, out VisitsUpdated))
        {
          return false;
        }

        return true;
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      return false;
    } // Common_RegisterExit

  }
}
