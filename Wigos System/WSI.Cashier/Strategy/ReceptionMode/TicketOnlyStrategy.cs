﻿//------------------------------------------------------------------------------
// Copyright © 2007-2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: TicketOnlyStrategy.cs
// 
//   DESCRIPTION: Implementation of ticket only strategy class
//                
//                
//                
//                
//                
//                
// 
//        AUTHOR: Ariel Vazquez Zerpa.
// 
// CREATION DATE: 01-FEB-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 01-FEB-2016 AVZ    First release. Product Backlog Item 8673:Visitas / Recepción: Configuración de Modo de Recepción
// 10-OCT-2017 DPC    [WIGOS-5525]: Reception - Register customer output - Cashier button - Exit logic
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using WSI.Cashier.MVC.Model.PlayerEditDetails;
using WSI.Common;
using WSI.Common.Entrances;
using WSI.Common.Entrances.Entities;

namespace WSI.Cashier.Strategy.ReceptionMode
{
  public class TicketOnlyStrategy : ReceptionModeStrategy
  {
    public override bool? Entry(PlayerEditDetailsReceptionModel Model, AccountsReception AccountsReception, CardData CardData)
    {
      try
      {
        Int64 _voucher_id;
        String _error_str = string.Empty;
        Voucher _voucher;
        DateTime _entrance_expiration;
        TimeSpan _timespan;
        TicketData _ticket_data;

        _ticket_data = base.GetEmptyTicketData();
        AccountsReception.AceptedAgreement = false;

        //If the customer doesn't visit the casino in this working date, register entry with new voucher
        if (!Entrance.CustomerHasValidVisitWithVoucher((Int64)Model.AccountId, out _voucher_id, out _entrance_expiration))
          return base.RegisterEntry(AccountsReception, _ticket_data, CardData, false, true);

        //If the customer visit the casino in this working date
        DialogResult _dlr = frm_message.Show(Resource.String("STR_FRM_RECEPTION_SEARCH_UNEXPIRED_ENTRY"), Resource.String("STR_APP_GEN_MSG_WARNING"),
          MessageBoxButtons.OKCancel, Images.CashierImage.Warning, null);

        //Register entry without new voucher and reprint the existing voucher
        Entrance.GetLastEntranceVoucher(_voucher_id, out _voucher);
        _ticket_data.Sequence = _voucher.VoucherSeqNumber;
        _ticket_data.VoucherId = _voucher.VoucherId;
        _ticket_data.ExpireDate = _entrance_expiration;
        _timespan = _ticket_data.ExpireDate - Misc.TodayOpening();
        _ticket_data.ValidGamingDays = Math.Max(0, _timespan.Days);

        if (base.RegisterEntry(AccountsReception, _ticket_data, CardData, false, false))
        {
          if ((_dlr == DialogResult.OK) && (_voucher != null))
            VoucherPrint.Print(_voucher, true);

          return true;
        }

        return false;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Log.Warning("Error en TicketOnlyStrategy.Entry");
        return false;
      }
    }

    /// <summary>
    /// Register exit
    /// </summary>
    /// <param name="Model"></param>
    /// <param name="AccountsReception"></param>
    /// <param name="VisitsUpdated"></param>
    /// <returns></returns>
    public override bool? Exit(PlayerEditDetailsReceptionModel Model, AccountsReception AccountsReception, out Int32 VisitsUpdated)
    {
      VisitsUpdated = 0;

      try
      {
        return base.RegisterExit(AccountsReception, out VisitsUpdated);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      
      return false;
    } // Exit

  }
}
