﻿//------------------------------------------------------------------------------
// Copyright © 2007-2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: ReceptionModeStrategyFactory.cs
// 
//   DESCRIPTION: Implementation of reception mode strategy factory class. Creates
//                the strategy based the enum value
//                
//                
//                
//                
//                
// 
//        AUTHOR: Ariel Vazquez Zerpa.
// 
// CREATION DATE: 01-FEB-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 01-FEB-2016 AVZ    First release. Product Backlog Item 8673:Visitas / Recepción: Configuración de Modo de Recepción
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using WSI.Common.Entrances.Enums;

namespace WSI.Cashier.Strategy.ReceptionMode
{
  public class ReceptionModeStrategyFactory
  {
    public ReceptionModeStrategy CreateStrategy(ENUM_RECEPTION_MODE ReceptionMode)
    {
      switch(ReceptionMode)
      {
        case ENUM_RECEPTION_MODE.Lite:
          return new LiteModeStrategy();
        case ENUM_RECEPTION_MODE.TicketOnly:
          return new TicketOnlyStrategy();
        case ENUM_RECEPTION_MODE.TicketWithPrice:
          return new TicketWithPriceStrategy();
        default:
          return new LiteModeStrategy();
      }
    }
  }
}
