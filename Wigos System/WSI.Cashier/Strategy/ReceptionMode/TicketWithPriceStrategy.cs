﻿//------------------------------------------------------------------------------
// Copyright © 2007-2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: TicketWithPriceStrategy.cs
// 
//   DESCRIPTION: Implementation of ticket with price strategy class
//                
//                
//                
//                
//                
//                
// 
//        AUTHOR: Ariel Vazquez Zerpa.
// 
// CREATION DATE: 01-FEB-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 01-FEB-2016 AVZ    First release. Product Backlog Item 8673:Visitas / Recepción: Configuración de Modo de Recepción
// 05-APR-2016 YNM    Fixed Bug 11344: registering new entry error. Reprint ticket
// 10-OCT-2017 DPC    [WIGOS-5525]: Reception - Register customer output - Cashier button - Exit logic
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using WSI.Cashier.MVC.Model.PlayerEditDetails;
using WSI.Common;
using WSI.Common.Entrances;
using WSI.Common.Entrances.Entities;

namespace WSI.Cashier.Strategy.ReceptionMode
{
  public class TicketWithPriceStrategy : ReceptionModeStrategy
  {
    public override bool? Entry(PlayerEditDetailsReceptionModel Model, AccountsReception AccountsReception, CardData CardData)
    {
      try
      {
        TicketData _ticket_data = null;
        Int64 _voucher_id;
        DateTime _entrance_expiration;
        Voucher _voucher;
        TimeSpan _timespan;

        bool _has_visited_with_ticket = Entrance.CustomerHasValidVisitWithVoucher((Int64)Model.AccountId, out _voucher_id, out _entrance_expiration);

        //If the customer visit the casino in this working date - ask the cashier if he wants to create a new ticket or reprint the existing ticket
        if (_has_visited_with_ticket)
        {
          DialogResult _dlr = frm_message.Show(Resource.String("STR_FRM_RECEPTION_UNEXPIRED_ENTRY_CHARGE_AGAIN"), Resource.String("STR_APP_GEN_MSG_WARNING"),
            MessageBoxButtons.YesNo, Images.CashierImage.Warning, null);

          if (_dlr != DialogResult.OK)
          {
            //Register entry without new voucher and reprint the existing voucher
            Entrance.GetLastEntranceVoucher(_voucher_id, out _voucher);
            _ticket_data = base.GetEmptyTicketData();
            _ticket_data.ExpireDate = _entrance_expiration;
            _ticket_data.Sequence = _voucher.VoucherSeqNumber;
            _ticket_data.VoucherId = _voucher.VoucherId;
            _timespan = _ticket_data.ExpireDate - Misc.TodayOpening();
            _ticket_data.ValidGamingDays = Math.Max(0, _timespan.Days);

            if (base.RegisterEntry(AccountsReception, _ticket_data, CardData, false, false))
            {
              return true;
            }

            return false;
          }
        }

        //If the customer doesn't visit the casino in this working date or the cashier wants to create a new voucher, open the Reception Ticket Form
        using (var _frm_yesno = new frm_yesno())
        {
          _frm_yesno.Opacity = 0.6;
          _frm_yesno.Show();

          using (frm_reception_ticket _frm_recep = new frm_reception_ticket())
          {
            _ticket_data = _frm_recep.ProcessTicket(Model.AccountId, Model.HolderDocument, Model.CardLevel, false);

            if (_ticket_data != null)
            {
              AccountsReception.AceptedAgreement = _ticket_data.Agreement;
              return (base.RegisterEntry(AccountsReception, _ticket_data, CardData, true, true));
            }
            return null;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Log.Warning("Error en TicketWithPriceStrategy.Entry");
        return false;
      }
    }

    /// <summary>
    /// Resgister exit
    /// </summary>
    /// <param name="Model"></param>
    /// <param name="AccountsReception"></param>
    /// <param name="VisitsUpdated"></param>
    /// <returns></returns>
    public override bool? Exit(PlayerEditDetailsReceptionModel Model, AccountsReception AccountsReception, out Int32 VisitsUpdated)
    {
      VisitsUpdated = 0;

      try
      {
        return base.RegisterExit(AccountsReception, out VisitsUpdated);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // Exit

  }
}
