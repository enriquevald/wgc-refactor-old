﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WSI.Cashier.Controls;
using WSI.Common;

namespace WSI.Cashier
{
  public partial class frm_pin_imput : frm_base
  {
    #region " Members "

    private Form m_parent_form;
    private UserCard m_card;
    private String m_error_message;
    private Boolean m_is_login;
    private Boolean m_close_form;

    #endregion 

    #region " Properties "

    public UserCard UserCard
    {
      get { return this.m_card; }
      set { this.m_card = value; }
    } // UserCard

    public String ErrorMessage
    {
      get { return this.m_error_message; }
      set { this.m_error_message = value; }
    } // ErrorMessage

    public Boolean IsLogin
    {
      get { return this.m_is_login; }
      set { this.m_is_login = value; }
    } // ErrorMessage

    #endregion

    #region " Public Methods "

    public frm_pin_imput()
    {
      InitializeComponent();

      base.PerformOnExitClicked += this.CloseUserPinWindow;
    } // frm_pin_imput

    /// <summary>
    /// Show Pin imput form
    /// </summary>
    /// <param name="UserId"></param>
    /// <param name="Parent"></param>
    /// <returns></returns>
    public DialogResult Show(Int32 UserId)
    {
      return this.Show(UserId, null);
    }
    public DialogResult Show(Int32 UserId, Form Parent)
    {
      this.IsLogin = (Parent != null);
      this.ErrorMessage = String.Empty;
      this.DialogResult = DialogResult.Abort;

      try
      {
        this.FormTitle = Resource.String("STR_FRM_USER_INSERT_PIN");
        // Initialize txt pin
        this.txt_pin.Text = String.Empty;

        // Get User data
        this.UserCard = new UserCard(UserId, WCP_CardTypes.CARD_TYPE_EMPLOYEE);
        
        switch (this.UserCard.Status)
        {
          case UserCardStatus.Ok:

            if (Parent != null)
            {
              // Set parent form
              m_parent_form = Parent;
              m_parent_form.Visible = false;
            }

            this.ShowDialog(m_parent_form);
          break;

          case UserCardStatus.AccountBlocked:
            this.ErrorMessage = Resource.String("STR_FRM_USER_BLOCKED_ERROR");

            // Only for blocked account in change PIN verification.
            if (!this.IsLogin)
            {
              this.DialogResult = DialogResult.No;
            }

            break;

          case UserCardStatus.NoPin:
            this.ErrorMessage = Resource.String("STR_FRM_NO_PIN_ERROR");            
            break;

          default:
            // Nothing TODO
          break;
        }        
      }
      catch(Exception ex)
      {
        Log.Exception(ex);            
      }
      
      this.ShowError();

      if (Parent != null)
      {
        // Show login form
        m_parent_form.Visible = (this.DialogResult != DialogResult.OK);
      }

      return this.DialogResult;
    } // Show

    #endregion

    #region " Private Functions "

    /// <summary>
    /// Check account pin
    /// </summary>
    /// <returns></returns>
    private Boolean IsPinOk(String PinRequest)
    {
      this.m_close_form = true;

      switch (this.UserCard.CheckUserPin(PinRequest))
      {
        case PinCheckStatus.OK:
        {
          this.ErrorMessage = String.Empty;
          this.DialogResult = DialogResult.OK;

          return true;
        }

        case PinCheckStatus.WRONG_PIN:
          this.m_close_form = false;
          this.ErrorMessage = Resource.String("STR_FRM_WRONG_PIN_ERROR");
          this.ShowError();
          break;

        case PinCheckStatus.ACCOUNT_BLOCKED:
          this.DialogResult = (this.IsLogin) ? DialogResult.Abort : DialogResult.No;
          this.ErrorMessage = Resource.String("STR_FRM_USER_BLOCKED_ERROR");
          break;

        case PinCheckStatus.ACCOUNT_NO_PIN:
          this.DialogResult = DialogResult.Abort;
          this.ErrorMessage = Resource.String("STR_FRM_NO_PIN_ERROR");            
          break;

        case PinCheckStatus.ERROR:
        default:
          this.DialogResult = DialogResult.Abort;
          this.ErrorMessage = Resource.String("STR_FRM_PIN_ERROR");          
          break;
      } // switch

      return false;

    } // IsPinOk

    /// <summary>
    /// Add Pin numbers to textbox
    /// </summary>
    /// <param name="Number"></param>
    private void AddNumber(String Number)
    {
      this.txt_pin.Text += Number;

      if (this.txt_pin.Text.Length < 4)
      {
        return;
      }

      this.ValidatePin();
    } // AddNumber

     /// <summary>
    /// Validate PIN
    /// </summary>
    private void ValidatePin()
    {
      if (this.txt_pin.Text.Length == 4)
      {
        if (!IsPinOk(this.txt_pin.Text))
        {
          this.txt_pin.Text = String.Empty;
        }
      }

      if(this.m_close_form)
      {
        this.Close();
      }
    } // ValidatePin

    /// <summary>
    ///  Show error message
    /// </summary>
    private void ShowError()
    {
      if(String.IsNullOrEmpty(this.ErrorMessage))
      {
        return;
      }

      frm_message.Show( this.ErrorMessage
                      , Resource.String("STR_FRM_PIN_CAPTION")
                      , MessageBoxButtons.OK
                      , MessageBoxIcon.Error
                      , m_parent_form);

      this.ErrorMessage = String.Empty;
    } // ShowError

    #endregion

    #region " Events "

    private void btn_num_1_Click(object sender, EventArgs e)
    {
      this.AddNumber(((ButtonBase)sender).Text);
    } // btn_num_1_Click

    private void btn_num_2_Click(object sender, EventArgs e)
    {
      this.AddNumber(((ButtonBase)sender).Text);
    } // btn_num_2_Click

    private void btn_num_3_Click(object sender, EventArgs e)
    {
      this.AddNumber(((ButtonBase)sender).Text);
    } // btn_num_3_Click

    private void btn_num_4_Click(object sender, EventArgs e)
    {
      this.AddNumber(((ButtonBase)sender).Text);
    } // btn_num_4_Click

    private void btn_num_5_Click(object sender, EventArgs e)
    {
      this.AddNumber(((ButtonBase)sender).Text);
    } // btn_num_5_Click

    private void btn_num_6_Click(object sender, EventArgs e)
    {
      this.AddNumber(((ButtonBase)sender).Text);
    } // btn_num_6_Click

    private void btn_num_7_Click(object sender, EventArgs e)
    {
      this.AddNumber(((ButtonBase)sender).Text);
    } // btn_num_7_Click

    private void btn_num_8_Click(object sender, EventArgs e)
    {
      this.AddNumber(((ButtonBase)sender).Text);
    } // btn_num_8_Click

    private void btn_num_9_Click(object sender, EventArgs e)
    {
      this.AddNumber(((ButtonBase)sender).Text);
    } // btn_num_9_Click

    private void btn_num_0_Click(object sender, EventArgs e)
    {
      this.AddNumber(((ButtonBase)sender).Text);
    } // btn_num_0_Click

    private void btn_num_delete_Click(object sender, EventArgs e)
    {
      this.txt_pin.Text = String.Empty;
    } // btn_num_delete_Click

    //private void txt_pin_TextChanged(object sender, EventArgs e)
    //{
    //  this.ValidatePin();
    //} // txt_pin_TextChanged
    
    private void CloseUserPinWindow()
    {
      this.DialogResult = DialogResult.Cancel;
    } // CloseUserPinWindow

    #endregion
  }
}
