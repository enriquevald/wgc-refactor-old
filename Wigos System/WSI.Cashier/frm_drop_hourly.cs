﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_drop_hourly.cs
// 
//   DESCRIPTION: 
//
//        AUTHOR: Rafa Arjona
// 
// CREATION DATE: 26-APR-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 26-APR-2017 RAB    First release (PBI 26811: Drop gaming table information - Drop hourly report)
// 05-MAY-2017 DHA    PBI 27175:Drop gaming table information - Global drop hourly report (Filter)
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WSI.Cashier.Controls;
using WSI.Common;

namespace WSI.Cashier
{
  public partial class frm_drop_hourly : frm_base
  {
    #region Constants

    private const Int32 GRID_MAX_COLUMNS = 2;

    private const Int32 GRID_COLUMN_HOURLY_DROP_DATE = 0;
    private const Int32 GRID_COLUMN_TOTAL_SELL_CHIPS = 1;
    private const Int32 GRID_COLUMN_TOTAL_SELL_CHIPS_ADD_ISO_CODE = 2;

    private const Int32 GRID_COLUMN_HOURLY_DROP_DATE_WIDTH = 180;
    private const Int32 GRID_COLUMN_TOTAL_SELL_CHIPS_WIDTH = 0;
    private const Int32 GRID_COLUMN_TOTAL_SELL_CHIPS_ADD_ISO_CODE_WIDTH = 190;

    private const Int32 SQL_COLUMN_SESSION_ID = 0;
    private const Int32 SQL_COLUMN_HOURLY_DROP_DATE = 1;
    private const Int32 SQL_COLUMN_ISO_CODE = 2;
    private const Int32 SQL_COLUMN_TOTAL_SELL_CHIPS = 3;

    #endregion

    #region Constructor

    public frm_drop_hourly()
    {
      InitializeComponent();
    } //frm_drop_hourly

    #endregion

    #region Public Methods

    static public void Show(Form Parent, Int64 GamingTableSessionId)
    {
      frm_drop_hourly _drop_hourly;
      _drop_hourly = new frm_drop_hourly();
      _drop_hourly.Init(Parent, GamingTableSessionId);

      using (frm_yesno form_yes_no = new frm_yesno())
      {
        form_yes_no.Opacity = 0.6;
        form_yes_no.Show();
        _drop_hourly.ShowDialog(form_yes_no);
      }

      if (Parent != null)
      {
        Parent.Focus();
      }

      _drop_hourly.Dispose();
      _drop_hourly = null;
    } // Show


    #endregion

    #region Private Methods

    /// <summary>
    /// Init variables
    /// </summary>
    private void Init(Form Parent, Int64 GamingTableSessionId)
    {
      this.FormTitle = Resource.String("STR_UC_BANK_BTN_GAMING_TABLE_DROP_HOURLY");
      btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CLOSE");

      Location = new Point(1024 / 2 - this.Width / 2, 768 / 2 - this.Height / 2 - 90);

      GetTotalHourlyDrop(GamingTableSessionId);
    } // Init

    /// <summary>
    /// Get total hourly drop of gaming table session id
    /// </summary>
    /// <param name="GamingTableSessionId">Current gaming table session id</param>
    /// <returns>The function result (true/false)</returns>
    private Boolean GetTotalHourlyDrop(Int64 GamingTableSessionId)
    {
      DataSet _ds;

      try
      {
        _ds = new DataSet();
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand("GT_HourlyDrop", _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.CommandType = CommandType.StoredProcedure;
            _sql_cmd.Parameters.Add("@pGamingTableSessionId", SqlDbType.BigInt).Value = GamingTableSessionId;
            _sql_cmd.Parameters.Add("@pDateFrom", SqlDbType.DateTime).Value = DBNull.Value;
            _sql_cmd.Parameters.Add("@pDateTo", SqlDbType.DateTime).Value = DBNull.Value;
            _sql_cmd.Parameters.Add("@pIsoCode", SqlDbType.NVarChar).Value = DBNull.Value;
            _sql_cmd.Parameters.Add("@pSaleChipsMov", SqlDbType.Int).Value = PlayerTrackingMovementType.SaleChips;
            _sql_cmd.Parameters.Add("@pValCopyDealerMov", SqlDbType.Int).Value = PlayerTrackingMovementType.DealerCopyValidate;
            _sql_cmd.Parameters.Add("@pRedeemableChipsType", SqlDbType.Int).Value = CurrencyExchangeType.CASINO_CHIP_RE;
            _sql_cmd.Parameters.Add("@pGamingTablesId", SqlDbType.NVarChar).Value = DBNull.Value;

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              _sql_da.Fill(_ds);
            }
          }

          dgv_hourly_drop.DataSource = AccumulateTotalHourlyDrop(_ds, _db_trx.SqlTransaction);
        }

        HourlyDropGridStyle();
        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } //GetTotalHourlyDrop

    /// <summary>
    /// Accumulated total hourly drop converting not national currency to national currency value.
    /// </summary>
    /// <param name="HourlyDropValues">Values of hourly drop dataset</param>
    /// <param name="Transaction">SQL Transaction</param>
    /// <returns>Data to display in the grid</returns>
    private DataTable AccumulateTotalHourlyDrop(DataSet HourlyDropValues, SqlTransaction Transaction)
    {
      DataTable _dt_total_hourly_drop;
      DateTime _last_datetime;
      Currency _exchange_drop;

      _last_datetime = DateTime.MinValue;
      _dt_total_hourly_drop = new DataTable();
      _dt_total_hourly_drop.Columns.Add("HOURLY_DROP_DATE", typeof(DateTime));
      _dt_total_hourly_drop.Columns.Add("TOTAL_SELL_CHIPS", typeof(Currency));      
      _dt_total_hourly_drop.Columns.Add("TOTAL_SELL_CHIP_ADD_ISO_CODE", typeof(String));

      foreach (DataRow _row in HourlyDropValues.Tables[0].Rows)
      {
        _exchange_drop = CurrencyExchange.GetExchange(Format.ParseCurrency(_row[SQL_COLUMN_TOTAL_SELL_CHIPS].ToString()), _row[SQL_COLUMN_ISO_CODE].ToString(), CurrencyExchange.GetNationalCurrency(), Transaction);

        //It compares the current datetime to the previous. If its the same it means that you have to accumulate.
        if (_row[SQL_COLUMN_HOURLY_DROP_DATE].Equals(_last_datetime))
        {
          Currency _drop_amount;
          _drop_amount = (Currency)_dt_total_hourly_drop.Rows[_dt_total_hourly_drop.Rows.Count - 1][GRID_COLUMN_TOTAL_SELL_CHIPS];
          _drop_amount += _exchange_drop;
          _dt_total_hourly_drop.Rows[_dt_total_hourly_drop.Rows.Count - 1][GRID_COLUMN_TOTAL_SELL_CHIPS] = _drop_amount;
          _dt_total_hourly_drop.Rows[_dt_total_hourly_drop.Rows.Count - 1][GRID_COLUMN_TOTAL_SELL_CHIPS_ADD_ISO_CODE] = Currency.Format(_drop_amount, CurrencyExchange.GetNationalCurrency());
        }
        //It compares the current datetime to the previous. If different creates a new row.
        else
        {
          DataRow _new_row;
          _new_row = _dt_total_hourly_drop.NewRow();
          _new_row.ItemArray = new Object[] { (DateTime)_row.ItemArray[SQL_COLUMN_HOURLY_DROP_DATE], 
                                              _exchange_drop, Currency.Format(_exchange_drop, CurrencyExchange.GetNationalCurrency())
                                            };
          _dt_total_hourly_drop.Rows.InsertAt(_new_row, _dt_total_hourly_drop.Rows.Count);
        }
        _last_datetime = (DateTime)_row[SQL_COLUMN_HOURLY_DROP_DATE];
      }

      //If there aren't records in the previous datatable displays the current time zone and drop 0 national currency.
      if(_dt_total_hourly_drop.Rows.Count == 0)
      {
        _dt_total_hourly_drop = new DataTable();
        _dt_total_hourly_drop.Columns.Add("HOURLY_DROP_DATE", typeof(String));
        _dt_total_hourly_drop.Columns.Add("TOTAL_SELL_CHIPS", typeof(String));
        _dt_total_hourly_drop.Columns.Add("TOTAL_SELL_CHIP_ADD_ISO_CODE", typeof(String));

        DataRow _new_row;
        DateTime _initial_datetime;
        _new_row = _dt_total_hourly_drop.NewRow();
        _initial_datetime = new DateTime(WGDB.Now.Year, 
                                         WGDB.Now.Month, 
                                         WGDB.Now.Day, 
                                         WGDB.Now.Hour, 
                                         0, 
                                         0);
        
        _new_row.ItemArray = new Object[] { _initial_datetime, 
                                            0, 
                                            Currency.Format(0, CurrencyExchange.GetNationalCurrency())
                                          };
        _dt_total_hourly_drop.Rows.InsertAt(_new_row, 0);
      }

      return _dt_total_hourly_drop;
    } //AccumulateTotalHourlyDrop

    /// <summary>
    /// Define grid style
    /// </summary>
    private void HourlyDropGridStyle()
    {
      String _date_format;
      String _time_separator;
      _date_format = System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern;
      _time_separator = System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.TimeSeparator;

      dgv_hourly_drop.Columns[GRID_COLUMN_HOURLY_DROP_DATE].Width = GRID_COLUMN_HOURLY_DROP_DATE_WIDTH;
      dgv_hourly_drop.Columns[GRID_COLUMN_HOURLY_DROP_DATE].SortMode = DataGridViewColumnSortMode.NotSortable;
      dgv_hourly_drop.Columns[GRID_COLUMN_HOURLY_DROP_DATE].HeaderText = Resource.String("STR_SESSION_DATE");
      dgv_hourly_drop.Columns[GRID_COLUMN_HOURLY_DROP_DATE].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
      dgv_hourly_drop.Columns[GRID_COLUMN_HOURLY_DROP_DATE].DefaultCellStyle.Format = _date_format + " HH" + _time_separator + "mm" + _time_separator + "ss"; //Seen in frm_vouchers_reprint.cs

      dgv_hourly_drop.Columns[GRID_COLUMN_TOTAL_SELL_CHIPS].Visible = false;

      dgv_hourly_drop.Columns[GRID_COLUMN_TOTAL_SELL_CHIPS_ADD_ISO_CODE].Width = GRID_COLUMN_TOTAL_SELL_CHIPS_ADD_ISO_CODE_WIDTH;
      dgv_hourly_drop.Columns[GRID_COLUMN_TOTAL_SELL_CHIPS_ADD_ISO_CODE].SortMode = DataGridViewColumnSortMode.NotSortable;
      dgv_hourly_drop.Columns[GRID_COLUMN_TOTAL_SELL_CHIPS_ADD_ISO_CODE].HeaderText = Resource.String("STR_GAMING_TABLE_REPORT_DROP");
      dgv_hourly_drop.Columns[GRID_COLUMN_TOTAL_SELL_CHIPS_ADD_ISO_CODE].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

      this.dgv_hourly_drop.Sort(this.dgv_hourly_drop.Columns[GRID_COLUMN_HOURLY_DROP_DATE], ListSortDirection.Descending);
    } //HourlyDropGridStyle

    #endregion
  }
}