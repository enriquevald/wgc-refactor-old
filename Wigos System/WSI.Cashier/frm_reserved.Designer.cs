﻿namespace WSI.Cashier
{
  partial class frm_reserved
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.lbl_reserved_title = new System.Windows.Forms.Label();
      this.uc_input_amount = new WSI.Cashier.uc_input_amount();
      this.chk_mode_reserved = new WSI.Cashier.Controls.uc_checkBox();
      this.btn_cancel = new WSI.Cashier.Controls.uc_round_button();
      this.btn_ok = new WSI.Cashier.Controls.uc_round_button();
      this.gb_reserved = new WSI.Cashier.Controls.uc_round_panel();
      this.lbl_reserved = new System.Windows.Forms.Label();
      this.gb_total_redeem = new WSI.Cashier.Controls.uc_round_panel();
      this.lbl_total_redeem = new System.Windows.Forms.Label();
      this.pnl_data.SuspendLayout();
      this.gb_reserved.SuspendLayout();
      this.gb_total_redeem.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.Controls.Add(this.uc_input_amount);
      this.pnl_data.Controls.Add(this.chk_mode_reserved);
      this.pnl_data.Controls.Add(this.btn_cancel);
      this.pnl_data.Controls.Add(this.btn_ok);
      this.pnl_data.Controls.Add(this.gb_reserved);
      this.pnl_data.Controls.Add(this.gb_total_redeem);
      this.pnl_data.Size = new System.Drawing.Size(392, 629);
      // 
      // lbl_reserved_title
      // 
      this.lbl_reserved_title.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_reserved_title.ForeColor = System.Drawing.Color.White;
      this.lbl_reserved_title.Location = new System.Drawing.Point(3, 5);
      this.lbl_reserved_title.Name = "lbl_reserved_title";
      this.lbl_reserved_title.Size = new System.Drawing.Size(324, 16);
      this.lbl_reserved_title.TabIndex = 2;
      this.lbl_reserved_title.Text = "xNone";
      // 
      // uc_input_amount
      // 
      this.uc_input_amount.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.uc_input_amount.Location = new System.Drawing.Point(8, 182);
      this.uc_input_amount.Name = "uc_input_amount";
      this.uc_input_amount.Size = new System.Drawing.Size(380, 380);
      this.uc_input_amount.TabIndex = 26;
      this.uc_input_amount.WithDecimals = false;
      this.uc_input_amount.OnAmountSelected += new WSI.Cashier.uc_input_amount.AmountSelectedEventHandler(this.uc_input_amount_OnAmountSelected);
      // 
      // chk_mode_reserved
      // 
      this.chk_mode_reserved.BackColor = System.Drawing.Color.Transparent;
      this.chk_mode_reserved.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.chk_mode_reserved.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.chk_mode_reserved.Location = new System.Drawing.Point(14, 6);
      this.chk_mode_reserved.MinimumSize = new System.Drawing.Size(212, 30);
      this.chk_mode_reserved.Name = "chk_mode_reserved";
      this.chk_mode_reserved.Padding = new System.Windows.Forms.Padding(5);
      this.chk_mode_reserved.Size = new System.Drawing.Size(212, 30);
      this.chk_mode_reserved.TabIndex = 25;
      this.chk_mode_reserved.Text = "xModeReserved";
      this.chk_mode_reserved.UseVisualStyleBackColor = true;
      this.chk_mode_reserved.Click += new System.EventHandler(this.chk_mode_reserved_CheckedChanged);
      // 
      // btn_cancel
      // 
      this.btn_cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.btn_cancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btn_cancel.FlatAppearance.BorderSize = 0;
      this.btn_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_cancel.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_cancel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_cancel.Image = null;
      this.btn_cancel.IsSelected = false;
      this.btn_cancel.Location = new System.Drawing.Point(62, 569);
      this.btn_cancel.Name = "btn_cancel";
      this.btn_cancel.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_cancel.Size = new System.Drawing.Size(128, 48);
      this.btn_cancel.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_cancel.TabIndex = 23;
      this.btn_cancel.Text = "CANCEL";
      this.btn_cancel.UseVisualStyleBackColor = false;
      this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
      // 
      // btn_ok
      // 
      this.btn_ok.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.btn_ok.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_ok.FlatAppearance.BorderSize = 0;
      this.btn_ok.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_ok.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_ok.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_ok.Image = null;
      this.btn_ok.IsSelected = false;
      this.btn_ok.Location = new System.Drawing.Point(203, 569);
      this.btn_ok.Name = "btn_ok";
      this.btn_ok.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_ok.Size = new System.Drawing.Size(128, 48);
      this.btn_ok.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_ok.TabIndex = 24;
      this.btn_ok.Text = "OK";
      this.btn_ok.UseVisualStyleBackColor = false;
      this.btn_ok.Click += new System.EventHandler(this.btn_accept_Click);
      // 
      // gb_reserved
      // 
      this.gb_reserved.BackColor = System.Drawing.Color.Transparent;
      this.gb_reserved.BorderColor = System.Drawing.Color.Empty;
      this.gb_reserved.Controls.Add(this.lbl_reserved);
      this.gb_reserved.CornerRadius = 10;
      this.gb_reserved.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
      this.gb_reserved.ForeColor = System.Drawing.Color.Black;
      this.gb_reserved.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gb_reserved.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_reserved.HeaderHeight = 25;
      this.gb_reserved.HeaderSubText = null;
      this.gb_reserved.HeaderText = null;
      this.gb_reserved.Location = new System.Drawing.Point(8, 112);
      this.gb_reserved.Name = "gb_reserved";
      this.gb_reserved.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_reserved.Size = new System.Drawing.Size(380, 64);
      this.gb_reserved.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NONE;
      this.gb_reserved.TabIndex = 22;
      this.gb_reserved.Text = "xReserved";
      // 
      // lbl_reserved
      // 
      this.lbl_reserved.Font = new System.Drawing.Font("Microsoft Sans Serif", 34F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_reserved.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_reserved.Location = new System.Drawing.Point(6, 21);
      this.lbl_reserved.Name = "lbl_reserved";
      this.lbl_reserved.Size = new System.Drawing.Size(340, 41);
      this.lbl_reserved.TabIndex = 22;
      this.lbl_reserved.Text = "xTotal Reserved";
      // 
      // gb_total_redeem
      // 
      this.gb_total_redeem.BackColor = System.Drawing.Color.Transparent;
      this.gb_total_redeem.BorderColor = System.Drawing.Color.Empty;
      this.gb_total_redeem.Controls.Add(this.lbl_total_redeem);
      this.gb_total_redeem.CornerRadius = 10;
      this.gb_total_redeem.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
      this.gb_total_redeem.ForeColor = System.Drawing.Color.Black;
      this.gb_total_redeem.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gb_total_redeem.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_total_redeem.HeaderHeight = 25;
      this.gb_total_redeem.HeaderSubText = null;
      this.gb_total_redeem.HeaderText = null;
      this.gb_total_redeem.Location = new System.Drawing.Point(8, 42);
      this.gb_total_redeem.Name = "gb_total_redeem";
      this.gb_total_redeem.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_total_redeem.Size = new System.Drawing.Size(380, 70);
      this.gb_total_redeem.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NONE;
      this.gb_total_redeem.TabIndex = 21;
      this.gb_total_redeem.Text = "xTotal Redeem";
      // 
      // lbl_total_redeem
      // 
      this.lbl_total_redeem.Font = new System.Drawing.Font("Microsoft Sans Serif", 34F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_total_redeem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_total_redeem.Location = new System.Drawing.Point(6, 21);
      this.lbl_total_redeem.Name = "lbl_total_redeem";
      this.lbl_total_redeem.Size = new System.Drawing.Size(340, 41);
      this.lbl_total_redeem.TabIndex = 22;
      this.lbl_total_redeem.Text = "xTotal Redeem";
      this.lbl_total_redeem.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // frm_reserved
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(392, 684);
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "frm_reserved";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "frm_reserved";
      this.pnl_data.ResumeLayout(false);
      this.gb_reserved.ResumeLayout(false);
      this.gb_total_redeem.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Label lbl_reserved_title;
    private uc_input_amount uc_input_amount;
    private Controls.uc_checkBox chk_mode_reserved;
    private Controls.uc_round_button btn_cancel;
    private Controls.uc_round_button btn_ok;
    private Controls.uc_round_panel gb_reserved;
    private System.Windows.Forms.Label lbl_reserved;
    private Controls.uc_round_panel gb_total_redeem;
    private System.Windows.Forms.Label lbl_total_redeem;
  }
}