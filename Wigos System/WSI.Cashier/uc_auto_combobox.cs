//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : uc_auto_combobox.cs
// 
//   DESCRIPTION : Auto completable combobox control
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 26-JUN-2013 ANG    First release
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Data;
using System.Drawing;
using System.ComponentModel;
using WSI.Common;

namespace WSI.Cashier
{
  public class uc_auto_combobox : ComboBox
  {

    #region "Properties"

    private String m_typped_text;
    private Boolean m_allow_unlisted_values = false;
    private Int32 m_prev_match_item = Int32.MinValue;

    private ICustomFormatValidable m_format_validator;

    [CategoryAttribute("Data")]
    public ICustomFormatValidable FormatValidator
    {
      get { return this.m_format_validator; }
      set { this.m_format_validator = value; }
    }
    
    [CategoryAttribute("Data")]
    [DefaultValue(false)]
    public Boolean AllowUnlistedValues
    {
      get { return this.m_allow_unlisted_values; }
      set { this.m_allow_unlisted_values = value; }
    }

    [BrowsableAttribute(true)]
    [DefaultValue(50)]
    public new int DropDownHeight { 
      get { return base.DropDownHeight;}
      set { base.DropDownHeight = value;}
    }

    #endregion

    #region "Constructor"

    // PURPOSE: Class default constructor
    //          Call auto combobox without custom format validator.m  
    //  PARAMS:
    //     - INPUT:
    //     - OUTPUT:
    // RETURNS:
    public uc_auto_combobox()
      : this(null)
    {
      // Call contructor without format validator

    } // uc_auto_combobox

    // PURPOSE: Class default constructor
    //          Call auto combobox without custom format validator.m  
    //  PARAMS:
    //     - INPUT: CustomFormatValidator ,
    //              Object that implements ICustomFormatValidable interface 
     //             and knows how to validate format, like CharacterTextBox or NumericTextBox
    //     - OUTPUT:
    // RETURNS:
    public uc_auto_combobox(ICustomFormatValidable CustomFormatValidator)
      : base()
    {
      this.DropDownStyle = ComboBoxStyle.DropDown;
      this.AutoCompleteMode = AutoCompleteMode.None;
      this.m_typped_text = String.Empty;
      this.m_format_validator = CustomFormatValidator;
    }

    #endregion

    #region "Events"

    // PURPOSE: On key Down event handler
    //          Order : keyDown - Key Press - Key Up
    //  PARAMS:
    //     - INPUT:
    //           - KeyEventArgs e
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - void
    protected override void OnKeyDown(KeyEventArgs e)
    {
      base.OnKeyDown(e);

      if (this.DropDownStyle != ComboBoxStyle.DropDown)
      {
        return;
      }
      if (this.Text == String.Empty)
      {
        this.m_typped_text = String.Empty;
      }

      if (e.KeyCode == Keys.Escape)
      {
        this.DroppedDown = false;
        e.Handled = true;
      }

      if (!m_allow_unlisted_values)
      {
        // Allow to clear previous text when user type some key
        if (this.SelectedIndex > -1)
        {
          // Little trick ;) On focus all text are selected ;)
          if (this.SelectionLength == this.Text.Length)
          {
            this.SelectedIndex = -1;
          }
        }
      }

    } // OnKeyDown

    // PURPOSE: On key press event handler
    //
    //  PARAMS:
    //     - INPUT:
    //           - KeyEventArgs e
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - void
    // NOTE : 
    //          Order : keyDown - Key Press - Key Up
    //          The KeyPress event is not raised by noncharacter keys; however, the noncharacter keys do raise the KeyDown and KeyUp events.
    protected override void OnKeyPress(KeyPressEventArgs e)
    {
      base.OnKeyPress(e);

      if (this.DropDownStyle != ComboBoxStyle.DropDown)
      {
        return;
      }

      if (m_format_validator != null)
      {
        m_format_validator.Text = String.Empty + (char)e.KeyChar;
        if (!m_format_validator.ValidateFormat())
        {
          e.Handled = true; // Cancell user input!
        }
      }

      // Check for a valid key that match with content list.
      if (!m_allow_unlisted_values
            && e.KeyChar != (char)Keys.Back)
      {
        //String _cadena;
        if (this.SelectionStart > 0)
        {
          m_typped_text = this.Text.Substring(0, this.SelectionStart);
        }
        else
        {
          m_typped_text = this.Text;
        }
        m_typped_text += (char)e.KeyChar;

        m_prev_match_item = this.FindString(m_typped_text);

        if (m_prev_match_item < 0)
        {
          e.Handled = true; // Cancell user input!
        }
        
      }


    }  // OnKeyPress

    // PURPOSE: On Key up event handler
    //          Autocomplete user input and higlight match text.
    //          Order : keyDown - Key Press - Key Up
    //  PARAMS:
    //     - INPUT:
    //           - KeyEventArgs e
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - void
    protected override void OnKeyUp(KeyEventArgs e)
    {
      base.OnKeyUp(e);

      if (this.DropDownStyle != ComboBoxStyle.DropDown)
      {
        return;
      }
      // Remove content
      if (e.KeyCode == Keys.Delete || e.KeyCode == Keys.Back || e.KeyCode == Keys.Escape )
      {
        e.Handled = true;
        return;
      }

      // Allow to naviegate throught text
      if ( e.KeyCode == Keys.Left 
        || e.KeyCode == Keys.Right || e.KeyCode == Keys.Up 
        || e.KeyCode == Keys.Down)
      {
        this.SelectionLength = 0;
        e.Handled = true;
      }

      // Autocomplete

      if (m_prev_match_item >= 0)
      {
        this.DroppedDown = true;
        this.SelectedIndex = m_prev_match_item;
        this.SelectionStart = m_typped_text.Length;
        this.SelectionLength = Math.Max(this.Text.Length - m_typped_text.Length, 0);
      }

      m_prev_match_item = Int32.MinValue;
      e.Handled = true;
    } // OnKeyUp

    // PURPOSE: OnDropDownClosed
    // Util to avoid problems with on-screen keyboard
    // Retains auto-complete selection on drop down closed
    //  PARAMS:
    //     - INPUT:
    //           - EventArgs e
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - void
    protected override void OnDropDownClosed(EventArgs e)
    {
      base.OnDropDownClosed(e);

      if (this.DropDownStyle != ComboBoxStyle.DropDown)
      {
        return;
      }
      this.SelectionStart = this.m_typped_text.Length;
      this.SelectionLength = Math.Max(this.Text.Length - m_typped_text.Length, 0);
      
    } // OnDropDownClosed

    // Warning! afecta tamb� quan es pica la fletxa!
    protected override void OnClick(EventArgs e)
    {
      MouseEventArgs _mouse_event;
      int _dropDownButtonWidth;
      
      base.OnClick(e);

      if (this.DropDownStyle != ComboBoxStyle.DropDown)
      {
        return;
      }
      _mouse_event = (MouseEventArgs)e;
      _dropDownButtonWidth = System.Windows.Forms.SystemInformation.HorizontalScrollBarArrowWidth;

      if (_mouse_event.X <= (ClientRectangle.Width - _dropDownButtonWidth))
      {
        this.DroppedDown = !this.DroppedDown;
      }

    } // OnClick

    //protected override void OnEnter(EventArgs e)
    //{
    //  base.OnEnter(e);
    //  this.DroppedDown = true;
    //} // OnEnter

    // PURPOSE: OnLeave event handler
    //          Clean use typped text.
    //  PARAMS:
    //     - INPUT:
    //           - KeyPressEventArgs e
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - void
    protected override void OnLeave(EventArgs e)
    {
      this.m_typped_text = String.Empty;
      base.OnLeave(e);

      if (this.DropDownStyle != ComboBoxStyle.DropDown)
      {
        return;
      }
      if (!m_allow_unlisted_values)
      {
        // Autocomplete on finish if not all value typed o selected

        this.SelectedIndex = this.FindString(this.Text);

      }
    } // OnLeave

    #endregion

  }
}
