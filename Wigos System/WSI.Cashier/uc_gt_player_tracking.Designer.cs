namespace WSI.Cashier
{
  partial class uc_gt_player_tracking
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(uc_gt_player_tracking));
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
      this.pictureBox4 = new System.Windows.Forms.PictureBox();
      this.pictureBox5 = new System.Windows.Forms.PictureBox();
      this.pictureBox6 = new System.Windows.Forms.PictureBox();
      this.pb_exit = new System.Windows.Forms.PictureBox();
      this.pb_pause = new System.Windows.Forms.PictureBox();
      this.pb_change_seat = new System.Windows.Forms.PictureBox();
      this.pb_free = new System.Windows.Forms.PictureBox();
      this.pb_ocupated = new System.Windows.Forms.PictureBox();
      this.pb_away = new System.Windows.Forms.PictureBox();
      this.pb_disabled = new System.Windows.Forms.PictureBox();
      this.transparentPanel1 = new WSI.Common.uc_transparent_panel();
      this.dgv_gt_movememnts = new WSI.Cashier.Controls.uc_DataGridView();
      this.pnl_information = new WSI.Cashier.Controls.uc_round_panel();
      this.tlp_information = new System.Windows.Forms.TableLayoutPanel();
      this.pb_legend_disabled_seat = new System.Windows.Forms.PictureBox();
      this.pb_legend_resume_session = new System.Windows.Forms.PictureBox();
      this.pb_legend_end_session = new System.Windows.Forms.PictureBox();
      this.pb_legend_pause_session = new System.Windows.Forms.PictureBox();
      this.pb_legend_swap_position = new System.Windows.Forms.PictureBox();
      this.pb_legend_free_seat = new System.Windows.Forms.PictureBox();
      this.pb_legend_taken_seat = new System.Windows.Forms.PictureBox();
      this.pb_legend_away_player = new System.Windows.Forms.PictureBox();
      this.lbl_legend_end_session = new WSI.Cashier.Controls.uc_label();
      this.lbl_legend_resume_session = new WSI.Cashier.Controls.uc_label();
      this.lbl_legend_pause_session = new WSI.Cashier.Controls.uc_label();
      this.lbl_legend_swap_position = new WSI.Cashier.Controls.uc_label();
      this.lbl_legend_free_seat = new WSI.Cashier.Controls.uc_label();
      this.lbl_legend_taken_seat = new WSI.Cashier.Controls.uc_label();
      this.lbl_legend_away_player = new WSI.Cashier.Controls.uc_label();
      this.lbl_legend_disabled_seat = new WSI.Cashier.Controls.uc_label();
      this.pnl_report = new WSI.Cashier.Controls.uc_round_panel();
      this.webBrowserGtSessionInfo = new WSI.Cashier.Controls.uc_round_webbrowser();
      this.pnl_table_options = new WSI.Cashier.Controls.uc_round_panel();
      this.pnl_container = new WSI.Cashier.Controls.uc_round_panel();
      this.rp_legend_movements = new WSI.Cashier.Controls.uc_round_panel();
      this.tlp_legend_movements = new System.Windows.Forms.TableLayoutPanel();
      this.pb_legend_amount = new System.Windows.Forms.PictureBox();
      this.pb_legend_chip_nre = new System.Windows.Forms.PictureBox();
      this.pb_legend_chip_color = new System.Windows.Forms.PictureBox();
      this.lbl_legend_chip_nre = new WSI.Cashier.Controls.uc_label();
      this.lbl_legend_chip_color = new WSI.Cashier.Controls.uc_label();
      this.lbl_legend_amount = new WSI.Cashier.Controls.uc_label();
      this.pb_legend_chip_re = new System.Windows.Forms.PictureBox();
      this.lbl_legend_chip_re = new WSI.Cashier.Controls.uc_label();
      this.lbl_name_and_data_open_session = new WSI.Cashier.Controls.uc_label();
      this.uc_gt_player_in_session1 = new WSI.Cashier.uc_gt_player_in_session();
      this.gamingTablePanel1 = new WSI.Common.GamingTablePanel();
      this.uc_gt_dealer_copy_ticket_reader1 = new WSI.Cashier.uc_gt_dealer_copy_ticket_reader();
      this.uc_input_amount_resizable1 = new WSI.Cashier.uc_input_amount_resizable();
      this.uc_gt_card_reader1 = new WSI.Cashier.uc_gt_card_reader();
      this.pnl_bottom = new WSI.Cashier.Controls.uc_round_panel();
      this.lbl_drop_national_currency_amount = new WSI.Cashier.Controls.uc_label();
      this.lbl_drop_national_currency = new WSI.Cashier.Controls.uc_label();
      this.btn_table_iso_code = new WSI.Cashier.Controls.uc_round_button();
      this.pnl_menu_table_mode = new WSI.Cashier.Controls.uc_round_panel();
      this.btn_menu_table_mode = new WSI.Cashier.Controls.uc_round_button();
      this.lbl_max_bet_value = new WSI.Cashier.Controls.uc_label();
      this.lbl_min_bet_name = new WSI.Cashier.Controls.uc_label();
      this.lbl_bet = new WSI.Cashier.Controls.uc_label();
      this.lbl_min_bet_value = new WSI.Cashier.Controls.uc_label();
      this.cb_table_speed = new WSI.Cashier.Controls.uc_round_combobox();
      this.btn_legend = new WSI.Cashier.Controls.uc_round_button();
      this.btn_play_pause = new WSI.Cashier.Controls.uc_round_button();
      this.lbl_max_bet_name = new WSI.Cashier.Controls.uc_label();
      this.lbl_speed = new WSI.Cashier.Controls.uc_label();
      this.uc_scrollable_button_list1 = new WSI.Cashier.uc_scrollable_button_list();
      this.cb_banks = new WSI.Cashier.Controls.uc_round_combobox();
      this.lbl_change_position = new WSI.Cashier.Controls.uc_label();
      this.lbl_info_exit = new WSI.Cashier.Controls.uc_label();
      this.lbl_info_pause = new WSI.Cashier.Controls.uc_label();
      this.lbl_info_free = new WSI.Cashier.Controls.uc_label();
      this.lbl_info_away = new WSI.Cashier.Controls.uc_label();
      this.label2 = new WSI.Cashier.Controls.uc_label();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.pb_exit)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.pb_pause)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.pb_change_seat)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.pb_free)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.pb_ocupated)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.pb_away)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.pb_disabled)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.dgv_gt_movememnts)).BeginInit();
      this.pnl_information.SuspendLayout();
      this.tlp_information.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pb_legend_disabled_seat)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.pb_legend_resume_session)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.pb_legend_end_session)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.pb_legend_pause_session)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.pb_legend_swap_position)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.pb_legend_free_seat)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.pb_legend_taken_seat)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.pb_legend_away_player)).BeginInit();
      this.pnl_report.SuspendLayout();
      this.pnl_container.SuspendLayout();
      this.rp_legend_movements.SuspendLayout();
      this.tlp_legend_movements.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pb_legend_amount)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.pb_legend_chip_nre)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.pb_legend_chip_color)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.pb_legend_chip_re)).BeginInit();
      this.gamingTablePanel1.SuspendLayout();
      this.pnl_bottom.SuspendLayout();
      this.pnl_menu_table_mode.SuspendLayout();
      this.SuspendLayout();
      // 
      // pictureBox4
      // 
      this.pictureBox4.Dock = System.Windows.Forms.DockStyle.Left;
      this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
      this.pictureBox4.Location = new System.Drawing.Point(3, 207);
      this.pictureBox4.Name = "pictureBox4";
      this.pictureBox4.Size = new System.Drawing.Size(49, 45);
      this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
      this.pictureBox4.TabIndex = 3;
      this.pictureBox4.TabStop = false;
      // 
      // pictureBox5
      // 
      this.pictureBox5.Dock = System.Windows.Forms.DockStyle.Left;
      this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
      this.pictureBox5.Location = new System.Drawing.Point(3, 258);
      this.pictureBox5.Name = "pictureBox5";
      this.pictureBox5.Size = new System.Drawing.Size(49, 45);
      this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
      this.pictureBox5.TabIndex = 4;
      this.pictureBox5.TabStop = false;
      // 
      // pictureBox6
      // 
      this.pictureBox6.Dock = System.Windows.Forms.DockStyle.Left;
      this.pictureBox6.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox6.Image")));
      this.pictureBox6.Location = new System.Drawing.Point(3, 309);
      this.pictureBox6.Name = "pictureBox6";
      this.pictureBox6.Size = new System.Drawing.Size(49, 45);
      this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
      this.pictureBox6.TabIndex = 5;
      this.pictureBox6.TabStop = false;
      // 
      // pb_exit
      // 
      this.pb_exit.Dock = System.Windows.Forms.DockStyle.Left;
      this.pb_exit.Image = ((System.Drawing.Image)(resources.GetObject("pb_exit.Image")));
      this.pb_exit.Location = new System.Drawing.Point(3, 3);
      this.pb_exit.Name = "pb_exit";
      this.pb_exit.Size = new System.Drawing.Size(44, 44);
      this.pb_exit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
      this.pb_exit.TabIndex = 0;
      this.pb_exit.TabStop = false;
      // 
      // pb_pause
      // 
      this.pb_pause.Dock = System.Windows.Forms.DockStyle.Left;
      this.pb_pause.Image = ((System.Drawing.Image)(resources.GetObject("pb_pause.Image")));
      this.pb_pause.Location = new System.Drawing.Point(3, 103);
      this.pb_pause.Name = "pb_pause";
      this.pb_pause.Size = new System.Drawing.Size(44, 44);
      this.pb_pause.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
      this.pb_pause.TabIndex = 1;
      this.pb_pause.TabStop = false;
      // 
      // pb_change_seat
      // 
      this.pb_change_seat.Dock = System.Windows.Forms.DockStyle.Left;
      this.pb_change_seat.Image = ((System.Drawing.Image)(resources.GetObject("pb_change_seat.Image")));
      this.pb_change_seat.Location = new System.Drawing.Point(3, 153);
      this.pb_change_seat.Name = "pb_change_seat";
      this.pb_change_seat.Size = new System.Drawing.Size(44, 44);
      this.pb_change_seat.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
      this.pb_change_seat.TabIndex = 2;
      this.pb_change_seat.TabStop = false;
      // 
      // pb_free
      // 
      this.pb_free.Dock = System.Windows.Forms.DockStyle.Left;
      this.pb_free.Image = ((System.Drawing.Image)(resources.GetObject("pb_free.Image")));
      this.pb_free.Location = new System.Drawing.Point(3, 203);
      this.pb_free.Name = "pb_free";
      this.pb_free.Size = new System.Drawing.Size(44, 44);
      this.pb_free.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
      this.pb_free.TabIndex = 3;
      this.pb_free.TabStop = false;
      // 
      // pb_ocupated
      // 
      this.pb_ocupated.Dock = System.Windows.Forms.DockStyle.Left;
      this.pb_ocupated.Image = ((System.Drawing.Image)(resources.GetObject("pb_ocupated.Image")));
      this.pb_ocupated.Location = new System.Drawing.Point(3, 253);
      this.pb_ocupated.Name = "pb_ocupated";
      this.pb_ocupated.Size = new System.Drawing.Size(44, 44);
      this.pb_ocupated.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
      this.pb_ocupated.TabIndex = 4;
      this.pb_ocupated.TabStop = false;
      // 
      // pb_away
      // 
      this.pb_away.Dock = System.Windows.Forms.DockStyle.Left;
      this.pb_away.Image = ((System.Drawing.Image)(resources.GetObject("pb_away.Image")));
      this.pb_away.Location = new System.Drawing.Point(3, 303);
      this.pb_away.Name = "pb_away";
      this.pb_away.Size = new System.Drawing.Size(44, 44);
      this.pb_away.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
      this.pb_away.TabIndex = 5;
      this.pb_away.TabStop = false;
      // 
      // pb_disabled
      // 
      this.pb_disabled.Dock = System.Windows.Forms.DockStyle.Left;
      this.pb_disabled.Image = ((System.Drawing.Image)(resources.GetObject("pb_disabled.Image")));
      this.pb_disabled.Location = new System.Drawing.Point(3, 353);
      this.pb_disabled.Name = "pb_disabled";
      this.pb_disabled.Size = new System.Drawing.Size(44, 44);
      this.pb_disabled.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
      this.pb_disabled.TabIndex = 6;
      this.pb_disabled.TabStop = false;
      // 
      // transparentPanel1
      // 
      this.transparentPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.transparentPanel1.BackColor = System.Drawing.Color.DimGray;
      this.transparentPanel1.Image = null;
      this.transparentPanel1.Location = new System.Drawing.Point(9, 52);
      this.transparentPanel1.Name = "transparentPanel1";
      this.transparentPanel1.Opacity = 128;
      this.transparentPanel1.Size = new System.Drawing.Size(930, 625);
      this.transparentPanel1.TabIndex = 76;
      this.transparentPanel1.ButtonClick += new WSI.Common.uc_transparent_panel.ButtonClickEventHandler(this.transparentPanel1_ButtonClick);
      // 
      // dgv_gt_movememnts
      // 
      this.dgv_gt_movememnts.AllowUserToAddRows = false;
      this.dgv_gt_movememnts.AllowUserToDeleteRows = false;
      this.dgv_gt_movememnts.AllowUserToResizeColumns = false;
      this.dgv_gt_movememnts.AllowUserToResizeRows = false;
      this.dgv_gt_movememnts.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.dgv_gt_movememnts.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
      this.dgv_gt_movememnts.BackgroundColor = System.Drawing.SystemColors.Control;
      this.dgv_gt_movememnts.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.dgv_gt_movememnts.ColumnHeaderHeight = 35;
      this.dgv_gt_movememnts.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
      dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      dataGridViewCellStyle1.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
      dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
      dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
      this.dgv_gt_movememnts.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
      this.dgv_gt_movememnts.ColumnHeadersHeight = 35;
      this.dgv_gt_movememnts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
      this.dgv_gt_movememnts.CornerRadius = 10;
      this.dgv_gt_movememnts.DefaultCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.dgv_gt_movememnts.DefaultCellForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.dgv_gt_movememnts.DefaultCellSelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.dgv_gt_movememnts.DefaultCellSelectionForeColor = System.Drawing.Color.White;
      dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      dataGridViewCellStyle2.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.White;
      dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
      this.dgv_gt_movememnts.DefaultCellStyle = dataGridViewCellStyle2;
      this.dgv_gt_movememnts.EnableHeadersVisualStyles = false;
      this.dgv_gt_movememnts.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.dgv_gt_movememnts.GridColor = System.Drawing.Color.LightGray;
      this.dgv_gt_movememnts.HeaderBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
      this.dgv_gt_movememnts.HeaderDefaultCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.dgv_gt_movememnts.HeaderDefaultCellForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.dgv_gt_movememnts.HeaderImages = null;
      this.dgv_gt_movememnts.Location = new System.Drawing.Point(555, 53);
      this.dgv_gt_movememnts.Name = "dgv_gt_movememnts";
      this.dgv_gt_movememnts.ReadOnly = true;
      this.dgv_gt_movememnts.RowHeadersVisible = false;
      this.dgv_gt_movememnts.RowHeadersWidth = 40;
      this.dgv_gt_movememnts.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
      this.dgv_gt_movememnts.RowTemplate.Height = 40;
      this.dgv_gt_movememnts.RowTemplateHeight = 35;
      this.dgv_gt_movememnts.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
      this.dgv_gt_movememnts.Size = new System.Drawing.Size(382, 495);
      this.dgv_gt_movememnts.Style = WSI.Cashier.Controls.uc_DataGridView.DataGridViewStyle.WITH_ALL_ROUND_CORNERS;
      this.dgv_gt_movememnts.TabIndex = 79;
      // 
      // pnl_information
      // 
      this.pnl_information.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.pnl_information.BackColor = System.Drawing.Color.Transparent;
      this.pnl_information.BorderColor = System.Drawing.Color.Empty;
      this.pnl_information.Controls.Add(this.tlp_information);
      this.pnl_information.CornerRadius = 10;
      this.pnl_information.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.pnl_information.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.pnl_information.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.pnl_information.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.pnl_information.HeaderHeight = 18;
      this.pnl_information.HeaderSubText = null;
      this.pnl_information.HeaderText = "LEYENDA";
      this.pnl_information.Location = new System.Drawing.Point(801, 297);
      this.pnl_information.Name = "pnl_information";
      this.pnl_information.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.pnl_information.Size = new System.Drawing.Size(138, 379);
      this.pnl_information.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.MIN_HEAD;
      this.pnl_information.TabIndex = 0;
      this.pnl_information.Visible = false;
      // 
      // tlp_information
      // 
      this.tlp_information.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.tlp_information.BackColor = System.Drawing.Color.Transparent;
      this.tlp_information.ColumnCount = 2;
      this.tlp_information.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
      this.tlp_information.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.tlp_information.Controls.Add(this.pb_legend_disabled_seat, 0, 7);
      this.tlp_information.Controls.Add(this.pb_legend_resume_session, 0, 1);
      this.tlp_information.Controls.Add(this.pb_legend_end_session, 0, 0);
      this.tlp_information.Controls.Add(this.pb_legend_pause_session, 0, 2);
      this.tlp_information.Controls.Add(this.pb_legend_swap_position, 0, 3);
      this.tlp_information.Controls.Add(this.pb_legend_free_seat, 0, 4);
      this.tlp_information.Controls.Add(this.pb_legend_taken_seat, 0, 5);
      this.tlp_information.Controls.Add(this.pb_legend_away_player, 0, 6);
      this.tlp_information.Controls.Add(this.lbl_legend_end_session, 1, 0);
      this.tlp_information.Controls.Add(this.lbl_legend_resume_session, 1, 1);
      this.tlp_information.Controls.Add(this.lbl_legend_pause_session, 1, 2);
      this.tlp_information.Controls.Add(this.lbl_legend_swap_position, 1, 3);
      this.tlp_information.Controls.Add(this.lbl_legend_free_seat, 1, 4);
      this.tlp_information.Controls.Add(this.lbl_legend_taken_seat, 1, 5);
      this.tlp_information.Controls.Add(this.lbl_legend_away_player, 1, 6);
      this.tlp_information.Controls.Add(this.lbl_legend_disabled_seat, 1, 7);
      this.tlp_information.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.tlp_information.Location = new System.Drawing.Point(1, 19);
      this.tlp_information.Name = "tlp_information";
      this.tlp_information.RowCount = 8;
      this.tlp_information.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
      this.tlp_information.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
      this.tlp_information.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
      this.tlp_information.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
      this.tlp_information.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
      this.tlp_information.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
      this.tlp_information.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
      this.tlp_information.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
      this.tlp_information.Size = new System.Drawing.Size(137, 357);
      this.tlp_information.TabIndex = 0;
      // 
      // pb_legend_disabled_seat
      // 
      this.pb_legend_disabled_seat.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.pb_legend_disabled_seat.Image = ((System.Drawing.Image)(resources.GetObject("pb_legend_disabled_seat.Image")));
      this.pb_legend_disabled_seat.Location = new System.Drawing.Point(3, 318);
      this.pb_legend_disabled_seat.Name = "pb_legend_disabled_seat";
      this.pb_legend_disabled_seat.Size = new System.Drawing.Size(39, 39);
      this.pb_legend_disabled_seat.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
      this.pb_legend_disabled_seat.TabIndex = 6;
      this.pb_legend_disabled_seat.TabStop = false;
      // 
      // pb_legend_resume_session
      // 
      this.pb_legend_resume_session.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.pb_legend_resume_session.Image = ((System.Drawing.Image)(resources.GetObject("pb_legend_resume_session.Image")));
      this.pb_legend_resume_session.Location = new System.Drawing.Point(3, 48);
      this.pb_legend_resume_session.Name = "pb_legend_resume_session";
      this.pb_legend_resume_session.Size = new System.Drawing.Size(39, 39);
      this.pb_legend_resume_session.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
      this.pb_legend_resume_session.TabIndex = 0;
      this.pb_legend_resume_session.TabStop = false;
      // 
      // pb_legend_end_session
      // 
      this.pb_legend_end_session.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.pb_legend_end_session.Image = ((System.Drawing.Image)(resources.GetObject("pb_legend_end_session.Image")));
      this.pb_legend_end_session.Location = new System.Drawing.Point(3, 3);
      this.pb_legend_end_session.Name = "pb_legend_end_session";
      this.pb_legend_end_session.Size = new System.Drawing.Size(39, 39);
      this.pb_legend_end_session.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
      this.pb_legend_end_session.TabIndex = 0;
      this.pb_legend_end_session.TabStop = false;
      // 
      // pb_legend_pause_session
      // 
      this.pb_legend_pause_session.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.pb_legend_pause_session.Image = ((System.Drawing.Image)(resources.GetObject("pb_legend_pause_session.Image")));
      this.pb_legend_pause_session.Location = new System.Drawing.Point(3, 93);
      this.pb_legend_pause_session.Name = "pb_legend_pause_session";
      this.pb_legend_pause_session.Size = new System.Drawing.Size(39, 39);
      this.pb_legend_pause_session.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
      this.pb_legend_pause_session.TabIndex = 1;
      this.pb_legend_pause_session.TabStop = false;
      // 
      // pb_legend_swap_position
      // 
      this.pb_legend_swap_position.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.pb_legend_swap_position.Image = ((System.Drawing.Image)(resources.GetObject("pb_legend_swap_position.Image")));
      this.pb_legend_swap_position.Location = new System.Drawing.Point(3, 138);
      this.pb_legend_swap_position.Name = "pb_legend_swap_position";
      this.pb_legend_swap_position.Size = new System.Drawing.Size(39, 39);
      this.pb_legend_swap_position.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
      this.pb_legend_swap_position.TabIndex = 2;
      this.pb_legend_swap_position.TabStop = false;
      // 
      // pb_legend_free_seat
      // 
      this.pb_legend_free_seat.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.pb_legend_free_seat.Image = ((System.Drawing.Image)(resources.GetObject("pb_legend_free_seat.Image")));
      this.pb_legend_free_seat.InitialImage = null;
      this.pb_legend_free_seat.Location = new System.Drawing.Point(3, 183);
      this.pb_legend_free_seat.Name = "pb_legend_free_seat";
      this.pb_legend_free_seat.Size = new System.Drawing.Size(39, 39);
      this.pb_legend_free_seat.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
      this.pb_legend_free_seat.TabIndex = 3;
      this.pb_legend_free_seat.TabStop = false;
      // 
      // pb_legend_taken_seat
      // 
      this.pb_legend_taken_seat.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.pb_legend_taken_seat.Image = ((System.Drawing.Image)(resources.GetObject("pb_legend_taken_seat.Image")));
      this.pb_legend_taken_seat.Location = new System.Drawing.Point(3, 228);
      this.pb_legend_taken_seat.Name = "pb_legend_taken_seat";
      this.pb_legend_taken_seat.Size = new System.Drawing.Size(39, 39);
      this.pb_legend_taken_seat.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
      this.pb_legend_taken_seat.TabIndex = 4;
      this.pb_legend_taken_seat.TabStop = false;
      // 
      // pb_legend_away_player
      // 
      this.pb_legend_away_player.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.pb_legend_away_player.Image = ((System.Drawing.Image)(resources.GetObject("pb_legend_away_player.Image")));
      this.pb_legend_away_player.Location = new System.Drawing.Point(3, 273);
      this.pb_legend_away_player.Name = "pb_legend_away_player";
      this.pb_legend_away_player.Size = new System.Drawing.Size(39, 39);
      this.pb_legend_away_player.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
      this.pb_legend_away_player.TabIndex = 5;
      this.pb_legend_away_player.TabStop = false;
      // 
      // lbl_legend_end_session
      // 
      this.lbl_legend_end_session.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.lbl_legend_end_session.AutoSize = true;
      this.lbl_legend_end_session.BackColor = System.Drawing.Color.Transparent;
      this.lbl_legend_end_session.Font = new System.Drawing.Font("Open Sans Semibold", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_legend_end_session.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_legend_end_session.Location = new System.Drawing.Point(57, 15);
      this.lbl_legend_end_session.Name = "lbl_legend_end_session";
      this.lbl_legend_end_session.Size = new System.Drawing.Size(67, 14);
      this.lbl_legend_end_session.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_10PX_BLACK;
      this.lbl_legend_end_session.TabIndex = 7;
      this.lbl_legend_end_session.Text = "xEndSession";
      this.lbl_legend_end_session.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lbl_legend_resume_session
      // 
      this.lbl_legend_resume_session.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.lbl_legend_resume_session.AutoSize = true;
      this.lbl_legend_resume_session.BackColor = System.Drawing.Color.Transparent;
      this.lbl_legend_resume_session.Font = new System.Drawing.Font("Open Sans Semibold", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_legend_resume_session.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_legend_resume_session.Location = new System.Drawing.Point(50, 53);
      this.lbl_legend_resume_session.Name = "lbl_legend_resume_session";
      this.lbl_legend_resume_session.Size = new System.Drawing.Size(82, 28);
      this.lbl_legend_resume_session.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_10PX_BLACK;
      this.lbl_legend_resume_session.TabIndex = 8;
      this.lbl_legend_resume_session.Text = "xResumeSession";
      this.lbl_legend_resume_session.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lbl_legend_pause_session
      // 
      this.lbl_legend_pause_session.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.lbl_legend_pause_session.AutoSize = true;
      this.lbl_legend_pause_session.BackColor = System.Drawing.Color.Transparent;
      this.lbl_legend_pause_session.Font = new System.Drawing.Font("Open Sans Semibold", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_legend_pause_session.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_legend_pause_session.Location = new System.Drawing.Point(52, 105);
      this.lbl_legend_pause_session.Name = "lbl_legend_pause_session";
      this.lbl_legend_pause_session.Size = new System.Drawing.Size(78, 14);
      this.lbl_legend_pause_session.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_10PX_BLACK;
      this.lbl_legend_pause_session.TabIndex = 9;
      this.lbl_legend_pause_session.Text = "xPauseSession";
      this.lbl_legend_pause_session.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lbl_legend_swap_position
      // 
      this.lbl_legend_swap_position.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.lbl_legend_swap_position.AutoSize = true;
      this.lbl_legend_swap_position.BackColor = System.Drawing.Color.Transparent;
      this.lbl_legend_swap_position.Font = new System.Drawing.Font("Open Sans Semibold", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_legend_swap_position.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_legend_swap_position.Location = new System.Drawing.Point(52, 150);
      this.lbl_legend_swap_position.Name = "lbl_legend_swap_position";
      this.lbl_legend_swap_position.Size = new System.Drawing.Size(77, 14);
      this.lbl_legend_swap_position.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_10PX_BLACK;
      this.lbl_legend_swap_position.TabIndex = 10;
      this.lbl_legend_swap_position.Text = "xSwapPosition";
      this.lbl_legend_swap_position.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lbl_legend_free_seat
      // 
      this.lbl_legend_free_seat.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.lbl_legend_free_seat.AutoSize = true;
      this.lbl_legend_free_seat.BackColor = System.Drawing.Color.Transparent;
      this.lbl_legend_free_seat.Font = new System.Drawing.Font("Open Sans Semibold", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_legend_free_seat.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_legend_free_seat.Location = new System.Drawing.Point(63, 195);
      this.lbl_legend_free_seat.Name = "lbl_legend_free_seat";
      this.lbl_legend_free_seat.Size = new System.Drawing.Size(55, 14);
      this.lbl_legend_free_seat.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_10PX_BLACK;
      this.lbl_legend_free_seat.TabIndex = 11;
      this.lbl_legend_free_seat.Text = "xFreeSeat";
      this.lbl_legend_free_seat.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lbl_legend_taken_seat
      // 
      this.lbl_legend_taken_seat.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.lbl_legend_taken_seat.AutoSize = true;
      this.lbl_legend_taken_seat.BackColor = System.Drawing.Color.Transparent;
      this.lbl_legend_taken_seat.Font = new System.Drawing.Font("Open Sans Semibold", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_legend_taken_seat.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_legend_taken_seat.Location = new System.Drawing.Point(59, 240);
      this.lbl_legend_taken_seat.Name = "lbl_legend_taken_seat";
      this.lbl_legend_taken_seat.Size = new System.Drawing.Size(64, 14);
      this.lbl_legend_taken_seat.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_10PX_BLACK;
      this.lbl_legend_taken_seat.TabIndex = 12;
      this.lbl_legend_taken_seat.Text = "xTakenSeat";
      this.lbl_legend_taken_seat.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lbl_legend_away_player
      // 
      this.lbl_legend_away_player.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.lbl_legend_away_player.AutoSize = true;
      this.lbl_legend_away_player.BackColor = System.Drawing.Color.Transparent;
      this.lbl_legend_away_player.Font = new System.Drawing.Font("Open Sans Semibold", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_legend_away_player.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_legend_away_player.Location = new System.Drawing.Point(56, 285);
      this.lbl_legend_away_player.Name = "lbl_legend_away_player";
      this.lbl_legend_away_player.Size = new System.Drawing.Size(69, 14);
      this.lbl_legend_away_player.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_10PX_BLACK;
      this.lbl_legend_away_player.TabIndex = 13;
      this.lbl_legend_away_player.Text = "xAwayPlayer";
      this.lbl_legend_away_player.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lbl_legend_disabled_seat
      // 
      this.lbl_legend_disabled_seat.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.lbl_legend_disabled_seat.AutoSize = true;
      this.lbl_legend_disabled_seat.BackColor = System.Drawing.Color.Transparent;
      this.lbl_legend_disabled_seat.Font = new System.Drawing.Font("Open Sans Semibold", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_legend_disabled_seat.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_legend_disabled_seat.Location = new System.Drawing.Point(53, 330);
      this.lbl_legend_disabled_seat.Name = "lbl_legend_disabled_seat";
      this.lbl_legend_disabled_seat.Size = new System.Drawing.Size(76, 14);
      this.lbl_legend_disabled_seat.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_10PX_BLACK;
      this.lbl_legend_disabled_seat.TabIndex = 14;
      this.lbl_legend_disabled_seat.Text = "xDisabledSeat";
      this.lbl_legend_disabled_seat.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // pnl_report
      // 
      this.pnl_report.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.pnl_report.AutoScroll = true;
      this.pnl_report.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
      this.pnl_report.BackColor = System.Drawing.Color.Transparent;
      this.pnl_report.BorderColor = System.Drawing.Color.Empty;
      this.pnl_report.Controls.Add(this.webBrowserGtSessionInfo);
      this.pnl_report.CornerRadius = 10;
      this.pnl_report.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.pnl_report.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.pnl_report.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.pnl_report.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.pnl_report.HeaderHeight = 0;
      this.pnl_report.HeaderSubText = null;
      this.pnl_report.HeaderText = null;
      this.pnl_report.Location = new System.Drawing.Point(86, 379);
      this.pnl_report.Name = "pnl_report";
      this.pnl_report.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.pnl_report.Size = new System.Drawing.Size(398, 240);
      this.pnl_report.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.WITHOUT_HEAD;
      this.pnl_report.TabIndex = 5;
      // 
      // webBrowserGtSessionInfo
      // 
      this.webBrowserGtSessionInfo.AllowWebBrowserDrop = false;
      this.webBrowserGtSessionInfo.AutoSize = true;
      this.webBrowserGtSessionInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.webBrowserGtSessionInfo.Dock = System.Windows.Forms.DockStyle.Fill;
      this.webBrowserGtSessionInfo.Location = new System.Drawing.Point(0, 0);
      this.webBrowserGtSessionInfo.Margin = new System.Windows.Forms.Padding(0);
      this.webBrowserGtSessionInfo.MinimumSize = new System.Drawing.Size(100, 99);
      this.webBrowserGtSessionInfo.Name = "webBrowserGtSessionInfo";
      this.webBrowserGtSessionInfo.Size = new System.Drawing.Size(398, 240);
      this.webBrowserGtSessionInfo.TabIndex = 2;
      this.webBrowserGtSessionInfo.WebBrowserShortcutsEnabled = false;
      // 
      // pnl_table_options
      // 
      this.pnl_table_options.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.pnl_table_options.AutoSize = true;
      this.pnl_table_options.BackColor = System.Drawing.Color.CornflowerBlue;
      this.pnl_table_options.BorderColor = System.Drawing.Color.Empty;
      this.pnl_table_options.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.pnl_table_options.CornerRadius = 1;
      this.pnl_table_options.HeaderBackColor = System.Drawing.Color.Empty;
      this.pnl_table_options.HeaderForeColor = System.Drawing.Color.Empty;
      this.pnl_table_options.HeaderHeight = 0;
      this.pnl_table_options.HeaderSubText = null;
      this.pnl_table_options.HeaderText = null;
      this.pnl_table_options.Location = new System.Drawing.Point(10, 651);
      this.pnl_table_options.Margin = new System.Windows.Forms.Padding(0);
      this.pnl_table_options.Name = "pnl_table_options";
      this.pnl_table_options.PanelBackColor = System.Drawing.Color.Empty;
      this.pnl_table_options.Size = new System.Drawing.Size(95, 29);
      this.pnl_table_options.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NONE;
      this.pnl_table_options.TabIndex = 78;
      this.pnl_table_options.Visible = false;
      // 
      // pnl_container
      // 
      this.pnl_container.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.pnl_container.BackColor = System.Drawing.Color.Transparent;
      this.pnl_container.BorderColor = System.Drawing.Color.Empty;
      this.pnl_container.Controls.Add(this.rp_legend_movements);
      this.pnl_container.Controls.Add(this.lbl_name_and_data_open_session);
      this.pnl_container.Controls.Add(this.uc_gt_player_in_session1);
      this.pnl_container.Controls.Add(this.gamingTablePanel1);
      this.pnl_container.Controls.Add(this.uc_gt_card_reader1);
      this.pnl_container.CornerRadius = 1;
      this.pnl_container.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.pnl_container.HeaderBackColor = System.Drawing.Color.Empty;
      this.pnl_container.HeaderForeColor = System.Drawing.Color.Empty;
      this.pnl_container.HeaderHeight = 0;
      this.pnl_container.HeaderSubText = null;
      this.pnl_container.HeaderText = null;
      this.pnl_container.Location = new System.Drawing.Point(9, 52);
      this.pnl_container.Name = "pnl_container";
      this.pnl_container.PanelBackColor = System.Drawing.Color.Transparent;
      this.pnl_container.Size = new System.Drawing.Size(930, 625);
      this.pnl_container.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NONE;
      this.pnl_container.TabIndex = 77;
      // 
      // rp_legend_movements
      // 
      this.rp_legend_movements.BackColor = System.Drawing.Color.Transparent;
      this.rp_legend_movements.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.rp_legend_movements.Controls.Add(this.tlp_legend_movements);
      this.rp_legend_movements.CornerRadius = 10;
      this.rp_legend_movements.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.rp_legend_movements.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.rp_legend_movements.HeaderBackColor = System.Drawing.Color.Empty;
      this.rp_legend_movements.HeaderForeColor = System.Drawing.Color.Empty;
      this.rp_legend_movements.HeaderHeight = 0;
      this.rp_legend_movements.HeaderSubText = null;
      this.rp_legend_movements.HeaderText = null;
      this.rp_legend_movements.Location = new System.Drawing.Point(692, 12);
      this.rp_legend_movements.Name = "rp_legend_movements";
      this.rp_legend_movements.PanelBackColor = System.Drawing.Color.Transparent;
      this.rp_legend_movements.Size = new System.Drawing.Size(230, 117);
      this.rp_legend_movements.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NONE;
      this.rp_legend_movements.TabIndex = 80;
      this.rp_legend_movements.Visible = false;
      // 
      // tlp_legend_movements
      // 
      this.tlp_legend_movements.BackColor = System.Drawing.Color.Transparent;
      this.tlp_legend_movements.ColumnCount = 4;
      this.tlp_legend_movements.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16F));
      this.tlp_legend_movements.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 34F));
      this.tlp_legend_movements.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16F));
      this.tlp_legend_movements.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 34F));
      this.tlp_legend_movements.Controls.Add(this.pb_legend_amount, 2, 1);
      this.tlp_legend_movements.Controls.Add(this.pb_legend_chip_nre, 0, 1);
      this.tlp_legend_movements.Controls.Add(this.pb_legend_chip_color, 2, 0);
      this.tlp_legend_movements.Controls.Add(this.lbl_legend_chip_nre, 1, 1);
      this.tlp_legend_movements.Controls.Add(this.lbl_legend_chip_color, 3, 0);
      this.tlp_legend_movements.Controls.Add(this.lbl_legend_amount, 3, 1);
      this.tlp_legend_movements.Controls.Add(this.pb_legend_chip_re, 0, 0);
      this.tlp_legend_movements.Controls.Add(this.lbl_legend_chip_re, 1, 0);
      this.tlp_legend_movements.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tlp_legend_movements.Location = new System.Drawing.Point(0, 0);
      this.tlp_legend_movements.Name = "tlp_legend_movements";
      this.tlp_legend_movements.RowCount = 2;
      this.tlp_legend_movements.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.tlp_legend_movements.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.tlp_legend_movements.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
      this.tlp_legend_movements.Size = new System.Drawing.Size(230, 117);
      this.tlp_legend_movements.TabIndex = 82;
      // 
      // pb_legend_amount
      // 
      this.pb_legend_amount.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pb_legend_amount.Location = new System.Drawing.Point(120, 64);
      this.pb_legend_amount.Margin = new System.Windows.Forms.Padding(6);
      this.pb_legend_amount.Name = "pb_legend_amount";
      this.pb_legend_amount.Size = new System.Drawing.Size(24, 47);
      this.pb_legend_amount.TabIndex = 7;
      this.pb_legend_amount.TabStop = false;
      // 
      // pb_legend_chip_nre
      // 
      this.pb_legend_chip_nre.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pb_legend_chip_nre.Location = new System.Drawing.Point(6, 64);
      this.pb_legend_chip_nre.Margin = new System.Windows.Forms.Padding(6);
      this.pb_legend_chip_nre.Name = "pb_legend_chip_nre";
      this.pb_legend_chip_nre.Size = new System.Drawing.Size(24, 47);
      this.pb_legend_chip_nre.TabIndex = 6;
      this.pb_legend_chip_nre.TabStop = false;
      // 
      // pb_legend_chip_color
      // 
      this.pb_legend_chip_color.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pb_legend_chip_color.Location = new System.Drawing.Point(120, 6);
      this.pb_legend_chip_color.Margin = new System.Windows.Forms.Padding(6);
      this.pb_legend_chip_color.Name = "pb_legend_chip_color";
      this.pb_legend_chip_color.Size = new System.Drawing.Size(24, 46);
      this.pb_legend_chip_color.TabIndex = 5;
      this.pb_legend_chip_color.TabStop = false;
      // 
      // lbl_legend_chip_nre
      // 
      this.lbl_legend_chip_nre.AutoSize = true;
      this.lbl_legend_chip_nre.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_legend_chip_nre.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_legend_chip_nre.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_legend_chip_nre.Location = new System.Drawing.Point(39, 58);
      this.lbl_legend_chip_nre.Name = "lbl_legend_chip_nre";
      this.lbl_legend_chip_nre.Size = new System.Drawing.Size(72, 59);
      this.lbl_legend_chip_nre.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_legend_chip_nre.TabIndex = 1;
      this.lbl_legend_chip_nre.Text = "xChipNRE";
      this.lbl_legend_chip_nre.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_legend_chip_color
      // 
      this.lbl_legend_chip_color.AutoSize = true;
      this.lbl_legend_chip_color.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_legend_chip_color.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_legend_chip_color.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_legend_chip_color.Location = new System.Drawing.Point(153, 0);
      this.lbl_legend_chip_color.Name = "lbl_legend_chip_color";
      this.lbl_legend_chip_color.Size = new System.Drawing.Size(74, 58);
      this.lbl_legend_chip_color.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_legend_chip_color.TabIndex = 2;
      this.lbl_legend_chip_color.Text = "xChipColor";
      this.lbl_legend_chip_color.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_legend_amount
      // 
      this.lbl_legend_amount.AutoSize = true;
      this.lbl_legend_amount.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_legend_amount.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_legend_amount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_legend_amount.Location = new System.Drawing.Point(153, 58);
      this.lbl_legend_amount.Name = "lbl_legend_amount";
      this.lbl_legend_amount.Size = new System.Drawing.Size(74, 59);
      this.lbl_legend_amount.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_legend_amount.TabIndex = 3;
      this.lbl_legend_amount.Text = "xAmount";
      this.lbl_legend_amount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // pb_legend_chip_re
      // 
      this.pb_legend_chip_re.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pb_legend_chip_re.Location = new System.Drawing.Point(6, 6);
      this.pb_legend_chip_re.Margin = new System.Windows.Forms.Padding(6);
      this.pb_legend_chip_re.Name = "pb_legend_chip_re";
      this.pb_legend_chip_re.Size = new System.Drawing.Size(24, 46);
      this.pb_legend_chip_re.TabIndex = 4;
      this.pb_legend_chip_re.TabStop = false;
      // 
      // lbl_legend_chip_re
      // 
      this.lbl_legend_chip_re.AutoSize = true;
      this.lbl_legend_chip_re.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_legend_chip_re.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_legend_chip_re.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_legend_chip_re.Location = new System.Drawing.Point(39, 0);
      this.lbl_legend_chip_re.Name = "lbl_legend_chip_re";
      this.lbl_legend_chip_re.Size = new System.Drawing.Size(72, 58);
      this.lbl_legend_chip_re.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_legend_chip_re.TabIndex = 0;
      this.lbl_legend_chip_re.Text = "xChipRe";
      this.lbl_legend_chip_re.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_name_and_data_open_session
      // 
      this.lbl_name_and_data_open_session.BackColor = System.Drawing.Color.Transparent;
      this.lbl_name_and_data_open_session.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_name_and_data_open_session.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_name_and_data_open_session.Location = new System.Drawing.Point(301, 193);
      this.lbl_name_and_data_open_session.Margin = new System.Windows.Forms.Padding(0);
      this.lbl_name_and_data_open_session.Name = "lbl_name_and_data_open_session";
      this.lbl_name_and_data_open_session.Size = new System.Drawing.Size(211, 70);
      this.lbl_name_and_data_open_session.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.lbl_name_and_data_open_session.TabIndex = 83;
      this.lbl_name_and_data_open_session.Text = "xNameAndDataOpenSession";
      this.lbl_name_and_data_open_session.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.lbl_name_and_data_open_session.Visible = false;
      // 
      // uc_gt_player_in_session1
      // 
      this.uc_gt_player_in_session1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.uc_gt_player_in_session1.BackColor = System.Drawing.Color.Transparent;
      this.uc_gt_player_in_session1.ChipsSell = new decimal(new int[] {
            0,
            0,
            0,
            131072});
      this.uc_gt_player_in_session1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.uc_gt_player_in_session1.Location = new System.Drawing.Point(1, 0);
      this.uc_gt_player_in_session1.Margin = new System.Windows.Forms.Padding(2);
      this.uc_gt_player_in_session1.Name = "uc_gt_player_in_session1";
      this.uc_gt_player_in_session1.PointsWon = new decimal(new int[] {
            0,
            0,
            0,
            0});
      this.uc_gt_player_in_session1.Seat = null;
      this.uc_gt_player_in_session1.Size = new System.Drawing.Size(928, 217);
      this.uc_gt_player_in_session1.TabIndex = 2;
      this.uc_gt_player_in_session1.TableIdlePlays = 0;
      // 
      // gamingTablePanel1
      // 
      this.gamingTablePanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.gamingTablePanel1.AutoScroll = true;
      this.gamingTablePanel1.AutoSize = true;
      this.gamingTablePanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.gamingTablePanel1.BackgroundPicture = null;
      this.gamingTablePanel1.CanMoveSeats = false;
      this.gamingTablePanel1.Controls.Add(this.uc_gt_dealer_copy_ticket_reader1);
      this.gamingTablePanel1.Controls.Add(this.uc_input_amount_resizable1);
      this.gamingTablePanel1.DoubleBufferred = true;
      this.gamingTablePanel1.Location = new System.Drawing.Point(0, 219);
      this.gamingTablePanel1.Margin = new System.Windows.Forms.Padding(0);
      this.gamingTablePanel1.ModeDesign = false;
      this.gamingTablePanel1.Name = "gamingTablePanel1";
      this.gamingTablePanel1.ReadOnly = false;
      this.gamingTablePanel1.Size = new System.Drawing.Size(933, 404);
      this.gamingTablePanel1.TabIndex = 4;
      // 
      // uc_gt_dealer_copy_ticket_reader1
      // 
      this.uc_gt_dealer_copy_ticket_reader1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.uc_gt_dealer_copy_ticket_reader1.Location = new System.Drawing.Point(102, 26);
      this.uc_gt_dealer_copy_ticket_reader1.Name = "uc_gt_dealer_copy_ticket_reader1";
      this.uc_gt_dealer_copy_ticket_reader1.Size = new System.Drawing.Size(282, 287);
      this.uc_gt_dealer_copy_ticket_reader1.TabIndex = 7;
      this.uc_gt_dealer_copy_ticket_reader1.UcPopupHelperParent = null;
      this.uc_gt_dealer_copy_ticket_reader1.Visible = false;
      // 
      // uc_input_amount_resizable1
      // 
      this.uc_input_amount_resizable1.AcceptDecimals = true;
      this.uc_input_amount_resizable1.Anchor = System.Windows.Forms.AnchorStyles.Top;
      this.uc_input_amount_resizable1.BackColor = System.Drawing.Color.LightSkyBlue;
      this.uc_input_amount_resizable1.Location = new System.Drawing.Point(301, 0);
      this.uc_input_amount_resizable1.Margin = new System.Windows.Forms.Padding(0);
      this.uc_input_amount_resizable1.MaximumSize = new System.Drawing.Size(375, 375);
      this.uc_input_amount_resizable1.Name = "uc_input_amount_resizable1";
      this.uc_input_amount_resizable1.Size = new System.Drawing.Size(375, 375);
      this.uc_input_amount_resizable1.TabIndex = 6;
      this.uc_input_amount_resizable1.Visible = false;
      this.uc_input_amount_resizable1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.uc_input_amount_resizable1_MouseMove);
      // 
      // uc_gt_card_reader1
      // 
      this.uc_gt_card_reader1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.uc_gt_card_reader1.BackColor = System.Drawing.Color.Transparent;
      this.uc_gt_card_reader1.Location = new System.Drawing.Point(450, 3);
      this.uc_gt_card_reader1.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
      this.uc_gt_card_reader1.Name = "uc_gt_card_reader1";
      this.uc_gt_card_reader1.Size = new System.Drawing.Size(400, 204);
      this.uc_gt_card_reader1.TabIndex = 3;
      this.uc_gt_card_reader1.Visible = false;
      // 
      // pnl_bottom
      // 
      this.pnl_bottom.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.pnl_bottom.BackColor = System.Drawing.Color.Transparent;
      this.pnl_bottom.BorderColor = System.Drawing.Color.Empty;
      this.pnl_bottom.Controls.Add(this.lbl_drop_national_currency_amount);
      this.pnl_bottom.Controls.Add(this.lbl_drop_national_currency);
      this.pnl_bottom.Controls.Add(this.btn_table_iso_code);
      this.pnl_bottom.Controls.Add(this.pnl_menu_table_mode);
      this.pnl_bottom.Controls.Add(this.lbl_max_bet_value);
      this.pnl_bottom.Controls.Add(this.lbl_min_bet_name);
      this.pnl_bottom.Controls.Add(this.lbl_bet);
      this.pnl_bottom.Controls.Add(this.lbl_min_bet_value);
      this.pnl_bottom.Controls.Add(this.cb_table_speed);
      this.pnl_bottom.Controls.Add(this.btn_legend);
      this.pnl_bottom.Controls.Add(this.btn_play_pause);
      this.pnl_bottom.Controls.Add(this.lbl_max_bet_name);
      this.pnl_bottom.Controls.Add(this.lbl_speed);
      this.pnl_bottom.CornerRadius = 20;
      this.pnl_bottom.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.pnl_bottom.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.pnl_bottom.HeaderBackColor = System.Drawing.Color.Transparent;
      this.pnl_bottom.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.pnl_bottom.HeaderHeight = 0;
      this.pnl_bottom.HeaderSubText = null;
      this.pnl_bottom.HeaderText = null;
      this.pnl_bottom.Location = new System.Drawing.Point(7, 682);
      this.pnl_bottom.Name = "pnl_bottom";
      this.pnl_bottom.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.pnl_bottom.Size = new System.Drawing.Size(933, 50);
      this.pnl_bottom.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NONE;
      this.pnl_bottom.TabIndex = 0;
      // 
      // lbl_drop_national_currency_amount
      // 
      this.lbl_drop_national_currency_amount.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
      this.lbl_drop_national_currency_amount.BackColor = System.Drawing.Color.Transparent;
      this.lbl_drop_national_currency_amount.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_drop_national_currency_amount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.lbl_drop_national_currency_amount.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
      this.lbl_drop_national_currency_amount.Location = new System.Drawing.Point(466, 14);
      this.lbl_drop_national_currency_amount.Name = "lbl_drop_national_currency_amount";
      this.lbl_drop_national_currency_amount.Size = new System.Drawing.Size(100, 23);
      this.lbl_drop_national_currency_amount.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_WHITE;
      this.lbl_drop_national_currency_amount.TabIndex = 90;
      this.lbl_drop_national_currency_amount.Text = "X,XXX,XXX,XX";
      this.lbl_drop_national_currency_amount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_drop_national_currency
      // 
      this.lbl_drop_national_currency.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.lbl_drop_national_currency.BackColor = System.Drawing.Color.Transparent;
      this.lbl_drop_national_currency.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_drop_national_currency.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.lbl_drop_national_currency.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
      this.lbl_drop_national_currency.Location = new System.Drawing.Point(368, 6);
      this.lbl_drop_national_currency.Name = "lbl_drop_national_currency";
      this.lbl_drop_national_currency.Size = new System.Drawing.Size(95, 39);
      this.lbl_drop_national_currency.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_WHITE;
      this.lbl_drop_national_currency.TabIndex = 89;
      this.lbl_drop_national_currency.Text = "xDrop (MXN):";
      this.lbl_drop_national_currency.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // btn_table_iso_code
      // 
      this.btn_table_iso_code.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_table_iso_code.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(90)))), ((int)(((byte)(102)))));
      this.btn_table_iso_code.FlatAppearance.BorderSize = 0;
      this.btn_table_iso_code.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_table_iso_code.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_table_iso_code.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_table_iso_code.Image = null;
      this.btn_table_iso_code.IsSelected = false;
      this.btn_table_iso_code.Location = new System.Drawing.Point(821, 4);
      this.btn_table_iso_code.Name = "btn_table_iso_code";
      this.btn_table_iso_code.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_table_iso_code.Size = new System.Drawing.Size(49, 44);
      this.btn_table_iso_code.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.GAMING_TABLE;
      this.btn_table_iso_code.TabIndex = 88;
      this.btn_table_iso_code.Text = "XCUR";
      this.btn_table_iso_code.UseVisualStyleBackColor = false;
      // 
      // pnl_menu_table_mode
      // 
      this.pnl_menu_table_mode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.pnl_menu_table_mode.BackColor = System.Drawing.Color.Transparent;
      this.pnl_menu_table_mode.BorderColor = System.Drawing.Color.Empty;
      this.pnl_menu_table_mode.Controls.Add(this.btn_menu_table_mode);
      this.pnl_menu_table_mode.CornerRadius = 10;
      this.pnl_menu_table_mode.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.pnl_menu_table_mode.HeaderBackColor = System.Drawing.Color.Empty;
      this.pnl_menu_table_mode.HeaderForeColor = System.Drawing.Color.Empty;
      this.pnl_menu_table_mode.HeaderHeight = 0;
      this.pnl_menu_table_mode.HeaderSubText = null;
      this.pnl_menu_table_mode.HeaderText = null;
      this.pnl_menu_table_mode.Location = new System.Drawing.Point(7, 4);
      this.pnl_menu_table_mode.Name = "pnl_menu_table_mode";
      this.pnl_menu_table_mode.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.pnl_menu_table_mode.Size = new System.Drawing.Size(50, 45);
      this.pnl_menu_table_mode.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NONE;
      this.pnl_menu_table_mode.TabIndex = 87;
      // 
      // btn_menu_table_mode
      // 
      this.btn_menu_table_mode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.btn_menu_table_mode.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(90)))), ((int)(((byte)(102)))));
      this.btn_menu_table_mode.FlatAppearance.BorderSize = 0;
      this.btn_menu_table_mode.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_menu_table_mode.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_menu_table_mode.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_menu_table_mode.Image = ((System.Drawing.Image)(resources.GetObject("btn_menu_table_mode.Image")));
      this.btn_menu_table_mode.IsSelected = false;
      this.btn_menu_table_mode.Location = new System.Drawing.Point(-2, -1);
      this.btn_menu_table_mode.Margin = new System.Windows.Forms.Padding(0);
      this.btn_menu_table_mode.Name = "btn_menu_table_mode";
      this.btn_menu_table_mode.SelectedColor = System.Drawing.Color.Empty;
      this.btn_menu_table_mode.Size = new System.Drawing.Size(49, 45);
      this.btn_menu_table_mode.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.GAMING_TABLE;
      this.btn_menu_table_mode.TabIndex = 73;
      this.btn_menu_table_mode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.btn_menu_table_mode.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
      this.btn_menu_table_mode.UseVisualStyleBackColor = false;
      this.btn_menu_table_mode.Click += new System.EventHandler(this.btn_menu_table_mode_Click);
      // 
      // lbl_max_bet_value
      // 
      this.lbl_max_bet_value.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_max_bet_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_max_bet_value.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_max_bet_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.lbl_max_bet_value.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
      this.lbl_max_bet_value.Location = new System.Drawing.Point(701, 4);
      this.lbl_max_bet_value.Name = "lbl_max_bet_value";
      this.lbl_max_bet_value.Size = new System.Drawing.Size(114, 23);
      this.lbl_max_bet_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_WHITE;
      this.lbl_max_bet_value.TabIndex = 86;
      this.lbl_max_bet_value.Text = "xValueMaxBet";
      this.lbl_max_bet_value.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_min_bet_name
      // 
      this.lbl_min_bet_name.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_min_bet_name.BackColor = System.Drawing.Color.Transparent;
      this.lbl_min_bet_name.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_min_bet_name.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.lbl_min_bet_name.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
      this.lbl_min_bet_name.Location = new System.Drawing.Point(641, 24);
      this.lbl_min_bet_name.Name = "lbl_min_bet_name";
      this.lbl_min_bet_name.Size = new System.Drawing.Size(44, 21);
      this.lbl_min_bet_name.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_WHITE;
      this.lbl_min_bet_name.TabIndex = 85;
      this.lbl_min_bet_name.Text = "xMinBet";
      this.lbl_min_bet_name.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_bet
      // 
      this.lbl_bet.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_bet.BackColor = System.Drawing.Color.Transparent;
      this.lbl_bet.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_bet.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.lbl_bet.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
      this.lbl_bet.Location = new System.Drawing.Point(572, 14);
      this.lbl_bet.Name = "lbl_bet";
      this.lbl_bet.Size = new System.Drawing.Size(63, 23);
      this.lbl_bet.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_WHITE;
      this.lbl_bet.TabIndex = 84;
      this.lbl_bet.Text = "xBet";
      this.lbl_bet.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_min_bet_value
      // 
      this.lbl_min_bet_value.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_min_bet_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_min_bet_value.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_min_bet_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.lbl_min_bet_value.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
      this.lbl_min_bet_value.Location = new System.Drawing.Point(701, 25);
      this.lbl_min_bet_value.Name = "lbl_min_bet_value";
      this.lbl_min_bet_value.Size = new System.Drawing.Size(114, 21);
      this.lbl_min_bet_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_WHITE;
      this.lbl_min_bet_value.TabIndex = 83;
      this.lbl_min_bet_value.Text = "xValueMinBet";
      this.lbl_min_bet_value.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // cb_table_speed
      // 
      this.cb_table_speed.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.cb_table_speed.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_table_speed.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.cb_table_speed.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.cb_table_speed.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.cb_table_speed.CornerRadius = 5;
      this.cb_table_speed.DataSource = null;
      this.cb_table_speed.DisplayMember = "";
      this.cb_table_speed.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
      this.cb_table_speed.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cb_table_speed.DropDownWidth = 162;
      this.cb_table_speed.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.cb_table_speed.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_table_speed.FormattingEnabled = true;
      this.cb_table_speed.IsDroppedDown = false;
      this.cb_table_speed.ItemHeight = 40;
      this.cb_table_speed.Location = new System.Drawing.Point(199, 6);
      this.cb_table_speed.MaxDropDownItems = 8;
      this.cb_table_speed.Name = "cb_table_speed";
      this.cb_table_speed.OnFocusOpenListBox = true;
      this.cb_table_speed.SelectedIndex = -1;
      this.cb_table_speed.SelectedItem = null;
      this.cb_table_speed.SelectedValue = null;
      this.cb_table_speed.SelectionLength = 0;
      this.cb_table_speed.SelectionStart = 0;
      this.cb_table_speed.Size = new System.Drawing.Size(162, 40);
      this.cb_table_speed.Sorted = false;
      this.cb_table_speed.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.BASIC;
      this.cb_table_speed.TabIndex = 82;
      this.cb_table_speed.TabStop = false;
      this.cb_table_speed.ValueMember = "";
      // 
      // btn_legend
      // 
      this.btn_legend.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_legend.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(90)))), ((int)(((byte)(102)))));
      this.btn_legend.FlatAppearance.BorderColor = System.Drawing.Color.White;
      this.btn_legend.FlatAppearance.BorderSize = 0;
      this.btn_legend.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_legend.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_legend.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_legend.Image = ((System.Drawing.Image)(resources.GetObject("btn_legend.Image")));
      this.btn_legend.IsSelected = false;
      this.btn_legend.Location = new System.Drawing.Point(876, 4);
      this.btn_legend.Margin = new System.Windows.Forms.Padding(0);
      this.btn_legend.Name = "btn_legend";
      this.btn_legend.SelectedColor = System.Drawing.Color.Empty;
      this.btn_legend.Size = new System.Drawing.Size(49, 44);
      this.btn_legend.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.GAMING_TABLE;
      this.btn_legend.TabIndex = 81;
      this.btn_legend.UseVisualStyleBackColor = false;
      // 
      // btn_play_pause
      // 
      this.btn_play_pause.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(90)))), ((int)(((byte)(102)))));
      this.btn_play_pause.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_play_pause.FlatAppearance.BorderSize = 0;
      this.btn_play_pause.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_play_pause.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_play_pause.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_play_pause.Image = ((System.Drawing.Image)(resources.GetObject("btn_play_pause.Image")));
      this.btn_play_pause.IsSelected = false;
      this.btn_play_pause.Location = new System.Drawing.Point(61, 4);
      this.btn_play_pause.Name = "btn_play_pause";
      this.btn_play_pause.SelectedColor = System.Drawing.Color.Empty;
      this.btn_play_pause.Size = new System.Drawing.Size(49, 44);
      this.btn_play_pause.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.GAMING_TABLE;
      this.btn_play_pause.TabIndex = 80;
      this.btn_play_pause.UseVisualStyleBackColor = false;
      this.btn_play_pause.Click += new System.EventHandler(this.btn_play_pause_Click);
      // 
      // lbl_max_bet_name
      // 
      this.lbl_max_bet_name.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_max_bet_name.BackColor = System.Drawing.Color.Transparent;
      this.lbl_max_bet_name.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_max_bet_name.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.lbl_max_bet_name.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
      this.lbl_max_bet_name.Location = new System.Drawing.Point(641, 5);
      this.lbl_max_bet_name.Name = "lbl_max_bet_name";
      this.lbl_max_bet_name.Size = new System.Drawing.Size(47, 21);
      this.lbl_max_bet_name.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_WHITE;
      this.lbl_max_bet_name.TabIndex = 78;
      this.lbl_max_bet_name.Text = "XMAXBET";
      this.lbl_max_bet_name.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_speed
      // 
      this.lbl_speed.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.lbl_speed.BackColor = System.Drawing.Color.Transparent;
      this.lbl_speed.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_speed.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.lbl_speed.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
      this.lbl_speed.Location = new System.Drawing.Point(116, 14);
      this.lbl_speed.Name = "lbl_speed";
      this.lbl_speed.Size = new System.Drawing.Size(80, 23);
      this.lbl_speed.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_WHITE;
      this.lbl_speed.TabIndex = 76;
      this.lbl_speed.Text = "xSpeed";
      this.lbl_speed.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // uc_scrollable_button_list1
      // 
      this.uc_scrollable_button_list1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.uc_scrollable_button_list1.ArrowButtonColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(47)))), ((int)(((byte)(50)))));
      this.uc_scrollable_button_list1.BackColor = System.Drawing.Color.Transparent;
      this.uc_scrollable_button_list1.ButtonColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.uc_scrollable_button_list1.ForeColor = System.Drawing.SystemColors.ButtonFace;
      this.uc_scrollable_button_list1.Location = new System.Drawing.Point(12, 5);
      this.uc_scrollable_button_list1.Margin = new System.Windows.Forms.Padding(3, 3, 3, 6);
      this.uc_scrollable_button_list1.MaxButtons = 0;
      this.uc_scrollable_button_list1.MinButtonWidth = 0;
      this.uc_scrollable_button_list1.Name = "uc_scrollable_button_list1";
      this.uc_scrollable_button_list1.Size = new System.Drawing.Size(620, 42);
      this.uc_scrollable_button_list1.TabIndex = 74;
      this.uc_scrollable_button_list1.ButtonClicked += new WSI.Cashier.uc_scrollable_button_list.ButtonClickedEventHandler(this.uc_scrollable_gambling_tables_ButtonClicked);
      // 
      // cb_banks
      // 
      this.cb_banks.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.cb_banks.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_banks.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.cb_banks.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.cb_banks.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.cb_banks.CornerRadius = 5;
      this.cb_banks.DataSource = null;
      this.cb_banks.DisplayMember = "";
      this.cb_banks.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
      this.cb_banks.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cb_banks.DropDownWidth = 273;
      this.cb_banks.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.cb_banks.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_banks.FormattingEnabled = true;
      this.cb_banks.IsDroppedDown = false;
      this.cb_banks.ItemHeight = 40;
      this.cb_banks.Location = new System.Drawing.Point(666, 4);
      this.cb_banks.MaxDropDownItems = 8;
      this.cb_banks.Name = "cb_banks";
      this.cb_banks.OnFocusOpenListBox = true;
      this.cb_banks.SelectedIndex = -1;
      this.cb_banks.SelectedItem = null;
      this.cb_banks.SelectedValue = null;
      this.cb_banks.SelectionLength = 0;
      this.cb_banks.SelectionStart = 0;
      this.cb_banks.Size = new System.Drawing.Size(273, 40);
      this.cb_banks.Sorted = false;
      this.cb_banks.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.BASIC;
      this.cb_banks.TabIndex = 0;
      this.cb_banks.TabStop = false;
      this.cb_banks.ValueMember = "";
      // 
      // lbl_change_position
      // 
      this.lbl_change_position.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.lbl_change_position.AutoSize = true;
      this.lbl_change_position.BackColor = System.Drawing.Color.Transparent;
      this.lbl_change_position.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_change_position.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.lbl_change_position.Location = new System.Drawing.Point(77, 162);
      this.lbl_change_position.Name = "lbl_change_position";
      this.lbl_change_position.Size = new System.Drawing.Size(35, 13);
      this.lbl_change_position.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_WHITE;
      this.lbl_change_position.TabIndex = 10;
      this.lbl_change_position.Text = "xChangePosition";
      this.lbl_change_position.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lbl_info_exit
      // 
      this.lbl_info_exit.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.lbl_info_exit.AutoSize = true;
      this.lbl_info_exit.BackColor = System.Drawing.Color.Transparent;
      this.lbl_info_exit.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_info_exit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.lbl_info_exit.Location = new System.Drawing.Point(54, 18);
      this.lbl_info_exit.Name = "lbl_info_exit";
      this.lbl_info_exit.Size = new System.Drawing.Size(78, 13);
      this.lbl_info_exit.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_WHITE;
      this.lbl_info_exit.TabIndex = 7;
      this.lbl_info_exit.Text = "xEndSession";
      this.lbl_info_exit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lbl_info_pause
      // 
      this.lbl_info_pause.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.lbl_info_pause.AutoSize = true;
      this.lbl_info_pause.BackColor = System.Drawing.Color.Transparent;
      this.lbl_info_pause.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_info_pause.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.lbl_info_pause.Location = new System.Drawing.Point(57, 118);
      this.lbl_info_pause.Name = "lbl_info_pause";
      this.lbl_info_pause.Size = new System.Drawing.Size(73, 13);
      this.lbl_info_pause.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_WHITE;
      this.lbl_info_pause.TabIndex = 9;
      this.lbl_info_pause.Text = "Pausar sesi�n";
      this.lbl_info_pause.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lbl_info_free
      // 
      this.lbl_info_free.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.lbl_info_free.AutoSize = true;
      this.lbl_info_free.BackColor = System.Drawing.Color.Transparent;
      this.lbl_info_free.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_info_free.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.lbl_info_free.Location = new System.Drawing.Point(61, 218);
      this.lbl_info_free.Name = "lbl_info_free";
      this.lbl_info_free.Size = new System.Drawing.Size(64, 13);
      this.lbl_info_free.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_WHITE;
      this.lbl_info_free.TabIndex = 11;
      this.lbl_info_free.Text = "Asiento libre";
      this.lbl_info_free.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lbl_info_away
      // 
      this.lbl_info_away.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.lbl_info_away.AutoSize = true;
      this.lbl_info_away.BackColor = System.Drawing.Color.Transparent;
      this.lbl_info_away.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_info_away.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.lbl_info_away.Location = new System.Drawing.Point(71, 312);
      this.lbl_info_away.Name = "lbl_info_away";
      this.lbl_info_away.Size = new System.Drawing.Size(45, 26);
      this.lbl_info_away.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_WHITE;
      this.lbl_info_away.TabIndex = 13;
      this.lbl_info_away.Text = "Jugador ausente";
      this.lbl_info_away.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // label2
      // 
      this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.label2.AutoSize = true;
      this.label2.BackColor = System.Drawing.Color.Transparent;
      this.label2.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.label2.Location = new System.Drawing.Point(77, 162);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(35, 13);
      this.label2.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_WHITE;
      this.label2.TabIndex = 10;
      this.label2.Text = "xChangePosition";
      this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // uc_gt_player_tracking
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.Controls.Add(this.dgv_gt_movememnts);
      this.Controls.Add(this.pnl_information);
      this.Controls.Add(this.pnl_report);
      this.Controls.Add(this.pnl_table_options);
      this.Controls.Add(this.pnl_container);
      this.Controls.Add(this.pnl_bottom);
      this.Controls.Add(this.uc_scrollable_button_list1);
      this.Controls.Add(this.cb_banks);
      this.Controls.Add(this.transparentPanel1);
      this.Name = "uc_gt_player_tracking";
      this.Size = new System.Drawing.Size(950, 735);
      this.SizeChanged += new System.EventHandler(this.uc_gt_player_tracking_SizeChanged);
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.pb_exit)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.pb_pause)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.pb_change_seat)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.pb_free)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.pb_ocupated)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.pb_away)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.pb_disabled)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.dgv_gt_movememnts)).EndInit();
      this.pnl_information.ResumeLayout(false);
      this.tlp_information.ResumeLayout(false);
      this.tlp_information.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pb_legend_disabled_seat)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.pb_legend_resume_session)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.pb_legend_end_session)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.pb_legend_pause_session)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.pb_legend_swap_position)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.pb_legend_free_seat)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.pb_legend_taken_seat)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.pb_legend_away_player)).EndInit();
      this.pnl_report.ResumeLayout(false);
      this.pnl_report.PerformLayout();
      this.pnl_container.ResumeLayout(false);
      this.pnl_container.PerformLayout();
      this.rp_legend_movements.ResumeLayout(false);
      this.tlp_legend_movements.ResumeLayout(false);
      this.tlp_legend_movements.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pb_legend_amount)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.pb_legend_chip_nre)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.pb_legend_chip_color)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.pb_legend_chip_re)).EndInit();
      this.gamingTablePanel1.ResumeLayout(false);
      this.pnl_bottom.ResumeLayout(false);
      this.pnl_menu_table_mode.ResumeLayout(false);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private uc_gt_player_in_session uc_gt_player_in_session1;
    private uc_gt_card_reader uc_gt_card_reader1;
    private WSI.Common.GamingTablePanel gamingTablePanel1;
    private WSI.Cashier.Controls.uc_round_combobox cb_banks;
    private uc_input_amount_resizable uc_input_amount_resizable1;
    private WSI.Cashier.Controls.uc_round_panel pnl_information;
    private System.Windows.Forms.TableLayoutPanel tlp_information;
    private System.Windows.Forms.PictureBox pb_legend_end_session;
    private System.Windows.Forms.PictureBox pb_legend_resume_session;
    private System.Windows.Forms.PictureBox pb_legend_pause_session;
    private System.Windows.Forms.PictureBox pb_legend_swap_position;
    private System.Windows.Forms.PictureBox pb_legend_free_seat;
    private System.Windows.Forms.PictureBox pb_legend_taken_seat;
    private System.Windows.Forms.PictureBox pb_legend_away_player;
    private System.Windows.Forms.PictureBox pb_legend_disabled_seat;
    private System.Windows.Forms.PictureBox pictureBox4;
    private System.Windows.Forms.PictureBox pictureBox5;
    private System.Windows.Forms.PictureBox pictureBox6;
    private WSI.Cashier.Controls.uc_label lbl_legend_end_session;
    private WSI.Cashier.Controls.uc_label lbl_legend_resume_session;
    private WSI.Cashier.Controls.uc_label lbl_legend_pause_session;
    private WSI.Cashier.Controls.uc_label lbl_legend_swap_position;
    private System.Windows.Forms.PictureBox pb_exit;
    private WSI.Cashier.Controls.uc_label lbl_change_position;
    private System.Windows.Forms.PictureBox pb_pause;
    private System.Windows.Forms.PictureBox pb_change_seat;
    private System.Windows.Forms.PictureBox pb_free;
    private System.Windows.Forms.PictureBox pb_ocupated;
    private System.Windows.Forms.PictureBox pb_away;
    private WSI.Cashier.Controls.uc_label lbl_legend_free_seat;
    private WSI.Cashier.Controls.uc_label lbl_legend_taken_seat;
    private System.Windows.Forms.PictureBox pb_disabled;
    private WSI.Cashier.Controls.uc_label lbl_legend_away_player;
    private WSI.Cashier.Controls.uc_label lbl_legend_disabled_seat;
    private WSI.Cashier.Controls.uc_label lbl_info_exit;
    private WSI.Cashier.Controls.uc_label lbl_info_pause;
    private WSI.Cashier.Controls.uc_label lbl_info_free;
    private WSI.Cashier.Controls.uc_label lbl_info_away;
    private uc_scrollable_button_list uc_scrollable_button_list1;
    private WSI.Cashier.Controls.uc_round_panel pnl_bottom;
    private WSI.Cashier.Controls.uc_label lbl_bet;
    private WSI.Cashier.Controls.uc_label lbl_min_bet_value;
    private WSI.Cashier.Controls.uc_round_combobox cb_table_speed;
    private WSI.Cashier.Controls.uc_round_button btn_legend;
    private WSI.Cashier.Controls.uc_round_button btn_play_pause;
    private WSI.Cashier.Controls.uc_label lbl_max_bet_name;
    private WSI.Cashier.Controls.uc_label lbl_speed;
    private WSI.Cashier.Controls.uc_label label2;
    private WSI.Cashier.Controls.uc_label lbl_max_bet_value;
    private WSI.Cashier.Controls.uc_label lbl_min_bet_name;
    private WSI.Common.uc_transparent_panel transparentPanel1;
    private WSI.Cashier.Controls.uc_round_panel pnl_container;
    private WSI.Cashier.Controls.uc_round_panel pnl_menu_table_mode;
    private WSI.Cashier.Controls.uc_round_button btn_menu_table_mode;
    private WSI.Cashier.Controls.uc_round_panel pnl_table_options;
    private WSI.Cashier.Controls.uc_round_panel pnl_report;
    private WSI.Cashier.Controls.uc_DataGridView dgv_gt_movememnts;
    private Controls.uc_round_webbrowser webBrowserGtSessionInfo;
    private Controls.uc_round_button btn_table_iso_code;
    private Controls.uc_label lbl_name_and_data_open_session;
    private Controls.uc_round_panel rp_legend_movements;
    private System.Windows.Forms.TableLayoutPanel tlp_legend_movements;
    private System.Windows.Forms.PictureBox pb_legend_amount;
    private System.Windows.Forms.PictureBox pb_legend_chip_nre;
    private System.Windows.Forms.PictureBox pb_legend_chip_color;
    private Controls.uc_label lbl_legend_chip_nre;
    private Controls.uc_label lbl_legend_chip_color;
    private Controls.uc_label lbl_legend_amount;
    private System.Windows.Forms.PictureBox pb_legend_chip_re;
    private Controls.uc_label lbl_legend_chip_re;
    private uc_gt_dealer_copy_ticket_reader uc_gt_dealer_copy_ticket_reader1;
    private Controls.uc_label lbl_drop_national_currency;
    private Controls.uc_label lbl_drop_national_currency_amount;
  }
}
