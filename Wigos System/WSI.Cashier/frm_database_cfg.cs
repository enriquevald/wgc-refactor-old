//------------------------------------------------------------------------------
// Copyright � 2008 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_database_cfg.cs
// 
//   DESCRIPTION: Change database configuration
// 
//        AUTHOR: Alberto Cuesta
// 
// CREATION DATE: 04-NOV-2008
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 04-NOV-2008 ACC    First release.
// 28-MAY-2013 NMR    Deleting uses of StringNoVoucherMode() funcionality
// 09-NOV-2015 JMV    Product Backlog Item 5858:Dise�o cajero: Aplicar en frm_database_cfg.cs
// 16-FEB-2017 ATB    Add logging system to the shutdowns to control it
// 18-SEP-2017 DPC    WIGOS-5268: Cashier & GUI - Icons - Implement
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WSI.Common;
using WSI.Cashier.Controls;


namespace WSI.Cashier
{
  public partial class frm_database_cfg : frm_base_icon
  {
    private Boolean m_connecting;

    #region Constructor

    public frm_database_cfg()
    {
      InitializeComponent();

      InitializeControlResources();
    }

    #endregion

    private void InitializeControlResources()
    {
      // - NLS Strings:
      //   - Title: External set
      lbl_message_title.Text = Resource.String("STR_FRM_DATABASE_CFG_ERROR");

      // - Images:
      //btn_shutdown.Image = WSI.Cashier.Images.Get32x32(Images.CashierImage.Shutdown);

    } // InitializeControlResources


    //------------------------------------------------------------------------------
    // PURPOSE : Event data changed.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void uc_database_cfg1_ValuesChanged(object sender, EventArgs e)
    {
      Hide();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Event btn shutdown.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void btn_shutdown_Click(object sender, EventArgs e)
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[SHUTDOWN] REASON: User action (from database configuration", Log.Type.Message);
      Cashier.Shutdown(false);
    }

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Public function for show dialog.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public static frm_database_cfg Show(Boolean Connecting, Form Parent)
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_database_cfg", Log.Type.Message);
      frm_database_cfg form_database = new frm_database_cfg();

      form_database.m_connecting = Connecting;

      if (Connecting)
      {
        form_database.lbl_app_name.Visible = true;
        form_database.lbl_copyright.Visible = true;
        form_database.lbl_version.Visible = true;
        form_database.lbl_version.Text = "(" + VersionControl.VersionStr + ")";
        form_database.lbl_connecting_to_db.Visible = true;
        form_database.lbl_connecting_to_db.Text = Resource.String("STR_FRM_DATABASE_CFG_CONNECTING");

        form_database.lbl_app_name.Top = 6;
        form_database.lbl_copyright.Top = 105;
        form_database.lbl_version.Top = 51;
        form_database.lbl_connecting_to_db.Top = 159;
        form_database.pb_connecting.Top = 190;

        form_database.lbl_message_title.Visible = false;
        form_database.pb_connecting.Visible = true;
        form_database.pb_connecting.Value = form_database.pb_connecting.Minimum;
        form_database.pb_connecting.Value = 0;
        form_database.uc_database_cfg1.Visible = false;
        form_database.btn_shutdown.Visible = false;
        form_database.timer1.Enabled = true;
        form_database.timer1.Start();

        form_database.Width = 386;
        form_database.Height = 220;
      }
      else
      {
        form_database.lbl_app_name.Visible = false;
        form_database.lbl_copyright.Visible = false;
        form_database.lbl_version.Visible = false;
        form_database.lbl_connecting_to_db.Visible = false;

        form_database.lbl_message_title.Visible = true;
        form_database.lbl_message_title.Text = Resource.String("STR_FRM_DATABASE_CFG_ERROR");
        form_database.pb_connecting.Visible = false;
        form_database.uc_database_cfg1.Visible = true;
        form_database.btn_shutdown.Visible = true;
        form_database.timer1.Enabled = false;

        form_database.Width = 486;
        form_database.Height = 260;
      }

      form_database.Location = new Point(1024/2 - form_database.Width/2, 768/2 - form_database.Height);
      //SJA 17-02-2016 Stop throwing null parent exception when in paranoia exception mode

      if (Parent != null)
      {
        form_database.ShowDialog(Parent);
        Parent.Focus();
      }
      else
      {
        form_database.ShowDialog();
      }
      return form_database;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Public function for show dialog.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    static public void Hide(frm_database_cfg Form, Form Parent)
    {
      if (Parent != null)
      {
        Parent.Focus();
      }
    }

    #endregion

    //------------------------------------------------------------------------------
    // PURPOSE : Timer for progress.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void timer1_Tick(object sender, EventArgs e)
    {
      if (pb_connecting.Value >= pb_connecting.Maximum)
      {
        pb_connecting.Value = pb_connecting.Minimum;
      }

      pb_connecting.Value += pb_connecting.Step;
    }

  }
}