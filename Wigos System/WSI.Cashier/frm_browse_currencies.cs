//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: CurrencyExchange.cs
// 
//   DESCRIPTION: frm_browse_currencies class
// 
//        AUTHOR: David Lasdiez
// 
// CREATION DATE: 12-JUN-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 25-JUN-2014 DLL    First version.
// 25-JUL-2014 DLL    Chips are moved to a separate table
// 09-NOV-2015 FOS    Product Backlog Item 5850:Apply new design in form
// 24-MAY-2017 JML    PBI 27484:WIGOS-1226 Win / loss - Introduce the WinLoss - EDITION SCREEN - PER DENOMINATION
// 24-MAY-2017 JML    PBI 27486:WIGOS-1227 Win / loss - Introduce the WinLoss - EDITION SCREEN - ACCUMULATED
// 23-FEB-2018 RGR    Bug 31724:WIGOS-3707 X02 appears in when selecting currency in Deposit and Cage Request 
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WSI.Common;
using WSI.Cashier.Controls;

namespace WSI.Cashier
{
  public partial class frm_browse_currencies : frm_base
  {
    private const Int32 GRID_COLUMN_IMAGE = 0;
    private const Int32 GRID_COLUMN_ISO_CODE = 1;
    private const Int32 GRID_COLUMN_DESCRIPTION = 2;

    #region Attributes
    private CurrencyExchange m_selected_currency;
    private frm_yesno form_yes_no;
    private Dictionary<String, CurrencyExchange> m_all_currencies;
    #endregion

    #region Constructor
    /// <summary>
    /// Constructor method.
    /// 1. Initialize controls.
    /// 2. Initialize control�s resources. (NLS, Images, etc)
    /// </summary>
    public frm_browse_currencies()
    {
      InitializeComponent();

      InitializeControlResources();

      form_yes_no = new frm_yesno();
      form_yes_no.Opacity = 0.6;
    }

    #endregion
    //------------------------------------------------------------------------------
    // PURPOSE : Initialize control resources (NLS strings, images, etc).
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //

    #region Private Methods

    private void InitializeControlResources()
    {
      // - NLS Strings:
      //   - Title:
      this.FormTitle = Resource.String("STR_SELECT_CURRENCY_EXCHANGE");

      //   - Labels

      //   - Buttons
      btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Initialize control size
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void CreateScreen(Int32 NumCurrencies)
    {
      Int32 _width;
      Int32 _height;

      _width = 164;
      _height = 84;

      if (NumCurrencies > 4)
      {
        pnl_container.Height += _height;
        pnl_data.Height += _height;
      }

      if (NumCurrencies > 6)
      {
        pnl_container.Width += _width;
        pnl_data.Width += _width;
      }

      if (NumCurrencies > 9)
      {
        pnl_container.Height += _height;
        pnl_data.Height += _height;
      }

      if (NumCurrencies > 12)
      {
        pnl_container.Width += _width;
        pnl_data.Width += _width;
      }

      if (NumCurrencies > 16)
      {
        pnl_container.Height += _height;
        pnl_data.Height += _height;
      }

      if (NumCurrencies > 20)
      {
        pnl_container.Height += _height;
        pnl_data.Height += _height;
      }

      if (NumCurrencies > 24)
      {
        pnl_container.Width += 16;
        pnl_data.Width += 20;
      }

    }


    //------------------------------------------------------------------------------
    // PURPOSE : Count ListCurrencies including only visible buttons
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //   Int32: visible buttons count.
    //   NOTES :
    //
    private Int32 CountCurrencyButtons(List<CurrencyExchange> ListCurrencies, VoucherTypes VoucherType)
    {
      Int32 _count_currency;

      _count_currency = 0;

      foreach (CurrencyExchange _currency in ListCurrencies)
      {
        if (ShowCurrencyInForm(VoucherType, _currency))
        {
          _count_currency++;
        }
      }
      return _count_currency;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Create buttons for each currency
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void CreateButtons(List<CurrencyExchange> ListCurrencies, VoucherTypes VoucherType)
    {
      Int32 _currency_position_x;
      Int32 _currency_position_y;
      Int32 _width;
      Int32 _height;
      uc_round_button _btn;
      Label _lbl_iso;
      CurrencyExchangeProperties _properties;
      Int32 _initial_x;
      Int32 _initial_y;

      _initial_x = 4;
      _initial_y = 4;
      _currency_position_x = _initial_x;
      _currency_position_y = _initial_y;
      _width = 160;
      _height = 80;
      m_all_currencies = new Dictionary<string, CurrencyExchange>();


      foreach (CurrencyExchange _currency in ListCurrencies)
      {
        if (ShowCurrencyInForm(VoucherType, _currency))
        {
          _properties = CurrencyExchangeProperties.GetProperties(_currency.CurrencyCode);
          _btn = new uc_round_button();
          _btn.Name = _currency.CurrencyCode + "-" + _currency.Type + "-" + _currency.SubType;
          if (_properties != null)
          {
            _btn.Image = _properties.GetIcon(_currency.Type, 50, 50, true);
          }

          _btn.TextAlign = ContentAlignment.MiddleCenter;
          _btn.ImageAlign = ContentAlignment.MiddleCenter;
          _btn.TextImageRelation = TextImageRelation.ImageBeforeText;
          _btn.Style = uc_round_button.RoundButonStyle.PRINCIPAL;
          _btn.Text = _currency.Description;
          _btn.Size = new Size(_width, _height);
          m_all_currencies.Add(_btn.Name, _currency);
          _btn.Click += new EventHandler(_btn_Click);

          if (_currency_position_x + _width + 3 >= pnl_container.Width)
          {
            _currency_position_y += _height + _initial_y;
            _currency_position_x = _initial_x;
          }
          _btn.Location = new Point(_currency_position_x, _currency_position_y);
          pnl_container.Controls.Add(_btn);


          _lbl_iso = new Label();
          _lbl_iso.BackColor = Color.FromArgb(0x6B, 0x83, 0xA1);
          _lbl_iso.Size = new Size(40, 11);
          _lbl_iso.Font = new System.Drawing.Font("Open Sans Semibold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
          _lbl_iso.ForeColor = Color.White;
          _lbl_iso.Text = _currency.Type  == CurrencyExchangeType.CASINO_CHIP_COLOR  ? string.Empty : _currency.CurrencyCode;
          //_lbl_iso.Location = new Point(_currency_position_x + 3, _currency_position_y + 1);
          _lbl_iso.Location = new Point(_currency_position_x + 19, _currency_position_y + 65);
          pnl_container.Controls.Add(_lbl_iso);
          _lbl_iso.BringToFront();

          _currency_position_x += _width + _initial_x;
        }
      }
    }

    private bool ShowCurrencyInForm(VoucherTypes VoucherType, CurrencyExchange Currency)
    {
      Boolean _return;
      _return = false;

      if (FeatureChips.IsChipsType(Currency.Type))
      {
        if (Cage.IsCageEnabled())
        {
          if (VoucherType == VoucherTypes.CloseCash
         || VoucherType == VoucherTypes.DropBoxCount
         || VoucherType == VoucherTypes.OpenCash
         || VoucherType == VoucherTypes.FillIn
         || VoucherType == VoucherTypes.FillOut
         || VoucherType == VoucherTypes.AmountRequest
         || VoucherType == VoucherTypes.CageConcepts
         || VoucherType == VoucherTypes.ChipsStock
         || VoucherType == VoucherTypes.ChipsPurchaseAmountInput
         || VoucherType == VoucherTypes.ChipsSaleAmountInput
         || VoucherType == VoucherTypes.ChangeChipsIn
         || VoucherType == VoucherTypes.ChangeChipsOut
         || VoucherType == VoucherTypes.WinLoss
         )
          {
            _return = true;
          }
        }
      }
      else
      {
        _return = true;
      }

      return _return;
    }

    private void _btn_Click(object sender, EventArgs e)
    {
      m_selected_currency = m_all_currencies[((Button)sender).Name];

      Misc.WriteLog("[FORM CLOSE] frm_browse_currencies", Log.Type.Message);
      this.Close();
    }

    private void btn_cancel_Click(object sender, EventArgs e)
    {
      Misc.WriteLog("[FORM CLOSE] frm_browse_currencies (cancel)", Log.Type.Message);
      this.Close();
    }//Close

    #endregion

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Init and Show the form.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //

    public void Show(List<CurrencyExchange> ListCurrencies, out CurrencyExchange CurrencySelected, VoucherTypes VoucherType)
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_browse_currencies", Log.Type.Message);
      CreateScreen(CountCurrencyButtons(ListCurrencies, VoucherType));
      CreateButtons(ListCurrencies, VoucherType);

      form_yes_no.Show();
      this.ShowDialog();
      form_yes_no.Hide();
      CurrencySelected = m_selected_currency;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Dispose yes_no form.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    new public void Dispose()
    {
      form_yes_no.Dispose();
      form_yes_no = null;

      base.Dispose();
    }

    #endregion

  }
}