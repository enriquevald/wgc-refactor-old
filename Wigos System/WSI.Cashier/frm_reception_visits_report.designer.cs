namespace WSI.Cashier
{
  partial class frm_reception_visits_report
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
      this.gb_last_operations = new WSI.Cashier.Controls.uc_round_panel();
      this.dgv_last_operations = new WSI.Cashier.Controls.uc_DataGridView();
      this.lbl_total_records = new WSI.Cashier.Controls.uc_label();
      this.lbl_record_count = new WSI.Cashier.Controls.uc_label();
      this.lbl_price_total = new WSI.Cashier.Controls.uc_label();
      this.lbl_total_price = new WSI.Cashier.Controls.uc_label();
      this.dt_dateparam = new WSI.Cashier.uc_datetime();
      this.btn_close = new WSI.Cashier.Controls.uc_round_button();
      this.btn_refresh = new WSI.Cashier.Controls.uc_round_button();
      this.lbl_total_entraces = new WSI.Cashier.Controls.uc_label();
      this.lbl_entrance_count = new WSI.Cashier.Controls.uc_label();
      this.chk_players_on_site = new WSI.Cashier.Controls.uc_checkBox();
      this.pnl_data.SuspendLayout();
      this.gb_last_operations.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dgv_last_operations)).BeginInit();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.Controls.Add(this.chk_players_on_site);
      this.pnl_data.Controls.Add(this.lbl_entrance_count);
      this.pnl_data.Controls.Add(this.lbl_total_entraces);
      this.pnl_data.Controls.Add(this.btn_refresh);
      this.pnl_data.Controls.Add(this.btn_close);
      this.pnl_data.Controls.Add(this.dt_dateparam);
      this.pnl_data.Controls.Add(this.lbl_total_price);
      this.pnl_data.Controls.Add(this.lbl_price_total);
      this.pnl_data.Controls.Add(this.lbl_record_count);
      this.pnl_data.Controls.Add(this.lbl_total_records);
      this.pnl_data.Controls.Add(this.gb_last_operations);
      this.pnl_data.Size = new System.Drawing.Size(1024, 658);
      this.pnl_data.TabIndex = 0;
      // 
      // gb_last_operations
      // 
      this.gb_last_operations.BackColor = System.Drawing.Color.Transparent;
      this.gb_last_operations.BorderColor = System.Drawing.Color.Empty;
      this.gb_last_operations.Controls.Add(this.dgv_last_operations);
      this.gb_last_operations.CornerRadius = 1;
      this.gb_last_operations.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
      this.gb_last_operations.HeaderBackColor = System.Drawing.Color.Empty;
      this.gb_last_operations.HeaderForeColor = System.Drawing.Color.Empty;
      this.gb_last_operations.HeaderHeight = 0;
      this.gb_last_operations.HeaderSubText = null;
      this.gb_last_operations.HeaderText = null;
      this.gb_last_operations.Location = new System.Drawing.Point(12, 6);
      this.gb_last_operations.Name = "gb_last_operations";
      this.gb_last_operations.PanelBackColor = System.Drawing.Color.Empty;
      this.gb_last_operations.Size = new System.Drawing.Size(1000, 576);
      this.gb_last_operations.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NONE;
      this.gb_last_operations.TabIndex = 95;
      this.gb_last_operations.Text = "xLastOperations";
      // 
      // dgv_last_operations
      // 
      this.dgv_last_operations.AllowUserToAddRows = false;
      this.dgv_last_operations.AllowUserToDeleteRows = false;
      this.dgv_last_operations.AllowUserToResizeColumns = false;
      this.dgv_last_operations.AllowUserToResizeRows = false;
      dataGridViewCellStyle1.BackColor = System.Drawing.Color.LightSteelBlue;
      this.dgv_last_operations.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
      this.dgv_last_operations.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.dgv_last_operations.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.dgv_last_operations.ColumnHeaderHeight = 35;
      this.dgv_last_operations.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
      dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      dataGridViewCellStyle2.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
      dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
      dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
      this.dgv_last_operations.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
      this.dgv_last_operations.ColumnHeadersHeight = 35;
      this.dgv_last_operations.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
      this.dgv_last_operations.CornerRadius = 10;
      this.dgv_last_operations.DefaultCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.dgv_last_operations.DefaultCellForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.dgv_last_operations.DefaultCellSelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.dgv_last_operations.DefaultCellSelectionForeColor = System.Drawing.Color.White;
      dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      dataGridViewCellStyle3.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.White;
      dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
      this.dgv_last_operations.DefaultCellStyle = dataGridViewCellStyle3;
      this.dgv_last_operations.Dock = System.Windows.Forms.DockStyle.Fill;
      this.dgv_last_operations.EnableHeadersVisualStyles = false;
      this.dgv_last_operations.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.dgv_last_operations.GridColor = System.Drawing.Color.LightGray;
      this.dgv_last_operations.HeaderBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
      this.dgv_last_operations.HeaderDefaultCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.dgv_last_operations.HeaderDefaultCellForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.dgv_last_operations.HeaderImages = null;
      this.dgv_last_operations.Location = new System.Drawing.Point(0, 0);
      this.dgv_last_operations.MultiSelect = false;
      this.dgv_last_operations.Name = "dgv_last_operations";
      this.dgv_last_operations.ReadOnly = true;
      this.dgv_last_operations.RowHeadersVisible = false;
      this.dgv_last_operations.RowHeadersWidth = 20;
      this.dgv_last_operations.RowTemplate.Height = 35;
      this.dgv_last_operations.RowTemplateHeight = 35;
      this.dgv_last_operations.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
      this.dgv_last_operations.Size = new System.Drawing.Size(1000, 576);
      this.dgv_last_operations.Style = WSI.Cashier.Controls.uc_DataGridView.DataGridViewStyle.WITH_ALL_ROUND_CORNERS;
      this.dgv_last_operations.TabIndex = 0;
      this.dgv_last_operations.DoubleClick += new System.EventHandler(this.dgv_last_operations_DoubleClick);
      this.dgv_last_operations.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler (this.dgv_last_operations_CellClick);

      // 
      // lbl_total_records
      // 
      this.lbl_total_records.BackColor = System.Drawing.Color.Transparent;
      this.lbl_total_records.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_total_records.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_total_records.Location = new System.Drawing.Point(610, 590);
      this.lbl_total_records.Name = "lbl_total_records";
      this.lbl_total_records.Size = new System.Drawing.Size(121, 23);
      this.lbl_total_records.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_total_records.TabIndex = 97;
      this.lbl_total_records.Text = "xCount Visits.:";
      this.lbl_total_records.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_record_count
      // 
      this.lbl_record_count.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_record_count.BackColor = System.Drawing.Color.Transparent;
      this.lbl_record_count.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_record_count.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_record_count.Location = new System.Drawing.Point(733, 590);
      this.lbl_record_count.Name = "lbl_record_count";
      this.lbl_record_count.Size = new System.Drawing.Size(924, 23);
      this.lbl_record_count.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_record_count.TabIndex = 98;
      this.lbl_record_count.Text = "99,999";
      this.lbl_record_count.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_price_total
      // 
      this.lbl_price_total.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_price_total.BackColor = System.Drawing.Color.Transparent;
      this.lbl_price_total.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_price_total.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_price_total.Location = new System.Drawing.Point(908, 590);
      this.lbl_price_total.Name = "lbl_price_total";
      this.lbl_price_total.Size = new System.Drawing.Size(924, 23);
      this.lbl_price_total.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_price_total.TabIndex = 99;
      this.lbl_price_total.Text = "99,999";
      this.lbl_price_total.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_total_price
      // 
      this.lbl_total_price.BackColor = System.Drawing.Color.Transparent;
      this.lbl_total_price.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_total_price.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_total_price.Location = new System.Drawing.Point(781, 590);
      this.lbl_total_price.Name = "lbl_total_price";
      this.lbl_total_price.Size = new System.Drawing.Size(121, 23);
      this.lbl_total_price.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_total_price.TabIndex = 100;
      this.lbl_total_price.Text = "xTotal Price.:";
      this.lbl_total_price.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // dt_dateparam
      // 
      this.dt_dateparam.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
      this.dt_dateparam.DateText = "xDateText";
      this.dt_dateparam.DateTextFont = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.dt_dateparam.DateTextWidth = 94;
      this.dt_dateparam.DateValue = new System.DateTime(((long)(0)));
      this.dt_dateparam.FormatFont = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.dt_dateparam.FormatWidth = 39;
      this.dt_dateparam.Invalid = false;
      this.dt_dateparam.Location = new System.Drawing.Point(209, 600);
      this.dt_dateparam.Name = "dt_dateparam";
      this.dt_dateparam.Size = new System.Drawing.Size(320, 41);
      this.dt_dateparam.TabIndex = 1;
      this.dt_dateparam.ValuesFont = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.dt_dateparam.OnDateChanged += new System.EventHandler(this.dt_dateparam_OnDateChanged);
      // 
      // btn_close
      // 
      this.btn_close.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_close.FlatAppearance.BorderSize = 0;
      this.btn_close.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_close.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_close.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_close.Image = null;
      this.btn_close.IsSelected = false;
      this.btn_close.Location = new System.Drawing.Point(12, 592);
      this.btn_close.Name = "btn_close";
      this.btn_close.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_close.Size = new System.Drawing.Size(147, 59);
      this.btn_close.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_close.TabIndex = 105;
      this.btn_close.TabStop = false;
      this.btn_close.Text = "XCLOSE/CANCEL";
      this.btn_close.UseVisualStyleBackColor = false;
      this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
      // 
      // btn_refresh
      // 
      this.btn_refresh.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_refresh.FlatAppearance.BorderSize = 0;
      this.btn_refresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_refresh.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_refresh.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_refresh.Image = null;
      this.btn_refresh.IsSelected = false;
      this.btn_refresh.Location = new System.Drawing.Point(535, 605);
      this.btn_refresh.Name = "btn_refresh";
      this.btn_refresh.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_refresh.Size = new System.Drawing.Size(32, 32);
      this.btn_refresh.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_refresh.TabIndex = 106;
      this.btn_refresh.TabStop = false;
      this.btn_refresh.UseVisualStyleBackColor = false;
      this.btn_refresh.Click += new System.EventHandler(this.btn_refresh_Click);
      // 
      // lbl_total_entraces
      // 
      this.lbl_total_entraces.BackColor = System.Drawing.Color.Transparent;
      this.lbl_total_entraces.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_total_entraces.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_total_entraces.Location = new System.Drawing.Point(581, 618);
      this.lbl_total_entraces.Name = "lbl_total_entraces";
      this.lbl_total_entraces.Size = new System.Drawing.Size(150, 23);
      this.lbl_total_entraces.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_total_entraces.TabIndex = 107;
      this.lbl_total_entraces.Text = "xCount Entr.:";
      this.lbl_total_entraces.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_entrance_count
      // 
      this.lbl_entrance_count.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_entrance_count.BackColor = System.Drawing.Color.Transparent;
      this.lbl_entrance_count.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_entrance_count.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_entrance_count.Location = new System.Drawing.Point(733, 618);
      this.lbl_entrance_count.Name = "lbl_entrance_count";
      this.lbl_entrance_count.Size = new System.Drawing.Size(924, 23);
      this.lbl_entrance_count.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_entrance_count.TabIndex = 99;
      this.lbl_entrance_count.Text = "99,999";
      this.lbl_entrance_count.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // chk_players_on_site
      // 
      this.chk_players_on_site.AutoSize = true;
      this.chk_players_on_site.BackColor = System.Drawing.Color.Transparent;
      this.chk_players_on_site.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
      this.chk_players_on_site.Cursor = System.Windows.Forms.Cursors.Default;
      this.chk_players_on_site.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.chk_players_on_site.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.chk_players_on_site.Location = new System.Drawing.Point(812, 614);
      this.chk_players_on_site.MinimumSize = new System.Drawing.Size(200, 30);
      this.chk_players_on_site.Name = "chk_players_on_site";
      this.chk_players_on_site.Padding = new System.Windows.Forms.Padding(5);
      this.chk_players_on_site.Size = new System.Drawing.Size(200, 33);
      this.chk_players_on_site.TabIndex = 108;
      this.chk_players_on_site.Text = "xPlayersOnSite";
      this.chk_players_on_site.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      this.chk_players_on_site.UseVisualStyleBackColor = true;
      // 
      // frm_reception_visits_report
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(1024, 713);
      this.Name = "frm_reception_visits_report";
      this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
      this.Style = WSI.Cashier.Controls.frm_base.BaseFormStyle.PRINCIPAL;
      this.Text = "Vouchers Reprint";
      this.pnl_data.ResumeLayout(false);
      this.pnl_data.PerformLayout();
      this.gb_last_operations.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.dgv_last_operations)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private Controls.uc_round_panel gb_last_operations;
    private Controls.uc_DataGridView dgv_last_operations;
    private Controls.uc_label lbl_total_records;
    private Controls.uc_label lbl_record_count;
    private Controls.uc_label lbl_total_price;
    private Controls.uc_label lbl_price_total;
    private uc_datetime dt_dateparam;
    private Controls.uc_round_button btn_close;
    private Controls.uc_round_button btn_refresh;
    private Controls.uc_label lbl_entrance_count;
    private Controls.uc_label lbl_total_entraces;
    private Controls.uc_checkBox chk_players_on_site;

  }
}