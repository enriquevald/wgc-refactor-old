//------------------------------------------------------------------------------
// Copyright � 2009 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: ProfilePermissions.cs
// 
//   DESCRIPTION: Class to manage an the control of functional operation permissions.
// 
//        AUTHOR: Ronald Rodr�guez T.
// 
// CREATION DATE: 13-NOV-2009
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 13-NOV-2009 RRT    First release.
// 22-MAY-2012 RCI    Added a parameter in function CheckPermissions() to allow to NOT reset the authorized user.
// 29-AUG-2013 JCOR   Added AuthorizedByUserId AuthorizedByUserName.
// 13-SEP-2013 QMP    Added Work Shifts
// 10-OCT-2013 NMR    Added new TITO permissions with tickets
// 14-OCT-2013 RMS    Added scpecific permissions for account creation and edition depending on type: anonymous or personal.
// 26-MAR-2014 RRR    Added permissions check in Mobile Bank select by name button
// 16-APR-2014 JCOR   Load user's permissions on memory.
// 12-AUG-2014 DCS    Add aditional permission for edit points in player tracking
// 26-AUG-2014 DHA    Add aditional permission for gambling table mode
// 28-APR-2015 FJC    Add permissionsd for WinLossStatement Request
// 04-SEP-2015 GMV    Add permissions for last bill & tickets (TFS 4096 / �ltimos billetes y tiquets ingresados)
// 29-OCT-2015 ETP    Product Backlog Item: 4695 Gaming-Hall permissions.
// 11-NOV-2015 FOS    Product Backlog Item: 3709 Payment tickets & handpays
// 19-JAN-2016 RAB    Product Backlog Item 7941: Multiple buckets: Cajero: A�adir/Restar valores a los buckets
// 22-JAN-2016 YNM    Product Backlog Item 6656:Visitas / Recepci�n: Crear ticket de entrada
// 25-JAN-2016 AVZ    Product Backlog Item 8253:Visitas / Recepci�n: Modificaciones Review
// 08-FEB-2016 RGR    Product Backlog Item 6462: Garcia River-08: Cash Promotions NR: Print Ticket TITO or assign account
// 09-FEB-2016 FAV    Add permission for void a Machine Ticket
// 15-FEB-2016 AVZ    Product Backlog Item 9046:Visitas / Recepci�n: BlackLists
// 07-MAR-2016 LTC    Product Backlog Item 10227:TITA: TITO ticket exchange for gambling chips
// 07-JUL-2016 ESE    Product Backlog Item 14984:TPV Televisa: Set permissions to credit card forms
// 13-JUL-2016 ESE    Product Backlog Item 15239:TPV Televisa: Cashier, totalizing strip
// 18-JUL-2016 FJC    PBI 15447:Visitas / Recepci�n: MEJORAS - Cajero - Mejorar mensaje de error "...documentos expirados o lista de prohibidos..."
// 30-SEP-2016 LTC    PBItem 17964:Televisa - Cashdesk draw (Participation prize) - Configuration
// 11-NOV-2016  ESE   PBI 19932: Add permisions to Gaming table menu button
// 28-MAR-2017 RAB    PBI 25976: MES10 Ticket validation - Buy-in
// 20-APR-2017 RAB    PBI 26809: Drop gaming table information - Actual drop
// 08-MAY-2017 RAB    PBI 27177: Win / loss - Access to introduction screen
// 14-JUN-2017 DHA    PBI 27996:WIGOS-2482 - Dealer copy ticket - Validation in gaming table
// 19-JUN-2017 FGB    PBI 28031: Gaming table tournament - Main screen
// 26-JUN-2017 ATB    PBI 28033: Gaming table tournament - UnRebuy player voucher
// 05-JUL-2017 ATB    PBI 28454: Gaming table tournament - tournament winner assignment
// 03-OCT-2017 RAB    PBI 30021:WIGOS-4048 Payment threshold authorisation - Recharge authorisation
// 03-OCT-2017 RAB    PBI 30018:WIGOS-4036 Payment threshold authorization - Payment authorisation
// 06-MAR-2018 EOR    Bug 31759: WIGOS-8452 [Ticket #12485] Bot�n Reabir Caja
// 02-MAY-2018 RGR    PBI 32503: WIGOS-10392 C2Go - Implement the new cancellation movement of a C2Go recharge
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Windows.Forms;
using WSI.Common;

namespace WSI.Cashier
{
  public static class ProfilePermissions
  {
    #region Constants
    #endregion

    #region Enums

    public enum TypeOperation
    {
      ShowMsg = 0,
      RequestPasswd = 1,
      OnlyCheck = 2,
    };

    public enum CashierFormFuncionality   // Must match ENUM_CASHIER_FORM (GUI)
    {
      CardAddPromoNRCredit = 1
    , OpenCloseCash = 2
    , DepositCash = 3
    , WithdrawnCash = 4
    , CardAddCredit = 5
    , CardLastMovements = 6
    , AccountAsociateCard = 7
    , AccountEditPersonal = 8
    , AccountLock = 9
    , PrintCard = 10
    , OptionsDBConfig = 11
    , OptionsLanguage = 12
    , OptionsCalibrate = 13
    , GameSessions = 14
    , MobileBank = 15
    , AccessCash = 16
    , CardHandPay = 17
    , CardHandPayCancel = 18
    , AccountLogOff = 19
    , ManualHandPay = 20
    , ManualHandPayCancel = 21
    , GiftRequest = 22
    , GiftDelivery = 23
    , AccountLevel = 24
    , PromoSpecialPermissionA = 25
    , PromoSpecialPermissionB = 26
    , GiftPrintList = 27
    , DrawTicketsPrint = 28
    , AccountSearchByName = 29
    , CardAddCreditMaxAllowed = 30
    , CardAddCreditMaxAllowedDaily = 31
    , AutomaticPrintMobileBank = 32
    , BalanceMismatchHighestBalance = 33
    , VoucherHistory = 34
    , VoucherHistoryPrint = 35
    , VoucherHistoryPrintAll = 36
    , WitholdDocument = 37
    , RecyleCard = 38
    , PromotionCancel = 39
    , GiftCancel = 40
    , DuplicateAccount = 41
    , PaymentOrder = 42
    , CardSubtractCreditMaxAllowed = 43
    , CashOpeningMaxAllowedDaily = 44
    , TITO_CreateRETicket = 45
    , TITO_RedeemTicket = 46
    , TITO_ReprintTicket = 47
    , AccountEditAnonymous = 48
    , AccountCreatePersonal = 49
    , AccountCreateAnonymous = 50
    , UndoLastOperation = 51
    , AML_ExceedReportLimitRecharge = 52
    , AML_ExceedReportLimitPrize = 53
    , AML_ExceedMaximumLimitRecharge = 54
    , AML_ExceedMaximumLimitPrize = 55
    , ShowBalanceInfo = 56
    , TITO_RedeemTicketOffline = 57
    , ChipsSale = 58
    , ChipsPurchase = 59
    , TITO_HandPay = 60
    , ChipsPurchase_MaxAllowed = 61
    , ChipsSales_MaxAllowed = 62
    //, ChipsStockControl = 63
    , UndoLastChipsPurchase = 64
    , UndoLastChipsSale = 65
    , TransferAllRedeemableCredit = 66
    , TITO_ValidateTicket = 67
    , TITO_DiscardTicket = 68
      // RRR 26-MAR-2014
    , MobileBankSearchByName = 69
    , CardPartialRedeemCredit = 70
    , CardTotalRedeemCredit = 71
    , CardReplacementForFree = 72
    , PlayerTrackingExecute = 73
    , PlayerTrackingEditPoints = 74
    , ChipsSaleRegister = 75
    , CashierGamblingTableMode = 76
    , CashAdvance = 77
    , UndoLastCashAdvance = 78
    , EditBankCardTransactionData = 79
    , OthersAccess = 80
    , HandpayAuthorization = 81
    , HandpayApplyNoTax = 82
    , HandpayVoid = 83
      // OPC 13-OCT-2014
    , EditCommentsRequiredReading = 84
    , MobileBankChangeLimits = 85 
    , TITO_CreatePromoRETicket = 86
    , TITO_CreatePromoNRTicket = 87
    , CardAddPromoRE = 88
    , CardAddCreditRE = 89
    , MBUndoLastCashIn = 90
    , MBUndoLastDeposit = 91
      // OPC 18-FEB-2015
    , SlotAttendantPayAuthorization = 92
    , GamingTableOpen = 93
    , GamingTableFillerIn = 94
    , GamingTableFillerOut = 95
    , GamingTableClose = 96
    , GamingTableReport = 97
    , AccountGenerateRFC = 98
      // JBC 10-MAR-2015
    , GamingDayCloseToEndSelector = 99
    , TITO_AllowTicketPaymentAfterExpirationDate = 100
    , WinLossStatementRequest = 101
    , UndoWithdrawn = 102
      // GMV 04-SEP-2015
    , TITO_ListLastBillAndTickets = 103
      // ETP 27-OCT-2015
    , GAMINGHALL_Collect = 104
    , GAMINGHALL_ReadMeters = 105
    , GAMINGHALL_HandPay = 106
    , HandpayTaxCollector = 107
    , CustomersReceptionSearch = 108
    , GAMINGHALL_WriteMeters = 109
    //, CardRedeemSuspiciousActivity = 110
      // RAB 19-JAN-2016
    , SetBuckets = 111
    , Reception_ChangePrice = 112 
    , Reception_ReprintEntry = 113
    , Reception_AllowEntryWithExpiredDocuments = 114
      // RGR 04-FEB-2016
    , TITO_AssignPromoNR = 115
    , TITO_VoidMachineTicket = 116

      // RGR 09-MAR-16
    , TITO_ChipsSwap = 117

      // 15-FEB-2016 AVZ
    , Reception_AllowRemoveAccountFromLocalBlackList = 118

      // FAV 11-MAR-2016
    , GAMINGHALL_Refill = 119

      //ETP 03-MAY-2016
    , MultiCurrency_Exchange = 120
    , MultiCurrency_Return = 121
    , MultiCurrency_ExceedMaxReturn = 122
    , MultiCurrency_AllowNoComission = 123
    , CashAdvandce_AllowNoComission = 124
    , UndoLastCurrencyExchange = 125
    , UndoLastCurrencyDevolution = 126

    , Reception_AllowAddAccountToLocalBlackList = 127
    , CashAdvandce_Return = 128

      // AVZ 16-06-2016 Prize Payout Permission 
    , PrizePayout = 129

      //ETP 03-JUN-2016
     , PinPad_enable = 130

      //ESE 07-JUL-2016
      , Credit_card_recharge = 131
      , Credit_card_cash_advance = 132

      //ESE 13-JUL-2016
      , Totalizing_strip = 133

      //FJC 15-JUL-2016 PBI 15447:Visitas / Recepci�n: MEJORAS - Cajero - Mejorar mensaje de error
      , Reception_AllowEntryBeingInBlackList = 134

      //LTC 30-SEP-2016
      , RedeemWithoutPlay = 135

      //JRC 09-NOV-2016
      ,C2GO_activate_account = 136
      ,C2GO_block_account = 137
      ,C2GO_update_info_account = 138
      ,C2GO_deposit_into_wigos = 139
      ,C2GO_withdrawal_from_wigos = 140

      //ESE 11-NOV-2016
      , Gaming_Tables_Btn = 141

      , CashierSessionReOpen  = 142     //FGB 03-NOV-2016 Reopen gambling table session
      , GamingTableReOpen = 143  //FGB 03-NOV-2016 Reopen cashier session

      // CCG 13-MAR-2017
      , Reception_DeleteCoincidenceInBlackList = 144

      // ETP 16-MAR-2017
      , CreditLine_Withdrawal_With_Debt = 145
      // DPC 21-MAR-2017
      ,
      CreditLine_Get_Marker_TTO = 146 // Obsolette
      // ETP 16-MAR-2017
      , CreditLine_Payback = 147 
      // RAB 28-MAR-2017
      , Chips_Buy_In = 148,

      // RAB 20-APR-2017
      GamingTablesHourlyDropReport = 150,
      // RAB 08-MAY-2017
      GamingTablesWinLoss = 151,
      // DHA 14-JUN-2017
      GamingTablesCheckAccountValidationTicket = 152,

      //FGB 19-JUN-2017: PBI 28031: Gaming table tournament - Main screen
      GamingTablesTournamentEnroll = 153,
      GamingTablesTournamentRebuy = 154,
      GamingTablesTournamentPrizePayment = 155,
      GamingTablesTournamentStart = 156,
      GamingTablesTournamentEnd = 157,
      GamingTablesTournamentWinnerAssignment = 158,

      //RAB 03-OCT-2017: Recharge and payment threshold permission
      TITO_PaymentThreshold = 159,
      TITO_RechargeThreshold = 160,

      //FGB 12-FEB-2018 Access to mobibank
      //MobibankAccess = 161,
      //DMT 29-MAY-2018 Access to mobibank
      MobibankAccess = 684, 

      //RGR 02-MAY-2018
      C2GO_UndoLastRechargeOperation = 162
    };

    #endregion

    #region Class Attributes

    private static DataTable m_permissions_table;
    private static UserPermissions m_user_permissions;
    private static Dictionary<Int32, UserPermissions> m_permissions_dictionary;

    private class UserPermissions
    {
      public Int32 UserIdKey;
      public Int64 TickRead;
      public DataTable Permissions;

      //------------------------------------------------------------------------------
      // PURPOSE : Delete outdated users from the dictionary.
      //
      //  PARAMS :
      //      - INPUT :
      //        - Boolean MustDeleteAll.
      //
      //      - OUTPUT :
      //
      // RETURNS :
      //
      public void DeletePermissionsUser(Boolean MustDeleteAll)
      {
        List<Int32> up_to_delete_dic = new List<int>();
        Int64 _elapsed;
        Int32 _reload_user;

        _reload_user = GeneralParam.GetInt32("Cashier", "ReloadUserPermissionsMinutes", 15 * 60000, 1, 60000 * 24);

        foreach (KeyValuePair<int, UserPermissions> _user_permissions in m_permissions_dictionary)
        {
          _elapsed = Misc.GetElapsedTicks((Int32)_user_permissions.Value.TickRead);

          if ((MustDeleteAll) || (_elapsed >= _reload_user * 60000))
          {
              up_to_delete_dic.Add(_user_permissions.Key);
            }
          }

        foreach (int key in up_to_delete_dic)
        {
          m_permissions_dictionary.Remove(key);
        }
      } // DeletePermissionsUser

      //------------------------------------------------------------------------------
      // PURPOSE : Initializes and does all the process of load users and their
      //           permissions in memory.
      //
      //  PARAMS :
      //      - INPUT :
      //        - Int32 UserIdToCheck.
      //
      //      - OUTPUT :
      //
      // RETURNS :
      //
      private void ReLoad(Int32 UserIdToCheck)
      {
        try
        {
          if (m_permissions_dictionary == null)
          {
            m_permissions_dictionary = new Dictionary<int, UserPermissions>();
          }

          // Delete permissions from an old users, specified in generalparam.
          DeletePermissionsUser(false);

          if (!m_permissions_dictionary.ContainsKey(UserIdToCheck))
          {
            GetPermissionsFromDB(UserIdToCheck);
            LoadPermissionsIntoDictionary(UserIdToCheck);
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }
      } // ReLoad

      //------------------------------------------------------------------------------
      // PURPOSE : Checks if the dictionary user has the permissions list.
      //
      //  PARAMS :
      //      - INPUT :
      //        - Int32 UserIdToCheck.
      //        - List<CashierFormFuncionality> PermissionList.
      //
      //      - OUTPUT :
      //
      // RETURNS :
      //   
      public Boolean CheckPermissionsNew(Int32 UserIdToCheck, List<CashierFormFuncionality> PermissionList)
      {
        DataRow[] _dr;
        UserPermissions _user_permissions;

        ReLoad(UserIdToCheck);

        if (!m_permissions_dictionary.ContainsKey(UserIdToCheck))
        {
          return false;
        }

        m_permissions_dictionary.TryGetValue(UserIdToCheck, out _user_permissions);

        foreach (CashierFormFuncionality _form_function in PermissionList)
        {
          _dr = _user_permissions.Permissions.Select("GPF_FORM_ID = " + (Int32)_form_function);

          if (_dr.Length == 0)
          {
            return false;
          }
        }

        return true;
      } // CheckPermissionsNew
    }
    #endregion

    #region Constructor

    #endregion

    #region Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Inserts user/permissions into the dictionary.
    //
    //  PARAMS :
    //      - INPUT :
    //        - Int32 UserIdToCheck.
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //   
    static void LoadPermissionsIntoDictionary(Int32 UserIdToCheck)
    {
      UserPermissions _up = new UserPermissions();

      _up.UserIdKey = UserIdToCheck;
      _up.Permissions = m_permissions_table;
      _up.TickRead = Misc.GetTickCount();

      m_permissions_dictionary.Add(_up.UserIdKey, _up);
    } // LoadPermissionsIntoDictionary

    //------------------------------------------------------------------------------
    // PURPOSE : Loads the datatable from database.
    //
    //  PARAMS :
    //      - INPUT :
    //        - Int32 UserId.
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //   
    static void GetPermissionsFromDB(Int32 UserIdToCheck)
    {
      StringBuilder _sb;
      DataColumn _column;

      m_permissions_table = new DataTable();

      // Form Id.
      _column = m_permissions_table.Columns.Add("GPF_FORM_ID");
      _column.DataType = typeof(Int64);
      _column.Unique = true;

      // Read permission.
      _column = m_permissions_table.Columns.Add("GPF_READ_PERM");
      _column.DataType = typeof(Boolean);

      m_permissions_table.PrimaryKey = new DataColumn[] { m_permissions_table.Columns["GPF_FORM_ID"] };

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT   GPF_FORM_ID                       ");
        _sb.AppendLine("        , GPF_READ_PERM                     ");
        _sb.AppendLine("        , GU_USER_ID                        ");
        _sb.AppendLine("   FROM   GUI_PROFILE_FORMS                 ");
        _sb.AppendLine("        , GUI_USERS                         ");
        _sb.AppendLine("  WHERE   GPF_PROFILE_ID  =  GU_PROFILE_ID  ");
        _sb.AppendLine("    AND   GU_USER_ID      =  @pUserId       ");
        _sb.AppendLine("    AND   GPF_GUI_ID      =  @pGuiId        ");
        _sb.AppendLine("    AND   GPF_READ_PERM   =  1              ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = UserIdToCheck;
            _sql_cmd.Parameters.Add("@pGuiId", SqlDbType.Int).Value = (int)ENUM_GUI.CASHIER;

            m_permissions_table.Load(_db_trx.ExecuteReader(_sql_cmd));
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } // GetPermissionsFromDB

    #endregion

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE: Deletes all users and permissions on memory.
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    public static void ClearBufferPermissions()
    {
      m_user_permissions.DeletePermissionsUser(true);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Check permisions of some funcionalities from table GUI_PROFILE_FORMS.
    //          User must have all the permissions in the list.
    //
    //  PARAMS:
    //      - INPUT:
    //        - CashierFormFuncionality FormId
    //        - TypeOperation ControlOption
    //        - Form Parent
    //
    //      - OUTPUT:
    //        - String ErrorStr
    //
    // RETURNS:
    //    - true: User has permission.
    //    - false: No permission.
    //
    public static Boolean CheckPermissions(CashierFormFuncionality FormId,
                                           TypeOperation ControlOption,
                                           Form Parent,
                                           out String ErrorStr)
    {
      return CheckPermissions(FormId, ControlOption, Parent, true, out ErrorStr);
    } // CheckPermissions

    public static Boolean CheckPermissions(CashierFormFuncionality FormId,
                                           TypeOperation ControlOption,
                                           Form Parent,
                                           Boolean ResetAuthorized,
                                           out String ErrorStr)
    {
      Boolean _has_permissions;

      ErrorStr = String.Empty;

      if (ResetAuthorized)
      {
        // TJG 16-JUL-2010
        // Set Current User as the Authorized By User as default
        Cashier.AuthorizedByUserId = Cashier.UserId;
        Cashier.AuthorizedByUserName = Cashier.UserName;

        WSI.Common.CommonCashierInformation.AuthorizedByUserId = Cashier.UserId;
        WSI.Common.CommonCashierInformation.AuthorizedByUserName = Cashier.UserName;
      }

      if (m_user_permissions == null)
      {
        m_user_permissions = new UserPermissions();
      }

      List<CashierFormFuncionality> _permissions_list = new List<CashierFormFuncionality>();

      _permissions_list.Add(FormId);

      _has_permissions = m_user_permissions.CheckPermissionsNew(Cashier.UserId, _permissions_list);

      if (!_has_permissions)
      {
        ErrorStr = Resource.String("STR_FRM_USER_LOGIN_ERROR_LOGIN_PERMS");

        return CheckPermissionsOptions(FormId, ErrorStr, ControlOption, Parent);
      }

      return true;
    } // CheckPermissions

    //------------------------------------------------------------------------------
    // PURPOSE: Show Message, Request Password or Only Check for funcionalities.
    //
    //  PARAMS:
    //      - INPUT:
    //        - CashierFormFuncionality FormId
    //        - String ErrorStr
    //        - TypeOperation ControlOption
    //        - Form Parent
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //    - true: The ControlOption is Request Password and the new user entered has permission.
    //    - false: For the other cases of ControlOption: No permission.
    //
    public static Boolean CheckPermissionsOptions(CashierFormFuncionality FormId,
                                                  String ErrorStr,
                                                  TypeOperation ControlOption,
                                                  Form Parent)
    {
      Boolean _allowed_flag;

      _allowed_flag = false;

      switch (ControlOption)
      {
        case TypeOperation.ShowMsg: // Show message
          frm_message.Show(ErrorStr, Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, Parent);
          Parent.Focus();
          break;

        case TypeOperation.RequestPasswd: // Request authorized user credentials. Show login form
          using (frm_user_login login_screen = new frm_user_login(frm_user_login.LOGIN_MODE_AUTHORIZATION))
          {
          login_screen.Show(FormId, Parent, out _allowed_flag);
          }
          break;

        case TypeOperation.OnlyCheck: // Only check
          break;

        default:
          break;
      }

      return _allowed_flag;
    } //CheckPermissionsOptions

    public static Boolean CheckPermissionList(CashierFormFuncionality Permission,
                                              TypeOperation ControlOption,
                                              Form Parent,
                                              Int32 UserId,
                                              out String ErrorStr)
    {
      List<CashierFormFuncionality> _list_permissions;

      _list_permissions = new List<CashierFormFuncionality>(1);
      _list_permissions.Add(Permission);

      return CheckPermissionList(_list_permissions, ControlOption, Parent, UserId, out ErrorStr);
    } // CheckPermissionList

    //------------------------------------------------------------------------------
    // PURPOSE: Check the list of permisions of some funcionalities from table GUI_PROFILE_FORMS.
    //          User must have all the permissions in the list.
    //
    //  PARAMS:
    //      - INPUT:
    //        - List<CashierFormFuncionality> PermissionList
    //        - TypeOperation ControlOption
    //        - Form Parent
    //        - Int32 UserId
    //
    //      - OUTPUT:
    //        - String ErrorStr
    //
    // RETURNS:
    //    - true: User has all the permissions in the list.
    //    - false: No permission.
    //
    public static Boolean CheckPermissionList(List<CashierFormFuncionality> PermissionList,
                                              TypeOperation ControlOption,
                                              Form Parent,
                                              Int32 UserId,
                                              out String ErrorStr)
    {
      Boolean _allowed_flag;
      Int32 _user_id;
      Boolean _has_permissions;

      ErrorStr = string.Empty;

      // TJG 16-JUL-2010
      // Set Current User as the Authorized By User as default
      if (UserId == 0)
      {
        _user_id = Cashier.UserId;

        Cashier.AuthorizedByUserId = Cashier.UserId;
        Cashier.AuthorizedByUserName = Cashier.UserName;

        WSI.Common.CommonCashierInformation.AuthorizedByUserId = Cashier.UserId;
        WSI.Common.CommonCashierInformation.AuthorizedByUserName = Cashier.UserName;
      }
      else
      {
        _user_id = UserId;
      }

      if (m_user_permissions == null)
      {
        m_user_permissions = new UserPermissions();
      }

      _has_permissions = m_user_permissions.CheckPermissionsNew(_user_id, PermissionList);

      if (!_has_permissions)
      {
        ErrorStr = Resource.String("STR_FRM_USER_LOGIN_ERROR_LOGIN_PERMS");
        _allowed_flag = CheckPermissionListOptions(PermissionList, ErrorStr, ControlOption, Parent);

        return _allowed_flag;
      }

      return true;
    } // CheckPermissionList

    //------------------------------------------------------------------------------
    // PURPOSE: Show Message, Request Password or Only Check for the list of funcionalities.
    //
    //  PARAMS:
    //      - INPUT:
    //        - List<CashierFormFuncionality> PermissionList
    //        - String ErrorStr
    //        - TypeOperation ControlOption
    //        - Form Parent
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //    - true: The ControlOption is Request Password and the new user entered has all the permissions in the list.
    //    - false: For the other cases of ControlOption: No permission.
    //
    public static Boolean CheckPermissionListOptions(List<CashierFormFuncionality> PermissionList,
                                                     String ErrorStr,
                                                     TypeOperation ControlOption,
                                                     Form Parent)
    {
      Boolean _allowed_flag;

      _allowed_flag = false;

      switch (ControlOption)
      {
        case TypeOperation.ShowMsg: // Show message
          frm_message.Show(ErrorStr, Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, Parent);
          Parent.Focus();
          break;

        case TypeOperation.RequestPasswd: // Request authorized user credentials. Show login form
          using (frm_user_login login_screen = new frm_user_login(frm_user_login.LOGIN_MODE_MULTI_AUTHORIZATION))
          {
          login_screen.Show(PermissionList, Parent, out _allowed_flag);
          }
          break;

        case TypeOperation.OnlyCheck: // Only check
          break;

        default:
          break;
      }

      return _allowed_flag;
    } //CheckPermissionListOptions

    #endregion

  } // Class ProfilePermissions
}
