//------------------------------------------------------------------------------
// Copyright � 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_gaming_table_select.cs
// 
//   DESCRIPTION: Implements the following classes:
//
//        AUTHOR: Ram�n Moncl�s
// 
// CREATION DATE: 21-DEC-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 21-DIC-2013 RMS    First release.
// 21-AUG-2014 DRV    Fixed Bug WIG-1193: No one should have to select a table for a chip sale register
//------------------------------------------------------------------------------
//

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WSI.Common;

namespace WSI.Cashier
{
  public partial class frm_gaming_table_select : WSI.Cashier.Controls.frm_base
  {
    public frm_gaming_table_select()
    {
      InitializeComponent();

      InitializeControls();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Init and Show the form.
    //
    //  PARAMS :
    //      - INPUT :
    //          - ParentForm
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public Boolean Show(String Title, out WSI.Common.LinkedGamingTable GamingTable)
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_gaming_table_select", Log.Type.Message);
      GamingTable = null;

      try
      {

        GamingTable = new WSI.Common.LinkedGamingTable();

        this.lbl_gaming_table_sel_title.Text = Title;

        this.FormTitle = Title.ToUpper();

        this.uc_gaming_table_filter1.LoadData();

        this.bt_ok.Enabled = uc_gaming_table_filter1.SelectedGamingTable().TableId > -1;

        //this.ShowCloseButton = false;
        
        this.ShowDialog();

        if (this.DialogResult == DialogResult.OK)
        {
          GamingTable = uc_gaming_table_filter1.SelectedGamingTable();

        }
        else
        {
          GamingTable = null;
          return false;
        }

      }
      catch 
      {

      }

      return true;
    }

    //
    private void btn_close_Click(object sender, EventArgs e)
    {
      this.DialogResult = DialogResult.Cancel; 
    }

    //
    private void bt_ok_Click(object sender, EventArgs e)
    {
      this.DialogResult = DialogResult.OK;

    } 

    //
    private void InitializeControls()
    {
      //this.gb_search.Text = Resource.String("STR_FRM_GAMING_TABLE_SELECT_GROUP_BOX");
      this.bt_ok.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_OK");
      this.btn_close.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");

      this.bt_ok.Enabled = false;
    }

    private void uc_gaming_table_filter1_GamingTableSelected_1(object sender, WSI.Cashier.uc_gaming_table_filter.GamingTableFilterGridChange e)
    {
      this.bt_ok.Enabled = (e.SelectedRow != -1);
    }

    private void frm_gaming_table_select_Shown(object sender, EventArgs e)
    {
      this.uc_gaming_table_filter1.Focus();
    }

    private void pnl_data_Paint(object sender, PaintEventArgs e)
    {

    }

  }
}