namespace WSI.Cashier
{
  partial class uc_button_panel
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.ContainerPanels = new System.Windows.Forms.FlowLayoutPanel();
      this.btn_close = new System.Windows.Forms.Button();
      this.ContainerPanels.SuspendLayout();
      this.SuspendLayout();
      // 
      // ContainerPanels
      // 
      this.ContainerPanels.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
      this.ContainerPanels.BackColor = System.Drawing.Color.CornflowerBlue;
      this.ContainerPanels.Controls.Add(this.btn_close);
      this.ContainerPanels.Dock = System.Windows.Forms.DockStyle.Fill;
      this.ContainerPanels.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
      this.ContainerPanels.Location = new System.Drawing.Point(0, 0);
      this.ContainerPanels.Name = "ContainerPanels";
      this.ContainerPanels.Size = new System.Drawing.Size(113, 286);
      this.ContainerPanels.TabIndex = 25;
      // 
      // btn_close
      // 
      this.btn_close.BackColor = System.Drawing.Color.LightSkyBlue;
      this.btn_close.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
      this.btn_close.FlatAppearance.BorderSize = 0;
      this.btn_close.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btn_close.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
      this.btn_close.ImageIndex = 0;
      this.btn_close.Location = new System.Drawing.Point(0, 0);
      this.btn_close.Margin = new System.Windows.Forms.Padding(0);
      this.btn_close.Name = "btn_close";
      this.btn_close.Padding = new System.Windows.Forms.Padding(0, 0, 4, 0);
      this.btn_close.Size = new System.Drawing.Size(112, 48);
      this.btn_close.TabIndex = 70;
      this.btn_close.Text = "xBack";
      this.btn_close.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.btn_close.UseVisualStyleBackColor = false;
      this.btn_close.Click += new System.EventHandler(this.Button_Click);
      // 
      // uc_button_panel
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
      this.Controls.Add(this.ContainerPanels);
      this.Name = "uc_button_panel";
      this.Size = new System.Drawing.Size(113, 286);
      this.ContainerPanels.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.FlowLayoutPanel ContainerPanels;
    private System.Windows.Forms.Button btn_close;
  }
}
