namespace WSI.Cashier
{
  partial class frm_catalog
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }
    
    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_catalog));
      this.cb_reason = new WSI.Cashier.Controls.uc_round_combobox();
      this.btn_cancel = new WSI.Cashier.Controls.uc_round_button();
      this.lbl_block_unblock_description = new WSI.Cashier.Controls.uc_label();
      this.btn_accept = new WSI.Cashier.Controls.uc_round_button();
      this.lbl_unblock_description = new WSI.Cashier.Controls.uc_label();
      this.txt_reason = new WSI.Cashier.Controls.uc_round_textbox();
      this.btn_keyboard = new WSI.Cashier.Controls.uc_round_button();
      this.pnl_data.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.Controls.Add(this.btn_keyboard);
      this.pnl_data.Controls.Add(this.btn_cancel);
      this.pnl_data.Controls.Add(this.lbl_block_unblock_description);
      this.pnl_data.Controls.Add(this.btn_accept);
      this.pnl_data.Controls.Add(this.lbl_unblock_description);
      this.pnl_data.Controls.Add(this.txt_reason);
      this.pnl_data.Controls.Add(this.cb_reason);
      this.pnl_data.Size = new System.Drawing.Size(589, 340);
      // 
      // cb_reason
      // 
      this.cb_reason.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_reason.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.cb_reason.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.cb_reason.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.cb_reason.CornerRadius = 5;
      this.cb_reason.DataSource = null;
      this.cb_reason.DisplayMember = "";
      this.cb_reason.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
      this.cb_reason.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cb_reason.DropDownWidth = 568;
      this.cb_reason.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.cb_reason.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_reason.FormattingEnabled = true;
      this.cb_reason.IsDroppedDown = false;
      this.cb_reason.ItemHeight = 40;
      this.cb_reason.Location = new System.Drawing.Point(9, 43);
      this.cb_reason.MaxDropDownItems = 8;
      this.cb_reason.Name = "cb_reason";
      this.cb_reason.SelectedIndex = -1;
      this.cb_reason.SelectedItem = null;
      this.cb_reason.SelectedValue = null;
      this.cb_reason.SelectionLength = 0;
      this.cb_reason.SelectionStart = 0;
      this.cb_reason.Size = new System.Drawing.Size(568, 40);
      this.cb_reason.Sorted = false;
      this.cb_reason.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.BASIC;
      this.cb_reason.TabIndex = 0;
      this.cb_reason.TabStop = false;
      this.cb_reason.ValueMember = "";
      // 
      // btn_cancel
      // 
      this.btn_cancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_cancel.FlatAppearance.BorderSize = 0;
      this.btn_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_cancel.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_cancel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_cancel.Image = null;
      this.btn_cancel.IsSelected = false;
      this.btn_cancel.Location = new System.Drawing.Point(255, 262);
      this.btn_cancel.Name = "btn_cancel";
      this.btn_cancel.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_cancel.Size = new System.Drawing.Size(155, 60);
      this.btn_cancel.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_cancel.TabIndex = 3;
      this.btn_cancel.Text = "XCANCEL";
      this.btn_cancel.UseVisualStyleBackColor = false;
      this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
      // 
      // lbl_block_unblock_description
      // 
      this.lbl_block_unblock_description.AutoSize = true;
      this.lbl_block_unblock_description.BackColor = System.Drawing.Color.Transparent;
      this.lbl_block_unblock_description.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_block_unblock_description.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_block_unblock_description.Location = new System.Drawing.Point(6, 96);
      this.lbl_block_unblock_description.Name = "lbl_block_unblock_description";
      this.lbl_block_unblock_description.Size = new System.Drawing.Size(247, 18);
      this.lbl_block_unblock_description.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_block_unblock_description.TabIndex = 2;
      this.lbl_block_unblock_description.Text = "xDescriptionBlockUnblockReason";
      // 
      // btn_accept
      // 
      this.btn_accept.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_accept.FlatAppearance.BorderSize = 0;
      this.btn_accept.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_accept.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_accept.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_accept.Image = null;
      this.btn_accept.IsSelected = false;
      this.btn_accept.Location = new System.Drawing.Point(420, 262);
      this.btn_accept.Name = "btn_accept";
      this.btn_accept.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_accept.Size = new System.Drawing.Size(155, 60);
      this.btn_accept.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_accept.TabIndex = 2;
      this.btn_accept.Text = "XACCEPT";
      this.btn_accept.UseVisualStyleBackColor = false;
      this.btn_accept.Click += new System.EventHandler(this.btn_accept_Click);
      // 
      // lbl_unblock_description
      // 
      this.lbl_unblock_description.AutoSize = true;
      this.lbl_unblock_description.BackColor = System.Drawing.Color.Transparent;
      this.lbl_unblock_description.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_unblock_description.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_unblock_description.Location = new System.Drawing.Point(6, 14);
      this.lbl_unblock_description.Name = "lbl_unblock_description";
      this.lbl_unblock_description.Size = new System.Drawing.Size(241, 18);
      this.lbl_unblock_description.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_unblock_description.TabIndex = 0;
      this.lbl_unblock_description.Text = "xDescriptionBlockUnblockAction";
      // 
      // txt_reason
      // 
      this.txt_reason.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_reason.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_reason.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_reason.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_reason.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_reason.CornerRadius = 5;
      this.txt_reason.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_reason.Location = new System.Drawing.Point(9, 119);
      this.txt_reason.MaxLength = 255;
      this.txt_reason.Multiline = true;
      this.txt_reason.Name = "txt_reason";
      this.txt_reason.PasswordChar = '\0';
      this.txt_reason.ReadOnly = false;
      this.txt_reason.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
      this.txt_reason.SelectedText = "";
      this.txt_reason.SelectionLength = 0;
      this.txt_reason.SelectionStart = 0;
      this.txt_reason.Size = new System.Drawing.Size(568, 126);
      this.txt_reason.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.MULTILINE;
      this.txt_reason.TabIndex = 1;
      this.txt_reason.TabStop = false;
      this.txt_reason.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_reason.UseSystemPasswordChar = false;
      this.txt_reason.WaterMark = null;
      this.txt_reason.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // btn_keyboard
      // 
      this.btn_keyboard.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(47)))), ((int)(((byte)(50)))));
      this.btn_keyboard.FlatAppearance.BorderSize = 0;
      this.btn_keyboard.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_keyboard.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_keyboard.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_keyboard.Image = ((System.Drawing.Image)(resources.GetObject("btn_keyboard.Image")));
      this.btn_keyboard.IsSelected = false;
      this.btn_keyboard.Location = new System.Drawing.Point(11, 261);
      this.btn_keyboard.Name = "btn_keyboard";
      this.btn_keyboard.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_keyboard.Size = new System.Drawing.Size(70, 60);
      this.btn_keyboard.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.TERTIARY;
      this.btn_keyboard.TabIndex = 8;
      this.btn_keyboard.UseVisualStyleBackColor = false;
      this.btn_keyboard.Click += new System.EventHandler(this.btn_keyboard_Click);
      // 
      // frm_catalog
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(589, 395);
      this.Name = "frm_catalog";
      this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
      this.Style = WSI.Cashier.Controls.frm_base.BaseFormStyle.PRINCIPAL;
      this.Text = "Block / Unblock Account";
      this.pnl_data.ResumeLayout(false);
      this.pnl_data.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    //private System.Windows.Forms.SplitContainer splitContainer1;
    private WSI.Cashier.Controls.uc_label lbl_block_unblock_description;
    private WSI.Cashier.Controls.uc_label lbl_unblock_description;
    private WSI.Cashier.Controls.uc_round_textbox txt_reason;
    private WSI.Cashier.Controls.uc_round_button btn_cancel;
    private WSI.Cashier.Controls.uc_round_button btn_accept;
    private WSI.Cashier.Controls.uc_round_combobox cb_reason;
    private Controls.uc_round_button btn_keyboard;
  }
}