﻿namespace WSI.Cashier
{
  partial class uc_card_reader
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose (bool disposing)
    {
      if ( disposing && ( components != null ) )
      {
        components.Dispose ();
      }
      base.Dispose (disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent ()
    {
      this.components = new System.ComponentModel.Container();
      this.tmr_clear = new System.Windows.Forms.Timer(this.components);
      this.pictureBox1 = new System.Windows.Forms.PictureBox();
      this.lbl_swipe_card = new WSI.Cashier.Controls.uc_label();
      this.lbl_invalid_card = new WSI.Cashier.Controls.uc_label();
      this.txt_track_number = new WSI.Cashier.Controls.uc_round_textbox();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
      this.SuspendLayout();
      // 
      // tmr_clear
      // 
      this.tmr_clear.Interval = 250;
      this.tmr_clear.Tick += new System.EventHandler(this.tmr_clear_Tick);
      // 
      // pictureBox1
      // 
      this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
      this.pictureBox1.Image = global::WSI.Cashier.Properties.Resources.card_header1;
      this.pictureBox1.Location = new System.Drawing.Point(5, 5);
      this.pictureBox1.Margin = new System.Windows.Forms.Padding(0);
      this.pictureBox1.Name = "pictureBox1";
      this.pictureBox1.Size = new System.Drawing.Size(50, 50);
      this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
      this.pictureBox1.TabIndex = 1;
      this.pictureBox1.TabStop = false;
      this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
      // 
      // lbl_swipe_card
      // 
      this.lbl_swipe_card.BackColor = System.Drawing.Color.Transparent;
      this.lbl_swipe_card.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_swipe_card.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_swipe_card.Location = new System.Drawing.Point(98, 12);
      this.lbl_swipe_card.Name = "lbl_swipe_card";
      this.lbl_swipe_card.Size = new System.Drawing.Size(103, 36);
      this.lbl_swipe_card.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_swipe_card.TabIndex = 0;
      this.lbl_swipe_card.Text = "XINPUT CARD";
      // 
      // lbl_invalid_card
      // 
      this.lbl_invalid_card.BackColor = System.Drawing.Color.Transparent;
      this.lbl_invalid_card.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_invalid_card.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.lbl_invalid_card.Location = new System.Drawing.Point(469, 19);
      this.lbl_invalid_card.Name = "lbl_invalid_card";
      this.lbl_invalid_card.Size = new System.Drawing.Size(324, 23);
      this.lbl_invalid_card.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_RED;
      this.lbl_invalid_card.TabIndex = 2;
      this.lbl_invalid_card.Text = "Número de tarjeta no válido ...";
      this.lbl_invalid_card.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // txt_track_number
      // 
      this.txt_track_number.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_track_number.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_track_number.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_track_number.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_track_number.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_track_number.CornerRadius = 5;
      this.txt_track_number.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_track_number.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.txt_track_number.Location = new System.Drawing.Point(227, 10);
      this.txt_track_number.MaxLength = 32767;
      this.txt_track_number.Multiline = false;
      this.txt_track_number.Name = "txt_track_number";
      this.txt_track_number.PasswordChar = '●';
      this.txt_track_number.ReadOnly = false;
      this.txt_track_number.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_track_number.SelectedText = "";
      this.txt_track_number.SelectionLength = 0;
      this.txt_track_number.SelectionStart = 0;
      this.txt_track_number.Size = new System.Drawing.Size(226, 40);
      this.txt_track_number.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_track_number.TabIndex = 1;
      this.txt_track_number.Text = "12345678901234567890";
      this.txt_track_number.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.txt_track_number.UseSystemPasswordChar = false;
      this.txt_track_number.WaterMark = null;
      this.txt_track_number.TextChanged += new System.EventHandler(this.txt_track_number_TextChanged);
      this.txt_track_number.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_track_number_KeyPress);
      // 
      // uc_card_reader
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.lbl_swipe_card);
      this.Controls.Add(this.lbl_invalid_card);
      this.Controls.Add(this.pictureBox1);
      this.Controls.Add(this.txt_track_number);
      this.MaximumSize = new System.Drawing.Size(800, 90);
      this.MinimumSize = new System.Drawing.Size(800, 60);
      this.Name = "uc_card_reader";
      this.Size = new System.Drawing.Size(800, 60);
      this.VisibleChanged += new System.EventHandler(this.uc_card_reader_VisibleChanged);
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private WSI.Cashier.Controls.uc_round_textbox txt_track_number;
    private System.Windows.Forms.PictureBox pictureBox1;
    private System.Windows.Forms.Timer tmr_clear;
    private WSI.Cashier.Controls.uc_label lbl_invalid_card;
    private WSI.Cashier.Controls.uc_label lbl_swipe_card;
  }
}
