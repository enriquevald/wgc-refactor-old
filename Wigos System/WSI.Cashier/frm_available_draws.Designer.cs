using WSI.Cashier.Controls;
namespace WSI.Cashier
{
  partial class frm_available_draws
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.lbl_no_numbers_left = new WSI.Cashier.Controls.uc_label();
      this.btn_print_selected = new WSI.Cashier.Controls.uc_round_button();
      this.btn_close = new WSI.Cashier.Controls.uc_round_button();
      this.dgv_draws = new WSI.Cashier.Controls.uc_DataGridView();
      this.pnl_data.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dgv_draws)).BeginInit();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.Controls.Add(this.lbl_no_numbers_left);
      this.pnl_data.Controls.Add(this.btn_print_selected);
      this.pnl_data.Controls.Add(this.btn_close);
      this.pnl_data.Controls.Add(this.dgv_draws);
      this.pnl_data.Size = new System.Drawing.Size(710, 461);
      // 
      // lbl_no_numbers_left
      // 
      this.lbl_no_numbers_left.AutoSize = true;
      this.lbl_no_numbers_left.BackColor = System.Drawing.Color.Transparent;
      this.lbl_no_numbers_left.Font = new System.Drawing.Font("Montserrat", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_no_numbers_left.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_no_numbers_left.Location = new System.Drawing.Point(12, 377);
      this.lbl_no_numbers_left.Name = "lbl_no_numbers_left";
      this.lbl_no_numbers_left.Size = new System.Drawing.Size(195, 15);
      this.lbl_no_numbers_left.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK_BOLD;
      this.lbl_no_numbers_left.TabIndex = 29;
      this.lbl_no_numbers_left.Text = "* xNo Numbers left in the draw";
      // 
      // btn_print_selected
      // 
      this.btn_print_selected.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_print_selected.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_print_selected.FlatAppearance.BorderSize = 0;
      this.btn_print_selected.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_print_selected.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_print_selected.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_print_selected.Image = null;
      this.btn_print_selected.Location = new System.Drawing.Point(542, 387);
      this.btn_print_selected.Name = "btn_print_selected";
      this.btn_print_selected.Size = new System.Drawing.Size(155, 60);
      this.btn_print_selected.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_print_selected.TabIndex = 28;
      this.btn_print_selected.Text = "XPRINTSELECTED";
      this.btn_print_selected.UseVisualStyleBackColor = false;
      this.btn_print_selected.Click += new System.EventHandler(this.btn_print_selected_Click);
      // 
      // btn_close
      // 
      this.btn_close.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_close.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_close.FlatAppearance.BorderSize = 0;
      this.btn_close.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_close.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_close.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_close.Image = null;
      this.btn_close.Location = new System.Drawing.Point(379, 387);
      this.btn_close.Name = "btn_close";
      this.btn_close.Size = new System.Drawing.Size(155, 60);
      this.btn_close.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_close.TabIndex = 27;
      this.btn_close.Text = "XCLOSE";
      this.btn_close.UseVisualStyleBackColor = false;
      this.btn_close.Click += new System.EventHandler(this.btn_exit_Click);
      // 
      // dgv_draws
      // 
      this.dgv_draws.AllowUserToAddRows = false;
      this.dgv_draws.AllowUserToDeleteRows = false;
      this.dgv_draws.AllowUserToResizeColumns = false;
      this.dgv_draws.AllowUserToResizeRows = false;
      this.dgv_draws.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.None;
      this.dgv_draws.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.dgv_draws.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.dgv_draws.ColumnHeaderHeight = 35;
      this.dgv_draws.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
      this.dgv_draws.ColumnHeadersHeight = 35;
      this.dgv_draws.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
      this.dgv_draws.CornerRadius = 10;
      this.dgv_draws.DefaultCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.dgv_draws.DefaultCellForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.dgv_draws.EnableHeadersVisualStyles = false;
      this.dgv_draws.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.dgv_draws.GridColor = System.Drawing.Color.White;
      this.dgv_draws.HeaderBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
      this.dgv_draws.HeaderDefaultCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.dgv_draws.HeaderDefaultCellForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.dgv_draws.Location = new System.Drawing.Point(14, 14);
      this.dgv_draws.Name = "dgv_draws";
      this.dgv_draws.ReadOnly = true;
      this.dgv_draws.RowHeadersVisible = false;
      this.dgv_draws.RowHeadersWidth = 20;
      this.dgv_draws.RowTemplate.Height = 35;
      this.dgv_draws.RowTemplateHeight = 35;
      this.dgv_draws.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
      this.dgv_draws.Size = new System.Drawing.Size(683, 360);
      this.dgv_draws.Style = WSI.Cashier.Controls.uc_DataGridView.DataGridViewStyle.WITH_ALL_ROUND_CORNERS;
      this.dgv_draws.TabIndex = 26;
      this.dgv_draws.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_draws_CellClick);
      this.dgv_draws.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.dgv_draws_RowPrePaint);
      // 
      // frm_available_draws
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(710, 516);
      this.Name = "frm_available_draws";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Style = WSI.Cashier.Controls.frm_base.BaseFormStyle.PRINCIPAL;
      this.Text = "Available Draws";
      this.Shown += new System.EventHandler(this.OnShown);
      this.pnl_data.ResumeLayout(false);
      this.pnl_data.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dgv_draws)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private uc_label lbl_no_numbers_left;
    private uc_round_button btn_print_selected;
    private uc_round_button btn_close;
    private uc_DataGridView dgv_draws;

  }
}