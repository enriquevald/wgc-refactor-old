﻿//------------------------------------------------------------------------------
// Copyright © 2007-2014 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_player_photo.cs
// 
//   DESCRIPTION: Implements the frm_player_photo user control
//                Class: frm_player_photo
//
//        AUTHOR: David Hernández
// 
// CREATION DATE: 10-JUL-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 10-JUL-2017 DHA     First release.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WSI.Common;

namespace WSI.Cashier
{
  public partial class frm_player_photo : Controls.frm_base
  {
    public Image PlayerPhoto
    {      
      set { this.pb_user.Image = value; }
    }    

    public frm_player_photo()
    {
      InitializeComponent();
      InitializeControlResources();
    }

    private void InitializeControlResources()
    {
      btn_close.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CLOSE");
    }

    private void btn_close_Click(object sender, EventArgs e)
    {
      Misc.WriteLog("[FORM CLOSE] frm_player_photo (exit)", Log.Type.Message);
      this.Close();
    }
  }
}
