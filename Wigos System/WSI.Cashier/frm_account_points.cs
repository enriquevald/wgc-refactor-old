//------------------------------------------------------------------------------
// Copyright � 2010 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : frm_account_points.cs
// 
//   DESCRIPTION : Implements the following classes:
//                 1. frm_account_points
//
// REVISION HISTORY :
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 10-SEP-2010 TJG    First release.
// 25-MAY-2012 SSC    Moved CardData, PlaySession and PlayerTrackingData classes to WSI.Common
// 29-MAY-2012 SSC    Moved LoyaltyProgram, Gift and GiftInstance classes to WSI.Common
// 26-SEP-2012 MPO    Cancel gift instance.
//                    Fit columns of dgv
// 06-FEB-2013 QMP    Enable/disable Request Gift button depending on PointsStatus
// 17-APR-2013 DHA    Enable/disable Print List button depending on GP.Cashier.Voucher.Print
// 22-MAY-2013 JMA    Changed queries and other features to limit the amount of gifts redemptions
// 24-MAY-2013 HBB    Fixed Bug #798: Gifts of Draws: The filters of draws aren't used
// 03-JUL-2013 RRR    Moddified view parameters for Window Mode
// 21-AUG-2013 JMM    Fixed Bug WIG-137: Gifts request screen doesn't generate activity.
// 17-SEP-2013 LEM    Show gift list according points cost for account level (Modified GetSqlQuery())
// 12-FEB-2014 FBA    Added account holder id type control via CardData class IdentificationTypes
// 17-MAR-2014 JRM    Added new functionality. Description contained in RequestPinCodeDUT.docx DUT
// 03-APR-2014 JRM    Fixed bug: WIGOSTITO-1187
// 14-APR-2014 DLL    Added new functionality: VIP Account
// 13-JUL-2017 RGR    Fixed Bug 28592:WIGOS-3609 The users that buy and cancel a gift limited to a 1 by gaming day are not able to buy again the same gift
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using WSI.Common;
using System.Collections;

namespace WSI.Cashier
{
  public partial class frm_account_points : Controls.frm_base
  {
    private const String FORMAT_POINTS = "#,##0";
    private const String FORMAT_POINTS_DEC = "#,##0.00";

    private const Int32 GRID_COLUMN_GIFT_ID = 0;
    private const Int32 GRID_COLUMN_TYPE = 1;
    private const Int32 GRID_COLUMN_NAME = 2;
    private const Int32 GRID_COLUMN_POINTS = 3;
    private const Int32 GRID_COLUMN_CONVERSION_TO_NRC = 4;
    private const Int32 GRID_COLUMN_DRAW_ID = 5;
    private const Int32 GRID_COLUMN_INSTANCE_ID = 6;

    private const Int32 GRID_COLUMN_ACCOUNT_DAY_LIMIT = 7;
    private const Int32 GRID_COLUMN_ACCOUNT_MONTH_LIMIT = 8;
    private const Int32 GRID_COLUMN_GLOBAL_DAY_LIMIT = 9;
    private const Int32 GRID_COLUMN_GLOBAL_MONTH_LIMIT = 10;

    #region Attributes

    private frm_yesno form_yes_no;
    private static CardData m_card;
    private bool m_print_voucher;

    #endregion

    #region Constructor

    /// <summary>
    /// Constructor method.
    /// 1. Initialize controls.
    /// 2. Initialize control�s resources. (NLS, Images, etc)
    /// </summary>
    public frm_account_points(CardData RelatedCard)
    {
      InitializeComponent();

      InitControls();

      InitializeControlResources();

      m_card = RelatedCard;

      m_print_voucher = VoucherBusinessLogic.GetPrintVoucher();

      form_yes_no = new frm_yesno();
      form_yes_no.Opacity = 0.6;
    }

    #endregion

    #region Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Add event handler to scan and configure buttons
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    private void InitControls()
    {
      EventLastAction.AddEventLastAction(this.Controls);

      this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
      this.btn_gift_request.Click += new System.EventHandler(this.btn_gift_request_Click);
      this.btn_print.Click += new System.EventHandler(this.btn_print_Click);
      this.btn_close.Click += new System.EventHandler(this.btn_exit_Click);
    }

    //------------------------------------------------------------------------------
    // PURPOSE : RefreshData form data.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    private void RefreshData()
    {
      DataSet _ds;
      CardData _card_data;

      try
      {
        _card_data = new CardData();

        // TODO RCI 10-MAY-2011: Need to refresh data using AccountWatcher.
        if (!CardData.DB_CardGetAllData(m_card.AccountId, _card_data))
        {
          return;
        }
        m_card = _card_data;

        lbl_points_balance.Text = m_card.PlayerTracking.TruncatedPoints.ToString();

        _ds = GetGiftTables();

        // Center Close button 
        //this.btn_close.Location = new Point(this.Width / 2 - (btn_close.Width / 2), this.btn_close.Location.Y);

        dgv_available_gifts.DataSource = _ds.Tables[0];
        dgv_next_gifts.DataSource = _ds.Tables[1];
        dgv_available_instances.DataSource = _ds.Tables[2];
        FormatGridView(dgv_available_gifts, Resource.String("STR_FRM_ACCOUNT_POINTS_005"));
        FormatGridView(dgv_next_gifts, Resource.String("STR_FRM_ACCOUNT_POINTS_007"));
        FormatGridView(dgv_available_instances, Resource.String("STR_FRM_ACCOUNT_POINTS_024"));
        //    - Instance ID
        dgv_available_instances.Columns[GRID_COLUMN_INSTANCE_ID].Width = 0;
        dgv_available_instances.Columns[GRID_COLUMN_INSTANCE_ID].Visible = false;

        btn_cancel.Enabled = (dgv_available_instances.RowCount > 0);
        btn_gift_request.Enabled = (dgv_available_gifts.RowCount > 0);
        btn_print.Enabled = (m_print_voucher && dgv_available_gifts.RowCount + dgv_next_gifts.RowCount > 0);
        lbl_msg_blink.Visible = false;

        // QMP 06-FEB-2013: Enable/disable Gift Request button depending on PointsStatus and show informative label
        if (m_card.PointsStatus == ACCOUNT_POINTS_STATUS.REDEEM_NOT_ALLOWED)
        {
          btn_gift_request.Enabled = false;
          lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_POINTS_REDEEM_NOT_ALLOWED");
          lbl_msg_blink.Visible = true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } // RefreshData

    //------------------------------------------------------------------------------
    // PURPOSE : Initialize control resources (NLS strings, images, etc).
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    public void InitializeControlResources()
    {
      // NLS Strings
      //   - Title
      //   - Frames
      //   - Labels
      //   - Buttons

      //   - Title
      this.FormTitle = Resource.String("STR_FRM_ACCOUNT_POINTS_001");                 // Gifts

      //   - Frames
      gb_gifts.HeaderText = Resource.String("STR_FRM_ACCOUNT_POINTS_002");                        // Gifts List
      gp_gifts_cancellation.HeaderText = Resource.String("STR_FRM_ACCOUNT_POINTS_025");           // Requested Gifts

      //   - Labels
      lbl_points_balance_title.Text = Resource.String("STR_FRM_ACCOUNT_POINTS_008") + ":";  // Available Points

      //   - Buttons
      btn_close.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CLOSE");
      btn_print.Text = Resource.String("STR_FRM_ACCOUNT_POINTS_003");           // Print List
      btn_gift_request.Text = Resource.String("STR_FRM_ACCOUNT_POINTS_004");    // Gift Request
      btn_cancel.Text = Resource.String("STR_FRM_ACCOUNT_POINTS_029");          // Cancel Gift

    } // InitializeControlResources

    #endregion

    #region Public Methods

    private String GetSqlQuery(Boolean Available)
    {
      StringBuilder _sql_txt;
      String _gi_points_col;
      String _dr_points_col;

      _sql_txt = new StringBuilder();

      if (m_card.PlayerTracking.CardLevel < 1)
      {
        return "";
      }

      _gi_points_col = "GI_POINTS_LEVEL" + m_card.PlayerTracking.CardLevel;
      _dr_points_col = "DR_POINTS_LEVEL" + m_card.PlayerTracking.CardLevel;

      if (Available)
      {
        _sql_txt.AppendLine("SELECT ");
      }
      else
      {
        // RCI 22-JUN-2011: Do a SELECT TOP 3 * FROM ( all query) ORDER BY.
        //     If not, it doesn't work properly. TOP 3 inside makes ORDER don't work.
        //     And also, it retrieves 4 rows in total (including draws).

        // MAR 09-MAY-2013: Added sub queries to fetch the gift limits information.
        // If gift limit is exceeded at a customer or global level then thegift is not displayed

        _sql_txt.AppendLine("SELECT   TOP 3 * ");
        _sql_txt.AppendLine("  FROM ( ");
        _sql_txt.AppendLine("SELECT ");
      }

      _sql_txt.AppendLine("         GI_GIFT_ID                                                   ");
      _sql_txt.AppendLine("       , GI_TYPE                                                      ");
      _sql_txt.AppendLine("       , GI_NAME                                                      ");
      _sql_txt.AppendLine("       , " + _gi_points_col + " AS GI_POINTS                          ");
      _sql_txt.AppendLine("       , ISNULL (GI_CONVERSION_TO_NRC, 0) GI_NON_REDEEMABLE           ");
      _sql_txt.AppendLine("       , 0 GI_DRAW_ID                                                 ");
      _sql_txt.AppendLine("       , 0 GI_INSTANCE_ID                                             ");
      _sql_txt.AppendLine("       , ISNULL (GI_ACCOUNT_DAILY_LIMIT  , 0) GI_ACCOUNT_DAILY_LIMIT  ");
      _sql_txt.AppendLine("       , ISNULL (GI_ACCOUNT_MONTHLY_LIMIT, 0) GI_ACCOUNT_MONTHLY_LIMIT");
      _sql_txt.AppendLine("       , ISNULL (GI_GLOBAL_DAILY_LIMIT   , 0) GI_GLOBAL_DAILY_LIMIT   ");
      _sql_txt.AppendLine("       , ISNULL (GI_GLOBAL_MONTHLY_LIMIT , 0) GI_GLOBAL_MONTHLY_LIMIT ");
      _sql_txt.AppendLine("       , 0                                    GI_DRAW_LEVEL_FILTER    ");
      _sql_txt.AppendLine("       , 0                                    GI_GENDER_FILTER        ");
      _sql_txt.AppendLine("       , ISNULL(GI_VIP, 0)                    GI_VIP ");

      _sql_txt.AppendLine("FROM GIFTS LEFT JOIN                                                  ");

      _sql_txt.AppendLine("( SELECT  SUM(CASE WHEN GIN_REQUESTED >= @pTodayOpening AND GIN_ACCOUNT_ID	  = @AccountId  THEN GIN_NUM_ITEMS ELSE 0 END) AS ACC_DAY       ");
      _sql_txt.AppendLine("        , SUM(CASE WHEN GIN_ACCOUNT_ID = @AccountId 									                      THEN GIN_NUM_ITEMS ELSE 0 END) AS ACC_MONTH     ");
      _sql_txt.AppendLine(" 	     , SUM(CASE WHEN GIN_REQUESTED >= @pTodayOpening                                    THEN GIN_NUM_ITEMS ELSE 0 END) AS GLOBAL_DAY    ");
      _sql_txt.AppendLine(" 	     , SUM(GIN_NUM_ITEMS)																						                                                   AS GLOBAL_MONTH  ");
      _sql_txt.AppendLine("        , GIN_GIFT_ID                                                                 ");
      _sql_txt.AppendLine("   FROM   GIFT_INSTANCES                                                              ");
      _sql_txt.AppendLine("  WHERE   GIN_REQUESTED >= @pFirstDayOfMonth AND GIN_REQUEST_STATUS <> @CancelStatus  ");
      _sql_txt.AppendLine(" GROUP BY GIN_GIFT_ID ) AS TOT_REQUESTED ON TOT_REQUESTED.GIN_GIFT_ID = GI_GIFT_ID    ");
      _sql_txt.AppendLine("  WHERE   GI_AVAILABLE <> 0                                                           ");

      if (Available)
      {
        _sql_txt.AppendLine("AND   " + _gi_points_col + " <= @AccountPoints                                                                        ");
        _sql_txt.AppendLine("AND   ( ISNULL(GI_ACCOUNT_DAILY_LIMIT  , 0) = 0 OR GI_ACCOUNT_DAILY_LIMIT   > ISNULL(TOT_REQUESTED.ACC_DAY  , 0))     ");
        _sql_txt.AppendLine("AND   ( ISNULL(GI_ACCOUNT_MONTHLY_LIMIT, 0) = 0 OR GI_ACCOUNT_MONTHLY_LIMIT > ISNULL(TOT_REQUESTED.ACC_MONTH   , 0))  ");
        _sql_txt.AppendLine("AND   ( ISNULL(GI_GLOBAL_DAILY_LIMIT   , 0) = 0 OR GI_GLOBAL_DAILY_LIMIT    > ISNULL(TOT_REQUESTED.GLOBAL_DAY, 0))    ");
        _sql_txt.AppendLine("AND   ( ISNULL(GI_GLOBAL_MONTHLY_LIMIT , 0) = 0 OR GI_GLOBAL_MONTHLY_LIMIT  > ISNULL(TOT_REQUESTED.GLOBAL_MONTH , 0)) ");
      }
      else
      {
        _sql_txt.AppendLine("AND   " + _gi_points_col + " > @AccountPoints ");
      }
      _sql_txt.AppendLine("AND (   ( GI_TYPE = @GiftTypeObject AND GI_CURRENT_STOCK > 0 AND GI_CURRENT_STOCK > GI_REQUEST_COUNTER ) ");
      _sql_txt.AppendLine("        OR ( GI_TYPE = @GiftTypeNRC )           ");
      _sql_txt.AppendLine("        OR ( GI_TYPE = @GiftTypeRC )            ");
      _sql_txt.AppendLine("        OR ( GI_TYPE = @GiftTypeServices    )   ");
      _sql_txt.AppendLine("        )                                       ");
      _sql_txt.AppendLine("UNION                                           ");
      _sql_txt.AppendLine("SELECT  0 AS GI_GIFT_ID                             ");
      _sql_txt.AppendLine("       , @GiftTypeDrawNumbers AS GI_TYPE            ");
      _sql_txt.AppendLine("       , '" + Resource.String("STR_RAFFLE_TITLE") + "' + DR_NAME as GI_NAME       ");
      _sql_txt.AppendLine("       , " + _dr_points_col + " as GI_POINTS        ");
      _sql_txt.AppendLine("       , 0 as GI_NON_REDEEMABLE                     ");
      _sql_txt.AppendLine("       , DR_ID as GI_DRAW_ID                        ");
      _sql_txt.AppendLine("       , 0 as GI_INSTANCE_ID                        ");
      _sql_txt.AppendLine("       , 0                                          ");
      _sql_txt.AppendLine("       , 0                                          ");
      _sql_txt.AppendLine("       , 0                                          ");
      _sql_txt.AppendLine("       , 0                                          ");
      _sql_txt.AppendLine("       , DR_LEVEL_FILTER AS GI_DRAW_LEVEL_FILTER    ");
      _sql_txt.AppendLine("       , DR_GENDER_FILTER AS GI_GENDER_FILTER       ");
      _sql_txt.AppendLine("       , ISNULL(DR_VIP, 0) AS GI_VIP");

      _sql_txt.AppendLine(" FROM   DRAWS                                       ");
      _sql_txt.AppendLine("WHERE   DR_STARTING_DATE <= GETDATE ()              ");
      _sql_txt.AppendLine("  AND   DR_ENDING_DATE   >  GETDATE ()              ");
      _sql_txt.AppendLine("  AND   DR_STATUS        = 0                        ");
      _sql_txt.AppendLine("  AND   DR_LAST_NUMBER   < DR_MAX_NUMBER            ");
      _sql_txt.AppendLine("  AND   " + _dr_points_col + " > 0                  ");


      if (Available)
      {
        _sql_txt.AppendLine("  AND   " + _dr_points_col + " <= @AccountPoints  ");
      }
      else
      {
        _sql_txt.AppendLine("  AND   " + _dr_points_col + " > @AccountPoints   ");
      }
      if (Available)
      {
        _sql_txt.AppendLine("ORDER BY GI_POINTS DESC, GI_NAME ASC       ");
      }
      else
      {
        _sql_txt.AppendLine(") TT ");
        _sql_txt.AppendLine("ORDER BY GI_POINTS ASC, GI_NAME ASC       ");
      }


      return _sql_txt.ToString();
    }

    private DataSet GetGiftTables()
    {
      StringBuilder _sql_txt;
      DataSet _ds;
      DateTime _today;
      DateTime _first_day_of_month;

      _sql_txt = new StringBuilder();

      _sql_txt.AppendLine(GetSqlQuery(true));
      _sql_txt.AppendLine(" ; ");
      _sql_txt.AppendLine(GetSqlQuery(false));
      _sql_txt.AppendLine(" ; ");

      _ds = new DataSet();

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          SqlDataAdapter _da;

          _today = Misc.TodayOpening();
          _first_day_of_month = new DateTime(_today.Year, _today.Month, 1, _today.Hour, _today.Minute, _today.Second);

          _da = new SqlDataAdapter();
          _da.SelectCommand = new SqlCommand(_sql_txt.ToString());
          _da.SelectCommand.Parameters.Add("@AccountPoints", SqlDbType.Money).Value = m_card.PlayerTracking.TruncatedPoints.SqlMoney;
          _da.SelectCommand.Parameters.Add("@GiftTypeObject", SqlDbType.Int).Value = GIFT_TYPE.OBJECT;
          _da.SelectCommand.Parameters.Add("@GiftTypeNRC", SqlDbType.Int).Value = GIFT_TYPE.NOT_REDEEMABLE_CREDIT;
          _da.SelectCommand.Parameters.Add("@GiftTypeRC", SqlDbType.Int).Value = GIFT_TYPE.REDEEMABLE_CREDIT;
          _da.SelectCommand.Parameters.Add("@GiftTypeDrawNumbers", SqlDbType.Int).Value = GIFT_TYPE.DRAW_NUMBERS;
          _da.SelectCommand.Parameters.Add("@GiftTypeServices", SqlDbType.Int).Value = GIFT_TYPE.SERVICES;
          _da.SelectCommand.Parameters.Add("@CancelStatus", SqlDbType.Int).Value = GIFT_REQUEST_STATUS.CANCELED;        
          _da.SelectCommand.Parameters.Add("@AccountId", SqlDbType.BigInt).Value = m_card.AccountId;
          _da.SelectCommand.Parameters.Add("@pTodayOpening", SqlDbType.DateTime).Value = _today;
          _da.SelectCommand.Parameters.Add("@pFirstDayOfMonth", SqlDbType.DateTime).Value = _first_day_of_month;


          _db_trx.Fill(_da, _ds);

          // HBB 22-MAY-2013: Delete the draws that are not availible for the current player
          foreach (DataTable _dt in _ds.Tables)
          {
            foreach (DataRow _dr in _dt.Rows)
            {
              if (!Misc.IsVIP(m_card.PlayerTracking.HolderIsVIP, (Boolean)_dr[13]))
              {
                _dr.Delete();

                continue;
              }

              if ((Int64)_dr[5] != 0)   // It's a draw
              {
                if (!Misc.IsLevelAllowed(m_card.PlayerTracking.CardLevel, (Int32)_dr[11]))
                {
                  _dr.Delete();

                  continue;
                }

                if (!Misc.IsGenderAllowed(m_card.PlayerTracking.HolderGender, (DrawGenderFilter)_dr[12]))
                {
                  _dr.Delete();

                  continue;
                }
              }
            }
            _dt.Columns.Remove("GI_DRAW_LEVEL_FILTER"); //this column is deleted because it can't be shown in datagrids
            _dt.Columns.Remove("GI_GENDER_FILTER");
            _dt.Columns.Remove("GI_VIP");
            _dt.AcceptChanges();
          }

          DataTable cancellableGifts = GiftInstance.GetCancellableGift(m_card.AccountId, _db_trx.SqlTransaction);
          cancellableGifts.Columns.Remove("GI_DESCRIPTION");
          cancellableGifts.Columns.Remove("GI_SMALL_RESOURCE_ID");
          cancellableGifts.Columns.Remove("GI_LARGE_RESOURCE_ID");
          cancellableGifts.Columns.Remove("GIN_REQUESTED");
          cancellableGifts.Columns.Remove("GIN_NUM_ITEMS");

          _ds.Tables.Add(cancellableGifts);

          return _ds;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return null;
    }

    private void FormatGridView(DataGridView GridView, String Title)
    {
      GridView.ColumnHeadersHeight = 30;
      GridView.RowTemplate.Height = 30;
      GridView.RowTemplate.DividerHeight = 0;

      //    - Gift Id
      GridView.Columns[GRID_COLUMN_GIFT_ID].Width = 0;
      GridView.Columns[GRID_COLUMN_GIFT_ID].Visible = false;

      //    - Type
      GridView.Columns[GRID_COLUMN_TYPE].Width = 0;
      GridView.Columns[GRID_COLUMN_TYPE].Visible = false;

      //    - Name
      GridView.Columns[GRID_COLUMN_NAME].HeaderCell.Value = Title;
      GridView.Columns[GRID_COLUMN_NAME].Width = 395;
      GridView.Columns[GRID_COLUMN_NAME].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

      //    - Points
      GridView.Columns[GRID_COLUMN_POINTS].HeaderCell.Value = Resource.String("STR_FRM_ACCOUNT_POINTS_006");
      GridView.Columns[GRID_COLUMN_POINTS].Width = 100;
      GridView.Columns[GRID_COLUMN_POINTS].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
      GridView.Columns[GRID_COLUMN_POINTS].DefaultCellStyle.Format = FORMAT_POINTS;
      GridView.Columns[GRID_COLUMN_POINTS].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

      //    - Conversion to NR Credits
      GridView.Columns[GRID_COLUMN_CONVERSION_TO_NRC].Width = 0;
      GridView.Columns[GRID_COLUMN_CONVERSION_TO_NRC].Visible = false;

      //    - Draw ID
      GridView.Columns[GRID_COLUMN_DRAW_ID].Width = 0;
      GridView.Columns[GRID_COLUMN_DRAW_ID].Visible = false;

      //    - Instance ID
      GridView.Columns[GRID_COLUMN_INSTANCE_ID].Width = 0;
      GridView.Columns[GRID_COLUMN_INSTANCE_ID].Visible = false;

      //    - Account gift redeem day limit
      GridView.Columns[GRID_COLUMN_ACCOUNT_DAY_LIMIT].Width = 0;
      GridView.Columns[GRID_COLUMN_ACCOUNT_DAY_LIMIT].Visible = false;

      //    - Account gift redeem month limit
      GridView.Columns[GRID_COLUMN_ACCOUNT_MONTH_LIMIT].Width = 0;
      GridView.Columns[GRID_COLUMN_ACCOUNT_MONTH_LIMIT].Visible = false;

      //    - Global gift redeem day limit
      GridView.Columns[GRID_COLUMN_GLOBAL_DAY_LIMIT].Width = 0;
      GridView.Columns[GRID_COLUMN_GLOBAL_DAY_LIMIT].Visible = false;

      //    - Global gift redeem month limit
      GridView.Columns[GRID_COLUMN_GLOBAL_MONTH_LIMIT].Width = 0;
      GridView.Columns[GRID_COLUMN_GLOBAL_MONTH_LIMIT].Visible = false;
    }


    //------------------------------------------------------------------------------
    // PURPOSE : Init and Show the form.
    //
    //  PARAMS :
    //      - INPUT : ParentForm
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //

    public void Show(Form ParentForm)
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_account_points", Log.Type.Message);
      String _message;
      String[] _message_params = { "", "", "", "", "", "" };

      try
      {
        if (m_card.PlayerTracking.CardLevel < 1)
        {
          _message_params[0] = m_card.AccountId.ToString();

          _message = Resource.String("STR_FRM_ACCOUNT_POINTS_014", _message_params);
          _message = _message.Replace("\\r\\n", "\r\n");

          frm_message.Show(_message,
                            Resource.String("STR_APP_GEN_MSG_WARNING"),
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Warning,
                            ParentForm);

          return;
        }

        RefreshData();

        form_yes_no.Show();
        this.ShowDialog(form_yes_no);
        form_yes_no.Hide();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } // Show

    //------------------------------------------------------------------------------
    // PURPOSE : Dispose yes_no form.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    new public void Dispose()
    {
      form_yes_no.Dispose();
      form_yes_no = null;

      base.Dispose();
    }

    #endregion

    #region Buttons

    //------------------------------------------------------------------------------
    // PURPOSE : Handle the click on the Select button.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    private void btn_print_Click(object sender, EventArgs e)
    {
      VoucherGiftList _voucher_gift_list;
      Boolean _voucher_saved;
      String _available_gift_list;
      String _future_gift_list;
      SqlTransaction _sql_trx;
      SqlConnection _sql_conn;
      String _error_str;

      // Check if current user is an authorized user
      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.GiftPrintList,
                                               ProfilePermissions.TypeOperation.RequestPasswd,
                                               this,
                                               out _error_str))
      {
        return;
      }

      _available_gift_list = "";
      _future_gift_list = "";

      foreach (DataGridViewRow _row in dgv_available_gifts.Rows)
      {
        _available_gift_list += "<tr><td align=\"left\"  width=\"80%\">";
        _available_gift_list += _row.Cells[GRID_COLUMN_NAME].Value.ToString();
        _available_gift_list += "</td>\n<td align=\"right\" width=\"20%\">";
        _available_gift_list += ((decimal)_row.Cells[GRID_COLUMN_POINTS].Value).ToString(FORMAT_POINTS);
        _available_gift_list += "</td></tr>\n";
      }

      foreach (DataGridViewRow _row in dgv_next_gifts.Rows)
      {
        _future_gift_list += "<tr><td align=\"left\"  width=\"80%\">";
        _future_gift_list += _row.Cells[GRID_COLUMN_NAME].Value.ToString();
        _future_gift_list += "</td>\n<td align=\"right\" width=\"20%\">";
        _future_gift_list += ((decimal)_row.Cells[GRID_COLUMN_POINTS].Value).ToString(FORMAT_POINTS);
        _future_gift_list += "</td></tr>\n";
      }

      _sql_conn = WGDB.Connection();
      _sql_trx = _sql_conn.BeginTransaction();

      _voucher_gift_list = new VoucherGiftList(m_card.VoucherAccountInfo()
                                             , m_card.PlayerTracking.TruncatedPoints
                                             , _available_gift_list
                                             , _future_gift_list
                                             , PrintMode.Print
                                             , _sql_trx);

      _voucher_saved = _voucher_gift_list.Save(_sql_trx);

      _sql_trx.Commit();

      // RCI 21-JUL-2011: Print always after Commit.
      if (_voucher_saved)
      {
        VoucherPrint.Print(_voucher_gift_list);
      }

    } // btn_print_Click

    //------------------------------------------------------------------------------
    // PURPOSE : Handle the click on the Manual button.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    private void btn_gift_request_Click(object sender, EventArgs e)
    {
      GiftCashier _gift = new GiftCashier();
      String _error_str;
      DataRowView _drv;
      List<ProfilePermissions.CashierFormFuncionality> _list_permissions;

      if (dgv_available_gifts.CurrentRow == null)
      {
        return;
      }

      // Selected row
      _drv = (DataRowView)dgv_available_gifts.CurrentRow.DataBoundItem;
      if (_drv == null)
      {
        return;
      }
      if (_drv.Row == null)
      {
        return;
      }

      _gift = new GiftCashier(_drv.Row);

      // Check if current user is an authorized user
      _list_permissions = new List<ProfilePermissions.CashierFormFuncionality>();

      _list_permissions.Add(ProfilePermissions.CashierFormFuncionality.GiftRequest);

      if (WSI.Common.TITO.Utils.IsTitoMode())
      {
        if (_gift.Type == GIFT_TYPE.REDEEMABLE_CREDIT)
        {
          _list_permissions.Add(ProfilePermissions.CashierFormFuncionality.TITO_CreatePromoRETicket);
        }
        else if (_gift.Type == GIFT_TYPE.NOT_REDEEMABLE_CREDIT)
        {
          _list_permissions.Add(ProfilePermissions.CashierFormFuncionality.TITO_CreatePromoNRTicket);
        }
      }

      if (!ProfilePermissions.CheckPermissionList(_list_permissions,
                                                  ProfilePermissions.TypeOperation.RequestPasswd,
                                                  this,
                                                  0,
                                                  out _error_str))
      {
        return;
      }

      if (!CashierBusinessLogic.RequestPin(this, Accounts.PinRequestOperationType.GIFT_REQUEST, m_card, 0))
      {
        return;
      }

      // Whether gift can be requested or not is checked out in form_gift_request.Show
      frm_gift_request form_gift_request = new frm_gift_request(_gift, m_card);
      form_gift_request.Show(this.form_yes_no);
      // RCI 10-MAY-2011: Refresh data and stay in the Account Points screen 
      RefreshData();

      form_gift_request.Dispose();
    } // btn_gift_request_Click

    //------------------------------------------------------------------------------
    // PURPOSE : Handle the click on the Exit button.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    private void btn_exit_Click(object sender, EventArgs e)
    {
      Misc.WriteLog("[FORM CLOSE] frm_account_points", Log.Type.Message);
      this.Close();
    }

    private void btn_cancel_Click(object sender, EventArgs e)
    {
      Int64 _instance_id;
      String _gift_name;
      GiftInstance.GIFT_INSTANCE_MSG _msg;
      ArrayList _voucher_list;
      GiftInstance _instance;
      String _error_str;

      if (this.dgv_available_instances.SelectedRows.Count != 1)
      {
        return;
      }

      try
      {

        if (!ProfilePermissions.CheckPermissionList(ProfilePermissions.CashierFormFuncionality.GiftCancel,
                                                    ProfilePermissions.TypeOperation.RequestPasswd,
                                                    this, 0,
                                                    out _error_str))
        {
          return;
        }

        _instance_id = (Int64)dgv_available_instances.SelectedRows[0].Cells[GRID_COLUMN_INSTANCE_ID].Value;
        _gift_name = (String)dgv_available_instances.SelectedRows[0].Cells[GRID_COLUMN_NAME].Value;

        if (frm_message.Show(Resource.String("STR_FRM_ACCOUNT_POINTS_026", _gift_name),
                            Resource.String("STR_FRM_ACCOUNT_POINTS_027"),
                            MessageBoxButtons.YesNo, MessageBoxIcon.Question, this) == DialogResult.Cancel)
        {

          return;
        }

        _instance = new GiftInstance();
        _voucher_list = null;

        using (DB_TRX _trx = new DB_TRX())
        {
          _instance.DB_ReadInstaceAndGift(_instance_id, _trx.SqlTransaction);
          if (GiftInstance.CancelInstance(_instance, Cashier.CashierSessionInfo(), out _voucher_list, out _msg, _trx.SqlTransaction))
          {
            _trx.Commit();

            frm_message.Show(Resource.String("STR_FRM_ACCOUNT_POINTS_028"),
                             Resource.String("STR_FRM_ACCOUNT_POINTS_027"),
                             MessageBoxButtons.OK, MessageBoxIcon.Information, this);


          }
          else
          {
            _trx.Rollback();

            String _message;
            String _caption;
            MessageBoxIcon _msg_icon;

            _instance.GetMessage(_msg, out _message, out _caption, out _msg_icon);
            frm_message.Show(_message, _caption, MessageBoxButtons.OK, _msg_icon, this);
          }

          if (_voucher_list != null)
          {
            VoucherPrint.Print(_voucher_list);
          }

        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      RefreshData();

    }

    private void frm_account_points_Shown(object sender, EventArgs e)
    {
      dgv_available_gifts.Focus();
    } // btn_cancel_Click

    #endregion Buttons
  }
}
