﻿namespace WSI.Cashier
{
  partial class frm_tpv
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.btn_cancel = new WSI.Cashier.Controls.uc_round_button();
      this.lbl_text = new WSI.Cashier.Controls.uc_label();
      this.btn_retry = new WSI.Cashier.Controls.uc_round_button();
      this.pb_CircularProgressBar = new System.Windows.Forms.PictureBox();
      this.pnl_data.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pb_CircularProgressBar)).BeginInit();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.Controls.Add(this.pb_CircularProgressBar);
      this.pnl_data.Controls.Add(this.btn_retry);
      this.pnl_data.Controls.Add(this.lbl_text);
      this.pnl_data.Controls.Add(this.btn_cancel);
      this.pnl_data.Size = new System.Drawing.Size(539, 233);
      // 
      // btn_cancel
      // 
      this.btn_cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_cancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_cancel.FlatAppearance.BorderSize = 0;
      this.btn_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_cancel.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_cancel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_cancel.Image = null;
      this.btn_cancel.IsSelected = false;
      this.btn_cancel.Location = new System.Drawing.Point(370, 159);
      this.btn_cancel.Name = "btn_cancel";
      this.btn_cancel.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_cancel.Size = new System.Drawing.Size(155, 60);
      this.btn_cancel.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_cancel.TabIndex = 4;
      this.btn_cancel.Text = "XCANCEL";
      this.btn_cancel.UseVisualStyleBackColor = false;
      this.btn_cancel.Click += new System.EventHandler(this.btn_login_cancel_Click);
      // 
      // lbl_text
      // 
      this.lbl_text.BackColor = System.Drawing.Color.Transparent;
      this.lbl_text.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_text.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_text.Location = new System.Drawing.Point(77, 22);
      this.lbl_text.Name = "lbl_text";
      this.lbl_text.Size = new System.Drawing.Size(448, 122);
      this.lbl_text.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_text.TabIndex = 5;
      this.lbl_text.Text = "xMsg";
      this.lbl_text.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // btn_retry
      // 
      this.btn_retry.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_retry.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_retry.FlatAppearance.BorderSize = 0;
      this.btn_retry.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_retry.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_retry.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_retry.Image = null;
      this.btn_retry.IsSelected = false;
      this.btn_retry.Location = new System.Drawing.Point(201, 159);
      this.btn_retry.Name = "btn_retry";
      this.btn_retry.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_retry.Size = new System.Drawing.Size(155, 60);
      this.btn_retry.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_retry.TabIndex = 6;
      this.btn_retry.Text = "XREINTENTAR";
      this.btn_retry.UseVisualStyleBackColor = false;
      this.btn_retry.Click += new System.EventHandler(this.btn_retry_Click);
      // 
      // pb_CircularProgressBar
      // 
      this.pb_CircularProgressBar.Image = global::WSI.Cashier.Properties.Resources.CircularProgressBar;
      this.pb_CircularProgressBar.Location = new System.Drawing.Point(7, 49);
      this.pb_CircularProgressBar.Name = "pb_CircularProgressBar";
      this.pb_CircularProgressBar.Size = new System.Drawing.Size(68, 68);
      this.pb_CircularProgressBar.TabIndex = 14;
      this.pb_CircularProgressBar.TabStop = false;
      this.pb_CircularProgressBar.Visible = false;
      // 
      // frm_tpv
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(539, 288);
      this.Name = "frm_tpv";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "frm_tpv";
      this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frm_tpv_FormClosed);
      this.Shown += new System.EventHandler(this.frm_tpv_Shown);
      this.pnl_data.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.pb_CircularProgressBar)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private Controls.uc_round_button btn_cancel;
    private Controls.uc_label lbl_text;
    private Controls.uc_round_button btn_retry;
    private System.Windows.Forms.PictureBox pb_CircularProgressBar;
  }
}