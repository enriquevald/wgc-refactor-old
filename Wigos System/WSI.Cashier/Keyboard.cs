//------------------------------------------------------------------------------
// Copyright � 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Keyboard.cs
// 
//   DESCRIPTION: Class that offers windows keyboard associated methods.
// 
//        AUTHOR: Alberto Cuesta
// 
// CREATION DATE: 03-AGU-2007
//
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 03-AGU-2007 ACC    First release.
// 21-MAY-2013 DHA    Fixed Bug #788: OSK supported for 64 bits windows
// 12-AUG-2013 DRV    Add control to the OSK enabled/disabled option
// 15-OCT-2014 DHA    Added keyboard support for Windows 8 and higher versions
// 20-OCT-2016 ESE    Bug 10699: Lost focus when close keyboard
// 02-DIC-2016 ESE    Bug 10699: Undo changes to the keyboard
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using WSI.Common;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Windows.Forms;

using System.Management;
using Microsoft.Win32;
using System.IO;

namespace WSI.Cashier
{

  static class WSIKeyboard
  {
    [DllImport("user32.dll")]
    public static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

    [DllImport("user32.dll")]
    static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int X,
    int Y, int cx, int cy, uint uFlags);

    [DllImport("user32.dll")]
    static extern bool SetForegroundWindow(IntPtr hWnd);

    [DllImport("user32.dll")]
    static extern IntPtr SetWindowLong(IntPtr hWnd, int nIndex, Int32 x);

    [DllImport("kernel32.dll")]
    static extern bool Wow64DisableWow64FsRedirection(ref long oldvalue);

    [DllImport("kernel32.dll")]
    static extern bool Wow64RevertWow64FsRedirection(long oldvalue);

    [DllImport("kernel32.dll", SetLastError = true, CallingConvention = CallingConvention.Winapi)]
    [return: MarshalAs(UnmanagedType.Bool)]
    public static extern bool IsWow64Process([In] IntPtr processHandle,
         [Out, MarshalAs(UnmanagedType.Bool)] out bool wow64Process);

    [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
    internal static extern IntPtr GetProcAddress([In] IntPtr hModule, [In, MarshalAs(UnmanagedType.LPStr)] string lpProcName);

    [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
    private static extern IntPtr GetModuleHandle(string moduleName);

    private static Version m_win8_version = new Version(6, 2, 9200, 0);

    private static Boolean m_is_win8_or_higher = IsWin8();

    //------------------------------------------------------------------------------
    // PURPOSE : Validate if Operative Systems is Windows 8 or Upper
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    static private bool IsWin8()
    {
      Boolean isWindows8;

      isWindows8 = Environment.OSVersion.Platform == PlatformID.Win32NT && Environment.OSVersion.Version >= m_win8_version;

      return  isWindows8;
    }

    static Boolean isWin10()
    {
      if (GetOSName().Contains("Windows 10"))
      {
        return true;
      }

      return false;
    }

    static private string GetRegistryOSName(string path, string key)
    {
      try
      {
        RegistryKey rk = Registry.LocalMachine.OpenSubKey(path);
        if (rk == null) return String.Empty;
        return (string)rk.GetValue(key);
      }
      catch { return String.Empty; }
    }

    static private string GetOSName()
    {
      string ProductName = GetRegistryOSName(@"SOFTWARE\Microsoft\Windows NT\CurrentVersion", "ProductName");
      string CSDVersion = GetRegistryOSName(@"SOFTWARE\Microsoft\Windows NT\CurrentVersion", "CSDVersion");
      if (ProductName != String.Empty)
      {
        return (ProductName.StartsWith("Microsoft") ? String.Empty : "Microsoft ") + ProductName +
                    (CSDVersion != String.Empty ? " " + CSDVersion : String.Empty);
      }
      return String.Empty;
    }


    /// <summary>
    /// Create process with keyboard
    /// </summary>
    static private Process GetKeyboardProcess(Boolean CreateIfNotExists)
    {
      Process[] _processes;
      long _wow64_redirect_return;
      Boolean _is_wow64;
      String _keyboard_process_name;

      _wow64_redirect_return = 0;
      _is_wow64 = false;

      if (m_is_win8_or_higher)
      {
        _keyboard_process_name = "tabtip";
      }
      else
      {
        _keyboard_process_name = "osk";
      }


      try
      {
        // Check if Virtual Keyboard is running
        _processes = Process.GetProcessesByName(_keyboard_process_name);

        if (_processes.Length == 0 && CreateIfNotExists)
        {
          DirectoryInfo _dir;
          Process _process;

          _dir = new DirectoryInfo(@"\Program Files\Common Files\Microsoft Shared\ink");          
          _process = new Process();

          _process.StartInfo.UseShellExecute = false;
          _process.StartInfo.RedirectStandardOutput = true;
          _process.StartInfo.RedirectStandardError = true;
          _process.StartInfo.CreateNoWindow = false; // true;
          _process.StartInfo.FileName = _keyboard_process_name; // "c:\\winnt\\system32\\osk.exe";
          _process.StartInfo.Arguments = "";
          _process.StartInfo.WorkingDirectory = (isWin10()) ? _dir.FullName : "c:\\";
          _process.StartInfo.WindowStyle = ProcessWindowStyle.Minimized;

          if (m_is_win8_or_higher || isWin10())
          {
            _process.StartInfo.UseShellExecute = true;
            _process.StartInfo.RedirectStandardOutput = false;
            _process.StartInfo.RedirectStandardError = false;
          }

          try
          {
            _is_wow64 = IsWow64();

            if (_is_wow64)
            {
              Wow64DisableWow64FsRedirection(ref _wow64_redirect_return);
            }

            if (isWin10())
            {
              string _ruta = _dir.FullName + "\\Tabtip.exe";
              Process.Start(_ruta);
            }
            else
            {
              _process.Start(); // Start Onscreen Keyboard
            }
          }
          finally
          {
            if (_is_wow64)
            {
              Wow64RevertWow64FsRedirection(_wow64_redirect_return);
            }
          }

          _processes = Process.GetProcessesByName(_keyboard_process_name);

        }

        foreach (Process _process in _processes)
        {
          if (m_is_win8_or_higher || isWin10())
          {
            return _process;
          }
          else
          {
            if (!_process.HasExited)
            {
              return _process;
            }
          }
        }
      }
      catch {}

      return null;
    }
    //------------------------------------------------------------------------------
    // PURPOSE : Initializes and shows OSK if it's enabled
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    static public void Show()
    {
      if (Cashier.HideOsk)
      {
        return;
      }

      ForceShow();
    }
    //------------------------------------------------------------------------------
    // PURPOSE : Initializes and shows OSK
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES : This function it's only used when the user clicks on a keyboard Button
    //
    static public void ForceShow()
    {
      Process _process;

      _process = GetKeyboardProcess(true);

      if (_process == null)
      {
        return;
      }

      try
      {
        if (!_process.HasExited)
        {
          if (!isWin10())
          {
            SetWindowPos(_process.MainWindowHandle,
                          new IntPtr(0),       // Parent Window
                          0,                    // Keypad Position X
                          768 - 312,            // Keypad Position Y
                          1024,                 // Keypad Width
                          312,                  // Keypad Height
                          0x0040 | 0x0004);     // Show Window and Place on Top
          }

          SetForegroundWindow(_process.MainWindowHandle);
        }
      }
      catch { }
    }
    static public void Hide()
    {
      Process[] processes;
      String _keyboard_process_name;

      if (m_is_win8_or_higher)
      {
        _keyboard_process_name = "tabtip";
      }
      else
      {
        _keyboard_process_name = "osk";
      }

      // Check if Virtual Keyboard is running
      processes = Process.GetProcessesByName(_keyboard_process_name);
      
      foreach (Process process in processes)
      {
        // Kill every osk currently running
        process.Kill();
      }

      // Check if Virtual Keyboard is running
      processes = Process.GetProcessesByName("msswchx");
      foreach (Process process in processes)
      {
        // Kill every osk currently running
        process.Kill();
      }
    }
    //------------------------------------------------------------------------------
    // PURPOSE : Toogle OSK if it's enabled
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    static public void Toggle()
    {
      if (Cashier.HideOsk)
      {
        return;
      }

      ForceToggle();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Toogle OSK. 
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES : This function it's only used when the user clicks on a keyboard Button
    //
    static public void ForceToggle()
    {
      if (GetKeyboardProcess(false) == null)
      {
        ForceShow();

        return;
      }

      Hide();
    }
    /// <summary>
    /// Validate if it is a 32 bits application on windows 64 bits
    /// </summary>
    /// <returns></returns>
    private static Boolean IsWow64()
    {
      Boolean _ret_val;

      _ret_val = false;

      try
      {
        // Validate it's possible to call IsWowProcess
        if ((IntPtr)GetProcAddress(GetModuleHandle("kernel32"), "IsWow64Process") != null)
        {
          // A pointer to a value that is set to TRUE if the process is running under WOW64. 
          // If the process is running under 32-bit Windows, the value is set to FALSE. 
          // If the process is a 64-bit application running under 64-bit Windows, the value is also set to FALSE.
          IsWow64Process(Process.GetCurrentProcess().Handle, out _ret_val);
        }
      }
      catch { }

      return _ret_val;
    }
  }
}