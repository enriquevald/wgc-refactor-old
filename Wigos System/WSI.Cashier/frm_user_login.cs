//------------------------------------------------------------------------------
// Copyright � 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_user_login.cs
// 
//   DESCRIPTION: Implements the following classes:
//                1. frm_user_login
//
//        AUTHOR: AA
// 
// CREATION DATE: 21-AUG-2007
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 21-AUG-2007 AA     First release.
// 31-OCT-2011 ACC    Add alarm for insert alarm MAX failed attemps of login.
// 03-JAN-2012 JMM    Avoid superuser blocking cause login failures
// 21-MAR-2012 SSC    Moved function ReadTerminalId(SqlTransaction SqlTrx, String TerminalName) to WSI.Common
// 29-MAR-2012 MPO    User session: Check if exist other user with a session opened.
// 07-AUG-2012 RRB    Add Auditory (Access denied and blocked)
// 26-SEP-2012 JAB    Fixed Defect #05: Lost focus when cashier is initialized.
// 21-DEC-2012 ICS    Added a checkbox to change password.
// 28-MAY-2013 NMR    Preventing the focus loss on main window
// 02-JUL-2013 RRR    Moddified view parameters for Window Mode
// 10-JUL-2013 DHA    Changed the length from the field 'Password' from 15 to 22 characters
// 12-AUG-2013 DRV    Added support to show automatic OSK if it's enabled or open it if the keyboard button is clicked
// 27-AUG-2013 AMF & DDM   Fixed Bug WIG-163 corporate user can't be blocked
// 29-AUG-2013 JPJ    Fixed Bug WIG-165 Change password policy with corporate users
// 29-AUG-2013 JCOR   Added AuthorizedByUserId AuthorizedByUserName.
// 10-SEP-2013 JPJ    Fixed Bug WIG-196 No block notification has to be sent in the audity using a corporate user.
// 13-SEP-2013 QMP    Added Work Shifts
// 30-SEP-2013 QMP    Fixed Bug WIG-231: Cash Openings with authorized user do not show up correctly on GUI.
// 22-NOV-2013 JBC    Added ScrollBar in TextBox.
// 25-FEB-2014 AMF    Fixed Bug WIG-70: User last activity
// 25-FEB-2014 AMF    Fixed Bug WIG-673: Audit when empty password
// 16-APR-2014 ICS, MPO & RCI    Fixed Bug WIG-830: Not enough available storage exception.
// 25-APR-2014 DLL    Fixed Bug WIG-857: only allawed change terminal when the terminals are not gaming table(both)
// 08-MAY-2014 LEM    Fixed Bug WIG-891: wrong message on failed login 
// 08-AUG-2014 DCS    If mode is gaming table show gaming table else cage
// 26-SEP-2014 DRV    Added KeyboardMessageEvent.
// 19-OCT-2015 ETP    Product Backlog Item 4690 Added new start point for system mode gaming hall.
// 09-NOV-2015 JMV    Product Backlog Item 5902:Dise�o cajero: Aplicar en frm_user_login.cs
// 10-FEB-2016 DDS    Bug 9112:No pueden cambiar las credenciales del cajero pasando la tarjeta mediante el lector de tarjetas
// 14-MAR-2016 FOS    Fixed Bug 9357: New design corrections
// 28-NOV-2016 FAV    PBI 20347:EGASA: Login with card
// 07-DEC-2016 JBP    Bug 21315:Cajero: No se muestra el bot�n cancelar en el formulario de permisos
//                    Refactor
// 14-MAR-2016 FOS     Fixed Bug 9357: New design corrections
// 16-FEB-2017 ATB    Add logging system to the shutdowns to control it
// 09-AUG-2017 ATB    Bug 29230: The user is able to use the tab "Otras operac." when the cashier is out of the gaming day (WIGOS-4267)
// 22-AGU-2017 DHA    Bug 29419:WIGOS-4272 Tables - Player tracking is showing the gaming table session information when cashier user session is closed.
// 20-FEB-2018 AGS    Bug 31592:WIGOS-8249 Cashier Login do not have the focus in place.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using WSI.Common;
using WSI.Cashier.TITO;
using WSI.Cashier.Controls;

namespace WSI.Cashier
{
  public partial class frm_user_login : frm_base
  {
    #region Constants

    public const Int32 LOGIN_MODE_REGULAR = 0;
    public const Int32 LOGIN_MODE_AUTHORIZATION = 1;
    public const Int32 LOGIN_MODE_MULTI_AUTHORIZATION = 2;

    private const Int32 CASHIER_FORM_MAIN = 0;
    private const Int32 CASHIER_FORM_NO_REDEEMABLE = 1;

    private const Int32 ADJUST_LINE_INCREMENT = 15;

    private const Int32 DEFAULT_FORM_SIZE_X = 573;
    private const Int32 DEFAULT_FORM_SIZE_Y = 427;
    private const Int32 DEFAULT_BUTTONS_Y = 300;
    private const Int32 LOGIN_PIN_RESET_Y_INCREMENT = 98;

    private const Int16 NUM_LOGIN_CONTROLS = 9;
    private const Int16 CONTROL_USER_NAME_LBL = 0;
    private const Int16 CONTROL_USER_NAME_TXT = 1;
    private const Int16 CONTROL_USER_PASS_LBL = 2;
    private const Int16 CONTROL_USER_PASS_TXT = 3;
    private const Int16 CONTROL_USER_URN_LBL = 4;
    private const Int16 CONTROL_USER_PIN_TXT = 5;
    private const Int16 CONTROL_USER_PIN_LBL = 6;
    private const Int16 CONTROL_USER_CONF_TXT = 7;
    private const Int16 CONTROL_USER_CONF_LBL = 8;

    public const Int32 FIELD_USERDATA_USERNAME = 0;
    public const Int32 FIELD_USERDATA_PASSWORD = 1;

    private const Int32 DB_USER_INFO_PROFILE_ID = 0;
    private const Int32 DB_USER_INFO_BLOCK_REASON = 1;
    private const Int32 DB_USER_INFO_NOT_VALID_BEFORE = 2;
    private const Int32 DB_USER_INFO_NOT_VALID_AFTER = 3;
    private const Int32 DB_USER_INFO_LAST_CHANGED = 4;
    private const Int32 DB_USER_INFO_PASSWORD_EXP = 5;
    private const Int32 DB_USER_INFO_PASSWORD_CHG_REQ = 6;
    private const Int32 DB_USER_INFO_LOGIN_FAILURES = 7;
    private const Int32 DB_USER_INFO_MASTER_USER = 8;

    #endregion

    #region " Structs "

    internal struct UserInfo
    {
      internal Int32 ProfileId;
      internal Int32 Id;
      internal String UserName;
      internal Boolean MasterUser;
      internal Boolean Enabled;
      internal Int32 LoginFailures;
      internal Boolean PasswordChangeRequired;
      internal Int32 PasswordExpireDays;
      internal DateTime NotValidBefore;
      internal DateTime NotValidAfter;
      internal DateTime LastChanged;
      internal DateTime PasswordExpireDate;
    } // UserInfo

    #endregion

    #region Attributes

    private UserCard m_user_card;
    private Boolean m_user_without_pin;
    private Int32[] m_controls_y_position;
    private static frm_yesno m_form_yes_no;
    private Int32 m_cashier_form;
    private List<ProfilePermissions.CashierFormFuncionality> m_permission_list;
    private Int32 m_adjust_offset;

    frm_container m_parent_window;

    private Boolean m_correct_login;

    // Login Mode: 
    //  - LOGIN_MODE_REGULAR (0): regular application login
    //  - LOGIN_MODE_AUTHORIZATION (1): authorization login for ONE permission
    //  - LOGIN_MODE_MULTI_AUTHORIZATION (2): authorization login for a list of permissions
    private int m_login_mode;

    // When requesting an authorization, if it has been accepted or not
    private Boolean m_authorized;

    private String m_str_cashier_forms;
    private Boolean m_card_login;

    #endregion

    #region Constructor

    public frm_user_login(int LoginMode)
    {
      InitializeComponent();

      m_form_yes_no = new frm_yesno();  // Background Shader
      m_form_yes_no.Opacity = 0.6;

      this.txt_user_name.Select();
      this.txt_user_password.UseSystemPasswordChar = false;

      this.txt_user_name.MaxLength = PasswordPolicy.MAX_LOGIN_LENGTH_CONST;
      this.txt_user_password.MaxLength = PasswordPolicy.MAX_PASSWORD_LENGTH_CONST;

      m_correct_login = false;

      m_login_mode = LoginMode;
      InitializeControlResources();
      InitializeControlsYPositions();
      ResetCredentialsImputs();

      KeyboardMessageFilter.RegisterHandler(this, BarcodeType.Account, this.BarcodeListener);
    }

    #endregion

    #region Private Methods

    private UserInfo GetNewUserInfo()
    {
      return new UserInfo()
      {
        ProfileId = 0,
        Id = 0,
        UserName = String.Empty,
        MasterUser = false,
        Enabled = false,
        LoginFailures = 0,
        PasswordChangeRequired = false,
        PasswordExpireDays = 0,
        NotValidBefore = WGDB.Now,
        NotValidAfter = WGDB.Now,
        LastChanged = WGDB.Now,
        PasswordExpireDate = WGDB.Now
      };
    }


    /// <summary>
    /// Barcode listener
    /// </summary>
    /// <param name="Type"></param>
    /// <param name="Data"></param>
    private void BarcodeListener(KbdMsgEvent Type, Object Data)
    {
      Barcode _barcode;

      // Check message event type
      if (Type != KbdMsgEvent.EventBarcode)
      {
        return;
      }

      try
      {
        _barcode = (Barcode)Data;

        // Insert card password
        if (m_card_login)
        {
          this.WritePassword(_barcode.ExternalCode);

          return;
        }

        // Login with card
        m_user_card = new UserCard(_barcode.ExternalCode, WCP_CardTypes.CARD_TYPE_EMPLOYEE);

        this.CardLogin(_barcode.ExternalCode);
      }
      finally
      {
        m_card_login = m_user_without_pin;
      }

    } // BarcodeListener

    /// <summary>
    /// Login with card
    /// </summary>
    /// <param name="ExternalCode"></param>
    private void CardLogin(String ExternalCode)
    {
      this.ResetCredentialsImputs();

      switch (m_user_card.Status)
      {
        case UserCardStatus.Ok:
          this.m_card_login = true;
          this.Login(); // try to Login

          break;

        case UserCardStatus.NoExist:
          {
            this.WritePassword(ExternalCode);
            this.Login();

            return;
          }

        case UserCardStatus.NoPin:
          this.ShowSetUserPasswordView();
          break;

        default:
          ShowError(Resource.String("STR_APP_GEN_ERROR_DATABASE_ERROR_READING"));
          break;
      }
    } // CardLogin

    /// <summary>
    /// Write trackData in txt password 
    /// </summary>
    /// <param name="ExternalCode"></param>
    private void WritePassword(String ExternalCode)
    {
      if (this.txt_user_name.Text.Length > 0 || m_card_login)
      {
        this.txt_user_password.Text = ExternalCode;
      }
    } // WritePassword

    /// <summary>
    /// Show new user password view
    /// </summary>
    private void ShowSetUserPasswordView()
    {
      this.m_card_login = true;

      if (this.m_user_card == null)
      {
        this.ShowError(Resource.String("STR_FRM_USER_NOT_EXIST_ERROR"));

        return;
      }

      this.m_user_without_pin = true;
      this.lbl_user_username.Text = this.m_user_card.UserName;

      this.AdjustScreen();

    } // ShowSetUserPasswordView

    /// <summary>
    /// Initialize controls (Y position)
    /// </summary>
    private void InitializeControlsYPositions()
    {
      m_controls_y_position = new Int32[NUM_LOGIN_CONTROLS];

      m_controls_y_position[CONTROL_USER_NAME_LBL] = lbl_user_name.Location.Y;
      m_controls_y_position[CONTROL_USER_NAME_TXT] = txt_user_name.Location.Y;
      m_controls_y_position[CONTROL_USER_PASS_LBL] = lbl_user_password.Location.Y;
      m_controls_y_position[CONTROL_USER_PASS_TXT] = txt_user_password.Location.Y;
      m_controls_y_position[CONTROL_USER_URN_LBL] = lbl_user_username.Location.Y;
      m_controls_y_position[CONTROL_USER_PIN_TXT] = lbl_user_pin.Location.Y;
      m_controls_y_position[CONTROL_USER_PIN_LBL] = txt_user_pin.Location.Y;
      m_controls_y_position[CONTROL_USER_CONF_TXT] = lbl_user_pin_confirm.Location.Y;
      m_controls_y_position[CONTROL_USER_CONF_LBL] = txt_user_pin_confirm.Location.Y;

    } // InitializeControlsYPositions

    /// <summary>
    /// Adjust control size and positions
    /// </summary>
    private void AdjustScreen()
    {
      Int32 _y_button_location;
      Int32 _form_height;

      if (m_login_mode != LOGIN_MODE_MULTI_AUTHORIZATION)
      {
        pnl_permission_list.Visible = false;
        m_adjust_offset = -(pnl_permission_list.Size.Height + 14);
      }
      else
      {
        if (m_permission_list.Count > 1)
        {
          m_adjust_offset = ADJUST_LINE_INCREMENT * (m_permission_list.Count - 1);
        }

        pnl_permission_list.Size = new Size(pnl_permission_list.Size.Width, pnl_permission_list.Size.Height + m_adjust_offset);
      }

      // Adjust control positions for pin
      this.GetControlPositions(out _y_button_location, out _form_height);

      this.Size = new Size(this.Size.Width, _form_height);
      btn_shutdown.Location = new Point(btn_shutdown.Location.X, _y_button_location);
      btn_keyboard.Location = new Point(btn_keyboard.Location.X, _y_button_location);
      btn_user_login.Location = new Point(btn_user_login.Location.X, _y_button_location);
      btn_login_cancel.Location = new Point(btn_login_cancel.Location.X, _y_button_location);
      lbl_user_name.Location = new Point(lbl_user_name.Location.X, m_controls_y_position[CONTROL_USER_NAME_LBL] + m_adjust_offset);
      txt_user_name.Location = new Point(txt_user_name.Location.X, m_controls_y_position[CONTROL_USER_NAME_TXT] + m_adjust_offset);
      lbl_user_password.Location = new Point(lbl_user_password.Location.X, m_controls_y_position[CONTROL_USER_PASS_LBL] + m_adjust_offset);
      txt_user_password.Location = new Point(txt_user_password.Location.X, m_controls_y_position[CONTROL_USER_PASS_TXT] + m_adjust_offset);
      lbl_user_username.Location = new Point(lbl_user_username.Location.X, m_controls_y_position[CONTROL_USER_URN_LBL] + m_adjust_offset);
      lbl_user_pin.Location = new Point(lbl_user_pin.Location.X, m_controls_y_position[CONTROL_USER_PIN_TXT] + m_adjust_offset);
      txt_user_pin.Location = new Point(txt_user_pin.Location.X, m_controls_y_position[CONTROL_USER_PIN_LBL] + m_adjust_offset);
      lbl_user_pin_confirm.Location = new Point(lbl_user_pin_confirm.Location.X, m_controls_y_position[CONTROL_USER_CONF_TXT] + m_adjust_offset);
      txt_user_pin_confirm.Location = new Point(txt_user_pin_confirm.Location.X, m_controls_y_position[CONTROL_USER_CONF_LBL] + m_adjust_offset);

      this.SetControlVisibility();

    } // AdjustScreen

    /// <summary>
    /// Get controls positions and form height
    /// </summary>
    /// <param name="YButtonLocation"></param>
    /// <param name="FormHeight"></param>
    private void GetControlPositions(out Int32 YButtonLocation, out Int32 FormHeight)
    {
      FormHeight = DEFAULT_FORM_SIZE_Y + m_adjust_offset;
      YButtonLocation = DEFAULT_BUTTONS_Y + m_adjust_offset;

      if (!m_card_login)
      {
        YButtonLocation -= LOGIN_PIN_RESET_Y_INCREMENT;
        FormHeight -= LOGIN_PIN_RESET_Y_INCREMENT;
      }

      m_user_without_pin = m_card_login;
    } // GetControlPositions

    /// <summary>
    /// Get controls positions and form height
    /// </summary>
    /// <returns></returns>
    private void SetControlVisibility()
    {
      // normal mode controls
      txt_user_name.Visible = !m_card_login;

      // Pin controls
      txt_user_pin.Visible = m_card_login;
      txt_user_pin_confirm.Visible = m_card_login;
      lbl_user_username.Visible = m_card_login;
      lbl_user_pin.Visible = m_card_login;
      lbl_user_pin_confirm.Visible = m_card_login;

      // Show cancel button if login_mode is not LOGIN_MODE_REGULAR or is not card login
      btn_login_cancel.Visible = (m_login_mode != LOGIN_MODE_REGULAR || m_card_login);
      btn_login_cancel.Enabled = (m_login_mode != LOGIN_MODE_REGULAR || m_card_login);

    } // SetControlVisibility

    /// <summary>
    /// Initialize NLS strings
    /// </summary>
    private void InitializeControlResources()
    {
      // - NLS Strings:
      //   - Title
      if (m_login_mode == LOGIN_MODE_REGULAR)
      {
        // Regular login window
        this.FormTitle = Resource.String("STR_FRM_USER_LOGIN_TITLE");
      }
      else
      {
        // Authorization dialog for non-redeemable credit
        this.FormTitle = Resource.String("STR_FRM_AUTHORIZED_USER_TITLE");
      }

      //   - Labels
      lbl_user_name.Text = Resource.String("STR_FRM_USER_LOGIN_USERNAME_NAME");
      lbl_user_password.Text = Resource.String("STR_FRM_USER_LOGIN_USERNAME_PASSWD");
      lbl_user_pin.Text = Resource.String("STR_FRM_USER_NEW_PIN");
      lbl_user_pin_confirm.Text = Resource.String("STR_FRM_USER_CONFIRM_PIN");

      //   - Buttons
      if (m_login_mode == LOGIN_MODE_REGULAR)
      {
        // Regular login window
        btn_user_login.Text = Resource.String("STR_FRM_USER_LOGIN_BTN_LOGIN");
        btn_login_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");
        btn_login_cancel.Visible = false;
        btn_login_cancel.Enabled = false;
      }
      else
      {
        btn_user_login.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_OK");
        btn_login_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");
        btn_login_cancel.Visible = true;
        btn_login_cancel.Enabled = true;
      }

      // - Images:
      if (m_login_mode == LOGIN_MODE_REGULAR)
      {
        // Regular login window
        btn_shutdown.Image = Resources.ResourceImages.shutdown; //  WSI.Cashier.Images.Get32x32(Images.CashierImage.Shutdown);
        btn_shutdown.Enabled = true;
        btn_shutdown.Visible = true;
      }
      else
      {
        btn_shutdown.Image = null;
        btn_shutdown.Enabled = false;
        btn_shutdown.Visible = false;

        btn_keyboard.Location = new Point(btn_shutdown.Location.X, btn_shutdown.Location.Y + m_adjust_offset);
      }

      // DRV 12-AUG-2013: This button it's not disabled to avoid possible error disabling osk
      // btn_keyboard.Visible = !Cashier.HideOsk;

    } // InitializeControlResources

    /// <summary>
    /// Login  user
    /// </summary>
    /// <param name="IsCheckCredentials"></param>
    ///<param name="ErrorStr"></param>
    /// <returns></returns>
    private Boolean LoginUser(out String ErrorStr)
    {
      return LoginUser(false, out ErrorStr);
    } // LoginUser

    private Boolean LoginUser(Boolean IsCheckCredentials, out String ErrorStr)
    {
      String _user_name;
      Encoding _encoding;
      Boolean _enabled;
      Int32 _user_id;
      PasswordPolicy _pwd_policy;
      String AuditStr;

      _encoding = Encoding.Default;
      _user_id = 0;
      _enabled = false;

      ErrorStr = Resource.String("STR_FRM_USER_LOGIN_ERROR_LOGIN_INVALID");
      AuditStr = Resource.String("STR_FRM_USER_LOGIN_ERROR_WRONG_PASSWORD");

      _pwd_policy = new PasswordPolicy();

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          // Get user name
          _user_name = GetUserName();

          // Checking that the username is not empty
          if (String.IsNullOrEmpty(_user_name))
          {
            //empty field not allowed

            return false;
          }

          // Checking that the password is not empty
          if (!this.m_card_login && txt_user_password.Text == "")
          {
            //empty field not allowed
            Auditor.Audit(ENUM_GUI.CASHIER,             // GuiId
                          0,                            // UserId
                          _user_name,                   // UserName
                          Environment.MachineName,      // ComputerName
                          4,                            // AuditCode
                          5000 + 107,                   // NlsId (GUI)
                       AuditStr, "", "", "", "");

            return false;
          }

          // Check username
          if (!DB_CheckUser(_user_name, _db_trx.SqlTransaction, out _user_id, out _enabled))
          {
            Auditor.Audit(ENUM_GUI.CASHIER,             // GuiId
                          0,                            // UserId
                          "SU",                         // UserName
                          Environment.MachineName,      // ComputerName
                          4,                            // AuditCode
                          5000 + 457,                   // NlsId (GUI)
                          _user_name, "", "", "", "");

            return false;
          }

          if (_user_id > 0)
          {
            // Card login (Request PIN)
            if (this.m_card_login && !this.m_user_without_pin)
            {
              if (_enabled && !PinLogin(ref _user_id, ref ErrorStr))
              {
                return false;
              }
            }
            // Default login
            else if (!_pwd_policy.VerifyCurrentPassword(_user_id, txt_user_password.Text, _db_trx.SqlTransaction))
            {
              _user_id = 0;
            }
          }

          //user locked or wrong password
          if (!_enabled || _user_id == 0)
          {
            return WrongUserManager(_user_id
                                    , _user_name
                                    , _enabled
                                    , _pwd_policy
                                    , _db_trx.SqlTransaction
                                    , ref AuditStr
                                    , ref ErrorStr);
          }

          // Login user manager
          return LoginUserManager(IsCheckCredentials
                                  , _user_id
                                  , _user_name
                                  , _enabled
                                  , _db_trx.SqlTransaction
                                  , ref AuditStr
                                  , ref ErrorStr);
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      return false;
    } // LoginUser

    /// <summary>
    /// Pin login
    /// </summary>
    /// <param name="UserId"></param>
    /// <returns></returns>
    private Boolean PinLogin(ref Int32 UserId, ref String ErrorStr)
    {
      switch (this.ShowPinForm(UserId))
      {
        case DialogResult.Abort:
          {
            UserId = 0;
            ErrorStr = string.Empty;
          }
          break;

        case DialogResult.Cancel:
          {
            ErrorStr = string.Empty;

            return false;
          }

        default:
          // Nothing TODO
          break;
      }

      return true;

    } // PinLogin

    /// <summary>
    /// Wrong user manager
    /// </summary>
    /// <param name="UserId"></param>
    /// <param name="UserName"></param>
    /// <param name="Enabled"></param>
    /// <param name="PasswordPolicy"></param>
    /// <param name="AuditStr"></param>
    /// <param name="ErrorStr"></param>
    /// <returns></returns>
    private Boolean WrongUserManager(Int32 UserId, String UserName, Boolean Enabled, PasswordPolicy PasswordPolicy, SqlTransaction SqlTrx, ref String AuditStr, ref String ErrorStr)
    {
      Int32 _login_failures;
      Boolean _is_corporate_user;

      _login_failures = 0;
      _is_corporate_user = false;

      if (Enabled)
      {
        // Increase Password failures
        if (!DB_IncreasePasswordFailures(UserName))
        {
          return false;
        }

        // Get login failures
        if (!DB_GetLoginFailures(UserName, out _login_failures, out _is_corporate_user, SqlTrx))
        {
          return false;
        }

        // Check MaxLoginAttempts parameter
        if (_login_failures == PasswordPolicy.MaxLoginAttempts)
        {
          if (!DB_BlockUser(UserName, ref AuditStr))
          {
            return false;
          }

          //JPJ 10-SEP-2013: Defect WIG-196 We only notify when is not a corporate user, they don't block.
          if (!_is_corporate_user)
          {
            // RRB 07-AUG-2012: Add Auditory "User has been blocked." 
            Auditor.Audit(ENUM_GUI.CASHIER          // GuiId
                          , UserId                    // UserId
                          , UserName                  // UserName
                          , Environment.MachineName   // ComputerName
                          , 4                         // AuditCode        
                          , 5000 + 107                // NlsId (GUI)
                          , AuditStr, "", "", "", "");
          }

          // Insert alarm MAX failed attemps of login
          Alarm.Register(AlarmSourceCode.User
                        , Cashier.TerminalId
                        , UserName + "@" + Environment.MachineName
                        , AlarmCode.User_MaxLoginAttemps);

          return false;
        }
      }
      else
      {
        //user locked
        if (UserId != 0)
        {
          //password is correct
          AuditStr = Resource.String("STR_FRM_USER_LOGIN_ERROR_LOGIN_DISABLED");
          ErrorStr = Resource.String("STR_FRM_USER_LOGIN_ERROR_LOGIN_DISABLED");
        }
      }

      // RRB 07-AUG-2012: Add Auditory "Access Denied." 
      Auditor.Audit(ENUM_GUI.CASHIER           // GuiId
                     , UserId                   // UserId
                     , UserName                 // UserName
                     , Environment.MachineName  // ComputerName
                     , 4                        // AuditCode
                     , 5000 + 107               // NlsId (GUI)
                     , AuditStr, "", "", "", "");

      return false;
    } // WrongUserManager

    /// <summary>
    /// Login user manager
    /// </summary>
    /// <param name="UserId"></param>
    /// <param name="Enabled"></param>
    /// <param name="AuditStr"></param>
    /// <param name="ErrorStr"></param>
    /// <returns></returns>
    private Boolean LoginUserManager(Boolean IsCheckCredentials, Int32 UserId, String UserName, Boolean Enabled, SqlTransaction SqlTrx, ref String AuditStr, ref String ErrorStr)
    {
      Int32 _current_user_id;
      UserLogin.PasswordChange _pwd_change;
      UserInfo _user_info;

      _pwd_change = UserLogin.PasswordChange.PWD_NO_CHANGE;
      _current_user_id = Convert.ToInt32(UserId);

      if (!DB_GetUserInfo(_current_user_id, out _user_info, SqlTrx))
      {
        ErrorStr = Resource.String("STR_APP_GEN_ERROR_DATABASE_ERROR_READING");

        return false;
      }

      _user_info.UserName = UserName;

      if (!Enabled)
      {
        ErrorStr = Resource.String("STR_FRM_USER_LOGIN_ERROR_LOGIN_DISABLED");

        return false;
      }

      // If is check credentials, end. 
      if (IsCheckCredentials)
      {
        // Save new password
        if (!this.SaveUserPin(out ErrorStr))
        {
          return false;
        }
      }

      // User validation
      if (!UserValidation(_user_info, ref _pwd_change, ref ErrorStr, SqlTrx))
      {
        return false;
      }

      // Reset Password failures
      if (!DB_ResetPasswordFailures(_user_info.Id))
      {
        ErrorStr = Resource.String("STR_APP_GEN_ERROR_DATABASE_ERROR_WRITING");

        return false;
      }

      if (m_login_mode == LOGIN_MODE_REGULAR)
      {
        if (!LoginModeRegular(_user_info, _pwd_change, AuditStr, ref ErrorStr, SqlTrx))
        {
          return false;
        }
      }
      else
      {
        // Set fields in order to include in audit records instead of the currently logged it user
        Cashier.AuthorizedByUserId = _current_user_id;
        Cashier.AuthorizedByUserName = _user_info.UserName;

        WSI.Common.CommonCashierInformation.AuthorizedByUserId = _current_user_id;
        WSI.Common.CommonCashierInformation.AuthorizedByUserName = _user_info.UserName;

        Users.SetUserActivity(_current_user_id, "Authorized permission for forms: " + m_str_cashier_forms);
      }

      return true;
    } // LoginUserManager

    /// <summary>
    /// Login mode regular
    /// </summary>
    /// <param name="UserInfo"></param>
    /// <param name="PasswordChange"></param>
    /// <param name="AuditStr"></param>
    /// <param name="ErrorStr"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean LoginModeRegular(UserInfo UserInfo, UserLogin.PasswordChange PasswordChange, String AuditStr, ref String ErrorStr, SqlTransaction SqlTrx)
    {
      String _terminal_name;
      Int32 _current_terminal_id;
      Cashier.BankMode _current_bank_mode;
      Int64 _current_session_id;
      String _message;
      String _title;
      Users.USER_SESSION_STATE _session_state;
      DateTime _gaming_day;

      _terminal_name = Environment.MachineName;
      _gaming_day = new DateTime();

      // Obtain the TerminalId and BankMode
      _current_bank_mode = CashierBusinessLogic.ReadBankMode(SqlTrx);
      if (_current_bank_mode == Cashier.BankMode.Unknown)
      {
        frm_message.Show(Resource.String("STR_APP_GEN_ERROR_DATABASE_ERROR_READING"), Resource.String("STR_APP_GEN_MSG_ERROR"), System.Windows.Forms.MessageBoxButtons.OK, MessageBoxIcon.Error, this);
      }
      // SSC 21-MAR-2012: Moved function ReadTerminalId(SqlTransaction SqlTrx, String TerminalName) to WSI.Common
      _current_terminal_id = WSI.Common.Cashier.ReadTerminalId(SqlTrx, _terminal_name, GU_USER_TYPE.USER);

      if (_current_terminal_id == 0)
      {
        ErrorStr = Resource.String("STR_FRM_USER_LOGIN_TERMINAL_NAME_REPEATED", _terminal_name);
        Log.Error(String.Format("GetOrOpenSystemCashierSession.ReadTerminalId: Error getting the Terminal Id. Terminal Name = {0}", _terminal_name));

        return false;
      }

      if (!GamingTablesSessions.GetGamingTableInfo(_current_terminal_id, SqlTrx))
      {
        Log.Error("Failed GetGamingTableInfo - TerminalId: " + _current_terminal_id);
      }

      if (GamingTablesSessions.GamingTableInfo.IsGamingTable && !GamingTablesSessions.GamingTableInfo.IsEnabled)
      {
        ErrorStr = Resource.String("STR_FRM_USER_LOGIN_GAMBLING_DISABLED").Replace("\\r\\n", "\r\n");

        return false;
      }

      //
      // Check if exist other user with a session opened
      //
      _message = String.Empty;
      _title = String.Empty;
      _session_state = Users.GetStateUserSession("CASHIER", UserInfo.Id, Environment.MachineName, out _message, out _title);

      if (_session_state == Users.USER_SESSION_STATE.OPENED)
      {
        if (UserInfo.Id != 0)
        {
          ErrorStr = _message;

          return false;
        }
      }
      else if (_session_state == Users.USER_SESSION_STATE.EXPIRED || _session_state == Users.USER_SESSION_STATE.UNEXPECTED)
      {
        frm_message.Show(_message, _title,
                          MessageBoxButtons.OK,
                          MessageBoxIcon.Warning,
                          this.ParentForm);
      }

      _current_session_id = CashierBusinessLogic.GetSessionId(SqlTrx, _current_bank_mode, _current_terminal_id, UserInfo.Id);

      // Get opening date and store it into memory variable,
      // and reset notification
      if (_current_session_id != 0)
      {
        this.m_parent_window.SystemInfo.CashSessionOpeningDate = CashierBusinessLogic.GetSessionOpeningDate(_current_session_id, SqlTrx);
        _gaming_day = CashierBusinessLogic.GetSessionGamingDay(_current_session_id, SqlTrx);
      }

      // Check if any cashier session is open in another terminal
      switch (_current_bank_mode)
      {
        case Cashier.BankMode.PerUser:
          // Check cashier sessions
          if (!DB_CheckCashierSessions(UserInfo.UserName, _current_terminal_id, _current_session_id, ref ErrorStr, SqlTrx))
          {
            // Check Gaming table cashier sessions
            if (!DB_CheckCashierSessions_GamingTables(UserInfo.UserName, _current_terminal_id, _current_session_id, ref ErrorStr, SqlTrx))
            {
              return false;
            }

            // Request user
            if (frm_message.Show(Resource.String("STR_FRM_USER_LOGIN_SESSION_OPEN", UserInfo.UserName)
                                , Resource.String("STR_APP_GEN_MSG_WARNING")
                                , MessageBoxButtons.YesNo
                                , MessageBoxIcon.Warning
                                , this.ParentForm) == DialogResult.Cancel)
            {
              ErrorStr = Resource.String("STR_FRM_USER_LOGIN_ERROR_LOGIN_CANCEL");

              return false;
            }
          }

          break;
      }

      // Check if password change required flag is active
      if (UserInfo.PasswordChangeRequired)
      {
        PasswordChange = UserLogin.PasswordChange.PWD_CHANGE_REQUIRED;
      }

      // Check if password must be changed
      if (PasswordChange != UserLogin.PasswordChange.PWD_NO_CHANGE)
      {
        //JPJ 29-AUG-2013: Defect WIG-165
        if (WSI.Common.GeneralParam.GetBoolean("Site", "MultiSiteMember", false) && UserInfo.MasterUser)
        {
          ErrorStr = Resource.String("STR_FRM_USER_LOGIN_ERROR_CORPORATE_USER_LOGIN_REQUIRED");

          WSI.Common.Auditor.Audit(ENUM_GUI.CASHIER,                // GuiId
                                    UserInfo.Id,                    // UserId
                                    UserInfo.UserName,              // UserName
                                    Environment.MachineName,        // ComputerName
                                    4,                              // AuditCode
                                    16000 + 2380,                   // NlsId (GUI)
                                    AuditStr, "", "", "", "");

          return false;
        }

        // Password change audit needs Cashier info
        Cashier.SetUserLoggedIn(_current_terminal_id, _terminal_name, UserInfo.Id, UserInfo.UserName, _current_bank_mode, _current_session_id, _gaming_day);

        switch (this.UserPwdChange(UserInfo.Id, UserInfo.UserName, PasswordChange))
        {
          case DialogResult.OK:
            break;

          case DialogResult.Cancel:
              ErrorStr = String.Empty;

              return false;

          default:
              ErrorStr = Resource.String("STR_FRM_USER_LOGIN_ERROR_PWD_CHANGE");

              return false;
            }
        }

      // Maybe setted before
      if (Cashier.TerminalId == 0)
      {
        Cashier.SetUserLoggedIn(_current_terminal_id, _terminal_name, UserInfo.Id, UserInfo.UserName, _current_bank_mode, _current_session_id, _gaming_day);
      }

      return true;

    } // LoginModeRegular


    /// <summary>
    /// Block user 
    /// </summary>
    /// <param name="UserName"></param>
    /// <returns></returns>
    private Boolean DB_BlockUser(String UserName, ref String AuditStr)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      // XCD 21-AUG-2012 Changed GU_ENABLED to GU_BLOCK_REASON
      _sb.AppendLine(" UPDATE   GUI_USERS ");
      _sb.AppendLine("    SET   GU_BLOCK_REASON = GU_BLOCK_REASON | @pBlockReason ");
      _sb.AppendLine("        , GU_LOGIN_FAILURES = 0                             ");
      _sb.AppendLine("  WHERE   GU_USERNAME = @pUserName                          ");
      _sb.AppendLine("    AND   GU_USER_ID <> 0                                   "); // only block user when is not a superuser

      // AMF & DDM 27-AUG-2013 Fixed Bug WIG-163
      if (WSI.Common.GeneralParam.GetBoolean("Site", "MultiSiteMember", false))
      {
        // Not block if is a corporate user!!
        _sb.AppendLine("    AND   GU_MASTER_ID IS NULL");
      }

      using (DB_TRX _db_trx = new DB_TRX())
      {
        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
        {
          _sql_cmd.Parameters.Add("@pBlockReason", SqlDbType.Int).Value = (Int32)GUI_USER_BLOCK_REASON.WRONG_PASSWORD;
          _sql_cmd.Parameters.Add("@pUserName", SqlDbType.NVarChar).Value = UserName;

          if (_sql_cmd.ExecuteNonQuery() == 1)
          {
            AuditStr = Resource.String("STR_FRM_USER_LOGIN_ERROR_LOGIN_DISABLING");

            return _db_trx.Commit();
          }
        }
      }

      return false;

    } // DB_BlockUser

    /// <summary>
    /// Get login failures
    /// </summary>
    /// <param name="UserName"></param>
    /// <param name="LoginFailures"></param>
    /// <param name="IsCorporateUser"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean DB_GetLoginFailures(String UserName, out Int32 LoginFailures, out Boolean IsCorporateUser, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      LoginFailures = 0;
      IsCorporateUser = false;

      _sb = new StringBuilder();
      _sb.AppendLine("  SELECT   ISNULL(GU_LOGIN_FAILURES, 0)                                                 ");
      _sb.AppendLine("         , CAST (CASE WHEN GU_MASTER_ID IS NULL THEN 0 ELSE 1 END AS BIT) AS GU_MASTER  ");
      _sb.AppendLine("    FROM   GUI_USERS                                                                    ");
      _sb.AppendLine("   WHERE   GU_USERNAME = @pUserName                                                     ");

      using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
      {
        _sql_cmd.Parameters.Add("@pUserName", SqlDbType.NVarChar, 50, "GU_USERNAME").Value = UserName;

        using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
        {
          if (_reader.Read())
          {
            LoginFailures = (Int32)_reader[0];
            IsCorporateUser = (Boolean)_reader[1];

            return true;
          }
        }
      }

      return false;
    } // DB_GetLoginFailures

    /// <summary>
    /// Increase password failures
    /// </summary>
    /// <param name="UserName"></param>
    /// <returns></returns>
    private Boolean DB_IncreasePasswordFailures(String UserName)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine(" UPDATE   GUI_USERS                                             ");
      _sb.AppendLine("    SET   GU_LOGIN_FAILURES = ISNULL(GU_LOGIN_FAILURES, 0) + 1  ");
      _sb.AppendLine("  WHERE   GU_USERNAME = @pUserName                              ");
      _sb.AppendLine("    AND   GU_USER_ID <> 0                                       ");

      using (DB_TRX _db_trx = new DB_TRX())
      {
        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
        {
          _sql_cmd.Parameters.Add("@pUserName", SqlDbType.NVarChar, 50, "GU_USERNAME").Value = UserName;

          if (_sql_cmd.ExecuteNonQuery() == 1)
          {
            return _db_trx.Commit();
          }
        }
      }

      return false;
    } // DB_IncreasePasswordFailures

    /// <summary>
    /// Check cashier session
    /// </summary>
    /// <param name="CurrentTerminalId"></param>
    /// <param name="CurrentSessionId"></param>
    /// <param name="ErrorStr"></param>
    /// <returns></returns>
    private Boolean DB_CheckCashierSessions(String UserName, Int32 CurrentTerminalId, Int64 CurrentSessionId, ref String ErrorStr, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine(" SELECT   CS_USER_ID                    ");
      _sb.AppendLine("   FROM   CASHIER_SESSIONS              ");
      _sb.AppendLine("  WHERE   CS_CASHIER_ID <> @pCashierId  ");
      _sb.AppendLine("    AND   CS_SESSION_ID  = @pSessionId  ");
      _sb.AppendLine("    AND   CS_STATUS      = @pStatusOpen ");

      using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
      {
        _sql_cmd.Parameters.Add("@pCashierId", SqlDbType.Int).Value = CurrentTerminalId;
        _sql_cmd.Parameters.Add("@pSessionId", SqlDbType.Int).Value = CurrentSessionId;
        _sql_cmd.Parameters.Add("@pStatusOpen", SqlDbType.Int).Value = CASHIER_SESSION_STATUS.OPEN;

        return (_sql_cmd.ExecuteScalar() == null);
      }
    } // DB_CheckCashierSessions

    /// <summary>
    /// Check cashier sessions of gaming tables 
    /// </summary>
    /// <param name="UserName"></param>
    /// <param name="CurrentTerminalId"></param>
    /// <param name="CurrentSessionId"></param>
    /// <param name="ErrorStr"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean DB_CheckCashierSessions_GamingTables(String UserName, Int32 CurrentTerminalId, Int64 CurrentSessionId, ref String ErrorStr, SqlTransaction SqlTrx)
    {
      String _sql_sessions;
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine(" SELECT   *                                     ");
      _sb.AppendLine("   FROM   GAMING_TABLES                         ");
      _sb.AppendLine("  WHERE   GT_CASHIER_ID IN (@pCashierId, {0} )  ");

      _sql_sessions = "(SELECT CS_CASHIER_ID FROM CASHIER_SESSIONS WHERE CS_SESSION_ID = @pSessionId)";

      using (SqlCommand _sql_cmd = new SqlCommand(String.Format(_sb.ToString(), _sql_sessions), SqlTrx.Connection, SqlTrx))
      {
        _sql_cmd.Parameters.Add("@pCashierId", SqlDbType.Int).Value = CurrentTerminalId;
        _sql_cmd.Parameters.Add("@pSessionId", SqlDbType.Int).Value = CurrentSessionId;
        _sql_cmd.Parameters.Add("@pStatusOpen", SqlDbType.Int).Value = CASHIER_SESSION_STATUS.OPEN;

        if (_sql_cmd.ExecuteScalar() != null)
        {
          ErrorStr = Resource.String("STR_FRM_USER_LOGIN_GAMING_SESSION_OPEN", UserName);

          return false;
        }
      }

      return true;
    } // DB_CheckCashierSessions_GamingTables

    /// <summary>
    /// Update login failures
    /// </summary>
    /// <param name="UserId"></param>
    /// <returns></returns>
    private bool DB_ResetPasswordFailures(Int32 UserId)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine("   UPDATE   GUI_USERS                     ");
      _sb.AppendLine("      SET   GU_LOGIN_FAILURES = 0         ");
      _sb.AppendLine("    WHERE   GU_USER_ID        = @pUserId  ");

      using (DB_TRX _db_trx = new DB_TRX())
      {
        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
        {
          _sql_cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = UserId;

          if (_sql_cmd.ExecuteNonQuery() == 1)
          {
            return _db_trx.Commit();
          }
        }
      }

      return false;
    } // DB_ResetPasswordFailures

    /// <summary>
    /// Read and Check User: Enable, Not expired, Permissions, ...
    /// </summary>
    /// <param name="UserInfo"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private bool DB_ReadPermissions(UserInfo UserInfo, SqlTransaction SqlTrx)
    {
      Object _obj;
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine(" SELECT   GPF_READ_PERM                  ");
      _sb.AppendLine("   FROM   GUI_PROFILE_FORMS              ");
      _sb.AppendLine("  WHERE   GPF_PROFILE_ID  =  @pProfileId ");
      _sb.AppendLine("    AND   GPF_FORM_ID     =  @pFormId    ");
      _sb.AppendLine("    AND   GPF_GUI_ID      =  @pGuiId     ");

      // XCD 21-AUG-2012 Changed GU_ENABLED to GU_BLOCK_REASON
      using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
      {
        _sql_cmd.Parameters.Add("@pProfileId", SqlDbType.Int).Value = UserInfo.ProfileId;
        _sql_cmd.Parameters.Add("@pFormId", SqlDbType.Int).Value = (m_login_mode == LOGIN_MODE_REGULAR) ? CASHIER_FORM_MAIN : m_cashier_form;
        _sql_cmd.Parameters.Add("@pGuiId", SqlDbType.Int).Value = (int)ENUM_GUI.CASHIER;

        _obj = _sql_cmd.ExecuteScalar();

        if (_obj != null)
        {
          return (Boolean)_obj;
        }
      }

      return false;
    } // DB_ReadPermissions

    /// <summary>
    /// Get user info
    /// </summary>
    /// <param name="CurrentUserId"></param>
    /// <param name="UserInfo"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean DB_GetUserInfo(Int32 CurrentUserId, out UserInfo UserInfo, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      UserInfo = this.GetNewUserInfo();

      _sb = new StringBuilder();
      _sb.AppendLine("   SELECT   GU_PROFILE_ID                                                               ");
      _sb.AppendLine("          , GU_BLOCK_REASON                                                             ");
      _sb.AppendLine("          , GU_NOT_VALID_BEFORE                                                         ");
      _sb.AppendLine("          , GU_NOT_VALID_AFTER                                                          ");
      _sb.AppendLine("          , GU_LAST_CHANGED                                                             ");
      _sb.AppendLine("          , GU_PASSWORD_EXP                                                             ");
      _sb.AppendLine("          , GU_PWD_CHG_REQ                                                              ");
      _sb.AppendLine("          , ISNULL(GU_LOGIN_FAILURES, 0) AS GU_LOGIN_FAILURES                           ");
      _sb.AppendLine("          , CAST (CASE WHEN GU_MASTER_ID IS NULL THEN 0 ELSE 1 END AS BIT) AS GU_MASTER ");
      _sb.AppendLine("     FROM   GUI_USERS                                                                   ");
      _sb.AppendLine("    WHERE   GU_USER_ID = @pUserId                                                       ");

      // 
      // Read and Check User: Enable, Not expired, Permissions, ...
      //

      // XCD 21-AUG-2012 Changed GU_ENABLED to GU_BLOCK_REASON
      using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
      {
        _sql_cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = CurrentUserId;

        using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
        {
          if (_reader.Read())
          {
            UserInfo.Id = CurrentUserId;

            UserInfo.ProfileId = (Int32)_reader[DB_USER_INFO_PROFILE_ID];
            UserInfo.Enabled = ((Int32)_reader[DB_USER_INFO_BLOCK_REASON]).Equals((Int32)GUI_USER_BLOCK_REASON.NONE) ? true : false;
            UserInfo.PasswordChangeRequired = (Boolean)_reader[DB_USER_INFO_PASSWORD_CHG_REQ];
            UserInfo.LoginFailures = (Int32)_reader[DB_USER_INFO_LOGIN_FAILURES];
            UserInfo.MasterUser = (Boolean)_reader[DB_USER_INFO_MASTER_USER];
            UserInfo.NotValidBefore = (DateTime)_reader[DB_USER_INFO_NOT_VALID_BEFORE];

            UserInfo.NotValidAfter = (_reader[DB_USER_INFO_NOT_VALID_AFTER] == DBNull.Value) ? WGDB.Now.AddDays(1) : (DateTime)_reader[DB_USER_INFO_NOT_VALID_AFTER];
            UserInfo.LastChanged = (_reader[DB_USER_INFO_LAST_CHANGED] == DBNull.Value) ? WGDB.Now : (DateTime)_reader[DB_USER_INFO_LAST_CHANGED];

            if (_reader[DB_USER_INFO_PASSWORD_EXP] == DBNull.Value)
            {
              UserInfo.PasswordExpireDate = WGDB.Now;
              UserInfo.PasswordExpireDays = 0;
            }
            else
            {
              UserInfo.PasswordExpireDate = (DateTime)_reader[DB_USER_INFO_PASSWORD_EXP];
              UserInfo.PasswordExpireDays = UserInfo.PasswordExpireDate.Subtract(UserInfo.NotValidBefore).Days;
            }

            return true;
          }
        }
      }

      return false;
    } // DB_GetUserInfo

    /// <summary>
    /// Check user and get user id and enabled property
    /// </summary>
    /// <param name="UserName"></param>
    /// <param name="SqlTrx"></param>
    /// <param name="UserId"></param>
    /// <param name="Enabled"></param>
    /// <returns></returns>
    private Boolean DB_CheckUser(String UserName, SqlTransaction SqlTrx, out Int32 UserId, out Boolean Enabled)
    {
      Int32 _block_reason;
      StringBuilder _sb;

      UserId = 0;
      Enabled = false;

      _sb = new StringBuilder();
      _sb.AppendLine("     SELECT   GU_USER_ID                  ");
      _sb.AppendLine("            , ISNULL(GU_BLOCK_REASON, 0)  ");
      _sb.AppendLine("       FROM   GUI_USERS                   ");
      _sb.AppendLine("      WHERE   GU_USERNAME = @pUserName    ");

      using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
      {
        _sql_cmd.Parameters.Add("@pUserName", SqlDbType.NVarChar).Value = UserName;

        using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
        {
          if (_reader.Read())
          {
            UserId = Convert.ToInt32(_reader[0]);
            _block_reason = Convert.ToInt32(_reader[1]);

            Enabled = (_block_reason.Equals((Int32)GUI_USER_BLOCK_REASON.NONE));

            return true;
          }
        }
      }

      return false;

    } // DB_CheckUser

    /// <summary>
    /// User validation
    /// </summary>
    /// <param name="UserInfo"></param>
    /// <param name="PasswordChange"></param>
    /// <param name="ErrorStr"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean UserValidation(UserInfo UserInfo, ref UserLogin.PasswordChange PasswordChange, ref String ErrorStr, SqlTransaction SqlTrx)
    {
      Double _total_days;

      //JPJ 29-AUG-2013: Defect WIG-165
      if (GeneralParam.GetBoolean("Site", "MultiSiteMember", false) && UserInfo.MasterUser)
      {
        if (PasswordChange == UserLogin.PasswordChange.PWD_CHANGE_REQUIRED)
        {
          ErrorStr = Resource.String("STR_FRM_USER_LOGIN_ERROR_CORPORATE_USER_LOGIN_CHANGE");

          return false;
        }
      }

      _total_days = WGDB.Now.Subtract(UserInfo.NotValidBefore).TotalDays;
      if (_total_days < 0.0)
      {
        ErrorStr = Resource.String("STR_FRM_USER_LOGIN_ERROR_LOGIN_DISABLED");

        return false;
      }

      _total_days = UserInfo.NotValidAfter.Subtract(WGDB.Now).TotalDays;
      if (_total_days < 0.0)
      {
        ErrorStr = Resource.String("STR_FRM_USER_LOGIN_ERROR_LOGIN_DISABLED");

        return false;
      }

      _total_days = WGDB.Now.Subtract(UserInfo.LastChanged).TotalDays;
      if (UserInfo.PasswordExpireDays > 0 && _total_days >= UserInfo.PasswordExpireDays)
      {
        PasswordChange = UserLogin.PasswordChange.PWD_CHANGE_REQUIRED;
      }

      // Check permissions
      if (m_login_mode == LOGIN_MODE_MULTI_AUTHORIZATION)
      {
        if (!ProfilePermissions.CheckPermissionList(m_permission_list, ProfilePermissions.TypeOperation.OnlyCheck, this, UserInfo.Id, out ErrorStr))
        {
          return false;
        }
      }
      else
      {
        // Read permisions
        if (!DB_ReadPermissions(UserInfo, SqlTrx))
        {
          ErrorStr = Resource.String("STR_FRM_USER_LOGIN_ERROR_LOGIN_PERMS");

          return false;
        }
      }

      return true;

    } // UserValidation

    /// <summary>
    /// Get user name
    /// </summary>
    /// <returns></returns>
    private String GetUserName()
    {
      if (m_card_login)
      {
        return m_user_card.UserName;
      }

      return txt_user_name.Text;
    } // GetUserName

    private DialogResult ShowPinForm(Int32 UserId)
    {
      try
      {
        if (UserId <= 0)
        {
          return DialogResult.Abort;
        }

        using (frm_pin_imput _frm_pin_input = new frm_pin_imput())
        {
          return _frm_pin_input.Show(UserId, this);
      }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return DialogResult.Abort;

    } // ShowPinForm

    private void VerifyLoginInput()
    {
      btn_user_login.Enabled = ((txt_user_name.Text != "") && (txt_user_password.Text != ""));
    } // VerifyLoginInput

    // PURPOSE: Function used to performs
    //          the User Password Change
    //          
    //    - INPUT: 
    //             GUIId:      Gui identifier, used to get the User permissions
    //             GUIName:    User Name
    //             ChangeMode: Password change mode
    //
    //    - OUTPUT: None
    //
    // RETURNS: 
    //         - TRUE:  Password Change OK
    //         - FALSE: Password Change fails
    //
    // NOTES
    //
    private DialogResult UserPwdChange(Int32 GUIId,
                                       String UserName,
                                       UserLogin.PasswordChange ChangeMode)
    {
      DialogResult _result;

      // ICS 14-APR-2014 Free the object
      using (frm_pwd_change _frm = new frm_pwd_change(GUIId, UserName, ChangeMode))
      {
        _result = _frm.Show(this);
      }

      return (_result);
    } // UserPwdChange

    /// <summary>
    /// Restore Parent Focus
    /// </summary>
    private void RestoreParentFocus()
    {
      if (m_parent_window != null)
      {
        m_parent_window.Focus();
        m_parent_window.BringToFront();

        SetImputFocus();
      }
    } // RestoreParentFocus

    #endregion

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Init and Show the form.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Parent
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public void Show(frm_container Parent)
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_user_login", Log.Type.Message);

      AdjustScreen();

      Parent.uc_card1.Visible = false;
      Parent.uc_bank1.Visible = false;
      Parent.uc_gt_player_tracking1.Visible = false;

      m_parent_window = Parent;

      // Regular application login
      m_login_mode = LOGIN_MODE_REGULAR;

      m_form_yes_no.Show(Parent);

      if (WindowManager.StartUpMode == WindowManager.StartUpModes.Explorer)
      {
        this.Location = new Point(WindowManager.GetFormCenterLocation(m_form_yes_no).X - (this.Width / 2)
                                 , WindowManager.GetFormCenterLocation(m_form_yes_no).Y - (this.Height / 2) - 85);
      }
      else
      {
        this.Location = new Point(Parent.Width / 2 - this.Width / 2, Parent.Height / 2 - 200);
      }

#if DEBUG
      String _aux_value1;
      String _aux_value2;
      Boolean _show_keyboard = true;

      if (Environment.GetEnvironmentVariable("LKS_VC_DEV") != null)
      {
        _aux_value1 = Environment.GetEnvironmentVariable("LKS_VC_CASHIER_USER_NAME");
        _aux_value2 = Environment.GetEnvironmentVariable("LKS_VC_CASHIER_USER_PASSWORD");

        if (_aux_value1 != null && _aux_value2 != null)
        {
          txt_user_name.Text = _aux_value1;
          txt_user_password.Text = _aux_value2;

          _show_keyboard = false;
        }
      }

      if (_show_keyboard)
      {
        WSIKeyboard.Show();
      }
      else
      {
        WSIKeyboard.Hide();
      }
#else
      //Shows OSK if Automatic OSK it's enabled
      WSIKeyboard.Show();
#endif

      Parent.Activate();
      this.Activate();
      this.RestoreParentFocus();
      this.ShowDialog(m_form_yes_no);

      WSIKeyboard.Hide();
      m_form_yes_no.Hide();

      this.RestoreParentFocus();
    } // Show

    public void Show(ProfilePermissions.CashierFormFuncionality Frm, Form Parent, out Boolean Authorized)
    {
      m_cashier_form = (int)Frm;
      m_str_cashier_forms = m_cashier_form.ToString("000");

      Show(Parent, out Authorized);
    }

    public void Show(List<ProfilePermissions.CashierFormFuncionality> PermissionList, Form Parent, out Boolean Authorized)
    {
      String _resource_name;
      String _str_perm;

      m_permission_list = PermissionList;

      txt_permission_list.Text = " " + Resource.String("STR_FRM_AUTHORIZED_USER_PERMISSIONS");

      m_str_cashier_forms = "";
      foreach (ProfilePermissions.CashierFormFuncionality _perm in PermissionList)
      {
        if (!String.IsNullOrEmpty(m_str_cashier_forms))
        {
          m_str_cashier_forms += ", ";
        }
        _str_perm = ((Int32)_perm).ToString("000");
        m_str_cashier_forms += _str_perm;

        _resource_name = "STR_FRM_AUTHORIZED_USER_PERMISSION_" + _str_perm;

        txt_permission_list.Text += "\r\n     * " + Resource.String(_resource_name);
      }

      //JBC 11-NOV-2013: Allow to replace Anti-Money Laundering Name.
      if (txt_permission_list.Text.Contains(Resource.String("STR_MSG_ANTIMONEYLAUNDERING")))
      {
        txt_permission_list.Text = txt_permission_list.Text.Replace(Resource.String("STR_MSG_ANTIMONEYLAUNDERING"), Accounts.getAMLName());
      }

      Show(Parent, out Authorized);
    }

    public void Show(Form Parent, out Boolean Authorized)
    {
      if (Parent == null)
      {
        Authorized = false;
        return;
      }

      AdjustScreen();

      this.Location = new Point(Parent.Location.X + Parent.Width / 2 - this.Width / 2,
                                Parent.Location.Y + Parent.Height / 2 - 250);

      m_form_yes_no.Show(Parent);

      WSIKeyboard.Show();
      txt_user_name.Focus();

      this.ShowDialog(m_form_yes_no);

      Authorized = m_authorized;

      WSIKeyboard.Hide();
      m_form_yes_no.Hide();

      this.RestoreParentFocus();
    } // Show

    /// <summary>
    /// Login function
    /// </summary>
    private void Login()
    {
      try
      {
        // Login with UserCard mode
        if (m_card_login)
        {
          this.LoginUserCard();

          return;
        }

        // Traditional login mode
        this.UserLoginManager();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } // Login

    /// <summary>
    /// Login with user card
    /// </summary>
    private void LoginUserCard()
    {
      String _error_str;

      // Check user credentials 
      if (!LoginUser(this.m_user_without_pin, out _error_str))
      {
        InitialFormState(_error_str);

        m_card_login = m_user_without_pin;

        return;
      }

      this.LoginManager();

      m_card_login = false;

    } // Login

    //------------------------------------------------------------------------------
    // PURPOSE : Dispose yes_no form.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    new public void Dispose()
    {
      m_form_yes_no.Dispose();
      m_form_yes_no = null;

      base.Dispose();
    }

    #endregion

    #region ButtonHandling

    private void btn_ok_Click(object sender, EventArgs e)
    {
      this.Login();
    } // btn_ok_Click

    /// <summary>
    /// User login manager
    /// </summary>
    private void UserLoginManager()
    {
      String error_str;

      if (LoginUser(out error_str))
      {
        this.LoginManager();
      }
      else
      {
        InitialFormState(error_str);
      }

    } // UserLoginManager

    /// <summary>
    /// Initial form state
    /// </summary>
    /// <param name="ErrorStr"></param>
    private void InitialFormState(String ErrorStr)
    {
      ShowError(ErrorStr);

      this.ResetCredentialsImputs();

      txt_user_name.SelectAll();
      txt_user_name.Focus();

      if (m_login_mode != LOGIN_MODE_REGULAR)
      {
        // Deny authorization
        m_authorized = false;
      }
    } // InitialFormState

    /// <summary>
    /// Reset credential imputs
    /// </summary>
    private void ResetCredentialsImputs()
    {
      this.txt_user_password.Text = String.Empty;
      this.txt_user_pin.Text = String.Empty;
      this.txt_user_pin_confirm.Text = String.Empty;

      this.SetImputFocus();
    } // ResetCredentialsImputs

    /// <summary>
    /// Set imput focus
    /// </summary>
    private void SetImputFocus()
    {
      this.txt_user_name.Focus();

      if (m_card_login && m_user_without_pin)
      {
        this.txt_user_password.Focus();
      }
    } // SetImputFocus

    /// <summary>
    ///  Manage login
    /// </summary>
    private void LoginManager()
    {
      String _error_str;
      KeyboardMessageFilter.UnregisterHandler(this, this.BarcodeListener);

      if (m_login_mode == LOGIN_MODE_REGULAR)
      {
        m_correct_login = true;

        Misc.WriteLog("[FORM CLOSE] frm_ok (login_mode_regular)", Log.Type.Message);
        this.Close();

        if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.AccessCash, ProfilePermissions.TypeOperation.OnlyCheck, this.ParentForm, out _error_str) && !Misc.IsGamingHallMode())
        {
          m_parent_window.btn_cards_Click(new object(), new EventArgs());
        }
        else
        {
          if (Cashier.SessionId != 0)
          {
            switch (Misc.SystemMode())
            {
              case SYSTEM_MODE.CASHLESS:
              case SYSTEM_MODE.MICO2:
                m_parent_window.btn_cards_Click(new object(), new EventArgs());
                break;

              case SYSTEM_MODE.TITO:
              case SYSTEM_MODE.WASS:
                // In case the cashier status is expired, the cash will be shown
                if (m_parent_window.SystemInfo.WorkShiftExpired == SystemInfo.StatusInfo.EXPIRED)
                {
                  m_parent_window.btn_bank_Click(new object(), new EventArgs());
                }
                else
                {
                  m_parent_window.btn_create_ticket_Click(new object(), new EventArgs());
                }
                break;

              case SYSTEM_MODE.GAMINGHALL:
                if (ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.GAMINGHALL_Collect, ProfilePermissions.TypeOperation.OnlyCheck, this.ParentForm, out _error_str))
                {
                  m_parent_window.btn_collect_Click(new object(), new EventArgs());
                }
                else
                {
                  m_parent_window.btn_bank_Click(new object(), new EventArgs());
                }
                break;

              default:
                Log.Error("btn_ok_Click() SYSTEM_MODE not definet: " + Misc.SystemMode());
                break;
            }
          }
          else
          {
            m_parent_window.btn_bank_Click(new object(), new EventArgs());
          }
        }
        // DCS - 08-AUG-2014: If mode is gaming table show gaming table else cage
        if (WindowManager.IsTableMode && GamingTableBusinessLogic.IsEnabledGTPlayerTracking())
        {
          m_parent_window.btn_player_tracking_Click();
        }
      }
      else
      {
        // Set final answer
        m_authorized = true;

        Hide();

        Misc.WriteLog("[FORM CLOSE] frm_user_login (not login_mode_regular)", Log.Type.Message);
        this.Close();
      }
    } // LoginManager

    /// <summary>
    /// Save user pin
    /// </summary>
    private Boolean SaveUserPin(out String ErrorStr)
    {
      String _pin;
      String _confirm_pin;
      Int32 _numeric_pin;

      ErrorStr = String.Empty;
      _pin = this.txt_user_pin.Text;
      _confirm_pin = this.txt_user_pin_confirm.Text;

      if (_pin != _confirm_pin)
      {
        ErrorStr = Resource.String("STR_FRM_USER_CONFIRM_PIN_ERROR");

        return false;
      }

      if (_pin.Length != 4
        || !Int32.TryParse(_pin, out _numeric_pin))
      {
        ErrorStr = Resource.String("STR_FRM_INCORRECT_PIN_FORMAT");

        return false;
      }

      if (!this.m_user_card.UpdatePin(_pin))
      {
        ErrorStr = Resource.String("STR_FRM_USER_LOGIN_ERROR_LOGIN_INVALID");

        return false;
      }

      return true;
    } // Save user pin

    /// <summary>
    /// Show error message
    /// </summary>
    /// <param name="Error"></param>
    private void ShowError(String Error)
    {
      if (!String.IsNullOrEmpty(Error))
      {
        frm_message.Show(Error, Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this);
      }
    } // ShowError

    private void btn_login_cancel_Click(object sender, EventArgs e)
    {
      if (m_card_login)
      {
        this.m_card_login = false;
        this.AdjustScreen();
        this.ResetCredentialsImputs();

        return;
      }

      // Deny Authorization
      m_authorized = false;
      Hide();

      Misc.WriteLog("[FORM CLOSE] frm_login_cancel", Log.Type.Message);
      this.Close();
    } // btn_login_cancel_Click

    private void btn_keys_Click(object sender, EventArgs e)
    {
      //Shows OSK
      WSIKeyboard.ForceToggle();
      txt_user_name.SelectAll();
      txt_user_name.Focus();
    }

    private void btn_shutdown_Click(object sender, EventArgs e)
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[PROMPTED SHUTDOWN] REASON: User action (Login screen)", Log.Type.Message);
      Cashier.Shutdown(true, Users.EXIT_CODE.NOTLOGIN);
      this.BringToFront();
    }

    #endregion

    #region EventHandling

    private void frm_user_login_FormClosing(object sender, FormClosingEventArgs e)
    {
      if (e.CloseReason == CloseReason.WindowsShutDown)
      {
        // ATB 16-FEB-2017
        Misc.WriteLog("[SHUTDOWN] REASON: Close login screen", Log.Type.Message);
        Cashier.Shutdown(false);

        return;
      }

      if (!m_correct_login)
      {
        // Prevent user from closing the form with ALT + F4 shortcut
        e.Cancel = true;
      }
    }

    private void frm_user_login_Shown(object sender, EventArgs e)
    {
      this.txt_user_name.Focus();
    } // frm_user_login_Shown

    #endregion

  }
}