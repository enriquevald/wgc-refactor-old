﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: ExchangeMultiDenomination.cs
// 
//   DESCRIPTION: 
// 
//        AUTHOR: Ramón Monclús
// 
// CREATION DATE: 03-MAY-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 03-MAY-2016 FAV    First Release 
// 31-MAY-2016 ETP    Fixed Bug 13994 - Excepción no controlada al no tener configurada ninguna divisa.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using WSI.Common;

namespace WSI.Cashier
{
  public class ExchangeMultiDenomination
  {
    #region Attributes
    #endregion

    #region Constructor
    #endregion

    #region Properties
    #endregion

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Change foreing currency to national currency. For currency exchange.
    //
    //  PARAMS :
    //      - INPUT :
    //          - CashierSession
    //          - CardData
    //          - CurrencyExchangeResult
    //
    //      - OUTPUT : 
    //          - Voucher 
    //
    // RETURNS :
    //      True if the operation was successfully
    //
    public Boolean ChangeForeignCurrencyToNationalCurrency(CashierSessionInfo CashierSession, CardData CardData,
                                                           CurrencyExchangeResult CurrencyExchangeResult, out VoucherMultiCurrencyExchange Voucher)
    {
      Int64 _operation_id;
      AccountMovementsTable _account_movement;
      CashierMovementsTable _cm_mov_table;
      CurrencyExchangeVoucher _voucherData;
      Decimal _net_amount;
      Decimal _current_national_balance;
      Voucher = null;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _net_amount = CurrencyExchangeResult.NetAmount;

          // 1. Check if there is national currency in the Cashier
          _current_national_balance = GetCashCashierCurrentBalance(CurrencyExchangeResult.OutCurrencyCode);

          if (_net_amount > _current_national_balance)
          {
            Log.Error("There is not enough national money in the cash desk");
            return false;
          }

          // 2. Account Operation Id
          if (!Operations.DB_InsertOperation(OperationCode.CURRENCY_EXCHANGE_CHANGE, CardData.AccountId, CashierSession.CashierSessionId,
                                              0, 0, _net_amount, 0, 0, 0, string.Empty, out _operation_id, _db_trx.SqlTransaction))
          {
            Log.Message(string.Format("Error creating the OperationId. AccountID [{0}], SessionID [{1}]", CardData.AccountId, CashierSession.CashierSessionId));
            return false;
          }

          // 3. Insert the account movements 
          _account_movement = new AccountMovementsTable(CashierSession);

          if (!_account_movement.Add(_operation_id, CardData.AccountId, MovementType.CurrencyExchangeCashierIn, 0, 0, _net_amount, _net_amount)
              || !_account_movement.Add(_operation_id, CardData.AccountId, MovementType.CurrencyExchangeCashierOut, _net_amount, _net_amount, 0, 0)
              || !_account_movement.Save(_db_trx.SqlTransaction))
          {
            Log.Message(string.Format("Error creating the Account movements. AccountID [{0}], SessionID [{1}]", CardData.AccountId, CashierSession.CashierSessionId));
            return false;
          }

          // 4. Insert Cashier movements
          _cm_mov_table = new CashierMovementsTable(CashierSession);

          if (!_cm_mov_table.AddCurrencyExchange(_operation_id, CardData.AccountId, CardData.TrackData, CurrencyExchangeResult, CurrencyExchange.GetNationalCurrency())
              || !_cm_mov_table.Add(_operation_id, CASHIER_MOVEMENT.CURRENCY_EXCHANGE_AMOUNT_OUT, _net_amount, CardData.AccountId, CardData.TrackData)
              || !_cm_mov_table.Save(_db_trx.SqlTransaction))
          {
            Log.Message(string.Format("Error creating the cashier movements. AccountID [{0}], SessionID [{1}]", CardData.AccountId, CashierSession.CashierSessionId));
            return false;
          }

          // 5. Create Voucher
          _voucherData = new CurrencyExchangeVoucher(CardData.VoucherAccountInfo(), CASHIER_MOVEMENT.CURRENCY_EXCHANGE_TO_NATIONAL_CURRENCY, CurrencyExchangeResult);
          Voucher = new VoucherMultiCurrencyExchange(_voucherData, WSI.Common.PrintMode.Print, _db_trx.SqlTransaction);

          if (!Voucher.Save(_operation_id, _db_trx.SqlTransaction)
              || !VoucherBusinessLogic.VoucherSetHtml(Voucher.VoucherId, _operation_id, Voucher.VoucherHTML, _db_trx.SqlTransaction))
          {
            Log.Message(string.Format("Error creating the Voucher. AccountID [{0}], SessionID [{1}]", CardData.AccountId, CashierSession.CashierSessionId));
            return false;
          }

          // Commit 
          _db_trx.SqlTransaction.Commit();
          return true;
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Change national currency to  foreing currency. For currency devolution.
    //
    //  PARAMS :
    //      - INPUT :
    //          - CashierSession
    //          - CardData
    //          - CurrencyExchangeResult
    //
    //      - OUTPUT : 
    //          - Voucher 
    //
    // RETURNS :
    //      True if the operation was successfully
    //
    public Boolean ChangeNationalCurrencyToForeignCurrency(CashierSessionInfo CashierSession, CardData CardData,
                                                          CurrencyExchangeResult CurrencyExchangeResult, out VoucherMultiCurrencyExchange Voucher)
    {
      Int64 _operation_id;
      AccountMovementsTable _account_movement;
      CashierMovementsTable _cm_mov_table;
      CurrencyExchangeVoucher _voucherData;
      Decimal _net_amount;
      Decimal _net_amount_devolution;
      Decimal _in_amount;
      Decimal _current_foreign_balance;
      Decimal _amount_changed;
      Voucher = null;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _net_amount = CurrencyExchangeResult.NetAmount;
          _in_amount = CurrencyExchangeResult.InAmount;
          _net_amount_devolution = CurrencyExchangeResult.NetAmountDevolution;
          _amount_changed = _in_amount - _net_amount_devolution;

          // 1. Check if there is national currency in the Cashier
          _current_foreign_balance = GetCashCashierCurrentBalance(CurrencyExchangeResult.OutCurrencyCode);

          if (_net_amount > _current_foreign_balance)
          {
            Log.Error("There is not enough foreign money in the cash desk");
            return false;
          }

          // 2. Account Operation Id
          if (!Operations.DB_InsertOperation(OperationCode.CURRENCY_EXCHANGE_DEVOLUTION, CardData.AccountId, CashierSession.CashierSessionId,
                                             0, 0, _amount_changed, 0, 0, 0, string.Empty, out _operation_id, _db_trx.SqlTransaction))
          {
            Log.Message(string.Format("Error creating the OperationId. AccountID [{0}], SessionID [{1}]", CardData.AccountId, CashierSession.CashierSessionId));
            return false;
          }

          // 3. Insert the account movements 
          _account_movement = new AccountMovementsTable(CashierSession);

          if (!_account_movement.Add(_operation_id, CardData.AccountId, MovementType.CurrencyExchangeCashierOut, _amount_changed, _amount_changed, 0, 0)
              || !_account_movement.Add(_operation_id, CardData.AccountId, MovementType.CurrencyExchangeCashierIn, 0, 0, _amount_changed, _amount_changed)
              || !_account_movement.Save(_db_trx.SqlTransaction))
          {
            Log.Message(string.Format("Error creating the Account movements. AccountID [{0}], SessionID [{1}]", CardData.AccountId, CashierSession.CashierSessionId));
            return false;
          }

          // 4. Insert Cashier movements
          _cm_mov_table = new CashierMovementsTable(CashierSession);

          if (!_cm_mov_table.AddCurrencyExchange(_operation_id, CardData.AccountId, CardData.TrackData, CurrencyExchangeResult, CurrencyExchange.GetNationalCurrency())
              || !_cm_mov_table.Add(_operation_id, CASHIER_MOVEMENT.CURRENCY_EXCHANGE_AMOUNT_IN, CurrencyExchangeResult.InAmount, CardData.AccountId, CardData.TrackData)
              || !_cm_mov_table.Add(_operation_id, CASHIER_MOVEMENT.CURRENCY_EXCHANGE_AMOUNT_IN_CHANGE, _net_amount_devolution, CardData.AccountId, CardData.TrackData)
              || !_cm_mov_table.Save(_db_trx.SqlTransaction))
          {
            Log.Message(string.Format("Error creating the cashier movements. AccountID [{0}], SessionID [{1}]", CardData.AccountId, CashierSession.CashierSessionId));
            return false;
          }

          // 5. Create Voucher
          _voucherData = new CurrencyExchangeVoucher(CardData.VoucherAccountInfo(), CASHIER_MOVEMENT.CURRENCY_EXCHANGE_TO_FOREIGN_CURRENCY, CurrencyExchangeResult);
          Voucher = new VoucherMultiCurrencyExchange(_voucherData, WSI.Common.PrintMode.Print, _db_trx.SqlTransaction);

          if (!Voucher.Save(_operation_id, _db_trx.SqlTransaction)
              || !VoucherBusinessLogic.VoucherSetHtml(Voucher.VoucherId, _operation_id, Voucher.VoucherHTML, _db_trx.SqlTransaction))
          {
            Log.Message(string.Format("Error creating the Voucher. AccountID [{0}], SessionID [{1}]", CardData.AccountId, CashierSession.CashierSessionId));
            return false;
          }

          // Commit 
          _db_trx.SqlTransaction.Commit();
          return true;
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : It returns the cash amount in the cashier for the currency code parameter
    //
    //  PARAMS :
    //      - INPUT :
    //          - CurrencyCode
    //
    //      - OUTPUT : 
    //          - Voucher 
    //
    // RETURNS :
    //          - Decimal: Amount
    //
    public Decimal GetCashCashierCurrentBalance(String CurrencyCode)
    {
      Currency _cashier_current_balance = 0;
      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (CurrencyCode == CurrencyExchange.GetNationalCurrency())
            _cashier_current_balance = CashierBusinessLogic.UpdateSessionBalance(_db_trx.SqlTransaction, Cashier.SessionId, 0);
          else
            _cashier_current_balance = CashierBusinessLogic.UpdateSessionBalance(_db_trx.SqlTransaction, Cashier.SessionId, 0, CurrencyCode, CurrencyExchangeType.CURRENCY);
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return _cashier_current_balance;
    }

    public static Decimal GetEnabledCurrencies()
    {
      List<CurrencyExchangeType> _allowed_types;
      List<CurrencyExchange> _currencies_aux;
      CurrencyExchange _national_currency;
      Decimal _enabled_currencies;
      _enabled_currencies = 0;

      _allowed_types = new List<CurrencyExchangeType>();
      _allowed_types.Add(CurrencyExchangeType.CURRENCY);

      CurrencyExchange.GetAllowedCurrencies(true, _allowed_types, false, false, out _national_currency, out _currencies_aux);

      _enabled_currencies = _currencies_aux.Count - 1;

      return _enabled_currencies;
    }

    #endregion

    #region Private Methods

   

    #endregion

  }
}
