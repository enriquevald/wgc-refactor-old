namespace WSI.Cashier
{
  partial class frm_pwd_change
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_pwd_change));
      this.lbl_user = new WSI.Cashier.Controls.uc_label();
      this.lbl_username = new WSI.Cashier.Controls.uc_label();
      this.txt_old_password = new WSI.Cashier.Controls.uc_round_textbox();
      this.lbl_old_password = new WSI.Cashier.Controls.uc_label();
      this.btn_cancel = new WSI.Cashier.Controls.uc_round_button();
      this.btn_shutdown = new WSI.Cashier.Controls.uc_round_button();
      this.btn_keyboard = new WSI.Cashier.Controls.uc_round_button();
      this.btn_ok = new WSI.Cashier.Controls.uc_round_button();
      this.txt_confirm_password = new WSI.Cashier.Controls.uc_round_textbox();
      this.txt_new_password = new WSI.Cashier.Controls.uc_round_textbox();
      this.lbl_confirm_password = new WSI.Cashier.Controls.uc_label();
      this.lbl_new_password = new WSI.Cashier.Controls.uc_label();
      this.pnl_data.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.Controls.Add(this.lbl_user);
      this.pnl_data.Controls.Add(this.lbl_username);
      this.pnl_data.Controls.Add(this.txt_old_password);
      this.pnl_data.Controls.Add(this.lbl_old_password);
      this.pnl_data.Controls.Add(this.btn_cancel);
      this.pnl_data.Controls.Add(this.btn_shutdown);
      this.pnl_data.Controls.Add(this.btn_keyboard);
      this.pnl_data.Controls.Add(this.btn_ok);
      this.pnl_data.Controls.Add(this.txt_confirm_password);
      this.pnl_data.Controls.Add(this.txt_new_password);
      this.pnl_data.Controls.Add(this.lbl_confirm_password);
      this.pnl_data.Controls.Add(this.lbl_new_password);
      this.pnl_data.Size = new System.Drawing.Size(573, 289);
      this.pnl_data.TabIndex = 0;
      // 
      // lbl_user
      // 
      this.lbl_user.BackColor = System.Drawing.Color.Transparent;
      this.lbl_user.Font = new System.Drawing.Font("Montserrat", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_user.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_user.Location = new System.Drawing.Point(241, 18);
      this.lbl_user.Name = "lbl_user";
      this.lbl_user.Size = new System.Drawing.Size(272, 25);
      this.lbl_user.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_user.TabIndex = 1;
      this.lbl_user.Text = "xUSERNAME";
      this.lbl_user.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_username
      // 
      this.lbl_username.BackColor = System.Drawing.Color.Transparent;
      this.lbl_username.Font = new System.Drawing.Font("Montserrat", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_username.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_username.Location = new System.Drawing.Point(59, 18);
      this.lbl_username.Name = "lbl_username";
      this.lbl_username.Size = new System.Drawing.Size(180, 25);
      this.lbl_username.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_username.TabIndex = 0;
      this.lbl_username.Text = "xUSERNAME";
      this.lbl_username.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // txt_old_password
      // 
      this.txt_old_password.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_old_password.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_old_password.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_old_password.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_old_password.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_old_password.CornerRadius = 5;
      this.txt_old_password.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_old_password.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.txt_old_password.Location = new System.Drawing.Point(245, 53);
      this.txt_old_password.MaxLength = 15;
      this.txt_old_password.Multiline = false;
      this.txt_old_password.Name = "txt_old_password";
      this.txt_old_password.PasswordChar = '*';
      this.txt_old_password.ReadOnly = false;
      this.txt_old_password.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_old_password.SelectedText = "";
      this.txt_old_password.SelectionLength = 0;
      this.txt_old_password.SelectionStart = 0;
      this.txt_old_password.Size = new System.Drawing.Size(268, 40);
      this.txt_old_password.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_old_password.TabIndex = 3;
      this.txt_old_password.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_old_password.UseSystemPasswordChar = false;
      this.txt_old_password.WaterMark = null;
      this.txt_old_password.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_old_password_KeyPress);
      // 
      // lbl_old_password
      // 
      this.lbl_old_password.BackColor = System.Drawing.Color.Transparent;
      this.lbl_old_password.Font = new System.Drawing.Font("Montserrat", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_old_password.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_old_password.Location = new System.Drawing.Point(59, 60);
      this.lbl_old_password.Name = "lbl_old_password";
      this.lbl_old_password.Size = new System.Drawing.Size(180, 25);
      this.lbl_old_password.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_old_password.TabIndex = 2;
      this.lbl_old_password.Text = "xOLDPASS";
      this.lbl_old_password.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // btn_cancel
      // 
      this.btn_cancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_cancel.FlatAppearance.BorderSize = 0;
      this.btn_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_cancel.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_cancel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_cancel.Image = null;
      this.btn_cancel.Location = new System.Drawing.Point(233, 212);
      this.btn_cancel.Name = "btn_cancel";
      this.btn_cancel.Size = new System.Drawing.Size(155, 60);
      this.btn_cancel.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_cancel.TabIndex = 10;
      this.btn_cancel.Text = "XCANCEL";
      this.btn_cancel.UseVisualStyleBackColor = false;
      this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
      // 
      // btn_shutdown
      // 
      this.btn_shutdown.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(47)))), ((int)(((byte)(50)))));
      this.btn_shutdown.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
      this.btn_shutdown.FlatAppearance.BorderSize = 0;
      this.btn_shutdown.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_shutdown.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_shutdown.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_shutdown.Image = ((System.Drawing.Image)(resources.GetObject("btn_shutdown.Image")));
      this.btn_shutdown.Location = new System.Drawing.Point(15, 212);
      this.btn_shutdown.Name = "btn_shutdown";
      this.btn_shutdown.Size = new System.Drawing.Size(80, 60);
      this.btn_shutdown.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.TERTIARY;
      this.btn_shutdown.TabIndex = 8;
      this.btn_shutdown.UseVisualStyleBackColor = false;
      this.btn_shutdown.Click += new System.EventHandler(this.btn_shutdown_Click);
      // 
      // btn_keyboard
      // 
      this.btn_keyboard.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(47)))), ((int)(((byte)(50)))));
      this.btn_keyboard.FlatAppearance.BorderSize = 0;
      this.btn_keyboard.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_keyboard.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_keyboard.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_keyboard.Image = ((System.Drawing.Image)(resources.GetObject("btn_keyboard.Image")));
      this.btn_keyboard.Location = new System.Drawing.Point(108, 212);
      this.btn_keyboard.Name = "btn_keyboard";
      this.btn_keyboard.Size = new System.Drawing.Size(80, 60);
      this.btn_keyboard.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.TERTIARY;
      this.btn_keyboard.TabIndex = 9;
      this.btn_keyboard.UseVisualStyleBackColor = false;
      this.btn_keyboard.Click += new System.EventHandler(this.btn_keys_Click);
      // 
      // btn_ok
      // 
      this.btn_ok.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_ok.FlatAppearance.BorderSize = 0;
      this.btn_ok.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_ok.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_ok.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_ok.Image = null;
      this.btn_ok.Location = new System.Drawing.Point(402, 212);
      this.btn_ok.Name = "btn_ok";
      this.btn_ok.Size = new System.Drawing.Size(155, 60);
      this.btn_ok.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_ok.TabIndex = 11;
      this.btn_ok.Text = "XOK";
      this.btn_ok.UseVisualStyleBackColor = false;
      this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
      // 
      // txt_confirm_password
      // 
      this.txt_confirm_password.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_confirm_password.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_confirm_password.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_confirm_password.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_confirm_password.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_confirm_password.CornerRadius = 5;
      this.txt_confirm_password.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_confirm_password.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.txt_confirm_password.Location = new System.Drawing.Point(245, 149);
      this.txt_confirm_password.MaxLength = 15;
      this.txt_confirm_password.Multiline = false;
      this.txt_confirm_password.Name = "txt_confirm_password";
      this.txt_confirm_password.PasswordChar = '*';
      this.txt_confirm_password.ReadOnly = false;
      this.txt_confirm_password.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_confirm_password.SelectedText = "";
      this.txt_confirm_password.SelectionLength = 0;
      this.txt_confirm_password.SelectionStart = 0;
      this.txt_confirm_password.Size = new System.Drawing.Size(268, 40);
      this.txt_confirm_password.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_confirm_password.TabIndex = 7;
      this.txt_confirm_password.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_confirm_password.UseSystemPasswordChar = false;
      this.txt_confirm_password.WaterMark = null;
      this.txt_confirm_password.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_confirm_password_KeyPress);
      // 
      // txt_new_password
      // 
      this.txt_new_password.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_new_password.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_new_password.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_new_password.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_new_password.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_new_password.CornerRadius = 5;
      this.txt_new_password.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_new_password.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.txt_new_password.Location = new System.Drawing.Point(245, 101);
      this.txt_new_password.MaxLength = 15;
      this.txt_new_password.Multiline = false;
      this.txt_new_password.Name = "txt_new_password";
      this.txt_new_password.PasswordChar = '*';
      this.txt_new_password.ReadOnly = false;
      this.txt_new_password.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_new_password.SelectedText = "";
      this.txt_new_password.SelectionLength = 0;
      this.txt_new_password.SelectionStart = 0;
      this.txt_new_password.Size = new System.Drawing.Size(268, 40);
      this.txt_new_password.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_new_password.TabIndex = 5;
      this.txt_new_password.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_new_password.UseSystemPasswordChar = false;
      this.txt_new_password.WaterMark = null;
      this.txt_new_password.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_new_password_KeyPress);
      // 
      // lbl_confirm_password
      // 
      this.lbl_confirm_password.BackColor = System.Drawing.Color.Transparent;
      this.lbl_confirm_password.Font = new System.Drawing.Font("Montserrat", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_confirm_password.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_confirm_password.Location = new System.Drawing.Point(58, 156);
      this.lbl_confirm_password.Name = "lbl_confirm_password";
      this.lbl_confirm_password.Size = new System.Drawing.Size(181, 25);
      this.lbl_confirm_password.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_confirm_password.TabIndex = 6;
      this.lbl_confirm_password.Text = "xCONFIRMPASS";
      this.lbl_confirm_password.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_new_password
      // 
      this.lbl_new_password.BackColor = System.Drawing.Color.Transparent;
      this.lbl_new_password.Font = new System.Drawing.Font("Montserrat", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_new_password.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_new_password.Location = new System.Drawing.Point(53, 108);
      this.lbl_new_password.Name = "lbl_new_password";
      this.lbl_new_password.Size = new System.Drawing.Size(186, 25);
      this.lbl_new_password.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_new_password.TabIndex = 4;
      this.lbl_new_password.Text = "xNEWPASS";
      this.lbl_new_password.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // frm_pwd_change
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
      this.ClientSize = new System.Drawing.Size(573, 344);
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "frm_pwd_change";
      this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
      this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
      this.Style = WSI.Cashier.Controls.frm_base.BaseFormStyle.PRINCIPAL;
      this.Text = "Change Password";
      this.pnl_data.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion


    private WSI.Cashier.Controls.uc_round_textbox txt_new_password;
    private WSI.Cashier.Controls.uc_label lbl_confirm_password;
    private WSI.Cashier.Controls.uc_label lbl_new_password;
    private WSI.Cashier.Controls.uc_round_textbox txt_confirm_password;
    private WSI.Cashier.Controls.uc_round_button btn_ok;
    private WSI.Cashier.Controls.uc_round_button btn_keyboard;
    private WSI.Cashier.Controls.uc_round_button btn_shutdown;
    private WSI.Cashier.Controls.uc_round_button btn_cancel;
    private WSI.Cashier.Controls.uc_round_textbox txt_old_password;
    private WSI.Cashier.Controls.uc_label lbl_old_password;
    private WSI.Cashier.Controls.uc_label lbl_username;
    private WSI.Cashier.Controls.uc_label lbl_user;


  }
}