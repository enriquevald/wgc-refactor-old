﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Uc_Adress.cs
// 
//   DESCRIPTION: 
//
//        AUTHOR: .
// 
// CREATION DATE: 02-DEC-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// //-//-////  ---    First release.
// 28-NOV-2016 FJC    Fixed Bug 21043:Cashier: Error al crear cuenta y guardar datos de contacto
// 26-JUL-2017 RGR    Fixed Bug 28998:WIGOS-3606 The cashier ask to the user to save modifications when the user didn't any modification 
// 13-FEB-2018 EOR    Bug 31515: WIGOS-8032 [Ticket #12229] Formulario de inscripción de jugadores en registro por límite de pago 
// 27-JUN-2018 DLM    Bug 33349:WIGOS -13084 : Accounts - Country list should be displayed in order even there is a country selected.
//-------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using WSI.Cashier.Controls;
using WSI.Cashier.MVC.Controller.PlayerEditDetails.Common;
using WSI.Common;
using WSI.Common.Entities.General;

namespace WSI.Cashier
{
  /// <summary>
  ///   Address Edit Control
  /// </summary>
  public partial class uc_address : UserControl
  {
    private readonly Color m_color_error = Color.Red;
    private readonly Color m_color_normal = Color.Transparent;
    private int m_country_id = -999;

    private Zip.Address m_address_modified;

    /// <summary>
    ///   create instance of address edit control
    /// </summary>
    public uc_address()
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[UC LOAD] uc_address", Log.Type.Message);

      InitializeComponent();
      try
      {
        m_color_normal = lbl_address_01.ForeColor;
        DataTable _dt_countries;

        DataRow _dr_empty_row;
        pnl_auto_address.Top = 0;
        pnl_auto_address.Left = 0;
        pnl_manual_address.Top = 0;
        pnl_manual_address.Left = 0;
        chk_address_mode.BringToFront();
        lbl_address_validation.Text = "";
        lbl_address_validation.BringToFront();
        _dt_countries = CardData.GetCountriesList();

        // Non-mandatory field.  An empty row is needed.
        _dr_empty_row = _dt_countries.NewRow();
        _dr_empty_row["CO_COUNTRY_ID"] = -1;
        _dr_empty_row["CO_NAME"] = "";

        _dt_countries.Rows.InsertAt(_dr_empty_row, 0);


        var _country_list = _dt_countries.ToSelectableOptionList<Country>();
        BindDatasourceToCombo(_country_list, cb_country_auto);
        BindDatasourceToCombo(_country_list, cb_country);

        var _fed_entity_lst = new List<State>() {new State(){Id = -1,Name = ""}};

        BindDatasourceToCombo(_fed_entity_lst, cb_fed_entity);
        BindDatasourceToCombo(_fed_entity_lst, cb_fed_entity_auto);
        VisibleFields.HideControls();

        if (txt_address_01.Enabled && txt_ext_num.Enabled && txt_address_02.Enabled && txt_address_03.Enabled &&
            txt_city.Enabled && txt_zip.Enabled && cb_fed_entity.Enabled && cb_country.Enabled)
        {
          return;
        }
        chk_address_mode.Visible = false;
        chk_address_mode.Enabled = false;
      }
      catch (Exception)
      {
        // ignored
      }
    } //uc_address


    public VisibleData VisibleFields
    {
      get
      {
        {
          VisibleData _visible_data;
          _visible_data = new VisibleData();

          _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.STREET, new Control[] {lbl_address_01, txt_address_01});
          _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.EXTERNAL_NUMBER, new Control[] {lbl_ext_num, txt_ext_num});
          _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.COLONY, new Control[] {lbl_address_02, txt_address_02});
          _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.DELEGATION, new Control[] {lbl_address_03, txt_address_03});
          _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.CITY, new Control[] {lbl_city, txt_city});
          _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.ZIP, new Control[] {lbl_zip, txt_zip});
          _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.FEDERAL_ENTITY, new Control[] {lbl_fed_entity, cb_fed_entity});
          _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.ADDRESS_COUNTRY, new Control[] {lbl_country, cb_country});
          return _visible_data;
        }
      }
    } //VisibleFields

    public void SelectFirstControl()
    {
      ActiveControl = txt_address_01;
    }

    /// <summary>
    ///   house number property
    /// </summary>
    public string HouseNumber
    {
      get { return chk_address_mode.Checked ? txt_ext_num.Text : txt_ext_num_auto.Text; }
      set
      {
        txt_ext_num.Text = value;
        txt_ext_num_auto.Text = value;
      }
    } //HouseNumber

    /// <summary>
    ///   Address line 1 property
    /// </summary>
    public string AddressLine1
    {
      get { return chk_address_mode.Checked ? txt_address_01.Text : txt_address_01_auto.Text; }
      set
      {
        txt_address_01.Text = value;
        txt_address_01_auto.Text = value;
      }
    } //AddressLine1

    /// <summary>
    ///   Address line 2 property
    /// </summary>
    public string AddressLine2
    {
      get { return chk_address_mode.Checked ? txt_address_02.Text : cb_address_02_auto.Text; }
      set { txt_address_02.Text = value; }
    } //AddressLine2

    /// <summary>
    ///   Address line 3 property
    /// </summary>
    public string AddressLine3
    {
      get { return chk_address_mode.Checked ? txt_address_03.Text : cb_address_03_auto.Text; }
      set { txt_address_03.Text = value; }
    } //AddressLine3

    /// <summary>
    ///   Town property
    /// </summary>
    public string Town
    {
      get { return chk_address_mode.Checked ? txt_city.Text : cb_city_auto.Text; }
      set { txt_city.Text = value; }
    } //Town

    /// <summary>
    ///   State property
    /// </summary>
    public ISelectableOption State
    {
      get
      {
        return cb_fed_entity.Enabled
          ? (chk_address_mode.Checked
          ? (ISelectableOption) cb_fed_entity.SelectedItem
            : (ISelectableOption) cb_fed_entity_auto.SelectedItem)
          : new State() {Id = 0, Name = ""};
      }
      set
      {
        try
        {
          cb_fed_entity.SelectedItem = GetSelectableOption(cb_fed_entity.DataSource as List<State>,
            value == null ? -1 : value.Id, "");
        }
        catch (Exception)
        {
          // ignored
        }
      }
    } //State

    /// <summary>
    ///   Country property
    /// </summary>
    public ISelectableOption Country
    {
      get
      {
        return cb_country.Enabled
          ? (chk_address_mode.Checked
          ? (ISelectableOption) cb_country.SelectedItem
            : (ISelectableOption) cb_country_auto.SelectedItem)
          : new Country() {Id = -1, Name = ""};
      }
      set
      {
        try
        {
          cb_country.SelectedItem = GetSelectableOption(cb_country.DataSource as List<Country>,
            value == null ? -1 : value.Id, "");
        }
        catch (Exception)
        {
          // ignored
        }
      }
    } //Country

    /// <summary>
    ///   Postcode property
    /// </summary>
    public string PostCode
    {
      get { return chk_address_mode.Checked ? txt_zip.Text : txt_zip_auto.Text; }
      set
      {
        txt_zip.Text = value;
        txt_zip_auto.Text = value;
      }
    } //PostCode

    /// <summary>
    ///   Automatic address property
    /// </summary>
    public bool Automatic
    {
      get { return chk_address_mode.Checked; }
      set { chk_address_mode.Checked = value; }
    } //Automatic

    /// <summary>
    ///   routine to set tags of labels and controls to
    ///   facilitate highlighting and focusing of error controls
    /// </summary>
    public new object Tag
    {
      get { return base.Tag; }
      set
      {
        base.Tag = value;
        lbl_address_01.Tag = "l" + base.Tag + lbl_address_01.Tag;
        lbl_address_02.Tag = "l" + base.Tag + lbl_address_02.Tag;
        lbl_address_03.Tag = "l" + base.Tag + lbl_address_03.Tag;
        lbl_city.Tag = "l" + base.Tag + lbl_city.Tag;
        lbl_zip.Tag = "l" + base.Tag + lbl_zip.Tag;
        lbl_ext_num.Tag = "l" + base.Tag + lbl_ext_num.Tag;
        lbl_country.Tag = "l" + base.Tag + lbl_country.Tag;
        lbl_fed_entity.Tag = "l" + base.Tag + lbl_fed_entity.Tag;

        lbl_num_ext_auto.Tag = lbl_ext_num.Tag;
        lbl_address_01_auto.Tag = lbl_address_01.Tag;
        lbl_address_02_auto.Tag = lbl_address_02.Tag;
        lbl_address_03_auto.Tag = lbl_address_03.Tag;
        lbl_city_auto.Tag = lbl_city.Tag;
        lbl_zip_auto.Tag = lbl_zip.Tag;
        lbl_country_auto.Tag = lbl_country.Tag;
        lbl_fed_entity_auto.Tag = lbl_fed_entity.Tag;
        chk_address_mode.Tag = "l" + base.Tag + chk_address_mode.Tag;

        txt_ext_num.Tag = "" + base.Tag + txt_ext_num.Tag;
        txt_address_01.Tag = "" + base.Tag + txt_address_01.Tag;
        txt_address_02.Tag = "" + base.Tag + txt_address_02.Tag;
        txt_address_03.Tag = "" + base.Tag + txt_address_03.Tag;
        txt_city.Tag = "" + base.Tag + txt_city.Tag;
        cb_country.Tag = "" + base.Tag + cb_country.Tag;
        cb_fed_entity.Tag = "" + base.Tag + cb_fed_entity.Tag;
        txt_zip.Tag = "" + base.Tag + txt_zip.Tag;

        txt_ext_num_auto.Tag = txt_ext_num.Tag;
        txt_address_01_auto.Tag = txt_address_01.Tag;
        cb_address_02_auto.Tag = txt_address_02.Tag;
        cb_address_03_auto.Tag = txt_address_03.Tag;
        cb_city_auto.Tag = txt_city.Tag;
        cb_country_auto.Tag = cb_country.Tag;
        cb_fed_entity_auto.Tag = cb_fed_entity.Tag;
        txt_zip_auto.Tag = txt_zip.Tag;
      }
    } //Tag

    /// <summary>
    /// routine to set the value of the combo taking into consideration the general parameters
    /// </summary>    
    public void SetComboCountryValue<TU>(TU setValue, string subjectGP = null) where TU : ISelectableOption
    {
      cb_country.Set<TU>(setValue, subjectGP);
    }

    /// <summary>
    /// routine to set the value of the combo taking into consideration the general parameters
    /// </summary>   
    public void SetComboFederalEntityValue<TU>(TU setValue, string subjectGP = null) where TU : ISelectableOption
    {
      cb_fed_entity.Set<TU>(setValue, subjectGP);
    }

    /// <summary>
    /// Remove cb_country_event (in this case SelectIndexChanged)
    /// </summary>
    public void RemoveEventCboCountry()
    {
      cb_country_auto.SelectedIndexChanged -= CbCountryAutoChanged;
    } // RemoveEventCboCountry

    /// <summary>
    /// Add cb_country_event (in this case SelectIndexChanged)
    /// </summary>
    public void AddEventCboCountry()
    {
      cb_country_auto.SelectedIndexChanged += CbCountryAutoChanged;
    } // AddEventCboCountry

    /// <summary>
    ///   Create a types datasource from a given list
    /// </summary>
    /// <typeparam name="TU"></typeparam>
    /// <param name="List"></param>
    /// <returns></returns>
    private List<TU> DatasourceFromList<TU>(List<TU> List) where TU : ISelectableOption
    {
      List<TU> _new_ds = new List<TU>();
      if (List == null)
      {
        return _new_ds;
      }
      foreach (var _selectable_option in List)
      {
        _new_ds.Add(_selectable_option);
      }
      return _new_ds;
    } //DatasourceFromList

    /// <summary>
    ///   bind a list to a combo box
    /// </summary>
    /// <typeparam name="TU"></typeparam>
    /// <param name="List"></param>
    /// <param name="Combo"></param>
    private void BindDatasourceToCombo<TU>(List<TU> List, uc_round_auto_combobox Combo) where TU : ISelectableOption
    {
      Combo.DataSource = DatasourceFromList(List);
      Combo.ValueMember = "Id";
      Combo.DisplayMember = "Name";
      Combo.AllowUnlistedValues = false;
      Combo.FormatValidator = new CharacterTextBox();
    } //BindDatasourceToCombo

    /// <summary>
    ///   Set datasource for combo and select item -1
    /// </summary>
    /// <param name="Combo"></param>
    /// <param name="List"></param>
    private void SetDataSourceAndAssignSupplemental(uc_round_auto_combobox Combo, List<ISelectableOption> List)
    {
      Combo.DataSource = List;
      Combo.ValueMember = "Id";
      Combo.DisplayMember = "Name";
      Combo.AllowUnlistedValues = false;
      Combo.FormatValidator = new CharacterTextBox();
      SetSelectedComboItem(Combo, -1);
    } //SetDataSourceAndAssignSupplemental

    /// <summary>
    ///   react to change of form mode, automatic to manual, manual to automatic
    /// </summary>
    /// <param name="Sender"></param>
    /// <param name="E"></param>
    private void chk_address_mode_CheckedChanged(object Sender, EventArgs E)
    {
      if (!chk_address_mode.Checked)
      {
        m_address_modified = FillStructAddress();
        cb_country_auto.Enabled = true;
        txt_zip_auto.Enabled = true;
        cb_fed_entity_auto.Enabled = false;
        cb_city_auto.Enabled = false;
        cb_address_03_auto.Enabled = false;
        cb_address_02_auto.Enabled = false;
        txt_address_01_auto.Enabled = true;
        txt_ext_num_auto.Enabled = true;

        SetDataSourceAndAssignSupplemental(cb_address_02_auto,
          new List<ISelectableOption> {new Colony {Id = -1, Name = m_address_modified.Colony}});
        SetDataSourceAndAssignSupplemental(cb_address_03_auto,
          new List<ISelectableOption> {new Delegation {Id = -1, Name = m_address_modified.Delegation}});
        SetDataSourceAndAssignSupplemental(cb_city_auto,
          new List<ISelectableOption> {new City {Id = -1, Name = m_address_modified.City}});
        SetDataSourceAndAssignSupplemental(cb_fed_entity_auto,
          new List<ISelectableOption> {new State {Id = -1, Name = m_address_modified.FederalEntity}});
        txt_address_01_auto.Text = txt_address_01.Text;
        txt_ext_num_auto.Text = txt_ext_num.Text;

        lbl_address_validation.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MANUAL_ENTRY_VALIDATION");

        txt_zip_auto.Text = txt_zip.Text;
        DoAddressAutoSelectPart1();
        cb_country_auto.SelectedIndexChanged += CbCountryAutoChanged;
        txt_zip_auto.TextChanged += TxtZipAutoChanged;
      }
      else
      {
        cb_country_auto.SelectedIndexChanged -= CbCountryAutoChanged;
        txt_zip_auto.TextChanged -= TxtZipAutoChanged;
        CopyAutoAddressToManual();
      }

      pnl_manual_address.Visible = chk_address_mode.Checked;
      pnl_auto_address.Visible = !chk_address_mode.Checked;
    } //chk_address_mode_CheckedChanged

    /// <summary>
    ///   replicate automatic address in manual address
    /// </summary>
    private void CopyAutoAddressToManual()
    {
      cb_country.SelectedItem = GetSelectableOption(cb_country.DataSource as List<Country>,
        ((ISelectableOption) cb_country_auto.SelectedItem).Id, "");
      txt_zip.Text = txt_zip_auto.Text;
      if (cb_fed_entity_auto.SelectedItem != null)
      {
        cb_fed_entity.SelectedItem = GetSelectableOption(cb_fed_entity.DataSource as List<State>,
          ((ISelectableOption) cb_fed_entity_auto.SelectedItem).Name);
      }
      else
      {
        cb_fed_entity.SelectedIndex = -1;
      }

      txt_city.Text = cb_city_auto.SelectedItem != null ? ((ISelectableOption) cb_city_auto.SelectedItem).Name : "";
      txt_address_03.Text = cb_address_03_auto.SelectedItem != null
        ? ((ISelectableOption) cb_address_03_auto.SelectedItem).Name
        : "";
      txt_address_02.Text = cb_address_02_auto.SelectedItem != null
        ? ((ISelectableOption) cb_address_02_auto.SelectedItem).Name
        : "";


      txt_address_01.Text = txt_address_01_auto.Text;
      txt_ext_num.Text = txt_ext_num_auto.Text;
      lbl_address_validation.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MANUAL_ENTRY");
    } //CopyAutoAddressToManual

    /// <summary>
    ///   get the select item that matches the id
    /// </summary>
    /// <typeparam name="TU"></typeparam>
    /// <param name="List"></param>
    /// <param name="ItemId"></param>
    /// <param name="BlankValue"></param>
    /// <returns></returns>
    private TU GetSelectableOption<TU>(List<TU> List, int ItemId, string BlankValue) where TU : ISelectableOption, new()
    {
      TU _option;
      _option = new TU {Id = ItemId, Name = BlankValue};
      if (ItemId > 0)
      {
        foreach (TU _opt in List)
        {
          if (_opt.Id == ItemId)
          {
            _option = _opt;
            break;
          }
        }
      }
      else
      {
        _option = List[0];
      }
      return _option;
    } //GetSelectableOption

    /// <summary>
    ///   get the select item that matches the name
    /// </summary>
    /// <typeparam name="TU"></typeparam>
    /// <param name="List"></param>
    /// <param name="ItemName"></param>
    /// <returns></returns>
    private TU GetSelectableOption<TU>(List<TU> List, string ItemName) where TU : ISelectableOption, new()
    {
      TU _option;
      _option = new TU {Id = -1, Name = ItemName};
      foreach (TU _opt in List)
      {
        if (_opt.Name.Equals(ItemName))
        {
          _option = _opt;
          break;
        }
      }
      return _option;
    } //GetSelectableOption

    /// <summary>
    ///   Fill an address object with data from form
    /// </summary>
    /// <returns></returns>
    private Zip.Address FillStructAddress()
    {
      Zip.Address _retval = new Zip.Address
        {
          Street = txt_address_01.Text,
          ExtNum = txt_ext_num.Text,
          Zip = txt_zip.Text,
          Colony = txt_address_02.Text,
          Delegation = txt_address_03.Text,
          City = txt_city.Text,
          CountryId = Convert.ToInt32(cb_country.SelectedValue),
          FederalEntity = cb_fed_entity.Text
        };
      return _retval;
    } //FillStructAddress

    /// <summary>
    ///   get the address case for the calling control
    /// </summary>
    /// <param name="Ctl"></param>
    /// <returns></returns>
    private string GetCaseForControl(Control Ctl)
    {
      switch (Ctl.Name)
      {
        case "cb_fed_entity_auto":
          return Zip.CASE_FEDERAL_ENTITY;
        case "cb_city_auto":
          return Zip.CASE_CITY;
        case "cb_address_03_auto":
          return Zip.CASE_DELEGATION;
        case "cb_address_02_auto":
          return Zip.CASE_COLONY;
        default:
          return "";
      }
    } //GetCaseForControl

    /// <summary>
    ///   set the selected item in the combo by id
    /// </summary>
    /// <param name="Combo"></param>
    /// <param name="Id"></param>
    private void SetSelectedComboItem(uc_round_auto_combobox Combo, int Id)
    {
      foreach (var _item in Combo.Items)
      {
        if (((ISelectableOption) _item).Id != Id)
        {
          continue;
        }
        Combo.SelectedItem = _item;
        break;
      }
    } //SetSelectedComboItem

    /// <summary>
    ///   get the address object for automatic address validation
    ///   with only the fields confirmed so far
    /// </summary>
    /// <param name="Phase"></param>
    /// <returns></returns>
    private Zip.Address GetAddress(int Phase)
    {
      var _addr = new Zip.Address();
      if (Phase == 0)
      {
        return _addr;
      }

      _addr.CountryId = cb_country_auto.SelectedItem != null
        ? ((ISelectableOption) cb_country_auto.SelectedItem).Id
        : -1;
      if (Phase == 1)
      {
        return _addr;
      }

      _addr.Zip = txt_zip_auto.Text;
      if (Phase == 2)
      {
        return _addr;
      }

      _addr.FederalEntity = cb_fed_entity_auto.SelectedItem != null
        ? ((ISelectableOption) cb_fed_entity_auto.SelectedItem).Name
        : "";
      if (Phase == 3)
      {
        return _addr;
      }

      _addr.City = cb_city_auto.SelectedItem != null ? ((ISelectableOption) cb_city_auto.SelectedItem).Name : "";
      if (Phase == 4)
      {
        return _addr;
      }

      _addr.Delegation = cb_address_03_auto.SelectedItem != null
        ? ((ISelectableOption) cb_address_03_auto.SelectedItem).Name
        : "";
      if (Phase == 5)
      {
        return _addr;
      }

      _addr.Colony = cb_address_02_auto.SelectedItem != null
        ? ((ISelectableOption) cb_address_02_auto.SelectedItem).Name
        : "";
      return _addr;
    } //GetAddress

    /// <summary>
    ///   perform the first part of the automatic address routine
    /// </summary>
    private void DoAddressAutoSelectPart1()
    {
      SetSelectedComboItem(cb_country_auto, m_address_modified.CountryId);
      if (cb_country_auto.SelectedItem == null || ((ISelectableOption) cb_country_auto.SelectedItem).Id < 1)
      {
        cb_country_auto.Enabled = true;
        cb_country_auto.Focus();
      }
      else
      {
        SetSelectedComboItem(cb_country_auto, m_address_modified.CountryId);
        DoAddressAutoSelectPart2();
      }
    } //DoAddressAutoSelectPart1

    /// <summary>
    ///   perform the second part of the automatic address routine
    /// </summary>
    private void DoAddressAutoSelectPart2()
    {
      TxtZipAutoChanged(txt_zip_auto, null);
    } //DoAddressAutoSelectPart2

    /// <summary>
    ///   perform subsequent parts of the address routine
    /// </summary>
    /// <param name="Control"></param>
    private void DoAddressAutoSelectPart(Control Control)
    {
      if (!(Control is uc_round_auto_combobox))
      {
        return;
      }
      DataTable _dt;

      var _ctrl = (uc_round_auto_combobox) Control;
      {
        Console.WriteLine(_ctrl.Name);
        var _addr = GetAddress(GetPhaseForControl(_ctrl));
        _dt = Zip.GetDataTableToAddress(GetCaseForControl(_ctrl), Zip.GetFilterToAddress(_addr, false));
        _ctrl.DataSource = null;
        var _supval = GetManualAddresValueForControl(_ctrl);
        _ctrl.DataSource = GetList(_dt, _ctrl, _supval);
        _ctrl.ValueMember = "Id";
        _ctrl.DisplayMember = "Name";
        _ctrl.AllowUnlistedValues = false;
        _ctrl.FormatValidator = new CharacterTextBox();

        if (_ctrl.Items.Count > 1)
        {
          _ctrl.Enabled = true;
          _ctrl.SelectedIndexChanged += _ctrl_SelectedIndexChanged;
          _ctrl.IsDroppedDown = true;
          _ctrl.Focus();
        }
        else
        {
          if (_ctrl.Items.Count == 0)
          {
            PaintThisAndNextControls(_ctrl, m_color_error);
            return;
          }
          GetLabelForControl(_ctrl).ForeColor = m_color_normal;
          _ctrl.SelectedIndex = 0;
          _ctrl.Enabled = false;
          DoAddressAutoSelectPart(GetNextControl(_ctrl));
        }
      }
      CopyAutoAddressToManual();
    } //DoAddressAutoSelectPart

    /// <summary>
    ///   get the address part phase for the calling control
    /// </summary>
    /// <param name="Ctrl"></param>
    /// <returns></returns>
    private int GetPhaseForControl(Control Ctrl)
    {
      switch (Ctrl.Name)
      {
        case "txt_address_01_auto":
          return 7;
        case "txt_ext_num_auto":
          return 6;
        case "cb_address_02_auto":
          return 5;
        case "cb_address_03_auto":
          return 4;
        case "cb_city_auto":
          return 3;
        case "cb_fed_entity_auto":
          return 2;
        case "txt_zip_auto":
          return 1;
        case "cb_country_auto":
          return 0;
        default:
          return 8;
      }
    } //GetPhaseForControl

    /// <summary>
    ///   get the manual address value for the automatic calling control
    /// </summary>
    /// <param name="Ctrl"></param>
    /// <returns></returns>
    private string GetManualAddresValueForControl(Control Ctrl)
    {
      switch (Ctrl.Name)
      {
        case "txt_address_01_auto":
          return m_address_modified.Street;
        case "txt_ext_num_auto":
          return m_address_modified.ExtNum;
        case "cb_address_02_auto":
          return m_address_modified.Colony;
        case "cb_address_03_auto":
          return m_address_modified.Delegation;
        case "cb_city_auto":
          return m_address_modified.City;
        case "cb_fed_entity_auto":
          return m_address_modified.FederalEntity;
        case "txt_zip_auto":
          return m_address_modified.Zip;
        case "cb_country_auto":
          return GetSelectableOption(cb_country_auto.DataSource as List<Country>, m_address_modified.CountryId, "").Name;
        default:
          return "";
      }
    } //GetManualAddresValueForControl

    /// <summary>
    ///   paint this and the following controls the specified colour
    /// </summary>
    /// <param name="Ctrl"></param>
    /// <param name="Colour"></param>
    private void PaintThisAndNextControls(Control Ctrl, Color Colour)
    {
      GetLabelForControl(Ctrl).ForeColor = Colour;
      Control _next_control = GetNextControl(Ctrl);
      while (_next_control != null)
      {
        GetLabelForControl(_next_control).ForeColor = Colour;
        _next_control = GetNextControl(_next_control);
      }
    } //PaintThisAndNextControls

    /// <summary>
    ///   get the label that represents the control
    /// </summary>
    /// <param name="Ctrl"></param>
    /// <returns></returns>
    private Label GetLabelForControl(Control Ctrl)
    {
      switch (Ctrl.Name)
      {
        case "txt_address_01_auto":
          return lbl_address_01_auto;
        case "txt_ext_num_auto":
          return lbl_num_ext_auto;
        case "cb_address_02_auto":
          return lbl_address_02_auto;
        case "cb_address_03_auto":
          return lbl_address_03_auto;
        case "cb_city_auto":
          return lbl_city_auto;
        case "cb_fed_entity_auto":
          return lbl_fed_entity_auto;
        case "cb_country_auto":
          return lbl_country_auto;
        case "txt_zip_auto":
          return lbl_zip_auto;
        default:
          return null;
      }
    } //GetLabelForControl

    /// <summary>
    ///   event that is attached to the current address part control
    /// </summary>
    /// <param name="Sender"></param>
    /// <param name="E"></param>
    private void _ctrl_SelectedIndexChanged(object Sender, EventArgs E)
    {
      if (((ISelectableOption) ((uc_round_auto_combobox) Sender).SelectedItem).Id < 0)
      {
        PaintThisAndNextControls((Control) Sender, m_color_error);
        return;
      }
      ((uc_round_auto_combobox) Sender).SelectedIndexChanged -= _ctrl_SelectedIndexChanged;
      GetLabelForControl(((uc_round_auto_combobox) Sender)).ForeColor = m_color_normal;
      var _uc_round_auto_combobox = (uc_round_auto_combobox) Sender;
      if (_uc_round_auto_combobox != null)
      {
        _uc_round_auto_combobox.Enabled = false;
      }
      DoAddressAutoSelectPart(GetNextControl(((uc_round_auto_combobox) Sender)));
    } //_ctrl_SelectedIndexChanged

    /// <summary>
    ///   get the next control in the address routine
    /// </summary>
    /// <param name="Ctrl"></param>
    /// <returns></returns>
    private Control GetNextControl(Control Ctrl)
    {
      switch (Ctrl.Name)
      {
        case "cb_country_auto":
          return txt_zip_auto;
        case "txt_zip_auto":
          return cb_fed_entity_auto;
        case "cb_fed_entity_auto":
          return cb_city_auto;
        case "cb_city_auto":
          return cb_address_03_auto;
        case "cb_address_03_auto":
          return cb_address_02_auto;
        default:
          return null;
      }
    } //GetNextControl

    /// <summary>
    ///   get the list for the calling combo box
    /// </summary>
    /// <param name="Dt"></param>
    /// <param name="Ctrl"></param>
    /// <param name="SupVal"></param>
    /// <returns></returns>
    private object GetList(DataTable Dt, uc_round_auto_combobox Ctrl, string SupVal)
    {
      switch (Ctrl.Name)
      {
        case "cb_fed_entity_auto":
          return Dt.ToFederalEntityList(((ISelectableOption) cb_country_auto.SelectedItem).Id, SupVal);
        case "cb_city_auto":
          return Dt.ToSelectableOptionList<City>(SupVal);
        case "cb_address_03_auto":
          return Dt.ToSelectableOptionList<Delegation>(SupVal);
        case "cb_address_02_auto":
          return Dt.ToSelectableOptionList<Colony>(SupVal);
        case "cb_country_auto":
          return Dt.ToSelectableOptionList<Country>(SupVal);
        default:
          return "";
      }
    } //GetList

    /// <summary>
    ///   event fired on change of postcode
    /// </summary>
    /// <param name="Sender"></param>
    /// <param name="E"></param>
    private void TxtZipAutoChanged(object Sender, EventArgs E)
    {
      if (!Zip.ValidationZip(((ISelectableOption) cb_country_auto.SelectedItem).Id, txt_zip_auto.Text))
      {
        PaintThisAndNextControls(txt_zip_auto, m_color_error);
        txt_zip_auto.Focus();
        return;
      }

      DoAddressAutoSelectPart(cb_fed_entity_auto);
      if (cb_fed_entity_auto.Items.Count == 0 ||
          (((ISelectableOption) cb_fed_entity_auto.SelectedItem).Id < 0 && cb_fed_entity_auto.Items.Count == 1))
      {
        PaintThisAndNextControls(txt_zip_auto, m_color_error);
        txt_zip_auto.Focus();
        return;
      }
      lbl_zip_auto.ForeColor = m_color_normal;
      CopyAutoAddressToManual();
    } //TxtZipAutoChanged

    /// <summary>
    ///   event fired on change of auto routine country
    /// </summary>
    /// <param name="Sender"></param>
    /// <param name="E"></param>
    private void CbCountryAutoChanged(object Sender, EventArgs E)
    {
      PaintThisAndNextControls(txt_zip_auto, m_color_error);

      cb_country_auto.Enabled = true;
      txt_zip_auto.Enabled = true;
      cb_fed_entity_auto.Enabled = false;
      cb_city_auto.Enabled = false;
      cb_address_03_auto.Enabled = false;
      cb_address_02_auto.Enabled = false;
      txt_address_01_auto.Enabled = true;
      txt_ext_num_auto.Enabled = true;
      txt_zip_auto.Text = "";
      cb_fed_entity_auto.SelectedItem = null;
      cb_address_03_auto.SelectedItem = null;
      cb_address_02_auto.SelectedItem = null;
      cb_city_auto.SelectedItem = null;
      if (((ISelectableOption) cb_country_auto.SelectedItem).Id != m_address_modified.CountryId)
      {
        m_address_modified.Zip = "";
        m_address_modified.City = "";
        m_address_modified.Colony = "";
        m_address_modified.Delegation = "";
        m_address_modified.FederalEntity = "";
        cb_address_02_auto.DataSource = new List<ISelectableOption>();
        cb_address_02_auto.SelectedItem = null;
        cb_address_02_auto.Text = null;
        cb_address_03_auto.DataSource = new List<ISelectableOption>();
        cb_address_03_auto.SelectedItem = null;
        cb_address_03_auto.Text = null;
        cb_city_auto.DataSource = new List<ISelectableOption>();
        cb_city_auto.SelectedItem = null;
        cb_city_auto.Text = null;
        cb_fed_entity_auto.DataSource = new List<ISelectableOption>();
        cb_fed_entity_auto.SelectedItem = null;
        cb_fed_entity_auto.Text = null;
      }
      if (((ISelectableOption) cb_country_auto.SelectedItem).Id <= 0)
      {
        return;
      }
      //m_address_modified.CountryId = ((ISelectableOption)m_host_view.CbCountryAuto.SelectedItem).Id;
      txt_zip_auto.Text = m_address_modified.Zip;
      DoAddressAutoSelectPart2();
      CopyAutoAddressToManual();
    } //CbCountryAutoChanged

    /// <summary>
    ///   event fired on change of manual country
    /// </summary>
    /// <param name="Sender"></param>
    /// <param name="E"></param>
    private void cb_country_SelectedIndexChanged(object Sender, EventArgs E)
    {
      DataTable _dt_fed_states;
      DataRow _dr_empty_row;
      string _name;
      string _adj;
      string _iso2 = "";

      int _country_id = ((ISelectableOption) cb_country.SelectedItem).Id;
      if (_country_id > 0 && _country_id != m_country_id)
      {
        m_country_id = _country_id;
        Countries.GetCountryInfo(_country_id, out _name, out _adj, out _iso2);
        States.GetStates(out _dt_fed_states, _iso2);

          // Non-mandatory field.  An empty row is needed.
        _dr_empty_row = _dt_fed_states.NewRow();
        _dr_empty_row["FS_STATE_ID"] = -1;
        _dr_empty_row["FS_NAME"] = "";

        _dt_fed_states.Rows.InsertAt(_dr_empty_row, 0);
        BindDatasourceToCombo(_dt_fed_states.ToSelectableOptionList<State>(), cb_fed_entity);
      }
    } //cb_country_SelectedIndexChanged

    /// <summary>
    ///   routine to execute on form load to set label translations
    /// </summary>
    /// <param name="Sender"></param>
    /// <param name="E"></param>
    private void uc_address_Load(object Sender, EventArgs E)
    {
      lbl_address_01.Text = AccountFields.GetAddressName(); // Resource.String("STR_UC_PLAYER_TRACK_ADDRESS_01");
      lbl_address_02.Text = AccountFields.GetColonyName();  //Resource.String("STR_UC_PLAYER_TRACK_ADDRESS_02");
      lbl_address_03.Text = AccountFields.GetDelegationName();  //Resource.String("STR_UC_PLAYER_TRACK_ADDRESS_03");
      lbl_city.Text = AccountFields.GetTownshipName();  //Resource.String("STR_UC_PLAYER_TRACK_CITY");
      lbl_zip.Text = AccountFields.GetZipName(); //Resource.String("STR_UC_PLAYER_TRACK_ZIP");
      lbl_ext_num.Text = AccountFields.GetNumExtName(); //Resource.String("STR_FRM_ACCOUNT_USER_EDIT_EXT_NUM");
      lbl_country.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_COUNTRY");
      lbl_fed_entity.Text = AccountFields.GetStateName();                                 //AccountFields.GetStateName();
      lbl_address_01_auto.Text = AccountFields.GetAddressName();                          //Resource.String("STR_UC_PLAYER_TRACK_ADDRESS_01");
      lbl_address_02_auto.Text = AccountFields.GetColonyName();                           //Resource.String("STR_UC_PLAYER_TRACK_ADDRESS_02");
      lbl_address_03_auto.Text = AccountFields.GetDelegationName(); //Resource.String("STR_UC_PLAYER_TRACK_ADDRESS_03");
      lbl_city_auto.Text = AccountFields.GetTownshipName(); //Resource.String("STR_UC_PLAYER_TRACK_CITY");
      lbl_zip_auto.Text = AccountFields.GetZipName();                                     //Resource.String("STR_UC_PLAYER_TRACK_ZIP");
      lbl_num_ext_auto.Text = AccountFields.GetNumExtName();                              //Resource.String("STR_FRM_ACCOUNT_USER_EDIT_EXT_NUM");
      lbl_country_auto.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_COUNTRY");
      lbl_fed_entity_auto.Text = AccountFields.GetStateName();                            //AccountFields.GetStateName();
      chk_address_mode.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MANUAL_ENTRY");
    } //uc_address_Load

    internal void CleanUp()
    {
      cb_address_02_auto.DataSource = null;
      cb_address_03_auto.DataSource = null;
      cb_city_auto.DataSource = null;
      cb_country_auto.DataSource = null;
      cb_fed_entity_auto.DataSource = null;
      cb_country.DataSource = null;
      cb_fed_entity.DataSource = null;

      cb_address_02_auto.Dispose();
      cb_address_03_auto.Dispose();
      cb_city_auto.Dispose();
      cb_country_auto.Dispose();
      cb_fed_entity_auto.Dispose();
      cb_country.Dispose();
      cb_fed_entity.Dispose();
      cb_address_02_auto.Dispose();
      cb_address_03_auto.Dispose();
      
      cb_address_02_auto = null;
      cb_address_03_auto = null;
      cb_city_auto = null;
      cb_country_auto = null;
      cb_fed_entity_auto = null;
      cb_country = null;
      cb_fed_entity = null;
      cb_address_02_auto = null;
      cb_address_03_auto = null;
      

    }
  }
}
