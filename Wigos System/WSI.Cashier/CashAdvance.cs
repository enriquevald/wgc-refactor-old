﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: CashAdvance.cs
// 
//   DESCRIPTION: 
// 
//        AUTHOR: Francisco Vicente
// 
// CREATION DATE: 19-MAY-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 19-MAY-2016 FAV    First Release 
//
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using WSI.Common;

namespace WSI.Cashier
{
  public class CashAdvance
  {
    #region Attributes
    #endregion

    #region Constructor
    #endregion

    #region Properties
    #endregion

    #region Public Methods

    /// <summary>
    /// It gets a cashier movements list of 'cash advance' that it can to undo
    /// </summary>
    /// <param name="CardData"></param>
    /// <param name="CashierMovements"></param>
    public  void GetCashAdvanceListForUndo(CardData CardData, out List<CashierMovementsInfo> CashierMovements)
    {
      List<CASHIER_MOVEMENT> _type_list;
      SqlConnection _sql_conn = null;
      SqlTransaction _sql_trx = null;

      // Cashier movement of Cash Advance
      _type_list = new List<CASHIER_MOVEMENT>() {CASHIER_MOVEMENT.CASH_ADVANCE_GENERIC_BANK_CARD, CASHIER_MOVEMENT.CASH_ADVANCE_CREDIT_BANK_CARD, 
                                                 CASHIER_MOVEMENT.CASH_ADVANCE_DEBIT_BANK_CARD, CASHIER_MOVEMENT.CASH_ADVANCE_CHECK,
                                                 CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_GENERIC_BANK_CARD, CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_CREDIT_BANK_CARD,
                                                 CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_DEBIT_BANK_CARD, CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_CHECK};

      _sql_conn = WSI.Common.WGDB.Connection();
      using (_sql_trx = _sql_conn.BeginTransaction())
      {
        CashierBusinessLogic.GetCashierMovementsByAccountAndType(CardData.AccountId, _type_list, true, true, out CashierMovements, _sql_trx);
      }
    }

    #endregion

    #region Private Methods
    #endregion

    #region Events
    #endregion

  }
}
