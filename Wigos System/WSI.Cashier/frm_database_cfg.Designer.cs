namespace WSI.Cashier
{
  partial class frm_database_cfg
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_database_cfg));
      this.timer1 = new System.Windows.Forms.Timer(this.components);
      this.pb_connecting = new System.Windows.Forms.ProgressBar();
      this.lbl_copyright = new WSI.Cashier.Controls.uc_label();
      this.lbl_version = new WSI.Cashier.Controls.uc_label();
      this.lbl_app_name = new WSI.Cashier.Controls.uc_label();
      this.lbl_connecting_to_db = new WSI.Cashier.Controls.uc_label();
      this.btn_shutdown = new WSI.Cashier.Controls.uc_round_button();
      this.uc_database_cfg1 = new WSI.Cashier.uc_database_cfg();
      this.lbl_message_title = new WSI.Cashier.Controls.uc_label();
      this.SuspendLayout();
      // 
      // timer1
      // 
      this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
      // 
      // pb_connecting
      // 
      this.pb_connecting.Location = new System.Drawing.Point(12, 574);
      this.pb_connecting.Name = "pb_connecting";
      this.pb_connecting.Size = new System.Drawing.Size(362, 18);
      this.pb_connecting.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
      this.pb_connecting.TabIndex = 13;
      this.pb_connecting.UseWaitCursor = true;
      // 
      // lbl_copyright
      // 
      this.lbl_copyright.BackColor = System.Drawing.Color.Transparent;
      this.lbl_copyright.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_copyright.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_copyright.Location = new System.Drawing.Point(9, 489);
      this.lbl_copyright.Name = "lbl_copyright";
      this.lbl_copyright.Size = new System.Drawing.Size(365, 54);
      this.lbl_copyright.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_copyright.TabIndex = 17;
      this.lbl_copyright.Text = "Wigos System\r\nCopyright � 2008-2016 WSI";
      this.lbl_copyright.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // lbl_version
      // 
      this.lbl_version.BackColor = System.Drawing.Color.Transparent;
      this.lbl_version.Font = new System.Drawing.Font("Montserrat", 32F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_version.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_version.Location = new System.Drawing.Point(9, 435);
      this.lbl_version.Name = "lbl_version";
      this.lbl_version.Size = new System.Drawing.Size(365, 40);
      this.lbl_version.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_32PX_BLACK;
      this.lbl_version.TabIndex = 16;
      this.lbl_version.Text = "(CC.BBB)";
      this.lbl_version.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lbl_app_name
      // 
      this.lbl_app_name.BackColor = System.Drawing.Color.Transparent;
      this.lbl_app_name.Font = new System.Drawing.Font("Montserrat", 32F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_app_name.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_app_name.Location = new System.Drawing.Point(12, 390);
      this.lbl_app_name.Name = "lbl_app_name";
      this.lbl_app_name.Size = new System.Drawing.Size(362, 45);
      this.lbl_app_name.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_32PX_BLACK;
      this.lbl_app_name.TabIndex = 15;
      this.lbl_app_name.Text = "WSI.CASHIER";
      this.lbl_app_name.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lbl_connecting_to_db
      // 
      this.lbl_connecting_to_db.BackColor = System.Drawing.Color.Transparent;
      this.lbl_connecting_to_db.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_connecting_to_db.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_connecting_to_db.Location = new System.Drawing.Point(9, 543);
      this.lbl_connecting_to_db.Name = "lbl_connecting_to_db";
      this.lbl_connecting_to_db.Size = new System.Drawing.Size(365, 28);
      this.lbl_connecting_to_db.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_connecting_to_db.TabIndex = 14;
      this.lbl_connecting_to_db.Text = "xConnecting to Database";
      this.lbl_connecting_to_db.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // btn_shutdown
      // 
      this.btn_shutdown.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_shutdown.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
      this.btn_shutdown.FlatAppearance.BorderSize = 0;
      this.btn_shutdown.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_shutdown.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_shutdown.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_shutdown.Image = ((System.Drawing.Image)(resources.GetObject("btn_shutdown.Image")));
      this.btn_shutdown.Location = new System.Drawing.Point(23, 189);
      this.btn_shutdown.Name = "btn_shutdown";
      this.btn_shutdown.Size = new System.Drawing.Size(108, 53);
      this.btn_shutdown.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_shutdown.TabIndex = 11;
      this.btn_shutdown.UseVisualStyleBackColor = false;
      this.btn_shutdown.Click += new System.EventHandler(this.btn_shutdown_Click);
      // 
      // uc_database_cfg1
      // 
      this.uc_database_cfg1.BackColor = System.Drawing.Color.Transparent;
      this.uc_database_cfg1.Location = new System.Drawing.Point(25, 36);
      this.uc_database_cfg1.Mode = WSI.Cashier.DatabaseCfgMode.READ;
      this.uc_database_cfg1.Name = "uc_database_cfg1";
      this.uc_database_cfg1.Size = new System.Drawing.Size(450, 235);
      this.uc_database_cfg1.TabIndex = 10;
      this.uc_database_cfg1.ValuesChanged += new System.EventHandler(this.uc_database_cfg1_ValuesChanged);
      // 
      // lbl_message_title
      // 
      this.lbl_message_title.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.lbl_message_title.Dock = System.Windows.Forms.DockStyle.Top;
      this.lbl_message_title.Font = new System.Drawing.Font("Montserrat", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_message_title.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(167)))), ((int)(((byte)(221)))), ((int)(((byte)(241)))));
      this.lbl_message_title.Location = new System.Drawing.Point(0, 0);
      this.lbl_message_title.Name = "lbl_message_title";
      this.lbl_message_title.Size = new System.Drawing.Size(486, 28);
      this.lbl_message_title.Style = WSI.Cashier.Controls.uc_label.LabelStyle.POPUP_HEADER;
      this.lbl_message_title.TabIndex = 9;
      this.lbl_message_title.Text = "XDATABASE ERROR";
      this.lbl_message_title.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // frm_database_cfg
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.AutoSize = true;
      this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
      this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
      this.ClientSize = new System.Drawing.Size(486, 606);
      this.Controls.Add(this.lbl_copyright);
      this.Controls.Add(this.lbl_version);
      this.Controls.Add(this.lbl_app_name);
      this.Controls.Add(this.lbl_connecting_to_db);
      this.Controls.Add(this.pb_connecting);
      this.Controls.Add(this.btn_shutdown);
      this.Controls.Add(this.uc_database_cfg1);
      this.Controls.Add(this.lbl_message_title);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "frm_database_cfg";
      this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
      this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
      this.Text = "Database Configuration";
      this.TopMost = true;
      this.ResumeLayout(false);

    }

    #endregion

    private WSI.Cashier.Controls.uc_label lbl_message_title;
    private uc_database_cfg uc_database_cfg1;
    private WSI.Cashier.Controls.uc_round_button btn_shutdown;
    private System.Windows.Forms.Timer timer1;
    private System.Windows.Forms.ProgressBar pb_connecting;
    private WSI.Cashier.Controls.uc_label lbl_connecting_to_db;
    private WSI.Cashier.Controls.uc_label lbl_app_name;
    private WSI.Cashier.Controls.uc_label lbl_version;
    private WSI.Cashier.Controls.uc_label lbl_copyright;
  }
}