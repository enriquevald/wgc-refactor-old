//------------------------------------------------------------------------------
// Copyright � 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_mb_account_user_edit.cs
// 
//   DESCRIPTION: Implements the following classes:
//                1. frm_mb_account_user_edit
//
//        AUTHOR: APB
// 
// CREATION DATE: 10-JUL-2009
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 10-JUL-2009 APB    First release.
// 13-AUG-2013 DRV    Removed disabling OSK if it's in window mode. And added Force OSK if keyboard button is clicked
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WSI.Common;

namespace WSI.Cashier
{  
  public partial class frm_mb_account_user_edit: Controls.frm_base
  {
    #region Attributes

    string account_id;
    string initial_user_name;
    //uc_card parent_form;

    #endregion

    #region Constructor

    public frm_mb_account_user_edit (/*uc_card Parent*/)
    {
      InitializeComponent ();

      InitializeControlResources();

      //parent_form = Parent;
      pictureBox1.Image = Resources.ResourceImages.userMB;
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Initialize NLS strings
    /// </summary>
    private void InitializeControlResources()
    {
      // - NLS Strings:
      //   - Title
      this.FormTitle = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_TITLE");

      //   - Labels
      lbl_account.Text = Resource.String("STR_FORMAT_GENERAL_NAME_MB");
      lbl_mode.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MODE");
      lbl_name.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_NAME");

      //   - Buttons
      btn_ok.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_OK");
      btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");
      btn_mode_to_anonymous.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_BTN_CARD_MODE_TO_ANONYM");
      btn_mode_to_personal.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_BTN_CARD_MODE_TO_PERSONAL");

      // - Images:

    } // InitializeControlResources

    private void Init()
    {
      this.Location = new Point(1024 / 2 - this.Width / 2 , 768 / 2 - this.Height / 2 - 70);

      this.lbl_account_id_value.Text = this.account_id;
      //this.lbl_account_id_value.Location = new Point(lbl_account.Location.X+lbl_account.Width,lbl_account.Location.Y);

      if (initial_user_name == "")
      {
        // Mode: Anonymous
        btn_mode_to_anonymous.Enabled = false;
        btn_mode_to_personal.Enabled = true;

        txt_name.Enabled = false;
        txt_name.Text = "";
        initial_user_name = "";

        txt_name.Visible = false;
        lbl_name.Visible = false;

        btn_mode_to_anonymous.Visible = false;
        btn_mode_to_personal.Visible = true;

        pictureBox1.Image = Resources.ResourceImages.userMB;
        btn_keyboard.Image = WSI.Cashier.Resources.ResourceImages.keyboard;
        lbl_mode_value.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_CARD_MODE_ANONYMOUS");
        
      }
      else
      {
        // Mode: Personal
        btn_mode_to_anonymous.Enabled = true;
        btn_mode_to_personal.Enabled = false;

        txt_name.Enabled = true;
        txt_name.Text = this.initial_user_name;

        txt_name.Visible = true;
        lbl_name.Visible = true;

        btn_mode_to_anonymous.Visible = true;
        btn_mode_to_personal.Visible = false;

        pictureBox1.Image = Resources.ResourceImages.userMB;

        lbl_mode_value.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_CARD_MODE_PERSONAL");
      }
      //Shows OSK if Automatic OSK it's enabled
      WSIKeyboard.Toggle();
    }

    private void btn_ok_Click(object sender, EventArgs e)
    {
      this.timer1.Enabled = true;

      // MB name shouldn't be empty
      if (this.txt_name.Text.Trim() == "" && btn_mode_to_anonymous.Visible == true)
      {
        this.txt_name.Focus();
        this.lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK");
        this.lbl_msg_blink.Visible = true;

        return;
      }

      // MB name format validate 
      if (!ValidateFormat.TextWithNumbers(txt_name.Text))
      {
        this.txt_name.Focus();

        lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", this.lbl_name.Text);        
        this.lbl_msg_blink.Visible = true;

        return;
      }

      if (this.txt_name.Text != this.initial_user_name)
      {
        CashierBusinessLogic.DB_UpdateMBCardHolder(this.account_id, this.txt_name.Text);
      }

      WSIKeyboard.Hide();
      this.Dispose();

    }

    private void btn_cancel_Click(object sender, EventArgs e)
    {
      WSIKeyboard.Hide();
      this.Dispose();
    }

    /// <summary>
    /// Actions for Mode: "To Anonymous"
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btn_anonym_Click(object sender, EventArgs e)
    {
      txt_name.Text = "";
      txt_name.Enabled = false;
      txt_name.Visible = false;

      btn_mode_to_personal.Enabled = true;
      btn_mode_to_anonymous.Enabled = false;

      lbl_name.Visible = false;

      btn_mode_to_anonymous.Visible = false;
      btn_mode_to_personal.Visible = true;

      pictureBox1.Image = Resources.ResourceImages.userMB;
      lbl_mode_value.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_CARD_MODE_ANONYMOUS");
    }

    /// <summary>
    /// Actions for Mode: "To Personal"
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btn_personal_Click(object sender, EventArgs e)
    {
      btn_mode_to_personal.Enabled = false;
      btn_mode_to_anonymous.Enabled = true;

      txt_name.Enabled = true;
      txt_name.Visible = true;
      txt_name.Text = this.initial_user_name;
      lbl_name.Visible = true;

      btn_mode_to_anonymous.Visible = true;
      btn_mode_to_personal.Visible = false;

      pictureBox1.Image = Resources.ResourceImages.userMB;
     
      lbl_mode_value.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_CARD_MODE_PERSONAL");

      txt_name.Focus();
    }

    private void btn_keyboard_Click(object sender, EventArgs e)
    {
      //Shows OSK
      WSIKeyboard.ForceToggle();            
    }

    private void timer1_Tick(object sender, EventArgs e)
    { 
      if (lbl_msg_blink.Visible == true)
      {
        lbl_msg_blink.Visible = false;
        timer1.Enabled = false;        
      }
    }

    private void frm_mb_account_user_edit_Shown(object sender, EventArgs e)
    {
      if (this.txt_name.Visible)
      {
        this.txt_name.Focus();
      }
      else
      {
        this.lbl_mode.Focus();
      }
    } // frm_mb_account_user_edit_Shown

    #endregion

    #region Public Methods

    public void GetUserData(string Id, string Name)
    {
      account_id = Id;
      initial_user_name = Name;

      Init();
    }

    public void GetUserData(string Id)
    {
      account_id = Id;
      initial_user_name = "";

      Init();
    }


    #endregion

  }
}