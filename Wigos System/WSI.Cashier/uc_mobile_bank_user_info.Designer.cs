﻿namespace WSI.Cashier
{
  partial class uc_mobile_bank_user_info
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(uc_mobile_bank_user_info));
      this.gbMobileBankUserInfo = new WSI.Cashier.Controls.uc_round_panel();
      this.lbl_username_value = new WSI.Cashier.Controls.uc_label();
      this.lbl_track_data_title = new WSI.Cashier.Controls.uc_label();
      this.lbl_holder_name_title = new WSI.Cashier.Controls.uc_label();
      this.lbl_track_data_value = new WSI.Cashier.Controls.uc_label();
      this.lbl_username_title = new WSI.Cashier.Controls.uc_label();
      this.pb_user = new System.Windows.Forms.PictureBox();
      this.lbl_holder_name_value = new WSI.Cashier.Controls.uc_label();
      this.gbMobileBankUserInfo.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pb_user)).BeginInit();
      this.SuspendLayout();
      // 
      // gbMobileBankUserInfo
      // 
      this.gbMobileBankUserInfo.BackColor = System.Drawing.Color.Transparent;
      this.gbMobileBankUserInfo.BorderColor = System.Drawing.Color.Empty;
      this.gbMobileBankUserInfo.Controls.Add(this.lbl_username_value);
      this.gbMobileBankUserInfo.Controls.Add(this.lbl_track_data_title);
      this.gbMobileBankUserInfo.Controls.Add(this.lbl_holder_name_title);
      this.gbMobileBankUserInfo.Controls.Add(this.lbl_track_data_value);
      this.gbMobileBankUserInfo.Controls.Add(this.lbl_username_title);
      this.gbMobileBankUserInfo.Controls.Add(this.pb_user);
      this.gbMobileBankUserInfo.Controls.Add(this.lbl_holder_name_value);
      this.gbMobileBankUserInfo.CornerRadius = 10;
      this.gbMobileBankUserInfo.Dock = System.Windows.Forms.DockStyle.Fill;
      this.gbMobileBankUserInfo.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gbMobileBankUserInfo.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.gbMobileBankUserInfo.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.gbMobileBankUserInfo.HeaderHeight = 0;
      this.gbMobileBankUserInfo.HeaderSubText = null;
      this.gbMobileBankUserInfo.HeaderText = null;
      this.gbMobileBankUserInfo.Location = new System.Drawing.Point(0, 0);
      this.gbMobileBankUserInfo.Name = "gbMobileBankUserInfo";
      this.gbMobileBankUserInfo.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gbMobileBankUserInfo.Size = new System.Drawing.Size(413, 152);
      this.gbMobileBankUserInfo.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.WITHOUT_HEAD;
      this.gbMobileBankUserInfo.TabIndex = 4;
      // 
      // lbl_username_value
      // 
      this.lbl_username_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_username_value.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_username_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_username_value.Location = new System.Drawing.Point(158, 22);
      this.lbl_username_value.Name = "lbl_username_value";
      this.lbl_username_value.Size = new System.Drawing.Size(234, 21);
      this.lbl_username_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_username_value.TabIndex = 1;
      this.lbl_username_value.Text = "xUsername";
      this.lbl_username_value.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_track_data_title
      // 
      this.lbl_track_data_title.BackColor = System.Drawing.Color.Transparent;
      this.lbl_track_data_title.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_track_data_title.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_track_data_title.Location = new System.Drawing.Point(7, 71);
      this.lbl_track_data_title.Name = "lbl_track_data_title";
      this.lbl_track_data_title.Size = new System.Drawing.Size(96, 19);
      this.lbl_track_data_title.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_track_data_title.TabIndex = 3;
      this.lbl_track_data_title.Text = "xTrackdata:";
      this.lbl_track_data_title.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_holder_name_title
      // 
      this.lbl_holder_name_title.BackColor = System.Drawing.Color.Transparent;
      this.lbl_holder_name_title.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_holder_name_title.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_holder_name_title.Location = new System.Drawing.Point(6, 106);
      this.lbl_holder_name_title.Name = "lbl_holder_name_title";
      this.lbl_holder_name_title.Size = new System.Drawing.Size(110, 23);
      this.lbl_holder_name_title.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_holder_name_title.TabIndex = 5;
      this.lbl_holder_name_title.Text = "xHolderName:";
      this.lbl_holder_name_title.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_track_data_value
      // 
      this.lbl_track_data_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_track_data_value.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_track_data_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_track_data_value.Location = new System.Drawing.Point(120, 72);
      this.lbl_track_data_value.Name = "lbl_track_data_value";
      this.lbl_track_data_value.Size = new System.Drawing.Size(272, 20);
      this.lbl_track_data_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_track_data_value.TabIndex = 4;
      this.lbl_track_data_value.Text = "88888888888888888812";
      this.lbl_track_data_value.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_username_title
      // 
      this.lbl_username_title.BackColor = System.Drawing.Color.Transparent;
      this.lbl_username_title.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_username_title.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_username_title.Location = new System.Drawing.Point(57, 22);
      this.lbl_username_title.Name = "lbl_username_title";
      this.lbl_username_title.Size = new System.Drawing.Size(94, 21);
      this.lbl_username_title.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_username_title.TabIndex = 0;
      this.lbl_username_title.Text = "xUserName:";
      this.lbl_username_title.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // pb_user
      // 
      this.pb_user.Image = global::WSI.Cashier.Resources.ResourceImages.anonymous_user;
      this.pb_user.InitialImage = ((System.Drawing.Image)(resources.GetObject("pb_user.InitialImage")));
      this.pb_user.Location = new System.Drawing.Point(5, 8);
      this.pb_user.Name = "pb_user";
      this.pb_user.Size = new System.Drawing.Size(48, 46);
      this.pb_user.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
      this.pb_user.TabIndex = 72;
      this.pb_user.TabStop = false;
      // 
      // lbl_holder_name_value
      // 
      this.lbl_holder_name_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_holder_name_value.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_holder_name_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_holder_name_value.Location = new System.Drawing.Point(120, 106);
      this.lbl_holder_name_value.Name = "lbl_holder_name_value";
      this.lbl_holder_name_value.Size = new System.Drawing.Size(272, 23);
      this.lbl_holder_name_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_holder_name_value.TabIndex = 6;
      this.lbl_holder_name_value.Text = "TTTTTTTTTTTTT";
      this.lbl_holder_name_value.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.lbl_holder_name_value.UseMnemonic = false;
      // 
      // uc_mobile_bank_user_info
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.gbMobileBankUserInfo);
      this.Name = "uc_mobile_bank_user_info";
      this.Size = new System.Drawing.Size(413, 152);
      this.gbMobileBankUserInfo.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.pb_user)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private Controls.uc_round_panel gbMobileBankUserInfo;
    private Controls.uc_label lbl_username_value;
    private Controls.uc_label lbl_track_data_title;
    private Controls.uc_label lbl_holder_name_title;
    private Controls.uc_label lbl_track_data_value;
    private Controls.uc_label lbl_username_title;
    private System.Windows.Forms.PictureBox pb_user;
    private Controls.uc_label lbl_holder_name_value;
  }
}
