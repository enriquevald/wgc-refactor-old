using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using WSI.Common;

namespace WSI.Cashier
{
  public partial class uc_currency_amount : UserControl
  {
    #region Constructor
    
    public uc_currency_amount()
    {
      InitializeComponent();
    }

    #endregion


    private void uc_currency_amount_Load(object sender, EventArgs e)
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[UC LOAD] uc_currency_amount", Log.Type.Message);

      CurrencyExchange _national_currency;
      List<CurrencyExchange> _allowed_currencies;
      CurrencyExchangeProperties _curr_ex_proper;
      Bitmap _image_flag;
      
      CurrencyExchange.GetAllowedCurrencies(true, out _national_currency, out _allowed_currencies);

      if (_national_currency != null && _allowed_currencies.Count > 0)
      {
        this.lbl_equal.Visible = true;
        this.chk_CurrAct.Visible = true;
        this.txt_amount.Size = new Size(235, 44);

        _curr_ex_proper = CurrencyExchangeProperties.GetProperties(_national_currency.CurrencyCode);
        this.chk_CurrAct.Text = _national_currency.CurrencyCode;
        try
        {
          _image_flag = _curr_ex_proper.GetFlag(this.chk_CurrAct.Size.Width, this.chk_CurrAct.Size.Height);
          if (_image_flag != null)
          {
            this.chk_CurrAct.Image = _image_flag;
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }
      }
      else
      {
        this.lbl_equal.Visible = false;
        this.chk_CurrAct.Visible = false;
        this.txt_amount.Size = new Size(291, 44);
      }
    }
  }
}
