//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_reception_visits_report.cs
// 
//   DESCRIPTION: PBI 15443 Visitas / Recepción: MEJORAS - Cajero - Reporte de Entradas
//
//        AUTHOR: JRC
// 
// CREATION DATE: 18-JUL-2016
//
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 18-JUL-2016 JRC    First release.
// 05-AGO-2016 FJC    Bug 16435:Reporte de visitas: No se actualiza el estado
// 19-SEP-2016 JRC    PBI 17677:Visitas / Recepción: MEJORAS II - Ref. 48 - Reportes recepción - separar columna Nombre
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using WSI.Common;
using WSI.Common.TITO;
using WSI.Cashier.TITO;
using System.IO;
using System.Drawing.Text;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Globalization;
using System.Collections;
using WSI.IDScanner.Model;
using WSI.IDCamera;
using WSI.Cashier.MVC.Controller.PlayerEditDetails;
using WSI.Cashier.MVC.View.PlayerEditDetails;

namespace WSI.Cashier
{
  public partial class frm_reception_visits_report : Controls.frm_base
  {
    #region Enums
    #endregion //Enums

    #region Constants

    // Number of columns
    public Int32 GRID_COLUMN_MAX_COLUMNS = 10; //Must be updated if change number of columns

    // Columns defined
    public Int32 GRID_COLUMN_IMAGE = 0;
    public Int32 GRID_COLUMN_NAME = 1;
    public Int32 GRID_COLUMN_LASTNAME = 2;
    public Int32 GRID_COLUMN_GENDER = 3;
    public Int32 GRID_COLUMN_BIRTHDAY = 4;
    public Int32 GRID_COLUMN_DOCUMENT_TYPE = 5;
    public Int32 GRID_COLUMN_DOCUMENT_NUMBER = 6;
    public Int32 GRID_COLUMN_LEVEL = 7;
    public Int32 GRID_COLUMN_ENTRANCES = 8;
    public Int32 GRID_COLUMN_PRICE = 9;
    public Int32 GRID_COLUMN_TIME = 10;
    public Int32 GRID_COLUMN_ACCOUNT_ID = 11;
    public Int32 GRID_COLUMN_EXIT = 12;


    // Column widths
    private const Int32 GRID_COLUMN_WIDTH_IMAGE = 80;
    private const Int32 GRID_COLUMN_WIDTH_NAME = 285;
    private const Int32 GRID_COLUMN_WIDTH_LASTNAME = 0;
    private const Int32 GRID_COLUMN_WIDTH_DOCUMENT_TYPE = 80;
    private const Int32 GRID_COLUMN_WIDTH_DOCUMENT_NUMBER = 100;
    private const Int32 GRID_COLUMN_WIDTH_TIME = 60;
    private const Int32 GRID_COLUMN_WIDTH_PRICE = 70;
    private const Int32 GRID_COLUMN_WIDTH_GENDER = 55;
    private const Int32 GRID_COLUMN_WIDTH_LEVEL = 60;
    private const Int32 GRID_COLUMN_WIDTH_BIRTHDAY = 90;
    private const Int32 GRID_COLUMN_WIDTH_ENTRANCES = 65;
    private const Int32 GRID_COLUMN_WIDTH_ACCOUNT_ID = 0;
    private const Int32 GRID_COLUMN_WIDTH_EXIT = 50;

    #endregion // Constants

    #region Attributes


    private frm_yesno form_yes_no;
    private Boolean m_found_record;
    private CustomerReception m_customer_reception;
    private CameraModule m_camera_module;
    private uc_bank m_uc_bank;
    private DateTime m_search_date;
    #endregion // Attributes

    #region Properties
    public IDScannerInfo LastScannedInfo { get; set; } //LastScannedInfo
    public IDScanner.IDScanner IDScanner { get; set; }
    #endregion

    #region Constructor

    /// <summary>
    ///  Constructor
    /// </summary>
    public frm_reception_visits_report(uc_bank UCBank)
    {
      InitializeComponent();
      InitializeControlResources();
      m_uc_bank = UCBank;

      form_yes_no = new frm_yesno();
      form_yes_no.Opacity = 0.6;
    } //frm_reception_visits_report

    #endregion // Constructor

    #region Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Initialize Operations Data Grid.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private void InitOperationsDataGrid()
    {
      String _date_format;
      DataGridViewImageColumn _column;
      _date_format = System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern;

      dgv_last_operations.Columns[GRID_COLUMN_IMAGE].Width = GRID_COLUMN_WIDTH_IMAGE;
      dgv_last_operations.Columns[GRID_COLUMN_NAME].Width = GRID_COLUMN_WIDTH_NAME;
      dgv_last_operations.Columns[GRID_COLUMN_LASTNAME].Width = GRID_COLUMN_WIDTH_LASTNAME;
      dgv_last_operations.Columns[GRID_COLUMN_DOCUMENT_TYPE].Width = GRID_COLUMN_WIDTH_DOCUMENT_TYPE;
      dgv_last_operations.Columns[GRID_COLUMN_DOCUMENT_NUMBER].Width = GRID_COLUMN_WIDTH_DOCUMENT_NUMBER;
      dgv_last_operations.Columns[GRID_COLUMN_TIME].Width = GRID_COLUMN_WIDTH_TIME;
      dgv_last_operations.Columns[GRID_COLUMN_PRICE].Width = GRID_COLUMN_WIDTH_PRICE;
      dgv_last_operations.Columns[GRID_COLUMN_GENDER].Width = GRID_COLUMN_WIDTH_GENDER;
      dgv_last_operations.Columns[GRID_COLUMN_LEVEL].Width = GRID_COLUMN_WIDTH_LEVEL;
      dgv_last_operations.Columns[GRID_COLUMN_BIRTHDAY].Width = GRID_COLUMN_WIDTH_BIRTHDAY;
      dgv_last_operations.Columns[GRID_COLUMN_ENTRANCES].Width = GRID_COLUMN_WIDTH_ENTRANCES;
      dgv_last_operations.Columns[GRID_COLUMN_ACCOUNT_ID].Width = GRID_COLUMN_WIDTH_ACCOUNT_ID;
      dgv_last_operations.Columns[GRID_COLUMN_EXIT].Width = GRID_COLUMN_WIDTH_EXIT;


      dgv_last_operations.Columns[GRID_COLUMN_IMAGE].HeaderCell.Value = Resource.String("STR_FRM_RECEPTION_VISITS_REPORT_02");
      dgv_last_operations.Columns[GRID_COLUMN_IMAGE].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
      dgv_last_operations.Columns[GRID_COLUMN_IMAGE].ValueType = typeof(DataGridViewImageColumn);
      _column = (DataGridViewImageColumn)dgv_last_operations.Columns[GRID_COLUMN_IMAGE];
      _column.ImageLayout = DataGridViewImageCellLayout.Zoom;

      dgv_last_operations.Columns[GRID_COLUMN_NAME].HeaderCell.Value = Resource.String("STR_FRM_RECEPTION_VISITS_REPORT_03");
      dgv_last_operations.Columns[GRID_COLUMN_NAME].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

      dgv_last_operations.Columns[GRID_COLUMN_LASTNAME].HeaderCell.Value = Resource.String("STR_FRM_RECEPTION_VISITS_REPORT_19");
      dgv_last_operations.Columns[GRID_COLUMN_LASTNAME].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
      dgv_last_operations.Columns[GRID_COLUMN_LASTNAME].Visible = false;

      dgv_last_operations.Columns[GRID_COLUMN_DOCUMENT_TYPE].HeaderCell.Value = Resource.String("STR_FRM_RECEPTION_VISITS_REPORT_04");
      dgv_last_operations.Columns[GRID_COLUMN_DOCUMENT_TYPE].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
      dgv_last_operations.Columns[GRID_COLUMN_DOCUMENT_TYPE].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

      dgv_last_operations.Columns[GRID_COLUMN_DOCUMENT_NUMBER].HeaderCell.Value = Resource.String("STR_FRM_RECEPTION_VISITS_REPORT_05");
      dgv_last_operations.Columns[GRID_COLUMN_DOCUMENT_NUMBER].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
      dgv_last_operations.Columns[GRID_COLUMN_DOCUMENT_NUMBER].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

      dgv_last_operations.Columns[GRID_COLUMN_TIME].HeaderCell.Value = Resource.String("STR_FRM_RECEPTION_VISITS_REPORT_06");
      dgv_last_operations.Columns[GRID_COLUMN_TIME].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
      dgv_last_operations.Columns[GRID_COLUMN_TIME].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
      dgv_last_operations.Columns[GRID_COLUMN_TIME].DefaultCellStyle.Format = "HH:mm";

      dgv_last_operations.Columns[GRID_COLUMN_PRICE].HeaderCell.Value = Resource.String("STR_FRM_RECEPTION_VISITS_REPORT_07");
      dgv_last_operations.Columns[GRID_COLUMN_PRICE].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

      dgv_last_operations.Columns[GRID_COLUMN_PRICE].DefaultCellStyle.Format = "C" + Math.Abs(Format.GetFormattedDecimals(CurrencyExchange.GetNationalCurrency())).ToString();
      dgv_last_operations.Columns[GRID_COLUMN_PRICE].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

      dgv_last_operations.Columns[GRID_COLUMN_GENDER].HeaderCell.Value = Resource.String("STR_FRM_RECEPTION_VISITS_REPORT_08");
      dgv_last_operations.Columns[GRID_COLUMN_GENDER].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
      dgv_last_operations.Columns[GRID_COLUMN_GENDER].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

      dgv_last_operations.Columns[GRID_COLUMN_LEVEL].HeaderCell.Value = Resource.String("STR_FRM_RECEPTION_VISITS_REPORT_09");
      dgv_last_operations.Columns[GRID_COLUMN_LEVEL].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
      dgv_last_operations.Columns[GRID_COLUMN_LEVEL].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

      dgv_last_operations.Columns[GRID_COLUMN_BIRTHDAY].HeaderCell.Value = Resource.String("STR_FRM_RECEPTION_VISITS_REPORT_10");
      dgv_last_operations.Columns[GRID_COLUMN_BIRTHDAY].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
      dgv_last_operations.Columns[GRID_COLUMN_BIRTHDAY].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
      dgv_last_operations.Columns[GRID_COLUMN_BIRTHDAY].DefaultCellStyle.Format = "dd/MM/yyyy";

      dgv_last_operations.Columns[GRID_COLUMN_ENTRANCES].HeaderCell.Value = Resource.String("STR_FRM_RECEPTION_VISITS_REPORT_11");
      dgv_last_operations.Columns[GRID_COLUMN_ENTRANCES].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
      dgv_last_operations.Columns[GRID_COLUMN_ENTRANCES].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

      dgv_last_operations.Columns[GRID_COLUMN_ACCOUNT_ID].Visible = false;

      dgv_last_operations.Columns[GRID_COLUMN_EXIT].HeaderCell.Value = Resource.String("STR_FRM_RECEPTION_VISITS_REPORT_20");
      dgv_last_operations.Columns[GRID_COLUMN_EXIT].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
      dgv_last_operations.Columns[GRID_COLUMN_EXIT].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
      dgv_last_operations.Columns[GRID_COLUMN_EXIT].DefaultCellStyle.Format = "HH:mm";

      // Last Operations DataGrid
      dgv_last_operations.ColumnHeadersHeight = 30;
      dgv_last_operations.RowTemplate.Height = 60;
      dgv_last_operations.RowTemplate.DividerHeight = 0;

    } // InitOperationsDataGrid

    /// <summary>
    /// Initialize controls
    /// </summary>
    private void InitControls()
    {
      m_customer_reception = new CustomerReception();
      dt_dateparam.DateValue = Misc.TodayOpening();
      m_search_date = Misc.TodayOpening();
    } // InitControls

    //------------------------------------------------------------------------------
    // PURPOSE : Get DataTable with visits
    //
    //  PARAMS :
    //      - INPUT :

    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - DataTable: Visits List
    //
    private DataTable GetFilteredVisitsList(Int32 GamingDay, Boolean PlayersOnSite)
    {
      DataTable _dt_tickets;
      StringBuilder _sb;

      _dt_tickets = CreateVisitsTable();

      _sb = new StringBuilder();

      try
      {
        _sb.AppendLine("  EXECUTE ReceptionCustomerVisits ");
        _sb.AppendLine("  @pStartDate,");
        _sb.AppendLine("  @pPlayersOnSite");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pStartDate", SqlDbType.Int).Value = GamingDay;
            _cmd.Parameters.Add("@pPlayersOnSite", SqlDbType.Bit).Value = PlayersOnSite;
            _dt_tickets.Load(_db_trx.ExecuteReader(_cmd));

          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return _dt_tickets;
    } // GeTicketsFromCashierSessionId

    private void LoadVisitsGrid(Int32 GamingDay, Boolean PlayersOnSite)
    {
      dgv_last_operations.DataSource = ConvertSets(GetFilteredVisitsList(GamingDay, PlayersOnSite));
      this.FormTitle = Resource.String("STR_FRM_RECEPTION_VISITS_REPORT_01") + " " + this.m_search_date.ToString("dd/MM/yyyy");
      this.Text = this.FormTitle;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Open frm_player_edit to edit data account or create new account
    // 
    //  PARAMS:
    //      - INPUT: 
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //    
    // 
    //   NOTES:
    private void OpenPlayerEdit(DataSet OutDs, bool NewCustomer = false, IDScannerInfo ScannedInfo = null, string TrackNumeber = null)
    {
      try
      {
        if (IDScanner != null)
        {
          IDScanner.DetatchFromAllAvailable(null);
        }
        //btn_escanear_id.Enabled = false;
        CardData _card_data;
        DialogResult _dgr;
        _card_data = new CardData();


        if (m_customer_reception == null)
        {
          m_customer_reception = new CustomerReception();
        }

        if (m_customer_reception.AccountId > 0)
        {
          _card_data.AccountId = m_customer_reception.AccountId;
          _card_data.TrackData = m_customer_reception.CardCodBar;
          CardData.DB_CardGetPersonalData(_card_data.AccountId, _card_data);
        }

        if (!m_found_record)
        {
          _card_data.TrackData = m_customer_reception.CardCodBar;
        }

        if (m_camera_module == null)
        {
          m_camera_module = new CameraModule(this, typeof(frmCapturePhoto), typeof(SelectDevice)) { EventMode = false };
        }

        using (frm_yesno _shader = new frm_yesno())
        {
          using (PlayerEditDetailsReceptionView _reception_view = new PlayerEditDetailsReceptionView())
          {
            using (var _controller =
              new PlayerEditDetailsReceptionController(OutDs, m_customer_reception.StringSearchSql, NewCustomer,
                ScannedInfo, _reception_view, IDScanner, this.m_uc_bank, TrackNumeber, _reception_view.ViewTopResults) { SearchedDocumentId = m_customer_reception.Document })
            {
              _shader.Opacity = 0.6;
              _shader.Show();
              if (WindowManager.StartUpMode == WindowManager.StartUpModes.Explorer)
              {
                _reception_view.Location = new Point(_shader.Location.X, _shader.Location.Y);
              }
              var _player_edit_details_reception_controller =
                _reception_view.Controller as PlayerEditDetailsReceptionController;
              if (_player_edit_details_reception_controller != null)
              {
                if (!_card_data.IsNew && _card_data.PlayerTracking != null)
                  _player_edit_details_reception_controller.MustShowComments =
                    _card_data.PlayerTracking.ShowCommentsOnCashier;
                _player_edit_details_reception_controller.CameraModule = m_camera_module;
              }
              _dgr = DialogResult.Cancel;
              if (!_controller.AbortOpen)
                _dgr = _reception_view.ShowDialog(_shader);
            }
          }
        }
        m_customer_reception = null;

        // Call for Refresh Report Visits if any change in card account if succes
        this.RefreshReportVisit();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {

      }
    } //OpenPlayerEdit

    private void LoadEntry()
    {


      try
      {
        DataSet _out_ds;
        m_found_record = false;
        if (!m_customer_reception.GetCustomerAccounts(out _out_ds))
        {
          return;
        }

        if (!_out_ds.Tables.Contains("Accounts"))
        {
          return;
        }

        if (_out_ds.Tables["Accounts"].Rows.Count <= 0)
        {
          return;
        }

        if (!string.IsNullOrEmpty(m_customer_reception.CardCodBar))
        {
          m_customer_reception.AccountId = (long)_out_ds.Tables["Accounts"].Rows[0][0]; //account_ID

        }

        if (m_customer_reception == null)
        {
          return;
        }

        OpenPlayerEdit(_out_ds, false, LastScannedInfo);
        m_found_record = true;
      }
      catch (Exception)
      {
        frm_message.Show(WSI.Common.Resource.String("STR_RECEPTION_ERROR_GENERAL_TITLE"),
          WSI.Common.Resource.String("STR_RECEPTION_ERROR_GENERAL_TEXT"), MessageBoxButtons.OK, MessageBoxIcon.Error, this);
        // ignored
      }
    } //LoadEntry

    /// <summary>
    /// Refresh ReporVisit when come form edit for ex.
    /// </summary>
    private void RefreshReportVisit()
    {
      Int64 _account_selected;

      _account_selected = 0;
      try
      {
        _account_selected = Convert.ToInt64(dgv_last_operations.Rows[dgv_last_operations.CurrentRow.Index].Cells[GRID_COLUMN_ACCOUNT_ID].Value);
        LoadVisitsGrid(Int32.Parse(m_search_date.ToString("yyyyMMdd")), this.chk_players_on_site.Checked);

        foreach (DataGridViewRow row in dgv_last_operations.Rows)
        {
          if (row.Cells[GRID_COLUMN_ACCOUNT_ID].Value.ToString().Equals(_account_selected.ToString()))
          {
            row.Selected = true;
            dgv_last_operations.FirstDisplayedScrollingRowIndex = row.Index;

            break;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    }

    #endregion // Private Methods

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Create Table Schema for ticket grid control
    //
    //  PARAMS :
    //      - INPUT :
    //      - OUTPUT :
    //
    // RETURNS : DataTable
    //
    //   NOTES :
    //
    public DataTable CreateVisitsTable()
    {
      DataTable _table = new DataTable();
      return _table;
    } //CreateVisitsTable

    /// <summary>
    /// ConvertSets : Converts elements from its Ids to string representation
    /// </summary>
    /// <param name="Table"></param>
    /// <returns></returns>
    public DataTable ConvertSets(DataTable Table)
    {
      DataTable _table;
      Int16 _level;
      GeneralParam.Dictionary _general_param;
      DataTable _dt_scan_id_types;
      Currency _total_price;
      int _entrances;


      _total_price = 0;
      _entrances = 0;
      _general_param = GeneralParam.GetDictionary();
      _table = Table.Copy();
      foreach (System.Data.DataColumn col in _table.Columns) col.ReadOnly = false;
      _dt_scan_id_types = IdentificationTypes.GetIdentificationTypes(false);
      lbl_record_count.Text = _table.Rows.Count.ToString();

      foreach (DataRow _row in _table.Rows)
      {
        _level = Int16.Parse(_row["AC_HOLDER_LEVEL"].ToString());
        _row.BeginEdit();

        // level to str
        _row["AC_HOLDER_LEVEL"] = _general_param.GetString("PlayerTracking", "Level" + _level.ToString("00") + ".Name");

        //gender to str
        if (_row["CUT_GENDER"].ToString() == "1")
          _row["CUT_GENDER"] = Resource.String("STR_FRM_PLAYER_TRACKING_CB_GENDER_MALE_SHORT");
        else
          _row["CUT_GENDER"] = Resource.String("STR_FRM_PLAYER_TRACKING_CB_GENDER_FEMALE_SHORT");

        // document type to str
        _row["CUE_DOCUMENT_TYPE"] = IdentificationTypes.DocIdTypeString(Int16.Parse(_row["CUE_DOCUMENT_TYPE"].ToString()));

        if (_row["APH_PHOTO"] == DBNull.Value)  //no photo!!!
        {
          _row["APH_PHOTO"] = CashierStyle.Image.ImageToByteArray(Resources.ResourceImages.anonymous_user);
        }

        _total_price += Decimal.Parse(_row["SUM_CUE_TICKET_ENTRY_PRICE_PAID"].ToString());
        _row.EndEdit();

        _entrances = _entrances + Convert.ToInt16(_row[8].ToString());
      }

      lbl_entrance_count.Text = _entrances.ToString();
      lbl_price_total.Text = _total_price.ToString();
      //_table.Columns.Remove("SUM_CUE_TICKET_ENTRY_PRICE_PAID");

      return _table;
    } //ConvertSets

    //------------------------------------------------------------------------------
    // PURPOSE : Init and Show the form.
    //
    //  PARAMS :
    //      - INPUT :
    //          - ParentForm
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public void Show(Form Parent)
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_reception_visits_report", Log.Type.Message);

      Int32 _gaming_day;

      if (!Int32.TryParse(Misc.TodayOpening().ToString("yyyyMMdd"), out _gaming_day))
      {
        _gaming_day = 0;
      }

      try
      {
        InitControls();
        LoadVisitsGrid(_gaming_day, this.chk_players_on_site.Checked);
        InitOperationsDataGrid();
        form_yes_no.Show(Parent);
        this.ShowDialog(form_yes_no);
        form_yes_no.Hide();

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } // Show

    //------------------------------------------------------------------------------
    // PURPOSE : Initialize control resources (NLS strings, images, etc).
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public void InitializeControlResources()
    {
      // labels 
      this.FormTitle = Resource.String("STR_FRM_RECEPTION_VISITS_REPORT_01") + " " + Misc.TodayOpening().ToString("dd/MM/yyyy"); ;
      this.Text = this.FormTitle;
      this.btn_close.Text = Resource.String("STR_FRM_RECEPTION_VISITS_REPORT_13");
      this.lbl_total_records.Text = Resource.String("STR_FRM_RECEPTION_VISITS_REPORT_14") + " : ";
      this.lbl_total_price.Text = Resource.String("STR_FRM_RECEPTION_VISITS_REPORT_16") + " : ";
      this.dt_dateparam.DateText = Resource.String("STR_FRM_RECEPTION_VISITS_REPORT_17");
      this.lbl_total_entraces.Text = Resource.String("STR_FRM_RECEPTION_VISITS_REPORT_ENTANCES") + " : ";
      this.btn_refresh.Image = Images.Get32x32(Images.CashierImage.Recycle);
      this.lbl_record_count.Width = 100;
      this.lbl_price_total.Width = 100;
      this.chk_players_on_site.Text = Resource.String("STR_FRM_RECEPTION_VISITS_REPORT_PLAYERS_ON_SITE");

    } // InitializeControlResources

    //------------------------------------------------------------------------------
    // PURPOSE : Dispose yes_no form.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    new public void Dispose()
    {
      form_yes_no.Dispose();
      form_yes_no = null;


      base.Dispose();
    } // Dispose

    #endregion // Public Methods

    #region Events
    //------------------------------------------------------------------------------
    // PURPOSE : Handle the click on the Close/Cancel button.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private void btn_close_Click(object sender, EventArgs e)
    {
      // Close Button

      Misc.WriteLog("[FORM CLOSE] frm_reception_visits_report (close)", Log.Type.Message);
      this.Close();

      return;

    }

    /// <summary>
    /// btn_refresh click event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btn_refresh_Click(object sender, EventArgs e)
    {
      m_search_date = dt_dateparam.DateValue;
      LoadVisitsGrid(Int32.Parse(dt_dateparam.DateValue.ToString("yyyyMMdd")), this.chk_players_on_site.Checked);
    } //btn_refresh_Click


    private void dgv_last_operations_CellClick(object sender, DataGridViewCellEventArgs e)
    {

      if (e.ColumnIndex == 0 && dgv_last_operations.Rows[e.RowIndex].Cells[0].Value != null)
      {
        using (var ms = new MemoryStream((byte[])dgv_last_operations.Rows[e.RowIndex].Cells[0].Value))
        {

          frm_account_photo frm = new frm_account_photo(dgv_last_operations.Rows[e.RowIndex].Cells[1].Value.ToString(),
            Image.FromStream(ms));

          frm.ShowDialog();
        }
      }
    }


    private void dgv_last_operations_DoubleClick(object sender, EventArgs e)
    {
      try
      {
        m_customer_reception = new CustomerReception();
        foreach (DataGridViewRow _row in dgv_last_operations.SelectedRows)
        {
          m_customer_reception.AccountId = Convert.ToInt64(_row.Cells[GRID_COLUMN_ACCOUNT_ID].Value);
        }

        LoadEntry();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    }
    #endregion // Buttons

    private void dt_dateparam_OnDateChanged(object sender, EventArgs e)
    {
      if (!dt_dateparam.ValidateFormat())
      {
        btn_refresh.Enabled = false;
        dt_dateparam.DateError();
      }
      else
      {
        btn_refresh.Enabled = true;
        dt_dateparam.NoError();
      }
    }



  } // 

} // WSI.Cashier
