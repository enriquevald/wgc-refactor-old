﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems Ltd.
//------------------------------------------------------------------------------
//  
//   MODULE NAME : CustomerRecepti0n.cs
// 
//   DESCRIPTION : Manages Customer reception module.
// 
// REVISION HISTORY :
// 
// Date        Author   Description
// ----------- ------   ----------------------------------------------------------
// 19-Nov-2015 ADI      Initial Draft.
// 04-FEB-2016 FJC      Product Backlog Item 8427 - Task 8942:Visitas / Recepción: Modificaciones / Bugs equipo
// 26-JUL-2016 PDM      PBI 15444:Visitas / Recepción: MEJORAS - Cajero - Expandir formulario de resultados de búsqueda
// 20-SEP-2016 JRC      PBI 17677:Visitas / Recepción: MEJORAS II - Ref. 48 - Reportes recepción - separar columna Nombre (GUI)
// 11-OCT-2016 XGJ      PBI Bug 18716:Recepción-Cajero: Al buscar un cliente en el módulo de recepción por Documento sin la letra final, la búsqueda no otorga resultados
// 28-DEC-2016 DPC      Bug 22010:Reception: Last entries, names do not follow visible field configuration
using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using WSI.Common;
using WSI.Common.Utilities.Extensions;


namespace WSI.Cashier
{
  public class CustomerReception
  {
    #region Members Properties
    const int TrackNumberLength = 20;
    #endregion
    #region Public Properties

    public string CardCodBar
    {
      get;
      set;
    }
    public string Document
    {
      get;
      set;
    }
    public string CustomerName
    {
      get;
      set;
    }
    public string CustomerName2 { get; set; }
    public string CustomerLastname1
    {
      get;
      set;
    }
    public string CustomerLastname2
    {
      get;
      set;
    }
    public long AccountId
    {
      get;
      set;
    }
    public bool ScannerEnabled
    {
      get;
      set;
    }
    public string StringSearchSql
    {
      get;
      set;
    }

    public int Gender
    {
      get;
      set;
    }

    public DateTime BirthDate
    {
      get;
      set;
    }
    #endregion

    #region Public Methods


    //------------------------------------------------------------------------------
    // PURPOSE: Gets customers from accounts 
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT: DataTable Customers
    //
    // RETURNS:
    //    True if all the information in text controls is correct.
    //    False otherwise.
    // 
    //   NOTES:

    public bool GetCustomerAccounts(out DataSet AccountCustomers)
    {

      SqlCommand _cmd;
      string _string_sql;
      DataTable _accounts;

      AccountCustomers = new DataSet();
      _accounts = new DataTable();
      _cmd = new SqlCommand();

      try
      {

        _string_sql = string.Format("{0}{1}", GetStringSQLAccounts(), GetStringParametersAccount());
        _string_sql += "{0} order by AC_ACCOUNT_ID";

        StringSearchSql = _string_sql;

        _string_sql = _string_sql.Replace("@N", (GeneralParam.GetBoolean("Reception", "ResultsView.Mode", false) ?
                                                  GeneralParam.GetInt32("Reception", "ResultsView.Extended.TopResults", 50, 1, 100) :
                                                  GeneralParam.GetInt32("Reception", "ResultsView.Collapsed.TopResults", 4, 1, 100)).ToString());

        using (DB_TRX _db_trx = new DB_TRX())
        {
          //Accounts table
          using (SqlDataAdapter _da = new SqlDataAdapter(_cmd))
          {
            _accounts.TableName = "accounts";
            _cmd.CommandText = string.Format(_string_sql, "");
            _cmd.Connection = _db_trx.SqlTransaction.Connection;
            _cmd.Transaction = _db_trx.SqlTransaction;


            _da.Fill(_accounts);
            if (_accounts.Rows.Count > 0)
            {
              AccountCustomers.Tables.Add(_accounts);
            }
          }

          if (AccountCustomers.Tables.Count > 0)
          {
            return true;
          }

        }
      }
      catch (Exception _ex)
      {
        Log.Error("CustomerReception.GetCostumers: Error executing procedure ");
        Log.Exception(_ex);
        throw;
      }

      finally
      {
        ClearParameters();
      }
      return false;
    }//GetCostumers

    public bool IsCardMobileBank()
    {

      SqlCommand _cmd;
      StringBuilder _string_sql;
      int _result;
      Object _obj;

      _string_sql = new StringBuilder();
      _cmd = new SqlCommand();
      _result = -1;
      _string_sql.Append("SELECT 1 ");
      _string_sql.Append("FROM MOBILE_BANKS ");
      _string_sql.Append("WHERE MB_TRACK_DATA = ");
      _string_sql.Append("dbo.TrackDataToInternal('");
      _string_sql.Append(String.Format("{0}')", CardCodBar));
      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {

          _cmd.CommandText = _string_sql.ToString();
          _cmd.Connection = _db_trx.SqlTransaction.Connection;
          _cmd.Transaction = _db_trx.SqlTransaction;

          _obj = _cmd.ExecuteScalar();

          if (_obj != null && _obj != DBNull.Value)
          {
            _result = (int)_obj;
          }

          if (_result == 1)
          {
            return true;
          }

        }
      }
      catch (Exception _ex)
      {
        Log.Error("CustomerReception.GetCostumers: Error executing procedure ");
        Log.Exception(_ex);

      }

      finally
      {
        ClearParameters();
      }
      return false;
    }//GetCostumers
    #endregion

    #region Private Methods

    /// <summary>
    /// Get AccountId 
    /// </summary>
    /// <param name="VoucherSequence"></param>
    /// <param name="DataSetAccounts"></param>
    /// <returns>TRUE if datareader gets records</returns>
    public bool GetAccountByVoucher(long VoucherSequence, out DataSet DataSetAccounts)
    {
      SqlCommand _cmd;
      StringBuilder _string_sql;
      DataTable _dt_accounts;

      DataSetAccounts = new DataSet();
      _cmd = new SqlCommand();
      _string_sql = new StringBuilder();
      _dt_accounts = new DataTable();

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _string_sql.Append("    SELECT   AC_ACCOUNT_ID                                        ");
          _string_sql.Append("           , dbo.TrackDataToExternal(AC_TRACK_DATA) AS TRACK_DATA ");

          _string_sql.Append("           , AC_HOLDER_NAME3                                              ");

          _string_sql.Append("           , CASE WHEN AC_HOLDER_NAME1 IS NULL THEN '' ELSE AC_HOLDER_NAME1 END                           ");
          _string_sql.Append("             +                                                                                            ");
          _string_sql.Append("             CASE WHEN AC_HOLDER_NAME2 IS NOT NULL AND AC_HOLDER_NAME1 IS NOT NULL THEN ' ' ELSE '' END   ");
          _string_sql.Append("             +                                                                                            ");
          _string_sql.Append("             CASE WHEN AC_HOLDER_NAME2 IS NULL THEN '' ELSE AC_HOLDER_NAME2 END AS AC_HOLDER_NAME12       ");


          _string_sql.Append("           , AC_HOLDER_ID                                         ");
          _string_sql.Append("           , AC_HOLDER_BIRTH_DATE                                 ");
          _string_sql.Append("      FROM   ACCOUNTS                                             ");
          _string_sql.Append("INNER JOIN (                                                      ");
          _string_sql.Append("            SELECT   CUSTOMER_VISITS.CUT_CUSTOMER_ID              ");
          _string_sql.Append("              FROM   CUSTOMER_ENTRANCES                           ");
          _string_sql.Append("        INNER JOIN   CUSTOMER_VISITS                              ");
          _string_sql.Append("                ON   CUE_VISIT_ID = CUT_VISIT_ID                  ");
          _string_sql.Append("        INNER JOIN   CASHIER_TERMINALS                            ");
          _string_sql.Append("                ON   CUE_CASHIER_TERMINAL_ID = CT_CASHIER_ID      ");
          _string_sql.Append("             WHERE   CUE_VOUCHER_SEQUENCE = @VoucherSequence      ");
          _string_sql.Append("          GROUP BY   CUSTOMER_VISITS.CUT_CUSTOMER_ID) CVA         ");
          _string_sql.Append("                ON   AC_ACCOUNT_ID = CVA.CUT_CUSTOMER_ID          ");

          _cmd.CommandText = string.Format(_string_sql.ToString());
          _cmd.Connection = _db_trx.SqlTransaction.Connection;
          _cmd.Transaction = _db_trx.SqlTransaction;
          _cmd.Parameters.Add("@VoucherSequence", SqlDbType.BigInt).Value = VoucherSequence;

          using (SqlDataAdapter _da = new SqlDataAdapter(_cmd))
          {
            _dt_accounts.TableName = "Accounts";
            _cmd.CommandText = string.Format(_string_sql.ToString());
            _cmd.Connection = _db_trx.SqlTransaction.Connection;
            _cmd.Transaction = _db_trx.SqlTransaction;

            _da.Fill(_dt_accounts);

            if (_dt_accounts.Rows.Count > 0)
            {
              DataSetAccounts.Tables.Add(_dt_accounts);

              return true;
            }
          }
        }

        return false;
      }
      catch (Exception _ex)
      {
        Log.Error("CustomerReception.GetAccountByVoucher: Error executing procedure");
        Log.Exception(_ex);
      }

      return false;
    }// GetAccountByVoucher




    //------------------------------------------------------------------------------
    // PURPOSE: Gets parameters WHERE for string SQL
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT: 
    //
    // RETURNS:
    //    
    //    StringBuilder with the string WHERE configured 
    // 
    //   NOTES:
    private string GetStringParametersAccount()
    {
      string _string_where = "";
      try
      {
        if (AccountId > 0)
        {
          _string_where =
                           string.Format(" WHERE AC_ACCOUNT_ID =  {0} ", AccountId);
          return _string_where;
        }

        if (!string.IsNullOrEmpty(CardCodBar) && CardCodBar.Length == TrackNumberLength)
        {
          _string_where =
                           string.Format(" WHERE AC_TRACK_DATA = dbo.TrackDataToInternal( '{0}') ", CardCodBar);
          return _string_where;

        }

        if (!string.IsNullOrEmpty(CustomerName))
        {
          _string_where += (_string_where.IsNullOrWhiteSpace() ? " WHERE (" : " AND ") +
                           string.Format("AC_HOLDER_NAME3 LIKE '%{0}%' ", CustomerName);
        }
        if (!string.IsNullOrEmpty(CustomerName2))
        {
          _string_where += (_string_where.IsNullOrWhiteSpace() ? " WHERE (" : " AND ") +
                           string.Format("AC_HOLDER_NAME4 LIKE '%{0}%' ", CustomerName2);
        }

        if (!string.IsNullOrEmpty(CustomerLastname1))
        {
          _string_where += (_string_where.IsNullOrWhiteSpace() ? " WHERE (" : " AND ") +
                           string.Format("AC_HOLDER_NAME1 LIKE '%{0}%' ", CustomerLastname1);
        }

        if (!string.IsNullOrEmpty(CustomerLastname2))
        {
          _string_where += (_string_where.IsNullOrWhiteSpace() ? " WHERE (" : " AND ") +
                           string.Format("AC_HOLDER_NAME2 LIKE '%{0}%' ", CustomerLastname2);
        }

        if (!BirthDate.Equals(DateTime.MinValue))
        {
          _string_where += (_string_where.IsNullOrWhiteSpace() ? " WHERE (" : " AND ") +
                           string.Format("AC_HOLDER_BIRTH_DATE = '{0}' ", BirthDate.ToString("yyyy-MM-dd"));
        }

        if (Gender > 0)
        {
          _string_where += (_string_where.IsNullOrWhiteSpace() ? " WHERE (" : " AND ") +
                           string.Format("AC_HOLDER_GENDER =  {0} ", Gender);
        }

        if (!string.IsNullOrEmpty(Document))
        {

          _string_where += (_string_where.IsNullOrWhiteSpace() ? " WHERE (" : ") OR (") +
                                       string.Format("AC_HOLDER_ID LIKE '%{0}%' or (curd_data='{0}' and ((curd_type> 5999 and curd_type<7000) or curd_type=4))", Document);

          //                         string.Format("AC_HOLDER_ID LIKE '%{0}%' or ac_account_id in (select cr.cur_customer_id from customer_records cr join customer_record_details crd on crd.curd_record_id = cr.cur_record_id and curd_type> 5999 and curd_type<7000 and curd_data= '{0}')", Document);

        }
        if (!_string_where.IsNullOrWhiteSpace())
        {
          _string_where += ")";
        }

      }



      catch (Exception _ex)
      {
        Log.Error("Error en procedimiento CustomerReception.GetStringParameters");
        Log.Exception(_ex);
      }

      return _string_where;
    } //GetStringParametersAccount

    //------------------------------------------------------------------------------
    // PURPOSE: Set string SQL to Account Table
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT: 
    //
    // RETURNS:
    //    
    //    StringBuilder with the string SQL
    // 
    //   NOTES:

    private StringBuilder GetStringSQLAccounts()
    {
      StringBuilder _string_sql;
      _string_sql = new StringBuilder();
      Boolean _visible_name1;
      Boolean _visible_name2;
      Boolean _visible_name3;
      Boolean _visible_name4;
      string _str_Key_GP;
      
      try
      {
        _str_Key_GP = "Account.VisibleField";

        _visible_name1 = GeneralParam.GetBoolean(_str_Key_GP, "Name1", false);
        _visible_name2 = GeneralParam.GetBoolean(_str_Key_GP, "Name2", false);
        _visible_name3 = GeneralParam.GetBoolean(_str_Key_GP, "Name3", false);
        _visible_name4 = GeneralParam.GetBoolean(_str_Key_GP, "Name4", false);

        _string_sql.AppendLine(" SELECT distinct top @N AC_ACCOUNT_ID ");
        _string_sql.AppendLine("          , dbo.TrackDataToExternal(AC_TRACK_DATA) AS TRACK_DATA                          ");

        if (_visible_name3 && _visible_name4)
        {
          _string_sql.AppendLine("          , AC_HOLDER_NAME3  + ' ' +  AC_HOLDER_NAME4 AS AC_HOLDER_NAME3                ");
        }
        else if (_visible_name3 && !_visible_name4)
        {
          _string_sql.AppendLine("          , AC_HOLDER_NAME3                                                             ");
        }
        else if (!_visible_name3 && _visible_name4)
        {
          _string_sql.AppendLine("          , AC_HOLDER_NAME4 AS AC_HOLDER_NAME3                                          ");
        }
        else
        {
          _string_sql.AppendLine("          , '' AS AC_HOLDER_NAME3                                                       ");
        }

        if (_visible_name1 && _visible_name2)
        {
          _string_sql.AppendLine("          , AC_HOLDER_NAME1  + ' ' +  AC_HOLDER_NAME2 AS AC_HOLDER_NAME12               ");
        }
        else if (_visible_name1 && !_visible_name2)
        {
          _string_sql.AppendLine("          , AC_HOLDER_NAME1 AS AC_HOLDER_NAME12                                         ");
        }
        else if (!_visible_name1 && _visible_name2)
        {
          _string_sql.AppendLine("          , AC_HOLDER_NAME2 AS AC_HOLDER_NAME12                                         ");
        }
        else
        {
          _string_sql.AppendLine("          , '' AS AC_HOLDER_NAME12                                                      ");
        }

        _string_sql.AppendLine("          , AC_HOLDER_ID                                                                  ");
        _string_sql.AppendLine("          , AC_HOLDER_BIRTH_DATE                                                          ");
        _string_sql.AppendLine("          , AC_USER_TYPE                                                                  ");
        _string_sql.AppendLine("     FROM   accounts AS accounts                                                          ");
        _string_sql.AppendLine("LEFT OUTER JOIN customer_records AS cr ON accounts.ac_account_id = cr.cur_customer_id     ");
        _string_sql.AppendLine("LEFT OUTER JOIN customer_record_details AS crd ON cr.cur_record_id = crd.curd_record_id   ");
      }

      catch (Exception _ex)
      {
        Log.Error("Error en procedimiento CustomerReception.GetStringSQL");
        Log.Exception(_ex);
      }
      return _string_sql;
    }//GetStringSQLAccounts

    //------------------------------------------------------------------------------
    // PURPOSE: Clears Parameters after query
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT: 
    //
    // RETURNS:
    //    
    //    StringBuilder with the string SQL
    // 
    //   NOTES:
    private void ClearParameters()
    {
      //this.CustomerName = String.Empty;
      //this.Document = String.Empty;
      //this.CardCodBar = String.Empty;

    }//ClearParameters


    /// <summary>
    /// Get five last entries in reception
    /// </summary>
    /// <param name="LastEntries"></param>
    /// <returns>TRUE if datatable gets records</returns>
    public bool GetLastEntries(out DataTable LastEntries)
    {

      SqlCommand _cmd;
      StringBuilder _string_sql;

      LastEntries = new DataTable();
      try
      {
        _cmd = new SqlCommand();

        _string_sql = new StringBuilder();

        using (DB_TRX _db_trx = new DB_TRX())
        {

          _string_sql.Append("SELECT  TOP (4)                                                              ");
          _string_sql.Append("               AC_ACCOUNT_ID,                                                ");
          _string_sql.Append("               AC_HOLDER_NAME3,                                              ");

          _string_sql.Append("               CASE WHEN AC_HOLDER_NAME1 IS NULL THEN '' ELSE AC_HOLDER_NAME1 END                           ");
          _string_sql.Append("               +                                                                                            ");
          _string_sql.Append("               CASE WHEN AC_HOLDER_NAME2 IS NOT NULL AND AC_HOLDER_NAME1 IS NOT NULL THEN ' ' ELSE '' END   ");
          _string_sql.Append("               +                                                                                            ");
          _string_sql.Append("               CASE WHEN AC_HOLDER_NAME2 IS NULL THEN '' ELSE AC_HOLDER_NAME2 END AS AC_HOLDER_NAME12,       ");


          _string_sql.Append("               CUE_ENTRANCE_DATETIME,                                        ");
          _string_sql.Append("               CUE_DOCUMENT_NUMBER,                                          ");
          _string_sql.Append("               CT_NAME                                                       ");
          _string_sql.Append("FROM                                                                         ");
          _string_sql.Append("CUSTOMER_ENTRANCES INNER JOIN CUSTOMER_VISITS                                ");
          _string_sql.Append("ON CUE_VISIT_ID = CUT_VISIT_ID                                               ");
          _string_sql.Append("INNER JOIN ACCOUNTS ON                                                       ");
          _string_sql.Append("CUT_CUSTOMER_ID = AC_ACCOUNT_ID                                              ");
          _string_sql.Append("INNER JOIN CASHIER_TERMINALS ON                                              ");
          _string_sql.Append("CUE_CASHIER_TERMINAL_ID = CT_CASHIER_ID                                      ");
          _string_sql.Append("ORDER BY CUE_ENTRANCE_DATETIME DESC                                          ");

          using (SqlDataAdapter _da = new SqlDataAdapter(_cmd))
          {
            LastEntries.TableName = "Accounts";
            _cmd.CommandText = string.Format(_string_sql.ToString());
            _cmd.Connection = _db_trx.SqlTransaction.Connection;
            _cmd.Transaction = _db_trx.SqlTransaction;

            _da.Fill(LastEntries);


            if (LastEntries.Rows.Count > 0)
            {
              return true;
            }

          }

        }
      }
      catch (Exception _ex)
      {
        Log.Error("Error en procedimiento CustomerReception.GetLastEntries");
        Log.Exception(_ex);
      }
      return false;
    }





    #endregion

  }
}
