﻿using WSI.Cashier.Controls;

namespace WSI.Cashier
{
    partial class SelectDevice
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="Disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool Disposing)
        {
            if (Disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(Disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
      this.lstDevices = new WSI.Cashier.Controls.uc_round_list_box();
      this.btnSelecteDevice = new WSI.Cashier.Controls.uc_round_button();
      this.pnl_data.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.Controls.Add(this.btnSelecteDevice);
      this.pnl_data.Controls.Add(this.lstDevices);
      this.pnl_data.Size = new System.Drawing.Size(506, 354);
      // 
      // lstDevices
      // 
      this.lstDevices.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.lstDevices.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.lstDevices.CornerRadius = 5;
      this.lstDevices.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lstDevices.DrawMode = System.Windows.Forms.DrawMode.Normal;
      this.lstDevices.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lstDevices.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.lstDevices.FormattingEnabled = true;
      this.lstDevices.ItemHeight = 18;
      this.lstDevices.Location = new System.Drawing.Point(0, 0);
      this.lstDevices.Name = "lstDevices";
      this.lstDevices.SelectedIndex = -1;
      this.lstDevices.SelectedItem = null;
      this.lstDevices.Size = new System.Drawing.Size(506, 354);
      this.lstDevices.Sorted = false;
      this.lstDevices.Style = WSI.Cashier.Controls.uc_round_list_box.RoundListBoxStyle.COMBOBOX_LIST_CUSTOM;
      this.lstDevices.TabIndex = 0;
      this.lstDevices.TopIndex = 0;
      this.lstDevices.DoubleClick += new System.EventHandler(this.listBox1_DoubleClick);
      // 
      // btnSelecteDevice
      // 
      this.btnSelecteDevice.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btnSelecteDevice.DialogResult = System.Windows.Forms.DialogResult.OK;
      this.btnSelecteDevice.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.btnSelecteDevice.FlatAppearance.BorderSize = 0;
      this.btnSelecteDevice.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btnSelecteDevice.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btnSelecteDevice.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btnSelecteDevice.Image = null;
      this.btnSelecteDevice.IsSelected = false;
      this.btnSelecteDevice.Location = new System.Drawing.Point(0, 302);
      this.btnSelecteDevice.Name = "btnSelecteDevice";
      this.btnSelecteDevice.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btnSelecteDevice.Size = new System.Drawing.Size(506, 52);
      this.btnSelecteDevice.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btnSelecteDevice.TabIndex = 1;
      this.btnSelecteDevice.Text = "STRING \'STR_FORMAT_GENERAL_BUTTONS_SELECT\' NOT FOUND.";
      this.btnSelecteDevice.UseVisualStyleBackColor = false;
      // 
      // SelectDevice
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(506, 409);
      this.Name = "SelectDevice";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Style = WSI.Cashier.Controls.frm_base.BaseFormStyle.PRINCIPAL;
      this.Text = "String \'STR_SELECT_DEVICE\' not found.";
      this.Load += new System.EventHandler(this.SelectDevice_Load);
      this.pnl_data.ResumeLayout(false);
      this.ResumeLayout(false);

        }

        #endregion

        private uc_round_list_box lstDevices;
        private uc_round_button btnSelecteDevice;
    }
}