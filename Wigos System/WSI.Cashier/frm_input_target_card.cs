//------------------------------------------------------------------------------
// Copyright � 2014 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_input_target_card.cs
// 
//   DESCRIPTION: Implements the following classes:
//
//        AUTHOR: Ram�n Moncl�s
// 
// CREATION DATE: 21-DEC-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 31-JAN-2014 RMS    First release.
// 23-DEC-2014 RCI    Fixed Bug WIG-1890: Coupon Cover promotions are not cancelled (and must be!)
//------------------------------------------------------------------------------
//

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WSI.Common;
using System.Collections;
using System.Data.SqlClient;

namespace WSI.Cashier
{
  public partial class frm_input_target_card : Controls.frm_base
  {
    #region class attributes
    CardData m_origin_card_data;
    Form m_parent_form;
    Accounts.TRANSFER_CREDIT_RESULT m_transfer_result;
    #endregion

    public frm_input_target_card(CardData Card)
    {
      InitializeComponent();
      m_origin_card_data = Card;
      base.PerformOnExitClicked += OnCloseWindow;

      txt_amount.Text = m_origin_card_data.CurrentBalance.ToString();
      InitializeResources();
      this.Shown += new System.EventHandler(this.frm_input_target_card_Shown);
    }

    private void OnCloseWindow()
    {
      m_transfer_result = Accounts.TRANSFER_CREDIT_RESULT.TRANSFER_OK;
    }

    #region private functions
    private void InitializeResources()
    {
      this.FormTitle = Resource.String("STR_INPUT_TARGET_CARD_BUTTON");
      this.lbl_input_target_card.Text = Resource.String("STR_INPUT_TARGET_CARD_INSERT_TARGET");
      this.btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");
      uc_card_reader1.ShowMessageBottom();
      
    }
    #endregion

    #region events

    private void uc_card_reader1_OnTrackNumberReadEvent(string TrackNumber, ref bool IsValid)
    {
      if (IsValid)
      {
        m_transfer_result = DB_TransferAllRedeemableCredit(m_origin_card_data, TrackNumber);

        this.Dispose();
      }
    }

    private Accounts.TRANSFER_CREDIT_RESULT DB_TransferAllRedeemableCredit(CardData SourceCardData,
                                                                           String DestinationExternalTrackData)
    {
      CardData _target_card_data;
      ArrayList _vouchers_to_print = new ArrayList();
      Accounts.TRANSFER_CREDIT_RESULT _result;
      String _message;
      string[] _message_params = { "", "", "" };

      try
      {
        _result = Accounts.CanTransferCredit(SourceCardData);

        if (_result != Accounts.TRANSFER_CREDIT_RESULT.TRANSFER_OK
          && _result != Accounts.TRANSFER_CREDIT_RESULT.ERROR_SOURCE_ACCOUNT_HAVE_PROMO)
        {
          return _result;
        }

        using (DB_TRX _db_trx = new DB_TRX())
        {
          _target_card_data = new CardData();

          if (!CardData.DB_CardGetPersonalData(DestinationExternalTrackData, _target_card_data, _db_trx.SqlTransaction))
          {
            return Accounts.TRANSFER_CREDIT_RESULT.ERROR;
          }
        }

        if (SourceCardData.AccountId == _target_card_data.AccountId)
        {
          return Accounts.TRANSFER_CREDIT_RESULT.ERROR_TARGET_ACCOUNT_IS_SOURCE_ACCOUNT;
        }

        _message_params[0] = SourceCardData.PlayerTracking.HolderName;
        _message_params[1] = _target_card_data.PlayerTracking.HolderName;

        if (_message_params[0] == string.Empty)
        {
          _message_params[0] = Resource.String("STR_UC_CARD_CARD_HOLDER_ANONYMOUS").ToUpper();
        }

        if (_message_params[1] == string.Empty)
        {
          _message_params[1] = Resource.String("STR_UC_CARD_CARD_HOLDER_ANONYMOUS").ToUpper();
        }

        if (_result == Accounts.TRANSFER_CREDIT_RESULT.ERROR_SOURCE_ACCOUNT_HAVE_PROMO
          && SourceCardData.AccountBalance.TotalNotRedeemable > 0)
        {
          _message_params[2] = String.Format(Accounts.TranslateTransferCreditResultToMessage(_result),
                                             (Currency)SourceCardData.AccountBalance.TotalNotRedeemable);
        }

        _message = Resource.String("STR_INPUT_TARGET_CARD_CONFIRM_POP_UP_TEXT", _message_params);
        _message = _message.Replace("\\r\\n", "\r\n");

        if (frm_message.Show(_message, Resource.String("STR_INPUT_TARGET_CARD_BUTTON"), MessageBoxButtons.YesNo,
                             Images.CashierImage.Question, m_parent_form) != DialogResult.OK)
        {
          return Accounts.TRANSFER_CREDIT_RESULT.TRANSFER_OK;
        }

        using (DB_TRX _db_trx = new DB_TRX())
        {
          _result = Accounts.DB_TransferAllRedeemableCredit(SourceCardData, _target_card_data,
                                                            Cashier.CashierSessionInfo(),
                                                            out _vouchers_to_print, _db_trx.SqlTransaction);

          if (_result == Accounts.TRANSFER_CREDIT_RESULT.TRANSFER_OK
           || _result == Accounts.TRANSFER_CREDIT_RESULT.ERROR_SOURCE_ACCOUNT_HAVE_PROMO)
          {
            _db_trx.Commit();
          }
        }

        if (_result == Accounts.TRANSFER_CREDIT_RESULT.TRANSFER_OK
         || _result == Accounts.TRANSFER_CREDIT_RESULT.ERROR_SOURCE_ACCOUNT_HAVE_PROMO)
        {
          VoucherPrint.Print(_vouchers_to_print);
        }

        return _result;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return Accounts.TRANSFER_CREDIT_RESULT.ERROR_UNKNOWN;
    }

    private void btn_cancel_Click(object sender, EventArgs e)
    {
      m_transfer_result = Accounts.TRANSFER_CREDIT_RESULT.TRANSFER_OK;
      this.Dispose();
    }

    private void frm_input_target_card_Shown(object sender, EventArgs e)
    {
      this.uc_card_reader1.Focus();
    }
    #endregion

    #region public methods
    //------------------------------------------------------------------------------
    // PURPOSE : Init and Show the form.
    //
    //  PARAMS :
    //      - INPUT : ParentForm
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public Accounts.TRANSFER_CREDIT_RESULT Show(Form ParentForm)
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_input_target_card", Log.Type.Message);

      m_parent_form = ParentForm;

      this.ShowDialog(ParentForm);

      return m_transfer_result;
    }


    #endregion

    private void pnl_data_Paint(object sender, PaintEventArgs e)
    {

    }
  }
}