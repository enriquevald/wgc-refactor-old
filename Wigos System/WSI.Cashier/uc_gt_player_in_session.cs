//------------------------------------------------------------------------------
// Copyright � 2007-2014 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_gt_card_reader.cs
// 
//   DESCRIPTION: Implements the uc_gt_card_reader user control
//                Class: uc_gt_card_reader
//
//        AUTHOR: Jos� Mart�nez
// 
// CREATION DATE: 05-JUN-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 05-JUN-2014 JML     First release.
// 11-JUL-2014 AMF     Regionalization
// 25-JUL-2014 DHA     Fixed Bug #WIG-1119: set SetLastAction for player tracking gaming tables
// 12-AUG-2014 DCS     Add aditional permission for edit points in player tracking
// 26-SEP-2014 DHA     Fixed Bug #WIG-1311: corrected format NLS
// 22-OCT-2014 JML     Fixed Bug #WIG-1558: The modified points are lost when loading play session
// 28-OCT-2014 DCS     Fixed Bug #WIG-1586: Awarded points was lost when user was paused because inserted in another table and then close sesion from GUI
// 28-OCT-2014 DHA     Fixed Bug #WIG-1587: Disabled awraded manual points when external loyalty program is enabled
// 13-APR-2016 DHA     Product Backlog Item 9950: added chips operation (sales and purchases)
// 11-JUL-2016 JML     Product Backlog Item 15065: Gaming tables (Fase 4): Player tracking. Multicurrency
// 21-SEP-2016 FOS     Fixed Bug 17882: Tables: Exception when exist a gamingtable without seats in design
// 22-SEP-2016 LTC     Product Backlog Item 17461: Gamingtables 46 - Cash desk draw for gamblign tables: Chips purchase
// 29-SEP-2016 FOS     Fixed Bug 17813: PlayerSeat IsoCode selected
// 28-MAR-2017 RAB     PBI 25976: MES10 Ticket validation - Buy-in
// 03-APR-2017 RAB     PBI 26401: MES10 Ticket validation - Ticket status change to cancelled
// 04-ABR-2017 JML     PBI 26401:MES10 Ticket validation - Ticket status change to cancelled
// 06-JUN-2017 DHA     PBI 27760:WIGOS-257: MES21 - Gaming table chair assignment with card selection
// 10-JUL-2017 DHA     PBI 28610:WIGOS-3458 - Show Customer Image in a Pop up - Accounts
// 04-SEP-2017 JML     PBI 29533: WIGOS-3413 MES16 - Points assignment: gaming table proposal
// 07-SEP-2017 DHA     Bug 29632:WIGOS-4972 Gaming tables: points can be assigned to anonymous user on player tracking.
// 21-SEP-2017 RAB     PBI 29318: WIGOS-3699: MES13 Specific alarms: Gaming table bets over
// 29-SEP-2017 RAB     PBI 29841: WIGOS-3756 MES13 Specific alarms - Gaming table bet deviation
// 24-OCT-2017 RAB     Bug 30378:WIGOS-6018 Gaming tables - Points assignment: It is allowed to assign points to Anonymous player in cashier
// 06-NOV-2017 RAB     Bug 30569:WIGOS-6208 Gambling tables alarms: Bets by customer alarm is triggered by duplicated
// 06-NOV-2017 RAB     Bug 30537:WIGOS-6337 Gaming tables point assignment: Computed and assigned points are calculated and saved to anonymous user in gaming table session editor
// 20-APR-2018 RGR     Bug 32393:WIGOS-6613 Gambling tables: the currency selection comboBox is automatically opened when the dealer copy ticket is validated on linked gaming table
// 12-APR-2018 FOS     WIGOS-10063: CLONE - Cashier crashes when sitting a player in a table
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Threading;
using WSI.Common;
using WSI.Cashier.TITO;
using WSI.Cashier.MVC.View.PlayerEditDetails;
using WSI.Cashier.MVC.Controller.PlayerEditDetails;

namespace WSI.Cashier
{
  public partial class uc_gt_player_in_session : UserControl
  {
    public enum ButtonClicked
    {
      Unknown = -1,
      Close = 0,
      PlayPause = 1,
      Swap = 2
    }

    public enum TextBoxSelected
    {
      Unknown = -1,
      Played_Current = 0,
      Points_Won = 1,
      Chips_In = 2,
      Chips_Out = 3,
      Chips_Sell = 4,
      Btn_Buy_In = 5
    }

    #region Class Atributes

    const Int32 WAIT_FOR_EDIT = 4;

    private frm_container m_parent;
    private Seat m_seat;
    private GamingTableBusinessLogic.GT_SHOW_MESSAGE m_show_msg_when_validate_values;
    private Currency m_table_max_bet;
    private Currency m_table_min_bet;
    private Int32 m_table_idle_plays;

    private Int32 m_tick_last_change = 0;

    private Color m_msg_back_color = Color.FromArgb(255, 255, 128);

    private Boolean m_go_back = true;

    public delegate void ButtonClick(Seat Seat, ButtonClicked ButtonClicked);
    public event ButtonClick ButtonClickedInPlayerInSession;

    public delegate void TexBoxEnter(Decimal Amount, TextBoxSelected TextBoxEnter, Boolean GoBack);
    public event TexBoxEnter TextBoxEnterEvent;

    public delegate void ValueChanged(Seat Seat);
    public event ValueChanged ValueChangedInSession;

    public delegate void ValuePlayerIsoCodeChanged(Seat Seat, String OldIsoCode, String NewIsoCode);
    public event ValuePlayerIsoCodeChanged ValuePlayerIsoCodeChangedInSession;

    private System.Globalization.NumberFormatInfo m_nfi;

    private static frm_yesno form_yes_no;
    frm_amount_input form_credit;

    private Boolean m_gaming_table_paused;

    // When points edited by user in away status any calculated field must change the value since the status change to run
    private Boolean m_points_edited_by_user;

    private Int32 m_elp_mode;

    private Boolean m_is_TITO_mode;

    private String m_national_currency;

    private Boolean m_exist_photo = false;

    #endregion  // Class Atributes

    #region Properties

    public IDScanner.IDScanner IDScanner { get; set; }

    public Seat Seat
    {
      get
      {
        return m_seat;
      }
      set
      {
        if (this.Enabled)
        {
          m_seat = value;
          FillPlayerData();
          CalculateFields();
          this.Invalidate();
        }
      }
    }
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    public Currency PlayedCurrent
    {
      get
      {
        Decimal _value;

        if (!Decimal.TryParse(tb_current_bet.Text.Replace(m_nfi.CurrencySymbol, ""), out _value))
        {
          _value = 0;
        }
        return _value;
      }
      set
      {
        m_seat.PlaySession.CurrentBet = value;
        
        tb_current_bet.Text = m_seat.PlaySession.CurrentBet.ToString(false);
        m_seat.HasPendingChanges = true;

        CalculateFields();
        ValidateCurrentBet(true, true);

        m_seat.PlaySession.LastPlayedAmount = m_seat.PlaySession.PlayedAmount;
        m_seat.PlaySession.LastPlaysCount = m_seat.PlaySession.PlaysCount;
        
        GenerateAlarmFromBetsOverHand();
        GenerateAlarmFromBetsVariation();

        UpdateRefreshFields(tb_current_bet);
      }
    }
    public Decimal PointsWon
    {
      get
      {
        Decimal _value = 0;

        if (!Decimal.TryParse(tb_points_won.Text, out _value))
        {
          _value = 0;
        }
        return _value;
      }
      set
      {
        if (m_seat != null && tb_points_won.Text != value.ToString())
        {
          m_seat.PlaySession.AwardedPoints = value;
          tb_points_won.Text = value.ToString();
          m_seat.HasPendingChanges = true;

          ShowValidateMsg(Resource.String("STR_CASHIER_GAMING_TABLE_AWARED_POINTS_INFO_MSG"), Resource.String("STR_APP_GEN_MSG_WARNING"), MessageBoxButtons.YesNo, MessageBoxIcon.Question, Cashier.MainForm);

          UpdateRefreshFields(tb_points_won);
        }
      }
    }
    public Currency ChipsIn
    {
      get
      {
        Decimal _value = 0;

        if (!Decimal.TryParse(tb_chips_in.Text, out _value))
        {
          _value = 0;
        }
        return _value;
      }
      set
      {
        if (m_seat != null && tb_chips_in.Text != value.ToString(false))
        {
          m_seat.PlaySession.ChipsIn = value;
          tb_chips_in.Text = value.ToString(false);
          m_seat.HasPendingChanges = true;

          CalculateFields();
          UpdateRefreshFields(tb_chips_in);
        }
      }
    }
    public Currency ChipsOut
    {
      get
      {
        Decimal _value = 0;

        if (!Decimal.TryParse(tb_chips_out.Text, out _value))
        {
          _value = 0;
        }
        return _value;
      }
      set
      {
        if (m_seat != null && tb_chips_out.Text != value.ToString(false))
        {
          m_seat.PlaySession.ChipsOut = value;
          tb_chips_out.Text = value.ToString(false);
          m_seat.HasPendingChanges = true;

          CalculateFields();
          UpdateRefreshFields(tb_chips_out);
        }
      }
    }
    public Decimal ChipsSell
    {
      get
      {
        Decimal _value = 0;

        if (!Decimal.TryParse(lbl_chips_sale_accumulate.Text, out _value))
        {
          _value = 0;
        }
        return _value;
      }
      set
      {
        if (m_seat != null)
        {
          m_seat.PlaySession.TotalSellChips += value;
          lbl_chips_sale_accumulate.Text = m_seat.PlaySession.TotalSellChips.ToString(false);

          CalculateFields();

          TextBoxEnterEvent(0, TextBoxSelected.Unknown, m_go_back);
          ValueChangedInSession(m_seat);
        }
      }
    }
    public Boolean GamingTablePaused
    {
      set
      {
        if (value != m_gaming_table_paused)
        {
          m_gaming_table_paused = value;
          SetControls();
        }
      }
    }
    public Currency TableMaxBet
    {
      get
      {
        return m_table_max_bet;
      }
      set
      {
        m_table_max_bet = value;

        CalculateFields();

        if (m_seat != null && m_seat.HasPendingChanges)
        {
          SaveChanges();
          ValueChangedInSession(m_seat);
        }
      }
    }
    public Currency TableMinBet
    {
      get
      {
        return m_table_min_bet;
      }
      set
      {
        m_table_min_bet = value;

        CalculateFields();

        if (m_seat != null && m_seat.HasPendingChanges)
        {
          SaveChanges();
          ValueChangedInSession(m_seat);
        }
      }
    }
    public Int32 TableIdlePlays
    {
      get
      {
        return m_table_idle_plays;
      }
      set
      {
        m_table_idle_plays = value;
      }
    }
    #endregion Properties

    #region Constructor

    /// <summary>
    /// Constructor
    /// </summary>
    public uc_gt_player_in_session()
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[UC LOAD] uc_gt_player_in_session", Log.Type.Message);

      InitializeComponent();

      InitControls();
      InitResources();

      form_yes_no = new frm_yesno();

      InitCmb();

      SetControls();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Add event handler to scan and configure buttons
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    private void InitControls()
    {
      // These events are inserted on parent control (uc_gt_player_tracking).
      EventLastAction.AddEventLastAction(this.Controls);

      this.btn_swap_seat.Click += new System.EventHandler(this.btn_swap_seat_Click);
      this.btn_play_pause.Click += new System.EventHandler(this.btn_play_pause_Click);
      this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
      this.btn_sell_chips.Click += new System.EventHandler(this.btn_sale_chips_Click);
      //this.btn_buy_chips.Click += new System.EventHandler(this.btn_buy_chips_Click);

      SetMouseDownEventRecursive(this.Controls);
      this.MouseDown += new MouseEventHandler(Control_MouseDown);

      m_is_TITO_mode = TITO.BusinessLogic.IsModeTITO;

      m_national_currency = CurrencyExchange.GetNationalCurrency();

      form_credit = new frm_amount_input(this.CheckPermissionsToRedeem, null);
    }

    void Control_MouseDown(object sender, MouseEventArgs e)
    {
      Decimal _value;

      tb_current_bet.BackColor = CashierStyle.Colors.m_colors_dic[CashierStyle.Colors.ENUM_COLORS.HEX_E1E7E9];
      tb_points_won.BackColor = CashierStyle.Colors.m_colors_dic[CashierStyle.Colors.ENUM_COLORS.HEX_E1E7E9];
      tb_chips_in.BackColor = CashierStyle.Colors.m_colors_dic[CashierStyle.Colors.ENUM_COLORS.HEX_E1E7E9];
      tb_chips_out.BackColor = CashierStyle.Colors.m_colors_dic[CashierStyle.Colors.ENUM_COLORS.HEX_E1E7E9];


      if (m_seat == null || sender == null)
      {
        return;
      }
      try
      {
        _value = 0;
        TextBoxEnterEvent(0, TextBoxSelected.Unknown, m_go_back);

        if (sender.Equals(tb_current_bet))
        {
          Decimal.TryParse(tb_current_bet.Text.Replace(m_nfi.CurrencySymbol, ""), out _value);
          TextBoxEnterEvent(_value, TextBoxSelected.Played_Current, m_go_back);
          tb_current_bet.BackColor = Color.LightGoldenrodYellow;
        }
        else if (sender.Equals(tb_points_won))
        {
          Decimal.TryParse(tb_points_won.Text.Replace(m_nfi.CurrencySymbol, ""), out _value);
          TextBoxEnterEvent(_value, TextBoxSelected.Points_Won, m_go_back);
          tb_points_won.BackColor = Color.LightGoldenrodYellow;
        }
        else if (sender.Equals(tb_chips_in))
        {
          Decimal.TryParse(tb_chips_in.Text.Replace(m_nfi.CurrencySymbol, ""), out _value);
          TextBoxEnterEvent(_value, TextBoxSelected.Chips_In, m_go_back);
          tb_chips_in.BackColor = Color.LightGoldenrodYellow;
        }
        else if (sender.Equals(tb_chips_out))
        {
          Decimal.TryParse(tb_chips_out.Text.Replace(m_nfi.CurrencySymbol, ""), out _value);
          TextBoxEnterEvent(_value, TextBoxSelected.Chips_Out, m_go_back);
          tb_chips_out.BackColor = Color.LightGoldenrodYellow;
        }
      }
      finally
      {
        GC.Collect();
        ThreadPool.QueueUserWorkItem(delegate
        {
          GC.WaitForPendingFinalizers();
          GC.Collect();
        });
      }
    }

    #endregion  // Constructor

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE: Initialize controls
    // 
    //  PARAMS:
    //      - INPUT: Parent: frm_container
    //               Seat:
    //      - OUTPUT: 
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    public void InitControls(frm_container Parent, Seat Seat, Currency MaxBet, Currency MinBet, Int32 TableIdlePlays)
    {
      Boolean _points_editable;
      String _error_str;

      m_elp_mode = GeneralParam.GetInt32("PlayerTracking.ExternalLoyaltyProgram", "Mode", 0);

      m_parent = Parent;
      m_seat = Seat;
      m_table_max_bet = MaxBet;
      m_table_min_bet = MinBet;
      m_table_idle_plays = TableIdlePlays;


      timerRefresh.Enabled = false;

      if (Seat != null)
      {
        InitializeControlResources();

        m_seat.HasPendingChanges = false;
        m_show_msg_when_validate_values = GamingTableBusinessLogic.GetShowInformationMessages();

        _points_editable = ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.PlayerTrackingEditPoints,
                                           ProfilePermissions.TypeOperation.OnlyCheck,
                                           m_parent,
                                           out _error_str);

        if ((Seat.PlayerType == GTPlayerType.Customized) && _points_editable && !m_seat.PlaySession.Started_walk.HasValue && GamingTableBusinessLogic.GetAssignmentPointsMode() == GamingTableBusinessLogic.ASSIGNMENT_POINTS_MODE.AUTOMATIC)
        {
          tb_points_won.Enabled = true;
        }
        else
        {
          tb_points_won.Enabled = false;
        }

        m_nfi = System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat;

        lbl_action_description.Visible = false;

        FillPlayerData();

        // 22-OCT-2014 JML  Fixed Bug #WIG-1558
        m_points_edited_by_user = (m_seat.PlaySession.AwardedPoints != m_seat.PlaySession.AwardedPointsCalculated) && m_seat.PlaySession.Started_walk.HasValue;
        CalculateFields();
        RefreshWindow();

        timerRefresh.Enabled = GamingTableBusinessLogic.IsEnabledGTPlayerTracking();
        timerRefresh.Interval = GeneralParam.GetInt32("GamingTable.PlayerTracking", "RefreshIntervalSeconds", 10) * 1000;

        Control_MouseDown(null, null);
      }
      else
      {
        SetControls();
      }

      this.Invalidate();
    }  // InitControls

    //------------------------------------------------------------------------------
    // PURPOSE: Set initial focus
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    public void InitFocus()
    {
      pb_user.Focus();
    }  // InitFocus

    //------------------------------------------------------------------------------
    // PURPOSE: Show Hide Swap Label
    // 
    //  PARAMS:
    //      - INPUT: IsVisible
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    public void ShowHideSwapLabel(Boolean IsVisible)
    {
      lbl_action_description.Visible = IsVisible;
      if (IsVisible == true)
      {
        lbl_action_description.Text = Resource.String("STR_PLAYER_GAMING_SWAP_REQUIRED");
        timerClear.Stop();
        SetControls();
      }
      else
      {
        ValidateCurrentBet(false, true);
      }

    }  // ShowHideSwapLabel

    public void ShowHideSeatUserLabel(Boolean IsVisible)
    {
      lbl_action_description.Visible = IsVisible;
      if (IsVisible == true)
      {
        lbl_action_description.Enabled = true;
        lbl_action_description.Text = Resource.String("STR_PLAYER_GAMING_SWAP_REQUIRED");
        timerClear.Stop();
      }

    }

    public void ShowHideValidatedCopyDealerTicket(Boolean IsVisible, Decimal TicketAmount)
    {
      lbl_action_description.Visible = IsVisible;
      if (IsVisible == true)
      {
        lbl_action_description.Enabled = true;
        lbl_action_description.Text = Resource.String("STR_PLAYER_GAMING_TABLE_VALIDATED_COPY_DEALER_TICKET", Currency.Format(TicketAmount, m_seat.PlaySession.PlayerIsoCode));
        timerClear.Enabled = true;
      }

    }

    //------------------------------------------------------------------------------
    // PURPOSE: Save Seat changes
    // 
    //  PARAMS:
    //      - INPUT: 
    //
    //      - OUTPUT: 
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    public Boolean SaveChanges()
    {
      Boolean _is_ok = false;
      Cursor _previous_cursor;
      GamingTableBusinessLogic.GT_RESPONSE_CODE _error_code;
      m_nfi = System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat;

      _previous_cursor = this.Cursor;

      try
      {
        this.Cursor = Cursors.WaitCursor;
        if (m_seat != null)
        {
          m_seat.PlaySession.PlayerSpeed = (GTPlayerTrackingSpeed)int.Parse(cb_speed.SelectedValue.ToString());
          m_seat.PlaySession.PlayerSkill = (GTPlaySessionPlayerSkill)int.Parse(cb_skill.SelectedValue.ToString());
          m_seat.PlaySession.PlayerIsoCode = cb_player_iso_code.SelectedValue.ToString();

          if (m_seat.PlaySession.PlaySessionId != 0)
          {
            _error_code = GamingTableBusinessLogic.UpdateGTPlaySession(m_seat, Cashier.UserId, Cashier.TerminalId);
          }

          m_seat.HasPendingChanges = false;
        }
        _is_ok = true;
      }
      catch
      {
      }
      finally
      {
        this.Cursor = _previous_cursor;
      }
      return _is_ok;

    } // SaveChanges

    public Boolean CheckPermissionsToRedeem(Int64 AccountId, Currency TotalToPay, Currency PrizeAmount,
                                            List<ProfilePermissions.CashierFormFuncionality> PermissionList)
    {
      Int32 _max_allowed_int;
      Currency _max_allowed;
      Currency _min_limit_exceded;
      String _message;
      DialogResult _dialog_result;
      //ENUM_ANTI_MONEY_LAUNDERING_LEVEL _aml_level;
      //Boolean _threshold_crossed;

      _min_limit_exceded = -1;

      // ACM 14-MAY-2013:  Check MaxAllowedCashOut.
      _max_allowed_int = GeneralParam.GetInt32("Cashier", "MaxAllowedCashOut");
      _max_allowed = _max_allowed_int;

      if (_max_allowed > 0 && TotalToPay > _max_allowed)
      {
        _min_limit_exceded = _max_allowed;
        PermissionList.Add(ProfilePermissions.CashierFormFuncionality.CardSubtractCreditMaxAllowed);
      }

      // Show popup for max allowed exceded.
      if (_min_limit_exceded != -1)
      {
        _message = Resource.String("STR_CARD_SUBTRACT_CREDIT_MAX_EXCEDED", TotalToPay.ToString(false), _min_limit_exceded.ToString(false));
        _message = _message.Replace("\\r\\n", "\r\n");

        _dialog_result = ShowValidateMsg(_message, Resource.String("STR_APP_GEN_MSG_WARNING"), MessageBoxButtons.YesNo, MessageBoxIcon.Question, Cashier.MainForm);

        if (_dialog_result != DialogResult.OK)
        {
          return false;
        }
      }

      // TODO: REVISAR
      //if (!CheckAntiMoneyLaundering(AccountId, ENUM_CREDITS_DIRECTION.Redeem, PrizeAmount, out _aml_level, out _threshold_crossed))
      //{
      //  return false;
      //}

      // DLL && RCI 28-OCT-2013: AML: Add permission to exceed report limit.
      //if (frm_amount_input.AML_RequestPermissionToExceedReportLimit(ENUM_CREDITS_DIRECTION.Redeem, _aml_level, _threshold_crossed))
      //{
      //  if (_aml_level == ENUM_ANTI_MONEY_LAUNDERING_LEVEL.MaxAllowed)
      //  {
      //    PermissionList.Add(ProfilePermissions.CashierFormFuncionality.AML_ExceedMaximumLimitPrize);
      //  }
      //  else
      //  {
      //    PermissionList.Add(ProfilePermissions.CashierFormFuncionality.AML_ExceedReportLimitPrize);
      //  }
      //}

      return true;
    } // CheckPermissionsToRedeem

    #endregion  // Public Methods

    public void EnabledControls(Boolean Enabled)
    {
      gb_account.Enabled = Enabled;
      gb_bet.Enabled = Enabled;
      gb_played_time.Enabled = Enabled;
      gb_away_time.Enabled = Enabled;
      gb_points.Enabled = Enabled;
      gb_chips.Enabled = Enabled;
      btn_close.Enabled = Enabled;
      btn_play_pause.Enabled = Enabled;
      btn_swap_seat.Enabled = Enabled;
    }

    #region Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE:  InitResources
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    private void InitResources()
    {
      pb_user.Image = Resources.ResourceImages.anonymous_user; // WSI.Cashier.Images.Get(Images.CashierImage.UserAnonymous);
      btn_close.Image = Resources.ResourceImages.endSession1; // WSI.Cashier.Images.Get(Images.CashierImage.GT_Close);
      btn_swap_seat.Image = Resources.ResourceImages.swapPosition1; // WSI.Cashier.Images.Get(Images.CashierImage.GT_Swap);
      btn_play_pause.Image = Resources.ResourceImages.play1;

      gb_account.HeaderText = Resource.String("STR_FORMAT_GENERAL_NAME_ACCOUNT");
      lbl_account_id_name.Text = Resource.String("STR_FORMAT_GENERAL_NAME_ACCOUNT") + ":";
      lbl_trackdata.Text = Resource.String("STR_FORMAT_GENERAL_NAME_CARD") + ":";
      lbl_points_balance_title.Text = Resource.String("STR_UC_CARD_POINTS_BALANCE_TITLE") + ":";    // "Balance"
      lbl_level_title.Text = Resource.String("STR_UC_CARD_POINTS_LEVEL_TITLE") + ":";               // "Nivel"

      //      - Play
      lbl_total_bet.Text = Resource.String("STR_CASHIER_GAMING_TABLE_TOTAL_BET") + ":";
      lbl_current_bet.Text = Resource.String("STR_CASHIER_GAMING_TABLE_CURRENT_BET") + ":";
      lbl_max_played.Text = Resource.String("STR_CASHIER_GAMING_TABLE_MAX_BET") + ":";
      lbl_min_played.Text = Resource.String("STR_CASHIER_GAMING_TABLE_MIN_BET") + ":";
      lbl_average_bet.Text = Resource.String("STR_CASHIER_GAMING_TABLE_AVERAGE_BET") + ":";

      lbl_netwin.Text = Resource.String("STR_CASHIER_GAMING_TABLE_NET_WIN") + ":";
      gb_bet.HeaderText = Resource.String("STR_CASHIER_GAMING_TABLE_BET_GROUP_BOX");
      gb_points.HeaderText = Resource.String("STR_CASHIER_GAMING_TABLE_POINTS_GROUP_BOX");
      //gb_chips.HeaderText = Resource.String("STR_CASHIER_GAMING_TABLE_CHIPS_GROUP_BOX");
      //lbl_average_won.Text = Resource.String("STR_CASHIER_GAMING_TABLE_AVERAGE_WON");
      lbl_points_won_calculate.Text = Resource.String("STR_CASHIER_GAMING_TABLE_POINTS_WON_CALCULATE") + ":";
      lbl_points_won.Text = Resource.String("STR_CASHIER_GAMING_TABLE_POINTS_WON") + ":";

      lbl_speed.Text = Resource.String("STR_CASHIER_GAMING_TABLE_SPEED");
      lbl_skill.Text = Resource.String("STR_CASHIER_GAMING_TABLE_SKILL");

      lbl_play_date_name.Text = Resource.String("STR_PLAYER_GAMING_TABLE_DATE");
      lbl_play_time_name.Text = Resource.String("STR_PLAYER_GAMING_TABLE_TIME"); ;

      gb_away_time.HeaderText = Resource.String("STR_CASHIER_GAMING_TABLE_GB_AWAY_TIME");
      lbl_pause_date_name.Text = Resource.String("STR_PLAYER_GAMING_TABLE_PAUSE_DATE");
      lbl_pause_time_name_total.Text = Resource.String("STR_PLAYER_GAMING_TABLE_PAUSE_TIME_TOTAL");
      lbl_pause_time_name_actual.Text = Resource.String("STR_PLAYER_GAMING_TABLE_PAUSE_TIME_ACTUAL");


      lbl_action_description.Text = Resource.String("STR_PLAYER_GAMING_SWAP_REQUIRED");
      lbl_plays_name.Text = Resource.String("STR_PLAYER_GAMING_TABLE_PLAYS");

      //      - Chips
      lbl_chips_in.Text = Resource.String("STR_CASHIER_GAMING_TABLE_CHIPS_IN") + ":";
      lbl_chips_out.Text = Resource.String("STR_CASHIER_GAMING_TABLE_CHIPS_OUT") + ":";
      lbl_total_chips_sell.Text = Resource.String("STR_CASHIER_GAMING_TABLE_TOTAL_CHIPS_SELL") + ":";

      //      - Buttons
      btn_close.Text = "";
      btn_play_pause.Text = "";
      btn_swap_seat.Text = "";

      //btn_buy_chips.Text = Resource.String("STR_UC_CARD_BTN_BUY_CHIPS");
      btn_sell_chips.Text = GeneralParam.GetString("GamingTable.PlayerTracking", "PlayerTracking.SellChips.Text", Resource.String("STR_GAMING_TABLE_BTN_SELL_CHIPS"));

    }  // InitResources

    //------------------------------------------------------------------------------
    // PURPOSE: Put correct image in the walk button
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    private void SetWalkButton()
    {
      DateTime _temp_datetime;
      TimeSpan _temp_seconds_game_time;
      TimeSpan _temp_seconds_walk_time_total;
      TimeSpan _temp_seconds_walk_time_actual;
      String _datetime_format;

      if (m_seat != null && m_seat.PlayerType != GTPlayerType.Unknown)
      {
        _datetime_format = Format.TimeCustomFormatString(false);
        _temp_seconds_game_time = new TimeSpan(0, 0, (Int32)m_seat.PlaySession.GameTime);
        _temp_seconds_walk_time_total = new TimeSpan(0, 0, (Int32)m_seat.PlaySession.Walk);
        _temp_seconds_walk_time_actual = new TimeSpan(0, 0, (Int32)m_seat.PlaySession.ActualWalk);

        lbl_play_date_value.Text = m_seat.PlaySession.Started.ToString(_datetime_format);
        lbl_play_time_value.Text = Format.TimeSpanToString(_temp_seconds_game_time, true);
        lbl_pause_time_value_total.Text = Format.TimeSpanToString(_temp_seconds_walk_time_total, true);

        if (m_seat.PlaySession.Started_walk.HasValue)
        {
          _temp_datetime = (DateTime)m_seat.PlaySession.Started_walk;
          btn_play_pause.Image = Resources.ResourceImages.play1; // WSI.Cashier.Images.Get(Images.CashierImage.GT_Play);
          lbl_pause_date_value.Text = _temp_datetime.ToString(_datetime_format);
          lbl_pause_time_value_actual.Text = Format.TimeSpanToString(_temp_seconds_walk_time_actual, true);
          lbl_pause_time_value_actual.BackColor = Color.LightGoldenrodYellow;
        }

        if (!m_seat.PlaySession.Started_walk.HasValue)
        {
          btn_play_pause.Image = Resources.ResourceImages.pause1; // WSI.Cashier.Images.Get(Images.CashierImage.GT_Pause);
          lbl_pause_date_value.Text = "---";
          lbl_pause_time_value_actual.Text = "---";
          lbl_pause_time_value_actual.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
        }
      }
      else
      {
        lbl_play_date_value.Text = "---";
        lbl_pause_date_value.Text = "---";
        lbl_pause_time_value_actual.Text = "---";
      }
    }  // SetWalkButton

    //------------------------------------------------------------------------------
    // PURPOSE: initialize combo box
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    private void InitCmb()
    {
      DataTable _dt;
      cb_skill.Items.Clear();
      cb_speed.Items.Clear();

      // Skill combo
      _dt = new DataTable();

      _dt.Columns.Add("Key", Type.GetType("System.Int32")).AllowDBNull = false;
      _dt.Columns.Add("Value", Type.GetType("System.String")).AllowDBNull = false;

      _dt.Rows.Add(GTPlaySessionPlayerSkill.Soft, Resource.String("STR_FRM_PLAYER_TRACKING_CB_SOFT"));
      _dt.Rows.Add(GTPlaySessionPlayerSkill.Average, Resource.String("STR_FRM_PLAYER_TRACKING_CB_AVERAGE"));
      _dt.Rows.Add(GTPlaySessionPlayerSkill.Hard, Resource.String("STR_FRM_PLAYER_TRACKING_CB_HARD"));

      cb_skill.DataSource = _dt;
      cb_skill.ValueMember = "Key";
      cb_skill.DisplayMember = "Value";


      // Skill combo
      _dt = new DataTable();

      _dt.Columns.Add("Key", Type.GetType("System.Int32")).AllowDBNull = false;
      _dt.Columns.Add("Value", Type.GetType("System.String")).AllowDBNull = false;

      _dt.Rows.Add(GTPlayerTrackingSpeed.Slow, Resource.String("STR_FRM_PLAYER_TRACKING_CB_SLOW"));
      _dt.Rows.Add(GTPlayerTrackingSpeed.Medium, Resource.String("STR_FRM_PLAYER_TRACKING_CB_MEDIUM"));
      _dt.Rows.Add(GTPlayerTrackingSpeed.Fast, Resource.String("STR_FRM_PLAYER_TRACKING_CB_FAST"));

      cb_speed.DataSource = _dt;
      cb_speed.ValueMember = "Key";
      cb_speed.DisplayMember = "Value";

    }  // InitCmb

    //------------------------------------------------------------------------------
    // PURPOSE: initialize combo box -- Currency
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    public void InitCmbCurrency(Int64? GamingTableId)
    {
      DataTable _dt;
      _dt = new DataTable();

      //if (m_is_TITO_mode)
      //{
      _dt = FeatureChips.ChipsSets.GetCurrenciesAccepted(GamingTableId);

      this.cb_player_iso_code.SelectedIndexChanged -= new System.EventHandler(this.cb_SelectedIndexChanged);
      //}
      //else
      //{
      //  _dt.Columns.Add("ISO_CODE", Type.GetType("System.String")).AllowDBNull = false;
      //  _dt.Rows.Add(CurrencyExchange.GetNationalCurrency());

      //  cb_player_iso_code.Enabled = false;
      //}

      cb_player_iso_code.DataSource = _dt;
      cb_player_iso_code.ValueMember = "ISO_CODE";
      cb_player_iso_code.DisplayMember = "ISO_CODE";
      this.cb_player_iso_code.SelectedIndexChanged += new System.EventHandler(this.cb_SelectedIndexChanged);

    }  // InitCmbCurrency


    //------------------------------------------------------------------------------
    // PURPOSE: Initialize control resources. (NLS string, images, etc.)
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    private void InitializeControlResources()
    {
      lbl_account_id_value.Text = m_seat.AccountId.ToString();
      lbl_account_id_value.Visible = (m_seat.AccountId != 0);
      lbl_track_number.Text = m_seat.TrackData;

      if (m_seat.HolderName == null)
      {
        lbl_holder_name.Text = "";
      }
      else if (m_seat.HolderName == "")
      {
        lbl_holder_name.Text = Resource.String("STR_UC_CARD_CARD_HOLDER_ANONYMOUS");
      }
      else
      {
        lbl_holder_name.Text = m_seat.HolderName;
      }
      lbl_level.Text = LoyaltyProgram.LevelName(m_seat.HolderLevel);
      lbl_points_balance.Text = String.Format("{0} {1} ", m_seat.Points.ToString(), Resource.String("STR_UC_CARD_POINTS_BALANCE_SUFFIX"));

      tb_current_bet.Text = "0";
      tb_max_played.Text = "0";
      tb_min_played.Text = "0";
      tb_points_won.Text = "0";
      lbl_calculate_points_won.Text = "0";
      tb_total_bet.Text = "0";
      tb_netwin.Text = "0";

      this.cb_speed.SelectedIndexChanged -= new System.EventHandler(this.cb_SelectedIndexChanged);
      this.cb_skill.SelectedIndexChanged -= new System.EventHandler(this.cb_SelectedIndexChanged);

      cb_skill.SelectedValue = m_seat.PlaySession.PlayerSkill;
      cb_speed.SelectedValue = m_seat.PlaySession.PlayerSpeed;

      this.cb_skill.SelectedIndexChanged += new System.EventHandler(this.cb_SelectedIndexChanged);
      this.cb_speed.SelectedIndexChanged += new System.EventHandler(this.cb_SelectedIndexChanged);

      this.cb_player_iso_code.SelectedIndexChanged -= new System.EventHandler(this.cb_SelectedIndexChanged);
      if (m_seat.PlaySession.PlayerIsoCode != null)
      {
        cb_player_iso_code.SelectedValue = m_seat.PlaySession.PlayerIsoCode;
        if (cb_player_iso_code.Items.Count > 1)
        {
          ValuePlayerIsoCodeChangedInSession(m_seat, m_seat.PlaySession.PlayerIsoCode, cb_player_iso_code.SelectedValue.ToString());
        }
      }
      else
      {
        cb_player_iso_code.SelectedValue = 1;
      }
      this.cb_player_iso_code.SelectedIndexChanged += new System.EventHandler(this.cb_SelectedIndexChanged);

    }  // InitializeControlResources

    //------------------------------------------------------------------------------
    // PURPOSE: Fill Player Data on screen
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    private void FillPlayerData()
    {
      if (m_seat != null)
      {
        Int64 _account_id;
        if (m_seat.PlayerType == GTPlayerType.Customized || m_seat.PlayerType == GTPlayerType.AnonymousWithCard)
        {
          _account_id = m_seat.AccountId;
          lbl_account_id_value.Text = m_seat.AccountId.ToString();
          lbl_track_number.Text = m_seat.TrackData;
        }
        else
        {
          _account_id = 0;
          lbl_account_id_value.Text = "---";
          lbl_track_number.Text = "---";
        }

        // AJQ 28-MAY-2015, Account Photo        
        AccountPhotoFunctions.SetAccountPhoto(pb_user, _account_id, (GENDER)m_seat.HolderGender, out m_exist_photo);

        lbl_account_id_value.Visible = (m_seat.AccountId != 0);

        lbl_holder_name.Text = m_seat.HolderName;

        lbl_level.Text = LoyaltyProgram.LevelName(m_seat.HolderLevel);
        lbl_points_balance.Text = String.Format("{0} {1} ", m_seat.Points.ToString(), Resource.String("STR_UC_CARD_POINTS_BALANCE_SUFFIX"));

        if (tb_current_bet.BackColor != Color.LightGoldenrodYellow)
        {
          tb_current_bet.Text = m_seat.PlaySession.CurrentBet.ToString(false);
        }

        tb_max_played.Text = m_seat.PlaySession.PlayedMax.ToString(false);
        tb_min_played.Text = m_seat.PlaySession.PlayedMin.ToString(false);
        lbl_calculate_points_won.Text = m_seat.PlaySession.AwardedPointsCalculated.ToString();

        if (tb_points_won.BackColor != Color.LightGoldenrodYellow && GamingTableBusinessLogic.GetAssignmentPointsMode() == GamingTableBusinessLogic.ASSIGNMENT_POINTS_MODE.AUTOMATIC)
        {
          tb_points_won.Text = m_seat.PlaySession.AwardedPoints.ToString();
        }

        tb_total_bet.Text = m_seat.PlaySession.PlayedAmount.ToString(false);
        tb_netwin.Text = m_seat.PlaySession.NetWin.ToString(false);
        lbl_plays_value.Text = m_seat.PlaySession.PlaysCount.ToString();
        lbl_average_bet_value.Text = m_seat.PlaySession.PlayedAvg.ToString(false);
        lbl_chips_sale_accumulate.Text = m_seat.PlaySession.TotalSellChips.ToString(false);

        if (tb_chips_in.BackColor != Color.LightGoldenrodYellow)
        {
          tb_chips_in.Text = m_seat.PlaySession.ChipsIn.ToString(false);
        }

        if (tb_chips_out.BackColor != Color.LightGoldenrodYellow)
        {
          tb_chips_out.Text = m_seat.PlaySession.ChipsOut.ToString(false);
        }

        if (!cb_skill.Focused)
        {
          if (m_seat.PlaySession.PlayerSkill == GTPlaySessionPlayerSkill.Unknown)
          {
            cb_skill.SelectedValue = GTPlaySessionPlayerSkill.Average;
          }
          else
          {
            cb_skill.SelectedValue = m_seat.PlaySession.PlayerSkill;
          }
        }

        if (!cb_speed.Focused)
        {
          if (m_seat.PlaySession.PlayerSpeed == GTPlayerTrackingSpeed.Unknown)
          {
            cb_speed.SelectedValue = GTPlayerTrackingSpeed.Medium;
          }
          else
          {
            cb_speed.SelectedValue = m_seat.PlaySession.PlayerSpeed;
          }
        }

        if (!cb_player_iso_code.Focused)
        {
          cb_player_iso_code.SelectedValue = m_seat.PlaySession.PlayerIsoCode;
        }

        // Set background color when Played current is not longer updated
        if (m_seat.PlayerType != GTPlayerType.Unknown &&
          (m_seat.PlaySession.PlaysCount - m_seat.PlaySession.LastPlaysCount > m_table_idle_plays ||
          ValidateCurrentBet(false, false)))
        {
          tb_current_bet.ForeColor = Color.DarkOrange;
          tb_current_bet.Font = new Font(tb_current_bet.Font, FontStyle.Bold);
        }
        else
        {
          tb_current_bet.ForeColor = Color.Black;
          tb_current_bet.Font = new Font(tb_current_bet.Font, FontStyle.Regular);
        }

        SetControls();
        m_seat.HasPendingChanges = false;
      }
      else
      {
        SetValuesControls();
        SetControls();
      }

      DrawSeatImage();

    }  // FillPlayerData  

    /// <summary>
    ///  Depending on condition get the alarm of the bets over hand by customer.
    /// </summary>
    private void GenerateAlarmFromBetsOverHand()
    {
      Currency _bets_over_hand_by_customer;
      _bets_over_hand_by_customer = GamingTableAlarms.GetBetsOverHandByCustomer();

      if (_bets_over_hand_by_customer > 0 && m_seat.PlaySession.CurrentBet > _bets_over_hand_by_customer)
      {
        String _current_bet;
        if (m_seat.PlaySession.PlayerIsoCode != CurrencyExchange.GetNationalCurrency())
        {
          _current_bet = Currency.Format(m_seat.PlaySession.CurrentBet, m_seat.PlaySession.PlayerIsoCode);
        }
        else
        {
          _current_bet = m_seat.PlaySession.CurrentBet.ToString();
        }

        Alarm.Register(AlarmSourceCode.User
                       , CommonCashierInformation.TerminalId
                       , CommonCashierInformation.UserName + "@" + CommonCashierInformation.TerminalName
                       , AlarmCode.User_GamingTables_BetsOverHandByCustomer
                       , Resource.String("STR_ALARM_GAMBLING_TABLE_BETS_OVER_HAND_BY_CUSTOMER", _current_bet, _bets_over_hand_by_customer, m_seat.AccountId));
      }
    } // GenerateAlarmFromBetsOverHand

    /// <summary>
    /// Depending on condition get the alarm of the variation of a customer bet per session in a given period.
    /// </summary>
    private void GenerateAlarmFromBetsVariation()
    {
      Decimal _bet_variation_parameter;
      Decimal _current_bet_variation;

      if (m_seat.PlaySession.PlayedAvg == 0)
      {
        return;
      }

      _current_bet_variation = Math.Abs(((m_seat.PlaySession.CurrentBet - m_seat.PlaySession.PlayedAvg) / m_seat.PlaySession.PlayedAvg) * 100);
      _bet_variation_parameter = GamingTableAlarms.GetBetsVariationCustomerPerSession();

      if (_bet_variation_parameter > 0 && _current_bet_variation >= _bet_variation_parameter)
      {
        Int32 _num_decimals;
        _num_decimals = WSI.Common.Format.GetFormattedDecimals(m_seat.PlaySession.PlayerIsoCode);

        Alarm.Register(AlarmSourceCode.User
                       , CommonCashierInformation.TerminalId
                       , CommonCashierInformation.UserName + "@" + CommonCashierInformation.TerminalName
                       , AlarmCode.User_GamingTables_BetsVariationCustomerPerSession
                       , Resource.String("STR_ALARM_GAMBLING_TABLES_BETS_VARIATION_CUSTOMER_PER_SESSION", Currency.Format(_current_bet_variation, String.Empty), m_seat.PlaySession.AccountId, Currency.Format(_bet_variation_parameter, String.Empty)));
      }
    } // GenerateAlarmFromBetsVariation

    //------------------------------------------------------------------------------
    // PURPOSE: Set default values in screen if input is false, 
    //                and call SetControls
    //  PARAMS:
    //      - INPUT:  IsEnabled 
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES: 
    //
    private void SetValuesControls()
    {
      Currency _default_currency;
      Points _default_points;

      _default_currency = 0;
      _default_points = 0;

      lbl_account_id_value.Text = "---";
      lbl_track_number.Text = "----------";
      lbl_level.Text = "---";
      lbl_points_balance.Text = String.Format("0 {0} ", Resource.String("STR_UC_CARD_POINTS_BALANCE_SUFFIX")); ;
      lbl_play_date_value.Visible = false;
      lbl_pause_date_value.Text = "---";
      lbl_play_time_value.Text = "---";
      lbl_pause_time_value_total.Text = "---";
      lbl_pause_time_value_actual.Text = "---";
      lbl_plays_value.Text = "0";
      lbl_play_date_value.Text = "---";

      tb_total_bet.Text = _default_currency.ToString(false);
      tb_current_bet.Text = _default_currency.ToString(false);
      tb_max_played.Text = _default_currency.ToString(false);
      tb_min_played.Text = _default_currency.ToString(false);
      tb_netwin.Text = _default_currency.ToString(false);
      tb_total_bet.Text = _default_currency.ToString(false);
      lbl_calculate_points_won.Text = _default_points.ToString();
      if (GamingTableBusinessLogic.GetAssignmentPointsMode() == GamingTableBusinessLogic.ASSIGNMENT_POINTS_MODE.AUTOMATIC)
      {
      tb_points_won.Text = _default_points.ToString();
      }
      lbl_average_bet_value.Text = _default_currency.ToString(false);
      tb_chips_in.Text = _default_currency.ToString(false);
      tb_chips_out.Text = _default_currency.ToString(false);

      lbl_chips_sale_accumulate.Text = _default_currency.ToString(false);

      pb_user.Image = Resources.ResourceImages.anonymous_user; // WSI.Cashier.Images.Get(Images.CashierImage.UserAnonymous);
      lbl_pause_time_value_actual.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;

      cb_skill.SelectedIndex = 1;
      cb_speed.SelectedIndex = 1;
      if (m_seat != null)
      {
        m_seat.HasPendingChanges = false;
        lbl_holder_name.Text = Resource.String("PLAYER_TRACKING_LEGEND_FREE_SEAT");
      }
      else
      {
        lbl_holder_name.Text = "---";
      }

      cb_player_iso_code.SelectedIndex = 1;

      lbl_action_description.Visible = false;

      tb_current_bet.ForeColor = Color.Black;
      tb_current_bet.Font = new Font(tb_current_bet.Font, FontStyle.Regular);

      cb_player_iso_code.IsDroppedDown = false;

    } // SetEnabledValuesControls

    //------------------------------------------------------------------------------
    // PURPOSE: Visible/Invisiblee controls
    // 
    //  PARAMS:
    //      - INPUT: IsEnabled
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    private void SetVisibleControls(Boolean IsVisible)
    {
      Boolean _has_pending_changes;

      _has_pending_changes = false;

      if (m_seat != null)
      {
        _has_pending_changes = m_seat.HasPendingChanges;
      }

      gb_bet.Visible = IsVisible;
      gb_points.Visible = IsVisible;
      gb_chips.Visible = IsVisible;
      lbl_play_date_value.Visible = IsVisible;
      lbl_play_date_name.Visible = IsVisible;
      lbl_play_time_value.Visible = IsVisible;
      lbl_play_time_name.Visible = IsVisible;
      gb_away_time.Visible = IsVisible;
      gb_played_time.Visible = IsVisible;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Enable/disable controls
    // 
    //  PARAMS:
    //      - INPUT: IsEnabled
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    public void SetControls()
    {
      GTPlayerType _player_type;
      Boolean _started_walk;
      CASHIER_STATUS _cashier_status;
      //Boolean _chip_sale_register;
      Boolean _points_editable;
      String _error_str;
      Boolean _unknown_player;
      Boolean _visible_fields;
      Boolean _is_enabled;

      // Initialize Variables
      _player_type = GTPlayerType.Unknown;
      _started_walk = false;
      //_chip_sale_register = false;
      _points_editable = false;
      _visible_fields = false;
      _unknown_player = false;
      _is_enabled = false;
      _unknown_player = false;

      if (m_gaming_table_paused)
      {
        tb_current_bet.BackColor = WSI.Cashier.Controls.uc_label.DefaultBackColor;
        tb_points_won.BackColor = WSI.Cashier.Controls.uc_label.DefaultBackColor;
        tb_chips_in.BackColor = WSI.Cashier.Controls.uc_label.DefaultBackColor;
        tb_chips_out.BackColor = WSI.Cashier.Controls.uc_label.DefaultBackColor;
      }

      // If external loyalty program is enabled or permission edited points is disabled, disabled editing points
      if ((m_elp_mode > 0) || !ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.PlayerTrackingEditPoints,
                                                                  ProfilePermissions.TypeOperation.OnlyCheck,
                                                                  m_parent,
                                                                  out _error_str))
      {
        _points_editable = false;
      }
      else
      {
        _points_editable = true;
      }

      //_chip_sale_register = GeneralParam.GetBoolean("GamingTables", "Cashier.RegisterChipSaleAtTableEnabled", false);
      _cashier_status = CashierBusinessLogic.ReadCashierStatus(Cashier.SessionId);

      if (m_seat != null)
      {
        _player_type = m_seat.PlayerType;
        _started_walk = m_seat.PlaySession.Started_walk.HasValue;

        if (_player_type == GTPlayerType.Unknown)
        {
          _unknown_player = true;
        }
        else
        {
          _visible_fields = true;
        }
      }
      else
      {
        SetValuesControls();
        _visible_fields = true;
      }

      //Set visible controls
      SetVisibleControls(_visible_fields);

      if (!m_gaming_table_paused & m_seat != null & !_unknown_player)
      {
        _is_enabled = true;
      }

      // Check general case
      tb_current_bet.Enabled = _is_enabled;
      cb_speed.Enabled = _is_enabled;
      cb_skill.Enabled = _is_enabled;
      tb_chips_in.Enabled = _is_enabled;
      tb_chips_out.Enabled = _is_enabled;
      btn_swap_seat.Enabled = _is_enabled;
      btn_play_pause.Enabled = _is_enabled;
      cb_player_iso_code.Enabled = _is_enabled;

      // Check special case
      tb_points_won.Enabled = GamingTableBusinessLogic.GetAssignmentPointsMode() == GamingTableBusinessLogic.ASSIGNMENT_POINTS_MODE.AUTOMATIC &&
                             _is_enabled && _points_editable && _started_walk && (_player_type == GTPlayerType.Customized);
      btn_close.Enabled = _is_enabled && _started_walk;
      if (cb_player_iso_code.Items.Count > 0)
      {
        btn_sell_chips.Enabled = _is_enabled & (m_is_TITO_mode || (cb_player_iso_code.SelectedValue != null && m_national_currency == cb_player_iso_code.SelectedValue.ToString()));
      }
      SetWalkButton();

    } // SetControls

    //------------------------------------------------------------------------------
    // PURPOSE: Show message
    // 
    //  PARAMS:
    //      - INPUT: 
    //                String: Msg
    //                String: Header
    //                MessageBoxButtons: MsgBoxButtons
    //                MessageBoxIcon: Icon
    //                Form: ParentForm
    //
    //      - OUTPUT: 
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    private DialogResult ShowValidateMsg(String Msg, String Header, MessageBoxButtons MsgBoxButtons, MessageBoxIcon Icon, Form ParentForm)
    {
      return ShowValidateMsg(Msg, Header, MessageBoxButtons.OK, MessageBoxIcon.Warning, ParentForm, m_show_msg_when_validate_values);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Show message
    // 
    //  PARAMS:
    //      - INPUT: 
    //                String: Msg
    //                String: Header
    //                MessageBoxButtons: MsgBoxButtons
    //                MessageBoxIcon: Icon
    //                Form: ParentForm
    //                GamingTableBusinessLogic.GT_SHOW_MESSAGE: MessageType
    //
    //      - OUTPUT: 
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    private DialogResult ShowValidateMsg(String Msg, String Header, MessageBoxButtons MsgBoxButtons, MessageBoxIcon Icon, Form ParentForm, GamingTableBusinessLogic.GT_SHOW_MESSAGE MessageType)
    {
      DialogResult _dialog_result;

      _dialog_result = DialogResult.OK;
      lbl_action_description.Text = "";

      if (Msg.Length == 0 || MessageType == GamingTableBusinessLogic.GT_SHOW_MESSAGE.HIDE)
      {
        return _dialog_result;
      }

      if (MessageType == GamingTableBusinessLogic.GT_SHOW_MESSAGE.MESSAGE_BOX)
      {
        _dialog_result = frm_message.Show(Msg, Header, MsgBoxButtons, Icon, ParentForm);
      }
      else if (MessageType == GamingTableBusinessLogic.GT_SHOW_MESSAGE.LABEL)
      {
        lbl_action_description.Text = Msg;
        lbl_action_description.Visible = true;
        timerClear.Enabled = true;
      }

      return _dialog_result;
    } // ShowValidateMsg

    //------------------------------------------------------------------------------
    // PURPOSE: Refresh windows
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    private void RefreshWindow()
    {
      if (m_seat != null && m_seat.PlayerType != GTPlayerType.Unknown)
      {
        // Set background color when Played current is not longer updated
        if (m_seat.PlaySession.PlaysCount - m_seat.PlaySession.LastPlaysCount > m_table_idle_plays || ValidateCurrentBet(false, false))
        {
          tb_current_bet.ForeColor = Color.DarkOrange;
          tb_current_bet.Font = new Font(tb_current_bet.Font, FontStyle.Bold);
        }
        else
        {
          tb_current_bet.ForeColor = Color.Black;
          tb_current_bet.Font = new Font(tb_current_bet.Font, FontStyle.Regular);
        }

        // Update the fields in the case when the thread was executed the user was making changes and can not updated
        if (tb_current_bet.BackColor != Color.LightGoldenrodYellow)
        {
          tb_current_bet.Text = m_seat.PlaySession.CurrentBet.ToString(false);
        }

        if (tb_chips_in.BackColor != Color.LightGoldenrodYellow)
        {
          tb_chips_in.Text = m_seat.PlaySession.ChipsIn.ToString(false);
        }

        if (tb_chips_out.BackColor != Color.LightGoldenrodYellow)
        {
          tb_chips_out.Text = m_seat.PlaySession.ChipsOut.ToString(false);
        }

        if (!cb_skill.Focused)
        {
          if (m_seat.PlaySession.PlayerSkill == GTPlaySessionPlayerSkill.Unknown)
          {
            cb_skill.SelectedValue = GTPlaySessionPlayerSkill.Average;
          }
          else
          {
            cb_skill.SelectedValue = m_seat.PlaySession.PlayerSkill;
          }
        }

        if (!cb_speed.Focused)
        {
          if (m_seat.PlaySession.PlayerSpeed == GTPlayerTrackingSpeed.Unknown)
          {
            cb_speed.SelectedValue = GTPlayerTrackingSpeed.Medium;
          }
          else
          {
            cb_speed.SelectedValue = m_seat.PlaySession.PlayerSpeed;
          }
        }

        //if (!cb_player_iso_code.Focused)
        //{
        //  cb_player_iso_code.SelectedValue = m_seat.PlaySession.PlayerIsoCode;
        //}

        SetControls();
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Draw seat image
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    private void DrawSeatImage()
    {
      GamingSeatProperties _seat = new GamingSeatProperties();

      if (m_seat != null)
      {
        _seat.SeatPosition = m_seat.SeatPosition;

        // Seat status
        if (m_seat.PlayerType == GTPlayerType.Unknown)
        {
          _seat.SeatStatus = SEAT_STATUS_PLAYER_TRACKING.FREE;
        }
        else if (m_seat.PlaySession.Started_walk != null)
        {
          _seat.SeatStatus = SEAT_STATUS_PLAYER_TRACKING.WALK;
        }
        else
        {
          _seat.SeatStatus = SEAT_STATUS_PLAYER_TRACKING.OCUPATED;
        }
      }
      else
      {
        _seat.SeatStatus = SEAT_STATUS_PLAYER_TRACKING.DISABLED;
      }

      pb_seat_img.Image = UtilsDesign.GetDrawSeatImage(_seat);

    }

    //------------------------------------------------------------------------------
    // PURPOSE: Restore parent focus
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    private void RestoreParentFocus()
    {
      this.ParentForm.Focus();
      this.ParentForm.BringToFront();
    }  // RestoreParentFocus

    //------------------------------------------------------------------------------
    // PURPOSE: Validate current bet
    // 
    //  PARAMS:
    //      - INPUT: 
    //               Boolean: UpdateCurrentBet, validate current bet is updated to Seat object
    //               Boolean: ShowMessage, if message must be shown
    //      - OUTPUT: 
    //               Boolean: true if current bet has validate and value is not between table limits
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    private Boolean ValidateCurrentBet(Boolean UpdateCurrentBet, Boolean ShowMessage)
    {
      Boolean _show_message;
      Boolean _is_validated;
      Currency _current_bet;

      _show_message = false;
      _is_validated = false;
      _current_bet = m_seat.PlaySession.CurrentBet;

      ShowMessage &= m_seat.PlayerType != GTPlayerType.Unknown;

      // Update first time
      if (!UpdateCurrentBet)
      {
        UpdateCurrentBet = m_seat.PlaySession.PlaysCount == 0;
      }

      if (m_seat.PlaySession.CurrentBet < 0)
      {
        _current_bet = 0;
        _show_message = true;
      }

      if (m_table_min_bet != 0 && m_seat.PlaySession.CurrentBet < m_table_min_bet)
      {
        _current_bet = m_table_min_bet;
        _show_message = true;
      }

      if (m_table_max_bet != 0 && m_seat.PlaySession.CurrentBet > m_table_max_bet)
      {
        _current_bet = m_table_max_bet;
        _show_message = true;
      }

      if (UpdateCurrentBet && (m_seat.PlaySession.CurrentBet != 0 || m_seat.PlaySession.PlaysCount > 0 || m_seat.PlaySession.Started_walk == null))
      {
        m_seat.PlaySession.CurrentBet = _current_bet;
        m_seat.HasPendingChanges = true;
      }
      else
      {
        _show_message = false;
      }

      _is_validated = _show_message;
      _show_message &= ShowMessage;

      tb_current_bet.Text = m_seat.PlaySession.CurrentBet.ToString(false);

      if (UpdateCurrentBet && m_show_msg_when_validate_values > 0 && _show_message)
      {
        ShowValidateMsg(Resource.String("STR_CASHIER_GAMING_TABLE_CURRENT_BET_INFO_MSG"), Resource.String("STR_APP_GEN_MSG_WARNING"), MessageBoxButtons.OK, MessageBoxIcon.Warning, this.ParentForm);
      }
      else if (!UpdateCurrentBet && m_show_msg_when_validate_values > 0 && _show_message)
      {
        if (!lbl_action_description.Visible)
        {
          lbl_action_description.Visible = true;
          lbl_action_description.Text = Resource.String("STR_CASHIER_GAMING_TABLE_MIN_MAX_EDITED_INFO_MSG");
          timerClear.Stop();
        }
      }
      else if (ShowMessage)
      {
        lbl_action_description.Visible = false;
      }

      return _is_validated;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Calculate fields: NetWin, Points, etc.
    // 
    //  PARAMS:
    //      - INPUT: 
    //
    //      - OUTPUT: 
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    private void CalculateFields()
    {
      Decimal _calculated_points;
      WonBucketDict _wonBucketList = new WonBucketDict();

      if (m_seat != null)
      {
        if (m_seat.PlayerType != GTPlayerType.Unknown)
        {
          // Validate Current Bet
          ValidateCurrentBet(false, true);

          // Calculated Points
          _calculated_points = 0;
          if (m_seat.PlayerType == GTPlayerType.Customized)
          {
            if (GamingTableBusinessLogic.Get_CalculateBuckets(m_seat, ref _wonBucketList) == GamingTableBusinessLogic.GT_RESPONSE_CODE.OK)
            {
              if (_wonBucketList.Dict.ContainsKey(Buckets.BucketId.RedemptionPoints))
              {
                _calculated_points = _wonBucketList.Dict[Buckets.BucketId.RedemptionPoints].WonValue;
              }
            }
          }

          if(m_seat.PlayerType == GTPlayerType.Customized)
          {
          m_seat.PlaySession.AwardedPointsCalculated = _calculated_points;
          }
          else
          {
            m_seat.PlaySession.AwardedPointsCalculated = 0;
          }          

          // NetWin
          m_seat.PlaySession.NetWin = m_seat.PlaySession.ChipsIn + m_seat.PlaySession.TotalSellChips - m_seat.PlaySession.ChipsOut;

          // Refresh Labels
          tb_min_played.Text = m_seat.PlaySession.PlayedMin.ToString(false);
          tb_max_played.Text = m_seat.PlaySession.PlayedMax.ToString(false);
          tb_total_bet.Text = m_seat.PlaySession.PlayedAmount.ToString(false);
          lbl_average_bet_value.Text = m_seat.PlaySession.PlayedAvg.ToString(false);
          lbl_plays_value.Text = m_seat.PlaySession.PlaysCount.ToString();

          if (!m_seat.PlaySession.Started_walk.HasValue && GamingTableBusinessLogic.GetAssignmentPointsMode() == GamingTableBusinessLogic.ASSIGNMENT_POINTS_MODE.AUTOMATIC)
          {
            tb_points_won.Text = m_seat.PlaySession.AwardedPointsCalculated.ToString();
          }

          if ((!m_points_edited_by_user) && (m_seat.PlaySession.Started_walk.HasValue) && GamingTableBusinessLogic.GetAssignmentPointsMode() == GamingTableBusinessLogic.ASSIGNMENT_POINTS_MODE.AUTOMATIC)
          {
            tb_points_won.Text = m_seat.PlaySession.AwardedPointsCalculated.ToString();
          }

          lbl_calculate_points_won.Text = m_seat.PlaySession.AwardedPointsCalculated.ToString();
          tb_netwin.Text = m_seat.PlaySession.NetWin.ToString(false);
        }
      }
    }

    private void UpdateRefreshFields(Control Control)
    {
      if (Control != null)
      {
        Control.BackColor = Control.DefaultBackColor;
      }

      SaveChanges();

      ValueChangedInSession(m_seat);
    }

    #endregion  // Private Methods

    #region Events

    private void btn_close_Click(object sender, EventArgs e)
    {
      Cursor _previous_cursor;
      GamingTableBusinessLogic.GT_RESPONSE_CODE _error_code;
      Decimal _points_awared;

      m_nfi = System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat;

      _previous_cursor = this.Cursor;

      try
      {
        this.Cursor = Cursors.WaitCursor;

        if (Decimal.TryParse(tb_points_won.Text, out _points_awared))
        {
          m_seat.PlaySession.AwardedPoints = _points_awared;
        }

        _error_code = GamingTableBusinessLogic.EndGTPlaySession(m_seat, Cashier.UserId, Cashier.TerminalId, Cashier.TerminalName);

        if (_error_code != GamingTableBusinessLogic.GT_RESPONSE_CODE.OK)
        {
          return;
        }

        m_seat.Init();

        ButtonClickedInPlayerInSession(m_seat, ButtonClicked.Close);

        DrawSeatImage();

        return;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {
        this.Cursor = _previous_cursor;
        this.Invalidate();
      }
      return;

    }  // btn_close_Click

    private void btn_play_pause_Click(object sender, EventArgs e)
    {
      Cursor _previous_cursor;
      GamingTableBusinessLogic.GT_RESPONSE_CODE _error_code;
      Decimal _input_value;

      _previous_cursor = this.Cursor;

      try
      {
        this.Cursor = Cursors.WaitCursor;

        Decimal.TryParse(tb_points_won.Text.Replace(m_nfi.CurrencySymbol, ""), out _input_value);
        m_seat.PlaySession.AwardedPoints = _input_value;
        m_points_edited_by_user = false;

        _error_code = GamingTableBusinessLogic.UpdateSeatWalk(m_seat, Cashier.UserId, Cashier.TerminalId);

        SetControls();

        ButtonClickedInPlayerInSession(m_seat, ButtonClicked.PlayPause);

        DrawSeatImage();
      }
      finally
      {
        this.Cursor = _previous_cursor;
        this.Invalidate();
      }
    }  // btn_play_pause_Click

    private void btn_swap_seat_Click(object sender, EventArgs e)
    {
      ButtonClickedInPlayerInSession(m_seat, ButtonClicked.Swap);

      DrawSeatImage();
    }  // btn_swap_seat_Click

    private void cb_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (m_seat != null)
      {
        m_seat.HasPendingChanges = true;
        if (sender.Equals(cb_speed))
        {
          if (m_seat.PlaySession.PlayerSpeed != (GTPlayerTrackingSpeed)int.Parse(cb_speed.SelectedValue.ToString()))
          {
            m_seat.PlaySession.PlayerSpeed = (GTPlayerTrackingSpeed)int.Parse(cb_speed.SelectedValue.ToString());
            SaveChanges();
            ValueChangedInSession(m_seat);
          }
        }
        if (sender.Equals(cb_skill))
        {
          if (m_seat.PlaySession.PlayerSkill != (GTPlaySessionPlayerSkill)int.Parse(cb_skill.SelectedValue.ToString()))
          {
            m_seat.PlaySession.PlayerSkill = (GTPlaySessionPlayerSkill)int.Parse(cb_skill.SelectedValue.ToString());
            SaveChanges();
            ValueChangedInSession(m_seat);
          }
        }
        if (sender.Equals(cb_player_iso_code))
        {
          if (m_seat.PlaySession.PlayerIsoCode != cb_player_iso_code.SelectedValue.ToString())
          {
           ValuePlayerIsoCodeChangedInSession(m_seat, m_seat.PlaySession.PlayerIsoCode, cb_player_iso_code.SelectedValue.ToString());
          }
        }
      }

      DrawSeatImage();
      btn_sell_chips.Enabled = cb_player_iso_code.Enabled & (m_is_TITO_mode || (cb_player_iso_code.Items.Count > 0 && m_national_currency == cb_player_iso_code.SelectedValue.ToString()));

    }  // cb_SelectedIndexChanged

    private void tb_TextChanged(object sender, CancelEventArgs e)
    {
      Decimal _input_value;
      m_nfi = System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat;

      try
      {
        _input_value = 0;

        if (sender.Equals(tb_current_bet))
        {
          Decimal.TryParse(tb_current_bet.Text.Replace(m_nfi.CurrencySymbol, ""), out _input_value);
          if (PlayedCurrent != _input_value)
          {
            PlayedCurrent = _input_value;
          }
        }
        if (sender.Equals(tb_points_won))
        {
          Decimal.TryParse(tb_points_won.Text.Replace(m_nfi.CurrencySymbol, ""), out _input_value);
          m_points_edited_by_user = true;
          if (PointsWon != _input_value)
          {
            PointsWon = _input_value;
          }
        }
        if (sender.Equals(tb_chips_in))
        {
          Decimal.TryParse(tb_chips_in.Text.Replace(m_nfi.CurrencySymbol, ""), out _input_value);
          if (ChipsIn == _input_value)
          {
            ChipsIn = _input_value;
          }
        }
        if (sender.Equals(tb_chips_out))
        {
          Decimal.TryParse(tb_chips_out.Text.Replace(m_nfi.CurrencySymbol, ""), out _input_value);
          if (ChipsOut == _input_value)
          {
            ChipsOut = _input_value;
          }
        }
      }
      finally
      {
        GC.Collect();
        ThreadPool.QueueUserWorkItem(delegate
        {
          GC.WaitForPendingFinalizers();
          GC.Collect();
        });
      }

    } // tb_TextChanged

    private void timerClear_Tick(object sender, EventArgs e)
    {
      m_tick_last_change += 1;
      if (m_tick_last_change > 10) // 5 seconds
      {
        lbl_action_description.Text = "";
        lbl_action_description.Visible = false;
        m_tick_last_change = 0;
        timerClear.Enabled = false;
      }
    }  // timerClear_Tick

    ////////////private void btn_buy_chips_Click(object sender, EventArgs e)
    ////////////{
    ////////////  CardData _card_data;
    ////////////  ChipsAmount _chips_amount;
    ////////////  ParamsTicketOperation _operations_params;
    ////////////  String _error_str;

    ////////////  // Check if current user is an authorized user
    ////////////  if (!ProfilePermissions.CheckPermissionList(ProfilePermissions.CashierFormFuncionality.ChipsPurchase,
    ////////////                                              ProfilePermissions.TypeOperation.RequestPasswd, this.ParentForm, 0, out _error_str))
    ////////////  {
    ////////////    return;
    ////////////  }

    ////////////  // Get Card data
    ////////////  if (m_seat.AccountId == 0)
    ////////////  {
    ////////////    return;
    ////////////  }

    ////////////  _card_data = new CardData();

    ////////////  if (!CardData.DB_CardGetAllData(m_seat.AccountId, _card_data))
    ////////////  {
    ////////////    return;
    ////////////  }

    ////////////  try
    ////////////  {
    ////////////    form_yes_no.Show(this.ParentForm);

    ////////////    if (!CashierChips.ChipsPurchase_GetAmount(_card_data, this.CheckPermissionsToRedeem, m_seat.GamingTableId, form_yes_no, out _chips_amount, out _operations_params))
    ////////////    {
    ////////////      return;
    ////////////    }

    ////////////    _operations_params.in_window_parent = this.ParentForm;
    ////////////    if (!CashierChips.ChipsPurchase_Save(_card_data, _chips_amount, _operations_params))
    ////////////    {
    ////////////      ShowValidateMsg(Resource.String("STR_GAMING_TABLE_BUY_CHIPS_ERROR"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this.ParentForm);
    ////////////      return;
    ////////////    }
    ////////////  }
    ////////////  finally
    ////////////  {
    ////////////    form_yes_no.Hide();
    ////////////    this.RestoreParentFocus();
    ////////////  }
    ////////////}  // btn_buy_chips_Click

    private void btn_sale_chips_Click(object sender, EventArgs e)
    {
      String _error_str;

      if (!ProfilePermissions.CheckPermissionList(ProfilePermissions.CashierFormFuncionality.Chips_Buy_In,
                                                  ProfilePermissions.TypeOperation.RequestPasswd,
                                                  this.ParentForm,
                                                  0,
                                                  out _error_str))
      {
        return;
      }

      if (!GeneralParam.GetBoolean("GamingTables", "Cashier.RegisterChipSaleAtTableEnabled", false) && !m_is_TITO_mode ||
        m_is_TITO_mode)
      {
        if (GamingTableBusinessLogic.IsEnableCashDeskDealerTicketsValidation())
        {
          TextBoxEnterEvent(0, TextBoxSelected.Btn_Buy_In, m_go_back);
        }
        else
        {
        TextBoxEnterEvent(0, TextBoxSelected.Chips_Sell, m_go_back);
      }
      }
      else
      {
        Sale_Chips(null);
      }
    }  // btn_sale_chips_Click

    public Boolean Sale_Chips(Ticket Ticket, Decimal Amount = 0)
    {
      FeatureChips.ChipsOperation _chips_amount;

      if (!m_is_TITO_mode && GeneralParam.GetBoolean("GamingTables", "Cashier.RegisterChipSaleAtTableEnabled", false))
      {
        if (!Chips_Sale_Register(out _chips_amount))
        {
          return false;
        }
        Amount = _chips_amount.ChipsAmount;
      }

      using (DB_TRX _db_trx = new DB_TRX())
      {
        if (Ticket != null)
        {
          if (!GamingTableBusinessLogic.PlayerTracking_SaleChips(Ticket, Seat, Cashier.UserId, Cashier.TerminalId, _db_trx))
        {
          return false;
        }

          ChipsSell = Ticket.Amount;

          // Show informative message
          ShowHideValidatedCopyDealerTicket(true, Ticket.Amount);
        }
        else
        {
          if (!GamingTableBusinessLogic.PlayerTracking_SaleChips(Seat, Cashier.UserId, Cashier.TerminalId, Amount, _db_trx))
          {
            return false;
          }
          ChipsSell = Amount;
        }
        _db_trx.Commit();
      }

      return true;
    } // Sale_Chips

    private Boolean Chips_Sale_Register(out FeatureChips.ChipsOperation ChipsAmount)
    {
      CardData _card_data;
      String _error_str;
      MessageBoxIcon _message_icon;
      CashierOperations.TYPE_CARD_CASH_IN _cash_operation;
      CurrencyExchangeResult _exchange_result;
      Boolean _ok;
      RechargeOutputParameters OutputParameter; //LTC 22-SEP-2016
      Ticket _ticket_copy_dealer;

      ChipsAmount = null;
      _cash_operation = new CashierOperations.TYPE_CARD_CASH_IN();
      _exchange_result = new CurrencyExchangeResult();
      _ticket_copy_dealer = null;

      // Check if current user is an authorized user
      if (!ProfilePermissions.CheckPermissionList(ProfilePermissions.CashierFormFuncionality.ChipsSaleRegister,
                                                  ProfilePermissions.TypeOperation.RequestPasswd, this.ParentForm, 0, out _error_str))
      {

        return false;
      }

      // Get Card data
      if (m_seat.AccountId == 0)
      {
        return false;
      }

      _card_data = new CardData();

      if (!CardData.DB_CardGetAllData(m_seat.AccountId, _card_data))
      {
        return false;
      }

      try
      {
        form_yes_no.Show(this.ParentForm);

        _ok = false;

        _ok = CashierChips.ChipsSaleRegister_GetAmount(_card_data, form_credit, VoucherTypes.ChipsSaleWithRecharge, m_seat.GamingTableId, form_yes_no, out ChipsAmount, out _cash_operation, out _exchange_result);

        if (!_ok)
        {
          return false;
        }

        _ok = CashierChips.ChipsSaleWithRecharge_Save(_card_data, form_yes_no, ChipsAmount, _cash_operation, _exchange_result, ChipSaleType.RegisterTableSale, out _error_str, out OutputParameter, ParticipateInCashDeskDraw.No, out _ticket_copy_dealer);

        if (!_ok)
        {
          _message_icon = MessageBoxIcon.Warning;

          // Generic error
          if (_error_str == String.Empty)
          {
            _error_str = Resource.String("STR_UC_CARD_USER_MSG_GENERIC_ERROR");
            _message_icon = MessageBoxIcon.Error;
          }

          frm_message.Show(_error_str,
                            Resource.String("STR_APP_GEN_MSG_WARNING"),
                            MessageBoxButtons.OK,
                            _message_icon,
                            ParentForm);

          return false;
        }
      }
      finally
      {
        form_yes_no.Hide();
        this.RestoreParentFocus();
      }

      return true;
    } // Sale_Chips_Register

    private Boolean Chips_Sale_TITO(out FeatureChips.ChipsOperation ChipsAmount)
    {
      CardData _card_data;
      String _error_str;
      MessageBoxIcon _message_icon;
      Boolean _chips_sale_mode;
      CashierOperations.TYPE_CARD_CASH_IN _cash_operation;
      CurrencyExchangeResult _exchange_result;
      Boolean _ok;
      RechargeOutputParameters OutputParameter; //LTC 22-SEP-2016
      Ticket _ticket_copy_dealer;

      ChipsAmount = null;
      _cash_operation = new CashierOperations.TYPE_CARD_CASH_IN();
      _exchange_result = new CurrencyExchangeResult();
      _ticket_copy_dealer = null;

      // Check if current user is an authorized user
      if (!ProfilePermissions.CheckPermissionList(ProfilePermissions.CashierFormFuncionality.ChipsSale,
                                                  ProfilePermissions.TypeOperation.RequestPasswd, this.ParentForm, 0, out _error_str))
      {

        return false;
      }

      _chips_sale_mode = GeneralParam.GetBoolean("GamingTables", "Cashier.Mode");

      if (!GamingTableBusinessLogic.HasChipsBalance(Cashier.SessionId, _chips_sale_mode))
      {
        frm_message.Show(Resource.String("STR_FRM_AMOUNT_INPUT_MSG_CHIPS_AMOUNT_EXCEED_BANK").Replace("\\r\\n", "\r\n"),
                Resource.String("STR_APP_GEN_MSG_WARNING"),
                MessageBoxButtons.OK,
                MessageBoxIcon.Warning,
                ParentForm);

        return false;
      }

      // Get Card data
      if (m_seat.AccountId == 0)
      {
        return false;
      }

      _card_data = new CardData();

      if (!CardData.DB_CardGetAllData(m_seat.AccountId, _card_data))
      {
        return false;
      }

      try
      {
        form_yes_no.Show(this.ParentForm);

        _ok = false;

        VoucherTypes _voucher_type;

        _voucher_type = VoucherTypes.ChipsSaleWithRecharge;
        _ok = CashierChips.ChipsSale_GetAmount(_card_data, form_credit, _voucher_type, m_seat.GamingTableId, form_yes_no, out ChipsAmount, out _cash_operation, out _exchange_result);

        if (!_ok)
        {
          return false;
        }

        _ok = CashierChips.ChipsSaleWithRecharge_Save(_card_data, form_yes_no, ChipsAmount, _cash_operation, _exchange_result, ChipSaleType.CashDeskSale, out _error_str, out OutputParameter, ParticipateInCashDeskDraw.No, out _ticket_copy_dealer);

        if (!_ok)
        {
          _message_icon = MessageBoxIcon.Warning;

          // Generic error
          if (_error_str == String.Empty)
          {
            _error_str = Resource.String("STR_UC_CARD_USER_MSG_GENERIC_ERROR");
            _message_icon = MessageBoxIcon.Error;
          }

          frm_message.Show(_error_str,
                            Resource.String("STR_APP_GEN_MSG_WARNING"),
                            MessageBoxButtons.OK,
                            _message_icon,
                            ParentForm);

          return false;
        }
      }
      finally
      {
        form_yes_no.Hide();
        this.RestoreParentFocus();
      }

      return true;
    }

    private void timerRefresh_Tick(object sender, EventArgs e)
    {
      RefreshWindow();
    } // timerRefresh_Tick

    private void SetMouseDownEventRecursive(ControlCollection Controls)
    {
      foreach (Control _control in Controls)
      {
        if (_control is WSI.Cashier.Controls.uc_round_textbox)
        {
          _control.MouseDown += new MouseEventHandler(Control_MouseDown);
        }
        else if (_control.Controls != null && _control.Controls.Count > 0)
        {
          SetMouseDownEventRecursive(_control.Controls);
        }
        else
        {
          _control.MouseDown += new MouseEventHandler(Control_MouseDown);
        }
      }
    }

    private void pb_user_Click(object sender, EventArgs e)
    {
      if (GeneralParam.GetBoolean("Account", "PlayerPicture.AllowZoom", false))
      {
        if (m_exist_photo)
        {
          frm_account_photo frm = new frm_account_photo(m_seat.HolderName, this.pb_user.Image);
          frm.ShowDialog();
        }
      }
    }

    #endregion  // Events

  } // class uc_gt_player_in_session  

} // namespace


