//////////------------------------------------------------------------------------------
////////// Copyright � 2010 Win Systems Ltd.
//////////------------------------------------------------------------------------------
////////// 
//////////   MODULE NAME: frm_draw_voucher.cs
////////// 
//////////   DESCRIPTION: Implements the following classes:
//////////                1. frm_draw_voucher
//////////
//////////        AUTHOR: MBF
////////// 
////////// CREATION DATE: 09-JUN-2010
////////// 
////////// REVISION HISTORY:
////////// 
////////// Date        Author Description
////////// ----------- ------ ----------------------------------------------------------
////////// 09-JUN-2010 MBF     First release.
//////////------------------------------------------------------------------------------
////////using System;
////////using System.Collections.Generic;
////////using System.ComponentModel;
////////using System.Data;
////////using System.Drawing;
////////using System.Text;
////////using System.Windows.Forms;
////////using System.Resources;
////////using System.IO;
////////using System.Data.SqlClient;
////////using WSI.Common;

////////namespace WSI.Cashier
////////{
////////  public partial class frm_draw_voucher : Form
////////  {
////////    #region Attributes

////////      DrawTicket m_draw_ticket;
////////      Boolean    m_print_voucher;
      
////////    #endregion 

////////    #region Constructor

////////    /// <summary>
////////    /// Constructor method.
////////    /// 1. Initialize controls.
////////    /// 2. Initialize control�s resources. (NLS, Images, etc)
////////    /// </summary>
////////    public frm_draw_voucher(CardData Card)
////////    {
////////      InitializeComponent();

////////      InitializeControlResources();

////////      m_print_voucher = true;

////////      LoadTicket(Card);
////////    }

////////    /// <summary>
////////    /// Constructor method.
////////    /// 1. Initialize controls.
////////    /// 2. Initialize control�s resources. (NLS, Images, etc)
////////    /// </summary>
////////    public frm_draw_voucher(DrawTicket DrawTicket)
////////    {
////////      InitializeComponent();

////////      InitializeControlResources();

////////      m_draw_ticket = DrawTicket;
////////      m_print_voucher = false;

////////      ShowVoucher();
////////    }

////////    #endregion

////////    #region Private Methods

////////    /// <summary>
////////    /// Initialize control resources (NLS strings, images, etc)
////////    /// </summary>
////////    private void InitializeControlResources()
////////    {
////////      // - NLS Strings:
////////      //   - Title:
////////      lbl_browser_title.Text = Resource.String("STR_VOUCHER_CARD_DRAW_TITLE");

////////      //   - Labels

////////      //   - Buttons
////////       btn_close.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CLOSE");

////////      // - Images:

////////    } // InitializeControlResources

////////    //------------------------------------------------------------------------------
////////    // PURPOSE: Load ticket information and calls show
////////    // 
////////    //  PARAMS:
////////    //      - INPUT:
////////    //
////////    //      - OUTPUT:
////////    //
////////    // RETURNS:
////////    // 
////////    //   NOTES:
////////    private void LoadTicket(CardData Card)
////////    {
////////      SqlConnection _sql_conn;
////////      SqlTransaction _sql_trx;
////////      String _str;
////////      Images.CashierImage _cashier_image;

////////      _sql_conn = null;
////////      _sql_trx = null;

////////      try
////////      {
////////        _sql_conn = WGDB.Connection();
////////        _sql_trx = _sql_conn.BeginTransaction();

////////        m_draw_ticket = DrawTicketLogic.CreateTicket(_sql_trx, Card);

////////        if (m_draw_ticket.CreationCode == DrawTicketLogic.GetTicketCodes.OK)
////////        {
////////          _sql_trx.Commit();
////////        }
////////      }
////////      catch (Exception _ex)
////////      {
////////        Log.Exception(_ex);
////////      }
////////      finally
////////      {
////////        if (_sql_trx != null)
////////        {
////////          if (_sql_trx.Connection != null)
////////          {
////////            try { _sql_trx.Rollback(); }
////////            catch { }
////////          }
////////          _sql_trx.Dispose();
////////          _sql_trx = null;
////////        }

////////        // Close connection
////////        if (_sql_conn != null)
////////        {
////////          try { _sql_conn.Close(); }
////////          catch { }
////////          _sql_conn = null;
////////        }
////////      }

////////      switch (m_draw_ticket.CreationCode)
////////      {
////////        case DrawTicketLogic.GetTicketCodes.OK:
////////          ShowVoucher();
////////          return;

////////        default:
////////          _str = DrawTicketLogic.GetMsgError(m_draw_ticket, out _cashier_image);
////////          frm_message.Show(_str, Resource.String("STR_VOUCHER_ERROR_TITLE"), MessageBoxButtons.OK,
////////                           _cashier_image, Cashier.MainForm);
////////          break;
////////      }

////////      Close();
////////    } // LoadTicket

////////    //------------------------------------------------------------------------------
////////    // PURPOSE: Show voucher
////////    // 
////////    //  PARAMS:
////////    //      - INPUT:
////////    //
////////    //      - OUTPUT:
////////    //
////////    // RETURNS:
////////    // 
////////    //   NOTES:
////////    private void ShowVoucher()
////////    {
////////      String _voucher_file_name;

////////      _voucher_file_name = "";

////////      if (m_draw_ticket.Voucher != null)
////////      {
////////        _voucher_file_name = m_draw_ticket.Voucher.GetFileName();
////////        web_browser.Navigate(_voucher_file_name);

////////        if (m_print_voucher)
////////        {
////////          VoucherPrint.Print(m_draw_ticket.Voucher);
////////        }
////////      }

////////    } // ShowVoucher

////////    //------------------------------------------------------------------------------
////////    // PURPOSE: Close button click implementation
////////    // 
////////    //  PARAMS:
////////    //      - INPUT:
////////    //
////////    //      - OUTPUT:
////////    //
////////    // RETURNS:
////////    // 
////////    //   NOTES:
////////    private void btn_close_Click(object sender, EventArgs e)
////////    {
////////      Close();
////////    } // btn_close_Click

////////    #endregion

////////    #region Public Methods

////////    #endregion

////////  }
////////}