﻿namespace WSI.Cashier
{
  partial class uc_gt_dealer_copy_ticket_reader
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
      this.lbl_head = new WSI.Cashier.Controls.uc_label();
      this.pb_close = new System.Windows.Forms.PictureBox();
      this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
      this.panel1 = new System.Windows.Forms.Panel();
      this.lbl_invalid_ticket = new WSI.Cashier.Controls.uc_label();
      this.pictureBox1 = new System.Windows.Forms.PictureBox();
      this.txt_validation_number = new WSI.Cashier.Controls.uc_round_textbox();
      this.lbl_swipe_ticket = new WSI.Cashier.Controls.uc_label();
      this.tmr_clear = new System.Windows.Forms.Timer(this.components);
      this.tableLayoutPanel4.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pb_close)).BeginInit();
      this.tableLayoutPanel1.SuspendLayout();
      this.panel1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
      this.SuspendLayout();
      // 
      // tableLayoutPanel4
      // 
      this.tableLayoutPanel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.tableLayoutPanel4.ColumnCount = 2;
      this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 38F));
      this.tableLayoutPanel4.Controls.Add(this.lbl_head, 0, 0);
      this.tableLayoutPanel4.Controls.Add(this.pb_close, 1, 0);
      this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 0);
      this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(0);
      this.tableLayoutPanel4.Name = "tableLayoutPanel4";
      this.tableLayoutPanel4.RowCount = 1;
      this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
      this.tableLayoutPanel4.Size = new System.Drawing.Size(282, 38);
      this.tableLayoutPanel4.TabIndex = 26;
      // 
      // lbl_head
      // 
      this.lbl_head.AutoSize = true;
      this.lbl_head.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.lbl_head.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_head.Font = new System.Drawing.Font("Open Sans Semibold", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_head.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(167)))), ((int)(((byte)(221)))), ((int)(((byte)(241)))));
      this.lbl_head.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
      this.lbl_head.Location = new System.Drawing.Point(0, 0);
      this.lbl_head.Margin = new System.Windows.Forms.Padding(0);
      this.lbl_head.Name = "lbl_head";
      this.lbl_head.Size = new System.Drawing.Size(244, 38);
      this.lbl_head.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.lbl_head.TabIndex = 23;
      this.lbl_head.Text = "xDealerCopyValidation";
      this.lbl_head.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // pb_close
      // 
      this.pb_close.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.pb_close.Location = new System.Drawing.Point(244, 0);
      this.pb_close.Margin = new System.Windows.Forms.Padding(0);
      this.pb_close.MinimumSize = new System.Drawing.Size(35, 35);
      this.pb_close.Name = "pb_close";
      this.pb_close.Padding = new System.Windows.Forms.Padding(1, 1, 1, 5);
      this.pb_close.Size = new System.Drawing.Size(35, 35);
      this.pb_close.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
      this.pb_close.TabIndex = 0;
      this.pb_close.TabStop = false;
      this.pb_close.Click += new System.EventHandler(this.pb_close_Click);
      // 
      // tableLayoutPanel1
      // 
      this.tableLayoutPanel1.ColumnCount = 1;
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
      this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel4, 0, 0);
      this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 1);
      this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
      this.tableLayoutPanel1.Name = "tableLayoutPanel1";
      this.tableLayoutPanel1.RowCount = 2;
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.tableLayoutPanel1.Size = new System.Drawing.Size(280, 290);
      this.tableLayoutPanel1.TabIndex = 27;
      // 
      // panel1
      // 
      this.panel1.Controls.Add(this.lbl_invalid_ticket);
      this.panel1.Controls.Add(this.pictureBox1);
      this.panel1.Controls.Add(this.txt_validation_number);
      this.panel1.Controls.Add(this.lbl_swipe_ticket);
      this.panel1.Location = new System.Drawing.Point(3, 41);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(276, 241);
      this.panel1.TabIndex = 27;
      // 
      // lbl_invalid_ticket
      // 
      this.lbl_invalid_ticket.BackColor = System.Drawing.Color.Transparent;
      this.lbl_invalid_ticket.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_invalid_ticket.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.lbl_invalid_ticket.Location = new System.Drawing.Point(26, 142);
      this.lbl_invalid_ticket.Name = "lbl_invalid_ticket";
      this.lbl_invalid_ticket.Size = new System.Drawing.Size(222, 60);
      this.lbl_invalid_ticket.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_RED;
      this.lbl_invalid_ticket.TabIndex = 10;
      this.lbl_invalid_ticket.Text = "Número de ticket no válido ...";
      this.lbl_invalid_ticket.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // pictureBox1
      // 
      this.pictureBox1.Image = global::WSI.Cashier.Properties.Resources.ticketsTito_header;
      this.pictureBox1.Location = new System.Drawing.Point(8, 16);
      this.pictureBox1.Name = "pictureBox1";
      this.pictureBox1.Size = new System.Drawing.Size(50, 50);
      this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
      this.pictureBox1.TabIndex = 8;
      this.pictureBox1.TabStop = false;
      // 
      // txt_validation_number
      // 
      this.txt_validation_number.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_validation_number.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_validation_number.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_validation_number.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_validation_number.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_validation_number.CornerRadius = 5;
      this.txt_validation_number.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_validation_number.Location = new System.Drawing.Point(22, 81);
      this.txt_validation_number.MaxLength = 32767;
      this.txt_validation_number.Multiline = false;
      this.txt_validation_number.Name = "txt_validation_number";
      this.txt_validation_number.PasswordChar = '\0';
      this.txt_validation_number.ReadOnly = false;
      this.txt_validation_number.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_validation_number.SelectedText = "";
      this.txt_validation_number.SelectionLength = 0;
      this.txt_validation_number.SelectionStart = 0;
      this.txt_validation_number.Size = new System.Drawing.Size(226, 40);
      this.txt_validation_number.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_validation_number.TabIndex = 9;
      this.txt_validation_number.TabStop = false;
      this.txt_validation_number.Text = "12345678901234567890";
      this.txt_validation_number.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.txt_validation_number.UseSystemPasswordChar = false;
      this.txt_validation_number.WaterMark = null;
      this.txt_validation_number.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      this.txt_validation_number.TextChanged += new System.EventHandler(this.txt_track_number_TextChanged);
      this.txt_validation_number.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_track_number_KeyPress);
      this.txt_validation_number.Click += new System.EventHandler(this.txt_validation_number_Click);
      // 
      // lbl_swipe_ticket
      // 
      this.lbl_swipe_ticket.BackColor = System.Drawing.Color.Transparent;
      this.lbl_swipe_ticket.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_swipe_ticket.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_swipe_ticket.Location = new System.Drawing.Point(97, 31);
      this.lbl_swipe_ticket.Name = "lbl_swipe_ticket";
      this.lbl_swipe_ticket.Size = new System.Drawing.Size(169, 20);
      this.lbl_swipe_ticket.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_swipe_ticket.TabIndex = 7;
      this.lbl_swipe_ticket.Text = "xValidation Number";
      // 
      // tmr_clear
      // 
      this.tmr_clear.Interval = 250;
      this.tmr_clear.Tick += new System.EventHandler(this.tmr_clear_Tick);
      // 
      // uc_gt_dealer_copy_ticket_reader
      // 
      this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.Controls.Add(this.tableLayoutPanel1);
      this.Name = "uc_gt_dealer_copy_ticket_reader";
      this.Size = new System.Drawing.Size(280, 290);
      this.VisibleChanged += new System.EventHandler(this.uc_ticket_reader_VisibleChanged);
      this.tableLayoutPanel4.ResumeLayout(false);
      this.tableLayoutPanel4.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pb_close)).EndInit();
      this.tableLayoutPanel1.ResumeLayout(false);
      this.panel1.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
    private System.Windows.Forms.PictureBox pb_close;
    private Controls.uc_label lbl_head;
    private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    private System.Windows.Forms.Panel panel1;
    private Controls.uc_label lbl_invalid_ticket;
    private System.Windows.Forms.PictureBox pictureBox1;
    private Controls.uc_round_textbox txt_validation_number;
    private Controls.uc_label lbl_swipe_ticket;
    private System.Windows.Forms.Timer tmr_clear;

  }
}
