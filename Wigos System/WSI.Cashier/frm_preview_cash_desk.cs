//------------------------------------------------------------------------------
// Copyright � 2007-2010 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_preview_cash_desk.cs
// 
//   DESCRIPTION: Form for preview cash desk
// 
//        AUTHOR: Jesus Blanco
// 
// CREATION DATE: 08-AUG-2013
// 
// REVISION HISTORY:
// 
// Date         Author  Description
// -----------  ------  ----------------------------------------------------------
// 08-AUG-2013  JBC     First version. 
// 17-AUG-2015  DCS     Fixed Bug: WIG-2181 The barcode is not printed when it hides the currency symbol
// 22-SEP-2016  LTC     Product Backlog Item 17461: Gamingtables 46 - Cash desk draw for gamblign tables: Chips purchase
//--------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Globalization;
using WSI.Common;
using System.Data.SqlClient;
using System.Diagnostics;

using Microsoft.Win32;

namespace WSI.Cashier
{
  public partial class frm_preview_cash_desk : Controls.frm_base
  {
    #region Constructor

    /// <summary>
    /// Constructor method.
    /// 1. Initialize controls.
    /// </summary>
    public frm_preview_cash_desk(Result DrawResult)
    {
      VoucherDrawCashDesk _preview;

      InitializeComponent();

      InitializeControlResources();

      _preview = new VoucherDrawCashDesk(DrawResult, false, CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION.DRAW_00); //LTC 22-SEP-2016

      // DHA 12-FEB-2014: Added general param to hide currency symbol
      if (GeneralParam.GetBoolean("CashDesk.Draw" + CashDeskDrawBusinessLogic.GetCashDeskPostfij(Convert.ToInt16(CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION.DRAW_00)), "Voucher.HideCurrencySymbol", false) && //LTC 22-SEP-2016
             !String.IsNullOrEmpty(NumberFormatInfo.CurrentInfo.CurrencySymbol))
      {
        _preview.ReplaceCurrencySimbol("");
      }

      web_browser.DocumentText = _preview.VoucherHTML;
    }

    #endregion // Constructor

    #region Buttons

    private void btn_ok_Click(object sender, EventArgs e)
    {
      Misc.WriteLog("[FORM CLOSE] frm_preview_cash_desk (ok)", Log.Type.Message);
      this.Close();
    }
    
    #endregion // Buttons

    #region Private Methods

    
    /// <summary>
    /// Initialize control resources (NLS strings, images, etc)
    /// </summary>
    private void InitializeControlResources()
    {
      // - NLS Strings:
      
      //  -Labels
      this.FormTitle = Resource.String("STR_FRM_PREVIEW_CASH_DESK_DRAW_TITLE");

      //  - Buttons
      btn_ok.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_OK");
    }

    #endregion // Private Methods

  }
}