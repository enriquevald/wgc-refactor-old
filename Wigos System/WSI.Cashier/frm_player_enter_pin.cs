//------------------------------------------------------------------------------
// Copyright � 2011 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_player_enter_pin.cs
// 
//   DESCRIPTION: Show a window to enter the player PIN code
//
//        AUTHOR: Ra�l Cervera
// 
// CREATION DATE: 23-NOV-2011
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 23-NOV-2011 RCI    First release.
// 25-NOV-2011 JMM    Avoiding autocheck.  Click on OK mandatory
// 28-NOV-2011 JMM    Moving PIN length constants to WSI.Common.Accounts
// 17-MAR-2014 JRM    Added new functionality. Description contained in RequestPinCodeDUT.docx DUT
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WSI.Cashier.Controls;
using WSI.Common;

namespace WSI.Cashier
{
  public partial class frm_player_enter_pin : frm_base
  {
    #region Attributes

    private Int64 m_account_id;

    private Int32 m_tick_count;

    private Accounts.PinRequestOperationType m_pin_operation_type;

    #endregion // Attributes

    #region enums

    #endregion // enums

    #region Constructor

    public frm_player_enter_pin(Int64 AccountId,
                                Currency Amount,
                                Accounts.PinRequestOperationType OperationType)
    {
      m_pin_operation_type = OperationType;

      InitializeComponent();

      InitializeControlResources();

      Init();

      m_account_id = AccountId;

      txt_pin_value.Text = "";
      lbl_amount_value.Text = Amount.ToString();

      btn_ok.Enabled = false;
    }

    #endregion // Constructor

    #region Private Methods

    /// <summary>
    /// Initialize control resources (NLS strings, images, etc)
    /// </summary>
    private void InitializeControlResources()
    {
      switch (m_pin_operation_type)
      {
        case Accounts.PinRequestOperationType.GIFT_REQUEST:
          lbl_text_to_read.Text = Resource.String("STR_FRM_PLAYER_ENTER_PIN_TEXT_TO_READ_IN_GIFT_REQUEST_MODE");
          lbl_amount.Visible = false;
          lbl_amount_value.Visible = false;

          break;

        case Accounts.PinRequestOperationType.WITHDRAWAL:
          lbl_text_to_read.Text = Resource.String("STR_FRM_PLAYER_ENTER_PIN_TEXT_TO_READ_IN_WITHDRAWL_MODE");
          lbl_amount.Text = Resource.String("STR_FRM_PLAYER_ENTER_PIN_AMOUNT") + ":";
          
          break;

        case Accounts.PinRequestOperationType.PLAY:

          return;

        default:
          Log.Message("frm_player_enter-InitializeControlResources: Unexpected operation type: " + m_pin_operation_type.ToString());
          break;

      }

      lbl_enter_pin.Text = Resource.String("STR_FRM_PLAYER_ENTER_PIN_MSG");
      this.FormTitle = Resource.String("STR_FRM_PLAYER_ENTER_PIN_TITLE");
      btn_ok.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_OK");
      btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");      

    } // InitializeControlResources

    private void Init()
    {
      this.Location = new Point(1024 / 2 - this.Width / 2, 768 / 2 - this.Height / 2 - 90);

      WSIKeyboard.Show(); 
    } // Init

    /// <summary>
    /// Checks if the input is completely numeric.
    /// </summary>
    /// <param name="TrackNumber"></param>
    /// <returns></returns>
    private Boolean PartialPinNumberIsValid(String PinNumber)
    {
      Byte[] chars;

      chars = null;

      try
      {
        chars = Encoding.ASCII.GetBytes(PinNumber);
        foreach (byte b in chars)
        {
          if (b < '0')
          {
            return false;
          }
          if (b > '9')
          {
            return false;
          }
        }
      }
      finally
      {
        chars = null;
      }

      return true;
    } // PartialPinNumberIsValid

    private void ShowLabelMessage(String Message)
    {
      btn_cancel.Focus();

      lbl_status.Visible = true;
      lbl_status.Text = Message;
      tmr_status_label.Start();
    } // ShowLabelMessage

    private void CheckPin()
    {
      PinCheckStatus _status_pin;

      _status_pin = Accounts.DB_CheckAccountPIN(m_account_id, txt_pin_value.Text);

      lbl_enter_pin.Visible = false;

      switch (_status_pin)
      {
        case PinCheckStatus.OK:
          WSIKeyboard.Hide();
          DialogResult = DialogResult.OK;
          break;

        case PinCheckStatus.WRONG_PIN:
          txt_pin_value.Text = "";
          btn_ok.Enabled = false;
          ShowLabelMessage(Resource.String("STR_FRM_PLAYER_ENTER_PIN_ERROR_INVALID"));
          break;

        case PinCheckStatus.ACCOUNT_BLOCKED:
          txt_pin_value.Text = "";
          txt_pin_value.Enabled = false;
          btn_ok.Enabled = false;
          ShowLabelMessage(Resource.String("STR_FRM_PLAYER_ENTER_PIN_ERROR_INVALID_AND_BLOCK"));
          break;

        case PinCheckStatus.ERROR:
        default:
          txt_pin_value.Text = "";
          txt_pin_value.Enabled = false;
          ShowLabelMessage(Resource.String("STR_FRM_PLAYER_ENTER_PIN_INTERNAL_ERROR"));
          break;
      }
    } // CheckPin

    private void StopTimer()
    {
      m_tick_count = 0;
      tmr_status_label.Stop();
      lbl_status.Visible = false;
      lbl_status.Text = "";
    } // StopTimer

    #region Events

    /// <summary>
    /// Key pressed management.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void txt_pin_number_KeyPress(object sender, KeyPressEventArgs e)
    {
      String pin_number;

      pin_number = txt_pin_value.Text + e.KeyChar;

      switch (e.KeyChar)
      {
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
          StopTimer();
          if (PartialPinNumberIsValid(pin_number) && (pin_number.Length <= Accounts.PIN_MAX_VALIDATING_LENGTH))
          {
            txt_pin_value.Text = txt_pin_value.Text + e.KeyChar.ToString(); //.AppendText(e.KeyChar.ToString());
          }
          e.Handled = true;

          if (pin_number.Length >= Accounts.PIN_MIN_LENGTH)
          {
            //JMM 25-NOV-2011: Remove autocheck when getting PIN_NUMBER_LENGTH.  Press OK is mandatory
            //CheckPin();

            btn_ok.Enabled = true;
          }
          break;

        case '\b':
          // Backspace is allowed to go through, others are not
          StopTimer();

          if (txt_pin_value.Text.Length - 1 < Accounts.PIN_MIN_LENGTH)
          {
            btn_ok.Enabled = false;
          }
          break;

        case '\r':
          CheckPin();
          break;

        default:
          e.KeyChar = '\0';
          break;
      } // switch
    } // txt_pin_number_KeyPress

    private void btn_ok_Click(object sender, EventArgs e)
    {
      CheckPin();
    } //btn_OK_Click

    private void btn_cancel_Click(object sender, EventArgs e)
    {
      WSIKeyboard.Hide();

      if (txt_pin_value.Enabled)
      {
        DialogResult = DialogResult.Cancel;
      }
      else
      {
        DialogResult = DialogResult.Abort;
      }
    } // btn_cancel_Click

    private void frm_player_enter_pin_Shown(object sender, EventArgs e)
    {
      this.txt_pin_value.Focus();
    } // frm_player_enter_pin_Shown

    private void tmr_status_label_Tick(object sender, EventArgs e)
    {
      m_tick_count++;

      if (m_tick_count == 5)
      {
        StopTimer();

        if (!txt_pin_value.Enabled)
        {
          DialogResult = DialogResult.Abort;
          WSIKeyboard.Hide();
        }
        else
        {
          lbl_enter_pin.Visible = true;
          txt_pin_value.Focus();
        }
      }
      else if (m_tick_count == 2)
      {
        if (txt_pin_value.Enabled)
        {
          lbl_enter_pin.Visible = true;
          txt_pin_value.Focus();
        }
      }
    } // tmr_status_label_Tick

    #endregion // Events

    #endregion // Private Methods

  } // frm_player_enter_pin
} // WSI.Cashier
