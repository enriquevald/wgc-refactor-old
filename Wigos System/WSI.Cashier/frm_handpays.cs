//------------------------------------------------------------------------------
// Copyright � 2010 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : frm_handpays.cs
// 
//   DESCRIPTION : Implements the following classes:
//                 1. frm_handpays: List of candidate sessions to apply handpay
//
// REVISION HISTORY :
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 02-JUL-2010 RCI    First release.
// 19-JUL-2010 TJG    Handpays implementation
// 23-NOV-2011 RCI    Optional sections in Handpay vouchers: signatures, game meters and play session.
// 02-DEC-2011 JMM    Adding denomination to Handpay vouchers
// 27-FEB-2012 RCI    Use Store Procedure to select pending handpays
// 25-MAY-2012 SSC    Moved CardData, PlaySession and PlayerTrackingData classes to WSI.Common
// 31-AUG-2012 MPO    Fixed bug: On create/cancel handpay it didn't update correctly play_sessions
// 27-MAR-2013 RBG    Create operation for handpays and handpays cancelation.
// 03-JUL-2013 RRR    Moddified view parameters for Window Mode
// 15-JUL-2013 LEM    Moved RegisterOrCancel, HandpayRegisterOrCancelParameters and GeName to Common.Handpays.
// 11-JUL-2014 JMM    Fixed Bug #WIGOSTITO-1244: Error on pay handpay when there is no playsession and GP to print played amount is active
// 11-JUL-2014 AMF    Regionalization
// 04-SEP-2014 JCO    Added level and progressive_id.
// 08-SEP-2014 JCO    Added hp_id.
// 08-SEP-2014 FJC    HANDPAY_TYPE: 'MANUAL_PROGRESSIVE' should not appear.
// 09-OCT-2014 OPC    Added news HANDPAY_TYPE not allowed for cancel.
// 15-OCT-2014 MPO    WIG-1503: Incorrect message when the terminal is playing.
// 16-JAN-2015 JMM & MPO  Heed on partial pay field comming from the 1B SAS poll
// 03-NOV-2015 JMV    Product Backlog Item 5884:Dise�o cajero: Aplicar en frm_handpays.cs
// 11-NOV-2015 FOS    Product Backlog Item: 3709 Payment tickets & handpays
// 23-NOV-2015 FOS    Bug 6771:Payment tickets & handpays
// 18-DEC-2015 FOS    Fixed Bug 6769: It doesn't delete taxes when TITO tickets are paid
// 24-FEB-2016 EOR    Product Backlog Item 7402:SAS20: Add Comments HandPays
// 22-MAR-2016 RAB    Bug 9140: Do not work manual Cashless payments
// 30-NOV-2016 ETP    Bug 21102 Old handpays are not payed after update
// 16-SEP-2017 DPC    [WIGOS-4274]: [Ticket #855] Error encontrado Estad�sticas - Progresivos
// 23-NOV-2017 RAB    Bug 30896:WIGOS-6573 [Ticket #10452] Falla al pagar pagos manuales de entrada manual
// 12-JUN-2018 DLM    Fix The list in the Handpay screen does not change its content.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using WSI.Common;
using WSI.Cashier.Controls;

namespace WSI.Cashier
{
  public partial class frm_handpays : frm_base
  {
    private const Int32 GRID_MAX_COLUMNS = 6;

    // Common columns for both handpays (pending and last paid).
    private const Int32 GRID_COLUMN_TERM_ID = 0;
    private const Int32 GRID_COLUMN_DATE = 1;
    private const Int32 GRID_COLUMN_TERM_PROVIDER = 2;
    private const Int32 GRID_COLUMN_TERM_NAME = 3;
    private const Int32 GRID_COLUMN_TYPE = 4;
    private const Int32 GRID_COLUMN_TYPE_NAME = 5;
    private const Int32 GRID_COLUMN_SITE_JACKPOT_NAME = 6;
    private const Int32 GRID_COLUMN_AMOUNT = 7;
    // Only for the pending handpays.
    private const Int32 GRID_COLUMN_SITE_JACKPOT_AWARDED_ON_TERMINAL_ID = 8;
    // RAB 22-MAR-2016: Bug 9140
    private const Int32 GRID_COLUMN_AMT0 = 9;
    // Only for the last paid handpay.
    private const Int32 GRID_COLUMN_MOV_ID = 8;
    private const Int32 GRID_COLUMN_PLAY_SESSION_ID = 9;
    private const Int32 GRID_COLUMN_LEVEL_JACKPOT = 10;

    #region Attributes

    private frm_yesno form_yes_no;
    private static CardData m_card;

    #endregion

    #region Constructor

    /// <summary>
    /// Constructor method.
    /// 1. Initialize controls.
    /// 2. Initialize control�s resources. (NLS, Images, etc)
    /// </summary>
    public frm_handpays(CardData RelatedCard)
    {
      InitializeComponent();

      InitializeControlResources();

      m_card = RelatedCard;

      form_yes_no = new frm_yesno();
      form_yes_no.Opacity = 0.6;
    }

    #endregion

    #region Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Initialize control resources (NLS strings, images, etc).
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    private void InitializeControlResources()
    {
      // NLS Strings
      //   - Title
      //   - Frames
      //   - Buttons

      //   - Title

      this.FormTitle = Resource.String("STR_FRM_HANDPAYS_001");    // Handpays

      //   - Frames
      gb_pending_handpays.HeaderText = Resource.String("STR_FRM_HANDPAYS_023");    // Pending Handpays
      gb_last_handpay.HeaderText = Resource.String("STR_FRM_HANDPAYS_024");    // Pending Handpays

      //   - Buttons
      btn_close.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CLOSE");
      btn_pay.Text = Resource.String("STR_FRM_HANDPAYS_007");            // Pay
      btn_manual.Text = Resource.String("STR_FRM_HANDPAYS_008");            // Manual Entry
      btn_cancel_handpay.Text = Resource.String("STR_FRM_HANDPAYS_012");    // Cancel

    } // InitializeControlResources

    #endregion

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Init and Show the form.
    //
    //  PARAMS :
    //      - INPUT : ParentForm
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //

    public void refreshHandPays()
    {
     
        String _sql_query;
        SqlConnection _sql_conn;
        SqlCommand _sql_command;
        DataSet _ds;
        SqlDataAdapter _da;
        String _date_format;
        int _handpay_time_period = 0;
        int _handpay_cancel_time_period = 0;
        Int32 _idx_row;
        HANDPAY_TYPE _handpay_type;

        _sql_conn = null;
        _ds = null;
        _da = null;

        try
        {
            // Get the Handpay settings from the General Parameters table
            Handpays.GetParameters(out _handpay_time_period, out _handpay_cancel_time_period);

            // Get Sql Connection 
            _sql_conn = WGDB.Connection();
            _ds = new DataSet();

            // RCI 27-FEB-2012: Use Store Procedure to select pending handpays
            _sql_command = new SqlCommand("WSP_AccountPendingHandpays", _sql_conn);
            _sql_command.CommandType = CommandType.StoredProcedure;
            _sql_command.Parameters.Add("@AccountId", SqlDbType.BigInt).Value = m_card.AccountId;

            // RCI & AJQ 26-JUL-2011
            _da = new SqlDataAdapter(_sql_command);

            _da.Fill(_ds);
            dgv_pending_handpays.DataSource = _ds.Tables[0];

            _date_format = Format.DateTimeCustomFormatString(true);

            // Pending handpays datagrid
            dgv_pending_handpays.RowTemplate.DividerHeight = 0;

            // Columns
            //    - Terminal Id
            //    - DateTime
            //    - Terminal Name
            //    - Provider Name 
            //    - Game
            //    - Amount

            //    - Terminal Id
            dgv_pending_handpays.Columns[GRID_COLUMN_TERM_ID].Width = 0;
            dgv_pending_handpays.Columns[GRID_COLUMN_TERM_ID].Visible = false;

            //    - DateTime
            dgv_pending_handpays.Columns[GRID_COLUMN_DATE].HeaderCell.Value = Resource.String("STR_FRM_HANDPAYS_002");
            dgv_pending_handpays.Columns[GRID_COLUMN_DATE].Width = 200;
            dgv_pending_handpays.Columns[GRID_COLUMN_DATE].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgv_pending_handpays.Columns[GRID_COLUMN_DATE].DefaultCellStyle.Format = _date_format;

            //    - Provider Name 
            dgv_pending_handpays.Columns[GRID_COLUMN_TERM_PROVIDER].HeaderCell.Value = Resource.String("STR_FRM_HANDPAYS_004");
            dgv_pending_handpays.Columns[GRID_COLUMN_TERM_PROVIDER].Width = 200;
            dgv_pending_handpays.Columns[GRID_COLUMN_TERM_PROVIDER].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            //    - Terminal Name
            dgv_pending_handpays.Columns[GRID_COLUMN_TERM_NAME].HeaderCell.Value = Resource.String("STR_FRM_HANDPAYS_003");
            dgv_pending_handpays.Columns[GRID_COLUMN_TERM_NAME].Width = 200;
            dgv_pending_handpays.Columns[GRID_COLUMN_TERM_NAME].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            //    - Type 
            dgv_pending_handpays.Columns[GRID_COLUMN_TYPE].Width = 0;
            dgv_pending_handpays.Columns[GRID_COLUMN_TYPE].Visible = false;

            //    - Type Name
            dgv_pending_handpays.Columns[GRID_COLUMN_TYPE_NAME].HeaderCell.Value = Resource.String("STR_FRM_HANDPAYS_005");
            dgv_pending_handpays.Columns[GRID_COLUMN_TYPE_NAME].Width = 200;
            dgv_pending_handpays.Columns[GRID_COLUMN_TYPE_NAME].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            //    - Site Jackpot Name
            dgv_pending_handpays.Columns[GRID_COLUMN_SITE_JACKPOT_NAME].Width = 0;
            dgv_pending_handpays.Columns[GRID_COLUMN_SITE_JACKPOT_NAME].Visible = false;

            //    - Site Jackpot Awarded on Terminal Id
            dgv_pending_handpays.Columns[GRID_COLUMN_SITE_JACKPOT_AWARDED_ON_TERMINAL_ID].Width = 0;
            dgv_pending_handpays.Columns[GRID_COLUMN_SITE_JACKPOT_AWARDED_ON_TERMINAL_ID].Visible = false;

            //    - Amount
            dgv_pending_handpays.Columns[GRID_COLUMN_AMOUNT].HeaderCell.Value = Resource.String("STR_FRM_HANDPAYS_006");
            dgv_pending_handpays.Columns[GRID_COLUMN_AMOUNT].Width = 138;
            dgv_pending_handpays.Columns[GRID_COLUMN_AMOUNT].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgv_pending_handpays.Columns[GRID_COLUMN_AMOUNT].DefaultCellStyle.Format = "c";

            //    - Amt0
            // RAB 22-MAR-2016: Bug 9140
            dgv_pending_handpays.Columns[GRID_COLUMN_AMT0].Width = 0;
            dgv_pending_handpays.Columns[GRID_COLUMN_AMT0].Visible = false;

            //    - Level jackpot 
            dgv_pending_handpays.Columns[GRID_COLUMN_LEVEL_JACKPOT].Width = 0;
            dgv_pending_handpays.Columns[GRID_COLUMN_LEVEL_JACKPOT].Visible = false;

            for (_idx_row = 0; _idx_row < dgv_pending_handpays.Rows.Count; _idx_row++)
            {
                _handpay_type = (HANDPAY_TYPE)dgv_pending_handpays.Rows[_idx_row].Cells[GRID_COLUMN_TYPE].Value;
                dgv_pending_handpays.Rows[_idx_row].Cells[GRID_COLUMN_TYPE_NAME].Value = Handpays.HandpayTypeString(_handpay_type, (Int32)dgv_pending_handpays.Rows[_idx_row].Cells[GRID_COLUMN_LEVEL_JACKPOT].Value);
            }

            // Query to obtain last paid handpays
            _ds = new DataSet();

            _sql_query = " SELECT TOP (1) HP_TERMINAL_ID"
                                     + ", HP_DATETIME"
                                     + ", HP_TE_PROVIDER_ID"
                                     + ", HP_TE_NAME"
                                     + ", HP_TYPE"
                                     + ", ' ' "                                      // Type Name
                                     + ", HP_SITE_JACKPOT_NAME "                     // Site Jackpot Name
                                     + ", HP_AMOUNT"
                                     + ", AM_MOVEMENT_ID"
                                     + ", HP_PLAY_SESSION_ID"
                                    + ", ISNULL(HP_LEVEL,0) AS HP_LEVEL"
                      + " FROM ( SELECT TOP (1) HP_TERMINAL_ID"
                                           + ", HP_DATETIME"
                                           + ", HP_TE_PROVIDER_ID"
                                           + ", HP_TE_NAME"
                                           + ", HP_TYPE"
                                           + ", ISNULL (HP_SITE_JACKPOT_NAME, ' ') AS HP_SITE_JACKPOT_NAME "
                                           + ", HP_AMOUNT"
                                           + ", AM_MOVEMENT_ID"
                                           + ", AM_TYPE"
                                           + ", HP_PLAY_SESSION_ID"
                                          + ", ISNULL(HP_LEVEL,0) AS HP_LEVEL"
                                     + " FROM ACCOUNT_MOVEMENTS LEFT OUTER JOIN HANDPAYS ON AM_MOVEMENT_ID = HP_MOVEMENT_ID"
                                    + " WHERE AM_ACCOUNT_ID = @AccountId"
                                      + " AND AM_TYPE IN (@Handpay, @HandpayCancellation, @ManualHandpay, @ManualHandpayCancellation)"
                                      + " AND DATEADD(MINUTE, @CancelTimePeriod, AM_DATETIME) >= GETDATE()"
                                      + " AND HP_TYPE NOT IN(@ManualProgressiveHandPay, @SiteJackPot) "
                                 + " ORDER BY AM_DATETIME DESC ) A"
                                   + " WHERE AM_TYPE IN (@Handpay, @ManualHandpay)";

            _sql_command = new SqlCommand(_sql_query, _sql_conn);
            _sql_command.Parameters.Add("@AccountId", SqlDbType.BigInt).Value = m_card.AccountId;
            _sql_command.Parameters.Add("@Handpay", SqlDbType.Int).Value = MovementType.Handpay;
            _sql_command.Parameters.Add("@HandpayCancellation", SqlDbType.Int).Value = MovementType.HandpayCancellation;
            _sql_command.Parameters.Add("@ManualHandpay", SqlDbType.Int).Value = MovementType.ManualHandpay;
            _sql_command.Parameters.Add("@ManualHandpayCancellation", SqlDbType.Int).Value = MovementType.ManualHandpayCancellation;
            _sql_command.Parameters.Add("@CancelTimePeriod", SqlDbType.Int).Value = _handpay_cancel_time_period;
            _sql_command.Parameters.Add("@ManualProgressiveHandPay", SqlDbType.Int).Value = HANDPAY_TYPE.SPECIAL_PROGRESSIVE;
            _sql_command.Parameters.Add("@SiteJackPot", SqlDbType.Int).Value = HANDPAY_TYPE.SITE_JACKPOT;

            // RCI & AJQ 26-JUL-2011
            _da = new SqlDataAdapter(WGDB.ParametersToVariables(_sql_command));

            _da.Fill(_ds);
            dgv_last_handpay.DataSource = _ds.Tables[0];

            // Last handpay datagrid
            dgv_last_handpay.RowTemplate.DividerHeight = 0;

            // Columns
            //    - Terminal Id
            //    - DateTime
            //    - Terminal Name
            //    - Provider Name 
            //    - Game
            //    - Amount

            //    - Terminal Id
            dgv_last_handpay.Columns[GRID_COLUMN_TERM_ID].Width = 0;
            dgv_last_handpay.Columns[GRID_COLUMN_TERM_ID].Visible = false;

            //    - DateTime
            dgv_last_handpay.Columns[GRID_COLUMN_DATE].HeaderCell.Value = Resource.String("STR_FRM_HANDPAYS_002");
            dgv_last_handpay.Columns[GRID_COLUMN_DATE].Width = 200;
            dgv_last_handpay.Columns[GRID_COLUMN_DATE].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgv_last_handpay.Columns[GRID_COLUMN_DATE].DefaultCellStyle.Format = _date_format;

            //    - Provider Name 
            dgv_last_handpay.Columns[GRID_COLUMN_TERM_PROVIDER].HeaderCell.Value = Resource.String("STR_FRM_HANDPAYS_004");
            dgv_last_handpay.Columns[GRID_COLUMN_TERM_PROVIDER].Width = 200;
            dgv_last_handpay.Columns[GRID_COLUMN_TERM_PROVIDER].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            //    - Terminal Name
            dgv_last_handpay.Columns[GRID_COLUMN_TERM_NAME].HeaderCell.Value = Resource.String("STR_FRM_HANDPAYS_003");
            dgv_last_handpay.Columns[GRID_COLUMN_TERM_NAME].Width = 200;
            dgv_last_handpay.Columns[GRID_COLUMN_TERM_NAME].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            //    - Type 
            dgv_last_handpay.Columns[GRID_COLUMN_TYPE].Width = 0;
            dgv_last_handpay.Columns[GRID_COLUMN_TYPE].Visible = false;

            //    - Type Name
            dgv_last_handpay.Columns[GRID_COLUMN_TYPE_NAME].HeaderCell.Value = Resource.String("STR_FRM_HANDPAYS_005");
            dgv_last_handpay.Columns[GRID_COLUMN_TYPE_NAME].Width = 200;
            dgv_last_handpay.Columns[GRID_COLUMN_TYPE_NAME].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            //    - Site Jackpot Name
            dgv_last_handpay.Columns[GRID_COLUMN_SITE_JACKPOT_NAME].Width = 0;
            dgv_last_handpay.Columns[GRID_COLUMN_SITE_JACKPOT_NAME].Visible = false;

            //    - Amount
            dgv_last_handpay.Columns[GRID_COLUMN_AMOUNT].HeaderCell.Value = Resource.String("STR_FRM_HANDPAYS_006");
            dgv_last_handpay.Columns[GRID_COLUMN_AMOUNT].Width = 138;
            dgv_last_handpay.Columns[GRID_COLUMN_AMOUNT].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgv_last_handpay.Columns[GRID_COLUMN_AMOUNT].DefaultCellStyle.Format = "c";

            //    - Movement Id
            dgv_last_handpay.Columns[GRID_COLUMN_MOV_ID].Width = 0;
            dgv_last_handpay.Columns[GRID_COLUMN_MOV_ID].Visible = false;

            //    - Play Session Id
            dgv_last_handpay.Columns[GRID_COLUMN_PLAY_SESSION_ID].Width = 0;
            dgv_last_handpay.Columns[GRID_COLUMN_PLAY_SESSION_ID].Visible = false;

            //    - Level jackpot 
            dgv_last_handpay.Columns[GRID_COLUMN_LEVEL_JACKPOT].Width = 0;
            dgv_last_handpay.Columns[GRID_COLUMN_LEVEL_JACKPOT].Visible = false;

            for (_idx_row = 0; _idx_row < dgv_last_handpay.Rows.Count; _idx_row++)
            {
                _handpay_type = (HANDPAY_TYPE)dgv_last_handpay.Rows[_idx_row].Cells[GRID_COLUMN_TYPE].Value;
                dgv_last_handpay.Rows[_idx_row].Cells[GRID_COLUMN_TYPE_NAME].Value = Handpays.HandpayTypeString(_handpay_type, (Int32)dgv_last_handpay.Rows[_idx_row].Cells[GRID_COLUMN_LEVEL_JACKPOT].Value);
            }

            btn_pay.Enabled = (dgv_pending_handpays.RowCount > 0);
            btn_cancel_handpay.Enabled = (dgv_last_handpay.RowCount > 0);

            form_yes_no.Show();
            this.ShowDialog(form_yes_no);
            form_yes_no.Hide();

            return;
        }

        catch (Exception _ex)
        {
            Log.Exception(_ex);

            return;
        }

        finally
        {
            // Close connection
            if (_da != null)
            {
                _da.Dispose();
                _da = null;
            }

            if (_ds != null)
            {
                _ds.Dispose();
                _ds = null;
            }

            if (_sql_conn != null)
            {
                _sql_conn.Close();
                _sql_conn = null;
            }
        }
    } // Show


    //------------------------------------------------------------------------------
    // PURPOSE : Init and Show the form.
    //
    //  PARAMS :
    //      - INPUT : ParentForm
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //

    public void Show(Form ParentForm)
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_handpays", Log.Type.Message);

      /*String _sql_query;
      SqlConnection _sql_conn;
      SqlCommand _sql_command;
      DataSet _ds;
      SqlDataAdapter _da;
      String _date_format;
      int _handpay_time_period = 0;
      int _handpay_cancel_time_period = 0;
      Int32 _idx_row;
      HANDPAY_TYPE _handpay_type;

      _sql_conn = null;
      _ds = null;
      _da = null;*/

      /*try
      {
        // Get the Handpay settings from the General Parameters table
      /*  Handpays.GetParameters(out _handpay_time_period, out _handpay_cancel_time_period);

        // Get Sql Connection 
        _sql_conn = WGDB.Connection();
        _ds = new DataSet();

        // RCI 27-FEB-2012: Use Store Procedure to select pending handpays
        _sql_command = new SqlCommand("WSP_AccountPendingHandpays", _sql_conn);
        _sql_command.CommandType = CommandType.StoredProcedure;
        _sql_command.Parameters.Add("@AccountId", SqlDbType.BigInt).Value = m_card.AccountId;

        // RCI & AJQ 26-JUL-2011
        _da = new SqlDataAdapter(_sql_command);

        _da.Fill(_ds);
        dgv_pending_handpays.DataSource = _ds.Tables[0];


        _date_format = Format.DateTimeCustomFormatString(true);

        // Pending handpays datagrid
        dgv_pending_handpays.RowTemplate.DividerHeight = 0;

        // Columns
        //    - Terminal Id
        //    - DateTime
        //    - Terminal Name
        //    - Provider Name 
        //    - Game
        //    - Amount

        //    - Terminal Id
        dgv_pending_handpays.Columns[GRID_COLUMN_TERM_ID].Width = 0;
        dgv_pending_handpays.Columns[GRID_COLUMN_TERM_ID].Visible = false;

        //    - DateTime
        dgv_pending_handpays.Columns[GRID_COLUMN_DATE].HeaderCell.Value = Resource.String("STR_FRM_HANDPAYS_002");
        dgv_pending_handpays.Columns[GRID_COLUMN_DATE].Width = 200;
        dgv_pending_handpays.Columns[GRID_COLUMN_DATE].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
        dgv_pending_handpays.Columns[GRID_COLUMN_DATE].DefaultCellStyle.Format = _date_format;

        //    - Provider Name 
        dgv_pending_handpays.Columns[GRID_COLUMN_TERM_PROVIDER].HeaderCell.Value = Resource.String("STR_FRM_HANDPAYS_004");
        dgv_pending_handpays.Columns[GRID_COLUMN_TERM_PROVIDER].Width = 200;
        dgv_pending_handpays.Columns[GRID_COLUMN_TERM_PROVIDER].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

        //    - Terminal Name
        dgv_pending_handpays.Columns[GRID_COLUMN_TERM_NAME].HeaderCell.Value = Resource.String("STR_FRM_HANDPAYS_003");
        dgv_pending_handpays.Columns[GRID_COLUMN_TERM_NAME].Width = 200;
        dgv_pending_handpays.Columns[GRID_COLUMN_TERM_NAME].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

        //    - Type 
        dgv_pending_handpays.Columns[GRID_COLUMN_TYPE].Width = 0;
        dgv_pending_handpays.Columns[GRID_COLUMN_TYPE].Visible = false;

        //    - Type Name
        dgv_pending_handpays.Columns[GRID_COLUMN_TYPE_NAME].HeaderCell.Value = Resource.String("STR_FRM_HANDPAYS_005");
        dgv_pending_handpays.Columns[GRID_COLUMN_TYPE_NAME].Width = 200;
        dgv_pending_handpays.Columns[GRID_COLUMN_TYPE_NAME].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

        //    - Site Jackpot Name
        dgv_pending_handpays.Columns[GRID_COLUMN_SITE_JACKPOT_NAME].Width = 0;
        dgv_pending_handpays.Columns[GRID_COLUMN_SITE_JACKPOT_NAME].Visible = false;

        //    - Site Jackpot Awarded on Terminal Id
        dgv_pending_handpays.Columns[GRID_COLUMN_SITE_JACKPOT_AWARDED_ON_TERMINAL_ID].Width = 0;
        dgv_pending_handpays.Columns[GRID_COLUMN_SITE_JACKPOT_AWARDED_ON_TERMINAL_ID].Visible = false;

        //    - Amount
        dgv_pending_handpays.Columns[GRID_COLUMN_AMOUNT].HeaderCell.Value = Resource.String("STR_FRM_HANDPAYS_006");
        dgv_pending_handpays.Columns[GRID_COLUMN_AMOUNT].Width = 138;
        dgv_pending_handpays.Columns[GRID_COLUMN_AMOUNT].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        dgv_pending_handpays.Columns[GRID_COLUMN_AMOUNT].DefaultCellStyle.Format = "c";

        //    - Amt0
        // RAB 22-MAR-2016: Bug 9140
        dgv_pending_handpays.Columns[GRID_COLUMN_AMT0].Width = 0;
        dgv_pending_handpays.Columns[GRID_COLUMN_AMT0].Visible = false;

        //    - Level jackpot 
        dgv_pending_handpays.Columns[GRID_COLUMN_LEVEL_JACKPOT].Width = 0;
        dgv_pending_handpays.Columns[GRID_COLUMN_LEVEL_JACKPOT].Visible = false;

        for (_idx_row = 0; _idx_row < dgv_pending_handpays.Rows.Count; _idx_row++)
        {
          _handpay_type = (HANDPAY_TYPE)dgv_pending_handpays.Rows[_idx_row].Cells[GRID_COLUMN_TYPE].Value;
          dgv_pending_handpays.Rows[_idx_row].Cells[GRID_COLUMN_TYPE_NAME].Value = Handpays.HandpayTypeString(_handpay_type, (Int32)dgv_pending_handpays.Rows[_idx_row].Cells[GRID_COLUMN_LEVEL_JACKPOT].Value);
          }

        // Query to obtain last paid handpays
        _ds = new DataSet();

        _sql_query = " SELECT TOP (1) HP_TERMINAL_ID"
                                 + ", HP_DATETIME"
                                 + ", HP_TE_PROVIDER_ID"
                                 + ", HP_TE_NAME"
                                 + ", HP_TYPE"
                                 + ", ' ' "                                      // Type Name
                                 + ", HP_SITE_JACKPOT_NAME "                     // Site Jackpot Name
                                 + ", HP_AMOUNT"
                                 + ", AM_MOVEMENT_ID"
                                 + ", HP_PLAY_SESSION_ID"
                                + ", ISNULL(HP_LEVEL,0) AS HP_LEVEL"
                  + " FROM ( SELECT TOP (1) HP_TERMINAL_ID"
                                       + ", HP_DATETIME"
                                       + ", HP_TE_PROVIDER_ID"
                                       + ", HP_TE_NAME"
                                       + ", HP_TYPE"
                                       + ", ISNULL (HP_SITE_JACKPOT_NAME, ' ') AS HP_SITE_JACKPOT_NAME "
                                       + ", HP_AMOUNT"
                                       + ", AM_MOVEMENT_ID"
                                       + ", AM_TYPE"
                                       + ", HP_PLAY_SESSION_ID"
                                      + ", ISNULL(HP_LEVEL,0) AS HP_LEVEL"
                                 + " FROM ACCOUNT_MOVEMENTS LEFT OUTER JOIN HANDPAYS ON AM_MOVEMENT_ID = HP_MOVEMENT_ID"
                                + " WHERE AM_ACCOUNT_ID = @AccountId"
                                  + " AND AM_TYPE IN (@Handpay, @HandpayCancellation, @ManualHandpay, @ManualHandpayCancellation)"
                                  + " AND DATEADD(MINUTE, @CancelTimePeriod, AM_DATETIME) >= GETDATE()"
                                  + " AND HP_TYPE NOT IN(@ManualProgressiveHandPay, @SiteJackPot) "
                             + " ORDER BY AM_DATETIME DESC ) A"
                               + " WHERE AM_TYPE IN (@Handpay, @ManualHandpay)";

        _sql_command = new SqlCommand(_sql_query, _sql_conn);
        _sql_command.Parameters.Add("@AccountId", SqlDbType.BigInt).Value = m_card.AccountId;
        _sql_command.Parameters.Add("@Handpay", SqlDbType.Int).Value = MovementType.Handpay;
        _sql_command.Parameters.Add("@HandpayCancellation", SqlDbType.Int).Value = MovementType.HandpayCancellation;
        _sql_command.Parameters.Add("@ManualHandpay", SqlDbType.Int).Value = MovementType.ManualHandpay;
        _sql_command.Parameters.Add("@ManualHandpayCancellation", SqlDbType.Int).Value = MovementType.ManualHandpayCancellation;
        _sql_command.Parameters.Add("@CancelTimePeriod", SqlDbType.Int).Value = _handpay_cancel_time_period;
        _sql_command.Parameters.Add("@ManualProgressiveHandPay", SqlDbType.Int).Value = HANDPAY_TYPE.SPECIAL_PROGRESSIVE;
        _sql_command.Parameters.Add("@SiteJackPot", SqlDbType.Int).Value = HANDPAY_TYPE.SITE_JACKPOT;

        // RCI & AJQ 26-JUL-2011
        _da = new SqlDataAdapter(WGDB.ParametersToVariables(_sql_command));

        _da.Fill(_ds);
        dgv_last_handpay.DataSource = _ds.Tables[0];

        // Last handpay datagrid
        dgv_last_handpay.RowTemplate.DividerHeight = 0;

        // Columns
        //    - Terminal Id
        //    - DateTime
        //    - Terminal Name
        //    - Provider Name 
        //    - Game
        //    - Amount

        //    - Terminal Id
        dgv_last_handpay.Columns[GRID_COLUMN_TERM_ID].Width = 0;
        dgv_last_handpay.Columns[GRID_COLUMN_TERM_ID].Visible = false;

        //    - DateTime
        dgv_last_handpay.Columns[GRID_COLUMN_DATE].HeaderCell.Value = Resource.String("STR_FRM_HANDPAYS_002");
        dgv_last_handpay.Columns[GRID_COLUMN_DATE].Width = 200;
        dgv_last_handpay.Columns[GRID_COLUMN_DATE].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
        dgv_last_handpay.Columns[GRID_COLUMN_DATE].DefaultCellStyle.Format = _date_format;

        //    - Provider Name 
        dgv_last_handpay.Columns[GRID_COLUMN_TERM_PROVIDER].HeaderCell.Value = Resource.String("STR_FRM_HANDPAYS_004");
        dgv_last_handpay.Columns[GRID_COLUMN_TERM_PROVIDER].Width = 200;
        dgv_last_handpay.Columns[GRID_COLUMN_TERM_PROVIDER].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

        //    - Terminal Name
        dgv_last_handpay.Columns[GRID_COLUMN_TERM_NAME].HeaderCell.Value = Resource.String("STR_FRM_HANDPAYS_003");
        dgv_last_handpay.Columns[GRID_COLUMN_TERM_NAME].Width = 200;
        dgv_last_handpay.Columns[GRID_COLUMN_TERM_NAME].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

        //    - Type 
        dgv_last_handpay.Columns[GRID_COLUMN_TYPE].Width = 0;
        dgv_last_handpay.Columns[GRID_COLUMN_TYPE].Visible = false;

        //    - Type Name
        dgv_last_handpay.Columns[GRID_COLUMN_TYPE_NAME].HeaderCell.Value = Resource.String("STR_FRM_HANDPAYS_005");
        dgv_last_handpay.Columns[GRID_COLUMN_TYPE_NAME].Width = 200;
        dgv_last_handpay.Columns[GRID_COLUMN_TYPE_NAME].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

        //    - Site Jackpot Name
        dgv_last_handpay.Columns[GRID_COLUMN_SITE_JACKPOT_NAME].Width = 0;
        dgv_last_handpay.Columns[GRID_COLUMN_SITE_JACKPOT_NAME].Visible = false;

        //    - Amount
        dgv_last_handpay.Columns[GRID_COLUMN_AMOUNT].HeaderCell.Value = Resource.String("STR_FRM_HANDPAYS_006");
        dgv_last_handpay.Columns[GRID_COLUMN_AMOUNT].Width = 138;
        dgv_last_handpay.Columns[GRID_COLUMN_AMOUNT].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        dgv_last_handpay.Columns[GRID_COLUMN_AMOUNT].DefaultCellStyle.Format = "c";

        //    - Movement Id
        dgv_last_handpay.Columns[GRID_COLUMN_MOV_ID].Width = 0;
        dgv_last_handpay.Columns[GRID_COLUMN_MOV_ID].Visible = false;

        //    - Play Session Id
        dgv_last_handpay.Columns[GRID_COLUMN_PLAY_SESSION_ID].Width = 0;
        dgv_last_handpay.Columns[GRID_COLUMN_PLAY_SESSION_ID].Visible = false;

        //    - Level jackpot 
        dgv_last_handpay.Columns[GRID_COLUMN_LEVEL_JACKPOT].Width = 0;
        dgv_last_handpay.Columns[GRID_COLUMN_LEVEL_JACKPOT].Visible = false;

        for (_idx_row = 0; _idx_row < dgv_last_handpay.Rows.Count; _idx_row++)
        {
          _handpay_type = (HANDPAY_TYPE)dgv_last_handpay.Rows[_idx_row].Cells[GRID_COLUMN_TYPE].Value;
          dgv_last_handpay.Rows[_idx_row].Cells[GRID_COLUMN_TYPE_NAME].Value = Handpays.HandpayTypeString(_handpay_type, (Int32)dgv_last_handpay.Rows[_idx_row].Cells[GRID_COLUMN_LEVEL_JACKPOT].Value);
        }

        btn_pay.Enabled = (dgv_pending_handpays.RowCount > 0);
        btn_cancel_handpay.Enabled = (dgv_last_handpay.RowCount > 0);

        form_yes_no.Show();
        this.ShowDialog(form_yes_no);
        form_yes_no.Hide();
          */
          //
        this.refreshHandPays();

   /*     return;
      }

      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return;
      }

      finally
      {
        // Close connection
        if (_da != null)
        {
          _da.Dispose();
          _da = null;
        }

        if (_ds != null)
        {
          _ds.Dispose();
          _ds = null;
        }

        if (_sql_conn != null)
        {
          _sql_conn.Close();
          _sql_conn = null;
        }
      }*/
    } // Show

    //------------------------------------------------------------------------------
    // PURPOSE : Dispose yes_no form.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    new public void Dispose()
    {
      form_yes_no.Dispose();
      form_yes_no = null;

      base.Dispose();
    }

    #endregion

    #region DataGridEvents

    //------------------------------------------------------------------------------
    // PURPOSE : Paint a datagrid row before actually filling it.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    private void dgv_handpays_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
    {
    } // dgv_handpays_RowPrePaint

    #endregion DataGridEvents

    #region Buttons

    //------------------------------------------------------------------------------
    // PURPOSE : Handle the click on the Select button.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    private void btn_select_Click(object sender, EventArgs e)
    {
      DataGridViewRow _view_row;
      DataRow _row;
      HandPay _handpay;
      String _error_str;
      Boolean _print_played_amount;
      Currency _played_amount;
      String _denomination_str;
      String _denomination_value;
      Int64 _handpay_id;
      Int32 _level;
      Int64 _progressive_id;

      btn_pay.Enabled = false;
      switch (this.dgv_pending_handpays.SelectedRows.Count)
      {
        case 1:
          break;

        case 0:
        default:
          frm_message.Show(Resource.String("STR_FRM_HANDPAYS_009"),
                           Resource.String("STR_APP_GEN_MSG_ERROR"),
                           MessageBoxButtons.OK,
                           MessageBoxIcon.Error,
                           this.ParentForm);
          this.Focus();

          btn_pay.Enabled = true;
          return;
      }

      // Check if current user is an authorized user
      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.CardHandPay,
                                               ProfilePermissions.TypeOperation.RequestPasswd,
                                               this,
                                               out _error_str))
      {
        btn_pay.Enabled = true;
        return;
      }

      _view_row = dgv_pending_handpays.SelectedRows[0];
      _row = ((DataRowView)_view_row.DataBoundItem).Row;

      _handpay = new HandPay(m_card);
      _handpay.TerminalId = (Int32)_row[GRID_COLUMN_TERM_ID];
      _handpay.TerminalName = (String)_row[GRID_COLUMN_TERM_NAME];
      _handpay.TerminalProvider = (String)_row[GRID_COLUMN_TERM_PROVIDER];
      _handpay.Amount = (decimal)_row[GRID_COLUMN_AMOUNT];
      _handpay.Type = (HANDPAY_TYPE)_row[GRID_COLUMN_TYPE];
      _handpay.DateTime = (DateTime)_row[GRID_COLUMN_DATE];

      // Only usefull for Site Jackpot Name, but it can be assigned in all cases.
      _handpay.SiteJackpotName = (String)_row[GRID_COLUMN_SITE_JACKPOT_NAME];
      _handpay.SiteJackpotAwardedOnTerminalId = (Int32)_row[GRID_COLUMN_SITE_JACKPOT_AWARDED_ON_TERMINAL_ID];
      // RAB 21-MAR-2016: Bug 9140
      _handpay.Amt0 = (Decimal)_row[GRID_COLUMN_AMT0];

      // RCI 22-NOV-2011: Indicates if have to print in the Voucher the PlayedAmount for the play_session 
      //                  that produces the Handpay.
      _print_played_amount = GeneralParam.GetBoolean("Cashier.Voucher", "Handpay.PrintPlayedAmount");

      if (_print_played_amount)
      {
        if (!Handpays.GetHandpaySessionPlayedAmount(m_card.AccountId
                                          , _handpay.Type == HANDPAY_TYPE.SITE_JACKPOT ? _handpay.SiteJackpotAwardedOnTerminalId : _handpay.TerminalId
                                          , _handpay.DateTime
                                          , out _played_amount
                                          , out _denomination_str
                                          , out _denomination_value))
        {
          _played_amount = -1;
        }

        _handpay.SessionPlayedAmount = _played_amount;
        _handpay.DenominationStr = _denomination_str;
        _handpay.DenominationValue = _denomination_value;
      }
      else
      {
        _handpay.SessionPlayedAmount = -1;
      }

      if (!Handpays.GetHandpayProgressiveInfo(_handpay.TerminalId
                                        , _handpay.DateTime
                                        , _handpay.Amount
                                        , out _level
                                        , out _progressive_id
                                        , out _handpay_id))
      {
        _level = 0;
        _progressive_id = 0;
        _handpay_id = 0;
      }

      _handpay.Level = _level;
      _handpay.ProgressiveId = _progressive_id;
      _handpay.HandpayId = _handpay_id;

      // Check whether the account is available or not
      if (!_handpay.CheckPayment())
      {
        _handpay.HandpayMsg(Handpays.HANDPAY_MSG.REGISTER_NOT_ALLOWED, this.ParentForm, false);

        btn_pay.Enabled = true;
        return;
      }

      if (!_handpay.HandpayMsg(Handpays.HANDPAY_MSG.REGISTER_CONF, this.ParentForm, false))
      {
        btn_pay.Enabled = true;
        return;
      }

      // Process the selected handpay
      if (!_handpay.Register((DateTime)_row[GRID_COLUMN_DATE]))
      {
        _handpay.HandpayMsg(Handpays.HANDPAY_MSG.REGISTER_ERROR, this.ParentForm, false);

        this.Focus();

        btn_pay.Enabled = true;
        return;
      }

      btn_pay.Enabled = true;
      Misc.WriteLog("[FORM CLOSE] frm_handpays (select)", Log.Type.Message);
      // Handpay was registered => Close the Handpays screen and return to the account screen
      this.Close();

    } // btn_select_Click

    //------------------------------------------------------------------------------
    // PURPOSE : Handle the click on the Manual button.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    private void btn_manual_Click(object sender, EventArgs e)
    {
      HandPay _handpay;
      _handpay = new HandPay(m_card);

      try
      {
        // DLM 12-JUN-2018 using (frm_handpay_action _frm = new frm_handpay_action(frm_handpay_action.ENTRY_TYPE.MANUAL, _handpay.HandpayId, m_card))
        using (frm_handpay_action _frm = new frm_handpay_action(frm_handpay_action.ENTRY_TYPE.MANUAL, _handpay.HandpayId, m_card, this))
        {
          _frm.Show(this);
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      return;
    } // btn_manual_Click

    //------------------------------------------------------------------------------
    // PURPOSE : Handle the click on the Exit button.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    private void btn_exit_Click(object sender, EventArgs e)
    {
      Misc.WriteLog("[FORM CLOSE] frm_handpays (exit)", Log.Type.Message);
      this.Close();
    } // btn_exit_Click

    //------------------------------------------------------------------------------
    // PURPOSE : Handle the clicks on the Cancel Last Handpay button.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    private void btn_cancel_handpay_Click(object sender, EventArgs e)
    {
      String _sql_query;
      SqlConnection _sql_conn;
      SqlCommand _sql_command;
      DataSet _ds;
      SqlDataAdapter _da;

      HandPay _handpay;
      HandPay _selected_handpay;
      String _error_str;

      int _handpay_time_period = 0;
      int _handpay_cancel_time_period = 0;

      Int64 _handpay_id;
      Int32 _level;
      Int64 _progressive_id;

      DataRow _row;
      DataGridViewRow _view_row;
      ProfilePermissions.CashierFormFuncionality _required_permission;

      switch (this.dgv_last_handpay.RowCount)
      {
        case 1:
          break;

        case 0:
        default:
          _selected_handpay = new HandPay(m_card);
          _selected_handpay.HandpayMsg(Handpays.HANDPAY_MSG.CANCEL_NOT_FOUND, this.ParentForm, false);

          return;
      }

      _view_row = dgv_last_handpay.Rows[0];
      _row = ((DataRowView)_view_row.DataBoundItem).Row;

      _selected_handpay = new HandPay(m_card);
      _selected_handpay.TerminalId = (Int32)_row[GRID_COLUMN_TERM_ID];
      _selected_handpay.TerminalName = (String)_row[GRID_COLUMN_TERM_NAME];
      _selected_handpay.TerminalProvider = (String)_row[GRID_COLUMN_TERM_PROVIDER];
      _selected_handpay.Amount = (decimal)_row[GRID_COLUMN_AMOUNT];
      _selected_handpay.Type = (HANDPAY_TYPE)_row[GRID_COLUMN_TYPE];
      _selected_handpay.DateTime = (DateTime)_row[GRID_COLUMN_DATE];
      _selected_handpay.MovementId = (Int64)_row[GRID_COLUMN_MOV_ID];
      _selected_handpay.PlaySessionId = (Int64)_row[GRID_COLUMN_PLAY_SESSION_ID];

      // Only usefull for Site Jackpot Name, but it can be assigned in all cases.
      _selected_handpay.SiteJackpotName = (String)_row[GRID_COLUMN_SITE_JACKPOT_NAME];

      // Check if current user is an authorized user
      _required_permission = ProfilePermissions.CashierFormFuncionality.CardHandPayCancel;
      switch (_selected_handpay.Type)
      {
        case HANDPAY_TYPE.CANCELLED_CREDITS:
        case HANDPAY_TYPE.JACKPOT:
        case HANDPAY_TYPE.SITE_JACKPOT:
        case HANDPAY_TYPE.ORPHAN_CREDITS:
          _required_permission = ProfilePermissions.CashierFormFuncionality.CardHandPayCancel;
          break;

        case HANDPAY_TYPE.MANUAL:
          _required_permission = ProfilePermissions.CashierFormFuncionality.ManualHandPayCancel;
          break;
      }

      if (!ProfilePermissions.CheckPermissions(_required_permission,
                                               ProfilePermissions.TypeOperation.RequestPasswd,
                                               this,
                                               out _error_str))
      {
        return;
      }

      _sql_conn = null;
      _ds = null;
      _da = null;

      try
      {
        // Get the Handpay settings from the General Parameters table
        Handpays.GetParameters(out _handpay_time_period, out _handpay_cancel_time_period);

        // Get Sql Connection 
        _sql_conn = WGDB.Connection();
        _ds = new DataSet();

        _sql_query = "SELECT TOP (1) HP_TERMINAL_ID"
                                + ", HP_DATETIME"
                                + ", HP_TE_PROVIDER_ID"
                                + ", HP_TE_NAME"
                                + ", HP_TYPE"
                                + ", ' '"
                                + ", HP_SITE_JACKPOT_NAME"         // Site Jackpot Name
                                + ", HP_AMOUNT"
                                + ", AM_MOVEMENT_ID"
                                + ", HP_PLAY_SESSION_ID"
                                + ", HP_AMT0"
                 + " FROM ( SELECT TOP (1) HP_TERMINAL_ID"
                                      + ", HP_DATETIME"
                                      + ", HP_TE_PROVIDER_ID"
                                      + ", HP_TE_NAME"
                                      + ", HP_TYPE"
                                      + ", ISNULL (HP_SITE_JACKPOT_NAME, ' ') AS HP_SITE_JACKPOT_NAME "
                                      + ", HP_AMOUNT"
                                      + ", AM_MOVEMENT_ID"
                                      + ", AM_TYPE"
                                      + ", HP_PLAY_SESSION_ID"
                                      + ", ISNULL (HP_AMT0, HP_AMOUNT) AS HP_AMT0"
                                + " FROM ACCOUNT_MOVEMENTS LEFT OUTER JOIN HANDPAYS ON AM_MOVEMENT_ID = HP_MOVEMENT_ID"
                               + " WHERE AM_ACCOUNT_ID = @AccountId"
                                 + " AND AM_TYPE IN (@Handpay, @HandpayCancellation, @ManualHandpay, @ManualHandpayCancellation)"
                                 + " AND DATEADD(MINUTE, @CancelTimePeriod, AM_DATETIME) >= GETDATE()"
                                 + " AND HP_TYPE <> @ManualProgressiveHandPay "
                            + " ORDER BY AM_DATETIME DESC ) A"
                              + " WHERE AM_TYPE IN (@Handpay, @ManualHandpay)";

        _sql_command = new SqlCommand(_sql_query, _sql_conn);

        _sql_command.Parameters.Add("@AccountId", SqlDbType.BigInt).Value = m_card.AccountId;
        _sql_command.Parameters.Add("@Handpay", SqlDbType.Int).Value = MovementType.Handpay;
        _sql_command.Parameters.Add("@HandpayCancellation", SqlDbType.Int).Value = MovementType.HandpayCancellation;
        _sql_command.Parameters.Add("@ManualHandpay", SqlDbType.Int).Value = MovementType.ManualHandpay;
        _sql_command.Parameters.Add("@ManualHandpayCancellation", SqlDbType.Int).Value = MovementType.ManualHandpayCancellation;
        _sql_command.Parameters.Add("@CancelTimePeriod", SqlDbType.Int).Value = _handpay_cancel_time_period;
        _sql_command.Parameters.Add("@ManualProgressiveHandPay", SqlDbType.Int).Value = HANDPAY_TYPE.SPECIAL_PROGRESSIVE;

        _da = new SqlDataAdapter(_sql_command);
        _da.Fill(_ds);

        if (_ds.Tables[0].Rows.Count != 1)
        {
          _handpay = new HandPay(m_card);
          _handpay.HandpayMsg(Handpays.HANDPAY_MSG.CANCEL_NOT_FOUND, this.ParentForm, false);

          return;
        }

        // Get the selected handpay
        _row = _ds.Tables[0].Rows[0];

        //// Check returned record is properly typed:
        ////  - If retrieved record is a handpay cancellation then no further cancellations are allowed
        //if ((MovementType) _row["AM_TYPE"] != MovementType.Handpay)
        //{
        //  _handpay = new HandPay(m_card);
        //  _handpay.HandpayMsg(HandPay.HANDPAY_MSG.CANCEL_NOT_FOUND, this.ParentForm);

        //  return;
        //}

        _handpay = new HandPay(m_card);
        _handpay.TerminalId = (Int32)_row["HP_TERMINAL_ID"];
        _handpay.TerminalName = (String)_row["HP_TE_NAME"];
        _handpay.TerminalProvider = (String)_row["HP_TE_PROVIDER_ID"];
        _handpay.Amount = (decimal)_row["HP_AMOUNT"];
        _handpay.DateTime = (DateTime)_row["HP_DATETIME"];
        _handpay.MovementId = (Int64)_row["AM_MOVEMENT_ID"];
        _handpay.Type = (HANDPAY_TYPE)_row["HP_TYPE"];
        _handpay.PlaySessionId = (Int64)_row["HP_PLAY_SESSION_ID"];

        // Only usefull for Site Jackpot Name, but it can be assigned in all cases.
        _handpay.SiteJackpotName = (String)_row["HP_SITE_JACKPOT_NAME"];
        // RAB 22-MAR-2016: Bug 9140
        _handpay.Amt0 = (Decimal)_row["HP_AMT0"];

        // Check whether the handpay can be discounted from the account
        if (!_handpay.CheckCancellation())
        {
          _handpay.HandpayMsg(Handpays.HANDPAY_MSG.CANCEL_NOT_ALLOWED, this.ParentForm, false);

          return;
        }

        // Ask for confirmation
        if (!_handpay.HandpayMsg(Handpays.HANDPAY_MSG.CANCEL_CONF, this.ParentForm, false))
        {
          // Last handpay was NOT cancelled => Stay in the Handpays screen
          return;
        }

        // Check obtained handpay is the same as it is shown in the grid
        if ((_handpay.PlaySessionId != _selected_handpay.PlaySessionId)
              || (_handpay.MovementId != _selected_handpay.MovementId)
              || (_handpay.TerminalId != _selected_handpay.TerminalId)
              || (_handpay.DateTime != _selected_handpay.DateTime)
              || (_handpay.Amount != _selected_handpay.Amount))
        {
          // Selected handpay is no longer available for cancellation
          _handpay.HandpayMsg(Handpays.HANDPAY_MSG.CANCEL_EXPIRED, this.ParentForm, false);

          return;
        }

        if (!Handpays.GetHandpayProgressiveInfo(_handpay.TerminalId
                                          , _handpay.DateTime
                                          , _handpay.Amount
                                          , out _level
                                          , out _progressive_id
                                          , out _handpay_id))
        {
          _level = 0;
          _progressive_id = 0;
          _handpay_id = 0;
        }

        _handpay.Level = _level;
        _handpay.ProgressiveId = _progressive_id;
        _handpay.HandpayId = _handpay_id;

        // Let's proceed to the handpay cancellation
        if (!_handpay.Cancel())
        {
          // Error cancelling the Handpay => Return to the account screen
          _handpay.HandpayMsg(Handpays.HANDPAY_MSG.CANCEL_NOT_ALLOWED, this.ParentForm, false);

          return;
        }

        Misc.WriteLog("[FORM CLOSE] frm_handpays (cancel)", Log.Type.Message);
        // Last handpay was cancelled => Close the Handpays screen and return to the account screen
        this.Close();
      }

      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return;
      }

      finally
      {
        // Close connection
        if (_da != null)
        {
          _da.Dispose();
          _da = null;
        }

        if (_ds != null)
        {
          _ds.Dispose();
          _ds = null;
        }

        if (_sql_conn != null)
        {
          _sql_conn.Close();
          _sql_conn = null;
        }
      }

    } // btn_cancel_handpay_Click

    #endregion Buttons
  }

  public class HandPay
  {
    private Int32 m_terminal_id;
    private String m_terminal_name;
    private String m_terminal_provider;
    private Decimal m_amount;
    private Int64 m_movement_id;
    private CardData m_card;
    private DateTime m_datetime;
    private HANDPAY_TYPE m_type;
    private String m_type_text;
    private Int64 m_play_session_id;
    private String m_site_jackpot_name;
    private Int32 m_site_jackpot_awarded_on_terminal_id;
    private Currency m_session_played_amount;
    private String m_denomination_str;
    private String m_denomination_value;
    private Int32 m_level;
    private Int64 m_progressive_id;
    private Int64 m_handpay_id = 0;
    private Int32 m_status;
    private Decimal m_tax_base_amount = 0;
    private Decimal m_tax_amount = 0;
    private Decimal m_tax_pct = 0;
    private Decimal m_partial_pay = 0;
    private Int64 m_account_operation_reason_id;
    private String m_account_operation_comment;
    private String m_ao_comment_handpays; // EOR 24-FEB-2016 Add Comments HandPays
    private Boolean m_is_dispute; // EOR 25-FEB-2016 Dispute
    private Currency m_amt_0;
    private String m_cur_0;
    private Currency m_amt_1;
    private String m_cur_1;
    private Handpays.HANDPAY_MSG m_handpay_error_msg; 

    public HandPay(CardData RelatedCard)
    {
      Card = RelatedCard;

      TerminalId = 0;
      TerminalName = "";
      TerminalProvider = "";
      Amount = 0;
      MovementId = 0;
      Type = HANDPAY_TYPE.UNKNOWN;
      PlaySessionId = 0;
      AccountOperationReasonId = 0;
      AccountOperationComment = "";
      AccountOperationCommentHandPays = "";
    }

    public CardData Card
    {
      get { return m_card; }
      set { m_card = value; }
    }

    public Int32 TerminalId
    {
      get { return m_terminal_id; }
      set { m_terminal_id = value; }
    }

    public String TerminalName
    {
      get { return m_terminal_name; }
      set { m_terminal_name = value; }
    }

    public String TerminalProvider
    {
      get { return m_terminal_provider; }
      set { m_terminal_provider = value; }
    }

    public Decimal Amount
    {
      get { return m_amount; }
      set { m_amount = value; }
    }

    public Int64 MovementId
    {
      get { return m_movement_id; }
      set { m_movement_id = value; }
    }

    public DateTime DateTime
    {
      get { return m_datetime; }
      set { m_datetime = value; }
    }

    public Int32 Level
    {
      get { return m_level; }
      set { m_level = value; }
    }

    public Int64 ProgressiveId
    {
      get { return m_progressive_id; }
      set { m_progressive_id = value; }
    }

    public Int64 HandpayId
    {
      get { return m_handpay_id; }
      set { m_handpay_id = value; }
    }

    public Int32 Status
    {
      get { return m_status; }
      set { m_status = value; }
    }

    public HANDPAY_TYPE Type
    {
      get { return m_type; }
      set
      {
        m_type = value;

        TypeName = Handpays.HandpayTypeString(m_type, m_level);
        if (String.IsNullOrEmpty(TypeName))
        {
          m_type = HANDPAY_TYPE.UNKNOWN;
        }

      } // set
    }

    public String TypeName
    {
      get { return m_type_text; }
      set { m_type_text = value; }
    }

    public String SiteJackpotName
    {
      get { return m_site_jackpot_name; }
      set { m_site_jackpot_name = value; }
    }

    public Int32 SiteJackpotAwardedOnTerminalId
    {
      get { return m_site_jackpot_awarded_on_terminal_id; }
      set { m_site_jackpot_awarded_on_terminal_id = value; }
    }

    public Int64 PlaySessionId
    {
      get { return m_play_session_id; }
      set { m_play_session_id = value; }
    }

    public Currency SessionPlayedAmount
    {
      get { return m_session_played_amount; }
      set { m_session_played_amount = value; }
    }

    public String DenominationStr
    {
      get { return m_denomination_str; }
      set { m_denomination_str = value; }
    }

    public String DenominationValue
    {
      get { return m_denomination_value; }
      set { m_denomination_value = value; }
    }

    public Decimal TaxBaseAmount
    {
      get { return m_tax_base_amount; }
      set { m_tax_base_amount = value; }
    }

    public Decimal TaxAmount
    {
      get { return m_tax_amount; }
      set { m_tax_amount = value; }
    }

    public Decimal TaxPct
    {
      get { return m_tax_pct; }
      set { m_tax_pct = value; }
    }

    public Decimal PartialPay
    {
      get { return m_partial_pay; }
      set { m_partial_pay = value; }
    }

    public Int64 AccountOperationReasonId
    {
      get { return m_account_operation_reason_id; }
      set { m_account_operation_reason_id = value; }
    }

    public String AccountOperationComment
    {
      get { return m_account_operation_comment; }
      set { m_account_operation_comment = value; }
    }

    public Decimal Amt0
    {
      get { return m_amt_0; }
      set { m_amt_0 = value; }
    }

    public String Cur0
    {
      get { return m_cur_0; }
      set { m_cur_0 = value; }
    }

    public Decimal Amt1
    {
      get { return m_amt_1; }
      set { m_amt_1 = value; }
    }

    public String Cur1
    {
      get { return m_cur_1; }
      set { m_cur_1 = value; }
    }

    //EOR 24-FEB-2016 Add Comments HandPays
    public String AccountOperationCommentHandPays
    {
      get { return m_ao_comment_handpays; }
      set { m_ao_comment_handpays = value; }
    }

    //EOR 25-FEB-2016 Dispute
    public Boolean IsDispute
    {
      get { return m_is_dispute; }
      set { m_is_dispute = value; }
    }

    public Handpays.HANDPAY_MSG HandpayErrorMsg
    {
      get { return m_handpay_error_msg; }
      set { m_handpay_error_msg = value; }
    }

    public void SetPaidStatus()
    {
      this.Status = Handpays.ChangeStatus(this.Status, HANDPAY_STATUS.PAID);
    }

    public void SetAuthorizedStatus()
    {
      this.Status = Handpays.ChangeStatus(this.Status, HANDPAY_STATUS.PENDING);
      this.Status = Handpays.ChangeStatus(this.Status, HANDPAY_STATUS.AUTHORIZED);
    }

    public void SetVoidedStatus()
    {
      this.Status = Handpays.ChangeStatus(this.Status, HANDPAY_STATUS.VOIDED);
    }

    public bool HandpayMsg(Handpays.HANDPAY_MSG MsgType, Form ParentForm, Boolean IsApplyTax)
    {
      DialogResult _msbox_answer;
      string[] _message_params = { "", "", "", "", "", "" };
      string _message;
      string _message_title; //EOR 25-FEB-2016
      bool _result;

      _result = true;
      switch (MsgType)
      {
        case Handpays.HANDPAY_MSG.REGISTER_CONF:
          if (Type == HANDPAY_TYPE.SITE_JACKPOT)
          {
            _message_params[0] = TypeName + " " + SiteJackpotName;
          }
          else
          {
            _message_params[0] = TypeName;
          }
          _message_params[1] = Amount.ToString("c");

          _message = Resource.String("STR_FRM_HANDPAYS_010", _message_params);
          _message = _message.Replace("\\r\\n", "\r\n");

          //EOR 25-FEB-2016
          if (IsDispute)
          {
            _message_title = Resource.String("STR_HANDPAY_ACTION_29");
          }
          else
          {
            _message_title = Resource.String("STR_UC_CARD_BTN_HAND_PAY");
          }

          _msbox_answer = frm_message.Show(_message,
                                            _message_title,
                                            MessageBoxButtons.YesNo,
                                            MessageBoxIcon.Question,
                                            ParentForm);

          _result = (_msbox_answer == DialogResult.OK);

          break;

        case Handpays.HANDPAY_MSG.REGISTER_CONF_TITO:
          if (Type == HANDPAY_TYPE.SITE_JACKPOT)
          {
            _message_params[0] = TypeName + " " + SiteJackpotName;
          }
          else
          {
            _message_params[0] = TypeName;
          }

          Handpays.HPAmountsCalculated _calculated;
          Handpays.CalculateCashAmountHandpay(Status, 
                                              Amount, 
                                              TaxBaseAmount,
                                              Type, 
                                              Card,
                                              IsApplyTax,
                                              Tax.ApplyTaxCollectionByHandPay(Type),
                                              out _calculated);

          _message_params[1] = _calculated.CashRedeem.TotalPaid.ToString("c");
          _message = Resource.String("STR_FRM_HANDPAYS_010", _message_params);
          _message = _message.Replace("\\r\\n", "\r\n");

          if (IsDispute)
          {
            _message_title = Resource.String("STR_HANDPAY_ACTION_29");
          }
          else
          {
            _message_title = Resource.String("STR_UC_CARD_BTN_HAND_PAY");
          }

          _msbox_answer = frm_message.Show(_message,
                                            _message_title,
                                            MessageBoxButtons.YesNo,
                                            MessageBoxIcon.Question,
                                            ParentForm);

          _result = (_msbox_answer == DialogResult.OK);
          break;

        case Handpays.HANDPAY_MSG.CANCEL_CONF:
          _message_params[0] = DateTime.ToString();
          _message_params[1] = TerminalProvider;
          _message_params[2] = TerminalName;
          if (Type == HANDPAY_TYPE.SITE_JACKPOT)
          {
            _message_params[3] = TypeName + " " + SiteJackpotName;
          }
          else
          {
            _message_params[3] = TypeName;
          }
          _message_params[4] = Amount.ToString("c");

          if (Card.PlayerTracking.HolderName != "")
          {
            _message_params[5] = Card.AccountId.ToString() + " - " + Card.PlayerTracking.HolderName;
          }
          else
          {
            _message_params[5] = Card.AccountId.ToString();
          }

          _message = Resource.String("STR_FRM_HANDPAYS_013", _message_params);
          _message = _message.Replace("\\r\\n", "\r\n");

          _msbox_answer = frm_message.Show(_message,
                                            Resource.String("STR_FRM_HANDPAYS_022"),
                                            MessageBoxButtons.YesNo,
                                            MessageBoxIcon.Question,
                                            ParentForm);

          _result = (_msbox_answer == DialogResult.OK);
          break;

        case Handpays.HANDPAY_MSG.REGISTER_ERROR:
          _message_params[0] = DateTime.ToString();
          _message_params[1] = TerminalProvider;
          _message_params[2] = TerminalName;
          if (Type == HANDPAY_TYPE.SITE_JACKPOT)
          {
            _message_params[3] = TypeName + " " + SiteJackpotName;
          }
          else
          {
            _message_params[3] = TypeName;
          }
          _message_params[4] = Amount.ToString("c");

          if (Card.PlayerTracking.HolderName != "")
          {
            _message_params[5] = Card.AccountId.ToString() + " - " + Card.PlayerTracking.HolderName;
          }
          else
          {
            _message_params[5] = Card.AccountId.ToString();
          }

          _message = Resource.String("STR_FRM_HANDPAYS_011", _message_params);
          _message = _message.Replace("\\r\\n", "\r\n");

          _msbox_answer = frm_message.Show(_message,
                                            Resource.String("STR_APP_GEN_MSG_ERROR"),
                                            MessageBoxButtons.OK,
                                            MessageBoxIcon.Error,
                                            ParentForm);

          Log.Error("frm_handpays.SELECT.Handpay.Register: " + _message);

          break;

        case Handpays.HANDPAY_MSG.REGISTER_ERROR_ALREADY_PAID:
          _message_params[0] = DateTime.ToString();
          _message_params[1] = TerminalProvider;
          _message_params[2] = TerminalName;
          if (Type == HANDPAY_TYPE.SITE_JACKPOT)
          {
            _message_params[3] = TypeName + " " + SiteJackpotName;
          }
          else
          {
            _message_params[3] = TypeName;
          }
          _message_params[4] = Amount.ToString("c");

          if (Card.PlayerTracking.HolderName != "")
          {
            _message_params[5] = Card.AccountId.ToString() + " - " + Card.PlayerTracking.HolderName;
          }
          else
          {
            _message_params[5] = Card.AccountId.ToString();
          }

          _message = Resource.String("STR_FRM_HANDPAYS_050", _message_params);
          _message = _message.Replace("\\r\\n", "\r\n");

          _msbox_answer = frm_message.Show(_message,
                                            Resource.String("STR_APP_GEN_MSG_ERROR"),
                                            MessageBoxButtons.OK,
                                            MessageBoxIcon.Warning,
                                            ParentForm);

          Log.Error("frm_handpays.SELECT.Handpay.Register: " + _message);

          break;

        case Handpays.HANDPAY_MSG.REGISTER_ERROR_VOIDED:
          _message_params[0] = DateTime.ToString();
          _message_params[1] = TerminalProvider;
          _message_params[2] = TerminalName;
          if (Type == HANDPAY_TYPE.SITE_JACKPOT)
          {
            _message_params[3] = TypeName + " " + SiteJackpotName;
          }
          else
          {
            _message_params[3] = TypeName;
          }
          _message_params[4] = Amount.ToString("c");

          if (Card.PlayerTracking.HolderName != "")
          {
            _message_params[5] = Card.AccountId.ToString() + " - " + Card.PlayerTracking.HolderName;
          }
          else
          {
            _message_params[5] = Card.AccountId.ToString();
          }

          _message = Resource.String("STR_FRM_HANDPAYS_051", _message_params);
          _message = _message.Replace("\\r\\n", "\r\n");

          _msbox_answer = frm_message.Show(_message,
                                            Resource.String("STR_APP_GEN_MSG_ERROR"),
                                            MessageBoxButtons.OK,
                                            MessageBoxIcon.Warning,
                                            ParentForm);

          Log.Error("frm_handpays.SELECT.Handpay.Register: " + _message);

          break;

        case Handpays.HANDPAY_MSG.CANCEL_NOT_FOUND:
          if (Card.PlayerTracking.HolderName != "")
          {
            _message_params[0] = Card.AccountId.ToString() + " - " + Card.PlayerTracking.HolderName;
          }
          else
          {
            _message_params[0] = Card.AccountId.ToString();
          }

          _message = Resource.String("STR_FRM_HANDPAYS_014", _message_params);

          _msbox_answer = frm_message.Show(_message,
                                            Resource.String("STR_APP_GEN_MSG_WARNING"),
                                            MessageBoxButtons.OK,
                                            MessageBoxIcon.Warning,
                                            ParentForm);
          break;

        case Handpays.HANDPAY_MSG.CANCEL_NOT_ALLOWED:
          if (Card.PlayerTracking.HolderName != "")
          {
            _message_params[0] = Card.AccountId.ToString() + " - " + Card.PlayerTracking.HolderName;
          }
          else
          {
            _message_params[0] = Card.AccountId.ToString();
          }

          _message_params[1] = Amount.ToString("#,##0.00");

          _message = Resource.String("STR_FRM_HANDPAYS_015", _message_params);
          _message = _message.Replace("\\r\\n", "\r\n");

          _msbox_answer = frm_message.Show(_message,
                                            Resource.String("STR_APP_GEN_MSG_WARNING"),
                                            MessageBoxButtons.OK,
                                            MessageBoxIcon.Warning,
                                            ParentForm);
          break;

        case Handpays.HANDPAY_MSG.CANCEL_EXPIRED:
          _message = Resource.String("STR_FRM_HANDPAYS_025", _message_params);

          _msbox_answer = frm_message.Show(_message,
                                            Resource.String("STR_APP_GEN_MSG_WARNING"),
                                            MessageBoxButtons.OK,
                                            MessageBoxIcon.Warning,
                                            ParentForm);

          break;

        case Handpays.HANDPAY_MSG.REGISTER_NOT_ALLOWED:
          if (Card.PlayerTracking.HolderName != "")
          {
            _message_params[0] = Card.AccountId.ToString() + " - " + Card.PlayerTracking.HolderName;
          }
          else
          {
            _message_params[0] = Card.AccountId.ToString();
          }

          _message = Resource.String("STR_FRM_HANDPAYS_016", _message_params);
          _message = _message.Replace("\\r\\n", "\r\n");

          _msbox_answer = frm_message.Show(_message,
                                            Resource.String("STR_APP_GEN_MSG_WARNING"),
                                            MessageBoxButtons.OK,
                                            MessageBoxIcon.Warning,
                                            ParentForm);
          break;

        default:
          _result = false;
          break;
      }

      return _result;
    } // HandpayMsg

    public Boolean DB_InsertHandpayPlaySession(HANDPAY_TYPE Type,
                                               Currency HandpayAmount,
                                               out Int64 PlaySessionId,
                                               SqlTransaction SqlTrx)
    {
      SqlCommand _sql_command;
      String _sql_query;
      SqlParameter _sql_play_session;
      Int32 _num_rows_inserted;

      _num_rows_inserted = 0;
      PlaySessionId = -1;

      try
      {
        _sql_query = "INSERT INTO PLAY_SESSIONS ( PS_ACCOUNT_ID"
                                             + ", PS_TERMINAL_ID"
                                             + ", PS_TYPE"
                                             + ", PS_INITIAL_BALANCE"
                                             + ", PS_FINAL_BALANCE"
                                             + ", PS_PLAYED_COUNT"
                                             + ", PS_PLAYED_AMOUNT"
                                             + ", PS_WON_COUNT"
                                             + ", PS_WON_AMOUNT"
                                             + ", PS_CASH_IN"
                                             + ", PS_CASH_OUT"
                                             + ", PS_STATUS"
                                             + ", PS_STARTED"
                                             + ", PS_FINISHED"
                                             + ", PS_STAND_ALONE"
                                             + ", PS_PROMO"
                                             + ")"
                                     + "VALUES ( @AccountId"
                                            + ", @TerminalId"
                                            + ", @Type"
                                            + ", @InitialBalance"
                                            + ", @FinalBalance"
                                            + ", 0"
                                            + ", 0"
                                            + ", 0"
                                            + ", @WonAmount"
                                            + ", 0"
                                            + ", 0"
                                            + ", 1"
                                            + ", GETDATE()"
                                            + ", GETDATE()"
                                            + ", 1"
                                            + ", 0"
                                            + ")"
                                         + " SET @PlaySessionId = SCOPE_IDENTITY()";

        _sql_command = new SqlCommand(_sql_query);
        _sql_command.Connection = SqlTrx.Connection;
        _sql_command.Transaction = SqlTrx;

        _sql_command.Parameters.Add("@AccountId", SqlDbType.BigInt, 8, "PS_ACCOUNT_ID").Value = Card.AccountId;
        _sql_command.Parameters.Add("@TerminalId", SqlDbType.Int, 4, "PS_TERMINAL_ID").Value = TerminalId;
        _sql_command.Parameters.Add("@Type", SqlDbType.Int, 4, "PS_TYPE").Value = (Int32)0;
        _sql_command.Parameters.Add("@InitialBalance", SqlDbType.Money).Value = (Decimal)Card.CurrentBalance;
        _sql_command.Parameters.Add("@FinalBalance", SqlDbType.Money).Value = (Decimal)(Card.CurrentBalance + HandpayAmount);

        switch (Type)
        {
          case HANDPAY_TYPE.JACKPOT:
          case HANDPAY_TYPE.SITE_JACKPOT:
            _sql_command.Parameters.Add("@WonAmount", SqlDbType.Money).Value = (Decimal)HandpayAmount;
            break;

          case HANDPAY_TYPE.CANCELLED_CREDITS:
          case HANDPAY_TYPE.ORPHAN_CREDITS:
          case HANDPAY_TYPE.MANUAL:
          default:
            _sql_command.Parameters.Add("@WonAmount", SqlDbType.Money).Value = 0;
            break;
        }

        _sql_play_session = _sql_command.Parameters.Add("@PlaySessionId", SqlDbType.BigInt, 8, "PS_PLAY_SESSION_ID");
        _sql_play_session.Direction = ParameterDirection.Output;

        _num_rows_inserted = _sql_command.ExecuteNonQuery();

        PlaySessionId = (Int64)_sql_play_session.Value;

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }

      finally
      {
        if (_num_rows_inserted != 1)
        {
          Log.Error("DB_InsertHandpayPlaySession. Play Session not inserted.");
        }
      }

    } // DB_InsertHandpayPlaySession

    //------------------------------------------------------------------------------
    // PURPOSE : Checks whether the handpay can be cancelled or not
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    // 
    //   NOTES :

    public Boolean CheckPayment()
    {
      CardData _card_data;

      // Requisites
      //    - Card must not be in use
      //    - Related account must have enough credit to return the handpay amount

      try
      {
        // Get the card data
        _card_data = new CardData();

        if (!CardData.DB_CardGetAllData(Card.AccountId, _card_data))
        {
          Log.Error("HandPay.CheckPayment: Error getting account data.");

          return false;
        }

        //    - Card must not be in use
        // MPO: WIG-1503: the message should show when the card is not virtual
        if (!_card_data.IsVirtualCard && (_card_data.IsLocked || _card_data.IsLoggedIn))
        {
          // The account is in use
          return false;
        }

        // Handpay can be paid
        return true;
      }
      catch (Exception ex)
      {
        Log.Error("HandPay.CheckPayment");
        Log.Exception(ex);

        return false;
      }
      finally
      {
      } // finally

    } // CheckPayment

    public Boolean Register(DateTime HandpayDate)
    {
      Handpays.HandpayRegisterOrCancelParameters _input;
      TYPE_SITE_JACKPOT_WINNER _jackpot_info;
      Int16 _handpay_error_msg;

      using (DB_TRX _db_trx = new DB_TRX())
      {

        _input = new Handpays.HandpayRegisterOrCancelParameters();
        _input.CancelHandpay = false;

        _input.HandpayType = Type;
        _input.HandpayDate = HandpayDate;
        _input.HandpayAmount = Amount;

        _input.CardData = Card;
        _input.TerminalId = TerminalId;
        _input.TerminalProvider = TerminalProvider;
        _input.TerminalName = TerminalName;
        _input.SessionPlayedAmount = SessionPlayedAmount;
        _input.DenominationStr = DenominationStr;
        _input.DenominationValue = DenominationValue;
        _input.ProgressiveId = ProgressiveId;
        _input.HandpayId = HandpayId;
        _input.Level = Level;
        _input.HandpayAmt0 = Amt0;
        _input.HandpayCur0 = Cur0;
        _input.HandpayAmt1 = Amt1;
        _input.HandpayCur1 = Cur1;
        _input.AccountOperationCommentHandPay = AccountOperationCommentHandPays;

        if (!Handpays.RegisterOrCancel(_input, Cashier.CashierSessionInfo(), OperationCode.HANDPAY, out _handpay_error_msg, _db_trx.SqlTransaction))
        {
          return false;
        }

        if (_input.HandpayType == HANDPAY_TYPE.SITE_JACKPOT)
        {
          _jackpot_info = new TYPE_SITE_JACKPOT_WINNER();

          _jackpot_info.awarded_date = _input.HandpayDate;
          _jackpot_info.awarded_on_terminal_id = _input.TerminalId;
          _jackpot_info.amount = _input.HandpayAmount;
          _jackpot_info.awarded_to_account_id = _input.CardData.AccountId;

          CardData.UpdateJackpotNotifiedStatus(_jackpot_info, SITE_JACKPOT_NOTIFICATION_STATUS.Notified, _db_trx.SqlTransaction);
        }

        _db_trx.Commit();

        if (_input.Voucher != null)
        {
          VoucherPrint.Print(_input.Voucher);
        }

      }

      return true;

    } // Register

    //------------------------------------------------------------------------------
    // PURPOSE : Checks whether the handpay can be cancelled or not
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    // 
    //   NOTES :

    public Boolean CheckCancellation()
    {
      CardData _card_data;

      // Requisites
      //    - Card must not be in use
      //    - Related account must have enough credit to return the handpay amount

      try
      {
        // Get the card data
        _card_data = new CardData();

        if (!CardData.DB_CardGetAllData(Card.AccountId, _card_data))
        {
          Log.Error("HandPay.CheckCancellation: Error getting account data.");

          return false;
        }

        //    - Card must not be in use
        if (_card_data.IsLocked || _card_data.IsLoggedIn)
        {
          // The account is in use
          return false;
        }

        //  - Related account must have enough credit to return the handpay amount
        if (Amount > _card_data.CurrentBalance)
        {
          Log.Error("HandPay.CheckCancellation: Not enough credit to subtract.");

          return false;
        }

        // Handpay can be cancelled
        return true;
      }
      catch (Exception ex)
      {
        Log.Error("HandPay.CheckCancellation");
        Log.Exception(ex);

        return false;
      }
      finally
      {
      } // finally

    } // CheckCancellation

    //------------------------------------------------------------------------------
    // PURPOSE : Cancels the handpay
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    // 
    //   NOTES :

    public Boolean Cancel()
    {
      Handpays.HandpayRegisterOrCancelParameters _input;
      TYPE_SITE_JACKPOT_WINNER _jackpot_info;
      Int16 _handpay_error_msg;

      using (DB_TRX _db_trx = new DB_TRX())
      {
        _input = new Handpays.HandpayRegisterOrCancelParameters();
        _input.CancelHandpay = true;
        _input.CancelMovementId = MovementId;
        _input.CancelPlaySessionId = PlaySessionId;

        _input.HandpayType = Type;
        _input.HandpayDate = DateTime;
        _input.HandpayAmount = Amount;
        // RAB 22-MAR-2016: Bug 9140
        _input.HandpayAmt0 = Amt0;

        _input.CardData = Card;
        _input.TerminalId = TerminalId;
        _input.TerminalProvider = TerminalProvider;
        _input.TerminalName = TerminalName;
        _input.SessionPlayedAmount = SessionPlayedAmount;
        _input.DenominationStr = DenominationStr;
        _input.DenominationValue = DenominationValue;
        _input.HandpayId = HandpayId;
        _input.Level = Level;
        _input.ProgressiveId = ProgressiveId;

        if (!Handpays.RegisterOrCancel(_input, Cashier.CashierSessionInfo(), OperationCode.HANDPAY_CANCELLATION, out _handpay_error_msg, _db_trx.SqlTransaction))
        {
          return false;
        }

        if (_input.HandpayType == HANDPAY_TYPE.SITE_JACKPOT)
        {
          _jackpot_info = new TYPE_SITE_JACKPOT_WINNER();

          _jackpot_info.awarded_date = _input.HandpayDate;
          _jackpot_info.awarded_on_terminal_id = _input.TerminalId;
          _jackpot_info.amount = _input.HandpayAmount;
          _jackpot_info.awarded_to_account_id = _input.CardData.AccountId;

          CardData.UpdateJackpotNotifiedStatus(_jackpot_info, SITE_JACKPOT_NOTIFICATION_STATUS.NotNotified, _db_trx.SqlTransaction);
        }

        _db_trx.Commit();

        if (_input.Voucher != null)
        {
          VoucherPrint.Print(_input.Voucher);
        }

      }

      return true;


    } // Cancel
  }
}
