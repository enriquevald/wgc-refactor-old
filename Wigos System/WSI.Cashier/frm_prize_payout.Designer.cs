﻿namespace WSI.Cashier
{
  partial class frm_prize_payout
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.btn_cancel = new WSI.Cashier.Controls.uc_round_button();
      this.btn_pay = new WSI.Cashier.Controls.uc_round_button();
      this.btn_pay_and_recharge = new WSI.Cashier.Controls.uc_round_button();
      this.uc_input_amount = new WSI.Cashier.uc_input_amount();
      this.web_browser = new System.Windows.Forms.WebBrowser();
      this.gb_total = new WSI.Cashier.Controls.uc_round_panel();
      this.lbl_total_to_pay = new WSI.Cashier.Controls.uc_label();
      this.pnl_data.SuspendLayout();
      this.gb_total.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.Controls.Add(this.gb_total);
      this.pnl_data.Controls.Add(this.web_browser);
      this.pnl_data.Controls.Add(this.uc_input_amount);
      this.pnl_data.Controls.Add(this.btn_pay_and_recharge);
      this.pnl_data.Controls.Add(this.btn_cancel);
      this.pnl_data.Controls.Add(this.btn_pay);
      this.pnl_data.Size = new System.Drawing.Size(747, 658);
      // 
      // btn_cancel
      // 
      this.btn_cancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btn_cancel.FlatAppearance.BorderSize = 0;
      this.btn_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_cancel.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_cancel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_cancel.Image = null;
      this.btn_cancel.IsSelected = false;
      this.btn_cancel.Location = new System.Drawing.Point(260, 592);
      this.btn_cancel.Name = "btn_cancel";
      this.btn_cancel.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_cancel.Size = new System.Drawing.Size(155, 60);
      this.btn_cancel.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_cancel.TabIndex = 23;
      this.btn_cancel.Text = "XCANCEL";
      this.btn_cancel.UseVisualStyleBackColor = false;
      // 
      // btn_pay
      // 
      this.btn_pay.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_pay.Enabled = false;
      this.btn_pay.FlatAppearance.BorderSize = 0;
      this.btn_pay.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_pay.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_pay.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_pay.Image = null;
      this.btn_pay.IsSelected = false;
      this.btn_pay.Location = new System.Drawing.Point(421, 592);
      this.btn_pay.Name = "btn_pay";
      this.btn_pay.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_pay.Size = new System.Drawing.Size(155, 60);
      this.btn_pay.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_pay.TabIndex = 24;
      this.btn_pay.Text = "XPAY";
      this.btn_pay.UseVisualStyleBackColor = false;
      this.btn_pay.Click += new System.EventHandler(this.btn_pay_Click);
      // 
      // btn_pay_and_recharge
      // 
      this.btn_pay_and_recharge.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_pay_and_recharge.Enabled = false;
      this.btn_pay_and_recharge.FlatAppearance.BorderSize = 0;
      this.btn_pay_and_recharge.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_pay_and_recharge.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_pay_and_recharge.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_pay_and_recharge.Image = null;
      this.btn_pay_and_recharge.IsSelected = false;
      this.btn_pay_and_recharge.Location = new System.Drawing.Point(582, 592);
      this.btn_pay_and_recharge.Name = "btn_pay_and_recharge";
      this.btn_pay_and_recharge.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_pay_and_recharge.Size = new System.Drawing.Size(155, 60);
      this.btn_pay_and_recharge.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_pay_and_recharge.TabIndex = 25;
      this.btn_pay_and_recharge.Text = "XPAYANDRECHARGE";
      this.btn_pay_and_recharge.UseVisualStyleBackColor = false;
      this.btn_pay_and_recharge.Click += new System.EventHandler(this.btn_pay_and_recharge_Click);
      // 
      // uc_input_amount
      // 
      this.uc_input_amount.BackColor = System.Drawing.Color.Transparent;
      this.uc_input_amount.Location = new System.Drawing.Point(17, 17);
      this.uc_input_amount.Margin = new System.Windows.Forms.Padding(0);
      this.uc_input_amount.Name = "uc_input_amount";
      this.uc_input_amount.Size = new System.Drawing.Size(382, 382);
      this.uc_input_amount.TabIndex = 26;
      this.uc_input_amount.WithDecimals = false;
      this.uc_input_amount.OnAmountSelected += new WSI.Cashier.uc_input_amount.AmountSelectedEventHandler(this.uc_input_amount_OnAmountSelected);
      this.uc_input_amount.OnModifyValue += new WSI.Cashier.uc_input_amount.ModifyValueEventHandler(this.uc_input_amount_OnModifyValue);
      // 
      // web_browser
      // 
      this.web_browser.Location = new System.Drawing.Point(413, 17);
      this.web_browser.MinimumSize = new System.Drawing.Size(20, 20);
      this.web_browser.Name = "web_browser";
      this.web_browser.Size = new System.Drawing.Size(320, 448);
      this.web_browser.TabIndex = 29;
      // 
      // gb_total
      // 
      this.gb_total.BackColor = System.Drawing.Color.Transparent;
      this.gb_total.BorderColor = System.Drawing.Color.Empty;
      this.gb_total.Controls.Add(this.lbl_total_to_pay);
      this.gb_total.CornerRadius = 10;
      this.gb_total.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_total.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.gb_total.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_total.HeaderHeight = 35;
      this.gb_total.HeaderSubText = null;
      this.gb_total.HeaderText = "XTOTAL";
      this.gb_total.Location = new System.Drawing.Point(413, 471);
      this.gb_total.Name = "gb_total";
      this.gb_total.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.gb_total.Size = new System.Drawing.Size(320, 115);
      this.gb_total.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.TOTAL;
      this.gb_total.TabIndex = 8;
      // 
      // lbl_total_to_pay
      // 
      this.lbl_total_to_pay.BackColor = System.Drawing.Color.Transparent;
      this.lbl_total_to_pay.Font = new System.Drawing.Font("Open Sans Semibold", 32F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_total_to_pay.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.lbl_total_to_pay.Location = new System.Drawing.Point(3, 50);
      this.lbl_total_to_pay.Name = "lbl_total_to_pay";
      this.lbl_total_to_pay.Size = new System.Drawing.Size(314, 46);
      this.lbl_total_to_pay.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_32PX_WHITE;
      this.lbl_total_to_pay.TabIndex = 26;
      this.lbl_total_to_pay.Text = "$XXX.XX";
      this.lbl_total_to_pay.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // frm_prize_payout
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(747, 713);
      this.Name = "frm_prize_payout";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "frm_prize_payout";
      this.pnl_data.ResumeLayout(false);
      this.gb_total.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private Controls.uc_round_button btn_pay_and_recharge;
    private Controls.uc_round_button btn_cancel;
    private Controls.uc_round_button btn_pay;
    private uc_input_amount uc_input_amount;
    public System.Windows.Forms.WebBrowser web_browser;
    private Controls.uc_round_panel gb_total;
    private Controls.uc_label lbl_total_to_pay;

  }
}