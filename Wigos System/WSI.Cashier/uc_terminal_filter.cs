//------------------------------------------------------------------------------
// Copyright � 2007-2013 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_terminal_filter.cs
// 
//   DESCRIPTION: Control to filter providers/terminals
//
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 11-NOV-2013 LEM     First release.
// 20-NOV-2013 ICS     Now it can be configured to perform only single searches
// 23-DEC-2013 QMP     Removed External ID and associated function (no longer necessary)
// 10-JAN-2014 LEM     LoadData optimization 
// 15-JAN-2014 JPJ     Refactoring and now it's possible to select a terminal different from the first one
// 29-OCT-2015 ETP     Now it's possible to validate row changes.
// 19-NOV-2015 FAV     Added two events for each list: terminal and provider
// 23-NOV-2015 ETP     PBI 6902: Bug not provider returned if first terminal is selected.
//------------------------------------------------------------------------------

using System;
using WSI.Common;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Windows.Forms;

namespace WSI.Cashier
{
  public partial class uc_terminal_filter : UserControl
  {
    #region Members

    private DataTable m_terminals;
    private Boolean m_show_all_terminals;
    private Boolean m_single_search;
    private Boolean m_select_none;
    private Boolean m_updating_data = false;

    # endregion

    #region Constants

    private Int32 PROVIDER_COLUMN = 0;
    private Int32 TERMINAL_ID_COL = 0;
    private Int32 TERMINAL_NAME_COL = 1;
    private Int32 TERMINAL_PROVIDER_COL = 2;

    # endregion


    public delegate void TerminalOrProviderChangedEventHandler();
    public event TerminalOrProviderChangedEventHandler TerminalOrProviderChanged;

    public delegate void TerminalChangedEventHandler();
    public event TerminalOrProviderChangedEventHandler TerminalChanged;

    public delegate void ProviderChangedEventHandler();
    public event TerminalOrProviderChangedEventHandler ProviderChanged;

    public delegate void TerminalOrProviderRowValidatingHandler(DataGridViewCellValidatingEventArgs e);
    public event TerminalOrProviderRowValidatingHandler ValidatingTerminalOrProvider;

    public uc_terminal_filter()
    {
      InitializeComponent();
    }

    public void InitControls(String Title, String WhereCondition, Boolean ShowAllTerminals)
    {
      InitControls(Title, WhereCondition, ShowAllTerminals, false);
    }
    public void InitControls(String Title, String WhereCondition, Boolean ShowAllTerminals, Boolean SingleSearch)
    {
      Boolean _enabled;
      _enabled = this.Enabled;

      m_show_all_terminals = ShowAllTerminals;
      m_single_search = SingleSearch;
      InitControlsResources();
      this.Enabled = true;
      LoadData(WhereCondition);
      this.Enabled = _enabled;
      this.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
    }

    public String ProviderSelected()
    {
      Int32 _row_idx;

      _row_idx = dgv_providers.Rows.GetFirstRow(DataGridViewElementStates.Selected);

      if (!m_single_search && _row_idx == 0)
      {
        return "";
      }

      if (_row_idx >= 0)
      {
        return (String)dgv_providers.Rows[_row_idx].Cells[PROVIDER_COLUMN].Value;
      }

      return "";
    }

    public Int32 TerminalSelected()
    {
      Int32 _row_idx;

      _row_idx = dgv_terminals.Rows.GetFirstRow(DataGridViewElementStates.Selected);

      if (_row_idx >= 0)
      {
        return (Int32)dgv_terminals.Rows[_row_idx].Cells[TERMINAL_ID_COL].Value;
      }

      return 0;
    }

    public String TerminalSelectedName()
    {
      Int32 _row_idx;

      _row_idx = dgv_terminals.Rows.GetFirstRow(DataGridViewElementStates.Selected);

      if (_row_idx >= 0)
      {
        return (String)(dgv_terminals.Rows[_row_idx].Cells[TERMINAL_NAME_COL].Value);
      }

      return "";
    }

    public String TerminalSelectedProvider()
    {
      Int32 _row_idx;

      _row_idx = dgv_terminals.Rows.GetFirstRow(DataGridViewElementStates.Selected);

      if (_row_idx >= 0)
      {
        return (String)dgv_terminals.Rows[_row_idx].Cells[TERMINAL_PROVIDER_COL].Value;
      }

      return "";
    }

    public Int32 TerminalsCount()
    {
      return dgv_terminals.Rows.Count;
    }

    public Boolean SelectTerminal(Int32 TerminalId)
    {
      String _provider;
      DataRow[] _row;
      Int32 _idxrow;
      Boolean _providerfound;

      _providerfound = false;
      if (m_terminals == null)
      {

        return false;
      }

      _row = m_terminals.Select("TE_TERMINAL_ID = " + TerminalId);

      if (_row.Length == 0)
      {

        return false;
      }

      _provider = _row[0][2].ToString();
      // Select provider in the grid
      for (_idxrow = 0; _idxrow < dgv_providers.RowCount; _idxrow++)
      {
        if (dgv_providers.Rows[_idxrow].Cells[PROVIDER_COLUMN].Value.ToString() == _provider.ToString())
        {
          dgv_providers.CurrentCell = dgv_providers.Rows[_idxrow].Cells[PROVIDER_COLUMN];
          UpdateTerminalsGrid(_idxrow);
          _providerfound = true;

          break;
        }
      }

      if (_providerfound)
      {
        // Select terminal in the grid
        for (_idxrow = 0; _idxrow < dgv_terminals.RowCount; _idxrow++)
        {
          if ((Int32)dgv_terminals.Rows[_idxrow].Cells[TERMINAL_ID_COL].Value == TerminalId)
          {
            dgv_terminals.Refresh();
            dgv_terminals.CurrentCell = dgv_terminals.Rows[_idxrow].Cells[TERMINAL_NAME_COL];

            return true;
          }
        }
      }

      return false;
    }

    private void LoadData(String WhereCondition)
    {
      StringBuilder _sb;
      DataTable _term_table;
      DataTable _prov_table;

      m_select_none = true;
      _term_table = new DataTable();

      dgv_providers.Rows.Clear();
      dgv_terminals.Rows.Clear();

      _sb = new StringBuilder();
      _sb.AppendLine("   SELECT    TE_TERMINAL_ID     ");
      _sb.AppendLine("           , TE_NAME            ");
      _sb.AppendLine("           , TE_PROVIDER_ID     ");
      _sb.AppendLine("     FROM    TERMINALS          ");
      _sb.AppendLine(WhereCondition);
      _sb.AppendLine(" ORDER BY    TE_NAME ASC ");

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            using (SqlDataAdapter _da = new SqlDataAdapter(_cmd))
            {
              _db_trx.Fill(_da, _term_table);
            }
          }
        }

        m_terminals = _term_table;

        _prov_table = _term_table.DefaultView.ToTable(true, "TE_PROVIDER_ID");

        UpdateProvidersGrid(_prov_table.Select("TE_PROVIDER_ID IS NOT NULL", "TE_PROVIDER_ID ASC"));
        if (dgv_providers.Rows.Count > 0)
        {
          UpdateTerminalsGrid(0);
        }
        m_select_none = false;

      }
      catch (Exception)
      {

      }

    }

    private void UpdateProvidersGrid(DataRow[] _rows)
    {
      Int32 _row_idx;
      String _prov;

      if (!m_single_search)
      {
        _row_idx = dgv_providers.Rows.Add();
        dgv_providers.Rows[_row_idx].Cells[PROVIDER_COLUMN].Value = Resource.String("UC_TERMINALS_SEL_03");
      }

      foreach (DataRow _row in _rows)
      {
        _prov = _row["TE_PROVIDER_ID"].ToString();
        _row_idx = dgv_providers.Rows.Add();
        dgv_providers.Rows[_row_idx].Cells[PROVIDER_COLUMN].Value = _prov;
      }
    }

    private void InitControlsResources()
    {
      dgv_providers.Columns[PROVIDER_COLUMN].HeaderText = Resource.String("UC_TERMINALS_SEL_01");
      dgv_terminals.Columns[TERMINAL_NAME_COL].HeaderText = Resource.String("UC_TERMINALS_SEL_02");
    }

    private void UpdateTerminalsGrid(Int32 ProviderSelectedIdx)
    {
      String _provider;
      Int32 _row_idx;

      _provider = "";
      m_updating_data = true;

      dgv_terminals.Rows.Clear();

      if (ProviderSelectedIdx == -1)
      {

        return;
      }

      if (ProviderSelectedIdx >= 0)
      {
        _provider = dgv_providers.Rows[ProviderSelectedIdx].Cells[PROVIDER_COLUMN].Value.ToString();
      }

      if (!m_single_search)
      {
        _row_idx = dgv_terminals.Rows.Add();
        dgv_terminals.Rows[_row_idx].Cells[TERMINAL_ID_COL].Value = 0;
        dgv_terminals.Rows[_row_idx].Cells[TERMINAL_NAME_COL].Value = Resource.String("UC_TERMINALS_SEL_03");
      }

      if (ProviderSelectedIdx > 0 || m_show_all_terminals)
      {
        foreach (DataRow _row in m_terminals.Rows)
        {
          if (_provider == "" || _provider == _row["TE_PROVIDER_ID"].ToString())
          {
            _row_idx = dgv_terminals.Rows.Add();
            dgv_terminals.Rows[_row_idx].Cells[TERMINAL_ID_COL].Value = _row["TE_TERMINAL_ID"];
            dgv_terminals.Rows[_row_idx].Cells[TERMINAL_NAME_COL].Value = _row["TE_NAME"];
            dgv_terminals.Rows[_row_idx].Cells[TERMINAL_PROVIDER_COL].Value = _row["TE_PROVIDER_ID"];
          }
        }
      }

      m_updating_data = false;
    }

    private void dgv_providers_SelectionChanged(object sender, EventArgs e)
    {
      if (m_select_none)
      {
        return;
      }

      UpdateTerminalsGrid(dgv_providers.Rows.GetFirstRow(DataGridViewElementStates.Selected));

      if (!m_updating_data && TerminalOrProviderChanged != null)
      {
        TerminalOrProviderChanged();
      }

      if (!m_updating_data && ProviderChanged != null)
      {
        ProviderChanged();
      }
    }

    private void uc_terminal_filter_EnabledChanged(object sender, EventArgs e)
    {
      dgv_providers.Enabled = this.Enabled;
      dgv_terminals.Enabled = this.Enabled;

      if (this.Enabled)
      {
        dgv_providers.ForeColor = Color.FromKnownColor(KnownColor.ControlText);
        dgv_terminals.ForeColor = Color.FromKnownColor(KnownColor.ControlText);
      }
      else
      {
        dgv_providers.ForeColor = dgv_providers.BackgroundColor;
        dgv_terminals.ForeColor = dgv_terminals.BackgroundColor;
      }
    }

    private void dgv_terminals_SelectionChanged(object sender, EventArgs e)
    {
      if (!m_updating_data && TerminalOrProviderChanged != null)
      {
        TerminalOrProviderChanged();
      }

      if (!m_updating_data && TerminalChanged != null)
      {
        TerminalChanged();
      }
    }

    private void dgv_terminals_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
    {
      if (!m_updating_data && ValidatingTerminalOrProvider != null && this.ContainsFocus)
      {
        ValidatingTerminalOrProvider(e);
      }
    }

  }
}