//------------------------------------------------------------------------------
// Copyright � 2011 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_balance_mismatch.cs
// 
//   DESCRIPTION: Implements the balance mismatch dialog. 
//
//        AUTHOR: Alberto Cuesta
// 
// CREATION DATE: 14-OCT-2011
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 14-OCT-2011 ACC     First release.
// 21-NOV-2013 AMF     New comment when close session.
// 12-NOV-2015 RGR     Change style.
// 04-FEB-2016 GDA     Se agrega una opci�n de cierre al balance descuadrado (balance mismatch) para 
//                     permitir cerrar en cero si no son cero ni el calculado ni el reportado
// 14-MAR-2016 FOS     Fixed Bug 9357: New design corrections
// 25-APR-2016 ETP     Fixed Bug 11740: dgv_close_options visible in not mismatch mode
// 27-JUL-2016 JML     Fixed Bug 15893: Error closing session with balance mismatch
// 12-AUG-2016 FAV     Fixed Bug 16736: Error closing session without balance mismatch
// 12-SEP-2016 JML     Fixed Bug 16305: Closing session with balance mismatch. error in focus.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Windows.Forms;
using WSI.Common;
using System.Threading;
using System.Drawing;
using WSI.Cashier.Controls;

namespace WSI.Cashier
{
  public partial class frm_balance_mismatch : frm_base
  {
    #region Constants

    private const Int32 GRID_COLUMN_OPTION_ID = 0;
    private const Int32 GRID_COLUMN_OPTION_SELECTED = 1;
    private const Int32 GRID_COLUMN_OPTION_NAME = 2;

    #endregion Constants

    #region Enums

    private enum ENUM_BALANCE_CLOSE_OPTION
    {
      CALCULATED = 1,
      REPORTED = 2,
      // 04-FEB-2016 GDA Se agrega un valor al enumerador para la opci�n de cerrar el balance en cero
      ZERO = 3
    }

    #endregion Enums

    #region Members

    private Currency m_current_balance;
    private Currency m_last_reported;
    private Currency m_last_calculated;

    // 04-FEB-2016 GDA Se agrega variable interna para la opci�n de cerrar el balance en cero
    private Currency m_zero_balance;

    private Boolean m_change_balance;
    private Currency m_new_balance;

    private Boolean m_cancel_operation;

    private frm_yesno form_yes_no;

    private DataTable m_dt_close_options = null;

    private Boolean m_balance_mismatch;

    #endregion Members

    #region Constructor

    /// <summary>
    /// Constructor method.
    /// 1. Initialize controls.
    /// 2. Initialize control�s resources. (NLS, Images, etc)
    /// </summary>
    public frm_balance_mismatch()
    {
      InitializeComponent();
      InitializeControlResources();
      form_yes_no = new frm_yesno();
      form_yes_no.Opacity = 0.6;

    }

    #endregion Constructor

    #region Events

    #region DataGridEvents

    private void dgv_close_options_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
    {
        dgv_close_options.Columns[GRID_COLUMN_OPTION_ID].Width = 0;
        dgv_close_options.Columns[GRID_COLUMN_OPTION_ID].Visible = false;

    } // dgv_close_options_RowPrePaint

    private void dgv_close_options_CellClick(object sender, DataGridViewCellEventArgs e) 
    {
        if (e.RowIndex < 0)
        {
            return;
        }
        if ((Boolean)dgv_close_options.Rows[e.RowIndex].Cells[GRID_COLUMN_OPTION_SELECTED].Value)
        {
            return;
        }

        for (Int32 _idx_row = 0; _idx_row < dgv_close_options.Rows.Count; _idx_row++)
        {
            dgv_close_options.Rows[_idx_row].Cells[GRID_COLUMN_OPTION_SELECTED].Value = false;
        }
        dgv_close_options.Rows[e.RowIndex].Cells[GRID_COLUMN_OPTION_SELECTED].Value = true;

        btn_close_session.Enabled = true;
    } // dgv_close_options_CellClick

    private void dgv_close_options_ColumnAdded(object sender, DataGridViewColumnEventArgs e)
    {
        e.Column.SortMode = DataGridViewColumnSortMode.NotSortable;
    } // dgv_close_options_ColumnAdded

    #endregion DataGridEvents

    #region Buttons

    //------------------------------------------------------------------------------
    // PURPOSE : Cancel button
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void btn_cancel_Click(object sender, EventArgs e)
    {
      m_cancel_operation = true;

      Hide();
      form_yes_no.Hide();
    } // btn_cancel_Click

    //------------------------------------------------------------------------------
    // PURPOSE : Close session button
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void btn_close_session_Click(object sender, EventArgs e)
    {
      Int32 _idx_row;
      ENUM_BALANCE_CLOSE_OPTION _close_option;

      for (_idx_row = 0; _idx_row < dgv_close_options.Rows.Count; _idx_row++)
      {
        if ((Boolean)dgv_close_options.Rows[_idx_row].Cells[GRID_COLUMN_OPTION_SELECTED].Value)
        {
          break;
        }
      }

      if (_idx_row == dgv_close_options.Rows.Count)
      {
        if (m_balance_mismatch)
        {
          return;
        }
        else
        {
          m_new_balance = 0;
        }
      }
      else
      {
        _close_option = (ENUM_BALANCE_CLOSE_OPTION)dgv_close_options.Rows[_idx_row].Cells[GRID_COLUMN_OPTION_ID].Value;

        switch (_close_option)
        {
          case ENUM_BALANCE_CLOSE_OPTION.CALCULATED:
            if (m_current_balance != m_last_calculated)
            {
              m_change_balance = true;
              m_new_balance = m_last_calculated;
            }
            break;

          case ENUM_BALANCE_CLOSE_OPTION.REPORTED:
            if (m_current_balance != m_last_reported)
            {
              m_change_balance = true;
              m_new_balance = m_last_reported;
            }
            break;

                // 04-FEB-2016 GDA Se agrega el c�digo que controla el cierre de sesi�n
                //             si se seleccion� la opci�n de balance cero
                case ENUM_BALANCE_CLOSE_OPTION.ZERO:
                    if (m_current_balance != m_zero_balance)
                    {
                        m_change_balance = true;
                        m_new_balance = m_zero_balance;
        }
                    break;
      }
        }

      Hide();
      form_yes_no.Hide();

    } // btn_close_session_Click

    #endregion Buttons

    #region Form

    private void frm_balance_mismatch_Load(object sender, EventArgs e)
    {
      if (dgv_close_options.Rows.Count > 0)
      {
      dgv_close_options.Rows[0].Selected = false;
    }
    }

    #endregion

    #endregion Events

    #region Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Init resources
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void InitializeControlResources()
    {
      // NLS Strings
      //lbl_balance_mismatch_title.Text = "";
      btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");

      if (m_dt_close_options == null)
      {
        m_dt_close_options = new DataTable("CLOSE_OPTIONS");
        m_dt_close_options.Columns.Add("Id", Type.GetType("System.Int32"));
        m_dt_close_options.Columns.Add("Selected", Type.GetType("System.Boolean"));
        m_dt_close_options.Columns.Add("Name", Type.GetType("System.String"));
      }
    } // InitializeControlResources

    //------------------------------------------------------------------------------
    // PURPOSE : Init 
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void Init()
    {

      btn_close_session.Text = Resource.String("STR_UC_CARD_BTN_SESSION_LOG_OFF");
      btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");
      lbl_comment.Text = Resource.String("STR_FRM_BALANCE_LBL_COMMENT");

      //this.Location = new Point(1024 / 2 - this.Width / 2, 768 / 2 - this.Height / 2);

      if (m_balance_mismatch)
      {
        this.FormTitle = Resource.String("STR_FRM_BALANCE_MISMATCH_TITLE");
        btn_close_session.Enabled = false;
      }
      else
      {
        this.FormTitle = Resource.String("STR_UC_CARD_BTN_SESSION_LOG_OFF");
        this.Size = new Size(588, 281);
      }

      txt_comment.Select();
    } // Init

    //------------------------------------------------------------------------------
    // PURPOSE : Initialize Available Draws Data Grid.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private void InitCloseOptions()
    {
      // Close Options DataGrid
      dgv_close_options.RowTemplate.DividerHeight = 0;
      dgv_close_options.Height = 142;

      //    - Option Id
      dgv_close_options.Columns[GRID_COLUMN_OPTION_ID].Width = 0;
      dgv_close_options.Columns[GRID_COLUMN_OPTION_ID].Visible = false;

      //    - Option Selected?
      dgv_close_options.Columns[GRID_COLUMN_OPTION_SELECTED].Width = 0;
      dgv_close_options.Columns[GRID_COLUMN_OPTION_SELECTED].Visible = false;

      //    - Option Name
      dgv_close_options.Columns[GRID_COLUMN_OPTION_NAME].Width = 530;
      dgv_close_options.Columns[GRID_COLUMN_OPTION_NAME].HeaderCell.Value = Resource.String("STR_FRM_BALANCE_MISMATCH_SELECT_OPTION");
      dgv_close_options.Columns[GRID_COLUMN_OPTION_NAME].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

    } // InitCloseOptions

    private void FillCloseOptionsDataTable()
    {
      DataRow _row;

      m_dt_close_options.Clear();

      _row = m_dt_close_options.NewRow();
      _row["Id"] = ENUM_BALANCE_CLOSE_OPTION.CALCULATED;
      _row["Selected"] = false;
      _row["Name"] = Resource.String("STR_FRM_BALANCE_MISMATCH_CALCULATED", m_last_calculated.ToString());
      m_dt_close_options.Rows.Add(_row);

      _row = m_dt_close_options.NewRow();
      _row["Id"] = ENUM_BALANCE_CLOSE_OPTION.REPORTED;
      _row["Selected"] = false;
      _row["Name"] = Resource.String("STR_FRM_BALANCE_MISMATCH_REPORTED", m_last_reported.ToString());
      m_dt_close_options.Rows.Add(_row);

      // 04-FEB-2016 GDA Se agrega una opci�n de cierre al balance descuadrado (balance mismatch) para 
      //                 permitir cerrar en cero si no son cero ni el calculado ni el reportado

      if (m_last_calculated != 0 && m_last_reported != 0)
      {
          m_zero_balance = 0;
          _row = m_dt_close_options.NewRow();
          _row["Id"] = ENUM_BALANCE_CLOSE_OPTION.ZERO;
          _row["Selected"] = false;
          _row["Name"] = Resource.String("STR_FRM_BALANCE_MISMATCH_ZERO", m_zero_balance.ToString());
          m_dt_close_options.Rows.Add(_row);
      }

      // 04-FEB-2016 GDA Se agrega una opci�n de cierre al balance descuadrado (balance mismatch) para 
      //                 permitir cerrar en cero si no son cero ni el calculado ni el reportado

      if (m_last_calculated != 0 && m_last_reported != 0)
      {
          m_zero_balance = 0;
          _row = m_dt_close_options.NewRow();
          _row["Id"] = ENUM_BALANCE_CLOSE_OPTION.ZERO;
          _row["Selected"] = false;
          _row["Name"] = Resource.String("STR_FRM_BALANCE_MISMATCH_ZERO", m_zero_balance.ToString());
          m_dt_close_options.Rows.Add(_row);
      }

    } // FillCloseOptionsDataTable

    #endregion Private Methods

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Show dialog function.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    static public DialogResult Show(Decimal CurrentBalance, Decimal CalculatedBalance, Decimal ReportedBalance, Form Parent, Boolean BalanceMismatch,
                                    out Boolean ChangeBalance, out Decimal NewBalance, out String CloseSessionComment)
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_balance_mismatch", Log.Type.Message);
      DialogResult return_value;
      frm_balance_mismatch wgc_balance_mismatch = new frm_balance_mismatch();

      wgc_balance_mismatch.m_current_balance = (Decimal)CurrentBalance;
      wgc_balance_mismatch.m_last_calculated = (Decimal)CalculatedBalance;
      wgc_balance_mismatch.m_last_reported = (Decimal)ReportedBalance;

      wgc_balance_mismatch.m_cancel_operation = false;
      wgc_balance_mismatch.m_change_balance = false;
      wgc_balance_mismatch.dgv_close_options.Visible = false;
      wgc_balance_mismatch.m_new_balance = wgc_balance_mismatch.m_current_balance;
      wgc_balance_mismatch.m_balance_mismatch = BalanceMismatch;

      if (BalanceMismatch)
      {
        wgc_balance_mismatch.dgv_close_options.Visible = true;
        wgc_balance_mismatch.FillCloseOptionsDataTable();
        wgc_balance_mismatch.dgv_close_options.DataSource = wgc_balance_mismatch.m_dt_close_options;
        wgc_balance_mismatch.InitCloseOptions();
        wgc_balance_mismatch.dgv_close_options.Columns[GRID_COLUMN_OPTION_NAME].SortMode = DataGridViewColumnSortMode.NotSortable;
      }

      wgc_balance_mismatch.Init();
      wgc_balance_mismatch.form_yes_no.Show(Parent);
      wgc_balance_mismatch.ShowDialog(wgc_balance_mismatch.form_yes_no);
      wgc_balance_mismatch.form_yes_no.Hide();

      if (Parent != null)
      {
        Parent.Focus();
      }

      ChangeBalance = wgc_balance_mismatch.m_change_balance;
      NewBalance = wgc_balance_mismatch.m_new_balance;
      CloseSessionComment = wgc_balance_mismatch.txt_comment.Text;

      return_value = DialogResult.OK;
      if (wgc_balance_mismatch.m_cancel_operation)
      {
        return_value = DialogResult.Cancel;
      }

      wgc_balance_mismatch.Dispose();
      wgc_balance_mismatch = null;

      return return_value;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Dispose yes_no form.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    new public void Dispose()
    {
      form_yes_no.Dispose();
      form_yes_no = null;

      base.Dispose();
    }

    #endregion Public Methods

    public override void pb_exit_Click(object sender, EventArgs e)
    {
      m_cancel_operation = true;
      base.pb_exit_Click(sender, e);
    }

  }
}