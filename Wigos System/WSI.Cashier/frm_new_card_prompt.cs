﻿//------------------------------------------------------------------------------
// Copyright © 2007-2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_new_card_prompt.cs
// 
//   DESCRIPTION: validator for cashier model
//                
//                
//                
//                
//                
//                
// 
//        AUTHOR: Scott Adamson.
// 
// CREATION DATE: 02-DEC-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 02-DEC-2007 SJA    First release.
// 03-FEB-2016 FJC    Product Backlog Item 8427:Visitas / Recepción: Modificaciones / Bugs equipo
//------------------------------------------------------------------------------

using System;
using System.Windows.Forms;
using WSI.Cashier.Controls;
using WSI.Common;

namespace WSI.Cashier
{
  public partial class frm_new_card_prompt : frm_base
  {
    public frm_new_card_prompt(string FormTitle = null)
    {
      InitializeComponent();
      uc_card_reader1.ErrorMessageBelow = true;
      base.PerformOnExitClicked+=frm_new_card_prompt_PerformOnExitClicked;
      TrackData = null;
      this.FormTitle = FormTitle ?? Resource.String("STR_FRM_REQUIRE_NEW_CARD_PROMPT");
    }

    private void frm_new_card_prompt_PerformOnExitClicked()
    {
      base.DialogResult= DialogResult.Cancel;
    }

    /// <summary>
    /// react to scan card number
    /// </summary>
    /// <param name="TrackNumber"></param>
    /// <param name="IsValid"></param>
    private void uc_card_reader1_OnTrackNumberReadEvent(string TrackNumber, ref bool IsValid)
    {
      CardData _card_data;
      _card_data = new CardData();


      //Get Data by TrackNumber
      CardData.DB_CardGetAllData(TrackNumber, _card_data, false);

      if (!CardData.CheckTrackdataSiteId(new ExternalTrackData(TrackNumber)))
      {

        Log.Error("LoadCardByAccountId. Card: " + _card_data.TrackData + " doesn´t belong to this site.");

        frm_message.Show(Resource.String("STR_CARD_SITE_ID_ERROR"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK,
                         MessageBoxIcon.Warning, this.ParentForm);

        uc_card_reader1.Focus();

        DialogResult = DialogResult.Cancel;
        return;

      }

      if (_card_data.IsNew)
      {
        _card_data.TrackData = TrackNumber;
        NewAccount(_card_data);
      }
      else
      {
        this.DialogResult = DialogResult.Abort;
      }
    }
    /// <summary>
    /// create a new account for a card which has not yet been assigned
    /// </summary>
    /// <param name="CardData"></param>
    private void NewAccount(CardData CardData)
    {
      String _msgbox_text;
      CardData _aux_card = new CardData();
      String _error_str;

      // MBF 30-NOV-2009 - Check BEFORE asking if the user wants to create
      // Check provided trackdata is not already associated to a MB account
      if (CashierBusinessLogic.DB_IsMBCardDefined(CardData.TrackData))
      {
        // MsgBox: Card already assigned to a Mobile Bank account.
        // frm_message.Show(Resource.String("STR_UC_CARD_USER_MSG_CARD_LINKED_TO_MB_ACCT"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this.ParentForm);

        // uc_card_reader1.Focus();

        // We are going to change the form shown, expire the timer to disable buttons in case of return to this form.
        //tick_count_tracknumber_read = CashierBusinessLogic.DURATION_BUTTONS_ENABLED_AFTER_OPERATION + 1;

        //m_parent.SwitchToMobileBanck(CardData.TrackData);
        // ClearCardData();
        uc_card_reader1.ClearTrackNumber();
        this.DialogResult = DialogResult.Abort;
        return;
      }

      // Card does not exist: request confirmation to create a new one.
      _aux_card.TrackData = CardData.TrackData;

      // RCI 18-OCT-2011: Can't create new accounts when cashier is not open.
      if (CashierBusinessLogic.ReadCashierStatus(Cashier.SessionId) != CASHIER_STATUS.OPEN)
      {
        _msgbox_text = Resource.String("STR_UC_CARD_USER_MSG_CARD_NOT_EXIST_AND_CASH_CLOSED",
          "\n\n" + _aux_card.VisibleTrackdata() + "\n\n");
        frm_message.Show(_msgbox_text, Resource.String("STR_UC_CARD_USER_MSG_CARD_NOT_EXIST_TITLE"),
          MessageBoxButtons.OK, MessageBoxIcon.Warning, this.ParentForm);

        //FJC 03-FEB-2016 Product Backlog Item 8427:Visitas / Recepción: Modificaciones / Bugs equipo
        //  Task 8933:Visitas / Recepción: Creación de Customer
        this.DialogResult = DialogResult.Cancel;

        return;
      }
      // RRR & RCI 23-JUL-2014: Fixed Bug WIG-1121: Error in account refresh.
      //Reset timer to disable buttons
      //tick_count_tracknumber_read = CashierBusinessLogic.DURATION_BUTTONS_ENABLED_AFTER_OPERATION + 1;

      //m_is_new_card = true;
      // JCM 13-APR-2012 Ask the new account type.
      // Check if current user is an authorized user
      if (
        !ProfilePermissions.CheckPermissionList(ProfilePermissions.CashierFormFuncionality.AccountCreatePersonal,
          ProfilePermissions.TypeOperation.RequestPasswd, this.ParentForm, 0, out _error_str))
      {
        this.DialogResult = DialogResult.Abort;
        return;
      }
      this.TrackData = CardData.TrackData;
      this.DialogResult = DialogResult.OK;
      //this.ClearCardData();
      //m_current_track_number = CardData.TrackData;
      //btn_edit_Click(null, null);

    } // NewAccount

    public string TrackData { get; set; }

  }
}