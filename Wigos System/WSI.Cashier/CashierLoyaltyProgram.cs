//------------------------------------------------------------------------------
// Copyright � 2010 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : CashierLoyaltyProgram.cs
// 
//   DESCRIPTION : Contains the classes to manage the gifts points program
//                    1. General Params
//                    2. Loyalty Program
//                    3. Gift
//                    4. Gift Instance
//
// REVISION HISTORY
// 
// Date        Author Description
// ----------- ------ -------------------------------------------------------------------------------------
// 01-OCT-2010 TJG    First release.
// 20-OCT-2010 TJG    Gifts's counters: stock, request and delivery.
// 29-OCT-2010 TJG    New gift: Points per draw numbers
// 25-MAY-2012 SSC    Moved CardData, PlaySession and PlayerTrackingData classes to WSI.Common
// 29-MAY-2012 SSC    Moved LoyaltyProgram, Gift and GiftInstance classes to WSI.Common
// 08-JUN-2012 SSC    Added: Can't request draw numbers, not redeemible and redeemable gifts if the Cashier is closed. Cashier movements for these gift types.
// 13-JUN-2012 SSC    Added new movements for Gifts/Points
// 28-SEP-2012 MPO    Protect delivery function. If is expired or canceled cannot be delivery.
// 04-MAR-2013 DHA    Fixed Bug #178: In Delivery(), changed the initial, final, add and sub values for the account movement.
// 24-MAY-2013 LEM    Modified GiftInstanceCashier.Request to create AccountMovementsTable and CashierMovementsTable with TerminalName.
// 22-OCT-2013 LEM    Unify saving account and cashier movements using SharedMovementsTables clases
// 25-MAR-2014 LEM & DRV Fixed Bug WIGOSTITO-1171: The status of a promotion it's not updated for RE and NR credit gifts.
// 30-JUN-2015 MPO    WIG-2492: Get and set account promo id for calculate cost (Refactor vars)
// 23-NOV-2015 FOS    Bug 6771:Payment tickets & handpays
// 18-AGU-2016 DHA    Fixed Bug 16878: Gift credit on TITO generate tickets with 0 amount 
// 12-SEP-2016 ETP    PBI 17561: Print Tito Ticket in Promobox Gifts.
// 18-OCT-2016 FAV    PBI 20407: TITO Ticket: Calculated fields
//---------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Drawing;
using System.IO;
using System.Text;
using System.Security.Cryptography;
using System.Windows.Forms;
using System.Collections;
using WSI.Common;
using WSI.Cashier.TITO;

namespace WSI.Cashier
{
  #region Public Classes

  //------------------------------------------------------------------------------
  // CLASS : Gift
  // 
  // NOTES :

  public class GiftCashier : Gift
  {
    public GiftCashier()
      : base()
    {
    } // GiftCashier

    public GiftCashier(DataRow Row)
      : base(Row)
    {
    } // GiftCashier

    //------------------------------------------------------------------------------
    // PURPOSE : Function to manage all the messages related to Gifts' basic operations
    //
    //  PARAMS :
    //      - INPUT :
    //          - MsgType
    //          - Card
    //          - ParentForm
    //
    //      - OUTPUT :
    //
    // RETURNS :
    // 
    //   NOTES :
    //
    public bool Message(GIFT_MSG MsgType, CardData Card, Form ParentForm)
    {
      string _message;


      if (!GetMessage(MsgType, Card, out _message))
      {
        return false;
      }

      frm_message.Show(_message,
                        Resource.String("STR_APP_GEN_MSG_WARNING"),
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Warning,
                        ParentForm);

      return true;
    } // Message

    //------------------------------------------------------------------------------
    // PURPOSE : Checks whether the gift can be requested in charge to the account 
    //           represented by the received card.
    //
    //  PARAMS :
    //      - INPUT :
    //          - CardToCheck
    //          - ParentForm
    //
    //      - OUTPUT :
    //
    // RETURNS :
    // 
    //   NOTES :
    //

    //public Boolean CheckGiftRequestCashier (CardData CardToCheck, Form ParentForm)
    //{
    //  GIFT_MSG _gift_msg;

    //  // Requisites
    //  //    - Card must not be in use
    //  //    - NOT_REDEEMABLE_CREDIT gift : Card must not have non-redeemable balance 
    //  //    - REDEEMABLE_CREDIT gift : Card must not have non-redeemable balance 
    //  //    - Related account must have enough points to request the gift
    //  //    - Gift must be available
    //  //    - OBJECT gift : Gift must be in stock 
    //  //    - DRAW NUMBERS gift : 
    //  //    - SERVICES gift :

    //  try
    //  {

    //    if (!CheckGiftRequest(CardToCheck, true, out _gift_msg))
    //    {
    //      Message(_gift_msg, CardToCheck, ParentForm);

    //      return false;
    //    }

    //    // Gift can be requested
    //    return true;
    //  }
    //  catch ( Exception _ex )
    //  {
    //    Log.Error("GiftCashier.CheckGiftRequestCashier");
    //    Log.Exception(_ex);

    //    return false;
    //  }
    //  finally
    //  {
    //  } // finally

    //} // CheckGiftRequestCashier

  } // Class GiftCashier

  //------------------------------------------------------------------------------
  // CLASS : GiftInstance
  // 
  // NOTES :
  //
  public class GiftInstanceCashier : GiftInstance
  {

    #region Members
    private GiftCashier m_gift_cashier;
    #endregion Members

    public GiftInstanceCashier(GiftCashier GiftItem, CardData CardToUse)
      : base(GiftItem, CardToUse)
    {
      m_gift_cashier = GiftItem;
    } // GiftInstanceCashier

    public GiftInstanceCashier(String ExternalCode)
    {
      m_gift_cashier = null;

      if (!Barcode.TryParse(ExternalCode, out m_voucher_number))
      {
        return;
      }
      if (m_voucher_number.Type != BarcodeType.Gift)
      {
        return;
      }
      //    -  Get the gift instance from the database
      if (!DB_GiftInstanceData(m_voucher_number.UniqueId))
      {
        // Wrong voucher id
        return;
      }

      GiftOk = true;

    } // GiftInstanceCashier

    public GiftCashier RelatedGiftCashier
    {
      get { return m_gift_cashier; }
      set { m_gift_cashier = value; }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Retrieves the selected gift data from GIFT_INSTANCES table
    //
    //  PARAMS :
    //      - INPUT :
    //          - GiftInstanceId
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - true: data was retrieved successfully
    //      - false: data was not retrieved
    // 
    //   NOTES :

    private Boolean DB_GiftInstanceData(Int64 GiftInstanceId)
    {

      String _sql_query;

      try
      {

        _sql_query = "SELECT   GIN_OPER_REQUEST_ID " +
                     "       , ISNULL (GIN_OPER_DELIVERY_ID, 0) " +
                     "       , GIN_ACCOUNT_ID " +
                     "       , GIN_GIFT_ID " +
                     "       , GIN_GIFT_NAME " +
                     "       , GIN_GIFT_TYPE " +
                     "       , GIN_POINTS " +
                     "       , ISNULL (GIN_CONVERSION_TO_NRC, 0.0)" +
                     "       , GIN_SPENT_POINTS " +
                     "       , GIN_REQUESTED " +
                     "       , GIN_EXPIRATION " +
                     "       , GIN_DELIVERED " +
                     "       , GIN_NUM_ITEMS " +
                     "       , GIN_DATA_01 " +
                     "       , GIN_REQUEST_STATUS " +
                     "  FROM   GIFT_INSTANCES " +
                     " WHERE   GIN_GIFT_INSTANCE_ID = @GiftInstanceId ";

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sql_query, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@GiftInstanceId", SqlDbType.BigInt).Value = GiftInstanceId;

            using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
            {
              // Read the result corresponding to the gift request
              if (!_reader.Read())
              {
                return false;
              }

              m_gift_cashier = new GiftCashier();
              RelatedCard = new CardData();

              InstanceId = GiftInstanceId;
              RequestOperationId = _reader.GetInt64(0);
              DeliveryOperationId = _reader.GetInt64(1);
              m_account_id = _reader.GetInt64(2);
              m_gift_id = _reader.GetInt64(3);
              m_gift_cashier.GiftName = _reader.GetString(4);
              m_gift_cashier.Type = (GIFT_TYPE)_reader[5];
              m_gift_cashier.Points = _reader.GetDecimal(6);
              m_gift_cashier.ConversionNRC = _reader.GetDecimal(7);
              SpentPoints = _reader.GetDecimal(8);
              RequestDateTime = _reader.GetDateTime(9);
              ExpirationDateTime = _reader.GetDateTime(10);


              Delivered = false;
              if (!_reader.IsDBNull(11))
              {
                Delivered = true;
                DeliveryDateTime = _reader.GetDateTime(11);
              }

              NumUnits = _reader.GetInt32(12);

              if (m_gift_cashier.Type == GIFT_TYPE.DRAW_NUMBERS)
              {
                DrawId = _reader.GetInt64(13);
              }

              m_status = (GIFT_REQUEST_STATUS)_reader.GetInt32(14);

              RelatedGift = m_gift_cashier;
            }
          }
        }

        // Get account's card 
        CardData.DB_CardGetAllData(m_account_id, RelatedCard);

        return true;
      } // try
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }

    } // DB_GiftInstanceData

    //------------------------------------------------------------------------------
    // PURPOSE : Check the gift request the instant before requesting the gift
    //
    //  PARAMS :
    //      - INPUT :
    //          - ParentForm
    //
    //      - OUTPUT :
    //
    // RETURNS :
    // 
    //   NOTES :
    //
    public Boolean CheckGiftRequestCashier(Form ParentForm)
    {
      GiftInstance.GIFT_INSTANCE_MSG _msg_gift_instance;
      Gift.GIFT_MSG _msg_gift;

      RelatedGift = RelatedGiftCashier;

      if (!this.CheckGiftRequest(true, out _msg_gift_instance, out _msg_gift))
      {
        if (_msg_gift_instance != GIFT_INSTANCE_MSG.UNKNOWN)
        {
          this.Message(_msg_gift_instance, ParentForm);
        }
        else if (_msg_gift != Gift.GIFT_MSG.UNKNOWN)
        {
          this.RelatedGiftCashier.Message(_msg_gift, RelatedCard, ParentForm);
        }

        return false;
      }

      return true;
    } // CheckGiftRequestCashier

    //------------------------------------------------------------------------------
    // PURPOSE : Updates into the DB all the data required to request a gift
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    // 
    //   NOTES :
    //
    public Boolean Request(out GiftInstance.GIFT_INSTANCE_MSG Msg)
    {
      ArrayList _voucher_list;
      Boolean _bln_need_cashier_session;
      CashierSessionInfo _cashier_session;
      AccountMovementsTable _ac_mov_table;
      CashierMovementsTable _cm_mov_table;
      Ticket _ticket_promo;
      PrinterStatus _printer_status;
      String _printer_status_text;
      ParamsTicketOperation _operation_params;

      _ticket_promo = null;
      _bln_need_cashier_session = false;
      Msg = GIFT_INSTANCE_MSG.REQUEST_ERROR;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          switch (m_gift_cashier.Type)
          {
            case GIFT_TYPE.DRAW_NUMBERS:
            case GIFT_TYPE.NOT_REDEEMABLE_CREDIT:
            case GIFT_TYPE.REDEEMABLE_CREDIT:
              _bln_need_cashier_session = true;
              break;

            case GIFT_TYPE.OBJECT:
            case GIFT_TYPE.SERVICES:
            case GIFT_TYPE.UNKNOWN:
            default:
              _bln_need_cashier_session = false;
              break;
          }

          // SSC 08-JUN-2012: Can't request any gift if the Cashier is closed.
          if (_bln_need_cashier_session && !WSI.Common.Cashier.IsSessionOpen(Cashier.SessionId, _db_trx.SqlTransaction))
          {
            return false;
          }

          _cashier_session = Cashier.CashierSessionInfo();
          _ac_mov_table = new AccountMovementsTable(_cashier_session);
          _cm_mov_table = new CashierMovementsTable(_cashier_session);

          Ticket _tito_ticket;
          if (!CommonRequest(_cashier_session.CashierSessionId, _ac_mov_table, _cm_mov_table, TerminalTypes.UNKNOWN, _db_trx.SqlTransaction, out _voucher_list, out Msg, out _tito_ticket))
          {
            return false;
          }

          if (BusinessLogic.IsModeTITO && (m_gift_cashier.Type == GIFT_TYPE.NOT_REDEEMABLE_CREDIT || m_gift_cashier.Type == GIFT_TYPE.REDEEMABLE_CREDIT))
          {
            if (!TitoPrinter.IsReady())
            {
              TitoPrinter.GetStatus(out _printer_status, out _printer_status_text);
              Log.Error("Ticket printer error. Printer Status: " + _printer_status_text);

              Msg = GIFT_INSTANCE_MSG.PRINTER_IS_NOT_READY;

              return false;
            }

            _operation_params = new ParamsTicketOperation();
            _operation_params.in_account = this.RelatedCard;
            _operation_params.out_cash_amount = this.NotRedeemableCredits;
            _operation_params.out_promotion_id = m_gift_cashier.PromotionId;
            _operation_params.out_operation_id = this.RequestOperationId;
            _operation_params.session_info = _cashier_session;
            _operation_params.out_account_promo_id = m_promotion_unique_id;

            // DHA 18-AGU-16: 
            _operation_params.in_cash_amt_0 = this.NotRedeemableCredits;
            _operation_params.in_cash_cur_0 = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode");

            if (m_gift_cashier.Type == GIFT_TYPE.REDEEMABLE_CREDIT)
            {
              if (GeneralParam.GetBoolean("Gifts.RedeemableCredits", "AsCashIn"))
              {
                _operation_params.ticket_type = TITO_TICKET_TYPE.CASHABLE;
              }
              else
              {
                _operation_params.ticket_type = TITO_TICKET_TYPE.PROMO_REDEEM;
              }
            }
            else if (m_gift_cashier.Type == GIFT_TYPE.NOT_REDEEMABLE_CREDIT)
            {
              _operation_params.ticket_type = TITO_TICKET_TYPE.PROMO_NONREDEEM;
            }
            else
            {
              Msg = GIFT_INSTANCE_MSG.DELIVERY_ERROR;

              return false;
            }

            if (!BusinessLogic.CreateTicket(_operation_params, true, out _ticket_promo, _db_trx.SqlTransaction))
            {
              return false;
            }
          }

          _db_trx.Commit();
        }

        if (_ticket_promo != null)
        {
          if (!TitoPrinter.PrintTicket(_ticket_promo.ValidationNumber, _ticket_promo.MachineTicketNumber, _ticket_promo.TicketType, _ticket_promo.Amount, _ticket_promo.CreatedDateTime, _ticket_promo.ExpirationDateTime, _cashier_session))
          {
            TitoPrinter.GetStatus(out _printer_status, out _printer_status_text);

            Log.Error("Gif Request: PrintTicket. Error on printing ticket. Printer Status: " + _printer_status_text);

            Msg = GIFT_INSTANCE_MSG.PRINTER_PRINT_FAIL;

            using (DB_TRX _trx = new DB_TRX())
            {
              this.DB_ReadInstaceAndGift(this.InstanceId, _trx.SqlTransaction);

              if (BusinessLogic.UndoGift(this.RelatedCard, this.RequestOperationId, OperationCode.GIFT_DELIVERY, _trx.SqlTransaction))
              {
                Msg = GIFT_INSTANCE_MSG.PRINTER_IS_NOT_READY;

                _trx.Commit();
              }
            }

            return false;
          }
        }

        VoucherPrint.Print(_voucher_list);
      }
      catch (Exception _ex)
      {
        // TODO: Show message indicating that the print failed but request has been submitted 
        Log.Exception(_ex);

        return false;
      }

      return true;
    } // Request

    //------------------------------------------------------------------------------
    // PURPOSE : Retrieves the selected gift data from GIFT_INSTANCES table
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - true: data was retrieved successfully
    //      - false: data was not retrieved
    // 
    //   NOTES :
    //
    public Boolean Delivery()
    {
      SqlConnection _sql_conn;
      SqlTransaction _sql_trx;
      SqlCommand _sql_cmd_gift;
      SqlCommand _sql_cmd_gin;
      String _sql_query;
      Int32 _num_rows_updated;
      Int64 _operation_id;
      //Int64             _movement_id;
      ArrayList _voucher_list;
      Boolean _error = true;
      MovementType _mov_type;

      AccountMovementsTable _account_mov_table;

      _sql_conn = null;
      _sql_trx = null;

      try
      {
        // Check if is not be expired or canceled

        if (m_status == GIFT_REQUEST_STATUS.CANCELED)
        {
          return false;
        }

        if (ExpirationDateTime <= WGDB.Now)
        {
          return false;
        }

        // Get Sql Connection 
        _sql_conn = WGDB.Connection();
        _sql_trx = _sql_conn.BeginTransaction();

        // Steps
        //    - Operation : GIFT_DELIVERY 
        //    - Card movement: Gift Delivery (OBJECT) | Gift Services (SERVICES)
        //    - Decrease gift stock (Gift = OBJECT)
        //    - Decrease gift request counter (Gift = OBJECT | SERVICES)
        //    - Increase gift delivery counter (Gift = OBJECT | SERVICES)
        //    - Update Gift Instace as delivered
        //    - Generate the gift delivery voucher

        //    - Operation : GIFT_DELIVERY 
        if (!Operations.DB_InsertOperation(OperationCode.GIFT_DELIVERY,
                                              m_account_id,           // AccountId
                                              Cashier.SessionId,      // CashierSessionId
                                              0,                      // MbAccountId
                                              SpentPoints,            // Amount = POINTS
                                              0,                      // PromotionId
                                              NotRedeemableCredits,   // NonRedeemable
                                              NotRedeemableCredits,   // NonRedeemableWonLock
                                              0,                      // OperationData
                                              0, string.Empty,
                                              out _operation_id,      // OperationId
                                              _sql_trx))
        {
          string _message;

          _message = "Gift.Delivery. DB_InsertOperation Operation Code: " + OperationCode.GIFT_DELIVERY.ToString()
                                                          + ", Card Id: " + m_account_id.ToString()
                                                          + ", Gift: " + m_gift_id.ToString() + "-" + m_gift_cashier.GiftName
                                                          + ", Points: " + SpentPoints.ToString(Gift.FORMAT_POINTS)
                                                                          ;
          throw new Exception(_message);
        }

        switch (m_gift_cashier.Type)
        {
          case GIFT_TYPE.OBJECT:
            _mov_type = MovementType.PointsGiftDelivery;

            // Prepare SQL Query for Gift = OBJECT
            //    - Decrease gift stock 
            //    - Decrease gift request counter 
            //    - Increase gift delivery counter 
            _sql_query = "UPDATE   GIFTS  " +
                         "   SET   GI_CURRENT_STOCK    = CASE WHEN (GI_CURRENT_STOCK > 0) THEN GI_CURRENT_STOCK - 1 ELSE 0 END      " +
                         "       , GI_REQUEST_COUNTER  = CASE WHEN (GI_REQUEST_COUNTER > 0) THEN GI_REQUEST_COUNTER - 1 ELSE 0 END  " +
                         "       , GI_DELIVERY_COUNTER = GI_DELIVERY_COUNTER + 1                                                    " +
                         " WHERE   GI_GIFT_ID = @GiftId ";

            break;

          case GIFT_TYPE.SERVICES:
            _mov_type = MovementType.PointsGiftServices;

            // Prepare SQL Query for Gift = SERVICES
            //    - Decrease gift request counter 
            //    - Increase gift delivery counter 
            _sql_query = "UPDATE   GIFTS " +
                         "   SET   GI_REQUEST_COUNTER  = CASE WHEN (GI_REQUEST_COUNTER > 0) THEN GI_REQUEST_COUNTER - 1 ELSE 0 END " +
                         "       , GI_DELIVERY_COUNTER = GI_DELIVERY_COUNTER + 1                                                   " +
                         " WHERE   GI_GIFT_ID = @GiftId ";
            break;

          default:
            _mov_type = MovementType.PointsGiftDelivery;
            _sql_query = "";
            break;
        }

        // DHA 04-APR-2013: Only the fields INITIAL and FINAL have the same points because is a delivery, fields ADD and SUB are 0.
        //    - Card movement: Gift Delivery (OBJECT) | Gift Services (SERVICES)
        _account_mov_table = new AccountMovementsTable(Cashier.CashierSessionInfo());

        _account_mov_table.Add(_operation_id, m_account_id, _mov_type,
                               (Decimal)this.RelatedCard.PlayerTracking.CurrentPoints,
                               0,
                               0,
                               (Decimal)this.RelatedCard.PlayerTracking.CurrentPoints);

        if (!_account_mov_table.Save(_sql_trx))
        {
          string _message;

          _message = "Gift.Delivery. Account Movement not saved. Gift Instance Id: " + InstanceId.ToString()
                                                                        + ", Gift: " + m_gift_id.ToString() + "-" + m_gift_cashier.GiftName;

          throw new Exception(_message);
        }

        //    - Decrease gift stock (Gift = OBJECT)
        //    - Decrease gift request counter (Gift = OBJECT | SERVICES)
        //    - Increase gift delivery counter (Gift = OBJECT | SERVICES)
        if (m_gift_cashier.Type == GIFT_TYPE.OBJECT || m_gift_cashier.Type == GIFT_TYPE.SERVICES)
        {
          _sql_cmd_gift = new SqlCommand(_sql_query);
          _sql_cmd_gift.Connection = _sql_trx.Connection;
          _sql_cmd_gift.Transaction = _sql_trx;

          _sql_cmd_gift.Parameters.Add("@GiftId", SqlDbType.BigInt).Value = m_gift_id;
          _num_rows_updated = _sql_cmd_gift.ExecuteNonQuery();

          // Ignore update error: the gift may have been removed from the catalog but the delivery must go on.
          if (_num_rows_updated != 1)
          {
            string _message;

            _message = "Gift.Delivery. GIFT's counters NOT updated. Gift Instance Id: " + InstanceId.ToString()
                                                                           + ", Gift: " + m_gift_id.ToString() + "-" + m_gift_cashier.GiftName;

            Log.Error(_message);
          }
        }

        //    - Update Gift Instace as delivered
        _sql_query = "UPDATE   GIFT_INSTANCES                              " +
                     "   SET   GIN_OPER_DELIVERY_ID = @OperationDeliveryId " +
                     "       , GIN_DELIVERED        = GETDATE()            " +
                     "       , GIN_REQUEST_STATUS   = @RequestStatus       " +
                     " WHERE   GIN_GIFT_INSTANCE_ID = @GiftInstanceId      " +
                     "   AND   GIN_OPER_DELIVERY_ID IS NULL                ";

        _sql_cmd_gin = new SqlCommand(_sql_query);
        _sql_cmd_gin.Connection = _sql_trx.Connection;
        _sql_cmd_gin.Transaction = _sql_trx;

        _sql_cmd_gin.Parameters.Add("@OperationDeliveryId", SqlDbType.BigInt).Value = _operation_id;
        _sql_cmd_gin.Parameters.Add("@GiftInstanceId", SqlDbType.BigInt).Value = InstanceId;
        _sql_cmd_gin.Parameters.Add("@RequestStatus", SqlDbType.Int).Value = GIFT_REQUEST_STATUS.DELIVERED;

        _num_rows_updated = _sql_cmd_gin.ExecuteNonQuery();

        if (_num_rows_updated != 1)
        {
          string _message;

          _message = "Gift.Delivery. GIFT_INSTANCES not updated. Gift Instance Id: " + InstanceId.ToString()
                                                                        + ", Gift: " + m_gift_id.ToString() + "-" + m_gift_cashier.GiftName;

          throw new Exception(_message);
        }

        //    - Generate the gift delivery voucher
        _voucher_list = VoucherBuilder.GiftDelivery(_operation_id,
                                                     RelatedCard.VoucherAccountInfo(),    // Account Info 
                                                     m_voucher_number.ExternalCode,       // Gift Number
                                                     m_gift_cashier.GiftName,             // Gift Name
                                                     PrintMode.Print,
                                                     _sql_trx);

        // Save the transaction
        _sql_trx.Commit();

        // Print the voucher
        VoucherPrint.Print(_voucher_list);

        _error = false;

        return true;
      }

      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }

      finally
      {
        if (_sql_trx != null)
        {
          if (_error && _sql_trx.Connection != null)
          {
            _sql_trx.Rollback();
          }

          _sql_trx.Dispose();
          _sql_trx = null;
        }
        if (_sql_conn != null)
        {
          _sql_conn.Close();
          _sql_conn = null;
        }
      }

    } // Delivery

    //------------------------------------------------------------------------------
    // PURPOSE : Function to manage all the messages related to Gift Instance
    //
    //  PARAMS :
    //      - INPUT :
    //          - MsgType
    //          - ParentForm
    //
    //      - OUTPUT :
    //
    // RETURNS :
    // 
    //   NOTES :
    //
    public bool Message(GIFT_INSTANCE_MSG MsgType, Form ParentForm)
    {
      DialogResult _msbox_answer;
      string _message;
      string _caption;
      MessageBoxIcon _icon;

      _message = "";
      _caption = "";

      if (!GetMessage(MsgType, out _message, out _caption, out _icon))
      {
        return false;
      }

      _msbox_answer = frm_message.Show(_message,
                                       _caption,
                                       MessageBoxButtons.OK,
                                       _icon,
                                       ParentForm);

      return true;
    } // Message

  } // Class GiftInstance

  #endregion Public Classes

}