//------------------------------------------------------------------------------
// Copyright � 2009 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_browse_mobile_banks.cs
// 
//   DESCRIPTION: Implements the following classes:
//                1. frm_browse_mobile_banks
//
//        AUTHOR: APB
// 
// CREATION DATE: 15-JUL-2009
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 15-JUL-2009 APB     First release.
// 05-NOV-2015 SMN     Change designer
// 14-MAR-2016 FOS     Fixed Bug 9357: New design corrections
// 18-MAR-2016 ETP     Fixed Bug 10711: Correct design of grid with en-US config and MobileBank.RegisterShortfallOnDeposit enabled.
//------------------------------------------------------------------------------
using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WSI.Common;

namespace WSI.Cashier
{
  public partial class frm_browse_mobile_banks : WSI.Cashier.Controls.frm_base
  {
    #region Constants
    private const Int32 GRID_MAX_COLUMNS = 6;

    private const Int32 GRID_COLUMN_MB_ID = 0;
    private const Int32 GRID_COLUMN_MB_TRACKDATA = 1;
    private const Int32 GRID_COLUMN_MB_ACCT_TYPE = 2;
    private const Int32 GRID_COLUMN_MB_HOLDER_NAME = 3;
    private const Int32 GRID_COLUMN_MB_PENDING_CASH = 4;
    private const Int32 GRID_COLUMN_MB_CASH_OVER = 5;
    private const Int32 GRID_COLUMN_MB_SHORTFALL = 6;

    private const String TRACKDATA_ERROR = " * * * * * ";
    #endregion

    #region Attributes

    private frm_yesno form_yes_no;
    private String m_external_track_data;
    Boolean m_gp_pending_cash_partial;

    #endregion

    #region Constructor

    /// <summary>
    /// Constructor method.
    /// 1. Initialize controls.
    /// 2. Initialize control�s resources. (NLS, Images, etc)
    /// </summary>
    public frm_browse_mobile_banks()
    {
      InitializeComponent();

      InitializeControlResources();

      m_gp_pending_cash_partial = MobileBank.GP_GetRegisterShortfallOnDeposit();

      RestoreInitialFormWidth();

      form_yes_no = new frm_yesno();
      form_yes_no.Opacity = 0.6;
    }

    #endregion

    #region Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Restore initial values for form and grid width.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //      Restoring these values must be done because every call to this form
    //  adds a few pixels to its width because of the vertical scroll bar
    //
    private void RestoreInitialFormWidth()
    {
      if (!m_gp_pending_cash_partial)
      {
        // ORIGINAL FORM WIDTH VALUES
        this.Width = 687;
        this.dataGridView1.Width = 670;
      }
      else
      {
        this.Width = 835;
        this.dataGridView1.Width = 817;
      }
    } // RestoreInitialFormWidth

    /// <summary>
    /// Get SQL for mobile banks data
    /// </summary>
    /// <returns></returns>
    private String GetSQLMobileBanks()
    {
      String _fields;

      _fields = "MB_ACCOUNT_ID, MB_TRACK_DATA, MB_ACCOUNT_TYPE, MB_HOLDER_NAME, MB_PENDING_CASH, MB_OVER_CASH";
      if (MobileBank.GP_GetRegisterShortfallOnDeposit())
      {
        _fields = _fields + " , MB_SHORTFALL_CASH";
      }

      StringBuilder _sb;
      _sb = new StringBuilder();
      _sb.AppendLine("   SELECT   " + _fields);
      _sb.AppendLine("     FROM   MOBILE_BANKS                                ");
      _sb.AppendLine("    WHERE   MB_CASHIER_SESSION_ID  = @pCashierSession   ");
      _sb.AppendLine(" ORDER BY   MB_PENDING_CASH  DESC, MB_ACCOUNT_ID        ");

      return _sb.ToString();
    }

    /// <summary>
    /// Get mobile banks data and refresh grid
    /// </summary>
    /// <returns></returns>
    private Boolean GetMobileBanksData()
    {
      if (Cashier.SessionId == 0)
      {
        return false;
      }

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(GetSQLMobileBanks(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pCashierSession", SqlDbType.BigInt).Value = Cashier.SessionId;

            using (DataTable _dt_mobile_bank = new DataTable())
            {
              _dt_mobile_bank.Load(_cmd.ExecuteReader());

              dataGridView1.DataSource = _dt_mobile_bank;
            }
          }
        }

        return true;
      }
      catch (Exception ex)
      {
        Log.Error(ex.Message);
      }

      return false;
    }

    /// <summary>
    /// Set grid columns header, aligment, width
    /// </summary>
    private void SetGridColumns()
    {
      dataGridView1.Width += System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;

      dataGridView1.MultiSelect = false;
      dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;

      // Columns
      dataGridView1.Columns[GRID_COLUMN_MB_ID].Width = 120;
      dataGridView1.Columns[GRID_COLUMN_MB_ID].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

      dataGridView1.Columns[GRID_COLUMN_MB_TRACKDATA].Width = 180;
      dataGridView1.Columns[GRID_COLUMN_MB_TRACKDATA].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

      dataGridView1.Columns[GRID_COLUMN_MB_ACCT_TYPE].Width = 0;
      dataGridView1.Columns[GRID_COLUMN_MB_ACCT_TYPE].Visible = false;

      dataGridView1.Columns[GRID_COLUMN_MB_HOLDER_NAME].Width = 170;
      dataGridView1.Columns[GRID_COLUMN_MB_HOLDER_NAME].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

      dataGridView1.Columns[GRID_COLUMN_MB_PENDING_CASH].Width = 100;
      dataGridView1.Columns[GRID_COLUMN_MB_PENDING_CASH].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
      dataGridView1.Columns[GRID_COLUMN_MB_PENDING_CASH].DefaultCellStyle.Format = "#,##0.00";

      dataGridView1.Columns[GRID_COLUMN_MB_CASH_OVER].Width = 115;
      dataGridView1.Columns[GRID_COLUMN_MB_CASH_OVER].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
      dataGridView1.Columns[GRID_COLUMN_MB_CASH_OVER].DefaultCellStyle.Format = "#,##0.00";

      if (m_gp_pending_cash_partial)
      {
        dataGridView1.Columns[GRID_COLUMN_MB_SHORTFALL].Width = 148;
        dataGridView1.Columns[GRID_COLUMN_MB_SHORTFALL].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        dataGridView1.Columns[GRID_COLUMN_MB_SHORTFALL].DefaultCellStyle.Format = "#,##0.00";
      }

      // Datagrid Headers
      dataGridView1.Columns[GRID_COLUMN_MB_ID].HeaderCell.Value = Resource.String("STR_FRM_BROWSE_MB_COLUMN_ACCOUNT").ToUpper();
      dataGridView1.Columns[GRID_COLUMN_MB_TRACKDATA].HeaderCell.Value = Resource.String("STR_FRM_BROWSE_MB_COLUMN_TRACKDATA").ToUpper();
      dataGridView1.Columns[GRID_COLUMN_MB_ACCT_TYPE].HeaderCell.Value = "AcctType"; // Not Visible      
      dataGridView1.Columns[GRID_COLUMN_MB_HOLDER_NAME].HeaderCell.Value = Resource.String("STR_FRM_BROWSE_MB_COLUMN_HOLDER_NAME").ToUpper();
      dataGridView1.Columns[GRID_COLUMN_MB_PENDING_CASH].HeaderCell.Value = Resource.String("STR_FRM_BROWSE_MB_COLUMN_PENDING_CASH").ToUpper();
      dataGridView1.Columns[GRID_COLUMN_MB_CASH_OVER].HeaderCell.Value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_DIFF_EXTRA").ToUpper();

      if (m_gp_pending_cash_partial)
      {
        dataGridView1.Columns[GRID_COLUMN_MB_SHORTFALL].HeaderCell.Value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_DIFF_MISSING").ToUpper();
      }
    }

    /// <summary>
    /// Get external trackdata from selected row
    /// </summary>
    /// <returns></returns>
    private String GetSelectedExternalTrackdata()
    {
      String _external_track_data;

      _external_track_data = String.Empty;

      if (this.dataGridView1.RowCount > 0)
      {
        if (this.dataGridView1.CurrentRow.Cells[GRID_COLUMN_MB_TRACKDATA].Value != DBNull.Value)
        {
          _external_track_data = ((String)this.dataGridView1.CurrentRow.Cells[GRID_COLUMN_MB_TRACKDATA].Value);

          if ((String.IsNullOrEmpty(_external_track_data) || (String.Equals(_external_track_data, TRACKDATA_ERROR))))
          {
            _external_track_data = String.Empty;
          }
        }
      }

      return _external_track_data;
    }
    #endregion

    #region Public Methods
    //------------------------------------------------------------------------------
    // PURPOSE : Initialize control resources (NLS strings, images, etc).
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //

    public void InitializeControlResources()
    {
      //NLS Strings:

      //Title:
      this.Text = Resource.String("STR_FRM_BROWSE_MB_TITLE");
      this.FormTitle = this.Text.ToUpper();
      this.ShowCloseButton = false;

      //Buttons
      btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");
      btn_select.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_SELECT");
    } // InitializeControlResources

    //------------------------------------------------------------------------------
    // PURPOSE : Init and Show the form.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public String SelectMobileBank()
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_browse_mobile_banks", Log.Type.Message);

      Int32 idx_row;
      String internal_card_id;
      string encrypted_trackdata;
      int card_type;
      bool rc;

      if (!GetMobileBanksData())
      {
        return String.Empty;
      }

      this.InitializeControlResources();

      this.Width += System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;

      //Set grid columns header, aligment, width
      SetGridColumns();

      for (idx_row = 0; idx_row < dataGridView1.Rows.Count; idx_row++)
      {
        if ((Int16)dataGridView1.Rows[idx_row].Cells[GRID_COLUMN_MB_ACCT_TYPE].Value == 0)
        {
          dataGridView1.Rows[idx_row].Cells[GRID_COLUMN_MB_HOLDER_NAME].Value = DBNull.Value;
        }

        if ((String)dataGridView1.Rows[idx_row].Cells[GRID_COLUMN_MB_TRACKDATA].Value != "")
        {
          internal_card_id = (String)dataGridView1.Rows[idx_row].Cells[GRID_COLUMN_MB_TRACKDATA].Value;
          card_type = 0;
          rc = CardNumber.TrackDataToExternal(out encrypted_trackdata, internal_card_id, card_type);
          if (rc)
          {
            dataGridView1.Rows[idx_row].Cells[GRID_COLUMN_MB_TRACKDATA].Value = encrypted_trackdata;
          }
          else
          {
            dataGridView1.Rows[idx_row].Cells[GRID_COLUMN_MB_TRACKDATA].Value = TRACKDATA_ERROR;
          }
        }
      } // for

      dataGridView1.Focus();

      // Avoid selection from an empty grid
      btn_select.Enabled = (dataGridView1.RowCount > 0);

      form_yes_no.Show();
      this.ShowDialog();
      form_yes_no.Hide();

      RestoreInitialFormWidth();

      return m_external_track_data;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Dispose yes_no form.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    new public void Dispose()
    {
      form_yes_no.Dispose();
      form_yes_no = null;

      base.Dispose();
    }
    #endregion Public methods

    #region DataGridEvents
    //------------------------------------------------------------------------------
    // PURPOSE : Handle the double click on a datagrid row.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //      This method is used to allow record selection by double-clicking on the grid
    //
    private void dataGridView1_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
    {
      m_external_track_data = GetSelectedExternalTrackdata();

      Misc.WriteLog("[FORM CLOSE] frm_browse_mobile_banks (cellmousedoubleclick)", Log.Type.Message);
      this.Close();
    } // dataGridView1_CellMouseDoubleClick

    //------------------------------------------------------------------------------
    // PURPOSE : Paint a datagrid row before actually filling it.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void dataGridView1_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
    {
      // Highlight mobile banks with pending amounts to be cashed
      if ((Decimal)dataGridView1.Rows[e.RowIndex].Cells[GRID_COLUMN_MB_PENDING_CASH].Value > 0)
      {
        dataGridView1.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.NavajoWhite;
      }
    } // dataGridView1_RowPrePaint

    #endregion DataGridEvents

    #region Buttons

    //------------------------------------------------------------------------------
    // PURPOSE : Handle the click on the Cancel button (exit form).
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void btn_cancel_Click(object sender, EventArgs e)
    {
      m_external_track_data = String.Empty;

      Misc.WriteLog("[FORM CLOSE] frm_browse_mobile_banks (cancel)", Log.Type.Message);
      this.Close();
    } // btn_cancel_Click

    //------------------------------------------------------------------------------
    // PURPOSE : Handle the click on the Select button (exit form and pass the
    //           selected record identifier to the caller).
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void btn_select_Click(object sender, EventArgs e)
    {
      m_external_track_data = GetSelectedExternalTrackdata();

      Misc.WriteLog("[FORM CLOSE] frm_browse_mobile_banks (select)", Log.Type.Message);
      this.Close();
    } // btn_select_Click

    //------------------------------------------------------------------------------
    // PURPOSE: Change a button appearance depending on its status (enabled/disabled).
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    private void buttons_EnabledChanged(object sender, EventArgs e)
    {
      System.Windows.Forms.Button a;

      a = (System.Windows.Forms.Button)sender;

      if (a.Enabled)
      {
        a.BackColor = System.Drawing.Color.Transparent;
      }
      else
      {
        a.BackColor = System.Drawing.Color.Transparent;
      }
    } // buttons_EnabledChanged

    #endregion Buttons
  }
}