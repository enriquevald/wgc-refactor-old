namespace WSI.Cashier
{
  partial class frm_sub_amount_confirm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.btn_ok = new System.Windows.Forms.Button();
      this.btn_cancel = new System.Windows.Forms.Button();
      this.uc_card_balance1 = new WSI.Cashier.uc_card_balance();
      this.SuspendLayout();
      // 
      // btn_ok
      // 
      this.btn_ok.BackColor = System.Drawing.Color.LightSkyBlue;
      this.btn_ok.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btn_ok.Location = new System.Drawing.Point(18, 199);
      this.btn_ok.Name = "btn_ok";
      this.btn_ok.Size = new System.Drawing.Size(112, 48);
      this.btn_ok.TabIndex = 1;
      this.btn_ok.Text = "xOk";
      this.btn_ok.UseVisualStyleBackColor = false;
      // 
      // btn_cancel
      // 
      this.btn_cancel.BackColor = System.Drawing.Color.LightSkyBlue;
      this.btn_cancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btn_cancel.Location = new System.Drawing.Point(159, 200);
      this.btn_cancel.Name = "btn_cancel";
      this.btn_cancel.Size = new System.Drawing.Size(112, 48);
      this.btn_cancel.TabIndex = 2;
      this.btn_cancel.Text = "xCancel";
      this.btn_cancel.UseVisualStyleBackColor = false;
      // 
      // uc_card_balance1
      // 
      this.uc_card_balance1.Location = new System.Drawing.Point(24, 53);
      this.uc_card_balance1.Name = "uc_card_balance1";
      this.uc_card_balance1.Size = new System.Drawing.Size(240, 116);
      this.uc_card_balance1.TabIndex = 0;
      // 
      // frm_sub_amount_confirm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(292, 273);
      this.Controls.Add(this.btn_cancel);
      this.Controls.Add(this.btn_ok);
      this.Controls.Add(this.uc_card_balance1);
      this.Name = "frm_sub_amount_confirm";
      this.Text = "frm_sub_amount_confirm";
      this.ResumeLayout(false);

    }

    #endregion

    private uc_card_balance uc_card_balance1;
    private System.Windows.Forms.Button btn_ok;
    private System.Windows.Forms.Button btn_cancel;
  }
}