//------------------------------------------------------------------------------
// Copyright © 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_card_balance.cs
// 
//   DESCRIPTION: User control to display the card balance.
// 
//        AUTHOR: Ronald Rodríguez.
// 
// CREATION DATE: 10-AUG-2007
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 10-AUG-2007 RRT    Inital Draft.
// 26-JUL-2012 RCI    TotalRedeem button must be enabled only when there is something to pay to the client.
// 21-MAY-2013 LEM    Fixed Bug #791: Money (pb_money) image not shown correctly 
// 07-ENE-2014 LJM    Changes to show/hide some of the information.
// 20-APR-2016 EOR    Product Backlog Item 11296: Codere - Custody Tax: Implement Devolution Tax Custody
// 04-NOV-2015 AVZ    Backlog Item 6145:BonoPLUS: Reserved credit - Task 6153 y 6154: BonoPLUS Reserved - Added reserved data.
// 11-NOV-2015 FOS    Product Backlog Item: 3709 Payment tickets & handpays
// 23-NOV-2015 FOS    Bug 6771:Payment tickets & handpays
// 11-DIC-2015 JCA & FJC  BackLog Item 7311: Bonoplay=> Mixed Mode.
// 21-APR-2016 ETP   Fixed bug 11743: Mostrar buckets de credito redimible & no redimible en el cajero.
// 19-MAY-2016 ETP   Fixed bug 13344: Cajero: En la pantalla de Reintegro salen los conceptos solapados
// 10-JUN-2016 FAV   Fixed bug 14236: 'retenciones' always with value 0.
// 04-OCT-2016 LTC   Product Backlog Item 17448:Televisa - Cash desk draw (Cash) - Voucher
// 14-MAR-2017 FAV   PBI 25268:Third TAX - Payment Voucher
// 15-MAR-2017 ETP   WIGOS- 109: Add marker information to cashier forms.
// 11-DIC-2015 JCA & 
//             FJC    BackLog Item 7311: Bonoplay=> Mixed Mode.
// 21-APR-2016 ETP    Fixed bug 11743: Mostrar buckets de credito redimible & no redimible en el cajero.
// 19-MAY-2016 ETP    Fixed bug 13344: Cajero: En la pantalla de Reintegro salen los conceptos solapados
// 10-JUN-2016 FAV    Fixed bug 14236: 'retenciones' always with value 0.
// 04-OCT-2016 LTC    Product Backlog Item 17448:Televisa - Cash desk draw (Cash) - Voucher
// 22-JUN-2017 FJC    WIGOS-3142 WS2S - Set / Retrieve Reserved Credit on the Cashier
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using WSI.Common;
using WSI.Common.CreditLines;

namespace WSI.Cashier
{
  public partial class uc_card_balance : UserControl
  {
    #region Attributes

    Currency m_cashable = 0;
    Currency m_total_paid = 0;
    Boolean m_partial_redeem_allowed = true;
    Currency m_credit_line_spent = 0;

    #endregion

    #region Properties

    public Boolean PartialRedeemAllowed
    {
      get
      {
        return m_partial_redeem_allowed;
      }
    }

    public Currency Cashable
    {
      get
      {
        return m_cashable;
      }
    }

    public Currency TotalPaid
    {
      get
      {
        return m_total_paid;
      }
    }

    public Currency CreditLineSpent
    {
      get
      {
        return m_credit_line_spent;
      }
    }

    #endregion

    #region Constructor

    /// <summary>
    /// Control constructor.
    /// </summary>
    public uc_card_balance()
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[UC LOAD] uc_balance", Log.Type.Message);

      InitializeComponent();

      InitializeControlResources();

      FillCardCountersBalance(new CardData());
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Initialize control resources (NLS strings, images, etc)
    /// </summary>
    public void InitializeControlResources()
    {
      // - NLS Strings:
      //   - Title:

      //   - Labels
      //lbl_promo.Text = Resource.String("STR_UC_CARD_BALANCE_PROMO_SHORT") + ":";      APB 15-JUN-2010 
      lbl_balance_name_1.Text = Resource.String("STR_UC_CARD_BALANCE_BALANCE_NAME") + ":";
      lbl_initial_cash_in_name.Text = Resource.String("STR_UC_CARD_BALANCE_INITIAL_CASH_IN") + ":";
      lbl_redeemable.Text = Resource.String("STR_UC_CARD_BALANCE_REEDEMABLE") + ":";
      lbl_promo_redeemable_name.Text = Resource.String("STR_UC_CARD_BALANCE_PROMOTION_REDEEMABLE") + ":";
      lbl_promo_not_redeemable_name.Text = Resource.String("STR_UC_CARD_BALANCE_PROMOTION_NON_REDEEMABLE") + ":";
      lbl_summary_not_redeemable_name.Text = Resource.String("STR_UC_CARD_BALANCE_NOT_REDEEMABLE") + ":";

      lbl_cash_in_name_2.Text = Resource.String("STR_UC_CARD_BALANCE_CASH_IN") + ":";
      lbl_won_name_2.Text = Resource.String("STR_UC_CARD_BALANCE_WON") + ":";

      lbl_aux1_name.Text = "";
      lbl_aux2_name.Text = "";

      lbl_casheable_name.Text = Resource.String("STR_UC_CARD_BALANCE_CASHEABLE") + ":";

      lbl_nr_withhold_name.Text = Resource.String("STR_UC_CARD_BALANCE_LOCKED") + ":";
      lbl_nr_won_lock_name.Text = Resource.String("STR_UC_CARD_BALANCE_NR_WON_LOCK") + ":";

      if (Misc.IsGameGatewayEnabled())
      {
      lbl_reserved.Text = GeneralParam.GetString("GameGateway", "Name", "BonoPlay") + ":";
      }
      else
      {
        if (Misc.IsWS2SFBMEnabled())
        {
          lbl_reserved.Text = GeneralParam.GetString("WS2S", "TextReserved", String.Empty) + ":";
        }
      }

      lbl_credit_line_balance.Text = Resource.String("STR_UC_CREDIT_LINE_AVAILABLE") + ":";

      if (!GeneralParam.GetBoolean("CreditLine", "Enabled", false))
      {
        lbl_credit_line_balance.Visible = false;
        lbl_credit_line_balance_value.Visible = false;
      }

      BucketsList _aux_list = new BucketsList();
      _aux_list.GetList();

      lbl_bucket_nr.Text = Resource.String("STR_BUCKET_TITTLE") + " " + _aux_list.GetBucketName((Int32)Buckets.BucketId.Credit_NR) + ":";
      lbl_bucket_re.Text = Resource.String("STR_BUCKET_TITTLE") + " " + _aux_list.GetBucketName((Int32)Buckets.BucketId.Credit_RE) + ":";
      lbl_bucket_nr.Visible = false;
      lbl_bucket_nr_value.Visible = false;

      lbl_bucket_re.Visible = false;
      lbl_bucket_re_value.Visible = false;

      pb_money.Image = Resources.ResourceImages.coins;

      this.Invalidate();

      //   - Buttons
      //btn_ok.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_OK");
      //btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");

      // - Images:

    } // InitializeControlResources

    /// <summary>
    /// Hides/Shows the labels
    /// </summary>
    /// <param name="Visible"></param>
    /// <param name="Height"></param>
    private void SetVisibility(Boolean Visible, int Height)
    {
      lbl_initial_cash_in_name.Visible = Visible;
      lbl_initial_cash_in_value.Visible = Visible;

      lbl_cash_in_name_2.Visible = Visible;
      lbl_cash_in_value_2.Visible = Visible;

      lbl_won_name_2.Visible = Visible;
      lbl_won_value_2.Visible = Visible;

      lbl_aux1_name.Visible = Visible;
      lbl_aux1_value.Visible = Visible;

      lbl_aux2_name.Visible = Visible;
      lbl_aux2_value.Visible = Visible;

      lbl_casheable_name.Visible = Visible;
      lbl_casheable_value.Visible = Visible;

      lbl_service_name.Visible = Visible;
      lbl_service_value.Visible = Visible;

      lbl_separator_2.Visible = Visible;

      //lbl_bucket_nr.Visible = Visible;
      //lbl_bucket_nr_value.Visible = Visible;

      //lbl_bucket_re.Visible = Visible;
      //lbl_bucket_re_value.Visible = Visible;

      this.Height = Height;
    }

    #endregion

    #region Public Methods

    public Boolean FillCardCountersAll(CardData CardData)
    {
      TYPE_SPLITS _splits;
      TYPE_CASH_REDEEM _cash_redeem;
      TYPE_CASH_REDEEM _a_redeem;
      TYPE_CASH_REDEEM _b_redeem;
      MultiPromos.PromoBalance _ini_balance;
      MultiPromos.PromoBalance _fin_balance;
      Currency _promo_re;
      Currency _promo_nr;
      Currency _bucket_promo_re;
      Currency _bucket_promo_nr;
      Currency _redeemable;
      Currency _reserved;
      Currency _neg_service_charge;
      Currency _neg_total_tax;
      Currency _credit_line_balance;
      CashAmount _cash;
      Boolean _show_reserved;
      Int32 _mode_reserved;
      CashierSessionInfo _cashier_session_info; // LTC 04-OCT-2016 
      Int64 OperationId;

      _cash = new CashAmount();
      _promo_nr = CardData.AccountBalance.PromoNotRedeemable;
      _promo_re = CardData.AccountBalance.PromoRedeemable;
      _bucket_promo_nr = (Currency)CardData.AccountBalance.BucketNRCredit;
      _bucket_promo_re = (Currency)CardData.AccountBalance.BucketRECredit;
      _redeemable = (Currency)CardData.AccountBalance.Redeemable;
      _reserved = (Currency)CardData.AccountBalance.Reserved;
      _credit_line_balance = (Currency)CardData.CreditLineData.SpentAmount;

      // Refresh card counters
      //lbl_balance_value_1.Text = (CardData.CurrentBalance - CardData.AccountBalance.BucketNRCredit - CardData.AccountBalance.BucketRECredit).ToString();
      lbl_balance_value_1.Text = ((Currency)(CardData.CurrentBalance)).ToString();
      lbl_initial_cash_in_value.Text = CardData.MaxDevolution.ToString();
      lbl_promo_not_redeemable_value.Text = _promo_nr.ToString();
      lbl_promo_redeemable_value.Text = _promo_re.ToString();
      lbl_nr_withhold_value.Text = CardData.Nr1Withhold.ToString();
      lbl_redeemable_value.Text = _redeemable.ToString(true);
      lbl_reserved_value.Text = _reserved.ToString(true);
      lbl_credit_line_balance_value.Text = _credit_line_balance.ToString(true);
      m_credit_line_spent = _credit_line_balance;

      lbl_bucket_nr_value.Text = _bucket_promo_nr.ToString(true);
      lbl_bucket_re_value.Text = _bucket_promo_re.ToString(true);

      // AMF 26-NOV-2015
      _mode_reserved = GeneralParam.GetInt32("GameGateway", "Reserved.Enabled", (Int32)MultiPromos.GAME_GATEWAY_RESERVED_MODE.GAME_GATEWAY_RESERVED_MODE_WITHOUT_BUCKETS);
      _show_reserved = (Misc.IsGameGatewayEnabled()
                      && (( (_mode_reserved == (Int32)(MultiPromos.GAME_GATEWAY_RESERVED_MODE.GAME_GATEWAY_RESERVED_MODE_WITH_BUCKETS) && CardData.AccountBalance.ModeReserved))
                         || (_mode_reserved == (Int32)(MultiPromos.GAME_GATEWAY_RESERVED_MODE.GAME_GATEWAY_RESERVED_MODE_MIXED)))
                      || Misc.IsWS2SFBMEnabled());

      lbl_reserved.Visible = _show_reserved;
      lbl_reserved_value.Visible = _show_reserved;

      if (!Split.ReadSplitParameters(out _splits))
      {
        return false;
      }

      _ini_balance = new MultiPromos.PromoBalance(CardData.AccountBalance, CardData.PlayerTracking.TruncatedPoints);


      // LTC 04-OCT-2016
      Tax.ServiceChageOnlyCancellable = false;
      //CHECK IF VOUCHER MODE == TPV
      if (Misc.IsVoucherModeTV() && GeneralParam.GetBoolean("Cashier.TotalRedeem", "RedeemWithoutPlay.Enabled", false))
      {
        //CHECK IF CHASH IN IS THE LAST MOVEMENT
        _cashier_session_info = Cashier.CashierSessionInfo();
        if (CashierBusinessLogic.IsLastMovementCashIn(CardData.AccountId, out OperationId, _cashier_session_info.CashierSessionId, OperationCode.CASH_IN))
        {
          Tax.ServiceChageOnlyCancellable = true;
        }
      }

      if (!_cash.CalculateCashAmount(CASH_MODE.TOTAL_REDEEM,
                                        _splits,
                                        _ini_balance,
                                        0,
                                        false,
                                        true,
                                        false,
                                        CardData,
                                        out _fin_balance,
                                        out _cash_redeem,
                                        out _a_redeem,
                                        out _b_redeem))
      {
        return false;
      }

      m_cashable = _cash_redeem.TotalRedeemAfterTaxes;  // Cashable amount
      // RCI 26-JUL-2012
      m_total_paid = _cash_redeem.TotalPaid;

      // AJQ 21-AUG-2012, Conditionally disable Partial Redeem 
      m_partial_redeem_allowed = !GeneralParam.GetBoolean("Cashier", "PartialRedeem.Disabled"); ;
      m_partial_redeem_allowed = m_partial_redeem_allowed && m_cashable > 0;          // and there is something to be redeemed

      if (m_partial_redeem_allowed && _cash_redeem.prize > 0)
      {
        Boolean _witholding_required;
        Boolean[] _document_required;

        if (!Witholding.CheckWitholding(_cash_redeem.prize, out _witholding_required, out _document_required))
        {
          return false;
        }
        m_partial_redeem_allowed = m_partial_redeem_allowed && !_witholding_required;   // When Withholding Documents are required, no partial is allowed

        if (_cash_redeem.TotalTaxes > 0)
        {
          DevolutionPrizeParts _parts;

          _parts = new DevolutionPrizeParts();

          m_partial_redeem_allowed = m_partial_redeem_allowed && !_parts.PrizeThresholdDefined;
        }
      }


      // Compute the values of:
      //  - NotRedeemable
      //  - CashIn 
      //  - CashWon 
      lbl_cash_in_value_2.Text = _cash_redeem.TotalDevolution.ToString();
      lbl_summary_not_redeemable_value.Text = _promo_nr.ToString();
      lbl_nr_won_lock_value.Text = CardData.NonRedeemableWonLock.ToString();
      lbl_won_value_2.Text = _cash_redeem.prize.ToString();

      // AJQ 30-AUG-2012: Tax Returning (shown as rounding)
      Currency _total_tax;
      _total_tax = _cash_redeem.tax1 + _cash_redeem.tax2 + _cash_redeem.tax3;

      _neg_total_tax = -_total_tax;

      lbl_aux1_name.Text = Resource.String("STR_UC_CARD_BALANCE_WITHHOLDING") + ":";
      lbl_aux1_value.Text = _neg_total_tax.ToString();
      lbl_aux1_value.ForeColor = Color.Red;

      lbl_aux2_name.Text = Resource.String("STR_UC_CARD_BALANCE_ROUNDING") + ":";
      lbl_aux2_value.Text = _cash_redeem.TotalRounding.ToString();
      lbl_aux2_value.ForeColor = (_cash_redeem.TotalRounding < 0 ? Color.Red : Color.Black);

      //EOR 20-APR-2016
      if (Misc.IsTaxCustodyEnabled())
      {
        lbl_tax_custody.Visible = true;
        lbl_tax_custody.Text = Misc.TaxCustodyRefundName() + ":";
        lbl_custody_value.Text = _cash_redeem.DevolutionCustody.ToString();
      }
      else
      {
        lbl_tax_custody.Visible = false;
        lbl_custody_value.Visible = false;
      }
      lbl_casheable_value.Text = _cash_redeem.TotalRedeemAfterTaxes.ToString();

      _neg_service_charge = -_cash_redeem.ServiceCharge;

      lbl_service_name.Text = Resource.String("STR_FRM_LAST_MOVEMENTS_SERVICE_CHARGE") + ":";
      lbl_service_value.Text = _neg_service_charge.ToString();

      SetVisibility(true, this.Height);
      return true;
    }

    /// <summary>
    /// Fill Card  counters from CardData.
    /// </summary>
    /// <param name="CardId">CardData.</param>
    public Boolean FillCardCountersBalance(CardData CardData)
    {
      Currency _promo_nr;
      Currency _promo_re;
      Currency _redeemable;
      Currency _reserved;
      Boolean _show_reserved;
      Int32 _mode_reserved;
      Currency _bucket_promo_re;
      Currency _bucket_promo_nr;
      Currency _credit_line_balance;

      _bucket_promo_nr = (Currency)CardData.AccountBalance.BucketNRCredit;
      _bucket_promo_re = (Currency)CardData.AccountBalance.BucketRECredit;

      _promo_nr = (Currency)CardData.AccountBalance.PromoNotRedeemable;
      _promo_re = CardData.AccountBalance.PromoRedeemable;

      _redeemable = (Currency)CardData.AccountBalance.Redeemable;
      _reserved = (Currency)CardData.AccountBalance.Reserved;
      _mode_reserved = (Int32)(MultiPromos.GAME_GATEWAY_RESERVED_MODE.GAME_GATEWAY_RESERVED_MODE_WITHOUT_BUCKETS);
      _credit_line_balance = (Currency)CardData.CreditLineData.AvailableAmountNoTTO;

      if (_credit_line_balance < 0)
      {
        _credit_line_balance = 0;
      }


      // Refresh card counters
      //lbl_balance_value_1.Text = (CardData.CurrentBalance + CardData.AccountBalance.BucketNRCredit + CardData.AccountBalance.BucketRECredit).ToString();
      lbl_balance_value_1.Text = ((Currency)(CardData.CurrentBalance)).ToString();
      lbl_promo_not_redeemable_value.Text = _promo_nr.ToString(true);
      lbl_promo_redeemable_value.Text = _promo_re.ToString();

      lbl_redeemable_value.Text = _redeemable.ToString(true);
      lbl_summary_not_redeemable_value.Text = _promo_nr.ToString(true);
      lbl_nr_withhold_value.Text = CardData.Nr1Withhold.ToString();
      lbl_nr_won_lock_value.Text = CardData.NonRedeemableWonLock.ToString();
      lbl_reserved_value.Text = _reserved.ToString(true);

      // AMF 26-NOV-2015
      _mode_reserved = GeneralParam.GetInt32("GameGateway", "Reserved.Enabled", (Int32)MultiPromos.GAME_GATEWAY_RESERVED_MODE.GAME_GATEWAY_RESERVED_MODE_WITHOUT_BUCKETS);

      _show_reserved = (Misc.IsGameGatewayEnabled()
                       && (((  _mode_reserved == (Int32)(MultiPromos.GAME_GATEWAY_RESERVED_MODE.GAME_GATEWAY_RESERVED_MODE_WITH_BUCKETS) && CardData.AccountBalance.ModeReserved))
                           || (_mode_reserved == (Int32)(MultiPromos.GAME_GATEWAY_RESERVED_MODE.GAME_GATEWAY_RESERVED_MODE_MIXED)))
                       || Misc.IsWS2SFBMEnabled());

      lbl_reserved.Visible = _show_reserved;
      lbl_reserved_value.Visible = _show_reserved;

      lbl_credit_line_balance_value.Text = _credit_line_balance.ToString(true);
      m_credit_line_spent = _credit_line_balance;

      // Hiding
      SetVisibility(false, 200);

      return true;
    }

    #endregion

  }
}
