﻿using WSI.Cashier.Controls;
namespace WSI.Cashier
{
  partial class frm_account_photo
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_account_photo));
      this.btn_cancel = new WSI.Cashier.Controls.uc_round_button();
      this.pb_user = new System.Windows.Forms.PictureBox();
      this.pnl_data.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pb_user)).BeginInit();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.Controls.Add(this.pb_user);
      this.pnl_data.Controls.Add(this.btn_cancel);
      this.pnl_data.Size = new System.Drawing.Size(871, 658);
      this.pnl_data.TabIndex = 0;
      // 
      // btn_cancel
      // 
      this.btn_cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
      this.btn_cancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btn_cancel.FlatAppearance.BorderSize = 0;
      this.btn_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_cancel.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_cancel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_cancel.Image = null;
      this.btn_cancel.IsSelected = false;
      this.btn_cancel.Location = new System.Drawing.Point(729, 598);
      this.btn_cancel.Name = "btn_cancel";
      this.btn_cancel.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_cancel.Size = new System.Drawing.Size(128, 48);
      this.btn_cancel.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_cancel.TabIndex = 13;
      this.btn_cancel.Text = "XCANCEL";
      this.btn_cancel.UseVisualStyleBackColor = false;
      this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
      // 
      // pb_user
      // 
      this.pb_user.Image = global::WSI.Cashier.Resources.ResourceImages.anonymous_user;
      this.pb_user.InitialImage = ((System.Drawing.Image)(resources.GetObject("pb_user.InitialImage")));
      this.pb_user.Location = new System.Drawing.Point(14, 6);
      this.pb_user.Name = "pb_user";
      this.pb_user.Size = new System.Drawing.Size(843, 577);
      this.pb_user.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
      this.pb_user.TabIndex = 73;
      this.pb_user.TabStop = false;
      // 
      // frm_account_photo
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(871, 713);
      this.ControlBox = false;
      this.KeyPreview = true;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "frm_account_photo";
      this.ShowIcon = false;
      this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
      this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
      this.Style = WSI.Cashier.Controls.frm_base.BaseFormStyle.PRINCIPAL;
      this.Text = "xAccount Search";
      this.pnl_data.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.pb_user)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private uc_round_button btn_cancel;
    private System.Windows.Forms.PictureBox pb_user;
  }
}


