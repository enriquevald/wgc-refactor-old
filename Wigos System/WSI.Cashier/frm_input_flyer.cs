﻿
//------------------------------------------------------------------------------
// Copyright © 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_input_flyer.cs
// 
//   DESCRIPTION: Allows the input of the Flyer Code
//
//        AUTHOR: AAC
// 
// CREATION DATE: 07-AUG-2007
//  
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 18-APR-2017 MS      First release.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WSI.Common;
using WSI.Cashier.JunketsCashier;

namespace WSI.Cashier
{
  public partial class frm_input_flyer : WSI.Cashier.Controls.frm_base
  {
    #region Attributes

    private const Int16 NUM_FLYERS_LENGTH = 6;
    private Int64 m_account_id;
    private Int64 m_promotion_id;
    private Int64 m_flag_id;
    private JunketsShared m_junket_info;

    #endregion

    #region " Constructor "

    public frm_input_flyer()
    {
      InitializeComponent();

      InitializeControlResources();
    }

    #endregion " Constructor "

    #region " Private Methods "

    /// <summary>
    /// Initialize control resources (NLS strings, images, etc)
    /// </summary>
    private void InitializeControlResources()
    {
      // - NLS Strings:
      //   - Title:
      this.FormTitle = Resource.String("STR_APP_JUNKET_FLYER_TITLE").ToUpper();

      this.m_junket_info = null;

      //   - Buttons
      btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");
      btn_ok.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_OK");
      txt_num_flyer.MaxLength = NUM_FLYERS_LENGTH;

      lbl_enter_num_flyer.Text = Resource.String("STR_APP_JUNKET_MSG_ENTER_CODE");

      // - Images:

    } // InitializeControlResources


    /// <summary>
    /// Init Form
    /// </summary>
    private void Init()
    {
      this.Location = new Point(1024 / 2 - this.Width / 2, 768 / 2 - this.Height / 2 - 90);

      //Shows keyboard if Automatic OSK it's enabled
      WSIKeyboard.Toggle();
    } // Init

   #endregion " Private Methods "

    #region " Public Methods "

    /// <summary>
    /// Show form 
    /// </summary>
    /// <param name="AccountId"></param>
    /// <param name="Parent"></param>
    /// <param name="JunketInfo"></param>
    public void Show(Int64 AccountId,Form Parent, out JunketsShared JunketInfo)
    {
      Misc.WriteLog("[FORM LOAD] frm_input_flyer", Log.Type.Message);

      // TODO:  Perform any initialization here

      this.Init();
      this.txt_num_flyer.Text = "";
      this.m_account_id = AccountId;

      KeyboardMessageFilter.RegisterHandler(this, BarcodeType.Flyer, FlyerReceivedEvent);

      this.StartPosition = FormStartPosition.CenterParent;

      this.ShowDialog(Parent);      

      JunketInfo = m_junket_info;          

    } // Show

    /// <summary>
    /// Show form.
    /// </summary>
    /// <param name="Parent"></param>
    public void Show(Form Parent)
    {
      Misc.WriteLog("[FORM LOAD] frm_input_flyer", Log.Type.Message);

      // TODO:  Perform any initialization here

      this.Init();
      this.txt_num_flyer.Text = "";

      KeyboardMessageFilter.RegisterHandler(this, BarcodeType.Flyer, FlyerReceivedEvent);

      this.ShowDialog(Parent);

    } // Show

    #endregion " Public Methods "

    #region Events

    /// <summary>
    /// Event for flyers recived.
    /// </summary>
    /// <param name="e"></param>
    /// <param name="data"></param>
    private void FlyerReceivedEvent(KbdMsgEvent e, Object data)
    {
      String _barcode; ;

      _barcode = String.Empty;

      if (e == KbdMsgEvent.EventFlyer)
      {
        _barcode = (String)data;
        txt_num_flyer.Text = _barcode;
        btn_ok_Click(null, null);
      }
    } // FlyerReceivedEvent


    /// <summary>
    /// Keyboard click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btn_keyboard_Click(object sender, EventArgs e)
    {
      try
      {
        WSIKeyboard.ForceToggle();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } // btn_keyboard_Click

    /// <summary>
    /// Cancel click
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btn_cancel_Click(object sender, EventArgs e)
    {
      WSIKeyboard.Hide();
      this.Dispose();

    } // btn_cancel_Click

    /// <summary>
    /// Ok Click
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btn_ok_Click(object sender, EventArgs e)
    {
      JunketsShared _junket_info = new JunketsShared();

      if (txt_num_flyer.Text != "")
      {
        if (_junket_info.ValidateFlyerCode(m_account_id, txt_num_flyer.Text, this))
        {
          if (_junket_info.BusinessLogic.PromotionID != null)
          {
            this.m_promotion_id = (Int64)_junket_info.BusinessLogic.PromotionID;
          }
          this.m_flag_id = _junket_info.BusinessLogic.FlagId;
          this.m_junket_info = _junket_info;
          this.Dispose();
        }
      }

      WSIKeyboard.Hide();

    } // btn_ok_Click

    #endregion " Events "

  }
}
