﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: ExternalPaymentAndSellValidationCommon.cs
// 
//   DESCRIPTION: Miscellaneous utilities for External Validation Operation
// 
//        AUTHOR: Francis Gretz
// 
// CREATION DATE: 01-SEP-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 01-SEP-2016 FGB    First release (Header).
// 19-SEP-2016 FGB    PBI 15985: Exped System: Cashier: Check if the payment/sale can be done
//------------------------------------------------------------------------------
//
using System;
using System.Collections.Generic;
using System.Text;
using WSI.Common.ExternalPaymentAndSaleValidation;

namespace WSI.Common
{
  public enum ExternalPaymentAndSellValidationMode
  { 
    MODE_UNKNOWN = -1,
    MODE_DISABLED = 0,
    MODE_EXPED = 1
  }

  public static class ExternalPaymentAndSellValidationCommon
  {
    public const String GP_KEY_EXTERNAL_PAYMENT_VALIDATION = "ExternalPaymentAndSaleValidation";

    /// <summary>
    /// Returns the mode
    /// </summary>
    /// <returns></returns>
    private static ExternalPaymentAndSellValidationMode GetExternalPaymentValidationMode()
    {
      Int64 _external_payment_validation_mode;

      try
      {
        _external_payment_validation_mode = GeneralParam.GetInt64(GP_KEY_EXTERNAL_PAYMENT_VALIDATION, "Mode", 0);
        return (ExternalPaymentAndSellValidationMode)_external_payment_validation_mode;
      }
      catch
      {
      }

      return ExternalPaymentAndSellValidationMode.MODE_UNKNOWN;
    }

    /// <summary>
    /// Returns if is Enabled the External Payment Validation
    /// </summary>
    /// <returns></returns>
    private static Boolean IsEnabledExternalPaymentValidation(ExternalPaymentAndSellValidationMode Mode)
    {
      return (Mode == ExternalPaymentAndSellValidationMode.MODE_EXPED);
    }

    /// <summary>
    /// Returns the limit of the External Payment Validation
    /// </summary>
    /// <returns></returns>
    private static Decimal GetExternalPaymentValidationLimit()
    {
      Decimal _payment_limit;
      _payment_limit = GeneralParam.GetDecimal(GP_KEY_EXTERNAL_PAYMENT_VALIDATION, "Cashier.Payment.Limit", 0m);

      return _payment_limit;
    }

    /// <summary>
    /// Returns if the External Payment validates the Payment to AccountId
    /// </summary>
    /// <param name="AccountId">AccountId to validate</param>
    /// <param name="Ammount">Ammount to validate</param>
    /// <param name="ErrorMessage">Message that shows why the AccountId wasn't validated</param>
    /// <returns></returns>
    public static Boolean ExternalPaymentValidation(Int64 AccountId, Decimal Ammount, out String ErrorMessage)
    {
      ExternalPaymentAndSellValidationMode _mode;

      ErrorMessage = String.Empty;

      _mode = GetExternalPaymentValidationMode();
      if (!IsEnabledExternalPaymentValidation(_mode))
      {
        return true;
      }

      switch (_mode)
      {
        //If ExternalPaymentService Disabled then do not validate
        case ExternalPaymentAndSellValidationMode.MODE_DISABLED:
          return true;

        //Exped Mode -> Validate with Exped WebService
        case ExternalPaymentAndSellValidationMode.MODE_EXPED:
          return ExternalPaymentValidation_Exped(AccountId, Ammount, out ErrorMessage);

        default:
          //case ExternalPaymentAndSellValidationMode.MODE_UNKNOWN      included in the default
          //TODO: FGB: 2016-08-02: Check if ErrorMessage must have an error message
          return false;
      }
    }

    /// <summary>
    /// Returns if the Exped Qebservice validates the Payment to AccountId
    /// </summary>
    /// <param name="AccountId"></param>
    /// <param name="Ammount"></param>
    /// <param name="ErrorMessage"></param>
    /// <returns></returns>
    public static Boolean ExternalPaymentValidation_Exped(Int64 AccountId, Decimal Ammount, out String ErrorMessage)
    {
      Decimal _payment_limit;
      IExternalPaymentAndSaleValidation _external_payment_validation;
      EnumExternalPaymentAndSaleValidationState _payment_validation_Code;

      ErrorMessage = String.Empty;

      //Check if amount >= limit
      _payment_limit = GetExternalPaymentValidationLimit();

      if (Ammount >= _payment_limit)
      {
        //ExternalPaymentService check
        _external_payment_validation = new ExternalPaymentAndSaleValidation_Exped();

        return (_external_payment_validation.ExternalPaymentAndSaleValidation(AccountId, out _payment_validation_Code, out ErrorMessage));
      }

      return true;    
    }
  }
}
