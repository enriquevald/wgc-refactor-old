namespace WSI.Cashier
{
  partial class frm_scan_document
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.btn_rotate = new WSI.Cashier.Controls.uc_round_button();
      this.btn_exit = new WSI.Cashier.Controls.uc_round_button();
      this.btn_configure = new WSI.Cashier.Controls.uc_round_button();
      this.btn_delete = new WSI.Cashier.Controls.uc_round_button();
      this.btn_scan = new WSI.Cashier.Controls.uc_round_button();
      this.lbl_must_scan = new WSI.Cashier.Controls.uc_label();
      this.lbl_msg_blink = new WSI.Cashier.Controls.uc_label();
      this.lbl_images_list = new WSI.Cashier.Controls.uc_label();
      this.lbl_text = new WSI.Cashier.Controls.uc_label();
      this.lst_images = new WSI.Cashier.Controls.uc_round_list_box();
      this.cb_document_type = new WSI.Cashier.Controls.uc_round_combobox();
      this.pnl_document_image = new System.Windows.Forms.Panel();
      this.pnl_data.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.Controls.Add(this.lbl_must_scan);
      this.pnl_data.Controls.Add(this.lbl_msg_blink);
      this.pnl_data.Controls.Add(this.lbl_text);
      this.pnl_data.Controls.Add(this.lst_images);
      this.pnl_data.Controls.Add(this.cb_document_type);
      this.pnl_data.Controls.Add(this.btn_scan);
      this.pnl_data.Controls.Add(this.pnl_document_image);
      this.pnl_data.Controls.Add(this.lbl_images_list);
      this.pnl_data.Controls.Add(this.btn_rotate);
      this.pnl_data.Controls.Add(this.btn_exit);
      this.pnl_data.Controls.Add(this.btn_configure);
      this.pnl_data.Controls.Add(this.btn_delete);
      this.pnl_data.Size = new System.Drawing.Size(1024, 658);
      this.pnl_data.TabIndex = 0;
      // 
      // btn_rotate
      // 
      this.btn_rotate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.btn_rotate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.btn_rotate.FlatAppearance.BorderSize = 0;
      this.btn_rotate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_rotate.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_rotate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.btn_rotate.Image = null;
      this.btn_rotate.Location = new System.Drawing.Point(521, 576);
      this.btn_rotate.Name = "btn_rotate";
      this.btn_rotate.Size = new System.Drawing.Size(155, 60);
      this.btn_rotate.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_rotate.TabIndex = 9;
      this.btn_rotate.Text = "XROTATE IMAGE";
      this.btn_rotate.UseVisualStyleBackColor = false;
      // 
      // btn_exit
      // 
      this.btn_exit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.btn_exit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.btn_exit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btn_exit.FlatAppearance.BorderSize = 0;
      this.btn_exit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_exit.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_exit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.btn_exit.Image = null;
      this.btn_exit.Location = new System.Drawing.Point(847, 576);
      this.btn_exit.Name = "btn_exit";
      this.btn_exit.Size = new System.Drawing.Size(155, 60);
      this.btn_exit.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_exit.TabIndex = 10;
      this.btn_exit.Text = "XCLOSE";
      this.btn_exit.UseVisualStyleBackColor = false;
      // 
      // btn_configure
      // 
      this.btn_configure.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.btn_configure.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.btn_configure.FlatAppearance.BorderSize = 0;
      this.btn_configure.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_configure.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_configure.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.btn_configure.Image = null;
      this.btn_configure.Location = new System.Drawing.Point(23, 576);
      this.btn_configure.Name = "btn_configure";
      this.btn_configure.Size = new System.Drawing.Size(155, 60);
      this.btn_configure.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.TERTIARY;
      this.btn_configure.TabIndex = 7;
      this.btn_configure.Text = "XCONFIGURE";
      this.btn_configure.UseVisualStyleBackColor = false;
      // 
      // btn_delete
      // 
      this.btn_delete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.btn_delete.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.btn_delete.FlatAppearance.BorderSize = 0;
      this.btn_delete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_delete.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_delete.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.btn_delete.Image = null;
      this.btn_delete.Location = new System.Drawing.Point(333, 576);
      this.btn_delete.Name = "btn_delete";
      this.btn_delete.Size = new System.Drawing.Size(155, 60);
      this.btn_delete.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_delete.TabIndex = 8;
      this.btn_delete.Text = "XDELETE IMAGE";
      this.btn_delete.UseVisualStyleBackColor = false;
      // 
      // btn_scan
      // 
      this.btn_scan.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.btn_scan.FlatAppearance.BorderSize = 0;
      this.btn_scan.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_scan.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_scan.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.btn_scan.Image = null;
      this.btn_scan.Location = new System.Drawing.Point(44, 109);
      this.btn_scan.Name = "btn_scan";
      this.btn_scan.Size = new System.Drawing.Size(205, 50);
      this.btn_scan.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_scan.TabIndex = 2;
      this.btn_scan.Text = "XSCAN";
      this.btn_scan.UseVisualStyleBackColor = false;
      // 
      // lbl_must_scan
      // 
      this.lbl_must_scan.BackColor = System.Drawing.Color.Transparent;
      this.lbl_must_scan.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_must_scan.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.lbl_must_scan.Location = new System.Drawing.Point(22, 174);
      this.lbl_must_scan.Name = "lbl_must_scan";
      this.lbl_must_scan.Size = new System.Drawing.Size(256, 191);
      this.lbl_must_scan.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_must_scan.TabIndex = 3;
      this.lbl_must_scan.Text = "x * Must Scan Edit Account";
      // 
      // lbl_msg_blink
      // 
      this.lbl_msg_blink.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_msg_blink.BackColor = System.Drawing.Color.Transparent;
      this.lbl_msg_blink.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
      this.lbl_msg_blink.ForeColor = System.Drawing.Color.White;
      this.lbl_msg_blink.Location = new System.Drawing.Point(803, 677);
      this.lbl_msg_blink.Name = "lbl_msg_blink";
      this.lbl_msg_blink.Padding = new System.Windows.Forms.Padding(3);
      this.lbl_msg_blink.Size = new System.Drawing.Size(210, 39);
      this.lbl_msg_blink.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.lbl_msg_blink.TabIndex = 7;
      this.lbl_msg_blink.Text = "xError Message";
      this.lbl_msg_blink.Visible = false;
      // 
      // lbl_images_list
      // 
      this.lbl_images_list.BackColor = System.Drawing.Color.Transparent;
      this.lbl_images_list.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_images_list.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.lbl_images_list.Location = new System.Drawing.Point(16, 14);
      this.lbl_images_list.Name = "lbl_images_list";
      this.lbl_images_list.Size = new System.Drawing.Size(277, 39);
      this.lbl_images_list.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_images_list.TabIndex = 0;
      this.lbl_images_list.Text = "XSELECT A DOCUMENT TYPE TO SCAN";
      // 
      // lbl_text
      // 
      this.lbl_text.BackColor = System.Drawing.Color.Transparent;
      this.lbl_text.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_text.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.lbl_text.Location = new System.Drawing.Point(732, 13);
      this.lbl_text.Name = "lbl_text";
      this.lbl_text.Size = new System.Drawing.Size(274, 538);
      this.lbl_text.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_text.TabIndex = 6;
      this.lbl_text.Text = "xHelp Message";
      // 
      // lst_images
      // 
      this.lst_images.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.lst_images.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.lst_images.CornerRadius = 0;
      this.lst_images.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
      this.lst_images.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lst_images.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.lst_images.FormattingEnabled = true;
      this.lst_images.ItemHeight = 40;
      this.lst_images.Location = new System.Drawing.Point(22, 375);
      this.lst_images.Name = "lst_images";
      this.lst_images.SelectedIndex = -1;
      this.lst_images.SelectedItem = null;
      this.lst_images.Size = new System.Drawing.Size(256, 176);
      this.lst_images.Sorted = false;
      this.lst_images.Style = WSI.Cashier.Controls.uc_round_list_box.RoundListBoxStyle.BASIC;
      this.lst_images.TabIndex = 4;
      this.lst_images.TopIndex = 0;
      // 
      // cb_document_type
      // 
      this.cb_document_type.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.cb_document_type.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.cb_document_type.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.cb_document_type.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.cb_document_type.CornerRadius = 5;
      this.cb_document_type.DataSource = null;
      this.cb_document_type.DisplayMember = "";
      this.cb_document_type.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
      this.cb_document_type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
      this.cb_document_type.DropDownWidth = 257;
      this.cb_document_type.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.cb_document_type.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.cb_document_type.FormattingEnabled = true;
      this.cb_document_type.IsDroppedDown = false;
      this.cb_document_type.ItemHeight = 40;
      this.cb_document_type.Location = new System.Drawing.Point(21, 56);
      this.cb_document_type.MaxDropDownItems = 8;
      this.cb_document_type.Name = "cb_document_type";
      this.cb_document_type.SelectedIndex = -1;
      this.cb_document_type.SelectedItem = null;
      this.cb_document_type.SelectedValue = null;
      this.cb_document_type.SelectionLength = 0;
      this.cb_document_type.SelectionStart = 0;
      this.cb_document_type.Size = new System.Drawing.Size(257, 40);
      this.cb_document_type.Sorted = false;
      this.cb_document_type.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.BASIC;
      this.cb_document_type.TabIndex = 1;
      this.cb_document_type.TabStop = false;
      this.cb_document_type.ValueMember = "";
      // 
      // pnl_document_image
      // 
      this.pnl_document_image.BackColor = System.Drawing.SystemColors.AppWorkspace;
      this.pnl_document_image.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
      this.pnl_document_image.Location = new System.Drawing.Point(303, 13);
      this.pnl_document_image.Name = "pnl_document_image";
      this.pnl_document_image.Size = new System.Drawing.Size(412, 538);
      this.pnl_document_image.TabIndex = 5;
      // 
      // frm_scan_document
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.AutoSize = true;
      this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
      this.ClientSize = new System.Drawing.Size(1024, 713);
      this.HeaderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "frm_scan_document";
      this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
      this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
      this.Style = WSI.Cashier.Controls.frm_base.BaseFormStyle.PRINCIPAL;
      this.Text = "xScanDocument";
      this.pnl_data.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private WSI.Cashier.Controls.uc_round_button btn_exit;
    private WSI.Cashier.Controls.uc_label lbl_text;
    private WSI.Cashier.Controls.uc_round_button btn_scan;
    private WSI.Cashier.Controls.uc_round_button btn_configure;
    private WSI.Cashier.Controls.uc_round_button btn_delete;
    private System.Windows.Forms.Panel pnl_document_image;
    private WSI.Cashier.Controls.uc_round_list_box lst_images; 
    private WSI.Cashier.Controls.uc_round_combobox cb_document_type;
    private WSI.Cashier.Controls.uc_label lbl_images_list;
    private WSI.Cashier.Controls.uc_round_button btn_rotate;
    private WSI.Cashier.Controls.uc_label lbl_msg_blink;
    private WSI.Cashier.Controls.uc_label lbl_must_scan;


  }
}