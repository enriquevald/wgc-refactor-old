//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_scan_document.cs
// 
//   DESCRIPTION: Implements document scan dialog. 
//
//        AUTHOR: Adri�n Cuartas
// 
// CREATION DATE: 18-JUL-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 18-JUL-2013 ACM    First release.
// 16-AUG-2013 LEM    Fixed Bug WIG-124: Not checked max number of images for UNKNOWN document type
// 12-FEB-2014 FBA    Added account holder id type control via CardData class IdentificationTypes
// 11-JUL-2014 XIT    Added Multicountry support
// 02-NOV-2015 JMV    Product Backlog Item 5904:Dise�o cajero: Aplicar en frm_scan_document.cs
// 07-NOV-2017 DHA    Bug 30537:WIGOS-6156 [Ticket #9894] Garcia River - Release 03.005.105 - SnapShell - Imagen escaneada
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using WSI.Common;
using System.Drawing;
using System.IO;
using WSI.Cashier.Controls;

namespace WSI.Cashier
{
  public partial class frm_scan_document : frm_base
  {
    delegate void SetEnableButtonsCallback(bool Enabled);

    #region Constants

    private const int MAX_SIZE_ALLOWED = 1048576; //1048576 bytes  = 1 MByte

    #endregion

    #region Members

    private frm_yesno form_yes_no;

    private DocumentList m_docs_list;
    private Image m_document_image;
    private ACCOUNT_SCANNED_OWNER m_docs_owner;
    private Int32 m_max_allowed_scan;
    private String m_title;
    private Boolean m_documents_changed;
    private Boolean m_anti_ml_mode;
    private String m_id3_type;
    private List<string> m_initial_document_list;

    #endregion

    #region Properties

    public DocumentList DocumentsList
    {
      get { return m_docs_list; }
    }

    public String Id3Type
    {
      get { return m_id3_type; }
    }

    public Boolean DocumentsChanged
    {
      get { return m_documents_changed; }
    }

    #endregion

    #region Constructor

    /// <summary>
    /// Constructor method.
    /// 1. Initialize controls.
    /// 2. Initialize control�s resources. (NLS, Images, etc)
    /// </summary>
    public frm_scan_document(ACCOUNT_SCANNED_OWNER DocsOwner, String ScanTitle, CardData PlayerData, Boolean DocumentsListChanged, Boolean AntiMoneyLaunderingMode)
    {
      // Initialize controls
      InitializeComponent();

      m_title = ScanTitle;
      m_docs_owner = DocsOwner;
      m_document_image = null;
      m_initial_document_list = new List<string>();
      m_max_allowed_scan = GeneralParam.GetInt32("Cashier.Scan", "MaxDocuments", 4);
      m_anti_ml_mode = AntiMoneyLaunderingMode;
      m_documents_changed = DocumentsListChanged;

      if (PlayerData.PlayerTracking.HolderScannedDocs == null || PlayerData.PlayerTracking.BeneficiaryScannedDocs == null)
      {
        // Load Existing Documents
        PlayerData.LoadAccountDocuments();
      }

      switch (m_docs_owner)
      {
        case ACCOUNT_SCANNED_OWNER.HOLDER:
          m_docs_list = PlayerData.PlayerTracking.HolderScannedDocs;
          break;
        case ACCOUNT_SCANNED_OWNER.BENEFICIARY:
          m_docs_list = PlayerData.PlayerTracking.BeneficiaryScannedDocs;
          break;
      }

      // Add event handler to scan and configure buttons
      InitControls();

      InitializeControlResources();

      Init();
    }

    #endregion

    #region Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Add event handler to scan and configure buttons
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    private void InitControls()
    {
      EventLastAction.AddEventLastAction(this.Controls);

      this.btn_configure.Click += new EventHandler(this.btn_configure_Click);
      this.btn_exit.Click += new EventHandler(this.btn_exit_Click);
      this.btn_scan.Click += new EventHandler(this.btn_scan_Click);
      this.btn_delete.Click += new EventHandler(this.btn_delete_Click);
      this.btn_rotate.Click += new EventHandler(this.btn_rotate_Click);
      this.pnl_document_image.Click += new EventHandler(this.pnl_document_image_Click);
      this.cb_document_type.SelectedIndexChanged += new EventHandler(this.cb_document_type_SelectIndexChanged);
      this.lst_images.SelectedIndexChanged += lst_images_SelectedIndexChanged;

      this.lbl_must_scan.Visible = false;

      LoadCombos();
      LoadImagesList();
      SetEnableButtons(true);

    }

    private void LoadRequiredScanDocuments()
    {
      String _req_doc_list;
      String _req_doc_list_money_laund;
      String _final_label;

      _req_doc_list = String.Empty;
      _req_doc_list_money_laund = String.Empty;
      _final_label = String.Empty;

      // Account Edit:
      // Holder
      if (m_docs_owner == ACCOUNT_SCANNED_OWNER.HOLDER)
      {
        if (GeneralParam.GetBoolean("Account.RequestedField", "DocScan"))
        {
          _final_label = Resource.String("STR_FRM_SCAN_MESSAGE_4");
          _req_doc_list = IdentificationTypes.DocIdTypeStringList(GeneralParam.GetString("Account.RequestedField", "DocScanTypeList"));

          //Any
          if (String.IsNullOrEmpty(_req_doc_list))
          {
            _req_doc_list = Resource.String("STR_FRM_SCAN_MESSAGE_7");
          }

          _final_label = _final_label + Resource.String("STR_FRM_SCAN_MESSAGE_6", _req_doc_list);
        }
      }
      // Beneficiary
      if (m_docs_owner == ACCOUNT_SCANNED_OWNER.BENEFICIARY)
      {
        if (GeneralParam.GetBoolean("Beneficiary.RequestedField", "DocScan"))
        {
          _final_label = Resource.String("STR_FRM_SCAN_MESSAGE_4");
          _req_doc_list = IdentificationTypes.DocIdTypeStringList(GeneralParam.GetString("Beneficiary.RequestedField", "DocScanTypeList"));

          //Any
          if (String.IsNullOrEmpty(_req_doc_list))
          {
            _req_doc_list = Resource.String("STR_FRM_SCAN_MESSAGE_7");
          }

          _final_label = _final_label + Resource.String("STR_FRM_SCAN_MESSAGE_6", _req_doc_list);
        }
      }

      // Anti-money Laundering:
      if (m_anti_ml_mode)
      {
        // Holder
        if (m_docs_owner == ACCOUNT_SCANNED_OWNER.HOLDER)
        {
          if (GeneralParam.GetBoolean("Account.RequestedField", "AntiMoneyLaundering.DocScan"))
          {
            _req_doc_list_money_laund = IdentificationTypes.DocIdTypeStringList(GeneralParam.GetString("Account.RequestedField", "AntiMoneyLaundering.DocScanTypeList"));

            //Any
            if (String.IsNullOrEmpty(_req_doc_list_money_laund))
            {
              _req_doc_list_money_laund = Resource.String("STR_FRM_SCAN_MESSAGE_7");
            }

            //Must Scan?
            if (String.IsNullOrEmpty(_final_label))
            {
              _final_label = Resource.String("STR_FRM_SCAN_MESSAGE_4");
            }
            _final_label = _final_label + "\r\n" + Resource.String("STR_FRM_SCAN_MESSAGE_5", Accounts.getAMLName(), _req_doc_list_money_laund);
          }
        }
        // Beneficiary
        if (m_docs_owner == ACCOUNT_SCANNED_OWNER.BENEFICIARY)
        {
          if (GeneralParam.GetBoolean("Beneficiary.RequestedField", "AntiMoneyLaundering.DocScan"))
          {
            _req_doc_list_money_laund = IdentificationTypes.DocIdTypeStringList(GeneralParam.GetString("Beneficiary.RequestedField", "AntiMoneyLaundering.DocScanTypeList"));

            //Any
            if (String.IsNullOrEmpty(_req_doc_list_money_laund))
            {
              _req_doc_list_money_laund = Resource.String("STR_FRM_SCAN_MESSAGE_7");
            }

            //Must Scan?
            if (String.IsNullOrEmpty(_final_label))
            {
              _final_label = Resource.String("STR_FRM_SCAN_MESSAGE_4");
            }
            _final_label = _final_label + "\r\n" + Resource.String("STR_FRM_SCAN_MESSAGE_5", Accounts.getAMLName(), _req_doc_list_money_laund);
          }
        }
      }
      //Show Label?
      if (!String.IsNullOrEmpty(_final_label))
      {
        lbl_must_scan.Text = _final_label.Replace("\\r\\n", "\r\n");
        lbl_must_scan.Visible = true;
      }
    }

    private void LoadCombos()
    {
      DataTable _dt_scan_id_types;

      _dt_scan_id_types = IdentificationTypes.GetIdentificationTypes(false);
      this.cb_document_type.DataSource = _dt_scan_id_types;
      this.cb_document_type.ValueMember = "IDT_ID";
      this.cb_document_type.DisplayMember = "IDT_NAME";

      // Select the default document type.
      this.cb_document_type.SelectedValue = GeneralParam.GetInt32("Account.DefaultValues", "DocumentType");
      // If not selected, select the first one.
      if (this.cb_document_type.SelectedValue == null)
      {
        this.cb_document_type.SelectedIndex = 0;
      }
    }

    public void CleanupForm()
    {
      this.cb_document_type.Dispose();
      if (form_yes_no != null)
      {
        form_yes_no.Dispose();
        form_yes_no = null;
      }
      m_docs_list = null;
      m_document_image = null;
      m_initial_document_list = null;
      this.Dispose();
    }

    private void LoadImagesList()
    {
      if (m_docs_list.Count > 0)
      {
        String _document_type;

        // Document name structure (example): XXX_Y_Random Number
        // XXX: Scanned Document Type
        // Y: Scanned Document Owner --> Holder, Beneficiary.
        foreach (IDocument _document in m_docs_list)
        {
          if (CardData.GetInfoDocName(_document.Name, out  _document_type))
          {
            // Set the document type name
            _document_type = IdentificationTypes.DocIdTypeString((ACCOUNT_HOLDER_ID_TYPE)Convert.ToInt32(_document_type));
            // Add to list
            lst_images.Items.Add(GetDocumentsTypeNameCopies(_document_type));
            m_initial_document_list.Add(_document_type);
          }
        }

        lst_images.SelectedIndex = 0;

      }
    }


    //------------------------------------------------------------------------------
    // PURPOSE : Init screen strings
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    private void InitializeControlResources()
    {
      // NLS Strings    
      this.FormTitle = Resource.String("STR_FRM_SCAN_TITLE") + " " + m_title;    // "Escanear documento"
      this.btn_exit.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CLOSE");
      this.btn_scan.Text = Resource.String("STR_FRM_SCAN_SCAN");
      this.btn_configure.Text = Resource.String("STR_FRM_SCAN_CONFIG");
      this.btn_delete.Text = Resource.String("STR_FRM_SCAN_DELETE");
      this.btn_rotate.Text = Resource.String("STR_FRM_SCAN_ROTATE");
      this.lbl_text.Text = Resource.String("STR_FRM_SCAN_MESSAGE").Replace("\\r\\n", "\r\n");
      this.lbl_images_list.Text = Resource.String("STR_FRM_SCAN_MESSAGE_2");

      LoadRequiredScanDocuments(); //  lbl_must_scan and lbl_must_scan_ml labels+

    } // InitializeControlResources

    private void Init()
    {
      if (!ScanImage.CheckCmdTwainFiles())
      {
        cb_document_type.Enabled = false;
        btn_scan.Enabled = false;
        this.lbl_must_scan.Visible = false;
      }
    } // Init  

    private void SetEnableButtons(bool Enabled)
    {
      if (this.btn_exit.InvokeRequired ||
          this.btn_scan.InvokeRequired ||
          this.btn_configure.InvokeRequired ||
          this.btn_delete.InvokeRequired ||
          this.btn_rotate.InvokeRequired)
      {
        SetEnableButtonsCallback _buttons_callback = new SetEnableButtonsCallback(SetEnableButtons);
        this.Invoke(_buttons_callback, new object[] { Enabled });
      }
      else
      {
        this.btn_delete.Enabled = Enabled;
        this.btn_exit.Enabled = Enabled;
        this.btn_scan.Enabled = Enabled;
        this.btn_configure.Enabled = Enabled;
        this.btn_rotate.Enabled = Enabled;

        //Disable "Delete" and "Rotate" button
        if (this.lst_images.Items.Count == 0)
        {
          this.btn_delete.Enabled = false;
          this.btn_rotate.Enabled = false;
        }

        //Disable "Scan" button
        if (this.btn_scan.Enabled == true &&
           ((this.lst_images.Items.Count >= this.m_max_allowed_scan ||
             this.m_docs_list.Length() > MAX_SIZE_ALLOWED ||
             this.LimitOfCopiesReached())))
        {
          this.btn_scan.Enabled = false;
        }
      }
    }
    private bool PreviewImage(Image Image)
    {
      try
      {
        if (Image == null)
        {
          return false;
        }

        pnl_document_image.BackgroundImage = Image;
        pnl_document_image.Refresh();

        //Don't Show label error message
        lbl_msg_blink.Visible = false;

        return true;
      }
      catch (Exception Ex)
      {
        Log.Exception(Ex);
      }

      return false;
    }

    private void ConfigureScanner()
    {
      ScanImage.ConfigureScanner();

    } // ConfigureScanner


    //------------------------------------------------------------------------------
    // PURPOSE :  Throws the scanning window, get the file scanned, preview of the image and save in the Documents List. 
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    private void ScanID()
    {
      try
      {
        SetEnableButtons(false);

        frm_scan_preview _frm_scan_preview;
        RectangleF _rectangle_to_crop;
        String _selected_document_type;
        String _error_message;

        _selected_document_type = cb_document_type.SelectedValue.ToString();
        _error_message = String.Empty;
        _frm_scan_preview = new frm_scan_preview();
        lbl_msg_blink.Visible = false;

        if (_frm_scan_preview.Show())
        {
          this.Cursor = Cursors.WaitCursor;

          m_document_image = Image.FromStream(new MemoryStream(File.ReadAllBytes(_frm_scan_preview.OutFileName)));

          //Get rectangle with the area and position values
          if (ScanImage.GetRectangleFromValues(_selected_document_type, out _rectangle_to_crop))
          {
            //Crop Image
            if (!ScanImage.CropImage(m_document_image,
                                     _rectangle_to_crop,
                                     GeneralParam.GetBoolean("Cashier.Scan", "GrayScale"),
                                     GeneralParam.GetInt32("Cashier.Scan", "Quality", 90),
                                     out m_document_image))
            {
              m_document_image = Image.FromStream(new MemoryStream(File.ReadAllBytes(_frm_scan_preview.OutFileName)));
              _error_message = Resource.String("STR_FRM_SCAN_MESSAGE_8");
            }
          }
          else //If rectangle values are bad
          {
            _error_message = Resource.String("STR_FRM_SCAN_MESSAGE_8");
          }

          //Delete scanned file from disk
          File.Delete(_frm_scan_preview.OutFileName);

          //Put the scanned image in the panel (Preview)
          if (PreviewImage(m_document_image))
          {
            String _list_box_item_name;
            String _doc_list_name;

            _list_box_item_name = IdentificationTypes.DocIdTypeString((ACCOUNT_HOLDER_ID_TYPE)cb_document_type.SelectedValue);

            if (CardData.GenerateDocName(m_docs_owner, _selected_document_type, out _doc_list_name))
            {
              //Add scanned image to Document List object 
              AddImageToDocumentList(m_document_image, _doc_list_name, ref m_docs_list);

              //Add image name to List Box control
              lst_images.Items.Add(GetDocumentsTypeNameCopies(_list_box_item_name, m_initial_document_list));
              m_initial_document_list.Add(_list_box_item_name);
              lst_images.SelectedIndex = m_docs_list.Count - 1;
              m_documents_changed = true;
            }
          }

          if (!String.IsNullOrEmpty(_error_message))
          {
            lbl_msg_blink.Text = _error_message;
            lbl_msg_blink.Visible = true;
          }
        }
        else
        {
          if (_frm_scan_preview.ErrorMessage != String.Empty)
          {
            lbl_msg_blink.Text = _frm_scan_preview.ErrorMessage;
            lbl_msg_blink.Visible = true;
          }
        }
        _frm_scan_preview.Dispose();
      }

      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      finally
      {
        this.Cursor = Cursors.Default;
        SetEnableButtons(true);
      }

    } // ScanId

    //-------------------------------------------------------------------------------------
    // PURPOSE :  Set an image and a name to a Document. Add the Document to Document List.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    private void AddImageToDocumentList(Image DocumentImage, String DocumentName, ref DocumentList ImagesList)
    {
      ReadOnlyDocument _doc_image;
      MemoryStream _ms;

      if (DocumentImage != null)
      {
        _ms = new MemoryStream();

        DocumentImage.Save(_ms, System.Drawing.Imaging.ImageFormat.Jpeg);

        _doc_image = new ReadOnlyDocument(DocumentName, _ms);
        ImagesList.Add(_doc_image);
      }
    } // AddImageToAccountDocuments


    //------------------------------------------------------------------------------
    // PURPOSE :  Replace the image of existing document.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    private void UpdateImageInDocumentList(Image DocumentImage, Int32 DocumentIndex, ref DocumentList ImagesList)
    {
      ReadOnlyDocument _doc_image;
      MemoryStream _ms;

      if (DocumentImage != null)
      {
        _ms = new MemoryStream();

        DocumentImage.Save(_ms, System.Drawing.Imaging.ImageFormat.Jpeg);

        _doc_image = new ReadOnlyDocument(ImagesList[DocumentIndex].Name, _ms);
        ImagesList[DocumentIndex] = _doc_image;
      }
    } // UpdateImageInDocumentList


    //------------------------------------------------------------------------------
    // PURPOSE :  Get a String with the document type in the list.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES : Documents Type format: (example) "001,002,003,001"
    private String GetScannedDocumentsTypes()
    {
      String _documents_types;
      String _document_type;

      _documents_types = String.Empty;

      if (m_docs_list.Count == 1)
      {
        if (!CardData.GetInfoDocName(m_docs_list[0].Name, out _documents_types))
        {
          return String.Empty;
        }
      }

      if (m_docs_list.Count > 1)
      {
        foreach (IDocument _document in m_docs_list)
        {
          if (!CardData.GetInfoDocName(_document.Name, out _document_type))
          {
            return String.Empty;
          }
          //First time
          if (_documents_types == String.Empty)
          {
            _documents_types = _document_type;
          }
          else
          {
            _documents_types = String.Format("{0},{1}", _documents_types, _document_type);
          }
        }
      }

      return _documents_types;
    }// GetScannedDocumentsTypes

    //-----------------------------------------------------------------------------------------
    // PURPOSE :  Get a String with the document type name from list Box control, if exist, add a number of the copy. (RFC, RFC (2), RFC (3)).
    //
    //  PARAMS :
    //      - INPUT : String with document type name
    //
    //      - OUTPUT : 
    //
    // RETURNS : String with document type + copy number
    //
    private String GetDocumentsTypeNameCopies(String DocumentTypeName)
    {
      if (!String.IsNullOrEmpty(DocumentTypeName))
      {
        Int16 _copy_number;

        _copy_number = 1;

        if (lst_images.Items.Count > 0)
        {
          foreach (String _doc_type_name in lst_images.Items)
          {
            if (_doc_type_name.Contains(DocumentTypeName))
            {
              _copy_number++;
            }
          }

          if (_copy_number > 1)
          {
            return String.Format("{0} ({1})", DocumentTypeName, _copy_number.ToString());
          }
        }
      }

      return DocumentTypeName;
    }// GetDocumentsTypeNameCopies

    //-----------------------------------------------------------------------------------------
    // PURPOSE :  Get a String with the document type name from list<string> object, if exist, add a number of the copy. (RFC, RFC (2), RFC (3)).
    //
    //  PARAMS :
    //      - INPUT : String with document type name
    //
    //      - OUTPUT : 
    //
    // RETURNS : String with document type + copy number
    //
    private String GetDocumentsTypeNameCopies(String DocumentTypeName, List<string> DocumentTypeListString)
    {
      if (!String.IsNullOrEmpty(DocumentTypeName))
      {
        Int16 _copy_number;

        _copy_number = 1;

        if (DocumentTypeListString.Count > 0)
        {
          foreach (String _doc_type_name in DocumentTypeListString)
          {
            if (_doc_type_name.Contains(DocumentTypeName))
            {
              _copy_number++;
            }
          }

          if (_copy_number > 1)
          {
            return String.Format("{0} ({1})", DocumentTypeName, _copy_number.ToString());
          }
        }
      }

      return DocumentTypeName;
    }// GetDocumentsTypeNameCopies

    //-----------------------------------------------------------------------------------------
    // PURPOSE :  Limiting documents in two images maximum. Except the unknown type.
    //
    //  PARAMS :
    //      - INPUT : String with document type name
    //
    //      - OUTPUT : 
    //
    // RETURNS : True if limit is reached. 
    //
    private Boolean LimitOfCopiesReached()
    {
      String _document_type_name;
      Int32 _max_copies_allowed;
      Int16 _copy_number;

      //Allowed max 2 documents for each type if not UNKNOWN type
      _max_copies_allowed = 2;

      // First time the comboBox is loaded throws the selectedIndexChanged event without a real selected value.
      try
      {
        _document_type_name = IdentificationTypes.DocIdTypeString((ACCOUNT_HOLDER_ID_TYPE)cb_document_type.SelectedValue);
      }
      catch (Exception)
      {
        return false;
      }

      // Unknown type(Other) has max 10 images
      if ((ACCOUNT_HOLDER_ID_TYPE)cb_document_type.SelectedValue == ACCOUNT_HOLDER_ID_TYPE.UNKNOWN)
      {
        _max_copies_allowed = 10;
      }

      if (!String.IsNullOrEmpty(_document_type_name))
      {
        _copy_number = 0;

        if (lst_images.Items.Count > 0)
        {
          foreach (String _doc_type_name in lst_images.Items)
          {
            if (_doc_type_name.Contains(_document_type_name))
            {
              _copy_number++;
            }
          }

          if (_copy_number >= _max_copies_allowed)
          {
            return true;
          }
        }
      }

      return false;
    }// LimitOfCopiesReached

    #endregion

    #region Events

    private void btn_scan_Click(object sender, EventArgs e)
    {
      ScanID();
    }

    private void btn_configure_Click(object sender, EventArgs e)
    {
      SetEnableButtons(false);

      ConfigureScanner();

      SetEnableButtons(true);
    }//btn_configure_Click

    private void btn_exit_Click(object sender, EventArgs e)
    {
      m_id3_type = GetScannedDocumentsTypes();
      this.DialogResult = DialogResult.OK;
    }//btn_exit_Click

    private void btn_delete_Click(object sender, EventArgs e)
    {
      DialogResult _question_yes_no;
      String _message;

      _message = Resource.String("STR_FRM_SCAN_DELETE_MSG");
      _message = _message.Replace("\\r\\n", "\r\n");

      _question_yes_no = frm_message.Show(_message,
                                           Resource.String("STR_APP_GEN_MSG_WARNING"),
                                           MessageBoxButtons.YesNo,
                                           Images.CashierImage.Question,
                                           this.ParentForm);

      if (_question_yes_no != DialogResult.OK)
      {
        return;
      }

      // Remove from Documents List
      if (m_docs_list.Remove(m_docs_list[lst_images.SelectedIndex]))
      {
        // Remove from List Box
        lst_images.Items.Remove(lst_images.Items[lst_images.SelectedIndex]);

        // Select first element after delete item
        if (lst_images.Items.Count > 0)
        {
          lst_images.SelectedIndex = 0;
        }
        // There's no scanned documents. Reset config of controls.
        else
        {
          pnl_document_image.BackgroundImage = null;
          m_document_image.Dispose();
          m_document_image = null;
        }

        m_documents_changed = true;
        SetEnableButtons(true);
      }
    }

    private void btn_rotate_Click(object sender, EventArgs e)
    {
      try
      {
        // Rotate Image
        m_document_image.RotateFlip(RotateFlipType.Rotate90FlipNone);

        //Update Document(with the rotated image) in DocumentsList 
        UpdateImageInDocumentList(m_document_image, lst_images.SelectedIndex, ref m_docs_list);

        // Refresh the image preview and the Images List
        PreviewImage(m_document_image);

        m_documents_changed = true;
      }
      catch (Exception Ex)
      {
        Log.Error(Ex.Message);
      }
    }

    private void cb_document_type_SelectIndexChanged(object sender, EventArgs e)
    {
      SetEnableButtons(true);

      //Don't Show label error message
      lbl_msg_blink.Visible = false;
    }

    private void pnl_document_image_Click(object sender, EventArgs e)
    {
      ViewImage.ViewFullScreenImage(m_document_image);

      //Don't Show label error message
      lbl_msg_blink.Visible = false;
    }

    void lst_images_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (lst_images.Items.Count > 0 && lst_images.SelectedItem != null)
      {
        IDocument _doc_image;
        if (m_docs_list.Find(m_docs_list[lst_images.SelectedIndex].Name, out _doc_image))
        {
          if (m_document_image != null)
          {
            m_document_image.Dispose();
          }
          m_document_image = Image.FromStream(new System.IO.MemoryStream(_doc_image.Content));          
          PreviewImage(m_document_image);

          m_documents_changed = true;
        }
      }
    }

    #endregion

    #region Public Methods

    public new Boolean Show()
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_scan_document", Log.Type.Message);

      form_yes_no = new frm_yesno();
      form_yes_no.Opacity = 0.6;
      form_yes_no.Show();

      // Center screen
      if (WindowManager.StartUpMode == WindowManager.StartUpModes.Explorer)
      {
        this.Location = new Point(WindowManager.GetFormCenterLocation(form_yes_no).X - (this.Width / 2)
                                  , WindowManager.GetFormCenterLocation(form_yes_no).Y - (form_yes_no.Height / 2));
      }
      else
      {
        this.Location = new Point(1024 / 2 - this.Width / 2, 0);
      }
      WSIKeyboard.Hide();

      this.ShowDialog(form_yes_no);

      form_yes_no.Hide();

      return (this.DialogResult == DialogResult.OK);
    } // Show

    #endregion

  }
}
