//------------------------------------------------------------------------------
// Copyright � 2009 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_mb_card_balance.cs
// 
//   DESCRIPTION: User control to display the mobile bank card balance.
// 
//        AUTHOR: Agust� Poch
// 
// CREATION DATE: 05-JUN-2009
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 05-JUN-2009 APB    Inital Draft.
// 07-AUG-2012 JCM    Removed Initial Limit, Total Limit extensions, other fields.
// 30-MAY-2017 JML    Bug 27787:WIGOS-533 Cashier - The amount of the field "Deposits" in the screen mobile bank is displayed in red
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using WSI.Common;

namespace WSI.Cashier
{
  public partial class uc_mb_card_balance : UserControl
  {
    #region Attributes

    Currency m_available = 0;
    Currency m_pending = 0;
    Boolean m_gp_pending_cash_partial;

    #endregion

    #region Properties

    public String CurrentBalance
    {
      get
      {
        return (String)lbl_available_value_1.Text;
      }
      set
      {
        lbl_available_value_1.Text = value;
      }
    }

    public String CashIn
    {
      get
      {
        return (String)lbl_cash_in_value.Text;
      }
      set
      {
        lbl_cash_in_value.Text = value;
      }
    }

    public String CashOut
    {
      get
      {
        return (String)lbl_deposit_value_2.Text;
      }
      set
      {
        lbl_deposit_value_2.Text = value;
      }
    }

    public Currency Available
    {
      get
      {
        return m_available;
      }
    }

    public Currency Pending
    {
      get
      {
        return m_pending;
      }
    }

    #endregion

    #region Constructor

    /// <summary>
    /// Control constructor.
    /// </summary>
    public uc_mb_card_balance()
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[UC LOAD] uc_mb_card_balance", Log.Type.Message);

      InitializeComponent();

      m_gp_pending_cash_partial = MobileBank.GP_GetRegisterShortfallOnDeposit();

      InitializeControlResources();

      FillCardCounters(new MBCardData());
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Initialize control resources (NLS strings, images, etc)
    /// </summary>
    public void InitializeControlResources()
    {
      // - NLS Strings:
      //   - Title:

      //   - Labels

      rp_panel1.HeaderText = Resource.String("STR_UC_MB_CARD_BALANCE_BALANCE_NAME");

      pictureBox2.Image = Resources.ResourceImages.coins; 
      lbl_splitline.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(95)))), ((int)(((byte)(95)))));
      
      lbl_available_name_1.Text = Resource.String("STR_UC_MB_CARD_BALANCE_BALANCE_NAME") + ":";
      lbl_mb_session_closed.Text = Resource.String("STR_UC_MB_CARD_CLOSED_SESSION");
      lbl_cash_in_name.Text = Resource.String("STR_UC_MB_CARD_BALANCE_CASH_IN_NAME") + ":";
      lbl_deposit_name_2.Text = Resource.String("STR_UC_MB_CARD_BALANCE_CASH_DEPOSIT_NAME") + ":";
      lbl_cash_lost_name.Text = Resource.String("STR_UC_CARD_PENDING_DEPOSITS") + ":";
      lbl_mb_excess.Text = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_DIFF_EXTRA") + ":";

      if (!m_gp_pending_cash_partial)
      {
        lbl_pending_cash_name.Text = Resource.String("STR_UC_MB_CARD_BALANCE_PENDING_CASH_NAME") + ":";

        lbl_cash_lost_name.Visible = false;
        lbl_cash_lost_value.Visible = false;

        //lbl_separator_1.Location = new Point(3, 200);

        //lbl_pending_cash_name.Location = new Point(16, 210);
        //lbl_pending_cash_value.Location = new Point(183, 202);
      }
      else
      {
        lbl_pending_cash_name.Text = Resource.String("STR_UC_MB_CARD_BALANCE_PENDING_CASH_NAME") + ":";
      }

      this.Invalidate();

      //   - Buttons
      //btn_ok.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_OK");
      //btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");

      // - Images:

    } // InitializeControlResources

    #endregion

    #region Public Methods

    /// <summary>
    /// Fill Card  counters from Card Id.
    /// </summary>
    /// <param name="CardId">Card Id.</param>
    public void FillCardCounters(MBCardData CardData)
    {
      MBCardData card_data;

      card_data = CardData;
      m_available = card_data.SalesLimit;
      m_pending = (card_data.BalanceLost == 0 ? (((Decimal)card_data.DisposedCash + card_data.OverCash) - (card_data.DepositCash)) : (Decimal)card_data.BalanceLost);

      // Refresh card counters
      lbl_available_value_1.Text = card_data.SalesLimit.ToString();
      lbl_cash_in_value.Text = card_data.DisposedCash.ToString();
      lbl_deposit_value_2.Text = card_data.DepositCash.ToString();
      lbl_cash_lost_value.Text = card_data.ShortFallCash.ToString();//m_pending.ToString();
      lbl_excess_value.Text = card_data.OverCash.ToString();
      lbl_pending_cash_value.Text = card_data.PendingCash.ToString();

    } // FillCardCounters

    /// <summary>
    /// show/hide "closed session" label
    /// </summary>
    public void ShowClosedSessionLabel(Boolean ShowLabel)
    {
      lbl_mb_session_closed.Visible = ShowLabel;
    }

    private void uc_round_list_box1_Click(object sender, EventArgs e)
    {

    }

    private void lbl_pending_cash_name_Click(object sender, EventArgs e)
    {

    } // ShowClosedSessionLabel

    #endregion
  }
}
