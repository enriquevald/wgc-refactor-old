﻿//------------------------------------------------------------------------------
// Copyright © 2007-2014 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_gt_card_reader.cs
// 
//   DESCRIPTION: Implements the uc_gt_dealer_copy_ticket_reader user control
//                Class: uc_gt_dealer_copy_ticket_reader
//
//        AUTHOR: David Hernández
// 
// CREATION DATE: 28-MAR-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 28-MAR-2017 DHA     First release.
// 06-APR-2017 RAB     PBI 26540: MES10 Ticket validation - Prevent the use of TITO tickets
// 03-MAY-2017 DHA     PBI 27171:MES10 Ticket validation - Ticket validation at player tracking screen
// ----------- ------ ----------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using WSI.Common;
using WSI.Common.TITO;
using WSI.Cashier.TITO;
using System.Diagnostics;

namespace WSI.Cashier
{
  public partial class uc_gt_dealer_copy_ticket_reader : UserControl
  {

    #region Class Atributes

    public delegate void ValidationNumberReadEventHandler(ref Boolean IsValid, Ticket Ticket);

    public delegate void ValidationNumberReadingHandler();

    public event ValidationNumberReadEventHandler OnValidationNumberReadEvent;
    public event ValidationNumberReadingHandler OnValidationNumberReadingEvent;

    private const Int32 VALIDATION_NUMBER_LENGTH = 20;

    private const Int32 HIDE_HEIGTH = 0; // 30;

    private Boolean m_External_message;
    private Boolean is_kbd_msg_filter_event = false;
    private String received_track_data;

    private Int32 m_heigth;

    private Int32 tick_last_change = 0;
    private Boolean all_data_is_entered = false;
    private Boolean ignore_additional_numbers = false;

    private uc_input_amount_resizable m_uc_input_amount_resizable;

    uc_popup_helper m_uc_popup_helper;
    uc_popup_helper m_uc_popup_helper_parent;

    public uc_popup_helper UcPopupHelperParent
    {
      get { return m_uc_popup_helper_parent; }
      set { m_uc_popup_helper_parent = value; }
    }

    #endregion

    #region Public Methods

    public uc_gt_dealer_copy_ticket_reader()
    {
      InitializeComponent();

      InitUserControl();

      m_heigth = this.Height;

    }

    public void Hide_DealerCopyValidation()
    {
      this.Height = HIDE_HEIGTH;
      this.Visible = false;
    }

    public void Show_DealerCopyValidation()
    {
      pb_close.Image = WSI.Cashier.Images.Get(Images.CashierImage.Calc_Header_Close);

      this.Height = m_heigth;
      this.Visible = true;
      pb_close.Image = WSI.Cashier.Images.Get(Images.CashierImage.Calc_Header_Close);
      txt_validation_number.Focus();
      txt_validation_number.SelectAll();

    }

    public void ShowNumericKeyoard()
    {
      Point _location;
      TableLayoutPanelCellPosition _cell_pos;
      Int32 _cell_height;

      _location = this.PointToScreen(new Point(0, 0));

      _cell_pos = tableLayoutPanel1.GetCellPosition(lbl_head);
      _cell_height = tableLayoutPanel1.GetRowHeights()[_cell_pos.Row];

      m_uc_input_amount_resizable.txt_amount.Text = this.txt_validation_number.Text;

      m_uc_popup_helper.Show(new Point(_location.X + this.Width - 1, _cell_height + _location.Y));

    }

    /// <summary>
    /// Initialize control resources. (NLS string, images, etc.) 
    /// </summary>
    public void InitializeControlResources()
    {
      // - NLS Strings:
      //   - Title

      //   - Labels
      lbl_head.Text = Resource.String("STR_UC_DEALER_COPY_TITLE");
      lbl_swipe_ticket.Text = Resource.String("STR_UC_TICKET_VALID_NUMBER");
      lbl_invalid_ticket.Text = Resource.String("STR_UC_TICKET_NOT_FOUND");
      m_External_message = false;

    } // InitializeControlResources

    /// <summary>
    /// Clear input for track number actions.
    /// </summary>
    public void ClearTrackNumber()
    {
      txt_validation_number.Text = "";
      tick_last_change = 0;

      lbl_invalid_ticket.Visible = false;
      all_data_is_entered = false;
      ignore_additional_numbers = false;

    } // ClearTrackNumber

    public void TicketMessage(String ResourceString)
    {
      lbl_invalid_ticket.Text = ResourceString;
      lbl_invalid_ticket.Visible = true;
      m_External_message = true;
    }

    public void ShowMessageBottom()
    {
      lbl_invalid_ticket.Location = new System.Drawing.Point(txt_validation_number.Location.X, txt_validation_number.Location.Y + 45);
    } // ShowMessageBottom

    public void AutoTicketReadMode(Barcode Barcode)
    {
      String _error_str;

      if (!ProfilePermissions.CheckPermissionList(ProfilePermissions.CashierFormFuncionality.Chips_Buy_In,
                                                  ProfilePermissions.TypeOperation.OnlyCheck,
                                                  this.ParentForm,
                                                  0,
                                                  out _error_str))
      {
        return;
      }

      received_track_data = Barcode.ExternalCode;

      is_kbd_msg_filter_event = true;
      txt_track_number_TextChanged(this, new EventArgs());
      this.txt_validation_number.Text = Barcode.ExternalCode.ToString();

      txt_track_number_KeyPress(null, new KeyPressEventArgs('\r'));
    }

    #endregion Public Methods

    #region Private Methods

    private void pb_close_Click(object sender, EventArgs e)
    {
      Hide_DealerCopyValidation();
    }

    private void InitUserControl()
    {
      InitializeControlResources();

      InitNumericKeyboard();

      if (!this.DesignMode)
      {
        tmr_clear.Enabled = true;
        ClearTrackNumber();
        txt_validation_number.MaxLength = VALIDATION_NUMBER_LENGTH;
      }

      pb_close.Image = WSI.Cashier.Images.Get(Images.CashierImage.Calc_Header_Close);

      txt_validation_number.Focus();


    }

    private void InitNumericKeyboard()
    {
      m_uc_input_amount_resizable = new uc_input_amount_resizable(uc_input_amount_resizable.DisplayWindowMode.ONLY_NUM_KEYS_WITHOUT_DOT, uc_input_amount_resizable.InputMode.TICKET, 300, 250);
      m_uc_input_amount_resizable.OnModifyValue += m_uc_input_amount_resizable_OnModifyValue;
      m_uc_input_amount_resizable.OnAmountSelected += m_uc_input_amount_resizable_OnAmountSelected;
      m_uc_popup_helper = new uc_popup_helper(m_uc_input_amount_resizable);

      m_uc_popup_helper.OnClose += m_uc_popup_helper_OnClose;

    }

    void m_uc_input_amount_resizable_OnAmountSelected(decimal Amount)
    {
      m_uc_popup_helper.Close();

      txt_validation_number.Text = Amount.ToString();

      txt_track_number_KeyPress(null, new KeyPressEventArgs('\r'));
    }

    void m_uc_input_amount_resizable_OnModifyValue(string Amount)
    {
      txt_validation_number.Text = Amount;
    }

    void m_uc_popup_helper_OnClose()
    {
      m_uc_input_amount_resizable.txt_amount.Text = String.Empty;
    }

    private void ProcessTicketNumber(String ValidationNumber, ref Boolean IsValid, out String ErrorStr, out Ticket Ticket)
    {
      Int64 _validation_number;
      TITO_VALIDATION_TYPE _validation_type;
      DateTime _expiration_date_limit;
      String _error_str;

      IsValid = false;
      ErrorStr = "";
      Ticket = null;
      _validation_number = 0;
      _error_str = string.Empty;

      try
      {
        //  // Disable buttons
        //  EnableButtonStatus(false);

        if (String.IsNullOrEmpty(ValidationNumber))
        {
          IsValid = true;

          return;
        }

        _validation_number = ValidationNumberManager.UnFormatValidationNumber(ValidationNumber, out _validation_type);

        if (_validation_number > 0)
        {

          using (DB_TRX _db_trx = new DB_TRX())
          {
            Ticket = BusinessLogic.SelectTicketByCondition(String.Format(" TI_VALIDATION_NUMBER = {0} ", _validation_number),
                                                          String.Format(" TI_TYPE_ID = {0}", TITO_TICKET_TYPE.CHIPS_DEALER_COPY));

            if (Ticket.ValidationNumber > 0)
            {

              if(Ticket.TicketType != TITO_TICKET_TYPE.CHIPS_DEALER_COPY)
              {
                ErrorStr = Resource.String("STR_UC_COPY_DEALER_TICKET_STATUS_INVALID");
                return;
              }

              BusinessLogic.CheckGracePeriod(Ticket.ExpirationDateTime, out _expiration_date_limit);

              if (Ticket.Status != TITO_TICKET_STATUS.VALID)
              {
                ErrorStr = GetInvalidStatusMsg(Ticket.Status);
                return;
              }

              if (Ticket.ExpirationDateTime < WGDB.Now)
              {
                ErrorStr = ErrorStr = GetInvalidStatusMsg(TITO_TICKET_STATUS.EXPIRED);

                return;
              }

              IsValid = true;
            }
          }
        }
      }
      catch { }

    }

    private void uc_gt_ticket_reader1_Click(object sender, EventArgs e)
    {
      ShowNumericKeyoard();
    }

    private String GetInvalidStatusMsg(TITO_TICKET_STATUS Status)
    {
      String _msg;

      _msg = "";

      //SGB 31-OCT-2014: Create ticket status of pending_cancel
      switch (Status)
      {
        case TITO_TICKET_STATUS.DISCARDED:
          _msg = Resource.String("STR_UC_COPY_DEALER_TICKET_STATUS_DISCARDED");
          break;
        case TITO_TICKET_STATUS.CANCELED:
          _msg = Resource.String("STR_UC_COPY_DEALER_TICKET_STATUS_CANCELED");
          break;
        case TITO_TICKET_STATUS.EXPIRED:
          _msg = Resource.String("STR_UC_COPY_DEALER_TICKET_STATUS_EXPIRED");
          break;
        default:
          _msg = Resource.String("STR_UC_COPY_DEALER_TICKET_STATUS_INVALID");          
          break;
      }

      return _msg;
      
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void pictureBox1_Click(object sender, EventArgs e)
    {
      ClearTrackNumber();
      txt_validation_number.Focus();
    } // pictureBox1_Click

    /// <summary>
    /// Checks is the input is completely numeric.
    /// </summary>
    /// <param name="TrackNumber"></param>
    /// <returns></returns>
    private Boolean PartialTrackNumberIsValid(String TrackNumber)
    {
      Byte[] chars;

      chars = null;

      try
      {
        chars = Encoding.ASCII.GetBytes(TrackNumber);
        foreach (byte b in chars)
        {
          if (b < '0')
          {
            return false;
          }
          if (b > '9')
          {
            return false;
          }
        }
      }
      finally
      {
        chars = null;
      }

      return true;
    } // PartialTrackNumberIsValid

    /// <summary>
    /// Track number checking.
    /// </summary>
    /// <param name="TrackNumber"></param>
    /// <returns></returns>
    private Boolean TrackNumberIsValid(String TrackNumber)
    {
      if (TrackNumber.Length == VALIDATION_NUMBER_LENGTH)
      {
        if (PartialTrackNumberIsValid(TrackNumber))
        {
          String _internal_track;
          int _card_type;

          if (WSI.Common.CardNumber.TrackDataToInternal(TrackNumber, out _internal_track, out _card_type))
          {
            if (_card_type == 0
                || _card_type == 1)
            {
              return true;
            }
          }
        }
      }
      return false;
    } // TrackNumberIsValid

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void txt_track_number_TextChanged(object sender, EventArgs e)
    {
      String track_number;
      //if card readed meanwhile disconnected message, do nothing
      if (WSI.Common.WGDB.ConnectionState != ConnectionState.Open
          && WSI.Common.WGDB.ConnectionState != ConnectionState.Executing
          && WSI.Common.WGDB.ConnectionState != ConnectionState.Fetching)
      {
        ClearTrackNumber();
        return;
      }

      // Variables initialization
      tick_last_change = 0;
      if (is_kbd_msg_filter_event)
      {
        track_number = received_track_data;
      }
      else
      {
        track_number = txt_validation_number.Text;
      }
      all_data_is_entered = (track_number.Length >= VALIDATION_NUMBER_LENGTH);

      if (track_number.Length > 0)
      {
        // DHA 13-JAN-2014: When user is entering a ticket number, the invalid ticket message is hidden
        lbl_invalid_ticket.Visible = false;
        if (OnValidationNumberReadingEvent != null)
        {
          OnValidationNumberReadingEvent();
        }
      }
    } // txt_track_number_TextChanged

    /// <summary>
    /// Key pressed management.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void txt_track_number_KeyPress(object sender, KeyPressEventArgs e)
    {
      String track_number;
      Boolean is_valid;
      String _error_str;
      Ticket _ticket;

      tick_last_change = 0;
      _error_str = "";

      if (is_kbd_msg_filter_event)
      {
        track_number = received_track_data;
        is_kbd_msg_filter_event = false;
      }
      else
      {
        track_number = txt_validation_number.Text;
      }

      switch (e.KeyChar)
      {
        case '%':
          // Ignore track start/end markers
          ignore_additional_numbers = false;
          e.KeyChar = '\0';
          break;

        case ';':
          // Ignore track start/end markers
          ignore_additional_numbers = true;
          e.KeyChar = '\0';
          break;

        case '?':
          // Ignore track start/end markers
          e.KeyChar = '\0';
          break;

        case '\r':
          if (!ignore_additional_numbers)
          {
            //TODOif (all_data_is_entered)
            {
              if (track_number.Length <= VALIDATION_NUMBER_LENGTH)
              {
                is_valid = true;
                //TODO: is_valid = TrackNumberIsValid(track_number);
                if (is_valid)
                {
                  tmr_clear.Enabled = false;
                  ProcessTicketNumber(track_number, ref is_valid, out _error_str, out _ticket);
                  if (OnValidationNumberReadEvent != null)
                  {
                    try
                    {
                      WSI.Common.Users.SetLastAction("ReadTicket", "");
                      OnValidationNumberReadEvent(ref is_valid, _ticket);
                    }
                    catch
                    {
                      is_valid = false;
                    }
                  }
                  tmr_clear.Enabled = true;
                }

                if (!is_valid)
                {
                  Show_DealerCopyValidation();

                  txt_validation_number.SelectAll();

                  lbl_invalid_ticket.Text = !String.IsNullOrEmpty(_error_str) ? _error_str : Resource.String("STR_UC_TICKET_NOT_FOUND");
                  lbl_invalid_ticket.Visible = true;

                }
                else
                {
                  if (!m_External_message)
                  {
                    ClearTrackNumber();
                  }
                  m_External_message = false;
                }
              }
            }
          }
          else
          {
            // Allow processing additional keystrokes
            ignore_additional_numbers = false;
          }

          e.Handled = true;
          break;

        default:
          if (ignore_additional_numbers)
          {
            // All keystrokes must be ignored until a valid sequence starts
            e.KeyChar = '\0';
          }
          break;
      } // switch
    } // txt_track_number_KeyPress

    /// <summary>
    /// Timer to clear card input, if key is not pressed in five seconds.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void tmr_clear_Tick(object sender, EventArgs e)
    {
      tick_last_change += 1;
      if (tick_last_change > 20) // 5 seconds
      {
        ClearTrackNumber();
        tick_last_change = 0;
      }
    } // tmr_clear_Tick

    private void BarCodeReceivedEvent(KbdMsgEvent e, Object data)
    {
      Barcode _barcode;
      KeyPressEventArgs _event_args;

      if (e == KbdMsgEvent.EventBarcode)
      {
        _barcode = (Barcode)data;
        received_track_data = _barcode.ExternalCode;

        is_kbd_msg_filter_event = true;
        txt_track_number_TextChanged(this, new EventArgs());
        _event_args = new KeyPressEventArgs('\r');
        txt_track_number_KeyPress(this, _event_args);
      }
    }

    private void uc_ticket_reader_VisibleChanged(object sender, EventArgs e)
    {
      if (this.Visible)
      {
        KeyboardMessageFilter.RegisterHandler(this, BarcodeType.FilterAnyTito, BarCodeReceivedEvent);
      }
    }

    private void txt_validation_number_Click(object sender, EventArgs e)
    {
      txt_validation_number.Text = String.Empty;
      m_uc_input_amount_resizable.txt_amount.Text = String.Empty;

      ShowNumericKeyoard();
    }

    #endregion Private Methods

  }
}
