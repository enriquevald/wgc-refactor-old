﻿//------------------------------------------------------------------------------
// Copyright © 2007-2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: PaymentThresholdAuthorization.cs
// 
//   DESCRIPTION: Implements the PaymentThresholdAuthorization
//                Class: PaymentThresholdAuthorization (Payment Threshold Authorization)
//
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 03-OCT-2017 DHA     First release.
// 03-OCT-2017 RAB     PBI 30019: WIGOS-4040 Payment threshold authorization - Payment threshold configuration
// 03-OCT-2017 RAB     PBI 30020: WIGOS-4046 Payment threshold authorization - Recharge threshold configuration
// 05-OCT-2017 RAB     PBI 30021: WIGOS-4048 Payment threshold authorisation - Recharge authorisation
// 05-OCT-2017 RAB     PBI 30018: WIGOS-4036 Payment threshold authorization - Payment authorisation
// 09-OCT-2017 JML     PBI 30022:WIGOS-4052 Payment threshold registration - Recharge with Check payment
// 10-OCT-2017 JML     PBI 30023:WIGOS-4068 Payment threshold registration - Transaction information
// 30-OCT-2017 RAB     Bug 30446:WIGOS-6052 Threshold: Screen order is incorrect in HP and dispute (paying with PO) exceeding threshold and without permissions
//--------------------------------------------------------------------------------------------


using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Windows.Forms;
using WSI.Cashier.MVC.Controller.PlayerEditDetails;
using WSI.Cashier.MVC.View.PlayerEditDetails;
using WSI.Common;


namespace WSI.Cashier
{
  public class PaymentAndRechargeThresholdData
  {
    #region Members

    private CurrencyExchangeType m_transaction_type;
    private CurrencyExchangeSubType _m_transaction_subtype;
    private DateTime  m_transaction_date;
    private Int64 m_undo_operation_id;
    private Decimal m_cash_amount;

    #endregion

    #region Properties
    public CurrencyExchangeType TransactionType
    {
      get { return m_transaction_type; }
      set { m_transaction_type = value; }
    }
    public CurrencyExchangeSubType TransactionSubType
    {
      get { return _m_transaction_subtype; }
      set { _m_transaction_subtype = value; }
    }
    public DateTime TransactionDate
    {
      get { return m_transaction_date; }
      set { m_transaction_date = value; }
    }
    public Int64 UndoOperationId
    {
      get { return m_undo_operation_id; }
      set { m_undo_operation_id = value; }
    }
    public Decimal CashAmount
    {
      get { return m_cash_amount; }
      set { m_cash_amount = value; }
    }

    #endregion
  }

  public class PaymentThresholdAuthorization
  {
    #region Enums
    public enum PaymentThresholdAuthorizationOperation
    {
      Input = 0, // Recharge threshold
      Output = 1 // Payment threshold
    }
    #endregion

    #region Members
    private CardData m_card_data;
    private Currency m_input_threshold;
    private Currency m_output_threshold;
    private Currency m_total_amount;
    //private Currency m_recharge_threshold;
    //private Currency m_payment_threshold;
    private PaymentAndRechargeThresholdData m_payment_and_recharge_threshold_data;

    public PaymentAndRechargeThresholdData PaymentAndRechargeThresholdData
    {
      get { return m_payment_and_recharge_threshold_data; }
      set { m_payment_and_recharge_threshold_data = value; }
    }
    
    #endregion

    #region Properties

    public Currency InputThreshold
    {
      get
      {
        return m_input_threshold;
      }
    }
    
    public Currency OutputThreshold
    {
      get
      {
        return m_output_threshold;
      }
    }
    #endregion

    #region Public functions
    public PaymentThresholdAuthorization(Decimal TotalAmount, CardData CardData, PaymentAndRechargeThresholdData Data, PaymentThresholdAuthorizationOperation Operation, Form Form, out Boolean ContinueProcess)
    {
      m_total_amount = TotalAmount;

      if (Data.TransactionType == null)
      {
        Data.TransactionType = CurrencyExchangeType.CURRENCY;
      }
      if (Data.TransactionSubType == null)
      {
        Data.TransactionSubType = CurrencyExchangeSubType.NONE;
      }
      Data.CashAmount = 0;

      PaymentAndRechargeThresholdData = Data;

      switch (Operation)
      {
        case PaymentThresholdAuthorizationOperation.Input:
          m_input_threshold = GetInputThreshold();

          if (m_input_threshold == 0 || TotalAmount < m_input_threshold)
          {
            ContinueProcess = true;
            return;
          }
          
          break;
        case PaymentThresholdAuthorizationOperation.Output:
          m_output_threshold = GetOutputThreshold();

          if (m_output_threshold == 0 || TotalAmount < m_output_threshold)
          {
            ContinueProcess = true;
            return;
          }

          break;
        default:
          ContinueProcess = true;
          return;
      }
      
      if (!RegistrationAccount(CardData))
      {
        ContinueProcess = false;
        return;
      }

      ContinueProcess = true;
    }
    #endregion

    private Boolean RegistrationAccount(CardData CardData)
    {
      DialogResult _dgr_ac_edit;

      using (PlayerEditDetailsCashierView _view = new PlayerEditDetailsCashierView())
      {
        using (PlayerEditDetailsCashierController _controller =
          new PlayerEditDetailsCashierController(CardData, _view, new IDScanner.IDScanner(), PlayerEditDetailsCashierView.SCREEN_MODE.REGISTER_CODERE))
        {
          _controller.MustShowComments = false;

          if (WindowManager.StartUpMode == WindowManager.StartUpModes.Explorer)
          {
            _view.Location = WindowManager.GetMainFormAreaLocation();
          }

          _dgr_ac_edit = _view.ShowDialog();

          if (_dgr_ac_edit != System.Windows.Forms.DialogResult.OK)
          {
            return false;
          }

          m_card_data = _controller.CardData;
        }
      }

      return true;

    }

    /// <summary>
    /// Get recharge threshold parameter
    /// </summary>
    /// <returns></returns>
    public static Currency GetInputThreshold()
    {
      if(!WSI.Common.TITO.Utils.IsTitoMode())
      {
        return 0;
      }

      return GeneralParam.GetCurrency("Client.Registration", "Input.Threshold", 0);
    }

    /// <summary>
    /// Get payment threshold parameter
    /// </summary>
    /// <returns></returns>
    public static Currency GetOutputThreshold()
    {
      if (!WSI.Common.TITO.Utils.IsTitoMode())
      {
        return 0;
      }

      return GeneralParam.GetCurrency("Client.Registration", "Output.Threshold", 0);
    }

    public Boolean SaveThresholdOperation(Int64 OperationId, OperationCode OperationCode, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Int32 _temp_int32;
      Int64 _temp_int64;
      String _temp_string;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" INSERT INTO   THRESHOLD_OPERATIONS        ");
        _sb.AppendLine("             ( TO_TRANSACTION_TYPE         ");
        _sb.AppendLine("             , TO_TRANSACTION_SUBTYPE      ");
        _sb.AppendLine("             , TO_TOTAL_AMOUNT             ");
        _sb.AppendLine("             , TO_CASH_AMOUNT              ");
        _sb.AppendLine("             , TO_TRANSACTION_DATE         ");
        _sb.AppendLine("             , TO_OPERATION_ID             ");
        _sb.AppendLine("             , TO_OPERATION_CODE           ");
        _sb.AppendLine("             , TO_UNDO_OPERATION_ID        ");
        _sb.AppendLine("             , TO_HOLDER_ID_TYPE           ");
        _sb.AppendLine("             , TO_HOLDER_ID                ");
        _sb.AppendLine("             , TO_USER_ID                  ");
        _sb.AppendLine("             , TO_USER_NAME                ");
        _sb.AppendLine("             , TO_ACCOUNT_ID               ");
        _sb.AppendLine("             , TO_HOLDER_NAME              ");
        _sb.AppendLine("             , TO_HOLDER_NAME1             ");
        _sb.AppendLine("             , TO_HOLDER_NAME2             ");
        _sb.AppendLine("             , TO_HOLDER_NAME3             ");
        _sb.AppendLine("             , TO_HOLDER_NAME4             ");
        _sb.AppendLine("             , TO_HOLDER_ADDRESS_01        ");
        _sb.AppendLine("             , TO_HOLDER_ADDRESS_02        ");
        _sb.AppendLine("             , TO_HOLDER_ADDRESS_03        ");
        _sb.AppendLine("             , TO_HOLDER_EXT_NUM           ");
        _sb.AppendLine("             , TO_HOLDER_CITY              ");
        _sb.AppendLine("             , TO_HOLDER_FED_ENTITY        ");
        _sb.AppendLine("             , TO_HOLDER_ZIP               ");
        _sb.AppendLine("             , TO_HOLDER_ADDRESS_COUNTRY   ");
        _sb.AppendLine("             , TO_HOLDER_NATIONALITY       ");
        _sb.AppendLine("             , TO_HOLDER_BIRTH_DATE        ");
        _sb.AppendLine("             , TO_HOLDER_OCCUPATION_ID     ");
        _sb.AppendLine("             , TO_SCANNED_IDENTITY_CARD    ");
        _sb.AppendLine("             )                             ");
        _sb.AppendLine("      VALUES ( @pTransactionType           ");
        _sb.AppendLine("             , @pTransactionSubType        ");
        _sb.AppendLine("             , @pTotalAmount               ");
        _sb.AppendLine("             , @pCashAmount                ");
        _sb.AppendLine("             , GETDATE()                   ");
        _sb.AppendLine("             , @pOperationId               ");
        _sb.AppendLine("             , @pOperationCode             ");
        _sb.AppendLine("             , @pUndoOperationId           ");
        _sb.AppendLine("             , @pHolderIdType              ");
        _sb.AppendLine("             , @pHolderId                  ");
        _sb.AppendLine("             , @pUserId                    ");
        _sb.AppendLine("             , @pUserName                  ");
        _sb.AppendLine("             , @pAccountId                 ");
        _sb.AppendLine("             , @pHolderName                ");
        _sb.AppendLine("             , @pHolderName1               ");
        _sb.AppendLine("             , @pHolderName2               ");
        _sb.AppendLine("             , @pHolderName3               ");
        _sb.AppendLine("             , @pHolderName4               ");
        _sb.AppendLine("             , @pHolderAddress01           ");
        _sb.AppendLine("             , @pHolderAddress02           ");
        _sb.AppendLine("             , @pHolderAddress03           ");
        _sb.AppendLine("             , @pHolderExtNum              ");
        _sb.AppendLine("             , @pHolderCity                ");
        _sb.AppendLine("             , @pHolderFedEntity           ");
        _sb.AppendLine("             , @pHolderZip                 ");
        _sb.AppendLine("             , @pHolderAddressCountry      ");
        _sb.AppendLine("             , @pHolderNationality         ");
        _sb.AppendLine("             , @pHolderBirthDate           ");
        _sb.AppendLine("             , @pHolderOccupationId        ");
        _sb.AppendLine("             , @pScannedIdentityCard       ");
        _sb.AppendLine("             )                             ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pTransactionType", SqlDbType.Int).Value = (Int32)m_payment_and_recharge_threshold_data.TransactionType;
          _cmd.Parameters.Add("@pTransactionSubType", SqlDbType.Int).Value = (Int32)m_payment_and_recharge_threshold_data.TransactionSubType;
          _cmd.Parameters.Add("@pTotalAmount", SqlDbType.Money).Value = m_total_amount.Decimal;
          _cmd.Parameters.Add("@pCashAmount", SqlDbType.Money).Value = PaymentAndRechargeThresholdData.CashAmount;

          _cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt).Value = (OperationId == 0) ? DBNull.Value : (object)OperationId;

          _cmd.Parameters.Add("@pOperationCode", SqlDbType.Int).Value = (Int32)OperationCode;

          _temp_int64 = m_payment_and_recharge_threshold_data.UndoOperationId;
          _cmd.Parameters.Add("@pUndoOperationId", SqlDbType.BigInt).Value = (_temp_int64 == 0) ? DBNull.Value : (object)_temp_int64;

          _temp_int32 = (Int32)m_card_data.PlayerTracking.HolderIdType;
          _cmd.Parameters.Add("@pHolderIdType", SqlDbType.Int).Value = (_temp_int32 == 0) ? DBNull.Value : (object)_temp_int32;

          _temp_string = m_card_data.PlayerTracking.HolderId.ToUpperInvariant();
          _cmd.Parameters.Add("@pHolderId", SqlDbType.NVarChar).Value = (String.IsNullOrEmpty(_temp_string)) ? DBNull.Value : (object)_temp_string;

          _cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = CommonCashierInformation.AuthorizedByUserId;
          _cmd.Parameters.Add("@pUserName", SqlDbType.NVarChar).Value = CommonCashierInformation.AuthorizedByUserName;

          _temp_int64 =  m_card_data.AccountId;
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = (_temp_int64 == 0) ? DBNull.Value : (object)_temp_int64;

          _temp_string = m_card_data.PlayerTracking.HolderName;
          _cmd.Parameters.Add("@pHolderName", SqlDbType.NVarChar).Value = (String.IsNullOrEmpty(_temp_string)) ? DBNull.Value : (object)_temp_string;

          _temp_string =m_card_data.PlayerTracking.HolderName1;
          _cmd.Parameters.Add("@pHolderName1", SqlDbType.NVarChar).Value = (String.IsNullOrEmpty(_temp_string)) ? DBNull.Value : (object)_temp_string;

          _temp_string =m_card_data.PlayerTracking.HolderName2;
          _cmd.Parameters.Add("@pHolderName2", SqlDbType.NVarChar).Value = (String.IsNullOrEmpty(_temp_string)) ? DBNull.Value : (object)_temp_string;

          _temp_string =m_card_data.PlayerTracking.HolderName3;
          _cmd.Parameters.Add("@pHolderName3", SqlDbType.NVarChar).Value = (String.IsNullOrEmpty(_temp_string)) ? DBNull.Value : (object)_temp_string;

          _temp_string = m_card_data.PlayerTracking.HolderName4;
          _cmd.Parameters.Add("@pHolderName4", SqlDbType.NVarChar).Value = (String.IsNullOrEmpty(_temp_string)) ? DBNull.Value : (object)_temp_string;

          _temp_string =m_card_data.PlayerTracking.HolderAddress01;
          _cmd.Parameters.Add("@pHolderAddress01", SqlDbType.NVarChar).Value = (String.IsNullOrEmpty(_temp_string)) ? DBNull.Value : (object)_temp_string;

          _temp_string = m_card_data.PlayerTracking.HolderAddress02;
          _cmd.Parameters.Add("@pHolderAddress02", SqlDbType.NVarChar).Value = (String.IsNullOrEmpty(_temp_string)) ? DBNull.Value : (object)_temp_string;

          _temp_string = m_card_data.PlayerTracking.HolderAddress03;
          _cmd.Parameters.Add("@pHolderAddress03", SqlDbType.NVarChar).Value = (String.IsNullOrEmpty(_temp_string)) ? DBNull.Value : (object)_temp_string;

          _temp_string =m_card_data.PlayerTracking.HolderExtNumber;
          _cmd.Parameters.Add("@pHolderExtNum", SqlDbType.NVarChar).Value = (String.IsNullOrEmpty(_temp_string)) ? DBNull.Value : (object)_temp_string;

          _temp_string =m_card_data.PlayerTracking.HolderCity;
          _cmd.Parameters.Add("@pHolderCity", SqlDbType.NVarChar).Value = (String.IsNullOrEmpty(_temp_string)) ? DBNull.Value : (object)_temp_string;

          _temp_int32 =m_card_data.PlayerTracking.HolderFedEntity;
          _cmd.Parameters.Add("@pHolderFedEntity", SqlDbType.Int).Value = (_temp_int32 == 0) ? DBNull.Value : (object)_temp_int32;

          _temp_string = m_card_data.PlayerTracking.HolderZip;
          _cmd.Parameters.Add("@pHolderZip", SqlDbType.NVarChar).Value = (String.IsNullOrEmpty(_temp_string)) ? DBNull.Value : (object)_temp_string;

          _temp_int32 =m_card_data.PlayerTracking.HolderAddressCountry;
          _cmd.Parameters.Add("@pHolderAddressCountry", SqlDbType.Int).Value = (_temp_int32 == 0) ? DBNull.Value : (object)_temp_int32;

          _temp_int64 =m_card_data.PlayerTracking.HolderNationality;
          _cmd.Parameters.Add("@pHolderNationality", SqlDbType.BigInt).Value = (_temp_int64 == 0) ? DBNull.Value : (object)_temp_int64;


          _cmd.Parameters.Add("@pHolderBirthDate", SqlDbType.DateTime).Value = m_card_data.PlayerTracking.HolderBirthDate;

          _temp_int32 =m_card_data.PlayerTracking.HolderOccupationId;
          _cmd.Parameters.Add("@pHolderOccupationId", SqlDbType.Int).Value = (_temp_int32 == 0) ? DBNull.Value : (object)_temp_int32;

          if (m_card_data.PlayerTracking.HolderScannedDocs.Count > 0)
          {
            _cmd.Parameters.Add("@pScannedIdentityCard", SqlDbType.Binary).Value = m_card_data.PlayerTracking.HolderScannedDocs.ConvertToBinary();
          }
          else
          {
            _cmd.Parameters.Add("@pScannedIdentityCard", SqlDbType.Binary).Value = DBNull.Value;
          }

          if (_cmd.ExecuteNonQuery() != 1)
          {
            Log.Error("SaveThresholdOperation. AccountId: " + m_card_data.AccountId.ToString());

            return false;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

        return false;

    }
  }
}
