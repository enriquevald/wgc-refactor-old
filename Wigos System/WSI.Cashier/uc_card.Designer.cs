namespace WSI.Cashier
{
  partial class uc_card
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose (bool disposing)
    {
      if ( disposing && ( components != null ) )
      {
        components.Dispose ();
      }
      base.Dispose (disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent ()
    {
      this.components = new System.ComponentModel.Container();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(uc_card));
      this.timer1 = new System.Windows.Forms.Timer(this.components);
      this.lbl_separator_1 = new System.Windows.Forms.Label();
      this.uc_round_panel1 = new WSI.Cashier.Controls.uc_round_panel();
      this.btn_disputes = new WSI.Cashier.Controls.uc_round_button();
      this.btn_undo_operation = new WSI.Cashier.Controls.uc_round_button();
      this.btn_handpays = new WSI.Cashier.Controls.uc_round_button();
      this.btn_lasts_movements = new WSI.Cashier.Controls.uc_round_button();
      this.btn_player_browse = new WSI.Cashier.Controls.uc_round_button();
      this.gb_chips = new WSI.Cashier.Controls.uc_round_panel();
      this.pb_chips = new System.Windows.Forms.PictureBox();
      this.btn_chips_sale_register = new WSI.Cashier.Controls.uc_round_button();
      this.btn_sell_chips = new WSI.Cashier.Controls.uc_round_button();
      this.btn_buy_chips = new WSI.Cashier.Controls.uc_round_button();
      this.btn_reserved = new WSI.Cashier.Controls.uc_round_button();
      this.gb_gifts = new WSI.Cashier.Controls.uc_round_panel();
      this.btn_gifts = new WSI.Cashier.Controls.uc_round_button();
      this.lbl_points_balance = new WSI.Cashier.Controls.uc_label();
      this.lbl_level = new WSI.Cashier.Controls.uc_label();
      this.lbl_level_title = new WSI.Cashier.Controls.uc_label();
      this.lbl_points_balance_suffix = new WSI.Cashier.Controls.uc_label();
      this.lbl_points_balance_title = new WSI.Cashier.Controls.uc_label();
      this.groupBox4 = new WSI.Cashier.Controls.uc_round_panel();
      this.btn_win_loss_statement_request = new WSI.Cashier.Controls.uc_round_button();
      this.btn_cash_advance = new WSI.Cashier.Controls.uc_round_button();
      this.btn_transfer_credit = new WSI.Cashier.Controls.uc_round_button();
      this.btn_cancel_promo = new WSI.Cashier.Controls.uc_round_button();
      this.btn_draw = new WSI.Cashier.Controls.uc_round_button();
      this.lbl_deposit = new WSI.Cashier.Controls.uc_label();
      this.lbl_deposit_name = new WSI.Cashier.Controls.uc_label();
      this.uc_card_balance = new WSI.Cashier.uc_card_balance();
      this.btn_credit_add = new WSI.Cashier.Controls.uc_round_button();
      this.btn_credit_redeem = new WSI.Cashier.Controls.uc_round_button();
      this.btn_promotion = new WSI.Cashier.Controls.uc_round_button();
      this.groupBox3 = new WSI.Cashier.Controls.uc_round_panel();
      this.btn_multiple_buckets = new WSI.Cashier.Controls.uc_round_button();
      this.lbl_vip_account = new WSI.Cashier.Controls.uc_label();
      this.btn_recycle_card = new WSI.Cashier.Controls.uc_round_button();
      this.lbl_account_id_value = new WSI.Cashier.Controls.uc_label();
      this.lbl_block_reason = new WSI.Cashier.Controls.uc_label();
      this.lbl_blocked = new WSI.Cashier.Controls.uc_label();
      this.lbl_trackdata = new WSI.Cashier.Controls.uc_label();
      this.btn_print_card = new WSI.Cashier.Controls.uc_round_button();
      this.lbl_name = new WSI.Cashier.Controls.uc_label();
      this.btn_card_block = new WSI.Cashier.Controls.uc_round_button();
      this.pb_blocked = new System.Windows.Forms.PictureBox();
      this.btn_account_edit = new WSI.Cashier.Controls.uc_round_button();
      this.lbl_happy_birthday = new WSI.Cashier.Controls.uc_label();
      this.btn_new_card = new WSI.Cashier.Controls.uc_round_button();
      this.lbl_id = new WSI.Cashier.Controls.uc_label();
      this.lbl_id_title = new WSI.Cashier.Controls.uc_label();
      this.lbl_track_number = new WSI.Cashier.Controls.uc_label();
      this.lbl_birth_title = new WSI.Cashier.Controls.uc_label();
      this.lbl_account_id_name = new WSI.Cashier.Controls.uc_label();
      this.lbl_birth = new WSI.Cashier.Controls.uc_label();
      this.pb_user = new System.Windows.Forms.PictureBox();
      this.lbl_holder_name = new WSI.Cashier.Controls.uc_label();
      this.gb_session = new WSI.Cashier.Controls.uc_round_panel();
      this.lbl_balance_mismatch = new WSI.Cashier.Controls.uc_label();
      this.lbl_terminal = new WSI.Cashier.Controls.uc_label();
      this.lbl_separator_2 = new WSI.Cashier.Controls.uc_label();
      this.lbl_session_initial_balance_value = new WSI.Cashier.Controls.uc_label();
      this.lbl_session_initial_balance = new WSI.Cashier.Controls.uc_label();
      this.lbl_session_final_balance_value = new WSI.Cashier.Controls.uc_label();
      this.lbl_session_final_balance = new WSI.Cashier.Controls.uc_label();
      this.lbl_won_amount = new WSI.Cashier.Controls.uc_label();
      this.lbl_won_count = new WSI.Cashier.Controls.uc_label();
      this.lbl_session_won_plays = new WSI.Cashier.Controls.uc_label();
      this.lbl_played_amount = new WSI.Cashier.Controls.uc_label();
      this.lbl_played_count = new WSI.Cashier.Controls.uc_label();
      this.lbl_session_plays = new WSI.Cashier.Controls.uc_label();
      this.lbl_last_activity_value = new WSI.Cashier.Controls.uc_label();
      this.lbl_last_activity = new WSI.Cashier.Controls.uc_label();
      this.btn_session_log_off = new WSI.Cashier.Controls.uc_round_button();
      this.lbl_terminal_value = new WSI.Cashier.Controls.uc_label();
      this.lbl_card_in_use = new WSI.Cashier.Controls.uc_label();
      this.gb_divisas = new WSI.Cashier.Controls.uc_round_panel();
      this.btn_undo_cash_advance = new WSI.Cashier.Controls.uc_round_button();
      this.btn_undo_last_devolution_currency = new WSI.Cashier.Controls.uc_round_button();
      this.btn_undo_last_exchange_currency = new WSI.Cashier.Controls.uc_round_button();
      this.btn_change_currency = new WSI.Cashier.Controls.uc_round_button();
      this.btn_devolution_currency = new WSI.Cashier.Controls.uc_round_button();
      this.uc_card_reader1 = new WSI.Cashier.uc_card_reader();
      this.gb_loyalty_program = new WSI.Cashier.Controls.uc_round_panel();
      this.btn_vouchers_space = new WSI.Cashier.Controls.uc_round_button();
      this.btn_handpay_mico2 = new WSI.Cashier.Controls.uc_round_button();
      this.uc_round_panel1.SuspendLayout();
      this.gb_chips.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pb_chips)).BeginInit();
      this.gb_gifts.SuspendLayout();
      this.groupBox4.SuspendLayout();
      this.groupBox3.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pb_blocked)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.pb_user)).BeginInit();
      this.gb_session.SuspendLayout();
      this.gb_divisas.SuspendLayout();
      this.gb_loyalty_program.SuspendLayout();
      this.SuspendLayout();
      // 
      // timer1
      // 
      this.timer1.Interval = 500;
      this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
      // 
      // lbl_separator_1
      // 
      this.lbl_separator_1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_separator_1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(95)))), ((int)(((byte)(95)))));
      this.lbl_separator_1.Location = new System.Drawing.Point(-3, 60);
      this.lbl_separator_1.Name = "lbl_separator_1";
      this.lbl_separator_1.Size = new System.Drawing.Size(885, 1);
      this.lbl_separator_1.TabIndex = 2;
      // 
      // uc_round_panel1
      // 
      this.uc_round_panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.uc_round_panel1.BackColor = System.Drawing.Color.Transparent;
      this.uc_round_panel1.BorderColor = System.Drawing.Color.Empty;
      this.uc_round_panel1.Controls.Add(this.btn_disputes);
      this.uc_round_panel1.Controls.Add(this.btn_undo_operation);
      this.uc_round_panel1.Controls.Add(this.btn_handpays);
      this.uc_round_panel1.CornerRadius = 10;
      this.uc_round_panel1.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.uc_round_panel1.HeaderBackColor = System.Drawing.Color.Empty;
      this.uc_round_panel1.HeaderForeColor = System.Drawing.Color.Empty;
      this.uc_round_panel1.HeaderHeight = 0;
      this.uc_round_panel1.HeaderSubText = null;
      this.uc_round_panel1.HeaderText = null;
      this.uc_round_panel1.Location = new System.Drawing.Point(417, 633);
      this.uc_round_panel1.Name = "uc_round_panel1";
      this.uc_round_panel1.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.uc_round_panel1.Size = new System.Drawing.Size(457, 70);
      this.uc_round_panel1.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.WITHOUT_HEAD;
      this.uc_round_panel1.TabIndex = 0;
      // 
      // btn_disputes
      // 
      this.btn_disputes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_disputes.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_disputes.FlatAppearance.BorderSize = 0;
      this.btn_disputes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_disputes.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_disputes.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_disputes.Image = null;
      this.btn_disputes.IsSelected = false;
      this.btn_disputes.Location = new System.Drawing.Point(313, 13);
      this.btn_disputes.Name = "btn_disputes";
      this.btn_disputes.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_disputes.Size = new System.Drawing.Size(136, 48);
      this.btn_disputes.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_disputes.TabIndex = 2;
      this.btn_disputes.Text = "XDISPUTES";
      this.btn_disputes.UseVisualStyleBackColor = false;
      // 
      // btn_undo_operation
      // 
      this.btn_undo_operation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_undo_operation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_undo_operation.FlatAppearance.BorderSize = 0;
      this.btn_undo_operation.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_undo_operation.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_undo_operation.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_undo_operation.Image = null;
      this.btn_undo_operation.IsSelected = false;
      this.btn_undo_operation.Location = new System.Drawing.Point(10, 13);
      this.btn_undo_operation.Name = "btn_undo_operation";
      this.btn_undo_operation.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_undo_operation.Size = new System.Drawing.Size(136, 48);
      this.btn_undo_operation.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_undo_operation.TabIndex = 0;
      this.btn_undo_operation.Text = "XUNDO OPERATION";
      this.btn_undo_operation.UseVisualStyleBackColor = false;
      // 
      // btn_handpays
      // 
      this.btn_handpays.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_handpays.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_handpays.FlatAppearance.BorderSize = 0;
      this.btn_handpays.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_handpays.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_handpays.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_handpays.Image = null;
      this.btn_handpays.IsSelected = false;
      this.btn_handpays.Location = new System.Drawing.Point(162, 13);
      this.btn_handpays.Name = "btn_handpays";
      this.btn_handpays.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_handpays.Size = new System.Drawing.Size(136, 48);
      this.btn_handpays.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_handpays.TabIndex = 1;
      this.btn_handpays.Text = "XHAND PAY";
      this.btn_handpays.UseVisualStyleBackColor = false;
      // 
      // btn_lasts_movements
      // 
      this.btn_lasts_movements.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_lasts_movements.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_lasts_movements.FlatAppearance.BorderSize = 0;
      this.btn_lasts_movements.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_lasts_movements.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_lasts_movements.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_lasts_movements.Image = null;
      this.btn_lasts_movements.IsSelected = false;
      this.btn_lasts_movements.Location = new System.Drawing.Point(265, 334);
      this.btn_lasts_movements.Name = "btn_lasts_movements";
      this.btn_lasts_movements.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_lasts_movements.Size = new System.Drawing.Size(136, 48);
      this.btn_lasts_movements.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_lasts_movements.TabIndex = 2;
      this.btn_lasts_movements.Text = "XLASTS MOVEMENTS";
      this.btn_lasts_movements.UseVisualStyleBackColor = false;
      // 
      // btn_player_browse
      // 
      this.btn_player_browse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_player_browse.BackColor = System.Drawing.Color.Transparent;
      this.btn_player_browse.CornerRadius = 0;
      this.btn_player_browse.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(114)))), ((int)(((byte)(121)))));
      this.btn_player_browse.FlatAppearance.BorderSize = 0;
      this.btn_player_browse.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_player_browse.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_player_browse.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_player_browse.Image = global::WSI.Cashier.Properties.Resources.btnSearch;
      this.btn_player_browse.IsSelected = false;
      this.btn_player_browse.Location = new System.Drawing.Point(817, 3);
      this.btn_player_browse.Margin = new System.Windows.Forms.Padding(0);
      this.btn_player_browse.Name = "btn_player_browse";
      this.btn_player_browse.SelectedColor = System.Drawing.Color.Empty;
      this.btn_player_browse.Size = new System.Drawing.Size(55, 55);
      this.btn_player_browse.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.NONE;
      this.btn_player_browse.TabIndex = 1;
      this.btn_player_browse.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.btn_player_browse.UseVisualStyleBackColor = false;
      // 
      // gb_chips
      // 
      this.gb_chips.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.gb_chips.BackColor = System.Drawing.Color.Transparent;
      this.gb_chips.BorderColor = System.Drawing.Color.Empty;
      this.gb_chips.Controls.Add(this.pb_chips);
      this.gb_chips.Controls.Add(this.btn_chips_sale_register);
      this.gb_chips.Controls.Add(this.btn_sell_chips);
      this.gb_chips.Controls.Add(this.btn_buy_chips);
      this.gb_chips.CornerRadius = 10;
      this.gb_chips.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_chips.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gb_chips.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_chips.HeaderHeight = 35;
      this.gb_chips.HeaderSubText = null;
      this.gb_chips.HeaderText = "XFICHAS";
      this.gb_chips.Location = new System.Drawing.Point(417, 416);
      this.gb_chips.Name = "gb_chips";
      this.gb_chips.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_chips.Size = new System.Drawing.Size(457, 101);
      this.gb_chips.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.gb_chips.TabIndex = 8;
      this.gb_chips.Text = "xChips";
      this.gb_chips.Visible = false;
      // 
      // pb_chips
      // 
      this.pb_chips.Image = ((System.Drawing.Image)(resources.GetObject("pb_chips.Image")));
      this.pb_chips.InitialImage = null;
      this.pb_chips.Location = new System.Drawing.Point(11, 52);
      this.pb_chips.Name = "pb_chips";
      this.pb_chips.Size = new System.Drawing.Size(35, 35);
      this.pb_chips.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
      this.pb_chips.TabIndex = 108;
      this.pb_chips.TabStop = false;
      // 
      // btn_chips_sale_register
      // 
      this.btn_chips_sale_register.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_chips_sale_register.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_chips_sale_register.FlatAppearance.BorderSize = 0;
      this.btn_chips_sale_register.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_chips_sale_register.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_chips_sale_register.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_chips_sale_register.Image = null;
      this.btn_chips_sale_register.IsSelected = false;
      this.btn_chips_sale_register.Location = new System.Drawing.Point(314, 46);
      this.btn_chips_sale_register.Name = "btn_chips_sale_register";
      this.btn_chips_sale_register.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_chips_sale_register.Size = new System.Drawing.Size(136, 48);
      this.btn_chips_sale_register.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_chips_sale_register.TabIndex = 2;
      this.btn_chips_sale_register.Text = "XREGISTER";
      this.btn_chips_sale_register.UseVisualStyleBackColor = false;
      this.btn_chips_sale_register.Visible = false;
      // 
      // btn_sell_chips
      // 
      this.btn_sell_chips.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_sell_chips.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_sell_chips.FlatAppearance.BorderSize = 0;
      this.btn_sell_chips.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_sell_chips.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_sell_chips.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_sell_chips.Image = null;
      this.btn_sell_chips.IsSelected = false;
      this.btn_sell_chips.Location = new System.Drawing.Point(225, 45);
      this.btn_sell_chips.Name = "btn_sell_chips";
      this.btn_sell_chips.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_sell_chips.Size = new System.Drawing.Size(136, 48);
      this.btn_sell_chips.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_sell_chips.TabIndex = 1;
      this.btn_sell_chips.Text = "XSELL CHIPS";
      this.btn_sell_chips.UseVisualStyleBackColor = false;
      this.btn_sell_chips.Visible = false;
      // 
      // btn_buy_chips
      // 
      this.btn_buy_chips.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_buy_chips.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_buy_chips.FlatAppearance.BorderSize = 0;
      this.btn_buy_chips.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_buy_chips.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_buy_chips.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_buy_chips.Image = null;
      this.btn_buy_chips.IsSelected = false;
      this.btn_buy_chips.Location = new System.Drawing.Point(83, 45);
      this.btn_buy_chips.Name = "btn_buy_chips";
      this.btn_buy_chips.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_buy_chips.Size = new System.Drawing.Size(136, 48);
      this.btn_buy_chips.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_buy_chips.TabIndex = 0;
      this.btn_buy_chips.Text = "XBUY CHIPS";
      this.btn_buy_chips.UseVisualStyleBackColor = false;
      this.btn_buy_chips.Visible = false;
      // 
      // btn_reserved
      // 
      this.btn_reserved.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_reserved.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_reserved.FlatAppearance.BorderSize = 0;
      this.btn_reserved.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_reserved.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_reserved.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_reserved.Image = null;
      this.btn_reserved.IsSelected = false;
      this.btn_reserved.Location = new System.Drawing.Point(678, 7);
      this.btn_reserved.Name = "btn_reserved";
      this.btn_reserved.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_reserved.Size = new System.Drawing.Size(136, 48);
      this.btn_reserved.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_reserved.TabIndex = 7;
      this.btn_reserved.Text = "XRESERVED";
      this.btn_reserved.UseVisualStyleBackColor = false;
      // 
      // gb_gifts
      // 
      this.gb_gifts.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.gb_gifts.BackColor = System.Drawing.Color.Transparent;
      this.gb_gifts.BorderColor = System.Drawing.Color.Empty;
      this.gb_gifts.Controls.Add(this.btn_gifts);
      this.gb_gifts.Controls.Add(this.lbl_points_balance);
      this.gb_gifts.Controls.Add(this.lbl_level);
      this.gb_gifts.Controls.Add(this.lbl_level_title);
      this.gb_gifts.Controls.Add(this.lbl_points_balance_suffix);
      this.gb_gifts.Controls.Add(this.lbl_points_balance_title);
      this.gb_gifts.CornerRadius = 10;
      this.gb_gifts.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_gifts.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gb_gifts.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_gifts.HeaderHeight = 35;
      this.gb_gifts.HeaderSubText = null;
      this.gb_gifts.HeaderText = "XPOINTS";
      this.gb_gifts.Location = new System.Drawing.Point(417, 525);
      this.gb_gifts.Name = "gb_gifts";
      this.gb_gifts.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_gifts.Size = new System.Drawing.Size(457, 100);
      this.gb_gifts.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.gb_gifts.TabIndex = 9;
      this.gb_gifts.Text = "xLoyalty Program";
      // 
      // btn_gifts
      // 
      this.btn_gifts.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_gifts.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_gifts.FlatAppearance.BorderSize = 0;
      this.btn_gifts.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_gifts.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_gifts.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_gifts.Image = null;
      this.btn_gifts.IsSelected = false;
      this.btn_gifts.Location = new System.Drawing.Point(314, 45);
      this.btn_gifts.Name = "btn_gifts";
      this.btn_gifts.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_gifts.Size = new System.Drawing.Size(136, 48);
      this.btn_gifts.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_gifts.TabIndex = 6;
      this.btn_gifts.Text = "XGIFTS";
      this.btn_gifts.UseVisualStyleBackColor = false;
      // 
      // lbl_points_balance
      // 
      this.lbl_points_balance.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.lbl_points_balance.BackColor = System.Drawing.Color.Transparent;
      this.lbl_points_balance.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_points_balance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_points_balance.Location = new System.Drawing.Point(138, 65);
      this.lbl_points_balance.Name = "lbl_points_balance";
      this.lbl_points_balance.Size = new System.Drawing.Size(103, 20);
      this.lbl_points_balance.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_points_balance.TabIndex = 3;
      this.lbl_points_balance.Text = "0";
      this.lbl_points_balance.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_level
      // 
      this.lbl_level.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.lbl_level.BackColor = System.Drawing.Color.Transparent;
      this.lbl_level.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_level.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_level.Location = new System.Drawing.Point(21, 65);
      this.lbl_level.Name = "lbl_level";
      this.lbl_level.Size = new System.Drawing.Size(120, 20);
      this.lbl_level.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_level.TabIndex = 2;
      this.lbl_level.Text = "xSilver";
      this.lbl_level.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_level_title
      // 
      this.lbl_level_title.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.lbl_level_title.BackColor = System.Drawing.Color.Transparent;
      this.lbl_level_title.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_level_title.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_level_title.Location = new System.Drawing.Point(21, 45);
      this.lbl_level_title.Name = "lbl_level_title";
      this.lbl_level_title.Size = new System.Drawing.Size(76, 20);
      this.lbl_level_title.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_level_title.TabIndex = 0;
      this.lbl_level_title.Text = "xLevel:";
      this.lbl_level_title.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_points_balance_suffix
      // 
      this.lbl_points_balance_suffix.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.lbl_points_balance_suffix.BackColor = System.Drawing.Color.Transparent;
      this.lbl_points_balance_suffix.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_points_balance_suffix.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_points_balance_suffix.Location = new System.Drawing.Point(240, 65);
      this.lbl_points_balance_suffix.Name = "lbl_points_balance_suffix";
      this.lbl_points_balance_suffix.Size = new System.Drawing.Size(69, 20);
      this.lbl_points_balance_suffix.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_points_balance_suffix.TabIndex = 4;
      this.lbl_points_balance_suffix.Text = "xpuntos";
      this.lbl_points_balance_suffix.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_points_balance_title
      // 
      this.lbl_points_balance_title.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.lbl_points_balance_title.BackColor = System.Drawing.Color.Transparent;
      this.lbl_points_balance_title.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_points_balance_title.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_points_balance_title.Location = new System.Drawing.Point(197, 45);
      this.lbl_points_balance_title.Name = "lbl_points_balance_title";
      this.lbl_points_balance_title.Size = new System.Drawing.Size(76, 20);
      this.lbl_points_balance_title.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_points_balance_title.TabIndex = 1;
      this.lbl_points_balance_title.Text = "xBalance:";
      this.lbl_points_balance_title.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // groupBox4
      // 
      this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.groupBox4.BackColor = System.Drawing.Color.Transparent;
      this.groupBox4.BorderColor = System.Drawing.Color.Empty;
      this.groupBox4.Controls.Add(this.btn_win_loss_statement_request);
      this.groupBox4.Controls.Add(this.btn_cash_advance);
      this.groupBox4.Controls.Add(this.btn_transfer_credit);
      this.groupBox4.Controls.Add(this.btn_cancel_promo);
      this.groupBox4.Controls.Add(this.btn_draw);
      this.groupBox4.Controls.Add(this.lbl_deposit);
      this.groupBox4.Controls.Add(this.lbl_deposit_name);
      this.groupBox4.Controls.Add(this.uc_card_balance);
      this.groupBox4.Controls.Add(this.btn_credit_add);
      this.groupBox4.Controls.Add(this.btn_credit_redeem);
      this.groupBox4.Controls.Add(this.btn_promotion);
      this.groupBox4.CornerRadius = 10;
      this.groupBox4.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.groupBox4.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.groupBox4.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.groupBox4.HeaderHeight = 0;
      this.groupBox4.HeaderSubText = null;
      this.groupBox4.HeaderText = null;
      this.groupBox4.Location = new System.Drawing.Point(417, 70);
      this.groupBox4.Name = "groupBox4";
      this.groupBox4.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.groupBox4.Size = new System.Drawing.Size(457, 338);
      this.groupBox4.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.WITHOUT_HEAD;
      this.groupBox4.TabIndex = 6;
      // 
      // btn_win_loss_statement_request
      // 
      this.btn_win_loss_statement_request.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_win_loss_statement_request.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_win_loss_statement_request.FlatAppearance.BorderSize = 0;
      this.btn_win_loss_statement_request.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_win_loss_statement_request.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_win_loss_statement_request.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_win_loss_statement_request.Image = null;
      this.btn_win_loss_statement_request.IsSelected = false;
      this.btn_win_loss_statement_request.Location = new System.Drawing.Point(10, 226);
      this.btn_win_loss_statement_request.Name = "btn_win_loss_statement_request";
      this.btn_win_loss_statement_request.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_win_loss_statement_request.Size = new System.Drawing.Size(136, 48);
      this.btn_win_loss_statement_request.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_win_loss_statement_request.TabIndex = 6;
      this.btn_win_loss_statement_request.Text = "XWINLOSSSTATEMENT";
      this.btn_win_loss_statement_request.UseVisualStyleBackColor = false;
      // 
      // btn_cash_advance
      // 
      this.btn_cash_advance.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_cash_advance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_cash_advance.FlatAppearance.BorderSize = 0;
      this.btn_cash_advance.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_cash_advance.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_cash_advance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_cash_advance.Image = null;
      this.btn_cash_advance.IsSelected = false;
      this.btn_cash_advance.Location = new System.Drawing.Point(162, 226);
      this.btn_cash_advance.Name = "btn_cash_advance";
      this.btn_cash_advance.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_cash_advance.Size = new System.Drawing.Size(136, 48);
      this.btn_cash_advance.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_cash_advance.TabIndex = 7;
      this.btn_cash_advance.Text = "XCASHADVANCE";
      this.btn_cash_advance.UseVisualStyleBackColor = false;
      // 
      // btn_transfer_credit
      // 
      this.btn_transfer_credit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_transfer_credit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_transfer_credit.FlatAppearance.BorderSize = 0;
      this.btn_transfer_credit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_transfer_credit.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_transfer_credit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_transfer_credit.Image = null;
      this.btn_transfer_credit.IsSelected = false;
      this.btn_transfer_credit.Location = new System.Drawing.Point(313, 118);
      this.btn_transfer_credit.Name = "btn_transfer_credit";
      this.btn_transfer_credit.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_transfer_credit.Size = new System.Drawing.Size(136, 48);
      this.btn_transfer_credit.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_transfer_credit.TabIndex = 4;
      this.btn_transfer_credit.Text = "XTRANSFER CREDIT";
      this.btn_transfer_credit.UseVisualStyleBackColor = false;
      // 
      // btn_cancel_promo
      // 
      this.btn_cancel_promo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_cancel_promo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_cancel_promo.FlatAppearance.BorderSize = 0;
      this.btn_cancel_promo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_cancel_promo.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_cancel_promo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_cancel_promo.Image = null;
      this.btn_cancel_promo.IsSelected = false;
      this.btn_cancel_promo.Location = new System.Drawing.Point(313, 280);
      this.btn_cancel_promo.Name = "btn_cancel_promo";
      this.btn_cancel_promo.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_cancel_promo.Size = new System.Drawing.Size(136, 48);
      this.btn_cancel_promo.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_cancel_promo.TabIndex = 9;
      this.btn_cancel_promo.Text = "XCANCELPROMO";
      this.btn_cancel_promo.UseVisualStyleBackColor = false;
      // 
      // btn_draw
      // 
      this.btn_draw.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_draw.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_draw.FlatAppearance.BorderSize = 0;
      this.btn_draw.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_draw.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_draw.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_draw.Image = null;
      this.btn_draw.IsSelected = false;
      this.btn_draw.Location = new System.Drawing.Point(313, 172);
      this.btn_draw.Name = "btn_draw";
      this.btn_draw.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_draw.Size = new System.Drawing.Size(136, 48);
      this.btn_draw.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_draw.TabIndex = 5;
      this.btn_draw.Text = "XDRAW";
      this.btn_draw.UseVisualStyleBackColor = false;
      // 
      // lbl_deposit
      // 
      this.lbl_deposit.BackColor = System.Drawing.Color.Transparent;
      this.lbl_deposit.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_deposit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_deposit.Location = new System.Drawing.Point(173, 300);
      this.lbl_deposit.Name = "lbl_deposit";
      this.lbl_deposit.Size = new System.Drawing.Size(128, 20);
      this.lbl_deposit.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_deposit.TabIndex = 0;
      this.lbl_deposit.Text = "0,00";
      this.lbl_deposit.TextAlign = System.Drawing.ContentAlignment.BottomRight;
      // 
      // lbl_deposit_name
      // 
      this.lbl_deposit_name.BackColor = System.Drawing.Color.Transparent;
      this.lbl_deposit_name.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_deposit_name.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_deposit_name.Location = new System.Drawing.Point(7, 300);
      this.lbl_deposit_name.Name = "lbl_deposit_name";
      this.lbl_deposit_name.Size = new System.Drawing.Size(148, 20);
      this.lbl_deposit_name.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_deposit_name.TabIndex = 10;
      this.lbl_deposit_name.Text = "xCard Deposit";
      this.lbl_deposit_name.TextAlign = System.Drawing.ContentAlignment.BottomRight;
      // 
      // uc_card_balance
      // 
      this.uc_card_balance.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.uc_card_balance.Cursor = System.Windows.Forms.Cursors.IBeam;
      this.uc_card_balance.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.uc_card_balance.Location = new System.Drawing.Point(3, 4);
      this.uc_card_balance.Name = "uc_card_balance";
      this.uc_card_balance.Size = new System.Drawing.Size(310, 311);
      this.uc_card_balance.TabIndex = 1;
      // 
      // btn_credit_add
      // 
      this.btn_credit_add.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_credit_add.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_credit_add.FlatAppearance.BorderSize = 0;
      this.btn_credit_add.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_credit_add.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_credit_add.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_credit_add.Image = null;
      this.btn_credit_add.IsSelected = false;
      this.btn_credit_add.Location = new System.Drawing.Point(313, 10);
      this.btn_credit_add.Name = "btn_credit_add";
      this.btn_credit_add.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_credit_add.Size = new System.Drawing.Size(136, 48);
      this.btn_credit_add.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_credit_add.TabIndex = 2;
      this.btn_credit_add.Text = "XADD CREDIT";
      this.btn_credit_add.UseVisualStyleBackColor = false;
      // 
      // btn_credit_redeem
      // 
      this.btn_credit_redeem.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_credit_redeem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_credit_redeem.FlatAppearance.BorderSize = 0;
      this.btn_credit_redeem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_credit_redeem.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_credit_redeem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_credit_redeem.Image = null;
      this.btn_credit_redeem.IsSelected = false;
      this.btn_credit_redeem.Location = new System.Drawing.Point(313, 64);
      this.btn_credit_redeem.Name = "btn_credit_redeem";
      this.btn_credit_redeem.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_credit_redeem.Size = new System.Drawing.Size(136, 48);
      this.btn_credit_redeem.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_credit_redeem.TabIndex = 3;
      this.btn_credit_redeem.Text = "XCREDITREDEEM";
      this.btn_credit_redeem.UseVisualStyleBackColor = false;
      // 
      // btn_promotion
      // 
      this.btn_promotion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_promotion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_promotion.FlatAppearance.BorderSize = 0;
      this.btn_promotion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_promotion.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_promotion.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_promotion.Image = null;
      this.btn_promotion.IsSelected = false;
      this.btn_promotion.Location = new System.Drawing.Point(313, 226);
      this.btn_promotion.Name = "btn_promotion";
      this.btn_promotion.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_promotion.Size = new System.Drawing.Size(136, 48);
      this.btn_promotion.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_promotion.TabIndex = 8;
      this.btn_promotion.Text = "XPROMOTION";
      this.btn_promotion.UseVisualStyleBackColor = false;
      // 
      // groupBox3
      // 
      this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.groupBox3.BackColor = System.Drawing.Color.Transparent;
      this.groupBox3.BorderColor = System.Drawing.Color.Empty;
      this.groupBox3.Controls.Add(this.btn_multiple_buckets);
      this.groupBox3.Controls.Add(this.lbl_vip_account);
      this.groupBox3.Controls.Add(this.btn_lasts_movements);
      this.groupBox3.Controls.Add(this.btn_recycle_card);
      this.groupBox3.Controls.Add(this.lbl_account_id_value);
      this.groupBox3.Controls.Add(this.lbl_block_reason);
      this.groupBox3.Controls.Add(this.lbl_blocked);
      this.groupBox3.Controls.Add(this.lbl_trackdata);
      this.groupBox3.Controls.Add(this.btn_print_card);
      this.groupBox3.Controls.Add(this.lbl_name);
      this.groupBox3.Controls.Add(this.btn_card_block);
      this.groupBox3.Controls.Add(this.pb_blocked);
      this.groupBox3.Controls.Add(this.btn_account_edit);
      this.groupBox3.Controls.Add(this.lbl_happy_birthday);
      this.groupBox3.Controls.Add(this.btn_new_card);
      this.groupBox3.Controls.Add(this.lbl_id);
      this.groupBox3.Controls.Add(this.lbl_id_title);
      this.groupBox3.Controls.Add(this.lbl_track_number);
      this.groupBox3.Controls.Add(this.lbl_birth_title);
      this.groupBox3.Controls.Add(this.lbl_account_id_name);
      this.groupBox3.Controls.Add(this.lbl_birth);
      this.groupBox3.Controls.Add(this.pb_user);
      this.groupBox3.Controls.Add(this.lbl_holder_name);
      this.groupBox3.CornerRadius = 10;
      this.groupBox3.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.groupBox3.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.groupBox3.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.groupBox3.HeaderHeight = 0;
      this.groupBox3.HeaderSubText = null;
      this.groupBox3.HeaderText = null;
      this.groupBox3.Location = new System.Drawing.Point(7, 70);
      this.groupBox3.Name = "groupBox3";
      this.groupBox3.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.groupBox3.Size = new System.Drawing.Size(405, 385);
      this.groupBox3.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.WITHOUT_HEAD;
      this.groupBox3.TabIndex = 3;
      // 
      // btn_multiple_buckets
      // 
      this.btn_multiple_buckets.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_multiple_buckets.FlatAppearance.BorderSize = 0;
      this.btn_multiple_buckets.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_multiple_buckets.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_multiple_buckets.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_multiple_buckets.Image = null;
      this.btn_multiple_buckets.IsSelected = false;
      this.btn_multiple_buckets.Location = new System.Drawing.Point(8, 334);
      this.btn_multiple_buckets.Name = "btn_multiple_buckets";
      this.btn_multiple_buckets.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_multiple_buckets.Size = new System.Drawing.Size(132, 48);
      this.btn_multiple_buckets.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_multiple_buckets.TabIndex = 126;
      this.btn_multiple_buckets.Text = "XMULTIPLEBUCKETS";
      this.btn_multiple_buckets.UseVisualStyleBackColor = false;
      this.btn_multiple_buckets.Click += new System.EventHandler(this.btn_multiple_buckets_Click);
      // 
      // lbl_vip_account
      // 
      this.lbl_vip_account.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.lbl_vip_account.CornerRadius = 5;
      this.lbl_vip_account.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_vip_account.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.lbl_vip_account.Location = new System.Drawing.Point(56, 43);
      this.lbl_vip_account.Name = "lbl_vip_account";
      this.lbl_vip_account.Size = new System.Drawing.Size(152, 23);
      this.lbl_vip_account.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_WHITE_BOLD;
      this.lbl_vip_account.TabIndex = 2;
      this.lbl_vip_account.Text = "xVip Account";
      this.lbl_vip_account.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // btn_recycle_card
      // 
      this.btn_recycle_card.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_recycle_card.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_recycle_card.FlatAppearance.BorderSize = 0;
      this.btn_recycle_card.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_recycle_card.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_recycle_card.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_recycle_card.Image = null;
      this.btn_recycle_card.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
      this.btn_recycle_card.IsSelected = false;
      this.btn_recycle_card.Location = new System.Drawing.Point(265, 226);
      this.btn_recycle_card.Name = "btn_recycle_card";
      this.btn_recycle_card.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
      this.btn_recycle_card.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_recycle_card.Size = new System.Drawing.Size(136, 48);
      this.btn_recycle_card.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_recycle_card.TabIndex = 17;
      this.btn_recycle_card.Text = "XRECYCLE CARD";
      this.btn_recycle_card.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.btn_recycle_card.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
      this.btn_recycle_card.UseVisualStyleBackColor = false;
      // 
      // lbl_account_id_value
      // 
      this.lbl_account_id_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_account_id_value.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_account_id_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_account_id_value.Location = new System.Drawing.Point(130, 20);
      this.lbl_account_id_value.Name = "lbl_account_id_value";
      this.lbl_account_id_value.Size = new System.Drawing.Size(99, 21);
      this.lbl_account_id_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_account_id_value.TabIndex = 1;
      this.lbl_account_id_value.Text = "012345";
      this.lbl_account_id_value.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_block_reason
      // 
      this.lbl_block_reason.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_block_reason.BackColor = System.Drawing.Color.Transparent;
      this.lbl_block_reason.Font = new System.Drawing.Font("Open Sans", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_block_reason.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_block_reason.Location = new System.Drawing.Point(282, 27);
      this.lbl_block_reason.Name = "lbl_block_reason";
      this.lbl_block_reason.Size = new System.Drawing.Size(118, 35);
      this.lbl_block_reason.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK_BOLD;
      this.lbl_block_reason.TabIndex = 13;
      this.lbl_block_reason.Text = "xPor Tarjeta Abandonada";
      this.lbl_block_reason.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_blocked
      // 
      this.lbl_blocked.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_blocked.BackColor = System.Drawing.Color.Transparent;
      this.lbl_blocked.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_blocked.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_blocked.Location = new System.Drawing.Point(282, 6);
      this.lbl_blocked.Name = "lbl_blocked";
      this.lbl_blocked.Size = new System.Drawing.Size(118, 21);
      this.lbl_blocked.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_blocked.TabIndex = 12;
      this.lbl_blocked.Text = "xBloqueado";
      this.lbl_blocked.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_trackdata
      // 
      this.lbl_trackdata.BackColor = System.Drawing.Color.Transparent;
      this.lbl_trackdata.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_trackdata.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_trackdata.Location = new System.Drawing.Point(6, 81);
      this.lbl_trackdata.Name = "lbl_trackdata";
      this.lbl_trackdata.Size = new System.Drawing.Size(52, 23);
      this.lbl_trackdata.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_trackdata.TabIndex = 3;
      this.lbl_trackdata.Text = "Tarjeta:";
      this.lbl_trackdata.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // btn_print_card
      // 
      this.btn_print_card.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_print_card.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_print_card.FlatAppearance.BorderSize = 0;
      this.btn_print_card.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_print_card.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_print_card.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_print_card.Image = null;
      this.btn_print_card.IsSelected = false;
      this.btn_print_card.Location = new System.Drawing.Point(265, 280);
      this.btn_print_card.Name = "btn_print_card";
      this.btn_print_card.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_print_card.Size = new System.Drawing.Size(136, 48);
      this.btn_print_card.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_print_card.TabIndex = 18;
      this.btn_print_card.Text = "XPRINT CARD";
      this.btn_print_card.UseVisualStyleBackColor = false;
      // 
      // lbl_name
      // 
      this.lbl_name.BackColor = System.Drawing.Color.Transparent;
      this.lbl_name.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_name.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_name.Location = new System.Drawing.Point(6, 123);
      this.lbl_name.Name = "lbl_name";
      this.lbl_name.Size = new System.Drawing.Size(96, 23);
      this.lbl_name.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_name.TabIndex = 5;
      this.lbl_name.Text = "xName:";
      this.lbl_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // btn_card_block
      // 
      this.btn_card_block.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_card_block.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_card_block.FlatAppearance.BorderSize = 0;
      this.btn_card_block.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_card_block.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_card_block.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_card_block.Image = null;
      this.btn_card_block.IsSelected = false;
      this.btn_card_block.Location = new System.Drawing.Point(265, 172);
      this.btn_card_block.Name = "btn_card_block";
      this.btn_card_block.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_card_block.Size = new System.Drawing.Size(136, 48);
      this.btn_card_block.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_card_block.TabIndex = 16;
      this.btn_card_block.Text = "XUNBLOCK";
      this.btn_card_block.UseVisualStyleBackColor = false;
      // 
      // pb_blocked
      // 
      this.pb_blocked.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.pb_blocked.Enabled = false;
      this.pb_blocked.Location = new System.Drawing.Point(232, 7);
      this.pb_blocked.Name = "pb_blocked";
      this.pb_blocked.Size = new System.Drawing.Size(48, 46);
      this.pb_blocked.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
      this.pb_blocked.TabIndex = 75;
      this.pb_blocked.TabStop = false;
      // 
      // btn_account_edit
      // 
      this.btn_account_edit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_account_edit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_account_edit.FlatAppearance.BorderSize = 0;
      this.btn_account_edit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_account_edit.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_account_edit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_account_edit.Image = null;
      this.btn_account_edit.IsSelected = false;
      this.btn_account_edit.Location = new System.Drawing.Point(265, 118);
      this.btn_account_edit.Name = "btn_account_edit";
      this.btn_account_edit.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_account_edit.Size = new System.Drawing.Size(136, 48);
      this.btn_account_edit.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_account_edit.TabIndex = 15;
      this.btn_account_edit.Text = "XEDIT";
      this.btn_account_edit.UseVisualStyleBackColor = false;
      // 
      // lbl_happy_birthday
      // 
      this.lbl_happy_birthday.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(166)))), ((int)(((byte)(81)))));
      this.lbl_happy_birthday.CornerRadius = 5;
      this.lbl_happy_birthday.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_happy_birthday.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.lbl_happy_birthday.Location = new System.Drawing.Point(6, 250);
      this.lbl_happy_birthday.Name = "lbl_happy_birthday";
      this.lbl_happy_birthday.Size = new System.Drawing.Size(247, 23);
      this.lbl_happy_birthday.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.lbl_happy_birthday.TabIndex = 11;
      this.lbl_happy_birthday.Text = "xHappy Birthday!";
      this.lbl_happy_birthday.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.lbl_happy_birthday.Visible = false;
      // 
      // btn_new_card
      // 
      this.btn_new_card.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_new_card.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_new_card.FlatAppearance.BorderSize = 0;
      this.btn_new_card.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_new_card.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_new_card.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_new_card.Image = null;
      this.btn_new_card.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
      this.btn_new_card.IsSelected = false;
      this.btn_new_card.Location = new System.Drawing.Point(265, 64);
      this.btn_new_card.Name = "btn_new_card";
      this.btn_new_card.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
      this.btn_new_card.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_new_card.Size = new System.Drawing.Size(136, 48);
      this.btn_new_card.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_new_card.TabIndex = 14;
      this.btn_new_card.Text = "XNEW CARD";
      this.btn_new_card.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.btn_new_card.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
      this.btn_new_card.UseVisualStyleBackColor = false;
      this.btn_new_card.Click += new System.EventHandler(this.btn_new_card_Click);
      // 
      // lbl_id
      // 
      this.lbl_id.BackColor = System.Drawing.Color.Transparent;
      this.lbl_id.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_id.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_id.Location = new System.Drawing.Point(6, 196);
      this.lbl_id.Name = "lbl_id";
      this.lbl_id.Size = new System.Drawing.Size(247, 23);
      this.lbl_id.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_id.TabIndex = 8;
      this.lbl_id.Text = "AAAAAAAAAAAAAAAAAAAB";
      this.lbl_id.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_id_title
      // 
      this.lbl_id_title.BackColor = System.Drawing.Color.Transparent;
      this.lbl_id_title.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_id_title.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_id_title.Location = new System.Drawing.Point(6, 171);
      this.lbl_id_title.Name = "lbl_id_title";
      this.lbl_id_title.Size = new System.Drawing.Size(181, 23);
      this.lbl_id_title.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_id_title.TabIndex = 7;
      this.lbl_id_title.Text = "xCarnet de padre:";
      this.lbl_id_title.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_track_number
      // 
      this.lbl_track_number.BackColor = System.Drawing.Color.Transparent;
      this.lbl_track_number.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_track_number.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_track_number.Location = new System.Drawing.Point(60, 82);
      this.lbl_track_number.Name = "lbl_track_number";
      this.lbl_track_number.Size = new System.Drawing.Size(199, 20);
      this.lbl_track_number.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_track_number.TabIndex = 4;
      this.lbl_track_number.Text = "88888888888888888812";
      this.lbl_track_number.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_birth_title
      // 
      this.lbl_birth_title.BackColor = System.Drawing.Color.Transparent;
      this.lbl_birth_title.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_birth_title.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_birth_title.Location = new System.Drawing.Point(6, 226);
      this.lbl_birth_title.Name = "lbl_birth_title";
      this.lbl_birth_title.Size = new System.Drawing.Size(118, 23);
      this.lbl_birth_title.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_birth_title.TabIndex = 9;
      this.lbl_birth_title.Text = "Nacimiento:";
      this.lbl_birth_title.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_account_id_name
      // 
      this.lbl_account_id_name.BackColor = System.Drawing.Color.Transparent;
      this.lbl_account_id_name.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_account_id_name.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_account_id_name.Location = new System.Drawing.Point(55, 19);
      this.lbl_account_id_name.Name = "lbl_account_id_name";
      this.lbl_account_id_name.Size = new System.Drawing.Size(74, 21);
      this.lbl_account_id_name.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_account_id_name.TabIndex = 0;
      this.lbl_account_id_name.Text = "xAccount";
      this.lbl_account_id_name.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_birth
      // 
      this.lbl_birth.BackColor = System.Drawing.Color.Transparent;
      this.lbl_birth.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_birth.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_birth.Location = new System.Drawing.Point(127, 225);
      this.lbl_birth.Name = "lbl_birth";
      this.lbl_birth.Size = new System.Drawing.Size(129, 23);
      this.lbl_birth.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_birth.TabIndex = 10;
      this.lbl_birth.Text = "99 / 99 / 9999";
      this.lbl_birth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // pb_user
      // 
      this.pb_user.Image = global::WSI.Cashier.Resources.ResourceImages.anonymous_user;
      this.pb_user.InitialImage = ((System.Drawing.Image)(resources.GetObject("pb_user.InitialImage")));
      this.pb_user.Location = new System.Drawing.Point(5, 8);
      this.pb_user.Name = "pb_user";
      this.pb_user.Size = new System.Drawing.Size(48, 46);
      this.pb_user.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
      this.pb_user.TabIndex = 72;
      this.pb_user.TabStop = false;
      this.pb_user.Click += new System.EventHandler(this.pb_user_Click);
      // 
      // lbl_holder_name
      // 
      this.lbl_holder_name.BackColor = System.Drawing.Color.Transparent;
      this.lbl_holder_name.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_holder_name.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_holder_name.Location = new System.Drawing.Point(6, 146);
      this.lbl_holder_name.Name = "lbl_holder_name";
      this.lbl_holder_name.Size = new System.Drawing.Size(255, 23);
      this.lbl_holder_name.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_holder_name.TabIndex = 6;
      this.lbl_holder_name.Text = "TTTTTTTTTTTTT";
      this.lbl_holder_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.lbl_holder_name.UseMnemonic = false;
      // 
      // gb_session
      // 
      this.gb_session.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.gb_session.BackColor = System.Drawing.Color.Transparent;
      this.gb_session.BorderColor = System.Drawing.Color.Empty;
      this.gb_session.Controls.Add(this.lbl_balance_mismatch);
      this.gb_session.Controls.Add(this.lbl_terminal);
      this.gb_session.Controls.Add(this.lbl_separator_2);
      this.gb_session.Controls.Add(this.lbl_session_initial_balance_value);
      this.gb_session.Controls.Add(this.lbl_session_initial_balance);
      this.gb_session.Controls.Add(this.lbl_session_final_balance_value);
      this.gb_session.Controls.Add(this.lbl_session_final_balance);
      this.gb_session.Controls.Add(this.lbl_won_amount);
      this.gb_session.Controls.Add(this.lbl_won_count);
      this.gb_session.Controls.Add(this.lbl_session_won_plays);
      this.gb_session.Controls.Add(this.lbl_played_amount);
      this.gb_session.Controls.Add(this.lbl_played_count);
      this.gb_session.Controls.Add(this.lbl_session_plays);
      this.gb_session.Controls.Add(this.lbl_last_activity_value);
      this.gb_session.Controls.Add(this.lbl_last_activity);
      this.gb_session.Controls.Add(this.btn_session_log_off);
      this.gb_session.Controls.Add(this.lbl_terminal_value);
      this.gb_session.Controls.Add(this.lbl_card_in_use);
      this.gb_session.CornerRadius = 10;
      this.gb_session.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_session.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gb_session.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_session.HeaderHeight = 35;
      this.gb_session.HeaderSubText = null;
      this.gb_session.HeaderText = "XLASTPLAYSESSION";
      this.gb_session.Location = new System.Drawing.Point(7, 460);
      this.gb_session.Name = "gb_session";
      this.gb_session.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_session.Size = new System.Drawing.Size(405, 243);
      this.gb_session.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.gb_session.TabIndex = 5;
      this.gb_session.Text = "xLast Game Session";
      // 
      // lbl_balance_mismatch
      // 
      this.lbl_balance_mismatch.BackColor = System.Drawing.Color.Transparent;
      this.lbl_balance_mismatch.CornerRadius = 5;
      this.lbl_balance_mismatch.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_balance_mismatch.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_balance_mismatch.Location = new System.Drawing.Point(20, 204);
      this.lbl_balance_mismatch.Name = "lbl_balance_mismatch";
      this.lbl_balance_mismatch.Size = new System.Drawing.Size(227, 23);
      this.lbl_balance_mismatch.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.lbl_balance_mismatch.TabIndex = 9;
      this.lbl_balance_mismatch.Text = "xBalance Mismatch";
      this.lbl_balance_mismatch.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lbl_terminal
      // 
      this.lbl_terminal.BackColor = System.Drawing.Color.Transparent;
      this.lbl_terminal.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_terminal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_terminal.Location = new System.Drawing.Point(6, 53);
      this.lbl_terminal.Name = "lbl_terminal";
      this.lbl_terminal.Size = new System.Drawing.Size(82, 23);
      this.lbl_terminal.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_terminal.TabIndex = 0;
      this.lbl_terminal.Text = "xTerminal:";
      this.lbl_terminal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_separator_2
      // 
      this.lbl_separator_2.BackColor = System.Drawing.Color.Transparent;
      this.lbl_separator_2.Font = new System.Drawing.Font("Open Sans Semibold", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_separator_2.Location = new System.Drawing.Point(5, 167);
      this.lbl_separator_2.Name = "lbl_separator_2";
      this.lbl_separator_2.Size = new System.Drawing.Size(243, 1);
      this.lbl_separator_2.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.lbl_separator_2.TabIndex = 7;
      this.lbl_separator_2.Text = "0,00";
      this.lbl_separator_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_session_initial_balance_value
      // 
      this.lbl_session_initial_balance_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_session_initial_balance_value.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_session_initial_balance_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_session_initial_balance_value.Location = new System.Drawing.Point(148, 89);
      this.lbl_session_initial_balance_value.Name = "lbl_session_initial_balance_value";
      this.lbl_session_initial_balance_value.Size = new System.Drawing.Size(105, 23);
      this.lbl_session_initial_balance_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_session_initial_balance_value.TabIndex = 10;
      this.lbl_session_initial_balance_value.Text = "TTTTTTTTTTTTT";
      this.lbl_session_initial_balance_value.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_session_initial_balance
      // 
      this.lbl_session_initial_balance.BackColor = System.Drawing.Color.Transparent;
      this.lbl_session_initial_balance.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_session_initial_balance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_session_initial_balance.Location = new System.Drawing.Point(4, 89);
      this.lbl_session_initial_balance.Name = "lbl_session_initial_balance";
      this.lbl_session_initial_balance.Size = new System.Drawing.Size(90, 23);
      this.lbl_session_initial_balance.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_session_initial_balance.TabIndex = 2;
      this.lbl_session_initial_balance.Text = "xInitial Bal.:";
      this.lbl_session_initial_balance.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_session_final_balance_value
      // 
      this.lbl_session_final_balance_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_session_final_balance_value.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_session_final_balance_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_session_final_balance_value.Location = new System.Drawing.Point(148, 178);
      this.lbl_session_final_balance_value.Name = "lbl_session_final_balance_value";
      this.lbl_session_final_balance_value.Size = new System.Drawing.Size(105, 23);
      this.lbl_session_final_balance_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_session_final_balance_value.TabIndex = 13;
      this.lbl_session_final_balance_value.Text = "TTTTTTTTTTTTT";
      this.lbl_session_final_balance_value.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_session_final_balance
      // 
      this.lbl_session_final_balance.BackColor = System.Drawing.Color.Transparent;
      this.lbl_session_final_balance.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_session_final_balance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_session_final_balance.Location = new System.Drawing.Point(4, 178);
      this.lbl_session_final_balance.Name = "lbl_session_final_balance";
      this.lbl_session_final_balance.Size = new System.Drawing.Size(90, 23);
      this.lbl_session_final_balance.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_session_final_balance.TabIndex = 8;
      this.lbl_session_final_balance.Text = "xFinal Bal:";
      this.lbl_session_final_balance.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_won_amount
      // 
      this.lbl_won_amount.BackColor = System.Drawing.Color.Transparent;
      this.lbl_won_amount.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_won_amount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_won_amount.Location = new System.Drawing.Point(148, 147);
      this.lbl_won_amount.Name = "lbl_won_amount";
      this.lbl_won_amount.Size = new System.Drawing.Size(105, 23);
      this.lbl_won_amount.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_won_amount.TabIndex = 12;
      this.lbl_won_amount.Text = "TTTTTTTTTTTTT";
      this.lbl_won_amount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_won_count
      // 
      this.lbl_won_count.BackColor = System.Drawing.Color.Transparent;
      this.lbl_won_count.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_won_count.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_won_count.Location = new System.Drawing.Point(95, 147);
      this.lbl_won_count.Name = "lbl_won_count";
      this.lbl_won_count.Size = new System.Drawing.Size(43, 23);
      this.lbl_won_count.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_won_count.TabIndex = 6;
      this.lbl_won_count.Text = "99,999";
      this.lbl_won_count.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      this.lbl_won_count.Visible = false;
      // 
      // lbl_session_won_plays
      // 
      this.lbl_session_won_plays.BackColor = System.Drawing.Color.Transparent;
      this.lbl_session_won_plays.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_session_won_plays.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_session_won_plays.Location = new System.Drawing.Point(4, 147);
      this.lbl_session_won_plays.Name = "lbl_session_won_plays";
      this.lbl_session_won_plays.Size = new System.Drawing.Size(90, 23);
      this.lbl_session_won_plays.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_session_won_plays.TabIndex = 5;
      this.lbl_session_won_plays.Text = "xWon:";
      this.lbl_session_won_plays.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_played_amount
      // 
      this.lbl_played_amount.BackColor = System.Drawing.Color.Transparent;
      this.lbl_played_amount.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_played_amount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_played_amount.Location = new System.Drawing.Point(148, 118);
      this.lbl_played_amount.Name = "lbl_played_amount";
      this.lbl_played_amount.Size = new System.Drawing.Size(105, 23);
      this.lbl_played_amount.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_played_amount.TabIndex = 11;
      this.lbl_played_amount.Text = "TTTTTTTTTTTTT";
      this.lbl_played_amount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_played_count
      // 
      this.lbl_played_count.BackColor = System.Drawing.Color.Transparent;
      this.lbl_played_count.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_played_count.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_played_count.Location = new System.Drawing.Point(95, 118);
      this.lbl_played_count.Name = "lbl_played_count";
      this.lbl_played_count.Size = new System.Drawing.Size(43, 23);
      this.lbl_played_count.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_played_count.TabIndex = 4;
      this.lbl_played_count.Text = "99,999";
      this.lbl_played_count.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      this.lbl_played_count.Visible = false;
      // 
      // lbl_session_plays
      // 
      this.lbl_session_plays.BackColor = System.Drawing.Color.Transparent;
      this.lbl_session_plays.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_session_plays.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_session_plays.Location = new System.Drawing.Point(4, 118);
      this.lbl_session_plays.Name = "lbl_session_plays";
      this.lbl_session_plays.Size = new System.Drawing.Size(90, 23);
      this.lbl_session_plays.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_session_plays.TabIndex = 3;
      this.lbl_session_plays.Text = "xPlays:";
      this.lbl_session_plays.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_last_activity_value
      // 
      this.lbl_last_activity_value.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_last_activity_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_last_activity_value.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_last_activity_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_last_activity_value.Location = new System.Drawing.Point(247, 205);
      this.lbl_last_activity_value.Name = "lbl_last_activity_value";
      this.lbl_last_activity_value.Size = new System.Drawing.Size(154, 22);
      this.lbl_last_activity_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_last_activity_value.TabIndex = 17;
      this.lbl_last_activity_value.Text = "TTTTTTTTTTTTT";
      this.lbl_last_activity_value.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lbl_last_activity
      // 
      this.lbl_last_activity.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_last_activity.BackColor = System.Drawing.Color.Transparent;
      this.lbl_last_activity.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_last_activity.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_last_activity.Location = new System.Drawing.Point(274, 181);
      this.lbl_last_activity.Name = "lbl_last_activity";
      this.lbl_last_activity.Size = new System.Drawing.Size(128, 16);
      this.lbl_last_activity.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_last_activity.TabIndex = 16;
      this.lbl_last_activity.Text = "xLast Activity";
      this.lbl_last_activity.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // btn_session_log_off
      // 
      this.btn_session_log_off.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_session_log_off.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_session_log_off.FlatAppearance.BorderSize = 0;
      this.btn_session_log_off.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_session_log_off.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_session_log_off.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_session_log_off.Image = null;
      this.btn_session_log_off.IsSelected = false;
      this.btn_session_log_off.Location = new System.Drawing.Point(273, 89);
      this.btn_session_log_off.Name = "btn_session_log_off";
      this.btn_session_log_off.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_session_log_off.Size = new System.Drawing.Size(128, 48);
      this.btn_session_log_off.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_session_log_off.TabIndex = 14;
      this.btn_session_log_off.Text = "XLOG OFF SESSION";
      this.btn_session_log_off.UseVisualStyleBackColor = false;
      // 
      // lbl_terminal_value
      // 
      this.lbl_terminal_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_terminal_value.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_terminal_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_terminal_value.Location = new System.Drawing.Point(84, 55);
      this.lbl_terminal_value.Name = "lbl_terminal_value";
      this.lbl_terminal_value.Size = new System.Drawing.Size(315, 23);
      this.lbl_terminal_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_terminal_value.TabIndex = 1;
      this.lbl_terminal_value.Text = "TTTTTTTTTTTTTttttttttttttttt";
      this.lbl_terminal_value.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_card_in_use
      // 
      this.lbl_card_in_use.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_card_in_use.BackColor = System.Drawing.Color.Transparent;
      this.lbl_card_in_use.CornerRadius = 5;
      this.lbl_card_in_use.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_card_in_use.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_card_in_use.Location = new System.Drawing.Point(274, 146);
      this.lbl_card_in_use.Name = "lbl_card_in_use";
      this.lbl_card_in_use.Size = new System.Drawing.Size(121, 23);
      this.lbl_card_in_use.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_card_in_use.TabIndex = 15;
      this.lbl_card_in_use.Text = "xIN USE";
      this.lbl_card_in_use.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // gb_divisas
      // 
      this.gb_divisas.BackColor = System.Drawing.Color.Transparent;
      this.gb_divisas.BorderColor = System.Drawing.Color.Empty;
      this.gb_divisas.Controls.Add(this.btn_undo_cash_advance);
      this.gb_divisas.Controls.Add(this.btn_undo_last_devolution_currency);
      this.gb_divisas.Controls.Add(this.btn_undo_last_exchange_currency);
      this.gb_divisas.Controls.Add(this.btn_change_currency);
      this.gb_divisas.Controls.Add(this.btn_devolution_currency);
      this.gb_divisas.CornerRadius = 10;
      this.gb_divisas.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_divisas.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_divisas.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gb_divisas.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_divisas.HeaderHeight = 18;
      this.gb_divisas.HeaderSubText = null;
      this.gb_divisas.HeaderText = null;
      this.gb_divisas.Location = new System.Drawing.Point(3, 404);
      this.gb_divisas.Name = "gb_divisas";
      this.gb_divisas.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_divisas.Size = new System.Drawing.Size(322, 208);
      this.gb_divisas.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.MIN_HEAD;
      this.gb_divisas.TabIndex = 18;
      this.gb_divisas.Visible = false;
      // 
      // btn_undo_cash_advance
      // 
      this.btn_undo_cash_advance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_undo_cash_advance.FlatAppearance.BorderSize = 0;
      this.btn_undo_cash_advance.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_undo_cash_advance.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_undo_cash_advance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_undo_cash_advance.Image = null;
      this.btn_undo_cash_advance.IsSelected = false;
      this.btn_undo_cash_advance.Location = new System.Drawing.Point(97, 152);
      this.btn_undo_cash_advance.Name = "btn_undo_cash_advance";
      this.btn_undo_cash_advance.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_undo_cash_advance.Size = new System.Drawing.Size(136, 48);
      this.btn_undo_cash_advance.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_undo_cash_advance.TabIndex = 4;
      this.btn_undo_cash_advance.Text = "XUNDO LAST CASH ADVANCE";
      this.btn_undo_cash_advance.UseVisualStyleBackColor = false;
      this.btn_undo_cash_advance.Click += new System.EventHandler(this.btn_undo_cash_advance_Click);
      // 
      // btn_undo_last_devolution_currency
      // 
      this.btn_undo_last_devolution_currency.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_undo_last_devolution_currency.FlatAppearance.BorderSize = 0;
      this.btn_undo_last_devolution_currency.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_undo_last_devolution_currency.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_undo_last_devolution_currency.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_undo_last_devolution_currency.Image = null;
      this.btn_undo_last_devolution_currency.IsSelected = false;
      this.btn_undo_last_devolution_currency.Location = new System.Drawing.Point(16, 95);
      this.btn_undo_last_devolution_currency.Name = "btn_undo_last_devolution_currency";
      this.btn_undo_last_devolution_currency.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_undo_last_devolution_currency.Size = new System.Drawing.Size(136, 48);
      this.btn_undo_last_devolution_currency.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_undo_last_devolution_currency.TabIndex = 2;
      this.btn_undo_last_devolution_currency.Text = "XUNDODEVOLUTION";
      this.btn_undo_last_devolution_currency.UseVisualStyleBackColor = false;
      this.btn_undo_last_devolution_currency.Click += new System.EventHandler(this.btn_undo_last_devolution_currency_Click);
      // 
      // btn_undo_last_exchange_currency
      // 
      this.btn_undo_last_exchange_currency.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_undo_last_exchange_currency.FlatAppearance.BorderSize = 0;
      this.btn_undo_last_exchange_currency.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_undo_last_exchange_currency.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_undo_last_exchange_currency.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_undo_last_exchange_currency.Image = null;
      this.btn_undo_last_exchange_currency.IsSelected = false;
      this.btn_undo_last_exchange_currency.Location = new System.Drawing.Point(16, 29);
      this.btn_undo_last_exchange_currency.Name = "btn_undo_last_exchange_currency";
      this.btn_undo_last_exchange_currency.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_undo_last_exchange_currency.Size = new System.Drawing.Size(136, 48);
      this.btn_undo_last_exchange_currency.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_undo_last_exchange_currency.TabIndex = 0;
      this.btn_undo_last_exchange_currency.Text = "XUNDOCHANGE";
      this.btn_undo_last_exchange_currency.UseVisualStyleBackColor = false;
      this.btn_undo_last_exchange_currency.Click += new System.EventHandler(this.btn_undo_last_exchange_currency_Click);
      // 
      // btn_change_currency
      // 
      this.btn_change_currency.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_change_currency.FlatAppearance.BorderSize = 0;
      this.btn_change_currency.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_change_currency.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_change_currency.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_change_currency.Image = null;
      this.btn_change_currency.IsSelected = false;
      this.btn_change_currency.Location = new System.Drawing.Point(169, 31);
      this.btn_change_currency.Name = "btn_change_currency";
      this.btn_change_currency.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_change_currency.Size = new System.Drawing.Size(136, 48);
      this.btn_change_currency.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_change_currency.TabIndex = 1;
      this.btn_change_currency.Text = "XCHANGECURRENCY";
      this.btn_change_currency.UseVisualStyleBackColor = false;
      this.btn_change_currency.Click += new System.EventHandler(this.btn_change_currency_Click);
      // 
      // btn_devolution_currency
      // 
      this.btn_devolution_currency.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_devolution_currency.FlatAppearance.BorderSize = 0;
      this.btn_devolution_currency.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_devolution_currency.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_devolution_currency.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_devolution_currency.Image = null;
      this.btn_devolution_currency.IsSelected = false;
      this.btn_devolution_currency.Location = new System.Drawing.Point(169, 96);
      this.btn_devolution_currency.Name = "btn_devolution_currency";
      this.btn_devolution_currency.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_devolution_currency.Size = new System.Drawing.Size(136, 48);
      this.btn_devolution_currency.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_devolution_currency.TabIndex = 3;
      this.btn_devolution_currency.Text = "XDEVOLUTIONCURRENCY";
      this.btn_devolution_currency.UseVisualStyleBackColor = false;
      this.btn_devolution_currency.Click += new System.EventHandler(this.btn_devolution_currency_Click);
      // 
      // uc_card_reader1
      // 
      this.uc_card_reader1.ErrorMessageBelow = false;
      this.uc_card_reader1.InvalidCardTextVisible = false;
      this.uc_card_reader1.Location = new System.Drawing.Point(0, 0);
      this.uc_card_reader1.MaximumSize = new System.Drawing.Size(800, 60);
      this.uc_card_reader1.MinimumSize = new System.Drawing.Size(800, 60);
      this.uc_card_reader1.Name = "uc_card_reader1";
      this.uc_card_reader1.Size = new System.Drawing.Size(800, 60);
      this.uc_card_reader1.TabIndex = 0;
      // 
      // gb_loyalty_program
      // 
      this.gb_loyalty_program.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.gb_loyalty_program.BackColor = System.Drawing.Color.Transparent;
      this.gb_loyalty_program.BorderColor = System.Drawing.Color.Empty;
      this.gb_loyalty_program.Controls.Add(this.btn_vouchers_space);
      this.gb_loyalty_program.CornerRadius = 10;
      this.gb_loyalty_program.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_loyalty_program.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gb_loyalty_program.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_loyalty_program.HeaderHeight = 35;
      this.gb_loyalty_program.HeaderSubText = null;
      this.gb_loyalty_program.HeaderText = "XLOYALTYPROGRAM";
      this.gb_loyalty_program.Location = new System.Drawing.Point(418, 525);
      this.gb_loyalty_program.Name = "gb_loyalty_program";
      this.gb_loyalty_program.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_loyalty_program.Size = new System.Drawing.Size(457, 100);
      this.gb_loyalty_program.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.gb_loyalty_program.TabIndex = 19;
      this.gb_loyalty_program.Text = "xLoyalty Program";
      // 
      // btn_vouchers_space
      // 
      this.btn_vouchers_space.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_vouchers_space.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_vouchers_space.FlatAppearance.BorderSize = 0;
      this.btn_vouchers_space.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_vouchers_space.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_vouchers_space.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_vouchers_space.Image = null;
      this.btn_vouchers_space.IsSelected = false;
      this.btn_vouchers_space.Location = new System.Drawing.Point(162, 45);
      this.btn_vouchers_space.Name = "btn_vouchers_space";
      this.btn_vouchers_space.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_vouchers_space.Size = new System.Drawing.Size(136, 48);
      this.btn_vouchers_space.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_vouchers_space.TabIndex = 5;
      this.btn_vouchers_space.Text = "XVOUCHERSPACE";
      this.btn_vouchers_space.UseVisualStyleBackColor = false;
      // 
      // btn_handpay_mico2
      // 
      this.btn_handpay_mico2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_handpay_mico2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_handpay_mico2.FlatAppearance.BorderSize = 0;
      this.btn_handpay_mico2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_handpay_mico2.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_handpay_mico2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_handpay_mico2.Image = null;
      this.btn_handpay_mico2.IsSelected = false;
      this.btn_handpay_mico2.Location = new System.Drawing.Point(536, 7);
      this.btn_handpay_mico2.Name = "btn_handpay_mico2";
      this.btn_handpay_mico2.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_handpay_mico2.Size = new System.Drawing.Size(136, 48);
      this.btn_handpay_mico2.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_handpay_mico2.TabIndex = 3;
      this.btn_handpay_mico2.Text = "XHAND PAY";
      this.btn_handpay_mico2.UseVisualStyleBackColor = false;
      this.btn_handpay_mico2.Click += new System.EventHandler(this.btn_handpay_mico2_Click);
      // 
      // uc_card
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.Controls.Add(this.btn_handpay_mico2);
      this.Controls.Add(this.gb_loyalty_program);
      this.Controls.Add(this.uc_round_panel1);
      this.Controls.Add(this.btn_player_browse);
      this.Controls.Add(this.lbl_separator_1);
      this.Controls.Add(this.gb_chips);
      this.Controls.Add(this.btn_reserved);
      this.Controls.Add(this.gb_gifts);
      this.Controls.Add(this.groupBox4);
      this.Controls.Add(this.groupBox3);
      this.Controls.Add(this.uc_card_reader1);
      this.Controls.Add(this.gb_session);
      this.Controls.Add(this.gb_divisas);
      this.Name = "uc_card";
      this.Size = new System.Drawing.Size(884, 715);
      this.uc_round_panel1.ResumeLayout(false);
      this.gb_chips.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.pb_chips)).EndInit();
      this.gb_gifts.ResumeLayout(false);
      this.groupBox4.ResumeLayout(false);
      this.groupBox3.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.pb_blocked)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.pb_user)).EndInit();
      this.gb_session.ResumeLayout(false);
      this.gb_divisas.ResumeLayout(false);
      this.gb_loyalty_program.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Timer timer1;
    private WSI.Cashier.Controls.uc_round_button btn_lasts_movements;
    private WSI.Cashier.Controls.uc_round_panel groupBox4;
    private uc_card_balance uc_card_balance;
    private WSI.Cashier.Controls.uc_round_button btn_credit_add;
    private WSI.Cashier.Controls.uc_round_button btn_credit_redeem;
    private WSI.Cashier.Controls.uc_round_button btn_promotion;
    private WSI.Cashier.Controls.uc_round_panel groupBox3;
    private WSI.Cashier.Controls.uc_round_button btn_new_card;
    private WSI.Cashier.Controls.uc_label lbl_track_number;
    private System.Windows.Forms.PictureBox pb_user;
    private WSI.Cashier.Controls.uc_round_button btn_account_edit;
    private WSI.Cashier.Controls.uc_label lbl_holder_name;
    private System.Windows.Forms.PictureBox pb_blocked;
    private WSI.Cashier.Controls.uc_round_button btn_card_block;
    private WSI.Cashier.Controls.uc_round_panel gb_session;
    private WSI.Cashier.Controls.uc_label lbl_session_initial_balance_value;
    private WSI.Cashier.Controls.uc_label lbl_session_initial_balance;
    private WSI.Cashier.Controls.uc_label lbl_session_final_balance_value;
    private WSI.Cashier.Controls.uc_label lbl_session_final_balance;
    private WSI.Cashier.Controls.uc_label lbl_won_amount;
    private WSI.Cashier.Controls.uc_label lbl_won_count;
    private WSI.Cashier.Controls.uc_label lbl_session_won_plays;
    private WSI.Cashier.Controls.uc_label lbl_played_amount;
    private WSI.Cashier.Controls.uc_label lbl_played_count;
    private WSI.Cashier.Controls.uc_label lbl_session_plays;
    private WSI.Cashier.Controls.uc_label lbl_last_activity_value;
    private WSI.Cashier.Controls.uc_label lbl_last_activity;
    private WSI.Cashier.Controls.uc_round_button btn_session_log_off;
    private WSI.Cashier.Controls.uc_label lbl_terminal_value;
    private WSI.Cashier.Controls.uc_label lbl_card_in_use;
    private WSI.Cashier.Controls.uc_label lbl_account_id_value;
    private WSI.Cashier.Controls.uc_label lbl_account_id_name;
    private uc_card_reader uc_card_reader1;
    private WSI.Cashier.Controls.uc_label lbl_separator_2;
    private WSI.Cashier.Controls.uc_label lbl_deposit_name;
    private WSI.Cashier.Controls.uc_label lbl_deposit;
    private WSI.Cashier.Controls.uc_round_button btn_print_card;
    private WSI.Cashier.Controls.uc_label lbl_id_title;
    private WSI.Cashier.Controls.uc_label lbl_id;
    private WSI.Cashier.Controls.uc_label lbl_birth_title;
    private WSI.Cashier.Controls.uc_label lbl_birth;
    private WSI.Cashier.Controls.uc_round_button btn_draw;
    private WSI.Cashier.Controls.uc_round_button btn_handpays;
    private WSI.Cashier.Controls.uc_label lbl_happy_birthday;
    private WSI.Cashier.Controls.uc_label lbl_name;
    private WSI.Cashier.Controls.uc_label lbl_trackdata;
    private WSI.Cashier.Controls.uc_label lbl_blocked;
    private WSI.Cashier.Controls.uc_label lbl_terminal;
    private WSI.Cashier.Controls.uc_round_panel gb_gifts;
    private WSI.Cashier.Controls.uc_round_button btn_gifts;
    private WSI.Cashier.Controls.uc_label lbl_block_reason;
    private WSI.Cashier.Controls.uc_round_button btn_player_browse;
    private WSI.Cashier.Controls.uc_label lbl_balance_mismatch;
    private WSI.Cashier.Controls.uc_round_button btn_recycle_card;
    private WSI.Cashier.Controls.uc_round_button btn_cancel_promo;
    private WSI.Cashier.Controls.uc_round_button btn_undo_operation;
    private WSI.Cashier.Controls.uc_round_button btn_transfer_credit;
    private WSI.Cashier.Controls.uc_label lbl_vip_account;
    private WSI.Cashier.Controls.uc_round_button btn_sell_chips;
    private WSI.Cashier.Controls.uc_round_button btn_buy_chips;
    private WSI.Cashier.Controls.uc_round_panel gb_chips;
    private WSI.Cashier.Controls.uc_round_button btn_chips_sale_register;
    private System.Windows.Forms.PictureBox pb_chips;
    private WSI.Cashier.Controls.uc_round_button btn_cash_advance;
    private WSI.Cashier.Controls.uc_round_button btn_win_loss_statement_request;
    private System.Windows.Forms.Label lbl_separator_1;
    private Controls.uc_label lbl_points_balance;
    private Controls.uc_label lbl_level;
    private Controls.uc_label lbl_level_title;
    private Controls.uc_label lbl_points_balance_suffix;
    private Controls.uc_label lbl_points_balance_title;
    private Controls.uc_round_panel uc_round_panel1;
    private WSI.Cashier.Controls.uc_round_button btn_reserved;
    private Controls.uc_round_button btn_multiple_buckets;
    private Controls.uc_round_button btn_disputes;
    private Controls.uc_round_panel gb_divisas;
    private Controls.uc_round_button btn_undo_last_devolution_currency;
    private Controls.uc_round_button btn_undo_last_exchange_currency;
    private Controls.uc_round_button btn_change_currency;
    private Controls.uc_round_button btn_devolution_currency;
    private Controls.uc_round_button btn_undo_cash_advance;
    private Controls.uc_round_panel gb_loyalty_program;
    private Controls.uc_round_button btn_vouchers_space;
    private Controls.uc_round_button btn_handpay_mico2;

  }
}
