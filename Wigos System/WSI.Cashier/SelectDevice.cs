﻿using System;
using System.Windows.Forms;
using WSI.Cashier.Controls;
using WSI.IDCamera;
using WSI.IDCamera.Model;
using WSI.IDCamera.Model.Exceptions;
using WSI.Common;

namespace WSI.Cashier
{


  public partial class SelectDevice : frm_base, ISelectDevice
  {
    //Private
    private CameraModule cameraModule = null;

    //Constructor
    public SelectDevice()
    {
      InitializeComponent();
      btnSelecteDevice.Text= Common.Resource.String("STR_FORMAT_GENERAL_BUTTONS_SELECT");
      FormTitle=Common.Resource.String("STR_SELECT_DEVICE");
    }

    //Methods
    public CameraDevice GetDevice(CameraModule CameraModule, Form frm)
    {
      this.cameraModule = CameraModule;
      var _dialog_result = this.ShowDialog(frm);

      if (_dialog_result == DialogResult.OK)
      {
        return (CameraDevice)lstDevices.SelectedItem;
      }
      else if (_dialog_result == DialogResult.Abort)
      {
        throw new NoCameraDevicesAvailable("Failed to find any camera devices", new Exception());
      }
      return null;
    }

    private void listBox1_DoubleClick(object sender, EventArgs e)
    {
      if (lstDevices.SelectedItem != null)
      {
        this.DialogResult = DialogResult.OK;

        Misc.WriteLog("[FORM CLOSE] frm_selectdevice (listbox1_doubleclick)", Log.Type.Message);
        this.Close();
      }
    }
    private void SelectDevice_Load(object sender, EventArgs e)
    {
      lstDevices.ValueMember = "Name";
      lstDevices.Items.Clear();
      foreach (var device in cameraModule.GetAllAttatchedDevices(this))
      {
        lstDevices.Items.Add(device);
      }
      if (lstDevices.Items.Count == 1)
      {
        lstDevices.SelectedIndex = 0;
        DialogResult = DialogResult.OK;
      }
      else if (lstDevices.Items.Count == 0)
      {
        DialogResult = DialogResult.Abort;
      }
      //if (lstDevices.Items.Count == 0)
      //{
      //  throw new NoCameraDevicesAvailable("Failed to find any camera devices",new Exception());
      //}
    }
  }
}
