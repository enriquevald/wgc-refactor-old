//------------------------------------------------------------------------------
// Copyright � 2010 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Draws.cs
// 
//   DESCRIPTION: Implements the following classes:
//                1. Draws
//
//        AUTHOR: MBF
// 
// CREATION DATE: 09-JUN-2010
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 09-JUN-2010 MBF    First release.
// 14-JUN-2010 RCI    Insert cashier and account movement when ticket is created.
// 20-OCT-2011 MPO    add new features: Limit per operation and first cash in constrained
// 21-DEC-2011 RCI    Added Bingo format flag
// 02-JAN-2012 JMM    CashInPosteriori draws loading enabled
// 30-MAY-2012 SSC    Moved DrawTicket and DrawTicketLogic classes to WSI.Common 
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using WSI.Common;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Globalization;
using System.Windows.Forms;

namespace WSI.Cashier
{

  public class DrawTicketLogicCashier : DrawTicketLogic
  {

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE: Returns the error message string corresponding to DrawTicket.CreationCode.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - DrawTicket
    //
    //      - OUTPUT:
    //          - CashierImage
    //
    // RETURNS:
    //      - String with the error message
    // 
    //   NOTES:
    public static String GetMsgError(DrawTicket DrawTicket, out Images.CashierImage CashierImage)
    {
      String _str;

      switch (DrawTicket.CreationCode)
      {
        case GetTicketCodes.ERROR:
          _str = Resource.String("STR_VOUCHER_ERROR_MSG");
          CashierImage = Images.CashierImage.Error;
          break;

        case GetTicketCodes.NO_SALES:
          _str = Resource.String("STR_VOUCHER_ERROR_MSG_SALES");
          CashierImage = Images.CashierImage.Information;
          break;

        case GetTicketCodes.NO_DRAW:
          _str = Resource.String("STR_VOUCHER_ERROR_MSG_DRAW");
          CashierImage = Images.CashierImage.Information;
          break;

        case GetTicketCodes.LAST_NUMBER_ERROR:
          _str = Resource.String("STR_VOUCHER_ERROR_MSG_LAST_NUMBER");
          CashierImage = Images.CashierImage.Error;
         break;

        case GetTicketCodes.NUMBERS_ERROR:
          _str = Resource.String("STR_VOUCHER_ERROR_MSG_NUMBERS");
          CashierImage = Images.CashierImage.Error;
          break;

        case GetTicketCodes.TICKET_ERROR:
          _str = Resource.String("STR_VOUCHER_ERROR_MSG_TICKET");
          CashierImage = Images.CashierImage.Error;
          break;

        case GetTicketCodes.LIMIT_REACHED:
          _str = Resource.String("STR_VOUCHER_ERROR_MSG_LIMIT_REACHED", DrawTicket.LimitPerDayAndPlayer);
          CashierImage = Images.CashierImage.Information;
          break;

        case GetTicketCodes.DRAW_MAX_REACHED:
          _str = Resource.String("STR_VOUCHER_ERROR_MSG_DRAW_MAX_REACHED");
          CashierImage = Images.CashierImage.Information;
          break;

        case GetTicketCodes.PRESELECTED_DRAW_CHANGED:
          _str = Resource.String("STR_VOUCHER_ERROR_MSG_PRESELECTED_DRAW_CHANGED");
          CashierImage = Images.CashierImage.Error;
          break;

        default:
          Log.Error("frm_draw_voucher. CreationCode: " + DrawTicket.CreationCode);
          _str = Resource.String("STR_VOUCHER_ERROR_MSG");
          CashierImage = Images.CashierImage.Error;
          break;
      }

      return _str;
    } // GetMsgError

    #endregion

  }
}
