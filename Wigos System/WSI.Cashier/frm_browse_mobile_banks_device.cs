//------------------------------------------------------------------------------
// Copyright � 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_browse_mobile_banks_device.cs
// 
//   DESCRIPTION: Implements the following classes:
//                1. frm_browse_mobile_banks_device
//
//        AUTHOR: Joan Benito
// 
// CREATION DATE: 10-NOV-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 10-NOV-2017 JBM     First release.
// 22-NOV-2017 FGB     WIGOS-3372: MobiBank: Cashier management - Main screen (when there's not cashier session id be able to open mobile banks browser though the search button).
// 29-MAY-2018 DMT     PBI32633:WIGOS GUI - Mobibank - V1 MAIN-DEV-MOVIBANK WIGOS-12304 WigosGUI - User can access to Movibank button when has permissions disabled.
//------------------------------------------------------------------------------
using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WSI.Common;

namespace WSI.Cashier
{
  public partial class frm_browse_mobile_banks_device : WSI.Cashier.Controls.frm_base
  {
    #region Constants
    //private const Int32 GRID_COLUMN_MB_USER_ID = 0;
    //private const Int32 GRID_COLUMN_MB_USER_NAME = 1;
    //private const Int32 GRID_COLUMN_MB_USER_FULLNAME = 2;
    //private const Int32 GRID_COLUMN_MB_ACCT_TYPE = 3;
    //private const Int32 GRID_COLUMN_MB_HOLDER_NAME = 4;
    //private const Int32 GRID_COLUMN_MB_PENDING_CASH = 5;
    //private const Int32 GRID_COLUMN_MB_CASH_OVER = 6;
    //private const Int32 GRID_COLUMN_MB_TRACKDATA = 7;
    //private const Int32 GRID_COLUMN_MB_SHORTFALL = 8;

    private const Int32 GRID_COLUMN_MB_USER_ID = 0;
    private const Int32 GRID_COLUMN_MB_USER_NAME = 1;
    private const Int32 GRID_COLUMN_MB_ACCT_TYPE = 2;
    private const Int32 GRID_COLUMN_MB_HOLDER_NAME = 3;
    private const Int32 GRID_COLUMN_MB_PENDING_CASH = 4;
    private const Int32 GRID_COLUMN_MB_CASH_OVER = 5;
    private const Int32 GRID_COLUMN_MB_TRACKDATA = 6;
    private const Int32 GRID_COLUMN_MB_SHORTFALL = 7;

    private const Int32 GRID_WIDTH_MB_USER_ID = 0;
    private const Int32 GRID_WIDTH_MB_USER_NAME = 140;
    //private const Int32 GRID_WIDTH_MB_USER_FULLNAME = 180; //Not used automatically set
    private const Int32 GRID_WIDTH_MB_ACCT_TYPE = 0;
    private const Int32 GRID_WIDTH_MB_HOLDER_NAME = 140;
    private const Int32 GRID_WIDTH_MB_PENDING_CASH = 100;
    private const Int32 GRID_WIDTH_MB_CASH_OVER = 115;
    private const Int32 GRID_WIDTH_MB_TRACKDATA = 0;
    private const Int32 GRID_WIDTH_MB_SHORTFALL = 148;

    private const String TRACKDATA_ERROR = " * * * * * ";
    #endregion

    #region Attributes
    private frm_yesno form_yes_no;
    private Int32 m_mobibank_user_id;
    Boolean m_gp_pending_cash_partial;
    #endregion

    #region Constructor
    /// <summary>
    /// Constructor method.
    /// 1. Initialize controls.
    /// 2. Initialize control�s resources. (NLS, Images, etc)
    /// </summary>
    public frm_browse_mobile_banks_device()
    {
      InitializeComponent();

      InitializeControlResources();

      m_gp_pending_cash_partial = MobileBank.GP_GetRegisterShortfallOnDeposit();

      RestoreInitialFormWidth();

      form_yes_no = new frm_yesno();
      form_yes_no.Opacity = 0.6;
    }
    #endregion

    #region Private Methods
    //------------------------------------------------------------------------------
    // PURPOSE : Restore initial values for form and grid width.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //      Restoring these values must be done because every call to this form
    //  adds a few pixels to its width because of the vertical scroll bar
    //
    private void RestoreInitialFormWidth()
    {
      if (!m_gp_pending_cash_partial)
      {
        // ORIGINAL FORM WIDTH VALUES
        this.Width = 687;
        this.dataGridView1.Width = 670;
      }
      else
      {
        this.Width = 835;
        this.dataGridView1.Width = 817;
      }
    } // RestoreInitialFormWidth

    /// <summary>
    /// Set grid columns header, aligment, width
    /// </summary>
    private void SetGridColumns()
    {
      dataGridView1.Width += System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;

      dataGridView1.MultiSelect = false;
      dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;

      // Columns
      dataGridView1.Columns[GRID_COLUMN_MB_USER_ID].Width = GRID_WIDTH_MB_USER_ID;
      dataGridView1.Columns[GRID_COLUMN_MB_USER_ID].Visible = false;

      dataGridView1.Columns[GRID_COLUMN_MB_USER_NAME].Width = GRID_WIDTH_MB_USER_NAME;
      dataGridView1.Columns[GRID_COLUMN_MB_USER_NAME].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

      //dataGridView1.Columns[GRID_COLUMN_MB_USER_FULLNAME].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;    //Adjust column width
      //dataGridView1.Columns[GRID_COLUMN_MB_USER_FULLNAME].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
      //dataGridView1.Columns[GRID_COLUMN_MB_USER_FULLNAME].Visible = false;

      dataGridView1.Columns[GRID_COLUMN_MB_ACCT_TYPE].Width = GRID_WIDTH_MB_ACCT_TYPE;
      dataGridView1.Columns[GRID_COLUMN_MB_ACCT_TYPE].Visible = false;

      //dataGridView1.Columns[GRID_COLUMN_MB_HOLDER_NAME].Width = GRID_WIDTH_MB_HOLDER_NAME;
      dataGridView1.Columns[GRID_COLUMN_MB_HOLDER_NAME].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;    //Adjust column width
      dataGridView1.Columns[GRID_COLUMN_MB_HOLDER_NAME].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

      dataGridView1.Columns[GRID_COLUMN_MB_PENDING_CASH].Width = GRID_WIDTH_MB_PENDING_CASH;
      dataGridView1.Columns[GRID_COLUMN_MB_PENDING_CASH].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
      dataGridView1.Columns[GRID_COLUMN_MB_PENDING_CASH].DefaultCellStyle.Format = "#,##0.00";

      dataGridView1.Columns[GRID_COLUMN_MB_CASH_OVER].Width = GRID_WIDTH_MB_CASH_OVER;
      dataGridView1.Columns[GRID_COLUMN_MB_CASH_OVER].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
      dataGridView1.Columns[GRID_COLUMN_MB_CASH_OVER].DefaultCellStyle.Format = "#,##0.00";

      dataGridView1.Columns[GRID_COLUMN_MB_TRACKDATA].Width = GRID_WIDTH_MB_TRACKDATA;
      dataGridView1.Columns[GRID_COLUMN_MB_TRACKDATA].Visible = false;

      if (m_gp_pending_cash_partial)
      {
        dataGridView1.Columns[GRID_COLUMN_MB_SHORTFALL].Width = GRID_WIDTH_MB_SHORTFALL;
        dataGridView1.Columns[GRID_COLUMN_MB_SHORTFALL].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        dataGridView1.Columns[GRID_COLUMN_MB_SHORTFALL].DefaultCellStyle.Format = "#,##0.00";
      }
      else
      {
        dataGridView1.Columns[GRID_COLUMN_MB_SHORTFALL].Width = 0;
        dataGridView1.Columns[GRID_COLUMN_MB_SHORTFALL].Visible = false;
      }

      // Datagrid Headers
      dataGridView1.Columns[GRID_COLUMN_MB_USER_ID].HeaderCell.Value = "xUserId"; // Not Visible
      dataGridView1.Columns[GRID_COLUMN_MB_USER_NAME].HeaderCell.Value = Resource.String("STR_FRM_BROWSE_MOBILE_BANK_DEVICE_USER_NAME").ToUpper();
      //dataGridView1.Columns[GRID_COLUMN_MB_USER_FULLNAME].HeaderCell.Value = Resource.String("STR_FRM_BROWSE_MOBILE_BANK_DEVICE_USER_FULLNAME").ToUpper();
      dataGridView1.Columns[GRID_COLUMN_MB_ACCT_TYPE].HeaderCell.Value = "xAcctType"; // Not Visible
      dataGridView1.Columns[GRID_COLUMN_MB_HOLDER_NAME].HeaderCell.Value = Resource.String("STR_FRM_BROWSE_MB_COLUMN_HOLDER_NAME").ToUpper();
      dataGridView1.Columns[GRID_COLUMN_MB_PENDING_CASH].HeaderCell.Value = Resource.String("STR_FRM_BROWSE_MB_COLUMN_PENDING_CASH").ToUpper();
      dataGridView1.Columns[GRID_COLUMN_MB_CASH_OVER].HeaderCell.Value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_DIFF_EXTRA").ToUpper();
      dataGridView1.Columns[GRID_COLUMN_MB_TRACKDATA].HeaderCell.Value = "xTrackdata"; // Not Visible

      if (m_gp_pending_cash_partial)
      {
        dataGridView1.Columns[GRID_COLUMN_MB_SHORTFALL].HeaderCell.Value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_DIFF_MISSING").ToUpper();
      }
    }

    /// <summary>
    /// Get user id from selected row
    /// </summary>
    /// <returns></returns>
    private Int32 GetSelectedUserId()
    {
      Int32 _user_id;

      _user_id = MobileBankUserInfo.GetEmptyUserId();

      if (this.dataGridView1.RowCount > 0)
      {
        if (this.dataGridView1.CurrentRow.Cells[GRID_COLUMN_MB_USER_ID].Value != DBNull.Value)
        {
          _user_id = (Int32)this.dataGridView1.CurrentRow.Cells[GRID_COLUMN_MB_USER_ID].Value;
        }
      }

      return _user_id;
    }

    /// <summary>
    /// Get fields to retrieve
    /// </summary>
    /// <returns></returns>
    private String GetSQLFields()
    {
      StringBuilder _str;

      _str = new StringBuilder();
      //_str.AppendLine("GU_USER_ID, GU_USERNAME, GU_FULL_NAME, MB_ACCOUNT_TYPE, MB_HOLDER_NAME, ISNULL(MB_PENDING_CASH, 0) AS MB_PENDING_CASH");
      _str.AppendLine("GU_USER_ID, GU_USERNAME, MB_ACCOUNT_TYPE, MB_HOLDER_NAME, ISNULL(MB_PENDING_CASH, 0) AS MB_PENDING_CASH");
      _str.AppendLine(", ISNULL(MB_OVER_CASH, 0) AS MB_OVER_CASH");
      _str.AppendLine(", MB_TRACK_DATA");
      _str.AppendLine(", ISNULL(MB_SHORTFALL_CASH, 0) AS MB_SHORTFALL_CASH");

      return _str.ToString();
    }

    /// <summary>
    /// Get SQL for mobile banks data
    /// </summary>
    /// <returns></returns>
    private String GetSQLMobileBanks(Boolean ShowAllRunners)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine("          SELECT   " + GetSQLFields());
      _sb.AppendLine("            FROM   GUI_USERS                                                                 ");

      if (!ShowAllRunners)
      {
        //Only those assigned to the Cashier session
        _sb.AppendLine("      INNER JOIN   MOBILE_BANKS  ON  MB_USER_ID  =  GU_USER_ID                             ");
        _sb.AppendLine("           WHERE   (MB_CASHIER_SESSION_ID  =  @pCashierSession)                            ");
        _sb.AppendLine("           AND   (MB_CASHIER_SESSION_ID  <> 0 AND  MB_CASHIER_SESSION_ID  IS NOT NULL)     ");
      }
      else
      {
        //Those users with Mobibank permission and assigned to the Cashier session or not assigned to another Cashier session
        //Users with Mobibank permission 
        _sb.AppendLine(" LEFT OUTER JOIN   MOBILE_BANKS  ON  MB_USER_ID  =  GU_USER_ID                             ");
        _sb.AppendLine("           WHERE   GU_BLOCK_REASON = 0                                                     ");
        _sb.AppendLine("             AND   EXISTS (                                                                ");
        _sb.AppendLine("                           SELECT   *                                                      ");
        _sb.AppendLine("                             FROM   GUI_PROFILE_FORMS                                      ");
        _sb.AppendLine("                            WHERE   GPF_PROFILE_ID = GU_PROFILE_ID                         ");
        _sb.AppendLine("                              AND   GPF_GUI_ID     = @pGUIId                               ");
        _sb.AppendLine("                              AND   GPF_FORM_ID    = @pMobibankAccess                      ");
        _sb.AppendLine("                              AND   GPF_READ_PERM  = 1                                     ");
        _sb.AppendLine("                          )                                                                ");
        //Assigned to the Cashier session or not assigned to another Cashier session
        _sb.AppendLine("             AND   ( CASE                                                                  ");
        _sb.AppendLine("                        WHEN  (MB_CASHIER_SESSION_ID  IS NULL)  THEN  @pCashierSession     ");
        _sb.AppendLine("                        WHEN  (MB_CASHIER_SESSION_ID  = 0)      THEN  @pCashierSession     ");
        _sb.AppendLine("                        ELSE  MB_CASHIER_SESSION_ID                                        ");
        _sb.AppendLine("                     END )  =  @pCashierSession                                            ");
      }

      _sb.AppendLine("        ORDER BY   MB_PENDING_CASH DESC, GU_USERNAME                                         ");

      return _sb.ToString();
    }

    /// <summary>
    /// Get mobile banks data and refresh grid
    /// </summary>
    /// <returns></returns>
    private Boolean GetMobileBanksData()
    {
      Boolean _show_all_runners;

      _show_all_runners = chk_show_all_runners.Checked;
      btn_select.Enabled = false;

      if (Cashier.SessionId == 0 && (!WSI.Common.MobileBankConfig.IsMobileBankLinkedToADevice))
      {
        return false;
      }

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(GetSQLMobileBanks(_show_all_runners), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {

            _cmd.Parameters.Add("@pCashierSession", SqlDbType.BigInt).Value = Cashier.SessionId;

            if (_show_all_runners)
            {
              _cmd.Parameters.Add("@pGUIId", SqlDbType.Int).Value = (int)ENUM_GUI.WIGOS_GUI;
              _cmd.Parameters.Add("@pMobibankAccess", SqlDbType.Int).Value = (int)WSI.Cashier.ProfilePermissions.CashierFormFuncionality.MobibankAccess;
            }

            using (DataTable _dt_mobile_bank = new DataTable())
            {
              _dt_mobile_bank.Load(_cmd.ExecuteReader());

              dataGridView1.DataSource = _dt_mobile_bank;
              dataGridView1.Refresh();
            }
          }
        }

        // Avoid selection from an empty grid
        btn_select.Enabled = (dataGridView1.RowCount > 0);

        return true;
      }
      catch (Exception ex)
      {
        Log.Error(ex.Message);
      }

      return false;
    }
    #endregion

    #region Public Methods
    //------------------------------------------------------------------------------
    // PURPOSE : Initialize control resources (NLS strings, images, etc).
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public void InitializeControlResources()
    {
      //NLS Strings:

      //Title:
      this.Text = Resource.String("STR_FRM_BROWSE_MB_TITLE");
      this.FormTitle = this.Text.ToUpper();
      this.ShowCloseButton = false;

      //Buttons
      btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");
      btn_select.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_SELECT");

      //Check box
      chk_show_all_runners.Text = Resource.String("STR_FRM_BROWSE_MOBILE_BANK_DEVICE_ALL_RUNNERS");
      chk_show_all_runners.Checked = false;
    } // InitializeControlResources

    //------------------------------------------------------------------------------
    // PURPOSE : Init and Show the form.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public Int32 SelectUserFromMobileBank()
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_browse_mobile_banks_device", Log.Type.Message);
      Int32 idx_row;
      String internal_card_id;
      string encrypted_trackdata;
      int card_type;
      bool rc;

      if (!GetMobileBanksData())
      {
        return MobileBankUserInfo.GetEmptyUserId();
      }

      this.InitializeControlResources();

      this.Width += System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;

      //Grid adjustment
      SetGridColumns();

      for (idx_row = 0; idx_row < dataGridView1.Rows.Count; idx_row++)
      {
        //if ((Int16)dataGridView1.Rows[idx_row].Cells[GRID_COLUMN_MB_ACCT_TYPE].Value == 0)
        //{
        //  dataGridView1.Rows[idx_row].Cells[GRID_COLUMN_MB_HOLDER_NAME].Value = DBNull.Value;
        //}

        if (dataGridView1.Rows[idx_row].Cells[GRID_COLUMN_MB_TRACKDATA].Value != DBNull.Value)
        {
          if ((String)dataGridView1.Rows[idx_row].Cells[GRID_COLUMN_MB_TRACKDATA].Value != "")
          {
            internal_card_id = (String)dataGridView1.Rows[idx_row].Cells[GRID_COLUMN_MB_TRACKDATA].Value;
            card_type = 0;
            rc = CardNumber.TrackDataToExternal(out encrypted_trackdata, internal_card_id, card_type);

            if (rc)
            {
              dataGridView1.Rows[idx_row].Cells[GRID_COLUMN_MB_TRACKDATA].Value = encrypted_trackdata;
            }
            else
            {
              dataGridView1.Rows[idx_row].Cells[GRID_COLUMN_MB_TRACKDATA].Value = TRACKDATA_ERROR;
            }
          }
        }
      } // for

      dataGridView1.Focus();

      form_yes_no.Show();
      this.ShowDialog();
      form_yes_no.Hide();

      RestoreInitialFormWidth();

      return m_mobibank_user_id;
    } // Show

    //------------------------------------------------------------------------------
    // PURPOSE : Dispose yes_no form.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    new public void Dispose()
    {
      form_yes_no.Dispose();
      form_yes_no = null;

      base.Dispose();
    }

    #endregion Public methods

    #region DataGridEvents
    //------------------------------------------------------------------------------
    // PURPOSE : Handle the double click on a datagrid row.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //      This method is used to allow record selection by double-clicking on the grid
    //
    private void dataGridView1_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
    {
      m_mobibank_user_id = GetSelectedUserId();

      Misc.WriteLog("[FORM CLOSE] frm_browse_mobile_banks_device (cellmousedoubleclick)", Log.Type.Message);
      this.Close();
    } // dataGridView1_CellMouseDoubleClick

    //------------------------------------------------------------------------------
    // PURPOSE : Paint a datagrid row before actually filling it.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void dataGridView1_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
    {
      // Highlight mobile banks with pending amounts to be cashed
      if ((dataGridView1.Rows[e.RowIndex].Cells[GRID_COLUMN_MB_PENDING_CASH].Value != DBNull.Value)
            && ((Decimal)dataGridView1.Rows[e.RowIndex].Cells[GRID_COLUMN_MB_PENDING_CASH].Value > 0))
      {
        dataGridView1.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.NavajoWhite;
      }
    } // dataGridView1_RowPrePaint
    #endregion DataGridEvents

    #region Buttons & Checkbox
    //------------------------------------------------------------------------------
    // PURPOSE : Handle the click on the Cancel button (exit form).
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void btn_cancel_Click(object sender, EventArgs e)
    {
      m_mobibank_user_id = MobileBankUserInfo.GetEmptyUserId();

      Misc.WriteLog("[FORM CLOSE] frm_browse_mobile_banks_device (cancel)", Log.Type.Message);
      this.Close();
    } // btn_cancel_Click

    //------------------------------------------------------------------------------
    // PURPOSE : Handle the click on the Select button (exit form and pass the
    //           selected record identifier to the caller).
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void btn_select_Click(object sender, EventArgs e)
    {
      m_mobibank_user_id = GetSelectedUserId();

      Misc.WriteLog("[FORM CLOSE] frm_browse_mobile_banks_device (select)", Log.Type.Message);
      this.Close();
    } // btn_select_Click

    /// <summary>
    /// Event for the all runners chekcbox checked
    /// </summary>
    private void chk_show_all_runners_CheckedChanged(object sender, EventArgs e)
    {
      GetMobileBanksData();
    }
    #endregion Buttons
  }
}