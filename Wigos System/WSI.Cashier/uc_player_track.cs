//////using System;
//////using System.Collections.Generic;
//////using System.ComponentModel;
//////using System.Drawing;
//////using System.Data;
//////using System.Text;
//////using System.Windows.Forms;
//////using WSI.Common;

//////namespace WSI.Cashier
//////{
//////  public partial class uc_player_track : UserControl
//////  {
//////    #region Class Atributes

//////    CardPrinter card_printer;
//////    AccountWatcher account_watcher;
//////    ////////Boolean anonymous;
//////    Boolean m_is_new_card = false;
//////    String track_number;
//////    //////Boolean is_logged_in;
//////    Int64 m_current_card_id;
//////    Points m_current_card_points_balance;
//////    Int32 tick_count_tracknumber_read = 0;
//////    Boolean first_time = true;
//////    frm_last_movements form_last_plays = new frm_last_movements();
//////    String string_empty = " ";
//////    Int32 m_sub_type;

//////    #endregion

//////    #region Constructor

//////    public uc_player_track()
//////    {
//////      InitializeComponent();

//////      card_printer = new CardPrinter();
//////    }

//////    #endregion

//////    #region Internal

//////    /// <summary>
//////    /// Init control resources
//////    /// </summary>
//////    internal void InitializeControlResources()
//////    {
//////      //   - Labels
//////      lbl_account_id_name.Text = Resource.String("STR_FORMAT_GENERAL_NAME_ACCOUNT") + ":";
//////      lbl_type_title.Text = Resource.String("STR_UC_PLAYER_TRACK_TYPE") + ":";
//////      lbl_cards_title.Text = Resource.String("STR_FORMAT_GENERAL_NAME_PLAYER_TRACKING");
//////      lbl_id_title.Text = Resource.String("STR_UC_PLAYER_TRACK_HOLDER_ID") + ":";
//////      lbl_marital_status_title.Text = Resource.String("STR_UC_PLAYER_TRACK_MARITAL_STATUS") + ":";
//////      lbl_gender_title.Text = Resource.String("STR_UC_PLAYER_TRACK_GENDER") + ":";
//////      lbl_birth_title.Text = Resource.String("STR_UC_PLAYER_TRACK_BIRTH") + ":";
//////      lbl_phone_title.Text = Resource.String("STR_UC_PLAYER_TRACK_PHONE_01") + ":";
//////      lbl_email_title.Text = Resource.String("STR_UC_PLAYER_TRACK_EMAIL_01") + ":";
//////      lbl_address_01_title.Text = Resource.String("STR_UC_PLAYER_TRACK_ADDRESS_01") + ":";
//////      lbl_address_02_title.Text = Resource.String("STR_UC_PLAYER_TRACK_ADDRESS_02") + ":";
//////      lbl_address_03_title.Text = Resource.String("STR_UC_PLAYER_TRACK_ADDRESS_03") + ":";
//////      lbl_city_title.Text = Resource.String("STR_UC_PLAYER_TRACK_CITY") + ":";
//////      lbl_zip_title.Text = Resource.String("STR_UC_PLAYER_TRACK_ZIP") + ":";
//////      gb_comments.Text = Resource.String("STR_UC_PLAYER_TRACK_COMMENTS");
//////      gb_address.Text = Resource.String("STR_UC_PLAYER_TRACK_ADDRESS");
//////      btn_lasts_movements.Text = Resource.String("STR_UC_CARD_BTN_LAST_MOVEMENTS");
//////      this.form_last_plays.InitializeControlResources();
//////      this.uc_card_reader1.InitializeControlResources();
//////    }

//////    internal void EnableDisableButtons(CASHIER_STATUS CashierStatus)
//////    {
//////      switch (CashierStatus)
//////      {
//////        case CASHIER_STATUS.OPEN:
//////          break;

//////        case CASHIER_STATUS.CLOSE:
//////        default:
//////          break;
//////      }
//////    }


//////    public void InitFocus()
//////    {
//////      uc_card_reader1.Focus();
//////    }

//////    #endregion

//////    #region Public

//////    /// <summary>
//////    /// Init control logic
//////    /// </summary>
//////    public void InitControls()
//////    {
//////      pb_user.Image = WSI.Cashier.Images.Get(Images.CashierImage.UserMale);

//////      ClearCardData();
//////      EnableDisableButtons(false);

//////      if (first_time)
//////      {
//////        first_time = false;

//////        account_watcher = new AccountWatcher();
//////        account_watcher.Start();
//////        account_watcher.AccountChangedEvent += new AccountWatcher.AccountChangedHandler(account_watcher_AccountChangedEvent);

//////        uc_card_reader1.OnTrackNumberReadEvent += new uc_card_reader.TrackNumberReadEventHandler(uc_card_reader1_OnTrackNumberReadEvent);
//////        uc_card_reader1.OnTrackNumberReadingEvent += new uc_card_reader.TrackNumberReadingHandler(uc_card_reader1_OnTrackNumberReadingEvent);


//////        btn_account_edit.Click += new EventHandler(btn_button_clicked);
//////        btn_lasts_movements.Click += new EventHandler(btn_button_clicked);
//////        btn_transfer_p.Click += new EventHandler(btn_button_clicked);
//////        btn_print_card.Click += new EventHandler(btn_button_clicked);

//////        InitializeControlResources();
//////      }
//////    }

//////    public void ClearCardData()
//////    {
//////      track_number = "";
//////      RefreshData(new CardData());
//////    }

//////    #endregion

//////    #region Private

//////    /// <summary>
//////    /// Call to refresh screen values with the present card
//////    /// </summary>
//////    private void RefreshData()
//////    {
//////      RefreshData(account_watcher.Card);
//////    }

//////    /// <summary>
//////    /// Refresh screen values
//////    /// </summary>
//////    /// <param name="CardData"></param>
//////    private void RefreshData(CardData CardData)
//////    {
//////      CardData card_data;

//////      card_data = CardData;
//////      if (card_data == null)
//////      {
//////        card_data = new CardData();
//////      }

//////      track_number = card_data.VisibleTrackdata();

//////      // lbl_section_1.Text = track_number;
//////      //lbl_track_number.Text = track_number;

//////      // Display card data
//////      m_current_card_id = card_data.CardId;
//////      m_current_card_points_balance = card_data.PlayerTracking.CurrentPoints;

//////      if (card_data.PlayerTracking.HolderName == "" || card_data.PlayerTracking.HolderName == null)
//////      {
//////        ////this.anonymous = true;
//////        lbl_holder_name.Text = Resource.String("STR_UC_CARD_CARD_HOLDER_ANONYMOUS");
//////        pb_user.Image = WSI.Cashier.Images.Get(Images.CashierImage.UserAnonymous);

//////        lbl_gender.Text = string_empty;
//////        lbl_birth.Text = string_empty;
//////        lbl_phones.Text = string_empty;
//////        lbl_emails.Text = string_empty;
//////        lbl_emails_2.Text = string_empty;
//////        lbl_address_01.Text = string_empty;
//////        lbl_address_02.Text = string_empty;
//////        lbl_address_03.Text = string_empty;
//////        lbl_city.Text = string_empty;
//////        lbl_zip.Text = string_empty;
//////        lbl_comments.Text = string_empty;
//////        lbl_type_user.Text = string_empty;
//////        lbl_marital_status.Text = string_empty;
//////        lbl_id.Text = string_empty;

//////      }
//////      else
//////      {
//////        ////this.anonymous = false;
//////        lbl_holder_name.Text = card_data.PlayerTracking.HolderName;
//////        lbl_gender.Text = GenderToString(card_data.PlayerTracking.HolderGender);
//////        lbl_marital_status.Text = MaritalToString(card_data.PlayerTracking.HolderMaritalStatus);
//////        lbl_id.Text = card_data.PlayerTracking.HolderId;
//////        if (card_data.PlayerTracking.HolderBirthDate != DateTime.MinValue)
//////        {
//////          lbl_birth.Text = card_data.PlayerTracking.HolderBirthDate.ToShortDateString();
//////        }
//////        else
//////        {
//////          lbl_birth.Text = string_empty;
//////        }
//////        lbl_phones.Text = card_data.PlayerTracking.HolderPhone01;
//////        if (card_data.PlayerTracking.HolderPhone02 != "")
//////        {
//////          lbl_phones.Text += " / " + card_data.PlayerTracking.HolderPhone02;
//////        }
//////        lbl_emails.Text = card_data.PlayerTracking.HolderEmail01;
//////        lbl_emails_2.Text = card_data.PlayerTracking.HolderEmail02;
//////        lbl_address_01.Text = card_data.PlayerTracking.HolderAddress01;
//////        lbl_address_02.Text = card_data.PlayerTracking.HolderAddress02;
//////        lbl_address_03.Text = card_data.PlayerTracking.HolderAddress03;
//////        lbl_city.Text = card_data.PlayerTracking.HolderCity;
//////        lbl_zip.Text = card_data.PlayerTracking.HolderZip;
//////        lbl_comments.Text = card_data.PlayerTracking.HolderComments;

//////        switch (card_data.PlayerTracking.HolderGender)
//////        {
//////          case 1:
//////            pb_user.Image = WSI.Cashier.Images.Get(Images.CashierImage.UserMale);
//////            break;
//////          case 2:
//////            pb_user.Image = WSI.Cashier.Images.Get(Images.CashierImage.UserFemale);
//////            break;
//////          default:
//////            pb_user.Image = WSI.Cashier.Images.Get(Images.CashierImage.UserAnonymous);
//////            break;
//////        }
//////      }

//////      m_sub_type = card_data.PlayerTracking.SubType;

//////      switch (card_data.PlayerTracking.SubType)
//////      {
//////        // Player (CJ)
//////        case 1:
//////          {
//////            lbl_type_user.Text = Resource.String("STR_UC_PLAYER_TRACK_AC_PLAYER");

//////            break;
//////          }

//////        // Service (restaurant)
//////        case 2:
//////          {
//////            lbl_type_user.Text = Resource.String("STR_UC_PLAYER_TRACK_AC_RESTAURANT");

//////            break;
//////          }

//////        // Service (playable credits)
//////        case 3:
//////          {
//////            lbl_type_user.Text = Resource.String("STR_UC_PLAYER_TRACK_AC_CREDITS");

//////            break;
//////          }

//////        // Service (expired credits)
//////        case 4:
//////          {
//////            lbl_type_user.Text = Resource.String("STR_UC_PLAYER_TRACK_AC_EXPIRED");

//////            break;
//////          }

//////        // Not a player tracking card
//////        default:
//////          {
//////            lbl_type_user.Text = string_empty;

//////            break;
//////          }
//////      }

//////      lbl_account_id_value.Text = m_current_card_id.ToString();

//////      m_is_new_card = false;

//////      uc_points_balance1.SetCard(card_data);
//////    }

//////    /// <summary>
//////    /// Returns gender string from db number
//////    /// </summary>
//////    /// <param name="Gender"></param>
//////    private string GenderToString(int Gender)
//////    {
//////      switch (Gender)
//////      {
//////        case 1:
//////          return Resource.String("STR_FRM_PLAYER_TRACKING_CB_GENDER_MALE");

//////        case 2:
//////          return Resource.String("STR_FRM_PLAYER_TRACKING_CB_GENDER_FEMALE");

//////        default:
//////          return string_empty;
//////      }
//////    }


//////    /// <summary>
//////    /// Returns Marital Status string from db number
//////    /// </summary>
//////    /// <param name="Gender"></param>
//////    private string MaritalToString(int Marital)
//////    {
//////      switch (Marital)
//////      {
//////        case 1:
//////          return Resource.String("STR_FRM_PLAYER_TRACKING_CB_MARITAL_SINGLE");
       
//////        case 2:
//////          return Resource.String("STR_FRM_PLAYER_TRACKING_CB_MARITAL_MARRIED");
       
//////        case 3:
//////          return Resource.String("STR_FRM_PLAYER_TRACKING_CB_MARITAL_DIVORCED");
       
//////        case 4:
//////          return Resource.String("STR_FRM_PLAYER_TRACKING_CB_MARITAL_WIDOWED");
        
//////        case 5:
//////          return Resource.String("STR_FRM_PLAYER_TRACKING_CB_MARITAL_UNMARRIED");
        
//////        case 6:
//////          return Resource.String("STR_FRM_PLAYER_TRACKING_CB_MARITAL_OTHER");

//////        default:
//////          return string_empty;
//////      }
//////    }

//////    /// <summary>
//////    /// Show last card movements (plays)
//////    /// </summary>
//////    /// <param name="sender"></param>
//////    /// <param name="e"></param>
//////    private void btn_last_plays_Click(object sender, EventArgs e)
//////    {
//////      String sql_str;

//////      if (m_current_card_id == 0)
//////      {
//////        return;
//////      }

//////      // Query to obtain last cards movements
//////      sql_str = "SELECT TOP 150 AM_TYPE, '  ' AS TYPE_NAME, AM_DATETIME, '  ' AS USER_NAME, TE_NAME, AM_TERMINAL_ID, AM_SECOND_ACCOUNT_ID, AM_INITIAL_POINTS_BALANCE, AM_SUB_POINTS, AM_ADD_POINTS, AM_FINAL_POINTS_BALANCE " +
//////                "FROM ( SELECT TOP 150 AM_MOVEMENT_ID, AM_TYPE, AM_DATETIME, AM_TERMINAL_ID, " +
//////                        "CASE WHEN AM_SECOND_ACCOUNT_ID IS NOT NULL THEN AM_SECOND_ACCOUNT_ID ELSE 0 END AS AM_SECOND_ACCOUNT_ID, " +
//////                        "CASE WHEN AM_INITIAL_POINTS_BALANCE IS NOT NULL THEN AM_INITIAL_POINTS_BALANCE ELSE 0 END AS AM_INITIAL_POINTS_BALANCE, " +
//////                        "CASE WHEN AM_ADD_POINTS IS NOT NULL THEN AM_ADD_POINTS ELSE 0 END AS AM_ADD_POINTS, " + 
//////                        "CASE WHEN AM_SUB_POINTS IS NOT NULL THEN AM_SUB_POINTS ELSE 0 END AS AM_SUB_POINTS, " +
//////                        "CASE WHEN AM_FINAL_POINTS_BALANCE IS NOT NULL THEN AM_FINAL_POINTS_BALANCE ELSE 0 END AS AM_FINAL_POINTS_BALANCE " +
//////                        "FROM ACCOUNT_MOVEMENTS " +
//////                        "WHERE AM_TYPE IN (0,14,15,16,17,19) AND AM_ACCOUNT_ID = " + m_current_card_id.ToString() + "  " +
//////                        "ORDER BY AM_DATETIME DESC, AM_MOVEMENT_ID DESC " +
//////                     ") X LEFT OUTER JOIN TERMINALS AS T ON AM_TERMINAL_ID = T.TE_TERMINAL_ID " +
//////                     " ORDER BY X.AM_DATETIME DESC ";

//////      this.Cursor = Cursors.WaitCursor;
//////      form_last_plays.Show(sql_str, frm_last_movements.MovementShow.Player_Tracking, track_number);
//////      this.Cursor = Cursors.Default;
//////    }

//////    /// <summary>
//////    /// Check for input card in order to notify user to create card.
//////    /// </summary>
//////    /// <param name="TrackNumber"></param>
//////    private void LoadCardByTrackNumber(String TrackNumber)
//////    {
//////      // Check for no input card
//////      if (TrackNumber == "")
//////      {
//////        return;
//////      }

//////      CardData card_data = new CardData();

//////      track_number = "";

//////      // Get Card data
//////      if (!CashierBusinessLogic.DB_CardGetAllData(TrackNumber, card_data))
//////      {
//////        ClearCardData();

//////        Log.Error("LoadCardByTrackNumber. DB_GetCardAllData: Error reading card.");

//////        return;
//////      }

//////      // Check the card belong to the site.
//////      if (Program.site_id != card_data.SiteId)
//////      {
//////        ClearCardData();

//////        Log.Error("LoadCardByTrackNumber. Card: " + TrackNumber + " doesn�t belong to this site.");

//////        frm_message.Show(Resource.String("STR_CARD_SITE_ID_ERROR"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Warning, this.ParentForm);

//////        uc_card_reader1.Focus();

//////        return;
//////      }

//////      if (card_data.CardId == 0)
//////      {
//////        String msgbox_text;
//////        DialogResult msbox_answer;

//////        // Check provided trackdata is not already associated to a MB account
//////        if (CashierBusinessLogic.DB_IsMBCardDefined(TrackNumber))
//////        {
//////          // MsgBox: Card already assigned to a Mobile Bank account.
//////          frm_message.Show(Resource.String("STR_UC_CARD_USER_MSG_CARD_LINKED_TO_MB_ACCT"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this.ParentForm);

//////          uc_card_reader1.Focus();

//////          return;
//////        }

//////        // Card does not exist: request confirmation to create a new one.
//////        msgbox_text = Resource.String("STR_UC_CARD_USER_MSG_CARD_NOT_EXIST", "\n\n" + TrackNumber + "\n\n");
//////        msbox_answer = frm_message.Show(msgbox_text, Resource.String("STR_UC_CARD_USER_MSG_CARD_NOT_EXIST_TITLE"), MessageBoxButtons.YesNo, MessageBoxIcon.Warning, this.ParentForm);
//////        if (msbox_answer == DialogResult.OK)
//////        {
//////          if (!CashierBusinessLogic.DB_CreateCard(TrackNumber))
//////          {
//////            Log.Error("LoadCardByTrackNumber. DB_CreateCard: Card already exists or error creating card.");

//////            uc_card_reader1.Focus();

//////            return;
//////          }

//////          // Load data after create account.
//////          LoadCardByTrackNumber(TrackNumber);
//////        }
//////        else
//////        {
//////          this.ClearCardData();
//////          this.InitControls();
//////        }
//////      }
//////      else
//////      {
//////        // Keep card read data.
//////        track_number = TrackNumber;
//////        m_current_card_id = card_data.CardId;
//////        m_sub_type = card_data.PlayerTracking.SubType;
//////        m_current_card_points_balance = card_data.PlayerTracking.CurrentPoints;
//////        //account_watcher.AccountId = card_data.CardId;
//////        account_watcher.SetAccountId(card_data.CardId, AccountWatcher.account_type_player);
//////      }
//////    } // LoadCardByTrackNumber

//////    private void ToggleApparenceCtrl(Control Ctrl, Boolean Enabled)
//////    {
//////      if (Ctrl.Equals(uc_card_reader1)
//////          || Ctrl.Equals(lbl_cards_title)
//////          || Ctrl is Button)
//////      {
//////        return;
//////      }
//////      if (!Ctrl.Equals(this))
//////      {
//////        Ctrl.Enabled = Enabled;
//////      }

//////      foreach (Control ctrl in Ctrl.Controls)
//////      {
//////        ToggleApparenceCtrl(ctrl, Enabled);
//////      }

//////    } //ToggleApparenceCtrl

//////    //------------------------------------------------------------------------------
//////    // PURPOSE: Change the status of the cashier buttons based on the current cashier 
//////    //          status (open / not open), the account status (locked / in use) and the 
//////    //          incoming parameter.
//////    // 
//////    //  PARAMS:
//////    //      - INPUT:
//////    //          - EnableButtons
//////    //              - TRUE: try to enable buttons, actually enabled buttons depend on 
//////    //                      cashier and account status.
//////    //
//////    //      - OUTPUT:
//////    //
//////    // RETURNS:
//////    // 
//////    //   NOTES:
//////    //

//////    private void EnableDisableButtons(Boolean EnableButtons)
//////    {
//////      ////////Boolean redeem_stat;
//////      ////////////Boolean button_enable_flag;
//////      ////////Boolean red_p_flag;

//////      ////////////button_enable_flag = EnableButtons && !is_logged_in;
//////      ////////////redeem_stat = button_enable_flag;

//////      ////////if (m_current_card_points_balance <= 0)
//////      ////////{
//////      ////////  redeem_stat = false;
//////      ////////}

//////      ////////tick_count_tracknumber_read = 0;
//////      ////////timer1.Enabled = EnableButtons;
//////      ////////btn_lasts_movements.Enabled = EnableButtons;

//////      ////////ToggleApparenceCtrl(this, EnableButtons);

//////      ////////switch (m_sub_type)
//////      ////////{
//////      ////////  // Player (CJ)
//////      ////////  case 1:
//////      ////////    {
//////      ////////      btn_account_edit.Enabled = button_enable_flag;
//////      ////////      // Only show Redimir Puntos if CurrentPoints are > 0
//////      ////////      red_p_flag = button_enable_flag && (m_current_card_points_balance > 0);
//////      ////////      btn_red_p.Enabled = false; // red_p_flag; - TODO
//////      ////////      btn_transfer_p.Enabled = false;
//////      ////////      btn_lasts_movements.Enabled = button_enable_flag;
//////      ////////      btn_print_card.Enabled = button_enable_flag && card_printer.PrinterExist();

//////      ////////      break;
//////      ////////    }

//////      ////////  // Service (restaurant)
//////      ////////  // Service (playable credits)
//////      ////////  case 2:
//////      ////////  case 3:
//////      ////////    {
//////      ////////      btn_account_edit.Enabled = false;
//////      ////////      btn_red_p.Enabled = false;
//////      ////////      btn_transfer_p.Enabled = button_enable_flag;
//////      ////////      btn_lasts_movements.Enabled = button_enable_flag;
//////      ////////      btn_print_card.Enabled = button_enable_flag && card_printer.PrinterExist();

//////      ////////      break;
//////      ////////    }

//////      ////////  // Service (expired credits)
//////      ////////  case 4:
//////      ////////    {
//////      ////////      btn_account_edit.Enabled = false;
//////      ////////      btn_red_p.Enabled = false;
//////      ////////      btn_transfer_p.Enabled = false;
//////      ////////      btn_lasts_movements.Enabled = false;
//////      ////////      btn_print_card.Enabled = button_enable_flag && card_printer.PrinterExist();

//////      ////////      break;
//////      ////////    }

//////      ////////  default:
//////      ////////    {
//////      ////////      btn_account_edit.Enabled = false;
//////      ////////      btn_red_p.Enabled = false;
//////      ////////      btn_transfer_p.Enabled = false;
//////      ////////      btn_lasts_movements.Enabled = false;
//////      ////////      btn_print_card.Enabled = false;
//////      ////////      break;
//////      ////////    }
//////      ////////}

//////      ////////// TODO: Check points and redeem coupouns

//////      ////////if (EnableButtons)
//////      ////////{
//////      ////////  if (CashierBusinessLogic.ReadCashierStatus(Cashier.SessionId) != CASHIER_STATUS.OPEN)
//////      ////////  {
//////      ////////    EnableDisableButtons(CASHIER_STATUS.CLOSE);
//////      ////////  }
//////      ////////}
//////    } // EnableDisableButtons

//////    private void buttons_EnabledChanged(object sender, EventArgs e)
//////    {
//////      System.Windows.Forms.Button a;

//////      a = (System.Windows.Forms.Button)sender;

//////      if (a.Enabled)
//////      {
//////        a.BackColor = System.Drawing.Color.AntiqueWhite;
//////      }
//////      else
//////      {
//////        a.BackColor = System.Drawing.Color.LightGray;
//////      }
//////    }

//////    #endregion

//////    #region Handlers

//////    private void timer1_Tick(object sender, EventArgs e)
//////    {
//////      tick_count_tracknumber_read += 1;

//////      if (tick_count_tracknumber_read >= 30)
//////      {
//////        EnableDisableButtons(false);

//////        return;
//////      }
//////    }

//////    void account_watcher_AccountChangedEvent(Int64 AccountId)
//////    {
//////      if (this.InvokeRequired)
//////      {
//////        this.Invoke(new AccountWatcher.AccountChangedHandler(account_watcher_AccountChangedEvent), new object[] { AccountId });

//////        return;
//////      }

//////      RefreshData();
//////    }

//////    /// <summary>
//////    /// Event fired when a card is swiped.
//////    /// </summary>
//////    /// <param name="TrackNumber">Number read</param>
//////    /// <param name="IsValid">Track read is valid</param>
//////    private void uc_card_reader1_OnTrackNumberReadEvent(string TrackNumber, ref Boolean IsValid)
//////    {
//////      // Load and load track number
//////      LoadCardByTrackNumber(TrackNumber);

//////      if (track_number == "" || track_number == null)
//////      {
//////        IsValid = false;
//////      }
      
//////      EnableDisableButtons(IsValid);
      
//////      m_is_new_card = IsValid;
//////    }

//////    private void uc_card_reader1_OnTrackNumberReadingEvent()
//////    {
//////      EnableDisableButtons(false);
//////    }

//////    private void btn_button_clicked(object sender, EventArgs e)
//////    {
//////      if (sender.Equals(btn_account_edit))
//////      {
//////        btn_edit_Click(sender, e);
//////      }

//////      if (sender.Equals(btn_lasts_movements))
//////      {
//////        btn_last_plays_Click(sender, e);
//////      }

//////      if (sender.Equals(btn_transfer_p))
//////      {
//////        btn_transfer_p_Click(sender, e);
//////      }

//////      if (sender.Equals(btn_print_card))
//////      {
//////        btn_print_card_Click(sender, e);
//////      }

//////      EnableDisableButtons(false);

//////      if (!uc_card_reader1.Focused)
//////      {
//////        uc_card_reader1.Focus();
//////      }
//////    }

//////    private void btn_edit_Click(object sender, EventArgs e)
//////    {
//////      // Check if current user is an authorized user
//////      String error_str;
//////      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.AccountEdit, ProfilePermissions.TypeOperation.RequestPasswd, this.ParentForm, out error_str))
//////      {
//////        return;
//////      }

//////      frm_player_edit account_edit = new frm_player_edit(/*this,*/ account_watcher.Card);

//////      frm_yesno shader = new frm_yesno();

//////      shader.Opacity = 0.6;
//////      shader.Show();

//////      account_edit.ShowDialog();

//////      shader.Hide();
//////      shader.Dispose();

//////      account_edit.Hide();
//////      account_edit.Dispose();

//////      account_watcher.ForceRefresh();
//////      RefreshData();

//////    }

//////    private void btn_transfer_p_Click(object sender, EventArgs e)
//////    {
//////      frm_points_transfer points_transfer = new frm_points_transfer();

//////      frm_yesno shader = new frm_yesno();

//////      shader.Opacity = 0.6;
//////      shader.Show();

//////      points_transfer.Show(account_watcher.Card, this);

//////      shader.Hide();
//////      shader.Dispose();

//////      points_transfer.Hide();
//////      points_transfer.Dispose();

//////      account_watcher.ForceRefresh();
//////      RefreshData();

//////    }

//////    /// <summary>
//////    /// Print Card
//////    /// </summary>
//////    /// <param name="sender"></param>
//////    /// <param name="e"></param>
//////    private void btn_print_card_Click(object sender, EventArgs e)
//////    {
//////      // Check if current user is an authorized user
//////      String error_str;
//////      DialogResult msg_answer;

//////      if ( ! ProfilePermissions.CheckPermissions (ProfilePermissions.CashierFormFuncionality.PrintCard, 
//////                                                  ProfilePermissions.TypeOperation.ShowMsg, 
//////                                                  this.ParentForm, 
//////                                                  out error_str) )
//////      {
//////        return;
//////      }

//////      // Check the printer 
//////      if ( ! card_printer.PrinterExist () )
//////      {
//////        frm_message.Show(Resource.String("STR_PRINT_CARD_PRINTER_IS_NOT_INSTALLED"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this.ParentForm);

//////        return;
//////      }

//////      // Ask for confirmation before printing 
//////      msg_answer = frm_message.Show(Resource.String("STR_PRINT_CARD_PRINTER_INPUT_INFO"),
//////                                    Resource.String("STR_PRINT_CARD_PRINTER_INPUT_INFO_MSG"),
//////                                    MessageBoxButtons.YesNo,
//////                                    MessageBoxIcon.Warning,
//////                                    this.ParentForm);

//////      if ( msg_answer == DialogResult.Cancel )
//////      {
//////        return;
//////      }

//////      // Send document to print
//////      this.Cursor = Cursors.WaitCursor;

//////      card_printer.PrintNameInCard(lbl_holder_name.Text);

//////      this.Cursor = Cursors.Default;

//////    } // btn_print_card_Click

//////    #endregion

//////  }
//////}
