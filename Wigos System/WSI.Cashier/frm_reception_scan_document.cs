﻿//------------------------------------------------------------------------------
// Copyright © 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_reception_scan_document.cs
// 
//   DESCRIPTION: Implements document scan dialog for reception functionality. 
//
//        AUTHOR: Ariel Vazquez Zerpa
// 
// CREATION DATE: 18-JUL-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 07-ENE-2016 AVZ    First release. Product Backlog Item 6648:Visitas / Recepción: Pantalla de introducción de datos del Customer / Cliente - Task 7096: Integración escaneo de documentos
// 28-APR-2017 RGR    PBI 27131: Implementation of the Duplex Scan in the Cashier
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using WSI.Cashier.Controls;
using WSI.Cashier.MVC.Model.PlayerEditDetails;
using WSI.Common;
using WSI.Common.Entities.General;
using WSI.Common.Entrances;
using WSI.Common.Utilities.Extensions;
using WSI.IDScanner.Model;

namespace WSI.Cashier
{
  public partial class frm_reception_scan_document : frm_base
  {
    #region Properties

    public Image ScannedImage
    {
      get { return m_scanned_image; }
    }

    public Nullable<DateTime> ExpirationDate
    {
      get { return m_expiration_date; }
    }

    public int DocumentTypeId
    {
      get { return m_document_type_id; }
    }

    public string DocumentTypeDescription
    {
      get { return m_document_type_description; }
    }

    #endregion

    #region Members

    private frm_yesno form_yes_no;
    private Image m_scanned_image;
    private int m_document_type_id;
    private string m_document_type_description;
    private Nullable<DateTime> m_expiration_date;
    private WSI.IDScanner.IDScanner m_id_scanner;

    #endregion

    public frm_reception_scan_document()
    {
      InitializeComponent();

      InitializeControlResources();
    }

    /// <summary>
    /// Initialize resources (NLS, etc)
    /// </summary>
    private void InitializeControlResources()
    {
      // NLS Strings    
      this.FormTitle = Resource.String("STR_FRM_SCAN_TITLE");
      this.btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");
      this.btn_scan.Text = Resource.String("STR_FRM_SCAN_SCAN");
      this.btn_configure.Text = Resource.String("STR_FRM_SCAN_CONFIG");
      this.btn_ok.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_OK");
      this.btn_rotate.Text = Resource.String("STR_FRM_SCAN_ROTATE");
      this.lbl_document_type.Text = Resource.String("STR_FRM_RECEPTION_SCAN_DOCUMENT_TYPE");
      this.lbl_expiration_date.Text = Resource.String("STR_FRM_RECEPTION_SCAN_EXPIRATION_DATE");
    }

    /// <summary>
    /// Load unused identification types
    /// </summary>
    /// <param name="CurrentDocument"></param>
    /// <param name="UsedScannedDocuments"></param>
    private void LoadDocumentTypes(ScannedDocument CurrentDocument, List<ScannedDocument> UsedScannedDocuments)
    {
      DataTable _dt_scan_id_types;

      _dt_scan_id_types = IdentificationTypes.GetIdentificationTypes(false);

      //remove used scanned documents
      if (CurrentDocument != null)
        UsedScannedDocuments.Remove(CurrentDocument);

      foreach (ScannedDocument _doc in UsedScannedDocuments)
      {
        if (_doc.DocumentTypeId != IdentificationTypes.OtherId)
          _dt_scan_id_types.Delete(string.Format("IDT_ID = {0}", _doc.DocumentTypeId));
      }

      _dt_scan_id_types.AcceptChanges();

      this.cb_document_type.DataSource = _dt_scan_id_types;
      this.cb_document_type.ValueMember = "IDT_ID";
      this.cb_document_type.DisplayMember = "IDT_NAME";

      // Select the default document type.
      this.cb_document_type.SelectedValue = GeneralParam.GetInt32("Account.DefaultValues", "DocumentType");
      // If not selected, select the first one.
      if (this.cb_document_type.SelectedValue == null)
      {
        this.cb_document_type.SelectedIndex = 0;
      }
    }

    /// <summary>
    /// Start scan operation
    /// </summary>
    private void ScanID()
    {
      if (m_id_scanner != null)
        m_id_scanner.DetatchFromAllAvailable(IDScannerEventHandler);

      if (m_id_scanner != null && m_id_scanner.AttatchToAllAvailable(IDScannerEventHandler) > 0)
      {
        m_id_scanner.ForceScan();
        return;

      }

      try
      {


        if (GeneralParam.GetBoolean("Account.ScanDocument", "UseFileDialog", false))
        {
          using (OpenFileDialog dialog = new OpenFileDialog())
          {
            dialog.Title = "Open Image";
            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
              m_scanned_image = new Bitmap(dialog.FileName);
              PreviewImage(m_scanned_image);
            }
          }
        }
        else
        {
          frm_scan_preview _frm_scan_preview;
          _frm_scan_preview = new frm_scan_preview();
          if (_frm_scan_preview.Show())
          {
            this.Cursor = Cursors.WaitCursor;
            m_scanned_image = Image.FromStream(new MemoryStream(File.ReadAllBytes(_frm_scan_preview.OutFileName)));
            ProcessScannedImage();
            this.Cursor = Cursors.Default;
            if (!_frm_scan_preview.OutFileName.IsNullOrWhiteSpace())
              File.Delete(_frm_scan_preview.OutFileName);
          }
          else
          {
            if (_frm_scan_preview.ErrorMessage != String.Empty)
            {
              lbl_msg_blink.Text = _frm_scan_preview.ErrorMessage;
              lbl_msg_blink.Visible = true;
            }
          }
        }
        //_frm_scan_preview.Dispose();
      }

      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      finally
      {
        this.Cursor = Cursors.Default;
      }
    }

    private void ProcessScannedImage()
    {
      if (InvokeRequired)
      {
        Invoke(new MethodInvoker(ProcessScannedImage));
        return;
      }
      RectangleF _rectangle_to_crop;
      String _selected_document_type;
      String _error_message;

      _selected_document_type = cb_document_type.SelectedValue.ToString();
      _error_message = String.Empty;
      lbl_msg_blink.Visible = false;

      try
      {
        DateTime _expdate = m_expiration_date ?? DateTime.Now;
        uc_dt_expiration.DateValue = _expdate == default(DateTime) ? DateTime.Now : _expdate;
      }
      catch
      {
        //swallow
      }
      //Get rectangle with the area and position values
      if (ScanImage.GetRectangleFromValues(_selected_document_type, out _rectangle_to_crop))
      {
        Image backupimage = m_scanned_image.Clone() as Image;
        //Crop Image
        if (!ScanImage.CropImage(m_scanned_image,
          _rectangle_to_crop,
          GeneralParam.GetBoolean("Cashier.Scan", "GrayScale"),
          GeneralParam.GetInt32("Cashier.Scan", "Quality", 90),
          out m_scanned_image))
        {
          m_scanned_image = backupimage.Clone() as Image;
          _error_message = Resource.String("STR_FRM_SCAN_MESSAGE_8");
        }
      }
      else //If rectangle values are bad
      {
        _error_message = Resource.String("STR_FRM_SCAN_MESSAGE_8");
      }

      //Delete scanned file from disk

      //Put the scanned image in the panel (Preview)
      PreviewImage(m_scanned_image);

      if (!String.IsNullOrEmpty(_error_message))
      {
        lbl_msg_blink.Text = _error_message;
        lbl_msg_blink.Visible = true;
      }

    }

    private void IDScannerEventHandler(IDScannerInfoEvent Event)
    {
      m_id_scanner.DetatchFromAllAvailable(IDScannerEventHandler);



      if (Event.info.Fields.ContainsKey(Event.info.IDScannerFixedFieldMapping[ENUM_IDScannerFixedField.FRONT]))
      {
        string _filename =
          Event.info.Fields[Event.info.IDScannerFixedFieldMapping[ENUM_IDScannerFixedField.FRONT]] as string;
        if (!string.IsNullOrEmpty(_filename) && File.Exists(_filename))
        {
          Image _img;
          using (var _bmp_temp = new Bitmap(_filename))
          {
            _img = new Bitmap(_bmp_temp);
          }
          m_scanned_image = _img;
          m_expiration_date =
            GetDateField(Event.info.Fields,
              Event.info.IDScannerFixedFieldMapping[ENUM_IDScannerFixedField.DOCUMENT_EXPIRY]) ?? default(DateTime);
        }
      }
      else
      {
        m_scanned_image = null;
      }
      ProcessScannedImage();
      GC.Collect();
      ThreadPool.QueueUserWorkItem(delegate
      {
        GC.WaitForPendingFinalizers();
        GC.Collect();
      });
    }

    private DateTime? GetDateField(Dictionary<string, object> Fields, string FieldName,
      DateTime Default = default(DateTime))
    {
      if (!Fields.ContainsKey(FieldName) || !(Fields[FieldName] as DateTime?).HasValue)
        return Default;
      return Fields[FieldName] as DateTime?;
    }


    private void btn_scan_Click(object sender, EventArgs e)
    {
      ScanID();
    }

    /// <summary>
    /// Show preview of image
    /// </summary>
    /// <param name="Image"></param>
    /// <returns></returns>
    private bool PreviewImage(Image Image)
    {
      try
      {
        if (Image == null)
        {
          return false;
        }

        pb_scanned_photo.Image = Image;
        pb_scanned_photo.Refresh();

        //Don't Show label error message
        lbl_msg_blink.Visible = false;

        return true;
      }
      catch (Exception Ex)
      {
        Log.Exception(Ex);
      }

      return false;
    }

    private void btn_rotate_Click(object sender, EventArgs e)
    {
      RotateCurrentImage();
    }

    /// <summary>
    /// Rotate the current image
    /// </summary>
    private void RotateCurrentImage()
    {
      try
      {
        if (m_scanned_image == null)
          return;

        // Rotate Image
        m_scanned_image.RotateFlip(RotateFlipType.Rotate90FlipNone);

        // Refresh the image preview and the Images List
        PreviewImage(this.m_scanned_image);
      }
      catch (Exception Ex)
      {
        Log.Error(Ex.Message);
      }
    }

    private void btn_ok_Click(object sender, EventArgs e)
    {
      if (Validate())
      {
        m_scanned_image = this.pb_scanned_photo.Image;
        m_document_type_id = (int) this.cb_document_type.SelectedValue;
        m_document_type_description = this.cb_document_type.Text;
        m_expiration_date = this.uc_dt_expiration.DateValue != DateTime.MinValue
          ? this.uc_dt_expiration.DateValue
          : default(Nullable<DateTime>);
        this.DialogResult = System.Windows.Forms.DialogResult.OK;
      }
    }

    /// <summary>
    /// Validate controls data
    /// </summary>
    /// <returns></returns>
    private new bool Validate()
    {
      if (this.pb_scanned_photo.Image == null)
      {
        lbl_msg_blink.Text = Resource.String("STR_FRM_RECEPTION_SCAN_ERROR_IMAGE");
        lbl_msg_blink.Visible = true;
        return false;
      }

      Nullable<DateTime> _selected_date = this.uc_dt_expiration.DateValue != DateTime.MinValue
        ? this.uc_dt_expiration.DateValue
        : default(Nullable<DateTime>);
      if (_selected_date.HasValue && _selected_date.Value.Date < WGDB.Now.Date)
      {
        lbl_msg_blink.Text = Resource.String("STR_FRM_RECEPTION_SCAN_ERROR_DATE");
        lbl_msg_blink.Visible = true;
        return false;
      }

      if (this.cb_document_type.SelectedValue == null)
      {
        lbl_msg_blink.Text = Resource.String("STR_FRM_RECEPTION_SCAN_ERROR_DOCUMENT_TYPE");
        lbl_msg_blink.Visible = true;
        return false;
      }

      return true;
    }

    /// <summary>
    /// Show the reception scan document dialog
    /// </summary>
    /// <param name="InitializeScanning">Start the scan inmediately</param>
    /// <param name="CurrentDocument">Current scanned document</param>
    /// <param name="UsedScannedDocuments">Used scanned documents</param>
    /// <returns></returns>
    public DialogResult Show(bool InitializeScanning, ScannedDocument CurrentDocument,
      List<ScannedDocument> UsedScannedDocuments, WSI.IDScanner.IDScanner IDScanner = null)
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_reception_scan_document", Log.Type.Message);

      m_id_scanner = IDScanner;
      using (form_yes_no = new frm_yesno())
      {
        form_yes_no.Opacity = 0.6;
        form_yes_no.Show();

        // Center screen
        if (WindowManager.StartUpMode == WindowManager.StartUpModes.Explorer)
        {
          this.Location = new Point(WindowManager.GetFormCenterLocation(form_yes_no).X - (this.Width/2)
            , WindowManager.GetFormCenterLocation(form_yes_no).Y - (form_yes_no.Height/2));
        }
        else
        {
          this.Location = new Point(1024/2 - this.Width/2, 0);
        }
        WSIKeyboard.Hide();

        LoadDocumentTypes(CurrentDocument, UsedScannedDocuments);

        if (CurrentDocument != null)
          SetDocumentData(CurrentDocument);
        else
          SetDefaultExpirationDate();

        if (InitializeScanning)
          m_must_scan_on_shown = true;

        this.ShowDialog(form_yes_no);

        form_yes_no.Hide();

        return this.DialogResult;
      }
    }

    private bool m_must_scan_on_shown = false;

    /// <summary>
    /// Set initial document data
    /// </summary>
    /// <param name="Document"></param>
    private void SetDocumentData(ScannedDocument Document)
    {
      this.pb_scanned_photo.Image = Document.Image.ToImage();
      m_scanned_image = this.pb_scanned_photo.Image;
      this.cb_document_type.SelectedValue = Document.DocumentTypeId;
      if (Document.Expiration.HasValue)
        this.uc_dt_expiration.DateValue = Document.Expiration.Value;
    }

    /// <summary>
    /// Set default expiration date
    /// </summary>
    private void SetDefaultExpirationDate()
    {
      this.uc_dt_expiration.DateValue = Customers.GetDefaultExpirationDate();
    }

    private void pb_scanned_photo_Click(object sender, EventArgs e)
    {
      if (m_scanned_image != null)
        ViewImage.ViewFullScreenImage(m_scanned_image);
    }

    private void frm_reception_scan_document_FormClosed(object sender, FormClosedEventArgs e)
    {
      if (m_id_scanner != null)
        m_id_scanner.DetatchFromAllAvailable(IDScannerEventHandler);
    }

    public DateTime m_expiry { get; set; }

    private void frm_reception_scan_document_Shown(object sender, EventArgs e)
    {
      if (m_must_scan_on_shown)
        btn_scan.PerformClick();

    }

    private void btn_configure_Click(object sender, EventArgs e)
    {
      SetEnableButtons(false);
      ScanImage.ConfigureScanner();
      SetEnableButtons(true);
    }

    private void SetEnableButtons(bool enabled)
    {
      this.btn_scan.Enabled = enabled;
      this.btn_rotate.Enabled = enabled;
      this.btn_configure.Enabled  = enabled;
      this.btn_cancel.Enabled = enabled;
      this.btn_ok.Enabled = enabled;
    }
  }
}
