//------------------------------------------------------------------------------
// Copyright � 2010 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: PrintMobileBankTickets.cs
// 
//   DESCRIPTION: Print all not-already printed vouchers from a cashier session
//                that are operations from Mobile Bank (MB_CASH_IN).
// 
//        AUTHOR: Raul Cervera
// 
// CREATION DATE: 16-SEP-2010
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 16-SEP-2010 RCI    First release.
// 09-MAR-2012 SSC    Added new Operation Codes for Note Acceptor
// 04-APR-2012 JAB    Changes to move operations to obtain and view tickets to the project WSI.Common
// 13-APR-2012 JAB    Changes to move print voucher functions to the project WSI.Common (VoucherManager.cs), remove print functions
// 07-NOV-2012 JMM    Added OperationCode.PROMOBOX_TOTAL_RECHARGES_PRINT to the listing vouchers
// 05-APR-2013 ICS    GetVouchersFromCashierSessionId() returns more columns (AO_OPERATION_ID, AO_CODE, AO_ACCOUNT_ID)
// 06-MAY-2013 HBB    Fixed Bug #684: Ticket reprint: Do not show  "System Sessions" neither "Note Acceptor Sessions"
// 03-SEP-2013 DRV    Fixed Bug Wig-184:Ticket reprint: Do not show some tickets
// 04-SEP-2013 NMR    Loading Ticket ID from last saved ticket-card in table cashier_vouchers
// 13-SEP-2013 NMR    Preventing view TITO vouchers in Cashless mode
// 23-SEP-2013 JRM    Code revision and unnecesary code removal
// 27-SEP-2013 NMR    Changed search tickets condition
// 07-OCT-2013 HBB    Moved the function GetFilteredTicketsList to frm_tickets_reprint
// 14-NOV-2013 JRM    Removed remaining traces of Tito functionallity in this file-
// 29-JAN-2014 DRV    Removed SysTitoUser cashier sessions from the grid
// 04-FEB-2014 DLL    Fixed Bug WIGOSTITO-1020: Removed SysGamingTable sessions of the grid
// 10-FEB-2014 LJM    Fixed Bug WIG-606: Added credit transfer to the filter
// 08-APR-2014 RMS    Forced index IX_session_id on GetLastVoucherInfo query
// 01-AUG-2014 JML    Fixed Bug WIG-1153 Tickets for CHIPS_PURCHASE_WITH_CASH_OUT are not displayed
// 19-JAN-2016 FOS    Backlog Item 7969: Apply Taxes in handpay authorization
// 24-MAR-2016 RAB    Bug 10745: Reception: with the GP requiereNewCard to "0" when assigning a card client is counted as "replacement"
// 31-MAR-2016 RGR    Product Backlog Item 10981:Sprint Review 21 Winions - Mex
// 06-MAY-2016 ETP    Fixed Bug 14116: Don't show Currency in points movements.
// 19-DIC-2016 ETP    Fixed Bug : Error Index 0 when opening cashier tickets
// 31-MAR-2017 ETP    PBI 25791: Payback vouchers.
// 18-JUN-2018 LQB    Bug 33149:WIGOS-13034 Prize payment is showing always 0 in voucher history screen and cashier
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using WSI.Common;
using System.Data;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using System.Drawing;
using System.Collections;

namespace WSI.Cashier
{
  class PrintMobileBankTickets
  {

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE: Get the current Cashier Session + 20 Last Sessions.
    //
    //  PARAMS:
    //      - INPUT:
    //          - Trx
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    public static DataTable LastSessions(SqlTransaction Trx)
    {
      DataTable _dt_sessions;
      StringBuilder _sql_txt;
      SqlCommand _sql_cmd;
      SqlDataAdapter _sql_adap;

      try
      {
        _dt_sessions = new DataTable("CASHIER_SESSIONS");

        _sql_txt = new StringBuilder();
        _sql_txt.AppendLine(" SELECT 1 AS CURRENT_SESSION, CS_SESSION_ID ");
        _sql_txt.AppendLine("        , CS_NAME ");
        _sql_txt.AppendLine("        , CS_STATUS ");
        _sql_txt.AppendLine("        , ' ' AS CS_STATUS_NAME ");
        _sql_txt.AppendLine("   FROM   CASHIER_SESSIONS ");
        _sql_txt.AppendLine("  WHERE   CS_SESSION_ID   = @pCashierSessionId ");
        _sql_txt.AppendLine("  UNION ");
        _sql_txt.AppendLine(" SELECT   0 AS CURRENT_SESSION, CS_SESSION_ID ");
        _sql_txt.AppendLine("        , CS_NAME ");
        _sql_txt.AppendLine("        , CS_STATUS ");
        _sql_txt.AppendLine("        , ' ' AS CS_STATUS_NAME ");
        _sql_txt.AppendLine("   FROM ( ");
        _sql_txt.AppendLine("          SELECT   TOP 20 CS_SESSION_ID ");
        _sql_txt.AppendLine("                 , CS_NAME ");
        _sql_txt.AppendLine("                 , CS_STATUS ");
        _sql_txt.AppendLine("            FROM   CASHIER_SESSIONS ");
        _sql_txt.AppendLine("      INNER JOIN   GUI_USERS ON CASHIER_SESSIONS.CS_USER_ID = GUI_USERS.GU_USER_ID ");
        _sql_txt.AppendLine("           WHERE   CS_SESSION_ID   <> @pCashierSessionId ");
        _sql_txt.AppendLine("             AND   GUI_USERS.GU_USER_TYPE NOT IN (@pSysSystemUser, @pSysAcceptorUser, @pSysTitoUser, @pSysGamingTable)");
        _sql_txt.AppendLine("        ORDER BY   1 DESC ");
        _sql_txt.AppendLine("        ) AS X ");
        _sql_txt.AppendLine("  ORDER BY 1 DESC, 2 DESC ");

        _sql_cmd = new SqlCommand(_sql_txt.ToString());
        _sql_cmd.Connection = Trx.Connection;
        _sql_cmd.Transaction = Trx;

        _sql_cmd.Parameters.Add("@pCashierSessionId", SqlDbType.BigInt).Value = Cashier.SessionId;
        _sql_cmd.Parameters.Add("@pSysSystemUser", SqlDbType.SmallInt).Value = GU_USER_TYPE.SYS_SYSTEM;
        _sql_cmd.Parameters.Add("@pSysAcceptorUser", SqlDbType.SmallInt).Value = GU_USER_TYPE.SYS_ACCEPTOR;
        _sql_cmd.Parameters.Add("@pSysTitoUser", SqlDbType.SmallInt).Value = GU_USER_TYPE.SYS_TITO;
        _sql_cmd.Parameters.Add("@pSysGamingTable", SqlDbType.SmallInt).Value = GU_USER_TYPE.SYS_GAMING_TABLE;

        _sql_adap = new SqlDataAdapter();
        _sql_adap.SelectCommand = _sql_cmd;
        _sql_adap.InsertCommand = null;
        _sql_adap.UpdateCommand = null;
        _sql_adap.DeleteCommand = null;

        _sql_adap.Fill(_dt_sessions);

        return _dt_sessions;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return null;
      }
    } // LastSessions



    //------------------------------------------------------------------------------
    // PURPOSE: Get Last 20 Operations for the CashierSessionId and OperationSource.
    //
    //  PARAMS:
    //      - INPUT:
    //          - CashierSessionId
    //          - OperationSource
    //          - Trx
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    public static DataTable LastOperationsFromCashierSessionId(Int64 CashierSessionId,
                                                               OperationSource OpSource,
                                                               SqlTransaction Trx)
    {
      String _codes;
      String _operation;
      String _join;
      DataTable _dt_operations;
      StringBuilder _sql_txt;
      SqlCommand _sql_cmd;
      SqlDataAdapter _sql_adap;
      Int64 _ident;
      VoucherSource _voucher_source;
      DateTime _voucher_date;
      DataRow _row;

      try
      {
        _sql_cmd = new SqlCommand();
        _sql_cmd.Connection = Trx.Connection;
        _sql_cmd.Transaction = Trx;

        _codes = "";
        _operation = "";
        _join = "INNER JOIN   ACCOUNTS ON AO_ACCOUNT_ID = AC_ACCOUNT_ID";
        _dt_operations = new DataTable("OPERATIONS");

        switch (OpSource)
        {
          case OperationSource.MOBILE_BANK:
            _codes = "    AND AO_CODE       IN ( @pCodeCs01, @pCodeCs02, @pCodeCs03, @pCodeCs04, @pCodeCs05) ";
            _sql_cmd.Parameters.Add("@pCodeCs01", SqlDbType.Int).Value = (Int32)OperationCode.MB_CASH_IN;
            _sql_cmd.Parameters.Add("@pCodeCs02", SqlDbType.Int).Value = (Int32)OperationCode.MB_PROMOTION;
            //SSC 09-MAR-2012: Added new Operation Codes for Note Acceptor
            _sql_cmd.Parameters.Add("@pCodeCs03", SqlDbType.Int).Value = (Int32)OperationCode.NA_CASH_IN;
            _sql_cmd.Parameters.Add("@pCodeCs04", SqlDbType.Int).Value = (Int32)OperationCode.NA_PROMOTION;
            _sql_cmd.Parameters.Add("@pCodeCs05", SqlDbType.Int).Value = (Int32)OperationCode.PROMOBOX_TOTAL_RECHARGES_PRINT;
            break;

          case OperationSource.CASHIER:
            _codes = "    AND AO_CODE       IN ( @pCodeCs01, @pCodeCs02, @pCodeCs03, @pCodeCs04, @pCodeCs05, @pCodeCs06, " +
                                                "@pCodeCs07, @pCodeCs08, @pCodeCs09, @pCodeCs10, @pCodeCs11, @pCodeCs12, " +
                                                "@pCodeCs13, @pCodeCs14, @pCodeCs15, @pCodeCs16, @pCodeCs17, @pCodeCs18, " +
                                                "@pCodeCs19, @pCodeCs20, @pCodeCs21, @pCodeCs22, @pCodeCs23, @pCodeCs24, " +
                                                "@pCodeCs25, @pCodeCs26, @pCodeCs27, @pCodeCs28, @pCodeCs29, @pCodeCs30, " +
                                                "@pCodeCs31, @pCodeCs32,@pCodeCs33)";
            _sql_cmd.Parameters.Add("@pCodeCs01", SqlDbType.Int).Value = (Int32)OperationCode.CASH_IN;
            _sql_cmd.Parameters.Add("@pCodeCs02", SqlDbType.Int).Value = (Int32)OperationCode.CASH_OUT;
            _sql_cmd.Parameters.Add("@pCodeCs03", SqlDbType.Int).Value = (Int32)OperationCode.PROMOTION;
            _sql_cmd.Parameters.Add("@pCodeCs04", SqlDbType.Int).Value = (Int32)OperationCode.DRAW_TICKET;
            _sql_cmd.Parameters.Add("@pCodeCs05", SqlDbType.Int).Value = (Int32)OperationCode.GIFT_DRAW_TICKET;
            _sql_cmd.Parameters.Add("@pCodeCs06", SqlDbType.Int).Value = (Int32)OperationCode.GIFT_REQUEST;
            _sql_cmd.Parameters.Add("@pCodeCs07", SqlDbType.Int).Value = (Int32)OperationCode.GIFT_DELIVERY;
            _sql_cmd.Parameters.Add("@pCodeCs08", SqlDbType.Int).Value = (Int32)OperationCode.GIFT_REDEEMABLE_AS_CASHIN;
            _sql_cmd.Parameters.Add("@pCodeCs09", SqlDbType.Int).Value = (Int32)OperationCode.CANCEL_GIFT_INSTANCE;
            _sql_cmd.Parameters.Add("@pCodeCs10", SqlDbType.Int).Value = (Int32)OperationCode.HANDPAY;
            _sql_cmd.Parameters.Add("@pCodeCs11", SqlDbType.Int).Value = (Int32)OperationCode.HANDPAY_CANCELLATION;
            _sql_cmd.Parameters.Add("@pCodeCs12", SqlDbType.Int).Value = (Int32)OperationCode.CHIPS_PURCHASE;
            _sql_cmd.Parameters.Add("@pCodeCs13", SqlDbType.Int).Value = (Int32)OperationCode.CHIPS_SALE;
            _sql_cmd.Parameters.Add("@pCodeCs14", SqlDbType.Int).Value = (Int32)OperationCode.CHIPS_SALE_WITH_RECHARGE;
            _sql_cmd.Parameters.Add("@pCodeCs15", SqlDbType.Int).Value = (Int32)OperationCode.TRANSFER_CREDIT_OUT;
            _sql_cmd.Parameters.Add("@pCodeCs16", SqlDbType.Int).Value = (Int32)OperationCode.TITO_OFFLINE;
            _sql_cmd.Parameters.Add("@pCodeCs17", SqlDbType.Int).Value = (Int32)OperationCode.CHIPS_PURCHASE_WITH_CASH_OUT;
            _sql_cmd.Parameters.Add("@pCodeCs18", SqlDbType.Int).Value = (Int32)OperationCode.CASH_ADVANCE;
            _sql_cmd.Parameters.Add("@pCodeCs19", SqlDbType.Int).Value = (Int32)OperationCode.CARD_REPLACEMENT;
            _sql_cmd.Parameters.Add("@pCodeCs20", SqlDbType.Int).Value = (Int32)OperationCode.TERMINAL_REFILL_HOPPER;
            _sql_cmd.Parameters.Add("@pCodeCs21", SqlDbType.Int).Value = (Int32)OperationCode.TERMINAL_COLLECT_DROPBOX;
            _sql_cmd.Parameters.Add("@pCodeCs22", SqlDbType.Int).Value = (Int32)OperationCode.HANDPAY_VALIDATION;
            _sql_cmd.Parameters.Add("@pCodeCs23", SqlDbType.Int).Value = (Int32)OperationCode.MULTIPLE_BUCKETS_MANUAL;

            // RAB 24-MAR-2016: Bug 10745: Add parameter to query for cashier movements.
            _sql_cmd.Parameters.Add("@pCodeCs24", SqlDbType.Int).Value = (Int32)OperationCode.CARD_ASSOCIATE;

            _sql_cmd.Parameters.Add("@pCodeCs25", SqlDbType.Int).Value = (Int32)OperationCode.PROMOTION_WITH_TAXES;
            _sql_cmd.Parameters.Add("@pCodeCs26", SqlDbType.Int).Value = (Int32)OperationCode.PRIZE_PAYOUT;
            _sql_cmd.Parameters.Add("@pCodeCs27", SqlDbType.Int).Value = (Int32)OperationCode.PRIZE_PAYOUT_AND_RECHARGE;
            _sql_cmd.Parameters.Add("@pCodeCs28", SqlDbType.Int).Value = (Int32)OperationCode.CHIPS_SWAP;
            _sql_cmd.Parameters.Add("@pCodeCs29", SqlDbType.Int).Value = (Int32)OperationCode.CURRENCY_EXCHANGE_CHANGE;
            _sql_cmd.Parameters.Add("@pCodeCs30", SqlDbType.Int).Value = (Int32)OperationCode.CURRENCY_EXCHANGE_DEVOLUTION;
            _sql_cmd.Parameters.Add("@pCodeCs31", SqlDbType.Int).Value = (Int32)OperationCode.REOPEN_OPERATION;
            _sql_cmd.Parameters.Add("@pCodeCs32", SqlDbType.Int).Value = (Int32)OperationCode.CLOSE_OPERATION;
            _sql_cmd.Parameters.Add("@pCodeCs33", SqlDbType.Int).Value = (Int32)OperationCode.CREDIT_LINE;

            break;

          case OperationSource.LAST_VOUCHER:
            if (!GetLastVoucherInfo(CashierSessionId, out _voucher_source, out _ident, out _voucher_date, Trx))
            {
              return null;
            }
            switch (_voucher_source)
            {
              case VoucherSource.FROM_OPERATION:
                _join = " LEFT JOIN   ACCOUNTS ON AO_ACCOUNT_ID = AC_ACCOUNT_ID";
                _operation = "    AND AO_OPERATION_ID = @pOperationId";
                _sql_cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt).Value = _ident;
                break;

              case VoucherSource.FROM_VOUCHER:
                // Define table column
                _dt_operations.Columns.Add("OPERATION_ID", Type.GetType("System.Int64"));
                _dt_operations.Columns.Add("VOUCHER_DATE", Type.GetType("System.DateTime"));
                _dt_operations.Columns.Add("CODE", Type.GetType("System.Int32"));
                _dt_operations.Columns.Add("CODE_NAME", Type.GetType("System.String"));
                _dt_operations.Columns.Add("AMOUNT", Type.GetType("System.Decimal"));
                _dt_operations.Columns.Add("ACCOUNT_ID", Type.GetType("System.Int64"));
                _dt_operations.Columns.Add("HOLDER_NAME", Type.GetType("System.String"));
                _dt_operations.Columns.Add("VOUCHER_SOURCE", Type.GetType("System.Int32"));
                _dt_operations.Columns.Add("CASHIER_MOVEMENT", Type.GetType("System.Int64"));

                // Add voucher as a fake operation
                _row = _dt_operations.NewRow();
                _row[0] = _ident;
                _row[1] = _voucher_date;
                _row[2] = 0;
                _row[3] = "";
                _row[4] = 0;
                _row[5] = 0;
                _row[6] = "";
                _row[7] = VoucherSource.FROM_VOUCHER;
                _dt_operations.Rows.Add(_row);

                return _dt_operations;

              default:
                return null;
            }
            break;

          default:
            return null;
        }

        _sql_txt = new StringBuilder();
        _sql_txt.AppendLine(" SELECT   TOP 300 ");
        _sql_txt.AppendLine("          AO_OPERATION_ID ");
        _sql_txt.AppendLine("        , AO_DATETIME ");
        _sql_txt.AppendLine("        , AO_CODE ");
        _sql_txt.AppendLine("        , ' ' AS AO_CODE_NAME ");
        _sql_txt.AppendLine("        , (CASE WHEN AO_CODE = " + (Int32)OperationCode.PRIZE_PAYOUT + " OR AO_CODE = " + (Int32)OperationCode.PRIZE_PAYOUT_AND_RECHARGE + " THEN AO_NON_REDEEMABLE ELSE AO_AMOUNT END) AO_AMOUNT ");
        _sql_txt.AppendLine("        , AO_ACCOUNT_ID ");
        _sql_txt.AppendLine("        , ISNULL(AC_HOLDER_NAME, '') ");
        _sql_txt.AppendLine("        , " + (Int32)VoucherSource.FROM_OPERATION + " AS VOUCHER_SOURCE ");

        // ETP 06-06-2016 El siguiente c�digo es para consultar el cashier_movement s�lo las OPERATIONS de BUCKETS.El objetivo es definir si se necesita el simbolo de moneda o no.
        // En un futuro si se crea algun otro cashier movewent asociado a la operacion OperationCode.MULTIPLE_BUCKETS_MANUAL quiz� hay que modificar la sub-query.
        _sql_txt.AppendLine("        , ( CASE                                                                               ");
        _sql_txt.AppendLine("                WHEN AO_CODE = @pBucketCode THEN (SELECT   TOP 1 CM_TYPE                       ");
        _sql_txt.AppendLine("                                                    FROM   CASHIER_MOVEMENTS                   ");
        _sql_txt.AppendLine("                                                   WHERE   CM_OPERATION_ID = AO_OPERATION_ID)  ");
        _sql_txt.AppendLine("                ELSE NULL                                                                      ");
        _sql_txt.AppendLine("             END ) AS CASHIER_MOVEMENT                                                         ");

        _sql_txt.AppendLine("   FROM   ACCOUNT_OPERATIONS ");
        _sql_txt.AppendLine(_join);
        _sql_txt.AppendLine("  WHERE   AO_CASHIER_SESSION_ID = @pCashierSessionId ");
        _sql_txt.AppendLine(_codes);
        _sql_txt.AppendLine(_operation);
        _sql_txt.AppendLine("  ORDER BY 2 DESC ");

        _sql_cmd.CommandText = _sql_txt.ToString();

        _sql_cmd.Parameters.Add("@pCashierSessionId", SqlDbType.BigInt).Value = CashierSessionId;
        _sql_cmd.Parameters.Add("@pBucketCode", SqlDbType.Int).Value = OperationCode.MULTIPLE_BUCKETS_MANUAL;

        _sql_adap = new SqlDataAdapter();
        _sql_adap.SelectCommand = _sql_cmd;
        _sql_adap.InsertCommand = null;
        _sql_adap.UpdateCommand = null;
        _sql_adap.DeleteCommand = null;

        _sql_adap.Fill(_dt_operations);

        return _dt_operations;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return null;
      }
    } // LastOperationsFromCashierSessionId

    //------------------------------------------------------------------------------
    // PURPOSE: Get Last Voucher as a fake Operation.
    //
    //  PARAMS:
    //      - INPUT:
    //          - CashierSessionId
    //          - Trx
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    private static Boolean GetLastVoucherInfo(Int64 CashierSessionId,
                                              out VoucherSource VoucherSource,
                                              out Int64 Ident,
                                              out DateTime VoucherDate,
                                              SqlTransaction Trx)
    {
      StringBuilder _sql_txt;
      SqlCommand _sql_cmd;
      SqlDataReader _sql_reader;
      Int64 _operation_id;

      VoucherSource = VoucherSource.FROM_VOUCHER;
      Ident = 0;
      VoucherDate = WGDB.Now;
      _sql_reader = null;

      try
      {
        _sql_cmd = new SqlCommand();
        _sql_cmd.Connection = Trx.Connection;
        _sql_cmd.Transaction = Trx;

        _sql_txt = new StringBuilder();
        _sql_txt.AppendLine(" SELECT   TOP 1 CV_VOUCHER_ID ");
        _sql_txt.AppendLine("        , CV_DATETIME ");
        _sql_txt.AppendLine("        , ISNULL(CV_OPERATION_ID, 0) ");
        _sql_txt.AppendLine("   FROM   CASHIER_VOUCHERS WITH (INDEX(IX_session_id)) ");
        _sql_txt.AppendLine("  WHERE   CV_SESSION_ID = @pCashierSessionId ");
        _sql_txt.AppendLine("    AND   CV_TYPE != @pVoucherType ");
        _sql_txt.AppendLine("  ORDER   BY 1 DESC ");

        _sql_cmd.CommandText = _sql_txt.ToString();

        _sql_cmd.Parameters.Add("@pCashierSessionId", SqlDbType.BigInt).Value = CashierSessionId;
        _sql_cmd.Parameters.Add("@pVoucherType", SqlDbType.SmallInt).Value = (Int16)CashierVoucherType.TicketOut;

        _sql_reader = _sql_cmd.ExecuteReader();
        if (_sql_reader.Read())
        {
          VoucherSource = VoucherSource.FROM_VOUCHER;
          Ident = _sql_reader.GetInt64(0);
          VoucherDate = _sql_reader.GetDateTime(1);
          _operation_id = _sql_reader.GetInt64(2);

          if (_operation_id > 0)
          {
            VoucherSource = VoucherSource.FROM_OPERATION;
            Ident = _operation_id;
          }

          return true;
        }

        return false;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return false;
      }
      finally
      {
        if (_sql_reader != null)
        {
          _sql_reader.Close();
        }
      }
    } // GetLastVoucherInfo

    //------------------------------------------------------------------------------
    // PURPOSE: Get all the Vouchers for the CashierSessionId and OperationSource.
    //
    //  PARAMS:
    //      - INPUT:
    //          - CashierSessionId
    //          - OperationSource
    //          - Trx
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //

    public static DataTable GetVouchersFromCashierSessionId(Int64 CashierSessionId,
                                                            OperationSource OpSource,
                                                            SqlTransaction Trx)
    {
      String _codes;
      DataTable _dt_vouchers;
      StringBuilder _sql_txt;
      SqlCommand _sql_cmd;
      SqlDataAdapter _sql_adap;

      try
      {
        _sql_cmd = new SqlCommand();
        _sql_cmd.Connection = Trx.Connection;
        _sql_cmd.Transaction = Trx;

        switch (OpSource)
        {
          case OperationSource.MOBILE_BANK:
            _codes = "    AND AO_CODE       IN ( @pCodeCs01, @pCodeCs02, @pCodeCs03, @pCodeCs04, @pCodeCs05 ) ";
            _sql_cmd.Parameters.Add("@pCodeCs01", SqlDbType.Int).Value = (Int32)OperationCode.MB_CASH_IN;
            _sql_cmd.Parameters.Add("@pCodeCs02", SqlDbType.Int).Value = (Int32)OperationCode.MB_PROMOTION;
            //SSC 09-MAR-2012: Added new Operation Codes for Note Acceptor
            _sql_cmd.Parameters.Add("@pCodeCs03", SqlDbType.Int).Value = (Int32)OperationCode.NA_CASH_IN;
            _sql_cmd.Parameters.Add("@pCodeCs04", SqlDbType.Int).Value = (Int32)OperationCode.NA_PROMOTION;
            _sql_cmd.Parameters.Add("@pCodeCs05", SqlDbType.Int).Value = (Int32)OperationCode.PROMOBOX_TOTAL_RECHARGES_PRINT;
            break;

          case OperationSource.CASHIER:
            _codes = "    AND AO_CODE       IN ( @pCodeCs01, @pCodeCs02, @pCodeCs03, @pCodeCs04, @pCodeCs05 )";
            _sql_cmd.Parameters.Add("@pCodeCs01", SqlDbType.Int).Value = (Int32)OperationCode.CASH_IN;
            _sql_cmd.Parameters.Add("@pCodeCs02", SqlDbType.Int).Value = (Int32)OperationCode.CASH_OUT;
            _sql_cmd.Parameters.Add("@pCodeCs03", SqlDbType.Int).Value = (Int32)OperationCode.PROMOTION;
            _sql_cmd.Parameters.Add("@pCodeCs04", SqlDbType.Int).Value = (Int32)OperationCode.DRAW_TICKET;
            _sql_cmd.Parameters.Add("@pCodeCs05", SqlDbType.Int).Value = (Int32)OperationCode.GIFT_DRAW_TICKET;
            break;

          default:
            return null;
        }

        _dt_vouchers = new DataTable("VOUCHERS");

        // ICS 05-APR-2013: Added 3 new columns in the _vouchers DataTable: AO_OPERATION_ID, AO_CODE and AO_ACCOUNT_ID needed for audit.

        _sql_txt = new StringBuilder();
        _sql_txt.AppendLine("SELECT   CV_VOUCHER_ID ");
        _sql_txt.AppendLine("       , CV_HTML ");
        _sql_txt.AppendLine("       , AO_OPERATION_ID ");
        _sql_txt.AppendLine("       , AO_CODE ");
        _sql_txt.AppendLine("       , AO_ACCOUNT_ID ");
        _sql_txt.AppendLine("  FROM   ACCOUNT_OPERATIONS ");
        _sql_txt.AppendLine("       , CASHIER_VOUCHERS ");
        _sql_txt.AppendLine(" WHERE   AO_OPERATION_ID       = CV_OPERATION_ID ");
        _sql_txt.AppendLine(_codes);
        _sql_txt.AppendLine("   AND   AO_CASHIER_SESSION_ID = @pCashierSessionId ");
        _sql_txt.AppendLine(" ORDER BY 1 ");

        _sql_cmd.CommandText = _sql_txt.ToString();

        _sql_cmd.Parameters.Add("@pCashierSessionId", SqlDbType.BigInt).Value = CashierSessionId;

        _sql_adap = new SqlDataAdapter();
        _sql_adap.SelectCommand = _sql_cmd;
        _sql_adap.InsertCommand = null;
        _sql_adap.UpdateCommand = null;
        _sql_adap.DeleteCommand = null;

        _sql_adap.Fill(_dt_vouchers);

        return _dt_vouchers;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return null;
      }
    } // GetVouchersFromCashierSessionId

    #endregion // Public Methods

    #region Private Methods

    #endregion // Private Methods

  } // PrintMobileBankTickets

} // WSI.Cashier
