namespace WSI.Cashier
{
  partial class frm_comment
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.btn_ok = new WSI.Cashier.Controls.uc_round_button();
      this.txt_block_comment = new WSI.Cashier.Controls.uc_round_textbox();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.Size = new System.Drawing.Size(450, 263);
      // 
      // btn_ok
      // 
      this.btn_ok.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_ok.FlatAppearance.BorderSize = 0;
      this.btn_ok.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_ok.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_ok.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_ok.Image = null;
      this.btn_ok.IsSelected = false;
      this.btn_ok.Location = new System.Drawing.Point(326, 262);
      this.btn_ok.Name = "btn_ok";
      this.btn_ok.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_ok.Size = new System.Drawing.Size(112, 48);
      this.btn_ok.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_ok.TabIndex = 1;
      this.btn_ok.Text = "XOK";
      this.btn_ok.UseVisualStyleBackColor = false;
      this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
      // 
      // txt_block_comment
      // 
      this.txt_block_comment.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txt_block_comment.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_block_comment.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_block_comment.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_block_comment.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_block_comment.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_block_comment.CornerRadius = 5;
      this.txt_block_comment.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_block_comment.Location = new System.Drawing.Point(12, 68);
      this.txt_block_comment.MaxLength = 100;
      this.txt_block_comment.Multiline = true;
      this.txt_block_comment.Name = "txt_block_comment";
      this.txt_block_comment.PasswordChar = '\0';
      this.txt_block_comment.ReadOnly = false;
      this.txt_block_comment.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
      this.txt_block_comment.SelectedText = "";
      this.txt_block_comment.SelectionLength = 0;
      this.txt_block_comment.SelectionStart = 0;
      this.txt_block_comment.Size = new System.Drawing.Size(426, 188);
      this.txt_block_comment.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.MULTILINE;
      this.txt_block_comment.TabIndex = 0;
      this.txt_block_comment.TabStop = false;
      this.txt_block_comment.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_block_comment.UseSystemPasswordChar = false;
      this.txt_block_comment.WaterMark = null;
      this.txt_block_comment.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // frm_comment
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.AutoSize = true;
      this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
      this.ClientSize = new System.Drawing.Size(450, 318);
      this.Controls.Add(this.btn_ok);
      this.Controls.Add(this.txt_block_comment);
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "frm_comment";
      this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Style = WSI.Cashier.Controls.frm_base.BaseFormStyle.PRINCIPAL;
      this.Text = "Message";
      this.Controls.SetChildIndex(this.pnl_data, 0);
      this.Controls.SetChildIndex(this.txt_block_comment, 0);
      this.Controls.SetChildIndex(this.btn_ok, 0);
      this.ResumeLayout(false);

    }

    #endregion

    private WSI.Cashier.Controls.uc_round_button btn_ok;
    private WSI.Cashier.Controls.uc_round_textbox txt_block_comment;


  }
}