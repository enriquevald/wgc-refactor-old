namespace WSI.Cashier
{
  partial class frm_input_tokens
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.txt_num_tokens = new WSI.Cashier.Controls.uc_round_textbox();
      this.lbl_enter_num_tokens = new WSI.Cashier.Controls.uc_label();
      this.btn_ok = new WSI.Cashier.Controls.uc_round_button();
      this.btn_cancel = new WSI.Cashier.Controls.uc_round_button();
      this.pnl_data.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.Controls.Add(this.btn_ok);
      this.pnl_data.Controls.Add(this.btn_cancel);
      this.pnl_data.Controls.Add(this.txt_num_tokens);
      this.pnl_data.Controls.Add(this.lbl_enter_num_tokens);
      this.pnl_data.Size = new System.Drawing.Size(512, 181);
      this.pnl_data.TabIndex = 0;
      this.pnl_data.Paint += new System.Windows.Forms.PaintEventHandler(this.pnl_data_Paint);
      // 
      // txt_num_tokens
      // 
      this.txt_num_tokens.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_num_tokens.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_num_tokens.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.txt_num_tokens.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.txt_num_tokens.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_num_tokens.CornerRadius = 5;
      this.txt_num_tokens.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_num_tokens.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.txt_num_tokens.Location = new System.Drawing.Point(213, 55);
      this.txt_num_tokens.MaxLength = 3;
      this.txt_num_tokens.Multiline = false;
      this.txt_num_tokens.Name = "txt_num_tokens";
      this.txt_num_tokens.PasswordChar = '\0';
      this.txt_num_tokens.ReadOnly = false;
      this.txt_num_tokens.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_num_tokens.SelectedText = "";
      this.txt_num_tokens.SelectionLength = 0;
      this.txt_num_tokens.SelectionStart = 0;
      this.txt_num_tokens.Size = new System.Drawing.Size(80, 40);
      this.txt_num_tokens.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_num_tokens.TabIndex = 1;
      this.txt_num_tokens.TabStop = false;
      this.txt_num_tokens.Text = "x1234";
      this.txt_num_tokens.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.txt_num_tokens.UseSystemPasswordChar = false;
      this.txt_num_tokens.WaterMark = null;
      this.txt_num_tokens.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_num_tokens_KeyPress);
      // 
      // lbl_enter_num_tokens
      // 
      this.lbl_enter_num_tokens.AutoSize = true;
      this.lbl_enter_num_tokens.BackColor = System.Drawing.Color.Transparent;
      this.lbl_enter_num_tokens.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_enter_num_tokens.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.lbl_enter_num_tokens.Location = new System.Drawing.Point(171, 21);
      this.lbl_enter_num_tokens.Name = "lbl_enter_num_tokens";
      this.lbl_enter_num_tokens.Size = new System.Drawing.Size(157, 18);
      this.lbl_enter_num_tokens.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_enter_num_tokens.TabIndex = 0;
      this.lbl_enter_num_tokens.Text = "xNone enter number";
      this.lbl_enter_num_tokens.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // btn_ok
      // 
      this.btn_ok.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.btn_ok.FlatAppearance.BorderSize = 0;
      this.btn_ok.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_ok.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_ok.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.btn_ok.Image = null;
      this.btn_ok.Location = new System.Drawing.Point(263, 110);
      this.btn_ok.Name = "btn_ok";
      this.btn_ok.Size = new System.Drawing.Size(205, 50);
      this.btn_ok.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_ok.TabIndex = 3;
      this.btn_ok.Text = "XOK";
      this.btn_ok.UseVisualStyleBackColor = false;
      this.btn_ok.Click += new System.EventHandler(this.btn_ok_click);
      // 
      // btn_cancel
      // 
      this.btn_cancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.btn_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btn_cancel.FlatAppearance.BorderSize = 0;
      this.btn_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_cancel.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_cancel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.btn_cancel.Image = null;
      this.btn_cancel.Location = new System.Drawing.Point(37, 110);
      this.btn_cancel.Name = "btn_cancel";
      this.btn_cancel.Size = new System.Drawing.Size(205, 50);
      this.btn_cancel.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_cancel.TabIndex = 2;
      this.btn_cancel.Text = "XCANCEL";
      this.btn_cancel.UseVisualStyleBackColor = false;
      this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
      // 
      // frm_input_tokens
      // 
      this.AcceptButton = this.btn_ok;
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.btn_cancel;
      this.ClientSize = new System.Drawing.Size(512, 236);
      this.ControlBox = false;
      this.HeaderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.Name = "frm_input_tokens";
      this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
      this.Style = WSI.Cashier.Controls.frm_base.BaseFormStyle.PRINCIPAL;
      this.Text = "xNum Tokens";
      this.Shown += new System.EventHandler(this.frm_input_tokens_Shown);
      this.pnl_data.ResumeLayout(false);
      this.pnl_data.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private WSI.Cashier.Controls.uc_round_button btn_cancel;
    private WSI.Cashier.Controls.uc_round_button btn_ok;
    private WSI.Cashier.Controls.uc_round_textbox txt_num_tokens;
    private WSI.Cashier.Controls.uc_label lbl_enter_num_tokens;
  }
}