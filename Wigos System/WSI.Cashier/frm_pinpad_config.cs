﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_pinpad_config.cs
// 
//   DESCRIPTION: Configure PinPads
// 
//        AUTHOR: Alberto Marcos
// 
// CREATION DATE: 21-JUN-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 21-JUN-2016 AMF    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using WSI.Cashier.Controls;
using WSI.Common;
using WSI.Common.PinPad;

namespace WSI.Cashier
{
  public partial class frm_pinpad_config : frm_base
  {
    [DllImport("gdi32.dll")]
    private static extern IntPtr AddFontMemResourceEx(IntPtr pbFont, uint cbFont, IntPtr pdv, [In] ref uint pcFonts);

    #region  " Members "

    private PinPadCashierTerminals m_pinpads;

    #endregion " Members "

    #region " Constants "

    private const Int32 GRID_COLUMN_ID = 0;
    private const Int32 GRID_COLUMN_TYPE = 1;
    private const Int32 GRID_COLUMN_TYPE_NAME = 2;
    private const Int32 GRID_COLUMN_PORT = 3;
    private const Int32 GRID_COLUMN_PORT_NAME = 4;
    private const Int32 GRID_COLUMN_ENABLED = 5;
    private const Int32 GRID_COLUMN_ENABLED_NAME = 6;

    #endregion " Constants "

    #region " Public Method "

    /// <summary>
    /// Constructor
    /// </summary>
    public frm_pinpad_config()
    {
      InitializeComponent();

      InitializeControlResources();

      m_pinpads = new PinPadCashierTerminals();
      m_pinpads.CashierId = (Int32)Cashier.TerminalId;

      RefreshData();
    }

    #endregion " Public Method "

    #region " Private Method "

    /// <summary>
    /// Initialize control resources (NLS strings, images, etc)
    /// </summary>
    private void InitializeControlResources()
    {
      this.FormTitle = Resource.String("STR_FRM_PINPAD_CONFIG_TITLE");
      this.btn_close.Text = Resource.String("STR_FRM_PINPAD_CONFIG_BTN_CLOSE");
      this.btn_edit.Text = Resource.String("STR_FRM_PINPAD_CONFIG_BTN_EDIT");
      this.btn_new.Text = Resource.String("STR_FRM_PINPAD_CONFIG_BTN_NEW");

    } // InitializeControlResources

    /// <summary>
    /// Refresh Data
    /// </summary>
    private void RefreshData()
    {
      Int32 _idx_row;
      PinPadType _type;
      PinPadCashierTerminal.PINPAD_PORT _port;
      Boolean _enabled;

      m_pinpads.DB_GetDataTableByCashierId();
      dgw_pinpads.DataSource = m_pinpads.PinPads;
      if (m_pinpads.PinPads.Rows.Count == Enum.GetNames(typeof(WSI.Common.PinPadType)).Length)
      {
        this.btn_new.Enabled = false;
      }
      else
      {
        this.btn_new.Enabled = true;
      }

      FormatGridView();

      if (m_pinpads.PinPads.Rows.Count == 0)
      {
        this.btn_edit.Enabled = false;
      }
      else
      {
        this.btn_edit.Enabled = true;
      }

      for (_idx_row = 0; _idx_row < dgw_pinpads.Rows.Count; _idx_row++)
      {
        _type = (PinPadType)dgw_pinpads.Rows[_idx_row].Cells[GRID_COLUMN_TYPE].Value;
        dgw_pinpads.Rows[_idx_row].Cells[GRID_COLUMN_TYPE_NAME].Value = _type.ToString();

        _port = (PinPadCashierTerminal.PINPAD_PORT)dgw_pinpads.Rows[_idx_row].Cells[GRID_COLUMN_PORT].Value;
        dgw_pinpads.Rows[_idx_row].Cells[GRID_COLUMN_PORT_NAME].Value = _port.ToString();

        _enabled = (Boolean)dgw_pinpads.Rows[_idx_row].Cells[GRID_COLUMN_ENABLED].Value;
        dgw_pinpads.Rows[_idx_row].Cells[GRID_COLUMN_ENABLED_NAME].Value = _enabled ? "Si" : "No";

      }

    } // RefreshData

    /// <summary>
    /// Format data grid view
    /// </summary>
    private void FormatGridView()
    {
      dgw_pinpads.RowTemplate.DividerHeight = 0;

      // Id
      dgw_pinpads.Columns[GRID_COLUMN_ID].Width = 0;
      dgw_pinpads.Columns[GRID_COLUMN_ID].Visible = false;

      // Type
      dgw_pinpads.Columns[GRID_COLUMN_TYPE].Width = 0;
      dgw_pinpads.Columns[GRID_COLUMN_TYPE].Visible = false;

      // Type Name
      dgw_pinpads.Columns[GRID_COLUMN_TYPE_NAME].HeaderCell.Value = Resource.String("STR_FRM_PINPAD_CONFIG_DGV_TYPE");
      dgw_pinpads.Columns[GRID_COLUMN_TYPE_NAME].Width = 120;
      dgw_pinpads.Columns[GRID_COLUMN_TYPE_NAME].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

      // Port
      dgw_pinpads.Columns[GRID_COLUMN_PORT].Width = 0;
      dgw_pinpads.Columns[GRID_COLUMN_PORT].Visible = false;

      // Port Name
      dgw_pinpads.Columns[GRID_COLUMN_PORT_NAME].HeaderCell.Value = Resource.String("STR_FRM_PINPAD_CONFIG_DGV_PORT");
      dgw_pinpads.Columns[GRID_COLUMN_PORT_NAME].Width = 120;
      dgw_pinpads.Columns[GRID_COLUMN_PORT_NAME].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

      // Enabled
      dgw_pinpads.Columns[GRID_COLUMN_ENABLED].Width = 0;
      dgw_pinpads.Columns[GRID_COLUMN_ENABLED].Visible = false;

      // Enabled
      dgw_pinpads.Columns[GRID_COLUMN_ENABLED_NAME].HeaderCell.Value = Resource.String("STR_FRM_PINPAD_CONFIG_DGV_ACTIVE");
      dgw_pinpads.Columns[GRID_COLUMN_ENABLED_NAME].Width = 120;
      dgw_pinpads.Columns[GRID_COLUMN_ENABLED_NAME].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

    } // FormatGridView

    #region " Events "

    /// <summary>
    /// Button Close
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btn_close_Click(object sender, EventArgs e)
    {
      Misc.WriteLog("[FORM CLOSE] frm_pinpad_config (close)", Log.Type.Message);
      this.Close();
    } // btn_close_Click

    /// <summary>
    /// Button New
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btn_new_Click(object sender, EventArgs e)
    {
      frm_pinpad_config_edit _pinpad_config_edit;

      _pinpad_config_edit = new frm_pinpad_config_edit(frm_pinpad_config_edit.DISPLAY_MODE.NEW);
      _pinpad_config_edit.PinPands = m_pinpads.PinPads;
      _pinpad_config_edit.ShowDialog(this);

      _pinpad_config_edit.Hide();
      _pinpad_config_edit.Dispose();

      RefreshData();
    } // btn_new_Click

    /// <summary>
    /// Button Edit
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btn_edit_Click(object sender, EventArgs e)
    {
      frm_pinpad_config_edit _pinpad_config_edit;
      PinPadCashierTerminal _pinpad;

      _pinpad = new PinPadCashierTerminal();
      _pinpad.Id = (Int32)dgw_pinpads.SelectedRows[0].Cells[GRID_COLUMN_ID].Value;
      _pinpad.CashierId = m_pinpads.CashierId;
      _pinpad.Type = (WSI.Common.PinPadType)dgw_pinpads.SelectedRows[0].Cells[GRID_COLUMN_TYPE].Value;
      _pinpad.Port = (PinPadCashierTerminal.PINPAD_PORT)dgw_pinpads.SelectedRows[0].Cells[GRID_COLUMN_PORT].Value;
      _pinpad.Enabled = (Boolean)dgw_pinpads.SelectedRows[0].Cells[GRID_COLUMN_ENABLED].Value;

      _pinpad_config_edit = new frm_pinpad_config_edit(frm_pinpad_config_edit.DISPLAY_MODE.EDIT, _pinpad);
      _pinpad_config_edit.ShowDialog(this);

      _pinpad_config_edit.Hide();
      _pinpad_config_edit.Dispose();

      RefreshData();
    } // btn_edit_Click

    #endregion " Events "

    #endregion " Private Method "

  }
}
