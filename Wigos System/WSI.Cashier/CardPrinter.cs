//------------------------------------------------------------------------------
// Copyright © 2009 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: CardPrinter.cs
// 
//   DESCRIPTION: Class to manage the Card Printer.
// 
//        AUTHOR: Ronald Rodríguez T.
// 
// CREATION DATE: 19-NOV-2009
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 19-NOV-2009 RRT    First release.
// 03-OCT-2013 RMS    WIG-256: Eliminates unnecessary call of method SendDocToPrint in PrintNameInCard()
// 18-JUN-2018 LQB    Bug 33149:WIGOS-13034 Prize payment is showing always 0 in voucher history screen and cashier
// PRUEBA DE CAMBIOS
//---------------------------------------------------------------------------------
using System;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Printing;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using WSI.Common;

using Microsoft.Win32;

namespace WSI.Cashier
{
  public class CardPrinter
  {
    #region Constants

    private const String CardPrintDocName = "testcard.txt";
    private const String CardPrintDefaultName = "Magicard Pronto";

    #endregion

    #region Enums

    #endregion

    #region Class Attributes

    private String PrinterName;
    private TextPrintDocument PrinterDocument;

    #endregion

    #region Constructor

    public CardPrinter(String Printer)
    {
      PrinterName = Printer;
    }

    public CardPrinter()
    {
      PrinterName = Db_GetCardPrinterName();
    }
    
    #endregion

    #region Delegates

    delegate void PrintInBackgroundDelegate();

    #endregion

    #region Private Methods

    /// <summary>
    /// Print completed callback method.
    /// </summary>
    /// <param name="r"></param>
    //private void PrintInBackgroundComplete(IAsyncResult r)
    //{
    //}


    /// <summary>
    /// Obtain the print area parameters.
    /// </summary>
    /// <param name="TopX"></param>
    /// <param name="TopY"></param>
    /// <param name="BottomX"></param>
    /// <param name="BottomY"></param>
    /// <param name="FontSize"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    private static Boolean Db_GetCardPrintAreaValues(out Int32 TopX, out Int32 TopY, out Int32 BottomX, out Int32 BottomY, out Int32 FontSize, out Int32 FontBold)
    {
      SqlCommand _sql_command;
      SqlTransaction sql_trx;
      SqlConnection sql_conn;
      String sql_str;
      Object _obj;

      TopX = 0;
      TopY = 0;
      BottomX = 0;
      BottomY = 0;
      FontSize = 10;
      FontBold = 0;

      sql_conn = null;
      sql_trx = null;

      try
      {
        // Get Sql Connection 
        sql_conn = WSI.Common.WGDB.Connection();
        sql_trx = sql_conn.BeginTransaction();

        // Obtain print area values:
        // 1. TopX
        // 2. TopY
        // 3. BottomX
        // 4. BottomY
        // 5. Font Size
        // 6. Bold

        // 1. TopX
        sql_str = "";
        sql_str += "SELECT   CAST (GP_KEY_VALUE AS INT) ";
        sql_str += "  FROM   GENERAL_PARAMS ";
        sql_str += " WHERE   GP_GROUP_KEY   = 'Cashier.CardPrint' ";
        sql_str += "   AND   GP_SUBJECT_KEY = 'PrintNameArea_TopX' ";

        _sql_command = new SqlCommand(sql_str);
        _sql_command.Connection = sql_trx.Connection;
        _sql_command.Transaction = sql_trx;

        _obj = _sql_command.ExecuteScalar();
        if (_obj != null)
        {
          TopX = (Int32)Convert.ToDecimal(Format.DBNumberToLocalNumber(_obj.ToString()));
        }

        // 2. TopY
        sql_str = "";
        sql_str += "SELECT   CAST (GP_KEY_VALUE AS INT) ";
        sql_str += "  FROM   GENERAL_PARAMS ";
        sql_str += " WHERE   GP_GROUP_KEY   = 'Cashier.CardPrint' ";
        sql_str += "   AND   GP_SUBJECT_KEY = 'PrintNameArea_TopY' ";

        _sql_command = new SqlCommand(sql_str);
        _sql_command.Connection = sql_trx.Connection;
        _sql_command.Transaction = sql_trx;

        _obj = _sql_command.ExecuteScalar();
        if (_obj != null)
        {
          TopY = (Int32)Convert.ToDecimal(Format.DBNumberToLocalNumber(_obj.ToString()));
        }

        // 3. BottomX
        sql_str = "";
        sql_str += "SELECT   CAST (GP_KEY_VALUE AS INT) ";
        sql_str += "  FROM   GENERAL_PARAMS ";
        sql_str += " WHERE   GP_GROUP_KEY   = 'Cashier.CardPrint' ";
        sql_str += "   AND   GP_SUBJECT_KEY = 'PrintNameArea_BottomX' ";

        _sql_command = new SqlCommand(sql_str);
        _sql_command.Connection = sql_trx.Connection;
        _sql_command.Transaction = sql_trx;

        _obj = _sql_command.ExecuteScalar();
        if (_obj != null)
        {
          BottomX = (Int32)Convert.ToDecimal(Format.DBNumberToLocalNumber(_obj.ToString()));
        }

        // 4. BottomY
        sql_str = "";
        sql_str += "SELECT   CAST (GP_KEY_VALUE AS INT) ";
        sql_str += "  FROM   GENERAL_PARAMS ";
        sql_str += " WHERE   GP_GROUP_KEY   = 'Cashier.CardPrint' ";
        sql_str += "   AND   GP_SUBJECT_KEY = 'PrintNameArea_BottomY' ";

        _sql_command = new SqlCommand(sql_str);
        _sql_command.Connection = sql_trx.Connection;
        _sql_command.Transaction = sql_trx;

        _obj = _sql_command.ExecuteScalar();
        if (_obj != null)
        {
          BottomY = (Int32)Convert.ToDecimal(Format.DBNumberToLocalNumber(_obj.ToString()));
        }

        // 5. Font Size
        sql_str = "";
        sql_str += "SELECT   CAST (GP_KEY_VALUE AS INT) ";
        sql_str += "  FROM   GENERAL_PARAMS ";
        sql_str += " WHERE   GP_GROUP_KEY   = 'Cashier.CardPrint' ";
        sql_str += "   AND   GP_SUBJECT_KEY = 'PrintNameArea_FontSize' ";

        _sql_command = new SqlCommand(sql_str);
        _sql_command.Connection = sql_trx.Connection;
        _sql_command.Transaction = sql_trx;

        _obj = _sql_command.ExecuteScalar();
        if (_obj != null)
        {
          FontSize = (Int32)Convert.ToDecimal(Format.DBNumberToLocalNumber(_obj.ToString()));
        }

        // 6. Font Bold
        sql_str = "";
        sql_str += "SELECT   CAST (GP_KEY_VALUE AS INT) ";
        sql_str += "  FROM   GENERAL_PARAMS ";
        sql_str += " WHERE   GP_GROUP_KEY   = 'Cashier.CardPrint' ";
        sql_str += "   AND   GP_SUBJECT_KEY = 'PrintNameArea_Bold' ";

        _sql_command = new SqlCommand(sql_str);
        _sql_command.Connection = sql_trx.Connection;
        _sql_command.Transaction = sql_trx;

        _obj = _sql_command.ExecuteScalar();
        if (_obj != null)
        {
          FontBold = (Int32)_obj;
        }

      }
      catch (Exception ex)
      {
        Log.Error("Db_GetCardPrintAreaValues.Error reading Db values. " + ex.Message);

        return false;
      }
      finally
      {
        _sql_command = null;
        sql_trx = null;
        sql_conn = null;
      }

      return true;

    } // Db_GetCardPrintAreaValues


    private String Db_GetCardPrinterName()
    {
      String card_printer_name;
      String read_value;
      SqlCommand sql_command;
      SqlTransaction sql_trx;
      SqlConnection sql_conn;
      String sql_str;

      sql_conn = null;
      sql_trx = null;

      // Default value
      card_printer_name = CardPrintDefaultName;

      try
      {
        // Get Sql Connection 
        sql_conn = WSI.Common.WGDB.Connection();
        sql_trx = sql_conn.BeginTransaction();

        // Get general parameters key
        sql_str = "";
        sql_str += "SELECT   GP_KEY_VALUE ";
        sql_str += "  FROM   GENERAL_PARAMS ";
        sql_str += " WHERE   GP_GROUP_KEY   = 'Cashier.CardPrint' ";
        sql_str += "   AND   GP_SUBJECT_KEY = 'QueueName' ";

        sql_command = new SqlCommand(sql_str);
        sql_command.Connection = sql_trx.Connection;
        sql_command.Transaction = sql_trx;

        read_value = (String)sql_command.ExecuteScalar();

        if (read_value != null)
        {
          card_printer_name = read_value;
        }
      }
      catch (Exception ex)
      {
        Log.Error("Db_GetCardPrinterName.Error reading Db values. " + ex.Message);
      }
      finally
      {
        sql_command = null;
        sql_trx = null;
        sql_conn = null;
      }

      return card_printer_name;
    } // Db_GetCardPrinterName

    /// <summary>
    /// Settings for card printing.
    /// </summary>
    /// <returns></returns>
    private Boolean SetPrinterSettings(Int32 TopX, Int32 TopY, Int32 BottomX, Int32 BottomY)
    {
      try
      {
        PrinterDocument.PrinterSettings.MaximumPage = 1;
        PrinterDocument.PrinterSettings.ToPage = 1;

        PrinterDocument.DefaultPageSettings.Landscape = true;
        PrinterDocument.DefaultPageSettings.Margins.Top = TopY;
        PrinterDocument.DefaultPageSettings.Margins.Left = TopX;

        PrinterDocument.DefaultPageSettings.Margins.Bottom = BottomX;
        PrinterDocument.DefaultPageSettings.Margins.Right = BottomY;
      }
      catch(Exception ex)
      {
        Log.Exception(ex);

        return false;
      }

      return true;
    } // SetPrinterSettings


    /// <summary>
    /// Print in background method.
    /// </summary>
    //private void PrintInBackground()
    //{
    //  try
    //  {
    //    PrinterDocument.Print();
    //  }
    //  catch (Exception ex)
    //  {
    //    Log.Exception(ex);
    //  }
    //}

    /// <summary>
    /// Send a document to Print:
    /// 1.- Set the printer to use.
    /// 2.- Checks the printer is valid.
    /// 
    /// Checks if the card printer is installed on the computer.
    /// </summary>
    /// <returns></returns>
    private Boolean SendDocToPrint()
    {
      StandardPrintController print_control = new StandardPrintController();

      try
      {
        PrinterDocument.PrintController = print_control;

        if (PrinterDocument.PrinterSettings.IsValid  )
        {
          //PrintInBackgroundDelegate d = new PrintInBackgroundDelegate(PrintInBackground);
          //d.BeginInvoke(new AsyncCallback(PrintInBackgroundComplete), null);

         PrinterDocument.Print();
        }
        else
        {
          Log.Error("SendDocToPrint. Printer is not valid: " + PrinterName);

          return false;
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);

        return false;
      }

      return true;
    } // SendDocToPrint

    #endregion

    #region Protected Methods

    #endregion


    #region Public Methods

    /// <summary>
    /// Check if the printer exists.
    /// </summary>
    /// <returns></returns>
    public Boolean PrinterExist()
    {
      try
      {
        foreach (String printer_name in PrinterSettings.InstalledPrinters)
        {
          if (String.Compare(printer_name, PrinterName) == 0)
          {
            return true;
          }
        }
      }
      catch (Exception ex)
      {
        Log.Error("WSI.Cashier.CardPrinter.PrinterExist. - " + ex.Message);
      }

      return false;
    } // PrinterExist


    /// <summary>
    /// Saves the holder name in a file and send it to print
    /// </summary>
    public void PrintNameInCard(String HolderName)
    {
      Int32 topX = 0;
      Int32 topY = 0;
      Int32 bottomX = 0;
      Int32 bottomY = 0;
      Int32 fontsize = 10;
      Int32 fontbold = 0;

      PrinterDocument = new TextPrintDocument("CardHolder.txt");

      PrinterDocument.SaveDocumentFile(HolderName);

      PrinterDocument.SetPrinterForDoc(PrinterName);

      if (!Db_GetCardPrintAreaValues(out topX, out topY, out bottomX, out bottomY, out fontsize, out fontbold))
      {
        Log.Error("Db_GetCardPrintAreaValues. Error getting print area values from database.Using Default values.");
      }

      if (SetPrinterSettings(topX, topY, bottomX, bottomY))
      {
        if (fontbold == 1)
        {
          PrinterDocument.Font = new Font("Verdana", fontsize, FontStyle.Bold);
        }
        else
        {
          PrinterDocument.Font = new Font("Verdana", fontsize);
        }

        SendDocToPrint();
      }
    } // PrintNameInCard


    #endregion

	}
}
