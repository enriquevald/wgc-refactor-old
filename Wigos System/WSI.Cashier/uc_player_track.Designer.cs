//////namespace WSI.Cashier
//////{
//////  partial class uc_player_track
//////  {
//////    /// <summary> 
//////    /// Required designer variable.
//////    /// </summary>
//////    private System.ComponentModel.IContainer components = null;

//////    /// <summary> 
//////    /// Clean up any resources being used.
//////    /// </summary>
//////    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
//////    protected override void Dispose(bool disposing)
//////    {
//////      if (disposing && (components != null))
//////      {
//////        components.Dispose();
//////      }
//////      base.Dispose(disposing);
//////    }

//////    #region Component Designer generated code

//////    /// <summary> 
//////    /// Required method for Designer support - do not modify 
//////    /// the contents of this method with the code editor.
//////    /// </summary>
//////    private void InitializeComponent()
//////    {
//////      this.components = new System.ComponentModel.Container();
//////      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(uc_player_track));
//////      this.lbl_cards_title = new System.Windows.Forms.Label();
//////      this.lbl_separator_1 = new System.Windows.Forms.Label();
//////      this.groupBox2 = new System.Windows.Forms.GroupBox();
//////      this.lbl_id_title = new System.Windows.Forms.Label();
//////      this.lbl_id = new System.Windows.Forms.Label();
//////      this.lbl_marital_status = new System.Windows.Forms.Label();
//////      this.lbl_marital_status_title = new System.Windows.Forms.Label();
//////      this.lbl_emails_2 = new System.Windows.Forms.Label();
//////      this.gb_comments = new System.Windows.Forms.GroupBox();
//////      this.lbl_comments = new System.Windows.Forms.Label();
//////      this.gb_address = new System.Windows.Forms.GroupBox();
//////      this.lbl_address_01 = new System.Windows.Forms.Label();
//////      this.lbl_address_02 = new System.Windows.Forms.Label();
//////      this.lbl_address_03 = new System.Windows.Forms.Label();
//////      this.lbl_zip_title = new System.Windows.Forms.Label();
//////      this.lbl_city = new System.Windows.Forms.Label();
//////      this.lbl_city_title = new System.Windows.Forms.Label();
//////      this.lbl_zip = new System.Windows.Forms.Label();
//////      this.lbl_address_03_title = new System.Windows.Forms.Label();
//////      this.lbl_address_01_title = new System.Windows.Forms.Label();
//////      this.lbl_address_02_title = new System.Windows.Forms.Label();
//////      this.lbl_birth_title = new System.Windows.Forms.Label();
//////      this.lbl_gender_title = new System.Windows.Forms.Label();
//////      this.lbl_email_title = new System.Windows.Forms.Label();
//////      this.lbl_phone_title = new System.Windows.Forms.Label();
//////      this.lbl_emails = new System.Windows.Forms.Label();
//////      this.lbl_phones = new System.Windows.Forms.Label();
//////      this.lbl_birth = new System.Windows.Forms.Label();
//////      this.lbl_gender = new System.Windows.Forms.Label();
//////      this.pb_user = new System.Windows.Forms.PictureBox();
//////      this.btn_account_edit = new System.Windows.Forms.Button();
//////      this.lbl_holder_name = new System.Windows.Forms.Label();
//////      this.btn_print_card = new System.Windows.Forms.Button();
//////      this.btn_lasts_movements = new System.Windows.Forms.Button();
//////      this.groupBox1 = new System.Windows.Forms.GroupBox();
//////      this.uc_points_balance1 = new WSI.Cashier.uc_points_balance();
//////      this.btn_transfer_p = new System.Windows.Forms.Button();
//////      this.btn_red_p = new System.Windows.Forms.Button();
//////      this.timer1 = new System.Windows.Forms.Timer(this.components);
//////      this.lbl_account_id_value = new System.Windows.Forms.Label();
//////      this.lbl_account_id_name = new System.Windows.Forms.Label();
//////      this.lbl_type_user = new System.Windows.Forms.Label();
//////      this.lbl_type_title = new System.Windows.Forms.Label();
//////      this.uc_card_reader1 = new WSI.Cashier.uc_card_reader();
//////      this.groupBox2.SuspendLayout();
//////      this.gb_comments.SuspendLayout();
//////      this.gb_address.SuspendLayout();
//////      ((System.ComponentModel.ISupportInitialize)(this.pb_user)).BeginInit();
//////      this.groupBox1.SuspendLayout();
//////      this.SuspendLayout();
//////      // 
//////      // lbl_cards_title
//////      // 
//////      this.lbl_cards_title.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//////      this.lbl_cards_title.Location = new System.Drawing.Point(23, 32);
//////      this.lbl_cards_title.Name = "lbl_cards_title";
//////      this.lbl_cards_title.Size = new System.Drawing.Size(180, 69);
//////      this.lbl_cards_title.TabIndex = 92;
//////      this.lbl_cards_title.Text = "xPLAYER TRACKING";
//////      // 
//////      // lbl_separator_1
//////      // 
//////      this.lbl_separator_1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
//////                  | System.Windows.Forms.AnchorStyles.Right)));
//////      this.lbl_separator_1.BackColor = System.Drawing.Color.Black;
//////      this.lbl_separator_1.Location = new System.Drawing.Point(-5, 130);
//////      this.lbl_separator_1.Name = "lbl_separator_1";
//////      this.lbl_separator_1.Size = new System.Drawing.Size(980, 2);
//////      this.lbl_separator_1.TabIndex = 90;
//////      // 
//////      // groupBox2
//////      // 
//////      this.groupBox2.Controls.Add(this.lbl_id_title);
//////      this.groupBox2.Controls.Add(this.lbl_id);
//////      this.groupBox2.Controls.Add(this.lbl_marital_status);
//////      this.groupBox2.Controls.Add(this.lbl_marital_status_title);
//////      this.groupBox2.Controls.Add(this.lbl_emails_2);
//////      this.groupBox2.Controls.Add(this.gb_comments);
//////      this.groupBox2.Controls.Add(this.gb_address);
//////      this.groupBox2.Controls.Add(this.lbl_birth_title);
//////      this.groupBox2.Controls.Add(this.lbl_gender_title);
//////      this.groupBox2.Controls.Add(this.lbl_email_title);
//////      this.groupBox2.Controls.Add(this.lbl_phone_title);
//////      this.groupBox2.Controls.Add(this.lbl_emails);
//////      this.groupBox2.Controls.Add(this.lbl_phones);
//////      this.groupBox2.Controls.Add(this.lbl_birth);
//////      this.groupBox2.Controls.Add(this.lbl_gender);
//////      this.groupBox2.Controls.Add(this.pb_user);
//////      this.groupBox2.Controls.Add(this.btn_account_edit);
//////      this.groupBox2.Controls.Add(this.lbl_holder_name);
//////      this.groupBox2.Location = new System.Drawing.Point(3, 168);
//////      this.groupBox2.Name = "groupBox2";
//////      this.groupBox2.Size = new System.Drawing.Size(411, 419);
//////      this.groupBox2.TabIndex = 97;
//////      this.groupBox2.TabStop = false;
//////      // 
//////      // lbl_id_title
//////      // 
//////      this.lbl_id_title.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
//////                  | System.Windows.Forms.AnchorStyles.Right)));
//////      this.lbl_id_title.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//////      this.lbl_id_title.Location = new System.Drawing.Point(57, 42);
//////      this.lbl_id_title.Name = "lbl_id_title";
//////      this.lbl_id_title.Size = new System.Drawing.Size(29, 23);
//////      this.lbl_id_title.TabIndex = 104;
//////      this.lbl_id_title.Text = "xCI:";
//////      this.lbl_id_title.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
//////      // 
//////      // lbl_id
//////      // 
//////      this.lbl_id.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
//////                  | System.Windows.Forms.AnchorStyles.Right)));
//////      this.lbl_id.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//////      this.lbl_id.Location = new System.Drawing.Point(92, 42);
//////      this.lbl_id.Name = "lbl_id";
//////      this.lbl_id.Size = new System.Drawing.Size(174, 23);
//////      this.lbl_id.TabIndex = 103;
//////      this.lbl_id.Text = "HEGG560427MVZRRL05";
//////      this.lbl_id.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
//////      // 
//////      // lbl_marital_status
//////      // 
//////      this.lbl_marital_status.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
//////                  | System.Windows.Forms.AnchorStyles.Right)));
//////      this.lbl_marital_status.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//////      this.lbl_marital_status.Location = new System.Drawing.Point(307, 88);
//////      this.lbl_marital_status.Name = "lbl_marital_status";
//////      this.lbl_marital_status.Size = new System.Drawing.Size(98, 23);
//////      this.lbl_marital_status.TabIndex = 102;
//////      this.lbl_marital_status.Text = "Free";
//////      this.lbl_marital_status.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
//////      // 
//////      // lbl_marital_status_title
//////      // 
//////      this.lbl_marital_status_title.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
//////                  | System.Windows.Forms.AnchorStyles.Right)));
//////      this.lbl_marital_status_title.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//////      this.lbl_marital_status_title.Location = new System.Drawing.Point(162, 88);
//////      this.lbl_marital_status_title.Name = "lbl_marital_status_title";
//////      this.lbl_marital_status_title.Size = new System.Drawing.Size(139, 23);
//////      this.lbl_marital_status_title.TabIndex = 101;
//////      this.lbl_marital_status_title.Text = "xMarital_Status:";
//////      this.lbl_marital_status_title.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
//////      // 
//////      // lbl_emails_2
//////      // 
//////      this.lbl_emails_2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
//////                  | System.Windows.Forms.AnchorStyles.Right)));
//////      this.lbl_emails_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//////      this.lbl_emails_2.Location = new System.Drawing.Point(91, 157);
//////      this.lbl_emails_2.Name = "lbl_emails_2";
//////      this.lbl_emails_2.Size = new System.Drawing.Size(314, 23);
//////      this.lbl_emails_2.TabIndex = 100;
//////      this.lbl_emails_2.Text = "email_email_email_email_email_";
//////      this.lbl_emails_2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
//////      // 
//////      // gb_comments
//////      // 
//////      this.gb_comments.Controls.Add(this.lbl_comments);
//////      this.gb_comments.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//////      this.gb_comments.Location = new System.Drawing.Point(5, 325);
//////      this.gb_comments.Name = "gb_comments";
//////      this.gb_comments.Size = new System.Drawing.Size(266, 85);
//////      this.gb_comments.TabIndex = 98;
//////      this.gb_comments.TabStop = false;
//////      this.gb_comments.Text = "xComments";
//////      // 
//////      // lbl_comments
//////      // 
//////      this.lbl_comments.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
//////                  | System.Windows.Forms.AnchorStyles.Right)));
//////      this.lbl_comments.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//////      this.lbl_comments.Location = new System.Drawing.Point(6, 16);
//////      this.lbl_comments.Name = "lbl_comments";
//////      this.lbl_comments.Size = new System.Drawing.Size(254, 66);
//////      this.lbl_comments.TabIndex = 86;
//////      this.lbl_comments.Text = "123456789.123456789.123456789.123456789.123456789.123456789.123456789.123456789.1" +
//////          "23456789.123456789.123456789.";
//////      // 
//////      // gb_address
//////      // 
//////      this.gb_address.Controls.Add(this.lbl_address_01);
//////      this.gb_address.Controls.Add(this.lbl_address_02);
//////      this.gb_address.Controls.Add(this.lbl_address_03);
//////      this.gb_address.Controls.Add(this.lbl_zip_title);
//////      this.gb_address.Controls.Add(this.lbl_city);
//////      this.gb_address.Controls.Add(this.lbl_city_title);
//////      this.gb_address.Controls.Add(this.lbl_zip);
//////      this.gb_address.Controls.Add(this.lbl_address_03_title);
//////      this.gb_address.Controls.Add(this.lbl_address_01_title);
//////      this.gb_address.Controls.Add(this.lbl_address_02_title);
//////      this.gb_address.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//////      this.gb_address.Location = new System.Drawing.Point(5, 181);
//////      this.gb_address.Name = "gb_address";
//////      this.gb_address.Size = new System.Drawing.Size(400, 138);
//////      this.gb_address.TabIndex = 97;
//////      this.gb_address.TabStop = false;
//////      this.gb_address.Text = "xAddress";
//////      // 
//////      // lbl_address_01
//////      // 
//////      this.lbl_address_01.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
//////                  | System.Windows.Forms.AnchorStyles.Right)));
//////      this.lbl_address_01.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//////      this.lbl_address_01.Location = new System.Drawing.Point(86, 16);
//////      this.lbl_address_01.Name = "lbl_address_01";
//////      this.lbl_address_01.Size = new System.Drawing.Size(308, 23);
//////      this.lbl_address_01.TabIndex = 80;
//////      this.lbl_address_01.Text = "calle.....calle.....calle.....calle.....calle.....calle.....";
//////      this.lbl_address_01.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
//////      // 
//////      // lbl_address_02
//////      // 
//////      this.lbl_address_02.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
//////                  | System.Windows.Forms.AnchorStyles.Right)));
//////      this.lbl_address_02.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//////      this.lbl_address_02.Location = new System.Drawing.Point(86, 41);
//////      this.lbl_address_02.Name = "lbl_address_02";
//////      this.lbl_address_02.Size = new System.Drawing.Size(308, 23);
//////      this.lbl_address_02.TabIndex = 81;
//////      this.lbl_address_02.Text = "colonia...colonia...colonia...colonia...colonia...";
//////      this.lbl_address_02.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
//////      // 
//////      // lbl_address_03
//////      // 
//////      this.lbl_address_03.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
//////                  | System.Windows.Forms.AnchorStyles.Right)));
//////      this.lbl_address_03.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//////      this.lbl_address_03.Location = new System.Drawing.Point(86, 64);
//////      this.lbl_address_03.Name = "lbl_address_03";
//////      this.lbl_address_03.Size = new System.Drawing.Size(308, 23);
//////      this.lbl_address_03.TabIndex = 83;
//////      this.lbl_address_03.Text = "delegacion...delegacion...delegacion...delegacion...delegacion...";
//////      this.lbl_address_03.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
//////      // 
//////      // lbl_zip_title
//////      // 
//////      this.lbl_zip_title.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
//////                  | System.Windows.Forms.AnchorStyles.Right)));
//////      this.lbl_zip_title.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//////      this.lbl_zip_title.Location = new System.Drawing.Point(6, 109);
//////      this.lbl_zip_title.Name = "lbl_zip_title";
//////      this.lbl_zip_title.Size = new System.Drawing.Size(78, 23);
//////      this.lbl_zip_title.TabIndex = 94;
//////      this.lbl_zip_title.Text = "xC.P.:";
//////      this.lbl_zip_title.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
//////      // 
//////      // lbl_city
//////      // 
//////      this.lbl_city.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
//////                  | System.Windows.Forms.AnchorStyles.Right)));
//////      this.lbl_city.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//////      this.lbl_city.Location = new System.Drawing.Point(86, 87);
//////      this.lbl_city.Name = "lbl_city";
//////      this.lbl_city.Size = new System.Drawing.Size(308, 23);
//////      this.lbl_city.TabIndex = 84;
//////      this.lbl_city.Text = "municipio...municipio...municipio...municipio...municipio...";
//////      this.lbl_city.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
//////      // 
//////      // lbl_city_title
//////      // 
//////      this.lbl_city_title.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
//////                  | System.Windows.Forms.AnchorStyles.Right)));
//////      this.lbl_city_title.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//////      this.lbl_city_title.Location = new System.Drawing.Point(6, 87);
//////      this.lbl_city_title.Name = "lbl_city_title";
//////      this.lbl_city_title.Size = new System.Drawing.Size(78, 23);
//////      this.lbl_city_title.TabIndex = 93;
//////      this.lbl_city_title.Text = "xMunicipio:";
//////      this.lbl_city_title.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
//////      // 
//////      // lbl_zip
//////      // 
//////      this.lbl_zip.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
//////                  | System.Windows.Forms.AnchorStyles.Right)));
//////      this.lbl_zip.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//////      this.lbl_zip.Location = new System.Drawing.Point(86, 109);
//////      this.lbl_zip.Name = "lbl_zip";
//////      this.lbl_zip.Size = new System.Drawing.Size(308, 23);
//////      this.lbl_zip.TabIndex = 85;
//////      this.lbl_zip.Text = "codigopostal..codigopostal..codigopostal..codigopostal..";
//////      this.lbl_zip.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
//////      // 
//////      // lbl_address_03_title
//////      // 
//////      this.lbl_address_03_title.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
//////                  | System.Windows.Forms.AnchorStyles.Right)));
//////      this.lbl_address_03_title.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//////      this.lbl_address_03_title.Location = new System.Drawing.Point(1, 64);
//////      this.lbl_address_03_title.Name = "lbl_address_03_title";
//////      this.lbl_address_03_title.Size = new System.Drawing.Size(83, 23);
//////      this.lbl_address_03_title.TabIndex = 92;
//////      this.lbl_address_03_title.Text = "Delegación:";
//////      this.lbl_address_03_title.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
//////      // 
//////      // lbl_address_01_title
//////      // 
//////      this.lbl_address_01_title.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
//////                  | System.Windows.Forms.AnchorStyles.Right)));
//////      this.lbl_address_01_title.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//////      this.lbl_address_01_title.Location = new System.Drawing.Point(6, 16);
//////      this.lbl_address_01_title.Name = "lbl_address_01_title";
//////      this.lbl_address_01_title.Size = new System.Drawing.Size(78, 23);
//////      this.lbl_address_01_title.TabIndex = 90;
//////      this.lbl_address_01_title.Text = "xCalle:";
//////      this.lbl_address_01_title.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
//////      // 
//////      // lbl_address_02_title
//////      // 
//////      this.lbl_address_02_title.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
//////                  | System.Windows.Forms.AnchorStyles.Right)));
//////      this.lbl_address_02_title.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//////      this.lbl_address_02_title.Location = new System.Drawing.Point(6, 41);
//////      this.lbl_address_02_title.Name = "lbl_address_02_title";
//////      this.lbl_address_02_title.Size = new System.Drawing.Size(78, 23);
//////      this.lbl_address_02_title.TabIndex = 91;
//////      this.lbl_address_02_title.Text = "xColonia:";
//////      this.lbl_address_02_title.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
//////      // 
//////      // lbl_birth_title
//////      // 
//////      this.lbl_birth_title.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
//////                  | System.Windows.Forms.AnchorStyles.Right)));
//////      this.lbl_birth_title.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//////      this.lbl_birth_title.Location = new System.Drawing.Point(5, 65);
//////      this.lbl_birth_title.Name = "lbl_birth_title";
//////      this.lbl_birth_title.Size = new System.Drawing.Size(80, 23);
//////      this.lbl_birth_title.TabIndex = 96;
//////      this.lbl_birth_title.Text = "Nacimiento:";
//////      this.lbl_birth_title.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
//////      // 
//////      // lbl_gender_title
//////      // 
//////      this.lbl_gender_title.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
//////                  | System.Windows.Forms.AnchorStyles.Right)));
//////      this.lbl_gender_title.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//////      this.lbl_gender_title.Location = new System.Drawing.Point(14, 88);
//////      this.lbl_gender_title.Name = "lbl_gender_title";
//////      this.lbl_gender_title.Size = new System.Drawing.Size(72, 23);
//////      this.lbl_gender_title.TabIndex = 95;
//////      this.lbl_gender_title.Text = "xSexo:";
//////      this.lbl_gender_title.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
//////      // 
//////      // lbl_email_title
//////      // 
//////      this.lbl_email_title.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
//////                  | System.Windows.Forms.AnchorStyles.Right)));
//////      this.lbl_email_title.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//////      this.lbl_email_title.Location = new System.Drawing.Point(3, 134);
//////      this.lbl_email_title.Name = "lbl_email_title";
//////      this.lbl_email_title.Size = new System.Drawing.Size(83, 23);
//////      this.lbl_email_title.TabIndex = 89;
//////      this.lbl_email_title.Text = "xEmail:";
//////      this.lbl_email_title.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
//////      // 
//////      // lbl_phone_title
//////      // 
//////      this.lbl_phone_title.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
//////                  | System.Windows.Forms.AnchorStyles.Right)));
//////      this.lbl_phone_title.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//////      this.lbl_phone_title.Location = new System.Drawing.Point(3, 111);
//////      this.lbl_phone_title.Name = "lbl_phone_title";
//////      this.lbl_phone_title.Size = new System.Drawing.Size(83, 23);
//////      this.lbl_phone_title.TabIndex = 88;
//////      this.lbl_phone_title.Text = "xTelefono:";
//////      this.lbl_phone_title.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
//////      // 
//////      // lbl_emails
//////      // 
//////      this.lbl_emails.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
//////                  | System.Windows.Forms.AnchorStyles.Right)));
//////      this.lbl_emails.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//////      this.lbl_emails.Location = new System.Drawing.Point(91, 134);
//////      this.lbl_emails.Name = "lbl_emails";
//////      this.lbl_emails.Size = new System.Drawing.Size(314, 23);
//////      this.lbl_emails.TabIndex = 78;
//////      this.lbl_emails.Text = "email_email_email_email_email_";
//////      this.lbl_emails.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
//////      // 
//////      // lbl_phones
//////      // 
//////      this.lbl_phones.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
//////                  | System.Windows.Forms.AnchorStyles.Right)));
//////      this.lbl_phones.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//////      this.lbl_phones.Location = new System.Drawing.Point(91, 111);
//////      this.lbl_phones.Name = "lbl_phones";
//////      this.lbl_phones.Size = new System.Drawing.Size(308, 23);
//////      this.lbl_phones.TabIndex = 77;
//////      this.lbl_phones.Text = "1234567890 123456879";
//////      this.lbl_phones.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
//////      // 
//////      // lbl_birth
//////      // 
//////      this.lbl_birth.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
//////                  | System.Windows.Forms.AnchorStyles.Right)));
//////      this.lbl_birth.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//////      this.lbl_birth.Location = new System.Drawing.Point(92, 65);
//////      this.lbl_birth.Name = "lbl_birth";
//////      this.lbl_birth.Size = new System.Drawing.Size(88, 23);
//////      this.lbl_birth.TabIndex = 76;
//////      this.lbl_birth.Text = "99 / 99 / 9999";
//////      this.lbl_birth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
//////      // 
//////      // lbl_gender
//////      // 
//////      this.lbl_gender.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
//////                  | System.Windows.Forms.AnchorStyles.Right)));
//////      this.lbl_gender.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//////      this.lbl_gender.Location = new System.Drawing.Point(92, 88);
//////      this.lbl_gender.Name = "lbl_gender";
//////      this.lbl_gender.Size = new System.Drawing.Size(64, 23);
//////      this.lbl_gender.TabIndex = 75;
//////      this.lbl_gender.Text = "genero...";
//////      this.lbl_gender.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
//////      // 
//////      // pb_user
//////      // 
//////      this.pb_user.Image = ((System.Drawing.Image)(resources.GetObject("pb_user.Image")));
//////      this.pb_user.InitialImage = ((System.Drawing.Image)(resources.GetObject("pb_user.InitialImage")));
//////      this.pb_user.Location = new System.Drawing.Point(5, 19);
//////      this.pb_user.Name = "pb_user";
//////      this.pb_user.Size = new System.Drawing.Size(48, 46);
//////      this.pb_user.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
//////      this.pb_user.TabIndex = 72;
//////      this.pb_user.TabStop = false;
//////      // 
//////      // btn_account_edit
//////      // 
//////      this.btn_account_edit.BackColor = System.Drawing.Color.Gainsboro;
//////      this.btn_account_edit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
//////      this.btn_account_edit.Location = new System.Drawing.Point(277, 362);
//////      this.btn_account_edit.Name = "btn_account_edit";
//////      this.btn_account_edit.Size = new System.Drawing.Size(128, 48);
//////      this.btn_account_edit.TabIndex = 73;
//////      this.btn_account_edit.Text = "Personalizar";
//////      this.btn_account_edit.UseVisualStyleBackColor = false;
//////      this.btn_account_edit.EnabledChanged += new System.EventHandler(this.buttons_EnabledChanged);
//////      // 
//////      // lbl_holder_name
//////      // 
//////      this.lbl_holder_name.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
//////                  | System.Windows.Forms.AnchorStyles.Right)));
//////      this.lbl_holder_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//////      this.lbl_holder_name.Location = new System.Drawing.Point(59, 19);
//////      this.lbl_holder_name.Name = "lbl_holder_name";
//////      this.lbl_holder_name.Size = new System.Drawing.Size(346, 23);
//////      this.lbl_holder_name.TabIndex = 74;
//////      this.lbl_holder_name.Text = "Nombre....Nombre....Nombre....Nombre....Nombre....Nombre....";
//////      this.lbl_holder_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
//////      // 
//////      // btn_print_card
//////      // 
//////      this.btn_print_card.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
//////      this.btn_print_card.BackColor = System.Drawing.Color.Gainsboro;
//////      this.btn_print_card.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//////      this.btn_print_card.Location = new System.Drawing.Point(599, 530);
//////      this.btn_print_card.Name = "btn_print_card";
//////      this.btn_print_card.Size = new System.Drawing.Size(128, 48);
//////      this.btn_print_card.TabIndex = 100;
//////      this.btn_print_card.Text = "Print Card";
//////      this.btn_print_card.UseVisualStyleBackColor = false;
//////      this.btn_print_card.EnabledChanged += new System.EventHandler(this.buttons_EnabledChanged);
//////      // 
//////      // btn_lasts_movements
//////      // 
//////      this.btn_lasts_movements.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
//////      this.btn_lasts_movements.BackColor = System.Drawing.Color.Gainsboro;
//////      this.btn_lasts_movements.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//////      this.btn_lasts_movements.Location = new System.Drawing.Point(742, 530);
//////      this.btn_lasts_movements.Name = "btn_lasts_movements";
//////      this.btn_lasts_movements.Size = new System.Drawing.Size(128, 48);
//////      this.btn_lasts_movements.TabIndex = 99;
//////      this.btn_lasts_movements.Text = "Lasts Movements";
//////      this.btn_lasts_movements.UseVisualStyleBackColor = false;
//////      this.btn_lasts_movements.EnabledChanged += new System.EventHandler(this.buttons_EnabledChanged);
//////      // 
//////      // groupBox1
//////      // 
//////      this.groupBox1.Controls.Add(this.uc_points_balance1);
//////      this.groupBox1.Controls.Add(this.btn_transfer_p);
//////      this.groupBox1.Controls.Add(this.btn_red_p);
//////      this.groupBox1.Location = new System.Drawing.Point(420, 168);
//////      this.groupBox1.Name = "groupBox1";
//////      this.groupBox1.Size = new System.Drawing.Size(461, 356);
//////      this.groupBox1.TabIndex = 101;
//////      this.groupBox1.TabStop = false;
//////      // 
//////      // uc_points_balance1
//////      // 
//////      this.uc_points_balance1.Location = new System.Drawing.Point(5, 18);
//////      this.uc_points_balance1.Name = "uc_points_balance1";
//////      this.uc_points_balance1.Size = new System.Drawing.Size(311, 332);
//////      this.uc_points_balance1.TabIndex = 102;
//////      // 
//////      // btn_transfer_p
//////      // 
//////      this.btn_transfer_p.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
//////      this.btn_transfer_p.BackColor = System.Drawing.Color.Gainsboro;
//////      this.btn_transfer_p.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//////      this.btn_transfer_p.Location = new System.Drawing.Point(322, 72);
//////      this.btn_transfer_p.Name = "btn_transfer_p";
//////      this.btn_transfer_p.Size = new System.Drawing.Size(128, 48);
//////      this.btn_transfer_p.TabIndex = 101;
//////      this.btn_transfer_p.Text = "Transferir Puntos";
//////      this.btn_transfer_p.UseVisualStyleBackColor = false;
//////      this.btn_transfer_p.EnabledChanged += new System.EventHandler(this.buttons_EnabledChanged);
//////      // 
//////      // btn_red_p
//////      // 
//////      this.btn_red_p.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
//////      this.btn_red_p.BackColor = System.Drawing.Color.Gainsboro;
//////      this.btn_red_p.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//////      this.btn_red_p.Location = new System.Drawing.Point(322, 18);
//////      this.btn_red_p.Name = "btn_red_p";
//////      this.btn_red_p.Size = new System.Drawing.Size(128, 48);
//////      this.btn_red_p.TabIndex = 100;
//////      this.btn_red_p.Text = "Redimir   Puntos";
//////      this.btn_red_p.UseVisualStyleBackColor = false;
//////      this.btn_red_p.EnabledChanged += new System.EventHandler(this.buttons_EnabledChanged);
//////      // 
//////      // timer1
//////      // 
//////      this.timer1.Interval = 500;
//////      this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
//////      // 
//////      // lbl_account_id_value
//////      // 
//////      this.lbl_account_id_value.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold);
//////      this.lbl_account_id_value.ForeColor = System.Drawing.Color.Black;
//////      this.lbl_account_id_value.Location = new System.Drawing.Point(173, 135);
//////      this.lbl_account_id_value.Name = "lbl_account_id_value";
//////      this.lbl_account_id_value.Size = new System.Drawing.Size(241, 30);
//////      this.lbl_account_id_value.TabIndex = 103;
//////      this.lbl_account_id_value.Text = "0";
//////      this.lbl_account_id_value.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
//////      // 
//////      // lbl_account_id_name
//////      // 
//////      this.lbl_account_id_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold);
//////      this.lbl_account_id_name.Location = new System.Drawing.Point(4, 135);
//////      this.lbl_account_id_name.Name = "lbl_account_id_name";
//////      this.lbl_account_id_name.Size = new System.Drawing.Size(163, 30);
//////      this.lbl_account_id_name.TabIndex = 102;
//////      this.lbl_account_id_name.Text = "xAccount";
//////      this.lbl_account_id_name.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
//////      // 
//////      // lbl_type_user
//////      // 
//////      this.lbl_type_user.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold);
//////      this.lbl_type_user.ForeColor = System.Drawing.Color.Black;
//////      this.lbl_type_user.Location = new System.Drawing.Point(535, 135);
//////      this.lbl_type_user.Name = "lbl_type_user";
//////      this.lbl_type_user.Size = new System.Drawing.Size(346, 30);
//////      this.lbl_type_user.TabIndex = 104;
//////      this.lbl_type_user.Text = "xClient";
//////      this.lbl_type_user.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
//////      // 
//////      // lbl_type_title
//////      // 
//////      this.lbl_type_title.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold);
//////      this.lbl_type_title.Location = new System.Drawing.Point(420, 135);
//////      this.lbl_type_title.Name = "lbl_type_title";
//////      this.lbl_type_title.Size = new System.Drawing.Size(109, 30);
//////      this.lbl_type_title.TabIndex = 105;
//////      this.lbl_type_title.Text = "xType";
//////      this.lbl_type_title.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
//////      // 
//////      // uc_card_reader1
//////      // 
//////      this.uc_card_reader1.Location = new System.Drawing.Point(249, 17);
//////      this.uc_card_reader1.Name = "uc_card_reader1";
//////      this.uc_card_reader1.Size = new System.Drawing.Size(342, 110);
//////      this.uc_card_reader1.TabIndex = 91;
//////      // 
//////      // uc_player_track
//////      // 
//////      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
//////      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
//////      this.BackColor = System.Drawing.Color.WhiteSmoke;
//////      this.Controls.Add(this.lbl_type_title);
//////      this.Controls.Add(this.lbl_type_user);
//////      this.Controls.Add(this.lbl_account_id_value);
//////      this.Controls.Add(this.lbl_account_id_name);
//////      this.Controls.Add(this.groupBox1);
//////      this.Controls.Add(this.btn_print_card);
//////      this.Controls.Add(this.btn_lasts_movements);
//////      this.Controls.Add(this.groupBox2);
//////      this.Controls.Add(this.lbl_cards_title);
//////      this.Controls.Add(this.uc_card_reader1);
//////      this.Controls.Add(this.lbl_separator_1);
//////      this.Name = "uc_player_track";
//////      this.Size = new System.Drawing.Size(884, 717);
//////      this.groupBox2.ResumeLayout(false);
//////      this.gb_comments.ResumeLayout(false);
//////      this.gb_address.ResumeLayout(false);
//////      ((System.ComponentModel.ISupportInitialize)(this.pb_user)).EndInit();
//////      this.groupBox1.ResumeLayout(false);
//////      this.ResumeLayout(false);

//////    }

//////    #endregion

//////    private System.Windows.Forms.Label lbl_cards_title;
//////    private uc_card_reader uc_card_reader1;
//////    private System.Windows.Forms.Label lbl_separator_1;
//////    private System.Windows.Forms.GroupBox groupBox2;
//////    private System.Windows.Forms.PictureBox pb_user;
//////    private System.Windows.Forms.Button btn_account_edit;
//////    private System.Windows.Forms.Label lbl_holder_name;
//////    private System.Windows.Forms.Button btn_print_card;
//////    private System.Windows.Forms.Button btn_lasts_movements;
//////    private System.Windows.Forms.GroupBox groupBox1;
//////    private System.Windows.Forms.Timer timer1;
//////    private System.Windows.Forms.Button btn_transfer_p;
//////    private System.Windows.Forms.Button btn_red_p;
//////    private System.Windows.Forms.Label lbl_account_id_value;
//////    private System.Windows.Forms.Label lbl_account_id_name;
//////    private System.Windows.Forms.Label lbl_comments;
//////    private System.Windows.Forms.Label lbl_zip;
//////    private System.Windows.Forms.Label lbl_city;
//////    private System.Windows.Forms.Label lbl_address_03;
//////    private System.Windows.Forms.Label lbl_address_02;
//////    private System.Windows.Forms.Label lbl_address_01;
//////    private System.Windows.Forms.Label lbl_emails;
//////    private System.Windows.Forms.Label lbl_phones;
//////    private System.Windows.Forms.Label lbl_birth;
//////    private System.Windows.Forms.Label lbl_gender;
//////    private System.Windows.Forms.Label lbl_type_user;
//////    private System.Windows.Forms.Label lbl_type_title;
//////    private System.Windows.Forms.Label lbl_phone_title;
//////    private System.Windows.Forms.Label lbl_email_title;
//////    private System.Windows.Forms.Label lbl_birth_title;
//////    private System.Windows.Forms.Label lbl_gender_title;
//////    private System.Windows.Forms.Label lbl_zip_title;
//////    private System.Windows.Forms.Label lbl_city_title;
//////    private System.Windows.Forms.Label lbl_address_03_title;
//////    private System.Windows.Forms.Label lbl_address_02_title;
//////    private System.Windows.Forms.Label lbl_address_01_title;
//////    private uc_points_balance uc_points_balance1;
//////    private System.Windows.Forms.GroupBox gb_address;
//////    private System.Windows.Forms.GroupBox gb_comments;
//////    private System.Windows.Forms.Label lbl_emails_2;
//////    private System.Windows.Forms.Label lbl_marital_status_title;
//////    private System.Windows.Forms.Label lbl_marital_status;
//////    private System.Windows.Forms.Label lbl_id_title;
//////    private System.Windows.Forms.Label lbl_id;
//////  }
//////}
