//------------------------------------------------------------------------------
// Copyright � 2010 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_tournament_prize_list.cs
// 
//   DESCRIPTION: Gaming Tables Tournaments prizes list
//
//        AUTHOR: FGB
// 
// CREATION DATE: 21-APR-2017
//
// REVISION HISTORY:
// 
// Date         Author  Description
// ----------- ------   ----------------------------------------------------------
// 21-APR-2017  FGB     WIGOS-576: Gaming table tournament - Pending Tournament payment
// 24-MAY-2017  ATB     PBI 27522:[2021] Gaming table tournament - List of prize tournament payment to undo
//--------------------------------------------------------------------------------

using System;
using System.Text;
using System.Windows.Forms;
using WSI.Cashier.Controls;
using WSI.Common;
using WSI.Common.GamingTablesTournament;

namespace WSI.Cashier
{
  public partial class frm_tournament_prize_list : frm_base
  {
    #region Properties
    private GamingTablesTournamentPrizeRowEnterEventArgs PrizeSelected { get; set; }
    #endregion

    #region Constructor
    public frm_tournament_prize_list()
    {
      PrizeSelected = null;

      InitializeComponent();

      InitializeControlResources();
    }
    #endregion

    #region Public Methods
    /// <summary>
    /// Init and Show the form.
    /// </summary>
    /// <param name="ParentForm"></param>
    public void Show(Form ParentForm)
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_tournament_prize_list", Log.Type.Message);

      RefreshTournamentPrizeList(false);

      using (frm_yesno form_yes_no = new frm_yesno())
      {
        form_yes_no.Opacity = 0.6;
        form_yes_no.Show();
        this.ShowDialog(form_yes_no);
      }
    }
    #endregion

    #region Private Methods
    /// <summary>
    /// Initialize control resources (NLS strings, images, etc).
    /// </summary>
    private void InitializeControlResources()
    {
      FormTitle = Resource.String("STR_TOURNAMENT_PRIZE_LIST_TITLE");           // Tournaments prize list
      Text = FormTitle;

      btn_pay_prize.Text = Resource.String("STR_TOURNAMENT_PRIZE_PAYMENT");     // Pay prize to player
      btn_undo_prize.Text = Resource.String("STR_FRM_LAST_UNDO");               // Undo
      btn_close.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CLOSE");     // Close window

      btn_pay_prize.Enabled = false;
      btn_undo_prize.Enabled = false;
      btn_close.Enabled = true;

      //Assign event
      uc_tournament_prize_list1.GamingTablesTournamentPrizeRowEnterEvent += new GamingTablesTournamentPrizeRowEnterEventHandler(eventPrizeRowEnter);
    }

    /// <summary>
    /// Switches the enabled property of the Payment button depending on the values of the selected prize
    /// </summary>
    private void switchBtnPayPrize()
    {
      if (ValidatePrizeSelected())
      {
        btn_pay_prize.Enabled = ((PrizeSelected.TournamentId > 0) && (PrizeSelected.AccountId > 0) && (PrizeSelected.PrizeAmount > 0));
      }
      else
      {
        btn_pay_prize.Enabled = false;
      }
    }

    /// <summary>
    /// Switches the enabled property of the Undo button depending on the values of the selected prize
    /// </summary>
    private void switchBtnUndo()
    {
      if (ValidatePrizeSelected())
      {
        btn_undo_prize.Enabled = ((PrizeSelected.TournamentId > 0) && (PrizeSelected.AccountId > 0) && (PrizeSelected.PrizeAmount > 0));
      }
      else
      {
        btn_undo_prize.Enabled = false;
      }
    }

    /// <summary>
    /// Is prize selected
    /// </summary>
    private Boolean ValidatePrizeSelected()
    {
      return (PrizeSelected != null);
    }

    /// <summary>
    /// Tournament prize payout
    /// </summary>
    private Boolean TournamentPrizePayout()
    {
      GAMING_TABLES_TOURNAMENT_PRIZE_PAYMENT_METHOD _tournament_prize_payment_method;
      String _error_message;
      CardData _card_data;
      PaymentOrder _payment_order;
      Boolean _keep_position;

      if (!ValidatePrizeSelected())
      {
        ShowErrorMessage(Resource.String("STR_FRM_TOURNAMENT_PRIZE_NOT_SELECTED"));

        return false;
      }

      if (PrizeSelected.PrizeStatus != GAMING_TABLES_TOURNAMENT_PRIZE_STATUS.AWARDED)
      {
        ShowErrorMessage(Resource.String("STR_TOURNAMENT_ERROR_PRIZE_STATUS_NOT_PAYABLE"));

        return false;
      }

      //Get method to pay the prize to the player
      if (!GetPrizePaymentMethod(out _tournament_prize_payment_method))
      {
        return false;
      }

      //Get card data
      _card_data = GetCardDataFromAccountId(PrizeSelected.AccountId);

      //Get payment order
      _payment_order = null;
      //if (_tournament_prize_payment_method == GAMING_TABLES_TOURNAMENT_PRIZE_PAYMENT_METHOD.PAYMENT_ORDER)
      //{
      //  //Get payment order
      //  //if (!CashierBusinessLogic.EnterOrderPaymentData(this, _card_data, PrizeSelected.PrizeAmount, PrizeSelected.PrizeAmount, null, out _payment_order))
      //  {
      //    // Data entering process was cancelled => no further process
      //    return false;
      //  }
      //}

      if (_payment_order == null)
      {
        _payment_order = new PaymentOrder();
      }

      //Pay the prize
      if (!MakeTournamentPrizePayment(_tournament_prize_payment_method, _card_data, _payment_order, Cashier.CashierSessionInfo(), out _error_message))
      {
        ShowErrorMessage(_error_message);

        return false;
      }

      //Is paid status is selected and we pay a prize, we may want to undo it -> Keep the position
      _keep_position = (uc_tournament_prize_list1.IsPaidPrizeStatusSelected());
      RefreshTournamentPrizeList(_keep_position);

      return true;
    }

    private String GetCardHolderName(CardData cardData)
    {
      String _holder_name;

      if (cardData == null)
      {
        throw new ArgumentException("GetCardHolderName. Card must have a value.");
      }

      _holder_name = cardData.GetAccountName();

      return _holder_name;
    }

    /// <summary>
    /// Ask confirmation for undo prize payment
    /// </summary>
    private Boolean AskConfirmationForUndo(CardData cardData)
    {
      StringBuilder _data_to_undo;
      String _confimation_message;
      DialogResult _rc;

      // Data to undo
      _data_to_undo = new StringBuilder();
      _data_to_undo.AppendLine(Resource.String("STR_TOURNAMENT_PRIZE_ACCOUNT") + ": " + GetCardHolderName(cardData));
      _data_to_undo.AppendLine(Resource.String("STR_TOURNAMENT_PRIZE_AWARD_AMOUNT") + ": " + PrizeSelected.PrizeAmount.ToString("c"));

      // Show message
      _confimation_message = Resource.String("STR_GENERIC_WARNING_TOURNAMENT_UNDO_PRIZE_PAYMENT", _data_to_undo.ToString());
      _confimation_message = _confimation_message.Replace("\\r\\n", "\r\n");

      _rc = frm_message.Show(_confimation_message, Resource.String("STR_APP_GEN_MSG_WARNING"), MessageBoxButtons.YesNo, MessageBoxIcon.Warning, ParentForm);

      // Does do user really want to undo prize payment
      return (_rc == DialogResult.OK);
    }

    /// <summary>
    /// Undo tournament prize payout
    /// </summary>
    private Boolean UndoTournamentPrizePayout()
    {
      String _error_message;
      CardData _card_data;
      Boolean _keep_position;

      if (!ValidatePrizeSelected())
      {
        ShowErrorMessage(Resource.String("STR_FRM_TOURNAMENT_PRIZE_NOT_SELECTED"));

        return false;
      }

      if (PrizeSelected.PrizeStatus != GAMING_TABLES_TOURNAMENT_PRIZE_STATUS.PAID)
      {
        ShowErrorMessage(Resource.String("STR_TOURNAMENT_ERROR_PRIZE_STATUS_NOT_PAID"));

        return false;
      }

      //Get card data
      _card_data = GetCardDataFromAccountId(PrizeSelected.AccountId);

      if (!AskConfirmationForUndo(_card_data))
      {
        return false;
      }

      //Undo the tournament prize payout
      if (!UndoTournamentPrizePayment(_card_data, Cashier.CashierSessionInfo(), out _error_message))
      {
        ShowErrorMessage(_error_message);

        return false;
      }

      //Is paid status is selected and we pay a prize, we may want to undo it -> Keep the position
      _keep_position = (uc_tournament_prize_list1.IsPaidPrizeStatusSelected());
      RefreshTournamentPrizeList(_keep_position);

      return true;
    }

    /// <summary>
    /// Get prize payment method
    /// </summary>
    /// <param name="TournamentPrizePaymentMethod"></param>
    /// <returns></returns>
    private Boolean GetPrizePaymentMethod(out GAMING_TABLES_TOURNAMENT_PRIZE_PAYMENT_METHOD TournamentPrizePaymentMethod)
    {
      frm_tournament_prize_payout _frm_payment;
      DialogResult _rc;

      _frm_payment = new frm_tournament_prize_payout(PrizeSelected.TournamentId,
                                                     PrizeSelected.AccountId,
                                                     PrizeSelected.PrizeAmount,
                                                     null);

      _rc = _frm_payment.Show(this, out TournamentPrizePaymentMethod);

      if (_rc != DialogResult.OK)
      {
        return false;
      }

      if ((TournamentPrizePaymentMethod == GAMING_TABLES_TOURNAMENT_PRIZE_PAYMENT_METHOD.PAYOUT)
            && (PrizeSelected.PrizeAmount > 0))
      {
        if (!ValidateCashierConceptOperationResult(PrizeSelected.PrizeAmount))
        {
          ShowErrorMessage(Resource.String("STR_FRM_AMOUNT_INPUT_MSG_AMOUNT_EXCEED_BANK"));

          return false;
        }
      }

      if (TournamentPrizePaymentMethod == GAMING_TABLES_TOURNAMENT_PRIZE_PAYMENT_METHOD.UNKNOWN)
      {
        ShowErrorMessage(Resource.String("STR_FRM_TOURNAMENT_PRIZE_PAYOUT_INVALID_METHOD"));

        return false;
      }

      return true;
    }

    /// <summary>
    /// Refresh prize list
    /// </summary>
    private void RefreshTournamentPrizeList(Boolean KeepPosition)
    {
      PrizeSelected = null;
      uc_tournament_prize_list1.RefreshTournamentPrizeList(KeepPosition);

      switchBtnPayPrize();
      switchBtnUndo();
    }

    /// <summary>
    /// Get currency exchange result
    /// </summary>
    private CurrencyExchangeResult GetCurrencyExchangeResult(GAMING_TABLES_TOURNAMENT_PRIZE_PAYMENT_METHOD TournamentPrizePaymentMethod, Decimal PrizeAmount)
    {
      CurrencyExchangeResult _currency_exchange_result;
      _currency_exchange_result = new CurrencyExchangeResult();

      switch (TournamentPrizePaymentMethod)
      {
        case GAMING_TABLES_TOURNAMENT_PRIZE_PAYMENT_METHOD.PAYOUT:
          _currency_exchange_result.InType = CurrencyExchangeType.CURRENCY;
          break;

        case GAMING_TABLES_TOURNAMENT_PRIZE_PAYMENT_METHOD.PAYOUT_AND_RECHARGE:
          _currency_exchange_result.InType = CurrencyExchangeType.ACCOUNT;
          break;

        case GAMING_TABLES_TOURNAMENT_PRIZE_PAYMENT_METHOD.PAYMENT_ORDER:
          _currency_exchange_result.InType = CurrencyExchangeType.CHECK;
          break;

        case GAMING_TABLES_TOURNAMENT_PRIZE_PAYMENT_METHOD.UNKNOWN:
        default:
          throw new ArgumentException("Invalid TournamentPrizePaymentMethod : '" + TournamentPrizePaymentMethod.ToString() + "'.");
      }

      _currency_exchange_result.SubType = CurrencyExchangeSubType.NONE;
      _currency_exchange_result.InAmount = PrizeAmount;
      _currency_exchange_result.GrossAmount = PrizeAmount;

      return _currency_exchange_result;
    }

    /// <summary>
    /// Makes payment of a tournament prize
    /// </summary>
    /// <param name="TournamentPrizePaymentMethod"></param>
    /// <param name="cardData"></param>
    /// <param name="PayOrder"></param>
    /// <param name="CashierSession"></param>
    /// <param name="MessageError"></param>
    /// <returns></returns>
    private Boolean MakeTournamentPrizePayment(GAMING_TABLES_TOURNAMENT_PRIZE_PAYMENT_METHOD TournamentPrizePaymentMethod, CardData cardData, PaymentOrder PayOrder, CashierSessionInfo CashierSession, out String MessageError)
    {
      GamingTablesTournamentPrizePaymentInputParameters _prize_input_parameters;
      GamingTablesTournamentPrizePaymentOutputParameters _prize_output_parameters;

      MessageError = String.Empty;

      if (!ValidatePrizeSelected())
      {
        MessageError = Resource.String("STR_FRM_TOURNAMENT_PRIZE_NOT_SELECTED");

        return false;
      }

      //Input parameters
      _prize_input_parameters = new GamingTablesTournamentPrizePaymentInputParameters();
      _prize_input_parameters.Card = cardData;
      _prize_input_parameters.TournamentId = PrizeSelected.TournamentId;
      _prize_input_parameters.PaymentOrder = PayOrder;
      _prize_input_parameters.CashierSession = CashierSession;
      _prize_input_parameters.ExchangeResult = GetCurrencyExchangeResult(TournamentPrizePaymentMethod, PrizeSelected.PrizeAmount);

      //Output parameters
      _prize_output_parameters = new GamingTablesTournamentPrizePaymentOutputParameters();

      using (DB_TRX _db_trx = new DB_TRX())
      {
        if (!GamingTablesTournamentPrizeBusinessLogic.PayTournamentPrize(_prize_input_parameters, ref _prize_output_parameters, _db_trx.SqlTransaction))
        {
          MessageError = _prize_output_parameters.ErrorMessage;

          if (String.IsNullOrEmpty(MessageError))
          {
            MessageError = Resource.String("STR_GENERIC_ERROR_TOURNAMENT_PRIZE_PAYMENT");
          }

          return false;
        }

        _db_trx.Commit();
      }

      VoucherPrint.Print(_prize_output_parameters.VoucherList);

      return true;
    }

    /// <summary>
    /// Undo a payment of a tournament prize
    /// </summary>
    /// <param name="cardData"></param>
    /// <param name="CashierSession"></param>
    /// <param name="MessageError"></param>
    /// <returns></returns>
    private Boolean UndoTournamentPrizePayment(CardData cardData, CashierSessionInfo CashierSession, out String MessageError)
    {
      GamingTablesTournamentPrizePaymentInputParameters _prize_input_parameters;
      GamingTablesTournamentPrizePaymentOutputParameters _prize_output_parameters;

      MessageError = String.Empty;

      if (!ValidatePrizeSelected())
      {
        MessageError = Resource.String("STR_FRM_TOURNAMENT_PRIZE_NOT_SELECTED");

        return false;
      }

      //Input parameters
      _prize_input_parameters = new GamingTablesTournamentPrizePaymentInputParameters();
      _prize_input_parameters.Card = cardData;
      _prize_input_parameters.TournamentId = PrizeSelected.TournamentId;
      _prize_input_parameters.PaymentOrder = null;
      _prize_input_parameters.CashierSession = CashierSession;
      _prize_input_parameters.ExchangeResult = GetCurrencyExchangeResult(GAMING_TABLES_TOURNAMENT_PRIZE_PAYMENT_METHOD.PAYOUT, PrizeSelected.PrizeAmount); //TODO:ZZZZ - (FGB): 2017-06-13: Check if correct
      _prize_input_parameters.OperationId = 0;

      //Output parameters
      _prize_output_parameters = new GamingTablesTournamentPrizePaymentOutputParameters();

      using (DB_TRX _db_trx = new DB_TRX())
      {
        if (!GamingTablesTournamentPrizeBusinessLogic.UndoPayTournamentPrize(ref _prize_input_parameters, ref _prize_output_parameters, _db_trx.SqlTransaction))
        {
          MessageError = _prize_output_parameters.ErrorMessage;

          if (String.IsNullOrEmpty(MessageError))
          {
            MessageError = Resource.String("STR_GENERIC_ERROR_TOURNAMENT_UNDO_PRIZE_PAYMENT");
          }

          return false;
        }

        _db_trx.Commit();
      }

      VoucherPrint.Print(_prize_output_parameters.VoucherList);

      return true;
    }

    /// <summary>
    /// Show message error
    /// </summary>
    /// <param name="ErrorMessage"></param>
    private void ShowErrorMessage(String ErrorMessage)
    {
      String _title;
      MessageBoxIcon _icon;

      if (String.IsNullOrEmpty(ErrorMessage))
      {
        return;
      }

      _title = Resource.String("STR_APP_GEN_MSG_ERROR");
      _icon = MessageBoxIcon.Error;

      frm_message.Show(ErrorMessage,
                       _title,
                       MessageBoxButtons.OK,
                       _icon,
                       this);
    }

    /// <summary>
    /// Get CardData from AccountId
    /// </summary>
    private CardData GetCardDataFromAccountId(Int64 AccountId)
    {
      CardData _card_data;

      _card_data = new CardData();

      // Get card data info
      using (DB_TRX _db_trx = new DB_TRX())
      {
        if (!CardData.DB_CardGetAllData(AccountId, _card_data, _db_trx.SqlTransaction))
        {
          Log.Error("GetCardDataFromAccountId: Could not get card data. Accout_Id = " + AccountId);

          return null;
        }

        _db_trx.Commit();
      }

      return _card_data;
    }

    /// <summary>
    /// Validate current balance is >= Amount
    /// </summary>
    /// <param name="Amount"></param>
    /// <returns></returns>
    private Boolean ValidateCashierConceptOperationResult(Decimal Amount)
    {
      Currency _cashier_current_balance;

      using (DB_TRX _db_trx = new DB_TRX())
      {
        _cashier_current_balance = CashierBusinessLogic.UpdateSessionBalance(_db_trx.SqlTransaction,
                                                                             Cashier.SessionId,
                                                                             0);
      }

      return (_cashier_current_balance >= Amount);
    }

    #endregion

    #region Events
    /// <summary>
    /// Event when enter on a row
    /// </summary>
    private void eventPrizeRowEnter(object sender, GamingTablesTournamentPrizeRowEnterEventArgs e)
    {
      PrizeSelected = e;

      switchBtnPayPrize();
      switchBtnUndo();
    }
    #endregion

    #region Buttons
    /// <summary>
    /// Handle the click on the Close/Cancel button.
    /// </summary>
    private void btn_close_Click(object sender, EventArgs e)
    {
      this.Close();
    }

    /// <summary>
    /// Handle the click on the Pay Prize button.
    /// </summary>
    private void btn_pay_prize_Click(object sender, EventArgs e)
    {
      TournamentPrizePayout();
    }

    /// <summary>
    /// Handle the click on the Undo Prize button.
    /// </summary>
    private void btn_undo_prize_Click(object sender, EventArgs e)
    {
      UndoTournamentPrizePayout();
    }
    #endregion
  }
}