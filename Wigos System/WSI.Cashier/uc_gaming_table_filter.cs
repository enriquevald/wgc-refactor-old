//------------------------------------------------------------------------------
// Copyright � 2007-2013 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_gaming_table_filter.cs
// 
//   DESCRIPTION: Control to filter gaming tables
//
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 21-DEC-2013 RMS     First release.
// 18-APR-2016 DHA     Fixed Bug 9575: error deleting first row
//------------------------------------------------------------------------------

using System;
using WSI.Common;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Windows.Forms;

namespace WSI.Cashier
{

  public partial class uc_gaming_table_filter : UserControl
  {

    #region " Required classes "
    
    //------------------------------------------------------------------------------
    // PURPOSE : Class to interact with the selected item on the control.
    //
    //   NOTES :
    //
    public partial class GamingTableFilterGridChange : EventArgs
    {
      private Int64 m_selected_row;

      public GamingTableFilterGridChange(Int64 SelectedRow)
      {
        m_selected_row = SelectedRow;
      }

      //------------------------------------------------------------------------------
      // PURPOSE : Property with the selected row.
      //
      //   NOTES :
      //
      public Int64 SelectedRow
      {
        get
        {
          return m_selected_row;
        }
      }

    }

    #endregion
    
    #region " Constructor "

    //------------------------------------------------------------------------------
    // PURPOSE : Constructor.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public uc_gaming_table_filter()
    {
      InitializeComponent();

      InitControlsResources();

    }

#endregion

    #region " Constants "

    // Gaming table type column index constants
    private const Int32 GTT_TYPE_NAME = 0;
    private const Int32 GTT_TYPE_ID = 1;

    // Gaming tables column index constants
    private const Int32 GT_NAME = 0;
    private const Int32 GT_ID = 1;
    private const Int32 GT_TYPE_ID = 2;
    private const Int32 GT_CASHIER_SESSION = 3;

    #endregion

    #region " Private declarations "
    
    // Data of gaming tables and types container
    private DataTable m_gaming_tables_and_types;

    // Default selection members
    private Int32 m_default_type;
    private Int32 m_default_table;
    private Int32 m_default_selection_type;
    private Int32 m_default_selection_table;

    // Delegate and event to control when an item is selected
    public delegate void GamingTableSelectedEventHandler(object sender, GamingTableFilterGridChange e);
    public event GamingTableSelectedEventHandler GamingTableSelected;

    #endregion

    #region " Public methods "

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the selected gaming table id.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public Int32 GamingTableIdSelected()
    {
      Int32 _selected_game_table;

      _selected_game_table = -1;

      if (dgv_gaming_tables.Rows.GetFirstRow(DataGridViewElementStates.Selected) >= 0)
      {
        _selected_game_table = (Int32)dgv_gaming_tables.Rows[dgv_gaming_tables.Rows.GetFirstRow(DataGridViewElementStates.Selected)].Cells[GT_ID].Value;
      }

      return _selected_game_table;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the selected gaming table name.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public String GamingTableNameSelected()
    {
      String _selected_game_table;

      _selected_game_table = String.Empty;

      if (dgv_gaming_tables.Rows.GetFirstRow(DataGridViewElementStates.Selected) >= 0)
      {
        _selected_game_table = (String)dgv_gaming_tables.Rows[dgv_gaming_tables.Rows.GetFirstRow(DataGridViewElementStates.Selected)].Cells[GT_NAME].Value;
      }

      return _selected_game_table;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Return the gmaing table type id.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public Int32 GamingTableTypeSelected()
    {
      Int32 _selected_game_table_type;

      _selected_game_table_type = -1;

      if (dgv_gaming_tables.Rows.GetFirstRow(DataGridViewElementStates.Selected) >= 0)
      {
        _selected_game_table_type = (Int32)dgv_gaming_tables.Rows[dgv_gaming_tables.Rows.GetFirstRow(DataGridViewElementStates.Selected)].Cells[GT_TYPE_ID].Value;
      }

      return _selected_game_table_type;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the name of the selected gaming table type.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public String GamingTableTypeNameSelected()
    {
      String _selected_game_table_type;

      _selected_game_table_type = String.Empty;

      if (dgv_gaming_table_types.Rows.GetFirstRow(DataGridViewElementStates.Selected) >= 0)
      {
        _selected_game_table_type = (String)dgv_gaming_table_types.Rows[dgv_gaming_table_types.Rows.GetFirstRow(DataGridViewElementStates.Selected)].Cells[GTT_TYPE_NAME].Value;
      }

      return _selected_game_table_type;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the cashier session of the selected gaming table.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public Int64 GamingTableCashierSessionSelected()
    {
      Int64 _selected_game_table_session;

      _selected_game_table_session = -1;

      if (dgv_gaming_tables.Rows.GetFirstRow(DataGridViewElementStates.Selected) >= 0)
      {
        _selected_game_table_session = (Int64)dgv_gaming_tables.Rows[dgv_gaming_tables.Rows.GetFirstRow(DataGridViewElementStates.Selected)].Cells[GT_CASHIER_SESSION].Value;
      }

      return _selected_game_table_session;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Returns all information about the selected gaming table.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS : LinkedGamingTable
    //
    //   NOTES :
    //
    public LinkedGamingTable SelectedGamingTable()
    {
      LinkedGamingTable _selected_game_table;

      _selected_game_table = new LinkedGamingTable();

      _selected_game_table.TableId = this.GamingTableIdSelected();
      _selected_game_table.TableName = this.GamingTableNameSelected();
      _selected_game_table.TypeId = this.GamingTableTypeSelected();
      _selected_game_table.TypeName = this.GamingTableTypeNameSelected();
      _selected_game_table.CashierSession = this.GamingTableCashierSessionSelected();
      _selected_game_table.GamingTableSession = 0;

      return _selected_game_table;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Initializes the data container.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public void LoadData()
    {
      StringBuilder _sb;
      
      m_gaming_tables_and_types = new DataTable();

      try
      {

        _sb = new StringBuilder();

        _sb.AppendLine("   SELECT   GT_NAME                                                            ");
        _sb.AppendLine("          , GT_GAMING_TABLE_ID                                                 ");
        _sb.AppendLine("          , GT_TYPE_ID                                                         ");
        _sb.AppendLine("          , GTT_NAME                                                           ");
        _sb.AppendLine("          , GT_HAS_INTEGRATED_CASHIER                                          ");
        _sb.AppendLine("          , GT_CASHIER_ID                                                      ");
        _sb.AppendLine("          , MAX(CS_SESSION_ID) AS GT_CASHIER_SESSION                           ");
        _sb.AppendLine("     FROM   GAMING_TABLES                                                      ");
        _sb.AppendLine("    INNER   JOIN GAMING_TABLES_TYPES ON GT_TYPE_ID = GTT_GAMING_TABLE_TYPE_ID  ");
        _sb.AppendLine("    INNER   JOIN CASHIER_SESSIONS    ON GT_CASHIER_ID = CS_CASHIER_ID          ");
        _sb.AppendLine("    INNER   JOIN GUI_USERS           ON CS_USER_ID = GU_USER_ID AND GU_USER_TYPE IN (0, 5)");
        _sb.AppendLine("    WHERE   CS_STATUS = 0                                                      ");
        _sb.AppendLine("      AND   GT_ENABLED = 1                                                     ");
        _sb.AppendLine("      AND   GTT_ENABLED = 1                                                    ");
        _sb.AppendLine(" GROUP BY   GT_GAMING_TABLE_ID                                                 ");
        _sb.AppendLine("          , GT_NAME                                                            ");
        _sb.AppendLine("          , GT_TYPE_ID                                                         ");
        _sb.AppendLine("          , GTT_NAME                                                           ");
        _sb.AppendLine("          , GT_HAS_INTEGRATED_CASHIER                                          ");
        _sb.AppendLine("          , GT_CASHIER_ID                                                      ");
        _sb.AppendLine(" ORDER BY   GT_NAME                                                            ");

       // Get the data
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              _sql_da.Fill(m_gaming_tables_and_types); 
            }
          }
        }

        // Update the gaming table types 
        if (m_gaming_tables_and_types.Rows.Count > 0)
        {
          CheckOwnedGamingTable();

          UpdateGamingTableTypesGrid(m_gaming_tables_and_types.DefaultView.ToTable(true, "GTT_NAME", "GT_TYPE_ID").Select());

          if (m_default_selection_type >= 0)
          {
            // Set default selected items.
            dgv_gaming_table_types.CurrentCell = dgv_gaming_table_types.Rows[m_default_selection_type].Cells[GTT_TYPE_NAME];

            UpdateGamingTablesGrid();

            if (m_default_selection_table >= 0)
            {
              dgv_gaming_tables.CurrentCell = dgv_gaming_tables.Rows[m_default_selection_table].Cells[GT_NAME];
              new GamingTableFilterGridChange(m_default_selection_table);
            }
          }

        } 

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

    }

#endregion

    #region " Private methods "

    //------------------------------------------------------------------------------
    // PURPOSE : Prepares de default selected item if the cashier is table integrated.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    private void CheckOwnedGamingTable()
    {
      DataRow[] m_data;

      m_data = m_gaming_tables_and_types.Select("GT_HAS_INTEGRATED_CASHIER = 1 AND GT_CASHIER_ID = " + Cashier.CashierSessionInfo().TerminalId);

      m_default_selection_type = -1;
      m_default_selection_table = -1;

      if (m_data.Length > 0)
      {
        m_default_table = (Int32)m_data[0][GT_ID];
        m_default_type = (Int32)m_data[0][GT_TYPE_ID];
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Updates the gaming table types grid.
    //
    //  PARAMS :
    //      - INPUT :
    //        - GamingTableTypesRows: Rows with gaming table types
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void UpdateGamingTableTypesGrid(DataRow[] GamingTableTypesRows)
    {
      Int32 _row_idx;

      _row_idx = dgv_gaming_table_types.Rows.Add();
      dgv_gaming_table_types.Rows[_row_idx].Height = 0;
      
      // Fill the gaming table types datagrid
      foreach (DataRow _row in GamingTableTypesRows)
      {
        _row_idx = dgv_gaming_table_types.Rows.Add();
        dgv_gaming_table_types.Rows[_row_idx].Cells[GTT_TYPE_NAME].Value = _row["GTT_NAME"];
        dgv_gaming_table_types.Rows[_row_idx].Cells[GTT_TYPE_ID].Value = _row["GT_TYPE_ID"];
        
        if ((Int32)_row["GT_TYPE_ID"] == m_default_type)
        {
          m_default_selection_type = _row_idx;
        }
      }

      if (m_default_selection_type >= 0)
      {
        dgv_gaming_table_types.Rows.RemoveAt(0);
        m_default_selection_type -= 1;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Initializes the strings of the grids.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //    
    private void InitControlsResources()
    {
      dgv_gaming_table_types.Columns[GTT_TYPE_NAME].HeaderText = Resource.String("STR_UC_GAMING_TABLE_FILTER_TYPE_COLUMN_NAME").ToUpper();
      dgv_gaming_tables.Columns[GT_NAME].HeaderText = Resource.String("STR_UC_GAMING_TABLE_FILTER_TABLE_COLUMN_NAME").ToUpper();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Updates the gaming tables grid rows.
    //
    //  PARAMS :
    //      - INPUT :
    //        - GamingTableTypeSelected: the gaming table type id
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void UpdateGamingTablesGrid()
    {
      Int32 _row_idx;
      DataTable _gaming_tables;

      dgv_gaming_tables.Rows.Clear();

      if (dgv_gaming_table_types.CurrentRow.Cells[GTT_TYPE_ID].Value == null)
      {
        if (GamingTableSelected != null)
        {
          GamingTableSelected(this, new GamingTableFilterGridChange(-1));
        }
        return;
      }

      // Get the data table 
      _gaming_tables = m_gaming_tables_and_types.DefaultView.ToTable(true, "GT_NAME", "GT_GAMING_TABLE_ID", "GT_TYPE_ID", "GT_CASHIER_SESSION");

      // Fill the gaming tables datagrid filtering by selected gaming table type id
      foreach (DataRow _row in _gaming_tables.Select("GT_TYPE_ID = " + dgv_gaming_table_types.CurrentRow.Cells[GTT_TYPE_ID].Value))
        {
         _row_idx = dgv_gaming_tables.Rows.Add();

         dgv_gaming_tables.Rows[_row_idx].Cells[GT_NAME].Value = _row["GT_NAME"];  
         dgv_gaming_tables.Rows[_row_idx].Cells[GT_ID].Value = _row["GT_GAMING_TABLE_ID"];
         dgv_gaming_tables.Rows[_row_idx].Cells[GT_TYPE_ID].Value = _row["GT_TYPE_ID"];
         dgv_gaming_tables.Rows[_row_idx].Cells[GT_CASHIER_SESSION].Value = _row["GT_CASHIER_SESSION"];

         if ((Int32)_row["GT_GAMING_TABLE_ID"] == m_default_table)
         {
           m_default_selection_table = _row_idx;
         }
        }

      }

#endregion

#region " Events "
    
    private void dgv_gaming_tables_SelectionChanged(object sender, EventArgs e)
    {
      if (GamingTableSelected != null)
      {
        if (dgv_gaming_tables.CurrentCell != null && dgv_gaming_tables.CurrentCell.Selected == true)
        {
          GamingTableSelected(this, new GamingTableFilterGridChange(dgv_gaming_tables.CurrentCell.RowIndex));
        }
        else
        {
          GamingTableSelected(this, new GamingTableFilterGridChange(-1));
        }
      }
    }

    private void dgv_gaming_table_types_SelectionChanged(object sender, EventArgs e)
    {
      if (dgv_gaming_table_types.CurrentCell != null && dgv_gaming_table_types.CurrentCell.Selected == true)
      {
        UpdateGamingTablesGrid();
      }
      else
      {
        dgv_gaming_tables.Rows.Clear();
      }

    }

#endregion

  }

}
