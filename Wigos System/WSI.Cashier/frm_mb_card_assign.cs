//------------------------------------------------------------------------------
// Copyright � 2009 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_mb_card_assign.cs
// 
//   DESCRIPTION: Implements the following classes:
//                1. frm_mb_card_assign
//
//        AUTHOR: APB
// 
// CREATION DATE: 17-JUL-2009
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 17-JUL-2009 APB    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WSI.Common;

namespace WSI.Cashier
{
  public partial class frm_mb_card_assign : Controls.frm_base
  {
    #region Attributes

    private Int64 account_id;
    private UInt64 track_data;

    #endregion

    #region Constructor

    public frm_mb_card_assign()
    {
      InitializeComponent();

      InitializeControlResources();
      uc_card_reader1.ShowMessageBottom();
     
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Initialize control resources (NLS strings, images, etc)
    /// </summary>
    private void InitializeControlResources()
    {
      // - NLS Strings:
      //   - Title:
          this.FormTitle = Resource.String("STR_FRM_CARD_ASSIGN_001");
          //lbl_card_assign_title.Text = Resource.String("STR_FRM_CARD_ASSIGN_001");
        
      //   - Labels
      lbl_account.Text = Resource.String("STR_FORMAT_GENERAL_NAME_MB");
      lbl_card_previous.Text = Resource.String("STR_FRM_CARD_ASSIGN_002");

      lbl_existent_card.Text = Resource.String("STR_FRM_CARD_ASSIGN_004");
      //   - Buttons
      btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");

      //   - Other
      this.gp_card_box_mb.HeaderText = Resource.String("STR_INFORMATION_ASSIGN_CARD");
      this.gp_box_assign_card.HeaderText = Resource.String("STR_FRM_CARD_LINK");

      // - Images:

    } // InitializeControlResources

    private void Init()
    {
      this.account_id = 0;
      this.track_data = 0;

      this.lbl_account_value.Text = "";
      this.lbl_trackdata.Text = "";
    } // Init

    private void btn_cancel_Click(object sender, EventArgs e)
    {
      this.Dispose();
    } // btn_cancel_Click

    private void uc_card_reader1_OnTrackNumberReadEvent(string TrackNumber, ref bool IsValid)
    {
      if (IsValid)
      {

        if (!CardData.CheckTrackdataSiteId(new ExternalTrackData(TrackNumber)))
        {
          lbl_existent_card.Text = Resource.String("STR_CARD_SITE_ID_ERROR");
          lbl_existent_card.Visible = true;
          timer1.Enabled = true;

          return;
        }

        // TJG 01-DEC-2009
        // Check provided trackdata is not associated to an existing account
        //    - Player Account
        //    - Mobile Bank Account
     
        //    - Player Account
        if ( CashierBusinessLogic.DB_IsCardDefined(TrackNumber) )
        {
          // This card is already assigned to a 'Player' account.
          frm_message.Show(Resource.String("STR_UC_MB_CARD_USER_MSG_CARD_ALREADY_LINKED_TO_USER_ACCT"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this);
          lbl_existent_card.Visible = true;
          timer1.Enabled = true;

          return;
        }
        
        //    - Mobile Bank Account
        if ( CashierBusinessLogic.DB_IsMBCardDefined(TrackNumber) )
        {
          // This card is already assigned to a 'Mobile Bank' account.
          frm_message.Show(Resource.String("STR_UC_CARD_USER_MSG_CARD_ALREADY_LINKED_TO_MB_ACCT"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this);
          lbl_existent_card.Visible = true;
          timer1.Enabled = true;

          return;
        }

        if (CashierBusinessLogic.DB_MBCardUpdateInfo(TrackNumber, account_id) == true)
        {
          this.Dispose();
        }
        else
        {
          lbl_existent_card.Text = Resource.String("STR_FRM_CARD_ASSIGN_004");
          lbl_existent_card.Visible = true;
          timer1.Enabled = true;
        }
      }
    } // uc_card_reader1_OnTrackNumberReadEvent

    private void timer1_Tick(object sender, EventArgs e)
    {
      this.lbl_existent_card.Visible = false;
      timer1.Enabled = false;
    } // timer1_Tick

    private void frm_mb_card_assign_Shown(object sender, EventArgs e)
    {
      this.uc_card_reader1.Focus();
    } // frm_mb_card_assign_Shown

    #endregion

    #region Public Methods

    public void GetCardData(Int64 Id, UInt64 TrackData)
    {
      this.account_id = Id;
      this.track_data = TrackData;

      this.lbl_account_value.Text = Id.ToString();
      this.lbl_trackdata.Text = String.Format("{0:D20}", TrackData);
    }

    private void pnl_data_Paint(object sender, PaintEventArgs e)
    {

    } // GetCardData

    #endregion

  }
}