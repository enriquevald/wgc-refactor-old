namespace WSI.Cashier
{
  partial class frm_gift_request
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_gift_request));
      this.gp_account = new WSI.Cashier.Controls.uc_round_panel();
      this.lbl_gift_limits = new WSI.Cashier.Controls.uc_label();
      this.lbl_acc_gift_points_suffix = new WSI.Cashier.Controls.uc_label();
      this.lbl_acc_final_points_suffix = new WSI.Cashier.Controls.uc_label();
      this.lbl_acc_starting_points_suffix = new WSI.Cashier.Controls.uc_label();
      this.lbl_acc_gift_points = new WSI.Cashier.Controls.uc_label();
      this.lbl_acc_gift_points_prefix = new WSI.Cashier.Controls.uc_label();
      this.lbl_acc_final_points = new WSI.Cashier.Controls.uc_label();
      this.lbl_acc_final_points_prefix = new WSI.Cashier.Controls.uc_label();
      this.lbl_acc_starting_points = new WSI.Cashier.Controls.uc_label();
      this.lbl_acc_starting_points_prefix = new WSI.Cashier.Controls.uc_label();
      this.gp_gift = new WSI.Cashier.Controls.uc_round_panel();
      this.lbl_gift_items = new WSI.Cashier.Controls.uc_label();
      this.lbl_gift_value = new WSI.Cashier.Controls.uc_label();
      this.lbl_gift_name = new WSI.Cashier.Controls.uc_label();
      this.gp_voucher_box = new WSI.Cashier.Controls.uc_round_panel();
      this.web_browser = new System.Windows.Forms.WebBrowser();
      this.btn_ok = new WSI.Cashier.Controls.uc_round_button();
      this.btn_cancel = new WSI.Cashier.Controls.uc_round_button();
      this.gp_digits_box = new WSI.Cashier.Controls.uc_round_panel();
      this.lbl_info = new WSI.Cashier.Controls.uc_label();
      this.txt_points_amount = new WSI.Cashier.Controls.uc_round_textbox();
      this.btn_num_1 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_back = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_2 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_intro = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_3 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_4 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_10 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_5 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_dot = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_6 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_9 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_7 = new WSI.Cashier.Controls.uc_round_button();
      this.btn_num_8 = new WSI.Cashier.Controls.uc_round_button();
      this.pnl_data.SuspendLayout();
      this.gp_account.SuspendLayout();
      this.gp_gift.SuspendLayout();
      this.gp_voucher_box.SuspendLayout();
      this.gp_digits_box.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.Controls.Add(this.gp_account);
      this.pnl_data.Controls.Add(this.gp_gift);
      this.pnl_data.Controls.Add(this.gp_voucher_box);
      this.pnl_data.Controls.Add(this.btn_ok);
      this.pnl_data.Controls.Add(this.btn_cancel);
      this.pnl_data.Controls.Add(this.gp_digits_box);
      this.pnl_data.Size = new System.Drawing.Size(833, 627);
      // 
      // gp_account
      // 
      this.gp_account.BackColor = System.Drawing.Color.Transparent;
      this.gp_account.Controls.Add(this.lbl_gift_limits);
      this.gp_account.Controls.Add(this.lbl_acc_gift_points_suffix);
      this.gp_account.Controls.Add(this.lbl_acc_final_points_suffix);
      this.gp_account.Controls.Add(this.lbl_acc_starting_points_suffix);
      this.gp_account.Controls.Add(this.lbl_acc_gift_points);
      this.gp_account.Controls.Add(this.lbl_acc_gift_points_prefix);
      this.gp_account.Controls.Add(this.lbl_acc_final_points);
      this.gp_account.Controls.Add(this.lbl_acc_final_points_prefix);
      this.gp_account.Controls.Add(this.lbl_acc_starting_points);
      this.gp_account.Controls.Add(this.lbl_acc_starting_points_prefix);
      this.gp_account.CornerRadius = 10;
      this.gp_account.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gp_account.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gp_account.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gp_account.HeaderHeight = 35;
      this.gp_account.HeaderSubText = null;
      this.gp_account.HeaderText = null;
      this.gp_account.Location = new System.Drawing.Point(10, 155);
      this.gp_account.Name = "gp_account";
      this.gp_account.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gp_account.Size = new System.Drawing.Size(472, 149);
      this.gp_account.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.gp_account.TabIndex = 107;
      this.gp_account.Text = "xAccount";
      // 
      // lbl_gift_limits
      // 
      this.lbl_gift_limits.BackColor = System.Drawing.Color.Transparent;
      this.lbl_gift_limits.Font = new System.Drawing.Font("Montserrat", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_gift_limits.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_gift_limits.Location = new System.Drawing.Point(5, 125);
      this.lbl_gift_limits.Name = "lbl_gift_limits";
      this.lbl_gift_limits.Size = new System.Drawing.Size(462, 20);
      this.lbl_gift_limits.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_gift_limits.TabIndex = 119;
      this.lbl_gift_limits.Text = "xGift Limits";
      this.lbl_gift_limits.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
      // 
      // lbl_acc_gift_points_suffix
      // 
      this.lbl_acc_gift_points_suffix.BackColor = System.Drawing.Color.Transparent;
      this.lbl_acc_gift_points_suffix.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_acc_gift_points_suffix.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_acc_gift_points_suffix.Location = new System.Drawing.Point(262, 69);
      this.lbl_acc_gift_points_suffix.Name = "lbl_acc_gift_points_suffix";
      this.lbl_acc_gift_points_suffix.Size = new System.Drawing.Size(89, 30);
      this.lbl_acc_gift_points_suffix.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_acc_gift_points_suffix.TabIndex = 118;
      this.lbl_acc_gift_points_suffix.Text = "xpoints";
      this.lbl_acc_gift_points_suffix.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_acc_final_points_suffix
      // 
      this.lbl_acc_final_points_suffix.BackColor = System.Drawing.Color.Transparent;
      this.lbl_acc_final_points_suffix.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_acc_final_points_suffix.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_acc_final_points_suffix.Location = new System.Drawing.Point(262, 96);
      this.lbl_acc_final_points_suffix.Name = "lbl_acc_final_points_suffix";
      this.lbl_acc_final_points_suffix.Size = new System.Drawing.Size(89, 30);
      this.lbl_acc_final_points_suffix.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_acc_final_points_suffix.TabIndex = 117;
      this.lbl_acc_final_points_suffix.Text = "xpoints";
      this.lbl_acc_final_points_suffix.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_acc_starting_points_suffix
      // 
      this.lbl_acc_starting_points_suffix.BackColor = System.Drawing.Color.Transparent;
      this.lbl_acc_starting_points_suffix.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_acc_starting_points_suffix.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_acc_starting_points_suffix.Location = new System.Drawing.Point(262, 41);
      this.lbl_acc_starting_points_suffix.Name = "lbl_acc_starting_points_suffix";
      this.lbl_acc_starting_points_suffix.Size = new System.Drawing.Size(89, 30);
      this.lbl_acc_starting_points_suffix.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_acc_starting_points_suffix.TabIndex = 116;
      this.lbl_acc_starting_points_suffix.Text = "xpoints";
      this.lbl_acc_starting_points_suffix.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_acc_gift_points
      // 
      this.lbl_acc_gift_points.BackColor = System.Drawing.Color.Transparent;
      this.lbl_acc_gift_points.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_acc_gift_points.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.lbl_acc_gift_points.Location = new System.Drawing.Point(153, 69);
      this.lbl_acc_gift_points.Name = "lbl_acc_gift_points";
      this.lbl_acc_gift_points.Size = new System.Drawing.Size(107, 30);
      this.lbl_acc_gift_points.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_RED;
      this.lbl_acc_gift_points.TabIndex = 115;
      this.lbl_acc_gift_points.Text = "12.999.999";
      this.lbl_acc_gift_points.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_acc_gift_points_prefix
      // 
      this.lbl_acc_gift_points_prefix.BackColor = System.Drawing.Color.Transparent;
      this.lbl_acc_gift_points_prefix.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_acc_gift_points_prefix.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_acc_gift_points_prefix.Location = new System.Drawing.Point(3, 69);
      this.lbl_acc_gift_points_prefix.Name = "lbl_acc_gift_points_prefix";
      this.lbl_acc_gift_points_prefix.Size = new System.Drawing.Size(143, 30);
      this.lbl_acc_gift_points_prefix.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_acc_gift_points_prefix.TabIndex = 114;
      this.lbl_acc_gift_points_prefix.Text = "xGift:";
      this.lbl_acc_gift_points_prefix.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_acc_final_points
      // 
      this.lbl_acc_final_points.BackColor = System.Drawing.Color.Transparent;
      this.lbl_acc_final_points.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_acc_final_points.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_acc_final_points.Location = new System.Drawing.Point(153, 96);
      this.lbl_acc_final_points.Name = "lbl_acc_final_points";
      this.lbl_acc_final_points.Size = new System.Drawing.Size(107, 30);
      this.lbl_acc_final_points.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_acc_final_points.TabIndex = 113;
      this.lbl_acc_final_points.Text = "12.999.999";
      this.lbl_acc_final_points.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_acc_final_points_prefix
      // 
      this.lbl_acc_final_points_prefix.BackColor = System.Drawing.Color.Transparent;
      this.lbl_acc_final_points_prefix.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_acc_final_points_prefix.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_acc_final_points_prefix.Location = new System.Drawing.Point(3, 96);
      this.lbl_acc_final_points_prefix.Name = "lbl_acc_final_points_prefix";
      this.lbl_acc_final_points_prefix.Size = new System.Drawing.Size(143, 30);
      this.lbl_acc_final_points_prefix.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_acc_final_points_prefix.TabIndex = 112;
      this.lbl_acc_final_points_prefix.Text = "xFinal Balance:";
      this.lbl_acc_final_points_prefix.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_acc_starting_points
      // 
      this.lbl_acc_starting_points.BackColor = System.Drawing.Color.Transparent;
      this.lbl_acc_starting_points.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_acc_starting_points.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_acc_starting_points.Location = new System.Drawing.Point(153, 41);
      this.lbl_acc_starting_points.Name = "lbl_acc_starting_points";
      this.lbl_acc_starting_points.Size = new System.Drawing.Size(107, 30);
      this.lbl_acc_starting_points.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_acc_starting_points.TabIndex = 111;
      this.lbl_acc_starting_points.Text = "12.999.999";
      this.lbl_acc_starting_points.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_acc_starting_points_prefix
      // 
      this.lbl_acc_starting_points_prefix.BackColor = System.Drawing.Color.Transparent;
      this.lbl_acc_starting_points_prefix.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_acc_starting_points_prefix.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_acc_starting_points_prefix.Location = new System.Drawing.Point(3, 41);
      this.lbl_acc_starting_points_prefix.Name = "lbl_acc_starting_points_prefix";
      this.lbl_acc_starting_points_prefix.Size = new System.Drawing.Size(143, 30);
      this.lbl_acc_starting_points_prefix.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_acc_starting_points_prefix.TabIndex = 110;
      this.lbl_acc_starting_points_prefix.Text = "xStarting Balance:";
      this.lbl_acc_starting_points_prefix.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // gp_gift
      // 
      this.gp_gift.BackColor = System.Drawing.Color.Transparent;
      this.gp_gift.Controls.Add(this.lbl_gift_items);
      this.gp_gift.Controls.Add(this.lbl_gift_value);
      this.gp_gift.Controls.Add(this.lbl_gift_name);
      this.gp_gift.CornerRadius = 10;
      this.gp_gift.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gp_gift.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gp_gift.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gp_gift.HeaderHeight = 35;
      this.gp_gift.HeaderSubText = null;
      this.gp_gift.HeaderText = null;
      this.gp_gift.Location = new System.Drawing.Point(10, 10);
      this.gp_gift.Name = "gp_gift";
      this.gp_gift.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gp_gift.Size = new System.Drawing.Size(472, 139);
      this.gp_gift.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.gp_gift.TabIndex = 106;
      this.gp_gift.Text = "xGift";
      // 
      // lbl_gift_items
      // 
      this.lbl_gift_items.BackColor = System.Drawing.Color.Transparent;
      this.lbl_gift_items.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_gift_items.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_gift_items.Location = new System.Drawing.Point(4, 113);
      this.lbl_gift_items.Name = "lbl_gift_items";
      this.lbl_gift_items.Size = new System.Drawing.Size(462, 20);
      this.lbl_gift_items.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_gift_items.TabIndex = 111;
      this.lbl_gift_items.Text = "xGift Items";
      this.lbl_gift_items.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
      // 
      // lbl_gift_value
      // 
      this.lbl_gift_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_gift_value.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_gift_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_gift_value.Location = new System.Drawing.Point(4, 74);
      this.lbl_gift_value.Name = "lbl_gift_value";
      this.lbl_gift_value.Size = new System.Drawing.Size(462, 34);
      this.lbl_gift_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_gift_value.TabIndex = 110;
      this.lbl_gift_value.Text = "xGift Value";
      this.lbl_gift_value.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lbl_gift_name
      // 
      this.lbl_gift_name.BackColor = System.Drawing.Color.Transparent;
      this.lbl_gift_name.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_gift_name.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_gift_name.Location = new System.Drawing.Point(4, 41);
      this.lbl_gift_name.Name = "lbl_gift_name";
      this.lbl_gift_name.Size = new System.Drawing.Size(462, 34);
      this.lbl_gift_name.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_gift_name.TabIndex = 109;
      this.lbl_gift_name.Text = "xGift Name";
      this.lbl_gift_name.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // gp_voucher_box
      // 
      this.gp_voucher_box.BackColor = System.Drawing.Color.Transparent;
      this.gp_voucher_box.Controls.Add(this.web_browser);
      this.gp_voucher_box.CornerRadius = 10;
      this.gp_voucher_box.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gp_voucher_box.HeaderBackColor = System.Drawing.Color.Empty;
      this.gp_voucher_box.HeaderForeColor = System.Drawing.Color.Empty;
      this.gp_voucher_box.HeaderHeight = 0;
      this.gp_voucher_box.HeaderSubText = null;
      this.gp_voucher_box.HeaderText = null;
      this.gp_voucher_box.Location = new System.Drawing.Point(488, 10);
      this.gp_voucher_box.Name = "gp_voucher_box";
      this.gp_voucher_box.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gp_voucher_box.Size = new System.Drawing.Size(332, 536);
      this.gp_voucher_box.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.WITHOUT_HEAD;
      this.gp_voucher_box.TabIndex = 19;
      // 
      // web_browser
      // 
      this.web_browser.Location = new System.Drawing.Point(1, 3);
      this.web_browser.MinimumSize = new System.Drawing.Size(20, 20);
      this.web_browser.Name = "web_browser";
      this.web_browser.Size = new System.Drawing.Size(328, 533);
      this.web_browser.TabIndex = 16;
      // 
      // btn_ok
      // 
      this.btn_ok.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_ok.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_ok.FlatAppearance.BorderSize = 0;
      this.btn_ok.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_ok.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_ok.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_ok.Image = null;
      this.btn_ok.Location = new System.Drawing.Point(668, 558);
      this.btn_ok.Name = "btn_ok";
      this.btn_ok.Size = new System.Drawing.Size(155, 60);
      this.btn_ok.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_ok.TabIndex = 18;
      this.btn_ok.Text = "OK";
      this.btn_ok.UseVisualStyleBackColor = false;
      this.btn_ok.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_gift_request_KeyPress);
      // 
      // btn_cancel
      // 
      this.btn_cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_cancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btn_cancel.FlatAppearance.BorderSize = 0;
      this.btn_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_cancel.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_cancel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_cancel.Image = null;
      this.btn_cancel.Location = new System.Drawing.Point(489, 558);
      this.btn_cancel.Name = "btn_cancel";
      this.btn_cancel.Size = new System.Drawing.Size(155, 60);
      this.btn_cancel.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_cancel.TabIndex = 17;
      this.btn_cancel.Text = "CANCEL";
      this.btn_cancel.UseVisualStyleBackColor = false;
      this.btn_cancel.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_gift_request_KeyPress);
      // 
      // gp_digits_box
      // 
      this.gp_digits_box.BackColor = System.Drawing.Color.Transparent;
      this.gp_digits_box.Controls.Add(this.lbl_info);
      this.gp_digits_box.Controls.Add(this.txt_points_amount);
      this.gp_digits_box.Controls.Add(this.btn_num_1);
      this.gp_digits_box.Controls.Add(this.btn_back);
      this.gp_digits_box.Controls.Add(this.btn_num_2);
      this.gp_digits_box.Controls.Add(this.btn_intro);
      this.gp_digits_box.Controls.Add(this.btn_num_3);
      this.gp_digits_box.Controls.Add(this.btn_num_4);
      this.gp_digits_box.Controls.Add(this.btn_num_10);
      this.gp_digits_box.Controls.Add(this.btn_num_5);
      this.gp_digits_box.Controls.Add(this.btn_num_dot);
      this.gp_digits_box.Controls.Add(this.btn_num_6);
      this.gp_digits_box.Controls.Add(this.btn_num_9);
      this.gp_digits_box.Controls.Add(this.btn_num_7);
      this.gp_digits_box.Controls.Add(this.btn_num_8);
      this.gp_digits_box.CornerRadius = 10;
      this.gp_digits_box.Font = new System.Drawing.Font("Montserrat", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gp_digits_box.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gp_digits_box.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.gp_digits_box.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gp_digits_box.HeaderHeight = 55;
      this.gp_digits_box.HeaderSubText = null;
      this.gp_digits_box.HeaderText = null;
      this.gp_digits_box.Location = new System.Drawing.Point(10, 310);
      this.gp_digits_box.Name = "gp_digits_box";
      this.gp_digits_box.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gp_digits_box.Size = new System.Drawing.Size(380, 309);
      this.gp_digits_box.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NONE;
      this.gp_digits_box.TabIndex = 15;
      this.gp_digits_box.Text = "xEnter Points To Convert";
      // 
      // lbl_info
      // 
      this.lbl_info.BackColor = System.Drawing.Color.Transparent;
      this.lbl_info.Font = new System.Drawing.Font("Montserrat", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_info.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_info.Location = new System.Drawing.Point(5, 57);
      this.lbl_info.Name = "lbl_info";
      this.lbl_info.Size = new System.Drawing.Size(231, 18);
      this.lbl_info.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_info.TabIndex = 25;
      this.lbl_info.Text = "xMsgInfo";
      this.lbl_info.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // txt_points_amount
      // 
      this.txt_points_amount.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_points_amount.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_points_amount.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.txt_points_amount.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.txt_points_amount.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_points_amount.CornerRadius = 0;
      this.txt_points_amount.Font = new System.Drawing.Font("Montserrat", 40F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_points_amount.Location = new System.Drawing.Point(3, 6);
      this.txt_points_amount.MaxLength = 18;
      this.txt_points_amount.Multiline = false;
      this.txt_points_amount.Name = "txt_points_amount";
      this.txt_points_amount.PasswordChar = '\0';
      this.txt_points_amount.ReadOnly = true;
      this.txt_points_amount.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_points_amount.SelectedText = "";
      this.txt_points_amount.SelectionLength = 0;
      this.txt_points_amount.SelectionStart = 0;
      this.txt_points_amount.Size = new System.Drawing.Size(374, 44);
      this.txt_points_amount.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.CALCULATOR;
      this.txt_points_amount.TabIndex = 14;
      this.txt_points_amount.TabStop = false;
      this.txt_points_amount.Text = "123";
      this.txt_points_amount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      this.txt_points_amount.UseSystemPasswordChar = false;
      this.txt_points_amount.WaterMark = null;
      this.txt_points_amount.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(153)))), ((int)(((byte)(49)))));
      this.txt_points_amount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_gift_request_KeyPress);
      this.txt_points_amount.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_gift_request_PreviewKeyDown);
      // 
      // btn_num_1
      // 
      this.btn_num_1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_1.CornerRadius = 0;
      this.btn_num_1.FlatAppearance.BorderSize = 0;
      this.btn_num_1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_1.Font = new System.Drawing.Font("Montserrat", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_1.Image = null;
      this.btn_num_1.Location = new System.Drawing.Point(4, 85);
      this.btn_num_1.Name = "btn_num_1";
      this.btn_num_1.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_1.Size = new System.Drawing.Size(70, 50);
      this.btn_num_1.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_1.TabIndex = 0;
      this.btn_num_1.TabStop = false;
      this.btn_num_1.Text = "1";
      this.btn_num_1.UseVisualStyleBackColor = false;
      this.btn_num_1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_gift_request_KeyPress);
      this.btn_num_1.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_gift_request_PreviewKeyDown);
      // 
      // btn_back
      // 
      this.btn_back.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_back.CornerRadius = 0;
      this.btn_back.FlatAppearance.BorderSize = 0;
      this.btn_back.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_back.Font = new System.Drawing.Font("Montserrat", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_back.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_back.Image = ((System.Drawing.Image)(resources.GetObject("btn_back.Image")));
      this.btn_back.Location = new System.Drawing.Point(235, 85);
      this.btn_back.Name = "btn_back";
      this.btn_back.Size = new System.Drawing.Size(141, 50);
      this.btn_back.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_BACKSPACE;
      this.btn_back.TabIndex = 13;
      this.btn_back.TabStop = false;
      this.btn_back.UseVisualStyleBackColor = false;
      this.btn_back.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_gift_request_KeyPress);
      this.btn_back.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_gift_request_PreviewKeyDown);
      // 
      // btn_num_2
      // 
      this.btn_num_2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_2.CornerRadius = 0;
      this.btn_num_2.FlatAppearance.BorderSize = 0;
      this.btn_num_2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_2.Font = new System.Drawing.Font("Montserrat", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_2.Image = null;
      this.btn_num_2.Location = new System.Drawing.Point(81, 85);
      this.btn_num_2.Name = "btn_num_2";
      this.btn_num_2.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_2.Size = new System.Drawing.Size(70, 50);
      this.btn_num_2.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_2.TabIndex = 1;
      this.btn_num_2.TabStop = false;
      this.btn_num_2.Text = "2";
      this.btn_num_2.UseVisualStyleBackColor = false;
      this.btn_num_2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_gift_request_KeyPress);
      this.btn_num_2.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_gift_request_PreviewKeyDown);
      // 
      // btn_intro
      // 
      this.btn_intro.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(153)))), ((int)(((byte)(49)))));
      this.btn_intro.CornerRadius = 0;
      this.btn_intro.FlatAppearance.BorderSize = 0;
      this.btn_intro.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_intro.Font = new System.Drawing.Font("Montserrat", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_intro.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_intro.Image = ((System.Drawing.Image)(resources.GetObject("btn_intro.Image")));
      this.btn_intro.Location = new System.Drawing.Point(235, 140);
      this.btn_intro.Name = "btn_intro";
      this.btn_intro.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_intro.Size = new System.Drawing.Size(141, 160);
      this.btn_intro.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_ENTER;
      this.btn_intro.TabIndex = 12;
      this.btn_intro.UseVisualStyleBackColor = false;
      this.btn_intro.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_gift_request_KeyPress);
      this.btn_intro.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_gift_request_PreviewKeyDown);
      // 
      // btn_num_3
      // 
      this.btn_num_3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_3.CornerRadius = 0;
      this.btn_num_3.FlatAppearance.BorderSize = 0;
      this.btn_num_3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_3.Font = new System.Drawing.Font("Montserrat", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_3.Image = null;
      this.btn_num_3.Location = new System.Drawing.Point(158, 85);
      this.btn_num_3.Name = "btn_num_3";
      this.btn_num_3.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_3.Size = new System.Drawing.Size(70, 50);
      this.btn_num_3.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_3.TabIndex = 2;
      this.btn_num_3.TabStop = false;
      this.btn_num_3.Text = "3";
      this.btn_num_3.UseVisualStyleBackColor = false;
      this.btn_num_3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_gift_request_KeyPress);
      this.btn_num_3.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_gift_request_PreviewKeyDown);
      // 
      // btn_num_4
      // 
      this.btn_num_4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_4.CornerRadius = 0;
      this.btn_num_4.FlatAppearance.BorderSize = 0;
      this.btn_num_4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_4.Font = new System.Drawing.Font("Montserrat", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_4.Image = null;
      this.btn_num_4.Location = new System.Drawing.Point(4, 140);
      this.btn_num_4.Name = "btn_num_4";
      this.btn_num_4.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_4.Size = new System.Drawing.Size(70, 50);
      this.btn_num_4.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_4.TabIndex = 3;
      this.btn_num_4.TabStop = false;
      this.btn_num_4.Text = "4";
      this.btn_num_4.UseVisualStyleBackColor = false;
      this.btn_num_4.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_gift_request_KeyPress);
      this.btn_num_4.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_gift_request_PreviewKeyDown);
      // 
      // btn_num_10
      // 
      this.btn_num_10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_10.CornerRadius = 0;
      this.btn_num_10.FlatAppearance.BorderSize = 0;
      this.btn_num_10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_10.Font = new System.Drawing.Font("Montserrat", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_10.Image = null;
      this.btn_num_10.Location = new System.Drawing.Point(4, 250);
      this.btn_num_10.Name = "btn_num_10";
      this.btn_num_10.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_10.Size = new System.Drawing.Size(147, 50);
      this.btn_num_10.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_10.TabIndex = 10;
      this.btn_num_10.TabStop = false;
      this.btn_num_10.Text = "0";
      this.btn_num_10.UseVisualStyleBackColor = false;
      this.btn_num_10.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_gift_request_KeyPress);
      this.btn_num_10.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_gift_request_PreviewKeyDown);
      // 
      // btn_num_5
      // 
      this.btn_num_5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_5.CornerRadius = 0;
      this.btn_num_5.FlatAppearance.BorderSize = 0;
      this.btn_num_5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_5.Font = new System.Drawing.Font("Montserrat", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_5.Image = null;
      this.btn_num_5.Location = new System.Drawing.Point(81, 140);
      this.btn_num_5.Name = "btn_num_5";
      this.btn_num_5.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_5.Size = new System.Drawing.Size(70, 50);
      this.btn_num_5.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_5.TabIndex = 4;
      this.btn_num_5.TabStop = false;
      this.btn_num_5.Text = "5";
      this.btn_num_5.UseVisualStyleBackColor = false;
      this.btn_num_5.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_gift_request_KeyPress);
      this.btn_num_5.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_gift_request_PreviewKeyDown);
      // 
      // btn_num_dot
      // 
      this.btn_num_dot.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_dot.CornerRadius = 0;
      this.btn_num_dot.FlatAppearance.BorderSize = 0;
      this.btn_num_dot.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_dot.Font = new System.Drawing.Font("Montserrat", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_dot.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_dot.Image = null;
      this.btn_num_dot.Location = new System.Drawing.Point(158, 250);
      this.btn_num_dot.Name = "btn_num_dot";
      this.btn_num_dot.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_dot.Size = new System.Drawing.Size(70, 50);
      this.btn_num_dot.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_dot.TabIndex = 9;
      this.btn_num_dot.TabStop = false;
      this.btn_num_dot.Text = "�";
      this.btn_num_dot.UseVisualStyleBackColor = false;
      this.btn_num_dot.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_gift_request_KeyPress);
      this.btn_num_dot.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_gift_request_PreviewKeyDown);
      // 
      // btn_num_6
      // 
      this.btn_num_6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_6.CornerRadius = 0;
      this.btn_num_6.FlatAppearance.BorderSize = 0;
      this.btn_num_6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_6.Font = new System.Drawing.Font("Montserrat", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_6.Image = null;
      this.btn_num_6.Location = new System.Drawing.Point(158, 140);
      this.btn_num_6.Name = "btn_num_6";
      this.btn_num_6.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_6.Size = new System.Drawing.Size(70, 50);
      this.btn_num_6.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_6.TabIndex = 5;
      this.btn_num_6.TabStop = false;
      this.btn_num_6.Text = "6";
      this.btn_num_6.UseVisualStyleBackColor = false;
      this.btn_num_6.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_gift_request_KeyPress);
      this.btn_num_6.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_gift_request_PreviewKeyDown);
      // 
      // btn_num_9
      // 
      this.btn_num_9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_9.CornerRadius = 0;
      this.btn_num_9.FlatAppearance.BorderSize = 0;
      this.btn_num_9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_9.Font = new System.Drawing.Font("Montserrat", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_9.Image = null;
      this.btn_num_9.Location = new System.Drawing.Point(158, 195);
      this.btn_num_9.Name = "btn_num_9";
      this.btn_num_9.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_9.Size = new System.Drawing.Size(70, 50);
      this.btn_num_9.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_9.TabIndex = 8;
      this.btn_num_9.TabStop = false;
      this.btn_num_9.Text = "9";
      this.btn_num_9.UseVisualStyleBackColor = false;
      this.btn_num_9.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_gift_request_KeyPress);
      this.btn_num_9.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_gift_request_PreviewKeyDown);
      // 
      // btn_num_7
      // 
      this.btn_num_7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_7.CornerRadius = 0;
      this.btn_num_7.FlatAppearance.BorderSize = 0;
      this.btn_num_7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_7.Font = new System.Drawing.Font("Montserrat", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_7.Image = null;
      this.btn_num_7.Location = new System.Drawing.Point(4, 195);
      this.btn_num_7.Name = "btn_num_7";
      this.btn_num_7.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_7.Size = new System.Drawing.Size(70, 50);
      this.btn_num_7.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_7.TabIndex = 6;
      this.btn_num_7.TabStop = false;
      this.btn_num_7.Text = "7";
      this.btn_num_7.UseVisualStyleBackColor = false;
      this.btn_num_7.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_gift_request_KeyPress);
      this.btn_num_7.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_gift_request_PreviewKeyDown);
      // 
      // btn_num_8
      // 
      this.btn_num_8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.btn_num_8.CornerRadius = 0;
      this.btn_num_8.FlatAppearance.BorderSize = 0;
      this.btn_num_8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_num_8.Font = new System.Drawing.Font("Montserrat", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_num_8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_num_8.Image = null;
      this.btn_num_8.Location = new System.Drawing.Point(81, 195);
      this.btn_num_8.Name = "btn_num_8";
      this.btn_num_8.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
      this.btn_num_8.Size = new System.Drawing.Size(70, 50);
      this.btn_num_8.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.CALC_NUMBER;
      this.btn_num_8.TabIndex = 7;
      this.btn_num_8.TabStop = false;
      this.btn_num_8.Text = "8";
      this.btn_num_8.UseVisualStyleBackColor = false;
      this.btn_num_8.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_gift_request_KeyPress);
      this.btn_num_8.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_gift_request_PreviewKeyDown);
      // 
      // frm_gift_request
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.btn_cancel;
      this.ClientSize = new System.Drawing.Size(833, 682);
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "frm_gift_request";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Style = WSI.Cashier.Controls.frm_base.BaseFormStyle.PRINCIPAL;
      this.Text = "Gift Request";
      this.Shown += new System.EventHandler(this.frm_gift_request_Shown);
      this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_gift_request_KeyPress);
      this.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_gift_request_PreviewKeyDown);
      this.pnl_data.ResumeLayout(false);
      this.gp_account.ResumeLayout(false);
      this.gp_gift.ResumeLayout(false);
      this.gp_voucher_box.ResumeLayout(false);
      this.gp_digits_box.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private WSI.Cashier.Controls.uc_round_button btn_num_1;
    private WSI.Cashier.Controls.uc_round_button btn_num_10;
    private WSI.Cashier.Controls.uc_round_button btn_num_dot;
    private WSI.Cashier.Controls.uc_round_button btn_num_9;
    private WSI.Cashier.Controls.uc_round_button btn_num_8;
    private WSI.Cashier.Controls.uc_round_button btn_num_7;
    private WSI.Cashier.Controls.uc_round_button btn_num_6;
    private WSI.Cashier.Controls.uc_round_button btn_num_5;
    private WSI.Cashier.Controls.uc_round_button btn_num_4;
    private WSI.Cashier.Controls.uc_round_button btn_num_3;
    private WSI.Cashier.Controls.uc_round_button btn_num_2;
    private WSI.Cashier.Controls.uc_round_button btn_back;
    private WSI.Cashier.Controls.uc_round_button btn_intro;
    private WSI.Cashier.Controls.uc_round_textbox txt_points_amount;
    private WSI.Cashier.Controls.uc_round_panel gp_digits_box;
    public System.Windows.Forms.WebBrowser web_browser;
    private WSI.Cashier.Controls.uc_round_button btn_cancel;
    private WSI.Cashier.Controls.uc_round_button btn_ok;
    private WSI.Cashier.Controls.uc_round_panel gp_voucher_box;
    private WSI.Cashier.Controls.uc_round_panel gp_gift;
    private WSI.Cashier.Controls.uc_label lbl_gift_name;
    private WSI.Cashier.Controls.uc_round_panel gp_account;
    private WSI.Cashier.Controls.uc_label lbl_acc_final_points;
    private WSI.Cashier.Controls.uc_label lbl_acc_final_points_prefix;
    private WSI.Cashier.Controls.uc_label lbl_acc_starting_points;
    private WSI.Cashier.Controls.uc_label lbl_acc_starting_points_prefix;
    private WSI.Cashier.Controls.uc_label lbl_acc_gift_points;
    private WSI.Cashier.Controls.uc_label lbl_acc_gift_points_prefix;
    private WSI.Cashier.Controls.uc_label lbl_gift_value;
    private WSI.Cashier.Controls.uc_label lbl_acc_gift_points_suffix;
    private WSI.Cashier.Controls.uc_label lbl_acc_final_points_suffix;
    private WSI.Cashier.Controls.uc_label lbl_acc_starting_points_suffix;
    private WSI.Cashier.Controls.uc_label lbl_gift_items;
    private WSI.Cashier.Controls.uc_label lbl_gift_limits;
    private Controls.uc_label lbl_info;
  }
}