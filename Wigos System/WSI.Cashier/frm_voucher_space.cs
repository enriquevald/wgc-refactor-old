//------------------------------------------------------------------------------
// Copyright � 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_voucher_space.cs
// 
//   DESCRIPTION: Implements the following classes:
//                1. frm_card_assign
// 
//        AUTHOR: AA
// 
// CREATION DATE: 04-SEP-2007
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 04-SEP-2007 AA     First release.
// 25-MAY-2012 SSC    Moved CardData, PlaySession and PlayerTrackingData classes to WSI.Common
// 08-MAY-2013 JCA    Added credit redemption functionality.
// 22-SEP-2014 LEM    Fixed Bug #WIG-1247: Wrong provider statistics SPACE-TITO.
// 08-OCT-2014 SMN    Change default error message
// 30-JUN-2015 MPO    WIG-2492: Get and set account promo id for calculate cost
// 09-NOV-2015 SGB    Backlog Item WIG-5898: Change designer
// 18-OCT-2016 FAV    PBI 20407: TITO Ticket: Calculated fields
//------------------------------------------------------------------------------
using System;
using System.Collections;
using System.Drawing;
using WSI.Common;
using System.Windows.Forms;
using System.Threading;
using WSI.Cashier.TITO;
using WSI.Common.TITO;
using System.Data;

namespace WSI.Cashier
{
    public partial class frm_voucher_space : WSI.Cashier.Controls.frm_base
    {
        #region Attributes

        private static frm_amount_input m_frm_amount_input = new frm_amount_input();
        private static CardData m_card_data = new CardData();
        private Thread m_thread;
        private static WWP_MSRequestStatus m_request_status;
        private static String m_output_data;
        private static Int64 m_unique_id_redeem;
        private static Int64 m_unique_id_confirm_redeem;
        private static Elp01InputData m_input_data;
        private static String m_input_data_str;
        private static Int64 m_account_id;
        private static String m_voucher_id;
        //private static Boolean m_waiting_status;
        private static Elp01OutputData m_elp01_output_data;
        private static Boolean m_voucher_is_already_paid;
        private static Boolean m_database_error;
        private static int m_tick_redeem;

        #endregion

        #region Constructor
        //------------------------------------------------------------------------------
        // PURPOSE : Constructor.
        //
        //  PARAMS :
        //      - INPUT :
        //
        //      - OUTPUT :
        //
        // RETURNS :
        //
        //   NOTES :
        //
        public frm_voucher_space()
        {
            InitializeComponent();

            InitializeControlResources();
        } // frm_voucher_space

        #endregion

        #region Private Methods

        //------------------------------------------------------------------------------
        // PURPOSE : Initialize control resources (NLS strings, images, etc)
        //
        //  PARAMS :
        //      - INPUT :
        //
        //      - OUTPUT :
        //
        // RETURNS :
        //
        //   NOTES :
        //
        private void InitializeControlResources()
        {
            // NLS Strings:
            this.FormTitle = Resource.String("STR_FRM_VOUCHER_SPACE_TITLE");    // Credit redemption

            btn_accept.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_OK");               // Accept.

            btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");           // Cancel.

            m_frm_amount_input.InitializeControlResources();

        } // InitializeControlResources

        //------------------------------------------------------------------------------
        // PURPOSE : Initialize values.
        //
        //  PARAMS :
        //      - INPUT :
        //
        //      - OUTPUT :
        //
        // RETURNS :
        //
        //   NOTES :
        //
        private void Init()
        {
            m_request_status = WWP_MSRequestStatus.MS_REQUEST_STATUS_PENDING;
            m_output_data = string.Empty;
            m_account_id = 0;
            m_unique_id_redeem = 0;
            m_unique_id_confirm_redeem = 0;
            //m_waiting_status = false;
            lbl_waiting.Visible = false;

            StartPosition = FormStartPosition.CenterParent;

        } // Init

        //------------------------------------------------------------------------------
        // PURPOSE : Enable controls.
        //
        //  PARAMS :
        //      - INPUT : bolDisable
        //
        //      - OUTPUT :
        //
        // RETURNS :
        //
        //   NOTES :
        //
        private void EnableControls(Boolean bolDisable)
        {
            btn_accept.Enabled = bolDisable;
            btn_cancel.Enabled = bolDisable;
            txt_voucher_name.Enabled = bolDisable;
        } // EnableControls

        //------------------------------------------------------------------------------
        // PURPOSE : Initialize and starts the Timer.
        //
        //  PARAMS :
        //      - INPUT : 
        //
        //      - OUTPUT :
        //
        // RETURNS :
        //
        //   NOTES :
        //
        private void InitTimer()
        {
            //timer1.Tick += timer1_Tick;
            timer1.Interval = 50;      // Timer will tick every 500 miliseconds.
            timer1.Enabled = true;      // Enable the timer.
            timer1.Start();             // Start the timer.
        } // InitTimer

        //------------------------------------------------------------------------------
        // PURPOSE : Main thread for credit redemption.
        //
        //  PARAMS :
        //      - INPUT : 
        //
        //      - OUTPUT :
        //
        // RETURNS :
        //
        //   NOTES :
        //
        private static void DoRequestToMultisite()
        {
            WWP_MSRequestStatus _output_status;

            m_voucher_is_already_paid = false; // Set value in FindVouchersById
            m_database_error = true;
            _output_status = WWP_MSRequestStatus.MS_REQUEST_STATUS_ERROR;

            try
            {
                m_input_data = new Elp01InputData();
                m_input_data.RequestType = ELP01_Requests.ELP01_REQUEST_VOUCHER_REDEEM;
                m_input_data.AccountId = m_account_id;
                m_input_data.VoucherId = m_voucher_id;
                m_input_data_str = m_input_data.ToXml();

                using (DB_TRX _db_trx = new DB_TRX())
                {
                    // Find "VoucherId" into Database
                    if (Elp01Vouchers.FindVouchersById(m_account_id, m_voucher_id, out m_voucher_is_already_paid, _db_trx.SqlTransaction))
                    {
                        m_database_error = false;

                        // Voucher is not redeemed
                        if (!m_voucher_is_already_paid)
                        {
                            // Add Request "Redimir Voucher" into Database.
                            if (MultiSiteRequests.AddRequest(WWP_MultiSiteRequestType.MULTISITE_REQUEST_TYPE_ELP01, m_input_data_str, 1, 1, out m_unique_id_redeem, _db_trx.SqlTransaction))
                            {
                                _db_trx.Commit();

                                _output_status = MultiSiteRequests.WaitRequest(m_unique_id_redeem, out m_output_data);
                            }
                        }
                    }
                }
            }
            catch (Exception _ex)
            {
                Log.Exception(_ex);
            }
            finally
            {
                m_request_status = _output_status;

                if (_output_status == WWP_MSRequestStatus.MS_REQUEST_STATUS_ERROR)
                {
                    Alarm.Register(AlarmSourceCode.Cashier,     // AlarmSourceCode SourceCode.
                                   (long)ENUM_GUI.CASHIER,      // long sourceId.
                                   "Cashier",                   // string sourceName.
                                   AlarmCode.ELP_ErrorStatus,   // AlarmCode code
                                   Resource.String("STR_FRM_VOUCHER_SPACE_ALARM_1"));
                }
            }
        } // DoRequestToMultisite

        //------------------------------------------------------------------------------
        // PURPOSE : Check voucher number.
        //
        //  PARAMS :
        //      - INPUT : 
        //
        //      - OUTPUT :
        //
        // RETURNS :
        //
        //   NOTES :
        //
        private Boolean IsVoucherNumberOK()
        {
            if (txt_voucher_name.Text.Trim() == string.Empty)
            {
                lbl_waiting.Text = Resource.String("STR_FRM_VOUCHER_SPACE_EMPTY");
                lbl_waiting.Visible = true;
                return false;
            }

            lbl_waiting.Visible = false;
            lbl_waiting.Text = string.Empty;
            return true;

        } // IsVoucherNumberOK


        private void btn_accept_Click(object sender, EventArgs e)
        {

            m_tick_redeem = Environment.TickCount;
            try
            {
                if (!IsVoucherNumberOK())
                {
                    return;
                }

                using (DB_TRX _db_trx = new DB_TRX())
                {
                    if (WGDB.GetMultiSiteStatus(_db_trx.SqlTransaction) != WGDB.MULTISITE_STATUS.MEMBER_CONNECTED)
                    {
                        lbl_waiting.Visible = false;
                        frm_message.Show(Resource.String("STR_ELP_NOT_CONNECTED_TO_MULTISITE"),
                                          Resource.String("STR_FRM_VOUCHER_SPACE_POPUP"),
                                          MessageBoxButtons.OK,
                                          Images.CashierImage.Error,
                                          Cashier.MainForm);

                        WSIKeyboard.Hide();
                        Close();

                        return;
                    }
                }

                m_request_status = WWP_MSRequestStatus.MS_REQUEST_STATUS_PENDING;

                WSIKeyboard.Hide();

                m_account_id = m_card_data.AccountId;
                m_voucher_id = txt_voucher_name.Text;

                // Disable buttons for waiting request.
                EnableControls(false);

                // Show message.
                lbl_waiting.Text = Resource.String("STR_FRM_VOUCHER_SPACE_WAITING");
                lbl_waiting.Visible = true;

                m_thread = new Thread(DoRequestToMultisite);
                m_thread.Name = "DoRequestToMultisite";
                m_thread.SetApartmentState(ApartmentState.STA);
                m_thread.Start();

                // Initialize and starts the timer.
                InitTimer();
            }
            catch (Exception _ex)
            {
                Log.Exception(_ex);
            }
        } // btn_accept_Click


        //------------------------------------------------------------------------------
        // PURPOSE : Click event from "Cancel" button.
        //
        //  PARAMS :
        //      - INPUT : 
        //
        //      - OUTPUT :
        //
        // RETURNS :
        //
        //   NOTES :
        //
        private void btn_cancel_Click(object sender, EventArgs e)
        {
            // Hide keyboard.
            WSIKeyboard.Hide();
            Close();

        } // Click


        //------------------------------------------------------------------------------
        // PURPOSE : Event Tick for Timer1.
        //
        //  PARAMS :
        //      - INPUT : 
        //
        //      - OUTPUT :
        //
        // RETURNS :
        //
        //   NOTES :
        //
        private void timer1_Tick(object sender, EventArgs e)
        {
            Int32 _process_tick;
            String _str_message;
            Boolean _is_error;
            Boolean _valid_voucher;

            ////Currency _amount_redeemable;
            ////Currency _amount_non_redeemable;
            ////OperationCode _operation_code;

            ////Boolean _input_params_no_money = false;

            // Do nothing if the output is empty.
            if (m_request_status == WWP_MSRequestStatus.MS_REQUEST_STATUS_PENDING)
            {
                return;
            }

            _process_tick = Environment.TickCount;

            // Default Error message
            // 08-OCT-2014 SMN
            _str_message = String.Format(Resource.String("STR_FRM_VOUCHER_SPACE_CREDIT_ADDED_ERROR"), GeneralParam.GetString("ExternalLoyaltyProgram.Mode01", "Name")).Replace("\\r\\n", "\r\n");

            _is_error = true;
            _valid_voucher = true;

            try
            {
                // Delete id request in the DDBB.
                MultiSiteRequests.DeleteRequest(m_unique_id_redeem);

                switch (m_request_status)
                {
                    case WWP_MSRequestStatus.MS_REQUEST_STATUS_OK:        // MS_REQUEST_STATUS_OK

                        if (m_output_data == string.Empty)
                        {
                            return;
                        }

                        // Getting the XML response.
                        m_elp01_output_data = new Elp01OutputData();
                        m_elp01_output_data.LoadXml(m_output_data);

                        switch (m_elp01_output_data.ResponseCodes)
                        {
                            case ELP01_ResponseCodes.ELP01_RESPONSE_CODE_OK:  // ELP01_RESPONSE_CODE_OK

                                ACCOUNT_PROMO_CREDIT_TYPE _credit_type;
                                Promotion.PROMOTION_TYPE _promotion_type;
                                ArrayList _voucher_list;
                                String _nls_id;
                                String _elp_details;
                                Int64 _operation_id;
                                Decimal _amount_to_add;
                                Boolean _voucher_already_paid;
                                AccountPromotion _account_promotion;
                                TITO_TICKET_TYPE _ticket_type;
                                String _promotion_name;
                                Boolean _promo_as_cash_in;
                                Boolean _is_tito_mode;

                                _operation_id = 0;
                                _voucher_list = new ArrayList();
                                _elp_details = Resource.String("STR_PROMO_EXTERNAL_DETAIL") + " " + m_input_data.VoucherId;
                                _is_tito_mode = Utils.IsTitoMode();
                                _promo_as_cash_in = false;
                                _promotion_name = null;

                                // Amount to add is grater than 0
                                if (m_elp01_output_data.AmountNonRedeemableCents > 0
                                  || m_elp01_output_data.AmountRedeemableCents > 0)
                                {
                                    if (m_elp01_output_data.AmountRedeemableCents > 0)
                                    {
                                        if (_is_tito_mode)
                                        {
                                            _nls_id = "STR_ELP_ALARM_REDEEM_VOUCHER_OK";
                                        }
                                        else
                                        {
                                            _nls_id = "STR_FRM_VOUCHER_SPACE_CREDIT_ADDED_RE";
                                        }
                                        _ticket_type = TITO_TICKET_TYPE.PROMO_REDEEM;
                                        _promotion_type = Promotion.PROMOTION_TYPE.EXTERNAL_REDEEM;
                                        _amount_to_add = ((Decimal)m_elp01_output_data.AmountRedeemableCents / 100);
                                        _credit_type = ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE;
                                    }
                                    else
                                    {
                                        if (_is_tito_mode)
                                        {
                                            _nls_id = "STR_ELP_ALARM_REDEEM_VOUCHER_OK";
                                        }
                                        else
                                        {
                                            _nls_id = "STR_FRM_VOUCHER_SPACE_CREDIT_ADDED_NR";
                                        }
                                        _ticket_type = TITO_TICKET_TYPE.PROMO_NONREDEEM;
                                        _promotion_type = Promotion.PROMOTION_TYPE.EXTERNAL_NON_REDEEM;
                                        _amount_to_add = ((Decimal)m_elp01_output_data.AmountNonRedeemableCents / 100);
                                        _credit_type = ACCOUNT_PROMO_CREDIT_TYPE.NR1;

                                        if (m_elp01_output_data.RestrictedToGroupId >= 1)
                                        {
                                            // Default group promotion error message
                                            _valid_voucher = false;
                                            _str_message = Resource.String("STR_FRM_VOUCHER_SPACE_INCORRECT_GROUP_PROMOTION", m_elp01_output_data.RestrictedToGroupId);

                                            if (m_elp01_output_data.RestrictedToGroupId <= 9)
                                            {
                                                int _aux;
                                                _aux = (int)Promotion.PROMOTION_TYPE.EXTERNAL_NON_REDEEM_RESTRICTED_01;
                                                _aux += (m_elp01_output_data.RestrictedToGroupId - 1);
                                                _promotion_type = (Promotion.PROMOTION_TYPE)_aux;
                                                _str_message = Resource.String("STR_FRM_VOUCHER_SPACE_GROUP_PROMOTION_ERROR", m_elp01_output_data.RestrictedToGroupId);

                                                // Check Promotion is enabled
                                                using (DB_TRX _db_trx = new DB_TRX())
                                                {
                                                    if (Promotion.ReadPromotionData(_db_trx.SqlTransaction, _promotion_type, out _account_promotion))
                                                    {
                                                        // Group promotion disabled error message
                                                        _str_message = Resource.String("STR_FRM_VOUCHER_SPACE_GROUP_PROMOTION_NOT_ACTIVE_ERROR", m_elp01_output_data.RestrictedToGroupId);

                                                        if (_account_promotion.enabled)
                                                        {
                                                            _valid_voucher = true;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    if (_valid_voucher)
                                    {
                                        using (DB_TRX _db_trx = new DB_TRX())
                                        {
                                            // Get the voucherList.
                                            Boolean _continue = true;
                                            DataTable _dt_account_promo;

                                            if (!Accounts.ProcessRecharge(Cashier.CashierSessionInfo(), m_input_data.AccountId, _amount_to_add, true, _promotion_type,
                                                _elp_details, _db_trx.SqlTransaction, out _voucher_list, out _operation_id, out _promo_as_cash_in, out _dt_account_promo))
                                            {
                                                _continue = false;
                                                if (_dt_account_promo.Rows.Count > 0)
                                                {
                                                    _promotion_name = (String)_dt_account_promo.Rows[0][AccountPromotion.SQL_COLUMN_ACP_PROMO_NAME];
                                                }

                                                if (_is_tito_mode)
                                                {
                                                    if (_promotion_name != null)
                                                    {
                                                        _str_message = String.Format("{0} {1}", Resource.String("STR_FRM_TITO_PROMO_ERROR_PRINTER_FAILS"), _promotion_name);
                                                    }
                                                    else
                                                    {
                                                        _str_message = Resource.String("STR_FRM_TITO_PROMO_ERROR_OPERATION");
                                                    }
                                                }
                                                else
                                                {
                                                    _str_message = Resource.String("STR_FRM_VOUCHER_SPACE_CREDIT_DATABASE_ERROR");
                                                }
                                            }

                                            if (_continue)
                                            {
                                                Int64 _account_promo_id;
                                                Int64 _promo_id;

                                                _promo_id = 0;
                                                _account_promo_id = 0;
                                                if (_dt_account_promo.Rows.Count > 0)
                                                {
                                                    _promotion_name = (String)_dt_account_promo.Rows[0][AccountPromotion.SQL_COLUMN_ACP_PROMO_NAME];
                                                    _account_promo_id = (Int64)_dt_account_promo.Rows[0][AccountPromotion.SQL_COLUMN_ACP_UNIQUE_ID];
                                                    _promo_id = (Int64)_dt_account_promo.Rows[0][AccountPromotion.SQL_COLUMN_ACP_PROMO_ID];
                                                }

                                                if (Elp01Vouchers.InsertVouchers(m_account_id, m_voucher_id, _credit_type, _amount_to_add, _operation_id, _db_trx.SqlTransaction, out _voucher_already_paid))
                                                {
                                                    m_input_data = new Elp01InputData();
                                                    m_input_data.RequestType = ELP01_Requests.ELP01_REQUEST_CONFIRM_VOUCHER_REDEEM;
                                                    m_input_data.AccountId = m_account_id;
                                                    m_input_data.VoucherId = m_voucher_id;
                                                    m_input_data_str = m_input_data.ToXml();

                                                    // Insert a new Request into MULTISITE_REQUESTS table.
                                                    if (MultiSiteRequests.AddRequest(WWP_MultiSiteRequestType.MULTISITE_REQUEST_TYPE_ELP01,
                                                                                      m_input_data_str,
                                                                                      int.MaxValue,
                                                                                      0,
                                                                                      out m_unique_id_confirm_redeem,
                                                                                      _db_trx.SqlTransaction))
                                                    {

                                                        _str_message = String.Format("{0} {1:n2}", Resource.String(_nls_id), _amount_to_add);

                                                        if (_is_tito_mode)
                                                        {
                                                            CardData _card = new CardData();
                                                            if (CardData.DB_CardGetAllData(m_account_id, _card, _db_trx.SqlTransaction))
                                                            {
                                                                ParamsTicketOperation _params_ticket_operation;
                                                                Ticket _ticket;

                                                                // Print TicketOut
                                                                _params_ticket_operation = new ParamsTicketOperation();
                                                                _params_ticket_operation.out_cash_amount = _amount_to_add;
                                                                _params_ticket_operation.ticket_type = _promo_as_cash_in ? TITO_TICKET_TYPE.CASHABLE : _ticket_type; //TITO_TICKET_TYPE.PROMO_REDEEM; TITO_TICKET_TYPE.PROMO_NONREDEEM
                                                                _params_ticket_operation.in_account = _card;
                                                                _params_ticket_operation.out_promotion_id = _promo_id;
                                                                _params_ticket_operation.out_account_promo_id = _account_promo_id; // Needed for promotion cost
                                                                _params_ticket_operation.session_info = Cashier.CashierSessionInfo();
                                                                _params_ticket_operation.out_operation_id = _operation_id;

                                                                if (BusinessLogic.CreateTicket(_params_ticket_operation, true, out _ticket, _db_trx.SqlTransaction))
                                                                {
                                                                  if (TitoPrinter.PrintTicket(_ticket.ValidationNumber, _ticket.MachineTicketNumber, _ticket.TicketType, _ticket.Amount, _ticket.CreatedDateTime, _ticket.ExpirationDateTime, Cashier.CashierSessionInfo()))
                                                                    {
                                                                        _is_error = false;
                                                                    }
                                                                    else
                                                                    {
                                                                        Log.Error("timer_1_Tick: PrintTicket. Error on printing ticket.");

                                                                        PrinterStatus _status;
                                                                        String _status_text;
                                                                        TitoPrinter.GetStatus(out _status, out _status_text);
                                                                        _str_message = TitoPrinter.GetMsgError(_status);
                                                                        _str_message += "\r\n" + Resource.String("STR_FRM_CONTAINER_EVENTS_DESC_PRINTER_CANCEL_OPERATION");

                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    Log.Error("timer1_Tick: BusinessLogic.CreateTicket. Error creating ticket.");

                                                                    PrinterStatus _status;
                                                                    String _status_text;
                                                                    TitoPrinter.GetStatus(out _status, out _status_text);
                                                                    _str_message = TitoPrinter.GetMsgError(_status);
                                                                    _str_message += "\r\n" + Resource.String("STR_FRM_CONTAINER_EVENTS_DESC_PRINTER_CANCEL_OPERATION");
                                                                }
                                                            }
                                                        } // if titomode
                                                        else
                                                        {
                                                            _is_error = false;
                                                        }

                                                        if (!_is_error)
                                                        {
                                                            _db_trx.Commit();

                                                            // Print voucher.
                                                            VoucherPrint.Print(_voucher_list);
                                                        }

                                                    }
                                                    else
                                                    {
                                                        m_request_status = WWP_MSRequestStatus.MS_REQUEST_STATUS_ERROR;
                                                    }
                                                }
                                                else
                                                {
                                                    // Error in Elp01Vouchers - InsertVouchers. 
                                                    if (_is_tito_mode)
                                                    {
                                                        _str_message = String.Format("{0} {1}", Resource.String("STR_FRM_TITO_PROMO_ERROR_PRINTER_FAILS"), _promotion_name);
                                                    }
                                                    else
                                                    {
                                                        _str_message = Resource.String("STR_FRM_VOUCHER_SPACE_CREDIT_DATABASE_ERROR");
                                                    }

                                                    // Voucher is already paid
                                                    if (_voucher_already_paid)
                                                    {
                                                        _str_message = Resource.String("STR_FRM_VOUCHER_SPACE_CREDIT_ALREADY_PAID");
                                                    }
                                                }
                                            }
                                        }
                                    }
                                } //End Using  

                                EnableControls(true);

                                break;

                            case ELP01_ResponseCodes.ELP01_RESPONSE_CODE_TIMEOUT:
                                _str_message = Resource.String("STR_FRM_VOUCHER_SPACE_TIMEOUT");
                                break;

                            case ELP01_ResponseCodes.ELP01_RESPONSE_CODE_INCORRECT_VOUCHER:
                                _str_message = Resource.String("STR_FRM_VOUCHER_SPACE_NOT_FOUND");
                                break;

                            case ELP01_ResponseCodes.ELP01_RESPONSE_CODE_WS_URL_FAILED:
                                _str_message = Resource.String("STR_FRM_VOUCHER_SPACE_WS_URL_FAILED");
                                break;
                        } // switch

                        break;

                    case WWP_MSRequestStatus.MS_REQUEST_STATUS_ERROR:   // ELP01_RESPONSE_CODE_ERROR

                        if (m_database_error)
                        {
                            _str_message = Resource.String("STR_FRM_VOUCHER_SPACE_CREDIT_DATABASE_ERROR");
                        }
                        else if (m_voucher_is_already_paid)
                        {
                            _str_message = Resource.String("STR_FRM_VOUCHER_SPACE_CREDIT_ALREADY_PAID");
                        }

                        lbl_waiting.Text = string.Empty;
                        lbl_waiting.Visible = false;

                        EnableControls(true);

                        break;

                    case WWP_MSRequestStatus.MS_REQUEST_STATUS_TIMEOUT: // ELP01_RESPONSE_CODE_TIMEOUT         

                        lbl_waiting.Text = string.Empty;
                        lbl_waiting.Visible = false;

                        EnableControls(true);

                        break;

                } // switch

            }
            catch (Exception _ex)
            {
                Log.Exception(_ex);
            }
            finally
            {
                /* Error by default: STR_FRM_VOUCHER_SPACE_CREDIT_ADDED_ERROR */

                timer1.Stop();


                lbl_waiting.Visible = false;

                Log.Message("SPACE Voucher id: " + m_voucher_id + " Reddem elapsed time: " + Misc.GetElapsedTicks(m_tick_redeem).ToString() + " ms, Process ellapsed time: " + Misc.GetElapsedTicks(_process_tick).ToString() + " ms.");

                frm_message.Show(_str_message,
                                  Resource.String("STR_FRM_VOUCHER_SPACE_POPUP"),
                                  MessageBoxButtons.OK,
                                  (_is_error) ? Images.CashierImage.Error : Images.CashierImage.Information,
                                  Cashier.MainForm);

                WSIKeyboard.Hide();
                Close();

            } // try

        } // timer1_Tick

        #endregion

        #region Public Methods

        private void frm_voucher_space_Shown(object sender, EventArgs e)
        {
            if (Top > 100)
            {
                Top -= 100;
            }

            txt_voucher_name.Focus();

            // Hide keyboard.
            WSIKeyboard.Toggle();
        }

        //------------------------------------------------------------------------------
        // PURPOSE : Init and Show the form.
        //
        //  PARAMS :
        //      - INPUT :
        //          - CardData
        //
        //      - OUTPUT :
        //
        // RETURNS :
        //
        //   NOTES :
        //

        public void Show(CardData card_data, Form parentForm)
        {
          // ATB 16-FEB-2017
          Misc.WriteLog("[FORM LOAD] frm_voucher_space", Log.Type.Message);

            // Initialize values.
            Init();

            m_card_data = card_data;

            ShowDialog(parentForm);
        }

        #endregion
    }
}