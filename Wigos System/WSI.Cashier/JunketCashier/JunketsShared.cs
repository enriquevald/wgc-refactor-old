﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Junkets.cs
// 
//   DESCRIPTION: Junkets
// 
//        AUTHOR: Enric Tomas
// 
// CREATION DATE: 19-APR-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 19-APR-2017 ETP    First release (Header).
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Windows.Forms;
using WSI.Common;
using WSI.Common.Junkets;

namespace WSI.Cashier.JunketsCashier
{
  public class JunketsShared
  {

    #region " properties "
    public JunketsBusinessLogic BusinessLogic { get; set; }

    #endregion " properties "

    #region " public methods "
    
    /// <summary>
    /// Validate flyer Code introduced.
    /// </summary>
    /// <param name="AccountId"></param>
    /// <param name="FlyerCode"></param>
    /// <param name="Parent"></param>
    /// <returns></returns>
    public Boolean ValidateFlyerCode(Int64? AccountId, String FlyerCode, Form Parent)
    {
      JunketsBusinessLogic _junket;
      
      if (!JunketsBusinessLogic.IsJunketsEnabled())
      {
        return true;
      }

      _junket = new JunketsBusinessLogic()
      {
        AccountId = AccountId,
        FlyerCode = FlyerCode
      };

        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (!_junket.GetJunketData(_db_trx.SqlTransaction))
          {
            this.BusinessLogic = _junket;
            ShowErrorMessage(Parent, FlyerCode);

            return false;
          }
          this.BusinessLogic = _junket;
          ShowMessage(Parent, FlyerCode);
        }

      return true;
    } // ValidateFlyerCode


    /// <summary>
    /// Process Junket To Account
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public Boolean ProcessJunketToAccount(Int64 OperationId, IDbTransaction SqlTrx)
    {
      if (!JunketsBusinessLogic.IsJunketsEnabled())
      {
        return true;
      }

      this.BusinessLogic.OperationId = OperationId;

      if (!this.BusinessLogic.AddFlagsToAccount((SqlTransaction)SqlTrx))
     {
       return false;
     }

      if (!this.BusinessLogic.ProcessCommissionsCustomerAndEnrolled((SqlTransaction)SqlTrx))
     {
       return false;
     }

      return true;
    } // ProcessJunketToAccount

    /// <summary>
    /// Creater Voucher for the junket.
    /// </summary>
    /// <param name="OperationId"></param>
    /// <param name="Card"></param>
    /// <param name="Mode"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public VoucherJunket CreateVoucher(Int64 OperationId, CardData Card, PrintMode Mode, SqlTransaction SqlTrx)
   {
     VoucherJunket _junket_voucher;

     _junket_voucher = new VoucherJunket(Card.VoucherAccountInfo(),
                                         BusinessLogic.TitleVoucher,
                                         BusinessLogic.TextVoucher,
                                         BusinessLogic.FlyerCode,
                                         OperationId,
                                         Mode,
                                         SqlTrx);
     if (Mode == PrintMode.Print)
     {
       _junket_voucher.Save(OperationId, SqlTrx);
     }
     return _junket_voucher;
   } // CreateVoucher

    #endregion " public methods "

    #region " private methods "



    /// <summary>
    /// Show error meessage by type.
    /// </summary>
    /// <param name="Parent"></param>
    private void ShowErrorMessage(Form Parent, string FlyerCode)
    {
      String _message;

      switch (this.BusinessLogic.ErrorFlyer)
      {
        case ErrorCode.Applied:
          _message = Resource.String("STR_APP_JUNKET_MSG_WARNING_ALREDY_USED", FlyerCode);
          break;
        case ErrorCode.Expired:
          _message = Resource.String("STR_APP_JUNKET_MSG_WARNING_EXPIRED", FlyerCode);
          break;
        case ErrorCode.NotExist:
          _message = Resource.String("STR_APP_JUNKET_MSG_WARNING_NOT_EXIST", FlyerCode);
          break;
        case ErrorCode.HasAlredy:
          _message = Resource.String("STR_APP_JUNKET_MSG_WARNING_HAS_EXISTING_FLYER");
          break;
        case ErrorCode.Unknown:
        default:
          _message = Resource.String("STR_APP_GEN_ERROR_DATABASE_ERROR_READING");
          break;
      }

      frm_message.Show(_message, Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, Images.CashierImage.Warning, Parent);

    } // ShowErrorMessage

    /// <summary>
    /// Show junket message if needed.
    /// </summary>
    /// <param name="Parent"></param>
    private void ShowMessage(Form Parent, string FlyerCode)
    {
      String _message;
      _message = Resource.String("STR_APP_JUNKET_MSG_VALID_FLYER_CODE", FlyerCode);

      if (!String.IsNullOrEmpty(this.BusinessLogic.TextPopUp))
      {
        _message = _message + Environment.NewLine + this.BusinessLogic.TextPopUp;
      }

      //allways show a message
      //if (!String.IsNullOrEmpty(_message))
      //{
        frm_message.Show(_message, Resource.String("STR_APP_GEN_MSG_INFORMATION"), MessageBoxButtons.OK, Images.CashierImage.Warning, Parent);
      //}
    } // ShowMessage

    #endregion " private methods "
  }
}
