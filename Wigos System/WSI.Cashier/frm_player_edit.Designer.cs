using WSI.Common;
namespace WSI.Cashier
{
  partial class frm_player_edit
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_player_edit));
      this.timer1 = new System.Windows.Forms.Timer(this.components);
      this.label3 = new WSI.Cashier.Controls.uc_label();
      this.label4 = new WSI.Cashier.Controls.uc_label();
      this.textBox1 = new WSI.Cashier.Controls.uc_round_textbox();
      this.comboBox1 = new WSI.Cashier.Controls.uc_round_combobox();
      this.label5 = new WSI.Cashier.Controls.uc_label();
      this.comboBox2 = new WSI.Cashier.Controls.uc_round_combobox();
      this.label6 = new WSI.Cashier.Controls.uc_label();
      this.label7 = new WSI.Cashier.Controls.uc_label();
      this.label8 = new WSI.Cashier.Controls.uc_label();
      this.label9 = new WSI.Cashier.Controls.uc_label();
      this.textBox2 = new WSI.Cashier.Controls.uc_round_textbox();
      this.btn_id_card_scan = new WSI.Cashier.Controls.uc_round_button();
      this.m_img_photo = new System.Windows.Forms.PictureBox();
      this.btn_print_information = new WSI.Cashier.Controls.uc_round_button();
      this.tab_player_edit = new WSI.Cashier.Controls.uc_round_tab_control();
      this.principal = new System.Windows.Forms.TabPage();
      this.tlp_principal1 = new System.Windows.Forms.TableLayoutPanel();
      this.btn_scan_docs = new WSI.Cashier.Controls.uc_round_button();
      this.cb_marital = new WSI.Cashier.Controls.uc_round_combobox();
      this.lbl_marital_status = new WSI.Cashier.Controls.uc_label();
      this.cb_occupation = new WSI.Cashier.Controls.uc_round_auto_combobox();
      this.lbl_occupation = new WSI.Cashier.Controls.uc_label();
      this.cb_birth_country = new WSI.Cashier.Controls.uc_round_auto_combobox();
      this.lbl_birth_country = new WSI.Cashier.Controls.uc_label();
      this.cb_nationality = new WSI.Cashier.Controls.uc_round_auto_combobox();
      this.lbl_nationality = new WSI.Cashier.Controls.uc_label();
      this.cb_gender = new WSI.Cashier.Controls.uc_round_combobox();
      this.lbl_gender = new WSI.Cashier.Controls.uc_label();
      this.tlp_name = new System.Windows.Forms.TableLayoutPanel();
      this.txt_name4 = new WSI.Cashier.CharacterTextBox();
      this.lbl_name4 = new WSI.Cashier.Controls.uc_label();
      this.txt_name = new WSI.Cashier.CharacterTextBox();
      this.lbl_name = new WSI.Cashier.Controls.uc_label();
      this.lbl_last_name1 = new WSI.Cashier.Controls.uc_label();
      this.txt_last_name1 = new WSI.Cashier.CharacterTextBox();
      this.txt_last_name2 = new WSI.Cashier.CharacterTextBox();
      this.lbl_last_name2 = new WSI.Cashier.Controls.uc_label();
      this.tlp_principal = new System.Windows.Forms.TableLayoutPanel();
      this.txt_CURP = new WSI.Cashier.CharacterTextBox();
      this.lbl_wedding_date = new WSI.Cashier.Controls.uc_label();
      this.lbl_birth_date = new WSI.Cashier.Controls.uc_label();
      this.txt_RFC = new WSI.Cashier.CharacterTextBox();
      this.lbl_CURP = new WSI.Cashier.Controls.uc_label();
      this.btn_RFC = new WSI.Cashier.Controls.uc_round_button();
      this.cb_document_type = new WSI.Cashier.Controls.uc_round_combobox();
      this.lbl_document = new WSI.Cashier.Controls.uc_label();
      this.lbl_type_document = new WSI.Cashier.Controls.uc_label();
      this.txt_document = new WSI.Cashier.CharacterTextBox();
      this.uc_dt_wedding = new WSI.Cashier.uc_datetime();
      this.uc_dt_birth = new WSI.Cashier.uc_datetime();
      this.contact = new System.Windows.Forms.TabPage();
      this.tlp_contact = new System.Windows.Forms.TableLayoutPanel();
      this.lbl_twitter = new WSI.Cashier.Controls.uc_label();
      this.txt_twitter = new WSI.Cashier.Controls.uc_round_textbox();
      this.lbl_phone_01 = new WSI.Cashier.Controls.uc_label();
      this.txt_phone_01 = new WSI.Cashier.PhoneNumberTextBox();
      this.lbl_email_02 = new WSI.Cashier.Controls.uc_label();
      this.txt_email_02 = new WSI.Cashier.Controls.uc_round_textbox();
      this.lbl_phone_02 = new WSI.Cashier.Controls.uc_label();
      this.txt_phone_02 = new WSI.Cashier.PhoneNumberTextBox();
      this.txt_email_01 = new WSI.Cashier.Controls.uc_round_textbox();
      this.lbl_email_01 = new WSI.Cashier.Controls.uc_label();
      this.address = new System.Windows.Forms.TabPage();
      this.lbl_address_validation = new WSI.Cashier.Controls.uc_label();
      this.chk_address_mode = new WSI.Cashier.Controls.uc_checkBox();
      this.tlp_address_new = new System.Windows.Forms.TableLayoutPanel();
      this.cb_city = new WSI.Cashier.Controls.uc_round_auto_combobox();
      this.cb_address_03 = new WSI.Cashier.Controls.uc_round_auto_combobox();
      this.cb_address_02 = new WSI.Cashier.Controls.uc_round_auto_combobox();
      this.lbl_country_new = new WSI.Cashier.Controls.uc_label();
      this.lbl_address_01_new = new WSI.Cashier.Controls.uc_label();
      this.cb_fed_entity_new = new WSI.Cashier.Controls.uc_round_auto_combobox();
      this.txt_ext_num_new = new WSI.Cashier.CharacterTextBox();
      this.lbl_fed_entity_new = new WSI.Cashier.Controls.uc_label();
      this.txt_address_01_new = new WSI.Cashier.CharacterTextBox();
      this.txt_zip_new = new WSI.Cashier.Controls.uc_round_textbox();
      this.lbl_zip_new = new WSI.Cashier.Controls.uc_label();
      this.lbl_city_new = new WSI.Cashier.Controls.uc_label();
      this.lbl_address_03_new = new WSI.Cashier.Controls.uc_label();
      this.lbl_address_02_new = new WSI.Cashier.Controls.uc_label();
      this.lbl_ext_num_new = new WSI.Cashier.Controls.uc_label();
      this.cb_country_new = new WSI.Cashier.Controls.uc_round_auto_combobox();
      this.tlp_address = new System.Windows.Forms.TableLayoutPanel();
      this.lbl_country = new WSI.Cashier.Controls.uc_label();
      this.lbl_address_01 = new WSI.Cashier.Controls.uc_label();
      this.cb_fed_entity = new WSI.Cashier.Controls.uc_round_auto_combobox();
      this.txt_ext_num = new WSI.Cashier.CharacterTextBox();
      this.lbl_fed_entity = new WSI.Cashier.Controls.uc_label();
      this.txt_address_01 = new WSI.Cashier.CharacterTextBox();
      this.txt_zip = new WSI.Cashier.Controls.uc_round_textbox();
      this.lbl_zip = new WSI.Cashier.Controls.uc_label();
      this.lbl_city = new WSI.Cashier.Controls.uc_label();
      this.lbl_address_03 = new WSI.Cashier.Controls.uc_label();
      this.txt_city = new WSI.Cashier.CharacterTextBox();
      this.lbl_address_02 = new WSI.Cashier.Controls.uc_label();
      this.lbl_ext_num = new WSI.Cashier.Controls.uc_label();
      this.txt_address_02 = new WSI.Cashier.CharacterTextBox();
      this.txt_address_03 = new WSI.Cashier.CharacterTextBox();
      this.cb_country = new WSI.Cashier.Controls.uc_round_auto_combobox();
      this.beneficiary = new System.Windows.Forms.TabPage();
      this.chk_holder_has_beneficiary = new WSI.Cashier.Controls.uc_checkBox();
      this.tlp_ben_name = new System.Windows.Forms.TableLayoutPanel();
      this.lbl_beneficiary_name = new WSI.Cashier.Controls.uc_label();
      this.txt_beneficiary_name = new WSI.Cashier.CharacterTextBox();
      this.lbl_beneficiary_last_name1 = new WSI.Cashier.Controls.uc_label();
      this.txt_beneficiary_last_name1 = new WSI.Cashier.CharacterTextBox();
      this.lbl_beneficiary_last_name2 = new WSI.Cashier.Controls.uc_label();
      this.txt_beneficiary_last_name2 = new WSI.Cashier.CharacterTextBox();
      this.tlp_ben_principal1 = new System.Windows.Forms.TableLayoutPanel();
      this.lbl_beneficiary_gender = new WSI.Cashier.Controls.uc_label();
      this.cb_beneficiary_gender = new WSI.Cashier.Controls.uc_round_combobox();
      this.lbl_beneficiary_occupation = new WSI.Cashier.Controls.uc_label();
      this.cb_beneficiary_occupation = new WSI.Cashier.Controls.uc_round_auto_combobox();
      this.btn_ben_scan_docs = new WSI.Cashier.Controls.uc_round_button();
      this.tlp_ben_principal = new System.Windows.Forms.TableLayoutPanel();
      this.uc_dt_ben_birth = new WSI.Cashier.uc_datetime();
      this.lbl_beneficiary_RFC = new WSI.Cashier.Controls.uc_label();
      this.txt_beneficiary_RFC = new WSI.Cashier.CharacterTextBox();
      this.lbl_beneficiary_CURP = new WSI.Cashier.Controls.uc_label();
      this.txt_beneficiary_CURP = new WSI.Cashier.CharacterTextBox();
      this.lbl_beneficiary_birth_date = new WSI.Cashier.Controls.uc_label();
      this.gb_beneficiary = new WSI.Cashier.Controls.uc_round_panel();
      this.lbl_beneficiary_full_name = new WSI.Cashier.Controls.uc_label();
      this.points = new System.Windows.Forms.TabPage();
      this.lbl_points_to_maintain_prefix = new WSI.Cashier.Controls.uc_label();
      this.lbl_points_to_maintain = new WSI.Cashier.Controls.uc_label();
      this.lbl_level_prefix = new WSI.Cashier.Controls.uc_label();
      this.gb_next_level = new WSI.Cashier.Controls.uc_round_panel();
      this.lbl_next_level_name = new WSI.Cashier.Controls.uc_label();
      this.lbl_points_last_days = new WSI.Cashier.Controls.uc_label();
      this.lbl_points_discretionaries_prefix = new WSI.Cashier.Controls.uc_label();
      this.lbl_points_discretionaries = new WSI.Cashier.Controls.uc_label();
      this.lbl_points_generated_prefix = new WSI.Cashier.Controls.uc_label();
      this.lbl_points_generated = new WSI.Cashier.Controls.uc_label();
      this.lbl_points_to_next_level = new WSI.Cashier.Controls.uc_label();
      this.lbl_points_to_next_level_prefix = new WSI.Cashier.Controls.uc_label();
      this.lbl_points_level_prefix = new WSI.Cashier.Controls.uc_label();
      this.lbl_points_level = new WSI.Cashier.Controls.uc_label();
      this.lbl_points_req_prefix = new WSI.Cashier.Controls.uc_label();
      this.lbl_points_req = new WSI.Cashier.Controls.uc_label();
      this.lbl_level_entered = new WSI.Cashier.Controls.uc_label();
      this.lbl_points_sufix = new WSI.Cashier.Controls.uc_label();
      this.lbl_level_entered_prefix = new WSI.Cashier.Controls.uc_label();
      this.lbl_points = new WSI.Cashier.Controls.uc_label();
      this.lbl_level_expiration = new WSI.Cashier.Controls.uc_label();
      this.lbl_points_prefix = new WSI.Cashier.Controls.uc_label();
      this.lbl_level_expiration_prefix = new WSI.Cashier.Controls.uc_label();
      this.lbl_level = new WSI.Cashier.Controls.uc_label();
      this.block = new System.Windows.Forms.TabPage();
      this.pb_unblocked = new System.Windows.Forms.PictureBox();
      this.lbl_block_description = new WSI.Cashier.Controls.uc_label();
      this.pb_blocked = new System.Windows.Forms.PictureBox();
      this.lbl_blocked = new WSI.Cashier.Controls.uc_label();
      this.txt_block_description = new WSI.Cashier.Controls.uc_round_textbox();
      this.comments = new System.Windows.Forms.TabPage();
      this.chk_show_comments = new WSI.Cashier.Controls.uc_checkBox();
      this.txt_comments = new WSI.Cashier.Controls.uc_round_textbox();
      this.pin = new System.Windows.Forms.TabPage();
      this.lbl_pin_msg = new WSI.Cashier.Controls.uc_label();
      this.txt_confirm_pin = new WSI.Cashier.NumericTextBox();
      this.lbl_pin = new WSI.Cashier.Controls.uc_label();
      this.txt_pin = new WSI.Cashier.NumericTextBox();
      this.lbl_confirm_pin = new WSI.Cashier.Controls.uc_label();
      this.btn_generate_random_pin = new WSI.Cashier.Controls.uc_round_button();
      this.btn_save_pin = new WSI.Cashier.Controls.uc_round_button();
      this.lbl_no_photo = new System.Windows.Forms.Label();
      this.lbl_msg_blink = new WSI.Cashier.Controls.uc_label();
      this.btn_keyboard = new WSI.Cashier.Controls.uc_round_button();
      this.btn_cancel = new WSI.Cashier.Controls.uc_round_button();
      this.btn_ok = new WSI.Cashier.Controls.uc_round_button();
      this.label13 = new WSI.Cashier.Controls.uc_label();
      this.label14 = new WSI.Cashier.Controls.uc_label();
      this.label15 = new WSI.Cashier.Controls.uc_label();
      this.label16 = new WSI.Cashier.Controls.uc_label();
      this.label17 = new WSI.Cashier.Controls.uc_label();
      this.label18 = new WSI.Cashier.Controls.uc_label();
      this.label19 = new WSI.Cashier.Controls.uc_label();
      this.label20 = new WSI.Cashier.Controls.uc_label();
      this.label21 = new WSI.Cashier.Controls.uc_label();
      this.label22 = new WSI.Cashier.Controls.uc_label();
      this.label23 = new WSI.Cashier.Controls.uc_label();
      this.comboBox3 = new WSI.Cashier.Controls.uc_round_combobox();
      this.label24 = new WSI.Cashier.Controls.uc_label();
      this.label25 = new WSI.Cashier.Controls.uc_label();
      this.button1 = new WSI.Cashier.Controls.uc_round_button();
      this.panel1 = new System.Windows.Forms.Panel();
      this.label26 = new WSI.Cashier.Controls.uc_label();
      this.label27 = new WSI.Cashier.Controls.uc_label();
      this.label28 = new WSI.Cashier.Controls.uc_label();
      this.label29 = new WSI.Cashier.Controls.uc_label();
      this.label30 = new WSI.Cashier.Controls.uc_label();
      this.label31 = new WSI.Cashier.Controls.uc_label();
      this.label32 = new WSI.Cashier.Controls.uc_label();
      this.label33 = new WSI.Cashier.Controls.uc_label();
      this.label34 = new WSI.Cashier.Controls.uc_label();
      this.label35 = new WSI.Cashier.Controls.uc_label();
      this.comboBox4 = new WSI.Cashier.Controls.uc_round_combobox();
      this.label36 = new WSI.Cashier.Controls.uc_label();
      this.comboBox5 = new WSI.Cashier.Controls.uc_round_combobox();
      this.label37 = new WSI.Cashier.Controls.uc_label();
      this.label38 = new WSI.Cashier.Controls.uc_label();
      this.label39 = new WSI.Cashier.Controls.uc_label();
      this.button2 = new WSI.Cashier.Controls.uc_round_button();
      this.comboBox6 = new WSI.Cashier.Controls.uc_round_combobox();
      this.label10 = new WSI.Cashier.Controls.uc_label();
      this.button3 = new WSI.Cashier.Controls.uc_round_button();
      this.button4 = new WSI.Cashier.Controls.uc_round_button();
      this.button5 = new WSI.Cashier.Controls.uc_round_button();
      this.numericTextBox1 = new WSI.Cashier.NumericTextBox();
      this.numericTextBox2 = new WSI.Cashier.NumericTextBox();
      this.numericTextBox3 = new WSI.Cashier.NumericTextBox();
      this.characterTextBox1 = new WSI.Cashier.CharacterTextBox();
      this.characterTextBox2 = new WSI.Cashier.CharacterTextBox();
      this.numericTextBox4 = new WSI.Cashier.NumericTextBox();
      this.numericTextBox5 = new WSI.Cashier.NumericTextBox();
      this.numericTextBox6 = new WSI.Cashier.NumericTextBox();
      this.characterTextBox3 = new WSI.Cashier.CharacterTextBox();
      this.characterTextBox4 = new WSI.Cashier.CharacterTextBox();
      this.characterTextBox5 = new WSI.Cashier.CharacterTextBox();
      this.numericTextBox7 = new WSI.Cashier.NumericTextBox();
      this.numericTextBox8 = new WSI.Cashier.NumericTextBox();
      this.numericTextBox9 = new WSI.Cashier.NumericTextBox();
      this.lbl_full_name = new WSI.Cashier.Controls.uc_label();
      this.btn_reg_entry1 = new WSI.Cashier.Controls.uc_round_button();
      this.pnl_data.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.m_img_photo)).BeginInit();
      this.tab_player_edit.SuspendLayout();
      this.principal.SuspendLayout();
      this.tlp_principal1.SuspendLayout();
      this.tlp_name.SuspendLayout();
      this.tlp_principal.SuspendLayout();
      this.contact.SuspendLayout();
      this.tlp_contact.SuspendLayout();
      this.address.SuspendLayout();
      this.tlp_address_new.SuspendLayout();
      this.tlp_address.SuspendLayout();
      this.beneficiary.SuspendLayout();
      this.tlp_ben_name.SuspendLayout();
      this.tlp_ben_principal1.SuspendLayout();
      this.tlp_ben_principal.SuspendLayout();
      this.gb_beneficiary.SuspendLayout();
      this.points.SuspendLayout();
      this.gb_next_level.SuspendLayout();
      this.block.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pb_unblocked)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.pb_blocked)).BeginInit();
      this.comments.SuspendLayout();
      this.pin.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.Controls.Add(this.btn_reg_entry1);
      this.pnl_data.Controls.Add(this.btn_id_card_scan);
      this.pnl_data.Controls.Add(this.m_img_photo);
      this.pnl_data.Controls.Add(this.btn_print_information);
      this.pnl_data.Controls.Add(this.tab_player_edit);
      this.pnl_data.Controls.Add(this.lbl_msg_blink);
      this.pnl_data.Controls.Add(this.btn_keyboard);
      this.pnl_data.Controls.Add(this.btn_cancel);
      this.pnl_data.Controls.Add(this.btn_ok);
      this.pnl_data.Controls.Add(this.lbl_full_name);
      this.pnl_data.Controls.Add(this.lbl_no_photo);
      this.pnl_data.Size = new System.Drawing.Size(1024, 512);
      // 
      // timer1
      // 
      this.timer1.Interval = 4000;
      this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
      // 
      // label3
      // 
      this.label3.BackColor = System.Drawing.Color.Transparent;
      this.label3.Font = new System.Drawing.Font("Open Sans Semibold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.label3.Location = new System.Drawing.Point(365, 130);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(19, 38);
      this.label3.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.label3.TabIndex = 35;
      this.label3.Text = "/";
      this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // label4
      // 
      this.label4.BackColor = System.Drawing.Color.Transparent;
      this.label4.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.label4.Location = new System.Drawing.Point(6, 77);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(144, 38);
      this.label4.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.label4.TabIndex = 34;
      this.label4.Text = "xC�digo Elector";
      this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // textBox1
      // 
      this.textBox1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.textBox1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.textBox1.BackColor = System.Drawing.Color.White;
      this.textBox1.BorderColor = System.Drawing.Color.Empty;
      this.textBox1.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.textBox1.CornerRadius = 0;
      this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.textBox1.Location = new System.Drawing.Point(156, 76);
      this.textBox1.MaxLength = 20;
      this.textBox1.Multiline = false;
      this.textBox1.Name = "textBox1";
      this.textBox1.PasswordChar = '\0';
      this.textBox1.ReadOnly = false;
      this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.textBox1.SelectedText = "";
      this.textBox1.SelectionLength = 0;
      this.textBox1.SelectionStart = 0;
      this.textBox1.Size = new System.Drawing.Size(510, 38);
      this.textBox1.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.NONE;
      this.textBox1.TabIndex = 2;
      this.textBox1.TabStop = false;
      this.textBox1.Text = "AAAAAAAAAAAAAAAAAAAAAAAAAB";
      this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.textBox1.UseSystemPasswordChar = false;
      this.textBox1.WaterMark = null;
      this.textBox1.WaterMarkColor = System.Drawing.SystemColors.WindowText;
      // 
      // comboBox1
      // 
      this.comboBox1.ArrowColor = System.Drawing.Color.Empty;
      this.comboBox1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.comboBox1.BackColor = System.Drawing.Color.White;
      this.comboBox1.BorderColor = System.Drawing.Color.White;
      this.comboBox1.CornerRadius = 5;
      this.comboBox1.DataSource = null;
      this.comboBox1.DisplayMember = "";
      this.comboBox1.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
      this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.comboBox1.DropDownWidth = 174;
      this.comboBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.comboBox1.FormattingEnabled = true;
      this.comboBox1.IsDroppedDown = false;
      this.comboBox1.ItemHeight = 21;
      this.comboBox1.Location = new System.Drawing.Point(156, 238);
      this.comboBox1.MaxDropDownItems = 8;
      this.comboBox1.Name = "comboBox1";
      this.comboBox1.SelectedIndex = -1;
      this.comboBox1.SelectedItem = null;
      this.comboBox1.SelectedValue = null;
      this.comboBox1.SelectionLength = 0;
      this.comboBox1.SelectionStart = 0;
      this.comboBox1.Size = new System.Drawing.Size(174, 50);
      this.comboBox1.Sorted = false;
      this.comboBox1.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.NONE;
      this.comboBox1.TabIndex = 7;
      this.comboBox1.TabStop = false;
      this.comboBox1.ValueMember = "";
      // 
      // label5
      // 
      this.label5.BackColor = System.Drawing.Color.Transparent;
      this.label5.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.label5.Location = new System.Drawing.Point(6, 238);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(144, 38);
      this.label5.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.label5.TabIndex = 26;
      this.label5.Text = "xEstado Civil";
      this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // comboBox2
      // 
      this.comboBox2.ArrowColor = System.Drawing.Color.Empty;
      this.comboBox2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.comboBox2.BackColor = System.Drawing.Color.White;
      this.comboBox2.BorderColor = System.Drawing.Color.White;
      this.comboBox2.CornerRadius = 5;
      this.comboBox2.DataSource = null;
      this.comboBox2.DisplayMember = "";
      this.comboBox2.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
      this.comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.comboBox2.DropDownWidth = 149;
      this.comboBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.comboBox2.FormattingEnabled = true;
      this.comboBox2.IsDroppedDown = false;
      this.comboBox2.ItemHeight = 21;
      this.comboBox2.Location = new System.Drawing.Point(156, 184);
      this.comboBox2.MaxDropDownItems = 8;
      this.comboBox2.Name = "comboBox2";
      this.comboBox2.SelectedIndex = -1;
      this.comboBox2.SelectedItem = null;
      this.comboBox2.SelectedValue = null;
      this.comboBox2.SelectionLength = 0;
      this.comboBox2.SelectionStart = 0;
      this.comboBox2.Size = new System.Drawing.Size(149, 50);
      this.comboBox2.Sorted = false;
      this.comboBox2.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.NONE;
      this.comboBox2.TabIndex = 6;
      this.comboBox2.TabStop = false;
      this.comboBox2.ValueMember = "";
      // 
      // label6
      // 
      this.label6.BackColor = System.Drawing.Color.Transparent;
      this.label6.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.label6.Location = new System.Drawing.Point(6, 184);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(144, 38);
      this.label6.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.label6.TabIndex = 24;
      this.label6.Text = "xGenero";
      this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // label7
      // 
      this.label7.BackColor = System.Drawing.Color.Transparent;
      this.label7.Font = new System.Drawing.Font("Open Sans Semibold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.label7.Location = new System.Drawing.Point(248, 130);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(19, 38);
      this.label7.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.label7.TabIndex = 31;
      this.label7.Text = "/";
      this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // label8
      // 
      this.label8.BackColor = System.Drawing.Color.Transparent;
      this.label8.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.label8.Location = new System.Drawing.Point(6, 131);
      this.label8.Name = "label8";
      this.label8.Size = new System.Drawing.Size(144, 38);
      this.label8.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.label8.TabIndex = 25;
      this.label8.Text = "xNacimiento";
      this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // label9
      // 
      this.label9.BackColor = System.Drawing.Color.Transparent;
      this.label9.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.label9.Location = new System.Drawing.Point(6, 23);
      this.label9.Name = "label9";
      this.label9.Size = new System.Drawing.Size(144, 38);
      this.label9.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.label9.TabIndex = 1;
      this.label9.Text = "xName";
      this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // textBox2
      // 
      this.textBox2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.textBox2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.textBox2.BackColor = System.Drawing.Color.White;
      this.textBox2.BorderColor = System.Drawing.Color.Empty;
      this.textBox2.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.textBox2.CornerRadius = 0;
      this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.textBox2.Location = new System.Drawing.Point(156, 22);
      this.textBox2.MaxLength = 50;
      this.textBox2.Multiline = false;
      this.textBox2.Name = "textBox2";
      this.textBox2.PasswordChar = '\0';
      this.textBox2.ReadOnly = false;
      this.textBox2.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.textBox2.SelectedText = "";
      this.textBox2.SelectionLength = 0;
      this.textBox2.SelectionStart = 0;
      this.textBox2.Size = new System.Drawing.Size(510, 38);
      this.textBox2.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.NONE;
      this.textBox2.TabIndex = 1;
      this.textBox2.TabStop = false;
      this.textBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.textBox2.UseSystemPasswordChar = false;
      this.textBox2.WaterMark = null;
      this.textBox2.WaterMarkColor = System.Drawing.SystemColors.WindowText;
      // 
      // btn_id_card_scan
      // 
      this.btn_id_card_scan.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_id_card_scan.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(47)))), ((int)(((byte)(50)))));
      this.btn_id_card_scan.FlatAppearance.BorderSize = 0;
      this.btn_id_card_scan.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_id_card_scan.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_id_card_scan.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_id_card_scan.Image = null;
      this.btn_id_card_scan.IsSelected = false;
      this.btn_id_card_scan.Location = new System.Drawing.Point(411, 443);
      this.btn_id_card_scan.Name = "btn_id_card_scan";
      this.btn_id_card_scan.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_id_card_scan.Size = new System.Drawing.Size(117, 60);
      this.btn_id_card_scan.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.TERTIARY;
      this.btn_id_card_scan.TabIndex = 18;
      this.btn_id_card_scan.Text = "ID CARD SCAN";
      this.btn_id_card_scan.UseVisualStyleBackColor = false;
      this.btn_id_card_scan.Click += new System.EventHandler(this.btn_id_card_scan_Click);
      // 
      // m_img_photo
      // 
      this.m_img_photo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.m_img_photo.Location = new System.Drawing.Point(3, 5);
      this.m_img_photo.Name = "m_img_photo";
      this.m_img_photo.Size = new System.Drawing.Size(95, 100);
      this.m_img_photo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
      this.m_img_photo.TabIndex = 17;
      this.m_img_photo.TabStop = false;
      this.m_img_photo.Click += new System.EventHandler(this.m_img_photo_Click);
      // 
      // btn_print_information
      // 
      this.btn_print_information.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_print_information.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(47)))), ((int)(((byte)(50)))));
      this.btn_print_information.FlatAppearance.BorderSize = 0;
      this.btn_print_information.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_print_information.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_print_information.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_print_information.Image = null;
      this.btn_print_information.IsSelected = false;
      this.btn_print_information.Location = new System.Drawing.Point(534, 443);
      this.btn_print_information.Name = "btn_print_information";
      this.btn_print_information.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_print_information.Size = new System.Drawing.Size(155, 60);
      this.btn_print_information.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.TERTIARY;
      this.btn_print_information.TabIndex = 2;
      this.btn_print_information.Text = "XPRINT INFORMATION";
      this.btn_print_information.UseVisualStyleBackColor = false;
      // 
      // tab_player_edit
      // 
      this.tab_player_edit.Controls.Add(this.principal);
      this.tab_player_edit.Controls.Add(this.contact);
      this.tab_player_edit.Controls.Add(this.address);
      this.tab_player_edit.Controls.Add(this.beneficiary);
      this.tab_player_edit.Controls.Add(this.points);
      this.tab_player_edit.Controls.Add(this.block);
      this.tab_player_edit.Controls.Add(this.comments);
      this.tab_player_edit.Controls.Add(this.pin);
      this.tab_player_edit.DisplayStyle = WSI.Cashier.Controls.TabStyle.Rounded;
      // 
      // 
      // 
      this.tab_player_edit.DisplayStyleProvider.BorderColor = System.Drawing.SystemColors.ControlDark;
      this.tab_player_edit.DisplayStyleProvider.BorderColorHot = System.Drawing.SystemColors.ControlDark;
      this.tab_player_edit.DisplayStyleProvider.BorderColorSelected = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(157)))), ((int)(((byte)(185)))));
      this.tab_player_edit.DisplayStyleProvider.FocusTrack = true;
      this.tab_player_edit.DisplayStyleProvider.HotTrack = true;
      this.tab_player_edit.DisplayStyleProvider.Opacity = 1F;
      this.tab_player_edit.DisplayStyleProvider.Overlap = 0;
      this.tab_player_edit.DisplayStyleProvider.Padding = new System.Drawing.Point(6, 3);
      this.tab_player_edit.DisplayStyleProvider.Radius = 10;
      this.tab_player_edit.DisplayStyleProvider.TabColor = System.Drawing.SystemColors.ControlDark;
      this.tab_player_edit.DisplayStyleProvider.TabColorDisabled = System.Drawing.SystemColors.ControlDark;
      this.tab_player_edit.DisplayStyleProvider.TabColorHeader = System.Drawing.SystemColors.ControlDark;
      this.tab_player_edit.DisplayStyleProvider.TabColorSelected = System.Drawing.SystemColors.ControlDark;
      this.tab_player_edit.DisplayStyleProvider.TextColor = System.Drawing.SystemColors.ControlText;
      this.tab_player_edit.DisplayStyleProvider.TextColorDisabled = System.Drawing.SystemColors.ControlDark;
      this.tab_player_edit.DisplayStyleProvider.TextColorSelected = System.Drawing.SystemColors.ControlText;
      this.tab_player_edit.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.tab_player_edit.HotTrack = true;
      this.tab_player_edit.ItemSize = new System.Drawing.Size(100, 40);
      this.tab_player_edit.Location = new System.Drawing.Point(100, 5);
      this.tab_player_edit.Name = "tab_player_edit";
      this.tab_player_edit.SelectedIndex = 0;
      this.tab_player_edit.Size = new System.Drawing.Size(914, 402);
      this.tab_player_edit.TabIndex = 0;
      this.tab_player_edit.TabStop = false;
      // 
      // principal
      // 
      this.principal.Controls.Add(this.tlp_principal1);
      this.principal.Controls.Add(this.tlp_name);
      this.principal.Controls.Add(this.tlp_principal);
      this.principal.Location = new System.Drawing.Point(4, 45);
      this.principal.Name = "principal";
      this.principal.Padding = new System.Windows.Forms.Padding(3);
      this.principal.Size = new System.Drawing.Size(906, 353);
      this.principal.TabIndex = 0;
      this.principal.Text = "xPrincipal";
      this.principal.UseVisualStyleBackColor = true;
      // 
      // tlp_principal1
      // 
      this.tlp_principal1.ColumnCount = 2;
      this.tlp_principal1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
      this.tlp_principal1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 300F));
      this.tlp_principal1.Controls.Add(this.btn_scan_docs, 1, 0);
      this.tlp_principal1.Controls.Add(this.cb_marital, 1, 5);
      this.tlp_principal1.Controls.Add(this.lbl_marital_status, 0, 5);
      this.tlp_principal1.Controls.Add(this.cb_occupation, 1, 4);
      this.tlp_principal1.Controls.Add(this.lbl_occupation, 0, 4);
      this.tlp_principal1.Controls.Add(this.cb_birth_country, 1, 3);
      this.tlp_principal1.Controls.Add(this.lbl_birth_country, 0, 3);
      this.tlp_principal1.Controls.Add(this.cb_nationality, 1, 2);
      this.tlp_principal1.Controls.Add(this.lbl_nationality, 0, 2);
      this.tlp_principal1.Controls.Add(this.cb_gender, 1, 1);
      this.tlp_principal1.Controls.Add(this.lbl_gender, 0, 1);
      this.tlp_principal1.Location = new System.Drawing.Point(479, 69);
      this.tlp_principal1.Name = "tlp_principal1";
      this.tlp_principal1.RowCount = 7;
      this.tlp_principal1.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_principal1.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_principal1.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_principal1.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_principal1.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_principal1.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_principal1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
      this.tlp_principal1.Size = new System.Drawing.Size(421, 292);
      this.tlp_principal1.TabIndex = 2;
      // 
      // btn_scan_docs
      // 
      this.btn_scan_docs.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_scan_docs.FlatAppearance.BorderSize = 0;
      this.btn_scan_docs.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_scan_docs.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_scan_docs.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_scan_docs.Image = null;
      this.btn_scan_docs.IsSelected = false;
      this.btn_scan_docs.Location = new System.Drawing.Point(153, 3);
      this.btn_scan_docs.Name = "btn_scan_docs";
      this.btn_scan_docs.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_scan_docs.Size = new System.Drawing.Size(258, 40);
      this.btn_scan_docs.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_scan_docs.TabIndex = 0;
      this.btn_scan_docs.Text = "XDOCUMENT DIGITIZING";
      this.btn_scan_docs.UseVisualStyleBackColor = false;
      // 
      // cb_marital
      // 
      this.cb_marital.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_marital.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.cb_marital.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.cb_marital.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.cb_marital.CornerRadius = 5;
      this.cb_marital.DataSource = null;
      this.cb_marital.DisplayMember = "";
      this.cb_marital.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
      this.cb_marital.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cb_marital.DropDownWidth = 258;
      this.cb_marital.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.cb_marital.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_marital.FormattingEnabled = true;
      this.cb_marital.IsDroppedDown = false;
      this.cb_marital.ItemHeight = 40;
      this.cb_marital.Location = new System.Drawing.Point(153, 233);
      this.cb_marital.MaxDropDownItems = 4;
      this.cb_marital.Name = "cb_marital";
      this.cb_marital.SelectedIndex = -1;
      this.cb_marital.SelectedItem = null;
      this.cb_marital.SelectedValue = null;
      this.cb_marital.SelectionLength = 0;
      this.cb_marital.SelectionStart = 0;
      this.cb_marital.Size = new System.Drawing.Size(258, 40);
      this.cb_marital.Sorted = false;
      this.cb_marital.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.BASIC;
      this.cb_marital.TabIndex = 5;
      this.cb_marital.TabStop = false;
      this.cb_marital.ValueMember = "";
      this.cb_marital.Leave += new System.EventHandler(this.control_Leave);
      // 
      // lbl_marital_status
      // 
      this.lbl_marital_status.BackColor = System.Drawing.Color.Transparent;
      this.lbl_marital_status.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_marital_status.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_marital_status.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_marital_status.Location = new System.Drawing.Point(3, 230);
      this.lbl_marital_status.Name = "lbl_marital_status";
      this.lbl_marital_status.Size = new System.Drawing.Size(144, 46);
      this.lbl_marital_status.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_marital_status.TabIndex = 1001;
      this.lbl_marital_status.Text = "xMarital Status";
      this.lbl_marital_status.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // cb_occupation
      // 
      this.cb_occupation.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_occupation.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.cb_occupation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.cb_occupation.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.cb_occupation.CornerRadius = 5;
      this.cb_occupation.DataSource = null;
      this.cb_occupation.DisplayMember = "";
      this.cb_occupation.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
      this.cb_occupation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
      this.cb_occupation.DropDownWidth = 258;
      this.cb_occupation.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.cb_occupation.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_occupation.FormattingEnabled = true;
      this.cb_occupation.FormatValidator = null;
      this.cb_occupation.IsDroppedDown = false;
      this.cb_occupation.ItemHeight = 40;
      this.cb_occupation.Location = new System.Drawing.Point(153, 187);
      this.cb_occupation.MaxDropDownItems = 4;
      this.cb_occupation.Name = "cb_occupation";
      this.cb_occupation.SelectedIndex = -1;
      this.cb_occupation.SelectedItem = null;
      this.cb_occupation.SelectedValue = null;
      this.cb_occupation.SelectionLength = 0;
      this.cb_occupation.SelectionStart = 0;
      this.cb_occupation.Size = new System.Drawing.Size(258, 40);
      this.cb_occupation.Sorted = false;
      this.cb_occupation.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.BASIC;
      this.cb_occupation.TabIndex = 4;
      this.cb_occupation.TabStop = false;
      this.cb_occupation.ValueMember = "";
      // 
      // lbl_occupation
      // 
      this.lbl_occupation.BackColor = System.Drawing.Color.Transparent;
      this.lbl_occupation.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_occupation.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_occupation.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_occupation.Location = new System.Drawing.Point(3, 184);
      this.lbl_occupation.Name = "lbl_occupation";
      this.lbl_occupation.Size = new System.Drawing.Size(144, 46);
      this.lbl_occupation.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_occupation.TabIndex = 1001;
      this.lbl_occupation.Text = "xOccupation";
      this.lbl_occupation.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // cb_birth_country
      // 
      this.cb_birth_country.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_birth_country.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.cb_birth_country.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.cb_birth_country.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.cb_birth_country.CornerRadius = 5;
      this.cb_birth_country.DataSource = null;
      this.cb_birth_country.DisplayMember = "";
      this.cb_birth_country.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
      this.cb_birth_country.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
      this.cb_birth_country.DropDownWidth = 258;
      this.cb_birth_country.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.cb_birth_country.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_birth_country.FormattingEnabled = true;
      this.cb_birth_country.FormatValidator = null;
      this.cb_birth_country.IsDroppedDown = false;
      this.cb_birth_country.ItemHeight = 40;
      this.cb_birth_country.Location = new System.Drawing.Point(153, 141);
      this.cb_birth_country.MaxDropDownItems = 4;
      this.cb_birth_country.Name = "cb_birth_country";
      this.cb_birth_country.SelectedIndex = -1;
      this.cb_birth_country.SelectedItem = null;
      this.cb_birth_country.SelectedValue = null;
      this.cb_birth_country.SelectionLength = 0;
      this.cb_birth_country.SelectionStart = 0;
      this.cb_birth_country.Size = new System.Drawing.Size(258, 40);
      this.cb_birth_country.Sorted = false;
      this.cb_birth_country.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.BASIC;
      this.cb_birth_country.TabIndex = 3;
      this.cb_birth_country.TabStop = false;
      this.cb_birth_country.ValueMember = "";
      // 
      // lbl_birth_country
      // 
      this.lbl_birth_country.BackColor = System.Drawing.Color.Transparent;
      this.lbl_birth_country.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_birth_country.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_birth_country.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_birth_country.Location = new System.Drawing.Point(3, 138);
      this.lbl_birth_country.Name = "lbl_birth_country";
      this.lbl_birth_country.Size = new System.Drawing.Size(144, 46);
      this.lbl_birth_country.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_birth_country.TabIndex = 1001;
      this.lbl_birth_country.Text = "xBirth Country";
      this.lbl_birth_country.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // cb_nationality
      // 
      this.cb_nationality.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_nationality.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.cb_nationality.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.cb_nationality.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.cb_nationality.CornerRadius = 5;
      this.cb_nationality.DataSource = null;
      this.cb_nationality.DisplayMember = "";
      this.cb_nationality.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
      this.cb_nationality.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
      this.cb_nationality.DropDownWidth = 258;
      this.cb_nationality.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.cb_nationality.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_nationality.FormattingEnabled = true;
      this.cb_nationality.FormatValidator = null;
      this.cb_nationality.IsDroppedDown = false;
      this.cb_nationality.ItemHeight = 40;
      this.cb_nationality.Location = new System.Drawing.Point(153, 95);
      this.cb_nationality.MaxDropDownItems = 4;
      this.cb_nationality.Name = "cb_nationality";
      this.cb_nationality.SelectedIndex = -1;
      this.cb_nationality.SelectedItem = null;
      this.cb_nationality.SelectedValue = null;
      this.cb_nationality.SelectionLength = 0;
      this.cb_nationality.SelectionStart = 0;
      this.cb_nationality.Size = new System.Drawing.Size(258, 40);
      this.cb_nationality.Sorted = false;
      this.cb_nationality.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.BASIC;
      this.cb_nationality.TabIndex = 2;
      this.cb_nationality.TabStop = false;
      this.cb_nationality.ValueMember = "";
      // 
      // lbl_nationality
      // 
      this.lbl_nationality.BackColor = System.Drawing.Color.Transparent;
      this.lbl_nationality.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_nationality.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_nationality.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_nationality.Location = new System.Drawing.Point(3, 92);
      this.lbl_nationality.Name = "lbl_nationality";
      this.lbl_nationality.Size = new System.Drawing.Size(144, 46);
      this.lbl_nationality.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_nationality.TabIndex = 1001;
      this.lbl_nationality.Text = "xNationality";
      this.lbl_nationality.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // cb_gender
      // 
      this.cb_gender.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_gender.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.cb_gender.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.cb_gender.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.cb_gender.CornerRadius = 5;
      this.cb_gender.DataSource = null;
      this.cb_gender.DisplayMember = "";
      this.cb_gender.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
      this.cb_gender.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cb_gender.DropDownWidth = 258;
      this.cb_gender.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.cb_gender.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_gender.FormattingEnabled = true;
      this.cb_gender.IsDroppedDown = false;
      this.cb_gender.ItemHeight = 40;
      this.cb_gender.Location = new System.Drawing.Point(153, 49);
      this.cb_gender.MaxDropDownItems = 4;
      this.cb_gender.Name = "cb_gender";
      this.cb_gender.SelectedIndex = -1;
      this.cb_gender.SelectedItem = null;
      this.cb_gender.SelectedValue = null;
      this.cb_gender.SelectionLength = 0;
      this.cb_gender.SelectionStart = 0;
      this.cb_gender.Size = new System.Drawing.Size(258, 40);
      this.cb_gender.Sorted = false;
      this.cb_gender.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.BASIC;
      this.cb_gender.TabIndex = 1;
      this.cb_gender.TabStop = false;
      this.cb_gender.ValueMember = "";
      // 
      // lbl_gender
      // 
      this.lbl_gender.BackColor = System.Drawing.Color.Transparent;
      this.lbl_gender.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_gender.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_gender.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_gender.Location = new System.Drawing.Point(3, 46);
      this.lbl_gender.Name = "lbl_gender";
      this.lbl_gender.Size = new System.Drawing.Size(144, 46);
      this.lbl_gender.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_gender.TabIndex = 1001;
      this.lbl_gender.Text = "xGender";
      this.lbl_gender.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // tlp_name
      // 
      this.tlp_name.ColumnCount = 4;
      this.tlp_name.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
      this.tlp_name.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
      this.tlp_name.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
      this.tlp_name.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
      this.tlp_name.Controls.Add(this.txt_name4, 1, 1);
      this.tlp_name.Controls.Add(this.lbl_name4, 1, 0);
      this.tlp_name.Controls.Add(this.txt_name, 0, 1);
      this.tlp_name.Controls.Add(this.lbl_name, 0, 0);
      this.tlp_name.Controls.Add(this.lbl_last_name1, 2, 0);
      this.tlp_name.Controls.Add(this.txt_last_name1, 2, 1);
      this.tlp_name.Controls.Add(this.txt_last_name2, 3, 1);
      this.tlp_name.Controls.Add(this.lbl_last_name2, 3, 0);
      this.tlp_name.Location = new System.Drawing.Point(6, 4);
      this.tlp_name.Name = "tlp_name";
      this.tlp_name.RowCount = 2;
      this.tlp_name.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_name.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_name.Size = new System.Drawing.Size(887, 65);
      this.tlp_name.TabIndex = 0;
      // 
      // txt_name4
      // 
      this.txt_name4.AllowSpace = true;
      this.txt_name4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
      this.txt_name4.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_name4.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_name4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_name4.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_name4.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.txt_name4.CornerRadius = 5;
      this.txt_name4.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_name4.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.BaseMode;
      this.txt_name4.Location = new System.Drawing.Point(224, 22);
      this.txt_name4.MaxLength = 50;
      this.txt_name4.Multiline = false;
      this.txt_name4.Name = "txt_name4";
      this.txt_name4.PasswordChar = '\0';
      this.txt_name4.ReadOnly = false;
      this.txt_name4.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_name4.SelectedText = "";
      this.txt_name4.SelectionLength = 0;
      this.txt_name4.SelectionStart = 0;
      this.txt_name4.Size = new System.Drawing.Size(215, 40);
      this.txt_name4.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_name4.TabIndex = 1;
      this.txt_name4.TabStop = false;
      this.txt_name4.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_name4.UseSystemPasswordChar = false;
      this.txt_name4.WaterMark = null;
      this.txt_name4.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      this.txt_name4.TextChanged += new System.EventHandler(this.txt_name_or_last_names_TextChanged);
      // 
      // lbl_name4
      // 
      this.lbl_name4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_name4.BackColor = System.Drawing.Color.Transparent;
      this.lbl_name4.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_name4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_name4.Location = new System.Drawing.Point(224, 0);
      this.lbl_name4.Name = "lbl_name4";
      this.lbl_name4.Size = new System.Drawing.Size(215, 18);
      this.lbl_name4.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_name4.TabIndex = 1002;
      this.lbl_name4.Text = "xHolderName4";
      this.lbl_name4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // txt_name
      // 
      this.txt_name.AllowSpace = true;
      this.txt_name.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
      this.txt_name.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_name.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_name.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_name.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_name.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.txt_name.CornerRadius = 5;
      this.txt_name.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_name.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.BaseMode;
      this.txt_name.Location = new System.Drawing.Point(3, 22);
      this.txt_name.MaxLength = 50;
      this.txt_name.Multiline = false;
      this.txt_name.Name = "txt_name";
      this.txt_name.PasswordChar = '\0';
      this.txt_name.ReadOnly = false;
      this.txt_name.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_name.SelectedText = "";
      this.txt_name.SelectionLength = 0;
      this.txt_name.SelectionStart = 0;
      this.txt_name.Size = new System.Drawing.Size(215, 40);
      this.txt_name.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_name.TabIndex = 0;
      this.txt_name.TabStop = false;
      this.txt_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_name.UseSystemPasswordChar = false;
      this.txt_name.WaterMark = null;
      this.txt_name.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      this.txt_name.TextChanged += new System.EventHandler(this.txt_name_or_last_names_TextChanged);
      // 
      // lbl_name
      // 
      this.lbl_name.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_name.BackColor = System.Drawing.Color.Transparent;
      this.lbl_name.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_name.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_name.Location = new System.Drawing.Point(3, 0);
      this.lbl_name.Name = "lbl_name";
      this.lbl_name.Size = new System.Drawing.Size(215, 18);
      this.lbl_name.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_name.TabIndex = 1001;
      this.lbl_name.Text = "xHolderName3";
      this.lbl_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_last_name1
      // 
      this.lbl_last_name1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_last_name1.BackColor = System.Drawing.Color.Transparent;
      this.lbl_last_name1.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_last_name1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_last_name1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.lbl_last_name1.Location = new System.Drawing.Point(445, 0);
      this.lbl_last_name1.Name = "lbl_last_name1";
      this.lbl_last_name1.Size = new System.Drawing.Size(215, 19);
      this.lbl_last_name1.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_last_name1.TabIndex = 1001;
      this.lbl_last_name1.Text = "xHolderName1";
      this.lbl_last_name1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // txt_last_name1
      // 
      this.txt_last_name1.AllowSpace = true;
      this.txt_last_name1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
      this.txt_last_name1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_last_name1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_last_name1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_last_name1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_last_name1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.txt_last_name1.CornerRadius = 5;
      this.txt_last_name1.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_last_name1.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.BaseMode;
      this.txt_last_name1.Location = new System.Drawing.Point(445, 22);
      this.txt_last_name1.MaxLength = 50;
      this.txt_last_name1.Multiline = false;
      this.txt_last_name1.Name = "txt_last_name1";
      this.txt_last_name1.PasswordChar = '\0';
      this.txt_last_name1.ReadOnly = false;
      this.txt_last_name1.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_last_name1.SelectedText = "";
      this.txt_last_name1.SelectionLength = 0;
      this.txt_last_name1.SelectionStart = 0;
      this.txt_last_name1.Size = new System.Drawing.Size(215, 40);
      this.txt_last_name1.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_last_name1.TabIndex = 2;
      this.txt_last_name1.TabStop = false;
      this.txt_last_name1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_last_name1.UseSystemPasswordChar = false;
      this.txt_last_name1.WaterMark = null;
      this.txt_last_name1.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      this.txt_last_name1.TextChanged += new System.EventHandler(this.txt_name_or_last_names_TextChanged);
      // 
      // txt_last_name2
      // 
      this.txt_last_name2.AllowSpace = true;
      this.txt_last_name2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
      this.txt_last_name2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_last_name2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_last_name2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_last_name2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_last_name2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.txt_last_name2.CornerRadius = 5;
      this.txt_last_name2.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_last_name2.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.BaseMode;
      this.txt_last_name2.Location = new System.Drawing.Point(666, 22);
      this.txt_last_name2.MaxLength = 50;
      this.txt_last_name2.Multiline = false;
      this.txt_last_name2.Name = "txt_last_name2";
      this.txt_last_name2.PasswordChar = '\0';
      this.txt_last_name2.ReadOnly = false;
      this.txt_last_name2.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_last_name2.SelectedText = "";
      this.txt_last_name2.SelectionLength = 0;
      this.txt_last_name2.SelectionStart = 0;
      this.txt_last_name2.Size = new System.Drawing.Size(218, 40);
      this.txt_last_name2.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_last_name2.TabIndex = 3;
      this.txt_last_name2.TabStop = false;
      this.txt_last_name2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_last_name2.UseSystemPasswordChar = false;
      this.txt_last_name2.WaterMark = null;
      this.txt_last_name2.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      this.txt_last_name2.TextChanged += new System.EventHandler(this.txt_name_or_last_names_TextChanged);
      // 
      // lbl_last_name2
      // 
      this.lbl_last_name2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_last_name2.BackColor = System.Drawing.Color.Transparent;
      this.lbl_last_name2.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_last_name2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_last_name2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.lbl_last_name2.Location = new System.Drawing.Point(666, 0);
      this.lbl_last_name2.Name = "lbl_last_name2";
      this.lbl_last_name2.Size = new System.Drawing.Size(218, 19);
      this.lbl_last_name2.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_last_name2.TabIndex = 1001;
      this.lbl_last_name2.Text = "xHolderName2";
      this.lbl_last_name2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // tlp_principal
      // 
      this.tlp_principal.ColumnCount = 2;
      this.tlp_principal.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 149F));
      this.tlp_principal.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 351F));
      this.tlp_principal.Controls.Add(this.txt_CURP, 1, 3);
      this.tlp_principal.Controls.Add(this.lbl_wedding_date, 0, 5);
      this.tlp_principal.Controls.Add(this.lbl_birth_date, 0, 4);
      this.tlp_principal.Controls.Add(this.txt_RFC, 1, 2);
      this.tlp_principal.Controls.Add(this.lbl_CURP, 0, 3);
      this.tlp_principal.Controls.Add(this.btn_RFC, 0, 2);
      this.tlp_principal.Controls.Add(this.cb_document_type, 1, 0);
      this.tlp_principal.Controls.Add(this.lbl_document, 0, 1);
      this.tlp_principal.Controls.Add(this.lbl_type_document, 0, 0);
      this.tlp_principal.Controls.Add(this.txt_document, 1, 1);
      this.tlp_principal.Controls.Add(this.uc_dt_wedding, 1, 5);
      this.tlp_principal.Controls.Add(this.uc_dt_birth, 1, 4);
      this.tlp_principal.Location = new System.Drawing.Point(3, 70);
      this.tlp_principal.Name = "tlp_principal";
      this.tlp_principal.RowCount = 6;
      this.tlp_principal.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_principal.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_principal.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_principal.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_principal.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_principal.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_principal.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_principal.Size = new System.Drawing.Size(491, 294);
      this.tlp_principal.TabIndex = 1;
      // 
      // txt_CURP
      // 
      this.txt_CURP.AllowSpace = true;
      this.txt_CURP.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_CURP.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_CURP.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_CURP.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_CURP.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.txt_CURP.CornerRadius = 5;
      this.txt_CURP.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_CURP.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.ExtendedMode;
      this.txt_CURP.Location = new System.Drawing.Point(152, 141);
      this.txt_CURP.MaxLength = 20;
      this.txt_CURP.Multiline = false;
      this.txt_CURP.Name = "txt_CURP";
      this.txt_CURP.PasswordChar = '\0';
      this.txt_CURP.ReadOnly = false;
      this.txt_CURP.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_CURP.SelectedText = "";
      this.txt_CURP.SelectionLength = 0;
      this.txt_CURP.SelectionStart = 0;
      this.txt_CURP.Size = new System.Drawing.Size(258, 40);
      this.txt_CURP.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_CURP.TabIndex = 6;
      this.txt_CURP.TabStop = false;
      this.txt_CURP.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_CURP.UseSystemPasswordChar = false;
      this.txt_CURP.WaterMark = null;
      this.txt_CURP.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // lbl_wedding_date
      // 
      this.lbl_wedding_date.BackColor = System.Drawing.Color.Transparent;
      this.lbl_wedding_date.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_wedding_date.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_wedding_date.Location = new System.Drawing.Point(3, 233);
      this.lbl_wedding_date.Name = "lbl_wedding_date";
      this.lbl_wedding_date.Size = new System.Drawing.Size(143, 46);
      this.lbl_wedding_date.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_wedding_date.TabIndex = 1001;
      this.lbl_wedding_date.Text = "xWedding Date";
      this.lbl_wedding_date.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_birth_date
      // 
      this.lbl_birth_date.BackColor = System.Drawing.Color.Transparent;
      this.lbl_birth_date.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_birth_date.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_birth_date.Location = new System.Drawing.Point(3, 184);
      this.lbl_birth_date.Name = "lbl_birth_date";
      this.lbl_birth_date.Size = new System.Drawing.Size(143, 46);
      this.lbl_birth_date.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_birth_date.TabIndex = 1001;
      this.lbl_birth_date.Text = "xBirthDay";
      this.lbl_birth_date.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // txt_RFC
      // 
      this.txt_RFC.AllowSpace = true;
      this.txt_RFC.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_RFC.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_RFC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_RFC.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_RFC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.txt_RFC.CornerRadius = 5;
      this.txt_RFC.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_RFC.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.ExtendedMode;
      this.txt_RFC.Location = new System.Drawing.Point(152, 95);
      this.txt_RFC.MaxLength = 13;
      this.txt_RFC.Multiline = false;
      this.txt_RFC.Name = "txt_RFC";
      this.txt_RFC.PasswordChar = '\0';
      this.txt_RFC.ReadOnly = false;
      this.txt_RFC.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_RFC.SelectedText = "";
      this.txt_RFC.SelectionLength = 0;
      this.txt_RFC.SelectionStart = 0;
      this.txt_RFC.Size = new System.Drawing.Size(258, 40);
      this.txt_RFC.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_RFC.TabIndex = 5;
      this.txt_RFC.TabStop = false;
      this.txt_RFC.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_RFC.UseSystemPasswordChar = false;
      this.txt_RFC.WaterMark = null;
      this.txt_RFC.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      this.txt_RFC.TextChanged += new System.EventHandler(this.txt_RFC_TextChanged);
      // 
      // lbl_CURP
      // 
      this.lbl_CURP.BackColor = System.Drawing.Color.Transparent;
      this.lbl_CURP.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_CURP.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_CURP.Location = new System.Drawing.Point(3, 138);
      this.lbl_CURP.Name = "lbl_CURP";
      this.lbl_CURP.Size = new System.Drawing.Size(143, 46);
      this.lbl_CURP.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_CURP.TabIndex = 1004;
      this.lbl_CURP.Text = "xCURP";
      this.lbl_CURP.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // btn_RFC
      // 
      this.btn_RFC.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_RFC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(47)))), ((int)(((byte)(50)))));
      this.btn_RFC.FlatAppearance.BorderSize = 0;
      this.btn_RFC.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_RFC.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_RFC.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_RFC.Image = null;
      this.btn_RFC.IsSelected = false;
      this.btn_RFC.Location = new System.Drawing.Point(46, 95);
      this.btn_RFC.Name = "btn_RFC";
      this.btn_RFC.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_RFC.Size = new System.Drawing.Size(100, 40);
      this.btn_RFC.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.TERTIARY;
      this.btn_RFC.TabIndex = 5;
      this.btn_RFC.Text = "XRFC";
      this.btn_RFC.UseVisualStyleBackColor = false;
      this.btn_RFC.Click += new System.EventHandler(this.btn_RFC_Click);
      // 
      // cb_document_type
      // 
      this.cb_document_type.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_document_type.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.cb_document_type.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.cb_document_type.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.cb_document_type.CornerRadius = 5;
      this.cb_document_type.DataSource = null;
      this.cb_document_type.DisplayMember = "";
      this.cb_document_type.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
      this.cb_document_type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cb_document_type.DropDownWidth = 258;
      this.cb_document_type.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.cb_document_type.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_document_type.FormattingEnabled = true;
      this.cb_document_type.IsDroppedDown = false;
      this.cb_document_type.ItemHeight = 40;
      this.cb_document_type.Location = new System.Drawing.Point(152, 3);
      this.cb_document_type.MaxDropDownItems = 4;
      this.cb_document_type.Name = "cb_document_type";
      this.cb_document_type.SelectedIndex = -1;
      this.cb_document_type.SelectedItem = null;
      this.cb_document_type.SelectedValue = null;
      this.cb_document_type.SelectionLength = 0;
      this.cb_document_type.SelectionStart = 0;
      this.cb_document_type.Size = new System.Drawing.Size(258, 40);
      this.cb_document_type.Sorted = false;
      this.cb_document_type.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.BASIC;
      this.cb_document_type.TabIndex = 3;
      this.cb_document_type.TabStop = false;
      this.cb_document_type.ValueMember = "";
      // 
      // lbl_document
      // 
      this.lbl_document.BackColor = System.Drawing.Color.Transparent;
      this.lbl_document.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_document.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_document.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_document.Location = new System.Drawing.Point(3, 46);
      this.lbl_document.Name = "lbl_document";
      this.lbl_document.Size = new System.Drawing.Size(143, 46);
      this.lbl_document.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_document.TabIndex = 1001;
      this.lbl_document.Text = "xDocument";
      this.lbl_document.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_type_document
      // 
      this.lbl_type_document.BackColor = System.Drawing.Color.Transparent;
      this.lbl_type_document.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_type_document.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_type_document.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_type_document.Location = new System.Drawing.Point(3, 0);
      this.lbl_type_document.Name = "lbl_type_document";
      this.lbl_type_document.Size = new System.Drawing.Size(143, 46);
      this.lbl_type_document.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_type_document.TabIndex = 1001;
      this.lbl_type_document.Text = "xDocumentType";
      this.lbl_type_document.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // txt_document
      // 
      this.txt_document.AllowSpace = true;
      this.txt_document.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_document.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_document.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_document.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_document.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.txt_document.CornerRadius = 5;
      this.txt_document.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_document.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.ExtendedMode;
      this.txt_document.Location = new System.Drawing.Point(152, 49);
      this.txt_document.MaxLength = 20;
      this.txt_document.Multiline = false;
      this.txt_document.Name = "txt_document";
      this.txt_document.PasswordChar = '\0';
      this.txt_document.ReadOnly = false;
      this.txt_document.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_document.SelectedText = "";
      this.txt_document.SelectionLength = 0;
      this.txt_document.SelectionStart = 0;
      this.txt_document.Size = new System.Drawing.Size(258, 40);
      this.txt_document.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_document.TabIndex = 4;
      this.txt_document.TabStop = false;
      this.txt_document.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_document.UseSystemPasswordChar = false;
      this.txt_document.WaterMark = null;
      this.txt_document.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // uc_dt_wedding
      // 
      this.uc_dt_wedding.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
      this.uc_dt_wedding.DateText = "";
      this.uc_dt_wedding.DateTextFont = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.uc_dt_wedding.DateTextWidth = 0;
      this.uc_dt_wedding.DateValue = new System.DateTime(((long)(0)));
      this.uc_dt_wedding.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.uc_dt_wedding.FormatFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
      this.uc_dt_wedding.FormatWidth = 156;
      this.uc_dt_wedding.Location = new System.Drawing.Point(152, 236);
      this.uc_dt_wedding.Name = "uc_dt_wedding";
      this.uc_dt_wedding.Size = new System.Drawing.Size(345, 43);
      this.uc_dt_wedding.TabIndex = 1011;
      this.uc_dt_wedding.ValuesFont = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      // 
      // uc_dt_birth
      // 
      this.uc_dt_birth.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
      this.uc_dt_birth.DateText = "";
      this.uc_dt_birth.DateTextFont = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.uc_dt_birth.DateTextWidth = 0;
      this.uc_dt_birth.DateValue = new System.DateTime(((long)(0)));
      this.uc_dt_birth.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.uc_dt_birth.FormatFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.uc_dt_birth.FormatWidth = 119;
      this.uc_dt_birth.Location = new System.Drawing.Point(152, 187);
      this.uc_dt_birth.Name = "uc_dt_birth";
      this.uc_dt_birth.Size = new System.Drawing.Size(308, 43);
      this.uc_dt_birth.TabIndex = 1010;
      this.uc_dt_birth.ValuesFont = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.uc_dt_birth.OnDateChanged += new System.EventHandler(this.uc_dt_birth_OnDateChanged);
      // 
      // contact
      // 
      this.contact.Controls.Add(this.tlp_contact);
      this.contact.Location = new System.Drawing.Point(4, 45);
      this.contact.Name = "contact";
      this.contact.Size = new System.Drawing.Size(906, 353);
      this.contact.TabIndex = 2;
      this.contact.Text = "xContacto";
      this.contact.UseVisualStyleBackColor = true;
      // 
      // tlp_contact
      // 
      this.tlp_contact.ColumnCount = 2;
      this.tlp_contact.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
      this.tlp_contact.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.tlp_contact.Controls.Add(this.lbl_twitter, 0, 4);
      this.tlp_contact.Controls.Add(this.txt_twitter, 1, 4);
      this.tlp_contact.Controls.Add(this.lbl_phone_01, 0, 0);
      this.tlp_contact.Controls.Add(this.txt_phone_01, 1, 0);
      this.tlp_contact.Controls.Add(this.lbl_email_02, 0, 3);
      this.tlp_contact.Controls.Add(this.txt_email_02, 1, 3);
      this.tlp_contact.Controls.Add(this.lbl_phone_02, 0, 1);
      this.tlp_contact.Controls.Add(this.txt_phone_02, 1, 1);
      this.tlp_contact.Controls.Add(this.txt_email_01, 1, 2);
      this.tlp_contact.Controls.Add(this.lbl_email_01, 0, 2);
      this.tlp_contact.Location = new System.Drawing.Point(3, 3);
      this.tlp_contact.Name = "tlp_contact";
      this.tlp_contact.RowCount = 6;
      this.tlp_contact.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_contact.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_contact.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_contact.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_contact.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_contact.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_contact.Size = new System.Drawing.Size(874, 324);
      this.tlp_contact.TabIndex = 20;
      // 
      // lbl_twitter
      // 
      this.lbl_twitter.BackColor = System.Drawing.Color.Transparent;
      this.lbl_twitter.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_twitter.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_twitter.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_twitter.Location = new System.Drawing.Point(3, 240);
      this.lbl_twitter.Name = "lbl_twitter";
      this.lbl_twitter.Size = new System.Drawing.Size(144, 60);
      this.lbl_twitter.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_twitter.TabIndex = 19;
      this.lbl_twitter.Text = "xTwitter";
      this.lbl_twitter.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // txt_twitter
      // 
      this.txt_twitter.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.txt_twitter.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_twitter.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_twitter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_twitter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_twitter.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_twitter.CornerRadius = 5;
      this.txt_twitter.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_twitter.Location = new System.Drawing.Point(153, 250);
      this.txt_twitter.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
      this.txt_twitter.MaxLength = 50;
      this.txt_twitter.Multiline = false;
      this.txt_twitter.Name = "txt_twitter";
      this.txt_twitter.PasswordChar = '\0';
      this.txt_twitter.ReadOnly = false;
      this.txt_twitter.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_twitter.SelectedText = "";
      this.txt_twitter.SelectionLength = 0;
      this.txt_twitter.SelectionStart = 0;
      this.txt_twitter.Size = new System.Drawing.Size(718, 40);
      this.txt_twitter.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_twitter.TabIndex = 5;
      this.txt_twitter.TabStop = false;
      this.txt_twitter.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_twitter.UseSystemPasswordChar = false;
      this.txt_twitter.WaterMark = null;
      this.txt_twitter.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // lbl_phone_01
      // 
      this.lbl_phone_01.BackColor = System.Drawing.Color.Transparent;
      this.lbl_phone_01.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_phone_01.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_phone_01.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_phone_01.Location = new System.Drawing.Point(3, 0);
      this.lbl_phone_01.Name = "lbl_phone_01";
      this.lbl_phone_01.Size = new System.Drawing.Size(144, 60);
      this.lbl_phone_01.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_phone_01.TabIndex = 15;
      this.lbl_phone_01.Text = "xTel.Movil";
      this.lbl_phone_01.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // txt_phone_01
      // 
      this.txt_phone_01.AllowSpace = true;
      this.txt_phone_01.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.txt_phone_01.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_phone_01.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_phone_01.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_phone_01.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_phone_01.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_phone_01.CornerRadius = 5;
      this.txt_phone_01.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_phone_01.Location = new System.Drawing.Point(153, 10);
      this.txt_phone_01.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
      this.txt_phone_01.MaxLength = 20;
      this.txt_phone_01.Multiline = false;
      this.txt_phone_01.Name = "txt_phone_01";
      this.txt_phone_01.PasswordChar = '\0';
      this.txt_phone_01.ReadOnly = false;
      this.txt_phone_01.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_phone_01.SelectedText = "";
      this.txt_phone_01.SelectionLength = 0;
      this.txt_phone_01.SelectionStart = 0;
      this.txt_phone_01.Size = new System.Drawing.Size(718, 40);
      this.txt_phone_01.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_phone_01.TabIndex = 1;
      this.txt_phone_01.TabStop = false;
      this.txt_phone_01.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_phone_01.UseSystemPasswordChar = false;
      this.txt_phone_01.WaterMark = null;
      this.txt_phone_01.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // lbl_email_02
      // 
      this.lbl_email_02.BackColor = System.Drawing.Color.Transparent;
      this.lbl_email_02.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_email_02.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_email_02.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_email_02.Location = new System.Drawing.Point(3, 180);
      this.lbl_email_02.Name = "lbl_email_02";
      this.lbl_email_02.Size = new System.Drawing.Size(144, 60);
      this.lbl_email_02.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_email_02.TabIndex = 16;
      this.lbl_email_02.Text = "xEmail2";
      this.lbl_email_02.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // txt_email_02
      // 
      this.txt_email_02.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.txt_email_02.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_email_02.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_email_02.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_email_02.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_email_02.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_email_02.CornerRadius = 5;
      this.txt_email_02.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_email_02.Location = new System.Drawing.Point(153, 190);
      this.txt_email_02.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
      this.txt_email_02.MaxLength = 50;
      this.txt_email_02.Multiline = false;
      this.txt_email_02.Name = "txt_email_02";
      this.txt_email_02.PasswordChar = '\0';
      this.txt_email_02.ReadOnly = false;
      this.txt_email_02.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_email_02.SelectedText = "";
      this.txt_email_02.SelectionLength = 0;
      this.txt_email_02.SelectionStart = 0;
      this.txt_email_02.Size = new System.Drawing.Size(718, 40);
      this.txt_email_02.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_email_02.TabIndex = 4;
      this.txt_email_02.TabStop = false;
      this.txt_email_02.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_email_02.UseSystemPasswordChar = false;
      this.txt_email_02.WaterMark = null;
      this.txt_email_02.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      this.txt_email_02.Enter += new System.EventHandler(this.txt_email_02_Enter);
      this.txt_email_02.Leave += new System.EventHandler(this.control_Leave);
      // 
      // lbl_phone_02
      // 
      this.lbl_phone_02.BackColor = System.Drawing.Color.Transparent;
      this.lbl_phone_02.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_phone_02.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_phone_02.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_phone_02.Location = new System.Drawing.Point(3, 60);
      this.lbl_phone_02.Name = "lbl_phone_02";
      this.lbl_phone_02.Size = new System.Drawing.Size(144, 60);
      this.lbl_phone_02.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_phone_02.TabIndex = 14;
      this.lbl_phone_02.Text = "xTel.Fijo";
      this.lbl_phone_02.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // txt_phone_02
      // 
      this.txt_phone_02.AllowSpace = true;
      this.txt_phone_02.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.txt_phone_02.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_phone_02.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_phone_02.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_phone_02.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_phone_02.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_phone_02.CornerRadius = 5;
      this.txt_phone_02.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_phone_02.Location = new System.Drawing.Point(153, 70);
      this.txt_phone_02.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
      this.txt_phone_02.MaxLength = 20;
      this.txt_phone_02.Multiline = false;
      this.txt_phone_02.Name = "txt_phone_02";
      this.txt_phone_02.PasswordChar = '\0';
      this.txt_phone_02.ReadOnly = false;
      this.txt_phone_02.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_phone_02.SelectedText = "";
      this.txt_phone_02.SelectionLength = 0;
      this.txt_phone_02.SelectionStart = 0;
      this.txt_phone_02.Size = new System.Drawing.Size(718, 40);
      this.txt_phone_02.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_phone_02.TabIndex = 2;
      this.txt_phone_02.TabStop = false;
      this.txt_phone_02.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_phone_02.UseSystemPasswordChar = false;
      this.txt_phone_02.WaterMark = null;
      this.txt_phone_02.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // txt_email_01
      // 
      this.txt_email_01.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.txt_email_01.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_email_01.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_email_01.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_email_01.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_email_01.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_email_01.CornerRadius = 5;
      this.txt_email_01.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_email_01.Location = new System.Drawing.Point(153, 130);
      this.txt_email_01.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
      this.txt_email_01.MaxLength = 50;
      this.txt_email_01.Multiline = false;
      this.txt_email_01.Name = "txt_email_01";
      this.txt_email_01.PasswordChar = '\0';
      this.txt_email_01.ReadOnly = false;
      this.txt_email_01.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_email_01.SelectedText = "";
      this.txt_email_01.SelectionLength = 0;
      this.txt_email_01.SelectionStart = 0;
      this.txt_email_01.Size = new System.Drawing.Size(718, 40);
      this.txt_email_01.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_email_01.TabIndex = 3;
      this.txt_email_01.TabStop = false;
      this.txt_email_01.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_email_01.UseSystemPasswordChar = false;
      this.txt_email_01.WaterMark = null;
      this.txt_email_01.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // lbl_email_01
      // 
      this.lbl_email_01.BackColor = System.Drawing.Color.Transparent;
      this.lbl_email_01.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_email_01.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_email_01.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_email_01.Location = new System.Drawing.Point(3, 120);
      this.lbl_email_01.Name = "lbl_email_01";
      this.lbl_email_01.Size = new System.Drawing.Size(144, 60);
      this.lbl_email_01.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_email_01.TabIndex = 17;
      this.lbl_email_01.Text = "xEmail1";
      this.lbl_email_01.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // address
      // 
      this.address.Controls.Add(this.lbl_address_validation);
      this.address.Controls.Add(this.chk_address_mode);
      this.address.Controls.Add(this.tlp_address_new);
      this.address.Controls.Add(this.tlp_address);
      this.address.Location = new System.Drawing.Point(4, 45);
      this.address.Name = "address";
      this.address.Padding = new System.Windows.Forms.Padding(3);
      this.address.Size = new System.Drawing.Size(906, 353);
      this.address.TabIndex = 1;
      this.address.Text = "xDirecci�n";
      this.address.UseVisualStyleBackColor = true;
      // 
      // lbl_address_validation
      // 
      this.lbl_address_validation.BackColor = System.Drawing.Color.Transparent;
      this.lbl_address_validation.Cursor = System.Windows.Forms.Cursors.Default;
      this.lbl_address_validation.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_address_validation.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_address_validation.Location = new System.Drawing.Point(378, 356);
      this.lbl_address_validation.Name = "lbl_address_validation";
      this.lbl_address_validation.Size = new System.Drawing.Size(516, 20);
      this.lbl_address_validation.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLUE;
      this.lbl_address_validation.TabIndex = 1002;
      this.lbl_address_validation.Text = "xAddressValidation";
      this.lbl_address_validation.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      this.lbl_address_validation.UseMnemonic = false;
      // 
      // chk_address_mode
      // 
      this.chk_address_mode.AutoSize = true;
      this.chk_address_mode.BackColor = System.Drawing.Color.Transparent;
      this.chk_address_mode.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.chk_address_mode.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.chk_address_mode.Location = new System.Drawing.Point(6, 316);
      this.chk_address_mode.MinimumSize = new System.Drawing.Size(189, 30);
      this.chk_address_mode.Name = "chk_address_mode";
      this.chk_address_mode.Padding = new System.Windows.Forms.Padding(5);
      this.chk_address_mode.Size = new System.Drawing.Size(189, 33);
      this.chk_address_mode.TabIndex = 40;
      this.chk_address_mode.Text = "xAddressModeFree";
      this.chk_address_mode.UseVisualStyleBackColor = true;
      this.chk_address_mode.CheckedChanged += new System.EventHandler(this.chk_address_mode_CheckedChanged);
      // 
      // tlp_address_new
      // 
      this.tlp_address_new.ColumnCount = 4;
      this.tlp_address_new.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
      this.tlp_address_new.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
      this.tlp_address_new.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
      this.tlp_address_new.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
      this.tlp_address_new.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
      this.tlp_address_new.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
      this.tlp_address_new.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
      this.tlp_address_new.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
      this.tlp_address_new.Controls.Add(this.cb_city, 1, 4);
      this.tlp_address_new.Controls.Add(this.cb_address_03, 1, 3);
      this.tlp_address_new.Controls.Add(this.cb_address_02, 1, 2);
      this.tlp_address_new.Controls.Add(this.lbl_country_new, 0, 5);
      this.tlp_address_new.Controls.Add(this.lbl_address_01_new, 0, 0);
      this.tlp_address_new.Controls.Add(this.cb_fed_entity_new, 3, 5);
      this.tlp_address_new.Controls.Add(this.txt_ext_num_new, 1, 1);
      this.tlp_address_new.Controls.Add(this.lbl_fed_entity_new, 2, 5);
      this.tlp_address_new.Controls.Add(this.txt_address_01_new, 1, 0);
      this.tlp_address_new.Controls.Add(this.txt_zip_new, 3, 1);
      this.tlp_address_new.Controls.Add(this.lbl_zip_new, 2, 1);
      this.tlp_address_new.Controls.Add(this.lbl_city_new, 0, 4);
      this.tlp_address_new.Controls.Add(this.lbl_address_03_new, 0, 3);
      this.tlp_address_new.Controls.Add(this.lbl_address_02_new, 0, 2);
      this.tlp_address_new.Controls.Add(this.lbl_ext_num_new, 0, 1);
      this.tlp_address_new.Controls.Add(this.cb_country_new, 1, 5);
      this.tlp_address_new.Location = new System.Drawing.Point(6, 8);
      this.tlp_address_new.Name = "tlp_address_new";
      this.tlp_address_new.RowCount = 7;
      this.tlp_address_new.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_address_new.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_address_new.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_address_new.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_address_new.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_address_new.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_address_new.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_address_new.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
      this.tlp_address_new.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_address_new.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_address_new.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_address_new.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_address_new.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_address_new.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_address_new.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_address_new.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
      this.tlp_address_new.Size = new System.Drawing.Size(874, 310);
      this.tlp_address_new.TabIndex = 41;
      // 
      // cb_city
      // 
      this.cb_city.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.cb_city.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_city.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.cb_city.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.cb_city.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.tlp_address_new.SetColumnSpan(this.cb_city, 3);
      this.cb_city.CornerRadius = 5;
      this.cb_city.DataSource = null;
      this.cb_city.DisplayMember = "";
      this.cb_city.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
      this.cb_city.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
      this.cb_city.DropDownWidth = 721;
      this.cb_city.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.cb_city.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_city.FormattingEnabled = true;
      this.cb_city.FormatValidator = null;
      this.cb_city.IsDroppedDown = false;
      this.cb_city.ItemHeight = 40;
      this.cb_city.Location = new System.Drawing.Point(153, 194);
      this.cb_city.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
      this.cb_city.MaxDropDownItems = 4;
      this.cb_city.Name = "cb_city";
      this.cb_city.SelectedIndex = -1;
      this.cb_city.SelectedItem = null;
      this.cb_city.SelectedValue = null;
      this.cb_city.SelectionLength = 0;
      this.cb_city.SelectionStart = 0;
      this.cb_city.Size = new System.Drawing.Size(721, 40);
      this.cb_city.Sorted = false;
      this.cb_city.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.BASIC;
      this.cb_city.TabIndex = 5;
      this.cb_city.TabStop = false;
      this.cb_city.ValueMember = "";
      this.cb_city.SelectedIndexChanged += new System.EventHandler(this.cb_city_SelectedIndexChanged);
      // 
      // cb_address_03
      // 
      this.cb_address_03.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.cb_address_03.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_address_03.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.cb_address_03.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.cb_address_03.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.tlp_address_new.SetColumnSpan(this.cb_address_03, 3);
      this.cb_address_03.CornerRadius = 5;
      this.cb_address_03.DataSource = null;
      this.cb_address_03.DisplayMember = "";
      this.cb_address_03.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
      this.cb_address_03.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
      this.cb_address_03.DropDownWidth = 721;
      this.cb_address_03.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.cb_address_03.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_address_03.FormattingEnabled = true;
      this.cb_address_03.FormatValidator = null;
      this.cb_address_03.IsDroppedDown = false;
      this.cb_address_03.ItemHeight = 40;
      this.cb_address_03.Location = new System.Drawing.Point(153, 150);
      this.cb_address_03.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
      this.cb_address_03.MaxDropDownItems = 4;
      this.cb_address_03.Name = "cb_address_03";
      this.cb_address_03.SelectedIndex = -1;
      this.cb_address_03.SelectedItem = null;
      this.cb_address_03.SelectedValue = null;
      this.cb_address_03.SelectionLength = 0;
      this.cb_address_03.SelectionStart = 0;
      this.cb_address_03.Size = new System.Drawing.Size(721, 40);
      this.cb_address_03.Sorted = false;
      this.cb_address_03.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.BASIC;
      this.cb_address_03.TabIndex = 4;
      this.cb_address_03.TabStop = false;
      this.cb_address_03.ValueMember = "";
      this.cb_address_03.SelectedIndexChanged += new System.EventHandler(this.cb_address_03_SelectedIndexChanged);
      // 
      // cb_address_02
      // 
      this.cb_address_02.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.cb_address_02.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_address_02.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.cb_address_02.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.cb_address_02.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.tlp_address_new.SetColumnSpan(this.cb_address_02, 3);
      this.cb_address_02.CornerRadius = 5;
      this.cb_address_02.DataSource = null;
      this.cb_address_02.DisplayMember = "";
      this.cb_address_02.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
      this.cb_address_02.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
      this.cb_address_02.DropDownWidth = 721;
      this.cb_address_02.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.cb_address_02.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_address_02.FormattingEnabled = true;
      this.cb_address_02.FormatValidator = null;
      this.cb_address_02.IsDroppedDown = false;
      this.cb_address_02.ItemHeight = 40;
      this.cb_address_02.Location = new System.Drawing.Point(153, 106);
      this.cb_address_02.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
      this.cb_address_02.MaxDropDownItems = 4;
      this.cb_address_02.Name = "cb_address_02";
      this.cb_address_02.SelectedIndex = -1;
      this.cb_address_02.SelectedItem = null;
      this.cb_address_02.SelectedValue = null;
      this.cb_address_02.SelectionLength = 0;
      this.cb_address_02.SelectionStart = 0;
      this.cb_address_02.Size = new System.Drawing.Size(721, 40);
      this.cb_address_02.Sorted = false;
      this.cb_address_02.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.BASIC;
      this.cb_address_02.TabIndex = 3;
      this.cb_address_02.TabStop = false;
      this.cb_address_02.ValueMember = "";
      this.cb_address_02.SelectedIndexChanged += new System.EventHandler(this.cb_address_02_SelectedIndexChanged);
      // 
      // lbl_country_new
      // 
      this.lbl_country_new.BackColor = System.Drawing.Color.Transparent;
      this.lbl_country_new.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_country_new.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_country_new.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_country_new.Location = new System.Drawing.Point(3, 236);
      this.lbl_country_new.Name = "lbl_country_new";
      this.lbl_country_new.Size = new System.Drawing.Size(144, 52);
      this.lbl_country_new.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_country_new.TabIndex = 12;
      this.lbl_country_new.Text = "xPa�s";
      this.lbl_country_new.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_address_01_new
      // 
      this.lbl_address_01_new.BackColor = System.Drawing.Color.Transparent;
      this.lbl_address_01_new.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_address_01_new.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_address_01_new.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_address_01_new.Location = new System.Drawing.Point(3, 0);
      this.lbl_address_01_new.MinimumSize = new System.Drawing.Size(140, 0);
      this.lbl_address_01_new.Name = "lbl_address_01_new";
      this.lbl_address_01_new.Size = new System.Drawing.Size(144, 52);
      this.lbl_address_01_new.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_address_01_new.TabIndex = 0;
      this.lbl_address_01_new.Text = "xCalle";
      this.lbl_address_01_new.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // cb_fed_entity_new
      // 
      this.cb_fed_entity_new.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.cb_fed_entity_new.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_fed_entity_new.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.cb_fed_entity_new.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.cb_fed_entity_new.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.cb_fed_entity_new.CornerRadius = 5;
      this.cb_fed_entity_new.DataSource = null;
      this.cb_fed_entity_new.DisplayMember = "";
      this.cb_fed_entity_new.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
      this.cb_fed_entity_new.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
      this.cb_fed_entity_new.DropDownWidth = 359;
      this.cb_fed_entity_new.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.cb_fed_entity_new.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_fed_entity_new.FormattingEnabled = true;
      this.cb_fed_entity_new.FormatValidator = null;
      this.cb_fed_entity_new.IsDroppedDown = false;
      this.cb_fed_entity_new.ItemHeight = 40;
      this.cb_fed_entity_new.Location = new System.Drawing.Point(515, 242);
      this.cb_fed_entity_new.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
      this.cb_fed_entity_new.MaxDropDownItems = 4;
      this.cb_fed_entity_new.Name = "cb_fed_entity_new";
      this.cb_fed_entity_new.SelectedIndex = -1;
      this.cb_fed_entity_new.SelectedItem = null;
      this.cb_fed_entity_new.SelectedValue = null;
      this.cb_fed_entity_new.SelectionLength = 0;
      this.cb_fed_entity_new.SelectionStart = 0;
      this.cb_fed_entity_new.Size = new System.Drawing.Size(359, 40);
      this.cb_fed_entity_new.Sorted = false;
      this.cb_fed_entity_new.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.BASIC;
      this.cb_fed_entity_new.TabIndex = 7;
      this.cb_fed_entity_new.TabStop = false;
      this.cb_fed_entity_new.ValueMember = "";
      this.cb_fed_entity_new.SelectedIndexChanged += new System.EventHandler(this.cb_fed_entity_new_SelectedIndexChanged);
      // 
      // txt_ext_num_new
      // 
      this.txt_ext_num_new.AllowSpace = true;
      this.txt_ext_num_new.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.txt_ext_num_new.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_ext_num_new.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_ext_num_new.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_ext_num_new.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_ext_num_new.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_ext_num_new.CornerRadius = 5;
      this.txt_ext_num_new.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_ext_num_new.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.ExtendedMode;
      this.txt_ext_num_new.Location = new System.Drawing.Point(153, 58);
      this.txt_ext_num_new.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
      this.txt_ext_num_new.MaxLength = 10;
      this.txt_ext_num_new.Multiline = false;
      this.txt_ext_num_new.Name = "txt_ext_num_new";
      this.txt_ext_num_new.PasswordChar = '\0';
      this.txt_ext_num_new.ReadOnly = false;
      this.txt_ext_num_new.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_ext_num_new.SelectedText = "";
      this.txt_ext_num_new.SelectionLength = 0;
      this.txt_ext_num_new.SelectionStart = 0;
      this.txt_ext_num_new.Size = new System.Drawing.Size(200, 40);
      this.txt_ext_num_new.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_ext_num_new.TabIndex = 1;
      this.txt_ext_num_new.TabStop = false;
      this.txt_ext_num_new.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_ext_num_new.UseSystemPasswordChar = false;
      this.txt_ext_num_new.WaterMark = null;
      this.txt_ext_num_new.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      this.txt_ext_num_new.TextChanged += new System.EventHandler(this.txt_ext_num_new_TextChanged);
      // 
      // lbl_fed_entity_new
      // 
      this.lbl_fed_entity_new.BackColor = System.Drawing.Color.Transparent;
      this.lbl_fed_entity_new.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_fed_entity_new.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_fed_entity_new.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_fed_entity_new.Location = new System.Drawing.Point(359, 236);
      this.lbl_fed_entity_new.Name = "lbl_fed_entity_new";
      this.lbl_fed_entity_new.Size = new System.Drawing.Size(150, 52);
      this.lbl_fed_entity_new.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_fed_entity_new.TabIndex = 14;
      this.lbl_fed_entity_new.Text = "xFederal State";
      this.lbl_fed_entity_new.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // txt_address_01_new
      // 
      this.txt_address_01_new.AllowSpace = true;
      this.txt_address_01_new.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.txt_address_01_new.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_address_01_new.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_address_01_new.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_address_01_new.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_address_01_new.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.tlp_address_new.SetColumnSpan(this.txt_address_01_new, 3);
      this.txt_address_01_new.CornerRadius = 5;
      this.txt_address_01_new.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_address_01_new.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.ExtendedMode;
      this.txt_address_01_new.Location = new System.Drawing.Point(153, 6);
      this.txt_address_01_new.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
      this.txt_address_01_new.MaxLength = 50;
      this.txt_address_01_new.Multiline = false;
      this.txt_address_01_new.Name = "txt_address_01_new";
      this.txt_address_01_new.PasswordChar = '\0';
      this.txt_address_01_new.ReadOnly = false;
      this.txt_address_01_new.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_address_01_new.SelectedText = "";
      this.txt_address_01_new.SelectionLength = 0;
      this.txt_address_01_new.SelectionStart = 0;
      this.txt_address_01_new.Size = new System.Drawing.Size(721, 40);
      this.txt_address_01_new.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_address_01_new.TabIndex = 0;
      this.txt_address_01_new.TabStop = false;
      this.txt_address_01_new.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_address_01_new.UseSystemPasswordChar = false;
      this.txt_address_01_new.WaterMark = null;
      this.txt_address_01_new.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      this.txt_address_01_new.TextChanged += new System.EventHandler(this.txt_address_01_new_TextChanged);
      // 
      // txt_zip_new
      // 
      this.txt_zip_new.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.txt_zip_new.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_zip_new.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_zip_new.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_zip_new.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_zip_new.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_zip_new.CornerRadius = 5;
      this.txt_zip_new.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_zip_new.Location = new System.Drawing.Point(515, 58);
      this.txt_zip_new.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
      this.txt_zip_new.MaxLength = 10;
      this.txt_zip_new.Multiline = false;
      this.txt_zip_new.Name = "txt_zip_new";
      this.txt_zip_new.PasswordChar = '\0';
      this.txt_zip_new.ReadOnly = false;
      this.txt_zip_new.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_zip_new.SelectedText = "";
      this.txt_zip_new.SelectionLength = 0;
      this.txt_zip_new.SelectionStart = 0;
      this.txt_zip_new.Size = new System.Drawing.Size(200, 40);
      this.txt_zip_new.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_zip_new.TabIndex = 2;
      this.txt_zip_new.TabStop = false;
      this.txt_zip_new.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_zip_new.UseSystemPasswordChar = false;
      this.txt_zip_new.WaterMark = null;
      this.txt_zip_new.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      this.txt_zip_new.TextChanged += new System.EventHandler(this.txt_zip_new_TextChanged);
      // 
      // lbl_zip_new
      // 
      this.lbl_zip_new.BackColor = System.Drawing.Color.Transparent;
      this.lbl_zip_new.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_zip_new.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_zip_new.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_zip_new.Location = new System.Drawing.Point(359, 52);
      this.lbl_zip_new.Name = "lbl_zip_new";
      this.lbl_zip_new.Size = new System.Drawing.Size(150, 52);
      this.lbl_zip_new.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_zip_new.TabIndex = 4;
      this.lbl_zip_new.Text = "xC.Postal";
      this.lbl_zip_new.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_city_new
      // 
      this.lbl_city_new.BackColor = System.Drawing.Color.Transparent;
      this.lbl_city_new.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_city_new.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_city_new.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_city_new.Location = new System.Drawing.Point(3, 192);
      this.lbl_city_new.Name = "lbl_city_new";
      this.lbl_city_new.Size = new System.Drawing.Size(144, 44);
      this.lbl_city_new.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_city_new.TabIndex = 10;
      this.lbl_city_new.Text = "xMunicipio";
      this.lbl_city_new.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_address_03_new
      // 
      this.lbl_address_03_new.BackColor = System.Drawing.Color.Transparent;
      this.lbl_address_03_new.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_address_03_new.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_address_03_new.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_address_03_new.Location = new System.Drawing.Point(3, 148);
      this.lbl_address_03_new.Name = "lbl_address_03_new";
      this.lbl_address_03_new.Size = new System.Drawing.Size(144, 44);
      this.lbl_address_03_new.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_address_03_new.TabIndex = 8;
      this.lbl_address_03_new.Text = "xDelegaci�n";
      this.lbl_address_03_new.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_address_02_new
      // 
      this.lbl_address_02_new.BackColor = System.Drawing.Color.Transparent;
      this.lbl_address_02_new.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_address_02_new.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_address_02_new.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_address_02_new.Location = new System.Drawing.Point(3, 104);
      this.lbl_address_02_new.Name = "lbl_address_02_new";
      this.lbl_address_02_new.Size = new System.Drawing.Size(144, 44);
      this.lbl_address_02_new.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_address_02_new.TabIndex = 6;
      this.lbl_address_02_new.Text = "xColonia";
      this.lbl_address_02_new.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_ext_num_new
      // 
      this.lbl_ext_num_new.BackColor = System.Drawing.Color.Transparent;
      this.lbl_ext_num_new.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_ext_num_new.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_ext_num_new.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_ext_num_new.Location = new System.Drawing.Point(3, 52);
      this.lbl_ext_num_new.Name = "lbl_ext_num_new";
      this.lbl_ext_num_new.Size = new System.Drawing.Size(144, 52);
      this.lbl_ext_num_new.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_ext_num_new.TabIndex = 2;
      this.lbl_ext_num_new.Text = "xN�m. Ext.";
      this.lbl_ext_num_new.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // cb_country_new
      // 
      this.cb_country_new.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_country_new.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.cb_country_new.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.cb_country_new.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.cb_country_new.CornerRadius = 5;
      this.cb_country_new.DataSource = null;
      this.cb_country_new.DisplayMember = "";
      this.cb_country_new.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
      this.cb_country_new.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
      this.cb_country_new.DropDownWidth = 200;
      this.cb_country_new.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.cb_country_new.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_country_new.FormattingEnabled = true;
      this.cb_country_new.FormatValidator = null;
      this.cb_country_new.IsDroppedDown = false;
      this.cb_country_new.ItemHeight = 40;
      this.cb_country_new.Location = new System.Drawing.Point(153, 242);
      this.cb_country_new.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
      this.cb_country_new.MaxDropDownItems = 4;
      this.cb_country_new.Name = "cb_country_new";
      this.cb_country_new.SelectedIndex = -1;
      this.cb_country_new.SelectedItem = null;
      this.cb_country_new.SelectedValue = null;
      this.cb_country_new.SelectionLength = 0;
      this.cb_country_new.SelectionStart = 0;
      this.cb_country_new.Size = new System.Drawing.Size(200, 40);
      this.cb_country_new.Sorted = false;
      this.cb_country_new.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.BASIC;
      this.cb_country_new.TabIndex = 6;
      this.cb_country_new.TabStop = false;
      this.cb_country_new.ValueMember = "";
      this.cb_country_new.SelectedIndexChanged += new System.EventHandler(this.cb_country_new_SelectedIndexChanged);
      // 
      // tlp_address
      // 
      this.tlp_address.ColumnCount = 4;
      this.tlp_address.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
      this.tlp_address.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
      this.tlp_address.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
      this.tlp_address.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
      this.tlp_address.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
      this.tlp_address.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
      this.tlp_address.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
      this.tlp_address.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
      this.tlp_address.Controls.Add(this.lbl_country, 0, 5);
      this.tlp_address.Controls.Add(this.lbl_address_01, 0, 0);
      this.tlp_address.Controls.Add(this.cb_fed_entity, 3, 5);
      this.tlp_address.Controls.Add(this.txt_ext_num, 1, 1);
      this.tlp_address.Controls.Add(this.lbl_fed_entity, 2, 5);
      this.tlp_address.Controls.Add(this.txt_address_01, 1, 0);
      this.tlp_address.Controls.Add(this.txt_zip, 3, 1);
      this.tlp_address.Controls.Add(this.lbl_zip, 2, 1);
      this.tlp_address.Controls.Add(this.lbl_city, 0, 4);
      this.tlp_address.Controls.Add(this.lbl_address_03, 0, 3);
      this.tlp_address.Controls.Add(this.txt_city, 1, 4);
      this.tlp_address.Controls.Add(this.lbl_address_02, 0, 2);
      this.tlp_address.Controls.Add(this.lbl_ext_num, 0, 1);
      this.tlp_address.Controls.Add(this.txt_address_02, 1, 2);
      this.tlp_address.Controls.Add(this.txt_address_03, 1, 3);
      this.tlp_address.Controls.Add(this.cb_country, 1, 5);
      this.tlp_address.Location = new System.Drawing.Point(6, 9);
      this.tlp_address.Name = "tlp_address";
      this.tlp_address.RowCount = 7;
      this.tlp_address.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_address.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_address.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_address.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_address.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_address.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_address.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_address.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_address.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_address.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_address.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_address.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_address.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_address.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_address.Size = new System.Drawing.Size(874, 309);
      this.tlp_address.TabIndex = 16;
      // 
      // lbl_country
      // 
      this.lbl_country.BackColor = System.Drawing.Color.Transparent;
      this.lbl_country.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_country.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_country.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_country.Location = new System.Drawing.Point(3, 260);
      this.lbl_country.Name = "lbl_country";
      this.lbl_country.Size = new System.Drawing.Size(144, 56);
      this.lbl_country.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_country.TabIndex = 12;
      this.lbl_country.Text = "xPa�s";
      this.lbl_country.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_address_01
      // 
      this.lbl_address_01.BackColor = System.Drawing.Color.Transparent;
      this.lbl_address_01.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_address_01.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_address_01.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_address_01.Location = new System.Drawing.Point(3, 0);
      this.lbl_address_01.MinimumSize = new System.Drawing.Size(140, 0);
      this.lbl_address_01.Name = "lbl_address_01";
      this.lbl_address_01.Size = new System.Drawing.Size(144, 52);
      this.lbl_address_01.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_address_01.TabIndex = 0;
      this.lbl_address_01.Text = "xCalle";
      this.lbl_address_01.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // cb_fed_entity
      // 
      this.cb_fed_entity.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.cb_fed_entity.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_fed_entity.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.cb_fed_entity.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.cb_fed_entity.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.cb_fed_entity.CornerRadius = 5;
      this.cb_fed_entity.DataSource = null;
      this.cb_fed_entity.DisplayMember = "";
      this.cb_fed_entity.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
      this.cb_fed_entity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
      this.cb_fed_entity.DropDownWidth = 359;
      this.cb_fed_entity.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.cb_fed_entity.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_fed_entity.FormattingEnabled = true;
      this.cb_fed_entity.FormatValidator = null;
      this.cb_fed_entity.IsDroppedDown = false;
      this.cb_fed_entity.ItemHeight = 40;
      this.cb_fed_entity.Location = new System.Drawing.Point(515, 268);
      this.cb_fed_entity.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
      this.cb_fed_entity.MaxDropDownItems = 8;
      this.cb_fed_entity.Name = "cb_fed_entity";
      this.cb_fed_entity.SelectedIndex = -1;
      this.cb_fed_entity.SelectedItem = null;
      this.cb_fed_entity.SelectedValue = null;
      this.cb_fed_entity.SelectionLength = 0;
      this.cb_fed_entity.SelectionStart = 0;
      this.cb_fed_entity.Size = new System.Drawing.Size(359, 40);
      this.cb_fed_entity.Sorted = false;
      this.cb_fed_entity.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.BASIC;
      this.cb_fed_entity.TabIndex = 15;
      this.cb_fed_entity.TabStop = false;
      this.cb_fed_entity.ValueMember = "";
      // 
      // txt_ext_num
      // 
      this.txt_ext_num.AllowSpace = true;
      this.txt_ext_num.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.txt_ext_num.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_ext_num.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_ext_num.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_ext_num.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_ext_num.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_ext_num.CornerRadius = 5;
      this.txt_ext_num.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_ext_num.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.ExtendedMode;
      this.txt_ext_num.Location = new System.Drawing.Point(153, 58);
      this.txt_ext_num.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
      this.txt_ext_num.MaxLength = 10;
      this.txt_ext_num.Multiline = false;
      this.txt_ext_num.Name = "txt_ext_num";
      this.txt_ext_num.PasswordChar = '\0';
      this.txt_ext_num.ReadOnly = false;
      this.txt_ext_num.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_ext_num.SelectedText = "";
      this.txt_ext_num.SelectionLength = 0;
      this.txt_ext_num.SelectionStart = 0;
      this.txt_ext_num.Size = new System.Drawing.Size(200, 40);
      this.txt_ext_num.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_ext_num.TabIndex = 3;
      this.txt_ext_num.TabStop = false;
      this.txt_ext_num.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_ext_num.UseSystemPasswordChar = false;
      this.txt_ext_num.WaterMark = null;
      this.txt_ext_num.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // lbl_fed_entity
      // 
      this.lbl_fed_entity.BackColor = System.Drawing.Color.Transparent;
      this.lbl_fed_entity.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_fed_entity.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_fed_entity.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_fed_entity.Location = new System.Drawing.Point(359, 260);
      this.lbl_fed_entity.Name = "lbl_fed_entity";
      this.lbl_fed_entity.Size = new System.Drawing.Size(150, 56);
      this.lbl_fed_entity.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_fed_entity.TabIndex = 14;
      this.lbl_fed_entity.Text = "xFederal State";
      this.lbl_fed_entity.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // txt_address_01
      // 
      this.txt_address_01.AllowSpace = true;
      this.txt_address_01.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.txt_address_01.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_address_01.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_address_01.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_address_01.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_address_01.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.tlp_address.SetColumnSpan(this.txt_address_01, 3);
      this.txt_address_01.CornerRadius = 5;
      this.txt_address_01.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_address_01.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.ExtendedMode;
      this.txt_address_01.Location = new System.Drawing.Point(153, 6);
      this.txt_address_01.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
      this.txt_address_01.MaxLength = 50;
      this.txt_address_01.Multiline = false;
      this.txt_address_01.Name = "txt_address_01";
      this.txt_address_01.PasswordChar = '\0';
      this.txt_address_01.ReadOnly = false;
      this.txt_address_01.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_address_01.SelectedText = "";
      this.txt_address_01.SelectionLength = 0;
      this.txt_address_01.SelectionStart = 0;
      this.txt_address_01.Size = new System.Drawing.Size(721, 40);
      this.txt_address_01.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_address_01.TabIndex = 1;
      this.txt_address_01.TabStop = false;
      this.txt_address_01.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_address_01.UseSystemPasswordChar = false;
      this.txt_address_01.WaterMark = null;
      this.txt_address_01.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // txt_zip
      // 
      this.txt_zip.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.txt_zip.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_zip.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_zip.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_zip.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_zip.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_zip.CornerRadius = 5;
      this.txt_zip.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_zip.Location = new System.Drawing.Point(515, 58);
      this.txt_zip.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
      this.txt_zip.MaxLength = 10;
      this.txt_zip.Multiline = false;
      this.txt_zip.Name = "txt_zip";
      this.txt_zip.PasswordChar = '\0';
      this.txt_zip.ReadOnly = false;
      this.txt_zip.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_zip.SelectedText = "";
      this.txt_zip.SelectionLength = 0;
      this.txt_zip.SelectionStart = 0;
      this.txt_zip.Size = new System.Drawing.Size(200, 40);
      this.txt_zip.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_zip.TabIndex = 5;
      this.txt_zip.TabStop = false;
      this.txt_zip.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_zip.UseSystemPasswordChar = false;
      this.txt_zip.WaterMark = null;
      this.txt_zip.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      this.txt_zip.Enter += new System.EventHandler(this.txt_zip_Enter);
      this.txt_zip.Leave += new System.EventHandler(this.control_Leave);
      // 
      // lbl_zip
      // 
      this.lbl_zip.BackColor = System.Drawing.Color.Transparent;
      this.lbl_zip.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_zip.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_zip.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_zip.Location = new System.Drawing.Point(359, 52);
      this.lbl_zip.Name = "lbl_zip";
      this.lbl_zip.Size = new System.Drawing.Size(150, 52);
      this.lbl_zip.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_zip.TabIndex = 4;
      this.lbl_zip.Text = "xC.Postal";
      this.lbl_zip.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_city
      // 
      this.lbl_city.BackColor = System.Drawing.Color.Transparent;
      this.lbl_city.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_city.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_city.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_city.Location = new System.Drawing.Point(3, 208);
      this.lbl_city.Name = "lbl_city";
      this.lbl_city.Size = new System.Drawing.Size(144, 52);
      this.lbl_city.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_city.TabIndex = 10;
      this.lbl_city.Text = "xMunicipio";
      this.lbl_city.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_address_03
      // 
      this.lbl_address_03.BackColor = System.Drawing.Color.Transparent;
      this.lbl_address_03.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_address_03.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_address_03.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_address_03.Location = new System.Drawing.Point(3, 156);
      this.lbl_address_03.Name = "lbl_address_03";
      this.lbl_address_03.Size = new System.Drawing.Size(144, 52);
      this.lbl_address_03.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_address_03.TabIndex = 8;
      this.lbl_address_03.Text = "xDelegaci�n";
      this.lbl_address_03.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // txt_city
      // 
      this.txt_city.AllowSpace = true;
      this.txt_city.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.txt_city.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_city.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_city.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_city.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_city.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.tlp_address.SetColumnSpan(this.txt_city, 3);
      this.txt_city.CornerRadius = 5;
      this.txt_city.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_city.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.ExtendedMode;
      this.txt_city.Location = new System.Drawing.Point(153, 214);
      this.txt_city.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
      this.txt_city.MaxLength = 50;
      this.txt_city.Multiline = false;
      this.txt_city.Name = "txt_city";
      this.txt_city.PasswordChar = '\0';
      this.txt_city.ReadOnly = false;
      this.txt_city.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_city.SelectedText = "";
      this.txt_city.SelectionLength = 0;
      this.txt_city.SelectionStart = 0;
      this.txt_city.Size = new System.Drawing.Size(721, 40);
      this.txt_city.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_city.TabIndex = 11;
      this.txt_city.TabStop = false;
      this.txt_city.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_city.UseSystemPasswordChar = false;
      this.txt_city.WaterMark = null;
      this.txt_city.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // lbl_address_02
      // 
      this.lbl_address_02.BackColor = System.Drawing.Color.Transparent;
      this.lbl_address_02.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_address_02.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_address_02.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_address_02.Location = new System.Drawing.Point(3, 104);
      this.lbl_address_02.Name = "lbl_address_02";
      this.lbl_address_02.Size = new System.Drawing.Size(144, 52);
      this.lbl_address_02.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_address_02.TabIndex = 6;
      this.lbl_address_02.Text = "xColonia";
      this.lbl_address_02.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_ext_num
      // 
      this.lbl_ext_num.BackColor = System.Drawing.Color.Transparent;
      this.lbl_ext_num.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_ext_num.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_ext_num.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_ext_num.Location = new System.Drawing.Point(3, 52);
      this.lbl_ext_num.Name = "lbl_ext_num";
      this.lbl_ext_num.Size = new System.Drawing.Size(144, 52);
      this.lbl_ext_num.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_ext_num.TabIndex = 2;
      this.lbl_ext_num.Text = "xN�m. Ext.";
      this.lbl_ext_num.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // txt_address_02
      // 
      this.txt_address_02.AllowSpace = true;
      this.txt_address_02.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.txt_address_02.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_address_02.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_address_02.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_address_02.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_address_02.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.tlp_address.SetColumnSpan(this.txt_address_02, 3);
      this.txt_address_02.CornerRadius = 5;
      this.txt_address_02.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_address_02.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.ExtendedMode;
      this.txt_address_02.Location = new System.Drawing.Point(153, 110);
      this.txt_address_02.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
      this.txt_address_02.MaxLength = 100;
      this.txt_address_02.Multiline = false;
      this.txt_address_02.Name = "txt_address_02";
      this.txt_address_02.PasswordChar = '\0';
      this.txt_address_02.ReadOnly = false;
      this.txt_address_02.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_address_02.SelectedText = "";
      this.txt_address_02.SelectionLength = 0;
      this.txt_address_02.SelectionStart = 0;
      this.txt_address_02.Size = new System.Drawing.Size(721, 40);
      this.txt_address_02.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_address_02.TabIndex = 7;
      this.txt_address_02.TabStop = false;
      this.txt_address_02.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_address_02.UseSystemPasswordChar = false;
      this.txt_address_02.WaterMark = null;
      this.txt_address_02.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // txt_address_03
      // 
      this.txt_address_03.AllowSpace = true;
      this.txt_address_03.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.txt_address_03.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_address_03.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_address_03.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_address_03.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_address_03.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.tlp_address.SetColumnSpan(this.txt_address_03, 3);
      this.txt_address_03.CornerRadius = 5;
      this.txt_address_03.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_address_03.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.ExtendedMode;
      this.txt_address_03.Location = new System.Drawing.Point(153, 162);
      this.txt_address_03.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
      this.txt_address_03.MaxLength = 50;
      this.txt_address_03.Multiline = false;
      this.txt_address_03.Name = "txt_address_03";
      this.txt_address_03.PasswordChar = '\0';
      this.txt_address_03.ReadOnly = false;
      this.txt_address_03.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_address_03.SelectedText = "";
      this.txt_address_03.SelectionLength = 0;
      this.txt_address_03.SelectionStart = 0;
      this.txt_address_03.Size = new System.Drawing.Size(721, 40);
      this.txt_address_03.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_address_03.TabIndex = 9;
      this.txt_address_03.TabStop = false;
      this.txt_address_03.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_address_03.UseSystemPasswordChar = false;
      this.txt_address_03.WaterMark = null;
      this.txt_address_03.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // cb_country
      // 
      this.cb_country.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_country.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.cb_country.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.cb_country.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.cb_country.CornerRadius = 5;
      this.cb_country.DataSource = null;
      this.cb_country.DisplayMember = "";
      this.cb_country.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
      this.cb_country.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
      this.cb_country.DropDownWidth = 200;
      this.cb_country.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.cb_country.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_country.FormattingEnabled = true;
      this.cb_country.FormatValidator = null;
      this.cb_country.IsDroppedDown = false;
      this.cb_country.ItemHeight = 40;
      this.cb_country.Location = new System.Drawing.Point(153, 266);
      this.cb_country.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
      this.cb_country.MaxDropDownItems = 4;
      this.cb_country.Name = "cb_country";
      this.cb_country.SelectedIndex = -1;
      this.cb_country.SelectedItem = null;
      this.cb_country.SelectedValue = null;
      this.cb_country.SelectionLength = 0;
      this.cb_country.SelectionStart = 0;
      this.cb_country.Size = new System.Drawing.Size(200, 40);
      this.cb_country.Sorted = false;
      this.cb_country.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.BASIC;
      this.cb_country.TabIndex = 13;
      this.cb_country.TabStop = false;
      this.cb_country.ValueMember = "";
      // 
      // beneficiary
      // 
      this.beneficiary.Controls.Add(this.chk_holder_has_beneficiary);
      this.beneficiary.Controls.Add(this.tlp_ben_name);
      this.beneficiary.Controls.Add(this.tlp_ben_principal1);
      this.beneficiary.Controls.Add(this.tlp_ben_principal);
      this.beneficiary.Controls.Add(this.gb_beneficiary);
      this.beneficiary.Location = new System.Drawing.Point(4, 45);
      this.beneficiary.Name = "beneficiary";
      this.beneficiary.Padding = new System.Windows.Forms.Padding(3);
      this.beneficiary.Size = new System.Drawing.Size(906, 353);
      this.beneficiary.TabIndex = 7;
      this.beneficiary.Text = "xBeneficiary";
      this.beneficiary.UseVisualStyleBackColor = true;
      // 
      // chk_holder_has_beneficiary
      // 
      this.chk_holder_has_beneficiary.AutoSize = true;
      this.chk_holder_has_beneficiary.BackColor = System.Drawing.Color.Transparent;
      this.chk_holder_has_beneficiary.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.chk_holder_has_beneficiary.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.chk_holder_has_beneficiary.Location = new System.Drawing.Point(6, 316);
      this.chk_holder_has_beneficiary.MinimumSize = new System.Drawing.Size(230, 30);
      this.chk_holder_has_beneficiary.Name = "chk_holder_has_beneficiary";
      this.chk_holder_has_beneficiary.Padding = new System.Windows.Forms.Padding(5);
      this.chk_holder_has_beneficiary.Size = new System.Drawing.Size(230, 33);
      this.chk_holder_has_beneficiary.TabIndex = 39;
      this.chk_holder_has_beneficiary.Text = "xHolder has a beneficiary";
      this.chk_holder_has_beneficiary.UseVisualStyleBackColor = true;
      this.chk_holder_has_beneficiary.CheckedChanged += new System.EventHandler(this.chk_holder_has_beneficiary_CheckedChanged);
      // 
      // tlp_ben_name
      // 
      this.tlp_ben_name.ColumnCount = 3;
      this.tlp_ben_name.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
      this.tlp_ben_name.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
      this.tlp_ben_name.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
      this.tlp_ben_name.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
      this.tlp_ben_name.Controls.Add(this.lbl_beneficiary_name, 0, 0);
      this.tlp_ben_name.Controls.Add(this.txt_beneficiary_name, 0, 1);
      this.tlp_ben_name.Controls.Add(this.lbl_beneficiary_last_name1, 1, 0);
      this.tlp_ben_name.Controls.Add(this.txt_beneficiary_last_name1, 1, 1);
      this.tlp_ben_name.Controls.Add(this.lbl_beneficiary_last_name2, 2, 0);
      this.tlp_ben_name.Controls.Add(this.txt_beneficiary_last_name2, 2, 1);
      this.tlp_ben_name.Location = new System.Drawing.Point(4, 8);
      this.tlp_ben_name.Name = "tlp_ben_name";
      this.tlp_ben_name.RowCount = 2;
      this.tlp_ben_name.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_ben_name.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_ben_name.Size = new System.Drawing.Size(887, 68);
      this.tlp_ben_name.TabIndex = 0;
      // 
      // lbl_beneficiary_name
      // 
      this.lbl_beneficiary_name.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_beneficiary_name.BackColor = System.Drawing.Color.Transparent;
      this.lbl_beneficiary_name.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_beneficiary_name.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_beneficiary_name.Location = new System.Drawing.Point(3, 0);
      this.lbl_beneficiary_name.Name = "lbl_beneficiary_name";
      this.lbl_beneficiary_name.Size = new System.Drawing.Size(289, 19);
      this.lbl_beneficiary_name.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_beneficiary_name.TabIndex = 1001;
      this.lbl_beneficiary_name.Text = "xBeneficiaryName3";
      this.lbl_beneficiary_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // txt_beneficiary_name
      // 
      this.txt_beneficiary_name.AllowSpace = true;
      this.txt_beneficiary_name.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
      this.txt_beneficiary_name.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_beneficiary_name.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_beneficiary_name.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_beneficiary_name.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_beneficiary_name.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.txt_beneficiary_name.CornerRadius = 5;
      this.txt_beneficiary_name.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_beneficiary_name.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.BaseMode;
      this.txt_beneficiary_name.Location = new System.Drawing.Point(3, 23);
      this.txt_beneficiary_name.MaxLength = 50;
      this.txt_beneficiary_name.Multiline = false;
      this.txt_beneficiary_name.Name = "txt_beneficiary_name";
      this.txt_beneficiary_name.PasswordChar = '\0';
      this.txt_beneficiary_name.ReadOnly = false;
      this.txt_beneficiary_name.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_beneficiary_name.SelectedText = "";
      this.txt_beneficiary_name.SelectionLength = 0;
      this.txt_beneficiary_name.SelectionStart = 0;
      this.txt_beneficiary_name.Size = new System.Drawing.Size(289, 40);
      this.txt_beneficiary_name.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_beneficiary_name.TabIndex = 0;
      this.txt_beneficiary_name.TabStop = false;
      this.txt_beneficiary_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_beneficiary_name.UseSystemPasswordChar = false;
      this.txt_beneficiary_name.WaterMark = null;
      this.txt_beneficiary_name.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      this.txt_beneficiary_name.TextChanged += new System.EventHandler(this.txt_beneficiary_name_or_last_names_TextChanged);
      // 
      // lbl_beneficiary_last_name1
      // 
      this.lbl_beneficiary_last_name1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_beneficiary_last_name1.BackColor = System.Drawing.Color.Transparent;
      this.lbl_beneficiary_last_name1.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_beneficiary_last_name1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_beneficiary_last_name1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.lbl_beneficiary_last_name1.Location = new System.Drawing.Point(298, 0);
      this.lbl_beneficiary_last_name1.Name = "lbl_beneficiary_last_name1";
      this.lbl_beneficiary_last_name1.Size = new System.Drawing.Size(289, 19);
      this.lbl_beneficiary_last_name1.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_beneficiary_last_name1.TabIndex = 1001;
      this.lbl_beneficiary_last_name1.Text = "xBeneficiaryName1";
      this.lbl_beneficiary_last_name1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // txt_beneficiary_last_name1
      // 
      this.txt_beneficiary_last_name1.AllowSpace = true;
      this.txt_beneficiary_last_name1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
      this.txt_beneficiary_last_name1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_beneficiary_last_name1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_beneficiary_last_name1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_beneficiary_last_name1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_beneficiary_last_name1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.txt_beneficiary_last_name1.CornerRadius = 5;
      this.txt_beneficiary_last_name1.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_beneficiary_last_name1.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.BaseMode;
      this.txt_beneficiary_last_name1.Location = new System.Drawing.Point(298, 23);
      this.txt_beneficiary_last_name1.MaxLength = 50;
      this.txt_beneficiary_last_name1.Multiline = false;
      this.txt_beneficiary_last_name1.Name = "txt_beneficiary_last_name1";
      this.txt_beneficiary_last_name1.PasswordChar = '\0';
      this.txt_beneficiary_last_name1.ReadOnly = false;
      this.txt_beneficiary_last_name1.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_beneficiary_last_name1.SelectedText = "";
      this.txt_beneficiary_last_name1.SelectionLength = 0;
      this.txt_beneficiary_last_name1.SelectionStart = 0;
      this.txt_beneficiary_last_name1.Size = new System.Drawing.Size(289, 40);
      this.txt_beneficiary_last_name1.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_beneficiary_last_name1.TabIndex = 1;
      this.txt_beneficiary_last_name1.TabStop = false;
      this.txt_beneficiary_last_name1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_beneficiary_last_name1.UseSystemPasswordChar = false;
      this.txt_beneficiary_last_name1.WaterMark = null;
      this.txt_beneficiary_last_name1.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      this.txt_beneficiary_last_name1.TextChanged += new System.EventHandler(this.txt_beneficiary_name_or_last_names_TextChanged);
      // 
      // lbl_beneficiary_last_name2
      // 
      this.lbl_beneficiary_last_name2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_beneficiary_last_name2.BackColor = System.Drawing.Color.Transparent;
      this.lbl_beneficiary_last_name2.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_beneficiary_last_name2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_beneficiary_last_name2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.lbl_beneficiary_last_name2.Location = new System.Drawing.Point(593, 0);
      this.lbl_beneficiary_last_name2.Name = "lbl_beneficiary_last_name2";
      this.lbl_beneficiary_last_name2.Size = new System.Drawing.Size(291, 19);
      this.lbl_beneficiary_last_name2.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_beneficiary_last_name2.TabIndex = 1001;
      this.lbl_beneficiary_last_name2.Text = "xBeneficiaryName2";
      this.lbl_beneficiary_last_name2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // txt_beneficiary_last_name2
      // 
      this.txt_beneficiary_last_name2.AllowSpace = true;
      this.txt_beneficiary_last_name2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
      this.txt_beneficiary_last_name2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_beneficiary_last_name2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_beneficiary_last_name2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_beneficiary_last_name2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_beneficiary_last_name2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.txt_beneficiary_last_name2.CornerRadius = 5;
      this.txt_beneficiary_last_name2.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_beneficiary_last_name2.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.BaseMode;
      this.txt_beneficiary_last_name2.Location = new System.Drawing.Point(593, 23);
      this.txt_beneficiary_last_name2.MaxLength = 50;
      this.txt_beneficiary_last_name2.Multiline = false;
      this.txt_beneficiary_last_name2.Name = "txt_beneficiary_last_name2";
      this.txt_beneficiary_last_name2.PasswordChar = '\0';
      this.txt_beneficiary_last_name2.ReadOnly = false;
      this.txt_beneficiary_last_name2.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_beneficiary_last_name2.SelectedText = "";
      this.txt_beneficiary_last_name2.SelectionLength = 0;
      this.txt_beneficiary_last_name2.SelectionStart = 0;
      this.txt_beneficiary_last_name2.Size = new System.Drawing.Size(291, 40);
      this.txt_beneficiary_last_name2.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_beneficiary_last_name2.TabIndex = 2;
      this.txt_beneficiary_last_name2.TabStop = false;
      this.txt_beneficiary_last_name2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_beneficiary_last_name2.UseSystemPasswordChar = false;
      this.txt_beneficiary_last_name2.WaterMark = null;
      this.txt_beneficiary_last_name2.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      this.txt_beneficiary_last_name2.TextChanged += new System.EventHandler(this.txt_beneficiary_name_or_last_names_TextChanged);
      // 
      // tlp_ben_principal1
      // 
      this.tlp_ben_principal1.ColumnCount = 2;
      this.tlp_ben_principal1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
      this.tlp_ben_principal1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 300F));
      this.tlp_ben_principal1.Controls.Add(this.lbl_beneficiary_gender, 0, 1);
      this.tlp_ben_principal1.Controls.Add(this.cb_beneficiary_gender, 1, 1);
      this.tlp_ben_principal1.Controls.Add(this.lbl_beneficiary_occupation, 0, 2);
      this.tlp_ben_principal1.Controls.Add(this.cb_beneficiary_occupation, 1, 2);
      this.tlp_ben_principal1.Controls.Add(this.btn_ben_scan_docs, 1, 0);
      this.tlp_ben_principal1.Location = new System.Drawing.Point(477, 86);
      this.tlp_ben_principal1.Name = "tlp_ben_principal1";
      this.tlp_ben_principal1.RowCount = 5;
      this.tlp_ben_principal1.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_ben_principal1.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_ben_principal1.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_ben_principal1.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_ben_principal1.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_ben_principal1.Size = new System.Drawing.Size(412, 159);
      this.tlp_ben_principal1.TabIndex = 2;
      // 
      // lbl_beneficiary_gender
      // 
      this.lbl_beneficiary_gender.BackColor = System.Drawing.Color.Transparent;
      this.lbl_beneficiary_gender.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_beneficiary_gender.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_beneficiary_gender.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_beneficiary_gender.Location = new System.Drawing.Point(3, 56);
      this.lbl_beneficiary_gender.Name = "lbl_beneficiary_gender";
      this.lbl_beneficiary_gender.Size = new System.Drawing.Size(144, 50);
      this.lbl_beneficiary_gender.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_beneficiary_gender.TabIndex = 1001;
      this.lbl_beneficiary_gender.Text = "xGender";
      this.lbl_beneficiary_gender.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // cb_beneficiary_gender
      // 
      this.cb_beneficiary_gender.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_beneficiary_gender.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.cb_beneficiary_gender.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.cb_beneficiary_gender.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.cb_beneficiary_gender.CornerRadius = 5;
      this.cb_beneficiary_gender.DataSource = null;
      this.cb_beneficiary_gender.DisplayMember = "";
      this.cb_beneficiary_gender.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
      this.cb_beneficiary_gender.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cb_beneficiary_gender.DropDownWidth = 258;
      this.cb_beneficiary_gender.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.cb_beneficiary_gender.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_beneficiary_gender.FormattingEnabled = true;
      this.cb_beneficiary_gender.IsDroppedDown = false;
      this.cb_beneficiary_gender.ItemHeight = 40;
      this.cb_beneficiary_gender.Location = new System.Drawing.Point(153, 59);
      this.cb_beneficiary_gender.MaxDropDownItems = 4;
      this.cb_beneficiary_gender.Name = "cb_beneficiary_gender";
      this.cb_beneficiary_gender.SelectedIndex = -1;
      this.cb_beneficiary_gender.SelectedItem = null;
      this.cb_beneficiary_gender.SelectedValue = null;
      this.cb_beneficiary_gender.SelectionLength = 0;
      this.cb_beneficiary_gender.SelectionStart = 0;
      this.cb_beneficiary_gender.Size = new System.Drawing.Size(258, 40);
      this.cb_beneficiary_gender.Sorted = false;
      this.cb_beneficiary_gender.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.BASIC;
      this.cb_beneficiary_gender.TabIndex = 1;
      this.cb_beneficiary_gender.TabStop = false;
      this.cb_beneficiary_gender.ValueMember = "";
      // 
      // lbl_beneficiary_occupation
      // 
      this.lbl_beneficiary_occupation.BackColor = System.Drawing.Color.Transparent;
      this.lbl_beneficiary_occupation.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_beneficiary_occupation.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_beneficiary_occupation.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_beneficiary_occupation.Location = new System.Drawing.Point(3, 106);
      this.lbl_beneficiary_occupation.Name = "lbl_beneficiary_occupation";
      this.lbl_beneficiary_occupation.Size = new System.Drawing.Size(144, 46);
      this.lbl_beneficiary_occupation.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_beneficiary_occupation.TabIndex = 1001;
      this.lbl_beneficiary_occupation.Text = "xOccupation";
      this.lbl_beneficiary_occupation.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // cb_beneficiary_occupation
      // 
      this.cb_beneficiary_occupation.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_beneficiary_occupation.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.cb_beneficiary_occupation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.cb_beneficiary_occupation.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.cb_beneficiary_occupation.CornerRadius = 5;
      this.cb_beneficiary_occupation.DataSource = null;
      this.cb_beneficiary_occupation.DisplayMember = "";
      this.cb_beneficiary_occupation.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
      this.cb_beneficiary_occupation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
      this.cb_beneficiary_occupation.DropDownWidth = 258;
      this.cb_beneficiary_occupation.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.cb_beneficiary_occupation.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_beneficiary_occupation.FormattingEnabled = true;
      this.cb_beneficiary_occupation.FormatValidator = null;
      this.cb_beneficiary_occupation.IsDroppedDown = false;
      this.cb_beneficiary_occupation.ItemHeight = 40;
      this.cb_beneficiary_occupation.Location = new System.Drawing.Point(153, 109);
      this.cb_beneficiary_occupation.MaxDropDownItems = 4;
      this.cb_beneficiary_occupation.Name = "cb_beneficiary_occupation";
      this.cb_beneficiary_occupation.SelectedIndex = -1;
      this.cb_beneficiary_occupation.SelectedItem = null;
      this.cb_beneficiary_occupation.SelectedValue = null;
      this.cb_beneficiary_occupation.SelectionLength = 0;
      this.cb_beneficiary_occupation.SelectionStart = 0;
      this.cb_beneficiary_occupation.Size = new System.Drawing.Size(258, 40);
      this.cb_beneficiary_occupation.Sorted = false;
      this.cb_beneficiary_occupation.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.BASIC;
      this.cb_beneficiary_occupation.TabIndex = 2;
      this.cb_beneficiary_occupation.TabStop = false;
      this.cb_beneficiary_occupation.ValueMember = "";
      // 
      // btn_ben_scan_docs
      // 
      this.btn_ben_scan_docs.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(47)))), ((int)(((byte)(50)))));
      this.btn_ben_scan_docs.FlatAppearance.BorderSize = 0;
      this.btn_ben_scan_docs.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_ben_scan_docs.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_ben_scan_docs.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_ben_scan_docs.Image = null;
      this.btn_ben_scan_docs.IsSelected = false;
      this.btn_ben_scan_docs.Location = new System.Drawing.Point(153, 3);
      this.btn_ben_scan_docs.Name = "btn_ben_scan_docs";
      this.btn_ben_scan_docs.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_ben_scan_docs.Size = new System.Drawing.Size(258, 50);
      this.btn_ben_scan_docs.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.TERTIARY;
      this.btn_ben_scan_docs.TabIndex = 0;
      this.btn_ben_scan_docs.Text = "XDOCUMENT DIGITIZING";
      this.btn_ben_scan_docs.UseVisualStyleBackColor = false;
      // 
      // tlp_ben_principal
      // 
      this.tlp_ben_principal.ColumnCount = 2;
      this.tlp_ben_principal.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
      this.tlp_ben_principal.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 350F));
      this.tlp_ben_principal.Controls.Add(this.uc_dt_ben_birth, 1, 2);
      this.tlp_ben_principal.Controls.Add(this.lbl_beneficiary_RFC, 0, 0);
      this.tlp_ben_principal.Controls.Add(this.txt_beneficiary_RFC, 1, 0);
      this.tlp_ben_principal.Controls.Add(this.lbl_beneficiary_CURP, 0, 1);
      this.tlp_ben_principal.Controls.Add(this.txt_beneficiary_CURP, 1, 1);
      this.tlp_ben_principal.Controls.Add(this.lbl_beneficiary_birth_date, 0, 2);
      this.tlp_ben_principal.Location = new System.Drawing.Point(3, 86);
      this.tlp_ben_principal.Name = "tlp_ben_principal";
      this.tlp_ben_principal.RowCount = 4;
      this.tlp_ben_principal.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_ben_principal.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_ben_principal.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_ben_principal.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tlp_ben_principal.Size = new System.Drawing.Size(470, 136);
      this.tlp_ben_principal.TabIndex = 1;
      // 
      // uc_dt_ben_birth
      // 
      this.uc_dt_ben_birth.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
      this.uc_dt_ben_birth.DateText = "";
      this.uc_dt_ben_birth.DateTextFont = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.uc_dt_ben_birth.DateTextWidth = 0;
      this.uc_dt_ben_birth.DateValue = new System.DateTime(((long)(0)));
      this.uc_dt_ben_birth.FormatFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
      this.uc_dt_ben_birth.FormatWidth = 135;
      this.uc_dt_ben_birth.Location = new System.Drawing.Point(153, 95);
      this.uc_dt_ben_birth.Name = "uc_dt_ben_birth";
      this.uc_dt_ben_birth.Size = new System.Drawing.Size(324, 40);
      this.uc_dt_ben_birth.TabIndex = 2;
      this.uc_dt_ben_birth.ValuesFont = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
      // 
      // lbl_beneficiary_RFC
      // 
      this.lbl_beneficiary_RFC.BackColor = System.Drawing.Color.Transparent;
      this.lbl_beneficiary_RFC.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_beneficiary_RFC.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_beneficiary_RFC.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_beneficiary_RFC.Location = new System.Drawing.Point(3, 0);
      this.lbl_beneficiary_RFC.Name = "lbl_beneficiary_RFC";
      this.lbl_beneficiary_RFC.Size = new System.Drawing.Size(144, 46);
      this.lbl_beneficiary_RFC.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_beneficiary_RFC.TabIndex = 1001;
      this.lbl_beneficiary_RFC.Text = "xRFC";
      this.lbl_beneficiary_RFC.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // txt_beneficiary_RFC
      // 
      this.txt_beneficiary_RFC.AllowSpace = true;
      this.txt_beneficiary_RFC.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_beneficiary_RFC.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_beneficiary_RFC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_beneficiary_RFC.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_beneficiary_RFC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.txt_beneficiary_RFC.CornerRadius = 5;
      this.txt_beneficiary_RFC.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_beneficiary_RFC.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.ExtendedMode;
      this.txt_beneficiary_RFC.Location = new System.Drawing.Point(153, 3);
      this.txt_beneficiary_RFC.MaxLength = 20;
      this.txt_beneficiary_RFC.Multiline = false;
      this.txt_beneficiary_RFC.Name = "txt_beneficiary_RFC";
      this.txt_beneficiary_RFC.PasswordChar = '\0';
      this.txt_beneficiary_RFC.ReadOnly = false;
      this.txt_beneficiary_RFC.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_beneficiary_RFC.SelectedText = "";
      this.txt_beneficiary_RFC.SelectionLength = 0;
      this.txt_beneficiary_RFC.SelectionStart = 0;
      this.txt_beneficiary_RFC.Size = new System.Drawing.Size(258, 40);
      this.txt_beneficiary_RFC.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_beneficiary_RFC.TabIndex = 0;
      this.txt_beneficiary_RFC.TabStop = false;
      this.txt_beneficiary_RFC.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_beneficiary_RFC.UseSystemPasswordChar = false;
      this.txt_beneficiary_RFC.WaterMark = null;
      this.txt_beneficiary_RFC.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // lbl_beneficiary_CURP
      // 
      this.lbl_beneficiary_CURP.BackColor = System.Drawing.Color.Transparent;
      this.lbl_beneficiary_CURP.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_beneficiary_CURP.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_beneficiary_CURP.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_beneficiary_CURP.Location = new System.Drawing.Point(3, 46);
      this.lbl_beneficiary_CURP.Name = "lbl_beneficiary_CURP";
      this.lbl_beneficiary_CURP.Size = new System.Drawing.Size(144, 46);
      this.lbl_beneficiary_CURP.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_beneficiary_CURP.TabIndex = 1001;
      this.lbl_beneficiary_CURP.Text = "xCURP";
      this.lbl_beneficiary_CURP.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // txt_beneficiary_CURP
      // 
      this.txt_beneficiary_CURP.AllowSpace = true;
      this.txt_beneficiary_CURP.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_beneficiary_CURP.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_beneficiary_CURP.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_beneficiary_CURP.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_beneficiary_CURP.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.txt_beneficiary_CURP.CornerRadius = 5;
      this.txt_beneficiary_CURP.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_beneficiary_CURP.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.ExtendedMode;
      this.txt_beneficiary_CURP.Location = new System.Drawing.Point(153, 49);
      this.txt_beneficiary_CURP.MaxLength = 20;
      this.txt_beneficiary_CURP.Multiline = false;
      this.txt_beneficiary_CURP.Name = "txt_beneficiary_CURP";
      this.txt_beneficiary_CURP.PasswordChar = '\0';
      this.txt_beneficiary_CURP.ReadOnly = false;
      this.txt_beneficiary_CURP.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_beneficiary_CURP.SelectedText = "";
      this.txt_beneficiary_CURP.SelectionLength = 0;
      this.txt_beneficiary_CURP.SelectionStart = 0;
      this.txt_beneficiary_CURP.Size = new System.Drawing.Size(258, 40);
      this.txt_beneficiary_CURP.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_beneficiary_CURP.TabIndex = 1;
      this.txt_beneficiary_CURP.TabStop = false;
      this.txt_beneficiary_CURP.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_beneficiary_CURP.UseSystemPasswordChar = false;
      this.txt_beneficiary_CURP.WaterMark = null;
      this.txt_beneficiary_CURP.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // lbl_beneficiary_birth_date
      // 
      this.lbl_beneficiary_birth_date.BackColor = System.Drawing.Color.Transparent;
      this.lbl_beneficiary_birth_date.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_beneficiary_birth_date.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_beneficiary_birth_date.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_beneficiary_birth_date.Location = new System.Drawing.Point(3, 92);
      this.lbl_beneficiary_birth_date.Name = "lbl_beneficiary_birth_date";
      this.lbl_beneficiary_birth_date.Size = new System.Drawing.Size(144, 46);
      this.lbl_beneficiary_birth_date.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_beneficiary_birth_date.TabIndex = 1001;
      this.lbl_beneficiary_birth_date.Text = "xBirthDay";
      this.lbl_beneficiary_birth_date.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // gb_beneficiary
      // 
      this.gb_beneficiary.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.gb_beneficiary.BackColor = System.Drawing.Color.Transparent;
      this.gb_beneficiary.BorderColor = System.Drawing.Color.Empty;
      this.gb_beneficiary.Controls.Add(this.lbl_beneficiary_full_name);
      this.gb_beneficiary.CornerRadius = 1;
      this.gb_beneficiary.HeaderBackColor = System.Drawing.Color.Empty;
      this.gb_beneficiary.HeaderForeColor = System.Drawing.Color.Empty;
      this.gb_beneficiary.HeaderHeight = 0;
      this.gb_beneficiary.HeaderSubText = null;
      this.gb_beneficiary.HeaderText = null;
      this.gb_beneficiary.Location = new System.Drawing.Point(-7, -16);
      this.gb_beneficiary.Name = "gb_beneficiary";
      this.gb_beneficiary.PanelBackColor = System.Drawing.Color.Empty;
      this.gb_beneficiary.Size = new System.Drawing.Size(0, 0);
      this.gb_beneficiary.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NONE;
      this.gb_beneficiary.TabIndex = 0;
      // 
      // lbl_beneficiary_full_name
      // 
      this.lbl_beneficiary_full_name.BackColor = System.Drawing.Color.Transparent;
      this.lbl_beneficiary_full_name.Cursor = System.Windows.Forms.Cursors.Default;
      this.lbl_beneficiary_full_name.Font = new System.Drawing.Font("Open Sans Semibold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_beneficiary_full_name.ForeColor = System.Drawing.SystemColors.HotTrack;
      this.lbl_beneficiary_full_name.Location = new System.Drawing.Point(28, 79);
      this.lbl_beneficiary_full_name.Name = "lbl_beneficiary_full_name";
      this.lbl_beneficiary_full_name.RightToLeft = System.Windows.Forms.RightToLeft.No;
      this.lbl_beneficiary_full_name.Size = new System.Drawing.Size(870, 20);
      this.lbl_beneficiary_full_name.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.lbl_beneficiary_full_name.TabIndex = 1001;
      this.lbl_beneficiary_full_name.Text = "xFull Name";
      this.lbl_beneficiary_full_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.lbl_beneficiary_full_name.UseMnemonic = false;
      // 
      // points
      // 
      this.points.Controls.Add(this.lbl_points_to_maintain_prefix);
      this.points.Controls.Add(this.lbl_points_to_maintain);
      this.points.Controls.Add(this.lbl_level_prefix);
      this.points.Controls.Add(this.gb_next_level);
      this.points.Controls.Add(this.lbl_level_entered);
      this.points.Controls.Add(this.lbl_points_sufix);
      this.points.Controls.Add(this.lbl_level_entered_prefix);
      this.points.Controls.Add(this.lbl_points);
      this.points.Controls.Add(this.lbl_level_expiration);
      this.points.Controls.Add(this.lbl_points_prefix);
      this.points.Controls.Add(this.lbl_level_expiration_prefix);
      this.points.Controls.Add(this.lbl_level);
      this.points.Location = new System.Drawing.Point(4, 45);
      this.points.Name = "points";
      this.points.Padding = new System.Windows.Forms.Padding(3);
      this.points.Size = new System.Drawing.Size(906, 353);
      this.points.TabIndex = 4;
      this.points.Text = "xPuntos";
      this.points.UseVisualStyleBackColor = true;
      // 
      // lbl_points_to_maintain_prefix
      // 
      this.lbl_points_to_maintain_prefix.BackColor = System.Drawing.Color.Transparent;
      this.lbl_points_to_maintain_prefix.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_points_to_maintain_prefix.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_points_to_maintain_prefix.Location = new System.Drawing.Point(3, 180);
      this.lbl_points_to_maintain_prefix.Name = "lbl_points_to_maintain_prefix";
      this.lbl_points_to_maintain_prefix.Size = new System.Drawing.Size(273, 38);
      this.lbl_points_to_maintain_prefix.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_points_to_maintain_prefix.TabIndex = 69;
      this.lbl_points_to_maintain_prefix.Text = "xPuntos Mantener";
      this.lbl_points_to_maintain_prefix.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_points_to_maintain
      // 
      this.lbl_points_to_maintain.BackColor = System.Drawing.Color.Transparent;
      this.lbl_points_to_maintain.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_points_to_maintain.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_points_to_maintain.Location = new System.Drawing.Point(284, 181);
      this.lbl_points_to_maintain.Name = "lbl_points_to_maintain";
      this.lbl_points_to_maintain.Size = new System.Drawing.Size(125, 38);
      this.lbl_points_to_maintain.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_points_to_maintain.TabIndex = 68;
      this.lbl_points_to_maintain.Text = "---";
      this.lbl_points_to_maintain.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_level_prefix
      // 
      this.lbl_level_prefix.BackColor = System.Drawing.Color.Transparent;
      this.lbl_level_prefix.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_level_prefix.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_level_prefix.Location = new System.Drawing.Point(3, 64);
      this.lbl_level_prefix.Name = "lbl_level_prefix";
      this.lbl_level_prefix.Size = new System.Drawing.Size(273, 38);
      this.lbl_level_prefix.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_level_prefix.TabIndex = 49;
      this.lbl_level_prefix.Text = "xNivel";
      this.lbl_level_prefix.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // gb_next_level
      // 
      this.gb_next_level.BackColor = System.Drawing.Color.Transparent;
      this.gb_next_level.BorderColor = System.Drawing.Color.Empty;
      this.gb_next_level.Controls.Add(this.lbl_next_level_name);
      this.gb_next_level.Controls.Add(this.lbl_points_last_days);
      this.gb_next_level.Controls.Add(this.lbl_points_discretionaries_prefix);
      this.gb_next_level.Controls.Add(this.lbl_points_discretionaries);
      this.gb_next_level.Controls.Add(this.lbl_points_generated_prefix);
      this.gb_next_level.Controls.Add(this.lbl_points_generated);
      this.gb_next_level.Controls.Add(this.lbl_points_to_next_level);
      this.gb_next_level.Controls.Add(this.lbl_points_to_next_level_prefix);
      this.gb_next_level.Controls.Add(this.lbl_points_level_prefix);
      this.gb_next_level.Controls.Add(this.lbl_points_level);
      this.gb_next_level.Controls.Add(this.lbl_points_req_prefix);
      this.gb_next_level.Controls.Add(this.lbl_points_req);
      this.gb_next_level.CornerRadius = 10;
      this.gb_next_level.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_next_level.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gb_next_level.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_next_level.HeaderHeight = 35;
      this.gb_next_level.HeaderSubText = null;
      this.gb_next_level.HeaderText = "XNEXTLEVEL";
      this.gb_next_level.Location = new System.Drawing.Point(503, 46);
      this.gb_next_level.Name = "gb_next_level";
      this.gb_next_level.PanelBackColor = System.Drawing.Color.White;
      this.gb_next_level.Size = new System.Drawing.Size(355, 249);
      this.gb_next_level.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL_WHITE;
      this.gb_next_level.TabIndex = 58;
      this.gb_next_level.Text = "xSiguiente Nivel";
      // 
      // lbl_next_level_name
      // 
      this.lbl_next_level_name.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_next_level_name.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_next_level_name.Location = new System.Drawing.Point(3, 38);
      this.lbl_next_level_name.Name = "lbl_next_level_name";
      this.lbl_next_level_name.Size = new System.Drawing.Size(349, 28);
      this.lbl_next_level_name.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_next_level_name.TabIndex = 69;
      this.lbl_next_level_name.Text = "---";
      this.lbl_next_level_name.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lbl_points_last_days
      // 
      this.lbl_points_last_days.BackColor = System.Drawing.Color.Transparent;
      this.lbl_points_last_days.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_points_last_days.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_points_last_days.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
      this.lbl_points_last_days.Location = new System.Drawing.Point(101, 214);
      this.lbl_points_last_days.Name = "lbl_points_last_days";
      this.lbl_points_last_days.Size = new System.Drawing.Size(248, 20);
      this.lbl_points_last_days.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_points_last_days.TabIndex = 68;
      this.lbl_points_last_days.Text = "*xPuntos obtenidos en los �ltimos XX d�as";
      this.lbl_points_last_days.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_points_discretionaries_prefix
      // 
      this.lbl_points_discretionaries_prefix.BackColor = System.Drawing.Color.Transparent;
      this.lbl_points_discretionaries_prefix.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_points_discretionaries_prefix.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_points_discretionaries_prefix.Location = new System.Drawing.Point(7, 123);
      this.lbl_points_discretionaries_prefix.Name = "lbl_points_discretionaries_prefix";
      this.lbl_points_discretionaries_prefix.Size = new System.Drawing.Size(253, 20);
      this.lbl_points_discretionaries_prefix.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_points_discretionaries_prefix.TabIndex = 66;
      this.lbl_points_discretionaries_prefix.Text = "xPuntos Discrecionales";
      this.lbl_points_discretionaries_prefix.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_points_discretionaries
      // 
      this.lbl_points_discretionaries.BackColor = System.Drawing.Color.Transparent;
      this.lbl_points_discretionaries.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_points_discretionaries.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_points_discretionaries.Location = new System.Drawing.Point(266, 123);
      this.lbl_points_discretionaries.Name = "lbl_points_discretionaries";
      this.lbl_points_discretionaries.Size = new System.Drawing.Size(83, 20);
      this.lbl_points_discretionaries.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_points_discretionaries.TabIndex = 65;
      this.lbl_points_discretionaries.Text = "x10.000";
      this.lbl_points_discretionaries.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_points_generated_prefix
      // 
      this.lbl_points_generated_prefix.BackColor = System.Drawing.Color.Transparent;
      this.lbl_points_generated_prefix.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_points_generated_prefix.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_points_generated_prefix.Location = new System.Drawing.Point(7, 103);
      this.lbl_points_generated_prefix.Name = "lbl_points_generated_prefix";
      this.lbl_points_generated_prefix.Size = new System.Drawing.Size(253, 20);
      this.lbl_points_generated_prefix.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_points_generated_prefix.TabIndex = 64;
      this.lbl_points_generated_prefix.Text = "xPuntos Generados";
      this.lbl_points_generated_prefix.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_points_generated
      // 
      this.lbl_points_generated.BackColor = System.Drawing.Color.Transparent;
      this.lbl_points_generated.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_points_generated.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_points_generated.Location = new System.Drawing.Point(266, 103);
      this.lbl_points_generated.Name = "lbl_points_generated";
      this.lbl_points_generated.Size = new System.Drawing.Size(83, 20);
      this.lbl_points_generated.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_points_generated.TabIndex = 63;
      this.lbl_points_generated.Text = "x10.000";
      this.lbl_points_generated.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_points_to_next_level
      // 
      this.lbl_points_to_next_level.BackColor = System.Drawing.Color.Transparent;
      this.lbl_points_to_next_level.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_points_to_next_level.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_points_to_next_level.Location = new System.Drawing.Point(266, 173);
      this.lbl_points_to_next_level.Name = "lbl_points_to_next_level";
      this.lbl_points_to_next_level.Size = new System.Drawing.Size(84, 38);
      this.lbl_points_to_next_level.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_points_to_next_level.TabIndex = 62;
      this.lbl_points_to_next_level.Text = "---";
      this.lbl_points_to_next_level.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_points_to_next_level_prefix
      // 
      this.lbl_points_to_next_level_prefix.BackColor = System.Drawing.Color.Transparent;
      this.lbl_points_to_next_level_prefix.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_points_to_next_level_prefix.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_points_to_next_level_prefix.Location = new System.Drawing.Point(7, 173);
      this.lbl_points_to_next_level_prefix.Name = "lbl_points_to_next_level_prefix";
      this.lbl_points_to_next_level_prefix.Size = new System.Drawing.Size(253, 38);
      this.lbl_points_to_next_level_prefix.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_points_to_next_level_prefix.TabIndex = 61;
      this.lbl_points_to_next_level_prefix.Text = "xPuntos Faltantes";
      this.lbl_points_to_next_level_prefix.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_points_level_prefix
      // 
      this.lbl_points_level_prefix.BackColor = System.Drawing.Color.Transparent;
      this.lbl_points_level_prefix.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_points_level_prefix.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_points_level_prefix.Location = new System.Drawing.Point(7, 136);
      this.lbl_points_level_prefix.Name = "lbl_points_level_prefix";
      this.lbl_points_level_prefix.Size = new System.Drawing.Size(253, 38);
      this.lbl_points_level_prefix.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_points_level_prefix.TabIndex = 60;
      this.lbl_points_level_prefix.Text = "xPuntos Nivel*";
      this.lbl_points_level_prefix.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_points_level
      // 
      this.lbl_points_level.BackColor = System.Drawing.Color.Transparent;
      this.lbl_points_level.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_points_level.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_points_level.Location = new System.Drawing.Point(269, 136);
      this.lbl_points_level.Name = "lbl_points_level";
      this.lbl_points_level.Size = new System.Drawing.Size(83, 38);
      this.lbl_points_level.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_points_level.TabIndex = 59;
      this.lbl_points_level.Text = "x10.000";
      this.lbl_points_level.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_points_req_prefix
      // 
      this.lbl_points_req_prefix.BackColor = System.Drawing.Color.Transparent;
      this.lbl_points_req_prefix.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_points_req_prefix.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_points_req_prefix.Location = new System.Drawing.Point(6, 67);
      this.lbl_points_req_prefix.Name = "lbl_points_req_prefix";
      this.lbl_points_req_prefix.Size = new System.Drawing.Size(254, 38);
      this.lbl_points_req_prefix.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_points_req_prefix.TabIndex = 58;
      this.lbl_points_req_prefix.Text = "xPuntos Requeridos";
      this.lbl_points_req_prefix.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_points_req
      // 
      this.lbl_points_req.BackColor = System.Drawing.Color.Transparent;
      this.lbl_points_req.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_points_req.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_points_req.Location = new System.Drawing.Point(263, 67);
      this.lbl_points_req.Name = "lbl_points_req";
      this.lbl_points_req.Size = new System.Drawing.Size(86, 38);
      this.lbl_points_req.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_points_req.TabIndex = 57;
      this.lbl_points_req.Text = "---";
      this.lbl_points_req.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_level_entered
      // 
      this.lbl_level_entered.BackColor = System.Drawing.Color.Transparent;
      this.lbl_level_entered.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_level_entered.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_level_entered.Location = new System.Drawing.Point(284, 103);
      this.lbl_level_entered.Name = "lbl_level_entered";
      this.lbl_level_entered.Size = new System.Drawing.Size(221, 38);
      this.lbl_level_entered.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_level_entered.TabIndex = 50;
      this.lbl_level_entered.Text = "x10-10-2010";
      this.lbl_level_entered.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_points_sufix
      // 
      this.lbl_points_sufix.BackColor = System.Drawing.Color.Transparent;
      this.lbl_points_sufix.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_points_sufix.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_points_sufix.Location = new System.Drawing.Point(415, 219);
      this.lbl_points_sufix.Name = "lbl_points_sufix";
      this.lbl_points_sufix.Size = new System.Drawing.Size(93, 38);
      this.lbl_points_sufix.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_points_sufix.TabIndex = 56;
      this.lbl_points_sufix.Text = "xPuntos";
      this.lbl_points_sufix.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_level_entered_prefix
      // 
      this.lbl_level_entered_prefix.BackColor = System.Drawing.Color.Transparent;
      this.lbl_level_entered_prefix.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_level_entered_prefix.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_level_entered_prefix.Location = new System.Drawing.Point(3, 102);
      this.lbl_level_entered_prefix.Name = "lbl_level_entered_prefix";
      this.lbl_level_entered_prefix.Size = new System.Drawing.Size(273, 38);
      this.lbl_level_entered_prefix.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_level_entered_prefix.TabIndex = 51;
      this.lbl_level_entered_prefix.Text = "xFecha Entrada";
      this.lbl_level_entered_prefix.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_points
      // 
      this.lbl_points.BackColor = System.Drawing.Color.Transparent;
      this.lbl_points.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_points.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_points.Location = new System.Drawing.Point(284, 219);
      this.lbl_points.Name = "lbl_points";
      this.lbl_points.Size = new System.Drawing.Size(125, 38);
      this.lbl_points.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_points.TabIndex = 54;
      this.lbl_points.Text = "x10.000";
      this.lbl_points.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_level_expiration
      // 
      this.lbl_level_expiration.BackColor = System.Drawing.Color.Transparent;
      this.lbl_level_expiration.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_level_expiration.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_level_expiration.Location = new System.Drawing.Point(284, 141);
      this.lbl_level_expiration.Name = "lbl_level_expiration";
      this.lbl_level_expiration.Size = new System.Drawing.Size(221, 38);
      this.lbl_level_expiration.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_level_expiration.TabIndex = 52;
      this.lbl_level_expiration.Text = "x10-10-2010";
      this.lbl_level_expiration.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_points_prefix
      // 
      this.lbl_points_prefix.BackColor = System.Drawing.Color.Transparent;
      this.lbl_points_prefix.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_points_prefix.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_points_prefix.Location = new System.Drawing.Point(3, 218);
      this.lbl_points_prefix.Name = "lbl_points_prefix";
      this.lbl_points_prefix.Size = new System.Drawing.Size(273, 38);
      this.lbl_points_prefix.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_points_prefix.TabIndex = 55;
      this.lbl_points_prefix.Text = "xBalance";
      this.lbl_points_prefix.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_level_expiration_prefix
      // 
      this.lbl_level_expiration_prefix.BackColor = System.Drawing.Color.Transparent;
      this.lbl_level_expiration_prefix.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_level_expiration_prefix.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_level_expiration_prefix.Location = new System.Drawing.Point(3, 140);
      this.lbl_level_expiration_prefix.Name = "lbl_level_expiration_prefix";
      this.lbl_level_expiration_prefix.Size = new System.Drawing.Size(273, 38);
      this.lbl_level_expiration_prefix.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_level_expiration_prefix.TabIndex = 53;
      this.lbl_level_expiration_prefix.Text = "xFecha Caducidad";
      this.lbl_level_expiration_prefix.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_level
      // 
      this.lbl_level.BackColor = System.Drawing.Color.Transparent;
      this.lbl_level.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_level.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_level.Location = new System.Drawing.Point(284, 65);
      this.lbl_level.Name = "lbl_level";
      this.lbl_level.Size = new System.Drawing.Size(221, 38);
      this.lbl_level.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_level.TabIndex = 48;
      this.lbl_level.Text = "xSelecci�n";
      this.lbl_level.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // block
      // 
      this.block.Controls.Add(this.pb_unblocked);
      this.block.Controls.Add(this.lbl_block_description);
      this.block.Controls.Add(this.pb_blocked);
      this.block.Controls.Add(this.lbl_blocked);
      this.block.Controls.Add(this.txt_block_description);
      this.block.Location = new System.Drawing.Point(4, 45);
      this.block.Name = "block";
      this.block.Padding = new System.Windows.Forms.Padding(3);
      this.block.Size = new System.Drawing.Size(906, 353);
      this.block.TabIndex = 6;
      this.block.Text = "xBloqueo";
      this.block.UseVisualStyleBackColor = true;
      // 
      // pb_unblocked
      // 
      this.pb_unblocked.Enabled = false;
      this.pb_unblocked.Image = ((System.Drawing.Image)(resources.GetObject("pb_unblocked.Image")));
      this.pb_unblocked.Location = new System.Drawing.Point(26, 17);
      this.pb_unblocked.Name = "pb_unblocked";
      this.pb_unblocked.Size = new System.Drawing.Size(48, 46);
      this.pb_unblocked.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
      this.pb_unblocked.TabIndex = 81;
      this.pb_unblocked.TabStop = false;
      // 
      // lbl_block_description
      // 
      this.lbl_block_description.AutoSize = true;
      this.lbl_block_description.BackColor = System.Drawing.Color.Transparent;
      this.lbl_block_description.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_block_description.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_block_description.Location = new System.Drawing.Point(21, 76);
      this.lbl_block_description.Name = "lbl_block_description";
      this.lbl_block_description.Size = new System.Drawing.Size(123, 19);
      this.lbl_block_description.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_block_description.TabIndex = 78;
      this.lbl_block_description.Text = "xMotivoBloqueo";
      this.lbl_block_description.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // pb_blocked
      // 
      this.pb_blocked.Enabled = false;
      this.pb_blocked.Image = ((System.Drawing.Image)(resources.GetObject("pb_blocked.Image")));
      this.pb_blocked.Location = new System.Drawing.Point(26, 18);
      this.pb_blocked.Name = "pb_blocked";
      this.pb_blocked.Size = new System.Drawing.Size(48, 46);
      this.pb_blocked.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
      this.pb_blocked.TabIndex = 80;
      this.pb_blocked.TabStop = false;
      // 
      // lbl_blocked
      // 
      this.lbl_blocked.AutoSize = true;
      this.lbl_blocked.BackColor = System.Drawing.Color.Transparent;
      this.lbl_blocked.Font = new System.Drawing.Font("Open Sans Semibold", 32F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_blocked.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_blocked.Location = new System.Drawing.Point(80, 21);
      this.lbl_blocked.MaximumSize = new System.Drawing.Size(800, 0);
      this.lbl_blocked.MinimumSize = new System.Drawing.Size(0, 35);
      this.lbl_blocked.Name = "lbl_blocked";
      this.lbl_blocked.Size = new System.Drawing.Size(204, 43);
      this.lbl_blocked.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_32PX_BLACK;
      this.lbl_blocked.TabIndex = 77;
      this.lbl_blocked.Text = "xBloqueado";
      this.lbl_blocked.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // txt_block_description
      // 
      this.txt_block_description.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txt_block_description.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_block_description.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_block_description.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_block_description.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_block_description.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_block_description.CornerRadius = 5;
      this.txt_block_description.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_block_description.Location = new System.Drawing.Point(24, 98);
      this.txt_block_description.MaxLength = 100;
      this.txt_block_description.Multiline = true;
      this.txt_block_description.Name = "txt_block_description";
      this.txt_block_description.PasswordChar = '\0';
      this.txt_block_description.ReadOnly = true;
      this.txt_block_description.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
      this.txt_block_description.SelectedText = "";
      this.txt_block_description.SelectionLength = 0;
      this.txt_block_description.SelectionStart = 0;
      this.txt_block_description.Size = new System.Drawing.Size(0, 263);
      this.txt_block_description.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.MULTILINE;
      this.txt_block_description.TabIndex = 79;
      this.txt_block_description.TabStop = false;
      this.txt_block_description.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_block_description.UseSystemPasswordChar = false;
      this.txt_block_description.WaterMark = null;
      this.txt_block_description.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // comments
      // 
      this.comments.Controls.Add(this.chk_show_comments);
      this.comments.Controls.Add(this.txt_comments);
      this.comments.Location = new System.Drawing.Point(4, 45);
      this.comments.Name = "comments";
      this.comments.Size = new System.Drawing.Size(906, 353);
      this.comments.TabIndex = 3;
      this.comments.Text = "xComentarios";
      this.comments.UseVisualStyleBackColor = true;
      // 
      // chk_show_comments
      // 
      this.chk_show_comments.AutoSize = true;
      this.chk_show_comments.BackColor = System.Drawing.Color.Transparent;
      this.chk_show_comments.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.chk_show_comments.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.chk_show_comments.Location = new System.Drawing.Point(13, 315);
      this.chk_show_comments.MinimumSize = new System.Drawing.Size(364, 30);
      this.chk_show_comments.Name = "chk_show_comments";
      this.chk_show_comments.Padding = new System.Windows.Forms.Padding(5);
      this.chk_show_comments.Size = new System.Drawing.Size(364, 33);
      this.chk_show_comments.TabIndex = 40;
      this.chk_show_comments.Text = "xShow comments when reading player card";
      this.chk_show_comments.UseVisualStyleBackColor = true;
      // 
      // txt_comments
      // 
      this.txt_comments.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_comments.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_comments.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_comments.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_comments.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_comments.CornerRadius = 5;
      this.txt_comments.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_comments.Location = new System.Drawing.Point(13, 12);
      this.txt_comments.MaxLength = 100;
      this.txt_comments.Multiline = true;
      this.txt_comments.Name = "txt_comments";
      this.txt_comments.PasswordChar = '\0';
      this.txt_comments.ReadOnly = false;
      this.txt_comments.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
      this.txt_comments.SelectedText = "";
      this.txt_comments.SelectionLength = 0;
      this.txt_comments.SelectionStart = 0;
      this.txt_comments.Size = new System.Drawing.Size(880, 300);
      this.txt_comments.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.MULTILINE;
      this.txt_comments.TabIndex = 1;
      this.txt_comments.TabStop = false;
      this.txt_comments.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_comments.UseSystemPasswordChar = false;
      this.txt_comments.WaterMark = null;
      this.txt_comments.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      this.txt_comments.Enter += new System.EventHandler(this.txt_comments_Enter);
      this.txt_comments.Leave += new System.EventHandler(this.control_Leave);
      // 
      // pin
      // 
      this.pin.Controls.Add(this.lbl_pin_msg);
      this.pin.Controls.Add(this.txt_confirm_pin);
      this.pin.Controls.Add(this.lbl_pin);
      this.pin.Controls.Add(this.txt_pin);
      this.pin.Controls.Add(this.lbl_confirm_pin);
      this.pin.Controls.Add(this.btn_generate_random_pin);
      this.pin.Controls.Add(this.btn_save_pin);
      this.pin.Location = new System.Drawing.Point(4, 45);
      this.pin.Name = "pin";
      this.pin.Padding = new System.Windows.Forms.Padding(3);
      this.pin.Size = new System.Drawing.Size(906, 353);
      this.pin.TabIndex = 5;
      this.pin.Text = "xPin";
      this.pin.UseVisualStyleBackColor = true;
      // 
      // lbl_pin_msg
      // 
      this.lbl_pin_msg.BackColor = System.Drawing.Color.Transparent;
      this.lbl_pin_msg.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_pin_msg.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_pin_msg.Location = new System.Drawing.Point(422, 59);
      this.lbl_pin_msg.Name = "lbl_pin_msg";
      this.lbl_pin_msg.Size = new System.Drawing.Size(285, 57);
      this.lbl_pin_msg.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_pin_msg.TabIndex = 11;
      this.lbl_pin_msg.Text = "xPinMsg";
      // 
      // txt_confirm_pin
      // 
      this.txt_confirm_pin.AllowSpace = false;
      this.txt_confirm_pin.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_confirm_pin.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_confirm_pin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_confirm_pin.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_confirm_pin.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_confirm_pin.CornerRadius = 5;
      this.txt_confirm_pin.FillWithCeros = false;
      this.txt_confirm_pin.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_confirm_pin.Location = new System.Drawing.Point(226, 86);
      this.txt_confirm_pin.MaxLength = 12;
      this.txt_confirm_pin.Multiline = false;
      this.txt_confirm_pin.Name = "txt_confirm_pin";
      this.txt_confirm_pin.PasswordChar = '*';
      this.txt_confirm_pin.ReadOnly = false;
      this.txt_confirm_pin.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_confirm_pin.SelectedText = "";
      this.txt_confirm_pin.SelectionLength = 0;
      this.txt_confirm_pin.SelectionStart = 0;
      this.txt_confirm_pin.Size = new System.Drawing.Size(163, 40);
      this.txt_confirm_pin.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_confirm_pin.TabIndex = 8;
      this.txt_confirm_pin.TabStop = false;
      this.txt_confirm_pin.Text = "0000";
      this.txt_confirm_pin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.txt_confirm_pin.UseSystemPasswordChar = false;
      this.txt_confirm_pin.WaterMark = null;
      this.txt_confirm_pin.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      this.txt_confirm_pin.TextChanged += new System.EventHandler(this.txt_confirm_pin_TextChanged);
      this.txt_confirm_pin.Enter += new System.EventHandler(this.txt_confirm_pin_Enter);
      // 
      // lbl_pin
      // 
      this.lbl_pin.AutoSize = true;
      this.lbl_pin.BackColor = System.Drawing.Color.Transparent;
      this.lbl_pin.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_pin.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_pin.Location = new System.Drawing.Point(96, 46);
      this.lbl_pin.Name = "lbl_pin";
      this.lbl_pin.Size = new System.Drawing.Size(39, 19);
      this.lbl_pin.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_pin.TabIndex = 6;
      this.lbl_pin.Text = "xPin";
      // 
      // txt_pin
      // 
      this.txt_pin.AllowSpace = false;
      this.txt_pin.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_pin.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_pin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_pin.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_pin.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_pin.CornerRadius = 5;
      this.txt_pin.FillWithCeros = false;
      this.txt_pin.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_pin.Location = new System.Drawing.Point(226, 37);
      this.txt_pin.MaxLength = 12;
      this.txt_pin.Multiline = false;
      this.txt_pin.Name = "txt_pin";
      this.txt_pin.PasswordChar = '*';
      this.txt_pin.ReadOnly = false;
      this.txt_pin.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_pin.SelectedText = "";
      this.txt_pin.SelectionLength = 0;
      this.txt_pin.SelectionStart = 0;
      this.txt_pin.Size = new System.Drawing.Size(163, 40);
      this.txt_pin.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_pin.TabIndex = 5;
      this.txt_pin.TabStop = false;
      this.txt_pin.Text = "0000";
      this.txt_pin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.txt_pin.UseSystemPasswordChar = false;
      this.txt_pin.WaterMark = null;
      this.txt_pin.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      this.txt_pin.TextChanged += new System.EventHandler(this.txt_pin_TextChanged);
      this.txt_pin.Enter += new System.EventHandler(this.txt_pin_Enter);
      // 
      // lbl_confirm_pin
      // 
      this.lbl_confirm_pin.AutoSize = true;
      this.lbl_confirm_pin.BackColor = System.Drawing.Color.Transparent;
      this.lbl_confirm_pin.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_confirm_pin.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_confirm_pin.Location = new System.Drawing.Point(96, 98);
      this.lbl_confirm_pin.Name = "lbl_confirm_pin";
      this.lbl_confirm_pin.Size = new System.Drawing.Size(96, 19);
      this.lbl_confirm_pin.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_confirm_pin.TabIndex = 7;
      this.lbl_confirm_pin.Text = "xConfirmPin";
      // 
      // btn_generate_random_pin
      // 
      this.btn_generate_random_pin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(47)))), ((int)(((byte)(50)))));
      this.btn_generate_random_pin.FlatAppearance.BorderSize = 0;
      this.btn_generate_random_pin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_generate_random_pin.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_generate_random_pin.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_generate_random_pin.Image = null;
      this.btn_generate_random_pin.IsSelected = false;
      this.btn_generate_random_pin.Location = new System.Drawing.Point(426, 143);
      this.btn_generate_random_pin.Name = "btn_generate_random_pin";
      this.btn_generate_random_pin.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_generate_random_pin.Size = new System.Drawing.Size(155, 60);
      this.btn_generate_random_pin.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.TERTIARY;
      this.btn_generate_random_pin.TabIndex = 10;
      this.btn_generate_random_pin.Text = "XGENERATEPIN";
      this.btn_generate_random_pin.UseVisualStyleBackColor = false;
      // 
      // btn_save_pin
      // 
      this.btn_save_pin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(47)))), ((int)(((byte)(50)))));
      this.btn_save_pin.FlatAppearance.BorderSize = 0;
      this.btn_save_pin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_save_pin.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_save_pin.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_save_pin.Image = null;
      this.btn_save_pin.IsSelected = false;
      this.btn_save_pin.Location = new System.Drawing.Point(226, 143);
      this.btn_save_pin.Name = "btn_save_pin";
      this.btn_save_pin.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_save_pin.Size = new System.Drawing.Size(155, 60);
      this.btn_save_pin.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.TERTIARY;
      this.btn_save_pin.TabIndex = 9;
      this.btn_save_pin.Text = "XSAVEPIN";
      this.btn_save_pin.UseVisualStyleBackColor = false;
      // 
      // lbl_no_photo
      // 
      this.lbl_no_photo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_no_photo.Location = new System.Drawing.Point(7, 61);
      this.lbl_no_photo.Name = "lbl_no_photo";
      this.lbl_no_photo.Size = new System.Drawing.Size(924, 13);
      this.lbl_no_photo.TabIndex = 20;
      this.lbl_no_photo.Text = "XNo Photo";
      this.lbl_no_photo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.lbl_no_photo.Click += new System.EventHandler(this.m_img_photo_Click);
      // 
      // lbl_msg_blink
      // 
      this.lbl_msg_blink.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.lbl_msg_blink.BackColor = System.Drawing.Color.Transparent;
      this.lbl_msg_blink.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_msg_blink.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.lbl_msg_blink.Location = new System.Drawing.Point(104, 410);
      this.lbl_msg_blink.Name = "lbl_msg_blink";
      this.lbl_msg_blink.Size = new System.Drawing.Size(907, 23);
      this.lbl_msg_blink.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_RED;
      this.lbl_msg_blink.TabIndex = 2;
      this.lbl_msg_blink.Text = "xPERSONAL NAME CANNOT BE BLANK";
      this.lbl_msg_blink.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.lbl_msg_blink.UseMnemonic = false;
      this.lbl_msg_blink.Visible = false;
      // 
      // btn_keyboard
      // 
      this.btn_keyboard.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.btn_keyboard.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(47)))), ((int)(((byte)(50)))));
      this.btn_keyboard.FlatAppearance.BorderSize = 0;
      this.btn_keyboard.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_keyboard.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_keyboard.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_keyboard.Image = ((System.Drawing.Image)(resources.GetObject("btn_keyboard.Image")));
      this.btn_keyboard.IsSelected = false;
      this.btn_keyboard.Location = new System.Drawing.Point(10, 356);
      this.btn_keyboard.Name = "btn_keyboard";
      this.btn_keyboard.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_keyboard.Size = new System.Drawing.Size(70, 60);
      this.btn_keyboard.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.TERTIARY;
      this.btn_keyboard.TabIndex = 1;
      this.btn_keyboard.TabStop = false;
      this.btn_keyboard.UseVisualStyleBackColor = false;
      this.btn_keyboard.Click += new System.EventHandler(this.btn_keyboard_Click);
      // 
      // btn_cancel
      // 
      this.btn_cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_cancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_cancel.FlatAppearance.BorderSize = 0;
      this.btn_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_cancel.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_cancel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_cancel.Image = null;
      this.btn_cancel.IsSelected = false;
      this.btn_cancel.Location = new System.Drawing.Point(695, 443);
      this.btn_cancel.Name = "btn_cancel";
      this.btn_cancel.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_cancel.Size = new System.Drawing.Size(155, 60);
      this.btn_cancel.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_cancel.TabIndex = 4;
      this.btn_cancel.Text = "XCANCEL";
      this.btn_cancel.UseVisualStyleBackColor = false;
      this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
      // 
      // btn_ok
      // 
      this.btn_ok.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_ok.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_ok.FlatAppearance.BorderSize = 0;
      this.btn_ok.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_ok.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_ok.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_ok.Image = null;
      this.btn_ok.IsSelected = false;
      this.btn_ok.Location = new System.Drawing.Point(856, 443);
      this.btn_ok.Name = "btn_ok";
      this.btn_ok.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_ok.Size = new System.Drawing.Size(155, 60);
      this.btn_ok.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_ok.TabIndex = 3;
      this.btn_ok.Text = "XOK";
      this.btn_ok.UseVisualStyleBackColor = false;
      this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
      // 
      // label13
      // 
      this.label13.BackColor = System.Drawing.Color.Transparent;
      this.label13.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.label13.Location = new System.Drawing.Point(6, 244);
      this.label13.Name = "label13";
      this.label13.Size = new System.Drawing.Size(801, 38);
      this.label13.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.label13.TabIndex = 57;
      this.label13.Text = "xPr�ximoNivel";
      this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // label14
      // 
      this.label14.BackColor = System.Drawing.Color.Transparent;
      this.label14.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.label14.Location = new System.Drawing.Point(304, 144);
      this.label14.Name = "label14";
      this.label14.Size = new System.Drawing.Size(93, 38);
      this.label14.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.label14.TabIndex = 56;
      this.label14.Text = "xPuntos";
      this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // label15
      // 
      this.label15.BackColor = System.Drawing.Color.Transparent;
      this.label15.Font = new System.Drawing.Font("Open Sans Semibold", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.label15.Location = new System.Drawing.Point(202, 143);
      this.label15.Name = "label15";
      this.label15.Size = new System.Drawing.Size(100, 38);
      this.label15.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.label15.TabIndex = 54;
      this.label15.Text = "x10.000";
      this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // label16
      // 
      this.label16.BackColor = System.Drawing.Color.Transparent;
      this.label16.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.label16.Location = new System.Drawing.Point(6, 144);
      this.label16.Name = "label16";
      this.label16.Size = new System.Drawing.Size(190, 38);
      this.label16.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.label16.TabIndex = 55;
      this.label16.Text = "xBalance";
      this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // label17
      // 
      this.label17.BackColor = System.Drawing.Color.Transparent;
      this.label17.Font = new System.Drawing.Font("Open Sans Semibold", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.label17.Location = new System.Drawing.Point(202, 29);
      this.label17.Name = "label17";
      this.label17.Size = new System.Drawing.Size(605, 38);
      this.label17.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.label17.TabIndex = 48;
      this.label17.Text = "xSelecci�n";
      this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // label18
      // 
      this.label18.BackColor = System.Drawing.Color.Transparent;
      this.label18.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.label18.Location = new System.Drawing.Point(6, 106);
      this.label18.Name = "label18";
      this.label18.Size = new System.Drawing.Size(190, 38);
      this.label18.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.label18.TabIndex = 53;
      this.label18.Text = "xFecha Caducidad";
      this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // label19
      // 
      this.label19.BackColor = System.Drawing.Color.Transparent;
      this.label19.Font = new System.Drawing.Font("Open Sans Semibold", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.label19.Location = new System.Drawing.Point(202, 105);
      this.label19.Name = "label19";
      this.label19.Size = new System.Drawing.Size(297, 38);
      this.label19.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.label19.TabIndex = 52;
      this.label19.Text = "x10-10-2010";
      this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // label20
      // 
      this.label20.BackColor = System.Drawing.Color.Transparent;
      this.label20.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.label20.Location = new System.Drawing.Point(6, 68);
      this.label20.Name = "label20";
      this.label20.Size = new System.Drawing.Size(190, 38);
      this.label20.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.label20.TabIndex = 51;
      this.label20.Text = "xFecha Entrada";
      this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // label21
      // 
      this.label21.BackColor = System.Drawing.Color.Transparent;
      this.label21.Font = new System.Drawing.Font("Open Sans Semibold", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.label21.Location = new System.Drawing.Point(202, 67);
      this.label21.Name = "label21";
      this.label21.Size = new System.Drawing.Size(297, 38);
      this.label21.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.label21.TabIndex = 50;
      this.label21.Text = "x10-10-2010";
      this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // label22
      // 
      this.label22.BackColor = System.Drawing.Color.Transparent;
      this.label22.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.label22.Location = new System.Drawing.Point(6, 30);
      this.label22.Name = "label22";
      this.label22.Size = new System.Drawing.Size(190, 38);
      this.label22.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.label22.TabIndex = 49;
      this.label22.Text = "xNivel";
      this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // label23
      // 
      this.label23.BackColor = System.Drawing.Color.Transparent;
      this.label23.Font = new System.Drawing.Font("Open Sans Semibold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.label23.Location = new System.Drawing.Point(383, 103);
      this.label23.Name = "label23";
      this.label23.Size = new System.Drawing.Size(92, 26);
      this.label23.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.label23.TabIndex = 52;
      this.label23.Text = "xDocument";
      this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // comboBox3
      // 
      this.comboBox3.ArrowColor = System.Drawing.Color.Empty;
      this.comboBox3.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.comboBox3.BackColor = System.Drawing.Color.White;
      this.comboBox3.BorderColor = System.Drawing.Color.White;
      this.comboBox3.CornerRadius = 5;
      this.comboBox3.DataSource = null;
      this.comboBox3.DisplayMember = "";
      this.comboBox3.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
      this.comboBox3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.comboBox3.DropDownWidth = 164;
      this.comboBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold);
      this.comboBox3.FormattingEnabled = true;
      this.comboBox3.IsDroppedDown = false;
      this.comboBox3.ItemHeight = 21;
      this.comboBox3.Location = new System.Drawing.Point(142, 99);
      this.comboBox3.MaxDropDownItems = 8;
      this.comboBox3.Name = "comboBox3";
      this.comboBox3.SelectedIndex = -1;
      this.comboBox3.SelectedItem = null;
      this.comboBox3.SelectedValue = null;
      this.comboBox3.SelectionLength = 0;
      this.comboBox3.SelectionStart = 0;
      this.comboBox3.Size = new System.Drawing.Size(164, 44);
      this.comboBox3.Sorted = false;
      this.comboBox3.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.NONE;
      this.comboBox3.TabIndex = 3;
      this.comboBox3.TabStop = false;
      this.comboBox3.ValueMember = "";
      // 
      // label24
      // 
      this.label24.BackColor = System.Drawing.Color.Transparent;
      this.label24.Cursor = System.Windows.Forms.Cursors.Default;
      this.label24.Font = new System.Drawing.Font("Open Sans Semibold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.label24.ForeColor = System.Drawing.SystemColors.HotTrack;
      this.label24.Location = new System.Drawing.Point(21, 76);
      this.label24.Name = "label24";
      this.label24.Size = new System.Drawing.Size(770, 17);
      this.label24.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.label24.TabIndex = 50;
      this.label24.Text = "xFull Name";
      this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.label24.UseMnemonic = false;
      // 
      // label25
      // 
      this.label25.BackColor = System.Drawing.Color.Transparent;
      this.label25.Font = new System.Drawing.Font("Open Sans Semibold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.label25.Location = new System.Drawing.Point(2, 171);
      this.label25.Name = "label25";
      this.label25.Size = new System.Drawing.Size(135, 26);
      this.label25.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.label25.TabIndex = 63;
      this.label25.Text = "xOccupation";
      this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // button1
      // 
      this.button1.CornerRadius = 0;
      this.button1.FlatAppearance.BorderSize = 0;
      this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.button1.Image = null;
      this.button1.IsSelected = false;
      this.button1.Location = new System.Drawing.Point(526, 173);
      this.button1.Name = "button1";
      this.button1.SelectedColor = System.Drawing.Color.Empty;
      this.button1.Size = new System.Drawing.Size(101, 35);
      this.button1.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.NONE;
      this.button1.TabIndex = 62;
      this.button1.Text = "xScanID";
      this.button1.UseVisualStyleBackColor = true;
      // 
      // panel1
      // 
      this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
      this.panel1.Location = new System.Drawing.Point(694, 146);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(110, 139);
      this.panel1.TabIndex = 61;
      // 
      // label26
      // 
      this.label26.BackColor = System.Drawing.Color.Transparent;
      this.label26.Font = new System.Drawing.Font("Open Sans Semibold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.label26.Location = new System.Drawing.Point(368, 254);
      this.label26.Name = "label26";
      this.label26.Size = new System.Drawing.Size(135, 26);
      this.label26.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.label26.TabIndex = 60;
      this.label26.Text = "x(DD/MM/AAAA)";
      this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // label27
      // 
      this.label27.BackColor = System.Drawing.Color.Transparent;
      this.label27.Font = new System.Drawing.Font("Open Sans Semibold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.label27.Location = new System.Drawing.Point(262, 247);
      this.label27.Name = "label27";
      this.label27.Size = new System.Drawing.Size(19, 38);
      this.label27.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.label27.TabIndex = 59;
      this.label27.Text = "/";
      this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // label28
      // 
      this.label28.BackColor = System.Drawing.Color.Transparent;
      this.label28.Font = new System.Drawing.Font("Open Sans Semibold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.label28.Location = new System.Drawing.Point(191, 247);
      this.label28.Name = "label28";
      this.label28.Size = new System.Drawing.Size(19, 38);
      this.label28.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.label28.TabIndex = 58;
      this.label28.Text = "/";
      this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // label29
      // 
      this.label29.BackColor = System.Drawing.Color.Transparent;
      this.label29.Font = new System.Drawing.Font("Open Sans Semibold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.label29.Location = new System.Drawing.Point(2, 255);
      this.label29.Name = "label29";
      this.label29.Size = new System.Drawing.Size(135, 26);
      this.label29.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.label29.TabIndex = 57;
      this.label29.Text = "xBoda";
      this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // label30
      // 
      this.label30.BackColor = System.Drawing.Color.Transparent;
      this.label30.Font = new System.Drawing.Font("Open Sans Semibold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.label30.Location = new System.Drawing.Point(368, 211);
      this.label30.Name = "label30";
      this.label30.Size = new System.Drawing.Size(135, 26);
      this.label30.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.label30.TabIndex = 53;
      this.label30.Text = "x(DD/MM/AAAA)";
      this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // label31
      // 
      this.label31.BackColor = System.Drawing.Color.Transparent;
      this.label31.Font = new System.Drawing.Font("Open Sans Semibold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.label31.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.label31.Location = new System.Drawing.Point(546, 15);
      this.label31.Name = "label31";
      this.label31.Size = new System.Drawing.Size(205, 26);
      this.label31.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.label31.TabIndex = 39;
      this.label31.Text = "xLastName2";
      this.label31.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
      // 
      // label32
      // 
      this.label32.BackColor = System.Drawing.Color.Transparent;
      this.label32.Font = new System.Drawing.Font("Open Sans Semibold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.label32.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.label32.Location = new System.Drawing.Point(280, 15);
      this.label32.Name = "label32";
      this.label32.Size = new System.Drawing.Size(205, 26);
      this.label32.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.label32.TabIndex = 38;
      this.label32.Text = "xLastName1";
      this.label32.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
      // 
      // label33
      // 
      this.label33.BackColor = System.Drawing.Color.Transparent;
      this.label33.Font = new System.Drawing.Font("Open Sans Semibold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.label33.Location = new System.Drawing.Point(18, 15);
      this.label33.Name = "label33";
      this.label33.Size = new System.Drawing.Size(205, 26);
      this.label33.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.label33.TabIndex = 37;
      this.label33.Text = "xName";
      this.label33.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
      // 
      // label34
      // 
      this.label34.BackColor = System.Drawing.Color.Transparent;
      this.label34.Font = new System.Drawing.Font("Open Sans Semibold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.label34.Location = new System.Drawing.Point(262, 204);
      this.label34.Name = "label34";
      this.label34.Size = new System.Drawing.Size(19, 38);
      this.label34.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.label34.TabIndex = 35;
      this.label34.Text = "/";
      this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // label35
      // 
      this.label35.BackColor = System.Drawing.Color.Transparent;
      this.label35.Font = new System.Drawing.Font("Open Sans Semibold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.label35.Location = new System.Drawing.Point(2, 103);
      this.label35.Name = "label35";
      this.label35.Size = new System.Drawing.Size(135, 26);
      this.label35.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.label35.TabIndex = 34;
      this.label35.Text = "xDocumentType";
      this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // comboBox4
      // 
      this.comboBox4.ArrowColor = System.Drawing.Color.Empty;
      this.comboBox4.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.comboBox4.BackColor = System.Drawing.Color.White;
      this.comboBox4.BorderColor = System.Drawing.Color.White;
      this.comboBox4.CornerRadius = 5;
      this.comboBox4.DataSource = null;
      this.comboBox4.DisplayMember = "";
      this.comboBox4.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
      this.comboBox4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.comboBox4.DropDownWidth = 207;
      this.comboBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold);
      this.comboBox4.FormattingEnabled = true;
      this.comboBox4.IsDroppedDown = false;
      this.comboBox4.ItemHeight = 21;
      this.comboBox4.Location = new System.Drawing.Point(481, 135);
      this.comboBox4.MaxDropDownItems = 8;
      this.comboBox4.Name = "comboBox4";
      this.comboBox4.SelectedIndex = -1;
      this.comboBox4.SelectedItem = null;
      this.comboBox4.SelectedValue = null;
      this.comboBox4.SelectionLength = 0;
      this.comboBox4.SelectionStart = 0;
      this.comboBox4.Size = new System.Drawing.Size(207, 44);
      this.comboBox4.Sorted = false;
      this.comboBox4.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.NONE;
      this.comboBox4.TabIndex = 6;
      this.comboBox4.TabStop = false;
      this.comboBox4.ValueMember = "";
      // 
      // label36
      // 
      this.label36.BackColor = System.Drawing.Color.Transparent;
      this.label36.Font = new System.Drawing.Font("Open Sans Semibold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.label36.Location = new System.Drawing.Point(371, 139);
      this.label36.Name = "label36";
      this.label36.Size = new System.Drawing.Size(104, 26);
      this.label36.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.label36.TabIndex = 26;
      this.label36.Text = "xEstado Civil";
      this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // comboBox5
      // 
      this.comboBox5.ArrowColor = System.Drawing.Color.Empty;
      this.comboBox5.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.comboBox5.BackColor = System.Drawing.Color.White;
      this.comboBox5.BorderColor = System.Drawing.Color.White;
      this.comboBox5.CornerRadius = 5;
      this.comboBox5.DataSource = null;
      this.comboBox5.DisplayMember = "";
      this.comboBox5.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
      this.comboBox5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.comboBox5.DropDownWidth = 149;
      this.comboBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold);
      this.comboBox5.FormattingEnabled = true;
      this.comboBox5.IsDroppedDown = false;
      this.comboBox5.ItemHeight = 21;
      this.comboBox5.Location = new System.Drawing.Point(142, 135);
      this.comboBox5.MaxDropDownItems = 8;
      this.comboBox5.Name = "comboBox5";
      this.comboBox5.SelectedIndex = -1;
      this.comboBox5.SelectedItem = null;
      this.comboBox5.SelectedValue = null;
      this.comboBox5.SelectionLength = 0;
      this.comboBox5.SelectionStart = 0;
      this.comboBox5.Size = new System.Drawing.Size(149, 44);
      this.comboBox5.Sorted = false;
      this.comboBox5.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.NONE;
      this.comboBox5.TabIndex = 5;
      this.comboBox5.TabStop = false;
      this.comboBox5.ValueMember = "";
      // 
      // label37
      // 
      this.label37.BackColor = System.Drawing.Color.Transparent;
      this.label37.Font = new System.Drawing.Font("Open Sans Semibold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.label37.Location = new System.Drawing.Point(2, 139);
      this.label37.Name = "label37";
      this.label37.Size = new System.Drawing.Size(135, 26);
      this.label37.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.label37.TabIndex = 24;
      this.label37.Text = "xGenero";
      this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // label38
      // 
      this.label38.BackColor = System.Drawing.Color.Transparent;
      this.label38.Font = new System.Drawing.Font("Open Sans Semibold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.label38.Location = new System.Drawing.Point(191, 204);
      this.label38.Name = "label38";
      this.label38.Size = new System.Drawing.Size(19, 38);
      this.label38.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.label38.TabIndex = 31;
      this.label38.Text = "/";
      this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // label39
      // 
      this.label39.BackColor = System.Drawing.Color.Transparent;
      this.label39.Font = new System.Drawing.Font("Open Sans Semibold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.label39.Location = new System.Drawing.Point(2, 212);
      this.label39.Name = "label39";
      this.label39.Size = new System.Drawing.Size(135, 26);
      this.label39.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.label39.TabIndex = 25;
      this.label39.Text = "xNacimiento";
      this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // button2
      // 
      this.button2.CornerRadius = 0;
      this.button2.FlatAppearance.BorderSize = 0;
      this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.button2.Image = null;
      this.button2.IsSelected = false;
      this.button2.Location = new System.Drawing.Point(255, 68);
      this.button2.Name = "button2";
      this.button2.SelectedColor = System.Drawing.Color.Empty;
      this.button2.Size = new System.Drawing.Size(97, 48);
      this.button2.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.NONE;
      this.button2.TabIndex = 5;
      this.button2.Text = "xScanID2";
      this.button2.UseVisualStyleBackColor = true;
      // 
      // comboBox6
      // 
      this.comboBox6.ArrowColor = System.Drawing.Color.Empty;
      this.comboBox6.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.comboBox6.BackColor = System.Drawing.Color.White;
      this.comboBox6.BorderColor = System.Drawing.Color.White;
      this.comboBox6.CornerRadius = 5;
      this.comboBox6.DataSource = null;
      this.comboBox6.DisplayMember = "";
      this.comboBox6.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
      this.comboBox6.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.comboBox6.DropDownWidth = 215;
      this.comboBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold);
      this.comboBox6.FormattingEnabled = true;
      this.comboBox6.IsDroppedDown = false;
      this.comboBox6.ItemHeight = 21;
      this.comboBox6.Location = new System.Drawing.Point(138, 27);
      this.comboBox6.MaxDropDownItems = 8;
      this.comboBox6.Name = "comboBox6";
      this.comboBox6.SelectedIndex = -1;
      this.comboBox6.SelectedItem = null;
      this.comboBox6.SelectedValue = null;
      this.comboBox6.SelectionLength = 0;
      this.comboBox6.SelectionStart = 0;
      this.comboBox6.Size = new System.Drawing.Size(215, 44);
      this.comboBox6.Sorted = false;
      this.comboBox6.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.NONE;
      this.comboBox6.TabIndex = 1;
      this.comboBox6.TabStop = false;
      this.comboBox6.ValueMember = "";
      // 
      // label10
      // 
      this.label10.BackColor = System.Drawing.Color.Transparent;
      this.label10.Font = new System.Drawing.Font("Open Sans Semibold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.label10.Location = new System.Drawing.Point(12, 31);
      this.label10.Name = "label10";
      this.label10.Size = new System.Drawing.Size(120, 26);
      this.label10.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.label10.TabIndex = 0;
      this.label10.Text = "xDocumentType";
      this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // button3
      // 
      this.button3.CornerRadius = 0;
      this.button3.FlatAppearance.BorderSize = 0;
      this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.button3.Image = null;
      this.button3.IsSelected = false;
      this.button3.Location = new System.Drawing.Point(372, 15);
      this.button3.Name = "button3";
      this.button3.SelectedColor = System.Drawing.Color.Empty;
      this.button3.Size = new System.Drawing.Size(97, 48);
      this.button3.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.NONE;
      this.button3.TabIndex = 3;
      this.button3.Text = "xViewID";
      this.button3.UseVisualStyleBackColor = true;
      // 
      // button4
      // 
      this.button4.CornerRadius = 0;
      this.button4.FlatAppearance.BorderSize = 0;
      this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.button4.Image = null;
      this.button4.IsSelected = false;
      this.button4.Location = new System.Drawing.Point(372, 69);
      this.button4.Name = "button4";
      this.button4.SelectedColor = System.Drawing.Color.Empty;
      this.button4.Size = new System.Drawing.Size(97, 48);
      this.button4.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.NONE;
      this.button4.TabIndex = 4;
      this.button4.Text = "xDeleteID";
      this.button4.UseVisualStyleBackColor = true;
      // 
      // button5
      // 
      this.button5.CornerRadius = 0;
      this.button5.FlatAppearance.BorderSize = 0;
      this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.button5.Image = null;
      this.button5.IsSelected = false;
      this.button5.Location = new System.Drawing.Point(138, 69);
      this.button5.Name = "button5";
      this.button5.SelectedColor = System.Drawing.Color.Empty;
      this.button5.Size = new System.Drawing.Size(97, 48);
      this.button5.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.NONE;
      this.button5.TabIndex = 2;
      this.button5.Text = "xScanID1";
      this.button5.UseVisualStyleBackColor = true;
      // 
      // numericTextBox1
      // 
      this.numericTextBox1.AllowSpace = false;
      this.numericTextBox1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.numericTextBox1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.numericTextBox1.BackColor = System.Drawing.Color.White;
      this.numericTextBox1.BorderColor = System.Drawing.Color.Empty;
      this.numericTextBox1.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.numericTextBox1.CornerRadius = 0;
      this.numericTextBox1.FillWithCeros = false;
      this.numericTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.numericTextBox1.Location = new System.Drawing.Point(390, 130);
      this.numericTextBox1.MaxLength = 4;
      this.numericTextBox1.Multiline = false;
      this.numericTextBox1.Name = "numericTextBox1";
      this.numericTextBox1.PasswordChar = '\0';
      this.numericTextBox1.ReadOnly = false;
      this.numericTextBox1.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.numericTextBox1.SelectedText = "";
      this.numericTextBox1.SelectionLength = 0;
      this.numericTextBox1.SelectionStart = 0;
      this.numericTextBox1.Size = new System.Drawing.Size(86, 38);
      this.numericTextBox1.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.NONE;
      this.numericTextBox1.TabIndex = 5;
      this.numericTextBox1.TabStop = false;
      this.numericTextBox1.Text = "1984";
      this.numericTextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.numericTextBox1.UseSystemPasswordChar = false;
      this.numericTextBox1.WaterMark = null;
      this.numericTextBox1.WaterMarkColor = System.Drawing.SystemColors.WindowText;
      // 
      // numericTextBox2
      // 
      this.numericTextBox2.AllowSpace = false;
      this.numericTextBox2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.numericTextBox2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.numericTextBox2.BackColor = System.Drawing.Color.White;
      this.numericTextBox2.BorderColor = System.Drawing.Color.Empty;
      this.numericTextBox2.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.numericTextBox2.CornerRadius = 0;
      this.numericTextBox2.FillWithCeros = false;
      this.numericTextBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.numericTextBox2.Location = new System.Drawing.Point(156, 130);
      this.numericTextBox2.MaxLength = 2;
      this.numericTextBox2.Multiline = false;
      this.numericTextBox2.Name = "numericTextBox2";
      this.numericTextBox2.PasswordChar = '\0';
      this.numericTextBox2.ReadOnly = false;
      this.numericTextBox2.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.numericTextBox2.SelectedText = "";
      this.numericTextBox2.SelectionLength = 0;
      this.numericTextBox2.SelectionStart = 0;
      this.numericTextBox2.Size = new System.Drawing.Size(86, 38);
      this.numericTextBox2.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.NONE;
      this.numericTextBox2.TabIndex = 3;
      this.numericTextBox2.TabStop = false;
      this.numericTextBox2.Text = "06";
      this.numericTextBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.numericTextBox2.UseSystemPasswordChar = false;
      this.numericTextBox2.WaterMark = null;
      this.numericTextBox2.WaterMarkColor = System.Drawing.SystemColors.WindowText;
      // 
      // numericTextBox3
      // 
      this.numericTextBox3.AllowSpace = false;
      this.numericTextBox3.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.numericTextBox3.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.numericTextBox3.BackColor = System.Drawing.Color.White;
      this.numericTextBox3.BorderColor = System.Drawing.Color.Empty;
      this.numericTextBox3.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.numericTextBox3.CornerRadius = 0;
      this.numericTextBox3.FillWithCeros = false;
      this.numericTextBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.numericTextBox3.Location = new System.Drawing.Point(273, 130);
      this.numericTextBox3.MaxLength = 2;
      this.numericTextBox3.Multiline = false;
      this.numericTextBox3.Name = "numericTextBox3";
      this.numericTextBox3.PasswordChar = '\0';
      this.numericTextBox3.ReadOnly = false;
      this.numericTextBox3.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.numericTextBox3.SelectedText = "";
      this.numericTextBox3.SelectionLength = 0;
      this.numericTextBox3.SelectionStart = 0;
      this.numericTextBox3.Size = new System.Drawing.Size(86, 38);
      this.numericTextBox3.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.NONE;
      this.numericTextBox3.TabIndex = 4;
      this.numericTextBox3.TabStop = false;
      this.numericTextBox3.Text = "11";
      this.numericTextBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.numericTextBox3.UseSystemPasswordChar = false;
      this.numericTextBox3.WaterMark = null;
      this.numericTextBox3.WaterMarkColor = System.Drawing.SystemColors.WindowText;
      // 
      // characterTextBox1
      // 
      this.characterTextBox1.AllowSpace = true;
      this.characterTextBox1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.characterTextBox1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.characterTextBox1.BackColor = System.Drawing.Color.White;
      this.characterTextBox1.BorderColor = System.Drawing.Color.Empty;
      this.characterTextBox1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.characterTextBox1.CornerRadius = 0;
      this.characterTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold);
      this.characterTextBox1.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.BaseMode;
      this.characterTextBox1.Location = new System.Drawing.Point(540, 44);
      this.characterTextBox1.MaxLength = 50;
      this.characterTextBox1.Multiline = false;
      this.characterTextBox1.Name = "characterTextBox1";
      this.characterTextBox1.PasswordChar = '\0';
      this.characterTextBox1.ReadOnly = false;
      this.characterTextBox1.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.characterTextBox1.SelectedText = "";
      this.characterTextBox1.SelectionLength = 0;
      this.characterTextBox1.SelectionStart = 0;
      this.characterTextBox1.Size = new System.Drawing.Size(251, 29);
      this.characterTextBox1.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.NONE;
      this.characterTextBox1.TabIndex = 2;
      this.characterTextBox1.TabStop = false;
      this.characterTextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.characterTextBox1.UseSystemPasswordChar = false;
      this.characterTextBox1.WaterMark = null;
      this.characterTextBox1.WaterMarkColor = System.Drawing.SystemColors.WindowText;
      // 
      // characterTextBox2
      // 
      this.characterTextBox2.AllowSpace = true;
      this.characterTextBox2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.characterTextBox2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.characterTextBox2.BackColor = System.Drawing.Color.White;
      this.characterTextBox2.BorderColor = System.Drawing.Color.Empty;
      this.characterTextBox2.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.characterTextBox2.CornerRadius = 0;
      this.characterTextBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold);
      this.characterTextBox2.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.BaseMode;
      this.characterTextBox2.Location = new System.Drawing.Point(142, 171);
      this.characterTextBox2.MaxLength = 50;
      this.characterTextBox2.Multiline = false;
      this.characterTextBox2.Name = "characterTextBox2";
      this.characterTextBox2.PasswordChar = '\0';
      this.characterTextBox2.ReadOnly = false;
      this.characterTextBox2.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.characterTextBox2.SelectedText = "";
      this.characterTextBox2.SelectionLength = 0;
      this.characterTextBox2.SelectionStart = 0;
      this.characterTextBox2.Size = new System.Drawing.Size(333, 29);
      this.characterTextBox2.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.NONE;
      this.characterTextBox2.TabIndex = 64;
      this.characterTextBox2.TabStop = false;
      this.characterTextBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.characterTextBox2.UseSystemPasswordChar = false;
      this.characterTextBox2.WaterMark = null;
      this.characterTextBox2.WaterMarkColor = System.Drawing.SystemColors.WindowText;
      // 
      // numericTextBox4
      // 
      this.numericTextBox4.AllowSpace = false;
      this.numericTextBox4.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.numericTextBox4.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.numericTextBox4.BackColor = System.Drawing.Color.White;
      this.numericTextBox4.BorderColor = System.Drawing.Color.Empty;
      this.numericTextBox4.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.numericTextBox4.CornerRadius = 0;
      this.numericTextBox4.FillWithCeros = false;
      this.numericTextBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold);
      this.numericTextBox4.Location = new System.Drawing.Point(282, 251);
      this.numericTextBox4.MaxLength = 4;
      this.numericTextBox4.Multiline = false;
      this.numericTextBox4.Name = "numericTextBox4";
      this.numericTextBox4.PasswordChar = '\0';
      this.numericTextBox4.ReadOnly = false;
      this.numericTextBox4.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.numericTextBox4.SelectedText = "";
      this.numericTextBox4.SelectionLength = 0;
      this.numericTextBox4.SelectionStart = 0;
      this.numericTextBox4.Size = new System.Drawing.Size(68, 29);
      this.numericTextBox4.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.NONE;
      this.numericTextBox4.TabIndex = 56;
      this.numericTextBox4.TabStop = false;
      this.numericTextBox4.Text = "1984";
      this.numericTextBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.numericTextBox4.UseSystemPasswordChar = false;
      this.numericTextBox4.WaterMark = null;
      this.numericTextBox4.WaterMarkColor = System.Drawing.SystemColors.WindowText;
      // 
      // numericTextBox5
      // 
      this.numericTextBox5.AllowSpace = false;
      this.numericTextBox5.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.numericTextBox5.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.numericTextBox5.BackColor = System.Drawing.Color.White;
      this.numericTextBox5.BorderColor = System.Drawing.Color.Empty;
      this.numericTextBox5.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.numericTextBox5.CornerRadius = 0;
      this.numericTextBox5.FillWithCeros = true;
      this.numericTextBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold);
      this.numericTextBox5.Location = new System.Drawing.Point(142, 251);
      this.numericTextBox5.MaxLength = 2;
      this.numericTextBox5.Multiline = false;
      this.numericTextBox5.Name = "numericTextBox5";
      this.numericTextBox5.PasswordChar = '\0';
      this.numericTextBox5.ReadOnly = false;
      this.numericTextBox5.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.numericTextBox5.SelectedText = "";
      this.numericTextBox5.SelectionLength = 0;
      this.numericTextBox5.SelectionStart = 0;
      this.numericTextBox5.Size = new System.Drawing.Size(49, 29);
      this.numericTextBox5.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.NONE;
      this.numericTextBox5.TabIndex = 54;
      this.numericTextBox5.TabStop = false;
      this.numericTextBox5.Text = "06";
      this.numericTextBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.numericTextBox5.UseSystemPasswordChar = false;
      this.numericTextBox5.WaterMark = null;
      this.numericTextBox5.WaterMarkColor = System.Drawing.SystemColors.WindowText;
      // 
      // numericTextBox6
      // 
      this.numericTextBox6.AllowSpace = false;
      this.numericTextBox6.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.numericTextBox6.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.numericTextBox6.BackColor = System.Drawing.Color.White;
      this.numericTextBox6.BorderColor = System.Drawing.Color.Empty;
      this.numericTextBox6.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.numericTextBox6.CornerRadius = 0;
      this.numericTextBox6.FillWithCeros = true;
      this.numericTextBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold);
      this.numericTextBox6.Location = new System.Drawing.Point(213, 251);
      this.numericTextBox6.MaxLength = 2;
      this.numericTextBox6.Multiline = false;
      this.numericTextBox6.Name = "numericTextBox6";
      this.numericTextBox6.PasswordChar = '\0';
      this.numericTextBox6.ReadOnly = false;
      this.numericTextBox6.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.numericTextBox6.SelectedText = "";
      this.numericTextBox6.SelectionLength = 0;
      this.numericTextBox6.SelectionStart = 0;
      this.numericTextBox6.Size = new System.Drawing.Size(49, 29);
      this.numericTextBox6.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.NONE;
      this.numericTextBox6.TabIndex = 55;
      this.numericTextBox6.TabStop = false;
      this.numericTextBox6.Text = "11";
      this.numericTextBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.numericTextBox6.UseSystemPasswordChar = false;
      this.numericTextBox6.WaterMark = null;
      this.numericTextBox6.WaterMarkColor = System.Drawing.SystemColors.WindowText;
      // 
      // characterTextBox3
      // 
      this.characterTextBox3.AllowSpace = true;
      this.characterTextBox3.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.characterTextBox3.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.characterTextBox3.BackColor = System.Drawing.Color.White;
      this.characterTextBox3.BorderColor = System.Drawing.Color.Empty;
      this.characterTextBox3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.characterTextBox3.CornerRadius = 0;
      this.characterTextBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold);
      this.characterTextBox3.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.BaseMode;
      this.characterTextBox3.Location = new System.Drawing.Point(278, 44);
      this.characterTextBox3.MaxLength = 50;
      this.characterTextBox3.Multiline = false;
      this.characterTextBox3.Name = "characterTextBox3";
      this.characterTextBox3.PasswordChar = '\0';
      this.characterTextBox3.ReadOnly = false;
      this.characterTextBox3.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.characterTextBox3.SelectedText = "";
      this.characterTextBox3.SelectionLength = 0;
      this.characterTextBox3.SelectionStart = 0;
      this.characterTextBox3.Size = new System.Drawing.Size(251, 29);
      this.characterTextBox3.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.NONE;
      this.characterTextBox3.TabIndex = 1;
      this.characterTextBox3.TabStop = false;
      this.characterTextBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.characterTextBox3.UseSystemPasswordChar = false;
      this.characterTextBox3.WaterMark = null;
      this.characterTextBox3.WaterMarkColor = System.Drawing.SystemColors.WindowText;
      // 
      // characterTextBox4
      // 
      this.characterTextBox4.AllowSpace = true;
      this.characterTextBox4.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.characterTextBox4.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.characterTextBox4.BackColor = System.Drawing.Color.White;
      this.characterTextBox4.BorderColor = System.Drawing.Color.Empty;
      this.characterTextBox4.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.characterTextBox4.CornerRadius = 0;
      this.characterTextBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold);
      this.characterTextBox4.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.BaseMode;
      this.characterTextBox4.Location = new System.Drawing.Point(16, 44);
      this.characterTextBox4.MaxLength = 50;
      this.characterTextBox4.Multiline = false;
      this.characterTextBox4.Name = "characterTextBox4";
      this.characterTextBox4.PasswordChar = '\0';
      this.characterTextBox4.ReadOnly = false;
      this.characterTextBox4.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.characterTextBox4.SelectedText = "";
      this.characterTextBox4.SelectionLength = 0;
      this.characterTextBox4.SelectionStart = 0;
      this.characterTextBox4.Size = new System.Drawing.Size(251, 29);
      this.characterTextBox4.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.NONE;
      this.characterTextBox4.TabIndex = 0;
      this.characterTextBox4.TabStop = false;
      this.characterTextBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.characterTextBox4.UseSystemPasswordChar = false;
      this.characterTextBox4.WaterMark = null;
      this.characterTextBox4.WaterMarkColor = System.Drawing.SystemColors.WindowText;
      // 
      // characterTextBox5
      // 
      this.characterTextBox5.AllowSpace = true;
      this.characterTextBox5.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.characterTextBox5.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.characterTextBox5.BackColor = System.Drawing.Color.White;
      this.characterTextBox5.BorderColor = System.Drawing.Color.Empty;
      this.characterTextBox5.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.characterTextBox5.CornerRadius = 0;
      this.characterTextBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold);
      this.characterTextBox5.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.ExtendedMode;
      this.characterTextBox5.Location = new System.Drawing.Point(481, 103);
      this.characterTextBox5.MaxLength = 20;
      this.characterTextBox5.Multiline = false;
      this.characterTextBox5.Name = "characterTextBox5";
      this.characterTextBox5.PasswordChar = '\0';
      this.characterTextBox5.ReadOnly = false;
      this.characterTextBox5.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.characterTextBox5.SelectedText = "";
      this.characterTextBox5.SelectionLength = 0;
      this.characterTextBox5.SelectionStart = 0;
      this.characterTextBox5.Size = new System.Drawing.Size(245, 29);
      this.characterTextBox5.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.NONE;
      this.characterTextBox5.TabIndex = 4;
      this.characterTextBox5.TabStop = false;
      this.characterTextBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.characterTextBox5.UseSystemPasswordChar = false;
      this.characterTextBox5.WaterMark = null;
      this.characterTextBox5.WaterMarkColor = System.Drawing.SystemColors.WindowText;
      // 
      // numericTextBox7
      // 
      this.numericTextBox7.AllowSpace = false;
      this.numericTextBox7.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.numericTextBox7.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.numericTextBox7.BackColor = System.Drawing.Color.White;
      this.numericTextBox7.BorderColor = System.Drawing.Color.Empty;
      this.numericTextBox7.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.numericTextBox7.CornerRadius = 0;
      this.numericTextBox7.FillWithCeros = false;
      this.numericTextBox7.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold);
      this.numericTextBox7.Location = new System.Drawing.Point(282, 208);
      this.numericTextBox7.MaxLength = 4;
      this.numericTextBox7.Multiline = false;
      this.numericTextBox7.Name = "numericTextBox7";
      this.numericTextBox7.PasswordChar = '\0';
      this.numericTextBox7.ReadOnly = false;
      this.numericTextBox7.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.numericTextBox7.SelectedText = "";
      this.numericTextBox7.SelectionLength = 0;
      this.numericTextBox7.SelectionStart = 0;
      this.numericTextBox7.Size = new System.Drawing.Size(68, 29);
      this.numericTextBox7.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.NONE;
      this.numericTextBox7.TabIndex = 9;
      this.numericTextBox7.TabStop = false;
      this.numericTextBox7.Text = "1984";
      this.numericTextBox7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.numericTextBox7.UseSystemPasswordChar = false;
      this.numericTextBox7.WaterMark = null;
      this.numericTextBox7.WaterMarkColor = System.Drawing.SystemColors.WindowText;
      // 
      // numericTextBox8
      // 
      this.numericTextBox8.AllowSpace = false;
      this.numericTextBox8.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.numericTextBox8.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.numericTextBox8.BackColor = System.Drawing.Color.White;
      this.numericTextBox8.BorderColor = System.Drawing.Color.Empty;
      this.numericTextBox8.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.numericTextBox8.CornerRadius = 0;
      this.numericTextBox8.FillWithCeros = true;
      this.numericTextBox8.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold);
      this.numericTextBox8.Location = new System.Drawing.Point(142, 208);
      this.numericTextBox8.MaxLength = 2;
      this.numericTextBox8.Multiline = false;
      this.numericTextBox8.Name = "numericTextBox8";
      this.numericTextBox8.PasswordChar = '\0';
      this.numericTextBox8.ReadOnly = false;
      this.numericTextBox8.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.numericTextBox8.SelectedText = "";
      this.numericTextBox8.SelectionLength = 0;
      this.numericTextBox8.SelectionStart = 0;
      this.numericTextBox8.Size = new System.Drawing.Size(49, 29);
      this.numericTextBox8.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.NONE;
      this.numericTextBox8.TabIndex = 7;
      this.numericTextBox8.TabStop = false;
      this.numericTextBox8.Text = "06";
      this.numericTextBox8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.numericTextBox8.UseSystemPasswordChar = false;
      this.numericTextBox8.WaterMark = null;
      this.numericTextBox8.WaterMarkColor = System.Drawing.SystemColors.WindowText;
      // 
      // numericTextBox9
      // 
      this.numericTextBox9.AllowSpace = false;
      this.numericTextBox9.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.numericTextBox9.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.numericTextBox9.BackColor = System.Drawing.Color.White;
      this.numericTextBox9.BorderColor = System.Drawing.Color.Empty;
      this.numericTextBox9.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.numericTextBox9.CornerRadius = 0;
      this.numericTextBox9.FillWithCeros = true;
      this.numericTextBox9.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold);
      this.numericTextBox9.Location = new System.Drawing.Point(213, 208);
      this.numericTextBox9.MaxLength = 2;
      this.numericTextBox9.Multiline = false;
      this.numericTextBox9.Name = "numericTextBox9";
      this.numericTextBox9.PasswordChar = '\0';
      this.numericTextBox9.ReadOnly = false;
      this.numericTextBox9.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.numericTextBox9.SelectedText = "";
      this.numericTextBox9.SelectionLength = 0;
      this.numericTextBox9.SelectionStart = 0;
      this.numericTextBox9.Size = new System.Drawing.Size(49, 29);
      this.numericTextBox9.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.NONE;
      this.numericTextBox9.TabIndex = 8;
      this.numericTextBox9.TabStop = false;
      this.numericTextBox9.Text = "11";
      this.numericTextBox9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.numericTextBox9.UseSystemPasswordChar = false;
      this.numericTextBox9.WaterMark = null;
      this.numericTextBox9.WaterMarkColor = System.Drawing.SystemColors.WindowText;
      // 
      // lbl_full_name
      // 
      this.lbl_full_name.BackColor = System.Drawing.Color.Transparent;
      this.lbl_full_name.Cursor = System.Windows.Forms.Cursors.Default;
      this.lbl_full_name.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_full_name.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_full_name.Location = new System.Drawing.Point(25, 325);
      this.lbl_full_name.Name = "lbl_full_name";
      this.lbl_full_name.Size = new System.Drawing.Size(870, 20);
      this.lbl_full_name.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLUE;
      this.lbl_full_name.TabIndex = 1001;
      this.lbl_full_name.Text = "xFull Name";
      this.lbl_full_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.lbl_full_name.UseMnemonic = false;
      this.lbl_full_name.Visible = false;
      // 
      // btn_reg_entry1
      // 
      this.btn_reg_entry1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_reg_entry1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(47)))), ((int)(((byte)(50)))));
      this.btn_reg_entry1.FlatAppearance.BorderSize = 0;
      this.btn_reg_entry1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_reg_entry1.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_reg_entry1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_reg_entry1.Image = null;
      this.btn_reg_entry1.IsSelected = false;
      this.btn_reg_entry1.Location = new System.Drawing.Point(295, 443);
      this.btn_reg_entry1.Name = "btn_reg_entry1";
      this.btn_reg_entry1.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_reg_entry1.Size = new System.Drawing.Size(110, 60);
      this.btn_reg_entry1.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.TERTIARY;
      this.btn_reg_entry1.TabIndex = 19;
      this.btn_reg_entry1.Text = "XENTRY REGISTRY";
      this.btn_reg_entry1.UseVisualStyleBackColor = false;
      this.btn_reg_entry1.Click += new System.EventHandler(this.btn_reg_entry_Click);
      // 
      // frm_player_edit
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.ClientSize = new System.Drawing.Size(1024, 567);
      this.ControlBox = false;
      this.CustomLocation = true;
      this.KeyPreview = true;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "frm_player_edit";
      this.ShowIcon = false;
      this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
      this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
      this.Style = WSI.Cashier.Controls.frm_base.BaseFormStyle.PRINCIPAL;
      this.Text = "frm_mb_account_user_edit";
      this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_player_edit_FormClosing);
      this.Load += new System.EventHandler(this.frm_player_edit_Load);
      this.Shown += new System.EventHandler(this.frm_player_edit_Shown);
      this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frm_player_edit_KeyDown);
      this.pnl_data.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.m_img_photo)).EndInit();
      this.tab_player_edit.ResumeLayout(false);
      this.principal.ResumeLayout(false);
      this.tlp_principal1.ResumeLayout(false);
      this.tlp_name.ResumeLayout(false);
      this.tlp_principal.ResumeLayout(false);
      this.contact.ResumeLayout(false);
      this.tlp_contact.ResumeLayout(false);
      this.address.ResumeLayout(false);
      this.address.PerformLayout();
      this.tlp_address_new.ResumeLayout(false);
      this.tlp_address.ResumeLayout(false);
      this.beneficiary.ResumeLayout(false);
      this.beneficiary.PerformLayout();
      this.tlp_ben_name.ResumeLayout(false);
      this.tlp_ben_principal1.ResumeLayout(false);
      this.tlp_ben_principal.ResumeLayout(false);
      this.gb_beneficiary.ResumeLayout(false);
      this.points.ResumeLayout(false);
      this.gb_next_level.ResumeLayout(false);
      this.block.ResumeLayout(false);
      this.block.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pb_unblocked)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.pb_blocked)).EndInit();
      this.comments.ResumeLayout(false);
      this.comments.PerformLayout();
      this.pin.ResumeLayout(false);
      this.pin.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private WSI.Cashier.Controls.uc_round_button btn_cancel;
    private WSI.Cashier.Controls.uc_round_button btn_ok;
    private WSI.Cashier.Controls.uc_round_button btn_keyboard;
    internal System.Windows.Forms.Timer timer1;
    private WSI.Cashier.Controls.uc_label lbl_msg_blink;
    private WSI.Cashier.Controls.uc_round_tab_control tab_player_edit;
    private System.Windows.Forms.TabPage principal;
    private System.Windows.Forms.TabPage address;
    private System.Windows.Forms.TabPage contact;
    private WSI.Cashier.Controls.uc_round_textbox txt_email_02;
    private PhoneNumberTextBox txt_phone_01;
    private WSI.Cashier.Controls.uc_label lbl_email_02;
    private WSI.Cashier.Controls.uc_label lbl_phone_01;
    private WSI.Cashier.Controls.uc_label lbl_email_01;
    private WSI.Cashier.Controls.uc_round_textbox txt_email_01;
    private WSI.Cashier.Controls.uc_label lbl_phone_02;
    private PhoneNumberTextBox txt_phone_02;
    private System.Windows.Forms.TabPage comments;
    private System.Windows.Forms.TabPage points;
    private WSI.Cashier.Controls.uc_label lbl_points_sufix;
    private WSI.Cashier.Controls.uc_label lbl_points;
    private WSI.Cashier.Controls.uc_label lbl_points_prefix;
    private WSI.Cashier.Controls.uc_label lbl_level;
    private WSI.Cashier.Controls.uc_label lbl_level_expiration_prefix;
    private WSI.Cashier.Controls.uc_label lbl_level_expiration;
    private WSI.Cashier.Controls.uc_label lbl_level_entered_prefix;
    private WSI.Cashier.Controls.uc_label lbl_level_entered;
    private WSI.Cashier.Controls.uc_label lbl_level_prefix;
    private System.Windows.Forms.TabPage pin;
    private WSI.Cashier.Controls.uc_label label3;
    private WSI.Cashier.Controls.uc_label label4;
    private WSI.Cashier.Controls.uc_round_textbox textBox1;
    private WSI.Cashier.Controls.uc_round_combobox comboBox1;
    private WSI.Cashier.Controls.uc_label label5;
    private NumericTextBox numericTextBox1;
    
    private WSI.Cashier.Controls.uc_round_combobox comboBox2;
    private NumericTextBox numericTextBox2;
    private WSI.Cashier.Controls.uc_label label6;
    private WSI.Cashier.Controls.uc_label label7;
    private WSI.Cashier.Controls.uc_label label8;
    private NumericTextBox numericTextBox3;
    private WSI.Cashier.Controls.uc_label label9;
    private WSI.Cashier.Controls.uc_round_textbox textBox2;
    private WSI.Cashier.Controls.uc_round_textbox txt_twitter;
    private WSI.Cashier.Controls.uc_label lbl_twitter;
    private WSI.Cashier.Controls.uc_label lbl_pin_msg;
    private NumericTextBox txt_confirm_pin;
    private NumericTextBox txt_pin;
    private WSI.Cashier.Controls.uc_round_button btn_generate_random_pin;
    private WSI.Cashier.Controls.uc_round_button btn_save_pin;
    private WSI.Cashier.Controls.uc_label lbl_confirm_pin;
    private WSI.Cashier.Controls.uc_label lbl_pin;
    private System.Windows.Forms.TabPage block;
    private WSI.Cashier.Controls.uc_label lbl_block_description;
    private System.Windows.Forms.PictureBox pb_blocked;
    private WSI.Cashier.Controls.uc_round_textbox txt_block_description;
    private WSI.Cashier.Controls.uc_label lbl_blocked;
    private WSI.Cashier.Controls.uc_label label13;
    private WSI.Cashier.Controls.uc_label label14;
    private WSI.Cashier.Controls.uc_label label15;
    private WSI.Cashier.Controls.uc_label label16;
    private WSI.Cashier.Controls.uc_label label17;
    private WSI.Cashier.Controls.uc_label label18;
    private WSI.Cashier.Controls.uc_label label19;
    private WSI.Cashier.Controls.uc_label label20;
    private WSI.Cashier.Controls.uc_label label21;
    private WSI.Cashier.Controls.uc_label label22;
    private WSI.Cashier.Controls.uc_label label23;
    private WSI.Cashier.Controls.uc_round_combobox comboBox3;
    private WSI.Cashier.Controls.uc_label label24;
    private CharacterTextBox characterTextBox1;
    private CharacterTextBox characterTextBox2;
    private WSI.Cashier.Controls.uc_label label25;
    private WSI.Cashier.Controls.uc_round_button button1;
    private System.Windows.Forms.Panel panel1;
    private WSI.Cashier.Controls.uc_label label26;
    private WSI.Cashier.Controls.uc_label label27;
    private NumericTextBox numericTextBox4;
    private NumericTextBox numericTextBox5;
    private WSI.Cashier.Controls.uc_label label28;
    private WSI.Cashier.Controls.uc_label label29;
    private NumericTextBox numericTextBox6;
    private WSI.Cashier.Controls.uc_label label30;
    private CharacterTextBox characterTextBox3;
    private CharacterTextBox characterTextBox4;
    private WSI.Cashier.Controls.uc_label label31;
    private WSI.Cashier.Controls.uc_label label32;
    private WSI.Cashier.Controls.uc_label label33;
    private WSI.Cashier.Controls.uc_label label34;
    private WSI.Cashier.Controls.uc_label label35;
    private CharacterTextBox characterTextBox5;
    private WSI.Cashier.Controls.uc_round_combobox comboBox4;
    private WSI.Cashier.Controls.uc_label label36;
    private NumericTextBox numericTextBox7;
    private WSI.Cashier.Controls.uc_round_combobox comboBox5;
    private NumericTextBox numericTextBox8;
    private WSI.Cashier.Controls.uc_label label37;
    private WSI.Cashier.Controls.uc_label label38;
    private WSI.Cashier.Controls.uc_label label39;
    private NumericTextBox numericTextBox9;
    private System.Windows.Forms.TabPage beneficiary;
    private WSI.Cashier.Controls.uc_round_panel gb_beneficiary;
    private WSI.Cashier.Controls.uc_label lbl_beneficiary_full_name;
    private CharacterTextBox txt_beneficiary_last_name2;
    private CharacterTextBox txt_beneficiary_last_name1;
    private CharacterTextBox txt_beneficiary_RFC;
    private CharacterTextBox txt_beneficiary_name;
    private WSI.Cashier.Controls.uc_label lbl_beneficiary_RFC;
    private WSI.Cashier.Controls.uc_label lbl_beneficiary_last_name2;
    private WSI.Cashier.Controls.uc_label lbl_beneficiary_last_name1;
    private WSI.Cashier.Controls.uc_label lbl_beneficiary_name;
    private WSI.Cashier.Controls.uc_label lbl_beneficiary_birth_date;
    private CharacterTextBox txt_beneficiary_CURP;
    private WSI.Cashier.Controls.uc_label lbl_beneficiary_CURP;
    private WSI.Cashier.Controls.uc_label lbl_beneficiary_occupation;
    private WSI.Cashier.Controls.uc_round_combobox cb_beneficiary_gender;
    private WSI.Cashier.Controls.uc_label lbl_beneficiary_gender;
    private WSI.Cashier.Controls.uc_round_button button2;
    private WSI.Cashier.Controls.uc_round_combobox comboBox6;
    private WSI.Cashier.Controls.uc_label label10;
    private WSI.Cashier.Controls.uc_round_button button3;
    private WSI.Cashier.Controls.uc_round_button button4;
    private WSI.Cashier.Controls.uc_round_button button5;
    private WSI.Cashier.Controls.uc_round_button btn_ben_scan_docs;
    private WSI.Cashier.Controls.uc_checkBox chk_holder_has_beneficiary;
    private WSI.Cashier.Controls.uc_round_panel gb_next_level;
    private WSI.Cashier.Controls.uc_label lbl_points_to_next_level_prefix;
    private WSI.Cashier.Controls.uc_label lbl_points_level_prefix;
    private WSI.Cashier.Controls.uc_label lbl_points_level;
    private WSI.Cashier.Controls.uc_label lbl_points_req_prefix;
    private WSI.Cashier.Controls.uc_label lbl_points_req;
    private WSI.Cashier.Controls.uc_label lbl_points_to_next_level;
    private WSI.Cashier.Controls.uc_label lbl_points_discretionaries_prefix;
    private WSI.Cashier.Controls.uc_label lbl_points_discretionaries;
    private WSI.Cashier.Controls.uc_label lbl_points_generated_prefix;
    private WSI.Cashier.Controls.uc_label lbl_points_generated;
    private WSI.Cashier.Controls.uc_label lbl_points_to_maintain_prefix;
    private WSI.Cashier.Controls.uc_label lbl_points_to_maintain;
    private WSI.Cashier.Controls.uc_label lbl_points_last_days;
    private WSI.Cashier.Controls.uc_round_button btn_print_information;
    private WSI.Cashier.Controls.uc_round_auto_combobox cb_beneficiary_occupation;
    private System.Windows.Forms.TableLayoutPanel tlp_contact;
    private System.Windows.Forms.TableLayoutPanel tlp_ben_name;
    private System.Windows.Forms.TableLayoutPanel tlp_ben_principal;
    private System.Windows.Forms.TableLayoutPanel tlp_ben_principal1;
    private uc_datetime uc_dt_ben_birth;
    private System.Windows.Forms.PictureBox m_img_photo;
    private WSI.Cashier.Controls.uc_round_button btn_id_card_scan;
    private System.Windows.Forms.TableLayoutPanel tlp_name;
    private CharacterTextBox txt_name4;
    private Controls.uc_label lbl_name4;
    private CharacterTextBox txt_name;
    private Controls.uc_label lbl_name;
    private Controls.uc_label lbl_last_name1;
    private CharacterTextBox txt_last_name1;
    private CharacterTextBox txt_last_name2;
    private Controls.uc_label lbl_last_name2;
    private System.Windows.Forms.TableLayoutPanel tlp_principal;
    private uc_datetime uc_dt_birth;
    private CharacterTextBox txt_CURP;
    private Controls.uc_label lbl_wedding_date;
    private Controls.uc_label lbl_birth_date;
    private CharacterTextBox txt_RFC;
    private Controls.uc_label lbl_CURP;
    private Controls.uc_round_button btn_RFC;
    private WSI.Cashier.Controls.uc_round_combobox cb_document_type;
    private Controls.uc_label lbl_document;
    private Controls.uc_label lbl_type_document;
    private CharacterTextBox txt_document;
    private uc_datetime uc_dt_wedding;
    private System.Windows.Forms.TableLayoutPanel tlp_principal1;
    private Controls.uc_round_button btn_scan_docs;
    private WSI.Cashier.Controls.uc_round_combobox cb_marital;
    private Controls.uc_label lbl_marital_status;
    private WSI.Cashier.Controls.uc_round_auto_combobox cb_occupation;
    private Controls.uc_label lbl_occupation;
    private WSI.Cashier.Controls.uc_round_auto_combobox cb_birth_country;
    private Controls.uc_label lbl_birth_country;
    private WSI.Cashier.Controls.uc_round_auto_combobox cb_nationality;
    private Controls.uc_label lbl_nationality;
    private WSI.Cashier.Controls.uc_round_combobox cb_gender;
    private Controls.uc_label lbl_gender;
    private Controls.uc_round_textbox txt_comments;
    private Controls.uc_checkBox chk_show_comments;
    private System.Windows.Forms.PictureBox pb_unblocked;
    private System.Windows.Forms.TableLayoutPanel tlp_address;
    private Controls.uc_label lbl_country;
    private Controls.uc_label lbl_address_01;
    private WSI.Cashier.Controls.uc_round_auto_combobox cb_fed_entity;
    private CharacterTextBox txt_ext_num;
    private Controls.uc_label lbl_fed_entity;
    private CharacterTextBox txt_address_01;
    private Controls.uc_round_textbox txt_zip;
    private Controls.uc_label lbl_zip;
    private Controls.uc_label lbl_city;
    private Controls.uc_label lbl_address_03;
    private CharacterTextBox txt_city;
    private Controls.uc_label lbl_address_02;
    private Controls.uc_label lbl_ext_num;
    private CharacterTextBox txt_address_02;
    private CharacterTextBox txt_address_03;
    private WSI.Cashier.Controls.uc_round_auto_combobox cb_country;
    private System.Windows.Forms.TableLayoutPanel tlp_address_new;
    private WSI.Cashier.Controls.uc_round_auto_combobox cb_city;
    private WSI.Cashier.Controls.uc_round_auto_combobox cb_address_03;
    private WSI.Cashier.Controls.uc_round_auto_combobox cb_address_02;
    private Controls.uc_label lbl_country_new;
    private Controls.uc_label lbl_address_01_new;
    private WSI.Cashier.Controls.uc_round_auto_combobox cb_fed_entity_new;
    private CharacterTextBox txt_ext_num_new;
    private Controls.uc_label lbl_fed_entity_new;
    private CharacterTextBox txt_address_01_new;
    private Controls.uc_round_textbox txt_zip_new;
    private Controls.uc_label lbl_zip_new;
    private Controls.uc_label lbl_city_new;
    private Controls.uc_label lbl_address_03_new;
    private Controls.uc_label lbl_address_02_new;
    private Controls.uc_label lbl_ext_num_new;
    private WSI.Cashier.Controls.uc_round_auto_combobox cb_country_new;
    private Controls.uc_label lbl_address_validation;
    private Controls.uc_checkBox chk_address_mode;
    private Controls.uc_label lbl_full_name;
    private System.Windows.Forms.Label lbl_no_photo;
    private Controls.uc_round_button btn_reg_entry1;
    private Controls.uc_label lbl_next_level_name;
  }
}