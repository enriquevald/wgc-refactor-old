namespace WSI.Cashier
{
  partial class frm_mb_card_assign
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.lbl_card_assign_title = new WSI.Cashier.Controls.uc_label();
      this.btn_cancel = new WSI.Cashier.Controls.uc_round_button();
      this.gp_card_box_mb = new WSI.Cashier.Controls.uc_round_panel();
      this.lbl_trackdata = new WSI.Cashier.Controls.uc_label();
      this.lbl_account_value = new WSI.Cashier.Controls.uc_label();
      this.lbl_card_previous = new WSI.Cashier.Controls.uc_label();
      this.lbl_account = new WSI.Cashier.Controls.uc_label();
      this.timer1 = new System.Windows.Forms.Timer(this.components);
      this.gp_box_assign_card = new WSI.Cashier.Controls.uc_round_panel();
      this.lbl_existent_card = new WSI.Cashier.Controls.uc_label();
      this.uc_card_reader1 = new WSI.Cashier.uc_card_reader();
      this.pnl_data.SuspendLayout();
      this.gp_card_box_mb.SuspendLayout();
      this.gp_box_assign_card.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.Controls.Add(this.uc_card_reader1);
      this.pnl_data.Controls.Add(this.btn_cancel);
      this.pnl_data.Controls.Add(this.gp_card_box_mb);
      this.pnl_data.Controls.Add(this.lbl_card_assign_title);
      this.pnl_data.Controls.Add(this.gp_box_assign_card);
      this.pnl_data.Size = new System.Drawing.Size(508, 374);
      this.pnl_data.TabIndex = 0;
      this.pnl_data.Paint += new System.Windows.Forms.PaintEventHandler(this.pnl_data_Paint);
      // 
      // lbl_card_assign_title
      // 
      this.lbl_card_assign_title.AutoSize = true;
      this.lbl_card_assign_title.BackColor = System.Drawing.Color.Transparent;
      this.lbl_card_assign_title.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_card_assign_title.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_card_assign_title.Location = new System.Drawing.Point(11, 3);
      this.lbl_card_assign_title.Name = "lbl_card_assign_title";
      this.lbl_card_assign_title.Size = new System.Drawing.Size(55, 18);
      this.lbl_card_assign_title.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_card_assign_title.TabIndex = 0;
      this.lbl_card_assign_title.Text = "xNone";
      this.lbl_card_assign_title.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.lbl_card_assign_title.Visible = false;
      // 
      // btn_cancel
      // 
      this.btn_cancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btn_cancel.FlatAppearance.BorderSize = 0;
      this.btn_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_cancel.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_cancel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_cancel.Image = null;
      this.btn_cancel.Location = new System.Drawing.Point(341, 304);
      this.btn_cancel.Name = "btn_cancel";
      this.btn_cancel.Size = new System.Drawing.Size(155, 60);
      this.btn_cancel.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_cancel.TabIndex = 4;
      this.btn_cancel.Text = "XCANCEL";
      this.btn_cancel.UseVisualStyleBackColor = false;
      this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
      // 
      // gp_card_box_mb
      // 
      this.gp_card_box_mb.BackColor = System.Drawing.Color.Transparent;
      this.gp_card_box_mb.Controls.Add(this.lbl_trackdata);
      this.gp_card_box_mb.Controls.Add(this.lbl_account_value);
      this.gp_card_box_mb.Controls.Add(this.lbl_card_previous);
      this.gp_card_box_mb.Controls.Add(this.lbl_account);
      this.gp_card_box_mb.CornerRadius = 10;
      this.gp_card_box_mb.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gp_card_box_mb.ForeColor = System.Drawing.Color.Black;
      this.gp_card_box_mb.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gp_card_box_mb.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gp_card_box_mb.HeaderHeight = 35;
      this.gp_card_box_mb.HeaderSubText = null;
      this.gp_card_box_mb.HeaderText = null;
      this.gp_card_box_mb.Location = new System.Drawing.Point(11, 183);
      this.gp_card_box_mb.Name = "gp_card_box_mb";
      this.gp_card_box_mb.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gp_card_box_mb.Size = new System.Drawing.Size(485, 115);
      this.gp_card_box_mb.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.gp_card_box_mb.TabIndex = 3;
      this.gp_card_box_mb.Text = "xCard Data";
      // 
      // lbl_trackdata
      // 
      this.lbl_trackdata.BackColor = System.Drawing.Color.Transparent;
      this.lbl_trackdata.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_trackdata.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_trackdata.Location = new System.Drawing.Point(129, 75);
      this.lbl_trackdata.Name = "lbl_trackdata";
      this.lbl_trackdata.Size = new System.Drawing.Size(213, 16);
      this.lbl_trackdata.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_trackdata.TabIndex = 3;
      this.lbl_trackdata.Text = "XXXXXXXXXXXXXXXXXX";
      this.lbl_trackdata.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_account_value
      // 
      this.lbl_account_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_account_value.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_account_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_account_value.Location = new System.Drawing.Point(129, 47);
      this.lbl_account_value.Name = "lbl_account_value";
      this.lbl_account_value.Size = new System.Drawing.Size(223, 16);
      this.lbl_account_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_account_value.TabIndex = 1;
      this.lbl_account_value.Text = "XXXXXXXXXXXXXXXXXX";
      this.lbl_account_value.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_card_previous
      // 
      this.lbl_card_previous.BackColor = System.Drawing.Color.Transparent;
      this.lbl_card_previous.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_card_previous.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_card_previous.Location = new System.Drawing.Point(8, 75);
      this.lbl_card_previous.Name = "lbl_card_previous";
      this.lbl_card_previous.Size = new System.Drawing.Size(115, 16);
      this.lbl_card_previous.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_card_previous.TabIndex = 2;
      this.lbl_card_previous.Text = "xPrevious Card";
      this.lbl_card_previous.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_account
      // 
      this.lbl_account.BackColor = System.Drawing.Color.Transparent;
      this.lbl_account.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_account.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_account.Location = new System.Drawing.Point(9, 48);
      this.lbl_account.Name = "lbl_account";
      this.lbl_account.Size = new System.Drawing.Size(115, 16);
      this.lbl_account.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_account.TabIndex = 0;
      this.lbl_account.Text = "xAccount";
      this.lbl_account.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // timer1
      // 
      this.timer1.Interval = 4000;
      this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
      // 
      // gp_box_assign_card
      // 
      this.gp_box_assign_card.BackColor = System.Drawing.Color.Transparent;
      this.gp_box_assign_card.Controls.Add(this.lbl_existent_card);
      this.gp_box_assign_card.CornerRadius = 10;
      this.gp_box_assign_card.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gp_box_assign_card.ForeColor = System.Drawing.Color.Black;
      this.gp_box_assign_card.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gp_box_assign_card.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gp_box_assign_card.HeaderHeight = 35;
      this.gp_box_assign_card.HeaderSubText = null;
      this.gp_box_assign_card.HeaderText = null;
      this.gp_box_assign_card.Location = new System.Drawing.Point(11, 24);
      this.gp_box_assign_card.Name = "gp_box_assign_card";
      this.gp_box_assign_card.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gp_box_assign_card.Size = new System.Drawing.Size(485, 154);
      this.gp_box_assign_card.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.gp_box_assign_card.TabIndex = 1;
      this.gp_box_assign_card.Text = "xCard Data";
      // 
      // lbl_existent_card
      // 
      this.lbl_existent_card.BackColor = System.Drawing.Color.Transparent;
      this.lbl_existent_card.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_existent_card.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.lbl_existent_card.Location = new System.Drawing.Point(0, 133);
      this.lbl_existent_card.Name = "lbl_existent_card";
      this.lbl_existent_card.Size = new System.Drawing.Size(485, 19);
      this.lbl_existent_card.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_RED;
      this.lbl_existent_card.TabIndex = 0;
      this.lbl_existent_card.Text = "xCARD ID ALREADY EXISTS!";
      this.lbl_existent_card.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.lbl_existent_card.Visible = false;
      // 
      // uc_card_reader1
      // 
      this.uc_card_reader1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.uc_card_reader1.Location = new System.Drawing.Point(15, 64);
      this.uc_card_reader1.MaximumSize = new System.Drawing.Size(800, 90);
      this.uc_card_reader1.MinimumSize = new System.Drawing.Size(300, 90);
      this.uc_card_reader1.Name = "uc_card_reader1";
      this.uc_card_reader1.Size = new System.Drawing.Size(473, 90);
      this.uc_card_reader1.TabIndex = 2;
      this.uc_card_reader1.OnTrackNumberReadEvent += new WSI.Cashier.uc_card_reader.TrackNumberReadEventHandler(this.uc_card_reader1_OnTrackNumberReadEvent);
      // 
      // frm_mb_card_assign
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(508, 429);
      this.ControlBox = false;
      this.Name = "frm_mb_card_assign";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Style = WSI.Cashier.Controls.frm_base.BaseFormStyle.PRINCIPAL;
      this.Text = "MB Card Assign";
      this.Shown += new System.EventHandler(this.frm_mb_card_assign_Shown);
      this.pnl_data.ResumeLayout(false);
      this.pnl_data.PerformLayout();
      this.gp_card_box_mb.ResumeLayout(false);
      this.gp_box_assign_card.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

   
    private WSI.Cashier.Controls.uc_label lbl_card_assign_title;
    private WSI.Cashier.Controls.uc_label lbl_card_previous;
    private WSI.Cashier.Controls.uc_label lbl_account;
    private WSI.Cashier.Controls.uc_label lbl_trackdata;
    private WSI.Cashier.Controls.uc_label lbl_account_value;
    private WSI.Cashier.Controls.uc_round_button btn_cancel;
    private System.Windows.Forms.Timer timer1;
    private Controls.uc_round_panel gp_card_box_mb;
    private Controls.uc_round_panel gp_box_assign_card;
    private uc_card_reader uc_card_reader1;
    private Controls.uc_label lbl_existent_card;
  }
}