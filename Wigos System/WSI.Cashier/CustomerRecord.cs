﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WSI.Cashier
{
  public class AccountsReception
  {
    public int FirstName
    {
      get
      {
        throw new System.NotImplementedException();
      }
      set
      {
      }
    }

    public String SecondName
    {
      get
      {
        throw new System.NotImplementedException();
      }
      set
      {
      }
    }

    public int SecondSurname
    {
      get
      {
        throw new System.NotImplementedException();
      }
      set
      {
      }
    }

    public String FirstSurname
    {
      get
      {
        throw new System.NotImplementedException();
      }
      set
      {
      }
    }

    public DateTime BirthDate
    {
      get
      {
        throw new System.NotImplementedException();
      }
      set
      {
      }
    }
    public Contact Contact
    {
      get
      {
        throw new System.NotImplementedException();
      }
      set
      {
      }
    }
    public String Document
    {
      get
      {
        throw new System.NotImplementedException();
      }
      set
      {
      }
    }

    public int Gender
    {
      get
      {
        throw new System.NotImplementedException();
      }
      set
      {
      }
    }

    public String Nacionality
    {
      get
      {
        throw new System.NotImplementedException();
      }
      set
      {
      }
    }

    public String Country
    {
      get
      {
        throw new System.NotImplementedException();
      }
      set
      {
      }
    }

    public Int32 AccountId
    {
      get
      {
        throw new System.NotImplementedException();
      }
      set
      {
      }
    }



    public ENUM_BLACK_LIST BlackList
    {
      get
      {
        throw new System.NotImplementedException();
      }
      set
      {
      }
    }

    public ENUM_RECORD Record
    {
      get
      {
        throw new System.NotImplementedException();
      }
      set
      {
      }
    }

    public List <Adress> Adress
    {
      get
      {
        throw new System.NotImplementedException();
      }
      set
      {
      }
    }

    public ENUM_DOCUMENT_TYPE TypeDocument
    {
      get
      {
        throw new System.NotImplementedException();
      }
      set
      {
      }
    }

    public String Comments
    {
      get
      {
        throw new System.NotImplementedException();
      }
      set
      {
      }
    }




  }

  public enum ENUM_RECORD
  {
    VALID = 1,
    NOT_VALID = 2,
    PROCESSING = 3,
    ERROR = 4
  }

 
  public enum ENUM_BLACK_LIST
  {
    VALID = 1,
    NOT_VALID = 2,
    PROCESSING = 3,
    ERROR = 4
  }

  public class Contact
  {
    public String FirstTelephone
    {
      get
      {
        throw new System.NotImplementedException();
      }
      set
      {
      }
    }

    public String Email
    {
      get
      {
        throw new System.NotImplementedException();
      }
      set
      {
      }
    }

    public String SecondTelephone
    {
      get
      {
        throw new System.NotImplementedException();
      }
      set
      {
      }
    }
  }

  public class Adress
  {
    public ENUM_ADRESS TypeAdress
    {
      get
      {
        throw new System.NotImplementedException();
      }
      set
      {
      }
    }

    public String Street
    {
      get
      {
        throw new System.NotImplementedException();
      }
      set
      {
      }
    }

    public String Cp
    {
      get
      {
        throw new System.NotImplementedException();
      }
      set
      {
      }
    }

    public int Number
    {
      get
      {
        throw new System.NotImplementedException();
      }
      set
      {
      }
    }

    public String Colony
    {
      get
      {
        throw new System.NotImplementedException();
      }
      set
      {
      }
    }

    public String Delegation
    {
      get
      {
        throw new System.NotImplementedException();
      }
      set
      {
      }
    }

    public String City
    {
      get
      {
        throw new System.NotImplementedException();
      }
      set
      {
      }
    }

    public String CountryAdress
    {
      get
      {
        throw new System.NotImplementedException();
      }
      set
      {
      }
    }

    public String FederalEntity
    {
      get
      {
        throw new System.NotImplementedException();
      }
      set
      {
      }
    }
  }

  public enum ENUM_ADRESS
  {
    HOME_ADRESS,
    SECONDARY_ADRESS
  }

  public class CustomersReception : AccountsReception
  {
    public Int32 CurrentRecordID
    {
      get
      {
        throw new System.NotImplementedException();
      }
      set
      {
      }
    }

    public Int32 CustomerID
    {
      get
      {
        throw new System.NotImplementedException();
      }
      set
      {
      }
    }

    public CustomerVisit CustomerVisit
    {
      get
      {
        throw new System.NotImplementedException();
      }
      set
      {
      }
    }

    public CustomerRecords CustomerRecord
    {
      get
      {
        throw new System.NotImplementedException();
      }
      set
      {
      }
    }
  }

  public class CustomerRecords
  {
    public Int32 CustomerRecordID
    {
      get
      {
        throw new System.NotImplementedException();
      }
      set
      {
      }
    }

    public Boolean CustomerRecordDeleted
    {
      get
      {
        throw new System.NotImplementedException();
      }
      set
      {
      }
    }

    public DateTime CustomerRecordCreated
    {
      get
      {
        throw new System.NotImplementedException();
      }
      set
      {
      }
    }

    public DateTime CustomerRecordExpiration
    {
      get
      {
        throw new System.NotImplementedException();
      }
      set
      {
      }
    }

    public List <CustomerRecordDetails> CustomerRecordDetail
    {
      get
      {
        throw new System.NotImplementedException();
      }
      set
      {
      }
    }
  }

  public class CustomerRecordDetails
  {

    public Int32 RecordDetailID
    {
      get
      {
        throw new System.NotImplementedException();
      }
      set
      {
      }
    }

    public bool RecordDetailDeleted
    {
      get
      {
        throw new System.NotImplementedException();
      }
      set
      {
      }
    }

    public String RecordDetailData
    {
      get
      {
        throw new System.NotImplementedException();
      }
      set
      {
      }
    }

    public DateTime RecordDetailCreated
    {
      get
      {
        throw new System.NotImplementedException();
      }
      set
      {
      }
    }

    public DateTime RecordDetailExpiration
    {
      get
      {
        throw new System.NotImplementedException();
      }
      set
      {
      }
    }

    public ENUM_TYPE_DETAIL RecordDetailType
    {
      get
      {
        throw new System.NotImplementedException();
      }
      set
      {
      }
    }
  }

  public enum ENUM_TYPE_DETAIL
  {
    ACCOUNT_ID = 1,
    FIRST_SURNAME = 2,
    SECOND_SURNAME = 3,
    DOCUMENT = 4,
    BIRTH_DATE = 5,
    GENDER = 6,
    NATIONALITY = 7,
    COUNTRY = 8,
    FIRST_NAME = 9,
    SECOND_NAME = 10,
    DOCUMENT_TYPE = 11,
    EMAIL = 12,
    FIRST_TEL_NUMBER = 13,
    SECOND_TEL_NUMBER = 14,
    BLACK_LIST = 15,
    RECORD = 16,
    TYPE_DOCUMENT_ID = 17,
    XML_HOME_ADRESS = 500,
    XML_SECONDARY_ADRESS = 501,
  }

  public enum ENUM_DOCUMENT_TYPE
  {
    DNI = 0,
    PASSPORT = 1,
    SS = 2,
    DRIVE_LICENSE = 3,
    OTHER = 4
  }

  public class CustomerVisit
  {
    public Int32 VisitID
    {
      get
      {
        throw new System.NotImplementedException();
      }
      set
      {
      }
    }

    public int VisitGamingDay
    {
      get
      {
        throw new System.NotImplementedException();
      }
      set
      {
      }
    }

    public int VisitLevel
    {
      get
      {
        throw new System.NotImplementedException();
      }
      set
      {
      }
    }

    public Boolean VisitIsVip
    {
      get
      {
        throw new System.NotImplementedException();
      }
      set
      {
      }
    }

    public List <CustomerEntrance> CustomerEntrance
    {
      get
      {
        throw new System.NotImplementedException();
      }
      set
      {
      }
    }
  }

  public class CustomerEntrance
  {
    public Int32 EntranceDate
    {
      get
      {
        throw new System.NotImplementedException();
      }
      set
      {
      }
    }

    public int CashierSessionID
    {
      get
      {
        throw new System.NotImplementedException();
      }
      set
      {
      }
    }

    public int UserID
    {
      get
      {
        throw new System.NotImplementedException();
      }
      set
      {
      }
    }

    public Int16 BlockReason
    {
      get
      {
        throw new System.NotImplementedException();
      }
      set
      {
      }
    }

    public Int16 BlockReason2
    {
      get
      {
        throw new System.NotImplementedException();
      }
      set
      {
      }
    }

    public String BlockDescription
    {
      get
      {
        throw new System.NotImplementedException();
      }
      set
      {
      }
    }

    public String Remarks
    {
      get
      {
        throw new System.NotImplementedException();
      }
      set
      {
      }
    }

    public Int32 TicketID
    {
      get
      {
        throw new System.NotImplementedException();
      }
      set
      {
      }
    }

    public double TicketPrice
    {
      get
      {
        throw new System.NotImplementedException();
      }
      set
      {
      }
    }

    public ENUM_DOCUMENT_TYPE TypeDocument
    {
      get
      {
        throw new System.NotImplementedException();
      }
      set
      {
      }
    }

    public String DocumentNumber
    {
      get
      {
        throw new System.NotImplementedException();
      }
      set
      {
      }
    }

    public int VoucherID
    {
      get
      {
        throw new System.NotImplementedException();
      }
      set
      {
      }
    }
  }






}
