//------------------------------------------------------------------------------
// Copyright � 2010 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : frm_generic_amount_input.cs
// 
//   DESCRIPTION : Implements the frm_generic_amount_input
//
// REVISION HISTORY :
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 09-JAN-2014 DLL    First release.
// 10-DEC-2014 DRV    Decimal currency formatting: DUT 201411 - Tito Chile - Currency formatting without decimals
// 11-NOV-2015 SGB    Backlog Item WIG-5880: Change designer
// 28-JUL-2016 ESE    Product Backlog Item 15239:TPV Televisa: Cashier, totalizing strip
// 12-SEP-2016 JML    Bug 17344: Gaming tables: Number of visits is not reported correctly when close the table.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WSI.Common;
using System.Data.SqlClient;


namespace WSI.Cashier
{
  public partial class frm_generic_amount_input : Controls.frm_base
  {
    private Decimal m_amount;
    private Size m_size_form;
    private String m_iso_code;

    private GENERIC_AMOUNT_INPUT_TYPE m_form_type;

    public delegate void KeyPressed(String Amount);
    public event KeyPressed OnKeyPressed;

    public void HideMoneyButtons()
    {
      uc_input_amount.ShowMoneyButtons = false;
      uc_input_amount.ReplaceAtFirstTouch = true;

      btn_cancel.Location = new System.Drawing.Point(btn_cancel.Location.X, btn_cancel.Location.Y - 80);
      this.pnl_data.Size = new Size(this.pnl_data.Size.Width, this.pnl_data.Size.Height - 80);
      this.Size = new Size(this.Size.Width, this.Size.Height - 80);
    }

    public frm_generic_amount_input()
    {
      InitializeComponent();

      uc_input_amount.OnAmountSelected += new uc_input_amount.AmountSelectedEventHandler(uc_input_amount_OnAmountSelected);

      m_size_form = this.Size;

      EventLastAction.AddEventLastAction(this.Controls);

      m_amount = 0;
    }

    public Boolean Show(GENERIC_AMOUNT_INPUT_TYPE FormType,
                        String CaptionText,
                        ref Decimal Amount,
                        String AmountText = "0"/*Optional*/)
    {
      return Show(FormType, CaptionText, "", ref Amount, AmountText);
    }
    public Boolean Show(GENERIC_AMOUNT_INPUT_TYPE FormType,
                        String CaptionText,
                        String IsoCode,
                        ref Decimal Amount,
                        String AmountText = "0"/*Optional*/)
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_generic_amount_input", Log.Type.Message);

      m_form_type = FormType;
      this.FormTitle = CaptionText;
      m_iso_code = IsoCode;

      m_amount = Amount;

      ShowLabels();
      DefaultValues();

      switch (FormType)
      {
        case GENERIC_AMOUNT_INPUT_TYPE.AMOUNT:
          this.FormHeaderVisible = false;
          this.Location = new Point(this.Location.X, this.Location.Y + 55); // 55 = Header
          this.btn_cancel.Visible = false;
          this.Size = new Size(386, 385);
          uc_input_amount.txt_amount.Visible = false;

          this.Show();
          break;
        case GENERIC_AMOUNT_INPUT_TYPE.METER:
          this.FormHeaderVisible = false;
          this.Size = new Size(386, 466);
          DialogResult _dlg_rc_meter = this.ShowDialog();
          if (_dlg_rc_meter != DialogResult.OK)
          {
            Amount = 0;

            return false;
          }
          break;
        case GENERIC_AMOUNT_INPUT_TYPE.CURRENCY:
          this.FormHeaderVisible = false;
          this.Size = new Size(386, 466);

          DialogResult _dlg_rc_currency = this.ShowDialog();
          if (_dlg_rc_currency != DialogResult.OK)
          {
            Amount = 0;

            return false;
          }
          break;

        case GENERIC_AMOUNT_INPUT_TYPE.HOUR:
          uc_input_amount.txt_amount.Text = int.Parse(AmountText).ToString();
          uc_input_amount.SetTextFocus();

          DialogResult _dlg_rc_hour = this.ShowDialog();
          if (_dlg_rc_hour != DialogResult.OK)
          {
            Amount = 0;
            return false;
          }
          break;

        default:
          DialogResult _dlg_rc = this.ShowDialog();
          if (_dlg_rc != DialogResult.OK)
          {
            Amount = 0;

            return false;
          }
          break;
      }

      Amount = m_amount;

      return true;
    }

    private void btn_cancel_Click(object sender, EventArgs e)
    {
      this.DialogResult = DialogResult.Cancel;

      Misc.WriteLog("[FORM CLOSE] frm_generic_amount_input (cancel)", Log.Type.Message);
      this.Close();
    }

    private void uc_input_amount_OnAmountSelected(Decimal Amount)
    {
      switch (m_form_type)
      {
        case GENERIC_AMOUNT_INPUT_TYPE.METER:
          break;
        default:
          m_amount = Amount;
          break;
      }

      this.DialogResult = DialogResult.OK;

      Misc.WriteLog("[FORM CLOSE] frm_generic_amount_input (uc_input_amount)", Log.Type.Message);
      this.Close();
    }

    private void uc_input_amount_OnModifyValue(String Amount)
    {
      Decimal.TryParse(Amount, out m_amount);

      if (OnKeyPressed != null)
      {
        OnKeyPressed(Amount);
      }
    }

    private void ShowLabels()
    {
      this.btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");
      switch (m_form_type)
      {
        case GENERIC_AMOUNT_INPUT_TYPE.VISITS:
          uc_input_amount.gp_digits_box.Text = Resource.String("STR_UC_INPUT_AMOUNT_GROUP_BOX_VISITS");
          break;
        case GENERIC_AMOUNT_INPUT_TYPE.AMOUNT:
          uc_input_amount.gp_digits_box.Text = String.Empty;
          uc_input_amount.WithDecimals = true;
          uc_input_amount.SetDotButtonEnabled(true);
          uc_input_amount.SetTextNoVisible();
          uc_input_amount.OnModifyValue += new uc_input_amount.ModifyValueEventHandler(uc_input_amount_OnModifyValue);
          if (Format.GetFormattedDecimals(m_iso_code) == 0)
          {
            uc_input_amount.WithDecimals = false;
            uc_input_amount.SetDotButtonEnabled(false);
          }

          this.Size = new Size(m_size_form.Width, m_size_form.Height - 44);

          break;

        case GENERIC_AMOUNT_INPUT_TYPE.CURRENCY:
          uc_input_amount.gp_digits_box.Text = String.Empty;
          uc_input_amount.WithDecimals = true;
          uc_input_amount.SetDotButtonEnabled(true);

          uc_input_amount.OnModifyValue += new uc_input_amount.ModifyValueEventHandler(uc_input_amount_OnModifyValue);
          if (Format.GetFormattedDecimals(m_iso_code) == 0)
          {
            uc_input_amount.WithDecimals = false;
            uc_input_amount.SetDotButtonEnabled(false);
          }

          this.Size = new Size(m_size_form.Width, m_size_form.Height - 44);

          break;

        case GENERIC_AMOUNT_INPUT_TYPE.METER:
          uc_input_amount.WithDecimals = false;
          uc_input_amount.SetDotButtonEnabled(false);
          uc_input_amount.OnModifyValue += new uc_input_amount.ModifyValueEventHandler(uc_input_amount_OnModifyValue);
          break;
        default:
          break;
      }

    } // ShowLabels

    private void DefaultValues()
    {
      Decimal _visits;

      switch (m_form_type)
      {
        case GENERIC_AMOUNT_INPUT_TYPE.VISITS:
          LoadDefaultVisits(out _visits);
          uc_input_amount.txt_amount.Text = _visits.ToString("0");
          break;

        case GENERIC_AMOUNT_INPUT_TYPE.AMOUNT:
        case GENERIC_AMOUNT_INPUT_TYPE.METER:
          if (m_amount != TerminalMetersInfo.NOT_READED_METER_VALUE)
            uc_input_amount.txt_amount.Text = Format.CompactNumber(m_amount);
          break;
        default:
          break;
      }


    } // DefaultValues

    private Boolean LoadDefaultVisits(out Decimal Visits)
    {
      GamingTablesSessions _gt_session;
      StringBuilder _sql_str;

      Visits = 0;

      try
      {
        _sql_str = new StringBuilder();
        _sql_str.AppendLine(" SELECT   GTS_CLIENT_VISITS ");
        _sql_str.AppendLine("   FROM   GAMING_TABLES_SESSIONS ");
        _sql_str.AppendLine("  WHERE   GTS_GAMING_TABLE_ID    = @pGamingTableId ");
        _sql_str.AppendLine("    AND   GTS_CASHIER_SESSION_ID = @pCashierSessionId ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          GamingTablesSessions.GetOpenedSession(GamingTablesSessions.GamingTableInfo.GamingTableId, out _gt_session, _db_trx.SqlTransaction);

          using (SqlCommand _sql_cmd = new SqlCommand(_sql_str.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pGamingTableId", SqlDbType.Int).Value = GamingTablesSessions.GamingTableInfo.GamingTableId;
            _sql_cmd.Parameters.Add("@pCashierSessionId", SqlDbType.Int).Value = _gt_session.CashierSessionId;

            using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
            {
              if (!_reader.Read())
              {
                Visits = 0;
                return false;
              }
              Visits = (Decimal)_reader[0];
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Visits = 0;

        return false;
      }

      return true;
    }
  }
}