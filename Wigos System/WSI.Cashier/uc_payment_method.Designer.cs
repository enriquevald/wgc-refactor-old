﻿namespace WSI.Cashier
{
  partial class uc_payment_method
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.gb_payment = new WSI.Cashier.Controls.uc_round_panel();
      this.lbl_currency_iso_code = new WSI.Cashier.Controls.uc_label();
      this.pb_currency = new System.Windows.Forms.PictureBox();
      this.btn_selected_currency = new WSI.Cashier.Controls.uc_round_button();
      this.lbl_paymet_method = new WSI.Cashier.Controls.uc_label();
      this.gb_payment.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pb_currency)).BeginInit();
      this.SuspendLayout();
      // 
      // gb_payment
      // 
      this.gb_payment.BackColor = System.Drawing.Color.Transparent;
      this.gb_payment.BorderColor = System.Drawing.Color.Empty;
      this.gb_payment.Controls.Add(this.lbl_currency_iso_code);
      this.gb_payment.Controls.Add(this.pb_currency);
      this.gb_payment.Controls.Add(this.btn_selected_currency);
      this.gb_payment.Controls.Add(this.lbl_paymet_method);
      this.gb_payment.CornerRadius = 10;
      this.gb_payment.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_payment.ForeColor = System.Drawing.Color.Black;
      this.gb_payment.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gb_payment.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_payment.HeaderHeight = 35;
      this.gb_payment.HeaderSubText = null;
      this.gb_payment.Location = new System.Drawing.Point(0, 0);
      this.gb_payment.Name = "gb_payment";
      this.gb_payment.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_payment.Size = new System.Drawing.Size(382, 115);
      this.gb_payment.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.gb_payment.TabIndex = 4;
      // 
      // lbl_currency_iso_code
      // 
      this.lbl_currency_iso_code.AutoSize = true;
      this.lbl_currency_iso_code.BackColor = System.Drawing.Color.Transparent;
      this.lbl_currency_iso_code.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_currency_iso_code.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_currency_iso_code.Location = new System.Drawing.Point(18, 91);
      this.lbl_currency_iso_code.Name = "lbl_currency_iso_code";
      this.lbl_currency_iso_code.Size = new System.Drawing.Size(35, 17);
      this.lbl_currency_iso_code.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_currency_iso_code.TabIndex = 2;
      this.lbl_currency_iso_code.Text = "xISO";
      // 
      // pb_currency
      // 
      this.pb_currency.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.pb_currency.BackColor = System.Drawing.Color.Transparent;
      this.pb_currency.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.pb_currency.Location = new System.Drawing.Point(15, 52);
      this.pb_currency.Name = "pb_currency";
      this.pb_currency.Size = new System.Drawing.Size(36, 36);
      this.pb_currency.TabIndex = 46;
      this.pb_currency.TabStop = false;
      // 
      // btn_selected_currency
      // 
      this.btn_selected_currency.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.btn_selected_currency.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_selected_currency.FlatAppearance.BorderSize = 0;
      this.btn_selected_currency.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_selected_currency.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_selected_currency.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_selected_currency.Image = null;
      this.btn_selected_currency.IsSelected = false;
      this.btn_selected_currency.Location = new System.Drawing.Point(235, 50);
      this.btn_selected_currency.Name = "btn_selected_currency";
      this.btn_selected_currency.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_selected_currency.Size = new System.Drawing.Size(130, 50);
      this.btn_selected_currency.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_selected_currency.TabIndex = 1;
      this.btn_selected_currency.Text = "XCHANGE";
      this.btn_selected_currency.UseVisualStyleBackColor = false;
      this.btn_selected_currency.Click += new System.EventHandler(this.btn_selected_currency_Click);
      // 
      // lbl_paymet_method
      // 
      this.lbl_paymet_method.BackColor = System.Drawing.Color.Transparent;
      this.lbl_paymet_method.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_paymet_method.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_paymet_method.Location = new System.Drawing.Point(71, 51);
      this.lbl_paymet_method.Name = "lbl_paymet_method";
      this.lbl_paymet_method.Size = new System.Drawing.Size(161, 47);
      this.lbl_paymet_method.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_paymet_method.TabIndex = 0;
      this.lbl_paymet_method.Text = "xPayment Method";
      this.lbl_paymet_method.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // uc_payment_method
      // 
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
      this.Controls.Add(this.gb_payment);
      this.Name = "uc_payment_method";
      this.Size = new System.Drawing.Size(381, 116);
      this.gb_payment.ResumeLayout(false);
      this.gb_payment.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pb_currency)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private Controls.uc_round_panel gb_payment;
    private Controls.uc_label lbl_currency_iso_code;
    private System.Windows.Forms.PictureBox pb_currency;
    private Controls.uc_round_button btn_selected_currency;
    private Controls.uc_label lbl_paymet_method;
  }
}
