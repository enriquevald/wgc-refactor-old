//------------------------------------------------------------------------------
// Copyright � 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_input_amount.cs
// 
//   DESCRIPTION: Implements the following classes:
//                1. uc_input_amount
//
//        AUTHOR: David Lasdiez
// 
// CREATION DATE: 19-SEP-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 19-SEP-2014  DLL     First release.
// 28-JUL-2016  ESE     Product Backlog Item 15239:TPV Televisa: Cashier, totalizing strip
// 28-DEC-2017  EOR     Bug 31158:WIGOS-6081 The user is not able to delete the integer number from an amount in the manual handpay UI
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using WSI.Common;

namespace WSI.Cashier
{
    public partial class uc_input_amount : UserControl
    {
        #region Events
        public delegate void AmountSelectedEventHandler(Decimal Amount);
        public event AmountSelectedEventHandler OnAmountSelected;

        public delegate void ModifyValueEventHandler(String Amount);
        public event ModifyValueEventHandler OnModifyValue;

        #endregion

        #region Members
        private Boolean m_with_decimals;
        private Size m_group_box_size;
        private Boolean m_replace_at_first_touch;
        #endregion

        #region Properties

        public Boolean WithDecimals
        {
            get { return m_with_decimals; }
            set { m_with_decimals = value; }
        }

        public Boolean AmountValueIsEmpty
        {
            get { return String.IsNullOrEmpty(this.txt_amount.Text); }
        }

        public Boolean ShowMoneyButtons
        {
          set
          {
            btn_money_1.Visible = value;
            btn_money_2.Visible = value;
            btn_money_3.Visible = value;
            btn_money_4.Visible = value;

            AdjustButton(value);
          }
        }

        public Boolean ReplaceAtFirstTouch
        {
          set { m_replace_at_first_touch = value; }
        }

        #endregion

        private static String m_decimal_str = System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencyDecimalSeparator;

        #region Constructor

        public uc_input_amount()
        {
          // ATB 16-FEB-2017
          Misc.WriteLog("[UC LOAD] uc_input_amount", Log.Type.Message);

            InitializeComponent();

            m_with_decimals = false;

            InitializeControlResources();

        }

        #endregion

        #region Buttons

        //private Boolean flag_money = false;
        private Currency btn_money_1_amount;
        private Currency btn_money_2_amount;
        private Currency btn_money_3_amount;
        private Currency btn_money_4_amount;
        private string btn_money_1_value;
        private string btn_money_2_value;
        private string btn_money_3_value;
        private string btn_money_4_value;

        //------------------------------------------------------------------------------
        // PURPOSE : Initialize Amount Buttons money.
        //
        //  PARAMS :
        //      - INPUT :
        //
        //      - OUTPUT :
        //
        // RETURNS :
        //
        //   NOTES :
        //
        private void InitMoneyButtons()
        {
            String _currency_format = ""; //"C0";

            btn_money_1_amount = 0;
            btn_money_1_value = "";
            btn_money_2_amount = 0;
            btn_money_2_value = "";
            btn_money_3_amount = 0;
            btn_money_3_value = "";
            btn_money_4_amount = 0;
            btn_money_4_value = "";

            LoadDefaultAmountButtonsSQL();

            btn_money_1.Text = "+" + btn_money_1_amount.ToString(_currency_format);
            if ((Decimal)btn_money_1_amount == 0)
            {
                this.btn_money_1.Enabled = false;
                this.btn_money_1.Visible = false;
            }
            if ((Decimal)btn_money_1_amount >= 1000)
            {
                this.btn_money_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            }

            btn_money_2.Text = "+" + btn_money_2_amount.ToString(_currency_format);
            if ((Decimal)btn_money_2_amount == 0)
            {
                this.btn_money_2.Enabled = false;
                this.btn_money_2.Visible = false;
            }
            if ((Decimal)btn_money_2_amount >= 1000)
            {
                this.btn_money_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            }

            btn_money_3.Text = "+" + btn_money_3_amount.ToString(_currency_format);
            if ((Decimal)btn_money_3_amount == 0)
            {
                this.btn_money_3.Enabled = false;
                this.btn_money_3.Visible = false;
            }
            if ((Decimal)btn_money_3_amount >= 1000)
            {
                this.btn_money_3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            }

            btn_money_4.Text = "+" + btn_money_4_amount.ToString(_currency_format);
            if ((Decimal)btn_money_4_amount == 0)
            {
                this.btn_money_4.Enabled = false;
                this.btn_money_4.Visible = false;
            }
            if ((Decimal)btn_money_4_amount >= 1000)
            {
                this.btn_money_4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            }
        }

        //------------------------------------------------------------------------------
        // PURPOSE : Load Default Amount Buttons money.
        //
        //  PARAMS :
        //      - INPUT :
        //
        //      - OUTPUT :
        //
        // RETURNS :
        //
        //   NOTES :
        //
        private void LoadDefaultAmountButtonsSQL()
        {
            Decimal convert_money;

            try
            {
                btn_money_1_value = GeneralParam.GetString("Cashier.AddAmount", "GamblingTable.CustomButton1", GeneralParam.GetString("Cashier.AddAmount", "CustomButton1", "50"));
                convert_money = Decimal.Parse(btn_money_1_value);
                btn_money_1_amount = (Currency)convert_money;

                btn_money_2_value = GeneralParam.GetString("Cashier.AddAmount", "GamblingTable.CustomButton2", GeneralParam.GetString("Cashier.AddAmount", "CustomButton2", "100"));
                convert_money = Decimal.Parse(btn_money_2_value);
                btn_money_2_amount = (Currency)convert_money;

                btn_money_3_value = GeneralParam.GetString("Cashier.AddAmount", "GamblingTable.CustomButton3", GeneralParam.GetString("Cashier.AddAmount", "CustomButton3", "200"));
                convert_money = Decimal.Parse(btn_money_3_value);
                btn_money_3_amount = (Currency)convert_money;

                btn_money_4_value = GeneralParam.GetString("Cashier.AddAmount", "GamblingTable.CustomButton4", GeneralParam.GetString("Cashier.AddAmount", "CustomButton4", "300"));
                convert_money = Decimal.Parse(btn_money_4_value);
                btn_money_4_amount = (Currency)convert_money;
            }
            catch (Exception ex)
            {
                Log.Exception(ex);
            }

        } // LoadDefaultAmountButtonsSQL

        #endregion


        #region Private Methods

        private void InitializeControlResources()
        {
            gp_digits_box.Text = Resource.String("STR_FRM_AMOUNT_INPUT_DIGITS_GROUP_BOX");
            gp_digits_box.HeaderHeight = 55;
            gp_digits_box.HeaderBackColor = WSI.Cashier.CashierStyle.Colors.m_colors_dic[CashierStyle.Colors.ENUM_COLORS.HEX_373B3F];
            this.BackColor = WSI.Cashier.CashierStyle.Colors.m_colors_dic[CashierStyle.Colors.ENUM_COLORS.HEX_E3E7E9];

            txt_amount.Text = "";

            InitMoneyButtons();

            this.btn_num_dot.Enabled = false;

            m_group_box_size = gp_digits_box.Size;

        }


        private void SumNumber(String NumberStr)
        {
            Decimal _number;
            Decimal _amount;
            Decimal _input_amount;

            if (!Decimal.TryParse(NumberStr, out _number)) _number = 0;


            if (String.IsNullOrEmpty(txt_amount.Text) || txt_amount.Text == "0")
            {
                txt_amount.Text = NumberStr;
            }
            else
            {
                if (!Decimal.TryParse(txt_amount.Text, out _amount)) _amount = 0;
                Decimal _temp = _number + _amount;

                // JBP 18-FEB-2014: Checks max amount
                _input_amount = Decimal.Parse(_temp.ToString());
                if (_input_amount > Cashier.MAX_ALLOWED_AMOUNT)
                {
                    return;
                }

                txt_amount.Text = _temp.ToString();
            }

        } // SumNumber

        /// <summary>
        /// Check and Add digit to amount
        /// </summary>
        /// <param name="NumberStr"></param>
        private void AddNumber(String NumberStr)
        {
            String aux_str;
            String tentative_number;
            Currency input_amount;
            int dec_symbol_pos;

            // Prevent exceeding maximum number of decimals (2)
            dec_symbol_pos = txt_amount.Text.IndexOf(m_decimal_str);
            if (dec_symbol_pos > -1)
            {
                aux_str = txt_amount.Text.Substring(dec_symbol_pos);
                if (aux_str.Length > 2)
                {
                    return;
                }
            }

            // Prevent exceeding maximum amount length
            if (txt_amount.Text.Length >= Cashier.MAX_ALLOWED_AMOUNT_LENGTH)
            {
                return;
            }

            tentative_number = txt_amount.Text + NumberStr;

            try
            {
                input_amount = Decimal.Parse(tentative_number);
                if (input_amount > Cashier.MAX_ALLOWED_AMOUNT)
                {
                    return;
                }
            }
            catch
            {
                input_amount = 0;

                return;
            }

            // Remove unneeded leading zeros
            // Unless we are entering a decimal value, there must not be any leading 0
            if (dec_symbol_pos == -1)
            {
                txt_amount.Text = txt_amount.Text.TrimStart('0') + NumberStr;
            }
            else
            {
                // Accept new symbol/digit 
                txt_amount.Text = txt_amount.Text + NumberStr;
            }
            txt_amount.Focus();
            txt_amount.Select(0, 0);
        }

        private void AdjustButton(Boolean VisibleMoneyButtons)
        {
          if (!VisibleMoneyButtons)
          {
            this.btn_num_1.Location = new System.Drawing.Point(btn_num_1.Location.X, btn_num_1.Location.Y - 80);
            this.btn_num_2.Location = new System.Drawing.Point(btn_num_2.Location.X, btn_num_2.Location.Y - 80);
            this.btn_num_3.Location = new System.Drawing.Point(btn_num_3.Location.X, btn_num_3.Location.Y - 80);
            this.btn_back.Location = new System.Drawing.Point(btn_back.Location.X, btn_back.Location.Y - 80);

            this.btn_num_4.Location = new System.Drawing.Point(btn_num_4.Location.X, btn_num_4.Location.Y - 80);
            this.btn_num_5.Location = new System.Drawing.Point(btn_num_5.Location.X, btn_num_5.Location.Y - 80);
            this.btn_num_6.Location = new System.Drawing.Point(btn_num_6.Location.X, btn_num_6.Location.Y - 80);
            this.btn_intro.Location = new System.Drawing.Point(btn_intro.Location.X, btn_intro.Location.Y - 80);

            this.btn_num_7.Location = new System.Drawing.Point(btn_num_7.Location.X, btn_num_7.Location.Y - 80);
            this.btn_num_8.Location = new System.Drawing.Point(btn_num_8.Location.X, btn_num_8.Location.Y - 80);
            this.btn_num_9.Location = new System.Drawing.Point(btn_num_9.Location.X, btn_num_9.Location.Y - 80);

            this.btn_num_10.Location = new System.Drawing.Point(btn_num_10.Location.X, btn_num_10.Location.Y - 80);
            this.btn_num_dot.Location = new System.Drawing.Point(btn_num_dot.Location.X, btn_num_dot.Location.Y - 80);

            this.Height = this.Height - 80;
          }
        }

        #endregion


        #region Events

        private void btn_num_Click(object sender, EventArgs e)
        {
          if (m_replace_at_first_touch)
          {
            txt_amount.Text = ((System.Windows.Forms.ButtonBase)(sender)).Text;
            m_replace_at_first_touch = false;
          }
          else
          {
            AddNumber(((System.Windows.Forms.ButtonBase)(sender)).Text);
          }
        } // btn_num_Click


        private void btn_custom_Click(object sender, EventArgs e)
        {
            Decimal _money_value;
            String _button_text;

            try
            {
                _button_text = ((ButtonBase)sender).Text;
                _button_text = _button_text.Remove(0, 1); // Remove "+"
                _money_value = Decimal.Parse(_button_text);
            }
            catch
            {
                _money_value = 0;
                _button_text = String.Empty;

            }

            if (_money_value <= Cashier.MAX_ALLOWED_AMOUNT)
            {
                SumNumber(_button_text);
            }
        } // btn_custom_Click

        private void btn_intro_Click(object sender, EventArgs e)
        {
            Decimal _input_amount;

            Decimal.TryParse(txt_amount.Text, out _input_amount);

            if (OnAmountSelected != null)
            {
                OnAmountSelected(_input_amount);
            }
        } // btn_intro_Click


        //------------------------------------------------------------------------------
        // PURPOSE : Deletes the last character.
        //
        //  PARAMS :
        //      - INPUT :
        //          - object:     sender
        //          - EventArgs:  e
        //
        //      - OUTPUT :
        //
        // RETURNS :
        //
        //   NOTES :
        //
        private void btn_back_Click(object sender, EventArgs e)
        {
          if (txt_amount.Text.Length > 0)
          {
            txt_amount.Text = txt_amount.Text.Substring(0, txt_amount.Text.Length - 1);

            // 28-DEC-2017 EOR
            if (txt_amount.Text != "" && txt_amount.Text.Substring(txt_amount.Text.Length - 1, 1) == ".")
            {
              txt_amount.Text = txt_amount.Text.Substring(0, txt_amount.Text.Length - 1);
            }
          }

          txt_amount.Text = txt_amount.Text.Trim() == "" ? "0" : txt_amount.Text;
          txt_amount.Focus();
          txt_amount.Select(0, 0);
        } // btn_back_Click

        private void btn_num_dot_Click(object sender, EventArgs e)
        {
            if (txt_amount.Text.IndexOf(m_decimal_str) == -1)
            {
                txt_amount.Text = txt_amount.Text + m_decimal_str;
            }
            txt_amount.Focus();
        }

        //------------------------------------------------------------------------------
        // PURPOSE : Handle the KeyPress.
        //
        //  PARAMS :
        //      - INPUT :
        //          - object:             sender
        //          - KeyPressEventArgs:  e
        //
        //      - OUTPUT :
        //
        // RETURNS :
        //
        //   NOTES :
        //
        private void uc_input_amount_KeyPress(object sender, KeyPressEventArgs e)
        {
            String aux_str;
            char c;

            c = e.KeyChar;
            e.Handled = true;

            if (c >= '0' && c <= '9')
            {
                aux_str = "" + c;
                AddNumber(aux_str);
            }

            if (c == '\r')
            {
                btn_intro_Click(null, null);
            }

            if (c == '\b')
            {
                btn_back_Click(null, null);
            }

            if (m_with_decimals && c == '.')
            {
                btn_num_dot_Click(null, null);
            }

        }  // uc_input_amount_resizable_KeyPress

        #endregion

        private void uc_input_amount_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            char c;

            c = Convert.ToChar(e.KeyCode);
            if (c == '\r')
            {
                e.IsInputKey = true;
            }
        }

        #region Public Methods
        public void SetDotButtonEnabled(Boolean Enabled)
        {
            this.btn_num_dot.Enabled = Enabled;
        }
        public void SetTextNoVisible()
        {
            this.txt_amount.Visible = false;
            this.gp_digits_box.Size = new Size(this.m_group_box_size.Width, this.m_group_box_size.Height - this.txt_amount.Height);
        }

        public void SetTextVisible()
        {
          this.txt_amount.Visible = true;
          this.gp_digits_box.Size = new Size(this.m_group_box_size.Width, this.m_group_box_size.Height - this.txt_amount.Height);
        }

        public void ResetAmount()
        {
          this.txt_amount.Text = "";
        }

        public void SetTextClear()
        {
          this.txt_amount.Text = "";
        }

        public void SetTextFocus()
        {
          this.txt_amount.Focus();
        }

        #endregion

        private void txt_amount_TextChanged(object sender, EventArgs e)
        {
            if (OnModifyValue != null && txt_amount.Text.Length>0)
            {
                OnModifyValue(txt_amount.Text);
            }
        }
    }
}
