//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : CashierWitholding.cs
// 
//   DESCRIPTION : Class to manage the generation of Witholding documents
//
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 09-FEB-2012 TJG    First release
// 16-FEB-2012 MPO    Create the documents "constancia" in a "for each"
// 22-MAR-2012 MPO    Move CheckWitholding and DB_CreateWithodingDoc to Common.Witholding
// 02-APR-2012 JCM    Control column length NAME, ID1, ID2 from holder
// 30-JAN-2013 MPO    #554: Remove page default setting because printer ticket has error
// 12-DEC-2013 RMS    Adapt document printing routines to accept a transaction
// 16-APR-2014 ICS, MPO & RCI    Fixed Bug WIG-830: Not enough available storage exception.
// 02-FEB-2015 MPO    WIG-1969: System crash when printing "constancias"
// 10-FEB-2015 MPO & DHA    WIG-2038: Error on printing federal "constancias"
// 29-SEP-2015 DHA    Product Backlog Item 3902: external withholing
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using WSI.Common;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Globalization;
using System.Windows.Forms;
using System.Collections;
using System.IO;
using Microsoft.Win32;
using System.Drawing.Printing;


namespace WSI.Cashier
{
  public static class PrintingDocuments
  {
    private const String SUFIX_TEMP_DOC = ".TempDocPrint";

    private static String REGISTERING_KEY_NAME = "WitholdingPrinterName";

    public enum STATUS_DOC_PRINTING
    {
      OK = 0,
      ERROR = 1,
      NO_DOCUMENTS_TO_PRINT = 2,
    }

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Print the witholding documents
    // 
    //  PARAMS:
    //      - INPUT :
    //          - OperationId: 
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:

    public static Boolean PrintDialogDocuments(Int64 OperationId)
    {
      STATUS_DOC_PRINTING _status;

      using (DB_TRX _db_trx = new DB_TRX())
      {
        _status = PrintDialogDocuments(OperationId, _db_trx.SqlTransaction);
      }

      if (_status == STATUS_DOC_PRINTING.OK)
      {
        //    - Withold document was saved OK and it was printed 
        // "Los documentos han sido enviados a la impresora seleccionada."
        frm_message.Show(Resource.String("STR_FRM_WITHOLDING_014"),
                         "",
                         MessageBoxButtons.OK,
                         MessageBoxIcon.Information,
                         Cashier.MainForm);
      }
      else if (_status == STATUS_DOC_PRINTING.ERROR)
      {
        //    - Withold document was saved OK but it was not printed 
        // "Los documentos fueron generados correctamente pero no fueron impresos."
        frm_message.Show(Resource.String("STR_FRM_WITHOLDING_013"),
                         Resource.String("STR_APP_GEN_MSG_WARNING"),
                         MessageBoxButtons.OK,
                         MessageBoxIcon.Warning,
                         Cashier.MainForm);
      }

      return _status != STATUS_DOC_PRINTING.ERROR;
    }

    public static STATUS_DOC_PRINTING PrintDialogDocuments(Int64 OperationId, SqlTransaction Trx)
    {
      DialogResult _dialog;
      PrinterSettings _ps_selected;
      PrinterSettings _ps_default;
      String _printer_name;

      _ps_default = new PrinterSettings();
      _printer_name = GetDefaultPrinter();
      if (!String.IsNullOrEmpty(_printer_name))
      {
        _ps_default.PrinterName = _printer_name;
      }

      _ps_default.Copies = 2;

      _dialog = SelectPrinterDlg(_ps_default, out _ps_selected);

      if (_dialog == DialogResult.OK)
      {
        SetDefaultPrinter(_ps_selected.PrinterName);
        return PrintDocuments(OperationId, _ps_selected.PrinterName, Trx);
      }

      return STATUS_DOC_PRINTING.ERROR;

    } // PrintDialogWitholdingDocuments

    #endregion

    #region Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Print the witholding documents
    // 
    //  PARAMS :
    //      - INPUT :
    //                OperationId:
    //                PrinterName: If is empty got the printer of SO
    //                SqlTrx:
    //
    //      - OUTPUT :
    //
    // RETURNS :
    // 
    //   NOTES :

    private static STATUS_DOC_PRINTING PrintDocuments(Int64 OperationId, String PrinterName)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        return PrintDocuments(OperationId, PrinterName, _db_trx.SqlTransaction);
      }
    }

    private static STATUS_DOC_PRINTING PrintDocuments(Int64 OperationId, String PrinterName, SqlTransaction Trx)
    {
      Int64 _document_id;
      StringBuilder _sb;
      DOCUMENT_TYPE _document_type;
      DocumentList _doc_list;
      GhostScriptParameter _print_params;
      Boolean _print_correct;
      String _str_log;
      String _tmp_filename;
      List<Int64> _documents;
      String[] _files_paths_to_delete;

      _documents = new List<Int64>();

      // print params for all documents
      _print_params = new GhostScriptParameter("", 2, PrinterName);

      _sb = new StringBuilder();

      _sb.AppendLine("SELECT AMP_DOCUMENT_ID1 FROM ACCOUNT_MAJOR_PRIZES WHERE AMP_OPERATION_ID = @pOperationId ");
      _sb.AppendLine("UNION");
      _sb.AppendLine("SELECT APO_DOCUMENT_ID FROM ACCOUNT_PAYMENT_ORDERS WHERE APO_OPERATION_ID = @pOperationId ");

      try
      {

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt).Value = OperationId;
          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            while (_reader.Read())
            {

              _document_id = _reader.IsDBNull(0) ? 0 : (Int64)_reader.GetInt64(0);

              if (_document_id == 0)
              {
                continue;
              }
              _documents.Add(_document_id);

            } // while
          } // using _reader
        } // using _cmd

        if (_documents.Count == 0)
        {
          return STATUS_DOC_PRINTING.NO_DOCUMENTS_TO_PRINT;
        }

        foreach (Int64 _id in _documents)
        {
          if (TableDocuments.Load(_id, out _document_type, out _doc_list, Trx))
          {
            foreach (IDocument _doc in _doc_list)
            {
              _tmp_filename = Path.Combine(Path.GetTempPath(), Path.GetTempFileName()) + SUFIX_TEMP_DOC + ".pdf";
              Template.Save(new MemoryStream(_doc.Content), _tmp_filename);
              _print_params.FileName = _tmp_filename;
              _print_correct = GSPrint.Print(_print_params);

              if (!_print_correct)
              {
                return STATUS_DOC_PRINTING.ERROR;
              }
            } //foreach docs retrieved
          } // if
        } // foreach documents id

        // DHA 10-FEB-2015: Delete all temporal files after print
        _files_paths_to_delete = Directory.GetFiles(Path.GetTempPath(), "*" + SUFIX_TEMP_DOC + ".pdf");

        foreach (String _delete_file_path in _files_paths_to_delete)
        {
          try
          {
            File.Delete(_delete_file_path);
          }
          catch
          {
          }
        }

        return STATUS_DOC_PRINTING.OK;
      }
      catch (Exception _ex)
      {
        _str_log = String.Format("PrintDocuments OpId={0}", OperationId.ToString());
        Log.Error(_str_log);
        Log.Exception(_ex);
      }

      return STATUS_DOC_PRINTING.ERROR;

    } // PrintDocuments

    //------------------------------------------------------------------------------
    // PURPOSE : Save in operating system registry the printer name used with print Witholding Document
    // 
    //  PARAMS:
    //      - INPUT :
    //          - PrinterName : 
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:

    private static void SetDefaultPrinter(String PrinterName)
    {

      RegistryKey _cashier;
      RegistryKey _common;

      _cashier = null;
      _common = null;

      try
      {

        _cashier = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\LK_SYSTEM\CASHIER", false);

        if (_cashier == null)
        {
          _cashier = Registry.CurrentUser.CreateSubKey(@"Software\LK_SYSTEM\CASHIER");
          _common = _cashier.CreateSubKey("Common", RegistryKeyPermissionCheck.ReadWriteSubTree);
        }
        else
        {
          _common = _cashier.OpenSubKey("Common", true);
        }

        _common.SetValue(REGISTERING_KEY_NAME, PrinterName);

      }
      finally
      {

        if (_common != null) _common.Close();
        if (_cashier != null) _cashier.Close();

      }

    } // SetWitholdingPrinter

    //------------------------------------------------------------------------------
    // PURPOSE : Get to operating system registry the printer name used with print Witholding Document
    // 
    //  PARAMS:
    //      - INPUT :
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      String: Printer name
    // 
    //   NOTES:

    private static String GetDefaultPrinter()
    {

      RegistryKey _cashier;
      RegistryKey _common;
      String _printer;

      _cashier = null;
      _common = null;
      _printer = "";

      try
      {

        _cashier = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\LK_SYSTEM\CASHIER", false);

        if (_cashier != null)
        {
          _common = _cashier.OpenSubKey("Common", false);
          _printer = _common.GetValue(REGISTERING_KEY_NAME).ToString();
        }

        return _printer;

      }
      catch
      {
        return "";
      }
      finally
      {

        if (_common != null) _common.Close();
        if (_cashier != null) _cashier.Close();

      }

    } // GetWitholdingPrinter

    //------------------------------------------------------------------------------
    // PURPOSE : Printer dialog
    // 
    //  PARAMS:
    //      - INPUT :
    //          - Card : Card Data
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:

    private static DialogResult SelectPrinterDlg(PrinterSettings DefaultPrinterSetting, out PrinterSettings PrinterSettingsResults)
    {
      PrintDialog _print_dialog;
      DialogResult _dialog_result;

      _print_dialog = new PrintDialog();

      _print_dialog.PrinterSettings = DefaultPrinterSetting;
      _print_dialog.UseEXDialog = true;
      _print_dialog.PrintToFile = false;
      _print_dialog.AllowPrintToFile = false;
      _print_dialog.AllowSomePages = false;
      _dialog_result = _print_dialog.ShowDialog();
      PrinterSettingsResults = _print_dialog.PrinterSettings;

      // ICS 14-APR-2014 Free the object
      _print_dialog.Dispose();

      return _dialog_result;

    } // SelectPrinterDlg

    #endregion

  } // PrintingDocuments class

}



