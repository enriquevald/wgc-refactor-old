//------------------------------------------------------------------------------
// Copyright © 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_card_assign.cs
// 
//   DESCRIPTION: Implements the following classes:
//                1. frm_card_assign
// 
//        AUTHOR: AA
// 
// CREATION DATE: 04-SEP-2007
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 04-SEP-2007 AA     First release.
// 25-MAY-2012 SSC    Moved CardData, PlaySession and PlayerTrackingData classes to WSI.Common
// 16-APR-2014 ICS, MPO & RCI    Fixed Bug WIG-830: Not enough available storage exception.
// 16-APR-2014 ICS & RCI    Fixed Bug WIG-837: Exception.
// 16-APR-2014 DDM & ICS    Fixed Bug WIG-838: Can't assign card.
// 01-JUL-2014 AMF    Personal Card Replacement
// 15-JUL-2014 AMF    Fixed Bug WIG-1082: Several errors in the new functionality
// 06-JUL-2015 FOS    CardPayMode when is Substituted
// 03-AUG-2015 ETP    Fixed BUG 3496 Use of common header for card replacement.
// 02-SEP-2015 DCS    Fixed Bug 4152 Show form transaction data when it's not necessary 
// 09-SEP-2015 YNM    Fixed Bug 4324: Card Replacement with cost = 0 Subtract commision amount
// 18-SEP-2015 ETP    Fixed bug 4540: corrected payment mode in uc_card_reader_OnTrackNumberReadEvent.
// 14-DEC-2015 FAV    Fixed Bug 7599: Errors with permissions for card replacement free
// 24-FEB-2016 DDS    Fixed Bug 9679: Error message 'Invalid card number' is partially hidden
// 09-MAR-2016 FOS    Fixed Bug 10139: The text on cards buttons doesn't show totally
// 24-MAR-2016 RAB    Bug 10745: Reception: with the GP requiereNewCard to "0" when assigning a card client is counted as "replacement"
// 12-APR-2016 ETP    Fixed bug 10703: Reposition of buttons for better allignment.
// 01-MAY-2016 ETP    Fixed Bugs 14045 & 14060: associate card not defiened.
// 16-OCT-2017 DHA    Bug 30260:WIGOS-5742 [Ticket #9527] Issue: Garcia River - Release 03.005.103 - Associate Card - Ask permission
// 14-NOV-2017 DHA    Bug 30792:WIGOS-6606 Player card substitution: remove chips buttons in payment method screen
// 22-DEC-2017 EOR    Bug 31053:WIGOS-6903 [Ticket #10771] Mty I: Error in screen "Histórico de transacciones bancarias", no appear the movement "Sustitución tarjeta - Pago con tarjeta bancaria" 
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using WSI.Common;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using WSI.Cashier.Controls;

namespace WSI.Cashier
{
  public partial class frm_card_assign : frm_base
  {
    #region Attributes

    private const String FREE_CURRENCY_CODE = "FREE";
    private const String NAME_BTN_FREE = "FREE-FREE-NONE";

    // Static attributes initialized in definition: Don't DISPOSE them!!
    private static frm_amount_input m_frm_amount_input = new frm_amount_input();
    private static frm_yesno m_form_shadow = new frm_yesno();

    private Int64 m_account_id;
    private UInt64 m_track_data;
    private CardData m_card_data = new CardData();
    private Currency m_replacement_price;
    private CardSettings m_card_setting = new CardSettings();
    private PAY_MODE_CARD_REPLACEMENT m_pay_mode = PAY_MODE_CARD_REPLACEMENT.CASH;
    private String m_currency_selected;
    private CurrencyExchangeResult m_exchange_result;
    private CardData m_account_carddata;
    private CurrencyExchangeSubType m_subtype;
    private String m_currency_code;
    private CurrencyExchangeType m_type;
    private Button m_last_button_selected;
    private String m_track_number;

    #endregion

    #region Constructor

    public frm_card_assign()
    {
      InitializeComponent();
      InitializeControlResources();
      Init();
      CreateButtons();
      uc_card_reader.ErrorMessageBelow = true;
      uc_card_reader.Height = 90;
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Initialize control resources (NLS strings, images, etc)
    /// </summary>
    private void InitializeControlResources()
    {
      // NLS Strings:
      this.FormTitle = Resource.String("STR_FRM_CARD_ASSIGN_001");    // Asociar Tarjeta

      gp_replacement_type.HeaderText = Resource.String("STR_FRM_AMOUNT_PAYMENT_METHOD_GROUP_BOX");      // Forma de pago 

      lbl_account.Text = Resource.String("STR_FORMAT_GENERAL_NAME_ACCOUNT");      // Cuenta 
      lbl_old_track.Text = Resource.String("STR_FRM_CARD_ASSIGN_002");            // Tarjeta Anterior
      lbl_new_card.Text = Resource.String("STR_FRM_CARD_ASSIGN_009");
      lbl_replacement_price.Text = Resource.String("STR_FRM_CARD_ASSIGN_006");    // Precio Substitución
      lbl_existent_card.Text = Resource.String("STR_FRM_CARD_ASSIGN_004");        // ¡NÚMERO DE TARJETA YA UTILIZADO!

      btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");     // Cancelar
      btn_accept.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_OK");
      gp_card_box.HeaderText = Resource.String("STR_FRM_CARD_ASSIGN_003");              // Información de la Tarjeta

      lbl_replacement_price_value.Text = String.Empty;

      m_frm_amount_input.InitializeControlResources();

    } // InitializeControlResources

    private void Init()
    {
      this.m_account_id = 0;
      this.m_track_data = 0;

      this.lbl_account_value.Text = "";
      this.lbl_old_track_value.Text = "";
      this.lbl_new_card_value.Text = "";
      this.lbl_replacement_price_value.Text = "";
    }

    private void btn_cancel_Click(object sender, EventArgs e)
    {
      this.Dispose();
    }

    private void uc_card_reader_OnTrackNumberReadEvent(string TrackNumber, ref bool IsValid)
    {

      m_subtype = CurrencyExchangeSubType.BANK_CARD;
      m_track_number = String.Empty;
      this.lbl_new_card_value.Text = String.Empty;

      if (IsValid)
      {

        if (!CardData.CheckTrackdataSiteId( new ExternalTrackData(TrackNumber) ))
        {

          lbl_existent_card.Text = Resource.String("STR_CARD_SITE_ID_ERROR");
          lbl_existent_card.Visible = true;
          timer1.Enabled = true;

          return;
        }

        // TJG 01-DEC-2009
        // Check provided trackdata is not associated to an existing account
        //    - Player Account
        //    - Mobile Bank Account

        //    - Player Account
        if (CashierBusinessLogic.DB_IsCardDefined(TrackNumber))
        {
          // This card is already assigned to a 'Player' account.
          frm_message.Show(Resource.String("STR_UC_MB_CARD_USER_MSG_CARD_ALREADY_LINKED_TO_USER_ACCT"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this);
          lbl_existent_card.Visible = true;
          timer1.Enabled = true;

          return;
        }

        //    - Mobile Bank Account
        if (CashierBusinessLogic.DB_IsMBCardDefined(TrackNumber))
        {
          // This card is already assigned to a 'Mobile Bank' account.
          frm_message.Show(Resource.String("STR_UC_CARD_USER_MSG_CARD_ALREADY_LINKED_TO_MB_ACCT"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this);
          lbl_existent_card.Visible = true;
          timer1.Enabled = true;

          return;
        }

        SetTypeAndSubTypeReplacementCardOperation();
        m_track_number = TrackNumber;

          }
      else // Is not valid
      {
          this.lbl_existent_card.Text = this.uc_card_reader.InvalidCardText;
        this.lbl_existent_card.Visible = !String.IsNullOrEmpty(TrackNumber);
          this.uc_card_reader.InvalidCardTextVisible = false;
      }

      this.lbl_new_card_value.Text = WSI.Common.Misc.VisibleTrackdata(m_track_number);
    }

    private void uc_card_reader_OnTrackNumberReadingEvent()
    {
        this.lbl_existent_card.Text = this.uc_card_reader.InvalidCardText;
      this.lbl_existent_card.Visible = this.uc_card_reader.InvalidCardTextVisible;
        this.uc_card_reader.InvalidCardTextVisible = false;
    }

    private void SetPayModeCard()
    {

      m_pay_mode = PAY_MODE_CARD_REPLACEMENT.CARD_GENERIC;
      m_subtype = CurrencyExchangeSubType.BANK_CARD;

      if (m_currency_selected.Contains("CREDIT"))
      {
        m_pay_mode = PAY_MODE_CARD_REPLACEMENT.CARD_CREDIT;
        m_subtype = CurrencyExchangeSubType.CREDIT_CARD;
      }
      else if (m_currency_selected.Contains("DEBIT"))
      {
        m_pay_mode = PAY_MODE_CARD_REPLACEMENT.CARD_DEBIT;
        m_subtype = CurrencyExchangeSubType.DEBIT_CARD;
      }

    }

    private void TypeCardReplacement(string TrackNumber)
    {
      TYPE_CARD_REPLACEMENT _card_replacement = new TYPE_CARD_REPLACEMENT();
      
      _card_replacement.type = m_pay_mode;
      _card_replacement.account_id = m_account_id;
      _card_replacement.old_track_data = m_card_data.TrackData;
      _card_replacement.new_track_data = TrackNumber;
      _card_replacement.AccountInfo = m_card_data.VoucherAccountInfo(TrackNumber);
      _card_replacement.card_replacement_price = m_replacement_price;
      _card_replacement.ExchangeResult = m_exchange_result;

      // RAB 24-MAR-2016: Bug 10745: Add CardPaid input parameter to function.
      if (!CashierBusinessLogic.DB_CardReplacementUpdateInfo(_card_replacement, m_card_data.CardPaid, this))
      {
        lbl_existent_card.Text = Resource.String("STR_FRM_CARD_ASSIGN_004");
        lbl_existent_card.Visible = true;
        timer1.Enabled = true;

        return;
      }
    }   //TypeCardReplacement

    private void timer1_Tick(object sender, EventArgs e)
    {
      this.lbl_existent_card.Visible = false;
      timer1.Enabled = false;
    }

    private void frm_card_assign_Shown(object sender, EventArgs e)
    {
      this.uc_card_reader.Focus();
    }

    private void National_Currency_Selected()
    {

      m_pay_mode = PAY_MODE_CARD_REPLACEMENT.CASH;

      ShowPriceOrPoints();

      SetReplacementPriceValues();

      this.uc_card_reader.Focus();
    }

    // adapted copy of frm_payment_method.cs/btn_ok_Click
    private Boolean SaveBankTransaction(CurrencyExchangeType Currencyexchangetype)
    {
      Boolean _save_bank_transaction;
      frm_bank_transaction_data _frm_bank_transaction;
      CardData _card_data;

      switch (Currencyexchangetype)
      {
        case CurrencyExchangeType.CARD:
          _save_bank_transaction = GeneralParam.GetBoolean("Cashier.PaymentMethod", "BankCard.EnterTransactionDetails");
          break;
        case CurrencyExchangeType.CHECK:
          _save_bank_transaction = GeneralParam.GetBoolean("Cashier.PaymentMethod", "Check.EnterTransactionDetails");
          break;
        default:
          _save_bank_transaction = false;
          break;
      }

      if (_save_bank_transaction)
      {
        _card_data = m_account_carddata;

        m_exchange_result.BankTransactionData.TransactionType = TransactionType.CARD_REPLACEMENT;
        m_exchange_result.SubType = m_subtype;

        _frm_bank_transaction = new frm_bank_transaction_data(m_exchange_result.InType);

        DialogResult _rc = _frm_bank_transaction.Show(ref m_exchange_result);
        _frm_bank_transaction.Dispose();

        if (_rc != DialogResult.OK)
        {
          return false;
        }
      }

      return true;
    }

    private void CalculateCommissions(CurrencyExchangeType Currencyexchangetype, String CurrencyCode)
    {

      CurrencyExchange _selected_currency;

      _selected_currency = new CurrencyExchange();
      CurrencyExchange.ReadCurrencyExchange(Currencyexchangetype, CurrencyCode, out _selected_currency);
      _selected_currency.ApplyExchange((Decimal)m_replacement_price, out m_exchange_result, true);
    }

    private void ShowPriceOrPoints()
    {
      switch (m_pay_mode)
      {
        case PAY_MODE_CARD_REPLACEMENT.POINT:
          lbl_replacement_price.Text = "Puntos";
          break;
        case PAY_MODE_CARD_REPLACEMENT.FREE:
        case PAY_MODE_CARD_REPLACEMENT.CASH:
        default:
          lbl_replacement_price.Text = Resource.String("STR_FRM_CARD_ASSIGN_006");    // Precio Substitución
          break;
      }
    }

    private void SetReplacementCommissions()
    {

      SetTypeAndSubTypeReplacementCardOperation();

      CalculateCommissions(m_type, m_currency_code);

      lbl_replacement_commission.Text = Resource.String("STR_FRM_CARD_ASSIGN_007");  //Comisión
      lbl_replacement_commission_value.Text = ((Currency)m_exchange_result.Comission).ToString();
    }

    private void SetTotalPriceReplacement()
    {
      Decimal total;
      // RAB 24-MAR-2016: Bug 10745: Define total to CardPaid variable.
      total = m_pay_mode == PAY_MODE_CARD_REPLACEMENT.FREE
        ? 0                       //EOR 22-DEC-2017
        : (m_card_data.CardPaid ? m_replacement_price : m_card_setting.PersonalCardPrice) + m_exchange_result.Comission;
      lbl_replacement_total_price.Text = Resource.String("STR_FRM_CARD_ASSIGN_008"); //Monto Total
      lbl_replacement_total_price_value.Text = ((Currency)total).ToString();
    }

    private void SetTypeAndSubTypeReplacementCardOperation()
    {
      String _currency_type;


      m_currency_code = m_currency_selected.Substring(0, m_currency_selected.IndexOf("-"));
      _currency_type = m_currency_selected.Substring(m_currency_selected.IndexOf("-") + 1, m_currency_selected.LastIndexOf("-") - m_currency_selected.IndexOf("-") - 1);

      switch (_currency_type)
      {
        case "CHECK":
          m_pay_mode = PAY_MODE_CARD_REPLACEMENT.CHECK;
          m_subtype = CurrencyExchangeSubType.CHECK;
          m_type = CurrencyExchangeType.CHECK;
          break;
        case "CARD":
          SetPayModeCard();
          m_type = CurrencyExchangeType.CARD;
          break;
        case "CREDITLINE":
          m_pay_mode = PAY_MODE_CARD_REPLACEMENT.CREDITLINE;
          break;
        default:
          m_subtype = CurrencyExchangeSubType.NONE;
          m_type = CurrencyExchangeType.CURRENCY;
          break;
      }
    }

    // adapted copy of frm_browse_currencies.cs.cs/CreateButtons
    private void CreateButtons()
    {
      Boolean _distinct_bank_card;
      CurrencyExchange _national_currency;
      CurrencyExchange _free_currency;
      List<CurrencyExchange> _currencies_exchange;
      Int32 _currency_position_x;
      Int32 _currency_position_y;
      Int32 _width;
      Int32 _height;
      Int32 _initial_x;
      Int32 _initial_y;
      Int32 _separation;
      uc_round_button _btn;
      Panel _pnl;
      CurrencyExchangeProperties _properties;
      Boolean _is_selected;
      Int32 _currencies_print_count;
      Int32 _button_number;

      _distinct_bank_card = GeneralParam.GetBoolean("Cashier.PaymentMethod", "BankCard.DifferentiateType", false);
      CurrencyExchange.GetAllowedCurrencies(true, null, false, _distinct_bank_card, false, false, out _national_currency, out _currencies_exchange);
      _is_selected = true;

      _initial_x = 2;
      _initial_y = 2;

      _width = 91;
      _height = 91;
      _separation = 2;
      _currencies_print_count = 0;

      //remove non national iso code.
      _currencies_exchange.RemoveAll(x => !(_national_currency.CurrencyCode.Equals(x.CurrencyCode)));

      //add free currency button
      _free_currency = new CurrencyExchange();
      _free_currency.CurrencyCode = FREE_CURRENCY_CODE;
      _free_currency.Type = CurrencyExchangeType.FREE;
      _free_currency.SubType = CurrencyExchangeSubType.NONE;
      _free_currency.Description = Resource.String("STR_FRM_CARD_ASSIGN_FREE");                                // Gratis
      _currencies_exchange.Add(_free_currency);

      _button_number = _currencies_exchange.Count;

      switch (_button_number)
      {
        case 1:
          _initial_x = 190;
          _separation = 200;
          break;
        case 2:
          _separation = 95;
          _initial_x = 90;
          break;
        case 3:
          _separation = 95;
          break;
        case 4:
          _separation = 32;
          break;
        default:
          _separation = 2;
          break;
      }
      _currency_position_x = _initial_x;
      _currency_position_y = _initial_y;


      foreach (CurrencyExchange _currency in _currencies_exchange)
      {
          _btn = new uc_round_button();
          _btn.Name = _currency.CurrencyCode + "-" + _currency.Type + "-" + _currency.SubType;
          if (m_currency_selected == null)
          {
            m_currency_selected = _btn.Name;
            m_last_button_selected = _btn;
          }

          _properties = CurrencyExchangeProperties.GetProperties(_currency.CurrencyCode);
          if (_properties != null)
          {
            _btn.Image = _properties.GetIcon(_currency.Type, 45, 45, true);
          }

        _btn.Size = new Size(_width, _height);
          _btn.ImageAlign = ContentAlignment.TopCenter;
          _btn.Style = uc_round_button.RoundButonStyle.PAYMENT_METHOD;
          _btn.Text = _currency.Description;
          _btn.TextAlign = ContentAlignment.BottomCenter;
          _btn.TextImageRelation = TextImageRelation.ImageAboveText;
          _btn.Click += new EventHandler(_btn_Click);



            if (_currency_position_x + _width + 3 >= pnl_container.Width)
            {
              _currency_position_y += _height + _initial_y;
              _currency_position_x = _initial_x;

            }
            _btn.Location = new Point(1, 1);

            _pnl = new Panel();
            _pnl.Anchor = System.Windows.Forms.AnchorStyles.Top | AnchorStyles.Left;
            _pnl.Controls.Add(_btn);
            _pnl.Location = new System.Drawing.Point(_currency_position_x, _currency_position_y);
            _pnl.Name = "pnl_" + _currency.CurrencyCode + "-" + _currency.Type + "-" + _currency.SubType;
            _pnl.Size = new System.Drawing.Size(_width, _height);
            _btn.IsSelected = _is_selected;
            _is_selected = false;    // only paints first in selected mode

            _currencies_print_count = _currencies_print_count + 1;

            pnl_container.Controls.Add(_pnl);
        _currency_position_x += _width + _separation;
      }// foreach

      //gp_replacement_type.Height = pnl_container.Height + 18;


    }   //CreateButtons

    private void _btn_Click(object sender, EventArgs e)
    {
      Boolean _panel_selected;

      _panel_selected = false;
      // Unselect previous button, select new button, identify new button
      foreach (Panel _pnl in pnl_container.Controls)
      {
        foreach (uc_round_button _btn in _pnl.Controls)
        {
          if (!_panel_selected && String.IsNullOrEmpty(((Button)sender).Name))
          {
            _btn.IsSelected = true;
            m_currency_selected = _btn.Name;
            _panel_selected = true;
            break;
          }

          if (_pnl.Name == "pnl_" + ((Button)sender).Name)
          {
            _btn.IsSelected = true;
            m_currency_selected = _btn.Name;
          }
          else
          {
            _btn.IsSelected = false;
            _pnl.Refresh();
          }

        }
      }

      //set the new replacement price, if necessary
      if (m_currency_selected == NAME_BTN_FREE)
      {
        // The validation was done
        Free_Selected();
      }
      else
      {
        m_pay_mode = PAY_MODE_CARD_REPLACEMENT.CASH;
        m_last_button_selected = (Button)sender;
        SetReplacementPriceValues();
      }

    }   //_btn_Click

    private void SetReplacementPriceValues()
    {
      SetReplacementPrice();

      SetReplacementCommissions();

      SetTotalPriceReplacement();
    }

    private void Free_Selected()
    {
      uc_round_button _btn;
      Boolean _isValid;
      
      _isValid = true;
      _btn = new uc_round_button();

      if (gp_replacement_type.Enabled) // CardPrice <> 0 isn't necesarry validate permission
      {
          m_replacement_price = 0;

          // RAB 24-MAR-2016: Bug 10745: Define replacement price value text according to CardPaid value.
          this.lbl_replacement_price_value.Text = m_card_data.CardPaid ? m_card_setting.PersonalCardReplacementPrice.ToString() : m_card_setting.PersonalCardPrice.ToString();

          m_pay_mode = PAY_MODE_CARD_REPLACEMENT.FREE;

          ShowPriceOrPoints();
        //}
        //else
        //{
        //  _btn_Click(m_last_button_selected, null);
        //  _isValid = false;
        //}
      }

      if (_isValid && m_currency_selected != NAME_BTN_FREE)
      {
        //_btn.Name = NAME_BTN_FREE;

        _btn_Click(_btn, null);
      }

      SetReplacementCommissions();

      SetTotalPriceReplacement();

      this.uc_card_reader.Focus();

    }

    private void SetReplacementPrice()
    {
      if ((m_card_data.CardReplacementCount >= m_card_setting.PersonalCardReplacementChargeAfterTimes) &&
        (m_card_setting.PersonalCardReplacementPrice > 0))
      {
        // RAB 24-MAR-2016: Bug 10745: Define replacement price value according to CardPaid value.
        m_replacement_price = m_card_data.CardPaid ? m_card_setting.PersonalCardReplacementPrice : m_card_setting.PersonalCardPrice;
      }
      else
      {
        m_replacement_price = m_card_data.CardPaid ? 0 : m_card_setting.PersonalCardPrice;
      }


    }

    #endregion

    #region Properties

    #endregion
        
    #region Public Methods

    public void CardAssignParams(CardData AccountCardData, UInt64 TrackData, String VisibleTrackdata, out CurrencyExchangeResult ExchangeResult)
    {
      ExchangeResult = new CurrencyExchangeResult();

      this.m_account_carddata = AccountCardData;
      this.m_account_id = AccountCardData.AccountId;
      this.m_track_data = TrackData;
      this.m_replacement_price = 0;

      this.lbl_account_value.Text = this.m_account_id.ToString();
      this.lbl_old_track_value.Text = VisibleTrackdata;

      // TJG 01-MAR-2010
      // Replacement of personal cards may be charged

      //    - Get account's current card data
      if (!CardData.DB_CardGetAllData(m_account_id, m_card_data))
      {
        Log.Error("uc_card_reader1_OnTrackNumberReadEvent: DB_GetCardAllData. Error reading card.");

        return;
      }

      // RAB 24-MAR-2016: Bug 10745
      CardData.DB_CardPriceSettings(ref m_card_setting);

      //    - Check whether it is a Personal Card 
      if (m_card_data.CardPaid && m_card_data.PlayerTracking.CardLevel > 0)
      {
        // Default MXN
        National_Currency_Selected();

        this.lbl_replacement_price_value.Text = m_replacement_price.ToString();
      }

      SetReplacementPriceValues();

      if ((m_card_data.PlayerTracking.CardLevel == 0) || (m_replacement_price == 0))
      {
        Free_Selected();
        SetTotalPriceReplacement();

        gp_replacement_type.Enabled = m_replacement_price != 0;

        gp_replacement_type.Refresh();
      }

      ExchangeResult = m_exchange_result;
    }

    #endregion

    private void btn_accept_Click(object sender, EventArgs e)
    {
      String _error_str;

      CurrencyExchangeType _type;

      if (String.IsNullOrEmpty(m_track_number))
      {
        return;
      }

      switch (m_pay_mode)
      {
        case PAY_MODE_CARD_REPLACEMENT.CARD_CREDIT:
        case PAY_MODE_CARD_REPLACEMENT.CARD_DEBIT:
        case PAY_MODE_CARD_REPLACEMENT.CARD_GENERIC:
          _type = CurrencyExchangeType.CARD;
          break;

        case PAY_MODE_CARD_REPLACEMENT.CHECK:
          _type = CurrencyExchangeType.CHECK;
          break;

        case PAY_MODE_CARD_REPLACEMENT.CASH:
        case PAY_MODE_CARD_REPLACEMENT.CASH_CURRENCY:
          _type = CurrencyExchangeType.CURRENCY;
          break;

        case PAY_MODE_CARD_REPLACEMENT.FREE:
        default:
          _type = CurrencyExchangeType.FREE;
          break;
      }

      if (m_replacement_price == 0 && !ProfilePermissions.CheckPermissionList(ProfilePermissions.CashierFormFuncionality.CardReplacementForFree,
                                                 ProfilePermissions.TypeOperation.RequestPasswd,
                                                 this,
                                                 0,
                                                 out _error_str))
      {
        return;
      }

      // TJG 01-MAR-2010
      // Replacement of personal cards may be charged
      if (m_replacement_price > 0)
      {
        Currency _aux_amount;

        //check commissions
        CalculateCommissions(m_type, m_currency_code);

        m_form_shadow.Opacity = 0.6;
        m_form_shadow.Show();

        // CardReplacement voucher uses LoggedInTerminalName string to pass the track data related to the new card read
        m_card_data.LoggedInTerminalName = m_track_number;

        // RAB 24-MAR-2016: Bug 10745: Define CaptionText accordint to CardPaid variable.
        if (!m_frm_amount_input.Show(m_card_data.CardPaid ? Resource.String("STR_FRM_CARD_ASSIGN_005") : Resource.String("STR_FRM_CARD_ASSIGN_001"),
                                     out _aux_amount,
                                     VoucherTypes.CardReplacement,
                                     m_card_data,
                                     m_replacement_price,
                                     CASH_MODE.TOTAL_REDEEM,
                                     m_form_shadow,
                                     m_exchange_result))
        {
          m_form_shadow.Hide();
          this.Focus();

          return;
        }

        m_form_shadow.Hide();
        this.Focus();

        if (m_pay_mode == PAY_MODE_CARD_REPLACEMENT.CARD_CREDIT || m_pay_mode == PAY_MODE_CARD_REPLACEMENT.CARD_DEBIT || m_pay_mode == PAY_MODE_CARD_REPLACEMENT.CARD_GENERIC || m_pay_mode == PAY_MODE_CARD_REPLACEMENT.CHECK)
        {
          if (!SaveBankTransaction(_type))
          {
            return;
          }
          //if (m_exchange_result != null && m_exchange_result.InType == CurrencyExchangeType.CARD && Common.Cashier.ReadPinPadEnabled())
          //{
          //  frm_tpv form = new frm_tpv(m_exchange_result, OutputParameter.OperationId, m_card_data);
          //  if (!form.Show())
          //  {
          //    uc_card.UndoCreditCardTPV(m_card_data, OutputParameter.OperationId, out _voucher_list_undo);
          //  }
          //}  
        }
      }
      else
      {
        if (m_pay_mode == PAY_MODE_CARD_REPLACEMENT.POINT)
        {
          if (m_card_data.PlayerTracking.TruncatedPoints < m_card_setting.PersonalCardReplacementPriceInPoints)
          {
            frm_message.Show(Resource.String("STR_FRM_CARD_ASSIGN_POINTS_ERROR"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this);
            timer1.Enabled = true;

            return;
          }

          frm_gift_request _form_gift_request = new frm_gift_request(m_card_setting.PersonalCardReplacementPriceInPoints, m_card_data);
          if (!_form_gift_request.Show(m_form_shadow))
          {
            m_form_shadow.Hide();
            this.Focus();
            timer1.Enabled = true;

            return;
          }
          m_form_shadow.Hide();
          this.Focus();
        }
      }

      TypeCardReplacement(m_track_number);

      this.Dispose();
    }

  }
}