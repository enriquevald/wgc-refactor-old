﻿namespace WSI.Cashier
{
  partial class frm_reception_ticket
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
      this.pb_currency = new System.Windows.Forms.PictureBox();
      this.btn_selected_currency = new WSI.Cashier.Controls.uc_round_button();
      this.lbl_paymet_method = new WSI.Cashier.Controls.uc_label();
      this.gb_payment = new WSI.Cashier.Controls.uc_round_panel();
      this.lbl_currency_iso_code = new WSI.Cashier.Controls.uc_label();
      this.lbl_total_to_pay = new WSI.Cashier.Controls.uc_label();
      this.gb_total = new WSI.Cashier.Controls.uc_round_panel();
      this.web_browser = new System.Windows.Forms.WebBrowser();
      this.btn_cancel = new WSI.Cashier.Controls.uc_round_button();
      this.btn_ok = new WSI.Cashier.Controls.uc_round_button();
      this.gb_entry_details = new WSI.Cashier.Controls.uc_round_panel();
      this.btn_apply_coupon = new WSI.Cashier.Controls.uc_round_button();
      this.dgv_prices = new WSI.Cashier.Controls.uc_DataGridView();
      this.cb_data_agreement = new WSI.Cashier.Controls.uc_checkBox();
      this.txt_coupon_number = new WSI.Cashier.Controls.uc_round_textbox();
      this.lb_entry_price = new WSI.Cashier.Controls.uc_label();
      this.lb_coupon_number = new WSI.Cashier.Controls.uc_label();
      this.lbl_currency = new WSI.Cashier.Controls.uc_label();
      this.btn_keyboard = new WSI.Cashier.Controls.uc_round_button();
      this.pnl_data.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pb_currency)).BeginInit();
      this.gb_payment.SuspendLayout();
      this.gb_total.SuspendLayout();
      this.gb_entry_details.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dgv_prices)).BeginInit();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.Controls.Add(this.btn_keyboard);
      this.pnl_data.Controls.Add(this.lbl_currency);
      this.pnl_data.Controls.Add(this.gb_entry_details);
      this.pnl_data.Controls.Add(this.btn_cancel);
      this.pnl_data.Controls.Add(this.btn_ok);
      this.pnl_data.Controls.Add(this.gb_total);
      this.pnl_data.Controls.Add(this.web_browser);
      this.pnl_data.Controls.Add(this.gb_payment);
      this.pnl_data.Size = new System.Drawing.Size(743, 650);
      // 
      // pb_currency
      // 
      this.pb_currency.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.pb_currency.BackColor = System.Drawing.Color.Transparent;
      this.pb_currency.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.pb_currency.Location = new System.Drawing.Point(15, 52);
      this.pb_currency.Name = "pb_currency";
      this.pb_currency.Size = new System.Drawing.Size(36, 36);
      this.pb_currency.TabIndex = 46;
      this.pb_currency.TabStop = false;
      // 
      // btn_selected_currency
      // 
      this.btn_selected_currency.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.btn_selected_currency.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_selected_currency.FlatAppearance.BorderSize = 0;
      this.btn_selected_currency.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_selected_currency.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_selected_currency.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_selected_currency.Image = null;
      this.btn_selected_currency.IsSelected = false;
      this.btn_selected_currency.Location = new System.Drawing.Point(235, 50);
      this.btn_selected_currency.Name = "btn_selected_currency";
      this.btn_selected_currency.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_selected_currency.Size = new System.Drawing.Size(130, 50);
      this.btn_selected_currency.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_selected_currency.TabIndex = 1;
      this.btn_selected_currency.Text = "XCHANGE";
      this.btn_selected_currency.UseVisualStyleBackColor = false;
      this.btn_selected_currency.Click += new System.EventHandler(this.btn_selected_currency_Click);
      // 
      // lbl_paymet_method
      // 
      this.lbl_paymet_method.BackColor = System.Drawing.Color.Transparent;
      this.lbl_paymet_method.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_paymet_method.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_paymet_method.Location = new System.Drawing.Point(71, 51);
      this.lbl_paymet_method.Name = "lbl_paymet_method";
      this.lbl_paymet_method.Size = new System.Drawing.Size(161, 47);
      this.lbl_paymet_method.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_paymet_method.TabIndex = 0;
      this.lbl_paymet_method.Text = "xPayment Method";
      this.lbl_paymet_method.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // gb_payment
      // 
      this.gb_payment.BackColor = System.Drawing.Color.Transparent;
      this.gb_payment.BorderColor = System.Drawing.Color.Empty;
      this.gb_payment.Controls.Add(this.lbl_currency_iso_code);
      this.gb_payment.Controls.Add(this.pb_currency);
      this.gb_payment.Controls.Add(this.btn_selected_currency);
      this.gb_payment.Controls.Add(this.lbl_paymet_method);
      this.gb_payment.CornerRadius = 10;
      this.gb_payment.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_payment.ForeColor = System.Drawing.Color.Black;
      this.gb_payment.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gb_payment.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_payment.HeaderHeight = 35;
      this.gb_payment.HeaderSubText = null;
      this.gb_payment.HeaderText = "XPAYMENT METHOD";
      this.gb_payment.Location = new System.Drawing.Point(14, 6);
      this.gb_payment.Name = "gb_payment";
      this.gb_payment.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_payment.Size = new System.Drawing.Size(380, 115);
      this.gb_payment.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.gb_payment.TabIndex = 4;
      // 
      // lbl_currency_iso_code
      // 
      this.lbl_currency_iso_code.AutoSize = true;
      this.lbl_currency_iso_code.BackColor = System.Drawing.Color.Transparent;
      this.lbl_currency_iso_code.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_currency_iso_code.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_currency_iso_code.Location = new System.Drawing.Point(18, 91);
      this.lbl_currency_iso_code.Name = "lbl_currency_iso_code";
      this.lbl_currency_iso_code.Size = new System.Drawing.Size(35, 17);
      this.lbl_currency_iso_code.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_currency_iso_code.TabIndex = 2;
      this.lbl_currency_iso_code.Text = "xISO";
      // 
      // lbl_total_to_pay
      // 
      this.lbl_total_to_pay.BackColor = System.Drawing.Color.Transparent;
      this.lbl_total_to_pay.Font = new System.Drawing.Font("Open Sans Semibold", 32F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_total_to_pay.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.lbl_total_to_pay.Location = new System.Drawing.Point(3, 50);
      this.lbl_total_to_pay.Name = "lbl_total_to_pay";
      this.lbl_total_to_pay.Size = new System.Drawing.Size(314, 46);
      this.lbl_total_to_pay.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_32PX_WHITE;
      this.lbl_total_to_pay.TabIndex = 26;
      this.lbl_total_to_pay.Text = "$XXX.XX";
      this.lbl_total_to_pay.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // gb_total
      // 
      this.gb_total.BackColor = System.Drawing.Color.Transparent;
      this.gb_total.BorderColor = System.Drawing.Color.Empty;
      this.gb_total.Controls.Add(this.lbl_total_to_pay);
      this.gb_total.CornerRadius = 10;
      this.gb_total.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_total.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.gb_total.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_total.HeaderHeight = 35;
      this.gb_total.HeaderSubText = null;
      this.gb_total.HeaderText = "XTOTAL";
      this.gb_total.Location = new System.Drawing.Point(410, 452);
      this.gb_total.Name = "gb_total";
      this.gb_total.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.gb_total.Size = new System.Drawing.Size(320, 115);
      this.gb_total.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.TOTAL;
      this.gb_total.TabIndex = 9;
      // 
      // web_browser
      // 
      this.web_browser.Location = new System.Drawing.Point(410, 6);
      this.web_browser.MinimumSize = new System.Drawing.Size(20, 20);
      this.web_browser.Name = "web_browser";
      this.web_browser.Size = new System.Drawing.Size(320, 440);
      this.web_browser.TabIndex = 8;
      // 
      // btn_cancel
      // 
      this.btn_cancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btn_cancel.FlatAppearance.BorderSize = 0;
      this.btn_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_cancel.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_cancel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_cancel.Image = null;
      this.btn_cancel.IsSelected = false;
      this.btn_cancel.Location = new System.Drawing.Point(410, 591);
      this.btn_cancel.Name = "btn_cancel";
      this.btn_cancel.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_cancel.Size = new System.Drawing.Size(155, 51);
      this.btn_cancel.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_cancel.TabIndex = 46;
      this.btn_cancel.Text = "CANCEL";
      this.btn_cancel.UseVisualStyleBackColor = false;
      this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
      // 
      // btn_ok
      // 
      this.btn_ok.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_ok.FlatAppearance.BorderSize = 0;
      this.btn_ok.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_ok.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_ok.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_ok.Image = null;
      this.btn_ok.IsSelected = false;
      this.btn_ok.Location = new System.Drawing.Point(575, 591);
      this.btn_ok.Name = "btn_ok";
      this.btn_ok.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_ok.Size = new System.Drawing.Size(155, 51);
      this.btn_ok.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_ok.TabIndex = 47;
      this.btn_ok.Text = "OK";
      this.btn_ok.UseVisualStyleBackColor = false;
      this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
      // 
      // gb_entry_details
      // 
      this.gb_entry_details.BackColor = System.Drawing.Color.Transparent;
      this.gb_entry_details.BorderColor = System.Drawing.Color.Empty;
      this.gb_entry_details.Controls.Add(this.btn_apply_coupon);
      this.gb_entry_details.Controls.Add(this.dgv_prices);
      this.gb_entry_details.Controls.Add(this.cb_data_agreement);
      this.gb_entry_details.Controls.Add(this.txt_coupon_number);
      this.gb_entry_details.Controls.Add(this.lb_entry_price);
      this.gb_entry_details.Controls.Add(this.lb_coupon_number);
      this.gb_entry_details.CornerRadius = 10;
      this.gb_entry_details.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_entry_details.ForeColor = System.Drawing.Color.Black;
      this.gb_entry_details.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gb_entry_details.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_entry_details.HeaderHeight = 35;
      this.gb_entry_details.HeaderSubText = null;
      this.gb_entry_details.HeaderText = "XENTRY DETAILS";
      this.gb_entry_details.Location = new System.Drawing.Point(14, 127);
      this.gb_entry_details.Name = "gb_entry_details";
      this.gb_entry_details.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_entry_details.Size = new System.Drawing.Size(380, 440);
      this.gb_entry_details.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.gb_entry_details.TabIndex = 47;
      // 
      // btn_apply_coupon
      // 
      this.btn_apply_coupon.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.btn_apply_coupon.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_apply_coupon.FlatAppearance.BorderSize = 0;
      this.btn_apply_coupon.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_apply_coupon.Font = new System.Drawing.Font("Open Sans Semibold", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_apply_coupon.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_apply_coupon.Image = null;
      this.btn_apply_coupon.IsSelected = false;
      this.btn_apply_coupon.Location = new System.Drawing.Point(329, 314);
      this.btn_apply_coupon.Name = "btn_apply_coupon";
      this.btn_apply_coupon.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_apply_coupon.Size = new System.Drawing.Size(40, 47);
      this.btn_apply_coupon.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_apply_coupon.TabIndex = 9;
      this.btn_apply_coupon.Text = "XAPPLY COUPON";
      this.btn_apply_coupon.UseVisualStyleBackColor = false;
      this.btn_apply_coupon.Click += new System.EventHandler(this.btn_apply_coupon_Click);
      // 
      // dgv_prices
      // 
      this.dgv_prices.AllowUserToAddRows = false;
      this.dgv_prices.AllowUserToDeleteRows = false;
      this.dgv_prices.AllowUserToResizeColumns = false;
      this.dgv_prices.AllowUserToResizeRows = false;
      this.dgv_prices.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.dgv_prices.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.dgv_prices.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
      this.dgv_prices.ColumnHeaderHeight = 35;
      this.dgv_prices.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
      dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      dataGridViewCellStyle1.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
      dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
      dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
      this.dgv_prices.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
      this.dgv_prices.ColumnHeadersHeight = 35;
      this.dgv_prices.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
      this.dgv_prices.CornerRadius = 10;
      this.dgv_prices.DefaultCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.dgv_prices.DefaultCellForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.dgv_prices.DefaultCellSelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.dgv_prices.DefaultCellSelectionForeColor = System.Drawing.Color.White;
      dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      dataGridViewCellStyle2.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.White;
      dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
      this.dgv_prices.DefaultCellStyle = dataGridViewCellStyle2;
      this.dgv_prices.EnableHeadersVisualStyles = false;
      this.dgv_prices.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.dgv_prices.GridColor = System.Drawing.Color.White;
      this.dgv_prices.HeaderBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
      this.dgv_prices.HeaderDefaultCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.dgv_prices.HeaderDefaultCellForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.dgv_prices.Location = new System.Drawing.Point(16, 68);
      this.dgv_prices.Name = "dgv_prices";
      this.dgv_prices.ReadOnly = true;
      this.dgv_prices.RowHeadersVisible = false;
      this.dgv_prices.RowHeadersWidth = 35;
      this.dgv_prices.RowTemplate.Height = 45;
      this.dgv_prices.RowTemplateHeight = 45;
      this.dgv_prices.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
      this.dgv_prices.Size = new System.Drawing.Size(350, 234);
      this.dgv_prices.Style = WSI.Cashier.Controls.uc_DataGridView.DataGridViewStyle.PROMOTIONS;
      this.dgv_prices.TabIndex = 8;
      this.dgv_prices.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_prices_CellClick);
      this.dgv_prices.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgv_prices_CellFormatting);
      // 
      // cb_data_agreement
      // 
      this.cb_data_agreement.BackColor = System.Drawing.Color.Transparent;
      this.cb_data_agreement.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.cb_data_agreement.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.cb_data_agreement.Location = new System.Drawing.Point(11, 364);
      this.cb_data_agreement.MinimumSize = new System.Drawing.Size(366, 30);
      this.cb_data_agreement.Name = "cb_data_agreement";
      this.cb_data_agreement.Padding = new System.Windows.Forms.Padding(5);
      this.cb_data_agreement.Size = new System.Drawing.Size(366, 65);
      this.cb_data_agreement.TabIndex = 0;
      this.cb_data_agreement.Text = "XAgreement Text";
      this.cb_data_agreement.UseVisualStyleBackColor = false;
      this.cb_data_agreement.CheckedChanged += new System.EventHandler(this.cb_data_agreement_CheckedChanged);
      // 
      // txt_coupon_number
      // 
      this.txt_coupon_number.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_coupon_number.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_coupon_number.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_coupon_number.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_coupon_number.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_coupon_number.CornerRadius = 5;
      this.txt_coupon_number.Font = new System.Drawing.Font("Open Sans Semibold", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_coupon_number.Location = new System.Drawing.Point(146, 314);
      this.txt_coupon_number.MaxLength = 18;
      this.txt_coupon_number.Multiline = false;
      this.txt_coupon_number.Name = "txt_coupon_number";
      this.txt_coupon_number.PasswordChar = '\0';
      this.txt_coupon_number.ReadOnly = false;
      this.txt_coupon_number.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_coupon_number.SelectedText = "";
      this.txt_coupon_number.SelectionLength = 0;
      this.txt_coupon_number.SelectionStart = 0;
      this.txt_coupon_number.Size = new System.Drawing.Size(177, 47);
      this.txt_coupon_number.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC_CUSTOM;
      this.txt_coupon_number.TabIndex = 7;
      this.txt_coupon_number.TabStop = false;
      this.txt_coupon_number.Text = "COD123";
      this.txt_coupon_number.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      this.txt_coupon_number.UseSystemPasswordChar = false;
      this.txt_coupon_number.WaterMark = null;
      this.txt_coupon_number.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // lb_entry_price
      // 
      this.lb_entry_price.BackColor = System.Drawing.Color.Transparent;
      this.lb_entry_price.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lb_entry_price.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lb_entry_price.Location = new System.Drawing.Point(12, 37);
      this.lb_entry_price.Name = "lb_entry_price";
      this.lb_entry_price.Size = new System.Drawing.Size(353, 28);
      this.lb_entry_price.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lb_entry_price.TabIndex = 2;
      this.lb_entry_price.Text = "xEntry Price";
      this.lb_entry_price.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lb_coupon_number
      // 
      this.lb_coupon_number.BackColor = System.Drawing.Color.Transparent;
      this.lb_coupon_number.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lb_coupon_number.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lb_coupon_number.Location = new System.Drawing.Point(12, 314);
      this.lb_coupon_number.Name = "lb_coupon_number";
      this.lb_coupon_number.Size = new System.Drawing.Size(140, 47);
      this.lb_coupon_number.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lb_coupon_number.TabIndex = 3;
      this.lb_coupon_number.Text = "xCoupon Number";
      this.lb_coupon_number.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_currency
      // 
      this.lbl_currency.BackColor = System.Drawing.Color.Transparent;
      this.lbl_currency.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_currency.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_currency.Location = new System.Drawing.Point(407, 570);
      this.lbl_currency.Name = "lbl_currency";
      this.lbl_currency.Size = new System.Drawing.Size(231, 18);
      this.lbl_currency.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_currency.TabIndex = 49;
      this.lbl_currency.Text = "xMsgCurrency";
      this.lbl_currency.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // btn_keyboard
      // 
      this.btn_keyboard.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
      this.btn_keyboard.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(47)))), ((int)(((byte)(50)))));
      this.btn_keyboard.FlatAppearance.BorderSize = 0;
      this.btn_keyboard.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_keyboard.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_keyboard.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_keyboard.Image = global::WSI.Cashier.Properties.Resources.keyboard;
      this.btn_keyboard.IsSelected = false;
      this.btn_keyboard.Location = new System.Drawing.Point(14, 597);
      this.btn_keyboard.Name = "btn_keyboard";
      this.btn_keyboard.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_keyboard.Size = new System.Drawing.Size(75, 45);
      this.btn_keyboard.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.TERTIARY;
      this.btn_keyboard.TabIndex = 50;
      this.btn_keyboard.TabStop = false;
      this.btn_keyboard.UseVisualStyleBackColor = false;
      this.btn_keyboard.Click += new System.EventHandler(this.btn_keyboard_Click);
      // 
      // frm_reception_ticket
      // 
      this.ClientSize = new System.Drawing.Size(743, 705);
      this.Name = "frm_reception_ticket";
      this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
      this.pnl_data.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.pb_currency)).EndInit();
      this.gb_payment.ResumeLayout(false);
      this.gb_payment.PerformLayout();
      this.gb_total.ResumeLayout(false);
      this.gb_entry_details.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.dgv_prices)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private Controls.uc_round_panel gb_payment;
    private Controls.uc_label lbl_currency_iso_code;
    private System.Windows.Forms.PictureBox pb_currency;
    private Controls.uc_round_button btn_selected_currency;
    private Controls.uc_label lbl_paymet_method;
    private Controls.uc_round_panel gb_total;
    private Controls.uc_label lbl_total_to_pay;
    public System.Windows.Forms.WebBrowser web_browser;
    private Controls.uc_round_button btn_cancel;
    private Controls.uc_round_button btn_ok;
    private Controls.uc_round_panel gb_entry_details;
    private Controls.uc_label lb_coupon_number;
    private Controls.uc_label lb_entry_price;
    private Controls.uc_checkBox cb_data_agreement;
    private Controls.uc_round_textbox txt_coupon_number;
    private Controls.uc_label lbl_currency;
    private Controls.uc_DataGridView dgv_prices;
    private Controls.uc_round_button btn_apply_coupon;
    private Controls.uc_round_button btn_keyboard;
  }
}
