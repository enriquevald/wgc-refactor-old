namespace WSI.Cashier
{
  partial class frm_chips
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
      this.gb_common = new WSI.Cashier.Controls.uc_round_panel();
      this.lbl_total_delivered_exchange = new WSI.Cashier.Controls.uc_label();
      this.btn_show_chips = new WSI.Cashier.Controls.uc_round_button();
      this.lbl_total_to_pay = new WSI.Cashier.Controls.uc_label();
      this.btn_clear = new WSI.Cashier.Controls.uc_round_button();
      this.dgv_common = new WSI.Cashier.Controls.uc_DataGridView();
      this.lbl_diference_amount = new WSI.Cashier.Controls.uc_label();
      this.lbl_total_delivered = new WSI.Cashier.Controls.uc_label();
      this.uc_payment_method1 = new WSI.Cashier.uc_payment_method();
      this.btn_apply_tax = new WSI.Cashier.Controls.uc_round_button();
      this.uc_input_amount = new WSI.Cashier.uc_input_amount();
      this.gb_info = new WSI.Cashier.Controls.uc_round_panel();
      this.lbl_label_1 = new WSI.Cashier.Controls.uc_label();
      this.lbl_gaming_table = new WSI.Cashier.Controls.uc_label();
      this.lbl_label_2 = new WSI.Cashier.Controls.uc_label();
      this.gb_distribution = new WSI.Cashier.Controls.uc_round_panel();
      this.btn_small_distribution = new WSI.Cashier.Controls.uc_round_button();
      this.btn_high_distribution = new WSI.Cashier.Controls.uc_round_button();
      this.btn_medium_distribution = new WSI.Cashier.Controls.uc_round_button();
      this.btn_ticket = new WSI.Cashier.Controls.uc_round_button();
      this.btn_check_redeem = new WSI.Cashier.Controls.uc_round_button();
      this.lbl_initial_amount = new WSI.Cashier.Controls.uc_label();
      this.pnl_buttons = new System.Windows.Forms.Panel();
      this.btn_ok = new WSI.Cashier.Controls.uc_round_button();
      this.btn_cancel = new WSI.Cashier.Controls.uc_round_button();
      this.gb_total = new WSI.Cashier.Controls.uc_round_panel();
      this.lbl_remaining = new WSI.Cashier.Controls.uc_label();
      this.pnl_data.SuspendLayout();
      this.gb_common.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dgv_common)).BeginInit();
      this.gb_info.SuspendLayout();
      this.gb_distribution.SuspendLayout();
      this.pnl_buttons.SuspendLayout();
      this.gb_total.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.Controls.Add(this.uc_payment_method1);
      this.pnl_data.Controls.Add(this.btn_check_redeem);
      this.pnl_data.Controls.Add(this.btn_ticket);
      this.pnl_data.Controls.Add(this.btn_apply_tax);
      this.pnl_data.Controls.Add(this.gb_total);
      this.pnl_data.Controls.Add(this.gb_common);
      this.pnl_data.Controls.Add(this.gb_info);
      this.pnl_data.Controls.Add(this.uc_input_amount);
      this.pnl_data.Controls.Add(this.pnl_buttons);
      this.pnl_data.Controls.Add(this.gb_distribution);
      this.pnl_data.Size = new System.Drawing.Size(1024, 658);
      this.pnl_data.TabIndex = 0;
      // 
      // gb_common
      // 
      this.gb_common.BackColor = System.Drawing.Color.Transparent;
      this.gb_common.BorderColor = System.Drawing.Color.Empty;
      this.gb_common.Controls.Add(this.lbl_total_delivered_exchange);
      this.gb_common.Controls.Add(this.btn_show_chips);
      this.gb_common.Controls.Add(this.lbl_total_to_pay);
      this.gb_common.Controls.Add(this.btn_clear);
      this.gb_common.Controls.Add(this.dgv_common);
      this.gb_common.Controls.Add(this.lbl_diference_amount);
      this.gb_common.Controls.Add(this.lbl_total_delivered);
      this.gb_common.CornerRadius = 10;
      this.gb_common.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_common.ForeColor = System.Drawing.Color.Black;
      this.gb_common.HeaderBackColor = System.Drawing.Color.Empty;
      this.gb_common.HeaderForeColor = System.Drawing.Color.Empty;
      this.gb_common.HeaderHeight = 0;
      this.gb_common.HeaderSubText = null;
      this.gb_common.HeaderText = null;
      this.gb_common.Location = new System.Drawing.Point(8, 6);
      this.gb_common.Name = "gb_common";
      this.gb_common.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_common.Size = new System.Drawing.Size(622, 471);
      this.gb_common.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.WITHOUT_HEAD;
      this.gb_common.TabIndex = 22;
      // 
      // lbl_total_delivered_exchange
      // 
      this.lbl_total_delivered_exchange.BackColor = System.Drawing.Color.Transparent;
      this.lbl_total_delivered_exchange.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_total_delivered_exchange.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_total_delivered_exchange.Location = new System.Drawing.Point(283, 384);
      this.lbl_total_delivered_exchange.Name = "lbl_total_delivered_exchange";
      this.lbl_total_delivered_exchange.Size = new System.Drawing.Size(330, 25);
      this.lbl_total_delivered_exchange.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_total_delivered_exchange.TabIndex = 6;
      this.lbl_total_delivered_exchange.Text = "Delivered exchange:  $XXX.XX";
      this.lbl_total_delivered_exchange.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // btn_show_chips
      // 
      this.btn_show_chips.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.btn_show_chips.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(47)))), ((int)(((byte)(50)))));
      this.btn_show_chips.FlatAppearance.BorderSize = 0;
      this.btn_show_chips.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_show_chips.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_show_chips.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_show_chips.Image = null;
      this.btn_show_chips.IsSelected = false;
      this.btn_show_chips.Location = new System.Drawing.Point(147, 399);
      this.btn_show_chips.Name = "btn_show_chips";
      this.btn_show_chips.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_show_chips.Size = new System.Drawing.Size(130, 50);
      this.btn_show_chips.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.TERTIARY;
      this.btn_show_chips.TabIndex = 5;
      this.btn_show_chips.Text = "XSHOW";
      this.btn_show_chips.UseVisualStyleBackColor = false;
      this.btn_show_chips.Click += new System.EventHandler(this.btn_show_chips_Click);
      // 
      // lbl_total_to_pay
      // 
      this.lbl_total_to_pay.BackColor = System.Drawing.Color.Transparent;
      this.lbl_total_to_pay.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_total_to_pay.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_total_to_pay.Location = new System.Drawing.Point(283, 443);
      this.lbl_total_to_pay.Name = "lbl_total_to_pay";
      this.lbl_total_to_pay.Size = new System.Drawing.Size(330, 25);
      this.lbl_total_to_pay.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_total_to_pay.TabIndex = 4;
      this.lbl_total_to_pay.Text = "Total to pay:  $XXX.XX";
      this.lbl_total_to_pay.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      this.lbl_total_to_pay.Visible = false;
      // 
      // btn_clear
      // 
      this.btn_clear.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.btn_clear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(47)))), ((int)(((byte)(50)))));
      this.btn_clear.FlatAppearance.BorderSize = 0;
      this.btn_clear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_clear.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_clear.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_clear.Image = null;
      this.btn_clear.IsSelected = false;
      this.btn_clear.Location = new System.Drawing.Point(11, 399);
      this.btn_clear.Name = "btn_clear";
      this.btn_clear.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_clear.Size = new System.Drawing.Size(130, 50);
      this.btn_clear.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.TERTIARY;
      this.btn_clear.TabIndex = 1;
      this.btn_clear.Text = "CLEAR";
      this.btn_clear.UseVisualStyleBackColor = false;
      this.btn_clear.Click += new System.EventHandler(this.btn_clear_Click);
      // 
      // dgv_common
      // 
      this.dgv_common.AllowUserToAddRows = false;
      this.dgv_common.AllowUserToDeleteRows = false;
      this.dgv_common.AllowUserToResizeColumns = false;
      this.dgv_common.AllowUserToResizeRows = false;
      this.dgv_common.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.dgv_common.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.dgv_common.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
      this.dgv_common.ColumnHeaderHeight = 35;
      this.dgv_common.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
      dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      dataGridViewCellStyle1.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
      dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
      dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
      this.dgv_common.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
      this.dgv_common.ColumnHeadersHeight = 35;
      this.dgv_common.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
      this.dgv_common.CornerRadius = 10;
      this.dgv_common.Cursor = System.Windows.Forms.Cursors.Arrow;
      this.dgv_common.DefaultCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.dgv_common.DefaultCellForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.dgv_common.DefaultCellSelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.dgv_common.DefaultCellSelectionForeColor = System.Drawing.Color.White;
      dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      dataGridViewCellStyle2.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.White;
      dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
      this.dgv_common.DefaultCellStyle = dataGridViewCellStyle2;
      this.dgv_common.Dock = System.Windows.Forms.DockStyle.Top;
      this.dgv_common.EnableHeadersVisualStyles = false;
      this.dgv_common.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.dgv_common.GridColor = System.Drawing.Color.White;
      this.dgv_common.HeaderBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
      this.dgv_common.HeaderDefaultCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.dgv_common.HeaderDefaultCellForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.dgv_common.HeaderImages = null;
      this.dgv_common.Location = new System.Drawing.Point(0, 0);
      this.dgv_common.Name = "dgv_common";
      this.dgv_common.ReadOnly = true;
      this.dgv_common.RowHeadersVisible = false;
      this.dgv_common.RowHeadersWidth = 35;
      this.dgv_common.RowTemplate.Height = 35;
      this.dgv_common.RowTemplateHeight = 35;
      this.dgv_common.Size = new System.Drawing.Size(622, 385);
      this.dgv_common.Style = WSI.Cashier.Controls.uc_DataGridView.DataGridViewStyle.WITH_ALL_ROUND_CORNERS;
      this.dgv_common.TabIndex = 0;
      this.dgv_common.SelectionChanged += new System.EventHandler(this.dgv_common_SelectionChanged);
      // 
      // lbl_diference_amount
      // 
      this.lbl_diference_amount.BackColor = System.Drawing.Color.Transparent;
      this.lbl_diference_amount.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_diference_amount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_diference_amount.Location = new System.Drawing.Point(283, 423);
      this.lbl_diference_amount.Name = "lbl_diference_amount";
      this.lbl_diference_amount.Size = new System.Drawing.Size(330, 25);
      this.lbl_diference_amount.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_diference_amount.TabIndex = 3;
      this.lbl_diference_amount.Text = "Diference:  $XXX.XX";
      this.lbl_diference_amount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_total_delivered
      // 
      this.lbl_total_delivered.BackColor = System.Drawing.Color.Transparent;
      this.lbl_total_delivered.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_total_delivered.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_total_delivered.Location = new System.Drawing.Point(283, 403);
      this.lbl_total_delivered.Name = "lbl_total_delivered";
      this.lbl_total_delivered.Size = new System.Drawing.Size(330, 25);
      this.lbl_total_delivered.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_total_delivered.TabIndex = 2;
      this.lbl_total_delivered.Text = "Delivered:  $XXX.XX";
      this.lbl_total_delivered.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // uc_payment_method1
      // 
      this.uc_payment_method1.BtnSelectedCurrencyEnabled = true;
      this.uc_payment_method1.Location = new System.Drawing.Point(636, 6);
      this.uc_payment_method1.Margin = new System.Windows.Forms.Padding(4);
      this.uc_payment_method1.Name = "uc_payment_method1";
      this.uc_payment_method1.showPaymentCreditLine = false;
      this.uc_payment_method1.Size = new System.Drawing.Size(382, 115);
      this.uc_payment_method1.TabIndex = 23;
      this.uc_payment_method1.CurrencyChanged += new WSI.Cashier.uc_payment_method.CurrencyChangedHandler(this.uc_payment_method1_CurrencyChanged);
      // 
      // btn_apply_tax
      // 
      this.btn_apply_tax.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.btn_apply_tax.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_apply_tax.FlatAppearance.BorderSize = 0;
      this.btn_apply_tax.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_apply_tax.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_apply_tax.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_apply_tax.Image = null;
      this.btn_apply_tax.IsSelected = false;
      this.btn_apply_tax.Location = new System.Drawing.Point(20, 590);
      this.btn_apply_tax.Name = "btn_apply_tax";
      this.btn_apply_tax.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_apply_tax.Size = new System.Drawing.Size(130, 60);
      this.btn_apply_tax.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_apply_tax.TabIndex = 1;
      this.btn_apply_tax.Text = "XAPPLY TAX";
      this.btn_apply_tax.UseVisualStyleBackColor = false;
      this.btn_apply_tax.Click += new System.EventHandler(this.btn_apply_tax_Click);
      // 
      // uc_input_amount
      // 
      this.uc_input_amount.BackColor = System.Drawing.Color.Transparent;
      this.uc_input_amount.Location = new System.Drawing.Point(636, 125);
      this.uc_input_amount.Name = "uc_input_amount";
      this.uc_input_amount.Size = new System.Drawing.Size(382, 368);
      this.uc_input_amount.TabIndex = 4;
      this.uc_input_amount.WithDecimals = false;
      // 
      // gb_info
      // 
      this.gb_info.BackColor = System.Drawing.Color.Transparent;
      this.gb_info.BorderColor = System.Drawing.Color.Empty;
      this.gb_info.Controls.Add(this.lbl_remaining);
      this.gb_info.Controls.Add(this.lbl_label_1);
      this.gb_info.Controls.Add(this.lbl_gaming_table);
      this.gb_info.Controls.Add(this.lbl_label_2);
      this.gb_info.CornerRadius = 10;
      this.gb_info.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_info.ForeColor = System.Drawing.Color.Black;
      this.gb_info.HeaderBackColor = System.Drawing.Color.Empty;
      this.gb_info.HeaderForeColor = System.Drawing.Color.Empty;
      this.gb_info.HeaderHeight = 0;
      this.gb_info.HeaderSubText = null;
      this.gb_info.HeaderText = null;
      this.gb_info.Location = new System.Drawing.Point(8, 483);
      this.gb_info.Name = "gb_info";
      this.gb_info.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_info.Size = new System.Drawing.Size(622, 103);
      this.gb_info.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.WITHOUT_HEAD;
      this.gb_info.TabIndex = 6;
      // 
      // lbl_label_1
      // 
      this.lbl_label_1.BackColor = System.Drawing.Color.Transparent;
      this.lbl_label_1.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_label_1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_label_1.Location = new System.Drawing.Point(6, 25);
      this.lbl_label_1.Name = "lbl_label_1";
      this.lbl_label_1.Size = new System.Drawing.Size(563, 25);
      this.lbl_label_1.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_label_1.TabIndex = 2;
      this.lbl_label_1.Text = "xMonto m�ximo permitido: $XXX.XX";
      this.lbl_label_1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_gaming_table
      // 
      this.lbl_gaming_table.BackColor = System.Drawing.Color.Transparent;
      this.lbl_gaming_table.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_gaming_table.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_gaming_table.Location = new System.Drawing.Point(6, 3);
      this.lbl_gaming_table.Name = "lbl_gaming_table";
      this.lbl_gaming_table.Size = new System.Drawing.Size(563, 22);
      this.lbl_gaming_table.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_gaming_table.TabIndex = 1;
      this.lbl_gaming_table.Text = "xMesa de juego seleccionada: <nombre mesa>";
      this.lbl_gaming_table.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_label_2
      // 
      this.lbl_label_2.BackColor = System.Drawing.Color.Transparent;
      this.lbl_label_2.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_label_2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_label_2.Location = new System.Drawing.Point(6, 50);
      this.lbl_label_2.Name = "lbl_label_2";
      this.lbl_label_2.Size = new System.Drawing.Size(563, 18);
      this.lbl_label_2.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_label_2.TabIndex = 0;
      this.lbl_label_2.Text = "xCr�ditos disponibles en la cuenta:  $XXX.XX";
      this.lbl_label_2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // gb_distribution
      // 
      this.gb_distribution.BackColor = System.Drawing.Color.Transparent;
      this.gb_distribution.BorderColor = System.Drawing.Color.Empty;
      this.gb_distribution.Controls.Add(this.btn_small_distribution);
      this.gb_distribution.Controls.Add(this.btn_high_distribution);
      this.gb_distribution.Controls.Add(this.btn_medium_distribution);
      this.gb_distribution.CornerRadius = 10;
      this.gb_distribution.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_distribution.ForeColor = System.Drawing.Color.Black;
      this.gb_distribution.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gb_distribution.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_distribution.HeaderHeight = 35;
      this.gb_distribution.HeaderSubText = null;
      this.gb_distribution.HeaderText = "XCHIPSDISTRIBUTION";
      this.gb_distribution.Location = new System.Drawing.Point(8, 483);
      this.gb_distribution.Name = "gb_distribution";
      this.gb_distribution.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_distribution.Size = new System.Drawing.Size(433, 98);
      this.gb_distribution.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.gb_distribution.TabIndex = 0;
      this.gb_distribution.Text = "xDistribution";
      // 
      // btn_small_distribution
      // 
      this.btn_small_distribution.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.btn_small_distribution.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_small_distribution.FlatAppearance.BorderSize = 0;
      this.btn_small_distribution.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_small_distribution.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_small_distribution.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_small_distribution.Image = null;
      this.btn_small_distribution.IsSelected = false;
      this.btn_small_distribution.Location = new System.Drawing.Point(11, 42);
      this.btn_small_distribution.Name = "btn_small_distribution";
      this.btn_small_distribution.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_small_distribution.Size = new System.Drawing.Size(130, 50);
      this.btn_small_distribution.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_small_distribution.TabIndex = 0;
      this.btn_small_distribution.Text = "SMALL";
      this.btn_small_distribution.UseVisualStyleBackColor = false;
      this.btn_small_distribution.Click += new System.EventHandler(this.btn_small_distribution_Click);
      // 
      // btn_high_distribution
      // 
      this.btn_high_distribution.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.btn_high_distribution.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_high_distribution.FlatAppearance.BorderSize = 0;
      this.btn_high_distribution.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_high_distribution.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_high_distribution.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_high_distribution.Image = null;
      this.btn_high_distribution.IsSelected = false;
      this.btn_high_distribution.Location = new System.Drawing.Point(291, 42);
      this.btn_high_distribution.Name = "btn_high_distribution";
      this.btn_high_distribution.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_high_distribution.Size = new System.Drawing.Size(130, 50);
      this.btn_high_distribution.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_high_distribution.TabIndex = 2;
      this.btn_high_distribution.Text = "HIGH";
      this.btn_high_distribution.UseVisualStyleBackColor = false;
      this.btn_high_distribution.Click += new System.EventHandler(this.btn_high_distribution_Click);
      // 
      // btn_medium_distribution
      // 
      this.btn_medium_distribution.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.btn_medium_distribution.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_medium_distribution.FlatAppearance.BorderSize = 0;
      this.btn_medium_distribution.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_medium_distribution.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_medium_distribution.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_medium_distribution.Image = null;
      this.btn_medium_distribution.IsSelected = false;
      this.btn_medium_distribution.Location = new System.Drawing.Point(151, 42);
      this.btn_medium_distribution.Name = "btn_medium_distribution";
      this.btn_medium_distribution.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_medium_distribution.Size = new System.Drawing.Size(130, 50);
      this.btn_medium_distribution.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_medium_distribution.TabIndex = 1;
      this.btn_medium_distribution.Text = "MED";
      this.btn_medium_distribution.UseVisualStyleBackColor = false;
      this.btn_medium_distribution.Click += new System.EventHandler(this.btn_medium_distribution_Click);
      // 
      // btn_ticket
      // 
      this.btn_ticket.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.btn_ticket.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_ticket.FlatAppearance.BorderSize = 0;
      this.btn_ticket.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_ticket.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_ticket.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_ticket.Image = null;
      this.btn_ticket.IsSelected = false;
      this.btn_ticket.Location = new System.Drawing.Point(159, 591);
      this.btn_ticket.Name = "btn_ticket";
      this.btn_ticket.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_ticket.Size = new System.Drawing.Size(130, 60);
      this.btn_ticket.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_ticket.TabIndex = 2;
      this.btn_ticket.Text = "XTICKET";
      this.btn_ticket.UseVisualStyleBackColor = false;
      this.btn_ticket.Visible = false;
      this.btn_ticket.Click += new System.EventHandler(this.btn_ticket_Click);
      // 
      // btn_check_redeem
      // 
      this.btn_check_redeem.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.btn_check_redeem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_check_redeem.FlatAppearance.BorderSize = 0;
      this.btn_check_redeem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_check_redeem.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_check_redeem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_check_redeem.Image = null;
      this.btn_check_redeem.IsSelected = false;
      this.btn_check_redeem.Location = new System.Drawing.Point(299, 591);
      this.btn_check_redeem.Name = "btn_check_redeem";
      this.btn_check_redeem.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_check_redeem.Size = new System.Drawing.Size(130, 60);
      this.btn_check_redeem.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_check_redeem.TabIndex = 3;
      this.btn_check_redeem.Text = "XCHECK REDEEM";
      this.btn_check_redeem.UseVisualStyleBackColor = false;
      this.btn_check_redeem.Visible = false;
      this.btn_check_redeem.Click += new System.EventHandler(this.btn_check_redeem_Click);
      // 
      // lbl_initial_amount
      // 
      this.lbl_initial_amount.BackColor = System.Drawing.Color.Transparent;
      this.lbl_initial_amount.Font = new System.Drawing.Font("Open Sans Semibold", 32F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_initial_amount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.lbl_initial_amount.Location = new System.Drawing.Point(3, 39);
      this.lbl_initial_amount.Name = "lbl_initial_amount";
      this.lbl_initial_amount.Size = new System.Drawing.Size(382, 46);
      this.lbl_initial_amount.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_32PX_WHITE;
      this.lbl_initial_amount.TabIndex = 30;
      this.lbl_initial_amount.Text = " Total: $XXX.XX";
      this.lbl_initial_amount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // pnl_buttons
      // 
      this.pnl_buttons.Controls.Add(this.btn_ok);
      this.pnl_buttons.Controls.Add(this.btn_cancel);
      this.pnl_buttons.Location = new System.Drawing.Point(636, 587);
      this.pnl_buttons.Name = "pnl_buttons";
      this.pnl_buttons.Size = new System.Drawing.Size(383, 67);
      this.pnl_buttons.TabIndex = 7;
      // 
      // btn_ok
      // 
      this.btn_ok.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.btn_ok.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_ok.FlatAppearance.BorderSize = 0;
      this.btn_ok.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_ok.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_ok.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_ok.Image = null;
      this.btn_ok.IsSelected = false;
      this.btn_ok.Location = new System.Drawing.Point(227, 4);
      this.btn_ok.Name = "btn_ok";
      this.btn_ok.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_ok.Size = new System.Drawing.Size(155, 60);
      this.btn_ok.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_ok.TabIndex = 1;
      this.btn_ok.Text = "OK";
      this.btn_ok.UseVisualStyleBackColor = false;
      this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
      // 
      // btn_cancel
      // 
      this.btn_cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.btn_cancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btn_cancel.FlatAppearance.BorderSize = 0;
      this.btn_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_cancel.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_cancel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_cancel.Image = null;
      this.btn_cancel.IsSelected = false;
      this.btn_cancel.Location = new System.Drawing.Point(2, 4);
      this.btn_cancel.Name = "btn_cancel";
      this.btn_cancel.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_cancel.Size = new System.Drawing.Size(155, 60);
      this.btn_cancel.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_cancel.TabIndex = 0;
      this.btn_cancel.Text = "CANCEL";
      this.btn_cancel.UseVisualStyleBackColor = false;
      this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
      // 
      // gb_total
      // 
      this.gb_total.BackColor = System.Drawing.Color.Transparent;
      this.gb_total.BorderColor = System.Drawing.Color.Empty;
      this.gb_total.Controls.Add(this.lbl_initial_amount);
      this.gb_total.CornerRadius = 10;
      this.gb_total.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_total.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.gb_total.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_total.HeaderHeight = 35;
      this.gb_total.HeaderSubText = null;
      this.gb_total.HeaderText = "XTOTAL";
      this.gb_total.Location = new System.Drawing.Point(636, 496);
      this.gb_total.Name = "gb_total";
      this.gb_total.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.gb_total.Size = new System.Drawing.Size(383, 90);
      this.gb_total.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.TOTAL;
      this.gb_total.TabIndex = 5;
      // 
      // lbl_remaining
      // 
      this.lbl_remaining.BackColor = System.Drawing.Color.Transparent;
      this.lbl_remaining.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_remaining.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_remaining.Location = new System.Drawing.Point(8, 74);
      this.lbl_remaining.Name = "lbl_remaining";
      this.lbl_remaining.Size = new System.Drawing.Size(563, 18);
      this.lbl_remaining.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLACK;
      this.lbl_remaining.TabIndex = 3;
      this.lbl_remaining.Text = "xMonto entergado de sobarntes de compras de fichas anteriores:  $XXX.XX";
      this.lbl_remaining.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // frm_chips
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.btn_cancel;
      this.ClientSize = new System.Drawing.Size(1024, 713);
      this.KeyPreview = true;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "frm_chips";
      this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
      this.Style = WSI.Cashier.Controls.frm_base.BaseFormStyle.PRINCIPAL;
      this.Text = "frm_add_credit";
      this.Shown += new System.EventHandler(this.frm_chips_Shown);
      this.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_chips_PreviewKeyDown);
      this.pnl_data.ResumeLayout(false);
      this.gb_common.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.dgv_common)).EndInit();
      this.gb_info.ResumeLayout(false);
      this.gb_distribution.ResumeLayout(false);
      this.pnl_buttons.ResumeLayout(false);
      this.gb_total.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private WSI.Cashier.Controls.uc_round_button btn_cancel;
    private WSI.Cashier.Controls.uc_round_button btn_ok;
    private WSI.Cashier.Controls.uc_DataGridView dgv_common;
    private System.Windows.Forms.Panel pnl_buttons;
    private WSI.Cashier.Controls.uc_label lbl_total_delivered;
    private WSI.Cashier.Controls.uc_round_button btn_clear;
    private WSI.Cashier.Controls.uc_label lbl_diference_amount;
    private WSI.Cashier.Controls.uc_round_button btn_medium_distribution;
    private WSI.Cashier.Controls.uc_round_button btn_small_distribution;
    private WSI.Cashier.Controls.uc_round_button btn_high_distribution;
    private WSI.Cashier.Controls.uc_label lbl_initial_amount;
    private WSI.Cashier.Controls.uc_round_button btn_check_redeem;
    private WSI.Cashier.Controls.uc_round_panel gb_distribution;
    private WSI.Cashier.Controls.uc_round_panel gb_info;
    private WSI.Cashier.Controls.uc_label lbl_label_1;
    private WSI.Cashier.Controls.uc_label lbl_gaming_table;
    private WSI.Cashier.Controls.uc_label lbl_label_2;
    private uc_input_amount uc_input_amount;
    private WSI.Cashier.Controls.uc_round_button btn_ticket;
    private WSI.Cashier.Controls.uc_label lbl_total_to_pay;
    private WSI.Cashier.Controls.uc_round_panel gb_common;
    private Controls.uc_round_panel gb_total;
    private Controls.uc_round_button btn_apply_tax;
    private uc_payment_method uc_payment_method1;
    private Controls.uc_round_button btn_show_chips;
    private Controls.uc_label lbl_total_delivered_exchange;
    private Controls.uc_label lbl_remaining;
  }
}