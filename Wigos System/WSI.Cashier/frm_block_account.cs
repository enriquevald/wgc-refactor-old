//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_block_account.cs
// 
//   DESCRIPTION: 
//
//        AUTHOR: Javi Barea
// 
// CREATION DATE: 06-MAY-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 06-MAY-2013 JBP    First release.
// 02-JUL-2013 RRR    Modified view parameters for Window Mode
// 13-AUG-2013 DRV    Modified automatic OSK for the Window Mode 
// 28-OCT-2014 SMN    Added new functionality: BLOCK_REASON field can have more than one block reason.
// 09-FEB-2015 FOS    Fixed Bug WIG-1997: Component gb_block_unblock don't have the correct dimensions.
// 21-MAY-2015 RCI    Fixed Bug WIG-2375: Don't allow to unlock accounts if they are locked by External PLD.
// 30-OCT-2015 JMV    Product Backlog Item 5846:Dise�o cajero: Aplicar en frm_block_account.cs
// 29-MAR-2016 EOR    Fixed Bug 10432: New Cashier: Error Lock/Unlock Accounts
// 07-JUN-2016 JBP    PBI 13253:C2GO - Implementaci�n m�todos WS Wigos
// 14-JUL-2016 PDM    PBI 15448:Visitas / Recepci�n: MEJORAS - Cajero & GUI - Nuevo motivo de bloqueo
// 02-DIC-2016 ESE    Bug 10699: Lost focus when close keyboard
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using WSI.Common;
using WSI.Cashier.Controls;

namespace WSI.Cashier
{
  public partial class frm_block_account : frm_base
  {
    #region Properties

    public CardData pCardData { get; set; } //EOR 29-MAR-2016 Add Public Property Type CardData

    #endregion

    #region Constants
    private const int BLOCK_ACCOUNT_Y_POSITION_SIZE_BALANCE = 158;
    #endregion

    #region Attributes

    private frm_yesno form_yes_no;
    private static CardData m_card;

    #endregion

    #region Constructor

    /// <summary>
    /// Constructor method.
    /// 1. Initialize controls.
    /// 2. Initialize control�s resources. (NLS, Images, etc)
    /// </summary>
    public frm_block_account(CardData Card)
    {
      InitializeComponent();

      m_card = Card;

      InitializeControlResources();

      Init();
    }

    #endregion

    #region Private Methods


    /// <summary>
    /// Initialize screen logic
    /// </summary>
    private void Init()
    {
      // Center screen
      this.Location = new Point(1024 / 2 - this.Width / 2, 0);

      // Show keyboard if Automatic OSK it's enabled
      //ESE 24-OCT2016 The keyboard shows on top screen TRUE
      WSIKeyboard.Toggle();

      // Set focus
      this.txt_reason.Select();

      form_yes_no = new frm_yesno();
      form_yes_no.Opacity = 0.6;
    }


    //------------------------------------------------------------------------------
    // PURPOSE : Block / Unblock account
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    private Boolean BlockUnblockAccount(AccountBlockUnblockSource Source)
    {
      CardData _card;
      string _error_str;
      ProfilePermissions.CashierFormFuncionality _permission;
      AccountBlockUnblock _account_block_unblock;


      try
      {
        if (!CardData.DB_CardGetPersonalData(m_card.AccountId, m_card))
        {
          return false;
        }

        if (!this.ExistExternalBlock())
        {
          return false;
        }


        if (Misc.IsReceptionEnabled() && uc_blacklist_checkbox.Checked)
        {
          _permission = m_card.Blocked ? ProfilePermissions.CashierFormFuncionality.Reception_AllowRemoveAccountFromLocalBlackList : ProfilePermissions.CashierFormFuncionality.Reception_AllowAddAccountToLocalBlackList;

          //Only continue if the user has the permission
          if (!ProfilePermissions.CheckPermissionList(
              _permission,
              ProfilePermissions.TypeOperation.RequestPasswd, this, 0, out _error_str))
          {
            frm_message.Show(_error_str, Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, Images.CashierImage.Error, this);

            return false;
          }

          _account_block_unblock = new AccountBlockUnblock(m_card, Source, txt_reason.Text, true);

          using (DB_TRX _db_trx = new DB_TRX())
          {
            if (!_account_block_unblock.ProceedToBlockUnblockAccount(Cashier.CashierSessionInfo(), !m_card.Blocked, _db_trx.SqlTransaction, out _card))
            {
              return false;
            }
            if (Misc.IsReceptionEnabled())
            {
              if (_card.Blocked)
              {
                WSI.Common.Entrances.Customers.AddToBlacklist(_card, Cashier.CashierSessionInfo(), Misc.TodayOpening(),
                  Common.Entrances.Enums.ENUM_BLACKLIST_REASON.BLOCK, txt_reason.Text, _db_trx.SqlTransaction);
                _account_block_unblock.AddCashierMovementsBlacklist(Cashier.CashierSessionInfo(),
                  CASHIER_MOVEMENT.ACCOUNT_ADD_BLACKLIST, _db_trx.SqlTransaction);
              }
              else
              {
                WSI.Common.Entrances.Customers.RemoveFromBlacklist(_card, Cashier.CashierSessionInfo(),
                  _db_trx.SqlTransaction);
                _account_block_unblock.AddCashierMovementsBlacklist(Cashier.CashierSessionInfo(),
                  CASHIER_MOVEMENT.ACCOUNT_REMOVE_BLACKLIST, _db_trx.SqlTransaction);
              }
            }
            _db_trx.Commit();
          }

          WSI.Common.Entrances.Customers.RefreshBlackListCache(WSI.Common.Entrances.Enums.ENUM_BLACKLIST_SOURCE.LOCAL);

        }
        else
        {
          _account_block_unblock = new AccountBlockUnblock(m_card, Source, txt_reason.Text, false);

          using (DB_TRX _db_trx = new DB_TRX())
          {

            if (!_account_block_unblock.ProceedToBlockUnblockAccount(Cashier.CashierSessionInfo(), !m_card.Blocked, _db_trx.SqlTransaction, out _card))
            {
              return false;
            }

            _db_trx.Commit();

          }
        }

        pCardData = _card;

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }

      return true;
    } // BlockUnblockAccount

    //------------------------------------------------------------------------------
    // PURPOSE : Check if exist external block (NLS strings, images, etc).
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    private Boolean ExistExternalBlock()
    {
      String _msg;
      AccountBlockReason _block_reason_found;
      AccountBlockReason[] _external_block_reasons;
      List<AccountBlockReason> _list;

      _msg = "";
      _external_block_reasons = new AccountBlockReason[]  { AccountBlockReason.EXTERNAL_SYSTEM_CANCELED
                                                          , AccountBlockReason.EXTERNAL_AML };

      _list = new List<AccountBlockReason>(_external_block_reasons);

      if (Accounts.ExistsBlockReasonInMask(_list, Accounts.ConvertOldBlockReason((Int32)m_card.BlockReason), out _block_reason_found))
      {
        switch (_block_reason_found)
        {
          case AccountBlockReason.EXTERNAL_SYSTEM_CANCELED:
            _msg = Resource.String("STR_CANCELLED_SPACE_ACCOUNTS");
            break;

          case AccountBlockReason.EXTERNAL_AML:
            _msg = Resource.String("STR_UNBLOCK_ERR_EXTERNAL_AML");
            break;
        }

        if (!String.IsNullOrEmpty(_msg))
        {
          frm_message.Show(_msg,
                           Resource.String("STR_APP_GEN_MSG_WARNING"),
                           MessageBoxButtons.OK,
                           MessageBoxIcon.Information,
                           ParentForm);
        }

        return false;
      }

      return true;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Initialize control resources (NLS strings, images, etc).
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    private void InitializeControlResources()
    {
      String _coma = String.Empty;

      if (!m_card.Blocked)
      {
        // - Hide unblock labels
        lbl_unblock_description.Visible = false;
        txt_block_description.Visible = false;

        // - Set Sizes and Positions
        this.Size = new Size(this.Size.Width, this.Size.Height - BLOCK_ACCOUNT_Y_POSITION_SIZE_BALANCE);
        this.gb_block_unblock.Size = new Size(this.gb_block_unblock.Size.Width, this.gb_block_unblock.Size.Height - BLOCK_ACCOUNT_Y_POSITION_SIZE_BALANCE);

        this.lbl_block_unblock_description.Location = new Point(this.lbl_block_unblock_description.Location.X, this.lbl_block_unblock_description.Location.Y - BLOCK_ACCOUNT_Y_POSITION_SIZE_BALANCE - 4);
        this.txt_reason.Location = new Point(this.txt_reason.Location.X, this.txt_reason.Location.Y - BLOCK_ACCOUNT_Y_POSITION_SIZE_BALANCE - 2);

        this.btn_keyboard.Location = new Point(this.btn_keyboard.Location.X, this.btn_keyboard.Location.Y - BLOCK_ACCOUNT_Y_POSITION_SIZE_BALANCE);
        this.btn_accept.Location = new Point(this.btn_accept.Location.X, this.btn_accept.Location.Y - BLOCK_ACCOUNT_Y_POSITION_SIZE_BALANCE);
        this.btn_cancel.Location = new Point(this.btn_cancel.Location.X, this.btn_cancel.Location.Y - BLOCK_ACCOUNT_Y_POSITION_SIZE_BALANCE);

        // - NLS Strings:

        this.FormTitle = Resource.String("STR_FRM_BLOCK_ACCOUNT_TITLE_BLOCK");
        lbl_block_unblock_description.Text = Resource.String("STR_FRM_BLOCK_ACCOUNT_LABEL_DESCRIPTION_BLOCK");
        gb_block_unblock.HeaderText = Resource.String("STR_UC_CARD_BLOCK_STATUS_UNLOCKED");

        btn_accept.Text = Resource.String("STR_FRM_BLOCK_ACCOUNT_BUTTON_BLOCK");

        uc_blacklist_checkbox.Text = Resource.String("STR_FRM_BLOCK_ACCOUNT_ADD_BLACKLIST");

        this.uc_blacklist_checkbox.Location = new Point(this.txt_reason.Location.X, this.txt_reason.Location.Y + this.txt_reason.Height + 10);

      }
      else
      {
        // - Set description text
        string _description;

        _description = Resource.String("STR_FRM_BLOCK_ACCOUNT_LABEL_DESCRIPTION_UNBLOCK_NO_DESCRIPTION");

        if (m_card.BlockDescription != null
         && m_card.BlockDescription != "")
        {
          _description = m_card.BlockDescription;
        }

        // - Show unblock labels
        lbl_unblock_description.Visible = true;
        txt_block_description.Visible = true;

        // - NLS Strings:
        this.FormTitle = Resource.String("STR_FRM_BLOCK_ACCOUNT_TITLE_UNBLOCK");
        lbl_unblock_description.Text = Resource.String("STR_FRM_BLOCK_ACCOUNT_LABEL_DESCRIPTION_UNBLOCK");
        txt_block_description.Text = _description;

        lbl_block_unblock_description.Text = Resource.String("STR_FRM_BLOCK_ACCOUNT_LABEL_UNBLOCK_DESCRIPTION");

        btn_accept.Text = Resource.String("STR_FRM_BLOCK_ACCOUNT_BUTTON_UNBLOCK");

        gb_block_unblock.HeaderText = Resource.String("STR_UC_CARD_BLOCK_STATUS_LOCKED") + " ";

        uc_blacklist_checkbox.Text = Resource.String("STR_FRM_BLOCK_ACCOUNT_REMOVE_BLACKLIST");

        foreach (KeyValuePair<AccountBlockReason, String> _block_reason_ in Accounts.GetBlockReason((Int32)m_card.BlockReason))
        {
          gb_block_unblock.HeaderText += _coma + _block_reason_.Value;
          _coma = ", ";
        }
      }

      uc_blacklist_checkbox.Visible = Misc.IsReceptionEnabled();
      
      btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");

    } // InitializeControlResources

    #endregion

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Init and Show the form.
    //
    //  PARAMS :
    //      - INPUT :
    //          - ParentForm
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public Boolean Show(Form ParentForm)
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_block_account", Log.Type.Message);
      form_yes_no.Show();

      if (WindowManager.StartUpMode == WindowManager.StartUpModes.Explorer)
      {
        this.Location = new Point(WindowManager.GetFormCenterLocation(form_yes_no).X/* - (this.Width / 2)*/, form_yes_no.Location.Y/* + 50*/);
      }
      this.ShowDialog(form_yes_no);

      form_yes_no.Hide();

      return (this.DialogResult != DialogResult.Abort);
    } // Show

    //------------------------------------------------------------------------------
    // PURPOSE : Dispose yes_no form.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    new public void Dispose()
    {
      form_yes_no.Dispose();
      form_yes_no = null;

      base.Dispose();
    }

    #endregion

    private void btn_accept_Click(object sender, EventArgs e)
    {
      BlockUnblockAccount(AccountBlockUnblockSource.CASHIER);

      // hide keyboard and close
      WSIKeyboard.Hide();

      Misc.WriteLog("[FORM CLOSE] frm_block_account (accept)", Log.Type.Message);
      this.Close();
    }

    private void btn_cancel_Click(object sender, EventArgs e)
    {
      // hide keyboard and close
      WSIKeyboard.Hide();

      Misc.WriteLog("[FORM CLOSE] frm_block_account (cancel)", Log.Type.Message);
      this.Close();
    }

    private void btn_keyboard_Click(object sender, EventArgs e)
    {
      // show keyboard (if hidden)
      //ESE 24-OCT2016 The keyboard shows on top screen TRUE
      WSIKeyboard.ForceToggle();

      //ESE 24-OCT-2016
      //Return the focus to the txt_reason
      txt_reason.Focus();
    }

    #region ButtonHandling



    #endregion

    #region EventHandling


    #endregion
  }
}
