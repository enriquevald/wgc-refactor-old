namespace WSI.Cashier
{
  partial class frm_vouchers_reprint
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
      this.btn_pause_continue = new WSI.Cashier.Controls.uc_round_button();
      this.lbl_print_status = new WSI.Cashier.Controls.uc_label();
      this.web_browser = new System.Windows.Forms.WebBrowser();
      this.opt_last_voucher = new WSI.Cashier.Controls.uc_radioButton();
      this.opt_mobile_bank = new WSI.Cashier.Controls.uc_radioButton();
      this.opt_cashier = new WSI.Cashier.Controls.uc_radioButton();
      this.btn_print_all = new WSI.Cashier.Controls.uc_round_button();
      this.btn_print_selected = new WSI.Cashier.Controls.uc_round_button();
      this.gb_cashier_sessions = new WSI.Cashier.Controls.uc_round_panel();
      this.dgv_cashier_sessions = new WSI.Cashier.Controls.uc_DataGridView();
      this.gb_last_operations = new WSI.Cashier.Controls.uc_round_panel();
      this.dgv_last_operations = new WSI.Cashier.Controls.uc_DataGridView();
      this.btn_close = new WSI.Cashier.Controls.uc_round_button();
      this.tmr_status_printing = new System.Windows.Forms.Timer(this.components);
      this.gb_source = new WSI.Cashier.Controls.uc_round_panel();
      this.pnl_data.SuspendLayout();
      this.gb_cashier_sessions.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dgv_cashier_sessions)).BeginInit();
      this.gb_last_operations.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dgv_last_operations)).BeginInit();
      this.gb_source.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.Controls.Add(this.gb_source);
      this.pnl_data.Controls.Add(this.web_browser);
      this.pnl_data.Controls.Add(this.gb_cashier_sessions);
      this.pnl_data.Controls.Add(this.btn_pause_continue);
      this.pnl_data.Controls.Add(this.lbl_print_status);
      this.pnl_data.Controls.Add(this.btn_print_selected);
      this.pnl_data.Controls.Add(this.gb_last_operations);
      this.pnl_data.Controls.Add(this.btn_close);
      this.pnl_data.Size = new System.Drawing.Size(1024, 658);
      this.pnl_data.TabIndex = 0;
      // 
      // btn_pause_continue
      // 
      this.btn_pause_continue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_pause_continue.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(47)))), ((int)(((byte)(50)))));
      this.btn_pause_continue.FlatAppearance.BorderSize = 0;
      this.btn_pause_continue.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_pause_continue.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_pause_continue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_pause_continue.Image = null;
      this.btn_pause_continue.IsSelected = false;
      this.btn_pause_continue.Location = new System.Drawing.Point(687, 582);
      this.btn_pause_continue.Name = "btn_pause_continue";
      this.btn_pause_continue.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_pause_continue.Size = new System.Drawing.Size(155, 60);
      this.btn_pause_continue.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.TERTIARY;
      this.btn_pause_continue.TabIndex = 6;
      this.btn_pause_continue.Text = "XPAUSE/CONTINUE";
      this.btn_pause_continue.UseVisualStyleBackColor = false;
      this.btn_pause_continue.Click += new System.EventHandler(this.btn_pause_continue_Click);
      // 
      // lbl_print_status
      // 
      this.lbl_print_status.BackColor = System.Drawing.Color.Transparent;
      this.lbl_print_status.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_print_status.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_print_status.Location = new System.Drawing.Point(17, 600);
      this.lbl_print_status.Name = "lbl_print_status";
      this.lbl_print_status.Size = new System.Drawing.Size(464, 25);
      this.lbl_print_status.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_print_status.TabIndex = 4;
      this.lbl_print_status.Text = "xPrintStatus";
      this.lbl_print_status.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // web_browser
      // 
      this.web_browser.AllowWebBrowserDrop = false;
      this.web_browser.CausesValidation = false;
      this.web_browser.Location = new System.Drawing.Point(688, 15);
      this.web_browser.MinimumSize = new System.Drawing.Size(20, 20);
      this.web_browser.Name = "web_browser";
      this.web_browser.ScriptErrorsSuppressed = true;
      this.web_browser.Size = new System.Drawing.Size(320, 550);
      this.web_browser.TabIndex = 2;
      this.web_browser.Url = new System.Uri("", System.UriKind.Relative);
      this.web_browser.WebBrowserShortcutsEnabled = false;
      // 
      // opt_last_voucher
      // 
      this.opt_last_voucher.AutoSize = true;
      this.opt_last_voucher.BackColor = System.Drawing.Color.Transparent;
      this.opt_last_voucher.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.opt_last_voucher.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.opt_last_voucher.Location = new System.Drawing.Point(11, 40);
      this.opt_last_voucher.Name = "opt_last_voucher";
      this.opt_last_voucher.Padding = new System.Windows.Forms.Padding(10);
      this.opt_last_voucher.Size = new System.Drawing.Size(141, 30);
      this.opt_last_voucher.TabIndex = 0;
      this.opt_last_voucher.TabStop = true;
      this.opt_last_voucher.Text = "xLastVoucher";
      this.opt_last_voucher.UseVisualStyleBackColor = true;
      this.opt_last_voucher.CheckedChanged += new System.EventHandler(this.opt_last_voucher_CheckedChanged);
      // 
      // opt_mobile_bank
      // 
      this.opt_mobile_bank.AutoSize = true;
      this.opt_mobile_bank.BackColor = System.Drawing.Color.Transparent;
      this.opt_mobile_bank.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.opt_mobile_bank.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.opt_mobile_bank.Location = new System.Drawing.Point(11, 110);
      this.opt_mobile_bank.Name = "opt_mobile_bank";
      this.opt_mobile_bank.Padding = new System.Windows.Forms.Padding(10);
      this.opt_mobile_bank.Size = new System.Drawing.Size(136, 30);
      this.opt_mobile_bank.TabIndex = 2;
      this.opt_mobile_bank.TabStop = true;
      this.opt_mobile_bank.Text = "xMobileBank";
      this.opt_mobile_bank.UseVisualStyleBackColor = true;
      this.opt_mobile_bank.CheckedChanged += new System.EventHandler(this.opt_mobile_bank_CheckedChanged);
      // 
      // opt_cashier
      // 
      this.opt_cashier.AutoSize = true;
      this.opt_cashier.BackColor = System.Drawing.Color.Transparent;
      this.opt_cashier.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.opt_cashier.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.opt_cashier.Location = new System.Drawing.Point(11, 75);
      this.opt_cashier.Name = "opt_cashier";
      this.opt_cashier.Padding = new System.Windows.Forms.Padding(10);
      this.opt_cashier.Size = new System.Drawing.Size(106, 30);
      this.opt_cashier.TabIndex = 1;
      this.opt_cashier.TabStop = true;
      this.opt_cashier.Text = "xCashier";
      this.opt_cashier.UseVisualStyleBackColor = true;
      this.opt_cashier.CheckedChanged += new System.EventHandler(this.opt_cashier_CheckedChanged);
      // 
      // btn_print_all
      // 
      this.btn_print_all.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_print_all.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_print_all.FlatAppearance.BorderSize = 0;
      this.btn_print_all.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_print_all.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_print_all.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_print_all.Image = null;
      this.btn_print_all.IsSelected = false;
      this.btn_print_all.Location = new System.Drawing.Point(306, 86);
      this.btn_print_all.Name = "btn_print_all";
      this.btn_print_all.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_print_all.Size = new System.Drawing.Size(155, 50);
      this.btn_print_all.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_print_all.TabIndex = 3;
      this.btn_print_all.Text = "XPRINTALL";
      this.btn_print_all.UseVisualStyleBackColor = false;
      this.btn_print_all.Click += new System.EventHandler(this.btn_print_all_Click);
      // 
      // btn_print_selected
      // 
      this.btn_print_selected.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_print_selected.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_print_selected.FlatAppearance.BorderSize = 0;
      this.btn_print_selected.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_print_selected.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_print_selected.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_print_selected.Image = null;
      this.btn_print_selected.IsSelected = false;
      this.btn_print_selected.Location = new System.Drawing.Point(853, 582);
      this.btn_print_selected.Name = "btn_print_selected";
      this.btn_print_selected.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_print_selected.Size = new System.Drawing.Size(155, 60);
      this.btn_print_selected.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_print_selected.TabIndex = 7;
      this.btn_print_selected.Text = "XPRINTSELECTED";
      this.btn_print_selected.UseVisualStyleBackColor = false;
      this.btn_print_selected.Click += new System.EventHandler(this.btn_print_selected_Click);
      // 
      // gb_cashier_sessions
      // 
      this.gb_cashier_sessions.BackColor = System.Drawing.Color.Transparent;
      this.gb_cashier_sessions.BorderColor = System.Drawing.Color.Empty;
      this.gb_cashier_sessions.Controls.Add(this.dgv_cashier_sessions);
      this.gb_cashier_sessions.CornerRadius = 10;
      this.gb_cashier_sessions.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_cashier_sessions.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gb_cashier_sessions.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_cashier_sessions.HeaderHeight = 35;
      this.gb_cashier_sessions.HeaderSubText = null;
      this.gb_cashier_sessions.HeaderText = "";
      this.gb_cashier_sessions.Location = new System.Drawing.Point(111, 15);
      this.gb_cashier_sessions.Name = "gb_cashier_sessions";
      this.gb_cashier_sessions.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_cashier_sessions.Size = new System.Drawing.Size(469, 212);
      this.gb_cashier_sessions.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.gb_cashier_sessions.TabIndex = 0;
      this.gb_cashier_sessions.Text = "xCashierSessions";
      // 
      // dgv_cashier_sessions
      // 
      this.dgv_cashier_sessions.AllowUserToAddRows = false;
      this.dgv_cashier_sessions.AllowUserToDeleteRows = false;
      this.dgv_cashier_sessions.AllowUserToResizeColumns = false;
      this.dgv_cashier_sessions.AllowUserToResizeRows = false;
      this.dgv_cashier_sessions.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.dgv_cashier_sessions.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
      this.dgv_cashier_sessions.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.dgv_cashier_sessions.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.dgv_cashier_sessions.ColumnHeaderHeight = 35;
      this.dgv_cashier_sessions.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
      dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      dataGridViewCellStyle1.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
      dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
      dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
      this.dgv_cashier_sessions.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
      this.dgv_cashier_sessions.ColumnHeadersHeight = 35;
      this.dgv_cashier_sessions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
      this.dgv_cashier_sessions.CornerRadius = 10;
      this.dgv_cashier_sessions.DefaultCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.dgv_cashier_sessions.DefaultCellForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.dgv_cashier_sessions.DefaultCellSelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.dgv_cashier_sessions.DefaultCellSelectionForeColor = System.Drawing.Color.White;
      dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      dataGridViewCellStyle2.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.White;
      dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
      this.dgv_cashier_sessions.DefaultCellStyle = dataGridViewCellStyle2;
      this.dgv_cashier_sessions.EnableHeadersVisualStyles = false;
      this.dgv_cashier_sessions.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.dgv_cashier_sessions.GridColor = System.Drawing.Color.LightGray;
      this.dgv_cashier_sessions.HeaderBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
      this.dgv_cashier_sessions.HeaderDefaultCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.dgv_cashier_sessions.HeaderDefaultCellForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.dgv_cashier_sessions.HeaderImages = null;
      this.dgv_cashier_sessions.Location = new System.Drawing.Point(-1, 35);
      this.dgv_cashier_sessions.Name = "dgv_cashier_sessions";
      this.dgv_cashier_sessions.ReadOnly = true;
      this.dgv_cashier_sessions.RowHeadersVisible = false;
      this.dgv_cashier_sessions.RowHeadersWidth = 20;
      this.dgv_cashier_sessions.RowTemplate.Height = 35;
      this.dgv_cashier_sessions.RowTemplateHeight = 35;
      this.dgv_cashier_sessions.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
      this.dgv_cashier_sessions.Size = new System.Drawing.Size(471, 177);
      this.dgv_cashier_sessions.Style = WSI.Cashier.Controls.uc_DataGridView.DataGridViewStyle.WITH_BOTTOM_ROUND_BORDERS;
      this.dgv_cashier_sessions.TabIndex = 0;
      this.dgv_cashier_sessions.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_cashier_sessions_RowEnter);
      // 
      // gb_last_operations
      // 
      this.gb_last_operations.BackColor = System.Drawing.Color.Transparent;
      this.gb_last_operations.BorderColor = System.Drawing.Color.Empty;
      this.gb_last_operations.Controls.Add(this.dgv_last_operations);
      this.gb_last_operations.CornerRadius = 10;
      this.gb_last_operations.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_last_operations.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gb_last_operations.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_last_operations.HeaderHeight = 35;
      this.gb_last_operations.HeaderSubText = null;
      this.gb_last_operations.HeaderText = null;
      this.gb_last_operations.Location = new System.Drawing.Point(14, 391);
      this.gb_last_operations.Name = "gb_last_operations";
      this.gb_last_operations.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_last_operations.Size = new System.Drawing.Size(662, 177);
      this.gb_last_operations.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.gb_last_operations.TabIndex = 3;
      this.gb_last_operations.Text = "xLastOperations";
      // 
      // dgv_last_operations
      // 
      this.dgv_last_operations.AllowUserToAddRows = false;
      this.dgv_last_operations.AllowUserToDeleteRows = false;
      this.dgv_last_operations.AllowUserToResizeColumns = false;
      this.dgv_last_operations.AllowUserToResizeRows = false;
      this.dgv_last_operations.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.dgv_last_operations.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
      this.dgv_last_operations.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.dgv_last_operations.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.dgv_last_operations.ColumnHeaderHeight = 35;
      this.dgv_last_operations.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
      dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      dataGridViewCellStyle3.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
      dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
      dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
      this.dgv_last_operations.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
      this.dgv_last_operations.ColumnHeadersHeight = 35;
      this.dgv_last_operations.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
      this.dgv_last_operations.CornerRadius = 10;
      this.dgv_last_operations.DefaultCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.dgv_last_operations.DefaultCellForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.dgv_last_operations.DefaultCellSelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.dgv_last_operations.DefaultCellSelectionForeColor = System.Drawing.Color.White;
      dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      dataGridViewCellStyle4.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      dataGridViewCellStyle4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.White;
      dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
      this.dgv_last_operations.DefaultCellStyle = dataGridViewCellStyle4;
      this.dgv_last_operations.EnableHeadersVisualStyles = false;
      this.dgv_last_operations.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.dgv_last_operations.GridColor = System.Drawing.Color.LightGray;
      this.dgv_last_operations.HeaderBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
      this.dgv_last_operations.HeaderDefaultCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.dgv_last_operations.HeaderDefaultCellForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.dgv_last_operations.HeaderImages = null;
      this.dgv_last_operations.Location = new System.Drawing.Point(-1, 34);
      this.dgv_last_operations.Name = "dgv_last_operations";
      this.dgv_last_operations.ReadOnly = true;
      this.dgv_last_operations.RowHeadersVisible = false;
      this.dgv_last_operations.RowHeadersWidth = 20;
      this.dgv_last_operations.RowTemplate.Height = 35;
      this.dgv_last_operations.RowTemplateHeight = 35;
      this.dgv_last_operations.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
      this.dgv_last_operations.Size = new System.Drawing.Size(664, 143);
      this.dgv_last_operations.Style = WSI.Cashier.Controls.uc_DataGridView.DataGridViewStyle.WITH_BOTTOM_ROUND_BORDERS;
      this.dgv_last_operations.TabIndex = 0;
      this.dgv_last_operations.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_last_operations_RowEnter);
      this.dgv_last_operations.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.dgv_last_operations_RowPrePaint);
      // 
      // btn_close
      // 
      this.btn_close.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_close.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_close.FlatAppearance.BorderSize = 0;
      this.btn_close.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_close.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_close.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_close.Image = null;
      this.btn_close.IsSelected = false;
      this.btn_close.Location = new System.Drawing.Point(521, 582);
      this.btn_close.Name = "btn_close";
      this.btn_close.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_close.Size = new System.Drawing.Size(155, 60);
      this.btn_close.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_close.TabIndex = 5;
      this.btn_close.Text = "XCLOSE/CANCEL";
      this.btn_close.UseVisualStyleBackColor = false;
      this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
      // 
      // tmr_status_printing
      // 
      this.tmr_status_printing.Interval = 300;
      this.tmr_status_printing.Tick += new System.EventHandler(this.tmr_status_printing_Tick);
      // 
      // gb_source
      // 
      this.gb_source.BackColor = System.Drawing.Color.Transparent;
      this.gb_source.BorderColor = System.Drawing.Color.Empty;
      this.gb_source.Controls.Add(this.opt_last_voucher);
      this.gb_source.Controls.Add(this.opt_mobile_bank);
      this.gb_source.Controls.Add(this.btn_print_all);
      this.gb_source.Controls.Add(this.opt_cashier);
      this.gb_source.CornerRadius = 10;
      this.gb_source.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_source.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gb_source.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_source.HeaderHeight = 35;
      this.gb_source.HeaderSubText = null;
      this.gb_source.HeaderText = null;
      this.gb_source.Location = new System.Drawing.Point(110, 237);
      this.gb_source.Name = "gb_source";
      this.gb_source.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_source.Size = new System.Drawing.Size(469, 145);
      this.gb_source.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.gb_source.TabIndex = 8;
      // 
      // frm_vouchers_reprint
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(1024, 713);
      this.Name = "frm_vouchers_reprint";
      this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
      this.Style = WSI.Cashier.Controls.frm_base.BaseFormStyle.PRINCIPAL;
      this.Text = "Vouchers Reprint";
      this.pnl_data.ResumeLayout(false);
      this.gb_cashier_sessions.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.dgv_cashier_sessions)).EndInit();
      this.gb_last_operations.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.dgv_last_operations)).EndInit();
      this.gb_source.ResumeLayout(false);
      this.gb_source.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private WSI.Cashier.Controls.uc_round_panel gb_cashier_sessions;
    private WSI.Cashier.Controls.uc_round_button btn_print_selected;
    private WSI.Cashier.Controls.uc_round_button btn_print_all;
    private WSI.Cashier.Controls.uc_round_panel gb_last_operations;
    private WSI.Cashier.Controls.uc_DataGridView dgv_last_operations;
    private WSI.Cashier.Controls.uc_round_button btn_close;
    private WSI.Cashier.Controls.uc_radioButton opt_cashier;
    private WSI.Cashier.Controls.uc_radioButton opt_mobile_bank;
    private WSI.Cashier.Controls.uc_radioButton opt_last_voucher;
    public System.Windows.Forms.WebBrowser web_browser;
    private WSI.Cashier.Controls.uc_label lbl_print_status;
    private WSI.Cashier.Controls.uc_round_button btn_pause_continue;
    private System.Windows.Forms.Timer tmr_status_printing;
    private WSI.Cashier.Controls.uc_DataGridView dgv_cashier_sessions;
    private Controls.uc_round_panel gb_source;
    

  }
}