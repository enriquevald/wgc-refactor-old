﻿using WSI.Cashier.Controls;

namespace WSI.Cashier
{
  partial class uc_address
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.pnl_manual_address = new System.Windows.Forms.Panel();
      this.lay_address = new System.Windows.Forms.TableLayoutPanel();
      this.txt_city = new WSI.Cashier.CharacterTextBox();
      this.lbl_country = new WSI.Cashier.Controls.uc_label();
      this.lbl_address_01 = new WSI.Cashier.Controls.uc_label();
      this.txt_ext_num = new WSI.Cashier.CharacterTextBox();
      this.lbl_fed_entity = new WSI.Cashier.Controls.uc_label();
      this.txt_address_01 = new WSI.Cashier.CharacterTextBox();
      this.txt_zip = new WSI.Cashier.Controls.uc_round_textbox();
      this.lbl_zip = new WSI.Cashier.Controls.uc_label();
      this.lbl_city = new WSI.Cashier.Controls.uc_label();
      this.lbl_address_03 = new WSI.Cashier.Controls.uc_label();
      this.lbl_address_02 = new WSI.Cashier.Controls.uc_label();
      this.lbl_ext_num = new WSI.Cashier.Controls.uc_label();
      this.cb_country = new WSI.Cashier.Controls.uc_round_auto_combobox();
      this.txt_address_02 = new WSI.Cashier.CharacterTextBox();
      this.txt_address_03 = new WSI.Cashier.CharacterTextBox();
      this.cb_fed_entity = new WSI.Cashier.Controls.uc_round_auto_combobox();
      this.pnl_auto_address = new System.Windows.Forms.Panel();
      this.lay_auto_address = new System.Windows.Forms.TableLayoutPanel();
      this.cb_city_auto = new WSI.Cashier.Controls.uc_round_auto_combobox();
      this.cb_address_03_auto = new WSI.Cashier.Controls.uc_round_auto_combobox();
      this.cb_address_02_auto = new WSI.Cashier.Controls.uc_round_auto_combobox();
      this.lbl_country_auto = new WSI.Cashier.Controls.uc_label();
      this.lbl_address_01_auto = new WSI.Cashier.Controls.uc_label();
      this.cb_fed_entity_auto = new WSI.Cashier.Controls.uc_round_auto_combobox();
      this.txt_ext_num_auto = new WSI.Cashier.CharacterTextBox();
      this.lbl_fed_entity_auto = new WSI.Cashier.Controls.uc_label();
      this.txt_address_01_auto = new WSI.Cashier.CharacterTextBox();
      this.txt_zip_auto = new WSI.Cashier.Controls.uc_round_textbox();
      this.lbl_zip_auto = new WSI.Cashier.Controls.uc_label();
      this.lbl_city_auto = new WSI.Cashier.Controls.uc_label();
      this.lbl_address_03_auto = new WSI.Cashier.Controls.uc_label();
      this.lbl_address_02_auto = new WSI.Cashier.Controls.uc_label();
      this.lbl_num_ext_auto = new WSI.Cashier.Controls.uc_label();
      this.cb_country_auto = new WSI.Cashier.Controls.uc_round_auto_combobox();
      this.lbl_address_validation = new WSI.Cashier.Controls.uc_label();
      this.chk_address_mode = new WSI.Cashier.Controls.uc_checkBox();
      this.pnl_manual_address.SuspendLayout();
      this.lay_address.SuspendLayout();
      this.pnl_auto_address.SuspendLayout();
      this.lay_auto_address.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_manual_address
      // 
      this.pnl_manual_address.Controls.Add(this.lay_address);
      this.pnl_manual_address.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pnl_manual_address.Location = new System.Drawing.Point(0, 0);
      this.pnl_manual_address.Name = "pnl_manual_address";
      this.pnl_manual_address.Size = new System.Drawing.Size(870, 315);
      this.pnl_manual_address.TabIndex = 2;
      // 
      // lay_address
      // 
      this.lay_address.ColumnCount = 4;
      this.lay_address.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
      this.lay_address.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
      this.lay_address.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
      this.lay_address.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
      this.lay_address.Controls.Add(this.txt_city, 1, 4);
      this.lay_address.Controls.Add(this.lbl_country, 0, 5);
      this.lay_address.Controls.Add(this.lbl_address_01, 0, 0);
      this.lay_address.Controls.Add(this.txt_ext_num, 1, 1);
      this.lay_address.Controls.Add(this.lbl_fed_entity, 2, 5);
      this.lay_address.Controls.Add(this.txt_address_01, 1, 0);
      this.lay_address.Controls.Add(this.txt_zip, 3, 1);
      this.lay_address.Controls.Add(this.lbl_zip, 2, 1);
      this.lay_address.Controls.Add(this.lbl_city, 0, 4);
      this.lay_address.Controls.Add(this.lbl_address_03, 0, 3);
      this.lay_address.Controls.Add(this.lbl_address_02, 0, 2);
      this.lay_address.Controls.Add(this.lbl_ext_num, 0, 1);
      this.lay_address.Controls.Add(this.cb_country, 1, 5);
      this.lay_address.Controls.Add(this.txt_address_02, 1, 2);
      this.lay_address.Controls.Add(this.txt_address_03, 1, 3);
      this.lay_address.Controls.Add(this.cb_fed_entity, 3, 5);
      this.lay_address.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lay_address.Location = new System.Drawing.Point(0, 0);
      this.lay_address.Name = "lay_address";
      this.lay_address.Padding = new System.Windows.Forms.Padding(3);
      this.lay_address.RowCount = 7;
      this.lay_address.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.lay_address.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.lay_address.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.lay_address.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.lay_address.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.lay_address.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.lay_address.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.lay_address.Size = new System.Drawing.Size(870, 315);
      this.lay_address.TabIndex = 16;
      // 
      // txt_city
      // 
      this.txt_city.AllowSpace = true;
      this.txt_city.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_city.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_city.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_city.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_city.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.lay_address.SetColumnSpan(this.txt_city, 3);
      this.txt_city.CornerRadius = 5;
      this.txt_city.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_city.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.ExtendedMode;
      this.txt_city.Location = new System.Drawing.Point(147, 183);
      this.txt_city.Margin = new System.Windows.Forms.Padding(0);
      this.txt_city.MaxLength = 50;
      this.txt_city.Multiline = false;
      this.txt_city.Name = "txt_city";
      this.txt_city.PasswordChar = '\0';
      this.txt_city.ReadOnly = false;
      this.txt_city.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_city.SelectedText = "";
      this.txt_city.SelectionLength = 0;
      this.txt_city.SelectionStart = 0;
      this.txt_city.Size = new System.Drawing.Size(708, 40);
      this.txt_city.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_city.TabIndex = 5;
      this.txt_city.TabStop = false;
      this.txt_city.Tag = "City";
      this.txt_city.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_city.UseSystemPasswordChar = false;
      this.txt_city.WaterMark = null;
      this.txt_city.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // lbl_country
      // 
      this.lbl_country.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_country.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_country.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_country.Location = new System.Drawing.Point(3, 228);
      this.lbl_country.Margin = new System.Windows.Forms.Padding(0);
      this.lbl_country.Name = "lbl_country";
      this.lbl_country.Size = new System.Drawing.Size(144, 45);
      this.lbl_country.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_country.TabIndex = 12;
      this.lbl_country.Tag = "AddressCountry";
      this.lbl_country.Text = "xPaís";
      this.lbl_country.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_address_01
      // 
      this.lbl_address_01.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_address_01.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_address_01.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_address_01.Location = new System.Drawing.Point(3, 3);
      this.lbl_address_01.Margin = new System.Windows.Forms.Padding(0);
      this.lbl_address_01.MinimumSize = new System.Drawing.Size(140, 0);
      this.lbl_address_01.Name = "lbl_address_01";
      this.lbl_address_01.Size = new System.Drawing.Size(144, 45);
      this.lbl_address_01.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_address_01.TabIndex = 0;
      this.lbl_address_01.Tag = "Address";
      this.lbl_address_01.Text = "xCalle";
      this.lbl_address_01.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // txt_ext_num
      // 
      this.txt_ext_num.AllowSpace = true;
      this.txt_ext_num.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_ext_num.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_ext_num.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_ext_num.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_ext_num.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_ext_num.CornerRadius = 5;
      this.txt_ext_num.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_ext_num.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.ExtendedMode;
      this.txt_ext_num.Location = new System.Drawing.Point(147, 48);
      this.txt_ext_num.Margin = new System.Windows.Forms.Padding(0);
      this.txt_ext_num.MaxLength = 10;
      this.txt_ext_num.Multiline = false;
      this.txt_ext_num.Name = "txt_ext_num";
      this.txt_ext_num.PasswordChar = '\0';
      this.txt_ext_num.ReadOnly = false;
      this.txt_ext_num.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_ext_num.SelectedText = "";
      this.txt_ext_num.SelectionLength = 0;
      this.txt_ext_num.SelectionStart = 0;
      this.txt_ext_num.Size = new System.Drawing.Size(200, 40);
      this.txt_ext_num.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_ext_num.TabIndex = 1;
      this.txt_ext_num.TabStop = false;
      this.txt_ext_num.Tag = "ExtNum";
      this.txt_ext_num.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_ext_num.UseSystemPasswordChar = false;
      this.txt_ext_num.WaterMark = null;
      this.txt_ext_num.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // lbl_fed_entity
      // 
      this.lbl_fed_entity.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_fed_entity.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_fed_entity.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_fed_entity.Location = new System.Drawing.Point(350, 228);
      this.lbl_fed_entity.Name = "lbl_fed_entity";
      this.lbl_fed_entity.Size = new System.Drawing.Size(150, 45);
      this.lbl_fed_entity.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_fed_entity.TabIndex = 14;
      this.lbl_fed_entity.Tag = "FedEntity";
      this.lbl_fed_entity.Text = "xFederal State";
      this.lbl_fed_entity.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // txt_address_01
      // 
      this.txt_address_01.AllowSpace = true;
      this.txt_address_01.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_address_01.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_address_01.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_address_01.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_address_01.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.lay_address.SetColumnSpan(this.txt_address_01, 3);
      this.txt_address_01.CornerRadius = 5;
      this.txt_address_01.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_address_01.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.ExtendedMode;
      this.txt_address_01.Location = new System.Drawing.Point(147, 3);
      this.txt_address_01.Margin = new System.Windows.Forms.Padding(0);
      this.txt_address_01.MaxLength = 50;
      this.txt_address_01.Multiline = false;
      this.txt_address_01.Name = "txt_address_01";
      this.txt_address_01.PasswordChar = '\0';
      this.txt_address_01.ReadOnly = false;
      this.txt_address_01.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_address_01.SelectedText = "";
      this.txt_address_01.SelectionLength = 0;
      this.txt_address_01.SelectionStart = 0;
      this.txt_address_01.Size = new System.Drawing.Size(708, 40);
      this.txt_address_01.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_address_01.TabIndex = 0;
      this.txt_address_01.TabStop = false;
      this.txt_address_01.Tag = "Address";
      this.txt_address_01.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_address_01.UseSystemPasswordChar = false;
      this.txt_address_01.WaterMark = null;
      this.txt_address_01.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // txt_zip
      // 
      this.txt_zip.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_zip.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_zip.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_zip.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_zip.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_zip.CornerRadius = 5;
      this.txt_zip.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_zip.Location = new System.Drawing.Point(503, 48);
      this.txt_zip.Margin = new System.Windows.Forms.Padding(0);
      this.txt_zip.MaxLength = 10;
      this.txt_zip.Multiline = false;
      this.txt_zip.Name = "txt_zip";
      this.txt_zip.PasswordChar = '\0';
      this.txt_zip.ReadOnly = false;
      this.txt_zip.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_zip.SelectedText = "";
      this.txt_zip.SelectionLength = 0;
      this.txt_zip.SelectionStart = 0;
      this.txt_zip.Size = new System.Drawing.Size(200, 40);
      this.txt_zip.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_zip.TabIndex = 2;
      this.txt_zip.TabStop = false;
      this.txt_zip.Tag = "Zip";
      this.txt_zip.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_zip.UseSystemPasswordChar = false;
      this.txt_zip.WaterMark = null;
      this.txt_zip.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // lbl_zip
      // 
      this.lbl_zip.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_zip.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_zip.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_zip.Location = new System.Drawing.Point(347, 48);
      this.lbl_zip.Margin = new System.Windows.Forms.Padding(0);
      this.lbl_zip.Name = "lbl_zip";
      this.lbl_zip.Size = new System.Drawing.Size(156, 45);
      this.lbl_zip.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_zip.TabIndex = 4;
      this.lbl_zip.Tag = "Zip";
      this.lbl_zip.Text = "xC.Postal";
      this.lbl_zip.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_city
      // 
      this.lbl_city.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_city.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_city.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_city.Location = new System.Drawing.Point(3, 183);
      this.lbl_city.Margin = new System.Windows.Forms.Padding(0);
      this.lbl_city.Name = "lbl_city";
      this.lbl_city.Size = new System.Drawing.Size(144, 45);
      this.lbl_city.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_city.TabIndex = 10;
      this.lbl_city.Tag = "City";
      this.lbl_city.Text = "xMunicipio";
      this.lbl_city.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_address_03
      // 
      this.lbl_address_03.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_address_03.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_address_03.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_address_03.Location = new System.Drawing.Point(3, 138);
      this.lbl_address_03.Margin = new System.Windows.Forms.Padding(0);
      this.lbl_address_03.Name = "lbl_address_03";
      this.lbl_address_03.Size = new System.Drawing.Size(144, 45);
      this.lbl_address_03.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_address_03.TabIndex = 8;
      this.lbl_address_03.Tag = "Address03";
      this.lbl_address_03.Text = "xDelegación";
      this.lbl_address_03.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_address_02
      // 
      this.lbl_address_02.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_address_02.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_address_02.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_address_02.Location = new System.Drawing.Point(3, 93);
      this.lbl_address_02.Margin = new System.Windows.Forms.Padding(0);
      this.lbl_address_02.Name = "lbl_address_02";
      this.lbl_address_02.Size = new System.Drawing.Size(144, 45);
      this.lbl_address_02.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_address_02.TabIndex = 6;
      this.lbl_address_02.Tag = "Address02";
      this.lbl_address_02.Text = "xColonia";
      this.lbl_address_02.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_ext_num
      // 
      this.lbl_ext_num.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_ext_num.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_ext_num.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_ext_num.Location = new System.Drawing.Point(3, 48);
      this.lbl_ext_num.Margin = new System.Windows.Forms.Padding(0);
      this.lbl_ext_num.Name = "lbl_ext_num";
      this.lbl_ext_num.Size = new System.Drawing.Size(144, 45);
      this.lbl_ext_num.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_ext_num.TabIndex = 2;
      this.lbl_ext_num.Tag = "ExtNum";
      this.lbl_ext_num.Text = "xNúm. Ext.";
      this.lbl_ext_num.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // cb_country
      // 
      this.cb_country.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_country.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.cb_country.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.cb_country.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.cb_country.CornerRadius = 5;
      this.cb_country.DataSource = null;
      this.cb_country.DisplayMember = "";
      this.cb_country.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
      this.cb_country.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
      this.cb_country.DropDownWidth = 200;
      this.cb_country.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.cb_country.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_country.FormattingEnabled = true;
      this.cb_country.FormatValidator = null;
      this.cb_country.IsDroppedDown = false;
      this.cb_country.ItemHeight = 40;
      this.cb_country.Location = new System.Drawing.Point(147, 228);
      this.cb_country.Margin = new System.Windows.Forms.Padding(0);
      this.cb_country.MaxDropDownItems = 8;
      this.cb_country.Name = "cb_country";
      this.cb_country.OnFocusOpenListBox = true;
      this.cb_country.SelectedIndex = -1;
      this.cb_country.SelectedItem = null;
      this.cb_country.SelectedValue = null;
      this.cb_country.SelectionLength = 0;
      this.cb_country.SelectionStart = 0;
      this.cb_country.Size = new System.Drawing.Size(200, 40);
      this.cb_country.Sorted = false;
      this.cb_country.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.BASIC;
      this.cb_country.TabIndex = 6;
      this.cb_country.TabStop = false;
      this.cb_country.Tag = "AddressCountry";
      this.cb_country.ValueMember = "";
      this.cb_country.SelectedIndexChanged += new System.EventHandler(this.cb_country_SelectedIndexChanged);
      // 
      // txt_address_02
      // 
      this.txt_address_02.AllowSpace = true;
      this.txt_address_02.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_address_02.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_address_02.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_address_02.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_address_02.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.lay_address.SetColumnSpan(this.txt_address_02, 3);
      this.txt_address_02.CornerRadius = 5;
      this.txt_address_02.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_address_02.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.ExtendedMode;
      this.txt_address_02.Location = new System.Drawing.Point(147, 93);
      this.txt_address_02.Margin = new System.Windows.Forms.Padding(0);
      this.txt_address_02.MaxLength = 100;
      this.txt_address_02.Multiline = false;
      this.txt_address_02.Name = "txt_address_02";
      this.txt_address_02.PasswordChar = '\0';
      this.txt_address_02.ReadOnly = false;
      this.txt_address_02.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_address_02.SelectedText = "";
      this.txt_address_02.SelectionLength = 0;
      this.txt_address_02.SelectionStart = 0;
      this.txt_address_02.Size = new System.Drawing.Size(708, 40);
      this.txt_address_02.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_address_02.TabIndex = 3;
      this.txt_address_02.TabStop = false;
      this.txt_address_02.Tag = "Address02";
      this.txt_address_02.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_address_02.UseSystemPasswordChar = false;
      this.txt_address_02.WaterMark = null;
      this.txt_address_02.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // txt_address_03
      // 
      this.txt_address_03.AllowSpace = true;
      this.txt_address_03.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_address_03.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_address_03.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_address_03.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_address_03.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.lay_address.SetColumnSpan(this.txt_address_03, 3);
      this.txt_address_03.CornerRadius = 5;
      this.txt_address_03.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_address_03.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.ExtendedMode;
      this.txt_address_03.Location = new System.Drawing.Point(147, 138);
      this.txt_address_03.Margin = new System.Windows.Forms.Padding(0);
      this.txt_address_03.MaxLength = 50;
      this.txt_address_03.Multiline = false;
      this.txt_address_03.Name = "txt_address_03";
      this.txt_address_03.PasswordChar = '\0';
      this.txt_address_03.ReadOnly = false;
      this.txt_address_03.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_address_03.SelectedText = "";
      this.txt_address_03.SelectionLength = 0;
      this.txt_address_03.SelectionStart = 0;
      this.txt_address_03.Size = new System.Drawing.Size(708, 40);
      this.txt_address_03.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_address_03.TabIndex = 4;
      this.txt_address_03.TabStop = false;
      this.txt_address_03.Tag = "Address03";
      this.txt_address_03.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_address_03.UseSystemPasswordChar = false;
      this.txt_address_03.WaterMark = null;
      this.txt_address_03.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // cb_fed_entity
      // 
      this.cb_fed_entity.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_fed_entity.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.cb_fed_entity.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.cb_fed_entity.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.cb_fed_entity.CornerRadius = 5;
      this.cb_fed_entity.DataSource = null;
      this.cb_fed_entity.DisplayMember = "";
      this.cb_fed_entity.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
      this.cb_fed_entity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
      this.cb_fed_entity.DropDownWidth = 346;
      this.cb_fed_entity.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.cb_fed_entity.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_fed_entity.FormattingEnabled = true;
      this.cb_fed_entity.FormatValidator = null;
      this.cb_fed_entity.IsDroppedDown = false;
      this.cb_fed_entity.ItemHeight = 40;
      this.cb_fed_entity.Location = new System.Drawing.Point(503, 228);
      this.cb_fed_entity.Margin = new System.Windows.Forms.Padding(0);
      this.cb_fed_entity.MaxDropDownItems = 8;
      this.cb_fed_entity.Name = "cb_fed_entity";
      this.cb_fed_entity.OnFocusOpenListBox = true;
      this.cb_fed_entity.SelectedIndex = -1;
      this.cb_fed_entity.SelectedItem = null;
      this.cb_fed_entity.SelectedValue = null;
      this.cb_fed_entity.SelectionLength = 0;
      this.cb_fed_entity.SelectionStart = 0;
      this.cb_fed_entity.Size = new System.Drawing.Size(346, 40);
      this.cb_fed_entity.Sorted = false;
      this.cb_fed_entity.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.BASIC;
      this.cb_fed_entity.TabIndex = 7;
      this.cb_fed_entity.Tag = "FedEntity";
      this.cb_fed_entity.ValueMember = "";
      // 
      // pnl_auto_address
      // 
      this.pnl_auto_address.Controls.Add(this.lay_auto_address);
      this.pnl_auto_address.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pnl_auto_address.Location = new System.Drawing.Point(0, 0);
      this.pnl_auto_address.Name = "pnl_auto_address";
      this.pnl_auto_address.Size = new System.Drawing.Size(870, 315);
      this.pnl_auto_address.TabIndex = 3;
      this.pnl_auto_address.Visible = false;
      // 
      // lay_auto_address
      // 
      this.lay_auto_address.ColumnCount = 4;
      this.lay_auto_address.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
      this.lay_auto_address.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
      this.lay_auto_address.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
      this.lay_auto_address.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
      this.lay_auto_address.Controls.Add(this.cb_city_auto, 1, 4);
      this.lay_auto_address.Controls.Add(this.cb_address_03_auto, 1, 3);
      this.lay_auto_address.Controls.Add(this.cb_address_02_auto, 1, 2);
      this.lay_auto_address.Controls.Add(this.lbl_country_auto, 0, 5);
      this.lay_auto_address.Controls.Add(this.lbl_address_01_auto, 0, 0);
      this.lay_auto_address.Controls.Add(this.cb_fed_entity_auto, 3, 5);
      this.lay_auto_address.Controls.Add(this.txt_ext_num_auto, 1, 1);
      this.lay_auto_address.Controls.Add(this.lbl_fed_entity_auto, 2, 5);
      this.lay_auto_address.Controls.Add(this.txt_address_01_auto, 1, 0);
      this.lay_auto_address.Controls.Add(this.txt_zip_auto, 3, 1);
      this.lay_auto_address.Controls.Add(this.lbl_zip_auto, 2, 1);
      this.lay_auto_address.Controls.Add(this.lbl_city_auto, 0, 4);
      this.lay_auto_address.Controls.Add(this.lbl_address_03_auto, 0, 3);
      this.lay_auto_address.Controls.Add(this.lbl_address_02_auto, 0, 2);
      this.lay_auto_address.Controls.Add(this.lbl_num_ext_auto, 0, 1);
      this.lay_auto_address.Controls.Add(this.cb_country_auto, 1, 5);
      this.lay_auto_address.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lay_auto_address.Location = new System.Drawing.Point(0, 0);
      this.lay_auto_address.Name = "lay_auto_address";
      this.lay_auto_address.Padding = new System.Windows.Forms.Padding(3);
      this.lay_auto_address.RowCount = 7;
      this.lay_auto_address.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.lay_auto_address.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.lay_auto_address.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.lay_auto_address.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.lay_auto_address.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.lay_auto_address.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.lay_auto_address.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.lay_auto_address.Size = new System.Drawing.Size(870, 315);
      this.lay_auto_address.TabIndex = 41;
      // 
      // cb_city_auto
      // 
      this.cb_city_auto.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_city_auto.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.cb_city_auto.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.cb_city_auto.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.lay_auto_address.SetColumnSpan(this.cb_city_auto, 3);
      this.cb_city_auto.CornerRadius = 5;
      this.cb_city_auto.DataSource = null;
      this.cb_city_auto.DisplayMember = "";
      this.cb_city_auto.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
      this.cb_city_auto.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
      this.cb_city_auto.DropDownWidth = 708;
      this.cb_city_auto.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.cb_city_auto.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_city_auto.FormattingEnabled = true;
      this.cb_city_auto.FormatValidator = null;
      this.cb_city_auto.IsDroppedDown = false;
      this.cb_city_auto.ItemHeight = 40;
      this.cb_city_auto.Location = new System.Drawing.Point(147, 183);
      this.cb_city_auto.Margin = new System.Windows.Forms.Padding(0);
      this.cb_city_auto.MaxDropDownItems = 8;
      this.cb_city_auto.Name = "cb_city_auto";
      this.cb_city_auto.OnFocusOpenListBox = true;
      this.cb_city_auto.SelectedIndex = -1;
      this.cb_city_auto.SelectedItem = null;
      this.cb_city_auto.SelectedValue = null;
      this.cb_city_auto.SelectionLength = 0;
      this.cb_city_auto.SelectionStart = 0;
      this.cb_city_auto.Size = new System.Drawing.Size(708, 40);
      this.cb_city_auto.Sorted = false;
      this.cb_city_auto.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.BASIC;
      this.cb_city_auto.TabIndex = 6;
      this.cb_city_auto.TabStop = false;
      this.cb_city_auto.Tag = "Town";
      this.cb_city_auto.ValueMember = "";
      // 
      // cb_address_03_auto
      // 
      this.cb_address_03_auto.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_address_03_auto.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.cb_address_03_auto.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.cb_address_03_auto.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.lay_auto_address.SetColumnSpan(this.cb_address_03_auto, 3);
      this.cb_address_03_auto.CornerRadius = 5;
      this.cb_address_03_auto.DataSource = null;
      this.cb_address_03_auto.DisplayMember = "";
      this.cb_address_03_auto.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
      this.cb_address_03_auto.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
      this.cb_address_03_auto.DropDownWidth = 708;
      this.cb_address_03_auto.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.cb_address_03_auto.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_address_03_auto.FormattingEnabled = true;
      this.cb_address_03_auto.FormatValidator = null;
      this.cb_address_03_auto.IsDroppedDown = false;
      this.cb_address_03_auto.ItemHeight = 40;
      this.cb_address_03_auto.Location = new System.Drawing.Point(147, 138);
      this.cb_address_03_auto.Margin = new System.Windows.Forms.Padding(0);
      this.cb_address_03_auto.MaxDropDownItems = 8;
      this.cb_address_03_auto.Name = "cb_address_03_auto";
      this.cb_address_03_auto.OnFocusOpenListBox = true;
      this.cb_address_03_auto.SelectedIndex = -1;
      this.cb_address_03_auto.SelectedItem = null;
      this.cb_address_03_auto.SelectedValue = null;
      this.cb_address_03_auto.SelectionLength = 0;
      this.cb_address_03_auto.SelectionStart = 0;
      this.cb_address_03_auto.Size = new System.Drawing.Size(708, 40);
      this.cb_address_03_auto.Sorted = false;
      this.cb_address_03_auto.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.BASIC;
      this.cb_address_03_auto.TabIndex = 5;
      this.cb_address_03_auto.TabStop = false;
      this.cb_address_03_auto.Tag = "Line3";
      this.cb_address_03_auto.ValueMember = "";
      // 
      // cb_address_02_auto
      // 
      this.cb_address_02_auto.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_address_02_auto.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.cb_address_02_auto.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.cb_address_02_auto.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.lay_auto_address.SetColumnSpan(this.cb_address_02_auto, 3);
      this.cb_address_02_auto.CornerRadius = 5;
      this.cb_address_02_auto.DataSource = null;
      this.cb_address_02_auto.DisplayMember = "";
      this.cb_address_02_auto.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
      this.cb_address_02_auto.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
      this.cb_address_02_auto.DropDownWidth = 708;
      this.cb_address_02_auto.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.cb_address_02_auto.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_address_02_auto.FormattingEnabled = true;
      this.cb_address_02_auto.FormatValidator = null;
      this.cb_address_02_auto.IsDroppedDown = false;
      this.cb_address_02_auto.ItemHeight = 40;
      this.cb_address_02_auto.Location = new System.Drawing.Point(147, 93);
      this.cb_address_02_auto.Margin = new System.Windows.Forms.Padding(0);
      this.cb_address_02_auto.MaxDropDownItems = 8;
      this.cb_address_02_auto.Name = "cb_address_02_auto";
      this.cb_address_02_auto.OnFocusOpenListBox = true;
      this.cb_address_02_auto.SelectedIndex = -1;
      this.cb_address_02_auto.SelectedItem = null;
      this.cb_address_02_auto.SelectedValue = null;
      this.cb_address_02_auto.SelectionLength = 0;
      this.cb_address_02_auto.SelectionStart = 0;
      this.cb_address_02_auto.Size = new System.Drawing.Size(708, 40);
      this.cb_address_02_auto.Sorted = false;
      this.cb_address_02_auto.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.BASIC;
      this.cb_address_02_auto.TabIndex = 4;
      this.cb_address_02_auto.TabStop = false;
      this.cb_address_02_auto.Tag = "Line2";
      this.cb_address_02_auto.ValueMember = "";
      // 
      // lbl_country_auto
      // 
      this.lbl_country_auto.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_country_auto.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_country_auto.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_country_auto.Location = new System.Drawing.Point(3, 228);
      this.lbl_country_auto.Margin = new System.Windows.Forms.Padding(0);
      this.lbl_country_auto.Name = "lbl_country_auto";
      this.lbl_country_auto.Size = new System.Drawing.Size(144, 45);
      this.lbl_country_auto.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_country_auto.TabIndex = 12;
      this.lbl_country_auto.Tag = "Country";
      this.lbl_country_auto.Text = "xPaís";
      this.lbl_country_auto.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_address_01_auto
      // 
      this.lbl_address_01_auto.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_address_01_auto.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_address_01_auto.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_address_01_auto.Location = new System.Drawing.Point(3, 3);
      this.lbl_address_01_auto.Margin = new System.Windows.Forms.Padding(0);
      this.lbl_address_01_auto.MinimumSize = new System.Drawing.Size(140, 0);
      this.lbl_address_01_auto.Name = "lbl_address_01_auto";
      this.lbl_address_01_auto.Size = new System.Drawing.Size(144, 45);
      this.lbl_address_01_auto.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_address_01_auto.TabIndex = 0;
      this.lbl_address_01_auto.Tag = "Line1";
      this.lbl_address_01_auto.Text = "xCalle";
      this.lbl_address_01_auto.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // cb_fed_entity_auto
      // 
      this.cb_fed_entity_auto.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_fed_entity_auto.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.cb_fed_entity_auto.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.cb_fed_entity_auto.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.cb_fed_entity_auto.CornerRadius = 5;
      this.cb_fed_entity_auto.DataSource = null;
      this.cb_fed_entity_auto.DisplayMember = "";
      this.cb_fed_entity_auto.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
      this.cb_fed_entity_auto.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
      this.cb_fed_entity_auto.DropDownWidth = 346;
      this.cb_fed_entity_auto.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.cb_fed_entity_auto.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_fed_entity_auto.FormattingEnabled = true;
      this.cb_fed_entity_auto.FormatValidator = null;
      this.cb_fed_entity_auto.IsDroppedDown = false;
      this.cb_fed_entity_auto.ItemHeight = 40;
      this.cb_fed_entity_auto.Location = new System.Drawing.Point(497, 228);
      this.cb_fed_entity_auto.Margin = new System.Windows.Forms.Padding(0);
      this.cb_fed_entity_auto.MaxDropDownItems = 8;
      this.cb_fed_entity_auto.Name = "cb_fed_entity_auto";
      this.cb_fed_entity_auto.OnFocusOpenListBox = true;
      this.cb_fed_entity_auto.SelectedIndex = -1;
      this.cb_fed_entity_auto.SelectedItem = null;
      this.cb_fed_entity_auto.SelectedValue = null;
      this.cb_fed_entity_auto.SelectionLength = 0;
      this.cb_fed_entity_auto.SelectionStart = 0;
      this.cb_fed_entity_auto.Size = new System.Drawing.Size(346, 40);
      this.cb_fed_entity_auto.Sorted = false;
      this.cb_fed_entity_auto.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.BASIC;
      this.cb_fed_entity_auto.TabIndex = 7;
      this.cb_fed_entity_auto.TabStop = false;
      this.cb_fed_entity_auto.Tag = "State";
      this.cb_fed_entity_auto.ValueMember = "";
      // 
      // txt_ext_num_auto
      // 
      this.txt_ext_num_auto.AllowSpace = true;
      this.txt_ext_num_auto.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_ext_num_auto.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_ext_num_auto.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_ext_num_auto.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_ext_num_auto.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_ext_num_auto.CornerRadius = 5;
      this.txt_ext_num_auto.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_ext_num_auto.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.ExtendedMode;
      this.txt_ext_num_auto.Location = new System.Drawing.Point(147, 48);
      this.txt_ext_num_auto.Margin = new System.Windows.Forms.Padding(0);
      this.txt_ext_num_auto.MaxLength = 10;
      this.txt_ext_num_auto.Multiline = false;
      this.txt_ext_num_auto.Name = "txt_ext_num_auto";
      this.txt_ext_num_auto.PasswordChar = '\0';
      this.txt_ext_num_auto.ReadOnly = false;
      this.txt_ext_num_auto.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_ext_num_auto.SelectedText = "";
      this.txt_ext_num_auto.SelectionLength = 0;
      this.txt_ext_num_auto.SelectionStart = 0;
      this.txt_ext_num_auto.Size = new System.Drawing.Size(200, 40);
      this.txt_ext_num_auto.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_ext_num_auto.TabIndex = 3;
      this.txt_ext_num_auto.TabStop = false;
      this.txt_ext_num_auto.Tag = "HouseNumber";
      this.txt_ext_num_auto.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_ext_num_auto.UseSystemPasswordChar = false;
      this.txt_ext_num_auto.WaterMark = null;
      this.txt_ext_num_auto.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // lbl_fed_entity_auto
      // 
      this.lbl_fed_entity_auto.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_fed_entity_auto.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_fed_entity_auto.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_fed_entity_auto.Location = new System.Drawing.Point(347, 228);
      this.lbl_fed_entity_auto.Margin = new System.Windows.Forms.Padding(0);
      this.lbl_fed_entity_auto.Name = "lbl_fed_entity_auto";
      this.lbl_fed_entity_auto.Size = new System.Drawing.Size(150, 45);
      this.lbl_fed_entity_auto.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_fed_entity_auto.TabIndex = 14;
      this.lbl_fed_entity_auto.Tag = "State";
      this.lbl_fed_entity_auto.Text = "xFederal State";
      this.lbl_fed_entity_auto.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // txt_address_01_auto
      // 
      this.txt_address_01_auto.AllowSpace = true;
      this.txt_address_01_auto.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_address_01_auto.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_address_01_auto.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_address_01_auto.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_address_01_auto.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.lay_auto_address.SetColumnSpan(this.txt_address_01_auto, 3);
      this.txt_address_01_auto.CornerRadius = 5;
      this.txt_address_01_auto.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_address_01_auto.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.ExtendedMode;
      this.txt_address_01_auto.Location = new System.Drawing.Point(147, 3);
      this.txt_address_01_auto.Margin = new System.Windows.Forms.Padding(0);
      this.txt_address_01_auto.MaxLength = 50;
      this.txt_address_01_auto.Multiline = false;
      this.txt_address_01_auto.Name = "txt_address_01_auto";
      this.txt_address_01_auto.PasswordChar = '\0';
      this.txt_address_01_auto.ReadOnly = false;
      this.txt_address_01_auto.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_address_01_auto.SelectedText = "";
      this.txt_address_01_auto.SelectionLength = 0;
      this.txt_address_01_auto.SelectionStart = 0;
      this.txt_address_01_auto.Size = new System.Drawing.Size(708, 40);
      this.txt_address_01_auto.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_address_01_auto.TabIndex = 2;
      this.txt_address_01_auto.TabStop = false;
      this.txt_address_01_auto.Tag = "Line1";
      this.txt_address_01_auto.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_address_01_auto.UseSystemPasswordChar = false;
      this.txt_address_01_auto.WaterMark = null;
      this.txt_address_01_auto.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // txt_zip_auto
      // 
      this.txt_zip_auto.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_zip_auto.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_zip_auto.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_zip_auto.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_zip_auto.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_zip_auto.CornerRadius = 5;
      this.txt_zip_auto.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_zip_auto.Location = new System.Drawing.Point(497, 48);
      this.txt_zip_auto.Margin = new System.Windows.Forms.Padding(0);
      this.txt_zip_auto.MaxLength = 10;
      this.txt_zip_auto.Multiline = false;
      this.txt_zip_auto.Name = "txt_zip_auto";
      this.txt_zip_auto.PasswordChar = '\0';
      this.txt_zip_auto.ReadOnly = false;
      this.txt_zip_auto.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_zip_auto.SelectedText = "";
      this.txt_zip_auto.SelectionLength = 0;
      this.txt_zip_auto.SelectionStart = 0;
      this.txt_zip_auto.Size = new System.Drawing.Size(200, 40);
      this.txt_zip_auto.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_zip_auto.TabIndex = 1;
      this.txt_zip_auto.TabStop = false;
      this.txt_zip_auto.Tag = "PostCode";
      this.txt_zip_auto.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_zip_auto.UseSystemPasswordChar = false;
      this.txt_zip_auto.WaterMark = null;
      this.txt_zip_auto.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // lbl_zip_auto
      // 
      this.lbl_zip_auto.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_zip_auto.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_zip_auto.Location = new System.Drawing.Point(347, 48);
      this.lbl_zip_auto.Margin = new System.Windows.Forms.Padding(0);
      this.lbl_zip_auto.Name = "lbl_zip_auto";
      this.lbl_zip_auto.Size = new System.Drawing.Size(150, 44);
      this.lbl_zip_auto.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_zip_auto.TabIndex = 4;
      this.lbl_zip_auto.Tag = "PostCode";
      this.lbl_zip_auto.Text = "xC.Postal";
      this.lbl_zip_auto.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_city_auto
      // 
      this.lbl_city_auto.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_city_auto.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_city_auto.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_city_auto.Location = new System.Drawing.Point(3, 183);
      this.lbl_city_auto.Margin = new System.Windows.Forms.Padding(0);
      this.lbl_city_auto.Name = "lbl_city_auto";
      this.lbl_city_auto.Size = new System.Drawing.Size(144, 45);
      this.lbl_city_auto.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_city_auto.TabIndex = 10;
      this.lbl_city_auto.Tag = "Town";
      this.lbl_city_auto.Text = "xMunicipio";
      this.lbl_city_auto.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_address_03_auto
      // 
      this.lbl_address_03_auto.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_address_03_auto.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_address_03_auto.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_address_03_auto.Location = new System.Drawing.Point(3, 138);
      this.lbl_address_03_auto.Margin = new System.Windows.Forms.Padding(0);
      this.lbl_address_03_auto.Name = "lbl_address_03_auto";
      this.lbl_address_03_auto.Size = new System.Drawing.Size(144, 45);
      this.lbl_address_03_auto.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_address_03_auto.TabIndex = 8;
      this.lbl_address_03_auto.Tag = "Line2";
      this.lbl_address_03_auto.Text = "xDelegación";
      this.lbl_address_03_auto.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_address_02_auto
      // 
      this.lbl_address_02_auto.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_address_02_auto.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_address_02_auto.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_address_02_auto.Location = new System.Drawing.Point(3, 93);
      this.lbl_address_02_auto.Margin = new System.Windows.Forms.Padding(0);
      this.lbl_address_02_auto.Name = "lbl_address_02_auto";
      this.lbl_address_02_auto.Size = new System.Drawing.Size(144, 45);
      this.lbl_address_02_auto.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_address_02_auto.TabIndex = 6;
      this.lbl_address_02_auto.Tag = "Line2";
      this.lbl_address_02_auto.Text = "xColonia";
      this.lbl_address_02_auto.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_num_ext_auto
      // 
      this.lbl_num_ext_auto.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbl_num_ext_auto.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_num_ext_auto.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_num_ext_auto.Location = new System.Drawing.Point(3, 48);
      this.lbl_num_ext_auto.Margin = new System.Windows.Forms.Padding(0);
      this.lbl_num_ext_auto.Name = "lbl_num_ext_auto";
      this.lbl_num_ext_auto.Size = new System.Drawing.Size(144, 45);
      this.lbl_num_ext_auto.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_num_ext_auto.TabIndex = 2;
      this.lbl_num_ext_auto.Tag = "HouseNumber";
      this.lbl_num_ext_auto.Text = "xNúm. Ext.";
      this.lbl_num_ext_auto.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // cb_country_auto
      // 
      this.cb_country_auto.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_country_auto.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.cb_country_auto.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.cb_country_auto.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.cb_country_auto.CornerRadius = 5;
      this.cb_country_auto.DataSource = null;
      this.cb_country_auto.DisplayMember = "";
      this.cb_country_auto.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
      this.cb_country_auto.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
      this.cb_country_auto.DropDownWidth = 200;
      this.cb_country_auto.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.cb_country_auto.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_country_auto.FormattingEnabled = true;
      this.cb_country_auto.FormatValidator = null;
      this.cb_country_auto.IsDroppedDown = false;
      this.cb_country_auto.ItemHeight = 40;
      this.cb_country_auto.Location = new System.Drawing.Point(147, 228);
      this.cb_country_auto.Margin = new System.Windows.Forms.Padding(0);
      this.cb_country_auto.MaxDropDownItems = 8;
      this.cb_country_auto.Name = "cb_country_auto";
      this.cb_country_auto.OnFocusOpenListBox = true;
      this.cb_country_auto.SelectedIndex = -1;
      this.cb_country_auto.SelectedItem = null;
      this.cb_country_auto.SelectedValue = null;
      this.cb_country_auto.SelectionLength = 0;
      this.cb_country_auto.SelectionStart = 0;
      this.cb_country_auto.Size = new System.Drawing.Size(200, 40);
      this.cb_country_auto.Sorted = false;
      this.cb_country_auto.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.BASIC;
      this.cb_country_auto.TabIndex = 0;
      this.cb_country_auto.TabStop = false;
      this.cb_country_auto.Tag = "Country";
      this.cb_country_auto.ValueMember = "";
      // 
      // lbl_address_validation
      // 
      this.lbl_address_validation.Cursor = System.Windows.Forms.Cursors.Default;
      this.lbl_address_validation.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_address_validation.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_address_validation.Location = new System.Drawing.Point(347, 281);
      this.lbl_address_validation.Name = "lbl_address_validation";
      this.lbl_address_validation.Size = new System.Drawing.Size(516, 20);
      this.lbl_address_validation.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_12PX_BLUE;
      this.lbl_address_validation.TabIndex = 1004;
      this.lbl_address_validation.Text = "xAddressValidation";
      this.lbl_address_validation.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      this.lbl_address_validation.UseMnemonic = false;
      // 
      // chk_address_mode
      // 
      this.chk_address_mode.AutoSize = true;
      this.chk_address_mode.BackColor = System.Drawing.Color.Transparent;
      this.chk_address_mode.Checked = true;
      this.chk_address_mode.CheckState = System.Windows.Forms.CheckState.Checked;
      this.chk_address_mode.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.chk_address_mode.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.chk_address_mode.Location = new System.Drawing.Point(6, 277);
      this.chk_address_mode.MinimumSize = new System.Drawing.Size(175, 30);
      this.chk_address_mode.Name = "chk_address_mode";
      this.chk_address_mode.Padding = new System.Windows.Forms.Padding(5);
      this.chk_address_mode.Size = new System.Drawing.Size(175, 33);
      this.chk_address_mode.TabIndex = 0;
      this.chk_address_mode.Text = "xAddressModeFree";
      this.chk_address_mode.UseVisualStyleBackColor = true;
      this.chk_address_mode.CheckedChanged += new System.EventHandler(this.chk_address_mode_CheckedChanged);
      // 
      // uc_address
      // 
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
      this.Controls.Add(this.lbl_address_validation);
      this.Controls.Add(this.chk_address_mode);
      this.Controls.Add(this.pnl_manual_address);
      this.Controls.Add(this.pnl_auto_address);
      this.Name = "uc_address";
      this.Size = new System.Drawing.Size(870, 315);
      this.Load += new System.EventHandler(this.uc_address_Load);
      this.pnl_manual_address.ResumeLayout(false);
      this.lay_address.ResumeLayout(false);
      this.pnl_auto_address.ResumeLayout(false);
      this.lay_auto_address.ResumeLayout(false);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Panel pnl_manual_address;
    private System.Windows.Forms.TableLayoutPanel lay_address;
    private uc_label lbl_country;
    private uc_label lbl_address_01;
    private CharacterTextBox txt_ext_num;
    private uc_label lbl_fed_entity;
    private CharacterTextBox txt_address_01;
    private WSI.Cashier.Controls.uc_round_textbox txt_zip;
    private uc_label lbl_zip;
    private uc_label lbl_city;
    private uc_label lbl_address_03;
    private CharacterTextBox txt_city;
    private uc_label lbl_address_02;
    private uc_label lbl_ext_num;
    private WSI.Cashier.Controls.uc_round_auto_combobox cb_country;
    private CharacterTextBox txt_address_02;
    private CharacterTextBox txt_address_03;
    private WSI.Cashier.Controls.uc_round_auto_combobox cb_fed_entity;
    private System.Windows.Forms.Panel pnl_auto_address;
    private System.Windows.Forms.TableLayoutPanel lay_auto_address;
    private WSI.Cashier.Controls.uc_round_auto_combobox cb_city_auto;
    private WSI.Cashier.Controls.uc_round_auto_combobox cb_address_03_auto;
    private WSI.Cashier.Controls.uc_round_auto_combobox cb_address_02_auto;
    private uc_label lbl_country_auto;
    private uc_label lbl_address_01_auto;
    private WSI.Cashier.Controls.uc_round_auto_combobox cb_fed_entity_auto;
    private CharacterTextBox txt_ext_num_auto;
    private uc_label lbl_fed_entity_auto;
    private CharacterTextBox txt_address_01_auto;
    private WSI.Cashier.Controls.uc_round_textbox txt_zip_auto;
    private uc_label lbl_zip_auto;
    private uc_label lbl_city_auto;
    private uc_label lbl_address_03_auto;
    private uc_label lbl_address_02_auto;
    private uc_label lbl_num_ext_auto;
    private WSI.Cashier.Controls.uc_round_auto_combobox cb_country_auto;
    private uc_label lbl_address_validation;
    private WSI.Cashier.Controls.uc_checkBox chk_address_mode;
  }
}
