//------------------------------------------------------------------------------
// Copyright � 2007-2009 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_container.cs
// 
//   DESCRIPTION: Implements the uc_card user control
//                Class: uc_card (Card Operations)
//
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 01-AUG-2007 AJQ     First release.
// 24-NOV-2011 RCI     If card is blocked, disable buttons.
// 24-NOV-2011 JMM     Automatically generate PIN on Cash-in, redeem... if account hasn't
// 29-MAR-2012 MPO     User session: Added a event for each button for set the last activity.
// 13-APR-2012 JCM     When a new player card is passed, now you can select between create an anonymous or personalized account.
// 19-APR-2012 JCM     Recycle Player Card functionallity added
// 02-MAY-2012 JCM     Fixed Bug #267: Fixed focus in card reader.
// 04-MAY-2012 JCM     After first new account created, buttons appears disabled
// 10-MAY-2012 DDM     If is SAS-HOST always change balance in the accounts and play_sessions.
// 22-MAY-2012 RCI     Reset authorized user at the uc_card/uc_mobile_bank level, not in internal routines (don't do it in DB_CardCreditAdd()).
// 25-MAY-2012 SSC     Moved CardData, PlaySession and PlayerTrackingData classes to WSI.Common
// 30-MAY-2012 SSC     Moved Draws, DrawNumbers and DrawNumberList classes to WSI.Common
// 03-JUL-2012 JCM     Can't change the account card until card is paid
// 26-JUL-2012 RCI     TotalRedeem button must be enabled only when there is something to pay to the client.
// 07-NOV-2012 RRB     Fixed bug: when customizing anonymous account, the first time shows error.
// 21-FEB-2013 JCM     Recharge/Redeem are only allowed using physic card.
// 25-MAR-2013 MPO     When is member of multisite, recycle button are disabled
// 09-MAY-2013 JCA     New functionality in btn_vouchers_space_Click.
// 17-MAY-2013 JMM     Call to GetBlockReason added.
// 28-MAY-2013 NMR     Preventing the focus losing on main window
// 29-MAY-2013 AJQ     Fixed Bug #806: After 'personalizacion' of a new account the 'recarga' goes to the previous read account.
// 14-JUN-2013 RRR     Fixed Bug #854: Set card as paid when account is created and card price is 0
// 02-JUL-2013 RRR     Moddified view parameters for Window Mode
// 24-JUL-2013 LEM     Added support to notificate and automatic payment of Site Jackpots.
// 22-JUL-2013 JCOR    AntiMoney laundering alert system.
// 25-JUL-2013 DLL     Support currency exchange
// 27-JUL-2013 RCI     Re-written the anti money laundering support.
// 20-AUG-2013 RMS     Fixed Bug WIG-109: Use of PreferredHolderId and PreferredHolderIdType to show the correct HolderId instead of HolderId.
// 19-SEP-2013 NMR     Minor change while calling ticket creation
// 25-SEP-2013 NMR     Changed conditions for TITO execution mode
// 30-SEP-2013 NMR     Searching/creating internal account for Cashier in TITO mode
// 10-OCT-2013 NMR     Calling another account creation
//                     Added new TITO permissions with tickets
// 11-OCT-2013 RMS     Fixed Bug WIG-272: Update Cashier Session Status
// 14-OCT-2013 RMS     Added scpecific check permissions for account creation and edition depending on type: anonymous or personal.
// 18-OCT-2013 JBC     Commented some code awaiting approval.
// 22-OCT-2013 ICS     Added Undo Operation support
// 28-OCT-2013 DLL     AML: Added permission to exceed report limit
// 04-NOV-2013 LJM     Added msgbox for failed recharges
// 05-NOV-2013 ICS     Fixed Bug WIG-374: An operation can be undone without passing Player Card
// 12-NOV-2013 JML     Fixed Bug WIG-403: Recharge failed to reach the sales limit.
// 21-NOV-2013 AMF     New comment when close session.
// 21-NOV-2013 JML     Fixed Bug WIG-424: Block account player: Some buttons on the Cashier are not disabled.
// 16-JAN-2014 ACM     Added Undo Chips Sale/Purchase operations
// 23-JAN-2013 DLL     Fixed Bug WIG-528: without gaming tables enabled, buttons Buy & Sell Chips are visible
// 23-JAN-2013 DLL     Fixed Bug WIG-530: Enabled buttons Buy & Sell Chips without card
// 29-ENE-2014 LJM     Recharges payed with credit card or check, should not be accounted for AML
// 03-FEB-2014 LJM     Transfer redeemable credit between accounts
// 14-FEB-2014 LJM     Fixed Bug WIG-632: cashier can sell chips when amount > balance
// 05-MAR-2014 DLL     Fixed Bug WIG-691: unnecessary message if we don't have permission to chip sale
// 17-MAR-2014 JRM     Added new functionality. Description contained in RequestPinCodeDUT.docx DUT
// 27-MAR-2014 JBC     Added new functionality. Check First Recharge to Anonymous Accounts
// 08-APR-2014 DLL     Fixed Bug WIG-807: lost focus when undo operation
// 10-APR-2014 RCI & JVV     Allow anonymous card change only if GP ExternalLoyaltyProgram.Mode01 - Cashier.AllowAnonymousCardChange is enabled.
// 14-APR-2014 DLL     Added new functionality: VIP Account
// 16-APR-2014 ICS, MPO & RCI    Fixed Bug WIG-830: Not enough available storage exception.
// 22-APR-2014 DLL     Added new functionality for print specific voucher for the Dealer
// 05-MAY-2014 RMS     Fixed Bug WIG-842: Personalizaci�n de Cuentas: Al intentar personalizar una cuenta el formulario aparece en blanco. 
// 02-JUN-2014 RRR     Fixed Bug WIG-959: PIN required in anonymous accounts when general param disables it
// 07-JUL-2014 DRV     Added new chips purchase with cash out functionality
// 08-JUL-2014 JCO     Added functionality to show comments when chk_show_comments is checked.
// 21-AUG-2014 DRV     Fixed Bug WIG-1193: No one should have to select a table for a chip sale register
// 22-AUG-2014 DRV     Fixed Bug WIG-1204: Button Undo last chips purchase must not be shown if integrated chip and credit operation is enabled
// 23-JUL-2014 RRR & RCI  Fixed Bug WIG-1220: Buttons get enabled without swipping a card.
// 18-SEP-2014 DLL     Added new movement Cash Advance
// 09-OCT-2014 JBC     Fixed Bug WIG-1458: CashAdvance button activated when cashdesk is closed and swipe card.
// 13-OCT-2014 SGB     Fixed Bug WIG-1468: Button redeem ticket not disable if cash is closing
// 28-OCT-2014 SMN     Added new functionality: BLOCK_REASON field can have more than one block reason.
// 15-DEC-2014 OPC     Fixed Bug WIG-1831: Added new functionality to show always the comments when the check "Show only comments" was checked
// 04-MAR-2015 OPC     Fixed Bug WIG-2134: After show comments, buttons must be enabled.
// 06-MAR-2015 RCI     Fixed Bug WIG-2142: GamingTables PlayerTracking doesn't have to condition the Undo Chips Sale operation.
// 21-MAY-2015 RCI     Fixed Bug WIG-2375: Don't allow to unlock accounts if they are locked by External PLD.
// 28-APR-2015 FJC     Add Button for WinLossStatement Request
// 17-JUL-2015 JPJ     Fixed Bug WIG-2605: Card replacement button active when cashier is closed.
// 05-AUG-2015 YNM     TFS-3109: Annonymous account min allowed cash in amount recharge
// 10-SEP-2015 YNM     Fixed BUg 4170: External PDL. Annonymous and personalized account allow reacharges 
// 06-NOV-2015 AMF     Backlog Item 6145:BonoPLUS: Cr�dito reservado
// 11-DIC-2015 JCA & FJC  BackLog Item 7311: Bonoplay=> Mixed Mode.
// 14-DIC-2015 FJC     Fixed Bug 7584:MonoPlay: Disable BonoPlay button for anonymous accounts
// 14-DEC-2015 FAV     Fixed Bug 7599: Errors with permissions for card replacement free
// 22-FEB-2016 LTC     Product Backlog Item: 7440 Disputes and Handpays division - New Button functionality
// 14-MAR-2016 FOS     Fixed Bug 9357: New design corrections
// 21-MAR-2016 JRC     PBI: 1792 Multiple Buckets: Desactivar Acumulaci�n de Buckets si SPACE is enabled
// 28-MAR-2016 LTC     Bug 10736:Cashier - Classic Cashless - Menu Avoid Last Operation Wrong
// 13-APR-2016 DHA     Product Backlog Item 9950: added chips operation (sales and purchases)
// 19-MAY-2016 ETP     Bug 13384: Multicurrency - Error si los permisos estan deshabilitados.
// 23-MAY-2016 FAV     PBI 13491: Multicurrency, added functionality for Cashless
// 01-JUN-2016 FAV     Fixed Bug 14036: The "Undo Cash Advance" Form doesn't show the confirmation popup
// 01-JUN-2016 ETP     Fixed Bug 14039 - Cambio de divisa: no se lleva a t�rmino la operaci�n al no aplicar las comisiones con el permiso deshabilitado
// 11-AUG-2016 ETP     Fixed Bug 16672 - Error when voiding pinpad operations.
// 16-SEP-2016 FOS     Fixed Bug 17799 - Error hide chips buttons when playertracking param is disabled
// 19-SEP-2016 FGB     PBI 15985: Exped System: Cashier: Check if the payment/sale can be done
// 22-SEP-2016 LTC     Product Backlog Item 17461: Gamingtables 46 - Cash desk draw for gamblign tables: Chips purchase
// 26-SEP-2016 ETP     Fixed Bug 18021: PinPad - Cashier - Canceling PinPad operations rest from cash balance.
// 30-SEP-2016 ETP     Bug 18003: Cashier: los vouchers de recarga no reflejan los cambios solicitados por Televisa
// 11-OCT-2016 FGB     Bug 18904: Buckets - Cashier: Error if there are no buckets enabled.
// 21-OCT-2016 ETP     Bug 19314: PinPad Card Payment Refactorized
// 26-OCT-2016 ETP     Fixed Bug 19314 Pin Pad recharge Wigos Rollback + Pinpad + Wigos Commit
// 07-NOV-2016 ATB     Bug 20155:Televisa: After a cash reload the voucher do not follow the Televisa's requested order
// 08-NOV-2016 ETP     Fixed Bug Bug 20035: Exception out of memory when pinpad is used.
// 15/NOV/2016 ETP     PBI 20630: Mico2 Belice - Handpay payment.
// 16/JAN/2017 ETP     Fixed Bug 22747: Multivisa: needed valid visid.
// 23-JAN-2017 JML     Fixed Bug 21868: Allows to perform Buy-In exceeding the number configured in the external PLD.
// 24-JAN-2017 JML     Fixed Bug 23372: Can not do recharge in an environment without anti-money laundering.
// 24-JAN-2017 FJC     PBI 22241:Sorteo de m�quina - Participaci�n - US 1 - Participaci�n
// 15-MAR-2017 FAV     PBI 25262: Exped - Payment authorisation
// 22-MAR-2017 ATB     PBI 25262: Exped - Payment authorisation
// 22-MAR-2017 DPC     PBI 25788:CreditLine - Get a Marker
// 23-MAR-2017 ATB     PBI 25262: Exped - Payment authorisation
// 24-MAR-2017 ETP     WIGOS-110: Creditlines - Paybacks
// 24-MAR-2017 ATB     PBI 25262: Exped - Payment authorisation
// 22-MAR-2017 DPC     PBI 25788:CreditLine - Get a Marker
// 24-MAR-2017 ETP     WIGOS-110: Creditlines - Paybacks
// 25-MAY-2017 DHA     Bug 27679:[Ticket #4870] Duplicate recharges
// 04-MAY-2017 DPC     WIGOS-1574: Junkets - Flyers - New field "Text in Promotional Voucher" - Change voucher
// 02-JUN-2017 DHA     Bug 27679:[Ticket #4870] Duplicate recharges
// 21-MAY-2017 ETP     Fixed Bug WIGOS-2997: Added voucher data.
// 22-JUN-2017 FJC     WIGOS-3142 WS2S - Set / Retrieve Reserved Credit on the Cashier
// 08-MAY-2017 MS      PBI 27045: Junket Vouchers
// 23-MAY-2017 DHA     PBI 27487: WIGOS-83 MES21 - Sales selection
// 06-JUN-2017 DHA     PBI 27760:WIGOS-257: MES21 - Gaming table chair assignment with card selection
// 10-JUL-2017 DHA     PBI 28610:WIGOS-3458 - Show Customer Image in a Pop up - Accounts
// 14-JUL-2017 DPC     PBI 16148: WebService FBM - Adaptar l�gica a los m�todos de FBM
// 26-JUL-2017 DPC     WIGOS-3990: Creditline: two messages appear when selling chips using expired credit line
// 28-AUG-2017 EOR     Bug 29474: WIGOS-4701 [Ticket #8360] Culiac�n - Pago en disputa al realizar una recarga
// 05-SEP-2017 DHA     PBI 28630:WIGOS-3324 MES22 - Gaming table chair assignment with linked gaming tables and card selection
// 03-OCT-2017 DHA     PBI 30017:WIGOS-4056 Payment threshold registration - Player registration
// 05-OCT-2017 RAB     PBI 30018:WIGOS-4036 Payment threshold authorization - Payment authorisation
// 09-OCT-2017 JML     PBI 30022:WIGOS-4052 Payment threshold registration - Recharge with Check payment
// 10-OCT-2017 JML     PBI 30023:WIGOS-4068 Payment threshold registration - Transaction information
// 09-OCT-2017 JML    PBI 30022:WIGOS-4052 Payment threshold registration - Recharge with Check payment
// 19-OCT-2017 JML     Fixed Bug 30321:WIGOS-5924 [Ticket #9450] F�rmula datos tarjeta bancaria (entregado-faltante) - Resumen de sesi�n de caja
// 08-NOV-2017 AMF     PBI 30550:[WIGOS-5561] Codere EGM Draw
// 13-NOV-2017 DHA     Bug 30775:WIGOS-6110 [Ticket #9672] MTY I - Plaza Real - PinPad con error y autorizadas
// 04-MAY-2018 AGS     Bug 32552:WIGOS-8179 [Ticket #12450] Reporte de Compra Venta de Fichas Duplica ventas en Caja V03.06.0035
// 08-MAY-2018 LQB     Bug 32615:WIGOS-10731 Missing warning message in the Cashier for cards not belonging to the site
// 23-JUN-2018 DPC     Bug 33001:[WIGOS-12663]: Account Last gaming sessions doesn't show the bill in
//--------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Drawing.Printing;
using WSI.Common;
using WSI.Cashier.TITO;
using WSI.Common.TITO;
using System.Collections;
using System.Threading;
using WSI.Cashier.MVC.Controller;
using WSI.Cashier.MVC.Controller.PlayerEditDetails;
using WSI.Cashier.MVC.View;
using WSI.Cashier.MVC.View.PlayerEditDetails;
using WSI.Common.Entrances;
using WSI.Common.PinPad;
using WSI.Common.ExternalPaymentAndSaleValidation;
using WSI.Common.CreditLines;
using System.Xml;
using WSI.Cashier.JunketsCashier;
using WSI.Common.Junkets;
using WSI.Common.TerminalDraw;

namespace WSI.Cashier
{
  public partial class uc_card : UserControl
  {

    #region ENUMS

    public enum CardStyle
    {
      DEFAULT = 0,
    }

    #endregion

    #region Class Atributes

    private static frm_yesno form_yes_no;
    private CardData m_card_data;
    Int64 m_current_card_id;
    Currency m_current_card_balance;
    frm_amount_input form_credit;
    frm_last_movements form_last_plays = new frm_last_movements();
    frm_search_players form_search_players = new frm_search_players();
    frm_available_draws form_available_draws = new frm_available_draws();
    String visible_track_number;
    String m_current_track_number;
    Int32 tick_count_tracknumber_read = CashierBusinessLogic.DURATION_BUTTONS_ENABLED_AFTER_OPERATION + 1;
    Boolean is_logged_in;
    Boolean can_log_off;
    Boolean is_locked;
    AccountWatcher account_watcher;
    CardPrinter card_printer;
    Boolean first_time = true;
    Currency m_deposit;
    Boolean m_promotion_enabled = false;
    Boolean m_promotion_convertible = false;
    private frm_container m_parent;
    Boolean m_balance_mismatch;
    Boolean m_is_new_card;
    Boolean m_jackpot_notified;
    Boolean m_is_gaming_tables_enabled;
    Boolean m_is_vip_account;
    Boolean m_integrated_chip_operation = false;
    Boolean m_pending_check_comments;
    Boolean m_must_show_comments;
    // Account loaded from physic card (true) or account obtained searching (false)
    Boolean m_swipe_card;
    Boolean m_is_chips_sale_enabled;
    Boolean m_is_cash_advance_enabled;
    Color m_line_separator_color;

    private Boolean m_exist_photo = false;

    #endregion

    #region Propierties

    public Color LineSeparatorColor
    {
      get
      {
        return m_line_separator_color;
      }
      set
      {
        m_line_separator_color = value;
      }
    }

    public IDScanner.IDScanner IDScanner { get; set; }

    #endregion

    #region Constructor

    /// <summary>
    /// Constructor
    /// </summary>
    public uc_card()
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[UC LOAD] uc_card", Log.Type.Message);

      InitializeComponent();

      form_yes_no = new frm_yesno();
      form_yes_no.Opacity = 0.6;

      card_printer = new CardPrinter();

      EventLastAction.AddEventLastAction(this.Controls);

      m_swipe_card = false;
      m_pending_check_comments = false;
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Initialize control resources. (NLS string, images, etc.)
    /// </summary>
    public void InitializeControlResources()
    {
      // NLS Strings
      //      - Title
      //      - Account 
      //      - Last Game Session
      //      - Points
      //      - Buttons

      //      - Title
      //lbl_cards_title.Text = Resource.String("STR_UC_CARD_TITLE");

      //      - Account 
      lbl_account_id_name.Text = Resource.String("STR_FORMAT_GENERAL_NAME_ACCOUNT") + ":";
      lbl_card_in_use.Text = Resource.String("STR_UC_CARD_IN_USE");
      lbl_vip_account.Text = Resource.String("STR_VIP_ACCOUNT");
      // DDM 06-AUG-2012
      lbl_id_title.Text = Resource.String("STR_FRM_PLAYER_EDIT_TYPE_DOCUMENT") + ":";
      lbl_birth_title.Text = Resource.String("STR_UC_PLAYER_TRACK_BIRTH") + ":";
      lbl_name.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_NAME") + ":";
      lbl_trackdata.Text = Resource.String("STR_FORMAT_GENERAL_NAME_CARD") + ":";
      lbl_terminal.Text = Resource.String("STR_FRM_CONTAINER_EVENTS_DATAGRID_HEADER_TERMINAL") + ":";

      //      - Last Game Session
      lbl_session_initial_balance.Text = Resource.String("STR_UC_CARD_SESSION_INFO_BALANCE_INITIAL") + ":";
      lbl_session_plays.Text = Resource.String("STR_UC_CARD_SESSION_INFO_PLAYS") + ":";
      lbl_session_won_plays.Text = Resource.String("STR_UC_CARD_SESSION_INFO_WON_PLAYS") + ":";
      lbl_session_final_balance.Text = Resource.String("STR_UC_CARD_SESSION_INFO_BALANCE_FINAL") + ":";

      lbl_last_activity.Text = Resource.String("STR_UC_CARD_SESSION_INFO_LAST_ACTIVITY");

      lbl_deposit_name.Text = WSI.Common.Misc.GetCardPlayerConceptName();

      //      - Points
      gb_gifts.HeaderText = " " + Resource.String("STR_FRM_ACCOUNT_POINTS_002") + " ";                // "Gift List"
      gb_loyalty_program.HeaderText = " " + Resource.String("STR_UC_CARD_GB_LOYALTY_PROGRAM") + " ";  // " Loyalty Program "
      lbl_points_balance_title.Text = Resource.String("STR_UC_CARD_POINTS_BALANCE_TITLE") + ":";      // "Balance"
      lbl_points_balance_suffix.Text = Resource.String("STR_UC_CARD_POINTS_BALANCE_SUFFIX");          // "puntos"
      lbl_level_title.Text = Resource.String("STR_UC_CARD_POINTS_LEVEL_TITLE") + ":";                 // "Nivel"

      //   - Buttons
      btn_new_card.Text = Resource.String("STR_UC_CARD_BTN_NEW_CARD");
      btn_account_edit.Text = Resource.String("STR_UC_CARD_BTN_ACCOUNT_EDIT");
      btn_card_block.Text = Resource.String("STR_UC_CARD_BLOCK_ACTION_LOCK");
      btn_draw.Text = Resource.String("STR_UC_CARD_BTN_DRAW");
      btn_session_log_off.Text = Resource.String("STR_UC_CARD_BTN_SESSION_LOG_OFF");
      btn_credit_add.Text = Resource.String("STR_UC_CARD_BTN_CREDIT_ADD");
      btn_credit_redeem.Text = Resource.String("STR_ACCOUNT_REDEEM_001");
      btn_lasts_movements.Text = Resource.String("STR_UC_CARD_BTN_LAST_MOVEMENTS");
      btn_print_card.Text = Resource.String("STR_UC_CARD_BTN_PRINT_CARD");
      btn_handpays.Text = Resource.String("STR_UC_CARD_BTN_HAND_PAY");
      btn_handpay_mico2.Text = Resource.String("STR_UC_CARD_BTN_HAND_PAY");
      btn_gifts.Text = Resource.String("STR_UC_CARD_BTN_GIFTS");                                    // "Regalos"
      btn_promotion.Text = Resource.String("STR_UC_CARD_BTN_PROMOTION_DELETE");
      btn_vouchers_space.Text = Resource.String("STR_VOUCHER_VOUCHER_BUTTON");
      this.btn_recycle_card.Image = Images.Get32x32(Images.CashierImage.Recycle);
      this.btn_new_card.Image = Images.Get32x32(Images.CashierImage.SwipeCard);
      this.btn_recycle_card.Text = Resource.String("STR_RECYCLE_CARD_TITLE");
      this.btn_cancel_promo.Text = Resource.String("STR_PROMO_CANCEL");
      this.btn_transfer_credit.Text = Resource.String("STR_INPUT_TARGET_CARD_BUTTON");

      if (Misc.IsGameGatewayEnabled())
      {
      this.btn_reserved.Text = GeneralParam.GetString("GameGateway", "Name", "BonoPlay");
      }
      else
      {
        if (Misc.IsWS2SFBMEnabled())
        {
          this.btn_reserved.Text = GeneralParam.GetString("WS2S", "TextReserved", String.Empty);
        }
      }
      
      // LTC 22-FEB-2019
      btn_disputes.Text = Resource.String("STR_FRM_TITO_HANDPAYS_OTHERS");

      if (m_is_gaming_tables_enabled)
      {
        btn_undo_operation.Text = Resource.String("STR_UNDO_LAST_OPERATION_GENERIC");
      }
      else
      {
        btn_undo_operation.Text = Resource.String("STR_UNDO_LAST_OPERATION");
      }
      this.gb_chips.HeaderText = Resource.String("STR_UC_CARD_GB_CHIPS");
      this.btn_buy_chips.Text = Resource.String("STR_UC_CARD_BTN_BUY_CHIPS");
      this.btn_sell_chips.Text = Resource.String("STR_UC_CARD_BTN_SELL_CHIPS");
      this.btn_chips_sale_register.Text = Resource.String("STR_UC_CARD_BTN_CHIPS_SALE_REGISTER");
      this.btn_cash_advance.Text = Resource.String("STR_VOUCHER_CASH_ADVANCE");
      this.btn_win_loss_statement_request.Text = Resource.String("STR_WIN_LOSS_STATEMENT_PLAYER_EDIT_REQUEST");

      //  - Datagrids

      //  - Other
      gb_session.HeaderText = " " + Resource.String("STR_UC_CARD_GROUP_BOX_SESSION") + " ";

      btn_multiple_buckets.Text = Resource.String("STR_BUCKET_BUTTON");

      this.uc_card_reader1.InitializeControlResources();
      this.uc_card_balance.InitializeControlResources();
      this.form_credit.InitializeControlResources();
      this.form_last_plays.InitializeControlResources();
      this.form_search_players.InitializeControlResources();
      this.form_available_draws.InitializeControlResources();
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Generate a random PIN
    // 
    //  PARAMS:
    //      - INPUT:
    //          CardData
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:

    public void GenerateNewPIN(CardData CardData)
    {
      CashierBusinessLogic.DB_UpdateCardPin(CardData, "", CardData.PlayerTracking.Pin, true);
    }

    private void RestoreParentFocus()
    {
      this.ParentForm.Focus();
      this.ParentForm.BringToFront();
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Undo the last currency exchange
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    private void UndoLastCurrencyExchange()
    {
      String _str_error;
      String _str_message;
      String _str_generic_error = String.Empty;
      DialogResult _dlg_rc = DialogResult.None;
      CashierSessionInfo _cashier_session_info;
      AccountOperations.Operation _account_operation;
      CurrencyIsoType _iso_type;
      Decimal _iso_amount;
      OperationUndo.UndoError _undo_error;
      ArrayList _voucher_list;
      CardData _card_data = account_watcher.Card;

      _str_generic_error = Resource.String("STR_CURRENCY_EXCHANGE_GENERIC_ERROR_EXCHANGE");

      try
      {
        if (!Misc.IsMultiCurrencyExchangeEnabled())
        {
          return;
        }

        form_yes_no.Show(m_parent);

        if (_card_data.AccountId > 0)
        {
          if (!IsCustomerRegisteredInReception(_card_data))
          {
            return;
          }

          _cashier_session_info = Cashier.CashierSessionInfo();

          // 1. Get last reversible operation
          if (!OperationUndo.GetLastReversibleOperation(OperationCode.CURRENCY_EXCHANGE_CHANGE, _card_data, _cashier_session_info.CashierSessionId, out _account_operation,
                                                out _iso_type, out _iso_amount, out _undo_error))
          {
            _str_error = OperationUndo.GetErrorMessage(_undo_error);

            frm_message.Show(_str_error, Resource.String("STR_APP_GEN_MSG_ERROR"),
                             MessageBoxButtons.OK, MessageBoxIcon.Error, m_parent);

            return;
          }

          // 2. Ask confirmation to user
          Currency _currency = new Currency();
          _currency = _iso_amount;
          _currency.CurrencyIsoCode = _iso_type.IsoCode;

          _str_message = Resource.String("STR_UNDO_LAST_CURRENCY_EXCHANGE", Currency.Format(_iso_amount, _iso_type.IsoCode));
          _str_message = _str_message.Replace("\\r\\n", "\r\n");
          _dlg_rc = frm_message.Show(_str_message,
                                      Resource.String("STR_CURRENCY_EXCHANGE_UNDO_LAST_EXCHANGE"),
                                      MessageBoxButtons.YesNo, Images.CashierImage.Question, Cashier.MainForm);


          if (_dlg_rc != DialogResult.OK)
          {
            return;
          }

          // 3. Undo operation
          OperationUndo.InputUndoOperation _params = new OperationUndo.InputUndoOperation();
          _params.CodeOperation = OperationCode.CURRENCY_EXCHANGE_CHANGE;
          _params.CardData = _card_data;
          _params.OperationIdForUndo = _account_operation.OperationId;
          _params.CashierSessionInfo = _cashier_session_info;

          if (!OperationUndo.UndoOperation(_params, out _voucher_list))
          {
            frm_message.Show(_str_generic_error, Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, m_parent);

            return;
          }

          // Print vouchers
          VoucherPrint.Print(_voucher_list);
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        frm_message.Show(_str_generic_error, Resource.String("STR_APP_GEN_MSG_ERROR"),
                MessageBoxButtons.OK, Images.CashierImage.Error, m_parent);
      }
      finally
      {
        form_yes_no.Hide();
        this.RestoreParentFocus();
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Undo the last currency devolution
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    private void UndoLastDevolution()
    {
      String _str_error;
      String _str_message;
      String _str_generic_error = String.Empty;
      DialogResult _dlg_rc = DialogResult.None;
      CashierSessionInfo _cashier_session_info;
      AccountOperations.Operation _account_operation;
      CurrencyIsoType _iso_type;
      Decimal _iso_amount;
      OperationUndo.UndoError _undo_error;
      ArrayList _voucher_list;
      CardData _card_data = account_watcher.Card;

      _str_generic_error = Resource.String("STR_CURRENCY_EXCHANGE_GENERIC_ERROR_DEVOLUTION");

      try
      {
        if (!Misc.IsMultiCurrencyExchangeEnabled())
        {
          return;
        }

        form_yes_no.Show(m_parent);

        if (_card_data.AccountId > 0)
        {
          if (!IsCustomerRegisteredInReception(_card_data))
          {
            return;
          }

          _cashier_session_info = Cashier.CashierSessionInfo();

          // 1. Get last reversible operation
          if (!OperationUndo.GetLastReversibleOperation(OperationCode.CURRENCY_EXCHANGE_DEVOLUTION, _card_data, _cashier_session_info.CashierSessionId, out _account_operation,
                                                out _iso_type, out _iso_amount, out _undo_error))
          {
            _str_error = OperationUndo.GetErrorMessage(_undo_error);

            frm_message.Show(_str_error, Resource.String("STR_APP_GEN_MSG_ERROR"),
                             MessageBoxButtons.OK, MessageBoxIcon.Error, m_parent);

            return;
          }

          // 2. Ask confirmation to user
          _str_message = Resource.String("STR_UNDO_LAST_CURRENCY_DEVOLUTION", Currency.Format(_iso_amount, _iso_type.IsoCode));
          _str_message = _str_message.Replace("\\r\\n", "\r\n");
          _dlg_rc = frm_message.Show(_str_message,
                                      Resource.String("STR_CURRENCY_EXCHANGE_UNDO_LAST_DEVOLUTION"),
                                      MessageBoxButtons.YesNo, Images.CashierImage.Question, Cashier.MainForm);
          if (_dlg_rc != DialogResult.OK)
          {
            return;
          }

          // 3. Undo operation
          OperationUndo.InputUndoOperation _params = new OperationUndo.InputUndoOperation();
          _params.CodeOperation = OperationCode.CURRENCY_EXCHANGE_DEVOLUTION;
          _params.CardData = _card_data;
          _params.OperationIdForUndo = _account_operation.OperationId;
          _params.CashierSessionInfo = _cashier_session_info;

          if (!OperationUndo.UndoOperation(_params, out _voucher_list))
          {
            frm_message.Show(_str_generic_error, Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, m_parent);

            return;
          }

          // Print vouchers
          VoucherPrint.Print(_voucher_list);

        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        frm_message.Show(_str_generic_error, Resource.String("STR_APP_GEN_MSG_ERROR"),
                MessageBoxButtons.OK, Images.CashierImage.Error, m_parent);
      }
      finally
      {
        form_yes_no.Hide();
        this.RestoreParentFocus();
      }

    }

    //------------------------------------------------------------------------------
    // PURPOSE: Undo last cash advance (card or check) selected in the list
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    private void UndoCardCash_CashAdvanceList()
    {
      CardData _card_data = account_watcher.Card;
      List<CashierMovementsInfo> _cashier_movements_info_list;
      try
      {
        if (!IsCustomerRegisteredInReception(_card_data))
        {
          return;
        }

        CashAdvance _cash_advance = new CashAdvance();
        _cash_advance.GetCashAdvanceListForUndo(_card_data, out _cashier_movements_info_list);

        frm_cash_advance_undo _frm = new frm_cash_advance_undo();
        _frm.Show(_card_data, _cashier_movements_info_list, m_parent);

      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Checks if the customer has been registered in reception
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    private Boolean IsCustomerRegisteredInReception(CardData CardData)
    {
      // Checks if the customer doesn't visit the casino in this working date
      if (!GeneralParam.GetBoolean("MultiCurrencyExchange", "ReceptionRequired"))
      {
        return true;
      }

      if (!Entrance.CustomerHasVisitedToday(CardData.AccountId))
      {
        frm_message.Show(Resource.String("STR_RECEPTION_CUSTOMER_NOT_REGISTERED"), Resource.String("STR_APP_GEN_MSG_WARNING"),
                                           MessageBoxButtons.OK, MessageBoxIcon.Error, m_parent);
        return false;
      }

      return true;
    }
    #endregion

    /// <summary>
    /// Initialize controls
    /// </summary>
    public void InitControls(frm_container Parent)
    {
      Boolean _chip_sale_register_enabled;

      CashierStyle.ControlStyle.InitStyle(this, CardStyle.DEFAULT);

      m_parent = Parent;

      if (!(GeneralParam.GetInt32("PlayerTracking.ExternalLoyaltyProgram", "Mode", 0) == 0)) //Buckets SPACE
      {
        btn_multiple_buckets.Visible = false;
      }

      m_is_chips_sale_enabled = GeneralParam.GetBoolean("GamingTables", "Cashier.ChipsSaleEnabled");
      _chip_sale_register_enabled = GeneralParam.GetBoolean("GamingTables", "Cashier.RegisterChipSaleAtTableEnabled");

      m_is_cash_advance_enabled = CurrencyExchange.IsCashAdvanceEnabled();

      pb_user.Image = Resources.ResourceImages.anonymous_user;
      pb_blocked.Image = WSI.Cashier.Images.Get(Images.CashierImage.Locked);

      lbl_terminal_value.Text = "";

      ClearCardData();
      // RCI & AJQ 18-FEB-2011: Expire the timer: Disable buttons.
      tick_count_tracknumber_read = CashierBusinessLogic.DURATION_BUTTONS_ENABLED_AFTER_OPERATION + 1;

      EnableDisableButtons(false);

      // DLL 10-JAN-2014: Set if gaming tables are enabled
      m_is_gaming_tables_enabled = GamingTableBusinessLogic.IsGamingTablesEnabled();

      lbl_happy_birthday.ForeColor = Color.White;
      lbl_happy_birthday.BackColor = CashierStyle.Colors.m_colors_dic[CashierStyle.Colors.ENUM_COLORS.HEX_00A651];

      lbl_balance_mismatch.ForeColor = Color.White;
      lbl_balance_mismatch.BackColor = Color.Red;

      btn_handpay_mico2.Visible = GeneralParam.GetBoolean("Cashier.Handpays", "AllowAnonymousPayment", false);

      if (first_time)
      {
        first_time = false;

        form_credit = new frm_amount_input(this.CheckPermissionsToRedeem, this.CheckAntiMoneyLaunderingToRedeem);

        account_watcher = new AccountWatcher();
        account_watcher.Start();
        account_watcher.AccountChangedEvent += new AccountWatcher.AccountChangedHandler(account_watcher_AccountChangedEvent);

        uc_card_reader1.OnTrackNumberReadEvent += new uc_card_reader.TrackNumberReadEventHandler(uc_card_reader1_OnTrackNumberReadEvent);
        uc_card_reader1.OnTrackNumberReadingEvent += new uc_card_reader.TrackNumberReadingHandler(uc_card_reader1_OnTrackNumberReadingEvent);

        btn_credit_add.Click += new EventHandler(btn_button_clicked);
        btn_recycle_card.Click += new EventHandler(btn_button_clicked);
        btn_new_card.Click += new EventHandler(btn_button_clicked);
        btn_card_block.Click += new EventHandler(btn_button_clicked);
        btn_promotion.Click += new EventHandler(btn_button_clicked);
        btn_draw.Click += new EventHandler(btn_button_clicked);
        btn_account_edit.Click += new EventHandler(btn_button_clicked);
        btn_session_log_off.Click += new EventHandler(btn_button_clicked);
        btn_credit_redeem.Click += new EventHandler(btn_button_clicked);
        btn_lasts_movements.Click += new EventHandler(btn_button_clicked);
        btn_print_card.Click += new EventHandler(btn_button_clicked);
        btn_handpays.Click += new EventHandler(btn_button_clicked);
        btn_gifts.Click += new EventHandler(btn_button_clicked);
        btn_player_browse.Click += new EventHandler(btn_button_clicked);
        btn_cancel_promo.Click += new EventHandler(btn_button_clicked);
        btn_sell_chips.Click += new EventHandler(btn_button_clicked);
        btn_buy_chips.Click += new EventHandler(btn_button_clicked);
        btn_transfer_credit.Click += new EventHandler(btn_button_clicked);
        btn_undo_operation.Click += new EventHandler(btn_button_clicked);
        btn_chips_sale_register.Click += new EventHandler(btn_button_clicked);
        btn_cash_advance.Click += new EventHandler(btn_button_clicked);
        btn_win_loss_statement_request.Click += new EventHandler(btn_button_clicked);
        btn_reserved.Click += new EventHandler(btn_button_clicked);

        pb_user.Click += new EventHandler(btn_button_clicked);
        // LTC 22-FEB-2016
        btn_disputes.Click += new EventHandler(btn_button_clicked);
        btn_vouchers_space.Click += new System.EventHandler(this.btn_vouchers_space_Click);


        InitializeControlResources();
      }

      if (m_is_gaming_tables_enabled)
      {
        this.gb_chips.Visible = true;
        this.btn_buy_chips.Visible = true;
        this.btn_buy_chips.Location = new Point(10, 45);
        if (m_is_chips_sale_enabled)
        {
          this.btn_sell_chips.Visible = true;
          this.btn_sell_chips.Location = new Point(162, 45);
        }
        else
        {
          this.btn_sell_chips.Visible = false;
        }

        if (_chip_sale_register_enabled)
        {
          this.btn_chips_sale_register.Visible = true;
          if (m_is_chips_sale_enabled)
          {
            this.btn_buy_chips.Location = new Point(10, 45);
            this.btn_sell_chips.Location = new Point(160, 45);
            this.btn_chips_sale_register.Location = new Point(311, 45);
            this.pb_chips.Visible = false;
          }
          else
          {
            this.btn_chips_sale_register.Location = new Point(162, 45);
            this.btn_buy_chips.Location = new Point(10, 45);
          }
        }
        else
        {
          if (!m_is_chips_sale_enabled)
          {
            this.btn_buy_chips.Location = new Point(160, 45);
          }
          else
          {
            this.btn_buy_chips.Location = new Point(83, 45);
            this.btn_sell_chips.Location = new Point(225, 45);
          }
        }
      }
    }

    void account_watcher_AccountChangedEvent()
    {
      if (this.InvokeRequired)
      {
        Delegate _method;

        _method = null;

        try
        {
          _method = new AccountWatcher.AccountChangedHandler(account_watcher_AccountChangedEvent);
          if (_method != null)
          {
            this.BeginInvoke(_method);
          }
          else
          {
            Log.Error("Account changed: nobody taking care of the event, the method is null.");
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }
        return;
      }

      RefreshData();
    }

    private void LockControls(bool p)
    {
      throw new Exception("The method or operation is not implemented.");
    }

    #region Buttons
    //------------------------------------------------------------------------------
    // PURPOSE: Creates a new Card Data Account
    // 
    //  PARAMS:
    //      - INPUT:
    //          CardData
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    private void NewAccount(CardData CardData)
    {
      Boolean _card_paid;
      String _msgbox_text;
      CardData _aux_card = new CardData();
      String _error_str;

      // MBF 30-NOV-2009 - Check BEFORE asking if the user wants to create
      // Check provided trackdata is not already associated to a MB account
      if (CashierBusinessLogic.DB_IsMBCardDefined(CardData.TrackData))
      {
        // MsgBox: Card already assigned to a Mobile Bank account.
        // frm_message.Show(Resource.String("STR_UC_CARD_USER_MSG_CARD_LINKED_TO_MB_ACCT"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this.ParentForm);

        // uc_card_reader1.Focus();

        // We are going to change the form shown, expire the timer to disable buttons in case of return to this form.
        tick_count_tracknumber_read = CashierBusinessLogic.DURATION_BUTTONS_ENABLED_AFTER_OPERATION + 1;

        m_parent.SwitchToMobileBanck(CardData.TrackData);
        ClearCardData();
        uc_card_reader1.ClearTrackNumber();

        return;
      }

      // Card does not exist: request confirmation to create a new one.
      _aux_card.TrackData = CardData.TrackData;

      // RCI 18-OCT-2011: Can't create new accounts when cashier is not open.
      if (CashierBusinessLogic.ReadCashierStatus(Cashier.SessionId) != CASHIER_STATUS.OPEN)
      {
        _msgbox_text = Resource.String("STR_UC_CARD_USER_MSG_CARD_NOT_EXIST_AND_CASH_CLOSED", "\n\n" + _aux_card.VisibleTrackdata() + "\n\n");
        frm_message.Show(_msgbox_text, Resource.String("STR_UC_CARD_USER_MSG_CARD_NOT_EXIST_TITLE"), MessageBoxButtons.OK, MessageBoxIcon.Warning, this.ParentForm);

        uc_card_reader1.Focus();

        return;
      }

      // RRR & RCI 23-JUL-2014: Fixed Bug WIG-1121: Error in account refresh.
      //Reset timer to disable buttons
      tick_count_tracknumber_read = CashierBusinessLogic.DURATION_BUTTONS_ENABLED_AFTER_OPERATION + 1;

      //m_is_new_card = true;
      // JCM 13-APR-2012 Ask the new account type.
      switch (frm_message.ShowAccountSelect(_aux_card.VisibleTrackdata(), this.ParentForm))
      {
        case ACCOUNT_USER_TYPE.ANONYMOUS:

          // Check if current user is an authorized user
          if (!ProfilePermissions.CheckPermissionList(ProfilePermissions.CashierFormFuncionality.AccountCreateAnonymous, ProfilePermissions.TypeOperation.RequestPasswd, this.ParentForm, 0, out _error_str))
          {
            return;
          }

          using (DB_TRX _db_trx = new DB_TRX())
          {
            // Creates CardData into DB
            if (!CashierBusinessLogic.DB_CreateCard(CardData.TrackData, out CardData.AccountId, _db_trx.SqlTransaction))
            {
              Log.Error("LoadCardByTrackNumber. DB_CreateCard: Card already exists or error creating card.");
              // TODO: MsgBox: Card already exist or error creating card.
              //frm_message.Show(Resource.String("STR_FRM_AMOUNT_INPUT_MSG_AMOUNT_EXCEED_PAYABLE"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this);

              uc_card_reader1.Focus();
            }
            else
            {
              if (CashierBusinessLogic.DB_UpdateCardPayAfterCreated(CardData.AccountId, ACCOUNT_USER_TYPE.ANONYMOUS, out _card_paid, _db_trx.SqlTransaction))
              {
                CardData.CardPaid = _card_paid;
              }

              _db_trx.Commit();
              m_is_new_card = false;

              tick_count_tracknumber_read = 0;

              // Load data after create account.
              LoadCardByAccountId(CardData.AccountId);
            }
          }

          return;

        case ACCOUNT_USER_TYPE.PERSONAL:

          // Check if current user is an authorized user
          if (!ProfilePermissions.CheckPermissionList(ProfilePermissions.CashierFormFuncionality.AccountCreatePersonal, ProfilePermissions.TypeOperation.RequestPasswd, this.ParentForm, 0, out _error_str))
          {
            return;
          }

          this.ClearCardData();
          m_current_track_number = CardData.TrackData;
          btn_edit_Click(null, null);

          break;

        case ACCOUNT_USER_TYPE.NONE:  // cancel
        default:
          this.ClearCardData();
          this.InitControls(m_parent);
          break;
      }
    } // NewAccount

    /// <summary>
    /// Check for input card in order to notify user to create card.
    /// Previously called LoadCardByTrackNumber
    /// </summary>
    /// <param name="AccountId"></param>
    private Boolean LoadCardByAccountId(Int64 AccountId, int SiteId = -1)
    {
      String _message;
      string[] _message_params = { "", "", "", "", "", "", "" };

      // Check for no input card
      if (AccountId < 0)
      {
        return false;
      }

      CardData card_data = new CardData();

      visible_track_number = "";

      // Get Card data
      if (!CardData.DB_CardGetAllData(AccountId, card_data, false))
      {
        ClearCardData();

        Log.Error("LoadCardByAccountId. DB_GetCardAllData: Error reading card.");

        return false;
      }

      if (!card_data.IsRecycled)
      {
        if (!CardData.CheckTrackdataSiteId(new ExternalTrackData( card_data.TrackData)))
        {
          ClearCardData();

          Log.Error("LoadCardByAccountId. Card: " + card_data.TrackData + " doesn�t belong to this site.");

          frm_message.Show(Resource.String("STR_CARD_SITE_ID_ERROR"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK,
                           MessageBoxIcon.Warning, this.ParentForm);

          uc_card_reader1.Focus();

          return false;

        }
      }

      if (card_data.AccountId != 0)
      {
        // Keep card read data.

        m_current_card_id = card_data.AccountId;
        m_current_track_number = card_data.TrackData;
        //m_is_new_card = false;

        card_data.IsSiteJackpotWinner = CardData.CheckWinner(AccountId, out card_data.SiteJackpotWinnerInfo);
        account_watcher.SetAccount(card_data);

        // RCI 03-DEC-2010: Show pop-up if player has won the Site Jackpot.
        if (card_data.IsSiteJackpotWinner && !m_jackpot_notified)
        {
          DialogResult _dlg_rc;

          m_jackpot_notified = true;

          _message_params[0] = card_data.SiteJackpotWinnerInfo.jackpot_name;
          _message_params[1] = card_data.SiteJackpotWinnerInfo.amount.ToString("c");
          if (card_data.SiteJackpotWinnerInfo.paid)
          {
            _message_params[2] = Resource.String("STR_SITE_JACKPOT_WINNER_MSG_STATE_PAID");//Pagado
          }
          else
          {
            _message_params[2] = Resource.String("STR_SITE_JACKPOT_WINNER_MSG_STATE_NOT_PAID");//Pendiente de pago
          }

          _message = Resource.String("STR_UC_CARD_USER_MSG_SITE_JACKPOT_WINNER", _message_params);

          _dlg_rc = DialogResult.Cancel;

          if (card_data.SiteJackpotWinnerInfo.paid)
          {
            _message += Resource.String("STR_UC_CARD_USER_MSG_SITE_JACKPOT_PAID");
            _message = _message.Replace("\\r\\n", "\r\n");

            frm_message.Show(_message,
                 Resource.String("STR_FRM_HANDPAYS_026"),
                 MessageBoxButtons.OK,
                 MessageBoxIcon.Information,
                 ParentForm);

            CardData.UpdateJackpotNotifiedStatus(card_data.SiteJackpotWinnerInfo, SITE_JACKPOT_NOTIFICATION_STATUS.Notified);
          }
          else
          {
            if (card_data.IsLocked || card_data.Blocked)
            {
              _message = _message.Replace("\\r\\n", "\r\n");

              frm_message.Show(_message,
                   Resource.String("STR_FRM_HANDPAYS_026"),
                   MessageBoxButtons.OK,
                   MessageBoxIcon.Information,
                   ParentForm);
            }
            else
            {
              _message += Resource.String("STR_UC_CARD_USER_MSG_SITE_JACKPOT_NOT_PAID");
              _message = _message.Replace("\\r\\n", "\r\n");

              _dlg_rc = frm_message.Show(_message,
                   Resource.String("STR_FRM_HANDPAYS_026"),
                   MessageBoxButtons.YesNo,
                   MessageBoxIcon.Question,
                   ParentForm);
            }

          }

          // Card must not be in use
          if (_dlg_rc == DialogResult.OK)
          {
            btn_handpays_Click(null, null);
          }
        }

        // RCI 10-NOV-2010: Show pop-up if recent change of level occurrs.
        if (card_data.PlayerTracking.HolderLevelNotify != 0)
        {
          Int32 _old_level_int;
          String _old_level;
          String _new_level;

          _old_level_int = card_data.PlayerTracking.CardLevel - card_data.PlayerTracking.HolderLevelNotify;
          _old_level = LoyaltyProgram.LevelName(_old_level_int);
          _new_level = LoyaltyProgram.LevelName(card_data.PlayerTracking.CardLevel);

          _message_params[0] = _old_level;
          _message_params[1] = _new_level;
          if (card_data.PlayerTracking.HolderLevelNotify > 0)
          {
            _message = Resource.String("STR_UC_CARD_USER_MSG_HOLDER_LEVEL_RISEN", _message_params);
          }
          else
          {
            _message = Resource.String("STR_UC_CARD_USER_MSG_HOLDER_LEVEL_FALLEN", _message_params);
          }
          _message = _message.Replace("\\r\\n", "\r\n");

          frm_message.Show(_message,
                           Resource.String("STR_APP_GEN_MSG_WARNING"),
                           MessageBoxButtons.OK,
                           MessageBoxIcon.Information,
                           ParentForm);

          CashierBusinessLogic.DB_ResetHolderLevelNotify(card_data);
        }

        m_must_show_comments = GetMustShowComments(card_data);

        if (m_must_show_comments)
        {
          m_pending_check_comments = false;
          btn_edit_Click(null, null);
          uc_card_reader1.Focus();
        }
      }

      return true;
    } // LoadCardByAccountId

    private Boolean GetMustShowComments(CardData Card)
    {
      return !m_is_new_card
             && Card.PlayerTracking.ShowCommentsOnCashier
             && m_pending_check_comments
             && CashierBusinessLogic.ReadCashierStatus(Cashier.SessionId) == CASHIER_STATUS.OPEN;
    }

    /// <summary>
    /// 
    /// </summary>
    private void RefreshData()
    {
      RefreshData(account_watcher.Card);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="CardData"></param>
    private void RefreshData(CardData CardData)
    {
      CardData _card_data;
      DateTime _birth_date;
      DateTime _now_date;
      TimeSpan _tspam;
      Int32 _timeout_seconds;
      Boolean _enabled_buttons_after_operation;
      Dictionary<AccountBlockReason, String> _block_reasons;


      _enabled_buttons_after_operation = false;

      _card_data = CardData;
      m_card_data = _card_data;
      if (_card_data == null)
      {
        _card_data = new CardData();
      }

      if (_card_data.IsRecycled)
      {
        visible_track_number = CardData.RECYCLED_TRACK_DATA;
      }
      else
      {
        visible_track_number = _card_data.VisibleTrackdata();
        m_current_track_number = _card_data.TrackData;
      }

      // lbl_section_1.Text = track_number;
      lbl_track_number.Text = visible_track_number;

      // Display card data
      m_current_card_id = _card_data.AccountId;
      m_current_card_balance = _card_data.CurrentBalance;

      lbl_happy_birthday.Visible = false;

      // Refresh the account's level
      lbl_level.Text = LoyaltyProgram.LevelName(_card_data.PlayerTracking.CardLevel);
      // DDM 08-AUG-2012: If Is anonymous or not have account loaded, is set "Document Type:"
      lbl_id_title.Text = Resource.String("STR_FRM_PLAYER_EDIT_TYPE_DOCUMENT") + ":";

      pb_user.Image = Resources.ResourceImages.anonymous_user;
      lbl_id.Visible = false;
      lbl_birth.Visible = false;

      if (_card_data.PlayerTracking.HolderName == null)
      {
        lbl_holder_name.Text = "";
      }
      else if (_card_data.PlayerTracking.HolderName == "")
      {
        lbl_holder_name.Text = Resource.String("STR_UC_CARD_CARD_HOLDER_ANONYMOUS");
      }
      else
      {
        lbl_id.Visible = true;
        lbl_id_title.Visible = true;
        lbl_birth.Visible = true;
        lbl_birth_title.Visible = true;
        lbl_name.Visible = true;

        lbl_holder_name.Text = _card_data.PlayerTracking.HolderName;

        // RMS 20-AUG-2013 WIG-109
        lbl_id_title.Text = CardData.GetLabelDocument(account_watcher.Card.PlayerTracking.PreferredHolderIdType);
        lbl_id.Text = _card_data.PlayerTracking.PreferredHolderId;

        if (_card_data.PlayerTracking.HolderBirthDate != DateTime.MinValue)
        {
          lbl_birth.Text = _card_data.PlayerTracking.HolderBirthDate.ToShortDateString();

          // MBF 26-OCT-2010 - People born in 29 of Feb have birthdays too.
          if (_card_data.PlayerTracking.HolderBirthDate.Month == 2
            && _card_data.PlayerTracking.HolderBirthDate.Day == 29
            && !DateTime.IsLeapYear(WGDB.Now.Year))
          {
            _birth_date = new DateTime(WGDB.Now.Year, _card_data.PlayerTracking.HolderBirthDate.Month, _card_data.PlayerTracking.HolderBirthDate.AddDays(-1).Day);
          }
          else
          {
            _birth_date = new DateTime(WGDB.Now.Year, _card_data.PlayerTracking.HolderBirthDate.Month, _card_data.PlayerTracking.HolderBirthDate.Day);
          }


          // HAPPY BIRTHDAY SECTION 

          // RCI 19-AUG-2010: Happy Birthday!

          // FJC 11-MAY-2016: IMPORTANT!!!!!! If make changes in this code section (HAPPY BIRTHDAY SECTION)
          // we have to make changes in PlayerEditDetailsBaseController.cs==> function: GetHolderBirthDayNotification()

          lbl_happy_birthday.ForeColor = Color.White;
          _now_date = new DateTime(WGDB.Now.Year, WGDB.Now.Month, WGDB.Now.Day);
          _tspam = _birth_date - _now_date;

          if (_tspam.TotalDays > 0 && _tspam.TotalDays <= GeneralParam.GetInt32("Accounts", "Player.BirthdayWarningDays", 7))
          {
            lbl_happy_birthday.Text = Resource.String("STR_UC_PLAYER_TRACK_BIRTHDAY_IN_NEXT_WEEK");
            lbl_happy_birthday.Visible = true;
          }
          else if (_tspam.TotalDays == 0)
          {
            lbl_happy_birthday.Text = Resource.String("STR_UC_PLAYER_TRACK_HAPPY_BIRTHDAY");
            lbl_happy_birthday.Visible = true;
          }

          //JBC 30-01-2015 BirthDay alarm feature
          if (!Alarm.CheckBirthdayAlarm(_card_data.AccountId, _card_data.PlayerTracking.HolderName,
            CommonCashierInformation.CashierSessionInfo().TerminalId + " - " + CommonCashierInformation.CashierSessionInfo().TerminalName, TerminalTypes.UNKNOWN))
          {

            Log.Error("uc_ticket_create.RefreshData: Error checking Birthday Alarm.");
          }
          // HAPPY BIRTHDAY SECTION 

        }
        else
        {
          lbl_birth.Visible = false;
          lbl_birth_title.Visible = false;
        }

        // RMS 20-AUG-2013 WIG-109
        if (String.IsNullOrEmpty(lbl_id.Text))
        {
          lbl_id.Visible = false;
          lbl_id_title.Visible = false;
        }

        // AJQ 28-MAY-2015, Account Photo        
        AccountPhotoFunctions.SetAccountPhoto(pb_user, _card_data.AccountId, (GENDER)_card_data.PlayerTracking.HolderGender, out m_exist_photo);
      }

      lbl_account_id_value.Text = m_current_card_id.ToString();
      lbl_account_id_value.Visible = (m_current_card_id != 0);

      is_logged_in = _card_data.IsLoggedIn;
      is_locked = _card_data.IsLocked;
      can_log_off = is_logged_in;
      m_is_vip_account = _card_data.PlayerTracking.HolderIsVIP;

      if (can_log_off)
      {
        try
        {
          if (!_card_data.CurrentPlaySession.BalanceMismatch)
          {
            _timeout_seconds = Int32.Parse(WSI.Common.Misc.ReadGeneralParams("Cashier", "MinTimeToCloseSession"));

            if (_card_data.CurrentPlaySession.SecondsSinceLastActivity < _timeout_seconds)
            {
              can_log_off = false;
            }
          }
        }
        catch (Exception ex)
        {
          Log.Exception(ex);
        }
      }

      gb_session.Visible = true;

      // Vip account
      lbl_vip_account.Visible = m_is_vip_account;

      // May be in use or reserved
      lbl_card_in_use.Visible = is_logged_in || is_locked;
      if (is_locked)
      {
        lbl_card_in_use.Text = Resource.String("STR_UC_CARD_LOCKED");
        lbl_card_in_use.ForeColor = System.Drawing.Color.Black;
        lbl_card_in_use.BackColor = System.Drawing.Color.Yellow;
      } // if
      else
      {
        if (is_logged_in)
        {
          lbl_card_in_use.Text = Resource.String("STR_UC_CARD_IN_USE");
          lbl_card_in_use.ForeColor = System.Drawing.Color.White;
          lbl_card_in_use.BackColor = System.Drawing.Color.Red;
        } // if
      } // else
      lbl_terminal_value.Text = _card_data.LoggedInTerminalProvider + " " + _card_data.LoggedInTerminalName;

      // Check for last activity
      if (_card_data.CurrentPlaySession.LastActivity == DateTime.MinValue)
      {
        lbl_last_activity_value.Text = Resource.String("STR_UC_CARD_SESSION_LAST_ACTIVITY_NONE");
      }
      else
      {
        lbl_last_activity_value.Text = Format.CustomFormatDateTime(_card_data.CurrentPlaySession.LastActivity, true);
      }

      lbl_balance_mismatch.Text = Resource.String("STR_UC_CARD_SESSION_BALANCE_MISMATCH");

      m_balance_mismatch = _card_data.CurrentPlaySession.BalanceMismatch;
      if (m_balance_mismatch)
      {
        lbl_balance_mismatch.ForeColor = Color.White;
        lbl_balance_mismatch.BackColor = Color.Red;
        lbl_balance_mismatch.Visible = true;
      }
      else
      {
        lbl_balance_mismatch.Visible = false;
      }

      // Game session

      // Initial Balance
      lbl_session_initial_balance_value.Text = _card_data.CurrentPlaySession.TotalCashIn.ToString();

      // Plays
      lbl_played_count.Text = _card_data.CurrentPlaySession.PlayedCount.ToString();
      lbl_played_amount.Text = _card_data.CurrentPlaySession.TotalPlayedAmount.ToString();

      // Won Plays
      lbl_won_count.Text = _card_data.CurrentPlaySession.WonCount.ToString();
      lbl_won_amount.Text = _card_data.CurrentPlaySession.TotalWonAmount.ToString();

      // Final Balance
      //if (_card_data.IsLoggedIn && _card_data.LoggedInTerminalType == TerminalTypes.WIN)
      //{
      //  _ps_final_balance = _card_data.CurrentPlaySession.InitialBalance
      //                      - _card_data.CurrentPlaySession.PlayedAmount + _card_data.CurrentPlaySession.WonAmount;
      //}
      //else
      //{
      //  _ps_final_balance = _card_data.CurrentPlaySession.FinalBalance;
      //}
      lbl_session_final_balance_value.Text = _card_data.CurrentPlaySession.TotalCashOut.ToString();

      // RCI 22-OCT-2010: Added block reason label.
      if (_card_data.Blocked)
      {
        btn_card_block.Text = Resource.String("STR_UC_CARD_BLOCK_ACTION_UNLOCK");
        lbl_blocked.Text = Resource.String("STR_UC_CARD_BLOCK_STATUS_LOCKED");
        _block_reasons = Accounts.GetBlockReason((Int32)_card_data.BlockReason);
        if (_block_reasons.Count > 1)
        {
          // Show "by several reasons"
          lbl_block_reason.Text = Resource.String("STR_UC_CARD_BLOCK_SEVERAL_REASONS");
        }
        else
        {
          // Show the only one block_reason
          foreach (KeyValuePair<AccountBlockReason, String> _key_value_pair in _block_reasons)
          {
            lbl_block_reason.Text = _key_value_pair.Value;
          }
        }

        pb_blocked.Image = WSI.Cashier.Images.Get(Images.CashierImage.Locked);
      }
      else
      {
        btn_card_block.Text = Resource.String("STR_UC_CARD_BLOCK_ACTION_LOCK");
        lbl_blocked.Text = Resource.String("STR_UC_CARD_BLOCK_STATUS_UNLOCKED");
        lbl_block_reason.Text = String.Empty;
        pb_blocked.Image = WSI.Cashier.Images.Get(Images.CashierImage.Unlocked);
      }

      // Hide the blocked label if not blocked
      lbl_blocked.Visible = _card_data.Blocked;
      lbl_block_reason.Visible = _card_data.Blocked;
      pb_blocked.Visible = _card_data.Blocked;

      // Refresh Card Counters
      uc_card_balance.FillCardCountersBalance(CardData);

      // Points
      lbl_points_balance.Text = _card_data.PlayerTracking.TruncatedPoints.ToString();

      m_deposit = _card_data.Deposit;
      if (_card_data.CardPaid)
      {
        lbl_deposit.Text = _card_data.Deposit.ToString();
      }
      else
      {
        lbl_deposit.Text = "---";
      }

      // RRB 28-JAN-2013
      Int32 _card_refundable;
      // Anonymous
      lbl_deposit_name.Text = Resource.String("STR_UC_CARD_PAID");

      if (_card_data.PlayerTracking.HolderName == "")
      {
        Int32.TryParse(WSI.Common.Misc.ReadGeneralParams("Cashier", "CardRefundable"), out _card_refundable);

        if (_card_refundable != 2)
        {
          lbl_deposit_name.Text = WSI.Common.Misc.GetCardPlayerConceptName();
        }
      }

      m_promotion_convertible = _card_data.PromotionToRedeemableAllowed;
      m_promotion_enabled = _card_data.PromotionRemoveAllowed;

      btn_draw.Text = Resource.String("STR_UC_CARD_BTN_DRAW");  // SLE 9-JUN-2010

      _enabled_buttons_after_operation = (WSI.Common.Misc.ReadGeneralParams("Cashier", "KeepEnabledButtonsAfterOperation") == "1");

      if (m_current_card_id > 0 &&
           (tick_count_tracknumber_read == 0 ||
            (_enabled_buttons_after_operation
             && tick_count_tracknumber_read < CashierBusinessLogic.DURATION_BUTTONS_ENABLED_AFTER_OPERATION)))
      {
        EnableDisableButtons(true);
      }
      else
      {
        // If must show comments, enabled buttons.
        if (m_must_show_comments)
        {
          tick_count_tracknumber_read = 0;
        }
        EnableDisableButtons(m_must_show_comments);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Promotion
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:

    private void btn_promotion_Click(object sender, EventArgs e)
    {
      CardData _card_data;

      _card_data = account_watcher.Card;

      if (_card_data.IsLoggedIn)
      {
        // NO PROMOTION RESET neither REDEEM with CARD IN USE
        frm_message.Show(Resource.String("STR_UC_CARD_IN_USE_ERROR"),
                         Resource.String("STR_APP_GEN_MSG_WARNING"),
                         MessageBoxButtons.OK,
                         MessageBoxIcon.Warning,
                         Cashier.MainForm);
        return;
      }

      if (m_promotion_convertible)
      {
        CashierBusinessLogic.DB_PromotionRedeem(_card_data.AccountId, _card_data.TrackData);
      }
      else
      {
        CashierBusinessLogic.DB_PromotionReset(_card_data.AccountId, _card_data.TrackData);
      }

      // Refresh
      RefreshAccount();
    } // btn_promotion_Click

    //------------------------------------------------------------------------------
    // PURPOSE: Draw
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    private void btn_draw_Click(object sender, EventArgs e)
    {
      String _error_str;

      // Check if current user is an authorized user
      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.DrawTicketsPrint,
                                               ProfilePermissions.TypeOperation.RequestPasswd,
                                               this.ParentForm,
                                               out _error_str))
      {
        return;
      }

      ConditionallyGenerateDrawTickets(OperationCode.DRAW_TICKET);

    } // btn_draw_Click

    public Boolean CheckAntiMoneyLaunderingToRedeem(Int64 AccountId, ENUM_CREDITS_DIRECTION CreditsDirection, Currency Amount,
                                              out ENUM_ANTI_MONEY_LAUNDERING_LEVEL AntiMlLevel, out Boolean ThresholdCrossed)
    {
      DialogResult _dlg_rc;
      AntiMoneyLaunderingInputParameters InputParameters;
      AntiMoneyLaunderingOutputParameters OutputParameters;

      InputParameters = new AntiMoneyLaunderingInputParameters();
      OutputParameters = new AntiMoneyLaunderingOutputParameters();

      InputParameters.m_account_id = AccountId;
      InputParameters.m_credits_direction = CreditsDirection;
      InputParameters.m_amount = Amount;

      AntiMlLevel = ENUM_ANTI_MONEY_LAUNDERING_LEVEL.None;
      ThresholdCrossed = false;

      if (!AntiMoneyLaundering.CheckAntiMoneyLaundering(InputParameters, out OutputParameters))
      {
        return false;
      }

      AntiMlLevel = OutputParameters.m_anti_m_l_level;
      ThresholdCrossed = OutputParameters.m_threshold_crossed;

      if (OutputParameters.m_anti_m_l_level == ENUM_ANTI_MONEY_LAUNDERING_LEVEL.None)
      {
        return true;
      }

      if (OutputParameters.m_player_edit_optional)
      {
        form_credit.Hide();
      }

      _dlg_rc = DialogResult.OK;
      if (OutputParameters.m_sb_msg.Length > 0)
      {
        if (GeneralParam.GetBoolean("AntiMoneyLaundering", "ExternalControl.Enabled"))
        {
          _dlg_rc = frm_message.Show(OutputParameters.m_sb_msg.ToString(), Resource.String("STR_MSG_ANTIMONEYLAUNDERING_WARNING"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation, ParentForm);
          _dlg_rc = DialogResult.No;
        }
        else
        {
          _dlg_rc = frm_message.Show(OutputParameters.m_sb_msg.ToString(), Resource.String("STR_MSG_ANTIMONEYLAUNDERING_WARNING"), MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, ParentForm);
        }
      }

      if (OutputParameters.m_player_edit_optional)
      {
        // Simulate event click to call frm_player_edit.
        if (_dlg_rc == DialogResult.OK)
        {
          btn_edit_Click(null, null);
        }
      }

      if (OutputParameters.m_player_decides_if_continue)
      {
        OutputParameters.m_complete_process = (_dlg_rc == DialogResult.OK);
      }

      return OutputParameters.m_complete_process;
    }

    /// <summary>
    /// Add credit to the Card
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btn_add_Click(object sender, EventArgs e)
    {
      CardData _card_data = new CardData();
      Object operation_data;
      CurrencyExchangeResult _exchange_result;
      ParticipateInCashDeskDraw _participate_in_cash_draw;
      Result _cash_desk_draw_result;
      ArrayList _voucher_list;
      RechargeOutputParameters OutputParameter;
      CashierSessionInfo _cashier_session_info;
      String _error_SalesLimitReached_text;
      String _error_text;

      Boolean _warning_amount_to_pay_zero;

      // ETP: PinPad vars:
      Boolean _return_error;
      Boolean _is_pinpad_recharge;
      frm_tpv _form_tpv;
      PinPadInput _pinpad_input;
      PinPadOutput _pinpad_output;
      Int64 _operation_id;
      ResultGetMarker _result_get_marker;

      // Check if current user is an authorized user
      String error_str;

      JunketsBusinessLogic _junket;

      _junket = null;
      
      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.CardAddCredit, ProfilePermissions.TypeOperation.RequestPasswd, this.ParentForm, out error_str))
      {
        return;
      }

      _pinpad_output = new PinPadOutput();

      // Get Card data
      if (m_current_card_id == 0)
      {
        return;
      }

      if (!m_swipe_card)
      {
        return;
      }

      // ACC & FJC Clear NR2 for get the same random value in this operation
      WSI.Common.Cashier.ClearNR2();

      _card_data = account_watcher.Card;
      if (m_current_card_id != _card_data.AccountId)
      {
        return;
      }

      try
      {
        form_yes_no.Show(this.ParentForm);
        if (!form_credit.Show(Resource.String("STR_UC_CARD_BTN_CREDIT_ADD"),
                              VoucherTypes.CardAdd,
                              _card_data,
                              form_yes_no,
                              out _participate_in_cash_draw,
                              out _exchange_result,
                              out operation_data))
        {
          return;
        }
      }
      finally
      {
        form_yes_no.Hide();
        this.RestoreParentFocus();
      }

      if (!WarningAccountRechargeWithLastOperationAccount(operation_data))
      {
        return;
      }

      // After a form_credit, refresh account data.
      account_watcher.RefreshAccount(_card_data);
      _card_data = account_watcher.Card;

      ArrayList _voucher_list_tpv;
      _voucher_list_tpv = new ArrayList();

      _cashier_session_info = Cashier.CashierSessionInfo();
      _is_pinpad_recharge = Misc.IsPinPadEnabled() && Common.Cashier.ReadPinPadEnabled() && _exchange_result != null && _exchange_result.InType == CurrencyExchangeType.CARD;

      _operation_id = 0;

      if (this.form_credit.JunketInfo != null && this.form_credit.JunketInfo.BusinessLogic != null)
      {
        _junket = this.form_credit.JunketInfo.BusinessLogic;
      }

      if (_is_pinpad_recharge)
      {
        if (!InternetConnection.GetServiceInternetConnectionStatus("WCP"))
        {
          frm_message.Show(Resource.String("STR_PINPAD_CANCELLED_OPERATION_ERROR_CONNEXION"), Resource.String("STR_APP_GEN_MSG_WARNING"), MessageBoxButtons.OK, MessageBoxIcon.Warning, ParentForm);
          return;
        }

        _pinpad_input = new PinPadInput();
        using (DB_TRX _db_trx = new DB_TRX())
        {
          
          if (!Accounts.CommonRecharge(operation_data, _card_data, _exchange_result, _participate_in_cash_draw, _cashier_session_info, _db_trx, _operation_id, _junket, out OutputParameter, out _warning_amount_to_pay_zero, out _error_SalesLimitReached_text, out _error_text)) // ETP 27/10/2016 Check if operation can be done.
          {
            _db_trx.Rollback();

            if (_warning_amount_to_pay_zero)
            {
              return;
            }

            if (!string.IsNullOrEmpty(_error_SalesLimitReached_text))
            {
              frm_message.Show(_error_SalesLimitReached_text,
                               Resource.String("STR_APP_GEN_MSG_WARNING"),
                               MessageBoxButtons.OK,
                               MessageBoxIcon.Warning,
                               ParentForm);

              form_yes_no.Hide();

              this.RestoreParentFocus();

              return;
            }

            if (!string.IsNullOrEmpty(_error_text))
            {
              frm_message.Show(_error_text, Resource.String("STR_APP_GEN_MSG_WARNING"), MessageBoxButtons.OK, MessageBoxIcon.Warning, ParentForm);
              return;
            }
            _db_trx.Rollback(); // // ETP 27/10/2016 Operation can be done / Rollback.
          }
          else
          {

            //Add Flag

            if (this.form_credit.JunketInfo != null)
            {
              if (!this.form_credit.JunketInfo.ProcessJunketToAccount(OutputParameter.OperationId, _db_trx.SqlTransaction))
              {
                _db_trx.Rollback();

                _error_text = Resource.String("STR_JUNKET_ERROR");

                frm_message.Show(_error_text, Resource.String("STR_APP_GEN_MSG_WARNING"), MessageBoxButtons.OK, MessageBoxIcon.Warning, ParentForm);
                _return_error = true;
              }
        }


          }
        }


        _pinpad_input.Amount = _exchange_result;
        _pinpad_input.Card = _card_data;
        _pinpad_input.TerminalName = _cashier_session_info.TerminalName;

        _form_tpv = new frm_tpv(_pinpad_input);
        if (!_form_tpv.Show(out _pinpad_output)) // 20/10/2016 ETP: Pin Pad payment
        {
          String _message = String.Empty;

          if (_form_tpv.OperationCanceled)
          {
            _message = Resource.String("STR_PINPAD_CANCELED_BY_USER");
          }
          else
          {
            _message = _pinpad_output.PinPadTransaction.ErrorMessage;
          }

          frm_message.Show(_message,
               Resource.String("STR_APP_GEN_MSG_WARNING"),
               MessageBoxButtons.OK,
               MessageBoxIcon.Warning,
               ParentForm);
          _form_tpv.Dispose();
          return;
        }
        _form_tpv.Dispose();
        // 20/10/2016 ETP: Transaction is Pending of udpating wigos data.
      }

      _return_error = false;

      using (DB_TRX _db_trx = new DB_TRX())
      {
        #region " CREDIT LINE "

        //Currency exchange CREDIT LINE
        if (_exchange_result != null && _exchange_result.InType == CurrencyExchangeType.CREDITLINE)
        {
          _result_get_marker = WSI.Cashier.CreditLines.CreditLine.getMarker(_card_data, _exchange_result.InAmount + _exchange_result.CardPrice, ParentForm, 
                                                                            _cashier_session_info, CreditLineMovements.CreditLineMovementType.GetMarker, 
                                                                            _db_trx.SqlTransaction, out _operation_id);
          if (_result_get_marker != ResultGetMarker.OK)
          {
            return;
          }
        }

        #endregion " CREDIT LINE "

        if (!Accounts.CommonRecharge(operation_data, _card_data, _exchange_result, _participate_in_cash_draw, _cashier_session_info, _db_trx, _operation_id, _junket, out OutputParameter, out _warning_amount_to_pay_zero, out _error_SalesLimitReached_text, out _error_text))
        {
          _db_trx.Rollback();

          if (_warning_amount_to_pay_zero)
          {
            _return_error = true;
          }

          if (!string.IsNullOrEmpty(_error_SalesLimitReached_text) && !_return_error)
          {
            frm_message.Show(_error_SalesLimitReached_text,
                             Resource.String("STR_APP_GEN_MSG_WARNING"),
                             MessageBoxButtons.OK,
                             MessageBoxIcon.Warning,
                             ParentForm);

            form_yes_no.Hide();

            this.RestoreParentFocus();

            _return_error = true;
          }

          if (!string.IsNullOrEmpty(_error_text) && !_return_error)
          {
            frm_message.Show(_error_text, Resource.String("STR_APP_GEN_MSG_WARNING"), MessageBoxButtons.OK, MessageBoxIcon.Warning, ParentForm);
            _return_error = true;
          }
        }
        else
        {

          //Add Flag

          if (this.form_credit.JunketInfo != null)
          {
            if (!this.form_credit.JunketInfo.ProcessJunketToAccount(OutputParameter.OperationId, _db_trx.SqlTransaction))
            {
              _db_trx.Rollback();

              _error_text = Resource.String("STR_JUNKET_ERROR");

            frm_message.Show(_error_text, Resource.String("STR_APP_GEN_MSG_WARNING"), MessageBoxButtons.OK, MessageBoxIcon.Warning, ParentForm);
            _return_error = true;
            }
          }
        }

        if (_is_pinpad_recharge && !_return_error)
        {
          PinPadTransactions _transaction;

          _transaction = _pinpad_output.PinPadTransaction;

          _transaction.OperationId = OutputParameter.OperationId;
          _transaction.StatusCode = PinPadTransactions.STATUS.APPROVED;  //  20/10/2016 ETP: PinPad is payed OK.

          if (!_transaction.DB_Insert(_db_trx.SqlTransaction) && !_return_error)
          {

            _return_error = true;
          }

          _transaction.UpdateCashierSessionHasPinPadOperation(_cashier_session_info.CashierSessionId, _db_trx.SqlTransaction);

          if (!PinPadTransactions.DeleteFromUndoPinPad(_transaction.ControlNumber, _db_trx.SqlTransaction) && !_return_error)
          {

            _return_error = true;
          }

          if (!_pinpad_output.PinPadTransaction.CreateVoucher(_exchange_result, _pinpad_output, _db_trx.SqlTransaction, out _voucher_list_tpv) && !_return_error)
          {
            _return_error = true;
          }
        }

        _voucher_list = OutputParameter.VoucherList;

        // Check if Flyer added
        if (_junket != null)
        {
          if (_junket.PrintVoucher)
          {
            JunketsShared _junket_shared;
            VoucherJunket _junket_voucher;

            // Add Flyer voucher to Vouchr List for Printing
            _junket_shared = new JunketsShared()
            {
              BusinessLogic = _junket
            };

            _junket_voucher = _junket_shared.CreateVoucher(OutputParameter.OperationId, _card_data, PrintMode.Print, _db_trx.SqlTransaction);
                        
            _voucher_list.Add(_junket_voucher);
          }
        }


        if (!_return_error)
        {
          _db_trx.Commit();
        }
      }


      if (_return_error)
      {
        // 20/10/2016 ETP: Undo PinPad Operations if and error has ocurred:
        if (_is_pinpad_recharge)
        {
          using (DB_TRX _db_trx = new DB_TRX())
          {
            _pinpad_output.PinPadTransaction.OperationId = OutputParameter.OperationId;
            _pinpad_output.PinPadTransaction.StatusCode = PinPadTransactions.STATUS.ERROR;
            _pinpad_output.PinPadTransaction.DB_Insert(_db_trx.SqlTransaction);

            _pinpad_output.PinPadTransaction.UpdateCashierSessionHasPinPadOperation(_cashier_session_info.CashierSessionId, _db_trx.SqlTransaction);

            PinPadTransactions.UpdateDateUndoPinPad(_pinpad_output.PinPadTransaction.ControlNumber, _db_trx.SqlTransaction);
            _db_trx.Commit();
          }
        }

        return;
      }


      _cash_desk_draw_result = OutputParameter.CashDeskDrawResult;

     


      _voucher_list.AddRange(_voucher_list_tpv);

      // ATB 07-NOV-2016 Bug 20155:Televisa: After a cash reload the voucher do not follow the Televisa's requested order
      if (Misc.IsVoucherModeTV())
      {
        ArrayList _sorted_array_list = new ArrayList();
        Voucher _temp_summary_voucher = new Voucher();
        foreach (Voucher _voucher in _voucher_list)
        {
          if (_voucher.GetType().Name == "VoucherSummaryCredits")
          {
            _temp_summary_voucher = _voucher;
          }
          else
          {
            _sorted_array_list.Add(_voucher);
          }
        }
        if (_temp_summary_voucher.VoucherId != 0)
        {
          _sorted_array_list.Add(_temp_summary_voucher);
        }

        VoucherPrint.Print(_sorted_array_list, true); //EOR 28-AUG-2017
      }
      else
      {
        VoucherPrint.Print(_voucher_list, true); //EOR 28-AUG-2017
      }

      if (OutputParameter.CashDeskDraw != CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION.DRAW_02
            && CashDeskDrawBusinessLogic.IsEnabledPreviewCashDeskDraws()
            && _cash_desk_draw_result != null
            && _cash_desk_draw_result.m_bet_numbers != null
            && _cash_desk_draw_result.m_winning_combination != null)
      {
        frm_preview_cash_desk _preview_cash_desk = new frm_preview_cash_desk(_cash_desk_draw_result);
        form_yes_no.Show(this.ParentForm);

        try
        {
          _preview_cash_desk.ShowDialog();
        }
        finally
        {
          // ICS 14-APR-2014 Free the object
          _preview_cash_desk.Dispose();

          form_yes_no.Hide();
          this.RestoreParentFocus();
        }
      }

      ConditionallyGenerateDrawTickets(OperationCode.CASH_IN);

      RefreshAccount();
    }

    /// <summary>
    /// Show message when a recharge operation will be done before a previous operation for the same account
    /// </summary>
    /// <param name="OperationData"></param>
    /// <returns></returns>
    private Boolean WarningAccountRechargeWithLastOperationAccount(Object OperationData)
    {
      StringBuilder _sb;
      Object _obj;
      DialogResult _rc;
      String _holder_name;
      String _message;
      CashierOperations.TYPE_CARD_CASH_IN _data;
      Boolean _show_warning_recharge;

      _sb = new StringBuilder();
      _data = (CashierOperations.TYPE_CARD_CASH_IN)OperationData;
      _obj = null;
      _rc = DialogResult.OK;
      _show_warning_recharge = GeneralParam.GetBoolean("Cashier.Recharge", "ShowSameAccountWarning", false);

      // Get last operation accountId
      _sb.AppendLine("  SELECT   TOP 1 AO_ACCOUNT_ID                                      ");
      _sb.AppendLine("    FROM   ACCOUNT_OPERATIONS  WITH(INDEX(IX_ao_session_id_date))   ");
      _sb.AppendLine("   WHERE   AO_ACCOUNT_ID <> 0                                       ");
      _sb.AppendLine("           AND AO_CASHIER_SESSION_ID = @pSessionId                  ");
      _sb.AppendLine("ORDER BY   AO_DATETIME DESC                                         ");

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = Cashier.CashierSessionInfo().CashierSessionId;

            _obj = _sql_cmd.ExecuteScalar();

            if (_obj == null || _obj == DBNull.Value || m_card_data.AccountId != (Int64)_obj)
            {
              return true;
            }
          }
        }

        // Get account name
        if (m_card_data.PlayerTracking.CardLevel != 0)
        {
          _holder_name = m_card_data.PlayerTracking.HolderName;
        }
        else
        {
          _holder_name = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_ACCOUNT_MODE_ANONYMOUS");
        }

        // Show message
        _message = Resource.String("STR_RECHARGE_SHOW_SAME_ACCOUNT_WARNING", m_card_data.AccountId, _holder_name);
        _message = _message.Replace("\\r\\n", "\r\n");

        if (_show_warning_recharge)
        {
          _rc = frm_message.Show(_message, Resource.String("STR_APP_GEN_MSG_WARNING"), MessageBoxButtons.YesNo, MessageBoxIcon.Warning, ParentForm);
        }

        // Log warning
        Log.Message(String.Format("Recharge using last operation account {0} -> User Id: {1} Amount: {2} Reply: {3} WarningShowed: {4}", m_card_data.AccountId, Cashier.CashierSessionInfo().UserName, _data.amount_to_pay, _rc.ToString(), _show_warning_recharge));

        if (_rc == DialogResult.OK)
        {
          return true;
        }
      }
      catch (Exception)
      {
      }

      return false;
    }

    /// <summary>
    /// Reserved credit
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btn_reserved_Click(object sender, EventArgs e)
    {
      CardData _card_data;
      frm_reserved form_reserved;
      AccountBlockReason _block_reason_found;

      // Check if current user is an authorized user
      String error_str;
      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.CardAddCredit, ProfilePermissions.TypeOperation.RequestPasswd, this.ParentForm, out error_str))
      {
        return;
      }

      _card_data = account_watcher.Card;

      if (Accounts.ExistsBlockReasonInMask(new List<AccountBlockReason>(new AccountBlockReason[] {
                                                    AccountBlockReason.EXTERNAL_SYSTEM_CANCELED, AccountBlockReason.EXTERNAL_AML }),
                                           Accounts.ConvertOldBlockReason((Int32)_card_data.BlockReason), out _block_reason_found))
      {
        String _msg;

        _msg = "";
        switch (_block_reason_found)
        {
          case AccountBlockReason.EXTERNAL_SYSTEM_CANCELED:
            _msg = Resource.String("STR_CANCELLED_SPACE_ACCOUNTS");
            break;

          case AccountBlockReason.EXTERNAL_AML:
            _msg = Resource.String("STR_UNBLOCK_ERR_EXTERNAL_AML");
            break;
        }

        if (!String.IsNullOrEmpty(_msg))
        {
          frm_message.Show(_msg,
                           Resource.String("STR_APP_GEN_MSG_WARNING"),
                           MessageBoxButtons.OK,
                           MessageBoxIcon.Information,
                           ParentForm);
        }

        return;
      }

      form_reserved = new frm_reserved(_card_data);
      try
      {
        if (!form_reserved.Show(this.ParentForm))
        {
          ClearCardData();
          Log.Error("btn_block_Click: BlockUnblockAccount. Error block / unblock account. AccountId: " + _card_data.AccountId.ToString());

          return;
        }
      }
      finally
      {
        form_reserved.Dispose();
      }

      RefreshAccount();
    }

    /// <summary>
    /// Show last card movements (plays)
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btn_last_plays_Click(object sender, EventArgs e)
    {
      String sql_str;

      if (m_current_card_id == 0)
      {
        return;
      }

      // Query to obtain last cards movements

      // MBF 08-FEB-2010 ROLLBACK Player Tracking

      //sql_str = "SELECT TOP 40 AM_TYPE, '  ' AS TYPE_NAME, AM_DATETIME, '  ' AS USER_NAME, TE_NAME, AM_TERMINAL_ID, AM_INITIAL_BALANCE, AM_SUB_AMOUNT, AM_ADD_AMOUNT, AM_FINAL_BALANCE, AM_ADD_POINTS " +
      //          "FROM ( SELECT TOP 40 AM_MOVEMENT_ID, AM_TYPE, AM_DATETIME, AM_TERMINAL_ID, AM_INITIAL_BALANCE, AM_ADD_AMOUNT, AM_SUB_AMOUNT, AM_FINAL_BALANCE, AM_ADD_POINTS " +
      //                  "FROM ACCOUNT_MOVEMENTS " +
      //                  "WHERE AM_TYPE NOT IN (14,15,16,17,19) AND AM_ACCOUNT_ID = " + m_current_card_id.ToString() + "  " +
      //                  "ORDER BY AM_DATETIME DESC, AM_MOVEMENT_ID DESC " +
      //               ") X LEFT OUTER JOIN TERMINALS AS T ON AM_TERMINAL_ID = T.TE_TERMINAL_ID ";

      // RCI 14-JUN-2010: Get TERMINAL or CASHIER Name and Id. Removed JOIN with TERMINALS Table: we have all 
      //                  data needed in ACCOUNT_MOVEMENTS Table.
      sql_str = "SELECT TOP 150 AM_TYPE " +
                "  , '  ' AS TYPE_NAME " +
                "  , AM_DATETIME " +
                "  , '  ' AS USER_NAME " +
                "  , CASE WHEN AM_TERMINAL_ID IS NOT NULL THEN AM_TERMINAL_NAME " +
                "         WHEN AM_CASHIER_ID IS NOT NULL THEN AM_CASHIER_NAME " +
                "         WHEN AM_TERMINAL_ID IS NULL AND AM_CASHIER_ID IS NULL THEN AM_CASHIER_NAME" +
                "          ELSE NULL " +
                "    END AM_TERM_NAME " +
                "  , CASE WHEN AM_TERMINAL_ID IS NOT NULL THEN AM_TERMINAL_ID " +
                "         WHEN AM_CASHIER_ID IS NOT NULL THEN AM_CASHIER_ID " +
                "         ELSE NULL " +
                "    END AM_TERM_ID " +
                "  , AM_INITIAL_BALANCE " +
                "  , AM_SUB_AMOUNT " +
                "  , AM_ADD_AMOUNT " +
                "  , AM_FINAL_BALANCE " +
                "  , ISNULL(AM_OPERATION_ID, 0) " +
                "  , '' AS COLOR_ROW " +
                "  , AM_DETAILS " +
                "  , AM_UNDO_STATUS " +
                "FROM ACCOUNT_MOVEMENTS " +
                "WHERE AM_ACCOUNT_ID = " + m_current_card_id.ToString() + "  " +
                "       AND AM_TYPE NOT IN ( " + (Int32)MovementType.HIDDEN_RechargeNotDoneWithCash + " ) " +
                "ORDER BY AM_DATETIME DESC, AM_MOVEMENT_ID DESC ";

      this.Cursor = Cursors.WaitCursor;
      form_last_plays.Show(sql_str, frm_last_movements.MovementShow.Card, account_watcher.Card.AccountId + " - " + account_watcher.Card.PlayerTracking.HolderName);
      this.Cursor = Cursors.Default;
    }

    /// <summary>
    /// Print Card
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btn_print_card_Click(object sender, EventArgs e)
    {
      // Check if current user is an authorized user
      String error_str;
      DialogResult msg_answer;

      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.PrintCard,
                                               ProfilePermissions.TypeOperation.ShowMsg,
                                               this.ParentForm,
                                               out error_str))
      {
        return;
      }

      // Check the printer 
      if (!card_printer.PrinterExist())
      {
        frm_message.Show(Resource.String("STR_PRINT_CARD_PRINTER_IS_NOT_INSTALLED"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this.ParentForm);

        return;
      }

      // Ask for confirmation before printing 
      msg_answer = frm_message.Show(Resource.String("STR_PRINT_CARD_PRINTER_INPUT_INFO"),
                                    Resource.String("STR_PRINT_CARD_PRINTER_INPUT_INFO_MSG"),
                                    MessageBoxButtons.YesNo,
                                    MessageBoxIcon.Warning,
                                    this.ParentForm);

      if (msg_answer == DialogResult.Cancel)
      {
        return;
      }

      // Send document to print
      this.Cursor = Cursors.WaitCursor;

      card_printer.PrintNameInCard(lbl_holder_name.Text);

      this.Cursor = Cursors.Default;

    } // btn_print_card_Click

    #endregion Buttons

    #region Methods

    #endregion

    public void ClearCardData()
    {
      m_current_track_number = "";
      visible_track_number = "";

      RefreshData(new CardData());
    }

    public void InitFocus()
    {
      uc_card_reader1.Focus();
    }

    /// <summary>
    /// Event fired when a card is swiped.
    /// </summary>
    /// <param name="TrackNumber">Number read</param>
    /// <param name="IsValid">Track read is valid</param>
    public void uc_card_reader1_OnTrackNumberReadEvent(string TrackNumber, ref Boolean IsValid)
    {
      CardData _card_data;
      _card_data = new CardData();

      //Get Data by TrackNumber
      CardData.DB_CardGetAllData(TrackNumber, _card_data, false);

      Log.Message(String.Format("Card reading - AccountId: {0}  Trackdata: {1}", _card_data.AccountId, TrackNumber));

      m_swipe_card = true;

      //If not exitst, then create account, else Load the Account
      m_is_new_card = _card_data.IsNew;
      if (_card_data.IsNew)
      {
        _card_data.TrackData = TrackNumber;
        NewAccount(_card_data);
      }
      else
      {
        m_pending_check_comments = true;
        if (!_card_data.IsVirtualCard)
        {
          //It is necessary to pass the SiteId for cards that are in an invalid site.
          uc_card_reader1_OnTrackNumberReadEvent(_card_data.AccountId, ref IsValid, _card_data.SiteId);
        }
        else
        {
          IsValid = false;
        }
      }
    }

    /// <summary>
    /// Event called after a card is swiped
    /// and called when a Player is searched
    /// </summary>
    /// <param name="TrackNumber">Number read</param>
    /// <param name="IsValid">Track read is valid</param>
    public void uc_card_reader1_OnTrackNumberReadEvent(Int64 AccountId, ref Boolean IsValid, int SideId = -1)
    {
      //Reset timer to disable buttons
      tick_count_tracknumber_read = 0;
      // Load and load track number
      m_pending_check_comments = true;
      m_jackpot_notified = false;

      IsValid = LoadCardByAccountId(AccountId, SideId);

      m_is_new_card = false;

      if (!IsValid)
      {
        EnableDisableButtons(IsValid);
    }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Change the status of the cashier buttons based on the current cashier 
    //          status (open / not open), the account status (locked / in use) and the 
    //          incoming parameter.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - EnableButtons
    //              - TRUE: try to enable buttons, actually enabled buttons depend on 
    //                      cashier and account status.
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //

    private void EnableDisableButtons(Boolean EnableButtons)
    {
      Boolean _enabled;
      Boolean _anon_allowed;
      Boolean _can_recycle;
      Boolean _account_is_anonymous;
      CASHIER_STATUS _status;
      Int32 _reserved_mode;

      _can_recycle = true;

      _account_is_anonymous = false;

      timer1.Enabled = EnableButtons;

      _status = CashierBusinessLogic.ReadCashierStatus(Cashier.SessionId);
      m_parent.ChangeLabelStatus();

      _enabled = EnableButtons && !is_logged_in;
      _anon_allowed = false;
      // MBF 21-12-10 account_watcher, Card or PlayerTracking can be NULL

      _reserved_mode = (Int32)MultiPromos.GAME_GATEWAY_RESERVED_MODE.GAME_GATEWAY_RESERVED_MODE_WITHOUT_BUCKETS;

      try
      {
        if (EnableButtons)
        {
          Int32 _aux;
          _aux = 0;
          Int32.TryParse(WSI.Common.Misc.ReadGeneralParams("Cashier", "AllowedAnonymous"), out _aux);
          _anon_allowed = (_aux == 0) && (account_watcher.Card.PlayerTracking.HolderName == "");
        }
      }
      catch (Exception)
      {
        // Allow anons by default
        _anon_allowed = true;
      }

      ToggleApparenceCtrl(this, EnableButtons);

      btn_session_log_off.Enabled = EnableButtons && can_log_off;
      //If holder is playing, the account can't change the card. 
      //03-JUL-2012 Can't change the account card until card is paid.
      btn_new_card.Enabled = _enabled && (account_watcher.Card.CardPaid || account_watcher.Card.IsRecycled);
      btn_account_edit.Enabled = EnableButtons;
      btn_card_block.Enabled = EnableButtons;
      btn_credit_add.Enabled = EnableButtons && !_anon_allowed;
      btn_vouchers_space.Enabled = EnableButtons;

      this.btn_sell_chips.Enabled = _enabled;
      this.btn_buy_chips.Enabled = _enabled;
      this.btn_chips_sale_register.Enabled = _enabled
                                          && GeneralParam.GetBoolean("GamingTables", "Cashier.RegisterChipSaleAtTableEnabled");
      this.btn_transfer_credit.Enabled = _enabled;

      this.btn_cash_advance.Visible = m_is_cash_advance_enabled;
      this.btn_cash_advance.Enabled = _enabled;

      _reserved_mode = GeneralParam.GetInt32("GameGateway", "Reserved.Enabled", (Int32)MultiPromos.GAME_GATEWAY_RESERVED_MODE.GAME_GATEWAY_RESERVED_MODE_WITHOUT_BUCKETS);

      this.btn_reserved.Visible = (Misc.IsGameGatewayEnabled()
                                 && ((MultiPromos.GAME_GATEWAY_RESERVED_MODE)_reserved_mode == MultiPromos.GAME_GATEWAY_RESERVED_MODE.GAME_GATEWAY_RESERVED_MODE_WITH_BUCKETS
                                    || (MultiPromos.GAME_GATEWAY_RESERVED_MODE)_reserved_mode == MultiPromos.GAME_GATEWAY_RESERVED_MODE.GAME_GATEWAY_RESERVED_MODE_MIXED)
                                 || Misc.IsWS2SFBMEnabled());

      //Fixed Bug 7584:MonoPlay: Disable BonoPlay button for anonymous accounts
      if (account_watcher != null && account_watcher.Card != null && account_watcher.Card.PlayerTracking.CardLevel == 0)
      {
        _account_is_anonymous = true;
      }
      this.btn_reserved.Enabled = (_enabled && !_account_is_anonymous);
      _account_is_anonymous = false;

      if (account_watcher != null && account_watcher.Card != null)
      {
        // RCI & AJQ 31-MAY-2011
        btn_credit_add.Enabled &= account_watcher.Card.CanAddCredit;

        if (account_watcher.Card.AccountBalance.TotalBalance <= 0)
        {
          btn_transfer_credit.Enabled = false;

          m_integrated_chip_operation = FeatureChips.IsIntegratedChipAndCreditOperations(account_watcher.Card.IsVirtualCard);
          if (!m_integrated_chip_operation && !FeatureChips.CashierChipsSaleMode(account_watcher.Card.IsVirtualCard))
          {
            btn_sell_chips.Enabled = false;
          }
        }
      }

      btn_credit_add.Enabled &= m_swipe_card;
      btn_sell_chips.Enabled &= m_swipe_card;
      btn_buy_chips.Enabled &= m_swipe_card;
      btn_chips_sale_register.Enabled &= m_swipe_card;
      btn_transfer_credit.Enabled &= m_swipe_card;
      btn_cash_advance.Enabled &= m_swipe_card;

      // RCI & AJQ 02-OCT-2010: Promotion button
      btn_cancel_promo.Enabled = _enabled;
      btn_promotion.Enabled = _enabled;
      btn_promotion.Text = Resource.String("STR_UC_CARD_BTN_PROMOTION_DELETE");
      if (!m_promotion_enabled)
      {
        btn_promotion.Enabled = false;
      }
      else
      {
        if (m_promotion_convertible)
        {
          btn_promotion.Text = Resource.String("STR_UC_CARD_BTN_PROMOTION_REDEEM");
        }
      }

      btn_draw.Enabled = _enabled; // SLE 9-JUN-2010

      btn_lasts_movements.Enabled = EnableButtons;
      btn_print_card.Enabled = EnableButtons && card_printer.PrinterExist();
      btn_handpays.Enabled = _enabled;
      btn_gifts.Enabled = _enabled && m_swipe_card;
      btn_multiple_buckets.Enabled = _enabled;
      // LTC 22-FEB-2016
      btn_disputes.Enabled = _enabled;

      // ICS 21-OCT-2013
      btn_undo_operation.Enabled = _enabled && GeneralParam.GetInt32("Cashier.Undo", "Allowed.Minutes") != 0;
      btn_undo_operation.Enabled &= m_swipe_card;

      btn_credit_redeem.Enabled = _enabled && m_swipe_card;

      // SGB 02-JUN-2015
      this.btn_win_loss_statement_request.Visible = GeneralParam.GetBoolean("WinLossStatement", "Enabled", false);
      this.btn_win_loss_statement_request.Enabled = _enabled && !(account_watcher.Card.PlayerTracking.HolderName == "");

      btn_handpay_mico2.Enabled = _status == CASHIER_STATUS.OPEN;

      if (EnableButtons)
      {
        if (_status != CASHIER_STATUS.OPEN)
        {
          btn_credit_add.Enabled = false;
          btn_credit_redeem.Enabled = false;
          btn_draw.Enabled = false;
          btn_handpays.Enabled = false;
          btn_handpay_mico2.Enabled = false;
          // LTC 22-FEB-2016
          btn_disputes.Enabled = false;

          btn_gifts.Enabled = false;
          btn_promotion.Enabled = false;
          // RCI 18-OCT-2011: Disable all buttons when cashier is not open, except last movements button.
          btn_new_card.Enabled = false;
          btn_account_edit.Enabled = false;
          btn_card_block.Enabled = false;
          btn_session_log_off.Enabled = false;
          btn_cancel_promo.Enabled = false;
          btn_undo_operation.Enabled = false;
          btn_sell_chips.Enabled = false;
          btn_buy_chips.Enabled = false;
          btn_chips_sale_register.Enabled = false;
          btn_transfer_credit.Enabled = false;
          btn_cash_advance.Enabled = false;
          //SGB 13-OCT-2014 Fixed Bug WIG-1468: Button redeem ticket not disable if cash is closing
          btn_vouchers_space.Enabled = false;
          // SGB 02-JUN-2015
          btn_win_loss_statement_request.Enabled = false;
          btn_reserved.Enabled = false;
          btn_multiple_buckets.Enabled = false;
          _can_recycle = false;
        }

        // RCI 24-NOV-2011: If card is blocked, disable the following buttons.
        if (account_watcher != null && account_watcher.Card != null)
        {
          if (account_watcher.Card.Blocked)
          {
            btn_credit_add.Enabled = false;
            btn_credit_redeem.Enabled = false;
            btn_draw.Enabled = false;
            btn_handpays.Enabled = false;
            // LTC 22-FEB-2016
            btn_disputes.Enabled = false;

            btn_gifts.Enabled = false;
            btn_promotion.Enabled = false;
            btn_vouchers_space.Enabled = false;
            // 21-NOV-2013 JML     Fixed Bug WIG-424: Block account player: Some buttons on the Cashier are not disabled.
            btn_cancel_promo.Enabled = false;
            btn_undo_operation.Enabled = false;
            // RRB 17-OCT-2012: When the card is blocked allow: associate new card and recycle card
            //btn_new_card.Enabled = false;
            //_can_recycle = false;

            btn_sell_chips.Enabled = false;
            btn_buy_chips.Enabled = false;
            btn_chips_sale_register.Enabled = false;
            btn_cash_advance.Enabled = false;
            btn_transfer_credit.Enabled = false;
            btn_reserved.Enabled = false;
            btn_multiple_buckets.Enabled = false;
          }

          // Card Anonymous
          if (account_watcher.Card.PlayerTracking.CardLevel == 0)
          {
            btn_vouchers_space.Enabled = false;
            _account_is_anonymous = true;
          }
        }
      }

      //If user no permited, button invisible,
      //If user don't have associated card, invisible
      // Use _enabled, if player is logged in (playing) we don't want to allow to recycle the card.
      HideRecycleButton(_enabled && _can_recycle);

      gb_loyalty_program.Visible = false;
      if (GeneralParam.GetInt32("PlayerTracking.ExternalLoyaltyProgram", "Mode") != 0)
      {
        // RCI JVV 10-APR-2014: Allow anonymous card change only if GP is enabled.
        // 17-JUL-2015 JPJ     Fixed Bug WIG-2605: Card replacement button active when cashier is closed.
        btn_new_card.Enabled = (CashierBusinessLogic.ReadCashierStatus(Cashier.SessionId) == CASHIER_STATUS.OPEN
                              && _account_is_anonymous
                              && GeneralParam.GetBoolean("ExternalLoyaltyProgram.Mode01", "Cashier.AllowAnonymousCardChange"));

        btn_account_edit.Enabled = false;
        gb_gifts.Visible = false;
        if (GeneralParam.GetInt32("PlayerTracking.ExternalLoyaltyProgram", "Mode") == 1)
        {
          gb_loyalty_program.Visible = true;
        }
      }

      if (account_watcher != null &&
          Accounts.ExistsBlockReasonInMask(new List<AccountBlockReason>(new AccountBlockReason[] {
                                                    AccountBlockReason.EXTERNAL_SYSTEM_CANCELED /*, AccountBlockReason.EXTERNAL_AML */ }),
                                           Accounts.ConvertOldBlockReason((Int32)account_watcher.Card.BlockReason)))
      {
        btn_card_block.Enabled = false;
      }
    } // EnableDisableButtons

    //------------------------------------------------------------------------------
    // PURPOSE: Enable Recycle Button, and others dependending of User Cashier Permissions
    //          Account Balance, or actual Account has a track_data_recycled   
    //          When is member of multisite, recycled button are disabled.
    // 
    //  PARAMS:
    //      - INPUT: EnableButtons is the flag if this procedure is called for
    //               Enable/Disable Buttons action
    //      - OUTPUT:
    // RETURNS:
    //   NOTES:
    //
    private void HideRecycleButton(Boolean EnableButtons)
    {
      String _error_str;

      if (m_current_track_number == CardData.RECYCLED_TRACK_DATA)
      {
        btn_account_edit.Enabled = false;
        btn_card_block.Enabled = false;
        btn_recycle_card.Enabled = false;
        btn_credit_add.Enabled = false;
        btn_credit_redeem.Enabled = false;
        btn_promotion.Enabled = false;
        btn_draw.Enabled = false;
        btn_gifts.Enabled = false;
        btn_print_card.Enabled = false;
        btn_handpays.Enabled = false;
        btn_cancel_promo.Enabled = false;
        btn_reserved.Enabled = false;
        //LTC 22-FEB-2016
        btn_disputes.Enabled = false;
      }

      // Check if current user is an authorized user
      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.RecyleCard,
                                               ProfilePermissions.TypeOperation.OnlyCheck, this.ParentForm, false, out _error_str))
      {
        btn_recycle_card.Visible = false;

        return;
      }

      btn_recycle_card.Visible = true;

      //If user no permited, button invisible returns FALSE
      //If user have deassociated card ,button invisible returns FALSE

      btn_recycle_card.Enabled = false;

      if (!EnableButtons)
      {
        return;
      }

      //If actual Account has Cash, don't show button
      if (m_current_card_balance != 0)
      {
        return;
      }

      if (m_current_track_number == CardData.RECYCLED_TRACK_DATA)
      {
        return;
      }

      if (GeneralParam.GetBoolean("Site", "MultiSiteMember", false))
      {
        return;
      }

      btn_recycle_card.Enabled = true;

    } // HideRecycleButton

    private void ToggleApparenceCtrl(Control Ctrl, Boolean Enabled)
    {
      if (Ctrl.Equals(uc_card_reader1)
        //|| Ctrl.Equals(lbl_cards_title)
          || Ctrl is Button)
      {
        return;
      }

      if (!Ctrl.Equals(this))
      {
        Ctrl.Enabled = Enabled;
      }

      foreach (Control ctrl in Ctrl.Controls)
      {
        ToggleApparenceCtrl(ctrl, Enabled);
      }
    }

    private void timer1_Tick(object sender, EventArgs e)
    {
      tick_count_tracknumber_read += 1;

      lbl_card_in_use.Visible = (is_logged_in || is_locked) && ((tick_count_tracknumber_read % 2) == 0);
      lbl_balance_mismatch.Visible = (m_balance_mismatch) && ((tick_count_tracknumber_read % 2) == 0);

      if (tick_count_tracknumber_read >= CashierBusinessLogic.DURATION_BUTTONS_ENABLED_AFTER_OPERATION)
      {
        // MBF 21-JAN-11 TO DO IN THE FUTURE
        // ClearCardData();
        EnableDisableButtons(false);

        //JCM 02-MAY-2012 Ensure focus is in the card reader after enable/disable buttons
        InitFocus();

        return;
      }
    }

    private void uc_card_reader1_OnTrackNumberReadingEvent()
    {
      EnableDisableButtons(false);
    }

    private void btn_block_Click(object sender, EventArgs e)
    {
      CardData _card_data;
      frm_block_account _block_account;
      AccountBlockReason _block_reason_found;

      // Check if current user is an authorized user
      String error_str;
      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.AccountLock, ProfilePermissions.TypeOperation.RequestPasswd, this.ParentForm, out error_str))
      {
        return;
      }

      _card_data = account_watcher.Card;

      if (Accounts.ExistsBlockReasonInMask(new List<AccountBlockReason>(new AccountBlockReason[] {
                                                    AccountBlockReason.EXTERNAL_SYSTEM_CANCELED, AccountBlockReason.EXTERNAL_AML }),
                                           Accounts.ConvertOldBlockReason((Int32)_card_data.BlockReason), out _block_reason_found))
      {
        String _msg;

        _msg = "";
        switch (_block_reason_found)
        {
          case AccountBlockReason.EXTERNAL_SYSTEM_CANCELED:
            _msg = Resource.String("STR_CANCELLED_SPACE_ACCOUNTS");
            break;

          case AccountBlockReason.EXTERNAL_AML:
            _msg = Resource.String("STR_UNBLOCK_ERR_EXTERNAL_AML");
            break;
        }

        if (!String.IsNullOrEmpty(_msg))
        {
          frm_message.Show(_msg,
                           Resource.String("STR_APP_GEN_MSG_WARNING"),
                           MessageBoxButtons.OK,
                           MessageBoxIcon.Information,
                           ParentForm);
        }

        return;
      }

      _block_account = new frm_block_account(_card_data);
      try
      {
        if (!_block_account.Show(this.ParentForm))
        {
          ClearCardData();
          Log.Error("btn_block_Click: BlockUnblockAccount. Error block / unblock account. AccountId: " + _card_data.AccountId.ToString());

          return;
        }
      }
      finally
      {
        _block_account.Dispose();
      }

      RefreshAccount();
    }

    private void RefreshAccount()
    {
      account_watcher.ForceRefresh();
    }

    /// <summary>
    /// Log of Card Session: Apply when a card remained connected on a bet machine without being really connected.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btn_logoff_Click(object sender, EventArgs e)
    {
      DialogResult _dlg_result;
      Boolean _log_off_status;
      CardData _card_data;
      Decimal _current_balance;
      Decimal _calculated_balance;
      Decimal _reported_balance;
      Boolean _selected_by_user;
      Boolean _dummy_balance_changed;
      Decimal _new_balance;
      String _error_str;
      String _close_session_comment;
      String _user_and_cashier_name;

      // Check if current user is an authorized user
      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.AccountLogOff,
                                               ProfilePermissions.TypeOperation.RequestPasswd, this.ParentForm, out _error_str))
      {
        return;
      }

      _card_data = account_watcher.Card;

      _selected_by_user = _card_data.CurrentPlaySession.BalanceMismatch;
      _new_balance = 0;
      _close_session_comment = String.Empty;

      _current_balance = _card_data.CurrentPlaySession.FinalBalance;
      // RCI 22-JUN-2012: Have in account the CashIn while in session to calculate the final balance.
      _calculated_balance = Math.Max(0, _card_data.CurrentPlaySession.InitialBalance + _card_data.CurrentPlaySession.CashIn
                                        - _card_data.CurrentPlaySession.PlayedAmount + _card_data.CurrentPlaySession.WonAmount);

      CashierBusinessLogic.DB_GetReportedBalance(_card_data.CurrentPlaySession.PlaySessionId, out _reported_balance);

      if (_card_data.LoggedInTerminalType == TerminalTypes.T3GS && GeneralParam.GetBoolean("WS2S", "CloseSessionToZero", false))
      {
        _dlg_result = DialogResult.OK;
      }
      else
      {
        _dlg_result = frm_balance_mismatch.Show(_current_balance, _calculated_balance, _reported_balance, this.ParentForm, _card_data.CurrentPlaySession.BalanceMismatch,
                                              out _dummy_balance_changed, out _new_balance, out _close_session_comment);
      }

      if (_dlg_result == DialogResult.OK)
      {
        if (_new_balance == Math.Max(_calculated_balance, _reported_balance))
        {
          // Check permission for the highest balance
          if (!ProfilePermissions.CheckPermissionList(ProfilePermissions.CashierFormFuncionality.BalanceMismatchHighestBalance,
                                                      ProfilePermissions.TypeOperation.RequestPasswd,
                                                      this.ParentForm, 0, out _error_str))
          {
            return;
          }
        }
      
        if (String.IsNullOrEmpty(Cashier.AuthorizedByUserName))
        {
          _user_and_cashier_name = Cashier.TerminalName;
        }
        else
        {
          _user_and_cashier_name = Cashier.AuthorizedByUserName + "@" + Cashier.TerminalName;
        }

        // Card data may have changed, refresh it using account_watcher.
        _card_data = account_watcher.Card;
        _log_off_status = CardData.DB_PlaySessionCloseManually(m_current_card_id, _card_data.CurrentPlaySession.PlaySessionId, _card_data.CurrentPlaySession.FinalBalance,
                                                               _card_data.LoggedInTerminalId, _card_data.LoggedInTerminalName, _card_data.LoggedInTerminalType,
                                                               _user_and_cashier_name,
                                                               _selected_by_user, _new_balance, _close_session_comment);
      }

      // Refresh Card Data
      account_watcher.ForceRefresh();
    }

    private void btn_edit_Click(object sender, EventArgs e)
    {
      CardData _card_data;
      String error_str;
      DialogResult _dgr_ac_edit;

      if (m_is_new_card)
      {
        // AJQ 29-MAY-2013, Clear data on new Track read & Stop the AccountWatcher
        _card_data = new CardData();
        _card_data.TrackData = m_current_track_number;
        account_watcher.RefreshAccount(_card_data);
        ClearCardData();
      }
      else
      {
        ////0 byAccountId, 1 byAccountId+TrackData (Anonymous), 3 byAccountId+TrackData(Anonymous+Personal)
        if (account_watcher.Card.PlayerTracking.CardLevel == 0)
        {
          if (GeneralParam.GetInt32("AntiMoneyLaundering", "GroupByMode", 0) == 1)
          {
            frm_message.Show(Resource.String("STR_UC_CARD_AML_CANT_CUSTOMIZE_ACCOUNT"),
                             Resource.String("STR_MSG_ANTIMONEYLAUNDERING_WARNING"),
                             MessageBoxButtons.OK,
                             MessageBoxIcon.Warning,
                             this.ParentForm);
            return;
          }
        }
        _card_data = account_watcher.Card;
        CardData.DB_CardGetPersonalData(_card_data.AccountId, _card_data);

        if (!m_must_show_comments)
        {
          // RMS 14-OCT-2013: not new card, check edition permission depending on account type anonymous or personal
          ProfilePermissions.CashierFormFuncionality _permission;

          _permission = (account_watcher.Card.PlayerTracking.CardLevel > 0) ? ProfilePermissions.CashierFormFuncionality.AccountEditPersonal :
                                                                              ProfilePermissions.CashierFormFuncionality.AccountEditAnonymous;

          if (!ProfilePermissions.CheckPermissionList(_permission, ProfilePermissions.TypeOperation.RequestPasswd, this.ParentForm, 0, out error_str))
          {
            return;
          }
        }
      }
      using (frm_yesno _shader = new frm_yesno())
      {
        using (PlayerEditDetailsCashierView _view = new PlayerEditDetailsCashierView())
        {
          using (PlayerEditDetailsCashierController _controller =
            new PlayerEditDetailsCashierController(_card_data, _view, IDScanner, PlayerEditDetailsCashierView.SCREEN_MODE.DEFAULT))
          {
            _controller.MustShowComments = m_must_show_comments;
            if (WindowManager.StartUpMode == WindowManager.StartUpModes.Explorer)
            {
              _view.Location = new Point(WindowManager.GetFormCenterLocation(_shader).X - (_view.Width / 2),
                _shader.Location.Y + 2);
            }
            _shader.Opacity = 0.6;
            _shader.Show();
            _dgr_ac_edit = _view.ShowDialog(_shader);
          }
        }
      }

      if (_dgr_ac_edit == DialogResult.OK)
      {
        // AJQ 29-MAY-2013, Reload the data of the "TrackData" being edited
        // Error: use the m_current_track_number that was refreshed on background)
        //        The background refresh has been disabled for tracks that are not associated to any account.
        CardData.DB_CardGetAllData(_card_data.TrackData, _card_data, false);

        // RCI 28-AUG-2014: A new personal card is created, reset the timer.
        if (m_is_new_card)
        {
          tick_count_tracknumber_read = 0;
        }

        m_is_new_card = _card_data.IsNew;
        m_pending_check_comments = false;
      }

      if (_card_data.AccountId != 0)
      {
        // AJQ 29-MAY-2013, Refresh the data on Screen when there is an account.
        //                  AccountId is 0 when the "Personalizacion" has been cancelled and the card was "new".
        //                  Avoids displaying the message "The card doesn't belong to the site".
        LoadCardByAccountId(_card_data.AccountId);
      }
    }

    private void btn_button_clicked(object sender, EventArgs e)
    {
      Boolean _enabled_buttons_after_operation;
      Boolean _is_track_valid;
      Boolean _is_anonymous;
      CardData _card;

      Cashier.ResetAuthorizedByUser();

      if (!uc_card_reader1.Focused)
      {
        uc_card_reader1.Focus();
      }

      if ((sender.Equals(btn_credit_add))
        || (sender.Equals(btn_credit_redeem))
        || (sender.Equals(btn_draw))
        || (sender.Equals(btn_promotion))
        || (sender.Equals(btn_gifts)))
      {
        _card = account_watcher.Card;

        _is_anonymous = String.IsNullOrEmpty(_card.PlayerTracking.HolderName);

        if (Accounts.DoPinRequestFlagsExist(_is_anonymous))
        {
          if (_card.PlayerTracking.Pin == "")
          {
            GenerateNewPIN(_card);
          }
        }
      }

      try
      {
        if (sender.Equals(btn_credit_add))
        {
          ((Button)sender).Enabled = false;
          btn_add_Click(sender, e);
        }
        if (sender.Equals(btn_new_card))
        {
          // btn_assign_Click (sender, e);
        }
        if (sender.Equals(btn_recycle_card))
        {
          btn_recycle_card_Click(sender, e);
        }
        if (sender.Equals(btn_card_block))
        {
          btn_block_Click(sender, e);
        }
        if (sender.Equals(btn_promotion))
        {
          btn_promotion_Click(sender, e);
        }
        if (sender.Equals(btn_draw))
        {
          btn_draw_Click(sender, e); // SLE 9-JUN-2010
        }
        if (sender.Equals(btn_account_edit))
        {
          btn_edit_Click(sender, e);
        }
        if (sender.Equals(btn_session_log_off))
        {
          btn_logoff_Click(sender, e);
        }
        if (sender.Equals(btn_credit_redeem))
        {
          btn_redeem_Click(sender, e);
        }
        if (sender.Equals(btn_lasts_movements))
        {
          btn_last_plays_Click(sender, e);
        }
        if (sender.Equals(btn_print_card))
        {
          btn_print_card_Click(sender, e);
        }
        if (sender.Equals(btn_handpays))
        {
          btn_handpays_Click(sender, e);
        }
        // LTC 22-FEB-2016
        if (sender.Equals(btn_disputes))
        {
          btn_disputes_Click(sender, e);
        }
        if (sender.Equals(btn_gifts))
        {
          btn_gifts_Click(sender, e);
        }
        if (sender.Equals(btn_cancel_promo))
        {
          btn_cancel_promo_Click(sender, e);
        }
        if (sender.Equals(btn_sell_chips))
        {
          btn_sell_chips_Click(sender, e);
        }
        if (sender.Equals(btn_buy_chips))
        {
          btn_buy_chips_Click(sender, e);
        }
        if (sender.Equals(btn_chips_sale_register))
        {
          btn_chips_sale_register_Click(sender, e);
        }
        if (sender.Equals(btn_transfer_credit))
        {
          this.btn_transfer_credit_Click(sender, e);
        }
        if (sender.Equals(btn_cash_advance))
        {
          this.btn_cash_advance_Click(sender, e);
        }
        if (sender.Equals(btn_undo_operation))
        {
          btn_undo_operation_Click(sender, e);
        }
        if (sender.Equals(btn_win_loss_statement_request))
        {
          btn_win_loss_statement_request_Click(sender, e);
        }
        if (sender.Equals(btn_reserved))
        {
          ((Button)sender).Enabled = false;
          btn_reserved_Click(sender, e);
        }

        if (sender.Equals(btn_player_browse))
        {
          _is_track_valid = btn_player_browse_Click(sender, e);

          EnableDisableButtons(_is_track_valid);

          return;
        }

        _enabled_buttons_after_operation = WSI.Common.Misc.ReadGeneralParams("Cashier", "KeepEnabledButtonsAfterOperation") == "1" ? true : false;

        // RCI & JBP 14-MAY-2013: Check if there is a visible account to enable buttons.
        if (_enabled_buttons_after_operation && !String.IsNullOrEmpty(visible_track_number))
        {
          tick_count_tracknumber_read = 0;
          EnableDisableButtons(true);
        }
        else
        {
          EnableDisableButtons(false);
        }
      }
      finally
      {
        Cashier.ResetAuthorizedByUser();

        if (!uc_card_reader1.Focused)
        {
          uc_card_reader1.Focus();
        }

        GC.Collect();
        ThreadPool.QueueUserWorkItem(delegate
        {
          GC.WaitForPendingFinalizers();
          GC.Collect();
        });

      }
    }

    private void btn_cancel_promo_Click(object sender, EventArgs e)
    {
      CardData _card_data;
      Currency _amount;

      _card_data = account_watcher.Card;

      form_yes_no.Show(this.ParentForm);

      try
      {
        form_credit.Show(Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_031"), out _amount, VoucherTypes.CancelPromotions, _card_data, 0, CASH_MODE.TOTAL_REDEEM, form_yes_no);
      }
      finally
      {
        form_yes_no.Hide();
        this.RestoreParentFocus();
      }
    } // btn_button_clicked

    private void btn_new_card_Click(object sender, EventArgs e)
    {
      String error_str;
      CurrencyExchangeResult _exchange_result;

      Cashier.ResetAuthorizedByUser();

      try
      {
        // Check if current user is an authorized user
        if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.AccountAsociateCard, ProfilePermissions.TypeOperation.RequestPasswd, this.ParentForm, out error_str))
        {
          return;
        }

        frm_card_assign card_assign = new frm_card_assign();
        frm_yesno shader = new frm_yesno();

        shader.Opacity = 0.6f;
        shader.Show();

        // RCI 20-APR-2012: Use AccountWatcher instead of the account label text.
        card_assign.CardAssignParams(account_watcher.Card, 0, visible_track_number, out _exchange_result);
        card_assign.ShowDialog(shader);

        shader.Hide();
        shader.Dispose();

        card_assign.Hide();
        card_assign.Dispose();

        RefreshAccount();
      }
      finally
      {
        Cashier.ResetAuthorizedByUser();
      }
    } // btn_new_card_Click

    //------------------------------------------------------------------------------
    // PURPOSE: Evente fired when user press button for desasociate the actual card 
    //          with the account (Recycle card)    
    //          
    // 
    //  PARAMS:
    //      - INPUT:
    //      - OUTPUT:
    // RETURNS:
    //   NOTES:
    //

    private void btn_recycle_card_Click(object sender, EventArgs e)
    {
      DialogResult _rc;
      String _error_str;
      CardData _card;
      String _message;

      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.RecyleCard,
                                               ProfilePermissions.TypeOperation.RequestPasswd, this.ParentForm, out _error_str))
      {
        return;
      }
      _message = Resource.String("STR_RECYCLE_CARD_ALERT");
      _message = _message.Replace("\\r\\n", "\r\n");

      _rc = frm_message.Show(_message, Resource.String("STR_RECYCLE_CARD_TITLE"),
                              MessageBoxButtons.YesNo, MessageBoxIcon.Warning, ParentForm);

      switch (_rc)
      {
        case DialogResult.OK:
        case DialogResult.Yes:
          break;
        default:
          return;
      }

      //Recycle

      _card = account_watcher.Card;

      if (!CashierBusinessLogic.DB_CardRecycle(_card.TrackData, _card.AccountId))
      {
        frm_message.Show(Resource.String("STR_APP_GEN_ERROR_DATABASE_ERROR_WRITING"), Resource.String("STR_APP_GEN_MSG_ERROR"),
                         MessageBoxButtons.OK, MessageBoxIcon.Error, this.ParentForm);
        return;
      }

      RefreshAccount();

      m_current_track_number = CardData.RECYCLED_TRACK_DATA;

    } //btn_recycle_card_Click

    private void btn_gifts_Click(object sender, EventArgs e)
    {
      CardData _card_data = new CardData();
      frm_account_points _account_points;

      if (m_current_card_id == 0)
      {
        return;
      }

      // Get Card data
      _card_data = account_watcher.Card;
      // Points Program checkings
      //      - Only available for non anonymous cards
      //      - Only if the account has points to exchange

      //      - Only available for non anonymous cards
      if (_card_data.PlayerTracking.HolderName == "")
      {
        string _message;
        string[] _message_params = { "", "", "", "", "", "" };

        _message_params[0] = _card_data.AccountId.ToString();

        _message = Resource.String("STR_FRM_ACCOUNT_POINTS_014", _message_params);
        _message = _message.Replace("\\r\\n", "\r\n");

        frm_message.Show(_message,
                          Resource.String("STR_APP_GEN_MSG_WARNING"),
                          MessageBoxButtons.OK,
                          MessageBoxIcon.Warning,
                          ParentForm);

        return;
      }

      _account_points = new frm_account_points(_card_data);
      _account_points.Show(this.ParentForm);
      _account_points.Dispose();

      return;
    } // btn_gifts_Click

    private void btn_handpays_Click(object sender, EventArgs e)
    {
      CardData _card_data = new CardData();
      frm_handpays _handpays;

      if (m_current_card_id == 0)
      {
        return;
      }

      // Get Card data
      _card_data = account_watcher.Card;

      _handpays = new frm_handpays(_card_data);
      _handpays.Show(this.ParentForm);
      _handpays.Dispose();
    }

    // LTC 22-FEB-2016
    /// <summary>
    /// Call frm_handpay_action form for setting the payment
    /// </summary>
    /// <param name="sender">Object Button</param>
    /// <param name="e">event</param>
    private void btn_disputes_Click(object sender, EventArgs e)
    {
      CardData _card_data = new CardData();
      frm_handpay_action _handpayaction;

      if (m_current_card_id == 0)
      {
        return;
      }

      // Get Card data
      _card_data = account_watcher.Card;

      // DLM 12-JUN-2018 
      //_handpayaction = new frm_handpay_action(frm_handpay_action.ENTRY_TYPE.MANUAL, 0, _card_data, true);
      _handpayaction = new frm_handpay_action(frm_handpay_action.ENTRY_TYPE.MANUAL, 0, _card_data, null, null, null, this, true);
      _handpayaction.Show(this.ParentForm);
      _handpayaction.Dispose();
    }

    //------------------------------------------------------------------------------
    // PURPOSE: High level handler for the MB Browse functionality (select from the 
    //          list of mobile banks associated to the current cashier session). 
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //

    private Boolean btn_player_browse_Click(object sender, EventArgs e)
    {
      Int64 _selected_account_id;
      Boolean _is_track_valid = true;
      Cursor _previous_cursor;
      String _error_str;

      _selected_account_id = -1;

      m_swipe_card = false;

      // Check if current user is an authorized user
      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.AccountSearchByName,
                                               ProfilePermissions.TypeOperation.RequestPasswd,
                                               this.ParentForm,
                                               out _error_str))
      {
        return false;
      }

      _previous_cursor = this.Cursor;

      try
      {
        this.Cursor = Cursors.WaitCursor;

        _selected_account_id = form_search_players.Show(this.ParentForm);

        if (_selected_account_id < 0)
        {
          return false;
        }

        // Load card data
        uc_card_reader1_OnTrackNumberReadEvent(_selected_account_id, ref _is_track_valid);

        return _is_track_valid;
      }
      finally
      {
        this.Cursor = _previous_cursor;
      }
    } // btn_player_browse_Click 

    void btn_vouchers_space_Click(object sender, EventArgs e)
    {
      CardData _card_data = new CardData();
      frm_voucher_space vs = new frm_voucher_space();

      _card_data = account_watcher.Card;

      try
      {
        form_yes_no.Show(ParentForm);
        vs.Show(_card_data, form_yes_no);
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
      finally
      {
        form_yes_no.Hide();
        vs.Dispose();
        uc_card_reader1.Focus();
      }
    } // btn_vouchers_space_Click

    private void btn_win_loss_statement_request_Click(object sender, EventArgs e)
    {
      frm_win_loss_statement _frm_win_loss_statement;

      String error_str;
      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.WinLossStatementRequest, ProfilePermissions.TypeOperation.RequestPasswd, this.ParentForm, out error_str))
      {
        return;
      }

      _frm_win_loss_statement = new frm_win_loss_statement(account_watcher.Card);

      try
      {

        form_yes_no.Show(ParentForm);

        _frm_win_loss_statement.Show();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {
        form_yes_no.Hide();
        _frm_win_loss_statement.Dispose();
        uc_card_reader1.Focus();
      }
    } // btn_win_loss_statement_request_Click


    public void ConditionallyGenerateDrawTickets(OperationCode LastOpCode)
    {
      Boolean _generate;
      Int32 _flag;

      switch (LastOpCode)
      {
        case OperationCode.CASH_IN:
          if (!Int32.TryParse(WSI.Common.Misc.ReadGeneralParams("Cashier.DrawTicket", "AutomaticPrintAfterCashIn"), out _flag))
          {
            _flag = 0;
          }
          _generate = (_flag != 0);
          break;

        case OperationCode.CASH_OUT:
          if (!Int32.TryParse(WSI.Common.Misc.ReadGeneralParams("Cashier.DrawTicket", "AutomaticPrintAfterCashOut"), out _flag))
          {
            _flag = 0;
          }
          _generate = (_flag != 0);
          break;

        case OperationCode.DRAW_TICKET:
          _generate = true;
          break;

        default:
          _generate = false;
          break;
      }

      if (_generate)
      {
        GenerateDrawTickets(LastOpCode);
      }
    }

    private void GenerateDrawTickets(OperationCode LastOpCode)
    {
      Boolean _ok;
      DrawNumberList _awarded_numbers;
      DrawNumberList _real_awarded_numbers;
      Int64 _num_pending;
      String _message;
      DialogResult _yes_no;
      Boolean _show_available_draws;

      // RCI 17-JUN-2011: Possible General Param to indicate if show the available draws form.
      _show_available_draws = true;

      switch (LastOpCode)
      {
        case OperationCode.CASH_IN:
        case OperationCode.CASH_OUT:
        case OperationCode.DRAW_TICKET:
          break;

        default:
          Log.Warning("Unexpected Operation Code: " + LastOpCode + ".");

          return;
      }

      // Check For Draw Numbers
      _ok = DrawsCashier.GenerateTickets(account_watcher.Card, true, null, out _awarded_numbers);
      if (!_ok)
      {
        // Show error message: Unable to check available draws.
        if (LastOpCode == OperationCode.DRAW_TICKET)
        {
          frm_message.Show(Resource.String("STR_DRAW_TICKETS_MSG_001"), Resource.String("STR_DRAW_TICKETS_TITLE"),
                           MessageBoxButtons.OK, MessageBoxIcon.Error, this.ParentForm);
        }

        return;
      }

      if (_awarded_numbers.Count == 0)
      {
        // Show info message: No tickets
        if (LastOpCode == OperationCode.DRAW_TICKET)
        {
          frm_message.Show(Resource.String("STR_VOUCHER_ERROR_MSG_DRAW"), Resource.String("STR_DRAW_TICKETS_TITLE"),
                           MessageBoxButtons.OK, MessageBoxIcon.Information, this.ParentForm);
        }

        return;
      }

      if (_show_available_draws && LastOpCode == OperationCode.DRAW_TICKET)
      {
        // Make a list of draws and show total and pending in the period.

        try
        {
          form_yes_no.Show(this.ParentForm);

          if (!form_available_draws.Show(ref _awarded_numbers, form_yes_no))
          {
            return;
          }
        }
        finally
        {
          form_yes_no.Hide();
          this.RestoreParentFocus();
        }
      }
      else
      {
        _num_pending = _awarded_numbers.NumPendingNumbers;

        if (_num_pending == 0)
        {
          // Show info message: No pending numbers to print.
          if (LastOpCode == OperationCode.DRAW_TICKET)
          {
            frm_message.Show(Resource.String("STR_DRAW_TICKETS_MSG_002"), Resource.String("STR_DRAW_TICKETS_TITLE"),
                               MessageBoxButtons.OK, MessageBoxIcon.Information, this.ParentForm);
          }
          return;
        }

        // Ask permission to print.
        _yes_no = DialogResult.OK;
        if (LastOpCode == OperationCode.DRAW_TICKET)
        {
          _yes_no = frm_message.Show(Resource.String("STR_DRAW_TICKETS_MSG_005"), Resource.String("STR_DRAW_TICKETS_TITLE"),
                                     MessageBoxButtons.YesNo, MessageBoxIcon.Information, this.ParentForm);
        }
        if (_yes_no != DialogResult.OK)
        {
          return;
        }
      }

      // RCI 05-JUL-2011: Used to refresh CardData current balance, for the DrawTicket movement.
      RefreshAccount();

      // Create Tickets - For Draw Numbers
      _ok = DrawsCashier.GenerateTickets(account_watcher.Card, false, _awarded_numbers, out _real_awarded_numbers);
      if (!_ok)
      {
        if (_real_awarded_numbers.Count > 0)
        {
          // Error printing (Tickets have been saved)
          _message = Resource.String("STR_DRAW_TICKETS_MSG_006");
        }
        else
        {
          // Error generating numbers
          _message = Resource.String("STR_DRAW_TICKETS_MSG_007");
        }
        _message = _message.Replace("\\r\\n", "\r\n");
        frm_message.Show(_message, Resource.String("STR_DRAW_TICKETS_TITLE"),
                         MessageBoxButtons.OK, MessageBoxIcon.Error, this.ParentForm);

        return;
      }

      if (_real_awarded_numbers.NumPendingNumbers == 0)
      {
        // Show info message: No tickets
        if (LastOpCode == OperationCode.DRAW_TICKET)
        {
          frm_message.Show(Resource.String("STR_VOUCHER_ERROR_MSG_DRAW"), Resource.String("STR_DRAW_TICKETS_TITLE"),
                           MessageBoxButtons.OK, MessageBoxIcon.Information, this.ParentForm);
        }

        return;
      }

    } // GenerateDrawTickets

    private void btn_undo_operation_Click(object sender, EventArgs e)
    {
      AccountOperations.Operation _account_operation;
      CashierSessionInfo _cashier_session_info;
      String _str_message;
      String _str_error;
      DialogResult _dlg_rc;
      CardData _card_data;
      CurrencyIsoType _iso_type;
      Decimal _iso_amount;
      String _str_amount;
      String _str_national_currency;
      OperationUndo.UndoError _undo_error;
      ArrayList _voucher_list;
      List<frm_browse.ButtonType> _list_button;
      frm_browse.ButtonType _btn_type;
      OperationCode _selected_operation;
      Object _selected_value;
      String _caption;

      _selected_operation = OperationCode.CASH_IN;
      _caption = Resource.String("STR_UNDO_LAST_OPERATION");

      if (m_is_gaming_tables_enabled || m_is_cash_advance_enabled)
      {
        _list_button = new List<frm_browse.ButtonType>();

        _btn_type = new frm_browse.ButtonType();
        _btn_type.Description = Resource.String("STR_UNDO_LAST_OPERATION");
        _btn_type.ReturnedElement = OperationCode.CASH_IN;
        _list_button.Add(_btn_type);

        if (m_is_gaming_tables_enabled)
        {
          if (!m_integrated_chip_operation)  // LTC 28-MAR-2016
          {
            _btn_type = new frm_browse.ButtonType();
            _btn_type.Description = Resource.String("STR_UNDO_LAST_OPERATION_PURCHASE_CHIPS");
            _btn_type.ReturnedElement = OperationCode.CHIPS_PURCHASE;
            _btn_type.Enabled = !FeatureChips.IsIntegratedChipAndCreditOperations(account_watcher.Card.IsVirtualCard);
            _list_button.Add(_btn_type);
          }

          if (m_is_chips_sale_enabled)  // LTC 28-MAR-2016 
          {
            _btn_type = new frm_browse.ButtonType();
            _btn_type.Description = Resource.String("STR_UNDO_LAST_OPERATION_SALE_CHIPS");
            _btn_type.ReturnedElement = OperationCode.CHIPS_SALE;
            _list_button.Add(_btn_type);
          }
        }

        if (m_is_cash_advance_enabled)
        {
          _btn_type = new frm_browse.ButtonType();
          _btn_type.Description = Resource.String("STR_UNDO_LAST_OPERATION_CASH_ADVANCE");
          _btn_type.ReturnedElement = OperationCode.CASH_ADVANCE;
          _list_button.Add(_btn_type);
        }

        frm_browse _browser = new frm_browse(BrowserType.UndoOperation);
        try
        {
          if (!_browser.Show(_list_button, out _selected_value))
          {
            return;
          }
        }
        finally
        {
          _browser.Dispose();
        }

        _selected_operation = (OperationCode)_selected_value;
      }

      // Check if current user is an authorized user
      switch (_selected_operation)
      {
        case OperationCode.CASH_IN:
          if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.UndoLastOperation,
                                                   ProfilePermissions.TypeOperation.RequestPasswd, this.ParentForm, out _str_error))
          {
            return;
          }
          break;
        case OperationCode.CHIPS_PURCHASE:
          if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.UndoLastChipsPurchase,
                                               ProfilePermissions.TypeOperation.RequestPasswd, this.ParentForm, out _str_error))
          {
            return;
          }
          if (m_integrated_chip_operation)
          {
            _selected_operation = OperationCode.CHIPS_PURCHASE_WITH_CASH_OUT;
          }
          break;
        case OperationCode.CHIPS_SALE:
          if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.UndoLastChipsSale,
                                               ProfilePermissions.TypeOperation.RequestPasswd, this.ParentForm, out _str_error))
          {
            return;
          }
          if (m_integrated_chip_operation)
          {
            _selected_operation = OperationCode.CHIPS_SALE_WITH_RECHARGE;
          }
          break;

        case OperationCode.CASH_ADVANCE:
          if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.UndoLastCashAdvance,
                                                ProfilePermissions.TypeOperation.RequestPasswd, this.ParentForm, out _str_error))
          {
            return;
          }
          break;

        default:
          return;
      }

      _card_data = account_watcher.Card;
      _cashier_session_info = Cashier.CashierSessionInfo();

      if (!OperationUndo.GetLastReversibleOperation(_selected_operation, _card_data, _cashier_session_info.CashierSessionId, out _account_operation,
                                                    out _iso_type, out _iso_amount, out _undo_error))
      {
        _str_error = OperationUndo.GetErrorMessage(_undo_error);

        frm_message.Show(_str_error, Resource.String("STR_APP_GEN_MSG_ERROR"),
                         MessageBoxButtons.OK, MessageBoxIcon.Error, this.ParentForm);

        return;
      }

      _str_message = "";

      switch (_selected_operation)
      {
        case OperationCode.CHIPS_PURCHASE:
        case OperationCode.CHIPS_PURCHASE_WITH_CASH_OUT:

          _caption = Resource.String("STR_UNDO_LAST_OPERATION_PURCHASE_CHIPS");
          _str_message = Resource.String("STR_UNDO_PURCHASE_CHIPS", ((Currency)_iso_amount).ToString());
          break;

        case OperationCode.CASH_IN:
        case OperationCode.CHIPS_SALE:
        case OperationCode.CHIPS_SALE_WITH_RECHARGE:
        case OperationCode.CASH_ADVANCE:

          // Terminal Draw - LetCancelRechargeWithPendingDraw
          if (_selected_operation == OperationCode.CASH_IN)
          {
            if (GeneralParam.GetBoolean("CashDesk.Draw.02", "Enabled", false) && !GeneralParam.GetBoolean("CashDesk.Draw.02", "LetCancelRechargeWithPendingDraw", true))
            {
              if (TerminalDraw.CheckExistsTerminalDrawRechargeByAccountId(_card_data.AccountId))
              {
                frm_message.Show(Resource.String("STR_TERMINAL_DRAW_UNDO_RECHARGE_WARNING"), 
                                 Resource.String("STR_APP_GEN_MSG_ERROR"),
                                 MessageBoxButtons.OK, MessageBoxIcon.Error, this.ParentForm);

                return;
              }
            }
          }

          _str_national_currency = CurrencyExchange.GetNationalCurrency();

          // Check if it's national currency
          if (_iso_type.IsoCode == _str_national_currency || String.IsNullOrEmpty(_iso_type.IsoCode))
          {
            _str_amount = ((Currency)_iso_amount).ToString();
          }
          else
          {
            _str_amount = Currency.Format(_iso_amount, _iso_type.IsoCode);
          }

          // Check if type is not Currency
          switch (_iso_type.Type)
          {
            case CurrencyExchangeType.CARD:
              _str_amount += " (" + Resource.String("STR_FRM_AMOUNT_INPUT_CREDIT_CARD_PAYMENT") + ")";
              break;

            case CurrencyExchangeType.CHECK:
              _str_amount += " (" + Resource.String("STR_FRM_AMOUNT_INPUT_CHECK_PAYMENT") + ")";
              break;

            default:
              break;
          }

          switch (_selected_operation)
          {
            case OperationCode.CASH_IN:
              _str_message = Resource.String("STR_UNDO_RECHARGE", _str_amount);
              break;

            case OperationCode.CHIPS_SALE:
            case OperationCode.CHIPS_SALE_WITH_RECHARGE:
              _caption = Resource.String("STR_UNDO_LAST_OPERATION_SALE_CHIPS");
              _str_message = Resource.String("STR_UNDO_SALE_CHIPS", _str_amount);
              break;

            case OperationCode.CASH_ADVANCE:
              _caption = Resource.String("STR_UNDO_LAST_OPERATION_CASH_ADVANCE");
              _str_message = Resource.String("STR_UNDO_CASH_ADVANCE", _str_amount);
              break;
          }

          break;
      }

      // Show information message
      _str_message = _str_message.Replace("\\r\\n", "\r\n");
      _dlg_rc = frm_message.Show(_str_message,
                                 _caption,
                                 MessageBoxButtons.YesNo, Images.CashierImage.Question, Cashier.MainForm);

      if (_dlg_rc != DialogResult.OK)
      {
        return;
      }

      account_watcher.RefreshAccount(_card_data);
      _card_data = account_watcher.Card;

      OperationUndo.InputUndoOperation _params = new OperationUndo.InputUndoOperation();
      _params.CodeOperation = _selected_operation;
      _params.CardData = _card_data;
      _params.OperationIdForUndo = _account_operation.OperationId;
      _params.CashierSessionInfo = _cashier_session_info;

      if (!OperationUndo.UndoOperation(_params, out _voucher_list))
      {
        switch (_selected_operation)
        {
          case OperationCode.CASH_IN:
            _str_message = Resource.String("STR_UNDO_RECHARGE_ERROR");
            break;

          case OperationCode.CHIPS_PURCHASE:
          case OperationCode.CHIPS_PURCHASE_WITH_CASH_OUT:
            _str_message = Resource.String("STR_UNDO_PURCHASE_CHIPS_ERROR");
            break;

          case OperationCode.CHIPS_SALE:
          case OperationCode.CHIPS_SALE_WITH_RECHARGE:
            _str_message = Resource.String("STR_UNDO_SALE_CHIPS_ERROR");
            break;

          case OperationCode.CASH_ADVANCE:
            _str_message = Resource.String("STR_UNDO_CASH_ADVANCE_ERROR");
            break;
        }

        frm_message.Show(_str_message, Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this.ParentForm);

        return;
      }

      // Print vouchers
      VoucherPrint.Print(_voucher_list);

    } // btn_undo_operation_Click


    /// <summary>
    /// UndoCreditCard transactions for TPV opeartions
    /// </summary>
    /// <param name="Card_data"></param>
    /// <param name="OperationId"></param>
    /// <param name="VoucherList"></param>
    /// <returns></returns>
    public static Boolean UndoCreditCardTPV(OperationCode Operation_Code, CardData Card_data, Int64 OperationId, out ArrayList VoucherList)
    {
      OperationUndo.InputUndoOperation _params = new OperationUndo.InputUndoOperation();
      _params.CodeOperation = Operation_Code;
      _params.CardData = Card_data;
      _params.OperationIdForUndo = OperationId;
      _params.CashierSessionInfo = Cashier.CashierSessionInfo();
      _params.ForceUndoTPV = true;

      if (!OperationUndo.UndoOperation(_params, out VoucherList))
      {
        return false;
      }

      return true;
    } //UndoCreditCardTPV


    public Boolean CheckPermissionsToRedeem(Int64 AccountId, Currency TotalToPay, Currency PrizeAmount,
                                            List<ProfilePermissions.CashierFormFuncionality> PermissionList)
    {
      Int32 _max_allowed_int;
      Currency _max_allowed;
      Currency _min_limit_exceded;
      String _message;
      ENUM_ANTI_MONEY_LAUNDERING_LEVEL _aml_level;
      Boolean _threshold_crossed;

      _min_limit_exceded = -1;

      // ACM 14-MAY-2013:  Check MaxAllowedCashOut.
      _max_allowed_int = GeneralParam.GetInt32("Cashier", "MaxAllowedCashOut");
      _max_allowed = _max_allowed_int;

      if (_max_allowed > 0 && TotalToPay > _max_allowed)
      {
        _min_limit_exceded = _max_allowed;
        PermissionList.Add(ProfilePermissions.CashierFormFuncionality.CardSubtractCreditMaxAllowed);
      }

      // Show popup for max allowed exceded.
      if (_min_limit_exceded != -1)
      {
        _message = Resource.String("STR_CARD_SUBTRACT_CREDIT_MAX_EXCEDED", TotalToPay.ToString(), _min_limit_exceded.ToString());
        _message = _message.Replace("\\r\\n", "\r\n");

        if (frm_message.Show(_message, Resource.String("STR_APP_GEN_MSG_WARNING"),
            MessageBoxButtons.YesNo, Images.CashierImage.Question, Cashier.MainForm) != DialogResult.OK)
        {
          return false;
        }
      }

      if (!CheckAntiMoneyLaunderingToRedeem(AccountId, ENUM_CREDITS_DIRECTION.Redeem, PrizeAmount, out _aml_level, out _threshold_crossed))
      {
        return false;
      }

      // DLL & RCI 28-OCT-2013: AML: Add permission to exceed report limit.
      if (frm_amount_input.AML_RequestPermissionToExceedReportLimit(ENUM_CREDITS_DIRECTION.Redeem, _aml_level, _threshold_crossed))
      {
        if (_aml_level == ENUM_ANTI_MONEY_LAUNDERING_LEVEL.MaxAllowed)
        {
          PermissionList.Add(ProfilePermissions.CashierFormFuncionality.AML_ExceedMaximumLimitPrize);
        }
        else
        {
          PermissionList.Add(ProfilePermissions.CashierFormFuncionality.AML_ExceedReportLimitPrize);
        }
      }

      return true;
    } // CheckPermissionsToRedeem

    private void btn_buy_chips_Click(object sender, EventArgs e)
    {
      CardData _card_data;
      FeatureChips.ChipsOperation _chips_amount;
      ParamsTicketOperation _operations_params;
      String _error_str;
      Boolean _integrated_chip_operation;

      // Check if current user is an authorized user
      if (!ProfilePermissions.CheckPermissionList(ProfilePermissions.CashierFormFuncionality.ChipsPurchase,
                                                  ProfilePermissions.TypeOperation.RequestPasswd, this.ParentForm, 0, out _error_str))
      {
        return;
      }

      // Get Card data
      if (m_current_card_id == 0)
      {
        return;
      }

      if (!m_swipe_card)
      {
        return;
      }

      _card_data = account_watcher.Card;
      if (m_current_card_id != _card_data.AccountId)
      {
        return;
      }

      try
      {
        form_yes_no.Show(this.ParentForm);

        _integrated_chip_operation = FeatureChips.IsIntegratedChipAndCreditOperations(_card_data.IsVirtualCard);

        PaymentThresholdAuthorization _payment_threshold_authorization;

        if (!CashierChips.ChipsPurchase_GetAmount(_card_data, this.CheckPermissionsToRedeem, form_yes_no, out _chips_amount, out _operations_params, out _payment_threshold_authorization))
        {
          return;
        }

        if (_integrated_chip_operation)
        {
          // DRV & RCI 20-AUG-2014: Read all data, because is a purchase with cash out.
          if (!CardData.DB_CardGetAllData(_card_data.AccountId, _card_data))
          {
            return;
          }
        }
        else
        {
          account_watcher.RefreshAccount(_card_data);
          _card_data = account_watcher.Card;
        }

        // If exped is active, we need to request authorization to refund
        if (ExternalPaymentAndSaleValidationCommon.ExpedMode == EnumExternalPaymentAndSaleValidationMode.MODE_EXPED)
        {
          OperationCode _operation_code;
          _operation_code = _integrated_chip_operation ? OperationCode.CHIPS_PURCHASE_WITH_CASH_OUT : OperationCode.CHIPS_PURCHASE;

          if (!ExternalWS.ExpedConnect(_card_data.AccountId, _chips_amount.ChipsAmount, _operation_code))
          {
            if (ExternalPaymentAndSaleValidationCommon.ExpedMode != EnumExternalPaymentAndSaleValidationMode.MODE_EXPED
                || Misc.ExternalWS.OutputParams.ValidationResult != EnumExternalPaymentAndSaleValidationResult.RESULT_AUTHORIZED)
            {
              frm_message.Show(ExternalPaymentAndSaleValidationCommon.GetErrorMessageFromExternalProvider(_card_data.AccountId, _card_data.PlayerTracking.HolderName, _chips_amount.ChipsAmount),
                             Resource.String("STR_APP_GEN_MSG_ERROR"),
                             MessageBoxButtons.OK,
                             MessageBoxIcon.Error,
                             this.ParentForm);
            }
            return;
          }
          if (Misc.ExternalWS.OutputParams.ValidationMessage == "cancel")
          {
            return;
          }
        }

        _operations_params.in_window_parent = this.ParentForm;
        if (!CashierChips.ChipsPurchase_Save(_card_data,
                                             _chips_amount,
                                             _operations_params,
                                             Misc.ExternalWS.InputParams,
                                             Misc.ExternalWS.OutputParams,
                                             _payment_threshold_authorization,
                                             out _error_str))
        {
          if (!String.IsNullOrEmpty(_error_str))
          {
            _error_str += "\\r\\n" + "\\r\\n"; //We add a pair of blank lines to separate messages
          }

          _error_str += Resource.String("STR_GAMING_TABLE_BUY_CHIPS_ERROR"); //Add general error

          _error_str = _error_str.Replace("\\r\\n", "\r\n");

          frm_message.Show(_error_str
                         , Resource.String("STR_APP_GEN_MSG_ERROR")
                         , MessageBoxButtons.OK
                         , Images.CashierImage.Error
                         , ParentForm);
          return;
        }
      }
      finally
      {
        form_yes_no.Hide();
        this.RestoreParentFocus();
      }

      RefreshAccount();

    } // btn_buy_chips_Click

    private void btn_chips_sale_register_Click(object sender, EventArgs e)
    {
      CardData _card_data;
      FeatureChips.ChipsOperation _chips_amount;
      String _error_str;
      MessageBoxIcon _message_icon;
      CashierOperations.TYPE_CARD_CASH_IN _cash_operation;
      CurrencyExchangeResult _exchange_result;
      Boolean _ok;
      RechargeOutputParameters OutputParameter; //LTC 22-SEP-2016
      Ticket _ticket_copy_dealer;

      _cash_operation = new CashierOperations.TYPE_CARD_CASH_IN();
      _exchange_result = new CurrencyExchangeResult();
      _ticket_copy_dealer = null;

      // Check if current user is an authorized user
      if (!ProfilePermissions.CheckPermissionList(ProfilePermissions.CashierFormFuncionality.ChipsSaleRegister,
                                                  ProfilePermissions.TypeOperation.RequestPasswd, this.ParentForm, 0, out _error_str))
      {
        return;
      }

      // Get Card data
      if (m_current_card_id == 0)
      {
        return;
      }

      if (!m_swipe_card)
      {
        return;
      }

      _card_data = account_watcher.Card;
      if (m_current_card_id != _card_data.AccountId)
      {
        return;
      }

      try
      {
        form_yes_no.Show(this.ParentForm);

        _ok = false;

        _ok = CashierChips.ChipsSaleRegister_GetAmount(_card_data, form_credit, VoucherTypes.ChipsSaleWithRecharge, form_yes_no, out _chips_amount, out _cash_operation, out _exchange_result);

        if (!_ok)
        {
          return;
        }

        account_watcher.RefreshAccount(_card_data);
        _card_data = account_watcher.Card;

        _ok = CashierChips.ChipsSaleWithRecharge_Save(_card_data, form_yes_no, _chips_amount, _cash_operation, _exchange_result, ChipSaleType.RegisterTableSale, out _error_str, out OutputParameter, ParticipateInCashDeskDraw.No, out _ticket_copy_dealer);

        if (!_ok)
        {
          _message_icon = MessageBoxIcon.Warning;

          // Generic error
          if (_error_str == String.Empty)
          {
            _error_str = Resource.String("STR_UC_CARD_USER_MSG_GENERIC_ERROR");
            _message_icon = MessageBoxIcon.Error;
          }

          frm_message.Show(_error_str,
                            Resource.String("STR_APP_GEN_MSG_WARNING"),
                            MessageBoxButtons.OK,
                            _message_icon,
                            ParentForm);

          return;
        }
      }
      finally
      {
        form_yes_no.Hide();
        this.RestoreParentFocus();
      }

      RefreshAccount();

    } // btn_chips_sale_register_Click

    private void btn_sell_chips_Click(object sender, EventArgs e)
    {
      CardData _card_data;
      FeatureChips.ChipsOperation _chips_amount;
      String _error_str;
      MessageBoxIcon _message_icon;
      Boolean _chips_sale_mode;
      CashierOperations.TYPE_CARD_CASH_IN _cash_operation;
      CurrencyExchangeResult _exchange_result;
      Boolean _ok;
      Result _cash_desk_draw_result;
      RechargeOutputParameters _outputParameter;
      ParticipateInCashDeskDraw _participate_in_cash_draw;
      CHIPS_SALE_MODE _chips_sale_mode_select;
      Ticket _ticket_copy_dealer;
      PaymentThresholdAuthorization _payment_threshold_authorization;

      _cash_operation = new CashierOperations.TYPE_CARD_CASH_IN();
      _exchange_result = new CurrencyExchangeResult();
      _outputParameter = new RechargeOutputParameters();
      _chips_sale_mode_select = CHIPS_SALE_MODE.GP_CONFIGURATION;
      _ticket_copy_dealer = null;

      // Check if current user is an authorized user
      if (!ProfilePermissions.CheckPermissionList(ProfilePermissions.CashierFormFuncionality.ChipsSale,
                                                  ProfilePermissions.TypeOperation.RequestPasswd, this.ParentForm, 0, out _error_str))
      {
        return;
      }

      _chips_sale_mode = GeneralParam.GetBoolean("GamingTables", "Cashier.Mode");

      //if (!Chips.HasChipsBalance(Cashier.SessionId, _chips_sale_mode))
      //{
      //  frm_message.Show(Resource.String("STR_FRM_AMOUNT_INPUT_MSG_CHIPS_AMOUNT_EXCEED_BANK").Replace("\\r\\n", "\r\n"),
      //          Resource.String("STR_APP_GEN_MSG_WARNING"),
      //          MessageBoxButtons.OK,
      //          MessageBoxIcon.Warning,
      //          ParentForm);

      //  return;
      //}

      // Get Card data
      if (m_current_card_id == 0)
      {
        return;
      }

      if (!m_swipe_card)
      {
        return;
      }

      _card_data = account_watcher.Card;
      if (m_current_card_id != _card_data.AccountId)
      {
        return;
      }

      try
      {

        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (!GamingTablesSessions.GetGamingTableInfo((Int32)Cashier.TerminalId, _db_trx.SqlTransaction))
          {
            return;
          }
        }
        
        form_yes_no.Show(this.ParentForm);

        _ok = false;

        VoucherTypes _voucher_type;

        _voucher_type = VoucherTypes.ChipsSaleAmountInput;

        if (FeatureChips.CashierChipsSaleMode(_card_data.IsVirtualCard))
        {
          _chips_sale_mode_select = frm_message.ShowChipsSaleModeSelect(this.ParentForm);
        }

        switch (_chips_sale_mode_select)
        {
          case CHIPS_SALE_MODE.GP_CONFIGURATION:

            _chips_sale_mode_select = m_integrated_chip_operation ? CHIPS_SALE_MODE.INTEGRATED_RECHARGE : CHIPS_SALE_MODE.FROM_ACCOUNT;
        _voucher_type = m_integrated_chip_operation ? VoucherTypes.ChipsSaleWithRecharge : VoucherTypes.ChipsSaleAmountInput;

            break;

          case CHIPS_SALE_MODE.INTEGRATED_RECHARGE:

            _voucher_type = VoucherTypes.ChipsSaleWithRecharge;

            break;

          case CHIPS_SALE_MODE.FROM_ACCOUNT:

            _voucher_type = VoucherTypes.ChipsSaleAmountInput;
            break;

          case CHIPS_SALE_MODE.NONE:
            return;
          default:
            break;
        }

        _ok = CashierChips.ChipsSale_GetAmount(_card_data, form_credit, _voucher_type, GamingTablesSessions.GamingTableInfo.GamingTableId, form_yes_no, out _chips_amount, out _cash_operation, out _exchange_result, out _participate_in_cash_draw, out _payment_threshold_authorization);

        if (!_ok)
        {
          return;
        }

        account_watcher.RefreshAccount(_card_data);
        _card_data = account_watcher.Card;

        if (_chips_sale_mode_select == CHIPS_SALE_MODE.INTEGRATED_RECHARGE)
        {
          // If exped is active, we need to request authorization to refund
          if (ExternalPaymentAndSaleValidationCommon.ExpedMode == EnumExternalPaymentAndSaleValidationMode.MODE_EXPED)
          {
            if (!ExternalWS.ExpedConnect(m_card_data.AccountId, _chips_amount.AmountNationalCurrency, OperationCode.CHIPS_SALE_WITH_RECHARGE))
            {

              if (ExternalPaymentAndSaleValidationCommon.ExpedMode != EnumExternalPaymentAndSaleValidationMode.MODE_EXPED
                  || Misc.ExternalWS.OutputParams.ValidationResult != EnumExternalPaymentAndSaleValidationResult.RESULT_AUTHORIZED)
              {
                frm_message.Show(ExternalPaymentAndSaleValidationCommon.GetErrorMessageFromExternalProvider(_card_data.AccountId, _card_data.PlayerTracking.HolderName, _chips_amount.ChipsAmount),
                               Resource.String("STR_APP_GEN_MSG_ERROR"),
                               MessageBoxButtons.OK,
                               MessageBoxIcon.Error,
                               m_parent);
                return;
              }
            }
            if (Misc.ExternalWS.OutputParams.ValidationMessage == "cancel")
            {
              return;
            }
          }

          _ok = CashierChips.ChipsSaleWithRecharge_Save(_card_data, form_yes_no, _chips_amount, _cash_operation, _exchange_result, ChipSaleType.CashDeskSale, out _error_str, out _outputParameter, _participate_in_cash_draw, out _ticket_copy_dealer);

          if (!_ok && _outputParameter.ErrorCode == RECHARGE_ERROR_CODE.ERROR_RECHARGE_WITH_CREDIT_LINE)
          {
            return;
          }
        }
        else
        {
          _ok = CashierChips.ChipsSale_Save(_card_data, form_yes_no, _chips_amount, ChipSaleType.CashDeskSale, out _error_str, out _ticket_copy_dealer);
        }

        if (!_ok)
        {
          _message_icon = MessageBoxIcon.Warning;

          // Generic error
          if (_error_str == String.Empty)
          {
            _error_str = Resource.String("STR_UC_CARD_USER_MSG_GENERIC_ERROR");
            _message_icon = MessageBoxIcon.Error;
          }

          frm_message.Show(_error_str,
                            Resource.String("STR_APP_GEN_MSG_WARNING"),
                            MessageBoxButtons.OK,
                            _message_icon,
                            ParentForm);

          return;
        }
        //LTC 22-SEP-2016
        _cash_desk_draw_result = _outputParameter.CashDeskDrawResult;

        if (CashDeskDrawBusinessLogic.IsEnabledPreviewCashDeskDraws(Convert.ToInt16(CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION.DRAW_01))
              && _cash_desk_draw_result != null
              && _cash_desk_draw_result.m_bet_numbers != null
              && _cash_desk_draw_result.m_winning_combination != null)
        {
          frm_preview_cash_desk _preview_cash_desk = new frm_preview_cash_desk(_cash_desk_draw_result);
          //form_yes_no.Show(this.ParentForm);

          try
          {
            _preview_cash_desk.ShowDialog();
          }
          finally
          {
            // ICS 14-APR-2014 Free the object
            _preview_cash_desk.Dispose();

            form_yes_no.Hide();
            this.RestoreParentFocus();
          }
        }

      }
      finally
      {
        form_yes_no.Hide();
        this.RestoreParentFocus();
      }

      RefreshAccount();

      DialogResult _dlg_rc;

      _dlg_rc = DialogResult.Cancel;
      
      // Only cashier with gamingTable integrated or linked
      if (GamingTableBusinessLogic.IsEnableCashDeskDealerTicketsValidation() && GamingTableBusinessLogic.IsGamingTablesEnabled()
        && (GamingTablesSessions.GamingTableInfo.IsGamingTable && GamingTablesSessions.GamingTableInfo.GamingTableId > 0 || GamingTablesSessions.GamingTableInfo.LinkedGamingTableId > 0) 
         && _ticket_copy_dealer != null)
      {
        Int32 _gaming_table_id;

        _gaming_table_id = 0;

        _dlg_rc = frm_message.Show(Resource.String("STR_CHIPS_SALE_AUTO_VALIDATE_CHIP_TICKET_MESSAGE"), Resource.String("STR_CHIPS_SALE_AUTO_VALIDATE_CHIP_TICKET_TITLE"), MessageBoxButtons.OKCancel, Cashier.MainForm);

        if (_dlg_rc == DialogResult.OK)
        {
          _gaming_table_id = GamingTablesSessions.GamingTableInfo.LinkedGamingTableId;

          if (GamingTablesSessions.GamingTableInfo.IsGamingTable && GamingTablesSessions.GamingTableInfo.GamingTableId > 0)
          {
            _gaming_table_id = GamingTablesSessions.GamingTableInfo.GamingTableId;
          }

          m_parent.SwitchToGamingTables(_gaming_table_id, _card_data, _ticket_copy_dealer);
        }
      }

    } // btn_sell_chips_Click

    //------------------------------------------------------------------------------
    // PURPOSE: Partial and Total redeem.  
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    private void btn_redeem_Click(object sender, EventArgs e)
    {
      frm_account_redeem _account_redeem;

      if (m_current_card_id == 0)
      {
        return;
      }

      if (!m_swipe_card)
      {
        return;
      }

      if (GeneralParam.GetBoolean("CashDesk.Draw.02", "Enabled", false) && !GeneralParam.GetBoolean("CashDesk.Draw.02", "LetWithdrawalWithPendingDraw", true))
      {
        if (TerminalDraw.CheckExistsTerminalDrawRechargeByAccountId(m_card_data.AccountId))
        {
          frm_message.Show(Resource.String("STR_TERMINAL_DRAW_WITHDRAWAL_WARNING"), 
                           Resource.String("STR_APP_GEN_MSG_ERROR"),
                           MessageBoxButtons.OK, MessageBoxIcon.Error, this.ParentForm);

        return;
      }
      }

      try
      {
        form_yes_no.Show(this.ParentForm);
        _account_redeem = new frm_account_redeem(account_watcher.Card, this.CheckPermissionsToRedeem, this.CheckAntiMoneyLaunderingToRedeem);
        _account_redeem.Show(form_yes_no);
        _account_redeem.Dispose();
      }
      finally
      {
        form_yes_no.Hide();
        this.RestoreParentFocus();
      }

      ConditionallyGenerateDrawTickets(OperationCode.CASH_OUT);

      RefreshAccount();

    } // btn_redeem_Click

    private void btn_transfer_credit_Click(object sender, EventArgs e)
    {
      frm_input_target_card _frm_input_target_card;

      // Check if current user is an authorized user
      string _error_str;
      Accounts.TRANSFER_CREDIT_RESULT _result;

      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.TransferAllRedeemableCredit, ProfilePermissions.TypeOperation.RequestPasswd, this.ParentForm, out _error_str))
      {
        return;
      }

      if (m_current_card_id == 0)
      {
        return;
      }

      if (!m_swipe_card)
      {
        return;
      }

      _result = Accounts.CanTransferCredit(account_watcher.Card);
      switch (_result)
      {
        case Accounts.TRANSFER_CREDIT_RESULT.TRANSFER_OK:
        case Accounts.TRANSFER_CREDIT_RESULT.ERROR_SOURCE_ACCOUNT_HAVE_PROMO:
          break;
        default:
          {
            _error_str = Accounts.TranslateTransferCreditResultToMessage(_result);
            frm_message.Show(_error_str, Resource.String("STR_APP_GEN_MSG_WARNING"), MessageBoxButtons.OK,
                             Images.CashierImage.Warning, form_yes_no);

            return;
          }
      }

      _error_str = Accounts.TranslateTransferCreditResultToMessage(Accounts.TRANSFER_CREDIT_RESULT.ERROR_UNKNOWN);

      _frm_input_target_card = new frm_input_target_card(account_watcher.Card);

      try
      {
        form_yes_no.Show(this.ParentForm);
        _result = _frm_input_target_card.Show(form_yes_no);

        _error_str = Accounts.TranslateTransferCreditResultToMessage(_result);
      }
      finally
      {
        _frm_input_target_card.Dispose();

        if (!String.IsNullOrEmpty(_error_str))
        {
          frm_message.Show(_error_str, Resource.String("STR_APP_GEN_MSG_WARNING"), MessageBoxButtons.OK,
                           Images.CashierImage.Warning, form_yes_no, false);
        }
        form_yes_no.Hide();
        this.RestoreParentFocus();
      }

    } // btn_transfer_credit_Click

    private void btn_cash_advance_Click(object sender, EventArgs e)
    {
      CardData _card_data;
      String _error_str;
      CurrencyExchangeResult _exchange_result;
      RechargeInputParameters _input_params;
      Currency _session_balance;
      PaymentThresholdAuthorization _payment_threshold_authorization;

      _exchange_result = new CurrencyExchangeResult();
      _session_balance = 0;

      // Check if current user is an authorized user
      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.CashAdvance,
                                                  ProfilePermissions.TypeOperation.RequestPasswd, this.ParentForm, out _error_str))
      {

        return;
      }

      // Get Card data
      if (m_current_card_id == 0)
      {
        return;
      }

      if (!m_swipe_card)
      {
        return;
      }

      _card_data = account_watcher.Card;
      if (m_current_card_id != _card_data.AccountId)
      {
        return;
      }

      try
      {
        form_yes_no.Show(this.ParentForm);

        using (DB_TRX _db_trx = new DB_TRX())
        {
          _session_balance = CashierBusinessLogic.UpdateSessionBalance(_db_trx.SqlTransaction, Cashier.SessionId, 0);
        }
        if (_session_balance <= 0)
        {
          frm_message.Show(Resource.String("STR_FRM_AMOUNT_INPUT_MSG_AMOUNT_EXCEED_BANK"),
                           Resource.String("STR_APP_GEN_MSG_WARNING"),
                           MessageBoxButtons.OK,
                           MessageBoxIcon.Error,
                           ParentForm);
          return;
        }

        frm_payment_method _frm_payment = new frm_payment_method(this.ParentForm);
        if (!_frm_payment.Show(out _exchange_result, out _payment_threshold_authorization, _card_data))
        {
          return;
        }

        _input_params = new RechargeInputParameters();
        _input_params.AccountId = _card_data.AccountId;
        _input_params.AccountLevel = _card_data.PlayerTracking.CardLevel;
        _input_params.CardPaid = _card_data.CardPaid;
        _input_params.CardPrice = Accounts.CardDepositToPay(_card_data);
        _input_params.ExternalTrackData = _card_data.TrackData;
        _input_params.VoucherAccountInfo = _card_data.VoucherAccountInfo();
        _input_params.IsChipsOperation = false;
        _input_params.ChipSaleRegister = false;

        //JBC 01/10/2014: We need to save Holder Name into bank transaction database.
        if (_exchange_result != null && _exchange_result.BankTransactionData != null)
        {
          _exchange_result.BankTransactionData.AccountHolderName = _card_data.PlayerTracking.HolderName;
        }

        if (!CashierBusinessLogic.DB_CashAdvance(_input_params, _exchange_result, _payment_threshold_authorization, out _error_str))
        {
          frm_message.Show(_error_str,
                           Resource.String("STR_APP_GEN_MSG_WARNING"),
                           MessageBoxButtons.OK,
                           MessageBoxIcon.Error,
                           ParentForm);
        }
      }
      finally
      {
        form_yes_no.Hide();
        this.RestoreParentFocus();
      }

      RefreshAccount();

    } // btn_cash_advance_Click

    /// <summary>
    /// Returns if there are buckets enabled
    /// </summary>
    /// <returns></returns>
    private Boolean HasBucketsEnabled()
    {
      BucketsForm _buckets_frm;

      _buckets_frm = new BucketsForm();

      return (_buckets_frm.HasBucketsEnabled());
    }

    private void btn_multiple_buckets_Click(object sender, EventArgs e)
    {
      //RAB 19-JAN-2016
      String error_str;

      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.SetBuckets,
                                                 ProfilePermissions.TypeOperation.RequestPasswd,
                                                 this.ParentForm,
                                                 out error_str))
      {
        return;
      }

      //FGB 11-OCT-2016 Check if there are buckets enabled
      if (!HasBucketsEnabled())
      {
        frm_message.Show(Resource.String("STR_BUCKET_THERE_ARE_NO_BUCKET_ENABLED")
                       , Resource.String("STR_APP_GEN_MSG_WARNING")
                       , MessageBoxButtons.OK
                       , MessageBoxIcon.Error
                       , this.ParentForm);
        return;
      }

      CardData _card_data = new CardData();
      frm_add_amount_multiple_buckets vs = new frm_add_amount_multiple_buckets();

      _card_data = m_card_data;

      try
      {
        form_yes_no.Show(ParentForm);
        vs.Show(_card_data, form_yes_no);
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
      finally
      {
        form_yes_no.Hide();
        vs.Dispose();
        uc_card_reader1.Focus();
      }
    }

    private void btn_change_currency_Click(object sender, EventArgs e)
    {
      CardData _card_data = account_watcher.Card;
      frm_currency_exchange frm_exchange = null;
      CurrencyExchangeResult _exchange_result;
      ExchangeMultiDenomination _exchange;
      VoucherMultiCurrencyExchange _voucher;

      if (!Misc.IsMultiCurrencyExchangeEnabled())
      {
        return;
      }

      try
      {
        form_yes_no.Show(this.ParentForm);

        if (_card_data.AccountId > 0)
        {
          if (!IsCustomerRegisteredInReception(_card_data))
          {
            return;
          }

          frm_exchange = new frm_currency_exchange(_card_data, m_parent);
          if (frm_exchange.ShowDialog() == DialogResult.OK)
          {
            _exchange_result = frm_exchange.GetExchangeResult;

            _exchange = new ExchangeMultiDenomination();

            // Validate cash in the cashier
            if (_exchange_result.NetAmount > _exchange.GetCashCashierCurrentBalance(_exchange_result.OutCurrencyCode))
            {
              frm_message.Show(Resource.String("STR_FRM_AMOUNT_INPUT_MSG_AMOUNT_EXCEED_BANK"), Resource.String("STR_APP_GEN_MSG_WARNING"),
                                               MessageBoxButtons.OK, MessageBoxIcon.Error, this.ParentForm);
              return;
            }

            if (_exchange.ChangeForeignCurrencyToNationalCurrency(Cashier.CashierSessionInfo(), _card_data, _exchange_result, out _voucher))
            {
              VoucherPrint.Print(_voucher);
            }
            else
            {
              frm_message.Show(Resource.String("STR_CURRENCY_EXCHANGE_GENERIC_ERROR_EXCHANGE"), Resource.String("STR_APP_GEN_MSG_ERROR"),
                                MessageBoxButtons.OK, Images.CashierImage.Error, this.ParentForm);
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        frm_message.Show(Resource.String("STR_CURRENCY_EXCHANGE_GENERIC_ERROR_EXCHANGE"), Resource.String("STR_APP_GEN_MSG_ERROR"),
                MessageBoxButtons.OK, Images.CashierImage.Error, this.ParentForm);
      }
      finally
      {
        form_yes_no.Hide();
        this.RestoreParentFocus();
      }
    }

    private void btn_devolution_currency_Click(object sender, EventArgs e)
    {
      CardData _card_data = account_watcher.Card;
      frm_currency_refund frm_refund = null;
      CurrencyExchangeResult _exchange_result = null;
      VoucherMultiCurrencyExchange _voucher;
      ExchangeMultiDenomination _exchange;

      if (!Misc.IsMultiCurrencyExchangeEnabled())
      {
        return;
      }

      try
      {
        form_yes_no.Show(this.ParentForm);

        if (_card_data.AccountId > 0)
        {
          if (!IsCustomerRegisteredInReception(_card_data))
          {
            return;
          }

          frm_refund = new frm_currency_refund(_card_data);
          if (frm_refund.ShowDialog() == DialogResult.OK)
          {
            _exchange_result = frm_refund.GetExchangeResult;

            _exchange = new ExchangeMultiDenomination();

            // Validate cash in the cashier
            if (_exchange_result.NetAmount > _exchange.GetCashCashierCurrentBalance(_exchange_result.OutCurrencyCode))
            {
              frm_message.Show(Resource.String("STR_FRM_AMOUNT_INPUT_MSG_AMOUNT_EXCEED_BANK"), Resource.String("STR_APP_GEN_MSG_WARNING"),
                                               MessageBoxButtons.OK, MessageBoxIcon.Error, this.ParentForm);
              return;
            }

            // Validate min value
            Currency _min_check_amount;
            Decimal min_amount = 0; // From a parameter generaral
            _min_check_amount = Math.Max(min_amount, 0.01m);
            if (_exchange_result.InAmount < _min_check_amount)
            {
              frm_message.Show(Resource.String("STR_FRM_AMOUNT_INPUT_MSG_ZERO_AMOUNT"), Resource.String("STR_APP_GEN_MSG_WARNING"),
                                               MessageBoxButtons.OK, MessageBoxIcon.Error, this.ParentForm);
              return;
            }

            // Devolution
            if (_exchange.ChangeNationalCurrencyToForeignCurrency(Cashier.CashierSessionInfo(), _card_data, _exchange_result, out _voucher))
            {
              VoucherPrint.Print(_voucher);
            }
            else
            {
              frm_message.Show(Resource.String("STR_CURRENCY_EXCHANGE_GENERIC_ERROR_DEVOLUTION"), Resource.String("STR_APP_GEN_MSG_ERROR"),
                 MessageBoxButtons.OK, Images.CashierImage.Error, this.ParentForm);
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        frm_message.Show(Resource.String("STR_CURRENCY_EXCHANGE_GENERIC_ERROR_DEVOLUTION"), Resource.String("STR_APP_GEN_MSG_ERROR"),
           MessageBoxButtons.OK, Images.CashierImage.Error, this.ParentForm);
      }
      finally
      {
        form_yes_no.Hide();
        this.RestoreParentFocus();
      }
    }

    private void btn_undo_last_exchange_currency_Click(object sender, EventArgs e)
    {
      String _str_error;

      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.UndoLastCurrencyExchange,
                                         ProfilePermissions.TypeOperation.RequestPasswd, this.ParentForm, out _str_error))
      {
        return;
      }

      UndoLastCurrencyExchange();
    }

    private void btn_undo_last_devolution_currency_Click(object sender, EventArgs e)
    {
      String _str_error;

      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.UndoLastCurrencyDevolution,
                                         ProfilePermissions.TypeOperation.RequestPasswd, this.ParentForm, out _str_error))
      {
        return;
      }

      UndoLastDevolution();
    }

    private void btn_undo_cash_advance_Click(object sender, EventArgs e)
    {
      if (Misc.IsMultiCurrencyExchangeEnabled())
      {
        UndoCardCash_CashAdvanceList();
      }
      else
      {
        // TODO: Call to btn_cash_advance_Click event (It depends of the UI design)
      }
    }

    private void btn_handpay_mico2_Click(object sender, EventArgs e)
    {
      String _error_str;

      try
      {

        if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.TITO_HandPay,
                                                 ProfilePermissions.TypeOperation.RequestPasswd,
                                                 this.ParentForm,
                                                 out _error_str))
        {
          return;
        }

        frm_TITO_handpays _TITO_handpays;

        _TITO_handpays = new frm_TITO_handpays();

        _TITO_handpays.Show(this.ParentForm);
        _TITO_handpays.Dispose();
        _TITO_handpays = null;
        this.RestoreParentFocus();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    }

    private void pb_user_Click(object sender, EventArgs e)
    {

      if (GeneralParam.GetBoolean("Account", "PlayerPicture.AllowZoom", true))
      {
        if (m_exist_photo)
        {
          frm_account_photo frm = new frm_account_photo(this.lbl_holder_name.Text, this.pb_user.Image);
          frm.ShowDialog();
        }
        else
        {
          CardData _card_data;
          String error_str;
          DialogResult _dgr_ac_edit;

          if (m_is_new_card)
          {
            
            _card_data = new CardData();
            _card_data.TrackData = m_current_track_number;
            account_watcher.RefreshAccount(_card_data);
            ClearCardData();
          }
          else
          {
            ////0 byAccountId, 1 byAccountId+TrackData (Anonymous), 3 byAccountId+TrackData(Anonymous+Personal)
            if (account_watcher.Card.PlayerTracking.CardLevel == 0)
            {
              if (GeneralParam.GetInt32("AntiMoneyLaundering", "GroupByMode", 0) == 1)
              {
                frm_message.Show(Resource.String("STR_UC_CARD_AML_CANT_CUSTOMIZE_ACCOUNT"),
                                 Resource.String("STR_MSG_ANTIMONEYLAUNDERING_WARNING"),
                                 MessageBoxButtons.OK,
                                 MessageBoxIcon.Warning,
                                 this.ParentForm);
                return;
              }
            }
            _card_data = account_watcher.Card;
            CardData.DB_CardGetPersonalData(_card_data.AccountId, _card_data);

            if (!m_must_show_comments)
            {
              // RMS 14-OCT-2013: not new card, check edition permission depending on account type anonymous or personal
              ProfilePermissions.CashierFormFuncionality _permission;

              _permission = (account_watcher.Card.PlayerTracking.CardLevel > 0) ? ProfilePermissions.CashierFormFuncionality.AccountEditPersonal :
                                                                                  ProfilePermissions.CashierFormFuncionality.AccountEditAnonymous;

              if (!ProfilePermissions.CheckPermissionList(_permission, ProfilePermissions.TypeOperation.RequestPasswd, this.ParentForm, 0, out error_str))
              {
                return;
              }
            }
          }
          using (frm_yesno _shader = new frm_yesno())
          {
            using (PlayerEditDetailsCashierView _view = new PlayerEditDetailsCashierView())
            {
              using (PlayerEditDetailsCashierController _controller =
                new PlayerEditDetailsCashierController(_card_data, _view, IDScanner, PlayerEditDetailsCashierView.SCREEN_MODE.DEFAULT))
              {
                _controller.TakePhoto();



                if (_controller.Dialog_Cancel)
                {
                  return;
                }

                if (WindowManager.StartUpMode == WindowManager.StartUpModes.Explorer)
                {
                  _view.Location = new Point(WindowManager.GetFormCenterLocation(_shader).X - (_view.Width / 2),
                    _shader.Location.Y + 2);
                }
                _shader.Opacity = 0.6;
                _shader.Show();
                _dgr_ac_edit = _view.ShowDialog(_shader);
              }
            }
          }

          if (_dgr_ac_edit == DialogResult.OK)
          {
            // AJQ 29-MAY-2013, Reload the data of the "TrackData" being edited
            // Error: use the m_current_track_number that was refreshed on background)
            //        The background refresh has been disabled for tracks that are not associated to any account.
            CardData.DB_CardGetAllData(_card_data.TrackData, _card_data, false);

            // RCI 28-AUG-2014: A new personal card is created, reset the timer.
            if (m_is_new_card)
            {
              tick_count_tracknumber_read = 0;
            }

            m_is_new_card = _card_data.IsNew;
            m_pending_check_comments = false;
          }

          if (_card_data.AccountId != 0)
          {
            // AJQ 29-MAY-2013, Refresh the data on Screen when there is an account.
            //                  AccountId is 0 when the "Personalizacion" has been cancelled and the card was "new".
            //                  Avoids displaying the message "The card doesn't belong to the site".
            LoadCardByAccountId(_card_data.AccountId);
          }
          
        }
      }
    }


  } // uc_card
}