//------------------------------------------------------------------------------
// Copyright � 2007 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Program.cs
// 
//   DESCRIPTION: 
// 
//        AUTHOR: Andreu Julia
// 
// CREATION DATE: 01-AGO-2007
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 05-JUL-2013 LEM    Processed Release_Candidate at starting
// 12-AUG-2013 DRV    Added support to hide OSK
// 25-SEP-2013 NMR    Changed conditions for TITO execution mode
// 22-SEP-2014 AJQ & RCI    Fixed Bug WIG-1256: TITO barcode read doesn't work
// 22-SEP-2014 AJQ & RCI    Fixed Bug WIG-1258: Accounts card can't be read
// 16-FEB-2017 ATB    Add logging system to the shutdowns to control it
//-------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Windows.Forms;
using WSI.Common;
using System.Diagnostics;
using System.Data;
using System.Threading;
using System.Runtime.InteropServices;
using WSI.Cashier.TITO;
using System.Text;

namespace WSI.Cashier
{
  static class Program
  {
    [DllImport("CommonBase")]
    static extern Boolean Common_ProcessReleaseCandidate(String Path, String ExeName);

    static public Boolean print_voucher;
    static public Int32 site_id;

    [STAThread]
    static void Main()
    {
      CashierMain();
    }
    /// <summary>
    /// The main entry point for the application.
    /// </summary>
    [STAThread]
    static void CashierMain()
    {
      String[] language_files;

      Application.EnableVisualStyles();
      Application.SetCompatibleTextRenderingDefault(false);

      // AJQ 08-SEP-2014, Initialize the keyboard filter
      KeyboardMessageFilter.Init();

      Process current_process = Process.GetCurrentProcess();
      Process[] all_processes = Process.GetProcessesByName(current_process.ProcessName);

      // Create Language directories
      language_files = System.IO.Directory.GetFiles(System.IO.Directory.GetCurrentDirectory(), "*_WSI.Cashier.resources.dll");

      foreach (String language_file in language_files)
      {
        System.IO.Directory.CreateDirectory(language_file.Substring(0, language_file.IndexOf("_WSI.Cashier.resources.dll")));
        System.IO.File.Copy(language_file, language_file.Substring(0, language_file.IndexOf("_WSI.Cashier.resources.dll")) + "\\WSI.Cashier.resources.dll", true);
      }

      Resource.Init("en-US");

      if (all_processes.Length > 1)
      {
        // RCI & ACC 10-JUL-2013: When uppdating Cashier (without restarting computer), it's necessary to wait until the old process has dead.
        for (Int32 _count = 0; _count < 4; _count++)
        {
          Thread.Sleep(500);
          all_processes = Process.GetProcessesByName(current_process.ProcessName);

          if (all_processes.Length <= 1)
          {
            break;
          }
        }
        if (all_processes.Length > 1)
        {
          frm_message.Show(Resource.String("STR_APP_GEN_ERROR_APP_ALREADY_RUNNING"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, null, false);
        }
      }
      else
      {
        WSIKeyboard.Hide();

        // Application OK to start
        Init();

        Cashier.MainForm = new frm_container();
        Application.Run(Cashier.MainForm);
      }

      // ATB 16-FEB-2017
      Misc.WriteLog("[SHUTDOWN] REASON: The application did not start",Log.Type.Message);
      Cashier.Shutdown(false);
    }

    /// <summary>
    /// Initialize components of the application.
    /// </summary>
    static void Init()
    {
      Thread thread_connecting;
      Thread thread_XmlSerializer;
      String default_xml;
      String client_id_str;
      int client_id;
      String _current_path;
      String _app_name;
      TitoTicketDefault _ticket_template;

      Cashier.Init();

      // Initialize Logger
      Log.AddListener(new SimpleLogger("CASHIER"));
      Log.Message("============ Starting WSI Cashier ============");

      Log.Message("============ WSI Cashier Started ============");

      if (!Development.Enabled)
      {
        // Apply any pending software-update
        _current_path = ".";
        _app_name = Environment.CommandLine.Replace((Char)34, (Char)32).Trim();
        Common_ProcessReleaseCandidate(_current_path, _app_name);
      }

      //
      // Check Version
      //
      if (!VersionControl.IsVersionOk(Application.ProductName))
      {
        frm_message.Show(Resource.String("STR_APP_GEN_MSG_VERSION_ERROR", Application.ProductName, VersionControl.VersionStr), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, null, false);
        // ATB 16-FEB-2017
        Misc.WriteLog("[SHUTDOWN] REASON: Cashier version is not OK", Log.Type.Message);
        Cashier.Shutdown(false);
      }

      // Configuration default values
      default_xml = "<SiteConfig>" +
                      "<DBPrincipal></DBPrincipal>" +
                      "<DBMirror></DBMirror>" +
                      "<DBId>0</DBId>" +
                    "</SiteConfig>";

      // Initialize Config file
      if (!ConfigurationFile.Init("WSI.Cashier.cfg", default_xml))
      {
        Log.Error(" Reading application configuration settings. Application stopped");

        return;
      }

      // Database Connection
      while (true)
      {
        // Start Connecting thread
        thread_connecting = new Thread(ProgressConnectingThread);
        thread_connecting.Name = "ProgressConnectingThread";
        thread_connecting.Start();

        // Wait for show loading progress...
        Thread.Sleep(2000);

        try
        {
          // APB 12/11/2008: 'Client Identifier' is now taken from configuration file instead of from a hard-coded constant
          client_id_str = ConfigurationFile.GetSetting("DBId").Trim();
          client_id = Convert.ToInt32(client_id_str);
        } // try
        catch
        {
          client_id = 0;
        } // catch

        // Init database
        WGDB.Init(client_id, ConfigurationFile.GetSetting("DBPrincipal"), ConfigurationFile.GetSetting("DBMirror"));

        thread_connecting.Abort();

        if (WGDB.Initialized)
        {
          break;
        }

        // Config and Save Database configuration
        frm_database_cfg.Show(false, null);
      }

      // Connect to database
      WGDB.SetApplication("CASHIER", VersionControl.VersionStr);
      WGDB.ConnectAs("WGGUI");

      //DCS Enable Configuration Voucher Params
      OperationVoucherParams.LoadData = true;

      // Initialize language resources from DB set language (general params).
      Resource.Init(CashierBusinessLogic.CashierLanguage());

      // ACC 10-AUG-2012 Init HtmlPrinter
      if (!HtmlPrinter.Init())
      {
        // Printer not initialized correctly
        // ATB 16-FEB-2017
        Misc.WriteLog("[RESTART] REASON: Printer not initialized correctly", Log.Type.Message);
        Cashier.Shutdown(false, true);
      }

      // DDM 26-NOV-2013 Init TITO Printer
      if (BusinessLogic.IsModeTITO)
      {
        _ticket_template = new TitoTicketDefault(Computer.Alias(Environment.MachineName));
        if (!TitoPrinter.Init(_ticket_template))
        {
          // Printer not initialized correctly
          Log.Message("Tito Printer not initialized correctly!");
        }
      }

      // Initialize site identifier from DB
      if (CashierBusinessLogic.ReadSiteId(ref Program.site_id) == false)
      {
        frm_message.Show(Resource.String("STR_SITE_ID_ERROR"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, null);
        // ATB 16-FEB-2017
        Misc.WriteLog("[PROMPTED SHUTDOWN] REASON: Site ID do not match", Log.Type.Message);
        Cashier.Shutdown(true);
      }

      // RCI 30-DEC-2013: At the first beginning, register the terminal in CASHIER_TERMINALS if it does not exist.
      using (DB_TRX _db_trx = new DB_TRX())
      {
        Common.Cashier.ReadTerminalId(_db_trx.SqlTransaction, Environment.MachineName, GU_USER_TYPE.USER, false);
        _db_trx.Commit();
      }

      // Gets the OnScreenKeyboard Mode and sets the property
      WSI.Cashier.Cashier.HideOsk = Common.Cashier.ReadOSKMode();

      // Gets the PinPad enabled status and sets the property
      WSI.Cashier.Cashier.PinPadEnabled = Common.Cashier.ReadPinPadEnabled();

      //DDM 26-AUG-2014: Due to the lag on first loading gambling table, by one error at XMLSerializer
      if (GamingTableBusinessLogic.IsEnabledGTPlayerTracking())
      {
        thread_XmlSerializer = new Thread(InitializeXMLSerializeThread);
        thread_XmlSerializer.Name = "InitializeXMLSerializeThread";
        thread_XmlSerializer.Start();
      }

      // TODO
      // Check if exists the library itextsharp.dll and the Acrobat Reader application
      //if (!TemplatePDF.ApplicationAvailable())
      //{
      //}

			//if (!IDCardScannerHelper.InitModel())
			//{
			//	Log.Error("IDCardScannerHelper.Init() failed!");
			//}
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Thread for Show connecting to database
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    static private void ProgressConnectingThread()
    {
      frm_database_cfg frm_database;

      frm_database = null;

      try
      {
        frm_database = frm_database_cfg.Show(true, null);
      }
      catch
      {
        if (frm_database != null)
        {
          frm_database.Hide();
        }
      }
    } // ProgressConnectingThread

    //------------------------------------------------------------------------------
    // PURPOSE : Thread for inicialize XMLSerializer
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    static private void InitializeXMLSerializeThread()
    {
      System.Xml.Serialization.XmlSerializer _xml_ser;

      try
      {
        _xml_ser = new System.Xml.Serialization.XmlSerializer(typeof(GamingTableProperties));
        _xml_ser = new System.Xml.Serialization.XmlSerializer(typeof(GamingSeatProperties));
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } // InitializeXMLSerializeThread

  }
}