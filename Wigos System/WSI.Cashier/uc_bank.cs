//------------------------------------------------------------------------------
// Copyright � 2007-2008 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: bank.cs
//  
//   DESCRIPTION: Implements the class uc_bank (Cash Desk information)
// 
//        AUTHOR: Andreu Julia
// 
// CREATION DATE: 01-AUG-2007
// 
// REVISION HISTORY:
// 
// Date        Author     Description
// ----------- ------     ----------------------------------------------------------
// 01-AUG-2007 AJQ        Inital Draft.
// 27-MAR-2012 MPO        User session: Added a event for each button for set the last activity.
// 06-JUL-2012 DDM        Modified the labels of RE, NRE and Padlock.
// 01-AUG-2012 HBB        HBB 01-AUG-2012: Checking if the amount of money expected and delivered are equals.
// 08-FEB-2013 RRB        Reorganize and added labels taxes (federal/estatal).
// 21-FEB-2013 ICS        Moved the body of CashierSessionClose to WSI.Common.
// 28-MAY-2013 NMR        Preventing the focus losing on main window.
// 31-MAY-2013 LEM        Fixed Bug #813: Last cashier movements related with mobile banks are shown wrongly.
// 02-JUL-2013 DLL        Added labels for currency exchange.
// 21-AUG-2013 DLL        Fixed bug WIG-143: When print open cash ticket shows Iso Code.
// 26-AUG-2013 JBC        Added info from Points as Cashin.
// 10-SEP-2013 DLL        Added info for Check Recharge.
// 13-SEP-2013 QMP        Added Work Shifts.
// 26-SEP-2013 DLL        Modify Voucher OpenCash, CloseCash, CloseCashStatus, FillIn and FillOut.
// 02-OCT-2013 DLL        Added Web Browse control.
// 02-OCT-2013 DLL        Delete all labels.
// 13-OCT-2013 RMS        Fixed Bug WIG-272: Removed unnecessary call to Cashier.SetUserSession(0);.
// 22-OCT-2013 ICS        Added Undo Operation support. 
// 10-NOV-2013 FBA        Added Cage movements logic.
// 26-NOV-2013 JAB        Added BlindClousure mode.
// 06-DEC-2013 JAB        To get general parameters with TitoUtils.
// 08-DEC-2013 JAB        Changes to include tickets.
// 09-DEC-2013 DLL        Fixed Bug WIG-467.
// 11-DEC-2013 JAB        Added ShowBalance functionality to printed vouchers.
// 12-DEC-2013 JAB        Close and withdrawal cash requires open cage's session. (In Cage mode).
// 18-DEC-2013 ICS        Added a function to check collected tickets in cashier session closing
// 14-DEC-2013 JCA        Added FillIn when CAGE Movement Sent by Terminal.
// 23-DEC-2013 JAB        Added quick open mode.
// 15-JAN-2014 FBA & RRR  Added movement to inform gaming colleted in gaming tables.
// 15-JAN-2014 DLL        Added movement for Chips change.
// 31-JAN-2014 QMP        Fixed Bug WIG-563: Enabled buttons when cash session is closed.
// 13_FEB-2014 RRR        Fixed Bug WIG-624: Link ShowStock Enabled/Disabled status to Show Balance permissions.
// 20-FEB-2014 JPJ        Fixed Bug WIG-659: Detail of the cashier movement when it's a ticket denominaion.
// 20-FEB-2014 JCA        Fixed Bug WIG-663: When Cage session is closed, FILLER_OUT and CLOSE_SESSION Movements fails.
// 03-APR-2014 JAB        Added new functionality: Amount request.
// 16-APR-2014 ICS, MPO & RCI    Fixed Bug WIG-830: Not enough available storage exception.
// 22-APR-2014 DLL        When GP(ShowDenominations) is disabled, not show chips stock control and chips change.
// 13-MAY-2014 JCA        Fixed Bug WIG-893: Can close cashier if not has Cage session assigned.
// 16-MAY-2014 JAB        Added new functionality: Cage auto mode.
// 16-MAY-2014 DLL        Fixed Bug WIG-924: Delete ApplyExchange function. Values save in CM_AUX_AMOUNT.
// 21-MAY-2014 JCA        Fixed Bug WIG-937: Error in "AutoMode" and "AutoMode.CageStock" when FILL_OUT CHECK.
// 06-JUN-2014 JAB        Added two decimal characters in denominations fields.
// 25-JUL-2014 DLL        Chips are moved to a separate table
// 12-AGO-2014 SGB        Fixed Bug WIG-1167: Delete attribute m_is_readed_change_chips
// 08-AUG-2014 RMS        Added supoort for Specific Bank Card Types
// 17-SEP-2014 AMF        Added new movement Concepts Input - Output
// 25-SEP-2014 JAB        Fixed Bug WIG-1296: money request are wrong created when coins field  are used.
// 07-OCT-2014 SGB        Modified call to VoucherCashDeskOpen for new footer voucher
// 09-OCT-2014 LEM        Fixed Bug WIG-1460: Insert more than one cage movement when Cage.AutoMode = 1 on Close/FillOut Cash
// 10-OCT-2014 LEM        Fixed Bug WIG-1469: Isn't possible close cash when Cage.AutoMode = 1 with bank card amount > 0
// 15-OCT-2014 JAB        Fixed Bug WIG-1502: Cage request generates an empty voucher.
// 21-OCT-2014 JCA & DDM  Fixed BUG WIG-1545: Error in SQL 2005. Not allowed subquerys in insert
// 13-NOV-2014 LRS        Add currency type functionality
// 15-DEC-2014 JBC, DLL, JPJ    New Company B commission movements
// 23-OCT-2014 JBC        Fixed BUG WIG-1899: Error closing cashier session
// 17-MAR-2015 JPJ        TASK 653 Closing table with money
// 25-MAR-2015 YNM        Fixed Bug WIG-2165: When GP CageAutoMode = 1, Cashier button "Cage Request" appears enabled 
// 30-APR-2015 JBC        Fixed Bug WIG-2266: GamingDay open cashier session.
// 24-MAY-2015 DLL        Fixed Bug WIG-2398: Create money_collection for gaming_table when TITO
// 30-JUN-2015 FAV        Fixed Bug WIG-2505: "Undo last withdrawa" button. In the cashier amount, the value of the last withdrawal is added , when the box was originally to 0.
// 30-JUL-2015 RCI        Fixed Bug WIG-2617: ShowBalanceInfo button has to be always enabled, even the cashier session has expired (for working shift or gaming day).
// 21-AUG-2015 MPO        TFS-3884: Bugs & new functionalities for Undo withdrawal
// 02-OCT-2015 FAV        Fixed Bug TFS-4930: Shows warning box if there is not a withdrawal to undo
// 19-OCT-2015 ETP        Product Backlog Item 4690 remove call to general param TITO.TitoMode.
// 04-NOV-2015 JPJ        We hide two properties so the frm_container will not add them automatically when edited
// 04-NOV-2015 SGB        Backlog Item WIG-5431: Change designer
// 11-NOV-2015 FOS        Product Backlog Item: 3709 Payment tickets & handpays
// 12-NOV-2015 DLL        Fixed Bug TFS-6503: Error in cashier deposit in Cage automode and stock control automode
// 12-NOV-2015 DLL        Fixed Bug TFS-6503: Error in cashier deposit in Cage automode and stock control automode
// 23-NOV-2015 FOS        Bug 6771:Payment tickets & handpays
// 12-NOV-2015 DLL        Fixed Bug TFS-6503: Error in cashier deposit in Cage automode and stock control automode
// 04-MAR-2016 FAV        Fixed Bug TFS-10219: Error in gaming table for deposit (Cage automode and stock control automode)
// 28-MAR-2016 LTC        Bug 10771:Close Gamblig tables - Wrong Title
// 04-APR-2016 DHA        Product Backlog Item 9756: added chips (RE, NR and Color)
// 14-APR-2016 RAB        Product Backlog Item 10855: Tables (Phase 1): Currency/chip Selector.
// 05-MAY-2016 RAB        PBI 12736: Review 23: Changes and improvements (cashier operations in gaming tables)
// 05-MAY-2016 FGB        Product Backlog Item 9758: Tables (Phase 1): Review table reports
// 24-MAY-2016 DHA        Product Backlog Item 9848: drop box gaming tables
// 09-JUN-2016 DHA        Product Backlog Item 13402: Rolling & Fixed closing stock
// 16-JUN-2016 RAB        Product Backlog Item 9853: GamingTables (Phase 3): Fixed Bank (Phase 1)
// 28-JUN-2016 DHA        Product Backlog Item 15903: Gaming tables (Fase 5): Drop box collection on Cashier
// 28-JUL-2016 ESE        Product Backlog Item 15239:TPV Televisa: Cashier, totalizing strip
// 01-AUG-2016 RAB        Product Backlog Item 15614: TPV Televisa: Undo last operation on bank card
// 05-AGO-2016 LTC        Product Backlog Item 14990:TPV Televisa: Cashier Withdrawal
// 17-AUG-2016 RAB        Product Backlog Item 15914: Gaming tables (Phase 5): Order summary box and voucher currency
// 23-AGO-2016 LTC        Product Backlog Item 16198:TPV Televisa: Closing Cash
// 29-AGU-2016 DHA        Fixed Bug 17116: Gaming table - Error closing fixed bank on gaming table
// 29-AGU-2016 DHA        Fixed Bug 17113: error showing first fill in gaming tables (Fixed and chips sleeps on table)
// 08-SEP-2016 FOS        PBI 16567: Tables: Split PlayerTracking to GamingTables (Phase 5)
// 20-SEP-2016 FJC        Bug 17669:Anular retiro de caja desde b�veda/Cajero: no deshace el stock y conceptos de b�veda
// 22-SEP-2016 JML        Fixed Bug 17571: Gaming tables - Integrated Cashier: saling chips - wrong error message
// 26-SEP-2016 FAV        Fixed Bug 18124: The cash withdrawal is not reflected in the cage stock (automode = true)
// 27-SEP-2016 ESE        Bug 17833: Televisa voucher, status cash
// 27-SEP-2016 FAV        Fixed Bug 8816: Allow request to cage without cage opened.
// 29-SEP-2016 FJC        Fixed Bug (Reopened) 17669:Anular retiro de caja desde b�veda/Cajero: no deshace el stock y conceptos de b�veda
// 29-SEP-2016 FOS        Fixed Bug 18126: Error closing tables without playertracking
// 04-OCT-2016 ETP        Fixed Bug 18461: Incorrect 'Others' value for viewing current pending movements.
// 10-OCT-2016 JML        Bug 18840: Improvements opening and closing gaming tables 
// 20-SEP-2016 JMV        Bug 19354:WigosGUI: GP Cashier.ShowBalanceInfo not working
// 31-OCT-2016 JML        Fixed Bug 19865: Gamming tables stock control - not displayed in the cashier
// 31-OCT-2016 JML        Fixed Bug 19866: Chips shange control - not displayed in the cashier
// 14-NOV-2016 ETP        Fixed Bug 20285: Exception when cash session is already closed.
// 16-NOV-2016 DHA        Fixed Bug 20678: error closing gaming table with "Fixed Bank", stock sleep on table and Not auto cage mode
// 17-NOV-2016 ATB        Bug 19275:Televisa: the closing cash voucher does not match with Televisa's requirements
// 17-NOV-2016 ETP        Fixed Bug 20696: Cashier error when cashier seesion is closed and has a pending movement.
// 24-NOV-2016 DHA        Bug 20560: error initial and final chips amount on closing gaming tables sessions
// 28-NOV-2016 RGR        Bug 18295: Cage: The general report is not showing the correct data when a withdrawal is canceled
// 29-NOV-2016 ETP        Fixed Bug 21111: Error in fill gaming tables.
// 30-NOV-2016 FOS&FGB    PBI 19197: Reopen cashier sessions and gambling tables sessions
// 08-DIC-2016 RGR        Bug 18295: Cage: The general report is not showing the correct data when a withdrawal is canceled
// 12-DEC-2016 AMF        Bug 5446:�ltimos movimientos del cajero y de Wigos GUI: registros alternados en la primera recarga y posterior anulaci�n
// 14-DEC-2016 DHA        Bug 21495: Gaming table drop box collection generates duplicated values
// 20-DEC-2016 JML        Fised Bug 9290:Cashier: Change message not enougth stock in "Deposito de b�veda"
// 09-JAN-2017 DPC&FOS    Bug 21977: Error in cashier when realizes deposits 
// 11-JAN-2017 ATB        Bug 20508: Cashier: Malfunction after doing a second voucher and cash fill out
// 26-JAN-2017 RAB        Bug 23517: Cashier: Credit and Fill buttons don't respond.
// 27-JAN-2017 RAB        Bug 23546: Cashier: The user cann't open tables if you don't have permission "Cashier: Opening and closing"
// 31-JAN-2017 DHA        Bug 23600: Gaming table permissions request authorized user
// 03-FEB-2017 MS         Bug 24105:Cashier: Keep "Cage Request" button Enabled while a Request is Outstanding
// 03-FEB-2017 CCG        Bug 24064:Cajero: Cuando el cajero esta fuera de turno y se cierra caja los botones del men� salen deshabilitados
// 08-FEB-2017 DHA        PBI 24378: Reopen cashier/gambling tables sesions
// 13-FEB-2017 DHA        Bug 24541: Error on closing stock fill out movements amounts
// 17-FEB-2017 DHA        Bug 24753: error undo last withdrawl without Cage
// 22-FEB-2017 JML        PBI 24378:Reabrir sesiones de caja y mesas de juego: rehacer reapertura y correcci�n de Bugs
// 02-MAR-2017 ETP & AMF  Fixed Bug 25232: Error on closing stock with bank card and check.
// 09-MAR-2017 FGB        PBI 25269: Third TAX - Cash session summary
// 11-APR-2017 EOR        Bug 26642: Error in Cage stock new denomination currency 
// 27-APR-2017 DHA        Bug 26735:WTA-350: wait till cut manual paper on printer
// 16-MAY-2017 DHA        Bug 27482:WIGOS-2090 Voucher is not printed when you open a gaming table session.
// 24-MAY-2017 JML        PBI 27484:WIGOS-1226 Win / loss - Introduce the WinLoss - EDITION SCREEN - PER DENOMINATION
// 24-MAY-2017 JML        PBI 27486:WIGOS-1227 Win / loss - Introduce the WinLoss - EDITION SCREEN - ACCUMULATED
// 12-JUL-2017 DHA        Bug 28655:WIGOS-3546 "Reabrir caja" is not printing a voucher
// 17-JUL-2017 EOR        Bug 28800: WIGOS-3717 Cash is not correctly opened when cash timeout is reached
// 17-JUL-2017 DHA        Bug 28862:WIGOS-3717 Cash is not correctly opened when cash timeout is reached
// 26-JUL-2017 DHA        Bug 28989:WIGOS-3188 [Ticket #6581] Cashier (WTA-350) "freezed"
// 28-JUL-2017 DHA        Bug 29039:WIGOS-3469 [Ticket #6619] Informaci�n de reportes de mesas con montos duplicados / Information of tables reports with duplicate amounts
// 01-AUG-2017 CCG        WIGOS-4089: Message about maximum number of opening sessions reached appears when opening gambling tables even for first time
// 03-AUG-2017 EOR        Bug 29187: WIGOS-3944 Is not possible close a cash session by bankcard once the session was reopened
// 11-AUG-2017 DPC        Bug 29328:[WIGOS-4358] When the cash session has exceeded the maximum duration and only Cash and Gambling Tables are enabled, closing a Gambling Table enables all Cashier options menu.
// 01-SEP-2017 ETP        WIGOS-3817 The cancellation of a withdrawal with credit card is not working properly
// 21-SEP-2017 JML        PBI 29317:WIGOS-3409: MES13 Specific alarms: Gaming table day loss
// 22-SEP-2017 RAB        Bug 29823: WIGOS-4984 [Ticket #8492] Gambling Table out of working day - Exception v03.006.0023
// 08-NOV-2017 DHA        Bug 30651:WIGOS-6198 With autocage, the cage stock is getting modify with AutoMode.CageStock = 0
// 16-NOV-2017 JML        Bug 30830:WIGOS-6662 [Ticket #10509] aperura doble de mesa Super P 01 Casino Grand 634
// 23-NOV-2017 DHA        Bug 30885:WIGOS-6810 Gambling tables: variable banking gaming table type cannot be opened the next session. Exception "Referencia a objeto no establecida como instancia de un objeto".
// 07-FEB-2018 DHA        Bug 31436:WIGOS-8001 [Ticket #12150] Todos: Error Reporte de Sesi�n de Caja Tarjeta Bancaria/PinPad
// 14-FEB-2018 RGR        Bug 31533: WIGOS-5293 Gambling tables: user are enabled on player tracking when the gaming table has been closed 
// 01-MAR-2018 EOR        Bug 31765: WIGOS-8491 Closing Cash message gets error when cancelling message when closing unbalance 
// 06-MAR-2018 AGS        Bug 31816:WIGOS-8506 Error message when the user try to add 0$ chips in the opening of a gaming table configurated like "Banca fija"
// 08-MAR-2018 JML        Fixed Bug 31847:WIGOS-8651 Error opening a table with fixed float for 2 cashiers on the same time
// 14-MAR-2018 RGR        Bug 31934:WIGOS-8743 [Ticket #12993] Notificaci�n de Cliente Activo en Mesa de Juego V03.06.0035 
// 21-MAR-2018 AGS        Bug 32010: WIGOS-9318 Gambling table cannot be opened again after closing it and timeout log error
// 23-MAR-2018 AGS        Bug 32036: WIGOS-9369 Withdrawal does not print voucher
// 17-APR-2018 AGS        Bug 32367: WIGOS-10233 Cannot open cashier session
// 26-APR-2018 JML        Fixed Bug 32452:WIGOS-10429 - Cashier session cannot be reopened
// 26-APR-2018 AGS        Bug 32453: WIGOS-7719 [Ticket #11662] Apertura de mesa MB-2 (congela el sistema)
// 27-APR-2018 FOS        Bug 32471: WIGOS-10512 Integrated table cannot be opened if cashier is already logged in
// 03-MAY-2018 JML        Fixed Bug 32512:WIGOS-10528 Cannot update cage stock for some bills/chips
// 20-JUN-2018 AGS        Bug 33223:WIGOS-13078 Gambling tables: Close cash session unbalanced error "There has been an error. Please try again" is displayed in gambling table with integrated cashier when there are credit card and check operations.
// 21-JUN-2018 AGS        Bug 33254:WIGOS-13129 Gambling tables: Open cash session error with gambling table with integrated cashier error when previously the table has been setted to integrated.
// 22-JUN-2018 AGS        Bug 33273:WIGOS-13156 Gambling tables: Close cash session unhandled exception is displayed after reopened the gambling table with integrated cashier.
// 27-JUN-2018 LQB        Bug 33389:WIGOS-13129 Gambling tables: Open cash session error with gambling table with integrated cashier error when previously the table has been setted to integrated.
// 05-JUL-2018 FOS        Bug 33506:WIGOS-13443 Can't opening a gaming table FREE banking type
//------------------------------------------------------------------------------

#region Usings

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WSI.Common;
using WSI.Common.PinPad;
using WSI.Common.ReopenSessions;
using WSI.Common.TITO;
using WSI.PinPad;
using WSI.PinPad.Banorte.Adapter;

#endregion // Usings

namespace WSI.Cashier
{
  public partial class uc_bank : UserControl
  {
    #region Attributes

    SqlConnection sql_conn;
    SqlTransaction sql_trx;
    SqlCommand sql_command;
    String sql_str;
    Int64 terminal_id;
    static frm_amount_input AmountInput;
    static frm_last_movements PlayHistory;
    private static frm_yesno form_yes_no;
    frm_container parent_window;
    Currency m_total_amount;
    Boolean m_show_balance;
    CashierSessionInfo m_cashier_session_info;
    Int32? m_gaming_table_id;
    Boolean m_is_first_deposit;

    private const Int32 MB_RECORDS_BATCH_SIZE = 100;

    private const Int16 SQL_COLUMN_CAGE_AMOUNTS_CAA_TYPE = 1; //IF THIS VALUE IS CHANGED, CHANGE IN FR_AMOUNT_INPUT TO
    private const Int16 SQL_COLUMN_CAGE_AMOUNTS_CAA_ISO_CODE = 6; //IF THIS VALUE IS CHANGED, CHANGE IN FR_AMOUNT_INPUT TO
    private const Int16 SQL_COLUMN_CAGE_AMOUNTS_CAA_DENOMINATION = 8; //IF THIS VALUE IS CHANGED, CHANGE IN FR_AMOUNT_INPUT TO
    private const Int16 SQL_COLUMN_CAGE_AMOUNTS_CAA_UN = 9; //IF THIS VALUE IS CHANGED, CHANGE IN FR_AMOUNT_INPUT TO
    private const Int16 SQL_COLUMN_CAGE_AMOUNTS_CAA_TOTAL = 10;//IF THIS VALUE IS CHANGED, CHANGE IN FR_AMOUNT_INPUT TO
    private const Int16 SQL_COLUMN_CAGE_AMOUNTS_CAA_CURRENCY_TYPE = 11;
    private const Int16 SQL_COLUMN_CAGE_AMOUNTS_CAA_CHIP_ID = 12;

    private enum BUTTON_OPEN_TYPE
    {
      BUTTON_DESK_OPEN = 0,
      BUTTON_AMOUNT_REQUEST = 1
    }

    private enum BUTTON_CLOSE_TYPE
    {
      BUTTON_SESSION_DISABLE = -1,
      BUTTON_SESSION_CLOSE = 0,
      BUTTON_SESSION_REOPEN = 1
    }

    private enum REOPEN_CASHIER_SESSION_STATUS
    {
      EXISTS_OPEN_CASHIER_SESSION = 1,
      CAGE_SESSION_IS_CLOSED = 2,
      EXISTS_CLOSED_MOVEMENTS_RECAUDACION = 3,

    }

    private IList<Seat> m_seat_to_close = new List<Seat>();

    #endregion // Attributes

    #region Properties
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    public CashierSessionInfo CashierSessionInfo
    {
      get
      {
        if (m_cashier_session_info == null || m_cashier_session_info.TerminalId == Cashier.CashierSessionInfo().TerminalId)
        {
          // Set local cashier session when is not setet yet or if the terminal is the same
          m_cashier_session_info = Cashier.CashierSessionInfo();
          GamingTableId = GamingTablesSessions.GamingTableInfo.GamingTableId;
        }

        AmountInput.CashierSessionInfo = m_cashier_session_info;

        return m_cashier_session_info;
      }

      set
      {
        if (value == null)
        {
          m_cashier_session_info = Cashier.CashierSessionInfo();
          AmountInput.CashierSessionInfo = m_cashier_session_info;
          GamingTableId = GamingTablesSessions.GamingTableInfo.GamingTableId;
        }
        else
        {
          m_cashier_session_info = value;
          AmountInput.CashierSessionInfo = value;
        }
      }
    }

    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    public Int64? GamingTableId
    {
      get
      {
        if (m_gaming_table_id.HasValue)
        {
          return m_gaming_table_id;
        }

        return GamingTablesSessions.GamingTableInfo.GamingTableId;
      }

      set
      {
        if (value > 0)
        {
          AmountInput.IsGamingTable = true;
          AmountInput.GamingTableId = value;
          m_gaming_table_id = (Int32)value;
        }
        else
        {
          AmountInput.IsGamingTable = false;
          AmountInput.GamingTableId = null;
          m_gaming_table_id = null;
        }
      }
    }

    public IList<Seat> SeatToClose
    {
      get
      {
        return this.m_seat_to_close;
      }

      set
      {
        this.m_seat_to_close = value;
      }
    }

    #endregion Properties

    #region Constructors

    //------------------------------------------------------------------------------
    // PURPOSE : Initialize controls properties.
    //           Create form controls.
    //           Assign control resources.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //  
    public uc_bank()
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[UC LOAD] uc_bank", Log.Type.Message);

      InitializeComponent();

      Init();

      InitializeControlResources();

      EventLastAction.AddEventLastAction(this.Controls);
    } // uc_bank

    //------------------------------------------------------------------------------
    // PURPOSE : Initialize controls.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //  
    private void Init()
    {
      // Initialize fields     
      terminal_id = Cashier.TerminalId;
      AmountInput = new frm_amount_input();
      PlayHistory = new frm_last_movements();
      form_yes_no = new frm_yesno();
      form_yes_no.Opacity = 0.6;

#if DEBUG
      // TEST FOR TRANSFER BETWEEN CASHIER SESSIONS
      // FAV 24-AUG-2015
      #region Cashier sessions transfer

      btn_test_transfer.Visible = true;

      btn_test_transfer.Click += new System.EventHandler(btn_test_transfer_Click);
      btn_transfer_send_another.Click += new System.EventHandler(btn_transfer_send_another_Click);
      btn_transfer_send_to_this.Click += new System.EventHandler(btn_transfer_send_to_this_Click);
      btn_transfer_receive.Click += new System.EventHandler(btn_transfer_receive_Click);

      #endregion
#endif

    } // Init

    #endregion

    #region Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Initialize control resources. (NLS string, images, etc.).
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //  
    public void InitializeControlResources()
    {
      // - NLS Strings:

      // - Labels

      // - Buttons
      btn_open.Text = Resource.String("STR_UC_BANK_BTN_CASH_DESK_OPEN");
      btn_open.Tag = BUTTON_OPEN_TYPE.BUTTON_DESK_OPEN;
      SetCloseButtonProperties(BUTTON_CLOSE_TYPE.BUTTON_SESSION_CLOSE);

      btn_deposit.Text = Resource.String("STR_UC_BANK_BTN_CASH_DESK_DEPOSIT");
      btn_withdraw.Text = Resource.String("STR_UC_BANK_BTN_CASH_DESK_WITHDRAW");
      btn_history.Text = Resource.String("STR_UC_BANK_BTN_CASH_DESK_CASH_FLOW_MOVEMENTS");
      btn_status.Text = Resource.String("STR_VOUCHER_CASH_DESK_STATUS_TITLE");
      btn_show_balance_info.Text = Resource.String("STR_CASHIER_TEXT_BTN_SHOW_BALANCE");
      btn_change_chips.Text = Resource.String("STR_VOUCHER_CHIPS_CHANGE_TITLE");
      btn_chips_stock.Text = Resource.String("STR_CHIPS_STOCK_CONTROL");
      btn_undo_withdrawal.Text = Resource.String("STR_CASHIER_WITHDRAWAL_CANCELATION");
      btn_totalizing_strip.Text = Resource.String("STR_CASHIER_TOTALIZING_STRIP");

      AmountInput.InitializeControlResources();
      PlayHistory.InitializeControlResources();

      // To not show the browser with white backcolor.
      web_browser.Navigate("");
      web_browser.Document.BackColor = WSI.Cashier.CashierStyle.Colors.m_colors_dic[CashierStyle.Colors.ENUM_COLORS.HEX_E3E7E9];
      btn_show_balance_info.Location = new Point(web_browser.Location.X + (web_browser.Size.Width / 2) - (btn_show_balance_info.Size.Width / 2)
                                          , web_browser.Location.Y + (web_browser.Size.Height / 2) - (btn_show_balance_info.Size.Height / 2));

      //  - Datagrids

      //  - Other
      this.BackColor = WSI.Cashier.CashierStyle.Colors.m_colors_dic[CashierStyle.Colors.ENUM_COLORS.HEX_E3E7E9];
      label7.BackColor = WSI.Cashier.CashierStyle.Colors.m_colors_dic[CashierStyle.Colors.ENUM_COLORS.HEX_959595];
    } // InitializeControlResources

    protected override void OnLayout(LayoutEventArgs e)
    {
      base.OnLayout(e);
      this.BackColor = WSI.Cashier.CashierStyle.Colors.m_colors_dic[CashierStyle.Colors.ENUM_COLORS.HEX_E3E7E9];
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Return if is gaming table.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //  
    private Boolean IsGamingTable()
    {
      return ((GamingTableBusinessLogic.IsGamingTablesEnabled()) && m_gaming_table_id.HasValue && m_gaming_table_id.Value > 0);
    } // IsGamingTable

    //------------------------------------------------------------------------------
    // PURPOSE : Open a Cash Desk session with a specific amount.
    //
    //  PARAMS :
    //      - INPUT :
    //          - OpenAmount: SortedDictionary<CurrencyIsoType, Decimal>
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //  
    public Boolean CashierSessionOpen(SortedDictionary<CurrencyIsoType, Decimal> OpenAmount, DateTime WorkingDay, SqlTransaction Trx, out long CashierSessionId)
    {
      SqlParameter _sql_parameter;
      VoucherCashDeskOpen _voucher;
      Int32 _num_rows_created;
      Boolean _voucher_saved;
      Int64 _session_id;
      String _name;
      String _str_value;
      Decimal _dec_value;
      Int32 _num_sessions_opened;
      String _national_currency;
      CurrencyIsoType _iso_type;

      Int32 _gp_value;

      _str_value = "";
      _national_currency = CurrencyExchange.GetNationalCurrency();
      _iso_type = new CurrencyIsoType();

      // Initializations
      _voucher_saved = false;
      CashierSessionId = 0;

      // Update the cashier status with the opening amount and the local time
      try
      {
        if (Misc.IsGamingDayEnabled() && WorkingDay == DateTime.MinValue)
        {
          return false;
        }

        Cashier.SetUserSession(0);

        sql_str = "INSERT INTO   cashier_sessions    " +
                  "            ( cs_name             " +
                  "            , cs_user_id          " +
                  "            , cs_cashier_id       " +
                  "            , cs_tax_a_pct        " +
                  "            , cs_tax_b_pct        " +
                  "            , cs_gaming_day )     " +
                  "VALUES     ( @p1                  " +             // session name
                  "           , @p2                  " +             // session_user_id
                  "           , @p3                  " +             // session_terminal_id
                  "           , @tax_a_pct           " +             //
                  "           , @tax_b_pct           " +             //
                  "           , @pGamingDay )        " +             //
                  "   SET     @p4 = SCOPE_IDENTITY() ";

        sql_command = new SqlCommand(sql_str);
        sql_command.Connection = Trx.Connection;
        sql_command.Transaction = Trx;

        //
        // Format Session Name
        //
        switch (Cashier.Mode)
        {
          case Cashier.BankMode.PerUser:
            {
              _name = CashierSessionInfo.UserName + " " + WGDB.Now.ToString("dd/MMM/yyyy HH:mm");
              _name = _name.ToUpper();
            }
            break;

          case Cashier.BankMode.PerTerminal:
            {
              _name = CashierSessionInfo.TerminalName + " " + WGDB.Now.ToString("dd/MMM/yyyy HH:mm");
              _name = _name.ToUpper();
            }
            break;

          case Cashier.BankMode.Unknown:
          default:
            {
              return false;
            }
        }

        sql_command.Parameters.Add("@p1", SqlDbType.NVarChar, 50, "cs_name").Value = _name;
        sql_command.Parameters.Add("@p2", SqlDbType.Int, sizeof(int), "cs_user_id").Value = CashierSessionInfo.UserId;
        sql_command.Parameters.Add("@p3", SqlDbType.Int, sizeof(int), "cs_cashier_id").Value = (Int32)CashierSessionInfo.TerminalId;

        _str_value = Misc.ReadGeneralParams("Cashier", "Split.A.Tax.Pct");
        if (_str_value != null)
        {
          if (Decimal.TryParse(Format.DBNumberToLocalNumber(_str_value), out _dec_value))
          {
            sql_command.Parameters.Add("@tax_a_pct", SqlDbType.Decimal).Value = _dec_value;
          }
        }

        _str_value = Misc.ReadGeneralParams("Cashier", "Split.B.Tax.Pct");
        if (_str_value != null)
        {
          if (Decimal.TryParse(Format.DBNumberToLocalNumber(_str_value), out _dec_value))
          {
            sql_command.Parameters.Add("@tax_b_pct", SqlDbType.Decimal).Value = _dec_value;
          }
        }

        if (WorkingDay != DateTime.MinValue)
        {
          sql_command.Parameters.Add("@pGamingDay", SqlDbType.DateTime).Value = WorkingDay;
        }

        _sql_parameter = sql_command.Parameters.Add("@p4", SqlDbType.BigInt, 8, "cs_session_id");
        _sql_parameter.Direction = ParameterDirection.Output;
        _num_rows_created = sql_command.ExecuteNonQuery();

        if (_num_rows_created != 1)
        {
          throw new Exception("CashierSessionOpen. Error creating new session");
        }

        // store newly created session id
        _session_id = (Int64)_sql_parameter.Value;

        Cashier.SetUserSession(_session_id); // Save voucher needs the current session
        Cashier.SetGamingDay(WorkingDay);

        CashierSessionInfo.CashierSessionId = _session_id;

        // Get session opening date and store it into memory,
        // and reset notification
        this.parent_window.SystemInfo.CashSessionOpeningDate = CashierBusinessLogic.GetSessionOpeningDate(_session_id, Trx);


        // RCI 08-AUG-2011: Check if user has already open another cashier session.
        sql_str = " SELECT   COUNT(*)                  " +
                  "   FROM   CASHIER_SESSIONS          " +
                  "  WHERE   CS_USER_ID = @pUserId     " +
                  "    AND   CS_STATUS  = @pStatusOpen ";

        sql_command = new SqlCommand(sql_str, Trx.Connection, Trx);
        sql_command.Parameters.Add("@pUserId", SqlDbType.Int).Value = CashierSessionInfo.UserId;
        sql_command.Parameters.Add("@pStatusOpen", SqlDbType.Int).Value = CASHIER_SESSION_STATUS.OPEN;

        _num_sessions_opened = (Int32)sql_command.ExecuteScalar();

        // If already open, get that already open cashier session id.
        if (_num_sessions_opened > 1)
        {
          sql_str = " SELECT   MIN(CS_SESSION_ID)        " +
                    "   FROM   CASHIER_SESSIONS          " +
                    "  WHERE   CS_USER_ID = @pUserId     " +
                    "    AND   CS_STATUS  = @pStatusOpen ";

          sql_command = new SqlCommand(sql_str, Trx.Connection, Trx);
          sql_command.Parameters.Add("@pUserId", SqlDbType.Int).Value = CashierSessionInfo.UserId;
          sql_command.Parameters.Add("@pStatusOpen", SqlDbType.Int).Value = CASHIER_SESSION_STATUS.OPEN;

          _session_id = (Int64)sql_command.ExecuteScalar();

          Cashier.SetUserSession(_session_id); // Save voucher needs the current session

          CashierSessionInfo.CashierSessionId = _session_id;

          frm_message.Show(Resource.String("STR_UC_BANK_CASH_WARNING_ALREADY_OPENED", Cashier.UserName),
                           Resource.String("STR_UC_BANK_BTN_CASH_DESK_OPEN"),
                           MessageBoxButtons.OK, MessageBoxIcon.Warning, this.ParentForm);
          Log.Warning("CashierSessionOpen: User " + CashierSessionInfo.UserName + " already has an open cashier session. SessionId: " + _session_id + ".");

          return false;
        }

        _iso_type.IsoCode = _national_currency;
        _iso_type.Type = CurrencyExchangeType.CURRENCY;

        // Create movement for opened session
        CashierMovementsTable _cm_mov_table;
        _cm_mov_table = new CashierMovementsTable(CashierSessionInfo);
        _cm_mov_table.Add(0, CASHIER_MOVEMENT.OPEN_SESSION, OpenAmount[_iso_type], 0, "");

        // Insert voucher.

        // - Create voucher
        //SGB 08-OCT-2014: Modify call to VoucherCashDeskOpen for new parameter CashierVoucherType.CashOpening
        _voucher = new VoucherCashDeskOpen(CASHIER_MOVEMENT.OPEN_SESSION, CashierVoucherType.CashOpening, PrintMode.Print, Trx);

        VoucherCashDeskOpenBody _voucher_body;

        foreach (KeyValuePair<CurrencyIsoType, Decimal> _amount in OpenAmount)
        {
          if (_amount.Key.IsoCode != _national_currency && _amount.Key.Type == CurrencyExchangeType.CURRENCY)
          {
            _cm_mov_table.Add(0, CASHIER_MOVEMENT.OPEN_SESSION, _amount.Value, 0, "", "", _amount.Key.IsoCode);
            //initilize cashier_session_by_currency
            WSI.Common.Cashier.SetCashierSessionsByCurrency(_session_id, _amount.Key.IsoCode, _amount.Key.Type, 0, 0, Trx);
          }

          _voucher_body = new VoucherCashDeskOpenBody(CASHIER_MOVEMENT.OPEN_SESSION, _amount.Key, 0, _amount.Value);
          _voucher.SetParameterValue("@VOUCHER_CASH_DESK_OPEN_BODY", _voucher_body.VoucherHTML);
        }

        _voucher.AddString("VOUCHER_CASH_DESK_OPEN_BODY", "");

        if (!_cm_mov_table.Save(Trx))
        {
          Log.Error("CashierSessionOpen: Error saving cashier movement. SessionId: " + _session_id + ".");

          return false;
        }

        // - Save voucher
        _voucher_saved = _voucher.Save(Trx);

        // - Update Cashier Session Sales Limit and Mobile Banks Sales Limit
        if (!Int32.TryParse(GeneralParam.Value("Cashier", "SalesLimit.Enabled"), out _gp_value))
        {
          _gp_value = 0;
        }

        if (_gp_value == 1)
        {
          Currency _cs_limit_sales;

          if (!WSI.Common.Cashier.SalesLimitEfective(CashierSessionInfo.UserId, WSI.Common.Cashier.SalesLimitType.Cashier, out _cs_limit_sales))
          {
            Log.Error("CashierSessionOpen. Error reading Cashier Sesion Limit Sales. SessionId: " + _session_id.ToString());
          }
          if (!WSI.Common.Cashier.SetNewSalesLimit(CashierSessionInfo.UserId, _session_id, _cs_limit_sales, WSI.Common.Cashier.SalesLimitType.Cashier, Trx))
          {
            Log.Error("CashierSessionOpen. Error updating Cashier Sesion Limit Sales. SessionId: " + _session_id.ToString());
          }
          if (!WSI.Common.Cashier.SalesLimitEfective(CashierSessionInfo.UserId, WSI.Common.Cashier.SalesLimitType.MobileBank, out _cs_limit_sales))
          {
            Log.Error("CashierSessionOpen. Error reading Cashier Sesion MB Limit Sales. SessionId: " + _session_id.ToString());
          }
          if (!WSI.Common.Cashier.SetNewSalesLimit(CashierSessionInfo.UserId, _session_id, _cs_limit_sales, WSI.Common.Cashier.SalesLimitType.MobileBank, Trx))
          {
            Log.Error("CashierSessionOpen. Error updating Cashier Sesion MB Limit Sales. SessionId: " + _session_id.ToString());
          }
        }

        // - Update Cashier Session and Mobile Banks Total Sold
        if (!WSI.Common.Cashier.UpdateSessionTotalSold(_session_id, CashierSessionInfo.UserId, WSI.Common.Cashier.SalesLimitType.Cashier, 0, Trx))
        {
          Log.Error("CashierSessionOpen. Error updating Cashier Sesion Total Sales. SessionId: " + _session_id.ToString());
        }

        if (!WSI.Common.Cashier.UpdateSessionTotalSold(_session_id, CashierSessionInfo.UserId, WSI.Common.Cashier.SalesLimitType.MobileBank, 0, Trx))
        {
          Log.Error("CashierSessionOpen. Error updating Cashier Sesion MB Total Sales. SessionId: " + _session_id.ToString());
        }

        // In TITO mode create new money collection 
        if (WSI.Common.TITO.Utils.IsTitoMode() || Misc.IsWassMode())
        {
          Int64 _money_collection_id;
          if (!MoneyCollection.DB_CreateCashierMoneyCollection((Int32)CashierSessionInfo.TerminalId, _session_id, out _money_collection_id, Trx))
          {
            Log.Error("CashierSessionOpen. Error creating new money collection. SessionId: " + _session_id.ToString());
            return false;
          }
        }

        //GamingTables Mode
        if (GamingTableBusinessLogic.IsGamingTablesEnabled())
        {
          //IF GP GAMINTABLES ENABLED == TRUE
          if (!GamingTablesSessions.GetGamingTableInfo(CashierSessionInfo.TerminalId, Trx))
          {
            return false;
          }

          // Refresh gaming table id if there are any changes in GUI. ex: In case when integrated table is modified on GUI
          GamingTableId = GamingTablesSessions.GamingTableInfo.GamingTableId;

          if (GamingTablesSessions.GamingTableInfo.IsGamingTable)
          {
            if (!GamingTablesSessions.GetOrOpenSession(out m_is_first_deposit, m_gaming_table_id.Value, _session_id, Trx))
            {
              Log.Error("CashierSessionOpen. Error creating GamingTable sesion. SessionId: " + _session_id.ToString());

              return false;
            }
          }
        }

        if (_voucher_saved)
        {
          VoucherPrint.Print(_voucher);
        }
        else
        {
          Log.Error("CashierSessionOpen. Error saving voucher. Session id: " + _session_id.ToString());
        }
      }
      catch (Exception ex)
      {
        // Logger ...
        Log.Exception(ex);

        return false;
      }

      CashierSessionId = _session_id;
      return true;

    } // CashierSessionOpen

    //------------------------------------------------------------------------------
    // PURPOSE: Perform the complete Close Cash process.
    //          Close a cash desk session. (Cerrar Caja).
    // 
    //  PARAMS:
    //      - INPUT:
    //          - SessionStats:   WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS
    //          - CageSessionId:  Int64
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - None
    // 
    //   NOTES:
    //    This process involves the following steps:
    //    - Update mobile bank records associated to the Cashier Session (this update blocks 
    //      any attempt to perform additional movements)
    //    - Generate clsoing movement mobile banks associated to the cashier session
    //    - Update cashier session status and closing timestamp
    //    - Update cashier session balance
    //    - Generate cashier session closing movement
    //    - Generate and print close session voucher
    //
    //private void CashierSessionClose(WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS SessionStats, Int64 CageSessionId)
    //{
    //  CashierSessionClose(SessionStats, CageSessionId, new SortedDictionary<CurrencyIsoType, decimal>(), null);
    //} // CashierSessionClose

    //------------------------------------------------------------------------------
    // PURPOSE : Close the Cashier session.
    //
    //  PARAMS :
    //      - INPUT :
    //          - SessionStats:   WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS
    //          - CageSessionId:  Int64
    //          - ClosingAmount:  SortedDictionary<CurrencyIsoType, Decimal>
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void CashierSessionClose(WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS SessionStats
                                   , Int64 CageSessionId
                                   , SortedDictionary<CurrencyIsoType, Decimal> ClosingAmount
                                   , ClosingStocks ClosingStock)
    {
      System.Collections.ArrayList _voucher_list;
      CASHIER_SESSION_CLOSE_STATUS _session_status;
      Int64 _money_collection_id;
      MoneyCollection _money_collection;
      Decimal _amount_visits;
      GamingTablesSessions _gt_session;
      SortedDictionary<CurrencyIsoType, Decimal> _total_amounts;
      Decimal _chips_netwin;
      frm_generic_amount_input _generic_amount;
      DataTable _concepts_cashier_movements;
      frm_amount_input _drop_box_amount_input;
      String _gaming_table_name;
      SortedDictionary<CurrencyIsoType, Decimal> _drop_box_amount;
      WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS _dummy_session_stats;
      Boolean _dummy_check_redeem;
      Int64 _operation_id;
      CageDenominationsItems.CageDenominationsDictionary _cage_amounts_for_voucher = new CageDenominationsItems.CageDenominationsDictionary();

      Currency _session_alarm_amount;
      Currency _session_win_loss_amount;
      String _alarm_description;
      Currency _gambling_table_3_days_lost;
      Currency _gambling_table_3_days_win_loss_amount;
      
      _concepts_cashier_movements = new DataTable();
      _session_status = CASHIER_SESSION_CLOSE_STATUS.ERROR;
      _amount_visits = 0;
      _voucher_list = new ArrayList();
      _gaming_table_name = String.Empty;
      _drop_box_amount_input = null;
      _operation_id = 0;

      if ((GamingTablesSessions.GamingTableInfo.IsGamingTable || IsGamingTable()) && GamingTableBusinessLogic.IsGamingTablesEnabled())
      {
        // RAB 05-MAY-2016: PBI 12736: Review 23
        using (DB_TRX _db_trx = new DB_TRX())
        {
          GamingTablesSessions.GetGamingTableName(m_gaming_table_id.Value, out _gaming_table_name, _db_trx.SqlTransaction);
        }

        // Validate closing stock
        if (Cage.IsCageEnabled() && ClosingStock != null)
        {
          if (!ValidateClosingSessionStock(ClosingStock, CageSessionId))
          {
            return;
          }

          if (!ClosingStock.SetClosingAmountOnCloseFixedType(ClosingAmount, ClosingStock))
          {
            return;
          }
        }

        if (GamingTablesSessions.GamingTableInfo.HasDropbox && GamingTableBusinessLogic.GamingTableDropBoxCollectionCountMode() == GamingTableDropBoxCollectionCount.Cashier)
        {
          form_yes_no.Show(this.ParentForm);

          _drop_box_amount_input = new frm_amount_input();
          _drop_box_amount_input.GamingTableId = m_gaming_table_id.Value;
          _drop_box_amount_input.IsGamingTable = true;

          if (!_drop_box_amount_input.Show(Resource.String("STR_UC_BANK_GAMING_TABLE_DROP_BOX_COUNT", _gaming_table_name),
                                           out _drop_box_amount, out _dummy_check_redeem, VoucherTypes.DropBoxCount,
                                           null, 0, CASH_MODE.TOTAL_REDEEM, form_yes_no, out _dummy_session_stats, false, 0, false, CashierSessionInfo, null))
          {
            form_yes_no.Hide();
            this.RestoreParentFocus();

            return;
          }

          form_yes_no.Hide();
          this.RestoreParentFocus();
        }

        if (GeneralParam.GetBoolean("GamingTables", "EnabledInsertVisits", true))
        {
          _generic_amount = new frm_generic_amount_input();

          try
          {
            form_yes_no.Show(this.ParentForm);

            if (!_generic_amount.Show(GENERIC_AMOUNT_INPUT_TYPE.VISITS, Resource.String("STR_UC_BANK_GAMING_TABLE_VISITS"), ref _amount_visits))
            {
              form_yes_no.Hide();

              return;
            }
          }
          finally
          {
            // ICS 14-APR-2014 Free the object
            _generic_amount.Dispose();
          }
        }
      }

      Int64 _cage_movement_id;
      _cage_movement_id = 0;
      try
      {
        // FJC 02-SEP-2014: Fixed Bug WIG-1173: Change closing messages from CASH CLOSING and GAMBLING TABLE CLOSING.
        String _dlg_text_popup;
        String _dlg_title_popup;

        _dlg_text_popup = (IsGamingTable()) ? "STR_CAGE_CLOSE_GAMING_TABLE_USER_MSG_CONFIRMATION" : "STR_CAGE_CLOSE_SESSION_USER_MSG_CONFIRMATION";
        _dlg_title_popup = (IsGamingTable()) ? "STR_CAGE_GAMING_TABLE_CLOSE_TITLE" : "STR_VOUCHER_CASH_DESK_CLOSE_TITLE";

        if (!SessionStats.MsgUnbalanceAccept)
        {
        if (DialogResult.OK != frm_message.Show(Resource.String(_dlg_text_popup),
                                   Resource.String(_dlg_title_popup, _gaming_table_name),
                                   MessageBoxButtons.YesNo,
                                   MessageBoxIcon.Warning, this.ParentForm)) // �Desea Cerrar la caja? / �Desea cerrar la mesa de juego? 
        {
            _session_status = CASHIER_SESSION_CLOSE_STATUS.OK;
          return;
        }
        }

        foreach (KeyValuePair<CageDenominationsItems.CageDenominationKey, CageDenominationsItems.CageDenominationsItem> _item in AmountInput.m_cage_amounts)
        {
          _cage_amounts_for_voucher.Add(_item.Key, _item.Value);
        }

        // Get Sql Connection 
        using (DB_TRX _db_trx = new DB_TRX())
        {
          Operations.DB_InsertOperation(OperationCode.CLOSE_OPERATION, 0, CashierSessionInfo.CashierSessionId,
                                              0, 0, 0, 0, 0, 0, string.Empty, out _operation_id, _db_trx.SqlTransaction);

          // Cage Logic - Movements when close cashier
          if (Cage.IsCageEnabled())
          {
            // Save Closing Stock
            if (ClosingStock != null)
            {
              if (!ClosingStock.Save(AmountInput.m_cage_amounts, "ISO_CODE", "DENOMINATION", "CAGE_CURRENCY_TYPE_ENUM", "CHIP_ID", "UN", "TOTAL", _db_trx.SqlTransaction))
              {
                return;
              }

              // On Fixed bank and sleeps on table recalculate the cashiersessionstats collected amount because on closing the value is recalculating
              // but the credit/fill proposed is not considering on the previous recalcul of the closing
              if (ClosingStock.Type == ClosingStocks.ClosingStockType.FIXED && ClosingStock.SleepsOnTable)
              {
                SessionStats = new WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS();
                WSI.Common.Cashier.ReadCashierSessionData(_db_trx.SqlTransaction, CashierSessionInfo.CashierSessionId, ref SessionStats);

                foreach (KeyValuePair<CurrencyIsoType, Decimal> _amounts in ClosingAmount)
                {
                  if (_amounts.Value > 0)
                  {
                    SessionStats.collected[_amounts.Key] = _amounts.Value;
                  }
                }
              }

              // If chips/cash sleeps on table clear all amount to avoid the movement to cage
              if (ClosingStock.SleepsOnTable)
              {
                AmountInput.m_cage_amounts.Clear();
              }
            }

            // JCA 06-FEB-2014 For Collection from Cash Cage, can set the number of Tickets
            if (WSI.Common.TITO.Utils.IsTitoMode() || Misc.IsWassMode())
            {
              if (!GeneralParam.GetBoolean("TITO", "Cashier.TicketsCollection"))
              {
                // DRV 30-NOV-2013 Sets Money Collection Status to Pending Collection

                MoneyCollection.GetCashierMoneyCollectionId(CashierSessionInfo.CashierSessionId, out _money_collection_id, _db_trx.SqlTransaction);
                _money_collection = MoneyCollection.DB_Read(_money_collection_id, _db_trx.SqlTransaction);
                if (_money_collection.DB_ChangeStatusToPendingClosing(_db_trx.SqlTransaction))
                {
                  if (!AddTitoTicketsAmount(_money_collection_id, _db_trx.SqlTransaction))
                  {
                    return;
                  }
                }
                else
                {
                  return;
                }
              }
              else
              {
                MoneyCollection.GetCashierMoneyCollectionId(CashierSessionInfo.CashierSessionId, out _money_collection_id, _db_trx.SqlTransaction);
                _money_collection = MoneyCollection.DB_Read(_money_collection_id, _db_trx.SqlTransaction);
                if (!_money_collection.DB_ChangeStatusToPendingClosing(_db_trx.SqlTransaction))
                {
                  Log.Error("uc_bank::DB_ChangeStatusToPendingClosing. Money Collection status not updated. CollectionId: " + _money_collection.ToString());
                  return;
                }
              }
            }

            // Do movement to cage when cash/chips don't sleeps on table
            if (!ClosingStock.SleepsOnTable)
            {
              if (!CageLogicCloseFillOut(AmountInput.m_cage_amounts, CASHIER_MOVEMENT.CAGE_CLOSE_SESSION, CageSessionId, _db_trx.SqlTransaction, out _cage_movement_id, _operation_id))
              {
                return;
              }
            }

            //JPJ 17/03/2015 TASK 653
            if (!CageMeters.CashierSessionClose_UpdateCageMeters(CashierSessionInfo.CashierSessionId, out _concepts_cashier_movements, _db_trx.SqlTransaction))
            {
              return;
            }

            if (GamingTablesSessions.GamingTableInfo.HasDropbox)
            {
              if (!CageLogicCloseGamingTableDropbox(CageSessionId, _db_trx.SqlTransaction, _operation_id, _drop_box_amount_input == null ? null : _drop_box_amount_input.m_cage_amounts))
              {
                return;
              }
            }
          }

          //GamingTables Mode
          if (IsGamingTable())
          {
            Decimal _total_national_amount;


            _total_national_amount = 0;
            // DLL & RCI 27-FEB-2014: For integrated gaming tables, don't take into account _total_amount (can include Recharges/Redeems).
            //                        For money win: take into account Sales/Purchases chips netwin.
            //                        For chips: take into account the final - initial chips balance.
            GamingTablesSessions.GetCollectedAmount(SessionStats, out _total_amounts, out _total_national_amount, out _chips_netwin, _db_trx.SqlTransaction);

            //UpdateSesionMesa or CloseSession (_amount �n� de Visitas�, _db_trx)
            if (!GamingTablesSessions.GetOrOpenSession(out _gt_session, CashierSessionInfo.CashierSessionId, _db_trx.SqlTransaction))
            {
              // todo : show message?

              return;
            }

            if (!_gt_session.CloseSession(_total_amounts, _total_national_amount, _amount_visits, CageMeters.GetTipOfChips(_concepts_cashier_movements), SessionStats.collected, _db_trx.SqlTransaction))
            {
              // todo : show message?

              return;
            }

            // DHA 04-JUL-2016
            if (ClosingStock != null && ClosingStock.SleepsOnTable)
            {
              if (!ClosingStock.AddFillerOutMovement(CashierSessionInfo, ClosingAmount, _gt_session, _operation_id, _db_trx))
              {
                return;
              }
            }

            // FBA & RRR 15-JAN-2014: Added movement to inform gaming colleted in gaming tables.
            if (!FeatureChips.CreateGameCollectedMovements(CashierSessionInfo, ClosingAmount, _gt_session.GamingTableSessionId, true, _operation_id, _db_trx.SqlTransaction))
            {
              return;
            }

            // Alarm: Gaming table with session loss for 
            _session_alarm_amount = GamingTableAlarms.GetSessionLossFor();
            if (_session_alarm_amount >= 0)
            {
              _session_alarm_amount *= -1;
          }
            _session_win_loss_amount = 0;

            if (_session_alarm_amount != 0)
          {
              if (GamingTableAlarms.CheckForAlarm_SessionLossFor(CashierSessionInfo, _session_alarm_amount, out _session_win_loss_amount, _db_trx.SqlTransaction))
              {
                if (_session_win_loss_amount <= _session_alarm_amount)
                {

                  // Insert Alarm
                  _alarm_description = Resource.String("STR_ALARM_GAMBLING_TABLES_SESSION_LOSS_FOR", _session_win_loss_amount * -1, _session_alarm_amount * -1);

                  Alarm.Register(AlarmSourceCode.Cashier,
                                 CashierSessionInfo.UserId,
                                 CashierSessionInfo.UserName + "@" + CashierSessionInfo.TerminalName,
                                 (Int32)AlarmCode.User_GamnigTables_SessionLossFor,
                                 _alarm_description,
                                 AlarmSeverity.Warning,
                                 WGDB.Now, _db_trx.SqlTransaction);
          }
              }
            }
            // End Alarm: Gaming table with session loss for 

            // Alarm: Gaming table 3 day lost
            _gambling_table_3_days_lost = GamingTableAlarms.GetGamblingTable3DayLost();
            if (_gambling_table_3_days_lost >= 0)
            {
              _gambling_table_3_days_lost *= -1;
            }
            _gambling_table_3_days_win_loss_amount = 0;

            if (_gambling_table_3_days_lost != 0)
            {
              if (GamingTableAlarms.CheckForAlarm_GamblingTable3DaysLossFor(CashierSessionInfo, _gambling_table_3_days_lost, out _gambling_table_3_days_win_loss_amount, _db_trx.SqlTransaction))
              {
                if (_gambling_table_3_days_win_loss_amount <= _gambling_table_3_days_lost)
                {

                  // Insert Alarm
                  _alarm_description = Resource.String("STR_ALARM_GAMBLING_TABLES_3_DAYS_LOSS_FOR", _gambling_table_3_days_win_loss_amount * -1, _gambling_table_3_days_lost * -1);

                  Alarm.Register(AlarmSourceCode.Cashier,
                                 CashierSessionInfo.UserId,
                                 CashierSessionInfo.UserName + "@" + CashierSessionInfo.TerminalName,
                                 (Int32)AlarmCode.User_GamnigTables_GamingTable3DayLost,
                                 _alarm_description,
                                 AlarmSeverity.Warning,
                                 WGDB.Now, _db_trx.SqlTransaction);
                }
              }
            }
            // End Alarm: Gaming table 3 day lost

            if (GamingTableBusinessLogic.IsGamingTablesEnabledAndPlayerTracking())
            {
              int _gamingTableID;
              CashierSessionInfo _session;

              _session = Cashier.CashierSessionInfo();
              if (this.GamingTableId != null)
              {
                if (this.GamingTableId.Value > 0)
                {
                  if (int.TryParse(this.GamingTableId.Value.ToString(), out _gamingTableID))
                  {
                    GamingTableBusinessLogic.CloseAllGTPlaySession(_gamingTableID, _session, _db_trx.SqlTransaction);
          }
                }
              }
            }
          }

          SessionStats.ClosingStock = ClosingStock;

          _session_status = Common.Cashier.CashierSessionClose(SessionStats, CashierSessionInfo,
                                                               CashierSessionInfo.UserId, out _voucher_list, _db_trx.SqlTransaction, _operation_id);

          if (_session_status != CASHIER_SESSION_CLOSE_STATUS.OK)
          {
            return;
          }

          // JCA 06-FEB-2014 For Collection from Cash Cage, Tickets and MONEY_COLLECTION must be set as pending
          //if (GeneralParam.GetBoolean("TITO", "TITOMode"))
          //{
          //if (!GeneralParam.GetBoolean("TITO", "Cashier.TicketsCollection"))
          //{
          //  if (!MoneyCollection.GetCashierMoneyCollectionId(Cashier.SessionId, out _money_collection_id, _db_trx.SqlTransaction))
          //  {
          //    return;
          //  }

          //  if (!MoneyCollection.DB_MarkAsCollected(_money_collection_id, _db_trx.SqlTransaction))
          //  {
          //    return;
          //  }
          //}
          //else
          //{
          //  // DRV 30-NOV-2013 Sets Money Collection Status to Pending Collection
          //  MoneyCollection.GetCashierMoneyCollectionId(Cashier.SessionId, out _money_collection_id, _db_trx.SqlTransaction);
          //  _money_collection = MoneyCollection.DB_Read(_money_collection_id, _db_trx.SqlTransaction);
          //  _money_collection.DB_ChangeStatusToPendingClosing(_db_trx.SqlTransaction);
          //}
          // JCA 06-FEB-2014 For Collection from Cash Cage, Tickets and MONEY_COLLECTION must be set as pending
          //}

          //Create Cage Ticket
          if (Cage.IsCageEnabled())
          {
            VoucherCashDeskWithdraw voucher_cage_fill_out;
            VoucherCageFillInFillOutBody voucher_cage_body;

            // LTC 05-AGO-2016
            if (Misc.IsBankCardCloseCashEnabled() &&
                  Withdrawal.Type == CurrencyExchangeType.CARD)
            {
              Withdrawal.InsertReconciliationMovements(_operation_id);  //EOR 03-AUG-2017
              Withdrawal.GetPinpadMovementsNoReconciliated(CashierSessionInfo.CashierSessionId);
            }

            voucher_cage_fill_out = new VoucherCashDeskWithdraw(CASHIER_MOVEMENT.CLOSE_SESSION, PrintMode.Print, _db_trx.SqlTransaction, CashierVoucherType.NotSet);

            voucher_cage_body = new VoucherCageFillInFillOutBody(CASHIER_MOVEMENT.CLOSE_SESSION, _cage_amounts_for_voucher);
            voucher_cage_fill_out.SetParameterValue("@VOUCHER_CASH_DESK_WITHDRAW_BODY", voucher_cage_body.VoucherHTML);
            _voucher_list.Add(voucher_cage_fill_out);
            //Saving voucher
            voucher_cage_fill_out.Save(_operation_id, _db_trx.SqlTransaction);
          }

          if (Cage.IsCageEnabled())
          {
            if (!CageMeters.CashierSessionClose_UpdateCageSystemMeters(CashierSessionInfo.CashierSessionId, _db_trx.SqlTransaction))
            {
              return;
            }
          }

          if (IsGamingTable() && GamingTableBusinessLogic.IsGamingTablesEnabledAndPlayerTracking())
          {
            foreach (var _item in SeatToClose)
            {
              GamingTableBusinessLogic.EndGTPlaySession(_item, Cashier.UserId, Cashier.TerminalId, Cashier.TerminalName, _db_trx.SqlTransaction);
            } 
          }

          _db_trx.Commit();

        }

        VoucherPrint.Print(_voucher_list);

        // Only set if there is a local cashier session
        if (Cashier.TerminalId == CashierSessionInfo.TerminalId)
        {
          CashierSessionInfo.CashierSessionId = 0;
          Cashier.SetUserSession(0);
          m_cashier_session_info.CashierSessionId = 0;
        }

        // Re-enable buttons if session was expired
        if (IsSessionExpired())
        {
          this.parent_window.uc_card1.Enabled = true;
          this.parent_window.uc_bank1.DisableButton(CASHIER_STATUS.CLOSE);
          this.parent_window.uc_mobile_bank1.EnableDisableButtons(CASHIER_STATUS.CLOSE);
          this.parent_window.uc_gift_delivery1.Enabled = true;
          this.parent_window.uc_options1.Enabled = true;
          this.parent_window.uc_ticket_view.Enabled = true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        _session_status = CASHIER_SESSION_CLOSE_STATUS.ERROR;
      }
      finally
      {
        form_yes_no.Hide();
        this.RestoreParentFocus();

        if (_session_status == CASHIER_SESSION_CLOSE_STATUS.ALREADY_CLOSED)
        {
          // Only set if there is a local cashier session
          if (Cashier.TerminalId == CashierSessionInfo.TerminalId)
          {
            CashierSessionInfo.CashierSessionId = 0;
            Cashier.SetUserSession(0);
            m_cashier_session_info.CashierSessionId = 0;
          }

          frm_message.Show(Resource.String("STR_UC_BANK_CASH_WARNING_ALREADY_CLOSED",
                           Cashier.UserName),
                           Resource.String("STR_UC_BANK_BTN_CASH_DESK_CLOSE"),
                           MessageBoxButtons.OK,
                           MessageBoxIcon.Warning,
                           this.ParentForm);

          Log.Warning("CashierSessionClose: User " + CashierSessionInfo.UserName + " has already closed his/her cashier session.");
        }
        // Enable left menu buttons when the cashier session is closed after a work shift has expired
        else if (this.parent_window != null && this.parent_window.SystemInfo.WorkShiftExpired == SystemInfo.StatusInfo.EXPIRED &&
                 _session_status == CASHIER_SESSION_CLOSE_STATUS.OK && CashierSessionInfo.UserType != GU_USER_TYPE.SYS_GAMING_TABLE)
        {
          this.parent_window.EnableMenuButtons(true);
        }
        else if (_session_status == CASHIER_SESSION_CLOSE_STATUS.ERROR)
        {
          frm_message.Show(Resource.String("STR_UC_BANK_CASH_WARNING_ERROR_CLOSED"),
                           Resource.String("STR_UC_BANK_BTN_CASH_DESK_CLOSE"),
                           MessageBoxButtons.OK,
                           MessageBoxIcon.Warning,
                           this.ParentForm);
      }
      }

    } // CashierSessionClose

    //------------------------------------------------------------------------------
    // PURPOSE : Close the Cashier session.
    //
    //  PARAMS :
    //      - INPUT :
    //          - MoneyCollectionId:  Int64
    //          - SqlTrx:             SqlTransaction
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private static Boolean AddTitoTicketsAmount(Int64 MoneyCollectionId, SqlTransaction SqlTrx)
    {
      int _num_tickets;
      String _iso_code;
      String _denomination_showed;
      DataTable _dt_TITO_amounts;
      CageDenominationsItems.CageDenominationsItem _TITO_tickets_cage_dictionary_item;

      try
      {
        _iso_code = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode");
        _num_tickets = MoneyCollection.DB_CountPendingTickets(SqlTrx, MoneyCollectionId);

        if (_num_tickets > 0)
        {
          if (!AmountInput.m_cage_amounts.ContainsKey(_iso_code, CageCurrencyType.Bill))
          {
            _dt_TITO_amounts = Cage.CageAmountsInitTable();
            _TITO_tickets_cage_dictionary_item = new CageDenominationsItems.CageDenominationsItem();
            _TITO_tickets_cage_dictionary_item.AllowedFillOut = true;
            _TITO_tickets_cage_dictionary_item.TotalAmount = 0;
            _TITO_tickets_cage_dictionary_item.ItemAmounts = _dt_TITO_amounts;

            AmountInput.m_cage_amounts.Add(_iso_code, CageCurrencyType.Bill, _TITO_tickets_cage_dictionary_item);
          }

          _TITO_tickets_cage_dictionary_item = AmountInput.m_cage_amounts[_iso_code, CageCurrencyType.Bill];

          _denomination_showed = Cage.GetDenomination(Cage.TICKETS_CODE, _iso_code);

          Cage.CageAmountsPrepareRow(ref _TITO_tickets_cage_dictionary_item.ItemAmounts,
                                     null,
                                                                                         0,
                                                                                         _iso_code,
                                                                                         Cage.TICKETS_CODE,
                                                                                         true,
                                                                                         _denomination_showed,
                                                                                         CageCurrencyType.Bill,
                                                                                         CurrencyExchangeType.CURRENCY,
                                                                                         null,
                                                                                         null,
                                                                                         _num_tickets,
                                                                                         _num_tickets.ToString(),
                                                                                         DBNull.Value,
                                                                                         DBNull.Value); //Set bill currency type  because is default value

          AmountInput.m_cage_amounts[_iso_code, CageCurrencyType.Bill] = _TITO_tickets_cage_dictionary_item;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return false;
      }

      return true;
    } // AddTitoTicketsAmount

    //------------------------------------------------------------------------------
    // PURPOSE : Operations for cash desk:
    //             - Fill In (Dep�sito)
    //             - Fill Out (Retiro)
    //  PARAMS :   
    //      - INPUT :
    //          - MovementType:   CASHIER_MOVEMENT
    //          - CageSessionId:  long
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void AddFillInOutMovement(CASHIER_MOVEMENT MovementType, long CageSessionId)
    {
      AddFillInOutMovement(MovementType, CageSessionId, null);
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Operations for cash desk:
    //             - Fill In (Dep�sito)
    //             - Fill Out (Retiro)
    //  PARAMS :   
    //      - INPUT :
    //          - MovementType:   CASHIER_MOVEMENT
    //          - CageSessionId:  long
    //          - ClosingStocks:  ClosingStocks
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void AddFillInOutMovement(CASHIER_MOVEMENT MovementType, long CageSessionId, ClosingStocks ClosingStocks)
    {
      VoucherCashDeskOpen _voucher_fill_in;
      VoucherCashDeskWithdraw _voucher_fill_out;
      VoucherCashDeskWithdrawCard _voucher_fill_out_card;
      VoucherCashDeskWithdrawBody _voucher_body;
      VoucherCashDeskWithdrawCardBody _voucher_card_body;
      VoucherCashDeskOpenBody _voucher_open_body;
      VoucherCageFillInFillOutBody _voucher_cage_body;
      VoucherCashDeskOpen _voucher_cage_fill_in;
      VoucherCashDeskWithdraw _voucher_cage_fill_out;
      Boolean _voucher_saved;
      String _operation_name;
      Boolean _ask_to_user;
      SortedDictionary<CurrencyIsoType, Decimal> _fill_amount;
      VoucherTypes _voucher_type;
      Boolean _dummy_check_redeem;
      Boolean _is_amount_request;
      Int64 _request_movement_id;
      WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS _dummy_session_stats;
      CashierSessionInfo _cashier_session_info;
      CurrencyExchange _currency_exchange;
      CurrencyExchangeResult _exchange_result;

      Decimal _balance_currency;
      CurrencyIsoType _iso_type;
      String _national_currency;

      Boolean _cage_enabled;
      Boolean _cage_automode;
      Cage.CageStatus _status;
      Boolean _created_ok;

      GamingTablesSessions _gt_session;
      GTS_UPDATE_TYPE _gts_update_type;
      SortedDictionary<CurrencyIsoType, Decimal> _total_amounts;
      StringBuilder _sql_update_stock;

      Boolean _has_pending_movements;
      Decimal _aux_amount;

      Int64 _operation_id;

      ArrayList _voucher_list;

      VoucherCashDeskOpen _voucher_cash_open;
      Int32  _gaming_table_id_aux;

      // RAB 05-MAY-2016: PBI 12736: Review 23
      String _gaming_table_name;
      _gaming_table_name = "";
      m_is_first_deposit = false;
      _cashier_session_info = CashierSessionInfo.Copy();
      if (IsGamingTable())
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          GamingTablesSessions.GetGamingTableName(m_gaming_table_id.Value, out _gaming_table_name, _db_trx.SqlTransaction);
        }
      }

      _voucher_type = VoucherTypes.AmountRequest;
      _fill_amount = new SortedDictionary<CurrencyIsoType, Decimal>(new CurrencyIsoType.SortComparer());
      _iso_type = new CurrencyIsoType();
      _national_currency = CurrencyExchange.GetNationalCurrency();

      _is_amount_request = false;
      _request_movement_id = -1;

      _voucher_saved = false;
      _operation_name = "";
      _ask_to_user = false;

      _cage_enabled = Cage.IsCageEnabled();
      _cage_automode = Cage.IsCageAutoMode();

      _gts_update_type = 0;
      _total_amounts = new SortedDictionary<CurrencyIsoType, Decimal>(new CurrencyIsoType.SortComparer());

      _operation_id = 0;

      _has_pending_movements = false;

      _voucher_list = new ArrayList();
      _gaming_table_id_aux = (m_gaming_table_id == null? 0 : m_gaming_table_id.Value);

      try
      {
        switch (MovementType)
        {
          case CASHIER_MOVEMENT.FILLER_IN:
            _operation_name = IsGamingTable() ? Resource.String("STR_UC_BANK_BTN_CASH_DESK_DEPOSIT_GAMING_TABLE", _gaming_table_name) : Resource.String("STR_UC_BANK_BTN_CASH_DESK_DEPOSIT");

            _ask_to_user = true;
            _voucher_type = VoucherTypes.FillIn;
            break;

          case CASHIER_MOVEMENT.FILLER_OUT:
            _operation_name = IsGamingTable() ? Resource.String("STR_UC_BANK_BTN_CASH_DESK_WITHDRAW_GAMING_TABLE", _gaming_table_name) : Resource.String("STR_UC_BANK_BTN_CASH_DESK_WITHDRAW");

            if (ClosingStocks != null)
            {
              _operation_name = IsGamingTable() ? Resource.String("STR_UC_BANK_BTN_CASH_DESK_WITHDRAW_GAMING_TABLE_CLOSING_STOCK", _gaming_table_name) : Resource.String("STR_UC_BANK_BTN_CASH_DESK_WITHDRAW");
            }

            _ask_to_user = true;
            _voucher_type = VoucherTypes.FillOut;
            break;

          case CASHIER_MOVEMENT.NOT_ASSIGNED:
            _operation_name = IsGamingTable() ? Resource.String("STR_UC_BANK_BTN_AMOUNT_REQUEST_GAMING_TABLE", _gaming_table_name) : Resource.String("STR_UC_BANK_BTN_AMOUNT_REQUEST");
            _ask_to_user = true;
            _is_amount_request = true;
            break;

          case CASHIER_MOVEMENT.FILLER_IN_CLOSING_STOCK:
            _operation_name = IsGamingTable() ? Resource.String("STR_UC_BANK_BTN_CASH_DESK_OPENING_GAMING_TABLE", _gaming_table_name) : Resource.String("STR_UC_BANK_BTN_CASH_DESK_DEPOSIT");
            _ask_to_user = true;
            _voucher_type = VoucherTypes.FillIn;
            break;

          default:
            break;
        }

        form_yes_no.Show(this.ParentForm);

        // Show form for to input amount
        if (_ask_to_user)
        {
          Boolean _db_error;

          _db_error = false;
          _status = Cage.CageStatus.Sent;

          _has_pending_movements = HasPendingMovements(_status, out _db_error);
          if (!_has_pending_movements && _db_error)
          {
            frm_message.Show(Resource.String("STR_FRM_DATABASE_CFG_ERROR"),
                             Resource.String("STR_APP_GEN_MSG_ERROR"),
                             MessageBoxButtons.OK,
                             MessageBoxIcon.Warning,
                             this.ParentForm);
            return;
          }

          if (MovementType == CASHIER_MOVEMENT.FILLER_IN && !_has_pending_movements && _cage_enabled && !_cage_automode)
          {
            frm_message.Show(Resource.String("STR_CAGE_NOT_PENDING_MOVEMENTS"),
                             Resource.String("STR_CAGE_MOVEMENTS"),
                             MessageBoxButtons.OK,
                             MessageBoxIcon.Warning,
                             this.ParentForm);

            return;
          }

          // Show if there are pending movements
          if (MovementType == CASHIER_MOVEMENT.NOT_ASSIGNED && _has_pending_movements && _cage_enabled)
          {
            frm_message.Show(Resource.String("STR_UC_BANK_HAS_PENDING_MOVEMENTS").Replace("\\r\\n", "\r\n"),
                             Resource.String("STR_APP_GEN_MSG_WARNING"),
                             MessageBoxButtons.OK,
                             MessageBoxIcon.Information,
                             this.ParentForm);
            return;
          }

          // Show if there are pending requests
          if (MovementType == CASHIER_MOVEMENT.NOT_ASSIGNED && AmountInput.HasPendingRequests(Cage.CageOperationType.RequestOperation, out _request_movement_id) && _cage_enabled)
          {
            frm_message.Show(Resource.String("STR_UC_BANK_HAS_AMOUNT_REQUEST").Replace("\\r\\n", "\r\n"),
                             Resource.String("STR_UC_BANK_BTN_AMOUNT_REQUEST"),
                             MessageBoxButtons.OK,
                             MessageBoxIcon.Information,
                             this.ParentForm);

          }

          AmountInput.Show(_operation_name, out _fill_amount, out _dummy_check_redeem, _voucher_type, null,
                            m_total_amount, CASH_MODE.TOTAL_REDEEM, form_yes_no, out _dummy_session_stats,
                            _is_amount_request, _request_movement_id, _has_pending_movements, _cashier_session_info,
                            ClosingStocks);
          this.RestoreParentFocus();

          // TODO: Se setea dos veces el estatus del movimiento cuando este se cancela
          // CageLogicOpenCashNoOk; 
          if (AmountInput.DialogResult != DialogResult.OK)
          {
            return;
          }
        }
        // this else is for undo withdrawals, _ask_to_user = false because the user hasn't to be asked about quantities, 
        // because just the last withdrawal can be undo it. the user can't choose any quantity.
        else
        {

        }

        // Insert movement
        CashierMovementsTable _cm_mov_table;

        _voucher_fill_out = null;
        _voucher_fill_out_card = null;
        _voucher_fill_in = null;
        _voucher_cage_fill_in = null;
        _voucher_cage_fill_out = null;
        _gt_session = null;
        _voucher_cash_open = null;

        using (DB_TRX _db_trx = new DB_TRX())
        {
          try
          {
            #region Try

            switch (MovementType)
            {
              case CASHIER_MOVEMENT.FILLER_IN_CLOSING_STOCK:
                if (ClosingStocks != null
                  && (ClosingStocks.Type == Common.ClosingStocks.ClosingStockType.ROLLING || ClosingStocks.Type == Common.ClosingStocks.ClosingStockType.FIXED)
                  && ClosingStocks.SleepsOnTable)
                {
                  if (m_gaming_table_id == null && GamingTablesSessions.GetGamingTableInfo(Convert.ToInt32(m_cashier_session_info.TerminalId), _db_trx.SqlTransaction))
                  {
                    _gaming_table_id_aux = GamingTablesSessions.GamingTableInfo.GamingTableId;
                  }

                  if (!GamingTablesSessions.GetOpenedSession(_gaming_table_id_aux, out _gt_session, _db_trx.SqlTransaction))
                  {
                    return;
                  }

                  if (_gt_session != null && _gt_session.CashierSessionId >= 0)
                  {
                    return;
                  }
                }
                break;

              case CASHIER_MOVEMENT.FILLER_IN:
                if (ClosingStocks != null && ClosingStocks.Type == Common.ClosingStocks.ClosingStockType.FIXED && !ClosingStocks.SleepsOnTable)
                {
                  if (!GamingTablesSessions.GetOpenedSession(m_gaming_table_id.Value, out _gt_session, _db_trx.SqlTransaction))
                  {
                    return;
                  }
                  if (_gt_session != null && _gt_session.CashierSessionId >= 0)
                  {
                    return;
                  }
                }
                break;

              default:
                break;
            }

            if (_cashier_session_info.CashierSessionId <= 0)
            {
              long csid = 0;
              OpenGamingTable(null, out _gt_session, out _voucher_cash_open, _db_trx.SqlTransaction,out csid);
              if (csid <= 0)
              {
                return;
              }

              //_cashier_session_info = CashierSessionInfo;

              _cashier_session_info.CashierSessionId = csid;

              //CashierSessionInfo.CashierSessionId = csid;

              //CashierSessionInfo = _cashier_session_info.Copy();
              if (_voucher_cash_open != null)
              {
                _voucher_list.Add(_voucher_cash_open);
              }
            }
            else if (IsGamingTable())
            {
              if (!GamingTablesSessions.GetOpenedSession(m_gaming_table_id.Value, out _gt_session, _db_trx.SqlTransaction))
              {
                return;
              }
            }

            if (_gt_session != null && !_gt_session.IsFirstDeposit)
            {
              _gt_session.IsFirstDeposit = m_is_first_deposit;
            }

            _cm_mov_table = new CashierMovementsTable(_cashier_session_info);
            //_cashier_session_info = CashierSessionInfo;

            // ATB 11-JAN-2017
            // To control if there are one or more cards to reconciliate
            bool _is_card_type;
            Decimal _total = 0;
            Cage.GetAmountFromMovement(AmountInput.m_cage_amounts, _cashier_session_info, out _total);

            switch (MovementType)
            {
              case CASHIER_MOVEMENT.FILLER_IN:
                _voucher_fill_in = new VoucherCashDeskOpen(CASHIER_MOVEMENT.FILLER_IN, CashierVoucherType.CashDeposit, PrintMode.Print, _db_trx.SqlTransaction);
                if (_cage_enabled)
                {
                  _voucher_cage_fill_in = new VoucherCashDeskOpen(CASHIER_MOVEMENT.FILLER_IN, PrintMode.Print, _db_trx.SqlTransaction);
                }

                Operations.DB_InsertOperation(OperationCode.CASH_DEPOSIT, 0, _cashier_session_info.CashierSessionId,
                                              0, 0, _total, 0, 0, 0, string.Empty, out _operation_id, _db_trx.SqlTransaction);
                break;
              case CASHIER_MOVEMENT.FILLER_OUT:
              case CASHIER_MOVEMENT.FILLER_OUT_CHECK:
                // ATB 11-JAN-2017                
                _is_card_type = false;
                foreach (KeyValuePair<CurrencyIsoType, decimal> _type in _fill_amount)
                {
                  if (_type.Key.Type == CurrencyExchangeType.CARD)
                  {
                    _is_card_type = true;
                  }
                }
                // Fill Out: Retirar
                //insertar account_operation ???
                // LTC 05-AGO-2016
                // ATB 11-JAN-2017
                // In case of reconciliate cards, is also a valid option to generate the voucher
                if (MovementType == CASHIER_MOVEMENT.FILLER_OUT &&
                    Misc.IsBankCardWithdrawalEnabled() &&
                        (Withdrawal.Type == CurrencyExchangeType.CARD || _is_card_type))
                {
                  _voucher_fill_out_card = new VoucherCashDeskWithdrawCard(CASHIER_MOVEMENT.FILLER_OUT, PrintMode.Print, _db_trx.SqlTransaction, CashierVoucherType.CashWithdrawalCard);

                }

                _voucher_fill_out = new VoucherCashDeskWithdraw(CASHIER_MOVEMENT.FILLER_OUT, PrintMode.Print, _db_trx.SqlTransaction, CashierVoucherType.CashWithdrawal);
                if (_cage_enabled)
                {
                  _voucher_cage_fill_out = new VoucherCashDeskWithdraw(CASHIER_MOVEMENT.FILLER_OUT, PrintMode.Print, _db_trx.SqlTransaction);
                }

                Operations.DB_InsertOperation(OperationCode.CASH_WITHDRAWAL, 0, _cashier_session_info.CashierSessionId,
                                                0, 0, _total, 0, 0, 0, string.Empty, out _operation_id, _db_trx.SqlTransaction);
                break;

              case CASHIER_MOVEMENT.NOT_ASSIGNED:
                // TODO: si se desea crear el voucher habr�a que crear uno para CASHIER_MOVEMENT.NOT_ASSIGNED, de momento se usa
                //       CASHIER_MOVEMENT.FILLER_OUT
                //_voucher_fill_out = new VoucherCashDeskWithdraw(CASHIER_MOVEMENT.FILLER_OUT, PrintMode.Print, sql_trx);
                //if (_cage_enabled)
                //{
                //  _voucher_cage_fill_out = new VoucherCashDeskWithdraw(CASHIER_MOVEMENT.FILLER_OUT, PrintMode.Print, sql_trx);
                //}
                break;

              // REVISAR DHA: mirar que se ha de cambiar
              case CASHIER_MOVEMENT.FILLER_IN_CLOSING_STOCK:
                _voucher_fill_in = new VoucherCashDeskOpen(CASHIER_MOVEMENT.FILLER_IN_CLOSING_STOCK, CashierVoucherType.CashDeposit, PrintMode.Print, _db_trx.SqlTransaction);
                if (_cage_enabled)
                {
                  _voucher_cage_fill_in = new VoucherCashDeskOpen(CASHIER_MOVEMENT.FILLER_IN_CLOSING_STOCK, PrintMode.Print, _db_trx.SqlTransaction);
                }
                Operations.DB_InsertOperation(OperationCode.CASH_DEPOSIT, 0, _cashier_session_info.CashierSessionId,
                                            0, 0, _total, 0, 0, 0, string.Empty, out _operation_id, _db_trx.SqlTransaction);
                break;
            }

            if (IsGamingTable()
              && MovementType == CASHIER_MOVEMENT.FILLER_OUT)
            {
              if (!FeatureChips.CreateGameCollectedMovements(_cashier_session_info, _fill_amount, _gt_session.GamingTableSessionId, false, _operation_id, _db_trx.SqlTransaction))
              {
                return;
              }
            }

            // Insert cage movements
            Int64 _cage_movement_id;

            String _source_name;
            String _description;

            _cage_movement_id = 0;

            if (_cage_enabled)
            {
              switch (MovementType)
              {
                case CASHIER_MOVEMENT.NOT_ASSIGNED:
                  // TODO: Verificar que el movimiento no ha sido cancelado o atendido.
                  //////_cur_request_movement_id = _request_movement_id;
                  //////HasPendingRequests(Cage.CageOperationType.RequestOperation, out _request_movement_id);
                  //////if (_cur_request_movement_id != _request_movement_id)
                  //////{
                  //////  frm_message.Show(Resource.String("STR_UC_BANK_HAS_NOT_AMOUNT_REQUEST").Replace("\\r\\n", "\r\n"),
                  //////                   Resource.String("STR_UC_BANK_BTN_AMOUNT_REQUEST"),
                  //////                   MessageBoxButtons.OK,
                  //////                   MessageBoxIcon.Information,
                  //////                   this.ParentForm);

                  //////}
                  //////else
                  //////{
                  //////  // TODO: Hacer insert o un update con los datos obtenidos del formulario.
                  //////  InsertPendingRequest(AmountInput.m_cage_amounts);
                  //////}

                  if (!Cage.IsCageAutoMode() && InsertCageMovementToGUI(AmountInput.m_cage_amounts, Cage.CageOperationType.RequestOperation, Cage.CageStatus.Sent, out _cage_movement_id, _db_trx.SqlTransaction))
                  {
                    _db_trx.SqlTransaction.Commit();
                  }

                  return;

                case CASHIER_MOVEMENT.FILLER_IN:

                  Boolean _db_error;

                  // Verify if movement has been canceled during operation.
                  _status = Cage.CageStatus.Sent;
                  _db_error = false;
                  _has_pending_movements = HasPendingMovements(_status, _db_trx.SqlTransaction, out _db_error);

                  if (!_has_pending_movements && _db_error)
                  {
                    frm_message.Show(Resource.String("STR_FRM_DATABASE_CFG_ERROR"),
                                     Resource.String("STR_APP_GEN_MSG_ERROR"),
                                     MessageBoxButtons.OK,
                                     MessageBoxIcon.Warning,
                                     this.ParentForm);
                    return;
                  }

                  if (!_has_pending_movements)
                  {
                    if (!Cage.IsCageAutoMode())
                    {
                      frm_message.Show(Resource.String("STR_CAGE_CANCELED_PENDING_MOVEMENTS"),
                                       Resource.String("STR_CAGE_MOVEMENTS"),
                                       MessageBoxButtons.OK,
                                       MessageBoxIcon.Warning,
                                       this.ParentForm);

                      return;
                    }
                    else
                    {
                      //JCA Hacemos la l�gica que inserte el envio como si fuera original de b�veda
                      //    TODO: Crear recursos
                      //                - STR_FRM_CHIPS_ERROR_STOCK
                      //                - STR_CAGE_INSERT_MOVEMENT_ERROR

                      _sql_update_stock = new StringBuilder();
                      if (Cage.IsCageAutoModeCageStock() && !CheckCageStock(AmountInput.m_cage_amounts, MovementType, _db_trx.SqlTransaction))
                      {
                        if (AmountInput.get_cage_total_value == 0)
                        {
                          frm_message.Show(Resource.String("STR_FRM_CAGE_ERROR_STOCK_0"),
                                           Resource.String("STR_CAGE_MOVEMENTS"),
                                           MessageBoxButtons.OK,
                                           MessageBoxIcon.Warning,
                                           this.ParentForm);
                        }
                        else
                        {
                        frm_message.Show(Resource.String("STR_FRM_CAGE_ERROR_STOCK"),
                                         Resource.String("STR_CAGE_MOVEMENTS"),
                                         MessageBoxButtons.OK,
                                         MessageBoxIcon.Warning,
                                         this.ParentForm);
                        }
                        return;
                      }

                      // FAV 04-MAR-2016 
                      if (IsGamingTable() && Cashier.TerminalId != CashierSessionInfo.TerminalId)
                      {
                        _created_ok = Cage.CreateCageMovement(CageSessionId, _cashier_session_info, Cage.CageOperationType.ToGamingTable, Cage.CageStatus.OK, -1,
                                                                out _cage_movement_id, AmountInput.m_cage_amounts, MovementType, m_gaming_table_id.Value, _db_trx.SqlTransaction);
                      }
                      else
                      {
                        _created_ok = InsertCageMovementToGUI(AmountInput.m_cage_amounts, Cage.CageOperationType.ToCashier, Cage.CageStatus.OK, out _cage_movement_id, _db_trx.SqlTransaction);
                      }

                      if (!_created_ok)
                      {
                        frm_message.Show(Resource.String("STR_CAGE_NOT_SESSION_OPEN").Replace("\\r\\n", "\r\n"),
                                         Resource.String("STR_VOUCHER_CASH_DESK_FILL_OUT_TITLE_CAGE"),
                                         MessageBoxButtons.OK,
                                         MessageBoxIcon.Warning,
                                         this.ParentForm);
                        return;
                      }

                      _source_name = "";
                      _description = "";

                      if (Cage.InsertCageCurrenciesIntoCashierMovements(AmountInput.m_cage_amounts, CASHIER_MOVEMENT.CAGE_FILLER_IN, _cage_movement_id, _cashier_session_info, _operation_id, _db_trx.SqlTransaction))
                      {
                        if (AmountInput.m_cage_movement_status == Cage.CageStatus.OkDenominationImbalance)
                        {
                          _source_name = _cashier_session_info.UserName + "@" + _cashier_session_info.TerminalName;
                          _description = Resource.String("STR_EA_CASH_DESK_DENOMINATION_DIFF",
                                         Resource.String("STR_VOUCHER_CASH_DESK_FILL_IN_TITLE"));

                          Alarm.Register(AlarmSourceCode.User, _cashier_session_info.UserId, _source_name,
                                             (UInt32)AlarmCode.User_DenominationDepositUnbalanced, _description, AlarmSeverity.Warning, DateTime.MinValue, _db_trx.SqlTransaction);
                        }
                      }
                    }
                  }

                  // Movements for cage logic if fill In
                  if (AmountInput.m_current_pending_movement != 0)
                  {
                    if (CageLogicOpenCashOk(AmountInput.m_current_pending_movement, AmountInput.m_cage_amounts, AmountInput.m_cage_movement_status
                                            , CASHIER_MOVEMENT.CAGE_FILLER_IN, _db_trx.SqlTransaction))
                    {
                      AmountInput.m_current_pending_movement = 0;
                    }
                    else
                    {
                      frm_message.Show(Resource.String("STR_CAGE_MOVEMENTS_ERROR"),
                               Resource.String("STR_CAGE_MOVEMENTS"),
                               MessageBoxButtons.OK,
                               MessageBoxIcon.Warning,
                               this.ParentForm);
                      CageLogicOpenCashNoOk(AmountInput.m_current_pending_movement, WSI.Common.Cage.CageStatus.Sent, _db_trx.SqlTransaction);

                      return;
                    }
                  }

                  if (IsGamingTable())
                  {
                    if (_gt_session.IsFirstDeposit)
                    {
                      _gts_update_type = GTS_UPDATE_TYPE.OpenFillIn;
                    }
                    else
                    {
                      _gts_update_type = GTS_UPDATE_TYPE.FillIn;
                    }
                  }

                  break;

                case CASHIER_MOVEMENT.FILLER_OUT:
                  // Movements for cage logic if fill out
                  if (!CageLogicCloseFillOut(AmountInput.m_cage_amounts, CASHIER_MOVEMENT.CAGE_FILLER_OUT, CageSessionId, _db_trx.SqlTransaction, out _cage_movement_id, _operation_id))
                  {
                    return;
                  }

                  if (IsGamingTable())
                  {
                    _gts_update_type = GTS_UPDATE_TYPE.FillOut;
                  }
                  // insertar aqui el account operation

                  break;

                case CASHIER_MOVEMENT.FILLER_IN_CLOSING_STOCK:

                  _source_name = "";
                  _description = "";

                  if (Cage.InsertCageCurrenciesIntoCashierMovements(AmountInput.m_cage_amounts, CASHIER_MOVEMENT.CAGE_FILLER_IN, _cage_movement_id, _cashier_session_info, _operation_id, _db_trx.SqlTransaction))
                  {
                    if (AmountInput.m_cage_movement_status == Cage.CageStatus.OkDenominationImbalance)
                    {
                      _source_name = _cashier_session_info.UserName + "@" + _cashier_session_info.TerminalName;
                      _description = Resource.String("STR_EA_CASH_DESK_DENOMINATION_DIFF",
                                     Resource.String("STR_VOUCHER_CASH_DESK_FILL_IN_TITLE"));

                      Alarm.Register(AlarmSourceCode.User, _cashier_session_info.UserId, _source_name,
                                         (UInt32)AlarmCode.User_DenominationDepositUnbalanced, _description, AlarmSeverity.Warning, DateTime.MinValue, _db_trx.SqlTransaction);
                    }
                  }


                  if (IsGamingTable())
                  {
                    _gts_update_type = GTS_UPDATE_TYPE.OpenFillIn;
                  }

                  break;

                default:
                  return;
              }
            }

            foreach (KeyValuePair<CurrencyIsoType, Decimal> _amount in CurrencyExchange.OrderCurrenciesFromCurrencyConfiguration(_fill_amount))
            {
              _aux_amount = -1;
              switch (_amount.Key.Type)
              {
                case CurrencyExchangeType.CURRENCY:
                case CurrencyExchangeType.CASINOCHIP:
                case CurrencyExchangeType.CASINO_CHIP_RE:
                case CurrencyExchangeType.CASINO_CHIP_NRE:
                case CurrencyExchangeType.CASINO_CHIP_COLOR:

                  if (_amount.Key.IsoCode != _national_currency && _amount.Key.IsoCode != Cage.CHIPS_COLOR)
                  {
                    CurrencyExchange.ReadCurrencyExchange(_amount.Key.Type, _amount.Key.IsoCode, out _currency_exchange, _db_trx.SqlTransaction);
                    _currency_exchange.ApplyExchange(_amount.Value, out _exchange_result);
                    _aux_amount = _exchange_result.GrossAmount;
                  }

                  if (IsGamingTable()
                    && MovementType == CASHIER_MOVEMENT.FILLER_OUT)
                  {
                    // TODO: REVISAR DHA esto estar�a bien sacarlo fuera de la iteraci�n
                    ////////if (!GamingTablesSessions.GetOrOpenSession(out _gt_session, m_gaming_table_id.Value, _cashier_session_info.CashierSessionId, _db_trx.SqlTransaction))
                    ////////{
                    ////////  // todo �show error? �return?
                    ////////}

                    if (_national_currency == _amount.Key.IsoCode && !FeatureChips.IsChipsType(_amount.Key.Type))
                    {
                      _cm_mov_table.Add(_operation_id, MovementType, _fill_amount[_amount.Key], 0, String.Empty, String.Empty, String.Empty, 0, 0, _gt_session.GamingTableSessionId);
                    }
                    else
                    {
                      _cm_mov_table.Add(_operation_id, MovementType, _fill_amount[_amount.Key], 0, String.Empty, String.Empty, _amount.Key.IsoCode, 0, 0, _gt_session.GamingTableSessionId, _aux_amount, _amount.Key.Type, FeatureChips.ConvertToCageCurrencyType(_amount.Key.Type), null);
                    }
                  }
                  else
                  {
                    if (_amount.Key.IsoCode == _national_currency && !FeatureChips.IsChipsType(_amount.Key.Type))
                    {
                      _cm_mov_table.Add(_operation_id, MovementType, _fill_amount[_amount.Key], 0, "");
                    }
                    else
                    {
                      _cm_mov_table.Add(_operation_id, MovementType, _amount.Value, 0, String.Empty, String.Empty, _amount.Key.IsoCode, 0, 0, 0, _aux_amount, _amount.Key.Type, FeatureChips.ConvertToCageCurrencyType(_amount.Key.Type), null);
                    }
                  }
                  break;

                case CurrencyExchangeType.CHECK:

                  _cm_mov_table.Add(_operation_id, CASHIER_MOVEMENT.FILLER_OUT_CHECK, _amount.Value, 0, "", "", _amount.Key.IsoCode, 0, 0, 0, -1, _amount.Key.Type, null, null);
                  break;

                case CurrencyExchangeType.CARD:
                  _cm_mov_table.Add(_operation_id, CASHIER_MOVEMENT.FILLER_OUT, _amount.Value, 0, "", "", _amount.Key.IsoCode, -1, 0, 0, -1, _amount.Key.Type, null, null);
                  break;
              }

              if (_cashier_session_info.CashierSessionId == 0)
              {
                Log.Error("uc_bank.AddFillInOutMovement: Error close cashier session. Cashier session id -> " + _cashier_session_info.CashierSessionId);
                _cashier_session_info = CommonCashierInformation.CashierSessionInfo();
                return;
              }
              else if (_amount.Key.IsoCode == _national_currency && _amount.Key.Type == CurrencyExchangeType.CURRENCY)
              {
                _balance_currency = CashierBusinessLogic.UpdateSessionBalance(_db_trx.SqlTransaction, _cashier_session_info.CashierSessionId, 0);
              }
              else
              {
                _balance_currency = CashierBusinessLogic.UpdateSessionBalance(_db_trx.SqlTransaction, _cashier_session_info.CashierSessionId, 0, _amount.Key.IsoCode, _amount.Key.Type);
              }


              //initilize cashier_session_by_currency
              switch (MovementType)
              {
                case CASHIER_MOVEMENT.FILLER_IN:
                case CASHIER_MOVEMENT.FILLER_IN_CLOSING_STOCK:
                  _voucher_open_body = new VoucherCashDeskOpenBody(CASHIER_MOVEMENT.FILLER_IN, _amount.Key, _balance_currency, _amount.Value, m_show_balance);
                  _voucher_fill_in.SetParameterValue("@VOUCHER_CASH_DESK_OPEN_BODY", _voucher_open_body.VoucherHTML);
                  if (_cage_enabled)
                  {
                    _voucher_cage_body = new VoucherCageFillInFillOutBody(CASHIER_MOVEMENT.FILLER_IN, AmountInput.m_cage_amounts);
                    _voucher_cage_fill_in.SetParameterValue("@VOUCHER_CASH_DESK_OPEN_BODY", _voucher_cage_body.VoucherHTML);
                  }
                  break;
                case CASHIER_MOVEMENT.FILLER_OUT:
                case CASHIER_MOVEMENT.FILLER_OUT_CHECK:
                  // LTC 05-AGO-2016
                  if (MovementType == CASHIER_MOVEMENT.FILLER_OUT &&
                    Misc.IsBankCardWithdrawalEnabled() &&
                        _amount.Key.Type == CurrencyExchangeType.CARD)
                  {
                    _voucher_card_body = new VoucherCashDeskWithdrawCardBody(Convert.ToInt32(_amount.Value), Withdrawal.Count, Withdrawal.AmountTotal);
                    _voucher_fill_out_card.SetParameterValue("@VOUCHER_CASH_DESK_WITHDRAW_BODY", _voucher_card_body.VoucherHTML);
                    //LTC 23-AGO-2016
                    _voucher_body = new VoucherCashDeskWithdrawBody(_amount.Key, _balance_currency, Withdrawal.AmountTotal, m_show_balance);
                  }
                  else
                  {
                    _voucher_body = new VoucherCashDeskWithdrawBody(_amount.Key, _balance_currency, _amount.Value, m_show_balance);
                  }

                  _voucher_fill_out.SetParameterValue("@VOUCHER_CASH_DESK_WITHDRAW_BODY", _voucher_body.VoucherHTML);
                  if (_cage_enabled)
                  {
                    _voucher_cage_body = new VoucherCageFillInFillOutBody(MovementType, AmountInput.m_cage_amounts);
                    _voucher_cage_fill_out.SetParameterValue("@VOUCHER_CASH_DESK_WITHDRAW_BODY", _voucher_cage_body.VoucherHTML);
                  }

                  break;
              }
            }

            if (!_cm_mov_table.Save(_db_trx.SqlTransaction))
            {
              return;
            }

            // When systems has configured pinpad and the vouchers concilliation match with expected, Card session balance must be updated without the total amount card
            if (WSI.Common.PinPad.PinPadTransactions.GetCashierSessionHasPinPadOperation(_cashier_session_info.CashierSessionId, String.Empty, _db_trx.SqlTransaction))
            {
              foreach (KeyValuePair<CurrencyIsoType, Decimal> _amount in CurrencyExchange.OrderCurrenciesFromCurrencyConfiguration(_fill_amount))
              {
                if (_amount.Key.Type == CurrencyExchangeType.CARD && _amount.Value == Withdrawal.Count)
                {
                  _balance_currency = CashierBusinessLogic.UpdateSessionBalance(_db_trx.SqlTransaction, _cashier_session_info.CashierSessionId, -Withdrawal.AmountTotal, _amount.Key.IsoCode, _amount.Key.Type);
                }
              }
            }


            switch (MovementType)
            {
              case CASHIER_MOVEMENT.FILLER_IN:
              case CASHIER_MOVEMENT.FILLER_IN_CLOSING_STOCK:
                _voucher_fill_in.AddString("VOUCHER_CASH_DESK_OPEN_BODY", "");
                // - Save voucher
                _voucher_saved = _voucher_fill_in.Save(_operation_id, _db_trx.SqlTransaction);
                if (_voucher_saved && _cage_enabled)
                {
                  _voucher_saved = _voucher_cage_fill_in.Save(_operation_id, _db_trx.SqlTransaction);
                }
                break;
              case CASHIER_MOVEMENT.FILLER_OUT:
              case CASHIER_MOVEMENT.FILLER_OUT_CHECK:
                // ATB 11-JAN-2017
                _is_card_type = false;
                foreach (KeyValuePair<CurrencyIsoType, decimal> _type in _fill_amount)
                {
                  if (_type.Key.Type == CurrencyExchangeType.CARD)
                  {
                    _is_card_type = true;
                  }
                }

                if (MovementType == CASHIER_MOVEMENT.FILLER_OUT &&
                    Misc.IsBankCardWithdrawalEnabled() &&
                        Withdrawal.Type == CurrencyExchangeType.CARD)
                {
                  _voucher_fill_out_card.AddString("VOUCHER_CASH_DESK_WITHDRAW_BODY", "");
                  _voucher_saved = _voucher_fill_out_card.Save(_operation_id, _db_trx.SqlTransaction);

                }
                // ATB 11-JAN-2017
                // When one or more cards are selected to reconciliate, the system must register it
                // and not only when is a card type
                if (_voucher_saved || _is_card_type)
                {
                  Withdrawal.InsertReconciliationMovements(_operation_id);  //EOR 03-AUG-2017
                }
                _voucher_fill_out.AddString("VOUCHER_CASH_DESK_WITHDRAW_BODY", "");
                // - Save voucher

                _voucher_saved = _voucher_fill_out.Save(_operation_id, _db_trx.SqlTransaction);
                if (_voucher_saved && _cage_enabled)
                {
                  _voucher_saved = _voucher_cage_fill_out.Save(_operation_id, _db_trx.SqlTransaction);
                }

                break;
            }

            //GamingTables Mode 
            if (IsGamingTable())
            {
              //GamingTablesSessions.AcumCurrenciesAndChips(_fill_amount, out _total_chips, out _total_amount, sql_trx);
              Decimal _total_national_amount;
              GamingTablesSessions.AcumCurrenciesAndChips(_fill_amount, out _total_amounts, out _total_national_amount, _db_trx.SqlTransaction);

              //UPDATE TOTAL CHIPS IN GAMING TABLE SESION
              if (!_gt_session.UpdateSession(_gts_update_type, _total_amounts, _db_trx.SqlTransaction))
              {
                return;
              }

              if (_gts_update_type == GTS_UPDATE_TYPE.FillIn
                || _gts_update_type == GTS_UPDATE_TYPE.FillOut
                || _gts_update_type == GTS_UPDATE_TYPE.OpenFillIn
                || _gts_update_type == GTS_UPDATE_TYPE.CloseFillOut)
              {
                WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS _session_stats;

                _session_stats = new WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS();
                WSI.Common.Cashier.ReadCashierSessionData(_db_trx.SqlTransaction, _cashier_session_info.CashierSessionId, ref _session_stats);

                GamingTablesSessions.GetCollectedAmount(_session_stats, out _total_amounts, out _total_national_amount, _db_trx.SqlTransaction);

                // Update collected national amount in gaming table session
                if (!_gt_session.UpdateSession(GTS_UPDATE_TYPE.CollectedAmount, _total_national_amount, _db_trx.SqlTransaction))
                {
                  return;
                }

                // Update collected amount in gaming table session
                if (!_gt_session.UpdateSession(GTS_UPDATE_TYPE.CollectedAmount, _total_amounts, _db_trx.SqlTransaction))
                {
                  return;
                }

                // DLL 02-OCT-2014 Tips only are updated when close_cash

                //UPDATE TIPS IN GAMING TABLE SESSION
                //_tips_of_chips = GetTipOfChips(AmountInput.m_cage_amounts);
                //if (_tips_of_chips > 0)
                //{ 
                //  if (!_gt_session.UpdateSession(GTS_UPDATE_TYPE.Tips, _tips_of_chips, sql_trx))
                //  {
                //    // todo : show message?

                //    return;
                //  }
                //}
              }

            } // if

            _db_trx.Commit();
            CashierSessionInfo.CashierSessionId = _cashier_session_info.CashierSessionId;
            // RCI 21-JUL-2011: Print always after Commit.
            if (_voucher_saved)
            {
              if (MovementType == CASHIER_MOVEMENT.FILLER_IN ||
                   MovementType == CASHIER_MOVEMENT.FILLER_IN_CLOSING_STOCK)
              {
                _voucher_list.Add(_voucher_fill_in);
                if (_cage_enabled)
                {
                  _voucher_list.Add(_voucher_cage_fill_in);
                }
              }
              else
              {
                // LTC 05-AGO-2016
                if (MovementType == CASHIER_MOVEMENT.FILLER_OUT &&
                    Misc.IsBankCardWithdrawalEnabled() &&
                        Withdrawal.Type == CurrencyExchangeType.CARD)
                {
                  _voucher_list.Add(_voucher_fill_out_card);
                }

                _voucher_list.Add(_voucher_fill_out);
                if (_cage_enabled)
                {
                  _voucher_list.Add(_voucher_cage_fill_out);
                }
              }

              VoucherPrint.Print(_voucher_list);

            }
            else
            {
              Log.Error("AddFillInOutMovement. Error saving voucher. Session id: " + _cashier_session_info.CashierSessionId.ToString() + "Operation Type: " + MovementType.ToString());
            }
            #endregion Try
          }
          catch (Exception ex)
          {
            Log.Exception(ex);
            return;
          }
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        return;
      }
      finally
      {
        form_yes_no.Hide();
        CalculateCashierStats();
      }

    } // AddFillInOutMovement

    //------------------------------------------------------------------------------
    // PURPOSE : Check if has enough cage stock.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean: true = Has enough stock.
    //
    //   NOTES :
    //  
    private bool CheckCageStock(CageDenominationsItems.CageDenominationsDictionary CageAmount, CASHIER_MOVEMENT MovementType, SqlTransaction Trx)
    {
      StringBuilder _sb_cage_stock;
      String _currencies_used;
      DataTable _dt_cage_stock;
      Boolean _has_enough_stock;
      Boolean _matches_found;
      String _cur_amount_iso_code;
      Decimal _cur_amount_denomination;
      Int64 _cur_amount_chip_id;
      CageCurrencyType _cage_currency_type;
      Decimal _cur_amount_quantity;
      Decimal _cur_quantity_by_denom;
      Decimal _quantity_to_update;
      Decimal _denomination_to_update;
      CageCurrencyType _cage_currency_type_to_update;
      String _cur_stock_iso_code;
      Decimal _cur_stock_denomination;
      Int64 _cur_stock_chip_id;
      CageCurrencyType _cage_currency_type_stock;
      Decimal _cur_stock_quantity;
      StringBuilder _sb;
      DataTable _dt_currencies;
      DataTable _dt_chips;
      DataRow _dr;
      StringBuilder _sb_update;
      StringBuilder _sb_insert;

      _currencies_used = "";
      _cur_amount_denomination = 0;
      _cur_amount_iso_code = String.Empty;
      _denomination_to_update = 0;
      _cage_currency_type_to_update = CageCurrencyType.Bill;
      _matches_found = false;
      _sb = new StringBuilder();

      // Tables for batch update
      _dt_currencies = new DataTable();
      _dt_chips = new DataTable();

      _dt_currencies.Columns.Add("CGS_ISO_CODE", Type.GetType("System.String"));
      _dt_currencies.Columns.Add("CGS_DENOMINATION", Type.GetType("System.Decimal"));
      _dt_currencies.Columns.Add("CGS_CURRENCY_TYPE", Type.GetType("System.Decimal"));
      _dt_currencies.Columns.Add("CGS_QUANTITY", Type.GetType("System.Decimal"));
      _dt_currencies.Columns.Add("ROW_ACTION", Type.GetType("System.Int16"));

      _dt_chips.Columns.Add("CHSK_CHIP_ID", Type.GetType("System.Int64"));
      _dt_chips.Columns.Add("CHSK_QUANTITY", Type.GetType("System.Int32"));
      _dt_chips.Columns.Add("ROW_ACTION", Type.GetType("System.Int16"));

      foreach (KeyValuePair<CageDenominationsItems.CageDenominationKey, CageDenominationsItems.CageDenominationsItem> _cur_amount in CageAmount)
      {
        if (_cur_amount.Value.TotalAmount > 0)
        {
          if (!String.IsNullOrEmpty(_currencies_used))
          {
            _currencies_used += ", ";
          }
          _currencies_used += "'" + ((_cur_amount.Key.ISOCode.Length > Cage.MAX_ISO_CODE_DIGITS) ? _cur_amount.Key.ISOCode.Substring(0, Cage.MAX_ISO_CODE_DIGITS) : _cur_amount.Key.ISOCode) + "'";
        }
      }

      if (String.IsNullOrEmpty(_currencies_used))
      {
        _currencies_used = "''";
      }

      _sb_cage_stock = new StringBuilder();
      _sb_cage_stock.AppendLine(" SELECT   CGS_ISO_CODE  AS ISO_CODE  ");
      _sb_cage_stock.AppendLine("        , CGS_DENOMINATION  AS DENOMINATION  ");
      _sb_cage_stock.AppendLine("        , CGS_CAGE_CURRENCY_TYPE  AS CURRENCY_TYPE  ");
      _sb_cage_stock.AppendLine("        , CGS_QUANTITY  AS QUANTITY  ");
      _sb_cage_stock.AppendLine("        , 0 AS CHIP_ID  ");
      _sb_cage_stock.AppendLine("  FROM   CAGE_STOCK ");
      _sb_cage_stock.AppendLine("  WHERE  CGS_ISO_CODE IN (" + _currencies_used + ") ");
      _sb_cage_stock.AppendLine("  UNION  ");
      _sb_cage_stock.AppendLine(" SELECT   CH_ISO_CODE AS ISO_CODE ");
      _sb_cage_stock.AppendLine("        , CH_DENOMINATION  AS DENOMINATION   ");
      _sb_cage_stock.AppendLine("        , CH_CHIP_TYPE AS CURRENCY_TYPE  ");// is the same value CageCurrencyType and ChipsType (Only for chips)
      _sb_cage_stock.AppendLine("        , CHSK_QUANTITY  AS QUANTITY   ");
      _sb_cage_stock.AppendLine("        , CH_CHIP_ID AS CHIP_ID  ");
      _sb_cage_stock.AppendLine("   FROM   CHIPS_STOCK ");
      _sb_cage_stock.AppendLine("  INNER   JOIN CHIPS  ON CHSK_CHIP_ID = CH_CHIP_ID");
      _sb_cage_stock.AppendLine("  WHERE   CH_ISO_CODE IN (" + _currencies_used + ") ");

      _dt_cage_stock = new DataTable();

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb_cage_stock.ToString(), Trx.Connection, Trx))
        {
          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
          {
            _sql_da.Fill(_dt_cage_stock);
          }
        }
      }
      catch (SqlException _sql_ex)
      {
        // Not connection timeout
        if (_sql_ex.Number != -2)
        {
          Log.Warning(" *** Sql Exception: Number: " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message.ToString() + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");
        }

        return false;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return false;
      }

      if (MovementType == CASHIER_MOVEMENT.FILLER_IN && _dt_cage_stock.Rows.Count == 0)
      {
        return false;
      }

      foreach (KeyValuePair<CageDenominationsItems.CageDenominationKey, CageDenominationsItems.CageDenominationsItem> _cur_amount in CageAmount)
      {
        _cur_amount_quantity = 0;
        _cage_currency_type_stock = CageCurrencyType.Bill;

        if (_cur_amount.Value.TotalAmount > 0)
        {
          DataRow[] _rows_cage_stock = _dt_cage_stock.Select(String.Format("ISO_CODE = {0}", "'" + ((_cur_amount.Key.ISOCode.Length > Cage.MAX_ISO_CODE_DIGITS) ? _cur_amount.Key.ISOCode.Substring(0, Cage.MAX_ISO_CODE_DIGITS) : _cur_amount.Key.ISOCode) + "'"));
          if (MovementType == CASHIER_MOVEMENT.FILLER_IN && _rows_cage_stock.Length == 0)
          {
            return false;
          }

          _quantity_to_update = 0;
          _cur_quantity_by_denom = 0;
          foreach (DataRow _row_cur_amount in _cur_amount.Value.ItemAmounts.Rows)
          {
            _has_enough_stock = false;
            _cur_amount_chip_id = 0;

            // IsoCode      (CAA_ISO_CODE)
            _cur_amount_iso_code = _row_cur_amount["ISO_CODE"].ToString();
            _cur_amount_iso_code = (_cur_amount_iso_code.Length > Cage.MAX_ISO_CODE_DIGITS) ? _cur_amount_iso_code.Substring(0, Cage.MAX_ISO_CODE_DIGITS) : _cur_amount_iso_code;
            // Denomination (CAA_DENOMINATION)
            _cur_amount_denomination = Format.ParseCurrency(_row_cur_amount["DENOMINATION"].ToString());

            if (_row_cur_amount["CHIP_ID"] != DBNull.Value)
            {
              _cur_amount_chip_id = (Int64)_row_cur_amount["CHIP_ID"];
            }

            // Denomination (CAA_CURRENCY_tYPE)
            _cage_currency_type = CageCurrencyType.Bill; //default

            if (_row_cur_amount["CURRENCY_TYPE"] != DBNull.Value
             && (String)_row_cur_amount["CURRENCY_TYPE"] == Resource.String("STR_VOUCHER_CARD_HANDPAY_SESSION_CURRENCY_TYPE_COIN"))
            {
              _cage_currency_type = CageCurrencyType.Coin;
            }

            if (_cur_amount_denomination != Cage.CHECK_CODE && _cur_amount_denomination != Cage.BANK_CARD_CODE)
            {
              if (FeatureChips.IsChipsType((CageCurrencyType)_row_cur_amount["CAGE_CURRENCY_TYPE_ENUM"]))
              {
                _cage_currency_type = (CageCurrencyType)_row_cur_amount["CAGE_CURRENCY_TYPE_ENUM"];
              }
            }

            // Quantity     (CAA_UN / CAA_TOTAL)
            if (_cur_amount_denomination >= 0 || FeatureChips.IsChipsType(_cage_currency_type))
            {
              _cur_amount_quantity = Format.ParseCurrency(_row_cur_amount["UN"].ToString());
            }
            else
            {
              _cur_amount_quantity = Format.ParseCurrency(_row_cur_amount["TOTAL"].ToString());
            }

            if (_cur_amount_quantity == 0)
            {
              continue;
            }
            //ETP & AMF: Only if it is a valid register initialize value.
            _matches_found = false;

            //ETP & AMF Bank Card and check has tu accumulate quantity because can come some registers.
            if ((CageCurrencyType)_cur_amount_denomination == CageCurrencyType.BankCard
            || (CageCurrencyType)_cur_amount_denomination == CageCurrencyType.Check)
            {
              _cur_quantity_by_denom += _cur_amount_quantity;
            }
            else
            {
              _cur_quantity_by_denom = _cur_amount_quantity;
            }

            _denomination_to_update = _cur_amount_denomination;
            _cage_currency_type_to_update = _cage_currency_type;
            _quantity_to_update = _cur_quantity_by_denom;         //EOR 11-APR-2017

            foreach (DataRow _row_cage_stock in _rows_cage_stock)
            {
              // IsoCode      (CGS_ISO_CODE)
              _cur_stock_iso_code = _row_cage_stock["ISO_CODE"].ToString();

              // Denomination (CGS_DENOMINATION)
              _cur_stock_denomination = Format.ParseCurrency(_row_cage_stock["DENOMINATION"].ToString());
              _cur_stock_chip_id = (Int64)_row_cage_stock["CHIP_ID"];

              // Denomination (CGS_CURRENCY_TYPE)

              _cage_currency_type_stock = (CageCurrencyType)_row_cage_stock["CURRENCY_TYPE"];

              // Quantity     (CGS_QUANTITY)
              _cur_stock_quantity = Format.ParseCurrency(_row_cage_stock["QUANTITY"].ToString());

              if ((_cur_amount_iso_code == _cur_stock_iso_code)
                    && (_cage_currency_type == _cage_currency_type_stock))
              {
                if (_cur_amount_denomination == _cur_stock_denomination && !FeatureChips.IsChipsType(_cage_currency_type_stock)
                   || _cur_amount_chip_id == _cur_stock_chip_id && FeatureChips.IsChipsType(_cage_currency_type_stock))
                {
                  _matches_found = true;

                  if (MovementType == CASHIER_MOVEMENT.FILLER_IN)
                  {
                    if (_cur_quantity_by_denom <= _cur_stock_quantity)
                    {
                      _quantity_to_update = _cur_stock_quantity - _cur_quantity_by_denom;
                      _has_enough_stock = true;
                    }
                  }
                  else
                  {
                    _quantity_to_update = _cur_quantity_by_denom + _cur_stock_quantity;
                  }

                  //ETP & AMF Bank Card and check has tu accumulate quantity because can come some registers.
                  //Only insert 1 register to update / insert
                  if (!((CageCurrencyType)_cur_amount_denomination == CageCurrencyType.BankCard)
                   && !((CageCurrencyType)_cur_amount_denomination == CageCurrencyType.Check))
                  {
                    // Update data
                    if (!FeatureChips.IsChipsType(_cage_currency_type_to_update))
                    {
                      _dr = _dt_currencies.NewRow();
                      _dr["CGS_ISO_CODE"] = _cur_amount_iso_code;
                      _dr["CGS_DENOMINATION"] = _denomination_to_update;
                      _dr["CGS_CURRENCY_TYPE"] = _cage_currency_type_to_update;
                      _dr["CGS_QUANTITY"] = _quantity_to_update;
                      _dr["ROW_ACTION"] = 1;

                      _dt_currencies.Rows.Add(_dr);
                    }
                    else
                    {
                      _dr = _dt_chips.NewRow();
                      _dr["CHSK_CHIP_ID"] = _cur_amount_chip_id;
                      _dr["CHSK_QUANTITY"] = _quantity_to_update;
                      _dr["ROW_ACTION"] = 1;

                      _dt_chips.Rows.Add(_dr);
                    }
                  }

                  break;
                }
              }
            } //foreach 

            if (MovementType == CASHIER_MOVEMENT.FILLER_IN)
            {
              if (!_matches_found || !_has_enough_stock)
              {
                return false;
              }
            }
            else
            {
              //ETP & AMF Bank Card and check has tu accumulate quantity because can come some registers.
              //Only insert 1 register to update / insert
              if (!((CageCurrencyType)_cur_amount_denomination == CageCurrencyType.BankCard)
               && !((CageCurrencyType)_cur_amount_denomination == CageCurrencyType.Check))
              {
                if (!_matches_found)
                {
                  // Insert data
                  if (!FeatureChips.IsChipsType(_cage_currency_type))
                  {
                    _dr = _dt_currencies.NewRow();
                    _dr["CGS_ISO_CODE"] = _cur_amount_iso_code;
                    _dr["CGS_DENOMINATION"] = _denomination_to_update;
                    _dr["CGS_CURRENCY_TYPE"] = _cage_currency_type_to_update;
                    _dr["CGS_QUANTITY"] = _quantity_to_update;
                    _dr["ROW_ACTION"] = 2;

                    _dt_currencies.Rows.Add(_dr);
                  }
                  else
                  {
                    _dr = _dt_chips.NewRow();
                    _dr["CHSK_CHIP_ID"] = _denomination_to_update;
                    _dr["CHSK_QUANTITY"] = _quantity_to_update;
                    _dr["ROW_ACTION"] = 2;

                    _dt_chips.Rows.Add(_dr);
                  }
                }

              }
            } //if (MovementType == CASHIER_MOVEMENT.FILLER_IN)
          } //foreach --> _cur_amount.Value.ItemAmounts.Rows

          //ETP & AMF Bank Card and check has tu accumulate quantity because can come some registers.
          //Only insert 1 register to update / insert
          if ((CageCurrencyType)_cur_amount_denomination == CageCurrencyType.BankCard
           || (CageCurrencyType)_cur_amount_denomination == CageCurrencyType.Check)
          {
            _dr = _dt_currencies.NewRow();
            _dr["CGS_ISO_CODE"] = _cur_amount_iso_code;
            _dr["CGS_DENOMINATION"] = _denomination_to_update;
            _dr["CGS_CURRENCY_TYPE"] = _cage_currency_type_to_update;
            _dr["CGS_QUANTITY"] = _quantity_to_update;
            _dr["ROW_ACTION"] = _matches_found ? 1 : 2;

            _dt_currencies.Rows.Add(_dr);
          }

        } //if (_cur_amount.Value.TotalCurrency > 0)
      } //foreach --> AmountInput.m_cage_amounts

      try
      {

        // Update Chips datatable
        if (_dt_chips.Rows.Count > 0)
        {
          _dt_chips.AcceptChanges();

          foreach (DataRow row in _dt_chips.Rows)
          {
            switch ((Int16)row["ROW_ACTION"])
            {
              case 1:
                row.SetModified();
                break;

              case 2:
                row.SetAdded();
                break;
            }
          }

          _sb_update = new StringBuilder();
          _sb_insert = new StringBuilder();
          // JCA & DDM 21-OCT-2014: Error in SQL 2005. Not allowed subquerys in insert
          _sb_insert.AppendLine("  INSERT INTO   CHIPS_STOCK    ");
          _sb_insert.AppendLine("              ( CHSK_CHIP_ID   ");
          _sb_insert.AppendLine("              , CHSK_QUANTITY  ");
          _sb_insert.AppendLine("              )                ");
          _sb_insert.AppendLine("       VALUES                  ");
          _sb_insert.AppendLine("              (  @pChipId       ");
          _sb_insert.AppendLine("              ,  @pQuantity    ");
          _sb_insert.AppendLine("              )                ");
          _sb_update.AppendLine("  UPDATE  CHIPS_STOCK  ");
          _sb_update.AppendLine("     SET  CHSK_QUANTITY  = @pQuantity");
          _sb_update.AppendLine("   WHERE  CHSK_CHIP_ID   = @pChipId ");

          using (SqlCommand _sql_cmd_insert = new SqlCommand(_sb_insert.ToString(), Trx.Connection, Trx))
          {
            _sql_cmd_insert.Parameters.Add("@pQuantity", SqlDbType.Int).SourceColumn = "CHSK_QUANTITY";
            _sql_cmd_insert.Parameters.Add("@pChipId", SqlDbType.Decimal).SourceColumn = "CHSK_CHIP_ID";

            using (SqlCommand _sql_cmd_update = new SqlCommand(_sb_update.ToString(), Trx.Connection, Trx))
            {
              _sql_cmd_update.Parameters.Add("@pQuantity", SqlDbType.Int).SourceColumn = "CHSK_QUANTITY";
              _sql_cmd_update.Parameters.Add("@pChipId", SqlDbType.BigInt).SourceColumn = "CHSK_CHIP_ID";

              // Batch Update
              using (SqlDataAdapter _da = new SqlDataAdapter())
              {
                _da.InsertCommand = _sql_cmd_insert;
                _da.UpdateCommand = _sql_cmd_update;
                _da.UpdateBatchSize = 500;
                _da.InsertCommand.UpdatedRowSource = UpdateRowSource.None;
                _da.UpdateCommand.UpdatedRowSource = UpdateRowSource.None;

                if (_da.Update(_dt_chips) != _dt_chips.Rows.Count)
                {
                  // Not all data has been inserted
                  Log.Message("uc_bank.CheckCageStock Not all data has been updated");

                  return false;
                }

              } //SqlDataAdapter
            }
          }
        }

        // Update Currencies datatable
        if (_dt_currencies.Rows.Count > 0)
        {

          _dt_currencies.AcceptChanges();

          foreach (DataRow row in _dt_currencies.Rows)
          {
            switch ((Int16)row["ROW_ACTION"])
            {
              case 1:
                row.SetModified();
                break;

              case 2:
                row.SetAdded();
                break;
            }
          }

          _sb_update = new StringBuilder();
          _sb_insert = new StringBuilder();

          _sb_insert.AppendLine(" INSERT INTO   CAGE_STOCK        ");
          _sb_insert.AppendLine("             (                   ");
          _sb_insert.AppendLine("               CGS_QUANTITY      ");
          _sb_insert.AppendLine("            ,  CGS_ISO_CODE      ");
          _sb_insert.AppendLine("            ,  CGS_DENOMINATION  ");
          _sb_insert.AppendLine("            ,  CGS_CAGE_CURRENCY_TYPE  ");

          _sb_insert.AppendLine("             )                   ");
          _sb_insert.AppendLine("      VALUES (                   ");
          _sb_insert.AppendLine("               @pQuantity        ");
          _sb_insert.AppendLine("             , @pIsoCode         ");
          _sb_insert.AppendLine("             , @pDenomination    ");
          _sb_insert.AppendLine("             , @pCurrencyType    ");
          _sb_insert.AppendLine("             )                   ");

          _sb_update.AppendLine("  UPDATE  CAGE_STOCK          ");
          _sb_update.AppendLine("     SET  CGS_QUANTITY =   @pQuantity   ");
          _sb_update.AppendLine("   WHERE  CGS_ISO_CODE =   @pIsoCode ");
          _sb_update.AppendLine("     AND  CGS_DENOMINATION =  @pDenomination ");
          _sb_update.AppendLine("     AND  CGS_CAGE_CURRENCY_TYPE =  @pCurrencyType ");

          using (SqlCommand _sql_cmd_insert = new SqlCommand(_sb_insert.ToString(), Trx.Connection, Trx))
          {
            _sql_cmd_insert.Parameters.Add("@pQuantity", SqlDbType.Decimal).SourceColumn = "CGS_QUANTITY";
            _sql_cmd_insert.Parameters.Add("@pDenomination", SqlDbType.Decimal).SourceColumn = "CGS_DENOMINATION";
            _sql_cmd_insert.Parameters.Add("@pCurrencyType", SqlDbType.Int).SourceColumn = "CGS_CURRENCY_TYPE";
            _sql_cmd_insert.Parameters.Add("@pIsoCode", SqlDbType.NVarChar).SourceColumn = "CGS_ISO_CODE";

            using (SqlCommand _sql_cmd_update = new SqlCommand(_sb_update.ToString(), Trx.Connection, Trx))
            {
              _sql_cmd_update.Parameters.Add("@pQuantity", SqlDbType.Decimal).SourceColumn = "CGS_QUANTITY";
              _sql_cmd_update.Parameters.Add("@pDenomination", SqlDbType.Decimal).SourceColumn = "CGS_DENOMINATION";
              _sql_cmd_update.Parameters.Add("@pCurrencyType", SqlDbType.Int).SourceColumn = "CGS_CURRENCY_TYPE";
              _sql_cmd_update.Parameters.Add("@pIsoCode", SqlDbType.NVarChar).SourceColumn = "CGS_ISO_CODE";

              // Batch Update
              using (SqlDataAdapter _da = new SqlDataAdapter())
              {
                _da.InsertCommand = _sql_cmd_insert;
                _da.UpdateCommand = _sql_cmd_update;
                _da.UpdateBatchSize = 500;
                _da.InsertCommand.UpdatedRowSource = UpdateRowSource.None;
                _da.UpdateCommand.UpdatedRowSource = UpdateRowSource.None;

                if (_da.Update(_dt_currencies) != _dt_currencies.Rows.Count)
                {
                  // Not all data has been inserted
                  Log.Message("uc_bank.CheckCageStock Not all data has been updated");

                  return false;
                }
              }
            }
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

      }

      return false;

    } // CheckCageStock

    private Boolean InsertCageMovementToGUI(CageDenominationsItems.CageDenominationsDictionary CageAmounts, Cage.CageOperationType CageOperationType,
                                            Cage.CageStatus CageOperationStatus, out Int64 MovementId, SqlTransaction Trx)
    {
      StringBuilder _sb_movement;
      StringBuilder _sb_movement_detail;
      SqlParameter _param;
      Int64 _movement_id;
      Decimal _quantity;
      Decimal _denomination;
      CageCurrencyType _currency_type;
      Cage.DefaultCageSession _default_cage_session;
      long _cage_session_id;
      Int64? _chip_id;

      _cage_session_id = 0;
      _movement_id = 0;
      MovementId = _movement_id;

      _default_cage_session = (Cage.DefaultCageSession)Cage.CashierDefaultCageSession();

      if (!Cage.GetOpenOrDefaultCageSessionId(CashierSessionInfo.CashierSessionId, out _cage_session_id, Cashier.GamingDay, Trx))
      {
        Boolean _is_request_without_cage = false;

        // Is a 'request to cage' and there is not a cage session opened
        _is_request_without_cage = CageOperationType == Cage.CageOperationType.RequestOperation & _cage_session_id == 0
                                                        & !GeneralParam.GetBoolean("Cage", "AutoMode") & CashierSessionInfo.CashierSessionId > 0;

        if (!_is_request_without_cage)
        {
          return false;
        }
      }

      _sb_movement = new StringBuilder();
      _sb_movement.AppendLine(" INSERT  INTO   CAGE_MOVEMENTS          ");
      _sb_movement.AppendLine("       (                                ");
      _sb_movement.AppendLine("         CGM_TERMINAL_CASHIER_ID        ");
      _sb_movement.AppendLine("        ,CGM_USER_CASHIER_ID            ");
      _sb_movement.AppendLine("        ,CGM_USER_CAGE_ID               ");
      _sb_movement.AppendLine("        ,CGM_TYPE                       ");
      _sb_movement.AppendLine("        ,CGM_MOVEMENT_DATETIME          ");
      _sb_movement.AppendLine("        ,CGM_STATUS                     ");
      _sb_movement.AppendLine("        ,CGM_CASHIER_SESSION_ID         ");
      _sb_movement.AppendLine("        ,CGM_TARGET_TYPE                ");
      _sb_movement.AppendLine("        ,CGM_CAGE_SESSION_ID            ");

      if (CashierSessionInfo.UserType == GU_USER_TYPE.SYS_GAMING_TABLE)
      {
        _sb_movement.AppendLine("       ,CGM_GAMING_TABLE_ID             ");
      }

      _sb_movement.AppendLine("       )                                ");
      _sb_movement.AppendLine("VALUES (                                ");
      _sb_movement.AppendLine("        @pTerminalCashierId             ");
      _sb_movement.AppendLine("       ,@pUserCashierId                 ");
      _sb_movement.AppendLine("       ,@pUserCageId                    ");
      _sb_movement.AppendLine("       ,@pType                          ");
      _sb_movement.AppendLine("       ,@pMovementDatetime              ");
      _sb_movement.AppendLine("       ,@pStatus                        ");
      _sb_movement.AppendLine("       ,@pCashierSessionId              ");
      _sb_movement.AppendLine("       ,@pTargetType                    ");
      _sb_movement.AppendLine("       ,@pSessionId                     ");

      if (CashierSessionInfo.UserType == GU_USER_TYPE.SYS_GAMING_TABLE)
      {
        _sb_movement.AppendLine("       ,@pGamingTableID                  ");
      }

      _sb_movement.AppendLine("       )                                ");
      _sb_movement.AppendLine("  SET  @pMovementId = SCOPE_IDENTITY() ");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb_movement.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pTerminalCashierId", SqlDbType.BigInt).Value = CashierSessionInfo.TerminalId;  // No es necesario si va por solo por usuario.
          _cmd.Parameters.Add("@pUserCashierId", SqlDbType.BigInt).Value = CashierSessionInfo.UserId;
          _cmd.Parameters.Add("@pUserCageId", SqlDbType.BigInt).Value = CashierSessionInfo.UserId;
          _cmd.Parameters.Add("@pType", SqlDbType.Int).Value = CageOperationType;           //Cage.CageOperationType.RequestOperation;
          _cmd.Parameters.Add("@pMovementDatetime", SqlDbType.DateTime).Value = WGDB.Now;
          _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = CageOperationStatus;
          _cmd.Parameters.Add("@pCashierSessionId", SqlDbType.BigInt).Value = CashierSessionInfo.CashierSessionId;
          _cmd.Parameters.Add("@pTargetType", SqlDbType.Int).Value = CashierSessionInfo.UserType == GU_USER_TYPE.SYS_GAMING_TABLE ? Cage.PendingMovementType.ToGamingTable : Cage.PendingMovementType.ToCashierUser;
          _cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = _cage_session_id;

          if (CashierSessionInfo.UserType == GU_USER_TYPE.SYS_GAMING_TABLE)
          {
            Int32 _gaming_table_id = m_gaming_table_id.Value;
            _cmd.Parameters.Add("@pGamingTableID", SqlDbType.Int).Value = _gaming_table_id;
          }

          _param = _cmd.Parameters.Add("@pMovementId", SqlDbType.BigInt);
          _param.Direction = ParameterDirection.Output;

          if (_cmd.ExecuteNonQuery() == 1)
          {
            _movement_id = (Int64)_param.Value;
          }

        }// SqlCommand

        foreach (KeyValuePair<CageDenominationsItems.CageDenominationKey, CageDenominationsItems.CageDenominationsItem> _cur_amount in CageAmounts)
        {
          if (_cur_amount.Value.TotalAmount > 0 || _cur_amount.Value.TotalUnits > 0)
          {
            foreach (DataRow _cur_denomination in _cur_amount.Value.ItemAmounts.Rows)
            {
              _quantity = 0;
              _denomination = 0;
              _chip_id = null;

              // Insert chips detail
              if ((Int32)_cur_denomination[SQL_COLUMN_CAGE_AMOUNTS_CAA_CURRENCY_TYPE] == (Int32)CurrencyExchangeType.CASINOCHIP ||
                  (Int32)_cur_denomination[SQL_COLUMN_CAGE_AMOUNTS_CAA_CURRENCY_TYPE] == (Int32)CurrencyExchangeType.CASINO_CHIP_RE ||
                  (Int32)_cur_denomination[SQL_COLUMN_CAGE_AMOUNTS_CAA_CURRENCY_TYPE] == (Int32)CurrencyExchangeType.CASINO_CHIP_NRE ||
                  (Int32)_cur_denomination[SQL_COLUMN_CAGE_AMOUNTS_CAA_CURRENCY_TYPE] == (Int32)CurrencyExchangeType.CASINO_CHIP_COLOR)
              {
                _chip_id = (Int64)_cur_denomination[SQL_COLUMN_CAGE_AMOUNTS_CAA_CHIP_ID];
              }

              if ((!String.IsNullOrEmpty(_cur_denomination[SQL_COLUMN_CAGE_AMOUNTS_CAA_UN].ToString()) && Format.ParseCurrency(_cur_denomination[SQL_COLUMN_CAGE_AMOUNTS_CAA_UN].ToString()) > 0) || (!String.IsNullOrEmpty(_cur_denomination[SQL_COLUMN_CAGE_AMOUNTS_CAA_UN].ToString()) && Format.ParseCurrency(_cur_denomination[SQL_COLUMN_CAGE_AMOUNTS_CAA_TOTAL].ToString().Trim()) > 0))
              {
                _sb_movement_detail = new StringBuilder();
                _sb_movement_detail.AppendLine("   INSERT INTO   CAGE_MOVEMENT_DETAILS      ");
                _sb_movement_detail.AppendLine("               ( CMD_MOVEMENT_ID            ");
                _sb_movement_detail.AppendLine("               , CMD_QUANTITY               ");
                _sb_movement_detail.AppendLine("               , CMD_DENOMINATION           ");
                _sb_movement_detail.AppendLine("               , CMD_ISO_CODE               ");
                _sb_movement_detail.AppendLine("               , CMD_CHIP_ID                ");
                _sb_movement_detail.AppendLine("               , CMD_CAGE_CURRENCY_TYPE     ");
                _sb_movement_detail.AppendLine("               )                            ");
                _sb_movement_detail.AppendLine("        VALUES                              ");
                _sb_movement_detail.AppendLine("               ( @pMovementId               ");
                _sb_movement_detail.AppendLine("               , @pQuantity                 ");
                _sb_movement_detail.AppendLine("               , @pDenomination             ");
                _sb_movement_detail.AppendLine("               , @pIsoCode                  ");
                _sb_movement_detail.AppendLine("               , @pChipID                   ");
                _sb_movement_detail.AppendLine("               , @CageCurrencyType          ");
                _sb_movement_detail.AppendLine("               )                            ");

                using (SqlCommand _cmd = new SqlCommand(_sb_movement_detail.ToString(), Trx.Connection, Trx))
                {
                  _cmd.Parameters.Clear();

                  Decimal.TryParse(_cur_denomination[SQL_COLUMN_CAGE_AMOUNTS_CAA_DENOMINATION].ToString(), out _denomination);

                  if (_denomination < 0)
                  {
                    _quantity = _denomination;
                    Decimal.TryParse(_cur_denomination[SQL_COLUMN_CAGE_AMOUNTS_CAA_TOTAL].ToString(), out _denomination);
                  }
                  else
                  {
                    Decimal.TryParse(_cur_denomination[SQL_COLUMN_CAGE_AMOUNTS_CAA_UN].ToString(), out _quantity);
                  }

                  _cmd.Parameters.Add("@pMovementId", SqlDbType.BigInt).Value = _movement_id;
                  _cmd.Parameters.Add("@pQuantity", SqlDbType.Int).Value = _quantity;                   // CAA_UN
                  _cmd.Parameters.Add("@pDenomination", SqlDbType.Money).Value = _denomination;         // CAA_DENOMINATION
                  _cmd.Parameters.Add("@pIsoCode", SqlDbType.NVarChar, 3).Value = _cur_denomination["ISO_CODE"];  // CAA_ISO_CODE

                  if (_chip_id == null)
                  {
                    _cmd.Parameters.Add("@pChipID", SqlDbType.BigInt).Value = DBNull.Value;
                  }
                  else
                  {
                    _cmd.Parameters.Add("@pChipID", SqlDbType.BigInt).Value = _chip_id;
                  }

                  _currency_type = CageCurrencyType.Bill;

                  if ((Int32)_cur_denomination[SQL_COLUMN_CAGE_AMOUNTS_CAA_CURRENCY_TYPE] == (Int32)CurrencyExchangeType.CASINOCHIP || (Int32)_cur_denomination[SQL_COLUMN_CAGE_AMOUNTS_CAA_CURRENCY_TYPE] == (Int32)CurrencyExchangeType.CASINO_CHIP_RE)
                  {
                    _currency_type = CageCurrencyType.ChipsRedimible;
                  }
                  else if ((Int32)_cur_denomination[SQL_COLUMN_CAGE_AMOUNTS_CAA_CURRENCY_TYPE] == (Int32)CurrencyExchangeType.CASINO_CHIP_NRE)
                  {
                    _currency_type = CageCurrencyType.ChipsNoRedimible;
                  }
                  else if ((Int32)_cur_denomination[SQL_COLUMN_CAGE_AMOUNTS_CAA_CURRENCY_TYPE] == (Int32)CurrencyExchangeType.CASINO_CHIP_COLOR)
                  {
                    _currency_type = CageCurrencyType.ChipsColor;
                  }
                  else if (_cur_denomination[SQL_COLUMN_CAGE_AMOUNTS_CAA_TYPE].ToString() == Resource.String("STR_VOUCHER_CARD_HANDPAY_SESSION_CURRENCY_TYPE_COIN"))
                  {
                    _currency_type = CageCurrencyType.Coin;
                  }
                  else if (_cur_denomination[SQL_COLUMN_CAGE_AMOUNTS_CAA_TYPE].ToString() == String.Empty)
                  {
                    _currency_type = CageCurrencyType.Others;
                  }

                  _cmd.Parameters.Add("@CageCurrencyType", SqlDbType.BigInt).Value = _currency_type;

                  _cmd.ExecuteNonQuery();
                }
              }
            }
          }
        }

        MovementId = _movement_id;

        return true;
      }
      catch (SqlException _sql_ex)
      {
        if (_sql_ex.Number != -2) // Not connection timeout
        {
          Log.Warning(" *** Sql Exception: Number: " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message.ToString() + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");
        }

        return false;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
    } // InsertPendingRequest

    //------------------------------------------------------------------------------
    // PURPOSE : Return true when user has pending movements.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Status: Cage.CageStatus
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean: true = Has pending movements.
    //
    //   NOTES :
    //    
    private Boolean HasPendingMovements(Cage.CageStatus Status, out Boolean DBError)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        return HasPendingMovements(Status, _db_trx.SqlTransaction, out DBError);
      }
    }

    private Boolean HasPendingMovements(Cage.CageStatus Status, SqlTransaction Trx, out Boolean DBError)
    {
      StringBuilder _sb;
      Object _unique_id;
      Boolean _is_gaming_day_enabled;

      _is_gaming_day_enabled = Misc.IsGamingDayEnabled() && Cashier.GamingDay != DateTime.MinValue;
      DBError = false;

      _sb = new StringBuilder();
      _sb.AppendLine("DECLARE @last_cage_session_id as bigint                                                                      ");
      _sb.AppendLine("SET @last_cage_session_id = (                                                                                ");
      _sb.AppendLine("");
      _sb.AppendLine("    SELECT    TOP 1 CGM_CAGE_SESSION_ID                                                                      ");
      _sb.AppendLine("      FROM    CAGE_MOVEMENTS                                                                                 ");
      _sb.AppendLine("INNER JOIN    CAGE_SESSIONS                                                                                  ");
      _sb.AppendLine("        ON    CAGE_MOVEMENTS.CGM_CAGE_SESSION_ID = CAGE_SESSIONS.CGS_CAGE_SESSION_ID                         ");
      _sb.AppendLine("INNER JOIN    CASHIER_SESSIONS                                                                               ");
      _sb.AppendLine("        ON    CASHIER_SESSIONS.CS_SESSION_ID = CGM_CASHIER_SESSION_ID                                        ");
      _sb.AppendLine("     WHERE    CAGE_SESSIONS.CGS_CLOSE_DATETIME IS NULL                                                       ");
      _sb.AppendLine("       AND    cashier_sessions.cs_status = @pCashierStatus                                                   ");
      _sb.AppendLine("       AND    CGM_USER_CASHIER_ID = @pUserId                                                                 ");

      if (_is_gaming_day_enabled)
      {
        _sb.AppendLine("       AND    CAGE_SESSIONS.CGS_WORKING_DAY = @pGamingDay                                                  ");
      }

      _sb.AppendLine("  ORDER BY    CGM_MOVEMENT_ID DESC)                                                                          ");
      _sb.AppendLine("");

      _sb.AppendLine("    SELECT   TOP 1 CPM_MOVEMENT_ID                                                                           ");
      _sb.AppendLine("      FROM   CAGE_PENDING_MOVEMENTS                                                                          ");
      _sb.AppendLine("INNER JOIN   CAGE_MOVEMENTS ON CGM_MOVEMENT_ID = CPM_MOVEMENT_ID                                             ");

      if (Misc.IsGamingDayEnabled() && Cashier.GamingDay != DateTime.MinValue)
      {
        _sb.AppendLine("INNER JOIN    CAGE_SESSIONS                                                                                  ");
        _sb.AppendLine("        ON    CAGE_MOVEMENTS.CGM_CAGE_SESSION_ID = CAGE_SESSIONS.CGS_CAGE_SESSION_ID                         ");
      }

      _sb.AppendLine("     WHERE   (                                                                                               ");
      _sb.AppendLine("               (CPM_USER_ID = @pUserId    AND  CPM_TYPE = @pTypeUser)                                        ");
      _sb.AppendLine("               OR                                                                                            ");
      _sb.AppendLine("               (                                                                                             ");
      _sb.AppendLine("                       CPM_USER_ID = @pCashierId                                                             ");
      _sb.AppendLine("                  AND  CPM_TYPE = @pTypeCashier                                                              ");
      _sb.AppendLine("                  AND  cgm_cage_session_id = ISNULL(@last_cage_session_id, cgm_cage_session_id)              ");
      _sb.AppendLine("               )                                                                                             ");
      _sb.AppendLine("               OR                                                                                            ");
      _sb.AppendLine("               (CPM_USER_ID = @pCashierId AND  CPM_TYPE = @pTypeGamingTable)                                 ");
      _sb.AppendLine("             )                                                                                               ");

      if (Status != Cage.CageStatus.Any)
      {
        _sb.AppendLine("       AND   CGM_STATUS  = @pStatusInitial                                                                 ");
      }

      if (_is_gaming_day_enabled)
      {
        _sb.AppendLine("       AND    CAGE_SESSIONS.CGS_WORKING_DAY = @pGamingDay                                                  ");
      }

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = CashierSessionInfo.UserId;
          _cmd.Parameters.Add("@pCashierId", SqlDbType.Int).Value = CashierSessionInfo.TerminalId;

          if (Status != Cage.CageStatus.Any)
          {
            _cmd.Parameters.Add("@pStatusInitial", SqlDbType.Int).Value = Status;
          }

          _cmd.Parameters.Add("@pTypeUser", SqlDbType.Int).Value = Cage.PendingMovementType.ToCashierUser;
          _cmd.Parameters.Add("@pTypeCashier", SqlDbType.Int).Value = Cage.PendingMovementType.ToCashierTerminal;
          _cmd.Parameters.Add("@pTypeGamingTable", SqlDbType.Int).Value = Cage.PendingMovementType.ToGamingTable;
          _cmd.Parameters.Add("@pCashierStatus", SqlDbType.Int).Value = CASHIER_STATUS.OPEN;

          if (_is_gaming_day_enabled)
          {
            _cmd.Parameters.Add("@pGamingDay", SqlDbType.DateTime).Value = Cashier.GamingDay;
          }

          _unique_id = _cmd.ExecuteScalar();
        }// SqlCommand

        if (_unique_id == null)
        {
          return false;
        }

        if (_unique_id == DBNull.Value)
        {
          return false;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        DBError = true;
        return false;
      }

      return true;
    } // HasPendingMovements

    //------------------------------------------------------------------------------
    // PURPOSE : Reset cash desk summary labels.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void ResetCashDeskSummary()
    {
      ArrayList _voucher_list;
      VoucherCashDeskOverview.ParametersCashDeskOverview _parameters_overview;
      String _voucher_file_name;
      WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS _close_session_stats;

      // Set amounts and counters to zero (Same order as they appear)
      m_total_amount = 0;

      _close_session_stats = new WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS();
      _close_session_stats.currencies_balance = new Dictionary<CurrencyIsoType, Decimal>();
      _close_session_stats.prize_taxes_1_name = Misc.ReadGeneralParams("Cashier", "Tax.OnPrize.1.Name", sql_trx);
      _close_session_stats.prize_taxes_2_name = Misc.ReadGeneralParams("Cashier", "Tax.OnPrize.2.Name", sql_trx);
      _close_session_stats.prize_taxes_3_name = Misc.ReadGeneralParams("Cashier", "Tax.OnPrize.3.Name", sql_trx);

      _parameters_overview = new VoucherCashDeskOverview.ParametersCashDeskOverview();
      _parameters_overview.CashierSessionId = CashierSessionInfo.CashierSessionId;
      _parameters_overview.FromCashier = true;
      _parameters_overview.WithLiabilities = false;
      _parameters_overview.BackColor = this.BackColor;
      _parameters_overview.UserType = GU_USER_TYPE.USER;

      _voucher_list = new ArrayList();
      _voucher_list.Add(new VoucherCashDeskOverview(_close_session_stats, PrintMode.Preview, _parameters_overview, sql_trx));

      _voucher_file_name = VoucherPrint.GenerateFileForPreview(_voucher_list);

      web_browser.Navigate(_voucher_file_name);
    } // ResetCashDeskSummary

    //------------------------------------------------------------------------------
    // PURPOSE : Restore parent focus.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void RestoreParentFocus()
    {
      if (this.Visible)
      {
        this.ParentForm.Focus();
        this.ParentForm.BringToFront();
      }
    } // RestoreParentFocus

    //------------------------------------------------------------------------------
    // PURPOSE: Check if user has reached the daily maximum cash openings limit.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - TRUE if user has not reached the limit
    //      - FALSE if user has reached the limit
    // 
    private Boolean CheckMaxDailyCashOpenings()
    {
      Int32 _max_openings;
      StringBuilder _sb;
      Object _obj;
      String _message;

      _max_openings = GeneralParam.GetInt32("Cashier", "MaxDailyCashOpenings");

      // No limit Or User type is SYS-GamingTables
      if (_max_openings == 0 || CashierSessionInfo.UserType == GU_USER_TYPE.SYS_GAMING_TABLE)
      {
        return true;
      }

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT   COUNT(CS_SESSION_ID)             ");
        _sb.AppendLine("   FROM   CASHIER_SESSIONS                 ");
        _sb.AppendLine("  WHERE   CS_USER_ID       = @pUserId      ");
        _sb.AppendLine("    AND   CS_OPENING_DATE >= @pOpeningDate ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = CashierSessionInfo.UserId;
            _cmd.Parameters.Add("@pOpeningDate", SqlDbType.DateTime).Value = Misc.TodayOpening();

            _obj = _cmd.ExecuteScalar();

            if (_obj != null && _obj != DBNull.Value)
            {
              if ((Int32)_obj < _max_openings)
              {
                return true;
              }
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }

      _message = Resource.String("STR_UC_BANK_CASH_OPENING_DAILY_LIMIT_WARNING").Replace("\\r\\n", "\r\n");

      if (this.IsGamingTable())
      {
        _message = Resource.String("STR_UC_BANK_CASH_OPENING_DAILY_LIMIT_WARNING_GAMBLING_TABLE").Replace("\\r\\n", "\r\n");
      }

      if (frm_message.Show(_message, Resource.String("STR_APP_GEN_MSG_WARNING"), MessageBoxButtons.OKCancel, MessageBoxIcon.Warning, this.parent_window) == DialogResult.Cancel)
      {
        return false;
      }

      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.CashOpeningMaxAllowedDaily,
                                               ProfilePermissions.TypeOperation.RequestPasswd, this.ParentForm, out _message))
      {
        return false;
      }

      return true;
    } // CheckMaxDailyCashOpenings

    //------------------------------------------------------------------------------
    // PURPOSE: Show balance info.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    private void btn_show_balance_info_Click(object sender, EventArgs e)
    {
      ShowBalanceInfo(true);
    } // btn_show_balance_info_Click

    //------------------------------------------------------------------------------
    // PURPOSE: Show Ship stock.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    private void btn_chips_stock_Click(object sender, EventArgs e)
    {
      CardData _card_data = null;
      frm_chips _frm_chips;
      Boolean _check_redeem;
      FeatureChips.ChipsOperation _dummy_chips_amount;

      if (ShowCashierSessionClosed())
      {
        return;
      }

      _dummy_chips_amount = new FeatureChips.ChipsOperation(CashierSessionInfo.CashierSessionId, CASHIER_MOVEMENT.NOT_ASSIGNED);

      _frm_chips = new frm_chips();

      form_yes_no.Show(this.ParentForm);

      _frm_chips.Show(_card_data, true, null, FormType.Stock, ref _dummy_chips_amount, out _check_redeem);

      form_yes_no.Hide();

      // ICS 14-APR-2014 Free the object
      _frm_chips.Dispose();

    } // btn_chips_stock_Click

    //------------------------------------------------------------------------------
    // PURPOSE: Returns Gaming Day used by the current cashier session.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS: DateTime.MinValue if cage session is closed.
    // 
    private DateTime CheckGamingDay()
    {
      Boolean _close_to_end, _cage_session_open;
      Int32 _gp_min_before_closing;
      DateTime _now, _opening_time, _next_opening_time;
      String _error_str;
      DialogResult _result;

      _gp_min_before_closing = 0;
      _now = WGDB.Now;
      _error_str = string.Empty;
      _result = DialogResult.Cancel;
      _close_to_end = false;
      _cage_session_open = false;

      _opening_time = Misc.TodayOpening();

      try
      {
        _next_opening_time = _opening_time.AddDays(1);
        _gp_min_before_closing = GeneralParam.GetInt32("GamingDay", "CashOpening.Select.MinutesBeforeTodayOpening");

        if (Misc.IsGamingDayEnabled())
        {
          if (_now > _opening_time && _now < _next_opening_time)
          {
            using (DB_TRX _db_trx = new DB_TRX())
            {
              if (_now >= _next_opening_time.AddMinutes(-_gp_min_before_closing))
              {
                _close_to_end = true;

                if (Cage.IsCageSessionOpen(_next_opening_time, _db_trx.SqlTransaction))
                {
                  _cage_session_open = true;
                }

                if (Cage.IsCageSessionOpen(_opening_time, _db_trx.SqlTransaction))
                {
                  if (_cage_session_open)
                  {
                    _result = frm_message.ShowGamingDaySelector(Resource.String("STR_GAMING_DAY_CLOSE_TO_END")
                                      , Resource.String("STR_APP_GEN_MSG_WARNING")
                                      , _opening_time.ToString("dd-MMM").ToUpper()
                                      , _next_opening_time.ToString("dd-MMM").ToUpper()
                                      , this.ParentForm);
                  }
                  else
                  {
                    _result = DialogResult.None;
                  }
                }
                else
                {
                  if (_cage_session_open)
                  {
                    _result = DialogResult.OK;
                  }
                  else
                  {
                    _result = DialogResult.Cancel;
                  }
                }
              }
              else if (Cage.IsCageSessionOpen(_opening_time, _db_trx.SqlTransaction))
              {
                _result = DialogResult.None;
                _cage_session_open = true;
              }
            }

            switch (_result)
            {
              case DialogResult.OK:
                _opening_time = _next_opening_time;

                break;

              case DialogResult.Cancel:
                _opening_time = DateTime.MinValue;

                if (!_cage_session_open)
                {
                  frm_message.Show(Resource.String("STR_GAMING_DAY_NO_CAGE_SESSION"), "", MessageBoxButtons.OK, Images.CashierImage.Error, this.ParentForm);
                }

                break;

              default:
                if (_close_to_end)
                {
                  if (ProfilePermissions.CheckPermissionList(ProfilePermissions.CashierFormFuncionality.GamingDayCloseToEndSelector,
                                           ProfilePermissions.TypeOperation.RequestPasswd,
                                           this.ParentForm,
                                           Cashier.AuthorizedByUserId,
                                           out _error_str))
                  {
                    frm_message.Show(Resource.String("STR_GAMING_DAY_OPEN_CLOSE_TO_END"), "", MessageBoxButtons.OK, Images.CashierImage.Warning, this.ParentForm);
                  }
                  else
                  {
                    _opening_time = DateTime.MinValue;
                  }
                }
                break;
            }

            this.Focus();
            this.BringToFront();
          }
        }

      }
      catch (Exception _ex)
      {
        Log.Message("CheckGamingDay" + _ex.Message);
      }

      return _opening_time;
    } //CheckGamingDay

    #endregion // Private Methods

    #region Private Methods (CageLogic)

    //------------------------------------------------------------------------------
    // PURPOSE : Cage logic when closecashdesk (movements) or fill out.
    //
    //  PARAMS :
    //      - INPUT :
    //          - CageAmounts:      CageAmountsDictionary.
    //          - CashierMovement:  CASHIER_MOVEMENT.
    //          - CageSessionId:    long.
    //          - Trx:              SqlTransaction.
    //
    //      - OUTPUT :
    //          - MovementId:       Int64
    //
    // RETURNS :
    //      - TRUE:   create cage movement is ok.
    //      - FALSE:  create cage movement is not ok.
    //
    //   NOTES :
    //
    private Boolean CageLogicCloseFillOut(CageDenominationsItems.CageDenominationsDictionary CageAmounts
                                        , CASHIER_MOVEMENT CashierMovement
                                        , long CageSessionId
                                        , SqlTransaction Trx
                                        , out Int64 MovementId
                                        , Int64 OperationId)
    {
      //Int64 _movement_id;
      CashierSessionInfo _session;
      Cage.CageOperationType _cage_operation_type;
      Int32 _gaming_table_id;

      _session = CashierSessionInfo;
      _cage_operation_type = Cage.CageOperationType.FromCashier;
      _gaming_table_id = 0;

      // Change Cage operation for gaming table
      if (IsGamingTable() && Cashier.TerminalId != CashierSessionInfo.TerminalId)
      {
        _cage_operation_type = Cage.CageOperationType.FromGamingTable;
        _gaming_table_id = m_gaming_table_id.Value;
      }

      // Change cage operation type when is a closing session
      if (CashierMovement == CASHIER_MOVEMENT.CAGE_CLOSE_SESSION && _cage_operation_type == Cage.CageOperationType.FromCashier)
      {
        _cage_operation_type = Cage.CageOperationType.FromCashierClosing;
      }
      else if (CashierMovement == CASHIER_MOVEMENT.CAGE_CLOSE_SESSION && _cage_operation_type == Cage.CageOperationType.FromGamingTable)
      {
        _cage_operation_type = Cage.CageOperationType.FromGamingTableClosing;
      }

      if (!Cage.IsCageAutoMode())
      {
        if (!Cage.CreateCageMovement(CageSessionId, _session, _cage_operation_type, Cage.CageStatus.Sent, -1, out MovementId, null, CASHIER_MOVEMENT.NOT_ASSIGNED, _gaming_table_id, Trx))
        {
          Log.Error("uc_bank.CageLogicCloseFillOut.CreateCageMovement: Error creating pending cage movement. Cashier session id -> " + _session.CashierSessionId);
          return false;
        }

        if (!Cage.CreateCagePendingMovement(MovementId, Trx))
        {
          Log.Error("uc_bank.CageLogicCloseFillOut.CreateCagePendingMovement: Error creating pending cage movement. Cashier session id -> " + _session.CashierSessionId);
          return false;
        }
      }
      else
      {
        MovementId = -1;

        // DHA 03-MAY-2016: added because on fill in has the validation
        if (Cage.IsCageAutoModeCageStock() && !CheckCageStock(AmountInput.m_cage_amounts, CashierMovement, Trx))
        {
          Log.Error("uc_bank.CageLogicCloseFillOut.CheckCageStock: An error occurred while trying to get the Cage Stock. Cashier session id -> " + _session.CashierSessionId);
          return false;
        }

        if (Cage.CreateCageMovement(CageSessionId, _session, _cage_operation_type, Cage.CageStatus.OK, -1, out MovementId, AmountInput.m_cage_amounts, CashierMovement, _gaming_table_id, Trx))
        {
          //lmrs 24/11/2014 if cashier is in automode set the global concepts values
          if (Cage.IsCageAutoMode())
          {
            if (!CageMeters.UpdateGlobalCageMeters_CashierSessionClose(CashierSessionInfo.CashierSessionId, AmountInput.m_cage_amounts, Trx))
            {
              return false;
            }

            if (!CageMeters.CreateItemsMovementDetails(MovementId, AmountInput.m_cage_amounts, Trx))
            {
              return false;
            }

          }
          else
          {
            Log.Error("uc_bank.CageLogicCloseFillOut.CreateCageMovement: Error creating pending cage movement. Cashier session id -> " + _session.CashierSessionId);
            return false;
          }
        }
      }

      if (!Cage.InsertCageCurrenciesIntoCashierMovements(CageAmounts, CashierMovement, MovementId, CashierSessionInfo, OperationId, Trx))
      {
        Log.Error("uc_bank.CageLogicCloseFillOut.InsertCageAmountsIntoCashierMovements: Error creating pending cage movement. Cashier session id -> " + _session.CashierSessionId);
        return false;
      }

      return true;
    } // CageLogicCloseFillOut

    //------------------------------------------------------------------------------
    // PURPOSE : Cage logic when close gaming table, create dropbox movement
    //
    //  PARAMS :
    //      - INPUT :
    //          - CageSessionId:    long.
    //          - Trx:              SqlTransaction.
    //          - OperationId:      long.
    //
    //      - OUTPUT :
    //
    //
    // RETURNS :
    //      - TRUE:   create cage movement is ok.
    //      - FALSE:  create cage movement is not ok.
    //
    //   NOTES :
    //
    private Boolean CageLogicCloseGamingTableDropbox(long CageSessionId
                                                    , SqlTransaction Trx
                                                    , Int64 OperationId
                                                    , CageDenominationsItems.CageDenominationsDictionary CollectedDropboxAmount)
    {
      CashierSessionInfo _session;
      Int32 _gaming_table_id;
      Int64 _movement_id;
      CashierMovementsTable _cashier_movements;
      Cage.CageOperationType _cage_operation_type;
      SortedDictionary<CurrencyIsoType, Decimal> _currencies_amount_dic;
      GamingTablesSessions _gt_session;

      _session = CashierSessionInfo;
      _gaming_table_id = 0;
      _cage_operation_type = Cage.CageOperationType.FromGamingTableDropbox;
      _currencies_amount_dic = new SortedDictionary<CurrencyIsoType, Decimal>(new CurrencyIsoType.SortComparer());

      if (!IsGamingTable() && !GamingTablesSessions.GamingTableInfo.HasDropbox)
      {
        return true;
      }

      if (GamingTableBusinessLogic.GamingTableDropBoxCollectionCountMode() == GamingTableDropBoxCollectionCount.Cashier)
      {
        _cage_operation_type = Cage.CageOperationType.FromGamingTableDropboxWithExpected;
      }

      // Change Cage operation for gaming table
      if (IsGamingTable() && Cashier.TerminalId != CashierSessionInfo.TerminalId)
      {
        _gaming_table_id = m_gaming_table_id.Value;
      }

      _cashier_movements = new CashierMovementsTable(CashierSessionInfo);

      if (Cage.IsCageAutoMode() && GamingTableBusinessLogic.GamingTableDropBoxCollectionCountMode() == GamingTableDropBoxCollectionCount.Cashier)
      {
        if (Cage.IsCageAutoModeCageStock() && !CheckCageStock(CollectedDropboxAmount, CASHIER_MOVEMENT.CAGE_FILLER_OUT, Trx))
        {
          Log.Error("uc_bank.CageLogicCloseFillOut.CheckCageStock: An error occurred while trying to get the Cage Stock. Cashier session id -> " + _session.CashierSessionId);
          return false;
        }

        if (Cage.CreateCageMovement(CageSessionId, _session, _cage_operation_type, Cage.CageStatus.OK, -1, out _movement_id, CollectedDropboxAmount, CASHIER_MOVEMENT.CAGE_CASHIER_COLLECT_DROPBOX_GAMINGTABLE, _gaming_table_id, Trx))
        {
          if (Cage.IsCageAutoMode())
          {
            if (!CageMeters.UpdateGlobalCageMeters_CashierSessionClose(CashierSessionInfo.CashierSessionId, CollectedDropboxAmount, Trx))
            {
              return false;
            }
          }
          else
          {
            Log.Error("uc_bank.UpdateGlobalCageMeters_CashierSessionClose: Error creating pending cage movement. Cashier session id -> " + _session.CashierSessionId);
            return false;
          }
        }
        else
        {
          Log.Error("uc_bank.CageLogicCloseFillOut.CreateCageMovement: Error creating pending cage movement. Cashier session id -> " + _session.CashierSessionId);
          return false;
        }

        // Update collected amounts to dropbox
        if (CollectedDropboxAmount.Count > 0)
        {
          GamingTablesSessions.GetOpenedSession(_gaming_table_id, out _gt_session, Trx);

          _gt_session.UpdateGamblingTableSessionsDropbox(CollectedDropboxAmount, Trx);
        }
      }
      else
      {
        if (!Cage.CreateCageMovement(CageSessionId, _session, _cage_operation_type, Cage.CageStatus.Sent, -1, out _movement_id, null, CASHIER_MOVEMENT.CAGE_CASHIER_COLLECT_DROPBOX_GAMINGTABLE, _gaming_table_id, Trx))
        {
          Log.Error("uc_bank.CageLogicCloseGamingTableDropbox.CreateCageMovement: Error creating pending cage movement. Cashier session id -> " + _session.CashierSessionId);
          return false;
        }

        if (!Cage.CreateCagePendingMovement(_movement_id, Trx))
        {
          Log.Error("uc_bank.CageLogicCloseGamingTableDropbox.CreateCagePendingMovement: Error creating pending cage movement. Cashier session id -> " + _session.CashierSessionId);
          return false;
        }
      }

      _cashier_movements.Add(OperationId, CASHIER_MOVEMENT.CAGE_CASHIER_COLLECT_DROPBOX_GAMINGTABLE, 0, 0, "", "", "", 0, _movement_id, _session.CashierSessionId);

      if (!_cashier_movements.Save(Trx))
      {
        return false;
      }

      if (CollectedDropboxAmount != null)
      {
        if (!Cage.InsertCageCurrenciesIntoCashierMovements(CollectedDropboxAmount, CASHIER_MOVEMENT.CAGE_CASHIER_COLLECT_DROPBOX_GAMINGTABLE_DETAIL, _movement_id, CashierSessionInfo, OperationId, Trx))
        {
          Log.Error("uc_bank.CageLogicCloseGamingTableDropbox.InsertCageAmountsIntoCashierMovements: Error creating pending cage movement. Cashier session id -> " + _session.CashierSessionId);
          return false;
        }
      }

      return true;
    } // CageLogicCloseGamingTableDropbox

    //------------------------------------------------------------------------------
    // PURPOSE : Cage logic when opencashdesk (movements) not OK
    //
    //  PARAMS :
    //      - INPUT :
    //          - PendingMovement:      Int64
    //          - CageMovementStatus:   Cage.CageStatus
    //          - Trx:                  SqlTransaction
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private Boolean CageLogicOpenCashNoOk(Int64 PendingMovement
                                        , Cage.CageStatus CageMovementStatus
                                        , SqlTransaction Trx)
    {
      return UpdateCageMovementStatus(PendingMovement, CageMovementStatus, Trx, false);
    } // CageLogicOpenCashNoOk

    //------------------------------------------------------------------------------
    // PURPOSE : Cage logic when opencashdesk (movements)
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private Boolean CageLogicOpenCashOk(Int64 PendingMovement
                                      , CageDenominationsItems.CageDenominationsDictionary CageAmounts
                                      , Cage.CageStatus CageMovementStatus
                                      , CASHIER_MOVEMENT CashierMovement
                                      , SqlTransaction SqlTrx)
    {
      Boolean _continue;
      String _source_name;
      String _description;
      _continue = false;

      if (UpdateCageMovementStatus(PendingMovement, CageMovementStatus, SqlTrx, true))
      {
        if (DeletePendingMovement(PendingMovement, SqlTrx))
        {
          if (Cage.InsertCageCurrenciesIntoCashierMovements(CageAmounts, CashierMovement, PendingMovement, CashierSessionInfo, 0, SqlTrx))
          {
            if (CageMovementStatus == Cage.CageStatus.OkDenominationImbalance)
            {
              _source_name = CashierSessionInfo.UserName + "@" + CashierSessionInfo.TerminalName;
              _description = Resource.String("STR_EA_CASH_DESK_DENOMINATION_DIFF",
                                               Resource.String("STR_VOUCHER_CASH_DESK_FILL_IN_TITLE"));

              Alarm.Register(AlarmSourceCode.User, CashierSessionInfo.UserId, _source_name,
                               (UInt32)AlarmCode.User_DenominationDepositUnbalanced, _description, AlarmSeverity.Warning, DateTime.MinValue, SqlTrx);
            }

            _continue = true;
          }
        }
      }

      return _continue;
    } // CageLogicOpenCashOk

    //------------------------------------------------------------------------------
    // PURPOSE : Cage logic - Updates Movement
    //
    //  PARAMS :
    //      - INPUT :
    //          - PendingMovement:  Int64
    //          - Status:           Cage.CageStatus
    //          - Trx:              SqlTransaction
    //          - CashierSession:   Boolean
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - TRUE:   The pending movement has been updated.
    //      - FALSE:  The pending movement hasn't been updated.
    //
    //   NOTES :
    //
    private Boolean UpdateCageMovementStatus(Int64 PendingMovement
                                           , Cage.CageStatus Status
                                           , SqlTransaction Trx
                                           , Boolean CashierSession)
    {
      StringBuilder _sb;
      CashierSessionInfo _session;

      try
      {
        _session = CashierSessionInfo;

        _sb = new StringBuilder();
        _sb.AppendLine("UPDATE   CAGE_MOVEMENTS                         ");
        _sb.AppendLine("   SET   CGM_STATUS               = @p1         ");

        if (CashierSession)
        {
          _sb.AppendLine("     , CGM_CASHIER_SESSION_ID   = @p3         ");
          _sb.AppendLine("     , CGM_TERMINAL_CASHIER_ID  = @p4         ");
          _sb.AppendLine("     , CGM_USER_CASHIER_ID  = @p5             ");
        }

        _sb.AppendLine(" WHERE   CGM_MOVEMENT_ID  = @p2                 ");

        if (!CashierSession)
        {
          _sb.AppendLine("   AND   CGM_STATUS       = @pPreviousStatus    ");
        }

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@p1", SqlDbType.Int, sizeof(int)).Value = (Int16)Status;
          _cmd.Parameters.Add("@p2", SqlDbType.Int, sizeof(int)).Value = PendingMovement;

          if (CashierSession)
          {
            _cmd.Parameters.Add("@p3", SqlDbType.Int, sizeof(int)).Value = _session.CashierSessionId;
            _cmd.Parameters.Add("@p4", SqlDbType.Int, sizeof(int)).Value = _session.TerminalId;
            _cmd.Parameters.Add("@p5", SqlDbType.Int, sizeof(int)).Value = _session.UserId;
          }
          else
          {
            _cmd.Parameters.Add("@pPreviousStatus", SqlDbType.Int, sizeof(int)).Value = Cage.CageStatus.Sent;
          }

          if (_cmd.ExecuteNonQuery() == 1)
          {
            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // UpdateCageMovementStatus

    //------------------------------------------------------------------------------
    // PURPOSE : Cage logic - deletes related pending movement 
    //
    //  PARAMS :
    //      - INPUT :
    //          - PendingMovement:  Int64
    //          - Trx:              SqlTransaction
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - TRUE:   Delete pending movement is OK.
    //      - FALSE:  Delete pending movement is KO.
    //
    //   NOTES :
    //
    private Boolean DeletePendingMovement(Int64 PendingMovement, SqlTransaction Trx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("DELETE FROM CAGE_PENDING_MOVEMENTS  ");
        _sb.AppendLine(" WHERE CPM_MOVEMENT_ID = @p1 ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@p1", SqlDbType.Int, sizeof(int), "CPM_MOVEMENT_ID").Value = PendingMovement;

          if (_cmd.ExecuteNonQuery() == 1)
          {
            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // DeletePendingMovement

    #endregion // Private Methods (CageLogic)

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE: Refresh cash desk operation status.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    public void RefreshStatus()
    {
      string _out_msg;

      // Is local cashier session
      if (Cashier.TerminalId == CashierSessionInfo.TerminalId)
      {
        Cage.ShowDenominationsMode _show_denominations_mode;
        _show_denominations_mode = (Cage.ShowDenominationsMode)GeneralParam.GetInt32("Cage", "ShowDenominations", 1);

        // 03-FEB-2014  DLL: change visibility condition
        btn_change_chips.Visible = (GamingTableBusinessLogic.IsGamingTablesEnabled() && !GamingTablesSessions.GamingTableInfo.IsGamingTable) &&
                                    _show_denominations_mode != Cage.ShowDenominationsMode.HideAllDenominations;
        btn_chips_stock.Visible = (GamingTableBusinessLogic.IsGamingTablesEnabled() && !GamingTablesSessions.GamingTableInfo.IsGamingTable) &&
                                    GeneralParam.GetBoolean("GamingTables", "ChipsStockControl", false) &&
                                    _show_denominations_mode != Cage.ShowDenominationsMode.HideAllDenominations;
        //ESE 28-JUL-2016
        _out_msg = string.Empty;
        //Check if the user has permissions to the button
        btn_totalizing_strip.Visible = GeneralParam.GetBoolean("Cashier", "Cage.ShowButtonTotalizingStrip");

        if (!btn_change_chips.Visible)
        {
          btn_undo_withdrawal.Location = btn_change_chips.Location;
          panel7.Visible = false;
          panel9.Visible = false;
        }

        // Read Cashier Status (Open/Close) //EOR 17-JUL-2017
        if (CashierBusinessLogic.ReadCashierStatus(CashierSessionInfo.CashierSessionId) == CASHIER_STATUS.OPEN)
        {
          DisableButton(CASHIER_STATUS.OPEN);
          lbl_cash_desk_status.Text = Resource.String("STR_CASH_DESK_STATUS_OPENED");
          lbl_cash_desk_status.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_GREEN;
          CalculateCashierStats();
          lbl_cash_desk_name.Text = GetCurrentCashDeskName();

          if (Misc.IsGamingDayEnabled())
          {
            lbl_cash_desk_name.Text += "  (" + Resource.String("STR_GAMING_DAY") + " " + Cashier.GamingDay.ToString("dd/MMM").ToUpper() + ")";
          }

          // FAV 30-JUN-2015 Hides always "Undo last withdrawal" button to a cashier with integrated table
          if (IsGamingTable())
          {
            btn_undo_withdrawal.Visible = false;
          }
        }
        else
        {
          DisableButton(CASHIER_STATUS.CLOSE);
          lbl_cash_desk_status.Text = Resource.String("STR_CASH_DESK_STATUS_CLOSED");
          lbl_cash_desk_status.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_RED;
          lbl_cash_desk_name.Text = "";
        }

        this.parent_window.ChangeLabelStatus();
      }
    } // RefreshStatus

    //------------------------------------------------------------------------------
    // PURPOSE: Get cash desk name.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    private string GetCurrentCashDeskName()
    {
      string bank_name;

      try
      {
        sql_conn = WSI.Common.WGDB.Connection();
        sql_trx = sql_conn.BeginTransaction();

        sql_str = "   SELECT    cs_name " +
                  "     FROM    cashier_sessions   " +
                  "    WHERE    cs_session_id = @p1 ";

        sql_command = new SqlCommand(sql_str);
        sql_command.Connection = sql_trx.Connection;
        sql_command.Transaction = sql_trx;

        sql_command.Parameters.Add("@p1", SqlDbType.BigInt, 8, "cs_session_id").Value = CashierSessionInfo.CashierSessionId;

        bank_name = (String)sql_command.ExecuteScalar();

        if (bank_name == null)
        {
          return "";
        }

        sql_conn.Dispose();

        return bank_name;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return "";
      }
    } // GetCurrentCashDeskName

    //------------------------------------------------------------------------------
    // PURPOSE: Calculate cashier stats.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    public void CalculateCashierStats()
    {
      WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS _close_session_stats;
      String _voucher_file_name;
      ArrayList _voucher_list;
      VoucherCashDeskOverview.ParametersCashDeskOverview _parameters_overview;

      _voucher_list = new ArrayList();
      _voucher_file_name = "";

      sql_conn = WSI.Common.WGDB.Connection();
      sql_trx = sql_conn.BeginTransaction();

      m_total_amount = 0;
      _close_session_stats = new WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS();

      WSI.Common.Cashier.ReadCashierSessionData(sql_trx, CashierSessionInfo.CashierSessionId, ref _close_session_stats);

      m_total_amount = _close_session_stats.final_balance;

      _parameters_overview = new VoucherCashDeskOverview.ParametersCashDeskOverview();
      _parameters_overview.CashierSessionId = CashierSessionInfo.CashierSessionId;
      _parameters_overview.FromCashier = true;
      _parameters_overview.WithLiabilities = false;
      _parameters_overview.BackColor = this.BackColor;
      _parameters_overview.UserType = GU_USER_TYPE.USER;
      _voucher_list.Add(new VoucherCashDeskOverview(_close_session_stats, PrintMode.Preview, _parameters_overview, sql_trx));

      _voucher_file_name = VoucherPrint.GenerateFileForPreview(_voucher_list);

      web_browser.Navigate(_voucher_file_name);
    } // CalculateCashierStats

    /// <summary>
    /// Get button close current type
    /// </summary>
    /// <returns></returns>
    private BUTTON_CLOSE_TYPE GetButtonCloseType()
    {
      if (btn_close.Tag == null)
      {
        return BUTTON_CLOSE_TYPE.BUTTON_SESSION_CLOSE;
      }

      return ((BUTTON_CLOSE_TYPE)btn_close.Tag);
    }

    /// <summary>
    /// Sets button close properties (Close or Reopen)
    /// </summary>
    /// <param name="ButtonCloseType"></param>
    private void SetCloseButtonProperties(BUTTON_CLOSE_TYPE ButtonCloseType)
    {
      switch (ButtonCloseType)
      {
        case BUTTON_CLOSE_TYPE.BUTTON_SESSION_CLOSE:
          btn_close.Text = Resource.String("STR_UC_BANK_BTN_CASH_DESK_CLOSE");  //Close session
          btn_close.Enabled = true;

          break;
        case BUTTON_CLOSE_TYPE.BUTTON_SESSION_REOPEN:
          btn_close.Text = Resource.String("STR_UC_BANK_BTN_CASH_DESK_REOPEN"); //Reopen session
          btn_close.Enabled = true;

          break;

        case BUTTON_CLOSE_TYPE.BUTTON_SESSION_DISABLE:                          //Disabled
          btn_close.Enabled = false;

          break;

        default:

          throw new ArgumentException("Invalid button close type: '" + ButtonCloseType + "'.");
      }

      btn_close.Tag = ButtonCloseType;
    }

    /// <summary>
    /// Is session expired
    /// </summary>
    /// <returns></returns>
    public Boolean IsSessionExpired()
    {
      return (this.parent_window.SystemInfo.WorkShiftExpired == SystemInfo.StatusInfo.EXPIRED ||
               this.parent_window.SystemInfo.GamingDayExpired == SystemInfo.StatusInfo.EXPIRED);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Update cash desk status on top bar.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - CashierStatus:  CASHIER_STATUS
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    public void DisableButton(CASHIER_STATUS CashierStatus)
    {
      Boolean _sesion_expired;

      switch (CashierStatus)
      {
        case CASHIER_STATUS.OPEN:
          {
            _sesion_expired = IsSessionExpired();

            if (_sesion_expired)
            {
              if (Cage.IsCageEnabled() && !Cage.IsCageAutoMode())
              {
                if ((BUTTON_OPEN_TYPE)btn_open.Tag == BUTTON_OPEN_TYPE.BUTTON_DESK_OPEN)
                {
                  btn_open.Text = Resource.String("STR_UC_BANK_BTN_AMOUNT_REQUEST");
                  btn_open.Tag = BUTTON_OPEN_TYPE.BUTTON_AMOUNT_REQUEST;
                }
              }

              Int64 _request_movement_id;
              _request_movement_id = 0;

              // Bug 24105:Cashier: Keep "Cage Request" button Enabled while a Request is Outstanding
              if (AmountInput.HasPendingRequests(Cage.CageOperationType.RequestOperation, out _request_movement_id))
              {
                btn_open.Enabled = true;
              }
              else
              {
                btn_open.Enabled = false;
              }

            }
            else
            {
              //btn_open.Enabled = false;
              // If all is ok and Cage is enabled then open session button is renamed to amount request
              if (Cage.IsCageEnabled() && !Cage.IsCageAutoMode())
              {
                btn_open.Text = Resource.String("STR_UC_BANK_BTN_AMOUNT_REQUEST");
                btn_open.Tag = BUTTON_OPEN_TYPE.BUTTON_AMOUNT_REQUEST;
                btn_open.Enabled = true;
              }
              else
              {
                btn_open.Text = Resource.String("STR_UC_BANK_BTN_CASH_DESK_OPEN");
                btn_open.Tag = BUTTON_OPEN_TYPE.BUTTON_DESK_OPEN;
                btn_open.Enabled = false;
              }
            }

            SetCloseButtonProperties(BUTTON_CLOSE_TYPE.BUTTON_SESSION_CLOSE);

            btn_status.Enabled = !_sesion_expired;
            btn_deposit.Enabled = !_sesion_expired;
            btn_withdraw.Enabled = !_sesion_expired;
            btn_history.Enabled = !_sesion_expired;
            btn_change_chips.Enabled = !_sesion_expired;
            btn_chips_stock.Enabled = !_sesion_expired;
            btn_undo_withdrawal.Enabled = !_sesion_expired;
            btn_totalizing_strip.Enabled = !_sesion_expired;

            pb_chashier_status.Image = WSI.Cashier.Images.Get(Images.CashierImage.CashierOpen);
          }

          break;

        case CASHIER_STATUS.CLOSE:
          {
            ResetCashDeskSummary();

            // If all is ok and Cage is enabled then open session button is renamed to amount request
            if (Cage.IsCageEnabled())
            {
              if ((BUTTON_OPEN_TYPE)btn_open.Tag != BUTTON_OPEN_TYPE.BUTTON_DESK_OPEN)
              {
                btn_open.Text = Resource.String("STR_UC_BANK_BTN_CASH_DESK_OPEN");
                btn_open.Tag = BUTTON_OPEN_TYPE.BUTTON_DESK_OPEN;
              }
            }

            btn_open.Enabled = true;

            SetCloseButtonProperties(BUTTON_CLOSE_TYPE.BUTTON_SESSION_REOPEN);

            btn_status.Enabled = false;
            btn_deposit.Enabled = false;
            btn_withdraw.Enabled = false;
            btn_history.Enabled = false;
            btn_change_chips.Enabled = false;
            pb_chashier_status.Image = WSI.Cashier.Images.Get(Images.CashierImage.CashierClose);
            btn_chips_stock.Enabled = false;
            btn_undo_withdrawal.Enabled = false;
            btn_totalizing_strip.Enabled = false;
          }

          break;

        case CASHIER_STATUS.DISABLE:
          {
            btn_open.Enabled = false;
            SetCloseButtonProperties(BUTTON_CLOSE_TYPE.BUTTON_SESSION_DISABLE);

            btn_status.Enabled = false;
            btn_deposit.Enabled = false;
            btn_withdraw.Enabled = false;
            btn_history.Enabled = false;
            btn_change_chips.Enabled = false;
            btn_chips_stock.Enabled = false;
            btn_undo_withdrawal.Enabled = false;
            btn_totalizing_strip.Enabled = false;
          }

          break;

        default:
          {
            // should never get here
          }

          break;
      }

      ShowBalanceInfo(false);
    } // DisableButton


    // PURPOSE : Check and show message if cashier session is closed
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean: true = Has to show message of cashier session closed..
    //
    //   NOTES :
    //  Refresh status and show message if cashier session is closed.
    public Boolean ShowCashierSessionClosed()
    {
      if (CashierBusinessLogic.ReadCashierStatus(CashierSessionInfo.CashierSessionId) != CASHIER_STATUS.OPEN)
      {
        frm_message.Show(Resource.String("STR_UC_BANK_CASH_CLOSED").Replace("\\r\\n", "\r\n"),
                         Resource.String("STR_UC_BANK_CASH_CLOSED_TITLE"),
                         MessageBoxButtons.OK,
                         MessageBoxIcon.Warning,
                         this.ParentForm);

        RefreshStatus();

        return true;
      }

      return false;
    }


    #endregion

    #region Button Handling

    //------------------------------------------------------------------------------
    // PURPOSE: Cash desk open operations.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - sender:     object
    //          - EventArgs:  e
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    private void btn_open_Click(object sender, EventArgs e)
    {
      // DHA 03-MAR-2015: When cashier session is diferent than 0, make a Cage Request
      if (Cage.IsCageEnabled() && CashierSessionInfo.CashierSessionId > 0)
      {
        CreateAmountRequest();
      }
      else
      {
        try
        {
          ManagementOpenCashierSession(sender, e, false);
        }
        catch { ;}
      }

    } // btn_open_Click

    public Boolean OpenGamingTable(SortedDictionary<CurrencyIsoType, Decimal> OpeningAmount, out GamingTablesSessions GTSession, out VoucherCashDeskOpen Voucher, SqlTransaction Trx, out long CassierSessionId)
    {
      List<CurrencyExchangeType> _types = new List<CurrencyExchangeType>();
      Boolean _is_new_session;
      VoucherCashDeskOpen _voucher;
      long _cashier_session_id;

      GTSession = null;
      _is_new_session = false;
      _voucher = null;
      _cashier_session_id = 0;
      CassierSessionId = _cashier_session_id;

      Voucher = null;

      if (OpeningAmount == null)
      {
        OpeningAmount = GetOpeningAmount();
      }

      // Only when is a virtual gaming table session
      if (Cage.IsCageEnabled())
      {
        // Only when is a virtual gaming table session
        if (IsGamingTable() && Cashier.TerminalId != CashierSessionInfo.TerminalId)
        {
          CashierSessionInfo = WSI.Common.Cashier.GetSystemCashierSessionInfo(GU_USER_TYPE.SYS_GAMING_TABLE, Trx, CashierSessionInfo.TerminalName, out _is_new_session, out _voucher);
          _cashier_session_id = CashierSessionInfo.CashierSessionId;

          if (_is_new_session && _voucher != null)
          {
            Voucher = _voucher;
          }



          if (!GamingTablesSessions.GetOrOpenSession(out GTSession, m_gaming_table_id.Value, CashierSessionInfo.CashierSessionId, Trx))
          {
            return false;
          }

          foreach (KeyValuePair<CurrencyIsoType, Decimal> _currency in OpeningAmount)
          {
            if (_currency.Key.IsoCode == CurrencyExchange.GetNationalCurrency() && _currency.Key.Type == CurrencyExchangeType.CURRENCY)
            {
              continue;
            }

            //initilize cashier_session_by_currency
            WSI.Common.Cashier.SetCashierSessionsByCurrency(CashierSessionInfo.CashierSessionId, _currency.Key.IsoCode, _currency.Key.Type, 0, 0, Trx);
          }
        }
        else
        {
          CashierSessionOpen(OpeningAmount, CheckGamingDay(), Trx, out _cashier_session_id);
          if (_cashier_session_id == 0)
          {
            return false;
          }

          if (IsGamingTable())
          {
            if (!GamingTablesSessions.GetOrOpenSession(out GTSession, m_gaming_table_id.Value, _cashier_session_id, Trx))
            {
              return false;
            }
          }
        }

        m_cashier_session_info.AuthorizedByUserName = Cashier.AuthorizedByUserName;
        m_cashier_session_info.AuthorizedByUserId = Cashier.AuthorizedByUserId;
      }
      CassierSessionId = _cashier_session_id;
      return true;
    }

    public void btn_open_Click()
    {
      btn_open_Click(null, null);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Create amount request.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    private void CreateAmountRequest()
    {
      DisableButton(CASHIER_STATUS.DISABLE);

      try
      {
        // Check if current user is an authorized user
        String error_str;

        // Only GamingTable
        if (IsGamingTable() && Cashier.TerminalId != CashierSessionInfo.TerminalId)
        {
          if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.GamingTableFillerIn, ProfilePermissions.TypeOperation.RequestPasswd, this.ParentForm, out error_str))
          {
            return;
          }
        }
        else
        {
          // CashDesk and Cashdesk integrated with gamingtable
          if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.DepositCash, ProfilePermissions.TypeOperation.RequestPasswd, this.ParentForm, out error_str))
          {
            return;
          }
        }
        AddFillInOutMovement(CASHIER_MOVEMENT.NOT_ASSIGNED, 0);
      }
      finally
      {
        RefreshStatus();
      }
    } // CreateAmountRequest

    //------------------------------------------------------------------------------
    // PURPOSE: View Cashier Session Status.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - sender:     object
    //          - EventArgs:  e
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    private void btn_status_Click(object sender, EventArgs e)
    {
      SqlConnection _sql_conn;
      SqlTransaction _sql_trx;
      Currency _closing_amount;
      Boolean _voucher_saved;

      DisableButton(CASHIER_STATUS.DISABLE);

      try
      {
        // Initializations
        _voucher_saved = false;

        form_yes_no.Show(this.ParentForm);

        if (!AmountInput.Show(Resource.String("STR_VOUCHER_CASH_DESK_STATUS_TITLE"), out _closing_amount, VoucherTypes.StatusCash, null, 0, CASH_MODE.TOTAL_REDEEM, form_yes_no))
        {
          form_yes_no.Hide();
          this.RestoreParentFocus();

          return;
        }

        _sql_conn = null;
        _sql_trx = null;

        try
        {
          WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS _session_stats;

          if (Misc.IsGamingHallMode())
          {
            VoucherGamingHallCashDeskStatus _voucher;

            // Get Sql Connection 
            _sql_conn = WSI.Common.WGDB.Connection();
            _sql_trx = _sql_conn.BeginTransaction();

            // Obtain session stats
            _session_stats = new WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS();
            WSI.Common.Cashier.ReadCashierSessionData(_sql_trx, CashierSessionInfo.CashierSessionId, ref _session_stats);

            // - Create voucher
            _voucher = new VoucherGamingHallCashDeskStatus(PrintMode.Print, _sql_trx, _session_stats);

            // - Save voucher
            _voucher_saved = _voucher.Save(_sql_trx);

            _sql_trx.Commit();

            if (_voucher_saved)
            {
              VoucherPrint.Print(_voucher);
            }
            else
            {
              Log.Error("CashierSessionStatus. Error saving voucher. Session id: " + CashierSessionInfo.CashierSessionId.ToString());
            }
          }
          else
          {
            VoucherCashDeskStatus _voucher;

            // Get Sql Connection 
            _sql_conn = WSI.Common.WGDB.Connection();
            _sql_trx = _sql_conn.BeginTransaction();

            // - Create voucher
            _voucher = new VoucherCashDeskStatus(PrintMode.Print, _sql_trx);
            _session_stats = new WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS();

            WSI.Common.Cashier.ReadCashierSessionData(_sql_trx, CashierSessionInfo.CashierSessionId, ref _session_stats);

            // - Build voucher specific data (amounts)
            _voucher.SetupCashStatusVoucher(_session_stats);

            // - Save voucher
            _voucher_saved = _voucher.Save(_sql_trx);

            _sql_trx.Commit();

            if (_voucher_saved)
            {
              VoucherPrint.Print(_voucher);
            }
            else
            {
              Log.Error("CashierSessionStatus. Error saving voucher. Session id: " + CashierSessionInfo.CashierSessionId.ToString());
            }
          }

        }

        catch (Exception ex)
        {
          _sql_trx.Rollback();
          Log.Exception(ex);
        }

        finally
        {
          if (_sql_trx != null)
          {
            if (_sql_trx.Connection != null)
            {
              _sql_trx.Rollback();
            }
            _sql_trx.Dispose();
            _sql_trx = null;
          }

          // Close connection
          if (_sql_conn != null)
          {
            _sql_conn.Close();
            _sql_conn = null;
          }
        }

        form_yes_no.Hide();
        this.RestoreParentFocus();
      }
      finally
      {
        // With this the we avoid the card info at the main cashier screen
        Misc.PrintBankCardSection_StatusCash = false;
        RefreshStatus();
      }
    } // btn_status_Click

    private void btn_close_Click(object sender, EventArgs e)
    {
      if (GetButtonCloseType() == BUTTON_CLOSE_TYPE.BUTTON_SESSION_CLOSE)
      {
        CloseSession();
      }
      else
      {
        ReOpenCashier();
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Close Cashier Session:
    //          1. Update Db session status.
    //          2. Disable close session button.
    //          3. Update top bar cash desk status.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    public void CloseSession()
    {
      SortedDictionary<CurrencyIsoType, Decimal> _closing_amount;
      WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS _session_stats;
      Boolean _check_redeem;
      Int64 _cage_session_id;
      Boolean _continue;
      ClosingStocks _closing_stocks;
      GamingTablesSessions _gt_session;
      String _close_title;

      DisableButton(CASHIER_STATUS.DISABLE);

      _continue = false;
      _cage_session_id = 0;
      _closing_stocks = null;
      _gt_session = null;

      try
      {

        if (ShowCashierSessionClosed())
        {
          return;
        }

        // If cage is closed then cancel operation.
        //if (Cage.IsCageEnabled() && !Cage.IsCageSessionOpen(out _cage_session_id, Cashier.UserId))
        if (Cage.IsCageEnabled())
        {
          using (DB_TRX _db_trx = new DB_TRX())
          {
            _continue = Cage.GetOpenOrDefaultCageSessionId(CashierSessionInfo.CashierSessionId, out _cage_session_id, Cashier.GamingDay, _db_trx.SqlTransaction);

            // DHA 09-JUN-2016: load closing cash data
            _closing_stocks = new ClosingStocks(m_cashier_session_info.TerminalId, _db_trx.SqlTransaction);
          }

          if (!_continue)
          {
            form_yes_no.Show(this.ParentForm);
            frm_message.Show(Resource.String("STR_CAGE_NOT_SESSION_OPEN").Replace("\\r\\n", "\r\n"),
                             Resource.String("STR_VOUCHER_CASH_DESK_FILL_OUT_TITLE_CAGE"),
                             MessageBoxButtons.OK,
                             MessageBoxIcon.Warning,
                             this.ParentForm);
            form_yes_no.Hide();

            return;
          }
        }

        Currency pending_amount;

        // Check that no amounts are pending from mobile banks
        pending_amount = CashierBusinessLogic.CalculatePendingAmount(CashierSessionInfo.CashierSessionId);
        if (pending_amount > 0)
        {
          frm_message.Show(Resource.String("STR_FRM_AMOUNT_INPUT_MSG_MBCARD_PENDING_AMOUNT", pending_amount),
                           Resource.String("STR_APP_GEN_MSG_ERROR"),
                           MessageBoxButtons.OK,
                           MessageBoxIcon.Error,
                           this.ParentForm);

          form_yes_no.Hide();

          return;
        }

        Boolean _has_pending_movements;
        Boolean _db_error;

        _has_pending_movements = HasPendingMovements(Cage.CageStatus.Any, out _db_error);

        if (!_has_pending_movements && _db_error)
        {
          frm_message.Show(Resource.String("STR_FRM_DATABASE_CFG_ERROR"),
                           Resource.String("STR_APP_GEN_MSG_ERROR"),
                           MessageBoxButtons.OK,
                           MessageBoxIcon.Warning,
                           this.ParentForm);
          form_yes_no.Hide();
          return;
        }

        // If has any pending movement.
        if (_has_pending_movements)
        {
          String _message_title;
          String _message;

          form_yes_no.Show(this.ParentForm);

          _message_title = Resource.String("STR_CAGE_CLOSE_SESSION");
          _message = Resource.String("STR_CAGE_NOT_PENDING_MOVEMENT_DETAIL", Resource.String("STR_CAGE_NOT_PENDING_MOVEMENT_DETAIL_FILL_IN")).Replace("\\r\\n", "\r\n");

          if (this.IsGamingTable())
          {
            _message_title = Resource.String("STR_CAGE_CLOSE_SESSION_GAMBLING_TABLE");
            _message = Resource.String("STR_CAGE_NOT_PENDING_MOVEMENT_DETAIL_GAMBLING_TABLE", Resource.String("STR_CAGE_NOT_PENDING_MOVEMENT_DETAIL_FILL_IN")).Replace("\\r\\n", "\r\n");
          }

          frm_message.Show(_message,
                           _message_title,
                           MessageBoxButtons.OK,
                           MessageBoxIcon.Warning,
                           this.ParentForm);
          form_yes_no.Hide();

          return;
        }

        Int64 _request_movement_id;
        _request_movement_id = 0;

        if (AmountInput.HasPendingRequests(Cage.CageOperationType.RequestOperation, out _request_movement_id))
        {
          String _message_title;
          String _message;

          form_yes_no.Show(this.ParentForm);

          _message_title = Resource.String("STR_CAGE_CLOSE_SESSION");
          _message = Resource.String("STR_CAGE_NOT_PENDING_MOVEMENT_DETAIL", Resource.String("STR_CAGE_NOT_PENDING_MOVEMENT_DETAIL_REQUEST")).Replace("\\r\\n", "\r\n");

          if (this.IsGamingTable())
          {
            _message_title = Resource.String("STR_CAGE_CLOSE_SESSION_GAMBLING_TABLE");
            _message = Resource.String("STR_CAGE_NOT_PENDING_MOVEMENT_DETAIL_GAMBLING_TABLE", Resource.String("STR_CAGE_NOT_PENDING_MOVEMENT_DETAIL_REQUEST")).Replace("\\r\\n", "\r\n");
          }

          frm_message.Show(_message,
                           _message_title,
                           MessageBoxButtons.OK,
                           MessageBoxIcon.Warning,
                           this.ParentForm);
          form_yes_no.Hide();

          return;
        }

        // Check if current user is an authorized user
        String error_str;

        // Only GamingTable
        if (IsGamingTable() && Cashier.TerminalId != CashierSessionInfo.TerminalId)
        {
          if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.GamingTableClose, ProfilePermissions.TypeOperation.RequestPasswd, this.ParentForm, out error_str))
          {
            return;
          }

          // This case only applies to gaming tables
          m_cashier_session_info.AuthorizedByUserId = Cashier.AuthorizedByUserId;
          m_cashier_session_info.AuthorizedByUserName = Cashier.AuthorizedByUserName;
        }
        else
        {
          // CashDesk and Cashdesk integrated with gamingtable
          if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.OpenCloseCash, ProfilePermissions.TypeOperation.RequestPasswd, this.ParentForm, out error_str))
          {
            return;
          }
        }

        form_yes_no.Show(this.ParentForm);

        // RAB 05-MAY-2016: PBI 12736: Review 23
        String _gaming_table_name;
        _gaming_table_name = "";

        if (IsGamingTable())
        {
          using (DB_TRX _db_trx = new DB_TRX())
          {
            GamingTablesSessions.GetGamingTableName(m_gaming_table_id.Value, out _gaming_table_name, _db_trx.SqlTransaction);

            if (!GamingTablesSessions.GetGamingTableInfo(CashierSessionInfo.TerminalId, _db_trx.SqlTransaction))
            {
              return;
            }

            if (!GamingTablesSessions.GetOpenedSession(GamingTablesSessions.GamingTableInfo.GamingTableId, out _gt_session, _db_trx.SqlTransaction))
            {
              return;
            }

            if (!GamingTablesSessions.SetGamingTableSessionPreClosing(_gt_session.GamingTableSessionId, _db_trx.SqlTransaction))
            {
              return;
            }

            _db_trx.Commit();
          }
        }

        // LTC 28-MAR-2016
        _close_title = (this.GamingTableId > 0 && this.ParentForm == null) ?
                          Resource.String("STR_CAGE_GAMING_TABLE_CLOSE_TITLE", _gaming_table_name) :
                          Resource.String("STR_UC_BANK_BTN_CASH_DESK_CLOSE");
        if (!AmountInput.Show(_close_title,
                              out _closing_amount, out _check_redeem, VoucherTypes.CloseCash, null,
                              0, CASH_MODE.TOTAL_REDEEM, form_yes_no,
                              out _session_stats, false, 0, false, CashierSessionInfo, _closing_stocks))
        {
          form_yes_no.Hide();
          this.RestoreParentFocus();

          return;
        }

        form_yes_no.Hide();
        this.RestoreParentFocus();

        //GamingTables Mode
        if (IsGamingTable())
        {
          CashierSessionClose(_session_stats, _cage_session_id, _closing_amount, _closing_stocks);
        }
        else
        {
          CashierSessionClose(_session_stats, _cage_session_id, null, _closing_stocks);
        }
      }
      finally
      {
        Misc.PrintBankCardSection = false; //LTC 06-SEP-2016
        // ATB 16-NOV-2016
        // Set to false to avoid the Total Vouchers info at main Cashier screen
        Misc.PrintBankCardSection_StatusCash = false;
        RefreshStatus();
      }
    }

    private Boolean ValidateClosingSessionStock(ClosingStocks ClosingStocks, Int64 CageSessionId)
    {
      Boolean _close_session;
      CageDenominationsItems.CageDenominationsDictionary _closing_amount_cashier;

      _close_session = true;
      _closing_amount_cashier = AmountInput.m_cage_amounts;

      if (ClosingStocks == null)
      {
        return true;
      }

      if (!ClosingStocks.SleepsOnTable)
      {
        return true;
      }

      ClosingStocks.ValidateClosingStock(AmountInput.m_cage_amounts, "ISO_CODE", "DENOMINATION", "CAGE_CURRENCY_TYPE_ENUM", "CHIP_ID", "UN", "TOTAL");

      if (ClosingStocks.SleepsOnTable && ClosingStocks.Type == Common.ClosingStocks.ClosingStockType.FIXED)
      {
        if (ClosingStocks.CurrenciesStocksCredits != null && (Int64)ClosingStocks.CurrenciesStocksCredits.Compute("SUM(QUANTITY)", "") > 0)
        {
          Common.Cashier.TYPE_CASHIER_SESSION_STATS _cashier_session_data;
          _cashier_session_data = new Common.Cashier.TYPE_CASHIER_SESSION_STATS();
          _cashier_session_data.currencies_balance = new Dictionary<CurrencyIsoType, Decimal>();
          _cashier_session_data.collected = new SortedDictionary<CurrencyIsoType, Decimal>(new CurrencyIsoType.SortComparer());
          _cashier_session_data.collected_diff = new SortedDictionary<CurrencyIsoType, Decimal>(new CurrencyIsoType.SortComparer());
          CashierMovementsGrouped.FillCashierSessionCurrenciesDataFromSession(m_cashier_session_info.CashierSessionId, ref _cashier_session_data);

          string _filter = string.Empty;
          foreach (KeyValuePair<CurrencyIsoType, Decimal> _amount in _cashier_session_data.currencies_balance)
          {
            switch (_amount.Key.Type)
            {
              case CurrencyExchangeType.CARD:
              case CurrencyExchangeType.CHECK:
                //These denomination types are negative
                if (!string.IsNullOrEmpty(ClosingStocks.CurrenciesStocksCredits.Compute("SUM(TOTAL)", "DENOMINATION=-" + (Int32)_amount.Key.Type).ToString())
                  && (decimal)ClosingStocks.CurrenciesStocksCredits.Compute("SUM(TOTAL)", "DENOMINATION=-" + (Int32)_amount.Key.Type) != _amount.Value)
                {
                  if (!string.IsNullOrEmpty(_filter))
                  {
                    _filter += "|";
                  }
                  _filter += Resource.String("STR_CURRENCY_TYPE_" + (Int32)_amount.Key.Type);
                  _close_session = false;
                }
                break;
            }
          }
          if (!_close_session)
          {
            frm_message.Show(string.Format(Resource.String("STR_CAGE_CLOSE_CASHIER_INTEGRATED").Replace("\\r\\n", "\r\n"), _filter),
            Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_TITLE"),
            MessageBoxButtons.OK,
            MessageBoxIcon.Warning,
            this.ParentForm);
            return false;
          }
          AddFillInOutMovement(CASHIER_MOVEMENT.FILLER_OUT, CageSessionId, ClosingStocks);

          // If credit out movement is canceled, avoid to close session
          if (AmountInput.DialogResult != DialogResult.OK)
          {
            _close_session = false;
          }
        }

        if (_close_session && ClosingStocks.CurrenciesStocksFills != null && (Int64)ClosingStocks.CurrenciesStocksFills.Compute("SUM(QUANTITY)", "") > 0)
        {
          if (!Cage.IsCageAutoMode())
          {
            AddFillInOutMovement(CASHIER_MOVEMENT.NOT_ASSIGNED, CageSessionId, ClosingStocks);
          }
          else
          {
            AddFillInOutMovement(CASHIER_MOVEMENT.FILLER_IN, CageSessionId, ClosingStocks);
          }

          // If credit out movement is canceled, avoid to close session
          if (AmountInput.DialogResult != DialogResult.OK || !Cage.IsCageAutoMode())
          {
            _close_session = false;
          }
        }

        // Recalculate the values for the closing because there is a credit/fill operation
        ClosingStocks.FixedBankRecalculateClosingAmount(_closing_amount_cashier, out AmountInput.m_cage_amounts, "ISO_CODE", "DENOMINATION", "CAGE_CURRENCY_TYPE_ENUM", "CHIP_ID", "UN", "TOTAL");
      }

      return _close_session;
    }

    public void btn_close_Click()
    {
      btn_close_Click(null, null);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Add Filler Cash.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - sender:     object
    //          - EventArgs:  e
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    private void btn_filler_in_Click(object sender, EventArgs e)
    {
      Int64 _cage_session_id;
      Boolean _continue;
      Button _button;

      _cage_session_id = 0;

      DisableButton(CASHIER_STATUS.DISABLE);

      try
      {
        _button = (Button)sender;

        if (_button == btn_deposit)
        {
          if (ShowCashierSessionClosed())
          {
            return;
          }
        }

        if (Cage.IsCageEnabled())
        {
          using (DB_TRX _db_trx = new DB_TRX())
          {
            _continue = Cage.GetOpenOrDefaultCageSessionId(CashierSessionInfo.CashierSessionId, out _cage_session_id, Cashier.GamingDay, _db_trx.SqlTransaction);
          }

          if (!_continue)
          {
            form_yes_no.Show(this.ParentForm);
            frm_message.Show(Resource.String("STR_CAGE_NOT_SESSION_OPEN").Replace("\\r\\n", "\r\n"),
                             Resource.String("STR_VOUCHER_CASH_DESK_FILL_OUT_TITLE_CAGE"),
                             MessageBoxButtons.OK,
                             MessageBoxIcon.Warning,
                             this.ParentForm);
            form_yes_no.Hide();
            return;
          }
        }

        // Check if current user is an authorized user
        String error_str;

        // Only GamingTable
        if (IsGamingTable() && Cashier.TerminalId != CashierSessionInfo.TerminalId)
        {
          if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.GamingTableFillerIn, ProfilePermissions.TypeOperation.RequestPasswd, this.ParentForm, out error_str))
          {
            return;
          }

          // This case only applies to gaming tables
          m_cashier_session_info.AuthorizedByUserId = Cashier.AuthorizedByUserId;
          m_cashier_session_info.AuthorizedByUserName = Cashier.AuthorizedByUserName;
        }
        else
        {
          // CashDesk and Cashdesk integrated with gamingtable
          if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.DepositCash, ProfilePermissions.TypeOperation.RequestPasswd, this.ParentForm, out error_str))
          {
            return;
          }
        }

        AddFillInOutMovement(CASHIER_MOVEMENT.FILLER_IN, _cage_session_id);
      }
      finally
      {
        RefreshStatus();
      }
    } // btn_filler_in_Click

    public void btn_filler_in_Click()
    {
      btn_filler_in_Click(null, null);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Remove Filler Cash.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - sender:     object
    //          - EventArgs:  e
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    private void btn_filler_out_Click(object sender, EventArgs e)
    {
      Int64 _cage_session_id;
      Boolean _continue = false;

      DisableButton(CASHIER_STATUS.DISABLE);

      _cage_session_id = 0;
      try
      {
        if (ShowCashierSessionClosed())
        {
          return;
        }
        // If cage is closed then cancel operation.
        //if (Cage.IsCageEnabled() && !Cage.IsCageSessionOpen(out _cage_session_id, Cashier.UserId))
        if (Cage.IsCageEnabled())
        {
          using (DB_TRX _db_trx = new DB_TRX())
          {
            _continue = Cage.GetOpenOrDefaultCageSessionId(CashierSessionInfo.CashierSessionId, out _cage_session_id, Cashier.GamingDay, _db_trx.SqlTransaction);
          }

          if (!_continue)
          {
            form_yes_no.Show(this.ParentForm);
            frm_message.Show(Resource.String("STR_CAGE_NOT_SESSION_OPEN").Replace("\\r\\n", "\r\n"),
                             Resource.String("STR_VOUCHER_CASH_DESK_FILL_OUT_TITLE_CAGE"),
                             MessageBoxButtons.OK,
                             MessageBoxIcon.Warning,
                             this.ParentForm);
            form_yes_no.Hide();
            return;
          }
        }

        // Check if current user is an authorized user
        String error_str;

        if (IsGamingTable() && Cashier.TerminalId != CashierSessionInfo.TerminalId)
        {
          if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.GamingTableFillerOut, ProfilePermissions.TypeOperation.RequestPasswd, this.ParentForm, out error_str))
          {
            return;
          }

          // This case only applies to gaming tables
          m_cashier_session_info.AuthorizedByUserId = Cashier.AuthorizedByUserId;
          m_cashier_session_info.AuthorizedByUserName = Cashier.AuthorizedByUserName;

        }
        else
        {
          // CashDesk and Cashdesk integrated with gamingtable
          if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.WithdrawnCash, ProfilePermissions.TypeOperation.RequestPasswd, this.ParentForm, out error_str))
          {
            return;
          }
        }

        AddFillInOutMovement(CASHIER_MOVEMENT.FILLER_OUT, _cage_session_id);
      }
      finally
      {
        RefreshStatus();
      }
    } // btn_filler_out_Click

    public void btn_filler_out_Click()
    {
      btn_filler_out_Click(null, null);
    }
    //------------------------------------------------------------------------------
    // PURPOSE: Undo Filler Cash.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - sender:     object
    //          - EventArgs:  e
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    private void btn_undo_filler_out_Click(object sender, EventArgs e)
    {
      OperationUndo.InputUndoOperation _input_undo_operation;
      ArrayList _voucher_list;
      Boolean _trx_ok;
      Boolean _continue_cancel;
      SqlTransaction _sql_trx;
      AccountOperations.Operation _account_operation;
      OperationUndo.UndoError _error;
      long _cage_movement_id;

      if (ShowCashierSessionClosed())
      {
        return;
      }

      _continue_cancel = false;
      _trx_ok = false;


      // Check if current user is an authorized user
      String error_str;
      if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.UndoWithdrawn,
                                               ProfilePermissions.TypeOperation.RequestPasswd,
                                               this.ParentForm,
                                               out error_str))
      {
        return;
      }

      _input_undo_operation = new OperationUndo.InputUndoOperation();
      _input_undo_operation.CardData = new CardData();
      _input_undo_operation.CashierSessionInfo = CommonCashierInformation.CashierSessionInfo();
      _input_undo_operation.CodeOperation = OperationCode.CASH_WITHDRAWAL;
      _input_undo_operation.GenerateVoucherForUndo = true;
      _input_undo_operation.OperationId = 0;
      _input_undo_operation.OperationIdForUndo = 0;

      if (!OperationUndo.IsWithdrawReversible(_input_undo_operation, out _account_operation, out _error))
      {
        form_yes_no.Show(this.ParentForm);
        frm_message.Show(Resource.String("STR_NO_RECORDS_TO_UNDO_WITHDRAWAL"),
                         Resource.String("STR_CANT_UNDO_WITHDRAW"),
                         MessageBoxButtons.OK,
                         MessageBoxIcon.Warning,
                         this.ParentForm);
        form_yes_no.Hide();

        return;
      }

      if (frm_message.Show(Resource.String("STR_UNDO_WITHDRAWAL_WARNING").Replace("\\r\\n", "\r\n"),
                           Resource.String("STR_CANT_UNDO_WITHDRAW"),
                           MessageBoxButtons.OKCancel,
                           MessageBoxIcon.Warning,
                           this.ParentForm,
                           true) == DialogResult.OK)
      {
        _continue_cancel = true;
      }

      if (!_continue_cancel)
      {
        return;
      }

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _sql_trx = _db_trx.SqlTransaction;

          _trx_ok = OperationUndo.UndoOperation(_input_undo_operation, false, out _voucher_list, _sql_trx);

          if (_trx_ok && Cage.IsCageEnabled())
          {
            _cage_movement_id = Cage.GetCageMovementByAccountOperation(_input_undo_operation.OperationIdForUndo, _sql_trx);

            Cage.DeleteFromPendingMovement(_cage_movement_id, _sql_trx);

            _trx_ok = Cage.UpdateCageMovementStatus(_cage_movement_id, Cage.CageStatus.CanceledUncollectedCashierRetirement,
                  _input_undo_operation.CashierSessionInfo.UserId, _input_undo_operation.CashierSessionInfo.CashierSessionId, _sql_trx);

            if (_trx_ok && Cage.IsCageAutoMode() && Cage.IsCageAutoModeCageStock())
            {
              _trx_ok = Cage.UpdateCageStock(_cage_movement_id, _sql_trx);

              if (_trx_ok)
              {
                _trx_ok = CageMeters.UndoCageMeters(_cage_movement_id, false, _sql_trx);
              }
            }
          }

          _trx_ok = Withdrawal.DeleteReconciliationMovements(_input_undo_operation.OperationIdForUndo, _trx_ok, _sql_trx);

          if (_trx_ok)
          {
            _trx_ok = _db_trx.Commit();
            if (_trx_ok && _voucher_list != null)
            {
              VoucherPrint.Print(_voucher_list);
          }
        }
      }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        _trx_ok = false;
      }

      this.RefreshStatus();

      if (!_trx_ok)
      {
        frm_message.Show(Resource.String("STR_CANT_UNDO_WITHDRAW_CAPTION"),
                         Resource.String("STR_CANT_UNDO_WITHDRAW"),
                         MessageBoxButtons.OK,
                         MessageBoxIcon.Warning,
                         this.ParentForm,
                         true);
      }

    } // SetParent

    //------------------------------------------------------------------------------
    // PURPOSE: Change chips button click.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - sender:     object
    //          - EventArgs:  e
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    private void btn_change_chips_Click(object sender, EventArgs e)
    {
      frm_chips _frm_chips;
      FeatureChips.ChipsOperation _chips_operation_in;
      FeatureChips.ChipsOperation _chips_operation_out;
      ArrayList _voucher_list;

      DisableButton(CASHIER_STATUS.DISABLE);
      _voucher_list = new ArrayList();

      _frm_chips = new frm_chips();

      try
      {
        if (ShowCashierSessionClosed())
        {
          return;
        }

        form_yes_no.Show(this.ParentForm);

        _chips_operation_in = new FeatureChips.ChipsOperation(CashierSessionInfo.CashierSessionId, CASHIER_MOVEMENT.CHIPS_CHANGE_IN);
        _chips_operation_out = new FeatureChips.ChipsOperation(CashierSessionInfo.CashierSessionId, CASHIER_MOVEMENT.CHIPS_CHANGE_OUT);

        if (!_frm_chips.Show(ref _chips_operation_in, ref _chips_operation_out))
        {
          return;
        }

        // Get Sql Connection 
        sql_conn = WSI.Common.WGDB.Connection();
        sql_trx = sql_conn.BeginTransaction();

        if (!FeatureChips.ChipsChange(CashierSessionInfo, CashierSessionInfo.CashierSessionId, _chips_operation_in, _chips_operation_out, out _voucher_list, sql_trx))
        {
          sql_trx.Rollback();

          return;
        }

        sql_trx.Commit();

        VoucherPrint.Print(_voucher_list);
      }
      finally
      {
        // ICS 14-APR-2014 Free the object
        _frm_chips.Dispose();

        form_yes_no.Hide();

        this.RestoreParentFocus();
        RefreshStatus();
      }
    } // btn_change_chips_Click

    //------------------------------------------------------------------------------
    // PURPOSE: Show Movement History.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - sender:     object
    //          - EventArgs:  e
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    private void btn_history_Click(object sender, EventArgs e)
    {
      try
      {
        if (ShowCashierSessionClosed())
        {
          return;
        }

        this.Cursor = Cursors.WaitCursor;

        // TODO: RRB retrieva "Alias"

        sql_str = "SELECT TOP 150  CM_TYPE                            " +
                  "              , '  ' AS TYPE_NAME                  " +
                  "              , CM_DATE                            " +
                  "              , CM_USER_NAME                       " +
                  "              , CT_NAME                            " +
                  "              , CM_CASHIER_ID                      " +
                  "              , CM_INITIAL_BALANCE                 " +
                  "              , '' AS CM_SUB_AMOUNT_STR            " +
                  "              , '' AS CM_ADD_AMOUNT_STR            " +
                  "              , CM_FINAL_BALANCE                   " +
                  "              , ISNULL(CM_OPERATION_ID, 0)         " +
                  "              , '' AS COLOR_ROW                    " +
                  "              , CM_DETAILS                         " +
                  "              , CM_ADD_AMOUNT                      " +
                  "              , CM_SUB_AMOUNT                      " +
                  "              , CM_CURRENCY_ISO_CODE               " +
                  "              , CM_UNDO_STATUS                     " +
                  "              , CM_CURRENCY_DENOMINATION           " +
                  "              , CM_AUX_AMOUNT                      " +
          // 28-APR-2016 FGB  Product Backlog Item 9758: Tables (Phase 1): Review table reports -Added field CM_CAGE_CURRENCY_TYPE to the SELECT
                  "              , CM_CAGE_CURRENCY_TYPE              " +
                  "  FROM          CASHIER_MOVEMENTS                  " +
                  "              , CASHIER_TERMINALS                  " +
                  " WHERE          CM_SESSION_ID = " + CashierSessionInfo.CashierSessionId +
                  "  AND           CM_CASHIER_ID = CT_CASHIER_ID      " +
          //  "  AND           CM_TYPE       <> " + (Int32) CASHIER_MOVEMENT.MB_LIMIT_EXTENSION +
          // RMS 08-AUG-2014 : Added Credit and Debit card split2 movements
                  "  AND           CM_TYPE NOT IN (" + (Int32)CASHIER_MOVEMENT.CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT2 +
                                                ", " + (Int32)CASHIER_MOVEMENT.CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT2 +
                                                ", " + (Int32)CASHIER_MOVEMENT.CURRENCY_CARD_EXCHANGE_SPLIT2 +
                                                ", " + (Int32)CASHIER_MOVEMENT.CURRENCY_CARD_EXCHANGE_SPLIT2_UNDO +
                                                ", " + (Int32)CASHIER_MOVEMENT.CURRENCY_EXCHANGE_SPLIT2 +
                                                ", " + (Int32)CASHIER_MOVEMENT.CASINO_CHIPS_EXCHANGE_SPLIT2 +
                                                ", " + (Int32)CASHIER_MOVEMENT.CURRENCY_CHECK_SPLIT2 +
                                                ", " + (Int32)CASHIER_MOVEMENT.CAGE_CASHIER_REFILL_HOPPER_TERMINAL +
                                                ", " + (Int32)CASHIER_MOVEMENT.CAGE_CASHIER_COLLECT_DROPBOX_TERMINAL +
                                                ", " + (Int32)CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CARD_EXCHANGE_SPLIT2 +
                                                ", " + (Int32)CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT2 +
                                                ", " + (Int32)CASHIER_MOVEMENT.COMPANY_B_CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT2 +
                                                ", " + (Int32)CASHIER_MOVEMENT.COMPANY_B_CURRENCY_EXCHANGE_SPLIT2 +
                                                ", " + (Int32)CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CHECK_SPLIT2 + ")" +
                  "  AND          (CM_TYPE <= " + (Int32)CASHIER_MOVEMENT.CASHIER_BY_CONCEPT_IN +
                  "   OR           CM_TYPE  = " + (Int32)CASHIER_MOVEMENT.CASHIER_BY_CONCEPT_OUT + ")" +
                  " ORDER BY CM_MOVEMENT_ID DESC, CM_DATE DESC ";

        PlayHistory.Show(sql_str, frm_last_movements.MovementShow.Cashier, this.lbl_cash_desk_name.Text);
      }
      catch
      {
      }
      finally
      {
        this.Cursor = Cursors.Default;
        RefreshStatus();
      }
    } // btn_history_Click

    //------------------------------------------------------------------------------
    // PURPOSE: Print the totalizing strip
    // 
    //  PARAMS:
    //      - INPUT:
    //          - sender:     object
    //          - EventArgs:  e
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    private void btn_totalizing_strip_Click(object sender, EventArgs e)
    {
      String _out_msg;
      _out_msg = String.Empty;

      if (ShowCashierSessionClosed())
      {
        return;
      }

      if (ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.Totalizing_strip, ProfilePermissions.TypeOperation.RequestPasswd, this.ParentForm, out _out_msg))
      {
        //If has permission show form
        frm_print_totalizing_strip.Show(this.ParentForm);
      }
      else
      {
        this.Focus();
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Show Balance Info.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - FromBtn:     Boolean
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    private void ShowBalanceInfo(Boolean FromBtn)
    {
      String _error_str;
      Boolean _show_balance_info;

      _show_balance_info = false;

      if (FromBtn)
      {
        // Check if we have permission to show balance info
        _show_balance_info = ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.ShowBalanceInfo,
                                                                ProfilePermissions.TypeOperation.RequestPasswd,
                                                                this.ParentForm,
                                                                out _error_str);
      }
      else
      {
        // Check if GP is enabled to show balance info. If not, show button
        _show_balance_info = GeneralParam.GetBoolean("Cashier", "ShowBalanceInfo", true);
      }

      web_browser.Visible = _show_balance_info;
      btn_show_balance_info.Visible = !_show_balance_info;

      // RCI 30-JUL-2015: Fixed Bug WIG-2617: ShowBalanceInfo button has to be always enabled, even the session is expired.
      btn_show_balance_info.Enabled = true;

      if (CashierBusinessLogic.ReadCashierStatus(CashierSessionInfo.CashierSessionId) == CASHIER_STATUS.OPEN)
      {
        btn_status.Enabled = _show_balance_info;
        btn_history.Enabled = _show_balance_info;

        // RRR Fixed Bug WIG-624: Link ShowStock Enabled/Disabled status to Show Balance permissions
        btn_chips_stock.Enabled = _show_balance_info;
      }

      m_show_balance = _show_balance_info;
    } // ShowBalanceInfo

    private SortedDictionary<CurrencyIsoType, Decimal> GetOpeningAmount()
    {
      List<CurrencyExchange> _currencies;
      CurrencyExchange _national_currency;
      List<CurrencyExchangeType> _types = new List<CurrencyExchangeType>();

      SortedDictionary<CurrencyIsoType, Decimal> OpeningAmount;

      _types.Add(CurrencyExchangeType.CURRENCY);

      CurrencyExchange.GetAllowedCurrencies(true, _types, false, out _national_currency, out _currencies);

      OpeningAmount = new SortedDictionary<CurrencyIsoType, Decimal>(new CurrencyIsoType.SortComparer());

      OpeningAmount.Add(new CurrencyIsoType(_national_currency.CurrencyCode, _national_currency.Type), 0);

      return OpeningAmount;
    }

    public void ManagementOpenCashierSession(object sender, EventArgs e, bool ModeReception)
    {
      GamingTablesSessions _gt_session;
      SortedDictionary<CurrencyIsoType, Decimal> _opening_amount;
      Int64 _cage_session_id;
      Boolean _continue;

      WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS _dummy_session_stats;
      Boolean _dummy_check_redeem;
      VoucherCashDeskOpen _voucher_cash_desk_open;

      _voucher_cash_desk_open = null;

      try
      {
        if (GamingTableBusinessLogic.IsEnabledGTPlayerTracking() && m_gaming_table_id != null)
        {
          // We need created a new GamingTableSession Object to check if existing someone in process 
          _gt_session = new GamingTablesSessions(m_gaming_table_id.Value, 0);
          if (_gt_session.IsGamingTableProcessing(m_gaming_table_id.Value))
          {
            return;
          }

          using (DB_TRX _db_trx = new DB_TRX())
          {
            if (!GamingTablesSessions.SetGamingTableProcessing(m_gaming_table_id.Value, true, _db_trx.SqlTransaction))
            {
              return;
            }
            _db_trx.Commit();
          }
        }

        if (!ModeReception)
        {
          DisableButton(CASHIER_STATUS.DISABLE);
        }

        // Check if current user is an authorized user
        String error_str;

        // Only GamingTable
        if (IsGamingTable() && Cashier.TerminalId != CashierSessionInfo.TerminalId)
        {
          if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.GamingTableOpen, ProfilePermissions.TypeOperation.RequestPasswd, this.ParentForm, out error_str))
          {
            return;
          }

          // This case only applies to gaming tables
          m_cashier_session_info.AuthorizedByUserId = Cashier.AuthorizedByUserId;
          m_cashier_session_info.AuthorizedByUserName = Cashier.AuthorizedByUserName;
        }
        else
        {
          // CashDesk and Cashdesk integrated with gamingtable
          if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.OpenCloseCash, ProfilePermissions.TypeOperation.RequestPasswd, this.ParentForm, out error_str))
          {
            return;
          }
        }

        // Check if daily maximum cash openings limit has been reached
        if (!CheckMaxDailyCashOpenings())
        {
          return;
        }

        if (!Cage.IsCageEnabled())
        {
          if (ModeReception)
          {
            _opening_amount = GetOpeningAmount(); //TODO opening_amount set to 0
          }
          else
          {
            form_yes_no.Show(this.ParentForm);
            if (!AmountInput.Show(Resource.String("STR_UC_BANK_BTN_CASH_DESK_OPEN"), out _opening_amount, out _dummy_check_redeem, VoucherTypes.OpenCash, null, 0, CASH_MODE.TOTAL_REDEEM, form_yes_no, out _dummy_session_stats))
            {
              form_yes_no.Hide();
              this.RestoreParentFocus();
              return;
            }

            form_yes_no.Hide();
            this.RestoreParentFocus();
          }

          using (DB_TRX _db_trx = new DB_TRX())
          {
            long _ca_session_Id = 0;
            CashierSessionOpen(_opening_amount, CheckGamingDay(), _db_trx.SqlTransaction, out _ca_session_Id);
            CashierSessionInfo.CashierSessionId = _ca_session_Id;
            if (CashierSessionInfo.CashierSessionId > 0)
            {
              _db_trx.Commit();
            }
          }
        }
        else
        {
          ClosingStocks _closing_stocks;
          Boolean _has_pending_movements;
          Boolean _db_error;

          _closing_stocks = new ClosingStocks(m_cashier_session_info.TerminalId);

          _has_pending_movements = HasPendingMovements(Cage.CageStatus.Sent, out _db_error);

          // Fill in due to chips sleeps on table
          if (_closing_stocks.SleepsOnTable
            && _closing_stocks.CurrenciesStocks != null
            && _closing_stocks.CurrenciesStocks.Rows.Count > 0
            && !ModeReception)
          {
            AddFillInOutMovement(CASHIER_MOVEMENT.FILLER_IN_CLOSING_STOCK, 0, _closing_stocks);
          }
          else if (!_has_pending_movements && _db_error)
          {
            frm_message.Show(Resource.String("STR_FRM_DATABASE_CFG_ERROR"),
                             Resource.String("STR_APP_GEN_MSG_ERROR"),
                             MessageBoxButtons.OK,
                             MessageBoxIcon.Warning,
                             this.ParentForm);
            form_yes_no.Hide();
            return;
          }
          // Fill in when send from b�veda and inital amount
          else if (!(Misc.IsGamingDayEnabled()
                 && Cashier.GamingDay == DateTime.MinValue)
                 && _has_pending_movements
                 && !ModeReception)
          {
            this.btn_filler_in_Click(sender, e);
          }
          // Fill in when cage is auto mode and closing stock type is fixed 
          else if (Cage.IsCageAutoMode()
                && _closing_stocks.Type == ClosingStocks.ClosingStockType.FIXED
                && !_closing_stocks.SleepsOnTable
                && _closing_stocks.CurrenciesStocks != null
                && _closing_stocks.CurrenciesStocks.Rows.Count > 0
                && !ModeReception)
          {
            using (DB_TRX _db_trx = new DB_TRX())
            {
              _continue = Cage.GetOpenOrDefaultCageSessionId(CashierSessionInfo.CashierSessionId, out _cage_session_id, Cashier.GamingDay, _db_trx.SqlTransaction);
            }
            if (_continue && _cage_session_id > 0)
            {
              AddFillInOutMovement(CASHIER_MOVEMENT.FILLER_IN, _cage_session_id, _closing_stocks);
            }
          }
          else
          {
            using (DB_TRX _db_trx = new DB_TRX())
            {
              long _ca_session_id = 0;
              OpenGamingTable(null, out _gt_session, out _voucher_cash_desk_open, _db_trx.SqlTransaction, out _ca_session_id);
              CashierSessionInfo.CashierSessionId = _ca_session_id;
              if (CashierSessionInfo.CashierSessionId > 0)
              {
                _db_trx.Commit();
              }
            }

              if (_voucher_cash_desk_open != null)
              {
                VoucherPrint.Print(_voucher_cash_desk_open);
              }
            }
          }
        }
      catch (Exception _ex)
      {
        Log.Error(String.Format("uc_bank - ManagementOpenCashierSession() {0}", _ex.Message));
      }
      finally
      {
        RefreshStatus();

        if (GamingTableBusinessLogic.IsEnabledGTPlayerTracking() && m_gaming_table_id != null)
        {
          using (DB_TRX _db_trx = new DB_TRX())
          {
            if (GamingTablesSessions.SetGamingTableProcessing(m_gaming_table_id.Value, false, _db_trx.SqlTransaction))
            {
              _db_trx.Commit();
            }

          }
        }
      }


    }
    #endregion

    #region Internals

    internal void SetParent(frm_container ParentWindow)
    {
      parent_window = ParentWindow;
    } // SetParent

    #endregion // Internals

#if DEBUG
    // TEST FOR TRANSFER BETWEEN CASHIER SESSIONS
    // FAV 24-AUG-2015

    #region Cashier sessions transfer

    private void btn_test_transfer_Click(object sender, EventArgs e)
    {
      //panel_test_transfer.Visible = !panel_test_transfer.Visible;
      //panel_test_transfer.Location = new Point(535, 4);

      PinPadResponse _pinpad_response = null;
      PinPadResponse _pinpad_response_out;
      PinPadCard _pinpad_card;
      IPinPad _pinpad;
      PinPadCashierTerminal _pinpad_terminal;

      // TODO: Leer la configuracion desde Base de Datos
      _pinpad_terminal = new PinPadCashierTerminal();
      _pinpad_terminal.Port = PinPadCashierTerminal.PINPAD_PORT.COM9;
      _pinpad_terminal.Type = PinPadType.DUMMY;
      _pinpad_terminal.Enabled = true;

      _pinpad = new PinPadBanorte();
      _pinpad.Initialize();

      PinPadCommon _obj = new PinPadCommon(_pinpad, _pinpad_terminal);

      try
      {
        _obj.InitDevice();
        _obj.StartTransaction();
        if (_obj.Read(out _pinpad_card))
        {
          // TODO: Cambiar por el OperationId
          string _control_number = Misc.GetControlNumberBySequence("test");

          if (_obj.SendTransaction(_pinpad_card, 0.1m, _control_number, out _pinpad_response))
          {
            if (_obj.NotifyResult(_pinpad_response, out _pinpad_response_out))
            {
              frm_message.Show("Pago OK. Codigo de Autorizaci�n: " + _pinpad_response_out.AuthorizationCode,
                "Test PinPad", MessageBoxButtons.OK, MessageBoxIcon.Warning, this.ParentForm);

              return;
            }
          }
        }

        frm_message.Show("Error en la operaci�n." + (_pinpad_response != null ? _pinpad_response.ResponseMessage : ""),
              "Test PinPad", MessageBoxButtons.OK, MessageBoxIcon.Error, this.ParentForm);

        return;

      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        frm_message.Show("Error: " + ex.Message, "Exception", MessageBoxButtons.OK, MessageBoxIcon.Error, this.ParentForm);
      }
      finally
      {
        _obj.EndTransaction();
        _obj.ReleaseDevice();
      }
    }

    private void btn_transfer_send_another_Click(object sender, EventArgs e)
    {
      try
      {
        SortedDictionary<CurrencyIsoType, Decimal> _fill_amount;
        VoucherTransferCashiersSent _voucher_transfer_sent;
        CashierSessionTransferInfo _cashier_session_transfer_info_sent;
        bool _result = false;

        lbl_rpt_test_transfer.Text = string.Empty;

        _fill_amount = GetFillAmount(m_cashier_session_info);
        if (_fill_amount.Count == 0)
        {
          return;
        }

        // Target SessionID with an opened session
        Int64 _session_target_id = int.Parse(txt_target_session_id.Text);

        _result = CashierSessionTransferService.TransferCreditToCashierSession(m_cashier_session_info,
                                                                                _session_target_id,
                                                                                _fill_amount,
                                            out _cashier_session_transfer_info_sent,
                                            out _voucher_transfer_sent);

        if (_result)
        {
          lbl_rpt_test_transfer.Text = string.Format("Transfer sent sussesfully. CashierSessionTransferID: {0}, OperationSourceID: {1}",
                                                      _cashier_session_transfer_info_sent.CashierSessionTransferId,
                                                      _cashier_session_transfer_info_sent.OperationSourceId);

          VoucherPrint.Print(_voucher_transfer_sent);
        }
        else
        {
          throw new Exception("It couldn't made the transfer (send). Check the LOG file to find the error.");
        }
      }
      catch (Exception ex)
      {
        lbl_rpt_test_transfer.Text = "Error: " + ex.Message.ToString();
      }
      finally
      {
        CalculateCashierStats();

        RefreshStatus();
      }
    }

    private void btn_transfer_send_to_this_Click(object sender, EventArgs e)
    {
      try
      {
        SortedDictionary<CurrencyIsoType, Decimal> _fill_amount;
        VoucherTransferCashiersSent _voucher_transfer_sent;
        CashierSessionTransferInfo _cashier_session_transfer_info_sent;
        bool _result = false;

        lbl_rpt_test_transfer.Text = string.Empty;

        // Source SessionID with an opened session
        CashierSessionInfo _session_source = GetOpenedCashierSessionById(int.Parse(txt_source_session_id.Text));

        _fill_amount = GetFillAmount(_session_source);
        if (_fill_amount.Count == 0)
          return;

        _result = CashierSessionTransferService.TransferCreditToCashierSession(_session_source,
                                                                                m_cashier_session_info.CashierSessionId,
                                                                                _fill_amount,
                                            out _cashier_session_transfer_info_sent,
                                            out _voucher_transfer_sent);

        if (_result)
        {
          lbl_rpt_test_transfer.Text = string.Format("Transfer sent sussesfully. CashierSessionTransferID: {0}, OperationSourceID: {1}",
                                                      _cashier_session_transfer_info_sent.CashierSessionTransferId,
                                                      _cashier_session_transfer_info_sent.OperationSourceId);

          VoucherPrint.Print(_voucher_transfer_sent);
        }
        else
        {
          throw new Exception("It couldn't made the transfer (send). Check the LOG file to find the error.");
        }
      }
      catch (Exception ex)
      {
        lbl_rpt_test_transfer.Text = "Error: " + ex.Message.ToString();
      }
      finally
      {
        CalculateCashierStats();

        RefreshStatus();
      }

    }

    private void btn_transfer_receive_Click(object sender, EventArgs e)
    {
      try
      {
        VoucherTransferCashiersReceived _voucher_transfer_received = null;
        CashierSessionTransferInfo _cashier_transfer_info_sent = null;
        CashierSessionTransferInfo _cashier_session_transfer_info_Received;
        bool _result = false;

        lbl_rpt_test_transfer.Text = string.Empty;

        CashierSessionTransferService.GetCashierSessionTransferById(int.Parse(txt_session_transfer_id.Text), out _cashier_transfer_info_sent);

        _result = CashierSessionTransferService.ReceiveCreditFromCashierSession(m_cashier_session_info,
                                                    _cashier_transfer_info_sent,
                                                    out _cashier_session_transfer_info_Received,
                                                    out _voucher_transfer_received);

        if (_result)
        {
          lbl_rpt_test_transfer.Text = string.Format("Transfer received sussesfully. CashierSessionTransferID: {0}, OperationTargetID: {1}",
                                                      _cashier_session_transfer_info_Received.CashierSessionTransferId,
                                                      _cashier_session_transfer_info_Received.OperationTargetId);

          VoucherPrint.Print(_voucher_transfer_received);
        }
        else
        {
          throw new Exception("It couldn't made the transfer (reception). Check the LOG file to find the error.");
        }

      }
      catch (Exception ex)
      {
        lbl_rpt_test_transfer.Text = "Error: " + ex.Message.ToString();
      }
      finally
      {
        RefreshStatus();
      }
    }

    private SortedDictionary<CurrencyIsoType, Decimal> GetFillAmount(CashierSessionInfo CashierSession)
    {
      Boolean _dummy_check_redeem;
      SortedDictionary<CurrencyIsoType, Decimal> _fill_amount;
      WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS _dummy_session_stats;
      Boolean _is_amount_request;
      Int64 _request_movement_id;
      Boolean _has_pending_movements;
      VoucherTypes _voucher_type;

      _is_amount_request = true;
      _request_movement_id = -1;
      _has_pending_movements = false;
      _voucher_type = VoucherTypes.FillOut;

      form_yes_no.Show(this.ParentForm);

      AmountInput.Show("Transferencia a otro cajero", out _fill_amount, out _dummy_check_redeem, _voucher_type, null, m_total_amount, CASH_MODE.TOTAL_REDEEM,
                       form_yes_no, out _dummy_session_stats, _is_amount_request, _request_movement_id, _has_pending_movements, CashierSession, null);

      form_yes_no.Hide();
      this.RestoreParentFocus();

      return _fill_amount;
    }

    private CashierSessionInfo GetOpenedCashierSessionById(Int64 CashierSessionId)
    {
      Int32 _user_id;
      String _user_name;
      Int32 _cashier_id;
      String _cashier_name;
      Currency _final_movement_balance;
      DateTime _dummy_gaming_day;
      CashierSessionInfo CashierSessionInfo = null;

      using (DB_TRX _db_trx = new DB_TRX())
      {
        if (Common.Cashier.DB_ReadOpenedCashierSession(CashierSessionId, _db_trx.SqlTransaction, out _user_id, out _user_name, out _cashier_id, out _cashier_name, out _final_movement_balance, out _dummy_gaming_day))
        {
          CashierSessionInfo = new CashierSessionInfo()
          {
            CashierSessionId = CashierSessionId,
            GamingDay = _dummy_gaming_day,
            TerminalId = _cashier_id,
            TerminalName = _cashier_name,
            UserId = _user_id,
            UserName = _user_name,

            //******************************
            IsMobileBank = false,
            AuthorizedByUserId = _user_id,
            AuthorizedByUserName = _user_name,
            UserType = GU_USER_TYPE.USER
            //******************************
          };
        }
      }

      return CashierSessionInfo;
    }

    #endregion

#endif
    /// <summary>
    /// Has user permission to reopen cashier session
    /// </summary>
    /// <returns></returns>
    private Boolean HasUserPermissionToReopenCashierSession()
    {
      String _error_message;

      return ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.CashierSessionReOpen,
                                                 ProfilePermissions.TypeOperation.RequestPasswd,
                                                 parent_window,
                                                 out _error_message);
    }

    /// <summary>
    /// Function to Reopen a CashierSession
    /// </summary>
    public void ReOpenCashier()
    {
      ReopenCashierSession _reopen_cashier_session;
      String _error_message;
      ArrayList _voucher_list;

      Int32 _tmp_user_id;
      String _tmp_user_name;
      Int32 _tmp_terminal_id;
      String _tmp_terminal_name;
      Currency _tmp_cashier_balance;

      CashierSessionInfo _cashier_session_info_current;   //current session (closed)
      CashierSessionInfo _cashier_session_info_to_reopen; //session to reopen

      try
      {
        _error_message = String.Empty;

        _reopen_cashier_session = new ReopenCashierSession();
        _voucher_list = null;

        if (!HasUserPermissionToReopenCashierSession())
        {
          _error_message = Resource.String("STR_REOPEN_SESSION_ERROR_USER_DOES_NOT_HAVE_PERMISSION", _reopen_cashier_session.GetReopenTypeDescription());

          frm_message.Show(_error_message,
                           Resource.String("STR_UC_BANK_BTN_CASH_DESK_REOPEN"),
                           MessageBoxButtons.OK,
                           Images.CashierImage.Error,
                           this.ParentForm);
          return;
        }

        using (DB_TRX _db_trx = new DB_TRX())
        {
          // Get current session
          _cashier_session_info_current = m_cashier_session_info.Copy(); //We work with a copy of CashierSessionInfo, to avoid conflicts

          // Get session to reopen
          _cashier_session_info_to_reopen = _reopen_cashier_session.GetLastCashierSessionInfo(_cashier_session_info_current.UserId, _cashier_session_info_current.TerminalId, _db_trx.SqlTransaction);

          if (!WSI.Common.Cashier.GetCashierInfoBySession(_cashier_session_info_to_reopen.CashierSessionId, _db_trx.SqlTransaction, out _tmp_user_id, out _tmp_user_name,
                                                  out _tmp_terminal_id, out _tmp_terminal_name, out _tmp_cashier_balance))
          {
            return;
          }
          // Set the information as the following:
          //  - Machine: is the PC where the GUI is executed.
          //  - User: is the GUI user, not the Cashier user.
          CommonCashierInformation.SetCashierInformation(_cashier_session_info_to_reopen.CashierSessionId, _tmp_user_id, _tmp_user_name, _tmp_terminal_id, _tmp_terminal_name);
          
          _cashier_session_info_to_reopen.AuthorizedByUserId = Cashier.AuthorizedByUserId;
          _cashier_session_info_to_reopen.AuthorizedByUserName = Cashier.AuthorizedByUserName;

          // Set session to reopen as working session
          Cashier.SetUserSession(_cashier_session_info_to_reopen.CashierSessionId);

          try
          {
            //Undo session close (reopen session)
            if (!_reopen_cashier_session.Undo(_cashier_session_info_to_reopen, _db_trx.SqlTransaction, out _error_message, out _voucher_list))
            {
              frm_message.Show(_error_message,
                               Resource.String("STR_UC_BANK_BTN_CASH_DESK_REOPEN"),
                               MessageBoxButtons.OK,
                               Images.CashierImage.Error,
                               this.ParentForm);

              // We have to restore the Cashier session
              Cashier.SetUserSession(_cashier_session_info_current.CashierSessionId);

              return;
            }
          }
          catch (Exception _ex)
          {
            // We have to restore the Cashier session
            Cashier.SetUserSession(_cashier_session_info_current.CashierSessionId);

            Log.Exception(_ex);
          }

          _db_trx.Commit();

          // Force to reser values to take care if the session is expired
          this.parent_window.SystemInfo.GamingDayExpired = WSI.Cashier.SystemInfo.StatusInfo.UNKNOWN;
          this.parent_window.SystemInfo.WorkShiftExpired = WSI.Cashier.SystemInfo.StatusInfo.UNKNOWN;
        }

        if (_voucher_list != null)
        {
          VoucherPrint.Print(_voucher_list);
        }

        RefreshStatus();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    }
  }
}
