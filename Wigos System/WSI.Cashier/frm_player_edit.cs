//------------------------------------------------------------------------------
// Copyright � 2009 Win Systems Ltd.
//------------------------------------------------------------------------------
//  
//   MODULE NAME : frm_player_edit.cs
// 
//   DESCRIPTION : Player tracking edition
// 
// REVISION HISTORY :
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 17-DEC-2009 MBF    First
// 20-SEP-2010 TJG    Player Tracking Level
// 22-NOV-2011 JMM    Change PIN tab added
// 24-NOV-2011 JMM    PIN improvement
// 28-NOV-2011 JMM    PIN length up to MAX_PIN_LENGTH.
// 13-APR-2012 JCM    SaveCard Method now Inserts new Cards
// 17-MAY-2012 JCM    Allow Pin change when Wigos Kiosk is enabled
// 03-AUG-2012 DDM    Modifications from design and added field: document type, holder_name1,
//                    holder_name3 and holder_name3.
// 30-AUG-2012 DDM    Modified functions: - CheckData for the document message. 
//                                        - CheckIdAlreadyExists for the check with id and id type
// 01-OCT-2012 MPO    Required data for entry fields. (Modified CheckData function)
// 01-OCT-2012 QMP    Check for duplicate accounts with the same data
// 05-FEB-2013 LEM    Changed type of some controls for data validating (PhoneNumberTextBox and CharactertextBox)
// 11-FEB-2013 ICS    Added a format validation for the fields
// 12-FEB-2013 ICS    Fixed Bug with non numeric values in birthday fields 
//                    Moved birthday validation from CheckData() to ValidateFormat()
// 14-FEB-2013 JAB    Validate that the wedding date is less than birthday date.
// 27-MAR-2013 LEM    Moved CheckIdAlreadyExists and class ControlsRequired to WSI.Common.CardData
//                    Use IsValidBirthdate and IsValidWeddingdate functions for date validation in ValidateFormat
// 28-MAR-2013 LEM    Added new field Twitter Account
// 13-MAY-2013 JMM    Added next level points calculation
// 17-MAY-2013 JMM    Added Block tab
// 29-MAY-2013 JMM    Temporary next level calculation disabled for multisite members sites.
// 13-JUN-2013 ACM    Added three new documents types.
// 14-JUN-2013 RRR    #854: Set card as paid when account is created and card price is 0
// 04-JUL-2013 LEM    Added Audit for PlayerTracking fields.
// 09-AGO-2013 LEM    Auto TAB for date fields.
// 13-AUG-2013 DRV    Add automatic OSK support.
// 16-AUG-2013 DRV    Changed the way that documents are checked if allready exists.
// 19-AUG-2013 CCG    WIG-133: Add text validation for ocupation fields.
// 20-AUG-2013 FBA    WIG-104: Fixed bug with auto-tab on date fields.
// 28-AUG-2013 JAB    Added new camps in Customized's Voucher.
// 16-SEP-2013 FBA    Added new labels in Points tab.
// 17-OCT-2013 JAB    Added new button "Print Information"
// 25-OCT-2013 ACM    Fixed Bug #WIG-313: Ticket does't print nationality and birthplace.
// 25-OCT-2013 JAB    Setup date format over some labels.
// 25-OCT-2013 JAB    Fixed Bug #WIG-319: Error in points program tab, error in format values.
// 25-OCT-2013 JAB    Fixed Bug #WIG-321: Error in points program tab, error in format values.
// 25-OCT-2013 JAB    Fixed Bug #WIG-323: Error in points program tab, error in format values.
// 04-NOV-2013 ACM    Fixed Bug #WIG-355: Logic of "Print Data" Button
// 19-NOV-2013 FBA    Fixed Bug #WIG-413: Birth country and occupation validated incorrectly
// 09-DEC-2013 QMP    Check RFCs against regular expression and do not allow generic RFCs
// 06-FEB-2014 RCI    Focus on Name when creating new accounts. Focus on label (as it was) when account exists.
// 12-FEB-2014 FBA    Added account holder id type control via CardData class IdentificationTypes
// 17-MAR-2014 JRM    Added new functionality. Description contained in RequestPinCodeDUT.docx DUT
// 09-APR-2014 LEM    Confirm saving data before modify PIN if new account
// 02-JUN-2014 RRR    Fixed Bug WIG-959: PIN required in anonymous accounts when general param disables it
// 20-JUN-2014 LEM    Fixed Bug WIGOSTITO-1241: Wrong RFC validation
// 04-JUL-2014 JCO    Added checkbox chk_show_comments and functionality to show comments.
// 14-JUL-2014 AMF    Regionalization
// 15-JUL-2014 LEM    Fixed Bug WIG-1064: Allow introduce generic RFC.
// 24-JUL-2014 SGB    Change text value of emails and phone numbers in create new contact, part if use only one of type contact
// 30-JUL-2014 LEM    Fixed Bug WIG-1133: Validate RFC/CURP when are not visible.
// 25-AUG-2014 JBC    Fixed Bug WIG-1208: AML second name problem
// 25-SEP-2014 RCI    Fixed Bug WIG-1275: Only show the enabled occupations (and also the one that has the player)
// 28-OCT-2014 SMN    Added new functionality: BLOCK_REASON field can have more than one block reason.
// 04-NOV-2014 DHA    Fixed Bug WIG-1639: if RFC is generic and is anonymous account, not validate country
// 09-JAN-2015 RCI    Added functionality for record card track data in twitter field (Logrand)
// 09-FEB-2015 DRV    Fixed Bug WIG-2028: Personalization is not allowed
// 12-FEB-2015 YNM    Fixed Bug WIG-1087: When unblocking card, the unlocking reason was never show
// 09-MAR-2015 DHA    Added button to generate RFC
// 04-MAY-2015 YNM    Fixed Bug TFS-1160: Add birth Country and nationality on account import
// 04-MAY-2015 DLL & FAV    Fixed Bug WIG-2471: Error when personalize an account
// 03-JUL-2015 YNM    Added functionality to disable Scanner options 
// 12-AGO-2015 SGB    Backlog Item 3296: Add address country in account
// 16-SEP-2015 SGB    Added functionality if value of address is invisible unabled check_address_mode
// 21-SEP-2015 AMF    Fixed Bug TFS-4591: Check ZIP
// 29-SEP-2015 AMF    Fixed Bug TFS-4591: Postal Code: Cashier allow any states
// 05-OCT-2015 AMF    Fixed Bug TFS-4922: Postal Code, Federal entity depends country
// 06-OCT-2015 SGB    Fixed Bug TFS-5075: Postal Code, Not selected Federal entity in combobox.
// 17-DIC-2015 YNM    PBI 7503: Visits/Reception Trinidad y Tobago: search screen
// 28-JAN-2015 YNM    Fixed Bug 8741: Visits/Reception Btn Entry Registry has a wrong format
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using WSI.Common;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Collections;
using System.IO;
using WSI.IDCamera.Model.Exceptions;
using WSI.Cashier.Controls;

namespace WSI.Cashier
{
  public partial class frm_player_edit : frm_base
  {
    #region Constants

    public const int AUDIT_CODE_ACCOUNTS = 27;
    public const int AUDIT_TITLE_ID = 6500 + 4;
    public const int AUDIT_FIELD_ID = 6500 + 2;

    #endregion

    #region Enums



    #endregion Enums

    #region Attributes

    CardData m_card;
    CardData m_card_read;
    Boolean m_antimoneylaundering_enabled;

    // Keep tracking of the actual control selected
    Control m_control = null;

    Boolean m_send_tab = false;
    Boolean m_docs_holder_changed;
    Boolean m_docs_beneficiary_changed;
    Boolean m_print_information;
    Boolean m_must_show_comments;

    Int32 m_legal_age;
    Boolean m_pin_request_enabled;
    Int32 m_kiosk_auto_service;

    Dictionary<String, Object> m_beneficiary_data;
    Dictionary<String, String> m_initial_values;

    String m_scan_tile;

    Int32 m_default_document_type;
    String m_default_country;

    Boolean m_rfc_visible;
    Boolean m_curp_visible;

    Zip.Address m_address_original = new Zip.Address();
    Zip.Address m_address_modified = new Zip.Address();
    Boolean m_combo_charge_dt;

    #endregion

    #region Properties

    public Boolean MustShowComments
    {
      get { return m_must_show_comments; }
      set { m_must_show_comments = value; }
    }

    public Boolean IsFromReception { get;set;}

    #endregion

    #region Constructor

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="Parent"></param>
    /// <param name="Card"></param>
    public frm_player_edit(CardData Card, Boolean _isFromRecep = false)
    {
      this.IsFromReception = _isFromRecep;
      
      InitializeComponent();

      InitControls();

      InitializeControlResources();

      this.m_card = Card;

      Init();

      DuplicateCard();

      KeyboardMessageFilter.RegisterHandler(this, BarcodeType.FilterTrackData, this.TrackDataEvent);
      IDCardScanner.form_id_card_scanner.RegisterHandler(this, HandleEvent);
    }


    new public void Dispose()
    {
      IDCardScanner.form_id_card_scanner.RegisterHandler(null, null);
      KeyboardMessageFilter.UnregisterHandler(this, TrackDataEvent);
      base.Dispose();
    }

    #endregion

    #region Private Methods

    private void TrackDataEvent(KbdMsgEvent Type, Object Data)
    {
      try
      {
        if (Type != KbdMsgEvent.EventTrackData)
        {
          return;
        }
        if (tab_player_edit.SelectedTab != tab_player_edit.TabPages["contact"])
        {
          return;
        }

        txt_twitter.Text = (String)Data;
      }
      catch
      { }
    }

    private void SetFormTitle()
    {
      //   - Title
      FormTitle  = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_TITLE");

      if (MustShowComments)
      {
        FormTitle = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_TITLE_COMMENTS");
      }

      if (!String.IsNullOrEmpty(m_card.PlayerTracking.HolderName))
      {
        FormTitleArrowVisible = true;
        FormSubTitle = " " + m_card.AccountId + " - " + m_card.PlayerTracking.HolderName;
      }

    }

    private void InitControls()
    {
      Boolean _scanner_enabled;

      _scanner_enabled = GeneralParam.GetBoolean("IDCardScanner", "Enabled") && IDCardScannerHelper.Enabled;

      EventLastAction.AddEventLastAction(this.Controls);

      this.btn_scan_docs.Click += new EventHandler(this.btn_scan_docs_Click);
      this.btn_ben_scan_docs.Click += new EventHandler(this.btn_ben_scan_docs_Click);
      this.btn_print_information.Click += new EventHandler(this.btn_print_information_Click);

      this.btn_save_pin.Click += new EventHandler(this.btn_save_pin_Click);
      this.btn_generate_random_pin.Click += new EventHandler(this.btn_generate_random_pin_Click);

      this.btn_id_card_scan.Visible = _scanner_enabled;

      this.btn_reg_entry1.Visible = IsFromReception;

      if (_scanner_enabled)
      {
        IDCardScanner.Snapshell2.InitializeCheckScan();
      }
      else 
      {
        this.btn_reg_entry1.Location = this.btn_id_card_scan.Location;
        }
      }

    /// <summary>
    /// Initialize NLS strings
    /// </summary>
    private void InitializeControlResources()
    {

      // - NLS Strings:

      //   - Labels
      lbl_name.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_NAME");
      lbl_last_name1.Text = Resource.String("STR_FRM_PLAYER_EDIT_LAST_NAME1");
      lbl_last_name2.Text = Resource.String("STR_FRM_PLAYER_EDIT_LAST_NAME2");
      lbl_name4.Text = Resource.String("STR_FRM_PLAYER_EDIT_NAME4");
      lbl_full_name.Text = Resource.String("STR_FRM_PLAYER_EDIT_FULL_NAME");
      lbl_type_document.Text = Resource.String("STR_FRM_PLAYER_EDIT_TYPE_DOCUMENT");
      lbl_document.Text = Resource.String("STR_FRM_PLAYER_EDIT_DOCUMENT");
      btn_RFC.Text = IdentificationTypes.DocIdTypeString(ACCOUNT_HOLDER_ID_TYPE.RFC);
      lbl_CURP.Text = IdentificationTypes.DocIdTypeString(ACCOUNT_HOLDER_ID_TYPE.CURP);
      lbl_nationality.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_NATIONALITY");
      lbl_birth_country.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_BIRTH_COUNTRY");
      lbl_occupation.Text = Resource.String("STR_FRM_PLAYER_EDIT_OCCUPATION");
      btn_scan_docs.Text = Resource.String("STR_FRM_PLAYER_EDIT_SCAN");
      btn_print_information.Text = Resource.String("STR_FRM_PRINT_INFORMATION");
      btn_reg_entry1.Text = Resource.String("STR_FRM_SEARCH_RECEPTION_ENTRY");
      lbl_no_photo.Text = Resource.String("STR_FRM_SEARCH_NO_PHOTO");

      lbl_address_01.Text = Resource.String("STR_UC_PLAYER_TRACK_ADDRESS_01");
      lbl_address_02.Text = Resource.String("STR_UC_PLAYER_TRACK_ADDRESS_02");
      lbl_address_03.Text = Resource.String("STR_UC_PLAYER_TRACK_ADDRESS_03");
      lbl_city.Text = Resource.String("STR_UC_PLAYER_TRACK_CITY");
      lbl_zip.Text = Resource.String("STR_UC_PLAYER_TRACK_ZIP");
      lbl_ext_num.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_EXT_NUM");
      lbl_country.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_COUNTRY");
      lbl_fed_entity.Text = AccountFields.GetStateName();
      lbl_address_01_new.Text = Resource.String("STR_UC_PLAYER_TRACK_ADDRESS_01");
      lbl_address_02_new.Text = Resource.String("STR_UC_PLAYER_TRACK_ADDRESS_02");
      lbl_address_03_new.Text = Resource.String("STR_UC_PLAYER_TRACK_ADDRESS_03");
      lbl_city_new.Text = Resource.String("STR_UC_PLAYER_TRACK_CITY");
      lbl_zip_new.Text = Resource.String("STR_UC_PLAYER_TRACK_ZIP");
      lbl_ext_num_new.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_EXT_NUM");
      lbl_country_new.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_COUNTRY");
      lbl_fed_entity_new.Text = AccountFields.GetStateName();
      chk_address_mode.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MANUAL_ENTRY");

      lbl_email_01.Text = Resource.String("STR_UC_PLAYER_TRACK_EMAIL_01") + " 1";
      lbl_email_02.Text = Resource.String("STR_UC_PLAYER_TRACK_EMAIL_01") + " 2";
      lbl_twitter.Text = Resource.String("STR_UC_PLAYER_TRACK_TWITTER");
      lbl_phone_01.Text = Resource.String("STR_UC_PLAYER_TRACK_PHONE_01") + " 1";
      lbl_phone_02.Text = Resource.String("STR_UC_PLAYER_TRACK_PHONE_01") + " 2";
      lbl_gender.Text = Resource.String("STR_UC_PLAYER_TRACK_GENDER");
      lbl_marital_status.Text = Resource.String("STR_UC_PLAYER_TRACK_MARITAL_STATUS");
      lbl_birth_date.Text = Resource.String("STR_FRM_PLAYER_EDIT_BIRTH_DATE");
      lbl_wedding_date.Text = Resource.String("STR_FRM_PLAYER_EDIT_WEDDING_DATE");
      lbl_level_prefix.Text = Resource.String("STR_UC_PLAYER_TRACK_LEVEL") + ":";
      lbl_level_entered_prefix.Text = Resource.String("STR_UC_PLAYER_TRACK_LEVEL_ENTERED") + ":";
      lbl_level_expiration_prefix.Text = Resource.String("STR_UC_PLAYER_TRACK_LEVEL_EXPIRATION") + ":";
      lbl_points_prefix.Text = Resource.String("STR_UC_CARD_POINTS_BALANCE_TITLE") + ":";
      lbl_points_sufix.Text = Resource.String("STR_UC_CARD_POINTS_BALANCE_SUFFIX");
      //FBA 16-SEP-2013
      gb_next_level.HeaderText = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_NEXT_LEVEL");
      lbl_points_level_prefix.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_LEVEL_POINTS");
      lbl_points_req_prefix.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_REQUIRED_POINTS");
      lbl_points_to_next_level_prefix.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_POINTS_TO_NEXT_LEVEL");
      lbl_points_discretionaries_prefix.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_DISCRETIONATED_POINTS");
      lbl_points_generated_prefix.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_GENERATED_POINTS");
      lbl_points_last_days.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_DAYS_COUNTING_POINTS", GeneralParam.GetString("PlayerTracking", "Levels.DaysCountingPoints"));
      lbl_points_to_maintain_prefix.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_POINTS_TO_MAINTAIN_LEVEL");

      lbl_pin.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_PIN");
      lbl_confirm_pin.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_CONFIRM_PIN");
      lbl_block_description.Text = Resource.String("STR_FRM_BLOCK_ACCOUNT_LABEL_DESCRIPTION_UNBLOCK");

      lbl_beneficiary_name.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_NAME");
      lbl_beneficiary_last_name1.Text = Resource.String("STR_FRM_PLAYER_EDIT_LAST_NAME1");
      lbl_beneficiary_last_name2.Text = Resource.String("STR_FRM_PLAYER_EDIT_LAST_NAME2");
      lbl_beneficiary_full_name.Text = Resource.String("STR_FRM_PLAYER_EDIT_FULL_NAME");
      lbl_beneficiary_RFC.Text = IdentificationTypes.DocIdTypeString(ACCOUNT_HOLDER_ID_TYPE.RFC);
      lbl_beneficiary_CURP.Text = IdentificationTypes.DocIdTypeString(ACCOUNT_HOLDER_ID_TYPE.CURP);
      lbl_beneficiary_gender.Text = Resource.String("STR_UC_PLAYER_TRACK_GENDER");
      lbl_beneficiary_birth_date.Text = Resource.String("STR_FRM_PLAYER_EDIT_BIRTH_DATE");
      lbl_beneficiary_occupation.Text = Resource.String("STR_FRM_PLAYER_EDIT_OCCUPATION");
      chk_holder_has_beneficiary.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_HOLDER_HAS_BENEF");
      btn_ben_scan_docs.Text = Resource.String("STR_FRM_PLAYER_EDIT_SCAN");

      tab_player_edit.TabPages["principal"].Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_TAB_MAIN");
      tab_player_edit.TabPages["address"].Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_TAB_ADDRESS");
      tab_player_edit.TabPages["contact"].Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_TAB_CONTACT");
      tab_player_edit.TabPages["comments"].Text = Resource.String("STR_UC_PLAYER_TRACK_COMMENTS");
      tab_player_edit.TabPages["points"].Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_TAB_POINTS");
      tab_player_edit.TabPages["pin"].Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_PIN");
      tab_player_edit.TabPages["block"].Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_TAB_BLOCK");
      tab_player_edit.TabPages["beneficiary"].Text = Resource.String("STR_FRM_PLAYER_EDIT_BENEFICIARY");

      if (Accounts.DoPinRequestFlagsExist(false))
      {
        m_pin_request_enabled = true;
      }

      if (!Int32.TryParse(WSI.Common.Misc.ReadGeneralParams("WigosKiosk", "Enabled"), out m_kiosk_auto_service))
      {
        m_kiosk_auto_service = 0;
      }

      // 17-MAY-2012, JCM: Remove Tab Page Pin when Pin is not required for any thing
      if (!m_pin_request_enabled && m_kiosk_auto_service == 0)
      {
        tab_player_edit.TabPages.Remove(tab_player_edit.TabPages["pin"]);
      }

      //   - Buttons
      btn_ok.Text = (!GeneralParam.GetBoolean("Reception", "Enabled", false)) ? Resource.String("STR_FORMAT_GENERAL_BUTTONS_OK") : Resource.String("STR_DB_SAVE");
      btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");
      btn_save_pin.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_SAVE_PIN");
      btn_generate_random_pin.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_GENERATE_RANDOM_PIN");

      txt_pin.MaxLength = Accounts.PIN_MAX_SAVE_LENGTH;
      txt_confirm_pin.MaxLength = Accounts.PIN_MAX_SAVE_LENGTH;

      chk_show_comments.Text = Resource.String("STR_FRM_PLAYER_EDIT_SHOW_COMMENTS");

      uc_dt_birth.Init();
      uc_dt_wedding.Init();
      uc_dt_ben_birth.Init();

    } // InitializeControlResources

    /// <summary>
    /// Initialize screen logic
    /// </summary>
    private void Init()
    {
      // Center screen
      this.Location = new Point(1024 / 2 - this.Width / 2, 0);

#if DEBUG
      WSI.Common.GeneralParam.ReloadGP();
#endif

      // Show keyboard if Automatic OSK is enabled
      WSIKeyboard.Toggle();

      m_default_document_type = GeneralParam.GetInt32("Account.DefaultValues", "DocumentType");
      m_default_country = GeneralParam.GetString("Account.DefaultValues", "Country");

      // Load combo box content
      LoadCombos();

      // TJG 20-SEP-2010 
      // Player Tracking Level before calling LoadCard

      // Load existing card content from DB
      LoadCard();

      this.m_antimoneylaundering_enabled = GeneralParam.GetBoolean("AntiMoneyLaundering", "Enabled");
      this.m_print_information = false;
      if (!WSI.Common.Misc.HasBeneficiaryAndNotAMLExternalControl())
      {
        tab_player_edit.TabPages.Remove(beneficiary);
      }

      if (!WSI.Common.VisibleData.IsContactVisible())
      {
        tab_player_edit.TabPages.Remove(contact);
      }

      if (!WSI.Common.VisibleData.IsAddressVisible())
      {
        tab_player_edit.TabPages.Remove(address);
      }

      if (!WSI.Common.VisibleData.IsAllAddressVisible())
      {
        // Force manual if some address textbox is not visible
        chk_address_mode.Checked = true; 
        chk_address_mode.Enabled = false;
      }

      // RCI 19-AUG-2010: Disable timer. Check data only on Ok button press.
      // Check if data is correct and activate timer
      //CheckData(true);

      MustShowComments = false;

    }

    private Control[] ToList(params Control[] Ctrls)
    {
      return Ctrls;
    }

    /// <summary>
    /// Load existing card content from DB
    /// </summary>
    private void LoadCard()
    {
      txt_name.Text = m_card.PlayerTracking.HolderName3;
      txt_name4.Text = m_card.PlayerTracking.HolderName4;
      txt_last_name1.Text = m_card.PlayerTracking.HolderName1;
      txt_last_name2.Text = m_card.PlayerTracking.HolderName2;
      lbl_full_name.Text = m_card.PlayerTracking.HolderName;

      if (m_card.PlayerTracking.HolderIdType == ACCOUNT_HOLDER_ID_TYPE.RFC)
      {
        txt_document.Text = String.Empty;
        txt_RFC.Text = m_card.PlayerTracking.HolderId;
        txt_CURP.Text = m_card.PlayerTracking.HolderId2;
      }
      else if (m_card.PlayerTracking.HolderIdType == ACCOUNT_HOLDER_ID_TYPE.CURP)
      {
        txt_document.Text = String.Empty;
        txt_CURP.Text = m_card.PlayerTracking.HolderId;
        txt_RFC.Text = m_card.PlayerTracking.HolderId1;
      }
      else
      {
        txt_document.Text = m_card.PlayerTracking.HolderId;
        txt_RFC.Text = m_card.PlayerTracking.HolderId1;
        txt_CURP.Text = m_card.PlayerTracking.HolderId2;
      }

      if ((m_card.PlayerTracking.HolderIdType == ACCOUNT_HOLDER_ID_TYPE.RFC) ||
         (m_card.PlayerTracking.HolderIdType == ACCOUNT_HOLDER_ID_TYPE.CURP))
      {
        cb_document_type.SelectedValue = (Int32)ACCOUNT_HOLDER_ID_TYPE.UNKNOWN;
      }
      else
      {
        cb_document_type.SelectedValue = (Int32)m_card.PlayerTracking.HolderIdType;
        if (cb_document_type.SelectedValue == null)
        {
          cb_document_type.SelectedValue = m_default_document_type;
        }
      }

      txt_address_01.Text = m_card.PlayerTracking.HolderAddress01;
      txt_address_02.Text = m_card.PlayerTracking.HolderAddress02;
      txt_address_03.Text = m_card.PlayerTracking.HolderAddress03;

      uc_dt_birth.DateValue = m_card.PlayerTracking.HolderBirthDate;

      // ICS 13-FEB-2013: Wedding date
      uc_dt_wedding.DateValue = m_card.PlayerTracking.HolderWeddingDate;

      txt_city.Text = m_card.PlayerTracking.HolderCity;
      txt_zip.Text = m_card.PlayerTracking.HolderZip;
      txt_phone_01.Text = m_card.PlayerTracking.HolderPhone01;
      txt_phone_02.Text = m_card.PlayerTracking.HolderPhone02;
      txt_email_01.Text = m_card.PlayerTracking.HolderEmail01;
      txt_email_02.Text = m_card.PlayerTracking.HolderEmail02;
      txt_twitter.Text = m_card.PlayerTracking.HolderTwitter;
      txt_comments.Text = m_card.PlayerTracking.HolderComments;

      // Select combos option
      if (m_card.PlayerTracking.HolderMaritalStatus > 0)
      {
        cb_marital.SelectedIndex = m_card.PlayerTracking.HolderMaritalStatus - 1;
      }
      else
      {
        cb_marital.SelectedIndex = (Int32)ACCOUNT_MARITAL_STATUS.UNSPECIFIED;
      }

      if (m_card.PlayerTracking.HolderGender > 0)
      {
        cb_gender.SelectedIndex = m_card.PlayerTracking.HolderGender - 1;
      }

      // Select default doc for new custom cards o customized accounts
      if (m_card.IsNew || m_card.PlayerTracking.CardLevel == 0)
      {
        cb_document_type.SelectedValue = m_default_document_type;
      }

      // TJG 20-SEP-2010  Player Tracking Level
      // RCI 10-NOV-2010: Player Tracking Level modified... now, it only shows level info.
      lbl_level.Text = LoyaltyProgram.LevelName(m_card.PlayerTracking.CardLevel);
      lbl_points.Text = m_card.PlayerTracking.TruncatedPoints.ToString();
      // RCI 10-NOV-2010: Show Entered and Expiration Level dates.
      lbl_level_entered.Text = m_card.PlayerTracking.HolderLevelEntered == DateTime.MinValue ?
                                                     "---" : Format.CustomFormatDateTime(m_card.PlayerTracking.HolderLevelEntered, true);
      lbl_level_expiration.Text = m_card.PlayerTracking.HolderLevelExpiration == DateTime.MinValue ?
                                                     "---" : Format.CustomFormatDateTime(m_card.PlayerTracking.HolderLevelExpiration, true);

      // FBA 16-SEP-2013: Gets name, points generated, points required for next level
      GetNextLevelInfo();

      if (m_card.PlayerTracking.Pin != "")
      {
        txt_pin.Text = "0000" + m_card.PlayerTracking.Pin;
        txt_pin.Text = txt_pin.Text.Substring(txt_pin.Text.Length - 4);
      }

      btn_save_pin.Enabled = false;
      lbl_pin_msg.Visible = false;
      txt_confirm_pin.Enabled = false;

      // JMM 16-MAY-2013: Block Tab      
      lbl_blocked.Text = Resource.String("STR_UC_CARD_BLOCK_STATUS_UNLOCKED");
      txt_block_description.Text = "";
      txt_block_description.Visible = m_card.Blocked;
      lbl_block_description.Visible = m_card.Blocked;
      if (m_card.Blocked)
      {
        pb_unblocked.Hide();
      }
      else
      {
        pb_blocked.Hide();
      }

      if (m_card.Blocked)
      {
        String _coma = String.Empty;
        lbl_blocked.Text = Resource.String("STR_UC_CARD_BLOCK_STATUS_LOCKED") + " ";

        foreach (KeyValuePair<AccountBlockReason, String> _block_reason_ in Accounts.GetBlockReason((Int32)m_card.BlockReason))
        {
          lbl_blocked.Text += _coma + _block_reason_.Value;
          _coma = ", ";
        }
        txt_block_description.Text = m_card.BlockDescription;
      }
      else if (!String.IsNullOrEmpty(m_card.BlockDescription))
      {
        lbl_block_description.Visible = true;
        txt_block_description.Visible = true;
        lbl_block_description.Text = Resource.String("STR_FRM_BLOCK_ACCOUNT_LABEL_DESCRIPTION_UNBLOCK_SEC");
        txt_block_description.Text = m_card.BlockDescription;
      }

      Occupations.Update(cb_occupation.DataSource, m_card.PlayerTracking.HolderOccupationId, 1);
      cb_occupation.SelectedValue = m_card.PlayerTracking.HolderOccupationId;
      cb_nationality.SelectedValue = (m_card.PlayerTracking.HolderNationality == 0) ? 0 : m_card.PlayerTracking.HolderNationality;
      cb_birth_country.SelectedValue = (m_card.PlayerTracking.HolderBirthCountry == 0) ? 0 : m_card.PlayerTracking.HolderBirthCountry;

      if (m_card.IsNew || m_card.PlayerTracking.CardLevel == 0)
      {
        String _default_country;

        _default_country = m_default_country;
        if (!String.IsNullOrEmpty(_default_country))
        {
          DataRow[] _rows;
          DataTable _dt_country;

          _dt_country = (DataTable)cb_birth_country.DataSource;
          _rows = _dt_country.Select("CO_ISO2='" + _default_country + "'");

          if (_rows.Length > 0)
          {
            cb_birth_country.SelectedValue = _rows[0][0];
            cb_nationality.SelectedValue = _rows[0][0];
          }
        }
      }

      txt_ext_num.Text = m_card.PlayerTracking.HolderExtNumber;

      if (m_card.IsNew || m_card.PlayerTracking.CardLevel == 0)
      {
        int _country_id;
        if (Countries.ISO2ToCountryId(GeneralParam.GetString("RegionalOptions", "CountryISOCode2", "MX"), out _country_id))
        {
          m_card.PlayerTracking.HolderAddressCountry = _country_id;
        }
      }

      this.cb_country.SelectedIndexChanged -= new System.EventHandler(this.cb_country_SelectedIndexChanged);
      cb_country.SelectedValue = m_card.PlayerTracking.HolderAddressCountry;
      this.cb_country.SelectedIndexChanged += new System.EventHandler(this.cb_country_SelectedIndexChanged);

      this.cb_country_new.SelectedIndexChanged -= new System.EventHandler(this.cb_country_new_SelectedIndexChanged);
      cb_country_new.SelectedValue = m_card.PlayerTracking.HolderAddressCountry;
      this.cb_country_new.SelectedIndexChanged += new System.EventHandler(this.cb_country_new_SelectedIndexChanged);

      //Fed entities depends on country
      LoadFedEntitiesCombo();

      cb_fed_entity.SelectedValue = m_card.PlayerTracking.HolderFedEntity;

      FillStructAddress(ref m_address_original);
      FillStructAddress(ref m_address_modified);

      lbl_address_validation.Text = CardData.GetHolderAddressValidationString(m_card.PlayerTracking.HolderAddressValidation);

      //Enable/Disable controls of beneficiary tab
      btn_ben_scan_docs.Enabled = m_card.PlayerTracking.HolderHasBeneficiary;
      lbl_beneficiary_last_name1.Enabled = m_card.PlayerTracking.HolderHasBeneficiary;
      txt_beneficiary_last_name1.Enabled = m_card.PlayerTracking.HolderHasBeneficiary;
      lbl_beneficiary_last_name2.Enabled = m_card.PlayerTracking.HolderHasBeneficiary;
      txt_beneficiary_last_name2.Enabled = m_card.PlayerTracking.HolderHasBeneficiary;
      txt_beneficiary_name.Enabled = m_card.PlayerTracking.HolderHasBeneficiary;
      lbl_beneficiary_name.Enabled = m_card.PlayerTracking.HolderHasBeneficiary;
      lbl_beneficiary_full_name.Enabled = m_card.PlayerTracking.HolderHasBeneficiary;
      lbl_beneficiary_RFC.Enabled = m_card.PlayerTracking.HolderHasBeneficiary;
      txt_beneficiary_RFC.Enabled = m_card.PlayerTracking.HolderHasBeneficiary;
      lbl_beneficiary_CURP.Enabled = m_card.PlayerTracking.HolderHasBeneficiary;
      txt_beneficiary_CURP.Enabled = m_card.PlayerTracking.HolderHasBeneficiary;
      lbl_beneficiary_occupation.Enabled = m_card.PlayerTracking.HolderHasBeneficiary;
      cb_beneficiary_occupation.Enabled = m_card.PlayerTracking.HolderHasBeneficiary;
      lbl_beneficiary_gender.Enabled = m_card.PlayerTracking.HolderHasBeneficiary;
      cb_beneficiary_gender.Enabled = m_card.PlayerTracking.HolderHasBeneficiary;
      lbl_beneficiary_birth_date.Enabled = m_card.PlayerTracking.HolderHasBeneficiary;
      uc_dt_ben_birth.Enabled = m_card.PlayerTracking.HolderHasBeneficiary;

      //Beneficiary tab data
      if (!m_card.PlayerTracking.HolderHasBeneficiary)
      {
        txt_beneficiary_last_name1.Text = String.Empty;
        txt_beneficiary_last_name2.Text = String.Empty;
        txt_beneficiary_name.Text = String.Empty;
        lbl_beneficiary_full_name.Text = String.Empty;
        cb_beneficiary_occupation.SelectedValue = 0;
        cb_beneficiary_gender.SelectedValue = 0;
        txt_beneficiary_RFC.Text = String.Empty;
        txt_beneficiary_CURP.Text = String.Empty;
        uc_dt_ben_birth.DateValue = DateTime.MinValue;
      }
      else
      {
        txt_beneficiary_last_name1.Text = m_card.PlayerTracking.BeneficiaryName1;
        txt_beneficiary_last_name2.Text = m_card.PlayerTracking.BeneficiaryName2;
        txt_beneficiary_name.Text = m_card.PlayerTracking.BeneficiaryName3;
        lbl_beneficiary_full_name.Text = m_card.PlayerTracking.BeneficiaryName;
        Occupations.Update(cb_beneficiary_occupation.DataSource, m_card.PlayerTracking.BeneficiaryOccupationId, 1);
        cb_beneficiary_occupation.SelectedValue = m_card.PlayerTracking.BeneficiaryOccupationId;
        cb_beneficiary_gender.SelectedValue = m_card.PlayerTracking.BeneficiaryGender;
        txt_beneficiary_RFC.Text = m_card.PlayerTracking.BeneficiaryId1;
        txt_beneficiary_CURP.Text = m_card.PlayerTracking.BeneficiaryId2;

        uc_dt_ben_birth.DateValue = m_card.PlayerTracking.BeneficiaryBirthDate;
      }

      chk_holder_has_beneficiary.Checked = m_card.PlayerTracking.HolderHasBeneficiary;
      chk_show_comments.Checked = m_card.PlayerTracking.ShowCommentsOnCashier;


      // PGJ 04-JUL-2013: This has to be done after the txt_beneficiary_birth_XX = ""; because
      // next instruction triggers the check_changed event and there will improperly save
      // the default date of the beneficiary birthdate textboxes
      if (m_beneficiary_data == null)
      {
        m_beneficiary_data = new Dictionary<string, object>();
      }

      m_beneficiary_data.Add("txt_beneficiary_last_name1", m_card.PlayerTracking.BeneficiaryName1);
      m_beneficiary_data.Add("txt_beneficiary_last_name2", m_card.PlayerTracking.BeneficiaryName2);
      m_beneficiary_data.Add("txt_beneficiary_name", m_card.PlayerTracking.BeneficiaryName3);
      m_beneficiary_data.Add("txt_beneficiary_RFC", m_card.PlayerTracking.BeneficiaryId1);
      m_beneficiary_data.Add("txt_beneficiary_CURP", m_card.PlayerTracking.BeneficiaryId2);
      m_beneficiary_data.Add("cb_beneficiary_occupation", m_card.PlayerTracking.BeneficiaryOccupationId);

      m_beneficiary_data.Add("uc_dt_ben_birth", m_card.PlayerTracking.BeneficiaryBirthDate);

      m_beneficiary_data.Add("cb_beneficiary_gender", m_card.PlayerTracking.BeneficiaryGender);
      m_beneficiary_data.Add("BeneficiaryId3Type", m_card.PlayerTracking.BeneficiaryId3Type);

      m_docs_holder_changed = false;
      m_docs_beneficiary_changed = false;

      // DHA 09-MAR-2015
      EnableRfcButton();

      // AMF 02-SEP-2015
      if (m_card.IsNew && Zip.ValidationCountryDefault(m_card.PlayerTracking.HolderAddressCountry))
      {
        chk_address_mode.Checked = false;
        chk_address_mode_CheckedChanged(null, null);
      }
      else
      {
        chk_address_mode.Checked = true;
      }

    }

    private void HandleEvent(IDCardScanner.ScannedPersonalInfoEvent Ev)
    {
      if (!String.IsNullOrEmpty(Ev.info.Name1)
          || !String.IsNullOrEmpty(Ev.info.Name2)
          || !String.IsNullOrEmpty(Ev.info.Name3)
          || !String.IsNullOrEmpty(Ev.info.Name4))
      {
        txt_name.Text = Ev.info.Name3;
        txt_name4.Text = Ev.info.Name4;
        txt_last_name1.Text = Ev.info.Name1;
        txt_last_name2.Text = Ev.info.Name2;
      }

      if (!String.IsNullOrEmpty(Ev.info.Gender))
      {
        GENDER _gender;

        switch (Ev.info.Gender)
        {
          case "M": _gender = GENDER.MALE; break;
          case "F": _gender = GENDER.FEMALE; break;
          default: _gender = GENDER.UNKNOWN; break;
        }
        cb_gender.SelectedIndex = ((int)_gender) - 1;
      }

      if (Ev.info.BirthDate != DateTime.MinValue)
      {
        uc_dt_birth.DateValue = Ev.info.BirthDate;
      }

      if (!String.IsNullOrEmpty(Ev.info.DocumentID))
      {
        String _doc_type;
        _doc_type = String.IsNullOrEmpty(Ev.info.DocumentType) ? "" : Ev.info.DocumentType;
        cb_document_type.SelectedValue = IdentificationTypes.ScannedTypeToID(_doc_type);
        txt_document.Text = Ev.info.DocumentID;
      }

      if (!String.IsNullOrEmpty(Ev.info.ZipCode))
      {
        txt_zip.Text = Ev.info.ZipCode;
      }

      if (!String.IsNullOrEmpty(Ev.info.City))
      {
        txt_city.Text = Ev.info.City;
      }

      if (!String.IsNullOrEmpty(Ev.info.Address[0])) txt_address_01.Text = Ev.info.Address[0];
      if (!String.IsNullOrEmpty(Ev.info.Address[1])) txt_address_02.Text = Ev.info.Address[1];
      if (!String.IsNullOrEmpty(Ev.info.Address[2])) txt_address_03.Text = Ev.info.Address[2];

      if (!String.IsNullOrEmpty(Ev.info.CountryISO2))
      {
        int _country_id;
        if (Countries.ISO2ToCountryId(Ev.info.CountryISO2, out _country_id))
        {
          cb_nationality.SelectedValue = _country_id;
          cb_birth_country.SelectedValue = _country_id;
        }
      }
      if (Ev.info.image_photo != null)
      {
        m_img_photo.Image = Ev.info.image_photo;
        m_img_photo.Tag = 1;
        lbl_no_photo.Visible = false;
      }
    }

    private void DuplicateCard()
    {
      m_card_read = new CardData();
      m_initial_values = new Dictionary<String, String>();

      m_card_read.PlayerTracking.HolderName3 = m_card.PlayerTracking.HolderName3;
      m_card_read.PlayerTracking.HolderName4 = m_card.PlayerTracking.HolderName4;
      m_card_read.PlayerTracking.HolderName1 = m_card.PlayerTracking.HolderName1;
      m_card_read.PlayerTracking.HolderName2 = m_card.PlayerTracking.HolderName2;
      m_card_read.PlayerTracking.HolderName = m_card.PlayerTracking.HolderName;
      m_card_read.PlayerTracking.HolderId = m_card.PlayerTracking.HolderId;
      m_card_read.PlayerTracking.HolderId1 = m_card.PlayerTracking.HolderId1;
      m_card_read.PlayerTracking.HolderId2 = m_card.PlayerTracking.HolderId2;
      m_card_read.PlayerTracking.HolderAddress01 = m_card.PlayerTracking.HolderAddress01;
      m_card_read.PlayerTracking.HolderAddress02 = m_card.PlayerTracking.HolderAddress02;
      m_card_read.PlayerTracking.HolderAddress03 = m_card.PlayerTracking.HolderAddress03;
      m_card_read.PlayerTracking.HolderBirthDate = m_card.PlayerTracking.HolderBirthDate;
      m_card_read.PlayerTracking.HolderBirthDateText = m_card.PlayerTracking.HolderBirthDate.ToShortDateString();
      m_card_read.PlayerTracking.HolderWeddingDate = m_card.PlayerTracking.HolderWeddingDate;

      m_card_read.PlayerTracking.HolderCity = m_card.PlayerTracking.HolderCity;
      m_card_read.PlayerTracking.HolderZip = m_card.PlayerTracking.HolderZip;
      m_card_read.PlayerTracking.HolderPhone01 = m_card.PlayerTracking.HolderPhone01;
      m_card_read.PlayerTracking.HolderPhone02 = m_card.PlayerTracking.HolderPhone02;
      m_card_read.PlayerTracking.HolderEmail01 = m_card.PlayerTracking.HolderEmail01;
      m_card_read.PlayerTracking.HolderEmail02 = m_card.PlayerTracking.HolderEmail02;
      m_card_read.PlayerTracking.HolderTwitter = m_card.PlayerTracking.HolderTwitter;
      m_card_read.PlayerTracking.HolderComments = m_card.PlayerTracking.HolderComments;

      m_card_read.PlayerTracking.HolderMaritalStatus = m_card.PlayerTracking.HolderMaritalStatus;
      m_card_read.PlayerTracking.HolderGender = m_card.PlayerTracking.HolderGender;
      m_card_read.PlayerTracking.HolderIdType = m_card.PlayerTracking.HolderIdType;
      m_card_read.PlayerTracking.CardLevel = m_card.PlayerTracking.CardLevel;
      m_card_read.PlayerTracking.TruncatedPoints = m_card.PlayerTracking.TruncatedPoints;
      m_card_read.PlayerTracking.HolderLevelEntered = m_card.PlayerTracking.HolderLevelEntered;
      m_card_read.PlayerTracking.HolderLevelExpiration = m_card.PlayerTracking.HolderLevelExpiration;

      m_card_read.PlayerTracking.Pin = m_card.PlayerTracking.Pin;

      m_card_read.Blocked = m_card.Blocked;
      m_card_read.BlockReason = m_card.BlockReason;
      m_card_read.BlockDescription = m_card.BlockDescription;

      m_card_read.PlayerTracking.HolderOccupationId = m_card.PlayerTracking.HolderOccupationId;
      m_card_read.PlayerTracking.HolderNationality = m_card.PlayerTracking.HolderNationality;
      m_card_read.PlayerTracking.HolderBirthCountry = m_card.PlayerTracking.HolderBirthCountry;
      m_card_read.PlayerTracking.HolderExtNumber = m_card.PlayerTracking.HolderExtNumber;
      m_card_read.PlayerTracking.HolderFedEntity = m_card.PlayerTracking.HolderFedEntity;
      m_card_read.PlayerTracking.HolderAddressCountry = m_card.PlayerTracking.HolderAddressCountry;
      m_card_read.PlayerTracking.HolderAddressValidation = m_card.PlayerTracking.HolderAddressValidation;
      m_card_read.PlayerTracking.HolderId3Type = m_card.PlayerTracking.HolderId3Type;

      m_card_read.PlayerTracking.HolderHasBeneficiary = m_card.PlayerTracking.HolderHasBeneficiary;
      m_card_read.PlayerTracking.ShowCommentsOnCashier = m_card.PlayerTracking.ShowCommentsOnCashier;

      //BENEFICIARY
      if (m_card.PlayerTracking.HolderHasBeneficiary)
      {
        m_card_read.PlayerTracking.BeneficiaryName1 = m_card.PlayerTracking.BeneficiaryName1;
        m_card_read.PlayerTracking.BeneficiaryName2 = m_card.PlayerTracking.BeneficiaryName2;
        m_card_read.PlayerTracking.BeneficiaryName3 = m_card.PlayerTracking.BeneficiaryName3;
        m_card_read.PlayerTracking.BeneficiaryOccupationId = m_card.PlayerTracking.BeneficiaryOccupationId;
        m_card_read.PlayerTracking.BeneficiaryGender = m_card.PlayerTracking.BeneficiaryGender;
        m_card_read.PlayerTracking.BeneficiaryBirthDate = m_card.PlayerTracking.BeneficiaryBirthDate;
        m_card_read.PlayerTracking.BeneficiaryId1 = m_card.PlayerTracking.BeneficiaryId1;
        m_card_read.PlayerTracking.BeneficiaryId2 = m_card.PlayerTracking.BeneficiaryId2;
        m_card_read.PlayerTracking.BeneficiaryId3Type = m_card.PlayerTracking.BeneficiaryId3Type;
      }
      else
      {
        m_card_read.PlayerTracking.BeneficiaryName1 = String.Empty;
        m_card_read.PlayerTracking.BeneficiaryName2 = String.Empty;
        m_card_read.PlayerTracking.BeneficiaryName3 = String.Empty;
        m_card_read.PlayerTracking.BeneficiaryOccupationId = -1;
        m_card_read.PlayerTracking.BeneficiaryGender = -1;
        m_card_read.PlayerTracking.BeneficiaryBirthDate = DateTime.MinValue;
        m_card_read.PlayerTracking.BeneficiaryId1 = String.Empty;
        m_card_read.PlayerTracking.BeneficiaryId2 = String.Empty;
        m_card_read.PlayerTracking.BeneficiaryId3Type = String.Empty;
      }

      m_initial_values["HolderId"] = txt_document.Text;
      if (cb_document_type.SelectedValue == null)
      {
        cb_document_type.SelectedValue = m_default_document_type;
      }
      m_initial_values["HolderDocumentType"] = cb_document_type.SelectedValue.ToString();
      m_initial_values["HolderBirthDate"] = uc_dt_birth.DateValue.ToShortDateString();
      m_initial_values["HolderWeddingDate"] = uc_dt_wedding.DateValue.ToShortDateString();
      m_initial_values["BeneficiaryBirthDate"] = uc_dt_ben_birth.DateValue.ToShortDateString();

      if (m_card.AccountId != 0)
      {
        using (DB_TRX _trx = new DB_TRX())
        {
          Image _photo;
          if (IDCardScannerHelper.LoadAccountPhoto(m_card.AccountId, out _photo, _trx.SqlTransaction))
          {
            m_img_photo.Image = _photo;
            m_img_photo.Tag = 0; // No changes
            lbl_no_photo.Visible = (_photo == null);
          }
        }
      }
    }

    private void AuditPlayerData()
    {
      Auditor _auditor;
      String _beneficiary_sufix;
      String _date_read;
      String _date;
      String _audit_none_str;

      _beneficiary_sufix = " " + Resource.String("STR_FRM_PLAYER_EDIT_BENEFICIARY");
      _audit_none_str = Resource.String("STR_AUDIT_NONE_STRING");

      _auditor = new Auditor(AUDIT_CODE_ACCOUNTS);
      _auditor.SetName(AUDIT_TITLE_ID, Resource.String("STR_FORMAT_GENERAL_NAME_ACCOUNT"), m_card.AccountId + " - " + m_card.PlayerTracking.HolderName, "", "", "");


      // MAIN:
      // Name
      if (m_card_read.PlayerTracking.HolderName3 != m_card.PlayerTracking.HolderName3)
      {
        if (String.IsNullOrEmpty(m_card_read.PlayerTracking.HolderName3))
        {
          m_card_read.PlayerTracking.HolderName3 = _audit_none_str;
        }

        if (String.IsNullOrEmpty(m_card.PlayerTracking.HolderName3))
        {
          m_card.PlayerTracking.HolderName3 = _audit_none_str;
        }

        _auditor.SetField(AUDIT_FIELD_ID, Resource.String("STR_FRM_ACCOUNT_USER_EDIT_NAME"), m_card.PlayerTracking.HolderName3, m_card_read.PlayerTracking.HolderName3, "", "");

        if (m_card.PlayerTracking.HolderName3 == _audit_none_str)
        {
          m_card.PlayerTracking.HolderName3 = String.Empty;
        }
      }
      // Middle Name
      if (m_card_read.PlayerTracking.HolderName4 != m_card.PlayerTracking.HolderName4)
      {
        if (String.IsNullOrEmpty(m_card_read.PlayerTracking.HolderName4))
        {
          m_card_read.PlayerTracking.HolderName4 = _audit_none_str;
        }

        if (String.IsNullOrEmpty(m_card.PlayerTracking.HolderName4))
        {
          m_card.PlayerTracking.HolderName4 = _audit_none_str;
        }

        _auditor.SetField(AUDIT_FIELD_ID, Resource.String("STR_FRM_PLAYER_EDIT_NAME4"), m_card.PlayerTracking.HolderName4, m_card_read.PlayerTracking.HolderName4, "", "");

        if (m_card.PlayerTracking.HolderName4 == _audit_none_str)
        {
          m_card.PlayerTracking.HolderName4 = String.Empty;
        }
      }
      // Surname 1
      if (m_card_read.PlayerTracking.HolderName1 != m_card.PlayerTracking.HolderName1)
      {
        if (String.IsNullOrEmpty(m_card_read.PlayerTracking.HolderName1))
        {
          m_card_read.PlayerTracking.HolderName1 = _audit_none_str;
        }

        if (String.IsNullOrEmpty(m_card.PlayerTracking.HolderName1))
        {
          m_card.PlayerTracking.HolderName1 = _audit_none_str;
        }

        _auditor.SetField(AUDIT_FIELD_ID, Resource.String("STR_FRM_PLAYER_EDIT_LAST_NAME1"), m_card.PlayerTracking.HolderName1, m_card_read.PlayerTracking.HolderName1, "", "");

        if (m_card.PlayerTracking.HolderName1 == _audit_none_str)
        {
          m_card.PlayerTracking.HolderName1 = String.Empty;
        }
      }
      // Surname 2
      if (m_card_read.PlayerTracking.HolderName2 != m_card.PlayerTracking.HolderName2)
      {
        if (String.IsNullOrEmpty(m_card_read.PlayerTracking.HolderName2))
        {
          m_card_read.PlayerTracking.HolderName2 = _audit_none_str;
        }

        if (String.IsNullOrEmpty(m_card.PlayerTracking.HolderName2))
        {
          m_card.PlayerTracking.HolderName2 = _audit_none_str;
        }

        _auditor.SetField(AUDIT_FIELD_ID, Resource.String("STR_FRM_PLAYER_EDIT_LAST_NAME2"), m_card.PlayerTracking.HolderName2, m_card_read.PlayerTracking.HolderName2, "", "");

        if (m_card.PlayerTracking.HolderName2 == _audit_none_str)
        {
          m_card.PlayerTracking.HolderName2 = String.Empty;
        }
      }
      // Document
      if (m_card_read.PlayerTracking.HolderId != m_card.PlayerTracking.HolderId)
      {
        if (String.IsNullOrEmpty(m_card_read.PlayerTracking.HolderId))
        {
          m_card_read.PlayerTracking.HolderId = _audit_none_str;
        }

        if (String.IsNullOrEmpty(m_card.PlayerTracking.HolderId))
        {
          m_card.PlayerTracking.HolderId = _audit_none_str;
        }

        _auditor.SetField(AUDIT_FIELD_ID, Resource.String("STR_FRM_PLAYER_EDIT_DOCUMENT"), m_card.PlayerTracking.HolderId, m_card_read.PlayerTracking.HolderId, "", "");

        if (m_card.PlayerTracking.HolderId == _audit_none_str)
        {
          m_card.PlayerTracking.HolderId = String.Empty;
        }
      }

      // RFC
      if (m_card_read.PlayerTracking.HolderId1 != m_card.PlayerTracking.HolderId1)
      {
        if (String.IsNullOrEmpty(m_card_read.PlayerTracking.HolderId1))
        {
          m_card_read.PlayerTracking.HolderId1 = _audit_none_str;
        }

        if (String.IsNullOrEmpty(m_card.PlayerTracking.HolderId1))
        {
          m_card.PlayerTracking.HolderId1 = _audit_none_str;
        }

        _auditor.SetField(AUDIT_FIELD_ID, IdentificationTypes.DocIdTypeString(ACCOUNT_HOLDER_ID_TYPE.RFC), m_card.PlayerTracking.HolderId1, m_card_read.PlayerTracking.HolderId1, "", "");

        if (m_card.PlayerTracking.HolderId1 == _audit_none_str)
        {
          m_card.PlayerTracking.HolderId1 = String.Empty;
        }
      }
      // CURP
      if (m_card_read.PlayerTracking.HolderId2 != m_card.PlayerTracking.HolderId2)
      {
        if (String.IsNullOrEmpty(m_card_read.PlayerTracking.HolderId2))
        {
          m_card_read.PlayerTracking.HolderId2 = _audit_none_str;
        }

        if (String.IsNullOrEmpty(m_card.PlayerTracking.HolderId2))
        {
          m_card.PlayerTracking.HolderId2 = _audit_none_str;
        }

        _auditor.SetField(AUDIT_FIELD_ID, IdentificationTypes.DocIdTypeString(ACCOUNT_HOLDER_ID_TYPE.CURP), m_card.PlayerTracking.HolderId2, m_card_read.PlayerTracking.HolderId2, "", "");

        if (m_card.PlayerTracking.HolderId2 == _audit_none_str)
        {
          m_card.PlayerTracking.HolderId2 = String.Empty;
        }
      }

      // Birth Date
      if (m_card_read.PlayerTracking.HolderBirthDate != m_card.PlayerTracking.HolderBirthDate)
      {
        if (m_card.PlayerTracking.HolderBirthDate != DateTime.MinValue)
        {
          _date = m_card.PlayerTracking.HolderBirthDate.ToShortDateString();
        }
        else
        {
          _date = _audit_none_str;
        }

        if (m_card_read.PlayerTracking.HolderBirthDate != DateTime.MinValue)
        {
          _date_read = m_card_read.PlayerTracking.HolderBirthDate.ToShortDateString();
        }
        else
        {
          _date_read = _audit_none_str;
        }

        _auditor.SetField(AUDIT_FIELD_ID, Resource.String("STR_FRM_PLAYER_EDIT_BIRTH_DATE"), _date, _date_read, "", "");
      }
      // Doc. Type
      if (m_card_read.PlayerTracking.HolderIdType != m_card.PlayerTracking.HolderIdType)
      {
        _auditor.SetField(AUDIT_FIELD_ID, Resource.String("STR_FRM_PLAYER_EDIT_TYPE_DOCUMENT"), IdentificationTypes.DocIdTypeString(m_card.PlayerTracking.HolderIdType), IdentificationTypes.DocIdTypeString(m_card_read.PlayerTracking.HolderIdType), "", "");
      }

      // Scanned Doc. Type
      if (m_docs_holder_changed)
      {
        String _read_holder_id3_type;
        String _holder_id3_type;

        if (m_card_read.PlayerTracking.HolderId3Type == String.Empty)
        {
          _read_holder_id3_type = _audit_none_str;
        }
        else
        {
          _read_holder_id3_type = IdentificationTypes.DocIdTypeStringList(m_card_read.PlayerTracking.HolderId3Type);
        }

        if (m_card.PlayerTracking.HolderId3Type == String.Empty)
        {
          _holder_id3_type = _audit_none_str;
        }
        else
        {
          _holder_id3_type = IdentificationTypes.DocIdTypeStringList(m_card.PlayerTracking.HolderId3Type);
        }

        _auditor.SetField(AUDIT_FIELD_ID, Resource.String("STR_FRM_PLAYER_EDIT_SCAN_DOCUMENT"), _holder_id3_type, _read_holder_id3_type, "", "");
      }
      // Gender
      if (m_card_read.PlayerTracking.HolderGender != m_card.PlayerTracking.HolderGender)
      {
        String _read_holder_gender;
        String _holder_gender;

        if (String.IsNullOrEmpty(CardData.GenderString(m_card_read.PlayerTracking.HolderGender)))
        {
          _read_holder_gender = _audit_none_str;
        }
        else
        {
          _read_holder_gender = CardData.GenderString(m_card_read.PlayerTracking.HolderGender);
        }

        if (String.IsNullOrEmpty(CardData.GenderString(m_card.PlayerTracking.HolderGender)))
        {
          _holder_gender = _audit_none_str;
        }
        else
        {
          _holder_gender = CardData.GenderString(m_card.PlayerTracking.HolderGender);
        }

        _auditor.SetField(AUDIT_FIELD_ID, Resource.String("STR_UC_PLAYER_TRACK_GENDER"), _holder_gender, _read_holder_gender, "", "");
      }
      // Marital Status
      if (m_card_read.PlayerTracking.HolderMaritalStatus != m_card.PlayerTracking.HolderMaritalStatus)
      {
        String _read_marital_status;
        String _marital_status;

        if (String.IsNullOrEmpty(CardData.MaritalStatusString((ACCOUNT_MARITAL_STATUS)(m_card_read.PlayerTracking.HolderMaritalStatus - 1))))
        {
          _read_marital_status = _audit_none_str;
        }
        else
        {
          _read_marital_status = CardData.MaritalStatusString((ACCOUNT_MARITAL_STATUS)(m_card_read.PlayerTracking.HolderMaritalStatus - 1));
        }

        if (String.IsNullOrEmpty(CardData.MaritalStatusString((ACCOUNT_MARITAL_STATUS)(m_card.PlayerTracking.HolderMaritalStatus - 1))))
        {
          _marital_status = _audit_none_str;
        }
        else
        {
          _marital_status = CardData.MaritalStatusString((ACCOUNT_MARITAL_STATUS)(m_card.PlayerTracking.HolderMaritalStatus - 1));
        }

        _auditor.SetField(AUDIT_FIELD_ID, Resource.String("STR_UC_PLAYER_TRACK_MARITAL_STATUS"), _marital_status, _read_marital_status, "", "");
      }
      // Wedding Date
      if (m_card_read.PlayerTracking.HolderWeddingDate != m_card.PlayerTracking.HolderWeddingDate)
      {
        if (m_card.PlayerTracking.HolderWeddingDate != DateTime.MinValue)
        {
          _date = m_card.PlayerTracking.HolderWeddingDate.ToShortDateString();
        }
        else
        {
          _date = _audit_none_str;
        }
        if (m_card_read.PlayerTracking.HolderWeddingDate != DateTime.MinValue)
        {
          _date_read = m_card_read.PlayerTracking.HolderWeddingDate.ToShortDateString();
        }
        else
        {
          _date_read = _audit_none_str;
        }
        _auditor.SetField(AUDIT_FIELD_ID, Resource.String("STR_FRM_PLAYER_EDIT_WEDDING_DATE"), _date, _date_read, "", "");
      }
      // Occupation Id
      if (m_card_read.PlayerTracking.HolderOccupationId != m_card.PlayerTracking.HolderOccupationId)
      {
        String _read_occupation;
        String _occupation;

        if (String.IsNullOrEmpty(CardData.OccupationsDescriptionString((int)m_card_read.PlayerTracking.HolderOccupationId)))
        {
          _read_occupation = _audit_none_str;
        }
        else
        {
          _read_occupation = CardData.OccupationsDescriptionString((int)m_card_read.PlayerTracking.HolderOccupationId);
        }

        if (String.IsNullOrEmpty(CardData.OccupationsDescriptionString((int)m_card.PlayerTracking.HolderOccupationId)))
        {
          _occupation = _audit_none_str;
        }
        else
        {
          _occupation = CardData.OccupationsDescriptionString((int)m_card.PlayerTracking.HolderOccupationId);
        }

        _auditor.SetField(AUDIT_FIELD_ID, Resource.String("STR_FRM_PLAYER_EDIT_OCCUPATION"), _occupation, _read_occupation, "", "");
      }
      // Birth Country
      if (m_card_read.PlayerTracking.HolderBirthCountry != m_card.PlayerTracking.HolderBirthCountry)
      {
        String _read_birth_country;
        String _birth_country;

        if (String.IsNullOrEmpty(CardData.CountriesString((int)m_card_read.PlayerTracking.HolderBirthCountry)))
        {
          _read_birth_country = _audit_none_str;
        }
        else
        {
          _read_birth_country = CardData.CountriesString((int)m_card_read.PlayerTracking.HolderBirthCountry);
        }

        if (String.IsNullOrEmpty(CardData.CountriesString((int)m_card.PlayerTracking.HolderBirthCountry)))
        {
          _birth_country = _audit_none_str;
        }
        else
        {
          _birth_country = CardData.CountriesString((int)m_card.PlayerTracking.HolderBirthCountry);
        }

        _auditor.SetField(AUDIT_FIELD_ID, Resource.String("STR_FRM_ACCOUNT_USER_EDIT_BIRTH_COUNTRY"), _birth_country, _read_birth_country, "", "");
      }
      // Nationality
      if (m_card_read.PlayerTracking.HolderNationality != m_card.PlayerTracking.HolderNationality)
      {
        String _read_nationality;
        String _nationality;

        if (String.IsNullOrEmpty(CardData.NationalitiesString((int)m_card_read.PlayerTracking.HolderNationality)))
        {
          _read_nationality = _audit_none_str;
        }
        else
        {
          _read_nationality = CardData.NationalitiesString((int)m_card_read.PlayerTracking.HolderNationality);
        }

        if (String.IsNullOrEmpty(CardData.NationalitiesString((int)m_card.PlayerTracking.HolderNationality)))
        {
          _nationality = _audit_none_str;
        }
        else
        {
          _nationality = CardData.NationalitiesString((int)m_card.PlayerTracking.HolderNationality);
        }

        _auditor.SetField(AUDIT_FIELD_ID, Resource.String("STR_FRM_ACCOUNT_USER_EDIT_NATIONALITY"), _nationality, _read_nationality, "", "");
      }

      // CONTACT:
      // Phone 1
      if (m_card_read.PlayerTracking.HolderPhone01 != m_card.PlayerTracking.HolderPhone01)
      {
        if (String.IsNullOrEmpty(m_card_read.PlayerTracking.HolderPhone01))
        {
          m_card_read.PlayerTracking.HolderPhone01 = _audit_none_str;
        }

        if (String.IsNullOrEmpty(m_card.PlayerTracking.HolderPhone01))
        {
          m_card.PlayerTracking.HolderPhone01 = _audit_none_str;
        }
        _auditor.SetField(AUDIT_FIELD_ID, Resource.String("STR_UC_PLAYER_TRACK_PHONE_01") + " 1", m_card.PlayerTracking.HolderPhone01, m_card_read.PlayerTracking.HolderPhone01, "", "");

        if (m_card.PlayerTracking.HolderPhone01 == _audit_none_str)
        {
          m_card.PlayerTracking.HolderPhone01 = String.Empty;
        }
      }
      // Phone 2
      if (m_card_read.PlayerTracking.HolderPhone02 != m_card.PlayerTracking.HolderPhone02)
      {
        if (String.IsNullOrEmpty(m_card_read.PlayerTracking.HolderPhone02))
        {
          m_card_read.PlayerTracking.HolderPhone02 = _audit_none_str;
        }

        if (String.IsNullOrEmpty(m_card.PlayerTracking.HolderPhone02))
        {
          m_card.PlayerTracking.HolderPhone02 = _audit_none_str;
        }

        _auditor.SetField(AUDIT_FIELD_ID, Resource.String("STR_UC_PLAYER_TRACK_PHONE_01") + " 2", m_card.PlayerTracking.HolderPhone02, m_card_read.PlayerTracking.HolderPhone02, "", "");

        if (m_card.PlayerTracking.HolderPhone02 == _audit_none_str)
        {
          m_card.PlayerTracking.HolderPhone02 = String.Empty;
        }
      }
      // Email 1
      if (m_card_read.PlayerTracking.HolderEmail01 != m_card.PlayerTracking.HolderEmail01)
      {
        if (String.IsNullOrEmpty(m_card_read.PlayerTracking.HolderEmail01))
        {
          m_card_read.PlayerTracking.HolderEmail01 = _audit_none_str;
        }

        if (String.IsNullOrEmpty(m_card.PlayerTracking.HolderEmail01))
        {
          m_card.PlayerTracking.HolderEmail01 = _audit_none_str;
        }

        _auditor.SetField(AUDIT_FIELD_ID, Resource.String("STR_UC_PLAYER_TRACK_EMAIL_01") + " 1", m_card.PlayerTracking.HolderEmail01, m_card_read.PlayerTracking.HolderEmail01, "", "");


        if (m_card.PlayerTracking.HolderEmail01 == _audit_none_str)
        {
          m_card.PlayerTracking.HolderEmail01 = String.Empty;
        }
      }
      // Email 2
      if (m_card_read.PlayerTracking.HolderEmail02 != m_card.PlayerTracking.HolderEmail02)
      {
        if (String.IsNullOrEmpty(m_card_read.PlayerTracking.HolderEmail02))
        {
          m_card_read.PlayerTracking.HolderEmail02 = _audit_none_str;
        }

        if (String.IsNullOrEmpty(m_card.PlayerTracking.HolderEmail02))
        {
          m_card.PlayerTracking.HolderEmail02 = _audit_none_str;
        }

        _auditor.SetField(AUDIT_FIELD_ID, Resource.String("STR_UC_PLAYER_TRACK_EMAIL_01") + " 2", m_card.PlayerTracking.HolderEmail02, m_card_read.PlayerTracking.HolderEmail02, "", "");

        if (m_card.PlayerTracking.HolderEmail02 == _audit_none_str)
        {
          m_card.PlayerTracking.HolderEmail02 = String.Empty;
        }
      }
      //Twitter
      if (m_card_read.PlayerTracking.HolderTwitter != m_card.PlayerTracking.HolderTwitter)
      {
        if (String.IsNullOrEmpty(m_card_read.PlayerTracking.HolderTwitter))
        {
          m_card_read.PlayerTracking.HolderTwitter = _audit_none_str;
        }

        if (String.IsNullOrEmpty(m_card.PlayerTracking.HolderTwitter))
        {
          m_card.PlayerTracking.HolderTwitter = _audit_none_str;
        }

        _auditor.SetField(AUDIT_FIELD_ID, Resource.String("STR_UC_PLAYER_TRACK_TWITTER"), m_card.PlayerTracking.HolderTwitter, m_card_read.PlayerTracking.HolderTwitter, "", "");

        if (m_card.PlayerTracking.HolderTwitter == _audit_none_str)
        {
          m_card.PlayerTracking.HolderTwitter = String.Empty;
        }
      }

      // ADDRESS:
      // Street
      if (m_card_read.PlayerTracking.HolderAddress01 != m_card.PlayerTracking.HolderAddress01)
      {
        if (String.IsNullOrEmpty(m_card_read.PlayerTracking.HolderAddress01))
        {
          m_card_read.PlayerTracking.HolderAddress01 = _audit_none_str;
        }

        if (String.IsNullOrEmpty(m_card.PlayerTracking.HolderAddress01))
        {
          m_card.PlayerTracking.HolderAddress01 = _audit_none_str;
        }

        _auditor.SetField(AUDIT_FIELD_ID, Resource.String("STR_UC_PLAYER_TRACK_ADDRESS_01"), m_card.PlayerTracking.HolderAddress01, m_card_read.PlayerTracking.HolderAddress01, "", "");

        if (m_card.PlayerTracking.HolderAddress01 == _audit_none_str)
        {
          m_card.PlayerTracking.HolderAddress01 = String.Empty;
        }
      }
      // Colony
      if (m_card_read.PlayerTracking.HolderAddress02 != m_card.PlayerTracking.HolderAddress02)
      {
        if (String.IsNullOrEmpty(m_card_read.PlayerTracking.HolderAddress02))
        {
          m_card_read.PlayerTracking.HolderAddress02 = _audit_none_str;
        }

        if (String.IsNullOrEmpty(m_card.PlayerTracking.HolderAddress02))
        {
          m_card.PlayerTracking.HolderAddress02 = _audit_none_str;
        }

        _auditor.SetField(AUDIT_FIELD_ID, Resource.String("STR_UC_PLAYER_TRACK_ADDRESS_02"), m_card.PlayerTracking.HolderAddress02, m_card_read.PlayerTracking.HolderAddress02, "", "");

        if (m_card.PlayerTracking.HolderAddress02 == _audit_none_str)
        {
          m_card.PlayerTracking.HolderAddress02 = String.Empty;
        }
      }
      // Delegation
      if (m_card_read.PlayerTracking.HolderAddress03 != m_card.PlayerTracking.HolderAddress03)
      {
        if (String.IsNullOrEmpty(m_card_read.PlayerTracking.HolderAddress03))
        {
          m_card_read.PlayerTracking.HolderAddress03 = _audit_none_str;
        }

        if (String.IsNullOrEmpty(m_card.PlayerTracking.HolderAddress03))
        {
          m_card.PlayerTracking.HolderAddress03 = _audit_none_str;
        }

        _auditor.SetField(AUDIT_FIELD_ID, Resource.String("STR_UC_PLAYER_TRACK_ADDRESS_03"), m_card.PlayerTracking.HolderAddress03, m_card_read.PlayerTracking.HolderAddress03, "", "");

        if (m_card.PlayerTracking.HolderAddress03 == _audit_none_str)
        {
          m_card.PlayerTracking.HolderAddress03 = String.Empty;
        }
      }
      // City
      if (m_card_read.PlayerTracking.HolderCity != m_card.PlayerTracking.HolderCity)
      {
        if (String.IsNullOrEmpty(m_card_read.PlayerTracking.HolderCity))
        {
          m_card_read.PlayerTracking.HolderCity = _audit_none_str;
        }

        if (String.IsNullOrEmpty(m_card.PlayerTracking.HolderCity))
        {
          m_card.PlayerTracking.HolderCity = _audit_none_str;
        }

        _auditor.SetField(AUDIT_FIELD_ID, Resource.String("STR_UC_PLAYER_TRACK_CITY"), m_card.PlayerTracking.HolderCity, m_card_read.PlayerTracking.HolderCity, "", "");

        if (m_card.PlayerTracking.HolderCity == _audit_none_str)
        {
          m_card.PlayerTracking.HolderCity = String.Empty;
        }
      }
      // ZIP
      if (m_card_read.PlayerTracking.HolderZip != m_card.PlayerTracking.HolderZip)
      {
        if (String.IsNullOrEmpty(m_card_read.PlayerTracking.HolderZip))
        {
          m_card_read.PlayerTracking.HolderZip = _audit_none_str;
        }

        if (String.IsNullOrEmpty(m_card.PlayerTracking.HolderZip))
        {
          m_card.PlayerTracking.HolderZip = _audit_none_str;
        }

        _auditor.SetField(AUDIT_FIELD_ID, Resource.String("STR_UC_PLAYER_TRACK_ZIP"), m_card.PlayerTracking.HolderZip, m_card_read.PlayerTracking.HolderZip, "", "");

        if (m_card.PlayerTracking.HolderZip == _audit_none_str)
        {
          m_card.PlayerTracking.HolderZip = String.Empty;
        }
      }
      // Federal State
      if (m_card_read.PlayerTracking.HolderFedEntity != m_card.PlayerTracking.HolderFedEntity)
      {
        String _read_federal_state;
        String _federal_state;

        if (String.IsNullOrEmpty(CardData.FedEntitiesString((int)m_card_read.PlayerTracking.HolderFedEntity)))
        {
          _read_federal_state = _audit_none_str;
        }
        else
        {
          _read_federal_state = CardData.FedEntitiesString((int)m_card_read.PlayerTracking.HolderFedEntity);
        }

        if (String.IsNullOrEmpty(CardData.FedEntitiesString((int)m_card.PlayerTracking.HolderFedEntity)))
        {
          _federal_state = _audit_none_str;
        }
        else
        {
          _federal_state = (CardData.FedEntitiesString((int)m_card.PlayerTracking.HolderFedEntity));
        }

        _auditor.SetField(AUDIT_FIELD_ID, AccountFields.GetStateName(), _federal_state, _read_federal_state, "", "");
      }

      // Country Validation
      if (m_card_read.PlayerTracking.HolderAddressValidation != m_card.PlayerTracking.HolderAddressValidation)
      {
        _auditor.SetField(AUDIT_FIELD_ID,
                          Resource.String("STR_FRM_ACCOUNT_USER_EDIT_ADDRESS_VALIDATION"), 
                          CardData.GetHolderAddressValidationString(m_card.PlayerTracking.HolderAddressValidation),
                          CardData.GetHolderAddressValidationString(m_card_read.PlayerTracking.HolderAddressValidation), "", "");
      }

      // Country
      if (m_card_read.PlayerTracking.HolderAddressCountry != m_card.PlayerTracking.HolderAddressCountry)
      {
        String _read_country;
        String _country;

        if (String.IsNullOrEmpty(CardData.CountriesString((int)m_card_read.PlayerTracking.HolderAddressCountry)))
        {
          _read_country = _audit_none_str;
        }
        else
        {
          _read_country = CardData.CountriesString((int)m_card_read.PlayerTracking.HolderAddressCountry);
        }

        if (String.IsNullOrEmpty(CardData.CountriesString((int)m_card.PlayerTracking.HolderAddressCountry)))
        {
          _country = _audit_none_str;
        }
        else
        {
          _country = (CardData.CountriesString((int)m_card.PlayerTracking.HolderAddressCountry));
        }

        _auditor.SetField(AUDIT_FIELD_ID, Resource.String("STR_FRM_ACCOUNT_USER_EDIT_COUNTRY"), _country, _read_country, "", "");
      }

      // External Number
      if (m_card_read.PlayerTracking.HolderExtNumber != m_card.PlayerTracking.HolderExtNumber)
      {
        if (String.IsNullOrEmpty(m_card_read.PlayerTracking.HolderExtNumber))
        {
          m_card_read.PlayerTracking.HolderExtNumber = _audit_none_str;
        }

        if (String.IsNullOrEmpty(m_card.PlayerTracking.HolderExtNumber))
        {
          m_card.PlayerTracking.HolderExtNumber = _audit_none_str;
        }

        _auditor.SetField(AUDIT_FIELD_ID, Resource.String("STR_FRM_ACCOUNT_USER_EDIT_EXT_NUM"), m_card.PlayerTracking.HolderExtNumber, m_card_read.PlayerTracking.HolderExtNumber, "", "");

        if (m_card.PlayerTracking.HolderExtNumber == _audit_none_str)
        {
          m_card.PlayerTracking.HolderExtNumber = String.Empty;
        }
      }


      // COMMENTS:
      if (m_card_read.PlayerTracking.HolderComments != m_card.PlayerTracking.HolderComments)
      {
        if (String.IsNullOrEmpty(m_card_read.PlayerTracking.HolderComments))
        {
          m_card_read.PlayerTracking.HolderComments = _audit_none_str;
        }

        if (String.IsNullOrEmpty(m_card.PlayerTracking.HolderComments))
        {
          m_card.PlayerTracking.HolderComments = _audit_none_str;
        }

        _auditor.SetField(AUDIT_FIELD_ID, Resource.String("STR_UC_PLAYER_TRACK_COMMENTS"), m_card.PlayerTracking.HolderComments.Trim(), m_card_read.PlayerTracking.HolderComments.Trim(), "", "");

        if (m_card.PlayerTracking.HolderComments == _audit_none_str)
        {
          m_card.PlayerTracking.HolderComments = String.Empty;
        }

      }


      // BENEFICIARY:
      // Holder as Beneficiary
      if (m_card_read.PlayerTracking.HolderHasBeneficiary != m_card.PlayerTracking.HolderHasBeneficiary)
      {
        _auditor.SetField(AUDIT_FIELD_ID, Resource.String("STR_FRM_ACCOUNT_USER_EDIT_HOLDER_HAS_BENEF"),
          m_card.PlayerTracking.HolderHasBeneficiary ? Resource.String("STR_FORMAT_GENERAL_BUTTONS_YES") : Resource.String("STR_FORMAT_GENERAL_BUTTONS_NO"),
          m_card_read.PlayerTracking.HolderHasBeneficiary ? Resource.String("STR_FORMAT_GENERAL_BUTTONS_YES") : Resource.String("STR_FORMAT_GENERAL_BUTTONS_NO"),
          "", "");
      }

      //BENEFICIARY
      if (!m_card.PlayerTracking.HolderHasBeneficiary)
      {
        m_card.PlayerTracking.BeneficiaryName1 = String.Empty;
        m_card.PlayerTracking.BeneficiaryName2 = String.Empty;
        m_card.PlayerTracking.BeneficiaryName3 = String.Empty;
        m_card.PlayerTracking.BeneficiaryOccupationId = -1;
        m_card.PlayerTracking.BeneficiaryGender = -1;
        m_card.PlayerTracking.BeneficiaryBirthDate = DateTime.MinValue;
        m_card.PlayerTracking.BeneficiaryId1 = String.Empty;
        m_card.PlayerTracking.BeneficiaryId2 = String.Empty;
        m_card.PlayerTracking.BeneficiaryId3Type = String.Empty;
      }


      // Name
      if (m_card_read.PlayerTracking.BeneficiaryName3 != m_card.PlayerTracking.BeneficiaryName3)
      {
        if (String.IsNullOrEmpty(m_card_read.PlayerTracking.BeneficiaryName3))
        {
          m_card_read.PlayerTracking.BeneficiaryName3 = _audit_none_str;
        }

        if (String.IsNullOrEmpty(m_card.PlayerTracking.BeneficiaryName3))
        {
          m_card.PlayerTracking.BeneficiaryName3 = _audit_none_str;
        }

        _auditor.SetField(AUDIT_FIELD_ID, Resource.String("STR_FRM_ACCOUNT_USER_EDIT_NAME") + _beneficiary_sufix, m_card.PlayerTracking.BeneficiaryName3, m_card_read.PlayerTracking.BeneficiaryName3, "", "");

        if (m_card.PlayerTracking.BeneficiaryName3 == _audit_none_str)
        {
          m_card.PlayerTracking.BeneficiaryName3 = String.Empty;
        }
      }
      // Surname 1
      if (m_card_read.PlayerTracking.BeneficiaryName1 != m_card.PlayerTracking.BeneficiaryName1)
      {
        if (String.IsNullOrEmpty(m_card_read.PlayerTracking.BeneficiaryName1))
        {
          m_card_read.PlayerTracking.BeneficiaryName1 = _audit_none_str;
        }

        if (String.IsNullOrEmpty(m_card.PlayerTracking.BeneficiaryName1))
        {
          m_card.PlayerTracking.BeneficiaryName1 = _audit_none_str;
        }

        _auditor.SetField(AUDIT_FIELD_ID, Resource.String("STR_FRM_PLAYER_EDIT_LAST_NAME1") + _beneficiary_sufix, m_card.PlayerTracking.BeneficiaryName1, m_card_read.PlayerTracking.BeneficiaryName1, "", "");

        if (m_card.PlayerTracking.BeneficiaryName1 == _audit_none_str)
        {
          m_card.PlayerTracking.BeneficiaryName1 = String.Empty;
        }
      }
      // Surname 2
      if (m_card_read.PlayerTracking.BeneficiaryName2 != m_card.PlayerTracking.BeneficiaryName2)
      {
        if (String.IsNullOrEmpty(m_card_read.PlayerTracking.BeneficiaryName2))
        {
          m_card_read.PlayerTracking.BeneficiaryName2 = _audit_none_str;
        }

        if (String.IsNullOrEmpty(m_card.PlayerTracking.BeneficiaryName2))
        {
          m_card.PlayerTracking.BeneficiaryName2 = _audit_none_str;
        }

        _auditor.SetField(AUDIT_FIELD_ID, Resource.String("STR_FRM_PLAYER_EDIT_LAST_NAME2") + _beneficiary_sufix, m_card.PlayerTracking.BeneficiaryName2, m_card_read.PlayerTracking.BeneficiaryName2, "", "");

        if (m_card.PlayerTracking.BeneficiaryName2 == _audit_none_str)
        {
          m_card.PlayerTracking.BeneficiaryName2 = String.Empty;
        }
      }
      // Gender
      if (m_card_read.PlayerTracking.BeneficiaryGender != m_card.PlayerTracking.BeneficiaryGender)
      {
        String _read_holder_gender;
        String _holder_gender;

        if (String.IsNullOrEmpty(CardData.GenderString(m_card_read.PlayerTracking.BeneficiaryGender)))
        {
          _read_holder_gender = _audit_none_str;
        }
        else
        {
          _read_holder_gender = CardData.GenderString(m_card_read.PlayerTracking.BeneficiaryGender);
        }

        if (String.IsNullOrEmpty(CardData.GenderString(m_card.PlayerTracking.BeneficiaryGender)))
        {
          _holder_gender = _audit_none_str;
        }
        else
        {
          _holder_gender = CardData.GenderString(m_card.PlayerTracking.BeneficiaryGender);
        }

        _auditor.SetField(AUDIT_FIELD_ID, Resource.String("STR_UC_PLAYER_TRACK_GENDER") + _beneficiary_sufix, _holder_gender, _read_holder_gender, "", "");
      }
      // Occupation Id
      if (m_card_read.PlayerTracking.BeneficiaryOccupationId != m_card.PlayerTracking.BeneficiaryOccupationId)
      {
        String _read_occupation;
        String _occupation;

        if (String.IsNullOrEmpty(CardData.OccupationsDescriptionString((int)m_card_read.PlayerTracking.BeneficiaryOccupationId)))
        {
          _read_occupation = _audit_none_str;
        }
        else
        {
          _read_occupation = CardData.OccupationsDescriptionString((int)m_card_read.PlayerTracking.BeneficiaryOccupationId);
        }

        if (String.IsNullOrEmpty(CardData.OccupationsDescriptionString((int)m_card.PlayerTracking.BeneficiaryOccupationId)))
        {
          _occupation = _audit_none_str;
        }
        else
        {
          _occupation = CardData.OccupationsDescriptionString((int)m_card.PlayerTracking.BeneficiaryOccupationId);
        }

        _auditor.SetField(AUDIT_FIELD_ID, Resource.String("STR_FRM_PLAYER_EDIT_OCCUPATION") + _beneficiary_sufix, _occupation, _read_occupation, "", "");
      }
      //Birth Date
      if (m_card_read.PlayerTracking.BeneficiaryBirthDate != m_card.PlayerTracking.BeneficiaryBirthDate)
      {
        if (m_card.PlayerTracking.BeneficiaryBirthDate != DateTime.MinValue)
        {
          _date = m_card.PlayerTracking.BeneficiaryBirthDate.ToShortDateString();
        }
        else
        {
          _date = _audit_none_str;
        }
        if (m_card_read.PlayerTracking.BeneficiaryBirthDate != DateTime.MinValue)
        {
          _date_read = m_card_read.PlayerTracking.BeneficiaryBirthDate.ToShortDateString();
        }
        else
        {
          _date_read = _audit_none_str;
        }
        _auditor.SetField(AUDIT_FIELD_ID, Resource.String("STR_FRM_PLAYER_EDIT_BIRTH_DATE") + _beneficiary_sufix, _date, _date_read, "", "");
      }
      // Scanned Doc. Type
      if (m_docs_beneficiary_changed)
      {
        String _read_holder_id3_type;
        String _holder_id3_type;

        if (m_card_read.PlayerTracking.BeneficiaryId3Type == String.Empty)
        {
          _read_holder_id3_type = _audit_none_str;
        }
        else
        {
          _read_holder_id3_type = IdentificationTypes.DocIdTypeStringList(m_card_read.PlayerTracking.BeneficiaryId3Type);
        }

        if (m_card.PlayerTracking.BeneficiaryId3Type == String.Empty)
        {
          _holder_id3_type = _audit_none_str;
        }
        else
        {
          _holder_id3_type = IdentificationTypes.DocIdTypeStringList(m_card.PlayerTracking.BeneficiaryId3Type);
        }

        _auditor.SetField(AUDIT_FIELD_ID, Resource.String("STR_FRM_PLAYER_EDIT_SCAN_DOCUMENT") + _beneficiary_sufix, _holder_id3_type, _read_holder_id3_type, "", "");
      }
      // RFC
      if (m_card_read.PlayerTracking.BeneficiaryId1 != m_card.PlayerTracking.BeneficiaryId1)
      {
        if (String.IsNullOrEmpty(m_card_read.PlayerTracking.BeneficiaryId1))
        {
          m_card_read.PlayerTracking.BeneficiaryId1 = _audit_none_str;
        }

        if (String.IsNullOrEmpty(m_card.PlayerTracking.BeneficiaryId1))
        {
          m_card.PlayerTracking.BeneficiaryId1 = _audit_none_str;
        }

        _auditor.SetField(AUDIT_FIELD_ID, IdentificationTypes.DocIdTypeString(ACCOUNT_HOLDER_ID_TYPE.RFC) + _beneficiary_sufix, m_card.PlayerTracking.BeneficiaryId1, m_card_read.PlayerTracking.BeneficiaryId1, "", "");

        if (m_card.PlayerTracking.BeneficiaryId1 == _audit_none_str)
        {
          m_card.PlayerTracking.BeneficiaryId1 = String.Empty;
        }
      }
      // CURP
      if (m_card_read.PlayerTracking.BeneficiaryId2 != m_card.PlayerTracking.BeneficiaryId2)
      {
        if (String.IsNullOrEmpty(m_card_read.PlayerTracking.BeneficiaryId2))
        {
          m_card_read.PlayerTracking.BeneficiaryId2 = _audit_none_str;
        }

        if (String.IsNullOrEmpty(m_card.PlayerTracking.BeneficiaryId2))
        {
          m_card.PlayerTracking.BeneficiaryId2 = _audit_none_str;
        }

        _auditor.SetField(AUDIT_FIELD_ID, IdentificationTypes.DocIdTypeString(ACCOUNT_HOLDER_ID_TYPE.CURP) + _beneficiary_sufix, m_card.PlayerTracking.BeneficiaryId2, m_card_read.PlayerTracking.BeneficiaryId2, "", "");

        if (m_card.PlayerTracking.BeneficiaryId2 == _audit_none_str)
        {
          m_card.PlayerTracking.BeneficiaryId2 = String.Empty;
        }
      }


      if (_auditor.NumFields > 1) _auditor.Notify(ENUM_GUI.CASHIER, Cashier.UserId, Cashier.UserName, Cashier.TerminalName);

    }

    /// <summary>
    /// Save screen content to the card
    /// </summary>
    private Boolean SaveCard()
    {
      Boolean _ok;
      Boolean _card_paid;
      Int64 _account_id;
      String _duplicate_msg;
      Boolean _allow_duplicate;
      String _error_str;
      DialogResult _question_dialog;

      if (!MustShowComments)
      {
        GetScreenData();
      }

      _ok = false;

      if (!CardData.CheckDuplicateAccounts(m_card, out _duplicate_msg, out _allow_duplicate))
      {
        if (_allow_duplicate)
        {
          _question_dialog = frm_message.Show(_duplicate_msg,
                                 Resource.String("STR_APP_GEN_MSG_WARNING"),
                                 MessageBoxButtons.YesNo,
                                 MessageBoxIcon.Warning,
                                 this.ParentForm);

          if (_question_dialog == DialogResult.Cancel || !ProfilePermissions.CheckPermissionList(ProfilePermissions.CashierFormFuncionality.DuplicateAccount,
                                                                                                 ProfilePermissions.TypeOperation.RequestPasswd,
                                                                                                 this, 0,
                                                                                                 out _error_str))
          {

            return false;
          }
        }
        else
        {
          frm_message.Show(_duplicate_msg,
               Resource.String("STR_APP_GEN_MSG_WARNING"),
               MessageBoxButtons.OK,
               MessageBoxIcon.Warning,
               this.ParentForm);

          return false;
        }
      }

      using (DB_TRX _db_trx = new DB_TRX())
      {
        if (m_card.AccountId == 0)   //Creating New Account
        {
          // Creates CardData into DB
          if (CashierBusinessLogic.DB_CreateCard(m_card.TrackData, out _account_id, _db_trx.SqlTransaction))
          {
            if (CashierBusinessLogic.DB_UpdateCardPayAfterCreated(_account_id, ACCOUNT_USER_TYPE.PERSONAL, out _card_paid, _db_trx.SqlTransaction))
            {
              m_card.CardPaid = _card_paid;
            }

            // Set the new account id.
            m_card.AccountId = _account_id;
            m_card_read.AccountId = _account_id;
          }
        }

        if (m_card.AccountId != 0)
        {
          _ok = CashierBusinessLogic.DB_UpdateCardholder(m_card, m_card_read,
                                                         (m_docs_holder_changed || m_docs_beneficiary_changed),
                                                         m_print_information,
                                                         _db_trx.SqlTransaction);
        }


        if (_ok)
        {
          if (m_img_photo.Tag != null && (int)m_img_photo.Tag == 1) // Image changed!
          {
            _ok = IDCardScannerHelper.SaveAccountPhoto(m_card.AccountId, m_img_photo.Image, _db_trx.SqlTransaction);
          }
        }

        if (_ok)
        {
          _db_trx.Commit();
          m_docs_beneficiary_changed = false;
          m_docs_holder_changed = false;
        }
      }

      if (!_ok)
      {
        frm_message.Show(Resource.String("STR_APP_GEN_ERROR_DATABASE_ERROR_WRITING"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this.ParentForm);
      }

      return _ok;
    }

    /// <summary>
    /// Load combo box items
    /// </summary>
    private void LoadCombos()
    {
      DataTable _dt_beneficiary_gender;

      // Gender combo
      LoadComboNLS("STR_FRM_PLAYER_TRACKING_CB_GENDER_MALE", this.cb_gender);
      LoadComboNLS("STR_FRM_PLAYER_TRACKING_CB_GENDER_FEMALE", this.cb_gender);

      // Beneficiary Gender combo
      _dt_beneficiary_gender = new DataTable();

      _dt_beneficiary_gender.Columns.Add("GenderId", Type.GetType("System.Int32")).AllowDBNull = false;
      _dt_beneficiary_gender.Columns.Add("GenderName", Type.GetType("System.String")).AllowDBNull = false;

      _dt_beneficiary_gender.Rows.Add(1, Resource.String("STR_FRM_PLAYER_TRACKING_CB_GENDER_MALE"));     // Male
      _dt_beneficiary_gender.Rows.Add(2, Resource.String("STR_FRM_PLAYER_TRACKING_CB_GENDER_FEMALE"));   // Female

      this.cb_beneficiary_gender.DataSource = _dt_beneficiary_gender;
      this.cb_beneficiary_gender.ValueMember = "GenderId";
      this.cb_beneficiary_gender.DisplayMember = "GenderName";

      // Marital combo
      LoadComboNLS("STR_FRM_PLAYER_TRACKING_CB_MARITAL_SINGLE", this.cb_marital);
      LoadComboNLS("STR_FRM_PLAYER_TRACKING_CB_MARITAL_MARRIED", this.cb_marital);
      LoadComboNLS("STR_FRM_PLAYER_TRACKING_CB_MARITAL_DIVORCED", this.cb_marital);
      LoadComboNLS("STR_FRM_PLAYER_TRACKING_CB_MARITAL_WIDOWED", this.cb_marital);
      LoadComboNLS("STR_FRM_PLAYER_TRACKING_CB_MARITAL_UNMARRIED", this.cb_marital);
      LoadComboNLS("STR_FRM_PLAYER_TRACKING_CB_MARITAL_OTHER", this.cb_marital);
      LoadComboNLS("STR_FRM_PLAYER_TRACKING_CB_MARITAL_UNSPECIFIED", this.cb_marital); // Unspecified

      // Holder document type combo
      //FBA 12-FEB-2014
      LoadDocumentTypesCombo();
      LoadCountryCombo(ref cb_country);
      LoadCountryCombo(ref cb_country_new);
      LoadBirthCountriesCombo();
      LoadNationalitiesCombo();
      LoadOccupationsCombos();
    }

    private void LoadNationalitiesCombo()
    {
      DataTable _dt_nationalities;
      DataRow _dr_empty_row;

      _dt_nationalities = CardData.GetNationalitiesList();

      // Non-mandatory field.  An empty row is needed.
      _dr_empty_row = _dt_nationalities.NewRow();
      _dr_empty_row["CO_COUNTRY_ID"] = -1;
      _dr_empty_row["CO_ADJECTIVE"] = "";

      _dt_nationalities.Rows.InsertAt(_dr_empty_row, 0);

      cb_nationality.DataSource = _dt_nationalities;
      cb_nationality.ValueMember = "CO_COUNTRY_ID";
      cb_nationality.DisplayMember = "CO_ADJECTIVE";
      cb_nationality.AllowUnlistedValues = false;
      cb_nationality.FormatValidator = new CharacterTextBox();
    }

    private void LoadBirthCountriesCombo()
    {
      DataTable _dt_countries;
      DataRow _dr_empty_row;

      _dt_countries = CardData.GetCountriesList();

      // Non-mandatory field.  An empty row is needed.
      _dr_empty_row = _dt_countries.NewRow();
      _dr_empty_row["CO_COUNTRY_ID"] = -1;
      _dr_empty_row["CO_NAME"] = "";

      _dt_countries.Rows.InsertAt(_dr_empty_row, 0);

      cb_birth_country.DataSource = _dt_countries;
      cb_birth_country.ValueMember = "CO_COUNTRY_ID";
      cb_birth_country.DisplayMember = "CO_NAME";
      cb_birth_country.AllowUnlistedValues = false;
      cb_birth_country.FormatValidator = new CharacterTextBox();
    }

    private void LoadOccupationsCombos()
    {
      DataTable _dt_occupations;
      DataTable _dt_beneficiary_occupations;
      DataRow _dr_empty_row;
      DataRow _dr_beneficiary_empty_row;

      _dt_occupations = CardData.GetOccupationsList();
      _dt_beneficiary_occupations = CardData.GetOccupationsList();

      // Non-mandatory field.  An empty row is needed.
      _dr_empty_row = _dt_occupations.NewRow();
      _dr_empty_row["OC_ID"] = -1;
      _dr_empty_row["OC_DESCRIPTION"] = "";
      _dr_beneficiary_empty_row = _dt_beneficiary_occupations.NewRow();
      _dr_beneficiary_empty_row["OC_ID"] = -1;
      _dr_beneficiary_empty_row["OC_DESCRIPTION"] = "";

      _dt_occupations.Rows.InsertAt(_dr_empty_row, 0);
      _dt_beneficiary_occupations.Rows.InsertAt(_dr_beneficiary_empty_row, 0);

      // Holder
      cb_occupation.DataSource = _dt_occupations;
      cb_occupation.ValueMember = "OC_ID";
      cb_occupation.DisplayMember = "OC_DESCRIPTION";
      cb_occupation.AllowUnlistedValues = false;
      cb_occupation.FormatValidator = new CharacterTextBox();
      // Beneficiary
      cb_beneficiary_occupation.DataSource = _dt_beneficiary_occupations;
      cb_beneficiary_occupation.ValueMember = "OC_ID";
      cb_beneficiary_occupation.DisplayMember = "OC_DESCRIPTION";
      cb_beneficiary_occupation.AllowUnlistedValues = false;
      cb_beneficiary_occupation.FormatValidator = new CharacterTextBox();
    }

    private void LoadCountryCombo(ref uc_round_auto_combobox Combobox)
    {
      DataTable _dt_countries;
      DataRow _dr_empty_row;

      _dt_countries = CardData.GetCountriesList();
      // Non-mandatory field.  An empty row is needed.
      _dr_empty_row = _dt_countries.NewRow();
      _dr_empty_row["CO_COUNTRY_ID"] = -1;
      _dr_empty_row["CO_NAME"] = "";

      _dt_countries.Rows.InsertAt(_dr_empty_row, 0);

      m_combo_charge_dt = true;

      Combobox.DataSource = _dt_countries;
      Combobox.ValueMember = "CO_COUNTRY_ID";
      Combobox.DisplayMember = "CO_NAME";
      Combobox.AllowUnlistedValues = false;
      Combobox.FormatValidator = new CharacterTextBox();

      m_combo_charge_dt = false;
    }

    private void LoadFedEntitiesCombo()
    {
      DataTable _dt_fed_states;
      DataRow _dr_empty_row;
      String _name;
      String _adj;
      String _iso2;

      Countries.GetCountryInfo(Convert.ToInt32(cb_country.SelectedValue), out _name, out _adj, out _iso2);

      //_dt_fed_states = CardData.GetFedStatesList();
      States.GetStates(out _dt_fed_states, _iso2);

      // Non-mandatory field.  An empty row is needed.
      _dr_empty_row = _dt_fed_states.NewRow();
      _dr_empty_row["FS_STATE_ID"] = -1;
      _dr_empty_row["FS_NAME"] = "";

      _dt_fed_states.Rows.InsertAt(_dr_empty_row, 0);

      cb_fed_entity.DataSource = _dt_fed_states;
      cb_fed_entity.ValueMember = "FS_STATE_ID";
      cb_fed_entity.DisplayMember = "FS_NAME";
      cb_fed_entity.AllowUnlistedValues = false;
      cb_fed_entity.FormatValidator = new CharacterTextBox();
    }

    private void LoadDocumentTypesCombo()
    {
      DataTable _dt_doc_types;

      _dt_doc_types = IdentificationTypes.GetIdentificationTypes(true);
      cb_document_type.DataSource = _dt_doc_types;
      cb_document_type.ValueMember = "IDT_ID";
      cb_document_type.DisplayMember = "IDT_NAME";
    }

    /// <summary>
    /// Load combo box items from the NLS
    /// </summary>
    /// <param name="Key"></param>
    /// <param name="Combo"></param>
    private void LoadComboNLS(string Key, WSI.Cashier.Controls.uc_round_combobox Combo)
    {
      String _str_aux;
      _str_aux = Resource.String(Key);
      Combo.Items.Add(_str_aux);
    }

    private void GetScreenData()
    {
      try
      {
        if (!uc_dt_birth.IsEmpty)
        {
          m_card.PlayerTracking.HolderBirthDate = uc_dt_birth.DateValue;
          m_card.PlayerTracking.HolderBirthDateText = m_card.PlayerTracking.HolderBirthDate.ToShortDateString();
        }
        m_card.PlayerTracking.HolderName3 = txt_name.Text.Trim();
        m_card.PlayerTracking.HolderName4 = txt_name4.Text.Trim();
        m_card.PlayerTracking.HolderName1 = txt_last_name1.Text.Trim();
        m_card.PlayerTracking.HolderName2 = txt_last_name2.Text.Trim();
        m_card.PlayerTracking.HolderName = lbl_full_name.Text;

        m_card.PlayerTracking.HolderId = txt_document.Text.Trim();
        m_card.PlayerTracking.HolderIdType = (ACCOUNT_HOLDER_ID_TYPE)(cb_document_type.SelectedValue);

        m_card.PlayerTracking.HolderId1 = this.txt_RFC.Text;
        m_card.PlayerTracking.HolderId1Type = ACCOUNT_HOLDER_ID_TYPE.RFC;

        m_card.PlayerTracking.HolderId2 = this.txt_CURP.Text;
        m_card.PlayerTracking.HolderId2Type = ACCOUNT_HOLDER_ID_TYPE.CURP;

        if (!chk_address_mode.Checked)
        {
          ReplicateAddress();
        }
        FillStructAddress(ref m_address_modified);

        m_card.PlayerTracking.HolderAddress01 = txt_address_01.Text;
        m_card.PlayerTracking.HolderAddress02 = txt_address_02.Text;
        m_card.PlayerTracking.HolderAddress03 = txt_address_03.Text;
        m_card.PlayerTracking.HolderCity = txt_city.Text;
        m_card.PlayerTracking.HolderZip = txt_zip.Text;
        m_card.PlayerTracking.HolderPhone01 = txt_phone_01.Text;
        m_card.PlayerTracking.HolderPhone02 = txt_phone_02.Text;
        m_card.PlayerTracking.HolderEmail01 = txt_email_01.Text;
        m_card.PlayerTracking.HolderEmail02 = txt_email_02.Text;
        m_card.PlayerTracking.HolderTwitter = txt_twitter.Text;
        m_card.PlayerTracking.HolderComments = txt_comments.Text;

        m_card.PlayerTracking.HolderMaritalStatus = cb_marital.SelectedIndex + 1;
        m_card.PlayerTracking.HolderGender = cb_gender.SelectedIndex + 1;

        // ICS 13-FEB-2013: Wedding date
        m_card.PlayerTracking.HolderWeddingDate = DateTime.MinValue;

        if (!uc_dt_wedding.IsEmpty)
        {
          m_card.PlayerTracking.HolderWeddingDate = uc_dt_wedding.DateValue;
        }

        m_card.PlayerTracking.HolderOccupationId = Convert.ToInt32(cb_occupation.SelectedValue);
        m_card.PlayerTracking.HolderNationality = Convert.ToInt64(cb_nationality.SelectedValue);
        m_card.PlayerTracking.HolderBirthCountry = Convert.ToInt64(cb_birth_country.SelectedValue);
        m_card.PlayerTracking.HolderExtNumber = txt_ext_num.Text;
        m_card.PlayerTracking.HolderFedEntity = Convert.ToInt32(cb_fed_entity.SelectedValue);
        m_card.PlayerTracking.HolderAddressCountry = Convert.ToInt32(cb_country.SelectedValue);
        m_card.PlayerTracking.HolderAddressValidation = CardData.GetHolderAddressValidationValue(m_address_original, m_address_modified, chk_address_mode.Checked,
                                                                                                 m_card.PlayerTracking.HolderAddressValidation, m_card.PlayerTracking.HolderAddressCountry);
        m_card.PlayerTracking.ShowCommentsOnCashier = chk_show_comments.Checked;

        //Beneficiary
        m_card.PlayerTracking.HolderHasBeneficiary = chk_holder_has_beneficiary.Checked;
        m_card.PlayerTracking.BeneficiaryBirthDate = DateTime.MinValue;

        if (m_card.PlayerTracking.HolderHasBeneficiary)
        {
          m_card.PlayerTracking.BeneficiaryName3 = txt_beneficiary_name.Text.Trim();
          m_card.PlayerTracking.BeneficiaryName1 = txt_beneficiary_last_name1.Text.Trim();
          m_card.PlayerTracking.BeneficiaryName2 = txt_beneficiary_last_name2.Text.Trim();
          m_card.PlayerTracking.BeneficiaryName = lbl_beneficiary_full_name.Text;
          m_card.PlayerTracking.BeneficiaryOccupationId = Convert.ToInt32(cb_beneficiary_occupation.SelectedValue);
          m_card.PlayerTracking.BeneficiaryGender = Convert.ToInt32(cb_beneficiary_gender.SelectedValue);
          m_card.PlayerTracking.BeneficiaryId1 = txt_beneficiary_RFC.Text.Trim();
          m_card.PlayerTracking.BeneficiaryId2 = txt_beneficiary_CURP.Text.Trim();

          if (!uc_dt_ben_birth.IsEmpty)
          {
            m_card.PlayerTracking.BeneficiaryBirthDate = uc_dt_ben_birth.DateValue;
          }
        }
        else
        {
          m_card.PlayerTracking.BeneficiaryName1 = String.Empty;
          m_card.PlayerTracking.BeneficiaryName2 = String.Empty;
          m_card.PlayerTracking.BeneficiaryName3 = String.Empty;
          m_card.PlayerTracking.BeneficiaryOccupationId = -1;
          m_card.PlayerTracking.BeneficiaryGender = -1;
          m_card.PlayerTracking.BeneficiaryBirthDate = DateTime.MinValue;
          m_card.PlayerTracking.BeneficiaryId1 = String.Empty;
          m_card.PlayerTracking.BeneficiaryId2 = String.Empty;
          m_card.PlayerTracking.BeneficiaryId3Type = String.Empty;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
#if DEBUG
        frm_message.Show(_ex.Message, "DEBUG MODE", MessageBoxButtons.OK, MessageBoxIcon.Warning, this.ParentForm);
#endif
      }

    }

    //------------------------------------------------------------------------------
    // PURPOSE: Checks for changes in the form
    // 
    //  PARAMS:
    //      - INPUT:
    //              CardData.PlayerTrackingData InitialValue
    //              CardData.PlayerTrackingData FinalValue
    //       
    //      - OUTPUT:
    //
    // RETURNS:
    //    True there is changes. False otherwise.
    // 
    //   NOTES:
    private Boolean HasChanges(CardData.PlayerTrackingData InitialValue, CardData.PlayerTrackingData FinalValue)
    {
      // Must Show Comments
      if (MustShowComments)
      {
        if (InitialValue.HolderComments != FinalValue.HolderComments ||
            InitialValue.ShowCommentsOnCashier != FinalValue.ShowCommentsOnCashier)
        {
          return true;
        }
        return false;
      }

      // Main Tab
      if (InitialValue.HolderName1 != FinalValue.HolderName1 ||
          InitialValue.HolderName2 != FinalValue.HolderName2 ||
          InitialValue.HolderName3 != FinalValue.HolderName3 ||
          InitialValue.HolderName4 != FinalValue.HolderName4 ||
          InitialValue.HolderId1 != FinalValue.HolderId1 ||
          InitialValue.HolderId2 != FinalValue.HolderId2 ||
          InitialValue.HolderGender != FinalValue.HolderGender ||
          InitialValue.HolderNationality != FinalValue.HolderNationality ||
          InitialValue.HolderBirthCountry != FinalValue.HolderBirthCountry ||
          InitialValue.HolderOccupationId != FinalValue.HolderOccupationId ||
          InitialValue.HolderMaritalStatus != FinalValue.HolderMaritalStatus ||
        // HolderId and HolderDocumentType values depend of HolderId1 and HolderId2
          m_initial_values["HolderId"] != txt_document.Text ||
          m_initial_values["HolderDocumentType"] != cb_document_type.SelectedValue.ToString() ||
        // Dates haven't a value if don't "CheckData" previously.
          m_initial_values["HolderBirthDate"] != uc_dt_birth.DateValue.ToShortDateString() ||
          m_initial_values["HolderWeddingDate"] != uc_dt_wedding.DateValue.ToShortDateString())
      {
        return true;
      }

      // Contact Tab
      if (InitialValue.HolderPhone01 != FinalValue.HolderPhone01 ||
          InitialValue.HolderPhone02 != FinalValue.HolderPhone02 ||
          InitialValue.HolderEmail01 != FinalValue.HolderEmail01 ||
          InitialValue.HolderEmail02 != FinalValue.HolderEmail02 ||
          InitialValue.HolderTwitter != FinalValue.HolderTwitter)
      {
        return true;
      }

      // Address Tab
      if (InitialValue.HolderAddress01 != FinalValue.HolderAddress01 ||
         InitialValue.HolderAddress02 != FinalValue.HolderAddress02 ||
         InitialValue.HolderAddress03 != FinalValue.HolderAddress03 ||
         InitialValue.HolderExtNumber != FinalValue.HolderExtNumber ||
         InitialValue.HolderCity != FinalValue.HolderCity ||
         InitialValue.HolderZip != FinalValue.HolderZip ||
         InitialValue.HolderFedEntity != FinalValue.HolderFedEntity ||
         InitialValue.HolderAddressCountry != FinalValue.HolderAddressCountry ||
         InitialValue.HolderAddressValidation != FinalValue.HolderAddressValidation)
      {
        return true;
      }

      // Beneficiary Tab
      if (InitialValue.HolderHasBeneficiary != FinalValue.HolderHasBeneficiary ||
          InitialValue.BeneficiaryName1 != FinalValue.BeneficiaryName1 ||
          InitialValue.BeneficiaryName2 != FinalValue.BeneficiaryName2 ||
          InitialValue.BeneficiaryName3 != FinalValue.BeneficiaryName3 ||
          InitialValue.BeneficiaryId1 != FinalValue.BeneficiaryId1 ||
          InitialValue.BeneficiaryId2 != FinalValue.BeneficiaryId2 ||
          InitialValue.BeneficiaryGender != FinalValue.BeneficiaryGender ||
          InitialValue.BeneficiaryOccupationId != FinalValue.BeneficiaryOccupationId ||
        // Dates haven't a value if don't "CheckData" previously.
          m_initial_values["BeneficiaryBirthDate"] != uc_dt_ben_birth.DateValue.ToShortDateString())
      {
        return true;
      }

      // Comments Tab
      if (InitialValue.HolderComments != FinalValue.HolderComments ||
          InitialValue.ShowCommentsOnCashier != FinalValue.ShowCommentsOnCashier)
      {
        return true;
      }

      // Scanned Documents
      if (m_docs_holder_changed || m_docs_beneficiary_changed)
      {
        return true;
      }

      if (m_img_photo.Tag != null && (int)m_img_photo.Tag == 1)
      {
        // Photo changed;
        return true;
      }
      return false;
    } // Has Changes

    //------------------------------------------------------------------------------
    // PURPOSE: Check data in formulary
    // 
    //  PARAMS:
    //      - INPUT:
    //        focus_it : If true, focus incorrect text control.
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //    True if all the information in text controls is correct.
    //    False if not.
    // 
    //   NOTES:
    private Boolean CheckData(Boolean FocusIt,Boolean fromOk = false)
    {
      Int32 _selection_start;
      Int32 _selection_length;
      ACCOUNT_HOLDER_ID_TYPE _holder_doc_id_type;
      WSI.Cashier.Controls.uc_round_textbox _txt_doc_id;

      lbl_msg_blink.Visible = false;
      lbl_msg_blink.Text = "";
      // RCI 19-AUG-2010: Disable timer. Check data only on Ok button press.
      //timer1.Enabled = true;

      _selection_start = 0;
      _selection_length = 0;
      _txt_doc_id = txt_document;
      _holder_doc_id_type = ACCOUNT_HOLDER_ID_TYPE.UNKNOWN;


      try
      {
        // Check Account Mandatory Fields
        if (!CheckRequiredFieldsForAccount(PLAYER_DATA_TO_CHECK.ACCOUNT))
        {
          return false;
        }

        // Check Beneficiary Mandatory Fields
        if (chk_holder_has_beneficiary.Checked)
        {
          if (!CheckRequiredFieldsForBeneficiary(PLAYER_DATA_TO_CHECK.BENEFICIARY))
          {
            return false;
          }
        }

        // Validate format of the fields
        if (!ValidateFormat(FocusIt))
        {
          return false;
        }

        _selection_start = txt_document.SelectionStart;
        _selection_length = txt_document.SelectionLength;
        txt_document.Text = txt_document.Text.ToUpperInvariant();
        txt_document.Select(_selection_start, _selection_length);

        //LEM 15-07-2014: Don't allow empty txt_document when txt_RFC is generic(WIG-1064).
        if (Common.ValidateFormat.IsGenericRFC(txt_RFC.Text.Trim()) && String.IsNullOrEmpty(txt_document.Text.Trim()))
        {
          lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC", lbl_document.Text);

          return false;
        }

        if (!String.IsNullOrEmpty(txt_document.Text))
        {
          _txt_doc_id = txt_document;
          _holder_doc_id_type = (ACCOUNT_HOLDER_ID_TYPE)(cb_document_type.SelectedValue);
        }
        else if (!String.IsNullOrEmpty(txt_RFC.Text))
        {
          _txt_doc_id = txt_RFC;
          _holder_doc_id_type = ACCOUNT_HOLDER_ID_TYPE.RFC;
        }
        else if (!String.IsNullOrEmpty(txt_CURP.Text))
        {
          _txt_doc_id = txt_CURP;
          _holder_doc_id_type = ACCOUNT_HOLDER_ID_TYPE.CURP;
        }

        if (CardData.CheckIdAlreadyExists(_txt_doc_id.Text, m_card.AccountId, _holder_doc_id_type))
        {
          lbl_msg_blink.Text = Resource.String("STR_HOLDER_ID_ALREADY_EXISTS");
          if (FocusIt)
          {
            tab_player_edit.SelectedIndex = 0;
            _txt_doc_id.Focus();
          }

          return false;
        }


        if (m_pin_request_enabled || m_kiosk_auto_service != 0)
        {
          if ((lbl_full_name.Text != "") && (m_card.PlayerTracking.Pin == ""))
          {
            tab_player_edit.SelectedTab = pin;
            txt_pin.Focus();
            frm_message.Show(Resource.String("STR_FRM_ACCOUNT_USER_EDIT_NO_PIN"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this.ParentForm);

            return false;
          }
        }

        if (this.IsFromReception)
        {
          if (m_img_photo.Image == null)
          {
            lbl_msg_blink.Text = Resource.String("STR_FRM_CUSTOMER_RECEPTION_REG_ENTRANCE_PHOTO");
            lbl_no_photo.Visible = true;
            return false;
          }
        }

        if (this.m_antimoneylaundering_enabled)
        {
          // Check beneficiary Mandatory Fields
          if (!CheckRequiredFieldsForMLMode())
          {
            String _message;
            DialogResult _dlg_rc;

            if (this.m_print_information || fromOk)
            {
              // (Check to print) Some fields required for the money laundering control are blank.\r\n\r\nDo you want to continue?
              _message = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_PRINT_DATA_AML", Accounts.getAMLName());
            }
            else
            {
              // (Check to exit) Some fields required for the money laundering control are blank.\r\n\r\nDo you want to exit and save changes anyway? 
              _message = Resource.String("STR_ANTIMONEYLAUNDERING_MANDATORY_FIELDS", Accounts.getAMLName());
            }

            _message = _message.Replace("\\r\\n", "\r\n");
            _dlg_rc = frm_message.Show(_message, Resource.String("STR_ANTIMONEYLAUNDERING_MANDATORY_FIELDS_CAPTION", Accounts.getAMLName()),
                                       MessageBoxButtons.YesNo, Images.CashierImage.Warning, this);

            if (_dlg_rc != DialogResult.OK)
            {
              return false;
            }
          }
        }

        return true;
      }
      finally
      {
        if (lbl_msg_blink.Text != "")
        {
          lbl_msg_blink.Visible = true;
        }
      }
    } //CheckData

    private Boolean CheckRequiredFieldsForAccount(PLAYER_DATA_TO_CHECK _type_to_check)
    {
      RequiredData _controls_to_check;

      _controls_to_check = new RequiredData(_type_to_check);

      _controls_to_check.Add(ENUM_CARDDATA_FIELD.NAME3, txt_name.Text);
      if (_type_to_check != PLAYER_DATA_TO_CHECK.ANTIMONEYLAUNDERING_ACCOUNT)
      {
        _controls_to_check.Add(ENUM_CARDDATA_FIELD.NAME4, txt_name4.Text);
      }
      _controls_to_check.Add(ENUM_CARDDATA_FIELD.NAME1, txt_last_name1.Text);
      _controls_to_check.Add(ENUM_CARDDATA_FIELD.NAME2, txt_last_name2.Text);

      _controls_to_check.Add(ENUM_CARDDATA_FIELD.ID1, txt_RFC.Text);
      _controls_to_check.Add(ENUM_CARDDATA_FIELD.ID2, txt_CURP.Text);
      _controls_to_check.Add(ENUM_CARDDATA_FIELD.ID, txt_document.Text);
      _controls_to_check.Add(ENUM_CARDDATA_FIELD.DOC_TYPE, (Int32)((ACCOUNT_HOLDER_ID_TYPE)(cb_document_type.SelectedValue)));

      _controls_to_check.Add(ENUM_CARDDATA_FIELD.BIRTHDATE, !uc_dt_birth.IsEmpty);
      _controls_to_check.Add(ENUM_CARDDATA_FIELD.WEDDING_DATE, !uc_dt_wedding.IsEmpty);

      _controls_to_check.Add(ENUM_CARDDATA_FIELD.GENDER, cb_gender.Text);
      _controls_to_check.Add(ENUM_CARDDATA_FIELD.NATIONALITY, cb_nationality.SelectedValue);
      _controls_to_check.Add(ENUM_CARDDATA_FIELD.BIRTH_COUNTRY, cb_birth_country.SelectedValue);
      _controls_to_check.Add(ENUM_CARDDATA_FIELD.DOC_SCAN, m_card.PlayerTracking.HolderId3Type);
      _controls_to_check.Add(ENUM_CARDDATA_FIELD.OCCUPATION, cb_occupation.SelectedValue);
      _controls_to_check.Add(ENUM_CARDDATA_FIELD.MARITAL_STATUS, cb_marital.Text);

      _controls_to_check.Add(ENUM_CARDDATA_FIELD.PHONE1, txt_phone_01.Text);
      _controls_to_check.Add(ENUM_CARDDATA_FIELD.PHONE2, txt_phone_02.Text);
      _controls_to_check.Add(ENUM_CARDDATA_FIELD.EMAIL1, txt_email_01.Text);
      _controls_to_check.Add(ENUM_CARDDATA_FIELD.EMAIL2, txt_email_02.Text);
      _controls_to_check.Add(ENUM_CARDDATA_FIELD.TWITTER, txt_twitter.Text);

      if (chk_address_mode.Checked)
      {
        _controls_to_check.Add(ENUM_CARDDATA_FIELD.STREET, txt_address_01.Text);
        _controls_to_check.Add(ENUM_CARDDATA_FIELD.EXTERNAL_NUMBER, txt_ext_num.Text);
        _controls_to_check.Add(ENUM_CARDDATA_FIELD.COLONY, txt_address_02.Text);
        _controls_to_check.Add(ENUM_CARDDATA_FIELD.DELEGATION, txt_address_03.Text);
        _controls_to_check.Add(ENUM_CARDDATA_FIELD.CITY, txt_city.Text);
        _controls_to_check.Add(ENUM_CARDDATA_FIELD.ZIP, txt_zip.Text);
        _controls_to_check.Add(ENUM_CARDDATA_FIELD.FEDERAL_ENTITY, cb_fed_entity.Text);
        _controls_to_check.Add(ENUM_CARDDATA_FIELD.ADDRESS_COUNTRY, cb_country.Text);
      }
      else
      {
        if (!CheckAddressAutomatic())
        {
          return false;
        }
      }

      if (CheckRequired(_controls_to_check, true, PLAYER_DATA_TO_CHECK.ACCOUNT, false))
      {
        return false;
      }

      return true;
    }

    private Boolean CheckAddressAutomatic()
    {
      // Street
      if (txt_address_01_new.Text == String.Empty)
      {
        tab_player_edit.SelectedTab = address;
        lbl_address_01_new.Focus();
        lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC", lbl_address_01_new.Text);

        return false;
      }
      // Ext Num
      if (txt_ext_num_new.Text == String.Empty)
      {
        tab_player_edit.SelectedTab = address;
        txt_ext_num_new.Focus();
        lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC", lbl_ext_num_new.Text);

        return false;
      }
      //Country
      if (lbl_country_new.ForeColor == Color.Red)
      {
        tab_player_edit.SelectedTab = address;
        cb_country_new.Focus();
        lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC", lbl_country_new.Text);

        return false;
      }
      // ZIP
      if ((txt_zip_new.Text == String.Empty) || (lbl_zip_new.ForeColor == Color.Red))
      {
        tab_player_edit.SelectedTab = address;
        txt_zip_new.Focus();
        lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC", lbl_zip_new.Text);

        return false;
      }
      //Fed entity
      if (lbl_fed_entity_new.ForeColor == Color.Red)
      {
        tab_player_edit.SelectedTab = address;
        cb_fed_entity_new.Focus();
        lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC", lbl_fed_entity_new.Text);

        return false;
      }
      // Delegation
      if (lbl_address_03_new.ForeColor == Color.Red)
      {
        tab_player_edit.SelectedTab = address;
        cb_address_03.Focus();
        lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC", lbl_address_03_new.Text);

        return false;
      }
      // Colony
      if (lbl_address_02_new.ForeColor == Color.Red)
      {
        tab_player_edit.SelectedTab = address;
        cb_address_02.Focus();
        lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC", lbl_address_02_new.Text);

        return false;
      }
      // City
      if (lbl_city_new.ForeColor == Color.Red)
      {
        tab_player_edit.SelectedTab = address;
        cb_city.Focus();
        lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC", lbl_city_new.Text);

        return false;
      }

      return true;

    } // CheckAddressAutomatic

    private Boolean CheckRequiredFieldsForBeneficiary(PLAYER_DATA_TO_CHECK _type_to_check)
    {
      RequiredData _controls_to_check;

      _controls_to_check = new RequiredData(_type_to_check);

      _controls_to_check.Add(ENUM_CARDDATA_FIELD.NAME3, txt_beneficiary_name.Text);
      _controls_to_check.Add(ENUM_CARDDATA_FIELD.NAME1, txt_beneficiary_last_name1.Text);
      _controls_to_check.Add(ENUM_CARDDATA_FIELD.NAME2, txt_beneficiary_last_name2.Text);

      _controls_to_check.Add(ENUM_CARDDATA_FIELD.ID1, txt_beneficiary_RFC.Text);
      _controls_to_check.Add(ENUM_CARDDATA_FIELD.ID2, txt_beneficiary_CURP.Text);

      _controls_to_check.Add(ENUM_CARDDATA_FIELD.BIRTHDATE, !uc_dt_ben_birth.IsEmpty);

      _controls_to_check.Add(ENUM_CARDDATA_FIELD.GENDER, cb_beneficiary_gender.SelectedValue);

      _controls_to_check.Add(ENUM_CARDDATA_FIELD.OCCUPATION, cb_beneficiary_occupation.SelectedValue);

      _controls_to_check.Add(ENUM_CARDDATA_FIELD.DOC_SCAN, m_card.PlayerTracking.BeneficiaryId3Type);

      if (CheckRequired(_controls_to_check, true, PLAYER_DATA_TO_CHECK.BENEFICIARY, false))
      {
        return false;
      }

      return true;
    }

    private Boolean CheckRequiredFieldsForMLMode()
    {

      if (!CheckRequiredFieldsForAccount(PLAYER_DATA_TO_CHECK.ANTIMONEYLAUNDERING_ACCOUNT))
      {
        return false;
      }

      if (chk_holder_has_beneficiary.Checked)
      {
        if (!CheckRequiredFieldsForBeneficiary(PLAYER_DATA_TO_CHECK.ANTIMONEYLAUNDERING_BENEFICIARY))
        {
          return false;
        }
      }

      return true;

    }

    private Boolean CheckRequired(RequiredData ControlsToCheck, Boolean SetFocus, PLAYER_DATA_TO_CHECK PlayerDataToCheck, Boolean ForcedRequired)
    {
      ENUM_CARDDATA_FIELD _focus_control;
      String _label = "";

      if (!ControlsToCheck.IsAnyEmpty(ForcedRequired, out _focus_control))
      {
        return false;
      }

      SetControlFocus(_focus_control, SetFocus, PlayerDataToCheck, out _label);
      if (_focus_control == ENUM_CARDDATA_FIELD.DOC_SCAN)
      {
        lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_SCAN");
      }
      else
      {
        lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC", _label);
      }

      return true;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Set focus on controls and get label
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Ctrl: ENUM_CARDDATA_FIELD that identify the control
    //          - SetFocus:  if True control get focus
    //
    //      - OUTPUT:
    //          - Label: String label control
    //
    // RETURNS:
    private void SetControlFocus(ENUM_CARDDATA_FIELD Field, Boolean SetFocus, PLAYER_DATA_TO_CHECK PlayerDataToCheck, out String Label)
    {
      Control _control_focus;

      Label = "";
      _control_focus = null;

      switch (Field)
      {
        case ENUM_CARDDATA_FIELD.NAME3:
          if (PlayerDataToCheck == PLAYER_DATA_TO_CHECK.ACCOUNT)
          {
            tab_player_edit.SelectedTab = principal;
            _control_focus = txt_name;
            Label = lbl_name.Text;
          }
          else
          {
            tab_player_edit.SelectedTab = beneficiary;
            _control_focus = txt_beneficiary_name;
            Label = lbl_beneficiary_name.Text;
          }
          break;
        case ENUM_CARDDATA_FIELD.NAME1:
          if (PlayerDataToCheck == PLAYER_DATA_TO_CHECK.ACCOUNT)
          {
            tab_player_edit.SelectedTab = principal;
            _control_focus = txt_last_name1;
            Label = lbl_last_name1.Text;
          }
          else
          {
            tab_player_edit.SelectedTab = beneficiary;
            _control_focus = txt_beneficiary_last_name1;
            Label = lbl_beneficiary_last_name1.Text;
          }
          break;
        case ENUM_CARDDATA_FIELD.NAME2:
          if (PlayerDataToCheck == PLAYER_DATA_TO_CHECK.ACCOUNT)
          {
            tab_player_edit.SelectedTab = principal;
            _control_focus = txt_last_name2;
            Label = lbl_last_name2.Text;
          }
          else
          {
            tab_player_edit.SelectedTab = beneficiary;
            _control_focus = txt_beneficiary_last_name2;
            Label = lbl_beneficiary_last_name2.Text;
          }
          break;
        case ENUM_CARDDATA_FIELD.ID:
          tab_player_edit.SelectedTab = principal;
          _control_focus = txt_document;
          Label = lbl_document.Text;
          break;
        case ENUM_CARDDATA_FIELD.ID1:
          if (PlayerDataToCheck == PLAYER_DATA_TO_CHECK.ACCOUNT)
          {
            tab_player_edit.SelectedTab = principal;
            _control_focus = txt_RFC;
            Label = btn_RFC.Text;
          }
          else
          {
            tab_player_edit.SelectedTab = beneficiary;
            _control_focus = txt_beneficiary_RFC;
            Label = lbl_beneficiary_RFC.Text;
          }
          break;
        case ENUM_CARDDATA_FIELD.ID2:
          if (PlayerDataToCheck == PLAYER_DATA_TO_CHECK.ACCOUNT)
          {
            tab_player_edit.SelectedTab = principal;
            _control_focus = txt_CURP;
            Label = lbl_CURP.Text;
          }
          else
          {
            tab_player_edit.SelectedTab = beneficiary;
            _control_focus = txt_beneficiary_CURP;
            Label = lbl_beneficiary_CURP.Text;
          }
          break;
        case ENUM_CARDDATA_FIELD.GENDER:
          if (PlayerDataToCheck == PLAYER_DATA_TO_CHECK.ACCOUNT)
          {
            tab_player_edit.SelectedTab = principal;
            _control_focus = cb_gender;
            Label = lbl_gender.Text;
          }
          else
          {
            tab_player_edit.SelectedTab = beneficiary;
            _control_focus = cb_beneficiary_gender;
            Label = lbl_beneficiary_gender.Text;
          }
          break;
        case ENUM_CARDDATA_FIELD.DOC_TYPE:
          tab_player_edit.SelectedTab = principal;
          _control_focus = cb_document_type;
          Label = lbl_type_document.Text;
          break;
        case ENUM_CARDDATA_FIELD.MARITAL_STATUS:
          tab_player_edit.SelectedTab = principal;
          _control_focus = cb_marital;
          Label = lbl_marital_status.Text;
          break;
        case ENUM_CARDDATA_FIELD.BIRTHDATE:
          if (PlayerDataToCheck == PLAYER_DATA_TO_CHECK.ACCOUNT)
          {
            tab_player_edit.SelectedTab = principal;
            _control_focus = uc_dt_birth;
            Label = lbl_birth_date.Text;
          }
          else
          {
            tab_player_edit.SelectedTab = beneficiary;
            _control_focus = uc_dt_ben_birth;
            Label = lbl_beneficiary_birth_date.Text;
          }
          break;
        case ENUM_CARDDATA_FIELD.WEDDING_DATE:
          tab_player_edit.SelectedTab = principal;
          _control_focus = uc_dt_wedding;
          Label = lbl_wedding_date.Text;
          break;
        case ENUM_CARDDATA_FIELD.NATIONALITY:
          tab_player_edit.SelectedTab = principal;
          _control_focus = cb_nationality;
          Label = lbl_nationality.Text;
          break;
        case ENUM_CARDDATA_FIELD.BIRTH_COUNTRY:
          tab_player_edit.SelectedTab = principal;
          _control_focus = cb_birth_country;
          Label = lbl_birth_country.Text;
          break;
        case ENUM_CARDDATA_FIELD.PHONE1:
          tab_player_edit.SelectedTab = contact;
          _control_focus = txt_phone_01;
          Label = lbl_phone_01.Text;
          break;
        case ENUM_CARDDATA_FIELD.PHONE2:
          tab_player_edit.SelectedTab = contact;
          _control_focus = txt_phone_02;
          Label = lbl_phone_02.Text;
          break;
        case ENUM_CARDDATA_FIELD.EMAIL1:
          tab_player_edit.SelectedTab = contact;
          _control_focus = txt_email_01;
          Label = lbl_email_01.Text;
          break;
        case ENUM_CARDDATA_FIELD.EMAIL2:
          tab_player_edit.SelectedTab = contact;
          _control_focus = txt_email_02;
          Label = lbl_email_02.Text;
          break;
        case ENUM_CARDDATA_FIELD.TWITTER:
          tab_player_edit.SelectedTab = contact;
          _control_focus = txt_twitter;
          Label = lbl_twitter.Text;
          break;
        case ENUM_CARDDATA_FIELD.PIN:
          tab_player_edit.SelectedTab = pin;
          _control_focus = txt_pin;
          Label = lbl_pin.Text;
          break;
        case ENUM_CARDDATA_FIELD.STREET:
          tab_player_edit.SelectedTab = address;
          _control_focus = txt_address_01;
          Label = lbl_address_01.Text;
          break;
        case ENUM_CARDDATA_FIELD.EXTERNAL_NUMBER:
          tab_player_edit.SelectedTab = address;
          _control_focus = txt_ext_num;
          Label = lbl_ext_num.Text;
          break;
        case ENUM_CARDDATA_FIELD.COLONY:
          tab_player_edit.SelectedTab = address;
          _control_focus = txt_address_02;
          Label = lbl_address_02.Text;
          break;
        case ENUM_CARDDATA_FIELD.DELEGATION:
          tab_player_edit.SelectedTab = address;
          _control_focus = txt_address_03;
          Label = lbl_address_03.Text;
          break;
        case ENUM_CARDDATA_FIELD.CITY:
          tab_player_edit.SelectedTab = address;
          _control_focus = txt_city;
          Label = lbl_city.Text;
          break;
        case ENUM_CARDDATA_FIELD.ZIP:
          tab_player_edit.SelectedTab = address;
          _control_focus = txt_zip;
          Label = lbl_zip.Text;
          break;
        case ENUM_CARDDATA_FIELD.FEDERAL_ENTITY:
          tab_player_edit.SelectedTab = address;
          _control_focus = cb_fed_entity;
          Label = lbl_fed_entity.Text;
          break;
        case ENUM_CARDDATA_FIELD.ADDRESS_COUNTRY:
          tab_player_edit.SelectedTab = address;
          _control_focus = cb_country;
          Label = lbl_country.Text;
          break;
        case ENUM_CARDDATA_FIELD.COMMENTS:
          tab_player_edit.SelectedTab = comments;
          _control_focus = txt_comments;
          Label = comments.Text;
          break;
        case ENUM_CARDDATA_FIELD.OCCUPATION:
          if (PlayerDataToCheck == PLAYER_DATA_TO_CHECK.ACCOUNT)
          {
            tab_player_edit.SelectedTab = principal;
            _control_focus = cb_occupation;
            Label = lbl_occupation.Text;
          }
          else
          {
            tab_player_edit.SelectedTab = beneficiary;
            _control_focus = cb_beneficiary_occupation;
            Label = lbl_beneficiary_occupation.Text;
          }
          break;
        case ENUM_CARDDATA_FIELD.DOC_SCAN:
          if (PlayerDataToCheck == PLAYER_DATA_TO_CHECK.ACCOUNT)
          {
            tab_player_edit.SelectedTab = principal;
            _control_focus = btn_scan_docs;
            Label = btn_scan_docs.Text;
          }
          else
          {
            tab_player_edit.SelectedTab = beneficiary;
            _control_focus = btn_ben_scan_docs;
            Label = btn_ben_scan_docs.Text;
          }
          break;

        case ENUM_CARDDATA_FIELD.NAME4:
          tab_player_edit.SelectedTab = principal;
          _control_focus = txt_name4;
          Label = lbl_name4.Text;
          break;

      }
      if (SetFocus && _control_focus != null)
      {
        _control_focus.Focus();
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Do the PIN change update to BD operation
    // 
    //  PARAMS:
    //      - INPUT:
    //        NewPin: New PIN value
    //        RandomGeneration: True if is a random generation, false if is a manual input
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    private void pin_save(String NewPin, Boolean RandomGeneration)
    {
      DialogResult _dlg_rc;

      _dlg_rc = SavingCardConfirmation(true, false, Resource.String("STR_FRM_ACCOUNT_USER_EDIT_SAVE_CARD_PIN"));

      if (_dlg_rc == DialogResult.Cancel)
      {
        return;
      }

      if (_dlg_rc == DialogResult.OK)
      {
        if (!CheckData(true))
        {
          return;
        }

        if (!SaveCard())
        {
          return;
        }

        AuditPlayerData();

        DuplicateCard();

        lbl_msg_blink.Visible = false;
      }

      if (CashierBusinessLogic.DB_UpdateCardPin(m_card, NewPin, m_card.PlayerTracking.Pin, RandomGeneration))
      {
        m_card.PlayerTracking.Pin = txt_pin.Text;

        lbl_pin_msg.Visible = true;

        if (RandomGeneration)
        {
          lbl_pin_msg.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_PIN_GENERATED_CORRECTLY");
        }
        else
        {
          lbl_pin_msg.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_PIN_CHANGED_CORRECTLY");
        }

        lbl_pin_msg.ForeColor = Color.Green;

        txt_pin.Enabled = false;
        txt_confirm_pin.Enabled = false;
        btn_save_pin.Enabled = false;
        btn_generate_random_pin.Enabled = false;
        m_pin_request_enabled = false;
      }
      else
      {
        lbl_pin_msg.Visible = true;
        lbl_pin_msg.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_ERROR_UPDATING_PIN");
        lbl_pin_msg.ForeColor = Color.Red;
      }
    } // pin_save

    //------------------------------------------------------------------------------
    // PURPOSE: Manage control enabling according to PIN values input
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    private void btn_save_pin_enable()
    {
      txt_confirm_pin.Enabled = (txt_pin.Text.Length >= Accounts.PIN_MIN_LENGTH);
      btn_save_pin.Enabled = ((txt_pin.Text.Length >= Accounts.PIN_MIN_LENGTH)
                              && (txt_confirm_pin.Text.Length == txt_pin.Text.Length)
                              && (txt_pin.Text == txt_confirm_pin.Text));

      if ((txt_pin.Text.Length >= Accounts.PIN_MIN_LENGTH)
         && (txt_confirm_pin.Text.Length == txt_pin.Text.Length)
         && (txt_pin.Text != txt_confirm_pin.Text))
      {
        lbl_pin_msg.Visible = true;
        lbl_pin_msg.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_CONFIRMATION_PIN_DIFFERS");
        lbl_pin_msg.ForeColor = Color.Red;
      }
      else
      {
        lbl_pin_msg.Visible = false;
        lbl_pin_msg.Text = "";
      }
    } // btn_save_pin_enable

    // PURPOSE : Validates the format of the fields
    //
    //  PARAMS:
    //     - INPUT:
    //        focus_it : If true, focus incorrect text control.
    //
    //     - OUTPUT:
    //
    // RETURNS:
    //         - TRUE:   If the format of all fields is correct
    //         - FALSE:  If there is a field that has incorrect format
    //
    private Boolean ValidateFormat(Boolean focus_it)
    {
      String _beneficiary_surname1;
      String _beneficiary_surname2;
      String _beneficiary_name;

      RequiredData _date_to_check;

      GENDER _gender;
      GENDER _beneficiary_gender;
      ACCOUNT_HOLDER_ID_TYPE _which_one_is_wrong;
      String _expected_id;
      String _regular_expression;

      _date_to_check = new RequiredData();

      _expected_id = null;
      _regular_expression = "";

      _beneficiary_surname1 = txt_beneficiary_last_name1.Text;
      _beneficiary_surname2 = txt_beneficiary_last_name2.Text;
      _beneficiary_name = txt_beneficiary_name.Text;

      //
      // VALIDATE PLAYER DATA
      //

      // Validate format of Name field
      if (!String.IsNullOrEmpty(txt_name.Text) && !txt_name.ValidateFormat())
      {
        lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", this.lbl_name.Text);
        if (focus_it)
        {
          tab_player_edit.SelectedIndex = 0;
          txt_name.Focus();
        }

        return false;
      }

      // Validate format of Last Name 1 field
      if (!String.IsNullOrEmpty(txt_last_name1.Text) && !txt_last_name1.ValidateFormat())
      {
        lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", this.lbl_last_name1.Text);
        if (focus_it)
        {
          tab_player_edit.SelectedIndex = 0;
          txt_last_name1.Focus();
        }

        return false;
      }

      // Validate format of middle name
      if (!String.IsNullOrEmpty(txt_name4.Text) && !txt_name4.ValidateFormat())
      {
        lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", this.lbl_name4.Text);
        if (focus_it)
        {
          tab_player_edit.SelectedIndex = 0;
          txt_name4.Focus();
        }

        return false;
      }

      // Validate format of Last Name 2 field
      if (!String.IsNullOrEmpty(txt_last_name2.Text) && !txt_last_name2.ValidateFormat())
      {
        lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", this.lbl_last_name2.Text);
        if (focus_it)
        {
          tab_player_edit.SelectedIndex = 0;
          txt_last_name2.Focus();
        }

        return false;
      }

      switch (cb_gender.SelectedIndex)
      {
        case 0:
          _gender = GENDER.MALE;
          break;

        case 1:
          _gender = GENDER.FEMALE;
          break;

        default:
          _gender = GENDER.UNKNOWN;
          break;
      }


      // Validate format of Document field
      if (!String.IsNullOrEmpty(txt_document.Text) && !txt_document.ValidateFormat())
      {
        lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", this.lbl_document.Text);
        if (focus_it)
        {
          tab_player_edit.SelectedIndex = 0;
          txt_document.Focus();
        }

        return false;
      }

      // Validate  RFC format
      if (!String.IsNullOrEmpty(txt_RFC.Text) && !txt_RFC.ValidateFormat())
      {
        lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", this.btn_RFC.Text);

        if (focus_it)
        {
          tab_player_edit.SelectedIndex = 0;
          txt_RFC.Focus();
        }

        return false;
      }

      // Validate CURP format
      if (!String.IsNullOrEmpty(txt_CURP.Text) && !txt_CURP.ValidateFormat())
      {
        lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", this.lbl_CURP.Text);

        if (focus_it)
        {
          tab_player_edit.SelectedIndex = 0;
          txt_CURP.Focus();
        }

        return false;
      }

      if (!uc_dt_birth.ValidateFormat())
      {
        lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_BIRTHDATE_ERROR");

        if (focus_it)
        {
          tab_player_edit.SelectedIndex = 0;
          uc_dt_birth.Focus();
        }

        return false;
      }

      if (!CardData.IsValidBirthdate(uc_dt_birth.DateValue, out m_legal_age))
      {
        lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_NOT_ADULT", m_legal_age);
        if (focus_it)
        {
          tab_player_edit.SelectedIndex = 0;
          uc_dt_birth.Focus();
        }
        return false;
      }

      if (!Common.ValidateFormat.CheckHolderIds(this.txt_last_name1.Text.Trim(),
                                                this.txt_last_name2.Text.Trim(),
                                                this.txt_name.Text.Trim(),
                                                this.txt_RFC.Text,
                                                this.txt_CURP.Text,
                                                this.uc_dt_birth.DateValue,
                                                _gender,
                                                false,
                                                out _which_one_is_wrong,
                                                out _expected_id))
      {

        switch (_which_one_is_wrong)
        {
          case ACCOUNT_HOLDER_ID_TYPE.RFC:
            if (m_rfc_visible)
            {
              lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_EXPECTED_RFC", _expected_id);

              if (focus_it)
              {
                tab_player_edit.SelectedIndex = 0;
                txt_RFC.Focus();
              }
              return false;
            }
            break;

          case ACCOUNT_HOLDER_ID_TYPE.CURP:
            if (m_curp_visible)
            {
              lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_EXPECTED_CURP", _expected_id);

              if (focus_it)
              {
                tab_player_edit.SelectedIndex = 0;
                txt_CURP.Focus();
              }
              return false;
            }
            break;

          default:
            return false;
        }
      }

      // QMP 09-DEC-2013: Check RFC against regex and do not allow generic RFCs
      if (!String.IsNullOrEmpty(txt_RFC.Text.Trim()) && (Common.ValidateFormat.WrongRFC(txt_RFC.Text)))
      {
        lbl_msg_blink.Text = Resource.String("STR_FRM_WITHOLDING_009", btn_RFC.Text);

        if (focus_it)
        {
          tab_player_edit.SelectedIndex = 0;
          txt_RFC.Focus();
        }

        return false;
      }

      // Check if there is at least one non empty field of wedding.
      if (!uc_dt_wedding.IsEmpty)
      {

        if (!uc_dt_wedding.ValidateFormat())
        {
          lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_WEDDING_ERROR");

          if (focus_it)
          {
            tab_player_edit.SelectedIndex = 0;
            uc_dt_wedding.Focus();
          }
          return false;
        }

        if (!CardData.IsValidWeddingdate(uc_dt_birth.DateValue, uc_dt_wedding.DateValue))
        {
          lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_WEDDING_ERROR");

          if (focus_it)
          {
            tab_player_edit.SelectedIndex = 0;
            uc_dt_wedding.Focus();
          }
          return false;
        }
      }

      // FBA 18-NOV-2013: Validate if nationality combo has an actual value
      if (cb_nationality.Text != "" && cb_nationality.SelectedValue == null)
      {
        lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_VALID_MSG_BLINK_GENERIC", lbl_nationality.Text);
        if (focus_it)
        {
          tab_player_edit.SelectedIndex = 0;
          cb_nationality.Focus();
        }
        return false;
      }

      // FBA 18-NOV-2013: Validate if birth country combo has an actual value
      if (cb_birth_country.Text != "" && cb_birth_country.SelectedValue == null)
      {
        lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_VALID_MSG_BLINK_GENERIC", lbl_birth_country.Text);
        if (focus_it)
        {
          tab_player_edit.SelectedIndex = 0;
          cb_birth_country.Focus();
        }
        return false;
      }
      // FBA 18-NOV-2013: Validate if occupation combo has an actual value
      if (cb_occupation.Text != "" && cb_occupation.SelectedValue == null)
      {
        lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_VALID_MSG_BLINK_GENERIC", lbl_occupation.Text);
        if (focus_it)
        {
          tab_player_edit.SelectedIndex = 0;
          cb_occupation.Focus();
        }
        return false;

      }

      // Check if there is at least one non empty field of beneficiary birth date.
      if (this.chk_holder_has_beneficiary.Checked && !uc_dt_ben_birth.IsEmpty)
      {
        if (!uc_dt_ben_birth.ValidateFormat())
        {
          lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_BENEFICIARY_BIRTH_DATE_ERROR");

          if (focus_it)
          {
            tab_player_edit.SelectedTab = tab_player_edit.TabPages["beneficiary"];
            uc_dt_ben_birth.Focus();
          }
          return false;
        }

        if (!CardData.IsValidBirthdate(uc_dt_ben_birth.DateValue, out m_legal_age))
        {
          lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_NOT_ADULT_BENEFICIARY", m_legal_age);

          if (focus_it)
          {
            tab_player_edit.SelectedIndex = 3;
            uc_dt_ben_birth.Focus();
          }
          return false;
        }
      }

      // Validate format of Phone 1 field
      if (!String.IsNullOrEmpty(txt_phone_01.Text) && !txt_phone_01.ValidateFormat(false))
      {
        lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", this.lbl_phone_01.Text);
        if (focus_it)
        {
          tab_player_edit.SelectedIndex = 1;
          txt_phone_01.Focus();
        }

        return false;
      }

      // Validate format of Phone 2 field
      if (!String.IsNullOrEmpty(txt_phone_02.Text) && !txt_phone_02.ValidateFormat(false))
      {
        lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", this.lbl_phone_02.Text);
        if (focus_it)
        {
          tab_player_edit.SelectedIndex = 1;
          txt_phone_02.Focus();
        }

        return false;
      }

      // Validate format of Email 1 field
      if (!String.IsNullOrEmpty(txt_email_01.Text) && !Common.ValidateFormat.Email(txt_email_01.Text))
      {
        lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", lbl_email_01.Text);
        if (focus_it)
        {
          tab_player_edit.SelectedIndex = 1;
          txt_email_01.Focus();
        }

        return false;
      }

      // Validate format of Email 2 field
      if (!String.IsNullOrEmpty(txt_email_02.Text) && !Common.ValidateFormat.Email(txt_email_02.Text))
      {
        lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", lbl_email_02.Text);
        if (focus_it)
        {
          tab_player_edit.SelectedIndex = 1;
          txt_email_02.Focus();
        }

        return false;
      }

      // Validate format of twitter field
      if (!String.IsNullOrEmpty(txt_twitter.Text) && !Common.ValidateFormat.Twitter(txt_twitter.Text))
      {
        lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", lbl_twitter.Text);
        if (focus_it)
        {
          tab_player_edit.SelectedIndex = 1;
          txt_twitter.Focus();
        }

        return false;
      }

      // Validate format of Address 1 field
      if (chk_address_mode.Checked)
      {
        if (!String.IsNullOrEmpty(txt_address_01.Text) && !txt_address_01.ValidateFormat())
        {
          lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", this.lbl_address_01.Text);
          if (focus_it)
          {
            tab_player_edit.SelectedIndex = 2;
            txt_address_01.Focus();
          }

          return false;
        }
      }
      else
      {
        if (!String.IsNullOrEmpty(txt_address_01_new.Text) && !txt_address_01_new.ValidateFormat())
        {
          lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", this.lbl_address_01_new.Text);
          if (focus_it)
          {
            tab_player_edit.SelectedIndex = 2;
            txt_address_01_new.Focus();
          }

          return false;
        }
      }

      // Validate format of Address 2 field
      if (!String.IsNullOrEmpty(txt_address_02.Text) && !txt_address_02.ValidateFormat())
      {
        lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", this.lbl_address_02.Text);
        if (focus_it)
        {
          tab_player_edit.SelectedIndex = 2;
          txt_address_02.Focus();
        }

        return false;
      }

      // Validate format of Address 3 field
      if (!String.IsNullOrEmpty(txt_address_03.Text) && !txt_address_03.ValidateFormat())
      {
        lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", this.lbl_address_03.Text);
        if (focus_it)
        {
          tab_player_edit.SelectedIndex = 2;
          txt_address_03.Focus();
        }

        return false;
      }

      // Validate format of City field
      if (!String.IsNullOrEmpty(txt_city.Text) && !txt_city.ValidateFormat())
      {
        lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", this.lbl_city.Text);
        if (focus_it)
        {
          tab_player_edit.SelectedIndex = 2;
          txt_city.Focus();
        }

        return false;
      }

      // Validate format of Zip field
      _regular_expression = GeneralParam.GetString("Account", "ZipCode.ValidationRegex");
      if (!String.IsNullOrEmpty(txt_zip.Text) && !Common.ValidateFormat.CheckRegularExpression(txt_zip.Text, _regular_expression))
      {
        lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", this.lbl_zip.Text);
        if (focus_it)
        {
          tab_player_edit.SelectedIndex = 2;
          txt_zip.Focus();
        }

        return false;
      }

      switch (cb_beneficiary_gender.SelectedIndex)
      {
        case 0:
          _beneficiary_gender = GENDER.MALE;
          break;

        case 1:
          _beneficiary_gender = GENDER.FEMALE;
          break;

        default:
          _beneficiary_gender = GENDER.UNKNOWN;
          break;
      }

      //
      // VALIDATE BENEFICIARY DATA
      //

      if (this.chk_holder_has_beneficiary.Checked)
      {
        // Validate beneficiary RFC format
        if (!String.IsNullOrEmpty(txt_beneficiary_RFC.Text) && (!txt_beneficiary_RFC.ValidateFormat() || uc_dt_ben_birth.DateValue == DateTime.MinValue))
        {
          lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", this.lbl_beneficiary_RFC.Text);

          if (focus_it)
          {
            tab_player_edit.SelectedIndex = 3;
            txt_beneficiary_RFC.Focus();
          }

          return false;
        }

        // Validate beneficiary CURP format
        if (!String.IsNullOrEmpty(txt_beneficiary_CURP.Text) && (!txt_beneficiary_CURP.ValidateFormat() || uc_dt_ben_birth.DateValue == DateTime.MinValue))
        {
          lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", this.lbl_beneficiary_CURP.Text);

          if (focus_it)
          {
            tab_player_edit.SelectedIndex = 3;
            txt_beneficiary_CURP.Focus();
          }

          return false;
        }

        if (!Common.ValidateFormat.CheckHolderIds(_beneficiary_surname1, _beneficiary_surname2, _beneficiary_name,
                                                  this.txt_beneficiary_RFC.Text, this.txt_beneficiary_CURP.Text,
                                                  this.uc_dt_ben_birth.DateValue,
                                                  _beneficiary_gender,
                                                  false,
                                                  out _which_one_is_wrong, out _expected_id))
        {

          switch (_which_one_is_wrong)
          {
            case ACCOUNT_HOLDER_ID_TYPE.RFC:
              if (m_rfc_visible)
              {
                lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_EXPECTED_RFC", _expected_id);

                if (focus_it)
                {
                  tab_player_edit.SelectedIndex = 3;
                  txt_beneficiary_RFC.Focus();
                }
              }
              return false;

            case ACCOUNT_HOLDER_ID_TYPE.CURP:
              if (m_curp_visible)
              {
                lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_EXPECTED_CURP", _expected_id);

                if (focus_it)
                {
                  tab_player_edit.SelectedIndex = 3;
                  txt_beneficiary_CURP.Focus();
                }
              }
              return false;

            default:
              return false;
          }
        }
        // FBA 18-NOV-2013: Validate if occupation (beneficiary) combo has an actual value
        if (cb_beneficiary_occupation.Text != "" && cb_beneficiary_occupation.SelectedValue == null)
        {
          lbl_msg_blink.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_VALID_MSG_BLINK_GENERIC", lbl_beneficiary_occupation.Text);
          if (focus_it)
          {
            tab_player_edit.SelectedIndex = 3;
            cb_beneficiary_occupation.Focus();
          }
          return false;
        }

      }

      return true;
    } // ValidateFormat

    private void SetDocMsg(Label MsgLabel, int Sides)
    {
      MsgLabel.Visible = true;

      switch (Sides)
      {
        case 1:
          // "Falta el lado @p0"
          MsgLabel.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_ONE_SIDE_LEFT", 1);
          MsgLabel.ForeColor = Color.DarkRed;
          break;

        case 2:
          // "Falta el lado @p0"
          MsgLabel.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_ONE_SIDE_LEFT", 2);
          MsgLabel.ForeColor = Color.DarkRed;
          break;

        case 3:
          // "Faltan ambos lados"
          MsgLabel.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_BOTH_SIDES_LEFT");
          MsgLabel.ForeColor = Color.DarkRed;
          break;

        case 0:
        default:
          // "No hay documento escaneado"
          MsgLabel.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_DOC_NOT_SCANNED");
          MsgLabel.ForeColor = Color.Navy;
          break;

      }
    } // SetDocMsg

    // PURPOSE : Sets new level values
    //
    //  PARAMS:
    //     - INPUT:
    //
    //     - OUTPUT:
    //
    // RETURNS:
    //         
    private void GetNextLevelInfo()
    {
      AccountNextLevel _next_level_info;
      Points _generated;
      Points _discretionaries;
      Points _level_points;
      Points _required;
      Points _maintain;

      _next_level_info = new AccountNextLevel(m_card.AccountId);

      //29-MAY-2013: For the moment, is not possible no calculate the amount of points left to reach next level on multisite mode.
      if (_next_level_info.LevelId >= 0 && !GeneralParam.GetBoolean("Site", "MultiSiteMember", false))
      {
        _generated = Math.Truncate(_next_level_info.PointsGenerated);
        _discretionaries = Math.Truncate(_next_level_info.PointsDiscretionaries);
        _level_points = _generated + _discretionaries;
        _maintain = GeneralParam.GetInt32("PlayerTracking", "Level0" + m_card.PlayerTracking.CardLevel + ".PointsToKeep");
        _maintain = Math.Max(_maintain - (_level_points), 0);
        lbl_points_to_maintain.Text = m_card.PlayerTracking.CardLevel <= 1 ? "---" : _maintain.ToString();
        lbl_points_to_maintain.TextAlign = (m_card.PlayerTracking.CardLevel <= 1) ? ContentAlignment.MiddleLeft : ContentAlignment.MiddleRight;
        _required = GeneralParam.GetInt32("PlayerTracking", "Level0" + _next_level_info.LevelId + ".PointsToEnter");
        lbl_points_discretionaries.Text = (_discretionaries <= 0) ? "---" : _discretionaries.ToString();
        lbl_points_generated.Text = (_generated <= 0) ? "---" : _generated.ToString();
        lbl_points_level.Text = (_level_points <= 0) ? "---" : _level_points.ToString();

        if (_next_level_info.LevelId != m_card.PlayerTracking.CardLevel)
        {
          lbl_next_level_name.Text = _next_level_info.LevelName;
          lbl_points_to_next_level.Text = _next_level_info.PointsToReach.ToString();
          lbl_points_req.Text = _required.ToString();
        }
      }
      else
      {
        gb_next_level.Visible = false;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: throws the digitizing window
    // 
    //  PARAMS:
    //      - INPUT:     
    //
    //      - OUTPUT:
    //            -  Document List
    //            -  Id3Types
    // RETURNS:
    private void ScanId(ACCOUNT_SCANNED_OWNER DocsOwner, String ScanTitle, CardData PlayerData)
    {
      try
      {
        frm_scan_document _frm_scan_document;

        _frm_scan_document = new frm_scan_document(DocsOwner,
                                                   ScanTitle,
                                                   PlayerData,
                                                   DocsOwner == ACCOUNT_SCANNED_OWNER.HOLDER ? m_docs_holder_changed : m_docs_beneficiary_changed,
                                                   this.m_antimoneylaundering_enabled);

        lbl_msg_blink.Visible = false;

        if (_frm_scan_document.Show())
        {
          this.Cursor = Cursors.WaitCursor;

          switch (DocsOwner)
          {
            case ACCOUNT_SCANNED_OWNER.HOLDER:
              m_card.PlayerTracking.HolderScannedDocs = _frm_scan_document.DocumentsList;
              m_card.PlayerTracking.HolderId3Type = _frm_scan_document.Id3Type;
              m_docs_holder_changed = _frm_scan_document.DocumentsChanged;
              break;

            case ACCOUNT_SCANNED_OWNER.BENEFICIARY:
              m_card.PlayerTracking.BeneficiaryScannedDocs = _frm_scan_document.DocumentsList;
              m_card.PlayerTracking.BeneficiaryId3Type = _frm_scan_document.Id3Type;
              m_docs_beneficiary_changed = _frm_scan_document.DocumentsChanged;
              break;

            default:
              break;
          }
        }
        _frm_scan_document.Dispose();

        //Show keyboard if Automatic OSK is enabled
        WSIKeyboard.Show();

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {
        this.Cursor = Cursors.Default;
      }
    } // ScanId

    //------------------------------------------------------------------------------
    // PURPOSE: Check if necessary save card to continue action (require user confirmation) 
    // 
    //  PARAMS:
    //      - INPUT:     
    //            -  CheckNewAccount: check if account id is equal to cero
    //            -  CheckChanges: check if made any change on data
    //            -  Message: Confirmation message
    //      - OUTPUT:
    // 
    // RETURNS:
    //      -  DialogResult:
    //            -  None: is not necessary save the card
    //            -  Ok: must save the card
    //            -  Cancel: canceled action
    // 
    private DialogResult SavingCardConfirmation(Boolean CheckNewAccount, Boolean CheckChanges, String Message)
    {
      String _message;
      DialogResult _dlg_rc;
      Boolean _show_dlg;

      _dlg_rc = DialogResult.None;
      _show_dlg = (CheckNewAccount && m_card.AccountId == 0);

      if (!_show_dlg && CheckChanges)
      {
        GetScreenData();
        _show_dlg = HasChanges(m_card_read.PlayerTracking, m_card.PlayerTracking);
      }

      if (_show_dlg)
      {
        _message = Message.Replace("\\r\\n", "\r\n");
        _dlg_rc = frm_message.Show(_message, Resource.String("STR_APP_GEN_MSG_WARNING"),
                                   MessageBoxButtons.YesNo, Images.CashierImage.Warning, this);
      }

      return _dlg_rc;
    }

    private void SetFieldsVisibility()
    {
      SetFormTitle();

      VisibleData _visible_data;

      _visible_data = new VisibleData();
      _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.NAME3, ToList(lbl_name, txt_name));
      _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.NAME4, ToList(lbl_name4, txt_name4));
      _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.NAME1, ToList(lbl_last_name1, txt_last_name1));
      _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.NAME2, ToList(lbl_last_name2, txt_last_name2));

      _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.ID1, ToList(btn_RFC, txt_RFC));
      _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.ID2, ToList(lbl_CURP, txt_CURP));
      _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.DOC_SCAN, ToList(btn_scan_docs));
      _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.GENDER, ToList(lbl_gender, cb_gender));
      _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.NATIONALITY, ToList(lbl_nationality, cb_nationality));
      _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.BIRTH_COUNTRY, ToList(lbl_birth_country, cb_birth_country));
      _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.OCCUPATION, ToList(lbl_occupation, cb_occupation));
      _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.MARITAL_STATUS, ToList(lbl_marital_status, cb_marital));
      _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.WEDDING_DATE, ToList(lbl_wedding_date, uc_dt_wedding));

      _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.PHONE1, ToList(lbl_phone_01, txt_phone_01));
      _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.PHONE2, ToList(lbl_phone_02, txt_phone_02));
      _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.EMAIL1, ToList(lbl_email_01, txt_email_01));
      _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.EMAIL2, ToList(lbl_email_02, txt_email_02));
      _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.TWITTER, ToList(lbl_twitter, txt_twitter));

      _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.STREET, ToList(lbl_address_01, txt_address_01));
      _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.EXTERNAL_NUMBER, ToList(lbl_ext_num, txt_ext_num));
      _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.COLONY, ToList(lbl_address_02, txt_address_02));
      _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.DELEGATION, ToList(lbl_address_03, txt_address_03));
      _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.CITY, ToList(lbl_city, txt_city));
      _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.ZIP, ToList(lbl_zip, txt_zip));
      _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.FEDERAL_ENTITY, ToList(lbl_fed_entity, cb_fed_entity));
      _visible_data.AddDataAcc(ENUM_CARDDATA_FIELD.ADDRESS_COUNTRY, ToList(lbl_country, cb_country));

      _visible_data.HideControls();

      Int32 _count_name;
      Int32 _count;
      float _width;

      _count = 0;
      _count_name = 0;
      _count_name += lbl_name.Visible ? 1 : 0;
      _count_name += lbl_name4.Visible ? 1 : 0;
      _count_name += lbl_last_name1.Visible ? 1 : 0;
      _count_name += lbl_last_name2.Visible ? 1 : 0;

      if (_count_name == 0)
      {
        return;
      }

      _width = (float)(100 / _count_name);

      foreach (ColumnStyle _col_style in tlp_name.ColumnStyles)
      {
        switch (_count)
        {
          case 0:
            _col_style.Width = lbl_name.Visible ? _width : 0;
            break;
          case 1:
            _col_style.Width = lbl_name4.Visible ? _width : 0;
            break;
          case 2:
            _col_style.Width = lbl_last_name1.Visible ? _width : 0;
            break;
          case 3:
            _col_style.Width = lbl_last_name2.Visible ? _width : 0;
            break;
          default:
            break;
        }

        _count++;
      }

      if (this.txt_last_name1.Visible && !this.txt_last_name2.Visible)
      {
        this.lbl_last_name1.Text = Resource.String("STR_FRM_PLAYER_EDIT_LAST_NAME");
      }

      if (!GeneralParam.GetBoolean("Account.VisibleField", "Phone1", true)
        || !GeneralParam.GetBoolean("Account.VisibleField", "Phone2", true))
      {
        if (GeneralParam.GetBoolean("Account.VisibleField", "Phone1", true))
        {
          this.lbl_phone_01.Text = Resource.String("STR_FRM_PLAYER_EDIT_PHONE");
        }
        if (GeneralParam.GetBoolean("Account.VisibleField", "Phone2", true))
        {
          this.lbl_phone_02.Text = Resource.String("STR_FRM_PLAYER_EDIT_PHONE");
        }
      }

      if (!GeneralParam.GetBoolean("Account.VisibleField", "Email1", true)
        || !GeneralParam.GetBoolean("Account.VisibleField", "Email2", true))
      {
        if (GeneralParam.GetBoolean("Account.VisibleField", "Email1", true))
        {
          this.lbl_email_01.Text = Resource.String("STR_FRM_PLAYER_EDIT_EMAIL");
        }
        if (GeneralParam.GetBoolean("Account.VisibleField", "Email2", true))
        {
          this.lbl_email_02.Text = Resource.String("STR_FRM_PLAYER_EDIT_EMAIL");
        }
      }

      if (MustShowComments)
      {
        String error_str;
        tab_player_edit.SelectTab("comments");
        HideTabs();
        SetContentForComments(ProfilePermissions.CheckPermissionList(ProfilePermissions.CashierFormFuncionality.EditCommentsRequiredReading,
                                                                     ProfilePermissions.TypeOperation.OnlyCheck,
                                                                     this.ParentForm, 0, out error_str));
      }
      else
      {
        if (m_card.IsNew)
        {
          this.txt_name.Focus();
        }
        else
        {
          this.lbl_full_name.Focus();
        }
      }

    }

    private void SetContentForComments(Boolean IsEditable)
    {
      chk_show_comments.Enabled = IsEditable;

      btn_print_information.Visible = false;
      btn_ok.Visible = IsEditable;
      btn_keyboard.Visible = IsEditable;
      if (!IsEditable)
      {
        btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CLOSE");
      }
      txt_comments.ReadOnly = !IsEditable;
    } // SetContentForComments

    private void HideTabs()
    {
      if (tab_player_edit.TabCount > 0)
      {
        foreach (TabPage _tab in tab_player_edit.TabPages)
        {
          if (_tab.Name != "comments")
          {
            tab_player_edit.TabPages.Remove(_tab);
          }
        }
      }
    } // HideTabs

    private void EnableRfcButton()
    {
      String _error_str;

      if (!String.IsNullOrEmpty(txt_name.Text) && !String.IsNullOrEmpty(txt_last_name1.Text)
         && !String.IsNullOrEmpty(txt_last_name2.Text) && String.IsNullOrEmpty(txt_RFC.Text)
         && !uc_dt_birth.IsEmpty && uc_dt_birth.DateValue != DateTime.MinValue)
      {
        btn_RFC.Enabled = ProfilePermissions.CheckPermissionList(ProfilePermissions.CashierFormFuncionality.AccountGenerateRFC, ProfilePermissions.TypeOperation.OnlyCheck, this, 0, out _error_str);
      }
      else
      {
        btn_RFC.Enabled = false;
      }
    }

    private Boolean RegisterEntry()
    {
      CustomerVisitsTT _cvtt;

      _cvtt = new CustomerVisitsTT(m_card.AccountId, 0, false);
      return _cvtt.RegisterEntrance();

    }
    private Boolean SaveData(Boolean fromOk = false)
    {

      // It has to be first because of Anti-money Laundering checking
      if (MustShowComments)
      {
        if (!GetScreenDataForOnlyComment())
        {
          return false;
        }
      }
      else
      {
        if (!CheckData(true, fromOk))
        {
          return false;
    }

        GetScreenData();
      }

      if (HasChanges(m_card_read.PlayerTracking, m_card.PlayerTracking))
      {
        if (!SaveCard())
        {
          return false;
        }
        if (m_card_read.AccountId == 0)
        {
          m_card_read.AccountId = m_card.AccountId;
        }

        AuditPlayerData();
      }

      return true;
    }

    #region " AutoSelection "

    private void SetAddressAutoselection()
    {
      Zip.Address _address;

      this.Cursor = Cursors.WaitCursor;

      //gb_address.Visible = false;

      // Disable EventHandler
      ManageEventHandler(false);

      if (!String.IsNullOrEmpty(m_address_modified.Zip)) // Anonymous
      {
        _address = new Zip.Address();

        // Country
        cb_country_new.SelectedValue = m_address_modified.CountryId;
        _address.CountryId = m_address_modified.CountryId;

        // ZIP
        if (!Zip.ValidationZip(_address.CountryId, m_address_modified.Zip))
        {
          lbl_zip_new.ForeColor = Color.Red;
        }
        txt_zip_new.Text = m_address_modified.Zip;
        _address.Zip = m_address_modified.Zip;

        cb_address_03.Enabled = false;
        cb_address_02.Enabled = false;
        cb_city.Enabled = false;

        // Federal entity
        if (ManageComboboxAutoselection(ref cb_fed_entity_new, _address, Zip.CASE_FEDERAL_ENTITY, m_address_modified.FederalEntity, lbl_fed_entity_new, lbl_zip_new, false))
        {
          _address.FederalEntity = m_address_modified.FederalEntity;

          // Delegation
          if (ManageComboboxAutoselection(ref cb_address_03, _address, Zip.CASE_DELEGATION, m_address_modified.Delegation, lbl_address_03_new, lbl_fed_entity_new, false))
          {
            _address.Delegation = m_address_modified.Delegation;

            // Colony
            if (ManageComboboxAutoselection(ref cb_address_02, _address, Zip.CASE_COLONY, m_address_modified.Colony, lbl_address_02_new, lbl_address_03_new, false))
            {
              _address.Colony = m_address_modified.Colony;

              // City
              ManageComboboxAutoselection(ref cb_city, _address, Zip.CASE_CITY, m_address_modified.City, lbl_city_new, lbl_address_02_new, false);
              _address.City = m_address_modified.City;
            }
            else
            {
              cb_city.Text = m_address_modified.City;
            }
          }
          else
          {
            cb_address_02.Text = m_address_modified.Colony;
            cb_city.Text = m_address_modified.City;
          }
        }
        else
        {
          cb_address_03.Text = m_address_modified.Delegation;
          cb_address_02.Text = m_address_modified.Colony;
          cb_city.Text = m_address_modified.City;
        }

        // Street
        txt_address_01_new.Text = m_address_modified.Street;
        _address.Street = m_address_modified.Street;

        // External number
        txt_ext_num_new.Text = m_address_modified.ExtNum;
        _address.ExtNum = m_address_modified.ExtNum;
      }
      else
      {
        txt_zip_new.Text = String.Empty;
        lbl_zip_new.ForeColor = Color.Red;
        cb_address_03.Enabled = false;
        cb_address_02.Enabled = false;
        cb_city.Enabled = false;
        cb_fed_entity_new.Enabled = false;
      }

      // Enable EventHandler
      ManageEventHandler(true);

      //gb_address.Visible = true;

      this.Cursor = Cursors.Default;

    } // SetAddressAutoselection

    private Boolean ManageComboboxAutoselection(ref uc_round_auto_combobox Combobox, Zip.Address Address, String ParameterFilter, String ValueCombo,
                                                Label LabelCombo, Label LabelPrevious, Boolean Manage, Boolean ForceZipFilter = false)
    {
      if (Manage && !m_card.IsNew)
      {
        ManageEventHandler(false);
      }

      if (FillComboboxAutoselection(ref Combobox, Address, ParameterFilter, ForceZipFilter))
      {
        if (m_card.IsNew)
        {
          return true;
        }
        Combobox.SelectedIndex = -1;
        Combobox.Text = ValueCombo;
        if (Manage)
        {
          LabelPrevious.ForeColor = Color.Black;
        }
        if (Combobox.SelectedIndex == -1)
        {
          Combobox.Enabled = (Combobox.Items.Count > 0);
          LabelCombo.ForeColor = Color.Red;
          Combobox.Focus();

          return false;
        }
        else
        {
          LabelCombo.ForeColor = Color.Black;
          Combobox.Enabled = (Combobox.Items.Count > 1);
        }
      }
      else
      {
        LabelPrevious.ForeColor = Color.Red;
        Combobox.Text = ValueCombo;

        return false;
      }

      return true;

    } // ManageComboboxAutoselection

    private void ManageEventHandler(Boolean EnableEventHandler)
    {
      if (EnableEventHandler)
      {
        this.cb_country_new.SelectedIndexChanged += new System.EventHandler(this.cb_country_new_SelectedIndexChanged);
        this.txt_zip_new.TextChanged += new System.EventHandler(this.txt_zip_new_TextChanged);
        this.cb_fed_entity_new.SelectedIndexChanged += new System.EventHandler(this.cb_fed_entity_new_SelectedIndexChanged);
        this.cb_address_03.SelectedIndexChanged += new System.EventHandler(this.cb_address_03_SelectedIndexChanged);
        this.cb_address_02.SelectedIndexChanged += new System.EventHandler(this.cb_address_02_SelectedIndexChanged);
        this.cb_city.SelectedIndexChanged += new System.EventHandler(this.cb_city_SelectedIndexChanged);
        this.txt_address_01_new.TextChanged += new System.EventHandler(this.txt_address_01_new_TextChanged);
        this.txt_ext_num_new.TextChanged += new System.EventHandler(this.txt_ext_num_new_TextChanged);
      }
      else
      {
        this.cb_country_new.SelectedIndexChanged -= new System.EventHandler(this.cb_country_new_SelectedIndexChanged);
        this.txt_zip_new.TextChanged -= new System.EventHandler(this.txt_zip_new_TextChanged);
        this.cb_fed_entity_new.SelectedIndexChanged -= new System.EventHandler(this.cb_fed_entity_new_SelectedIndexChanged);
        this.cb_address_03.SelectedIndexChanged -= new System.EventHandler(this.cb_address_03_SelectedIndexChanged);
        this.cb_address_02.SelectedIndexChanged -= new System.EventHandler(this.cb_address_02_SelectedIndexChanged);
        this.cb_city.SelectedIndexChanged -= new System.EventHandler(this.cb_city_SelectedIndexChanged);
        this.txt_address_01_new.TextChanged -= new System.EventHandler(this.txt_address_01_new_TextChanged);
        this.txt_ext_num_new.TextChanged -= new System.EventHandler(this.txt_ext_num_new_TextChanged);
      }
    } // ManageEventHandler

    private Boolean FillComboboxAutoselection(ref uc_round_auto_combobox Combobox, Zip.Address Address, String ParameterFilter, Boolean ForceZipFilter)
    {
      DataTable _dt;

      _dt = Zip.GetDataTableToAddress(ParameterFilter, Zip.GetFilterToAddress(Address, ForceZipFilter));

      if (_dt.Rows.Count > 0)
      {
        m_combo_charge_dt = true;
        Combobox.DataSource = _dt;
        Combobox.ValueMember = Zip.SQL_ID;
        Combobox.DisplayMember = Zip.SQL_ADDR;
        Combobox.AllowUnlistedValues = false;
        Combobox.FormatValidator = new CharacterTextBox();
        m_combo_charge_dt = false;

        return true;
      }

      Combobox.DataSource = null;
      Combobox.Enabled = false;

      return false;

    } // FillComboboxAutoselection

    private void ReplicateAddress()
    {
      txt_address_01.Text = m_address_modified.Street;
      txt_ext_num.Text = m_address_modified.ExtNum;
      txt_zip.Text = m_address_modified.Zip;
      txt_address_02.Text = m_address_modified.Colony;
      txt_address_03.Text = m_address_modified.Delegation;
      txt_city.Text = m_address_modified.City;
      cb_country.SelectedValue = m_address_modified.CountryId;
      cb_fed_entity.Text = m_address_modified.FederalEntity;
    } // ReplicateAddress

    private void FillStructAddress(ref Zip.Address FillAddress)
    {
      FillAddress.Street = txt_address_01.Text;
      FillAddress.ExtNum = txt_ext_num.Text;
      FillAddress.Zip = txt_zip.Text;
      FillAddress.Colony = txt_address_02.Text;
      FillAddress.Delegation = txt_address_03.Text;
      FillAddress.City = txt_city.Text;
      FillAddress.CountryId = Convert.ToInt32(cb_country.SelectedValue);
      FillAddress.FederalEntity = cb_fed_entity.Text;
    } // FillStructAddress

    #endregion " AutoSelection "

    #endregion

    #region "Handlers"

    private void frm_player_edit_Shown(object sender, EventArgs e)
    {

    } // frm_player_edit_Shown

    private bool GetScreenDataForOnlyComment()
    {
      bool _IsOk = false;

      String error_str;
      ProfilePermissions.CashierFormFuncionality _permission;
      _permission = (m_card.PlayerTracking.CardLevel > 0) ? ProfilePermissions.CashierFormFuncionality.AccountEditPersonal :
                                                            ProfilePermissions.CashierFormFuncionality.AccountEditAnonymous;

      if (ProfilePermissions.CheckPermissionList(_permission, ProfilePermissions.TypeOperation.OnlyCheck, this.ParentForm, 0, out error_str))
      {
        m_card.PlayerTracking.ShowCommentsOnCashier = chk_show_comments.Checked;
        m_card.PlayerTracking.HolderComments = txt_comments.Text;

        _IsOk = true;
      }

      return _IsOk;
    }

    private void btn_ok_Click(object sender, EventArgs e)
    {
      if (SaveData(IsFromReception) && !IsFromReception)
        {
      // hide keyboard and close
      WSIKeyboard.Hide();
      this.DialogResult = DialogResult.OK;
      this.Dispose();

    }

    }

    private void btn_cancel_Click(object sender, EventArgs e)
    {
      if (!MustShowComments)
      {
        DialogResult _question_yes_no;
        String _message;

        _message = Resource.String("STR_FRM_SCAN_CHANGES");
        _message = _message.Replace("\\r\\n", "\r\n");

        // there are changes in Scanned Documents and they will be lost
        if (m_docs_beneficiary_changed || m_docs_holder_changed)
        {
          _question_yes_no = frm_message.Show(_message,
                                              Resource.String("STR_APP_GEN_MSG_WARNING"),
                                              MessageBoxButtons.YesNo,
                                              Images.CashierImage.Question,
                                              this.ParentForm);

          if (_question_yes_no != DialogResult.OK)
          {
            return;
          }

        }
      }

      MustShowComments = false;
      // hide keyboard and close
      WSIKeyboard.Hide();
      this.DialogResult = DialogResult.Cancel;
      this.Dispose();
    }

    private void btn_keyboard_Click(object sender, EventArgs e)
    {
      // show keyboard (if hidden)
      WSIKeyboard.ForceToggle();
    }

    private void timer1_Tick(object sender, EventArgs e)
    {
      // Check if data is correct
      CheckData(false);
    }

    private void frm_player_edit_KeyDown(object sender, KeyEventArgs e)
    {
      m_send_tab = false;

      if (e.KeyCode == Keys.Enter)
      {
        // For txt_comments, that is a multi-line TextBox, enter does not act as a TAB key.
        if (m_control != null && m_control.Equals(txt_comments))
        {
          return;
        }
        m_send_tab = true;
        SendKeys.Send("{TAB}");
      }
    }

    void txt_comments_Enter(object sender, System.EventArgs e)
    {
      m_control = txt_comments;
    }
    void txt_email_02_Enter(object sender, System.EventArgs e)
    {
      m_send_tab = false;
      m_control = txt_email_02;
    }
    void txt_zip_Enter(object sender, System.EventArgs e)
    {
      m_send_tab = false;
      m_control = txt_zip;
    }

    void control_Leave(object sender, System.EventArgs e)
    {
      if (m_control == null)
      {
        return;
      }

      if (m_send_tab)
      {
        m_send_tab = false;

        if (m_control.Equals(txt_email_02))
        {
          tab_player_edit.SelectedIndex = 2;
          txt_address_01.Focus();
        }
      }

      m_control = null;
    }

    private void btn_generate_random_pin_Click(object sender, EventArgs e)
    {
      Cursor _previous_cursor;

      _previous_cursor = this.Cursor;

      try
      {
        this.Cursor = Cursors.WaitCursor;

        pin_save("", true);
      }

      finally
      {
        this.Cursor = _previous_cursor;
      }
    }

    private void btn_save_pin_Click(object sender, EventArgs e)
    {
      Cursor _previous_cursor;

      _previous_cursor = this.Cursor;

      try
      {
        this.Cursor = Cursors.WaitCursor;

        if (!txt_pin.ValidateFormat())
        {
          lbl_pin_msg.Visible = true;

          lbl_pin_msg.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_MSG_BLINK_GENERIC_FORMAT", lbl_pin.Text);
          lbl_pin_msg.ForeColor = Color.Red;

          return;
        }

        if (txt_pin.Text.Length < WSI.Common.Accounts.PIN_MIN_LENGTH)
        {
          lbl_pin_msg.Visible = true;

          lbl_pin_msg.Text = Resource.String("STR_FRM_CHANGE_PIN_LENGTH_ERROR", WSI.Common.Accounts.PIN_MIN_LENGTH);
          lbl_pin_msg.ForeColor = Color.Red;

          return;
        }

        if (txt_pin.Text != txt_confirm_pin.Text)
        {
          lbl_pin_msg.Visible = true;
          lbl_pin_msg.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_CONFIRMATION_PIN_DIFFERS");
          lbl_pin_msg.ForeColor = Color.Red;

          return;
        }

        pin_save(txt_pin.Text, false);
      }

      finally
      {
        this.Cursor = _previous_cursor;
      }
    }

    private void txt_pin_Enter(object sender, EventArgs e)
    {
      txt_pin.Text = "";
      txt_confirm_pin.Text = "";
    }

    private void txt_confirm_pin_Enter(object sender, EventArgs e)
    {
      txt_confirm_pin.Text = "";
    }

    private void txt_pin_TextChanged(object sender, EventArgs e)
    {
      btn_save_pin_enable();
    }

    private void txt_confirm_pin_TextChanged(object sender, EventArgs e)
    {
      btn_save_pin_enable();
    }

    private void txt_name_or_last_names_TextChanged(object sender, EventArgs e)
    {
      String _aux_full_name;

      _aux_full_name = CardData.FormatFullName(FULL_NAME_TYPE.ACCOUNT, txt_name.Text, txt_name4.Text, txt_last_name1.Text, txt_last_name2.Text);
      this.lbl_full_name.Text = _aux_full_name.Trim();
      FormSubTitle = " " + m_card.AccountId + " - " + _aux_full_name.Trim();
      // DHA 09-MAR-2015
      EnableRfcButton();
    }

    private void btn_scan_docs_Click(object sender, EventArgs e)
    {
      m_scan_tile = String.Format("- {0}", Resource.String("STR_FRM_SCAN_HOLDER"));
      ScanId(ACCOUNT_SCANNED_OWNER.HOLDER, m_scan_tile, m_card);
    }

    private void btn_ben_scan_docs_Click(object sender, EventArgs e)
    {
      m_scan_tile = String.Format("- {0}", Resource.String("STR_FRM_PLAYER_EDIT_BENEFICIARY"));
      ScanId(ACCOUNT_SCANNED_OWNER.BENEFICIARY, m_scan_tile, m_card);
    }

    private void btn_print_information_Click(object sender, EventArgs e)
    {
      DialogResult _dlg_rc;

      this.m_print_information = true;

      _dlg_rc = SavingCardConfirmation(true, true, Resource.String("STR_FRM_ACCOUNT_USER_EDIT_PRINT_DATA"));

      if (_dlg_rc == DialogResult.Cancel)
      {
        this.m_print_information = false;

        return;
      }

      if (_dlg_rc == DialogResult.OK)
      {
        // Data Ok
        if (!CheckData(true))
        {
          this.m_print_information = false;
          return;
        }

        // Save
        if (!SaveCard())
        {
          this.m_print_information = false;
          return;
        }

        // Audit
        AuditPlayerData();

        DuplicateCard();
      }

      //Print Data
      PrintData();

      lbl_msg_blink.Visible = false;

      this.m_print_information = false;
    }

    private void PrintData()
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        VoucherCardHolderUpdate _voucher;
        _voucher = new VoucherCardHolderUpdate(m_card, PrintMode.Print, _db_trx.SqlTransaction);
        _voucher.AddString("VOUCHER_CARD_HOLDERNAME", m_card.PlayerTracking.HolderName);
        VoucherPrint.Print(_voucher);
      }
    }

    private void txt_beneficiary_name_or_last_names_TextChanged(object sender, EventArgs e)
    {
      String _aux_full_name;

      _aux_full_name = CardData.FormatFullName(FULL_NAME_TYPE.ACCOUNT, txt_beneficiary_name.Text, txt_beneficiary_last_name1.Text, txt_beneficiary_last_name2.Text);
      this.lbl_beneficiary_full_name.Text = _aux_full_name.Trim();
    }

    private void CleanBeneficiaryData()
    {
      txt_beneficiary_last_name1.Text = "";
      txt_beneficiary_last_name2.Text = "";
      txt_beneficiary_name.Text = "";
      txt_beneficiary_RFC.Text = "";
      txt_beneficiary_CURP.Text = "";
      uc_dt_ben_birth.DateValue = DateTime.MinValue;
      cb_beneficiary_gender.SelectedValue = 0;
      cb_beneficiary_occupation.SelectedValue = 0;

    } // CleanBeneficiaryData

    private void RecoverEditedBeneficiaryData()
    {
      if (m_beneficiary_data != null)
      {
        // Get beneficiary last_name1
        if (m_beneficiary_data.ContainsKey("txt_beneficiary_last_name1"))
        {
          txt_beneficiary_last_name1.Text = (String)m_beneficiary_data["txt_beneficiary_last_name1"];
        }

        // Get beneficiary last_name2
        if (m_beneficiary_data.ContainsKey("txt_beneficiary_last_name2"))
        {
          txt_beneficiary_last_name2.Text = (String)m_beneficiary_data["txt_beneficiary_last_name2"];
        }

        // Get beneficiary name
        if (m_beneficiary_data.ContainsKey("txt_beneficiary_name"))
        {
          txt_beneficiary_name.Text = (String)m_beneficiary_data["txt_beneficiary_name"];
        }

        // Get beneficiary RFC
        if (m_beneficiary_data.ContainsKey("txt_beneficiary_RFC"))
        {
          txt_beneficiary_RFC.Text = (String)m_beneficiary_data["txt_beneficiary_RFC"];
        }

        // Get beneficiary CURP
        if (m_beneficiary_data.ContainsKey("txt_beneficiary_CURP"))
        {
          txt_beneficiary_CURP.Text = (String)m_beneficiary_data["txt_beneficiary_CURP"];
        }

        // Get beneficiary birth day
        if (m_beneficiary_data.ContainsKey("uc_dt_ben_birth"))
        {
          uc_dt_ben_birth.DateValue = (DateTime)m_beneficiary_data["uc_dt_ben_birth"];
        }

        // Get beneficiary gender
        if (m_beneficiary_data.ContainsKey("cb_beneficiary_gender"))
        {
          cb_beneficiary_gender.SelectedValue = (GENDER)m_beneficiary_data["cb_beneficiary_gender"];
        }

        // Get beneficiary gender
        if (m_beneficiary_data.ContainsKey("cb_beneficiary_occupation"))
        {
          cb_beneficiary_occupation.SelectedValue = (GENDER)m_beneficiary_data["cb_beneficiary_occupation"];
        }
      }
    } // RecoverEditedBeneficiaryData

    private void chk_holder_has_beneficiary_CheckedChanged(object sender, EventArgs e)
    {
      if (!this.chk_holder_has_beneficiary.Checked)
      {
        CleanBeneficiaryData();
      }
      else
      {
        RecoverEditedBeneficiaryData();
      }

      btn_ben_scan_docs.Enabled = this.chk_holder_has_beneficiary.Checked;
      lbl_beneficiary_last_name1.Enabled = this.chk_holder_has_beneficiary.Checked;
      txt_beneficiary_last_name1.Enabled = this.chk_holder_has_beneficiary.Checked;
      lbl_beneficiary_last_name2.Enabled = this.chk_holder_has_beneficiary.Checked;
      txt_beneficiary_last_name2.Enabled = this.chk_holder_has_beneficiary.Checked;
      txt_beneficiary_name.Enabled = this.chk_holder_has_beneficiary.Checked;
      lbl_beneficiary_name.Enabled = this.chk_holder_has_beneficiary.Checked;
      lbl_beneficiary_full_name.Enabled = this.chk_holder_has_beneficiary.Checked;
      lbl_beneficiary_RFC.Enabled = this.chk_holder_has_beneficiary.Checked;
      txt_beneficiary_RFC.Enabled = this.chk_holder_has_beneficiary.Checked;
      lbl_beneficiary_CURP.Enabled = this.chk_holder_has_beneficiary.Checked;
      txt_beneficiary_CURP.Enabled = this.chk_holder_has_beneficiary.Checked;
      lbl_beneficiary_occupation.Enabled = this.chk_holder_has_beneficiary.Checked;
      lbl_beneficiary_gender.Enabled = this.chk_holder_has_beneficiary.Checked;
      cb_beneficiary_gender.Enabled = this.chk_holder_has_beneficiary.Checked;
      lbl_beneficiary_birth_date.Enabled = this.chk_holder_has_beneficiary.Checked;
      uc_dt_ben_birth.Enabled = this.chk_holder_has_beneficiary.Checked;
      cb_beneficiary_occupation.Enabled = this.chk_holder_has_beneficiary.Checked;
    }

    private void txt_date_TextChanged(object sender, EventArgs e)
    {
      TextBox _tag;

      _tag = (TextBox)sender;

      if (_tag.Text.Length < _tag.MaxLength)
      {
        return;
      }

      Control nextInTabOrder = GetNextControl((Control)sender, true);
      nextInTabOrder.Focus();

    }

    private void frm_player_edit_Load(object sender, EventArgs e)
    {
      SetFieldsVisibility();
      m_rfc_visible = IdentificationTypes.IsIdentificationEnabled(ACCOUNT_HOLDER_ID_TYPE.RFC);
      m_curp_visible = IdentificationTypes.IsIdentificationEnabled(ACCOUNT_HOLDER_ID_TYPE.CURP);

    } // frm_player_edit_Load

    private void uc_dt_birth_OnDateChanged(object sender, EventArgs e)
    {
      EnableRfcButton();
    } // uc_dt_birth_OnDateChanged

    private void txt_RFC_TextChanged(object sender, EventArgs e)
    {
      EnableRfcButton();
    } // txt_RFC_TextChanged

    private void btn_RFC_Click(object sender, EventArgs e)
    {
      String _error_str;

      if (ProfilePermissions.CheckPermissionList(ProfilePermissions.CashierFormFuncionality.AccountGenerateRFC, ProfilePermissions.TypeOperation.OnlyCheck, this, 0, out _error_str))
      {
        txt_RFC.Text = WSI.Common.ValidateFormat.ExpectedRFC(txt_last_name1.Text, txt_last_name2.Text, txt_name.Text, uc_dt_birth.DateValue);
      }
    } // btn_RFC_Click

    private void btn_id_card_scan_Click(object sender, EventArgs e)
    {
      IDCardScannerHelper.SettingsOpenDialog();
    }

    private void frm_player_edit_FormClosing(object sender, FormClosingEventArgs e)
    {
      IDCardScanner.Snapshell2.StopCheckScan();
    }

    private void cb_country_SelectedIndexChanged(object sender, EventArgs e)
    {
      LoadFedEntitiesCombo();
    } // cb_country_SelectedIndexChanged

    private void m_img_photo_Click(object sender, EventArgs e)
    {
      bool _exit;
      _exit = false;
      while (!_exit)
      {
        try
        {
          WSI.IDCamera.CameraModule _camera_module;
          Image _img;

          _camera_module = new WSI.IDCamera.CameraModule(this,typeof(frmCapturePhoto),typeof(SelectDevice));
          _img = _camera_module.GetImage();
          if (_img != null)
          {
            m_img_photo.Image = _img;
            m_img_photo.Tag = 1;
            lbl_no_photo.Visible = false;
          }
          _exit = true;
        }
        catch (CameraDeviceNotAvaiableException _ex)
        {
          Log.Error(string.Format("Camera missing error in {0}", this.Name));
          Log.Exception(_ex);
          frm_message.Show(Resource.String("STR_CAMERA_MISSING_MESSAGE_TEXT"), Resource.String("STR_CAMERA_MISSING_MESSAGE_CAPTION"), MessageBoxButtons.OK, this);
          _exit = true;
        }
        catch (NoCameraDevicesAvailable _ex)
        {
          Log.Error(string.Format("No camera devices available {0}", this.Name));
          Log.Exception(_ex);
          frm_message.Show(Resource.String("STR_CAMERA_NO_DEVICES_MESSAGE_TEXT"), Resource.String("STR_CAMERA_NO_DEVICES_MESSAGE_CAPTION"), MessageBoxButtons.OK, this);
          _exit = true;
        }
        catch (CameraDeviceTimeoutException _ex)
        {
          Log.Error(string.Format("Camera Timeout {0}", this.Name));
          Log.Exception(_ex);
          frm_message.Show(Resource.String("STR_CAMERA_TIMEOUT_MESSAGE_TEXT"), Resource.String("STR_CAMERA_TIMEOUT_MESSAGE_CAPTION"), MessageBoxButtons.OK, this);
          _exit = true;
        }
        catch (Exception _ex)
        {
          Log.Error(string.Format("Unspecified Exception in {0}", this.Name));
          Log.Exception(_ex);
          _exit = true;
        }
      }//m_img_photo_Click
    }

    private void btn_reg_entry_Click(object sender, EventArgs e)
    {
      if (SaveData())
      {
        if (RegisterEntry())
        {
          // hide keyboard and close
          WSIKeyboard.Hide();
          this.DialogResult = DialogResult.OK;
          this.Dispose();
        }
      
      }
    }// btn_reg_entry_Click
    
    #region " Address New "

    private void chk_address_mode_CheckedChanged(object sender, EventArgs e)
    {
      tlp_address.Visible = chk_address_mode.Checked;
      tlp_address_new.Visible = !chk_address_mode.Checked;

      if (!chk_address_mode.Checked)
      {
        FillStructAddress(ref m_address_modified);
        SetAddressAutoselection();
      }
      else
      {
        ReplicateAddress();
      }
    } // chk_address_mode_CheckedChanged

    private void cb_country_new_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (m_combo_charge_dt)
      {
        return;
      }

      m_address_modified.CountryId = Convert.ToInt32(cb_country_new.SelectedValue);

      txt_zip_new_TextChanged(sender, e);

    } // cb_country_new_SelectedIndexChanged

    private void txt_zip_new_TextChanged(object sender, EventArgs e)
    {
      Zip.Address _address;
      Boolean _force_zip_filter;

      m_address_modified.Zip = txt_zip_new.Text;
      _address = m_address_modified;
      _force_zip_filter = false;

      cb_city.Enabled = false;
      cb_address_02.Enabled = false;
      cb_address_03.Enabled = false;
      cb_fed_entity_new.Enabled = false;

      if (String.IsNullOrEmpty(txt_zip_new.Text))
      {
        lbl_zip_new.ForeColor = Color.Red;
        _force_zip_filter = true;
      }
      else if (!Zip.ValidationZip(m_address_modified.CountryId, m_address_modified.Zip))
      {
        lbl_zip_new.ForeColor = Color.Red;
      }
      else
      {
        lbl_zip_new.ForeColor = Color.Black;
      }

      _address.City = String.Empty;
      _address.Colony = String.Empty;
      _address.Delegation = String.Empty;
      _address.FederalEntity = String.Empty;

      if (ManageComboboxAutoselection(ref cb_fed_entity_new, _address, Zip.CASE_FEDERAL_ENTITY, m_address_modified.FederalEntity, lbl_fed_entity_new, lbl_zip_new, true, _force_zip_filter))
      {
        cb_fed_entity_new_SelectedIndexChanged(sender, e);
      }
      else
      {
        ManageEventHandler(false);
        ManageEventHandler(true);
      }
    } // txt_zip_new_TextChanged

    private void cb_fed_entity_new_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (m_combo_charge_dt)
      {
        return;
      }

      Zip.Address _address;

      ManageEventHandler(false);

      m_address_modified.FederalEntity = cb_fed_entity_new.Text;
      _address = m_address_modified;

      cb_city.Enabled = false;
      cb_address_02.Enabled = false;
      cb_address_03.Enabled = false;

      _address.City = String.Empty;
      _address.Colony = String.Empty;
      _address.Delegation = String.Empty;

      cb_fed_entity_new.Enabled = (cb_fed_entity_new.Items.Count > 1);

      if (ManageComboboxAutoselection(ref cb_address_03, _address, Zip.CASE_DELEGATION, m_address_modified.Delegation, lbl_address_03_new, lbl_fed_entity_new, true))
      {
        cb_address_03_SelectedIndexChanged(sender, e);
      }

      ManageEventHandler(false);
      ManageEventHandler(true);

    } // cb_fed_entity_new_SelectedIndexChanged

    private void cb_address_03_SelectedIndexChanged(object sender, EventArgs e)
    {

      if (m_combo_charge_dt)
      {
        return;
      }

      Zip.Address _address;

      ManageEventHandler(false);

      m_address_modified.Delegation = cb_address_03.Text;
      _address = m_address_modified;

      cb_city.Enabled = false;
      cb_address_02.Enabled = false;

      _address.City = String.Empty;
      _address.Colony = String.Empty;

      cb_address_03.Enabled = (cb_address_03.Items.Count > 1);

      if (ManageComboboxAutoselection(ref cb_address_02, _address, Zip.CASE_COLONY, m_address_modified.Colony, lbl_address_02_new, lbl_address_03_new, true))
      {
        cb_address_02_SelectedIndexChanged(sender, e);
      }

      ManageEventHandler(false);
      ManageEventHandler(true);

    } // cb_address_03_SelectedIndexChanged

    private void cb_address_02_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (m_combo_charge_dt)
      {
        return;
      }

      Zip.Address _address;

      ManageEventHandler(false);

      m_address_modified.Colony = cb_address_02.Text;
      _address = m_address_modified;

      cb_city.Enabled = false;

      _address.City = String.Empty;

      cb_address_02.Enabled = (cb_address_02.Items.Count > 1);

      if (ManageComboboxAutoselection(ref cb_city, _address, Zip.CASE_CITY, m_address_modified.City, lbl_city_new, lbl_address_02_new, true))
      {
        cb_city_SelectedIndexChanged(sender, e);
      }

      ManageEventHandler(false);
      ManageEventHandler(true);

    } // cb_address_02_SelectedIndexChanged

    private void cb_city_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (m_combo_charge_dt)
      {
        return;
      }

      m_address_modified.City = cb_city.Text;

      if (ManageComboboxAutoselection(ref cb_city, m_address_modified, Zip.CASE_CITY, m_address_modified.City, lbl_city_new, lbl_address_02_new, true))
      {
        ManageEventHandler(false);
        ManageEventHandler(true);
      }
    } // cb_city_SelectedIndexChanged

    private void txt_address_01_new_TextChanged(object sender, EventArgs e)
    {
      m_address_modified.Street = txt_address_01_new.Text;
    } // txt_address_01_new_TextChanged

    private void txt_ext_num_new_TextChanged(object sender, EventArgs e)
    {
      m_address_modified.ExtNum = txt_ext_num_new.Text;
    } // txt_ext_num_new_TextChanged

    #endregion " Address New "

    #endregion
  }
}
