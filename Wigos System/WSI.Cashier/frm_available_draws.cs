//------------------------------------------------------------------------------
// Copyright � 2011 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_available_draws.cs
// 
//   DESCRIPTION: Implements the following classes:
//                1. frm_available_draws
//
//        AUTHOR: RCI
// 
// CREATION DATE: 16-JUN-2011
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 16-JUN-2011 RCI     First release.
// 12-NOV-2015 RGR     Style change.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using WSI.Common;
using WSI.Cashier.Controls;

namespace WSI.Cashier
{
  public partial class frm_available_draws : frm_base
  {
    private const Int32 GRID_COLUMN_DRAW_ID = 0;
    private const Int32 GRID_COLUMN_DRAW_NAME = 1;
    private const Int32 GRID_COLUMN_TOTAL_NUMBERS = 2;
    // Real Pending Numbers are the numbers that the player has rights to have.
    // But it's possible that there are less numbers available in the draw (this is Pending Numbers).
    // String Pending Numbers: Used to show Pending Numbers as String. Needed as String because can contain the symbol '*'.
    private const Int32 GRID_COLUMN_PENDING_NUMBERS = 3;
    private const Int32 GRID_COLUMN_REAL_PENDING_NUMBERS = 4;
    private const Int32 GRID_COLUMN_STRING_PENDING_NUMBERS = 5;
    private const Int32 GRID_COLUMN_PRINT_FLAG = 6;
    private const Int32 GRID_COLUMN_PRINT_FLAG_STRING = 7;

    #region Attributes

    private DataTable m_dt_draws = null;
    private Boolean m_print_selected = false;

    #endregion

    #region Constructor

    /// <summary>
    /// Constructor method.
    /// 1. Initialize controls.
    /// 2. Initialize control�s resources. (NLS, Images, etc)
    /// </summary>
    public frm_available_draws()
    {
      InitializeComponent();

      InitializeControlResources();
    }

    #endregion

    #region Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Initialize Available Draws Data Grid.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private void InitAvailableDrawsDataGrid()
    {
      // Available Draws DataGrid
      dgv_draws.ColumnHeadersHeight = 30;
      dgv_draws.RowTemplate.Height = 30;
      dgv_draws.RowTemplate.DividerHeight = 0;

      //    - Draw Id
      dgv_draws.Columns[GRID_COLUMN_DRAW_ID].Width = 0;
      dgv_draws.Columns[GRID_COLUMN_DRAW_ID].Visible = false;

      //    - Draw Name
      dgv_draws.Columns[GRID_COLUMN_DRAW_NAME].Width = 350;
      dgv_draws.Columns[GRID_COLUMN_DRAW_NAME].HeaderCell.Value = Resource.String("STR_FRM_AVAILABLE_DRAWS_001");
      dgv_draws.Columns[GRID_COLUMN_DRAW_NAME].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

      //    - Total Numbers
      dgv_draws.Columns[GRID_COLUMN_TOTAL_NUMBERS].Width = 125;
      dgv_draws.Columns[GRID_COLUMN_TOTAL_NUMBERS].HeaderCell.Value = Resource.String("STR_FRM_AVAILABLE_DRAWS_002");
      dgv_draws.Columns[GRID_COLUMN_TOTAL_NUMBERS].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

      //    - Pending Numbers
      dgv_draws.Columns[GRID_COLUMN_PENDING_NUMBERS].Width = 0;
      dgv_draws.Columns[GRID_COLUMN_PENDING_NUMBERS].Visible = false;

      //    - Real Pending Numbers
      dgv_draws.Columns[GRID_COLUMN_REAL_PENDING_NUMBERS].Width = 0;
      dgv_draws.Columns[GRID_COLUMN_REAL_PENDING_NUMBERS].Visible = false;

      //    - String Pending Numbers: Used to show Pending Numbers as String.
      //      Needed as String because can contain the symbol '*'.
      dgv_draws.Columns[GRID_COLUMN_STRING_PENDING_NUMBERS].Width = 100;
      dgv_draws.Columns[GRID_COLUMN_STRING_PENDING_NUMBERS].HeaderCell.Value = Resource.String("STR_FRM_AVAILABLE_DRAWS_003");
      dgv_draws.Columns[GRID_COLUMN_STRING_PENDING_NUMBERS].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

      //    - Print Flag
      dgv_draws.Columns[GRID_COLUMN_PRINT_FLAG].Width = 0;
      dgv_draws.Columns[GRID_COLUMN_PRINT_FLAG].Visible = false;

      //    - Print Flag String (Yes or No)
      dgv_draws.Columns[GRID_COLUMN_PRINT_FLAG_STRING].Width = 90;
      dgv_draws.Columns[GRID_COLUMN_PRINT_FLAG_STRING].HeaderCell.Value = Resource.String("STR_FRM_AVAILABLE_DRAWS_004");
      dgv_draws.Columns[GRID_COLUMN_PRINT_FLAG_STRING].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

    } // InitAvailableDrawsDataGrid

    private void FillDrawDataTable(DrawNumberList DrawNumberList)
    {
      DataRow _row;

      m_dt_draws.Clear();

      foreach (DrawNumbers _draw_number in DrawNumberList)
      {
        _row = m_dt_draws.NewRow();
        _row["Id"] = _draw_number.Draw.DrawId;
        _row["Name"] = _draw_number.Draw.DrawName;
        _row["Total"] = _draw_number.TotalAwardedNumbers;
        _row["Pending"] = _draw_number.PendingNumbers;
        _row["RealPending"] = _draw_number.RealPendingNumbers;
        _row["PrintFlag"] = (_draw_number.PendingNumbers > 0);
        m_dt_draws.Rows.Add(_row);
      }
    } // FillDrawDataTable

    private void EnablePrintButton()
    {
      Boolean _enable_button;

      _enable_button = false;

      for (Int32 _idx_row = 0; _idx_row < dgv_draws.Rows.Count; _idx_row++)
      {
        if ((Boolean)dgv_draws.Rows[_idx_row].Cells[GRID_COLUMN_PRINT_FLAG].Value)
        {
          _enable_button = true;
        }
      }

      btn_print_selected.Enabled = _enable_button;
    } // EnablePrintButton

    private void FillDrawNumberList(DataTable TableDraws, ref DrawNumberList AwardedNumbersPerDraw)
    {
      foreach (DataRow _row in TableDraws.Rows)
      {
        if (!(Boolean)_row["PrintFlag"])
        {
          AwardedNumbersPerDraw.Remove((Int64)_row["Id"]);
        }
      }
    } // FillDrawNumberList

    #endregion

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Initialize control resources (NLS strings, images, etc).
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public void InitializeControlResources()
    {
      // - NLS Strings:
      //   - Title:
      this.FormTitle = Resource.String("STR_FRM_AVAILABLE_DRAWS_TITLE");
      //   - Buttons
      btn_close.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CLOSE");
      btn_print_selected.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_PRINT");
      lbl_no_numbers_left.Text = Resource.String("STR_FRM_AVAILABLE_DRAWS_007");

      if (m_dt_draws == null)
      {
        m_dt_draws = new DataTable("DRAWS");
        m_dt_draws.Columns.Add("Id", Type.GetType("System.Int64"));
        m_dt_draws.Columns.Add("Name", Type.GetType("System.String"));
        m_dt_draws.Columns.Add("Total", Type.GetType("System.Int64"));
        m_dt_draws.Columns.Add("Pending", Type.GetType("System.Int64"));
        m_dt_draws.Columns.Add("RealPending", Type.GetType("System.Int64"));
        m_dt_draws.Columns.Add("StringPending", Type.GetType("System.String"));
        m_dt_draws.Columns.Add("PrintFlag", Type.GetType("System.Boolean"));
        m_dt_draws.Columns.Add("PrintFlagString", Type.GetType("System.String")).DefaultValue = "";

        m_dt_draws.PrimaryKey = new DataColumn[] { m_dt_draws.Columns["Id"] };
      }
    } // InitializeControlResources

    //------------------------------------------------------------------------------
    // PURPOSE : Init and Show the form.
    //
    //  PARAMS :
    //      - INPUT :
    //          - DrawNumberList AwardedNumbersPerDraw
    //          - Form Parent
    //
    //      - OUTPUT :
    //          - DrawNumberList AwardedNumbersPerDraw
    //
    // RETURNS :
    //
    //   NOTES :
    //

    public Boolean Show(ref DrawNumberList AwardedNumbersPerDraw, Form Parent)
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_available_draw", Log.Type.Message);
      Boolean _enable_button;
      Boolean _show_message_no_numbers_left;
      Int64 _pending_numbers;
      Int64 _real_pending_numbers;

      FillDrawDataTable(AwardedNumbersPerDraw);
      dgv_draws.DataSource = m_dt_draws;
      dgv_draws.Sort(dgv_draws.Columns[0], ListSortDirection.Ascending);

      InitAvailableDrawsDataGrid();

      _enable_button = false;
      _show_message_no_numbers_left = false;

      for (Int32 _idx_row = 0; _idx_row < dgv_draws.Rows.Count; _idx_row++)
      {
        if ((Boolean)dgv_draws.Rows[_idx_row].Cells[GRID_COLUMN_PRINT_FLAG].Value)
        {
          dgv_draws.Rows[_idx_row].Cells[GRID_COLUMN_PRINT_FLAG_STRING].Value = Resource.String("STR_FRM_AVAILABLE_DRAWS_005");
          _enable_button = true;
        }
        else
        {
          dgv_draws.Rows[_idx_row].Cells[GRID_COLUMN_PRINT_FLAG_STRING].Value = Resource.String("STR_FRM_AVAILABLE_DRAWS_006");
        }

        _pending_numbers = (Int64)dgv_draws.Rows[_idx_row].Cells[GRID_COLUMN_PENDING_NUMBERS].Value;
        _real_pending_numbers = (Int64)dgv_draws.Rows[_idx_row].Cells[GRID_COLUMN_REAL_PENDING_NUMBERS].Value;

        dgv_draws.Rows[_idx_row].Cells[GRID_COLUMN_STRING_PENDING_NUMBERS].Value = _pending_numbers.ToString();
        if (_pending_numbers != _real_pending_numbers)
        {
          dgv_draws.Rows[_idx_row].Cells[GRID_COLUMN_STRING_PENDING_NUMBERS].Value += " *";

          _show_message_no_numbers_left = true;
        }
      }

      btn_print_selected.Enabled = _enable_button;
      lbl_no_numbers_left.Visible = _show_message_no_numbers_left;

      this.ShowDialog(Parent);

      // If button 'Print Selected' is NOT clicked, return false.
      if (!m_print_selected)
      {
        return false;
      }

      FillDrawNumberList(m_dt_draws, ref AwardedNumbersPerDraw);

      return true;
    } // Show

    #endregion

    #region DataGridEvents

    private void dgv_draws_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
    {
      dgv_draws.Columns[GRID_COLUMN_DRAW_ID].Width = 0;
      dgv_draws.Columns[GRID_COLUMN_DRAW_ID].Visible = false;
      dgv_draws.Columns[GRID_COLUMN_PENDING_NUMBERS].Width = 0;
      dgv_draws.Columns[GRID_COLUMN_PENDING_NUMBERS].Visible = false;
      dgv_draws.Columns[GRID_COLUMN_REAL_PENDING_NUMBERS].Width = 0;
      dgv_draws.Columns[GRID_COLUMN_REAL_PENDING_NUMBERS].Visible = false;
      dgv_draws.Columns[GRID_COLUMN_PRINT_FLAG].Width = 0;
      dgv_draws.Columns[GRID_COLUMN_PRINT_FLAG].Visible = false;

      //SetRowColor(e.RowIndex);
    } // dgv_draws_RowPrePaint

    private void dgv_draws_CellClick(object sender, DataGridViewCellEventArgs e)
    {
      Boolean _print_flag;

      if (e.RowIndex < 0)
      {
        return;
      }
      if ((Int64)dgv_draws.Rows[e.RowIndex].Cells[GRID_COLUMN_PENDING_NUMBERS].Value > 0)
      {
        _print_flag = !(Boolean)dgv_draws.Rows[e.RowIndex].Cells[GRID_COLUMN_PRINT_FLAG].Value;
        dgv_draws.Rows[e.RowIndex].Cells[GRID_COLUMN_PRINT_FLAG].Value = _print_flag;
        if (_print_flag)
        {
          dgv_draws.Rows[e.RowIndex].Cells[GRID_COLUMN_PRINT_FLAG_STRING].Value = Resource.String("STR_FRM_AVAILABLE_DRAWS_005");
        }
        else
        {
          dgv_draws.Rows[e.RowIndex].Cells[GRID_COLUMN_PRINT_FLAG_STRING].Value = Resource.String("STR_FRM_AVAILABLE_DRAWS_006");
        }

        //SetRowColor(e.RowIndex);

        EnablePrintButton();
      }
    } // dgv_draws_CellClick

    #endregion DataGridEvents

    #region Buttons

    //------------------------------------------------------------------------------
    // PURPOSE : Handle the click on the Exit button.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //

    private void btn_exit_Click(object sender, EventArgs e)
    {
      m_print_selected = false;

      Misc.WriteLog("[FORM CLOSE] frm_available_draws (exit)", Log.Type.Message);
      this.Close();
    } // btn_exit_Click

    private void btn_print_selected_Click(object sender, EventArgs e)
    {
      m_print_selected = true;

      Misc.WriteLog("[FORM CLOSE] frm_available_draws (print_selected)", Log.Type.Message);
      this.Close();
    } // btn_print_selected_Click

    private void OnShown(object sender, EventArgs e)
    {
      this.dgv_draws.Focus();
    }
    #endregion Buttons

  } // frm_available_draws

} // WSI.Cashier
