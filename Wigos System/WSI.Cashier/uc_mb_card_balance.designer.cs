namespace WSI.Cashier
{
  partial class uc_mb_card_balance
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(uc_mb_card_balance));
      this.rp_panel1 = new WSI.Cashier.Controls.uc_round_panel();
      this.lbl_splitline = new System.Windows.Forms.Label();
      this.pictureBox2 = new System.Windows.Forms.PictureBox();
      this.lbl_mb_session_closed = new WSI.Cashier.Controls.uc_label();
      this.lbl_available_value_1 = new WSI.Cashier.Controls.uc_label();
      this.lbl_pending_cash_name = new WSI.Cashier.Controls.uc_label();
      this.lbl_pending_cash_value = new WSI.Cashier.Controls.uc_label();
      this.lbl_available_name_1 = new WSI.Cashier.Controls.uc_label();
      this.lbl_cash_in_name = new WSI.Cashier.Controls.uc_label();
      this.lbl_excess_value = new WSI.Cashier.Controls.uc_label();
      this.lbl_deposit_name_2 = new WSI.Cashier.Controls.uc_label();
      this.lbl_mb_excess = new WSI.Cashier.Controls.uc_label();
      this.lbl_cash_lost_name = new WSI.Cashier.Controls.uc_label();
      this.lbl_cash_in_value = new WSI.Cashier.Controls.uc_label();
      this.lbl_deposit_value_2 = new WSI.Cashier.Controls.uc_label();
      this.lbl_cash_lost_value = new WSI.Cashier.Controls.uc_label();
      this.lbl_separator_1 = new WSI.Cashier.Controls.uc_label();
      this.rp_panel1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
      this.SuspendLayout();
      // 
      // rp_panel1
      // 
      this.rp_panel1.BackColor = System.Drawing.Color.Transparent;
      this.rp_panel1.Controls.Add(this.lbl_splitline);
      this.rp_panel1.Controls.Add(this.pictureBox2);
      this.rp_panel1.Controls.Add(this.lbl_mb_session_closed);
      this.rp_panel1.Controls.Add(this.lbl_available_value_1);
      this.rp_panel1.Controls.Add(this.lbl_pending_cash_name);
      this.rp_panel1.Controls.Add(this.lbl_pending_cash_value);
      this.rp_panel1.Controls.Add(this.lbl_available_name_1);
      this.rp_panel1.Controls.Add(this.lbl_cash_in_name);
      this.rp_panel1.Controls.Add(this.lbl_excess_value);
      this.rp_panel1.Controls.Add(this.lbl_deposit_name_2);
      this.rp_panel1.Controls.Add(this.lbl_mb_excess);
      this.rp_panel1.Controls.Add(this.lbl_cash_lost_name);
      this.rp_panel1.Controls.Add(this.lbl_cash_in_value);
      this.rp_panel1.Controls.Add(this.lbl_deposit_value_2);
      this.rp_panel1.Controls.Add(this.lbl_cash_lost_value);
      this.rp_panel1.Controls.Add(this.lbl_separator_1);
      this.rp_panel1.CornerRadius = 10;
      this.rp_panel1.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.rp_panel1.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.rp_panel1.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.rp_panel1.HeaderHeight = 0;
      this.rp_panel1.HeaderSubText = null;
      this.rp_panel1.HeaderText = null;
      this.rp_panel1.Location = new System.Drawing.Point(0, 0);
      this.rp_panel1.Name = "rp_panel1";
      this.rp_panel1.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.rp_panel1.Size = new System.Drawing.Size(313, 337);
      this.rp_panel1.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.WITHOUT_HEAD;
      this.rp_panel1.TabIndex = 0;
      // 
      // lbl_splitline
      // 
      this.lbl_splitline.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_splitline.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(95)))), ((int)(((byte)(95)))));
      this.lbl_splitline.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(95)))), ((int)(((byte)(95)))));
      this.lbl_splitline.Location = new System.Drawing.Point(17, 212);
      this.lbl_splitline.Name = "lbl_splitline";
      this.lbl_splitline.Size = new System.Drawing.Size(278, 1);
      this.lbl_splitline.TabIndex = 81;
      // 
      // pictureBox2
      // 
      this.pictureBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.pictureBox2.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox2.InitialImage")));
      this.pictureBox2.Location = new System.Drawing.Point(8, 87);
      this.pictureBox2.Name = "pictureBox2";
      this.pictureBox2.Size = new System.Drawing.Size(50, 50);
      this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
      this.pictureBox2.TabIndex = 65;
      this.pictureBox2.TabStop = false;
      // 
      // lbl_mb_session_closed
      // 
      this.lbl_mb_session_closed.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_mb_session_closed.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.lbl_mb_session_closed.Font = new System.Drawing.Font("Montserrat", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_mb_session_closed.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(167)))), ((int)(((byte)(221)))), ((int)(((byte)(241)))));
      this.lbl_mb_session_closed.Location = new System.Drawing.Point(32, 253);
      this.lbl_mb_session_closed.Name = "lbl_mb_session_closed";
      this.lbl_mb_session_closed.Size = new System.Drawing.Size(239, 23);
      this.lbl_mb_session_closed.Style = WSI.Cashier.Controls.uc_label.LabelStyle.POPUP_HEADER;
      this.lbl_mb_session_closed.TabIndex = 12;
      this.lbl_mb_session_closed.Text = "XCLOSED SESSION";
      this.lbl_mb_session_closed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.lbl_mb_session_closed.Visible = false;
      // 
      // lbl_available_value_1
      // 
      this.lbl_available_value_1.BackColor = System.Drawing.Color.Transparent;
      this.lbl_available_value_1.Font = new System.Drawing.Font("Montserrat", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_available_value_1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_available_value_1.Location = new System.Drawing.Point(165, 37);
      this.lbl_available_value_1.Name = "lbl_available_value_1";
      this.lbl_available_value_1.Size = new System.Drawing.Size(128, 36);
      this.lbl_available_value_1.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLUE;
      this.lbl_available_value_1.TabIndex = 1;
      this.lbl_available_value_1.Text = "9,999,999.99";
      this.lbl_available_value_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_pending_cash_name
      // 
      this.lbl_pending_cash_name.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_pending_cash_name.BackColor = System.Drawing.Color.Transparent;
      this.lbl_pending_cash_name.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_pending_cash_name.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_pending_cash_name.Location = new System.Drawing.Point(4, 220);
      this.lbl_pending_cash_name.Name = "lbl_pending_cash_name";
      this.lbl_pending_cash_name.Size = new System.Drawing.Size(173, 20);
      this.lbl_pending_cash_name.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_pending_cash_name.TabIndex = 10;
      this.lbl_pending_cash_name.Text = "XPENDING CASH";
      this.lbl_pending_cash_name.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      this.lbl_pending_cash_name.Click += new System.EventHandler(this.lbl_pending_cash_name_Click);
      // 
      // lbl_pending_cash_value
      // 
      this.lbl_pending_cash_value.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_pending_cash_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_pending_cash_value.Font = new System.Drawing.Font("Montserrat", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_pending_cash_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_pending_cash_value.Location = new System.Drawing.Point(165, 212);
      this.lbl_pending_cash_value.Name = "lbl_pending_cash_value";
      this.lbl_pending_cash_value.Size = new System.Drawing.Size(128, 36);
      this.lbl_pending_cash_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLUE;
      this.lbl_pending_cash_value.TabIndex = 11;
      this.lbl_pending_cash_value.Text = "9,999,999.99";
      this.lbl_pending_cash_value.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_available_name_1
      // 
      this.lbl_available_name_1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_available_name_1.BackColor = System.Drawing.Color.Transparent;
      this.lbl_available_name_1.Font = new System.Drawing.Font("Montserrat", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_available_name_1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_available_name_1.Location = new System.Drawing.Point(12, 45);
      this.lbl_available_name_1.Name = "lbl_available_name_1";
      this.lbl_available_name_1.Size = new System.Drawing.Size(163, 20);
      this.lbl_available_name_1.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_available_name_1.TabIndex = 0;
      this.lbl_available_name_1.Text = "XAVAILABLE";
      this.lbl_available_name_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_cash_in_name
      // 
      this.lbl_cash_in_name.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_cash_in_name.BackColor = System.Drawing.Color.Transparent;
      this.lbl_cash_in_name.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_cash_in_name.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_cash_in_name.Location = new System.Drawing.Point(9, 117);
      this.lbl_cash_in_name.Name = "lbl_cash_in_name";
      this.lbl_cash_in_name.Size = new System.Drawing.Size(166, 20);
      this.lbl_cash_in_name.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_cash_in_name.TabIndex = 2;
      this.lbl_cash_in_name.Text = "XCASH IN";
      this.lbl_cash_in_name.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_excess_value
      // 
      this.lbl_excess_value.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_excess_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_excess_value.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_excess_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_excess_value.Location = new System.Drawing.Point(191, 163);
      this.lbl_excess_value.Name = "lbl_excess_value";
      this.lbl_excess_value.Size = new System.Drawing.Size(102, 20);
      this.lbl_excess_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_excess_value.TabIndex = 7;
      this.lbl_excess_value.Text = "0,00";
      this.lbl_excess_value.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_deposit_name_2
      // 
      this.lbl_deposit_name_2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_deposit_name_2.BackColor = System.Drawing.Color.Transparent;
      this.lbl_deposit_name_2.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_deposit_name_2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_deposit_name_2.Location = new System.Drawing.Point(9, 140);
      this.lbl_deposit_name_2.Name = "lbl_deposit_name_2";
      this.lbl_deposit_name_2.Size = new System.Drawing.Size(166, 20);
      this.lbl_deposit_name_2.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_deposit_name_2.TabIndex = 4;
      this.lbl_deposit_name_2.Text = "XCASH DEPOSITS";
      this.lbl_deposit_name_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_mb_excess
      // 
      this.lbl_mb_excess.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_mb_excess.BackColor = System.Drawing.Color.Transparent;
      this.lbl_mb_excess.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_mb_excess.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_mb_excess.Location = new System.Drawing.Point(9, 163);
      this.lbl_mb_excess.Name = "lbl_mb_excess";
      this.lbl_mb_excess.Size = new System.Drawing.Size(166, 20);
      this.lbl_mb_excess.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_mb_excess.TabIndex = 6;
      this.lbl_mb_excess.Text = "XEXCESS IN";
      this.lbl_mb_excess.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_cash_lost_name
      // 
      this.lbl_cash_lost_name.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_cash_lost_name.BackColor = System.Drawing.Color.Transparent;
      this.lbl_cash_lost_name.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_cash_lost_name.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_cash_lost_name.Location = new System.Drawing.Point(9, 186);
      this.lbl_cash_lost_name.Name = "lbl_cash_lost_name";
      this.lbl_cash_lost_name.Size = new System.Drawing.Size(166, 20);
      this.lbl_cash_lost_name.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_cash_lost_name.TabIndex = 8;
      this.lbl_cash_lost_name.Text = "XPENDING CASH";
      this.lbl_cash_lost_name.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_cash_in_value
      // 
      this.lbl_cash_in_value.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_cash_in_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_cash_in_value.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_cash_in_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_cash_in_value.Location = new System.Drawing.Point(191, 117);
      this.lbl_cash_in_value.Name = "lbl_cash_in_value";
      this.lbl_cash_in_value.Size = new System.Drawing.Size(102, 20);
      this.lbl_cash_in_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_cash_in_value.TabIndex = 3;
      this.lbl_cash_in_value.Text = "0,00";
      this.lbl_cash_in_value.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_deposit_value_2
      // 
      this.lbl_deposit_value_2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_deposit_value_2.BackColor = System.Drawing.Color.Transparent;
      this.lbl_deposit_value_2.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_deposit_value_2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_deposit_value_2.Location = new System.Drawing.Point(191, 140);
      this.lbl_deposit_value_2.Name = "lbl_deposit_value_2";
      this.lbl_deposit_value_2.Size = new System.Drawing.Size(102, 20);
      this.lbl_deposit_value_2.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_deposit_value_2.TabIndex = 5;
      this.lbl_deposit_value_2.Text = "0,00";
      this.lbl_deposit_value_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_cash_lost_value
      // 
      this.lbl_cash_lost_value.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_cash_lost_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_cash_lost_value.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_cash_lost_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_cash_lost_value.Location = new System.Drawing.Point(191, 186);
      this.lbl_cash_lost_value.Name = "lbl_cash_lost_value";
      this.lbl_cash_lost_value.Size = new System.Drawing.Size(102, 20);
      this.lbl_cash_lost_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_cash_lost_value.TabIndex = 9;
      this.lbl_cash_lost_value.Text = "0,00";
      this.lbl_cash_lost_value.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_separator_1
      // 
      this.lbl_separator_1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_separator_1.BackColor = System.Drawing.Color.Transparent;
      this.lbl_separator_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
      this.lbl_separator_1.Location = new System.Drawing.Point(112, 212);
      this.lbl_separator_1.Name = "lbl_separator_1";
      this.lbl_separator_1.Size = new System.Drawing.Size(307, 1);
      this.lbl_separator_1.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.lbl_separator_1.TabIndex = 73;
      this.lbl_separator_1.Text = "0,00";
      this.lbl_separator_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // uc_mb_card_balance
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.rp_panel1);
      this.Name = "uc_mb_card_balance";
      this.Size = new System.Drawing.Size(313, 339);
      this.rp_panel1.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private WSI.Cashier.Controls.uc_label lbl_cash_lost_value;
    private WSI.Cashier.Controls.uc_label lbl_deposit_value_2;
    private WSI.Cashier.Controls.uc_label lbl_available_value_1;
    private System.Windows.Forms.PictureBox pictureBox2;
    private WSI.Cashier.Controls.uc_label lbl_cash_lost_name;
    private WSI.Cashier.Controls.uc_label lbl_deposit_name_2;
    private WSI.Cashier.Controls.uc_label lbl_available_name_1;
    private WSI.Cashier.Controls.uc_label lbl_separator_1;
    private WSI.Cashier.Controls.uc_label lbl_cash_in_value;
    private WSI.Cashier.Controls.uc_label lbl_cash_in_name;
    private WSI.Cashier.Controls.uc_label lbl_mb_session_closed;
    private WSI.Cashier.Controls.uc_label lbl_excess_value;
    private WSI.Cashier.Controls.uc_label lbl_mb_excess;
    private WSI.Cashier.Controls.uc_label lbl_pending_cash_value;
    private WSI.Cashier.Controls.uc_label lbl_pending_cash_name;
    protected internal Controls.uc_round_panel rp_panel1;
    private System.Windows.Forms.Label lbl_splitline;
                                 

  }
}
