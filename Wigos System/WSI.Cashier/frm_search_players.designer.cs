using WSI.Cashier.Controls;
namespace WSI.Cashier
{
  partial class frm_search_players
  {  
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_search_players));
      this.chk_vip_account = new WSI.Cashier.Controls.uc_checkBox();
      this.lbl_phone = new WSI.Cashier.Controls.uc_label();
      this.txt_phone = new WSI.Cashier.Controls.uc_round_textbox();
      this.lbl_external_id = new WSI.Cashier.Controls.uc_label();
      this.txt_external_id = new WSI.Cashier.Controls.uc_round_textbox();
      this.lbl_id = new WSI.Cashier.Controls.uc_label();
      this.txt_id = new WSI.Cashier.Controls.uc_round_textbox();
      this.lbl_name = new WSI.Cashier.Controls.uc_label();
      this.txt_name = new WSI.Cashier.Controls.uc_round_textbox();
      this.lbl_msg_blink = new WSI.Cashier.Controls.uc_label();
      this.btn_keyboard = new WSI.Cashier.Controls.uc_round_button();
      this.btn_cancel = new WSI.Cashier.Controls.uc_round_button();
      this.btn_ok = new WSI.Cashier.Controls.uc_round_button();
      this.uc_birth_date = new WSI.Cashier.uc_datetime();
      this.uc_wedding_date = new WSI.Cashier.uc_datetime();
      this.pnl_data.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.Controls.Add(this.uc_wedding_date);
      this.pnl_data.Controls.Add(this.uc_birth_date);
      this.pnl_data.Controls.Add(this.chk_vip_account);
      this.pnl_data.Controls.Add(this.lbl_phone);
      this.pnl_data.Controls.Add(this.txt_phone);
      this.pnl_data.Controls.Add(this.lbl_external_id);
      this.pnl_data.Controls.Add(this.txt_external_id);
      this.pnl_data.Controls.Add(this.lbl_id);
      this.pnl_data.Controls.Add(this.txt_id);
      this.pnl_data.Controls.Add(this.lbl_name);
      this.pnl_data.Controls.Add(this.txt_name);
      this.pnl_data.Controls.Add(this.lbl_msg_blink);
      this.pnl_data.Controls.Add(this.btn_keyboard);
      this.pnl_data.Controls.Add(this.btn_cancel);
      this.pnl_data.Controls.Add(this.btn_ok);
      this.pnl_data.Size = new System.Drawing.Size(736, 420);
      this.pnl_data.TabIndex = 0;
      // 
      // chk_vip_account
      // 
      this.chk_vip_account.AutoSize = true;
      this.chk_vip_account.BackColor = System.Drawing.Color.Transparent;
      this.chk_vip_account.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.chk_vip_account.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.chk_vip_account.Location = new System.Drawing.Point(169, 309);
      this.chk_vip_account.MinimumSize = new System.Drawing.Size(146, 30);
      this.chk_vip_account.Name = "chk_vip_account";
      this.chk_vip_account.Padding = new System.Windows.Forms.Padding(5);
      this.chk_vip_account.Size = new System.Drawing.Size(146, 33);
      this.chk_vip_account.TabIndex = 10;
      this.chk_vip_account.Text = "xVIP Account";
      this.chk_vip_account.UseVisualStyleBackColor = true;
      // 
      // lbl_phone
      // 
      this.lbl_phone.BackColor = System.Drawing.Color.Transparent;
      this.lbl_phone.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_phone.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_phone.Location = new System.Drawing.Point(24, 264);
      this.lbl_phone.Name = "lbl_phone";
      this.lbl_phone.Size = new System.Drawing.Size(144, 38);
      this.lbl_phone.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_phone.TabIndex = 8;
      this.lbl_phone.Text = "xTelefono";
      this.lbl_phone.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // txt_phone
      // 
      this.txt_phone.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_phone.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_phone.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_phone.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_phone.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_phone.CornerRadius = 5;
      this.txt_phone.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_phone.Location = new System.Drawing.Point(169, 263);
      this.txt_phone.MaxLength = 50;
      this.txt_phone.Multiline = false;
      this.txt_phone.Name = "txt_phone";
      this.txt_phone.PasswordChar = '\0';
      this.txt_phone.ReadOnly = false;
      this.txt_phone.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_phone.SelectedText = "";
      this.txt_phone.SelectionLength = 0;
      this.txt_phone.SelectionStart = 0;
      this.txt_phone.Size = new System.Drawing.Size(510, 40);
      this.txt_phone.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_phone.TabIndex = 9;
      this.txt_phone.TabStop = false;
      this.txt_phone.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_phone.UseSystemPasswordChar = false;
      this.txt_phone.WaterMark = null;
      this.txt_phone.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // lbl_external_id
      // 
      this.lbl_external_id.BackColor = System.Drawing.Color.Transparent;
      this.lbl_external_id.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_external_id.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_external_id.Location = new System.Drawing.Point(24, 171);
      this.lbl_external_id.Name = "lbl_external_id";
      this.lbl_external_id.Size = new System.Drawing.Size(144, 38);
      this.lbl_external_id.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_external_id.TabIndex = 5;
      this.lbl_external_id.Text = "xReferencia";
      this.lbl_external_id.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // txt_external_id
      // 
      this.txt_external_id.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_external_id.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_external_id.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_external_id.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_external_id.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_external_id.CornerRadius = 5;
      this.txt_external_id.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_external_id.Location = new System.Drawing.Point(169, 170);
      this.txt_external_id.MaxLength = 50;
      this.txt_external_id.Multiline = false;
      this.txt_external_id.Name = "txt_external_id";
      this.txt_external_id.PasswordChar = '\0';
      this.txt_external_id.ReadOnly = false;
      this.txt_external_id.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_external_id.SelectedText = "";
      this.txt_external_id.SelectionLength = 0;
      this.txt_external_id.SelectionStart = 0;
      this.txt_external_id.Size = new System.Drawing.Size(510, 40);
      this.txt_external_id.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_external_id.TabIndex = 6;
      this.txt_external_id.TabStop = false;
      this.txt_external_id.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_external_id.UseSystemPasswordChar = false;
      this.txt_external_id.WaterMark = null;
      this.txt_external_id.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // lbl_id
      // 
      this.lbl_id.BackColor = System.Drawing.Color.Transparent;
      this.lbl_id.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_id.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_id.Location = new System.Drawing.Point(24, 78);
      this.lbl_id.Name = "lbl_id";
      this.lbl_id.Size = new System.Drawing.Size(144, 38);
      this.lbl_id.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_id.TabIndex = 2;
      this.lbl_id.Text = "xDocumento";
      this.lbl_id.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // txt_id
      // 
      this.txt_id.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_id.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_id.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_id.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_id.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.txt_id.CornerRadius = 5;
      this.txt_id.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_id.Location = new System.Drawing.Point(169, 77);
      this.txt_id.MaxLength = 20;
      this.txt_id.Multiline = false;
      this.txt_id.Name = "txt_id";
      this.txt_id.PasswordChar = '\0';
      this.txt_id.ReadOnly = false;
      this.txt_id.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_id.SelectedText = "";
      this.txt_id.SelectionLength = 0;
      this.txt_id.SelectionStart = 0;
      this.txt_id.Size = new System.Drawing.Size(510, 40);
      this.txt_id.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_id.TabIndex = 3;
      this.txt_id.TabStop = false;
      this.txt_id.Text = "AAAAAAAAAAAAAAAAAAAAAAAAAB";
      this.txt_id.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_id.UseSystemPasswordChar = false;
      this.txt_id.WaterMark = null;
      this.txt_id.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // lbl_name
      // 
      this.lbl_name.BackColor = System.Drawing.Color.Transparent;
      this.lbl_name.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_name.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_name.Location = new System.Drawing.Point(24, 32);
      this.lbl_name.Name = "lbl_name";
      this.lbl_name.Size = new System.Drawing.Size(144, 38);
      this.lbl_name.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_name.TabIndex = 0;
      this.lbl_name.Text = "xNombre";
      this.lbl_name.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // txt_name
      // 
      this.txt_name.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_name.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_name.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_name.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_name.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.txt_name.CornerRadius = 5;
      this.txt_name.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_name.Location = new System.Drawing.Point(169, 31);
      this.txt_name.MaxLength = 50;
      this.txt_name.Multiline = false;
      this.txt_name.Name = "txt_name";
      this.txt_name.PasswordChar = '\0';
      this.txt_name.ReadOnly = false;
      this.txt_name.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_name.SelectedText = "";
      this.txt_name.SelectionLength = 0;
      this.txt_name.SelectionStart = 0;
      this.txt_name.Size = new System.Drawing.Size(510, 40);
      this.txt_name.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_name.TabIndex = 1;
      this.txt_name.TabStop = false;
      this.txt_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_name.UseSystemPasswordChar = false;
      this.txt_name.WaterMark = null;
      this.txt_name.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      this.txt_name.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_name_KeyPress);
      // 
      // lbl_msg_blink
      // 
      this.lbl_msg_blink.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
      this.lbl_msg_blink.BackColor = System.Drawing.Color.Transparent;
      this.lbl_msg_blink.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_msg_blink.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.lbl_msg_blink.Location = new System.Drawing.Point(97, 377);
      this.lbl_msg_blink.Name = "lbl_msg_blink";
      this.lbl_msg_blink.Size = new System.Drawing.Size(359, 23);
      this.lbl_msg_blink.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_RED;
      this.lbl_msg_blink.TabIndex = 12;
      this.lbl_msg_blink.Text = "xAT LEAST ONE FIELD MUST BE PROVIDED";
      this.lbl_msg_blink.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.lbl_msg_blink.Visible = false;
      // 
      // btn_keyboard
      // 
      this.btn_keyboard.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
      this.btn_keyboard.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(47)))), ((int)(((byte)(50)))));
      this.btn_keyboard.FlatAppearance.BorderSize = 0;
      this.btn_keyboard.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_keyboard.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_keyboard.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_keyboard.Image = ((System.Drawing.Image)(resources.GetObject("btn_keyboard.Image")));
      this.btn_keyboard.IsSelected = false;
      this.btn_keyboard.Location = new System.Drawing.Point(23, 352);
      this.btn_keyboard.Name = "btn_keyboard";
      this.btn_keyboard.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_keyboard.Size = new System.Drawing.Size(66, 48);
      this.btn_keyboard.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.TERTIARY;
      this.btn_keyboard.TabIndex = 11;
      this.btn_keyboard.TabStop = false;
      this.btn_keyboard.UseVisualStyleBackColor = false;
      this.btn_keyboard.Click += new System.EventHandler(this.btn_keyboard_Click);
      // 
      // btn_cancel
      // 
      this.btn_cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
      this.btn_cancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btn_cancel.FlatAppearance.BorderSize = 0;
      this.btn_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_cancel.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_cancel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_cancel.Image = null;
      this.btn_cancel.IsSelected = false;
      this.btn_cancel.Location = new System.Drawing.Point(462, 352);
      this.btn_cancel.Name = "btn_cancel";
      this.btn_cancel.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_cancel.Size = new System.Drawing.Size(128, 48);
      this.btn_cancel.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_cancel.TabIndex = 13;
      this.btn_cancel.Text = "XCANCEL";
      this.btn_cancel.UseVisualStyleBackColor = false;
      this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
      // 
      // btn_ok
      // 
      this.btn_ok.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
      this.btn_ok.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_ok.FlatAppearance.BorderSize = 0;
      this.btn_ok.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_ok.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_ok.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_ok.Image = null;
      this.btn_ok.IsSelected = false;
      this.btn_ok.Location = new System.Drawing.Point(596, 352);
      this.btn_ok.Name = "btn_ok";
      this.btn_ok.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_ok.Size = new System.Drawing.Size(128, 48);
      this.btn_ok.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_ok.TabIndex = 14;
      this.btn_ok.Text = "XOK";
      this.btn_ok.UseVisualStyleBackColor = false;
      this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
      // 
      // uc_birth_date
      // 
      this.uc_birth_date.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
      this.uc_birth_date.DateText = "xNacimiento";
      this.uc_birth_date.DateTextFont = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.uc_birth_date.DateTextWidth = 122;
      this.uc_birth_date.DateValue = new System.DateTime(1984, 6, 11, 0, 0, 0, 0);
      this.uc_birth_date.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.uc_birth_date.FormatFont = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.uc_birth_date.FormatWidth = 178;
      this.uc_birth_date.Location = new System.Drawing.Point(47, 123);
      this.uc_birth_date.Name = "uc_birth_date";
      this.uc_birth_date.Size = new System.Drawing.Size(489, 41);
      this.uc_birth_date.TabIndex = 4;
      this.uc_birth_date.ValuesFont = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      // 
      // uc_wedding_date
      // 
      this.uc_wedding_date.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
      this.uc_wedding_date.DateText = "xBoda";
      this.uc_wedding_date.DateTextFont = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.uc_wedding_date.DateTextWidth = 119;
      this.uc_wedding_date.DateValue = new System.DateTime(1984, 6, 11, 0, 0, 0, 0);
      this.uc_wedding_date.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.uc_wedding_date.FormatFont = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.uc_wedding_date.FormatWidth = 172;
      this.uc_wedding_date.Location = new System.Drawing.Point(51, 216);
      this.uc_wedding_date.Name = "uc_wedding_date";
      this.uc_wedding_date.Size = new System.Drawing.Size(480, 41);
      this.uc_wedding_date.TabIndex = 7;
      this.uc_wedding_date.ValuesFont = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      // 
      // frm_search_players
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(736, 475);
      this.ControlBox = false;
      this.KeyPreview = true;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "frm_search_players";
      this.ShowIcon = false;
      this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
      this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
      this.Style = WSI.Cashier.Controls.frm_base.BaseFormStyle.PRINCIPAL;
      this.Text = "xAccount Search";
      this.Shown += new System.EventHandler(this.frm_search_players_Shown);
      this.pnl_data.ResumeLayout(false);
      this.pnl_data.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private uc_checkBox chk_vip_account;
    private uc_label lbl_phone;
    private uc_round_textbox txt_phone;
    private uc_label lbl_external_id;
    private uc_round_textbox txt_external_id;
    private uc_label lbl_id;
    private uc_round_textbox txt_id;
    private uc_label lbl_name;
    private uc_round_textbox txt_name;
    private uc_label lbl_msg_blink;
    private uc_round_button btn_keyboard;
    private uc_round_button btn_cancel;
    private uc_round_button btn_ok;
    private uc_datetime uc_wedding_date;
    private uc_datetime uc_birth_date;


  }
}
