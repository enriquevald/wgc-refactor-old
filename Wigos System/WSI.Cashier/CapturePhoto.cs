﻿using System;
using System.Drawing;
using System.Windows.Forms;
using WSI.Cashier.Controls;
using WSI.IDCamera;

namespace WSI.Cashier
{

  public partial class frmCapturePhoto : frm_base, IfrmCapturePhoto
  {
    //Private
    private CameraModule _cameraModule;
    private bool m_capturing;
    //Contructor
    public frmCapturePhoto()
    {
      InitializeComponent();
      FormTitle = Common.Resource.String("STR_FORMAT_GENERAL_BUTTONS_TAKE_PHOTO");
      btn_take_photo.Text = Common.Resource.String("STR_FORMAT_GENERAL_BUTTONS_TAKE_PHOTO");
      btnCancel.Text = Common.Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");
    } //frmCapturePhoto

    //Properties
    public Image CapturedImage { get; set; } //CapturedImage

    //Methods
    public Image GetImage(CameraModule CameraModule, Form frm)
    {
      CapturedImage = null;
      Image retval = null;
      this._cameraModule = CameraModule;

      if (_cameraModule.EventMode)
      {
        if (!m_capturing)
        {
          m_capturing = _cameraModule.StartCapture(previewSuface.Handle);
        }
        if (m_capturing)
        {
          this.Show();

          this.BringToFront();
        }
      }
      else
      {
        if (_cameraModule.StartCapture(previewSuface.Handle))
        {
          using (frm_yesno _shader = new frm_yesno())
          {
            _shader.Opacity = 0.6;
            _shader.Show();
            ShowDialog(_shader);
          }
        }
        _cameraModule.Dispose();

        retval = CapturedImage;
      }

      return retval;
    } //GetImage

    private void previewSuface_MouseDown(object sender, MouseEventArgs e)
    {
      if (_cameraModule.EventMode)
      {
        _cameraModule.TakePhotoEvent();
        this.SendToBack();
        this.Hide();
      }
      else
      {
        CapturedImage = _cameraModule.TakePhoto();
        DialogResult = DialogResult.OK;
      }

    }

    private void frmCapturePhoto_FormClosing(object sender, FormClosingEventArgs e)
    {
      if (_cameraModule.EventMode)
      {
        e.Cancel = true;
        this.SendToBack();
        this.Hide();
      }
      else
      {
        DialogResult = DialogResult.Cancel;
      }

    } //btnTakePhoto_Click

    private void btnCancel_Click(object sender, EventArgs e)
    {
      if (_cameraModule.EventMode)
      {
        this.SendToBack();
        this.Hide();
      }
      else
      {
        DialogResult = DialogResult.Cancel;
      }
    }

    private void frmCapturePhoto_Deactivate(object sender, EventArgs e)
    {
      if (_cameraModule.EventMode)
      {
        _cameraModule.RegisterHandler(null,null);
        this.Hide();
      }
    }

    private void uc_round_button1_Click(object sender, EventArgs e)
    {
      if (_cameraModule.EventMode)
      {
        _cameraModule.TakePhotoEvent();
        this.SendToBack();
        this.Hide();
      }
      else
      {
        CapturedImage = _cameraModule.TakePhoto();
        DialogResult = DialogResult.OK;
      }

    }

  }
}
