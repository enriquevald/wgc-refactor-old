// Copyright � 2007-2014 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_gt_player.cs
// 
//   DESCRIPTION: Implements the uc_gt_player user control
//                Class: uc_gt_player
//
//        AUTHOR: Jos� Mart�nez
// 
// CREATION DATE: 05-JUN-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 05-JUN-2014 JML     First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using WSI.Common;

namespace WSI.Cashier
{
  public partial class uc_gt_player : UserControl
  {
    public uc_gt_player()
    {
      InitializeComponent();
    }
  } // uc_gt_player
}
