namespace WSI.Cashier
{
  partial class frm_payment_method
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
      this.pnl_currency = new WSI.Cashier.Controls.uc_round_panel();
      this.btn_currency = new WSI.Cashier.Controls.uc_round_button();
      this.pnl_currency_1 = new WSI.Cashier.Controls.uc_round_panel();
      this.btn_currency_1 = new WSI.Cashier.Controls.uc_round_button();
      this.pnl_currency_2 = new WSI.Cashier.Controls.uc_round_panel();
      this.btn_currency_2 = new WSI.Cashier.Controls.uc_round_button();
      this.txt_tax = new WSI.Cashier.Controls.uc_label();
      this.lbl_comission = new WSI.Cashier.Controls.uc_label();
      this.btn_cancel = new WSI.Cashier.Controls.uc_round_button();
      this.btn_ok = new WSI.Cashier.Controls.uc_round_button();
      this.lbl_total_to_advance = new WSI.Cashier.Controls.uc_label();
      this.lbl_total_to_paid = new WSI.Cashier.Controls.uc_label();
      this.txt_amount_to_advance = new WSI.Cashier.Controls.uc_round_textbox();
      this.pb_keyboard = new WSI.Cashier.Controls.uc_round_button();
      this.txt_amount_paid = new WSI.Cashier.Controls.uc_round_textbox();
      this.chk_without_comissions = new WSI.Cashier.Controls.uc_round_button();
      this.pnl_data.SuspendLayout();
      this.tableLayoutPanel1.SuspendLayout();
      this.pnl_currency.SuspendLayout();
      this.pnl_currency_1.SuspendLayout();
      this.pnl_currency_2.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.Controls.Add(this.chk_without_comissions);
      this.pnl_data.Controls.Add(this.btn_cancel);
      this.pnl_data.Controls.Add(this.btn_ok);
      this.pnl_data.Controls.Add(this.tableLayoutPanel1);
      this.pnl_data.Controls.Add(this.txt_tax);
      this.pnl_data.Controls.Add(this.lbl_comission);
      this.pnl_data.Controls.Add(this.lbl_total_to_advance);
      this.pnl_data.Controls.Add(this.lbl_total_to_paid);
      this.pnl_data.Controls.Add(this.txt_amount_to_advance);
      this.pnl_data.Controls.Add(this.pb_keyboard);
      this.pnl_data.Controls.Add(this.txt_amount_paid);
      this.pnl_data.Size = new System.Drawing.Size(457, 475);
      this.pnl_data.TabIndex = 0;
      // 
      // tableLayoutPanel1
      // 
      this.tableLayoutPanel1.ColumnCount = 2;
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 168F));
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 167F));
      this.tableLayoutPanel1.Controls.Add(this.pnl_currency, 0, 0);
      this.tableLayoutPanel1.Controls.Add(this.pnl_currency_1, 1, 0);
      this.tableLayoutPanel1.Controls.Add(this.pnl_currency_2, 0, 1);
      this.tableLayoutPanel1.Location = new System.Drawing.Point(61, 199);
      this.tableLayoutPanel1.Name = "tableLayoutPanel1";
      this.tableLayoutPanel1.RowCount = 2;
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 90F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 90F));
      this.tableLayoutPanel1.Size = new System.Drawing.Size(335, 177);
      this.tableLayoutPanel1.TabIndex = 6;
      // 
      // pnl_currency
      // 
      this.pnl_currency.BackColor = System.Drawing.Color.Transparent;
      this.pnl_currency.BorderColor = System.Drawing.Color.Empty;
      this.pnl_currency.Controls.Add(this.btn_currency);
      this.pnl_currency.CornerRadius = 1;
      this.pnl_currency.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pnl_currency.HeaderBackColor = System.Drawing.Color.Empty;
      this.pnl_currency.HeaderForeColor = System.Drawing.Color.Empty;
      this.pnl_currency.HeaderHeight = 0;
      this.pnl_currency.HeaderSubText = null;
      this.pnl_currency.HeaderText = null;
      this.pnl_currency.Location = new System.Drawing.Point(3, 3);
      this.pnl_currency.Name = "pnl_currency";
      this.pnl_currency.PanelBackColor = System.Drawing.Color.Empty;
      this.pnl_currency.Size = new System.Drawing.Size(162, 84);
      this.pnl_currency.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NONE;
      this.pnl_currency.TabIndex = 0;
      // 
      // btn_currency
      // 
      this.btn_currency.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_currency.FlatAppearance.BorderSize = 0;
      this.btn_currency.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_currency.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_currency.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_currency.Image = null;
      this.btn_currency.IsSelected = false;
      this.btn_currency.Location = new System.Drawing.Point(0, 0);
      this.btn_currency.Name = "btn_currency";
      this.btn_currency.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_currency.Size = new System.Drawing.Size(160, 80);
      this.btn_currency.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_currency.TabIndex = 0;
      this.btn_currency.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
      this.btn_currency.UseVisualStyleBackColor = false;
      this.btn_currency.Click += new System.EventHandler(this.btn_currency_Click);
      // 
      // pnl_currency_1
      // 
      this.pnl_currency_1.BackColor = System.Drawing.Color.Transparent;
      this.pnl_currency_1.BorderColor = System.Drawing.Color.Empty;
      this.pnl_currency_1.Controls.Add(this.btn_currency_1);
      this.pnl_currency_1.CornerRadius = 1;
      this.pnl_currency_1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pnl_currency_1.HeaderBackColor = System.Drawing.Color.Empty;
      this.pnl_currency_1.HeaderForeColor = System.Drawing.Color.Empty;
      this.pnl_currency_1.HeaderHeight = 0;
      this.pnl_currency_1.HeaderSubText = null;
      this.pnl_currency_1.HeaderText = null;
      this.pnl_currency_1.Location = new System.Drawing.Point(171, 3);
      this.pnl_currency_1.Name = "pnl_currency_1";
      this.pnl_currency_1.PanelBackColor = System.Drawing.Color.Empty;
      this.pnl_currency_1.Size = new System.Drawing.Size(161, 84);
      this.pnl_currency_1.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NONE;
      this.pnl_currency_1.TabIndex = 1;
      // 
      // btn_currency_1
      // 
      this.btn_currency_1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_currency_1.FlatAppearance.BorderSize = 0;
      this.btn_currency_1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_currency_1.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_currency_1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_currency_1.Image = null;
      this.btn_currency_1.IsSelected = false;
      this.btn_currency_1.Location = new System.Drawing.Point(0, 0);
      this.btn_currency_1.Name = "btn_currency_1";
      this.btn_currency_1.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_currency_1.Size = new System.Drawing.Size(160, 80);
      this.btn_currency_1.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_currency_1.TabIndex = 0;
      this.btn_currency_1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
      this.btn_currency_1.UseVisualStyleBackColor = false;
      this.btn_currency_1.Click += new System.EventHandler(this.btn_currency_Click);
      // 
      // pnl_currency_2
      // 
      this.pnl_currency_2.BackColor = System.Drawing.Color.Transparent;
      this.pnl_currency_2.BorderColor = System.Drawing.Color.Empty;
      this.pnl_currency_2.Controls.Add(this.btn_currency_2);
      this.pnl_currency_2.CornerRadius = 1;
      this.pnl_currency_2.HeaderBackColor = System.Drawing.Color.Empty;
      this.pnl_currency_2.HeaderForeColor = System.Drawing.Color.Empty;
      this.pnl_currency_2.HeaderHeight = 0;
      this.pnl_currency_2.HeaderSubText = null;
      this.pnl_currency_2.HeaderText = null;
      this.pnl_currency_2.Location = new System.Drawing.Point(3, 93);
      this.pnl_currency_2.Name = "pnl_currency_2";
      this.pnl_currency_2.PanelBackColor = System.Drawing.Color.Empty;
      this.pnl_currency_2.Size = new System.Drawing.Size(162, 81);
      this.pnl_currency_2.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NONE;
      this.pnl_currency_2.TabIndex = 2;
      // 
      // btn_currency_2
      // 
      this.btn_currency_2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_currency_2.FlatAppearance.BorderSize = 0;
      this.btn_currency_2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_currency_2.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_currency_2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_currency_2.Image = null;
      this.btn_currency_2.IsSelected = false;
      this.btn_currency_2.Location = new System.Drawing.Point(0, 0);
      this.btn_currency_2.Name = "btn_currency_2";
      this.btn_currency_2.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_currency_2.Size = new System.Drawing.Size(160, 80);
      this.btn_currency_2.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_currency_2.TabIndex = 0;
      this.btn_currency_2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
      this.btn_currency_2.UseVisualStyleBackColor = false;
      this.btn_currency_2.Click += new System.EventHandler(this.btn_currency_Click);
      // 
      // txt_tax
      // 
      this.txt_tax.BackColor = System.Drawing.Color.Transparent;
      this.txt_tax.Font = new System.Drawing.Font("Open Sans Semibold", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_tax.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.txt_tax.Location = new System.Drawing.Point(227, 87);
      this.txt_tax.Name = "txt_tax";
      this.txt_tax.Size = new System.Drawing.Size(213, 36);
      this.txt_tax.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.txt_tax.TabIndex = 3;
      this.txt_tax.Text = "$37,037.01";
      this.txt_tax.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_comission
      // 
      this.lbl_comission.BackColor = System.Drawing.Color.Transparent;
      this.lbl_comission.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_comission.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_comission.Location = new System.Drawing.Point(12, 84);
      this.lbl_comission.Name = "lbl_comission";
      this.lbl_comission.Size = new System.Drawing.Size(209, 43);
      this.lbl_comission.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_comission.TabIndex = 2;
      this.lbl_comission.Text = "xComission";
      this.lbl_comission.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // btn_cancel
      // 
      this.btn_cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.btn_cancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btn_cancel.FlatAppearance.BorderSize = 0;
      this.btn_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_cancel.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_cancel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_cancel.Image = null;
      this.btn_cancel.IsSelected = false;
      this.btn_cancel.Location = new System.Drawing.Point(64, 400);
      this.btn_cancel.Name = "btn_cancel";
      this.btn_cancel.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_cancel.Size = new System.Drawing.Size(155, 60);
      this.btn_cancel.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_cancel.TabIndex = 7;
      this.btn_cancel.Text = "XCANCEL";
      this.btn_cancel.UseVisualStyleBackColor = false;
      // 
      // btn_ok
      // 
      this.btn_ok.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_ok.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_ok.FlatAppearance.BorderSize = 0;
      this.btn_ok.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_ok.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_ok.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_ok.Image = null;
      this.btn_ok.IsSelected = false;
      this.btn_ok.Location = new System.Drawing.Point(237, 400);
      this.btn_ok.Name = "btn_ok";
      this.btn_ok.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_ok.Size = new System.Drawing.Size(155, 60);
      this.btn_ok.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_ok.TabIndex = 8;
      this.btn_ok.Text = "OK";
      this.btn_ok.UseVisualStyleBackColor = false;
      this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
      // 
      // lbl_total_to_advance
      // 
      this.lbl_total_to_advance.BackColor = System.Drawing.Color.Transparent;
      this.lbl_total_to_advance.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_total_to_advance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_total_to_advance.Location = new System.Drawing.Point(12, 28);
      this.lbl_total_to_advance.Name = "lbl_total_to_advance";
      this.lbl_total_to_advance.Size = new System.Drawing.Size(209, 44);
      this.lbl_total_to_advance.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_total_to_advance.TabIndex = 0;
      this.lbl_total_to_advance.Text = "xTotalToAdvance";
      this.lbl_total_to_advance.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_total_to_paid
      // 
      this.lbl_total_to_paid.BackColor = System.Drawing.Color.Transparent;
      this.lbl_total_to_paid.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_total_to_paid.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_total_to_paid.Location = new System.Drawing.Point(12, 137);
      this.lbl_total_to_paid.Name = "lbl_total_to_paid";
      this.lbl_total_to_paid.Size = new System.Drawing.Size(209, 44);
      this.lbl_total_to_paid.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_total_to_paid.TabIndex = 4;
      this.lbl_total_to_paid.Text = "xTotalToPaid";
      this.lbl_total_to_paid.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // txt_amount_to_advance
      // 
      this.txt_amount_to_advance.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_amount_to_advance.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_amount_to_advance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_amount_to_advance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_amount_to_advance.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_amount_to_advance.CornerRadius = 5;
      this.txt_amount_to_advance.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_amount_to_advance.Location = new System.Drawing.Point(227, 25);
      this.txt_amount_to_advance.MaxLength = 18;
      this.txt_amount_to_advance.Multiline = false;
      this.txt_amount_to_advance.Name = "txt_amount_to_advance";
      this.txt_amount_to_advance.PasswordChar = '\0';
      this.txt_amount_to_advance.ReadOnly = false;
      this.txt_amount_to_advance.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_amount_to_advance.SelectedText = "";
      this.txt_amount_to_advance.SelectionLength = 0;
      this.txt_amount_to_advance.SelectionStart = 0;
      this.txt_amount_to_advance.Size = new System.Drawing.Size(213, 50);
      this.txt_amount_to_advance.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC_CUSTOM;
      this.txt_amount_to_advance.TabIndex = 1;
      this.txt_amount_to_advance.TabStop = false;
      this.txt_amount_to_advance.Text = "123";
      this.txt_amount_to_advance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      this.txt_amount_to_advance.UseSystemPasswordChar = false;
      this.txt_amount_to_advance.WaterMark = null;
      this.txt_amount_to_advance.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      this.txt_amount_to_advance.TextChanged += new System.EventHandler(this.txt_amount_to_advance_TextChanged);
      this.txt_amount_to_advance.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_payment_method_KeyPress);
      this.txt_amount_to_advance.Enter += new System.EventHandler(this.txt_amount_to_advance_Enter);
      this.txt_amount_to_advance.Click += new System.EventHandler(this.txt_amount_to_advance_Click);
      this.txt_amount_to_advance.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_payment_method_PreviewKeyDown);
      // 
      // pb_keyboard
      // 
      this.pb_keyboard.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.pb_keyboard.CornerRadius = 0;
      this.pb_keyboard.FlatAppearance.BorderSize = 0;
      this.pb_keyboard.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.pb_keyboard.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.pb_keyboard.Image = global::WSI.Cashier.Properties.Resources.keyboard;
      this.pb_keyboard.IsSelected = false;
      this.pb_keyboard.Location = new System.Drawing.Point(397, 505);
      this.pb_keyboard.Name = "pb_keyboard";
      this.pb_keyboard.SelectedColor = System.Drawing.Color.Empty;
      this.pb_keyboard.Size = new System.Drawing.Size(44, 44);
      this.pb_keyboard.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.NONE;
      this.pb_keyboard.TabIndex = 2;
      this.pb_keyboard.UseVisualStyleBackColor = false;
      this.pb_keyboard.Visible = false;
      // 
      // txt_amount_paid
      // 
      this.txt_amount_paid.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_amount_paid.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_amount_paid.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_amount_paid.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_amount_paid.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_amount_paid.CornerRadius = 5;
      this.txt_amount_paid.Font = new System.Drawing.Font("Open Sans Semibold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_amount_paid.Location = new System.Drawing.Point(227, 134);
      this.txt_amount_paid.MaxLength = 18;
      this.txt_amount_paid.Multiline = false;
      this.txt_amount_paid.Name = "txt_amount_paid";
      this.txt_amount_paid.PasswordChar = '\0';
      this.txt_amount_paid.ReadOnly = false;
      this.txt_amount_paid.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_amount_paid.SelectedText = "";
      this.txt_amount_paid.SelectionLength = 0;
      this.txt_amount_paid.SelectionStart = 0;
      this.txt_amount_paid.Size = new System.Drawing.Size(213, 50);
      this.txt_amount_paid.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC_CUSTOM;
      this.txt_amount_paid.TabIndex = 5;
      this.txt_amount_paid.TabStop = false;
      this.txt_amount_paid.Text = "123";
      this.txt_amount_paid.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      this.txt_amount_paid.UseSystemPasswordChar = false;
      this.txt_amount_paid.WaterMark = null;
      this.txt_amount_paid.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      this.txt_amount_paid.TextChanged += new System.EventHandler(this.txt_amount_TextChanged);
      this.txt_amount_paid.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_payment_method_KeyPress);
      this.txt_amount_paid.Enter += new System.EventHandler(this.txt_amount_paid_Enter);
      this.txt_amount_paid.Click += new System.EventHandler(this.txt_amount_paid_Click);
      this.txt_amount_paid.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.frm_payment_method_PreviewKeyDown);
      // 
      // chk_without_comissions
      // 
      this.chk_without_comissions.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.chk_without_comissions.FlatAppearance.BorderColor = System.Drawing.SystemColors.Control;
      this.chk_without_comissions.FlatAppearance.BorderSize = 0;
      this.chk_without_comissions.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.chk_without_comissions.Font = new System.Drawing.Font("Open Sans Semibold", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.chk_without_comissions.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.chk_without_comissions.Image = null;
      this.chk_without_comissions.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
      this.chk_without_comissions.IsSelected = false;
      this.chk_without_comissions.Location = new System.Drawing.Point(120, 84);
      this.chk_without_comissions.Margin = new System.Windows.Forms.Padding(0);
      this.chk_without_comissions.Name = "chk_without_comissions";
      this.chk_without_comissions.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.chk_without_comissions.Size = new System.Drawing.Size(112, 42);
      this.chk_without_comissions.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PAYMENT_METHOD_SELECTED;
      this.chk_without_comissions.TabIndex = 31;
      this.chk_without_comissions.Text = "XWITHOUTCOMISSIONS";
      this.chk_without_comissions.UseVisualStyleBackColor = false;
      this.chk_without_comissions.Click += new System.EventHandler(this.chk_without_comissions_Click);
      // 
      // frm_payment_method
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.btn_cancel;
      this.ClientSize = new System.Drawing.Size(457, 530);
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "frm_payment_method";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Style = WSI.Cashier.Controls.frm_base.BaseFormStyle.PRINCIPAL;
      this.Text = "frm_payment_method";
      this.Activated += new System.EventHandler(this.frm_payment_method_Activated);
      this.pnl_data.ResumeLayout(false);
      this.tableLayoutPanel1.ResumeLayout(false);
      this.pnl_currency.ResumeLayout(false);
      this.pnl_currency_1.ResumeLayout(false);
      this.pnl_currency_2.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    //private System.Windows.Forms.SplitContainer splitContainer1;
    private WSI.Cashier.Controls.uc_round_textbox  txt_amount_paid;
    private WSI.Cashier.Controls.uc_round_button btn_cancel;
    private WSI.Cashier.Controls.uc_round_button btn_ok;
    private WSI.Cashier.Controls.uc_round_button pb_keyboard;
    private WSI.Cashier.Controls.uc_round_button btn_currency_2;
    private WSI.Cashier.Controls.uc_round_panel pnl_currency_1;
    private WSI.Cashier.Controls.uc_round_button btn_currency_1;
    private WSI.Cashier.Controls.uc_round_panel pnl_currency;
    private WSI.Cashier.Controls.uc_round_button btn_currency;
    private WSI.Cashier.Controls.uc_round_textbox txt_amount_to_advance;
    private WSI.Cashier.Controls.uc_label lbl_total_to_paid;
    private WSI.Cashier.Controls.uc_label lbl_total_to_advance;
    private WSI.Cashier.Controls.uc_label lbl_comission;
    private WSI.Cashier.Controls.uc_label txt_tax;
    private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    private Controls.uc_round_panel pnl_currency_2;
    private Controls.uc_round_button chk_without_comissions;

  }
}