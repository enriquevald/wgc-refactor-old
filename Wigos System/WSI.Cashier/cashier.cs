//------------------------------------------------------------------------------
// Copyright � 2010 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Cashier.cs
// 
//   DESCRIPTION: 
// 
//        AUTHOR: 
// 
// CREATION DATE: 
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 26-OCT-2011 AJQ    Set a unique CashierInformation for the whole Cashier Application
// 21-MAR-2012 MPO    User session: New parameters in WSI.Common.WGDB.SetUserLoggedIn and SetUserLoggedOff
// 20-JUN-2012 RCI    Added class CashierSessionInfo
// 20-JUN-2012 RCI    Added an static method to return an instance of the class CashierSessionInfo.
// 05-JUL-2013 LEM    Cashier app restart on updating instead of shutdown.
// 12-AUG-2013 DRV    Added an static property that indicates if onscreen keyboard is hidden or not
// 29-AUG-2013 JCOR   Added AuthorizedByUserId AuthorizedByUserName.
// 23-JUN-2016 AMF    Added PinPadEnabled.
// 25-JUL-2016 FJC    PBI 15446:Visitas / Recepci�n: MEJORAS - Cajero - Apertura sesi�n de caja
// 19-AUG-2016 RGR    Added an static method to return is closed
// 16-FEB-2017 ATB    Add logging system to the shutdowns to control it
// 09-MAR-2017 ATB    PBI 25262: Exped - Payment authorisation
// 13-MAR-2017 ATB    PBI 25262: Exped - Payment authorisation
// 14-MAR-2017 ATB    PBI 25262: Exped - Payment authorisation
// 15-MAR-2017 FAV    PBI 25262: Exped - Payment authorisation
// 15-MAR-2017 ATB    PBI 25262: Exped - Payment authorisation
//-------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using Microsoft.Win32;
using System.Runtime.InteropServices;
using WSI.Common;
using System.Threading;
using WSI.Common.ExternalPaymentAndSaleValidation;

namespace WSI.Cashier
{
  public static class Cashier
  {
    //
    // Functions Prototypes
    //
    [DllImport("CommonBase")]
    static extern int Common_SystemShutdown(int ShutdownType);

    [DllImport("kernel32.dll", SetLastError = true)]
    [return: MarshalAs(UnmanagedType.Bool)]
    static extern bool TerminateProcess(IntPtr hProcess, uint uExitCode);

    [DllImport("user32.dll", CharSet = CharSet.Auto)]
    static extern int SystemParametersInfo(int uAction, int uParam, string lpvParam, int fuWinIni);

    public enum BankMode
    {
      Unknown = 0,
      PerUser = 1,
      PerTerminal = 2
    }

    public enum CashierOpenAutomaticallyResult
    {
      OK = 0,
      NOT_OPEN_CASHIER_SESSION_BY_USER = 1,
      ERROR = 2
    }

    public enum WallpaperAction
    {
      Updating, Running
    }

    public const Decimal MAX_ALLOWED_AMOUNT = 999999999.99M;
    public const Decimal MAX_ALLOWED_AMOUNT_LENGTH = 12;

    public const Int32 MAX_ALLOWED_TICKET_LENGTH = 20;

    public const String UpdatingWallpaper = "UM_UpdatingApplication.bmp";
    public const String RunningWallpaper = "UM_Logo.bmp";

    private static Int32 terminal_id;
    private static String terminal_name;

    private static Int32 user_id;
    private static Int32 authorized_by_user_id;
    private static Int64 session_id;

    private static string user_name;
    private static string authorized_by_user_name;
    private static bool zero_opening;
    private static BankMode bank_mode;
    private static String[] casino_data;
    private static Form main_form;

    private static Boolean m_already_closing = false;

    private static Boolean logo = false;
    private static Boolean m_hide_osk = false;

    private static DateTime gaming_day;

    private static Boolean m_pinpad_enabled;

    public static Boolean PrintVoucherLogo
    {
      get { return logo; }
    }

    public static Form MainForm
    {
      get { return Cashier.main_form; }
      set { Cashier.main_form = value; }
    }

    public static String[] CasinoData
    {
      get { return Cashier.casino_data; }
      set { Cashier.casino_data = value; }
    }

    public static Int64 TerminalId
    {
      get { return terminal_id; }
    }

    public static String TerminalName
    {
      get { return terminal_name; }
    }

    public static Int32 UserId
    {
      get { return user_id; }
    }

    public static Int32 AuthorizedByUserId
    {
      get { return authorized_by_user_id; }
      set { Cashier.authorized_by_user_id = value; }
    }

    public static Int64 SessionId
    {
      get { return session_id; }
    }

    public static string UserName
    {
      get { return user_name; }
    }

    public static string AuthorizedByUserName
    {
      get { return authorized_by_user_name; }
      set { Cashier.authorized_by_user_name = value; }
    }

    public static bool ZERO_OPENING
    {
      get { return zero_opening; }
    }

    public static BankMode Mode
    {
      get { return bank_mode; }
    }

    public static Boolean AlreadyClosing
    {
      get { return m_already_closing; }
    }

    public static Boolean HideOsk
    {
      get { return m_hide_osk; }
      set
      {
        m_hide_osk = value;
        Common.Cashier.UpdateOSKMode(m_hide_osk);
      }
    }

    public static Boolean PinPadEnabled
    {
      get { return m_pinpad_enabled; }
      set
      {
        m_pinpad_enabled = value;
        Common.Cashier.UpdatePinPadEnabled(m_pinpad_enabled);
      }
    }

    public static DateTime GamingDay
    {
      get { return gaming_day; }
    }

    public static void SetUserSession(Int64 SessionId)
    {
      session_id = SessionId;
      WSI.Common.CommonCashierInformation.SetUserSession(SessionId);
    }
    public static void SetGamingDay(DateTime GamingDay)
    {
      gaming_day = GamingDay;
    }

    public static void SetUserLoggedIn(Int32 TerminalId, String TerminalName, Int32 UserId, String UserName, BankMode Mode, Int64 SessionId, DateTime GamingDay)
    {
      user_id = UserId;
      authorized_by_user_id = UserId;
      user_name = UserName;
      authorized_by_user_name = UserName;
      terminal_id = TerminalId;
      terminal_name = TerminalName;
      bank_mode = Mode;
      session_id = SessionId;
      gaming_day = GamingDay;
      WSI.Common.WGDB.SetUserLoggedIn(UserId, UserName, TerminalName);
      WSI.Common.CommonCashierInformation.SetCashierInformation(SessionId, UserId, UserName, TerminalId, TerminalName);
      // AUDIT_NAME_LOGIN_LOGOUT           4
      WSI.Common.Auditor.Audit(ENUM_GUI.CASHIER, UserId, UserName, TerminalName, 4, 5000 + 265, "", "", "", "", "");
    }

    public static void SetUserLoggedOff()
    {
      user_id = 0;
      user_name = "";
      session_id = 0;
      terminal_id = 0;
      bank_mode = BankMode.Unknown;
      WSI.Common.WGDB.SetUserLoggedOff();
    }

    public static void Init()
    {
      terminal_id = 0;
      SetUserLoggedOff();
      bank_mode = Cashier.BankMode.Unknown;
      zero_opening = true;
      Program.print_voucher = false;

      // ICS 31-MAY-2013 Must be used only for Cashier and GUI processes
      CommonCashierInformation.SetUnique();
    }

    public static void ResetAuthorizedByUser()
    {
      authorized_by_user_id = user_id;
      authorized_by_user_name = user_name;
      WSI.Common.CommonCashierInformation.AuthorizedByUserId = user_id;
      WSI.Common.CommonCashierInformation.AuthorizedByUserName = user_name;

    }

    //------------------------------------------------------------------------------
    // PURPOSE: Executes a system shutdown (always shutdown)
    // 
    //  PARAMS:
    //
    //      - INPUT:
    //          - QuestionMessage
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    // 
    //   NOTES:
    public static void Shutdown(Boolean QuestionMessage)
    {
      // ATB 16-FEB-2017
      // If the user has accepted to shutdown, the log is written
      if (QuestionMessage)
      {
        Misc.WriteLog("[PROMPTED SHUTDOWN]: Accepted", Log.Type.Message);
      }
      Shutdown(QuestionMessage, false, Users.EXIT_CODE.LOGGED_OUT);
    }

    public static void Shutdown(Boolean QuestionMessage, Boolean RestartSystem)
    {
      // ATB 16-FEB-2017
      // After setting the kind of shutdown, checks if the user really wants
      // to shutdown and write the log
      string shutdownKind = string.Empty;
      if (RestartSystem)
      {
        shutdownKind = "RESTART";
      }
      else
      {
        shutdownKind = "SHUTDOWN";
      }
      if (QuestionMessage)
      {
        Misc.WriteLog("[PROMPTED " + shutdownKind + "]: Accepted", Log.Type.Message);
      }
      Shutdown(QuestionMessage, RestartSystem, Users.EXIT_CODE.LOGGED_OUT);
    }

    public static void Shutdown(Boolean QuestionMessage, Users.EXIT_CODE ExitCode)
    {
      // ATB 16-FEB-2017
      // Checks if the user accepts to shutdown to notify it, and then it notifies
      // the exitcode in another log
      if (QuestionMessage)
      {
        Misc.WriteLog("[PROMPTED SHUTDOWN]: Accepted", Log.Type.Message);
      }
      Misc.WriteLog("[SHUTDOWN]: " + ExitCode.ToString(), Log.Type.Message);
      Shutdown(QuestionMessage, false, ExitCode);
    }
    public static void RestartApplicationForUpdate()
    {
      Shutdown(false, true, Users.EXIT_CODE.LOGGED_OUT);
    }
    //------------------------------------------------------------------------------
    // PURPOSE: Executes a system shutdown, option restart
    // 
    //  PARAMS:
    //
    //      - INPUT:
    //          - QuestionMessage
    //          - RestartSystem
    //          - ExitCode
    //          - NewVersionUpdating
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    // 
    //   NOTES:
    public static void Shutdown(Boolean QuestionMessage, Boolean RestartSystem, Users.EXIT_CODE ExitCode)
    {
      DialogResult _dlg_rc;
      System.Diagnostics.Process _current_process;

      if (m_already_closing)
      {
        return;
      }

      m_already_closing = true;

      _dlg_rc = DialogResult.OK;

      if (QuestionMessage)
      {
        if (WindowManager.StartUpMode == WindowManager.StartUpModes.Explorer)
        {
          _dlg_rc = frm_message.Show(Resource.String("STR_FRM_CONTAINER_USER_MSG_EXIT"), Resource.String("STR_FRM_CONTAINER_USER_MSG_EXIT_TITLE"), MessageBoxButtons.OKCancel, Images.CashierImage.Shutdown, Cashier.MainForm);
        }
        else
        {
          _dlg_rc = frm_message.Show(Resource.String("STR_FRM_CONTAINER_USER_MSG_SHUTDOWN"), Resource.String("STR_FRM_CONTAINER_USER_MSG_SHUTDOWN_TITLE"), MessageBoxButtons.OKCancel, Images.CashierImage.Shutdown, Cashier.MainForm);
        }
      }

      try
      {
        if (_dlg_rc == DialogResult.OK)
        {
          WSIKeyboard.Hide();
          Log.Close();
          Users.SetUserLoggedOff(ExitCode);
          if (MainForm != null)
            MainForm.Dispose();
          _current_process = System.Diagnostics.Process.GetCurrentProcess();

          if (RestartSystem)
          {
            Application.Restart();
          }
          else
          {
            if (WindowManager.StartUpMode == WindowManager.StartUpModes.CashDesk)
            {
              if (Environment.GetEnvironmentVariable("LKS_VC_DEV") == null)
              {
                // Don't do Stop when restarting. Only when shutting down the system.
                HtmlPrinter.Stop();

                // Shutdown system: 1 - Restart, 2 - Shutdown
                Common_SystemShutdown(2);
              }
            }
          }

          // ACC 28-SEP-2012 Change Exit(0) to TerminateProcess (avoid "Program not responding. --> End Now")
          //Environment.Exit(0);

          TerminateProcess(_current_process.Handle, 0);
        }
        else
        {
          m_already_closing = false;
        }
      }
      catch (Exception ex)
      {
        // ATB 16-FEB-2017
        // Logging the exception
        Log.Exception(ex);
        if (WSI.Cashier.Cashier.MainForm != null)
          WSI.Cashier.Cashier.MainForm.Dispose();
      }
    }

    public static void SetDesktopWallpaper(WallpaperAction Type)
    {
      String _path;
      RegistryKey _key;

      _key = Registry.CurrentUser.OpenSubKey("Control Panel\\Desktop", true);

      switch (Type)
      {
        case WallpaperAction.Updating:
          _path = UpdatingWallpaper;
          break;
        default:
          _path = RunningWallpaper;
          break;
      }

      _key.SetValue(@"WallpaperStyle", "0");
      _key.SetValue(@"TileWallpaper", "1");

      SystemParametersInfo(20, 0, _path, 0x01 | 0x02);
    }

    public static CashierSessionInfo CashierSessionInfo()
    {
      CashierSessionInfo _csi;

      _csi = new CashierSessionInfo();
      _csi.CashierSessionId = Cashier.SessionId;
      _csi.TerminalId = (Int32)Cashier.TerminalId;
      _csi.UserId = Cashier.UserId;
      _csi.TerminalName = Cashier.TerminalName;
      _csi.UserName = Cashier.UserName;
      _csi.AuthorizedByUserId = Cashier.AuthorizedByUserId;
      _csi.AuthorizedByUserName = Cashier.AuthorizedByUserName;
      _csi.IsMobileBank = false;
      _csi.GamingDay = Cashier.GamingDay;

      _csi.UserType = GU_USER_TYPE.USER;

      return _csi;
    } // CashierSessionInfo

    public static bool IsClosed()
    {
      bool _isOpen;
      CashierSessionInfo _cashierSessionInfo;

      _isOpen = false;
      _cashierSessionInfo = Cashier.CashierSessionInfo();

      if (_cashierSessionInfo.CashierSessionId != 0)
      {
        _isOpen = true;
      }
      return _isOpen;
    }

  } // Cashier

  /// <summary>
  /// Web services functionality
  /// </summary>
  public static class ExternalWS
  {
    #region Exped

    /// <summary>
    /// Connects with Exped provider
    /// </summary>
    /// <param name="AccountId"></param>
    /// <param name="AmountToSubstract"></param>
    /// <param name="OpCode"></param>
    /// <returns></returns>
    public static Boolean ExpedConnect(Int64 AccountId, Currency NetAmount, OperationCode OpCode)
    {
      DialogResult result_frm;
      decimal? _check_amount;
      string _check_number;
        Misc.ExternalWS.InputParams = new ExternalValidationInputParameters(AccountId
                                                            , NetAmount            //NetAmount
                                                            , 0                 //GrossAmount
                                                            , 0 //OperationId
                                                            , OpCode);
     
        Misc.ExternalWS.OutputParams = new ExternalValidationOutputParameters();

      // First step, check if the amount is over the payment threshold
      if (ExternalPaymentAndSaleValidationCommon.HasToValidate(Misc.ExternalWS.InputParams, out Misc.ExternalWS.OutputParams))
      {
        // Second step, request the authorization to Exped
        if (Misc.ExternalWS.ExpedRequestAuthorization(AccountId, NetAmount, OpCode))
        {
          // If the operation is a refund, the behaviour is as follows
          if (ExternalPaymentAndSaleValidationCommon.GetOperationTypeFromOperationCode(OpCode) == TipoOperacion.Pago)
          {
            // Checking if the payment cash limit exists
            if (ExternalPaymentAndSaleValidationCommon.ExpedCashierPaymentInCashLimit != 0
              && NetAmount >= ExternalPaymentAndSaleValidationCommon.ExpedCashierPaymentInCashLimit)
            {
              // Creating the check window, inside will be checked if the total amount is over the payment threshold
              frm_bankcheck_info frm_BankCheckInfo;
              frm_BankCheckInfo = new frm_bankcheck_info(Misc.ExternalWS.InputParams.NetAmount,
                                                          ExternalPaymentAndSaleValidationCommon.ExpedCashierPaymentInCashLimit);
              result_frm = frm_BankCheckInfo.Show(out _check_amount, out _check_number);

              // It is necessary to save the check amount and number at the params
              if (result_frm == DialogResult.OK)
              {
                Misc.ExternalWS.InputParams.BankCheckAmount = (decimal)_check_amount;
                Misc.ExternalWS.InputParams.BankCheckNumber = _check_number;
                return true;
              }
              else if (result_frm == DialogResult.Cancel)
              {
                Misc.ExternalWS.OutputParams.ValidationMessage = "cancel";
                return false;
              }
              else
              {
                return false;
              }
            }
            else
            {
              return true;
            }
          }
        }
      }
      // If the validation is false, the input amount is under the threshold so the payment will be done
      else
      {
        return true;
      }
      // If anything goes wrong
      return false;
    }

    #endregion
  }
}
