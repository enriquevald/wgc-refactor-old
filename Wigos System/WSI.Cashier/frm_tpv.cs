﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_tpv.cs
// 
//   DESCRIPTION: Messagebox for TPV
// 
//        AUTHOR: Enric
// 
// CREATION DATE: 01-JUL-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 01-JUL-2016 ETP    First release.
// 06-OCT-2016 FAV    Fixed Bug 18280:Televisa: Add 'Canceled' status for TPV transactions
// 21-OCT-2016 ETP    Bug 19314: PinPad Card Payment Refactorized
// 24-OCT-2016 ETP    Fixed Bug 19314 Pin Pad recharge refactoring
// 26-OCT-2016 ETP    Fixed Bug 19314 Pin Pad recharge Wigos Rollback + Pinpad + Wigos Commit
// 10-NOV-2016 ATB    Bug 19499: Cashier: Unmanaged exception during a Pin Pad payment 
// 21-MAY-2017 ETP    Fixed Bug WIGOS-2997: Added voucher data.
// 19-OCT-2017 JML    Fixed Bug 30321:WIGOS-5924 [Ticket #9450] Fórmula datos tarjeta bancaria (entregado-faltante) - Resumen de sesión de caja
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using WSI.Cashier.Controls;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using WSI.Common.PinPad;
using WSI.PinPad;
using WSI.Common;
using System.Collections;
using System.Data.SqlClient;
using WSI.PinPad.Banorte.Adapter;

namespace WSI.Cashier
{
  public partial class frm_tpv : frm_base
  {
    #region " const "

    public const int MAX_RETRY = 9;

    #endregion " const "

    #region " Enums "

    private enum PINPAD_CONNECTION_STATUS
    {
      Not_Initialized = 0,
      Initialized = 1,
      Finished = 2,
      Failed_To_Initialize = 3,
      Failed_To_Pay = 4,
      Max_Retry_Reached = 5
    }
    #endregion " Enums "

    #region " Members "

    private String m_control_number;

    private BackgroundWorker m_worker;

    private PINPAD_CONNECTION_STATUS m_connection_status;
    private PinPadCommon m_pinpadcommon;
    private frm_yesno m_form_yes_no;

    private PinPadResponse m_pinpad_response;
    private CurrencyExchangeResult m_amount;

    private Boolean m_success;
    private Boolean m_work_done;
    private CardData m_card_data;
    private Int64 m_retry;

    private String m_control_number_hex;
    private Boolean m_cancel_operation;
    private String m_terminal_name;

    private PinPadOutput m_pinpad_data;

    #endregion " Members "

    #region " Properties "

    public Boolean Success { get; set; }
    public Boolean OperationCanceled
    {
      get { return m_cancel_operation; }
    }

    public PinPadOutput OutputData
    {
      get
      {
        return m_pinpad_data;
      }
      set
      {
        m_pinpad_data = value;
      }
    }



    #endregion " Properties "

    #region " Constructor "

    public frm_tpv(PinPadInput Input)
    {

      m_form_yes_no = new frm_yesno();
      m_form_yes_no.Opacity = 0.6;
      m_form_yes_no.Show();

      m_amount = Input.Amount;
      m_card_data = Input.Card;
      m_terminal_name = Input.TerminalName;
      InitializeComponent();
      InitControlResources();
      Init();
    } // frm_tpv

    #endregion " Constructor "

    #region " Public Methods "

    public Boolean Show(out PinPadOutput Output)
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_tpv", Log.Type.Message);

      this.ShowDialog();
      Output = OutputData;
      return m_success;
    } // Show

    #endregion " Public Methods "

    #region " Private Methods "

    /// <summary>
    /// Init form
    /// </summary>
    private void Init()
    {
      m_worker = new BackgroundWorker();
      m_worker.DoWork += new DoWorkEventHandler(m_worker_DoWork);
      m_worker.ProgressChanged += new ProgressChangedEventHandler(m_worker_ProgressChanged);
      m_worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(m_worker_RunWorkerCompleted);
      m_worker.WorkerReportsProgress = true;
      m_worker.WorkerSupportsCancellation = true;
      btn_retry.Enabled = false;
      m_retry = 0;
      m_success = false;
      m_work_done = false;
      pb_CircularProgressBar.Visible = false;
      m_connection_status = PINPAD_CONNECTION_STATUS.Not_Initialized;

      if (String.IsNullOrEmpty(m_control_number))
      {
        if (!GetControlNumber(out m_control_number_hex))
        {
          Log.Error("frm_tpv.Pay: control number not get");
          m_connection_status = PINPAD_CONNECTION_STATUS.Failed_To_Pay;
          m_success = false;
          return;
        }
        m_control_number = Misc.GetControlNumberBySequence(m_control_number_hex);
      }

    }

    /// <summary>
    /// Init Control Resources
    /// </summary>
    private void InitControlResources()
    {
      btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");
      btn_retry.Text = Resource.String("STR_RETRY");
    }

    /// <summary>
    /// Init Tpv parameters.
    /// </summary>
    private void InitTPV()
    {
      List<PinPadCashierTerminal> _pinpad_cashier_terminals_list;
      PinPadCashierTerminals _pinpad_cashier_terminals;
      PinPadCashierTerminal _pinpad_cashier_terminal;

      _pinpad_cashier_terminals = new PinPadCashierTerminals();
      // ATB 10-NOV-2016
      // Catching error for a possible TerminalID failure
      try
      {
        _pinpad_cashier_terminals.CashierId = (Int32)Cashier.TerminalId;
      }
      catch (Exception ex)
      {
        Log.Error("TerminalId failure: " + Cashier.TerminalId + "/n" + ex.Message);
        Log.Exception(ex);
      }

      // ATB 10-NOV-2016
      // Catching error for a possible Terminal List failure
      try
      {
        _pinpad_cashier_terminals_list = _pinpad_cashier_terminals.DB_GetEnabledList();
        if (_pinpad_cashier_terminals_list != null)
        {
          if (_pinpad_cashier_terminals_list.Count > 0)
          {
            _pinpad_cashier_terminal = _pinpad_cashier_terminals_list[0];

            switch (_pinpad_cashier_terminal.Type)
            {
              case PinPadType.BANORTE:
                m_pinpadcommon = new PinPadCommon((IPinPad)new PinPadBanorte(), _pinpad_cashier_terminal);
                break;
              case PinPadType.DUMMY:
#if DEBUG
                m_pinpadcommon = new PinPadCommon((IPinPad)new PinPadDummy(), _pinpad_cashier_terminal);
#endif
                Log.Warning("InitTPV: Dummy defined");
                break;
              default:
                Log.Error("InitTPV: not type defined");
                break;
            }


            m_pinpadcommon.Initialize();

            if (m_pinpadcommon.InitDevice())
            {
              m_connection_status = PINPAD_CONNECTION_STATUS.Initialized;
              InitializeBDTransaction();
            }
          }
        }
      }
      catch (Exception ex)
      {
        Log.Error("Terminal list failure: " + ex.Message);
        Log.Exception(ex);
      }

      if (m_connection_status != PINPAD_CONNECTION_STATUS.Initialized)
      {
        m_connection_status = PINPAD_CONNECTION_STATUS.Failed_To_Initialize;
      }
    }

    private void Pay()
    {
      m_pinpad_response = null;

#if DEBUG
      DialogResult _msbox_answer;

      if (m_pinpadcommon.GetPinPadMode() == "PRD")
      {
        _msbox_answer = frm_message.Show("El pago se va a generar en modo producción!!!\nSe realizarán cargos a su cuenta \n\n¿Estás seguro?", "Atención!!!!!!!", MessageBoxButtons.YesNo, MessageBoxIcon.Stop, this.ParentForm);
        if (!(_msbox_answer == DialogResult.Cancel))
        {
          _msbox_answer = frm_message.Show("El pago se va a generar en modo producción!!!\nSe realizarán cargos a su cuenta \n\n¿Estás seguro?", "Atención!!!!!!!", MessageBoxButtons.YesNo, MessageBoxIcon.Stop, this.ParentForm);
        }
        if (_msbox_answer == DialogResult.Cancel)
        {
          m_success = false;
          m_connection_status = PINPAD_CONNECTION_STATUS.Failed_To_Pay;
          return;
        }
      }
#endif

      try
      {

        using (DB_TRX _db_trx = new DB_TRX())
        {

          if (m_pinpadcommon.Pay(m_amount.InAmount, m_control_number, out m_pinpad_response))
          {
            m_connection_status = PINPAD_CONNECTION_STATUS.Finished;
            m_success = true;
          }
          else
          {
            m_connection_status = PINPAD_CONNECTION_STATUS.Failed_To_Pay;
            m_success = false;
            PinPadTransactions.UpdateDateUndoPinPad(m_control_number, _db_trx.SqlTransaction);
          }

          UpdateBBDDTransaction(_db_trx.SqlTransaction);

          _db_trx.Commit();
        }
      }
      catch (Exception Ex)
      {
        Log.Exception(Ex);
      }
    }


    private void CloseForm()
    {
      if (m_worker.IsBusy)
      {
        m_worker.CancelAsync();
      }
    }

    /// <summary>
    /// Insert transaction to BBDD
    /// </summary>
    private void InitializeBDTransaction()
    {
      bank_card _bank_card;
      CashierSessionInfo      _cashier_session_info;

      _bank_card = new bank_card();
      _bank_card.Initialize(CARD_TYPE.NONE, CARD_SCOPE.NONE);
      _cashier_session_info = Cashier.CashierSessionInfo();

      m_pinpad_data.PinPadTransaction = new PinPadTransactions
      {
        UserId = Cashier.UserId,
        AccountId = m_card_data.AccountId,
        CardType = _bank_card,
        CardIsoCode = CurrencyExchange.GetNationalCurrency(),
        TransactionAmount = m_amount.InAmount - m_amount.Comission,
        CommissionAmount = m_amount.Comission,
        TotalAmount = m_amount.InAmount,
        StatusCode = PinPadTransactions.STATUS.INITIALIZED,
        ControlNumber = m_control_number,
        Reference = String.Empty,
        Retry = m_retry,
        TerminalName = m_terminal_name,
        PinPadId = Convert.ToInt64(m_pinpadcommon.GetPinPadTerminalID())

      };

      using (DB_TRX _db_trx = new DB_TRX())
      {
        m_pinpad_data.PinPadTransaction.DB_Insert(_db_trx.SqlTransaction);
        m_pinpad_data.PinPadTransaction.UpdateCashierSessionHasPinPadOperation(_cashier_session_info.CashierSessionId, _db_trx.SqlTransaction);
        PinPadTransactions.InsertToUndoPinPad(m_control_number, m_pinpadcommon.GetPinPadType(), m_pinpadcommon.GetPinPadTerminalID(), 30, _db_trx.SqlTransaction);
        _db_trx.Commit();
      }

    }


    private void UpdateBBDDTransaction(SqlTransaction SqlTrx)
    {
      String _card_number;
      bank_card _bank_card;
      CashierSessionInfo _cashier_session_info;

      _cashier_session_info = Cashier.CashierSessionInfo();
      _card_number = m_pinpad_response.PinPadCard.CardNumber;
      if (_card_number != null)
      {
        _card_number = "************" + _card_number.Substring(_card_number.Length - 4);
      }
      _bank_card = new bank_card();
      _bank_card.Initialize(CARD_TYPE.NONE, CARD_SCOPE.NONE);

      m_pinpad_data.PinPadTransaction.BankName = m_pinpad_response.PinPadCard.CardBankName;
      m_pinpad_data.PinPadTransaction.CardNumber = _card_number;
      m_pinpad_data.PinPadTransaction.CardIsoCode = CurrencyExchange.GetNationalCurrency();
      m_pinpad_data.PinPadTransaction.CardHolder = m_pinpad_response.PinPadCard.CardHolder;
      m_pinpad_data.PinPadTransaction.MerchantId = Convert.ToInt32(m_pinpad_response.MerchandId);

      m_pinpad_data.PinPadTransaction.StatusCode = (m_cancel_operation && m_pinpad_response.StatusResponse == PinPadTransactions.STATUS.NO_RESPONSE
                                             ? PinPadTransactions.STATUS.CANCELED : m_pinpad_response.StatusResponse);
      m_pinpad_data.PinPadTransaction.Retry = m_retry;
      m_pinpad_data.PinPadTransaction.Reference = m_pinpad_response.Reference;
      m_pinpad_data.PinPadTransaction.AuthCode = m_pinpad_response.AuthorizationCode;
      m_pinpad_data.PinPadTransaction.ErrorMessage = m_pinpad_response.ResponseMessage;
      m_pinpad_data.PinPadTransaction.TerminalName = m_terminal_name;
      m_pinpad_data.PinPadTransaction.PinPadId = Convert.ToInt64(m_pinpadcommon.GetPinPadTerminalID());



      m_cancel_operation = false;

      m_pinpad_data.PinPadTransaction.DB_Insert(SqlTrx);
      m_pinpad_data.PinPadTransaction.UpdateCashierSessionHasPinPadOperation(_cashier_session_info.CashierSessionId, SqlTrx);

      if (m_success)
      {
        m_pinpad_data.ProductType = m_pinpad_response.ProductType;
        m_pinpad_data.CardType = m_pinpad_response.CardType;
        m_pinpad_data.ExpirationDate = m_pinpad_response.ExpirationDate;
        m_pinpad_data.AID = m_pinpad_response.AID;
        m_pinpad_data.TVR = m_pinpad_response.TVR;
        m_pinpad_data.TSI = m_pinpad_response.TSI;
        m_pinpad_data.APN = m_pinpad_response.APN;
        m_pinpad_data.AL = m_pinpad_response.AL;
        m_pinpad_data.EntryMode = m_pinpad_response.PinPadCard.POSEntryMode;
        m_pinpad_data.WithSignature = m_pinpad_response.WithPin;
      }

    }


    /// <summary>
    /// Get num of tries for this operation.
    /// </summary>
    private void GetBBDDRetry()
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        if (m_pinpad_data.PinPadTransaction.DB_GetRetryById(_db_trx.SqlTransaction))
        {
          m_retry = m_pinpad_data.PinPadTransaction.Retry;
        }
        else
        {
          Log.Warning("frm_tpv.btn_retry_Click --> BBDD Error: load retry by default.");
        }
      }
    }


    private Boolean GetControlNumber(out String HexSequence)
    {
      Int64 _seq_number;
      HexSequence = String.Empty;

      using (DB_TRX _db_trx = new DB_TRX())
      {
        if (Sequences.GetValue(_db_trx.SqlTransaction, SequenceId.PinPadTransaction, out _seq_number))
        {
          HexSequence = Convert.ToString(_seq_number, 16);
          _db_trx.Commit();
          return true;
        }
      }

      return false;
    }

    #endregion " private methods "

    #region " Worker Events "
    private void m_worker_DoWork(object sender, DoWorkEventArgs e)
    {
      int _i = 0;
      int _iTimer = 1000;
      String _str;
      _str = "";
      while (!m_worker.CancellationPending && !m_work_done)
      {
        _i++;
        switch (m_connection_status)
        {
          case PINPAD_CONNECTION_STATUS.Not_Initialized:
            _iTimer = 1000;
            m_worker.ReportProgress(_i, (object)Resource.String("STR_INITIALIZING_TPV"));
            InitTPV();
            break;
          case PINPAD_CONNECTION_STATUS.Initialized:
            _iTimer = 1000;
            m_worker.ReportProgress(_i, (object)Resource.String("STR_INSERT_CARD"));
            Pay();
            break;
          case PINPAD_CONNECTION_STATUS.Failed_To_Pay:
            _iTimer = 1000;
            m_worker.ReportProgress(_i, (object)Resource.String("STR_ERROR_TPV_RETRY", m_pinpad_data.PinPadTransaction.ErrorMessage));
            break;

          case PINPAD_CONNECTION_STATUS.Max_Retry_Reached:
            _iTimer = 1000;
            m_worker.ReportProgress(_i, (object)Resource.String("STR_MAX_RETRY_REACHED"));
            m_work_done = true;
            break;

          case PINPAD_CONNECTION_STATUS.Finished:
            _iTimer = 0;
            m_work_done = true;
            break;
          case PINPAD_CONNECTION_STATUS.Failed_To_Initialize:
            _iTimer = 1000;
            _str = _str.Length < 3 ? _str + "." : "";
            m_worker.ReportProgress(_i, (object)Resource.String("STR_FAILED_TO_INITIALIZE", _str));
            InitTPV();
            break;
        }

        Thread.Sleep(_iTimer);
      }
    } //m_worker_DoWork

    void m_worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
    {
      this.lbl_text.Text = e.UserState.ToString().Replace("\\r\\n", "\r\n").Replace("\n\"", "\"");
      switch (m_connection_status)
      {
        case PINPAD_CONNECTION_STATUS.Finished:
          btn_retry.Enabled = false;
          break;
        case PINPAD_CONNECTION_STATUS.Failed_To_Pay:
          btn_retry.Enabled = true;
          break;
        default:
          break;
      }
      pb_CircularProgressBar.Visible = m_connection_status == PINPAD_CONNECTION_STATUS.Initialized || m_connection_status == PINPAD_CONNECTION_STATUS.Failed_To_Initialize;
    } //m_worker_ProgressChanged

    void m_worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
    {
      int _iTimer = 2000;

      if (m_connection_status == PINPAD_CONNECTION_STATUS.Finished)
      {
        lbl_text.Text = Resource.String("STR_OPERATION_ACCEPTED");
      }
      else
      {
        lbl_text.Text = Resource.String("STR_OPERATION_CANCELLED");
      }

      this.Refresh();
      Thread.Sleep(_iTimer);

      Misc.WriteLog("[FORM CLOSE] frm_m_worker_runworkercompleted", Log.Type.Message);
      this.Close();

    } // m_worker_RunWorkerCompleted

    #endregion " Worker Events "

    #region " Events "

    private void frm_tpv_Shown(object sender, EventArgs e)
    {
      // Communication main thread start
      m_worker.RunWorkerAsync();
    } //frm_tpv_Shown

    private void frm_tpv_FormClosed(object sender, FormClosedEventArgs e)
    {
      if (m_worker.IsBusy)
      {
        m_worker.CancelAsync();
      }
      m_form_yes_no.Hide();
      // ATB 10-NOV-2016
      // Clearing memory for the form
      m_form_yes_no.Dispose();
      m_pinpadcommon.ReleaseDevice();
    } //frm_tpv_FormClosed


    private void btn_login_cancel_Click(object sender, EventArgs e)
    {
      m_cancel_operation = true;

      Misc.WriteLog("[FORM CLOSE] frm_login_cancel", Log.Type.Message);
      this.Close();
    } //btn_login_cancel_Click

    private void btn_retry_Click(object sender, EventArgs e)
    {
      btn_retry.Enabled = false;
      GetBBDDRetry();
      m_retry++;
      if (m_retry < MAX_RETRY)
      {
        m_control_number = Misc.GetControlNumberBySequence(m_control_number_hex, m_retry);
        InitializeBDTransaction();
        m_connection_status = PINPAD_CONNECTION_STATUS.Initialized;
      }
      else
      {
        m_connection_status = PINPAD_CONNECTION_STATUS.Max_Retry_Reached;
      }
    } //btn_retry_Click

    #endregion " Events "

  }
}
