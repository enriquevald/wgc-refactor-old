﻿//------------------------------------------------------------------------------
// Copyright © 2015-2020 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: TopBarInfo.cs
// 
//   DESCRIPTION: Class to manage the uc_top_bar
//
//        AUTHOR: Didac Campanals
// 
// CREATION DATE: 28-OCT-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 28-OCT-2015 DCS     First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using WSI.Common;

namespace WSI.Cashier
{
  public class SystemInfo
  {
    public enum StatusInfo
    {
      UNKNOWN = -1,
      NONE = 0,
      OK = 1,
      WARNING = 2,
      ERROR = 3,
      NOT_EXPIRED = 5,
      EXPIRED = 6,
    }

    #region Properties

    private DateTime m_cash_session_opening_date;
    public StatusInfo m_gaming_day_expired;
    public StatusInfo m_work_shift_expired;

    public StatusInfo GamingDayExpired
    {
      get
      {
        return m_gaming_day_expired;
      }
      set
      {
        m_gaming_day_expired = value;
      }
    }

    public StatusInfo WorkShiftExpired
    {
      get
      {
        return m_work_shift_expired;
      }
      set
      {
        m_work_shift_expired = value;
      }
    }

    public DateTime CashSessionOpeningDate
    {
      get
      {
        return m_cash_session_opening_date;
      }
      set
      {
        m_cash_session_opening_date = value;
      }
    }

    #endregion Properties

    #region public Methods

    public String GetAliasTerminal()
    {
      String _text;

      _text = Computer.ShortAlias(Environment.MachineName); // WKS-48

      if (GamingTablesSessions.GamingTableInfo.IsGamingTable)
      {
        _text += " " + Resource.String("STR_TABLE"); // WKS-48 (TABLE)
      }

      return _text;
    }

    public String GetTextStatusCashier()
    {
      String _date_time;
      TimeSpan _elapsed_time;
      String _status_cashier;

      _status_cashier = String.Empty;
      _date_time = GetTimeClock();

      if (Cashier.SessionId != 0)
      {
        _elapsed_time = WGDB.Now - CashSessionOpeningDate;

        if (_elapsed_time.TotalHours < 24)
        {
          _date_time = string.Format("{0:00}:{1:00}", _elapsed_time.Hours, _elapsed_time.Minutes);
        }
        else
        {
          _date_time = "+24H";
        }

        _status_cashier = Resource.String("STR_CASH_DESK_STATUS_OPENED") + " " + _date_time;
      }
      else
      {
        _status_cashier = Resource.String("STR_CASH_DESK_STATUS_CLOSED");
      }

      return _status_cashier;
    }

    public String GetTimeClock()
    {
      String _date_time;
      String _time_format;
            
      _date_time = "";
      _time_format = "STR_FORMAT_CurrentTime";

      _date_time = WGDB.Now.ToString(Resource.String(_time_format)).ToUpper();
      _date_time = _date_time.Trim();

      return _date_time;      
    }

    public String GetDateClock()
    {
      String _date_time;
      String _time_format;

      _date_time = "";
      _time_format = "STR_FORMAT_CurrentDate";

      _date_time = WGDB.Now.ToString(Resource.String(_time_format)).ToUpper();
      _date_time = _date_time.Trim();

      return _date_time;
    }

    public StatusInfo GetCashierStatus()
    {
      CASHIER_STATUS _status;

      _status = CashierBusinessLogic.ReadCashierStatus(Cashier.SessionId);

      switch (_status)
      {
        case CASHIER_STATUS.OPEN:
          
          if (WorkShiftExpired == SystemInfo.StatusInfo.EXPIRED || GamingDayExpired == SystemInfo.StatusInfo.EXPIRED)
          {
            return StatusInfo.WARNING;
          }
          else
          {
            return StatusInfo.OK;
          }
          
        case CASHIER_STATUS.CLOSE:
        case CASHIER_STATUS.ERROR:
        default:
          return StatusInfo.ERROR;
      }
    }
    
    public SystemInfo()
    {
      m_gaming_day_expired = StatusInfo.UNKNOWN;
      m_work_shift_expired = StatusInfo.UNKNOWN;
      m_cash_session_opening_date = DateTime.MinValue;

    }

    #endregion public Methods
    
  }

  public class GenericThread
  {
    public delegate void ValuesChanged(Int64 ThreadTimestamp);
    public event ValuesChanged ValuesChangedEvent;

    private Boolean m_pause_thread; 
    private Int64 m_internal_sequence;
    public Int32 m_sleep_time = 5000;  // by default 1 sec
    private static AutoResetEvent m_ev_changed = new AutoResetEvent(true);
  
    public Boolean PauseThread
    {
      get
      {
        return m_pause_thread;
      }
      set
      {
        m_pause_thread = value;
      }
    }

    public Int32 SleepTime
    {
      get
      {
        return m_sleep_time;
      }
      set
      {
        m_sleep_time = value;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Initializes GenericThread
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public void Init()
    {
      Thread _thread;

      _thread = new Thread(UdateFunctionThread);
      _thread.Name = "GenericThread_" + WGDB.Now.TimeOfDay;
      _thread.Start();
      
      this.PauseThread = false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : UdateFunctionThread thread
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private void UdateFunctionThread()
    {
      
      while (true)
      {
        try
        {
          m_ev_changed.WaitOne(m_sleep_time, false);

          if (PauseThread)
          {
            continue;
          }

          lock (this)
          {
            m_internal_sequence++;
            
            if (ValuesChangedEvent != null)
            {
              ValuesChangedEvent(m_internal_sequence);
            }
          } // lock this
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Force thread
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public Int64 ForceRefresh()
    {
      Int64 _next_sequence;
      Int64 _current_sequence;

      lock (this)
      {
        _current_sequence = m_internal_sequence;
        _next_sequence = _current_sequence + 1;
        m_ev_changed.Set();
      }

      return _next_sequence;
    }
  }  
}
