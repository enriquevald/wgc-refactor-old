﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: CreditLine.cs
// 
//   DESCRIPTION: Miscellaneous utilities for Credit lines
// 
//        AUTHOR: David Perelló
// 
// CREATION DATE: 28-MAR-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 28-MAR-2017 DPC    First release (Header).
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Windows.Forms;
using WSI.Common;
using WSI.Common.CreditLines;

namespace WSI.Cashier.CreditLines
{
  public class CreditLine
  {

    /// <summary>
    /// Generic method for get a marker from cashier
    /// </summary>
    /// <param name="Card"></param>
    /// <param name="AddSubAmount"></param>
    /// <param name="ParentForm"></param>
    /// <param name="CashierSessionInfo"></param>
    /// <param name="TypeGetMarker"></param>
    /// <param name="Trx"></param>
    /// <param name="OperationId"></param>
    /// <returns></returns>
    public static ResultGetMarker getMarker(CardData Card, Currency AddSubAmount, Form ParentForm, CashierSessionInfo CashierSessionInfo, CreditLineMovements.CreditLineMovementType TypeGetMarker, SqlTransaction Trx, out Int64 OperationId)
    {

      ResultGetMarker _result_get_marker;
      String _msg_error;
      String _error_str;

      OperationId = 0;

      try
      {
        _result_get_marker = Card.CreditLineData.CheckGetMarker(AddSubAmount);

        if (_result_get_marker != ResultGetMarker.OK)
        {
          switch (_result_get_marker)
          {
            case ResultGetMarker.NOT_APPROVED:
            case ResultGetMarker.NOT_SUFICIENT_AMOUNT_AVAIBLE:
              _msg_error = (_result_get_marker == ResultGetMarker.NOT_APPROVED ? Resource.String("STR_CREDIT_LINE_ALERT_RECHARGE_NOT_APROVED") : Resource.String("STR_CREDIT_LINE_ALERT_BALANCE_MISMATCH"));
              frm_message.Show(_msg_error, Resource.String("STR_APP_GEN_MSG_WARNING"), MessageBoxButtons.OK, MessageBoxIcon.Warning, ParentForm);

              break;
            case ResultGetMarker.NEED_MORE_PERMISSIONS:
              if (frm_message.Show(Resource.String("STR_CREDIT_LINE_ALERT_USE_TTO"), Resource.String("STR_APP_GEN_MSG_WARNING"), MessageBoxButtons.OKCancel, MessageBoxIcon.Error, ParentForm) != DialogResult.OK)
              {
                break;
              }

              if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.CreditLine_Get_Marker_TTO,
                                             ProfilePermissions.TypeOperation.RequestPasswd, ParentForm, out _error_str))
              {
                break;
              }

              _result_get_marker = ResultGetMarker.OK;

              break;
          }

          if (_result_get_marker != ResultGetMarker.OK)
          {
            return ResultGetMarker.KO;
          }
        }

        //Create movements for CreditLine (Account, Cashier, Credit line)
        if (!WSI.Common.CreditLines.CreditLine.DoMovementsCreditLine(Card, AddSubAmount, CashierSessionInfo, TypeGetMarker, null, Trx, out OperationId))
        {
          frm_message.Show(Resource.String("STR_CREDIT_LINE_ERROR_GET_MARKER"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, ParentForm);

          return ResultGetMarker.KO;
        }

        return ResultGetMarker.OK;

      }
      catch (Exception _ex)
      {
        Log.Error("CreditLine.getMarker");
        Log.Exception(_ex);

        return ResultGetMarker.KO;
      }

    }

  }
}
