﻿namespace WSI.Cashier
{
  partial class frm_pinpad_config
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
      this.btn_new = new WSI.Cashier.Controls.uc_round_button();
      this.btn_edit = new WSI.Cashier.Controls.uc_round_button();
      this.btn_close = new WSI.Cashier.Controls.uc_round_button();
      this.dgw_pinpads = new WSI.Cashier.Controls.uc_DataGridView();
      this.pnl_data.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dgw_pinpads)).BeginInit();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.Controls.Add(this.dgw_pinpads);
      this.pnl_data.Controls.Add(this.btn_new);
      this.pnl_data.Controls.Add(this.btn_edit);
      this.pnl_data.Controls.Add(this.btn_close);
      this.pnl_data.Size = new System.Drawing.Size(516, 371);
      // 
      // btn_new
      // 
      this.btn_new.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(47)))), ((int)(((byte)(50)))));
      this.btn_new.FlatAppearance.BorderSize = 0;
      this.btn_new.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_new.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_new.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_new.Image = null;
      this.btn_new.IsSelected = false;
      this.btn_new.Location = new System.Drawing.Point(181, 293);
      this.btn_new.Name = "btn_new";
      this.btn_new.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_new.Size = new System.Drawing.Size(155, 60);
      this.btn_new.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.TERTIARY;
      this.btn_new.TabIndex = 9;
      this.btn_new.Text = "XNEW";
      this.btn_new.UseVisualStyleBackColor = false;
      this.btn_new.Click += new System.EventHandler(this.btn_new_Click);
      // 
      // btn_edit
      // 
      this.btn_edit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_edit.FlatAppearance.BorderSize = 0;
      this.btn_edit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_edit.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_edit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_edit.Image = null;
      this.btn_edit.IsSelected = false;
      this.btn_edit.Location = new System.Drawing.Point(347, 293);
      this.btn_edit.Name = "btn_edit";
      this.btn_edit.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_edit.Size = new System.Drawing.Size(155, 60);
      this.btn_edit.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_edit.TabIndex = 10;
      this.btn_edit.Text = "XEDIT";
      this.btn_edit.UseVisualStyleBackColor = false;
      this.btn_edit.Click += new System.EventHandler(this.btn_edit_Click);
      // 
      // btn_close
      // 
      this.btn_close.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_close.FlatAppearance.BorderSize = 0;
      this.btn_close.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_close.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_close.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_close.Image = null;
      this.btn_close.IsSelected = false;
      this.btn_close.Location = new System.Drawing.Point(15, 293);
      this.btn_close.Name = "btn_close";
      this.btn_close.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_close.Size = new System.Drawing.Size(155, 60);
      this.btn_close.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_close.TabIndex = 8;
      this.btn_close.Text = "XCLOSE";
      this.btn_close.UseVisualStyleBackColor = false;
      this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
      // 
      // dgw_pinpads
      // 
      this.dgw_pinpads.AllowUserToAddRows = false;
      this.dgw_pinpads.AllowUserToDeleteRows = false;
      this.dgw_pinpads.AllowUserToResizeColumns = false;
      this.dgw_pinpads.AllowUserToResizeRows = false;
      this.dgw_pinpads.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.dgw_pinpads.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
      this.dgw_pinpads.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.dgw_pinpads.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.dgw_pinpads.ColumnHeaderHeight = 35;
      this.dgw_pinpads.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
      dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      dataGridViewCellStyle1.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
      dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
      dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
      this.dgw_pinpads.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
      this.dgw_pinpads.ColumnHeadersHeight = 35;
      this.dgw_pinpads.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
      this.dgw_pinpads.CornerRadius = 10;
      this.dgw_pinpads.DefaultCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.dgw_pinpads.DefaultCellForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.dgw_pinpads.DefaultCellSelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.dgw_pinpads.DefaultCellSelectionForeColor = System.Drawing.Color.White;
      dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      dataGridViewCellStyle2.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.White;
      dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
      this.dgw_pinpads.DefaultCellStyle = dataGridViewCellStyle2;
      this.dgw_pinpads.EnableHeadersVisualStyles = false;
      this.dgw_pinpads.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.dgw_pinpads.GridColor = System.Drawing.Color.LightGray;
      this.dgw_pinpads.HeaderBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
      this.dgw_pinpads.HeaderDefaultCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.dgw_pinpads.HeaderDefaultCellForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.dgw_pinpads.Location = new System.Drawing.Point(15, 16);
      this.dgw_pinpads.Name = "dgw_pinpads";
      this.dgw_pinpads.ReadOnly = true;
      this.dgw_pinpads.RowHeadersVisible = false;
      this.dgw_pinpads.RowHeadersWidth = 20;
      this.dgw_pinpads.RowTemplate.Height = 35;
      this.dgw_pinpads.RowTemplateHeight = 35;
      this.dgw_pinpads.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
      this.dgw_pinpads.Size = new System.Drawing.Size(487, 256);
      this.dgw_pinpads.Style = WSI.Cashier.Controls.uc_DataGridView.DataGridViewStyle.WITH_ALL_ROUND_CORNERS;
      this.dgw_pinpads.TabIndex = 11;
      // 
      // frm_pinpad_config
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(516, 426);
      this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.Name = "frm_pinpad_config";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Style = WSI.Cashier.Controls.frm_base.BaseFormStyle.PRINCIPAL;
      this.Text = "frm_pinpad_config";
      this.pnl_data.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.dgw_pinpads)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private Controls.uc_round_button btn_new;
    private Controls.uc_round_button btn_edit;
    private Controls.uc_round_button btn_close;
    private Controls.uc_DataGridView dgw_pinpads;
  }
}