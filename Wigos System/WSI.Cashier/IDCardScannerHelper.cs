﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Text;
using System.Windows.Forms;
using WSI.Common;

namespace WSI.Cashier
{

	public static class AccountPhotoFunctions
	{
    public static Boolean SaveAccountPhoto(Int64 AccountId, Image Photo, SqlTransaction Trx)
    {
      try
      {
        StringBuilder _sb;
        MemoryStream _ms;
        Byte[] _buf;
        int _expected0;
        int _expected1;
        int _affected;

        _buf = null;
        _sb = new StringBuilder();

        if (Photo == null)
        {
          _expected0 = 0;
          _expected1 = 1;
          _sb.AppendLine("UPDATE ACCOUNT_PHOTO SET APH_PHOTO = NULL WHERE APH_ACCOUNT_ID = @pAccountId");
        }
        else
        {
          _expected0 = 1;
          _expected1 = 1;
          _ms = new MemoryStream();
          Photo.Save(_ms, ImageFormat.Jpeg);
          _buf = new Byte[_ms.Position];
          _ms.Position = 0;
          Array.Copy(_ms.GetBuffer(), _buf, _buf.Length);

          _sb.AppendLine("IF NOT EXISTS (SELECT 1 FROM ACCOUNT_PHOTO WHERE APH_ACCOUNT_ID = @pAccountId)");
          _sb.AppendLine("  INSERT INTO ACCOUNT_PHOTO (APH_ACCOUNT_ID, APH_PHOTO) VALUES (@pAccountId, @pPhoto)");
          _sb.AppendLine("ELSE");
          _sb.AppendLine("  UPDATE ACCOUNT_PHOTO SET APH_PHOTO = @pPhoto WHERE APH_ACCOUNT_ID = @pAccountId");
        }

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _cmd.Parameters.Add("@pPhoto", SqlDbType.Image).Value = _buf;

          _affected = _cmd.ExecuteNonQuery();
          if (_affected == _expected0 || _affected == _expected1)
          {
            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }
    public static void SetAccountPhoto(PictureBox PBox, Int64 AccountId, GENDER Gender, out Boolean ExistImage)
    {
      Boolean _loaded;
      ExistImage = false;

      try
      {
        PBox.Image = GetAccountPhoto(AccountId, Gender, out _loaded);
        PBox.Tag = 0; // No changes

        ExistImage = _loaded;

        if (_loaded)
        {
          PBox.Size = new Size(48, 46);
        }
        else
        {
          PBox.Size = new Size(48, 46);
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    }
		public static Image GetAccountPhoto(Int64 AccountId, GENDER Gender, out Boolean PhotoLoaded)
    {
      PhotoLoaded = false;
      try
      {
        if (AccountId > 0)
        {
          try
          {
            using (DB_TRX _trx = new DB_TRX())
            {
              Image _photo;

              LoadAccountPhoto(AccountId, out _photo, _trx.SqlTransaction);
              if (_photo != null)
              {
                PhotoLoaded = true;

                return _photo;
              }
            }
          }
          catch (Exception _ex)
          {
            Log.Exception(_ex);
          }
        }

        switch (Gender)
        {
          case GENDER.MALE: return Resources.ResourceImages.male_user;
          case GENDER.FEMALE: return Resources.ResourceImages.female_user;
          // 04-OCT-2015 JML TODO Imagen a cambiar por la de anonimo.
          default: return Resources.ResourceImages.anonymous_user;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      return null;
    }
    public static Boolean LoadAccountPhoto(Int64 AccountId, out Image Photo, SqlTransaction Trx)
    {
      Photo = null;
      
      try
      {
        Byte[] _buf;
        StringBuilder _sb;

        _sb = new StringBuilder();

        _sb.AppendLine("SELECT APH_PHOTO FROM ACCOUNT_PHOTO WHERE APH_ACCOUNT_ID = @pAccountId");

        _buf = null;
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _buf = (Byte[])_cmd.ExecuteScalar();
        }

        if (_buf == null)
        {
          return true;
        }
        if (_buf.Length == 0)
        {
          return true;
        }

        try
        {
          MemoryStream _ms;
          Image _photo;
          _ms = new MemoryStream(_buf, false);
          _photo = Image.FromStream(_ms);
          Photo = _photo;
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }
        
        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }
    public static Boolean HasAccountPhoto(Int64 AccountId, SqlTransaction Trx)
    {
      try
      {
				int _has_photo = 0;
        StringBuilder _sb;

        _sb = new StringBuilder();

        _sb.AppendLine("SELECT count(APH_ACCOUNT_ID) FROM ACCOUNT_PHOTO WHERE APH_ACCOUNT_ID = @pAccountId");
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _has_photo = (int)_cmd.ExecuteScalar();
        }

        if (_has_photo == 1)
        {
          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      return false;
    }
  }
}
