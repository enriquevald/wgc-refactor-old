﻿//------------------------------------------------------------------------------
// Copyright © 2009 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_payment_method.cs
// 
//   DESCRIPTION: Implements the following classes:
//                1. uc_payment_method
//
//        AUTHOR: DHA
// 
// CREATION DATE: 22-MAR-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 22-MAR-2016 DHA    First release.
// 15-APR-2016 RAB    Product Backlog Item 10855: Tables (Phase 1): Currency/chip Selector.
// 27-APR-2016 DHA    Product Backlog Item 10825: added chips types to cage concepts
// 06-JUL-2016 DHA    Product Backlog Item 15064: multicurrency chips sale/purchase
// 05-AGO-2016 LTC    Product Backlog Item 14990:TPV Televisa: Cashier Withdrawal
// 21-MAR-2017 DPC    PBI 25788:CreditLine - Get a Marker
// 24-MAY-2017 JML    PBI 27484:WIGOS-1226 Win / loss - Introduce the WinLoss - EDITION SCREEN - PER DENOMINATION
// 24-MAY-2017 JML    PBI 27486:WIGOS-1227 Win / loss - Introduce the WinLoss - EDITION SCREEN - ACCUMULATED
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WSI.Common;

namespace WSI.Cashier
{
  public partial class uc_payment_method : UserControl
  {

    #region Members

    private Boolean m_show_currency_exchange;
    private VoucherTypes m_voucher_type;
    private List<CurrencyExchange> m_currencies;
    private CurrencyExchange m_currency_exchange;
    private CurrencyExchangeSubType m_selected_bank_card_type;
    private Int64? m_gaming_table_id;
    private FeatureChips.ChipsOperation.Operation m_chip_operation;
    private string m_locked_iso_code;
    
    #endregion Members

    #region Events

    public event CurrencyChangedHandler CurrencyChanged;
    public delegate void CurrencyChangedHandler(CurrencyExchange CurrencyExchange);

    #endregion Events

    #region Properties

    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    public String HeaderText
    {
      get
      {
        return gb_payment.HeaderText;
      }
      set
      {
        gb_payment.HeaderText = value;
      }
    }

    public Boolean BtnSelectedCurrencyEnabled
    {
      get
      {
        return this.btn_selected_currency.Enabled;
      }

      set
      {
        this.btn_selected_currency.Enabled = value;
      }
    }

    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    public VoucherTypes VoucherType
    {
      get
      {
        return m_voucher_type;
      }

      set
      {
        m_voucher_type = value;
      }
    }

    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    public List<CurrencyExchange> CurrenciesList
    {
      get
      {
        return m_currencies;
      }

    }

    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    public CurrencyExchange CurrencyExchange
    {
      get
      {
        return m_currency_exchange;
      }

      set
      {
        m_currency_exchange = value;

        if (m_currencies != null && m_currency_exchange != null)
        {
          SetCurrencyExchange();
          if (CurrencyChanged != null)
          {
            CurrencyChanged(m_currency_exchange);
          }
        }
      }
    }

    public CurrencyExchangeSubType SelectedBankCardType
    {
      get
      {
        return m_selected_bank_card_type;
      }
    }

    public Int64? GamingTableId
    {
      get
      {
        return m_gaming_table_id;
      }
    }

    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    public FeatureChips.ChipsOperation.Operation ChipOperation
    {
      get
      {
        return m_chip_operation;
      }
    }

    public Boolean showPaymentCreditLine { get; set; }
    
    #endregion Properties

    #region Public Methods

    public uc_payment_method()
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[UC LOAD] uc_payment_method", Log.Type.Message);

      InitializeComponent();

      m_selected_bank_card_type = CurrencyExchangeSubType.NONE;
      m_show_currency_exchange = GeneralParam.GetBoolean("Cashier.Recharge", "ShowCurrencyExchange", false);

      gb_payment.HeaderText = Resource.String("STR_FRM_AMOUNT_CURRENCY_GROUP_BOX");
      btn_selected_currency.Text = Resource.String("STR_FRM_AMOUNT_CHANGE_BUTTON");

      InitControl(CurrenciesList, GamingTableId, ChipOperation);
    }


    //------------------------------------------------------------------------------
    // PURPOSE : Initialize control
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    public void InitControl(List<CurrencyExchange> CurrenciesList, Int64? GamingTableId, FeatureChips.ChipsOperation.Operation ChipOperation)
    {
      InitControl(CurrenciesList, GamingTableId, ChipOperation, null);
    }

    public void InitControl(List<CurrencyExchange> CurrenciesList, Int64? GamingTableId, FeatureChips.ChipsOperation.Operation ChipOperation, String IsoCode)
    {
      m_chip_operation = ChipOperation;
      m_gaming_table_id = GamingTableId;
      m_locked_iso_code = IsoCode;

      if (GamingTableId <= 0)
      {
        // Default gaming table
        m_gaming_table_id = null;
      }

      // Force to reload chips sets
      FeatureChips.ChipsSets.GamingTableId = GamingTableId;

      m_currencies = FeatureChips.FilterAllowedCurrenciesAndChips(m_chip_operation, CurrenciesList, m_gaming_table_id, IsoCode);
      
    } // InitControl

    //------------------------------------------------------------------------------
    // PURPOSE : Count currencies allowed
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    public int GetCurrenciesCount()
    {
      int _currencies_number;
      _currencies_number = m_currencies.Count;

      foreach (CurrencyExchange _currency in m_currencies)
      {
        if (!CurrencyAllowed(_currency))
        {
          _currencies_number--;
        }
      }

      return _currencies_number;
    } // GetCurrenciesCount

    //------------------------------------------------------------------------------
    // PURPOSE : Select next currency allowed
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    public void SelectNextCurrencyAllowed()
    {
      Int32 _index;

      _index = m_currencies.IndexOf(m_currency_exchange);
      _index = (_index + 1) % m_currencies.Count;

      SelectNextCurrencyAllowed(m_currencies[_index]);

      if (CurrencyChanged != null)
      {
        CurrencyChanged(m_currency_exchange);
      }

    } // SelectNextCurrencyAllowed

    #endregion Public Methods

    #region Private Methods

    private void btn_selected_currency_Click(object sender, EventArgs e)
    {
      ShowCurrenciesForm(false);
    }

    // ATB 03-NOV-2016
    /// <summary>
    /// Opens the currencies popup to select the active currency
    /// </summary>
    /// <param name="OnlyShowCurrencies"></param>
    /// <param name="ShowPopUp"></param>
    public void ShowCurrenciesForm(Boolean OnlyShowCurrencies)
    {
      frm_browse_currencies frm;
      CurrencyExchange _currency_returned;
      Int32 _index;
      Int32 _count_currencies = 0;
      CurrencyExchange _previous_currency_exchange;

      //DPC 21-MAR-2017: Remove the currencies are type credit line
      RemoveCurrenciesCreditLine(m_currencies);

      _previous_currency_exchange = m_currency_exchange;

      frm = new frm_browse_currencies();

      // Select currency only when card is paid
      if ((m_voucher_type == VoucherTypes.CardAdd
        || m_voucher_type == VoucherTypes.CloseCash
        || m_voucher_type == VoucherTypes.DropBoxCount
        || m_voucher_type == VoucherTypes.OpenCash
        || m_voucher_type == VoucherTypes.FillIn
        || m_voucher_type == VoucherTypes.FillOut
        || m_voucher_type == VoucherTypes.AmountRequest
        || m_voucher_type == VoucherTypes.ChipsStock
        || m_voucher_type == VoucherTypes.CageConcepts
        || m_voucher_type == VoucherTypes.ChipsPurchaseAmountInput
        || m_voucher_type == VoucherTypes.ChipsSaleAmountInput
        || m_voucher_type == VoucherTypes.ChangeChipsIn
        || m_voucher_type == VoucherTypes.ChangeChipsOut
        || m_voucher_type == VoucherTypes.WinLoss)
       && (m_currencies.Count > 0))
      {
        _count_currencies = GetCurrenciesCount();

        // ATB 03-NOV-2016
        // 3 different cases:
        // - The CP ShowCurrencyExchange is active and there are more than one currency to select
        // - More than 3 currencies to select, the voucher type is CardAdd and the CP ShowCurrencyExchange is active
        // - More than 3 currencies
        if (m_show_currency_exchange && _count_currencies > 1 || (_count_currencies > 3 && m_voucher_type == VoucherTypes.CardAdd && m_show_currency_exchange) || _count_currencies > 3)
        {
          try
          {
            frm.Show(m_currencies, out _currency_returned, m_voucher_type);
            if (_currency_returned != null)
            {
              m_currency_exchange = _currency_returned;
            }
          }
          finally
          {
            // MPO 15-APR-2014
            frm.Dispose();
          }
        }
        else if (!OnlyShowCurrencies)
        {
          _index = m_currencies.IndexOf(m_currency_exchange);
          _index = (_index + 1) % m_currencies.Count;

          m_currency_exchange = m_currencies[_index];
          m_currency_exchange = SelectNextCurrencyAllowed(m_currency_exchange);
        }

        SetCurrencyExchange();

        if (CurrencyChanged != null && _previous_currency_exchange != m_currency_exchange)
        {
          CurrencyChanged(m_currency_exchange);
        }
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Put the flag corresponding with Iso Code
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    private void SetCurrencyImage()
    {
      CurrencyExchangeProperties _properties;

      _properties = CurrencyExchangeProperties.GetProperties(m_currency_exchange.CurrencyCode);
      if (_properties != null)
      {
        pb_currency.Image = _properties.GetIcon(m_currency_exchange.Type, pb_currency.Size.Width, pb_currency.Size.Height, false);
      }
      else
      {
        pb_currency.Image = null;
      }
    } // SetCurrencyImage

    //------------------------------------------------------------------------------
    // PURPOSE : Select next currency allowed
    //
    //  PARAMS :
    //      - INPUT :
    //              CurrencyExchange: CurrencyExchange
    //
    //      - OUTPUT :
    //
    private CurrencyExchange SelectNextCurrencyAllowed(CurrencyExchange CurrencyExchange)
    {
      Int32 _index;

      m_currency_exchange = CurrencyExchange;

      if (!CurrencyAllowed(m_currency_exchange))
      {
        _index = m_currencies.IndexOf(CurrencyExchange);
        _index = (_index + 1) % m_currencies.Count;

        m_currency_exchange = SelectNextCurrencyAllowed(m_currencies[_index]);
      }

      SetCurrencyExchange();

      return m_currency_exchange;
    } // SelectNextCurrencyAllowed

    //------------------------------------------------------------------------------
    // PURPOSE : Set selected currency exchange
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    private void SetCurrencyExchange()
    {
      bool _contains;

      m_selected_bank_card_type = CurrencyExchangeSubType.NONE;

      _contains = false;
      foreach (CurrencyExchange _ce in m_currencies)
      {
        if ((m_currency_exchange.CurrencyCode == _ce.CurrencyCode) && (m_currency_exchange.Type == _ce.Type))
        {
          _contains = true;
          break;
        }
      }

      if (_contains)
      {
        switch (m_currency_exchange.Type)
        {
          case CurrencyExchangeType.CURRENCY:
            lbl_paymet_method.Text = Resource.String("STR_FRM_AMOUNT_INPUT_CASH_PAYMENT") + "\n";
            lbl_paymet_method.Text += m_currency_exchange.Description;
            lbl_currency_iso_code.Text = m_currency_exchange.CurrencyCode;
            break;

          case CurrencyExchangeType.CASINOCHIP:
          case CurrencyExchangeType.CASINO_CHIP_RE:
          case CurrencyExchangeType.CASINO_CHIP_NRE:
          case CurrencyExchangeType.CASINO_CHIP_COLOR:
            lbl_paymet_method.Text = m_currency_exchange.Description;
            lbl_currency_iso_code.Text = "";
            break;

          case CurrencyExchangeType.CARD:
            switch (m_currency_exchange.SubType)
            {
              case CurrencyExchangeSubType.NONE:
                lbl_paymet_method.Text = m_currency_exchange.Description;
                // LTC 05-AGO-2016
                if ((m_voucher_type == VoucherTypes.FillOut &&
                   Misc.IsBankCardWithdrawalEnabled()) || (m_voucher_type == VoucherTypes.CloseCash && Misc.IsBankCardCloseCashEnabled()))
                {
                  lbl_currency_iso_code.Text = Resource.String("STR_CASHIER_WITHDRAWAL_VOUCHERS_TITLE");
                }
                else
                {
                  lbl_currency_iso_code.Text = "";
                }

                break;

              case CurrencyExchangeSubType.CREDIT_CARD:
                m_selected_bank_card_type = CurrencyExchangeSubType.CREDIT_CARD;
                lbl_paymet_method.Text = m_currency_exchange.Description;
                lbl_currency_iso_code.Text = "";
                break;

              case CurrencyExchangeSubType.DEBIT_CARD:
                m_selected_bank_card_type = CurrencyExchangeSubType.DEBIT_CARD;
                lbl_paymet_method.Text = m_currency_exchange.Description;
                lbl_currency_iso_code.Text = "";
                break;

              default:
                lbl_paymet_method.Text = m_currency_exchange.Description;
                lbl_currency_iso_code.Text = "";
                break;
            }
            break;

          case CurrencyExchangeType.CHECK:
            lbl_paymet_method.Text = m_currency_exchange.Description;
            m_selected_bank_card_type = CurrencyExchangeSubType.CHECK;
            lbl_currency_iso_code.Text = "";
            break;

          default:
            lbl_paymet_method.Text = m_currency_exchange.Description;
            lbl_currency_iso_code.Text = "";
            break;

        }

        SetCurrencyImage();
      }
      else if (CurrenciesList.Count > 0)
      {
        SelectNextCurrencyAllowed();
      }
      else
      {
        m_currency_exchange = null;
      }

    } // SetCurrencyExchange

    //------------------------------------------------------------------------------
    // PURPOSE : True if the currency is allowed.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boobean: True when currency is allowed.
    //
    //   NOTES :
    //
    private bool CurrencyAllowed(CurrencyExchange Currency)
    {
      Boolean _return;

      _return = true;

      if (m_locked_iso_code != null && Currency.CurrencyCode != m_locked_iso_code)
      {
        _return = false;
      }
      else if (FeatureChips.IsFeatureChipsEnabled && FeatureChips.IsChipsType(Currency.Type))
      {
        _return = FeatureChips.AllowedChipOperation(ChipOperation, Currency.Type, Currency.CurrencyCode, GamingTableId);
      }
      else if (FeatureChips.IsChipsType(Currency.Type))
      {
        _return = false;
      }

      return _return;
    } // CurrencyAllowed

    /// <summary>
    /// Remove currencies are type credit line
    /// </summary>
    /// <param name="Currencies"></param>
    private void RemoveCurrenciesCreditLine(List<CurrencyExchange> Currencies)
    {
      List<CurrencyExchange> _currencies_credit_line;
      _currencies_credit_line = new List<CurrencyExchange>();

      if (Currencies != null && Currencies.Count > 0)
      {
        if (!GeneralParam.GetBoolean("CreditLine", "Enabled", false) || !this.showPaymentCreditLine)
        {
          foreach (CurrencyExchange _currency in Currencies)
          {
            if (_currency.Type == CurrencyExchangeType.CREDITLINE)
            {
              _currencies_credit_line.Add(_currency);
            }
          }

          if (_currencies_credit_line.Count > 0)
          {
            foreach (CurrencyExchange _currency in _currencies_credit_line)
            {
              Currencies.Remove(_currency);
            }
          }
        }
      }
    }

    #endregion Private Methods

  }
}
