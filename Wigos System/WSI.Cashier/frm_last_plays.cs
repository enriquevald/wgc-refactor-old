//------------------------------------------------------------------------------
// Copyright � 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_last_plays.cs
// 
//   DESCRIPTION: Implements the following classes:
//                1. frm_last_plays
//
//        AUTHOR: ACC
// 
// CREATION DATE: 07-AUG-2007
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 07-AUG-2007 ACC     First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using WSI.Common;

namespace WSI.Cashier
{
  public partial class frm_last_plays : Form
  {
    private const Int32 GRIG_MAX_COLUMNS = 10;

    private const Int32 GRIG_COLUMN_TYPE_ID = 0;
    private const Int32 GRIG_COLUMN_TYPE_NAME = 1;
    private const Int32 GRIG_COLUMN_DATE = 2;
    private const Int32 GRIG_COLUMN_USER_NAME = 3;
    private const Int32 GRIG_COLUMN_TERMINAL = 4;
    private const Int32 GRIG_COLUMN_TERM_ID = 5;
    private const Int32 GRIG_COLUMN_INITIAL_AMOUNT = 6;
    private const Int32 GRIG_COLUMN_SUB_AMOUNT = 7;
    private const Int32 GRIG_COLUMN_ADD_AMOUNT = 8;
    private const Int32 GRIG_COLUMN_FINAL_AMOUNT = 9;

    public enum MovementShow
    {
      Card = 1,
      Cashier = 2
    }

    #region Attributes

    private frm_yesno form_yes_no;
    private MovementShow m_mov_show;

    #endregion

    #region Constructor

    /// <summary>
    /// Constructor method.
    /// 1. Initialize controls.
    /// 2. Initialize control�s resources. (NLS, Images, etc)
    /// </summary>
    public frm_last_plays()
    {
      InitializeComponent();

      InitializeControlResources();

      form_yes_no = new frm_yesno();
      form_yes_no.Opacity = 0.6;
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Initialize control resources (NLS strings, images, etc)
    /// </summary>
    private void InitializeControlResources()
    {
      // - NLS Strings:
      //   - Title:
      lbl_last_movements_title.Text = Resource.String("STR_FRM_LAST_MOVEMENTS_TITLE");

      //   - Labels

      //   - Buttons
      btn_close.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CLOSE");

      // - Images:

    } // InitializeControlResources

    #endregion

    #region Public Methods

    /// <summary>
    /// Init and Show the form
    /// </summary>
    /// <param name="SqlQuery"></param>
    public void Show(String SqlQuery, MovementShow  MovShow, String Name)
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_last_plays", Log.Type.Message);

      SqlConnection conn;
      DataSet ds;
      SqlDataAdapter da;
      Int32 idx_row;
      Int32 movement_type;
      String date_format;
      String time_separator;

      m_mov_show = MovShow;

      if (m_mov_show == MovementShow.Card)
      {
        lbl_last_movements_title.Text = Resource.String("STR_FRM_LAST_MOVEMENTS_CARD") + " " + Name;
      }
      else if (m_mov_show == MovementShow.Cashier)
      {
        lbl_last_movements_title.Text = Resource.String("STR_FRM_LAST_MOVEMENTS_CASH_DESK") + " " + Name;
      }

      conn = WGDB.Connection();
      ds = new DataSet();

      try
      {
        da = new SqlDataAdapter(SqlQuery, conn);
        da.Fill(ds);
        dataGridView1.DataSource = ds.Tables[0];        
      }
      catch
      {
        return;
      }
      // Window Size

      if (m_mov_show == MovementShow.Cashier)
      {
        this.Width += 145;  // 130 (USER_NAME) + 45 (TYPE_NAME) - 30 ()
        dataGridView1.Width += 145;
      }

      this.Width += System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
      dataGridView1.Width += System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;


      // center button position
      this.btn_close.Location = new Point(this.Width / 2 - (btn_close.Width / 2), this.btn_close.Location.Y);

      date_format = System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern;
      time_separator = System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.TimeSeparator;

      // Columns
      dataGridView1.Columns[GRIG_COLUMN_TYPE_ID].Width = 0;
      dataGridView1.Columns[GRIG_COLUMN_TYPE_ID].Visible = false;

      dataGridView1.Columns[GRIG_COLUMN_TYPE_NAME].Width = 115;
      dataGridView1.Columns[GRIG_COLUMN_TYPE_NAME].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
      dataGridView1.Columns[GRIG_COLUMN_DATE].Width = 120;
      dataGridView1.Columns[GRIG_COLUMN_DATE].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
      dataGridView1.Columns[GRIG_COLUMN_DATE].DefaultCellStyle.Format = date_format + " HH" + time_separator + "mm" + time_separator + "ss";

      if (m_mov_show == MovementShow.Card)
      {
        dataGridView1.Columns[GRIG_COLUMN_USER_NAME].Width = 0;
        dataGridView1.Columns[GRIG_COLUMN_USER_NAME].Visible = false;
        dataGridView1.Columns[GRIG_COLUMN_TERMINAL].Width = 150;
        dataGridView1.Columns[GRIG_COLUMN_TERMINAL].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
      }
      else if (m_mov_show == MovementShow.Cashier)
      {
        dataGridView1.Columns[GRIG_COLUMN_USER_NAME].Width = 120;
        dataGridView1.Columns[GRIG_COLUMN_USER_NAME].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
        dataGridView1.Columns[GRIG_COLUMN_TERMINAL].Width = 120;
        dataGridView1.Columns[GRIG_COLUMN_TERMINAL].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

        dataGridView1.Columns[GRIG_COLUMN_TYPE_NAME].Width = 170;
      }

      dataGridView1.Columns[GRIG_COLUMN_TERM_ID].Width = 0;
      dataGridView1.Columns[GRIG_COLUMN_TERM_ID].Visible = false;
      dataGridView1.Columns[GRIG_COLUMN_INITIAL_AMOUNT].Width = 100;
      dataGridView1.Columns[GRIG_COLUMN_INITIAL_AMOUNT].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
      dataGridView1.Columns[GRIG_COLUMN_INITIAL_AMOUNT].DefaultCellStyle.Format = "#,##0.00";
      dataGridView1.Columns[GRIG_COLUMN_SUB_AMOUNT].Width = 90;
      dataGridView1.Columns[GRIG_COLUMN_SUB_AMOUNT].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
      dataGridView1.Columns[GRIG_COLUMN_SUB_AMOUNT].DefaultCellStyle.Format = "#,##0.00";
      dataGridView1.Columns[GRIG_COLUMN_ADD_AMOUNT].Width = 90;
      dataGridView1.Columns[GRIG_COLUMN_ADD_AMOUNT].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
      dataGridView1.Columns[GRIG_COLUMN_ADD_AMOUNT].DefaultCellStyle.Format = "#,##0.00";
      dataGridView1.Columns[GRIG_COLUMN_FINAL_AMOUNT].Width = 125;
      dataGridView1.Columns[GRIG_COLUMN_FINAL_AMOUNT].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
      dataGridView1.Columns[GRIG_COLUMN_FINAL_AMOUNT].DefaultCellStyle.Format = "#,##0.00";

      // Datagrid Headers
      if (m_mov_show == MovementShow.Card)
      {
        dataGridView1.Columns[GRIG_COLUMN_TYPE_ID].HeaderCell.Value = "TypeId"; // Not Visible
        dataGridView1.Columns[GRIG_COLUMN_TYPE_NAME].HeaderCell.Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DATAGRID_CARD_TYPE");
        dataGridView1.Columns[GRIG_COLUMN_DATE].HeaderCell.Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DATAGRID_CARD_DATE");
        dataGridView1.Columns[GRIG_COLUMN_USER_NAME].HeaderCell.Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DATAGRID_CARD_USER");
        dataGridView1.Columns[GRIG_COLUMN_TERMINAL].HeaderCell.Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DATAGRID_CARD_TERMINAL");
        dataGridView1.Columns[GRIG_COLUMN_TERM_ID].HeaderCell.Value = "TermId"; // Not Visible
        dataGridView1.Columns[GRIG_COLUMN_INITIAL_AMOUNT].HeaderCell.Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DATAGRID_CARD_BAL_INITIAL");
        dataGridView1.Columns[GRIG_COLUMN_SUB_AMOUNT].HeaderCell.Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DATAGRID_CARD_BAL_SUBS");
        dataGridView1.Columns[GRIG_COLUMN_ADD_AMOUNT].HeaderCell.Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DATAGRID_CARD_BAL_ADDED");
        dataGridView1.Columns[GRIG_COLUMN_FINAL_AMOUNT].HeaderCell.Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DATAGRID_CARD_BAL_FINAL");
      }
      else if (m_mov_show == MovementShow.Cashier)
      {
        dataGridView1.Columns[GRIG_COLUMN_TYPE_ID].HeaderCell.Value = "TypeId"; // Not Visible
        dataGridView1.Columns[GRIG_COLUMN_TYPE_NAME].HeaderCell.Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DATAGRID_CASH_DESK_TYPE");
        dataGridView1.Columns[GRIG_COLUMN_DATE].HeaderCell.Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DATAGRID_CASH_DESK_DATE");
        dataGridView1.Columns[GRIG_COLUMN_USER_NAME].HeaderCell.Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DATAGRID_CASH_DESK_USER");
        dataGridView1.Columns[GRIG_COLUMN_TERMINAL].HeaderCell.Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DATAGRID_CASH_DESK_CASHIER");
        dataGridView1.Columns[GRIG_COLUMN_TERM_ID].HeaderCell.Value = "TermId"; // Not Visible
        dataGridView1.Columns[GRIG_COLUMN_INITIAL_AMOUNT].HeaderCell.Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DATAGRID_CASH_DESK_BAL_INITIAL");
        dataGridView1.Columns[GRIG_COLUMN_SUB_AMOUNT].HeaderCell.Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DATAGRID_CASH_DESK_BAL_SUBS");
        dataGridView1.Columns[GRIG_COLUMN_ADD_AMOUNT].HeaderCell.Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DATAGRID_CASH_DESK_BAL_ADDED");
        dataGridView1.Columns[GRIG_COLUMN_FINAL_AMOUNT].HeaderCell.Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DATAGRID_CASH_DESK_BAL_FINAL");
      }

      for (idx_row = 0; idx_row < dataGridView1.Rows.Count; idx_row++)
      {
        if ((Decimal)dataGridView1.Rows[idx_row].Cells[GRIG_COLUMN_SUB_AMOUNT].Value== 0)
        {
          dataGridView1.Rows[idx_row].Cells[GRIG_COLUMN_SUB_AMOUNT].Value = DBNull.Value;
        }

        if ( ( (Decimal) dataGridView1.Rows[idx_row].Cells[GRIG_COLUMN_ADD_AMOUNT].Value ) == 0 )
        {
          dataGridView1.Rows[idx_row].Cells[GRIG_COLUMN_ADD_AMOUNT].Value = DBNull.Value;
        }

        movement_type = (Int32)dataGridView1.Rows[idx_row].Cells[GRIG_COLUMN_TYPE_ID].Value;
        if (m_mov_show == MovementShow.Card)
        {
          switch ((MovementType)movement_type)
          {
            case MovementType.Play:
              dataGridView1.Rows[idx_row].Cells[GRIG_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DATAGRID_COLUMN_PLAY");
            break;

            case MovementType.CashIn:
              dataGridView1.Rows[idx_row].Cells[GRIG_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DATAGRID_COLUMN_ADD");
            break;

            case MovementType.CashOut:
              dataGridView1.Rows[idx_row].Cells[GRIG_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DATAGRID_COLUMN_SUBS");
            break;

            case MovementType.Devolution:
              dataGridView1.Rows[idx_row].Cells[GRIG_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DATAGRID_COLUMN_DEV");
            break;
 
            case MovementType.Tax:
              dataGridView1.Rows[idx_row].Cells[GRIG_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DATAGRID_COLUMN_TAXES");
            break;

            case MovementType.StartCardSession:
              dataGridView1.Rows[idx_row].Cells[GRIG_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DATAGRID_COLUMN_SESSION_INIT");
            break;

            case MovementType.EndCardSession:
              dataGridView1.Rows[idx_row].Cells[GRIG_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DATAGRID_COLUMN_SESSION_FINAL");
            break;

            default:
            break;
          }
        }
        else if (m_mov_show == MovementShow.Cashier)
        {
          switch ((CASHIER_MOVEMENT)movement_type)
          {
            case CASHIER_MOVEMENT.OPEN_SESSION:
              dataGridView1.Rows[idx_row].Cells[GRIG_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DATAGRID_COLUMN_CASH_OPEN");
            break;

            case CASHIER_MOVEMENT.CLOSE_SESSION:
              dataGridView1.Rows[idx_row].Cells[GRIG_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DATAGRID_COLUMN_CASH_CLOSE");
            break;

            case CASHIER_MOVEMENT.CASH_IN:
              dataGridView1.Rows[idx_row].Cells[GRIG_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DATAGRID_COLUMN_ADD");
            break;

            case CASHIER_MOVEMENT.CASH_OUT:
              dataGridView1.Rows[idx_row].Cells[GRIG_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DATAGRID_COLUMN_SUBS");
            break;

            case CASHIER_MOVEMENT.TAX:
              dataGridView1.Rows[idx_row].Cells[GRIG_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DATAGRID_COLUMN_TAXES");
            break;

            case CASHIER_MOVEMENT.FILLER_IN:
              dataGridView1.Rows[idx_row].Cells[GRIG_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DATAGRID_COLUMN_DEPOSIT");
            break;

            case CASHIER_MOVEMENT.FILLER_OUT:
              dataGridView1.Rows[idx_row].Cells[GRIG_COLUMN_TYPE_NAME].Value = Resource.String("STR_FRM_LAST_MOVEMENTS_DATAGRID_COLUMN_WITHDRAW");
            break;

            default:
            break;
          }
        }
      }

      form_yes_no.Show();
      this.ShowDialog();
      form_yes_no.Hide();
    }

    #endregion

    #region DataGridEvents

    private void dataGridView1_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
    {
      if (m_mov_show == MovementShow.Card)
      {
        switch ((MovementType)dataGridView1.Rows[e.RowIndex].Cells[GRIG_COLUMN_TYPE_ID].Value)
        {
          case MovementType.Play:
            // None
          break;

          case MovementType.CashIn:
            dataGridView1.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LightSteelBlue;
          break;

          case MovementType.CashOut:
            dataGridView1.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LightSteelBlue;
          break;

          case MovementType.Devolution:
            dataGridView1.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LightSteelBlue;
          break;

          case MovementType.Tax:
            dataGridView1.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LightSteelBlue;
          break;

          case MovementType.StartCardSession:
            dataGridView1.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.NavajoWhite;
          break;

          case MovementType.EndCardSession:
            dataGridView1.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.NavajoWhite;
          break;

          default:
          break;
        }

        dataGridView1.Columns[GRIG_COLUMN_TYPE_ID].Width      = 0;
        dataGridView1.Columns[GRIG_COLUMN_TYPE_ID].Visible    = false;
        dataGridView1.Columns[GRIG_COLUMN_USER_NAME].Width    = 0;
        dataGridView1.Columns[GRIG_COLUMN_USER_NAME].Visible  = false;

      }
      else if (m_mov_show == MovementShow.Cashier)
      {
        switch ((CASHIER_MOVEMENT)dataGridView1.Rows[e.RowIndex].Cells[GRIG_COLUMN_TYPE_ID].Value)
        {
          case CASHIER_MOVEMENT.OPEN_SESSION:
            dataGridView1.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.NavajoWhite;
          break;

          case CASHIER_MOVEMENT.CLOSE_SESSION:
            dataGridView1.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.NavajoWhite;
          break;

          case CASHIER_MOVEMENT.CASH_IN:
            // None
          break;

          case CASHIER_MOVEMENT.CASH_OUT:
            dataGridView1.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.Coral;
          break;

          case CASHIER_MOVEMENT.TAX:
            //dataGridView1.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LightSteelBlue;
          break;

          case CASHIER_MOVEMENT.FILLER_IN:
            //dataGridView1.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LightSteelBlue;
          break;

          case CASHIER_MOVEMENT.FILLER_OUT:
            dataGridView1.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.Coral;
          break;

          default:
          break;
        }

        dataGridView1.Columns[GRIG_COLUMN_TYPE_ID].Width = 0;
        dataGridView1.Columns[GRIG_COLUMN_TYPE_ID].Visible = false;
      }
    }

    #endregion DataGridEvents

    #region Buttons

    private void btn_exit_Click(object sender, EventArgs e)
    {
      // ORIGINAL FORM WIDTH VALUES
      this.Width = 821;
      this.dataGridView1.Width = 797;

      this.Close();
    }

    #endregion Buttons

  }
}