//------------------------------------------------------------------------------
// Copyright � 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_chips.cs
// 
//   DESCRIPTION: Implements the following classes:
//
//        AUTHOR: Sergi martinez
// 
// CREATION DATE: 06-NOV-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 03-DIC-2013 SMN    First release.
// 27-JAN-2013 DLL    Fixed Bug WIG-550.
// 04-FEB-2014 DLL    Fixed Bug WIG-569
// 14-FEB-2014 DLL    Check if casino chip balance is less than amount. Only when is cashier, not gaming table
// 03-MAR-2014 RRR    Force selection visibility in ammounts grid
// 05-MAR-2014 DLL    Fixed Bug WIG-695: payment order is enabled when total amount is 0
// 17-MAR-2014 JRM    Added new functionality. Description contained in RequestPinCodeDUT.docx DUT
// 22-APR-2014 DLL    Added new functionality for print specific voucher for the Dealer.
// 06-JUN-2014 JAB    Added two decimal characters in denominations fields.
// 20-JUN-2014 DLL    Fixed Bug WIG-1041: Unhandled exception when no denomination is selected.
// 25-JUL-2014 DLL    Chips are moved to a separate table
// 20-AUG-2014 DRV & RCI    Fixed Bug WIG-1187: Request PIN if enabled.
// 27-AUG-2014 DRV    Fixed Bug WIG-1211: Chips Sale/Purchase doesn't work with decimals
// 21-OCT-2014 SGB    Fixed Bug WIG-1476: Change style form when buy or sale chips
// 26-MAR-2015 YNM    Fixed Bug WIG-2160: TITO - Chips Purchase - Error calculating retentions.
// 26-MAR-2015 YNM    Fixed Bug WIG-2161: TITO - Chips Purchase - Incorrect Amount generating Payment Order.
// 03-NOV-2015 ECP    Backlog Item 5638 - Task 5724 Modify parameter general: Autodistribuite Chips Denomination, for the moment does not apply
// 11-NOV-2015 FOS    Product Backlog Item: 3709 Payment tickets & handpays
// 23-NOV-2015 FOS    Bug 6771:Payment tickets & handpays
// 05-NOV-2015 SGB    Backlog Item WIG-5856: Change designer
// 25-NOV-2015 FOS    Product Backlog Item: 3709 Payment tickets & handpays, error when open stock control form
// 01-DEC-2015 FOS    Fixed Bug 7205: Protected taxWaiverMode and renamed function ApplyTaxCollection to EnableTitoTaxWaiver
// 02-DEC-2015 FOS    Fixed Bug 7208: Incorrect Tax Value on Handpays
// 07-MAR-2016 LTC    Product Backlog Item 10227:TITA: TITO ticket exchange for gambling chips
// 13-APR-2016 DHA    Product Backlog Item 9950: added chips operation (sales and purchases)
// 18-APR-2016 RAB    Product Backlog Item 10855: Tables (Phase 1): Currency/chip Selector.
// 26-MAY-2016 RAB    PBI 13462: Review Sprint 24 Winions (chips - modifications cashier).
// 09-JUN-2016 ESE    Bug 14355:Cashier: Not show label 'TOTAL'
// 21-JUN-2016 RAB    PBI 14486: GamingTables (Phase 1): Review compatibility (TITO - Cashier)
// 28-JUN-2016 RAB    PBI 14524: GamingTables (Phase 4): Management from deposit and withdrawals - Multicurrency in chips.
// 06-JUL-2016 DHA    Product Backlog Item 15064: multicurrency chips sale/purchase
// 17-AUG-2016 RAB    Bug 16794: Cashier - Unhandled exception when trying to buy chips
// 07-SEP-2016 DHA    Fixed Bug 17367: error on chips buy selecting differents iso code chips
// 12-SEP-2016 JML    Fixed Bug 17571 - Gaming tables: Wrong message "The Cash Desk does not have enough stock."
// 14-OCT-2016 ETP    Fixed Bug 18812: CASHIER: El control de stock en caja no muestra los totales
// 23-MAY-2017 DHA    PBI 27487: WIGOS-83 MES21 - Sales selection
// 06-JUL-2017 JML    PBI 28422: WIGOS-2702 Rounding - Consume the remaining amount
// 05-OCT-2017 RAB    PBI 30018:WIGOS-4036 Payment threshold authorization - Payment authorisation
// 09-OCT-2017 JML    PBI 30022:WIGOS-4052 Payment threshold registration - Recharge with Check payment
// 10-OCT-2017 JML    PBI 30023:WIGOS-4068 Payment threshold registration - Transaction information
// 30-OCT-2017 RAB    Bug 30446:WIGOS-6052 Threshold: Screen order is incorrect in HP and dispute (paying with PO) exceeding threshold and without permissions
// 12-APR-2018 JGC    Bug 32311:[WIGOS-9805]: [Ticket #13564] Reporte de "Resumen de caja"
//------------------------------------------------------------------------------
//

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WSI.Common;
using System.Data.SqlClient;
using System.Globalization;
using System.Collections;
using System.Reflection;

namespace WSI.Cashier
{

  public enum FormType
  {
    Chips = 0,
    Stock = 1
  };

  public partial class frm_chips : WSI.Cashier.Controls.frm_base
  {
    private const Int32 GRID_COLUMN_CHIPS_ID = 0;
    private const Int32 GRID_COLUMN_CHIPS_COLOR = 1;
    private const Int32 GRID_COLUMN_CHIPS_DENOMINATION = 2;
    private const Int32 GRID_COLUMN_CHIPS_NAME = 3;
    private const Int32 GRID_COLUMN_CHIPS_DRAW = 4;
    private const Int32 GRID_COLUMN_CHIPS_UN = 5;
    private const Int32 GRID_COLUMN_CHIPS_TOTAL = 6;
    private const Int32 GRID_COLUMN_CHIPS_STOCK = 7;

    private const Int32 WIDTH_COLUMN_CHIPS_ID = 0;
    private const Int32 WIDTH_COLUMN_CHIPS_COLOR = 35;
    private const Int32 WIDTH_COLUMN_CHIPS_DENOMINATION = 130;
    private const Int32 WIDTH_COLUMN_CHIPS_NAME = 105;
    private const Int32 WIDTH_COLUMN_CHIPS_DRAW = 105;
    private const Int32 WIDTH_COLUMN_CHIPS_UN = 80;
    private const Int32 WIDTH_COLUMN_CHIPS_TOTAL = 150;
    private const Int32 WIDTH_COLUMN_CHIPS_STOCK = 0;

    private frm_catalog m_form_catalog;
    private TYPE_CASH_REDEEM m_cash_redeem;
    private Boolean m_is_apply;
    private Boolean m_is_apply_result;
    private Boolean m_is_apply_initial;
    private Int64 m_account_operation_reason_id;
    private String m_account_operation_comment;
    private PaymentThresholdAuthorization m_payment_threshold_authorization;

    #region Attributes

    private static CardData m_card_data;
    private Boolean m_is_tito_mode;
    private FormType m_form_type;
    private Boolean m_is_chip_purchase_with_cash_out;

    private uc_card m_uc_card;

    private FeatureChips.ChipsOperation m_chips_operation;

    private static Boolean m_out_check_redeem = false;        // For the output parameter in the Show() function
    private static Boolean m_payment_order_enabled = false;   // To read GP PaymentOrder.Enabled
    private static Currency m_payment_order_min_amount = 0;   // To read GP PaymentOrder.MinAmount

    private Currency m_max_allowed_operation;
    private Boolean m_autodistribute_enabled = false;

    private Boolean m_calculate_cash_amount_ok;
    private String m_calculate_cash_amount_error_message;
    private Currency m_calculate_cash_amount_total_paid;

    private Boolean m_integrated_chip_operation = false;
    private Cage.ShowDenominationsMode m_show_denominations;
    private Boolean m_is_param_buy_chips_enabled;

    private List<CurrencyExchange> m_chips_allowed = null;
    private CurrencyExchange m_national_currency = null;
    private Boolean m_row_chips_visible_according_stock;

    #endregion

    #region Constructors

    //------------------------------------------------------------------------------
    // PURPOSE : Constructor method.
    //            1. Initialize controls.
    //            2. Initialize control�s resources. (NLS, Images, etc)
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public frm_chips()
      : this(null)
    {
    } // frm_chips

    //------------------------------------------------------------------------------
    // PURPOSE : Constructor method.
    //            1. Initialize controls.
    //            2. Initialize control�s resources. (NLS, Images, etc)
    //
    //  PARAMS :
    //      - INPUT :
    //          - uc_card: UcCard
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public frm_chips(uc_card UcCard)
    {
      m_uc_card = UcCard;

      InitializeComponent();

      EventLastAction.AddEventLastAction(this.Controls);

      InitializeControlResources();
    } // frm_chips

    #endregion

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Init and Show the form.
    //
    //  PARAMS :
    //      - INPUT :
    //          - CardData:     Card
    //          - Currency:     AuxAmount
    //          - CASH_MODE:    SubstractType
    //          - Form:         Parent
    //
    //      - OUTPUT :
    //          - out SortedDictionary<CurrencyIsoType, Decimal>:     Amount
    //          - out Boolean:                                        CheckRedeem
    //          - out WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS:  SessionStats
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public Boolean Show(CardData Card,
                        Boolean IsTitoMode,
                        Form Parent,
                        ref FeatureChips.ChipsOperation ChipsAmount,
                        Boolean IsChipsSaleWithRecharge)
    {
      Boolean _check_redeem;

      m_integrated_chip_operation = IsChipsSaleWithRecharge;

      return Show(Card, IsTitoMode, Parent, ref ChipsAmount, out _check_redeem);
    }

    public Boolean Show(ref FeatureChips.ChipsOperation ChipsAmountIn,
                        ref FeatureChips.ChipsOperation ChipsAmountOut)
    {
      Boolean _check_redeem;

      if (!Show(null, false, null, ref ChipsAmountIn, out _check_redeem))
      {
        return false;
      }

      // asign delivered to initial.
      ChipsAmountIn.FillChipsAmount = m_chips_operation.ChipsAmount;
      ChipsAmountIn.IsoCode = m_chips_operation.IsoCode;
      ChipsAmountIn.CurrencyExchangeType = m_chips_operation.CurrencyExchangeType;

      ChipsAmountOut.FillChipsAmount = ChipsAmountIn.FillChipsAmount;
      ChipsAmountOut.IsoCode = ChipsAmountIn.IsoCode;
      ChipsAmountOut.CurrencyExchangeType = ChipsAmountIn.CurrencyExchangeType;

      return Show(null, false, null, ref ChipsAmountOut, out _check_redeem);
    }
    public Boolean Show(CardData Card,
                        Boolean IsTitoMode,
                        Boolean IsChipsPurchaseWithCashOut,
                        Form Parent,
                        ref FeatureChips.ChipsOperation ChipsAmount,
                        out Boolean CheckRedeem)
    {
      Boolean _return;
      m_is_chip_purchase_with_cash_out = IsChipsPurchaseWithCashOut;

      _return = Show(Card, IsTitoMode, Parent, FormType.Chips, ref ChipsAmount, out CheckRedeem);

      return _return;
    }


    public Boolean Show(CardData Card,
                        Boolean IsTitoMode,
                        Form Parent,
                        ref FeatureChips.ChipsOperation ChipsAmount,
                        out Boolean CheckRedeem)
    {
      return Show(Card, IsTitoMode, Parent, FormType.Chips, ref ChipsAmount, out CheckRedeem);
    }

    public Boolean Show(CardData Card,
                        Boolean IsTitoMode,
                        Form Parent,
                        FormType Type,
                        ref FeatureChips.ChipsOperation ChipsAmount,
                        out Boolean CheckRedeem)
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_chips", Log.Type.Message);
      Int64? GamingTableId;
      String _iso_code;
      PaymentAndRechargeThresholdData _payment_and_recharge_threshold_data;

      _iso_code = null;
      CheckRedeem = false;
      m_chips_operation = ChipsAmount;
      m_card_data = Card;
      m_is_tito_mode = IsTitoMode;
      m_form_type = Type;
      m_row_chips_visible_according_stock = false;

      GamingTableId = null;
      if (m_chips_operation.SelectedGamingTable != null)
      {
        GamingTableId = m_chips_operation.SelectedGamingTable.TableId;
      }

      if (m_card_data == null)
      {
        m_integrated_chip_operation = false;
      }

      m_show_denominations = (Cage.ShowDenominationsMode)GeneralParam.GetInt32("Cage", "ShowDenominations");

      m_out_check_redeem = false;
      m_payment_order_enabled = PaymentOrder.IsEnabled();
      m_payment_order_min_amount = PaymentOrder.MinAmount();
      btn_check_redeem.Visible = false;
      btn_check_redeem.Enabled = false;
      m_max_allowed_operation = 0;

      GuiStyleSheet();

      uc_payment_method1.HeaderText = Resource.String("STR_FRM_AMOUNT_CURRENCY_GROUP_BOX");

      CurrencyExchange.GetAllowedCurrencies(true, null, false, false, out m_national_currency, out m_chips_allowed);

      switch (m_chips_operation.OperationType)
      {
        case FeatureChips.ChipsOperation.Operation.SALE:
          uc_payment_method1.VoucherType = VoucherTypes.ChipsSaleAmountInput;
          _iso_code = m_chips_operation.IsoCode;
          break;
        case FeatureChips.ChipsOperation.Operation.PURCHASE:
          uc_payment_method1.VoucherType = VoucherTypes.ChipsPurchaseAmountInput;
          break;
        case FeatureChips.ChipsOperation.Operation.CHANGE_IN:
          uc_payment_method1.VoucherType = VoucherTypes.ChangeChipsIn;
          break;
        case FeatureChips.ChipsOperation.Operation.CHANGE_OUT:
          uc_payment_method1.VoucherType = VoucherTypes.ChangeChipsOut;
          break;
        case FeatureChips.ChipsOperation.Operation.SHOW_STOCK:
          uc_payment_method1.VoucherType = VoucherTypes.ChipsStock;
          break;
        case FeatureChips.ChipsOperation.Operation.SWAP:
          uc_payment_method1.VoucherType = VoucherTypes.ChipsSwap;
          break;
      }

      uc_payment_method1.InitControl(m_chips_allowed, GamingTableId, m_chips_operation.OperationType, _iso_code);

      if (m_chips_operation.OperationType == FeatureChips.ChipsOperation.Operation.CHANGE_OUT || m_chips_operation.OperationType == FeatureChips.ChipsOperation.Operation.SWAP)
      {
        uc_payment_method1.CurrencyExchange = m_chips_allowed.FindLast(
          delegate(CurrencyExchange _currency_exchange_type)
          {
            return _currency_exchange_type.CurrencyCode == m_chips_operation.IsoCode &&
                    _currency_exchange_type.Type == m_chips_operation.CurrencyExchangeType;
          }
            );
        uc_payment_method1.Enabled = false;
      }
      else
      {
        uc_payment_method1.CurrencyExchange = m_national_currency;
      }

      if (uc_payment_method1.CurrenciesList.Count <= 1)
      {
        uc_payment_method1.Enabled = false;
      }

      if (uc_payment_method1.CurrencyExchange == null)
      {
        frm_message.Show(Resource.String("STR_MESSAGE_NO_CHIPS_SETS_ASSOCIATED"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this);
        return false;
      }

      btn_apply_tax.Visible = false;

      switch (m_form_type)
      {
        case FormType.Chips:
          if (!InitLabels())
          {

            return false;
          }

          btn_ok.Enabled = false;
          btn_ticket.Enabled = false;
          btn_check_redeem.Enabled = false;
          //m_autodistribute_enabled = Chips.IsAutoDistributeEnabled();
          m_autodistribute_enabled = false;

          if (m_is_tito_mode && m_chips_operation.OperationType == FeatureChips.ChipsOperation.Operation.PURCHASE)
          {
            btn_check_redeem.Left -= 120;
            btn_ticket.Visible = true;
            btn_ticket.Top = btn_check_redeem.Top;
            btn_ticket.Left = btn_check_redeem.Left - btn_check_redeem.Width - 10;
          }

          m_is_apply = true;

          if (Tax.EnableTitoTaxWaiver(true) && IsTitoMode)
          {
            btn_apply_tax.Visible = true;
            btn_ticket.Left = btn_apply_tax.Width + btn_check_redeem.Left - btn_check_redeem.Width - 20;
            btn_check_redeem.Left = btn_apply_tax.Width + btn_ticket.Left + 9;
            if (Tax.ApplyTaxChipsPurchase())
            {
              btn_apply_tax.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_APPLY_TAX");
              m_is_param_buy_chips_enabled = false;
              m_is_apply = false;
            }
            else
            {
              btn_apply_tax.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_NO_APPLY_TAX");
              m_is_param_buy_chips_enabled = true;
              m_is_apply = true;
            }
            m_is_apply_initial = m_is_apply;
          }
          else
          {
            btn_apply_tax.Visible = false;
          }

          break;

        case FormType.Stock:

          btn_clear.Visible = false;
          lbl_total_delivered_exchange.Visible = false;
          lbl_total_delivered.Visible = true;
          lbl_diference_amount.Visible = false;
          btn_check_redeem.Visible = false;

          gb_common.Size = new Size(gb_common.Size.Width, 520);

          gb_common.Controls.Add(btn_cancel);
          btn_cancel.Location = new Point(235, 450);

          this.FormTitle = Resource.String("STR_CHIPS_STOCK_CONTROL");
          btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CLOSE");

          this.Size = new Size(gb_common.Size.Width + 14, 725); //new Size(445, 725);

          uc_payment_method1.Location = new Point(130, 6);
          gb_common.Location = new Point(8, uc_payment_method1.Height + 15);

          btn_show_chips.Visible = false;
          break;
      }

      InitChipsGrid();

      if (m_chips_operation.OperationType == FeatureChips.ChipsOperation.Operation.SALE || m_chips_operation.OperationType == FeatureChips.ChipsOperation.Operation.SWAP)
      {
        gb_distribution.Visible = m_autodistribute_enabled;
        if (dgv_common.Rows.Count > 0)
        {
          dgv_common.Rows[dgv_common.Rows.Count - 1].Selected = true;
          set_dgv_common_next_row();
        }
      }
      else
      {
        gb_distribution.Visible = false;

        if (m_is_tito_mode || m_is_chip_purchase_with_cash_out)
        {
          if (m_payment_order_enabled && m_form_type != FormType.Stock)
          {
            btn_check_redeem.Visible = true;
            btn_check_redeem.Enabled = false;
          }

          if (m_is_chip_purchase_with_cash_out)
          {
            using (DB_TRX _db_trx = new DB_TRX())
            {
              Accounts.GetMaxDevolution(m_card_data.AccountId, m_card_data.MaxDevolution, m_is_chip_purchase_with_cash_out, out m_card_data.MaxDevolutionForChips,
                                        _db_trx.SqlTransaction);
            }
          }
        }
      }

      //  - Money buttons 

      DialogResult _dlg_rc = this.ShowDialog(Parent);

      if (_dlg_rc != DialogResult.OK)
      {
        ChipsAmount = null;

        return false;
      }

      if (Parent != null)
      {
        Parent.Focus();
      }

      m_chips_operation.IsApplyTaxes = m_is_apply;
      ChipsAmount = m_chips_operation;
      CheckRedeem = m_out_check_redeem;

      Currency _converted_amount;

      using (DB_TRX _db_trx = new DB_TRX())
      {
        _converted_amount = CurrencyExchange.GetExchange(m_chips_operation.ChipsAmount, m_chips_operation.IsoCode, CurrencyExchange.GetNationalCurrency(), _db_trx.SqlTransaction);
      }

      String _message_error;

      if (m_chips_operation.OperationType != FeatureChips.ChipsOperation.Operation.SALE)
      {
        if (PaymentThresholdAuthorization.GetOutputThreshold() > 0 && PaymentThresholdAuthorization.GetOutputThreshold() <= _converted_amount)
        {
          if (!ProfilePermissions.CheckPermissionList(ProfilePermissions.CashierFormFuncionality.TITO_PaymentThreshold,
                                              ProfilePermissions.TypeOperation.RequestPasswd,
                                              this,
                                              0,
                                              out _message_error))
          {
            return false;
          }
        }
      }
      return true;
    }  // Show

    #endregion

    #region Private Methods

    private Boolean InitLabels()
    {
      String _selected_gaming_table;
      Currency _total_redeemable;

      _total_redeemable = 0;
      _selected_gaming_table = String.Empty;

      switch (m_chips_operation.OperationType)
      {
        case FeatureChips.ChipsOperation.Operation.PURCHASE:
        case FeatureChips.ChipsOperation.Operation.SALE:
        // LTC 07-MAR-2016
        case FeatureChips.ChipsOperation.Operation.SWAP:
          _total_redeemable = m_card_data.AccountBalance.TotalRedeemable;
          break;

        //case ChipsOperation.CHANGE_IN:
        //case ChipsOperation.CHANGE_OUT:
        //  break;
      }

      _selected_gaming_table = m_chips_operation.SelectedGamingTable != null ? m_chips_operation.SelectedGamingTable.TableName : "---";
      this.FormTitle = String.Empty;

      lbl_gaming_table.Text = "";
      if (_selected_gaming_table != null)
      {
        lbl_gaming_table.Text = Resource.String("STR_FRM_CHIPS_SELECTED_GAMING_TABLE", _selected_gaming_table);
      }
      else
      {
        gb_info.Visible = false;
      }
      btn_small_distribution.Text = Resource.String("STR_FRM_DISTRIBUTION_SMALL");
      btn_medium_distribution.Text = Resource.String("STR_FRM_DISTRIBUTION_MEDIUM");
      btn_high_distribution.Text = Resource.String("STR_FRM_DISTRIBUTION_HIGH");
      btn_clear.Text = Resource.String("STR_CAGE_BTN_CLEAR");
      btn_check_redeem.Text = Resource.String("STR_ACCOUNT_PAYMENT_ORDER_PAYMENT_ORDER_CREATION");
      btn_ticket.Text = Resource.String("STR_TITO_TICKET");

      ShowLabels(m_form_type == FormType.Stock);

      btn_show_chips.Visible = false;
      switch (m_chips_operation.OperationType)
      {
        case FeatureChips.ChipsOperation.Operation.SALE:
          this.FormTitle = Resource.String("STR_VOUCHER_CASH_CHIPS_SALE_TITLE");
          btn_show_chips.Visible = true;

          // Only for TITO Chips can be sale in other chips exchanges
          if (WSI.Common.TITO.Utils.IsTitoMode())
          {
            this.FormSubTitle = String.Format("({0})", m_chips_operation.IsoCode);

            m_max_allowed_operation = GamingTableBusinessLogic.GetGamingTablesMaxAllowedSales(m_chips_operation.IsoCode);
          }
          else
          {
            m_max_allowed_operation = GamingTableBusinessLogic.GetGamingTablesMaxAllowedSales(CurrencyExchange.GetNationalCurrency());
          }

          break;

        case FeatureChips.ChipsOperation.Operation.PURCHASE:
          // Only for TITO Chips can be purchase in other chips exchanges
          if (WSI.Common.TITO.Utils.IsTitoMode())
          {
            this.FormSubTitle = String.Format("({0})", m_chips_operation.IsoCode);

            m_max_allowed_operation = GamingTableBusinessLogic.GetGamingTablesMaxAllowedPurchase(m_chips_operation.IsoCode);
          }
          else
          {
            m_max_allowed_operation = GamingTableBusinessLogic.GetGamingTablesMaxAllowedPurchase(CurrencyExchange.GetNationalCurrency());
          }

          if (m_is_chip_purchase_with_cash_out)
          {
            this.FormTitle = Resource.String("STR_FRM_CHIPS_PURCHASE_WITH_CASH_OUT_TITLE");
          }
          else
          {
            this.FormTitle = Resource.String("STR_VOUCHER_CASH_CHIPS_PURCHASE_TITLE");
          }
          if (m_is_chip_purchase_with_cash_out)
          {
            ShowLabels(true);
          }

          break;

        case FeatureChips.ChipsOperation.Operation.CHANGE_IN:
          this.FormTitle = Resource.String("STR_FRM_CHIPS_TITLE_CHANGE_CHIPS_IN");
          lbl_gaming_table.Text = Resource.String("STR_FRM_CHIPS_INFO_CHANGE_CHIPS_IN");
          break;

        case FeatureChips.ChipsOperation.Operation.CHANGE_OUT:
          this.FormTitle = Resource.String("STR_FRM_CHIPS_TITLE_CHANGE_CHIPS_OUT");
          lbl_gaming_table.Text = Resource.String("STR_FRM_CHIPS_INFO_CHANGE_CHIPS_OUT");

          lbl_initial_amount.Text = Currency.Format(m_chips_operation.FillChipsAmount, m_chips_operation.IsoCode);

          this.FormSubTitle = String.Format("({0})", m_chips_operation.IsoCode);

          gb_total.HeaderText = Resource.String("STR_FRM_CHIPS_AMOUNT_TO_CHANGE");
          ShowLabels(true);
          btn_show_chips.Visible = true;
          break;

        // LTC 07-MAR-2016
        case FeatureChips.ChipsOperation.Operation.SWAP:
          this.FormTitle = Resource.String("STR_FRM_CHIPS_TITLE_CHIPS_SWAP");
          btn_show_chips.Visible = true;

          // Only for TITO Chips can be sale in other chips exchanges
          if (WSI.Common.TITO.Utils.IsTitoMode())
          {
            this.FormSubTitle = String.Format("({0})", m_chips_operation.IsoCode);
            m_max_allowed_operation = GamingTableBusinessLogic.GetGamingTablesMaxAllowedSales(m_chips_operation.IsoCode);
          }
          else
          {
            m_max_allowed_operation = GamingTableBusinessLogic.GetGamingTablesMaxAllowedSales(CurrencyExchange.GetNationalCurrency());
          }

          break;

        case FeatureChips.ChipsOperation.Operation.NONE:
        default:
          return false;
      }

      lbl_label_1.Text = "";
      lbl_label_2.Text = "";

      if (m_max_allowed_operation > 0)
      {
        if (WSI.Common.TITO.Utils.IsTitoMode())
        {
          lbl_label_1.Text = Resource.String("STR_FRM_CHIPS_MAX_AMOUNT_ALLOWED", Currency.Format(m_max_allowed_operation, m_chips_operation.IsoCode));
        }
        else
        {
          lbl_label_1.Text = Resource.String("STR_FRM_CHIPS_MAX_AMOUNT_ALLOWED", m_max_allowed_operation);
        }

      }

      if (!m_is_tito_mode &&
         (m_chips_operation.OperationType != FeatureChips.ChipsOperation.Operation.PURCHASE && m_chips_operation.OperationType != FeatureChips.ChipsOperation.Operation.CHANGE_IN &&
          m_chips_operation.OperationType != FeatureChips.ChipsOperation.Operation.CHANGE_OUT))
      {
        if (String.IsNullOrEmpty(lbl_label_1.Text) && _total_redeemable > 0)
        {
          lbl_label_1.Text = Resource.String("STR_FRM_CHIPS_AVAILABLE_CREDITS", _total_redeemable);
        }
        else
        {
          lbl_label_2.Text = Resource.String("STR_FRM_CHIPS_AVAILABLE_CREDITS", _total_redeemable);
        }
      }

      if (m_chips_operation.DevolutionRoundingSaleChips > 0 && GamingTableBusinessLogic.IsEnableRoundingSaleChips())
      {
        lbl_remaining.Text = Resource.String("STR_FRM_CHIPS_ACCUMULATED_ROUNDING_AMOUNT", (Currency)m_chips_operation.DevolutionRoundingSaleChips); 
      }
      else
      {
        lbl_remaining.Text = String.Empty;
      }

      btn_show_chips.Text = Resource.String("STR_SHOW_CHIPS_WITHOUT_STOCK");
      return true;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Initialize control resources (NLS strings, images, etc).
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public void InitializeControlResources()
    {
      // - NLS Strings:
      //   - Title: External set
      //   - Labels

      uc_input_amount.gp_digits_box.Text = Resource.String("STR_CAGE_CASINO_CHIPS");
      gb_distribution.HeaderText = Resource.String("STR_FRM_CHIPS_DISTRIBUTION");
      gb_total.HeaderText = Resource.String("STR_FRM_INPUT_TOTAL_TO_PAY");

      //   - Buttons
      btn_ok.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_OK");
      btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");

      uc_input_amount.OnAmountSelected += new uc_input_amount.AmountSelectedEventHandler(btn_intro);

    } // InitializeControlResources

    //------------------------------------------------------------------------------
    // PURPOSE : Format common data grid.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //    
    private void GuiStyleSheet()
    {
      dgv_common.Rows.Clear();
      dgv_common.Columns.Clear();

      dgv_common.Columns.Add("ChipId", "");
      dgv_common.Columns.Add(new DataGridViewImageColumn());
      dgv_common.Columns.Add("Denomination", "");
      dgv_common.Columns.Add("Name", "");
      dgv_common.Columns.Add("Draw", "");
      dgv_common.Columns.Add("Units", "");
      dgv_common.Columns.Add("Total", "");
      dgv_common.Columns.Add("Stock", "");

      dgv_common.Columns[GRID_COLUMN_CHIPS_ID].Width = WIDTH_COLUMN_CHIPS_ID;
      dgv_common.Columns[GRID_COLUMN_CHIPS_ID].Visible = false;
      dgv_common.Columns[GRID_COLUMN_CHIPS_ID].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
      dgv_common.Columns[GRID_COLUMN_CHIPS_ID].SortMode = DataGridViewColumnSortMode.NotSortable;

      dgv_common.Columns[GRID_COLUMN_CHIPS_COLOR].HeaderCell.Value = "";
      dgv_common.Columns[GRID_COLUMN_CHIPS_COLOR].Width = WIDTH_COLUMN_CHIPS_COLOR;
      dgv_common.Columns[GRID_COLUMN_CHIPS_COLOR].Visible = true;
      //dgv_common.Columns[GRID_COLUMN_CHIPS_COLOR].DefaultCellStyle.SelectionBackColor = Color.Transparent;

      dgv_common.Columns[GRID_COLUMN_CHIPS_DENOMINATION].HeaderCell.Value = Resource.String("STR_CAGE_TEXT_COLUMN_DENOMINATION");
      dgv_common.Columns[GRID_COLUMN_CHIPS_DENOMINATION].Width = WIDTH_COLUMN_CHIPS_DENOMINATION;
      dgv_common.Columns[GRID_COLUMN_CHIPS_DENOMINATION].Visible = true;
      dgv_common.Columns[GRID_COLUMN_CHIPS_DENOMINATION].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
      dgv_common.Columns[GRID_COLUMN_CHIPS_DENOMINATION].SortMode = DataGridViewColumnSortMode.NotSortable;

      dgv_common.Columns[GRID_COLUMN_CHIPS_NAME].HeaderCell.Value = Resource.String("STR_VOUCHER_CASH_CHIPS_NAME");
      dgv_common.Columns[GRID_COLUMN_CHIPS_NAME].Width = WIDTH_COLUMN_CHIPS_NAME;
      dgv_common.Columns[GRID_COLUMN_CHIPS_NAME].Visible = true;
      dgv_common.Columns[GRID_COLUMN_CHIPS_NAME].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
      dgv_common.Columns[GRID_COLUMN_CHIPS_NAME].SortMode = DataGridViewColumnSortMode.NotSortable;

      dgv_common.Columns[GRID_COLUMN_CHIPS_DRAW].HeaderCell.Value = Resource.String("STR_UC_CARD_BTN_PRIZE_DRAW");
      dgv_common.Columns[GRID_COLUMN_CHIPS_DRAW].Width = WIDTH_COLUMN_CHIPS_DRAW;
      dgv_common.Columns[GRID_COLUMN_CHIPS_DRAW].Visible = true;
      dgv_common.Columns[GRID_COLUMN_CHIPS_DRAW].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
      dgv_common.Columns[GRID_COLUMN_CHIPS_DRAW].SortMode = DataGridViewColumnSortMode.NotSortable;

      dgv_common.Columns[GRID_COLUMN_CHIPS_UN].HeaderCell.Value = Resource.String("STR_FRM_TITO_PROMO_005");
      dgv_common.Columns[GRID_COLUMN_CHIPS_UN].Width = WIDTH_COLUMN_CHIPS_UN;
      dgv_common.Columns[GRID_COLUMN_CHIPS_UN].Visible = true;
      dgv_common.Columns[GRID_COLUMN_CHIPS_UN].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
      dgv_common.Columns[GRID_COLUMN_CHIPS_UN].SortMode = DataGridViewColumnSortMode.NotSortable;

      dgv_common.Columns[GRID_COLUMN_CHIPS_TOTAL].HeaderCell.Value = Resource.String("STR_CAGE_TEXT_COLUMN_TOTAL");
      dgv_common.Columns[GRID_COLUMN_CHIPS_TOTAL].Width = WIDTH_COLUMN_CHIPS_TOTAL;
      dgv_common.Columns[GRID_COLUMN_CHIPS_TOTAL].Visible = true;
      dgv_common.Columns[GRID_COLUMN_CHIPS_TOTAL].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
      dgv_common.Columns[GRID_COLUMN_CHIPS_TOTAL].SortMode = DataGridViewColumnSortMode.NotSortable;
      dgv_common.Columns[GRID_COLUMN_CHIPS_TOTAL].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

      dgv_common.Columns[GRID_COLUMN_CHIPS_STOCK].Width = WIDTH_COLUMN_CHIPS_STOCK;
      dgv_common.Columns[GRID_COLUMN_CHIPS_STOCK].Visible = false;
      dgv_common.Columns[GRID_COLUMN_CHIPS_STOCK].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
      dgv_common.Columns[GRID_COLUMN_CHIPS_STOCK].SortMode = DataGridViewColumnSortMode.NotSortable;

      dgv_common.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
      dgv_common.MultiSelect = false;
    } // GuiStyleSheet

    private void UpdateLabels(Boolean IsApplyTax)
    {
      Int64 _chip_id;
      Int64 _units;
      Currency _total;
      String _diference;
      Currency _diference_amount;
      Boolean _show_currency_symbol;

      _show_currency_symbol = true;

      if (uc_payment_method1.CurrencyExchange.Type == CurrencyExchangeType.CASINO_CHIP_COLOR)
      {
        _show_currency_symbol = false;
      }

      m_chips_operation.ChipsAmount = 0;

      foreach (DataGridViewRow _dgr in dgv_common.Rows)
      {
        _chip_id = (Int64)_dgr.Cells[GRID_COLUMN_CHIPS_ID].Value;
        Int64.TryParse(_dgr.Cells[GRID_COLUMN_CHIPS_UN].Value.ToString(), out _units);

        //if (_units > 0 && _units > m_chips_amount.ChipsStock[_denomination])
        if (m_chips_operation.OperationType == FeatureChips.ChipsOperation.Operation.SALE || m_chips_operation.OperationType == FeatureChips.ChipsOperation.Operation.CHANGE_OUT || m_chips_operation.OperationType == FeatureChips.ChipsOperation.Operation.SWAP)
        {
          if (m_chips_operation.ChipsUnits.ContainsKey(_chip_id) && m_chips_operation.ChipsUnits[_chip_id] > 0)
          {
            if (_units > m_chips_operation.ChipsUnits[_chip_id])
            {
              _dgr.DefaultCellStyle.ForeColor = Color.Red;
            }
            else
            {
              _dgr.DefaultCellStyle.ForeColor = Color.Black;
            }
          }
        }

        if (uc_payment_method1.CurrencyExchange.Type == CurrencyExchangeType.CASINO_CHIP_COLOR)
        {
          _total = _units;
        }
        else
        {
          _total = FeatureChips.Chips.GetChip(_chip_id).Denomination * _units;
        }
        if (_total > 0)
        {
          if (m_chips_operation.IsoCode == m_national_currency.CurrencyCode)
          {
            _dgr.Cells[GRID_COLUMN_CHIPS_TOTAL].Value = _total.ToString(_show_currency_symbol);
          }
          else
          {
            _dgr.Cells[GRID_COLUMN_CHIPS_TOTAL].Value = Currency.Format(_total, uc_payment_method1.CurrencyExchange.CurrencyCode);
          }

        }
        else
        {
          _dgr.Cells[GRID_COLUMN_CHIPS_TOTAL].Value = "";
        }

        m_chips_operation.ChipsAmount += _total;

      } // foreach

      _diference_amount = m_chips_operation.ChipsDifference;

      if (_diference_amount >= 0)
      {
        _diference = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_DIFF_MISSING") + ": ";
        lbl_diference_amount.ForeColor = Color.Black;
      }
      else
      {
        _diference = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_DIFF_EXTRA") + ": ";
        _diference_amount = -_diference_amount;
        lbl_diference_amount.ForeColor = Color.Red;
      }

      lbl_total_delivered_exchange.Visible = false;

      if (m_chips_operation.OperationType == FeatureChips.ChipsOperation.Operation.PURCHASE)
      {
        if (m_national_currency.CurrencyCode != uc_payment_method1.CurrencyExchange.CurrencyCode)
        {
          lbl_total_delivered_exchange.Visible = true;
          lbl_total_delivered_exchange.Text = Resource.String("STR_VOUCHER_TOTAL_AMOUNT") + ": " + Currency.Format(m_chips_operation.ChipsAmount, uc_payment_method1.CurrencyExchange.CurrencyCode);
        }
        lbl_total_delivered.Text = Resource.String("STR_VOUCHER_TOTAL_AMOUNT") + ": " + m_chips_operation.AmountNationalCurrency.ToString(_show_currency_symbol);
        lbl_diference_amount.Text = _diference + _diference_amount.ToString(_show_currency_symbol);
      }
      else
      {
        if (m_national_currency.CurrencyCode == uc_payment_method1.CurrencyExchange.CurrencyCode)
        {
          lbl_total_delivered.Text = Resource.String("STR_VOUCHER_TOTAL_AMOUNT") + ": " + m_chips_operation.ChipsAmount.ToString(_show_currency_symbol);
          lbl_diference_amount.Text = _diference + _diference_amount.ToString(_show_currency_symbol);
        }
        else
        {
          lbl_total_delivered.Text = Resource.String("STR_VOUCHER_TOTAL_AMOUNT") + ": " + Currency.Format(m_chips_operation.ChipsAmount, uc_payment_method1.CurrencyExchange.CurrencyCode);
          lbl_diference_amount.Text = _diference + Currency.Format(_diference_amount, uc_payment_method1.CurrencyExchange.CurrencyCode);
        }
      }

      if (m_chips_operation.OperationType != FeatureChips.ChipsOperation.Operation.CHANGE_OUT)
      {
        if (WSI.Common.TITO.Utils.IsTitoMode() && m_chips_operation.OperationType == FeatureChips.ChipsOperation.Operation.SALE)
        {
          lbl_initial_amount.Text = Currency.Format(m_chips_operation.FillChipsAmount, m_chips_operation.IsoCode);
        }
        else
        {
          lbl_initial_amount.Text = m_chips_operation.FillChipsAmount.ToString(_show_currency_symbol);
        }
      }

      ShowLabels(true);

      if ((m_is_tito_mode || m_is_chip_purchase_with_cash_out) && m_chips_operation.OperationType == FeatureChips.ChipsOperation.Operation.PURCHASE)
      {
        MultiPromos.PromoBalance _ini_balance;
        MultiPromos.PromoBalance _fin_balance;
        TYPE_SPLITS _splits;
        TYPE_CASH_REDEEM _cash_redeem;
        TYPE_CASH_REDEEM _a_redeem;
        TYPE_CASH_REDEEM _b_redeem;
        CashAmount _cash;

        _cash = new CashAmount();
        m_calculate_cash_amount_total_paid = 0;

        if (!Split.ReadSplitParameters(out _splits))
        {
          return;
        }

        m_card_data.AccountBalance = new MultiPromos.AccountBalance(MultiPromos.AccountBalance.Zero);
        m_card_data.AccountBalance.Redeemable += m_chips_operation.AmountNationalCurrency;

        try
        {
          _ini_balance = new MultiPromos.PromoBalance(m_card_data.AccountBalance, m_card_data.PlayerTracking.TruncatedPoints);

          if (m_is_tito_mode)
          {
            m_card_data.MaxDevolution = WSI.Common.TITO.Utils.MaxDevolutionAmount(m_chips_operation.AmountNationalCurrency);
            m_card_data.MaxDevolutionForChips = m_card_data.MaxDevolution;
          }
          m_calculate_cash_amount_ok = _cash.CalculateCashAmount(CASH_MODE.TOTAL_REDEEM, _splits, _ini_balance, 0,
                                                                      m_is_chip_purchase_with_cash_out || m_is_tito_mode, IsApplyTax, m_is_param_buy_chips_enabled,
                                                                      m_card_data, out _fin_balance, out _cash_redeem, out _a_redeem, out _b_redeem);
          if (m_calculate_cash_amount_ok)
          {
            m_calculate_cash_amount_total_paid = _cash_redeem.TotalRedeemAfterTaxes;
            m_chips_operation.ChipsNetAmount = m_calculate_cash_amount_total_paid;

            //JGC 03/05/2018: Trace Net Amount WIGOS-9805
            Misc.WriteLog(string.Format("Frm_Chips > NetAmount = {0}", m_chips_operation.ChipsNetAmount), Log.Type.Message);
          }
          else
          {
            _cash.CalculateCashAmount_GetErrorMessage(_ini_balance.Balance.TotalRedeemable, _cash_redeem.TotalPaid, true, m_card_data,
                                                            out m_calculate_cash_amount_error_message);
          }
          m_cash_redeem = _cash_redeem;
        }
        finally
        {
          m_card_data.AccountBalance.Redeemable -= m_chips_operation.AmountNationalCurrency;
        }

        if (Tax.EnableTitoTaxWaiver(true) && !IsApplyTax)
        {
          lbl_diference_amount.Text = Resource.String("STR_DEDUCTIONS") + ": " + "0";
          lbl_diference_amount.ForeColor = Color.Black;

          if (WSI.Common.TITO.Utils.IsTitoMode() & m_chips_operation.OperationType == FeatureChips.ChipsOperation.Operation.PURCHASE)
          {
            lbl_total_to_pay.Text = Resource.String("STR_VOUCHER_CARD_REPLACEMENT_005") + ": " + m_chips_operation.AmountNationalCurrency.ToString();
          }
          else if (uc_payment_method1.CurrencyExchange.CurrencyCode == m_national_currency.CurrencyCode)
          {
            lbl_total_to_pay.Text = Resource.String("STR_VOUCHER_CARD_REPLACEMENT_005") + ": " + m_chips_operation.ChipsAmount.ToString();
          }
          else
          {
            lbl_total_to_pay.Text = Resource.String("STR_VOUCHER_CARD_REPLACEMENT_005") + ": " + Currency.Format(m_chips_operation.ChipsAmount, uc_payment_method1.CurrencyExchange.CurrencyCode);
          }

          lbl_diference_amount.Visible = true;
          lbl_total_to_pay.Visible = true;
        }
        else
        {

          if (m_chips_operation.AmountNationalCurrency - m_calculate_cash_amount_total_paid >= 0 || m_is_chip_purchase_with_cash_out)
          {
            if (WSI.Common.TITO.Utils.IsTitoMode() & m_chips_operation.OperationType == FeatureChips.ChipsOperation.Operation.PURCHASE)
            {
              m_chips_operation.ChipsTaxAmount = m_chips_operation.AmountNationalCurrency - m_calculate_cash_amount_total_paid;
              lbl_diference_amount.Text = Resource.String("STR_DEDUCTIONS") + ": " + (m_chips_operation.ChipsTaxAmount).ToString();
              lbl_total_to_pay.Text = Resource.String("STR_VOUCHER_CARD_REPLACEMENT_005") + ": " + m_calculate_cash_amount_total_paid.ToString();
            }
            else if (uc_payment_method1.CurrencyExchange.CurrencyCode == m_national_currency.CurrencyCode)
            {
              m_chips_operation.ChipsTaxAmount = m_chips_operation.ChipsAmount - m_calculate_cash_amount_total_paid;
              lbl_diference_amount.Text = Resource.String("STR_DEDUCTIONS") + ": " + (m_chips_operation.ChipsTaxAmount).ToString();
              lbl_total_to_pay.Text = Resource.String("STR_VOUCHER_CARD_REPLACEMENT_005") + ": " + m_calculate_cash_amount_total_paid.ToString();
            }
            else
            {
              m_chips_operation.ChipsTaxAmount = m_chips_operation.ChipsAmount - m_calculate_cash_amount_total_paid;
              lbl_diference_amount.Text = Resource.String("STR_DEDUCTIONS") + ": " + Currency.Format(m_chips_operation.ChipsTaxAmount, uc_payment_method1.CurrencyExchange.CurrencyCode).ToString();
              lbl_total_to_pay.Text = Resource.String("STR_VOUCHER_CARD_REPLACEMENT_005") + ": " + Currency.Format(m_calculate_cash_amount_total_paid, uc_payment_method1.CurrencyExchange.CurrencyCode);
            }
            m_chips_operation.ChipsTaxAmount = (Currency) m_chips_operation.ChipsTaxAmount;

            lbl_diference_amount.ForeColor = Color.Black;
            lbl_diference_amount.Visible = true;
            lbl_total_to_pay.Visible = true;
          }
          else
          {
            m_calculate_cash_amount_total_paid = 0;
            lbl_total_to_pay.Visible = false;
            lbl_diference_amount.Visible = false;
            lbl_total_delivered_exchange.Visible = false;
          }
        }
      }

      btn_ok.Enabled = (m_chips_operation.ChipsAmount > 0);
      btn_check_redeem.Enabled = (m_chips_operation.ChipsAmount > 0 && m_calculate_cash_amount_total_paid >= m_payment_order_min_amount);
      btn_ticket.Enabled = (m_chips_operation.ChipsAmount > 0);

    }

    private void set_dgv_common_next_row()
    {
      Int32 _index_actual;
      Int32 _index_next;
      Int32 _forced_visible_index;
      Int64 _current_chip;
      Int64 _next_chip;

      try
      {
        _index_actual = dgv_common.SelectedRows[0].Index;
        _index_next = (_index_actual + 1) % dgv_common.Rows.Count;
        _current_chip = (Int64)dgv_common.Rows[_index_actual].Cells[GRID_COLUMN_CHIPS_ID].Value;
        _next_chip = (Int64)dgv_common.Rows[_index_next].Cells[GRID_COLUMN_CHIPS_ID].Value;

        while (_index_next != _index_actual)
        {
          if (m_chips_operation.OperationType == FeatureChips.ChipsOperation.Operation.PURCHASE || m_chips_operation.OperationType == FeatureChips.ChipsOperation.Operation.CHANGE_IN ||
              m_chips_operation.OperationType == FeatureChips.ChipsOperation.Operation.CHANGE_OUT ||
              FeatureChips.Chips.GetChip(_next_chip).Stock > 0)
          {

            break;
          }
          _index_next = (_index_next + 1) % dgv_common.Rows.Count;
          _next_chip = (Int64)dgv_common.Rows[_index_next].Cells[GRID_COLUMN_CHIPS_ID].Value;
        }

        dgv_common.Rows[_index_actual].Selected = false;
        dgv_common.Rows[_index_next].Selected = true;

        // RRR 03-MAR-2014: Force selection visibility  
        _forced_visible_index = CalculateForcedVisibleIndex(_index_next);

        if ((_forced_visible_index != _index_next) || (_index_next == 0))
        {
          dgv_common.FirstDisplayedScrollingRowIndex = _forced_visible_index;
        }


        uc_input_amount.Focus();
      }
      catch { }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Sets the next visible index
    //
    //  PARAMS :
    //      - INPUT : CurrentIndex
    //
    //      - OUTPUT : _index
    //
    // RETURNS : Next visble index
    //
    //   NOTES :
    //  
    private Int32 CalculateForcedVisibleIndex(Int32 CurrentIndex)
    {
      Int32 _index;
      Int32 _max_index_visible;

      _index = CurrentIndex;
      _max_index_visible = 12;

      if (CurrentIndex > _max_index_visible)
      {
        _index = CurrentIndex - _max_index_visible;
      }

      return _index;

    } // CalculateForcedVisibleIndex

    private void InitChipsGrid()
    {
      Int32 _idx;
      Boolean _with_stock;
      Int64 _chip_id;
      Currency _amount;
      Int32 _units;
      Currency _total_stock;
      Boolean _show_currency_symbol;
      Int64? GamingTableId;

      dgv_common.Rows.Clear();
      _total_stock = 0;
      _show_currency_symbol = true;

      GamingTableId = null;

      if (m_chips_operation.SelectedGamingTable != null && m_chips_operation.SelectedGamingTable.TableId > 0)
      {
        GamingTableId = m_chips_operation.SelectedGamingTable.TableId;
      }

      if (uc_payment_method1.CurrencyExchange.Type == CurrencyExchangeType.CASINO_CHIP_COLOR)
      {
        _show_currency_symbol = false;
      }

      if (m_form_type == FormType.Chips && m_show_denominations == Cage.ShowDenominationsMode.HideAllDenominations)
      {
        dgv_common.Columns[GRID_COLUMN_CHIPS_UN].Visible = false;
        dgv_common.Columns[GRID_COLUMN_CHIPS_DENOMINATION].Visible = false;

        dgv_common.Columns[GRID_COLUMN_CHIPS_TOTAL].HeaderCell.Value = Resource.String("STR_CAGE_TEXT_COLUMN_TOTAL");
        dgv_common.Columns[GRID_COLUMN_CHIPS_TOTAL].Width = 320;

        dgv_common.Rows.Add("", 1, "", "", "", "", true);
        dgv_common.Rows[0].Cells[GRID_COLUMN_CHIPS_DENOMINATION].Value = (Decimal)1; //value of denomination
        dgv_common.Rows[0].Cells[GRID_COLUMN_CHIPS_COLOR].Value = WSI.Cashier.Resources.ResourceImages.chips; //Resource.Image("chip");
        dgv_common.Rows[0].Cells[GRID_COLUMN_CHIPS_COLOR].Style.SelectionBackColor = Color.White;
      }
      else
      {

        _idx = 0;
        foreach (KeyValuePair<Int64, FeatureChips.Chips.Chip> _chip in FeatureChips.ChipsSets.GetChipsByTypeAndOperation(m_chips_operation.OperationType, uc_payment_method1.CurrencyExchange.Type, uc_payment_method1.CurrencyExchange.CurrencyCode, GamingTableId))
        {
          _chip_id = _chip.Key;
          _units = 0;

          if (m_form_type == FormType.Stock)
          {
            _units = _chip.Value.Stock;
          }
          _with_stock = (_chip.Value.Stock > 0);
          if (_units > 0 || m_form_type == FormType.Stock)
          {
            if (_chip.Value.Type == FeatureChips.ChipType.COLOR)
            {
              _amount = _units;
              dgv_common.Columns[GRID_COLUMN_CHIPS_DENOMINATION].Visible = false;
              dgv_common.Columns[GRID_COLUMN_CHIPS_TOTAL].Visible = false;
              dgv_common.Columns[GRID_COLUMN_CHIPS_NAME].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
              dgv_common.Columns[GRID_COLUMN_CHIPS_DRAW].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
            else
            {
              _amount = _chip.Value.Denomination * _units;
              dgv_common.Columns[GRID_COLUMN_CHIPS_DENOMINATION].Visible = true;
              dgv_common.Columns[GRID_COLUMN_CHIPS_TOTAL].Visible = true;
              dgv_common.Columns[GRID_COLUMN_CHIPS_NAME].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
              dgv_common.Columns[GRID_COLUMN_CHIPS_DRAW].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
            }
            _total_stock += _amount;

            if (m_national_currency.CurrencyCode == uc_payment_method1.CurrencyExchange.CurrencyCode)
            {
              dgv_common.Rows.Add(_chip_id, "", Decimal.Round(_chip.Value.Denomination, 2), _chip.Value.Name, _chip.Value.Drawing, _units, _amount.ToString(_show_currency_symbol), _with_stock);
            }
            else
            {
              dgv_common.Rows.Add(_chip_id, "", Decimal.Round(_chip.Value.Denomination, 2), _chip.Value.Name, _chip.Value.Drawing, _units, Currency.Format(_amount, uc_payment_method1.CurrencyExchange.CurrencyCode), _with_stock);
            }
          }
          else
          {
            dgv_common.Rows.Add(_chip_id, "", Decimal.Round(_chip.Value.Denomination, 2), _chip.Value.Name, _chip.Value.Drawing, "", "", _with_stock);
          }

          dgv_common.Rows[_idx].Cells[GRID_COLUMN_CHIPS_COLOR].Value = WSI.Cashier.Resources.ResourceImages.chips; // Resource.Image("chip");

          dgv_common.Rows[_idx].Cells[GRID_COLUMN_CHIPS_COLOR].Style.SelectionBackColor = Color.White;

          dgv_common.Rows[_idx].Cells[GRID_COLUMN_CHIPS_COLOR].Style.BackColor = Color.FromArgb((Int32)_chip.Value.Color);
          dgv_common.Rows[_idx].Cells[GRID_COLUMN_CHIPS_COLOR].Style.SelectionBackColor = Color.FromArgb((Int32)_chip.Value.Color);

          dgv_common.Rows[_idx].Cells[GRID_COLUMN_CHIPS_NAME].Value = String.IsNullOrEmpty(_chip.Value.Name) ? "" : _chip.Value.Name;
          dgv_common.Rows[_idx].Cells[GRID_COLUMN_CHIPS_DRAW].Value = String.IsNullOrEmpty(_chip.Value.Drawing) ? "" : _chip.Value.Drawing;

          dgv_common.Rows[_idx].Cells[GRID_COLUMN_CHIPS_STOCK].Value = _chip.Value.Stock;

          //Disable rows without stock
          //if (!_with_stock && m_chips_amount.ChipsDenomination[_denomination.Key] == 0)
          if ((m_chips_operation.OperationType == FeatureChips.ChipsOperation.Operation.SALE || m_chips_operation.OperationType == FeatureChips.ChipsOperation.Operation.CHANGE_OUT || m_chips_operation.OperationType == FeatureChips.ChipsOperation.Operation.SWAP) && !_with_stock)
          {
            dgv_common.Rows[_idx].DefaultCellStyle.ForeColor = Color.SlateGray;
            dgv_common.Rows[_idx].Visible = m_row_chips_visible_according_stock;
          }
          _idx++;
        }
      }

      if (m_form_type == FormType.Stock)
      {
        if (uc_payment_method1.CurrencyExchange.Type == CurrencyExchangeType.CASINO_CHIP_COLOR)
        {
          lbl_total_delivered.Text = Resource.String("STR_VOUCHER_TOTAL_AMOUNT") + ": " + Convert.ToInt32(_total_stock).ToString();
        }
        else
        {
          if (m_national_currency.CurrencyCode == uc_payment_method1.CurrencyExchange.CurrencyCode)
          {
            lbl_total_delivered.Text = Resource.String("STR_VOUCHER_TOTAL_AMOUNT") + ": " + _total_stock.ToString(_show_currency_symbol);
          }
          else
          {
            lbl_total_delivered.Text = Resource.String("STR_VOUCHER_TOTAL_AMOUNT") + ": " + Currency.Format(_total_stock, uc_payment_method1.CurrencyExchange.CurrencyCode);
          }
        }
      }

      if (m_form_type == FormType.Chips)
      {
        UpdateLabels(m_is_apply);
      }
    }

    private void ShowLabels(Boolean ShowLabels)
    {
      //LTC 07-MAR-2016
      lbl_diference_amount.Visible = (ShowLabels && (m_chips_operation.OperationType == FeatureChips.ChipsOperation.Operation.SALE || m_chips_operation.OperationType == FeatureChips.ChipsOperation.Operation.SWAP));
      gb_total.Visible = (ShowLabels && (m_chips_operation.OperationType == FeatureChips.ChipsOperation.Operation.SALE || m_chips_operation.OperationType == FeatureChips.ChipsOperation.Operation.CHANGE_OUT || m_chips_operation.OperationType == FeatureChips.ChipsOperation.Operation.SWAP));
      lbl_total_delivered.Visible = ShowLabels;
    } // ShowLabels

    #endregion

    #region Events

    //------------------------------------------------------------------------------
    // PURPOSE : Canceled opening.
    //
    //  PARAMS :
    //      - INPUT :
    //          - object:     sender
    //          - EventArgs:  e
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void btn_cancel_Click(object sender, EventArgs e)
    {
      // Clean To Pay label
      lbl_total_delivered.Text = "";

      DialogResult = DialogResult.Cancel;

      Misc.WriteLog("[FORM CLOSE] frm_chips (cancel)", Log.Type.Message);
      this.Close();
    } // btn_cancel_Click

    private void btn_ok_Click(object sender, EventArgs e)
    {
      btn_ok_event(false);
    } // btn_ok_Click

    private void btn_ok_event(Boolean PrintTicket)
    {
      String _message_error;
      Currency _cashier_current_balance;

      _message_error = String.Empty;
      m_chips_operation.PrintTicket = PrintTicket;

      if (!IsScreenDataOk(out _message_error))
      {
        frm_message.Show(_message_error,
                         Resource.String("STR_APP_GEN_MSG_WARNING"),
                         MessageBoxButtons.OK,
                         MessageBoxIcon.Warning,
                         ParentForm);

        return;
      }

      switch (m_chips_operation.OperationType)
      {
        case FeatureChips.ChipsOperation.Operation.CHANGE_OUT:
          if (m_chips_operation.FillChipsAmount != m_chips_operation.ChipsAmount)
          {
            if (m_chips_operation.IsoCode == m_national_currency.CurrencyCode)
            {
              frm_message.Show(Resource.String("STR_FRM_CHIPS_CHANGE_MESSAGE_ERROR", m_chips_operation.FillChipsAmount, m_chips_operation.ChipsAmount).Replace("\\r\\n", "\r\n"),
                               Resource.String("STR_APP_GEN_MSG_WARNING"),
                               MessageBoxButtons.OK,
                               MessageBoxIcon.Warning,
                               ParentForm);
            }
            else
            {
              frm_message.Show(Resource.String("STR_FRM_CHIPS_CHANGE_MESSAGE_ERROR", Currency.Format(m_chips_operation.FillChipsAmount, m_chips_operation.IsoCode), Currency.Format(m_chips_operation.ChipsAmount, m_chips_operation.IsoCode)).Replace("\\r\\n", "\r\n"),
                             Resource.String("STR_APP_GEN_MSG_WARNING"),
                             MessageBoxButtons.OK,
                             MessageBoxIcon.Warning,
                             ParentForm);
            }

            return;
          }
          break;

        case FeatureChips.ChipsOperation.Operation.PURCHASE:

          // Get cashier current balance
          _cashier_current_balance = 0;
          try
          {
            using (DB_TRX _db_trx = new DB_TRX())
            {
              _cashier_current_balance = CashierBusinessLogic.UpdateSessionBalance(_db_trx.SqlTransaction, Cashier.SessionId, 0);
            }
          }
          catch (Exception _ex)
          {
            Log.Exception(_ex);

            return;
          }

          if (m_is_tito_mode)
          {
            if (!m_calculate_cash_amount_ok)
            {
              frm_message.Show(m_calculate_cash_amount_error_message, Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this);

              return;
            }
          }

          if ((m_is_tito_mode && !m_chips_operation.PrintTicket) || m_is_chip_purchase_with_cash_out)
          {
            if (m_calculate_cash_amount_total_paid > _cashier_current_balance)
            {
              frm_message.Show(Resource.String("STR_FRM_AMOUNT_INPUT_MSG_AMOUNT_EXCEED_BANK"),
                               Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this);

              return;
            }

            // DRV & RCI 20-AUG-2014: Request PIN if configured.
            if (!CashierBusinessLogic.RequestPin(this, Accounts.PinRequestOperationType.WITHDRAWAL, m_card_data, m_chips_operation.ChipsAmount))
            {
              return;
            }
          }
          break;

        case FeatureChips.ChipsOperation.Operation.SALE:
        // LTC 07-MAR-2016
        case FeatureChips.ChipsOperation.Operation.SWAP:
          Boolean _chips_sale_voucher_mode;
          Decimal _total_chips;

          _chips_sale_voucher_mode = GeneralParam.GetBoolean("GamingTables", "Cashier.Mode");

          if (!_chips_sale_voucher_mode)
          {
            using (DB_TRX _db_trx = new DB_TRX())
            {
              if (GamingTablesSessions.GamingTableInfo.IsGamingTable)
              {
                GamingTablesSessions _gaming_table;
                if (!GamingTablesSessions.GetOrOpenSession(out _gaming_table, Cashier.SessionId, _db_trx.SqlTransaction))
                {
                  // TODO SHOW SESSION ERROR 

                  //frm_message.Show(Resource.String(""),
                  //               Resource.String("STR_APP_GEN_MSG_WARNING"),
                  //               MessageBoxButtons.OK,
                  //               MessageBoxIcon.Error,
                  //               ParentForm);

                  return;
                }

                _total_chips = _gaming_table.InitialChipsAmount + _gaming_table.FillChipsAmount + _gaming_table.OwnPurchaseAmount - _gaming_table.OwnSalesAmount;
              }
              else
              {
                _total_chips = WSI.Common.Cashier.UpdateSessionBalance(_db_trx.SqlTransaction, Cashier.SessionId, 0, uc_payment_method1.CurrencyExchange.CurrencyCode, uc_payment_method1.CurrencyExchange.Type);
              }
            }

            if (_total_chips < m_chips_operation.ChipsAmount)
            {
              frm_message.Show(Resource.String("STR_NOT_ENOUGH_CHIPS"),
                               Resource.String("STR_APP_GEN_MSG_WARNING"),
                               MessageBoxButtons.OK,
                               MessageBoxIcon.Warning,
                               ParentForm);

              return;
            }
          }

          if (m_chips_operation.ChipsAmount > m_chips_operation.FillChipsAmount)
          {
            frm_message.Show(Resource.String("STR_FRM_CHIPS_ERROR_LIMIT_DELIVERED"),
                             Resource.String("STR_APP_GEN_MSG_WARNING"),
                             MessageBoxButtons.OK,
                             MessageBoxIcon.Warning,
                             ParentForm);

            return;
          }

          break;

        case FeatureChips.ChipsOperation.Operation.CHANGE_IN:
        case FeatureChips.ChipsOperation.Operation.SHOW_STOCK:
        case FeatureChips.ChipsOperation.Operation.NONE:
        default:
          break;
      }

      DialogResult = DialogResult.OK;

      Misc.WriteLog("[FORM CLOSE] frm_chips (ok)", Log.Type.Message);
      this.Close();

    } // btn_ok


    //------------------------------------------------------------------------------
    // PURPOSE : Validate screen data
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //          - String MessageError
    //
    // RETURNS :
    //      - True: Screen data is correct
    //      - False: Screen data is not correct
    //
    //   NOTES :
    //
    private Boolean IsScreenDataOk(out String MessageError)
    {
      Int64 _units;
      Int64 _chip_id;

      MessageError = String.Empty;
      //LTC 07-MAR-2016
      if (m_chips_operation.OperationType == FeatureChips.ChipsOperation.Operation.SALE || m_chips_operation.OperationType == FeatureChips.ChipsOperation.Operation.SWAP)
      {
        if (m_integrated_chip_operation && m_chips_operation.FillChipsAmount != m_chips_operation.ChipsAmount)
        {
          if (m_chips_operation.IsoCode == m_national_currency.CurrencyCode)
          {
            MessageError = Resource.String("STR_FRM_CHIPS_ERROR_LIMIT_DELIVERED_EQUAL_AMOUNT", m_chips_operation.ChipsAmount, m_chips_operation.FillChipsAmount).Replace("\\r\\n", "\r\n");
          }
          else
          {
            MessageError = Resource.String("STR_FRM_CHIPS_ERROR_LIMIT_DELIVERED_EQUAL_AMOUNT", Currency.Format(m_chips_operation.ChipsAmount, m_chips_operation.IsoCode),
                                                                                               Currency.Format(m_chips_operation.FillChipsAmount, m_chips_operation.IsoCode).Replace("\\r\\n", "\r\n"));
          }

          return false;
        }
      }
      // Validate Amount //
      //LTC 07-MAR-2016
      if (!m_integrated_chip_operation && !m_is_tito_mode && (m_chips_operation.OperationType == FeatureChips.ChipsOperation.Operation.SALE || m_chips_operation.OperationType == FeatureChips.ChipsOperation.Operation.SWAP))
      {
        if (m_chips_operation.ChipsAmount > m_card_data.AccountBalance.TotalRedeemable)
        {
          MessageError = Resource.String("STR_FRM_CHIPS_ERROR_LIMIT_ACOCUNT_CREDITS", m_chips_operation.ChipsAmount, (Currency)m_card_data.AccountBalance.TotalRedeemable).Replace("\\r\\n", "\r\n");

          return false;

        }
      }

      // Validate Stock //
      //LTC 07-MAR-2016
      if (m_chips_operation.OperationType == FeatureChips.ChipsOperation.Operation.SALE ||
          m_chips_operation.OperationType == FeatureChips.ChipsOperation.Operation.CHANGE_OUT ||
          m_chips_operation.OperationType == FeatureChips.ChipsOperation.Operation.SWAP)
      {
        foreach (DataGridViewRow _dgr in dgv_common.Rows)
        {
          _chip_id = (Int64)_dgr.Cells[GRID_COLUMN_CHIPS_ID].Value;
          Int64.TryParse(_dgr.Cells[GRID_COLUMN_CHIPS_UN].Value.ToString(), out _units);

          if (_units > 0 && _units > FeatureChips.Chips.GetChip(_chip_id).Stock)
          {
            MessageError = Resource.String("STR_FRM_CHIPS_ERROR_STOCK");

            return false;
          }
        } // foreach
      }


      // Validate MaxAllowed //
      if (m_max_allowed_operation > 0 && m_chips_operation.ChipsAmount > m_max_allowed_operation)
      {
        // LTC 07-MAR-2016
        if (m_chips_operation.OperationType == FeatureChips.ChipsOperation.Operation.SALE || m_chips_operation.OperationType == FeatureChips.ChipsOperation.Operation.SWAP)
        {
          //MessageError = Resource.String("STR_FRM_CHIPS_ERROR_MAX_ALLOWED_SALE", m_max_allowed_operation).Replace("\\r\\n", "\r\n");

          if (!ProfilePermissions.CheckPermissionList(ProfilePermissions.CashierFormFuncionality.ChipsSales_MaxAllowed, ProfilePermissions.TypeOperation.RequestPasswd, this, 0, out MessageError))
          {
            return false;
          }
        }
        else
        {
          MessageError = Resource.String("STR_FRM_CHIPS_ERROR_MAX_ALLOWED_PURCHASE", m_max_allowed_operation).Replace("\\r\\n", "\r\n");

          if (!ProfilePermissions.CheckPermissionList(ProfilePermissions.CashierFormFuncionality.ChipsPurchase_MaxAllowed, ProfilePermissions.TypeOperation.RequestPasswd, this, 0, out MessageError))
          {
            return false;
          }
        }
        //return false;
      }

      return true;
    } // IsScreenDataOk

    //------------------------------------------------------------------------------
    // PURPOSE : Handle the PreviewKeyDown.
    //
    //  PARAMS :
    //      - INPUT :
    //          - object:                   sender
    //          - PreviewKeyDownEventArgs:  e
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void frm_chips_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
    {
      char c;

      c = Convert.ToChar(e.KeyCode);
      if (c == '\r')
      {
        e.IsInputKey = true;
      }
    } // frm_chips_PreviewKeyDown

    private void btn_clear_Click(object sender, EventArgs e)
    {
      Int64 _chip_id;

      foreach (DataGridViewRow _dgr in dgv_common.Rows)
      {
        _chip_id = (Int64)_dgr.Cells[GRID_COLUMN_CHIPS_ID].Value;
        m_chips_operation.RemoveChip(_chip_id);
        _dgr.Cells[GRID_COLUMN_CHIPS_UN].Value = "";
        _dgr.Cells[GRID_COLUMN_CHIPS_TOTAL].Value = "";
      } // foreach

      m_chips_operation.ChipsAmount = 0;

      uc_input_amount.txt_amount.Text = string.Empty;

      UpdateLabels(m_is_apply);

      btn_ok.Enabled = false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : When selected row is different to "Coins", the bottom dot is disabled.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void dgv_common_SelectionChanged(object sender, EventArgs e)
    {
      if (dgv_common.SelectedRows.Count > 0)
      {
        uc_input_amount.txt_amount.Text = dgv_common.SelectedRows[0].Cells[GRID_COLUMN_CHIPS_UN].Value.ToString().Trim();
        uc_input_amount.Focus();
      }
    }// dgv_common_SelectionChanged

    private void btn_high_distribution_Click(object sender, EventArgs e)
    {
      //InitChipsGrid(ChipsCalculateType.HIGH_CHIPS);
    }//btn_high_distribution_Click

    private void btn_medium_distribution_Click(object sender, EventArgs e)
    {
      //InitChipsGrid(ChipsCalculateType.MEDIUM_CHIPS);
    } //btn_medium_distribution_Click

    private void btn_small_distribution_Click(object sender, EventArgs e)
    {
      //InitChipsGrid(ChipsCalculateType.SMALL_CHIPS);
    } //btn_small_distribution_Click

    private void btn_check_redeem_Click(object sender, EventArgs e)
    {
      if (!CashierBusinessLogic.RequestPin(this, Accounts.PinRequestOperationType.WITHDRAWAL, m_card_data, m_chips_operation.ChipsAmount))
      {
        return;
      }

      m_out_check_redeem = true;

      DialogResult = DialogResult.OK;

      Misc.WriteLog("[FORM CLOSE] frm_chips (check_redeem)", Log.Type.Message);
      this.Close();

    }

    private void btn_intro(Decimal Amount)
    {
      Int64 _chip_id;
      Int32 _amount;

      if (dgv_common.SelectedRows.Count == 0)
      {
        frm_message.Show(Resource.String("STR_DENOMINATION_UNSELECTED"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this);

        return;
      }

      _chip_id = (Int64)dgv_common.SelectedRows[0].Cells[GRID_COLUMN_CHIPS_ID].Value;
      if (m_chips_operation.OperationType == FeatureChips.ChipsOperation.Operation.PURCHASE || m_chips_operation.OperationType == FeatureChips.ChipsOperation.Operation.CHANGE_IN
        || ((m_chips_operation.OperationType == FeatureChips.ChipsOperation.Operation.SALE || m_chips_operation.OperationType == FeatureChips.ChipsOperation.Operation.CHANGE_OUT || m_chips_operation.OperationType == FeatureChips.ChipsOperation.Operation.SWAP) && FeatureChips.Chips.GetChip(_chip_id).Stock > 0))
      {
        _amount = (Int32)Amount;

        if (_amount > 0)
        {
          dgv_common.SelectedRows[0].Cells[GRID_COLUMN_CHIPS_UN].Value = _amount;
        }
        else
        {
          dgv_common.SelectedRows[0].Cells[GRID_COLUMN_CHIPS_UN].Value = "";
        }

        if (m_chips_operation.ChipsUnits.ContainsKey(_chip_id))
        {
          m_chips_operation.RemoveChip(_chip_id);
        }
        m_chips_operation.AddChip(_chip_id, _amount);

        set_dgv_common_next_row();
        uc_input_amount.txt_amount.Text = dgv_common.SelectedRows[0].Cells[GRID_COLUMN_CHIPS_UN].Value.ToString();

        if (Tax.EnableTitoTaxWaiver(true) && !Tax.ApplyTaxChipsPurchase())
        {
          if (!btn_apply_tax.Enabled)
          {
            btn_apply_tax.Enabled = true;
            m_is_apply = m_is_apply_initial;
          }
          UpdateLabels(m_is_apply);
        }
        else
        {
          UpdateLabels(m_is_apply);
        }
      }
    }

    private void btn_ticket_Click(object sender, EventArgs e)
    {
      String _error_str;

      if (!ProfilePermissions.CheckPermissionList(ProfilePermissions.CashierFormFuncionality.TITO_CreateRETicket,
                                                  ProfilePermissions.TypeOperation.RequestPasswd, this, 0, out _error_str))
      {

        return;
      }

      btn_ok_event(true);
    } // btn_ticket_Click

    private void frm_chips_Shown(object sender, EventArgs e)
    {
      uc_input_amount.Focus();
    }

    private void btn_apply_tax_Click(object sender, EventArgs e)
    {
      string _error_str;
      _error_str = string.Empty;

      if (!ProfilePermissions.CheckPermissionList(ProfilePermissions.CashierFormFuncionality.HandpayTaxCollector,
                                                ProfilePermissions.TypeOperation.RequestPasswd, this, 0, out _error_str))
      {

        return;
      }

      m_form_catalog = new frm_catalog(Tax.ApplyTaxChipsPurchase());
      m_form_catalog.Show(this, out m_is_apply_result, out m_account_operation_reason_id, out m_account_operation_comment);

      m_chips_operation.AccountOperationReasonId = m_account_operation_reason_id;
      m_chips_operation.AccountOperationComment = m_account_operation_comment;

      if (m_is_apply_result)
      {
        if (Tax.ApplyTaxChipsPurchase())
        {
          m_is_apply = true;
          m_is_param_buy_chips_enabled = false;
          m_chips_operation.IsApplyTaxes = true;
        }
        else
        {
          m_is_apply = false;
          m_is_param_buy_chips_enabled = true;
          m_chips_operation.IsApplyTaxes = false;
        }
        UpdateLabels(m_is_apply);
        btn_apply_tax.Enabled = false;
      }
    }

    private void uc_payment_method1_CurrencyChanged(CurrencyExchange CurrencyExchange)
    {
      if (uc_payment_method1.CurrencyExchange != null)
      {
        InitChipsGrid();

        m_chips_operation.IsoCode = uc_payment_method1.CurrencyExchange.CurrencyCode;
        m_chips_operation.CurrencyExchangeType = uc_payment_method1.CurrencyExchange.Type;
        m_chips_operation.ChipsUnits.Clear();
      }

      InitLabels();
    }

    /// <summary>
    /// According to the overall value 'm_row_chips_visible_according_stock' is displayed or not 
    /// the rows of tiles that do not have stock.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btn_show_chips_Click(object sender, EventArgs e)
    {
      if (m_row_chips_visible_according_stock)
      {
        btn_show_chips.Text = Resource.String("STR_SHOW_CHIPS_WITHOUT_STOCK");
        m_row_chips_visible_according_stock = false;
      }
      else
      {
        btn_show_chips.Text = Resource.String("STR_HIDE_CHIPS_WITHOUT_STOCK");
        m_row_chips_visible_according_stock = true;
      }

      RowsVisibility();
    }

    /// <summary>
    /// According to the overall value 'm_row_chips_visible_according_stock' is displayed or not 
    /// the rows of tiles that do not have stock.
    /// </summary>
    private void RowsVisibility()
    {
      Int16 _idx;

      _idx = 0;

      foreach (DataGridViewRow _dgr in dgv_common.Rows)
      {
        if (Convert.ToInt32(_dgr.Cells[GRID_COLUMN_CHIPS_STOCK].Value) == 0)
        {
          dgv_common.Rows[_idx].Visible = m_row_chips_visible_according_stock;
        }

        _idx++;
      }
    }

    #endregion


  } //class frm_chips
}