//namespace WSI.Cashier
//{
//  partial class frm_account_user_edit
//  {
//    /// <summary>
//    /// Required designer variable.
//    /// </summary>
//    private System.ComponentModel.IContainer components = null;

//    /// <summary>
//    /// Clean up any resources being used.
//    /// </summary>
//    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
//    protected override void Dispose (bool disposing)
//    {
//      if ( disposing && ( components != null ) )
//      {
//        components.Dispose ();
//      }
//      base.Dispose (disposing);
//    }

//    #region Windows Form Designer generated code

//    /// <summary>
//    /// Required method for Designer support - do not modify
//    /// the contents of this method with the code editor.
//    /// </summary>
//    private void InitializeComponent ()
//    {
//      this.components = new System.ComponentModel.Container();
//      this.splitContainer1 = new System.Windows.Forms.SplitContainer();
//      this.lbl_account_edit_title = new System.Windows.Forms.Label();
//      this.lbl_msg_blink = new System.Windows.Forms.Label();
//      this.btn_keyboard = new System.Windows.Forms.Button();
//      this.groupBox1 = new System.Windows.Forms.GroupBox();
//      this.lbl_mode = new System.Windows.Forms.Label();
//      this.lbl_account = new System.Windows.Forms.Label();
//      this.lbl_mode_value = new System.Windows.Forms.Label();
//      this.lbl_account_id_value = new System.Windows.Forms.Label();
//      this.btn_mode_to_anonymous = new System.Windows.Forms.Button();
//      this.btn_mode_to_personal = new System.Windows.Forms.Button();
//      this.lbl_name = new System.Windows.Forms.Label();
//      this.txt_name = new System.Windows.Forms.TextBox();
//      this.pictureBox1 = new System.Windows.Forms.PictureBox();
//      this.btn_cancel = new System.Windows.Forms.Button();
//      this.btn_ok = new System.Windows.Forms.Button();
//      this.timer1 = new System.Windows.Forms.Timer(this.components);
//      this.splitContainer1.Panel1.SuspendLayout();
//      this.splitContainer1.Panel2.SuspendLayout();
//      this.splitContainer1.SuspendLayout();
//      this.groupBox1.SuspendLayout();
//      ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
//      this.SuspendLayout();
//      // 
//      // splitContainer1
//      // 
//      this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
//      this.splitContainer1.IsSplitterFixed = true;
//      this.splitContainer1.Location = new System.Drawing.Point(0, 0);
//      this.splitContainer1.Name = "splitContainer1";
//      this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
//      // 
//      // splitContainer1.Panel1
//      // 
//      this.splitContainer1.Panel1.BackColor = System.Drawing.Color.CornflowerBlue;
//      this.splitContainer1.Panel1.Controls.Add(this.lbl_account_edit_title);
//      // 
//      // splitContainer1.Panel2
//      // 
//      this.splitContainer1.Panel2.Controls.Add(this.lbl_msg_blink);
//      this.splitContainer1.Panel2.Controls.Add(this.btn_keyboard);
//      this.splitContainer1.Panel2.Controls.Add(this.groupBox1);
//      this.splitContainer1.Panel2.Controls.Add(this.btn_cancel);
//      this.splitContainer1.Panel2.Controls.Add(this.btn_ok);
//      this.splitContainer1.Size = new System.Drawing.Size(462, 253);
//      this.splitContainer1.SplitterDistance = 25;
//      this.splitContainer1.TabIndex = 0;
//      // 
//      // lbl_account_edit_title
//      // 
//      this.lbl_account_edit_title.AutoSize = true;
//      this.lbl_account_edit_title.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//      this.lbl_account_edit_title.ForeColor = System.Drawing.Color.White;
//      this.lbl_account_edit_title.Location = new System.Drawing.Point(3, 5);
//      this.lbl_account_edit_title.Name = "lbl_account_edit_title";
//      this.lbl_account_edit_title.Size = new System.Drawing.Size(159, 16);
//      this.lbl_account_edit_title.TabIndex = 0;
//      this.lbl_account_edit_title.Text = "xAccount Data Edition";
//      // 
//      // lbl_msg_blink
//      // 
//      this.lbl_msg_blink.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
//                  | System.Windows.Forms.AnchorStyles.Right)));
//      this.lbl_msg_blink.BackColor = System.Drawing.Color.Red;
//      this.lbl_msg_blink.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//      this.lbl_msg_blink.ForeColor = System.Drawing.Color.White;
//      this.lbl_msg_blink.Location = new System.Drawing.Point(15, 129);
//      this.lbl_msg_blink.Name = "lbl_msg_blink";
//      this.lbl_msg_blink.Size = new System.Drawing.Size(426, 23);
//      this.lbl_msg_blink.TabIndex = 18;
//      this.lbl_msg_blink.Text = "xPERSONAL NAME CANNOT BE BLANK";
//      this.lbl_msg_blink.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
//      this.lbl_msg_blink.Visible = false;
//      // 
//      // btn_keyboard
//      // 
//      this.btn_keyboard.Image = global::WSI.Cashier.Properties.Resources.keyboard;
//      this.btn_keyboard.Location = new System.Drawing.Point(12, 179);
//      this.btn_keyboard.Name = "btn_keyboard";
//      this.btn_keyboard.Size = new System.Drawing.Size(45, 34);
//      this.btn_keyboard.TabIndex = 14;
//      this.btn_keyboard.UseVisualStyleBackColor = false;
//      this.btn_keyboard.Click += new System.EventHandler(this.btn_keyboard_Click);
//      // 
//      // groupBox1
//      // 
//      this.groupBox1.Controls.Add(this.lbl_mode);
//      this.groupBox1.Controls.Add(this.lbl_account);
//      this.groupBox1.Controls.Add(this.lbl_mode_value);
//      this.groupBox1.Controls.Add(this.lbl_account_id_value);
//      this.groupBox1.Controls.Add(this.btn_mode_to_anonymous);
//      this.groupBox1.Controls.Add(this.btn_mode_to_personal);
//      this.groupBox1.Controls.Add(this.lbl_name);
//      this.groupBox1.Controls.Add(this.txt_name);
//      this.groupBox1.Controls.Add(this.pictureBox1);
//      this.groupBox1.Location = new System.Drawing.Point(12, 4);
//      this.groupBox1.Name = "groupBox1";
//      this.groupBox1.Size = new System.Drawing.Size(438, 113);
//      this.groupBox1.TabIndex = 14;
//      this.groupBox1.TabStop = false;
//      // 
//      // lbl_mode
//      // 
//      this.lbl_mode.AutoSize = true;
//      this.lbl_mode.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//      this.lbl_mode.Location = new System.Drawing.Point(106, 45);
//      this.lbl_mode.Name = "lbl_mode";
//      this.lbl_mode.Size = new System.Drawing.Size(54, 16);
//      this.lbl_mode.TabIndex = 17;
//      this.lbl_mode.Text = "xMode";
//      // 
//      // lbl_account
//      // 
//      this.lbl_account.AutoSize = true;
//      this.lbl_account.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//      this.lbl_account.Location = new System.Drawing.Point(94, 15);
//      this.lbl_account.Name = "lbl_account";
//      this.lbl_account.Size = new System.Drawing.Size(70, 16);
//      this.lbl_account.TabIndex = 16;
//      this.lbl_account.Text = "xAccount";
//      // 
//      // lbl_mode_value
//      // 
//      this.lbl_mode_value.AutoSize = true;
//      this.lbl_mode_value.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//      this.lbl_mode_value.Location = new System.Drawing.Point(163, 45);
//      this.lbl_mode_value.Name = "lbl_mode_value";
//      this.lbl_mode_value.Size = new System.Drawing.Size(89, 16);
//      this.lbl_mode_value.TabIndex = 15;
//      this.lbl_mode_value.Text = "XXXXXXXXX";
//      // 
//      // lbl_account_id_value
//      // 
//      this.lbl_account_id_value.AutoSize = true;
//      this.lbl_account_id_value.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//      this.lbl_account_id_value.Location = new System.Drawing.Point(162, 16);
//      this.lbl_account_id_value.Name = "lbl_account_id_value";
//      this.lbl_account_id_value.Size = new System.Drawing.Size(128, 16);
//      this.lbl_account_id_value.TabIndex = 14;
//      this.lbl_account_id_value.Text = "888888888888888";
//      // 
//      // btn_mode_to_anonymous
//      // 
//      this.btn_mode_to_anonymous.Location = new System.Drawing.Point(293, 16);
//      this.btn_mode_to_anonymous.Name = "btn_mode_to_anonymous";
//      this.btn_mode_to_anonymous.Size = new System.Drawing.Size(139, 41);
//      this.btn_mode_to_anonymous.TabIndex = 13;
//      this.btn_mode_to_anonymous.Text = "xChange to Anonymous Mode";
//      this.btn_mode_to_anonymous.UseVisualStyleBackColor = false;
//      this.btn_mode_to_anonymous.Click += new System.EventHandler(this.btn_anonym_Click);
//      // 
//      // btn_mode_to_personal
//      // 
//      this.btn_mode_to_personal.Location = new System.Drawing.Point(293, 16);
//      this.btn_mode_to_personal.Name = "btn_mode_to_personal";
//      this.btn_mode_to_personal.Size = new System.Drawing.Size(139, 41);
//      this.btn_mode_to_personal.TabIndex = 12;
//      this.btn_mode_to_personal.Text = "xChange to Personal Mode";
//      this.btn_mode_to_personal.UseVisualStyleBackColor = false;
//      this.btn_mode_to_personal.Click += new System.EventHandler(this.btn_personal_Click);
//      // 
//      // lbl_name
//      // 
//      this.lbl_name.AutoSize = true;
//      this.lbl_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//      this.lbl_name.Location = new System.Drawing.Point(31, 89);
//      this.lbl_name.Name = "lbl_name";
//      this.lbl_name.Size = new System.Drawing.Size(56, 16);
//      this.lbl_name.TabIndex = 1;
//      this.lbl_name.Text = "xName";
//      this.lbl_name.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
//      // 
//      // txt_name
//      // 
//      this.txt_name.BackColor = System.Drawing.Color.LightGoldenrodYellow;
//      this.txt_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//      this.txt_name.Location = new System.Drawing.Point(100, 80);
//      this.txt_name.MaxLength = 50;
//      this.txt_name.Name = "txt_name";
//      this.txt_name.Size = new System.Drawing.Size(330, 29);
//      this.txt_name.TabIndex = 2;
//      // 
//      // pictureBox1
//      // 
//      this.pictureBox1.Image = global::WSI.Cashier.Properties.Resources.CardOnly;
//      this.pictureBox1.Location = new System.Drawing.Point(6, 9);
//      this.pictureBox1.Name = "pictureBox1";
//      this.pictureBox1.Size = new System.Drawing.Size(85, 65);
//      this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
//      this.pictureBox1.TabIndex = 0;
//      this.pictureBox1.TabStop = false;
//      // 
//      // btn_cancel
//      // 
//      this.btn_cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
//      this.btn_cancel.Location = new System.Drawing.Point(322, 171);
//      this.btn_cancel.Name = "btn_cancel";
//      this.btn_cancel.Size = new System.Drawing.Size(128, 48);
//      this.btn_cancel.TabIndex = 11;
//      this.btn_cancel.Text = "xCancel";
//      this.btn_cancel.UseVisualStyleBackColor = false;
//      this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
//      // 
//      // btn_ok
//      // 
//      this.btn_ok.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
//      this.btn_ok.Location = new System.Drawing.Point(188, 171);
//      this.btn_ok.Name = "btn_ok";
//      this.btn_ok.Size = new System.Drawing.Size(128, 48);
//      this.btn_ok.TabIndex = 10;
//      this.btn_ok.Text = "xOk";
//      this.btn_ok.UseVisualStyleBackColor = false;
//      this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
//      // 
//      // timer1
//      // 
//      this.timer1.Interval = 4000;
//      this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
//      // 
//      // frm_account_user_edit
//      // 
//      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
//      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
//      this.BackColor = System.Drawing.Color.MediumTurquoise;
//      this.ClientSize = new System.Drawing.Size(462, 253);
//      this.ControlBox = false;
//      this.Controls.Add(this.splitContainer1);
//      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
//      this.MaximizeBox = false;
//      this.MinimizeBox = false;
//      this.Name = "frm_account_user_edit";
//      this.ShowIcon = false;
//      this.ShowInTaskbar = false;
//      this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
//      this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
//      this.Text = "frm_account_user_edit";
//      this.TopMost = true;
//      this.splitContainer1.Panel1.ResumeLayout(false);
//      this.splitContainer1.Panel1.PerformLayout();
//      this.splitContainer1.Panel2.ResumeLayout(false);
//      this.splitContainer1.ResumeLayout(false);
//      this.groupBox1.ResumeLayout(false);
//      this.groupBox1.PerformLayout();
//      ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
//      this.Shown += new System.EventHandler(this.frm_account_user_Shown);
//      this.ResumeLayout(false);

//    }

//    #endregion

//    private System.Windows.Forms.SplitContainer splitContainer1;
//    private System.Windows.Forms.Label lbl_account_edit_title;
//    private System.Windows.Forms.TextBox txt_name;
//    private System.Windows.Forms.Label lbl_name;
//    private System.Windows.Forms.PictureBox pictureBox1;
//    private System.Windows.Forms.Button btn_cancel;
//    private System.Windows.Forms.Button btn_ok;
//    private System.Windows.Forms.GroupBox groupBox1;
//    private System.Windows.Forms.Button btn_mode_to_anonymous;
//    private System.Windows.Forms.Button btn_mode_to_personal;
//    private System.Windows.Forms.Button btn_keyboard;
//    private System.Windows.Forms.Label lbl_mode;
//    private System.Windows.Forms.Label lbl_account;
//    private System.Windows.Forms.Label lbl_mode_value;
//    private System.Windows.Forms.Label lbl_account_id_value;
//    private System.Windows.Forms.Label lbl_msg_blink;
//    internal System.Windows.Forms.Timer timer1;
//  }
//}