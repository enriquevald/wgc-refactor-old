namespace WSI.Cashier
{
  partial class uc_datetime
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.pnl_main = new System.Windows.Forms.FlowLayoutPanel();
      this.lbl_date_text = new WSI.Cashier.Controls.uc_label();
      this.txt_day = new WSI.Cashier.NumericTextBox();
      this.lbl_sep1 = new WSI.Cashier.Controls.uc_label();
      this.txt_month = new WSI.Cashier.NumericTextBox();
      this.lbl_sep2 = new WSI.Cashier.Controls.uc_label();
      this.txt_year = new WSI.Cashier.NumericTextBox();
      this.lbl_format = new WSI.Cashier.Controls.uc_label();
      this.pnl_main.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_main
      // 
      this.pnl_main.Controls.Add(this.lbl_date_text);
      this.pnl_main.Controls.Add(this.txt_day);
      this.pnl_main.Controls.Add(this.lbl_sep1);
      this.pnl_main.Controls.Add(this.txt_month);
      this.pnl_main.Controls.Add(this.lbl_sep2);
      this.pnl_main.Controls.Add(this.txt_year);
      this.pnl_main.Controls.Add(this.lbl_format);
      this.pnl_main.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pnl_main.Location = new System.Drawing.Point(0, 0);
      this.pnl_main.Margin = new System.Windows.Forms.Padding(0);
      this.pnl_main.Name = "pnl_main";
      this.pnl_main.Size = new System.Drawing.Size(320, 41);
      this.pnl_main.TabIndex = 1009;
      this.pnl_main.WrapContents = false;
      // 
      // lbl_date_text
      // 
      this.lbl_date_text.BackColor = System.Drawing.Color.Transparent;
      this.pnl_main.SetFlowBreak(this.lbl_date_text, true);
      this.lbl_date_text.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_date_text.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_date_text.Location = new System.Drawing.Point(0, 0);
      this.lbl_date_text.Margin = new System.Windows.Forms.Padding(0);
      this.lbl_date_text.Name = "lbl_date_text";
      this.lbl_date_text.Size = new System.Drawing.Size(94, 41);
      this.lbl_date_text.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_date_text.TabIndex = 0;
      this.lbl_date_text.Text = "xDateText";
      this.lbl_date_text.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      this.lbl_date_text.SizeChanged += new System.EventHandler(this.lbl_date_text_SizeChanged);
      // 
      // txt_day
      // 
      this.txt_day.AllowSpace = false;
      this.txt_day.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_day.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_day.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_day.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_day.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_day.CornerRadius = 5;
      this.txt_day.FillWithCeros = false;
      this.txt_day.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_day.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.txt_day.Location = new System.Drawing.Point(94, 0);
      this.txt_day.Margin = new System.Windows.Forms.Padding(0);
      this.txt_day.MaxLength = 2;
      this.txt_day.Multiline = false;
      this.txt_day.Name = "txt_day";
      this.txt_day.PasswordChar = '\0';
      this.txt_day.ReadOnly = false;
      this.txt_day.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_day.SelectedText = "";
      this.txt_day.SelectionLength = 0;
      this.txt_day.SelectionStart = 0;
      this.txt_day.Size = new System.Drawing.Size(49, 40);
      this.txt_day.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_day.TabIndex = 1;
      this.txt_day.TabStop = false;
      this.txt_day.Text = "31";
      this.txt_day.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.txt_day.UseSystemPasswordChar = false;
      this.txt_day.WaterMark = "DD";
      this.txt_day.TextChanged += new System.EventHandler(this.txt_date_TextChanged);
      // 
      // lbl_sep1
      // 
      this.lbl_sep1.BackColor = System.Drawing.Color.Transparent;
      this.lbl_sep1.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_sep1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_sep1.Location = new System.Drawing.Point(143, 0);
      this.lbl_sep1.Margin = new System.Windows.Forms.Padding(0);
      this.lbl_sep1.Name = "lbl_sep1";
      this.lbl_sep1.Size = new System.Drawing.Size(19, 40);
      this.lbl_sep1.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_sep1.TabIndex = 2;
      this.lbl_sep1.Text = "/";
      this.lbl_sep1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // txt_month
      // 
      this.txt_month.AllowSpace = false;
      this.txt_month.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_month.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_month.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_month.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_month.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_month.CornerRadius = 5;
      this.txt_month.FillWithCeros = false;
      this.txt_month.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_month.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.txt_month.Location = new System.Drawing.Point(162, 0);
      this.txt_month.Margin = new System.Windows.Forms.Padding(0);
      this.txt_month.MaxLength = 2;
      this.txt_month.Multiline = false;
      this.txt_month.Name = "txt_month";
      this.txt_month.PasswordChar = '\0';
      this.txt_month.ReadOnly = false;
      this.txt_month.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_month.SelectedText = "";
      this.txt_month.SelectionLength = 0;
      this.txt_month.SelectionStart = 0;
      this.txt_month.Size = new System.Drawing.Size(49, 40);
      this.txt_month.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_month.TabIndex = 3;
      this.txt_month.TabStop = false;
      this.txt_month.Text = "12";
      this.txt_month.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.txt_month.UseSystemPasswordChar = false;
      this.txt_month.WaterMark = "MM";
      this.txt_month.TextChanged += new System.EventHandler(this.txt_date_TextChanged);
      // 
      // lbl_sep2
      // 
      this.lbl_sep2.BackColor = System.Drawing.Color.Transparent;
      this.lbl_sep2.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_sep2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_sep2.Location = new System.Drawing.Point(211, 0);
      this.lbl_sep2.Margin = new System.Windows.Forms.Padding(0);
      this.lbl_sep2.Name = "lbl_sep2";
      this.lbl_sep2.Size = new System.Drawing.Size(19, 40);
      this.lbl_sep2.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_sep2.TabIndex = 4;
      this.lbl_sep2.Text = "/";
      this.lbl_sep2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // txt_year
      // 
      this.txt_year.AllowSpace = false;
      this.txt_year.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_year.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_year.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_year.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_year.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_year.CornerRadius = 5;
      this.txt_year.FillWithCeros = false;
      this.txt_year.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_year.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.txt_year.Location = new System.Drawing.Point(230, 0);
      this.txt_year.Margin = new System.Windows.Forms.Padding(0);
      this.txt_year.MaxLength = 4;
      this.txt_year.Multiline = false;
      this.txt_year.Name = "txt_year";
      this.txt_year.PasswordChar = '\0';
      this.txt_year.ReadOnly = false;
      this.txt_year.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_year.SelectedText = "";
      this.txt_year.SelectionLength = 0;
      this.txt_year.SelectionStart = 0;
      this.txt_year.Size = new System.Drawing.Size(68, 40);
      this.txt_year.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_year.TabIndex = 5;
      this.txt_year.TabStop = false;
      this.txt_year.Text = "1990";
      this.txt_year.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.txt_year.UseSystemPasswordChar = false;
      this.txt_year.WaterMark = "YYYY";
      this.txt_year.TextChanged += new System.EventHandler(this.txt_date_TextChanged);
      // 
      // lbl_format
      // 
      this.lbl_format.BackColor = System.Drawing.Color.Transparent;
      this.lbl_format.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_format.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_format.Location = new System.Drawing.Point(298, 0);
      this.lbl_format.Margin = new System.Windows.Forms.Padding(0);
      this.lbl_format.Name = "lbl_format";
      this.lbl_format.Size = new System.Drawing.Size(39, 41);
      this.lbl_format.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_format.TabIndex = 6;
      this.lbl_format.Text = "xFormat";
      this.lbl_format.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.lbl_format.Visible = false;
      this.lbl_format.SizeChanged += new System.EventHandler(this.lbl_format_SizeChanged);
      // 
      // uc_datetime
      // 
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
      this.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
      this.Controls.Add(this.pnl_main);
      this.Name = "uc_datetime";
      this.Size = new System.Drawing.Size(320, 41);
      this.Resize += new System.EventHandler(this.uc_datetime_Resize);
      this.pnl_main.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private NumericTextBox txt_year;
    private WSI.Cashier.Controls.uc_label lbl_sep2;
    private NumericTextBox txt_month;
    private WSI.Cashier.Controls.uc_label lbl_sep1;
    private NumericTextBox txt_day;
    private WSI.Cashier.Controls.uc_label lbl_date_text;
    private System.Windows.Forms.FlowLayoutPanel pnl_main;
    private Controls.uc_label lbl_format;

  }
}
