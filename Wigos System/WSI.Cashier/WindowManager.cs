//------------------------------------------------------------------------------
// Copyright � 2007-2009 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WindowManager.cs
// 
//   DESCRIPTION: Implements the WindowMAnager class, to manage forms appearance in window mode
//
//        AUTHOR: Rub�n Rodr�guez
// 
// CREATION DATE: 02-JUL-2013 
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 02-JUL-2013 RRR    Initial Version.
// 17-JUL-2014 DHA    Added table mode.
// 14-AUG-2014 DCS    Check null values in register reads
// 15-DIC-2015 YNM    PBI 7612:Visits/Reception Trinidad y Tobago: Add Cashier buttom
// 07-SEP-2016 DHA    Product Backlog Item 15912: resize for differents windows size
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

using Microsoft.Win32;
using WSI.Common;
using System.Runtime.InteropServices;

namespace WSI.Cashier
{
  public class WindowManager
  {
    #region Enums

    public enum StartUpModes
    {
      CashDesk = 0,
      Explorer = 1,
    }

    #endregion

    public static WindowManager.StartUpModes StartUpMode = WindowManager.GetStartUpMode();
    public static Boolean IsWindowMode = WindowManager.GetIsWindowMode();
    public static Boolean IsTableMode = WindowManager.GetIsTableMode();
    public static Boolean IsReceptionMode = WindowManager.GetIsReceptionMode();
    public static Size CashierWindowSize = WindowManager.GetCashierWindowSize();

    private static Form MainWindowForm;

    #region Public Functions

    public static int GetMarginWidth(Form Frm)
    {
      return (Frm.Width - Frm.ClientSize.Width) / 2;
    }

    public static Int32 GetTittleBarHeight(Form Frm)
    {
      return Frm.Height - Frm.ClientSize.Height - WindowManager.GetMarginWidth(Frm);
    }

    public static Size GetClientAreaSize(Form Frm)
    {
      if (Frm is frm_container)
      {
        MainWindowForm = (Form)Frm;
      }

      return new Size(Frm.Width - (WindowManager.GetMarginWidth(Frm) * 2), Frm.Height - WindowManager.GetMarginWidth(Frm) - WindowManager.GetTittleBarHeight(Frm));
    }

    public static Point GetClientAreaLocation(Form Frm)
    {
      if (Frm is frm_container)
      {
        MainWindowForm = (Form)Frm;
      }

      return new Point(Frm.Location.X + WindowManager.GetMarginWidth(Frm), Frm.Location.Y + WindowManager.GetTittleBarHeight(Frm));
    }

    public static Point GetFormCenterLocation(Form Frm)
    {
      int _x;
      int _y;

      _x = Frm.Location.X + (Frm.Width / 2);
      _y = Frm.Location.Y + (Frm.Height / 2);

      return new Point(_x, _y);
    }

    public static void AdjustYesNoFormToParent(Form ParentForm, ref frm_yesno FormYesNo)
    {
      FormYesNo.Size = WindowManager.GetClientAreaSize(ParentForm);
      FormYesNo.Location = WindowManager.GetClientAreaLocation(ParentForm);
    }

    public static void CenterInParentForm(Form ParentForm, Form Frm)
    {
      Point _parent_center;
      int _x;
      int _y;
      
      if ((ParentForm == null) || (Frm == null))
      {
        return;
      }

      //Get center parent form
      _parent_center = GetFormCenterLocation(ParentForm);

      //Center in parent
      _x = _parent_center.X - (Frm.Width / 2);
      _y = _parent_center.Y - (Frm.Height / 2);

      Frm.Location = new Point(_x, _y);
    }

    public static Point GetMainFormAreaLocation()
    {
      if (MainWindowForm != null)
      {
        return GetClientAreaLocation(MainWindowForm);
      }

      return new Point(0, 0);
    }

    public static Size GetMainFormAreaSize()
    {
      if (MainWindowForm != null)
      {
        return GetClientAreaSize(MainWindowForm);
      }

      return new Size(0, 0);
    }

    #endregion // Public Functions

    #region Private Functions

    private static Boolean GetIsWindowMode()
    {
      RegistryKey _cashier;
      RegistryKey _common;
      Boolean _is_window_mode;

      _cashier = null;
      _common = null;
      _is_window_mode = false;

      //WindowManager.StartUpMode = WindowManager.StartUpModes.CashDesk;
      if (WindowManager.StartUpMode == StartUpModes.CashDesk)
      {
        return false;
      }

      try
      {
        _cashier = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\LK_SYSTEM\CASHIER", false);
        if (_cashier != null)
        {
          _common = _cashier.OpenSubKey("Common", false);
        }

        if (_common == null)
        {
          _is_window_mode = false;
        }
        else
        {
          if (_common.GetValue("WindowMode") == null)
          {
            _is_window_mode = false;
          }
          else
          {
            Boolean.TryParse(_common.GetValue("WindowMode").ToString(), out _is_window_mode);
          }

        }
      }
      catch (Exception)
      {
        if (_common != null) _common.Close();
        if (_cashier != null) _cashier.Close();
        return false;
      }

      if (_common != null) _common.Close();
      if (_cashier != null) _cashier.Close();

      return _is_window_mode;
    }

    private static Size GetCashierWindowSize()
    {
      RegistryKey _cashier;
      RegistryKey _common;
      String _cashier_window_size;
      String[] _size;

      _cashier = null;
      _common = null;
      _cashier_window_size = "1024x768";

      try
      {
        _cashier = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\LK_SYSTEM\CASHIER", false);
        if (_cashier != null)
        {
          _common = _cashier.OpenSubKey("Common", false);
        }

        if (_common != null)
        {
          if (_common.GetValue("CashierWindow") != null)
          {
            _cashier_window_size = _common.GetValue("CashierWindow").ToString();
          }

        }
      }
      catch (Exception)
      {
        if (_common != null) _common.Close();
        if (_cashier != null) _cashier.Close();
      }

      if (_common != null) _common.Close();
      if (_cashier != null) _cashier.Close();

      _size = _cashier_window_size.Split('x');

      return new Size(Int32.Parse(_size[0]), Int32.Parse(_size[1]));
    }

    private static Boolean GetIsTableMode()
    {
      RegistryKey _cashier;
      RegistryKey _common;
      Boolean _is_table_mode;

      _cashier = null;
      _common = null;
      _is_table_mode = false;

      ////WindowManager.StartUpMode = WindowManager.StartUpModes.CashDesk;
      //if (WindowManager.StartUpMode == StartUpModes.CashDesk)
      //{
      //  return false;
      //}

      try
      {
        _cashier = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\LK_SYSTEM\CASHIER", false);
        if (_cashier != null)
        {
          _common = _cashier.OpenSubKey("Common", false);
        }

        if (_common == null)
        {
          _is_table_mode = false;
        }
        else
        {
          if (_common.GetValue("TableMode") == null)
          {
            _is_table_mode = false;
          }
          else
          {
            Boolean.TryParse(_common.GetValue("TableMode").ToString(), out _is_table_mode);
          }
        }
      }
      catch (Exception)
      {
        if (_common != null) _common.Close();
        if (_cashier != null) _cashier.Close();
        return false;
      }

      if (_common != null) _common.Close();
      if (_cashier != null) _cashier.Close();

      return _is_table_mode;
    }

    private static Boolean GetIsReceptionMode()
    {
      RegistryKey _cashier;
      RegistryKey _common;
      Boolean _is_reception_mode;

      _cashier = null;
      _common = null;
      _is_reception_mode = false;

      try
      {
        _cashier = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\LK_SYSTEM\CASHIER", false);
        if (_cashier != null)
        {
          _common = _cashier.OpenSubKey("Common", false);
        }

        if (_common == null)
        {
          _is_reception_mode = false;
        }
        else
        {
          if (_common.GetValue("ReceptionMode") == null)
          {
            _is_reception_mode = false;
          }
          else
          {
            Boolean.TryParse(_common.GetValue("ReceptionMode").ToString(), out _is_reception_mode);
          }
        }
      }
      catch (Exception)
      {
        if (_common != null) _common.Close();
        if (_cashier != null) _cashier.Close();
        return false;
      }

      if (_common != null) _common.Close();
      if (_cashier != null) _cashier.Close();

      return _is_reception_mode;
    }

    private static WindowManager.StartUpModes GetStartUpMode()
    {
      RegistryKey _policies;
      RegistryKey _system;
      String _startup_mode;
      WindowManager.StartUpModes _ret;

      _policies = null;
      _system = null;
      _startup_mode = String.Empty;

      _ret = StartUpModes.Explorer;

      try
      {
        _policies = Registry.CurrentUser.OpenSubKey(@"Software\Microsoft\Windows\CurrentVersion\Policies", false);
        if (_policies != null)
        {
          _system = _policies.OpenSubKey("System", false);
        }

        if (_system != null)
        {
          if (_system.GetValue("Shell") != null)
          {
            _startup_mode = _system.GetValue("Shell").ToString();
          }

          if (!String.IsNullOrEmpty(_startup_mode))
          {
            _ret = StartUpModes.CashDesk;
          }
        }
      }

      catch (Exception)
      {
        if (_policies != null) _policies.Close();
        if (_system != null) _system.Close();

        return StartUpModes.Explorer;
      }

      if (_policies != null) _policies.Close();
      if (_system != null) _system.Close();

      return _ret;
    }

    #endregion // Private Functions

  }

}

[StructLayout(LayoutKind.Sequential)]
public struct DEVMODE
{
  private const int CCHDEVICENAME = 0x20;
  private const int CCHFORMNAME = 0x20;
  [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 0x20)]
  public string dmDeviceName;
  public short dmSpecVersion;
  public short dmDriverVersion;
  public short dmSize;
  public short dmDriverExtra;
  public int dmFields;
  public int dmPositionX;
  public int dmPositionY;
  public int dmDisplayOrientation;
  public int dmDisplayFixedOutput;
  public short dmColor;
  public short dmDuplex;
  public short dmYResolution;
  public short dmTTOption;
  public short dmCollate;

  [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 0x20)]
  public string dmFormName;

  public short dmLogPixels;
  public int dmBitsPerPel;
  public int dmPelsWidth;
  public int dmPelsHeight;
  public int dmDisplayFlags;
  public int dmDisplayFrequency;
  public int dmICMMethod;
  public int dmICMIntent;
  public int dmMediaType;
  public int dmDitherType;
  public int dmReserved1;
  public int dmReserved2;
  public int dmPanningWidth;
  public int dmPanningHeight;
}
public class DisplaySettings
{
  const int MIN_RESOLUTION_WIDTH = 1024;
  const int MIN_RESOLUTION_HEIGTH = 768;

  [DllImport("user32.dll")]
  public static extern Boolean EnumDisplaySettings(string deviceName, int modeNum, ref DEVMODE devMode);

  [System.Runtime.InteropServices.DllImport("user32.dll")]
  public static extern int GetSystemMetrics(int nIndex);

  public static List<string> GetScreenResolutions()
  {
    List<string> _system_resolutions;
    List<string> _resolutions;
    Int32 i = 0;
    DEVMODE devMode;
    Size _sys_windows_resolution;
    Int32 _max_height;
    Int32 _max_width;

    _sys_windows_resolution = GetCurrentScreenResolution();

    _system_resolutions = new List<string>();
    _resolutions = new List<string>();
    devMode = new DEVMODE();

    _max_width = Math.Min(_sys_windows_resolution.Width, GeneralParam.GetInt32("Cashier", "MaxScreenResolutionWidth", 1280));
    _max_height = Math.Min(_sys_windows_resolution.Height, GeneralParam.GetInt32("Cashier", "MaxScreenResolutionHeigth", 800));

    try
    {
      while (EnumDisplaySettings(null, i, ref devMode))
      {
        if (MIN_RESOLUTION_WIDTH <= devMode.dmPelsWidth && MIN_RESOLUTION_HEIGTH <= devMode.dmPelsHeight &&
          _max_width >= devMode.dmPelsWidth && _max_height >= devMode.dmPelsHeight)
        {
          _system_resolutions.Add(string.Format("{0}x{1}", devMode.dmPelsWidth, devMode.dmPelsHeight));
        }
        i++;
      }

      if (_system_resolutions.Count == 0)
      {
        return _resolutions;
      }

      foreach (String _item in _system_resolutions)
      {
        if (!_resolutions.Contains(_item))
        {
          _resolutions.Add(_item);
        }
      }

      return _resolutions;
    }
    catch (Exception)
    {
      Log.Error("DisplaySettings: Could not get screen resolutions.");
    }

    return _resolutions;
  }

  public static Size GetCurrentScreenResolution()
  {
    Int32 _width = GetSystemMetrics(0x00);
    Int32 _height = GetSystemMetrics(0x01);

    return new Size(_width, _height);
  }

}
