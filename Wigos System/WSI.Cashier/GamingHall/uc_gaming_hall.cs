﻿//------------------------------------------------------------------------------
// Copyright © 2015-2020 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_gaming_hall.cs
//  
//   DESCRIPTION: Implements user control to Collect (Collect and refill Terminals)
//                User control: uc_gaming_hall
// 
//        AUTHOR: Francisco Vicente
// 
// CREATION DATE: 25-JAN-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 25-JAN-2016 FAV     First release.
// 11-MAR-2016 FAV     Fixed Bug 10063: Added permission for gaming hall
// 22-MAR-2016 ETP     Fixed Bug 10798: ReadOnly property set if we restart cashier.
// 07-APR-2016 ETP     Fixed Bug 11542: Meters apears when seeing collecting terminals. 
//------------------------------------------------------------------------------

#region Usings

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using WSI.Cashier.GamingHall;
using WSI.Common;
using WSI.Common.GamingHall;

#endregion // Usings

namespace WSI.Cashier
{
  public partial class uc_gaming_hall : UserControl
  {
    #region Constants
    private const Int32 GRID_COLUMN_WIDTH_ICON = 40;
    private const Int32 GRID_COLUMN_WIDTH_COLLECTION_DATE = 160;
    private const Int32 GRID_COLUMN_WIDTH_TERMINAL_NAME = 175;
    private const Int32 GRID_COLUMN_WIDTH_HOPPER = 125;
    private const Int32 GRID_COLUMN_WIDTH_DROPBOX = 125;
    private const Int32 GRID_COLUMN_WIDTH_LAST_UPDATED = 150;
    private const Int32 GRID_COLUMN_WIDTH_EDIT_METERS = 110;

    internal static class IndexGridColumnCollect
    {
      public const Int32 GRID_COLUMN_STATUS_ICON = 0;
      public const Int32 GRID_COLUMN_TERMINAL_ID = 1;
      public const Int32 GRID_COLUMN_COLLECTION_DATE = 2;
      public const Int32 GRID_COLUMN_TERMINAL_NAME = 3;
      public const Int32 GRID_COLUMN_HOPPER = 4;
      public const Int32 GRID_COLUMN_DROPBOX = 5;
      public const Int32 GRID_COLUMN_LAST_UPDATED = 6;
      public const Int32 GRID_COLUMN_OPEN_METERS_FORM = 7;
      public const Int32 GRID_COLUMN_STATUS = 9;

      /// <summary>
      /// Index of the GRID
      /// </summary>
      public const Int32 GRID_COLUMN_MONEY_COLLECTION_ID = 8;
    }

    #endregion

    #region Attributes
    private CollectionGridMode m_grid_mode;

    private List<GamingHallTerminalCollectRefill> m_terminals_proposed;
    private List<GamingHallTerminalCollectRefill> m_terminals_collected;

    private GamingHallTerminalCollectRefill m_terminal_rc_selected;

    private frm_waiting_message m_frm_waiting_message;

    private delegate void FormCallback(Boolean Result);
    private delegate void SimpleCallBack();
    #endregion

    #region Constructor
    //------------------------------------------------------------------------------
    // PURPOSE : Constructor 
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //  
    public uc_gaming_hall()
    {
      InitializeComponent();

      if (DesignMode || LicenseManager.UsageMode == LicenseUsageMode.Designtime)
      {
        return;
      }

      InitializeControls();

      InitCollectionDataGrid();
    }
    #endregion

    #region Public Methods

    public void RefreshStatus()
    {
      LoadTerminalFilters();
    }
    #endregion

    #region Private Methods

    /// <summary>
    /// Start a collect for all terminals selected
    /// </summary>
    private void CollectTerminal()
    {
      List<TerminalCollectRefill> _terminal_list = new List<TerminalCollectRefill>();
      List<TerminalCollectRefill> _terminal_list_error;
      TerminalCashierCollection _terminal_cashier_collection;
      VoucherTerminalsCollect _vouchers;
      bool _result = false;

      try
      {
        // start close pop-up window
        m_frm_waiting_message = new frm_waiting_message();
        m_frm_waiting_message.CloseByTime = false;
        StartWaitingForm();

        // build TerminalCollectRefill 
        foreach (GamingHallTerminalCollectRefill _terminal in m_terminals_proposed)
        {
          if (IsTerminalInModePending(_terminal))
          {
            _terminal_list.Add(_terminal.TerminalCollectRefill);
          }
        }

        // Call to TerminalsChangeStacker method
        _terminal_cashier_collection = new TerminalCashierCollection(Cashier.CashierSessionInfo());
        _result = _terminal_cashier_collection.TerminalsCollectAndRefill(ref _terminal_list, out _terminal_list_error, out _vouchers);

        if (_result)
        {
          VoucherPrint.Print(_vouchers);

          UpdateTerminalAfterCollect(_terminal_list);

          // Go to next step
          ChangeToStepCollected();
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
      finally
      {
        // close pop-up window
        CloseWaitingForm(_result);
      }
    }

    /// <summary>
    /// Change stacket for all terminals selected
    /// </summary>
    private void ChangeStacker()
    {
      List<TerminalCollectRefill> _terminal_list = new List<TerminalCollectRefill>();
      List<TerminalCollectRefill> _terminal_list_error;
      TerminalCashierCollection _terminal_cashier_collection;
      bool _result = false;

      try
      {
        // start close pop-up window
        m_frm_waiting_message = new frm_waiting_message();
        m_frm_waiting_message.CloseByTime = false;
        StartWaitingForm();

        // build TerminalCollectRefill 
        foreach (GamingHallTerminalCollectRefill _terminal in m_terminals_proposed)
        {
          if (IsTerminalInModeProposed(_terminal))
          {
            _terminal_list.Add(_terminal.TerminalCollectRefill);
          }
        }

        // Call to TerminalsChangeStacker method
        _terminal_cashier_collection = new TerminalCashierCollection(Cashier.CashierSessionInfo());
        _result = _terminal_cashier_collection.TerminalsChangeStacker(ref _terminal_list,  out _terminal_list_error);

        if (_result)
        {
          // Update status (and Icon) in the datagrid 
          UpdateTerminalAfterChangeStacker(_terminal_list);

          // Go to next step
          ChangeToStepPending();
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
      finally
      {
        // close pop-up window
        CloseWaitingForm(_result);
      }
    }

    /// <summary>
    /// Cancell the current collect selected
    /// </summary>
    private void CancelCollect()
    {
      bool _result = false;
      TerminalCashierCollection _terminal_cashier_collection;

      try
      {
        // start close pop-up window
        m_frm_waiting_message = new frm_waiting_message();
        m_frm_waiting_message.CloseByTime = false;
        StartWaitingForm();

        // Cancel the current collection selected
        _terminal_cashier_collection = new TerminalCashierCollection(Cashier.CashierSessionInfo());
        _result = _terminal_cashier_collection.TerminalCancelCollect (m_terminal_rc_selected.CashierSessionId,m_terminal_rc_selected.TerminalId, 
                                                                      m_terminal_rc_selected.MoneyCollectionId, m_terminal_rc_selected.DenominationsCollect);
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
      finally
      {
        // close pop-up window
        CloseWaitingForm(_result);
      }
    }

    /// <summary>
    /// Remove collected terminals from proposed list
    /// </summary>
    /// <param name="TerminalsList"></param>
    private void UpdateTerminalAfterCollect(List<TerminalCollectRefill> TerminalsList)
    {
      List<GamingHallTerminalCollectRefill> _terminals_no_collected = new List<GamingHallTerminalCollectRefill>();

      foreach (GamingHallTerminalCollectRefill _item in m_terminals_proposed)
      {
        TerminalCollectRefill _terminal = TerminalsList.Find(delegate(TerminalCollectRefill x) { return x.TerminalId == _item.TerminalId; });

        if (_terminal != null && _terminal.MoneyCollectionInfoCollect != null)
        {
          _item.TerminalCollectRefill = _terminal;

          if (_item.TerminalCollectRefill.MoneyCollectionInfoCollect.Status.Value != TITO_MONEY_COLLECTION_STATUS.CLOSED_BALANCED
              && _item.TerminalCollectRefill.MoneyCollectionInfoCollect.Status.Value != TITO_MONEY_COLLECTION_STATUS.CLOSED_IN_IMBALANCE)
          {
            _terminals_no_collected.Add(_item);
          }
        }
      }

      // Rebuild
      m_terminals_proposed.Clear();
      foreach (GamingHallTerminalCollectRefill _item in _terminals_no_collected)
      {
        m_terminals_proposed.Add(_item);
      }
    }

    private void UpdateTerminalAfterChangeStacker(List<TerminalCollectRefill> TerminalsList)
    {
      foreach (GamingHallTerminalCollectRefill _item in m_terminals_proposed)
      {
        TerminalCollectRefill _terminal = TerminalsList.Find(delegate(TerminalCollectRefill x) { return x.TerminalId == _item.TerminalId; });

        if (_terminal != null)
        {
          _item.TerminalCollectRefill = _terminal;

          if (_item.TerminalCollectRefill.IsStackerChangedOK)
            _item.StatusGrid = MCGridStatus.PENDING;
          else
            _item.StatusGrid = MCGridStatus.NOT_READED;
        }
      }
    }

    private void AddTerminalToProposedList()
    {
      if (uc_terminal_filter1.TerminalSelected() > 0)
      {
        // Checks if one terminal exist in the proposed list
        GamingHallTerminalCollectRefill _item = m_terminals_proposed.Find(delegate(GamingHallTerminalCollectRefill x) { return x.TerminalId == uc_terminal_filter1.TerminalSelected(); });
        if (_item != null)
        {
          return;
        }
      }

      // Find the terminals in the database (only opened are valid)
      List<GamingHallTerminalCollectRefill> _terminals_opened_list;
      using (DB_TRX _db_trx = new DB_TRX())
      {
        GamingHallCollections obj = new GamingHallCollections();
        obj.GetTerminalsToShowInGrid(CollectionGridMode.OPEN, uc_terminal_filter1.ProviderSelected(), uc_terminal_filter1.TerminalSelected(), out _terminals_opened_list, _db_trx.SqlTransaction);
      }

      foreach (GamingHallTerminalCollectRefill _terminal_opened in _terminals_opened_list)
      {
        GamingHallTerminalCollectRefill _item = m_terminals_proposed.Find(delegate(GamingHallTerminalCollectRefill x) { return x.TerminalId == _terminal_opened.TerminalId; });
        if (_item == null)
        {
          _terminal_opened.StatusGrid = MCGridStatus.OPEN; // Default value for a new Terminal added
          m_terminals_proposed.Add(_terminal_opened);
        }
      }

      LoadGridViewWithTerminalsByMode(m_terminals_proposed, CollectionGridMode.OPEN);
    }

    private void LoadTerminalsInGridByMode(CollectionGridMode Mode)
    {
      switch (Mode)
      {
        case CollectionGridMode.OPEN:
          LoadGridViewWithTerminalsByMode(m_terminals_proposed, Mode);
          break;

        case CollectionGridMode.PENDING:
          LoadTerminalFilters();

          List<GamingHallTerminalCollectRefill> _terminals_pending;
          using (DB_TRX _db_trx = new DB_TRX())
          {
            GamingHallCollections obj = new GamingHallCollections();
            obj.GetTerminalsToShowInGrid(CollectionGridMode.PENDING, "", 0, out _terminals_pending, _db_trx.SqlTransaction);
          }

          // Update terminals proposed (manually or by calendar)
          foreach (GamingHallTerminalCollectRefill _t_proposed in m_terminals_proposed)
          {
            GamingHallTerminalCollectRefill _item = _terminals_pending.Find(delegate(GamingHallTerminalCollectRefill x) { return x.TerminalId == _t_proposed.TerminalId; });
            if (_item != null)
            {
              _t_proposed.DateTime = _item.DateTime;
              _t_proposed.LastUpdated = _item.LastUpdated;
              _t_proposed.MoneyCollectionId = _item.MoneyCollectionId;
            }
          }

          // Adds terminals in status pending from the database that haven't added manually or by calendar
          foreach (GamingHallTerminalCollectRefill _t_pending in _terminals_pending ?? new List<GamingHallTerminalCollectRefill>())
          {
            GamingHallTerminalCollectRefill _item = m_terminals_proposed.Find(delegate(GamingHallTerminalCollectRefill x) { return x.TerminalId == _t_pending.TerminalId; });
            if (_item == null)
            {
              _t_pending.StatusGrid = MCGridStatus.PENDING;
              m_terminals_proposed.Add(_t_pending);
            }
          }

          SortTerminalsByDateTime(ref m_terminals_proposed);

          LoadGridViewWithTerminalsByMode(m_terminals_proposed, Mode);

          break;
        case CollectionGridMode.COLLECTED:

          LoadTerminalFilters();

          using (DB_TRX _db_trx = new DB_TRX())
          {
            GamingHallCollections obj = new GamingHallCollections();
            obj.GetTerminalsToShowInGrid(CollectionGridMode.COLLECTED, uc_terminal_filter1.ProviderSelected(), uc_terminal_filter1.TerminalSelected(), out m_terminals_collected, _db_trx.SqlTransaction);
          }

          foreach (GamingHallTerminalCollectRefill _t_collected in m_terminals_collected)
          {
            if (_t_collected.StatusMoneyCollectionCollected.Value == TITO_MONEY_COLLECTION_STATUS.CLOSED_BALANCED)
              _t_collected.StatusGrid = MCGridStatus.CLOSED_BALANCED;
            else if (_t_collected.StatusMoneyCollectionCollected.Value == TITO_MONEY_COLLECTION_STATUS.CLOSED_IN_IMBALANCE)
              _t_collected.StatusGrid = MCGridStatus.CLOSED_IN_IMBALANCE;
          }

          LoadGridViewWithTerminalsByMode(m_terminals_collected, Mode);

          break;
      }
      if (dgv_collections.ColumnCount < IndexGridColumnCollect.GRID_COLUMN_OPEN_METERS_FORM + 1)
          return;
      dgv_collections.Columns[IndexGridColumnCollect.GRID_COLUMN_OPEN_METERS_FORM].Width = m_grid_mode != CollectionGridMode.PENDING ? 0 : GRID_COLUMN_WIDTH_EDIT_METERS;
      dgv_collections.Columns[IndexGridColumnCollect.GRID_COLUMN_OPEN_METERS_FORM].Visible = m_grid_mode == CollectionGridMode.PENDING || m_grid_mode == CollectionGridMode.COLLECTED;

    }

    private void LoadGridViewWithTerminalsByMode(List<GamingHallTerminalCollectRefill> Items, CollectionGridMode Mode,
                                                 String Provider = "", Int32 TerminalId = 0)
    {
      String _total_refill = String.Empty;
      String _total_collect = String.Empty;

      dgv_collections.Rows.Clear();

      foreach (GamingHallTerminalCollectRefill _item in Items ?? new List<GamingHallTerminalCollectRefill>())
      {
        bool _add_record = false;

        switch (Mode)
        {
          case CollectionGridMode.OPEN:
            // New records added manually and records wiht error in a ChangeStacker. 
            _add_record = IsTerminalInModeProposed(_item);
            break;
          case CollectionGridMode.PENDING:
            // Terminals with changeStacker OK and KO
            _add_record = IsTerminalInModePending(_item);
            break;
          case CollectionGridMode.COLLECTED:
            // Only Terminals collected
            _add_record = _item.StatusGrid == MCGridStatus.CLOSED_BALANCED || _item.StatusGrid == MCGridStatus.CLOSED_IN_IMBALANCE;
            break;
        }

        if (!String.IsNullOrEmpty(Provider))
        {
          _add_record = (Provider == _item.ProviderName);
        }
        if (TerminalId > 0)
        {
          _add_record = (TerminalId == _item.TerminalId);
        }

        if (_add_record)
        {
          var index = dgv_collections.Rows.Add();

          if (_item.StatusGrid == MCGridStatus.NOT_READED)
          {
            dgv_collections.Rows[index].Cells[IndexGridColumnCollect.GRID_COLUMN_STATUS_ICON].Value = pb_error_icon.Image;
            dgv_collections.Rows[index].Cells[IndexGridColumnCollect.GRID_COLUMN_STATUS].Value = 0;
          }
          else if (_item.StatusGrid == MCGridStatus.PENDING)
          {
            dgv_collections.Rows[index].Cells[IndexGridColumnCollect.GRID_COLUMN_STATUS_ICON].Value = pb_ok_icon.Image;
            dgv_collections.Rows[index].Cells[IndexGridColumnCollect.GRID_COLUMN_STATUS].Value = 1;
          }
          else
          {
            dgv_collections.Rows[index].Cells[IndexGridColumnCollect.GRID_COLUMN_STATUS_ICON].Value = null;
            dgv_collections.Rows[index].Cells[IndexGridColumnCollect.GRID_COLUMN_STATUS].Value = 2;
          }
          dgv_collections.Rows[index].Cells[IndexGridColumnCollect.GRID_COLUMN_TERMINAL_ID].Value = _item.TerminalId;
          dgv_collections.Rows[index].Cells[IndexGridColumnCollect.GRID_COLUMN_COLLECTION_DATE].Value = _item.DateTime;
          dgv_collections.Rows[index].Cells[IndexGridColumnCollect.GRID_COLUMN_TERMINAL_NAME].Value = _item.TerminalName;
          dgv_collections.Rows[index].Cells[IndexGridColumnCollect.GRID_COLUMN_LAST_UPDATED].Value = _item.LastUpdated;
          dgv_collections.Rows[index].Cells[IndexGridColumnCollect.GRID_COLUMN_OPEN_METERS_FORM].Value = "";
          dgv_collections.Rows[index].Cells[IndexGridColumnCollect.GRID_COLUMN_MONEY_COLLECTION_ID].Value = _item.MoneyCollectionId; // Index in this DataView

          if (Mode == CollectionGridMode.PENDING || Mode == CollectionGridMode.COLLECTED)
          {
            _total_refill = (_item.DenominationsRefill == null
                              || _item.DenominationsRefill.DenominationsWithQuantity.Count == 0 ? "" : _item.DenominationsRefill.TotalCurrency.ToString());
            _total_collect = (_item.DenominationsCollect == null
                              || _item.DenominationsCollect.DenominationsWithQuantity.Count == 0 ? "" : _item.DenominationsCollect.TotalCurrency.ToString());
          }

          dgv_collections.Rows[index].Cells[IndexGridColumnCollect.GRID_COLUMN_HOPPER].Value = _total_refill;
          dgv_collections.Rows[index].Cells[IndexGridColumnCollect.GRID_COLUMN_DROPBOX].Value = _total_collect;
        }
      }

      dgv_collections_SelectionChanged(this, null);
    }

    /// <summary>
    /// Validate if Terminal(s) is collected 
    /// </summary>
    /// <returns></returns>
    private bool ValidateTerminalsCollected()
    {
      List<GamingHallTerminalCollectRefill> _terminals_collected;
      Int32 _t_is_already_collected = 0;
      String _msg;

      if (!IsExistsTerminalsByMode(CollectionGridMode.OPEN))
        return false;

      // find terminals collected 
      using (DB_TRX _db_trx = new DB_TRX())
      {
        GamingHallCollections obj = new GamingHallCollections();

        obj.GetTerminalsToShowInGrid(CollectionGridMode.COLLECTED, "", 0, out _terminals_collected, _db_trx.SqlTransaction);
      }

      // ask if should start the collect
      if (_terminals_collected != null && _terminals_collected.Count > 0)
      {

        // find if the terminal is already collected for this period (> Misc.TodayOpening())
        foreach (GamingHallTerminalCollectRefill _t_collect in _terminals_collected)
        {
          GamingHallTerminalCollectRefill _item = m_terminals_proposed.Find(delegate(GamingHallTerminalCollectRefill x) { return x.TerminalId == _t_collect.TerminalId; });
          if (_item != null)
          {
            _t_is_already_collected++;
          }
        }

        if (_t_is_already_collected > 0)
        {
          if (_t_is_already_collected == 1)
            _msg = Resource.String("STR_UC_GAMINGHALL_MSG_TERMINAL_COLLECTED_TODAY");
          else
            _msg = Resource.String("STR_UC_GAMINGHALL_MSG_TERMINALS_COLLECTED_TODAY");

          return (frm_message.Show(_msg, "", MessageBoxButtons.YesNo, MessageBoxIcon.Question, this.ParentForm, true) == DialogResult.OK);
        }
        else
        {
          return true;
        }
      }
      else
      {
        return true;
      }
    }

    /// <summary>
    /// Validate if the quantities in the hopper and dropbox is valid to start a collect for each terminal in the grid
    /// </summary>
    /// <returns></returns>
    private bool ValidateHopperDropboxReadyToCollect()
    {
      String _msg = String.Empty;

      if (!IsExistsTerminalsByMode(CollectionGridMode.PENDING))
        return false;

      foreach (GamingHallTerminalCollectRefill _terminal in m_terminals_proposed)
      {
        if (IsTerminalInModePending(_terminal))
        {
          if (_terminal.DenominationsRefill == null || _terminal.DenominationsRefill.DenominationsWithQuantity.Count == 0)
          {
            if (_terminal.DenominationsCollect == null || _terminal.DenominationsCollect.DenominationsWithQuantity.Count == 0)
            {
              _msg = Resource.String("STR_UC_GAMINGHALL_MSG_WARNING_COLLECTING_2");
              break;
            }
          }
          else
          {
            if (_terminal.DenominationsCollect == null || _terminal.DenominationsCollect.DenominationsWithQuantity.Count == 0)
            {
              _msg = Resource.String("STR_UC_GAMINGHALL_MSG_WARNING_COLLECTING");
              break;
            }
          }
          if (String.IsNullOrEmpty(_terminal.EditableMeters))
          {
            _msg += Resource.String("STR_UC_GAMINGHALL_METERS_NEEDED", _terminal.TerminalName);
            break;
          }
        }
      }

      if (String.IsNullOrEmpty(_msg))
      {
        return true;
      }

      frm_message.Show(_msg, Resource.String("STR_FRM_CONTAINER_SEVERITY_WARNING"), MessageBoxButtons.OK, MessageBoxIcon.Error, this.ParentForm, true);
      return false;
    }

    private void InitializeControls()
    {
      this.lbl_termminal_title.Text = Resource.String("STR_UC_TERMINAL_COLLECT");
      this.gb_ViewMode.HeaderText = Resource.String("STR_UC_GAMINGHALL_VIEWMODE");//"Vista";
      this.gb_Collections.HeaderText = Resource.String("STR_UC_GAMINGHALL_COLLECTIONS"); // "Recaudaciones";
      this.gb_teminals.HeaderText = Resource.String("STR_UC_GAMINGHALL_TERMINALS"); // "Máquinas";
      this.rb_collection_prop.Text = Resource.String("STR_UC_GAMINGHALL_PROPOSED"); // "Propuestas";
      this.rb_pending_collect.Text = Resource.String("STR_UC_GAMINGHALL_PENDING"); // "Pendientes";
      this.rb_collected.Text = Resource.String("STR_UC_GAMINGHALL_COLLECTED");// "Recaudadas";
      this.btn_collections_collect.Text = Resource.String("STR_UC_GAMINGHALL_COLLECT");// "Recaudar";
      this.btn_delete_terminal.Text = Resource.String("STR_UC_GAMINGHALL_DELETE_ROW");// "Borrar";
      this.btn_add_terminal.Text = Resource.String("STR_UC_GAMINGHALL_ADD_ROW");// "Añadir";
      this.lbl_Totals.Text = Resource.String("STR_UC_GAMINGHALL_GRID_TOTALS_LBL");//Totales 
      this.lbl_legend.Text = Resource.String("STR_UC_GAMINGHALL_LEGEND");//Leyenda
      this.lbl_error_icon.Text = Resource.String("STR_UC_GAMINGHALL_COLLECTED_ERROR");
      this.lbl_ok_icon.Text = Resource.String("STR_UC_GAMINGHALL_COLLECTED_OK");

      this.pb_error_icon.Image = WSI.Cashier.Images.Get(Images.CashierImage.GH_Error);
      this.pb_ok_icon.Image = WSI.Cashier.Images.Get(Images.CashierImage.GT_Ok);
      this.lbl_Total_Hopper.Text = Currency.Zero().ToString();
      this.lbl_Total_Box.Text = Currency.Zero().ToString();

      this.rb_collection_prop.CheckedChanged += radio_button_selected_CheckedChanged;
      this.rb_pending_collect.CheckedChanged += radio_button_selected_CheckedChanged;
      this.rb_collected.CheckedChanged += radio_button_selected_CheckedChanged;

      this.rb_collection_prop.Checked = true;

      m_terminals_proposed = new List<GamingHallTerminalCollectRefill>();

    }

    private void UpdateTotals()
    {
      Decimal _total_hopper = 0;
      Decimal _total_dropbox = 0;

      switch (m_grid_mode)
      {
        case CollectionGridMode.PENDING:
          foreach (GamingHallTerminalCollectRefill _terminal in m_terminals_proposed)
          {
            if (IsTerminalInModePending(_terminal))
            {
              _total_hopper +=  _terminal.DenominationsRefill == null ? 0 : _terminal.DenominationsRefill.Total;
              _total_dropbox += _terminal.DenominationsCollect == null ? 0 : _terminal.DenominationsCollect.Total;
            }
          }
          break;

        case CollectionGridMode.COLLECTED:
          foreach (GamingHallTerminalCollectRefill _item in m_terminals_collected)
          {
            _total_hopper += _item.DenominationsRefill == null ? 0 : _item.DenominationsRefill.Total;
            _total_dropbox += _item.DenominationsCollect == null ? 0 : _item.DenominationsCollect.Total;
          }
          break;
      }

      lbl_Total_Hopper.Text = ((Currency)_total_hopper).ToString();
      lbl_Total_Box.Text = ((Currency)_total_dropbox).ToString();
    }

    /// <summary>
    /// Hide or show buttons, it changes the text in the button
    /// </summary>
    /// <param name="Mode"></param>
    private void ChangeEnvironmentByMode(CollectionGridMode Mode)
    {
      btn_delete_terminal.Visible = (Mode != CollectionGridMode.COLLECTED);
      btn_collections_collect.Visible = true;

      switch (Mode)
      {
        case CollectionGridMode.OPEN:
          lbl_Total_Hopper.Hide();
          lbl_Total_Box.Hide();
          lbl_Totals.Hide();

          this.btn_add_terminal.Text = Resource.String("STR_UC_GAMINGHALL_ADD_ROW");//Añadir
          this.btn_collections_collect.Text = Resource.String("STR_UC_GAMINGHALL_COLLECT");//"Recaudar";
          this.btn_delete_terminal.Text = Resource.String("STR_UC_GAMINGHALL_DELETE_ROW");//"Borrar";
          break;
        case CollectionGridMode.PENDING:
          lbl_Total_Hopper.Show();
          lbl_Total_Box.Show();
          lbl_Totals.Show();

          this.btn_add_terminal.Text = Resource.String("STR_UC_GAMINGHALL_FIND_ROW");//"Buscar";
          this.btn_collections_collect.Text = Resource.String("STR_UC_GAMINGHALL_SAVE_COLLECTIONS");// "Guardar Recaudación";
          this.btn_delete_terminal.Text = Resource.String("STR_UC_GAMINGHALL_CLEAN_ROW");// "Limpiar Datos";
          break;
        case CollectionGridMode.COLLECTED:
          lbl_Total_Hopper.Show();
          lbl_Total_Box.Show();
          lbl_Totals.Show();

          this.btn_add_terminal.Text = Resource.String("STR_UC_GAMINGHALL_FIND_ROW");//"Buscar";
          this.btn_collections_collect.Text = Resource.String("STR_UC_GAMINGHALL_CANCEL_COLLECT");// "Anular Recaudación";

          // TODO: Remove in the next version (fase 3) (Bug 9171)
          btn_collections_collect.Visible = false;

          this.btn_delete_terminal.Text = string.Empty;
          break;
      }
    }

    /// <summary>
    /// Shows Window to enter denominations in Hopper or Dropbox
    /// </summary>
    /// <param name="Box"></param>
    private void WindowToEnterDenominations(TerminalBoxType Box)
    {
      frm_gh_currency_quantity _form;
      TerminalDenominations _denominations = null;
      String _iso_code;

      _iso_code = CurrencyExchange.GetNationalCurrency();

      switch (Box)
      {
        case TerminalBoxType.Hopper:
          if (m_terminal_rc_selected.DenominationsRefill == null)
            _denominations = new TerminalDenominations(TerminalBoxType.Hopper, _iso_code, CurrencyExchangeType.CURRENCY);
          else
            _denominations = m_terminal_rc_selected.DenominationsRefill;
          break;
        case TerminalBoxType.Box:
          if (m_terminal_rc_selected.DenominationsCollect == null)
            _denominations = new TerminalDenominations(TerminalBoxType.Box, _iso_code, CurrencyExchangeType.CURRENCY);
          else
            _denominations = m_terminal_rc_selected.DenominationsCollect;
          break;
      }

      _form = new frm_gh_currency_quantity();
      _form.Show(this.ParentForm, ref _denominations, m_terminal_rc_selected.ProviderName, m_terminal_rc_selected.TerminalName);
      _form.Dispose();

      switch (Box)
      {
        case TerminalBoxType.Hopper:
          m_terminal_rc_selected.DenominationsRefill = _denominations;
          break;
        case TerminalBoxType.Box:
          m_terminal_rc_selected.DenominationsCollect = _denominations;
          break;
      }
    }

    /// <summary>
    /// Shows Window to shows denominations in Hopper or DropBox
    /// </summary>
    /// <param name="Box"></param>
    private void WindowToShowCurrentDenominations(TerminalBoxType Box)
    {
      frm_gh_currency_quantity _form;
      TerminalDenominations _denominations = null;

      switch (Box)
      {
        case TerminalBoxType.Hopper:
          _denominations = m_terminal_rc_selected.DenominationsRefill;
          break;
        case TerminalBoxType.Box:
          _denominations = m_terminal_rc_selected.DenominationsCollect;
          break;
      }

      _form = new frm_gh_currency_quantity(frm_gh_currency_quantity.EntryMode.READONLY);
    //  _form.ReadOnly = true;
      _form.Show(this.ParentForm, ref _denominations, m_terminal_rc_selected.ProviderName, m_terminal_rc_selected.TerminalName);
      _form.Dispose();
    }

    private void LoadTerminalFilters()
    {
      StringBuilder _where;
      _where = new StringBuilder();

      _where.AppendLine("    WHERE   TE_ACTIVE = 1                       ");
      _where.AppendLine("      AND   TE_STATUS = " + (int)TerminalStatus.ACTIVE);
      _where.AppendLine("      AND   TE_TERMINAL_TYPE = " + (Int32)TerminalTypes.SAS_HOST);

      uc_terminal_filter1.InitControls("", _where.ToString(), false);
    }

    private void ChangeToStepPending()
    {
      if (this.ParentForm.InvokeRequired)
      {
        SimpleCallBack d = new SimpleCallBack(ChangeToStepPending);
        this.Invoke(d);
      }
      else
      {
        rb_pending_collect.Checked = true;
      }
    }

    private void ChangeToStepCollected()
    {
      if (this.ParentForm.InvokeRequired)
      {
        SimpleCallBack d = new SimpleCallBack(ChangeToStepCollected);
        this.Invoke(d);
      }
      else
      {
        rb_collected.Checked = true;
      }
    }

    private void ClearTotolDropboxAndHopper()
    {
      if (m_grid_mode == CollectionGridMode.PENDING
          && IsExistsTerminalsByMode(CollectionGridMode.PENDING)
          && frm_message.Show(Resource.String("STR_UC_GAMINGHALL_MSG_CLEAN_ROW"), String.Empty, MessageBoxButtons.YesNo, MessageBoxIcon.Question, this.ParentForm, true) == DialogResult.OK)
      {
        foreach (GamingHallTerminalCollectRefill _terminal in m_terminals_proposed)
        {
          _terminal.DenominationsRefill = null;
          _terminal.DenominationsCollect = null;
        }

        LoadGridViewWithTerminalsByMode(m_terminals_proposed, m_grid_mode);

        UpdateTotals();
      }
    }

    private bool IsTerminalInModeProposed(GamingHallTerminalCollectRefill Terminal)
    { 
      return (Terminal.StatusGrid == MCGridStatus.OPEN || Terminal.StatusGrid == MCGridStatus.NOT_READED);
    }

    private bool IsTerminalInModePending(GamingHallTerminalCollectRefill Terminal)
    {
      return (Terminal.StatusGrid == MCGridStatus.NOT_READED || Terminal.StatusGrid == MCGridStatus.PENDING);
    }

    /// <summary>
    /// Checks if exists elements in the proposed terminals for a CollectionGridMode type
    /// </summary>
    /// <returns></returns>
    private bool IsExistsTerminalsByMode(CollectionGridMode Mode)
    {
      switch (Mode)
      {
        case CollectionGridMode.OPEN:
          foreach (GamingHallTerminalCollectRefill _terminal in m_terminals_proposed)
          {
            if (IsTerminalInModeProposed(_terminal))
              return true;
          }
          break;
        case CollectionGridMode.PENDING:
          foreach (GamingHallTerminalCollectRefill _terminal in m_terminals_proposed)
          {
            if (IsTerminalInModePending(_terminal))
              return true;
          }
          break;
      }

      return false;
    }

    /// <summary>
    /// Sort a List of GamingHallTerminalCollectRefill by the property DateTime
    /// </summary>
    /// <param name="Terminals"></param>
    private void SortTerminalsByDateTime(ref List<GamingHallTerminalCollectRefill> Terminals)
    {
      Terminals.Sort(delegate(GamingHallTerminalCollectRefill p1, GamingHallTerminalCollectRefill p2)
      {
        if (p1.DateTime == null)
        {
          return (p2.DateTime == null) ? 0 : -1;
        }
        if (p2.DateTime == null)
        {
          return 1;
        }
        int r = p1.DateTime.Value.CompareTo(p2.DateTime.Value);
        return r;
      });
    }

    private bool IsCollectSelectedValidToCancel()
    {
      Int32 _undo_allowed_minutes;
      MoneyCollectionInfo _current_money_collection;
      _undo_allowed_minutes = GeneralParam.GetInt32("Cashier.Undo", "Allowed.Minutes", 60);

      using (DB_TRX _db_trx = new DB_TRX())
      {
        MoneyCollection.GetMoneyCollection(m_terminal_rc_selected.MoneyCollectionId, out _current_money_collection, _db_trx.SqlTransaction);
      }
    
       // Check if the status hasn't changed
      if (_current_money_collection.Status.Value != m_terminal_rc_selected.StatusMoneyCollectionCollected)
      {
        return false;
      }

      // Checks the limit time configured to make an Undo 
      if (DateTime.Compare(m_terminal_rc_selected.DateTime.Value.Add(new TimeSpan(0, _undo_allowed_minutes, 0)), WGDB.Now) < 0)
      {
        return false;
      }

      return true;
    }

    #region Waiting Form

    private void StartWaitingForm()
    {
      Thread _thread = new Thread(ShowWaitingFormByMode);
      _thread.Name = "Thread WaitingForm";
      _thread.Start();
    }

    private void ShowWaitingFormByMode()
    {
      try
      {
        if (this.ParentForm.InvokeRequired)
        {
          SimpleCallBack d = new SimpleCallBack(ShowWaitingFormByMode);
          this.Invoke(d);
        }
        else
        {
          switch (m_grid_mode)
          {
            case CollectionGridMode.OPEN:
              m_frm_waiting_message.Show(this.ParentForm, Resource.String("STR_UC_GAMINGHALL_MSG_READING_METERS"), Resource.String("STR_UC_GAMINGHALL_MSG_READING_METERS"), 0);
              break;
            case CollectionGridMode.PENDING:
              m_frm_waiting_message.Show(this.ParentForm, Resource.String("STR_UC_GAMINGHALL_MSG_SAVING_COLLECTIONS"), Resource.String("STR_UC_GAMINGHALL_MSG_SAVING_COLLECTIONS"), 0);

              break;
          }
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
    }

    private void CloseWaitingForm(Boolean Result = false)
    {
      try
      {
        if (this.ParentForm.InvokeRequired)
        {
          FormCallback d = new FormCallback(CloseWaitingForm);
          this.Invoke(d, new object[] { Result });
        }
        else
        {
          m_frm_waiting_message.CloseAll();

          if (!Result)
          {
            switch (m_grid_mode)
            {
              case CollectionGridMode.OPEN:
                frm_message.Show(Resource.String("STR_ERROR_CHANGE_STACKER"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this.ParentForm);
                break;
              case CollectionGridMode.PENDING:
                frm_message.Show(Resource.String("STR_ERROR_COLLECTING"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this.ParentForm);
                break;
              case CollectionGridMode.COLLECTED:
                frm_message.Show(Resource.String("STR_ERROR_CANCELLING_COLLECT"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this.ParentForm);
                break;
            }
          }
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
    }

    #endregion

    #region Grid

    private void InitCollectionDataGrid()
    {
      dgv_collections.ColumnHeadersHeight = 30;
      dgv_collections.RowTemplate.Height = 30;
      dgv_collections.RowTemplate.DividerHeight = 0;

      dgv_collections.Columns.Add(new DataGridViewImageColumn());
      dgv_collections.Columns.Add(new DataGridViewTextBoxColumn());
      dgv_collections.Columns.Add(new DataGridViewTextBoxColumn());
      dgv_collections.Columns.Add(new DataGridViewTextBoxColumn());
      dgv_collections.Columns.Add(new DataGridViewTextBoxColumn());
      dgv_collections.Columns.Add(new DataGridViewTextBoxColumn());
      dgv_collections.Columns.Add(new DataGridViewTextBoxColumn());
      dgv_collections.Columns.Add(new DataGridViewButtonColumn
      {
        Visible = true,
        Name = "SetMetersColumn",
        Text = "Meters",
        HeaderText = "",
        AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader,
        UseColumnTextForButtonValue = true,
        DefaultCellStyle = new DataGridViewCellStyle
        {
          Padding = new Padding(2)
        }
      });

      dgv_collections.Columns.Add(new DataGridViewTextBoxColumn());
      dgv_collections.Columns.Add(new DataGridViewTextBoxColumn());

      //Status (Image)
      dgv_collections.Columns[IndexGridColumnCollect.GRID_COLUMN_STATUS_ICON].Width = GRID_COLUMN_WIDTH_ICON;
      dgv_collections.Columns[IndexGridColumnCollect.GRID_COLUMN_STATUS_ICON].HeaderCell.Value = "Status";
      dgv_collections.Columns[IndexGridColumnCollect.GRID_COLUMN_STATUS_ICON].HeaderText = "";
      dgv_collections.Columns[IndexGridColumnCollect.GRID_COLUMN_STATUS_ICON].ValueType = typeof(Bitmap);
      dgv_collections.Columns[IndexGridColumnCollect.GRID_COLUMN_STATUS_ICON].DefaultCellStyle.NullValue = null;

      // Terminal ID
      dgv_collections.Columns[IndexGridColumnCollect.GRID_COLUMN_TERMINAL_ID].Width = 0;
      dgv_collections.Columns[IndexGridColumnCollect.GRID_COLUMN_TERMINAL_ID].Visible = false;

      //MC Date      
      dgv_collections.Columns[IndexGridColumnCollect.GRID_COLUMN_COLLECTION_DATE].Width = GRID_COLUMN_WIDTH_COLLECTION_DATE;
      dgv_collections.Columns[IndexGridColumnCollect.GRID_COLUMN_COLLECTION_DATE].HeaderCell.Value = Resource.String("STR_UC_GAMINGHALL_GRID_DATE_CLEARANCE");
      dgv_collections.Columns[IndexGridColumnCollect.GRID_COLUMN_COLLECTION_DATE].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
      dgv_collections.Columns[IndexGridColumnCollect.GRID_COLUMN_COLLECTION_DATE].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

      // Terminal name
      dgv_collections.Columns[IndexGridColumnCollect.GRID_COLUMN_TERMINAL_NAME].Width = GRID_COLUMN_WIDTH_TERMINAL_NAME;
      dgv_collections.Columns[IndexGridColumnCollect.GRID_COLUMN_TERMINAL_NAME].HeaderCell.Value = Resource.String("STR_UC_GAMINGHALL_GRID_TERMINAL");
      dgv_collections.Columns[IndexGridColumnCollect.GRID_COLUMN_TERMINAL_NAME].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
      dgv_collections.Columns[IndexGridColumnCollect.GRID_COLUMN_TERMINAL_NAME].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

      // Hopper
      dgv_collections.Columns[IndexGridColumnCollect.GRID_COLUMN_HOPPER].Width = GRID_COLUMN_WIDTH_HOPPER;
      dgv_collections.Columns[IndexGridColumnCollect.GRID_COLUMN_HOPPER].HeaderCell.Value = Resource.String("STR_UC_GAMINGHALL_GRID_HOPPER");
      dgv_collections.Columns[IndexGridColumnCollect.GRID_COLUMN_HOPPER].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
      dgv_collections.Columns[IndexGridColumnCollect.GRID_COLUMN_HOPPER].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

      // Dropbox      
      dgv_collections.Columns[IndexGridColumnCollect.GRID_COLUMN_DROPBOX].Width = GRID_COLUMN_WIDTH_DROPBOX;
      dgv_collections.Columns[IndexGridColumnCollect.GRID_COLUMN_DROPBOX].HeaderCell.Value = Resource.String("STR_UC_GAMINGHALL_GRID_DROPBOX");
      dgv_collections.Columns[IndexGridColumnCollect.GRID_COLUMN_DROPBOX].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
      dgv_collections.Columns[IndexGridColumnCollect.GRID_COLUMN_DROPBOX].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

      // Last Updated
      dgv_collections.Columns[IndexGridColumnCollect.GRID_COLUMN_LAST_UPDATED].Width = GRID_COLUMN_WIDTH_LAST_UPDATED;
      dgv_collections.Columns[IndexGridColumnCollect.GRID_COLUMN_LAST_UPDATED].AutoSizeMode  = DataGridViewAutoSizeColumnMode.Fill;
      dgv_collections.Columns[IndexGridColumnCollect.GRID_COLUMN_LAST_UPDATED].HeaderCell.Value = Resource.String("STR_UC_GAMINGHALL_GRID_LAST_UPDATED");
      dgv_collections.Columns[IndexGridColumnCollect.GRID_COLUMN_LAST_UPDATED].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
      dgv_collections.Columns[IndexGridColumnCollect.GRID_COLUMN_LAST_UPDATED].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

      // Edit Meters
      dgv_collections.Columns[IndexGridColumnCollect.GRID_COLUMN_OPEN_METERS_FORM].Width = 0;
      dgv_collections.Columns[IndexGridColumnCollect.GRID_COLUMN_OPEN_METERS_FORM].Visible = false;

      // Money Collection ID
      dgv_collections.Columns[IndexGridColumnCollect.GRID_COLUMN_MONEY_COLLECTION_ID].Width = 0;
      dgv_collections.Columns[IndexGridColumnCollect.GRID_COLUMN_MONEY_COLLECTION_ID].Visible = false;

      // Status value
      dgv_collections.Columns[IndexGridColumnCollect.GRID_COLUMN_STATUS].Width = 0;
      dgv_collections.Columns[IndexGridColumnCollect.GRID_COLUMN_STATUS].Visible = false;
      dgv_collections.Columns[IndexGridColumnCollect.GRID_COLUMN_STATUS].ValueType = typeof(Int32);

    }

    private void DeleteRowDataGrid()
    {
      bool _is_proposed;

      if (m_terminal_rc_selected != null)
      {
        _is_proposed = false; // TODO: This values is used for added by user or added from a Calendar

        if (!_is_proposed)
        {
          if ((frm_message.Show(Resource.String("STR_UC_GAMINGHALL_MSG_DELETE"), String.Empty, MessageBoxButtons.YesNo, MessageBoxIcon.Question, this.ParentForm, true) == DialogResult.OK))
          {
            m_terminals_proposed.Remove(m_terminal_rc_selected);

            LoadGridViewWithTerminalsByMode(m_terminals_proposed, m_grid_mode);
          }
        }
        else
        {
          frm_message.Show(Resource.String("STR_UC_GAMINGHALL_MSG_ERROR_DELETE"), String.Empty, MessageBoxButtons.OK, MessageBoxIcon.Exclamation, this.ParentForm, true);
        }
      }
    }

    #endregion

    #endregion

    #region Events

    //------------------------------------------------------------------------------
    // PURPOSE : Sets m_grid_mode depending the radio selected
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void radio_button_selected_CheckedChanged(object sender, EventArgs e)
    {
      RadioButton rb = sender as RadioButton;
      if (rb != null && rb.Checked)
      {
        try
        {
          this.Cursor = Cursors.WaitCursor;

          // Set the MODE for the current operation
          if (rb_collection_prop.Checked)
          {
            m_grid_mode = CollectionGridMode.OPEN;
          }
          else if (rb_pending_collect.Checked)
          {
            m_grid_mode = CollectionGridMode.PENDING;
          }
          else if (rb_collected.Checked)
          {
            m_grid_mode = CollectionGridMode.COLLECTED;
          }

          ChangeEnvironmentByMode(m_grid_mode);

          LoadTerminalsInGridByMode(m_grid_mode);

          UpdateTotals();
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }
        finally
        {
          this.Cursor = Cursors.Default;
        }
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Add Terminal (Open mode) / Search (Pending and Collected mode)
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void btn_add_terminal_Click(object sender, EventArgs e)
    {
      try
      {
        this.Cursor = Cursors.WaitCursor;
        switch (m_grid_mode)
        {
          case CollectionGridMode.OPEN:
            AddTerminalToProposedList();
            break;

          case CollectionGridMode.PENDING:
            // Filter
            LoadGridViewWithTerminalsByMode(m_terminals_proposed, CollectionGridMode.PENDING, uc_terminal_filter1.ProviderSelected(), uc_terminal_filter1.TerminalSelected());
            break;

          case CollectionGridMode.COLLECTED:
            // Filter
            LoadGridViewWithTerminalsByMode(m_terminals_collected, CollectionGridMode.COLLECTED, uc_terminal_filter1.ProviderSelected(), uc_terminal_filter1.TerminalSelected());
            break;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {
        this.Cursor = Cursors.Default;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Delete selected row (Open Mode) / Clear total entered in Hopper and DropBox (Pending Mode)
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void btn_delete_terminal_Click(object sender, EventArgs e)
    {
      try
      {
        switch (m_grid_mode)
        {
          case CollectionGridMode.OPEN:
            DeleteRowDataGrid();
            break;

          case CollectionGridMode.PENDING:
            ClearTotolDropboxAndHopper();
            break;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Change stacker (Open Mode) / Collect (Pending Mode)
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void btn_collections_collect_Click(object sender, EventArgs e)
    {
      Thread _thread;

      switch (m_grid_mode)
      {
        case CollectionGridMode.OPEN:
          if (ValidateTerminalsCollected())
          {
            _thread = new Thread(ChangeStacker);
            _thread.Name = "Thread ChangeStacker";
            _thread.Start();
          }
          break;
        case CollectionGridMode.PENDING:
          if (ValidateHopperDropboxReadyToCollect())
          {
            _thread = new Thread(CollectTerminal);
            _thread.Name = "Thread CollectTerminal";
            _thread.Start();
          }
          break;
        case CollectionGridMode.COLLECTED:
          if (IsCollectSelectedValidToCancel())
          {
            if (frm_message.Show(Resource.String("STR_UC_GAMINGHALL_MSG_WARNING_CANCELL_COLLECT"), "", MessageBoxButtons.YesNo, MessageBoxIcon.Question, this.ParentForm, true) == DialogResult.OK)
            {
              _thread = new Thread(CancelCollect);
              _thread.Name = "Thread CancelCollect";
              _thread.Start();
            }
          }
          else
          {
            frm_message.Show(Resource.String("STR_UC_GAMINGHALL_MSG_CANCELL_NO_REVERSIBLE"), Resource.String("STR_APP_GEN_MSG_WARNING"),
                             MessageBoxButtons.OK, MessageBoxIcon.Warning, this.ParentForm);
          }
          break;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Shows the respetive Window (for enter values or show values) according the column and mode selected
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void dgv_collections_CellClick(object sender, DataGridViewCellEventArgs e)
    {
      try
      {
        if (m_terminal_rc_selected.TerminalId > 0)
        {
          switch (e.ColumnIndex)
          {
            case IndexGridColumnCollect.GRID_COLUMN_HOPPER:
              if (m_grid_mode == CollectionGridMode.PENDING)
              {
                WindowToEnterDenominations(TerminalBoxType.Hopper);

                // Update total refilled
                if (m_terminal_rc_selected.DenominationsRefill.DenominationsWithQuantity.Count == 0)
                  dgv_collections.Rows[e.RowIndex].Cells[IndexGridColumnCollect.GRID_COLUMN_HOPPER].Value = "";
                else
                  dgv_collections.Rows[e.RowIndex].Cells[IndexGridColumnCollect.GRID_COLUMN_HOPPER].Value = m_terminal_rc_selected.DenominationsRefill.TotalCurrency.ToString();

                UpdateTotals();
              }
              else if (m_grid_mode == CollectionGridMode.COLLECTED)
              {
                WindowToShowCurrentDenominations(TerminalBoxType.Hopper);
              }
              break;
            case IndexGridColumnCollect.GRID_COLUMN_DROPBOX:

              if (m_grid_mode == CollectionGridMode.PENDING)
              {
                WindowToEnterDenominations(TerminalBoxType.Box);

                // Update total collected
                if (m_terminal_rc_selected.DenominationsCollect.DenominationsWithQuantity.Count == 0)
                  dgv_collections.Rows[e.RowIndex].Cells[IndexGridColumnCollect.GRID_COLUMN_DROPBOX].Value = "";
                else
                  dgv_collections.Rows[e.RowIndex].Cells[IndexGridColumnCollect.GRID_COLUMN_DROPBOX].Value = m_terminal_rc_selected.DenominationsCollect.TotalCurrency.ToString();

                UpdateTotals();
              }
              else if (m_grid_mode == CollectionGridMode.COLLECTED)
              {
                WindowToShowCurrentDenominations(TerminalBoxType.Box);
              }
              break;
            case IndexGridColumnCollect.GRID_COLUMN_OPEN_METERS_FORM:
              String error_str;

              if (!ProfilePermissions.CheckPermissions(ProfilePermissions.CashierFormFuncionality.GAMINGHALL_ReadMeters,
                                        ProfilePermissions.TypeOperation.RequestPasswd,
                                        this.ParentForm,
                                        out error_str))
              {
                return;
              }

              Int32 _status;

              Int32.TryParse(dgv_collections.Rows[e.RowIndex].Cells[IndexGridColumnCollect.GRID_COLUMN_STATUS].Value.ToString(), out _status);

              frm_gh_set_meters _set_meters = new frm_gh_set_meters();
              _set_meters.Show(this, m_terminal_rc_selected.TerminalId,
                  m_terminal_rc_selected.EditableMeters, _status == 1);

              if (!String.IsNullOrEmpty(_set_meters.Meters))
                m_terminal_rc_selected.NewMeters = _set_meters.Meters;

              _set_meters.Dispose();
              break;
          }
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Take the current object selected in the Grid (GamingHallTerminalCollectRefill objet)
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void dgv_collections_SelectionChanged(object sender, EventArgs e)
    {
      try
      {
        if (dgv_collections.SelectedRows.Count > 0)
        {
          GamingHallTerminalCollectRefill _terminal = null;

          int _index = dgv_collections.SelectedRows[0].Index;
          int _money_collection_id;

          if (dgv_collections.Rows[_index].Cells[IndexGridColumnCollect.GRID_COLUMN_MONEY_COLLECTION_ID].Value == null)
            return;

          switch (m_grid_mode)
          {
            case CollectionGridMode.OPEN:
            case CollectionGridMode.PENDING:
              _money_collection_id = int.Parse(dgv_collections.Rows[_index].Cells[IndexGridColumnCollect.GRID_COLUMN_MONEY_COLLECTION_ID].Value.ToString());
              _terminal = m_terminals_proposed.Find(delegate(GamingHallTerminalCollectRefill x) { return x.MoneyCollectionId == _money_collection_id; });
              break;

            case CollectionGridMode.COLLECTED:
              _money_collection_id = int.Parse(dgv_collections.Rows[_index].Cells[IndexGridColumnCollect.GRID_COLUMN_MONEY_COLLECTION_ID].Value.ToString());
              _terminal = m_terminals_collected.Find(delegate(GamingHallTerminalCollectRefill x) { return x.MoneyCollectionId == _money_collection_id; });
              break;
          }

          if (_terminal != null)
          {
            // Current object (GamingHallTerminalCollectRefill) selected in the grid
            m_terminal_rc_selected = _terminal;
          }
        }
        else
        {
          m_terminal_rc_selected = null;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    }
    #endregion

  }
}
