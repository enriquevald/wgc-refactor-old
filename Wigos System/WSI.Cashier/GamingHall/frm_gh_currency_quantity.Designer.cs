namespace WSI.Cashier
{
    partial class frm_gh_currency_quantity
    {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
        if (disposing && (components != null))
        {
          components.Dispose();
        }
        base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
      this.btn_clear = new WSI.Cashier.Controls.uc_round_button();
      this.btn_ok = new WSI.Cashier.Controls.uc_round_button();
      this.btn_cancel = new WSI.Cashier.Controls.uc_round_button();
      this.lbl_denomination_total_value = new WSI.Cashier.Controls.uc_label();
      this.lbl_denomination_total_title = new WSI.Cashier.Controls.uc_label();
      this.dgv_common = new WSI.Cashier.Controls.uc_DataGridView();
      this.uc_input_amount1 = new WSI.Cashier.uc_input_amount();
      this.pnl_data.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dgv_common)).BeginInit();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.Controls.Add(this.lbl_denomination_total_value);
      this.pnl_data.Controls.Add(this.btn_clear);
      this.pnl_data.Controls.Add(this.lbl_denomination_total_title);
      this.pnl_data.Controls.Add(this.btn_ok);
      this.pnl_data.Controls.Add(this.dgv_common);
      this.pnl_data.Controls.Add(this.btn_cancel);
      this.pnl_data.Controls.Add(this.uc_input_amount1);
      this.pnl_data.Size = new System.Drawing.Size(849, 494);
      // 
      // btn_clear
      // 
      this.btn_clear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.btn_clear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_clear.FlatAppearance.BorderSize = 0;
      this.btn_clear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_clear.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_clear.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_clear.Image = null;
      this.btn_clear.IsSelected = false;
      this.btn_clear.Location = new System.Drawing.Point(18, 416);
      this.btn_clear.Name = "btn_clear";
      this.btn_clear.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_clear.Size = new System.Drawing.Size(155, 60);
      this.btn_clear.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_clear.TabIndex = 100;
      this.btn_clear.Text = "XCLEAR";
      this.btn_clear.UseVisualStyleBackColor = false;
      this.btn_clear.Click += new System.EventHandler(this.btn_clear_Click);
      // 
      // btn_ok
      // 
      this.btn_ok.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_ok.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_ok.FlatAppearance.BorderSize = 0;
      this.btn_ok.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_ok.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_ok.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_ok.Image = null;
      this.btn_ok.IsSelected = false;
      this.btn_ok.Location = new System.Drawing.Point(676, 416);
      this.btn_ok.Name = "btn_ok";
      this.btn_ok.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_ok.Size = new System.Drawing.Size(155, 60);
      this.btn_ok.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_ok.TabIndex = 3;
      this.btn_ok.Text = "XOK";
      this.btn_ok.UseVisualStyleBackColor = false;
      this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
      // 
      // btn_cancel
      // 
      this.btn_cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_cancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_cancel.FlatAppearance.BorderSize = 0;
      this.btn_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_cancel.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_cancel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_cancel.Image = null;
      this.btn_cancel.IsSelected = false;
      this.btn_cancel.Location = new System.Drawing.Point(506, 416);
      this.btn_cancel.Name = "btn_cancel";
      this.btn_cancel.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_cancel.Size = new System.Drawing.Size(155, 60);
      this.btn_cancel.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_cancel.TabIndex = 4;
      this.btn_cancel.Text = "XCANCEL";
      this.btn_cancel.UseVisualStyleBackColor = false;
      this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
      // 
      // lbl_denomination_total_value
      // 
      this.lbl_denomination_total_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_denomination_total_value.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_denomination_total_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_denomination_total_value.Location = new System.Drawing.Point(270, 389);
      this.lbl_denomination_total_value.Name = "lbl_denomination_total_value";
      this.lbl_denomination_total_value.Size = new System.Drawing.Size(161, 24);
      this.lbl_denomination_total_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_denomination_total_value.TabIndex = 103;
      this.lbl_denomination_total_value.Text = "V02";
      this.lbl_denomination_total_value.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      this.lbl_denomination_total_value.Visible = false;
      // 
      // lbl_denomination_total_title
      // 
      this.lbl_denomination_total_title.AutoSize = true;
      this.lbl_denomination_total_title.BackColor = System.Drawing.Color.Transparent;
      this.lbl_denomination_total_title.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_denomination_total_title.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_denomination_total_title.Location = new System.Drawing.Point(211, 392);
      this.lbl_denomination_total_title.Name = "lbl_denomination_total_title";
      this.lbl_denomination_total_title.Size = new System.Drawing.Size(34, 18);
      this.lbl_denomination_total_title.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_denomination_total_title.TabIndex = 102;
      this.lbl_denomination_total_title.Text = "V01";
      this.lbl_denomination_total_title.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.lbl_denomination_total_title.Visible = false;
      // 
      // dgv_common
      // 
      this.dgv_common.AllowUserToAddRows = false;
      this.dgv_common.AllowUserToDeleteRows = false;
      this.dgv_common.AllowUserToResizeColumns = false;
      this.dgv_common.AllowUserToResizeRows = false;
      this.dgv_common.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.dgv_common.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.dgv_common.ColumnHeaderHeight = 35;
      this.dgv_common.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
      this.dgv_common.ColumnHeadersHeight = 35;
      this.dgv_common.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
      this.dgv_common.CornerRadius = 10;
      this.dgv_common.DefaultCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.dgv_common.DefaultCellForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.dgv_common.EnableHeadersVisualStyles = false;
      this.dgv_common.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.dgv_common.GridColor = System.Drawing.Color.LightGray;
      this.dgv_common.HeaderBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
      this.dgv_common.HeaderDefaultCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.dgv_common.HeaderDefaultCellForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.dgv_common.Location = new System.Drawing.Point(18, 18);
      this.dgv_common.Name = "dgv_common";
      this.dgv_common.ReadOnly = true;
      this.dgv_common.RowHeadersVisible = false;
      this.dgv_common.RowHeadersWidth = 20;
      this.dgv_common.RowTemplateHeight = 35;
      this.dgv_common.Size = new System.Drawing.Size(413, 368);
      this.dgv_common.Style = WSI.Cashier.Controls.uc_DataGridView.DataGridViewStyle.WITH_ALL_ROUND_CORNERS;
      this.dgv_common.TabIndex = 101;
      this.dgv_common.SelectionChanged += new System.EventHandler(this.dgv_common_SelectionChanged);
      // 
      // uc_input_amount1
      // 
      this.uc_input_amount1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.uc_input_amount1.BackColor = System.Drawing.Color.Transparent;
      this.uc_input_amount1.Location = new System.Drawing.Point(447, 18);
      this.uc_input_amount1.Name = "uc_input_amount1";
      this.uc_input_amount1.Size = new System.Drawing.Size(384, 369);
      this.uc_input_amount1.TabIndex = 101;
      this.uc_input_amount1.WithDecimals = false;
      // 
      // frm_gh_currency_quantity
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(849, 549);
      this.Name = "frm_gh_currency_quantity";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Style = WSI.Cashier.Controls.frm_base.BaseFormStyle.PRINCIPAL;
      this.Text = "Meter reading";
      this.pnl_data.ResumeLayout(false);
      this.pnl_data.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dgv_common)).EndInit();
      this.ResumeLayout(false);

      }

      #endregion

      private WSI.Cashier.Controls.uc_round_button btn_cancel;
      private WSI.Cashier.Controls.uc_round_button btn_ok;
      private WSI.Cashier.Controls.uc_DataGridView dgv_common;
      private WSI.Cashier.Controls.uc_label lbl_denomination_total_value;
      private WSI.Cashier.Controls.uc_label lbl_denomination_total_title;
      private WSI.Cashier.Controls.uc_round_button btn_clear;
      private uc_input_amount uc_input_amount1;
    }
}