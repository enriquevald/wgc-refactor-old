﻿//------------------------------------------------------------------------------
// Copyright © 2007-2008 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_gaming_hall_collect.cs
//  
//   DESCRIPTION: Implements the class uc_gaming_hall (Collect and fill terminals.)
// 
//        AUTHOR Enric Tomas
// 
// CREATION DATE: 26-OCT-2015
// 
// REVISION HISTORY:
// 
// Date        Author     Description
//------------------------------------------------------------------------------
// 26-OCT-2015 ETP        Inital Draft.
//------------------------------------------------------------------------------

#region Usings

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using WSI.Common;
using WSI.Common.GamingHall;

#endregion // Usings

namespace WSI.Cashier
{
  public partial class uc_gaming_hall_collect : UserControl
  {
    #region Attributes
    private TerminalDenominations m_terminal_denominations = null;
    private TerminalBoxType m_terminal_box_type;

    #endregion

    #region Constructor
    public uc_gaming_hall_collect()
    {
      InitializeComponent();
      ProviderName = String.Empty;
      TerminalName = String.Empty;
    }

    #endregion //Constructor

    #region Properties

    public TerminalBoxType TerminalBoxType
    {
      set
      {
        m_terminal_box_type = value;
        switch (m_terminal_box_type)
        { 
          case Common.TerminalBoxType.Hopper:
            this.lbl_box_title.Text = Resource.String("STR_UC_GAMINGHALL_GRID_HOPPER");
            this.btn_denominations.Text = Resource.String("STR_BTN_HOPPER_DENOMINATIONS");
            break;
          case Common.TerminalBoxType.Box:
            this.lbl_box_title.Text = Resource.String("STR_UC_GAMINGHALL_GRID_DROPBOX");
            this.btn_denominations.Text = Resource.String("STR_BTN_DROPBOX_DENOMINATIONS");
            break;
          default:
            this.lbl_box_title.Text = "xBoxTitle";
            this.btn_denominations.Text = "xDeno";
            break;
        }
      }
      get
      {
        return m_terminal_box_type;
      }
    }

    public TerminalDenominations TerminalDenominationsSelected
    {
      get
      {
        return m_terminal_denominations;
      }
    }

    public new Boolean Enabled
    {
      set
      {
        this.lbl_box_title.Enabled = value;
        this.txt_amount.Enabled = value;
        this.btn_denominations.Enabled = value;
      }
      get
      {
        return btn_denominations.Enabled;
      }
    }

    public Boolean IsEmpty
    {
      get
      {
        return m_terminal_denominations == null || m_terminal_denominations.IsEmpty;
        
      }
    }

    public String ProviderName{get;set;}
    public String TerminalName{get;set;}

    #endregion //Properties

    #region Public Methods

    public void Clear()
    {
      m_terminal_denominations = null;
      txt_amount.Text = string.Empty;
    }
 
    #endregion //Public Methods

    #region Button Handling
    
    //------------------------------------------------------------------------------
    // PURPOSE: Add hopper denominations
    // 
    //  PARAMS:
    //      - INPUT:
    //          - sender:     object
    //          - EventArgs:  e
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    private void btn_hopper_denominations_Click(object sender, EventArgs e)
    {
      try
      {
        frm_gh_currency_quantity form;        
              

        if (m_terminal_denominations == null)
          GetNewTerminalDenominations();

        form = new frm_gh_currency_quantity();
        form.Show(this.ParentForm, ref m_terminal_denominations, ProviderName, TerminalName);

        if (m_terminal_denominations.IsEmpty)
        {
          txt_amount.Text = "";
        }
        else
        {
          txt_amount.Text = m_terminal_denominations.TotalCurrency.ToString();
        }

        form.Dispose();
      }
      catch (Exception ex)
      {
        Log.Error("Error getting hopper denominations. " + ex.ToString());
      }
    }

    private void GetNewTerminalDenominations()
    {
      string _iso_code;

      _iso_code = CurrencyExchange.GetNationalCurrency();

      switch (m_terminal_box_type)
      {
        case Common.TerminalBoxType.Hopper:
        case Common.TerminalBoxType.Box:
          m_terminal_denominations = new TerminalDenominations(m_terminal_box_type, _iso_code, CurrencyExchangeType.CURRENCY);
          break;
        default:
          throw new Exception("The TerminalBoxType value for this control is invalid");
      }
    }

    #endregion //Button Handling

    #region TextBox Handling
    
    private void txt_amount_Enter(object sender, EventArgs e)
    {
      this.ActiveControl = null;
    }

    #endregion    


  }
}
