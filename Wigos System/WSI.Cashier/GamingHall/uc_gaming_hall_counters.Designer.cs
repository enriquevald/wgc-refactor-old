﻿namespace WSI.Cashier.GamingHall
{
  partial class uc_gaming_hall_counters
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.txt_hand_pay_counter = new System.Windows.Forms.TextBox();
      this.txt_cash_out_counter = new System.Windows.Forms.TextBox();
      this.txt_cash_in_counter = new System.Windows.Forms.TextBox();
      this.lbl_hand_pay_title = new System.Windows.Forms.Label();
      this.lbl_cash_out_title = new System.Windows.Forms.Label();
      this.lbl_cash_in_title = new System.Windows.Forms.Label();
      this.grp_box_title = new System.Windows.Forms.GroupBox();
      this.grp_box_title.SuspendLayout();
      this.SuspendLayout();
      // 
      // txt_hand_pay_counter
      // 
      this.txt_hand_pay_counter.BackColor = System.Drawing.Color.LightGoldenrodYellow;
      this.txt_hand_pay_counter.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold);
      this.txt_hand_pay_counter.Location = new System.Drawing.Point(479, 30);
      this.txt_hand_pay_counter.Name = "txt_hand_pay_counter";
      this.txt_hand_pay_counter.Size = new System.Drawing.Size(223, 35);
      this.txt_hand_pay_counter.TabIndex = 105;
      this.txt_hand_pay_counter.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      // 
      // txt_cash_out_counter
      // 
      this.txt_cash_out_counter.BackColor = System.Drawing.Color.LightGoldenrodYellow;
      this.txt_cash_out_counter.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold);
      this.txt_cash_out_counter.Location = new System.Drawing.Point(124, 104);
      this.txt_cash_out_counter.Name = "txt_cash_out_counter";
      this.txt_cash_out_counter.Size = new System.Drawing.Size(223, 35);
      this.txt_cash_out_counter.TabIndex = 104;
      this.txt_cash_out_counter.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      // 
      // txt_cash_in_counter
      // 
      this.txt_cash_in_counter.BackColor = System.Drawing.Color.LightGoldenrodYellow;
      this.txt_cash_in_counter.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold);
      this.txt_cash_in_counter.Location = new System.Drawing.Point(124, 30);
      this.txt_cash_in_counter.Name = "txt_cash_in_counter";
      this.txt_cash_in_counter.Size = new System.Drawing.Size(223, 35);
      this.txt_cash_in_counter.TabIndex = 103;
      this.txt_cash_in_counter.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      // 
      // lbl_hand_pay_title
      // 
      this.lbl_hand_pay_title.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_hand_pay_title.Location = new System.Drawing.Point(312, 35);
      this.lbl_hand_pay_title.Name = "lbl_hand_pay_title";
      this.lbl_hand_pay_title.Size = new System.Drawing.Size(161, 25);
      this.lbl_hand_pay_title.TabIndex = 108;
      this.lbl_hand_pay_title.Text = "xHandPays";
      this.lbl_hand_pay_title.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_cash_out_title
      // 
      this.lbl_cash_out_title.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_cash_out_title.Location = new System.Drawing.Point(35, 109);
      this.lbl_cash_out_title.Name = "lbl_cash_out_title";
      this.lbl_cash_out_title.Size = new System.Drawing.Size(83, 25);
      this.lbl_cash_out_title.TabIndex = 107;
      this.lbl_cash_out_title.Text = "xCashOut";
      this.lbl_cash_out_title.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_cash_in_title
      // 
      this.lbl_cash_in_title.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_cash_in_title.Location = new System.Drawing.Point(35, 35);
      this.lbl_cash_in_title.Name = "lbl_cash_in_title";
      this.lbl_cash_in_title.Size = new System.Drawing.Size(83, 25);
      this.lbl_cash_in_title.TabIndex = 106;
      this.lbl_cash_in_title.Text = "xCashIn";
      this.lbl_cash_in_title.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // grp_box_title
      // 
      this.grp_box_title.Controls.Add(this.txt_cash_in_counter);
      this.grp_box_title.Controls.Add(this.txt_hand_pay_counter);
      this.grp_box_title.Controls.Add(this.lbl_cash_in_title);
      this.grp_box_title.Controls.Add(this.txt_cash_out_counter);
      this.grp_box_title.Controls.Add(this.lbl_cash_out_title);
      this.grp_box_title.Controls.Add(this.lbl_hand_pay_title);
      this.grp_box_title.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
      this.grp_box_title.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
      this.grp_box_title.ForeColor = System.Drawing.SystemColors.ControlText;
      this.grp_box_title.Location = new System.Drawing.Point(0, 0);
      this.grp_box_title.Name = "grp_box_title";
      this.grp_box_title.Size = new System.Drawing.Size(773, 165);
      this.grp_box_title.TabIndex = 109;
      this.grp_box_title.TabStop = false;
      // 
      // uc_gaming_hall_counters
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange;
      this.Controls.Add(this.grp_box_title);
      this.Name = "uc_gaming_hall_counters";
      this.Size = new System.Drawing.Size(780, 174);
      this.grp_box_title.ResumeLayout(false);
      this.grp_box_title.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.TextBox txt_hand_pay_counter;
    private System.Windows.Forms.TextBox txt_cash_out_counter;
    private System.Windows.Forms.TextBox txt_cash_in_counter;
    private System.Windows.Forms.Label lbl_hand_pay_title;
    private System.Windows.Forms.Label lbl_cash_out_title;
    private System.Windows.Forms.Label lbl_cash_in_title;
    private System.Windows.Forms.GroupBox grp_box_title;

  }
}
