﻿//------------------------------------------------------------------------------
// Copyright © 2007-2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_gh_set_sas_meters
//  
//   DESCRIPTION: Implements the class frm_gh_set_sas_meters (Get / Set terminal meters.)
// 
//        AUTHOR Daniel Dubé
// 
// CREATION DATE: 18-JAN-2016
// 
// REVISION HISTORY:
// 
// Date        Author     Description
//------------------------------------------------------------------------------
// 18-JAN-2016 DDS        Inital Draft.
// 08-APR-2016 ETP        Fixed Bug 11600: Needs to show / Save all Meters not only 3.
// 11-APR-2016 ETP        Fixed Bug 11600: Only for PCD terminals
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using WSI.Cashier.Controls;
using WSI.Common;

namespace WSI.Cashier.GamingHall
{
  public partial class frm_gh_set_meters : frm_base
  {
    #region Constants

    private const Int32 EVENT_START_BASE = 10000; // &H10000;
    private const Int32 CUSTOM_METERS_START_BASE = 1000; //  &H1000
    private const Int32 NLS_COUNTERS = 3100;
    private const Int32 NLS_EVENTS = 4000;
    private const Int32 NLS_CUSTOM_METERS = 3600;

    #endregion

    #region Members

    private Int32 m_terminal_id;
    private frm_yesno m_form_yes_no;
    private Boolean m_read_only;
    private Boolean m_can_edit;
    private String m_meters;
    private String m_original_xml_data;

    #endregion //Members

    #region Constructor

    public frm_gh_set_meters()
    {
      InitializeComponent();
    }

    #endregion // Constructor

    #region Public properties

    public String Meters
    {
      get { return this.m_meters; }
    }

    #endregion // Public properties

    #region Public methods

    public void Show(Control Parent, Int32 TerminalId, String XmlData, Boolean CanEditMeters)
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_gh_set_meters", Log.Type.Message);

      this.m_terminal_id = TerminalId;
      this.m_original_xml_data = XmlData;
      this.m_can_edit = CanEditMeters;
      this.LoadData(XmlData);
      this.InitializeForm();

      m_form_yes_no = new frm_yesno();
      m_form_yes_no.Opacity = 0.6;
      m_form_yes_no.Show(Parent);
      ShowDialog(m_form_yes_no);

      m_form_yes_no.Hide();
      Parent.Focus();

    }

    public override void pb_exit_Click(object sender, EventArgs e)
    {
      if (this.DiscardChanges())
      {
        base.pb_exit_Click(sender, e);
      }
    }

    #endregion //Public methods

    #region Private methods

    private Boolean SomeValueIsNeeded()
    {
      StringBuilder _errors;

      _errors = new StringBuilder();
      foreach (Control _ctrl in this.pnl_container.Controls)
      {
        uc_set_terminal_meter _meter = _ctrl as uc_set_terminal_meter;
        if (_meter != null && _meter.Visible && String.IsNullOrEmpty(_meter.Value))
        {
          _errors.AppendLine(Resource.String("STR_PCD_METER_NEEDS_VALUE", _meter.Description));
        }
      }
      if (_errors.Length > 0)
      {
        frm_message.Show(_errors.ToString(), Resource.String("STR_FRM_METERS_TITLE"), MessageBoxButtons.OK, MessageBoxIcon.Error, this.ParentForm);
        return true;
      }
      return false;
    }

    private void InitializeForm()
    {
      this.FormTitle = Resource.String("STR_FRM_METERS_TITLE");
      btn_ok.Text = Resource.String("STR_DB_SAVE");
      btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");
    }

    private void Save()
    {
      this.m_meters = CreateXmlData();

      Misc.WriteLog("[FORM CLOSE] frm_gh_set_meters (save)", Log.Type.Message);
      this.Close();
    }

    private String CreateXmlData()
    {
      StringBuilder _sb;

      _sb = new StringBuilder("<Meters>");

      foreach (Control _ctrl in this.pnl_container.Controls)
      {
        uc_set_terminal_meter _meter = _ctrl as uc_set_terminal_meter;
        if (_meter != null && _meter.Visible)
        {
          _sb.Append(_meter.GetMeterAsXml(false));
        }
      }

      _sb.Append("</Meters>");
      return _sb.ToString();
    }

    private String CreateEmptyXmlData()
        {
            IList<TerminalMeter> _meters;
            DataSet _pcd_info;
            DataRow[] _pcd_config_items;
            DataTable _sas_meters;
            SMIB_COMMUNICATION_TYPE _smib_comm_type;
            Int64 _smib_conf_id;
            String _configurable_meters;
            Int32 _meter_out_bills;
            Int32 _meter_out_coins;
            Int32 _meter_out_cents;

            _meter_out_bills = GeneralParam.GetInt32("GamingHall.SasMeter", "Meter.OutBills", 0);
            _meter_out_coins = GeneralParam.GetInt32("GamingHall.SasMeter", "Meter.OutCoins", 0);
            _meter_out_cents = GeneralParam.GetInt32("GamingHall.SasMeter", "Meter.OutCents", 0);
          
          _meters = new List<TerminalMeter>();
           
            using (DB_TRX _trx = new DB_TRX())
            {
              Terminal.Trx_GetTerminalSmibProtocol(this.m_terminal_id, TerminalTypes.SAS_HOST, _trx.SqlTransaction, out _smib_comm_type, out _smib_conf_id);
              if (_smib_comm_type == SMIB_COMMUNICATION_TYPE.PULSES)
              {
                _sas_meters = TerminalMetersInfo.ReadSASMeters(_trx.SqlTransaction, String.Empty);
                _pcd_info = TerminalMetersInfo.ReadPCDConfiguration(_trx.SqlTransaction, _smib_conf_id);
              }else
              {
                _configurable_meters = _meter_out_bills.ToString() + "," + _meter_out_coins.ToString() + "," + _meter_out_cents.ToString();
                _sas_meters = TerminalMetersInfo.ReadSASMeters(_trx.SqlTransaction, _configurable_meters);
                _pcd_info = null;              
              }

            }

            if (_smib_comm_type == SMIB_COMMUNICATION_TYPE.PULSES )
            {
              _pcd_config_items = _pcd_info.Tables[1].Select("PMT_PCD_IO_TYPE = 1");

              foreach (DataRow meter_info in _pcd_config_items)
              {
                DataRow[] _meters_codes;
                Int64 _meter_code;

                _meters_codes = _sas_meters.Select("SMCG_METER_CODE = " + meter_info["PMT_EGM_NUMBER"]);
                _meter_code = (Int64)meter_info["PMT_EGM_NUMBER"];

                if (_meters_codes == null || _meters_codes.Length == 0)
                  continue;

                _meters.Add(new TerminalMeter
                {
                  MeterCode = (Int32)_meter_code,
                  MeterValue = TerminalMetersInfo.NOT_READED_METER_VALUE
                });
              }

            }
            else
            {
              foreach (DataRow _row in _sas_meters.Rows)
              {
                if (!DBNull.Value.Equals(_row["SMCG_METER_CODE"]))
                {
                  _meters.Add(new TerminalMeter
                  {
                    MeterCode = (Int32)_row["SMCG_METER_CODE"],
                    MeterValue = TerminalMetersInfo.NOT_READED_METER_VALUE
                  });
                }
                else
                {
                  Log.Warning("CreateEmptyXML: MeterCode Not Read");
                }
              }
            }          
         
          return TerminalMetersInfo.GetMetersXml(_meters);
        }

    private void LoadData(String XmlData)
    {
      Byte _count;
      const byte inc = 46;
      XmlTextReader _reader;

      _count = 0;
      this.m_original_xml_data = XmlData;
      if (String.IsNullOrEmpty(this.m_original_xml_data))
      {
        this.m_original_xml_data = CreateEmptyXmlData();
        this.m_read_only = false;
      }

      // 20-JAN-2016 DDS https://msdn.microsoft.com/en-us/library/ayf5ffy5(v=vs.110).aspx
      _reader = new XmlTextReader(new System.IO.StringReader(this.m_original_xml_data));
      _reader.MoveToContent();
      _reader.ReadToDescendant("Meter");

      do
      {
        switch (_reader.NodeType)
        {
          case XmlNodeType.Element:
            uc_set_terminal_meter _meter;
            Decimal _value;
            Int32 _meter_code;
            String _meter_description;

            _meter = new uc_set_terminal_meter();
            this.pnl_container.Controls.Add(_meter);

            _meter.Visible = true;
            _meter.SetAction("NONE");
            _meter.DescriptionLocation = this.uc_set_terminal_meter_model.DescriptionLocation;
            _meter.DescriptionWidth = this.uc_set_terminal_meter_model.DescriptionWidth;
            _meter.ValueLocation = this.uc_set_terminal_meter_model.ValueLocation;
            _meter.ValueWidth = this.uc_set_terminal_meter_model.ValueWidth;
            _meter.Size = this.uc_set_terminal_meter_model.Size;
            _meter.MaxValue += _meter_MaxValue;
            _meter.Changed += _meter_Changed;
            _meter.SetMeterValues(_reader);


            if (Decimal.TryParse(_meter.Value, out _value) && _value > TerminalMetersInfo.NOT_READED_METER_VALUE)
              this.m_read_only = !this.m_can_edit;

            Int32.TryParse(_meter.Code, out _meter_code);
            _meter_description = Resource.String(String.Format("STR_SAS_METER_DESC_{0:00000}", _meter_code));

            _meter.Description = String.Format("{0}", _meter_description); // NLS ??
            _meter.Name = String.Format("uc_set_terminal_meter_{0}", _meter.Code.Replace(" ", "_"));

            _meter.Location = new Point(uc_set_terminal_meter_model.Location.X, uc_set_terminal_meter_model.Location.Y + (_count * inc));
            _meter.PadNumberHorizontalPosition = PadNumberHorizontalPosition.LEFT;
            _meter.PadNumberVerticalPosition = PadNumberVerticalPosition.MIDDLE;
            _count++;

            break;
          case XmlNodeType.Text:
          case XmlNodeType.EndElement:
            break;
        }
      } while (_reader.Read());

      if (this.m_read_only)
      {
        this.btn_ok.Visible = false;
        this.btn_cancel.Location = btn_ok.Location;
        this.btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CLOSE");
      }
    }

    private Boolean DiscardChanges()
    {
      this.m_meters = CreateXmlData();

      if (this.m_read_only || this.m_original_xml_data == this.m_meters)
      {
        this.m_meters = null;
        return true;
      }
      String _message = Resource.String("STR_PCD_METERS_LOST_CHANGES");
      _message = _message.Replace("\\r\\n", "\r\n");
      DialogResult _res = frm_message.Show(_message, Resource.String("STR_FRM_METERS_TITLE"), MessageBoxButtons.YesNo, MessageBoxIcon.Question, this.ParentForm);

      if (_res == System.Windows.Forms.DialogResult.OK)
      {
        this.m_meters = null;
        return true;
      }

      return false;
    }

    #endregion // private methods

    #region Events

    private void btn_cancel_Click(object sender, EventArgs e)
    {
      if (this.DiscardChanges())
      {
        Misc.WriteLog("[FORM CLOSE] frm_gh_set_meters (cancel)", Log.Type.Message);
        this.Close();
      }
    }

    private void btn_ok_Click(object sender, EventArgs e)
    {
      if (this.SomeValueIsNeeded())
      {
        return;
      }
      this.Save();
    }

    private void _meter_MaxValue(object sender, EventArgs e)
    {
      this.btn_ok.Enabled = false;
      frm_message.Show(((uc_set_terminal_meter)sender).MaxValueMsg, Resource.String("STR_PCD_METERS_FRM_TITLE"), MessageBoxButtons.OK, MessageBoxIcon.Warning, this.ParentForm);
    }

    private void _meter_Changed(object sender, EventArgs e)
    {
      this.btn_ok.Enabled = false;
      foreach (Control control in this.pnl_container.Controls)
      {
        uc_set_terminal_meter _meter = control as uc_set_terminal_meter;
        if (_meter != null && _meter.Visible && _meter.HasSelectedAction)
        {
          this.btn_ok.Enabled = true;
          Decimal _value;
          if (!Decimal.TryParse(_meter.Value, out _value) || _value == TerminalMetersInfo.NOT_READED_METER_VALUE
              || _value < 0)
          {
            this.btn_ok.Enabled = false;
            break;
          }
        }
      }
    }

    private void pnl_container_VisibleChanged(object sender, EventArgs e)
    {
      foreach (Control _ctrl in this.pnl_container.Controls)
      {
        uc_set_terminal_meter _meter = _ctrl as uc_set_terminal_meter;
        if (_meter != null && _meter.Visible)
        {
          if (this.m_read_only)
            _meter.ReadOnly = this.m_read_only;
        }
      }
    }

    #endregion Events
  }
}
