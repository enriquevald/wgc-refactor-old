﻿namespace WSI.Cashier
{
  partial class uc_gaming_hall
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
      this.pnl_flow_layout = new System.Windows.Forms.Panel();
      this.pb_collect_refill = new System.Windows.Forms.PictureBox();
      this.pb_error_icon = new System.Windows.Forms.PictureBox();
      this.pb_ok_icon = new System.Windows.Forms.PictureBox();
      this.lbl_ok_icon = new WSI.Cashier.Controls.uc_label();
      this.lbl_legend = new WSI.Cashier.Controls.uc_label();
      this.lbl_error_icon = new WSI.Cashier.Controls.uc_label();
      this.lbl_Totals = new WSI.Cashier.Controls.uc_label();
      this.btn_add_terminal = new WSI.Cashier.Controls.uc_round_button();
      this.lbl_separator_1 = new WSI.Cashier.Controls.uc_label();
      this.lbl_Total_Box = new WSI.Cashier.Controls.uc_label();
      this.lbl_Total_Hopper = new WSI.Cashier.Controls.uc_label();
      this.gb_ViewMode = new WSI.Cashier.Controls.uc_round_panel();
      this.pnl_radio_buttons = new System.Windows.Forms.Panel();
      this.rb_collected = new WSI.Cashier.Controls.uc_radioButton();
      this.rb_collection_prop = new WSI.Cashier.Controls.uc_radioButton();
      this.rb_pending_collect = new WSI.Cashier.Controls.uc_radioButton();
      this.btn_delete_terminal = new WSI.Cashier.Controls.uc_round_button();
      this.btn_collections_collect = new WSI.Cashier.Controls.uc_round_button();
      this.gb_Collections = new WSI.Cashier.Controls.uc_round_panel();
      this.dgv_collections = new WSI.Cashier.Controls.uc_DataGridView();
      this.gb_teminals = new WSI.Cashier.Controls.uc_round_panel();
      this.uc_terminal_filter1 = new WSI.Cashier.uc_terminal_filter();
      this.lbl_termminal_title = new WSI.Cashier.Controls.uc_label();
      ((System.ComponentModel.ISupportInitialize)(this.pb_collect_refill)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.pb_error_icon)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.pb_ok_icon)).BeginInit();
      this.gb_ViewMode.SuspendLayout();
      this.pnl_radio_buttons.SuspendLayout();
      this.gb_Collections.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dgv_collections)).BeginInit();
      this.gb_teminals.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_flow_layout
      // 
      this.pnl_flow_layout.AutoSize = true;
      this.pnl_flow_layout.Dock = System.Windows.Forms.DockStyle.Top;
      this.pnl_flow_layout.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.pnl_flow_layout.Location = new System.Drawing.Point(0, 0);
      this.pnl_flow_layout.Name = "pnl_flow_layout";
      this.pnl_flow_layout.Size = new System.Drawing.Size(850, 0);
      this.pnl_flow_layout.TabIndex = 59;
      // 
      // pb_collect_refill
      // 
      this.pb_collect_refill.Image = global::WSI.Cashier.Properties.Resources.extract2;
      this.pb_collect_refill.Location = new System.Drawing.Point(7, 4);
      this.pb_collect_refill.Margin = new System.Windows.Forms.Padding(2);
      this.pb_collect_refill.Name = "pb_collect_refill";
      this.pb_collect_refill.Size = new System.Drawing.Size(38, 41);
      this.pb_collect_refill.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
      this.pb_collect_refill.TabIndex = 70;
      this.pb_collect_refill.TabStop = false;
      // 
      // pb_error_icon
      // 
      this.pb_error_icon.Location = new System.Drawing.Point(39, 641);
      this.pb_error_icon.Margin = new System.Windows.Forms.Padding(2);
      this.pb_error_icon.Name = "pb_error_icon";
      this.pb_error_icon.Size = new System.Drawing.Size(12, 12);
      this.pb_error_icon.TabIndex = 114;
      this.pb_error_icon.TabStop = false;
      // 
      // pb_ok_icon
      // 
      this.pb_ok_icon.Location = new System.Drawing.Point(39, 660);
      this.pb_ok_icon.Margin = new System.Windows.Forms.Padding(2);
      this.pb_ok_icon.Name = "pb_ok_icon";
      this.pb_ok_icon.Size = new System.Drawing.Size(12, 12);
      this.pb_ok_icon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
      this.pb_ok_icon.TabIndex = 117;
      this.pb_ok_icon.TabStop = false;
      // 
      // lbl_ok_icon
      // 
      this.lbl_ok_icon.AutoSize = true;
      this.lbl_ok_icon.Font = new System.Drawing.Font("Open Sans Semibold", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_ok_icon.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_ok_icon.Location = new System.Drawing.Point(53, 659);
      this.lbl_ok_icon.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
      this.lbl_ok_icon.Name = "lbl_ok_icon";
      this.lbl_ok_icon.Size = new System.Drawing.Size(63, 14);
      this.lbl_ok_icon.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_10PX_BLACK;
      this.lbl_ok_icon.TabIndex = 116;
      this.lbl_ok_icon.Text = "xErrorLabel";
      // 
      // lbl_legend
      // 
      this.lbl_legend.AutoSize = true;
      this.lbl_legend.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_legend.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_legend.Location = new System.Drawing.Point(13, 617);
      this.lbl_legend.Name = "lbl_legend";
      this.lbl_legend.Size = new System.Drawing.Size(67, 19);
      this.lbl_legend.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_legend.TabIndex = 115;
      this.lbl_legend.Text = "xLegend";
      // 
      // lbl_error_icon
      // 
      this.lbl_error_icon.AutoSize = true;
      this.lbl_error_icon.Font = new System.Drawing.Font("Open Sans Semibold", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_error_icon.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_error_icon.Location = new System.Drawing.Point(53, 640);
      this.lbl_error_icon.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
      this.lbl_error_icon.Name = "lbl_error_icon";
      this.lbl_error_icon.Size = new System.Drawing.Size(63, 14);
      this.lbl_error_icon.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_10PX_BLACK;
      this.lbl_error_icon.TabIndex = 113;
      this.lbl_error_icon.Text = "xErrorLabel";
      // 
      // lbl_Totals
      // 
      this.lbl_Totals.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_Totals.AutoSize = true;
      this.lbl_Totals.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_Totals.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_Totals.Location = new System.Drawing.Point(258, 616);
      this.lbl_Totals.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
      this.lbl_Totals.Name = "lbl_Totals";
      this.lbl_Totals.Size = new System.Drawing.Size(59, 19);
      this.lbl_Totals.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_Totals.TabIndex = 111;
      this.lbl_Totals.Text = "xTotals";
      // 
      // btn_add_terminal
      // 
      this.btn_add_terminal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_add_terminal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(47)))), ((int)(((byte)(50)))));
      this.btn_add_terminal.FlatAppearance.BorderSize = 0;
      this.btn_add_terminal.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_add_terminal.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_add_terminal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_add_terminal.Image = null;
      this.btn_add_terminal.IsSelected = false;
      this.btn_add_terminal.Location = new System.Drawing.Point(698, 199);
      this.btn_add_terminal.Margin = new System.Windows.Forms.Padding(2);
      this.btn_add_terminal.Name = "btn_add_terminal";
      this.btn_add_terminal.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_add_terminal.Size = new System.Drawing.Size(141, 41);
      this.btn_add_terminal.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.TERTIARY;
      this.btn_add_terminal.TabIndex = 107;
      this.btn_add_terminal.Text = "XADD TERMINAL";
      this.btn_add_terminal.UseVisualStyleBackColor = false;
      this.btn_add_terminal.Click += new System.EventHandler(this.btn_add_terminal_Click);
      // 
      // lbl_separator_1
      // 
      this.lbl_separator_1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_separator_1.BackColor = System.Drawing.Color.Black;
      this.lbl_separator_1.Font = new System.Drawing.Font("Open Sans Semibold", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_separator_1.Location = new System.Drawing.Point(3, 47);
      this.lbl_separator_1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
      this.lbl_separator_1.Name = "lbl_separator_1";
      this.lbl_separator_1.Size = new System.Drawing.Size(844, 2);
      this.lbl_separator_1.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.lbl_separator_1.TabIndex = 111;
      // 
      // lbl_Total_Box
      // 
      this.lbl_Total_Box.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_Total_Box.BackColor = System.Drawing.Color.Transparent;
      this.lbl_Total_Box.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_Total_Box.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_Total_Box.Location = new System.Drawing.Point(463, 616);
      this.lbl_Total_Box.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
      this.lbl_Total_Box.Name = "lbl_Total_Box";
      this.lbl_Total_Box.Size = new System.Drawing.Size(97, 19);
      this.lbl_Total_Box.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_Total_Box.TabIndex = 110;
      this.lbl_Total_Box.Text = "x_Total_Dropbox";
      this.lbl_Total_Box.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_Total_Hopper
      // 
      this.lbl_Total_Hopper.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_Total_Hopper.BackColor = System.Drawing.Color.Transparent;
      this.lbl_Total_Hopper.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_Total_Hopper.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_Total_Hopper.Location = new System.Drawing.Point(336, 616);
      this.lbl_Total_Hopper.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
      this.lbl_Total_Hopper.Name = "lbl_Total_Hopper";
      this.lbl_Total_Hopper.Size = new System.Drawing.Size(97, 19);
      this.lbl_Total_Hopper.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_Total_Hopper.TabIndex = 109;
      this.lbl_Total_Hopper.Text = "xTotal_Hopper";
      this.lbl_Total_Hopper.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // gb_ViewMode
      // 
      this.gb_ViewMode.BackColor = System.Drawing.Color.Transparent;
      this.gb_ViewMode.BorderColor = System.Drawing.Color.Empty;
      this.gb_ViewMode.Controls.Add(this.pnl_radio_buttons);
      this.gb_ViewMode.CornerRadius = 10;
      this.gb_ViewMode.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_ViewMode.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gb_ViewMode.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_ViewMode.HeaderHeight = 35;
      this.gb_ViewMode.HeaderSubText = null;
      this.gb_ViewMode.HeaderText = null;
      this.gb_ViewMode.Location = new System.Drawing.Point(11, 61);
      this.gb_ViewMode.Margin = new System.Windows.Forms.Padding(2);
      this.gb_ViewMode.Name = "gb_ViewMode";
      this.gb_ViewMode.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_ViewMode.Size = new System.Drawing.Size(222, 145);
      this.gb_ViewMode.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.gb_ViewMode.TabIndex = 108;
      this.gb_ViewMode.Text = "xViewMode";
      // 
      // pnl_radio_buttons
      // 
      this.pnl_radio_buttons.Controls.Add(this.rb_collected);
      this.pnl_radio_buttons.Controls.Add(this.rb_collection_prop);
      this.pnl_radio_buttons.Controls.Add(this.rb_pending_collect);
      this.pnl_radio_buttons.Location = new System.Drawing.Point(5, 40);
      this.pnl_radio_buttons.Name = "pnl_radio_buttons";
      this.pnl_radio_buttons.Size = new System.Drawing.Size(214, 102);
      this.pnl_radio_buttons.TabIndex = 0;
      // 
      // rb_collected
      // 
      this.rb_collected.AutoSize = true;
      this.rb_collected.BackColor = System.Drawing.Color.Transparent;
      this.rb_collected.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.rb_collected.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.rb_collected.Location = new System.Drawing.Point(2, 70);
      this.rb_collected.Margin = new System.Windows.Forms.Padding(2);
      this.rb_collected.Name = "rb_collected";
      this.rb_collected.Padding = new System.Windows.Forms.Padding(10);
      this.rb_collected.Size = new System.Drawing.Size(119, 30);
      this.rb_collected.TabIndex = 111;
      this.rb_collected.TabStop = true;
      this.rb_collected.Text = "xCollected";
      this.rb_collected.UseVisualStyleBackColor = true;
      // 
      // rb_collection_prop
      // 
      this.rb_collection_prop.AutoSize = true;
      this.rb_collection_prop.BackColor = System.Drawing.Color.Transparent;
      this.rb_collection_prop.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.rb_collection_prop.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.rb_collection_prop.Location = new System.Drawing.Point(2, 2);
      this.rb_collection_prop.Margin = new System.Windows.Forms.Padding(2);
      this.rb_collection_prop.Name = "rb_collection_prop";
      this.rb_collection_prop.Padding = new System.Windows.Forms.Padding(10);
      this.rb_collection_prop.Size = new System.Drawing.Size(189, 30);
      this.rb_collection_prop.TabIndex = 109;
      this.rb_collection_prop.TabStop = true;
      this.rb_collection_prop.Text = "xCollection Proposal";
      this.rb_collection_prop.UseVisualStyleBackColor = true;
      // 
      // rb_pending_collect
      // 
      this.rb_pending_collect.AutoSize = true;
      this.rb_pending_collect.BackColor = System.Drawing.Color.Transparent;
      this.rb_pending_collect.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.rb_pending_collect.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.rb_pending_collect.Location = new System.Drawing.Point(2, 36);
      this.rb_pending_collect.Margin = new System.Windows.Forms.Padding(2);
      this.rb_pending_collect.Name = "rb_pending_collect";
      this.rb_pending_collect.Padding = new System.Windows.Forms.Padding(10);
      this.rb_pending_collect.Size = new System.Drawing.Size(162, 30);
      this.rb_pending_collect.TabIndex = 110;
      this.rb_pending_collect.TabStop = true;
      this.rb_pending_collect.Text = "xPending Collect";
      this.rb_pending_collect.UseVisualStyleBackColor = true;
      // 
      // btn_delete_terminal
      // 
      this.btn_delete_terminal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_delete_terminal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_delete_terminal.FlatAppearance.BorderSize = 0;
      this.btn_delete_terminal.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_delete_terminal.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_delete_terminal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_delete_terminal.Image = null;
      this.btn_delete_terminal.IsSelected = false;
      this.btn_delete_terminal.Location = new System.Drawing.Point(537, 641);
      this.btn_delete_terminal.Margin = new System.Windows.Forms.Padding(2);
      this.btn_delete_terminal.Name = "btn_delete_terminal";
      this.btn_delete_terminal.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_delete_terminal.Size = new System.Drawing.Size(140, 49);
      this.btn_delete_terminal.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_delete_terminal.TabIndex = 107;
      this.btn_delete_terminal.Text = "XCDELETE";
      this.btn_delete_terminal.UseVisualStyleBackColor = false;
      this.btn_delete_terminal.Click += new System.EventHandler(this.btn_delete_terminal_Click);
      // 
      // btn_collections_collect
      // 
      this.btn_collections_collect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_collections_collect.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_collections_collect.FlatAppearance.BorderSize = 0;
      this.btn_collections_collect.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_collections_collect.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_collections_collect.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_collections_collect.Image = null;
      this.btn_collections_collect.IsSelected = false;
      this.btn_collections_collect.Location = new System.Drawing.Point(698, 641);
      this.btn_collections_collect.Margin = new System.Windows.Forms.Padding(2);
      this.btn_collections_collect.Name = "btn_collections_collect";
      this.btn_collections_collect.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_collections_collect.Size = new System.Drawing.Size(140, 49);
      this.btn_collections_collect.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_collections_collect.TabIndex = 105;
      this.btn_collections_collect.Text = "XCOLLECT";
      this.btn_collections_collect.UseVisualStyleBackColor = false;
      this.btn_collections_collect.Click += new System.EventHandler(this.btn_collections_collect_Click);
      // 
      // gb_Collections
      // 
      this.gb_Collections.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.gb_Collections.BackColor = System.Drawing.Color.Transparent;
      this.gb_Collections.BorderColor = System.Drawing.Color.Empty;
      this.gb_Collections.Controls.Add(this.dgv_collections);
      this.gb_Collections.CornerRadius = 10;
      this.gb_Collections.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_Collections.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gb_Collections.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_Collections.HeaderHeight = 35;
      this.gb_Collections.HeaderSubText = null;
      this.gb_Collections.HeaderText = null;
      this.gb_Collections.Location = new System.Drawing.Point(11, 251);
      this.gb_Collections.Margin = new System.Windows.Forms.Padding(2);
      this.gb_Collections.Name = "gb_Collections";
      this.gb_Collections.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_Collections.Size = new System.Drawing.Size(827, 359);
      this.gb_Collections.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.gb_Collections.TabIndex = 99;
      this.gb_Collections.Text = "xCollections";
      // 
      // dgv_collections
      // 
      this.dgv_collections.AllowUserToAddRows = false;
      this.dgv_collections.AllowUserToDeleteRows = false;
      this.dgv_collections.AllowUserToResizeColumns = false;
      this.dgv_collections.AllowUserToResizeRows = false;
      this.dgv_collections.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.dgv_collections.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.dgv_collections.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.dgv_collections.ColumnHeaderHeight = 35;
      this.dgv_collections.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
      dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      dataGridViewCellStyle1.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      dataGridViewCellStyle1.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
      dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
      dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
      this.dgv_collections.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
      this.dgv_collections.ColumnHeadersHeight = 35;
      this.dgv_collections.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
      this.dgv_collections.CornerRadius = 10;
      this.dgv_collections.DefaultCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.dgv_collections.DefaultCellForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.dgv_collections.DefaultCellSelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.dgv_collections.DefaultCellSelectionForeColor = System.Drawing.Color.White;
      dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      dataGridViewCellStyle2.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(211)))), ((int)(((byte)(216)))));
      dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
      this.dgv_collections.DefaultCellStyle = dataGridViewCellStyle2;
      this.dgv_collections.EnableHeadersVisualStyles = false;
      this.dgv_collections.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.dgv_collections.GridColor = System.Drawing.Color.LightGray;
      this.dgv_collections.HeaderBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
      this.dgv_collections.HeaderDefaultCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.dgv_collections.HeaderDefaultCellForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.dgv_collections.HeaderImages = null;
      this.dgv_collections.Location = new System.Drawing.Point(-1, 28);
      this.dgv_collections.Margin = new System.Windows.Forms.Padding(2);
      this.dgv_collections.MultiSelect = false;
      this.dgv_collections.Name = "dgv_collections";
      this.dgv_collections.ReadOnly = true;
      dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle3.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
      dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
      dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
      dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
      this.dgv_collections.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
      this.dgv_collections.RowHeadersVisible = false;
      this.dgv_collections.RowHeadersWidth = 20;
      this.dgv_collections.RowTemplateHeight = 35;
      this.dgv_collections.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
      this.dgv_collections.Size = new System.Drawing.Size(829, 332);
      this.dgv_collections.Style = WSI.Cashier.Controls.uc_DataGridView.DataGridViewStyle.WITH_BOTTOM_ROUND_BORDERS;
      this.dgv_collections.TabIndex = 0;
      this.dgv_collections.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_collections_CellClick);
      this.dgv_collections.SelectionChanged += new System.EventHandler(this.dgv_collections_SelectionChanged);
      // 
      // gb_teminals
      // 
      this.gb_teminals.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.gb_teminals.BackColor = System.Drawing.Color.Transparent;
      this.gb_teminals.BorderColor = System.Drawing.Color.Empty;
      this.gb_teminals.Controls.Add(this.uc_terminal_filter1);
      this.gb_teminals.CornerRadius = 10;
      this.gb_teminals.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_teminals.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gb_teminals.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_teminals.HeaderHeight = 35;
      this.gb_teminals.HeaderSubText = null;
      this.gb_teminals.HeaderText = null;
      this.gb_teminals.Location = new System.Drawing.Point(241, 61);
      this.gb_teminals.Margin = new System.Windows.Forms.Padding(2);
      this.gb_teminals.Name = "gb_teminals";
      this.gb_teminals.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_teminals.Size = new System.Drawing.Size(376, 179);
      this.gb_teminals.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.gb_teminals.TabIndex = 98;
      this.gb_teminals.Text = "xTerminals";
      // 
      // uc_terminal_filter1
      // 
      this.uc_terminal_filter1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.uc_terminal_filter1.BackColor = System.Drawing.Color.Transparent;
      this.uc_terminal_filter1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.uc_terminal_filter1.Location = new System.Drawing.Point(-1, 35);
      this.uc_terminal_filter1.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
      this.uc_terminal_filter1.Name = "uc_terminal_filter1";
      this.uc_terminal_filter1.RightToLeft = System.Windows.Forms.RightToLeft.No;
      this.uc_terminal_filter1.Size = new System.Drawing.Size(378, 144);
      this.uc_terminal_filter1.TabIndex = 1;
      // 
      // lbl_termminal_title
      // 
      this.lbl_termminal_title.AutoSize = true;
      this.lbl_termminal_title.BackColor = System.Drawing.Color.Transparent;
      this.lbl_termminal_title.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_termminal_title.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_termminal_title.Location = new System.Drawing.Point(52, 18);
      this.lbl_termminal_title.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
      this.lbl_termminal_title.Name = "lbl_termminal_title";
      this.lbl_termminal_title.Size = new System.Drawing.Size(86, 22);
      this.lbl_termminal_title.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_termminal_title.TabIndex = 61;
      this.lbl_termminal_title.Text = "xTerminal";
      // 
      // uc_gaming_hall
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.Color.LightGray;
      this.Controls.Add(this.pb_ok_icon);
      this.Controls.Add(this.lbl_ok_icon);
      this.Controls.Add(this.lbl_legend);
      this.Controls.Add(this.pb_error_icon);
      this.Controls.Add(this.lbl_error_icon);
      this.Controls.Add(this.lbl_Totals);
      this.Controls.Add(this.btn_add_terminal);
      this.Controls.Add(this.lbl_separator_1);
      this.Controls.Add(this.lbl_Total_Box);
      this.Controls.Add(this.lbl_Total_Hopper);
      this.Controls.Add(this.gb_ViewMode);
      this.Controls.Add(this.btn_delete_terminal);
      this.Controls.Add(this.btn_collections_collect);
      this.Controls.Add(this.gb_Collections);
      this.Controls.Add(this.gb_teminals);
      this.Controls.Add(this.pb_collect_refill);
      this.Controls.Add(this.lbl_termminal_title);
      this.Controls.Add(this.pnl_flow_layout);
      this.Name = "uc_gaming_hall";
      this.Size = new System.Drawing.Size(850, 714);
      ((System.ComponentModel.ISupportInitialize)(this.pb_collect_refill)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.pb_error_icon)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.pb_ok_icon)).EndInit();
      this.gb_ViewMode.ResumeLayout(false);
      this.pnl_radio_buttons.ResumeLayout(false);
      this.pnl_radio_buttons.PerformLayout();
      this.gb_Collections.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.dgv_collections)).EndInit();
      this.gb_teminals.ResumeLayout(false);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    
    private uc_terminal_filter uc_terminal_filter1;
    private System.Windows.Forms.Panel pnl_flow_layout;
    private WSI.Cashier.Controls.uc_label lbl_termminal_title;
    private System.Windows.Forms.PictureBox pb_collect_refill;
    private WSI.Cashier.Controls.uc_round_panel gb_teminals;
    private WSI.Cashier.Controls.uc_round_panel gb_Collections;
    private WSI.Cashier.Controls.uc_DataGridView dgv_collections;
    private WSI.Cashier.Controls.uc_round_button btn_collections_collect;
    private WSI.Cashier.Controls.uc_round_button btn_delete_terminal;
    private WSI.Cashier.Controls.uc_round_button btn_add_terminal;
    private WSI.Cashier.Controls.uc_round_panel gb_ViewMode;
    private WSI.Cashier.Controls.uc_radioButton rb_collection_prop;
    private WSI.Cashier.Controls.uc_radioButton rb_pending_collect;
    private WSI.Cashier.Controls.uc_radioButton rb_collected;
    private WSI.Cashier.Controls.uc_label lbl_Total_Hopper;
    private WSI.Cashier.Controls.uc_label lbl_Total_Box;
    private WSI.Cashier.Controls.uc_label lbl_separator_1;
    private WSI.Cashier.Controls.uc_label lbl_error_icon;
    private System.Windows.Forms.PictureBox pb_error_icon;
    private WSI.Cashier.Controls.uc_label lbl_legend;
    private WSI.Cashier.Controls.uc_label lbl_Totals;
    private System.Windows.Forms.PictureBox pb_ok_icon;
    private Controls.uc_label lbl_ok_icon;
    private System.Windows.Forms.Panel pnl_radio_buttons;


  }
}
