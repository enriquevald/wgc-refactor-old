//------------------------------------------------------------------------------
// Copyright © 2007-2015 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_gaming_hall_currency_quantity.cs
// 
//   DESCRIPTION: Implements Denominations and Amount form.
//
//        AUTHOR: Francisco Vicente.
// 
// CREATION DATE: 16-OCT-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 16-OCT-2015 FAV     First release.
// 08-JAN-2016 ETP     Resize for entry mode Read Only.
// 14-JAN-2016 DDS     PBI 8000: Salones(2): Cajero: Recaudación con montos totales o por denominación
// 01-MAR-2016 DDS     Fixed Bug 10138. Hopper is empty when Cage.ShowDenominatios = 0 and entering value manually
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using WSI.Common;
using WSI.Common.GamingHall;
using WSI.Cashier.Controls;

namespace WSI.Cashier
{
  public partial class frm_gh_currency_quantity : frm_base, IDisposable
  {

    #region Attributes

    private const Int16 INDEX_COLUMN_DENOMINATION = 0;
    private const Int16 INDEX_COLUMN_CURRENCY_TYPE = 1;
    private const Int16 INDEX_COLUMN_AMOUNT = 2;
    private const Int16 INDEX_COLUMN_TOTAL = 3;
    private const Int16 WIDTH_COLUMN_DENOMINATION = 120;
    private const Int16 WIDTH_COLUMN_CURRENCY_TYPE = 50;
    private const Int16 WIDTH_COLUMN_AMOUNT = 80;
    private const Int16 WIDTH_COLUMN_TOTAL = 140;
    private const String NAME_COLUMN_DENOMINATION = "DENOMINATION";
    private const String NAME_COLUMN_CURRENCY_TYPE = "CURRENCY_TYPE";
    private const String NAME_COLUMN_AMOUNT = "AMOUNT";
    private const String NAME_COLUMN_TOTAL = "TOTAL";

    private frm_yesno m_form_yes_no;
    private TerminalDenominations m_term_denom = null;
    private bool m_close_ok = false;
    private EntryMode m_entry_mode;
    private bool m_show_all_denominations;

    #endregion

    #region Enum
    public enum EntryMode
    {
      EDITION = 0,
      READONLY = 1
    }
    #endregion

    #region Constructor

    public frm_gh_currency_quantity(EntryMode _entry_mode = EntryMode.EDITION)
    {
      m_entry_mode = _entry_mode;

      InitializeComponent();

      InitializeForm();
    }

    #endregion

    #region Properties

    public Boolean ReadOnly
    {
      set
      {
        uc_input_amount1.Enabled = false;
        btn_clear.Enabled = false;
        btn_cancel.Enabled = false;
        btn_ok.Enabled = true;

      }
      get
      {
        return uc_input_amount1.Enabled;
      }
    }
    #endregion

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Init and Show the form.
    //
    //  PARAMS :
    //      - INPUT :
    //          - ParentForm
    //          - TerminalDenominations
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public void Show(Form Parent, ref TerminalDenominations TermDenom, String ProviderName, String TerminalName)
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_gh_currency_quantity", Log.Type.Message);


      this.Location = new Point(Parent.Width / 2 - Width / 2, Parent.Height / 2 - 200);

      m_term_denom = new TerminalDenominations(TermDenom.TerminalBoxType, TermDenom.IsoCode, CurrencyExchangeType.CURRENCY);

      if (TermDenom == null || TermDenom.TerminalBoxType == TerminalBoxType.None)
        throw new Exception("The TerminalDenominations parameter doesn't have a TerminalBoxType valid!.");



      if (TermDenom.TerminalBoxType == TerminalBoxType.Box)
        this.FormTitle = Resource.String("STR_FRM_CONTAINER_BTN_COLLECT") + " " + Resource.String("STR_SESSION_MACHINE") + " " + ProviderName + " - " + TerminalName;
      else if (TermDenom.TerminalBoxType == TerminalBoxType.Hopper)
        this.FormTitle = Resource.String("STR_FRM_CONTAINER_BTN_FILL") + " " + Resource.String("STR_SESSION_MACHINE") + " " + ProviderName + " - " + TerminalName;

      //m_show_all_denominations = false;

      if (m_show_all_denominations)
      {
        m_term_denom.Denominations = GetInitialDenominations(TermDenom.TerminalBoxType);
        if (TermDenom.Denominations != null)
        {
          if (TermDenom.Denominations.Count <= m_term_denom.Denominations.Count)
          {
            foreach (TerminalCurrencyItem _tci in TermDenom.Denominations)
            {
              TerminalCurrencyItem _tci_aux;
              _tci_aux = m_term_denom.Denominations.Find(x => x.Denomination.Equals(_tci.Denomination));

              if (_tci_aux != null)
              {
                _tci_aux.Quantity = _tci.Quantity;
              }
              else
              {
                m_term_denom.Denominations.Add(_tci);
              }
            }
          }
        }
      }
      else
      {
        this.uc_input_amount1.SetDotButtonEnabled(true);
        m_term_denom.Denominations = new List<TerminalCurrencyItem>();
        TerminalCurrencyItem total_amount = new TerminalCurrencyItem(TermDenom.IsoCode, CurrencyExchangeType.CURRENCY, false);
        total_amount.Denomination = -100;
        if (TermDenom.Denominations != null)
        {
          TermDenom.Denominations.ForEach(delegate(TerminalCurrencyItem item)
          {
            total_amount.Total += item.Total;
          });
        }
        m_term_denom.Denominations.Add(total_amount);
      }

      dgv_common.DataSource = m_term_denom.Denominations;

      SetStyleToGrid();

      UpdateTotal();

      m_form_yes_no.Show(Parent);

      ShowDialog(m_form_yes_no);

      m_form_yes_no.Hide();

      if (m_close_ok)
        TermDenom = m_term_denom;

      Parent.Focus();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Dispose
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    void IDisposable.Dispose()
    {
      m_form_yes_no.Dispose();
      m_form_yes_no = null;

      base.Dispose();
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Initialize objects and resources (NLS strings, images, etc)
    /// </summary>
    private void InitializeForm()
    {
      uc_input_amount1.gp_digits_box.Text = Resource.String("STR_VOUCHER_CAGESESSIONREPORT_QUANTITY");
      lbl_denomination_total_title.Text = Resource.String("STR_VOUCHER_TOTAL_AMOUNT") + ":"; // Total:

      btn_ok.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_OK");
      btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");
      btn_clear.Text = Resource.String("STR_CAGE_BTN_CLEAR");

      uc_input_amount1.OnAmountSelected += uc_input_amount1_OnAmountSelected;

      this.m_show_all_denominations = GeneralParam.GetInt32("Cage", "ShowDenominations",
        (Int32)WSI.Common.Cage.ShowDenominationsMode.ShowAllDenominations) == (Int32)WSI.Common.Cage.ShowDenominationsMode.ShowAllDenominations;
      this.uc_input_amount1.SetDotButtonEnabled(!this.m_show_all_denominations);

      uc_input_amount1.Visible = (m_entry_mode == EntryMode.EDITION);
      btn_clear.Visible = (m_entry_mode == EntryMode.EDITION);
      btn_ok.Visible = (m_entry_mode == EntryMode.EDITION);

      if (m_entry_mode == EntryMode.READONLY)
      {
        this.Size = new System.Drawing.Size(dgv_common.Location.X + dgv_common.Width + dgv_common.Location.X, this.Size.Height);
        this.ShowCloseButton = false;

        btn_cancel.Text = Resource.String("STR_FRM_CONTAINER_BTN_EXIT");
        btn_cancel.Location = new System.Drawing.Point((dgv_common.Location.X + dgv_common.Width) - btn_ok.Width, btn_ok.Location.Y);
      }

      m_form_yes_no = new frm_yesno();
      m_form_yes_no.Opacity = 0.6;
      btn_ok.Enabled = false;
      uc_input_amount1.Focus();

    }

    /// <summary>
    /// Gets a list with the denominations for the terminal
    /// </summary>
    /// <param name="Direction"></param>
    /// <returns></returns>
    private List<TerminalCurrencyItem> GetInitialDenominations(TerminalBoxType Direction)
    {
      List<TerminalCurrencyItem> _list;
      TerminalCurrencyOperation _terminal_currency_op;

      _terminal_currency_op = new TerminalCurrencyOperation();
      _terminal_currency_op.GetTerminalCurrencyItems((Int32)Cashier.TerminalId, Direction, out _list);

      return _list;
    }

    private void SetStyleToGrid()
    {
      dgv_common.Columns[INDEX_COLUMN_DENOMINATION].HeaderText = Resource.String("STR_CAGE_TEXT_COLUMN_DENOMINATION_SHORT"); // "Denom.";
      dgv_common.Columns[INDEX_COLUMN_DENOMINATION].Width = WIDTH_COLUMN_DENOMINATION;
      dgv_common.Columns[INDEX_COLUMN_DENOMINATION].Visible = true;
      dgv_common.Columns[INDEX_COLUMN_DENOMINATION].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
      dgv_common.Columns[INDEX_COLUMN_DENOMINATION].SortMode = DataGridViewColumnSortMode.NotSortable;

      dgv_common.Columns[INDEX_COLUMN_CURRENCY_TYPE].HeaderText = Resource.String("STR_VOUCHER_CARD_HANDPAY_SESSION_CURRENCY_HEAD"); // "T";
      dgv_common.Columns[INDEX_COLUMN_CURRENCY_TYPE].Width = WIDTH_COLUMN_CURRENCY_TYPE;
      dgv_common.Columns[INDEX_COLUMN_CURRENCY_TYPE].Visible = true;
      dgv_common.Columns[INDEX_COLUMN_CURRENCY_TYPE].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
      dgv_common.Columns[INDEX_COLUMN_CURRENCY_TYPE].SortMode = DataGridViewColumnSortMode.NotSortable;

      dgv_common.Columns[INDEX_COLUMN_AMOUNT].HeaderText = Resource.String("STR_CAGE_TEXT_COLUMN_UNITS"); // "Cant.";
      dgv_common.Columns[INDEX_COLUMN_AMOUNT].Width = WIDTH_COLUMN_AMOUNT;
      dgv_common.Columns[INDEX_COLUMN_AMOUNT].Visible = true;
      dgv_common.Columns[INDEX_COLUMN_AMOUNT].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
      dgv_common.Columns[INDEX_COLUMN_AMOUNT].SortMode = DataGridViewColumnSortMode.NotSortable;

      dgv_common.Columns[INDEX_COLUMN_TOTAL].HeaderText = Resource.String("STR_CAGE_TEXT_COLUMN_TOTAL"); // "Total";
      dgv_common.Columns[INDEX_COLUMN_TOTAL].Width = WIDTH_COLUMN_TOTAL;
      dgv_common.Columns[INDEX_COLUMN_TOTAL].Visible = true;
      dgv_common.Columns[INDEX_COLUMN_TOTAL].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
      dgv_common.Columns[INDEX_COLUMN_TOTAL].SortMode = DataGridViewColumnSortMode.NotSortable;

      dgv_common.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
      dgv_common.MultiSelect = false;
      dgv_common.ColumnHeadersHeight = 30;
      dgv_common.RowTemplate.Height = 30;

    }

    /// <summary>
    /// Change to next row when the user enter a new Amount
    /// </summary>
    private void ChangeToNextRowInDataGrid()
    {
      Int32 _index_actual;
      Int32 _index_next;

      try
      {
        _index_actual = dgv_common.SelectedRows[0].Index;
        _index_next = (_index_actual + 1) % dgv_common.Rows.Count;

        if (_index_actual < dgv_common.Rows.Count - 1)
        {
          dgv_common.Rows[_index_actual].Selected = false;
          dgv_common.Rows[_index_next].Selected = true;

          if (_index_actual % 7 == 0) //Make a scroll in the grid
          {
            dgv_common.FirstDisplayedScrollingRowIndex = _index_actual;
          }
        }
        else if (_index_actual == dgv_common.Rows.Count - 1)
        {
          dgv_common.Rows[0].Selected = true;
          dgv_common.FirstDisplayedScrollingRowIndex = 0;
        }

        uc_input_amount1.Focus();
        UpdateTotal();
      }
      catch { }
    }

    /// <summary>
    /// Update total in the labels
    /// </summary>
    private void UpdateTotal()
    {

      bool _show_total = false;
      if(this.m_show_all_denominations)
      {
          _show_total = (m_term_denom.Total > 0);
          lbl_denomination_total_value.Text = m_term_denom.TotalCurrency.ToString();
      }
      else
      {
          _show_total = m_term_denom.Denominations!= null && m_term_denom.Denominations.Count == 1 && m_term_denom.Denominations[0].Total > 0;
          lbl_denomination_total_value.Text = m_term_denom.Denominations[0].TotalStr;
      }

      lbl_denomination_total_title.Visible = _show_total;
      lbl_denomination_total_value.Visible = _show_total;

      
    }

    #endregion

    #region Events

    /// <summary>
    /// Event raised when the user enters an amount in the control
    /// </summary>
    /// <param name="Amount"></param>
    private void uc_input_amount1_OnAmountSelected(decimal Amount)
    {
      if (m_show_all_denominations)
      {
        Int32 _amount;

        if (string.IsNullOrEmpty(uc_input_amount1.txt_amount.Text))
          return;

        if (dgv_common.SelectedRows.Count == 0)
        {
          frm_message.Show(Resource.String("STR_DENOMINATION_UNSELECTED"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this);

          return;
        }

        _amount = (Int32)Amount;

        m_term_denom.Denominations[dgv_common.SelectedRows[0].Index].Quantity = _amount;

        ChangeToNextRowInDataGrid();
        uc_input_amount1.txt_amount.Text = dgv_common.SelectedRows[0].Cells[INDEX_COLUMN_AMOUNT].Value.ToString();
      }
      else
      {
        m_term_denom.Denominations[dgv_common.SelectedRows[0].Index].Total = Amount;
        m_term_denom.Denominations[dgv_common.SelectedRows[0].Index].Quantity = 1;
        dgv_common.Refresh();
        UpdateTotal();
      }
      btn_ok.Enabled = true;

    }

    private void btn_clear_Click(object sender, EventArgs e)
    {
      uc_input_amount1.txt_amount.Text = string.Empty;

      foreach (TerminalCurrencyItem _item in m_term_denom.Denominations)
      {
        _item.Quantity = null;
        if (!this.m_show_all_denominations)
            _item.Total = 0;
      }

      if (dgv_common.Rows.Count > 0)
      {
        dgv_common.Rows[0].Selected = true;
        dgv_common.FirstDisplayedScrollingRowIndex = 0;
      }

      dgv_common.Refresh();
      UpdateTotal();
    }

    private void dgv_common_SelectionChanged(object sender, EventArgs e)
    {
      if (dgv_common.SelectedRows.Count > 0)
      {
        if (dgv_common.SelectedRows.Count > 1)
        {
          uc_input_amount1.txt_amount.Text = dgv_common.SelectedRows[0].Cells[INDEX_COLUMN_AMOUNT].Value.ToString().Trim();
        }
        uc_input_amount1.Focus();
      }
    }

    private void btn_cancel_Click(object sender, EventArgs e)
    {
      Misc.WriteLog("[FORM CLOSE] frm_gh_currency_quantity (cancel)", Log.Type.Message);
      this.Close();
    }

    private void btn_ok_Click(object sender, EventArgs e)
    {
      m_close_ok = true;

      Misc.WriteLog("[FORM CLOSE] frm_gh_currency_quantity (ok)", Log.Type.Message);
      this.Close();
    }

    #endregion

  }
}