﻿namespace WSI.Cashier
{
  partial class uc_gaming_hall_set_pcd_meters
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.pnl_flow_layout = new System.Windows.Forms.Panel();
      this.pb_collect_refill = new System.Windows.Forms.PictureBox();
      this.gb_terminal_config = new WSI.Cashier.Controls.uc_round_panel();
      this.gb_scroll_container = new WSI.Cashier.Controls.uc_round_panel();
      this.uc_set_pcd_meter_model = new WSI.Cashier.uc_set_terminal_meter();
      this.btn_process_changes = new WSI.Cashier.Controls.uc_round_button();
      this.gb_terminals = new WSI.Cashier.Controls.uc_round_panel();
      this.uc_terminal_filter1 = new WSI.Cashier.uc_terminal_filter();
      this.lbl_terminal = new System.Windows.Forms.Label();
      this.btn_select_terminal = new WSI.Cashier.Controls.uc_round_button();
      this.label7 = new WSI.Cashier.Controls.uc_label();
      this.lbl_termminal_title = new WSI.Cashier.Controls.uc_label();
      ((System.ComponentModel.ISupportInitialize)(this.pb_collect_refill)).BeginInit();
      this.gb_terminal_config.SuspendLayout();
      this.gb_scroll_container.SuspendLayout();
      this.gb_terminals.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_flow_layout
      // 
      this.pnl_flow_layout.AutoSize = true;
      this.pnl_flow_layout.Dock = System.Windows.Forms.DockStyle.Top;
      this.pnl_flow_layout.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.pnl_flow_layout.Location = new System.Drawing.Point(0, 0);
      this.pnl_flow_layout.Name = "pnl_flow_layout";
      this.pnl_flow_layout.Size = new System.Drawing.Size(886, 0);
      this.pnl_flow_layout.TabIndex = 59;
      // 
      // pb_collect_refill
      // 
      this.pb_collect_refill.Image = global::WSI.Cashier.Properties.Resources.settingCounter_black;
      this.pb_collect_refill.Location = new System.Drawing.Point(11, 4);
      this.pb_collect_refill.Name = "pb_collect_refill";
      this.pb_collect_refill.Size = new System.Drawing.Size(38, 41);
      this.pb_collect_refill.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
      this.pb_collect_refill.TabIndex = 70;
      this.pb_collect_refill.TabStop = false;
      // 
      // gb_terminal_config
      // 
      this.gb_terminal_config.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.gb_terminal_config.AutoSize = true;
      this.gb_terminal_config.BackColor = System.Drawing.Color.Transparent;
      this.gb_terminal_config.BorderColor = System.Drawing.Color.Empty;
      this.gb_terminal_config.Controls.Add(this.gb_scroll_container);
      this.gb_terminal_config.CornerRadius = 10;
      this.gb_terminal_config.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_terminal_config.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gb_terminal_config.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_terminal_config.HeaderHeight = 35;
      this.gb_terminal_config.HeaderSubText = null;
      this.gb_terminal_config.HeaderText = null;
      this.gb_terminal_config.Location = new System.Drawing.Point(13, 268);
      this.gb_terminal_config.Name = "gb_terminal_config";
      this.gb_terminal_config.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_terminal_config.Size = new System.Drawing.Size(848, 377);
      this.gb_terminal_config.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.gb_terminal_config.TabIndex = 98;
      this.gb_terminal_config.Text = "xTerminalConfig";
      // 
      // gb_scroll_container
      // 
      this.gb_scroll_container.AutoScroll = true;
      this.gb_scroll_container.BackColor = System.Drawing.Color.Transparent;
      this.gb_scroll_container.BorderColor = System.Drawing.Color.Empty;
      this.gb_scroll_container.Controls.Add(this.uc_set_pcd_meter_model);
      this.gb_scroll_container.CornerRadius = 10;
      this.gb_scroll_container.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_scroll_container.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gb_scroll_container.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_scroll_container.HeaderHeight = 0;
      this.gb_scroll_container.HeaderSubText = null;
      this.gb_scroll_container.HeaderText = null;
      this.gb_scroll_container.Location = new System.Drawing.Point(0, 35);
      this.gb_scroll_container.Name = "gb_scroll_container";
      this.gb_scroll_container.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_scroll_container.Size = new System.Drawing.Size(845, 339);
      this.gb_scroll_container.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.WITHOUT_HEAD;
      this.gb_scroll_container.TabIndex = 110;
      // 
      // uc_set_pcd_meter_model
      // 
      this.uc_set_pcd_meter_model.ActionsLocation = new System.Drawing.Point(518, 3);
      this.uc_set_pcd_meter_model.ActionsWidth = 125;
      this.uc_set_pcd_meter_model.BackColor = System.Drawing.Color.Transparent;
      this.uc_set_pcd_meter_model.Description = "xDescription";
      this.uc_set_pcd_meter_model.DescriptionLocation = new System.Drawing.Point(5, 8);
      this.uc_set_pcd_meter_model.DescriptionWidth = 366;
      this.uc_set_pcd_meter_model.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.uc_set_pcd_meter_model.Location = new System.Drawing.Point(3, 3);
      this.uc_set_pcd_meter_model.Name = "uc_set_pcd_meter_model";
      this.uc_set_pcd_meter_model.Size = new System.Drawing.Size(816, 46);
      this.uc_set_pcd_meter_model.TabIndex = 0;
      this.uc_set_pcd_meter_model.Value = "1234567890";
      this.uc_set_pcd_meter_model.ValueLocation = new System.Drawing.Point(680, 3);
      this.uc_set_pcd_meter_model.ValueWidth = 135;
      this.uc_set_pcd_meter_model.Visible = false;
      // 
      // btn_process_changes
      // 
      this.btn_process_changes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_process_changes.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_process_changes.Enabled = false;
      this.btn_process_changes.FlatAppearance.BorderSize = 0;
      this.btn_process_changes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_process_changes.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_process_changes.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_process_changes.Image = null;
      this.btn_process_changes.IsSelected = false;
      this.btn_process_changes.Location = new System.Drawing.Point(742, 657);
      this.btn_process_changes.Name = "btn_process_changes";
      this.btn_process_changes.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_process_changes.Size = new System.Drawing.Size(116, 49);
      this.btn_process_changes.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_process_changes.TabIndex = 109;
      this.btn_process_changes.Text = "XPROCESS";
      this.btn_process_changes.UseVisualStyleBackColor = false;
      this.btn_process_changes.Click += new System.EventHandler(this.btn_process_changes_Click);
      // 
      // gb_terminals
      // 
      this.gb_terminals.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.gb_terminals.BackColor = System.Drawing.Color.Transparent;
      this.gb_terminals.BorderColor = System.Drawing.Color.Empty;
      this.gb_terminals.Controls.Add(this.uc_terminal_filter1);
      this.gb_terminals.Controls.Add(this.lbl_terminal);
      this.gb_terminals.CornerRadius = 10;
      this.gb_terminals.Font = new System.Drawing.Font("Open Sans Semibold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_terminals.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gb_terminals.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_terminals.HeaderHeight = 35;
      this.gb_terminals.HeaderSubText = null;
      this.gb_terminals.HeaderText = null;
      this.gb_terminals.Location = new System.Drawing.Point(12, 83);
      this.gb_terminals.Name = "gb_terminals";
      this.gb_terminals.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_terminals.Size = new System.Drawing.Size(520, 179);
      this.gb_terminals.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.gb_terminals.TabIndex = 98;
      this.gb_terminals.Text = "xTerminals";
      // 
      // uc_terminal_filter1
      // 
      this.uc_terminal_filter1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.uc_terminal_filter1.BackColor = System.Drawing.Color.Transparent;
      this.uc_terminal_filter1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.uc_terminal_filter1.Location = new System.Drawing.Point(0, 34);
      this.uc_terminal_filter1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
      this.uc_terminal_filter1.Name = "uc_terminal_filter1";
      this.uc_terminal_filter1.RightToLeft = System.Windows.Forms.RightToLeft.No;
      this.uc_terminal_filter1.Size = new System.Drawing.Size(520, 143);
      this.uc_terminal_filter1.TabIndex = 1;
      this.uc_terminal_filter1.ValidatingTerminalOrProvider += new WSI.Cashier.uc_terminal_filter.TerminalOrProviderRowValidatingHandler(this.uc_terminal_filter1_ValidatingTerminalOrProvider);
      // 
      // lbl_terminal
      // 
      this.lbl_terminal.AutoSize = true;
      this.lbl_terminal.Location = new System.Drawing.Point(25, 30);
      this.lbl_terminal.Name = "lbl_terminal";
      this.lbl_terminal.Size = new System.Drawing.Size(66, 17);
      this.lbl_terminal.TabIndex = 103;
      this.lbl_terminal.Text = "xTerminal";
      this.lbl_terminal.TextAlign = System.Drawing.ContentAlignment.TopRight;
      // 
      // btn_select_terminal
      // 
      this.btn_select_terminal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_select_terminal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_select_terminal.FlatAppearance.BorderSize = 0;
      this.btn_select_terminal.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_select_terminal.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_select_terminal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_select_terminal.Image = null;
      this.btn_select_terminal.IsSelected = false;
      this.btn_select_terminal.Location = new System.Drawing.Point(742, 219);
      this.btn_select_terminal.Name = "btn_select_terminal";
      this.btn_select_terminal.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_select_terminal.Size = new System.Drawing.Size(116, 41);
      this.btn_select_terminal.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_select_terminal.TabIndex = 107;
      this.btn_select_terminal.Text = "XSELECT TERMINAL";
      this.btn_select_terminal.UseVisualStyleBackColor = false;
      this.btn_select_terminal.Click += new System.EventHandler(this.btn_select_terminal_Click);
      // 
      // label7
      // 
      this.label7.BackColor = System.Drawing.Color.Black;
      this.label7.Font = new System.Drawing.Font("Open Sans Semibold", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.label7.Location = new System.Drawing.Point(3, 47);
      this.label7.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(970, 2);
      this.label7.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.label7.TabIndex = 63;
      // 
      // lbl_termminal_title
      // 
      this.lbl_termminal_title.AutoSize = true;
      this.lbl_termminal_title.BackColor = System.Drawing.Color.Transparent;
      this.lbl_termminal_title.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_termminal_title.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_termminal_title.Location = new System.Drawing.Point(54, 12);
      this.lbl_termminal_title.Name = "lbl_termminal_title";
      this.lbl_termminal_title.Size = new System.Drawing.Size(125, 22);
      this.lbl_termminal_title.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_termminal_title.TabIndex = 61;
      this.lbl_termminal_title.Text = "xSetPcdMeters";
      // 
      // uc_gaming_hall_set_pcd_meters
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.Color.LightGray;
      this.Controls.Add(this.gb_terminal_config);
      this.Controls.Add(this.btn_process_changes);
      this.Controls.Add(this.gb_terminals);
      this.Controls.Add(this.btn_select_terminal);
      this.Controls.Add(this.pb_collect_refill);
      this.Controls.Add(this.label7);
      this.Controls.Add(this.lbl_termminal_title);
      this.Controls.Add(this.pnl_flow_layout);
      this.Name = "uc_gaming_hall_set_pcd_meters";
      this.Size = new System.Drawing.Size(886, 736);
      this.Validating += new System.ComponentModel.CancelEventHandler(this.uc_gaming_hall_set_pcd_meters_Validating);
      ((System.ComponentModel.ISupportInitialize)(this.pb_collect_refill)).EndInit();
      this.gb_terminal_config.ResumeLayout(false);
      this.gb_scroll_container.ResumeLayout(false);
      this.gb_terminals.ResumeLayout(false);
      this.gb_terminals.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private uc_terminal_filter uc_terminal_filter1;
    private System.Windows.Forms.Panel pnl_flow_layout;
    private WSI.Cashier.Controls.uc_label lbl_termminal_title;
    private WSI.Cashier.Controls.uc_label label7;
    private System.Windows.Forms.PictureBox pb_collect_refill;
    private WSI.Cashier.Controls.uc_round_panel gb_terminals;
    private System.Windows.Forms.Label lbl_terminal;
    private WSI.Cashier.Controls.uc_round_button btn_select_terminal;
    private WSI.Cashier.Controls.uc_round_panel gb_terminal_config;
    private WSI.Cashier.Controls.uc_round_button btn_process_changes;
    private uc_set_terminal_meter uc_set_pcd_meter_model;
    private Controls.uc_round_panel gb_scroll_container;

  }
}
