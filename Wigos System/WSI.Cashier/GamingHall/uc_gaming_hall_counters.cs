﻿//------------------------------------------------------------------------------
// Copyright © 2007-2015 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_gaming_hall_counters.cs
// 
//   DESCRIPTION: User control with counters of a Terminal
//
//        AUTHOR: Francisco Vicente.
// 
// CREATION DATE: 16-OCT-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 16-OCT-2015 FAV     First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using WSI.Common;

namespace WSI.Cashier.GamingHall
{
  public partial class uc_gaming_hall_counters : UserControl
  {
    #region Attributes

    private const Int32 MAX_ALLOWED_COUNTER_LENGTH = 9;
    private TerminalTypes m_terminal_type = TerminalTypes.UNKNOWN;

    #endregion

    #region Constructor

    public uc_gaming_hall_counters()
    {
      InitializeComponent();

      InitControl();
    }

    #endregion

    #region Properties

    public TerminalTypes TerminalType
    {
      set
      {
        m_terminal_type = value;
        SetMode(m_terminal_type);
      }
      get
      {
        return m_terminal_type;
      }
    }

    public Int32 CashIn
    {
      get
      {

        int _value = 0;
        int.TryParse(txt_cash_in_counter.Text, out _value);
        return _value;
      }
      set
      {
        txt_cash_in_counter.Text = value.ToString();
      }
    }

    public Int32 CashOut
    {
      get
      {
        int _value = 0;
        int.TryParse(txt_cash_out_counter.Text, out _value);
        return _value;
      }
      set
      {
        txt_cash_out_counter.Text = value.ToString();
      }
    }

    public Int32 HandPay
    {
      get
      {
        int _value = 0;
        int.TryParse(txt_hand_pay_counter.Text, out _value);
        return _value;
      }
      set
      {
        txt_hand_pay_counter.Text = value.ToString();
      }
    }

    public String XMLCounters
    {
      get
      {
        Dictionary<String, Int32> _items = new Dictionary<String, Int32>();
        _items.Add("CashIn", this.CashIn);
        _items.Add("CashOut", this.CashOut);
        _items.Add("HandPay", this.HandPay);

        return XMLDictionary.DictionaryToXML<String, Int32>(_items);
      }
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Initialize objects and resources (NLS strings, images, etc)
    /// </summary>
    private void InitControl()
    {
      txt_cash_in_counter.MaxLength = MAX_ALLOWED_COUNTER_LENGTH;
      txt_cash_out_counter.MaxLength = MAX_ALLOWED_COUNTER_LENGTH;
      txt_hand_pay_counter.MaxLength = MAX_ALLOWED_COUNTER_LENGTH;

      txt_cash_in_counter.KeyPress += Validation_KeyPress;
      txt_cash_out_counter.KeyPress += Validation_KeyPress;
      txt_hand_pay_counter.KeyPress += Validation_KeyPress;
    }

    private void SetMode(TerminalTypes TerminalType)
    {
      bool _is_offline = (TerminalType == TerminalTypes.OFFLINE);

      lbl_cash_in_title.Enabled = _is_offline;
      txt_cash_in_counter.Enabled = _is_offline;
      lbl_cash_out_title.Enabled = _is_offline;
      txt_cash_out_counter.Enabled = _is_offline;
      lbl_hand_pay_title.Enabled = _is_offline;
      txt_hand_pay_counter.Enabled = _is_offline;
    }

    #endregion

    #region publicMethods

    public void RefreshAmount()
    {
      this.CashIn = 0;
      this.CashOut = 0;
      this.HandPay = 0;
    }

    public Boolean HasPendingChanges()
    {
      return this.CashIn != 0 ||
        this.CashOut != 0 ||
        this.HandPay != 0;
    }

    #endregion

    #region Events

    /// <summary>
    /// It validates only numbers in counters, for each key pressed in the textbox
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void Validation_KeyPress(object sender, KeyPressEventArgs e)
    {
      int _value = -1;
      char _char;

      _char = e.KeyChar;

      if (_char == '\b')
        return;

      e.Handled = !(int.TryParse(_char.ToString(), out _value));
    }
    #endregion
  }
}
