﻿namespace WSI.Cashier
{
  partial class uc_gaming_hall_collect
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.txt_amount = new WSI.Cashier.Controls.uc_round_textbox();
      this.btn_denominations = new WSI.Cashier.Controls.uc_round_button();
      this.lbl_box_title = new WSI.Cashier.Controls.uc_label();
      this.SuspendLayout();
      // 
      // txt_amount
      // 
      this.txt_amount.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txt_amount.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_amount.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_amount.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_amount.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_amount.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_amount.CornerRadius = 5;
      this.txt_amount.Cursor = System.Windows.Forms.Cursors.IBeam;
      this.txt_amount.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_amount.Location = new System.Drawing.Point(100, 4);
      this.txt_amount.MaxLength = 18;
      this.txt_amount.Multiline = false;
      this.txt_amount.Name = "txt_amount";
      this.txt_amount.PasswordChar = '\0';
      this.txt_amount.ReadOnly = true;
      this.txt_amount.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_amount.SelectedText = "";
      this.txt_amount.SelectionLength = 0;
      this.txt_amount.SelectionStart = 0;
      this.txt_amount.Size = new System.Drawing.Size(181, 40);
      this.txt_amount.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_amount.TabIndex = 17;
      this.txt_amount.TabStop = false;
      this.txt_amount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      this.txt_amount.UseSystemPasswordChar = false;
      this.txt_amount.WaterMark = null;
      this.txt_amount.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      this.txt_amount.Enter += new System.EventHandler(this.txt_amount_Enter);
      // 
      // btn_denominations
      // 
      this.btn_denominations.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_denominations.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(47)))), ((int)(((byte)(50)))));
      this.btn_denominations.FlatAppearance.BorderSize = 0;
      this.btn_denominations.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_denominations.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_denominations.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_denominations.Image = null;
      this.btn_denominations.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.btn_denominations.IsSelected = false;
      this.btn_denominations.Location = new System.Drawing.Point(289, 4);
      this.btn_denominations.Name = "btn_denominations";
      this.btn_denominations.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_denominations.Size = new System.Drawing.Size(70, 40);
      this.btn_denominations.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.TERTIARY;
      this.btn_denominations.TabIndex = 0;
      this.btn_denominations.Text = "XDENO";
      this.btn_denominations.UseVisualStyleBackColor = false;
      this.btn_denominations.Click += new System.EventHandler(this.btn_hopper_denominations_Click);
      // 
      // lbl_box_title
      // 
      this.lbl_box_title.BackColor = System.Drawing.Color.Transparent;
      this.lbl_box_title.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_box_title.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_box_title.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.lbl_box_title.Location = new System.Drawing.Point(3, 5);
      this.lbl_box_title.Name = "lbl_box_title";
      this.lbl_box_title.Size = new System.Drawing.Size(91, 35);
      this.lbl_box_title.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_box_title.TabIndex = 5;
      this.lbl_box_title.Text = "xBoxTitle";
      this.lbl_box_title.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // uc_gaming_hall_collect
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.SystemColors.Control;
      this.Controls.Add(this.txt_amount);
      this.Controls.Add(this.btn_denominations);
      this.Controls.Add(this.lbl_box_title);
      this.Name = "uc_gaming_hall_collect";
      this.Size = new System.Drawing.Size(360, 44);
      this.ResumeLayout(false);

    }

    #endregion

    private WSI.Cashier.Controls.uc_label lbl_box_title;
    private WSI.Cashier.Controls.uc_round_button btn_denominations;
    private WSI.Cashier.Controls.uc_round_textbox txt_amount;
  }
}
