﻿namespace WSI.Cashier
{
  partial class frm_gaming_hall_refill
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.uc_gaming_hall_collect1 = new WSI.Cashier.uc_gaming_hall_collect();
      this.btn_close = new WSI.Cashier.Controls.uc_round_button();
      this.btn_refill = new WSI.Cashier.Controls.uc_round_button();
      this.uc_terminal_filter1 = new WSI.Cashier.uc_terminal_filter();
      this.gb_teminals = new WSI.Cashier.Controls.uc_round_panel();
      this.pnl_data.SuspendLayout();
      this.gb_teminals.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.Controls.Add(this.uc_gaming_hall_collect1);
      this.pnl_data.Controls.Add(this.gb_teminals);
      this.pnl_data.Controls.Add(this.btn_close);
      this.pnl_data.Controls.Add(this.btn_refill);
      this.pnl_data.Size = new System.Drawing.Size(624, 421);
      // 
      // uc_gaming_hall_collect1
      // 
      this.uc_gaming_hall_collect1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.uc_gaming_hall_collect1.Enabled = false;
      this.uc_gaming_hall_collect1.Location = new System.Drawing.Point(107, 261);
      this.uc_gaming_hall_collect1.Margin = new System.Windows.Forms.Padding(0);
      this.uc_gaming_hall_collect1.Name = "uc_gaming_hall_collect1";
      this.uc_gaming_hall_collect1.ProviderName = "";
      this.uc_gaming_hall_collect1.Size = new System.Drawing.Size(380, 48);
      this.uc_gaming_hall_collect1.TabIndex = 3;
      this.uc_gaming_hall_collect1.TerminalBoxType = WSI.Common.TerminalBoxType.None;
      this.uc_gaming_hall_collect1.TerminalName = "";
      // 
      // btn_close
      // 
      this.btn_close.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_close.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_close.FlatAppearance.BorderSize = 0;
      this.btn_close.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_close.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_close.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_close.Image = null;
      this.btn_close.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.btn_close.Location = new System.Drawing.Point(277, 342);
      this.btn_close.Name = "btn_close";
      this.btn_close.Size = new System.Drawing.Size(155, 60);
      this.btn_close.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_close.TabIndex = 0;
      this.btn_close.Text = "XCLOSE";
      this.btn_close.UseVisualStyleBackColor = false;
      this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
      // 
      // btn_refill
      // 
      this.btn_refill.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_refill.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_refill.FlatAppearance.BorderSize = 0;
      this.btn_refill.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_refill.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_refill.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_refill.Image = null;
      this.btn_refill.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.btn_refill.Location = new System.Drawing.Point(450, 342);
      this.btn_refill.Name = "btn_refill";
      this.btn_refill.Size = new System.Drawing.Size(155, 60);
      this.btn_refill.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_refill.TabIndex = 1;
      this.btn_refill.Text = "XREFILL";
      this.btn_refill.UseVisualStyleBackColor = false;
      this.btn_refill.Click += new System.EventHandler(this.btn_fill_Click);
      // 
      // uc_terminal_filter1
      // 
      this.uc_terminal_filter1.BackColor = System.Drawing.Color.Transparent;
      this.uc_terminal_filter1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.uc_terminal_filter1.Location = new System.Drawing.Point(-1, 35);
      this.uc_terminal_filter1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
      this.uc_terminal_filter1.Name = "uc_terminal_filter1";
      this.uc_terminal_filter1.RightToLeft = System.Windows.Forms.RightToLeft.No;
      this.uc_terminal_filter1.Size = new System.Drawing.Size(587, 195);
      this.uc_terminal_filter1.TabIndex = 2;
      this.uc_terminal_filter1.TerminalChanged += new WSI.Cashier.uc_terminal_filter.TerminalOrProviderChangedEventHandler(this.uc_terminal_filter1_TerminalChanged);
      this.uc_terminal_filter1.ProviderChanged += new WSI.Cashier.uc_terminal_filter.TerminalOrProviderChangedEventHandler(this.uc_terminal_filter1_ProviderChanged);
      // 
      // gb_teminals
      // 
      this.gb_teminals.BackColor = System.Drawing.Color.Transparent;
      this.gb_teminals.Controls.Add(this.uc_terminal_filter1);
      this.gb_teminals.CornerRadius = 10;
      this.gb_teminals.Font = new System.Drawing.Font("Montserrat", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_teminals.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gb_teminals.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_teminals.HeaderHeight = 35;
      this.gb_teminals.HeaderSubText = null;
      this.gb_teminals.HeaderText = null;
      this.gb_teminals.Location = new System.Drawing.Point(20, 18);
      this.gb_teminals.Name = "gb_teminals";
      this.gb_teminals.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_teminals.Size = new System.Drawing.Size(585, 308);
      this.gb_teminals.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.gb_teminals.TabIndex = 99;
      this.gb_teminals.Text = "xTerminals";
      // 
      // frm_gaming_hall_refill
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(624, 476);
      this.Name = "frm_gaming_hall_refill";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Refill Hopper";
      this.pnl_data.ResumeLayout(false);
      this.gb_teminals.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private uc_gaming_hall_collect uc_gaming_hall_collect1;
    private WSI.Cashier.Controls.uc_round_button btn_refill;
    private WSI.Cashier.Controls.uc_round_button btn_close;
    private uc_terminal_filter uc_terminal_filter1;
    private Controls.uc_round_panel gb_teminals;

  }
}