﻿namespace WSI.Cashier.GamingHall
{
  partial class frm_gh_set_meters
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.btn_ok = new WSI.Cashier.Controls.uc_round_button();
      this.btn_cancel = new WSI.Cashier.Controls.uc_round_button();
      this.pnl_container = new System.Windows.Forms.Panel();
      this.uc_set_terminal_meter_model = new WSI.Cashier.uc_set_terminal_meter();
      this.pnl_data.SuspendLayout();
      this.pnl_container.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.Controls.Add(this.pnl_container);
      this.pnl_data.Controls.Add(this.btn_ok);
      this.pnl_data.Controls.Add(this.btn_cancel);
      this.pnl_data.Size = new System.Drawing.Size(664, 412);
      // 
      // btn_ok
      // 
      this.btn_ok.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_ok.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_ok.Enabled = false;
      this.btn_ok.FlatAppearance.BorderSize = 0;
      this.btn_ok.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_ok.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_ok.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_ok.Image = null;
      this.btn_ok.IsSelected = false;
      this.btn_ok.Location = new System.Drawing.Point(476, 340);
      this.btn_ok.Name = "btn_ok";
      this.btn_ok.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_ok.Size = new System.Drawing.Size(155, 60);
      this.btn_ok.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_ok.TabIndex = 5;
      this.btn_ok.Text = "XOK";
      this.btn_ok.UseVisualStyleBackColor = false;
      this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
      // 
      // btn_cancel
      // 
      this.btn_cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_cancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_cancel.FlatAppearance.BorderSize = 0;
      this.btn_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_cancel.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_cancel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_cancel.Image = null;
      this.btn_cancel.IsSelected = false;
      this.btn_cancel.Location = new System.Drawing.Point(306, 340);
      this.btn_cancel.Name = "btn_cancel";
      this.btn_cancel.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_cancel.Size = new System.Drawing.Size(155, 60);
      this.btn_cancel.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_cancel.TabIndex = 6;
      this.btn_cancel.Text = "XCANCEL";
      this.btn_cancel.UseVisualStyleBackColor = false;
      this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
      // 
      // pnl_container
      // 
      this.pnl_container.AutoScroll = true;
      this.pnl_container.Controls.Add(this.uc_set_terminal_meter_model);
      this.pnl_container.Location = new System.Drawing.Point(3, 6);
      this.pnl_container.Margin = new System.Windows.Forms.Padding(3, 3, 0, 3);
      this.pnl_container.Name = "pnl_container";
      this.pnl_container.Size = new System.Drawing.Size(652, 326);
      this.pnl_container.TabIndex = 7;
      this.pnl_container.VisibleChanged += new System.EventHandler(this.pnl_container_VisibleChanged);
      // 
      // uc_set_terminal_meter_model
      // 
      this.uc_set_terminal_meter_model.ActionsLocation = new System.Drawing.Point(234, 3);
      this.uc_set_terminal_meter_model.ActionsWidth = 100;
      this.uc_set_terminal_meter_model.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.uc_set_terminal_meter_model.Description = "xDescription";
      this.uc_set_terminal_meter_model.DescriptionLocation = new System.Drawing.Point(3, 10);
      this.uc_set_terminal_meter_model.DescriptionWidth = 225;
      this.uc_set_terminal_meter_model.Location = new System.Drawing.Point(3, 4);
      this.uc_set_terminal_meter_model.Margin = new System.Windows.Forms.Padding(3, 3, 0, 3);
      this.uc_set_terminal_meter_model.Name = "uc_set_terminal_meter_model";
      this.uc_set_terminal_meter_model.Size = new System.Drawing.Size(625, 46);
      this.uc_set_terminal_meter_model.TabIndex = 3;
      this.uc_set_terminal_meter_model.Value = "1234567890";
      this.uc_set_terminal_meter_model.ValueLocation = new System.Drawing.Point(450, 3);
      this.uc_set_terminal_meter_model.ValueWidth = 175;
      this.uc_set_terminal_meter_model.Visible = false;
      // 
      // frm_gh_set_meters
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(664, 467);
      this.Name = "frm_gh_set_meters";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Style = WSI.Cashier.Controls.frm_base.BaseFormStyle.PRINCIPAL;
      this.Text = "frm_gh_set_sas_meters";
      this.pnl_data.ResumeLayout(false);
      this.pnl_container.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private Controls.uc_round_button btn_ok;
    private Controls.uc_round_button btn_cancel;
    private System.Windows.Forms.Panel pnl_container;
    private uc_set_terminal_meter uc_set_terminal_meter_model;
  }
}