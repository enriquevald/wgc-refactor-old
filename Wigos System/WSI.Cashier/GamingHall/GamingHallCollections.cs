﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : GamingHallCollections.cs
// 
//   DESCRIPTION : It has classes to use in the DataGrid of the Collect Form
//
// REVISION HISTORY
// 
// Date        Author Description
// ----------- ------ -------------------------------------------------------------------------------------
// 15-JAN-2016 FAV    First release.
//---------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WSI.Common;
using WSI.Common.GamingHall;

namespace WSI.Cashier.GamingHall
{
  public class GamingHallCollections
  {
    //------------------------------------------------------------------------------
    // PURPOSE : Get SqlCommand.
    //
    //  PARAMS :
    //      - INPUT :
    //          - GridMode
    //          - ProviderId
    //          - TerminalId
    //          - Adding
    //          - TerminalsCollectedRefilled
    //          - Trx
    //
    //      - OUTPUT : DataTable with Information about terminals collected and refilled
    //
    // RETURNS :
    //
    public Boolean GetTerminalsToShowInGrid(CollectionGridMode GridMode, String ProviderId, Int32 TerminalId,
                                            out List<GamingHallTerminalCollectRefill> TerminalsCollectedRefilled, SqlTransaction Trx)
    {
      StringBuilder _sb;
      _sb = new StringBuilder();
      TerminalsCollectedRefilled = new List<GamingHallTerminalCollectRefill>();

      try
      {
        _sb.AppendLine("SELECT            " + SetMcDatetime(GridMode) + "                                                            ");
        _sb.AppendLine("                , TE_PROVIDER_ID                                                                             ");
        _sb.AppendLine("                , TE_NAME                                                                                    ");
        _sb.AppendLine("                , MC_STATUS AS MC_STATUS                                                                     ");
        _sb.AppendLine("                , MC_REFILLED_HOPPER AS HOPPER                                                               ");
        _sb.AppendLine("                , (MC_COLLECTED_BILL_AMOUNT + MC_COLLECTED_COIN_AMOUNT) AS DROPBOX                           ");
        _sb.AppendLine("                , TE_TERMINAL_ID                                                                             ");
        _sb.AppendLine("                , MC_COLLECTION_ID                                                                           ");
        _sb.AppendLine("                , MC_CASHIER_SESSION_ID                                                                      ");
        _sb.AppendLine("                , MC_LAST_UPDATED                                                                            ");
        _sb.AppendLine("                , MC_FINAL_METERS                                                                            ");
        _sb.AppendLine("       FROM       MONEY_COLLECTIONS                                                                          ");
        _sb.AppendLine(" INNER JOIN       TERMINALS                                                                                  ");
        _sb.AppendLine("         ON       TE_TERMINAL_ID = MC_TERMINAL_ID                                                            ");
        _sb.AppendLine("      WHERE                                                                                                  ");

        switch (GridMode)
        {
          case CollectionGridMode.OPEN:
            _sb.AppendLine("                  MC_STATUS = @pStatus0                ");
            break;
          case CollectionGridMode.PENDING:
            _sb.AppendLine("                  MC_STATUS = @pStatus1           ");
            break;
          case CollectionGridMode.COLLECTED:
            _sb.AppendLine("                  MC_STATUS IN (@pStatus2,@pStatus3) AND         ");
            _sb.AppendLine("                  MC_COLLECTION_DATETIME >= @pCollectionDate     ");
            break;
        }

        if (TerminalId > 0)
        {
          _sb.AppendLine("        AND       TE_TERMINAL_ID = @pTerminalId ");
        }

        if (!String.IsNullOrEmpty(ProviderId))
        {
          _sb.AppendLine("        AND       TE_PROVIDER_ID = @pProviderId ");
        }

        _sb.AppendLine("        AND       TE_TERMINAL_TYPE = @pTermType");
        _sb.AppendLine("   ORDER BY       MC_STATUS_DATETIME DESC");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pGridMode", SqlDbType.Int).Value = (Int16)GridMode;
          _cmd.Parameters.Add("@pStatus0", SqlDbType.Int).Value = TITO_MONEY_COLLECTION_STATUS.OPEN;
          _cmd.Parameters.Add("@pStatus1", SqlDbType.Int).Value = TITO_MONEY_COLLECTION_STATUS.PENDING;
          _cmd.Parameters.Add("@pStatus2", SqlDbType.Int).Value = TITO_MONEY_COLLECTION_STATUS.CLOSED_BALANCED;
          _cmd.Parameters.Add("@pStatus3", SqlDbType.Int).Value = TITO_MONEY_COLLECTION_STATUS.CLOSED_IN_IMBALANCE;
          _cmd.Parameters.Add("@pType", SqlDbType.Int).Value = CASHIER_MOVEMENT.TERMINAL_REFILL_HOPPER;
          _cmd.Parameters.Add("@pCollectionDate", SqlDbType.DateTime).Value = Misc.TodayOpening();
          _cmd.Parameters.Add("@pTermType", SqlDbType.Int).Value = (Int32)TerminalTypes.SAS_HOST;
          _cmd.Parameters.Add("@pCashierSessionId", SqlDbType.Int).Value = Cashier.SessionId;

          if (TerminalId > 0)
          {
            _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
          }

          if (!String.IsNullOrEmpty(ProviderId))
          {
            _cmd.Parameters.Add("@pProviderId", SqlDbType.NVarChar, 50).Value = ProviderId;
          }

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            while (_reader.Read())
            {
              TerminalDenominations _denominations_collect = new TerminalDenominations(TerminalBoxType.Box, CurrencyExchange.GetNationalCurrency(), CurrencyExchangeType.CURRENCY);
              TerminalDenominations _denominations_refill = new TerminalDenominations(TerminalBoxType.Hopper, CurrencyExchange.GetNationalCurrency(), CurrencyExchangeType.CURRENCY);

              GamingHallTerminalCollectRefill _item = new GamingHallTerminalCollectRefill((Int32)_reader[6])
              {
                DateTime = _reader.IsDBNull(0) ? (DateTime?)null : (DateTime)_reader[0],
                ProviderName = _reader.IsDBNull(1) ? String.Empty : (String)_reader[1],
                TerminalName = _reader.IsDBNull(2) ? String.Empty : (String)_reader[2],
                StatusMoneyCollectionCollected = _reader.IsDBNull(3) ? (TITO_MONEY_COLLECTION_STATUS?)null : (TITO_MONEY_COLLECTION_STATUS)_reader[3],
                MoneyCollectionId = (Int64)_reader[7],
                CashierSessionId = (Int64)_reader[8],
                LastUpdated = _reader.IsDBNull(9) ? (DateTime?)null : (DateTime)_reader[9],
                NewMeters = _reader.IsDBNull(10) ? null : (String)_reader[10]
              };

              _denominations_collect.LoadDenominationsCollected(_item.MoneyCollectionId);
              _item.DenominationsCollect = _denominations_collect;

              _denominations_refill.Denominations = (_reader.IsDBNull(4) || _reader[4].ToString() == "<dictionary />" ? null : XMLList.XMLToList<TerminalCurrencyItem>(_reader[4].ToString()));
              _item.DenominationsRefill = _denominations_refill;


              TerminalsCollectedRefilled.Add(_item);
            }

            return true;
          }
        }
      }
      catch (SqlException _sql_ex)
      {
        if (_sql_ex.Number != -2)
        {
          Log.Warning(" *** Sql Exception: Number: " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message.ToString() + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Sets the date as selected by option button
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS : Date 
    //
    //   NOTES :
    // 
    private string SetMcDatetime(CollectionGridMode gridMode)
    {
      switch (gridMode)
      {
        case CollectionGridMode.PENDING:
          return "MC_EXTRACTION_DATETIME AS MC_STATUS_DATETIME";
        case CollectionGridMode.COLLECTED:
          return "MC_COLLECTION_DATETIME  AS MC_STATUS_DATETIME";
        default:
          return "MC_DATETIME  AS MC_STATUS_DATETIME";
      }
    }

  }

  public enum CollectionGridMode
  {
    OPEN = 0,
    PENDING = 1,
    COLLECTED = 2
  }

  public enum MCGridStatus
  {
    NONE = -1,

    OPEN = 0,

    PENDING = 1,
    NOT_READED = 2,

    CLOSED_IN_IMBALANCE = 3,
    CLOSED_BALANCED = 4,
  }

  public class GamingHallTerminalCollectRefill
  {
    #region Attributes

    private Int32 m_terminal_id;
    private TerminalCollectRefill m_terminal_collect_refill;

    #endregion

    #region Constructor

    public GamingHallTerminalCollectRefill(Int32 TerminalId)
    {
      m_terminal_id = TerminalId;
      m_terminal_collect_refill = new TerminalCollectRefill(TerminalId);
    }

    #endregion

    #region Properties

    public MCGridStatus StatusGrid { get; set; }

    public DateTime? DateTime { get; set; }

    public String ProviderName { get; set; }

    public String TerminalName { get; set; }

    public TITO_MONEY_COLLECTION_STATUS? StatusMoneyCollectionCollected { get; set; }

    public TerminalDenominations DenominationsCollect { get; set; }

    public TerminalDenominations DenominationsRefill { get; set; }

    public Int32 TerminalId
    {
      get
      {
        return m_terminal_id;
      }
    }

    public Int64 MoneyCollectionId { get; set; }

    public Int64 CashierSessionId { get; set; }

    public DateTime? LastUpdated { get; set; }

    public String NewMeters { get; set; }

    public String EditableMeters
    {
      get { return !String.IsNullOrEmpty(this.NewMeters) ? this.NewMeters : m_terminal_collect_refill.CurrentMeters; }
    }

    public TerminalCollectRefill TerminalCollectRefill
    {
      get
      {
        m_terminal_collect_refill.TerminalName = this.TerminalName;
        m_terminal_collect_refill.MoneyCollectionId = this.MoneyCollectionId;
        m_terminal_collect_refill.ProviderName = this.ProviderName;
        m_terminal_collect_refill.DenominationsCollect = this.DenominationsCollect;
        m_terminal_collect_refill.DenominationsRefill = this.DenominationsRefill;
        m_terminal_collect_refill.CurrentMeters = this.EditableMeters;

        return m_terminal_collect_refill;
      }
      set
      {
        if (m_terminal_id == value.TerminalId)
        {
          m_terminal_collect_refill = (TerminalCollectRefill)value.Clone();

          if (m_terminal_collect_refill.MoneyCollectionInfoCollect != null)
          {
            this.StatusMoneyCollectionCollected = m_terminal_collect_refill.MoneyCollectionInfoCollect.Status;
          }
        }
        else
        {
          throw new Exception("Invalid call to GamingHallTerminalCollectRefill.TerminalCollectRefill, you cannot set this property for another TerminalId");
        }
      }
    }

    #endregion

  }
}