﻿//------------------------------------------------------------------------------
// Copyright © 2007-2008 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_gaming_hall_refill.cs
//  
//   DESCRIPTION: Implements the class uc_gaming_hall (Collect, fill and read terminal meters.)
// 
//        AUTHOR Enric Tomas
// 
// CREATION DATE: 26-OCT-2015
// 
// REVISION HISTORY:
// 
// Date        Author     Description
//------------------------------------------------------------------------------
// 26-OCT-2015 ETP        Inital Draft.
// 09-FEB-2016 DDS        Bug 8998 :Cajero nuevo diseño-Salones: No es posible realizar una reposición
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WSI.Common;
using WSI.Common.GamingHall;
using WSI.Cashier.Controls;

namespace WSI.Cashier
{
  public partial class frm_gaming_hall_refill : frm_base
  {
    #region Attributes

    private TerminalCollectRefill m_terminal_refill_selected;
    private Boolean m_is_session_open;

    #endregion

    #region constructor
    public frm_gaming_hall_refill()
    {
      InitializeComponent();

      InitializeControlResources();
     
    }

    #endregion //constructor

    #region Public Methods

    public void Show(Form Parent)
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_gaming_gall_refill", Log.Type.Message);

      frm_yesno form_yes_no;
      form_yes_no = new frm_yesno();

      try
      {
        form_yes_no.Opacity = 0.6;
        form_yes_no.Show(Parent);

        LoadTerminalFilters();

        if (WindowManager.StartUpMode == WindowManager.StartUpModes.Explorer)
        {
          this.Location = new Point(WindowManager.GetFormCenterLocation(form_yes_no).X - (this.Width / 2)
                                  , WindowManager.GetFormCenterLocation(form_yes_no).Y - (this.Height / 2));
        }

        this.ShowDialog(form_yes_no);

        Misc.WriteLog("[FORM CLOSE] frm_gaming_hall_refill (show)", Log.Type.Message);
        this.Close();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {
        form_yes_no.Dispose();
      }
    } // Show

    #endregion //PublicMethods

    #region Private Methods

    private bool RefillHoppers(out VoucherTerminalsRefill Voucher)
    {
      TerminalCashierCollection _terminal_cashier_collection;
      List<TerminalCollectRefill> _terminals_to_refill;
      List<TerminalCollectRefill> _terminals_with_error;
      Voucher = null;

      if (m_terminal_refill_selected == null)
        return false;

      m_terminal_refill_selected.DenominationsRefill = uc_gaming_hall_collect1.TerminalDenominationsSelected;
      
      _terminals_to_refill = new List<TerminalCollectRefill>();
      _terminals_to_refill.Add (m_terminal_refill_selected);

      _terminal_cashier_collection = new TerminalCashierCollection(Cashier.CashierSessionInfo());
      return _terminal_cashier_collection.TerminalsRefillHopper(ref _terminals_to_refill, out _terminals_with_error, out Voucher);
    }

    private  void LoadTerminalFilters()
    {
      StringBuilder _where;

      _where = new StringBuilder();
      _where.AppendLine("    WHERE   TE_ACTIVE = 1                       ");
      _where.AppendLine("      AND   TE_STATUS = " + (int)TerminalStatus.ACTIVE);
      _where.AppendLine("     AND    TE_TERMINAL_TYPE = " + (Int32)TerminalTypes.SAS_HOST);

      uc_terminal_filter1.InitControls("", _where.ToString(), true, true);
      this.uc_terminal_filter1_TerminalChanged(); // Select at least first Terminal 
    }

    private void InitializeControlResources()
    {
      this.FormTitle = Resource.String("STR_LBL_REFILL");

      this.btn_close.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CLOSE");
      this.btn_refill.Text = Resource.String("STR_FRM_CONTAINER_BTN_FILL");
      this.gb_teminals.HeaderText = Resource.String("STR_UC_GAMINGHALL_TERMINALS"); // "Máquinas";

      uc_gaming_hall_collect1.TerminalBoxType = TerminalBoxType.Hopper;

      m_is_session_open = (CashierBusinessLogic.ReadCashierStatus(Cashier.SessionId) == CASHIER_STATUS.OPEN);
      btn_refill.Enabled = m_is_session_open;
      uc_gaming_hall_collect1.Enabled = m_is_session_open;
    }

    private void CheckNewTerminalSelection()
    {
      Boolean _continue = true;

      if (m_terminal_refill_selected != null
      && m_terminal_refill_selected.TerminalId != uc_terminal_filter1.TerminalSelected()
      && !uc_gaming_hall_collect1.IsEmpty)
      {
        string _text;
        _text = Resource.String("STR_TERMINAL_CHANGES_CANCEL");
        _text = _text.Replace("\\r\\n", "\r\n");
        _continue = (frm_message.Show(_text, uc_terminal_filter1.TerminalSelectedName(), MessageBoxButtons.YesNo,
                                       MessageBoxIcon.Question, this.ParentForm, true) == DialogResult.OK);
      }

      if (_continue)
      {
        if (m_terminal_refill_selected != null && m_terminal_refill_selected.TerminalId != uc_terminal_filter1.TerminalSelected())
        {
          uc_gaming_hall_collect1.Clear();
          m_terminal_refill_selected = null;
        }

        m_terminal_refill_selected = new TerminalCollectRefill(uc_terminal_filter1.TerminalSelected())
        {
          ProviderName = uc_terminal_filter1.ProviderSelected(),
          TerminalName = uc_terminal_filter1.TerminalSelectedName(),
        };
      }
      else
      {
        // Returns to the same position
        uc_terminal_filter1.SelectTerminal(m_terminal_refill_selected.TerminalId);
      }
    
    }

    #endregion //Private Methods

    #region Event Handling

    private void uc_terminal_filter1_TerminalChanged()
    {
      try
      {
        uc_gaming_hall_collect1.Enabled = (uc_terminal_filter1.TerminalSelected() > 0 && m_is_session_open);

        if (uc_gaming_hall_collect1.Enabled)
        {
          // check same terminal
          if (m_terminal_refill_selected != null
              && m_terminal_refill_selected.TerminalId == uc_terminal_filter1.TerminalSelected())
          {
            return;
          }
          uc_gaming_hall_collect1.TerminalName = uc_terminal_filter1.TerminalSelectedName(); 
          CheckNewTerminalSelection();
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    }

    private void uc_terminal_filter1_ProviderChanged()
    {
      try
      {
        if (uc_gaming_hall_collect1.Enabled)
        {
          // Check same provider
          if (m_terminal_refill_selected != null
              && m_terminal_refill_selected.ProviderName == uc_terminal_filter1.ProviderSelected())
             
          {           
            return;
          }
          uc_gaming_hall_collect1.ProviderName = uc_terminal_filter1.ProviderSelected(); 
          CheckNewTerminalSelection();
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    }

    private void btn_fill_Click(object sender, EventArgs e)
    {
      VoucherTerminalsRefill _voucher;

      try
      {
        Int32 _id_terminal = uc_terminal_filter1.TerminalSelected();

        if (uc_gaming_hall_collect1.TerminalDenominationsSelected == null || uc_gaming_hall_collect1.TerminalDenominationsSelected.Total == 0)
        {
          frm_message.Show(Resource.String("STR_ERROR_EMPTY_HOPPER"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this.ParentForm);
          return;
        }
        else
        {
          if (RefillHoppers (out _voucher))
          {
            VoucherPrint.Print(_voucher);

            Misc.WriteLog("[FORM CLOSE] frm_gaming_hall_refill (fill)", Log.Type.Message);
            this.Close();
          }
          else
          {
            throw new Exception("The refill operation was not completed successfully");
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        frm_message.Show(Resource.String("STR_ERROR_REFILLING"), Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, MessageBoxIcon.Error, this.ParentForm);
      }
    }

    private void btn_close_Click(object sender, EventArgs e)
    {
      Misc.WriteLog("[FORM CLOSE] frm_gaming_hall_refill (close)", Log.Type.Message);
      this.Close();
    }

    #endregion //Event Handling

  }
}
