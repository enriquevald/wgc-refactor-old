﻿//------------------------------------------------------------------------------
// Copyright © 2007-2015 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_gaming_hall_set_pcd_meters.cs
//  
//   DESCRIPTION: Implements the class uc_gaming_hall_set_pcd_meters (Set PCD terminal meters.)
// 
//        AUTHOR Daniel Dubé
// 
// CREATION DATE: 18-DEC-2015
// 
// REVISION HISTORY:
// 
// Date        Author     Description
//------------------------------------------------------------------------------
// 18-DEC-2015 DDS        Inital Draft.
// 25-FEB-2016 DDS        Fixed Bug 9166: Show only GUI configured meters 
// 07-APR-2016 ETP        Fixed Bug 11542: Error when configuring meters. Meters limited to: Coin in / Coin out / Jackpot credits /Games played / Games won 
// 11-APR-2016 ETP        Fixed Bug 11542: RollBack
// 11-APR-2016 ETP        Fixed Bug 11224: SALONES Refresh screen when change terminal event and prevent losing changes.
// 14-APR-2016 ETP        Fixed Bug 11590: Meter_code Needs to be converted to HEX.
//------------------------------------------------------------------------------

#region Usings

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using WSI.Common;
using WSI.Common.GamingHall;
using System.Threading;
using System.Data.SqlClient;

#endregion // Usings

namespace WSI.Cashier
{
  public partial class uc_gaming_hall_set_pcd_meters : UserControl
  {
    #region Constants

    private const string NONE = "NONE";
    private const string RESET = "RESET";
    private string CONFIGURATION_TITTLE = Resource.String("STR_PCD_METERS_CONFIGURATION");

    #endregion

    #region Members

    string[] m_clear_textbox_ops;
    private Int32 m_terminal_id = 0;

    #endregion

    #region Constructor

    //------------------------------------------------------------------------------
    // PURPOSE : Constructor 
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //  
    public uc_gaming_hall_set_pcd_meters()
    {
      InitializeComponent();
      InitializeControlResources();

      LoadTerminalFilters();
    }

    #endregion //constructor

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Initialize control resources. (NLS string, images, etc.).
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //  
    public void InitializeControlResources()
    {
      this.lbl_termminal_title.Text = Resource.String("STR_PCD_METERS_FRM_TITLE");
      this.gb_terminals.HeaderText = Resource.String("STR_UC_GAMINGHALL_TERMINALS"); // "Máquinas";
      this.btn_process_changes.Text = Resource.String("STR_PCD_METERS_SAVE"); // Guardar cambios
      this.btn_select_terminal.Text = Resource.String("STR_PCD_METERS_SELECT_TERMINAL");  // Seleccionar
      this.gb_terminal_config.HeaderText = CONFIGURATION_TITTLE;
      this.RemoveMetersUserControls();
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Load filters by entry mode
    // 
    //  PARAMS:
    //      - INPUT: 
    //
    //      - OUTPUT:
    //
    // RETURNS: True if Terminal selected is offline.
    // 
    public void LoadTerminalFilters()
    {
      StringBuilder _where;
      _where = new StringBuilder();

      _where.AppendLine("    WHERE   TE_ACTIVE = 1                       ");
      _where.AppendLine("      AND   TE_STATUS = " + (int)TerminalStatus.ACTIVE);
      _where.AppendLine("      AND   (TE_SMIB2EGM_COMM_TYPE = " + (Int32)SMIB_COMMUNICATION_TYPE.PULSES);
      _where.AppendLine("      OR     TE_SMIB2EGM_COMM_TYPE = " + (Int32)SMIB_COMMUNICATION_TYPE.SAS_IGT_POKER + ")");

      uc_terminal_filter1.InitControls("", _where.ToString(), true, true);
    }

    private Boolean HasPendingChanges()
    {
      Boolean _has_pending_changes;
      _has_pending_changes = false;

      foreach (Control control in this.gb_scroll_container.Controls)
      {
        uc_set_terminal_meter _meter = control as uc_set_terminal_meter;
        if (_meter == null || !_meter.HasSelectedAction)
        {
          continue;
        }
        _has_pending_changes = true;

        break;
      }

      return _has_pending_changes;
    }
    //------------------------------------------------------------------------------
    // PURPOSE: Refresh collect/fill/read status.
    // 
    //  PARAMS:
    //      - INPUT: 
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    public void RefreshStatus()
    {
      this.LoadTerminalFilters();
      this.RemoveMetersUserControls();
      this.btn_process_changes.Enabled = false;
    }

    #endregion //PublicMethods

    #region Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE: Remove any previous meter controls
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    private void RemoveMetersUserControls()
    {
      this.gb_terminal_config.HeaderText = String.Format("{0}", Resource.String("STR_PCD_METERS_CONFIGURATION"));

      for (int i = this.gb_scroll_container.Controls.Count - 1; i >= 0; i--)
      {
        uc_set_terminal_meter _meter = this.gb_scroll_container.Controls[i] as uc_set_terminal_meter;
        if (_meter != null)
        {
          _meter.Changed -= this._meter_Changed;
          this.gb_scroll_container.Controls.Remove(_meter);
        }
      }
      this.gb_scroll_container.Refresh();
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Create new Meters controls as needed
    // 
    //  PARAMS:
    //      - INPUT: 
    //
    //      - OUTPUT:
    //
    private void CreateMeters()
    {
      DataTable _operations = GetOperations();
      DataSet _pcd_info;
      DataRow[] _pcd_config_items;
      DataTable _sas_meters;
      SMIB_COMMUNICATION_TYPE _smib_comm_type;
      Int64 _smib_conf_id;
      Byte _height_pos;
      String _configurable_meters;

      m_clear_textbox_ops = new string[2] { NONE, RESET };

      this.m_terminal_id = this.uc_terminal_filter1.TerminalSelected();
      this.gb_terminal_config.HeaderText = String.Format("{0} ({1})", Resource.String("STR_PCD_METERS_CONFIGURATION"), this.uc_terminal_filter1.TerminalSelectedName());
      this.gb_terminal_config.Refresh();

      _configurable_meters = String.Empty;

      using (DB_TRX _trx = new DB_TRX())
      {
        Terminal.Trx_GetTerminalSmibProtocol(this.m_terminal_id, TerminalTypes.SAS_HOST, _trx.SqlTransaction, out _smib_comm_type, out _smib_conf_id);
        _pcd_info = TerminalMetersInfo.ReadPCDConfiguration(_trx.SqlTransaction, _smib_conf_id);
        _sas_meters = TerminalMetersInfo.ReadSASMeters(_trx.SqlTransaction, _configurable_meters);
      }

      _height_pos = 0;
      _pcd_config_items = _pcd_info.Tables[1].Select("PMT_PCD_IO_TYPE = 1");

      foreach (DataRow meter_info in _pcd_config_items)
      {
        String _description = null;
        DataRow[] _meters_codes;
        Int64 _meter_code;

        _meters_codes = _sas_meters.Select("SMCG_METER_CODE = " + meter_info["PMT_EGM_NUMBER"]);
        _meter_code = (Int64)meter_info["PMT_EGM_NUMBER"];

        if (_meters_codes == null || _meters_codes.Length == 0)
          continue;

        String _nls_description;
        _nls_description = Resource.String(String.Format("STR_SAS_METER_DESC_{0:00000}", _meter_code));
        if (String.IsNullOrEmpty(_nls_description))
        {
          _nls_description = _meters_codes[0]["SMC_DESCRIPTION"].ToString();
        }

        _description = String.Format("{0:x} - {1}", _meter_code, _nls_description);

        uc_set_terminal_meter meter = this.CreateDefaultMeter(_height_pos, _description, String.Empty);
        Point offset = new Point(meter.Location.X, meter.Location.Y);
        offset.Offset(new Point(0, _height_pos * meter.Height));
        meter.Code = meter_info["PMT_EGM_NUMBER"].ToString();
        meter.Location = offset;
        meter.Actions = _operations;
        meter.NoActionValue = NONE;
        meter.ClearTextBoxValues = m_clear_textbox_ops;
        meter.InitializeControlValues();
        meter.PadNumberHorizontalPosition = WSI.Cashier.Controls.PadNumberHorizontalPosition.LEFT;
        meter.PadNumberVerticalPosition = WSI.Cashier.Controls.PadNumberVerticalPosition.MIDDLE;
        this.gb_scroll_container.Controls.Add(meter);
        _height_pos++;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Create an Operations DataTable with possible values
    // 
    //  PARAMS:
    //      - INPUT: 
    //
    //      - OUTPUT:
    //
    private static DataTable GetOperations()
    {
      DataTable _operations;

      _operations = new DataTable();
      _operations.Columns.Add(new DataColumn(uc_set_terminal_meter.ValueMember));
      _operations.Columns.Add(new DataColumn(uc_set_terminal_meter.DisplayMember));

      _operations.Rows.Add(NONE, String.Empty); // Resource.String("STR_PCD_METERS_NONE"));
      _operations.Rows.Add(RESET, Resource.String("STR_PCD_METERS_RESET"));
      _operations.Rows.Add("SET", Resource.String("STR_PCD_METERS_SET"));
      _operations.Rows.Add("FIX", Resource.String("STR_PCD_METERS_FIX"));

      return _operations;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Gets a new Meter user Control
    // 
    //  PARAMS:
    //      - INPUT: Tab index, Item description, Item Value
    //
    //      - OUTPUT:
    //
    // RETURNS: uc_set_pcd_meter. 
    // 
    private uc_set_terminal_meter CreateDefaultMeter(Byte Index, String Description, String Value)
    {
      uc_set_terminal_meter _tmp_meter = new uc_set_terminal_meter();
      _tmp_meter.Visible = true;
      _tmp_meter.Actions = null;
      _tmp_meter.ActionsLocation = this.uc_set_pcd_meter_model.ActionsLocation;
      _tmp_meter.ActionsWidth = this.uc_set_pcd_meter_model.ActionsWidth;
      _tmp_meter.BackColor = System.Drawing.Color.Transparent;
      _tmp_meter.Description = Description;
      _tmp_meter.DescriptionLocation = this.uc_set_pcd_meter_model.DescriptionLocation;
      _tmp_meter.DescriptionWidth = this.uc_set_pcd_meter_model.DescriptionWidth;
      _tmp_meter.Location = this.uc_set_pcd_meter_model.Location;
      _tmp_meter.Name = "uc_set_pcd_meter_" + Index.ToString();
      _tmp_meter.Size = this.uc_set_pcd_meter_model.Size;
      _tmp_meter.TabIndex = Index;
      _tmp_meter.Value = Value;
      _tmp_meter.ValueLocation = this.uc_set_pcd_meter_model.ValueLocation;
      _tmp_meter.ValueWidth = this.uc_set_pcd_meter_model.ValueWidth;
      _tmp_meter.Changed += _meter_Changed;
      _tmp_meter.MaxValue += _tmp_meter_MaxValue;

      return _tmp_meter;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Validates all values in meters ( Empty values )
    // 
    //  PARAMS:
    //      - INPUT: 
    //
    //      - OUTPUT:
    //
    // RETURNS: True if values are coherent. False otherwise
    // 
    private bool IsScreenDataOk()
    {
      StringBuilder _errors;

      _errors = new StringBuilder();
      foreach (Control _control in this.gb_scroll_container.Controls)
      {
        uc_set_terminal_meter _meter = _control as uc_set_terminal_meter;
        if (_meter == null)
        {
          continue;
        }
        if (Array.IndexOf(m_clear_textbox_ops, _meter.SelectedAction) < 0 && String.IsNullOrEmpty(_meter.Value))
        {
          _errors.AppendLine(Resource.String("STR_PCD_METER_NEEDS_VALUE", _meter.Description));
        }
      }
      if (_errors.Length > 0)
      {
        frm_message.Show(_errors.ToString(), Resource.String("STR_PCD_METERS_FRM_TITLE"), MessageBoxButtons.OK, MessageBoxIcon.Error, this.ParentForm);

        return false;
      }

      return true;
    }

    private bool ApplyChanges()
    {
      String _xmlParameter;

      _xmlParameter = CreateXmlParameter();
      if (_xmlParameter.Length == 0)
      {
        return true;
      }
      using (DB_TRX _trx = new DB_TRX())
      {
        try
        {
          Boolean _result;
          _result = WcpCommands.InsertWcpCommand(this.m_terminal_id, WCP_CommandCode.RequestUpdatePCDMeters, _xmlParameter.ToString(), _trx.SqlTransaction);
          _trx.Commit();

          return _result;
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
          _trx.Rollback();

          return false;
        }
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Creates an Xml string as Parameter for WCPCommand
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT: 
    //
    // RETURNS: Xml Item
    // 
    private String CreateXmlParameter()
    {
      StringBuilder _xmlParameter;
      StringBuilder _xmlItems;

      _xmlParameter = new StringBuilder("<PCDMeters>");
      _xmlItems = new StringBuilder();

      foreach (Control _control in this.gb_scroll_container.Controls)
      {
        uc_set_terminal_meter _meter = _control as uc_set_terminal_meter;
        if (_meter == null || !_meter.Visible || !_meter.HasSelectedAction)
        {
          continue;
        }
        _xmlItems.Append(_meter.GetMeterAsXml());
      }

      if (_xmlItems.Length == 0)
      {
        _xmlParameter.Length = 0; // No data
      }
      else
      {
        _xmlParameter.Append(_xmlItems.ToString());
      }
      _xmlParameter.Append("</PCDMeters>");

      return _xmlParameter.ToString();
    }

    #endregion //Private Methods

    #region Event Handling

    private void btn_process_changes_Click(object sender, EventArgs e)
    {
      if (!IsScreenDataOk())
      {
        return;
      }

      if (ApplyChanges())
      {
        frm_message.Show(Resource.String("STR_PCD_METERS_CHANGES_OK"), Resource.String("STR_PCD_METERS_FRM_TITLE"), MessageBoxButtons.OK, MessageBoxIcon.Information, this.ParentForm);
        this.RefreshStatus();
      }
      else
      {
        frm_message.Show(Resource.String("STR_PCD_METERS_DB_ERROR"), Resource.String("STR_PCD_METERS_FRM_TITLE"), MessageBoxButtons.OK, MessageBoxIcon.Error, this.ParentForm);
      }

    }

    //------------------------------------------------------------------------------
    // PURPOSE: Selects one terminal
    // 
    //  PARAMS:
    //      - INPUT:
    //          - sender:     object
    //          - EventArgs:  e
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    private void btn_select_terminal_Click(object sender, EventArgs e)
    {
      this.gb_scroll_container.SuspendLayout();

      this.RemoveMetersUserControls();
      this.CreateMeters();


      this.gb_scroll_container.Refresh();
      this.gb_scroll_container.ResumeLayout(false);
      this.gb_scroll_container.PerformLayout();

      this.ResumeLayout(false);
      this.PerformLayout();

      //int horScroll = SystemInformation.HorizontalScrollBarHeight;

      //this.gb_scroll_container.Padding = new Padding(0, 0, horScroll, 0);

      //this.gb_scroll_container.AutoScroll = true;

    }

    //------------------------------------------------------------------------------
    // PURPOSE: Prevent user from loosing changes
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    private void uc_gaming_hall_set_pcd_meters_Validating(object sender, CancelEventArgs e)
    {
      if (!this.HasPendingChanges())
      {
        this.RefreshStatus();
        return;
      }

      String _message = Resource.String("STR_PCD_METERS_LOST_CHANGES");
      _message = _message.Replace("\\r\\n", "\r\n");
      DialogResult _res = frm_message.Show(_message, Resource.String("STR_PCD_METERS_FRM_TITLE"), MessageBoxButtons.YesNo, MessageBoxIcon.Question, this.ParentForm);
      e.Cancel = _res != DialogResult.OK;
      if (!e.Cancel)
      {
        this.RefreshStatus();
      }

    }

    private void _tmp_meter_MaxValue(object sender, EventArgs e)
    {
      this.btn_process_changes.Enabled = false;
      frm_message.Show(((uc_set_terminal_meter)sender).MaxValueMsg, Resource.String("STR_PCD_METERS_FRM_TITLE"), MessageBoxButtons.OK, MessageBoxIcon.Warning, this.ParentForm);
    }

    private void _meter_Changed(object sender, EventArgs e)
    {
      this.btn_process_changes.Enabled = false;
      foreach (Control control in this.gb_scroll_container.Controls)
      {
        uc_set_terminal_meter _meter = control as uc_set_terminal_meter;
        if (_meter != null && _meter.Visible && _meter.HasSelectedAction)
        {
          this.btn_process_changes.Enabled = true;
          break;
        }
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Prevent user from loosing changes when Terminal or Provider changed.
    // 
    //  PARAMS:
    //      - INPUT:
    //        DataGridViewCellValidatingEventArgs e
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    private void uc_terminal_filter1_ValidatingTerminalOrProvider(DataGridViewCellValidatingEventArgs e)
    {
      String _message;
      if (!this.gb_terminal_config.HeaderText.Equals(CONFIGURATION_TITTLE, StringComparison.OrdinalIgnoreCase))
      {
        _message = Resource.String("STR_PCD_METERS_LOST_CHANGES");
        _message = _message.Replace("\\r\\n", "\r\n");
        DialogResult _res = frm_message.Show(_message, Resource.String("STR_PCD_METERS_FRM_TITLE"), MessageBoxButtons.YesNo, MessageBoxIcon.Question, this.ParentForm);
        if (_res != DialogResult.OK)
        {
          e.Cancel = true;
          return;
        }
        this.gb_terminal_config.HeaderText = CONFIGURATION_TITTLE;
        this.RemoveMetersUserControls();
        this.btn_process_changes.Enabled = false;
        this.gb_terminal_config.Refresh();
      }
    }

    #endregion //Event Handling





  }
}
