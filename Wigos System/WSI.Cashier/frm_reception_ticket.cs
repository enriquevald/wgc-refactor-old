﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_reception_ticket.cs
// 
//   DESCRIPTION: Allows create Entrance for Reception Module
//
//        AUTHOR: YNM
// 
// CREATION DATE: 12-JAN-2016
//  
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 12-JAN-2016  YNM    First release.
// 02-FEB-2016  FJC    Product Backlog Item 8658:Visitas / Recepción: Modificaciones Review 25/01/2016
// 02-FEB-2016  FJC    Bug 8902:Visitas / Recepción: No se guarda el nivel del primer precio seleccionado al entrar en el formulario de pago de entradas.
// 22-MAR-2106  YNM    Bug 10759: Reception error paying with currency
// 06-APR-2016  DDS    Bug 11370: Reception cannot clear Coupon number
// 07-ABR-2016  YNM    Bug 11523: it didn't update propertly cash monitor
// 09-MAY-2015  JBP    Product Backlog Item 12931:Visitas / Recepción: 2º Ticket LOPD
// 10-MAY-2015  JBP    Product Backlog Item 12927:Visitas / Recepción: Caducidad de las entradas variable
// 05-JUL-2016  ETP    PBI 15060: Cambio de multidivisa: Identificar al cliente en cualquier operación con divisas
// 11-OCT-2016  SMN    Bug 18977:Recepción: No se pueden registrar entradas de clientes
// 18-APR-2017  ETP    PBI 26747: Junkets - Reception (Cashier)
// 09-MAY-2017  DPC    WIGOS-1219: Junkets - Voucher
// 31-AUG-2017  AMF    Bug 29507:[WIGOS-4810] [Ticket #8405] reception
//------------------------------------------------------------------------------
//
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WSI.Common;
using System.Data.SqlClient;
using System.Globalization;
using System.Collections;
using System.Reflection;
using System.Threading;
using WSI.Cashier.TITO;
using WSI.Common.TITO;
using System.IO;
using WSI.Common.Entrances.Entities;
using WSI.Common.Entrances;
using WSI.Common.Junkets;
using WSI.Cashier.JunketsCashier;

namespace WSI.Cashier
{
  public partial class frm_reception_ticket : Controls.frm_base
  {

    #region Atributes

    private Currency m_total_amount;
    private Currency m_entry_base_price;
    private Currency m_entry_sel_price;
    private String m_coupon;
    private Boolean m_agreement;
    private Boolean m_only_national_currency;
    private Boolean m_is_loading;
    private int m_level;
    private int m_sel_level_price;
    private Int64? m_account_id;
    private Int32 m_valid_gaming_days;
    CurrencyExchangeResult m_exchange_result;    
    private String m_price_description;

    private List<CurrencyExchange> m_currencies = null;
    private CurrencyExchange m_national_currency = null;
    private CurrencyExchange m_currency_exchange = null;
    private String[] m_account_Info = null;
    private CurrencyExchangeSubType m_selected_bank_card_type = CurrencyExchangeSubType.NONE;


    private Boolean _is_voucher_loaded = false;
    private Int32 m_agreement_enabled = 0;

    private Boolean m_entry_prices_empty = false;
    private Boolean m_entry_price_is_default = false;

    private JunketsShared m_junket = null;

    #endregion

    #region Constants

    private const Int32 GRID_COLUMN_ENTRANCE_PRICES_ID = 0;
    private const Int32 GRID_COLUMN_ENTRANCE_PRICES_DESCRIPTION = 1;
    private const Int32 GRID_COLUMN_ENTRANCE_PRICES_PRICE = 2;
    private const Int32 GRID_COLUMN_ENTRANCE_PRICES_LEVEL = 3;
    private const Int32 GRID_COLUMN_ENTRANCE_PRICES_DEFAULT = 4;
    private const Int32 GRID_COLUMN_ENTRANCE_PRICES_VALID_GAMING_DAYS = 5;
    private const Int32 GRID_COLUMN_ENTRANCE_PRICES_PRICE_STRING = 6;



    #endregion

    #region Public Methods

    public frm_reception_ticket()
    {
      Boolean _allow_bank_card_types;

      InitializeComponent();
      InitializeControlResources();

      EventLastAction.AddEventLastAction(this.Controls);

      List<CurrencyExchangeType> _coins_and_chips = new List<CurrencyExchangeType>();
      _coins_and_chips.Add(CurrencyExchangeType.CURRENCY);
      _coins_and_chips.Add(CurrencyExchangeType.CARD);
      _coins_and_chips.Add(CurrencyExchangeType.CHECK);

      _allow_bank_card_types = GeneralParam.GetBoolean("Cashier.PaymentMethod", "BankCard.DifferentiateType", false);
      CurrencyExchange.GetAllowedCurrencies(true, _coins_and_chips, false, _allow_bank_card_types, true, out m_national_currency, out m_currencies);

      m_currency_exchange = m_national_currency;

      m_agreement_enabled = GeneralParam.GetInt32("Reception", "Agreement.ShowMode");
      cb_data_agreement.Visible = (m_agreement_enabled != 0); //Don't show agreement check
      this.ShowCloseButton = false;
    }

    /// <summary>
    /// 
    /// </summary>
    public DialogResult Show(Int64? AccountId
                              , String Document
                              , int Level
                              , Boolean OnlyNationalCurrency
                              , out TicketData Voucher)
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_reception_ticket", Log.Type.Message);

      Voucher = new TicketData();

      m_account_id = AccountId;
      m_is_loading = true;

      m_only_national_currency = OnlyNationalCurrency;

      VoucherAccountInfo(m_account_id, Document, Level);

      if (m_only_national_currency)
      {
        m_currencies.Clear();
        m_currencies.Add(m_national_currency);
        btn_selected_currency.Enabled = false;
        btn_selected_currency.BackColor = System.Drawing.Color.LightGray;
      }
      this.ShowCurrencyExchange();

      this.SetControlsView();

      KeyboardMessageFilter.RegisterHandler(this, BarcodeType.Flyer, FlyerReceivedEvent);

      cb_data_agreement.Checked = (m_agreement_enabled != 0 && Entrance.IsNewCustomer(m_account_id));

      if (!_is_voucher_loaded)
        PreviewVoucher();

      //all controls are loaded for the fist time
      m_is_loading = false;
      // Show form
      DialogResult _dlg_rc = this.ShowDialog(Parent);

      Voucher.Agreement = m_agreement;
      Voucher.Coupon = m_coupon;
      Voucher.PriceReal = m_entry_base_price;
      Voucher.PricePaid = m_total_amount;
      Voucher.ValidGamingDays = m_valid_gaming_days;
      Voucher.ExchangeResult = m_exchange_result;
      Voucher.priceDescription = m_price_description;
      if (m_junket != null)
      {
        Voucher.junketInfo = m_junket.BusinessLogic;
      }

      return _dlg_rc;

    } // Show 

    /// <summary>
    /// Contruct client information for Voucher
    /// </summary>
    /// <param name="AccountId"></param>
    /// <param name="Level"></param>
    private void VoucherAccountInfo(Int64? AccountId, String Document, int Level)
    {
      CardData _card_data;
      String[] _voucher_info;

      //Set Account data  
      _card_data = new CardData();
      _card_data.AccountId = (Int64)AccountId;
      m_level = Level;

      CardData.DB_CardGetAllData(_card_data.AccountId, _card_data, true);
      _voucher_info = _card_data.VoucherAccountInfo();

      m_account_Info = new String[4];
      m_account_Info[0] = _voucher_info[0];
      m_account_Info[1] = Resource.String("STR_FRM_PLAYER_EDIT_DOCUMENT")
                            + ": " + Document + "\n";
      m_account_Info[2] = _voucher_info[1];
      m_account_Info[3] = _voucher_info[2];

    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Initialize control resources (NLS strings, images, etc)
    /// </summary>
    public void InitializeControlResources()
    {
      //Form 
      this.FormTitle = Resource.String("STR_FRM_CUSTOMER_RECEPTION_SAVE_ENTRY_TITLE");

      //   - Labels
      gb_payment.Text = Resource.String("STR_FRM_AMOUNT_CURRENCY_GROUP_BOX");
      gb_total.HeaderText = gb_total.HeaderText = Resource.String("STR_FRM_INPUT_TOTAL_TO_PAY");
      gb_payment.HeaderText = Resource.String("STR_FRM_AMOUNT_CURRENCY_GROUP_BOX");
      lb_entry_price.Text = Resource.String("STR_FRM_RECEPTION_TICKET_ENTRY_PRICE");
      lb_coupon_number.Text = Resource.String("STR_FRM_RECEPTION_TICKET_COUPON_NUMBER");
      gb_entry_details.HeaderText = Resource.String("STR_FRM_RECEPTION_TICKET_ENTRY_DETAILS");

      //   - Buttons
      btn_ok.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_OK");
      btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");
      btn_apply_coupon.Text = ">";

      btn_selected_currency.Text = Resource.String("STR_FRM_AMOUNT_CHANGE_BUTTON");
      cb_data_agreement.Text = GeneralParam.GetString("Reception", "Agreement.CashierTex");

    }

    /// <summary>
    /// 
    /// </summary>
    private void SetControlsView()
    {

      this.txt_coupon_number.Text = "";
      this.lbl_total_to_pay.Text = ShowTotal(0);

      this.PopulateEntrancePrices();
      this.FormatGridEntrancePrices();
      this.SetupRowEntrancePrices();

    } // SetControlsView

    /// <summary>
    /// Set selected currency exchange
    /// </summary>
    private void SetCurrencyExchange()
    {
      Int32 _index;
      CurrencyExchange _currency_returned;

      if (!m_is_loading)
      {

        if (m_currencies.Count > 3)
        {
          frm_browse_currencies frm = new frm_browse_currencies();

          try
          {
            frm.Show(m_currencies, out _currency_returned, VoucherTypes.CardAdd);
            if (_currency_returned != null)
            {
              m_currency_exchange = _currency_returned;
            }
          }
          finally
          {
            frm.Dispose();
          }

        }
        else
        {
          _index = m_currencies.IndexOf(m_currency_exchange);
          _index = (_index + 1) % m_currencies.Count;
          m_currency_exchange = m_currencies[_index];
        }

      }

      switch (m_currency_exchange.Type)
      {
        case CurrencyExchangeType.CURRENCY:
          lbl_paymet_method.Text = Resource.String("STR_FRM_AMOUNT_INPUT_CASH_PAYMENT") + "\n";
          lbl_paymet_method.Text += m_currency_exchange.Description;
          lbl_currency_iso_code.Text = m_currency_exchange.CurrencyCode;
          break;

        case CurrencyExchangeType.CASINOCHIP:
          lbl_paymet_method.Text = m_currency_exchange.Description;
          lbl_currency_iso_code.Text = String.Empty;
          break;
        case CurrencyExchangeType.CARD:
          switch (m_currency_exchange.SubType)
          {
            case CurrencyExchangeSubType.NONE:
              lbl_paymet_method.Text = m_currency_exchange.Description;
              lbl_currency_iso_code.Text = "";
              break;

            case CurrencyExchangeSubType.CREDIT_CARD:
              m_selected_bank_card_type = CurrencyExchangeSubType.CREDIT_CARD;
              lbl_paymet_method.Text = m_currency_exchange.Description;
              lbl_currency_iso_code.Text = "";
              break;

            case CurrencyExchangeSubType.DEBIT_CARD:
              m_selected_bank_card_type = CurrencyExchangeSubType.DEBIT_CARD;
              lbl_paymet_method.Text = m_currency_exchange.Description;
              lbl_currency_iso_code.Text = "";
              break;

            default:
              lbl_paymet_method.Text = m_currency_exchange.Description;
              lbl_currency_iso_code.Text = "";
              break;
          }
          break;

        case CurrencyExchangeType.CHECK:
          lbl_paymet_method.Text = m_currency_exchange.Description;
          m_selected_bank_card_type = CurrencyExchangeSubType.CHECK;
          lbl_currency_iso_code.Text = "";
          break;

        default:
          lbl_paymet_method.Text = m_currency_exchange.Description;
          lbl_currency_iso_code.Text = "";
          break;
      }
      m_total_amount.CurrencyIsoCode = m_currency_exchange.CurrencyCode;
      SetCurrencyImage();
      UpdateExchangeResult();

    } // SetCurrencyExchange

    /// <summary>
    /// 
    /// </summary>
    private void UpdateExchangeResult()
    {
      if (m_entry_sel_price > 0 && m_currency_exchange.Type == CurrencyExchangeType.CURRENCY)
      {
        m_currency_exchange.ApplyExchange((Decimal)dgv_prices.SelectedRows[0].Cells[GRID_COLUMN_ENTRANCE_PRICES_PRICE].Value, out m_exchange_result);
        lbl_currency.Text = Resource.String("STR_FRM_AMOUNT_INPUT_EQUALS") + " " + Currency.Format(m_exchange_result.InAmount, m_national_currency.CurrencyCode);
      }
      else
      {
        m_currency_exchange.ApplyExchange(m_entry_sel_price, out m_exchange_result);
        lbl_currency.Text = "";

      }
      lbl_currency.Visible = m_exchange_result.WasForeingCurrency;
    }

    /// <summary>
    ///  Put the flag corresponding with Iso Code
    /// </summary>
    private void SetCurrencyImage()
    {
      CurrencyExchangeProperties _properties;

      _properties = CurrencyExchangeProperties.GetProperties(m_currency_exchange.CurrencyCode);
      if (_properties != null)
      {
        pb_currency.Image = _properties.GetIcon(m_currency_exchange.Type, pb_currency.Size.Width, pb_currency.Size.Height);
      }
      else
      {
        pb_currency.Image = null;
      }
    }//SelectedCurrencySettings

    /// <summary>
    /// Show controls of Currency Exchange
    /// </summary>
    private void ShowCurrencyExchange()
    {
      Boolean _is_cage_enabled;
      _is_cage_enabled = Cage.IsCageEnabled();

      lbl_currency.Text = "";
      lbl_paymet_method.Text = "";
      pb_currency.Visible = true;
      lbl_currency.Visible = !_is_cage_enabled;

      SetCurrencyExchange();

    }

    /// <summary>
    /// Returns a string for -> Total : Amount$ formated whit the nls and the apropiated currency string
    /// </summary>
    /// <returns></returns>
    private String ShowTotal(Currency Amount)
    {
      String _national_currency;

      if (String.IsNullOrEmpty(Amount.CurrencyIsoCode))
      {
        Amount.CurrencyIsoCode = m_currency_exchange.CurrencyCode;
      }
      if (Amount.CurrencyIsoCode == Cage.CHIPS_ISO_CODE || m_currency_exchange.Type == CurrencyExchangeType.CASINOCHIP)
      {
        return Amount.ToString(WSI.Common.Gift.FORMAT_POINTS_DEC.ToString()) + " " + CurrencyString();
      }

      _national_currency = CurrencyExchange.GetNationalCurrency();
      if (Amount.CurrencyIsoCode == _national_currency)
      {
        return ((Currency)Amount).ToString();
      }
      else
      {
        return Currency.Format(Amount, Amount.CurrencyIsoCode);
      }

    }//SetCurrencyExchange

    /// <summary>
    /// Returns the currency code  string. If currency is X01 returns the NLS
    /// </summary>
    /// <returns></returns>
    private String CurrencyString()
    {
      switch (m_currency_exchange.Type)
      {
        case CurrencyExchangeType.CASINOCHIP:
          return Resource.String("STR_VOUCHER_OTHER_OPERATIONS_IN_CHIPS");

        case CurrencyExchangeType.CURRENCY:
          if (m_currency_exchange.CurrencyCode == Cage.CHIPS_ISO_CODE)
          {
            return Resource.String("STR_VOUCHER_OTHER_OPERATIONS_IN_CHIPS");
          }
          else
          {
            return m_currency_exchange.CurrencyCode;
          }

        default:

          return "";
      }
    }//CurrencyString

    /// <summary>
    /// Generate voucher Preview
    /// </summary>
    private void PreviewVoucher()
    {
      ArrayList _voucher_list;
      String _voucher_file_name;
      Currency[] _amounts;
      JunketsBusinessLogic _business_logic;

      _business_logic = null;

      m_total_amount = m_entry_sel_price;

      m_coupon = txt_coupon_number.Text;
      lbl_total_to_pay.Text = ShowTotal(m_total_amount);

      _amounts = new Currency[3];
      _amounts[0] = m_entry_base_price;// Base price
      _amounts[1] = m_total_amount;// Paid price
      _amounts[2] = m_entry_base_price - m_total_amount;// Difference


      using (DB_TRX _db_trx = new DB_TRX())
      {
        if (m_junket != null)
        {
          _business_logic = m_junket.BusinessLogic;
        }

        _voucher_list = VoucherBuilder.VoucherReception(m_account_Info
                                                        , 0
                                                        , CashierVoucherType.CustomerEntrance
                                                        , _amounts
                                                        , m_exchange_result
                                                        , m_coupon
                                                        , m_valid_gaming_days
                                                        , m_agreement
                                                        , PrintMode.Preview
                                                        , _business_logic
                                                        , 0
                                                        , _db_trx.SqlTransaction);

      }

      _voucher_file_name = VoucherPrint.GenerateFileForPreview(_voucher_list);
      web_browser.Navigate(_voucher_file_name);
      _is_voucher_loaded = true;

    }

    /// <summary>
    /// Fill prices for grid
    /// </summary>
    private void PopulateEntrancePrices()
    {
      DataTable _dt_entrance_prices;

      CustomerEntrancePrices.GetCEPricesToCashier(m_level, out _dt_entrance_prices);
      _dt_entrance_prices.Columns.Add("CUEP_PRICE_STRING");
      dgv_prices.DataSource = _dt_entrance_prices;

    }//PopulateEntrancePrices

    /// <summary>
    /// Give format to price grid
    /// </summary>
    private void FormatGridEntrancePrices()
    {
      dgv_prices.ColumnHeadersHeight = 35;
      dgv_prices.RowTemplate.Height = 50;
      dgv_prices.RowTemplate.DividerHeight = 0;

      //    - Customer Entrance Prices Id
      dgv_prices.Columns[GRID_COLUMN_ENTRANCE_PRICES_ID].Width = 0;
      dgv_prices.Columns[GRID_COLUMN_ENTRANCE_PRICES_ID].Visible = false;

      //    - Description
      dgv_prices.Columns[GRID_COLUMN_ENTRANCE_PRICES_DESCRIPTION].HeaderCell.Value = Resource.String("STR_FRM_RECEPTION_TICKET_DESCRIPTION");
      dgv_prices.Columns[GRID_COLUMN_ENTRANCE_PRICES_DESCRIPTION].Width = 190;
      dgv_prices.Columns[GRID_COLUMN_ENTRANCE_PRICES_DESCRIPTION].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

      //    - Price
      dgv_prices.Columns[GRID_COLUMN_ENTRANCE_PRICES_PRICE].Width = 0;
      dgv_prices.Columns[GRID_COLUMN_ENTRANCE_PRICES_PRICE].Visible = false;

      //    - Price string
      dgv_prices.Columns[GRID_COLUMN_ENTRANCE_PRICES_PRICE_STRING].HeaderCell.Value = Resource.String("STR_FRM_RECEPTION_TICKET_PRICE");
      dgv_prices.Columns[GRID_COLUMN_ENTRANCE_PRICES_PRICE_STRING].Width = 100;
      dgv_prices.Columns[GRID_COLUMN_ENTRANCE_PRICES_PRICE_STRING].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
      dgv_prices.Columns[GRID_COLUMN_ENTRANCE_PRICES_PRICE_STRING].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

      //    - Valid gaming days
      dgv_prices.Columns[GRID_COLUMN_ENTRANCE_PRICES_VALID_GAMING_DAYS].HeaderCell.Value = Resource.String("STR_FRM_RECEPTION_VALID_GAMING_DAYS");
      dgv_prices.Columns[GRID_COLUMN_ENTRANCE_PRICES_VALID_GAMING_DAYS].Width = 60;
      dgv_prices.Columns[GRID_COLUMN_ENTRANCE_PRICES_VALID_GAMING_DAYS].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
      dgv_prices.Columns[GRID_COLUMN_ENTRANCE_PRICES_VALID_GAMING_DAYS].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

      //    - Level
      dgv_prices.Columns[GRID_COLUMN_ENTRANCE_PRICES_LEVEL].Width = 0;
      dgv_prices.Columns[GRID_COLUMN_ENTRANCE_PRICES_LEVEL].Visible = false;

      //    - Default
      dgv_prices.Columns[GRID_COLUMN_ENTRANCE_PRICES_DEFAULT].Width = 0;
      dgv_prices.Columns[GRID_COLUMN_ENTRANCE_PRICES_DEFAULT].Visible = false;

    }//FormatGridEntrancePrices

    /// <summary>
    /// Set rows styles
    /// </summary>
    private void SetupRowEntrancePrices()
    {
      Decimal _price;
      string _price_with_curr;
      DataTable _dt;
      DataRow _dr;

      CurrencyExchangeResult _cer;

      _price = 0;
      m_entry_prices_empty = false;


      foreach (DataGridViewRow _row in dgv_prices.Rows)
      {
        m_currency_exchange.ApplyExchange((Decimal)_row.Cells[GRID_COLUMN_ENTRANCE_PRICES_PRICE].Value, out _cer);
        _price = this.m_currency_exchange.Type == CurrencyExchangeType.CHECK ? _cer.InAmount : _cer.NetAmount;

        _price_with_curr = ShowTotal((Currency)_price);
        _row.Cells[GRID_COLUMN_ENTRANCE_PRICES_PRICE_STRING].Value = _price_with_curr;

        if ((Boolean)_row.Cells[GRID_COLUMN_ENTRANCE_PRICES_DEFAULT].Value)
        {
          m_entry_base_price = _price;
          m_entry_base_price.CurrencyIsoCode = _cer.InCurrencyCode;
        }

        if (_row.Selected)
        {
          m_entry_sel_price = _price;
          m_valid_gaming_days = (Int32)_row.Cells[GRID_COLUMN_ENTRANCE_PRICES_VALID_GAMING_DAYS].Value;
          m_entry_sel_price.CurrencyIsoCode = _cer.InCurrencyCode;
          m_entry_price_is_default = (Boolean)_row.Cells[GRID_COLUMN_ENTRANCE_PRICES_DEFAULT].Value;
        }
      }

      if (dgv_prices.Rows.Count == 0)
      {
        m_entry_prices_empty = true;
        _dt = (DataTable)dgv_prices.DataSource;

        _dr = _dt.NewRow();
        _dr[GRID_COLUMN_ENTRANCE_PRICES_DESCRIPTION] = Resource.String("STR_FRM_CARD_ASSIGN_FREE");
        _dr[GRID_COLUMN_ENTRANCE_PRICES_PRICE] = 0;
        _dr[GRID_COLUMN_ENTRANCE_PRICES_LEVEL] = 0;
        _dr[GRID_COLUMN_ENTRANCE_PRICES_PRICE_STRING] = ShowTotal(0);
        _dr[GRID_COLUMN_ENTRANCE_PRICES_DEFAULT] = 1;
        _dr[GRID_COLUMN_ENTRANCE_PRICES_VALID_GAMING_DAYS] = 1;

        _dt.Rows.Add(_dr);
      }

    }//SetupRowEntrancePrices

    #endregion

    #region Events

    /// <summary>
    /// Common function who shows a pop-up to change the currency
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btn_selected_currency_Click(object sender, EventArgs e)
    {
      SetCurrencyExchange();
      SetupRowEntrancePrices();

      this.txt_coupon_number.Text = "";
      PreviewVoucher();

    } //btn_selected_currency_Click

    /// <summary>
    /// Clear som fields when conditions change
    /// </summary>
    private void ClearFields()
    {
      this.lbl_total_to_pay.Text = ShowTotal(0);
      this.web_browser.Navigate("");
      _is_voucher_loaded = false;
    }

    /// <summary>
    ///  Common function who close the from
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btn_cancel_Click(object sender, EventArgs e)
    {
      this.DialogResult = DialogResult.Cancel;

      Misc.WriteLog("[FORM CLOSE] frm_reception_ticket (cancel)", Log.Type.Message);
      this.Close();
    }//btn_cancel_Click

    /// <summary>
    ///  Common function who Load Vouhcer and Update totals
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btn_preview_Click(object sender, EventArgs e)
    {
      PreviewVoucher();

    }//btn_preview_Click

    /// <summary>
    ///  Update vale when checkbox change 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void cb_data_agreement_CheckedChanged(object sender, EventArgs e)
    {
      m_agreement = cb_data_agreement.Checked;
      PreviewVoucher();

    }//cb_data_agreement_CheckedChanged

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btn_ok_Click(object sender, EventArgs e)
    {
      String _message;
      DialogResult _dgr;
      Boolean _is_ag;
      String _error_str;
      Boolean _continue;

      _is_ag = false;

      _message = String.Empty;
      _error_str = Resource.String("STR_APP_GEN_MSG_ERROR");

      _dgr = DialogResult.OK;

      if (this.dgv_prices.RowCount > 0 && this.dgv_prices.SelectedRows.Count > 0)
      {
        this.dgv_prices_CellClick(null, new DataGridViewCellEventArgs(0, this.dgv_prices.SelectedRows[0].Index));
      }

      _continue = (m_sel_level_price == m_level || m_entry_prices_empty || m_entry_price_is_default);

      if (m_currency_exchange != null && GeneralParam.GetBoolean("Cashier.PaymentMethod", "BankCard.DifferentiateType", false))
      {
        m_currency_exchange.SubType = m_selected_bank_card_type;
      }

      if (!_continue)
      {
        _continue = (ProfilePermissions.CheckPermissionList(ProfilePermissions.CashierFormFuncionality.Reception_ChangePrice,
                                                           ProfilePermissions.TypeOperation.RequestPasswd,
                                                           this, 0, out _error_str));
      }

      if (_continue)
      {
        if (!_is_voucher_loaded)
        {
          _message = Resource.String("STR_FRM_RECEPTION_TICKET_GENERATE_VOUCHER");
        }
        else if (m_agreement_enabled != 0
               && Entrance.IsNewCustomer(m_account_id)
               && !cb_data_agreement.Checked)
        {
          _message = Resource.String("STR_FRM_RECEPTION_TICKET_ACCEPT_AGREEMENT");
          _is_ag = true;
        }

        if (!String.IsNullOrEmpty(_message))
        {
          _dgr = frm_message.Show(_message, Resource.String("STR_APP_GEN_MSG_WARNING"), MessageBoxButtons.OKCancel, Images.CashierImage.Warning, this.ParentForm);

        }

        if (String.IsNullOrEmpty(_message) || (_is_ag && _dgr == DialogResult.OK))
        {
          this.DialogResult = DialogResult.OK;

          Misc.WriteLog("[FORM CLOSE] frm_reception_ticket (ok)", Log.Type.Message);
          this.Close();
        }
      }
      else
      {
        frm_message.Show(_error_str, Resource.String("STR_APP_GEN_MSG_ERROR"), MessageBoxButtons.OK, Images.CashierImage.Error, this.ParentForm);
      }
    }//btn_ok_Click         

    /// <summary>
    /// Update entry price selected
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void dgv_prices_CellClick(object sender, DataGridViewCellEventArgs e)
    {
      Decimal _new_price;
      CurrencyExchangeResult _cer;
      DataGridViewRow _dgvr;

      if (e.RowIndex > -1)
      {
        _dgvr = dgv_prices.Rows[e.RowIndex];

        Decimal.TryParse(_dgvr.Cells[GRID_COLUMN_ENTRANCE_PRICES_PRICE].Value.ToString(), out _new_price);
        m_sel_level_price = (int)_dgvr.Cells[GRID_COLUMN_ENTRANCE_PRICES_LEVEL].Value;
        m_valid_gaming_days = (Int32)_dgvr.Cells[GRID_COLUMN_ENTRANCE_PRICES_VALID_GAMING_DAYS].Value;

        m_currency_exchange.ApplyExchange(_new_price, out _cer);
        m_entry_sel_price = this.m_currency_exchange.Type == CurrencyExchangeType.CHECK ? _cer.InAmount : _cer.NetAmount;        
        m_price_description = _dgvr.Cells[GRID_COLUMN_ENTRANCE_PRICES_DESCRIPTION].Value.ToString();

        UpdateExchangeResult();
        m_entry_price_is_default = (Boolean)_dgvr.Cells[GRID_COLUMN_ENTRANCE_PRICES_DEFAULT].Value;
        ClearFields();

        PreviewVoucher();
      }

    }//dgv_prices_CellClick

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btn_apply_coupon_Click(object sender, EventArgs e)
    {
      JunketsShared _junket;
      _junket = new JunketsShared();

      txt_coupon_number.Text = txt_coupon_number.Text.Trim();
      if (_junket.ValidateFlyerCode(m_account_id, txt_coupon_number.Text, this.ParentForm))
      {
        txt_coupon_number.Text.ToUpper();
        if (_junket.BusinessLogic != null)
        {
          m_junket = _junket;
        }
        else
        {
          m_junket = null;
        }
      }
      else
      {
        m_junket = null;
        txt_coupon_number.Text = String.Empty;
      }

      PreviewVoucher();

      this.Activate();

    }//btn_apply_coupon_Click



    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void dgv_prices_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
    {
      dgv_prices.Rows[e.RowIndex].DefaultCellStyle.SelectionBackColor = Color.FromArgb(45, 47, 50);
      dgv_prices.Rows[e.RowIndex].DefaultCellStyle.SelectionForeColor = Color.FromArgb(244, 246, 249);
    }//dgv_prices_CellFormatting

    /// <summary>
    ///   toggle the on screen keyboard
    /// </summary>
    /// <param name="Sender"></param>
    /// <param name="E"></param>
    private void btn_keyboard_Click(object Sender, EventArgs E)
    {
      WSIKeyboard.ForceToggle();
    }//btn_keyboard_Click

    /// <summary>
    /// Event for flyers recived.
    /// </summary>
    /// <param name="e"></param>
    /// <param name="data"></param>
    private void FlyerReceivedEvent(KbdMsgEvent e, Object data)
    {
      String _barcode; ;

      _barcode = String.Empty;

      if (e == KbdMsgEvent.EventFlyer)
      {
        _barcode = (String)data;
        txt_coupon_number.Text = _barcode;
        btn_apply_coupon_Click(null, null);
      }

    } // FlyerReceivedEvent

    #endregion // Events



    internal TicketData ProcessTicket(long? AccountId, string Document, int Level, bool OnlyNationalCurrency)
    {
      TicketData _voucher;
      return this.Show(AccountId, Document, Level, OnlyNationalCurrency, out _voucher) == DialogResult.OK ? _voucher : null;
    }
  }
}
