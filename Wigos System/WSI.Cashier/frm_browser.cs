//------------------------------------------------------------------------------
// Copyright � 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_amount_input.cs
// 
//   DESCRIPTION: Implements the following classes:
//                1. frm_browser
//
//        AUTHOR: RRT
// 
// CREATION DATE: 27-AUG-2007
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 27-AUG-2007 RRT    First release.
// 04-APR-2012 JAB    Changes to move operations to obtain and view tickets to the project WSI.Common
// 10-AUG-2012 ACC    Use HtmlPrinter class for printing html vouchers 
// 18-SEP-2017 DPC    WIGOS-5268: Cashier & GUI - Icons - Implement
//
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Resources;
using System.IO;
using WSI.Common;

namespace WSI.Cashier
{
  public partial class frm_browser : Controls.frm_base_icon
  {
    #region Attributes

    #endregion 

    #region Constructor

    /// <summary>
    /// Constructor method.
    /// 1. Initialize controls.
    /// 2. Initialize control�s resources. (NLS, Images, etc)
    /// </summary>
    public frm_browser()
    {
      InitializeComponent();

      InitializeControlResources();
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Initialize control resources (NLS strings, images, etc)
    /// </summary>
    private void InitializeControlResources()
    {
      // - NLS Strings:
      //   - Title:
      lbl_browser_title.Text = Resource.String("STR_FRM_BROWSER_TITLE");

      //   - Labels

      //   - Buttons
      btn_print.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_PRINT");
      btn_close.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CLOSE");

      // - Images:

    } // InitializeControlResources

    private void btn_reprint_Click(object sender, EventArgs e)
    {
      Program.print_voucher = WSI.Common.VoucherBusinessLogic.GetPrintVoucher();

      if (Program.print_voucher)
      {
        // Set header, footer and margins settings for internet explorer
        WSI.Common.Voucher.PageSetup();

        // ACC 10-AUG-2012 Use HtmlPrinter class for printing html vouchers 
        HtmlPrinter.AddHtml(web_browser.DocumentText);

        ////////// Print voucher
        ////////web_browser.Print();
      }
    }

    private void btn_close_Click(object sender, EventArgs e)
    {
      Close();
    }

    #endregion

    #region Public Methods

    public void LoadHTML(String HTML)
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_browser (html load)", Log.Type.Message);
      // SLE 12-MAR-2010 Fixed DefectId #75 --> Case 3
      InitializeControlResources();

      web_browser.Navigate(VoucherManager.SaveHTMLVoucherOnFile("", HTML));

      web_browser.Show();
    }

    #endregion

  }
}