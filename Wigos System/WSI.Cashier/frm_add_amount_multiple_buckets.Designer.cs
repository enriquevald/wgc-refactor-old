﻿namespace WSI.Cashier
{
  partial class frm_add_amount_multiple_buckets
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
      this.uc_input_amount1 = new WSI.Cashier.uc_input_amount();
      this.gb_Action = new WSI.Cashier.Controls.uc_round_panel();
      this.uc_radioButton_add = new WSI.Cashier.Controls.uc_radioButton();
      this.uc_radioButton_sub = new WSI.Cashier.Controls.uc_radioButton();
      this.pnl_buttons = new System.Windows.Forms.Panel();
      this.btn_cancel = new WSI.Cashier.Controls.uc_round_button();
      this.btn_ok = new WSI.Cashier.Controls.uc_round_button();
      this.gb_total = new WSI.Cashier.Controls.uc_round_panel();
      this.lbl_operacion = new WSI.Cashier.Controls.uc_label();
      this.lbl_result = new WSI.Cashier.Controls.uc_label();
      this.gb_Buckets = new WSI.Cashier.Controls.uc_round_panel();
      this.dgv_Bucket_Grid = new WSI.Cashier.Controls.uc_DataGridView();
      this.pnl_data.SuspendLayout();
      this.gb_Action.SuspendLayout();
      this.pnl_buttons.SuspendLayout();
      this.gb_total.SuspendLayout();
      this.gb_Buckets.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dgv_Bucket_Grid)).BeginInit();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.Controls.Add(this.gb_total);
      this.pnl_data.Controls.Add(this.pnl_buttons);
      this.pnl_data.Controls.Add(this.gb_Action);
      this.pnl_data.Controls.Add(this.uc_input_amount1);
      this.pnl_data.Controls.Add(this.gb_Buckets);
      this.pnl_data.Size = new System.Drawing.Size(965, 625);
      // 
      // uc_input_amount1
      // 
      this.uc_input_amount1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.uc_input_amount1.Location = new System.Drawing.Point(563, 24);
      this.uc_input_amount1.Name = "uc_input_amount1";
      this.uc_input_amount1.Size = new System.Drawing.Size(380, 380);
      this.uc_input_amount1.TabIndex = 9;
      this.uc_input_amount1.WithDecimals = false;
      this.uc_input_amount1.OnAmountSelected += new WSI.Cashier.uc_input_amount.AmountSelectedEventHandler(this.btn_intro);
      // 
      // gb_Action
      // 
      this.gb_Action.BackColor = System.Drawing.Color.Transparent;
      this.gb_Action.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.gb_Action.Controls.Add(this.uc_radioButton_add);
      this.gb_Action.Controls.Add(this.uc_radioButton_sub);
      this.gb_Action.CornerRadius = 10;
      this.gb_Action.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_Action.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_Action.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gb_Action.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_Action.HeaderHeight = 35;
      this.gb_Action.HeaderSubText = null;
      this.gb_Action.HeaderText = "XACTION";
      this.gb_Action.Location = new System.Drawing.Point(24, 418);
      this.gb_Action.Name = "gb_Action";
      this.gb_Action.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_Action.Size = new System.Drawing.Size(518, 112);
      this.gb_Action.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.gb_Action.TabIndex = 2;
      // 
      // uc_radioButton_add
      // 
      this.uc_radioButton_add.AutoCheck = false;
      this.uc_radioButton_add.BackColor = System.Drawing.Color.Transparent;
      this.uc_radioButton_add.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.uc_radioButton_add.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.uc_radioButton_add.Location = new System.Drawing.Point(31, 58);
      this.uc_radioButton_add.Name = "uc_radioButton_add";
      this.uc_radioButton_add.Padding = new System.Windows.Forms.Padding(10);
      this.uc_radioButton_add.Size = new System.Drawing.Size(205, 30);
      this.uc_radioButton_add.TabIndex = 4;
      this.uc_radioButton_add.TabStop = true;
      this.uc_radioButton_add.Text = "xAdd";
      this.uc_radioButton_add.UseVisualStyleBackColor = true;
      this.uc_radioButton_add.CheckedChanged += new System.EventHandler(this.uc_radioButton_add_CheckedChanged);
      // 
      // uc_radioButton_sub
      // 
      this.uc_radioButton_sub.AutoCheck = false;
      this.uc_radioButton_sub.BackColor = System.Drawing.Color.Transparent;
      this.uc_radioButton_sub.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.uc_radioButton_sub.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.uc_radioButton_sub.Location = new System.Drawing.Point(251, 58);
      this.uc_radioButton_sub.Name = "uc_radioButton_sub";
      this.uc_radioButton_sub.Padding = new System.Windows.Forms.Padding(10);
      this.uc_radioButton_sub.Size = new System.Drawing.Size(205, 30);
      this.uc_radioButton_sub.TabIndex = 3;
      this.uc_radioButton_sub.TabStop = true;
      this.uc_radioButton_sub.Text = "xSubstract";
      this.uc_radioButton_sub.UseVisualStyleBackColor = true;
      this.uc_radioButton_sub.CheckedChanged += new System.EventHandler(this.uc_radioButton_sub_CheckedChanged);
      // 
      // pnl_buttons
      // 
      this.pnl_buttons.BackColor = System.Drawing.Color.Transparent;
      this.pnl_buttons.Controls.Add(this.btn_cancel);
      this.pnl_buttons.Controls.Add(this.btn_ok);
      this.pnl_buttons.Location = new System.Drawing.Point(563, 551);
      this.pnl_buttons.Name = "pnl_buttons";
      this.pnl_buttons.Size = new System.Drawing.Size(380, 60);
      this.pnl_buttons.TabIndex = 29;
      // 
      // btn_cancel
      // 
      this.btn_cancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_cancel.FlatAppearance.BorderSize = 0;
      this.btn_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_cancel.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_cancel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_cancel.Image = null;
      this.btn_cancel.IsSelected = false;
      this.btn_cancel.Location = new System.Drawing.Point(0, 0);
      this.btn_cancel.Name = "btn_cancel";
      this.btn_cancel.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_cancel.Size = new System.Drawing.Size(155, 60);
      this.btn_cancel.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_cancel.TabIndex = 0;
      this.btn_cancel.Text = "CANCEL";
      this.btn_cancel.UseVisualStyleBackColor = false;
      this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
      // 
      // btn_ok
      // 
      this.btn_ok.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_ok.FlatAppearance.BorderSize = 0;
      this.btn_ok.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_ok.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_ok.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_ok.Image = null;
      this.btn_ok.IsSelected = false;
      this.btn_ok.Location = new System.Drawing.Point(222, 0);
      this.btn_ok.Name = "btn_ok";
      this.btn_ok.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_ok.Size = new System.Drawing.Size(155, 60);
      this.btn_ok.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_ok.TabIndex = 1;
      this.btn_ok.Text = "OK";
      this.btn_ok.UseVisualStyleBackColor = false;
      this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
      // 
      // gb_total
      // 
      this.gb_total.BackColor = System.Drawing.Color.Transparent;
      this.gb_total.BorderColor = System.Drawing.Color.Empty;
      this.gb_total.Controls.Add(this.lbl_operacion);
      this.gb_total.Controls.Add(this.lbl_result);
      this.gb_total.CornerRadius = 10;
      this.gb_total.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_total.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.gb_total.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_total.HeaderHeight = 35;
      this.gb_total.HeaderSubText = null;
      this.gb_total.HeaderText = "XTOTAL";
      this.gb_total.Location = new System.Drawing.Point(563, 418);
      this.gb_total.Name = "gb_total";
      this.gb_total.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.gb_total.Size = new System.Drawing.Size(380, 112);
      this.gb_total.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.TOTAL;
      this.gb_total.TabIndex = 30;
      // 
      // lbl_operacion
      // 
      this.lbl_operacion.BackColor = System.Drawing.Color.Transparent;
      this.lbl_operacion.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_operacion.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.lbl_operacion.Location = new System.Drawing.Point(3, 38);
      this.lbl_operacion.Name = "lbl_operacion";
      this.lbl_operacion.Size = new System.Drawing.Size(374, 21);
      this.lbl_operacion.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_WHITE;
      this.lbl_operacion.TabIndex = 27;
      this.lbl_operacion.Text = "$XXX.XX";
      this.lbl_operacion.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_result
      // 
      this.lbl_result.BackColor = System.Drawing.Color.Transparent;
      this.lbl_result.Font = new System.Drawing.Font("Open Sans Semibold", 32F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_result.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.lbl_result.Location = new System.Drawing.Point(3, 62);
      this.lbl_result.Name = "lbl_result";
      this.lbl_result.Size = new System.Drawing.Size(374, 46);
      this.lbl_result.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_32PX_WHITE;
      this.lbl_result.TabIndex = 26;
      this.lbl_result.Text = "$XXX.XX";
      this.lbl_result.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // gb_Buckets
      // 
      this.gb_Buckets.BackColor = System.Drawing.Color.Transparent;
      this.gb_Buckets.BorderColor = System.Drawing.Color.Empty;
      this.gb_Buckets.Controls.Add(this.dgv_Bucket_Grid);
      this.gb_Buckets.CornerRadius = 10;
      this.gb_Buckets.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_Buckets.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gb_Buckets.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_Buckets.HeaderHeight = 35;
      this.gb_Buckets.HeaderSubText = null;
      this.gb_Buckets.HeaderText = "XBUCKETS";
      this.gb_Buckets.Location = new System.Drawing.Point(24, 24);
      this.gb_Buckets.Name = "gb_Buckets";
      this.gb_Buckets.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_Buckets.Size = new System.Drawing.Size(518, 380);
      this.gb_Buckets.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.gb_Buckets.TabIndex = 32;
      this.gb_Buckets.Text = "xLastOperations";
      // 
      // dgv_Bucket_Grid
      // 
      this.dgv_Bucket_Grid.AllowUserToAddRows = false;
      this.dgv_Bucket_Grid.AllowUserToDeleteRows = false;
      this.dgv_Bucket_Grid.AllowUserToResizeColumns = false;
      this.dgv_Bucket_Grid.AllowUserToResizeRows = false;
      this.dgv_Bucket_Grid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.dgv_Bucket_Grid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
      this.dgv_Bucket_Grid.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.dgv_Bucket_Grid.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.dgv_Bucket_Grid.ColumnHeaderHeight = 35;
      this.dgv_Bucket_Grid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
      dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      dataGridViewCellStyle1.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
      dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
      dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
      this.dgv_Bucket_Grid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
      this.dgv_Bucket_Grid.ColumnHeadersHeight = 35;
      this.dgv_Bucket_Grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
      this.dgv_Bucket_Grid.CornerRadius = 10;
      this.dgv_Bucket_Grid.DefaultCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.dgv_Bucket_Grid.DefaultCellForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.dgv_Bucket_Grid.DefaultCellSelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.dgv_Bucket_Grid.DefaultCellSelectionForeColor = System.Drawing.Color.White;
      dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      dataGridViewCellStyle2.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.White;
      dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
      this.dgv_Bucket_Grid.DefaultCellStyle = dataGridViewCellStyle2;
      this.dgv_Bucket_Grid.EnableHeadersVisualStyles = false;
      this.dgv_Bucket_Grid.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.dgv_Bucket_Grid.GridColor = System.Drawing.Color.LightGray;
      this.dgv_Bucket_Grid.HeaderBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
      this.dgv_Bucket_Grid.HeaderDefaultCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.dgv_Bucket_Grid.HeaderDefaultCellForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.dgv_Bucket_Grid.Location = new System.Drawing.Point(-2, 34);
      this.dgv_Bucket_Grid.Name = "dgv_Bucket_Grid";
      this.dgv_Bucket_Grid.ReadOnly = true;
      this.dgv_Bucket_Grid.RowHeadersVisible = false;
      this.dgv_Bucket_Grid.RowHeadersWidth = 20;
      this.dgv_Bucket_Grid.RowTemplate.Height = 35;
      this.dgv_Bucket_Grid.RowTemplateHeight = 35;
      this.dgv_Bucket_Grid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
      this.dgv_Bucket_Grid.Size = new System.Drawing.Size(521, 346);
      this.dgv_Bucket_Grid.Style = WSI.Cashier.Controls.uc_DataGridView.DataGridViewStyle.WITH_BOTTOM_ROUND_BORDERS;
      this.dgv_Bucket_Grid.TabIndex = 0;
      this.dgv_Bucket_Grid.SelectionChanged += new System.EventHandler(this.dgv_Bucket_Grid_SelectionChanged);
      // 
      // frm_add_amount_multiple_buckets
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(965, 680);
      this.Name = "frm_add_amount_multiple_buckets";
      this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
      this.Text = "frm_add_amount_multiple_buckets";
      this.pnl_data.ResumeLayout(false);
      this.gb_Action.ResumeLayout(false);
      this.pnl_buttons.ResumeLayout(false);
      this.gb_total.ResumeLayout(false);
      this.gb_Buckets.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.dgv_Bucket_Grid)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private uc_input_amount uc_input_amount1;
    private Controls.uc_round_panel gb_Action;
    private System.Windows.Forms.Panel pnl_buttons;
    private Controls.uc_round_button btn_cancel;
    private Controls.uc_round_button btn_ok;
    private Controls.uc_round_panel gb_total;
    private Controls.uc_label lbl_result;
    private Controls.uc_radioButton uc_radioButton_add;
    private Controls.uc_radioButton uc_radioButton_sub;
    private Controls.uc_label lbl_operacion;
    private Controls.uc_round_panel gb_Buckets;
    private Controls.uc_DataGridView dgv_Bucket_Grid;
  }
}